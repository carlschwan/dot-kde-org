---
title: "Kopete: Instant Messaging for KDE"
date:    2003-08-18
authors:
  - "aexojo"
slug:    kopete-instant-messaging-kde
comments:
  - subject: "Excellent work!"
    date: 2003-08-18
    body: "I've been using Kopete ever since 0.6 and while it still crashes on occasions, it's pretty stable overall.\n\nAs far as the Ctrl+Enter behavior goes; I think you should allow the user to specify that in each plugin module. I for one like to hit enter and that's it; and I wasn't able to figure out how to do that so I got used to ctrl+enter now :)\n\nI hope you support file transfer (send and receive) in all protocols.\n\nGood luck!"
    author: "Jasem Mutlaq"
  - subject: "Re: Excellent work!"
    date: 2003-08-18
    body: "when in a chat window, go under the settings menu, select Configure Shortcuts and you can set the shortcut for Send message"
    author: "Aaron J. Seigo"
  - subject: "It is really getting better"
    date: 2003-08-18
    body: "\nThanks for the great work, it used the crash quite often in the past, but since release 0.6 I am really happy with it. Being invited to a yahoo group chat still crashes kopete 0.7.1 though.\n\nRegards, Joerg.\n"
    author: "Joerg Morbitzer"
  - subject: "Eheem"
    date: 2003-08-18
    body: "There are many similar intant messenger tools for KDE, hmm? Which are usable?"
    author: "Gerd"
  - subject: "Re: Eheem"
    date: 2003-08-18
    body: "And how many of them are multi-protocol?"
    author: "Anonymous"
  - subject: "gaim vs kopete??? (feature sets)"
    date: 2003-08-18
    body: "ok, i dont wanna start a flamewar, but, i use gaim, and i find it pretty good... so why should i try kopete...  real question, no trolling, no flaming...\n\ni mean, if its got something that gaim doesnt, then maybe i should switch... superficially, kopete's icon looks a lot better than gaim... but i wanna know feature set difference...\n\nim not kidding guys about this question... neither am i trolling or hoping to start a flamewar... just a real question...\n\nthanks..."
    author: ":-)"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2003-08-18
    body: "Why? KDE look-n-leel and integration like the upcoming addressbook integration."
    author: "Anonymous"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2003-08-18
    body: "Because kopete is 10 times better (to me).   It fits in to my KDE desktop far better.   It responds to all my kde settings, it matches the look and feel, the way it works.   It's prettier.  It has those really nice notifications when I get a message.   Basically, it's just a really cool program, and I can't even imagine the torture of having to use gaim after having used kopete.\n\nOk, ok - I guess you can tell that I really like it.   But it's important to me that it fits into my kde desktop."
    author: "TomL"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2003-08-18
    body: "I like kopete better than gaim, but i find myself using gaim because kopete still crashes a lot.. I can't keep it up for more than a few hours! I know that gaim has been around for five years and kopete is quite immature, and I think it's a wonder that kopete has gotten so far in such a short time, but I'm waiting for a stable release of kopete before I switch full time from gaim."
    author: "anon"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2003-08-18
    body: "I have not used gaim in ages, but a couple of thoughts. \n\n<PRO>\nKopete integrates nicely with KDE.\nIt is easy to use.\nHeaded in the right direction rather quickly.\n</PRO>\n\n<CON>\nStill an alpha level. Tried conference on yahoo the other day. quick death\nsmb windows -> qd\nspell checker -> major flaws.\nNo file sending or receiving.\nmemory issue, slowly use more and more; every few days I have to restart kopete.\n</CON>\n\nSo why do I keep useing it. \nOne interface to all these is very nice. \nI actually thought about switching to jabber.\nBut it is the approach of minimallism."
    author: "a.c."
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2003-08-19
    body: "What a waste...Kopete still stinks, but it's an awesome test bed for new KDE integration. The program just stinks right now. Gaim is amazing, but doesn't integrate that well with KDE. Oh well, I will continue using it, because:\n\nA.) It's a much more mature product, and works accordingly.\nB.) Has a Windows port (what I have to use at work)\nC.) Has many more features.\n\nPros for Kopete:\nA.) Integrates well into KDE.\nB.) Has KDE look and feel\nC.) In the future, will be MUCH better, with addressbook etc.\n\nOk, then, in the future I may use it, but for now, I'll go with the product that works better with a proven track record.\n"
    author: "Joe"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2003-08-19
    body: "hell, i'd even use kopete right now if it didn't randomly crash :(\n\nI know it's gone through a lot of big architectural changes, but I think it's time for a stablity track for 3.2 :)!"
    author: "anon"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2003-08-19
    body: "I'm using Kopete exclusively for more then a month now. It crashed maybe 3-4 times, but I just restart it and continue the conversation :) it's much more stable then Licq which is what I used before."
    author: "Anonymous"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2007-04-07
    body: "I use Kopete all the time and it hardly ever crashes. Of course I only use one account on it (MSN).\n\nBut again, you guys are just saying what you do or don't like and nothing about why you like or dislike them or much of anything about the features. I'm going to have to look elsewhere to get more information."
    author: "anonymous"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2004-01-20
    body: "idealistically you should get both, try them both out for a while and ditch one of them after a while.  Trying out new things is what makes the computer industry so popular"
    author: "linux student"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2004-03-08
    body: "I used to use Gaim. But then I compiled KDE 3.2. It really is beautiful. Hallelujah... Thanks KDE peeps. The arts issue does suck though. Well, I'm not really into sounds on my pc.\n\nSo Kopete is running flawlessy now. I notice no bugs yet. I'm not a major chatter. So you better find out yourself."
    author: "on the edge"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2005-06-22
    body: "I think one of the biggest hassles with instant messaging is that it\u0092s tied to the computer. If I\u0092m away from the computer, like watching tv, I may miss an important IM. I can leave the speakers on the PC really loud, but then I\u0092m always jumping up and running back to the PC to read an incoming IM in case it\u0092s important. Most of the time it\u0092s not. I can subscribe to a service with my cell phone\u0097but that costs money. So here is (I think) the perfect solution, and a good GAIM plug-in that\u0092s not too much work.\n\nI just installed a Pluto Home system (plutohome.com). It\u0092s a free, open source smarthome and media server. You put Bluetooth dongles on all the pc\u0092s in your house, and then when you enter a room your Symbian Bluetooth phone turns into a remote control for everything in that room. It already tracks your movement\u0097if you start listening to music in 1 room, your music will follow you as you move with your phone to another room. And it already sends messages to the phone based on events. For example, when the song changes, the cover art shown on my phone changes to show me what\u0092s playing.\n\nSo that got me thinking\u0085 Why not make a GAIM plugin for pluto so that whenever I get an IM, I see it on my Bluetooth phone? That way I can either type a reply on the phone, or go back to the computer if I want to use the keyboard, or ignore it if it\u0092s not important. And I\u0092m not having to run back and forth to the computer to check IM. And it\u0092s free since it uses Bluetooth! Plus, I think it\u0092s such a real convenience it would be a great way to get people to switch to GAIM.\n\nI talked to the programmers at Pluto and they said it would be really easy since their stuff is already written in small modules and plugins. However, since we\u0092re all open source, we could also just take whatever pieces were useful and do something completely new using the same concept.\n\nMany of the other GAIM projects listed are either specific for only some users (like Apple iChat), or would only be used by geeks (like the Perl interpreter). But not having to run back and forth to the computer is something everybody wants (imho). I don\u0092t see how to recommend a new idea for GAIM, so I\u0092ll just try the forums and hope somebody else likes it too."
    author: "laurac"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2005-06-22
    body: "I hope you are aware that 1) you are responsing to a two years old article and 2) that this site is not about GAIM at all."
    author: "ac"
  - subject: "Re: gaim vs kopete??? (feature sets)"
    date: 2005-07-24
    body: "i liked that idea. and there should not be a time limit imposed on thoughts. if i had a bluetooth phone i'd go for it. i would love to play music on one , travel to the next room and it picks up where i left off (AND recieve the IMs im getting from the next room)"
    author: "Xander"
  - subject: "I can't seem to get to the article"
    date: 2003-08-18
    body: "Fatal error: Call to undefined function: page_header() in /home/httpd/vhosts/badopi.org/httpdocs/node.php on line 6\n"
    author: "TomL"
  - subject: "Sorry :-("
    date: 2003-08-18
    body: "Sorry :-( Now it should work.\n\nThe webmaster was absent those days, and he returned just yesterday night, when I sent the new to the dot.\n\nI'm talking with him just now (using kopete, of course ;-) ), and he told me that he's upgrading to drupal 4.2. The theme still doesn't works well.\n\nSorry for the inconveniences. Best regards."
    author: "Alex"
  - subject: "Now it works"
    date: 2003-08-18
    body: "It's just that we were upgrading drupal 4.1 to 4.2.\n\nI didn't know the article by Alex was hot."
    author: "trukulo"
  - subject: "Re: I can't seem to get to the article"
    date: 2003-08-18
    body: "Check this:\n\nhttp://www.badopi.org/?q=node/view/181"
    author: "jerry"
  - subject: "New URL!!!"
    date: 2003-08-18
    body: "Sorry!! :-(\n\nAs I said, the webmaster updated to drupal 4.2 recently, and the URL has changed a bit:\n\nhttp://www.badopi.org/?q=node/view/181\n\nSorry for the problems you have experienced.\nBesr regards."
    author: "Alex"
  - subject: "IM ioslaves"
    date: 2003-08-18
    body: "Hi,\n\nI tried kopete 0.6 and there were some problems, but I hope they are fixed now (reading on the comments it's far more stable now), and when I boot to lin i will be able to use kopete instead of aim.\n\nI like the integration that Netscape7 does and you have integrated borwser, mail and messenger. I will probably continue to use Netscape7 because I run on all platforms one and the same app with one and the same profile.\nHowever because most IM protocols these days keep your profile on the server may be it would be nice to have konqueror+kopete integration.\n\nLet me explain what Netscape7 does, they have protocol aim: may be also they have icq: since it also supports icq. Also I am reading on the dot that kopete will be the \"official\" IM for KDE in the upcomming 3.2. I would be happy if konqueror has kioslaves/protocols for aim: icq: msn: yahoo: etc. Just like it does with mailto: protocol.\nAlso since NS and IE support some pseudo protocols as aim: (if AIM installed IE will support), may be soon it will be normal for sites to place <a href=\"aim:buddy\"> just they do with a< href=\"mailto:buddy@aol.com\">.\n\nI am staying still crossplatform guy but want to feel comfortable when booting to linux, and of course will be waiting for the moment when konqueror will be ported to other platforms (+kopete eventually) and load the profile remotely exactly as NS does, for now NS is the crossplatform winner. (for now we have only KHTML/KJS, thanks to Apple)\n\nAnyway, I beleive this ioslaves will be nice for kde, more than that kde has ioslaves for anything you guess, so it's some kind of tradition to make slave something:.\nLong time I played with cgi: kioslave while trying to make XP like control center with control: kioslave (that opens php that runs kcm modules, it was interesting for me becasue some guy at the kdelook said needs help, and i found it cool) so it should be it's easy to make this kioslave.\nIf i have time I would contribute a slave but may be kopete guy will do it faster since knows his app. However if you tell me a commandline argument for kopete that opens a chat window if kopete is running (and runs copete if it's not running) i will do it myself. I guess there should be a command like:\nkopete -aim buddy\n(that aim:buddy url in konqueror would activate using aim: slave)"
    author: "Anton Velev"
  - subject: "Re: IM ioslaves"
    date: 2003-08-18
    body: "Integration should be what KDE gives you better for your desktop experience. Maybe just not yet?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: IM ioslaves"
    date: 2003-08-18
    body: "> Also since NS and IE support some pseudo protocols as aim: (if AIM installed IE will support), may be soon it will be normal for sites to place <a href=\"aim:buddy\"> \n\nThis should be easy.. not even ioslaves have to be created.. "
    author: "anon"
  - subject: "SIM"
    date: 2003-08-18
    body: "I've been using kopete for a while, but I discovered a wonderfull QT client named SIM (http://sim-icq.sourceforge.net/).\n\nI just love it, even if it's only for ICQ.\nplease give a shot to sim too."
    author: "Philippe"
  - subject: "Re: SIM"
    date: 2003-08-18
    body: "I really have to agree here - SIM is a really, really great, just outstanding client. But altough it is around for awhile now it still seems to be a bit unknown so I am not ashamed to do a bit of promotion here. :)\n \nOn the one hand it is kept as \"simple\" as former ICQ Versions (pre 2k) were but on the other hand you don't miss a single feature - chat, filetransfer, SMS, you name it, it's there. (just saw in CVS that even ICQ2Go alike visualization if the other side is just typing has been implemented...\n\nSo for everyone out there who hasn't at least tried it yet - http://sim-im.sf.net and give it a try. I would even prefer it over Trillian and the official ICQ Win clients.\n\nOh, and sure - it has got a QT GUI and integrates very nice into KDE!"
    author: "Anonymous Coward"
  - subject: "Re: SIM"
    date: 2003-08-25
    body: "SIM has a lot of features... just look around ;)\nSIM is also available for a lot of plattforms...\nA friend use kopete and I ask him for testing SIM and he will switch to SIM if AIM support is offered by SIM. Lets look forward...  \nThe CVS version of sim has now support for icq, jabber and msn. AIM support is planned."
    author: "Crissi"
  - subject: "Re: SIM"
    date: 2005-02-01
    body: "Sorry - SIM is a cool application for ICQ but doesn\u00b4t work for yahoo."
    author: "Franklin"
  - subject: "Not ready yet, but getting there!"
    date: 2003-08-18
    body: "I really want to replace gaim with kopete on my KDE desktop. However, yahoo plugin obviously needs some more work (too many bugs). Maybe a childish request but smileys also need improvement; they are an integral part of IM experience. People complain about stability but due to bugs and lack of functionality, I had not the opportunity to try it over significant amounts of time.\n\nIn its current form (0.7.1), Kopete is not ready for prime-time. I think it is getting there fast though, and it is nice to hear that many developers are working on it.\n\nWaiting for a stable fully featured version. Go Kopete team!\ntarelax"
    author: "tarelax"
  - subject: "Re: Not ready yet, but getting there!"
    date: 2003-08-19
    body: "I've noticed that PhpBB and some other open-source apps also have ugly smilies. What's with the smilies we see in UBB and in other IMs? Are they copyrighted or something?"
    author: "Anonymous"
  - subject: "Re: Not ready yet, but getting there!"
    date: 2003-08-19
    body: "What issues do you have with the yahoo plugin? IIRC, there are only approximatly 5 or so bugs and wishes, and I'm in the process of fixing the smiley issues right now (I'm the plugin maintainer). If you see bugs with the yahoo plugin, please file a bug if one doesn't exist or vote for one if you're having the same problem (Don't send bugs to me directly, since I can't keep track of them all). "
    author: "Matt Rogers"
  - subject: "Re: Not ready yet, but getting there!"
    date: 2003-08-20
    body: "My main problem with yahoo is it's lack of firewall support.\nAt work, the only way I can use yahoo is through the firewall.\nAs I understand it, you are working on porting libyahoo2 which\nshould include this functionality. This is the only thing\nholding me back from dropping gaim and switching to kopete\nfull time.\n\nThanks for your work so far.\n\n"
    author: "E Soder"
  - subject: "Pet peeve"
    date: 2003-08-18
    body: "A little pet peeve of mine regarding Kopete:\n\nFrom the Kopete FAQ (http://kopete.kde.org/index.php?page=faq)\n\n\"Kopete's name comes from the chilean word Copete.\"\n\nCopete is a spanish word. In Chile they speak spanish. Maybe that's where the confusion came from.\n\nHere's the link to the entry for 'copete' in the Real Academia dictionary:\n\nhttp://buscon.rae.es/draeI/SrvltGUIBusUsual?TIPO_HTML=2&LEMA=copete\n\nBy the way, Kopete, the program is really nice. Has not crashed on me yet."
    author: "Ariel Arjona"
  - subject: "Re: Pet peeve"
    date: 2003-08-20
    body: "Copete is surely spanish, but Kopete was named not in that context.\nCopete word is also used in chile to refer to alcoholic drinks. It is used as a noun (copete), and also as an adjetive (copeteado). I named it that way, because when I started the kdevelop project, it asked me a name for it. given the fact I was still learning, I thought it will not become nothing serious, and given I was drinking Pisco+CocaCola while coding, I entered Kopete just for fun. People here love the name. And I like how stupid it sounds pronounced by english speakers :-)"
    author: "Duncan Mac-Vicar P."
  - subject: "Re: Pet peeve"
    date: 2008-07-23
    body: "I have a question. Are the creators of Kopete Chileans???"
    author: "CAMILO"
  - subject: "Re: Pet peeve"
    date: 2008-07-23
    body: "The original author is indeed Chilean, though he hasn't been around much the last few years.\n"
    author: "SadEagle"
  - subject: "Re: Pet peeve"
    date: 2008-07-23
    body: "Erm, and he would be the one whose post you replied to. \n"
    author: "SadEagle"
  - subject: "Re: Pet peeve"
    date: 2008-11-19
    body: "I think this word is really funny in my language (Indonesia). If you take the \"pete\" out of the word (scientific word: Parkia speciosa), it will mean a plant used in culinary. When u eat this \"pete\" (actually, the official word is \"petai\"), you will get bad breath and sometimes when u (sorry) pass a gas, it will be very terrible."
    author: "Alvin"
  - subject: "Re: Pet peeve"
    date: 2008-04-12
    body: "You should lobby the Royal Academy to include the Chilean slang meaning of \"copete\" in their dictionary. :-)"
    author: "Jordi Guti\u00e9rrez Hermoso"
  - subject: "Idea"
    date: 2003-08-18
    body: "How about an option to embed Kopete in a Konqueror sidebar? That'd be awesome :D."
    author: "Sho"
  - subject: "Re: Idea"
    date: 2003-08-19
    body: "Yes, that'd be nice indeed. Especially now due to bugreport 56945 which Joseph realized by creating a sidebar applet for Kicker in CVS. =)"
    author: "Datschge"
  - subject: "Re: Idea"
    date: 2003-08-19
    body: "I'd just *love* to surf around with Konqueror and have Kopete in a sidebar panel of the same window."
    author: "Sho"
  - subject: "Kopete still doesnt do file transfers!!!"
    date: 2004-01-03
    body: "Even at the KDE 3.2 kopete level, you still dont get file transfers.\n\nI mean, i love kopete, the way it works the way it looks, but cmon!!!! An instant messenger MUST have file transfers, its not an option, its a requirement!\n\nUntil Kopete gets file transfers, noone will take it seriously.\n\nAnd for all of you who are planning on replying that there are file transfers in kopete, the Send File feature hardly ever works. If i send lets say 5 files, usually only one gets through. And even with that feature, its still only send! You cannot receive any files.\n\nI think when this program was created, file transfers should have been one of the first features implemented next to sending messages. How anoying is it to constantly have to go to email to send someone a mp3 or a picture."
    author: "Euphorix"
---
After using Kopete 0.7 for a while, I decided to write a <a href="http://www.badopi.org/?q=node/view/181">brief review of this instant messenger and its features</a> (<a href="http://es.kde.org/modules/news/article.php?storyid=129">Spanish</a> and <a href="http://bulmalug.net/body.phtml?nIdNoticia=1844">Catalan</a> editions also available).  The article has even been updated now that <a href="http://kopete.kde.org/">Kopete 0.7.1</a> has been released. Thanks to <a href="http://www.keirstead.org">Jason Keirstead</a> for <a href="http://lists.kde.org/?l=kopete-devel&m=106096386427629&w=2">correcting mistakes</a> in the article.  Hope you enjoy it!

<!--break-->
