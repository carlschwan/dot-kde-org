---
title: "Interview with Shamyl Zakariya on SlicKer development"
date:    2003-01-19
authors:
  - "binner"
slug:    interview-shamyl-zakariya-slicker-development
comments:
  - subject: "I liked this quote..."
    date: 2003-01-19
    body: "\"I need to do more because I've run and adored KDE since the 1.x days, and when I don't submit patches I feel like a freeloader.\"\n\nProbably a common sentiment around here....\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: I liked this quote..."
    date: 2003-01-19
    body: "I lack both the talent and the time to learn how to code for KDE. Still, It's my desktop of choice and I try to advocate whenever I can and is apropriate.\n\n"
    author: "zelegans"
  - subject: "Re: I liked this quote..."
    date: 2003-01-19
    body: "I didn't mean it to be negative. I feel like a freeloader myself. Many others do also.\n\nYour comment summed up my feelings towards free software.\n\nDerek "
    author: "Derek Kite"
  - subject: "Re: I liked this quote..."
    date: 2003-01-20
    body: "There are plenty of ways to contribute to KDE even if you can't code or compile the cvs version. Promotion, website, documentation, organisation, usability, interviews, bug reports, advertising, translation, ...\n\nIf you would like to contribute some of your spare time although you don't have any coding skill, please drop me a mail (or better, to kde-promo). We can work something out together.\n\nKDE always needs more resources.\n\n    Philippe "
    author: "Philippe Fremy"
  - subject: "Re: I liked this quote..."
    date: 2003-01-20
    body: "Two words: bug reports.  You don't need to know jack beans about C++, and KDE has a nice menu system for automagically filling out most of the necessary information."
    author: "Secondsun"
  - subject: "Awesome!"
    date: 2003-01-19
    body: "I've been muttering for a while about the Kicker and how cluttered and awkward it feels. This UI you have been developing looks awesome. I'll use this as soon as it is available in a release!!!"
    author: "Will Stokes"
  - subject: "A little OT..."
    date: 2003-01-19
    body: "KDE 3.1.1 is out!!! ;P\nhttp://lists.kde.org/?l=kde-core-devel&m=104294664602730&w=2\n(Post from after 3am. Poor Mueller obviously need some sleep now, he definitelly deserves it.)"
    author: "Datschge"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "3.1.1? Why 3.1.1? Have I missed the 3.1.0 release?\nThx,\nFlorian"
    author: "Osiris"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "heh he corrected himself on the next message:\nhttp://lists.kde.org/?l=kde-core-devel&m=104294664602730&w=2\n\nPoor Dirk needs sleep.\n"
    author: "anon"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "Does anybody else have problems with the Konqueror:\nIn every form clicking on buttons have no effect at all.\nI would submit a bug report but I can't cause the \"Continue\" button is not useable for me.\n\nAlso still annoying: in Kate: if you change a file on disc you get notified twice that the file got changed. \n\nBut how to send a bug report if the bug itself prevents you from doing it?"
    author: "Peter"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "> In every form clicking on buttons have no effect at all.\n\nSuch things happen if you use CVS, that was fixed after some hours."
    author: "Anonymous"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "Hence I suggest we go for another Release Canidate\n(runs for life)"
    author: "IR"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: ">  But how to send a bug report if the bug itself prevents you from doing it?\n\nWell, have you considered using another browser to submit the bug report?\n"
    author: "Jesper Juhl"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "Guess how he must have \"Add\"ed a reply to dot.kde.org."
    author: "Anonymous"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "No, you can hit \"return\" in the title or name edit line, you don't need that button.\n"
    author: "me"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "Are there other browsers than the Konqueror? :-)"
    author: "Norbert"
  - subject: "Re: A little OT..."
    date: 2003-01-20
    body: "Well, I heard rumors about another browser on the way, I think it was called Subaru or similar, and evidently some big company is behind it.\n"
    author: "Johnny Andersson"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "From what I can see it KDE3.1RC7. And that is a good thing. A lot of things have bin fixed since RC6 and the last releas candidate should really be tha same thing as the final release. Just make sure that they wait a couple of weeks before they release the real thing. It takes quite a while to check everything out.\nPerhaps we can get a rock solid release for once."
    author: "Uno Engborg"
  - subject: "Re: A little OT..."
    date: 2003-01-19
    body: "RC7 is to be released as 3.1... it should hopefully be out today or tomorrow, yet."
    author: "Vic"
  - subject: "Nice!"
    date: 2003-01-19
    body: "This looks very nice indeed! I will definatly use this as soon as I can apt-get it :)\n"
    author: "eadz"
  - subject: "Re: Nice!"
    date: 2003-01-19
    body: "Yes, it's fantastic and something you don't know from Win...\nLooking forward to find it in an offical KDE release."
    author: "P."
  - subject: "Re: Nice!"
    date: 2003-01-19
    body: "Who says that it will be part of official KDE release? Or maybe not Slicker as whole but only the CardDesk part as Shamyl \"expects to run CardDesk alongside Kicker\" too?"
    author: "Anonymous"
  - subject: "Shweeeeet!"
    date: 2003-01-19
    body: "*Drool*"
    author: "Annonymous"
  - subject: "awesome, I'm on the dot"
    date: 2003-01-19
    body: "cool.. glad to see you people enjoying the interview.\n\n- Lovechild"
    author: "Lovechild"
  - subject: "I want Slicker!"
    date: 2003-01-19
    body: "Yes, Kicker is good. But it's nothing new. It does what it's supposed to do and it does it well. But everyone has something similar (GNOME, Windows, Mac OS...). Slicker is something different. And what's important: it's not different for the sake of being different. It actually seems highly functional.\n\nWhy not have talks with the Slicker-people and try to make it an official part of KDE? Users could then decide between traditional Kicker and Slicker. Maybe in KDE 3.2?"
    author: "Janne"
  - subject: "Two kicker replacements?"
    date: 2003-01-19
    body: "I was a little bit surprised, when I read that Shamyl Zakariya is developing a kicker alternative. Have a look at the kde 3.2 feature plan [1] and scroll down to Kicker (in the red section): \"Task oriented KMenu alternative Aaron J. Seigo <aseigo@olympusproject.org>\".\n\nDoes that mean that there are two projects? Or are you working together? Or am I just stupid and KMenu is not the same as kicker?\nThanks for any clarification.\n\n[1]: http://developer.kde.org/development-versions/kde-3.2-features.html"
    author: "Michael Jahn"
  - subject: "Re: Two kicker replacements?"
    date: 2003-01-19
    body: "Aaron's \"task oriented menu\" (TOM) is a Kicker *menu* alternative: http://urbanlizard.com/~aseigo/"
    author: "Anonymous"
  - subject: "Re: Two kicker replacements?"
    date: 2003-01-19
    body: "I think I understand the difference now.\nThank you."
    author: "Michael Jahn"
  - subject: "Sexy Screenshots!"
    date: 2003-01-19
    body: "Looks fantastic! This type of application has such a massive impact on one's desktop experience. Anyone who's prepared to do a bit of redesigning to make better use of those limited pixels, or to make the environment look sexier, deserves huge support! Trying out ideas should be what open source is all about.\n\n"
    author: "palefish"
  - subject: "One nitpick"
    date: 2003-01-20
    body: "This looks really nice, and I applaud the courage to try out new things.\nHowever, I personally don't like having launch buttons below the kmenu. With the current setup I can simply move the pointer all down left on the screen and click to fire up the kmenu. With launch buttons just below the menu, I'd have to move all down left and then carefully move slightly up to hit the menu."
    author: "Anonymous Coward"
  - subject: "Re: One nitpick"
    date: 2003-01-22
    body: "I think he's changed the design a bit so that the Big K sits again in the lower left corner, with nothing under. Check the site for the Flash animation. Looks quite nice actually."
    author: "Archie Steel"
  - subject: "Gorgeous"
    date: 2003-01-20
    body: "Shamyl seems to haves a rare and valuable mix of skills, both graphic designer and programmer. A designer who has the wherewithall to make his ideas happen.  Certainly the screen shots look fantastic. The idea sounds great. I hope it plays out as well as its sounds.\n\nWould this be a candidate for 3.2 or later?"
    author: "David O'Connell"
  - subject: "I go out of town for three days and..."
    date: 2003-01-20
    body: "Heh. I just got back in town; I spent the long weekend in philly with my GF and her friends and family. I actually spent a few days without thinking about CardDesk.\n\nSo, anyway, I have to say, just in case I don't make it completely clear what the distinction between CardDesk and SlicKer is... CardDesk is a framework and API, SlicKer is a kicker replacement which will be written on top of CardDesk.\n\nAnyhow, I want to let you all know I'm excited that you all are excited. It's fun to do some work which people can share in. Most of my programming experience is writing code for myself, to scratch my own itches. This is a nice change.\n\nWhen I release an alpha of CardDesk, I intend to include a handful of applets. The two main applets which excite me however are the konq filemanager applet and a \"scratch pad\" applet which will basically be a cut-n-paste and drag-n-drop repository. It will be a place you can drop off data types for retrieval later. E.g., you have a page or two in an essay you're writing which you want to rewrite. Well, you'll cut the text and paste it into the scratch pad and it'll stay there with everything else you've pasted until you explicitly delete it. You ought to be able to drag in images, text, urls, whatever. Anything you can cut and paste or drag and drop. \n\nI can't say off the top of my head what else I'll write. I'll just go where the wind takes me."
    author: "Shamyl Zakariya"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-01-21
    body: "Just an ideas about possible applets ;-)\n\n- IM (ICQ!) support with seeing how much messages (and from whom) there are (this means mainly integration with Licq/SIM/gaim etc. - nowadays it's much more better but still not perfect - best would be one general interface (XML?) for all such apps)\n\n- support for multiple mail/news accounts \n\n- remote machines monitoring (I mean - I have a Linux server somewhere outside and I'd like to see load, free memory, connected Samba/FTP/WWW users etc.)\n\n- good integration with PIM (when I got alarm possibility to reschedule it after few minutes, to-do list etc.)\n\n- Xinerama (multiple monitors) support - possibility to have SOME parts on diferent screent (and possibility to have some applets 2-3 times ;-))\n\nLooks very cool - very good job, now just tell us where we can download it .-)\n\n"
    author: "Abraxis"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-01-21
    body: "Actually, I have been thinking about the PIM route. The app I run the most in KDE is the adressbook client since I have a terrible memory.\n\nThe others, like mail and remote monitoring might be outside of my capability, since I'm not a network programming buf. Hell, I never had a decent network connection until last year. But it ought to be easy for others to write such plugins ;)\n\nBut, here's one thing that's important: My current codebase has an ABSOLUTELY broken layout management system, with respect to how the card elements are stacked and rotated. It works, but not well. Stacking cards with different widths works, generally, but not reliably. It's all my fault because I didn't properly understand some of the subtleties of QLayout\n\nAs of last night I finished writing a new set of classes which work more clesely with proper Qt layout management to handle this better. My tests work great, and after I've tested them more thoroughly I'll integrate it into the codebase. The good news is that once it's integrated, I ought to be able to cut out perhaps as much as 500 lines of code, since I'll be able to kill the LayoutSwitcher class and assorted hacks to make it work.\n\nUntil I get the layout fixed, I'm not going to progress. I'm more concerned with  the system working right, than moving forward."
    author: "Shamyl Zakariya"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-01-21
    body: "Wow, looks pretty cool!"
    author: "Marlo"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-01-22
    body: "I am glad to hear CardDesk will be a framework. Will the applets you release work across other WM's. I use enlightenment and like it, but the cards look like a great idea. I would love to use them in enlightenment. If you want someone to test you applets on enlightenment I would be glad to help.\n\n--Nate"
    author: "GnCuster"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-01-22
    body: "A fella on the gentoo forums tested it against several different WMs; it worked great under all but Metacity and WindowMaker; it basically didn't work under Metacity, and was dog slow under WindowMaker.\n\nI don't know about enlightenment... I don't think he tested against that. This doesn't require KDE to be running anyway, just that you have kdelibs installed."
    author: "Shamyl Zakariya"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-04-28
    body: "Cool!\nYoure on Linux now...\nWell so I dont ask you if you going to do something for ZETA?\n( http://www.yellowtab.com )\nRegards..."
    author: "Pasha"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-04-28
    body: "What do you mean? Are you suggesting that I join work on zeta? How, it's closed isn't it?\n\nAnyway, I'm on mac osx now... "
    author: "Shamyl Zakariya"
  - subject: "Re: I go out of town for three days and..."
    date: 2003-01-26
    body: "Are you going to make an option for the cards to be transparent? And how about them flashing if that applet needs attention (maybe different colors)?"
    author: "Aaron krill"
  - subject: "Re: about the KDE Menu"
    date: 2003-01-28
    body: "I've seen the marvelous shots and concept drawings, and I've got an idea for the K Menu, instead of making it a special card, why don't you include it in\n the clock and task area(the little panel at the bottom right of the desktop in the large drawing) this little panel could be moved and all the basic functions (Pim,status bar, Menu and quick terminal) will be in the same accesible place"
    author: "ra1n"
  - subject: "What a nice desktop"
    date: 2003-01-20
    body: "I really feel impressed by this desktop, it looks highly useable and rather nice.\nIt would be cool to add a new voice in the macOS-XP struggle for nice GUIs, a linux one.\nI notice several projects theses days that propose radical changes to linux desktops and \nvery good ideas to enhance human computer interaction in new ways. It's good to see that the free software movement starts getting it's R&D departments working on \"massively adopted\" projects like KDE. This can change the *nix desktop and maybe more. \n\nI think user interaction is the achilles' heel of linux,we all know it...\n(remember when you wanted your mother, sisters and friends to use linux)\n\nI am sure many people out there would appreciate to be able to compile and test and give feed back about Slicker (ok, I am lying, I just want to use it.. :) ).\n\n"
    author: "socialEcologist"
  - subject: "Associate virtual Desktops with cards"
    date: 2003-01-21
    body: "I would really like, if one could associate a virtual desktop with a card. This assosiations should be able to be configures, so you can have cards, that do not change the desktop (like Information) and one or more cards that change to a specific desktop (like MailCenter and OfficeCenter changing to the same \"OfficeWork\"-Desktop). Also one should be able to add shortcut-icons to a card (like in the menu-card) which are hidden while the card is collapsed. this would allow users to really cleanup their work-environments..."
    author: "cylab"
  - subject: "Re: Associate virtual Desktops with cards"
    date: 2003-01-21
    body: "I've been considering making cards be per-desktop. Right now they are across all desktops, but that's only because that's easiest for me at the moment. The one thing that I *can't* do however is support xinerama in a meaningful way, since I don't have a dual head machine (just a thinkpad).\n\nBut, if a card is not present on all desktops, it will be present on only one desktop. I don't think I can make a card be visible on desktops 1 & 3 but not 2 & 4. It's all or one, as far as I can tell."
    author: "Shamyl Zakariya"
  - subject: "Re: Associate virtual Desktops with cards"
    date: 2003-01-22
    body: "hmm... for the problem, of one card on multiple, but not all desktops, you could consider to use different cards with the same content as workaround, but this may waste resources. a little bit more optimized would be, to only have a duplicate of the title bar on another desktop and move the original card to the active desktop if one clicks on the title of the duplicate.\n\nthe idea i wanted to present in the first place, was that the cards should replace the desktop-switcher. i assumed, that all cards are visible on all desktops, so  activating a card could be used to change to an associated desktop.\n\n"
    author: "cylab"
  - subject: "This is superb - I want it too :-)"
    date: 2003-01-22
    body: "This idea is just superb. Finally something to show these people talking all the time about how KDE only copies Windows interface ideas. And it seems *very* interesting from the usability point of view..."
    author: "Piotr Gawrysiak"
  - subject: "Not empty desktop?"
    date: 2003-01-22
    body: "The conceptual drawings and the first screenshots look great. But most of the time i have several windows open. Are the cards then hidden? Or do they overlap the windows? Cards and panels are great, but in the end workspace is what matters. The more the better. I, for example, just the start button rarely the only thing i use on the panel is the taskbar and the virtual desktop switcher.\nI hope it doesn waste too much space in the end - since it looks pretty cool.\n- Stephan"
    author: "Stephan"
  - subject: "Re: Not empty desktop?"
    date: 2003-01-22
    body: "This is something which, sadly, I just haven't addressed yet. The current behaviour is to be always on top, and to not hinder the positioning of maximized windows. Tabs can be small enough that they don't overlap too much, for example, less than 22 or so pixels -- in this case having the tabs on the top, left or bottom is fine because they won't overlap any important part of the maximized app. The right sid eis no good, though, because the tabs might overlap the scrollbar :P\n\nFrankly, I'm not certain what the best approach is. I might have a configuration option to allow for tabs to be always on the bottom (just above the desktop) but be raised when the mouse cursor touches a screen edge.\n\nAny ideas? I'm open to suggestions."
    author: "Shamyl Zakariya"
  - subject: "Re: Not empty desktop?"
    date: 2003-01-22
    body: "one solution could be to make the tabs half tranparent. a card should the switch to opaque and clickable if you hit the screen-border at a position, where this specific card touches it.\n\nthis however might provoke some unwanted behavior, like opening a card instead of dragging the scroll-bar. but thats a delay-balancing issue. also i am not entirely sure if this would \"feel\" right or gets a little bit annoying."
    author: "cylab"
  - subject: "Re: Not empty desktop?"
    date: 2003-01-22
    body: "Sorry, but proper transparency isn't an option under XFree, and likely won't be for a while. \n\nTechnically, yes, the core alpha compositing code is in place, but the actual X11 display system isn't designed to support transparency. I don't think we'll see it ever, personally, as it would involve a huge amount of work, with little or no tangible benefits aside from eyecandy.\n\nI appreciate the suggestion, though.\n\n"
    author: "Shamyl Zakariya"
  - subject: "Re: Not empty desktop?"
    date: 2003-01-23
    body: "Work for this is already underway (see Keith Packard's web site). XFree86 5 should see this released (i'm told this may be the release after 4.3).\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Not empty desktop?"
    date: 2003-01-23
    body: "The tabs in the left-bottom corner do not seem that big, so they would never be a real problem.\n\nFor the right-bottom menu, why not make it like a \"hidden kicker\", thus an arrow button. If you click it, the button-bar opens, and when you do something (in the screenshot, for example, change the sound volume, check your calender...) it automatically hides again."
    author: "Daan"
  - subject: "Re: Not empty desktop?"
    date: 2003-01-23
    body: "My suggestion is using hot key witch raise the cards above the othr apps:\nLike pressing somthing like ctrl+shift or somthing, and then the cards are viewabls, then chose the relevent card and leave the keyboard...\nwhen you not press the chosen key, the cards are coverd by other open windows.."
    author: "Ronen"
  - subject: "Screenshots unavailable"
    date: 2003-01-22
    body: "Unfortunately, the 'conceptual drawings' link is down right now, and the account hosting the screenshot is over-quota. Does anyone have these images in their cache still? Any chance of a mirror?"
    author: "lilac"
  - subject: "Re: Screenshots unavailable"
    date: 2003-01-22
    body: "they updated the slicker sourceforge homepage. check out\nhttp://slicker.sourceforge.net/screen.php"
    author: "Fabian Uffer"
  - subject: "Widget flicker"
    date: 2003-01-23
    body: "I know this is unrelated, but is there any ongoing work to reduce widget flicker in KDE? For example, when I switch between tabs in Konqueror in KDE 3.1rc5, there's a bit of flicker, with a blank background, and then the page is drawn to the screen. Or is there something that might be wrong with my X setup?\n\nThanks"
    author: "Praveen"
  - subject: "sweet, maybe awkward?"
    date: 2003-01-23
    body: "maybe I've been indoctrinated by the ever-present horizontal bar (taskbar, panel, whatever its called) at the bottom of the screen of windows, and kde ... whenever I come across panels which don't fully extend from the bottom left of the screen to the bot. right- panels, which in other words leave a gap at the base of the screen (eg, Mac OSX, or in KDE when users adjust the panel to not take up the entirety of base of the screen, and in this slicker) ... I don't know how users overcome the awkward look of maximized applications in such setups (I actually don't know how Mac OS X handles this)\nAnyway....\nIt seems like, in slicker, a maximized application would leave an awkward gap of desktop in the middle at the base of the screen..\nOtherwise, I've never seen a more beautiful panel...\n\n(speaking of beautiful, the new mozilla-xft blows my mind)\nphilip\n"
    author: "philip"
  - subject: "Re: sweet, maybe awkward?"
    date: 2003-01-27
    body: "OS X is designed in such a way that I almost never feel a need to maximize a window.  I don't really know how to describe this, but the application design really does make it so that I don't need maximization."
    author: "Dave"
  - subject: "Screenshots of card applets in development"
    date: 2003-01-24
    body: "Screenshots of card applets in development are on http://web.umr.edu/~dgaston/"
    author: "Anonymous"
  - subject: "Looking good"
    date: 2003-01-25
    body: "I want this desktop ;-)\n"
    author: "Jesper Juhl"
  - subject: "Looking wonderful!"
    date: 2003-01-28
    body: "Please give us this in KDE 3.2!! That would be wonderful!"
    author: "jonas"
---
<a href="http://www.tinyminds.org/">tinyminds.org</a> is featuring an <a href="http://www.tinyminds.org/modules.php?op=modload&name=News&file=article&sid=928&mode=thread&order=0&thold=0">interview</a> with <a href="http://home.earthlink.net/~zakariya/">Shamyl Zakariya</a> on the development of <a href="http://slicker.sourceforge.net/">SlicKer</a>, an alternative to KDE's Kicker panel. <i>"SlicKer intends to provide similar functionality [as Kicker], but in a more modular, task based kind of approach; one "card" per logical function. There will be a kmenu card, a taskbar card, a system tray etc card, and so on."</i> <a href="http://slicker.sourceforge.net/screen.htm">Conceptual drawings</a> and a <a href="http://home.earthlink.net/~zakariya/files/carddesk/screenshot9.png">first actual screenshot</a> are available. <i>"I can honestly say that I'll be releasing an alpha for CardDesk in a month or two. When I say alpha, I mean feature complete, but not necessarily API stable or for that matter, stable (as in, not crashing)"</i>.
<!--break-->
