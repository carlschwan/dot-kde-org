---
title: "KDE Traffic #72 is Out"
date:    2003-12-25
authors:
  - "hpinto"
slug:    kde-traffic-72-out
comments:
  - subject: "Fisrt :)"
    date: 2003-12-25
    body: "I'm first :o)\nThanks to provide something for Christmas.\n\nMerry Xmas to all of you."
    author: "JC"
  - subject: "Janitor"
    date: 2003-12-25
    body: "new job titles?\n\n* Quality manager\n* human relations blablabla\n\nBe creative to create job titles, just look at the newspapers. \nOr read the V-model paper.\n\n\n:-)"
    author: "Hein"
  - subject: "Re: Janitor"
    date: 2003-12-25
    body: "http://www.v-modell.iabg.de/\n\nthis one?"
    author: "Gerd"
  - subject: "Re: Janitor"
    date: 2003-12-25
    body: "Yes, the so called teutonic cruelity. Get KDE.org ISO 9001 certified, kotz :-)\nBut the federal government requests this model as a standard of software engineering. I guess that's why some software engineers supported software patents, because they like bureaucracy. \n\nHowever the German diplom paper of Anita Pelka looks good (linked on the page). Perhaps some case support for quality management would be not bad at all.\n "
    author: "Hein"
  - subject: "Re: Janitor"
    date: 2003-12-26
    body: "The name probably will be Quality Team Member. Janitor has a nice twist because of the wonderful work of the linux kernel janitors and it is humorous, but I don't think it is nice for non developers. It was a working name anyway.\n"
    author: "cwoelz"
  - subject: "Plastik still not \"default\"?"
    date: 2003-12-25
    body: "I don't get it. Plastik is a way better and much cleaner looking default theme. It's so much better and cleaner than Keramik (which I never liked).\n\nCan't they at least make it an option in the KPersonalizer?"
    author: "Emiel Kollof"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "i vote for plastik as default too \n\n"
    author: "chris"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "> Plastik still not \"default\"?\n\nYes, I'd love Plastik to be default in kde 3.2, but it simply isn't possible.\n\nThe problem is that screenshots have to be remade for fifty different languages in the help manuals. It's a big PITA. If you're wondering about this, it's standard kde documentation policy to have all screenshots in the default style. Before KDE 3.0, I beleive, this wasn't the case, and you had a hodepodge of screenshots in different styles. There were actually a lot of bug reports from confused users.\n\nSo, the new default style will be decided early on in a release cycle. \n\n> Can't they at least make it an option in the KPersonalizer?\n\nI made a patch for this a few months ago when plastik was added into kdeartwork, but it was rejected because all of the kpersonalizer themes are based on the look and feel of other environments. Makes sense I guess. Somebody can make a kpersonalizer setting for a Luna/XP-ish using Plastik I guess.. *hint hint*."
    author: "anon"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "Can someone explain me why many people are fond of plastik?\nI tried it to check it out, but, with the limited knowledge I have of Microsoft products, it looks too much like the new Windows style, minus the absurd color scheme.\nIf this is the case, I can't think of using it as the default style, KDE should use something original. \nPersonally, I like the keramik widget style, but I don't use the window decoration with the same name."
    author: "Luciano"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "It's just clean and simple.  Easy on the eyes, not garish.\n\nPersonally, I prefer Alloy, since I use alloy for my java applications.\n\nI *think* plastic was derived from alloy, but I'm not sure."
    author: "TomL"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "> I *think* plastic was derived from alloy, but I'm not sure.\n\nnope.. it's written 100% from the ground up. Alloy is a nice style, but it could never be default in KDE.. it's look and feel is written by a company that sells it. "
    author: "anon"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "> I tried it to check it out, but, with the limited knowledge I have of Microsoft products, it looks too much like the new Windows style, minus the absurd color scheme.\n\nPlastik looks like Luna as much as Keramik looks like Aqua; that is, not very much. It's as original and unique to KDE as Keramik is.\n\nPeople generally like Plastik because it presents a clean and elegant style for KDE; something that are great qualities in a default style. I've used Keramik as my style for bouts of time, but like many people, I've looked for something different for dialy use because Keramik simply feels way too bulky. "
    author: "anon"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "Well, before the screenshot showed strange configured KDE desktops, so I would rather prefer to show it as default. but nobody cares whether screenshots are plastik or Chrystal?\n\nThat's Schilda - style.\n\nPlastik shall be default regardless of screenshots..."
    author: "possi"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-26
    body: "I know this sounds like another lazy user requesting a feature, but I honestly don't have the time, and I do a fair bit already (QuickRip, NewToLinux.org.uk, various other little things), so please excuse me :)\n\nWouldn't it be possible to put together a hack to automatically do the screenshots? A bit of wizardry with pyKDE or KJSEmbed, DCOP and a specification for all of the screen shots, and you could generate new screenshots given a small amount of processing time and a full installation of KDE, rather than a lot of wasted time with ksnapshot."
    author: "Tom"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-27
    body: "You could probably even more hackishly with a bunch of QCursor calls.. prone to breakage tho :-)D"
    author: "anon"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2003-12-30
    body: "AMEN!\nmake an automated script, so that you can do screen shots in whatever style is *BEST*, and then use the same script to do all of the screen shots in every supported language."
    author: "jcd"
  - subject: "Re: Plastik still not \"default\"?"
    date: 2004-02-19
    body: "Well, I've just installed KDE 3.2.0 and I can't see plastik or dotNet here to choose, only the old styles, however they're inside the kdeartwork package. What's happening?"
    author: "Paulo Igor"
  - subject: "ObjC  & GNUStep integration?"
    date: 2003-12-26
    body: "Great Traffic update. If I want to develop with ObjC I either choose Cocoa or I use GNUStep with WindowMaker.  Being a NeXT alum I can say simply, \"WindowMaker is no WorkspaceManager.app, let alone NeXTSTEP/Openstep.\"\n\nIs there interest in including ObjC support as a first class citizen within KDE?\n\nGNUStep references that it can work within KDE but after several headaches the big stopper is the font management differences between these two environments.  I'm not interested in C++ unless perchance I could use ObjC++ and then make apps that work identically either in KDE or OS X Cocoa.\n\n"
    author: "Marc J. Driftmeyer"
  - subject: "Re: ObjC  & GNUStep integration?"
    date: 2003-12-26
    body: "Free Software: Everybody is free to contribute."
    author: "possi"
  - subject: "Re: ObjC  & GNUStep integration?"
    date: 2003-12-26
    body: "The kdebindings module contain a 'kdeobjc' subdirectory. I don't know whether it works or not, but it is surely a start point."
    author: "Henrique Pinto"
  - subject: "Re: ObjC  & GNUStep integration?"
    date: 2003-12-28
    body: "\"The kdebindings module contain a 'kdeobjc' subdirectory. I don't know whether it works or not, but it is surely a start point.\"\n\nYes Objective-C bindings are there, but I never quite got it working with strange runtime link problems, and it's been on 'permanent hold' for the past couple of years. But if you look at the headers, you can see how the KDE api can been rendered with a GNUstep Foundation kit/Objective-C combination. The main problem with wrapping with Objective-C is that non-Apple versions of Objective-C can't call C++ directly, so you have to go via another C binding layer which makes the whole thing large, and slow to load.\n\nI'd personally still like to do a KDE objc binding, but based on the 'Smoke' common language runtime library. It needs a small change to the Smoke api, but then the interface with C would be via about 20-30 C functions, rather than via an entire additional enormous C binding library. \n\n-- Richard"
    author: "Richard Dale"
  - subject: "Quality - Start with easier bug reporting"
    date: 2003-12-26
    body: "Hey, if you want quality, why make it so difficult to report bugs - I went to the bug site (had to find it first) to report some bugs found with 3.2 (we're probably one of a small group using it hard in a production mode) but when I went to the bug site, it wants a registration, a password, it even demands cookies - the heck with it.  I thought Linux was supposed to be about openness and feedback.  And I won't subject our computers to cookies, the epitamy of security and privacy issues.  fyi among other things Konq consistently crashes after renaming files 3.2b2 on Fedora."
    author: "john"
  - subject: "Re: Quality - Start with easier bug reporting"
    date: 2003-12-26
    body: "\"I went to the bug site...\"\nYou don't need to go to the web site. In any KDE app click Help->Rport Bug.\n\n\"it wants a registration,\"\nIf you had registered you'd understand that some features(like voting) need registration. It's also the best way to keep unwanted people out (spammers, trools, etc). You just have to register once, if that's to much trouble for you then I doubt your bug reports would be worth it.\n\n\"a password,\"\nObviously, so that other people wont use your account.\n\n\"it even demands cookies\"\nThat way you don't have to enter your login/pass all the time.\n\n\"And I won't subject our computers to cookies, the epitamy of security and privacy issues. fyi among other things Konq consistently crashes after renaming files 3.2b2 on Fedora.\"\nThis is not bugs.kde.org\n\n\n\n"
    author: "Random"
  - subject: "Re: Quality - Start with easier bug reporting"
    date: 2003-12-27
    body: "We had a register-free bug system before using Bugzilla. We had even so many spam through it that some ISP classified kde.org as spam site.\n\nSo I do not think that we ever will have again a register-free bug reporting system.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Quality Team Idea"
    date: 2003-12-26
    body: "Wow. I have to admit, I had no idea Carlos had proposed this. Looking at it, it is a very good idea. In many regards I have been trying to manage various aspects of this but formalizing it in a structure would be beneficial as everyone would receive the benefits of the procedural experiences of all. I have managed to get a lot of feedback through our user list, which I consider an essential for getting a good handle on how well we're doing with our interface and features. It also provides some filtering. However what any successful OSS project needs is someone there who is knowledgable about the project to filter in;ut and assist those leading the project. It is the only way to sustain communication with any large growth and we expect growth. So the mechanisms must be in place in advance.\n\nCarlos is by far our largest supporter and in my book a man of resolute action. I think he's been bitten by the same bug as me because I am never willing to say I've done enough and it seems he is not resting on his success either. Carlos may still have some to learn about how projects are run but the biggest challenge in open source is getting people to take action and find the time and commitment. In this area I believe Carlos offers inspiration to everyone.\n\nCongratulations on this great proposal Carlos. We need to be prepared for growth."
    author: "Eric Laffoon"
  - subject: "Re: Quality Team Idea"
    date: 2003-12-26
    body: "Eric, just a quick note, *please* don't take this as a flame: You should stop comparing everybody you read about in these publications to yourself publically. It always seems like you intend to \"steal thunder\" or make sure people remember you. I don't think this creates a favorable impression. \n\nThere was some talk about this over here, too: http://dot.kde.org/1071007157/1071175697/1071266419/\n\nTotally off-topic, though, so I'll shut up now."
    author: "Eike Hein"
  - subject: "Re: Quality Team Idea"
    date: 2003-12-27
    body: "Yes, you are off topic, but Eric is on topic because:\n1) He is the fist quality team member: he does exactly what a quality team should do for Quanta, looking to all issues related to his application.\n2) He is giving concrete feedback on the proposal stressing the importance of user lists.\n3) It is Quanta related: I am a contributor and said it in the interview."
    author: "cwoelz"
  - subject: "Re: Quality Team Idea"
    date: 2003-12-27
    body: "> Eric, just a quick note, *please* don't take this as a flame: \n\nHow do you take such a preface?\n\n> You should stop comparing everybody you read about in these publications to yourself publically. It always seems like you intend to \"steal thunder\" or make sure people remember you. I don't think this creates a favorable impression. \n\nI think you're being over sensitive and allowing your predisposition to color your thinking as if it is everyone's. It is also actually directed at who I am, whether you realize it. Who I am has everythig to do with why I pursued Quanta.\n\nCarlos stated he is a sponsor of our project. I was commenting from that perspective. Anyone who actually does more than gloss over what I have to say knows that I give the credit to others. Ask anyone on one of our mailing lists who sees me interacting or one of our contributors. I consider myself as someone doing nothing special. I recognize that people like to associate the accomplishments of our project with me because it is human nature. \n\nPlease don't think I mean to insult you by my comments about your post but I think it demonstrates a lack of understanding. People remember me because I consider it important to promote the Quanta project. If you don't think a complex project needs promotion why do you read the Dot? What did you think it's mission was anyway? KDE needs more promotion, not less. Research has shown that people need repeated exposures to remember and since I post on the Dot it is merely the fact that I'm here that creates that familiarity. The implication is that I should stop posting in order to create a more favorable impression of myself. That's something to ponder. ;-) I see that you're not flaming here by the absence of four letter words. You should think about what the words you say mean... Without the benefit of detailed guidelines of what you consider acceptable limits of my free speech it appears you want me to shut up, but not consider that a flame. It actually doesn't upset me because it's such delicious irony... if only it were April 1.\n\nI doubt that many people know who Carlos is, which is a shame. I consider him to be someone of exceptional character and commitment. Our views are very much in alignment on F/OSS. I felt that it would be important to bring forward my endorsement of his effort. It was all about my respect for him and my well wishes. I'm sorry if you feel that my endorsement is a liability. If it's any consolation I believe that's a minority opinion. BTW don't take that as a flame. ;-)\n\nTo steal someone's thunder I would have to have some sort of announcement and would have to try to divert people's attention from his message. If anything your silly assertion does that by forcing me to defend my right to make a statement endorsing him. My apologies to Carlos.\n\nI have a counter suggestion. If you are bothered by my promoting or have enough time to get bored with your reading why not get more active? Then you can get excited about what you're doing and (this is the part I like) read someone critisizing you about what you're doing, even though they like what you did. \n\nMake sure not to take it as a flame. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Quality Team Idea"
    date: 2003-12-27
    body: "Eric,\n\nYou really don't have to get this much detail to answer this ;)"
    author: "cwoelz"
  - subject: "Re: Quality Team Idea"
    date: 2003-12-27
    body: "> How do you take such a preface?\n\nWell, in this case I hope you took it verbatim: It was not intended as a personal attack of some sorts, but merely as feedback from my - subjective, by nature! - point of view. Actually, it was intended to be constructive in nature. I have no desire to harm Quanta or KDE. I enjoy using both tremendously. \n\n\n> I think you're being over sensitive and allowing your predisposition to color your thinking as if it is everyone's. It is also actually directed at who I am, whether you realize it. Who I am has everythig to do with why I pursued Quanta.\n\nI don't think I'm overly sensitive about this - I never said it's the end of the world, for example :). I just don't think you're doing yourself a favor with these posts. I know what impression they have on me, I've read similar comments by others, and I've come to above conclusion. I am entitled to my opinion, am I?\n\n\n> Please don't think I mean to insult you by my comments about your post but I think it demonstrates a lack of understanding.\n\nJust as you're entitled to yours. I have no problem with that and I do value it.\n\n\n> To steal someone's thunder I would have to have some sort of announcement and would have to try to divert people's attention from his message\n\nBut that's exactly what you're doing, again, from my subjective point of view: In the \"Linux Show\" discussion, you compared your skills to Aarons, here you're comparing your skills to Carlos'. I'm happy that you're confident of yourself. A leader needs to have confidence. I don't even deny that you have very good reasons to be confident - I certainly don't have anything in my r\u00e9sum\u00e9 that compares to Quanta. \n\nNor do I want to deny you your right to write a statement endorsing him - nothing could be further from the truth! (I liked Carlos' proposal a lot, by the way.) You're making the decisions; you've earned your right to do that by working hard, it's not my place to question it, and I don't. However, I think I'm allowed to provide feedback. And I think your statement would've sounded a lot nicer without the comparision. It makes you appear ego-centric this way (<- I'm sorry if this sounds harsh; however it's not really harsher than calling me 'silly', and it's probably just as honest). If you want do praise Carlos, why talk about yourself?\n\nHowever, Carlos, you're apparently 100% OK with Eric's statement, and that's what truely matters. I'm just an ordinary user, and I don't want to blow this out of proportion. \n\n\n> I have a counter suggestion. If you are bothered by my promoting or have enough time to get bored with your reading why not get more active?\n\nThat's indeed a good suggestion. My programming skills are not up to the task of contributing any major parts of code (yet), however I'm striving and aware of the possibility to do smaller things. Among the best things Quanta does is provoke the desire to contribute in me. Oh, and I'm not bothered by you promoting *Quanta* at all. Quite on the contrary.\n\n\n> Then you can get excited about what you're doing and (this is the part I like) read someone critisizing you about what you're doing, even though they like what you did.\n\nDealing with annoying users is part of the job description, I guess ;-D.\n\nAnyway, thank you very much for your extensive reply. I do appreciate it.\n"
    author: "Eike Hein"
  - subject: "Re: Quality Team Idea"
    date: 2003-12-27
    body: "Wow, this is constructive.\nAnd I was fearing for flames!\n\nThank you for making yourself clear now. "
    author: "cwoelz"
  - subject: "Re: Quality Team Idea"
    date: 2003-12-26
    body: "Nice to see someone step up. I suspect that with someone organising things, the user community will jump in.\n\nTwo points he made in his note. One is the build environment. For meaningful contributions, it is critical that the contributor be able to test his changes. Konstruct is for released versions, not cvs. What is there available?\n\nHe mentions the i18n effort, which is by any measure a great success. Who started it? Where was the impetus, other than the obvious need by users? As a case study in building a successful community contribution network, we see the developers built support and tools to make things easy, a coordinator for gathering the vast amounts of contributions that would come in, web pages with statistics and how-to's, etc. Is there a writeup somewhere on how all this came about?\n\nI think this would be a great 'toot you own horn' story for KDE.\n\nDerek"
    author: "Derek Kite"
  - subject: "Ctrl+R"
    date: 2003-12-27
    body: "I went to #KDE on IRC (irc.kde.org), expressed my wishes that Konqueror would reload pages with Ctrl+R and not only F5, I argued for it, it was discussed there and then, and BAM, Aaron writes a patch for it and apparently has it accepted into the CVS.  That's how cool the KDE developers and community are!  This may be only a small improvement, but I think you'd be hard pressed to find any closed-source company that sensitive to user feedback."
    author: "Haakon Nilsen"
  - subject: "Sr. UNIX/Linux Systems Administrator"
    date: 2003-12-27
    body: "Hello all...\n\nYou KDE folks are awesome!!!\n\nThank you for everything you:\n\n\to  have done\n\n\to  are doing\n\n\to  will do\n\nWith EnTHUsiasm...\n\nYour friendly neighborhood SA,\n\ngs\n\nG. S. Khalsa\n818-845-5476 (24 hrs.)\nkhalsayogi@sbcglobal.net"
    author: "GS Khalsa"
  - subject: "integration of non-KDE applications in the KDE env"
    date: 2003-12-27
    body: "\"integration of non-KDE applications in the KDE environment\"\n\n\nhbasic???\nhttp://hbasic.sf.net\n\nBlender???\nhttp://www.openblender.org"
    author: "possi"
---
<A href="http://kt.zork.net/kde/kde20031222_72.html">KDE Traffic #72 is out</a>, featuring an interview with Carlos Leonhard Woelz regarding his Quality Team proposal,  integration of non-KDE applications in the KDE environment, last minute KDE 3.2 tweaks and more. 
<!--break-->
