---
title: "KDE Traffic #65 Part 1 is Out"
date:    2003-10-05
authors:
  - "rmiller"
slug:    kde-traffic-65-part-1-out
comments:
  - subject: "I'm not a GNOME developer"
    date: 2003-10-04
    body: ";-)"
    author: "Andras Mantia"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-04
    body: "LOL  :D"
    author: "Soknet"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-05
    body: "Please guys, what means == LOL ==..?"
    author: "new lammer"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-05
    body: "LOL = Laughing Out Loud"
    author: "der Mosher"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-05
    body: "I must have missed something. I searched the page and you're not even mentioned. Now I'm confused. Which project were we developing on again?\n\nEither Russell is messing with us... or senility is finally here... <Homer_Simpson>Ahhh... senility...</Homer_Simpson>"
    author: "Eric Laffoon"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-05
    body: "Eric, search harder.  He is neither crazy nor senile.  At least not because of this post."
    author: "Russell Miller"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-07
    body: "Argh! This is what I get for skimming the page, but not skimming fast enough. There's a first time for everything...\n\nAnd besides, I didn't mean that Andras was senile... I meant that I... What was I talking about? ;-)\n\nI can at least say unequivicably that I am not a GNOME developer, even when referencing these new criteria."
    author: "Eric Laffoon"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-05
    body: "Eric, your comment proves that _you_ are a GNOME developer - unless you succeed trying harder."
    author: "Olaf Jan Schmidt"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-07
    body: "Ah, I have succeeded in trying harder. (Now you know who the smart one is on the Quanta team) ;-)\n\nNow grasshopper, if you can snatch this virtual pebble from my hand before I can instantiate it..."
    author: "Eric Laffoon"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-06
    body: "I think he was being sarcastic."
    author: "Jilks"
  - subject: "Re: I'm not a GNOME developer"
    date: 2003-10-07
    body: "No Chris... he was saying YOU are a GNOME developer. ;-)"
    author: "Eric Laffoon"
  - subject: "Jihaa!"
    date: 2003-10-04
    body: "I officially qualified as NOT being a GNOME developer! :-)"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Jihaa!"
    date: 2003-10-05
    body: "Heh.  Amusing.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Jihaa!"
    date: 2003-10-05
    body: "Your logic is flawed: \"If not A, then B\" does not imply \"If A, then not B\".  Example \"If I can't lay eggs, I'm a ground animal\", which does not imply \"If I can lay eggs, I'm not a ground animal\"."
    author: "ac"
  - subject: "Re: Jihaa!"
    date: 2003-10-05
    body: "Well, if you can lay eggs, you can't post to the web. So you can't lay eggs. Liar! ;-)"
    author: "Roberto Alsina"
  - subject: "mail"
    date: 2003-10-04
    body: "Thanks for your hard work, but what was the reason the mail got deferred and rejected?  Why not send it again?  Zack will never notice otherwise.  I think if Zack has a valid email address you should be able to write him, no?"
    author: "ac"
  - subject: "Re: mail"
    date: 2003-10-04
    body: "> Why not send it again?\n\nHe did.  Several times.  Didn't you read that part?  Don't worry, I'm sure he will continue to try to contact Zack."
    author: "Spy Hunter"
  - subject: "Re: mail"
    date: 2003-10-04
    body: "Yes, I will.  Here's what happened.\n\nLast week, I attempted to send this issue on to Zack.  As Zack always takes a little while to post it, I waited a couple of days, then looked in my email and noticed that my email wasn't getting to him - that his email server was deferring it.\n\nI looked for his phone #, couldn't find it, couldn't find another address except for his contact address with joker (which I just tried).\n\nMeanwhile, time marched on.\n\nSo I downloaded his software and tried to figure out how it worked.  After about an hour or two I figured it out, produced this, and put it on line.\n\nI feel bad for having to do it.\n\nHowever, there are some benefits to the KDE traffic being on its own site.  Once I manage to get back in contact with Zack I will reevaluate whether this is necessary.  I may maintain this site in concert with his, I like the level of control that I have that I do not have with Zack's, but I don't want to deprive or usurp him, either.\n\nWe'll cross that bridge when we come to it.\n\n*shrug*.  One of the benefits of open source."
    author: "Russell Miller"
  - subject: "Re: mail"
    date: 2003-10-05
    body: "What is the reason the email server gave for deferring the email?  Did you get a bounce back? \n\nWhy not ask Zack for more access on his server? Just curious.  :)"
    author: "ac"
  - subject: "Re: mail"
    date: 2003-10-05
    body: "It said it couldn't be delivered.  I did manage to contact Zack, and he said his mail is intermittent and very bad right now.  I resent the traffic, we'll see if he gets it."
    author: "Russell Miller"
  - subject: "Re: mail"
    date: 2003-10-05
    body: "Errrr... I wouldn't say you're usurping anybody nor anything. You just can't wait for someone you can't get in contact to.\n\nJust try again contact him. Indeed, I hope he has seen the posting so he'll notice, and he'll try to contact you back. Ah.... that's love, two developers trying to contact each other desperately ;)) \n\nAnyway, now serious, if you still feel bad about it and cannot contact him for the next traffic, just create your own web with your own style, and create your own kde-traffic (you wouldn't be copying anything from him then). Even rename it if necessary to kde- or sort of. Hey, it's your work, not his.\n\nRegards,\n\n\tuga"
    author: "uga"
  - subject: "Re: mail"
    date: 2003-10-05
    body: "In my understanding the publication system work is his. It certainly needs attribution. And the audience related to the name is also his.\n\nBut then again, he certainly is not greedy. The thing about news is that it may get old quick. \n\nFor KDE it's not wrong to use external resources. If the maintainer was to change, will the next one be able to maintain the fork as well? Probably not. So we are better off to not fork until we have to.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: mail"
    date: 2003-10-05
    body: "Zack will always get attribution no matter what happens.  \"The audience relating to the name is also his\" is not strictly true, although it wouldn't be where it is without him.  I don't believe in \"owning\" audiences.\n\nThat said, he did release his code under the GPL, so a fork is entirely proper should it become time for that.  However, I've gotten hold of Zack, and we're back to where we were, so the temporary site will become permanent, but it will be a place for me to experiment with the layout, html, and backend engine, with the goal of sending some of those changes back to Zack and making things easier for me in the long run.\n\nI won't announce it unless I can't get hold of Zack for an extended period again.  However, he's provided me with his phone #, and we've put some additional precautions in place to deal with this should it happen again.  I don't expect it to be necessary unless, $DEITY forbid, he is hit by a bus.\n\nexport $DEITY=\n\nThanks for understanding, everyone."
    author: "Russell Miller"
  - subject: "KDE Jabber"
    date: 2003-10-04
    body: "I love the idea of getting a KDE Jabber server going but it would be a shame if only people with CVS accounts could connect."
    author: "anonymous"
  - subject: "Re: KDE Jabber"
    date: 2003-10-05
    body: "no it would be perfect to only be for KDE developers.... since why would we want a non kde developer to have a @kde.org developer address.  "
    author: "Ian Reinhart Geiser"
  - subject: "Re: KDE Jabber"
    date: 2003-10-05
    body: "> would we want a non kde developer to have a @kde.org developer address.\npersonally, i think this whole @kde.org vs @kdemail.net business is elitism.\nthere are a lot of kde developers that work harder than i do and they don't have @kde.org emails, yet i have one..."
    author: "Anonymous"
  - subject: "Re: KDE Jabber"
    date: 2003-10-05
    body: "> there are a lot of kde developers that work harder than i do and they don't have @kde.org emails, yet i have one...\n\nAnd you're sure that they actually asked for one and got rejected?"
    author: "Anonymous"
  - subject: "Re: KDE Jabber"
    date: 2003-10-05
    body: "I did."
    author: "Anonymous"
  - subject: "Re: KDE Jabber"
    date: 2003-10-06
    body: "> I did.\nWhat was the reason they rejected you for?"
    author: "Anonymous"
  - subject: "Re: KDE Jabber"
    date: 2003-10-07
    body: "That he'd rather give me a kdemail.org address later."
    author: "Anonymous"
  - subject: "Re: KDE Jabber"
    date: 2003-10-07
    body: "Why a separate domain?  That's so ugly."
    author: "ac"
  - subject: "Re: KDE Jabber"
    date: 2003-10-07
    body: "\"@kde.org\" looks like an official representative, see http://www.kde.org/areas/kde-ev/meetings/2002.php"
    author: "Anonymous"
  - subject: "Re: KDE Jabber"
    date: 2003-10-07
    body: "Exactly this is the problem, some people want one because it looks cool (opposite of ugly), which is cool, and not because he needs one. These people are partly not involved with KDE development at all but only wrote a third-party application. And I hear that there were cases that people disappeared after some weeks/commits or even requested a kde.org address as condition for their first contribution/commit."
    author: "anon"
  - subject: "Re: KDE Jabber"
    date: 2003-10-05
    body: "> I love the idea of getting a KDE Jabber server going\n> but it would be a shame if only people with CVS accounts\n> could connect.\n\nI'm not sure I follow you.  You don't need a kde.org address to communicate with kde.org Jabber users.  Jabber will let you communicate with anyone who uses Jabber.  The great thing about Jabber is that no one controls it.  It is distributed much like email, or the internet itself."
    author: "Burrhus"
  - subject: "Re: KDE Jabber"
    date: 2003-10-05
    body: "Hint: People accusing developers to shame will be ignored."
    author: "Anonymous"
  - subject: "Thanks Russ!"
    date: 2003-10-05
    body: "I also agree with the Jabber server idea and like the idea of having a DB for KDE, but yeah the framework should be furhter cleaned up as mentioned on kdedevelopers.net first. \n\nI also hope that the KDE book will cover KDE 4, since if it covers 3,2 and right after that there is a major binary incompatible release that would really be too bad. I would much rather have a book that covers KDE 4 so that it is generally valid for at least 2 years."
    author: "Alex"
  - subject: "Doesn't matter"
    date: 2003-10-05
    body: "The or one idea is to have the KDE book continously up-to-date. So it doesn't matter if a first version would describe 3.x if that is current at this moment."
    author: "Anonymous"
  - subject: "QSQlite license change"
    date: 2003-10-05
    body: "Just as a sidenote: minutes ago I got the last email with permission to change qaqlite to LGPL. It's unnecessary now, but hey, let's do it anyway."
    author: "Roberto Alsina"
  - subject: "7. Encouraging DB-using applications"
    date: 2003-10-06
    body: "embedding MySQl in KDE??? a full database????\n\nnice going KDE developers!!! bloat, bloat, bloat\n\nKDE --- sponsored by RAM chip manufacturers worldwide\nsoon you'll need 2Gig of RAM just to run anything.\n\nwhen will this madness stop and you people realize\nwhat you are doing hurts the users --- optimize!"
    author: "Anonymous"
  - subject: "Re: 7. Encouraging DB-using applications"
    date: 2003-10-06
    body: "SQLite is tiny. The library is 125kb, and the whole thing is (according to their homepage) 25k lines of code."
    author: "Rayiner Hashem"
  - subject: "Re: 7. Encouraging DB-using applications"
    date: 2003-10-06
    body: "When will users understand that you can not write competitive applications with limited resources using tools of the 8-bit age?\n\nWith C++ KDE is reasonably low-level, much lower than what Microsoft will have in the next generation or what Java is doing right now. My fear is that KDE's stuff is to low, rather than too high."
    author: "Jeff Johnson"
  - subject: "Re: 7. Encouraging DB-using applications"
    date: 2003-10-06
    body: "brought to you by the same idiots who invented corba"
    author: "Anonymous"
  - subject: "Re: 7. Encouraging DB-using applications"
    date: 2003-10-06
    body: "Judging by the amount of RAM KDE hogs, I doubt its too low."
    author: "Anonymous"
  - subject: "Re: 7. Encouraging DB-using applications"
    date: 2003-10-07
    body: "either get more RAM/CPU/whatever... OR\nuse a older version of of KDE/Windows/MacOS/whatever.\n\n*most* modern software is built for fairly modern hardware.\n\nThis is just how the computing world has *always* worked."
    author: "anon"
  - subject: "Re: 7. Encouraging DB-using applications"
    date: 2003-10-06
    body: "me thinks you don't know the difference between MySQl and sqllite. "
    author: "anon"
  - subject: "Re: 7. Encouraging DB-using applications"
    date: 2003-10-06
    body: "Or that DB drivers are loaded on demand."
    author: "Roberto Alsina"
---
<a href="http://www.duskglow.com/kt/kde/kde20030929_65.html">KDE Traffic #65 Part 1</a> is now available.  Note that it is at a <a href="http://www.duskglow.com/kt/">new temporary place</a> and is part one of a series of two, the other of which should come out today. This issue covers KDE 3.2, KDE 3.1.4, apidox, KMail, audiocd, db-aware applications, giving KDE a flak jacket, Jabber, JPEG, and more.
<!--break-->
