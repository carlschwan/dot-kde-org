---
title: "KDE on Television Show 24"
date:    2003-10-31
authors:
  - "jriddell"
slug:    kde-television-show-24
comments:
  - subject: "More KDE stars?"
    date: 2003-10-31
    body: "For a second I thought this was going to be about Andras Mantia being on a Hungarian Linux users show with about 100,000 viewers. I guess he's keeping his new TV star status a secret though. ;-)\n\nThis is really fun seeing KDE showing up on TV shows. It means that the guys behind the show think it's cool... and what people in Hollywood think is cool often becomes very trendy everywhere. Of course the funny thing is that when KDE isn't trendy any more people will be saying \"So what? Everybody uses KDE.\"\n\nSo Mac OS wasn't zooty enough... and we still have several more versions of KDE for them to try before they catch up. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: More KDE stars?"
    date: 2003-10-31
    body: "Talking about Hollywood showing something unknown to the TV users, I think is with the intent to scare or impress the stupid user with some (that user should think ) alien pentagon software. Computer replesentation in the Movies has always been very sick with the ideas like for example showing some trigonometry formulas in the Hackers movie or in other movies IPs with bigger than 255 numbers (hehe) or other stupid things like chatting in Textpad (!!!) and so on. The Movie directors should have to hire a geek to consult them how to represent a particular environment instead of improvise unreal situations. Anyway who cares, the stupid movie or TV watcher expects to hear some blips or to see some zoomin zoomouts or hacky matrix like symbols so that makes them happy, and of course if they put a regular XP or 2K nobody will be impressed.\nAbout the KDE1 granny make a closer look and you will see no item in the taskbar :), more than that you can clearly see under the taskbar another taskbar which looks to me like 2K hidden taskbar - seems that this guys just took a shot of old KDE and put it on a win machine. (sorry if I broke someone's dreams)"
    author: "Anton Velev"
  - subject: "Re: More KDE stars?"
    date: 2003-11-01
    body: "Maybe they use kde-cygwin?"
    author: "Stefan Heimers"
  - subject: "Re: More KDE stars?"
    date: 2003-11-01
    body: "The taskbar could have been on top of the screen. Wasn't this by default in KDE1?\n\nKDE 1 screenshots from the KDE.org site:\nhttp://www.kde.org/screenshots/images/medium/matthiase1.jpg\nhttp://www.kde.org/screenshots/images/medium/koffice1.jpg\nhttp://www.kde.org/screenshots/images/medium/klyx1.jpg"
    author: "Andy Goossens"
  - subject: "Re: More KDE stars?"
    date: 2003-11-01
    body: "I used kde1 for a long time before kde2 appeared. I remember my taskbar was on the bottom, and that there was an option to autohide (just like in win), and also there was an opition to be on top. I am not sure what was the default but on my old kde1 desktops it was always on the bottom.\nAnyway, being the taskbar on top or bottom this does not change the obvious fact that this is just a shot and that under the kde1's panel it's clearly noticable something else - may be a border of a window or hidden taskbar.\nKDE1 was a nice desktop for its time but it was more looking like windows, version after version KDE look gets better while imitating more and more Mac OS X. If (the movie directors) wanted to scare and impress the user of course they would not put a real Mac or KDE but of course they would put something more scary like KDE1 or CDE (personally I would elect WindowMaker for this purpose).\nNote: when I mean \"scary\", I mean very unknown, impressive and alien looking for the stupid computer unaware user."
    author: "Anton Velev"
  - subject: "Re: More KDE stars?"
    date: 2003-11-01
    body: "I remember the taskbar being on top too...  maybe your distro changed it."
    author: "anon"
  - subject: "Re: More KDE stars?"
    date: 2003-11-02
    body: "Was at the bottom for me too. \nAt that time using SuSE 5 and 6.x as far as I remember. Never mind..."
    author: "Anon"
  - subject: "KDE 1 taskbar"
    date: 2003-11-03
    body: "Hmmm ...\nLooking at my (current!) KDE 1.1 desktop, the taskbar is on the right-hand side of the screen, but the panel configuration allows it to be on any edge.  Quite the user's choice.\nAnd since you ask: no, I don't have the priveledges to upgrade.  I'm lucky that the IT staff haven't deleted 1.1 to make me use CDE."
    author: "D. C. Sessions"
  - subject: "Re: More KDE stars?"
    date: 2003-11-03
    body: "I still use KDE 1.1 under RedHat 7.0 at my school for the sound system. The default is for taskbar to be at the top, but like in this screenshot, I have my system setup for the main panel to be at the bottom and the taskbar underneath it with autohide turned on.\n\nBrandon Adams\nUrsuline Academy (too cheap to afford real machines to put a newer distro on)."
    author: "Brandon Adams"
  - subject: "nice"
    date: 2003-10-31
    body: "Nice catch!  I wouldn't mind seeing a screenshot of Alias with KDE in it too...  Btw the link to the page on http://jriddell.org/ itself is wrong."
    author: "ac"
  - subject: "Re: nice"
    date: 2003-10-31
    body: "<a href=\"http://groups.google.com/groups?hl=en&lr=&ie=UTF-8&selm=v24q9fgs1hq869%40corp.supernews.com\">KDE on Alias</a>"
    author: "Anonymous"
  - subject: "Re: nice"
    date: 2003-10-31
    body: "To boot, that looks like a much more recent KDE!"
    author: "anon"
  - subject: "Re: nice"
    date: 2003-10-31
    body: "Well this looks strange: a CDE Window in a KDE environment? Allways the same state of the \"background\" (KDE, Konsole). Maybe KDE is a screenshot displayed fullscreen  on a CDE machine (the CDE Window seems to do someting: ping pong, so this can't be a shot.... hmm, maybe a movie? The actor(s) can't play ping pong!?)."
    author: "panzi"
  - subject: "Re: nice"
    date: 2003-10-31
    body: "Again the Movie director is cheating the stupid TV user with a cheap tricks .... :)"
    author: "Anton Velev"
  - subject: "Re: nice"
    date: 2003-11-01
    body: "The guys using the computers in movies and TV shows are actors.\nThey are acting. What you see is always a slideshow.\n\nYou see, if they clicked wrongly on something, they would have to reshoot.\nThat costs money, and delays production.\n\nSo, they rather pay a few dimes to some nerd who can do slides, and have him click the space bar behind the camera (although sometimes the actor is the one that triggers switches, but not always)."
    author: "Roberto Alsina"
  - subject: "Re: nice"
    date: 2003-10-31
    body: "There was a time when KDE let you have different window decorations on different windows.  Maybe that was it."
    author: "anon"
  - subject: "Re: nice"
    date: 2003-11-01
    body: "This is KDE 3 running on Silicon Graphics (yeah, Irix). Hence the KDE & CDE stuff\n\nHetz\n"
    author: "Hetz Ben Hamo"
  - subject: "Re: nice"
    date: 2003-11-02
    body: "With two different window managers running on the same display?\n\nI think its more likely one of the wms that lets you have different window decorations for different windows. And the most likely option - a hack job in the gimp ..... erm photoshop.."
    author: "rjw"
  - subject: "Re: nice"
    date: 2003-11-06
    body: ">> Maybe KDE is a screenshot displayed fullscreen on a CDE machine\n\n>> Again the Movie director is cheating the stupid TV user with a cheap tricks\n\n>> There was a time when KDE let you have different window decorations on different windows. Maybe that was it.\n\n>> With two different window managers running on the same display?\n \n>> a hack job in the gimp ..... erm photoshop..\n\nAre none of you guys aware of X Windows' networking features?  This is exactly what you would expect, i.e. the big powerful server running Solaris or HP-UX etc. and the linux machine being used as an X-Term.\n "
    author: "nonamenobody"
  - subject: "What about Alias..."
    date: 2003-11-01
    body: "I've seen KDE ( 2.x ? ) been used in Alias quite a few times."
    author: "Tom"
  - subject: "Re: What about Alias..."
    date: 2003-11-01
    body: "Didn't bother to read the previous thread?"
    author: "Anonymous"
  - subject: "Re: What about Alias..."
    date: 2003-11-02
    body: "Yup...\nSorry"
    author: "Tom"
  - subject: "I think it is done with Macromedia Director"
    date: 2003-11-01
    body: "I think all these Operating Systems are made with Macromedia Director. There are no problems intrgrating dynamic looking Programms and what so ever. And Director ist muche more usefull than any Powerpoint Presentation.\n"
    author: "Calle"
  - subject: "Fair Use"
    date: 2003-11-01
    body: "> If a GPL'd set of icons had been used, would we now be\n> legally able to modify, sell and distribute the episode\n> under the terms of the GPL over the internet?\n\nI doubt it.  If what you suggest were true, movies would violate copyright every time they show anything (a can of Pepsi, a screenshot of Windows, a newspaper, etc.).  That would be ridiculous.  Such a display would fall under fair use."
    author: "Burrhus"
  - subject: "Re: Fair Use"
    date: 2003-11-01
    body: "Actually, I think they pay big bucks to display that stuff. I don't think Pepsi would want their product displayed in offensive ways. For example, a serial killer carves up some people and once the killer is done, she/he pulls out a Pepsi and downs it, then tosses the can saying \"Ahhh, the taste of a new generation.\" This might be why the X Files had the smoking man smoke the off-brand Marlboro... Marlow(?)."
    author: "Jilks"
  - subject: "Re: Fair Use"
    date: 2003-11-02
    body: "> Actually, I think they pay big bucks to display that stuff.\nNot really; more the other way around; ever heard of advertising ?\n\n> I don't think Pepsi would want their product displayed in offensive ways.\nProbably true (maybe); but totally separate from the former line. "
    author: "Thomas"
  - subject: "Re: Fair Use"
    date: 2003-11-03
    body: "Morley :)"
    author: "dh"
  - subject: "Re: Fair Use"
    date: 2003-11-04
    body: "Sorry, but Morley is a fake brand, and C.G.B. Spender's cigarettes were herbal, containing no tobacco."
    author: "HTH"
  - subject: "Re: Fair Use"
    date: 2003-11-04
    body: "<i>Actually, I think they pay big bucks to display that stuff. I don't think Pepsi would want their product displayed in offensive ways.</i><BR>\n<BR>\nNot really. In the industry they call this product placement and advertizing agencies pay big bucks to major studios for the privilage. The studios then require their productions to show these products on screen.<BR>\n<BR>\nIn Fightclub for example they made a point of putting all the obligitory pepsi vending units in the most unlikely places. Like the ally out back of the convenice store during the \"human sacrifice\" scene.<BR>\n<BR>\nI think the reason X files uses an off brand of smokes is becase tabco companies are baned from advertizing on TV in the US."
    author: "red5"
  - subject: "Re: Fair Use"
    date: 2003-11-02
    body: "Actually KDE icons are released under the LGPL so there's no problem here."
    author: "icon heini"
  - subject: "Re: Fair Use"
    date: 2003-11-03
    body: "Actually, I believe the GPL only covers stuff that you change. For instance if they modified the icon, or the code used to display the icon, they would have to distribute it. However, simply reusing the icon in another product does not constitute a change, therefore would not require redistribution. For instance, if I bundled KDE with my propietary operating system, I would have to redistribute any changes I made to KDE... but I would not be obligated to distribute the entire operating system. \n"
    author: "Venkman"
  - subject: "O, gosh"
    date: 2003-11-01
    body: "> I doubt it. If what you suggest were true, movies would violate copyright every > time they show anything (a can of Pepsi, a screenshot of Windows, a newspaper, > > etc.). That would be ridiculous. Such a display would fall under fair use.\n\nOh good Lord. IT'S A JOKE! Can't you realize that???????????"
    author: "Lucas"
  - subject: "Re: O, gosh"
    date: 2003-11-06
    body: ">> Oh good Lord. IT'S A JOKE! Can't you realize that???????????\n\nYou are joking aren't you?\n "
    author: "nonamenobody"
  - subject: "I alway wonderd"
    date: 2003-11-01
    body: "I heard very often that the Movie Industry in the US are using Linux on the Desktop for there Special Efects Producktions. 3D and Compositing are big omn Linux. \nNow I Wonder wich Desktop the Industry is in favour. It it Gnome because they are Red Hat Users, or it it KDE because... Well what ever. This Screenshot maks me belive it is KDE.\nBut maybe it is because of some other reason, and of course 24 is a TV Produktion.\n"
    author: "Calle"
  - subject: "oh please"
    date: 2003-11-02
    body: "\"If a GPL'd set of icons had been used, would we now be legally able to modify, sell and distribute the episode under the terms of the GPL over the internet?\"\n\nYeah, let's scare more people about using GPL-licensed products. The GPL seems more like a disease now than a copyright license."
    author: "RMS"
  - subject: "Re: oh please"
    date: 2003-11-03
    body: "Valid Point(TM)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: oh please"
    date: 2003-11-03
    body: "\"Yeah, let's scare more people about using GPL-licensed products. The GPL seems more like a disease now than a copyright license.\"\n\nIt doesn't scare people who know what they're talking about.  Licensing under the GNU General Public License scares people who don't take the time to understand the terms of what they are getting involved in (which is never advised, no matter what license is being considered)."
    author: "J.B. Nicholson-Owens"
  - subject: "If a GPL licence..."
    date: 2003-11-02
    body: "IANAL, but no.  In much the same way that music playing in a bar in the background in a news report doesn't require the news station to have the permission of the copyright holder.  Most certainly the episode wouldn't be considered a derivative work of the icons in the way that a modifided version of a GPL'd program is derivative."
    author: "John Allsup"
  - subject: "Re: If a GPL licence..."
    date: 2003-11-04
    body: "IANAL but in fact bars DO have to pay for playing music in the background.  My sister works at a bar that was just fined because they haven't payed their licencing fee(annual fee that covers most music).  Holeee S*.  And I thought the music industry were facists before!"
    author: "Stinky"
  - subject: "GPL'd icons"
    date: 2003-11-02
    body: "I doubt very much that the GPL would force itself on the episode.  It is almost certainly covered by fair use provisions."
    author: "Jonathan Bryce"
  - subject: "I don't understand..."
    date: 2003-11-03
    body: "... the sense of Jonathan's question about using the GPL icons? Are the KDE artists the least bit interested in producing an alternative 24 episode? Do we really need to alienate (even more than what proprietary proponents manage to) people from actually using and enjoying free software? As long as KDE's brand is used in actually advertising manners, do we care? OK, having a big power's military make a film about extremists using Linux/KDE on their desktops while planning the killing of thousands of innocents would be offensive and we would then try to do something. But even then, why should we be ourselves licensing extremists?"
    author: "Inorog"
  - subject: "Re: I don't understand..."
    date: 2003-11-03
    body: "It's a bad taste joke, directed to the only ones who will care and laugh at it, i.e. KDE developers. Ignore at will :)"
    author: "Cloaked Peng\u00fcin"
  - subject: "KDE on 24"
    date: 2003-11-03
    body: "Only some of the computers have switched.  Jack is till using a Powerbook.  "
    author: "Matt"
  - subject: "Re: KDE on 24"
    date: 2003-11-04
    body: "Somehow I want to buy a PowerBook now."
    author: "R.A.P.P."
  - subject: "Re: KDE on 24"
    date: 2003-11-04
    body: "Can it be a powerbook with Linux and KDE ?"
    author: "blah!"
  - subject: "Josh 2.0"
    date: 2003-11-03
    body: "I think i saw KDE runing on that new upn show, Jake 2.0, but i wasnt on the screen long enough for me to tell for sure, so it could have been fake like everything else in hollywood. At least they are trying to be more accurate in computer interface representations and have moved on from the cheesy interfaces like what we saw in Hackers. Im mean really??? when was the last time you \"flew\" through your file system to find a file??? "
    author: "Scott"
  - subject: "Re: Josh 2.0"
    date: 2003-11-04
    body: "yesterday, smartarse! :)\n\nhttp://www.thedumbterminal.co.uk/software/3dfm.html"
    author: "Anonymous Cow"
  - subject: "Not only an asinine thing, but a dangerous thing.."
    date: 2003-11-04
    body: " ...to say. Why would you even suggest the completely unrealistic possibility that by using GPL'd ICONS on a television show that that particular show would then be under the GPL? It's absurd and DANGEROUS.\n\n  A large number of people would like to see Open Source and GPL'd software used in more places. So, I dunno, let's make blanket statements that put fear of GPL and OSS into people that might use them... Yeah, that will work.\n\n  As I understand, by putting those ICONS under the GPL, the icons themselves could be used as bases for NEW icons and that those NEW ICONS would have to be released under the GPL. It says nothing of displaying those images through two mediums for the purpose of showing functionality. \n\n  All they did was display an image of something. If the image of the thing they showed was under GPL it wouldn't matter. You aren't seeing the actual image FILEs of the Icons, just the result of the Image Files for the Icons. There is a difference, it's not minute. You cannot take an icon that is displayed on the desktop screen of a computer within a movie/film/television show and have that icon instantly available on your desktop. It's not possible.\n\n  "
    author: "Rupert Nelson"
  - subject: "Re: Not only an asinine thing, but a dangerous thi"
    date: 2003-11-04
    body: "\nRead above.... the whole point of Show-Should-Be-GPLed is to be funny.\n\n"
    author: "R.A.P.P."
  - subject: "Copyrighted artwork in films"
    date: 2003-11-04
    body: "Copyright issues with artwork in films is not unknown. See http://the-future-of-ideas.com/excerpts/index.shtml\n\n--\nThe film Twelve Monkeys was stopped by a court twenty-eight days after its release because an artist claimed a chair in the movie resembled a sketch of a piece of furniture that he had designed. The movie Batman Forever was threatened because the Batmobile drove through an allegedly copyrighted courtyard and the original architect demanded money before the film could be released. In 1998, a judge stopped the release of The Devil\u0092s Advocate for two days because a sculptor claimed his art was used in the background.\n--\n\nMusic in the background during live broadcasts has explicit exceptions under UK laws but otherwise the music must be licenced.\n\nThe rights and wrongs of it can be argued forever, and of course the US has fair use exceptions as has been said.\n"
    author: "Jonathan Riddell"
  - subject: "Rationale for switching KDE versus MacOS?"
    date: 2003-11-04
    body: "To improve security? How often has MacOS (any version) been hacked relative to Linux?\n\nAnd how is KDE comparable to MacOS (any version); the latter is a full operating system,\nKDE is little more than a desktop (plus some other appendages)."
    author: "Hoju Han"
  - subject: "KDE on 24"
    date: 2003-11-04
    body: "Just to clarify, the screen shot posted was actually in the prison from the beginning of the episode, most of CTU is still using Macs as far as I can tell."
    author: "no one"
  - subject: "KDE is an OS now?"
    date: 2003-11-06
    body: "I am sure someone must have posted this, however I can't find it.\n\n>> The Counter Terrorist Unit seem to have switched operating system from MacOS to KDE\n\nSince when was KDE considered an operating system?"
    author: "nonamenobody"
---
The third series of <a href="http://www.fox.com/24/">television show 24</a> started in the US last week.  In the aim to improve security, The Counter Terrorist Unit seem to have <a href="http://jriddell.org/24-kde.html">switched operating system from MacOS to KDE</a>.  Interestingly they used a 3-year-old KDE 1.x desktop. These older icons are made available under a <a href="http://artist.kde.org/new/license.html">public domain licence</a>. If a GPL'd set of icons had been used, would we now be legally able to modify, sell and distribute the episode under the terms of the GPL over the internet?
<!--break-->
