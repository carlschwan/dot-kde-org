---
title: "cuckooo: OpenOffice.org nested in KDE"
date:    2003-08-20
authors:
  - "jholesovsky"
slug:    cuckooo-openofficeorg-nested-kde
comments:
  - subject: "impressed"
    date: 2003-08-19
    body: "I find it exceedingly interesting that you managed to accomplish this before the Bonobo guys, especially when Ximian has been throwing money at it.  Says a lot about the power of KDE component technology and perhaps your own hacker skills. \n\nGreat job!"
    author: "ac"
  - subject: "Re: impressed"
    date: 2003-08-19
    body: "Oh nevermind!  Looks like the OOo project accomplished the same thing but then the project died..."
    author: "ac"
  - subject: "Re: impressed"
    date: 2003-08-19
    body: "yup, the bonobo folks did this in early of 2002.. but the project died soon after.. let's hope that this one doesn't :) !\n\nlikely, the bonobo folks had direct help from Oo developers too."
    author: "anon"
  - subject: "Re: impressed"
    date: 2003-08-20
    body: "That looks very coool !\n\nNow, we need the similar to see KOffice files on windows.\n\nSo, we could send to windows users KOffice files.\n\nDoes somebody work on this job ?"
    author: "gerard"
  - subject: "Re: impressed"
    date: 2003-08-20
    body: "KOffice is switching to OOo formats, so maybe half the job will be done when that's done.\n"
    author: "anon"
  - subject: "Network"
    date: 2003-08-19
    body: "Does it also work via network - so that if you click a link to a .doc document on a website, you can view it immediately? That's what I like about Word integration in IE.\n\nFor the rest, I think this is a great thing, because OO is still better than KOffice."
    author: "Daan"
  - subject: "Re: Network"
    date: 2003-08-19
    body: "Yes, it works exactly as you described. Thanks to the KParts::ReadOnlyPart this feature is transparent both for the user and the developer."
    author: "Jan Holesovsky"
  - subject: "Re: Network"
    date: 2003-08-19
    body: "Yeah, of course.. all kparts are generally network-transparent..\n\nthis is how things like koffice, kghostview (and in kde 3.2: kpdf) work also.."
    author: "anon"
  - subject: "Very cool!"
    date: 2003-08-19
    body: "Good stuff, congratulations for the integration (which is never easy since it requires to understand two different worlds :).\n\nI hope that this doesn't create too much of a nightmare for packagers though - this stuff depends on both kdelibs-devel and the OOo source code (or devel packages?), to be compiled. No problem for users, of course."
    author: "David Faure"
  - subject: "hehe"
    date: 2003-08-19
    body: "Nice project name and logo!"
    author: "anon"
  - subject: "Re: hehe"
    date: 2003-08-20
    body: "Totally agree.  Nice integration of the K."
    author: "Ned Flanders"
  - subject: "Re: hehe"
    date: 2003-08-27
    body: "and the OO, 'o course."
    author: "zero08"
  - subject: "Re: hehe"
    date: 2003-08-20
    body: "Full ACK. The name is at least as cool as the technology ;-)"
    author: "Jan"
  - subject: "Applescript like macros"
    date: 2003-08-19
    body: "One thing I love about this idea is the possibilities it could throw open with tools like DCOP, KJSEmbed and even Kommander. Just imagine, you throw together a psuedo-application using a point-and-click GUI that will, with a keystroke or mouse click, go through lots of documents and perform complicated tasks like formatting, spellchecking, etc, putting OO together with other KDE apps. No need to sit and learn to script (DCOP, Applescript), nor to put up with simple macros that only work within the given application (any office app).\n\nOf course there's nothing (except time & code!) stopping this from happening with KOffice, but for the moment OO has the lead on support for things like MS Word, and it's more likely to be adopted by businesses.\n\nIt might also be cool to experiment with KJSEmbed, splitting OO up into many modular components and making it easy for people to make their own office suite, piecing together the parts they need. I, for example, only seem to use the basic text formatting tools, bullet/number points, footnotes and occasionally embedded charts and graphs. It'd be fun and probably useful to be able to make my \"own\" kword/kspread app that had a GUI I custom made for my needs."
    author: "Tom"
  - subject: "Re: Applescript like macros"
    date: 2003-08-19
    body: "> I, for example, only seem to use the basic text formatting tools,\n> bullet/number points, footnotes and occasionally embedded charts and\n> graphs. It'd be fun and probably useful to be able to make my \"own\"\n> kword/kspread app that had a GUI I custom made for my needs.\n\nThat's actually very easy - you just need to create your own menu and toolbar XML file. That's it.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Applescript like macros"
    date: 2003-08-22
    body: "What you are looking for is this:\nhttp://developer.kde.org/documentation/library/kdeqt/kde3arch/xmlgui.html\n\n/kidcat"
    author: "kidcat"
  - subject: "Cool"
    date: 2003-08-19
    body: "using Qt widgets and fully kde integration would be great!!!\ni am looking forward to what will come!!!\n"
    author: "gunnar"
  - subject: "Re: Cool"
    date: 2003-08-19
    body: "Last time I checked the OOo guys were planning on an abstract API to allow bindings to other toolkits like GTK and Qt. I also wonder how eventloop merging is supposed to work in this case. Anyone with details?"
    author: "Daniel Molkentin"
  - subject: "Re: Cool"
    date: 2003-08-20
    body: "This is very good. Then you can easy switch the widget library kde, qt, gtk, wxwindows etc, etc.\nIf kde chooses this solution to be the \"official office\" of kde then kde will be more independent from QT/GPL."
    author: "Anton Velev"
  - subject: "Re: Cool"
    date: 2003-08-20
    body: "KDE doesn't need anything \"official\". Use whatever you like. Calling something \"official\" doesn't make it better."
    author: "Bausi"
  - subject: "Re: Cool"
    date: 2003-08-20
    body: "That's good... I think. We do want OOo to look and behave Qt-ish.\n\nOn the other hand, I get a feeling that a *tool*kit shouldn't be something you have to support - it should be something you _use_ to get your work done. The toolkit isn't meant to be \"abstracted away\", it's meant to be taken full advantage of. Then again, separating UI code from the rest as far as possible is probably a good thing.\n\n(just a few random thoughts ;))"
    author: "Apollo Creed"
  - subject: "koffice?"
    date: 2003-08-19
    body: "> Have you ever dreamt of OpenOffice.org integration in KDE?\n\nNo.\nBut I have dreamt (and many with me) about a stable, neat and\nfunctional KOffice."
    author: "kde user"
  - subject: "Re: koffice?"
    date: 2003-08-20
    body: "This integration means upgrade of KOffice.\n\nThis OO components can entirely replace the current KDE components."
    author: "Anton Velev"
  - subject: "Re: koffice?"
    date: 2003-08-20
    body: "Dream on."
    author: "Datschge"
  - subject: "Re: koffice?"
    date: 2003-08-20
    body: "agreed. when comparing based on common functionality, i find KOffice is much nicer to work with. KOffice just isn't feature complete yet, and has some nagging performance issues. it's coming along nicely, though.\n\nthat said, having something that allows OOo to be integrated better into KDE is great. even if KOffice and OOo were feature equals, it would still be great simply because OOo has a huge user base. the importance of having as many applications work well with KDE can not be understated."
    author: "Aaron J. Seigo"
  - subject: "Re: koffice?"
    date: 2003-08-21
    body: "There is an another reason why I think this is great even though my idea of a word processor is LaTeX -- this is a pretty cool hack :-)\n"
    author: "Sad Eagle"
  - subject: "Re: koffice?"
    date: 2003-08-22
    body: "I agree - I think koffice is much nicer, though not finished.  On the other hand, it's done pretty much all I need it to do for a lot of my documents.  It's a rather nice tool."
    author: "TomL"
  - subject: "Re: koffice?"
    date: 2003-08-20
    body: "Then why don't you help?\n"
    author: "Craig Drummond"
  - subject: "Really impressed .."
    date: 2003-08-19
    body: "But can you tell something about its performance? I mean how much time does it take to display a *.sxw or a *.sxc file ?"
    author: "nx_in"
  - subject: "Re: Really impressed .."
    date: 2003-08-20
    body: "If you have an already running instance of OOo (e.g. cuckooo is already displaying a file in another window), it is just the same time it takes to OOo itself to load the file. If no, cuckooo must start OOo, and it takes some time. But it is done paralelly with downloading the file, so if your network is slow enough, you won't even notice it:)\n\nI have to have a look at OOo Quickstarter (http://segfaultskde.berlios.de/index.php?content=oooqs), maybe it can save additional startup time."
    author: "Jan Holesovsky"
  - subject: "Re: Really impressed .."
    date: 2003-08-20
    body: "OOo Quickstarter\n\nI use this:\n########################\n#!/bin/sh\nwhile true; do /usr/bin/openoffice -plugin -quickstart; done\n########################\n\nPut this script in HOME/.kde/Autostart/script\nchmod 755 HOME/.kde/Autostart/script\nand turn off automatic sesion saving in kcontrol.\n"
    author: "Marius"
  - subject: "Re: Really impressed .."
    date: 2003-08-22
    body: " Hmm... This is a neat hack for now.. but it resembles \"M$-win23-way-of-thinking\" to me.. and the \"keep all sorts of shit running just in case\" is just not my cup of tea so to speak :-P\n\n So lets hope someone gets a few neat ideas for optz regarding startup :-D\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Really impressed .."
    date: 2003-08-20
    body: "Checkout the \"pagein\" binary. It uses the madvide interfaces so that linux\nloads the libs into memory. It seems that this improves the loading of OOo quite a bit ( I've heard of 10-30 % improvement ). \nI wonder would ie also help with some of the KDE libs ?\n\n"
    author: "CPH"
  - subject: "KOpenOffice"
    date: 2003-08-20
    body: "Hello buddy,\nReally nice job, how functional is it?\n\nIt was the one I dreamt and would like to contribute.\nSeems that someone (you) who has time read the post (thanks for the referrence). \n\nWith your touch, now koffice is mature office suite! You are saving millions of years to kword, kspread, kpresenter, etc team, now they can just drop this parts and use the real ones that come from Sun team.\n\nAfter some porting to kde/qt widgets this will really look like regiar kde office. This will be nice because then it will adapt to my current theme instead of using some old gray style. Replacing old koffice components with OO components will make koffice the best office suite for my KDE, until now it was OO from Sun. OO is good, not good as msoffice but enough good to use it because of it's crossplatform portability."
    author: "Anton Velev"
  - subject: "Kivio"
    date: 2003-08-21
    body: "There are some really good featurs in Koffice that I would hope to see integrated into open office.  Kivio is one."
    author: "Anonymous Coward"
  - subject: "OK, but basically worthless"
    date: 2003-08-20
    body: "in my opinon (since this is a place to voice it).  I think OO sucks, sucks a lot.  I really wonder how many people use OO on a regular basis.  This last spring semester, I thought I would try to use it for all my lab reports and papers I needed to write for univ.  I turned from thinking OO could replace MS Office, to spending a lot more time in the Win2K lab in the EE building.  Basically, I find OO terrible.\n<p>\nMy number one compaint with it is its speed, which also contains an amusing anecdote.  In Linux, OO writer takes like about 13-20sec (avg of about 16) -- and yes I ran it 10 times in a row and then ran it another 10 times with it being the only other thing open besides running KDE HEAD.  It didn't really make too much of a difference when I had like 3 Konquerors, KMail, KJots, Kate, and 2 Konsoles -- about an additional second.  Now the amusing part.  In Windows XP, it takes no more than 8seconds to open and usally about 5seconds.  I ran it 10 times in row in XP and got almost the same exact startup speed.  My computer isn't too slow either.  I have a XP 2500+ (Barton) with 256KB of PC2700 and a Seagate 60GB HD running in UDMA4 (I think).  At any rate hdparm -tT reports about 50MB/s transfer speed and I'm running resier so I guess what like 15-20MB/s of actual bandwidth.\n<p>\n2) It is a general hacky feeling to the UI and GUI.  I think it suffers from your standard OSS/FS application problem. Usability.  More than once, I've tried to do something and could not figure out how to do it.  Going to the OO irc channel is worthless.  I've tried 3 times for help them and got nothing.  After the 2nd time I wasn't going to go back, but I did and of cource got not help.  Every time I was there, either got no answer or believe it or not \"I'm just a developer I've never really used it too much.  Try like #linpeople or something, or use the email lists.\"  Then the GUI problem, is I think its kind of ugly, and what's with the mouse cursor.  It reminds me of using the old Sun machines at school.  I will say one thing, it has been getting more responsive with most every release though.  Still its a bit \"draggy\" at some times.\n\n3) Stability.  This is the main reason I had to stop using OO and go to MS Office.  EVERY time I would write a lab report (those who are in univ know they're common especially for EE classes more than CS) I needed to add at least 1 graph.  Well guess what would happen, it would crash trying to paste the graph into OO Writer from the speadsheet.  Or trying to move the graph around would crash it.\n<p>\n4) One last thing that needs working on with OO is this and it makes me so fucking mad I could scream.  Why the HELL is there no up-one-line \"action\".  There is down-one-line, but not up.  Why?  There is even \"return to the beginning of the page or sentence or whatever\" but not up one line.  The only reason I can think of is that most OO developers are, basically, mad.  Completely mad.  And maybe it is just the Windows version that does not have an \"up one line\" action.\n<p>\nAt any rate, a good office suite is what the OSS/FS community lacks.  It is really what most people use so much.  Hopefully this next KOffice suite can at least equal OO if not surpase it.  With KDE's integration (kparts) and its decent usability and look, KOffice could be one application that could switch office workers over to Linux and Unixware (j/k)."
    author: "SupetPET Troll"
  - subject: "I disagree"
    date: 2003-08-20
    body: "Hi,\n<BR>\nI disagree. OO is the best free office, and the only who beats it is of course MS but it's commercial - normally to be better.\n<BR>\nDid you report a bug about OO? I see you are unhappy with some things but you could tell them and they fix.\n<BR>\nHere are the advantages of OO:\n1) Runs on all popular platforms (the only one)\n2) Supports the world standart document exchange formats (doc, xls, ppt), only does not support pdf\n3) Connects to multiple databases\n4) It's stable\n<BR>\nThe main reason for my choice is 1) because once you learn and start feeling comfortable with a given app crossing between platforms (while using same apps) will be no problem. Netscape is the other solution that gives you the same advantage.\nOf course if MS ports their office to linux they will have the same advantage too.\n<BR>\nIt will be a long step forward for the KDE office if it replaces it's own word, excel and powerpoint components with the ones ported from OO to KDE. And the only reason for that will be that look&feel will be consistent accross the apps in the environment (the native OO looks like old win app).\n<BR><BR>\nOne problem I had btw with the OO - it does not print correctly but it does not bothers me a lot because paperwork is for the previous century, we are now living in the digital century.\n<BR><BR>\nPS: Someone should fix this forum, it's not likely to use &lt;BR&gt; for line termination"
    author: "Anton Velev"
  - subject: "Re: I disagree"
    date: 2003-08-20
    body: "Have you people no return key on your keyboard that you use < p > and < br > all the time?"
    author: "Anonymous"
  - subject: "Re: I disagree"
    date: 2003-08-20
    body: "Yeah, heh.. html in this site has been disabled for nearly 3 years because of abuse..\n"
    author: "anon"
  - subject: "Re: I disagree"
    date: 2003-08-21
    body: ">>Here are the advantages of OO: 1) Runs on all popular platforms (the only one) \nWell the Mackie version is in the making. Still no MacOS <10 but that will be gone anyway. (though Macs tend to live longer than PCs)\n\n2) Supports the world standart document exchange formats (doc, xls, ppt), only does not support pdf \nIt does! OOo1.1 writes pdf as well as Flash .swf for presentations. It still doesn't open pdf which is a pity. (Koffice does but can't export it to something es\n\n3) Connects to multiple databases \nDepends what you're meaning to do. OOo/SO does connect to MySQL, dBASE and Adabas natively and via ODBC to all known databases. What it lacks is the ability to retrieve data from various sources (e.g. one customer table from MySQL and the transaction table from an Oracle db on an ODBC connection) and join them. That's a pity. The competing Office suite does. That's why so many folks still use Access to retrieve, join, calculate and report data though they know that MSoff sucks, spys and opens back doors as big as a railroad tunnel.\n\n4) It's stable \nThat's for sure. Stability has improved tremendously. Data loss doesn't occur - it has never happened to me at least.\n\n\nConrad"
    author: "beccon"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-20
    body: "I have to agree with this. I much prefer koffice to openoffice. Koffice starts a lot faster and overall it is more stable. I can't comment on how well it works with ms document formats but that is not an issue that I have but it does import pdfs nicely and creates them just fine also. Overall I don't use an office suit very much but when I do I use koffice. It does what I need and it works with the rest of my system. IO slaves I consider to be a critical feature and apps that don't have an equiv I don't want to use."
    author: "kosh"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-20
    body: "I agree with the stability issue. I frequently have some crashes when clicking on the \"Bold\" or \"Italic\" icon in the toolbar for example.\n\nBut about the startup speed, it's a known GCC/Runtime linker issue. That's why it starts faster under Windows. This is not at all a OOo issue.\n\nAs a lot of other users, I'm awaiting for a smart KOffice..."
    author: "aegir"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-20
    body: ">> As a lot of other users, I'm awaiting for a smart KOffice...\n\nAnd as a lot of other users, I am awaiting for a GCC + libs that won't break ever again between different compiler versions and being faster than ever when programming stuff using the C++ programming language.\n\nUnfortunately it seems those GCC guys hate C++ :|\n(bad decision, if they won't fix this very big issue between now and one year I'm going back to Windows + MSVC, which does allow one to just build a library and use it with any program, compiled with any compiler - and having better speed as well)\n"
    author: "schnitzelkopf@dashei\u00dfePost.com"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-21
    body: "Do you expect it to be a big loss for anybody?\n\nAre you informed at all?\n\nAnyway, why do you need a stable ABI in Free Software when all you need is to recompile?\n\nAh yes, go back and create your software on a system built exactly to allow hiding the source well.\n\nI for my part couldn't care less about incompatible gcc releases. I love Free Software and Debian will recompile most stuff for me anyway. For my own stuff I don't care either. \n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-22
    body: ">> Do you expect it to be a big loss for anybody?\n\nIf you would know what I'm working on, I would say yes, it could be quite a big loss.\n(not really big, but I'm sure many people will like the work I'm currently doing)\n\n\n>> Anyway, why do you need a stable ABI in Free Software when all you need is to recompile?\n\nYes, that's the big issue, I don't like to rebuild the software I'm running when it's not necessary.\n\nAnd I'm already compiling things way too much, so please, don't force me to rebuild my whole system as well, just to upgrade a compiler or whatever else..\n\nI've got better things to do with my (CPU) time than doing useless builds.\n(and no, I'm not going to use Debian)\n\n\n>> Ah yes, go back and create your software on a system built exactly to allow hiding the source well.\n\nI don't want to hide my source code, and I don't want anyone else to.\nThe only thing I'm asking for is more flexibility and choice.\n\n\n>> I love Free Software.\n\nWell, that makes two of us.\n"
    author: "schnitzelkopf@dashei\u00dfePost.com"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-22
    body: "Hey,\n\napparently I have misunderstood you then. :-)\n\nSo what's the big deal then. Your system is likely compiled with one compiler and you don't upgrade your system. Why not use that compiler?\n\nMaybe because it's not good enough yet? I believe the C++ language support of gcc is on a parser level among the best so far. Only the resulting binaries are lacking big ways in terms of quick loading. Too many relocations for virtual tables is the problem there.\n\nBut I believe that problem is one of the loader, binutils, not one the compiler can deal with. What can gcc do to change the way C++ works or the way ELF (the binary format) is or how it is loaded by the OS? Nothing.\n\nThe gcc guys are doing great work. Your anti-GCC guys sentiment must be remaining from the days when people felt it was necessary to create egcs fork which in the end is as of 2.95 the new official gcc.\n\nMy only complaint with gcc is the performance of compilation. But correctness comes first. Still NO compiler does parse standard C++. Not even the MSVC you mentioned. None. \n\ngcc is striving to be the first one. Just look at the comments of rewriting the C++ parser to be 2 pass and it no longer accepting incorrect KDE C++. It proves that they make a lot progress still on C++ field.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-22
    body: "Hey,\n\napparently I have misunderstood you then. :-)\n\nSo what's the big deal then. Your system is likely compiled with one compiler and you don't upgrade your system. Why not use that compiler?\n\nMaybe because it's not good enough yet? I believe the C++ language support of gcc is on a parser level among the best so far. Only the resulting binaries are lacking big ways in terms of quick loading. Too many relocations for virtual tables is the problem there.\n\nBut I believe that problem is one of the loader, binutils, not one the compiler can deal with. What can gcc do to change the way C++ works or the way ELF (the binary format) is or how it is loaded by the OS? Nothing.\n\nThe gcc guys are doing great work. Your anti-GCC guys sentiment must be remaining from the days when people felt it was necessary to create egcs fork which in the end is as of 2.95 the new official gcc.\n\nMy only complaint with gcc is the performance of compilation. But correctness comes first. Still NO compiler does parse standard C++. Not even the MSVC you mentioned. None. \n\ngcc is striving to be the first one. Just look at the comments of rewriting the C++ parser to be 2 pass and it no longer accepting incorrect KDE C++. It proves that they make a lot progress still on C++ field.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-20
    body: ">> with 256KB of PC2700\n\n'KB' eh? .. that might be the reason it starts so slow on your system ;)\n\n"
    author: "schnitzelkopf@dashei\u00dfePost.com"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-20
    body: "Have you tried OpenOffice 1.1? It's greatly improved in both speed and niceness-of-GUI from OpenOffice 1.0, which itself was a vast improvement from StarOffice 5. \n\nThe problem is that many of us need MS document compatability, but unfortunatly, koffice doesn't have that :(\n"
    author: "anon"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-20
    body: "Hear hear!\n\nOO1.1 is a vast improvement in speed over the old versions, and does a great job at importing MS stuff. I have on a linux machine at work, but due to my project management role, have to deal with stuff in MS formats where a complaint asking people why they don't use open formats will be met with a stern wall of bureaucratic resistance, so getting MS formats to import is a vital importance.\n\nI used to use VMWare + MS Office, but with OO 1.1 I am now capable of dumping MS altogether. In 99% of the cases it imports perfectly, with the following 1% needing just minor tweaks by me (margins and stuff, but then again that used to happen to me using MS Office as well).\n\nI dream of the day that KOffice can do the same, but it's just not there yet. For the time being projects like this to integrate the \"working-now\" OO are important to get people productive and working."
    author: "Dr_LHA"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-21
    body: "Just ask those as you call them \"mad developers\" your money back because they don't do what you want.\n\nOr sue them. Or whatever. \n\nAnd please identify the \"lacks of OSS/FS community\" or something and tell us. It is certain the community doesn't already know without you.\n\nI am surprised people don't ask you want you need in order to know what they need. But people are so mad anyway.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-22
    body: "***Just ask those as you call them \"mad developers\" your money back because they don't do what you want.\n \nOr sue them. Or whatever. ***\nHAHAHAHAAH uhm ok kiddo.  Maybe you need to go back home, have a glass of milk and some cookies and watch Sponge Bob.\n\nFirst of all, Oh Glorius Debian User, I never said one thing about compenstation.  I did, however make comments to its overall appearance and usability, which I think needs some big improvements if it is to replace MS Office.  Your attitude, and yes you do have a \"punk kid screw you\" attitude, is what a number of developers and especially those in the OSS/FS community have.  This \"if you don't like it fuck off\" just doesn't fly with anyone else.  I understand it comes from the almost complete lack of social interaction that some, like you have or I guess don't have?.  However, I've found that a good portion of KDE developers do not have this.  Perhaps it's an American/Canadian problem?  Any rate, if you are going to distribute OO and tote it as a replacement for MS Office, then I think you must get it through your thick skulls that your product now must meet the requirements of the users.  If you are not concerned about the users, then DONT RELEASE THE DAMN SOFTWARE TO THE PUBLIC.  Keep it in your own clique-ish circle so you ego-wackoff each other on irc.\n\n*** And please identify the \"lacks of OSS/FS community\" or something and tell us. It is certain the community doesn't already know without you.***\nI guess I don't know which statement you are referring to because I used the OSS/FS statement twice.\n  The first time in which I stated that a number of OSS/FS applications suffer from the same problem -- a complete and \"professional\" UI/GUI.  In my opinion KDE has the best out of ANY application and that's why I've used it on and off since KDE1, and completely since the KDE 2.0 betas.  If you'll notice, the KDE developers have taken the critiques and suggestions of Eugenia Loli-Queru who seems to be the OSS/FS only real \"guru\" on UI and GUI design and is actually quite good at it.  Getting back to what I was saying before, the UI and GUIs of most OSS/FS applications is functional but that only means that the funcionality that is required for usage is present and not that it is easy to use.  Hell, take the KDE clock applet.  It has really been simplified and is quite good now.  Because see, the problem is that most developers are not users.  Its like the difference between a carpenter and an architect.  A carpenter and build a solid good house, but it might not look good.  Whereas an architect can plan it out so it does look good.  Some developers are blessed with both talents, which is good, but not common.\n  The second time I used the \"OSS/FS community\" statement was when I was saying that the OSS/FS community needs a really good office suite to pull users in from MS.  Again, that nasty \"users\" word that I guess you cant stand.\n  What that <i>It is certain the community doesn't already know without you</i> means, I have no clue.  However, I've submitted bug reports a number of times for KDE and I've helped people out on IRC channels and such.  While I've never actively participated in development of any OSS/FS project, I think I've been at least a decent user.  I'm guessing that you only use the software and have never even submitted bug reports and I'm guessing you are also one of those assholes on IRC that yell at people if they don't know how to solve the problem they are on IRC for.\n\n***I am surprised people don't ask you want you need in order to know what they need.***\nUhm.... what??  Well I'd guess you are making the claim that I feel the entire OSS/FS community should ask me what I want in the software because I think I'm that important.  Well, you know what, I'm not suprised that they don't but in a way they should.  Again, what I was saying before, is that if you don't want users to use the software then don't release it.  Or just keep a simple crappy web page stating where you can get the software and thats it.  The point of all of this, I thought, was to make software for USERS so they can do what they need to do without being bogged down by high purchase prices, high software maintainence costs, and proprietary software.  I thought this was suppose to be a community, not a High School Clique where we outcast anyone who isn't \"one of us\".  This community suffers from a real lack of community-ism and more like a bunch of rowdy wrestlers like my state's ex-governor Jessie Ventura.\n\nSee I, and other users, can't even voice complaints without being yelled at by people like you -- who probably don't even develop the software.  People like you are what hurt this community as a whole.  I can guarantee that if regular people had a choice of dealing with MS software in its current state, or dealing with people like you for support.  They would gladly take the MS software.\n\nI appologise for this long reply, but I think it needed to be said.  I realise there are developers and users who are not like this, and I've talked with a number who aren't.  So if you are not like this, then do not take offense.  If you are like this, then just calm down and don't take an immediate hostility to this.  Think about it for a bit.  Would you really want someone yelling at you because you can't figure out how to use something they wrote whatever?\n"
    author: "SuperPET Troll"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-22
    body: ">> This \"if you don't like it fuck off\" just doesn't fly with anyone else. \n\nThank you. Do you think GNU/Linux needed you to come this far? Certainly not. To come any further? People who only have wishes and aggression to accompany it are only to keep it back.\n\nAccepting competent critic is something else but accepting being called mad by incompetent critics. The ability of expression is certainly higher with Eugenia (than you and me) who deserves adminiration for it.\n\nAnd to come back to the issue. You are in no position to speak for the community. You believe you understand what is needed. You believe you need to tell us. But the community never had a unifying will and never had attempted anything else but to serve its own needs.\n\nExamples exist many. If only that you talk of FS/OSS as one thing. It is not. KDE was initially a baby of the OSS movement. The FS movement would never have touched QT as long as it had no free licence. Or that people work on Gnome and KDE with many different \n\nWhat you don't understand is that the community you are trying to represent with your statements is taking all directions at the same time. GNU/Linux is improving on embeddded, desktop, servers and big iron. The base utilities are improved, everything in every way every day.\n\nAnd then you come and tell people what to do. I guess you also wonder why they don't do it. And when they do you believe it is because you said.\n\nSo well, I cannot stop you. You are part of the community as well. But community is only another word for everybody sooner or later.\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Nonsense"
    date: 2003-08-22
    body: "My wife and I have used it a lot for the last three years. My previous employer had several people inthe small company using it (our choice...we wanted to use it). OO is a great piece of s/w. Just wish it was ready for my iBook also."
    author: "Robocop"
  - subject: "Re: OK, but basically worthless"
    date: 2003-08-22
    body: "Try 1.1rc3, it is a *lot* faster than 1.0.3, and more stable. It takes a lot of time before they can clean things up and get to know the code. Kudos to the guy who pulled this off."
    author: "marko"
  - subject: "Very Nice"
    date: 2003-08-20
    body: "This is very very nice. Does anyone know what the current KOffice developers think of it? It would mean dumping years of work for them, right?"
    author: "AC"
  - subject: "Re: Very Nice"
    date: 2003-08-20
    body: "Don't think so. OOO.org is so sluggish and is toting so much code \naround. That won't change for quite a while because OOO.org must\ndo a lot of things by itself which are already integrated in KDE and\nare therefore already loaded into memory. KOffice will need a much smaller\nfootprint to achieve the same results. Furthermore KOffice has the\nsame open dialogues like KDE, generally the same UI experience which\nis quite important IMO. We're all in competition to M$ Windows.\nAnd you can say about M$ what you want (I don't like them either) BUT\nM$ Word XP seems far more polished and integrated into the OS than\nOOO.org. KOffice has a lot to do in terms of stability and functions but if\nyou just _start_ OOO.org and KOffice and i.e. open 1 document you will\nget a much more \"intergrated\" UI feel than with OOO.org."
    author: "Jan"
  - subject: "Re: Very Nice"
    date: 2003-08-20
    body: "But then what is the point of this 'hack' at all?"
    author: "AC"
  - subject: "Re: Very Nice"
    date: 2003-08-20
    body: ">>And you can say about M$ what you want (I don't like them either) BUT M$ Word XP seems far more polished and integrated into the OS than OOO.org. <<\n\nI agree that MS Word is more polished, but the last time I tried Word XP the integration into Windows XP was quite bad. The worst offense was that Word used the DotNet style, while Windows had the usual XP widget style. "
    author: "AC"
  - subject: "Re: Very Nice"
    date: 2003-08-20
    body: "I consider OOo a short term solution, the long-term solution (when KDE rulez the world ;) is KOffice."
    author: "AC"
  - subject: "kmozillapart"
    date: 2003-08-20
    body: "This is maybe a good time to revive/update the kmozillapart too."
    author: "Anonymous"
  - subject: "Re: kmozillapart"
    date: 2003-08-20
    body: "Why? Konqueror is already very nice and KHTML is actively developed and bugfixed? Why add something bloated as Mozilla as a part? That's bound to be lightning fast. Hm, or maybe not. :)\n"
    author: "Chakie"
  - subject: "Re: kmozillapart"
    date: 2003-08-20
    body: "agreed.. most OSX users I know prefer Safari to Mozilla-based browsers.. once KDE 3.2 rolls around, I think most KDE users will prefer Konqueror rather than many who have switched to Mozilla/Firebird.. I know I did, cvs-Konqi is very nice. \n\nGNOME and other desktop users will of course other browsers, because there is no native khtml-browser for them. Hmm.. somebody needs to make a khtml-based gtk browser that isn't nerfed like gtkhtml..\n"
    author: "anon"
  - subject: "Re: kmozillapart"
    date: 2003-08-20
    body: "\nperhaps because mozilla is less buggy than khtml?\nhttp://overrc.tripod.com/ works in mozilla, but not in khtml.\n\nAnyone want to write a konqueror developer-compatible test case? (apparently, presenting a web page which doesn't work isn't adequate)"
    author: "cbcbcb"
  - subject: "Re: kmozillapart"
    date: 2003-08-21
    body: "The page is buggy. I tried to validate it and it has no doctype so I tried it as both xhtml 1.0 transitional and html 4.01 transitional and neither validate by a fair bit. I also validated the CSS and there are a lot of errors and even more warnings whch should be cleaned up. When you give bad code you can't expect to get correct results. It will sometimes work but not always. \n\nAlso just because mozilla renders it how you want does not mean that it is correct. Mozilla still has a fair number of rendering bugs in it along with khtml."
    author: "kosh"
  - subject: "Re: kmozillapart"
    date: 2003-08-21
    body: "I appreciate the sentiment, but the fact is that there are a large number of pages that do work in Mozilla or Internet Explorer, that don't render properly (if at all) or completely hang or crash Konqueror.\n\nI vastly prefer Konqueror to Mozilla, but I've not had to kill any Mozilla processes in a long time; I have to do it fairly regularly to Konqueror when it freezes up while trying to render some page.\n\nThe correct solution is to have people write bug-free web pages, bug-free Javascript, and bug-free CSS... but that isn't going to happen, and is therefore not an option.  Konqueror should, at the very least, not crash when it hits one of these bad pages.\n\nThat said, I'm not complaining that Konqueror is bad; I'm merely saying that the argument that the parent uses is invalid."
    author: "Sean Russell"
  - subject: "Re: kmozillapart"
    date: 2003-08-20
    body: "I agree!\nFor different reasons:\n1) choice\n2) I develop, under linux, web apps/interfaces for browsers running on windows. Konqueror is not easily available on windows.We standardised on Phoenix. I run konqueror a lot, but need to use phoenix/MFirebird to test my developments.....\n3) XUL\n\n"
    author: "rb"
  - subject: "Re: kmozillapart"
    date: 2003-08-20
    body: "there's code in kdenonbeta that takes XUL files and renders them into KDE interfaces. should be interesting when/if it moves out of kdenonbeta. but that said, what does XUL have to do with embedding the gecko engine in konqueror?\n\nas for Konqi not being easily available on Windows, IE isn't easily available on Linux/UNIX either. ;-)\n\ni agree that testing across browsers is an important aspect of things, but that's for a small segment of the population. i've also noticed that KHTML in CVS is a lot closer to Mozilla's rendering of things (as in, gets more things right) that viewing in Konqi gives a pretty good image of what it will look like in Moz."
    author: "Aaron J. Seigo"
  - subject: "KOffice"
    date: 2003-08-20
    body: "I think would be better spend time with Konqueror importing OO MsOffice filters to KOffie or fixing the problems as Tables or the management of big files (KOffice usually crashes when you are working with big files). I think this is the practical way. Regards."
    author: "Dan"
  - subject: "Kaddressbook for mail-merge"
    date: 2003-08-20
    body: "that's great! The next step I hope for is the integration of Kaddressbook into OpenOffice for mail-merge..."
    author: "kde-user"
  - subject: "Re: Kaddressbook for mail-merge"
    date: 2003-08-20
    body: "..do not forget the kitchen sink!\n"
    author: "AC"
  - subject: "Re: Kaddressbook for mail-merge"
    date: 2003-08-20
    body: "Actually, I am working on doing a print with envelopes on kaddressbook.\nThat way you can select a number of address's, do print, send to printer, and then pick the envelope print."
    author: "a.c."
  - subject: "KDe"
    date: 2003-08-20
    body: "A integration of the GUI is more important for me. There is a script to use your own icon set in OO.org. So where is the controlcenter integration, when I select crystal icons I also want them to get used in OO.org.\n\nalso the menu shall be according to KDe standards. I don't care about gui toolkits, i care about look and feel.\n\n\nIf Ximian integrates OO to Gnome, nic. I want the same to be done for KDe. \n"
    author: "Wurzelgeist"
  - subject: "Re: KDe"
    date: 2003-08-20
    body: "So... dust of your coding skills and get to work, or, alternatively, draw out your wallet and pay someone else to do it for you. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: KDe"
    date: 2003-08-21
    body: "A first integration in the filemanager:\nhttp://souchay.net/news,20030504.html\n\nThe next step would be to create thumbnails as well :)"
    author: "Pierre Souchay"
  - subject: "Offtopic.."
    date: 2003-08-20
    body: "Perhaps a bit offtopic, but why is every newline removed from the messages on this page?\n\nTesting.. 1, 2, 3.. this line was written on a new line.\nThis one as well.\nAnd this one too.\n\n"
    author: "schnitzelkopf@dashei\u00dfePost.com"
  - subject: "Re: Offtopic.."
    date: 2003-08-20
    body: "Yes, it's pretty bad to read.\nReactivating some simple html tags would be good, too.\n"
    author: "CE"
  - subject: "Re: Offtopic.."
    date: 2003-08-20
    body: "Oops, bug..."
    author: "Navindra Umanee"
  - subject: "Re: Offtopic.."
    date: 2003-08-20
    body: "Thanks.."
    author: "schnitzelkopf@dashei\u00dfePost.com"
  - subject: "impressive"
    date: 2003-08-21
    body: "This is quite cool... quite cool indeed.  It would be really nifty to have a KPART that could act like a WM to whatever program.  for instance.... zsnes that way you could embed (insert non-kde app) as a kpart -- cool.\n\nI'm not sure why so many peoples here feel that porting OpenOffice.org to the qt toolkit is so easy because \"Ximian did it with gnome\".  i'm prety sure that Ximian's openoffice isn't gtk2 (although i could easily be mistaken correct me if i'm wrong).  \n\nI feel that koffice is a worthwhile pursuit and shouldn't be \"dumped\".  It's essential for KDE's success (and eventually KGX).  While this is a good devleopment,  I feel that improving KOffice is really the way to go.  KOffice is a REALLY good office suite for what it is (free [beer, speech], and little commercial support).  \n\nthanks koffice -- keep it up (hopefully you can benefit from code in openoffice, hm?) \n\nthanks to cuckoo -- excelent work.  look forward to further updates"
    author: "standsolid"
  - subject: "Re: impressive"
    date: 2003-08-22
    body: "afaik openoffice has its own gui classes and its own way of objective mechanisms... this was once done to make it platform independant... maybe a bit like mozilla.\nThe integration into the gnome-desktop was done mainly by replacing the default OO icons with gnome-like icons. \nIt would be great to replace the file and print-dialogs of OO with the native kde or gnome dialogs (that of course adds another lib to the dependencies..but that would be far more like real 'integration')"
    author: "Thomas"
  - subject: "I always like pretty qt-stuff"
    date: 2003-08-23
    body: "I'm waiting for the day when we just have a few libs like libgecko.so or libOO.so and can use the design we want to. QT is already loaded with klipper/Konqueror  (takes a while to load the 1st qt-app in other WMs) and it should decrease the start time.\n\nBut be careful because integration often leads to vulnerabilities."
    author: "Schugy"
---
Have you ever dreamt of OpenOffice.org integration in KDE?  Perhaps you should try <a href="http://artax.karlin.mff.cuni.cz/~kendy/cuckooo">cuckooo</a>, a <a href="http://developer.kde.org/documentation/tutorials/kparts/">KDE Part</a> which allows OpenOffice.org to be run in a Konqueror window. It is currently limited to just viewer capabilities, but as you can see from <a href="http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/index.php?page=screenshots">the screenshots</a>, the technology is promising. You can of course <a href="http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/index.php?page=download">download</a> and try it, but please be patient with it. Cuckooo is at a very early stage of development, so you might encounter debug info on the standard error output, misbehavior or even crashes. But if you are willing to test it, I am looking forward to feedback!
<!--break-->
