---
title: "Qt3 Documentation Now Available in Chinese"
date:    2003-05-06
authors:
  - "numanee"
slug:    qt3-documentation-now-available-chinese
comments:
  - subject: "I Wonder..."
    date: 2003-05-06
    body: "Nice work. \n\nI wonder if there are many programming languages and APIs that have been translated to non-English languages?  Or does the API have to be askey?"
    author: "anon"
  - subject: "Re: I Wonder..."
    date: 2003-05-06
    body: "I really hope they are NOT translating API's! That would couse a major chaos indeed! "
    author: "Andr\u00e9 Somers"
  - subject: "Re: I Wonder..."
    date: 2003-05-06
    body: "I once read that an over-zealous translator \nnot only translated the documentation of a printer\nbut also the English texts in its Postscript driver :-)"
    author: "cm"
  - subject: "Re: I Wonder..."
    date: 2003-05-07
    body: "What's wrong with that?\n\nI would be really curious to see what kind of code non-English coders write with English-based APIs!"
    author: "anon"
  - subject: "Re: I Wonder..."
    date: 2003-05-07
    body: "A long time ago someone posted that sources of OpenOffice.org that came from the german StarOffice team are with german comments and methods.\n\nJust Kool! (k)DE quality!\n\nNote1: DE = Germany\n\nNote2: hey why don't kde register k.de domain, it would be kool?"
    author: "Anton Velev"
  - subject: "Re: I Wonder..."
    date: 2003-05-07
    body: "argh...\nDE = \"Desktop Environment\"\nKDE != German\n\nplz see http://www.kde.de for german localization of kde homepage\n\nAs a lot of countries deal with each other in an unfriendly way these days it's more important than ever to emphasize that taking part in an open source project is free to _everyone_ on this planet... and KDE gives a good example as it has so many contributors from all over the world."
    author: "Thomas"
  - subject: "Re: I Wonder..."
    date: 2003-05-07
    body: "KDE is international project these days, thanks to globalization - and it should continue that way, but born in Germany, Europe.\nAnd still most contributors, sponsor comapnies, public acceptance and government support is highest in the motherland of KDE.\n\nhttp://www.mathematik.uni-bielefeld.de/~mmutz/kde-developers-2400n.jpg\nhttp://www.mathematik.uni-bielefeld.de/~mmutz/kde-developers-2400.jpg\n(little old but the only found on the web)"
    author: "Anton Velev"
  - subject: "Re: I Wonder..."
    date: 2003-05-07
    body: "While it's true that KDE was born in Germany, it certainly has never been a German project. Most of KDE's early spreading in Germany can be attributed to Kalle's c'T article about KDE development. This, of course, caused a spiril of developers from elsewhere. Today, probably less than 40% of KDE's developers are from germany (ROUGH GUESS)... compare that to Windows... about 60% of Microsoft's developers come from China and India. Is Windows a Chinese/Indian project?"
    author: "fault"
  - subject: "Re: I Wonder..."
    date: 2003-05-07
    body: "But compare 40% and the size of Germany to other countries..."
    author: "Somebody"
  - subject: "Re: I Wonder..."
    date: 2003-05-08
    body: "hm... please compare population numbers, not the area\n\no 370 000 000 people living in Europe (on 3 191 000 km^2)\n\no 83 000 000 european citizens living in Germany (on 357 000 km^2)\n\no 256 000 000 people living in the U.S. (on 9 626 000 km^2)\n\nstatistically there would be 50 % more developers in Europe than in the U.S.,\nwell... statistically...\n"
    author: "Thomas"
  - subject: "Re: I Wonder..."
    date: 2003-05-08
    body: "k.de cannot be registered due to German nic.de rules."
    author: "Anonymous"
  - subject: "Re: I Wonder..."
    date: 2003-05-08
    body: "DeNIC is a kind of mafia."
    author: "Somebody"
  - subject: "Re: I Wonder..."
    date: 2003-05-08
    body: "What the heck you talking about, Willis?  You can't even register single-letter domain names under .com, .org or .net.  X.org is an anomaly."
    author: "Navindra Umanee"
  - subject: "Re: I Wonder..."
    date: 2003-05-10
    body: "well, i beleive that K > X so why not another anomaly like X.org to be K.de ;)"
    author: "Anton Velev"
  - subject: "Hello world"
    date: 2003-05-07
    body: "Heh, \"Hello world\" seems to be \"Hello World\" in chinese :)\n\nhttp://www.qiliang.net/qt/tutorial.html\n\nA kind of international phrase."
    author: "Anton Velev"
  - subject: "Re: Hello world"
    date: 2003-05-12
    body: "I think the phrase \"Hello world\" in English can't be translated into Chinese easily.\n^_^\nI like \"Hello world\"!"
    author: "Cavendish"
  - subject: "Speaking of K..."
    date: 2003-05-08
    body: "Does the K in KDE stand for something??\n\nI mean I once heard it like this: Kinetic Desktop Enviroment, I think that's really cool, KDE sure is dynamic and fast! (could be a lot faster though =p)\n\nOr maybe its for Kool Desktop Enviroment, I heard that too, and its definetely true. \n\nI also heard it called the Kind Desktop Enviroment since its kind to new users and has a kind community =)\n\nThe last one was Korean Desktop Enviroment, but I'm almost positive that was made up by that zealous linux Korean site I visited by accident. \n\nI heard it called Krappy Desktop Enviroment in a message board on a GNOME topic once, but that can't be it google says KDE is the best. Type in: best desktop environment? and KDE comes up, that's how I know its the best.\n\nSomeone just said it was because K was before L and I don't really know what that would mean, amybe that KDE runs on top of linux. But, I really hope that's not why because G comes before K and that would make GNOME better.\n\nOthers say its the karbon, Kilobyte, Kindergarden?!! Karat and someone said it was Kevin's Desktop Enviroment... odd the guy was named Kevin too.\n\nCAN SOMEONE SERIOUSLY ANSWER THIS QUESTION!! THIS HAS BEEN PUZZLING BE FOR... MINUTES!!!!"
    author: "Mindless"
  - subject: "Re: Speaking of K..."
    date: 2003-05-08
    body: "KDE officially stands for \"K Desktop Environment\" \n\nThe K actually once stood for \"Kool\".. but that was fortunatly, and quitely dropped in 1996 :-)"
    author: "fault"
  - subject: "Re: Speaking of K..."
    date: 2003-05-08
    body: "My favorite interpretation is Konfigurable Desktop Environment."
    author: "Anonymous"
  - subject: "Re: Speaking of K..."
    date: 2003-05-08
    body: "Karamba Desktop Environment ;)"
    author: "nac"
  - subject: "Re: Speaking of K..."
    date: 2003-05-08
    body: "It's kindergarten, not kindergarden!"
    author: "Somebody"
  - subject: "something"
    date: 2003-05-13
    body: "sry\nhttp://www.qiliang.net/qt/zh_CN.html\nis the progress information.\n\nAnd \nhttp://www.qiliang.net/nehe_qt/index.html\nis a qt opengl tutorial in Simplified Chinese!\n\nEnjoy it!"
    author: "Cavendish"
  - subject: "Re: something"
    date: 2003-05-14
    body: "Sorry, it must be the ever erratic KDE cut'n'paste that tripped me up...  :-)"
    author: "Navindra Umanee"
---
<a href="mailto:cavendish@qiliang.net">Cavendish</a> informs us that the <a href="http://www.qiliang.net/qt.html">developer documentation</a> (<a href="http://www.trolltech.com/documentation/">en</a>) for <a href="http://www.qiliang.net/qt/index.html">Qt 3.0</a> (<a href="http://doc.trolltech.com/3.1/">en</a>) has now been translated to Simplified Chinese.  Progress information is <a href="http://www.qiliang.net/qt/zh_CN.html">available here</a>, and a mailing list is <a href="http://lists.linux.net.cn/wws/info/qt-cn">also available</a> for those interested.  New: <a href="http://www.qiliang.net/nehe_qt/index.html">translated Qt OpenGL tutorial</a>.
<!--break-->
