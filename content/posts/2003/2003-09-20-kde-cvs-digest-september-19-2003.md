---
title: "KDE-CVS-Digest for September 19, 2003"
date:    2003-09-20
authors:
  - "dkite"
slug:    kde-cvs-digest-september-19-2003
comments:
  - subject: "Jumping Icons"
    date: 2003-09-20
    body: "Go to \"Appearance & Themes\" -> \"Launch Feedback\" in the Control Center and select \"Bouncing Cursor\" in the dropdown menu. Discovered this today and love it. Unfortunately the apps loads so fast in KDE 3.2. ;-)"
    author: "Steffen"
  - subject: "Re: Jumping Icons"
    date: 2003-09-20
    body: "Hehehe, that's a good one!"
    author: "anon"
  - subject: "Re: Jumping Icons"
    date: 2003-09-20
    body: "hey, that sounds really cool ;-)\nwould it be possible to make a screenshots and post it on kdelook.org ?\nmany of kde3.2-hungrey KDE-Fans would enjoy previews like that :)\nthanks"
    author: "Mumumba"
  - subject: "Re: Jumping Icons"
    date: 2003-09-20
    body: ">would it be possible to make a screenshots\n\nYay! Captain Illiterate strikes again.\nNow how do you want to make _static sceenshots_ of _juming_ cursors?\n"
    author: "Anonymous"
  - subject: "Re: Jumping Icons"
    date: 2003-09-21
    body: "No one except you said static screenshots.  GIF or MNG would work, as would any number of other animation formats."
    author: "KDE User"
  - subject: "Re: Jumping Icons"
    date: 2003-09-21
    body: "Fill a wish to bugs.kde.org to ask \"Animatited screenshots for ksnapshot\" ;)"
    author: "Shift"
  - subject: "Re: Jumping Icons"
    date: 2003-09-21
    body: "No chance for KDE 3.2, as we already had feature freeze, but maybe for KDE 4.0 - it should not be too difficult to implement something like this in KMag."
    author: "Olaf Jan Schmidt"
  - subject: "Re: Jumping Icons"
    date: 2003-09-22
    body: "you could use KDE desktop sharing (x0rfb), and then use that vnc2swf recorder prog to make an evil flash file.  \n\nEh? "
    author: "rjw"
  - subject: "Re: Jumping Icons"
    date: 2003-09-20
    body: "I've been using that for awhile now. Its cool :) Now, I'm going to have to figure out a way to make the icon larger --- 16x16 is really just too small. "
    author: "Rayiner Hashem"
  - subject: "Re: Jumping Icons"
    date: 2003-09-20
    body: "Well it is a bit fun it can be annoying after a while. I'm still waiting for the Gnome style way of doing it, Display \"Starting Program\" in the task bar."
    author: "norman"
  - subject: "Re: Jumping Icons"
    date: 2003-09-21
    body: "The kicker taskbar is capable of that too. ... I get a small rotating sandglass in the taskbar indicating a starting program...Though I've deactivated the cursor symbol, the (small) taskbar indication is discreet and does not bother me (like e.g. the cursor symbol does)."
    author: "Thomas"
  - subject: "Re: Jumping Icons"
    date: 2003-09-21
    body: "KDE has been able to do this since KDE 2.0 or so. The icon on the mouse cursor came after."
    author: "Chris Howells"
  - subject: "thanks derek,"
    date: 2003-09-20
    body: "great read as always."
    author: "none"
  - subject: "Syncronization"
    date: 2003-09-20
    body: "Does Kwin III support any special synchronization between window frame and window contents? Supposedly, Metacity uses the SYNC extension to make sure that the window frame and window contents are updated syncronously, to minimize rubber-banding. \n\nHmm, I'm recompiling CVS right now. I guess I'll find out one way or another :)"
    author: "Rayiner Hashem"
  - subject: "Re: Syncronization"
    date: 2003-09-20
    body: ">> rubber-banding\n\nAre you sure you know what you're talking about?"
    author: "poephoofd"
  - subject: "Re: Syncronization"
    date: 2003-09-20
    body: "Of course. Why would you ask that? A description of the feature can be found here:\n\nhttp://mail.gnome.org/archives/wm-spec-list/2002-December/msg00023.html\n\nThe feature is actually implemented in the newest versions of Metacity, though I don't know if the GTK patch is integrated yet."
    author: "Rayiner Hashem"
  - subject: "Re: Syncronization"
    date: 2003-09-21
    body: ">> Of course. Why would you ask that?\n\nBecause rubber banding is something very different.\n\n\nAnyway, I totally agree, it would be great if KWin would support that as well.\n(it even made Metacity fast when I tried back in those days)\n"
    author: "poephoofd"
  - subject: "Re: Syncronization"
    date: 2003-09-21
    body: "I use rubber-banding to refer to the inability of the contents of the window to keep up with the window frame. What definition are you using?\n\nPS> Ugh, in the latest CVS, Konq resizes *much* worse than it used to. The canvas can't even keep up with the resizing of simple pages like dot.kde.org, much less complex ones like Slashdot. I wonder if this is just random fluctations in Konq (that happens sometimes) or if its a problem with the new kwin."
    author: "Rayiner Hashem"
  - subject: "Re: Syncronization"
    date: 2003-09-22
    body: "I've always considered rubber banding to be drawing a dashed line or rectangle to indicate where and how an object which is being resized will be drawn. Google seems to consider that the authorative meaning of rubber-banding.\n\nOfcourse, if you're not using opaque resize, then there will be rubber-banding, because the window will be reduced to a rubber band.\n"
    author: "Joeri Sebrechts"
  - subject: "Makes sense"
    date: 2003-09-20
    body: "> <rant>\n> It's great to plan future of KSpread when there are\n> such obvious, long-standing and easy to fix bugs.\n> </rant>\n\nWell, I think it makes sense to plan the future of KSpread if you have the chance to meet?! Especially if is about redesigning (which involves bug fixes here: performance bug,...). (See dot news about that)\n\nFix the bugs if they are easy to fix - if you have the time -> some people don't seem to have it right now..."
    author: "somebody"
  - subject: "Re: Makes sense"
    date: 2003-09-21
    body: "I do not think that Luk\u00e1? has meant it in the first degree. He is KOffice's release manager after all.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Makes sense"
    date: 2003-09-21
    body: "Grr.. the ? was supposed to be a s caron. Sorry, Lukas."
    author: "Nicolas Goutte"
  - subject: "Thank you!"
    date: 2003-09-20
    body: "The KWin rewrite sounds great and a lot easier for developers, maybe I'll even make a style ;)\n\nI'm also glad to ssee a lot of bugs being fixed. From the \"weekly summmary\" :\nhttp://bugs.kde.org/weekly-bug-summary.cgi \n\n424 bugs fixed and 144 wishes complete. by the time you read this, the number will probably be higher! This ought to be a far less buggy release than 3.1. \n\nHowever, I really hope it will be delayed about a month so that it can be in a new year and people will think it's even more modern, and also it will be more polished. In addition, few people would actually get 3.2 if it's released in December, most will wait 3-4 months for the distros, so even if it's rleased earlier than February it won't make a difference for most people. 80% of users just get KDE from their distributions.\n\nAlso, I'm wondering, with all these bugs fixed so fast will KDE be able to accomplish it's release goals http://developer.kde.org/development-versions/kde-3.2-features.html most items seem to be in the to-do and in progress section so what's goign on here? Are many items actually done but just not reported?\n\nAnyway, thanks KDE contributors! \n\n"
    author: "Mario"
  - subject: "Re: Thank you!"
    date: 2003-09-20
    body: "Years ago I worked for a fellow in construction. We did concrete work. We would get to the jobsite, figure out what needed doing, he would disappear for a few minutes, then announce that the concrete was ordered, and would arrive at 2 PM.\n\nWe got the forms done, just in time usually.\n\nSo coolo has decided that a finished release will happen on Dec 8.\n\nDerek"
    author: "Derek Kite"
  - subject: "default kwin decoration?"
    date: 2003-09-20
    body: "I don't know what is 3.2 default kwin decoration , but I think it shouldn't be keramik as in 3.1 , because it confuses new users as its buttons aren't the regular _ square X , I'd like to see knifty or the one with plastic style is the default 3.2 kwin decoration , what do you think?"
    author: "Mohasr"
  - subject: "Re: default kwin decoration?"
    date: 2003-09-20
    body: "One thing that confuses users most is constantly changing default themes. As we are talking about the type of user that never change their settings (do we?) those are best served with as few theme changes as possible. While I became a real fan of plastik (looks slicker than Keramik), I think it's not a good idea to force the user to yet another widgetstyle change. Maybe for 4.0, but not within the 3.x series.\n\nCheers,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "Change for the better"
    date: 2003-09-20
    body: "When a change is for the better, I am all for it. Still, even if it wasn't made the default theme, Plastik could be in kde3.2."
    author: "jukabazooka"
  - subject: "Re: Change for the better"
    date: 2003-09-21
    body: "Plastik is going to be in KDE 3.2. It's on the feature schedule. Can't remember if it's already been imported."
    author: "Chris Howells"
  - subject: "Re: Change for the better"
    date: 2003-09-21
    body: "It has already been imported into kdeartwork."
    author: "Anonymous"
  - subject: "Re: default kwin decoration?"
    date: 2003-09-20
    body: "Then at least make the buttons nicer/better/more usable."
    author: "poephoofd"
  - subject: "Re: default kwin decoration?"
    date: 2003-09-20
    body: "I'm beginning to consider Keramik a liability. KDE looks 10x prettier on my desktop than it does in any of the screenshots, mainly because Keramik looks really strange, and the defaut colors aren't that great either. The man problem is the gradient in the toolbars and menubar. It makes it look like the toolbar is curving out. The icons are still flat, so how are they sitting on that toolbar? Are they lying tangent to the edge of the curve? If so, how come there is no shadow underneath? You also get a weird effect with buttons that can be pushed in --- you'd expect the sunken portion to be deeper at the middle of the gradient, but it isn't. Also, Keramik highlights a redraw problem in KDE Styles. Styles with gradient toolbars cannot redraw fast enough to keep up with the window border, so you can see the toolbar creep across the screen as you resize it. With Keramik this is especially bad, because the gradient is so heavy and the toolbar looks so deep. You get a very visible \"cliff\" of the edge of the toolbar lagging behind the window frame. Styles that paint their background via Qt (use the PaletteBackground mode) or can redraw fast enough (like ThinKeramik and a few others) don't suffer from this problem."
    author: "Rayiner Hashem"
  - subject: "Re: default kwin decoration?"
    date: 2003-09-21
    body: "hey ! as for style , keramik was and *still* my favorite style ,\nBUT I'm talking about keramik Window Decoration that I think should be changed\n\nWindow decoration NOT widget style "
    author: "Mohasr"
  - subject: "Re: default kwin decoration?"
    date: 2003-09-21
    body: "Oh? I thought Plastik will be the default in 3.2?"
    author: "Wurzelgeist"
  - subject: "Re: default kwin decoration?"
    date: 2003-09-21
    body: "nope, there won't even be any consideration for it as the default style until KDE 4.0. We can't have three different default styles for three different >minor< (3.X) releases.\n\nAfter KDE 3.2 is released, I'm sure there will be some discussion about this issue, especially if KDE 3.3 is dropped in favor of 4.0. The prime candidates are Plastik and ThinKeramik. "
    author: "no"
  - subject: "Re: default kwin decoration?"
    date: 2003-09-21
    body: "Try using Keramik style + window decoration with the Plastik color scheme, I think this rocks! :-)"
    author: "Steffen"
  - subject: "25889 lines"
    date: 2003-09-20
    body: "Hmm. That shows how \"useful\" CVS statistics are. But anyway, that must be a record ;)."
    author: "Lubos Lunak"
  - subject: "Re: 25889 lines"
    date: 2003-09-20
    body: "Well, if you save up a year's work, commit it all at once to HEAD, this is what happens :)\n\nI think coolo had a week way up there, due to regenerating all the .po files.\n\nDerek"
    author: "Derek Kite"
  - subject: "Horizontal Scroll Bar Trouble"
    date: 2003-09-21
    body: "Does anyone else notice this page is wider than the browser window?  I'm using KDE 3.1.4, and this page is wider than my browser window.  The window has to be 965 pixels before the problem goes away (with the font size set at -4, the window only has to be 860 pixels wide).  I have to shrink the page to less than 700 pixels before Mozilla draws a horizontal scroll bar.  Interesingly, if you save the page and look at the preview in the open file dialog, it looks fine there.\n\nBug fix 61730 [1] was supposed to resolve this issue, and it did resolve the issue on some sites, but apparently Konqueror still exhibits the problem right here on the dot.\n\n[1] http://bugs.kde.org/show_bug.cgi?id=61730"
    author: "Burrhus"
  - subject: "Re: Horizontal Scroll Bar Trouble"
    date: 2003-09-23
    body: "Yep, happens here as wel.\nI use kde 3.1.4, and no matter how large I make the Konqueror window, the width of the page is larger then the Konqueror view, so a horizontal scroll bar is visible.\n\nSame with kopete, no matter how large its chat window is, there is always a horizontal scroll bar.\n\nRinse"
    author: "rinse"
  - subject: "Re: Horizontal Scroll Bar Trouble"
    date: 2003-10-26
    body: "Same here. 3.1.4 was supposed to fix this?? well it didn't!  :)"
    author: "alice"
  - subject: "Kicker notification of applications changing state"
    date: 2003-09-21
    body: "Just wondered if this in any is in CVS and if not WHY? As far as i see it, it's a pretty important feature, i mean if im chatting in kopete and just change to konq with teh caht window minimised i'd like to be notified by Kicker as to whether i recieve a message (app changes state?). Would this involve the new Kwin and Kicker or something else?\n\nBTW this isn't a feature request, just asking whether it is there, if not feature request will go up."
    author: "CMF"
  - subject: "Re: Kicker notification of applications changing state"
    date: 2003-09-21
    body: "Well, I think what you want is already possible, because I have seen new-message notification in Netscape Mail (4.xx). It would change the application icon in the taskbar. So, really, you should ask the kopete guys to implement something like this for you.\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: Kicker notification of applications changing state"
    date: 2003-09-21
    body: "I implemented KWindowInfo which offers this sort of thing in KDE 3.1. It lets you put messages in the titlebar and provide temporary overrides of the WM icon.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Kicker notification of applications changing s"
    date: 2003-09-21
    body: "Why not just a flashing or pulsing taskbar button?\n\nI really don't see it (quickly) if only an icon or text changes..\n"
    author: "poephoofd"
  - subject: "Re: Kicker notification of applications changing s"
    date: 2003-09-21
    body: "It can do that too, you just make the overlay have the appropriate appearance. \n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Kicker notification of applications changing state"
    date: 2003-09-23
    body: "I've seen apps flash in the application panel in CVS as of the latest rebuild (this past weekend). For example, if I start Konqueror and then go back to kmail when konq finally starts, it's taskbar item flashes blue (I don't have any startup notification things enabled)\n\nI have yet to test to see how well the flashing corresponds to application notification (ksirc, etc) but I think it's a great idea."
    author: "Vic"
  - subject: "Wishlist for features in konqueror 3.2"
    date: 2003-09-22
    body: "1. Per site loading policy for images and flashes (like for javascript and cookies).\n\n2. Downloading of files by external program (like galeon does).\n\n3. Possibility to save loaded into konqueror window by kparts .pdf,.dvi,.ps files.\nAbility to download these files by (external) download tool instead of loading by kdvi etc.\n\n4. Change in zooming policy, instead per page to per site or per tab (like in galeon).\n\n5. Blocking popups.\n\nI think it is not hard to implement these features."
    author: "Andrey V. Panov"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2003-09-22
    body: "Feature I want most for Konqueror are to have these 10 bugs or features fixed/added:\n\nhttp://bugs.kde.org/show_bug.cgi?id=62679\nhttp://bugs.kde.org/show_bug.cgi?id=62678\nhttp://bugs.kde.org/show_bug.cgi?id=58943\nhttp://bugs.kde.org/show_bug.cgi?id=58944\nhttp://bugs.kde.org/show_bug.cgi?id=59791\nhttp://bugs.kde.org/show_bug.cgi?id=59789\nhttp://bugs.kde.org/show_bug.cgi?id=52884\nhttp://bugs.kde.org/show_bug.cgi?id=53772\nhttp://bugs.kde.org/show_bug.cgi?id=37300\nhttp://bugs.kde.org/show_bug.cgi?id=62736\n\nThey were not odered by importance."
    author: "Alex"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2003-09-22
    body: "> I think it is not hard to implement these features.\n\nGreat, when do you start to?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2003-09-22
    body: "> I think it is not hard to implement these features.\n\nHeh.  Spoken like a true IT manager.\n\n"
    author: "Tukla Ratte"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2003-09-22
    body: ">> I think it is not hard to implement these features.\n\nNever underestimate the art of adding a new feature.\n"
    author: "poephoofd"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2003-09-22
    body: "1. Developers are going to do little more than giggle about feature requests posted here.  We have a bug tracking system for that.  bugs.kde.org is where this belongs.\n\n2. We're already several weeks into the feature freeze for 3.2, so if it's not on the feature plan and not already done, it ain't gonna happen this go around.  ;-)\n\n3. Please people, don't say, \"this should be easy\" if you don't know any of the issues involved.  If it's \"easy\" then where are your patches?  ;-)\n\n[steps off of soapbox]"
    author: "Scott Wheeler"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2003-09-23
    body: "Better and unified Graphics for KDe Games. Draughts and Chess..."
    author: "Andr\u00e9"
  - subject: "80% of your wishes come true"
    date: 2003-09-23
    body: "> 1. Per site loading policy for images and flashes (like for javascript and cookies).\n well, ok.. one point to you\n\n> 2. Downloading of files by external program (like galeon does).\nWhat are you talking of? KGet (an \"external\" download manager) ist the way it works... right now\n \n> 3. Possibility to save loaded into konqueror window by kparts .pdf,.dvi,.ps files.\n> Ability to download these files by (external) download tool instead of loading by kdvi etc.\nI don't get the point here, but out of a guess: either preview a file with the kparts component, open it separately or choose \"save link as\"... it's your choice\n \n > 4. Change in zooming policy, instead per page to per site or per tab (like in galeon).\nWell it _is_ per tab or per site (in a tab)\n \n>  5. Blocking popups.\nufff...konqueror actually blocks popups (Choose \"smart\" for the \"open new windows\" policy in the javascript settings)"
    author: "Thomas"
  - subject: "Re: 80% of your wishes come true"
    date: 2003-09-24
    body: "I want to respond to point #3:\nOften, save link as... does not work. the link is often a script, and downloading the script is not what you want. Moreover, this whish should be easy to fulfill:\nsince the \"send file...\" menu item is often present, a \"save as...\" menu item could be easily added. It simply means renaming a temporary file, instead of encoding it and sending it via mail."
    author: "luciano"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2004-01-30
    body: "Fix the long-standing bug (since Mosaic) which allows a linewrap before any start-tag, even if it's not preceded by space. Example: ...mail me for help (<tt>foo@bar.com</tt>). If this occurs such that the email address has to be wrapped to the next line of display, it leaves the open-parenthesis all by itself at the end of the line! (Go on, do it...create a file with a long line ending as above, display it, and then narrow the window until it wraps.)\n\nRight-click on a link should include a \"Copy Email Address\", the same as \"Copy Link Location\" but without the \"mailto:\" method.\n\nRight-click on a link should include a \"Send this Link by Email\", so you don't have to load a page just to be able to send its URI in an email message.\n\nPop-up control on a per-site basis.\n\nDon't tinker with the toolbars, they're just fine as they are.\n\nA Print Preview would be nice (export to PDF/PS?)\n\nAdd much better recognition of common filetypes. Having it ask if you want to open a .asp file in Kword is not useful. \n\nADD XML and XSLT! PLEASE! <grovel/> Having it spawn Moz every time is a pain.\n\nFix printing so it uses the configured fonts. Right now (3.1-15, KDE 3.1-12, RH9)  everything prints in 10pt Times or smaller, regardless of what the CSS said and regardless of what display font you have set. If necessary, have a \"Print Magnification\" setting that will let you blow everything up x% for printing.\n\nA new \"Syphon Image URIs\" and \"Syphon Link URIs\" function would be useful, either to a file or to a new tab for cut and paste. This is equivalent to the --images and --links options of dog (a drop-in replacement for cat, see http://jl.photodex.com/dog/): I'm sure Jason and Jacob would let you use their code.\n\nMake Konqueror restart with menus, a toolbar, bookmarks, and a location bar after a crash: currently it loses all these settings, sometimes even after a normal exit. If this config is read from a file on startup, do a close() when it's been read.\n\n///Peter"
    author: "Peter Flynn"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2004-01-30
    body: "> Fix the long-standing bug (since Mosaic)\n\nkhtml/Konqueror was never based on Mosaic.\n\n> Pop-up control on a per-site basis.\n> A Print Preview would be nice (export to PDF/PS?)\n\nLet me guess, you never used Konqueror 3.2 or in case of print preview even KDE 3.1."
    author: "Anonymous"
  - subject: "Re: Wishlist for features in konqueror 3.2"
    date: 2004-02-03
    body: "> khtml/Konqueror was never based on Mosaic.\n \nBut it displays the same error. Try it. Something, somewhere, must be using the same broken model of parsing as all the other browsers. The existence of a start-tag in a document does *not* mean this is a posssible line-wrap point: in particular, a start-tag immediately following an open-bracket is precisely *not* a place the line should be wrapped. Let Konqueror be the first browser to fix this bug!\n\n> Let me guess, you never used Konqueror 3.2 or in case of print preview even KDE 3.1.\n\nNo, for support reasons I'm dependent on the RPMs available from RH's up2date.\nThis is excellent news, thanks. My problem.\n\nUnfortunately preview via the KDE print filter hangs more often than not (I just tried it on this page...sure enough), and only displays Times, regardless of the fonts on the screen display. I was hoping for something a little more robust.\n\n///Peter"
    author: "Peter Flynn"
---
In <a href="http://members.shaw.ca/dkite/sep192003.html">this week's CVS digest</a>:
The KWin rewrite has been merged into mainline KDE. Optimizations in KABC, the <a href="http://kaddressbook.org/">address book</a> framework, and the
<a href="http://www.konqueror.org/">Konqueror</a> listview.
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/qtruby/README?rev=1.8&content-type=text/plain">QtRuby</a> gains support for KDE classes. <a href="http://xmelegance.org/kjsembed/">KJSEmbed,</a> a JavaScript implementation for KDE,
now has SQL database bindings. Plus a large number of bugfixes.
<!--break-->
