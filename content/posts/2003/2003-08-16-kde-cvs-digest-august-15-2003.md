---
title: "KDE-CVS-Digest for August 15, 2003"
date:    2003-08-16
authors:
  - "dkite"
slug:    kde-cvs-digest-august-15-2003
comments:
  - subject: "Thanks Derek"
    date: 2003-08-15
    body: "Never expected it to be out so fast, tahnks a lot!"
    author: "Dan"
  - subject: "55321 - 55312 "
    date: 2003-08-16
    body: "Jesse Yurkovich committed a change to kdelibs/kate/data\n* Add REXX and CUE sheet hightlighting  wish 55321\nRefer to Bug 55321 - korn is using way to much memory\n\nHuh?\n\nProbably a reference to bug 55312 makes more sense ;-)\n"
    author: "MK"
  - subject: "Re: 55321 - 55312 "
    date: 2003-08-16
    body: "Fixed. Thanks for pointing it out. The entry was moved to the feature section since it was a wishlist.\n\nDerek (who fondly remembers REXX)"
    author: "Derek Kite"
  - subject: "Re: 55321 - 55312 "
    date: 2003-08-16
    body: "REXX (OS/2) and AREXX (Amiga) were powerful languages.\nHere you can find a list of implementations:\nhttp://www2.hursley.ibm.com/rexx/rexxplat.htm\n(have a look also in dmoz.org)\n\nAnd here are the 3 available REXX interpreters for Linux:\n- Object REXX from IBM (freeware _not GPL_ under Linux)\n  http://www-3.ibm.com/software/awdtools/obj-rexx/\n- REXX/imc, GPL\n  http://users.comlab.ox.ac.uk/ian.collier/Rexx/index.html\n- Regina, GPL and cross-platform\n  http://regina-rexx.sourceforge.net/\n"
    author: "andrianarivony"
  - subject: "Kate!"
    date: 2003-08-16
    body: "<i>Kate has Smart Indent !! :)  Just for C-Style languages for now.</i>\n\nYES!!  Thank you Jesse!  Time to 'cvs up', methinks... \n:D\n\n(ps- why do my HTML tags never work? :( )"
    author: "Jason Harris"
  - subject: "Re: Kate!"
    date: 2003-08-16
    body: "HTML posts have been disabled for around 3 years now, ever since dot.kde.org was goatsexed."
    author: "Justin"
  - subject: "Re: Kate!"
    date: 2003-08-16
    body: ">why do my HTML tags never work?\n\nA long, long, time ago, someone created a javascript that when posted here, replaced all links with hrmm.. other websites :)"
    author: "fault"
  - subject: "Re: Kate!"
    date: 2003-08-16
    body: "Thanks! :)\n  I have some more improvements for C Style sitting locally too ... just waiting for the ok from the other Kate devs about some source organization issues.  Should be noted that C Style just insn't for C/C++ ... should work for C#/Java/ and possible Perl (somewhat :)\n\nBTW: Python is done now too and Pascal is in the works :) ... anyone code in Ada still ;)"
    author: "Jesse"
  - subject: "Re: Kate!"
    date: 2003-08-16
    body: "Haskell! Haskell! :)\nNow seriously for languages like haskell, which are indentation sensitive, automatic indentation is particulary usefull and help avoid many annoyoing errors.\n\nSting"
    author: "Sting"
  - subject: "Re: Kate!"
    date: 2003-08-16
    body: "now all kate needs is to forget about project management and get a dark background (aka black) colour scheme. let people choose between dark or light, or just rip out vim's wonderful syntax highlighting... \n\nthis is the major reason why i dont use kate often."
    author: "not_registered"
  - subject: "Re: Kate!"
    date: 2003-08-16
    body: "As long as I am sitting in front of a crt Monitor I always choose a dark background because it is better to the eyes. With a lcd display it's the other way round. A white background seems to be less straining to the eyes ...o.k., off topic..."
    author: "Thomas"
  - subject: "Re: Kate!"
    date: 2003-08-17
    body: "If you like vim more than kate, check out http://www.freehackers.org/kvim/\n\nAs always, choice is good."
    author: "also_not_registered"
  - subject: "Re: Kate!"
    date: 2003-08-20
    body: "Totally agree! Black backgrounds rule!\n\nSeriously, I would probably switch from Xemacs to Kate if there was a  light text on dark  background color theme and a drop down list of key bindings: emacs-key-bindings for me!!!\n\n-Amit"
    author: "KUser"
  - subject: "As always"
    date: 2003-08-16
    body: "Thanks Derek! I've always enjoyed reading your fine CVS digest :)\n"
    author: "Jasem Mutlaq"
  - subject: "kdelibs/kdeu optimization"
    date: 2003-08-16
    body: ">Here's a pretty significant speed improvement that takes an \"n\" out of\n>the time it takes to paint a listview.  This is very noticeable on\n>large lists where rendering the list is very time consuming.  This still\n>isn't as fast as painting QListViewItems, probably because itemBelow()\n>isn't as fast as it ideally would be, but it's a big step in the right\n>direction.\n\nany chance to have this backported into the 3.1 branch?"
    author: "ac"
  - subject: "Re: kdelibs/kdeu optimization"
    date: 2003-08-17
    body: "Optimizations aren't typically backported unless they fix a bug, which this doesn't."
    author: "Scott Wheeler"
  - subject: "One week quite active"
    date: 2003-08-16
    body: "Let us see if at this point I can find a little disc space and compile it, I have desire to prove that new KDE."
    author: "Vasten"
  - subject: "Re: One week quite active"
    date: 2003-08-16
    body: "Huh? Now what exactly do you want to tell us with that?\n*puzzled*\n"
    author: "Jan"
  - subject: "Re: One week quite active"
    date: 2003-08-16
    body: "You may want to put it off for a few weeks. There is a bit of brokeness currently, and you may have a challenge getting it to compile. I suspect post n7y things will be much better.\n\nDerek"
    author: "Derek Kite"
  - subject: "Bye bye, SCO"
    date: 2003-08-16
    body: "\"Ohhhhhh... this feels good... bye bye SCO...\"\n\nA little less SCO support in KDE, even though it was obsolete :)  Good going."
    author: "Haakon Nilsen"
  - subject: "ocrad link broken"
    date: 2003-08-17
    body: "ocrad is linked to an https url..."
    author: "Lee"
  - subject: "Re: ocrad link broken"
    date: 2003-08-17
    body: "You consider a secure link broken?"
    author: "Anonymous"
  - subject: "Re: ocrad link broken"
    date: 2003-08-17
    body: "Given that it doesn't work, and that changing it to http does work?  Yep ;)"
    author: "Lee"
  - subject: "Re: ocrad link broken"
    date: 2003-08-17
    body: "The https version https://savannah.gnu.org/projects/ocrad/ works for me...\n\n"
    author: "cm"
  - subject: "A bit OT: Spam Filter for kmail"
    date: 2003-08-18
    body: "These days I read an interesting article in a german computer magazine about self learning spam filters, the so called Bayes- filters.\nAs I deleted around 600 (!) spams from an email account yesterday, I'm just courious if somebody knows about such filters for kmail?\n\njust interested ):-\n\nthefrog"
    author: "thefrog"
  - subject: "Re: A bit OT: Spam Filter for kmail"
    date: 2003-08-18
    body: "http://bugs.kde.org/show_bug.cgi?id=46826#c6"
    author: "Anonymous"
  - subject: "Re: A bit OT: Spam Filter for kmail"
    date: 2003-08-19
    body: "Thanx!"
    author: "thefrog"
---
In this week's <a href="http://members.shaw.ca/dkite/aug152003.html">KDE-CVS-Digest</a>: <A href="http://www.kde.org/apps/kooka/">Kooka</A>, the KDE scanning application, now supports
<A href="https://savannah.gnu.org/projects/ocrad/">ocrad</A>, a GPL OCR engine.
<A href="http://www.slackorama.net/cgi-bin/content.pl?juk">JuK</A> gets a history playlist feature, along with
some serious optimizations.
KMenu, the <A href="http://kmail.kde.org">KMail</A> address selection dialog,
<A href="http://korganizer.kde.org">KOrganizer</A> and KSnapshot get usability improvements.
<A href="http://edu.kde.org/kstars">KStars</A> gains the capability of generating skymaps from the commandline.
And many bugfixes in <A href="http://www.koffice.org">KOffice</A>,
<A href="http://kate.kde.org">Kate</A> and
<A href="http://kopete.kde.org">Kopete</A>. 
<!--break-->
