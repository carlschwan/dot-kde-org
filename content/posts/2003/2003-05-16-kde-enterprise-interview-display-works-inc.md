---
title: "KDE Enterprise: Interview with Display Works Inc."
date:    2003-05-16
authors:
  - "ctenharmsel"
slug:    kde-enterprise-interview-display-works-inc
comments:
  - subject: "nice"
    date: 2003-05-16
    body: "Great story.  Thanks to Tim at Display Works for sharing it with us, and thanks to Christopher for reviving KDE::Enterprise!"
    author: "Navindra Umanee"
  - subject: "KDE for life!"
    date: 2003-05-16
    body: "GO KDE! :-)"
    author: "Mysterious"
  - subject: "Nice read, KDE3.2 when?"
    date: 2003-05-16
    body: "Hello,\n\nthis was indeed nice to read. When will KDE 3.2 have a release plan? Does it have one yet?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Nice read, KDE3.2 when?"
    date: 2003-05-16
    body: "There is a large thread in kde-devel about this. Essentially the feeling is 3.2 isn't ready yet, not quite feature complete, too many bugs. The PIM stuff is well along, but not finished. Kmail isn't quite stable with it's new functionality. etc.\n\nI would have to agree with the assessment. There are some serious improvements that all would like to enjoy, the speed of konqueror and improved rendering. But it isn't quite finished yet. \n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Nice read, KDE3.2 when?"
    date: 2003-05-16
    body: "That's Ok. I think that 3.1 is good enough to take a longer development phase now.\n\nWhen 3.2 is ready, it will be the complete roundup I guess. So thanks for being part of it.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Nice read, KDE3.2 when?"
    date: 2003-05-16
    body: "Too bad Konqueror can't be made a seperate package with its own upgrade and release cycles. \n\nI know it is bound to khtml which is bound to kdelibs, but its nice that Safari can be updated every once in awhile on the Mac without upgrading all of MacOS. \n\nWould there ever be a way to package the big KDE apps separately, with their own release cycles? Kmail, konqueror, etc. For those that wish to wait, they can wait for the next KDE release which would have all of the latest apps. \n\nSeems kind of silly that a more stable konqueror has to wait on less stabke kmail, etc. \n\n..just too anxious to see the new khtml with the Safari merges I guess :) "
    author: "Joe"
  - subject: "Re: Nice read, KDE3.2 when?"
    date: 2003-05-16
    body: "Certainly possible, but releasing stuff is a lot of work. The reason why KDE has only few large packages is that this is the most convenient way for developers...\n\n"
    author: "AC"
  - subject: "Re: Nice read, KDE3.2 when?"
    date: 2003-05-17
    body: "Why not grab kdelibs and kdebase (and perhaps also qt-copy) from CVS or ftp.kde.org snapshots and use DO_NOT_COMPILE env vars to only compile Konqueror and it's dependancies? I would recommend to install it seperate from your stable KDE installation."
    author: "fault"
  - subject: "konqueror as a standalone package"
    date: 2003-05-17
    body: "interesting....\n\nany howto for newbie ? \n\ni would like to see konqueror.org releasing nightly build.\n\n\n"
    author: "somekool"
  - subject: "Re: Nice read, KDE3.2 when?"
    date: 2003-05-16
    body: "What about KDE3.1.2?"
    author: "Janne"
  - subject: "Re: Nice read, KDE3.2 when?"
    date: 2003-05-16
    body: "Very very soon.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Great! But..."
    date: 2003-05-16
    body: "That's great! I'm always glad to see what real companies are doing with KDE and I am very happy to see things working so well for Display Works. But, surely there are larger \"enterprises\", that are using KDE, than a 12 user shop? And please, not the old City of Largo article again."
    author: "Huzzah"
  - subject: "Re: Great! But..."
    date: 2003-05-18
    body: "It would be nice if \"someone\" dug out some more companies who would give interviews ;-P"
    author: "kidcat"
  - subject: "Cool interview"
    date: 2003-05-16
    body: "Nice to see more companies adopting KDE.\n\nOn other news, Qt 3.2 beta was just released with a number of improvements.\n\nAlso is this a bug:\n\nWhen i save any html file called index.html in my home directory, every time I try to go to the home directory after restarting Konqueror I get that HTMl page and I DON'T KNOW HOW TO STOP IT, I actually needed to go into GNOME just to rename the file, I'm not sure if this affects other KDE filemanagers like Krusader."
    author: "Alex"
  - subject: "Re: Cool interview"
    date: 2003-05-16
    body: "As easy as \"View/[ ] Use index.html\"."
    author: "Anonymous"
  - subject: "Re: Cool interview"
    date: 2003-05-16
    body: "\nHi,\n\nhave you tried \"View->Use index.html\" ?"
    author: "yg"
  - subject: "Cool interview"
    date: 2003-05-16
    body: "Nice to see more companies adopting KDE.\n\nOn other news, Qt 3.2 beta was just released with a number of improvements.\n\nAlso is this a bug:\n\nWhen i save any html file called index.html in my home directory, every time I try to go to the home directory after restarting Konqueror I get that HTMl page and I DON'T KNOW HOW TO STOP IT, I actually needed to go into GNOME just to rename the file, I'm not sure if this affects other KDE filemanagers like Krusader."
    author: "Alex"
  - subject: "Re: Cool interview"
    date: 2003-05-16
    body: "In konqui, menu 'View',  select off  'Use index.html'."
    author: "a.c."
  - subject: "Re: Cool interview"
    date: 2003-05-17
    body: "You probably have View->use index.html on. It's useful for viewing local html sites and docs that are in html. A lot of software uses HTML docs. I don't think this is a bug at all, but a feature. \n\nOther KDE file managers, such as Krusader, don't have HTML browsing anyways, so it should not mimic this behavior. I'm not sure why Nautilus doesn't do this by default, but I think it sure should though.  Perhaps open up a bug report on gnome's bugzilla. I guess not many people use Nautilus to browse local html documents, but rather use Epiphany or Galeon."
    author: "fault"
  - subject: "Re: Cool interview"
    date: 2003-05-25
    body: "after unchecking \"use index.html\" I would like it to stay unchecked.  But it's back everytime I restart konqueror.  I've tried saving view profiles.  My changes stick but not that one.\n\n"
    author: "rob"
  - subject: "Re: Cool interview"
    date: 2004-02-26
    body: "This works:\nAdd \n[URL properties]\nHTMLAllowed=false\n\nto the .directory file in your home directory.\n\nThere's a LOT that doesn't work, up to and including adding HTMLAllowed=false to /etc/opt/kde3/share/config/konquerorrc\nor making sure that same option is in\n/home/$user/.kde/share/config/konquerorrc\nand doing a quick chmod -w on that (write-protecting it).\n\nThanks to the helpful folks on alt.os.linux.suse - sktsee in particular."
    author: "Hetta"
  - subject: "Re: Cool interview"
    date: 2004-02-26
    body: "Thank you Hetta,\n\nrob"
    author: "rob"
  - subject: "Good article"
    date: 2003-05-16
    body: "One thing confused me though:\n\n\"The experiment was a tremendous success. The support issues that would arise time and again had to do with the relative maturity of KDE as a desktop, such as how seamless kprinter was to the end user, and the cut and paste paradigm that seemed broken to them. However, for day to day use, KDE provided a consistent and usable interface for our users.\"\n\nIs it just me, or does that sentance not make sense? It appears to say two contradictory things at once....\n\nAnyway, nice one KDE people. Good to see companies starting to use free software on the desktop."
    author: "Mike Hearn"
  - subject: "Re: Good article"
    date: 2003-05-17
    body: "Basically:\n\nThey could not understand the X11 copy+paste system. It does indeed confuse people who are used to Windows or MacOS. I've seen people middle click in web pages and expect it to reveal a vertical scrolling widget like in Internet Explorer. Instead, in Konqueror, it either goes back in history or pastes text (if over a text area.)  whoops. Of course, people tend to live and learn. I don't think X11 copy+paste is any more difficult for someone who is just starting out computers than anything else. It just provides one more way of copy/pasting, selection paste from middle click.\n\n"
    author: "fault"
  - subject: "Re: Good article"
    date: 2003-05-17
    body: "Yeah, I realise that, I meant the grammar of the sentance. It appeared to be saying two contradictory things at once."
    author: "Mike Hearn"
  - subject: "Re: Good article"
    date: 2003-05-17
    body: "\"in Konqueror, it either goes back in history or pastes text\"\n\nOr it goes to the URL in your clipboard."
    author: "Mike"
  - subject: "Re: Good article"
    date: 2003-05-17
    body: "\"The support issues that would arise time and again had to do with the relative maturity of KDE as a desktop, such as how seamless kprinter was to the end user\"\n\nI think it means that KDE was a bit immature back then. When he says \"relative maturity of KDE\" he did not mean that KDE was mature, he meant that it was immature"
    author: "Janne"
  - subject: "Re: Good article"
    date: 2003-05-18
    body: "Yes it is alittle odd isn't it....\n\nAnyway, about the copy paste thing, you can always use Ctrl C and Ctrl v like in Windows. But, I like the X11 way a lot because I don't need to touch the keyboard.\n\nFor example if there is a link text I just select it highlight waht's in the adress bar and middle click than enter. "
    author: "Alex"
  - subject: "Clarification"
    date: 2003-05-18
    body: "The part quoted by Mike Hearn above was referring to KDE 2.1.2. That KDE used QT 2.x which didn't separate the \"normal\" clipboard from the selection buffer clipboard back then leading to the mentioned confusion. This changed with KDE/QT 3.x, and Tim Brodie consequentially says in the interview: \"In the beginning of May 2003, we migrated forward to KDE 3.1, and our staff have been very excited; 'blown away' is probably closer to the mark.\""
    author: "Datschge"
  - subject: "Thanks!"
    date: 2003-05-16
    body: "That helps a lot, =) Its always cool learning something new =). But, why would someone want to use index.html when clicking the filemanager mode? You wouldn't be able to get files would you?"
    author: "Alex"
  - subject: "Re: Thanks!"
    date: 2003-05-17
    body: "I'd guess using index.html when clicking the filemanager mode allows you to customize the filemanager view MS Windows Explorer style, ie. with a header sidebar on the left and some annoying \"you are not intended to view this folder\" etc. Combined with KDE's Kiosk feature it's surely very useful."
    author: "Datschge"
  - subject: "How do I do that"
    date: 2003-05-17
    body: "So far it seems to only display a whole html page."
    author: "Alex"
  - subject: "Re: How do I do that"
    date: 2003-05-18
    body: "index.html is extreemely usefull when browsing my local dirs with documentation.. clicking on a dir is like cliking on a \"book\""
    author: "kidcat"
---
Recently, Tim Brodie of <a href="http://www.displayworksinc.com/">Display Works Inc.</a> emailed KDE thanking us for a job well done.  Display Works Inc. has been using KDE as their official desktop environment for some time now, and recently migrated to KDE 3.1.  I had a chance to ask Mr. Brodie a few questions about Display Works' experience with KDE.  Head over to  <a href="http://enterprise.kde.org/">KDE::Enterprise</a> to <a href="http://enterprise.kde.org/interviews/displayworks/">read the interview</a>.
<!--break-->
