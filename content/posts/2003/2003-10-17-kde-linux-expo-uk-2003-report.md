---
title: "KDE at Linux Expo UK 2003 Report"
date:    2003-10-17
authors:
  - "jriddell"
slug:    kde-linux-expo-uk-2003-report
comments:
  - subject: "LockDown Documentation"
    date: 2003-10-17
    body: "It's available at/linked on http://www.kde.org/areas/sysadmin/"
    author: "Anonymous"
  - subject: "Re: LockDown Documentation"
    date: 2003-10-17
    body: "Thanks for this, I'll pass it on. Is it possible to get this site linked from the sidebar that appears on most KDE websites?"
    author: "Ben Lamb"
  - subject: "Re: LockDown Documentation"
    date: 2003-10-18
    body: "Good idea, just added it."
    author: "Chris Howells"
  - subject: "Merchandising in the EU"
    date: 2003-10-17
    body: "\"Our stock of polo shirts and badges sold out and almost 500 flyers and stickers were handed out.\"\n\nI've searched around for KDE Merchandising but all I found were U.S. based stores. Can I order KDE related stuff from somewhere within the European Union? Euro countries preferred.\n\nJ.A."
    author: "jadrian"
  - subject: "Re: Merchandising in the EU"
    date: 2003-10-17
    body: "KDE polo shirts and badges (called pins) are at the events registration page \n\nhttp://events.kde.org/registration/register.phtml"
    author: "Jonathan Riddell"
  - subject: "Re: Merchandising in the EU"
    date: 2003-10-17
    body: ">Can I order KDE related stuff from somewhere within the European Union? Euro countries preferred.\n\nSure, kernelconcepts sell EU wide: http://www.kernelconcepts.de/products/kde/index-en.shtml\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Merchandising in the EU"
    date: 2003-10-18
    body: "Thank you both. I just whish there was more selection, as in the KDE cafeshops like this one: http://www.cafeshops.com/kde :-/\n\nJ.A."
    author: "jadrian"
  - subject: "Re: Merchandising in the EU"
    date: 2003-10-18
    body: "Are you dutch. If so .. please e-mail me about KDE-merchandise ... \n\n\nFab"
    author: "Fab"
  - subject: "Re: Merchandising in the EU"
    date: 2003-10-18
    body: "Nope, I'm portuguese.\n\nJ.A."
    author: "jadrian"
  - subject: "Umbrello?"
    date: 2003-10-18
    body: "Integrated to Kdevelop???"
    author: "Gerd"
  - subject: "Re: Umbrello?"
    date: 2003-10-19
    body: "Sorry, the integration won't make it into KDE 3.2.\n\nRefer to http://sourceforge.net/mailarchive/forum.php?forum_id=472\nfor further discussions.\n\n--Oliver\n"
    author: "Oliver Kellogg"
  - subject: "Truely beautiful"
    date: 2003-10-18
    body: "KDE 3.2 really is very striking. Plastik and CrystalSVG really provide a clean, elegant look. However, one thing that bothers me is that KDE requires a lot of customization to look good. Compare the screenshots on kdelook.org to the default screenshots on kde.org, and you'll see what I mean. \n\nMy tastes run towards the minimal (I like classic Aqua BeOS, and the styling of the iPod) so I have the following customizations to my setup:\n\n1) Revamp of all app toolbars that I use to get rid of the Microsoft-ish \"replicate the entire menu in the toolbars\" mentality wherever it may hide. Konq goes down to one toolbar (navigation buttons + location bar) as does almost every other application. Kudos to JuK, KWrite, Kate, Kontact, and many others for coming that way by default.\n\n2) Patches to Plastik to makes scrollbars and combo-box sizes configurable, as well as to fix some drawing inefficiencies.\n\n3) Patch to Qt to make the drawing of menu icons (which I find immensly distracting, especially in apps like Konqueror that provide icons for nearly every menu item) optional.\n\nThe only thing that really bothers me today is that many panels don't have the right default size --- you have to resize them before you can see everything. While this problem is most likely due to me using large fonts (because my laptop LCD is very high-res) I have to say that GNOME handles the situation a lot better than KDE. In fact, GNOME usually looks a lot cleaner (very OS9) than KDE, even after all my modifications. However, I'd wager it'd be a lot less work to get KDE up to GNOME's UI-polish standards than to get GNOME up to KDE's infrastructure and technology standards :)"
    author: "Rayiner Hashem"
  - subject: "Re: Truely beautiful"
    date: 2003-10-18
    body: "> Compare the screenshots on kdelook.org to the default screenshots on kde.org, and you'll see what I mean. \n \nSimple reason, wouldn't it be boring and uncool if everyone would post screenshots of default settings?"
    author: "Anonymous"
  - subject: "Re: Truely beautiful"
    date: 2003-10-18
    body: "> 2) Patches to Plastik to makes scrollbars and combo-box sizes configurable, as well as to fix some drawing inefficiencies.\n\nPlease consider submitting any patches you have to kde-devel@mail.kde.org, particularly if you've noticed drawing inefficiencies. I'm sure they'd be appreciated."
    author: "Ben Lamb"
  - subject: "Re: Truely beautiful"
    date: 2003-10-18
    body: "I realised suddenly I agreed with you on the toolbar issue so i tried to remove what I could from the Konqi toolbar, but I couldn't remove the view + view - actions, nor the security action, nor find, no kget.\n\nAny tips?"
    author: "Max Howell"
  - subject: "Re: Truely beautiful"
    date: 2003-10-18
    body: "Ah, figured it out. Silly me. Had to alter the mainToolBar KHTMLpart one."
    author: "Max Howell"
  - subject: "Yes it is a problem"
    date: 2003-10-18
    body: "I have been using linux since 98, and I am the translator of KDE to Danish, but \nnevertheless I had never heard of imagemagic. So what is my point?:\nIn spite of running linux for many years, I did not know of some of the wonderful programs that exist on my platform. That is our biggest problem right now."
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "\"the application names are confusing\""
    date: 2003-10-18
    body: "\"the application names are confusing\" -- lets create a place where one can easily change the names of all apps! (ie the name in the menu and the name in the title bar)\n\nWhen about is clicked the original name will always reveal it self...\n\nPossible??"
    author: "cies"
  - subject: "Re: \"the application names are confusing\""
    date: 2003-10-18
    body: "I think it would be very confusing if the same application would be known under different names, especially for users compaining about bugs and the developers of these applications.\nIf you want, you can allready change the name in the menu."
    author: "Andr\u00e9 Somers"
  - subject: "Re: \"the application names are confusing\""
    date: 2003-10-18
    body: "I pointed out the Description field which is listed next to the application name on the K-menu. It can optionally be displayed instead of the application name.\n\nSome applications such as \"TNEF File Viewer\" don't have descriptions by default, I had to ask someone what this did. In case you're wondering, it's for decoding the winmail.dat files that Outlook uses to send attachments."
    author: "Ben Lamb"
---
<a href="http://www.linuxexpouk.co.uk/">Linux Expo UK 2003</a> took place in London on October 8 - 9. KDE took part in the .org village along with twenty other Free Software projects (<a href="http://jriddell.org/photos/2003-10-linux-expo-kde-stall.jpg">photo from front</a>, <a href="http://jriddell.org/photos/2003-10-linux-expo-kde-stall-back.jpg">photo from back</a>). We demonstrated KDE 3.2 Alpha to a constant stream of visitors (over 5 500 were at the show, most of them seemed to be at the .org village stand) and handed out several hundred leaflets. 
<!--break-->
<p>Several KDE developers including Jono Bacon, Chris Howells and Jonathan Riddell were joined by Debian packager David Pashley (<a href="http://jriddell.org/photos/2003-10-linux-expo-kde-stall-jonathan-david.jpg">photo</a>) and salesman extraordinaire Ben Lamb (<a href="http://jriddell.org/photos/2003-10-linux-expo-kde-stall-ben-and-punters.jpg">photo</a>) to demonstrate the forthcoming KDE 3.2 release and solicit feedback from existing users. We met several companies looking to deploy GNU/Linux and KDE on the desktop and talked to journalists from all the major UK Linux magazines. </p>

<p>The users have decreed KDE 3.2 looks "beautiful". Our demo machine was using the Plastik theme with the Crystal icons. </p>

<p>Besides the beautiful new look, visitors were very impressed with <a a href="http://www.kontact.org/">Kontact</a>. For people who knew about KDE 3.2 it was the first thing they asked to see. However confusion abounded that Kontact only worked with <a href="http://kolab.kde.org/">Kolab</a> and not standalone. We emphasised that even when running it as a standalone application it is possible to exchange appointments via email and publish calendars to the web. </p>

<p>Everyone had their own wishes. Some users asked for LDAP connectivity, another was very pleased to discover <a href="http://devel-home.kde.org/~kgpg/">KGPG</a> had all the functionality he needed to easily manage his encrytion and keys. Smaller features such as the new system tray applet for changing the screen resolution and TrueType font previews were universally applauded. </p>

<p>Almost everyone wanted to know if/when their distribution would be shipping KDE 3.2, how they could upgrade and whether we had the code available on CD. It's clear that many users do not know how to upgrade to the latest release and some are still running KDE 2. The difficulty of software upgrades and installation was one of the general GNU/Linux grumbles people kept mentioning. The others were drivers for some hardware (caused by manufacturers who do not work with the open source development process) and the integration issues which <a href="http://pdx.freedesktop.org/Software/hal">HAL</a> aims to fix. </p>

<p><a href="http://www.koffice.org/">KOffice</a> 1.3 was also demonstrated although most of the people were already using OpenOffice.org. Several people were unaware of KOffice altogether and many did not realise that the applications have improved considerably since its first release. <a href="http://www.koffice.org/kexi/">Kexi</a>, the database frontend, was generating a lot of interest but sadly we weren't able to demo it. A number of people asked after <a href="http://uml.sf.net/">Umbrello UML Modeller</a> which pleased Jonathan. </p>

<p>Criticism flowed as well, ranging from "the application names are confusing" to "the logo is rubbish". More constructively a plea, from a large corporate, for better documentation about locking down KDE desktops and possibly a GUI front-end to the kiosk framework. It is a shame when KDE has the features but they go unused for lack of documentation or publicity. </p>

<p>An almost continuous stream of people visited the stand on both days. Most were existing KDE users and at least a quarter had been following the progress of KDE 3.2 and would have upgraded anyway. After seeing a demo nearly everyone was convinced that it would be a worthwhile upgrade. </p>

<p>Our stock of polo shirts and badges sold out and almost 500 flyers and stickers were handed out. Next year we will take more. <p>

<p><i>Article by Ben Lamb and Jonathan Riddell, photos by Chris Howells and UK Linux .</i></p>