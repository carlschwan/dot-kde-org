---
title: "KDE Developers' Conference: Call for Papers"
date:    2003-05-24
authors:
  - "hporten"
slug:    kde-developers-conference-call-papers
comments:
  - subject: "Thanks!"
    date: 2003-05-24
    body: "Thanks Derek!... uh,... oh,... hey! This is not the CVS digest, is it!? ;-) Where's Derek today? Is he alright?\n\nAnyway, good luck all of you who will attend to the Nove Hrady Conference"
    author: "Derek's fan"
  - subject: "Re: Thanks!"
    date: 2003-05-24
    body: "How about if a Perl or PHP \"free geek\" writes dynamic webpage that generates the content of the digest for Derek?\nThen you will have digest whenever you reload it!\nIt has only to access the cvs databese and the kde C++ \"free geeks\" who decide that a given commit of their work is important to the project just just paste a line like this at the end of the commit message:\n\n---\nfor DerekScript\n[bug #34535] [optimize] [in-reply-to-commit #38945]\n---\n\nBeleive me it is possible.\n\n\"free geek\" is used for a \"open source community contributor\". I think Derek is definitely a HTML \"free geek\", but may be also he is a Perl or PHP one, or also may be he already did it."
    author: "Anton Velev"
  - subject: "Re: Thanks!"
    date: 2003-05-25
    body: "Why do you say that Derek is an \u00abHTML geek\u00bb?\nEveryone is capable of writting an HTML page using Mozilla Composer :p\nAnd hopefully, Quanta will also have an WYSIWYG mode!! Terrific :D"
    author: "Ricardo"
  - subject: "development strategy"
    date: 2003-05-24
    body: "Well, I already posted my suggested development strategy to the KDE Development list and basically, nobody seemed to like it.\n\n:-("
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "What is it? What have you suggested?\nI also would suggest something but if there was a kgod to hear it."
    author: "Anton Velev"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "I guess he means the bigheaded http://lists.kde.org/?l=kde-devel&m=105302958112948&w=2: \"I am a IT professional [..] I think that a mature IT professional should get some respect from self taught hackers half his age.\" Many of his postings show this attitude like: I'm professional, you're young/hacker, KDE is a mess. I'm here to ransom you."
    author: "Anonymous"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "Thanks for the link, I obviously missed it before. Now after reading it I know as much as before though. (Question to James Richard Tyrer:) What is that whole fuzz intended to be about?"
    author: "Datschge"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "Sorry, but that is the wrong link.\n\nMy comments on a \"Demming style\" development strategy were posted some time ago.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "http://lists.kde.org/?l=kde-devel&m=103855731706945&w=2"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "Did you ever consider that KDE 3.1.0 compiled for the great majority without problems and your compilation problem is not representative? In my opinion KDE 3.1.2 and what 3.1.x will follow show that developers care about bugs and a switch unstable->branch and stable->HEAD wouldn't change anything.\n\nAnd thanks for delivering another example of your attitude: \"I also wanted to make a note here so that everyone that reads this will know:  I am a professional programmer retired on disability.  I studied Electrical Engineering & Computer Science in college.  There is much that I do not know about the KDE project.  But I know a lot more about computer programing than some of the self taught hackers that have been telling me that I don't know anything (yes that is an arogrant remark). \n  I don't appreciate it and you only make fools of yourself doing it.\"\n\nAnd you still wonder that you don't get appreciation/respect back..."
    author: "Anonymous"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "> Did you ever consider that KDE 3.1.0 compiled for the great majority without\n> problems and your compilation problem is not representative?\n\nNo, because there were problems with it.\n\nWhy did I, and others, notice that there were problems with it?\n\nWell in my case it was because I had been building 3.0+ from CVS for some time without any problem but when I updated with the release tag that it didn't work.\n\nI'm sorry that I don't remember the exact problems but they were fixed.\n\n> In my opinion KDE 3.1.2 and what 3.1.x will follow show that developers care\n> about bugs and a switch unstable->branch and stable->HEAD wouldn't change \n> anything.\n\nThings have improved, but there is still some sentiment that some developers care more about new features than fixing bugs.\n\nWhat I said was more than your terse response indicates, but yes it would change things.  Bugs would first be fixed in the current release and new features would have to accommodate bug fixes rather than bug fixes having to accommodate new features.\n\nYou may disagree with my ideas, that is fine.  But your remarks actually show that you didn't take the time to understand my ideas before rejecting them out of hand.\n\n> And thanks for delivering another example of your attitude\n\nAnd I see that you didn't get the point of what I said was a deliberately arrogant remark either.\n\nI did have a point there.  I thought that it was possible that some of the 'arrogant developers' had made remarks because they thought that I was some newbie that had just wandered in.  I was wrong, 'arrogant developers' make these remarks to everyone that isn't part of the hacker culture.\n"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "You should be aware of the fact that this kind of discussion style is quickly bringing yourself on many peoples' ignore list. Please separate you ideas and suggestions from your personal issues and avoid using the latter. It's a waste of time discussing about personal issues while the same time could be used to clarify ideas and suggestions instead which would actually help everyone."
    author: "Datschge"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "> It's a waste of time discussing about personal issues while the same time could\n> be used to clarify ideas and suggestions instead which would actually help \n> everyone.\n\nThank you: I agree 100%.  So, I will stop responding to personal comments.\n\nI hope that you can see, as I do, that the problem does not lie with me.  I would like to discuss ideas but those that don't respond with personal attacks.\n\nWhy do you think that this is what happens?  It is certainly not what other engineers would do!  Are they threatened by someone that has different ideas?  Is this just part of the 'arrogant developer' syndrome?"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "I also had a suggestion: regular Knoppix based Live CD releases containing KDE CVS Head snapshots for danger-free preview and encouraging press coverage of all those \"technology previews\", teasing everyone who's saying KDE's not progressing due to missing development releases. Now I just need people picking that up and work on it (no kgod needed)."
    author: "Datschge"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "Why don't you create them? Telling what has to be done doesn't get the work done (with greetings to James 'I do not feel that I am currently able to write code' Richard Tyrer)."
    author: "Anonymous"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "I'm not telling what has to be done but suggesting what would be nice to have considering \"everyone\"'s request to KDE to release develoment releases aka snapshots like Gnome does but KDE remove long time ago (and rightly so imo). Sure, if nobody else picks up my suggestions I'm surely going to end up doing it myself."
    author: "Datschge"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "What about ftp://ftp.kde.org/pub/kde/snapshots ?\n\nOr are you talking about compiled snapshots? Then feel free to talk to the distributors or volunteer to do it yourself. To cite http://www.kde.org/download/packagepolicy.php:\n\"When will you offer packages for my favorite distribution?\nWe will ship packages for your favorite distribution when (and only when) your distribution or a dedicated user builds the packages and submits them to us for redistribution. The KDE Project itself does not make any binary packages.\"\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "I know that already, thanks you. =) What I was talking about was regularly creating a live CD containing a recent KDE snapshot from CVS Head to showcase it as \"technology preview\" so people can easily check out to where KDE is heading. Doing that as live CD has the advantages that it's easy to pick up, easy to review, easy to include a lot of warning signs \"not intended for productive use\", easy to promote the current development etc. etc. while avoiding the issues usually introduced with \"official development snapshot releases\" (ie. users installing it and then misleadingly thinking that it can be used for productive use creating a lot of questions and bug reports etc). And as I said if nobody intends to pick that up I'll surely giving it a try myself sometime. =)"
    author: "Datschge"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "Sorry, but this conference is only for development strategies with developers who are committed to do them...\n"
    author: "AC"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "Did I miss something here?\n\nA 'development strategy' is NOT writing code.\n\nIt is a methodology that is used in wrinting code.\n\nAnd just exactly why would a methodology based on Deming's ideas of quality assurance be unsuitable for this conference?\n\nTo me it appears to be a trade off: more features vs. higher quality. \n\nCommercial software has the exact same issue and they usually make the same (WRONG) choice: more features."
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "It's a waste of everyone's time to do more than to submit a feature or change request to bugs.kde.org, UNLESS there is a developer who is at least interested in implementing it. It just annoys everyone, as you can see.\n\nThe scarce resource is not ideas or concepts, it is developer time.\n"
    author: "AC"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "> It just annoys everyone, as you can see.\n\nNot really.. A lot of developers tend to look at bugs by number of votes. So, if you can get people to vote for your bugs, it can do wonders."
    author: "lit"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "Sure, voting is a good thing. But please dont make off-topic advertisements for your favorite bug, because when everybody does this dot.kde.org wont be much fun to read...\n"
    author: "AC"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "As an 18 year old who has been teaching himself C++ since he was 16 (specifically so that he could help with KDE development), I have to say that I did find your message rather rude and arrrogant, while at the same time you were accusing other people of being rude.\n\nYes, the way KDE is developed certainly could be improved I'm sure. However you seem not to totally understand the processes by which KDE is developed, and the way that KDE is developed is not going to suddenly change overnight because some relative outsider (e.g. somebody that doesn't contribute code) says that another way is \"better\"."
    author: "Chris Howells"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "I suppose that this will start a major flame war, but:\n\n1.  People have been rude to me.  They have dismissed what I have said with a one sentence comment that indicated that they didn't really know what I was talking about but appeared to indicate that I didn't know anything.\n\n2.  You probably don't understand it, but you do represent my remark.  You think that because you have taught yourself C++ that you now know programing.  So, you probably know C++ better than I do, but do you think that 5 years persuing a degree in EE & CS taught me nothing more than you know?\n\n3.  No, I do not understand the way KDE is developed, but I don't mean that the same way that you do.  Perhaps you could explain it to me.\n\n4.  I should make it clear that ideas I have suggested are not my ideas.  They are from what I have learned in college, and over the years since I attended UCSB in the '70s."
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "I'm not interested in flaming, only getting to the facts.\n\n1. I'm sorry that you feel that, but I do not consider people to have been rude to you, certainly not intentionally.\n\n2. Rubbish. Totally, absolutely, and fundamentally wrong. There's a vast amount about C++ and programming I don't know. I certainly don't consider that I \"know about programming\". I strive to improve where I can. I do not know of any fellow contributors _All_ of my fellow contributors are similarly eager to learn in my experience.\n\nOn the other hand, it seems to me that because you spent 5 years doing a degree in EE & CS you automatically feel qualified to try and force your opinions onto all about the way that KDE should be developed. \n\n3. Describing it in words would be rather hard. The only way you can understand is by obvserving the way that the project runs itself for several release cycles, maybe contribute a bit of code.\n"
    author: "Chris Howells"
  - subject: "Re: development strategy"
    date: 2003-05-24
    body: "\"I do not know of any fellow contributors _All_ of my fellow contributors are similarly eager to learn in my experience.\"\n\nshould read\n\n\"I do not know of any fellow contributors who are not similarly eager to learn.\""
    author: "Chris Howells"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "1/2/4. If you want to impress people with your programming skills, produce code (documentation etc) that impresses them. Or help people when they ask for help. \nBut when you send unsolicited change requests/re-design proposals/development strategies, then don't complain when people do not agree with you or don't want to do them for various reasons.\n\n3. Actually quite easy: who codes decides. Maintainers are responsible for their work. If there is something that affects more than a single sub-project or for other significant decisions, decisions are made by consensus. Or, in minor cases, by talking to one of the respected long-time developers.\n\n"
    author: "AC"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "> Actually quite easy: who codes[:] decides\n\nThat is what I thought, but didn't want to be so arrogant as to say it.\n\nThere is nothing wrong with the Maintainer making the final decision.  \n\nThe problem I have is that this may also mean:\n\n'who codes: designs'\n\nwhich is not good.  It is just something that I learned in engineering school that discussion of a designe with other engineers always leads to improvements.  Specifically, in trying to explain something to someone else, you learn about it.  Sort of the reverse of the Socratic method of teaching."
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "Maybe you should learn from Eric Laffoon.  Look what he achieved with Quanta Plus by being behind many of the design decisions and sponsoring."
    author: "ac"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "> It is just something that I learned in engineering school that discussion of a designe with other engineers always leads to improvements\n\nYup, I agree.. but this is not ALWAYS needed. Most of KDE, imho, is too trivial architecturally to be implemented in more than one method. Sure, improvements could be made, but gains would be marginal at best. \n\nInstead, perhaps you could focus on non-trivial things such as khtml, kdelibs/kdecore (most of kdelibs/kdeui is pretty trivial wrappings/extentions of Qt classes), and perhaps even Qt itself. Large apps such as koffice and kdevelop would also benefit, of course. \n\nNote that there is plenty of predicience in KDE's history to support this fact. For example, kparts, which was largely implemented initially by Torben Weis back before KDE 2.0, but only after a large amount of discussions on kde-devel, which resulted in many ideas/proposals that were shot down before Torben came out with kparts.\n\nMost of the freedesktop.org has also been developed in this matter."
    author: "lit"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: ">>Instead, perhaps you could focus on non-trivial things such as khtml, kdelibs/kdecore (most of kdelibs/kdeui is pretty trivial wrappings/extentions of Qt classes), and perhaps even Qt itself. Large apps such as koffice and kdevelop would also benefit, of course. <<\n\nBut here, again, all discussions are useless unless somebody wants to do the changes. Feel free to discuss changes when you want to improve it. But when you are not going to do it, such discussions are just a waste of time...\n\n\n \n"
    author: "AC"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "Yes, and quite often developers discuss their ideas with other people on the project's mailing lists or on irc, telephone, in real life and so on. Who says that this doesnt happen?\n\n"
    author: "AC"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "> Who says that this doesnt happen?\n\nI am not saying that it doesn't happen.  \n\nI was only saying that it should happen.\n\nWhat I was advocating on the KDE-Development list was that a suggestion box list might be a good idea -- where ideas could be discussed better than using bugzilla for that.  Some of the negative comments there indicate that a seperate list would be a good idea since some of the developers are not interested/do not have the time for this.  And, some of the KDE-Development list is requests for help.\n \n "
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "So you are another one of the mass of people who have oh-so-good ideas, but unlike the majority you imply that you are right because of your precious degree?\n\nYou want a suggestion-box list - maybe you should look at the kde-usability list archive? Such \"suggestion\" lists might work for smaller projects, but with KDE there is so much to suggest, and so many people who think they have a suggestion worth of sharing with the rest of the world, that it becomes impractical.\n\nDo yourself a favor, distinguish yourself from the rest and actually submit a patch. And please, save us from your \"I'm sick and thus have an excuse not to contribute anything but ideas, but I have degrees and thus you need to listen\" attitude. I recently had some serious health problems as well, which temporarily rendered me unable to use a computer. but unlike you, I realized that submitting my ideas (and trust me, I have a shitload of them) does not make much sense because the KDE folks already get enough of shallow suggestions, without actually implementing your proposal and attaching a patch, your suggestions will drown in the masses.\n\nMaybe you should start your \"What James thinks is right\" list and all the people who care could subscribe there."
    author: "anon"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "Notice the methodology of the anonymous troll.\n\nDeliberately misunderstanding what is said so that it is easier to ridicule it.\n\nAnd specifically making use of a common logical fallacy.\n\n\tall S is P -> all P is S\n\nHe takes as his first assumption that everything that I said is wrong and just finds ways to support his conclusion.\n\nI did not even mean to suggest that I was always \"right\" because of my \"precious degree\".  \n\nI digress to state what all non-trolls should know: that a degree is \"precious\" because of the work needed to earn it and for no other reason.  I mentioned my \"degree\" not because I expected people to accept everything I said because I expected everything I said to be accepted, but because I was tired of having anything I said dismissed with one line by some 'arrogant developer'.\n\nThe actual issue is where suggestions should be posted.  Currently, many of these suggestions are posted to Bugzilla as 'wishlist' items.  Many of these need some work which could be achieved by discussion.  Many of them are useless.  But what all of them do is clog up Bugzilla.  Is this a good thing?  Is it better than having a list for such suggestions?\n\n> Do yourself a favor, distinguish yourself from the rest and actually submit a\n> patch. \n\nI have submitted patches.  I helped fix two bugs with the current Ksplash.  I submitted a patch for the: 'startkde' script, but nobody on: kde-devel was interested in discussion of it.\n\n> And please, save us from your \"I'm sick and thus have an excuse ... \n\nIt is the simple fact.  I am available because I am retired on disability.  The project can accept that or not.\n\n>  KDE folks already get enough of shallow suggestions, ...\n\nExactly, and they clog up Bugzilla.  But with a suggestion-box list, they would either be worked on till they were usable or they would be discarded.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "Sir,\n Perhaps you should look at why people seem to quickly attack you.\nOne of the best pieces of advice I got was from one of my lecturers at university, who said that the trouble with students, is when they get their first job, they immediately start trying to fix all the problems.\n\n\n\nJohnFlux"
    author: "JohnFlux"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "Yes, John.\n\nBut, that *is* what engineers do.  It is what they are trained to do.\n\nFind the fault.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "> What I was advocating on the KDE-Development list was that a suggestion box list \n> might be a good idea --\n\nNo, that would be a very bad idea, there are way too many places where you can give suggestions already. Nobody can keep track of all of them, and adding yet another one won't make that any better. Bugzilla on the other hand is centralized.\n\n>  where ideas could be discussed better than using bugzilla for that.\n\nBugzilla is actually the ideal place since where else can you get other people to vote for your ideas, where else are all suggestions easily searchable as soon as someone intends to implement it, where else does a message or a patch not get lost?\n\n> Some of the negative comments there indicate that a seperate list would be a good\n> idea\n\nI count 81 mailing lists alone at lists.kde.org and quite some more mailing lists which are managed with pipermail instead mailman and such don't appear on lists.kde.org, you gotta be joking saying a separate list would change anything.\n\n> since some of the developers are not interested/do not have the time for this.\n\nAs pointed out many times already in the discussions you started: why discuss something if in the end there's nobody to implement it? Only code or other actually usable contributions count in the end, everything else simply is hot air. Get over this simple fact."
    author: "Datschge"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "> As pointed out many times already in the discussions you started: why discuss\n> something if in the end there's nobody to implement it? Only code or other \n> actually usable contributions count in the end, everything else simply is hot \n> air. Get over this simple fact.\n\nThank you, it is good to know that. :-)\n\nSo, it is my understanding that I should abandon my efforts to go through the large number of bugs clogging Bugzilla because this isn't a useful contribution. "
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "No, everybody here agreed that working with Bugzilla is great. It allows discussions, and even better: the maintainers of the application get all comments, unlike mailing lists that are not read by many maintainers."
    author: "AC"
  - subject: "Re: development strategy"
    date: 2003-05-25
    body: "You're wrongly mixing up relation and functionality of lists.kde.org<->mailman and pipermail."
    author: "Anonymous"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "Yes, please educate me. =) Nobody on the Nove Hrady mailing list seems to know why it doesn't appear on list.kde.org. And additionally the navigation style of the archive imo sucks compared to the style used in seemingly all mailing lists at list.kde.org. So again, please educate me since I'd love to know the problem to be able to solve it. =)"
    author: "Datschge"
  - subject: "Re: development strategy"
    date: 2003-05-30
    body: "Contact sysadmin@kde.org"
    author: "Waldo Bastian"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "i think you will have to prove that whatever (code or strategy) you come up with will work good, better than the current code/strategy in the current situation.\n\nand arguments like \"its documented in CS books\" don't count, you will have to explain why something will work better (not in the common case but in the KDE case). if you can convince enough coding people it will work better, your ideas will be taken into account.\n\noffcourse its difficult to get concrete convincing arguments for a development strategy, because you will have to show that switching is worth it: you will have to show that advantages are significantly more important than disadvantages.\n\nyour comments sometimes can be interpreted (don't know if you meant it) \"trust me, i DO now about that because ...\". That won't convince anyone, in contrary ..."
    author: "ik"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "Yes, you make a valid point.  That is why discussion is so important.  But, we don't have discussion.\n\nDiscussion is: \"tell me more about this, it might ... \"\n\n'Arrogant Developer' is: \"go away, you are wrong and I don't want to listen anyway\" :-)\n\nWhich do you think is better?\n\nI'm not going to say that \"its documented in [a] CS book\" although I might say that there was an interesting article in Communications (of the ACM) about this. -- you do read that, don't you?\n\nNo, I am NOT saying: \"trust me, i DO know about that because ...\".  If that is what you think, you have not understood.  What I am saying is: 'listen to me, I *do* know something about CS ...'."
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "Did you already think about giving a talk at Nove Hrady? After all the Call for Papers says:\n\"Do you want to present a particular programming pattern, a tool, a development strategy, or anything else that helps KDE developers become more productive?\"\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "> No, I am NOT saying: \"trust me, i DO know about that because ...\". If that is what you think,\n> you have not understood. What I am saying is: 'listen to me, I *do* know something about\n> CS ...'.\n\nah ok. i don't think that will convince people to listen either. And i think its unneeded, its not because someone flames you down that your message was read by nobody.\n\nthe difficult part is that the \"arrogant developer\" doesn't have to prove you are wrong, but you have to prove that you are right (especially if people don't know you). I think that is perfectly normal.\nyou also cannot expect others to do work for you when they are not convinced, sometimes they won't even give you information about the current situation (if that's what you mean). Your challenge is then to use your own knowledge/resource/time to find information and arguments yourself ...\n\nin short, i think if you come up with a very concrete \"look, now we do it that way, and its better if we do it that way because ...\" you will be heard.\n\nsaying 'i think A works better than now, but i don't know about now, so please explain me so i can start a discussion' will not work ...."
    author: "ik"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "i think i can summarize it like this: in an opensource world you can take less for granted than you did in CS world. So you have to do your own research more, even for stuff you were taking for granted (like a development strategy). And that research should come before serious discussion.\n"
    author: "ik"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "ok replying to myself again. i think your 'research' could look very vaguely like this (its just an idea, its not an outline or so)\n\n* what do you want to propose.\n- what are the documented advantages ?\n- what are the 'building blocks' ? (eg: a building block could be the fact you can give developers assignments).\n\n* the KDE community.\n- investigate how it works, what are its properties \n  (\"soft\" that can be changed if its really worth it, and \"hard\" that cannot be changed at all)\n- and what is the current strategy.\n- are those building blocks still present in the KDE community ?\n- are those documented advantages still advantages for KDE ?\n- which advantages does your thing have that the current strategy doesn't have ?\n- which advantages does the current strategy have that your strategy doesn't have ?\n- try to predict which advantages will be seen as significant by the community, and which won't.\n\n* adapting\n- see what you can achieve (you can only change some things, and you cannot change too much)\n- try to see if you can reach the advantages with the least intrusive change"
    author: "ik"
  - subject: "Re: development strategy"
    date: 2003-05-29
    body: "OK, I see your point.  Although this isn't the way I am used to working,\n\nI will try it (again) with another posting about the outdated font stuff (which doesn't work with current software) in the: \"startkde\" script.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "I don't know what one learnt at UCSB in the '70s (data base design?, mainframe programming?). But if you would have studied CS in Germany in the '70s or '80s then 95% of what you would have learnt would have been useless theory and at most 5% would have been useful practical experience. German CS graduates did know near nothing about programming but everything about the theory (aka hot air). That's why many companies prefered to hire mathematicians, electrical engineers, etc. instead of CS graduates because the former didn't just know how to give talks about programming they actually did write programs to solve their problems during their studies. In the meantime (in the last 5-10 years) this has changed in Germany.\n\nMy point is that a CS diploma doesn't mean that someone knows anything useful about programming. Especially if he studied CS back in the stone age of CS.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "HELLO: I studied Electronic Engineering & Computer Science (with emphasis on the first part) which pretty much negates most of what you said.\n\nAnd part of your premise is simply wrong.  A program is a program; it doesn't matter if it is written in ALGOL, BASIC [:-(], FORTRAN IV, FORTRAN 77, FORTRAN ??, K&R C, ANSI C, F, PL/Fiv, L, or languages many have never heard of.  To use your figure, 95% of it is just the same.\n\nYes, I know what punch cards are by actual experience, and I have used a Tek 104x graphics terminal.  Does that mean that I somehow know less?\n\nAlso, I learned OS/360 mostly by reading the manual.  This teaches an important skill that you probably don't get (and it isn't how to use OS/360).\n\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-26
    body: "Your opinion has been noted."
    author: "anon"
  - subject: "Re: development strategy"
    date: 2003-05-27
    body: "AFAICS (I don't know F and L) all the languages that you mentioned are procedural programming languages. But KDE is developed in C++ which is (or at least tries hard to be) object oriented. Developing OO programs is fundamentally different from developing programs in a procedural language (at least if one fully understood OOP). Knowing all about apples doesn't make you an expert for oranges.\n\nI'm not claiming that you know less about OOP than about procedural languages. But I wonder why you didn't mention any OOP language.\n\nBTW, I once learnt how to program an IBM 360/370 in assembler. So please stop making false claims about people you don't know.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: development strategy"
    date: 2003-05-27
    body: "Its good the hear that you know PL/Fiv. :-D\n\nAs an OOP skeptic: I point out that OOP is actually only a way of organizing the programs structure.  C++ is more of a quasi-OOP language -- C in an OOP wrapper.  The actual code is still written in a procedural language.  So it is more like apples vs. pears or with C++ and C, with different kinds of apples.  \n\nIf you read SIGPLAN notes, you will find that there is some really new stuff out there that is \"oranges\": :-)  FPLs and AOP.\n\nI told Datschge that I would stop responding to personal issues.\n\nBut, my congratulations on learning IBM 360/370 assembler."
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-27
    body: "You don't understand that Object-Oriented design is different from procedural design?\n\nYou don't understand that choice of language can have profound implications on productivity and design?\n\nYou think that Lisp is the same thing as C?"
    author: "anon"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "> You don't understand that Object-Oriented design is different from procedural\n> design?\n\nNo, what I understand is what I said; that it isn't as different as advocates of OOP say that it is.\n\nTell me, when you want to do something like:\n\n     y = m*x + b\n\ndo you do that differently in OOP?\n\n> You don't understand that choice of language can have profound implications on \n> productivity and design?\n\nI'm not certain that I see your point.  Yes, the choice of language can have profound implications.  That is why C is a always a poor choice.  However, no matter what language you choose to use, the program itself will be basically the same.  It will have a structure that appears quite different in the source code, but it will be much more the same than you might think.\n\n> You think that Lisp is the same thing as C?\n\nWell strictly speaking, Lisp is NOT a programing language (the same as BASIC isn't) but there are Lisp dialects which are compilable and meet the technical definition of a programing language.  If you use such a compilable Lisp dialect, then the exact same program could be written in Lisp or C.  And, I am certain that you will be shocked to learn that there are programs which will convert a Lisp program to a C program."
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "> Tell me, when you want to do something like:\n>\n> y = m*x + b\n> \n> do you do that differently in OOP?\n \nDepends. In a procedural language you can only write the above in case y, m, c, a^H^H^H^Hx, b are scalar variables. In C++ OTOH you can simply overload the = operator, the * operator and the + operator and so make this calculation/assignment work for virtually all types of objects for which this assignment makes sense (think of vectors and matrices). But operator overloading is in fact just a shorthand notation. In real OOP you would probably write this:\ny.copy( m.multiply(x).add(b) )\n\nBut what has this example to do with a development strategy? Development strategy isn't about the choice of the language, right?\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: development strategy"
    date: 2003-05-29
    body: "> But what has this example to do with a development strategy? \n\nAbsolutely nothing.  Very OT.\n\nMuch more relevant question is who are these anonymous trolls that ask such questions?\n\nYes, I know: I shouldn't answer them.\n\nBut since you said it, please note the exact one to one correspondence between:\n\ny.copy( m.multiply(x).add(b) )\n\ny = ( m*(x)+(b) )\n\nChanging the tokens does NOT change the syntax. :-)\n\nThere was a very good article on this in SIGPLAN Notices sometime in the last year or so, but I can't seem to find it at the moment, which showed with a short example the proper way to write OOP code and also the one to one correspondence to procedural code.\n\n> Development strategy isn't about the choice of the language, right?\n\nYes.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-28
    body: "Have you heard of Turing completeness?  What you say is *nothing* insightful.\n\nOf course you can turn C++ into C, but that is not the point of Object Oriented design.  Who the heck cares about \"y = m*x + b\"?  This is one single line of code and it is a mistake to look a single line of code and pretend that that says *anything* at all about Object-Oriented design vs procedural design.\n\nCompiling Lisp to C, oh geez.  Who cares?  Did you know you could compile Lisp to machine code too?  What's your point?  Focus, man."
    author: "anon"
  - subject: "Re: development strategy"
    date: 2003-05-29
    body: "I shouldn't reply to anonymous trolls, but:\n\nMy point is not about programing languages.  You represent my remark about those that, for some reason, think that the realization of a program in a certain language *is* the program.\n\nLike a Platonic form, the actual program has an existence separate from its realization.\n\nIt isn't about turning a C++ program into a C program.  It is about the fact that you can write the same program in different languages and it will be the same program.\n\nThe point with the \"single line\" is that with C++ when you get down to doing the actual work that it uses C (procedural) code.\n\n> Did you know you could compile Lisp to machine code too? \n\nIt depends on what you mean.  You can compile Common Lisp and Basic but they will NOT run without the run-time code.  This is why it is said that they are not really programing languages.\n\nOn the other hand, when you compile C, you get an assembler program which will run independently just the same as if you had written it in assembler.  And believe it or not, when you compile C++ it turns into a procedural program in assembler.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-29
    body: "Dude, you may have a name, but you're still a troll.  Just because I'm anonymous doesn't make me a troll.\n\nWho cares about your discussion?  I already told you about Turing completeness.  Your discussion about runtimes is a complete crock.  Have you ever heard of the C runtime?  Yep, it's called libc and it's on just about any machine you can find out there.  Your discussion about Lisp not being a programming language, whatever dude...  get serious.\n\nYou claim to be a design expert yet you don't admit that designs very much depend on the language paradigm you're using.   It's your words, not mine.  I'm not a troll because I point this stuff out to you."
    author: "anon"
  - subject: "Re: development strategy"
    date: 2003-05-30
    body: "> Just because I'm anonymous doesn't make me a troll\n\nOK, but are you anonymous because you don't know what you are talking about and don't want people to know who you are.\n\n> I already told you about Turing completeness\n\nYou have told me nothing about Turing completeness, only that you know that the term 'Turing completeness\" exists.  To an engineer (me) all this means is that a program is a FSM/FSA. \n\nIf you don't know how Common Lisp and Basic work, don't just display your ignorance, read up on it and you will then understand what I am saying.\n\nLibc is not the C runtime, it is a (statically) linkable library.  If compiled to machine code, both Basic and Common Lisp need a runtime program (NOT a linkable library) to work.  Can you statically link to Lisp or Basic?  LOOK IT UP!  Try using Google.\n\nLisp is NOT a programing language.  When you use Lisp, you write lists which are processed by the LISP processor.  You might call it a programing language, but Common Lisp can NOT be directly compiled into a static and linkable program.  When you \"compile\" Lisp, it is normally compiled into byte-code like java is.  This is still run through the interpreter.  Some Lisp implementations will compile it to machine code, but the machine code for Common Lisp still needs the runtime program to work.\n\nCan you prove that a LISP program *is* a FSM/FSA, that it meets the test of Turing completeness?  If not, what is your point of bring the concept up.\n\nWhat I have stated as a fact is the programs written in different languages are much more alike than the advocates of various languages claim they are.\n\nI can now see that it is true that there is no point to having gone to college and studied EE&CS as far as this crowd is concerned. :-)"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-29
    body: "> I shouldn't reply to anonymous trolls, but:\n\nThe anonymous people let their argument speak as compared to you who everytime needs to say \"I, professional computer expert, say...\"."
    author: "Anonymous"
  - subject: "Re: development strategy"
    date: 2003-05-30
    body: "Yes, and what \"anon\" said speaks for itself.\n\n(He shows this total ignorance).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: development strategy"
    date: 2003-05-30
    body: "OOOPPPSSS: Typo\n\nThat is 'ML' not 'L'.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "code conversion from c++ to c"
    date: 2005-04-09
    body: "i have some cod ewritten in c++ language and wish to get the code for the same in c language without recoding it in c . How do i do it?\n"
    author: "aditi"
  - subject: "Suggestion for paper topic"
    date: 2003-05-25
    body: "I realize that I would not possibly be qualified to write it but:\n\n\tThe 'arrogant developer' syndrome\n\nNotice in the other thread the implicit premise that a developer would never make any changes in his code based on the suggestions of others (especially mere users).\n\nTherefore: suggestions have no value.\n\n:-)"
    author: "James Richard Tyrer"
  - subject: "My suggestion for a research topic"
    date: 2003-05-26
    body: "Which suggestion is more likely to be realized:\n\nA) The plain suggestion and nothing else\nB) The suggestion and a lot of information about the person who suggested it\nC) The suggestion including use cases, involved problems and logical conclusions"
    author: "Datschge"
  - subject: "Re: My suggestion for a research topic"
    date: 2003-05-26
    body: "D) The suggestion that is thourghly discussed (and, therefore, improved)."
    author: "James Richard Tyrer"
  - subject: "Re: Suggestion for paper topic"
    date: 2003-05-30
    body: "This is the basis of the Open Source \"meritocracy\" and is at the foundation of the way that the community works.  Design suggestions just don't carry much weight without either patches or previously established credibility.  This isn't to say that users making requests as users are ignored, but users that want to make suggestions as a developer and yet not fill the role of a developer are ignored.\n\ni.e. \"It would be nice if there was a place to click to make this happen\" is fine.  \"Your code is poorly organized, you should be doing this, but I'm not willing to help\" will meet apathy.\n\nThere have also been some assumptions in what you've said that have been wrong:  you assume that KDE developers have not studied and are not professional programmers.  The majority are at least one of those and many are both (including myself).  The average KDE developer is probably mid-20's, university educated and with some professional experience.\n\nAs such, I'm more inclined to take suggestions from a KDE contributor that is a \"has an education and is a professional\" than someone who is not a KDE contributor.\n\nMy most common refrain within KDE / Open Source is \"We have no shortage of good ideas.\"  Really, KDE is developed by a small community of people with TODO lists growing faster than the things there can be completed.  Intrusive, developer-oriented suggestions that lack some more tangible willingness to help simply can't be accomedated.\n\nThere are some things about the KDE community that you simply won't pick up by reading kde-devel (i.e. that's not where decisions are made generally) and certainly apps.kde.com is not a good place to get a sampling of KDE's design and review culture.  Basically is looks like you're basing your analysis of KDE based on the periphery of KDE development rather than the core."
    author: "Scott Wheeler"
  - subject: "Re: Suggestion for paper topic"
    date: 2003-05-30
    body: "> This isn't to say that users making requests as users are ignored, but users \n> that want to make suggestions as a developer and yet not fill the role of a \n> developer are ignored.\n\nI'm not sure that I understand fully what you mean.\n\nI see an embedded premise (binary proposition) that makes little sense, but perhaps that isn't what you meant to say.\n\n> i.e. \"It would be nice if there was a place to click to make this happen\" is \n> fine. \"Your code is poorly organized, you should be doing this, but I'm not \n> willing to help\" will meet apathy.\n\nIs this meant as a binary proposition:  That input should either be naive suggestions from users with know knowledge of development or ready to apply patches from developers?\n\nBut that suggestions from a knowledgeable user are not acceptable.\n\nI note that I have made no comments at all about actual code.  And, I did not say that any code was poorly organized.  I have only commented on what the code does.\n\nAgain, I get back to Deming and the Japanese idea of constant improvement.  Is a statement that something can be improved to be taken negatively as a statement that there is something wrong with it?  If so, then this *is* a corporate culture issue.\n\nYour example: Does saying that the code could be *better* organized mean that it is *poorly* organized.\n\nAnd I am willing to help.  Some of my work *is* in the current release of KDE 3.1.x!\n\nTo use as an example the patch I submitted for the: \"startkde\" script:\n\nThe naive user would simply say that the fonts installed (as usr [i.e. in Basic Mode]) with the Control Center Font Installer don't seem to work.\n\nI know how the stuff works!  I know (almost) exactly what the problems are:\n  \nDoes this mean that my suggestions are:\n\n\t1.\tmore valuable\n\n\t2.\tless valuable\n\n\t3.\tworthless\n\nAn interesting question.  But perhaps I have completely misunderstood you.\n\nExample: I am willing to rewrite the font portion of the: \"startkde\" script, but I am not going to design it without discussing it with others.  AFAIK, the kde-devel list is the place to start.  \n\nI note from this example that I am seeing something that nobody has said: There is a clash here.  I went to engineering school, and even if (as some appear to think) I acquired no knowledge, I still learned something: a culture and I can see that it is quite different from the Open Source culture.  I *know* that I will do much better bouncing my ideas off other developers than I will discussing them with my cat.\n\n--\nJRT\n\n"
    author: "James Richard Tyrer"
  - subject: "Suggestion for paper topic <2>"
    date: 2003-06-01
    body: "\"works fine for me\"\n\nThe question is: is this a high enough standard for programing.\n\nI note that this was (as the hacker's credo) strongly criticized in that article from SIGPLAN Notices (that I still can't find).\n\nThe answer is NO -- unless you want Microsoft like code -- a program should be canonically correct and complete.\n\nI offer a totally unproven theory:\n\n1.\tIf software has mysterious problems that you can't find.\n\n2.\tIf software has various things that are not correct but \"WFFM\"\n\nThere is a possibility that there is a correlation between #1 & #2.\n\nI note that Microsoft is my model for the 'hacker culture'.  Others may have different definitions but that is mine.\n\nMicrosoft's software (DOS & Windows) was designed by hackers that had no knowledge of of CS or Intel documentation.  That is why it is such a mess (and yes I know and can discuss the details if anyone is interested).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Suggestion for paper topic <2>"
    date: 2003-06-02
    body: "Unlike Microsoft, KDE can not manage developer resources. Either somebody implements it, or he doesn't. Either it will be accepted into the CVS, or not. Either distributions ship it, or they don't.\n\nAnd, BTW, I would be pretty happy to have the breadth of technology available in KDE/Qt compatible way that MS offers for Win32/MFC or for .Net. As long as it is working... Free software does not neccessarily be better than the closed alternative, as long as it is free. It just needs to be good enough. \n\nBetter software is better, obviously, but first it is important to reach a competitive level. Free software has enough licensing advantages that a functional equivalent would be a very serious competitor to any closed product, even if it is better known.\n\n\n\n\n"
    author: "AC"
  - subject: "Re: Suggestion for paper topic"
    date: 2003-06-02
    body: "Sometimes WFM is just a shorthand for \"damn - I can't reproduce that bug, and until I do,\nfixing that bug will be much harder than if I *could* reproduce it.  In fact, I really wish I could\nreproduce it, as that would make my life much easier.  I *want* to fix the bug, but that's gonna \nbe pretty hard to do.  In the meantime, could you be ready to work with me to see if my changes\nfix it for you?\""
    author: "TomL"
---
The KDE Developers' Conference is a meeting of KDE contributors from
all over the world. It will feature three days of technical talks and
tutorials. Do you have a particular expertise related to KDE programming that
could be useful for your fellow developers? Do you want to present a
particular programming pattern, a tool, a development strategy, or
anything else that helps KDE developers become more productive? Then
consider talking about it or giving a tutorial at the KDE Developers'
Conference.
<!--break-->
<h2>KDE Developers' Conference - Call for Papers</h2>
<h3>August 23th to 25th 
Zamek, Nove Hrady, Czech Republic</h3>


<h3>Conference Program</h3>
<p>
The KDE Developers' Conference is a meeting of KDE contributors from
all over the world. It will feature three days of technical talks and
tutorials. Following the conference there will be some - albeit
limited - opportunity for interested groups to gather together in
computer labs for a hacking session until the 31st of August.
Besides, there will be plenty of time for socializing.
</p>
<p>
More information is available at <a href="http://events.kde.org/info/kastle">http://events.kde.org/info/kastle</a>
</p>
<p>
Do you have a particular expertise related to KDE programming that
could be useful for your fellow developers? Do you want to present a
particular programming pattern, a tool, a development strategy, or
anything else that helps KDE developers become more productive? Then
consider talking about it or giving a tutorial at the KDE Developers'
Conference.
</p>
<p>
We solicit submissions for presentations and tutorials from, but not
restricted to the following fields. Everything KDE-related will be
considered.
</p>
<ul>
<li>KDE and Qt programming tricks
<li>DCOP, KParts, KOffice, multimedia etc. development
<li>programming tools, patterns
<li>programming patterns and development strategies
<li>project management issues
<li>internationalization
<li>documentation
<li>usability, accessibility, interoperability
</ul>
<p>
Talks will be scheduled for 45 minutes talk and 15 minutes Q&A. In
some cases, it may be possible to occupy two slots for a total talk
time of 90 minutes or offer short presentations of 15 minutes
each. Please note the estimated talking time in your application.
</p>
<p>
Finally, BoF sessions can be announced in advance or planned on the
spot. A limited number of group rooms will be available for BoFs.
</p>

<h3>Submission Guidelines</h3>
<p>
Those proposing to present papers should submit an abstract including
the name and e-mail address of the author or authors to
<a href="mailto:nove-hrady-committee@kde.org">nove-hrady-committee@kde.org</a>. The conference language is English. The
abstracts will be reviewed by the program committee based on content,
presentation and suitability for the event. By submitting an abstract
you give permission to publish it on the conference web site.
</p>

<h3>Location</h3>
<p>
The KDE Developers' Conference will take place at the Zamek (&quot;Castle&quot;)
in Nove Hrady in the southern part of the Czech Republic in Central
Europe. Facilities are hosted and sponsored by the Institute of
Physical Biology of the University of South Bohemia. Nove Hrady cannot
easily be reached by public transportations, but a shuttle service
will be provided from Prague-Ruzyne airport and Prague Central
Station.
</p>

<h3>Speaker Incentives and Financial Compensation</h3>
<p>
The KDE Developers' Conference is run by KDE e.V., the Academic and
University Center Nove Hrady and the Polytechnic University of Upper
Austria in Hagenberg. KDE e.V. is a non-profit organization of
volunteering professionals and enthusiasts of free and open-source
software.
</p>
<p>
We only have a limited budget for this conference, and cannot
generally pay travel expenses or any other reimbursement or
gratification to speakers. We can provide accommodation and boarding
at a very low price (expect about 15-20 Euros per day for full board
and lodging, but don't expect luxury accommodation), and the shuttle
service from and to Prague will be free of charge.
</p>
<p>
KDE e.V. is soliciting donations from companies and individuals
towards this conference. These donations will be used both for running
the conference and for bursaries for delegates from other continents
and/or without income. Delegates with an accepted paper will be
prioritized in the distribution of these bursaries.
</p>
<p>
Please note that even if we can grant you a bursary, you will still be
expected to pay a part of trip yourself, as well as your boarding and
lodging.
</p>

<h3>Important Dates</h3>
<p>
Please submit your contributions no later than the indicated dates:
</p>
<p>
Abstract submission deadline:
May 31, 2003
<br>
Acceptance notification:
June 16, 2003
<br>
Final papers due:
July 16, 2003
<br>
all 23:59 UTC
</p>

<br>
For the Program Committee
<br>
<br>
Matthias Kalle Dalheimer
President, KDE e.V.