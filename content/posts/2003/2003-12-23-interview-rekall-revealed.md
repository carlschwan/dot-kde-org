---
title: "Interview: Rekall Revealed"
date:    2003-12-23
authors:
  - "fmous"
slug:    interview-rekall-revealed
comments:
  - subject: "Absolutely right"
    date: 2003-12-23
    body: "Shawn Gordon: It will either play a pivatol role or no role at all.\n>>>>>>>>>>>>>>>>>>\nI think he is absolutely right about this. The commercial market appears to be trying to force a standard, and KDE is definately at a crucial period right now."
    author: "Rayiner Hashem"
  - subject: "Re: Absolutely right"
    date: 2003-12-23
    body: "I disagree, it's saying like either big M's shares drop to a minority from one day to another or no other player will ever play an important role in the market. Even though there are way too many people preferring to see a \"decisive battle\" style \"dead or alive\" market, neither one is true. =P"
    author: "Datschge"
  - subject: "Re: Absolutely right"
    date: 2003-12-25
    body: "I also disagree. Who knows how the US IT software industry will look like in 10 years ? Is Sun, Novell, HP/Compaq still be there ? At least, I wont buy shares in any of these three companies. I think Americans are a bit over-obsessed by 'big players'.\n\nIt seems that these \"big-players\" are all trying to go the IBM way with an emphasis on consultancy and a disinvesting in ingeneering and R&D. Opensource is very good for them because they can sell products they have not bothered to create.\n\nIf you are a consultant, you try to guess what your client wants and then pretends you are a specialist of it. Novell should better keep one foot in the Gnome camp and another one in the KDE camp and invest the minimum in both projects.\n\nThe big challenge for Novell is not to choose between Gnome and KDE, it is to not destroy the expensive German toy they have just bought. They were a very American red-neck company and now they must make the most of some strange Old Europeans engineers, speaking a bizarre language and obsessed by their Dr. titles.\n\nThe cultural rift between the US and Europe has increased (especially last year) and it is not going to be an easy task to manage a company split between the two continents.\n\nBut that is Novell's problem. For KDE, if an American IT company chooses an inferior desktop. Too bad for them ! What is important for us is that our community strives, that we produce more code, more documentation, more translations. That we keep a good spirit and have pleasure working together.  2004 was a great year and I see no crisis looming for KDE.\n\nIt would be great if IBM hires 20 KDE fellows to work full-time on our beloved project. But if it does not happen, we will continue to move forward (albeit slower)."
    author: "Charles de Miramon"
  - subject: "Re: Absolutely right"
    date: 2003-12-23
    body: "If the corporate world decides to increasingly adopt Gnome it won't be because Gnome is better or that Gnome has a formal entity behind it. It will simply be because in order to develop non-free Qt applications you must pay a per-developer license to Trolltech and this becomes a huge expense for a large company. I wouldn't call it an unreasonable expense, but companies like Novell are moving toward Linux primarily because of the cost factor and like most for-profit companies they have their eyes focused on the bill of materials.\n\nIt still shocks me that the Gnome I used in January 2002 is still much the same, with its maddening shortchomings, while KDE has undergone major covert and overt improvements and continues to do so. Exactly what then is the value of all this corporate backing if they can't get better revisions of Gnome out the door half as fast as the KDE team can?\n\nOne unintended consequence of corporate Gnome development is the alienation of the majority of Linux desktop enthusiasts who more often than not prefer KDE. As for me, I've been running Linux on my desktop since 2000 but if faced with only two options, to use Gnome or to return to Windows XP..... I will return to Windows XP in a heartbeat. There are just too many productivity roadblocks with Gnome. I wonder how many IT folks feel the same as me.\n\nSean."
    author: "Sean Pecor"
  - subject: "Re: Absolutely right"
    date: 2003-12-23
    body: "What's interesting is that companies have *not* been moving to GNOME because of licensing issues. RedHat is a GNOME distro because many of its employees are GNOME developers. Sun was never really clear why they went with GNOME, but indicated that one reason was that its developers were simply more comfortable with C. And if Novell does go KDE (which is not a certainty at this point) it will be because of the Ximian acquisition. \n\nTo date, no corporation has really come forth and stated that it picked GTK+ over Qt because of the licensing issue. So it would be short-sighted of the Linux companies to push GNOME over KDE because of the licensing issue, when they don't even know if corporations care that much."
    author: "Rayiner Hashem"
  - subject: "Re: Absolutely right"
    date: 2003-12-24
    body: "Interestingly enough Gordon mentions HP having signed a contract with Ximian for using GNOME while HP next to their usual dominating Windows offerts also offers Mandrake (which is not GNOME based afaik) at the moment (see eg. http://tinyurl.com/2pcru). Red Hat supported GNOME from the very beginning due to the licensing issues back when GNOME was started due to political reasons, which are also the reason for the many GNOME developers they have (not bad since they cooperate through freedesktop.org). Sun went with GNOME due to it being more popular in the US and supposedly easier to connect with other languages like Java (also not bad since Sun's major contribution is the accessibility framework of which Qt/KDE can make use as well). As for the question what will happen to the Novell, Ximian, SuSE triangle I don't expect big changes to what Ximian and SuSE did as independent companies before: Ximian will keep trying to sell desktop solutions around Evolution, Red Carpet and Mono and add Novell products to it (Ximian could try to push its Mono project into GNOME, but it seems not to be very welcome there, also according to their Mono FAQ Ximian seem to do the same as Trolltech \"Mono components are also available to be licensed commercially\"), and SuSE will continue offering its KDE based server and desktop solutions and also add Novell products to them (SuSE offers both KDE and GNOME for a long time already, there is no reason for this to change, rather there's now the possibility for them to outsource the job of polishing GNOME in SuSE to Ximian instead doing that work twice). Novell already stated they want to work with the \"whole community\", explicitely mentioning both GNOME and KDE (this might result in a push on Ximian to cooperate more through freedesktop.org than they did until now, which wouldn't be bad either).\n\nIndeed, so far no corporation has really come forth and stated that it picked GTK+ over Qt because of the licensing issue."
    author: "Datschge"
  - subject: "Re: Absolutely right"
    date: 2003-12-26
    body: "> It will simply be because in order to develop non-free Qt applications you must pay a per-developer license to Trolltech and this becomes a huge expense for a large company.\n\nPlease explain to me how a license which, for one year, amount to less than half of what one programmer costs each month, be considered a \"huge expense\".\n\nLicenses for programming tools (specialized libraries or debugging tools like Purify, etc...) are generally 10 times the cost of Qt. Qt is *cheap*."
    author: "Guillaume Laurent"
  - subject: "Rumours"
    date: 2003-12-23
    body: "What are the bizarro rumours floating around? :-)"
    author: "AC"
  - subject: "Re: Rumours"
    date: 2003-12-27
    body: "I believe he is refering to a number of posts which appeared on comp.lang.python, www.zaurus.com, etc which stated that theKompany.com is verging on becoming bankrupt"
    author: "Moonchild"
  - subject: "Re: Rumours"
    date: 2003-12-28
    body: "No John, I'm referring to comments that get maid whenever a question gets answered.  I'll try to explain this but I'm not sure if it will come across clearly.  Say an interviewer asks me something like \"Where do you see KDE in 5 years\" and I say \"Probably the 2nd most popular desktop and we hope to be part of that growth\" then you'll see posts along the lines of \"TKC is trying to take over KDE like Ximian did with Gnome\" and other such conspiracy theories.  The other thing is when I don't answer every permutation that is possible to a question, then people say that I'm being evasive when in fact I answered it with all the information that I had available.  Another one is that some people assume we've planned out every possible future scenario in advance with a calendar that we are working off of that will tell us when we will do each thing.  We don't work like that, and I don't know anybody that does, but it doesn't stop the comments.  This would be something like \"When are you going to do a 3.0 release of tkcJabber, what will be in it and what will the upgrade price be?\", well we just haven't thought about it yet, we've got some ideas for some upgrades and aren't sure if they would be a 3.0 or 2.x or if it will be free or not.  This is just an example, but it's a common one.  So no John, it doesn't have to do with your misleading posts on those other forums."
    author: "Shawn Gordon"
  - subject: "KSQLPlus"
    date: 2003-12-23
    body: "What happened to KSQLPlus? Is there a qt3 version?\n\nWhat about TOra?\n\nWhat do you think about the Hbasic data base modules?"
    author: "Gerd "
  - subject: "GIMP UI"
    date: 2003-12-23
    body: "I prefer the freedom to choose.\nhttp://www.kdevelop.org/index.html?filename=screenshots1.html"
    author: "martin"
  - subject: "Gnome and KDE"
    date: 2003-12-23
    body: "\"The best I've seen is MR Project, but that is GTK, so I don't use it.\"\n\nIs it that hard to put aside gnome/kde preference and use a decent open-source project? Everyone has a preference but this remark seems rather dismissive and a litle petty. I appreciate that developers' toolkit preferences are based on more than just language but it would be nice to see a little more respect amongst developers of GPL/LGPL software.\n\n\"why can't we all just get along!\" :-)"
    author: "A user"
  - subject: "Re: Gnome and KDE"
    date: 2003-12-23
    body: "> \"The best I've seen is MR Project, but that is GTK, so I don't use it.\"\n \nKde's project management program can be found at the following url:\nhttp://www.koffice.org/kplato/\n\nIt is still in its infancy, and need someone that really wants to work\non it on a regular basis.  The fundament is there however.\n"
    author: "Richard"
  - subject: "Re: Gnome and KDE"
    date: 2003-12-23
    body: "Don't use because of gtk, sound really silly. \nMR Project, a KDEfication candidate?"
    author: "martin"
  - subject: "Re: Gnome and KDE"
    date: 2003-12-23
    body: "I tend to avoid GTK+ software unless I have no other choice (ex. gtkpod). Why? Because I've got a nice, integrated KDE desktop, and GTK+ apps stick out. Its an asthetic choice more than anything else."
    author: "Rayiner Hashem"
  - subject: "Re: Gnome and KDE"
    date: 2003-12-24
    body: "Isn't TaskJuggler a more sophisticated project manager than MrProject/Planner? It's just missing a KDE input interface."
    author: "Anonymous"
  - subject: "Fabrice"
    date: 2003-12-23
    body: "> So the Dot editors decided to contact the two parties who are involved on this matter\n\nSuffice it to say that since Fabrice joined the dot-editors teams, things have gotten so much more active here.  :-)\n\nFabrice is doing an impressive amount of work for the dot and otherwise -- all apart from his normal KDE translation work.\n\nThanks a lot, Fabrice!"
    author: "Navindra Umanee"
  - subject: "Re: Fabrice"
    date: 2003-12-23
    body: "FULL ACK!!! The last week (or two?) every day comes one or two news/stories. It is really impressive. KDE looks much more active now.\n"
    author: "Marc Tespe"
  - subject: "Re: Fabrice"
    date: 2003-12-23
    body: "but this is done by several people and anyone can do this. "
    author: "Fabrice Mous"
  - subject: "Re: Fabrice"
    date: 2003-12-24
    body: "Now you are mention it, yes\nthe dot has come more active.\n\nOne thing I like a lot is that\nthe articles, interviews,\ndigest, etc are very high\nquality. So, you and all the\ncontributers have my warm\nthanks!"
    author: "pieter"
  - subject: "Re: Fabrice"
    date: 2003-12-24
    body: "I want to second this. Fabrice makes an impact - without coding."
    author: "Anonymous"
  - subject: "KDE entity to negotiate"
    date: 2003-12-24
    body: "[...]It does show that as long as there isn't a KDE entity to negotiate contracts with, then there isn't going to be a mass adoption.\n\nSo... should we start it ? do we start it ? who is on board ? hehehe...\nThinking out loud...\nIf Ximian is doing well from a bussiness/economical point of view, wouldn't a company that makes the same 'servicies' arround Ximian be successfully as well (maybe more since we already know that KDE is better than Gnome ? ;)\nDo companies really choose Gnome because of Ximian ? or companies choose Ximian because of Gnome ? Would companies choose KDE because of 'the KDE-Ximian-like-company' ? Would companies choose 'the KDE-Ximian-like-company' because of KDE ?\nDoes the fact that Qt is only GPL and not LGPL impose a problem for making 'the KDE-Ximian-like-company' ?\nWould starting the KDE-Ximian-like-company out of an already KDE Company like KDE e.V. or theKompany be usefull ?\nDoes anyone have a thought about it ? Does anyone is thinking seriously about this ? (because I am thinking seriously).\nThanks"
    author: "Pupeno"
  - subject: "Re: KDE entity to negotiate"
    date: 2003-12-25
    body: "I don't know how the business model of Ximian worked. However there were several companys that worked on Kontact."
    author: "Bert"
  - subject: "Re: KDE entity to negotiate"
    date: 2003-12-25
    body: "Learn from \"professioals\":\n\nhttp://money.cnn.com/2003/12/24/technology/techinvestor/hellweg/?cnn=yes\n\"Prognosticators -- including me at times -- are proclaiming that we're seeing the beginning of Internet 2.0, when some of the promise [..] will finally happen. Quite a few tech stocks, such as [..] CNET, and SCO Group (SCOX: Research, Estimates), are up several fold this year (SCO, for example, is up from a buck and change to nearly $20). [..] Remember when all a company had to do to see its stock price pop was issue a press release containing the words \"Linux\" or \"B2B\"? \"\n\nHaha."
    author: "Hein"
  - subject: "Re: KDE entity to negotiate"
    date: 2003-12-27
    body: "You can forget theKompany.com. \n1. They don't have any money\n2. They are unable to sale their own products let alone KDE\n3. TKC is all talk and no action.\n4. TKC lacks motivation, direction, vision and drive.\n\nI would not trust the commercialization of KDE to TKC especially when you consider the fact that it took them 3 years to take BlackAdder RC 1 to V 1.0"
    author: "Moonchild"
  - subject: "Re: KDE entity to negotiate"
    date: 2003-12-28
    body: "Hi John, no need to hide behind an alter-ego there.\n\n1.  We have money\n2.  We sell products every day, we have about 40 products and they all sell pretty well thanks very much\n3.  No idea where that comes from\n4.  That's a really silly point considering how much we've done on so many platforms within such a short amount of time and with no outside investors.  We've been creative, quick and innovative.\n\nWe don't want to commercialize KDE, but what that has to do with BlackAdder is just odd.  If you know anything about BlackAdder then you know that the RC was essentially complete other than a few minor issues, we extended the RC to do a rewrite of the IDE so that the updates would continue to be free for the customers, we could have called it 1.0 long before the time that we did."
    author: "Shawn Gordon"
  - subject: "what rekall also needs"
    date: 2003-12-25
    body: "is better documentation IMO. I tried to use it, but the api documentation was (if i recall correctly) a bunch of class descriptions. Thats very nice, but (maybe i skipped it) there is not a lot information on how you can get a reference to an object / how everything fits together (like i'd want to popup another form from a script). Maybe i didn't read the docs well (did i ?), maybe i did and then i think better docs are needed ..."
    author: "ik"
  - subject: "Re: what rekall also needs"
    date: 2003-12-25
    body: "Well, the class definitions is the last part of the manual (appending something). Did you read the hundred or so pages (counted in the PDF version) first?\n\n\nMike\n"
    author: "mike ta"
---
Some time ago there was an <A href="http://dot.kde.org/1069090495/">announcement</A> on the Dot about <A href="http://www.thekompany.com/press_media/full.php3?Id=242">the GPL'ing</a> of <a href="http://www.thekompany.com/products/rekall/">Rekall</a>. So the Dot editors decided to contact the two parties who are involved on this matter: Mike Richardson and Shawn Gordon. We compiled a nice interview for your reading pleasure.






<!--break-->
<p><strong>Fabrice Mous: </strong><em>How many employees does <A href="http://www.thekompany.com">theKompany.Com</A> have?</em></p>
<p><strong>Shawn Gordon: </strong>About 15</p>

<p><strong>Fabrice Mous: </strong><em>How many applications does theKompany.Com have in her portfolio?</em></p>
<p><strong>Shawn Gordon: </strong>About 40</p>

<p><strong>Fabrice Mous: </strong><em>What are the typical customers of theKompany.Com?</em></p>
<p><strong>Shawn Gordon: </strong>That's a tough one to answer, since we don't have a sales staff that goes after customers,
and sell in stores and online, we've got people that just pick up our software, we aren't
even sure what platform sells the most because you get all platforms for one price.  We could
probably back in to the numbers by counting downloads on the particular files, but our key is
to keep the cost of customer acquisition low.  We know we've got a good number of corporate
customers just from signatures on emails, but because we have such a diverse software offering,
there isn't a 'typical' customer to point to, basically anyone that owns any computer can be
our customer.</p>


<p><strong>Fabrice Mous: </strong><em>How do customers typically get support for their (Linux) installs?</em></p>
<p><strong>Shawn Gordon: </strong>We have an online tracking system, we use "Mantis" for it, in addition we have some PHPBB forums
as well as product specific mail lists.  The vast majority of people don't have problems with installs,
but some do.  We finally perfected a method of distributing apps that doesn't require that we re-build
it for every distro/version, although with Rekall we still do that for the <A href="http://www.kde.org/">KDE</A>
specific versions, this allows Rekall to run on the default configuration for all the latest distros and KDE.</p>

<p><strong>Fabrice Mous: </strong><em>theKompany.Com started with a focus on pure KDE software. More recently the focus seems to have
broadened to include software for MS Windows and <A href="http://www.thekompany.com/embedded/">handhelds</A> as well. What motivated this change?</em></p>
<p><strong>Shawn Gordon: </strong>I wouldn't say recently, we changed our focus years ago.  There were two deciding factors, and to be honest one
of them was our frustration in working on Kivio as a component of <A href="http://www.koffice.org/">KOffice</A>.
The communication and control that should have been there, wasn't, and our developers that were involved with it basically refused to deal with it
any more.  I mention this not to open old wounds, but to illustrate that the interactions between the open
source community and businesses could and should be smoother, there is a lesson for us all in that, and we hope
that we learned enough to make the GPL'ing of Rekall successful and avoid the same frustrations.</p>

<p>At the same time we were talking to distributors about our first multi-platform product BlackAdder,
and we found that while there was little interest in a Linux application, there was a huge amount of interest
in an application that ran on both Linux and MS Windows in one box for one price.  Under normal circumstances I
probably wouldn't have gone to the effort of converting to a pure Qt base, but the combination of factors led
us to it.  We've also added Mac OS X support for a number of products (with more to come), and as you mentioned
handhelds, which is an interesting story.</p>

<p>We were actually approached by <A href="http://www.lineo.com/">Lineo</A> about 4 months before
the <A href="http://www.zaurus.com/">Zaurus</A> was announced to give them a back up plan for their proposal
to a "big company" that they were trying to convince to go with Linux instead of MS Windows on a new PDA, at
the time we didn't know it was <A href="http://sharp-world.com">Sharp</A>,
but we did the work.  A few months later while I was CEO for <A href="http://en.hancom.com/index.html">Hancom</A>
in the US in addition to my duties as CEO of theKompany.Com, Sharp came to us about having
<A href="http://desktop.mobile.hancom.com/product/img/mobile_word.gif">Hancom Office</A> on the device,
what Sharp US and Hancom US didn't know was that Sharp Japan and Hancom Korea
had already been talking (the joys of multi-nationals), so I went off and wanted my guys to port our Jabber
client to the device and Kivio.  Turned out Kivio was flat impossible because of how much Qt3 was being used,
but we got Jabber and several other applications written for the device within the first month and were able
to show them off to the Sharp executives at the big developer conference they did in November 2001.  I think
by the time the device was officially released we had about 15 applications available for it, we're up to 30
now with a handful having desktop counterparts such as Rekall/tkcRekall, tkJabber/tkcJabber,
tkWhiteboard/tkcWhiteboard, tkRadio/tkcRadio, tkCard/tkcCard.</p>

<p>I might as well explain the naming convention since that question always comes up.  We use to agonize over product
names, so on the Zaurus we decided to just use plain names and preface them with 'tkc' for 'The Kompany.Com',
the C can also stand for 'Compact'. Anything with a desktop counterpart we just take the 'C' off.</p>

<p><strong>Fabrice Mous: </strong><em>Novell acquired both <A href="http://www.ximian.com/">Ximian</A>
and <A href="http://www.suse.de/en/">SUSE</A> this year. Your company has considerable experience with the
Linux deskop market, what advice can you give <A href="http://www.novell.com/">Novell</A> for them to be
successful in that market?</em></p>

<p><strong>Shawn Gordon: </strong>Wow, this is a big question.  I've been watching Novell's acquisitions
with extreme interest.Without trying to step on any toes, if I was Novell, I'd listen to SUSE on this as much as
possible. I'm still scratching my head on the Ximian acquisition, however the word is that Miguel is in charge of
Novells Linux desktop strategy.  I think this is a mistake for the simple reason that Miguel doesn't come to
it with an unbiased opinion, he's going to say Gnome is best and never do a fair and honest evaluation about
what is best for Novells business model, at least this is my assumption.  If that is the case, then I think
this is a real problem for KDE, in terms of main stream distributions that just leaves Mandrake, and I don't
know if they are out of bankruptcy yet.  Fortunately you have companies like <A href="http://www.xandros.com/">Xandros</A> and
<A href="http://www.lindows.com/">Lindows</A> also focused on KDE, but I don't know what kind of market
penetration they have yet.</p>

<p><strong>Fabrice Mous: </strong><em>How does KDE help you run your business?</em></p>
<p><strong>Shawn Gordon: </strong>KDE doesn't really run our business, it's a part of doing business in that it is the desktop and tools that
we use on Linux.  We also have to work with MS Windows and Mac OS X, so we have lots of computers around the
office and tend to just use the best tool for the job, or the one that most applies to the work being done.
People who work on the MS Windows ports rarely work on Linux and the same is true in reverse.</p>


<p><strong>Fabrice Mous: </strong><em>There is widespread sentiment that Linux will make a big breakthrough on
  the business desktop in 2004, what role do you see KDE playing in that?</em></p>

<p><strong>Shawn Gordon: </strong>It will either play a pivatol role or no role at all.  I raised the red flag in a prior response
about what Novell might be doing, and this has been a perpetual issue since Sun and HP signed contracts
with Ximian to use Gnome.  Whether those contracts were successful or not, I don't know, but it does show
that as long as there isn't a KDE entity to negotiate contracts with, then there isn't going to be a
mass adoption.  There is a story I read just a few weeks ago that the feather in the KDE cap, the
City of Largo Florida is now going to Gnome from KDE.  I don't know why this hasn't been talked about
more, but does anyone know why this happened?
[Editor: <a href="http://www.newsforge.com/trends/03/11/25/1723233.shtml">The City of Largo made a statement on this topic in the meantime</a>]
This is a rather major blow, and I'm afraid that if
there isn't some corporate backers that are promoting KDE on a big level, it's not going to be relevant.
Compare MS Windows versus OS/2 as a good recent example of something similar, but in this case you had a
huge company behind OS/2, just not doing a good job getting it to the user.  I think this is a serious
potential problem for KDE, and I really hope that SUSE has some say on the Novell desktop strategy and
that their say is to use KDE because that could make all the difference in the world at this critical juncture.</p>



<!-- Rekall specific questions -->
<br>
<p><strong>Fabrice Mous: </strong><em>Why did theKompany.Com release Rekall under a GPL licence?</em></p>
<p><strong>Shawn Gordon: </strong>This is something we talked about from the earliest days, there were actually some GPL versions of
the pre-1.0 code a long long time ago.  Originally we were trying to find a way that made sense to have
it as a KOffice component, then later we were talking to MySQL about having a dual licensed app similar to
their database, the problem at both those times was that we didn't have any follow on revenue to justify
spending the resources on it.  Then we got in to a series of partnership deals, such as the Hancom one
and a couple that never materialized that I can't talk about that made that a non-option.  During the
intervening time we came up with a number of ways to make money and still release it GPL, we had learned
some things from our Aethera release with commercial plug ins and free core code.  We *know* that an MS Access
like application is hugely interesting in the enterprise especially, and for application developers
making database centric products, so we wanted to be able to get Rekall in to the largest number of
hands we could.  This would get more people using it, more people contributing and trying it, get things
done to it that we haven't thought of.  It's a bit of a risk, but we've always lived dangerously, so what the heck.</p>

<p><strong>Fabrice Mous: </strong><em>What's the focus of this project?</em></p>
<p><strong>Mike Richardson: </strong>Developing a database front end (not a database!) for Linux which is
comparable in functionality to Access. Actually, don't take that too
literally, there is a lot of nasty stuff in Access, but it is probably the
standard by which a lot of people will judge.</p>

<p>Also, I want to make sure that Rekall can run on MS Windows; not that I have any
love of MS Windows, but making Rekall a cross-platform application means that it
is easier for people to try it on existing MS Windows systems, and later be able
to migrate to Linux without having to reimplement a load of stuff.</p>

<p><strong>What changes will the GPL-ing of Rekall bring? Are commercial versions going to be stopped now?</em></p>
<p><strong>Shawn Gordon: </strong>Think of it as being very similar to the dual license of Qt.  The commercial development will continue,
additional features, functions and embedding will also happen.  The primary change we want to see is to get
even more people using Rekall.
</p>

<p><strong>Fabrice Mous: </strong><em>If I were a developer to which website should I resort? There is some confusion about that.</strong><br/>
  <ul>
   <li><strong><A href="http://www.totalrekall.co.uk/">www.totalrekall.co.uk</A></strong></li>
   <li><strong><A href="http://www.rekallrevealed.org">www.rekallrevealed.org</A></strong></li>
   <li><strong><A href="http://www.rekall.a-i-s.co.uk/">www.rekall.a-i-s.co.uk</A></strong></li>
   <li><strong><A href="http://www.thekompany.com/home/">www.thekompany.com</A></strong></li>
  </ul>
 </p>

<p><strong>Mike Richardson: </strong>First of, www.rekall.a-i-s.co.uk and www.rekallrevealed.org are one and the
same site, its just that I didn't get around to getting the
rekallrevealed.org domain until we were going GPL! There is even a third URL
that gets you to the same place ....</p>

<p>If you are a developer, then <A href="http://www.rekallrevealed.org/">rekallrevealed</A> is the site to go
for, plus the mail lists, the usual sort of set: <a href="mailto:rekall(at)rekallrevealed(dot)org">rekall</a>(<a href="mailto:rekall-join(at)rekallrevealed(dot)org">subscribe</a>),
<a href="mailto:rekall-announce(at)rekallrevealed(dot)org">rekall-announce</a>(<a href="mailto:rekall-announce-join(at)rekallrevealed(dot)org">subscribe</a>)
and <a href="mailto:rekall-devel(at)rekallrevealed(dot)org">rekall-devel</a>(<a href="mailto:rekall-devel-join(at)rekallrevealed(dot)org">subscribe</a>).
totalrekall.co.uk and thekompany.com are commercial sites offering packages
and services around Rekall.
</p>




<p><strong>Fabrice Mous: </strong><em>How many developers are working on Rekall now?</em></p>

<p><strong>Mike Richardson: </strong>At the moment, its mostly me. Roll up, roll up .... all support welcomed. I
can think of several things in my todo list which could be taken on without
having to be familiar with all the 130K lines of code (and 40K+ lines of comments:)</p>




<p><strong>Fabrice Mous: </strong><em>It has been some time now since the <A href="http://dot.kde.org/1069090495/">announcement</A>
of the GPL-ing of Rekall, has the project been able to attract developers to help out?</em></p>

<p><strong>Mike Richardson: </strong>Some people have been working on getting Rekall to build on more
distributions. We never did any builds on Debian in the past, so that will
improve the coverage, and someone is building it for Mandrake Cooker. Just at
the monent this is pretty useful.</p>


<p><strong>Fabrice Mous: </strong><em>What does the project need most right now?</em></p>
<p><strong>Mike Richardson: </strong>One really big thing is people to test the code, because a big
problem with an application like Rekall which has scripting support is that the number of interactions between
the components is so large. This makes testing a real hassle.</p>


<p><strong>Fabrice Mous: </strong><em>Will a MS Windows version appear as GPL? </em></p>
<p><strong>Mike Richardson: </strong>If and when <A href="http://www.trolltech.com/">TrollTech</A> release a GPL version of QT3 for MS Windows, then Rekall
for MS Windows can be released GPL. Of course, there is nothing to stop anyone
who has a copy of QT3 for MS Windows from downloading the source and building
it, though they would have to fugure out the build files.</p>
<p><strong>Shawn Gordon: </strong>Not for the time being, has to do with other licenses.</p>


<p><strong>Fabrice Mous: </strong><em>Is there any chance that some Rekall libraries will be released under the terms of LGPL?</em></p>
<p><strong>Mike Richardson: </strong>The real problem would be, which bits? The obvious library would be the driver
library, but the driver code is rather specific to Rekall (it has stuff in it
to try to give as much RDBMS independance as possible). But, ask and all will
be considered!</p>
<p><strong>Shawn Gordon: </strong>Anything is possible, but there are no current plans.</p>

<p><strong>Fabrice Mous: </strong><em>Can you describe the organization that are using Rekall? How does Rekall fit in their Linux strategy?</em></p>
<p><strong>Shawn Gordon: </strong>We've got a wide variety of people using Rekall, but remember, because this is a shrink wrap product,
we don't really know what everyone is doing, we don't necessarily interface with even a small percentage
of the customers.  I do know of one customer right now that is doing a Point of Sale system for a bible
college in it, which is really very cool.  We've got some case studies that we are going to start putting
up starting next month, so we should see some interesting stuff there.</p>


<p><strong>Fabrice Mous: </strong><em>How does Rekall compare to other Database applications like <A href="http://www.knoda.org/">Knoda</A>
and <A href="http://www.kexi-project.org/about.html">Kexi</A>?
Are there any differences?</em></p>
<p><strong>Mike Richardson: </strong>I've not tried running Knoda for a long time; the last time I tried I couldn't
get it to build (though I expect that was my fault:), so I can't really
comment. The biggest difference to Kexi is that Rekall is much more developed,
but maybe thats not a "real" difference, more a matter of time and
competition. Also, maybe, Kexi is headed on a route that leads to more
separated components, while Rekall is more monolithic.</p>
<p>I think that in the long run the similarities are more likely to stand out,
but maybe thats because the tables-forms-reports-scripts model of database
front ends is so well ingrained. Everyone follows the Access model (and no
doubt Access followed someone else's model). Maybe there is a radically
different model of database access that would be a big improvement on this,
but if there is then I'm not bright enough to figure it out!</p>



<p><strong>Fabrice Mous: </strong><em>Is Rekall going to compete with Kexi or do you think some overlap wil be shared. Are
there fundamental differences between the two? is there some cross collaboration in the
works maybe?</em></p>
<p><strong>Mike Richardson: </strong>I guess they will compete, in the short term at least - though competition is
no bad thing, we all know where lack of competition has taken us (or, rather,
taken others:). So far as differences go, the devil is in the details, not
the overall structure. For instance, Kexi (correct me if I'm wrong guys) is
using <A href="http://www.trolltech.com/products/qsa/index.html">QSA</A> for its scripting, while Rekall uses <A href="http://www.python.org/">Python</A> (though there is a KJS scripter in the works; it part worked a while back but I'm waiting on 3.2 before revisiting it).</p>


<p><strong>Fabrice Mous: </strong><em>Where do you see Rekall 1 year from now and where do you want the project to go in the future?</em></p>
<p><strong>Mike Richardson: </strong>The biggest thing that interests me at the moment is reusability. I've done
quite a lot of Perl and Python website development, and one of the nice
things is that you can define a class that (say) handles a text input field;
then you can derive a class that does a read-only field with blue text on a
grey background, stuff like that.</p>

<p>Doing things like read-only-blue-on-gray in Rekall (or Access:) is quick (if a
bit tedious) until the boss says "the management want all those read-only
fields changed to red-on-black" and you spend the next morning updating them,
which is really tedious. What is even more tedious is when he says
"management says thay want them back to blue-on-grey".</p>

<p>Rekall 2.1.x allows the user to define their own components, which can be
pasted into forms and reports. Rekall 2.2.x extends this to linking the
components, so that changing the component changes in all the forms that use
it, plus, components can link components, and you can override the event code
in the component. Which, basically, is objects and inheritance:)</p>

<p><strong>Shawn Gordon: </strong>What I'd personally like to see is integration in to KOffice and OpenOffice as a component.  Feature wise
there are some things that I think would be interesting such as web based forms and reports.  We introduced
a feature into KDE years ago called KDE-DB that eventually became our Rekall data access engine, but even in
that original incarnation you could then use our kpart from konqueror to browse the contents of a database and
perform basic queries.  I don't know why this got taken out, it was incredibly cool, but I hope it makes it back in.
You sure don't see that feature in any other browser as far as I know, and we had it 3 years ago.</p>


<!-- Personal KDE questions -->

<p><strong>Fabrice Mous: </strong><em>Are you using KDE as your destop?</em></p>
<p><strong>Mike Richardson: </strong>I use KDE as my main desktop, for everything I can. Regretably, switching
hats, I have Windaz XP/2K box next to me, used for projects for various other
clients.</p>
<p><strong>Shawn Gordon: </strong>When I work in Linux, yes, I don't use anything else</p>

<p><strong>Fabrice Mous: </strong><em>What version of KDE are you using?</em></p>
<p><strong>Mike Richardson: </strong>3.1.2</p>
<p><strong>Shawn Gordon: </strong>3.1.1</p>

<p><strong>Fabrice Mous: </strong><em>Do you use CVS?</em></p>
<p><strong>Mike Richardson: </strong>Not since KDE2 was about to come out. Keeping up with building Rekall for a
around 6 to 8 distributions provides enough version fun without throwing CVS
into the pot. Although, once 3.2 is out I'll be in there pretty quick!</p>
<p><strong>Shawn Gordon: </strong>The CVS version of KDE?  No.  We have to support and test on stock linux distros, so we tend to not
go bleeding edge, we almost never even look at the beta's of linux distros or KDE.</p>

<p><strong>Fabrice Mous: </strong><em>What are your favorite KDE apps? What are your favorite tools?</em></p>
<p><strong>Mike Richardson: </strong>Konqueror and Konsole. Being an edit-and-make guy (ssed? anyone ever heared of
ssed) konsole gets used the most. I guess I have to mention Cygwin, without
which Windaz would be totally intolerable (as opposed to just mostly
intolerable).</p>
<p><strong>Shawn Gordon: </strong>Konqueror is the most used for me, other than that it is almost all our apps
like Aethera (which does integrate KOrganizer) and its plugins tkJabberPro and tkWhiteboard for
collaboration with our team, tkcOggRipper for ripping my CD's, Kivio, KVirc and Rekall.  For office stuff
I tend to use OpenOffice because of the filters, but the performance is terrible.</p>


<p><strong>Fabrice Mous: </strong><em>If you could see one type of application offered by KDE (that isn't currently offered),
  what would it be?</em></p>
<p><strong>Mike Richardson: </strong>A substitute for the GIMP. Sorry, but I *hate* the GIMP. All those nastly
windows poping up all over the place, and that file dialog is out of the
stone age. I'm sure it is *really* powerful and has *loads* of features, but
I just can't get over the learning hump.</p>
<p><strong>Shawn Gordon: </strong>I was thinking about this the other day actually, and it's something that gets talked about off and on
all the time and that is good Project Management software, there are others, but we're already working on
those.  The best I've seen is MR Project, but that is GTK, so I don't use it.</p>



<p><strong>Fabrice Mous: </strong><em>Are you pleased with the rate at which KDE is evolving?</em></p>
<p><strong>Mike Richardson: </strong>Well, it is obviously progressing much too slowly! No, seriously, I'm very
happy.
</p>
<p><strong>Shawn Gordon: </strong>As a technology it is more than robust enough to accomodate pretty much anything I saw being done
in corporate America the last time I was working in it.  I've always been amazed what what KDE has
been able to accomplish with so much fewer resources than a lot of other less ambitious projects.</p>

<p><strong>Fabrice Mous: </strong><em>Any other comments or suggestions for this interview?</em></p>
<p><strong>Mike Richardson: </strong>Just one thing, which is to echo what I think Shawn is saying, and that is,
what about Novell, Suse and Ximian/Gnome? Like, I'd hate to have to port
Rekall to Gnome :(</p>
<p><strong>Shawn Gordon: </strong>A couple closing comments.  I do a lot of interviews, and lots of emails and people ask you questions about
things, and you answer them.  Afterwards people tend to parse and microscopically examine everything that was
said which is in stark contrast to how the answers were given.  Basically we haven't planned out every possible
scenario for every possible product or situation.  We do the best we can with the resources we have and we act
and react as needed as situations change.  When we started out over 4 years ago, I never would have thought that
I'd be in most of the markets that I am now.  So if you see things change, it isn't because of any grand plan or
conspiracy, it's just changing to accomodate current realities typically.  So if you don't understand something
about us, or hear something about us that doesn't seem right, just ask me, I answer emails very quickly, you
wouldn't believe some of the bizarro rumors I've seen floating around.</p>
