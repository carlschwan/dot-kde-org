---
title: "2003 LinuxQuestions.org Members Choice Awards"
date:    2003-11-17
authors:
  - "jeremy"
slug:    2003-linuxquestionsorg-members-choice-awards
comments:
  - subject: "Nominations"
    date: 2003-11-16
    body: "Now let's go and see what nominations they missed this time... :-)"
    author: "Anonymous"
  - subject: "Re: Nominations"
    date: 2003-11-16
    body: "The above list does not include JuK, k3b, Scribus, KStars, Kile, Kopete, KBarcode for starters.  :-)"
    author: "anon"
  - subject: "Re: Nominations"
    date: 2003-11-16
    body: "CD-Burning and Instant Messanging categories would have been indeed interesting this year."
    author: "Anonymous"
  - subject: "Re: Nominations"
    date: 2003-11-17
    body: "And I can't find kdevelop! Best IDE of the year."
    author: "panzi"
  - subject: "Re: Nominations"
    date: 2003-11-17
    body: "Really, better than Eclipse? someow Don't think so."
    author: "Maynard"
  - subject: "Re: Nominations"
    date: 2003-11-17
    body: ">Really, better than Eclipse? someow Don't think so.\nNot for Java devlopement, that's right, \nbut for C++ i would say lightyears ahead ;).\n\nRischwa"
    author: "Rischwa"
  - subject: "Re: Nominations"
    date: 2003-11-17
    body: "But Eclipse can do anything. The plugins architecture is also very good. Eclipse is rather rapdly becoming an inustry standard. Plus its totally cross platform too."
    author: "Maynard"
  - subject: "Re: Nominations"
    date: 2003-11-17
    body: "> But Eclipse can do anything.\n\nSo? Does not it fully support automake and am_edit? What\nabout support for qmake? Does it allow browsing kdelibs,\nqt, libc documentation with full text search capabilities?\nWhat about signal/slot support in the class browser?\nMan pages? Integration of any KPart? ioslaves? Integration\nof external programs? Konsole? Perforce support? Kompare\nsupport? Doxygen support? Regular expression tester? \n\nJust to name a few things ;-)"
    author: "Bernd Gehrmann"
  - subject: "Re: Nominations"
    date: 2003-11-17
    body: "We are actually getting to a situation where you can use any number of IDEs. Choose what you think is best!"
    author: "David"
  - subject: "My votes."
    date: 2003-11-17
    body: "I voted for KDE, OpenOffice, Konqueror, Fluxbox, Scalc, SWriter, Kmail, VIM and Mplayer. "
    author: "norman"
  - subject: "Re: My votes."
    date: 2003-11-17
    body: "Wow, thanks for that."
    author: "Yellowboy"
  - subject: "kspread ??"
    date: 2003-11-18
    body: "KDE has lots of great technologies, kspread is not yet one of them.  It is not complete enough to come anywhere near being usable for nontrivial operations.  There are reasons to chose ooo or gnumeric, but selecting kspread now is just silly.  Load up some of the regression tests from the gnumeric team and watch it get NONE of it correct, not just some, but a full fledged zero for some files.\n\nHave any of you actually used it or are we just a bunch of fanboys ?"
    author: "Derrick"
  - subject: "Similar polls here..."
    date: 2003-11-19
    body: "Similar surveys can also be found at http://www.linuxsurveys.com"
    author: "Anon"
---
Voting for the 2003 <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a> <a href="http://www.linuxquestions.org/questions/forumdisplay.php?forumid=37">Members Choice Awards</a> has <a href="http://www.linuxquestions.org/questions/showthread.php?threadid=116431">begun</a>.  There are awards for Distribution of the Year, Browser of the Year, Multimedia App of the Year and many other categories.  KDE related projects up for awards include KDE, Konqueror, KWin, KOffice, KSpread, KWord, Quanta, KMail and Kate.
<!--break-->
