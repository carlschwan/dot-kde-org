---
title: "KDE-CVS-Digest for October 31, 2003"
date:    2003-10-31
authors:
  - "dkite"
slug:    kde-cvs-digest-october-31-2003
comments:
  - subject: "WoW! Thanks, Derek!"
    date: 2003-10-31
    body: "Your early!!! =) Is this a Haloween treat?\n\nAnyway, I'm glad that its feature freeze, I've already noticed the difference, hundreds of bugs are being fixed now, Konueror developers for example have eliminated 200 bugs just this week, a lot of that is due to Stephan ;) \n\nAnyway, does that mean KDE is still on track for 2003? I was hoping it would be delayed till January 1st so we can celebrate the new year in style! Besides the feature schedule still has ways to go and already about 20% of the features planned have been moved for 3.3 http://developer.kde.org/development-versions/kde-3.2-features.html"
    author: "Alex"
  - subject: "Re: WoW! Thanks, Derek!"
    date: 2003-10-31
    body: "Konqueror appears to have a lot of rendering regressions since 3.1. There's likely to be loads of bugs reported once a few people start using the betas.\n\nIt probably doesn't help that the instructions for reporting bugs in konqueror say not to report rendering errors unless you have a test case though."
    author: "cbcbcb"
  - subject: "Re: WoW! Thanks, Derek!"
    date: 2003-10-31
    body: "I noticed that.\nwww.mplayerhq.hu renders its menu incorrectly.\nwww.showbandmarum.nl doesn't load at all.\nand java does not work at all any more.\n\nbut i heard some of my problems might also be 2.6 kernel related (java) so i'll first try and find that out before reporting.\n\nplease fix these regressions before releasing.\nkonqy is really frickin fast in 3.2 but with all those errors less usefull than it was in 3.1"
    author: "Mark Hannessen"
  - subject: "Re: WoW! Thanks, Derek!"
    date: 2003-10-31
    body: "> It probably doesn't help that the instructions for reporting bugs in konqueror say not to report rendering errors unless you have a test case though.\n\nIt does. Unfortunately it only partly prevents \"this whole site doesn't work\" reports. "
    author: "Anonymous"
  - subject: "Re: WoW! Thanks, Derek!"
    date: 2003-10-31
    body: "\nSo, any one want to do a test case for the Konqueror lock up at\nhttp://www.cambridge-news.co.uk/\n\nTo reproduce: visit that site. Wait a short while. Konqueror stops responding."
    author: "cbcbcb"
  - subject: "Re: WoW! Thanks, Derek!"
    date: 2003-11-01
    body: "I don't think many people would expect you to give a test case for a page that locks up.. I'd just submit it to b.k.o.\n\nUsually the khtml bugs that get the most attention are ones that are crashes, lockups, or ones with test cases. "
    author: "anon"
  - subject: "Re: WoW! Thanks, Derek!"
    date: 2003-10-31
    body: "> Anyway, does that mean KDE is still on track for 2003?\n\nI doubt it. Either there will be a buggy KDE 3.2.0 and only 3.2.1 or even 3.2.2 will be usable or the whole release gets delayed until [end of] January."
    author: "Anonymous"
  - subject: "Thanks Derek..."
    date: 2003-10-31
    body: "...and Happy Halloween everybody!!!\nArrgh, and I don't have to work tomorrow anyway :-(\nWhy oh why must Allhallows be on a Sat?"
    author: "Mike"
  - subject: "Re: Thanks Derek..."
    date: 2003-10-31
    body: "Just curious as a non American.. What is it you do on Halloween all day? Scare the shit out of everyone?\n\nBOOH!"
    author: "ac"
  - subject: "Re: Thanks Derek..."
    date: 2003-10-31
    body: "Give candy to little kids dressed in costumes..."
    author: "Joe"
  - subject: "Re: Thanks Derek..."
    date: 2003-11-01
    body: "Well, what I've done for Halloween so far has been dress up as the Grim Reaper for work, including wearing a black mesh mask over my face so no one could see it.\n\nChildren here typically come back from school, get dressed, and go out trick or treating with their parents or other relatives.  Basically, they go around from door to door knocking at houses and asking for candy.\n\nThose of us with no children have parties, as we're kinda waiting to do at this point..."
    author: "Michael Pyne"
  - subject: "No full kroupware support"
    date: 2003-10-31
    body: "Angry!!, we need this for kde3.2. Please spend a lot of time for this features.."
    author: "Phantom"
  - subject: "Re: No full kroupware support"
    date: 2003-10-31
    body: "Sorry, but we didn't have enough time/man power to do this. And since we are now in feature freeze... But there will (most likely) be an additional release of the PIM programs shortly (i.e. a few months) after KDE 3.2. So you won't have to wait for KDE 3.3 or whatever comes next.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: No full kroupware support"
    date: 2003-10-31
    body: "Will that release also feature kontact?\nIt look frickin cool, and i'd love to give it a try.\n(the 3.1 download availible didn't compile under 3.2 for me, and i don't have 3.1 anymore)"
    author: "Mark Hannessen"
  - subject: "Re: No full kroupware support"
    date: 2003-10-31
    body: "kde 3.2 will have kontact, but it won't have a full kolab or exchange2000 capabilities, and kmail won't have html editing =)"
    author: "anon"
  - subject: "Re: No full kroupware support"
    date: 2003-11-02
    body: "> kde 3.2 will have kontact, but it won't have a full\n> kolab or exchange2000 capabilities,\n\nIt's a pity\n\n> and kmail won't have html editing =)\n\nCool!\nI thought I would have to open a bug \"kmail has html editing\"."
    author: "Asdex"
  - subject: "Re: No full kroupware support"
    date: 2003-11-02
    body: "All three will probably be part of a seperate kontact release a few months after kde 3.2 however. \n\nnote that kontact will still be shipped as part of kde 3.2, but only the most stable parts (which means, no html editing in kmail, or full exchange/kolab support)"
    author: "roo"
  - subject: "why?"
    date: 2003-11-03
    body: "Why would anyone be against kmail having html redering end editing? If anything this seems like a so big plus and it is standard for most mail clients."
    author: "Alex"
  - subject: "Re: why?"
    date: 2003-11-03
    body: "some people are paranoia. (it is even a little true)\neverything that is more intelligent than \"plain text\" is a potential viri/hack/crack/self-executing worm/trojan horse or whatever evil you can imagine.\n\nsure \"writting a mail\" in HTML is not going to be a security risk.\nbut if someone sends you a HTML mail, someone else is going to recieve it.\n\nand that person can either \"read it as source\" (costs way to much time if you recieve 25+ mail per day.) or \"just blindly\" execute it.\n\nboth are not an option for most paranoia's\nso that is why they hate HTML.\n"
    author: "Mark Hannessen"
  - subject: "Re: No full kroupware support"
    date: 2003-10-31
    body: "Kontact is part of PIM and the whole idea of the extra release is to ship a version 1.0 of Kontact. See http://www.kdedevelopers.org/node/view/221"
    author: "Anonymous"
  - subject: "Re: No full kroupware support"
    date: 2003-10-31
    body: "Let's hope that it will be early enough to get included in the major distribution releases (Fedora, Mandrake, SUSE)."
    author: "Anonymous"
  - subject: "Enter closes completion popup"
    date: 2003-10-31
    body: "Hurrah! It's these little gems in amongst lots of major changes that makes reading the digest worth it :) The number of times I've submitted a form because of this bug!\n\nI was also pleased to see that ALT+F1 now opens the kmenu above the kmenu icon, not off the mouse cursor's position."
    author: "Tom"
  - subject: "Re: Enter closes completion popup"
    date: 2003-10-31
    body: "> I was also pleased to see that ALT+F1 now opens the kmenu above the kmenu icon, not off the mouse cursor's position.\n\nhttp://bugs.kde.org/show_bug.cgi?id=49601 should be closed then?"
    author: "anon"
  - subject: "Re: Enter closes completion popup"
    date: 2003-10-31
    body: "Thanks to your wrong suggestion (see comment about revert), someone misinformed now closed it. :-("
    author: "Anonymous"
  - subject: "Re: Enter closes completion popup"
    date: 2003-10-31
    body: "> I was also pleased to see that ALT+F1 now opens the kmenu above the kmenu icon\n\nThat commit was reverted later: http://lists.kde.org/?l=kde-cvs&m=106709727527477&w=2"
    author: "Anonymous"
  - subject: "Re: Enter closes completion popup"
    date: 2007-03-22
    body: "Um... I want the \"menu at the mouse pointer\" deal back. It was a usability win for KDE. If your mouse is somewhere else on screen, it means you're right at the menu to start clicking away... not having to go all the way down to the bottom left, click, blah. For people on laptops and things like that, they can't just whizz the mouse down there... and even if you are on a nice mouse, it's still quicker to have the menu at the pointer, because it's relative to the same mouse move every time, and your muscle memory gets very slick at automatic opens of common applications.\n\nKDE lost a little something when the menu got stuck in the corner. It seems easy enough to make it configurable?..."
    author: "Arron Bates"
  - subject: "As always,"
    date: 2003-10-31
    body: "a big thank you, Derek!\n\nAnyone an idea when we should expect the 3.2 beta?"
    author: "Hoek"
  - subject: "Re: As always,"
    date: 2003-10-31
    body: "go to ftp.de.kde.org/pub/kde/unstable and you'll see a 3.1.93 dir. Sorry, it's not browsable and is first created on 29th, modified today.\n\nI hoped to get 3.2beta today because they preparing 3.2beta since sunday. And because developers often release software (kernel, kde, gnome...) on friday for a nice weekend ;-)\nAnd this could have been an halloween release :-(\n"
    author: "anonymous"
  - subject: "Web page annotation function on Konqueror"
    date: 2003-11-01
    body: "I am interested in text markup, drawing, and attaching function on web page(similar to Imarkup http://www.imarkup.com ,but only works on IE). Is this feature in CVS or planned in future version? or I need to file it in the wishlist?\nThanks!\n\n"
    author: "Bill"
  - subject: "Re: Web page annotation function on Konqueror"
    date: 2003-11-01
    body: "No feature like that is planned.  You can submit it to bugs.kde.org as a wishlist item but I doubt it will get much attention.  I have never heard of this kind of thing before and I don't see how it would be useful for me.  The only way KDE will get this feature is if one of the developers likes it so much and thinks that it will be so useful that he/she decides to implement it.  Submitting all the wishlist items in the world will not help if no developer likes the idea.\n\nTo me this feature seems broken.  What if the webpage changes?  Do you lose your annotations?  Do they stay in the same place, annotating content which has moved farther down the page, or even left the page entirely?  A much better idea is to save a copy of the webpage on your hard disk, and annotate it there.  An application that allows this might be useful.  But basically the ideal thing would be to have a WYSIWYG HTML editor for KDE, so you could save the webpage to your hard drive and edit it to your heart's content there, adding annotations or changing the webpage entirely."
    author: "Spy Hunter"
  - subject: "Re: Web page annotation function on Konqueror"
    date: 2003-11-01
    body: "This feature went in in August. Don't know if it works with this particular site, but the idea is to support annotation and content management systems.\n\nhttp://members.shaw.ca/dkite/aug222003.html#Konqueror\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Web page annotation function on Konqueror"
    date: 2003-11-02
    body: "Yes! It works like a simple WYSIWYG HTML editor, which is tightly integrated with browser.It can save the annotaion and web page in a mhtml file. The equivallent product equill bought by MS is said to be integrated into the new office suite for editing team document. \nSince the Konqueror team has their way to implement new features,the best way for me now is saving than editing locally.\nThanks Spy Hunter and Derek's explanation!"
    author: "Bill"
---
In <a href="http://members.shaw.ca/dkite/oct312003.html">this week's KDE-CVS-Digest</a>: Feature freeze instituted for <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">3.2 release</a>. <a href="http://kolab.kde.org/">Groupware</a> support merged, sort of. Many bug fixes, including 'enter closes completion popup' in <a href="http://www.konqueror.org/">Konqueror</a>.
<!--break-->
