---
title: "LinuxQuestions.org Members Choice Nominations"
date:    2003-01-04
authors:
  - "jeremy"
slug:    linuxquestionsorg-members-choice-nominations
comments:
  - subject: "Editor of the Year"
    date: 2003-01-04
    body: "nano? pico? Bluefish? Why not Kate or Quanta?"
    author: "Anonymous"
  - subject: "Re: Editor of the Year"
    date: 2003-01-05
    body: "that was *exactly* what i thought when i saw that poll. had to go with VIM in lieu of Kate. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Editor of the Year"
    date: 2003-01-07
    body: "Kate has been added to the Editor of the Year Award.  Awards have also been added for Word Processor of the Year (KWord was nominated) and Spreadsheet of the Year (KSpread was nominated)."
    author: "linuxquestions"
  - subject: "Re: Editor of the Year"
    date: 2003-01-07
    body: "That still leaves Quanta, obviously my tool of choice. I admit I think of it more as a web development platform and it currently plugs in Kate as an editor, though it may be fully pluggable in the future. I already know how popular Quanta is. I'd bet if anyone wanted to see what the most popular Linux web development tool is Quanta would easily come up first. So I'm not going to get excited over another web poll. However given that Bluefish is in there it seems incongruous. It would seem logical if that is how they structure the category that both Quanta and Screem would be there.\n\nIt's quite common for people to berate web polls for being arbitrary and inaccurate. In fact if you wanted to be more comprehensive it would seem a few other editors would be in there and it would be categorized for console, general purpose and special purpose editors. Anyway, if I can't vote honestly for Quanta (30,000 source downloads on a major revision is already a pretty good vote I think) then I can't see voting."
    author: "Eric Laffoon"
  - subject: "common sense"
    date: 2003-01-04
    body: "SIX release candidates and in proportion to its complexity as many bugs as the MS Windows environment. While KDE is infinitely better than GNOME (\"but OK buttons are confusing, it says so in my research paper, I've a PhD in UI design you know!!\"), I still think MS can relax on the Desktop while the state of the art is merely catch-up, of features both good and bad..."
    author: "Confucius"
  - subject: "Re: common sense"
    date: 2003-01-05
    body: "PhD? Whats that?"
    author: "Andreas Pietzowski"
  - subject: "Re: common sense"
    date: 2003-01-05
    body: "About the catch-up thingy:\nHaven't been using much KDE lately, have we? \n\nAbout the bugs:\nThe big difference between KDE (or other Open Source projects) and Windows is that \nbugs are not kept in secret basements. Everyone can post/fix/comment on a bug \npublicly. Believe me, if there was a bugs.microsoft.com, more than a million MS-bugs would \nhave been posted. \n\nAbout the six release candidates:\nEveryone knows that the KDE 3.1 would have been released almost a month ago. The \nteam decided to take it easy and fix some security holes that were discovered. Now *this* \nis what I call responsibility towards the end user, especielly if you consider that the end user \ndoes not pay anything and KDE is provided without any warranty."
    author: "Dimitris Kamenopoulos"
  - subject: "Re: common sense"
    date: 2003-01-05
    body: "many of the bugs in bugs.kde.org are old and/or not very helpful to the developers (e.g. don't have enough info to accurately address)\n\nif you (and yes, i mean you in the sense that you can actually do this) were to go through and help go through the bugs and ask for more info where appropriate, try and duplicate the bugs locally, merge duplicate bugs, etc... we'd end up with fewer reports in the system. this would also increase the usefulness of the bugs system (which already very useful) as there would be less noise and more signal.\n\nalso remember that many of the reports in the bugs system are wishlist items.\n\nof course, any more than 0 bugs is too many. and of course, we'll never have 0 bugs. KDE is too complex, too big and has too much new development for a zero bug count reality. the good news is that the developers are getting much more use out of the new bugs.kde.org than they did with the old one, judging from activity and raw report #s."
    author: "Aaron J. Seigo"
  - subject: "Re: common sense"
    date: 2003-01-06
    body: "I dunno, there is that walking-on-air-for-shaped-decos bug in AMOR that may be best left unfixed (seeing that's it hilarious and thus potentially a feature for the app) ;-)\n"
    author: "Sad Eagle"
  - subject: "Re: common sense"
    date: 2003-01-06
    body: "hi,\nwhen u have an PhD on UI: why aren't u helping? or do you?\n\nwould be very useful!\n\n-gunnar"
    author: "gunnar"
  - subject: "Re: common sense"
    date: 2003-01-07
    body: "Since you are talking in that order of magnitude you might also want to take a look at http://www.pivx.com/larholm/unpatched/ for a list of 19 known but still not patched vulnerabilities in Microsoft's core product Internet Explorer, the oldest one dating one year back. All of Microsoft service and bug fixes releases could fix those vulnerabilities, they should have the man power and the money to do so easily. On the other hand KDE has got one known vulnerability and that leads to a delay for a complete source review to get rid of it. I don't think you are bitching at the right place here."
    author: "Datschge"
  - subject: "If only they had release candidates for vote lists"
    date: 2003-01-04
    body: "I also was astounded to not find Kate listed under editors. The number of votes that KWin received are surprinsing, given that then I didn't expect KWin to be listed as a window manager, after Kate had been ignored.\n\nKMail does the trick great for me. The only thing I have to regret is that I cannot connect to the addressbook we have in the company. It's Exchange stuff. I guess, I can have my calendar, my IMAP mail, but not the non-LDAP addressbook, right? Given the flamewars around KMail branches, I have the impression it's under quick development, despite already being good enough. That's great.\n\ncu\n"
    author: "Debian User"
  - subject: "Biased call for votes?"
    date: 2003-01-05
    body: "Will such article be posted to other sites too, like gnomedesktop.org, in order that Gnome has a chance to gain some votes too? :-)"
    author: "Anonymous"
  - subject: "Re: Biased call for votes?"
    date: 2003-01-05
    body: "Perhaps GNOME people are busy fixing bugs instead of playing around getting votes in, lets face it, very badly organised polls! The choice of applications in each category are clearly an 'off the top of my head' list by someone rather than a considered nomination of projects released in the year?"
    author: "KArgonaut"
  - subject: "Re: Biased call for votes?"
    date: 2003-01-05
    body: "> Perhaps GNOME people are busy fixing bugs instead of playing around getting votes in\n\nI doubt that jeremy@linuxquestions.org who posted this story is a KDE developer."
    author: "Anonymous"
  - subject: "Re: Biased call for votes?"
    date: 2003-01-07
    body: "The story has indeed been posted at gnomedesktop.org, just took a little longer to show up.  The polls are meant to be open to all projects - having a release during the year was not a prerequisite (atleast not this year). After looking at Kate it has been added to the Editor of the Year Award (it would have been added quicker had the suggestion been made in the poll itself).  Furthur suggestions are welcome."
    author: "linuxquestions"
  - subject: "Re: Biased call for votes?"
    date: 2003-01-07
    body: "It wasn't posted by you though.\n\nI posted it a day after I saw it on this site, and it was my submission that was posted. I doubt that they would have picked mine over yours (if indeed you did post one)."
    author: "IHateIain"
  - subject: "Re: Biased call for votes?"
    date: 2003-01-07
    body: "I posted identical stories at identical times - why they did not post mine, then posted yours days later I do not know.\n\n--jeremy"
    author: "linuxquestions"
  - subject: "Re: Biased call for votes?"
    date: 2003-01-07
    body: "Apologies then"
    author: "IHateIain"
  - subject: "Re: Biased call for votes?"
    date: 2003-01-07
    body: "> it would have been added quicker had the suggestion been made in the poll itself\n\nWell I tried, but as a newly-registered member (I registered to vote) it wouldn't let me post..."
    author: "My Left Foot"
  - subject: "don't forget to vote here too!"
    date: 2003-01-07
    body: "http://www.linuxformat.co.uk/modules.php?op=modload&name=Awards&file=index"
    author: "M"
---
<a href="http://www.kde.org/">KDE</a>, KWin, <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://kmail.kde.org/">KMail</a> and <a href="http://www.koffice.org/">KOffice</a> have been nominated for <A href="http://www.linuxquestions.org/questions/showthread.php?threadid=39873">Desktop Environment</a>, <a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39874">Window Manager</a>, <a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39867">Browser</a>, <a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39878">Mail Client</a> and <a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39876">Office Suite</a> of the Year at <a href="http://www.linuxquestions.org">LinuxQuestions.org</a>.  You can see the Members Choice Announcement <a href="http://www.linuxquestions.org/questions/showthread.php?s=&threadid=39866"> here</a> or go right to the <a href="http://www.linuxquestions.org/questions/forumdisplay.php?s=&forumid=21">nomination awards</a>.
<!--break-->
