---
title: "Tutorial: Shell Scripting with KDE Dialogs"
date:    2003-10-20
authors:
  - "bhards"
slug:    tutorial-shell-scripting-kde-dialogs
comments:
  - subject: "Easy as pie"
    date: 2003-10-20
    body: "Great tutorials make the subject matter look easy. Good work."
    author: "Ian Ventura-Whiting"
  - subject: "Re: Easy as pie"
    date: 2003-10-23
    body: "Yup. It is absolutely stuff that is needed. Simple enough to be understandable for newbie, covering enough to be useful out of a box.\n\n//larry "
    author: "larry"
  - subject: "Very good tutorial for a newbie"
    date: 2003-10-20
    body: "I'm not a coder at all, I barely can read \"C\" code, but this tutorial es pretty self explanatory, and since i'm good at scripting (<-- ??) i can think on a couple of nice dialogs I can use for my little taks\n\nThanks Brad !!\n\nMax"
    author: "Max"
  - subject: "Re: Very good tutorial for a newbie"
    date: 2003-10-23
    body: "There are so many other languages, basic for  instances,\n\nhbasic looks pretty good\nhbasic.sf.net\nGambas is nice\ngmabas.sf.net\n\n\nHowever it is not in production yet. i think KDe shall better incorporate an easy compiled programming language for beginners. For pure applications C is not first choice. \n"
    author: "gerd"
  - subject: "A very limited choice"
    date: 2003-10-20
    body: "I wonder if the author is aware of other choices that are far less constraining. I evaluated kdialog and it was a single widget simple dialog with no real power. Honestly I couldn't really find a lot of uses for it outside of the cute examples you always see. I always needed more than one single element in the equation. Try using it to manage tar or postscript from the command line. Try using it without a command line. I found for writing a script to make a dialog that Kaptain was much more powerful and still pretty easy. Kaptain has a lot of great features. Unfortunately as simple as it is people get turned off having to write the dialogs and we wanted to add a number of features... We just went ahead and wrote Kommander which has a visual dialog designer adopted from Qt Designer and it can incorporate shell scripts in it. It is great for dialog based mini applications and using DCOP and scripting to \"glue\" applications together or extend apps. It can actually do useful work with nothing more than point and click design.\n\nThe unfortunate thing of all of this is that for the most part people have no idea these tools even exist or what they can do for them. That's a shame. If you nead a dialog with more than a single widget, maybe a tabbed dialog and the ability to construct text strings then you can check out Kaptain at http://kaptain.sf.net and Kommander at http://quanta.sourceforge.net/main2.php?snapfile=snap02. Kommander is part of the Quanta project and is often used for text insertion or file creation. These are tools that have proven useful over the years to me."
    author: "Eric Laffoon"
  - subject: "Re: A very limited choice"
    date: 2003-10-20
    body: ":: The unfortunate thing of all of this is that for the most part people have no idea these tools even exist or what they can do for them. That's a shame.\n\nSo why not write a tutorial.  It seems to have worked in raising kdialog's exposure.\n\nFor me, Kommander is just that app that launches when I forget that Quanta Plus (another highly advocated app) isn't named 'quanta' and I hit Alt-F2 and type 'quanta'.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: A very limited choice"
    date: 2003-10-20
    body: "> So why not write a tutorial. It seems to have worked in raising kdialog's exposure.\n\nActually we did. We also had it reviewed by newbies and decided it could be more newbie friendly in terminology so it's being rewritten. We had a reviewer who was writing a review that did not develop. To top it all off, while it is very capable, stable and usable we have had difficulty with the maintainer's time or adding developers to do the little things to make it even sweeter for non technical users. It is ideally positioned for the non technical user because it can do text manipulation without actual programming of any traditional type. Unfortunately usability testing indicates that we do not yet get the concept across to the most casual of users well yet... so it's not quite your mom's dialog builder.\n\nMy comment was more directed that often open source projects list other similar projects if they might better serve the user and reviews often mention the other programs. I understand this is just a tutorial, so it's niether of the above, but given that many people have not heard of any of these programs it might be worth mentioning it is not unique, but part of a genre. I also have to admit I never really found the program to be very useful becuse it is so narrow focus. Kaptain and Kommander both allow you to grow what you're doing.\n\n> For me, Kommander is just that app that launches when I forget that Quanta Plus (another highly advocated app) isn't named 'quanta' and I hit Alt-F2 and type 'quanta'.\n\nWhat? Not on my system. The action you describe launches Quanta, as it should. Besides, to be precise, Kommander is two applications. There is a dialog designer and a dialog executor. I'm not sure the relevence lf \"highly advocated\" but given a variety of circumstances Quanta is well positioned to be the first KDE application that substantially exceeds the specifications of any competing commercial application and draws a lot of users to KDE. Given how thinly spread our developers are and how happy our users are we can use all the advocation we can get. ;-)"
    author: "Eric Laffoon"
  - subject: "Not at all"
    date: 2003-10-20
    body: "I agree with Eric Lafoon, KDialog is very limited, but is very useful when you need to port a shell script to a windowed environment.\n\nThink in Knoppix, it has a script who changes the IP configuration, it uses Xdialog but I can change sucessfully to KDialog for my own distro KDE Hispano Live 3.2.\n\nEvery thing has his utility, and Kommander is a bit dificult for me, but I never develop in C++ or with graphic interfaces.\n\nAnd about PyKDE, I really like to learn about it, but I have not time enough, even the day has 72 hours.\n\nGreetings and bye."
    author: "Pedro Jurado Maqueda"
  - subject: "Re: Not at all"
    date: 2003-10-22
    body: "Hmm, this two arguments attached one with the other seems like a bug ;)\n\n> Kommander is a bit dificult for me, but I never develop in C++ or with graphic interfaces.\n\nKommander and C++ do really have nothing in common in usage. You usually use the shell with kommander."
    author: "anon"
  - subject: "Re: Not at all"
    date: 2003-10-22
    body: ">> Kommander is a bit dificult for me, but I never develop in C++ or with >>graphic interfaces.\n \n> Kommander and C++ do really have nothing in common in usage. You usually use >the shell with kommander.\n\nI mean, I never used Qt Designer who is very similar to Kommander.\n\nAnd I try to use Kommander (I'm his Spanish translator), but only I can run the examples and not at all. Maybe I need to improve my skills with kommander, but as I say before, I have enough skill to use kdialog and this is the reason because I'm using it now"
    author: "Pedro Jurado Maqueda"
  - subject: "Re: A very limited choice"
    date: 2003-10-20
    body: "While kaptain looks nice and useful, it is not on my system. It is not standard with KDE.\nOTH, Kdialog goes with each and every kde install. The nice thing about using it, is that it can be counted on. Kinda of like choosing a script. Do you go with sh, ksh, bash, or perhaps zsh. bourne gives the minimalist approach. For the most part so does korn, but the problem is that it is not quite fully standardized. bash is on just about every OSS system, and freely available, but many of the older closed systems do not carry it. Finally zsh. Power. Good power. But it does not really used. Many of the linux do not even bother with default install. Personally, I would stick with perl :)."
    author: "a.c."
  - subject: "Re: A very limited choice"
    date: 2003-10-21
    body: "> While kaptain looks nice and useful, it is not on my system. It is not standard with KDE.\n\nYes, in fact the latest version was still just Qt last I checked. We were going to enable KDE for Terek but did Kommander instead. If I ever had time... ;-)\n\n> OTH, Kdialog goes with each and every kde install. The nice thing about using it, is that it can be counted on.\n\nIs it any wonder I make so much noise? Knowledgable people point out to me that Kdialog is simple to use and included with KDE... (sigh) Quanta is part of KDE. If you have it installed try typing \"kmdr-editor\" and seeing what happens.\n\nNow try making a dialog like the attached one with kdialog. (This dialog uses only a little shell scripting plus the native connections to control whether a field is enabled or not. Best of all a user can load it up and enhance it for their purposes.) You can get it and run it here. To run use \"kmdr-executor [path/]htmlquickstart.kmdr\".\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/quanta/quanta/data/scripts/htmlquickstart.kmdr\n\n> Personally, I would stick with perl :).\n\nKommander is language agnostic. It can store, integrate and run any script language your shell can. Shell scripting is built in too so no need to shebang."
    author: "Eric Laffoon"
  - subject: "Re: A very limited choice"
    date: 2003-10-21
    body: "Very neat (kmdr-editor).  I wish I knew about that before.  What can you do with the forms once you make them?  How do you attach event handlers?  I think I might have figured out how to attach a few basic event handlers, but what about custom ones with some actual code?"
    author: "David Walser"
  - subject: "Re: A very limited choice"
    date: 2003-10-21
    body: "This has been addressed in DCOPWidgets, but alas, it was not done for KDE 3.2 :P\nFor more information see: http://geiseri.myip.org/~geiseri/dcopwidgets/\n\nThis was not refined enough for 3.2, and im still working on a cleaner way to transparently map DCOP Signals to these function callbacks for use in scripts.  The advantage here over kdialog is that this only takes a shell script, and the version im hacking on now can even embed the ui file in the shell script.  This allows one to use things like python, perl, and bash shell.\n\nMaby 3.3 will have this, then we can have some real interactive shell script GUIs :)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: A very limited choice"
    date: 2003-10-22
    body: "How do I say this without sounding like an over stimulated geek - I was HOT for DCOP widgets! This looked like the most outstanding idea that answered so many questions. Unfortunately some things don't make schedule.\n\nIan, I think we will be doing a lot with Kommander when it can't be released with a KDE release. We will probably do a technology preview release (English only) the first quarter of next year. If you want to communicate on this and maybe have some more progress with your DCOP widgets that would be excellent. I think this combination has the possibility to demonstrate amazing power in KDE without actually needing to really program and yet allowing language agnostic extentions. As I see it that is the ultimate user upgrade."
    author: "Eric Laffoon"
  - subject: "Re: A very limited choice"
    date: 2003-10-22
    body: "First off you can use the \"text associations\" to enable widgets to be used to assemble text. You can then out put that to stdout or another dialog. So you can echo text (which we use for creating complext strings to insert into the editor in Quanta) or you can output something to the command line or you can call a program and execute it with parameters... you can even make dcop calls.\n\nAs far as event handlers, what has been done so far is to use the signals and slots interface in Designer to make a basic event model. So to make an element toggle enabled on the click of a check box you click the icon to put you in that mode, drag and drop one object on the other and select the signal and slot to connect. It's fairly intuitive, once you first see it. We will be adding more events too. I'm also curious your ideas on event handlers. \n\nPart of what we need to do is to go beyond the basic \"oh, we can do this this way\" to a more refined process that addresses more functionality. Our objective is to create something that is very capable and extensible, but it will not be quite like KJSEmbed or a Python IDE. It will offer more power with less effort in exchange for all the power of the KDE API which others will offer. We figure if you go that far you might as well learn C++. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: A very limited choice"
    date: 2003-10-20
    body: "> Actually we did. We also had it reviewed by newbies and decided it could be more newbie\n> friendly in terminology so it's being rewritten. We had a reviewer who was writing a review that\n\nThat's an understatement - the docs are terrible (sorry, had to be said!). A good first move would be to separate the Qt designer stuff from the Kommander specific stuff. Then add some examples of how @exec and @dcop work etc.\n\nAnyway - is it technically possible to execute a program from a form and display the results in the same form - say a simple file viewer which has a FileSelector widget, button to execute 'cat filename', and line edit widget to display the results?"
    author: "Hmmm"
  - subject: "Re: A very limited choice"
    date: 2003-10-21
    body: "> That's an understatement - the docs are terrible (sorry, had to be said!).\n\nI didn't write them and would never say that. I think they're pedantic, but then I've always preferred examples to technical documentation.\n\n> Anyway - is it technically possible to execute a program from a form and display the results in the same form - say a simple file viewer which has a FileSelector widget, button to execute 'cat filename', and line edit widget to display the results?\n\nIt should be, however it has gotten just past the initial development and hasn't had the attention it should. It generates a static dialog, but it does have some abilities to update it's self. It sort of strange because once you initiate a concept in design you get to see what is actually made possible. ;-) There may be a way I'm unaware of. I'd like to enable several ways. I know that Kaptian 0.7 can do this with sockets, but that's not exactly user friendly.\n\nCurrently this can be done in different ways.\n1) You can have it output to the command line. (In Quanta we use it to dump to the editor.)\n2) You can have it write the output to a temp file and then have it open in an editor.\n3) You can use the creative work around... create a display dialog with a text area, bring the XML source up in an edtor and chop it in half in the text input area. Then write a simple script to concatonate the pre string, text and post string, direct it to a temp file, call the file (this brings up the text in a dialog) and delete it. Since it is currently a static dialog you can delete it while it's up.\n\nI know this solution isn't pretty, but it produces a nice looking result. I used it to create our doc installer which sequences dialogs but only has one. You may need to escape the double quotes in the XML string. BTW because it is a standard designer XML file we also use Perl scripts to generate them on the fly for customizing templates in Quanta.\n\nI hope to have much better solutions shortly, however we will not likely get much of what we had originally been hoping for into 3.2. "
    author: "Eric Laffoon"
  - subject: "Re: A very limited choice"
    date: 2003-10-21
    body: "> 2) You can have it write the output to a temp file and then have it open in an editor.\n\nYes, I did something like this with kdialog - dumping results to a tempfile and then displaying in a textbox - good for a laugh, but not much else :)\n\nI presume the real power will come with the DCOPWidget stuff (He says, knowing absolutely nothing about it!), but anyway, I'll keep an eye on future Kommander releases - it certainly has the potential to be a great addition to KDE."
    author: "Hmmm"
  - subject: "Re: A very limited choice"
    date: 2003-10-21
    body: "Never having used Kommander or Quanta, nor fidled with the config files, I think it is a SuSE problem. I'm running SuSE 8.2, and Alt-F2 \"quanta\" gives the Kommander editor.\n\n"
    author: "Rich"
  - subject: "Re: A very limited choice"
    date: 2003-10-22
    body: "I don't know what to say except edit your menus. Somebody somewhere is really confused if they think you can easily make web pages with Kommander or that it is Quanta. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: A very limited choice"
    date: 2003-10-24
    body: "> Alt-F2 \"quanta\" gives the Kommander editor\nIndeed, it does. ;) LOL!\nWhat a luck I'm personally using Debian at home. :-)\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: A very limited choice"
    date: 2003-10-20
    body: "I guess I don't see it as a \"everyone should use <other application> in preference to kdialog\" type choice. I see it as a \"here's an option that you might like to use, although if it doesn't suit you, look for another option\".\nWhen it comes down to it, probably the most flexible and expressive way to do dialogs is to hand-code them in C++. kdialog is at the other end of the scale - trivially easy to use, but not much power. The other choices that have been mentioned (including pyKDE) can be thought of as points on the scale (or perhaps some multi-dimensional space that shows all the trade-offs - such as whether it uses code not in basic KDE installs).\nThe power-users of today can become the newbie developers of tomorrow, and easy first steps are important. kdialog might be the first tool they try, but its unlikely to be enough for any big task - it is really intended for situations where a little user input is required, as in my password example, and perhaps some basic notification, but most things are automated.\nAlso, for those who wondered about the lack of other content (such as DCOP scripting or other tools, which might be equally useful in shell scripts) - remember that this was to be a chapter of the new developers book, and other chapters were / are expected to cover other tools."
    author: "Brad Hards"
  - subject: "Re: A very limited choice"
    date: 2003-10-20
    body: "I agree with you that it's a matter of choice, and certainly you present what you're presenting. You do however make the point of the paradox of of kdialog. It is a tool for enabling dialogs on the command line which is suited for inexperienced users. Ponder that. To me it's like saying \"It's a way of getting better fuel economy in dragsters targeted at commuters\". The more I work with the command line the more the cost benefit analysis seems to me to say \"I'm just going to use the command line because the dialog isn't doing what a good shell script would for me.\"\n\nSo while it can provide a useful, albiet limited, service, it offers it to an audience who is not likely spending a lot of time in the theater. That's not a reflection of it's worth, but of it's target audience.\n\nMy point is that given that target audience it would be good to let them know it is in a diverse range of tools. While Kommander needs a little more polish in it's documentation and minor touches in usage to be truly a newbie friendly tool it does offer several compelling features, and being new it's likely most people are unaware of them.\n1) It allows for visually building powerful dialogs.\n2) It is able to construct strings by binding text to widgets, very much like web forms\n3) By using file associations it's possible to have it's dialogs do useful work from an icon launch instead of a command line.\n4) Because it uses DCOP it can interoperate with other KDE applications very nicely.\n5) It can internally store and run scripts in any language you can run on your system including native shell scripting.\n\nI apologize for the advertisement, but I doubt this is common knowledge and it adds up to a tool that is just about as quick and simple to use, but with a whole lot more power. It would be nice if more people knew and recognized it because with a little help it could make a real difference in making the KDE desktop extensible for newbies as well as power users, without having to introduce a programming language dogma."
    author: "Eric Laffoon"
  - subject: "Re: A very limited choice"
    date: 2003-10-21
    body: "I think you're misrepresenting me. Or perhaps I'm not expressing myself very well. Whatever.\nI actually see kdialog as having two markets:\n1. Power users (who are probably familiar with the command line), moving into KDE development. That is, they are inexperienced developers, not inexperienced users. They can write scripts - they just want them to work a bit better/nicer.\n2. Serious scripts (perhaps an installation wrapper) which has to work everywhere KDE works, and needs to get just a little information.\nIn car analogy terms, the first is like adding cup-holders to your own car - not much improvement, but at least you can do it yourself without needing to be a mechanic. The second market is like a fuel additive that works on any car. OK - its a stretched analogy.\nAgain, I've got nothing against Kommander (yep, I wasn't aware of it either:-). I just don't see how a tutorial on kdialog reduces choice."
    author: "Brad Hards"
  - subject: "Re: A very limited choice"
    date: 2003-10-22
    body: "How could you use the dialogs generated by Kommander?\nThis is XML files. How do you make them into dialogs in a shell script?"
    author: "Jarl E. Gjessing"
  - subject: "Re: A very limited choice"
    date: 2003-10-22
    body: "kmdr-executor file.kmdr\n\nThe shell (whatever) scripts are in the texts associated to widgets. The Kommander executor shows the dialog and executes the script commands you have written there.\n\nAndras"
    author: "Andras Mantia"
  - subject: "A thought"
    date: 2003-10-26
    body: "Eric, I just played for the last week with your kommander. Nice. Very nice. The one place that it would have a great use for would be in kcontrol. If you think about it, kcontrol is a nice easy to use admin place. For the most part it simply adjusts files.  Most files have tools that can be cli invoked to handle the dirty work. If kommander had a kcontrol module available, it would be easy for coders (or light-weight coders) to throw together simple admin tools."
    author: "a.c."
  - subject: "Re: A very limited choice"
    date: 2003-11-16
    body: "Well, I managed to write a rather nice shell script front end to s/key using this tutorial (and a klipper \"action\") in about 10 minutes.  Even if I was familiar with widget creation using Kommander I doubt I could have done it that easily and it would have been overkill.  If you are using Kommander to give shell scripts a dialogue you probably have to ask yourself whether or not you are using the right tool for the job.\n\nThis is like saying \"the shell is limited compared to the C language\"... yeah, of course it is.  Now to make a more logical comparison, if you want to see a \"limited choice\" try out the kdialogue predecessor, xmessage.\n\n\n"
    author: "Dan Hamilton"
  - subject: "pykde"
    date: 2003-10-20
    body: "Dear Friends \n\nTalking of KDE and scripting.\n\nWhy is pykde not included in KDE ? This would be the ideal way for\nquick development in KDE. Take a look at this tutorial on how to\ncombine qt-designer and pyqt:\n\nhttp://www.python.org/cgi-bin/moinmoin/JonathanGardnerPyQtTutorial\n\nSome reasons why pykde should be a part of the kde project:\n\n* pyqt and pykde are not being used by many developers because the \n  distros don't ship it by default (they ship gtk)\n* python is a natural prototyping language for c++ (see tutorial above)\n* for many applications, python+kde should be enough\n* a python interpreter will be embeded in koffice:\n  http://developer.kde.org/language-bindings/\n* python code is written much rapidly than c++ code (in a ratio of\n  1 to 3 or even more)\n\nIn short, including pykde in the kde project would greatly benefit\nthe project by providing a quick development path. Am I missing something ?\nWhy is it not happening ? Should I file a whishlist report ?\n\nCheers !\n"
    author: "MandrakeUser"
  - subject: "Re: pykde"
    date: 2003-10-20
    body: "I raised this on the pykde mailing list a while back. Thread:\n<br><br>\n<a href=\"http://www.mail-archive.com/pykde@mats.gmd.de/msg03488.html\">http://www.mail-archive.com/pykde@mats.gmd.de/msg03488.html</a>\n"
    author: "Julian Rockey"
  - subject: "Re: pykde"
    date: 2003-10-20
    body: "arrg, this thread is VERY discouraging. So, the short answer is: forget it. This is very bad. I understand the main developer, but at the same time he should be willing to put things on CVS and get help. It's not like it takes a month to learn to deal with CVS. \n\nHas anything happened after this thread ? I haven't seen a link in the pykde page regarding this issue. \n\nAlso, have you tried contacting the pyqt maintainer with a similar question ? Even getting pyqt in kdebindings would be a step forward. Any more pointer ? Thank you so much !"
    author: "MandrakeUser"
  - subject: "Re: pykde"
    date: 2003-10-20
    body: "I think the main issue is to do with testing PyKDE before releases. Jim (the maintainer) likes to thoroughly test PyKDE before an official release, and this appears to take some time, so if CVS is going to add to this then it will probably make the process more tiresome.\n\nAnyway, the mailing list address can be used to contact the authors of both PyQt and PyKDE, so it makes it even more convenient to offer help to either project. ;-)\n\nhttp://mats.imk.fraunhofer.de/mailman/listinfo/pykde"
    author: "David Boddie"
  - subject: "Re: pykde"
    date: 2003-10-21
    body: "Which is why replacing CVS with subversion is going to help the entire world a lot :-)"
    author: "OSX_KDE_Interpollination"
  - subject: "Re: pykde"
    date: 2003-10-21
    body: "Since PyKDE and PyQt are released under the GPL, there is nothing stopping somebody maintaining a branch in KDE CVS, so long as the CVS copy remains reasonably up-to-date and the RELEASE copies in KDE are always thoroughly checked."
    author: "Tom"
  - subject: "Re: pykde"
    date: 2003-10-21
    body: "This is true, but hopefully this will not be necessary. I guess the best case scenario would be to get maintainers of PyKDE and PyQt to put their code in CVS under the KDE Project, and to get the attention of the folks maintaining the core kdelibs to give a hand with these bindings. What I am getting at is: to me the python bindings should be part of the core KDE dev cycle IMHO, and they should be given the same status of relevance as the C++ API. I am afraid it a little too late already. The feature freeze for 3.2 is around the clock and this would go into KDE 3.3, maybe two years from now at best. It is a real shame :-("
    author: "MandrakeUser"
  - subject: "Re: pykde"
    date: 2003-10-21
    body: "It's not all bad news. As long as robust packages appear in time to get included in new Linux distributions, or alongside KDE bugfix releases, then it may not matter whether it's part of core KDE or not. I get the impression that working around the eccentricities of various Linux distributions is a large part of the effort in supporting new releases of PyKDE, but I could be wrong."
    author: "David Boddie"
  - subject: "Re: pykde"
    date: 2003-10-22
    body: "Giving Python the same status as the C++ API isn't a good idea since KDE definitely needs a plus on speed and not a big minus as you get by using an interpreter language like python. So the core development should better concentrate on making the basement safer&faster by using advanced features of C++ like template partial evaluation for compile-time linking. There's a big potential for decreasing the abstraction penalty. Language bindings should'nt influence the core development at all, they should be treatened as satellites.  "
    author: "Ruediger Knoerig"
  - subject: "Re: pykde"
    date: 2003-10-22
    body: "Sure, I agree in all you say, but I wasn't clear in my original post perhaps. I am not saying that KDE should write the libs or core software in python. Not at all. I am saying that it should adopt one particular set of bindings, python's. The reasons and benefits are hopefully clear in my posts above. The way to implement this would be to get the python bindings synced with the KDE libs before each release. And include the bindings as a part of the release. This way KDE would be offering an up to date C++ API (for performance) and an interpreted binding to the API (for rapid development). \n\nDon't get me wrong. C++ is my preferred language overall, but for many, many applications I think python is a good enough solution. And code development cycle is waaaaay faster."
    author: "MandrakeUser"
  - subject: "Re: pykde"
    date: 2003-10-23
    body: "100% ACK. "
    author: "Ruediger Knoerig"
  - subject: "KDEDEVELOPERS.ORG"
    date: 2003-10-21
    body: "What is going on with KDE DEVELOPERS.ORG! Its been broken from the very beginning. I tried registering, didn't work for about 5 e-mail addreses. Than I used another method! Finally worked after many headaches. Now I forgot my password and asked for it, after filling out the form correctly and it saying I will soon get a message its been 2 hours and still no message! I requested 2 more times, still nothing!\n\nSomeoen please figure out what's wrong with it, tis driving me crazy! ;<"
    author: "Alex"
  - subject: "probibly..."
    date: 2003-10-21
    body: "...because your email provider dislikes my domain... I get a few bounces now and again, but those are rare, and if someone's ISP feels like blocking my ip range there is nothing i can do.\n\nsorry\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser "
  - subject: "Re: probibly..."
    date: 2003-10-21
    body: "But how, I am registered and I have posted. But its the password retrieval that isn't working.\n\nI also remember right at the beginning of the website that I never received the registration information for 3 accounts I tried to make, one at hotmail, another yahoo, and two at myway.com of which one worked and I am trying to get the password to."
    author: "Alex"
  - subject: "Progress dialog?"
    date: 2003-10-21
    body: "After browsing through the tutorial looking for an example of how to do a progress dialog, I couldn't find it. Neither could I find any information about this using kdialog --help. Is it possible to get a progress bar with kdialog? If so, how? I would like to be able to use one for displaying the progress of big batch jobs."
    author: "M-trix"
  - subject: "Re: Progress dialog?"
    date: 2003-10-21
    body: "Looking at the source code, I don't think that kdialog supports dialogs with a progress bar ATM. \n\nBut it's a good idea so you might want to file a wish for the product 'kdialog' under http://bugs.kde.org.\n\nChristian"
    author: "cloose"
  - subject: "Re: Progress dialog?"
    date: 2003-10-21
    body: "If you are going to file such a bug (probably wishlist), can you explain how you would like it to work? What arguments would it take? What options should be available? \nIn particular, how should the progress be updated?"
    author: "Brad Hards"
  - subject: "Re: Progress dialog?"
    date: 2003-10-21
    body: "\"If you are going to file such a bug (probably wishlist), can you explain how you would like it to work?\"\n\nI've now filed a wish: http://bugs.kde.org/show_bug.cgi?id=66342\n\n\"What arguments would it take?\"\n\nAt a minimum: The initial text to be displayed, and the total number of steps.\n\n\"What options should be available?\"\n\nA added a few suggestions in the wishlist-report, but as usual there's no upper limit :-)\n\n\"In particular, how should the progress be updated?\"\n\nPreferrably using DCOP. That would mean that the progressbar would have to be started in the background.\n"
    author: "M-trix"
  - subject: "Re: Progress dialog?"
    date: 2003-10-21
    body: "That would be a somewhat difficult task, since you probably want the progress dialog to be displayed *while* your script is running. \nThe way kdialog works you call it, it displays a dialog, and then it closes the dialog and returns. For a progress dialog you would need to run in the background. That's not impossible (with a little DCOP), but more complicated than what it does now."
    author: "AC"
  - subject: "Re: Progress dialog?"
    date: 2003-10-24
    body: "man dialog\n/gauge\n\nIt's not that hard."
    author: "Divide"
  - subject: "Xdialog/gdialog/kdialog inconsitence!"
    date: 2003-10-21
    body: "Hallo out there!!\n\nI used Xdialog and cdialog as well for a long time!\n\nBut here is sth. I hate\n\nthere is a big inconsistence between several Dialog menus:\n\nXDIALOG --dirselect is not present in cdialog/kdialog\n\nXDIALOG --menubox/itembox is not present in KDE/Gnome dialog, or just structure inconsistent! - so large scripts got to be ported by changing the whole script(not by changing xdialog to kdialog!!)\n\n\nsooo \n\na wish for Gnome/KDE Dialog coders: Make it consitent to let everybody use dialog-scripts.\n\nAnd even without read DE - Xdialog will do everything - as well as cdialog in terminal mode!\n\n"
    author: "Matthias Fenner"
  - subject: "Re: Xdialog/gdialog/kdialog inconsitence!"
    date: 2003-10-21
    body: "Can you please file a bug for each dialog type that you need?\nIt would help a lot if you can tell us what it should be called, and what options it needs to take."
    author: "Brad Hards"
  - subject: "Re: Xdialog/gdialog/kdialog inconsitence!"
    date: 2003-10-22
    body: "Just look at my qs.sh script!\n\nIt has a Dialog Interface as Main part nad uses the $MODE Variable as dialog selector\n\nMode=\n\nx for Xdialog, k for kdialog, g for gdialog and c for cdialog"
    author: "Matthias Fenner"
  - subject: "Re: Xdialog/gdialog/kdialog inconsitence!"
    date: 2003-10-22
    body: "Sure, but if you want it fixed, file bugs on bugs.kde.org. If you just leave it here, it won't happen. \nA formal bug report isn't that much to ask if you want this fixed..."
    author: "Brad Hards"
  - subject: "Tutorial on how to save and open Data"
    date: 2003-10-23
    body: "Hi,\n\none thing  I really miss as a newbie, is a tutorial about how to save data, for example in an xml document and read it from an xml document. Or how to jump through several records, update a single record aso. If somebody could take the time and write something like this, i will be very happy!\n\nThx!"
    author: "Philipp"
  - subject: "Re: Tutorial on how to save and open Data"
    date: 2003-10-23
    body: "Ironically a DOM tutorial was the next thing I was considering. In the mean time, you can look at http://doc.trolltech.com/3.2/xml-examples.html"
    author: "Brad Hards"
  - subject: "Re: Tutorial on how to save and open Data"
    date: 2003-10-25
    body: "This sounds wonderfull, hope to sea it soon;) But don't hurry, quality not quantity is important;)"
    author: "Philipp"
---
As a newcomer to KDE, I have written up an <a href="http://developer.kde.org/documentation/tutorials/kdialog/t1.html">example-based tutorial on KDialog</a> that you might find useful.  For those of you not familiar with it, KDialog allows one to use KDE dialogs from shell scripts, which has the potential to make things look a lot more professional and well integrated into our favourite desktop environment.  This tutorial was originally going to be part of a KDE developers' book, but has been released while the future of that book is still being decided.  Incidentally, documentation turns out to be a thorough way of exploring an application, and a <a href="http://bugs.kde.org/show_bug.cgi?id=63517">bug</a> or <a href="http://bugs.kde.org/show_bug.cgi?id=62958">two</a> were discovered and fixed along the way!
<!--break-->
