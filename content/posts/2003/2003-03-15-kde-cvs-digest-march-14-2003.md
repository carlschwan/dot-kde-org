---
title: "KDE-CVS-Digest for March 14, 2003"
date:    2003-03-15
authors:
  - "dkite"
slug:    kde-cvs-digest-march-14-2003
comments:
  - subject: "First Derek rocks post!"
    date: 2003-03-15
    body: "hehe\n\nNo kidding ... you rock man ;)"
    author: "Adam Treat"
  - subject: "Re: First Derek rocks post!"
    date: 2004-01-15
    body: "thank you   you rock too"
    author: "derek"
  - subject: "You're having the same bug! Thanx! :\u00b4-)"
    date: 2003-03-15
    body: "Thanks Derek! Thanks to you I know I'm not the only one having this problem: \n\n In the screenshot of the virtual folders (http://members.shaw.ca/dkite/m14vfolder.png) you're having the same problem when using a background image for kicker.\n\nI posted about it in kde-devel... but nobody confirmed this... has anyone submited a bug already? :-)"
    author: "Myself"
  - subject: "Re: You're having the same bug! Thanx! :\u00b4-)"
    date: 2003-03-15
    body: "It was fine until I messed with the configuration.\n\nrm -r ~/.kdecvs\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: You're having the same bug! Thanx! :\u00b4-)"
    date: 2003-03-15
    body: "errr.... any less aggressive option? I wouldn't like to delete the whole .kdecvs... maybe kickerrc, ...?"
    author: "Myself"
  - subject: "Re: You're having the same bug! Thanx! :\u00b4-)"
    date: 2003-03-15
    body: "This has been fixed in CVS last evening (3/14). \n"
    author: "Sad Eagle"
  - subject: "Very nice CVS digest"
    date: 2003-03-15
    body: "Thanks Derek! I do suggest that you take a quick trip to kde-look.org and also your screenshots are alittle messed up at the bottom.\n\nBTW: I'm sorry to say this, but I beleive KDE is starting to move into the wrong direction. KDE is becoming too big, too fast. This is what lead to the doom of many empires. \n\nKDE has thousands of bugs, I really think KDE needs to focus all the developers they can get on the core applications, Konqueror, Kroupware, Kcontrol, internals, improving existing features (ie desktop sahring) and so on. KDe is spreading developers too far and wide, only Koffice has 10 applications each with many developers and even so we already have a good office suite for Linux. I can only imagine how much further KDE could be if the teamw as more focused on DKE alone and not on adding so many applications which often already have better equivalents.\n\nI think focusing on applications more should be the next step after KDE is just the way we want it. \n\n\nI also think  KDE needs to be simplified a little, better organized, and usabiltiy needs to be improved.\n\nI might be wrong, I mean afterall it's a tradeoff.\n"
    author: "mario"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: ">your screenshots are alittle messed up at the bottom.\n\nI'ts not a \"messed up screenshot\" it's a bug... my screen looks exactly the same ;-) \n \n"
    author: "Myself"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: "KDE without application has almost no value. Sure, you can use Oo for your office work but it is bloated like crazy and does not interact with the other component of KDE. That is why a lot of people prefer to use Koffice, even if it lacks (today) a lot of feature. \nWhat we really need today is not anymore more KDE feature but more applications.\nWhen Koffice will work with the Oo file format and will be able to compete feature wise, then, we will have a very consistent desktop. What we need the most today is the Kimp."
    author: "murphy"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: "Or making krita work... it's really broken... has it been abandoned?"
    author: "Myself"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: "Yes, Kimp is badly needed. \nI just refuse to work with the GIMP.\nIts stupid interface is so annoying I can't find the words for it.\nCurrently I use Adobe Photoshop 5.5 on Wine which has\nsome glitches but works good enough to keep me from switching\nto Windows for graphics editing."
    author: "Martin"
  - subject: "Re: Very nice CVS digest(little bit ot)"
    date: 2003-03-15
    body: "For a photoshop replacement, have tried this: http://www.pixel32.sk/\nAtleast it seems very promising, I am planning to purchase one in a near future.\nOh, almost forgot, thanks for the CVS digest, Derek :)\nYou are doing a fine job bringing us the news what new features to expect in the KDE's upcoming releases.\n\n--\nMar 13 1881\nAn anarchist from the radical group People's Will throws a bomb which disrupts Czar Alexander II's motorcade. After he thanks God for his deliverance, the anarchist yells \"It is too early to thank God\" and throws a second bomb, causing injuries from which Alexander bleeds to death.\n\n"
    author: "138"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-16
    body: "> What we need the most today is the Kimp.\n\nThat will be possible with the next major version of Gimp. \n\nIt will actually take less time waiting for that than updating krita to support the feature set of Gimp. Krita, while having periods of extreme development, has been unmaintained (AFAIK) for a while."
    author: "fault"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: ">screenshots are alittle messed up at the bottom.\n\nNope. A bug.\n\n>KDE has thousands of bugs\n\nIndeed. And some very fussy users who report and track them down.\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: "... resulting in very quick bugfixes :) Update kdebase/kicker..."
    author: "Hamish Rodda"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: ">KDE has thousands of bugs\n\nThere is no softwares without bugs...\n\nThe difference is how quickly the problems are solved. In a commercial software, try to see a bug solved in a few hours, like occurs in KDE and other open source projects. You will be putted in a list of priority that depends on the number of \"customers\" that have the same problems, or it will depend on how much bigger is your company.\n\nThank you very much for all developers and contributors. Y do a great job!!!"
    author: "Paulo Junqueira da Costa"
  - subject: "You misunderstand commercial software"
    date: 2003-03-15
    body: "The difference is not just in how fast bugs get solved. It is also in the quality of the code sometimes, how easy it is to read the code, how good are the comments, algorithms, object/class interfaces, documentation, GUI, efficency, etc. Often commercial software has better GUIS and neater code.\n\nCommercial software as much as we would like to believe is not really any more buggier than OSS. Bugs are often solved as fast as in OSS but the patches are better tested and are therefore released later on."
    author: "Mario"
  - subject: "Re: You misunderstand commercial software"
    date: 2003-03-15
    body: "Since you know so much about code, there is another piece of useful information you should know:\n\nAll your talk does nothing.  Show us the code.  That's the only way you can make KDE better."
    author: "anon"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-15
    body: "You can't force developers to work on things they don't want to - in many cases, developers work on an application that they need, adding features that they want.\n\nIf we do what you suggested, developers would simply take the applications out of KDE and work on them independently, losing the ability to coordinate releases with KDE itself. The end result of that would be applications that aren't as good and don't make use of new features in KDE, whilst providing no overall benefit to the rest of KDE.\n\nFor example, I don't have any problems with KDE itself, but I do need a quality mp3 library organiser / player, so I've contributed to (and will continue to contribute to) to JuK (going to be new in KDE 3.2). I don't use desktop sharing, koffice or kroupware, to quote your examples, so I'm not going to work on them."
    author: "GldnBlls"
  - subject: "Re: Very nice CVS digest"
    date: 2003-03-17
    body: "I have a simple reply for bugs and application development... Check out Quanta!\nhttp://bugs.kde.org/reports.cgi Select Quanta. We don't even show in the top 100 applications now. I salute Andras Mantia for his great work here!\n\nEric Laffoon\nQuanta Project Leader\nMailing list - http://mail.kde.org/mailman/listinfo/quanta"
    author: "Eric Laffoon"
  - subject: "Many features already available in KDE 3.1"
    date: 2003-03-15
    body: "> There are a few new features implemented, such as middle click to load page in new tab (configurable)\n\nThat's already implemented and working in KDE 3.1.\n\n> a new tab and close tab button\n\nWhere did you see them? The new tab widget is not committed yet afaik.\n\n> The panel has two enhancements that I will use. One is a Control Center icon that gives a list of all the modules.\n\nNot new in KDE 3.2: Panel Menu/Add/Special Button/Preferences\n\n> The second one gives the option To Current Desktop on grouped applications.\n\nToo a feature existing in KDE 3.1 already."
    author: "Anonymous"
  - subject: "Re: Many features already available in KDE 3.1"
    date: 2003-03-15
    body: "> a new tab and close tab button\n \n> Where did you see them? The new tab widget is not committed yet afaik.\n\nIn one of the extra toolbars. This one may be in 3.1 too, I'm not sure.\n\n > The second one gives the option To Current Desktop on grouped applications.\n \n > Too a feature existing in KDE 3.1 already.\n\nDon't think so.\n\nDerek\n \n"
    author: "Derek Kite"
  - subject: "Re: Many features already available in KDE 3.1"
    date: 2003-03-15
    body: ">Don't think so.\n\nDoing it right now (in KDE 3.1)...\nSorry, this time you're wrong. Still: I think you're doing a great job with your CVS updates, I always look forward to them!\n\nAndr\u00e9"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Many features already available in KDE 3.1"
    date: 2003-03-15
    body: ">Sorry, this time you're wrong.\n\nIndeed I am. Funny, I needed that functionality and couldn't find it.\n\nOh well.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Weekly View of KDE CVS commits"
    date: 2003-03-15
    body: "Sent this to Derek:\n\nHi Derek,\n\nI just read your latest cvs digest and I wanted to clear up some confusion with regards to some features I've committed that you comment on.\n\nFirstly KMail \"virtual folders\". I prefer the term 'Search folders' instead of 'virtual folders'. Virtual folders is I believe a Ximian Evolution term, but the basic idea is similar to KMail Search folders or Outlook views. I think search folder describes what the technology does better than virtual folder. But perhaps 'Views' would be an even better name as this would be consistent with KAddressBook and Outlook views.\n\nYou state: \"Also, if I had multiple virtual folders, when I checked mail, new messages would be added to some folders without any rhyme or reason.\" Please understand search folders are dynamically updated when new mail is discovered, so any new mail that meets the criteria of the search associated with the Search Folder will appear in the search folder. This is really the fundamental idea behind search folders.\n\nYou state \"Virtual folders are a useful tool for separating out a group of similar emails to work with. For this feature to be useful, it must be fast.\" you might be interested in the full text indexer that has been implemented for KMail. This is currently disabled by default, and is a work in progress, but when enabled speeds up searches immensely.\n\nIf people are interested in a stable version of KMail with the new search folder , as-you-type spell checking, KPartification so that it can be used in the kontact container, and many other features they can pick up a stable but feature-incomplete version from http://kontact.org. But I haven't really formally announced this site yet, as I'm only interested in feedback from dedicated users who are interested in helping debug any problems they encounter.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Weekly View of KDE CVS commits"
    date: 2003-03-15
    body: "> You state: \"Also, if I had multiple virtual folders, when I checked mail, new messages would be added to some folders without any rhyme or reason.\" Please understand search folders are dynamically updated when new mail is discovered, so any new mail that meets the criteria of the search associated with the Search Folder will appear in the search folder. This is really the fundamental idea behind search folders.\n \nThere is a bug here. I had search folders defined for example kdebase, kdepim, kdelibs, kdenetwork, etc. All the cvs directories. Say kdenetwork was defined last. On checking mail, the new messages that matched the kdenetwork query were added to kdenetwork, _and_ kdelibs,kdepim etc. Not all folders, but most. I didn't have time to confirm exactly what happens. I will however, and I'll file a bug.\n\nThanks for the work. There is no criticism meant by the comments. I wanted to give an idea how things are progressing. There is obviously lots of work to be done yet before 3.2 is ready.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Weekly View of KDE CVS commits"
    date: 2003-03-15
    body: "Okay please double check that what you're experiencing isn't expected behavior. I'm not aware of any outstanding bugs with the search folder implementation.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Weekly View of KDE CVS commits"
    date: 2003-03-15
    body: "and also another thing to check for is to make sure that the integrity of KMail files haven't been compromised due to using a buggy cvs version.\n\nMaybe best to remove all your search folders and recreate them if you're experiencing problems. I myself, and I believe several other KMail developers use search folders to 'filter' cvs mails for a particular app, so this is a pretty standard usage of search folders.\n"
    author: "Don"
  - subject: "Re: Weekly View of KDE CVS commits"
    date: 2003-03-15
    body: ">use search folders to 'filter' cvs mails for a particular app\n\nI was experimenting to see if this would work. Nice feature btw. My filter list in 3.1 is large and hard to manage.\n\nTwo things I was finding I wanted to do. One is edit / view an existing query, second is to query from date-time forward or date-time to date-time. There is an option of number of days, but not fine enough.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Weekly View of KDE CVS commits"
    date: 2003-03-15
    body: "> Two things I was finding I wanted to do. One is edit / view an existing query,\n\nYes I find myself wanting to do this also :) My plan is to implement searching a search folder by having this operation show the search for that search folder.\n\n> second is to query from date-time forward or date-time to date-time. There is an\n> option of number of days, but not fine enough.\n\nOk, this is something for me to think about.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Weekly View of KDE CVS commits"
    date: 2003-03-15
    body: "I haven't filed a bug yet. This weekend.\n\nThanks again. Excellent work.\n\nDerek"
    author: "Derek Kite"
  - subject: "Something i'd love to see in KDE 3.2 (or later)"
    date: 2003-03-15
    body: "Is incorporating the k3b cd burning software.  It's great stuff, complies with the KDE \"look and feel\" and would make a great addition to the KDE apps.  Or is there some backend being worked on to allow KDE to interface with CD-Burners without the need for other applications (i.e. when M$ did that with windows XP) ?"
    author: "standsolid"
  - subject: "Re: Something i'd love to see in KDE 3.2 (or later)"
    date: 2003-03-15
    body: "why we have like 3 of them in kde 3.1!\n\ntry kde-extragear\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "bug graph"
    date: 2003-03-15
    body: "Hi Derek, just curious, is there a more recent version of this graph:\nhttp://bugs.kde.org/reports.cgi?product=-All-&output=show_chart&datasets=NEW\nthat Stephan Kulow has posted on your previous cvs digest?\n\nHow often it is being regenerated, and if it's often can you provide url for most recent version?\n\nYou are doing a good work. Thanks"
    author: "Anton Velev"
  - subject: "Re: bug graph"
    date: 2003-03-15
    body: "It's created on requet by quering bugzilla at http://bugs.kde.org/reports.cgi For getting a graphical graph be sure choosing \"Bug Charts\" as \"Output:\"."
    author: "Datschge"
  - subject: "Re: bug graph"
    date: 2003-03-15
    body: ">How often it is being regenerated,\n\nI would think it's up to date. The command runs a query.\n\nDerek"
    author: "Derek Kite"
  - subject: "I WAS MISUNDERSTOOD"
    date: 2003-03-15
    body: "Alright, many of you misundeerstood, espcially the screenshot cmment> I KNOW IT'S A BUG! A bug which leads to messed up screenshots.\n\nIn adition, I know KDE has little value without apps, but it IMO is not the job of the KDE team to make KDEapps, that should be other developers. The Kteam should focus on KDE itself, optimizations, feature enchancements, usability, look n feel etc. of only core KDE applications. There shouldn't be 5 new applications with each release, if we keep this up we'll have 3-4 developers one ach application and not even a single application truly mature. \n\nIn adddition, a great pluarality preffers OO to Koffice, for good reasons. Placing a Qt inreface on every GTK application we find is a TOTAL WASTE of development. Sooner or later the GUI makeover Qt app will lag behind the original more and more, just look at Kpovray and so many others. IMO KGimp would be the biggest waste of time, GIMP is getting easier to use and 1.4 will be very nice.\nhttp://tigert.gimp.org/screenshot.png\nSeriously, don't reinvent the wheel, and GIMP's interface takes some time to get used to, but it is even more effective than normal interfaces.\n\n\nPLEASE STOP THE BLOAT, KDE DEVELOPERS SHOULD FOCUS ON KDE ALONE AND NOT ALL QT LINUX APS, OR THERE WILL NEVER BE A TRUE QT COMMUNITY FOR LINUX OUTSIE THE KDE PROJECT!"
    author: "Mario"
  - subject: "YOU DON'T UNDERSTAND"
    date: 2003-03-15
    body: "Maybe you should shut up, sit down and start contributing instead.  What is your bitching contributing?  If everybody did like you there would be more bitchers than users!\n\nYou don't like a KDE application?  Fix it, or don't use it.  So get off your butt and start writing bug reports and/or contributing bug fixes.\n\nTHAT WAY YOU CAN REALLY CHANGE SOMETHING."
    author: "anon"
  - subject: "Re: I WAS MISUNDERSTOOD"
    date: 2003-03-15
    body: "I don't think you were misunderstood, rather that others, me included, respectfully disagree.\n\nOver the last few months, there have been a number of applications added to the repository. Not necessarily the distribution tarballs however.\n\nWhat I have seen in the commit logs is an exchange of expertise within the kde development community. So applications are improved.\n\nThere are three cd writing apps in the repository. Two are actively developed. Which one should drop his ideas to add to the other? I would say neither. If an application isn't maintained, it is removed.\n\nRegarding Koffice, there is a 'law' in free software. It must reach a level of completeness before it attracts users / developers that will push development further. Koffice is in the limbo of nearly there, but not quite. In the meantime, the core developers work away at it.\n\nFree software means freedom for the developers too.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: I WAS MISUNDERSTOOD"
    date: 2003-03-15
    body: "> A bug which leads to messed up screenshots.\n\nWith all the respect: You're wrong again, why don't you verify things before behaving like that? People like you are the ones that fill the bug list with nonsense bugs.\n\nit's a kicker bug, which has been fixed just today. Nothing to do with the \"screenshoting process\"\n http://lists.kde.org/?l=kde-cvs&m=104769588424502&w=2\n\n> is not the job of the KDE team to make KDEapps,\n\nYou get it wrong *again*. It's not the kde team that develops applications. The fact is that people that develop applications first, join the kde team later. Same amount of people, same amount of apps. What's wrong?\n\nAt this moment I haven't joined the kde team, but I'd like to. I have done a few applications, one really usable. At some point *if* I have the time and I'm allowed I'd like to join them. That wouldn't affect to the quality of KDE, since there's one app more, but there would be one person more also.\n\n>In adddition, a great pluarality preffers OO to Koffice, for good reasons.\n\nYou're right, but the same great plurality preffers to be able to use Koffice if possible, because 1GHz/256Mb doesn't seem to be enough for OO. Whenever I need to write a small letter you won't see me opening OO... not even if I was paid 20 quid for it\n\n> Placing a Qt inreface on every GTK application we find is a TOTAL WASTE of development.\n\nI completely agree. That's why it's more interesting to develop a new set of programs, especially when there's support from governments and companies.\n"
    author: "Myself"
  - subject: "Okay, I get it"
    date: 2003-03-15
    body: "All I was saying is that I think that if the KDE developers  focus too much on applications the desktop will be lagging behind, and as it is now it is mature, but still has a  way to go there are many bugs (as in every project) and there are still a lot of things which can be improved and added to the desktop itself.\n\nI just don't want the KDE team to lose focus, and leave the desktop in the dust, I would rather have the best DE than the best apps. if I have the best DE, than most people would feel at home in it. Soon companies would see the very nice friendly, feature rich desktop and think this looks like the best desktop, but it is lacking in high quality applications, and slowly more and more commercial applications would find their way into Linux. In a short period of time, Linux would have as many commercial apps as MacOS, as the # of apps grow i think the # of users will too. If the KDE team goes in the other direction and brings a mediocre desktop coupled with commercial quality applications, than not only users would not feel extremely compelled to switch, but companies might be too scared to enter a market in which they have FREE competition. This is the way Netscape was killed after all. The alternate future would be that Linux will still become more and more popular, but not even at 1/10 of the rate.\n\nI'm just speculating, I have no idea what the future will be for sure.\n\nBTW: OpenOffice is only slow to startup; it feels fine when actually running.  \n"
    author: "Mario"
  - subject: "Re: Okay, I get it"
    date: 2003-03-16
    body: "So you're saying that we shouldn't let people work on quality, free applications for KDE, we should force everybody to work on the desktop?\n\nAnd why? So that we can get some quality commercial applications... wow, what a great goal for a free (in both senses) desktop.\n\nPut it this way, I'm a developer, and I've just started contributing to a KDE application because I want to add features to it. However, if somebody like you were to turn round and say I couldn't do that, that I had to work on the desktop, I wouldn't - because hacking on the desktop doesn't interest me personally. I suspect a lot of the more-experienced developers would say the same thing.\n\nBasically, we're all just scratching itches, but we all have itches in different places."
    author: "GldnBlls"
  - subject: "Re: Okay, I get it"
    date: 2003-03-17
    body: "You really need to reread my post. You obviously didn't understand half of it. I never said developers should be forced, I just thought it would be a good idea to do the above. \n\nThe ultimate goal was not too bring commercial applications to Linux, it was to gain widespread support as a desktop. It is much more complicated than it looks and than I stated in my post. However, commercial apps are very important.\n\nAS I mentioned I was just speculating and that was in m yopinion, please calm down anre reread my post."
    author: "mario"
  - subject: "Re: Okay, I get it"
    date: 2003-03-17
    body: "You really need to reread my post. You obviously didn't understand half of it. I never said developers should be forced, I just thought it would be a good idea to do the above. \n\nThe ultimate goal was not too bring commercial applications to Linux, it was to gain widespread support as a desktop. It is much more complicated than it looks and than I stated in my post. However, commercial apps are very important.\n\nAS I mentioned I was just speculating and that was in m yopinion, please calm down anre reread my post."
    author: "mario"
  - subject: "Re: Okay, I get it"
    date: 2003-03-17
    body: "I am calm, I just resent the implication that you or anybody should be telling me and others what I should and shouldn't be doing in my own time.\n\nI think you're the one who needs to down a touch, to be honest. I read your post, and I understood it. I just happen to disagree with it. Why is it that you accuse anybody who disagrees with you of not understanding what you posted? Maybe you don't understand our replies?"
    author: "GldnBlls"
  - subject: "BECAUSE YOU DIDN'T UNDERSTAND"
    date: 2003-03-17
    body: "You said that you wouldn't want to be FORCED to work on the desktop and I did not mention anyone should be forced. There were also other indications I will not waste my time mentioning.\n\nI also resent your arrogance, believe it or not, the developer is not always right and sometiems it is good to listen to the other end of the line. I think you have a horrible attitude, I was not telling you what you should or should not do with YOUR time. I was simply suggesting the path which as I mentioned often was in my opinion the one which would bring linux closest to average Joe's desktop.\n\nIf you disagreed with my post you could have just said so calmly and respectuflly while explaining your position. There was no need to take offense and act up as if you just got stabbed in the heart. IT WAS A SUGGESTION, PEOPLE HAVE DIFFRENT OPINIONS, LIVE WITH IT!"
    author: "mario"
  - subject: "Re: BECAUSE YOU DIDN'T UNDERSTAND"
    date: 2003-03-17
    body: "Hello Mario,\n\nfrom the principle I agree with GldnBlls, as his reaction is the same as I have.\nPlease understand, that we free software developers see so often people telling us that we should focus on things the writer thinks would be better because of some reasons.\nWhen you read such things too often you get angry and emotional, because you feel presonally offended.\nYou wrote \"The Kteam should focus...\" and GldnBlls and myself is part of the KDE team, so you write to us that we should focus, means we should switch what we are working on, means that what makes fun is not what you want to see, means what we are currently doing is rubbish. See? This is offending us and every OS dev.\nAnd if I see it 20 times, suddenly I write a reply too, mostly then emotional. Not really good for me, not good for KDE and not for the whole community, but brings my emotion down ;-).\nSo just accept, that what OS devs are doing is fun, cannot be managed and is puerely bazard style (ever tried to go to a merchand on a bazard and told him he/she should sell different stuff, because of blablabla?).\n\nFor me personally, your issue with commercial apps is okay, but I don't care. If I would care about commercial stuff, I would work for money. I work because I want to have my application on my desktop. If others like it as well, they can have my code, if they don't like it, fine too.\n\nAnd I find KDE good enough for my desktop, I don't need much more functionality.\n\nFinally, I know how to use a spreadsheet but don't have any knowledge about how to program a browser, a window manager, a theme and so on.\n\nPhilipp"
    author: "Philipp"
  - subject: "I didn't mean it that way"
    date: 2003-03-18
    body: "I think the KDE team is doing a  great job and KDE is my favorite desktop closely followed by GNOME 2.2.1. Its just that I see so many new apps in KDE and so many origianl apps improved so fast, I'm (don't know if this is true) afraid people will forget about KDE as a desktop and just work on apps.\n\nSorry if I offended anyone =("
    author: "mario"
  - subject: "Re: I didn't mean it that way"
    date: 2003-03-18
    body: "Nah, just join and help, and all is forgiven.  It's good to have an opinion if you can do something about it.  :-)\n\nGood constructive bug reports is one way to start."
    author: "anon"
  - subject: "what is kmail cool?"
    date: 2003-03-16
    body: "hi!\n\ncan someone please tell me what kmail-cool is? I've heard of the make_it_cool brach of kmail, but this is something different, right?\n\nA fork? Kmail with groupware features? What are the differences? Will they be merged back together somewhen?\n\nthanks!"
    author: "me"
  - subject: "Re: what is kmail cool?"
    date: 2003-03-16
    body: "> can someone please tell me what kmail-cool is?\n\nA KMail fork.\n\n> I've heard of the make_it_cool brach of kmail, but this is something different, right?\n\nNo, it's the same.\n\n> A fork?\n\nYes.\n\n> Kmail with groupware features?\n\nPossibly.\n\n> What are the differences?\n\nDifferent focus.\n\n> Will they be merged back together somewhen?\n\nThat's in progress right now."
    author: "Datschge"
  - subject: "Re: what is kmail cool?"
    date: 2003-03-16
    body: "Pretty much all of KMail Cool is merged back into HEAD now. Everything accept the BBDB functionality.\n"
    author: "Don"
  - subject: "Re: what is kmail cool?"
    date: 2003-03-16
    body: "Cool. ;) \n\nPS: BBDB (which I heard the first time now) is mentioned at http://trolls.troll.no/sanders/kmailcool/plannedfeatures.html referring to http://bbdb.sourceforge.net/. To me it looks like it would be like it could be realized within KDE as a hybrid between Kexi and KAddressbook, not too shabby. =)"
    author: "Datschge"
  - subject: "Re: what is kmail cool?"
    date: 2003-03-17
    body: "I hadn't considered Kexi. BBDB functionality has already been implemented in the make_it_cool branch, but there are scalability issues that make me think it's a good idea to use a conventional database for storing records in. I was considering embedded mysql but perhaps Kexi would be a better choice.\n\nThanks for the suggestion, I'll consider it further.\n\nDon.\n"
    author: "Don"
  - subject: "Re: what is kmail cool?"
    date: 2003-03-17
    body: "Heh, thanks for picking that up. =)"
    author: "Datschge"
  - subject: "Re: what is kmail cool?"
    date: 2003-03-18
    body: "Why not sqlite?\n\nI wrote a Qt driver for it, and I have heard it works :-)\n\nIt can handle way more than BBDB can throw at it, and is small enough you could even bundle it with KMail and noone would notice."
    author: "Roberto Alsina"
  - subject: "Sound previews"
    date: 2003-03-16
    body: "   Whats this about sound previews ? They've been working prfectly for me ever since they were introduced . . .\n\n   Any chance that they'll reappear, or do we have to wait for Arts' successor"
    author: "hmmm"
  - subject: "Re: Sound previews"
    date: 2003-03-16
    body: "They're disabled by default because they break a rediculously large number of users' desktop entirely.  This is not acceptable.  If someone fixes this stuff, and makes them work well enough that they don't end up in cases where the sound doesn't stop playing, then maybe they'll be on by default again one day."
    author: "George Staikos"
  - subject: "Re: Sound previews"
    date: 2003-03-16
    body: "   Ah. They're only disabled by default. I though the option had been removed altogether. . . \n\n   Thanks for the fantastic desktop. \n\n   I'm a graet user of LaTeX -- and I must say fish:// + embedded console make kate an absolute winner. Despite the lingering linebreaking bugs. . .  The filepicker is handy too.\n\n   And KDVI is in fact a very good dvi viewer -- more resilient to wierd orientation than xdvi. The only thing I miss is the magnifiying glass :)\n"
    author: "hmmm"
  - subject: "Re: Sound previews"
    date: 2003-03-17
    body: "I liked the feature and it worked well for me.\nCan you (or someone else) tell how to enable sound preview again please?\n\nTIA\n"
    author: "Sangohn Christian"
  - subject: "Re: Sound previews"
    date: 2003-03-17
    body: "View -> Preview -> Sound Files\n"
    author: "Sad Eagle"
  - subject: "Konqueror?"
    date: 2003-03-17
    body: "I have a request for the next KDE CVS digest. Like many other users, I'm very interested in the current state of Konqueror as a browser. Sure, we see all those Safari merges, but most look like framework fixes, while I don't see any of the rendering engine stuff coder David Hyatt (http://www.mozillazine.org/weblogs/hyatt/) mentions in his blog. Now, when he talks about fixing the rendering of margins at the bottom of <div> block elements or adding some support for CSS3 selectors, that's something I can understand. I understand how it will affect my daily surfing - I want to know if and when it turns up in Konqueror, 'cause I don't own a Mac, I use KDE.\n\nI think it's time for a little overview article. Now, I don't think the devs want to talk about how the relationship with Apple is going, that's probably a bit too delicate and political for the weekly digest, but some information on how we're standing after all these weeks, what is going to be merged and what is not, and maybe even a little roadmap - that'd be really *great*. :)"
    author: "Eike Hein"
  - subject: "Re: Konqueror?"
    date: 2003-03-19
    body: "It seems like there's now a private mailing list just for khtml and kjs for all the cooperation stuff. There are only seldomly khtml and kjs related discussions on kfm-devel nowadays, so what we need is someone who's member of that khtml and kjs mailing list and willing to report about interesting changes to khtml and kjs. Maybe Derek can take part there as an observer? Would be nice. =)"
    author: "Datschge"
  - subject: "A good K(de)H(arddisk)rec(order) ?!?!?"
    date: 2003-03-19
    body: "Hi everyone!\n\nI know this doesn't belong here BUT maybe someone has any idea what happend with this project?\n\nhttp://verona.phys.chemie.tu-muenchen.de/people/lorenz/khdrec/khdrec.html\n\n/Dave"
    author: "Dave"
---
<a href="http://members.shaw.ca/dkite/mar142003.html">This week in KDE-CVS-Digest</a>, our big feature is a review of the status of the KDE HEAD branch.  We cover everything from <a href="http://members.shaw.ca/dkite/m14vfolder.png">virtual folders</a> in <a href="http://kmail.kde.org/">KMail</a> to <a href="http://www.konqueror.org/">Konqueror</a>,  <a href="http://www.kontact.org/">Kontact</a> and more.  Plus, as usual we cover the latest KDE development updates and bugfixes.
<!--break-->
