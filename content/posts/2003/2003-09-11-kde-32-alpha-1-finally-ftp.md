---
title: "KDE 3.2 Alpha 1 Finally on FTP"
date:    2003-09-11
authors:
  - "skulow"
slug:    kde-32-alpha-1-finally-ftp
comments:
  - subject: "Thanks"
    date: 2003-09-10
    body: "Thanks for making this release Stephan, we'll stop bugging you now. (well for a day or two...).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Context menus No2"
    date: 2003-09-10
    body: "Does anyone know if the context menus have being cleaned up in this release?\n\nHere is one of the references http://dot.kde.org/1062916070/1062922457/ regarding the issue, which is going on for too long IMHO."
    author: "JigSaw"
  - subject: "Re: Context menus No2"
    date: 2003-09-10
    body: "Why don't you install the Alpha and see yourself? They partly have been."
    author: "Anonymous"
  - subject: "Re: Context menus No2"
    date: 2003-09-10
    body: "Because I have an old AMD K6 400 Mhz, and it will take, literally, days to compile the whole KDE and the other Qt/kde apps that might need recompilation because of the upgrade. I will have to wait for binaries at some point, or for a distro in the future to include it by default."
    author: "JigSaw"
  - subject: "Re: Context menus No2"
    date: 2003-09-11
    body: "From discussions on the usability list, I'd say that they have been cleaned up a lot, but that there are also lots of contentious items left in the menus, and lots of conflicting ideas as to what should appear and what shouldn't. Hopefully all major issues will be resolved by 3.2 final.\n\nIf you have the time, consider subscribing to the usability mailing list and posting your suggestions. Checking CVS HEAD or 3.1 alpha 1 versions make things easier, as does checking the archives, just to make sure you're not suggesting something that has already been discussed/fixed."
    author: "Tom"
  - subject: "speaking of broken kde"
    date: 2003-09-11
    body: "I remember a kde traffic announcing a knoppix based on kde CVS project. is it dead or what?"
    author: "pat"
  - subject: "Re: speaking of broken kde"
    date: 2003-09-11
    body: "http://dot.kde.org/1063132013/1063154101/"
    author: "Anonymous"
  - subject: "Re: speaking of broken kde"
    date: 2003-09-11
    body: "thanx a lot ! :)"
    author: "pat"
  - subject: "Apple Code"
    date: 2003-09-11
    body: "Does Apples KHTML and KJSCRIPT enhancements get included in 3.2 or will it be 3.3 before they are included."
    author: "Roberto J. Dohnert"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "Much of the original changes have already been merged in, but Apple has made many more changes before Safari 1.0 hit (and no doubt, they are still making changes as we speak), some of which has not been merged in. "
    author: "anon"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "Is it a high priority to integrate Apple's code? It really should be, but doesn't sound like there's a great deal of emphasis on it. The most exciting thing about 3.2 is definately going to be seeing what Konqueror is like. I'm not going to try any releases until 3.2 final comes out so hopefully I'll get blown away!"
    author: "foo"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "I think you'll be impressed. Right now it is fast, renders almost all pages well. It is a little unstable at this moment, but will get better at release.\n\nThe only time I fire up mozilla now is when the cvs version is broken. It works for all that I use it for, banking, browsing, kmplayer plugin, etc.\n\nDerek"
    author: "Derek Kite"
  - subject: "konqueror renders multiple times?"
    date: 2003-09-11
    body: "what is the reason that konqueror seems to render pages in multiple stages?\nEspecially opening huge sites you can see some reformattings (fonts,tables) which isn't very nice. Gecko-based browsers have solved this issue, Opera does it less often.\nDoes anybody know the reasons of this annoying effect and if it is been worked on?\n\ngreetings,\n  katakombi >8^)"
    author: "katakombi"
  - subject: "Re: konqueror renders multiple times?"
    date: 2003-09-12
    body: "It's useful for slow internet connection, you can start\nto read text before of loading all html/images.\n\nKonq is really good here.\n"
    author: "Dmitry"
  - subject: "Please don't remove that!"
    date: 2003-09-13
    body: "It's a life saver on dialup."
    author: "Anonymous"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "I have a kinda silly question. The website www.newz.dk has been broken in konqueror since they changed their layout, but works in all the other browsers I have tested. So can anyone tell me if this is a bug in konqueror or in the website itself and if I should repport it at bugs.kde.org (searching for similar bugs turned out too many results for me to wade through)? "
    author: "Mikkel Schubert"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "How is it broken? It looks to mee as if it renders just fine, but then again I have no clue what so ever on how it really should look:-)\nBut as it looks, it looks as if it is rendered correctly. Nice site, think I have to check it out some more.\nHere at CBS we're using SiteScape for our e-Learning system, and THAT is rendered completely wrong. All menues (javascript) are not displayed.\nThere is a TreeView where you can gain quick access to forums teams and such this tree is not displayed at all, and many more things.\nI like that konqueror is quickly loaded when you want to access a web site and I like that it is so integrated into KDE, but I really think that there is just too many things that konqueror cannot do.\nTo me it is not usable at all. I just use it to quickly look up some page, but in dayly usage at work I dont use it. Even though I would like to. I have also tried this new release, and I really cannot see any real changes to the items I think is missing/not working."
    author: "Jarl E. Gjessing"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "I've had similar experiences with Lotus Quickplace and Konqueror. But anyway, it says that Internet Explorer is needed. Most (commercial) web-based knowledge management systems I know make extensive use of IEs capabilities. No way to get decent results with any OS browser at all."
    author: "Thomas"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "If you read the user comments you will see that the positioning is a little off.  I am testing this with KDE 3.1.3.\n\n\nsmeat!"
    author: "SMEAT!"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "A little off? =)\nThis is what I get using a cvs build a couple of days old:\n Konqueror: http://vazagi.homepage.dk/screen2.jpg\n Opera: http://vazagi.homepage.dk/screen1.jpg"
    author: "Mikkel Schubert"
  - subject: "Re: Apple Code"
    date: 2003-09-11
    body: "If kde people want to hack their browser a bit, just try to get http://www.geek.com/ working!\n\nKinda blank page atleast on konqueror ;-)...\n"
    author: "Nobody"
  - subject: "Re: Apple Code"
    date: 2003-09-12
    body: "Oh my god the creators of that site should be hauled of and SHOT for such crappy coding. Look at the page source. It is using a lot of javascript document.write stuff and it checks for browser ident. If you tell konq to identify itself as ie, ns4 and some others it works just not in its default config. I would be hard pressed to find crappier code then that. Maybe not rendering that site should be a feature. :)"
    author: "kosh"
  - subject: "Re: Apple Code"
    date: 2003-09-12
    body: "or http://www.ajmao.org :)"
    author: "Nobody"
  - subject: "Re: Apple Code"
    date: 2003-09-12
    body: "It looks fine in my konq. Just as in your Opera snapshot!\nStrange"
    author: "Jarl E. Gjessing"
  - subject: "Re: Apple Code"
    date: 2003-09-14
    body: "It renders incorrectly for me in CVS HEAD from 2 days ago.\n\nWhich version are you using? 3.1x? (if this is the case, then this might be a regression)"
    author: "Troels Tolstrup"
  - subject: "Re: Apple Code"
    date: 2003-09-14
    body: "I think i isolated the problem, and have filed a bug report:\n\nhttp://bugs.kde.org/show_bug.cgi?id=64286"
    author: "Troels Tolstrup"
  - subject: "What's in a name?"
    date: 2003-09-11
    body: "Shouldn't the codename be \"brockenboring\", with a C? Without it it gives a different impression :-/"
    author: "ac"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "What is brockenboring anyway?  Someone clue me in!"
    author: "Navindra Umanee"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "Lord of the Rings I believe."
    author: "Chris Howells"
  - subject: "Re: What's in a name?"
    date: 2003-09-12
    body: "Here's the thread:\nhttp://lists.kde.org/?l=kde-core-devel&m=105661334918305&w=2\n"
    author: "cm"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "I think that coolo once mentioned that \"brockenborin\" is a city in GB, not sure though."
    author: "MK"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "This is a definitive list of all cities in Great Britain:\n\nAberdeen, Aberdeenshire\nBangor, Caernarfonshire\nBath, Somerset\nBirmingham, Warwickshire\nBradford, Yorkshire\nBrighton & Hove, Sussex\nBristol, Gloucestershire\nCambridge, Cambridgeshire\nCanterbury, Kent\nCardiff, Glamorganshire\nCarlisle, Cumberland\nChester, Cheshire\nChichester, Sussex\nCoventry, Warwickshire\nDerby, Derbyshire\nDundee, Angus\nDurham, County Durham\nEdinburgh, Midlothian\nEly, Cambridgeshire\nExeter, Devon\nGlasgow, Lanarkshire\nGloucester, Gloucestershire\nHereford, Herefordshire\nInverness, Invernessshire\nKingston-upon-Hull, Yorkshire\nLancaster, Lancashire\nLeeds, Yorkshire\nLeicester, Leicestershire\nLichfield, Staffordshire\nLincoln, Lincolnshire\nLiverpool, Lancashire\nLondon, Middlesex\nManchester, Lancashire\nNewcastle-upon-Tyne, Northumberland\nNewport, Monmouthshire\nNorwich, Norfolk\nNottingham, Nottinghamshire\nOxford, Oxfordshire\nPeterborough, Northamptonshire\nPlymouth, Devon\nPortsmouth, Hampshire\nPreston, Lancashire\nRipon, Yorkshire\nSalford, Lancashire\nSalisbury, Wiltshire\nSheffield, Yorkshire\nSouthampton, Hampshire\nSt Albans, Hertfordshire\nSt David's, Pembrokeshire\nStirling, Stirlingshire\nStoke-on-Trent, Staffordshire\nSunderland, County Durham\nSwansea, Glamorganshire\nTruro, Cornwall\nWakefield, Yorkshire\nWells, Somerset\nWestminster, Middlesex\nWinchester, Hampshire\nWolverhampton, Staffordshire\nWorcester, Worcestershire\nYork, Yorkshire\n"
    author: "Anonymous Coward"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "Unfortunately that list puts lots of cities in the wrong counties. So it's not really definitive. eg. Newcastle-upon-tyne is in Tyne and Wear not Northumberland, Manchester is a county in itself (Greater Manchester) etc.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "And where is Belfast? Isn't that in GB?"
    author: "Jan"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "I believe that Northern Ireland doesn't count as GB, even though it's in the UK."
    author: "anonymous bastard"
  - subject: "Re: What's in a name? \n"
    date: 2003-09-12
    body: "Quoting from my passport, \"United Kingdom of Great Britain and Northern Ireland\".\n\nSo Belfast is in Northern Ireland, while Wales, Scotland and England are in Great Britain. But all of them are in the United Kingdom. Nice and simple isn't it :)"
    author: "Chris Howells"
  - subject: "Re: What's in a name? "
    date: 2003-09-13
    body: "It's pretty simple. Great Britain is the largest island of the British Isles ... so Scotland, England and Wales are in (on?) Great Britain. So the countries of Great Britain, and the province of Northern Ireland together are known as the United Kingdom. Just remember that Great Britain is an island, and used as the collective name for the countries within it, and it all makes sense!"
    author: "Ed Moyse"
  - subject: "Re: What's in a name?"
    date: 2003-10-02
    body: "You are mixing up administrative areas with counties.\nNorthumberland, Lancashire, etc have existed for close on a thousand years.\nThen in 1888 administrative areas (or county councils) were formed that shared the same boundaries as the counties. It's these administrative areas that have been messed around with, not the ancient counties they were based on. In the case of the two administrative areas you speak of 'Tyne & Wear' and 'Greater Manchester' were created in 1974 and abolished in 1985. Eleven years of existance of a council area does not change close to a thousand years of where a place is!!!\n\nThere is a lot more info at http://www.abcounties.co.uk\n\n"
    author: "Anonymous Coward"
  - subject: "Re: What's in a name?"
    date: 2003-11-24
    body: "Peterborough is in Cambridgeshire"
    author: "Jon"
  - subject: "Re: What's in a name?"
    date: 2003-11-26
    body: "No, it's not: geographically, Peterborough is in the county of Northamptonshire (with the exception of those suburbs situated south of the Nene, which are in the county of Huntingdonshire); always has been (well, since AD 1011, anyway), always will be.\n\nWhat is true, though, is that Peterborough has been in a number of different *administrative* areas since formal local government was created in 1888. However, administrative areas (including \"administrative counties\") are NOT the same as counties. Here is a potted history of Peterborough's top-tier local government:\n\n&#9658;1889-1965: Peterborough was in the administrative county of the Soke of Peterborough (described in the Local Government Act 1888 as a division of the geographical county of Northamptonshire).\n\n&#9658;1965-1974: Soke of Peterborough CC was merged with Huntingdonshire CC (as both authorities were deemed too small for local government purposes) to create Huntingdon & Peterborough CC. \n\n&#9658;1974-1998: Huntingdon & Peterborough CC was merged with Cambridgeshire & Isle of Ely CC to form \"Cambridgeshire\" CC.\n\n&#9658;1998-    : Peterborough City Council was made a unitary authority, independent of Cambridgeshire CC.\n\nAs you can see, residents of Peterborough have been subject to rather confusing local government arrangements over the years, with three major local government changes over the last four decades: isn't it simpler just to use the geographical county, safe in the knowledge that this will never change?!\n\nFor everything except local government, Peterborough is in NORTHAMPTONSHIRE.\n\nIf you'd like more information about the counties of Great Britain (and how they differ from administrative areas), please visit http://www.abcounties.co.uk\n\nOlly =)"
    author: "Olly"
  - subject: "Re: What's in a name?"
    date: 2006-11-26
    body: "why have you only got some of the cities and towns. i needed all of them for my home work!!!! :("
    author: "bethan shortman"
  - subject: "Re: What's in a name?"
    date: 2003-09-11
    body: "I thought it was a play on the IKEA slogan \"unb\u00f6ring\"..."
    author: "dOxxx"
  - subject: "Great!"
    date: 2003-09-11
    body: "I tried Konqueror (the web browser) recently using the CVS version and many rendering bugs seems to have been fixed.  \n\nObviously, KDE 3.2 will be a great release.  I wonder why there is so little pro-KDE comments on Slashdot compared to pro-Gnome comments."
    author: "Ned Flanders"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "If you're referring to today's article about GNOME 2.4, it's probably because the item was about GNOME, some comments about KDE are off-topic unless they relate to GNOME."
    author: "Tom"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "No, I'm not only referring to today's article.  Even in KDE related articles there seems to be a lot of pro-Gnome comments.  Maybe I'm wrong, I'll see when KDE 3.2 will be released."
    author: "Ned Flanders"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "The GNOME trolls are just better organized.  It means nothing else."
    author: "ac"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: ">> I wonder why there is so little pro-KDE comments on Slashdot compared to pro-Gnome comments.\n\nSimple: GNOME is all about polish these days, KDE not really.\n\n(use of the Keramik theme and those nice Crystal icons does not make KDE more polished, just a bit nicer to look at)\n"
    author: "whoopie"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "One word. . . RedHat(e)"
    author: "Rick"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "Please don't feed the trolls."
    author: "Bob"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "Does it matter?"
    author: "Joergen Ramskov"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "That's because KDE is infected by the GPL, and you have to shell out money to develop a commercial application."
    author: "TrulyFree"
  - subject: "Re: Great!"
    date: 2003-09-11
    body: "Not true.  You can develop commercial applications with open source licenses!"
    author: "ac"
  - subject: "Re: Great!"
    date: 2003-09-12
    body: "I never cease to be amazed at how absurd some people can be. (I can see why they are anonymous though!) This guy can't even get his flame right. KDE's base libs are LGPL'd so that you can do commercial apps on it. The chant is supposed to be against Qt, and then don't forget RMS blessed the license so you may as well just admit you enjoy hating something and the something you've chosen is KDE and aren't you righteous.\n\nHere's my two questions for people with at least enough intelligence to do the stupid flame the way their hate mongering leaders have tried to teach them. The complaint is for all those poor application developers who want to develop applications on Windows with Qt having to buy a license so they can make money off their software. \n1) If you want your software for free, but then you want to turn around and make money off of it... why are you complaining about somebody else making money off of software when they're giving it to us to use for free? It is the sublime argument that all business is bad and this business is bad because it charges other businesses money which is unfair to the other businesses. Follow? (For the correct response see Monty Python's \"And Now For Something Completely Different\" and watch for \"How to defend yourself against an attacker with a banana\".)\n2) Has anyone reciting this lame chant ever looked through a Programmers Paradise catalog? Because if they had done that and then played with MFC they'd realize how ridiculous they sound. You can spend way more on developer software than anything else!\n\nHere's what's really ridiculous. These same people were angry at Qt for being QPL'd and wanted it GPL'd. Now that it's GPL'd we find that it's still not good enough for them. What would be good enough? If it went away.\n\nHey, TrulyFree(FromReality), grow up! The GPL protects me from having my work taken by a company and commercialized without giving me a dime while also allowing me to give it freely to people, even you. Attacking the GPL on the basis of freedom means never having to take your shoes off to count to your IQ. Go practice your rants and don't post again until you can get in lock step with the other trolls."
    author: "Eric Laffoon"
  - subject: "Re: Great!"
    date: 2003-09-12
    body: "LOL  The best answer yet to these idiots parading the 'Qt is infected' meme...  I know this hapless idiot couldn't even be bothered to stick with the Qt VS KDE script, but seriously, what a great answer Eric.  Thanks!"
    author: "manyoso"
  - subject: "Re: Great!"
    date: 2003-09-12
    body: "I know that handle... Hey, I'm glad I could make your day. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Great!"
    date: 2003-09-12
    body: "... Nice comment ! But Trolls wont have that level of intelligence to understand what you said.\n\nThink I should refer to your comment, whenever a new flame of QT vs KDE comes up !\n"
    author: "Uwe Liebrenz"
  - subject: "Re: Great!"
    date: 2003-09-13
    body: "Probably, that's because people who use KDE for serious stuff (I use at work, configure kde for our customers on x86, pSeries etc), have no interest/time to post 'kde is great, gnome is not' kind of messages on slashdot...\n\nBTW, kde is no doubt amazing software  Thanks developers!"
    author: "Anand"
  - subject: "spell checking"
    date: 2003-09-11
    body: "I write (documents, e-mails,...) in several languages (pt, br/us, fr) and for now it is still hard to change to the correct language (the one in the document) for spell checking.\nI've seen in the 3.2 feature plan the following: \"Easy language selection for spell checking\".\n\nDoes this mean that I can default my language to Portuguese and easily do a spell check of a document in English or any other language? Or does this feature allready exist and I am being to dumb? (used to change it with the kedit preferences :-)\n\nThanks to all the team for the great job. It is really a pleasure to use KDE"
    author: "simul"
  - subject: "Re: spell checking"
    date: 2003-09-11
    body: "now there is language selection combobox in spell check dialog. Thnx to Zack Rusin!"
    author: "cartman"
  - subject: "Thank you."
    date: 2003-09-11
    body: "Thanks to all those developers who spent countless hours, mostly voluntarily, to make technology as excellent as the K Desktop Environment available for free - and complete with sources. Guys, you're doing a great and important job. For me, KDE is not only the desktop I enjoy working with the most, it's also a part of my personal freedom as it can't be taken away from me or modified without me knowing about it. Thanks to projects like Linux and KDE, I feel a bit safer nowadays - I basically can't be screwed by my software. \n\nGee suss, I have to donate more."
    author: "Sho"
  - subject: "Thanks :)"
    date: 2003-09-11
    body: "Also on another note, GNOME 2.4 was just released with many improvements http://www.gnome.org/start/2.4/.\n\nMost improvements are usability, acessibility, and general polish, but there are also quite a few new features.\n\nI am very impressed with GNOME 2.4's usability, now jsut about everything in the stock GNOME is HIG compliant and GNOME has the best documentation available on Linux thanks to SUN which constantly updates it. Come to think of it, GNOME's acessibility features and it's HIG is also thanks to SUN, SUN is pretty much the one responsible behind the great progress in usability, acessibility, documentation etc. that GNOME is making.\n\nI really encourage KDE people to try it, not to switch, but so that we can have an idea of the competition and \"borrow\" good ideas like they \"borrowed\" from us, if nothing at least check out the awesome documentation and release notes, the best I've seen so far."
    author: "Mario"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Sometimes I really wonder about your posts, Mario. You show up quite often when there's an important KDE announcement, like to make sure that someone reminds the readers that there's Gnome, too. I'm not saying that there's anything wrong with what you're saying, in fact, I think Gnome makes a fine desktop and 2.4 is the best release yet - but I do doubt your motives. That I do. Anyway, let's not turn this into an argument."
    author: "Sho"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "I tried it today. To me, GNOME is a few years behind KDE. Literally. I am an ex GNOME user, but at some point (KDE 2.* series) the difference in favour of KDE became too big and switched. Anyways, nice trolling."
    author: "NewMandrakeUser"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "The two biggest, most popular and coolest Linux DE's release new versions after months of preparation on the same day! How could I not post about it!\n\nI believe competition is healthy and that both projects can learna  lot from each other, GNOME has some clear advantages to KDE in usability, acessibility and documentation, I wanted KDE contributors to know so that they could check it out and maybe help KDE catch up.\n\nOverall, I think KDE is better than GNOME, and it is the desktop I use, but that does not mean I don't like GNOME or have some kind of rule that I can only say Pro KDE stuff."
    author: "Mario"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Did you post and advertize KDE on the GNOME site?"
    author: "ac"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "He wasn't advertising GNOME at all. Pointing out that usability and accessibility is better in GNOME wasn't bad either. Might be a little offtopic, but not completely unexpected on this site. If you think that it is unexpected, you're a little bit naive :-)!\n\n"
    author: "fault"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Uh-huh.  And does he point out all that is better in KDE on the GNOME site, complete with URLs to KDE.org?"
    author: "ac"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Was a brand new stable release of KDE just released? No.. thank you very much.\n\nQuit your hating, nothing wrong with pointing out good things in other environments. "
    author: "fault"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Yes, I did post KDE \"advetisements\" on gnomedesktop.org sometimes, for example \n\n \"Their new panel, movement of special menu applets, mini calendar for clock, SVG, post it notes desktop applet, have been in KDE for a while now. \"\n\nNot, quite as often, but only because I care about KDE more and I am more interested that people know it's weak spots, the first step to resolving a problem than if GNOME knows how KDE is better in this and this. I want KDE to continue being the #1 linux desktop."
    author: "Mario"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Ok good.  :)\n\nAnd I wasn't hating anyone despite fault's lies and slander!"
    author: "ac"
  - subject: "Slander?"
    date: 2003-09-12
    body: "\nSlander? That's non-sense.\n\nIn writing it's called libel.\n\nOkay, I stole that from Spider-man."
    author: "Henry"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Are you sure GNOME has better documentation than KDE? There are a number of apps in GNOME that have extremely outdated documentation.. even Nautilus in 2.4rc1 does. \n\nSadly, docs in KDE are also very outdated. Both Doc projects are severely understaffed and is something non-developers can work on! :)"
    author: "anon"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "GNOME's User's Guide, Admin Guide and Accessibility Guide are very professional."
    author: "Anonymous"
  - subject: "Re: Thanks :)"
    date: 2003-09-12
    body: "That's because they were written by Sun's proffessionally trained and (assumingly) well payed documentation writers."
    author: "John Hughes"
  - subject: "KDE's usability the best"
    date: 2003-09-12
    body: "The usability in KDE is _at least_ as good as in Gnome. You just have to make the sufficient settings. In this way, KDE is more flexible, has more options, and with the correct settings _will_ become more usable than Gnome.\n\nThis is a fact because usability can only be measured in a given work context, thus the possibility to adopt to that context is essential. Adopting is possible when flexibility is high. Simply removing features decreases flexibility and does not help increase usability."
    author: "Olof"
  - subject: "Re: KDE's usability the best"
    date: 2003-09-12
    body: "I'll reply to myself here. Just to add an important point.\n\nUsability _does not_ come right out of the box. It doesn't matter if big software companies wants us to believe so. It just doesn't work that way."
    author: "Olof"
  - subject: "Re: KDE's usability the best"
    date: 2003-09-12
    body: "That depends on the user. Many users do not even try to change the default configuration. For them easy-to-use and simple defaults are most important point, and they are also Gnome's main target group.\n"
    author: "Jeff Johnson"
  - subject: "Re: KDE's usability the best"
    date: 2003-09-12
    body: "What is the \"default configuration\"?\n\nIs it what the gnome team has in their packages? No.\nIs it what a distribution includes? No.\nIs it what comes installed, set and configured as part of a product and/or system? Yes."
    author: "Alex"
  - subject: "Re: KDE's usability the best"
    date: 2003-09-12
    body: "Obviously depends on where you get your system from. But yes, for home users and all people who try out KDE for the first time, either the KDE or the distributions defaults are essential for their first impression of KDE. And if they don't bother to reconfigure, also for their last."
    author: "Jeff Johnson"
  - subject: "Re: KDE's usability the best"
    date: 2003-09-13
    body: "> either the KDE or the distributions defaults are essential\n\nYou assume that the one who sets up a system at a workplace actually uses the defaults that these groups have \"chosen\". For home desktop users this will only be true if they buy a distribution separately and installs it. I argue, that in other cases, the ability to change (=flexibility) will decide how near you can come to the optimal usability for a user."
    author: "Olof"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "I agree that GNOME has its strongest points in accessibility, documentation and usability. Those are AFAIK the areas where SUN is most heavily involved by throwing in the most man power.\n\nUsability is a topic where KDE is slowly but constantly improving (as man/women, who do the actual work after discussion, time permits). Accessibility is also on the focus with the new package kdeaccessibility and I have high hopes in the improved accessibility framework of the upcoming Qt 4.0. Finally documentation: This is where only you, the English-capable KDE users, can really help: http://i18n.kde.org/doc/gettingstarted.php"
    author: "Anonymous"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Could there be some sort of gail replacement within the kde (kail? ;-) to get the same accessibilities as gnome to be able to integrate atk and at-spi without any dependencies with glib/gtk+?\n\nWhat if that framework could be introduced through the freedesktop.org to the Qt-4.0?\n"
    author: "Nobody"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "ATK integration is work-in-progress for Qt 4.0, see the report about Qt 4.0.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "<< GNOME is HIG compliant >>\n\nSo much fuss about this. KDE has been compliant to his HIG from the very beginning and has been a lot better to comply with it than Gnome ever was. \n\nSo Gnome finally reaching a level that KDE always had. I am not impressed.\n\nThings like ATK are more interesting."
    author: "Philippe Fremy"
  - subject: "Re: Thanks :)"
    date: 2003-09-11
    body: "Assuming I actually believe your motives are genuine, I still don't agree. Not to start an anti-gnome war or anything, but every post I've ever made or read about KDE or something \"non-GNOME\" on gnomedestkop.org has been removed*, so I'm not exactly estatic to read about GNOME on a KDE web forum. If the avergage person used or cared about GNOME, then they would visit their webpage anyways. So your \"Spreading the Word\" posts are rather pointless.\n\n*After emailing the moderator, he informed me that he's only removed 3 posts in his time of managing the forum. He told me that they're not being removed, but that some GNOME zealot(s) moderated them as trolling, and the default threashold of the forum is set to 1. So basically, moderating to 0 has the same effect of deleting a post."
    author: "Chris Spencer"
  - subject: "GNOME is outdated stuff"
    date: 2003-09-11
    body: "They're soo behind in their technology so there's no wonder why the german, chinese and japanese government have choosen KDE and only KDE as the base for their open source migration. No working application communication protocol (their CORBA-based stuff is simply a mess...), a stone-aged widged set (compared to KDE & Qt), incoherent user interfaces of the programs and so on. Do you know why they're praising HIG (Hide Incoherent GUI-Stuff)? 'cause they cannot build on a mighty framework which leads to consistency by itself."
    author: "Ruediger Knoerig"
  - subject: "Donations"
    date: 2003-09-11
    body: "KDE needs to try and get more donations, unlike GNOME it doesen't have sun to do half the work and pay the bills, it needs to encourage it's users to donate. GNOME even has a large donation suggestion on the front page.\n\nCheck it out: http://www.gnome.org\nAlso vote for this wishlist: http://bugs.kde.org/show_bug.cgi?id=63868"
    author: "Adi"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "We already have means for people to support KDE:\n\n- If you want to support hackfests or fairs which improve productivity drastically, donate to the KDE e.V.(http://www.kde.org/areas/kde-ev/) or become a supportive member.\n\n- If you want to help the developers, have a look at the adopt-a-geek program (http://ktown.kde.org/~wheeler/adopt-a-geek/)\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Daniel"
    date: 2003-09-12
    body: "I never said KDE does not have the means to allow people to donate, it's simply that the donation web page and many other things can be greately improved in order to encourage more donations and make it easier.\n\nPlease read the bug report to see what I mean."
    author: "Adi"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "Guys, please don't take this the wrong way....\n\nIf I want to contribute to KDE, how do I know who / where my donation goes to? Is there a website or something explaining this? I *would* consider this in the same way I am considering joining MandrakeClub. I really think that all the developers of KDE / Open Source should be helped in whatever ways possible. If I had the know-how and time, I would certainly give it to the project(s). But until then, maybe a donation would do.\n\nThanks to all you guys who give up your own time to provide us with something so special....and for FREE!"
    author: "Trickster"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "If you donate money to the KDE e.V. association, you will not get accountability for it. That's the privilege and duty of the association members (deservered KDE developers/contributors). You can only be assured that it will be spent according to the objectives described in the bylaws."
    author: "Anonymous"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "You can make donations to KDE via the following page:\n<br><br>\n<a href=\"http://www.kde.org/support/support.php\">http://www.kde.org/support/support.php</a>"
    author: "Waldo Bastian"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "From my understanding ,SuSE and Mandrake, the number 1 and 2 Linux distros respectivly, contribute quite a nice ammount to KDE. I think Lindows contributes momey to KDE as well."
    author: "Chris Spencer"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "SuSE is a big supporter of KDE! They pay two developers to work on KDE and motivate their employees to do the same. They also co-sponsored the KDE developer conference and support KDE with booth computers on exhibitions where both KDE and SuSE attend. Mandrake used to support KDE with a paid developer but he was laid off due to financial problems. Lindows doesn't contribute money to KDE anymore, there is even talk that they are several months with payment behind for their half-sponsoring of kde-look.org.\n\nWhat really is missing is cash in the hands of KDE e.V. to allow smaller expenses. I don't think there will be ever enough money that KDE e.V. will be able to pay a developer partly or full-time. I'm talking about the possibility to pay booth material, produce T-shits and CDs, legal costs or allow reimbursement of travel and accommodation costs when KDE representatives join desktop related conferences/meetings in US, Europe and elsewhere."
    author: "Anonymous"
  - subject: "Re: Donations"
    date: 2003-09-12
    body: "there is always a kde developper paid by Mandrake , they was 2 ( dfaure and lmontel )"
    author: "Anonymous"
  - subject: "Donations"
    date: 2003-09-12
    body: "Laurent Montel is sponsored by Mandrake to work part time on KDE\nDavid Faure is sponsored by Trolltech to work part time on KDE"
    author: "Charles de Miramon"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "Crap !! I meant 2nd and 3rd Linux distros, RedHat obviously being first. Doh !!"
    author: "Chris Spencer"
  - subject: "Re: Donations"
    date: 2003-09-11
    body: "Need and use for donations varies, but they are always useful and appreciated. Aside from what has been posted here for donating to KDE as an organization and \"Adopt a geek\" there are projects like Quanta Plus set up to take donations. In our case my business, Kitty Hooch, is listed as a sponsor of Andras Mantia to code full time on Quanta. In reality since we are a young business it's been difficult as it actually has eaten into our funds for growing our business at times, which is a shame. If I had the financial resources I'd spend more funding development. For this reason we are very appreciative of the help we receive from contributors. Ironically it seems to clump up around releases. Fortunately we have others helping with sponsorship commitments.\n\nOne thing we are hoping to do later this year is bring Andras to the US to meet with me so we can work on our own little hack fest and also on our long term objectives for advancing Quanta and web development on KDE. We can look for the cheapest airfare so hopefully it will not be that much but because of the slow economy in Oregon the only certain way to put this together is with donations.\n\nThere is good and bad news about donations to our project. The bad news is that only a very tiny fraction of a percent of our users ever donate and we can go a month with only one donation. The good news is that, when we see donation activity, our few contributors have been very generous and have made a BIG difference. I have no doubt that our users will help Andras and I to meet face to face very soon. I've sent personal thank yous on each contribution. Thanks again to all who have donated, or plan to donate to the Quanta project! One person can make a difference.\n\nListing KDE contributors including Kitty Hooch:\nhttp://kde.org/support/thanks.php\n\nFor those wishing to donate to Quanta Plus or become a sponsor:\nhttp://quanta.sourceforge.net/main1.php?actfile=donate\n\nWhen you help out you become part of the success of the project, and we appreciate your efforts as much as you do ours. :-)"
    author: "Eric Laffoon"
  - subject: "Re: Donations"
    date: 2003-09-13
    body: "Doeas anybody know if these donations are tax deductable in the US?\n\nIf so, if makes it much easier to get them."
    author: "a.c."
  - subject: "Re: Donations"
    date: 2003-09-13
    body: "They hould be. They are for most projects like GNOME and Mozilla. "
    author: "Adi"
  - subject: "KDE CVS"
    date: 2003-09-11
    body: "I've been using KDE cvs in a dialy basis (I'm not a developer), and all I can say is: Good work, people! It's fast (performance options), the context menus are being reorganized and the overall quality is pretty good for a pre-alpha release (I can do a lot of work here).\n\nProblems: I am still experiencing rendering problems on konqueror. I wish that apple code merging on khtml could be made faster...\n\nSo, if you use debian unstable, give a try on orth's page at \nhttp://hobbiton.opendoorsoftware.com/\nFor binary packages from CVS. It's worth a try!\n\nLucas"
    author: "Lucas"
  - subject: "Re: KDE CVS"
    date: 2003-09-11
    body: "is there a possibility to install those without removing my kde 3.1.3 packages?\n\n  - kata >8^)"
    author: "katakombi"
  - subject: "Re: KDE CVS"
    date: 2003-09-11
    body: "http://developer.kde.org/build/build2ver.html\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: KDE CVS"
    date: 2003-09-11
    body: "> Problems: I am still experiencing rendering problems on konqueror. I wish that apple code merging on khtml could be made faster...\n\nMerging apple's code isn't the holy gral and it isn't easy:\n\n\n1. Both are moving targets which makes merging complicated.\n\n2. Apple also makes mistakes or changes that don't make sense. That's why you have to check the patches thoroughly. Sometimes this can take up so much time that it makes more sense to program the functionality yourself.\n\n3. KHTML developers and the Apple developers sometimes have different opinions on how things should be solved. Another reason why sometimes patches aren't merged.\n\n4. Sometimes the developers didn't explicitly stated that a cvs commit was a merge from Safari.\n\n5. etc...\n\n\nChristian\n\n-----\nhttp://www.kde.org/apps/cervisia"
    author: "cloose"
  - subject: "Re: KDE CVS"
    date: 2003-09-11
    body: "6. Sometimes when it's stated that the code is merged from Safari, it's really a nearly full rewrite that's smaller, faster, and more correct. \n"
    author: "Sad Eagle"
  - subject: "Fight with gnome people"
    date: 2003-09-11
    body: "For everyone here:\n\nLet's live in peace :) We don't need to fight with gnome people, they are our friends. Both DE's are evolving and nowadays they are aimed to different audiences. We can learn a lot from them, as they can do from us. This is all about free software and free software is made with cooperation.\n\nLucas\n\n"
    author: "Lucas"
  - subject: "Re: Fight with gnome people"
    date: 2003-09-11
    body: "+1 Insigthful"
    author: "Peter Plys"
  - subject: "But rule 1 of fighting - pick on someone small!"
    date: 2003-09-12
    body: "So GNOMEs are good choice :)"
    author: "Rob"
  - subject: "Using both kde 3.1 and kde 3.2"
    date: 2003-09-11
    body: "Hi!\n\nHow difficult would it be to use some programs from KDE 3.1\nand some programs from of KDE 3.2 on the same system? I would like \nto keep using kmail from 3.1, but might want to give konq\nfrom 3.2 a try.\n\nIf I install kde3.2 into /usr/local/kde3.2 and not point\nmy PATH towards this directory, would I then be able to\nuse my old kmail by typing kmail and the new konq by typing\nthe full path to it? Of course i want kmail to use the kde3.1\nlibraries and never the new unstable libraries, and konq\nto use the new kde3.2 libraries. \n\nIf not, any other nice way of doing this?\n\nWhat happens to my configuration files in ~/.kde? Can i \nfool a kde program into using an alternate directory for \nconfiguration files, for example by setting some environment\nvariable?\n\n(for what it's worth, I use slackware 9.1, which has kde \nand all libs in /opt/kde)\n\n"
    author: "mr. Anonymous Coward"
  - subject: "Re: Using both kde 3.1 and kde 3.2"
    date: 2003-09-11
    body: "In theory every KDE 3.1 program can be compiled against and can run with KDE 3.2. In practice KMail is one of the most stable programs in the KDE development versions, so don't hesitate to use it. Better don't mix different KDE installations at run-time, problems with configuration etc are immiment."
    author: "Anonymous"
  - subject: "Re: Using both kde 3.1 and kde 3.2"
    date: 2003-09-11
    body: "The best and easiest solution is to create a special user account for KDE 3.2. Then you can either start two sessions or you simply log into the other account, e.g. with 'ssh -X -l kde32user localhost' in Konsole and then start whatever program you want to try from the command line.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Using both kde 3.1 and kde 3.2"
    date: 2003-09-11
    body: "That would solve the configuration file problems.\n\nWhat about the libraries? Will KDE 3.2 programs use the new \nkde 3.2 libraries, and KDE 3.1 programs use the kde 3.1 \nlibraries automatically? \n\nI don't want for example kmail from kde 3.1 to use the\nkde 3.2 libraries, since they are alpha-stable. Also, \nI want to have konqueror from kde 3.2 use the kde 3.2 libraries\nsince they contain new stuff.\n\n\n\n"
    author: "mr Anonymous Coward"
  - subject: "Re: Using both kde 3.1 and kde 3.2"
    date: 2003-09-12
    body: "Well, you can have only one version of kdelibs-3.0 installed at a time. They are binary compatible, so old KDE-3.1 programs would still work with kdelibs-3.2. But it won't work the other way around. kmail from KDE-3.2 won't work with the old kdelibs, because new features have been added and are used by kmail and the other KDE-3.2 programs.\n\nResult: If you want to use kdelibs-3.1, then you must stick to KDE-3.1 programs as well."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Using both kde 3.1 and kde 3.2"
    date: 2003-09-12
    body: "This is no problem. Have a look at http://developer.kde.org/build/build2ver.html ."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Using both kde 3.1 and kde 3.2"
    date: 2003-09-16
    body: "Why on EARTH would you want to bother setting up an ssh connection to localhost with X forwarding?\n\nWhy not set xhost appropriately, and do:\n\nsu - kde32user\n\n?\n\nDavid"
    author: "David Pye"
  - subject: "the bugs?"
    date: 2003-09-11
    body: "what about the amount of bugs?\n\nkde 3.1 was released with many. ( 2000+ ? )\nand their was talk about bringing it down in 3.2\n\nhas this been a succes?"
    author: "Mark Hannessen"
  - subject: "Re: the bugs?"
    date: 2003-09-11
    body: "Well, find out yourself:\n<br><br>\n<a href=\"http://bugs.kde.org/weekly-bug-summary.cgi\">http://bugs.kde.org/weekly-bug-summary.cgi</a>"
    author: "cloose"
  - subject: "Re: the bugs?"
    date: 2003-09-11
    body: "Actually, considering the size of the project, being released with 2000 bugs isn't that bad at all :)"
    author: "Chris Spencer"
  - subject: "Re: the bugs?"
    date: 2003-09-11
    body: "But with 4884+ is...\n\nMore bug crushing (dualie delete) days before each alpha/beta/rc release?\n\nBut what about those 4468+ wishes and 362+ most wanted features?\n\nHalving those might be a huge enought goal?\n"
    author: "Nobody"
  - subject: "362+ most wanted features?"
    date: 2003-09-12
    body: "\"Most wanted\" because 2 people voted for it? Only 197 wishes are wanted by at least 3 people and only 73 by at least 5 people with the top item being wished by 73 people."
    author: "Anonymous"
  - subject: "Re: 362+ most wanted features?"
    date: 2003-09-12
    body: "Sure sure, but as you might notice, I just took the numbers bugs.kde.org gave, why is that bad? Sure it might make it look bad by numbers, but that is what people see on it.\n\nForget my mention about them if that bugs so much. ;-)\n\nSure the item that matters is to get bugs fixed before other things start to get done on those wishes/feature lists... Nothing wrong with that as that is the most sane way to do them.\n"
    author: "Nobody"
  - subject: "HOLY CRAP!!"
    date: 2003-09-12
    body: "4,890 bugs in KDE, wow, never knew KDE was that buggy!\n\nI think an acceptable release number would be under 2,000, man this new release schedule is messed up, I don't see how KDE developers can possibly get the project in the sub 2,000  area by December, I defintely hope it's delayed.\n\nWhat strikes me even more is that KDE isn't beating the tide of bugs, each week, it seems that more bugs are added than actually getting fixed. I really hope that most are duplicates.\n\nI think KDE needs a MUCH LONGER TESTING AND FREEZE PERIOD. In fact, KDE doesen't even have a freeze period anymore, now you can add new features even during an RC as long as it has approval. Ugh... not good.\n\nJesus Christ almost 5,000 bugs, don't tell me that that's a small number for a project KDE's size, because it isn't! And, I defintely do not want KDE to ship with 5,000 bugs."
    author: "Adi"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-09-12
    body: "It's an alpha, jackass."
    author: "ac"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-09-12
    body: "Remember that nearly half of those bugs are with Konqueror, and most of those bugs are khtml/kjs bugs. A modern HTML/CSS/javascript rendering engine is very complex, and takes years to perfect. "
    author: "anon"
  - subject: "Always the same..."
    date: 2003-09-12
    body: "Remember that a bug, can be like that:\n\n1) After loggin in, kde erased all my data.\n\nor...\n\n2) When I was returning from gym, I crossed the street very quickly, and after that, but only after that, when started konqueror from clicking a URL in KMail, and an evil javascript resized the window, then, a font doesn't displays at 2px size, but 3px.\n\nWhat I *mean*:\n\nWhen you erase all duplicated bugs, feature requests, silly render bugs in konqueror, typos in GUI's messages, etc., you will find that only 15, or 20 bugs are really serious, and need to be fixed before an stable release.\n\n(Sorry for my english, it's very very late, and I need to sleep)"
    author: "Alex"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-09-12
    body: "You \"just don't understand\". Software has bugs, that is a fact. KDE-folks could use the next 10 years killing bugs, and the software would still have bugs! New ones would get reported all the time. If you want to wait 'till KDE has 0 bugs, you would be in for a LONG wait!\n\nAnd those are reported bugs. Whether the bug is reported or not, they would still be bugs. Some piece of code might seem bug-free because there are no bug-reports, but reality would be vastly different. Thankfully KDE has active users that report bugs when they see them. That's why the number seems so high.\n\nAnd how many of those bugs really apply? How many are from old versions? How many are from packaging-errors? How many are fixed in CVS but are simply not closed for one reason or the other? How many are user-errors?"
    author: "Janne"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-09-12
    body: "It never  ceases to amaze me how totally ignorant and uninformed individuals love to just spout their mouth and state their opinion on a subject that they know nothing about, and actually expect people to do it. If you knew anything about programming and about the nature of the bugs in KDE you would know that of the \"almost 5000 bugs\", many are duplicates, once in a blue moon or non-critical errors, and so on. And the number of bugs has has increased dramatically in the past week as an alpha was just released, thus its expected to be buggy, and because a lot more people will use an alpha than just normal head, the number of bugs found will obviously stay high for next few weeks.\n\nAnd you also obviously have no idea about the release schedule. You can't just add features during the RC period. Even before the RC 1 release non-trivial CVS commits have to be aapproved by at least one other developer. Catch a clue, and stop trolling"
    author: "lrandall"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-09-12
    body: "Which version of Windows had 60,000 bugs when it shipped?\n\nAlso, like what you're doing here, many people spouted that statistic without understanding it. I think 70% of that figure were non-critical UI bugs, like certain widgets not getting erased properly, etc.\n\nThe fact that so many bugs are reported is the most important thing you can take home from that statistic. If that's still not good enough for you then you should try and fix a few. If you can't do C++, not all bugs require programming experience."
    author: "Max Howell"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-09-12
    body: "Yup, Windows 2000 shipped with 63,000 bugs according to an internal leaked memo from Microsoft.\n\nhttp://www.cnn.com/2000/TECH/computing/02/17/windows.2000/\n\nAnd Win2k has been the most stable version of Windows in a long time. WinXP and 98 were much less table out of the box, but later patches fixed that."
    author: "fault"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-10-19
    body: "Ummm... Windows includes the GUI + operating system.  KDE= GUI only.  Don't compare apples to oranges.  Or Windows to KDE."
    author: "goarl"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-10-19
    body: "Actually KDE = GUI+(some)Applications. Still useless, but more than the GUI.\n"
    author: "Tim Jansen"
  - subject: "Re: HOLY CRAP!!"
    date: 2003-10-24
    body: "Actually you compared Windows to KDE, I was just using Windows to illustrate my point; I had no intention to draw comparison. If you missed my point you should read my post again."
    author: "Max Howell"
  - subject: "Look at it Like This"
    date: 2003-09-12
    body: "I think another thing most of you are forgetting is exactly who the bug applies to. For example, if there is a bug that only affects FreeBSD running on Athlon processors, then it's counted in the overall KDE bug count, not as a specific quirk. You'd be amazed how many bugs only affect about 5% of all users. A prime example would be a glitch in the rendering engine of Intel i815 chipsets that causes a some GUI's to be rendered incorrectly. Even though it only affects KDE (not Gtk apps) when ran on the i815 chipset, and it's actually a fault in the hardware, it's still listed as as open bug for the XFree86 project."
    author: "Chris Spencer"
  - subject: "You people don't listen!!"
    date: 2003-09-13
    body: "How the hell did any of you pass your english classes?\n\n1. never said KDE should have 0 bugs, all I said that 3.2 should have under 2,000 bugs, in MY opinion jackass! I know it's an alpha and I didn't complaina bout the bugs in teh alpha all I said that by the time the release cycle is over and KDE 3.2 is released I defitnely do not want KDE to have 5,000 bugs. My biggest complaint is that KDE is not beating the tide and I can't imagine how they will make a release with even 2,500 bugs.\n\n2. I know there are many duplicates and irrelevant bugs, but those usually get closed pretty quickly.\n\n3. I know Windows was released with 63,000 bugs but in reality that is not as relevant. Microsoft has over 97 times the marketshare KDE has and it is bound to have many more known bugs. In addition, they have people who's actual job is testing the software and not just users and developers, so probably they're peole tend to find more relevant and just plain more bugs. Windows is also an ENTIRE OPERATING SYSTEM WITH A KERNEL DRIVERS, A DE, MEDIA PLAYER, MOVIE MAKER, CD BURNER, BROWSER, FILE MANAGER ETC. Therefore' it isn't fair to make this comparrison. I don't think a Linux desktop would really have a lower amount of bugs.\n\n4. Also, I NEVER said you could just add new features in RCs, I mentioned clearly that teh feature sneed approval.\n\n5. Please everyone here, please stop attacking me for a reason, I do have a clue and quite a good one, I've been involved in Linux for more than 5 years now.\n\nThe only valid point about releasing KDE 3.2 with 5,000 bugs is that many could be for oter architectures. Again, I nver said it was unacceptable for an alpha to have 5,000 bugs, I only said it is unacceptable to release it and I said this because KDE doe snot seem to be beating the tide of new bugs."
    author: "Adi"
  - subject: "Re: You people don't listen!!"
    date: 2003-09-13
    body: "Also don't bother to make the stupid point saying \"no developers manage to beat the tide\" this is simply not true. For example the Galeon project has been able to beat the tide losing over 220 bugs and fixing 279 in just the past week. \n\nAlso, don't say that GNOME has more duplicates because they also use Bugzilla and there is no reason to believe this.\n\nAlso, remember that Galeon isn't even part of GNOME anymore.\n\n-277"
    author: "Adi"
  - subject: "Re: You people don't listen!!"
    date: 2003-09-13
    body: "Remember, Galeon is not part of the GNOME project so nobody really uses it.  This means it has a lot of bugs that have not been found."
    author: "ac"
  - subject: "Re: You people don't listen!!"
    date: 2003-09-23
    body: "Of course it is used. I know users who actually prefer Galeon as a browser in KDE."
    author: "Eggert"
  - subject: "Re: You people don't listen!!"
    date: 2003-09-23
    body: "i think most of them switched to MozillaFirebird. \n\nGaleon has a bleak feature. NetCraft said that they are dying. With the recent introduction of Epiphany in GNOME 2.4, Galeon is all but dead corpse already."
    author: "ac"
  - subject: "Re: You people don't listen!!"
    date: 2003-09-13
    body: "Once again, most Konqueror bugs are related to khtml/kjs..\nThe galeon browser uses gecko, and does not maintain their own rendering engine.\n\nCompare something more realistic like Mozilla. They had 213 new bugs for their \"browser product\" over the last week, and 34 bugs fixed/closed. \n"
    author: "fault"
  - subject: "Boy, ac are you mistaken"
    date: 2003-09-13
    body: "Galeon, is not part of GNOME but the poll on GNOMEDESKTOP.org clearly shows tat it is the prffered browser for GNOME still. It has many users, a dozen of new bugs reported each week and still actively developed. Galeon Developers only and their not even part of the GNOME project fix over 275 bugs each week, KDE with all of it's components many times won't fix this many, so don't say Galeon is not being heavily worked on or being used.\n\nAlso, the fact that Galeon uses Gecko or not is besides the point."
    author: "Adi"
  - subject: "Re: Boy, ac are you mistaken"
    date: 2003-09-13
    body: "> Galeon Developers only and their not even part of the GNOME project \n\nYou can barely spell, yet you attack other people's English.  Go away, troll."
    author: "ac"
  - subject: "You're a total idiot"
    date: 2003-09-14
    body: "You really showed how stupid you are on that one. Galeon is merely a \"front-end\" to Mozilla. They don't even have to write/maintain the most complex part - the rendering engine. So obviously they it will be shipped with a minimal ammount of bugs, seeing how it doesn't do *any* of the dirty work. If you want a much more fair comparision, compare it to Mozilla. It's an excellent browser, but only God knows how many bugs are in it..."
    author: "Chris Spencer"
  - subject: "Re: You people don't listen!!"
    date: 2003-09-13
    body: "> Please everyone here, please stop attacking me for a reason, \n\nFine, we'll attack you for no reason.\n\n> I do have a clue and quite a good one, I've been involved in Linux for more than 5 years now.\n\nOh my.  Looks like we've found ourselves a bona fide computer scientist here folks!\n"
    author: "anon"
  - subject: "You guys are really smart! LOL!"
    date: 2003-09-13
    body: "I can spell very well, it's just that unlike you I don't care so much about some message on a news item and why should I really, it won't affet my life in any big way.\n\nI see you again have trouble READING and COMPREHENDING because I was not attacking anybody's english, I was attacking everybody's poor COMPREHENSION ability. Now, what does my spelling have to do with the issue at hand anyway. Nothing, stick to the topic and stop peronally attacking me. When you do this I consider that you fail to come up with anything to counter my arguement and so have decided to change the subject and launch personal attacks.\n\nAgain, I never said I was a computer scientist or anything like that, stop MINTERPRETING what I say again. All I said was that I am more than familiar with KDE, GNOME and most other important Linux projects, so don't think you're speaking to a newbie. I also, did have one year of C++, but that's about it. \n\nNow, any of you care to stop making smart ass unrelated remarks and either post a comment about what I'm saying or don't post at all. Your lack of focus is one reason that makes it so hard for you to understand my post \n\nBTW: I actually am in English Honors so, please stop making dumb assumtions."
    author: "Adi"
  - subject: "Re: You guys are really smart! LOL!"
    date: 2003-09-13
    body: "affet -> affect\n\nCOMPREHENSION ability -> ability to comprehend (or comprehensional ability if you want, but that sounds ugly).\n\nperonally -> personally\n\narguement -> argument\n\nMINTERPRETING -> MISINTERPRETING\n\nassumtions -> assumptions\n\nSorry, but I couldn't resist! ;-) And people have addressed the points you've made. I agree that some people were aggressive, but honestly, you brought it on yourself! "
    author: "Ed Moyse"
  - subject: "Re: You guys are really smart! LOL!"
    date: 2003-09-13
    body: "Don't you see what you're doing. Your, not posting on the topic as I just asked in hope that some people would actually want to discuss the topic. Really, this si very immature of all of you.\n\nEd, your post is a waste of space the DOt, it does not help or say anything new in any way. Correcting my spelling mistakes completely out of context is pointless and only proves that you must have low self esteem, to feel the need to do this when you clearly understand what my idea was and what each word should be. If at least, you rewrote my entire post with corrections, that might have been something. \n\nYou too have  problem reading, the topic was never  \"Adi's spelling mistakes\" it was about the number of bugs in KDE< bugs in software and the pace at which KDE is fixing them.\n\nMAybe you'r smart, I don't know, but doing this IMo shows your a dumbass who can't stay on topic or come up with good arguements. It makes me feel you don't understand the idea or have nothing to say and this applies to almost everyone else in this thread."
    author: "Adi"
  - subject: "Re: You guys are really smart! LOL!"
    date: 2003-09-13
    body: "Everyone had already answered your points, so there was no sense in duplicating that. Since you seemed not to notice, a summary might be: your original post was alarmist and defeatist, and no terribly grounded in reality.  \n\nWhat noone did was call you on your \"i can spell very well\" comment in a post riddled with spelling mistakes (and be fair, I did apologise in advance, and it was at least partly tongue in cheek!)\n\nLook, you really should calm down a bit. I understand that you're upset you weren't taken more seriously, but get a grip! Go out and have a beer, find a woman, but a dictionary (sorry! just can't help myself it seems!) ;-)\n"
    author: "Ed Moyse"
  - subject: "Re: You guys are really smart! LOL!"
    date: 2003-09-13
    body: "Oh dear - that's embarassing ... I meant to say BUY a dictionary of course. ;-)"
    author: "Ed Moyse"
  - subject: "Re: You guys are really smart! LOL!"
    date: 2003-09-13
    body: "The point is that you are a liar and a troll, that all your points have already been adequately addressed except for your obviously silly knee-jerk emotional reactions.\n"
    author: "ac"
  - subject: "Not true."
    date: 2003-09-13
    body: "You did not answer:\n\nhttp://tinyurl.com/n9jf\n\nand\n\nhttp://tinyurl.com/n9jk\n\nYou simply went on about my spelling mistakes which was far from the topic. Also, YES I CAN SPELL VERY WELL, on the STAR test I was far above average achieving 99%, the fact that I type fast and do never go back to correct mistakes is entirely different. There is a significant difference between spelling well and being able to spell well.\n\nAlso how am I a troll anda  liar? Please enlighten me and actually answer my posts instead of blabbering about how the sky is blue."
    author: "Adi"
  - subject: "Re: Not true."
    date: 2003-09-15
    body: "yeah..\n\ni can write excelent code !!\ntrust me.\n\nok, ok, i'll only donate crappy code because that writes faster.\nbut if i would want too....\n\nwhat point is there in \"being able too\" when you don't \"do\" it\n\nbut back on topic:\nlet's just see what will happen in the \"bug fix\" weeks.\nwe will then see how real the \"duplicate bugs\" thinggy is.\nafter that we will see what is left and what needs fixin before 3.2\n\nhow is that with you?\n"
    author: "Mark Hannessen"
  - subject: "Re: You guys are really smart! LOL!"
    date: 2004-03-07
    body: "When you say 'everybody's comprehension ability.', it should be 'everybodys' comprehension ability.'. This is because the comprehension ability belongs to more than one person. When it is both belonging to and plural, the apostraphe should be placed after the 's'.\n "
    author: "H.Tan"
  - subject: "kdeextragear-1/2 software integration"
    date: 2003-09-11
    body: "So what about some sort of page giving impression what programs should/might/could be integrated to the KDE-3.2 release?\n\nFor example k3b/cdlabelgen, kphone and kpdf start to look usable enought for being included on all distribution configurations... ?\n"
    author: "Nobody"
  - subject: "Re: kdeextragear-1/2 software integration"
    date: 2003-09-11
    body: "kdeextragear-* isn't a place where apps are supposed to mature until they are ready for integration (that's what kdenonbeta is for). Please see http://extragear.kde.org/home/about.php which lists several reasons why the apps are in those cvs modules.\n\nChristian"
    author: "cloose"
  - subject: "Re: kdeextragear-1/2 software integration"
    date: 2003-09-11
    body: "Yeah ok, I know, but the thing were that atleast all main distributions consider the k3b as the default CD/DVD burner nowdays, that's why (atleast to me) it would make sense to include it with the release sonner rather than later...\n\nAnd atleast to me it sounds a bit silly to keep that kand of functionality as \"extra packages\" for users to choose as most computer systems sold nowdays have atleast CD-RW if not DVD(+/-/+-)DVD drives.\n\nOf course there is also the cdbakeoven-thingie around, but it is becoming rapidly obsoleted (like kreatecd has already) by the k3b as k3b-0.10(cvs) also include quite extensive DVD support via dvd+rw-tools.\n\nSure major features missing from it (IMHO) are DVD ripping (discussed in that past too, something similar to quickrip) and as the user interface is actually quite close to konqueror (in file browsing mode) itself, so why not make it a internal part of the konq itself (simple side-card?)?\n\nAnd as cdlabelgen/kover feature also would make it closer to burning software in conpetitive operating systems...\n"
    author: "Nobody"
  - subject: "Re: kdeextragear-1/2 software integration"
    date: 2003-09-11
    body: "kdpf may be in KDE 3.2. k3b perhaps in the next but one KDE version, an inclusion into KDE would likely more slow down its development at the moment than help. Never heard of kphone being developed in kdeextragear-* and it's perhaps not as useful for the same amount of people as the other two."
    author: "Anonymous"
  - subject: "Re: kdeextragear-1/2 software integration"
    date: 2003-09-11
    body: "Did look it up again, and notices that I was wrong! :-)\n\nThere just were a directory in kdenonbeta (and yes, not in kdeextragear) but that dir was actually empty, maybe someone in the inner circle could explain it that something for the future, or some mishap from the past?\n"
    author: "Nobody"
  - subject: "Re: kdeextragear-1/2 software integration #2"
    date: 2003-09-11
    body: "More software that would make it more simple to come to conclusion what packages should be included without sacrificing functionality on average Linux distributions by suggesting what other packages could be eliminated:\n\nkmyfirewall (so there would be synerged way to control simple personal firewalls)\n\nkmplayer (Mplayer support much much more codecs than todays noatun/kaboodle/xine/xanim hassle)\n\nkplayer (yes, not yet worked on kde HEAD, but would eliminate pretty efficiently noatun/kaboodle)\n"
    author: "Nobody"
  - subject: "Do not include duplicates"
    date: 2003-09-12
    body: "I think it's a good idea to remove cdbackoven and just stick with K3b. There is no reason what so ever to include two applications that have supposedly the same functionality/purpose in the KDE repository. If someone wants to develop his/her own CD burner app, that's perfectly fine, but we need to include ONE application for each SPECEFIC purpose, not two.\n\nAnother example is KRec vs. KRecord, make up your minds and choose one of those, don't confuse the user.\n"
    author: "Slovin"
  - subject: "Re: Do not include duplicates"
    date: 2003-09-13
    body: "Totally agree with you. Besides, K3b is the best."
    author: "Adi"
  - subject: "Re: Do not include duplicates"
    date: 2003-09-13
    body: "For what it's worth, I agree too."
    author: "Ed Moyse"
  - subject: "Re: Do not include duplicates"
    date: 2003-11-22
    body: "What?? I don't understand you. What is KDE?? Huh! Why is it being duplicated?"
    author: "Happyyou"
  - subject: "Dashboard cluepackets?"
    date: 2003-09-11
    body: "Has anyone tried modding KDE apps to throw/catch Dashboard cluepackets?\n( http://www.nat.org/dashboard/ )\n\nThe project looks very cool, and the interface is pretty platform agnostic. In this respect KDE and Gnome apps could easily become more mutually integrated than windows programs are. (Of course, once the interface has stabilized it will be nice to have Dashboard recoded natively :-)"
    author: "Anonymous Coward"
  - subject: "Re: Dashboard cluepackets?"
    date: 2003-09-12
    body: "I had an informal look, and it seems the first step needed would be to create a backend for it.  The backend basically takes the clues, and adds it's own clues - this is what dashboard is all about.\nAn example for this would the whole kontact thing.  So then gaim or the kde equiv. would throw a clue packet for a particular person, then kontact would add information about that person, to be shown in dashboard.\n\nOnce that is up and running, a port for the actual dashboard client would be nice, and so on.\n\nPersonally, I think dashboard will be ignored for ages, then almost overnight become huge.\n\nAt the moment unfortunetly there seems to be a pause in development on it.\n\n"
    author: "John Flux"
  - subject: "RDP support"
    date: 2003-09-11
    body: "I saw in the Feature plan that RDP support for KRDC is still listed as \"In progress\", though I finished it about two months ago. Where should I report it as being finished?"
    author: "Arend jr."
  - subject: "Re: RDP support"
    date: 2003-09-11
    body: "You can change it yourself (I guess you have a cvs account). \n\nCheckout developer.kde.org/development-versions/kde-features.xml and just commit your changes."
    author: "cloose"
  - subject: "Re: RDP support"
    date: 2003-09-11
    body: "Yes, it worked. Thanks."
    author: "Arend jr."
  - subject: "removing posts"
    date: 2003-09-11
    body: "Hello slashdot user: http://slashdot.org/~ThyTurkeyIsDone.\n\njust posted this here: http://developers.slashdot.org/comments.pl?sid=78055&cid=6929804\n\nIn stead of removing posts, why doesn't the dot move to a score based forum?"
    author: "ac"
  - subject: "Re: removing posts"
    date: 2003-09-11
    body: "There really isn't the slightest need to provide a support channel for the garbage you've linked to above, IMHO."
    author: "Navindra Umanee"
  - subject: "Re: removing posts"
    date: 2003-09-11
    body: "Because it is just an excuse for stupid trolls. \"If you don't like my comment, mod it down\"..."
    author: "Jeff Johnson"
  - subject: "Re: removing posts"
    date: 2003-09-11
    body: "he also posted it here:\n\nhttp://slashdot.org/comments.pl?sid=76833&cid=6845043\nand\nhttp://slashdot.org/comments.pl?sid=76772&cid=6840235\n\nWow, what is wrong with this guy? This was funny a long time ago, but it's just redundant now."
    author: "anon"
  - subject: "screenshots"
    date: 2003-09-11
    body: "Can someone provide us with some screenshota of new cool things not-yet shown on dot.kde?\nI personally would like to see the WYSIWYG mode from Quanta."
    author: "Iuri Fiedoruk"
  - subject: "Re: screenshots"
    date: 2003-09-11
    body: "WYSIWG mode is still work in progress, and we hope that it will be ready for 3.2 (this is why it is in the feature plan), but it is NOT a promise. \n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: screenshots"
    date: 2003-09-11
    body: "es, I know, but I still want to see it ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: screenshots"
    date: 2003-09-12
    body: "Well you can throw Quanta into preview mode and have a look and it's a lot like that. ;-) There are a number of things that need to be done underneath to bring up anything substantially different in visuals there. There's not a lot to see but a lot of code. There are other things that are more visual at this time. For instance there is the new attribute inspector that lets you edit tags in either mode. We also have made KFileReplace into a plugin and it is in CVS right now. Some of the things it does are the most requested features for Quanta. KFileReplace is the ultimate multi-file/directory, multi-line, wildcard enabled find and replace I've ever seen. It's very cool. Also plugins can be loaded in file tabs now. I'm attaching a snapshot of the CVS version from a little while back running in KDE 3.1.x. Note that the plugin toolbar was something experimental I did but it is easy to do. Our most popular plugins are on the main toolbar now.\n\nEnjoy!"
    author: "Eric Laffoon"
  - subject: "Re: screenshots"
    date: 2003-09-12
    body: "It's good to hear that easier tag dialog will get.\nI've made sometime ago a better body dialog that never got into Quanta and I think most dialogs miss important tag options today. Maybe now, we can improve this part in Quanta ;)"
    author: "protoman"
  - subject: "Re: screenshots"
    date: 2003-09-14
    body: "> It's good to hear that easier tag dialog will get.\n\nMaybe that lost something in the translation... but yes, we have lots of options now including full auto completion with code hinting, the tag dialogs and the new attribute inspector which lets you navigate and edit tags. Next on the list is to get Visual Page Layout ready.\n\n> I've made sometime ago a better body dialog that never got into Quanta and I think most dialogs miss important tag options today. Maybe now, we can improve this part in Quanta ;)\n\nI have to say something about this because it's really frustrating. I'm sure you'll understand. First off, the old body dialog was a custom hand built dialog that was not at all compliant to any DTD or good design criteria. All the original dialogs were very HTML 3.2 and very much in need of deprication. All the current dialogs, with a few excpetions, are generated from Quanta's tagxml. I admit some lack non compliant things like name attributes for images however I tried to add all W3C compliant attributes with tooltips explaining anything depricated. Somewhere in a cleanup someone *cough*Andras*cough* must have removed some of that but our current dialogs have been reviewed extensively. It's entirely unfair to say that most are missing important options! Some, in our release versions, have attributes missing.\n\nCurrently in our CVS we have 21 DTDs, psuedo DTDs and Schemas supported. Thesehave been worked on and reviewed extensively and include several HTML and XHTML dialects versioned for strict, transitional and frameset. Seriously, I doubt there are many tools even close for hundreds of dollars. I just have to say, if you found something not right you need to file a bug report or post it on our user mailing list so we can address it. \n\nAs a further point... you can edit, update, change, fold spindle and mutilate dialogs to your heart's content in real time. All you need is a folder in $KDEHOME/share/apps/quanta/tags/[dialect] which you insert the tag.tag file such as body.tag and Quanta will substitute that tag. Our upcoming version has (in CVS) a tagxml editing environment so it's easy to edit and create here.\n\nOne last thing. You can also create Kommander dialogs. I've attached a screenshot of our new Quick Start dialog. What is cool about this is that you can also edit and change it to meet your needs using the Kommander dialog editor. It's built visually.\n\nSo please excuse me for saying this... but it's so frustrating to see people saying \"maybe now we can change or customize X in Quanta\" when I've been saying for the last year that we have been working hard to make it so you can. If more people understood this they would realize you don't need to know any C++ to be very helpful contributing to our project. We need these types of contributions worst but everybody still thinks they need to know C++ to help us."
    author: "Eric Laffoon"
  - subject: "Re: screenshots"
    date: 2003-09-14
    body: "I understand, I said that just because I get a bit sad when I did the manual work of editing the body xml for tag edition and it never got in Quanta I don't know why.\nThat's why I said editing those dialogs is a great thing, you can customize your dialogs even that they never get into the main branch :)\n\nMy complain about quanta is that there are a lot of good ideas in sourceforge project page that never got attention, BUT I'm no one to complain because I don't pay for Quanta and don't help developing it (I hope I do in the future, I'm already learning C and I hope next year I get C++ knowledge enought to help out), so just ignore my trolling, sorry."
    author: "Iuri Fiedoruk"
  - subject: "Re: screenshots"
    date: 2003-09-15
    body: "> I understand, I said that just because I get a bit sad when I did the manual work of editing the body xml for tag edition and it never got in Quanta I don't know why.\n\nDid you send it to me? If it fell through the cracks I apologize. That can happen with a million and three things going on. If it was unacceptable for some reason then pretend I didn't answer this. ;-) You would really need to check CVS to see what's happening. If you see something wrong and it's not addressed you have my permission to bug me until it is. If I have a reason not to want to do it I will tell you. I'm not shy.\n\n> That's why I said editing those dialogs is a great thing, you can customize your dialogs even that they never get into the main branch :)\n\nAnd that's the great thing because there are reasons we don't want to do some things and you shouldn't need to care. I was just trying to understand what happened.\n \n> My complain about quanta is that there are a lot of good ideas in sourceforge project page that never got attention, BUT I'm no one to complain because I don't pay for Quanta and don't help developing it (I hope I do in the future, I'm already learning C and I hope next year I get C++ knowledge enought to help out), so just ignore my trolling, sorry.\n\nI never ignore users (I respond to nearly every email) and I espeically try not to ignore potential developers. I don't know what ideas you're referencing but I did update the page a while back. We're also in work on a number of things and others we just have in the queue until we have someone there. In order to be fair you need to be more specific. Feel free to email me.\n\nI should point out that I did not know C++ when I started on this project and I have had to learn it. If you really want to get involved don't think you need someone to walk you through what a class or a virtual function is and hand you a piece of paper saying you're a programmer. (not that there's anything wrong with that) There are lots of resources and you get get on the developer list and kde-devel, read, practice and ask questions. In practice motivation always beats skill. Skill without action does nothing and motivation will eventually succeed. So take action."
    author: "Eric Laffoon"
  - subject: "Re: screenshots"
    date: 2003-09-15
    body: "I did posted on patches section on sourceforge, no hard feelings on this, my point is just that users should not be dependant of quanta developers all the time. We must help quanta development in any way we can, and if not, use this versatility of quanta to make for us a custom version that satisfies our needs. :)\n\nSorry my english, I just existed from a.. well, a english class, hehehe, I'm a little sleepy :)\n\nAnyway, I think you're a nice person and is a example as developers should hear their users Eric.\n\nAnyway, to put a end on the discussion, how can we help to improve quanta tag edition dialogs? I understand that you are only accepting W3C rules, is there a place where we can learn all tag options, compare with quanta and map the missing ones, so that they can be place in quanta?\n\nThanks in advance and reguards."
    author: "Iuri Fiedoruk"
  - subject: "ksvg ?"
    date: 2003-09-11
    body: "I saw that ksvg has been moved from kdenonbeta to kdegraphics. Does this mean it will be included in kde 3.2 final.\n\nAnyway, thanks for the great work to all kde developers."
    author: "Tom Gufler"
  - subject: "Re: ksvg ?"
    date: 2003-09-12
    body: "Yes it will be in kde 3.2 release."
    author: "cartman"
  - subject: "kde distribution files"
    date: 2003-09-11
    body: "So the file base for the kde seems to grow for every generation, I just guess will the kde-4.0 gonna have even more packages?\n\nCould there be some sort of package combination scheme executed during the kde-3.3->4.0 move?\n\nFor example releasing arts within kdelibs and kdesdk could include kdevelop and quanta... Why not, sure as thay are separate as the teams are working somewhat separate, but that would help the dependancy hell (overlapping eliminated) and even might start combining of those separate user interfaces (kdevelop looks pretty similar to quanta, so why not combine them?)?\n"
    author: "Nobody"
  - subject: "Re: kde distribution files"
    date: 2003-09-11
    body: "WYSIWG mode is still work in progress, and we hope that it will be ready for 3.2 (this is why it is in the feature plan), but it is NOT a promise. \n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: kde distribution files"
    date: 2003-09-11
    body: "Sorry, I posted under wrong thread..."
    author: "Andras Mantia"
  - subject: "Re: kde distribution files"
    date: 2003-09-11
    body: "Kdevelop & Quanta are similar (and will be even more similar), but the codebase is quite different nowadays (as both were rewritten independently and KDevelop's core is completely changed, while Quanta is partly based on an older KDevelop core) so merging them wouldn't be a trivial task. I don't say it will never happen, but right now it's not possible. But we will try to reuse at least the MDI framework (KMDI) and some KDevelop plugins in order to reduce code duplication and bloat. For me this seems to be somewhat similar to the KMail/KNode case, altough I don't know what are the code differences between them. For the user it seems to be trivial to merge the two products, but for the developer it isn't.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: kde distribution files"
    date: 2003-09-11
    body: "Understand it perfectly, it's pretty common to make software that acts exactly the same way, while being 80% different code.\n\nBut what about that file regrouping, as I bet virtually everyone that install kdevelop install also atleast kdesdk and I bet most install also the quanta... So why not bunch them all to single file (to kdesdk?)?\n\nIt might be simpler to start clearing duplicate code first and then start synergizing the rest after that... ?\n"
    author: "Nobody"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "> Understand it perfectly, it's pretty common to make software that acts exactly the same way, while being 80% different code.\n\nActually that's not really an easy statement to agree or disagree with. For instance looking at Kdevelop and Quanta, they both use the same base of KDE/Qt libraries. It is a coincidence that Quanta is based on Kdevelop 1.0 but both have been largely rewritten since then. Kdevelop also has a lot more developers so they have the luxury of approaches difficult for us. We also use similar things like editing components and plugins. Beyond that, the way software is so inherently flexible, there is no re-use that isn't designed to be reused. Appearances mean virtually nothing WRT base classes and approaches.\n \n> But what about that file regrouping, as I bet virtually everyone that install kdevelop install also atleast kdesdk and I bet most install also the quanta... So why not bunch them all to single file (to kdesdk?)?\n\nAre you volunteering some time here? (I have to wonder why I'm answering \"nobody\" but I'm sure it's of interest to sombody) First, we're talking a huge module. Second, translators only need kdesdk. Third, I only wish more kdevelop users/developers tried Quanta because they are doing cool things for web related stuff for kdevelop, but the two packages are somewhat variant in their missions. We'd love input from developers we didn't have to explain KDE/Qt to. There are other issues, but I'd like to be able to develop the Quanta module into a weblication suite. We already have Kommander in there. Eventually more related and pluggable apps could be there. Finally there are a lot of web developers who don't want to ever delve into Kdevelop or all the things it has for application development. It's also a big source package. In short order you're talking about the biggest module in KDE. Not everyone has the bandwidth.\n \n> It might be simpler to start clearing duplicate code first and then start synergizing the rest after that... ?\n \nYeah... that sounds really good. Unfortunately there are problems with it. First of all what's a one for one duplicate and what dependency issues does it create? Not to mention... which one do we keep and which one do we throw away? I don't want to disparage your suggestion because your objective is not without merit. Your approach however is not pragmatic. You're talking about a whole bunch of work!\n\nAt the recent KDE get together some Kdevelop developers and Andras discussed this very thing and Andras and I discussed it. Let me give you my position, which I think is fairly close to most people involved. Ideally I believe that both applications have different missions and that as one moves more to the mission of the other it just won't do it as well. It can do it adequately, but not excellently. We insist on being excellent as does kdevelop so we need to not compromise some things. I think it is essential to have the different packages. My personal opinion is a lot of what they do supporting web development has to do largely with their familiarity with that tool and wanting to use it. However much of it was initiated when Quanta was not part of KDE formal and some started when it was not yet clear I would not allow Quanta to die.\n\nAfter all these arguments in conflict with what you said you might be surprised that I very much agree with you in principle. I really believe that the two code bases need to become as interchangeable as possible. Frankly I'm really impressed with their plugin system and a number of other things... we just didn't have the resources to quite match it. I still think our plugin system was an elegant solution given the problem. Ideally I'd like to be able to use their plugins and gravitate toward more interchangability of the code bases. This would mean things developed for either application could be interchanged. I'd like to adopt other things from kdevelop too and I want to work together with their developers and keep an open dialog. Of course what would be even cooler is getting more developers involved so we could have the luxury of doing more.\n\nOver all it's far less of an efficiency matter because that ignores progect directions and differences inherent to objectives. It is more of a synery issue where various aspects can merge and realize benefits. Expect it to take some time."
    author: "Eric Laffoon"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "How I and my research team use both kdevelop and quanta is to make basic C/C++ and XHTML/XML stuff so I personally haven't been able to slice enought time to start looking at actual kdevelop/quanta source codes, but after projects I'm involved come to production level, then my team will need to get something new to do until there is next generation of hardware pruction line ready thus getting seriously involved with the KDE has been discussed between my team and organization big cheeses...\n\nSo at the earliest for the kde-3.3/4.0 generation we should be ready to make some tasks kde-people need to get done without any funds being blown. ;-)\n\nOf course we are more experienced with hardware design than software, so our software expertise is certainly not top-notch, but I bet there is enought janitor stuff to be done...\n\n\nPs. The name comes from the fact that I'm \"nobody\" until we can come to the public (one part of our project include full Linux distribution) with our technologies. :-)\n"
    author: "Nobody"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "> How I and my research team use both kdevelop and quanta is to make basic C/C++ and XHTML/XML stuff so I personally haven't been able to slice enought time to start looking at actual kdevelop/quanta source codes, but after projects I'm involved come to production level, then my team will need to get something new to do until there is next generation of hardware pruction line ready thus getting seriously involved with the KDE has been discussed between my team and organization big cheeses...\n\nThat sounds great! BTW my condolences on working with Basic. ;-) There's a lot of stuff in the works on Quanta, as well as in our CVS, that most people don't know about. One delay working to accomplish the task of partially merging the projects is that we don't want to stop feature development for a major point release. (Believe it or not we only have a few developers.) Minor point releases can't add new UI objects that need translation. Clearly the ability to get a tool that does what you want making you more productive is far more compelling than any cost per seat analysis because developer time is always the most expensive part of the equation. I'm frankly surprised more companies haven't moved to commit a fraction of their IT expense to OSS projects that have the potential to slash costs over time. If I can be any help let me know.\n \n> So at the earliest for the kde-3.3/4.0 generation we should be ready to make some tasks kde-people need to get done without any funds being blown. ;-)\n\nIf you would like to do collaborative efforts on Quanta please give me as much lead time as you can, as well as your objectives. We are currently in a feature freeze for KDE 3.2. It would be nice not to have to maintain a branch solely for lack of planning. I could also answer questions addressing current timelines and plans which might be helpful to your people too.\n \n> Of course we are more experienced with hardware design than software, so our software expertise is certainly not top-notch, but I bet there is enought janitor stuff to be done...\n\nActually we designed Quanta so that a whole lot of things can be done with XML, scripting (any language the shell can interpret) and Kommander dialogs. Anyone with C++ abilities will probably like KDE programming a lot. Our policy is that if you can contribute messy code that's faster to clean up than write, we like you. Even better if we can show you how to clean it up. ;-)\n \n> Ps. The name comes from the fact that I'm \"nobody\" until we can come to the public (one part of our project include full Linux distribution) with our technologies. :-)\n \nYour secret's safe with me, but I knew that if somebody was nobody then nobody had to be somebody. Seriously, we're all about building the best web tool in the world. So as long as you don't tell me your CEO is a loud bald guy who does the monkey dance we're off to a good start. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "After we get our stuff polished for the masses, then we get in touch for more serious chat about it...\n\nKeep it kool! ;-)\n\n\nPs. Fortunate enought to be able to forget (replaced with HDL, C/C++ and friends) most of the Basic/Pascal/LISP stuff I needed at school. :-)\n"
    author: "Nobody"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "I think KDE should be split up a LOT more than it is.  There are so many things that are included in the base packages that I don't want at all, so I have to do:\n\nDO_NOT_COMPILE='a b c ..' ./configure ..\n\nwhenever I'm building things, just to get the 1 or 2 useful apps that are included.\n\nIt'd make life (for me :) much easier if each program had its own tarball.  Some things seem to be included in KDE just because that's the way it's always been, and we're stuck with a boatload of programs many of us don't use.\n\nYou know, the ideal situation would be a web interface that has you check off all the programs you want.  It'd check dependencies, then generate one nice big tarball of the programs you want.  A single configure and make and you're set.  I know that'd be taxing on the server and hard to maintain, but man, I would love that.\n\nI'd donate a couple hundred bucks if such a system were put in place.. :)"
    author: "KDE User"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "Is there some way to get a list of the a b and c in that configure command?"
    author: "AC"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "> It'd make life (for me :) much easier if each program had its own tarball. Some things seem to be included in KDE just because that's the way it's always been, and we're stuck with a boatload of programs many of us don't use.\n\nIt's your distro's peragotive to split KDE packages into smaller packages. If they don't, it's their decision and perhaps you should switch distros.. debian does this for example. "
    author: "fault"
  - subject: "Re: kde distribution files"
    date: 2003-09-12
    body: "I know that's how you think it should be, and that's how it currently is.\n\nI'm saying that's now how I want it to be.\n\nYou can't argue with that cause it's an opinion."
    author: "KDE User"
  - subject: "KDEPIM"
    date: 2003-09-12
    body: "So, what are the big improvements to KDEPIM?\n\nI'm especially curious wether there will be more documentation on KARM (and it's logfile) or wether it'll have export capabilities..\n"
    author: "Me"
  - subject: "KWin shadows"
    date: 2003-09-12
    body: "I understand that the shadows under windows patch was included in the CVS but i cannot find it anywhere on KDE 3.2 alpha 1. Am i doing something wrong ? I have to uncomment some stuff in config files or add some stuff in kwin config file ? I need that cool thing :("
    author: "KrAzY_BoY"
  - subject: "Re: KWin shadows"
    date: 2003-09-12
    body: "Who says that it's included?"
    author: "Anonymous"
  - subject: "Re: KWin shadows"
    date: 2003-09-12
    body: "The shadows are a major hack and way to slow to be included. I think there is a patch at kde-look.org that does this but we already had numerous bug reports where kwin was crashing due to this patch, so I wouldn't recommend it\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: KWin shadows"
    date: 2003-09-12
    body: "I've been using the patch with HEAD for a few months without many problems. Earlier versions of the patch were very buggy indeed.\n\nHowever, as expected, it needs a fairly good computer. I don't know how good; it's perfectly fast on my Athlon XP 2200+"
    author: "fault"
  - subject: "HTML in KMail"
    date: 2003-09-12
    body: "Good to hear KDE keeps moving forward. I use it every day. Will we finally have HTML formatting on outgoing messages in KMail in this release? Almost every GUI e-mail client has it now-a-days...\n\n/Jeroen"
    author: "Jeroen"
  - subject: "Re: HTML in KMail"
    date: 2003-09-12
    body: "Look at \n\nhttp://bugs.kde.org/show_bug.cgi?id=4202\n\nAs you can see there is progress here :)\n\nCarsten"
    author: "Carsten Niehaus"
  - subject: "Where to go..."
    date: 2003-09-12
    body: "Got compilation errors in kdegraphics and kdemultimedia (didn't get further than that) with the konstruct script. Where should I file them?\n\nSince this is not a real release (I guess) I have the feeling bugs.kde.org is not the right place to go."
    author: "Trip"
  - subject: "KMail ?"
    date: 2003-09-13
    body: "Um... this is gonna sound stupid for you guys that are tracking CVS religiously :), but where is KMail? Isn't it supposed to be in the kdenetwork package (that's where it was in the 3.0.x days) ? I just downloaded it last night, and no kmail. Is it in kdepim now? Or do I have to get it from CVS manually?"
    author: "Alex Ruse"
  - subject: "Re: KMail ?"
    date: 2003-09-13
    body: "Yes, it's in kdepim."
    author: "cm"
  - subject: "Re: KMail ?"
    date: 2003-09-13
    body: "In kdepim. It was moved since the 3.1 release. You still need kdenetwork for some libraries I believe.\n\nDerek"
    author: "Derek Kite"
  - subject: "w00t"
    date: 2003-09-13
    body: "Woah, do you (KDevelop) guys actually double check to see if the released code actually builds correctly before doing a release??\n\n(alpha #6 doesn't)\n\nBad, bad, bad..\n"
    author: "blegh!"
  - subject: "Re: w00t"
    date: 2003-09-14
    body: "okay, nailed down the issue..\n\nIt just didn't work with my KDE CVS (dated: 2003-07-25).\nAfter fiddling a bit with the auto* files, specifically removing the check for kmdi and setting it to make use of qmdi (included with KDevelop/Gideon) instead almost fixed it.\n\nWith this qmdi library, there was only one issue left (4 errors about some methods not being found)."
    author: "blegh!"
  - subject: "build kdebase 3.1.91 failed"
    date: 2003-09-14
    body: "I use qt 3.2.1 to build kde 3.2 alpha1, but can't build kdebase, who can help me\nbelow is the error message:\nmake[3]: Entering directory `/opt/src/kdebase-3.1.91/kcontrol/konqhtml'\nif /bin/sh ../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../libkonq -I../../kcontrol/input -I/opt/kde/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT -z combreloc -falign-functions=4 -fomit-frame-pointer -mfancy-math-387 -O3 -march=i686 -mcpu=i686 -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -DNDEBUG -DNO_DEBUG -O2 -z combreloc -falign-functions=4 -fomit-frame-pointer -mfancy-math-387 -O3 -march=i686 -mcpu=i686 -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION -D_GNU_SOURCE  -MT kcm_konqhtml_la.all_cpp.lo -MD -MP -MF \".deps/kcm_konqhtml_la.all_cpp.Tpo\" \\\n  -c -o kcm_konqhtml_la.all_cpp.lo `test -f 'kcm_konqhtml_la.all_cpp.cpp' || echo './'`kcm_konqhtml_la.all_cpp.cpp; \\\nthen mv \".deps/kcm_konqhtml_la.all_cpp.Tpo\" \".deps/kcm_konqhtml_la.all_cpp.Plo\"; \\\nelse rm -f \".deps/kcm_konqhtml_la.all_cpp.Tpo\"; exit 1; \\\nfi\nIn file included from kcm_konqhtml_la.all_cpp.cpp:7:\nkhttpoptdlg.cpp: In member function `virtual void KHTTPOptions::load()':\nkhttpoptdlg.cpp:45: warning: `languages' is deprecated (declared at\n   /opt/kde/include/klocale.h:1079)\nkhttpoptdlg.cpp: In member function `virtual void KHTTPOptions::defaults()':\nkhttpoptdlg.cpp:61: warning: `languages' is deprecated (declared at\n   /opt/kde/include/klocale.h:1079)\nIn file included from advancedTabDialog.cpp:25,\n                 from kcm_konqhtml_la.all_cpp.cpp:13:\n/opt/kde/include/qslider.h: At global scope:\n/opt/kde/include/qslider.h:69: warning: comma at end of enumerator list\n/opt/kde/include/qslider.h:69: syntax error before numeric constant\n/opt/kde/include/qslider.h:74: missing ';' before right brace\n/opt/kde/include/qslider.h:76: syntax error before `*' token\n/opt/kde/include/qslider.h:77: syntax error before `,' token\n/opt/kde/include/qslider.h:78: syntax error before `int'\n/opt/kde/include/qslider.h:80: destructors must be member functions\n/opt/kde/include/qslider.h:82: `Orientation' was not declared in this scope\n/opt/kde/include/qslider.h:82: virtual outside class declaration\n/opt/kde/include/qslider.h:82: variable or field `setOrientation' declared void\n/opt/kde/include/qslider.h:83: syntax error before `)' token\n/opt/kde/include/qslider.h:84: virtual outside class declaration\n/opt/kde/include/qslider.h:85: non-member function `bool tracking()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:86: virtual outside class declaration\n/opt/kde/include/qslider.h:88: non-member function `int sliderStart()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:89: non-member function `QRect sliderRect()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:90: non-member function `QSize sizeHint()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:94: non-member function `QSizePolicy sizePolicy()'\n   cannot have `const' method qualifier\n/opt/kde/include/qslider.h:95: non-member function `QSize minimumSizeHint()'\n   cannot have `const' method qualifier\n/opt/kde/include/qslider.h:97: `TickSetting' was not declared in this scope\n/opt/kde/include/qslider.h:97: virtual outside class declaration\n/opt/kde/include/qslider.h:97: variable or field `setTickmarks' declared void\n/opt/kde/include/qslider.h:98: syntax error before `)' token\n/opt/kde/include/qslider.h:100: virtual outside class declaration\n/opt/kde/include/qslider.h:101: non-member function `int tickInterval()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h: In function `int tickInterval()':\n/opt/kde/include/qslider.h:101: `tickInt' undeclared (first use this function)\n/opt/kde/include/qslider.h:101: (Each undeclared identifier is reported only\n   once for each function it appears in.)\n/opt/kde/include/qslider.h: At global scope:\n/opt/kde/include/qslider.h:103: non-member function `int minValue()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:104: non-member function `int maxValue()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:107: non-member function `int lineStep()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:108: non-member function `int pageStep()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:111: non-member function `int value()' cannot have `\n   const' method qualifier\n/opt/kde/include/qslider.h:113: syntax error before `public'\n/opt/kde/include/qslider.h:120: syntax error before `protected'\n/opt/kde/include/qslider.h:126: syntax error before `protected'\n/opt/kde/include/qslider.h:145: syntax error before `private'\n/opt/kde/include/qslider.h:152: non-member function `int positionFromValue(int)\n   ' cannot have `const' method qualifier\n/opt/kde/include/qslider.h:153: non-member function `int valueFromPosition(int)\n   ' cannot have `const' method qualifier\n/opt/kde/include/qslider.h:157: non-member function `int available()' cannot\n   have `const' method qualifier\n/opt/kde/include/qslider.h:158: non-member function `int goodPart(const\n   QPoint&)' cannot have `const' method qualifier\n/opt/kde/include/qslider.h:166: 'State' is used as a type, but is not defined\n   as a type.\n/opt/kde/include/qslider.h:169: 'TickSetting' is used as a type, but is not\n   defined as a type.\n/opt/kde/include/qslider.h:170: `int tickInt' used prior to declaration\n/opt/kde/include/qslider.h:171: 'Orientation' is used as a type, but is not\n   defined as a type.\n/opt/kde/include/qslider.h:173: syntax error before `private'\n/opt/kde/include/qslider.h:176: syntax error before `&' token\n/opt/kde/include/qslider.h:181: no `bool QSlider::tracking() const' member\n   function declared in class `QSlider'\n/opt/kde/include/qslider.h:186: no `Qt::Orientation QSlider::orientation()\n   const' member function declared in class `QSlider'\n/opt/kde/include/qslider.h: In member function `Qt::Orientation\n   QSlider::orientation() const':\n/opt/kde/include/qslider.h:187: `orient' undeclared (first use this function)\n/opt/kde/include/qslider.h: At global scope:\n/opt/kde/include/qslider.h:191: no `int QSlider::sliderStart() const' member\n   function declared in class `QSlider'\n/opt/kde/include/qslider.h:196: no `void\n   QSlider::setSizePolicy(QSizePolicy::SizeType, QSizePolicy::SizeType, bool)'\n   member function declared in class `QSlider'\nmake[3]: *** [kcm_konqhtml_la.all_cpp.lo] Error 1\nmake[3]: Leaving directory `/opt/src/kdebase-3.1.91/kcontrol/konqhtml'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/opt/src/kdebase-3.1.91/kcontrol'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/opt/src/kdebase-3.1.91'\nmake: *** [all] Error 2"
    author: "younker"
  - subject: "Re: build kdebase 3.1.91 failed"
    date: 2003-09-14
    body: "Don't use the experimental \"--enable-final\". Several Alpha tarballs don't compile with it."
    author: "Anonymous"
  - subject: "Problems with kscd"
    date: 2003-09-14
    body: "Everything compiles just fine in the Alpha6 release, only problem for me is kscd!!\nIt compiles just fine, but when I try to start it, it says:\nkscd: relocation error: /usr/local/lib/libkdeui.so.4: undefined symbol: _ZN9QGroupBox10setEnabledEb\nAnyone know what to do about this?\nThanks in advance"
    author: "Jarl E. Gjessing"
  - subject: "Digital cameras'n'scanners"
    date: 2003-09-17
    body: "So is the \"kamera\" the only official way to access digital cameras even on the future kde's?\n\nWhy not implement gphoto2-interface to the kooka, as they are actualy pretty similar thingies (camera and scanner)... ?\n\nOr what about trying to get some 3rd party camera software maker (I would prefer digikam) to make their work integrated to the kde-3.2?\n"
    author: "Nobody"
  - subject: "Wifi cannot build from kdenetwork"
    date: 2003-09-21
    body: "I grasp the unstable konstruct to build KDE 3.2. \nEverything was ok so far but halted in the wifi module.\nHere is the error msg:\n\nmake[5]: Entering directory `/usr/share/konstruct/kde/kdenetwork/work/kdenetwork-3.1.91/wifi'\nif g++ -DHAVE_CONFIG_H -I. -I. -I.. -I/root/kde3.2-alpha1/include -I/usr/X11R6/include  -I/usr/kerberos/include  -DQT_THREAD_SUPPORT -I/root/kde3.2-alpha1/include -I/usr/X11R6/include -I/root/kde3.2-alpha1/include -I/usr/X11R6/include -D_REENTRANT -D_FILE_OFFSET_BITS=64  -D_REENTRANT -D_GNU_SOURCE -DTHREADS_HAVE_PIDS -DDEBUGGING -fno-strict-aliasing -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -I/usr/include/gdbm  -I/usr/lib/perl5/5.8.1/i386-linux-thread-multi/CORE   -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -O2 -I/root/kde3.2-alpha1/include -I/usr/X11R6/include -L/root/kde3.2-alpha1/lib -L/usr/X11R6/lib -O2 -pipe -I/root/kde3.2-alpha1/include -I/usr/X11R6/include -L/root/kde3.2-alpha1/lib -L/usr/X11R6/lib -O2 -pipe -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT stuff.o -MD -MP -MF \".deps/stuff.Tpo\" \\\n  -c -o stuff.o `test -f 'stuff.cpp' || echo './'`stuff.cpp; \\\nthen mv \".deps/stuff.Tpo\" \".deps/stuff.Po\"; \\\nelse rm -f \".deps/stuff.Tpo\"; exit 1; \\\nfi\nstuff.cpp:218: error: default argument given for parameter 1 of `\n   Interface_Talker::Interface_Talker(QString)'\nstuff.h:84: error: after previous specification in `\n   Interface_Talker::Interface_Talker(QString)'\nstuff.cpp: In member function `virtual bool\n   Interface_Talker_wirelessextensions::give_device_freq(float&)':\nstuff.cpp:252: warning: unused parameter `float&freq'\nstuff.cpp: In member function `virtual bool\n   Interface_Talker_wirelessextensions::give_mode(int&)':\nstuff.cpp:257: warning: unused parameter `int&mode'\nstuff.cpp: In member function `virtual bool\n   Interface_Talker_wirelessextensions::give_key(QString&, int, int)':\nstuff.cpp:262: warning: unused parameter `QString&key'\nstuff.cpp:262: warning: unused parameter `int size'\nstuff.cpp:262: warning: unused parameter `int flags'\nstuff.cpp: In member function `virtual bool\n   Interface_Talker_wirelessextensions::give_AP_info(QString&, QString&)':\nstuff.cpp:272: warning: unused parameter `QString&mac'\nstuff.cpp:272: warning: unused parameter `QString&ip'\nstuff.cpp: In member function `virtual bool\n   Interface_Talker_wirelessextensions::give_current_quality(int&, int&, int&)\n   ':\nstuff.cpp:287: warning: unused parameter `int&sig'\nstuff.cpp:287: warning: unused parameter `int&noi'\nstuff.cpp:287: warning: unused parameter `int&qua'\nmake[5]: *** [stuff.o] Error 1\nmake[5]: Leaving directory `/usr/share/konstruct/kde/kdenetwork/work/kdenetwork-3.1.91/wifi'\nmake[4]: *** [all-recursive] Error 1\nmake[4]: Leaving directory `/usr/share/konstruct/kde/kdenetwork/work/kdenetwork-3.1.91/wifi'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/usr/share/konstruct/kde/kdenetwork/work/kdenetwork-3.1.91'\nmake[2]: *** [all] Error 2\nmake[2]: Leaving directory `/usr/share/konstruct/kde/kdenetwork/work/kdenetwork-3.1.91'\nmake[1]: *** [build-work/kdenetwork-3.1.91/Makefile] Error 2\nmake[1]: Leaving directory `/usr/share/konstruct/kde/kdenetwork'\nmake: *** [dep-../../kde/kdenetwork] Error 2\n\nSo what's the problem I have got? I have a internal wi-fi\ndevice in my notebook.\n"
    author: "Xavier"
---
I've finally managed to get the last bits of the <a href="http://download.kde.org/download.php?url=unstable/3.1.91/src/">KDE 3.2 Alpha 1</a> (codenamed &quot;Brokenboring&quot;) including <a href="http://www.kdevelop.org/">KDevelop 3.0 Alpha 6</a>
on the ftp server.  The <a href="http://www.kde.org/mirrors/ftp.php">mirrors</a> should soon pick it up.
<!--break-->
<p>There won't be any binary packages for this release because the KDE &quot;Pi&quot;
release is coming out soon. Everyone using Brokenboring is asked to
compile it with --enable-debug, so that we can get valuable feedback. There is a new <a href="http://developer.kde.org/build/konstruct/unstable/">unstable version</a> of <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> also available to install it.
</p>
<p>
Other than that, feel free to play around with things, <a href="http://bugs.kde.org/">check if your bugs</a>
are still there or if there are <a href="http://www.kde.org/support/">places where you can help</a>. Check
<a href="http://developer.kde.org/development-versions/kde-3.2-features.html">the KDE 3.2 feature plan</a>
for things to look for.
</p> 
<p>
The code is quite rough in many places, but many of the developers
use it on a daily base and <a href="http://pim.kde.org/index.php">kdepim</a> has improved so much that you can't live without it once you've tried it. :)
</p>
<p>
Thanks to everyone who convinced me that the code is good enough for
a release when I was tempted to drop the idea altogether. Just one final wish: check
for duplicates before you file bug reports and compile with debug information enabled
before reporting crashes -- you might find tons of bugs, but with
debugging information the chances we can fix them are much higher.
</p>