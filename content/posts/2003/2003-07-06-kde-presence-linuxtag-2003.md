---
title: "KDE Presence at LinuxTag 2003"
date:    2003-07-06
authors:
  - "kstaerk"
slug:    kde-presence-linuxtag-2003
comments:
  - subject: "KDE 3.2"
    date: 2003-07-06
    body: "KDE 3.2 is gonne rock.\nhope it is ready soon."
    author: "Mark Hannessen"
  - subject: "Hope"
    date: 2003-07-06
    body: "Hope you will not forget to ask your governmental representatives about EU software patents. The German ministry of justice played an active role in the EU in favour of patent regulation. "
    author: "Hein"
  - subject: "Re: Hope"
    date: 2003-07-06
    body: "Well thought!"
    author: "SPArgh"
  - subject: "Re: Hope"
    date: 2003-07-08
    body: "I'd be surprised to hear LinuxTag is in favor of those patent regulation changes. Please remember that on the EU level out of the three sections, Justice, Economy and Art, only the Justice department is in favor of those changes. Unfortunatelly that department is also responsible for it. Judging from the responses I got from our country's representatives in the parliament all of them are well aware of the impact of these possible changes and that the majority of Europe's economy doesn't want them. One member told me that the changes would have been done in 1998 already if there were no resistance, which obviously wasn't the case."
    author: "Datschge"
  - subject: "Kontact <-> KDE Kolab Client"
    date: 2003-07-07
    body: "It's nice to see Kolab progress. Will any functionality of the KDE Kolab Client be included in Kontact 1.0?"
    author: "Anonymous"
  - subject: "Re: Kontact <-> KDE Kolab Client"
    date: 2003-07-08
    body: "Yes."
    author: "Datschge"
  - subject: "Re: Kontact <-> KDE Kolab Client"
    date: 2003-07-08
    body: "Every?"
    author: "Anonymous"
  - subject: "Re: Kontact <-> KDE Kolab Client"
    date: 2003-07-08
    body: "Afaics, yes."
    author: "Datschge"
  - subject: "Re: Kontact <-> KDE Kolab Client"
    date: 2004-05-10
    body: "hi,\ni wanna know how i can install kolab in Suse 9.1.\nI just know how to make it with Debian.\nI wanna build an intranet webserver"
    author: "maurice"
  - subject: "Karlsruhe?"
    date: 2003-07-09
    body: "Ugh! Karlsruhe of all places! Couldn't they chose a normal city??\nNo way I'm driving down there again. I've been to Mannheim once and\nall the people I met there were rather unfriendly: I've tried multiple\ntimes to ask for a specific street at a petrol station, a taxi driver,\nand so on, and all reacted _extremely_ unfriendly. For instance after I asked\nmy question the taxi driver closed his window and drove off. \nSo my first time in Southern Germany will hopefully be my last time.\nBTW: Is that language they are speaking there really German?? All people\ngoing to LinuxTag - My advice: You definitively need your Hexaglot.\nI could hardly lead any conversation though German is my native language.\nIsn't there something about KDE in Rhein-Ruhr, Brussels, Amsterdam, London\nor some other less hostile place?\n\n"
    author: "Jan"
  - subject: "Re: Karlsruhe?"
    date: 2003-07-09
    body: "FYI: the people in southern germany forked German quite a while ago. So did the people where I come from. Result: I'll need Kurt or Martin as a babelfish to decode that language :-)"
    author: "Ralf Nolden"
---
The <a href="http://www.kde.org/">KDE Project</a> will be present at <a href="http://www.linuxtag.de/">LinuxTag 2003</a>, the largest Linux and Open Source exhibition in Europe, being held in Karlsruhe, Germany from July 10-13 2003. The KDE Project's primary focus this year will be the latest stable 
KDE release, <a href="http://www.kde.org/info/3.1.2.php">KDE 3.1.2</a>, though KDE volunteers will also demonstrate 
other KDE programs like the <a href="http://pim.kde.org/">KDE PIM family</a> including the upcoming integrated personal information suite <a href="http://www.kontact.org/">Kontact</a>, the <a href="http://www.koffice.org/">KOffice suite</a> and the development tool <a href="http://www.kdevelop.org/">KDevelop</a>.
<!--break-->
<p>At the <a href="http://www.bsi.bund.de/">German Ministry for Security in Information Technology</a> booth you can also meet KDE developers presenting the <a href="http://kroupware.kde.org/">Kroupware Project</a>, which developed a Free Software groupware solution including a KDE Kolab client. The Kolab Server 1.0 is planned to be released officially at LinuxTag.</p>

<p>The <a href="http://www.linuxtag.org/2003/en/conferences/conferences.html">free conference program</a> features talks by notable KDE developers Ralf Nolden, Bo Thorsen and Matthias Kalle Dalheimer about <a href="http://www.linuxtag.org/2003/de/conferences/talk.xsp?id=23">KDE</a>, the <a href="http://www.linuxtag.org/2003/en/conferences/talk.xsp?id=49">KDE Kroupware Client</a> and <a href="http://www.linuxtag.org/2003/en/conferences/talk.xsp?id=29">Memory Debugging and Performance Tuning</a> with <a href="http://developer.kde.org/~sewardj/">Valgrind</a> and <a href="http://kcachegrind.sourceforge.net/">KCachegrind</a>.</p>

<p>You find the <a href="http://www.kde.org/">KDE project</a> on booth F29 and the <a href="http://kroupware.kde.org/">Kroupware</a> presentation on booth C20. Have a lot of fun on <a href="http://www.linuxtag.de/">LinuxTag 2003</a>!</p>