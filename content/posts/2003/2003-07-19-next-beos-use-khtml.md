---
title: "Next BeOS to use KHTML"
date:    2003-07-19
authors:
  - "ealmqvist"
slug:    next-beos-use-khtml
comments:
  - subject: "WebCore/JavaScriptCore 85"
    date: 2003-07-19
    body: "When will the state of Safari 1.0 be merged into CVS HEAD? The last merge already seems so far away."
    author: "Anonymous"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-19
    body: "A lot of it already has been merged into HEAD. More by the time 3.2 comes around. To be frank, though, there seems to be an impression (mostly from Mac users) that Apple have done tonnes of work and the KHTML team have been sitting around on their rears. They've been doing a lot, and while Safari have a few speed-ups still left and a few CSS fixes, there are many feature improvements, bugfixes, CSS fixes and other changes in CVS which Apple haven't merged yet; probably far more than go the other way.\n\nBut it is a continuing process."
    author: "Dawnrider"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-19
    body: "> there seems to be an impression (mostly from Mac users) that Apple have done tonnes of work and the KHTML team have been sitting around on their rears.\n\nIs that a wonder when Apple has several developers working full-time on it while khtml was developed by less people and in their spare time? And I can't see nothing wrong when the original khtml authors (I don't think any of them did  only khtml before) now work more on other parts of KDE as before and enjoy the features/fixes of Apple."
    author: "Anonymous"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-19
    body: "Remember that on the KDE side there is not a dozen of full time employees to improve khtml.. Actually this is what makes khtml so amazing... \n\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-19
    body: "It may be worth mentioning that what most Kde users are seeing is khtml before any Apple/Safari input. All safari stuff has been merged post 3.1.\n\nI must say that 3.2 from cvs rocks. Fast, stable, rarely any site it has trouble with, banking works. The only thing I need yet is a way to listen to the hockey games. \n\nhttp://nhl.com/intheslot/listen/radio/index.html\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-20
    body: "Won't mplayer (or kmplayer) work with that?"
    author: "Coolvibe"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-19
    body: "If I ever heard such a sentiment from a Mac user, here's what I'd probably say:\n\nApple got fully functioning HTML and JS engines when they adopted KHTML/KJS. Those large chunks of functioning code got there courtesy of the KHTML developers. During the original development spanning several years Apple did not contribute a thing to what has become one of the two best web browsers on UNIX, and one of the three best on any platform. Mac users should stop and think about that for a moment.\n\nApple is now involved in an Free Software project. If they represent the majority of man hours on the project, then they will probably end up putting in more code than others while doing so. This is the nature of Free Software development. Apple loses nothing in their product and only gains from whatever work others continue to do as well. It's a win-win situation for them. Mac users should stop and think about that for a moment.\n\nThe Free Software community (or at least large parts of it) have welcomed the arrival of Apple and its users. I know this Free Software thing is new to Mac users, and that many Mac users tend to hold Apple in high regard and the rest of the world in less regard, but it's high time they learned to appreciate the gifts from the Free Software community that help make their current platform what it is. It isn't an us-and-them thing, it's an us-and-us thing."
    author: "Aaron J. Seigo"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-20
    body: "I think you forget that allot of mac users come from the linux/open source platform. Well, atleast the mac os x users who know what KDE and KHTML is. Other users are just happy that they have a stable platform and a speedy browser to replace IE. They even help the open source project, by clicking on that bug button, and generating bug reports. (it's just too easy not to)"
    author: "osx user"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-19
    body: "hmm... but as far as I read safari started supporting the articles on www.iht.com way back in spring whereas current CVS-HEAD of kde3.2 still deosn't seem to support it."
    author: "johannes wilm"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-20
    body: "What do you mean by that?\nJust checked the site, and i can read the articles without any trouble (kde 3.1.3)\n\nRinse"
    author: "rinse"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-20
    body: "It works in 3.2 HEAD. There was a problem a while ago, but I think it was solved when some of the css fixes went in. Don't remember for sure.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: WebCore/JavaScriptCore 85"
    date: 2003-07-20
    body: "hmmm... still not here. and I am running cvs from about 14 days ago"
    author: "johannes wilm"
  - subject: "KHTML"
    date: 2003-07-19
    body: "When will KHTML be made independent of KDE and QT so that it is easier to use on other platforms like BeOS, OSX but also Windows and Syllabus?"
    author: "Me"
  - subject: "Re: KHTML"
    date: 2003-07-19
    body: "KHTML/KJS and related technologies are already largely independent of KDE. Things like Webcore make it independent of Qt too. \n\nWhat is Syllabus?"
    author: "anon"
  - subject: "Re: KHTML"
    date: 2003-07-19
    body: "Actually, KJS is fully independent of Qt, but KHTML isn't. "
    author: "Sad Eagle"
  - subject: "Re: KHTML"
    date: 2003-07-19
    body: "so is it possible to compile KHTML itself ? without anything else, making a .so. and update our favorite konqueror browser with the latest khtml available ? \n\n"
    author: "somekool"
  - subject: "Re: KHTML"
    date: 2003-07-19
    body: "Sorry, <A href=\"http://syllable.sourceforge.net/\">Syllable</A>.\nIt's the AtheOS fork, it uses KHTML in it's browser. \n"
    author: "Me"
  - subject: "About time for an AtheOS fork"
    date: 2003-07-20
    body: "the users have been demanding it ... "
    author: "OSX_KDE_Interpollination"
  - subject: "Re: KHTML"
    date: 2003-07-21
    body: "yes, but then configure comes in and tells you 'I aint generating no frickin makefiles until I see some Qt and X libs installed'. It really should be moved out from KDELibs or you should be able to tell configure to generate makefiles even though X and Qt is missing.\n\nIt sucks trying to port things to a platform without Qt and X when you even can't generate the makefiles."
    author: "me"
  - subject: "Re: KHTML"
    date: 2003-07-24
    body: "\"It sucks trying to port things to a platform without Qt and X when you even can't generate the makefiles.\"\n\nIf you fail to solve this problem, you aren't going to be able to port the rest of the libraries.  Creating a new configure.in is just a minor job, after all."
    author: "ac"
  - subject: "Re: KHTML"
    date: 2003-07-19
    body: "Yeah, then Mozilla's Gecko and KHTML will be the only browser code bases availble. Just think, OSS would eventually dominate the browser market (code base mareket anways). There would be a revolution of Browsers based on KHTML across all platforms. Excellent strategy to help break IE's strongehold."
    author: "Chris Spencer"
  - subject: "Re: KHTML"
    date: 2003-07-19
    body: "> When will KHTML be made independent of KDE and QT...\n\nI agree on this question, maybe all dependencies to qt in khtml should \nbe removed, and this in HEAD.\n\n\nThis way Apple and others could work directly on the branch"
    author: "ac"
  - subject: "Re: KHTML"
    date: 2003-07-20
    body: "> This way Apple and others could work directly on the branch\n\nI think you're misunderstanding the situation here.\n\nApple's KHTML is basically an old version of KHTML (from KDE 3.02) that has been updated with their own fixes. They interface it to Safari and OSX through Webcore and KWQ. They don't make any changes in KHTML itself since it is already pretty much independent of KDE. KHTML does require a very tiny subset of Qt, and as Apple has shown, it's much easier to make KHTML completely free of Qt than just write a simple wrapper that implements the Qt classes that KHTML needs. This is what KWQ does. "
    author: "anon"
  - subject: "Re: KHTML"
    date: 2003-07-20
    body: "Apple's Team merged the KHTML of KDE_3_1_BRANCH into their version shortly after the first Safari Beta. And I wouldn't be surprised if they continue to do so for changes in KDE's HEAD too."
    author: "Anonymous"
  - subject: "Re: KHTML"
    date: 2003-07-20
    body: "Is there a document describing which components of QT are needed so a wrapper can easily be created?\n"
    author: "Anonymous"
  - subject: "yea, while we're at it lets port it to XPCOM"
    date: 2003-07-19
    body: "then we could really deform it!"
    author: "Robert Guthrie"
  - subject: "Re: yea, while we're at it lets port it to XPCOM"
    date: 2003-07-19
    body: "I think it's a good idea, in that case al khtml based browsers will render sites the same"
    author: "Norbert Pil"
  - subject: "khtml 3.2 nice but..."
    date: 2003-07-19
    body: "It is faster and more responsive, but it screws up on some websites more than 3.1 does. For example globeandmail.com misrenders horribly and glek.net doesn't render at all :(.\n\nI wish somebody would fix glek.net, since it's the webste that I go to many times a day and I'm stuck using moz in kde."
    author: "Khtml bug #1"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-19
    body: "I don't know if I think they should care about sites like this. According to the w3c validator globeandmail.com got 275 coding errors (http://validator.w3.org/check?uri=http%3A%2F%2Fwww.globeandmail.com), and the glek.net index file doesn't even properly declare what type of document it is (http://validator.w3.org/check?uri=http%3A%2F%2Fwww.glek.net%2F)."
    author: "ealm"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-20
    body: "Hmm, I wouldn't mind some rendering problems with broken sites, but Konqueror could be a bit more tolerant. I've seen several sites lately showing up completly white. Unfortunately I didn't have the time for further test but it seams to be ralated to a missing doctype.\nMy point is, a broken page may look broken, but showing nothing at all isn't an option imho."
    author: "michael"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-20
    body: "One way to archive this is a broken CSS link.\n"
    author: "Tim Jansen"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-20
    body: "IIRC, not rendering anything when a site has its main stylesheet link broken was a bug Dirk fixed a few days ago; and IIRC it was actually related to some of the Apple stuff.\n"
    author: "Sad Eagle"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-20
    body: "A few days ago? I should update my KDE then... That's what I like about KDE: You have to be really fast, or the bug you found is fixed before you can report it... *g*\n\nMichael"
    author: "michael"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-19
    body: "glek.net: it may be unfortunate when the site does render in gecko and not in khtml, but please look at the html source, line 8:\n\n<link type=\"text/css\" rel=\"stylesheet\" href=\"file:///var/www/html/html.css\" />\n\nThat page is simply broken...\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-20
    body: "DUDE you rock!\n\nThank you. It was a silly mistake on my part :)\n\n\n\n\nTaras"
    author: "Taras"
  - subject: "Re: khtml 3.2 nice but..."
    date: 2003-07-20
    body: "glek.net works perfectly here on KDE-3.1.2. I was over at www.osnews.com the other day, and you should seen how many individuals were bad mouthing KHTML claiming the Safari developers will be it's only succour. I was extremely irritated.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "I use a Mac..."
    date: 2003-07-20
    body: "...and appreciate KHTML. I also use Linux and think it rocks to have the same rendering engine on my OS X box as my Linux box -- nice consistent web page display. Yah."
    author: "Dan"
  - subject: "Konqueror/Mozilla do not support many common sites"
    date: 2003-07-21
    body: "Khtml is wonderful and Konqueror rocks, but both it and mozilla have a long way to go before they can be used to replace IE. \n\nI find that more and more sites appear broken on both these browsers these days.\n\nFor example, consider going to http://www.coldwellbanker.com/\n(A real estate agent). \n\nMost of the features of the site are completely broken in both Mozilla and Konqueror. So are many other widely used real-estate sites. I am shopping for a house nowadays and find that I have to boot into windows while doing most web searches.\n\nI guess this must be due to some IE-specific technology that the open-source browsers do not support yet. Any idea what that is?\n\nThanks,\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: Konqueror/Mozilla do not support many common sites"
    date: 2003-07-21
    body: "The code for the Coldwell Banker web page is simply broken. \nIf you write an application in C without proper syntax it won't compile. Just the same case here...\n\nhttp://validator.w3.org/check?uri=http%3A%2F%2Fwww.coldwellbanker.com%2F"
    author: "ealm"
  - subject: "Re: Konqueror/Mozilla do not support many common sites"
    date: 2003-07-21
    body: "The problem is that users don't see this.  If the site renders on browser A but not browser B, the user will use browser A, regardless of the quality of browser B.\n\nIt's not just a matter of building the perfect rendering engine according to spec.  It's a matter of building a rendering engine that works for the most sites possible.  Arguing that the sites are broken and that's why they don't work doesn't get anywhere - the users will just go with the browser that they do work on."
    author: "DanM"
  - subject: "Re: Konqueror/Mozilla do not support many common s"
    date: 2003-07-21
    body: "Or a matter of mailing the webmasters and telling them their page doesn't follow standards, and if they did a bit of effort, it'd follow them and render in most mature browsers. Hint: more browsers = more visitors -> more clients."
    author: "Aracno-man"
  - subject: "Re: Konqueror/Mozilla do not support many common s"
    date: 2003-07-23
    body: "Or a matter of mailing the webmasters and CC-ing some management types, saying that their \"we're too sexy for standards\" attitude has lost them your business. If you have a relationship with a company or organisation based on money (ie. your money keeping them in business), things can quickly get changed using this approach because the technology banter passes the suits by - they just lock onto the bit about the money."
    author: "The Badger"
  - subject: "Re: Konqueror/Mozilla do not support many common s"
    date: 2003-07-31
    body: "\"I guess this must be due to some IE-specific technology that the open-source browsers do not support yet. Any idea what that is?\"\n\nNo, what's wrong is the web developers/designers who haven't gotten around to writing !correct! code so that sites will all work cross-browser.  Web standards, baby!\n\nActually, of all the current generation browsers that are out, IE/Win is in last place as far as DOM1, DOM2, CSS1, CSS2, XML/XSL and PNG support.  Microsoft needs to get with the program.  Actually, so do web designers.  GET WITH THE PROGRAM!  THE WEB ISN'T ALL ABOUT IE!"
    author: "Skyzyx"
---
<a href="http://www.yellowtab.com/">YellowTAB</a> is a German-based company developing an operating system called "Zeta". Based on the BeOS source code, for which they have obtained legal rights to use, it is meant to be the next-generation version of the now 3 years old BeOS r5. On their <a href="http://www.yellowtab.com/">website</a> they have just <a href="http://www.yellowtab.com/article.php?id=45">announced two new technologies</a> for their OS, based on KHTML: <i>"JavaScriptCore and WebCore are two technologies, ported from Apple's work on KHTML (the engine from Konqueror, a "modern Net+" for Linux's KDE), by YellowTab. These components will allow both developers and users to take advantage of the latest technology available, in their everyday Zeta usage."</i>
<!--break-->
