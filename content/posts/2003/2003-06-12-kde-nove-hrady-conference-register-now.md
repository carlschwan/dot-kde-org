---
title: "KDE Nove Hrady Conference: Register now!"
date:    2003-06-12
authors:
  - "dmolkentin"
slug:    kde-nove-hrady-conference-register-now
comments:
  - subject: "Oops"
    date: 2003-06-12
    body: "The requested URL /kastle/location.phtml was not found on this server."
    author: "AC"
  - subject: "Re: Oops"
    date: 2003-06-12
    body: "Try http://www.greentech.cz/ufb/about/location/index.html"
    author: "Anonymous"
  - subject: "Program / What Will Happen?"
    date: 2003-06-12
    body: "I think it is a ilttle bit sad that no one told us what to expect at Nove Hrady. There is no program yet, nobody published the Call-For-Paper submissions and I have not heard from a single CFP submittor what they would like to talk about.\n\nSo guys: what are your topics?\n"
    author: "AC"
  - subject: "Re: Program / What Will Happen?"
    date: 2003-06-12
    body: "The call for papers was a few weeks ago.... no I admit reading is a lost art here, but if you read the DOT article from then you would find out they will be announced on the 16th.  It was plainly listed, in a few mailing lists too. Now if you really got adventurous and went to the link above you would even get to see the current program of events.\n\nPlease if you dont RTFA, at least dont troll.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Program / What Will Happen?"
    date: 2003-06-12
    body: "Sorry, but I was curious about the SUBMISSIONS for the CFP. I know that they will be announced on the 16th, but why not make all submissions public before?\n"
    author: "AC"
  - subject: "Re: Program / What Will Happen?"
    date: 2003-06-12
    body: "Maby because they are being reviewed?  AFIAK I have never seen anyone announce entries before they where finalised.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Program / What Will Happen?"
    date: 2003-06-12
    body: "Sure, but I have never seen any conference accepting registrations before a single speaker has been announced or any other hints.\n"
    author: "AC"
  - subject: "Re: Program / What Will Happen?"
    date: 2003-06-12
    body: "hehe... 4 (read four) days..."
    author: "Thomas"
  - subject: "3.2 alpha after Nove Hrady ?"
    date: 2003-06-12
    body: "I hate to ask this again, there is a huge thread in the kde lists and I don't have the time to go through all of it. If you know, please share :-) Is the 3.2 release cicle starting right after the conference ? Thanks !"
    author: "MandrakeUser"
  - subject: "Re: 3.2 alpha after Nove Hrady ?"
    date: 2003-06-12
    body: "Sure, at some point after that, but don't ask when :)\n\nSeriously, I think it will mostly be decided rather spontanious depending on the outcome of the conference.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: 3.2 alpha after Nove Hrady ?"
    date: 2003-06-12
    body: "Thank you Daniel !\n\n\"I think it will mostly be decided rather spontanious depending on the outcome of the conference\"\n\nThat was exactly my feeling, too bad the final release will be late for the next Mandrake, RedHat and Suse ;-) But it is a wise decision !\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: 3.2 alpha after Nove Hrady ?"
    date: 2003-06-12
    body: "http://lists.kde.org/?l=kde-core-devel&m=105289213704104&w=2"
    author: "Anonymous"
  - subject: "Re: 3.2 alpha after Nove Hrady ?"
    date: 2003-06-13
    body: "Thanks  !. Confirmed !"
    author: "MandrakeUser"
  - subject: "Re: 3.2 alpha after Nove Hrady ?"
    date: 2003-06-14
    body: "Today's updated http://developer.kde.org/development-versions/kde-3.2-release-plan.html talks about an Alpha release after Nove Hrady and a possible earliest Release date 8th December if only one Beta release is needed."
    author: "Anonymous"
  - subject: "announce it on www.kde.org"
    date: 2003-06-13
    body: "Any reason not to put a big announcement/logo/banner of Nove Hrady on www.kde.org?"
    author: "ac"
  - subject: "Re: announce it on www.kde.org"
    date: 2003-06-13
    body: "Linuxtag is earlier and the Nove Hrady program is not published yet."
    author: "Anonymous"
---
For those of you who have been anxious: <a href="http://www.greentech.cz/kastle/preform.php">You can now register</a> for the Nove Hrady conference ("Kastle"). The <a href="http://events.kde.org/info/kastle/">Kastle info pages</a> have been updated to contain more details about arrival, departure and accommodation. Remember that places are limited, so better hurry!  Active contributors do of course have precedence. 
Companies that would like to send employees to the event to learn about KDE development firsthand <a href="http://events.kde.org/info/kastle/registration.phtml">can find information here</a>. Don't let this opportunity pass you by!

<!--break-->
