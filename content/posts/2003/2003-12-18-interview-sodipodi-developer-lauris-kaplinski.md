---
title: "Interview with Sodipodi Developer Lauris Kaplinski"
date:    2003-12-18
authors:
  - "pfremy"
slug:    interview-sodipodi-developer-lauris-kaplinski
comments:
  - subject: "Sodipodi icons in KDE"
    date: 2003-12-18
    body: "The icons for KSplashML kcontrol module and the new KDM icons in the userinfo module from kdeutils (both will be part of KDE 3.2) were drawn in Sodipodi by Hans Karlsson.\n"
    author: "Ravi"
  - subject: "Re: Sodipodi icons in KDE"
    date: 2003-12-18
    body: "Also, I designed the KStars icon using sodipodi."
    author: "LMCBoy"
  - subject: "Sodipodi..."
    date: 2003-12-18
    body: "I love Sodipodi and the Gimp, there aren't any real alternatives on the K desktop. So I love this initiative!\n\nQuestion: will Sodipodi be added to the KOffice if it has all Qt/KDE widgets (I know this won't happen ovenight).\n> Sodipodi dropped all mandatory gnome dependencies during migration to Gtk2\n\n-AC\n\nP.S.: IMO the Kimp initiative was a good one, although Gimp does rely more heavy on GNOME and gtk libs iirc."
    author: "yes as coward ;-)"
  - subject: "Re: Sodipodi..."
    date: 2003-12-21
    body: "The Gimp doesn't use a single library from GNOME, it only uses GTK+. It doesn't even use libglade!.\n\n"
    author: "Diego"
  - subject: "Re: Sodipodi..."
    date: 2006-08-08
    body: "Yeah, GTK stands for GIMP Tool Kit, not GNOME Tool Kit :-P"
    author: "Anonymous Coward"
  - subject: "sodipodi"
    date: 2003-12-18
    body: "I like this program very much, but I believe it would be better to go the Kdevelop patch of multiple UI styles. Some are used the UNIX //Gimp style. Other the way such programs are structured on the win desktop. Others prefer innovative approaches (tabbed mode). MDI, SDI ecc. Think it would be best to provide all of these. "
    author: "gerd"
  - subject: "print preview??"
    date: 2003-12-18
    body: "this does not look very good. ghostview?? Hope this will be imrpoved in KDE 3.x"
    author: "walter berens"
  - subject: "Win32 api"
    date: 2003-12-18
    body: ">Applications written for Windows95 still look like relatively good on\n>WindowsXP. Programs written for Gtk1 feel clearly foreign in the latest Gnome\n>(don't know about KDE/Qt). \n\nHence the horrible mess that is the Win32 API. When companies are releasing wrappers for MFC, MS's API wrapper, you know something is badly wrong.\n\nAFAICS, developers in the Linux-related world value elegance and simplicity over backwards compatibility. After all, nobody is forcing anyone to upgrade.\n\nOh yeah - and Sodipodi is a fantastic app!"
    author: "Ben Roe"
  - subject: "Re: Win32 api"
    date: 2003-12-19
    body: "Exactly. But on the other side this may become major headache for 3rd party application developers. You may save a little because of coding time and testing because of better environment, but will lose a lot because extra testing for multiple versions and platforms, tech support and so on. And no - even if your programs are free, you cannot expect volunteers to bear all your QA costs :-/"
    author: "Lauris"
  - subject: "A true SVG editor"
    date: 2003-12-18
    body: "SodiPodi is a bit of an odity. I really like it because it is a true SVG editor, and think of it as design program for programmer types. Illustrator is undoubtedley superior from a design perspective, but there is absolutely no way you would want to script the SVG it exports.\n\nWhen the web was young all the agencies used DreamWeaver to do the HTML, but it didn't take long for \"hand HTML coders only\" to start appearing in the job adverts, it being so much easier to script. Having all these W3C formats means that you can *in theory* achieve all these noble, worthwhile goals, but it requires a detail to attention that only programs like SodiPodi can deliver on.\n\nSVG still hasn't come of age, and I don't believe it will take off on the web until you can automaticly convert scripted SVG to SWF as a transitional measure. When it does, I think SodiPodi will be there to capitalize on that, and I wish it every success.\n\nOne last thing, I really believe we need a Semantic Wordprocessor along the same lines to allow non-developer types to easily contribute content for their companies web-sites without trouncing the company style. AFAIK, no existing word processor comes close to being able to offer rich CSS friendly semantic HTML. But, I would love to be proved wrong here!"
    author: "Dominic Chambers"
  - subject: "Re: A true SVG editor"
    date: 2003-12-18
    body: "> One last thing, I really believe we need a Semantic Wordprocessor\n> along the same lines to allow non-developer types to easily contribute\n> content for their companies web-sites without trouncing the company\n> style. AFAIK, no existing word processor comes close to being able\n> to offer rich CSS friendly semantic HTML. But, I would love to be\n> proved wrong here!\n\nAmaya? (http://www.w3.org/amaya) And yes, if some kind of kamaya was\navailable, I would be very happy. UI for this program is and always\nwas particularly horrible (a wild mess of EMACS and CUA for keyboards).\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: A true SVG editor"
    date: 2003-12-18
    body: "Yes your quite right -- definitely the closest fit. I have been keeping an eye on Amaya for some time, although I haven't checked the most recent stable release. Amaya is a product that I could use quite hapilly, but is probably not something I could reccomend for non-technical people. Also, like most WYSIWYG web editors, it tries to be a complete web site editor, rather than just dealing with the content that is piped into the final page.\n\nSo far AbiWord is the best tool I have seen for this, and will hopefull become a true proficient one day.\n"
    author: "Dominic Chambers"
  - subject: "Re: A true SVG editor"
    date: 2003-12-19
    body: "Amaya is certainly capable and has some interesting things to it. Unfortunately for me just trying to look through and see what those things are drives me crazy because I absolutely detest the interface and a number of design decisions. I haven't looked at it for a couple releases now, but if you want compliant WYSIWYG then it does deliver.\n\nIf you want to do this with a KDE application maybe you missed the news that the Quanta Plus team is working with KHTML coders to subclass KHTML and add the editing capabilities in the old Kafka design. So KDE 3.2 will ship with Quanta Plus which will have the ability to create DTD compliant markup. Or at least it will be relatively standards compliant because of schedules. Within a few months of release we will do an external release (new features in English only)  to enhance the functionality of it.\n\nVPL (Visual Page Layout) in Quanta will allow you to visually create HTML and XHTML pages. It will give you split view with text and text modes. It will offer a DOM tree which extends to show nodes for scripting too. It will show visuals and edit markup on PHP files showing tags for PHP. Along with doing site management it will allow you to do simple page edits without making you use a pile of wizards. It's in final development and due to ship in about a month.\n\nOur objective is to make VPL as word processor like in use as possible while making it useful to professional developers and fully compliant to the selected DTD of the document. Our initial release may not quite fulfill allthose objectives but we will continue and release as we reach them as they are not that far off."
    author: "Eric Laffoon"
  - subject: "Re: A true SVG editor"
    date: 2003-12-19
    body: "> Amaya is certainly capable and has some interesting things to it.\n> Unfortunately for me just trying to look through and see what those things\n> are drives me crazy because I absolutely detest the interface and a number\n> of design decisions.\n\nQuite! No, it's not much to look at, but is currently in a league of it's own when it comes to WSYIWYG over semantic html -- without trouncing the code.\n\nBy the way, I think what your attempting to do with VPL looks absolutely amazing. The DTD idea would be very cute too, but possibly not as useful as it sounds. When you have no prior knowledge of an XML vocabulary then the best you can do out of the box is either provide one of those horrible graphical XML tree editors, or (far more useful) provide inteligent syntax highlighting, and code insight based on what the DTD says is possible. But again, that's a programmer type thing.\n\nI'm personally interested in the idea of being able to allow everyone capable of using a wordprocessor to contribute to their company's or organisation's web site without having to have a webmaster involved at every turn. As well as being useful to large companies, it would also be a great enabler for small businesses and private individuals to be able to maintain a professionally branded website without having to make a continued investment in 'web expertise'. You see where I am going with this ... trying to do myself out of another job!\n\nIf Quanta ever became capable of this I would be truly amazed, since I have been thinking of something far more modest, and after over two years of waiting it still hasn't materialized. The beauty of Quanta in this scenario is that the entire web site could be modified safely. If you and the Quanta team can pull this off Eric than you will be, truly, flavour of the year (me, and a multitude of other web devlopers with clients that want to drive).\n\nIn case this is even remotely compatilble to where you have Quanta headed, here are the some things that would clench it:\n\n  1. allow the developer to lock down html documents based on the DTD\n     they reference. i.e. no source access (html or css), no or very limited\n     formatting options, and no allowing the html to become invalid as per\n     the schema.\n  2. allow the developer to specify extra semantic styles (e.g. latin, as in\n     <span class=\"latin\">lorum ipsem</span>) that can be applied to the text\n     by the user. this could be done in the DTD, but I think it might require\n     some serious entity-ref voodoo.\n  3. allow DTDs (including text style info) and stylesheets to be referenced\n     remotely so that an update in one place can affect multiple users.\n  4. allow the developer to bind kommander dialogs to given xml tags so that\n     the user can easily enter any meta information, or information that is\n     non-documental.\n\nWow! That would be a killer app. All my clients run Windows but I could easily convince them to dual-boot to KDE just for that one thing.\n\nRight, when will it be ready ;-)."
    author: "Dominic Chambers"
  - subject: "Re: A true SVG editor"
    date: 2003-12-19
    body: "another true WYSIWYG is Sketsa (http://www.kiyut.com/products/sketsa/index.html) it is written in Java, so it will be run on Linux\n\nanother is Jasc WebDraw (http://www.jasc.com) run only on Windows though"
    author: "me"
  - subject: "Sodipodi is great."
    date: 2003-12-18
    body: "Sodipodi is a *great* application! I appreciate it very much!\n\nSo by all means - feel free to keep up the good work ;-)\n\n\ncheers!\nFrans"
    author: "Frans Englich"
  - subject: "Sodipodi is real great stuff but..."
    date: 2003-12-18
    body: "...I wish it could be \"converted\" to a real QT Application. QT is a homogen Multiplatform Toolkit and not a Patchwork like GTK (yes, I don't like GTK).\nQT is also much better for Porting to Windows. "
    author: "noone"
  - subject: "Re: Sodipodi is real great stuff but..."
    date: 2003-12-18
    body: "It's \"Qt\" actually, \"QT\" usually refers to QuickTime."
    author: "Waldo Bastian"
  - subject: "Re: Sodipodi is real great stuff but..."
    date: 2003-12-19
    body: "Nonsense. The fact that Gtk+ consists of several libraries does not imply that it is not as coherent as Qt (which itself will be broken into components with version 4.x).\n"
    author: "Anonymous"
  - subject: "Re: Sodipodi is real great stuff but..."
    date: 2003-12-19
    body: "> QT is also much better for Porting to Windows.\n\nYes and no. For a company, it is much better. For an individual, since Qt for windows requires either to run XFree on windows or to pay the Trolltech licence (1500$), it is not that much better.\n\nHaving using it as a company, I am more than satisfied. The 30k lines program I ported took me 1 hour of fixes for broken visual C++ code, and 2 days of DLL problem. 0 minutes where dedicating to the port, it just worked.\n\n"
    author: "Totoro"
  - subject: "inkscape"
    date: 2003-12-19
    body: "if you guys like sodipodi check this out: www.inkscape.org. this is great! it has better dialogs, boolean operation, better menus and much more!"
    author: "cez2002"
  - subject: "Re: inkscape"
    date: 2003-12-19
    body: "For those unfamiliar with it, Inkscape is recent sodipodi fork. It is still based 99% on sodipodi code, so do not expect miracles yet."
    author: "Lauris"
  - subject: "Re: inkscape"
    date: 2003-12-19
    body: "at least they spend time on actual usability of sodipodi as an illustration app, they are going in a really nice direction.\n\none thing that allways bugged me about sodipodi where the bloated option windows. and not being able to implement a usefull color-palette. \n\nthe \"apply this color to:\" dropdown is also merely ridiculous. sure its better than no interface. afaik all the ui's apps like photoshop/illustrator use arent really copyrighted so one should better learn from their ui-design and try to rebuild it. \n\nthe main toolbar itself is merely a waste of space, unstructured and showing loads of unnecessary buttons (print button ????? file open button ???? - i use shortcuts  for such operations most of the time), instead of having a \n\n\n------     <--- display outline color\n| ###|##   <--- display current fill\n| ###|##\n|_###|##\n  ######\n\ndisplay current line style/thickness beneath them\n\nthis is standard user interface for each and every vector app, and since 3 years such a widget had no priority in sodipodi development. why?\n\ndoes open source mean that if you use it you punish yourself with cumbersome usability design?"
    author: "foo"
  - subject: "Re: inkscape"
    date: 2003-12-20
    body: "Actually I find the design of the sodipodi interface to be quite modern and nice. I LIKE having all the nice node editing and grouping tools on the main toolbar where i can get right at them. And you can pack them away as well.\n\nPerhaps you have a small monitor? The main toolbar is hardly a \"waste of space\" if you have 1024x768 or higher resolution. \n\nThanks for the interview Lauris. See you in IRC. \n\ndan ostrowski"
    author: "dan"
---
The <a href="http://www.sodipodi.com/index.php3?section=download">latest version</a> of the famous Vector Drawing program <a
href="http://www.sodipodi.com">Sodipodi</a> features <A href="http://www.sodipodi.com/screenshots/new/sodipodi-0.32-kde.jpg">KDE integration</a>. It can
be configured to use the <a href="http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/stuff/sodipodi-screenshots/sodipodi_savefiledialog2.png">
file dialog</a> and the <a href="http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/stuff/
sodipodi-screenshots/sodipodi_print_preview.png">print preview dialog</a> of KDE. Wondering why a once Gnome Office
application would now seek integration into KDE, I went for a little
interview with Lauris Kaplinski. 








<!--break-->
<p>
<b>Philippe Fremy:</b> <i>Can you present yourself? What are you doing when you are not working on free software?</i>
<p>
<b>Lauris Kaplinski:</b> I am 32 years old, living in Tartu, Estonia with my wife and 3 children.
I am working at a local biotech startup, writing software tools for
genotyping and related analysis, mostly Windows programs.
<p>
Other than coding I spend my time in the garden (we are living in my
grand-grandfather's mansion with large old park), learn Japanese (fell
for Japanese pop-culture some years ago, still haven't got over it),
read, draw, try to fix our house (it is old and likes to eat whatever
money we happen to have). Ah and, of course, 3 children mean that
you don't have to bother, how to spend free time (don't have any
'free' time to start with ;-)
<p>

<b>Philippe Fremy:</b> <i>What does Sodipodi do? How long have you been developing Sodipodi?  Why did you start it?</i>
<p>

<b>Lauris Kaplinski:</b> Sodipodi is quite usable as generic vector drawing application and
more specifically, as SVG creation tool. It is nothing near in quality
or feature set to big commercial programs, but people have used it to
design icon themes, posters, business cards and much more. Most expected
features are there - basic shapes, bezier paths, gradients,
bitmaps, transformations, transparency, grouping and so on. One
interesting feature is direct access to the SVG document tree, so users
can hand-tune elements if the UI does not support certain feature.
<p>

The first ChangeLog entry in Sodipodi was from September 1999, but the
project started about a year earlier as an experimental fork of Gill's codebase.
Gill was <a href="http://www.levien.com/">Raph Levien</a>'s project
trying to build a SVG editor on top of
the DOM library. Back then the hopes about DOM were high in the Gnome core
team, but there never emerged a usable implementation, so it was abandoned
eventually. Sodipodi dropped full-featured DOM support, but kept the basic
program logic, that is, a semi-independent XML layer at the bottom of the
document tree.
<p>

<b>Philippe Fremy:</b> <i>How does Sodipodi integrate with the Gnome Office framework? What Gnome technologies are you using?</i>
<p>

<b>Lauris Kaplinski:</b> As far as I know there is currently no serious Gnome Office framework,
despite it having been discussed many times. Major Gnome Office components
(Gnumeric and Abiword) are mature projects with dedicated (but separate)
development teams, and thus not easy to bring under single umbrella
(Abiword is written in C++, gnumeric in C, Abiword is cross-platform,
gnumeric gnome-only). Also the politics around OpenOffice affected the Gnome Office team 1-2 years ago quite seriously, slowing down overall development.
<p>

Sodipodi dropped all mandatory gnome dependencies during migration to Gtk2,
mainly to make a Windows port possible. There is still a compile time option
to support gnome-print. So currently it is aiming to be a cross-platform,
reasonably self-contained application, that integrates with as many
environments as possible (and reasonable).
<p>

<b>Philippe Fremy:</b> <i>What Desktop Environment are you running? What do you think of the latest versions of KDE and Gnome?</i>
<p>

<b>Lauris Kaplinski:</b> Currently I am running KDE 3.1. Before that it was Rox Desktop, before
that KDE and Gnome2. I have relatively modest needs in a desktop environment -
basically just a decent panel and well-behaving, unobtrusive window manager.
Application-wise I am running mostly Konqueror, Evolution, XChat and
Emacs.
<p>

I think KDE 3.1 is usable (that is why I use it, after all). There are
of course areas where lack of polish scratches an eye, and there are
still things I cannot configure, but overall it does make working
reasonably comfortable :-)
<p>

Gnome2 is much harder to judge. It is like MacOSX - one either loves it or
hates it, but few are indifferent. For me, like for several other people,
Gnome1 was a hacker's desktop (and we loved it), but Gnome2 is a corporate
desktop (and we do not feel comfortable with it).
<p>

<b>Philippe Fremy:</b> <i>Who are your users? Do you have any enterprise users? Are your users mainly KDE users or Gnome users?</i>
<p>

<b>Lauris Kaplinski:</b> Difficult to estimate. I do not know about any enterprise deployment (neither
do I think Sodipodi is ready for that). Most feedback has come from hobby
artists, coders who need some vector images, teachers using it for
students...
<p>

The biggest percentage already is, or soon will be, Windows users judging
from downloads... Others are really difficult to estimate, but I think
Gnome may be somewhere near 2/3 and KDE 1/3.
<p>

<b>Philippe Fremy:</b> <i>What was the goal of this KDE integration? How does it work? Did you look at other libraries to achieve that?
Was it difficult to do some C++ development? How hard was it to write?  Is your work reusable?</i>
<p>

<b>Lauris Kaplinski:</b> I am doing C++ development at work and starting to love it, so maybe there will be more C++ parts in Sodipodi in future...
<p>

The primary goal was to make Sodipodi to feel as natural as technically
reasonable for KDE users. Freedesktop standards and shared themes definitely
remove many lower-level differences between look and feel of different
application platforms. What still remains (and scratches an eye) are some
higher-level things, like printing system - if everything else on the desktop
prints through KDE-print Sodipodi shouldn't be the exception - and file dialogs (Gtk+ file dialogs look very foreign under KDE).
<p>

If Sodipodi is compiled with KDE support, it creates a KApplication (in
addition to initializing Gtk+) and KDEBridge object. The latter implements
timeout and guiThreadAwake slots that call the Gtk+ event loop while
Sodipodi is spending time inside the Qt/KDE event loop.
<p>

If Sodipodi wants to display a KDE dialog (all these are modal currently), it
simply calls the relevant static method, like KFileDialog::getOpenFileName,
and lets KDEBridge to manage keeping the Gtk+ windows updated.
For actual printing it is as easy as writing some glue code to output
Sodipodi-generated RGBA stripes to KPrinter.
<p>

Writing the KDE bridge was relatively easy (I really admire Qt/KDE reference
documentation ;-) Of course, it did take some time to simply get the big
picture (MOC, anyone ;-), but overall it was not harder than starting with any
other unknown library.
<p>

The code is quite usable, as long as printing and file dialogs are
everything you need. But it remains a hack, that should be written
correctly one day... I'd like to use the real main loop integration, but
the only reference I found was QGtkWidget by Bernhard Rosenkraenzer, and this
unfortunately was not ported to Gtk+ 2.0.
<p>

<b>Philippe Fremy:</b> <i>Isn't it contradictory to develop a Gtk app and integrate it into KDE?</i>
<p>

<b>Lauris Kaplinski:</b> I was 'enlightened' by an email from a devoted KDE user about year and a
half ago. That guy kept installing Gnome development libraries each time
a Sodipodi release came out, only to compile Sodipodi. And at that time
Sodipodi depended quite heavily on Gnome libs, so finding correct libraries
was not always an easy task.
<p>

So I decided to make the program for an as wide as possible audience. Unless
there is clear winner in the desktop wars, a similar approach may be the way
to go for the foreseeable future. Of course, many things will probably be
integrated into a common (freedesktop) layer eventually - but if you want to
deploy an application now, and you want to maximize potential usage, instead
of using it as a weapon in the desktop wars, you better try to integrate with
whatever the user prefers.
<p>

To be honest, I got the idea from Qt/Win32, which AFAIK can use Windows native
file and print dialogs, if requested.
There is also definitely a certain 'because you can' attitude involved - such
tricks make programming enjoyable. But I hope to extend KDE layer some day to
make it loadable module (so we can distribute single Sodipodi binary) and
add KIOSlaves...
<p>

<b>Philippe Fremy:</b> <i>If you have a wishlist to make such integration in the future easier, what would it be?  What do you think of current KDE/Gnome cooperation?</i>
<p>

<b>Lauris Kaplinski:</b> The usability of such multi-desktop integration 'hacks' depends heavily on the general cooperation between the different
desktops. Currently there is no choice -
if I want to give user access to all KDE printers from Sodipodi, I have to
link with KDEPrint myself. But if there emerges some standard for printing
on the desktop, this may be the way to go.
<p>

My biggest wish is easy main loop integration between Qt and Gtk+. It is
probably relatively easy (both are modern cross-platform toolkits), but
it needs someone with good knowledge about the internals of both...
<p>

KDE Gnome cooperation is definitely nice, but I do not think it is top
priority in making Linux viable desktop platform. In my opinion
interoperability between different versions of the same toolkit/desktop may
become a more serious problem.
Applications written for Windows95 still look like relatively good on WindowsXP.
Programs written for Gtk1 feel clearly foreign in the latest Gnome 
(don't know about KDE/Qt).
<p>

<b>Philippe Fremy:</b> <i>Anything else you would like to add?</i>
<p>

<b>Lauris Kaplinski:</b> Everyone is speaking about Linux on desktop, but sometimes I think, whether this really is, what we want. Are the online
communities ready for dealing with massive financial interests in their field
of interest? Are we planning to move on to next partisan platform to enjoy pure coding freedom?
Anyways, let's keep the train going for now ;-)
<p>

[Note from the Editor: You can help the train with a <a
href="http://www.sodipodi.com/index.php3?section=home/donations">donation</a>!]

