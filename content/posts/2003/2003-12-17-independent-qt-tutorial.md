---
title: "The Independent Qt Tutorial"
date:    2003-12-17
authors:
  - "jthelin"
slug:    independent-qt-tutorial
comments:
  - subject: "How to design object oriented application"
    date: 2003-12-17
    body: "The first part is quite nut and bad written. \nI'd suggest removing it as it is of no interest for building QT application.\nYou try to suggest what's the best way to design objects but this part can be so large it would deserve an entire book : better drop it."
    author: "Anonymous coward"
  - subject: "Re: How to design object oriented application"
    date: 2003-12-17
    body: "I'm aware of that problem, however, I want to introduce Qt's object model. I'll probably look into that part of the text though. Thanks for pointing it out."
    author: "Johan Thelin"
  - subject: "Re: How to design object oriented application"
    date: 2003-12-17
    body: "I'm aware of that problem and I will look into it. The problem is that I want to show the Qt object model, and I (apparently) drifted a bit off-topic. I will revise the text.\n\nAs for further suggestions, please mail me a copy too, since I didn't activate e-mail notification."
    author: "Johan Thelin"
  - subject: "Re: How to design object oriented application"
    date: 2003-12-17
    body: "Definitly a nice start! congrats.\n"
    author: "Mark Hannessen"
  - subject: "Examples"
    date: 2003-12-17
    body: "I only just started reading it and what I miss is first up some practical but very simple (did I mention SIMPLE!) walkthru of using Designer for real and THEN start with the extensive theory in the fisrt few chapters... with refrences to the VSRP (very simple real project) example. It seems to me you have written the first few chapters up to make sure you understand the theory and not so much from a complete novices point of view. Theory on top of theory without referring to real-world examples is often too much work to wade thru."
    author: "markc"
  - subject: "Re: Examples"
    date: 2003-12-17
    body: "Great idea. As I allready mentioned, I plan to redo the first chapters (concerning the object model). Perhaps I should put a Designer version of the Troll's LCD+Slider example there? That includes signals and slots while not getting to complex with Designer. My only problem then is that the Designer introduction that comes later will look to detailed, or? Suggestions?"
    author: "Johan Thelin"
  - subject: "Anonymous Coward"
    date: 2003-12-17
    body: "Hi Anonymous Coward here,\n\nI really like this, keep it up!"
    author: "ac"
  - subject: "Re: Anonymous Coward"
    date: 2003-12-17
    body: "Oh. ac = anonymous coward.\nI finally foun out why there were so many ac(s) on this list!"
    author: "The Real AC"
  - subject: "Error regarding POD structure"
    date: 2003-12-17
    body: "In Chapter 7, the Example 7-1 and the paragraph above, describing it, are erroneous. \n\nIn the paragraph it reads \"The structure that we will introduce is a so called POD structure. This means plain old data, and means that the structure has no methods.\" This is not entirely correct. At various places in the Standard there are requirements for POD types, but I think a POD structure can have non-virtual member funtions. The relevant part of the Standard is Section 9 [class], paragraph 4 I think. A description of POD types can be found at http://www.parashift.com/c++-faq-lite/intrinsic-types.html#faq-26.7\n\nAs the example class \"Contact\" consists of the non POD type \"QString\", it is not a POD type itself."
    author: "Volker Lukas"
  - subject: "Re: Error regarding POD structure"
    date: 2003-12-17
    body: "Thanks for pointing this out. I will fix the problem in the next revision."
    author: "Johan Thelin"
  - subject: "why not kde?"
    date: 2003-12-17
    body: "how about a A Hand Made KDE Application tutorial? they look so much better then Qt ones.\n this is dot.KDE.org damn it!\n:)\n"
    author: "Pat"
  - subject: "Re: why not kde?"
    date: 2003-12-18
    body: "Take a look http://perso.wanadoo.es/antlarr/tutorial/"
    author: "Pedro Echanove"
  - subject: "Update!"
    date: 2003-12-18
    body: "As you might have noticed I did an update yesterday evening (CET). Chapter one, three and seven got a facelift. Thanks for providing great feedback!"
    author: "Johan Thelin"
  - subject: "What about Ralf Nolden's KDE Development book?"
    date: 2003-12-18
    body: "Last summer Ralf Nolden announced that he was writing a FDL'd book covering C++, Qt and KDE development (http://lists.kde.org/?l=kde-core-devel&m=105850839120182&w=2 ).\n \nDoes anybody know how this effort is going?\n \nThanks,\n Quique"
    author: "Quique"
  - subject: "Re: What about Ralf Nolden's KDE Development book?"
    date: 2003-12-29
    body: "There is no actual work being done which is disappointing."
    author: "Anonymous"
---
I have started writing a gentle introduction to Qt called <a href="http://www.digitalfanatics.org/projects/qt_tutorial/">The Independent Qt Tutorial</a>. It currently consists of nine chapters and deals with how to setup Qt, how to use <a href="http://www.trolltech.com/products/qt/designer/">Qt Designer</a>, how to write an application by hand, etc. The purpose of the tutorial is to cover all areas of Qt needed to write top quality applications. Feel free to check it out.


<!--break-->
