---
title: "IBM, Adobe and Windows love Qt but Mozilla Bails"
date:    2003-02-28
authors:
  - "numanee"
slug:    ibm-adobe-and-windows-love-qt-mozilla-bails
comments:
  - subject: "I don't get it"
    date: 2003-02-28
    body: "April 1st isn't for another month by my calendar. Has someone been sipping too much whiskey, or is my humor meter just set too high?"
    author: "Rodion"
  - subject: "humm... so they could port it to linux"
    date: 2003-02-28
    body: "if adobe wished it.... but I don't belive they will do."
    author: "Iuri Fiedoruk"
  - subject: "Re: humm... so they could port it to linux"
    date: 2003-02-28
    body: "It's only natural for them to do it.  Maybe they already did internally.  This will be interesting if they decide to use Qt for their other products.  Look at Acrobat Reader it works on both Linux and Windows, so that could help them and better integration with KDE."
    author: "anon"
  - subject: "Re: humm... so they could port it to linux"
    date: 2003-02-28
    body: "Yes, let's hope they like QT a lot and decide to port all their programs to it, then it will be mostly a matter of recompiling the code for linux :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: humm... so they could port it to linux"
    date: 2003-02-28
    body: "AFAICT, Acrobat Reader for Linux is written in Motif (or something similar).  It's not a Qt app... Also, don't forget that although KDE is based on Qt, the fact that an app is written in Qt doesn't necessarily mean it will integrate with KDE well."
    author: "OUSpirit"
  - subject: "Re: humm... so they could port it to linux"
    date: 2003-03-01
    body: "\"the fact that an app is written in Qt doesn't necessarily mean it will integrate with KDE well.\"\n\nThe first question you need to ask is what needs to be integrated? For most end-user applications, very little is needed. The only thing I can think of that I would like to see in terms of acroread and integration is drag-and-drop. But that's standardized now, so there's no need to make it a KDE application to get it.\n\nThe \"look-and-feel\" is a different beast entirely, so I won't discuss it here."
    author: "David Johnson"
  - subject: "Re: humm... so they could port it to linux"
    date: 2003-03-01
    body: "Look and feel can be integrated too.  The KDE styles allow that and KControl is smart about qtrc."
    author: "Anon"
  - subject: "Re: humm... so they could port it to linux"
    date: 2003-03-01
    body: "Sone more:\n* The filedialog. It's a very common task to open/save files. But Qt / KDE filedialogs are very different (look&feel, shortcuts, bookmarks, preview...).\n* Many common icons are differnt (open, save, copy, paste,...). \n* Qt's printer-dialog is very simple compared to KDE's one.\n* more dialogs...\n\nThose are visible differences, every user sees. And with this differences Qt-applications do not seem to be \"integrated\" to your system.\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: humm... so they could port it to linux"
    date: 2003-03-01
    body: "Adobe uses a lot of energy to do Windows and Mac versions of there Software. I think the primary goal is to reduce the effort. A secondary object is to have Linux/Unix-versions available, when Adobe thinks it's time to sell them."
    author: "Manfred Tremmel"
  - subject: "WTF?"
    date: 2003-02-28
    body: "Um... what is this story about?  Can we re-summarize in a way that requires me to traverse maybe one link?  And written in a comprehensible manner?  As it is it looks like someone's very poor attempt at a joke."
    author: "Peter Kasting"
  - subject: "Re: WTF?"
    date: 2003-02-28
    body: "It's Qt news.  Qt is the toolkit used by KDE."
    author: "anon"
  - subject: "What is this news item about?"
    date: 2003-02-28
    body: "The language is colorful indeed, but I'm very confused.\nIt looks like there might be something of interest in\nthere, but I'm not sure I want to invest the effort to\nclick through _19_ links to try to figure it out."
    author: "confused"
  - subject: "Re: What is this news item about?"
    date: 2003-02-28
    body: "I just broke it up into paragraphs for each story point.  \n\nFigure it out. \n\nPlease stop complaining doggonit.  :-)"
    author: "Navindra Umanee"
  - subject: "Re: What is this news item about?"
    date: 2003-02-28
    body: "I like it. This ain't Cnet.com.com.com. And that is a good thing."
    author: "Roberto Alsina"
  - subject: "Re: What is this news item about?"
    date: 2003-02-28
    body: "Stupidity?\nOk, folks calm down.\nI think the problem here is that this news article\nis also read by an international/non-english-speaking audience \nwhich perhaps doesn't completely understand the subtle humour /\nirony and is therefore somewhat confused.\nWell, I liked it and I think it's important that we all don't\ntake this too seriously. We are not M$ after all so we can\nbe a bit more relaxed... ;-)\nIf this article isn't for you - just\nignore it. \nEspecially the part about the relationship and \"divorce\"\nof the two dinosaurs was quite a good idea and made my LOL.\nKeep up the good work."
    author: "Damon"
  - subject: "Re: What is this news item about?"
    date: 2003-02-28
    body: "I agree.  Well put!"
    author: "anon"
  - subject: "Re: What is this news item about?"
    date: 2003-03-01
    body: "And why is this response attached to my post?"
    author: "Roberto Alsina"
  - subject: "Re: What is this news item about?"
    date: 2003-03-01
    body: "Oh - sorry for that. It shouldn't be. But I hope everyone can \nmake the connection..."
    author: "Damon"
  - subject: "Re: What is this news item about?"
    date: 2003-02-28
    body: "Ok, maybe this should have been \"from the QT spreading\nlike wildfire departmetn\" ...the paragraphs do help...\nsorry for the grumble :-)\n\nQt being used by Adobe is pretty big news..."
    author: "confused"
  - subject: "Re: What is this news item about?"
    date: 2003-03-03
    body: "I'm not complaining. More stuff like this, you have a talent for it."
    author: "reihal"
  - subject: "Summary"
    date: 2003-02-28
    body: "\"Qt rocks.  Lots of people are starting to give it more and more attention.  But not Mozilla.\"\n\nI think that's about it.\n\n\nExpanded version (see links in story for more):\n- IBM is rumored to be exploring/investing more in Qt.\n- Adobe apparently based their Photo Album on it.\n- Qt-Win32 is cooking along as part of the KDE on Windows project.\n- Mozilla dropped the Qt native port because of a lack of maintenance (it hasn't kept up with the rest of Mozilla since 0.9.9 apparently)"
    author: "anon"
  - subject: "Re: Summary"
    date: 2003-02-28
    body: "Thank you for translating this story from Stupidish to English."
    author: "David Walser"
  - subject: "Re: Summary"
    date: 2003-02-28
    body: "You dont have to be a brain surgeon to understand that story."
    author: "anon"
  - subject: "Re: Summary"
    date: 2003-03-01
    body: "Yeah, you only have to be from Mars."
    author: "Stof"
  - subject: "Re: Summary"
    date: 2003-03-01
    body: "Then <a href=\"http://www.amazon.com/exec/obidos/tg/detail/-/006016848X/102-2132992-6002519?vi=glance\">John Gray</a> should be your saviour!\n"
    author: "Navindra Umanee"
  - subject: "Re: Summary"
    date: 2003-02-28
    body: "Stupidish ;) I like it a lot.\n\nTony"
    author: "Tony"
  - subject: "Re: Summary"
    date: 2003-02-28
    body: "Bah!  What you people need is a dot.kde.org for dummies.  :P"
    author: "Navindra Umanee"
  - subject: "Re: Summary"
    date: 2003-03-01
    body: "LOL\nI got it all on first time, I love the style! :)) Excellent piece of writing which changes from the usual flat news we always read. I was laughing at each click."
    author: "annma"
  - subject: "Re: Summary"
    date: 2003-03-01
    body: "Oh, thanks annma!  \n\nAh yes, gotta love the UK tabloids...  :-)"
    author: "Navindra Umanee"
  - subject: "Re: Summary"
    date: 2003-03-01
    body: "Hmm.. It is only \"funny\" if you know what the stories are already, and you happen to be called Beavis or Butthead. Even now I'm still not sure if I have got everything:\n\n- Article comparing MFC to QT\n- Article on using QT with Python\n- IBM using QT in embedded development platform\n- Adobe using QT for one of their apps.\n- Port of the GPL version of QT to Windows is making progress\n- Support for building mozilla against QT is finally removed after being unmaintained (and broken) for some time."
    author: "Corba the Geek"
  - subject: "Re: Summary"
    date: 2003-03-01
    body: "Isn't that the whole point?\nTwo words: average users.\n\nAverage users are unwilling to learn, have difficulty finding the Start-button, and can barely use Internet Explorer. Surely you don't expect the people that KDE is targeting to understand language like that, do you?\nBefore you know it, you will be flooded by \"the KDE community is full of elitists\"-trolls again."
    author: "Stof"
  - subject: "Re: Summary"
    date: 2003-03-01
    body: "Oh whatever,  whine whine whine.  One article and it is the end of KDE as we know it?  \n\nRelax. I didn't lose any information, you've just got to click to find it.  And the people who don't understand it, they don't care about Qt anyway right? GNOME posts messages like these all the time.  Maybe mine was a little too subtle, but at least some people got it. :-)\n\nIt's called multi-level humour and I just used the tabloid style as a vehicle...  obviously many people didn't even recognise the outrageous tabloid style... it's quite an institution in the US/UK/etc.\n\nEnjoy."
    author: "Navindra Umanee"
  - subject: "from the pagan-rituals dept."
    date: 2003-02-28
    body: "The Qt version of Mozilla was dropped because.. well, there wasn't enough developer interest to keep it maintained. There were calls for people to maintain it, but I guess nobody stepped up to the plate. \n\nOh well, at least two of the three \"alternative\" browsers use Qt.. Opera and Konq/Safari."
    author: "fault"
  - subject: "Re: from the pagan-rituals dept."
    date: 2003-02-28
    body: "People did step up according to the bug report but they were in a hurry to remove it."
    author: "anon"
  - subject: "Re: from the pagan-rituals dept."
    date: 2003-03-01
    body: "Try again.  They asked for a maintainer; they got a few people willing to get it to build again.  That's not close to the same thing."
    author: "Peter Kasting"
  - subject: "Re: from the pagan-rituals dept."
    date: 2003-03-01
    body: "You're wrong again.\n\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=178987\n\nOffers were made to be maintainer."
    author: "Anon"
  - subject: "Why such panic?"
    date: 2003-03-01
    body: "Why was there such panic to get it removed? Why couldn't it be left in there to \"brew\" a bit and get patches applied by the new contributors so that it would reach a stage where it compiles? It definitely will never be brought up to the same state as the other ports again as it was nuked into orbit.\n"
    author: "Chakie"
  - subject: "Re: from the pagan-rituals dept."
    date: 2003-03-07
    body: "Eh, I thought Apple commented out all the Qt code in Safari?"
    author: "ey"
  - subject: "Just give us a preview of QT-Mozilla :-)"
    date: 2003-02-28
    body: "It hasn't to be fully functional but give us the proof that it starts faster, looks nicer, is easier to develop and that it runs on all platforms that are already supported.\n\nWell, how would my Mozilla look like with AA-fonts, Skypilot Classic Theme in QT, kdeprint-management...\n\nThe code is open but perhaps it is too big to make facts on your own ;-)\n\nDamn, opera is always under construction (crashes,java1.4) while I think that konqueror is really usable now in 3.1. (Just tell my why Java + Konqueror works correctly only in KDE and not in my icewm [seperate applet windows])\n\nLoading klipper or any other qt-app after a fresh boot may take 20 seconds to load all libs and services but after that it's very fast and I have all other apps in 3 seconds.\n\nEven my Moneyplex uses qt statically but I think it looks to much like windows without kde-support :-)\n\nKeep up that great work ;-)"
    author: "Schugy"
  - subject: "Re: Just give us a preview of QT-Mozilla :-)"
    date: 2003-03-01
    body: "actually, to whatever extent it wasn't buggy, the QT mozilla would look and behave exactly the same as the glib/xlib one. Mozilla uses it's own widget set (for reasons both good and bad) and the QT port didn't change that, it just used QT to make the implementation of that widget set portable. However, this forgoes most of the good reasons to like QT, so I'm not surprised nobody was interested."
    author: "Kevin Puetz"
  - subject: "KDE on Cygwin"
    date: 2003-03-01
    body: "Hey ! \n\nis it ready for the mass ? does my grand-parent could install kmail on windows 2000 on their own ?\n\n"
    author: "somekool"
  - subject: "Re: KDE on Cygwin"
    date: 2003-03-01
    body: "Which one? Your fathers or your mothers father?\n\nTF"
    author: "Tooth Fary"
  - subject: "Re: KDE on Cygwin"
    date: 2003-03-01
    body: "I'm sorry, I hoped you understand\n\n"
    author: "somekool"
  - subject: "Re: KDE on Cygwin"
    date: 2003-03-04
    body: "The current status is that Qt 2.3 is being ported.  It's partially functional and when it's done, Qt3 will be ported (hopefully this will be easy once Qt 2.3 is done).  I believe after that, efforts to port KDE and KDE apps will begin."
    author: "Dave Brondsema"
  - subject: "Re: KDE on Cygwin"
    date: 2003-03-04
    body: "thanks for the information.\n\ni tried to install it recently but kde fails to run.\nsystem was not able to find qt-2-3.dll\nand i didnt find any lib path.\nwell well.\n\nit would be fun to move most of my friend to KMail on Windows and once is done, it will be easier to move them to Linux(KDE)\nmany of us hate their email client\n\n\n"
    author: "somekool"
  - subject: "OT: KDE Homepage no longer standard"
    date: 2003-03-01
    body: "Jus tried,\nhttp://validator.w3.org/check?uri=www.kde.org\n\nIt worked when I first tried it with the new homepage, so it must have been some recent changes. Please make it standard again. I like to shove it in the face of my friends who say good looking web pages cannot be done with standards only.\n\nThanks,\nS.P."
    author: "Standards Police"
  - subject: "Re: OT: KDE Homepage no longer standard"
    date: 2003-03-01
    body: "Looking at the validator output, the problem's only a minor gah-we've-used-the-wrong-ID issue, so it should get fixed sometime soon.\n\nBut if you still want something to shove in your friend's faces, there's always www.wired.com or www.redhat.com among others ..."
    author: "azza-bazoo"
  - subject: "Re: OT: KDE Homepage no longer standard"
    date: 2003-03-01
    body: "Well, I've used this site as an example way too many times before, and it looks prettier now and no longer passes the validator test. See what I mean?\nI can understand the web master making a mistake, or simply not caring about standards that much, but he owe us, - \"we\", the ones who shoved kde homepage down our friends throats, as an example of nice web pages development, promoting the kde web page and kde itself as we did that -, the right to keep on doing it!\n\nLet's hope it does get fixed soon.\nS.P."
    author: "Standards Police"
  - subject: "Re: OT: KDE Homepage no longer standard"
    date: 2003-03-01
    body: "Please mail the problems to webmaster@kde.org"
    author: "Navindra Umanee"
  - subject: "Re: OT: KDE Homepage no longer standard"
    date: 2003-03-03
    body: "fixed ;)\nPlease keep in mind the page is maintained by volunteers, there can sometimes be some bug in the code, but that will be fixed in a very short timespan normally :)\nSimply tell your friends: here, the page looks good, validates well the most time and if not, it gets fixed ;) "
    author: "Christoph Cullmann"
  - subject: "I did not have a tear in my eye..."
    date: 2003-03-03
    body: "...and I am really angry about the way dot.kde.org has rewritten my submission to their story queue. \"couldn't take it no more\", \"gracefully stumbled away\", \"ugly dragon\" and \"crocodile tears\" - none of these were written by me.\n\nI originally only submitted a news item to dot.kde.org to report that a) the Mozilla folks are about to remove the Qt branch from CVS, b) they have good reasons for that since the Qt branch doesn't work and c) if we don't want to loose Qt in Mozilla, developers should put some effort into this and a project leader is needed.\n\nThat news item was submitted to the story queue quite some time before the decision to drop Qt was finally done, so it's not only sad that Navindra made such a childish rant out of it, but also that he did it this late.\n\nBut yeah, I do think it's sad that Qt in Mozilla has been removed."
    author: "Hanno M\u00fcller"
  - subject: "Re: I did not have a tear in my eye..."
    date: 2003-03-03
    body: "Relax, nobody thinks you were crying or that you said those things.  It's all inventive lies, in true tabloid fashion.\n\nYour Mozilla Qt submission -- thanks a lot, but it probably wasn't going to be posted anyway.  Trust me when I tell you that posting this to dot.kde.org effectively wouldn't have made a difference to the Mozilla situation.  I just salvaged a few iffy Qt stories and posted them together.  I thought I would use yours too instead of throwing it away.\n\nThe reason your name is there at all is to credit you for the submission, not to ridicule you!"
    author: "Navindra Umanee"
  - subject: "mozilla-qt branch"
    date: 2003-03-03
    body: "There is a branch that has been created which contains the attempt to get mozilla going on Qt. (Qt support has just been removed from the main tree, but if this gets going properly it could be reincluded) It runs, but is much slower than the gtk version and has bugs. So if anyone is keen join in and help get it going ... check out the netscape.public.mozilla.qt newsgroup if you want to find out more or get involved...\nhttp://groups.google.com/groups?group=netscape.public.mozilla.qt"
    author: "David Fraser"
---
In breaking news, <A href="http://www.freehackers.org/">convicted</a> French hacker <a href="mailto:phil@freehackers.org">Philippe Fremy</a> has been accused of spreading malicious rumours regarding an <a href="http://www-106.ibm.com/developerworks/linux/library/l-qt/">alleged stab at Qt and companion PyQt</a> by the highly regarded <a href="http://www.ibm.com/developerworks/">IBM developerWorks</a>.  But the concerted effort at dragging people's names through the mud does not end there.  Emotions ran high when it was noted that the peeps at <a href="http://developers.slashdot.org/article.pl?sid=03/02/19/203246&mode=thread&tid=136&tid=137&tid=100">Slashdot</a> couldn't leave IBM's <a href="http://www.trolltech.com/newsroom/announcements/00000118.html">rumoured</a> <a href="http://www-916.ibm.com/press/prnews.nsf/jan/85CD2A95DBD6A4C385256CB50067A865">courtship</a> of <a href="http://www.trolltech.com/products/qtopia/">Qtopia</a> alone.  
<!--break-->
<p>
To make matters worse, the phones were buzzing non-stop regarding <a href="http://www.trolltech.com/newsroom/announcements/00000120.html">Adobe's not-so-cute love interest</a>.  Microsoft-lover Adam Treat was the first to dish out the dirt <a href="http://www.trolltech.com/images/screenshots/adobe/1.jpg">with</a> <a href="http://www.trolltech.com/images/screenshots/adobe/2.jpg">highly</a> <a href="http://www.trolltech.com/images/screenshots/adobe/3.jpg">dubious</a> <a href="http://www.trolltech.com/images/screenshots/adobe/4.jpg">claims</a> regarding an alleged <a href="http://www.adobe.com/products/photoshopalbum/">Photoshop Album</a>.
<p>
We were further sickened to learn of another Microsoft connection traced back to last December.  There is now <a href="http://kde-cygwin.sourceforge.net/qt2-win32/">concrete proof</a> that underground cult <a href="http://kde-cygwin.sourceforge.net/">KDE on Cygwin</a> is engaging a truly <A href="http://kde-cygwin.sourceforge.net/qt2-win32/screenshots.php">unholy</a> <a href="http://kde-cygwin.sourceforge.net/qt2-win32/features.php">pursuit</a> in a rather foolish attempt at cloning.
<p>
With a tear in his eye, <a href="mailto:sockpuppet@hanno.de">Hanno Müller</a> reported that the reptilian <a href="http://www.mozilla.org/">Mozilla</a> finally "couldn't take it no more" and <a href="http://bugzilla.mozilla.org/show_bug.cgi?id=178987">gracefully stumbled away</a> from the whole mess.  <a href="http://women.kde.org/images/katiesmall.png">This ugly dragon</a> was rather touchingly reported to have <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/xparts/">shed</a> <a href="http://www.konqueror.org/">crocodile</a> <a href="http://www.opera.com/products/desktop/index.dml?platform=linux">tears</a> over the incident.
<p>
Tabloid reporter yours truly has been working around the clock to bring you these shocking updates regarding the highly controversial lifestyle of the <a href="http://www.trolltech.com/products/qt/">troll cutie</a>.