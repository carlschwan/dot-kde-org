---
title: "KDE-CVS-Digest for December 26, 2003"
date:    2003-12-27
authors:
  - "dkite"
slug:    kde-cvs-digest-december-26-2003
comments:
  - subject: "Kapture this!"
    date: 2003-12-27
    body: "I'm really looking forward to use kapture instead of synaptic!\n\nand... ofcourse KDE 3.2!\n/David\n"
    author: "David"
  - subject: "Re: Kapture this!"
    date: 2003-12-27
    body: "Let's hope with Plastik as default. Screenshot, tse tse."
    author: "Hein"
  - subject: "Thanks"
    date: 2003-12-27
    body: "Thanks for the updates, I'm suprised Qt has so many bindings, I thought there was only a C++, and Python version until now. =) This is great, before I thought GTK+ was my only real choice for other languages."
    author: "Alex"
  - subject: "Re: Thanks"
    date: 2003-12-27
    body: "Afaik GTK+ binding will be supported in future versions."
    author: "Hein"
  - subject: "fuse_kio!"
    date: 2003-12-27
    body: "So now there's something called fuse_kio that will let you mount any KIO slave on the *filesystem*?  And this is mentioned only casually?  If it does what it says it does, it's freaking awesome!  Can someone explain how this works?  And will it be part of KDE 3.2?"
    author: "Haakon Nilsen"
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "And, is this a first step in letting GTK+ apps be able to use KIO slaves?"
    author: "Arend jr."
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "Speaking about GTK+, I saw this tarball on Zacks webpage:\nhttp://www.automatix.de/~zack/qtgtk.tar.bz2\nNice example of integrating GTK+ and KDE :)"
    author: "Niek"
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "From what I understand, this is just a KIO slave for the protocols supported by FUSE/AVFS. See http://www.inf.bme.hu/~mszeredi/avfs/ for a complete list of supported protocols."
    author: "Niek"
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "I see, oh well.  It looks like maybe AVFS is what I'm really looking for anyway, since it lets you mount ssh, webdav and other kinds of protocols too.  I still think it would be cool to have some sort of userspace program that utilizes kio slaves to \"mount\" any kio-supported protocol though, since it potentially could take kio usability to a whole new level (read: ANY program would benefit, be it KDE, GTK, console etc, and with no recompile or porting efforts)."
    author: "Haakon Nilsen"
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "I don't know about that, the first mention explicitly says that it will allow you to, \"...mount KIO slaves with the fuse_kio module.\" I don't usually say I'm mounting something unless I'm attaching it to the filesystem. \n\nAlso he writes:\n----\nI have disabled progress dialogs where I found them activated (it\nwas quite disturbing to have a progress indicator popup on CLI).\n----\n\nIf we were using FUSE/AVFS --> KIO then would he be worrying about  dialogs popping up? To me, this really makes it sound like it's KIO --> FUSE/AVFS.\n\nWhich would be excellent IHMO. :-)"
    author: "anon"
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "http://capzilla.net/blog/2451/ \n\nRob Kaper mentions kio_fuse with some links.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "Short: Yes, the original poster is right.\n\nLong: From what I understand, FUSE lets you write filesystems that run as normal userland programs, but can still be mounted like real kernel filesystems. It kind of bridges kernel and user space. Now, fuse_kio is a program for use with FUSE which lets you use any KIO slave. It kind of bridges FUSE and the KIO slave system. String all of that together and you can mount KIO slaves like any other kernel filesystem!\n\nKIO slaves of almost all of the AVFS protocols have been around for quite a long time already.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: fuse_kio!"
    date: 2003-12-28
    body: "It is a \"fuse protocol\" with supports kde ioslaves.\nI tested it until now only by mounting the fish-ioslave. But it's still very early in development.\nExpect more to come.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: fuse_kio!"
    date: 2003-12-27
    body: "Yes this is part of the KDE/Debian/Enterprise work by Alex and Kevin. =)"
    author: "KDE User"
  - subject: "Too many features/bugs!"
    date: 2003-12-27
    body: "I wasn't expecting to see so many new features get included by this time, when we approach a stable release. And instead of the increasing number of bugs, I was expecting to see most bugs get fixed. I like KDE and I used it every day, but I think it is starting to look like MS-Office: too many features you don't need + too many bugs!"
    author: "JohnCabron"
  - subject: "Re: Too many features/bugs!"
    date: 2003-12-27
    body: "None of the new features are for the 3.2 release. They are in kdenonbeta, which is where developers commit stuff they are working on.\n\nDerek"
    author: "Derek Kite"
  - subject: "icecream?"
    date: 2003-12-27
    body: "What exactly is the project \"icecream\" in kdenonbeta?"
    author: "ac"
  - subject: "Re: icecream?"
    date: 2003-12-27
    body: "building KDE with the help of distcc-like distributed compile machines...."
    author: "Kurt Pfeifle"
  - subject: "Re: icecream?"
    date: 2003-12-28
    body: "Is that what was used at the conference in August?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: icecream?"
    date: 2003-12-28
    body: "No, that was Teambuilder."
    author: "Anonymous"
  - subject: "Kopete to boil it's own soup? Why?"
    date: 2003-12-29
    body: "<i>\nMatt Rogers committed a change to kde_yahoo_backend: kdenetwork/kopete/protocols/yahoo\n\nStart of a native KDE backend for the yahoo protocol. I've had this in the\nworks for some time now. The many reasons include libyahoo's horrible \nmemory management and better integration with the rest of KDE (use of KIO, etc.)\n</i>\n\nInstead of reinventing the wheel, maybe some resources should be spend on \nimproving libyahoo [1].\nlibayhoo seems to be quite feature-complete (it even has webcam support), so I \nreally don't see the point of writing an own yahoo library...\nAnd this will certainly take a huge amount of time to reach the features of \nlibyahoo...\nMaybe better fix libyahoo's \"horrible memory management\"...\n\n\n[1] http://libyahoo2.sourceforge.net/\n"
    author: "ac"
  - subject: "Re: Kopete to boil it's own soup? Why?"
    date: 2004-01-06
    body: "If libyahoo is feature complete, then porting it to the KDE codebase will result in all those features still being supported, along with the benefits of KDE like KIO support for file transfers, better memory management through QObjects, etc.\n\nReally, maintaining links to these external C libraries in Kopete has never been anything but a pain in the ass. The code is horrible to maintain because it is all C based, there's constant memory problems, getting things updated in the main tree is often a pain, and you've got all this duplicated/obsoltee code everywhere because algorithms are already present elsewhere in QT or KDELibs. Couple this with the fact that you can't integrate the KDE code into the library because then you lose the ability to update it when new versions come out, and you've got a big mess.\n\nThats why we have our own homegrown support for all protocols now except Yahoo and SMS. It's just not worth the effort to use the other code.\n"
    author: "Jason Keirstead"
---
In <a href="http://members.shaw.ca/dkite/dec262003.html">this week's KDE-CVS-Digest</a>:
Java bindings are now auto-generated by the build process. You can now mount KIO slaves on the filesystem with the <a href="http://www.inf.bme.hu/~mszeredi/avfs/">fuse</a>_kio module. <A href="http://www.koffice.org/karbon">Karbon</A> now has snap to grid and curve smoothing. 
Initial import of the new theme manager. You can now create 
application configuration files with KConfEdit.
<!--break-->
