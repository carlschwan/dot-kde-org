---
title: "KDE-CVS-Digest for December 12, 2003"
date:    2003-12-13
authors:
  - "dkite"
slug:    kde-cvs-digest-december-12-2003
comments:
  - subject: "Business Logic...?"
    date: 2003-12-13
    body: "I'd like to know what language will be used to add business logic to the Kexi application. This would be useful in validating input and providing informative information to a user at form level. I also wonder whether things like `input masks` and form controls will be `event driven` just like MS Access forms are. Once I get the language to be used, I'll start educating myself on how to be more productive in Kexi, which already promises [and looks] to be an exciting application.\n\nBTW, will Kexi be able to import MS Access table data?\n\nCb.."
    author: "charles"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "\nIt seems that QSA is the lanuage being used (JavaScript/ECMA-Script like language)\nSee here: http://www.trolltech.com/products/qsa/index.html\n\nbut\n\nWe want Python as the language, python rules!!"
    author: "ac"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "QSA, not KJS?"
    author: "panzi"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "QSA is more than just a JavaScript interpreter."
    author: "Anonymous"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "There's KJSEmbed though."
    author: "Datschge"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "The difference between QSA and KJSEmbed:\nhttp://xmelegance.org/kjsembed/kjsembed-qsa.html"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "QSA is licensed under the GPL, but Kexi is LGPL'd.  If QSA is integrated into Kexi, wouldn't Kexi have to be GPL'd as well?"
    author: "Burrhus"
  - subject: "Re: Business Logic...?"
    date: 2003-12-15
    body: "That's already an issue as a result of using the QT toolkit.\n\nThe whole application will have to be GPL, but you could take parts of the code and use them under the terms of the LGPL."
    author: "Jonathan Bryce"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "Have you looked at the license of QSA? Its the GPL/QPL/Commercial trilicence we are so familiar with. \nThe interpretation of this that Trolltech have made is that when choosing the GPL option,  any use of Javascript to interface with the Qt library will be GPLed. Ie if you make a Kexi front end that throws up a dialog box, and you don't have a commercial QSA and QT licence, your app must be GPLed if distributed. $2000 for an office suite? Nice. \n\nThis is incredibly stupid. Any office document that uses QSA to create a button becomes GPLed, meaning no additional restrictions can be placed upon it. No NDAs, or confidential docs with dumb ass macros, etc. Excel sheets with VB crap in them are incredibly common in industry, and are often distributed to partners as a crappy client to some B2B functionality. This won't happen for KSpread if we try to force people to GPL their documents. \n\nIts currently unknown if Trolltech will try to force this interpretation onto KJS as well. \n\nThis is a tough question, ( data and code are indistinguishable in this case). I guess the eventual solution will be to do precisely what Open Office does, create abstractions meaning that Qt is used just to implement the scripting interface of Open Office. This way the docs are always usable in Open Office, and can't be said to be linking to Qt to specifically use its functionality. \n\nIts a sad thing that Trolltechs business model relies on this. I hope Novell, Sun, IBM or Redhat will just hurry up, buy them, and LGPL the lot.... this is such a stupid issue to lose the Linux desktop over. "
    author: "rjw"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "I have to agree, at least in part. The licensing situation needs to be worked out, simplified and laid out in black and white, and more provision needs to be made for small developers."
    author: "David"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "Trolltech did not design the GPL, the FSF did. Reading both the GPL and the FAQ on gnu.org, there is not much room for interpretation. From gnu.org:\n\n\"When the interpreter is extended to provide \"bindings\" to other facilities (often, but not necessarily, libraries), the interpreted program is effectively linked to the facilities it uses through these bindings. So if these facilities are released under the GPL, the interpreted program that uses them must be released in a GPL-compatible way. The JNI or Java Native Interface is an example of such a facility; libraries that are accessed in this way are linked dynamically with the Java programs that call them.\"\n\nThe license of the interpreter itself is not relevant for this issue. IANAL, but it looks like there is no difference between KJS and QSA license wise if the application that contains the interpreter is free software. \n\nQSA comes with a simple input dialog framework that is independend from the Qt API (http://doc.trolltech.com/qsa/qsa-6.html), still one might argue that it uses facitilities that are released under the GPL.\n\nIt's a tricky question. How can you license-wise distinguish between office documents and applications? The technical differences between interpreted script, bytecode, JIT-compiled bytecode or native machine code don't really matter, the GPL just talks about a program's \"executable form\". \n\nMaybe you want to take it up on some GNU mailing list?\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "There is also the fact that with an interpreted language isnt the source automaticly open?  I was always under the impression that these scripts where kinda defacto open source.  Correct me if im wrong here.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "OpenSource and Free Software are not only about availability of source code. \nYou can have source code written in a scripting language that\nis under a very restrictive license, as in: \n\n\"Here's one copy of my software written in PHP.  \nBecause you bought a license you may use it on one server \nwith one CPU.  If you want to upgrade your server \nto two CPUs come back and buy another license.  \nAnd don't you dare passing it on to anyone else!\" \n\nI don't think that software like this is common, \nbut from a legal point of view perfectly possible\n(all IMHO, IANAL). \n\nSo the source being available doesn't mean it's open as \nin \"OpenSource\", or even GPL-compatible. \n\n\n"
    author: "cm"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "To make it more clear: \n\nYou'd be missing the right to \n- Freely use \n- Modify \n- Redistribute \nthe software even though you have the source \nand can read it to your liking when you're bored!  \n\n"
    author: "cm"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "Distribution doesn't mean posting on freshmeat for free download. A company could put a script together, which by definition is source code, distribute it, even sell it. That meets the GPL. Any data is copyright of the authors.\n\nDerek"
    author: "Derek Kite"
  - subject: "Licensing terms"
    date: 2003-12-13
    body: "I am glad this topic is here...\n\nFrom Qt COMMERCIAL LICENSE AGREEMENT FOR PROFESSIONAL AND ENTERPRISE EDITIONS Agreement version 2.4 (GENERAL TERMS THAT APPLY TO APPLICATIONS AND REDISTRIBUTABLES):\n\n9.(vi) Applications must add primary and substantial functionality to the Licensed Software\n\n--> We're working hard on this :)\n\n9.(vii) Applications may not pass on functionality which in any way makes it\npossible for others to create software with the Licensed Software\n\n--> Passing on full Qt's functionality is exactly what is beeing done by using PyQt, PerlQt, and similar wrappers wrappers. Since it's acceptable for Kexi as tool for creating Free Software, it is not for Kexi as tool for creating proprietary solutions. We tend to develop environment for both worlds. Whatever (Python or KJS or ...) will be choosen, we need to cut off powerfull Qt functionality/API from functionality that will be exposed by Kexi scripting module -- I think about version available for proprietary use -- for Free Software there will be still \"freedom as in speech\", i.e. full Qt API available, (simply another reason for develop FS).\n\n9.(viii) Applications may not compete with the Licensed Software\n\nIt is hard to build formal arguments to proove that given solution built using Qt competes with it or not. If by \"competition\" term, Qt's vendor means being easier, most cost effective solution than Qt itself, then Qt would be not suitable for developing _many_ types of software today. \n\nExample of what Joe Power User told: \n\n\"My boss wants me to develop a small database app for my company to help managing invoices. \n\na) I can do this using Qt (~1 month of work, 6 months of learning). The app will be used on some win32 and linux boxes. I've bought Qt Enterprice Edition for $3495 receiving one year of commercial support.\n\nb) I can (now a bit theoretically) using Kexi (~ 1 week of work, one month of learning). The app will be used on some win32 and linux boxes. I've had possiblility to get Kexi for free from the Internet, but I've bought Kexi for $300 from the support company, receiving one year of commercial support and additional templates, etc.\"\n\nIs this a competition? \"It depends\" you can say, and you are right. IMHO the last term I've commented here (9.viii), if strictly applied, can be harmfull both for Qt commercial developers (less flexibility, freedom) and for Qt vendor (limited market). Hope it's just a \"lawyer's talk\".\n\nLast issue: Trolltech company does not want Qt to be available for *GPL'd software on win32, what is reasonable at least for me. With Kexi future scripting functionality, such software could be developed for/on win32 (although - without Qt API).\n\n--\nregards, \nJaroslaw\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Licensing terms"
    date: 2003-12-14
    body: "Before crippling your commercial application, I would simply sign a deal with TT.\n\nMatthias"
    author: "Matthias Ettrich"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "I think the FAQ on gnu.org misses the point. I don't think the question of dynamic linking is relevant. The relevant question seems to me whether Qt and the QSA / KJS script could be considered to be part of the same work in the sense of copyright. I think in general the two should be considered separate works under copyright law. This situation is not so different from the Linux kernel and applications that run on top of it. Linus has been so kind to clear that situation up with a small clearification to the Linux license. TrollTech could follow that example."
    author: "Waldo Bastian"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "Furthermore this whole discussion is mostly irrelevant because it comes only in play when the KJS/QSA script is distributed together with Qt. Even if one would hold, the IMO far fetched, opinion that a KJS/QSA script is a derived work of Qt, it will surely not be a derived work without Qt.\n\nI know of only one company that disagrees with me on this. SCO claims that JFS remains a derived work of its UnixWare product even when it no longer incorporates any UnixWare code. The people on GrokLaw have shown that this interpretation is not shared by the courts.\n\nThe GPL recognizes this reality explicitly:\n\"These requirements apply to the modified work as a whole. If identifiable sections of that work are not derived from the Program, and can be reasonably considered independent and separate works in themselves, then this License, and its terms, do not apply to those sections when you distribute them as separate works.\"\n\n\n"
    author: "Waldo Bastian"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "See also e.g. http://www.illuminata.com/cgi-local/pub.cgi?docid=scoderived&section=scoderived4\n\n--quote--\nCase law suggests that the courts are likely to be skeptical about SCO's expansive interpretation of derivative works copyright law. Take, for example, a 1992 ruling holding that a \"Game Genie\" device, inserted between a game cartridge and the Nintendo Entertainment System for the purpose of altering features of a Nintendo game, did not violate Nintendo copyrights.2 The Game Genie certainly interacted with the Nintendo components at an intimate level-it blocked the value of a single data byte sent by the game cartridge to the CPU unit in the Nintendo and replaced it with a new value. However, the court noted that \"the examples of derivative works provided by the [Copyright] Act all physically incorporate the underlying work or works. The act's legislative history similarly indicates that 'the infringing work must incorporate a portion of the copyrighted work in some form.' \" \n--end quote--\n"
    author: "Waldo Bastian"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "> it will surely not be a derived work without Qt.\n\nWhere is the difference? When KDE argued years ago that we only ship sourcecode, or binaries without Qt, people still accused the project for violating its own license. Isn't that the same with a script?\n\nLet's ignore Qt for now and take a plain GPL application as an example: a spreadsheet with its own interpreter running some sort of scripting language. In addition to core language facitilies the spreadsheet will offer futher facitilies via script bindings to the document. The document effectivly becomes a program, with the source form being identical to the executable form. If you buy into the GPL-FAQ the document is a derived work of the spreadsheet, and you cannot distribute it with restrictions beyound those of the GNU GPL. Whether or not you bundle it with the spreadsheet does not seem to make a difference.\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "See the GPL FAQ. ;-) For me it says that in this case the document is NOT a derived work:\n-------\nCan I use GPL-covered editors such as GNU Emacs to develop non-free programs? Can I use GPL-covered tools such as GCC to compile them?\n\n \nYes, because the copyright on the editors and tools does not cover the code you write. Using them does not place any restrictions, legally, on the license you use for your code. \n\nSome programs copy parts of themselves into the output for technical reasons--for example, Bison copies a standard parser program into its output file. In such cases, the copied text in the output is covered by the same license that covers it in the source code. Meanwhile, the part of the output which is derived from the program's input inherits the copyright status of the input. \n\n\nAs it happens, Bison can also be used to develop non-free programs. This is because we decided to explicitly permit the use of the Bison standard parser program in Bison output files without restriction. We made the decision because there were other tools comparable to Bison which already permitted use for non-free programs. \n\n-------------------\nIf a programming language interpreter is released under the GPL, does that mean programs written to be interpreted by it must be under GPL-compatible licenses?\n\nWhen the interpreter just interprets a language, the answer is no. The interpreted program, to the interpreter, is just data; a free software license like the GPL, based on copyright law, cannot limit what data you use the interpreter on. You can run it on any data (interpreted program), any way you like, and there are no requirements about licensing that data to anyone. \n\nHowever, when the interpreter is extended to provide \"bindings\" to other facilities (often, but not necessarily, libraries), the interpreted program is effectively linked to the facilities it uses through these bindings. So if these facilities are released under the GPL, the interpreted program that uses them must be released in a GPL-compatible way. The JNI or Java Native Interface is an example of such a facility; libraries that are accessed in this way are linked dynamically with the Java programs that call them. \n\n\nAnother similar and very common case is to provide libraries with the interpreter which are themselves interpreted. For instance, Perl comes with many Perl modules, and a Java implementation comes with many Java classes. These libraries and the programs that call them are always dynamically linked together. \n\n\nA consequence is that if you choose to use GPL'd Perl modules or Java classes in your program, you must release the program in a GPL-compatible way, regardless of the license used in the Perl or Java interpreter that the combined Perl or Java program will run on. \n----------\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "Andreas, I think you missed the point of the discussion. From the text you quoted:\n\n> However, when the interpreter is extended to provide \"bindings\" to other\n> facilities (often, but not necessarily, libraries), the interpreted program \n> is effectively linked to the facilities it uses through these bindings. So if\n> these facilities are released under the GPL, the interpreted program that\n> uses them must be released in a GPL-compatible way\n\nA program written in emacs does not link to facilities provided by emacs. A spreadsheet document that calls functions implemented by the spreedsheet application does.\n\nWhat Waldo argues is that this linkage alone does not make it a derived work. Similar - and very loud - discussion can be found on the kernel mailing list wrt binary kernel modules.\n"
    author: "Matthias Ettrich"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "Maybe I missed something, but if we are talking about interpreted languages, like the script in a spreadsheet is usually is, it's clearly\nstated there than in case we are talking about simple data. But yes, I understand that when\nit's about calling some underlying function, in this case if it's call to\nthe spreadsheet application functions which calls a GPL'd library (Qt) \nthe situation is not so clear and may fall under the part you have also\nquoted. Sincerely I don't know much about QSA, but the simple fact that \nQSA it's licensed under GPL does not impose you to release your scripts\nunder GPL unless QSA is linked to, provides \"bindings\" to other GPL\napplications/libraries.\nFor me it seems to be a corner case that can be clarified only by the\ncopyright holders. Otherwise everybody gets headache and some people may\nend in court sued by the copyright holders, because they were misinterpreted their rights.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Business Logic...?"
    date: 2003-12-15
    body: "If you take my example program\n\n#!/bin/bash\necho \"Hello World\";\n\nIs the bash interpreter providing a binding to my program by carrying out the functions requested in the echo statement?"
    author: "Jonathan Bryce"
  - subject: "Re: Business Logic...?"
    date: 2003-12-15
    body: "No it's not, it's writing out data, and that wasn't the question either. Imagine \"echo\" wasn't an external program but a facility provided under the terms of the GPL and implemented either in the interpreter or in a library that the interpreter links to. The question discussed was, whether your program links to the library implementing \"echo\" and furthermore, whether your program is derived from the library implementing \"echo\". If you ignore the fact that echo might be a too simple case, the FAQ seems to imply both.\n"
    author: "Matthias Ettrich"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "> > it will surely not be a derived work without Qt.\n> Where is the difference?\n\nIf the work is not a derived work without Qt, the GPL is not applicable.\n\n> If you buy into the GPL-FAQ\n\nI think that the linking part is FUD on the part of the FSF in order to drive their agenda and I think that TrollTech is hiding behind this. TrollTech, as sole copyright holder, can clearify this murky licensing situation by making a\nclear statement on this. That would take away the cloud that currently hangs \nover QSA. (See also my other post about estoppel)\n\nNote that this is different from the situation with KDE since KDE had much more copyright holders involved, including copyright holders from outside the project. For KDE it was impossible to guarantee that all past and future involved copyright holders would be in support of such license clearification. And without that guarantee our users would still not be able to rely on it.\n\nTo reiterate: 'the infringing work must incorporate a portion of the copyrighted work in some form.' That is what the law says. I think the position of Trolltech / FSF is not in line with that and I think TrollTech would do itself a favor if it cleared up this controversy. I don't think it helps TrollTech if it waits for KDE to engage in a campaign to educate our users on this matter."
    author: "Waldo Bastian"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "> If the work is not a derived work without Qt, the GPL is not applicable.\n\nI was asking about the difference whether or not the script is distributed along with Qt or independently and argued that there is no such difference. In order to execute the script you need the interpreter and the GPL'ed code that implements the facitilies. In that regard it is just like linking dynamically to C or C++ libaries.\n\n> That would take away the cloud that currently hangs over QSA.\n\nIf it was only QSA... The same clouds hang over KJS or any other scriptbinding to code distributed under the GNU GPL. The problem as such is neither KDE nor Qt specific.\n\n>(See also my other post about estoppel)\n\nI have seen it. It's an interesting thought. Waldo, if you have any suggestions how to phrase it, please send them to me. But keep in mind that the estoppel in the kernel has not stopped ongoing and heated discussions.\n\nMatthias\n\nbtw: Trolltech changed name from Troll Tech to Trolltech a few years ago."
    author: "Matthias Ettrich"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "> I was asking about the difference whether or not the script is distributed\n> along with Qt or independently and argued that there is no such difference.\n\nI can only repeat myself then: 'the infringing work must incorporate a portion of the copyrighted work in some form.'\n\nIf Qt is not distributed and no portion of Qt is included in the script then there can be no infringement, which means that the license of Qt is irrelevant because no license is needed under copyright law.\n\nWhen distributed together it becomes murky because then you can debate endlessly about whether it is one single work or two separate works.\n\n> If it was only QSA... The same clouds hang over KJS or any other\n> scriptbinding to code distributed under the GNU GPL. The problem as such is\n> neither KDE nor Qt specific.\n\nThe clouds are Qt specific because of they way how Trolltech contributes to the cloud with its FAQ and with your statements on this website. Trolltech can make the cloud disappear. I agree that that doesn't change anything for non-Qt software and quite frankly I don't care about that. What I do care about is the software that is used by KDE and the effect that it has on KDE.\n\n> I have seen it. It's an interesting thought. Waldo, if you have any\n> suggestions how to phrase it, please send them to me. \n\nI will do so.\n\n> But keep in mind that the estoppel in the kernel has not stopped\n> ongoing and heated discussions.\n\nI disagree. The estoppel is about use of the system call interface. The ongoing and heated discussions are about binary only drivers. Two different things.\n"
    author: "Waldo Bastian"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "This is a *very* interesting and well made point. Clearly the move towards using the OASIS XML document format will make this all completely irrelevant, since Qt/KOffice will not be necesarry to use (run) OASIS documents. But, what would have happened if this were not the case?\n\nYou state that $2000 makes for an expensive office package, but what about KDE's license? With KDE there is no non-GPL option; all active documents would have had to have been GPLed (maybe?).\n\nLinus Torvalds recently made a convincing argument about the ability to augment the GPL with a clarification, without breaking the GPL itself; this relied on what he called estoppel, rather than modifying the actual contract directly. If Qt were a non-issue, is this how KOffice would have indemnified it's own users, and is this something OO.o already provide, or is that where StartOffice becomes useful?\n\nOne day I may truly understand the GPL by myself, but until then I remain in hope!"
    author: "Dominic Chambers"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "IANAL (BIRGL) Yes, if the KOffice developers would add a statement to the license in which they clearify how they interpret the license they would then be bound by it due to estoppel.\n<p>\nSee <a href=\"http://www.lectlaw.com/def/e040.htm\">\nhttp://www.lectlaw.com/def/e040.htm</a><br>\nand <a href=\"http://www.legal-definitions.com/contract_definitions/estoppel.htm\">\nhttp://www.legal-definitions.com/contract_definitions/estoppel.htm\n</a>\n<p>\n\"Estoppel is the doctrine that prevents a person from adopting a position, action, or attitude inconsistent with an earlier position if it would result in an injury to someone else.\"\n<p>\nFor an example see <a href=\"http://www.redhat.com/legal/patent_policy.html\">\nhttp://www.redhat.com/legal/patent_policy.html</a> where RedHat says that it will not enforce its patents against open source software.\n"
    author: "Waldo Bastian"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "BIRGL - But I Read Grok Law? ;-) "
    author: "rjw"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "> Its a sad thing that Trolltechs business model relies on this. \n> I hope Novell, Sun, IBM or Redhat will just hurry up, buy them, \n> and LGPL the lot.... this is such a stupid issue to lose the \n> Linux desktop over. \n\nNow, how probable do you think this scenario is?  They could have done so years ago and avoided much effort spent on fruitless arguments about licensing issues. \n\nSun or Redhat?  Hardly, after they have sunk so many man years of resources into GNOME. \nNovell or IBM?  No idea. \n\nWhat's Trolltech's position on this?  Would they be willing? \n\n---\n\nAnyway, seeing a technically inferior framework (IMHO) being pushed by some big players makes me sad.  And this license thingy is not to be underestimated as a reason for choosing the other toolkit.  \n\n\n"
    author: "cm"
  - subject: "Re: Business Logic...?"
    date: 2003-12-14
    body: "TBH, Novell won't be pushing a technically inferior framework if they can wean themselves off the GObject crack and completely go for Mono.\n\n.Net will be pretty much as good a dev platform as C++ and Qt once the generics (templates) are in place, and if ( big if ) they redo GTK or another toolkit in   managed code . Don't blinker yourself to this. Take a good look at it, you can't say it  is so clearly technically inferior as you can when comparing to GObject etc. \n\nBut I still love programming Qt, and I want to be able to see a future in it. ATM, its pretty hard to see it being anything but a niche with the business model Trolltech have chosen. \n\nI know that the buyout is unlikely from past history. I still want it to happen. I want there to be a real technical competion and collaboration between two good frameworks. I don't want the decision for the one true dev environment to be made on licensing or on a gentlemans agreement between the big boys. \n\n"
    author: "rjw"
  - subject: "Re: Business Logic...?"
    date: 2003-12-15
    body: "> TBH, Novell won't be pushing a technically inferior framework if they \n> can wean themselves off the GObject crack and completely go for Mono.\n\nI was referring especially to Sun and Redhat w.r.t. GTK during the \npast few years, and the recent announcements of thousands of \ndeployed GNOME desktops. \n\nGNOME has not been ported to Mono yet, has it?  \n\n\n\n> I know that the buyout is unlikely from past history. I still want it to \n> happen. I want there to be a real technical competion and collaboration \n> between two good frameworks. I don't want the decision for the one true dev \n> environment to be made on licensing or on a gentlemans agreement between the \n> big boys. \n \nSo do I.  No argument about that.  Let's hope for the best.\n\n"
    author: "cm"
  - subject: "Re: Business Logic...?"
    date: 2003-12-17
    body: "No, gnome has not been ported to mono. \n\nThats why I said they need to wean themselves off the crack.... \n\nbut its only really necessary for new development, and making sure interop is good. Rewrites can wait. "
    author: "rjw"
  - subject: "Re: Business Logic...? and code"
    date: 2003-12-13
    body: "Can somebody paste some QSA code? I have never seen this code and have not been able to setup the QSA evironment? I'll very grateful.\n\n..b.."
    author: "borg"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "Python will also be used.. and perhaps also kjs."
    author: "anon"
  - subject: "Re: Business Logic...?"
    date: 2003-12-13
    body: "looks like we will decide for kjsembed or python, i do favour kjsembed because it will be the binding for the rest of koffice and it's syntax appears more beautiful :)"
    author: "Lucijan Busch"
  - subject: "Great job on 3.2 - beta 2 working well"
    date: 2003-12-13
    body: "To the whole KDE team - great job on 3.2.  I've been running beta 2 for a couple of days on Fedora and it's going great.  I even loaded it on my production laptop  mostly to get KDevelop3.  KDevelop imported all my previous Qt qmake projects without a hitch.  I loaded KDE on Fedora, it imported all my previous bookmarks, desktop icons, newsgroups, notes, calendar, todos, and mail just fine.  And the Keramik color style looks really sharp.  Really like the new feature in Knode that shows the number of responses.  Just a couple of minor glitches:\n1) In Kontact, the inbox speedbutton uses the reply icon instead of the original inbox icon.\n2) Programs loading from Kontact seem to take a long time to load, ie longer than if Kmail is loaded by itself.\n3) When exiting Knode - it keeps bringing up a KWallet dialog \n4) Once in a while the start menu freezes (cntl-alt seems to unfreeze it) - not sure if this is a KDE thing or Fedora threading issue.\n5) The warning sound really crackles but the beep in Konqueror seems ok.\n6) Kontact doesn't seem to have any link to KPilot conduits for synching.\n\nSo, if you haven't tried 3.2 yet, try it out - the beta2 seems pretty darn stable (of course any beta carries some risk)."
    author: "sage"
  - subject: "Re: Great job on 3.2 - beta 2 working well"
    date: 2003-12-14
    body: "KPilot conduits use or can use the same data as Kontact. So if you sync, the addressbook for example will import and export the addresses.\n\nNotes also works the same. Haven't been successful with appointments yet.\n\nAnyone can correct me if I'm wrong.\n\nDerek"
    author: "Derek Kite"
---
This week's <a href="http://members.shaw.ca/dkite/dec122003.html">CVS-Digest</a>:
Mostly bug fixes. Code folding and highlighting fixes in <a href="http://kate.kde.org/">Kate</a>. Message selection and deleting bugs fixed in 
<A href="http://kmail.kde.org/">KMail</A>.
<a href="http://www.iidea.pl/~js/kexi/kexi_startup/">Screenshots</a> of <A href="http://www.koffice.org/kexi/">Kexi</A>, the graphical database front-end. 
<!--break-->
