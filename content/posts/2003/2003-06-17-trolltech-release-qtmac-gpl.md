---
title: "Trolltech to release Qt/Mac GPL"
date:    2003-06-17
authors:
  - "binner"
slug:    trolltech-release-qtmac-gpl
comments:
  - subject: "This is welcome news..."
    date: 2003-06-17
    body: "As we all know, Unix on the desktop is about 1% and MacOS (in general here, i think OSX came in like 1.5%-2%  this was talked about last year on the MacApp list) is about 4% of the desktop...  Macintosh has had a looooong history of open/free/shareware software (Hyperarchive anyone?) so this is going to be a welcome path for developers to port KDE apps to MacOSX and MOST importantly port MacOSX apps to KDE!\n\nAnyway, now Im happy because now native KWord can ship on OSX, not that it matters because their Xlib vesion has flawless printing, and AA fonts, still native is cool :)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This is welcome news..."
    date: 2003-06-17
    body: "> Macintosh has had a looooong history of open/free/shareware software \n> (Hyperarchive anyone?)\n\nHmmm? Shareware is very different to free software.... Mac hardly has any history at all in that field.\n\n> and MOST importantly port MacOSX apps to KDE!\n\nHow does that work? MacOS X apps are built on proprietary APIs, not Qt. You would have to rewrite the apps in order to port them, and AFAIK there are no open source programs that are sufficiently compelling and unique to warrant that. It'd just be the same old thing, open source code gets ported to MacOS, but none comes back. It's expected, same thing has been happening for years with Windows. Making even open source Windows programs run on Linux is very hard, but the reverse in not true (to the same extent).\n\nI'm not sure how this benefits KDE, except perhaps by having more users of Qt based software (but then again mac users could use that before via X11). "
    author: "Mike Hearn"
  - subject: "Re: This is welcome news..."
    date: 2003-06-17
    body: "The nice thing is that Qt/Mac software, unlike X11 based Mac software, looks and feels native. Thus, software can be coded for OS X and QT/Mac, and easily be ported to Linux and Qt/Linux."
    author: "Rayiner Hashem"
  - subject: "Re: This is welcome news..."
    date: 2003-06-17
    body: "Yes, that's true enough. I'm not convinced Mac developers would use a non-native toolkit personally. Boy, do they love their cocoa ;)\n\nAlso, as I stated later, I think it uses a skin. A good skin, but still not native controls mind..."
    author: "Mike Hearn"
  - subject: "Re: This is welcome news..."
    date: 2003-06-18
    body: "> I'm not convinced Mac developers would use a non-native toolkit personally\n\nI dont think the Mac version of Qt is marketed for primarily Mac developers, but insteadd for people who want to develop apps cross platform. Mac users are being increasingly exposed to free software and I think more and more people \n\n\n> Also, as I stated later, I think it uses a skin. A good skin, but still not native controls mind...\n\nBased on what I've heard, it uses carbon. Qt-Mac apps, such as Psi (great app-- best jabber app ever btw, use it all the time) or TOra (which hasn't officially been released for OSX, but preview builds work), look and feel just like Carbon apps. If it uses skins, i'll be VERY suprised because it works with the carbon no-lines hack. Only thing that seems to be weird is the toolbar handle but I think that's because carbon doesn't natively support moving around toolbars. MS Office v.X has it's own toolbar handles as do any apps with that functionality."
    author: "rilla"
  - subject: "Re: This is welcome news..."
    date: 2003-06-18
    body: "Right. Early versions of Qt/Mac used a skin (pixmap based QStyle), but we changed over to using the OSX Appearance Manager API to draw  widgets some time ago. \n\nThis is native, even if an OSX machine is using a third party (unsupported) OSX theme, or if apple brings out a new theme then Qt apps will draw widgets using that theme.\n\nDon.\n"
    author: "Don Sanders"
  - subject: "Re: This is welcome news..."
    date: 2003-06-18
    body: "cocoa, once you start using it, proves to be IMHO the greatest interface and programming environment ever. don't get me wrong, i'm still a 40% linux user, and gcc is minimalistically awesome. but the 40% mac user in me loves cocoa to death. and the dev tools are free, like they should be. forget visual studio! but this Qt/Mac looks promising, considering that i now do some of my linux coding in Kate running inside X11 on Mac OS X, but it's a hindrance to have to run X11 just to get Kate to run. great job, guys! (in case you were wondering, the other 20% of me is windows XP!)"
    author: "Paul Irwin"
  - subject: "Re: This is welcome news..."
    date: 2003-08-17
    body: "If you like Cocoa and Linux, you might want to look at GNUstep (if you haven't already). It's based on the same OpenStep framework as Cocoa."
    author: "Rakko"
  - subject: "Re: This is welcome news..."
    date: 2003-06-18
    body: "I think, from the Konqueror shots, it just looks like a skin, because the Konq toolbars don't integrate so well into a Mac look."
    author: "Rayiner Hashem"
  - subject: "Re: This is welcome news..."
    date: 2003-06-18
    body: "MacOSX doesn't have a standard widget for windows-style toolbars handles. That's why it's like that. Microsoft apps, such as Office v.X are similiar-- but imho, a better apporach-- toolbar is a mini window at the top of the screen.. that isn't very OSX like, but is at least classic macos like.  It's not a skin. You can prove this by running one of many aqua themes from resexcellenece =)\n\nOther apps on OSX that have Windows-like toolbars also have their own custom toolbar handles. Even apps made in Cocoa do."
    author: "pret"
  - subject: "Re: This is welcome news..."
    date: 2003-06-18
    body: "They use Carbon, after all, which isn't really native like Cocoa is. Besides, I think the only reason the KDE screenshots look like they're using a skin is because the KDE toolbars don't fit the OS X look very well."
    author: "Rayiner Hashem"
  - subject: "Re: This is welcome news..."
    date: 2003-06-18
    body: "> They use Carbon, after all, which isn't really native like Cocoa is.\n\nYeah, Qt/Mac uses Carbon, but this doesn't matter anymore in since MacOSX 10.1.5. Both Cocoa and Carbon in 10.1.5 (and 10.2.x) go through the Appearance Manager before being sent to the Window Compositing Manager. Back in 10.0.x and 10.1.0-4, Carbon apps often looked similiar to Cocoa apps, but had \"one pixel off\"-type of problems that didn't always make them feel like Cocoa apps. This isn't true anymore and you're likely not to notice the difference. \n\nApple did a real great job with this after initially blundering it up in the initial release(s) of OSX. It's a good model that Microsoft didn't follow between win16 and win32. "
    author: "fault"
  - subject: "Changing Now: Apple is rockin' with OSS"
    date: 2003-06-18
    body: "shipping with KHTML, tcsh, apache, X-Window System, Mach-Mk/BSD/Darwin etc.\n\nThis is great news\n\nOh yeah Unix on the desktop == 1% ... I expect that to increase to greater thatn MacOS share very soon.  If you audit a campus for \"type of desktop\" you'll see lots of Unix and it's increasing.\n\nOur campus is more like 5% Unix or more ..."
    author: "OSX_KDE_Interpollination"
  - subject: "Re: Changing Now: Apple is rockin' with OSS"
    date: 2003-06-18
    body: "Erm, MacOSX is perfectly a form of a Unix (but isn't officially a OpenGroup-labelled \"UNIX\"), and has more of a right being called that than Linux, which is only Unix-like and not directly decended from either svr4 nor bsd, does.\n\nSee http://www.levenez.com/unix/history.html"
    author: "fault"
  - subject: "go into the light"
    date: 2003-06-18
    body: ">> Macintosh has had a looooong history of open/free/shareware software \n>> (Hyperarchive anyone?)\n>>\n> Hmmm? Shareware is very different to free software.... Mac hardly has\n>any history at all in that field.\n \nNo.  From a HyperArchive search, all of these are free, with source code that one can reuse.\n\n- SpriteWorld\n- TransSkel\n- Mozilla\n- Mesa/Mac\n- GW-Ada/Ed-Mac\n- Brian Hook's 3D tools\n- AIFF/DSP framework\n- StonerView \"produces a weirdly hypnotic helix of rippling colors\"\n- ircle, IRC client\n- Blender/Mac, 3D modeler\n- Jotto II, a word game based on logic and frustration\n- Quesa\n- Darwin\n- QuickTime streaming server\n- Road Runner/Mac\n- GCC enhancements.\n- Improvements to KHTML and KJS.\n- Countless enhancements to other free software.\n\n> How does that work? MacOS X apps are built on proprietary APIs, not Qt.\n\nQt is as much a proprietary API as OpenStep.\n\n> You would have to rewrite the apps in order to port them\n\nNot using GNUStep.\n\n> It's expected, same thing has been happening for years with Windows.\n\nIt is true: the rest of the world is still catching up to Unix circa 1980.\nBut we are catching up.\n\n> I'm not sure how this benefits KDE, except perhaps by having more\n> users of Qt based software (but then again mac users could use\n> that before via X11). \n\nGNUStep and Qt are the best multi-platform solutions that I know of right not.  Java needs speed and a free, obvious RAD environment.  I just became exposed to Qt and it was by Qt/Mac.  Perhaps now I will port my free software to Linux.\n\nEveryone wins as programmers become free to distribute on multiple OS's.\n\nCheers,\n  James\n\njamesp_uhuh_670@earthlink.net\nremove _uhuh and the second underscore to email"
    author: "verytechnical"
  - subject: "Wonderful News!!"
    date: 2003-06-17
    body: "This is real welcome and great news. It means several things.\n\n1). KDE apps running natively on OSX without X. Yummm!  I've been running KDE apps in OSX through fink for a while now-- they work well, but it'd work so much better, and natively, through Qt/Mac. \n\n2). It would be easier for MacOSX developers, such as Apple, to use KDE technologies, such as khtml. :)\n\n\nMad love and props to TrollTech!!"
    author: "pret"
  - subject: "Re: Wonderful News!!"
    date: 2003-06-19
    body: "Yeah, maybe the will use the KHTML engine to build a nice browser!! I wonder what they would call such an app... "
    author: "Lumpy"
  - subject: "Cool..."
    date: 2003-06-17
    body: "This is the best time to start a UI revision team improving KDE programs' UI by creating .ui files of suggestions. I expect something like that to work best with OSX users since they tend to be very picky about UI's. Let's take that opportunity. ;)"
    author: "Datschge"
  - subject: "Knew this would happen sooner or later"
    date: 2003-06-17
    body: "In fact, I could tell when people from TrollTech started to port parts of KDE to Qt/Mac :)\n\nNow, ask yourself, why would they do that? KDE is a huge way to show off what Qt can do, and porting it to Qt/Mac would undoubatly get more buyers of the commercial Qt/Mac. \n\nThanks Trolls!"
    author: "John"
  - subject: "QT+KDE"
    date: 2003-06-17
    body: "\"Hell, let's just merge Qt and KDE, release for all platforms and be done with it.\"\n\nThat's not a statement from the in-charge-guy at Qt, but I wish it was. ;)\n"
    author: "Apollo Creed"
  - subject: "Re: QT+KDE"
    date: 2003-06-17
    body: "in-charge-guy at TT, of course. But surely you got that."
    author: "Apollo Creed"
  - subject: "Re: QT+KDE"
    date: 2003-06-18
    body: "Maybe QT+KDE+GNUstep running on Mach Microkernel and BSD ... practically equals free less fancy OS/X where all apps are easily portable over to the commercial OS\n\nEventually a replacement for X would be nice though :-)\n"
    author: "OSX_KDE_Interpollination"
  - subject: "Can this be used for a non-GPL feasibility study?"
    date: 2003-06-17
    body: "I need to port a Win32 app to the Mac.\n\nI started a feasibility study of using wxWindows to do this, but their Mac port is  a little too behind the curve to use at this point.\n\nCould the same study be done with the GPL'd QT/Mac?  I sort of remember QT's stance being that you couldn't use the GPL'd version unless you're writing GPL'd code - even if you're willing to pay up before releasing your code.\n\nMy problem is, without a feasibility demo, I can't get my company to spring for commercial QT, but the company's even less likely to commit to GPL - especially for the purposes of said feasibility demo.  I need to operate below the radar at this point, and a strict GPL requirement would make this impolssible.\n\nAny comments?"
    author: "Rob"
  - subject: "Re: Can this be used for a non-GPL feasibility stu"
    date: 2003-06-17
    body: "Easy. The Qt evaluation program.\n\nhttp://www.trolltech.com/download/qt/evaluate.html?cid=10"
    author: "cwoelz"
  - subject: "Re: Can this be used for a non-GPL feasibility study?"
    date: 2003-06-17
    body: "The Qt Evaluation program was made for people/companies exactly in your situation. "
    author: "anon"
  - subject: "Re: Can this be used for a non-GPL feasibility study?"
    date: 2003-06-17
    body: "I don't understand your dilema. You may use GPL code internally without releasing it. As long as this is just a feasability study, you should have absolutely no constraints. If you decide to bundle your app or sell the app then you have to make your application GPL. The GPL makes it easy to operate below the radar until you are ready to sell your app. It sounds like that is the time you would want to purchase your  commercial license or better yet GPL your code!. Just my 2 cents. \nBrian"
    author: "Brian"
  - subject: "Re: Can this be used for a non-GPL feasibility stu"
    date: 2003-06-17
    body: "I thought QT specifically said you can't use the GPL version to develop an app and then pay up when you decide to release a non-GPL version.\n\nBut other posters mentioned a QT evaluation program, so I guess this problem's been eliminated.\n\nThanks,\nRob"
    author: "Rob"
  - subject: "Re: Can this be used for a non-GPL feasibility stu"
    date: 2003-06-18
    body: "GPL is GPL and Trolltech cannot prohibit using GPL version for any internal purposes, whatever they are. They are just unhappy when 5 programmers create commercial QT application, then they buy 1 licence, recompile application and release it. Such a behaviour seems legal to me, but a bit unfair, of course.\n\nJerzu"
    author: "Jerzu"
  - subject: "Re: Can this be used for a non-GPL feasibility stu"
    date: 2003-06-18
    body: "They can't put restrictions up the GPL, but they can put any restrictions they want on their own license for Qt Commercial. They can say, \"Qt Commercial can't be used in the development of an application that has previously been developed by Qt Free Edition without keeping the app open sourced...\" \n\nOf course, I've never actually heard trolltech going after someone for doing this...\n\n> They are just unhappy when 5 programmers create commercial QT application, then they buy 1 licence, recompile application and release it.\n\nYeah, this policy was made to stop this from happening. "
    author: "rilla"
  - subject: "Re: Can this be used for a non-GPL feasibility stu"
    date: 2003-06-20
    body: "Actually, there was recently a discussion on the Qt mailing list covering that topic.\n\nThe result was the following:\n\nIf parts of an application have been developed using Qt/Free you can't license that application with Qt/Commercial.\n\nThis was also confirmed by Dimitri from Trolltech. However, this seems hard to enforce. Still if you have product that uses Qt/Commercial and it somehow turns out that parts of this application have been developed using Qt/Free, you're violating the license terms and should probably call your lawyer ;)\n\n--\nStefan"
    author: "Stefan"
  - subject: "GPL only"
    date: 2003-06-17
    body: "From the announcement, this appears to be GPL only. It is not the X11 GPL/QPL licensing. What this means is that non-GPL but still 100% Free Software applications cannot use it. Examples of stuff you CANNOT port to GPL Qt/Mac include Cervisia, Kicker, and PixiePlus.\n\nPlease, Trolltech, offer this under the QPL as well. It makes absolutely no sense to have to purchase the proprietary version of Qt in order to distribute Open Source Software."
    author: "David Johnson"
  - subject: "Re: GPL only"
    date: 2003-06-17
    body: "<snip>\nPlease, Trolltech, offer this under the QPL as well. It makes absolutely no sense to have to purchase the proprietary version of Qt in order to distribute Open Source Software.\n</snip>\n\nIf you purchase Qt you can distribute your Qt app as closed source. Read before you post."
    author: "cartman"
  - subject: "Re: GPL only"
    date: 2003-06-17
    body: "And you should understand before you post."
    author: "Anonymous"
  - subject: "Re: GPL only"
    date: 2003-06-17
    body: "Uhhh... he was talking about free and open software that simply wasn't GPL'd. Examples of this are anything in KDE which is BSD, QPL, Artistic, etc.. licensed. \n\nMuch of this is made possible by the dual licensing. "
    author: "anon"
  - subject: "Re: GPL only"
    date: 2003-06-17
    body: "I don't know about the other licenses, but BSD apps can certainly be linked with GPLed QT, it's just that the derivative work is licensed under the GPL."
    author: "Jim Dabell"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "Nope, since QT is also under QPL which allows any and all true FOSS licences the app can keep its BSD licence."
    author: "Datschge"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "We are going in circles here.\n\nCartman was asking for QT/Mac to be licensed under the QPL - it isn't at the moment.\n\nThen anon asserted that this prevented BSD licensed software from linking with QT/Mac legally.\n\nThen I pointed out that this wasn't true, since you can link BSD and GPLed software, with the derivative work being GPLed.\n\nThe fact that other versions of QT are available under the QPL doesn't mean that the Mac version is.  The Mac version isn't, and this means that if you want to link to it, you must agree to the GPL, pay for the commercial license, or use the evaluation version, which expires after 30 days.  None of these are the QPL, and only the GPL is feasible for an open-source project to use."
    author: "Jim Dabell"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "Yes, you are right, I got fooled by the line stating QT/Mac has a dual licence (commercial, GPL). I guess I better start calling QT/X11's licencing scheme a triple licence (commercial, QPL, GPL) for avoiding confusing myself again."
    author: "Datschge"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "That's like saying you can vote for whichever candidate you want, so long as you vote for the one Trolltech wants you to vote for. It's not a BSD licensed app if it must be licensed under the GPL."
    author: "David Johnson"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "If I'm interpreting correctly, the app+Qt combination is GPL licenced on the Mac; if you ship a binary dependant on Qt/Mac, that binary as a whole is under the GPL. If your app is also available under another GPL-compatible licence, the end user can obtain that application under *that* licence, and is only then forced back to GPL if they distribute a Qt/Mac binary based on the free edition of Qt. If they use the commercial edition of Qt, they are not forced to GPL the combination."
    author: "Simon Farnsworth"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "No.\n\nYou write code.  The copyright on this particular code belongs to you.  This work can be licensed however you want.  Let's say you pick a BSD-style license.\n\nWhen linking it to the QT toolkit, you are creating a derivative work (the binary) based upon both your work and Trolltech's work.  Now, you need a license from Trolltech to distribute this.\n\nTrolltech offer a number of licenses.  If you are linking against the Mac edition, you have the opportunity of paying them for a license (the \"commercial\" license option), you can choose the evaluation license (which expires after 30 days), or you can choose the GPL license.\n\nThe first two options let you distribute the derivative work however you see fit.  The GPL has a clause that requires any derivative works to fall under the GPL license.\n\nNow, somebody else comes along.  They may want to distribute the derivative work under the GPL.  That's okay - since you offered your copyrighted work to the public via the BSD license - which allows incorporation into any and all derivative works without any other requirements.  They can therefore distribute the derivative work under the GPL.  This is what is meant when people talk about \"GPL-compatible licenses\".\n\nNow, another person comes along.  They have a commercial license for QT, and wish to distribute the derivative work in binary-only form.  This is also okay - your work is under the BSD license, which allows incorporation into any derivative works, and they already have a license saying that it's okay for them to do the same with Trolltech's work.\n\nAt no point has your original work not been available under the BSD license.  If people have a commercial license for QT, they can give away binary-only forms of your work.  If they port it to a platform where QT is available under the QPL, they can link it with that and give away binary-only forms of your work.  If they partially rewrite it so that it uses a different toolkit, they are subject to that toolkit's licensing constraints.\n\nIt is only the Trolltech original work's license that is restrictive - you are free to license your work any way you see fit, and you can pick the BSD license if you so desire.  But you can't relicense Trolltech's works for other people, you can only ask them (politely) to offer licenses that you want them to.\n"
    author: "Jim Dabell"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "<snip>When linking it to the QT toolkit, you are creating a derivative work (the binary) based upon both your work and Trolltech's work. Now, you need a license from Trolltech to distribute this.</snip>\n\nLinking does not create a derivative work. In the case of static linkage, the GPL applies because you are actually distributing the library along with the binary. But in the case of dynamic or runtime linkage, you are not copying, modifying or distributing the GPLd work, so the GPL should not apply. Trolltech and the FSF disagree, put here's a link to an informed and educated counter-opinion by the general counsel of the Open Source Initiative: http://rosenlaw.com/html/GL18.pdf.\n\nI don't have my own team of lawyers, so I find it easier to simply ask Trolltech to offer Qt/Mac under the same terms as Qt/X11, rather than challenge them on the matter in court."
    author: "David Johnson"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "It's simple:  Even dynamic linking creates a derivative work.\n\nThe reasoning is not legal, it's technical.  Dynamically linked binaries contain information from the libraries against which they were linked against.  In the case of linking your application against Qt, you are incorporating code compiled by from the Qt headers (which is under the GPL) in your binary."
    author: "anon"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "Linking as \"creating a derivative work\" is so widely accepted in the community that I would certainly not want to go against it.  That's a good link David, I was aware of the position, but unaware of the document to go with it.  Thanks.\n\nI personally disagree with the technical reason you cite, anon.  Otherwise, merely cloning the interface would be enough to infringe on the copyright (remember Harmony?).  There are fringe cases such as macros though."
    author: "Jim Dabell"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "Well it does mean that, and as far as I know it is true.  \n\nIf you clone the interface/macros etc in a clean room manner, then you've hit the jackpot.  Of course you have to link against your clone and make sure it actually works with the real Qt.\n\nLogical conclusion..."
    author: "anon"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "<snip>Dynamically linked binaries contain information from the libraries against which they were linked against</snip>\n\nThe information they contain is limited to a reference to an interface. You are using  the library in its intended manner through a published API. Absolutely no code from the library is incorporated into the application. A good analogy is a hyperlink on a webpage. Does that hyperlink indicate derivation from the linked page?\n\np.s. Macros and inlines may be an exception. If there are few of these then Fair Use may allow their fair use [sic]. If there are a lot of them, the argument might still be made that implementation has been placed into the API allowing it to be used by the intended and customary mechanism."
    author: "David Johnson"
  - subject: "Re: GPL only"
    date: 2003-06-17
    body: "I don't want to distribute my Qt app as closed source. I want to distribute it as 100% BSD licensed Free Software. I want to distribute my software with zero restrictions, but Trolltech is telling me that their \"free\" software requires that I restrict the rights of my users."
    author: "David Johnson"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "GPL doesn't restict the right of users, on the contrary, it protects users by not allowing companies to come along and integrate the open source code into their products and close it down into proprietary lock-in software as is unfortunately possible with BSD-style licences."
    author: "tuxo"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "Basically, the GPL restricts the freedom of people to restrict the freedom of others."
    author: "hydralisk"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "When it is not allowed for my users to use my software because Qt/Mac is GPL-only, then the GPL is indeed restricting the rights of my users."
    author: "David Johnson"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "You will restrict the rights of a minority of users that are up to no good and preserve the freedom for the rest. I prefer freedom over lock-in anytime."
    author: "tuxo"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "The GPL asserts absolutely no controls over the rights of end users. It is not a use, but a distribution license. It does affect your rights as a developer if you want to use their code. I think you may be using the word \"rights\" incorrectly here, to mean \"business desires.\""
    author: "Michael Greer"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "No, nobody is beiong restricted - you are perfectly free to not use Qt at all."
    author: "Stephen Douglas"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "Well said. I can't understand people complaining about Trolltech desisions.\nTrolltech is free to, to make the decisions they like or think are right, this includes excluding other licences other than GPL."
    author: "protoman"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "<snip>I can't understand people complaining about Trolltech desisions.</snip>\n\nWhen Microsoft recently released a license that said in essence \"you can't use the GPL\", the community was outraged. But when Trolltech says \"you must use the GPL\" it's okay. I don't see the fundamental difference between the two."
    author: "David Johnson"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "Yep, agreed.\nI think M$ should release anything they have in any license they wish, if this don't brake the law (or previous licenses), I don't see why so much complaining arround there.\n\nAnd remember people defends GPL agains the \"viral\" speech from M$ but attacks m$ right to do the shared source....\n\nI don't like BSD, I think it's bad (don't worry to reply about this, keep reading...), but that just me. I won't even say anything besides this from anyone who wants to use BSD, and I must always enphasis the \"I\" in the statement. Simply saying BSD is bad/GPL is bad isn't a nice thing, always remember it's just YOUR option and fights will diminish or vanish.\n\nPeace people!"
    author: "protoman"
  - subject: "Re: GPL only"
    date: 2003-06-19
    body: ">But when Trolltech says \"you must use the GPL\" it's okay. I don't see the >fundamental difference between the two.\nThe difference is that forcing GPL serves many people, while not allowing GPL serves mainly one big company, in this case M$."
    author: "tuxo"
  - subject: "Re: GPL only"
    date: 2003-06-18
    body: "See this posting by Matthias Ettrich of Trolltech, http://lists.kde.org/?l=kde-licensing&m=104970829609721&w=2 :\n\n\"The non-GPL code in KDE that I'm aware of grants a superset of rights (typically X11 or BSD-style licenses), meaning linking to a GPL'ed library is not a problem at all.\""
    author: "Anonymous"
  - subject: "QT Script?"
    date: 2003-06-17
    body: "Is QT Script comparable to VB Script/Visial Basic? Where are the tutorials? I think it's time to learn it. Can I use Kommander and utilize QT script behind the scenes to do useful stuff with databases the way I use VB/Access in the MS world?"
    author: "Charles"
  - subject: "Re: QT Script?"
    date: 2003-06-17
    body: "Sincerly I don't know anything about QT Scripts, but even if right now you can't use them directly in Kommander (can anyone with QT Script experience try it?) I believe it will be quite easy to implement if the only requirement is to pass the scripts to some interpreter to execute. And it will give you a lot of power.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: QT Script?"
    date: 2003-06-18
    body: "QT Script and Kommander are two different animals. It's possible to make Kommander use QT Script or, as an alternative, Rich Moore's Javascript bindings. Some consideration has been given to this along with enabling a default script repository to help users do scripted actions. There are some changes in the works for Kommander. they include more widget support, data widgets and script objects as well as sub dialog objects. Since Kommander is language neutral it will be very easy to use with PHP 4.3x in CGI mode. This means PHP authors could create quick mini apps much like PHP GTK but with KDE widgets and I think it a much nicer environment. Kommander also directly communicates via DCOP.\n\nPerhaps the primary difference between the two is that you can actually make a dialog with Kommander that does something and has absolutely no code because of our text associations. QT Script will have the advantage of a newer implementation with QT Designer and in this area we could catch up with more developers to support it. Marc has had little time for it lately.\n\nThese applications are similar. It appears QT Script will be more widely supported and will have extensive capabilities as a scripted development environment. However Kommander will remain simpler with text associations and have advantages in language neutral development, built in DCOP and possibly other areas.\n\nWhat I really hope is that a language specific program without the simple and quick abilities Kommander has does not end up overshadowing it. In my opinion Kommander has a lot of potential for all of KDE. I really hope we get some developers with time to work on it as we have with Quanta. It can extend any KDE app with DCOP pretty easily."
    author: "Eric Laffoon"
  - subject: "Re: QT Script?"
    date: 2003-06-18
    body: "QT Script is based on ECMAscript, so it's roughly the same as Javascript/JScript.\n\nhttp://www.trolltech.com/products/qsa/index.html"
    author: "Jim Dabell"
  - subject: "Looks cool"
    date: 2003-06-17
    body: "Good to see the GPLd Qt get more powerful! One question - is that just a Mac-style skin it's using in those screenshots, or is it actually using native widgets?\n\nIt kind of looks a bit, hmm, messy? Hard to describe. It looks like it is using a skin though...."
    author: "Mike Hearn"
  - subject: "Re: Looks cool"
    date: 2003-06-17
    body: "To keep to the notion of 'code-once-compile-many' Qt doesn't use native widgets on the different platforms. \n\nOn the MS platform, the style is very simple to emulate, and so you wouldn't notice, but on the Mac it shows. \n\nQt on the Mac does not utilise Cocoa."
    author: "AFAIK"
  - subject: "Re: Looks cool"
    date: 2003-06-18
    body: "No, actually, Qt/Mac does use Carbon to draw widgets. It's as native as Carbon is, which being an official Apple API, is pretty damn native. Back before Qt/Mac was released, Apple helped TT with this. Qt/Mac of course doesn't use Cocoa. \n\nQt/Windows on WindowsXP also uses natively-drawn widgets.\n\nNote that neither is \"Native Widgets\".. they are, however, \"Natively-drawn\"."
    author: "anon"
  - subject: "Re: Looks cool"
    date: 2003-06-24
    body: "Yes, it's just a skin.  That is the nature of Qt -- nothing ever maps to a native control -- ever.  That makes it all the more difficult for Trolltech to port Qt since it always has to change the appearance of its widgets whenever developers of the operating system change the look of their controls."
    author: "Mike Vance"
  - subject: "Re: Looks cool"
    date: 2003-06-24
    body: "Once again, it isn't a skin. It usses the apperance manager. It's as native as Carbon or Cocoa."
    author: "lit"
  - subject: "wonderful news"
    date: 2003-06-17
    body: "qt is a wonderful toolkit, hearing that it will become free for another platform is wonderful, but i continue hoping a gpl version also for windows. I don't know if trolltech is planning to do this, but it would be a great thing it would present kde and other qt application to the great public. \nAnd imagine a kde windows desktop where will be hard to recognize if it is windows or linux, great interoperability and it would be easier to switch.\nok, ok, stop dreaming :-)"
    author: "mart"
  - subject: "Re: wonderful news"
    date: 2003-06-19
    body: "I fully agree with you !"
    author: "fred"
  - subject: "My dream ..."
    date: 2003-06-18
    body: "Wouldn't it be nice if TT could LGPL the core part of QT (hopefully on all platforms)? Only the part the KDE libs link against. That would ease the adoption (w.r.t. to the LGPL GTK libs) and they would not loose their business case and could keep all the database / motif / whatever exotic QT-stuff separate GPL/commercial. "
    author: "AC"
  - subject: "So, CLX next?"
    date: 2003-06-18
    body: "Imagine Kylix for OS X! that's based on Qt."
    author: "blaising mark"
  - subject: "Re: So, CLX next?"
    date: 2003-06-18
    body: "Imagine CLX for QT3, or writing KDE apps in Kylix. Oh well keep dreaming..."
    author: "Simon"
  - subject: "Arg!"
    date: 2003-06-18
    body: "I've spent the last 4 months porting a fair amount (> 15 kloc) of qt code to std c++ on the backend (removing *all* qt, and writing my own classes that map to qt's classes where needed) and rewriting the gui in native cocoa/objective-c.\n\nAnd now I discover it was completely unnecessary!\n\nArg! :P"
    author: "Shamyl Zakariya"
  - subject: "Re: Arg!"
    date: 2003-06-18
    body: "Hey, my heart goes out to you. Kommander was supposed to be totally revolutionary and now the question remains whether it will get teh attention it still deserves. So... any chance you feel like working on Quanta or Kommander for the Mac? ;-)\n\nCheers,\n\nEric"
    author: "Eric Laffoon"
  - subject: "Re: Arg!"
    date: 2003-06-18
    body: "Kommander will get the attention when it develops once it stops being bundled with a cvs copy of Quanta. :)\n\nIt probably already works on MacOSX too. "
    author: "anon"
  - subject: "Re: Arg!"
    date: 2003-06-18
    body: "Probably it would have been unnecessary anyway.  When you say you're porting to std c++, I suppose you aren't talking about the GUI, just converting QString to std::string and likewise.  If that's the case, you could have used the non-GUI part of Qt/X11, which is very portable and should not been hard to compile under OS X at all.  This is done for example in Doxygen: http://www.doxygen.org\n"
    author: "em"
  - subject: "Double arg!"
    date: 2003-06-18
    body: "Well, I obviously didn't read your comment completely before posting my reply.  My bad.  What I said still applies to the \"backend\", though."
    author: "em"
  - subject: "Re: Arg!"
    date: 2003-06-18
    body: "yup.. doxygen does this, as does tinyqt (http://www.uwyn.com/projects/tinyq/-- which probably already builds fine on osx without any X dependencies)"
    author: "anon"
  - subject: "Great, what I want to read next is ..."
    date: 2003-06-19
    body: "Great, what I want to read next is: \"Apple integrates QT into next MacOS X version\", that would be wunderfull.\nI hope Unix/Linux community will profit form the QT-port, too, not only Mac users.\nWith CUPS integration into MacOS X I thougt, now it's easy to shipp MacOS and Linux drivers with all printers, but only MacOS X drivers are shipped."
    author: "Manfred Tremmel"
  - subject: "Re: Great, what I want to read next is ..."
    date: 2003-06-20
    body: "I know this is starting to get pretty off topic, but I was surprised to discover the other day that I could just copy those MacOS X drivers off my mac and on to my Linux box to get it to print to the same printer.  This was for a Lexmark Optra L.  Your mileage may vary."
    author: "Aaron Bolding"
  - subject: "Re: Great, what I want to read next is ..."
    date: 2003-06-20
    body: "Sounds good! I don't understand, why the printer manufacturer don't use this for marketing. To create a little RPM for RedHat and SuSE on the CD and a \"supporting Linux\" to the wrapping shouldn't be a problem."
    author: "Manfred Tremmel"
  - subject: "Re: Great, what I want to read next is ..."
    date: 2006-05-03
    body: "THE TORTOISE AND HARE\n\nThough feelings are kind\nTo any sound mind,\nThe slow are as well\nTo build a hard shell.\nWe're not all so bound\nTo cover much ground:\nThe tortoise and hare...\nHave nothibng to share."
    author: "Abdul Murphy"
  - subject: "Re: Great, what I want to read next is ..."
    date: 2006-05-03
    body: "THE PITCHER PLANT\n\nThe Pitcher Plant has won the prey,\nWhile they are half asleep -\nFrom soothing nectar by a pool...\nTo draw them in so deep.\nThough any creature on the ball\nCan turn and fly away...\nSo being free again to live\nAnd fight another day.\n\nThe Pitcher Plant and anything\nWith power, speed and charm...\nMay serve each other well - and both\nAre free of any harm.\nHowever: should another dare\nTo go in there with pride...\nThen they are surely travelling on\nThe road to suicide."
    author: "Headstone Frollo"
  - subject: "How do you get through to people?"
    date: 2006-04-17
    body: "I have moved into a property in Surbiton.\n\nThe previous resident was Harriet Little, who had 'The Business' newspaper delivered to her.\n\nAlthough I have phoned her several times, and then even wrote to 'The Business' newspaper people, and wrote to Harriet Little - it hasn't made the slightest bit of difference.\n\nHarriet Little has moved to: Orchard Close, 1 Oxshott Way, Cobham, Surrey.\n\nHow do I get through to people?\n\nAlthough it may be a Little waste (as I don't read that sort of rubbish) - maybe I should remember that nothing in Nature is wasted... and so the 'Disappearing Rain Forests' are no longer any concern of mine, or 'Food Mountains' for that matter."
    author: "New resident"
  - subject: "Re: How do you get through to people?"
    date: 2006-05-01
    body: "Being thought of (in and outside religion) as a boring old goody-goody -\nfor not wanting to get into men's talk,\nI thought I would let my hair down, and be a bit more worldly...\nand, therefore, here is some interesting reading:\n\n'GURU WITH ANY VIRGIN' by Bindya Dundett\n'ASIAN AFFECTION' by Sexton Zewon\n'PURITY WITHIN HOLY ORDERS' by Peter Fillier\nand...\n'SEXUAL APATHY' by Burnett Urben"
    author: "Abdul Murphy"
  - subject: "Possibly overcoming a certain problem?"
    date: 2006-05-10
    body: "Dear Reader,\n\nBeing born slow and clumsy, to me, the Law of the Jungle, hasn't appeared to allow me to be closely intimate with womankind - even though male peer-pressure (conveniently hidden from women) is trying to force me to think about women -  the way women don't want me to.\n\nThe consolation is that, although being slow and clumsy means that I need to learn physical and emotional Independence - yet still... being slow and clumsy does prevent me qualifying for the forces, when people should ever be called up.\n\nAlso, what made me live for the first time in ages was finding out, through experience, that:\n\nEating Only When Hungry Reduces Libido.\n\nBeing in more physical control helped me to emotionally see the Law of the Jungle in a deeper way, and bring Responsibility, Bravery and Unselfishness into the equasion:\n\nIf a woman cannot feel safe with any individual man - then she has every right to feel safe from him.\n\nI do hope this will help anyone concerned.\n\nYours sincerely,\n\nHeadstone Frollo "
    author: "Headstone Frollo"
  - subject: "Re: How do you get through to people?"
    date: 2006-08-23
    body: "how do people travel to and from the rainforests?\n\n[For School]"
    author: "briahna "
  - subject: "Re: Great, what I want to read next is ..."
    date: 2006-05-03
    body: "Question:\nWhat goes...\nBrown, yellow, black, pink,\nbrown, yellow, black, pink;\nbrown, yellow, black,\nbrown, yellow, black;\nbrown, yellow,\nbrown, yellow;\nbrown, brown, brown, brown, brown, brown...?\nAnswer:\nThe world, in time."
    author: "Abdul Murphy"
  - subject: "Will there ever Win32 port of Konqueror?"
    date: 2003-06-19
    body: "Or is it held hostage by the lack of a Win32 GPL version?"
    author: "Fredrik"
  - subject: "Re: Will there ever Win32 port of Konqueror?"
    date: 2003-06-25
    body: "Someone is trying to port Konqueror to Windows without qt, much like Safari is really Konqueror for Mac OS X without qt. I don't know how far along he is, maybe you want to look on the mailing list for the file browser at <http://lists.kde.org/?l=kfm-devel&r=1&w=2>"
    author: "Frank"
  - subject: "I am compiling Qt/Mac as we speak..."
    date: 2003-06-24
    body: "anyone know of docs on how to build KDE against this version?\n\nThanks"
    author: "Xslf"
  - subject: "Re: I am compiling Qt/Mac as we speak..."
    date: 2003-06-24
    body: "Get KDE, get the patches, apply them, build?"
    author: "Anonymous"
  - subject: "Re: I am compiling Qt/Mac as we speak..."
    date: 2003-06-25
    body: "Ah, but where do you get the right pathes from?"
    author: "anona"
  - subject: "Re: I am compiling Qt/Mac as we speak..."
    date: 2003-06-25
    body: "Click twice on \"got much of KDE builing\" links and last but not least enter the patches/ directory. Expect problems you may have to solve yourself, but try."
    author: "Anonymous"
---
<a href="http://www.trolltech.com/">Trolltech</a> announced today that they <a href="http://www.trolltech.com/newsroom/announcements/00000129.html">will release a GPL version</a> of <a href="http://www.trolltech.com/products/qt/mac.html">Qt/Mac</a> on June 23rd at the <a href="http://developer.apple.com/wwdc/">Apple's World Wide Developer Conference (WWDC) 2003</a> in San Francisco taking their successful dual licensing strategy for <a href="http://www.trolltech.com/products/qt/x11.html">Qt/X11</a> to the <a href="http://www.apple.com/">Mac</a>. Also the upcoming <a href="http://www.trolltech.com/products/qsa/index.html">Qt Script for Applications</a> (QSA), Trolltech's scripting toolkit for Qt-based applications, will be available under a dual license on Mac OS X. 
Sam Magnuson from Trolltech already <a href="http://ranger.befunk.com/blog/archives/000072.html">got much of KDE building on Qt/Mac</a> a while ago (screenshots showing <a href="http://ranger.befunk.com/screenshots/qtmac/ss_konqueror.jpg">Konqueror</a>, 
<a href="http://ranger.befunk.com/screenshots/qtmac/ss_kontact.jpg">Kontact</a>, <a href="http://ranger.befunk.com/screenshots/qtmac/ss_kproductivity.jpg">Games</a> and <a href="http://ranger.befunk.com/screenshots/qtmac/ss_kwordpresenter.jpg">KOffice</a>).  The new license now offered allows to distribute binary builds of KDE for Qt/Mac soon.
<!--break-->
