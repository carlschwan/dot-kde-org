---
title: "Kastle 2003: What to expect from Qt 4"
date:    2003-08-26
authors:
  - "binner"
slug:    kastle-2003-what-expect-qt-4
comments:
  - subject: "Homebrewn?"
    date: 2003-08-26
    body: "Why didn't Matthias use kpresenter?"
    author: "trynis"
  - subject: "Re: Homebrewn?"
    date: 2003-08-26
    body: "Because he's cool enough to write his own presentation tool. :)"
    author: "J\u00f6rgen Sigvardsson"
  - subject: "Re: Homebrewn?"
    date: 2003-08-26
    body: "Cool or fool - it's all the same...\n"
    author: "Gnulix"
  - subject: "Re: Homebrewn?"
    date: 2003-08-26
    body: "Those are probably the five most insightful words ever written on this site."
    author: "Neil Stevens"
  - subject: "Re: Homebrewn?"
    date: 2003-08-26
    body: "So \"not\" was not really insightful? Just wondering."
    author: "Matthias Ettrich (not really)"
  - subject: "Re: Homebrewn?"
    date: 2003-08-26
    body: "Because kpresenter sucks ... openoffice Impress could sound an insult ... M$ PowerPoint could start tht third world war ... Gnome stuffs =8-| oohhh nooo ... the best thing was done his own tool"
    author: "Yoko"
  - subject: "Re: Homebrewn?"
    date: 2003-08-26
    body: "KPresenter does not Suck, I use KOffice for all of my everyday things and I do enjoy  it.  I look forward to the next version of KOffice and KDE"
    author: "Roberto J. Dohnert"
  - subject: "KPresenter & OpenOffice"
    date: 2003-08-26
    body: "\nWe just need someone to pick up the ball with KOffice--As a programmer, I am far more happy with KOffice but as a user, I am happier with OpenOffice.  I often wonder if we could swap out various components of OpenOffice with KParts--at least simple ones like the File Open Dialog which would make it immensley more useful.  I like its web page designer for making quite sites, openning and saving through so many different means would make updating content of websites a lot easier.\n\nKOffice is a great theory and initial implementation but we need people to work on it.  I wish so much that I had the time.\n\nMatthew"
    author: "Matthew c. Tedder"
  - subject: "Re: KPresenter & OpenOffice"
    date: 2003-08-26
    body: "> I like its web page designer for making quite sites, openning and saving through so many different means would make updating content of websites a lot easier.\n \nOh please! I don't know what \"quite sites\" are exactly but if I had been satisfied with it as a tool Quanta Plus would have died pre 2.0. Now 3.2 will have visual page layout... and imagine this, with a tool actually designed to work on web sites you can hit F8 and click a button to upload all the files you updated. Last I looked there was visual candy, but no real tools. We spent several years in development to make sure visual design can conform to the DTD you selected instead of hacking how it saw fit. I hope you're not suggesting OO as a viable alternative development path. Quanta 3.2 will easily put such rambling to bed.\n\n>   KOffice is a great theory and initial implementation but we need people to work on it. I wish so much that I had the time.\n \nI had neither time nor many of the skills when I got involved with Quanta. I've had people tell me they wished they had the time who had a lot more free time than me. Rather than compare just look at the basic rule... you always find the time to do the things that are the most important to you. It's a matter of internal priorities, not external circumstances, that make you available. There are currently people working on Koffice but it could use more. I want to but I don't want to compromise Quanta/Kommander."
    author: "Eric Laffoon"
  - subject: "Re: KPresenter & OpenOffice"
    date: 2003-08-28
    body: "Eric:\n\nSorry to break it to ya: Quanta Plus' interface is noisy.\n\n-Peace out"
    author: "AC"
  - subject: "Re: KPresenter & OpenOffice"
    date: 2003-08-28
    body: "Have you compared it to dreamweaver MX recently? Much less noisy than that (but granted, less functional too)"
    author: "anon"
  - subject: "Re: Homebrewn?"
    date: 2003-08-27
    body: "I had annoying wrist problems when writing the presentation, and kpresenter still forces me to do many more things with my hands than just writing the text.\n\nThe little thinggy I was using instead was a 1000 lines hack I once did for fun. We now use it sometimes at Trolltech for internal presentations within the development team. It's unflexible, can't do effects and is rather boring. The \"master slide\" with the TT logo is hardcoded. But it allows me to write simple slides very fast and reorder them with little interaction.\n\nI would have loved to \"sex up\" my presentation with some of kpresenter's many features, but I wasn't able to this time."
    author: "Matthias Ettrich"
  - subject: "Re: Homebrewn?"
    date: 2003-09-01
    body: "So, if you don't mind, any plans in releasing this thingy? \n\nYour 1000 lines hack looks clean, lean and straight forward. Just enough to get a few text line to the auditorium, without messing up with mgp oder a huge application.\n\n--\nthorsten"
    author: "thorsten"
  - subject: "kool"
    date: 2003-08-26
    body: ">> # New set of classes as part of a main window/docking architecture that distinguish between dockwindows and toolbars with support for tabbed docks.\n\nYeah, better docking support, finally! :)\n\nWill it also have support for creating a 'unified' main window?\neg. When you look at Windows (and I believe OS X) programs you'll see that everything is drawn correctly with no useless or missing bevels, with every Linux program (Qt, Gtk, whatever other toolkit) this has never been the case and it's one of the reasons why many apps still have that geeky look or looking like something's missing (like with KWrite; missing bevel around the text window - another example would be Konqueror; the toolbars and lower views don't flow into each other, it looks like they're patched together).\n\nAnd about those icon views, will the native Qt views have support for alpha blended icons in Qt4?\n(so not the current stippled icons, which look terrible (especially on TFT/LCD monitors))\n\nAnd lastly, I really hope those double buffered widgets aren't going to make Qt4 slower.. (like happened with Gtk2)\n"
    author: "whatever"
  - subject: "Re: kool"
    date: 2003-08-26
    body: "> And lastly, I really hope those double buffered widgets aren't going to make Qt4 slower.. (like happened with Gtk2)\n \n\nThe problem is that with Qt 2 and beyond, half of the widgets are double buffered and half of them are not. I still see a lot of flicker in things like menus, but little in terms of things like text edits.\n\nI think consistancy is good approach here. Also, remember that flicker is also an accessability issue, since a lot of people (est. 12.5% of the population) get headaches from seeing flicker on CRT screens. Using a LCD will help, but not everyone can afford one yet. "
    author: "KDE"
  - subject: "Re: kool"
    date: 2003-08-26
    body: ">> The problem is that with Qt 2 and beyond, half of the widgets are double buffered and half of them are not. I still see a lot of flicker in things like menus, but little in terms of things like text edits.\n\nYeah, I've noticed it when resizing windows with scrollbars in it.. usually gives some very bad looking white flashes (most themes don't set the background color to a sane value I guess?)\n\n\n>> I think consistancy is good approach here. Also, remember that flicker is also an accessability issue, since a lot of people (est. 12.5% of the population) get headaches from seeing flicker on CRT screens. Using a LCD will help, but not everyone can afford one yet.\n\nI'm still getting headaches when seeing flicker on my LCD..\n"
    author: "whatever"
  - subject: "Re: kool"
    date: 2003-08-26
    body: "That's an entirely different kind of flicker from the refresh rate/interlace problems which CRTs have.  The 'update flicker' of GUIs is just... annoying :)"
    author: "Lee"
  - subject: "What happened to..."
    date: 2003-08-26
    body: "...the component API (Qt's answer to KParts etc.). Has it been shelved till Qt5?\n\nAlso according to DE-CVS-Digest for August 22 KDE dialog boxes can now be used outside of KDE apps. Will Qt take advantage of this to blend in a little more? Actually is Qt doing anything on its dialogs, I'm thinking specifically about the Open/Save dialogs. Everyone else has moved (or in the case of GTK is moving) on. They're looking pretty old fashioned now, any work on souping them up.\n\nThis is just icing on the cake though, Qt makes programming in C++ a dream, thanks to everyone at Trolltech."
    author: "Bryan Feeney"
  - subject: "Re: What happened to..."
    date: 2003-08-26
    body: "Very good question. My guess is that it is just too complicated, and Qt is already quite componentized.\n\nRegards,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Any RAD improvements?"
    date: 2003-08-26
    body: "While it is great that Trolltech concentrates on improving the core library, I  didn't see much that interested me as a commercial developer.  Maybe I missed something.  As a developer, I am much more interested in productivity features ie improvements in designer, new widgets, alpha support in graphics, a better html help system, more support in cross-compiling, UML, integration with editors or IDE's, etc.  RAD and the faster, better, cheaper mantra is not a goal in business today, it's survival and I just don't see anything here that really addresses this issue.  Maybe Trolltech believes RAD belongs in the KDevelop and Umbrello projects - and while the work on those projects are impressive, the delivery, support, integration, etc is just lacking for regular commercial use.  ie. Gideon has been out in various betas but I still can't get a binary copy for RH9 and don't have time to mess with CVS etc.  We need a suite, not necessarily from 1 vendor or project, but that is compatible and works easily together.  Bottom line: Library optimizations are good, but for a 50% speed improvement it's easier to buy next month's Intel box - coding productivity is our real bottleneck.\nJohn"
    author: "Sage"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "If coding productivity is your problem then why are you using C++? Since you have established that speed of execution is not a major issue use a higher level language to code in which will give you far better gains then the library can ever hope to give you. Then code only the parts the profiler shows you is too slow in a lower level language. Ovverall you app should be developed probably 9-10x faster and perform with 5-10% the speed of your current app but would bbe far cheaper to produce. You can often even get it faster since the higher level code is easier to use more intelligent algorithms for."
    author: "kosh"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "Maybe because C++ can be one the best languages regarding productivity.\nSTL and generic algorithms is the key. \n"
    author: "mtm"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "Ever heard of Eiffel ?"
    author: "marko"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "AHAHAHHA HAHHAHA HHHAAAAAAAAAA AAAAAAAA AAAAAAAAAA AAAAAAAAAA\n\n\nWe did Eiffel in my first year of Uni, and everybody utterly HATED it..\n\nThey wanted to Java instead... \n\nI have to agree, Eiffel does suck.\n\n!!theObject.make();"
    author: "David Pye"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-28
    body: "Java bites and Eiffel's syntax is horrible.  Just like Smalltalk: ugly as sin."
    author: "AC"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "mtm,\nMy thoughts too.  And add templates to the list.\nJohn"
    author: "sage"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-27
    body: "I have not seen any way that someone can be as productive in C++ as they can be in python. There are other higher level languages also that can also be more productive."
    author: "kosh"
  - subject: "Re: Any RAD improvements?"
    date: 2004-07-10
    body: "Speed improvements are important for embedded systems, where it might not be\npossible to just get more powerful hardware.  As far as I know qt is\nthe best for seamlessly going from desktop to embedded without having to \ndo a whole lot of work."
    author: "David"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "You forgot a compiler! ;-) Course if you buy a compiler on Windows/Mac you'd get a full IDE with it. And of course on Unix there IS a stable verison of KDevelop. And there's QDesigner for application design, which when merged with the IDE of your choice gives you a powerful RAD system. \n\nAt the end of the day coming up with an IDE that hooks up with all the compilers and source control systems on all Qt's platforms is a complex project, and one that a lot of organisations won't need. E.g. they might have Visual C++ as a compiler, in which case they've got the IDE and SourceSafe for version control. Same with Unix: they've G++, KDevelop and CVS. An experienced professional developer should be able to set these up.\n\nDon't see how new widgets and alpha-blending will help with \"productivity\". The widgets seem okay to an on-and-off again hobbiest like me, alpha-blending is handled by styles, don't see any great reason to have it in apps (there's always OpenGL). It's the utility classes that speed up implementaiton, a Qt equivalent to the KDE's KConfig and Java's Properties would be nice, implemented in a cross platform manner using, e.g. files on Unix and the registry on Windows. Some crypto classes would be nice too (nothing too exotic, SHA, MD5, DES, Blowfish if possible). These could possibly be worked into support for TLS networking. While I've never used it people do seem to want some sort of component technology as well.\n\nI will concede that by and large this seems to be a behind-the-scenes release. The work on threading is great and must have taken time. Same with the optimisations. The fact is Qt is maturing, there aren't a lot of big huge leaps forward left for it to make!"
    author: "Bryan Feeney"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-27
    body: "> It's the utility classes that speed up implementaiton, a Qt equivalent to the KDE's KConfig and Java's Properties would be nice, implemented in a cross platform manner using, e.g. files on Unix and the registry on Windows.\n\nYou can use QSettings class, it works without any problems (Windows: registry or UNIX: ~/.qt/.${appname}rc)."
    author: "V\u00e9lizar VESSELINOV"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "As Matthias pointed out, the memory and speed improvements help mostly in case of embended devices, but I don't think that anyone will complain if Qt uses less memory and is faster. And they also focus on other things, including RAD. He said that with the new Qt Designer, it will be possible to embend it in KDevelop. BTW, KDevelop3 had only alphas and no betas, so don't be surprised if no distro ships it. \n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "The talk was targeted towards open source developers, essentially things we considered the most interesting for the core kde developers here in Nove Hrady.\n\nRAD improvements are definitely a topic for Trolltech."
    author: "Matthias Ettrich"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "Hi Matthias, \nThks for info - I can understand you were targeting the audience - I just wanted to put in a plug for what we need most and glad Trolltech will be addressing that.\n\nbtw Qt has been a great product for us and already greatly increased productivity and performance over Java swing (which even faster processors don't help much unless maybe using native compilation such as swt/gnu-j - but I find Qt is still easier to use).  \n\nI guess I vision the day of quickly creating some classes in Umbrello that automagically generate all the grunt code like gets,sets, deep copy constuctors,etc (maybe based on our own custom templates), then quickly laying out some dialogs in designer that automagically does all the io and validation tasks, then us coding our own slots and algorithms, then instantly running it thru tests in Kdevelop such as valgrind, then clicking to compile to any platform and then creating a nice installation package thru a wizard.  Then it's simple to keep modifying program design and features.  It seems many of the pieces are already there or coming, and wow - that would make for super extreme programming or maybe Moore's law of application development!\nJohn Taber\n\n"
    author: "sage"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "I can fully see a kdevelop3-final, qt 4.0, and kde 4.0 all within the same timeframe (next year)\n\n"
    author: "anon"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-26
    body: "KDevelop 3 Final will be released together with KDE 3.2 based on Qt 3.2."
    author: "Anonymous"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-27
    body: "Gideon / KDevelop-3 should build (and, for the most part, work - some plugins may depend on KDE-3.2) on KDE-3.0.4+ and Qt-3.1+ (IIRC)"
    author: "teatime"
  - subject: "Re: Any RAD improvements?"
    date: 2003-08-27
    body: "Well, as kdevelop3 will go beta as kde 3.2 goes beta? is that the plan?\n\ncool :)"
    author: "anon"
  - subject: "\"QString::isNull is dead\""
    date: 2003-08-26
    body: "At last ! I'm very very happy to this moronic niggle in the API finally\ngo away. I'm also pleased to hear that the library is being split up.\n\nI hope the thread support is faster. Our app spends a significant amount\nof time locking and unlocking, but it's not even threaded (qt-mt is often\nthe only installed version ...)"
    author: "John Levon"
  - subject: "Re: \"QString::isNull is dead\""
    date: 2003-08-27
    body: "What's wrong with isNull()?"
    author: "xsr"
  - subject: "yahoo!"
    date: 2003-08-26
    body: "This is some very interesting news!\n\nFinally, Qt non-GUI is being split from Qt GUI.  This will make it easier to use Qt in non-GUI applications under KDE.  :-)\n\nSpeed increase?  Oh yeah!  Woohoo.\n\nAccessibility support?  I'm a bit skeptical of that one...  Who thinks they can do it?"
    author: "anon"
  - subject: "Re: yahoo!"
    date: 2003-08-26
    body: "> Accessibility support? I'm a bit skeptical of that one... Who thinks they can do it?\n\nThey already have good accessibility support under Windows, all they have to do is extend it to OSX (Macintosh Accessiblity Manager) and X11 (atk/atspi, whatever..)"
    author: "fault"
  - subject: "Binary and Source Compatibility"
    date: 2003-08-26
    body: "I don't want to be a wet blanket, but is breaking source and especially binary compatibility so soon really such a good idea? Optimisation and cleanups are great, but it doesn't strike me as being really important enough to make up for all of the headaches that will come with it. Hell, we can sit on our collective asses and do nothing for a year and Intel/AMD will provide us with a greater than 18% speed up. Binary and packaging compatibility is already a disaster on Linux.\n\n(I thought that a lot of startup problems on Linux had to do with the limp runtime linker. Has anything been fixed on that front?)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Binary and Source Compatibility"
    date: 2003-08-26
    body: "So soon?  Qt4 is expected a year from now.  Could be more.\n\nAnd with these changes you can now develop non-GUI libs that link against Qt core.  That gain alone is worth it for KDE...  DCOP for example."
    author: "anon"
  - subject: "Re: Binary and Source Compatibility"
    date: 2003-08-26
    body: "Also, if planned changes are announced now, when Qt4 will hit the shelves, most Qt3 software developed in the meanwhile will be VERY easy to port!"
    author: "Vajsravana"
  - subject: "Re: Binary and Source Compatibility"
    date: 2003-08-26
    body: "It seems to me that until this stuff is mature, the occasional refactoring is something we'll need to suck up.\n\nI.e., if it needs to happen, better to do it now than later.\n\nAnd once it does happen, if they're right about lowering the bar for new programmers, then OSS may be well served by the move."
    author: "Christian"
  - subject: "Re: Binary and Source Compatibility"
    date: 2003-08-26
    body: "It's not so much about mature vs not mature:\n\nhttp://konsole.kde.org/NoveHrady2003/Day2/dscf0158.jpg\n\nLike Matthias says, where is Motif now?  Even Windows has moved on with .Net."
    author: "ac"
  - subject: "Re: Binary and Source Compatibility"
    date: 2003-08-26
    body: "Thanks for the link.\n\nAlthough I stated it badly, I meant what Matthias said on the matter.  I wholly agree with his conjecture."
    author: "Christian"
  - subject: "Re: Binary and Source Compatibility"
    date: 2003-08-27
    body: ">>Intel/AMD will provide us with a greater than 18% speed up\nYes, but not everybody plans to change it's computer in the next year.\n\nI agree that binary compatibility is a disaster on Linux.  However, this is the price of always seeking for better and more easily understandable code.  I think it's more a problem of the distro makers, unless you use any of the source based distributions.\n\nIf you want KDE to beat the others GUI you have to make it faster than them, among other things.  Many people choose IceWM for this reason."
    author: "Ned Flanders"
  - subject: "Re: Binary and Source Compatibility"
    date: 2003-08-27
    body: "Unfortunately, unless Intel and/or AMD decide to distribute new CPUs and motherboards at no cost, some of us will still be using our 400MHz PIIs a year from now."
    author: "Tukla Ratte"
  - subject: "And?"
    date: 2003-08-26
    body: "> Among the open questions Matthias addressed to the KDE developers were if KDE will switch to Qt 4\n> and if so when, when KDE switches who does the initial port and where,\n> and will KDE use the opportunity to refactor some of its architecture too?\n\nWhat about the answers?"
    author: "CE"
  - subject: "Re: And?"
    date: 2003-08-26
    body: "In the current KDE CVS code base there and many remarks throught the code of depreciated classes or functions.  When KDE4 is released these will be removed/fixed.  This should provide a nice memory reduction along with making things cleaner.\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: And?"
    date: 2003-08-26
    body: "Can and have to those questions already be answered? KDE HEAD cannot switch to Qt4 until at least a Beta version is available, KDE cannot plan to make a release based on Qt4 if the Qt 4 Final release date is unknown. The work will be done by those who have time then, it will likely happen in HEAD and the architecture refactored."
    author: "Anonymous"
  - subject: "QKernelApplication"
    date: 2003-08-26
    body: "Yes!!  I have been waiting for the gui / non-gui separation.  Finally, using Qt in server applications will become a real possibility.  And yes I know of TinyQ, but it lacks QObject (and thus the event loop), which Qt 4 will have in the kernel portion.  Good stuff!"
    author: "Justin"
  - subject: "Re: QKernelApplication"
    date: 2003-08-26
    body: "You know you just have to pass false to the QApplications constructor to disable GUI, right?\n"
    author: "Kevin Krammer"
  - subject: "Re: QKernelApplication"
    date: 2003-08-26
    body: "Yep, that's what I do now.  The problem is that I still have to bag the whole Qt library, which requires X11 to compile and run, even if I don't use those parts."
    author: "Justin"
  - subject: "Re: QKernelApplication"
    date: 2003-08-27
    body: "How good would Qt be at, for example, developing a mail/calendaring/messaging server in? So instead of having Kroupware, which consists of a bunch of third-party servers, you could have a Qt-native groupware server sharing many of the same features supported by the client (Kontact).\n\nIt would seem Qt's networking support is pretty advanced based on the amount of different KIO protocols that have been developed...\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: QKernelApplication"
    date: 2003-08-27
    body: ">>How good would Qt be at, for example, developing a mail/calendaring/messaging server in?<<\n\nShould be good enough. Qt's networking support is indeed a weak spot, but it should be enough for TCP. Otherwise you could use thiagom's libqtaddon for this, or in the worst case the low-level socket api. And anyway, networking is just a tiny, almost negligible aspect of servers like that. TCP is trivial, the hard part is to implement the protocol and the data store.\n"
    author: "Tim Jansen"
  - subject: "Re: QKernelApplication"
    date: 2003-08-27
    body: "have you not used glib or curses before?"
    author: "deo"
  - subject: "Qt event loop lack"
    date: 2003-08-26
    body: "The biggest problem with the Qt event loop is that there is only one per application. If you have a multi-threaded program with persistent threads, then it is likely that each thread will need it's own event loop.\n\nI have written my own solution based on sigwait(), but it's a pain in the ass to keep hacking it in to each new version of Qt."
    author: "John Robertson"
  - subject: "What about SVG support?"
    date: 2003-08-26
    body: "\n"
    author: "BobCat"
  - subject: "Re: What about SVG support?"
    date: 2003-10-23
    body: "i would also like to see better support for vector graphics..."
    author: "blight"
  - subject: "QT AND KDE/QT INTEGRATION!!!! WHENNNN!!!???"
    date: 2003-08-26
    body: "Okay, this was promised forver now and this has to be one fo the most important things that Trolltech can do for DE developers. On every other platforms upported by Qt, error messages native, file dialogs are native etc. and yet on KDE the platform that is actually freaking built on Qt, we don't even have consistent file dialogs between Qt and KDE!! THIS IS RIDICULOUS! There has to be something done about this as was promised about a year ago!"
    author: "Alex"
  - subject: "Re: QT AND KDE/QT INTEGRATION!!!! WHENNNN!!!???"
    date: 2003-08-26
    body: "Exactly the fact that KDE is build on Qt is what complicates this, AFAIK. You will get circular dependencies. Maybe that would be solvable in runtime?"
    author: "Andr\u00e9 Somers"
  - subject: "Re: QT AND KDE/QT INTEGRATION!!!! WHENNNN!!!???"
    date: 2003-08-26
    body: "You should read the story before about the Kastle workshops carefully."
    author: "Anonymous"
  - subject: "Qt on DirectFB"
    date: 2003-08-26
    body: "You might want to have a look at this new QtDirectFB screenshot:\n\nhttp://www.directfb.org/screenshots/FirstQt.png\n\nI'm really looking forward to having the KDE libraries independent from QtX11.\n\n\nBest Regards,\nDenis Oliver Kropp\n"
    author: "Denis Oliver Kropp"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-26
    body: "I read Microsoft is going to introduce allot of animation using hardware accelleration for their Longhorn. Will this also be possible with all this?\n\nI find this interesting since I am not aware of active resarch on this, it seems Longhorn will steel the show on window animations. Please correct me if I'm wrong."
    author: "AC"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-27
    body: "Du you relly need a jumping OK button?"
    author: "Carlo"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-27
    body: "There are plenty of plans to get better eye candy on X11. \n\nProbably the most likely to succeed is plans to turn the OpenGL and X relationship inside out. \nThis would mean an X driver that uses OpenGL for all 2d operations, and hopefully splitting out hardware initialisation like mode switching etc. \n\nObviously it would still be possible for lower end systems not to use this, etc etc. We aren't going to kill your ipaq or P133 or whatever. \n\nFor this to be acceptable for all uses, some colour profile and optional high precision rendering work must go into OpenGL. \n\nThe next stage would be retooling the toolkits to draw directly with GL. \n\nAll this is long term, and probably dependent entirely on volunteers, so don't hold your breath. "
    author: "rjw"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-27
    body: "By now I have said this a couple of times on these boards, KDE WILL be doing (several) Microsoft catch ups 'again'. Not many developers really care about this, but it does reveal some bad aspects of how KDE is almost entirely dependant on (often jobless) volunteers. Holding ones breath for features is therefor often routine. Yes it works, but more and more open source projects themselves will have to 'compete' on how they manage themselves. \n \nAbout Longhorns new GUI, most developers respond with that MS will probably implement Apple GUI features. I believe this is true - to some extend. MS has created a 'new' GDI code-named Aero. I bet allot of research is going into this and the developers are experimenting/toying allot with it.  (with research I mean: what animations will help improve the windowing and make it easier to navigate) It will do what DirectFB, X11, Qt only promise to do. \n \nOne reaction on this board is: \"Do you rely need a jumping OK button?\". Such a mindset is so absolutely appalling when it comes to vision and progress of usability! Tell that to some Microsoft developers working on Longhorn and you will look so silly and irrelevant in your green dragon shirt. \n \nI am relatively new to Open Source, but how can KDE ever provide innovations trough _ongoing_ research? Isn't this an inherently impossible thing to do with KDE-Open Source and only possible with solid fundings? \n \nThe thing which just 'bothers' me is that now many developers are nay saying and when Longhorn comes out, you bet there will be catching up (again) and then they are stuck with X11 or some hack. Key to the issue is that Microsoft did research and KDE could only do catch up and eventually innovate from its new gained abilities. \n \nIts almost paradoxical, calling an open source desktop superior, while admitting there are shortcomings due to dependence on developer volunteering. Is there such a thing as \"KDE research\"? And if there is, would it be like KDE-usability, totally dependand on volunteers who seem to update their site 1 time a month (if not half a year)? \n \nOpen Source projects could improve in the organization/management beyond the developer organization. I truly believe that and am myself working on projects which are more concentrated on organization/managing of open source projects. The beauty is that these projects are all based on GPL software for web code. \n \nOne thing management should ensure is that any developer yelling \"Do you rely need a jumping OK button?\" will fall flat on his face and will be buried in visionary open source _competition_. Yes it already is that case, but it would be better to have less developers waste less time on pre-doomed mindsets. Key to all this is project alignment. \n \nI believe we will begin seeing a trend where projects will emerge which don't necessarily develop open source code, but who will develop open source \"supportive services\". Kind of like sourceforge but then stretched to other fields - all serving a supportive role for open source. \n \nAnyway, enough rant here is a possible informative link about Longhorn: http://www.winsupersite.com/showcase/longhorn_preview_2003.asp"
    author: "AC"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-27
    body: "With respect, AC, I think you may be overly optimistic about what Longhorn's graphical features will entail.\n\nIn the very, very long term, full 3D desktops might be desireable, but that would be in CAVEs and systems capable of offering the user an interactive 3D experience. A monitor is inherently 2D, and with the best will in the world, making each window a surface offers very few advantages. It essentially allows you to do window effects. If you look at OSX, you'll see clearly that it is used to shrink the window to a thumbnail in real time (looks like the window is being poured down a funnel) and a few other things, all of which are eye candy, and that is the most evident example. In fact, the only Quartz effect I have ever seen that is useful is the one in Panther, where you can shrink your windows on the desktop, so that you can zoom out and get a better look at all your work. Even this is of limited used, since the OSX gui limits users who want to open large numbers of windows and Quartz itself cripples the machine with large numbers of windows, simply because it allocates a full framebuffer-sized chunk of memory per window. Longhorn essentially wants to be able to play around with windows in the same way as OSX, with fancy effects. It isn't a productivity improvement or usability enhancement for the most part (the zooming being the only exception).\n\nMoreover, if you look at Longhorn, their other features are:\n- The bar becomes a PocketPC-type today application. Karamaba already does this, and on the larger space of the desktop. People have been trying to empty the desktop for years, but to what purpose? Karamba solves that question.\n- WinFS, the meta-searching of the file system. It's not actually a filesystem itself, just a search service over the top. Downsides:\n-- You need and SQL server always running in memory\n-- Your SQL server needs the contents of every file you want to index.\n-- You get namespace collisions\n-- You're trying to map a heirarchical filesystem to and from a relatonal db.\n-- Users don't want to try and Google for their files based on a phrase they think they added to it six months ago. They want to have a folder with an obvious name, and find files beneath that, even the ones they had forgotten.\n-- Users don't want to have to deal with adding metadata every time they save a file.\n\nAside from making the UI a bit more cute and shipping the .NET virtual machine with it, that's Longhorn in a nutshell.\n\nThere is nothing there that users actually need. There is nothing there that improves their workflow. Essentially, since windows 95 and office 95, nothing has changed in windows to improve the life of users. USB is the only thing I can think of that has benefitted me, and we all know how unstable windows' USB handling is, especially when hotplugging.\n\nKDE and Linux have now reached a point where they are fully usable on the desktop and are free to innovate further, while windows effectively sits where it is. I think it would be a complete travesty to simply mimic Longhorn, when there are more worthwhile things out there to do. ReiserFS 4, ACLs, tabbed browsing, cards, Umbrello and many others are examples of opensource projects really forging ahead and giving benefits to their users. That is just a taster of a very long list of applications and projects. This is where open source research is at the moment.\n\nFinal note; one of the problems we really do have is the assumption that because Apple and MS do something, they've put money and research behind it and it is the right thing to do. On usability, the suggestion that Apple always do what is best for usability is alarming, but it keeps on coming up. Be wary of such assumptions..."
    author: "Dawnrider"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-27
    body: "I was not referring to a 3D GUI experience here ;) Likely because I am not too good at constructing my thought in English (am dutch using a spell checker) and I am better at coding anyway. I was referring to using the hardware for animated windows themselves, not an interface with 3 axis's. Eye candy and pure usability benefits might breed each other. \n \n_I_ do not expect much exciting new features from Longhorn actually, however the large drones of window users are easily mind bended again and again. We look at it from a much different perspective. Their research capabilities do help them in their favor and I do expect to see some good features from a usability point of view - I could be wrong off coarse! \n \nAll I wanted to get at is that having a window as a surface it becomes (new English word I guess) animationable. One can have GUI specialists play with that and come up with innovative features you and I might not expect. That procedure is less likely to happen in the open source environment unless someone develops it in the first place - I think it shows an obstacle for research opportunity, altough my example may be a bad one. I think it applies for several fields in open source. \n \nYour references to all those projects, I was aware of those yes and they are exactly pointing to great open source things happening because of the open source model. What I am getting at is supportive projects for open source projects which I believe can go hand in hand and solve the problem of research I tried bring up. \n \nI'm better at being practical than ranting, so I am going back to work now ;)\n\nAC"
    author: "AC"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-27
    body: ">> All I wanted to get at is that having a window as a surface it becomes (new English word I guess) animationable.\n\n'animatable'.. ;)\n\nAnd yes, it would definitely make up for some very interesting effects when having the ability to apply xforms to windows or whatever.\n\nI don't think XFree86 will ever be able to do that though.. (not even using cairo)\nIt's too old for that.\n"
    author: "whatever"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-29
    body: "So what if it becomes even more clear that X11 cannot handle tomorrow's desktops and there will be real need for OpenGL accellerated windowing system, what then?\n\nSure you might argue that there is no need right now, and sure that is partially true (well atleast I would love to be able to show off Linux desktop to my Apple and my friends using Apple) but should the Qt4/KDE4 to be designed for the future or for today's hardware and software requirements? I think the only right way for that is to plan to use pure OpenGL as everyone else (like Apple with their Quartz Extreme is doing right now and Longhorn is also going to use it) are and Linux community is going to need to get it done!\n\nSo is there any already evaluated windowing systems that might fit the bill? Sure there are some but they are on pretty alpha stage but should there be some chosen system that would surely get more attention in the future?\n"
    author: "Nobody"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-29
    body: ">>So what if it becomes even more clear that X11 cannot handle tomorrow's desktops and there will be real need for OpenGL accellerated windowing system, what then?<<\n\nWhen did it become clear? X11 does not prevent you at all from using a MacOS X-like, OpenGL based rendering system, at least not at the application level. The apps still render their windows and get their events like they did in the last 20 years. They may use classic X11 functions, new X11 extensions like Cairo graphics or OpenGL over GLX, but they still use rectangular windows as primitives.\nThe window manager may need to be aware of such a composition system (obviously, current X11 can not transform windows), but not the apps. And even that could be done using a X11 extension, no need to replace X11.\n\nI will never understand why people are so eager to throw away the X11 system, even though they have yet to show a feature that X11 could not do - the only problem are implementations, not the protocol.\n\n"
    author: "Tim Jansen"
  - subject: "Re: Qt on DirectFB"
    date: 2003-08-30
    body: ">>>>I will never understand why people are so eager to throw away the X11 system, even though they have yet to show a feature that X11 could not do - the only problem are implementations, not the protocol\n\nI think this is related to the fact that not everybody understands how X is designed. Most people observe X is almost always causing troubles and causing literal headaches when setting up (refresh rate). Improving its setup procedure might remove most of the bitter experiences people now face with X."
    author: "AC"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-01
    body: "Of course X does not prevent you from doing whatever you want. When you get down to it X is basically a blown up graphics driver. You can pile everything on top of that, you can do all desired effects, etc. Nobody is arguing that point.\n\nBut why would you want to? Why are you using languages like C++, Smalltalk or whatnot instead of Assembler? Because you don't want to be bothered with all the bit-pushing that's going on behind the scenes. The same is true with X windows: Who does actually develop for it nowadays? Nobody! All the developers are building their work on toolkits that could be run on any other graphics driver as well (probably better than on X, too:-) or on APIs better suited for their purposes like OpenGL, libArt or some SVG library.\n\nUsers don't care about the low level stuff, they still stick with X since all their applications happen to run on that 'driver'. If they switch to something else they are bound to loose some of their favorite applications.\n\nDon't get me wrong: I do use X and I even like it a lot. It is a revolutionary product! But it is time to start to think about what could happen in the past-X(11R6) age. That could be something new like fresco (www.fresco.org) or picogui (www.picogui.org), it will most likely be X as we know it with some more extensions clobbered on or it could be DirectFB/Qt (I'd hate having that, networktransparency rocks) or something completely different!\n\nAnyway, what made me post this is that I hate those \"This is great and needs no fixing\" attitude. Nothing is perfect, not even X. Why are you so rough to people trying to think out of the box? They could very well come up with some good ideas... which might result in some X successor, however that may look like.\n\nI for one thing have a graphics card that has 10 times the clockspeed of my first PC's CPU. I payed for that power, I want it used. Of course I'd prefer something productive, but I'll settle for eye candy;-) If X can't do that - and as of today it can't - I'm looking for other projects and try to help those along."
    author: "Tobias Hunger"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-01
    body: ">>All the developers are building their work on toolkits that could be run on any other graphics driver as well (probably better than on X, too:-) or on APIs better suited for their purposes like OpenGL, libArt or some SVG library.<<\n\nToolkits are layers on top of X11, and there are good reasons for layering them. The most important ones, probably, are that a) layering increases flexibility and b) the relative simplicity of X11 makes things like NX possible. To use a analogy myself, an approach like Frisco is like improving HTTP by getting rid of TCP and writing an alternative that provides the functionality of HTTP over TCP. \n\nIf there is *functionality*, including eye candy, that can not be transported using X11 and its extensions, you could easily convince me that it is a good idea to work on a second protocol. But I don't see it a point as long as the user does not notice any difference except that the old applications don't work anymore. If you start the post-X11 era, you should have a compelling reason for this.\n\nIntegrating widgets on the server side in a way that's less flexible than DPS rather limits the capabilities of the window system than increases them. There are still, and always will be, applications that do not want your widget kit and provide their own. Examples are emulators like Wine, and apps that want their own widgets for eye candy reasons (games are the no 1 example, but just look how many Windows apps do not use the Windows widgets).\n"
    author: "Tim Jansen"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: ">> Toolkits are layers on top of X11, and there are good reasons for layering them. <<\n\nYou are right. My point is that with every application depending on the top layer only, what stops you from even considering switching the bottom layer? That's exactly the increased flexibility you gain by layering! Why are you so much against someone even bringing up the topic?\n\n>> To use a analogy myself, an approach like Frisco is like improving HTTP by getting rid of TCP and writing an alternative that provides the functionality of HTTP over TCP. <<\n\nTo stay in the layer-paradigm ou have brought up: Fresco is a set of interfaces defined on top of CORBA which in turn is a protocol defined on top of (among others) TCP/IP. So your analogy should be more like \"improving HTTP by replacing it with something more powerful on top of SOAP on top of TCP.\" with SOAP and TCP being widely adopted standards.\n\n>> If there is *functionality*, including eye candy, that can not be transported using X11 and its extensions, you could easily convince me [...] <<\n\nDevice independence, resolution independence, vector-based rendering, 3D capable desktop, and most important of all a consistent look and feel of applications, even while keeping the configurability we all know and love. Just to name a few...\n\nOf course nobody is stopping you from adding all that to X via some extension. We decided not to work on an X extension for a simple reason: All X could do for us is access to the graphics card. We can get that in a more flexible way by using SDL/GGI/DirectFB/GLUT, some of them being able to run inside X or outside. The same is true for input devices. Network/language transparency? We get that for free from using CORBA, a technology necessary to have all the objects communicate (no, we will not replace CORBA, we need all it offers; no, something you need is not bloat; yes, CORBA is slower than sockets but because our architecture is way different from what X does that impacts performance far less then you might think; no, we cannot remove CORBA and replace it with something else). We need to do our own focus- and fonthandling, etc., so why on earth should we limit ourself to being an X extension? Fresco is less then 200K lines of code... why dock that onto a 2M lines of code XFree-monster for that little it offers to us? Right now that would hinder more then it would help.\n\nAnyway: The important issue is that someone is trying out new things in free software and that the code is there. Take the code and turn it into an X extension if you think that's better than what we do. It doesen't matter to me what it is actually called and where it ends up getting used. What matters to me is that someone is experimenting.\n\n>> [...] that it is a good idea to work on a second protocol. <<\n\nWe aren't. The protocol is defined by the OMG as part of CORBA and is called IOP. We just provide a set of interfaces on top of this protocol. That's not much different from what KDE and Trolltech do by providing their libraries using the X protocol.\n\n>> If you start the post-X11 era, you should have a compelling reason for this. <<\n\nNo, you need a compelling reason not to want to improve on what is already known. The spirit of invention driving human development and all;-)\n\nI am further not starting the post-X era, neither is anyone else in Fresco. We are just trying out some (not that new but still untried) ideas. But then who could claim more for those guys at MIT originally writing X? ;-)\n\n>> Integrating widgets on the server side in a way that's less flexible than DPS rather limits the capabilities of the window system than increases them. <<\n\nCorrect. That's why we are not doing it.\n\n>> There are still, and always will be, applications that do not want your widget kit and provide their own. <<\n\n.. which of course they can in Fresco. We just try to discourage that as much as possible, just like Apple, Microsoft and probably KDE, too. And since our concept of a widget, being just some collection of graphics (like everything else on the screen) is way more flexible than the \"each widget is a object with hardcoded attributes\" used in traditional X toolkits, we are convinced that there is less need for custom widgets.\n\nIf you insist on using your own widgets you can, building them up from the same basic building blocks as the 'default' widgets. That is some work of course (not more then inheriting the basic functionality and extending it) and maybe someone will write up a \"GUI painter\" for that purpose someday... with the GUI consisting of the same elements as a graphics view that will be really easy. Well, there goes the consistency... ;-)\n\n>> Examples are emulators like Wine, and apps that want their own widgets for eye candy reasons <<\n\nThe eye-candy reasons does not hold in Fresco:-) If a user wants eye candz he can just replace the admittedly ugly MotifKit with a (to be written) \"Fully3DTexturizedMeshesWidgetKit\" on his system and he will have eye candy widgets for all his applications. The look and feel can be freely changed by the *user* in Fresco (in X the developer selects the look and feel and might offer some themeability so that the user won't notice this too much;-), all Fresco does is making sure that all applications (that do use the normal interfaces to produce their widgets) look and feel the same.\n\n>> (games are the no 1 example, but just look how many Windows apps do not use the Windows widgets) <<\n\nI do not consider games a no 1 example: They tend to run fullscreen and need all the power the graphics card can deliver, so they usually go around the GUI environment as much as possible. So I don't see much need to discuss games in this context.\n\nSome words about compatibility: You can extend X and you will end up with something else (let's call it Y for now). Anything written for Y won't run on X: backward compatibility is broken. Of course any X application will work on Y, too. That's great and a absolute must have if you ever want to think about replacing X.\n\nBut what about writting something (let's call it Fresco here) and implementing a X server in it? Any Fresco application won't work on X, but you could still run X apps on top of Fresco in the embedded X-server. So how does this differ from an extended X from a users point of view? That this is a feastable option was proven by Apple: It is what they have done with there MacOSX GUI."
    author: "Tobias Hunger"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: "> My point is that with every application depending on the top\n> layer only, what stops you from even considering switching the bottom\n> layer?\n\nThere are many top layers that people do not want to miss. Toolkits like Qt, GTK and XUL are very different and the holy grail of widget theming is still to implement a widget style that runs at least on Qt and GTK. So do you want to support only one toolkit, modify at least one of their APIs enough that you can use a single widget implementation or try to write one that is so flexible that it works with both?\n\nAnd, of course, beside regular toolkits there are other challenges like SDL, apps that use XLib directly for various reasons and statically linked binaries, especially for commercial apps.\n\n> Why are you so much against someone even bringing up the topic?\n\nSorry, I just heard way too often that dropping X11 solves all problems. And I never saw any proposal (or even code) that solved more problems than it created.\n\n> All X could do for us is access to the graphics card.\n\nIt would also offer a communication middleware that is very well known and analyzed, for example regarding performance issues. I don't think that such a amount of research has gone into any CORBA-based rendering system (or maybe even  CORBA for applications with real-time characteristics).\n\n> why dock that onto a 2M lines of code XFree-monster for that little it offers \n> to us?\n\nWhile I can understand detesting XFree's (or X.org's) prehistoric code base, it cumulates a lot of experience and code for using graphics hardware on hundreds of different architectures, and is the only widely used X11 implementation. If you want to implement any X11 emulation you would have an hard time to emulate X.org's exact behaviour. \nProprietary drivers for NVidia cards are also a showstopper for everything that does not use XFree's driver model (there is no chance to get people use an OpenGL-based system without NVidia support). \n\n"
    author: "Tim Jansen"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: "You know what's the whole issue with XFree86?\n\nIT IS TOO OLD AND NOT FLEXIBLE for todays standards (!)\n\nHaving said that, see the following comparison:\n\n\n* setting resolution (@ runtime)\n\n  DFB: IDirectFB::SetVideoMode\n  XFree86: Use XRandr, depth cannot be set\n\n* render an alpha blended rectangle\n\n  DFB: IDirectFBSurface::SetDrawingFlags, IDirectFBSurface::FillRectangle\n  XFree86: Use XRender (or Cairo, which usually uses XRender implicitly)\n\n* getting an OpenGL surface for doing 3D graphics\n\n  DFB: IDirectFBSurface::GetGL\n  XFree86: Use a mix of GLX and X\n\n* double or triple buffering of window surfaces\n\n  DFB: Automatic\n  XFree86: Backing store (b0rked, so not usable)\n\n* documentation\n\n  DFB: Nice and easy\n  XFree86: Boring and hard to read/understand\n\n* system integration\n\n  DFB: Boot kernel in graphical mode -> jump right into the desktop (nice!)\n  XFree86: Boot, mode switch, mode switch, mode switch, ... (ugly & unprofessional looking)\n\n\nIt's just the small things, but as you can see; one very important thing is that DirectFB can do things using a nice API without using yet another library/extension, thanks to the non-dependency on X11.\n\nAnd not only that, it's also just too easy to create a slow GUI (that is, without Gtk or Qt, though even both these toolkits are still making some mistakes -> see perf docs from KP and that guy @ HP) using XFree86, which is partly the fault of X11 and partly the fault of XFree86 not being improved anymore (for the last few years (!)).\n\nAnother issue: the latest standards for eye candy (which is often very usable as well, not just for nice looks (ie. window drop shadows)) are very hard to do using XFree86 (nearly impossible without hacking away).\nUsing DirectFB it's much easier.. eg. I don't think it would be hard to create genie-like window effects using DFB.\n\nOh, and writing a driver for DirectFB is also a far more pleasing experience.\n\nIt's time for something fresh..."
    author: "wup"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: "write back when I can use my NV Gf4 at reasonable speeds on DFB"
    author: "anon"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: ">IT IS TOO OLD AND NOT FLEXIBLE for todays standards.\n\nThen the easiest  solution is to improve it.\n"
    author: "Tim Jansen"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: ">> Then the easiest solution is to improve it.\n\nProbably not, as no person or even group of coders has/have succeeded in doing that the last few years..\n"
    author: "wup"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: "AFAIK no one tried. And by creating something from scratch things won't be easier, because you probably want more features than the 2 millions lines of code in XFree provide, not less."
    author: "Tim Jansen"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-09
    body: "It is easier to create something from scratch!\n\n* You are not stuck to an stone-age (IT industry-wise) protocol\n\n* You do not need to wade through heaps of code added for obsolete architectures\n\n* You do not have to work with a display model that matched the hardware that was available over 10 years ago and needs all kinds of clutches to get adapted to modern 3D accelerator boards and such\n\n* You get a clean set of interfaces instead of the extensions-mess we are seeing in X today with obsolete extensions being dragged through the releases and dozends of interfaces for basically the same functionality.\n\nIf throwing out 2 Mio. lines of working code scares you: Don't forget that the 2 Mio. lines of XFree code does include lots of rarely used clients, too which could go with no harm done to get replaced by KDE/Gnome apps providing the same functionality. Then throw out all those obsoleted extensions (only those marked as obsolete and those that never really worked anyway) and you get down to a surprisingly small SLOC count!\n\nThe only really interessting thing in XFree are the graphics drivers... and unfortunately those seem to be really hard to rip out due to heavy interdependencies with the rest of the code."
    author: "Tobias Hunger"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-10
    body: "> You are not stuck to an stone-age (IT industry-wise) protocol\n\nThen I would suggest you to stop using Internet, which is using\na protocol that predates X11 by a decade. Oh, while you are at it,\nyou may also consider dropping BSD, Linux and other Unix-like\noperating systems which are essentially technology of the\nlate 1960's :-)\n"
    author: "Bernd Gehrmann"
  - subject: "Re: Qt on DirectFB"
    date: 2003-10-08
    body: "Networks got faster, sure, but the basic assumption of \"shoving data in at one end and getting it out at the other\" still holds.\n\nGraphics hardware on the other hand went from \"set a couple of its in some chunk of memory\" to \"Render a Mesh with a lamp setup at this position\". With such a change of abstraction level something new might be in order, don't you think?"
    author: "Tobias Hunger"
  - subject: "Re: Qt on DirectFB"
    date: 2004-01-15
    body: "\"You know what's the whole issue with XFree86?\n\nIT IS TOO OLD AND NOT FLEXIBLE for todays standards (!)\"\n\n\nThis is EXACTLY the argument people use against Unix based OSes, and frankly it's a load of bullshit. \n\nMac OS X has lineage that can be traced directly back to the original AT&T Unix,  historic BSD, and CMU Mach, yet few people would argue that it is not flexible enough for today's standards.\n\nMS Windows is older than X11 (though some of X11's underpinning concepts predate Windows) yet few people would argue that Windows is not flexible enough for today's standards.\n\nThe actual limitations inherent in the X11 protocol are fewer than in most contemporary graphics systems since it is conceptually quite abstracted from the hardware, so in fact it's less likely that X11 is going to be fundamentally unsuitable for any type of graphics rendering than, say... Windows. \n\nMac OS X's Quartz window system uses many of the same ideas for network transparency as well as a fundamentally inefficient PDF based language to represent graphics, yet seems to do quite well for itself in both the performance and flexibility stakes, as evidenced by it's use of eye-candy so rich you want to floss your eyes after watching it for half an hour. \n\nWhen pointing your finger, you'd do well to remember that there are four more pointing somewhere else.\n"
    author: "Steve Keate"
  - subject: "Re: Qt on DirectFB"
    date: 2006-08-17
    body: "I totally agree with you. i just hope some day gnome and kde both will have a choice of running on either of X or DirectFB. \n\nAnd with intel open sourcing its drivers i hope directfb gets the much needed hardware support.\n"
    author: "novice_root"
  - subject: "Re: Qt on DirectFB"
    date: 2004-01-15
    body: "\"On usability, the suggestion that Apple always do what is best for usability is alarming, but it keeps on coming up. Be wary of such assumptions...\"\n\nI find it ironic that you complain about people claiming that people are constantly raising Apple as a bastion of usability. One only need look at GNOME and KDE's taskbar clones to see obvious evidence that people are in fact looking to Microsoft Windows for their cues for interface design when it comes time to implementation. \n\nPeople are not suggesting that Apple is a holy grail of usability, but that people should look at other environments rather than simply re-implementing Microsoft's interface because they lack imagination, or in the misguided belief that this makes it easier for people who have already used Windows. \n\nMy experience, and something that I recall reading from Sun's usability studies, is that when you clone the appearance of something, they expect that you would clone the behaviour as well. When this is not the case, the experience is very jarring and irritating. \n\nImagine getting into a car, and finding that the pedals are used to steer, and the steering wheel does not move side to side, but back and forth to control acceleration, and you have some idea of why this is. \n\nI would prefer to see a user interface that isn't a clone of Classic MacOS, OS X or Windows, and is in fact a rethink of all that we have come to hold dear in computing, if the end result is better than what we have now. \n\nI hold up Slicker and Enlightenment as examples of this kind of thing. You don't always win, but you also don't learn without making mistakes, and you don't innovate by copying. \n\nWhich I think brings us to the point you were trying to make. :)"
    author: "Steve Keate"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-01
    body: "Allot = a lot. Get it right, you product-of-society fucktard."
    author: "Blah"
  - subject: "Re: Qt on DirectFB"
    date: 2003-09-02
    body: "hahahahahahahaha\nplease rot :)"
    author: "Alex"
  - subject: "splitting up of the library..."
    date: 2003-08-26
    body: "is very nice. Would it be possible to put the core qt lib under LGPL ?\nThis could make it first choice for portable non-gui tools, no matter which platform, no matter which purpose. And maybe it would make commercial developers more hungry for the other Qt libs :-)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: splitting up of the library..."
    date: 2003-08-26
    body: "Mod parent up! :)"
    author: "ac"
  - subject: "Usability"
    date: 2003-08-27
    body: "Okay, just looked at this website: http://usability.kde.org/ What happened here? Why are all teh borders black?!"
    author: "Alex"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "Today it's software patent decision day, that's why.\n\n\n(ie. many websites are set to black today because software patents can mean death to OSS - see http://www.kde.org )\n"
    author: "whatever"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "Death to OSS, because Europe is the center of the world, right?\n\nGet over yourself."
    author: "Neil Stevens"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "Nah, but old Europe could stay sane without this US patent stuff.\n\nGo troll elsewhere."
    author: "bugix"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "Just what is your malfunction? You very often sound annoying, hostile, selfish and god knows what.\n\nWhat's wrong with protesting against software patents? That is IMO a good thing to protest again, and I fail to see why you need to whine about it. But I guess that's the thing you do best: whine."
    author: "Janne"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "So go protest. But leave www.kde.org out of it. Actually, I am in favor of using our hotspot, a dot story, etc etc. But the redirect is just plain European^Wsilly.\n"
    author: "Datschge"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "Damn computer sharing at N7Y. That was me, not Datschge."
    author: "Rob Kaper"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "Yes, I doubt Datschge would write such things."
    author: "CE"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "I was also surprised...\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Usability"
    date: 2003-08-27
    body: "What makes you go offensive like that?\n\nAnyway, my answer; no, because America has already shown more than once what software patents can do.\n\nAnd we Europeans do NOT want that cruft in Europe, just to let big companies get even bigger by killing out small companies and inviduals.\n\n(software) patents are useless and not used anymore for what they were invented.\n"
    author: "whatever"
  - subject: "I signed"
    date: 2003-08-27
    body: "his sounds like a stupid law that willl limit innovation."
    author: "Mario"
  - subject: "Re: Usability"
    date: 2003-08-28
    body: "Latest example of damage from the braindamaged patenting system in the US,\nwhy we do not want this shit in Europe and why this is important for KDE.\nKonqueror probably infringe on this too.\n\nhttp://www.w3.org/2003/08/patent\n\n\n"
    author: "Morty"
  - subject: "Needed Features! ?? :-\\"
    date: 2003-08-27
    body: "Qt currently needs two features:\n\n1.  A font system which works.\n\n2.  A PostScript driver which works.\n\n--\nJRT  "
    author: "James Richard Tyrer"
  - subject: "Re: Needed Features! ?? :-\\"
    date: 2003-08-27
    body: "And a WYSIWYG-enabled painter"
    author: "me"
  - subject: "I think they would re-design more things..."
    date: 2003-08-27
    body: "I think that in this new major version Trolltech should refactor more things. \n\n* Better STL integration (why not simply drop Qstring!) and follow modern c++ concepts (like namespace support -why should we keep decorating class names!- )\n* Use C++ prepocessor capabilities. Why keep using a signal/slot system based on moc when you can do that with C++ templates in a more efficient way? Most of required things (serializing, type checking, ...) could be done in compile time (not with dinamic metaobjects like java) so it would be faster and smaller (yes, I know that not so frecuently used things like object inspection would require a dynamic version but it can be also done with c++ templates)\n* Improve the GUI capabilities. The 3 generation desktops support avanced vector primitive rendering. For example, Mac OS X has a PDF-like rendering API very powerful to the developer; it can be HW accelerated and would help to reduce the client-server volume of data interchanged. The traditional fixed point QPainter/Qfont api is becoming very limited for new SW projects in KDE like Kword or KSVG (and new solutions are being proposed like Kpainter). And 3D graphics are the future! Maybe it's the moment to introduce some high level code (not only QGLWidget) -basic scene managers, ...- to test this market ;-)\n* Better network and distributed processing orientation. Haven't Trolltech introduced a scripting system? They also have a xml language describing GUIs and a library supporting dynamic created interfaces -libqtui? used in qtdesigner-. Why don't encourage developers to create fast cross-platform distributed script applications? It will be similar to mozilla XUL technology, more powerful than DHTML (fast prototypes design, forget .NET!) I think MS is looking for something similar in Longhorn, and the most similar technology in KDE is Kommander (from the Quanta team). I think there is no need for big changes, everything is there (maybe a CSS Qt styles definition to make the whole thing more Web standars compliant)\n* And other things I don't remember now (this is a enough long post :-) )\n \n\nI think these are things that will have to be done someday. My reasons for doing it soon:\n* Compatibility is not as critical as perfomance for many users. People who use Qt are looking for a modern C++ library and if these features are present I think version 4 will attract more developers. \n* C++ compilers are getting closer to the standard\n\nBy the way. Qt really rocks! And more modularization and optimization are good news for the new version at least.\n\nJust my opinion :-)"
    author: "John the anonymous"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-27
    body: ">>why not simply drop Qstring<<\n\nNo, please not the STL string which is completely inferior in practice. It just lacks dozens of features. \n\n>>yes, I know that not so frecuently used things like object inspection would require a dynamic version<<\n\nAnd exactly this is the killer feature needed for scripting/dynamic languages.\n\n>>The 3 generation desktops support avanced vector primitive rendering.<<\n\nThat would be, indeed, nice, but maybe a little bit early. MacOS got Qwartz, Windows GDI+, but for X11 there is no finished implementation. Cairo graphics may solve that in the future though.\n\n"
    author: "Tim Jansen"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-27
    body: "What in particular do you find QString has that an STL string does not? \n\nWhy would it not be better to add in the additional functions to string as described by Herb Sutter in www.gotw.ca/gotw\n\nCan you give me an example of when a dynamic object inspection would be useful and unprogrammable using current C++ methods? I'm not saying there aren't any I'd just like to see the class of problems that require it.\nSurely if it's not needed for most things then it should be done statically at compile time.\n\nRegards\n\nNick"
    author: "Nick"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-27
    body: "Sure, take a look JSObjectProxy in KJSEmbed. There's also stuff like KSpy and plenty of other examples too I guess.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-27
    body: "1. Something like QString.arg() is extremely important, especially for i18n purposes\n2. It lacks easy conversion from/to numbers\n3. It lacks easy conversion to encodings (utf8 etc)\n4. It lacks upper/lower case conversion\n5. Instead of functions that people use in the real world it has stuff like find_last_not_of(). Huh?\n6. It feels clumsy and not easy enough to use for a class that should be the most important class in any base library. Sure, I may find all the conversion stuff (numbers, encoding, case) somewhere else, but as it is the most frequently used class in most app it should make code short, sweet and readable, instead of designing a 'pure' class with a clean design but bad usability in the real world. It looks like a class designed by academics, not people who spent >50h per week writing applications\n\n\n\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-27
    body: "Imho QString is inferior to the std::basic_string template.\nYou can use the template with whatever charset you want. It doesn't make that much functions members, which shouldn't be (although there are some). Most of the stuff can be handled in non-member functions(e.g. your arg function). Not supporting upper-/lowercase conversion is because you just can't. How do you want to translate e.g. a \"scharfes s\" in german, should it be \"SZ\" or \"SS\" and this gets worse with supporting complete unicode.\n...\n\nRischwa\n"
    author: "Rischwa"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-27
    body: "But how often do you want that? You are talking about stuff that is almost never needed. I complain about stuff that's used all the time. When I implement a function, and I want it to be case-insensitive, I don't care which specific solution is taken for which platform. I just want it to work. \nIn some rare cases this kind of abstraction fails, but in the overwhelming majority of cases it just helps me to save my most precious resource, time. The STL design, like Java, usually aims to be correct in each and every case, while forgetting that a simple solution that works *almost* always is at least as important as a complicated, perfect solution. This is the beauty of Qt: it is designed from a practical programmer's point of view.\n"
    author: "Tim Jansen"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-27
    body: "> Not supporting upper-/lowercase conversion is because you just can't.\n\nYes you can, the unicode specs specify what character offsets in a given unicode table should be mapped to an opposite case \"partner\". Not all characters can, of course, but this is a fairly reliable method of changing case, and is what QString uses."
    author: "anon"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-08-28
    body: "No you can't, in many languages form of a glyph depends on context as well.\nAnyway, I would give my vote for removing QString and creating QStringTools dealing with string/wstring/text files/unicode/conversions/locales/i18n, instead.\nlike this:\n\nwstring QStringTools::stringToWide(int code_page_id, char* source);"
    author: "Mirza"
  - subject: "Re: I think they would re-design more things..."
    date: 2003-09-08
    body: "There is a note in Qt's doc about why signals/slots are not made with templates: http://doc.trolltech.com/3.1/templates.html\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "So when those zillion packages will be moved?"
    date: 2003-08-27
    body: "Just wondering are all those packages in kdenonbeta, kdeextragear-1/2 and kdekiosk on the CVS going to survive during the KDE-3.2 -> KDE-4.0 transfer or are those cleaned up, and if so is there a page to show what programs do get included to the next version of KDE and what will stay or get eliminated?\n\nAnd yes, the idea of rendering all GUI elements with OpenGL gets my vote as long as xlib, gtk+, SDL, tcl/tk, and other desktop libraries are supported via compability layers... ;-)\n"
    author: "nobody"
  - subject: "Re: So when those zillion packages will be moved?"
    date: 2003-08-28
    body: "If they're useful to someone and their is no better competitor they will survive. Just like most apps did for KDE2->KDE3."
    author: "Anonymous"
  - subject: "Re: So when those zillion packages will be moved?"
    date: 2003-08-28
    body: "Ok, but could there be indication on some info-page of what would move up to the real distribution so people playing around building distros could wipe out unneccesary overlapping software?\n"
    author: "Nobody"
  - subject: "Re: So when those zillion packages will be moved?"
    date: 2003-08-28
    body: "The thing is you'll usually see the result after the progress is finished, not beforehand. =P\nKdeextragear-1/2 is by definition only loosely connected to KDE anway (they won't be included in official KDE packages but are released on their own) so it's up to the authors what they'll do with their work there. In general you can expect long time idling and broken packages to be moved to kdeblackhole, but that doesn't occure often either.\n\nWhy are you so eager to see some \"wiping out\"? The only \"unneccesary overlapping\" projects I see are in kdeextragear, but again: that is one of its purposes (see http://extragear.kde.org/home/about.php )."
    author: "Datschge"
  - subject: "Re: So when those zillion packages will be moved?"
    date: 2003-08-29
    body: "Ok, my bad... :-)\n\nJust it seems a bit ridiccilous that there are so many multiple packages for a single function on virtually any Linux distribution (and yes, I'm part of one packager team).\n\nI just hoped that no later than that Qt4/KDE4 there would be some sort on spring cleaning happening as some of the stuff is there just to get bugzilla happy...\n\nBut the only place to see what packages are really in the development cycle can be only seen on the CVS/changelogs/kde-cvs-digest/kde-traffic and there could not be some sort of page indicating what parts should be (by administrators plans) going up (from kdenonbeta->relese and kdeextragear(s)->kdenonbeta)?\n"
    author: "Nobody"
  - subject: "Re: So when those zillion packages will be moved?"
    date: 2003-08-30
    body: "kdenonbeta and kdeextragear are not part of the KDE release cycle.\n\nkdenonbeta is experimental stuff that probably doesn't even work with the current release, let alone a future one, and kdeextragear is for applications that want to be available for packaging from the kde cvs, but are not actually part of a kde version."
    author: "Stuart Herring"
  - subject: "RFC: Better Mouse Wheel Support"
    date: 2003-08-28
    body: "Scrolling a fixed amount of lines is not appripriate\nin most cases. You either have to set it to a high\nvalue to get good results with large windows, but then\nyou scroll more than a whole page in small windows.\nOtherwise you need to use a low value to get good results\nwith small windows, but then your hand aches when you have\nto scroll in large windows.\n\nScrolling a whole page isn't good either, because with large\nwindows you need to move your eyes from the bottom of the\nscreen to the top (this is straining).\nAn other reason why it isn't good is that you won't notice\nwhen you accidentially scrolled two pages instead of one,\nbecause the text you read is not shown any more.\n\nMy suggestion is to add configuration option that scrolling\ndefaults to scroll half a page. I created a patch and sent it\nto kde-devel some time ago (for anyone who is interested).\nThe patch is quite simple. If you set the number of lines to\nscroll to 0 (zero) than all qt GUI elements will scroll half\na page.\n\nIMHO something in this regard should be done in QT4.\nDoes somebody from Trolltech please respond to my suggestions?\nThat would be great. :-)\n\n\n"
    author: "Gerold J. Wucherpfennig"
  - subject: "Re: RFC: Better Mouse Wheel Support"
    date: 2003-08-28
    body: "KDE already has a mouse configuration dialog for all that in the Peripherals section of the Control Center.\n\nWhat I would rather see is a smoothscroll like in Firebirds after you get an extension."
    author: "Alex"
  - subject: "Re: RFC: Better Mouse Wheel Support"
    date: 2003-08-29
    body: "> Scrolling a fixed amount of lines is not appripriate in most cases.\n\nI agree.\nBut I would like more flexibility in a different direction:\nMouse wheel support seems to be hard coded with support of 4th and 5th mouse button.\nThere a different wishes how to use these buttons:\nhttp://bugs.kde.org/show_bug.cgi?id=48062\nSo many configuration options are needed to support these different wishes.\n\nCertainly something like this should be done in QT4. ;)\nIs anybody from Trolltech reading this? =o"
    author: "Joe Hurke"
  - subject: "QT 3.2.1 RELEASED"
    date: 2003-08-28
    body: "http://www.trolltech.com/developer/changes/changes-3.2.1.html"
    author: "Somebody"
  - subject: "Have these ISSUES been adressed ;)"
    date: 2003-08-30
    body: "http://www.suse.de/~bastian/Export/linking.txt\n\nAlso, i hope Qt will be easier to sue and use more standard C++ like templates and stop relying so much on the preprocessor."
    author: "Right..."
  - subject: "Re: Have these ISSUES been adressed ;)"
    date: 2003-09-01
    body: "Templates makes your binary larger then it needs to be and thus decreases performance."
    author: "Bruno"
  - subject: "When on SuSE / What about articles"
    date: 2003-09-01
    body: "The key distro for KDE based developments in Europe (I'm in the U.K.) is, as far as I know, SuSE Linux.\n\nSo, for those of us who aren't fully aware of CVS and aren't happy with installing KDE and KDevelop by hand, when will these developments see the light of days in the latest SuSE Linux?\n\nAlso, I'd like to see some articles written about programming in Qt 4. I'm no stranger to writing technical articles myself (see http://www.accu.org/resources/public/terse/overload/UserDefinedTypesQualitiesPrinciplesArchetypes.rtf\nas an example, albeit the best ideas were given by the Overload editorial readers). \n\nSo I'd volunteer to write some articles about Qt 4 development, given appropriate hand holding (I've got a couple of years commercial C++ development under my belt and 6 years commercial C development).\n\nIan Bruntlett\nACCU Overload Editorial Reader"
    author: "Ian Bruntlett"
  - subject: "Re: When on SuSE / What about articles"
    date: 2003-09-01
    body: "> So, for those of us who aren't fully aware of CVS and aren't happy with installing KDE and KDevelop by hand, when will these developments see the light of days in the latest SuSE Linux?\n\nProbably in two weeks when KDE 3.2 alpha1 comes out. "
    author: "anon"
  - subject: "qt-mesa-free-4.0.0.tar.bz2"
    date: 2003-09-02
    body: "Just hoping for more 3D-oriented system so there would not be necessary differences between OS and game modes (for future games on Linux).\n"
    author: "n/a"
  - subject: "Support for big desktop with different resolutions"
    date: 2003-09-06
    body: "I think, that for home users, one of the main disadvantages is lack of support for big desktop on two monitors (like FlatPanel + TVset) with two different resolutions. I have this problem with 17\" FlatPanel (1280 x 1024) and TV (800 x 600).\nThe panel lays at the bottom of destop (1280+800 x 1024), so on the TV the panel and part of image is unreachable. The requesters are centered on the edge of screens (in the middle of desktop width). It is not possible to maximize window to only one screen. There should be one primary (for requesters and panel) and one secondary display (like ine windows XP and 2k).\nI am not sure if it is right place to talk about it, because I don't know if the Qt or KDE is responsible for desktop behavior. But the problem is important.\n\nBednar"
    author: "bednar"
  - subject: "Senior Principal Engineer"
    date: 2003-09-17
    body: "I believe boost (boost.org) signals template library would be a good basis for replacing the qt signal/slot implementation."
    author: "Neal D. Becker"
  - subject: "Minor niggle in window handling"
    date: 2004-12-15
    body: "I have one gripe with KDE 3.X or Qt 3.X that might finally get fixed:\n\nWhen I grab a window edge with the mouse pointer _while_ moving the mouse, I'll often miss. It looks like the mouse position of the click itself is forgotten, and instead the current position is used. On slow computers or under heavy load that might be quite a few pixels wrong.\n\nWhy care about such a simple thing? Because it's a usability issue. The percieved reliability is affected by this, it feels wrong that you have to be careful about slowing down to grab something when you otherwise know what you're doing. The two largest (commercial) user interfaces both got this one right.\n\nIf I can post this to a Bugzilla somewhere, please point me in the right direction. "
    author: "Henrik R Clausen"
---
On Saturday evening Matthias Ettrich, director of <a href="http://www.trolltech.com/qt/">Qt</a> development, gave within a talk at the <a href="http://events.kde.org/info/kastle/">KDE Developers' Conference 2003</a> in Nov&eacute; Hrady -- besides a <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day2/dscf0127.jpg">presentation of the company</a>, <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day2/dscf0130.jpg">the KDE/FreeQt Foundation</a> and the past and present of the Qt development toolkit -- an outlook on Qt 4. Qt 4 is expected to be released in 2004 and promises to deliver increased performance, both at startup and runtime, more flexibility and productivity and changes to ease the learning process. Read more excerpts from the slides shown with the help of Matthias' <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day2/dscf0158.jpg">home-brewn presentation program</a>.
<!--break-->
<p>
Qt 4 mostly tries to preserve source-compatibility with a little search and replace and a COMPAT compilation switch. More porting will be required for styles and code that uses the meta object system directly.
<p>
The increased startup performance will be reached through reduced number of symbols, less read/write data and fewer static initiliazers. Fewer mallocs, faster and more optimised tool classes and reduced memory consumption will improve the runtime performance enabling Qt 4 to run on embedded devices that are slower and have less memory than today's desktop computers too.
<p>
To back up these statements Matthias gave some numbers about Qt Designer which was ported to Qt 4 with only the necessary changes to make it compile: The libqt size decreased by 5%, Designer num relocs went down by 30%, mallocs use by 51%, and memory use by 15%. The measured Designer startup time went down by 18%.
<p>
Qt 4 will not be one library, but consists of many allowing finer granulation:
<ul>
 <li>Qt Kernel lib comprising tool, io, thread classes and QObject/QKernelApplication</li>
 <li>Qt GUI lib with QApplication, widgets, painter</li>
 <li>Others libs for network, XML, SQL, openGL and other purposes</li>
</ul>
<p>
Included among the presented planned features of Qt 4 as of today are:
<ul>
 <li>Designer split up into components. This makes it possible to create a form editor plugin for KDevelop.</li>
 <li>Model/view classes for list box, tree view, icon view and table</li>
 <li>New set of classes as part of a main window/docking architecture that distinguish between dockwindows and toolbars with support for tabbed docks.</li>
 <li>Accessibility support for the Macintosh platform and for ATK.</li>
 <li>Pixmap resource system</li>
 <li>Several API cleanups like unifying different APIs and reducing number of concepts aim to simplify common usage, increase flexibility and remove embarassements.</li>
 <li>New meta object code with static read-only data block (no constructors, no cleanup handlers, no relocations, no symbols) with significantly less generated code and minimal connect() memory overhead.</li>
 <li>Better connect() syntax</li>
 <li>New cast qt_cast<T*>(const QObject*) makes string-based QObject::inherits() obsolete.</li>
 <li>A new class QPointer, which is successing QGuardedPtr, is lighter and faster.</li>
 <li> The event filters were fixed and are now called from QApplication::notify() instead from QObject::event()</li>
 <li> The threaded library will be the default with more thread-safety/reentrancy. Atomic refcounting makes QDeepCopy superfluous.</li>
 <li> In the styles section the palette and background handling has been revised: A backgroundPolicy was introduced with non-toplevel widgets inheriting their background (policy) from the parent widget. Parents can propagate their content to child-widgets. This shall essentially give (semi)transparency for widgets. And last but not least system-wide double-buffering will allow flicker free update without any hacks at all.
</ul>
<p>
Qt 4 will introduce a new consistent set of light, safe and easy to use value based container tool classes which are implicitly shared and highly optimised, both for speed and memory usage. Using them will expand to less code compared to Qt 3.x and STL which will stay available for further usage together with Qt.
<p>
QString and QByteArray were both optimised for performance and memory usage besides API improvements. An important change is that isNull() is dead and will be only available in compatibility mode. Functions like latin1() will always return a valid and never a null pointer. This change led to quite some discussion and aversions among the KDE developers but Matthias pointed out that there still will be isEmpty() and operator!().
<p>
Qt 4 shall be available within a year, with a short public Beta period next year before the final release. Among the open questions Matthias addressed to the KDE developers were if KDE will switch to Qt 4 and if so when, when KDE switches who does the initial port and where, and will KDE use the opportunity to refactor some of its architecture too? In return many KDE developers will, during the hackfast week, ask Trolltech developers for additions of classes and functionality which they find long-term missing and required by KDE.