---
title: "Quanta's Eric Laffoon Meets Wil Wheaton"
date:    2003-07-15
authors:
  - "elaffoon"
slug:    quantas-eric-laffoon-meets-wil-wheaton
comments:
  - subject: "OT: The Eric Conspiracy"
    date: 2003-07-14
    body: "Hey Eric,\n\nAre you part of The Eric Conspiracy [http://www.catb.org/~esr/ecsl/] ?\nAre you plotting to conquer the world using Quanta as a tool?\n\nThat would explain a lot ;)\n\nCheers!"
    author: "cwoelz"
  - subject: "Re: OT: The Eric Conspiracy"
    date: 2003-07-15
    body: "> Hey Eric,\nHeya!\n \n> Are you part of The Eric Conspiracy [http://www.catb.org/~esr/ecsl/] ?\n\n(Shhh!) Yes, but I have not been publicizing it,\n\n> Are you plotting to conquer the world using Quanta as a tool?\n\n(SHHH!!!) This was not supposed to get out! We have been carefully plotting this in a secret mountain hideout. It's a highly secret specific adaptation of the \"Linux world domination\" agenda. Our initiates have been sworn to secrecy... Obviously someone has talked.\n \n> That would explain a lot ;)\n \nI've said too much already. I think there's a black helicopter outside my window.\n:-O"
    author: "Eric Laffoon"
  - subject: "I've said it before, and I'll say it again:"
    date: 2003-07-14
    body: "Wil Wheaton is a great guy! And Quanta Plus kicks ass as well!"
    author: "Janne"
  - subject: "Cool :-)"
    date: 2003-07-15
    body: "\"One of the more fun moments for me in open source was the day I got an email from someone telling me that Quanta Plus had been mentioned on Wil Wheaton's website.\"\n\nI'm proud to have been that guy who mailed ;-)  What a fun story.\n"
    author: "Haakon Nilsen"
  - subject: "Re: Cool :-)"
    date: 2003-07-15
    body: "Hi Haakon!\nI thought your name looked familiar. In fact the other day I searched my email archives and your name came up. Thanks so much for that email. You have introduced me to Wil, on a personal basis so to say. I've been enjoying his book. He has a great style and a fun sense of humor. I'm sure I'll be bumping into him again from time to time. Wil geeks out with the best of them and few things are as fun as being there when he does. ;-)"
    author: "Eric Laffoon"
  - subject: "Wow! It's here!"
    date: 2003-07-15
    body: "I submitted this the other day and when it didn't run right away I thought \"Maybe the editors thought we already get enough press?\" I've always thought the Dot editors made good editorial decisions. I know the interview with Andras and I drew a lot of attention so... I thought maybe this might be fun.\n\nThanks to the editores for posting and I hope Dot readers find it entertaining. I know I had fun.\n\nEric\n\nPS Thanks to all you Quanta users who have made us so popular and given us your feedback to help us to make it better. You know who you are. ;-)\n\nPPS We're closing in on a fantastic CVS snaphot release in the next few days! Watch for it!"
    author: "Eric Laffoon"
  - subject: "File not found"
    date: 2003-07-15
    body: "Look for the pictures at the bottom??\n\nI looked for them!  I found the links, but the webserver can't find the files.\n\nEric, I think you need to look for those pictures...!\n\n-- Steve"
    author: "Steve"
  - subject: "Re: File not found"
    date: 2003-07-15
    body: "Yeah, it kinda looks bad to have broken links on a story about a web page designer tool..."
    author: "Guillaume Laurent"
  - subject: "Re: File not found"
    date: 2003-07-15
    body: "It worked before. Looks like someone is still tampering with the story."
    author: "Anonymous"
  - subject: "Re: File not found"
    date: 2003-07-15
    body: "Maybe it was someone from the World Eric Watchers Organization? (Evil Grin)"
    author: "Andr\u00e9 Somers"
  - subject: "Re: File not found"
    date: 2003-07-15
    body: "> Yeah, it kinda looks bad to have broken links on a story about a web page designer tool...\n\nOh Great! Thanks for pointing that out! I was hoping nobody would notice. ;-)\n\nThis is very strange. I was trying out the KFileReplace plugin that will be in our upcoming CVS snapshot and found some legacy markup in several files. When I read the links were broken it was as if someone had gone in and wiped them out. (Probably the folks from Paramount again.) However it looks like when I submitted the article I had modified the links so the Dot could use source or a link, (the submission form was not real clear about submitting with a link) I accidentally replaced \"images/\" with the absolute address less the \"images/\", then saved it. It was included it in that upload yesterday to clean the markup. Oops! \n\nMea culpa,\n\nEric"
    author: "Eric Laffoon"
  - subject: "Re: File not found"
    date: 2003-07-15
    body: "The pictures were there and were accessible teh day before the aricle appeared on the dot. I wonder what happened... I emailed Eric to fix it, so they will be back soon.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: File not found"
    date: 2003-07-15
    body: "The pictures are still there in the \"images/\" subdirectory."
    author: "Anonymous"
  - subject: "one"
    date: 2003-07-15
    body: "i met a german prOnstar yesterday\nnow beat that ;)"
    author: "nice"
  - subject: "Re: one"
    date: 2003-07-15
    body: "Does she (or he?) support Open Source? ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: one"
    date: 2003-07-15
    body: "she supports open minds, is that good too ;)"
    author: "nice"
  - subject: "Re: one"
    date: 2003-07-16
    body: "Oh come on now. Talk about anonymous OT...\n\n> i met a german prOnstar yesterday\n\nOkay, I'm guessing that you didn't mean \"prawnstar\" so you must have been trying for \"porn star\". Anyway, I did document that I didn't just buy the first season of Star Trek TNG to \"meet\" Wil, so it's not like I just pulled \"Debbie Does Dishes\" out of of a plain brown wrapper.\n\n> now beat that ;)\n \nAre you sure you're not the straight man on a comedy team? I can think of a lot of snappy come backs here but I'm just going to leave beating it in your capable hands. Just remember, at least with Star Trek their mission was to \"Boldly go where no man had gone before\".\n\n> > Does she (or he?) support Open Source? ;-)\n > she supports open minds, is that good too ;)\n\nHmmm, well the general concept of the medium is really not about cerebral pursuits, although open something is bound to come into play. Personally I thought Groucho Marx said it very well to a lady on his game show who had something like 19 kids. \"Lady I enjoy a good cigar but I take it out once in a while.\"\n\nPerhaps this is a good time to repeat what mom used to tell me... \"don't forget your raincoat\". In fact this might be a good time to put the clinic on speed dial and study the encyclopedia of STDs. As John Belushi once said when looking at a picture of a friend's fiance \"I got the one with the donkey\". \n\nYou lucky dog! All I got to do is meet a really fun celeb. ;-) "
    author: "Eric Laffoon"
  - subject: "Man I HATE you (not really of course)"
    date: 2003-07-15
    body: "Man you really suck. You got to meant Ensign Crusher of the USS Enterprise NCC 1701-D. Man next thing i know you'll be meeting Commander Riker (Jonathan Frakes) and Captaon Picard (Patrick Stewart(OK its been awhile since Ive watched TNG so im not sure if i spelled this right.)) By the way, im glad I had a webpage editor (Quanta+) with this distribution.  "
    author: "Forbiddanlight"
  - subject: "Re: Man I HATE you (not really of course)"
    date: 2003-07-15
    body: "Captaon Picard (Patrick Stewart...)\n\nI doubt that. He is more locutists rather than the captan. After all he is a borg these days with MS."
    author: "a.c."
  - subject: "Re: Man I HATE you (not really of course)"
    date: 2003-07-15
    body: "Locutists? Man can anyone spell stuff corructly hear? Anyway I aint been tracking stewert lately so what's MS?\n\nP.S. Its Locutus"
    author: "Forbiddanlight"
  - subject: "Re: Man I HATE you (not really of course)"
    date: 2003-07-15
    body: "> Man you really suck. You got to meant Ensign Crusher of the USS Enterprise NCC 1701-D. \n\nWell I met Wil Wheaton. Ensign Crusher never talked to me as I was not on his subspace com channel. But I happen to agree with him that TNG was better than Deep Space 9 and Voyager. I miss it even on reruns. Next time I see it I can say \"Hey! I know that guy!\"\n\nMan next thing i know you'll be meeting Commander Riker (Jonathan Frakes) and Captaon Picard (Patrick Stewart(OK its been awhile since Ive watched TNG so im not sure if i spelled this right.))\n\nThat would be interesting. Maybe if they want to start blogging and ask Wil what software he uses... Actually that's a strange thought... I could be a sort of celebrity to celebrities, and nobody else would have any idea who I was. ;-) Except of course all my geek chic friends in open source. Now I'm wondering what celeb will email me next. As long at it's not Roseanne Barr or somebody who makes me insane listenting to them. ;-)\n\n> By the way, im glad I had a webpage editor (Quanta+) with this distribution. \n\nMe too! I'm glad you enjoy it. Next week we will be posting a tutorial on how to change the title bar to say \"Quanta Plus - Endorsed by the crew of the Enterprise NCC 1701-D\" with DCOP. ;-)"
    author: "Eric Laffoon"
  - subject: "Quanta rocks. Period."
    date: 2003-07-20
    body: "Although I hated Wil as Wesly Crusher, he really is a cool guy. And he has great taste in software too! I especially liked the celebrety role-reversal. You were the celebrety's celebrety. Nice :)\n\nThanks for Quanta, Eric. It actually replaced vim as my preferred web editing tool. Coming from me, that says a lot :) Although I still use vim for remote editing, Quanta just cant be beat. Keep it coming.\n\nAnd yes, evangelize Quanta all you like, I don't mind. Although it's a bit like preachint to the choir :-)"
    author: "Coolvibe"
---
I finally got to meet <a href="http://wilwheaton.net/">Wil Wheaton</a> of <a href="http://www.startrek.com/">Star Trek TNG</a> fame. Wil is a great guy, a <a href="http://books.slashdot.org/article.pl?sid=03/05/11/0538233&mode=thread&tid=188&tid=192&tid=129">very good writer</a> and an <a href="http://interviews.slashdot.org/article.pl?sid=01/10/29/173252&mode=thread&tid=129">open source advocate</a> we can all be proud to have on our side. What did Wil have to say to me about <a href="http://quanta.sourceforge.net/">Quanta Plus</a>? Was Wil happy to meet me? <a href="http://quanta.sourceforge.net/main2.php?newsfile=wilwheaton">Follow the link</a> dear reader and look for the pictures at the bottom.
<!--break-->
