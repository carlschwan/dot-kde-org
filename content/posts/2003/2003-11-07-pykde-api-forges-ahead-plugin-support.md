---
title: "PyKDE API Forges Ahead with Plugin Support"
date:    2003-11-07
authors:
  - "jbublitz"
slug:    pykde-api-forges-ahead-plugin-support
comments:
  - subject: "wow"
    date: 2003-11-06
    body: "Jim and David, this is terribly impressive stuff!  \n\nPyKDE is becoming an awesome desktop scripting platform and simply makes the KDE development platform as a whole that much more compelling -- and a beast to reckon with."
    author: "Navindra Umanee"
  - subject: "Perl?"
    date: 2003-11-06
    body: "If only the Perl/KDE bindings would materialize..."
    author: "Stunji"
  - subject: "Re: Perl?"
    date: 2003-11-06
    body: "Why not give the Python bindings a go? :)"
    author: "Richard Jones"
  - subject: "Re: Perl?"
    date: 2003-11-07
    body: "The PerlQt bindings use a library called 'Smoke', which has been Qt-only until recently. But I've got it working with the KDE classes now, and so it shouldn't be too long before Perl/KDE bindings are done. You'll have to ask Germain Garand of the PerlQt team - he's certainly got a Perl KDE hello world working at least.\n\nThe QtRuby bindings are also coming along well (they use Smoke too), and I hope there will be a KDE extension for ruby programming soon.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "This is soo cooool"
    date: 2003-11-06
    body: "PyQt has always rocked (I've used it for a number of years now) in the way it seamlessly brings Qt's object-oriented structure into Python's object-oriented elegence. The first time I extended QListViewItem in plain Python code to override the paintCell method ... that was eye-opening :) David Goodger is a frickin' genius :)\n\nAnd now there's PyKDE plugins too. Whee! Jim Bublitz is a frickin' genius :)\n\nI'm only a little disappointed that PyKDE isn't bundled with KDE itself, allowing others to have this kinda power at their fingertips in a stock-standard KDE install."
    author: "Richard Jones"
  - subject: "Re: This is soo cooool"
    date: 2003-11-06
    body: "I agree 100%. KDE needs a scripting alternative, and it should be available, documented and maintained like the regular KDE libraries are. If not, people can use it to scratch their own itches, but it will not have any major impact on the KDE community."
    author: "Claes"
  - subject: "Re: This is soo cooool"
    date: 2003-11-06
    body: "I couldn't agree more. See this recent thread, here at the dot:\nhttp://dot.kde.org/1066641180/1066669382/\n\n:-)"
    author: "MandrakeUser"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "I'll try to keep this short. I'm in favor in principle of PyKDE being more tightly integrated with KDE. In practice it isn't likely. I wouldn't mind a fork, but that also isn't likely to change much wrt KDE inclusion.\n\nFirst off, PyKDE depends on sip and PyQt written by Phil Thompson. I can't speak for Phil, but we have discussed this and it's extremely unlikely that sip and PyQt will end up in KDE CVS, and for what I think are very good reasons. So just putting PyKDE in KDE's CVS wouldn't help a lot (other than Phil's releases are probably more timely than mine).\n\nSecond, PyKDE isn't written - it's generated. That by itself makes CVS fairly useless. Third, PyKDE is at the end of the food chain - it can't be generated until KDE and sip/PyQt are done (sip is going from 3->4 and KDE 3.1->3.2 right now, and neither is done). Either sip4 will have bugs (as far as PyKDE is concerned - quite different from PyQt) or (more likely) I'll need to learn sip4, so there's some definite pain coming in that area. \n\nThe code (\"presip\") that generates PyKDE from h files will be released as its own project in 6 months or so. It's not KDE specific, so it doesn't belong in KDE CVS either. It's 100% Python.\n\nThere are also a lot of platform-specific problems in building PyKDE on various platforms. There is an excellent Debian maintainer who has made fantastic contributions to PyQt/PyKDE (hi, Ricardo), FreeBSD maintainers have come and gone.I test on SuSE, RH and Mdk, and given an average of 1hr/compile, that takes time (even with multiple boxes) and it takes time to do all the set up for those.\n\nLastly, I have a lot of time to work on this (and it takes a lot of time), but I can't guarantee to match KDE release schedules. There are times when I have to concentrate on earning money and that sometimes pushes things out several weeks or more.\n\nFortunately some people like David Boddie and a few others have jumped in and picked up some things like the plugin stuff and done a better job than I would have besides. I've had a basic understanding of how to do that for quite a while, but just haven't found the time to get to it (I had a KSpread plugin cobbled up long ago but the API has changed a lot since then).\n\nThese aren't the only considerations - there are things like politics that concern me rightly or wrongly and other stuff I can't think of at the moment. I'd be happy to discuss this with anyone, and I'm not easily offended.\n\nI don't know if all that helps any. If someone else wants to try and integrate this better with KDE, I wouldn't be offended and I'll even throw in the tools (GPL)and assistance. Otherwise, I'll just continue with the best effort I can to keep this up to date with KDE. I may not always be timely, but I've been at this over 2 1/2 years, so I think I can be counted on to get PyKDE out eventually.\n\nJim"
    author: "Jim Bublitz"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "Hi Jim,\n\nI understand your concerns, but most of these issuses are just practical ones, and should be solvable in one way or the other. If the current process is not a good fit for PyKDE, then perhaps the current process should be changed so that PyKDE can be part of a standard release. What is in CVS and what is not, and the exact date for a release to appear are minor issues in the overall picture. \n\nI am of the opinion that KDE is pretty full-featured as it is, and it should shift focus from adding features and configuration options to programs, and instead improve, optimize and stabilize its frameworks. Adding bindnings so that scripting languages can be tightly integrated and part of the release should be a major issue. If people in control of the KDE release process made a commitment in this direction, I think you would get a lot of help. In my opinion, bindings for Python is more important than SVG support, for example."
    author: "Claes"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "You and I are in almost complete agreement. The last time I approached \"KDE\" about this, the response was generally \"sure, if you put it in our CVS\". Admittedly this was quite a while ago, and the situation for both KDE and PyKDE was quite different (and I haven't pursued this lately - perhaps it's time for another try).\n\nPyKDE could work as a \"loosely coupled\" part of KDE - maintaining some degree of independence and handling the release schedule similar to the way KDevelop and KOffice do now. I'd like it to be easy for people to obtain, install and use PyKDE, but not be a general requirement.\n\nThis isn't just an \"I want to use a different language\" situation either - as Roberto pointed out in another post here, you can do things easily in Python that are hard in C++. When I had a plugin running for KSpread, I could do things like run multiple spreadsheet files or even manipulate spreadsheets in the background and have everything communicate, all from a single very small script. I don't believe KSpread does those things yet, at least not easily. Eventually I'd like to get back to that kind of thing, but the KOffice API has been a moving target the last year or so (as it should have been - that's not a complaint), and there's other infrastructure on my side that needs some completion.\n\nThis also isn't just a Python issue either - all of the other languages that KDE-Bindings is supporting can benefit from this too.\n\nOne of PyKDE's \"desgin goals\" is also to be completely \"non-invasive\" wrt KDE - PyKDE doesn't require any special hooks or mods to KDE, or require KDE dependencies on Python or other stuff,  and I don't want it to. That, of course, is why plugin support from the PyKDE side is so interesting.\n\nI think what I might do is put together a long explanation of the situation from my perspective and then post some pointers to it on some of the KDE lists.\n\nJim"
    author: "Jim Bublitz"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "Dear Jim\n\nThanks a lot for the comments, and for maintaining PyKDE of course :-)\n\nTo me, the difference between incorporating PyKDE with the KDE releases and releasing separately, is just enormous. In the former case, one could allow for regular KDE applications (released as a part of KDE) to be written with PyKDE. In the latter case, well not. In a practical sense, integration with KDE would be extremely fruitful for the bindings and for KDE.\n\nOf the reasons you point out, the most serious is the dependence on PyQt, and the fact that PyQt doesn't seem to be going to be included in CVS, ever. In the end, this is really bad. \n\nAs for waiting for KDE to stabilize before building the next pykde bindings. This is relative. There is also this issue for the rest of the KDE framework, with repespect to the kdelibs and pyqt. But they work it out. But for each release there is a feature commit freeze well in advance. In fact, you only need the kdelibs to freeze to build PyKDE. ANd I am sure they are pretty reasonable on this regard.\n\nThe only way I really see this happen is if a couple of KDElibs core devs commit to the python bindings, and then they coordinate things with you and Phil to ensure that the chain:\n\nQt --> kdelibs -| --> PyKDE   \n | --> PyQt ----|\n\nCan be built in time. Volunteers may help test/debug. \n\nThe differences between distros/Unix flavors might be solvable using an appropriate build framework, am I wrong ? The files locations are decided at compile time with appropriate configure options for the KDE packages. couldn't these options be used to build PyQT and PyKDE in a generic way ? That's where I would like to see assistance from KDE core devs.\n\nAs for the use of CVS, isn't it true that a core part of PyKDE only changes slightly from release to release ? Even if most of the bindings will be generated by sip, if the process is automated to a large extent, maybe one could build a generic snapshot every once in a while, cointaining a PyKDE binding that is current and \"mostly works\" with KDE CVS. Once the KDElibs are frozen, the process would converge rapidly to a stable binding. The key here is to which extent the bindings can be automated.\n\nOnce again, I am all for integrating your work and Phil's work with KDE, not at all in favor of a fork. PyQT and PyKDE exist thanks to you guys (and your respective teams). I am throughing these ideas almost as a proposal to see if they generate any interest in the KDE side and in your side. I hope I had time to commit to this project, but I could at best give a little help here and there. If there is a move in this direction I'll join the list and see how can I help.\n\nAnyways, cheers and keep up the great work !"
    author: "MandrakeUser"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "You make a lot of good points and I agree with the general tone of your comments, if not some of the specific details. For example, differences between distributions are one of the biggest problems (or have been - they're a little better lately), but not because of file hierarchies. There's an example on the PyKDE list this morning - kdelibs varies from distribution to distribution. One missing method or change in arguments is enough to break all or most of PyKDE, and those are very time-consuming to find.\n\nThere are other differences in PyKDE's build system to cut compile time by 80% (and it can still take 20 min to an hour) and allow a single fileset to support multiple distributions and Qt/KDE/Python versions.\n\nAs I mentioned in another post, I should probably lay out the details more clearly and then see what other people's opinions are. I would like to see KDE offer more in the way of scriptability because I think it's an important feature - look at how widely (and easily) VB is used, and it's not as good a language as Python, Perl, Ruby, etc.\n\nJim"
    author: "Jim Bublitz"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "Hi again Jim,\n\n>There's an example on the PyKDE list this morning - kdelibs varies from >distribution to distribution. One missing method or change in arguments is >enough to break all or most of PyKDE, and those are very time-consuming to >find.\n\nBut then again, isn't that the beauty of getting PyKDE in the KDE main trunk ?\n \nThe version of PyKDE included there should then compile clean with the \"vanilla\" kdelibs and PyQt provided by KDE. If then a distro decides to alter the kdelibs, it should be THEIR responsibility to patch PyKDE accordingly, to \nrerun sip, or whatever is needed to get PyKDE working with their changes,\nor am I missing something important here ? \n\nIMHO, to summarize, including PyKDE in the KDE main trunk, not only would benefit KDE immensily, but would also simplify PyKDE development quite a bit, by giving you just ONE target: the current CVS version of the libs ...\n\nOh, and I couldn't agree more in what you say in the last paragraph, that's why I am posting these messages instead of doing something otherwise more productive :-)\n\nCheers !"
    author: "MandrakeUser"
  - subject: "KDE CVS"
    date: 2003-11-07
    body: "Just to clarify the issue about SIP/PyQt and the KDE CVS.\n\nThe KDE CVS will never be the master repository for either SIP or PyQt, but there is no problem with the KDE CVS containing a copy of the GPL versions. That can happen today and be done by anybody with write access to the repository.\n\nThe only \"problem\" is that updates will have to be done manually, which actually might be a good thing. Given that PyQt and PyKDE have different release schedules, but PyKDE tends to be very dependent on the PyQt version, then deciding when to update the \"official KDE versions\" of each should be a considered decision.\n\nIt just needs somebody in the KDE project to take responsibility for it."
    author: "Phil Thompson"
  - subject: "Re: KDE CVS"
    date: 2003-11-07
    body: "Hi Phil\n\nThanks a lot for the input (and for PyQt). In fact, something similar is done with Qt as far as I know, there is always a copy of the \"current\" qt in CVS, I think the module is \"qtcopy\" or something similar.\n\nTerrific, If no KDE dev steps up in this thread I'll see what's best. I might fill a wishlist bug report or something similar\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: KDE CVS"
    date: 2003-11-07
    body: "Same here - I don't mind if someone wants to put PyKDE in KDE-CVS. I just don't have time to maintain that and it really doesn't fit my development process. I spend a lot of time developing the tools that generate PyKDE and relatively little time on PyKDE itself. PyKDE comes out pretty much \"all at once\" and just needs some tweaking and a little handwritten code, and that part is decreasing quite a bit.\n\nThe build systems for both PyQt and PyKDE don't conform to KDE's build system though. It would be possible/simple to write another \"make\" layer above what's there if that's an issue.\n\nJim\n"
    author: "Jim Bublitz"
  - subject: "Colour me muddled :)"
    date: 2003-11-06
    body: "Ehem. Got my Davids mixed up. Goodger is responsible for the awesome http://docutils.sf.net/ project.\n\nPyQt is the result of (primarily) Phil Thompson's hard work.\n\nDavid Boddie is obviously the David I was thinking about here, but I was clearly confused :)"
    author: "Richard Jones"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "Oh, yeah. I had one of those moments a little ago, too.\n\nI was using a qlistview in a designer form, and wanted to override the class\u00b4 methods without inheriting... just assign my function to the instance\u00b4s .__class__.method and it worked.\n\nI mean, that just can\u00b4t be done in C++, I have no idea how PyQt managed :-)\n\nI am doing all my coding (not that it\u00b4s too much) in PyQt and/or PyKDE for a while. Check out sf.net/projects/krsn for a sample (almost to become beta any day now ;-)"
    author: "Roberto Alsina"
  - subject: "krsn"
    date: 2003-11-07
    body: "So, Roberto, what is it about ? I found no info on sf, \nbut it must be cool :-) "
    author: "MandrakeUser"
  - subject: "Re: krsn"
    date: 2003-11-08
    body: "Nah, no big stuff, just a RSS aggregator :-)"
    author: "Roberto Alsina"
  - subject: "Re: This is soo cooool"
    date: 2003-11-07
    body: "Always check the CVS :)\n\nhttp://cvs.sourceforge.net/viewcvs.py/krsn/krsn/"
    author: "Richard Jones"
  - subject: "License?"
    date: 2003-11-06
    body: "Under what license(s) is PyKDE available. PyQt is GPL/commercial like Qt. Is PyKDE LGPL? Do I have to pay for PyKDE for closed source development?\n\n"
    author: "Nick"
  - subject: "Re: License?"
    date: 2003-11-07
    body: "http://www.riverbankcomputing.co.uk/pyqt/buy.php"
    author: "js"
  - subject: "Re: License?"
    date: 2003-11-07
    body: "PyKDE is released under the GPL (not LGPL)."
    author: "Jim Bublitz"
  - subject: "Re: License?"
    date: 2003-11-13
    body: "OK, first of all: Python for closed source development?  Yeah, I know it's possible, but c'mon.\n\nSecond of all: you want to write a closed-source app, release it . . . are you charging for it?  No?  That stinks.  Yes?  Then why aren't you willing to return the favor?  If all these people who want to release closed-source apps but aren't willing to pay for a license would stop and think about it a second, they'd realize how hypocritical they're being.\n\nPlus, if you're charging for your software, but are unwilling to take the risk of paying a licensing fee for Qt, you must not have that great of a product to begin with."
    author: "Shane Simmons"
  - subject: "pykde"
    date: 2003-11-06
    body: "i'm really looking forward to the pykde kparts export, that would be uber-cool (i really wonder how it will be done too).\n\nit also would be nice if pykde was easier to install (now its rather hard if your distro doesn't come with pykde) so people could start releasing pykde apps.."
    author: "ik"
  - subject: "Re: pykde"
    date: 2003-11-07
    body: "> it also would be nice if pykde was easier to install \n\nThere are generally rpms available at http://sourceforge.net/projects/pykde. They're done by volunteers and they lag the source release by a little bit. The tarball went out today, so I'd expect to see SuSE, RH and Mdk rpms in a week or so (but I have no control over when they actually occur).\n\nThere is a Debian maintainer who monitors the PyKDE list (address in the original article) and who makes packages available - I'm not sure if they're a part of \"official\" Debian or not.\n\nThere is at least one gentoo user on the PyKDE list - not sure if gentoo is packaging PyQt/PyKDE or not.\n\nThe build/compile is fairly long (30 minutes on an Athlon 1900, closer to an hour or more on an 800MHz PIII). If you have problems building, you can post to the PyKDE list - I usually respond quickly and I'm willing to spend as much time as you are (maybe more :) ) getting it running on your system.\n\n(now its rather hard if your distro doesn't come with pykde) \n\nAFAIK, no distro has PyKDE. They usually have sip and PyQt and are usually a version or two out of data (sometimes not a big deal, sometimes it is).\n\nJim"
    author: "Jim Bublitz"
  - subject: "Re: pykde"
    date: 2003-11-07
    body: "\"not sure if gentoo is packaging PyQt/PyKDE or not.\"\n\nYup - gentoo does have ebuilds, simple as 'emerge pykde'... installs from\nsource, deals seamlessly with all dependencies. \n\n\nscanner root # emerge -puv pykde\n\nThese are the packages that I would merge, in order:\n\nCalculating dependencies ...done!\n[ebuild  N    ] dev-python/sip-3.8\n[ebuild  N    ] dev-python/qscintilla-1.54\n[ebuild  N    ] dev-python/PyQt-3.8.1\n[ebuild  N    ] dev-python/pykde-3.7.4-r2\n\n"
    author: "Corey"
  - subject: "Re: pykde"
    date: 2003-11-07
    body: "The kde-redhat.sf.net guys also have rpms for PyQt and PyKDE, and they are (almost?) part of Fedora, so I would expect them to be available for \"Red Hat\" from now on."
    author: "Roberto Alsina"
  - subject: "Plataform Independence?"
    date: 2003-11-07
    body: "First: This is really cool! Keep up the good work!\n\nThe ability of creating plugins in Python is really awesome.\n\nOne point where UNIX-like plataforms are a nighmare is software installation. While I'm used to compile many things from source, most of our potential users are not. (And compiling from source is usually time spending.)\n\nWith the new PyKDE, a whole world of possibilities is now open. Python scripts are plataform independent: the same script runs under RedHat, Slackware, FreeBSD, Conectiva... Therefore, we can provide programs and components that will run everywhere, providing the user has KDE and PyKDE installed.\n\nWriting programs and plugins in a distro independent way may just be the killer feature for KDE. Or am I being too optimistic?"
    author: "Henrique Pinto"
  - subject: "Re: Platform Independence?"
    date: 2003-11-07
    body: "Ideally, all the compiling would be done by the packagers, leaving users to get on with writing Python applications and plugins. This approach sort of works for Panel Applets because the configuration information passed to the applet launcher can be used to infer the name of the relevant Python module. Unfortunately, this doesn't extend to all sorts of plugins because the mechanism for many of them appears to suit a one-library-per-plugin model.\n\nI would hope that most running KDE systems have compilers and linkers available so, by using GUI tools which automatically build libraries for the end user, hopefully the pain is minimised.\n\nWe'll see."
    author: "David Boddie"
  - subject: "Re: Plataform Independence?"
    date: 2003-11-07
    body: "You're right about the scripting - anything written in Python for PyKDE would be platform independent within *nix family. The problem, as you also note is that PyKDE binaries aren't available for all platforms.\n\nNormally there are RH rpms (usually at http://sourceforge.net/projects/pykde). There was a FreeBSD package at one time, but I don't believe it's been maintained. If Conectiva is Debian based, I would expect the Debian packages to work there. I know of at least one Slackware user of PyKDE, but he compiles it for his platform.\n\nGenerally, the only thing required to \"port\" PyKDE to one of the Linux platforms is the correct directory locations of the various files - the code doesn't require any modifications I'm aware of. There have also been individuals porting PyKDE to Solaris and HP, but I haven't seen anything made publicly available for those. PyQt is also working now on OSX (with sip 4) - not sure what would be involved in getting PyKDE operational there or on Linux PPC.\n\nI'd be happy to provide any assistance I can with ports/packages.\n\nJim"
    author: "Jim Bublitz"
  - subject: " Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "This seems really great and I am suprised that KDE is not jumping at the opportunity to use this great technology, after all it is under the same license as Qt.\n\nPlease check it out: http://www.trolltech.com/products/qsa/index.html\nIt seems very interesting, easy, and powerful. It iss a perfect amtch for Qt based applications, such as the KDE desktop, is it now?\n\nPlease explain the problems with it if you feela nother way."
    author: "Alex"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "Or for full KDE integration:\nhttp://xmelegance.org/kjsembed"
    author: "Ian Reinhart Geiser"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "This looks very good. However, I have a few questions.\n\n1.  Why was a completely new project started instead of building on top of QSA? The license seems a big enough reason for most people, but were there technical limitations?\n\n2. Which is better ;p ?\n\n3. Ar ethey compatible, do they use the same scripting language?"
    author: "Alex"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "1) this was started long before QSA\n2) this is better for KDE apps\n3) nope"
    author: "Ian Reinhart Geiser"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-09
    body: "Actually they do use the same language - both are Javascript (ECMA). They're even compatible to a degree, but not 100%.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "> 1. Why was a completely new project started instead of building on top of QSA?\n> The license seems a big enough reason for most people, but were there \n> technical limitations?\n\nYou'd have to ask TrollTech why they started a new project :) PyQt and the original PyKDE date back at least 4 years.The current PyKDE is about 2 1/2 years old. Both pre-date QSA.\n\nPyQt and PyKDE are (hopefully) only the tip of the eventual iceberg. The same tools can be used to bind lots of other libs for Python scripting. For example, I've looked at doing bindings for a 3D visualization package for a large US govt lab (turned out to be proprietary software, so I couldn't do it). You can use sip to write bindings for nearly any C/C++ libs and over the next year or so, that will become easier. That means almost anything can be combined with Qt/KDE using PyQt/PyKDE/Python if someone produces the bindings and/or wrappers for the \"other\" libs.\n\nIt's already pretty easy to do, but the learning curve for sip is steep because the docs are pretty sparse. One of my goals is to produce tutorials and tools for making that easier. At the same time, Phil Thompson is putting a lot of effort in to making the next sip version much easier to use (sip is the infrastructure that makes all this stuff possible in the first place).\n\nAnother way of saying that is that I'd like Python on KDE & Linux to be as ubiquitous and widely usable as VB is on Windows, and Python is already a much better language than VB.\n\n> 2. Which is better ;p ?\n\nI honestly haven't looked at QSA much, so I don't know if better applies in either direction - it depends on what you want scripting to accomplish. Python is a complete development environment with a PyQt/PyKDE/QtDesigner compatible IDE (eric), an extensive selection of libraries (for example strings, os support, XML,sockets, pdf support in ReportLab, math and plotting support, databases, lots of other stuff), convenient built-in data structures, integers as large as available memory to hold them, introspection and just an all-around nice language to work with. The only thing I use C/C++ for any more is PyKDE. Python development is extremely fast, and for GUI apps, the execution speed is more than sufficient, especially since anything with bindings still runs most of its code in C/C++ anyway. \n\n> 3. Are they compatible, do they use the same scripting language?\n\nDon't know - they're different languages, but I haven't looked at using them together.\n\nJim\n"
    author: "Jim Bublitz"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "Jim said it best. But let me emphasize it:\n\nPyQt and PyQt bind an extremely powerfull interpreted OO language, Python with KDE. So, it's not a matter of add some scripting binding, it is a matter of binding PYTHON in particular. There are several reasons why I think Python is THE interpreted language for KDE. "
    author: "MandrakeUser"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "The way I understand it, QSA can really only be used to control a C++ Qt application -- you cannot write a Qt application from scratch using QSA. I may very well be wrong in this but from a quick scan of the website it appears that way.\n\nPyQt/PyKDE allow you to write a complete Qt or KDE application purely in Python. And Python itself provides a wealth of modules with support for everything from data serialization to HTTP server implementations.\n\nIt's really quite nifty ;)\n"
    author: "Gordon Tyler"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-07
    body: "Yes, it's going to be rather humorous to one day reimplement the HTTP KPart using Python's urllib :)\n"
    author: "Richard Jones"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-08
    body: "Don't think I haven't considered it!\n<p>\nSeriously, I think there are application areas where embedded Python can make development a lot more convenient, at least for prototyping purposes. One such area is text manipulation:\n<p>\n<a href=\"http://www.boddie.org.uk/david/Projects/Python/KDE/Images/embedwiki-2.png\">http://www.boddie.org.uk/david/Projects/Python/KDE/Images/embedwiki-2.png</a>\n<p>\nAlthough I'm sure that Perl coders would contend its superiority over Python in this area."
    author: "David Boddie"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-08
    body: "That's awesome! I can just see the potential of a ReStructuredText editing KPart!\n\nBTW, I've *been* a Perl programmer, professionally, and I don't contend its superiority over Python for a second in any area except quick-n-dirty sub-10-line file munging scripts. Even then I've stopped using it for those, but mostly 'cos I've fallen out of practise with Perl."
    author: "Richard Jones"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-08
    body: "Actually, I didn't think of doing anything with ReStructuredText until I saw this:\n\nhttp://python.net/~gherman/ReSTedit.html\n\nI suppose that there are many similarities between Wiki-style \"markup\" and ReST. However, I was also interested in the possibility of dynamically changing the links between pages and creating new ones.\n\nI've been experimenting with providing different Python modules for processing the text, so it isn't impossible that a ReST module could be included."
    author: "David Boddie"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-08
    body: "I'm a *huge* fan of ReST - I've used it for documenting several projects, including:\n\n   http://roundup.sf.net/\n   http://gadfly.sf.net/\n\n... and any documentation I produce for work projects are also done in ReST. Along the way, I've convinced (very easily) several co-workers to go down the same path :)\n\nGiven ReST's great design, and internal DOM structure, I should think that a \"wysiwyg\"-ish structured text editor would be pretty easy. And with PDF and HTML outputs..."
    author: "Richard Jones"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-09
    body: "It looks like a ReST KPart would indeed be very useful. I noticed Ian Bicking's Wiki code in the Docutils Sandbox:\n\nhttp://docutils.sourceforge.net/#the-sandbox\n\nDo you think that this is a good place to start, or do you think that a wrapper for the HTML Writer would be more appropriate? Maybe we should discuss this via e-mail.\n\nI just hope that a tie-in with Docutils isn't going to lead to widespread confusion over which David is which. ;-)"
    author: "David Boddie"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-09
    body: "I don't have time to get into such a project right now. The docutils mailing list has been an invaluable resource for me in the past.\n\nPlenty of projects in the past have accessed the HTML writer at various levels. The ReST product for Zope, for example, pulls out just the body so it may be wrapped in a different page.\n\nOh, and really it's only me that you have to worry about getting Davids confused :)\n"
    author: "Richard Jones"
  - subject: "Re:  Qt Script for Applications (QSA) Anyone??!"
    date: 2003-11-08
    body: "Well.... if you look at KRsN (URL somewhere above :-) in CVS, you can see a stupid web browser implemented using QTextBrowser, urllib and some little glue :-)\n\nProbably about 100 lines of code."
    author: "Roberto Alsina"
  - subject: "PyKDE speed"
    date: 2003-11-07
    body: "Hey,,... is it slower at all ? \n"
    author: "somekool"
  - subject: "Re: PyKDE speed"
    date: 2003-11-07
    body: "I'm sure it's slower in absolute terms. There's the overhead of a byte code interpreter, the creation of objects in  \"C++-Space\" *and* \"Python-Space\" (things like QWidgets in Python have \"shadow\" objects in C++), the need to do lookups to find the binding code, and a bunch of other stuff. HOWEVER, what you write in PyKDE or PyQt is the \"I\" part of the GUI - the \"G\" part (drawing, clipping, most of the event handling like computing where the mouse was when clicked) still takes place in C++ - the same C++ code your C++ app would use. To a large extent, you're looking at adding a millisecond (probably less) to the part of the operation (the \"U\" in GUI) which is already orders of magnitude slower than that.\n\nI've never done any real testing. I'd just say most of the time the speed difference isn't noticeable, and in some of the cases where it is, careful coding can help. In general I think of Python as a \"dispatcher\" - if you use Python to sort a list (which Python's sort() does in C) it's very fast. If you write the sorting routine itself in Python it's likely to be slower. That's the advantage of writing bindings for a C/C++ lib as opposed to rewriting a lib in Python.\n\nJim"
    author: "Jim Bublitz"
  - subject: "Re: PyKDE speed"
    date: 2003-11-07
    body: "Not for most purposes. I've used PyQt for lots of things, I've even done a Qt widget style in PyQt, and never had any performance issues for my book on PyQt (http://www.opendocs.org/pyqt). As Jim says, all the heavy lifting is still done in C++, Python is only the glue.\n\nBut when I recently got interested in doing a pixel-based paint app, I had to move to C++. With all the looping over framebuffers to calculate effects, Python turned out to be too slow, even when using Numeric."
    author: "Boudewijn Rempt"
---
The <a href=http://www.riverbankcomputing.co.uk/pykde/index.php>latest release of  PyKDE</a> (3.8.0) includes <a href=http://www.riverbankcomputing.co.uk/pykde/docs/panapp1.html>the ability to write KDE panel applets</a> completely in Python -- absolutely no C++ required.  This is the first in what's planned to be a number of extensions for PyKDE that allow plugins and related objects to be created entirely in Python; David Boddie is nearing release of modules for authoring KParts for export (PyKDE already imports KParts), KDE Control Center modules, and IOSlaves. 
<!--break-->
<p>
Future plans include allowing QWidget subclasses created in Python to be imported into <a href="http://www.trolltech.com/products/qt/designer.html">Qt Designer</a> with complete functionality, and possibly Python scripting and plugins for KDE apps like <a href="http://www.koffice.org/">KOffice</a> and <a href="http://www.kontact.org/">Kontact</a>. The underlying mechanisms and code are similar in  all cases.
</p>
<p>
In some cases, specific .so libs will still be required (depends on the plugin loader), but the Python modules will include autogeneration of the necessary C++ code, along with installers to simplify the task of making the plugins available.
</p>
<p>
PyKDE 3.8.0 requires <a href=http://www.riverbankcomputing.co.uk/sip/index.php>SIP 3.8</a> and <a href=http://www.riverbankcomputing.co.uk/pyqt/index.php>PyQt 3.8</a>. While the PyKDE 3.8.0 tarball is available now, it will be several days or more before RPM and Debian packages are available. You can check the <a href=http://mats.imk.fraunhofer.de/mailman/listinfo/pykde>PyKDE mailing list</a> for more info.
</p>
