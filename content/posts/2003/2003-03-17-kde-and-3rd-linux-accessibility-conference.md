---
title: "KDE and the 3rd Linux Accessibility Conference"
date:    2003-03-17
authors:
  - "jschnapper-casteras"
slug:    kde-and-3rd-linux-accessibility-conference
comments:
  - subject: "LAX?"
    date: 2003-03-17
    body: "Where is LAX?"
    author: "Anon"
  - subject: "Re: LAX?"
    date: 2003-03-17
    body: "Los Angeles Airport near Inglewood, Westchester and El Segundo. I am not really to which of these three municipalities it belongs."
    author: "radix"
  - subject: "Re: LAX?"
    date: 2003-03-17
    body: "Making it an odd location to accompany a conference at CSUN. Northridge is in the San Fernando Valley, completely on the other side of the Los Angeles basin. \n\nWell, at least spending an hour or two on the 405 will give attendees an authentic taste of life in LA!"
    author: "Otter"
---
The 3rd Linux Accessibility conference <a href="http://ocularis.sourceforge.net/events/csun2003/">will take place</a> on March 20 to 21 in the La Jolla room of the LAX Marriot.  KDE-related activites include an update on the progress of the <a href="http://accessibility.kde.org/">KDE Accessibility Project</a> and a discussion on KDE accessibility and interoperability.  It will coincide with <a href="http://www.csun.edu/cod/conf/">CSUN 2003</a> (aka CSUN's 18th Annual International Conference: Technology and Persons with Disabilities), a large, long-standing conference held annually on the subject of technology for persons with disabilities.  Information about participating via phone is forthcoming.  Please <a href="mailto:jpsc@stanford.edu">email me</a> if you are interested in attending, or participating via phone, so that I can get a headcount.
<!--break-->
