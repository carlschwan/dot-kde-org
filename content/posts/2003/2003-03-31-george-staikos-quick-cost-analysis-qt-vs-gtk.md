---
title: "George Staikos: A Quick Cost Analysis of Qt vs GTK"
date:    2003-03-31
authors:
  - "numanee"
slug:    george-staikos-quick-cost-analysis-qt-vs-gtk
comments:
  - subject: "Michaels slides"
    date: 2003-03-31
    body: "Funny. Michaels slides mostly speak about issues KDE already solves - and this is the platform that is supposed to be cheaper to develop on (let alone using it) ?"
    author: "jmk"
  - subject: "Silly war"
    date: 2003-03-31
    body: "Please stop comparing KDE and Gnome, one war is more then enough"
    author: "Norbert Pil"
  - subject: "Re: Silly war"
    date: 2003-04-04
    body: "Cheers to that, brother."
    author: "Ryan"
  - subject: "Another calculation"
    date: 2003-03-31
    body: "10 developers cost 60.000$ per year (75.000$ is pretty the top for paying developers).\nThat's 1.200.000$ for a 2 year project.\nGetting the most out of Qt one needs \"Qt Enterprise Edition TrioPack\".\nThat's 37.200$ for the first,11.500$ for the second year and 48.700$ for the whole project.\nIn this calculation Qt costs 4% the developers cost (and maybe 3% of the project costs).\nFor gaining any benefit for using Qt one has to finish the development one month earlier. Which is possible by using Qt (as it's a damn good tool). Than one saves 50.000$. But saving development time is very much dependend of the project, and Qt may only be used for a few parts of the project and won't save any more than 1% of the time.\nAdditional benefit is maintainance, as Qt usually needs less code than MFC, GTK,... and is very easy to learn.\nOne more on Qt 50.000$ cost. It's nearly one additional developer for one year!\n\nConclusion:\nQt is a pretty good tool. But as any tool, one has to know which tool to use when. Qt is _not_ the solution for every (GUI) problem.\n\nciao"
    author: "birdy"
  - subject: "Ximian OpenOffice.org"
    date: 2003-03-31
    body: "who gives a $$@# !\nMy goodness, use what you want to use.\nI for one can't wait for the release of the Ximian Open Office. QT or not, it promises to be damn good."
    author: "pnut"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-03-31
    body: "MSOffice format as default?? http://www.gnome.org/~michael/XimianOOo/img14.html\nFirst .NET , now this. What next?\nSomehow this convinces me that the ex-microsoft guys of gnome and ximian left M$ to perpetrate the divide and rule policy for M$. "
    author: "aa"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-03-31
    body: "Give it up! Like it or not, MS Word is the widely supported de-facto standard, and there's nothing you can do to change that. Work together with the ruler or be destroyed."
    author: "Me"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-03-31
    body: "I do choose to be destroyed."
    author: "Day"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-03-31
    body: "What? Of course MS Office Format is the standard.... _but_: \nIt is _a lot_ better to save your documents in OO's own format. From what my experience is I can say that once a document is a bit corrupted most of the work can be restored with OO's file format whereas this is seldom possible with MS format... funny thing though that OO does a better job in restoring corrupted documents than MS itself... MS file format is a lot more complicated with a hell lot more of cross referencing pointers inside the format than OO's format. \n\nNote:\nOO's file format is superiour to MS file format from a technical point of view.\n\nAdvice:\nUse MS file format where you have to, but don't rely on it for storing and backing up your work. \n\no You don't have to send MS files to make others read your documents (please use the de-facto standard Adobe pdf), plus you don't have to trust everybody to not change the content of your docs before forwarding your documents (maybe with your companys high-quality logo embedded) to somebody else. \n\no If you want to exchange editable documents where somebody else has to modify or copy-and-paste your work: use MS office format.\n"
    author: "Thomas"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-03-31
    body: "\"\"\"OO's file format is superiour to MS file format from a technical point of view.\"\"\"\n\nWhere can I have more info on this? (XML-based, and open standard doesn't count sorry)."
    author: "Michele"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-04-01
    body: "Oh, but XML and open-standard do count. XML = more easily repairable in case of corruption (important in an ever-more networked world), and open-standard means more easily convertible and more easily accessible by secondary tools."
    author: "Rayiner Hashem"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-04-01
    body: "MSOffice uses XML too.  Don't mean nothing."
    author: "anon"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-04-01
    body: "XML is an option."
    author: "Datschge"
  - subject: "MSOffice doenst REALLY use XML"
    date: 2003-05-01
    body: "MS's \"support\" for XML is only skin deep. They are just using it as a wrapper for their binary constructs."
    author: "anon"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-04-01
    body: "I agree using OO's own format.\n\nMS Office Format may be the standard, meaning most of the world is using it.\n\nBut the MS Office format isn't open, or is it? Using a format that is analysed / reversed engineered isn't that wise. Especially IMPORTING e.g. a MS Word file always looks different in OpenOffice / StarOffice Writer than it does in MS Word."
    author: "Gert-Jan van der Heiden"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-04-02
    body: "I don't agree that pdf is a good way to get the file not changed, it is only a bit harder to do. The advantage of pdf is the wide spread of readers and the hardware independence of the displayed content. But to protect the content aginst changes there is only the way to sign the document with cryptographic methods."
    author: "Rudi"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-03-31
    body: "\"We should join with Sauron. It would be wise my friend.\""
    author: "David Johnson"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-05-01
    body: "LOL"
    author: "D4rk|y"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-04-05
    body: "\"...de-facto standard...\" like \"all\" salesmen told you?\n\nUse XML (even if M\u00a7 use \"there version\" only for the \"enterprise\" Office versions)!\n\nWay to go M\u00a7.\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Ximian OpenOffice.org"
    date: 2003-03-31
    body: "If you really don't give a $$@# about things like this, I think you www time is better spent reading other sites. ;)\n"
    author: "Apollo Creed"
  - subject: "Feeble"
    date: 2003-03-31
    body: "How on earth can you claim that 10% less 'lines of code' results in 10% less costs? I doubt that. If a programmer has to think for an hour to write a certain function in 100 lines of code, it's not very likely he will do it in 90 lines of code in 54 minutes. Probably the time needed is more or less the same and very dependent on the quality of the programmers. But then again who cares. KDE and Gnome should bash eachother less. There is no need for either of them to bash another product, both being brilliant..."
    author: "wzzrd"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "It is difficult to summarise in a small article all the gain of Qt over Gtk. This is why George has taken conservative numbers like 30% less line codes means 10% faster. However, if you really want to dig into it, there is a huge difference of productivity between Gtk and Qt, whichi in my opinion, is closer to the 50% productivity boost than 10%.\n<p>\nYou can check: <a href=\"http://www.kuro5hin.org/story/2003/3/19/8928/76183\">[kuro5hin.org]</a> for a remote idea. You can also have a look at <a href=\"http://phil.freehackers.org/kde/cmp-toolkits.html\">cmp-toolkits.html</a> .\n<p>\nThe heart of the difference is that you can have a better, as more versatile, more reusable, architecture with Qt than with Gtk. With Gtk, you spend also a lot of your coding time in writing structs and type-unsafe MACROS because Gtk re-invents the OO programming in C. The fact that C++ is OO in the language saves a lot of time and catches a lot of mistakes at compile time."
    author: "Philippe Fremy"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "You appear to be ignoring the existance of GTKmm, which produces similar looking code to Qt (but without a preprocessor and using the STL).\n\nI think Michael Meeks was a bit off base with focusing on the costs, I think the issue of control is far more important here, but to claim that GTK is \"less productive\" than Qt because you can, if you wish, write more LOC in C, sounds extremely suspect. Like most decent toolkits today, GTK has bindings into all kinds of languages. There's no need to write in C, not even if you wish to create new widgets."
    author: "Mike Hearn"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "I am aware of gtkmm. I have even discussed with some people who program in Gtk and C++. You should wonder why the use the C binding and not the C++ one. They told me that gtkmm it is too complicated and that you have better support for the native C.\n\nYou don't need C to program in Gtk only from a short-sighted point of view :\n- If you want to use gnome, you need C because most bindings don't bind gnome or are very late in doing it, and usually don't do it completely. So you need the C binding to take advantage of the interesting technologies. \n- If you want people to join and contribute to your project, you need C too because most people don't program in exotic language. Would you code a feature for a project like unison which uses gtk and OCaml ? (by the way, it is a pity that they can't use gnome, it would make the project much more interesting and usable).\n- If you want help and support from the community when you have a problem, it is better to use the native binding.\n- The tutorial are in C, the books about Gtk are about C programming.\n\nFor all these reasons, you end up with 95% of the applications written for gnome/gtk in C.\n\nWhile the emphasis may appear on the LOC, the problem of gtk is that it is a lot more complicated to do simple things. \n\nFor example, here is the code to declare a tictactoe widget that inherits a normal widget (I did not make it up, this comes from the gtk tutorial http://www.gtk.org/tutorial/app-codeexamples.html#AEN2923):\n\n#ifndef __TICTACTOE_H__\n#define __TICTACTOE_H__\n\n#include <glib.h>\n#include <glib-object.h>\n#include <gtk/gtktable.h>\n\n\nG_BEGIN_DECLS\n\n#define TICTACTOE_TYPE            (tictactoe_get_type ())\n#define TICTACTOE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TICTACTOE_TYPE, Tictactoe))\n#define TICTACTOE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TICTACTOE_TYPE, TictactoeClass))\n#define IS_TICTACTOE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TICTACTOE_TYPE))\n#define IS_TICTACTOE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((class), TICTACTOE_TYPE))\n\n\ntypedef struct _Tictactoe       Tictactoe;\ntypedef struct _TictactoeClass  TictactoeClass;\n\nstruct _Tictactoe\n{\n  GtkTable table;\n  \n  GtkWidget *buttons[3][3];\n};\n\nstruct _TictactoeClass\n{\n  GtkTableClass parent_class;\n\n  void (* tictactoe) (Tictactoe *ttt);\n};\n\nGType          tictactoe_get_type        (void);\nGtkWidget*     tictactoe_new             (void);\nvoid           tictactoe_clear           (Tictactoe *ttt);\n\nG_END_DECLS\n\n#endif /* __TICTACTOE_H__ */\n\n-----------------------------------------------------------\n#include <gtk/gtksignal.h>\n#include <gtk/gtktable.h>\n#include <gtk/gtktogglebutton.h>\n#include \"tictactoe.h\"\n\nenum {\n  TICTACTOE_SIGNAL,\n  LAST_SIGNAL\n};\n\nstatic void tictactoe_class_init          (TictactoeClass *klass);\nstatic void tictactoe_init                (Tictactoe      *ttt);\nstatic void tictactoe_toggle              (GtkWidget *widget, Tictactoe *ttt);\n\nstatic guint tictactoe_signals[LAST_SIGNAL] = { 0 };\n\nGType\ntictactoe_get_type (void)\n{\n  static GType ttt_type = 0;\n\n  if (!ttt_type)\n    {\n      static const GTypeInfo ttt_info =\n      {\n        sizeof (TictactoeClass),\n        NULL, /* base_init */\n        NULL, /* base_finalize */\n        (GClassInitFunc) tictactoe_class_init,\n        NULL, /* class_finalize */\n        NULL, /* class_data */\n        sizeof (Tictactoe),\n        0,\n        (GInstanceInitFunc) tictactoe_init,\n      };\n\n      ttt_type = g_type_register_static (GTK_TYPE_TABLE, \"Tictactoe\", &ttt_info, 0);\n    }\n\n  return ttt_type;\n}\n\nstatic void\ntictactoe_class_init (TictactoeClass *klass)\n{\n  \n  tictactoe_signals[TICTACTOE_SIGNAL] = g_signal_new (\"tictactoe\",\n                                         G_TYPE_FROM_CLASS (klass),\n                                         G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,\n                                         G_STRUCT_OFFSET (TictactoeClass, tictactoe),\n                                         NULL, \n                                         NULL,                \n                                         g_cclosure_marshal_VOID__VOID,\n                                         G_TYPE_NONE, 0);\n}\n\n----------------------------------------\n\nHere is the equivalent Qt code:\n\n#ifndef __TICTACTOE_H__\n#define __TICTACTOE_H__\n\n#include <qwidget.h>\n#include <qpushbutton.h>\n\nclass TicTacToe : public QWidget\n{\n    Q_OBJECT\n\npublic:\n    TicTacToe( QWidget * parent = 0L );\n    void clear();\n\nsignals:\n    void tictactoe();\n\nprotected slots:\n    void toggled(bool);\n\nprotected:\n    QPushButton *buttons[3][3];\n};\n\n#endif\n--------------------------------------------------\n\n\nHonestly, which one is more complicated ? Where do you have more chances of making a mistake ? Remember that the Gtk version is macro-based, so a simple error may not be detected at all. For example, I have introduced an error in the code above which may create a strange compilation error one day. Have you noticed it ?\n\nSo the easy part is to day that in this example, the gtk example takes a lot more lines of code, but the rationale is that it is a lot more complicated. Hope you see  my point now.\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "Hmm, so, you didn't actually _try_ GTKmm then, you just took some other peoples word for it that it's \"too complicated\" - in which case, why were they using it themselves?\n\nI really think you should form your own judgements about this.\n\n[ - If you want to use gnome, you need C because most bindings don't bind gnome or are very late in doing it, and usually don't do it completely. So you need the C binding to take advantage of the interesting technologies. ]\n\nNot really. Almost everything 95% of apps use is bound to C++ at any rate, I think perhaps you can't define Bonobo objects in C++ yet (but very few apps do that anyway). Sure, some of the less popular bindings aren't complete, but that's not the case for the mm series.\n\n[ - If you want people to join and contribute to your project, you need C too because most people don't program in exotic language. ]\n\nC++ is hardly exotic, but OK, point taken. How does this not apply to KDE/Qt though, esp as Qt extends C++ and redefines a lot of stuff in the STL.\n\n[ Would you code a feature for a project like unison which uses gtk and OCaml ? ]\n\nEr, probably not, unless it was really cool. That's the tradeoff with using any language though, it's got nothing to do with the widget toolkit used.\n\n[ - If you want help and support from the community when you have a problem, it is better to use the native binding. ]\n\nThat's rather subjective, perhaps it contains a grain of truth but all the major bindings for GTK have thriving communities and mailing lists.\n\n[ - The tutorial are in C, the books about Gtk are about C programming. ]\n\nThat's about the only point that makes sense. But if you can read C++ you can read C, and reading C does not force you to write in it.\n\n[ For example, here is the code to declare a tictactoe widget that inherits a normal widget (I did not make it up, this comes from the gtk tutorial http://www.gtk.org/tutorial/app-codeexamples.html#AEN2923): ]\n\nYes, I'm quite aware of GObject thanks. It is indeed rather ugly and verbose, but the reason it's used is to make it easier to create and use bindings! There's no way you can credibly claim that GTK is more complex unless you use the same language to compare, and actually *have* experience of both, as opposed to going on random stuff people have told you or first impressions from looking at the code. Take a look at this example (and it doesn't even use glade remember):\n\nhttp://tools.devchannel.org/devtools/03/03/21/1834244.shtml?tid=39\n\nI think that is a pretty easy to understand article, especially considering:\n\na) My C++ skills are rudimentary at best\nb) It doesn't use Glade (ie constructs the guis in code)\nc) It uses one of the most complex things in GTK, namely the model/view based tree widget."
    author: "Mike Hearn"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "I have tried gtkmm recently, and it didn't work out.  Nice interface, but when you need to debug something you're screwed when you have to wade through all the fugly gtk muck of C code that has to do all kinds of backflips to achieve what QT does more natively, and concisely.\n\nThe only good thing I got out of it was the nice sig/slot library, which is separated, thank goodness.  Also if you want to do any serious work on the internals of GNOME you're pretty much stuck with their spaghetti maze of interdependant libraries and weak interfaces.\n\nAnother thing, is that the way problems are idiomatically solved in C and C++ are quite different these days.  Someone who learned C++ first and now programs heavily with a functional/OO style can't necessarily just \"read\" (translation: follow just as well) C just because it's mostly a sub-language.\n\nThe OP was also making remarks about a QT and gtk comparison, and gtk != gtkmm, so there isn't any \"ignoring\" going on here.  On the contrary, you just decided to butt in some snide, unrelated remarks.\n\nAnd finally, the \"thriving communities\" of gtk bindings is mythic bullshit.  I've got several non-C programs with strange bugs right now that magically appear and dissappear between releases.\n\nTo be perfectly honest, you sound like a well-indoctrinated GTK/GNOME fanboy.  It would be nice if instead of regurgitating the line you're fed from others, you'd constrain yourself to things of your own knowledge and experience."
    author: "Anonymous"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "OK, so you tried GTKmm and didn't like it, that's fine by me, I don't have any problem with people using Qt in their apps. Some posters here appear(ed) to be unaware of its existance though. \n\nI don't agree that gtk != gtkmm, GTK is a widget toolkit, bindings are just different ways of accessing the same thing. So saying, \"GTK sucks because C is more verbose than C++\" is like say, oh, I dunno, Qt sucks because C++ multiple inheritance is confusing (or whatever). C++ is just the means of accessing Qt, it doesn't seem very relevant.\n\nBy \"thriving community\" I meant there are active (busy) mailing lists for the major bindings, those being C++ and Python. Bugs happen, we all know that, I don't see how that's relevant to my comment on communities either.\n\n[ To be perfectly honest, you sound like a well-indoctrinated GTK/GNOME fanboy. It would be nice if instead of regurgitating the line you're fed from others, you'd constrain yourself to things of your own knowledge and experience. ]\n\nWell, I would if others did the same, the reason I am posting here tonight is because I clicked \"Read more\" and saw lots of fanboys repeating stuff they'd been told by others  (presumably) or stuff that was just plain wrong about GTK/GNOME. I'm hardly a GNOME fanboy, there are parts of GNOME that suck and I'm happy to talk about them without getting rude about it. Defending GTK against inaccuracies and FUD doesn't make me a fanboy (or rather, shouldn't)."
    author: "Mike Hearn"
  - subject: "Re: Feeble"
    date: 2003-04-01
    body: "> Some posters here appear(ed) to be unaware of its existance though.\n\nI doubt anyone here is unaware of gtkmm's existance.\n\n> So saying, \"GTK sucks because C is more verbose than C++\" is like say, oh, I dunno, Qt sucks because C++ multiple inheritance is confusing (or whatever).\n\nNo, it's not. You have to *write* all those lines, and to write them correctly, and then to maintain them. Even though the general idea when it comes to languages is that \"all languages are equal, use what you like\", it is simply not true in practice. The more you have to write, the harder it is, period. It's purely a statistic game, more lines of code => more bug. There's no way around it. Perl and C are both two ways to access the kernel functions, would you agree that both are equal ? It doesn't make sense.\n\nAs for being a \"fanboy\", a google of name and a look at my homepage should be enough to prove you that I know what I'm talking about.\n\nThere's a reason why, in about 5 years of existence, only a handful of programs are based on gtkmm, and even some reverted from that to plain GTK+ or even Qt. It's not just because it's \"in C++\" that it's equal to Qt. I learned that the hard way :-)."
    author: "Guillaume Laurent"
  - subject: "Re: Feeble"
    date: 2003-04-01
    body: "You are _such_ a fanboy.\n\nNote that Phillip never said anything about gtkmm yet you felt the need to jump in and pump it up without really knowing anything about it?!  Why should anyone listen to you when you freely admit that you don't understand C++?  Here is a different perspective from someone involved in actually creating gtkmm:\n\nhttp://www.telegraph-road.org/writings/gnome_languages.html\n\n\"... the bottom-line is still that programming for Gnome in anything else than C is far from immediate and requires a significant added effort.\""
    author: "anon"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "Well, to be fair, here is the gtkmm equavilent:\n\n#ifndef __TICTACTOE_H__\n#define __TICTACTOE_H__\n\n#include <gtkmm.h>\n\nclass TicTacToe : public Gtk::Widget\n{\npublic:\nTicTacToe(Gtk::Widget *parent = 0L );\nvoid clear();\nSigC::Signal0<void> tictactoe() { return _tictactoe };\n\nvoid toggled(bool);\n\nprotected:\nSigC::Signal0<void> _tictactoe();\nGtk::Button *buttons[3][3];\n};\n\n\nI'm not sure if the above example is 100% correct (i don't even have any idea of what the original code does).\nIt's actually even less code than the Qt example, but the important part is that it is typesafe and it's genuine C++ (doesn't use a preprocessor like Qt). Gtk also uses modern C++ features (e.g. namespaces, STL) when appropriate. \n\nAlthough gtkmm is much more up-to-date than it used to be, i do understand that many programmers would like to use a \"pure\" C++ toolkit, but overlooking gtkmm is a serious mistake when discussing gtk vs qt.\n\n"
    author: "Fredrik"
  - subject: "Re: Feeble"
    date: 2003-04-01
    body: "http://www.telegraph-road.org/writings/gtkmm_vs_qt.html"
    author: "anon"
  - subject: "Re: Feeble"
    date: 2003-04-01
    body: "> but overlooking gtkmm is a serious mistake when discussing gtk vs qt.\n\nThis discussion is about commercial usage of gtk vs qt, and one thing that gtkmm lacks but that both gtk and qt have is: real usage, that is to say: both qt and gtk have been used successfully for large applications (open source or commercial).\n\ngtkmm has not been used for large applications as far as I'm aware (if I'm mistaking feel free to correct me), and commercial developpers *hate* to be first movers because usually the first one gets to find the bugs, find that some parts miss documentation,etc..\n\nSo I'd say that for commercial development gtkmm is mostly irrelevent.\n"
    author: "renoX"
  - subject: "Re: Feeble"
    date: 2003-04-02
    body: "Galeon? Epiphany? Gabber?"
    author: "Mike Hearn"
  - subject: "Re: Feeble"
    date: 2003-04-01
    body: "And here is the equivalent PyGTK since, we're comparing apples and oranges anyway:\n\nimport gobject, gtk\n\nclass TicTacToe(gtk.Table):\n  __g_signals__ = {'toggled': (gobject.RUN_SIGNAL_FIRST, \n                               gobject.TYPE_NONE, (gobject.TYPE_NONE))\n  buttons = []\n  def clear(self):\n    pass\n\nWithout preprocessor, without a compiler, just time and run."
    author: "Johan Dahlin"
  - subject: "Re: Feeble"
    date: 2003-04-01
    body: "What?  Are you sure?  That looks horribly ugly.  You should take a look at PyQt."
    author: "anon"
  - subject: "Re: Feeble"
    date: 2003-04-02
    body: "Indeed.\n\n\nfrom qt import *\nclass TicTacToe (TicTacToeBase):\n\tdef __init__(self,parent):\n\t\tTicTacToeBase.__init__(self,parent)\n\tdef clear(self):\n\t\tpass\n\tdef toggled(self,isToggled):\n\t\tpass\n\nNo need to declare the signals, all methods are slots, the TTTBase class is made in designer and defines the buttons, so no need to declare them here.\n\n"
    author: "Roberto Alsina"
  - subject: "How do you reuse components?"
    date: 2003-04-02
    body: "Say you developed a brand new spanking widget in gtkmm by deriving from a well defined base class? How can any gtk+ user benefit from your efforts? \n\nThe answer is they can't. So you are forced to implement your widget in gtk+ and then wrap it in gtkmm to be able to use it afterwards if you want others to benefit from your efforts. This is one point always overlooked by application development with toolkits such as Qt or gtk+. For others to be able to use your components the base development has to be done in the main implementation language which is C++ for Qt and C for gtk+. \n\nThe first is only a problem if you are into holy wars whereas the latter enforces doing things the hard way. If there were no C++ (or other OO-Language) then the approach to doing OO in C by hand wouldn't be as much frowned upon. When C++ wasn't up to the task (as it was about 6-8 years ago) there even was a  soon-to-be-replacing-motif (back then motif still was the rage) development (Fresco) that used C++ and failed because there weren't any decent compilers that could handle it's compilation.\n\nBut I believe in the \"choose the best tool for the job at hand\"-rule and being a C programmer for more than 16 years I still decided that GUI programs are best written in OO fashion and learned C++ (after doing GUI design in C on windows for about 3 years and hating it). In our company we have two commercial licenses of Qt since three years and my own development speed has more than doubled compared to the bare bones (API and MFC) Windows-development and quadrupled when compared to previous X application development (where I used Xt and tried XView).\n\nregards\nCharlyw"
    author: "Charlyw"
  - subject: "Re: How do you reuse components?"
    date: 2003-04-02
    body: "> For others to be able to use your components the base development has to be done in the main implementation language which is C++ for Qt and C for gtk+. \n \nWhich is what Microsoft has tried to fix with .NET."
    author: "meh"
  - subject: "Re: How do you reuse components?"
    date: 2003-04-02
    body: "But only by prescribing that all languages have to use the same kind of interface at the lowest level - even if this means that the language can't be used to it's full extent or the implementation has to go through loops and hoops to be compatible. It all comes at a price. And the price for this is the lockin into the Windows platform which I am not willing to pay.\n\nAs far as the argument between Qt andt gtk+ is concerned: as long as you are willing to pay the price (here using the main implementation language of C++/C respectively) then the corresponding toolkit is ok. But even with my decade long experiences - or just because of these - with C I wouldn't pay the price to use gtk+.\n\nregards\nCharlyw"
    author: "Charlyw"
  - subject: "Re: How do you reuse components?"
    date: 2003-04-02
    body: "No. .NET is not about being able to program is whatever language you like, this is totally wrong. It's about being able to reuse legacy code without too much trouble. If you start developing in .NET, you will do it in C#, period."
    author: "Guillaume Laurent"
  - subject: "Re: How do you reuse components?"
    date: 2003-04-02
    body: "Actually, AFAIK you can use KParts written in Java (w/Qt bindings) just fine. Gideon/KDevelop even ship with a few, IIRC. Yeah, it needs some bolierplate factory code in C++, but it is still possible. IIRC, this works since in order to embed a component, you don't actually need access to all of its functionality, and just the functionality for the base class, and the basic KParts interfaces. Of course, the things one can do are somewhat limited to basically just feeding data to the widget for editting or display or such, but there are plenty of uses for that...\n\n\n\n"
    author: "Sad Eagle"
  - subject: "Re: How do you reuse components?"
    date: 2003-04-02
    body: "But you are stuck, if you'd want to extend the widget by deriving from it and changing the behaviour of it. As nice as the whole embedding is (and it's unusable if you want to be portable between platforms such as Linux, Mac and Windows) you still have to resort to the implementation language of the widget to enhance it to do whatever you need it to do if it doesn't fully meet your requirements. \n\nAs nice as both bonoboo and kparts may be to make the components interoperable, if you want to use the foundations provided by the toolkits to build something new - such as a whole application - you are best off using the primary implementation language for at least the generic widgets that might be of use to others when extended. Component models are really something to get bigger applications work together.\n\nLanguage bindings though - however nice and complete they may be - are a cludge when compared to the real thing because you detach yourself from the main implementation and other people are barred from using your enhanced components by enhancing them themselves.\n\nregards\nCharlyw\n"
    author: "Charlyw"
  - subject: "Re: Feeble"
    date: 2003-03-31
    body: "Not only I have to agree: In his book \"The C++ programming language\" Stroustrup showed the problems when using programming paradigm which weren't those of the programming language. IMHO its a good proof that OO is the best known paradigm for GUI-related stuff when GNOME tries to implement OO paradigm in a procedural language. There are cases where a procedural paradigm fits better (which Stroustroup pointed out too), but for big, long-life projects its nice to have the abstraction layer of objects - at least object-based programming, and for easy-to-understand-designs generalization is a great help too. The big advantage of C++ is that it provides the C subset for the lowest layers of a design, resulting in a best-fit approach (OO for the upper levels, dirty C for the lowest levels).\nThat's in short "
    author: "Ruediger Knoerig"
  - subject: "Qt > GTK"
    date: 2003-03-31
    body: "Come on, you are talking only for initial development cost. What about the maintenance cost and bugfixes?! Also don't forget about that QT vs GTK, QT has very good OO advantage - which means that developer is strongly encouraged to reuse code!\n\nIn a big project bugfixes, later upgrades, maintenance, etc. strongly depends of amount and quality of the code, which on the other hand strongly depends of the quality of OO framework of your choice. I will not say anything more because one can guess, but QT is definite winner when talking about of efficient coding.\n\nThink about it.\n\nGreetings."
    author: "Anton Velev"
  - subject: "yes"
    date: 2003-03-31
    body: "I know I will get flamed for my opinion regardless for what I write rather than who I am but:\n<p>\n> the case for GNOME as the only viable desktop on Unix\n<p>\nI gonna belive this the day GNOME gets tools like those listed here:\n<p>\n<a href=\"http://www.osnews.com/comment.php?news_id=3099&limit=no#84115\">[osnews.com]</a>\n<p>\nWhy listening tools ? Well simple because if you compare the cost of development and development time then I better use a OO based foundation and know that I can develop apps with some lines of code rather than using a foundation written with C where I permanently stick into trouble and reinvent the wheels e.g. different Dialogs, Different Toolbars, Different Menu's, Different way of storing thumbnails etc. To solve all the existing issues in GNOME now will take them 3-4 times longer than using Objects which are programmed already and have them embedded into their own applications. I would say, let's wait another 2 years and compare back. While GNOME is still solving their heavy issues and talk what to adapt next KDE has stepped yet another way further into the right direction. All the applications are there (List above) they only need to get polished and enchanced but so far for business and real corporate this looks good.\n<p>\n> And, of course, the ABI/API stability Michael claims for GNOME\n<p>\nSuch as multiple Toolbars and that herodic FileSelector ? GNOME has 5 Different really stable Toolbars and a stable FileSelector that hasn't changed since day 1.\n<p>\n<a href=\"http://www.gnome.org/~chrisime/random/ui/\">gnome/chrisime/random/ui/</a>\n<p>\nI know my reply heatens this situation once again a tad more but I programmed long enough using GNOME libraries to know what I write about. Specially the rapid development of GNOME applications is worth to mention. Yes people really like to use the STABLE API/ABI of GNOME specially all the undocumented parts of the libraries and functions. I always celebrate the non-existing documentations for programmers."
    author: "oGALAXYo"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "What are you smoking?  kdevelop counterpart is anjuta (in cvs I think, but gideon is not out yet either), quanta has bluefish, and I'm sure many others have counterparts (ie, I don't want to waste time searching)."
    author: "Tyreth"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "I'm smoking 'the facts of reality'. Anjuta1 == GNOME 1. Anjuts2 == GNOME 2 but far from being usable, far from being a valid competitor to KDevelop. And comparing Quanta with BlueFish is simply laughable."
    author: "oGALAXYo"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "Nobody cares."
    author: "Me"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "You seem to care a lot otherwise you wouldn't have replied :)"
    author: "oGALAXYo"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "> And comparing Quanta with BlueFish is simply laughable.\n\nI'm pretty sure you're smoking something now too. Both tools have a good feature set. I suspect though that you are suggesting Quanta is laughable as you're obviously trolling. I can tell you Quanta is a lot more popular. I can also tell you haven't looked at it for some time. As I am a gentleman I won't laugh at Bluefish for not being DTD driven with XML configuration files. Code folding? I won't laugh because we have a WYSIWYG part in CVS, have dropped below 4 reported bugs in KDE Bugzilla and off the KDE top 100 bugs list, have a number of new features... Quanta is integrating Kommander, a visual dialog editor for custom text manipulation and script control with our user programmable actions... Our full document parsers are incredibly fast...\n\nIt would be rude to laugh at another software project just because they were falling behind in these areas... Especially when they were in existance when Quanta was started and when most of the original development team left Quanta was largely undeveloped for a year. Hmmm?\n\nQuanta takes a back seat to no web tool. Had we focused more on bells and whistles than our architecture prepping for being totally incomperable we could have been like other tools... but right now there is no tool open or closed source that is stacking up to Quanta's 3.2 feature set. That doesn't mean I will laugh at Dreamweaver either.\n\nBTW your credibility here is pretty much zero with such ignorant statements."
    author: "Eric Laffoon"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "I think he meant it the other way around ie Quanta is better and that the comparison with Bluefish simply doesn't hold.  :-)"
    author: "anon"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "In the context of his first comment I think it is clear he menat that Quanta is far ahead of BlueFish."
    author: "Spencer"
  - subject: "Re: yes"
    date: 2003-04-01
    body: "Sorry for being not precise.. As the other 2 repliers told you:\n\nQuanta > BlueFish"
    author: "oGALAXYo"
  - subject: "Re: yes-- HI Eric!!"
    date: 2003-04-04
    body: "Hey man, thanks for chewing me out and telling me about your fine experiences with Gentoo!\n\nI love gentoo now myself!\n\nAnyway, I think oGalaxyo or whatever does not speak english as his first language (sorry if I am wrong), but he was saying that Quanta is light years *AHEAD* of Bluefish, which is very very very true.\n\nThanks for all the work making such a great project!\n\nWill be buying cat toys soon!"
    author: "billy gates does bongs"
  - subject: "Re: yes"
    date: 2003-04-03
    body: "And KDE's equivalents to Evolution, The Gimp, GnuCash, gaim, red-carpet, rhythmbox, gnumeric, abiword, are equally laughable.  "
    author: "joe"
  - subject: "Re: yes"
    date: 2003-04-04
    body: "While the GNOME desktop itself is *extremely* laughable compared to KDE."
    author: "meh"
  - subject: "Re: yes"
    date: 2003-04-07
    body: "so on that comment, you have a great desktop, and shitty applications? Thats good, we're all in agreement here.\n\n"
    author: "ggg"
  - subject: "Re: yes"
    date: 2003-04-01
    body: "Sorry to burst your bubble, but bluefish is a GTK app, NOT a gnome one. It has no Gnome dependancies whatsoever. By your logic, I could make ANY window manager (for example XFce) the #1 desktop by just grouping it with a bunch of neutral gtk apps. And to be honest, most people can't even FIND Anjuta2, yet alone benifit from it's immature state (or Galeon2 either for that fact...)"
    author: "chillin"
  - subject: "Docs?"
    date: 2003-03-31
    body: "Galaxy, I'll skip the flames and will give you some good news. I'm sure you'll like it, since you only intend Gnome to improve, though I believe you choose the wrong methods.\n\nGnome documentation page updated:\n\nhttp://developer.gnome.org/doc/API/\n\nNow, with some devhelp books of that and finishing some incomplete docs there will be no reason to whine <:-)"
    author: "Dekkard"
  - subject: "Re: Docs?"
    date: 2003-03-31
    body: "Dekkard, dude I'm not clueless or limited or something. I'm well aware of the API reference manuals for a long, really long time. Many of them existed for GNOME 1 already. I was more refering to GNOME developer documentation that explains the purpose of the libraries, the purpose of the functions itself and how common tasks are meant to be done e.g. avoiding deprecated functioncalls, future target of GNOME, the means of libegg, how things are being handeled e.g. which functions are being removed, which replaced and so on (These are just some examples). I know a lot of libraries from their API reference manuals and a lot of them from investigating into the library code of my own but this doesn't change the general fact that serious documentations are missing e.g. Bonobo and more. A complete set of good documentations for the programmer to work with. The same you find here on KDE or for Motif. Even if I don't necessarily speak for myself here but look it the general way for new developers on the GNOME plattform. You are getting new users (if so) but no real new developers."
    author: "oGALAXYo"
  - subject: "Re: yes"
    date: 2003-03-31
    body: "Good points.  Everyone needs to work harder and then we will find out who \"won\". :)"
    author: "anon"
  - subject: "Re: yes"
    date: 2003-04-03
    body: "> [...]who \"won\".\n\nthat's easy... that would be the terrorists."
    author: "anonymous cow herd"
  - subject: "About the tab widget part"
    date: 2003-03-31
    body: "\"Recently, Zack Rusin and Stephen Binner took the initiative to enhance the tab widget in Qt, and within days their code was already appearing in development snapshots for the upcoming Qt release.\"\n\nThis is not correct. Zack and me created some patches against the original QTabWidget implementing much requested features like buttons besides tabs, colors in tabs and close buttons within tabs. We knew that not all these would be integrated when sending the patches. So it happened: For the first part Trolltech's engineers developed a more flexible and nifty (line- and API-wise) solution. The colors in tabs were rejected because they can be implemented in a derived widget, same for the third for which Trolltech even wrote a small example proposing a different solution. In summary: No code we sent will appear in Qt but necessary changes allowing to implement these features were made."
    author: "binner"
  - subject: "Re: About the tab widget part"
    date: 2003-04-01
    body: "Well you have proved one of the points in my article!  Thank you!  The TrollTech process works.  Their developers, who are -the- experts of Qt, were able to audit your work and take what is appropriate, reject what is not appropriate, and rework what was necessary.  That's an example of an open process that works, and works well.  Congratulations!"
    author: "George Staikos"
  - subject: "Re: About the tab widget part"
    date: 2003-04-03
    body: "Well, if that's how it works, then I am the one who made Qt have styles!\n\nMy themes-for-qt-1-without-touching-qt-source-hack was so overwhelmingly evil[1], they had look into themes, or else I might release it. I think a Troll's words were something like \"that's incredibly awful\" or something like that.\n\nSo there, threats against good taste help free software, too! ;-)\n\n[1] evil in the \"perverse abuse of innocent API\" sense."
    author: "Roberto Alsina"
  - subject: "Re: About the tab widget part"
    date: 2003-04-03
    body: "I remember that code - even *you* thought it was pretty evil! ;-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: About the tab widget part"
    date: 2003-04-03
    body: "I still have a copy somewhere. I should write an archelogical article about it someday :-)"
    author: "Roberto Alsina"
  - subject: "Total Cost of Development"
    date: 2003-03-31
    body: "is the argument here.\n\nWho can forget the millions that Eazel burnt through to give GNOME Nautilus, a file manager that while pretty, is rather ho-hum compared to the competition in functionality?\n\nCompare that to Konqueror, built for almost nothing, yet far exceeding Nautilus in usefulness, especially considering its web browsing capabilities.\n\nOr compare Evolution to the forthcoming Kolab PIM application.  Millions of dollars of development went into Evolution, and it's had three years to stabilize, yet it still crashes more often than my developed-on-a-shoestring Kolab RC build, and barfs on HTML emails that embedded KHTML renders perfectly.\n\nReal-world examples show up the lie that GTK+ is cheaper to develop with."
    author: "My Left Foot"
  - subject: "Re: Total Cost of Development"
    date: 2003-03-31
    body: "And why should you care? If Ximian spends millions of money and they eventually go out of business, isn't that a GOOD thing for you?\nYou should *encourage* Evil Companies(tm) to use GTK+ so that they eventually waste up all their money and go bankrupt. Then everybody will be happy."
    author: "Me"
  - subject: "Re: Total Cost of Development"
    date: 2003-03-31
    body: "Uh, so the fact that Eazel burnt cash like there's no tomorrow is entirely due to their choice of toolkit? \n\nI think you need to check out the facts before you draw conclusions from completely unrelated starting points. Eazel were just a hopelessly mismanaged company. Need I remind you that Redhat write all their config tools using Python GTK, and they're doing alright, aren't they?"
    author: "Mike Hearn"
  - subject: "Re: Total Cost of Development"
    date: 2003-03-31
    body: "> they're doing alright, aren't they?\n\nI don't think so. Last time I heard, they were still making\na big fat \"-\" very year. Can't go on like that forever..."
    author: "KDE User"
  - subject: "Re: Total Cost of Development"
    date: 2003-03-31
    body: "They made a profit of several thousand dollars last year...\nAnd yes, that *is* a good profit, especially since we're in the middle of an IT economy crisis."
    author: "FooBar"
  - subject: "Re: Total Cost of Development"
    date: 2003-04-01
    body: "Actually, they lost many many thousand dollars last year.\nWhat you wanted to say is that they did a profit of several thousand dollars in a quarter last year. But go see the numbers for the FIRST quarter of last year, now.\n\nAnd no, that is not a good profit. Come on, *I* made a bigger profit than Red Hat, and I am not even a company ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Total Cost of Development"
    date: 2003-04-02
    body: "> And no, that is not a good profit. Come on, *I* made a bigger profit than\n> Red Hat, and I am not even a company ;-)\n\nRed Hat doesn't have mouths to feed, cars to run, holidays to take ..."
    author: "Plato"
  - subject: "Re: Total Cost of Development"
    date: 2003-04-02
    body: "Well, I have no car, only one mouth, and one holiday or two a year at most ;-)\n\nBut really, Red Hat has lost tons of money.\n\nIn Q4/2001, lost 8.57M\nIn Q1/2002, lost 4.59M\nIn Q2/2002, lost 1.95M\nIn Q3/2002, made 214K\n\nSo, last year, they lost 14.6 million dollars, not \"made a few thousands\"\nIt does show a good tendency, though.\n\nhttp://quote.fool.com/Snapshot/financials.asp?currticker=rhat&symbols=rhat\n\nSo, unless you lost 15 million dollars lately, you also made more money than Red Hat last year ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Total Cost of Development"
    date: 2003-04-03
    body: "All the other Linux distributors (including the ones who ship KDE by default_ aren't doing much better.\nHeck, even many closed source companies have lost lots money last year."
    author: "FooBar"
  - subject: "Re: Total Cost of Development"
    date: 2003-04-03
    body: "Trolltech is kicking ass and expanding.  They are sure making money and they are one of the largest contributors of Free Software.  Three cheers for Trolltech!"
    author: "anon"
  - subject: "Re: Total Cost of Development"
    date: 2003-03-31
    body: "As pointed out, they are currently hovering at \"net income\" = 0, so it's hardly big fat. Last quarter they made a small (insignificant) loss, but ramped up R&D spending by a lot, the quarter before that they made a small profit.\n\nBasically, they are indeed doing alright."
    author: "Mike Hearn"
  - subject: "Nautilus != million$"
    date: 2003-04-02
    body: "Yeah well Apple paid a lot more to buy the dead-tech that was NeXT ... \n\nNautilus as a technology at most cost a few 100K -- probably 2-3 full time developer-years max.  The rest was services development, sales and marketing and golf.  It was supposed to be a business remember?  If you remember the dot.com era you'll remember that web sites like Dr.Koop .com burned through gazillions and even more wahcky sites like fuckedcompany.com charted the fall and charged $ for membership.\n\n"
    author: "KDE Fanatic"
  - subject: "Ximan outlook?"
    date: 2003-03-31
    body: "I think Michael's sides were specific to Xiaman's OpenOffice implementation where they are looking at their software moving a different route than both open source/free software and normal commecial software.\n\nIt seems their intent is to create a non-free OpenOffice implementation (hence the need for a non-free QT if their software was based on QT) yet - unlike normal proprietory software writing - they wish 'the open source community' to contribute code to their software.  For these open source hackers to contribute to this code base they would each require a paid-for copy of QT. Hence the comment of '5000 coders'.  Naturally that would be very expensive.  \n\nMost commerical software companies pay the people writing their software, it seems that Ximain wuld prefer to get them for free, hence the QT licences being a massive proportional cost...\n\nNow, whether any open source hackers would contribute to such a project is a different matter...\n"
    author: "Adrian Bool"
  - subject: "Re: Ximan outlook?"
    date: 2003-03-31
    body: "[ It seems their intent is to create a non-free OpenOffice implementation ]\n\nEvidence please?\n\nPeople seem to forget that not all programmers who'd wish to write non-GPLd software for Linux/UNIX (maybe bsd, maybe proprietary, whatever) are paid huge amounts of cash and work in teams.\n\nI once considered writing a commercial app using Qt, cos I wanted it to work nicely on Linux and Windows, but the sheer cost meant I'd instantly be in the red. Scratch that idea."
    author: "Mike Hearn"
  - subject: "Re: Ximan outlook?"
    date: 2003-03-31
    body: "Man you are daft.  Meeks says that they want 5000+ developers in the community working on OO and Ximian stuff.  He also says that they will need commercial TT licenses.  1+1=2.  Meeks is saying that Ximian wants 5000+ _proprietary developers_.  Read the article next time."
    author: "ao"
  - subject: "Re: Ximan outlook?"
    date: 2003-03-31
    body: "Uh, I did. Meeks said he'd *like* 5000 developers, I'm sure any open source project would like that amount of manpower. \n\nBitching about the cost of Qt for non-X11 platforms is IMO justified in this case, because OpenOffice runs on more platforms than just X11 based ones. My office uses it on Windows for instance. If all developers working on it for Windows needed to pay for Qt, OO development would be shafted.\n\nI don't get why you guys are so worked up about this. There are plenty of stories posted to the dot about why KDE is so cool, why GNOME sucks, rah rah so when a GNOME developer half-seriously talks about why he thinks GNOME is cool, just take it in your stride ok? It's hardly world domination stuff."
    author: "Mike Hearn"
  - subject: "Re: Ximan outlook?"
    date: 2003-03-31
    body: "Quit apologizing!\n\nMichael Meeks said he wanted to see 5000+ developers join him and implied that Qt (KDE) wasn't up to the task because Qt required those developers to purchase proprietary licenses.  He said that KDE was less Free because it uses a GPL library which is in direct and total contradiction to the FSF and is another putrid example of crap that GNOME people will say to disparage KDE.  The slide said nothing about cross platfrom OO rather the title of the slide was 'Why GNOME on Unix' and he conveniently concluded that GNOME was the only solution and then he lists his crap FUD about Qt.\n\nWe are so worked up because it is just another in a long line of top GNOME developers spreading this ridiculous FUD that they know is patently false and repudiated by the FSF and then GNOME apologists (_you_) come here to our site and apologize for this dung.  This is not a legitimate criticism and it is horribly hypocritical.  It just shows the ethical lapses among some of the GNOME developers."
    author: "anon"
  - subject: "Programmer Productivity"
    date: 2003-03-31
    body: "I have coded two projects that had identical functionality implemented using both GTK and Qt. The number of lines of GTK specific code was 20% and 40% greater than the number of lines of Qt specific code. Applications, and their related costs, aren't finished at the end of development - maintenance costs are significant. While the law of diminishing returns applies (for small developments), the larger a development is, the stronger the direct relationship between total costs and lines of code becomes.\n\nIn other words, the question is how many programmers do I need if I use Qt, and how many do I need if I use GTK. For a serious commercial application (where development costs, ie. programmer time, have to be recovered) the difference is significant and certainly greater than 1. (For smaller applications, or for open source projects where the development costs do not have to be recovered, it's less of an issue.)\n\nReducing the number of programmers required has benefits other than the reduction of the salary bill. Smaller teams are more productive, per person, than larger teams. The major cost of team management is communication with and between team members. This increases non-linearly with the size of the team.\n\nIn short, paying for Qt licenses saves you money. This is a statement of the bleeding obvious, otherwise Trolltech wouldn't be in business. It's also the reason why companies are willing to pay out even more money on things like PyQt because it provides their programmers with yet another productivity boost.\n\nPhil"
    author: "Phil Thompson"
  - subject: "Re: Programmer Productivity"
    date: 2003-03-31
    body: "This is a somewhat dangerous argumentation, since proponents of proprietary software (in particular Microsoft) do the same: Windows is more expensive because it makes you more productive..."
    author: "AC"
  - subject: "Re: Programmer Productivity"
    date: 2003-03-31
    body: "It is very simple:\nThe Microsoft argument is a lie. Windows is expensive compared to  the productivity you gain... You are less productive a a high price level (development costs _and_ license costs)\n\nWith Trolltech's Qt Toolkit you _can_ be very productive at a moderate price-level (no need to lie here)..\n\nWith GTk you can be productive at a moderate price level (please take development costs into account, not only the cost of the license)\n\nThis is the status at the moment. Maybe it changes in the future, who knows..."
    author: "Thomas"
  - subject: "Re: Programmer Productivity"
    date: 2003-03-31
    body: "In a way this is a matter of open vs. Proprietary. Here we have Qt which is free when used to write free software. And you have to pay if you use it to write closed software. So it encourages people to write Free software instead of proprietary software. I fail to see the problem.\n\nObviously, people can use Gtk and Gnome to push their proprietary software. And that is a good thing because....??\n\nOnly people who whine about Qt and it's licencing are the ones who want to write closed and un-free software. They want to use Qt for free so they themselves could profit from using it. They want to profit from their software, but they obviously think that others (in this case Trolltech) don't have the right to profit from their work (Qt in this case).\n\n\"Do as I say, not as I do!\""
    author: "Janne"
  - subject: "Re: Programmer Productivity"
    date: 2003-04-01
    body: "What about open software on Windows? OpenOffice on Windows is very usefull. There is no GPL edition of QT for Windows :-(((."
    author: "Petr Balas"
  - subject: "Re: Programmer Productivity"
    date: 2003-04-01
    body: "But there is a free-beer version for Windows that let's you use an Open Source license."
    author: "David Johnson"
  - subject: "Re: Programmer Productivity"
    date: 2003-04-04
    body: "Trolltech could start charging for that. free!=Open-source. Not equivalent too. Why no GPL. All it takes is a decision."
    author: "Maynard"
  - subject: "Re: Programmer Productivity"
    date: 2003-03-31
    body: "The proponents of any software (open source or proprietary) will say it makes you more productive - that's the whole point of software. The judgement call is between the cost vs. benefit of different packages that claim to do a similar job.\n\nAll I'm saying is that productivity benefits (and associated cost savings) of Qt easily outweigh the cost of the license."
    author: "Phil Thompson"
  - subject: "Re: Programmer Productivity"
    date: 2003-04-01
    body: "Up till recently there hasn't been any RAD environment comparable to delphi or vb, for quick bang-off database front end jobs. It was more expensive to take a free tool, write data io routines, compared to paying for something that had it built in.\n\nThis is one example of the cost/benefit analysis that needs to be done for a commercial project.\n\nThe goalposts are changing quickly however.\n\nThis whole issue is moot if a person writes and releases gpl software. Either gtk or qt allows that possibility. Microsoft, among others, doesn't.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Programmer Productivity"
    date: 2003-03-31
    body: "Did you use the same language for each? Did you use Glade in GTK and Qt Designer in Qt?"
    author: "Mike Hearn"
  - subject: "Re: Programmer Productivity"
    date: 2003-03-31
    body: "Your comment is right on target.\n\nCommercial Qt subsidizes GPL\u00b4d Qt. Eventually, GPL\u00b4d Qt applications \nwill displace  commercial Qt ones. I also fail to see the problem.\n\nCheers,\n\nCarlos Cesar"
    author: "Carlos Ceasr"
  - subject: "Comparison..."
    date: 2003-03-31
    body: "I programmed quite a lot with glade and gtk, which made a simple program which\njust needed an simple interface pretty easy... 2h to get a frontend.\n\nRecently i had taken a look at qt and the widgets and the signal/slot model\nseems apealing. \n\nSo from a technical point of view qt seems more advanced although i don't have seen \nthe newer gnome 2.0 libraries. \n\nas long as qt is free to develop on my favorite platform with gpl'd sources i don't really\ncare if there is money involved on the windows side. (i think windows existence is\nmainly based on money imho anyways).\n\nOT: The main reason that i didn't start with qt/kde was that kdevelop was not available\nas easy as glide on the debian platform and there was no vi kpart for the editor.\nIs the kvim part finished btw and available as debian package?"
    author: "Tim"
  - subject: "Re: Comparison..."
    date: 2003-04-01
    body: "try this:\n\ndeb http://ktown.kde.org/~nolden/debian/kde woody main\n\nYou'll get the latest KDE and kdevelop from there for woody. If you use unstable you're fine there too by just using the debian mirrors.\n\nRalf"
    author: "Ralf Nolden"
  - subject: "he doesnt like gnome either"
    date: 2003-03-31
    body: "Read on:\n<a href=\"http://mail.gnome.org/archives/desktop-devel-list/2003-February/msg01043.html\">msg01043.html</a>\ntill\n<p>\n<a href=\"http://mail.gnome.org/archives/desktop-devel-list/2003-March/msg00042.html\">msg00042.htm</a> from michael meeks:\n\"...Ultimately D/BUS doesn't matter 1 tiny piece. What really matters is\nthat Gnome has no conflict resolution process, nor shared vision, nor\nway of articulating one - this means that people can abuse their\nposition ( as a distributor, maintainer of core components etc. ) to\nachieve their particular goals. It also leads to a gladitorial\ndevelopment where people waste vast amounts of time and effort.\"\n<p>\nand eventually go to the famous (as in slashdot) post:\n<a href=\"http://mail.gnome.org/archives/desktop-devel-list/2003-March/msg00026.html\">msg00026.html</a> from mdi:<p>\n\"Reading today's Slashdot comments, you can see that our desktop is\nfalling behind stability-wise and feature wise to KDE.  It is of course\nyour time that is being spent on D-Bus, and maybe you do not have a lot\nof choice on what you get to spend your working time on, but end-users\nwhich do not care about the royalty issue do feel that KDE is a better\ndesktop.\" \n<p>\nThose ximian people are really messing things up since they got those 15 000 000$.  \nCan you say mono ?"
    author: "jay say say"
  - subject: "Re: he doesnt like gnome either"
    date: 2003-04-04
    body: "How insightful, you take two peoples comments totally out of context, and made no real conclusion. Yes I can say mono, whats your point? That it is a more standard language than Java? Can you say Java? Are you a developer, do you know anyhting about C# other than it was developed at microsoft? Maybe I should take your comments out of context and prove you are a moron.\n\nSheesh,\nRyan"
    author: "Ryan"
  - subject: "The best part"
    date: 2003-03-31
    body: "> $26,000 is probably close to the amount of money spent on free t-shirts and stuffed monkeys distributed at trade shows by software companies.\n\n:-)"
    author: "Anonymous"
  - subject: "Why Qt?"
    date: 2003-03-31
    body: "Does he even know why Qt was created?\n\nIt is made to speed up the development process.\nAnd it does so probably much better thatn Gtk.\n\nConclusions..."
    author: "KDE User"
  - subject: "not just GPL"
    date: 2003-03-31
    body: "there are other Open Source licensing choices besides the GPL that are compatible with FreeQt, though George's article doesn't say as much. such choices include the LGPL as well as BSD and MIT style licenses.\n\nas for personal experience, i have found that Qt does make a difference and is well worth the licenses. developers can work faster and worry about fewer things. from Unicode to i18n to collection classes to networking to databases to .... it's all handled in a clean, consistent and reliable manner.\n\none of my favourite moments was when helping a client decide whether or not to go with Qt for their application and i drew up a fully functioning dialog with a table and navigation buttons that connected to their Oracle database in Qt Designer in under a minute and previewed it live for them. jaws dropped open around the table and they never looked back.\n\nwith the KDE layer on top, it's even sweeter. fully functioning applications with rich and consistent interfaces can be brought to life in a matter of hours. on larger projects that means you can concentrate on what the program does that is unique rather than (poorly) reimplementing all the details ..."
    author: "Aaron J. Seigo"
  - subject: "Re: not just GPL"
    date: 2003-03-31
    body: "Really, then why don't Eclipse and wxWindows target QT/KDE?  "
    author: "Anonymous"
  - subject: "Re: not just GPL"
    date: 2003-03-31
    body: "I can tell you why wxWindows doesn't taget Qt: I said I would try to do it, but then decided it was not worth the effort. The idea of a WxQt was considered nice, and welcomed by the WxWindows community.\n\nAbout Eclipse, I don't really know, you would have to ask someone at IBM."
    author: "Roberto Alsina"
  - subject: "Re: not just GPL"
    date: 2003-04-01
    body: "Thanks for the info.  I don't have the URL handy, but I recall recently reading an interview from a wx guy who gave me the impression that a QT target was something of a foggy area, such that it was desirable to be avoided.\n\nI wouldn't mind being mistaken, but I've heard that from a bunch of other sources too.  I don't suppose you'd like to comment on that sort of thing?\n"
    author: "Anonymous"
  - subject: "Re: not just GPL"
    date: 2003-04-01
    body: "I don't see how I could comment on such a thing unless you had some sort of reference for what you say.\n\nSo, no, I wouldn't like to comment about how you (whoever that is) recalls reading an interview with \"a guy\" that gave you the impression that \"a QT target was a foggy area\", and got that from \"a bunch of other sources, too\".\n\nGet serious, then ask."
    author: "Roberto Alsina"
  - subject: "Re: not just GPL"
    date: 2003-04-02
    body: "http://www.fosdem.org/index/interviews/interviews_smart\n\nHere, Julian Smart says:\n\nAh, wxQt - the case of the missing port. It comes up from time to time on the mailing list but no-one has yet been sufficiently motivated to start the port. The fact that it would be a fully-featured C++ toolkit on top of another fully-featured C++ toolkit could indeed be a disadvantage but there's another problem: legality. TrollTech have never clarified whether they would permit a wxQt port, despite a fair number of emails. I can't imagine they'd be wild about it! \n\n\nThere is \"a guy\" who I think sounds \"serious.\"\n"
    author: "Anonymous"
  - subject: "Re: not just GPL"
    date: 2003-04-02
    body: "Trolltech don't have to \"permit\" anything as long as the licenses are fine."
    author: "anon"
  - subject: "Re: not just GPL"
    date: 2003-04-02
    body: "Ok, I now can reply about it ;-)\n\nIt is obviously legal, since the licenses are just fine.\nWhat would be more dubious is who exactly can USE the port to develop a program, and that difficulty is mainly caused by the GPL being a mess.\n\nWhether the trolls would be happy or not, I bet on unhappy, but it doesn't matter all that much. I can imagine that they didn't want to encourtage it by saying \"it's OK\", too.\n\nThe reason why I didn't build the port is that it was basically painful. Not that it was specially hard but:\n\na) At the time there was no full spec on the WxWindows API.\nb) WxWindows didn't use the same public headers on all versions, so all ports were subtly incompatible, and starting a new one was hard (which one would it be compatible with?)\nc) Basically the goal would have been to wrap Qt into a less convenient API. And I was not going to use it, so why bother?\n\na) and b) are not so much of a problem nowadays, because WxWindows API is more cleanly defined.\n\nc), however, is as much of a problem as ever. You need a guy that knows Qt well, and knows Wx well, and prefers using Wx. I only fitted one of those criteria,"
    author: "Roberto Alsina"
  - subject: "Re: not just GPL"
    date: 2003-04-02
    body: "Julian Smart obviously doesn't know how the GPL works!"
    author: "meh"
  - subject: "Re: not just GPL"
    date: 2003-04-02
    body: "> impression that a QT target was something of a foggy area, such that it was desirable to be avoided.\n \nROFL.. people who still believe that Qt is under some non-GPL or GPL compatible license."
    author: "meh"
  - subject: "Re: not just GPL"
    date: 2003-04-01
    body: "Maybe because the idea of targeting a crossplatform toolkit to another crossplatform toolkit is just strange."
    author: "David Johnson"
  - subject: "Re: not just GPL"
    date: 2003-04-02
    body: "Yup, I agree.  But I was referring to a legal issue."
    author: "Anonymous"
  - subject: "Re: not just GPL"
    date: 2003-04-02
    body: "It's simple if wxQt is closed source, no you can't.  If it's Open Source then it's fine.  How hard is that to understand?"
    author: "anon"
  - subject: "I am a..."
    date: 2003-03-31
    body: "...well maybe i've missed something, but as far as i recall JFK was not in Hamburg but in Berlin when saying the famous words \"Ich bin ein Berliner\". *g*\n\nhttp://www.gnome.org/~michael/XimianOOo/img3.html\n\nHmmm, maybe michal wanted to say something else... \"I am a hamburger...\"... c'mon, BigMac..."
    author: "Franz Keferboeck"
  - subject: "Re: I am a..."
    date: 2003-03-31
    body: "Well I am a Hamburger.. thus I know that in ancient past there has been a famous Company called Star Division (small team, some coders) over here in Hamburg. The people of this company had the stupid idea to compete directly with Microsoft and to come up with an alternative Office Suite. (big laugh..)... Well, see what came out of this?"
    author: "Thomas"
  - subject: "Re: I am a..."
    date: 2003-04-09
    body: "What JFK actually said was \" I am a jelly donut.\" : ein Berliner = a jelly donut\nWhat he meant to say was \" Ich bin Berliner. \""
    author: "Ron Gesell"
  - subject: "Re: I am a..."
    date: 2003-11-16
    body: "\"Ich bin Berliner\" means you are actually from Berlin. Kennedy did not mean to say that, and it would have been ridiculous to have done so, given his thick Bahston accent. He meant to say that he was a citizen of Berlin--an expression of unity. It's like the difference between saying \"I am American\" and \"I am an American.\" It's a subtle difference, but it was not lost on the crowd. I can assure you that no one in that crowd thought he was claiming to be a jelly donut, any more than you would think I was popular literary magazine if I told you I was a New Yorker."
    author: "Dave"
  - subject: "about pricing..."
    date: 2003-03-31
    body: "I was a Gnome users, now I use KDE, because since KDE 3.1 I think it is really bettere than gnome 2.2. However QT Pricing are very HIGH!!! Ex: I'd like to develop a little program for a little society and I like to sell it for 100 $.\nThe cheapest QT license is 1500$, while GTK is lgpl.\nThat's all.\n\nBye\nMic"
    author: "Michele Costantino Soccio"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "There is notting stopping you from writung a GPL program and selling it for $100. You just have to provide the source and nothing stops whoever buys it to freely redistribute, but you can do it. "
    author: "Morty"
  - subject: "Re: about pricing..."
    date: 2003-04-01
    body: "Thanks for trying to set the record straight, Morty.  So few people see that \"proprietary software\"!=\"commercial software\".  You can create commercial software using the free version of Qt and sell it for as much money as you see fit.  You just have to release it under the GPL.  Under the GPL, you don't even have to provide the source until your customers request it."
    author: "ac"
  - subject: "Re: about pricing..."
    date: 2003-04-02
    body: "The problem is that with the GPL, selling it for $100, when someone can take the source, compile it and redistribute the binaries becomes rather hard. I would not like to take the risk that someone won't think of it.\n\nSorry, but commercial *IS* difficult with the GPL. GPL wants you to make money of services, which is not yet a very viable business model. Only Redhat seems to be making any money of it, and that is because they have huge corporate customers."
    author: "The Problem"
  - subject: "Re: about pricing..."
    date: 2003-04-04
    body: "While it's true that generally you can develop commercial applications licensed under the GPL, you can _NOT_ develop commercial QT applications without buying a QT license.  Period.  Read Trolltech's license, it's dual GPL/QPL.\n\nTheir license states very clearly that you can not develop a commercial application using the free toolkit.  It does not matter if you release the source, license under the GPL, or whatever, no commercial application develop can be done with the free/GPL'd QT."
    author: "ch"
  - subject: "Re: about pricing..."
    date: 2003-04-04
    body: "that's nonsense, read the QPL again"
    author: "domi"
  - subject: "Re: about pricing..."
    date: 2003-04-04
    body: "Or the GPL, for that matter."
    author: "Roberto Alsina"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "This argument is daft. \n\nThere is a difference between good debt and bad debt. Qt is good debt, because it can be recovered and will soon be an asset.\n\nIf however, you simply can't purchase a license, do the following:\n\n1. Develop the app. and sell 15 copies (or however many is necessary to cover sundry costs, living costs etc.) under GPL (as already mentioned).\n2. Buy the Qt license.\n3. Release a new version of the app (even rename it if you so please) with a closed license and tell your existing clients that the version they currently possess will no longer be supported or maintained.\n4. Offer a no-cost upgrade option to your existing customers with an added bonus that they get one free upgrade (or whatever carrot you want to use).\n5. Sorted :)\n\nI have downloaded software that comes bundled with source before. As I have no intention of reinventing someone elses baby, I leave the code alone because the applications work. Do you really think your clients will immediately fork your code and go into direct competition with you?\n\nIf you really want to buy QT, this is just one example solution for you. \n\nIt really isn't that difficult when you compare it to the future gains of developing with Qt.\n\nCheers.\n3.1rOXtHEcASBAH"
    author: "3.1rOXtHEcASBAH"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "I thought you couldn't do that - if you want to use the edition that lets you do proprietary software, you have to use that edition from the start, ie you can't develop for free under the GPL then switch later.\n\nAt any rate, last time I checked that was made pretty clear on the TrollTech website. Perhaps it's changed."
    author: "Mike Hearn"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "Mike, can you point to the URL where that is stated.\n\nI was not aware of that.\n\nThank you.\n\n3.1rOXtHEcASBAH"
    author: "3.1rOXtHEcASBAH"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "Hmmm, TrollTech redesigned their site, and the rather nice Qt licensing FAQ seems to have disappeared. Here's the version they used to have up:\n\nhttp://web.archive.org/web/20020212103453/www.trolltech.com/developer/faqs/general.html?cr=1\n\nI was thinking of this one:\n\n\n\nCan we use the Free Edition while developing our non-free application and then purchase commercial licenses when we start to sell it?\n\nNo. The Free Edition license applies to the development phase - anything developed without Professional or Enterprise Edition licenses must be released as free/open source software.\n\n\n\nHmm, rereading your original post, maybe that's what you meant, or maybe I didn't understand the original FAQ question. I'm not sure whether releasing a new version with a new name would let you develop using the GPLd version or not. Possibly that'd be the case, as the old software would still be there GPLd regardless. \"Must be released\" seems a tad vague, does it apply in the future? Dunno, IANAL and all that. Perhaps you were right."
    author: "Mike Hearn"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "No, no.  If you release software as GPL you can't take back the rights.  However, you always have the rights to RELICENSE your own software however you want."
    author: "anon"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "Sure, I'm aware of that. That doesn't imply you still have permission to link against against Qt if you do so.\n\nThe confusing thing about this is that the wording of the FAQ implies that you can't develop your software as GPL then switch it to commercial software later, but clearly you can relicense your own software, and at that point I don't see why you couldn't just buy a Qt license. On the other hand, the FAQ says you can't develop as GPL then buy a commercial license, it has to be commercially licensed from the start.\n\nOw. My head hurts. IANAL and all that jazz."
    author: "Mike Hearn"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "\"Can we use the Free Edition while developing our non-free application and then purchase commercial licenses when we start to sell it?\n\nNo. The Free Edition license applies to the development phase - anything developed without Professional or Enterprise Edition licenses must be released as free/open source software.\"\n\n\nThis does not appear to relate to our discussion. Our discussion is about selling GPL software, and at a later stage selling non-GPL software.\n\nThanx Mike for the link.\n\nTo be safe, give your Generation1 software a name that you know will not be used in the longrun, like FooBarTastic2000 (got that from kde-look.org - Aqua icon set :) )\n\nSell Gen1 software to cover the license.\n\nBuy the Qt licence. \n\nStop development of Gen1.\n\nRelease Gen2, with it's intended identity and with a closed license (no re-licensing involved).\n\nAllow all your customers to upgrade for free.\n\nCan anyone see a problem with this point of view?\n\nThank you.\n3.1rOXtHEcASBAH"
    author: "3.1rOXtHEcASBAH"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "Well, I thought the original discussion was about how to get over the hurdle of paying for a Qt license when you are poor and have no money by releasing it as GPL first :)\n\nOn the view, no real problem I guess, but it sounds like it's dancing around the actual licensing, playing games with naming and such doesn't seem honourable - following the letter not the spirit of the license. I'm pretty sure TrollTech want paying if you're going to do it commercially some day.\n\nWow, their new website is pretty confusing, but I found the FAQ:\n\nhttp://www.trolltech.com/developer/faqs/free.html#q3\n\nI still don't \"get\" how you can write BSD/MIT software when linking to a GPL lib, but IANAL so I'm sure it's all proper, dual licensing confuses me any day. "
    author: "Mike Hearn"
  - subject: "Re: about pricing..."
    date: 2003-04-01
    body: "----\n I still don't \"get\" how you can write BSD/MIT software when linking to a GPL lib, but IANAL so I'm sure it's all proper, dual licensing confuses me any day. \n----\n\nOk, here is the 5-cent FSF-style explanation:\n\nFirst forget about the distinction between application and library. None is mentioned in the GPL, so they don't matter. What matters is the definition of \"work\" and both are works.\n\nYou must keep in mind that writing code is always ok. The GPL doesn't cover writing anything, it is only about distributing and copying. So, you can write whatever you want, but can you distribute it?\n\nYou can distribute a work that contains another (like an applicatoin containing a library), if the license for each piece of the work allows the distribution.\n\nThe GPL allows such distribution as long as all pieces of the larger work are distributed \"under  the terms of the GPL\". That is not the same as \"under the GPL\". It means that you must be able to do all the things the GPL lets you do (the GPL doesn't forbid you from doing anything, it only doesn't specifically allow some things).\n\nThe BSD license lets you do everything the GPL lets you do, and a lot more.\n\nSo, you can distribute your BSD app with the GPLd library.\n\n<mode=BSD advocate>\nThe good thing about doing that is that, in the future, someone may take the BSD code, and fully liberate it by removing the tainted GPLd code, making the whole app really free\n</mode>\n\n"
    author: "Roberto Alsina"
  - subject: "Re: about pricing..."
    date: 2003-04-01
    body: "And of course, since Qt is also QPL, you can use licenses like the Artistic as well.\n"
    author: "Sad Eagle"
  - subject: "Re: about pricing..."
    date: 2003-03-31
    body: "So you think that TT should offer their product to you for free, so you could write closed software for profit with it? Why do you have the right to write for-profit software but TT does not?\n\nBesides, you can sell GPL'ed software just fine. It's free as in speech, not free as in beer."
    author: "Janne"
  - subject: "Re: about pricing..."
    date: 2003-08-19
    body: "He never said free (beer).\n\n\nI agree with him. The licensing for QT is outlandish, when you consider that small software companies/startups would find it attractive since it is very clean but could never afford it.\n\nAltho I realise you can sell GPL software, the distinction is that it is difficult to make the investment in developing software when the resulting sales are undeterminable, since you dont know what customer #1 may do with your source.\n\nIt is also difficult to make the commitment to learn and use QT before you own a commercial license. While the idea of QT is great (you can use it to develop both free and non-free programs), the fact is the licensing is several orders of magnitude too rich. $500US for a single developer license I could stomach. Perhaps even if it was $500US per year. But several thousand dollars outlay for a one-man show, when you can use LGPL GTK (with C++ bindings) for $0 outlay and keep your resulting binaries closed until youve made your money, at which time you can relicense to GPL anyway.\n\nObviously GTK isnt as 'nice' as QT, but at the end of the day it may be preferable over QT when you consider that you may never be able to afford to make and sell closed source software with it as a small entity.\n"
    author: "Simon Scott"
  - subject: "Re: about pricing..."
    date: 2003-08-19
    body: "There is not fixed pricing, is has been mentioned often enough that Trolltech welcomes individuals to negotiate the exact pricing. Considering this it pretty foolish to keep stating that the poor developers and poor small software companies/startups would never be able to afford Qt."
    author: "Datschge"
  - subject: "even the editors are trolling"
    date: 2003-03-31
    body: "I'm used to seeing GNOME/KDE flames in user comments here on dot.kde, over at footnotes and of course at /. That's just the way things are and there's little to  do about it except trying to ignore it. I'm not used to seeing it on article level though. I expected *a lot more* from the dot.kde guys. \n\nLet me start out by saying that i agree with Georges article. License cost is a small fraction of developer cost in a commercial setting. No argument there. I would also like to point out that i have nothing against KDE, it's great software and one of the free software worlds best managed projects.\n\nThe article references a posting here on dot.kde (the \"further claims\" link) as an argument how GTK is controlled by an evil company (red hat).\nHe's arguments is (when the trolling and actual factual errors is filtered out):\n\n* Some people got pissed when GNOME changed direction.\nChanging direction in a large project like gnome always pisses some users off, and pleases some others. Does this mean that changing direction is bad? It is argued that this change in direction was coming \"from above\" (implying red hat) instead of \"from the users\". In reality, it came from \"the guys doing the actual coding\". If you contribute lots of good code to a project you have a lot of say in it's development. This is true for GNOME, KDE and most other large projects.\n\n* There was a controversy two years ago between one developer and the rest of the team. \nHappens all the time in large teams. (Can you say mosfet?)\n\nHe also claims gconf is registry-like (it's not!).  He distorts some statements from gnome developers that \"the user should not have to know what a window manager is to use the system effectively\" to \"the user doesn't know what a window manager is\" which is quite a different thing. There's even more lies and errors in that posting, but i don't have the time or the energy to go into furhter detail.\n\nThe article poster (this article, not the troll posting referenced by it) uses this (along with some tabwidget anecdote that turned out to be false, se comment in this thread) to argue how GTK is closed and Qt is open.\nExcuse me, but for a project to be open (in the \"community-developed\" sense) it has to fulfill two simple requirements:\n* Most discussions about the development of the software takes place on one or several mailing lists.\n* It has to be a meritochracy, meaning the people submitting the most code gets to be in on the big descisions, gets to subscribe to evil private mailing lists and so on.\n\nGTK qualifies, so does KDE but Qt clearly doesn't. Maybe Qt-copy does too, but KDE does not depend on that so it's irrelevant. Note that some GNU software doesn't qualify either so \"community-developed\" does not imply \"free software\" or the other way around. \n\n"
    author: "Fredrik"
  - subject: "Re: even the editors are trolling"
    date: 2003-03-31
    body: "I wrote a letter to Navindra last night about this very issue.\nNavindra claims 2 things\na) The KDE version of this Armegeddon is deleted on sight\nb) The GNOME version does not belong here.\n\nBoth of these things I agree with, however the policy here seems to be \"Delete the KDE troll, but leave the antiGNOME one intact, and in fact even reference it as \"other sources of information\".\n\nI usually accept the argument that \"KDE developers don't flame GNOME, and GNOME developers don't flame KDE, its just assholes flaming both sides\", but when the editor of the official KDE news site starts flaming, then there's something to be worried about."
    author: "Iain"
  - subject: "Re: even the editors are trolling"
    date: 2003-03-31
    body: "Hey, actually you did write me according to my procmail logs.  Annoyingly enough I can't seem to find your message in my mailbox.  I hadn't noticed it at all.  Argh...  I hate losing mail. I'm checking my backups right now (I just found it in msg.XsQC).\n\nYou know what?  I would have deleted that article had I caught it much sooner.  Unfortunately when I found it a ton of people were discussing it -- deleting it at that point just wasn't right and would have trashed all the responses. Why do we now delete the \"KDE version\" on sight?  Because it's been posted over and over and over...  That's just abuse.  The \"KDE version\" is actually nothing like GNOME Armageddon, btw.  I was wrong about that.  The \"KDE version\" I was thinking about was the one called \"KDE Myths\" although I'm sure someone can write a KDE Armageddon troll too.\n\nWhy did I link it?  Actually, you know what?  I'm removing the link.  It's less illustrative than I first thought it was, last night when I wrote my article."
    author: "Navindra Umanee"
  - subject: "Re: even the editors are trolling"
    date: 2003-03-31
    body: "Thank you very much."
    author: "Iain"
  - subject: "Re: even the editors are trolling"
    date: 2003-03-31
    body: "I also replied to your mail and snipped the Armageddon post...  people have read and replied to it to their heart's content already."
    author: "Navindra Umanee"
  - subject: "KDE has kdelibs but GNOME throws gnomelibs away"
    date: 2003-03-31
    body: "KDE depends on kdelibs in which they can pretty much override or extend anything they dont like in Qt.  GNOME is attached to GTK and is throwing away gnomelibs...  this is causing no end of pain like the never appearing improvements to the fileselector and toolbars.\n\nWhats my point?  The C++ architecture lets you build on top of Qt and extend it if you need to.  So the closed development of Qt becomes much less of a problem. GNOME says GTK development is open and yet GNOME is more restricted than KDE because they have an unwillingness to maintain gnomelibs."
    author: "anon"
  - subject: "Re: KDE has kdelibs but GNOME throws gnomelibs awa"
    date: 2003-03-31
    body: "Well GNOME did this as well for a long time (extending GTK objects) but Havoc in particular wants to eliminate that practice, because it leads to unnecessary bloat. There's a reason GNOME2 is so much smaller and faster than GNOME1, and it's in a large part because lots of extensions were removed.\n\nYes, with object systems the duplication is less because of inheritance (both QObject and GObject support this of course), but it's still noticeable.\n\nThe idea is (i think) that the platform becomes entirely neutral and standards based, as opposed to GnomeThis and GnomeThat. An admirable goal, if it's achievable IMO. It'll be a looooong time before that happens though.\n\nUntil then moving things into GTK makes sense, there's no point having GtkFoo and GnomeFoo (or QFoo and KFoo) when they're both open source projects."
    author: "Mike Hearn"
  - subject: "clarifications"
    date: 2003-03-31
    body: "> argument how GTK is controlled by an evil company (red hat).\n\nNote that I didn't link the article to show how \"Red Hat\" specifically influences GNOME but as a source of links that illustrates the influences behind GNOME development process and that control the direction of the project.\n\n> some tabwidget anecdote that turned out to be false\n\nYou are right that this particular example of the tab widget isn't spot on but it isn't all negative from binner's post.  Of course other patches for Qt have been known to be accepted, but you are correct that Qt is not community-developed.  \n\nI think the importance of that is overstated when even Ximian (and Eazel) had to develop their own libraries to supplement GTK/GNOME functionality (libeel and libgal I think...  two different libraries developed by two different companies that try to solve many of the same problems)."
    author: "Navindra Umanee"
  - subject: "Re: clarifications"
    date: 2003-03-31
    body: "> I think the importance of that is overstated when even Ximian (and Eazel) had to \n> develop their own libraries to supplement GTK/GNOME functionality (libeel and \n> libgal I think... two different libraries developed by two different companies \n> that try to solve many of the same problems).\n\nlibgal and libeel didn't solve the \"same problems\". They were simply utility libraries, collections of useful functions that were originally in Evolution and Nautilus respectively, but were moved into a seperate library for other programs to use.\n\nThe problem is that libraries that are part of the main desktop of GNOME (and I would assume KDE as well) need to remain API stable, so you don't want to be adding half implemented or half finished APIs. This is why libeel and libgal existed, so that people could use them, but they understood that the API might change."
    author: "Iain"
  - subject: "Re: clarifications"
    date: 2003-03-31
    body: "This is exactly my point.  Eazel or Ximian won't go changing the base GTK libraries willy-nilly, and they don't.  Instead they will come up with things like libgal and libeel which is comparable to what KDE does with kdelibs...  that is, KDE avoids changing Qt but rather extends it or adds the functionality it needs."
    author: "Navindra Umanee"
  - subject: "Re: clarifications"
    date: 2003-04-01
    body: "Ah, Sorry, I misunderstood your point.\nI thought you were indicating that libgal and libeel solved the same programming problems (ie they had similar contents), and that from this you were drawing out the conclusion that it was harder to get stuff into gTK.\n\nMy bad, as the americans say"
    author: "Iain"
  - subject: "Re: even the editors are trolling"
    date: 2003-03-31
    body: "> GTK qualifies, so does KDE but Qt clearly doesn't. Maybe Qt-copy does too, but KDE does not depend on that so it's irrelevant. Note that some GNU software doesn't qualify either so \"community-developed\" does not imply \"free software\" or the other way around. \n \n Qt doesn't need to for KDE as much as GTK needs to for GNOME, as KDE apps can pretty much override everything in Qt (easier than the OO in C approach in GTK at least!)"
    author: "lit"
  - subject: "Re: even the editors are trolling"
    date: 2003-03-31
    body: "\"Qt doesn't need to for KDE as much as GTK needs to for GNOME, as KDE apps can pretty much override everything in Qt (easier than the OO in C approach in GTK at least!)\"\n\nNot really, no. They can't really extend qt itself transparently. What they are doing is subclassing Qt classes. That's why we have KApplication instead of QApplication etc. But the applications must do this explicitly (unless you use a LD_PRELOAD hack but C programs can use them as well).\nGTK can do this as well AFAIK, it's one of the basic features of object systems.\n\n"
    author: "Fredrik"
  - subject: "Re: even the editors are trolling"
    date: 2003-04-02
    body: "Oh, well, we could if we hacked qt-copy while maintaining binary compatibility.\n\nI know, I know, not what you had in mind ;-)"
    author: "Roberto Alsina"
  - subject: "Re: even the editors are trolling"
    date: 2003-04-01
    body: "The tab widget example was not false.  It was an unexpected double example of how Qt developers audit all changes going into Qt, and implement them -right-.  They clearly didn't agree with the change as it was submitted, but they had a better idea of how to accomplish the same goal.  I think it was a shining example."
    author: "George Staikos"
  - subject: "Lost track of the REAL enemy"
    date: 2003-03-31
    body: "While you KDE guys are wasting your time whining about how GNOME is evil, Microsoft is getting a greater and greater advantage over you. If this keeps up, one day Microsoft will kill off the entire open source community, KDE included.\n\nOpen your eyes people! Microsoft is the *real* enemy here! Instead of spreading anti-GNOME propaganda, write code and make KDE/Linux/whatever BETTER than Windows XP! If you don't, you will eventually be destroyed by your own blindness."
    author: "FooBar"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "Although you show us your tendency to overreact, and MS isn't gaining any more momentum than they already have (imho), I must respond to the stop-flaming, start-cooperating part with: \"Hear hear!\" \n\nSomeone should also say this to 'certain'-Gnome devvers though :)"
    author: "wzzrd"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-01
    body: "\"Someone should also say this to 'certain'-Gnome devvers though :)\"\n\nLet me know who and I'll tell them next time I see them."
    author: "Iain"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "I agree, but I don't think this can be stopped. The community is strong, but all these artists, coders, translators, idea-contributors.... can simply not compete with a big global player like MS... We always think the Open Source community is enthusiastic about what 'we' all have reached 'together'...\n\nQuestion: Have you ever seen a real Ms employee, how enthusiastic these people are? That's far beyond imagination. From my point of view I always think they have left earth long ago and living on Mars already. The thing is: they really _believe_ in Microsoft (and the companys politics) much more than the OO community believes in itself. This (of course) makes Ms an unbeatable competitor.\n\nMS: thousands of people having exactly the same idea\n\nOpenSource: thousands of individuals having different (but nevertheless bright) ideas working somehow together with a lot of 'friction losses'"
    author: "Thomas"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "having thousands of people blindly buying into whatever the party line is only works if the party line is infallible. if, however, there is room for exploration, more than one possible answer and competition is actually a benefit then such narrow minded views are not a benefit. rather, they are a liability. the fact that in the Open Source world there are hundreds of active, well populated and indeed thriving individual communities all exploring their own visions is one of the things that makes it so strong and impervious to attack and even internal mistakes."
    author: "Aaron J. Seigo"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "So GNOME-folks can whine how TT/KDE is evil and sucks, but KDE-folks can't provide any counterarguments?"
    author: "Janne"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "The GNOME folks don't whine about how TT/KDE is evil. Find me an anti-GNOME article that is less than 6 months old from an official GNOME site. I dare you.\n\nNotice the \"official GNOME site\" part. NOT a random post from a single individual!"
    author: "FooBar"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "The REAL enemy is not KDE or GNOME, it's YOU.\n\nThis article you are replying to is nothing official.  It's from single individuals.  Just like Michael Meek's slides to THOUSANDS of OOo developers.  Or should I say Ximian?"
    author: "anon"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "> \"The REAL enemy is not KDE or GNOME, it's YOU.\"\n\nKeep Yast Online Update out of this!"
    author: "FooBar"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "> Find me an anti-GNOME article that is less than 6 months old from an official GNOME site.\n\nYou want to say that GNOME is missing self-criticism? Agreed."
    author: "Anonymous"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "Err.... typo. o_0\n\n2nd try:\nFind me an anti-KDE article that is less than 6 months old from an official GNOME site."
    author: "FooBar"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-01
    body: "http://www.gnome.org/~michael/XimianOOo/img7.html"
    author: "lit"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "Ummmmm... How about the slides everyone is talking about here? How GNOME is \"free\", while QT/KDE is \"less so\"? For reminder, here is the url:\n\nhttp://www.gnome.org/~michael/XimianOOo/img7.html\n\nWhat's that website? www.gnome.org? That's what I thought....\n\nAnd are there any anti-GNOME posts in official KDE-websites? Nope. The counterarticle in question was not on official KDE-website. It was on www.staikos.net."
    author: "Janne"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "And besides, if we should be concentrating on MS instead of other free projects, why is Michael Meeks attackign Qt and KDE? Shouldn't he be convincing people how MS sucks and how they should be working on GNOME and/or Linux instead?\n\nInstead of whining how KDE has \"lost track of the real enemy\", shouldn't we also be whining how GNOME and Michael Meeks has lost track of the real enemy?"
    author: "Janne"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-01
    body: "I don't really think that slide was a flame of KDE.\nI think the (KDE) in brackets was to say that KDE use QT.\nand I think the only real mistake was putting GNOME instead of GTK\nHe could have put GTK+ (GNOME) like the QT (KDE).\n\nBut are there any lies on the slide?\n\nThe LGPL does give you more freedom to do what you want than the GPL\n(which is both a strength and a weakness of the GPL)\n\nI personally think everyone is overreacting..."
    author: "Iain"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-01
    body: "\"http://www.gnome.org/~michael/XimianOOo/img7.html\"\n\nWhen it has a ~<username> in it, it stops being an official GNOME site."
    author: "Iain"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-02
    body: "No, that would be users/members/whatever.gnome.org/~<username>. The fact that it's on the same server means that either everything there is endorsed by the project or you can't accept anything there as official since I see nowhere a rule that \"~\" or whatever else makes the project not responsible for the content."
    author: "Datschge"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-03
    body: "umm... I used to work at an ISP that had some neo-nazi pages and had the ~username, and of *course*  we didn't endorse that sh-t. But in America we have a thing called freedom of speech."
    author: "anonymous"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "Alright, a note to Michael Meeks then:\n\nWhile you GNOME guys are wasting your time whining about how KDE is evil, Microsoft is getting a greater and greater advantage over you. If this keeps up, one day Microsoft will kill off the entire open source community, GNOME included.\n \nOpen your eyes people! Microsoft is the *real* enemy here! Instead of spreading anti-KDE propaganda, write code and make GNOME/Linux/whatever BETTER than Windows XP! If you don't, you will eventually be destroyed by your own blindness."
    author: "lit"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-03-31
    body: "No true. The people who _do_ further KDE development don't waste time on discussions like this one. How many postings from Waldo, Faure & c:o do you find on this page?\n\n"
    author: "Apollo Creed"
  - subject: "Re: WHO is the REAL enemy?"
    date: 2003-04-01
    body: "There is no particular reason to choose GNOME or Windows as a \"real\" enemy. Both are competitors to the KDE desktop.\n\nPerhaps Windows has a much larger user base, but at times GNOME can be more threatening for being able to run on the same OS as KDE. Windows, though, threatens to take people off X11, while GNOME users can still run some KDE programs (even konqueror!)."
    author: "ThreeFarthingStone"
  - subject: "Re: WHO is the REAL enemy?"
    date: 2003-04-01
    body: "\"Perhaps Windows has a much larger user base, but at times GNOME can be more threatening for being able to run on the same OS as KDE.\"\n\nIsn't that a *good* thing? KDE can run GNOME apps too.\n\nThe average user *wants* interoperability. They don't want any of this \"it's either us or them\"-stuff, they want to be able to run both."
    author: "FooBar"
  - subject: "Re: WHO is the REAL enemy?"
    date: 2003-04-01
    body: "> Isn't that a *good* thing? KDE can run GNOME apps too.\n\nIt definately is but the question is:\n\na) why running GNOME apps under KDE ? The KDE apps are far supperior, integrated, polished and configurable over GNOME apps.\nb) the reason why GNOME runs KDE apps is because they are missing native ones."
    author: "oGALAXYo"
  - subject: "Re: WHO is the REAL enemy?"
    date: 2003-04-03
    body: "> b) the reason why GNOME runs KDE apps is because they are missing native ones.\n\nGnome lacks some applications, KDE does as well!\n\nI love KDE, for me it is the best desktop. But surely I am very glad being able to run gnome apps like the gimp or zapper on it.\n\nI find this flaming very anoying. I don't need a kde-competitor to the gimp or whatsoever. Though a kimp-frontend for a gimp-library might be great."
    author: "Andreas Silberstorff"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-01
    body: "I think it's pretty clear that this was not an attack at Gnome.  It was a response to FUD intentionally spread about Qt and posted on public forums.  You're right, there are better targets for venting frustration and competing against.  Hopefully responses like these are unnecessary and infrequent in the future."
    author: "George Staikos"
  - subject: "Re: Lost track of the REAL enemy"
    date: 2003-04-01
    body: "Hello dc, also known as Stof.  Boy have you lived on this site.  Even got banned once, I see...  you sure do keep at it."
    author: "Navindra Umanee"
  - subject: "Qt"
    date: 2003-03-31
    body: "The \"reasonable\" developer costs you 9000$/year in Easter Europe or even less in Russia, India, China. Qt costs you 2300$, and additionally, upgrades cost almost 1000$/year and it doesn't include compiler ( on Windows ), so Qt isn't an option there.   \nIt probably isn't an option if you want to distribute your programs as shareware for 20$ ( for students or out of work development ). \nIt isn't an option for Sun and other commercial companies who can't tell their customers that they have to buy a second licence from Trolltech in order to use their product.\nIt isn't an option for all in-house script-makers, so it never become a main GUI toolkit on Unix."
    author: "Tom"
  - subject: "Re: Qt"
    date: 2003-03-31
    body: "Developers in China and in Russia develop with whatever they want. Most choose Microsoft platform with Visual Studio (now .net), because most of them do not pay for licenses at all (oh dear, what world are we living in... sad... :-)\nRemember Microsoft, that shiny bright big western company that pays sallaries an eastern europe/chinese/russian developer can only dream of?. They all want to step up to this bright new world, and MS sills are the ticket, because everybody needs to make a living! GTk-skills are not a ticket at all (QT skills neither)......"
    author: "Thomas"
  - subject: "Re: Qt"
    date: 2003-03-31
    body: ">It isn't an option for all in-house script-makers, so it never become a main GUI toolkit on Unix.\nWe\u0092re in that situation, using wxwindows instead. What would happen if Troll tech decides to tipple the license cost? They're not in it for charity. One of the biggest attraction with free software is the freedom to not bug the economic department. I think this will be one of the biggest issues for KDE in the corporate sector."
    author: "Tomten"
  - subject: "Re: Qt"
    date: 2003-03-31
    body: ">It isn't an option for Sun and other commercial companies who can't tell their customers that they >have to buy a second licence from Trolltech in order to use their product.\n\nYou do not need to buy a license to use a program built using Qt:\n\nhttp://www.trolltech.com/products/qt/licensing.html?cid=4\n\n\n--Jon\n\nhttp://www.witchspace.com\n"
    author: "Jonathan Belson"
  - subject: "Re: Qt"
    date: 2003-04-04
    body: "But if you have a plugin system for your application that requires GUI elements, then no one can create plugins unless they buy QT (even if your plugin system wraps the GUI with a scripting language).  That's no good.\n\nYou can use MFC (or .NET forms), Aqua, Gtk+, or whatever and plugin developers won't have any problems."
    author: "ch"
  - subject: "Re: Qt"
    date: 2003-04-01
    body: "> The \"reasonable\" developer costs you 9000$/year in Easter Europe or even less in Russia, India, China. Qt costs you 2300$, and additionally, upgrades cost almost 1000$/year and it doesn't include compiler ( on Windows ), so Qt isn't an option there. \n\nThis is an absurd argument. It's like saying \"sure the on board computer is a small percentage of the cost of a Mercedes 600 class but it's a larger percentage on an econo box\". This begs the question... what are you going to do with all the money you save?\n\nYou should check your facts. The scenario you are describing bears some resemblance to theKompany's business model. It has also opened doors for them on portable devices where QT is the only option. Whatever you want to say about them last I checked they continue to produce software... invalidating your argument."
    author: "Eric Laffoon"
  - subject: "Use WxWindows"
    date: 2003-03-31
    body: "Use WxWindows \nProvides a Wrapper to a existing toolkit (GTK on Linux).\nIt works on Linux,Win32,Mac\nNo licensing issues.\nPowerful and provides easy migration path for MFC programmers.\n\nI beleive KDE /Gnome should have common reusable binary architecture (Like COM)instead of  bonobo /Kparts,\nsince\nBonobo sucks due to complexity\nKparts is very much tied to QT.\n\nRajeev"
    author: "Rajeev Narayanan"
  - subject: "Re: Use WxWindows"
    date: 2003-04-01
    body: "\"Bonobo sucks due to complexity\nKparts is very much tied to QT.\"\n\nAnd COM isn't tied to Windows? Geez, at least get your arguments straight!\n "
    author: "David Johnson"
  - subject: "Re: Use WxWindows"
    date: 2003-04-02
    body: "No, COM isn't tied to Windows. Read a book, get a clue."
    author: "J\u00f6rgen S"
  - subject: "Didn't even manage to read it"
    date: 2003-03-31
    body: "I got as far as:\n\n\"Where I conveniently conclude that it can be the only solution on Unix for now\"\n\n...before giving up.  If the rest of the slides are as obnoxious and argumentative as that, I don't particularly want to read it.  The same would apply if the slides were promoting KDE instead of GNOME - it's hard to take a dickhead seriously.\n\n"
    author: "Jim"
  - subject: "Re: Didn't even manage to read it"
    date: 2003-04-01
    body: "this was also my reactionto these slides.\n"
    author: "xtian0009"
  - subject: "Cross platform development"
    date: 2003-03-31
    body: "I think the reason Michael included the cost of Qt on other platforms is because OpenOffice is (like a lot of large open source projects) portable between platforms. If OO was to use Qt, the cost would be a lot greater.\n\nI've also seem some people imply that GTK/Win32 is not very good or something, check this out, see for yourself:\n\nhttp://gtk-wimp.sourceforge.net/screenshots/\n\nRather clever that, I'm still trying to figure out the XP theming APIs :)\n\nGTK Mac is a rather sad state of affairs 'tis true.\n\nI'm not trolling, but I wish dot.kde.org was more like gnomedesktop.org - ie where most of the posts are to do with cool programs and the latest toys, as opposed to here, where most of the stories seem to be about trashing GNOME or pumping KDE/Qt. I do check here every so often because I am interested in KDE development, but only rarely is there a story of interest about the technology (sigh)."
    author: "Mike Hearn"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "Mike you are such a tool!\n\nThe only time I see you in here opening your pie hole is when you can criticize KDE in some minor way and/or tout your GTK/GNOME flamage.  \n\nI don't see you post when Derek gives the weekly CVS updates on all the cool technology.  I don't see you post when Navindra and the other editors post articles about KDE technology.  \n\nThe _only_ reason you come here is to post these passive aggressive comments that are really pathetic!  It is the same on all the other forums...  You claim to be so above the troll's but look in the mirror buddy, you are just a troll with a _weak voice_.  Damn, I can't stand such passive aggressive people like you."
    author: "anon"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "[ Mike you are such a tool! ]\n\nThanks. I love you too.\n\n[ I don't see you post when Derek gives the weekly CVS updates on all the cool technology. I don't see you post when Navindra and the other editors post articles about KDE technology. ]\n\nI don't normally have much to say about them. I do in fact read them though (well, skim the cvs updates, they are rather long). I don't usually post on the GNOME stories on gnomedesktop.org either, unless I have a question for instance. You can't draw any conclusions from that.\n\n[ The _only_ reason you come here is to post these passive aggressive comments that are really pathetic! ]\n\nOh hardly. I read comments, and when I disagree with people, or believe they're not in full possession of the facts, I respond. The people posting on this board are normally \n\na) Very knowlegable about KDE and \nb) Slightly (sometimes strongly) biased in its favour.\n\nI'm not an expert on either desktop, but I tend to have differing beliefs on GTK/GNOME to most posters here, so I post my thoughts. That IS what a comments section is for right?\n\nUnless \"troll\" now means, \"doesn't rejoice in the glory of KDE constantly\", I don't believe I'm a troll at all. If I went around using abusive language, posting unsubstantiated opinion and passing it off as fact etc, then perhaps, but I don't (or try not to).\n\nSo just chill dude, ok? Qt is a fine piece of software for sure, and KDE is a great project. But I like other stuff too, and I don't like to see people making stuff up about other projects - if that makes me a troll then I'm going straight to hell anyway."
    author: "Mike Hearn"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "\"I don't normally have much to say about them.\"\n\nRight.  You don't have shit to say unless it involves criticizing KDE in some way or touting your GTK/GNOME flamage.  It's as simple as that so quit with your passive aggressive 'dot.kde.org should be more like gnomedesktop' and take your biased overwrought passive aggressive self to some other forum.  Quit bemoaning the state of the dot and passing this off as intelligent commentary.\n\nYou're a tool and it's frightfully obvious from everyone that reads your posts that you are biased and passively hostile towards KDE and Qt.\n\n\"Oh hardly. I read comments, and when I disagree with people, or believe they're not in full possession of the facts, I respond. The people posting on this board are normally \"\n\nYou make every effort to apologize for everything GTK/GNOME and passively criticize every little thing about KDE and Qt that you can.  Anyone brings up Qt and you gush about gtkmm.  Some one says that C++ is superior to C and then you claim you know nothing/little about C++ and besides gtkmm is great while Qt is somehow not 'real C++'.  You just love all the bindings that GNOME provides and bash KDE bindings...\n\n\"But I like other stuff too, and I don't like to see people making stuff up about other projects - if that makes me a troll then I'm going straight to hell anyway.\"\n\nBut you make stuff up and apologize when others make stuff up about Qt and KDE.  That makes you obviously biased.  Coming here (a KDE site) and posting your obviously biased crap ad nauseum makes you a troll."
    author: "anon"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "[ It's as simple as that so quit with your passive aggressive 'dot.kde.org should be more like gnomedesktop' and take your biased overwrought passive aggressive self to some other forum. Quit bemoaning the state of the dot and passing this off as intelligent commentary. ]\n\nI never claimed it was intelligent commentary, so clearly you think it is - thanks :)\n\nI don't understand this passive agressive rubbish. If you mean I present my views without hurling insults at people, then yeah, bring it on. It's called civilised discussion in other parts of the world.\n\n[ You're a tool and it's frightfully obvious from everyone that reads your posts that you are biased and passively hostile towards KDE and Qt. ]\n\nIt's not obvious at all - apart from the fact that it's not really possible to be \"passively hostile\", I used to use KDE and think it's a cool project. Yes, I criticise it sometimes, I don't think it's perfect, and if you do then... well, you need to take a step back and get some perspective. Things can always improve.\n\n[ You make every effort to apologize for everything GTK/GNOME and passively criticize every little thing about KDE and Qt that you can. ]\n\nNo I don't. That's completely unsubstantiated, where have I said things about Qt that are not irrefutable fact? Pointing out details of TrollTechs licensing isn't criticizing every little thing btw.\n\n[ Some one says that C++ is superior to C and then you claim you know nothing/little about C++ and besides gtkmm is great while Qt is somehow not 'real C++'. ]\n\nWhere did I say Qt isn't \"real C++\"? It clearly is. It redefines a lot of stuff in the STL, will you deny this? That doesn't make it \"fake\" C++ though, I was simply drawing a comparison between them. Clearly George Staikos is allowed to compare the two, so why can't I?\n\n[ You just love all the bindings that GNOME provides and bash KDE bindings... ]\n\nWhere did I bash KDE bindings? You're the one making stuff up now, and it's not even about technology, it's about people, which is far less constructive.\n\nI mean for gods sake dude, get some perspective ok? We're discussing WIDGET TOOLKITS here, not war or politics!\n\n"
    author: "Mike Hearn"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "\"I don't understand this passive agressive rubbish. If you mean I present my views without hurling insults at people, then yeah, bring it on. It's called civilised discussion in other parts of the world.\"\n\nThe only reason you post on this forum is to criticize KDE/Qt wherever possible and laud GNOME whenever possible.  You are a troll and a liar.  This can be seen quite readily.\n\n\"Yes, I criticise it sometimes ...\"\n\nNo, you go out of your way to criticize it and tout GNOME in it's place.\n\n\"Where did I bash KDE bindings? You're the one making stuff up now, and it's not even about technology, it's about people, which is far less constructive.\n\nYou sir are a liar.\n\nhttp://developers.slashdot.org/comments.pl?sid=56544&cid=5480863\n\nFrom a thread responding to Richard Moore:\n\n\"I do apologise. I didn't make myself clear. What I meant was: Language bindings that work, and are useful for something other than feature checklist wars on slashdot. \n\nIn terms of working, useful language bindings KDE is a C++ environment only. Now kindly fuck off, loser. \n\nautopackage [autopackage.org] - Linux portable packaging\"\n\nHow freaking embarrasing Mike. LOL.  Now why don't you go crawl under a rock and quit trolling on the dot.  Sucker."
    author: "anon"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "Uh huh. You do realise that I didn't write that, don't you? If you seriously believe I'd write \"fuck off loser\" at the end of a post, then you need to review my other posts, because I've done that .... er, oh yeah. Never.\n\nI find it pretty disturbing that somebody is imitating me via my sig on Slashdot. I never post anonymously, and when you DO post in AC mode, your sig disappears, so whoever tried that was either extremely stupid, or doesn't understand how slashdot works.\n\nThat's a pretty transparent attempt to discredit me, and shows that clearly somebody has concluded that my arguments are sound, so it's easier to try and make me look bad. \n\nHow flattering. "
    author: "Mike Hearn"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "\"That's a pretty transparent attempt to discredit me, and shows that clearly somebody has concluded that my arguments are sound, so it's easier to try and make me look bad. \"\n\nAh so now you appeal to paranoia!  You deny that you made that post with your bald face hanging out.  That's so rich.\n\nROTFL  "
    author: "anon"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "Well like I said, AC posts on slashdot don't show your sig (for obvious reasons, wouldn't be very anonymous if it did, would it?).\n\nSo really, that post speaks for itself - why would I post as an AC but copy and paste my sig? Might as well use the karma bonus.\n\nI'm not going to reply to this thread anymore, it's a waste of time. The discussion has long left the realms of reality, and is no longer about issues, it's about how much you hate me and what I have to say, which isn't interesting to talk about."
    author: "Mike Hearn"
  - subject: "wow, such enlightened readers"
    date: 2003-04-01
    body: "this site has really slid downhill, these sorts of pathetic irrational flame posts are now the most common kind.  its a shame and an embarassment to the whole KDE community"
    author: "sadened kde user"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-01
    body: "Unfortunately the Gnome people are trolling this place today..."
    author: "anon"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-01
    body: "Well, that's a normal attitude of GNOME people. They do not necessarily troll here, they troll on all places. I had various comments with Mike HEarn myself where he namecalled me on various places for no reason but hey, he is not the only one, there are a lot of them. I have quit all my GNOME support for now and using KDE now. The people and community is much more friendly and the Desktop is targetting in the right direction of a cool Desktop environment."
    author: "oGALAXYo"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-01
    body: "Let me guess: You are using KDE for a couple of weeks now, right?"
    author: "Roberto Alsina"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-01
    body: "oh, my argentinian trolling friend."
    author: "oGALAXYo"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-01
    body: "Well, at least you are not calling me a GNOME troll anymore!"
    author: "Roberto Alsina"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-02
    body: "Galaxy wrote: \"Well, that's a normal attitude of GNOME people. They do not necessarily troll here, they troll on all places.\"\n\nROTFL... The irony here is hilarious!"
    author: "AN"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-02
    body: ">Galaxy wrote: \"Well, that's a normal attitude of GNOME people. They do not >necessarily troll here, they troll on all places.\"\n\n>ROTFL... The irony here is hilarious!\n\nYeah, beautiful.\n\nEvery discussion on internet where the word Gnome or Ximian is mentioned you can expect a venemous reply from oGALAXYo. It's becoming so obvious I can determine blind which AC comments on slashdot are from this person."
    author: "makkus"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-03
    body: "Oh and the way you reply now isn't an argument to criticise ? Most of the forums where I reply to are open forums or open places where people are allowed to comment with their own opinion and that's what I'm actually doing. The problem here is that many answers I get from Trolling people (like you for example) are not going into the context of what I wrote they are only meant to blame me in the public. Such as your reply. I wrote a lot of replies only describing and explaining what I personally think about GNOME from personal experience from own investigation from coding and instead getting replies from people to have a technical conversations I only get blamed because I was criticising their Desktop. None of the replies that I got from people was going into the technical things, they only replied to blame the person."
    author: "oGALAXYo"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-03
    body: "\" I have quit all my GNOME support\"\n\nThank heavens. Hopefully you will stay away from GNOME, and GNOME lists, and GNOME bugzilla, and GNOME .... forever! In short just forget that GNOME exists. Actually assume that GNOME never existed. If you can do this, then maybe you'll earn the respect that you conspicuosly lack. "
    author: "Soup"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-03
    body: "Stop the support doesn't mean that I gonna stay away from it, from Bugzilla, from their Mailinglists, from IRC and from all the developers that I still have good relationship to regardless of what dumb people such as you gonna Troll in the public and try others to make belive.\n\nStrange is that I only get blamed from shitheads that I don't know and never heard about and who never contributed one patch to GNOME. I bet that you Troll on more places than GOD knows.\n\nAnyways everyone is welcome reading what I have written on all sorts of places and If these people read carefully they will realize that I make a lot of points. Even now I get eMails from people to my private address who agree with me, either from the KDE corner, from the GNOME corner and from other places."
    author: "oGALAXYo"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-03
    body: "You are one sad human being. We pity you."
    author: "Soup"
  - subject: "Re: wow, such enlightened readers"
    date: 2003-04-03
    body: "OK, you criticise me for Trolling, well what do you call what you are doing ? Only proves what I initially wrote. If you pity me or if you call me a sad human is irrelevant. What matters is I at least did contribute to GNOME and still have a super relationship with the people. How about you ? People have problems differencing your ass with your face."
    author: "oGALAXYo"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "Give it up, it's more than obvious now that YOU wrote that fake Slashdot post."
    author: "FooBar"
  - subject: "Re: Cross platform development"
    date: 2003-04-02
    body: "Oh, gee.  Could it have been *you*?  dc/Stof/FooBar or whatever you've been going under here?\n<p>\n<a href=\"http://www.gnomedesktop.com/user.php?op=userinfo&uname=FooBarWidget\">[gnomedesktop.com]</a>\n<p>\nNice signature and link to autopackage.org, eh?\n<p>\n<a href=\"http://www.gnomedesktop.com/article.php?thold=-1&mode=flat&order=0&sid=1029#8909\">[gnomedesktop.com]</a>\n<p>\nNice IP address too.  Funny how well I remember that IP pattern.  Interesting things must have come from there...\n<p>\nAll very coincidental, eh? Oh and btw, autopackage.org has your full name and email address.  That's some nice patches you did... oh and wrote an entire module or two."
    author: "Navindra Umanee"
  - subject: "Re: Cross platform development"
    date: 2003-04-02
    body: "Good for you. I never claimed I'm somebody else. If you can see my IP - great. I knew that for years already.\n\n\n> Oh, gee. Could it have been *you*?\n\nAnd why would I post an anonymous Slashdot post imitate Mike Hearn?"
    author: "FooBar"
  - subject: "Re: Cross platform development"
    date: 2003-04-02
    body: "No doubt it helps to troll when you're anonymous, though."
    author: "Navindra Umanee"
  - subject: "Re: Cross platform development"
    date: 2003-04-02
    body: "You're contradicting yourself. To troll on what?\nOn Mike Hearn? Oh wait that can't be, I'm a fellow Autopackage developer.\nOn KDE? Can't be, that Slashdot post contains no reference to KDE whatsoever."
    author: "FooBar"
  - subject: "Re: Cross platform development"
    date: 2003-04-03
    body: "Oh gee, Hongli, I was hoping you would stop trolling this site...  but I guess not.  If you post one more repetitive message like \"Lost track of the REAL enemy\" like you have been posting AGAIN and AGAIN and AGAIN ever since this site was first launched, or any of other trollish/fudish stuff you've done under other nicks and IPs, well I'll make sure your ban sticks this time.\n\nIf you don't want to talk seriously about GNOME and KDE, then don't.  Just don't use serious discussions as an excuse for your anti-KDE propaganda."
    author: "Navindra Umanee"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "Just a note - I don't think Mike posted that message either - and it was in reply to me!\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "> while Qt is somehow not 'real C++'\n\nThen you explain what moc is."
    author: "FooBar"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "http://doc.trolltech.com/3.1/metaobjects.html\nhttp://doc.trolltech.com/3.1/moc.html\n\nYou'll be happy to know that KDE also uses this technology in eg. DCOP, a technology which Gnome is still trying to copy."
    author: "anon"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "A code generator?\n\nYou know, if it compiles using a C++ compiler, then it is C++.\nSaying something else is stupid."
    author: "Roberto Alsina"
  - subject: "Re: Cross platform development"
    date: 2003-04-03
    body: "And why in the world do we need a code generator AT ALL? Why not just do *everything* in C++ directly?"
    author: "FooBar"
  - subject: "Re: Cross platform development"
    date: 2003-04-03
    body: "Because it's boilerplate code? You know, You *could* create all the metaobject stuff manually. It would just be VERY BORING!\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Cross platform development"
    date: 2003-04-03
    body: "Ok, two replies to the same post :-P\n\nIt *is* C++. What part of it isn't? Take any KDE source file, and modulo bugs, it should pass any criteria defining what is C++ and what isn't.\n\nMaybr\u00a1e you think macros make it not C++?????\n\nBecause that's all \"signals:\" \"slots:\" \"SIGNAL\" \"SLOT\" \"Q_OBJECT\" are! Plain old silly (realy silly, look them up) cpp macros!\n\nI bet even gtkmm based programs have a macro or two somewhere in them ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Cross platform development"
    date: 2003-04-03
    body: "Why would you when you could just use a code generator?\n\nPlease trolling please."
    author: "Til"
  - subject: "Re: Cross platform development"
    date: 2003-04-03
    body: "> Why would you when you could just use a code generator?\n\nThe same reason why people say GOB is evil? -_-"
    author: "FooBar"
  - subject: "Re: Cross platform development"
    date: 2003-04-04
    body: "I honestly dont think you actually know what \"passive-agressive\" means.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: Cross platform development"
    date: 2003-05-17
    body: "As someone who's studied a bit of psychology, \nI *know* \"anon\" doesn't know what \"passive-aggressive\" means! \n\nIn fact I'm spending an hour today searching the Web, \ntrying to learn the exact lay (\"street\") definition of \"passive-aggressive\", \nand looking for the worst misuse of the term, \nand I must say \"anon\" is the winner \n(or opposite thereof ;^) ) \n\nThe curious can see the correct, clinical definition at \nhttp://www.merck.com/pubs/mmanual/section15/chapter191/191a.htm \n\n\tSincerely, \n\n\tJ. E. Brown  (mr.)\n\tRelationshop \n\tMaterials for Good Relationships \n\thttp://vla.com/Relationshop\n\tLos Alamos, New Mexico  USA \n\nP.S. Incidentally a common \"street\" usage of the term is ...\n... frankly off the topic, but write me if you're really interested. \n\n"
    author: "J. E. Brown"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: ">I do in fact read them though (well, skim the cvs updates, they are rather long).\n\nI'm insulted. You must be a troll!\n\nDerek :-}"
    author: "Derek Kite"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "> a) Very knowlegable about KDE and \n> b) Slightly (sometimes strongly) biased in its favour.\n \nNo shit? \n\nWE HAVE A SMART ONE HERE!\n"
    author: "meh"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "> Slightly (sometimes strongly) biased in its favour\n\nAnd why so? It's the dot.kde.org !!"
    author: "KDE User"
  - subject: "Re: Cross platform development"
    date: 2003-04-04
    body: "I cant stand such openly angry people like you. Learn tolerance child.\n\nHave a nice day,\nRyan"
    author: "Ryan"
  - subject: "Re: Cross platform development"
    date: 2003-03-31
    body: "Really nice, improved a lot really. I hope I can use it with php-gtk.\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "> Really nice, improved a lot really. I hope I can use it with php-gtk.\n \nIf you can use PHP-GTK. You can also use Kommander with PHP 4.3 which enables scripting more easily outside of a web environment. (You can use it with any scripting language but PHP 4.3 makes it easier.) Kommander is released now, has DCOP abilities and is stable. It's in the Quanta 3.1 and greater packages. Best of all you have the ability to visually draw dialogs as well as integrate with apps like Quanta."
    author: "Eric Laffoon"
  - subject: "Re: Cross platform development"
    date: 2003-04-01
    body: "Can you please elaborete a bit more?\nI have Quanta 3.1, but didn't find komander on it.\n\nAnyway, what is the relation of komander with php-gtk, it this a kind of php bindings for qt or just a tool to help programming for php-gtk?\n\nThanks in advance."
    author: "Iuri Fiedoruk"
  - subject: "Apps news like gnomedesktop.org"
    date: 2003-04-02
    body: "Mike Hearn, I think you should know already that the news site for KDE/QT apps is apps.kde.com. There's even a board for every single program/project, why should dot.kde.org duplicate everything when it already shows the bunch of apps releases on apps.kde.com in a neat box on the right side of every single page here?"
    author: "Datschge"
  - subject: "I think all GNOME developers should work on a GPL "
    date: 2003-03-31
    body: "My statement:\n\n\"I think all GNOME developers should work on a GPL stand-in for QT\"\n\nThis way they'll have something usefull to do that eventually will stop them wining!"
    author: "Anonymous Coward"
  - subject: "Re: I think all GNOME developers should work on a GPL "
    date: 2003-03-31
    body: "Uh, Qt is already GPL.  GNOME developers will never stop whining about Qt no matter what... It's in the blood ;)"
    author: "anon"
  - subject: "thats right, they are winning"
    date: 2003-04-01
    body: "because they dont spend all day bitching about what the KDE guys are up to and build awsome applications instead.  maybe you should take a hint."
    author: "sadened kde user"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-01
    body: "http://www.gnomedesktop.org"
    author: "anon"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-01
    body: "And which article exactly bitch about KDE?\n\nI'm waiting for an answer..."
    author: "FooBar"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-01
    body: "try this URL:\n\nhttp://www.gnome.org/~michael/XimianOOo/img7.html"
    author: "lit"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-02
    body: "That's not an article on Gnomedesktop."
    author: "FooBar"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-03
    body: "Go read the comments, genius.  Read it from people like redtux and the endless GNOME trolls over at gnomedesktop.com who always take potshots at KDE and when they can't get enough, they come over here and spread their FUD."
    author: "anon"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-03
    body: "Look at this GNOME troll spreading FUD about the KDE project:\n\nhttp://www.gnomedesktop.com/comments.php?op=Reply&pid=6543&sid=908\n\nDo more searches and you see a lot more.  These same people organise trolls against KDE on other sites like dot.kde.org, Slashdot and Linux Today."
    author: "anon"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-03
    body: "FUD? Those were *exact quotes* from dot.kde.org. You cannot deny that Mr Magoo, goosnargh and anonymous never said those things."
    author: "FooBar"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-03
    body: "\"It's quite clear that KDE people refuse to cooporate.\"\n\nThis is an exact quote from the KDE people?  Idiot."
    author: "anon"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-03
    body: "Then are you saying that that is untrue and that you DO want to cooporate?"
    author: "FooBar"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-03
    body: "Don't be a moron, okay?  What do you think Open-HCI and freedesktop.org are for?  For cooperating.  For cooperation between KDE, GNOME and anyone else who wants to cooperate.\n \nNow please take your FUD and go away if you are incapable of holding a serious discussion."
    author: "Navindra Umanee"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-04
    body: "Then you explain me what Mr Magoo, goosnargh and anonymous *really* meant. If what they said don't imply that they don't want cooporation, but not that they *do* want cooporation either, then what is it?"
    author: "FooBar"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-04
    body: "And just to clarify: that was a QUESTION, not a troll. A question that I want an answer on."
    author: "FooBar"
  - subject: "Re: thats right, they are winning"
    date: 2003-04-04
    body: "Fine,  you don't get it after 3 years?   Banning...\n\nRyan?  Yep, I have my eye on you too."
    author: "Navindra Umanee"
  - subject: "pre-pubescent flamers..."
    date: 2003-04-01
    body: "Who are you? I make it clear who I am here and I'm beginning to think anonymous posting here is becoming a problem. I don't have time to spend all day here but it looks like you do. I Lead the development of Quanta. Tell me it doesn't rock, and while you're at it show me correspsonging apps with less than 10 open bugs in their Bugzilla, a custom dialog building application to go with it and a WYSIWYG part in CVS under active development that you can show me a working demo of. 'nuff said!\n\nIt's really offensive for people to make statements under the guise of being any kind of a friend to what a site is dedicated to (regardless of what that is) when they are plainly looking to attack it and, in the tradition of all great prevaricators, pretending to be someone they're not in a meager attempt to validate their assault. That's juvenile behavior!  Here's a clue... let mom and dad use the computer to check their email, do your homework and grow up!\n\nI find it difficult to believe anyone would be sophmoric enough to imagine such a thin facade would fool anyone. You have my credentials. Until you can show your credentials don't expect that I consider your further rants worth responding to."
    author: "Eric Laffoon"
  - subject: "Re: pre-pubescent flamers..."
    date: 2003-04-02
    body: "So what, i post an occasional troll. And i like to read the feedback i get on it though dot.kde.org and not 1000 times in my inbox.\n\nAnd this troll wasn't that trolling, cauz _if_ the gnome people _would_ write a fully freesoftware stand in for Qt:\n> they'll have something usefull to do that eventually will stop them wining!\n\nSo.\n\nTake a laugh man! Smoke some! "
    author: "Anonymous Coward (the same as the poster)"
  - subject: "Re: pre-pubescent flamers..."
    date: 2003-04-03
    body: "If a person likes GNOME, use GNOME.  If a person likes KDE, use KDE.  Sheeeez, lighten up people.  You aren't going to convert anyone over to your side by telling them how stupid they are for not using what you THINK is best.  You might give SPECIFIC examples of why you think your preference works better than theirs. If you're as good as you say you are, then you shouldn't have a bit of trouble showing them the errors of their ways.\n"
    author: "LOfromMO"
  - subject: "the problem is: it's not LGPL"
    date: 2003-12-12
    body: "The license problem Gnome developers see with Qt is that it isn't covered by the LGPL.  The fact that people can build commercial applications on top of Gnome without paying anybody gives it a big advantage.  In fact, if Qt were covered by the LGPL, it probably would have taken over because technically, it used to be superior for many years."
    author: "anon"
  - subject: "Re: the problem is: it's not LGPL"
    date: 2003-12-12
    body: "> The license problem Gnome developers see with Qt is that it isn't covered by the LGPL.\n\nNot all Gnome developers. It would be interesting to know what share does it for this reason."
    author: "Anonymous"
  - subject: "What will the licensing cost be in 5 years time?"
    date: 2003-03-31
    body: "From a software engineers perspecive I really like QT, I like its programming model and its clean elegant APIs. And as a user I much prefer to use KDE to GNOME.  \n\nBut I have a problem with QT. Not a big problem today, in fact I agree with George Staikos positive assessment of QT cost/benefits, but the inklings of a future problem in the making. If KDE ever becomes the defacto GUI platform, especially on the corporate desktop, will Trolltech then not have as much leverage over commercial developers as Microsoft does now, with all the attendant abuses we have come to expect from this position of strength.\n\nAll this cost benefit analysis is just so much fluff, saying one platform is 10% better or worse. The bigger question is, could Trolltech become the next Microsoft? If KDE ever takes off, especially on the corporate desktop then I think it can.  Its control of core developer APIs would give it huge influence, along with a huge future revenue stream, placing it in a similar initial position that Microsoft found itself in with the control of the DOS APIs in the 1980s. This future Trolltech would be a very different company from the one we know today.\n\nA more pertinent question is not asking how much a commercial license costs today but what it could cost, once large scale developer lock in occurs on the QT platform several years from now.\n"
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-03-31
    body: "Please don't overestimate a GUI toolkit's influence and power nowadays. In fact Trolltech operates in a niche market (the Trolls have to be very flexible and to take innovative approaches to be successful..., hence the GPL'd version of Qt)\n\nI don't even want to know how many GUI's are available on the w32 and the Unix platform (interoperable or not) available 3 years after the year 2000. Even if somebody somehow finds a way to gain revenue from Linux and OpenSource (I really doubt it), you still can not turn back time. O.k.... 20 years ago a GUI toolkit might have been a key technology for a profitable future.\n\nIf you want to built a basis for a Microsoft-like monopoly-future in the IT-business in 2003: You must become a key-holder of a _key-technology_...(hey: it's not a GUI-toolkit... yes, really not a damn GUI toolkit).\n\nProblem/good thing: Even the Microsoft guys are not sure about future key  technologies at the moment, so the game is still open"
    author: "Thomas"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-03-31
    body: "But QT is now more than a GUI toolkit, it's really becoming an application developer API. Its potential for influence and control is large."
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-03-31
    body: "forgive my ignorance, but I would have thought for KDE programming you were using kdelibs, not QT, the fact that KDE itslf is QT based should not be an issue since it is licenced as GPL. So the cost of QT should be irrelevant, if you want to develop for kde, now and in 5 years down the line.\nRen"
    author: "renato"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-01
    body: "My understanding is that a commercial application coding to kdelibs would still require a commercial license to Trolltech as kdelibs is linked to QT and hence indirectly so is the commercial application\n\n"
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-02
    body: "No, commercial applications are allowed under the GPL.  If a software developer wants to charge for an application, they are allowed to without paying Trolltech a cent as long as the application is released under the GPL.  Therefore, nothing stops a commercial vendor from selling KDE-based software without going through Trolltech first."
    author: "Anonymous"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-02
    body: "Let me rephrase - Coding to the KDElibs and NOT making your application GPL would require a Trolltech license which I think was the gist of the conversation."
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-02
    body: "No. Coding to kdelibs and not making your application free software or open souce software would require a commercial Qt license.\n\nYou can make your application BSD, MIT, Artistic, LGPL and several other licenses without a need for a commercial license."
    author: "Roberto Alsina"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-02
    body: "You sir, are splitting hairs."
    author: "Re: What will the licensing cost be in 5 years time?"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-02
    body: "If the difference between GPL and LGPL were negligible, then the whole argument would be splitting hairs in the first place, wouldn't it?\n\nThe post I replied to was factually wrong. Mi response corrected it. Live with it."
    author: "Roberto Alsina"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "It is clear what he is trying to say. Just correct him, then reply to him. The fact is, sometimes GPL is *NOT* a good idea because it is designed for you to make money off the services. Now if I want to sell a download manager for example, the GPL pretty much makes sure I cannot do that, because the guy who buys it can legally give away the binary to his friends. The GPL does not allow you to impose an extra restriction like 'you are not allowed to redistribute the binary'\n\nSo non-community licensed software using the GPL Trolltech license is not a good idea. LGPL allows you to develop an app and license it in any way you want, but you must give back any changes you make to the libraries you might be linking to. Therefore is is more compatible with non-community licensd software than the GPL.\n\nPS. you can make your corrections if I made any errors, but I think you get the gist of my argument."
    author: "Maynard"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "------\n[The GPL] is designed for you to make money off the services.\n------\n\nThe GPL is perfectly fine for non-nagware shareware, too.\nAnd for custom software, since your client doesn't want to redistribute it anyway.\n\nAnd, again, Qt doesn't force you to license under the GPL! Get it? I said that clearly already two posts earlier. QT DOESN'T FORCE YOU TO USE THE GPL! YOU CAN \nUSE OTHER LICENSES! OTHER LICENSES THAT ARE NOT THE GPL! DIFFERENT ONES!\n\nWhat Qt forces you is to create open software. Unless you want to pay. That is about it.\n"
    author: "Roberto Alsina"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "Exactly what I meant. The point is now that if someone does NOT want to make their software open-source, or rather, does not want to make a decision NOW as whether to use GPL or a closed source license, he has to buy a license or defer develpment. They may not want to pay if they are going to use it for open-source software, so they rather go to using an LGPL toolkit.\n\n[quote]\nAnd, again, Qt doesn't force you to license under the GPL! Get it? I said that clearly already two posts earlier. QT DOESN'T FORCE YOU TO USE THE GPL! YOU CAN \nUSE OTHER LICENSES! OTHER LICENSES THAT ARE NOT THE GPL! DIFFERENT ONES!\n[/quote]\n\ncheck my post, I thought I tried very hard to say, 'community licensed' instead of identifying any license in particular. That Qt doesn't force you to use GPL I understand very much thank you."
    author: "Maynard"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "Quit beating around the bush Maynard and say what you mean:\n\n\"Qt is bad because it doesn't allow poor proprietary developers to create third party software for Free and Open Systems.\"\n\nIf that is your position then fine, but please do not ascribe any noble belief to it.  This position does not help Free and Open Systems.  The fact is that KDE and Qt provide a complete and Free (both beer and libre) platform for anyone to build Free and Open software.  Your only gripe is on behalf of extremely *poor* proprietary developers without any capital.\n\nThe FSF regards Trolltech and Qt to be one of the best success stories for the community.  The FSF encourages GPL'd libraries.  So, in the end all your griping is your problem because the community doesn't really care much for the poor proprietary developers anyways."
    author: "anon"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "Just because you are a proprietary developer doesn't mean you deserve to be gouged in the future by the toolkit owner once you are locked in. The network effect that benefited both Microsoft and also IBM in the 70's seems to be a problem particular to the IT industry and could well happen again if KDE/QT becomes a defacto developer platform.\n\nThe KDE/QT foundation protects open source but a successful KDE / Linux platform if it is to take off must provide a fair platform for both open and closed source developers, otherwise we are just repeating the lock in problem that happened on Windows."
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "Whaa??  That is the entire point of proprietary software man!  To gouge the customer once he get's locked in!  The same is true of Visual Studio and the same is true of MacOSX and the same is true of every other piece of proprietary software around.  How can you sit there and complain about proprietary developers getting proprietarily locked in?  Pot please meet the kettle. Holy Shit the lengths that people will go to assail Qt is really unbelievable!"
    author: "anon"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "----\ncheck my post, I thought I tried very hard to say, 'community licensed' instead of identifying any license in particular.\n----\n\nExcept here:\n\n-----\nthe GPL pretty much makes sure I cannot do that, because the guy who buys it can legally give away the binary to his friends. The GPL does not allow you to impose an extra restriction like 'you are not allowed to redistribute the binary'\n------\n\nAnd here\n\n-----\nSo non-community licensed software using the GPL Trolltech license is not a good idea.\n-----\n\nBecause Qt is also available under other licenses, like the QPL. You can code programs that use Qt, and release them under some licenses incompatible with the GPL because of that. So, even if your license makes using a GLPd Qt a bad idea, it may not make using Qt a bad idea.\n"
    author: "Roberto Alsina"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-05
    body: "But, at this point, purchasing a commercial QT licence do you get support from Trolltech even on kdelibs? (otherwise you are paying on something you do not recieve support on)\nRen\n"
    author: "renato"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2007-06-28
    body: "---quote\n\"Qt is bad because it doesn't allow poor proprietary developers to create third party software for Free and Open Systems.\"\n---quote\n\nIt's interesting that the word \"poor\" comes up here.\n\n---quote\nJust because you are a proprietary developer doesn't mean you deserve to be gouged in the future by the toolkit owner once you are locked in. The network effect that benefited both Microsoft and also IBM in the 70's seems to be a problem particular to the IT industry and could well happen again if KDE/QT becomes a defacto developer platform.\n---quote\n\nThere is still a certain level of paranoia hovering over the IT industry because of what these mega-corporations did. This is especially true when a piece of software becomes an integral part of the operating system.\n\nAs a system gets more and better apps it becomes much more valuble. This is true even when the apps are third party. Windows keeps a lot of its market because some people want a system that is compatible with a lot of software.\n\nUnfortunately, the \"poor\" app developers don't see much of these profits. They soon feel disgusted and let down as they see the company they helped to build turn into a monster.\n\nFor this reason, it has become imperative that the licensing of all core software and the motives of its owners be place under extreme scrutiny.\n"
    author: "K Smith"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "When I see how companies seems to be rallying around GTK, and their reasons for doing so, I think tey might lend credibility to the view that the toolkit is important. Just as the OS is important.\n\nFor Linux to come out of the hobby OS level to the real business level, it has to attract commercial as in proprietary development. No need to argue that. Imagine if Corel released a native Linux version of Wordperfect Office. They could use Qt and pay licenses, but if they ever sold enough, or gained enough market power, Trolltech could jack up prices of their toolkit to maximise their profits.\n\nIf Qt became preferred, they would have market power. There is absolutely no need for us to accept a potential problem is there."
    author: "Maynard"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "Companies are not rallying around GTK.  The only companies that are doing so are losing money out the ears.  The truth is that Qt is gaining market share all over the place with the likes of IBM, Adobe and many others.  This despite this pernicious myth that GTK is favored by business which it is _not_!  Companies that are interested in third party proprietary development want a sure and strong company, aka Trolltech, that will support the tools and provide documentation.  It can be argued that GTK is more widely used among free software programmers, but make no mistake that Qt is the champion for third party software developers.\n\n\"If Qt became preferred, they would have market power. There is absolutely no need for us to accept a potential problem is there.\"\n\nQt is already preferred.  They are gaining market power all over the place.  What I really find funny is all the gnome folks siting Trolltech as a potential problem, but look the other way when one of your leaders is proposing to put Mono in the some of the core desktop of gnome.  I mean, MS is a far scarier company than Trolltech by infinite orders of magnitude."
    author: "anon"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "Well I tend to agree with you. I think QT is gaining market share. I also think it is a better toolkit then GTK from a technical perspective but Maynard is right. QT will be a problem, a future problem.\n\nCompanies are short term creatures. IBM asked Microsoft a long time ago to produce DOS without thinking through the long term implications. It gave Microsoft ownership of developer APIs  and allowed them to own the developer. Microsoft was able to leverage that powerful lock to great effect, for the next 20 years.\n\nIf KDE reaches a critical mass and I think that might be soon then Trolltech will own the corporate developer especially, though of course it will be years before it will become obvious. It took years for IBM to realise the mistake it made. \n\nI find myself in the position of favouring the look and feel of KDE as a user, appreciating the QT toolkit as a developer, but also feel that Trolltech is in a very powerful position if KDE/ Linux takes off. A position it could and probably will abuse if KDE ever was to become the defacto desktop.\n\nHowever I also think the Mono project is madness itself, giving credibility to Microsofts APIs, but forever lagging the implementation of them much as Wine does and thus always been seen as an inferior implementation. Crazy."
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-03
    body: "David, I can see you are truly struggling with this.  Yes, Qt plays by proprietary rools when used by proprietary developers.  This is not a problem and will in know way harm our Free desktop.  Trolltech will not own any corporate developer because if they try then those companies will either port or they will reimplement Qt.  In the end this doesn't really matter because third party proprietary client software will not be long for this world on the Linux desktop.  It just is not going to happen.  And that leaves proprietary software with only a small niche which really won't be of concern since who cares if TT controls a small niche.  Really, I don't think you should be worrying so much about this since Trolltech is a wonderful Free Software company and the potential for abuse is really very small and would likely be swiftly and convincingly rectified by the community.  It's mountains out of mole hills here."
    author: "anon"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-04
    body: "I am always suspicious of people who fly the flag too strongly either way, both QT or GTK. Struggling is a term that is slightly derogative, no doubt deliberate, to describe my position on QT.\n\nHowever I don't think that TT, should KDE become a defacto platform, will be willing to control just a small niche, that is a purveyor of widgets. They would use there key position in the software stack, much as Microsoft has done, to expand both up into KDE functionality as well as large scale application development, with \"additions\" eventually coming under all sort of licences ranging from GPL to propriety that will be hard to unpick.\n\nYou mention that companies might port or re-implement QT if Trolltech ever got abusive. I don't believe history bears that out on the Windows platform. Despite the existence of other sometimes superior toolkits and development platforms for Windows, once lock in occured those other vendors led a marginal existence. Remember back in the mid 90s how the feeling was that despite Borlands some would say superiour C++ builder environment, most corporates went with the \"official\" MS studio / MFC /ATL, development environment. The feeling was that becuase Microsoft owned the APIs their tools would be first to benefit. Generally everything felt more cohesive because one vendor owned everything.\n\nRe-implementing QT (maybe under LGPL or BSD) is already reaching the point where it getting too large and would take several years to do so. By the time it was done, the official TT version would have moved on and latest KDE would then be using its new features, which of course commercial developers if they where also using kdelibs (in all likelyhood) would also need to track. Further Trolltech employs key KDE developers who would likely wish to track and use the latest QT features.\n\nLet me quote Erick Eng from Trolltech in an interview dot.kde.org  a while back\n\n\"So, as soon as we felt that we could outrun anyone trying to make a hostile fork, we switched to using the GPL\"\n\nSummary:\n\n- Porting - History hasn't born that out.\n- Forking / Reimplementing - Not a chance anymore, its too late.\n\nPredictions:\n \n1) It's probably too late for GNOME despite the backing of the largest distributor (Red Hat). In some ways KDE /QT has rightly won. It really is a better than GNOME /GTK in terms of usability and certainly as a developer platform. but the feeling is \"Hey, lets not worry about TrollTech, they are some pretty nice guys anyway and all this talk about them abusing their position is just conspiracy theories.\"\n\n2) KDE / QT become the defacto user platform in Linux.\n\n3) Eventually the economics of open source mean some day KDE /Linux become the defacto desktop and supplants Windows to a large extent, though this might be a some time.\n\n4) Honeymoon period. Cheap deployment of Linux / KDE everywhere. Third party developers can compete without looking over there shoulder, wondering when Microsoft was going use its position to crush them. Corporate application managers delighted, though get an initial suprise they need to cough up licenses fees for this thing that was supposedly free / cheap. No matter, everybody else is moving to Linux, and the license costs aren't all that bad.\n\n4) Trolltech expands, nice steady and rapidly growing income stream from commercial developers. Starts to employ armies of product managers, business analysts and people who basically don't give a fig about Trolltech open source past. They start figuring a way of leveraging the market network effect that is beginning to happen. License costs start to creep up, new and \"innovative\" license conditions for commercial developers. Non GPL and \"fuzzy\" open source licenses start to be implemented, e.g MS shared source, all the way to full proprietary \"add ons\" Heck, Microsoft when they see which way the wind is blowing might see TT has the only viable way to make proper money in Linux as opposed to all those shaky business plans that generate revenue from services. Use some of their spare $40 billion cash to jump ship with a very generous buy out of Trolltech.\n\n5) A feeling pervades that Trolltech is now leveraging its position to full extent, and using it much as Microsoft has done in the past to compete with its own third party developers unfairly. Options like GTK / GNOME have been semi abandoned long ago and do not provide the the full feature required by todays modern developer. Lots of grumbling but nowhere to go.\n\nIt would be interesting to see Trolltech floated on a public exchange. As it is privately held we cannot get a open market viewpoint on the potential value of its position. I would put my money where my mouth is. I would love to buy into it as I believe it is in a very strong position. Probably the only company that has a significant income stream should Linux take off."
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-04
    body: "Ok if you are splitting hairs.\n\nCompanies like SUN who are putting a lot of investment into Linux for the desktop, not just using some toolkit for some other reason.\n\nRedhat uses GTK and its about the only Linux company to make a profit. Eclipse is released using GTK and Motif. SUN seems to be supporting the GTK-isation of Openoffice, or does not seem opposed to it. Ximian is GTK-ising Openoffice. HP is doing deals with Redhat which will probably bring GTK to the fore. The main point is with GTK that uncertainty is NOT there. With Qt it is.\n\nA question, is it possible to fork Qt. I honestly do not know or have an opinion on that one here. Is Qt itself GPL, or is software produced under it using the free edition necesarily GPL. I get the feeling that the library itself may not be free, as in you cannot change it if you wanted. But this last part I DO NOT KNOW AT ALL, please do not take me wrong here."
    author: "Maynard"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-04
    body: "I am not splitting hairs.  You are grasping at straws.\n\nSUN is wallowing in red ink and have yet to do anything of interest on the Linux Desktop.  RedHat is another company that can't make a profit and they are supporting KDE too as well as trying desperately to nullify the differences with KDE.  Eclipse does not use GTK it uses SWT.  Claiming that this is helpful to GTK is really stretching as I can get the same thing by using Geramik.  Ximian (another company in the financial pit) is integrating some things with OO, but so what it looks minimal and it is not GTK-ising OO. \n\n\"The main point is with GTK that uncertainty is NOT there. With Qt it is.\"\n\nLOL\n\nFear.  UNCERTAINTY.  Doubt.  Now please don't claim you aren't spreading FUD, because you've just admitted it.    You damn GNOME fanboys should just leave dot.kde.org and go back to whatever rock you crawled out from under.  NO UNCERTAINTY EXISTS except the crap you and the Michael Meeks and Miguel and Mike Hearn and Soup and Stof and whoever else are trying to spread!  You people.  If you are all so damned, 'uncertain' then why don't you go and talk to Trolltech or how about talking to all the companies that are THRILLED with Qt ??  Oh yah, because that wouldn't help your goal of spreading FUD and you might have to grow up and admit you are full of #&$#! \n\n\"A question, is it possible to fork Qt.\"\n\nOf course.  Why on earth would you think it is not possible?  In fact the cygwin folks are working on a small fork right now.\n\n\"I get the feeling that the library itself may not be free, as in you cannot change it if you wanted.\"\n\nOf course it is Free.  What is so complicated?  Qt is released under a many different licenses.  You can use whatever license you wish."
    author: "anon"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-04
    body: "Actually I always make it a point whenever I install Linux to install BOTH KDE and GNOME. So I am a fanboy of both if you will.\n\nI currently run BOTH Redhat and Mandrake too.\n\nWhat I am saying, and what you are failing(refusing, stubbornly resisting) to do is to grasp what I mean by uncertainty.\n\nThe LGPL is compatible with more licenses than the GPL IIRC. Qt seems to have problems with SISSL the last time I checked.\n\nLGPL enables both opensource and non-opensource development without adding an extra layer of complexity.\n\nIf today, I decided to start a project to make an app with Qt, I would have to decide NOW whether it is going to be licensed opensource or closedsource. That may not always be appropriate for projects that start off as hobby projects and then might turn out to be of value such that I may want to make some money off them.\n\nActually, those(Redhat Sun) are some of the biggest Linux vendors there are, and Redhat probably the biggest on the software side and not too many people are doing much better. If they can't make it, then probably no one else will.\n\nIf you are of the opinion that closedsource is BAD, EVIL then there is no changing you. If you think that if I decide to make a closedsource program then I should pay then that is your opinion. If you think that closedsource developers should be at the mercy of a toolkit vendor then so be it. Closed source vendors attract attention to Linux and help improve it. I am not against closedsource. I am against giving someone the power to be able to control a platform.\n\ncheck out these screenshots. I see GTK there.\n\nhttp://pobox.com/~hp/eclipse-shots/eclipse-metacity.png\nhttp://pobox.com/~hp/eclipse-shots/eclipse-metacity-compare.png\nhttp://pobox.com/~hp/eclipse-shots/eclipse-templates.png\n\nThis is an IBM sponsored project by the way.\n\nAnd by the way, where I come from, people are born, they do not come up from under rocks. And where I come from it also shows a lot of maturity to call people fanboys and tell people that they are full of #&$#, as it does to rudely answer questions asked honestly."
    author: "Maynard"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-04
    body: "> check out these screenshots. I see GTK there.\n\nI see a proprietary win-copy-widge there packed in themed metacity windows. That's exactly the same like claiming Mozilla and Star/OpenOffice and now Java Swing are GTK based. Oh, and btw, they really aren't."
    author: "Datschge"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-04
    body: "Yes it is possible to fork QT. However the fork would only support open source development. It would not support developers who wanted to keep their source closed. In effect a KDE built on top of a forked QT would not be able to support commercial development which for KDE to be viable and a mainstream platform would need to do so.\n\nIts slightly ironic as forking is one of the basic rights that the GPL gives. however due to its position within KDE, KDE could never fork QT significantly as it would no longer be able to support commercial KDE development. KDE is tied to Trolltech QT, no matter which direction Trolltech decide to push QT."
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-04
    body: "\"In effect a KDE built on top of a forked QT would not be able to support commercial development which for KDE to be viable and a mainstream platform would need to do so.\"\n\nGet it through your thick skull, Qt (both the Open Source version[s] and the proprietary version, support commercial development!  This is a pervasive meme that was created by MS and here you are repeating it."
    author: "anon"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-06
    body: "\"Qt (both the Open Source version[s] and the proprietary version, support commercial development!\"\n\nThat's not what he is being saying. A forked open source version of QT wouldn't be able to support closed source commercial development. Ask Trolltech, before you spout off, nothing to do with Microsoft \"memes\"."
    author: "sickofidiots"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-06
    body: "Clearly you haven't read the post you are replying to.  Look in particular at the quoted text:\n\n\"In effect a KDE built on top of a forked QT would not be able to support commercial development which for KDE to be viable and a mainstream platform would need to do so.\"\n\nWhich is clearly wrong as the previous poster pointed out."
    author: "Anonymous"
  - subject: "Re: What will the licensing cost be in 5 years tim"
    date: 2003-04-08
    body: "Anyone who claims a commercial application can be GPL is insane.\n\nHow exactly do I make money selling a GPL'd application?\n\nWhile it's true you _can_ sell it, you'll only sell 1 or 2 copies.  After that other companies will be reselling your stuff or giving it away for free.  With GPL you can not limit redistribution.  That is the problem for commercial applications.\n\nI have no problems with my software being open-source, or even GPL-like, but there is no way I can allow someone to resell my product, or only buy only copy then use it with 100000 users.  Then I don't make any money and my company goes under.  GPL works if you're selling services, but not if you make your money selling software."
    author: "Grasis"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-03-31
    body: "That occured to me too.  Another thing that worries me is that the licensing issue is scaring away other 3rd party tools from targetting KDE as a platform.  For example, wxWindows doesn't target it, and neither does Eclipse.  They all target GTK because it's LGPLness is thought to be safe.\n\nThat puts me in an awkward position.  I'd like to use something nice like QT, or even just some minor subset so I could use one clean API to write multi-platform applications without having to pay a \"toll\" to use the best Linux DE.  It would be nice to be able to use a more liberal license (e.g. BSD) and target KDE without it being \"polluted\" by QT.\n\nIt would be nice if KDE could define some minimal, KDE-only subset under a LGPL (or more liberal) license so things like wxWindows and Eclipse could target it for people who wanted to.  That way it wouldn't threaten Trolltech much, and if someone wanted to go all full-blown QT with all its features and support they could still get it within some $ from Trolltech.\n"
    author: "Anonymous"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-04
    body: "\"It would be nice if KDE could define some minimal, KDE-only subset under a LGPL (or more liberal) license so things like wxWindows and Eclipse could target it for people who wanted to. \"\n\nHow is this for a subset:  the kdelibs is LGPL.  That's right, the core libraries of KDE are already and have been LGPL.  wxWindows and Eclipse can target them all they want just as they can target whatever they like.\n\n"
    author: "anon"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-04
    body: "It would be interesting to do a code analysis on KDE and see which classes / APIs in QT are being used. I suspect like almost everything in life it would follow the 80/20 rule and 80 percent of KDEs usage of QT would require a re-implementation of only 20 percent of QT. That would significantly reduce the time to re-implement a LGPL QT and make it more manageable to catch up with the current QT. One problem is Trolltech employs key KDE architects. Would they be tempted to ensure that KDE is locked into the latest Trolltech QT? There could be a conflict of interest here if a LGPL implementation was attempted.\n\nI have quoted Eirik Eng, president of Trolltech in an earlier post on this thread as saying \n\n\"So, as soon as we felt that we could outrun anyone trying to make a hostile fork, we switched to using the GPL\"\n\nI tend to agree with him. Reimplementing QT under a different license is too late, it is now too big and a 80/20 analysis would probably be the only way of cutting the scope of the work.\n\nI suspect Trolltech would consider an alternative implementation as hostile and a potential threat to their revenue stream as developers might prefer to code to KDE API based on a re-implemented LGPL one rather than dual licenced Trolltech one.\n\nHowever I think it would be better that the open source community rather than a single company dictated the direction of such a key API. It would be fairer for KDE and other open source developers as well as commercial developers."
    author: "David O'Connell"
  - subject: "Re: What will the licensing cost be in 5 years time?"
    date: 2003-04-08
    body: "\"Another thing that worries me is that the licensing issue is scaring away other 3rd party tools from targetting KDE as a platform\"\n\n...and the licensing issue with Windows is even worse. That's why nobody is writing apps for Windows.\n"
    author: "michael"
  - subject: "thank you for your enlightened comment."
    date: 2003-04-01
    body: "its a rare thing around here."
    author: "sadened kde user"
  - subject: "This article is just about equally biased"
    date: 2003-03-31
    body: "The facts about GTK+ and Qt are easy enough to decipher through Michael's market speak:\n\n1. GTK+ is LGPL\n2. Qt is GPL (for UNIX only)\n3. Qt requires a commercial license for commercial development (and not just when releasing, but throughout the development)\n\nThe savings argument:\nI buy the argument about cost savings might make up for the cost, but George does not actually prove that this is in fact the case. You can develop software using GTK+'s C++-bindings, which are actually quite nice, and quite well documented (http://www.gtkmm.org/gtkmm2/docs/). It might still be that Qt saves you money, I don't know, but it has not been proven at all. In addition, you might want to check out Mono, Java or perhaps Python if you want to save substantial amounts of money, and the argument about Qt being better, does not count for much on these platforms. It might be that Qt will save you money, but it has not been proven. The only proven _fact_ on the matter, is that Qt costs money for commercial development. Actually, one of the arguments used for free software against commercial software is just as usable here. With GTK+ you don't have to bother with licenses. You don't have to spend time making sure all of your developers have a commercial license. You don't have to pay for upgrades down the road. This is more complicated for Open Office, see below.\n\nCommercial license from the start:\nWith Qt you have to decide up front wether the software will be GPL or commercial. You may not \"change your mind\" just before the release, or you are violating Trolltech's licenses. This might be ok for some, but others may like to see how the software actually turns out, before they make such a decision.\n\nIn addition, the Qt-license is basically useless if you are a hobbyist developer, that develops software on your spare time, without knowing wether you want to release it as Free Software, or commercial software.\n\nThe choice of Qt for OpenOffice would be a really strange one, since it is licensed under several licenses, some of which would certainly require a commercial license, and some which would not. \nIf a volunteer contributes to OpenOffice and agrees that his patches can be applied to Star Office as well, does this volunteer have to have a commercial license? Would SUN have to have a commercial license for every single volunteer that helps out?\n\nI find it strange that there are lots of KDE-users that think the commercial GNOME-companies are evil (Ximian, Red Hat, SUN, etc.), but the commercial KDE/Qt-companies (Trolltech, SuSE, The Kompany, etc.) are not. I've never seen the same attitude from GNOME-users towards KDE. \n\nAs you might have guessed, I use GNOME. This does not mean that I hate KDE, in fact I think it is great. I love having the choice. If you like KDE, then more power to you. Just because you like KDE, does not mean that Qt is the right choice in all situations.\n\n"
    author: "Gaute Lindkvist"
  - subject: "not correct"
    date: 2003-03-31
    body: "Wrong, you don't have to GPL your code.  It can be almost any Open Source license.  And you can re-license your own code as proprietary any time you want but if someone has already downloaded your GPL code then you can't take rights away.\n\nSuSE, Trolltech and The Kompany none of these influence the core KDE development processes.  KDE can even thumb its nose at Trolltech by overriding whatever Qt provides in kdelibs."
    author: "anon"
  - subject: "Re: This article is just about equally biased"
    date: 2003-03-31
    body: ">    the commercial KDE/Qt-companies (Trolltech, SuSE, The Kompany, etc.) are not. I've never seen the same attitude from GNOME-users towards KDE. \n\n\n Please... GNOME developers have been calling TrollTech evil since 1997.\n"
    author: "lit"
  - subject: "Re: This article is just about equally biased"
    date: 2003-03-31
    body: "Er... no - they originally disliked the license of Qt, it had little or nothing to do with TrollTech. And nobody has cared for years now, except those who are involved with software that's either :\n\na) portable (ie toolkit must be free on all platforms) or\nb) non-GPLd software"
    author: "Mike Hearn"
  - subject: "Re: This article is just about equally biased"
    date: 2003-03-31
    body: "Er... no - they still disliked the license of Qt, and they still bash TrollTech at the slightest opportunity (See all the SCO owns Trolltech comments in the last month).  And Miguel and Michael Meeks and others still care enough (thanks guys) to FUD KDE."
    author: "ao"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-01
    body: "oh plz. the arguments are more like \"omg, trolltech is gonna control the UNIX desktop through KDE and then it will bitchslap the rest of us, and sell our free software souls to the devol\"... \n\nthis of course, assumes that:\n\n1. trolltech is an evil company\n\n"
    author: "lit"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-02
    body: "No, it assumes that Trolltech shareholders are going to pressure their employees, the directors or whoever at Trolltech, to work harder and increase profits one day. And then the director, with an evil grin decides that they are in a position to screw over everyone who uses the toolkit, and triple their developer licenses or something.\n\nI know this reeks of conspiracy theory, but it is nothing that does not have a precedent. Corporations are heartless, inhuman from their foundations. Trusting a corporation's 'goodwill' is like digging your own grave. Those things exist for the sole purpose of making money. Trolltech made a conscious decision to license their tools the way they did because they saw commercial value in it. GPL does not apply to them, it applies to anyone who uses their toolkit without the other licenses. Trolltech is the only company that can do whatever it wants with the toolkit."
    author: "Re: This article is just about equally biased "
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-02
    body: "TrollTech is a privately owned company, with 71% of it owned by the employees (and another 5% by a charity foundation). There are no sharedholders to pressure employees or management there. So please, before you start spreading those types of paranoid theories, learn of what you're talking about. \n"
    author: "Sad Eagle"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-03
    body: "You are assuming these people harbour no ambitions of becoming rich if given an opportunity.\n\nDo you think IBM would have given Bill Gates teh rights to make an OS for their system if they had known he would become so powerful then. No. Back then, MS was owned by its employees too. Only now they are much richer and and employ thousands more."
    author: "Maynard"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-03
    body: "And the same can be said of Ximian.  What's to stop Ximian from forking all the code they do in the future and royally gouging everyone?  Sure, the community could just continue with the free branch, but the same can be said for Trolltech and Qt.  Qt is completely Free and nothing Trolltech can do will change that."
    author: "anon"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-03
    body: "Ok, that covers 76 percent.  Who or what owns the remaining 24 percent (nearly 1/4 of the company)?\n"
    author: "LOfromMO"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-03
    body: "7.0% Employee and Foundation Owned\n\nRest is 5 other private investors, including:\n\nBorland - 8.3%\nCanopy Group - 5.8%\nTeknoinvest - 3.3%\nOrkla - 3.3%\nNorthzone Ventures - 3.3%\n\nNote that the sole controlling partner in Trolltech is it's employees. The Canopy Group, Teknoinvest, Orkla, and Northzone Venture are companies that invest in tens or even hundreds (in the case of Orkla, which is a rather huge conglomerate in Norway..) of companies. "
    author: "Til"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-03
    body: "Just because Trolltech is privately owned doesn't mean there is no shareholder pressure. Private owners are private shareholders and subject to many the same forces as public shareholders. Further Trolltech employees might well be tempted by an attractive offer and be bought out by a public company.\n\nIn fact, Microsoft was a private company until 1986.\n\nAsk yourself this. If Microsoft was to make an offer for Trolltech to sell out with some of its spare $40 billion cash, how long do you think those nice guys at Trolltech would hold out. Would you be happy with Microsoft in charge of key KDE underpinnings. Might they not try and leverage this postion?\n\nI don't believe there is a problem today with Trolltech, the thing is unfortunately, there wasn't a problem with Microsoft in the 1980s. They where the good guys allowing IT departments the world over to escape the yoke of IBM. \n\nI believe Trolltech owns something very similar in QT to what Microsoft had in the 1980s. It wasn't the ownership of the OS but the ownership of the key developer APIs that allowed Microsoft to leverage its position.\n\nThose who cannot remember the past are condemned to repeat it."
    author: "David O'Connell"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-02
    body: "From Trolltech's website \n\nhttp://www.trolltech.com\n\n[quote]\nAdobe Uses Qt to Build Adobe Photoshop Album \nIn the latest example of Trolltech's growing market power, software giant Adobe Systems announced that it has used Qt to build its innovative new Photoshop Album application. \n\n\"Qt simplified our task by providing high-level tools we could customize to meet our needs,\" said Mike DePaoli, Photoshop Album Engineering Manager. \"The product is excellent, the support was outstanding and we are extremely pleased with our decision to go with Qt\". \n\nThe software (which helps users find, fix, share, and protect their digital photos) received the \"Best of CES\" award in Photography and Imaging at this year's Consumer Electronics Show in Las Vegas. CES is the world's largest consumer electronics trade fair.\n[/quote]\n\nThe horse speaks."
    author: "Re: This article is just about equally biased "
  - subject: "Re: This article is just about equally biased"
    date: 2003-03-31
    body: "<l>2. Qt is GPL (for UNIX only)</l>\n\nNo, Qt is GPL, for X only because there is no other free version, not because it's forbidden."
    author: "Somebody"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-01
    body: "Qt is QPL too."
    author: "anon"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-01
    body: "There are two versions of Qt (apart from commercial) available for UNIX. Qt X11 and Qt embedded. Both are GPL, AFAIK."
    author: "Rithvik"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-01
    body: "The main point is that it isn't forbidden to develop a GPL version for Windows, Mac OS, etc.\nIt often looks like Qt is \"Unix only\" because of the license of the free Qt versions."
    author: "Somebody"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-01
    body: "The real fact of the matter is that even if there is no cost recovery, the cost of Qt is insignificant.  However, there are many examples of Qt code that is far quicker to develop and smaller in size than GTK or almost any other toolkit equivalent.  C++ really doesn't have much to do with it as far as I'm concerned.  Bindings mean nothing (and in fact are a bit useless for commercial development because they add another point of failure - the bane of commercial /Linux/ development).\n\nYou never have to pay for upgrades down the road unless you want new functionality.  Maintenance releases are free upgrades.  Generally those upgrades are worth the money though.  They provide real value.  The added functionality will often save development time, once again resulting in cost savings.\n\nPlease keep in mind that when I wrote this, I admittedly wrote as a Qt developer who has experienced both worlds and came to these conclusions independently.  This has nothing to do with GNOME or KDE, other than the fact that they use GTK and Qt respectively.  I don't talk about them at all.  I clearly wrote about the -costs- of development with Qt and GTK.  The licence issue does creep in because it affects commercial development, but I think it's clear (from a commercial developer's perspective, and from a KDE developer's perspective) that the licence of Qt is quite acceptable."
    author: "George Staikos"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-01
    body: ">You never have to pay for upgrades down the road unless you want new >functionality. Maintenance releases are free upgrades. Generally those upgrades >are worth the money though. They provide real value. The added functionality will >often save development time, once again resulting in cost savings.\n\nI'm not saying there is anything wrong about this kind of licensing, but if a shiny new Qt comes out with a new look and feel, you will have to upgrade to make your application fit in the the desktop, otherwise it will look and feel old and alien. This has already happened once (Qt 1 -> Qt 2).\n\nThis does add the insecurity about future licensing costs. GTK+ does not have this insecurity, since it is LGPL-based.\n\nBesides wether or not it is worth it is something that may very well change in the future. \n\nIt might be that this is all worth it, but if you are trying to be objective you should mention the downsides as well. Qt does look extremely nice, and this has nothing to do with why I like GNOME better than KDE.\n\nXimian does have a point when it comes to Star Office/Open Office. What kind, and how many licenses will actually have to be used when cross-licensing a product like this?"
    author: "Gaute Lindkvist"
  - subject: "Re: This article is just about equally biased"
    date: 2003-04-01
    body: "Quit apologizing!\n\nMichael Meeks said he wanted to see 5000+ developers join him and implied that Qt (KDE) wasn't up to the task because Qt required those developers to purchase proprietary licenses. He said that KDE was less Free because it uses a GPL library which is in direct and total contradiction to the FSF and is another putrid example of crap that GNOME people will say to disparage KDE. The slide said nothing about cross platfrom OO rather the title of the slide was 'Why GNOME on Unix' and he conveniently concluded that GNOME was the only solution and then he lists his crap FUD about Qt.\n\nWe are so worked up because it is just another in a long line of top GNOME developers spreading this ridiculous FUD that they know is patently false and repudiated by the FSF and then GNOME apologists (_you_) come here to our site and apologize for this dung. This is not a legitimate criticism and it is horribly hypocritical. It just shows the ethical lapses among some of the GNOME developers."
    author: "anon"
  - subject: "Oh please..."
    date: 2003-03-31
    body: "Come on guys... get over it... this is getting really silly and annoying."
    author: "Michele"
  - subject: "pot. kettle. black."
    date: 2003-03-31
    body: "Do you have anything to say that's not silly and annoying?"
    author: "anon"
  - subject: "Dual licensed code, Open Office, Mozilla, and QT"
    date: 2003-03-31
    body: "This discussion has raised an interesting point. Is it possible to base community-supported dual-licensed free software, such as Open Office or Mozilla, on QT? Any contributions to these projects would also have to be dual licensed. If the free QT code has to be GPLed, only those who owned the proprietary version of QT could contribute. This would rule out the vast majority of free software coders in the community."
    author: "Alexander"
  - subject: "Re: Dual licensed code, Open Office, Mozilla, and QT"
    date: 2003-03-31
    body: "The free code doesn't need to be GPL'ed.  It can be almost any open source license."
    author: "anon"
  - subject: "Re: Dual licensed code, Open Office, Mozilla, and "
    date: 2003-04-01
    body: "Can free QT be linked to SISSL, NPL or MPL code? Is there a web page somewhere that spells out what licenses free QT can linked to?"
    author: "Alexander"
  - subject: "Re: Dual licensed code, Open Office, Mozilla, and "
    date: 2003-04-01
    body: "FreeQT can link to any Free or Open Source license. It's spelled out in the QPL wha the requirements are. If you need a list of Free and Open Source licenses, I suggest you look at www.gnu.org and www.opensource.org.\n\nIt can't link to \"pure\" SISSL, because that license is neither Free nor Open. But you CAN link OpenOffice to Qt, since OpenOffice is is not \"pure\" SISSL."
    author: "David Johnson"
  - subject: "Nice choice of topic icon."
    date: 2003-03-31
    body: "Just want to say that the bomb icon for this story seems appropriate ;-)\nThanks, editors! \n"
    author: "Sad Eagle"
  - subject: "One satisfied customer"
    date: 2003-03-31
    body: "As someone who works for a company that owns Qt commercial (for unix), I'd like to offer my point of view.\n\nManagement tended to be overly estatic about this Linux development environment because it's \"free\".  So, obviously, there needed to be some justification of making a $2300 purchase in order to support this \"free\" linux system.\n\nWhen first learning about what was available a few years ago (GNOME and KDE) I set up machines to evaluate both.  We chose KDE, and it was 95% because of Qt; it's API, it's professionalism, the signal/slot idea, etc.  All of the software we've developed with it has been proprietary in house stuff; we haven't made any money off of software sales - just off of its use.\n\nBut I'm sure other people may feel differently.  Obviously if you're a lone consultant, it may be a bit too expensive for you to attempt to spend money up front in order to make money off of software.  I suppose that's part of capitalism.\n\nBut I am a happy Qt commercial user, and we will continue to support KDE development, and pay for Qt, as long as we're still writing software for Linux.\n\nIf a GNOME advocate can point me in the direction of information/screen shots of something that can match the power of Qt/KDE/ and KDE kiosk mode, while providing clean documentation, a quarterly newsletter, and 1 year of e-mail support, I'd be interested in finding out more information.\n\nUntil then, KDE and Qt well exceed our needs, and I'm happy to be able to use them."
    author: "Caleb Tennis"
  - subject: "Re: One satisfied customer"
    date: 2003-04-03
    body: "If you keep your software in-house, why did you buy a commercial QT licence?"
    author: "Chris"
  - subject: "Re: One satisfied customer"
    date: 2003-04-03
    body: "It's a requirement of the license, but also because we are now creating embedded products that will be sold utilizing Qt."
    author: "Caleb Tennis"
  - subject: "Re: One satisfied customer"
    date: 2003-04-03
    body: "Is it really a requirement of the licence? I don't think Trolltech forbids the use of the free version for in-house development, in fact even if they wanted to they couldn't since they use the GPL."
    author: "Chris"
  - subject: "Re: One satisfied customer"
    date: 2003-04-04
    body: "Just let him be happy that he helped a poor little company whose only product he happens to really like. ;)"
    author: "Datschge"
  - subject: "Re: One satisfied customer"
    date: 2003-04-04
    body: "See: http://www.trolltech.com/developer/faqs/free.html#q9\n\nUsing the Free Edition, can I make software for internal use in my company/organization? \n\nThe Qt Free Edition is not intended for such use; it is our policy that when you are using Qt for free, you should in return contribute to the free software community. If you cannot do that, you must get Professional/Enterprise Edition licenses instead. \nNote that software developed with Qt Free Edition must be distributed as free/Open Source software; i.e. the receivers must be free to give it to whomever they like. In-house distribution is no exception to this rule, of course. \n\n"
    author: "Caleb Tennis"
  - subject: "Re: One satisfied customer"
    date: 2003-04-10
    body: "It doesn't matter if they believe that the software is \"not intended for such use\" - such vague statements don't change the rule that *distribution* invokes the relevant clauses of the GPL. There could be a possibility of differing interpretations of distribution: does internal distribution count as distribution under the GPL? But if you were to provide source code internally in your company, I can't see how there can be any compulsion to buy non-free Qt licences.\n\nSince you're going on to release closed source applications based on Qt, this argument is irrelevant, however."
    author: "GuestX"
  - subject: "flamebait, troll and FUD!"
    date: 2003-04-01
    body: "First of all, OpenOffice dosen't even use GTK, it uses it's own custom rendering mechanism. This mechanism inherits the colour scheme from kde! So it looks just like native kde program.\n\n The OpenOffice 1.1 beta is vastly improved and is a lot faster, has anti-aliased fonts and adopts more of the freedesktop standards.\n\nQt is a much better professional toolkit than gtk. The revenue that trolltech goes into qt development and in turn helps kde. GTK has little support, looks ugly, less portable, less ergonomic and flexible, the \"cost\" of gtk is ximian/gnome/gtk's only \"excuse\" for their cruddy software.\n\n Still, the kde peole should make at least the kde core independant from any one toolkit, so people can make wrappers to be able to develop for kde in the toolkit of their choice. "
    author: "KDE User"
  - subject: "Re: flamebait, troll and FUD!"
    date: 2003-04-03
    body: "But open office will use Gtk sooner than you think.  :)  \n\nGTK has plenty of support, is very very portable, and can look like whatever you want.  In fact Gtk on my desktop looks absolutely gorgeous.  "
    author: "joe"
  - subject: "Re: flamebait, troll and FUD!"
    date: 2003-04-03
    body: "This will happen as soon as GNOME fullfills its promise of becoming the GNU Network Object Model Environment.  Very, very soon.  Any day now.  Any day now the GIMP will also get a usable interface.  Yeup.  And you can theme it in all sorts of ways that puts WinAmp to shame!!"
    author: "anon"
  - subject: "It's not bad if QT cost a lot"
    date: 2003-04-01
    body: "Hey, don't forget that while the license fee of QT is bad for commercial developer, it's still good for OpenSource project. Troll tech is, at the same time, making money (so they continuely ameliorate QT), and stimulating the Open Source movement. "
    author: "werfu"
  - subject: "Re: It's not bad if QT cost a little"
    date: 2003-04-01
    body: "> Hey, don't forget that while the license fee of QT is bad for commercial developer...\n\nDon't be taken in. This is total horse crap. Pick up a copy of Programmers Paradise, the software catalog, and start looking at the prices for programming tools. Just an editor can easily be $400. M$ .NET tool kit was at least in the area of two grand IIRC and I seem to remember something like over $3000 for developer network access. For that matter the enterprise version of Kylix is $2000. A full meal deal copy of IBM Webshere is around $50,000. Do you suppose they price it like this for no reason?\n\nPeople who complain about the cost of software tools and support are just not grounded in reality. It's dead easy to spend $5,000, $10,000 or more setting up a developer under windoze for commercial application development without ever touching a copy of QT. I think the reason they sell so many copies is that a lot of people tend to agree with me that it's a heck of a deal. If I were not so dedicated to open source software and wanted to develop a commercial cross platform app I'd be buying QT without batting an eye. And the only thing I'd complain about on windoze is the cost of an M$ compiler, mainly because I don't trust their tools. That's why OS/2 ran windoze over 10% faster. They used a Lattice compiler. ;-)"
    author: "Eric Laffoon"
  - subject: "the presentation"
    date: 2003-04-01
    body: "of course, the argument against qt amounted to only 1 out of 16 slides...\nI hope people actually take a look at what has been accomplished with open office--check out the before and after pictures: \nhttp://marketing.openoffice.org/conference/presentations-pdf/fri1200/GnomeIntegration.pdf"
    author: "pnut"
  - subject: "Re: the presentation"
    date: 2003-04-01
    body: "Well, basically that is just PR talk.\nI guess Ximian try to earn money somehow.\n\nTrolltech on the other hand is earning money,\nguess why?"
    author: "KDE User"
  - subject: "Kylix"
    date: 2003-04-01
    body: "The price of QT is reasonable for certain types of commercial development. ie if you work full time, or you buy unix servers and SAP, it's cheap.\nFor a hardware company (like us) that would like to make its programs cross platform, for essentially ideological reasons the cost is prohibitive. \n\n95% of visitors to my pages are windows users. 98% of customers. No customers to date have made anything WORK on linux. \nThe fact is that the linux version won't make me a cent. Not having it will lose me no sales. On that basis I can never recover the 3 months wages that cross-platform QT costs. Remember that for the price of QT I get the whole RAD development platform on Windows, not just a gui toolkit.\n\nFor me Kylix was the answer. It is the RAD tool I want, its cross-platform, and the QT license is bundled in it at the right price.\nUnfortunately there seems to be no integration with KDE, and it seems stuck at QT2, \n\nI have no problem with Trolltech setting thier price, they gotta eat. But the present price excludes shareware, small project developers, and those who don't see a commercial return from linux/qt versions of thier software.\n\nPerhaps they should offer a commercial Kdevelop/QT license for $99, or get far more aggressive in partnering deals as they have done with Kompany and Borland, so we can use the tool."
    author: "Simon"
  - subject: "Re: Kylix"
    date: 2003-04-01
    body: "Be careful when you quote the stuff like \"95% of visitors to my webpages are windows users\". If I visited your pages I'd look like a windows user too - I'm definitely not! I haven't used it for years, but to avoid poorly designed webpages I've sent Konqueror to identiify itself as IE5.5 running on win98."
    author: "Ed Moyse"
  - subject: "Re: Kylix"
    date: 2003-04-01
    body: "Heheh, I didn't even realise I was surfing as a Windows user until I happened to be checking some logs and got the shock of my life...  had changed the User-Agent in Konq some time back!"
    author: "Navindra Umanee"
  - subject: "Ximian and Sun boot KDE from show?"
    date: 2003-04-01
    body: "Look at all the Ximian and Sun speakers here in bold no less:\n\nhttp://www.realworldlinux.com/jsp/ts/100454/index.jsp?cnt_id=10\n\nKDE was supposed to have a speaker at this show but was silently kicked off when Ximian and Sun joined:\n\nhttp://lists.kde.org/?l=kde-promo&m=104915592612090&w=2\n\nNow tell me again that Ximian isn't evil?  This isn't the first time they have hit at KDE somehow.  Who suffers?  It's not just KDE who suffers when big corporations act like this.  Everybody suffers."
    author: "Anonymous"
  - subject: "Re: Ximian and Sun boot KDE from show?"
    date: 2003-04-01
    body: "Who are we to complain about this? Well _if_ they did so... that's their politics... it's their game and their money... KDE has not made a millions of dollar / euro / pound / whatever deal like Gnome... so let's face the facts: It's just like this now....\nOf course every big project must feel a bit jealousy of the Gnome project... hey, KDE could need some millions of dollars, too (who doesn't?)\nIf the G-team is using its newly gained power (money _is_ power) wisely they can even make KDE looking like a childish project; Competition is not 100% fair every time, you have to use your connections... But so far the Gnomers have been (mostly) fair...\n\nSo what? Me thinks KDE is the better desktop and thanks to it's GPL-ness nothing can really prevent further development of KDE. It's just that KDE will be locked out from some important decisions for the coming corporate Linux desktop. Only the future can tell, if this is going to be dangerous for the KDE-project."
    author: "Thomas"
  - subject: "Re: Ximian and Sun boot KDE from show?"
    date: 2003-04-01
    body: "I agree.  They are acting like Microsoft and trying to create a monopoly.  \n\nWe should get this news out and organize a boycott of Ximian and Sun."
    author: "anon"
  - subject: "Re: Ximian and Sun boot KDE from show?"
    date: 2003-04-01
    body: "Yep, this reminds me of when Ximian advertised in google on KDE search. Come on, these low handed blows are not what you would expect from a company that is supposadly friendly towards the free software company. \n\nOf course, I would however, expect this from Sun, who has had a long history of being quite ambivalent of free software. \n\nSun wants control.. they got that with GNOME. They got that with CDE. They get that with Java. They are just a mini-Microsoft."
    author: "Til"
  - subject: "Re: Ximian and Sun boot KDE from show?"
    date: 2003-04-02
    body: "> We should get this news out and organize a boycott of Ximian and Sun.\n\nAnd why hasn't this already happened 3 years ago? Back then HelixCode and Eazel were The Ultimate Evil(tm)."
    author: "FooBar"
  - subject: "Re: Ximian and Sun boot KDE from show?"
    date: 2003-04-03
    body: "There was no evidence presented that Ximian or Sun were responsible. Sounds like pure paranoia to me."
    author: "AN"
  - subject: "two things"
    date: 2003-04-01
    body: "1) It always amazes me how so many people that have such \"GNOME Superiority\" complexes always seem to make their way onto a KDE forum :)\n\n2) Seems to me the main problem is people who want something for nothing. You want to make money off of someone elses hard work - hard work that they donated in the belief that people deserve free software - yet aren't willing to pay for it. If you plan to make commercial software then I don't see anything wrong with charging for a licence. If you don't, then you have no gripe about Qt's prices. It's just like anything else in the world - if you want to make money off of it, you need to pay (i.e. a business licence, various certifications...)"
    author: "Chris Spencer"
  - subject: "Re: two things"
    date: 2003-04-01
    body: "You know, some people never needed to lift one finger to get what they want,\nthat's the truth behind those kind of comments."
    author: "KDE User"
  - subject: "Re: two things"
    date: 2003-04-02
    body: "> 1) It always amazes me how so many people that have such \"GNOME Superiority\"\n> complexes always seem to make their way onto a KDE forum :)\n\nYeah this forum is flooded with pro-GNOME \"GNOME IS SO MUCH BETTER THAN KDE!!!\"-posts.\nArgh, I must be blind, I can't find them!\n\n\n> 2)\n\nThis is only morally correct. But companies (especially big ones) are more interested in saving money than doing the morally correct thing.\n\nBut whatever they do, they are always wrong. Look at these situations:\n1) Company doesn't care about morals and seeks profit at all costs. People whine about how that company is evil.\n2) Company cares about morals. But because of that they make very little or no profit. People STILL whine about them, but this time about how they don't have a good business model and how they will never make money etc. etc.\n\nYou see? Damned if you do, damned if you don't."
    author: "FooBar"
  - subject: "Re: two things"
    date: 2003-04-02
    body: "stay off the drugs"
    author: "Chris Spencer"
  - subject: "For those who question licensing"
    date: 2003-04-02
    body: "Please have a look at this:\nhttp://freshmeat.net/stats/#license\n\nYou will see similar statistics here:\nhttp://sourceforge.net/softwaremap/trove_list.php?form_cat=13\n\nQt-free can be safely used with the overwhelming majority of free software projects, and in fact can be used with all commercial projects through commercial arrangements.  This was not the focus of the article, but it seems to be a constant sticking point for those arguing against it.  I find it strange that people who do not even use Qt would be so adamant and vocal against it.  It's not perfect for everyone, or there would not be alternatives.  However, let's not spread lies."
    author: "George Staikos"
  - subject: "Re: For those who question licensing"
    date: 2003-04-08
    body: "Wait a minute.  GPL is compatible with all commercial software?!\n\nYeah right.  Lets say KDE was commercial.  Sure, they could sell it, but then anyone who bought it would only need to buy 1 copy for their entire organisation and not only that, but they could turn around and re-sell the software for profit.  KDE would sell 1 copy.\n\nSorry, that doesn't sound like much of a business model.\n\nOn top of that someone could buy 1 copy of KDE, then give it away for free.  No one would ever buy anther copy from KDE, ever."
    author: "Grasis"
  - subject: "all this is about redhat"
    date: 2003-04-02
    body: "all this \"flame\" is about trying to make kde default for redhat..\nthey don't like it, but mandrake does...\nso the things will be better if both side make something about it...\nredhat and qt ..."
    author: "bozhan"
  - subject: "You are completely off topic."
    date: 2003-04-02
    body: "Are you aware of the fact that you are the first one writing a post just for the purpose of bringing Red Hat into this mud even though they aren't involved this time?"
    author: "Datschge"
  - subject: "Gnome?"
    date: 2003-04-02
    body: "Why do people come here to talk about Gnome, Openoffice etc.\n\nThis id dot.kde.org. This is where kde-fans meet to talk about\nkde, is that so hard to understand?"
    author: "KDE User"
  - subject: "Re: Gnome?"
    date: 2003-04-03
    body: "\nGood question.  I guess the real question is why the people posting here feel the need to bash GNOME and make false claims about GNOME which the GNOME users and developers feel they have to defend."
    author: "joe"
  - subject: "Re: Gnome?"
    date: 2003-04-03
    body: "Oh, like the GNOME trolls don't spread KDE FUD on gnomedesktop.org and Slashdot.  Wow, what saints you are.  Now you bring your holy KDE fud and propaganda here.\n\nNo thanks."
    author: "anon"
  - subject: "Re: Gnome?"
    date: 2003-04-03
    body: "Give an example of KDE FUD on the gnomedesktop.org to prove your point. Otherwise it's just FUD you are spreading as well."
    author: "Soup"
  - subject: "Re: Gnome?"
    date: 2003-04-05
    body: "I agree with you. I dont see why there has to be ANY bashing from anyone.\n\n*sings kumbaya* :)\n\nKDE r0x0rs!\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "'KDE \"Haxorrrd\" Dot News'?"
    date: 2003-04-02
    body: "Hi!\n\nWas that an April fools joke or had the dot really been compromised a few hours ago?\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: 'KDE \"Haxorrrd\" Dot News'?"
    date: 2003-04-02
    body: "I'm surprised anyone saw that.  :-)"
    author: "Navindra Umanee"
  - subject: "Re: 'KDE \"Haxorrrd\" Dot News'?"
    date: 2003-04-02
    body: "Hm, is that a) or b)? ;)\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: 'KDE \"Haxorrrd\" Dot News'?"
    date: 2003-04-03
    body: "Well, it was supposed to be a joke but it was only up for a few seconds anyway. :-)"
    author: "Navindra Umanee"
  - subject: "More mindless cheerleading"
    date: 2003-04-07
    body: "The biggest problem with both projects is the mindless cheerleading done by it's individual proponents. As someone who has developed both GTK+ and QT applications, I can tell you that development time for both is exactly the same once you have a familiarity with the library. \nGeorge Staikos's entire article is completely inflammatory and does nothing to improve KDE's image. (stuffed monkeys, why michael meeks does not make a living from software?). Someone needs to read or re-read the Advocacy HOWTO.\n"
    author: "ItchScratcher"
  - subject: "Re: More mindless cheerleading"
    date: 2003-04-07
    body: "Ximian is at fault here for their mindless KDE bashing.  What GTK+ and Qt applications have you developed, exactly?  Show us the code and prove your point."
    author: "Anonymous"
  - subject: "Come home...you tired programmers!"
    date: 2003-04-07
    body: "If you wish to program and make money then you know where you must be...on the Windows.\nWe will have Linux on the server, you can put in any desktop for configuration and management.\nThe fact is most, if not all, of you here already have Windows so why waste time on useless argumemnt?\nWith free .NET SDK now available, it is now becoming programming festival on Windows - you are invited ;-)\nWhy work enhancing and promoting QT for free while somebody is selling it and you cannot or even sell applications you develop with it?\n\nBest regards,\nPaul.\n"
    author: "Paul Selormey"
  - subject: "Re: Come home...you tired programmers!"
    date: 2003-04-09
    body: "Well, that's a very simple world-view you seem to have. Firstly, software isn't only shrink-wrapped and sold to punters, many other kinds of financial transactions involving the production of software go on in the technology industry. Secondly, .NET is \"free\" only in the \"no money up front\" sense of the word - if you're the sort of person who marvels at how \"cheap\" games consoles are, for example, I suppose you'll have missed the real point of Microsoft's incentives.\n\nStill, you'll probably realise what Microsoft's intentions are later on, and like many formerly-willing Microsoft stooges, I'm sure we'll hear the whining loud and clear: \"Microsoft's licensing just isn't fair!\" And so on."
    author: "GuestX"
  - subject: "Buying licenses"
    date: 2003-04-07
    body: "I've been thinking about this issue for the last week, and for the life of me, I can't think of a scenario where Ximian would need to buy licenses is if they wanted to port a qt-based OOo to Windows, because Trolltech's download page for the non-commercial qt for Windows says that it only supports Microsoft Visual C++.  That being the case, Ximian would have to pay for a lot of MS licenses, which most likely would not sit well with them.\n"
    author: "Mike"
  - subject: "Apples and oranges"
    date: 2003-04-08
    body: "Michael and George are both missing an important point: GTK+ and Qt are two very different toolkits.\n\nMichael is forgetting that Qt offers many more features than GTK+, most notably SQL and XML support, plus a UI designer tool, and more. GTK+, on the other hand, is basically just a GUI toolkit with some added utility functions.\n\nAs for George, he's forgetting that GTK+ is C and Qt is C++, so when you port a C application to C++, you will likely have a 10% code reduction just by the nature of C++, no matter what toolkit you use. He also fails to mention anything about gtkmm, an LGPL'd C++ wrapper that brings the benefits of C++ to GTK+.\n\nI'm also confused about George's focus on developer support. He seems to think that GTK+ developers will need help creating dialog boxes and custom widgets. I'm sorry, but if the developers on your team need help using the GTK+ toolkit, then you'd better fire them for incompetency. I mean, even freshman CS students are capable of building applications with these high-level kits. KDE, for example, was built mostly by undergrads.\n\nOf course, some developers might need help with the more advanced features of Qt, such as the Motif and ActiveX integration, but then we're no longer comparing GTK+ and Qt, are we? Once again: apples and oranges.\n\nLike so many arguments, the debaters are both right and wrong. The issue is not \"How much does it cost?\" but rather \"How much does it cost for what I want to do?\" If you develop commercial, closed-source software and want a free GUI toolkit that works great on Unix systems, choose GTK+. If you need the advanced features of Qt and can pay for the license or distribute your code open-source, choose Qt.\n\nTrevor\n"
    author: "Trevor Harmon"
  - subject: "Less \"freedom\" to make money"
    date: 2003-04-09
    body: "While I can understand that Ximian and other software companies don't want to be dependent on some other commercial enterprise whilst making money from proprietary software, it seems to me that up-front licensing costs such as those associated with Qt are actually pretty reasonable if your business is \"make money by selling software\". Moreover, if the software you're selling is GPL'd (like the not unreasonable model The Kompany has/had) you don't even need to pay Trolltech for Qt-on-X11 licences, and X11 is surely the most pertinent platform under discussion here. To top it all off, many people would go for wxWindows in preference to Gtk+ even if the superior cross-platform capabilities of wxWindows were ignored.\n\nIt seems to me that the tired arguments put forward by Ximian serve only as a distraction; indeed, one which only Ximian seems to be taken in by."
    author: "GuestX"
---
Last week, CORBA-lover <a href="http://www.gnome.org/~michael/">Michael Meeks</a> released <a href="http://www.gnome.org/~michael/XimianOOo/img7.html">some slides</a> that caused something of a stir amongst some in the KDE community.  In his slide, Michael Meeks attempted to make the case for GNOME as the only viable desktop on Unix  by directing the heat of his argument at the cost of <a href="http://www.trolltech.com/">Trolltech</a>'s <a href="http://www.trolltech.com/products/qt/">Qt</a> -- the wonderful cross-platform toolkit on which KDE is based -- for proprietary development.  Rising up to the challenge, <a href="http://people.kde.org/george.html">George Staikos</a> has written <a href="http://www.staikos.net/~staikos/whyqt/">a nice article</a> that compares the cost of Qt vs <a href="http://www.gtk.org/">GTK</a> in the real world.
<!--break-->
<p>
George successfully makes his case independent of the specifics, but there are few things in particular about Michael's slide that bother me.   For one, Michael seems to be making a skewed comparison of GNOME to Qt as opposed to comparing GNOME/GTK to KDE/Qt.  If comparing KDE to GNOME, then certainly two of the three points in favour of GNOME count for KDE.  
<p>
Other than the points George makes in Qt's favour (check out the dirt on the new <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/testwidgets/tab/">tab widget</a>!), and the fact that KDE more or less maintains a <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/qt-copy/">copy of Qt</a> in CVS, KDE has a very open development model, and I would even argue that in many ways KDE development is more open and accommodating to contributors than the GNOME development model which is heavily influenced by the commercial powers behind the <a href="http://foundation.gnome.org/">GNOME Foundation</a>.  Personally, I have also seen a lot of whining about how closely-controlled (by Red Hat) GTK development is.  <!-- Less reliable, though more colourful, sources make a href="http://dot.kde.org/1048780349/1048849685/"further claims/a. --> And, of course, the ABI/API stability Michael claims for GNOME certainly applies just as equally to KDE/Qt.
<p>
Another thing that puzzles me are <a href="http://www.trolltech.com/products/qt/pricing.html">the prices</a> Michael quotes for proprietary Qt development.  He is talking about proprietary development on Unix, and yet he quotes the prices for Qt on three platforms (Mac/Windows/X11) which is twice the price of Qt for X11 alone.  And if he does that, he really ought to take into account the state of GTK on Windows and Mac (ports of which do indeed exist).  But these quibbles are sort of besides the point, since indeed Qt does cost <i>something</i> for proprietary development whereas GTK is gratis up front.
<p>
So, by all means, read <a href="http://www.staikos.net/~staikos/whyqt/">George's article</a> carefully, then tell us what <i>you</i> think about this whole matter.  Have <i>you</i> used Qt for free or proprietary development?  If so, what have been your experiences?