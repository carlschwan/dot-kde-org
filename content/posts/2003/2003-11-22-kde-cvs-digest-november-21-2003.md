---
title: "KDE-CVS-Digest for November 21, 2003"
date:    2003-11-22
authors:
  - "dkite"
slug:    kde-cvs-digest-november-21-2003
comments:
  - subject: "Thank you "
    date: 2003-11-22
    body: "Thanks for the updates once again, Derek!"
    author: "Mike"
  - subject: "As always: Thanks Derek!"
    date: 2003-11-22
    body: "I think it's things like this digest which\nmake the KDE community one of the best in the\nopen source world because it let's even us non-programmers\nparticipate in the progress of our favourite project.\nBTW; What I'm definitely missing is an easier way\nto create fully compiled KDE apps. Does anyone know\nif there is any progress with a Lazarus-QT/KDE-plugin?\nThat would be great!\n\n\n"
    author: "Mike"
  - subject: "chess"
    date: 2003-11-22
    body: "Will a Chess client be integrated into KDE games? I was told there were several.\n\nEsp. in the games package I think there mus be more unification. Perhaps Atlantik will create a nice framwork for board games."
    author: "Gerd"
  - subject: "Knights"
    date: 2003-11-22
    body: "A very good client is knights. It is not part of the KDE base distribution, but it works very well."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Knights"
    date: 2003-11-22
    body: "Knights is probably the ebst chess client for Linux, and there are plans to include it in the next KDE version after 3.2 from what I've heard."
    author: "Mike"
  - subject: "Re: Knights"
    date: 2003-11-22
    body: "Nice. Last time I looked, Knights was still only for KDE 2.x.\n\nCompiling now. :^)"
    author: "Anonymouse"
  - subject: "Services"
    date: 2003-11-22
    body: "Hmm, never seen these services before. is there any documentation availbable? Clicking \"Help\" just leads me to the general KControl docs.\nWhat does KDE internet daemon do? And why is it running?\n\ncu Mike"
    author: "Mike"
  - subject: "wow"
    date: 2003-11-22
    body: "> A year ago, RC3 for the 3.1 release was prepared. The bulk of the work was bug fixes in preparation for the release.\n\nWow, can't beleive 3.1 was so long ago. "
    author: "anon"
  - subject: "Konversation"
    date: 2003-11-22
    body: "Cool, CVS Digest is covering Konversation now - it's quickly becoming my favorite GUI IRC client on Linux. Rock on."
    author: "Eike Hein"
  - subject: "Flash plugins"
    date: 2003-11-22
    body: "Just a note that flash plugins were fixed in CVS HEAD as of yesterday. I've downloaded the code this morning and it seems to work fine."
    author: "Vic"
---
In this <a href="http://members.shaw.ca/dkite/nov212003.html">week's KDE-CVS-Digest:</a> Read about <a href="http://xmelegance.org/kjsembed/">KJSEmbed</a> and what it is good for. 
Image handling speedups in KHTML. Start of <a href="http://www.openoffice.org/">OpenOffice.org</a> table import support in <a href="http://www.koffice.org/kword/">KWord</a>. Plus a continued focus on bugfixes.
<!--break-->
