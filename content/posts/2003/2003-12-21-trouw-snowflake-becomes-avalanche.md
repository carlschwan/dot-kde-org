---
title: "Trouw: Snowflake Becomes Avalanche"
date:    2003-12-21
authors:
  - "fmous"
slug:    trouw-snowflake-becomes-avalanche
comments:
  - subject: "nice article..."
    date: 2003-12-21
    body: "Now it should find myself this issue of Trouw, and read it (and have other read it) in Dutch. IIRC Trouw has allways reported very positive on the FreeSoftware movement.\n\nI read an article in 'de Volkskrant' (an other Dutch paper) lately in which the gave OOo nasty -- partly unfounded -- spanking. I had a good word with the autor about that."
    author: "cies"
  - subject: "Re: nice article..."
    date: 2003-12-21
    body: "It's very much the presence of Vincent Dekker that makes the difference -- one of the few technology journalists in the Netherlands who really knows what he's talking about, and, what's more, who can generally convey his knowledge in a way that the general public can understand. Of course, I'm a happy subscriber to Trouw (even though their weather man didn't know that a Christmas Carol isn't poetry, and their movie reviewer thinks it's Gandolf...)"
    author: "Boudewijn Rempt"
  - subject: "Re: nice article..."
    date: 2003-12-22
    body: "\"I read an article in 'de Volkskrant' (an other Dutch paper) lately in which the gave OOo nasty -- partly unfounded -- spanking. I had a good word with the autor about that.\"\n\nLet me guess....the author was Peter van Ammelrooy, notorious for writing negative articles about Linux and found on the \"key journalists\" list which was inadvertently leaked by MS a few years ago, where it says (translated):\n\n\"Kees Kraaijeveld writes about IT topics for the finance/economics redaction, together with Peter van Ammelrooy. It would be wise to target these two together\".\n\nSource: http://www.tonie.net/images/microsoft_keyjournalisten_NYC_2002_draft.xls \n\nSo even though I don't want to say that MS has this guy in their pockets (after all, he might just dislike Linux because it is \"so hard to install, blahblah\"), I find it really \"interesting\" that a journalist for a newspaper that would be placed in the left political corner by most people consistently writes negative articles about Linux and Open Source (and about Apple/Macs as well, for that matter).\n\nIn my experience, when De Volkskrant writes something positive about Open Source/Linux/anything non-MS, you don't even have to look at the author to know it won't be Peter van Ammelrooy :("
    author: "Wilke Havinga"
  - subject: "Nice!"
    date: 2003-12-21
    body: "KDE developers are not only excellent coders, but good PR people too!"
    author: "Haakon Nilsen"
  - subject: "KDE/MAC"
    date: 2003-12-21
    body: "KDE on MAc? Public perception is that MAcOS was better than Linux/KDE. I don't think this is true anymore. The only thing I lacked at KDE were my old dos games. then I installed dosbox and everything run fine. so I canceled my win partition. \n\nWhy does this article have to mention Lindows, afaik this Linux-ripoff plays no role at all in Europe?"
    author: "Heini"
  - subject: "Re: KDE/MAC"
    date: 2003-12-21
    body: "Michael Robertson of Lindows was in the Netherlands last week to give support to the dutch Lindows reseller that got bullied by Microsoft."
    author: "Waldo Bastian"
  - subject: "Re: KDE/MAC"
    date: 2003-12-21
    body: "I don't like that kind of media campaigns. He is focussed on bashing MS as a PR model. so he serves the journalists who like to write about the pseudo battle: win vs. Linux.\n\nWe know how large companies act... but this little dutch company was irrelevant. And Lindows is irrelevant on the european market as we can get more from Mandrake/suSe ecc. "
    author: "Jack"
  - subject: "Re: KDE/MAC"
    date: 2003-12-22
    body: "\nIspires me to:\n\nWhen they killed me as the last one, nobody was left to think I was \"relevant\".\n\nEvery little company that is promoting something Linux is on our side and relevant to us.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Translation?"
    date: 2003-12-21
    body: "\"Each version has a release dude\"\n\nAnd what is \"dude\" in Dutch? :P\n"
    author: "RMS"
  - subject: "Re: Translation?"
    date: 2003-12-21
    body: "gozer :-)"
    author: "Waldo Bastian"
  - subject: "No presence in Europe???"
    date: 2003-12-21
    body: "Have you seen http://choicepc.com ???\n\nHans"
    author: "Hans"
  - subject: "Thank you Lindows.com"
    date: 2003-12-21
    body: "Thank you Lindows.com for supporting KDE.  I noticed they support KDE League financially as well as are the main sponsor for KDE-look.org.\n\nhttp://info.lindows.com/opensource/\n\nIt's nice to see the popular distros contributing to KDE.\n\nRob Z."
    author: "Rob"
  - subject: "While Linux Fueds, Windows Rules!"
    date: 2003-12-21
    body: "Linux geeks wonder why Microsoft continues to kick their sorry a@#$'s.  It's because most Linux geeks spend more time fighting over who should get credit (ala \"why even mention Lindows\" blah, blah, blah) than MARKETING Linux to the MASSES.\n\nI think most Linux geeks LIKE having KDE a big secret.  It makes them feel special that they are the only ones who can figure out how to find, install and use Linux.\n\nHopefully this will eventually change.  Linux is a better choice for the masses, but they need to know about it.\n\n"
    author: "Bill Gates  =)"
  - subject: "Re: While Linux Fueds, Windows Rules!"
    date: 2003-12-21
    body: "Troll! Lindows is FOSS exploitation. Sell a ripped down distri, make media fuss and than sell packages that are included in other distributions by default. \n\nThere are so many easy to use and wonderful distributions that use KDe as default. We don't need Mr. Robertson."
    author: "Jack"
  - subject: "Re: While Linux Fueds, Windows Rules!"
    date: 2003-12-22
    body: "Also, this may just be a translation issue but \"the American-based entity known as Microsoft\" sounds like something out of a North Korean government statement."
    author: "Otter"
  - subject: "Re: While Linux Fueds, Windows Rules!"
    date: 2003-12-23
    body: "> Also, this may just be a translation issue but \"the American-based entity known as Microsoft\" sounds like something out of a North Korean government statement.\n\nTo me, it sounds like Star Trek talk, you know, \"the Q-entity\", \"the Borg-colective\" etc."
    author: "Gr8teful"
  - subject: "Re: While Linux Fueds, Windows Rules!"
    date: 2003-12-23
    body: "You forgot \"Carbon Based Units\". ;-)"
    author: "Steve Bergman"
  - subject: "KDE 3.2"
    date: 2003-12-21
    body: "KDE has improved. But it seems to me that the distributors decrease the usability again. For instance: Why do distributors include xboard as a default chess client to the games section. Probably because KDE Games is not shipped with a KDe chess client. it's often the first impression about integration that counts. Alpha and halfdone applications (KOffice) reduce the good impression."
    author: "Johannes"
  - subject: "Re: KDE 3.2"
    date: 2003-12-21
    body: "http://slibo.sourceforge.net/\n\nOder Knights?"
    author: "Matze"
  - subject: "KDE looks too much like Windoze"
    date: 2003-12-22
    body: "Why does the KDE desktop look like Windows? Why do we need a Windows like taskbar and 'start' button. Under Windows the more applications you have the more messy, cluttered and ridiculous the 'start' button becomes and KDE copies this mess.\n\nI'd prefer a minimum of icons on the desktop and better use of the right mouse button to click anywhere on the desktop and then select an application from an organized list. A small intelligent taskbar much like \"Black Box\" or even Apple's Dock would be much better.\n\nAnd what I really hate about all the Linux distros is that even when you install 'everything' you still get applications on the 'start' button that don't exist."
    author: "Paul Fredlein"
  - subject: "Re: KDE looks too much like Windoze"
    date: 2003-12-22
    body: "> I'd prefer a minimum of icons on the desktop and better use of the right\n> mouse button to click anywhere on the desktop and then select an application\n> from an organized list. A small intelligent taskbar much like \"Black Box\" or\n> even Apple's Dock would be much better.\n\nOkay, here goes. This is on a bog-standard KDE 3.1.4 from SuSE 9.0.\n\nDelete any icons on the desktop that you don't like. Right-click on desktop, then 'Configure Desktop'. Go to the 'Behaviour' section. For 'Clicks on Desktop', set one of them to 'Application Menu'. \n \nIf you don't like the way your distribution has arranged stuff in the application menu, edit it. Right-click on the 'K' button then 'Menu Editor', and hide programs, rearrange them or whatever. I haven't had to do this at all - SuSE generally does a fairly good job of putting useful stuff in, and in sensible places, and it's improved a lot with 9.0. You can create you own custom menus if you like, too.\n\nNow for the panel. Right-click on it, 'Configure Panel'. There's loads of options for changing the size, position, behaviour etc. Set it to auto-hide, add an external taskbar, etc etc etc. You can delete most of the buttons and applets from it if you like. The 'Window List Menu' is pretty cool if you want something equivalent to a taskbar but using much less space. You can even have that menu as a middle-click from the desktop and no visible panel at all, if you like.\n\nKDE's hugely configurable, and if some aspect of it displeases you, change it. The fact that the default setup looks a bit like Windows shouldn't discourage people. "
    author: "Adam Foster"
  - subject: "Re: KDE looks too much like Windoze"
    date: 2004-01-06
    body: "Hi,\n\nI would like to add Nescape in Programs with KDE menu editor.\nI added as root.When I login with different user I couldnt see Netscape in Menu.\nHow can I identify netscape in menu for all users.\nThanks\n"
    author: "Aysel Agdas"
  - subject: "Re: KDE looks too much like Windoze"
    date: 2004-08-27
    body: "I was organizing my Kmenu like on my WinXP machine... I put Tools, Settings, System, etc. all in a submenu called 'Main' (from way back win 3.1). I'm positive I saved it as that but that doesn't matter now. 'Main' is not there along with all it's new contents.\n\nI tried 'kappfinder', but it only found 1 internet app. I can't even find any way to set Kmenu back to 'Default'."
    author: "Saxywolf"
  - subject: "Icons..."
    date: 2003-12-22
    body: "No more Crystal icons on dot.kde.org? Just curious..."
    author: "wygiwyg"
  - subject: "Re: Icons..."
    date: 2003-12-22
    body: "We were slashdotted the other day so I switched to an offsite image server.  The image server I switched to happens to have an older version of our icons and I haven't quite had the chance to update it yet."
    author: "Navindra Umanee"
---
If you can still get your hands on a copy of <a href="http://www.trouw.nl/">Trouw</a> from last Friday, you might be thrilled to find half of page 16 dedicated to an interview with KDE contributors <a href="http://people.kde.org/charles.html">Charles Samuels</a>, <a href="http://people.kde.org/rob.html">Rob Kaper</a> and, myself, Fabrice Mous. The three of us met in The Hague with Vincent Dekker and the result is a long article in the Dutch newspaper.  A translation of the article follows with the permission of the paper.
<!--break-->
<p><hr>
<h2>Snowflake Becomes Avalanche</h2>
<p>by Vincent Dekker</p>

<p><em>Our enormous dependence on computer software from the
American-based entity known as Microsoft isn't a healthy situation.
Fortunately, the Linux community provides an alternative in the form
of free software for use on PCs and is already gaining quite some
terrain.  Despite this, Microsoft still reigns supreme in the desktop arena.
Hopefully this will not be for long as Lindows, a small competitor to
Microsoft, works to combine the power of Linux with the ease of use of
Windows.</em></p>

<p>Rob Kaper is truly a computer freak.  He's a student at the
"Academy of Digital Communication" at Utrecht and one can often find
him behind his PC even deep into the night. Fabrice Mous seems wiser:
he exchanges his study of "Design and Interaction" at The Hague for
working on the joint project only for "an average of an hour a day". And
Charles Samuels, American student of computer technology, is feeling
grumpy: "My current address in England has no internet connection so
currently I can work on KDE for barely an hour a week."</p>

<p>What binds the three is KDE, a world-wide initiative to deliver
computer users a good and especially useful alternative to Microsoft's
Windows. The meaning of K seems to be a riddle, but the D and E are the
first letters of Desktop Environment and that what it's all about. In
the world of computers the desktop is the part of the software with
which the user interacts directly.</p>

<p>"You can compare it to a car," says Rob Kaper. "Linux would be the
engine, a part where the driver might never interact with directly. A
desktop like KDE would be the dashboard with controls and warning
LEDs". Car manufacturers deliver many variants of engines but around
the dashboard everything has to be easily recognisable and usable. One
who drives a rented car should not have to search hours for the
light switch or brake pedal.</p>

<p>From that point of view Microsoft has done a big favor to the
world. As 90 percent of all desktop PCs in the world have Windows on
them and this forces other programs to offer standard procedures and
paradigms, we can easily switch from one PC to another. Starting a
program, printing something, closing windows: when somebody is
familiar with the concept of mouse clicks, you can perform the actions
anywhere.</p>


<p>In the beginning of the nineties it became clear that Microsoft was
developing a huge monopoly in the market of PC software
market. Competitors offered Unix, a somewhat older operating system
for powerful enterprise computers. They modified and polished it but
it failed miserably.  Each competitor wanted to make money and were
making their own unique versions.</p>

<p>In Finland a guy called Linus Torvalds chose another path. He made
a version of Unix which was free for all to use and enhance. Very
quickly a lot of computer enthusiasts began supporting his initiative
and that's how Linux was started, a powerful operating system that
keeps gaining territory in the operating system market.</p>

<p>But as said before, Linux is the 'engine'. What was missing is the
complementary 'dashboard', the desktop. As Rob Kaper explains, "In
October 1996, German Matthias Ettrich raised the alarm on the Internet
that every Unix vendor had its own desktop, severely complicating the
life of users.  He proposed to develop an integrated desktop within the
community."</p>

<p>"Meanwhile what started as a little snowflake, has grown to the
proportion of an avalanche," says Fabrice Mous. Scattered all over the
world, thousands of people are working voluntarily on making
enhancements and adjustments to what has become known as the KDE
desktop. Sometimes very handy stuff like file management, Internet
access and so on, sometimes just programs for fun. "The most
important element I contributed was a Monopoly game," stated Kaper. I
wanted that myself and when I wrote the program I thought 'Why keep
this to myself?'  And most importantly, people will report bugs when you
start releasing it for the KDE platform and that sounded like a good
idea."</p>

<p>"On the other hand he also reported and fixed some bugs in other
programs," supplies Fabrice Mous.  Mous is no programmer himself
but a translator. Together with other people from KDE.nl they take
care of the Dutch versions of the program. "Right now KDE has been
translated to 57 languages and the Dutch language is always one of
the first translations available."</p>

<p>Charles Samuels from San Jose, California is a programmer just like
Rob Kaper and the two know each other from their work on the KDE.  He is
currently in the Netherlands for a brief vacation.  Samuels has written
a so-called media player for playing MP3's and other music files on
the computer. Just like Kaper he did this primarily because he wanted
to have a nice media player for himself.</p>

<p>"Don't think we are a bunch of reds who want to destroy capitalism,
because we are not," says Fabrice Mous. "People who are working on this
project come from all walks of life.  KDE in the Netherlands has
participation of managers, lots of students like us and we have a cook
who is the coordinator of our Dutch group."</p>

<p>Reds or not, the way KDE is producing new versions would have
gotten quite some sympathy from a typical idealist from the sixties or
seventies.  Fabrice Mous, "There is not one
person who has a final say in this, like Linus Torvalds has with
Linux. Everybody is equal and every contribution is equal. Although we
have the concept that we have people with an account when they want to
touch the code themselves, and people without these accounts. This is
because not everybody is going to be involved for a long time. When it
looks like somebody is going to stick around for a while then it is
useful to get write access. It is a also meant as some form of
security. You don't want outsiders to do a lot of damage to a
program."</p>


<p>"All participants of the project determine together which parts
will be added to new versions or which parts need to be enhanced.
Each version has a release dude, some kind of coordinator.  This
person has to ensure that every workgroup is releasing their
contribution on time."</p>


<p>Early next year, maybe already in January, KDE 3.2 will be released
and already people, all over the world, are planning the
successor. For several years, Mous, Kaper and Samuels have been
devoted to continuously enhancing KDE. Slowly but surely the project
is harvesting world-wide recognition. As previously mentioned, the
American-based Lindows, competitor to Microsoft, has been working
solidly on their reputation, and have been making their Linux-based
operating system LindowsOS user-friendly with the help of the KDE
desktop. The biggest American department store Wal Mart is selling PCs
with LindowsOS pre-installed on it and Seagate, the biggest
manufacturer of hard disks will deliver LindowsOS on your harddisk
without extra payments. This will contribute to make KDE the de facto
standard for Windows alternatives.</p>


<p>Mous suspects that KDE already has that kind of position in
Europe. Partly thanks to SUSE, a German company by origin that
distributes a variant of Linux mostly aimed at enterprise
deployments. SUSE too uses KDE as the desktop component of their
operating system. In principle everybody delivers their contributions
to KDE without direct monetary compensation, although some do get
paid.  Mous: "SUSE asked the KDE group if they could develop a program
to burn DVDs on the PC.  We managed to do that and thus SUSE was able
to be the first Linux distribution to supply this burner program. But
afterwards the burner program became part of KDE and it is freely
available now."</p>


<p>Companies like SUSE are not the 'owner' of a Linux/KDE version and
can not sell the program individually. Instead of that they earn their
with providing services to companies who are using Linux. And if they
put their program on a CD and make it therefore easy to install on a
PC, they can ask a fee, mostly a small one, for that CD.</p>


<p>Also the government has shown more and more interest in programs
like Linux and KDE. On one hand they want to be less dependent on
Microsoft; on the other hand they notice that the so called Open
Source Software (software where the source code is totally public) is
winning more and more in quality.</p>

<p>The German government is already a big user of KDE. Kaper: "Germany
always wanted to get away from Windows but didn't want to lose certain
features they had with Windows programs.  As a result, a number of KDE
programmers were contracted to develop software to share addresses, phone
numbers, email addresses and appointments. This work has also become a
standard part of KDE."</p>

<p>A special development is that KDE can be used on the newest Apple
computers. "Apple always had their own operating system, but the
newest, OSX, is based on a version of Unix. This makes it possible to
run KDE on your desktop of your Apple computer. I think that Apple
noticed the power we have at hand to develop better versions every
time", says Kaper.</p>

<p>Having KDE on Apple computers is an interesting proposition. Many
people might think that the program Windows is an invention of
Microsoft, but in fact Microsoft copied a lot from Mac OS which was
developed by Apple and made quite a splash in the eighties. With KDE
on Apple, the user-friendly software is about to return to its
source.</p>


<p>Rob Kaper: "It isn't about world domination, but..." -- "Well,"
interrupts Charles, "it is a little bit about world domination."
"Sure, maybe a little bit. But eventually we just want to make the
best product available"</p>

<p>&copy; 2003 Trouw</p>