---
title: "Linux Journal Readers Choice Awards are Open"
date:    2003-07-05
authors:
  - "mbucciarelli"
slug:    linux-journal-readers-choice-awards-are-open
comments:
  - subject: "Nove Hrady Conference"
    date: 2003-07-05
    body: "There's the Linux Journal winners announced in November, and there's the developer conference before that in August. I just didn't get the connection between the two. You mentioned it in the news item as if there is one.\n"
    author: "ML"
  - subject: "Re: Nove Hrady Conference"
    date: 2003-07-05
    body: "Linux Journal could cover not only the contest winners but also the KDE conference in the same issue so its readership will notice that KDE will already have improved even more at the time of reading.\n\nSome of their categories are quite strange (or equipped with strange choices) but nevertheless there's much to do for KDE enthusiasts ;)"
    author: "josef"
  - subject: "we-didn't-post-this-at-first"
    date: 2003-07-06
    body: "So, gnome has already discussed it, and seeing the options proposed, their awards are biased, even though maybe more by mistake and ignorance than bad will.\n\nFrom http://www.gnomedesktop.org/article.php?sid=1199&mode=thread&order=0&thold=1 :\n- \"Bluecurve/GNOME/XD2 are listed  separately to divide up those who like  gnome (enlightenment and  window-maker can potentially divide further as several like using those with  gnome), whereas KDE has a single  option.\"\n- \"In text editor they have:  * vi (an dother vi clones)  and then  * vim\"\n- \" In office proram they have:  * KOffice  * Kword\", but neither single Kspread (included in Koffice then) nor Gnumeric (never included)\netc.\n\nBtw, I'm not a gnome addict: I'm using both projects since both have their advantages."
    author: "Henri"
  - subject: "Re: we-didn't-post-this-at-first"
    date: 2003-07-06
    body: "Well, it's pretty obvious who will win what. KDE has won best DE for the last four or five years or so (when the options were between 4 or 5 picks).. and OpenOffice is going to win best office program. \n\nAs for Bluecurve vs. GNOME. vs XD2, I don't see how Bluecurve splits just GNOME. It splits KDE in the same way. \n\nEnlightenment and WindowMaker are usable with both GNOME and KDE (I use the latest KDE with Enlightenment), and thus, they split KDE and GNOME in the same way. \n\nI think the gnome-footnotes guys are just seeing competition between KDE and GNOME when there isn't any. "
    author: "anon"
  - subject: "Re: we-didn't-post-this-at-first"
    date: 2003-07-06
    body: "Given the head start they had on voting, they will probably win this useless poll anyway."
    author: "anon"
  - subject: "Re: we-didn't-post-this-at-first"
    date: 2003-07-07
    body: "Yeah, that's my point. I was just quoting from gnome news since it was already discussed there and not only in term of kde vs. gnome but just that: this poll is useless."
    author: "Henri"
  - subject: "Re: we-didn't-post-this-at-first"
    date: 2003-07-07
    body: "You mean, every poll is useless unless your favourite wins?"
    author: "Anonymous"
  - subject: "Mandrake and distrowatch"
    date: 2003-07-07
    body: "Mandrake is not in their list of distro's, nor distrowatch in the list of sites. How did they make their lists?\n\n"
    author: "geert"
  - subject: "Re: Mandrake and distrowatch"
    date: 2003-07-07
    body: "Yes it is, as \"Linux Mandrake\"."
    author: "Michael Goffioul"
---
Do you like KDE and its applications? Don't hesitate to tell the world. A good possibility is the <a href="http://www.linuxjournal.com/rc2003/">2003 Linux Journal Readers' Choice Awards</a> where voting is now running.  <i>"Linux Journal, the leading magazine of the Linux community, is pleased to announce that voting in the 2003 Readers' Choice poll is officially underway. Make your vote count as you tell us your favorite distribution, development tools, LJ column and much more."</i>  Voting closes July 25.  The winners will be published in the November issue of Linux Journal, which will probably come out a few weeks after the <a href="http://events.kde.org/info/kastle/">KDE Contributor's Conference</a> wraps up. 
<!--break-->
