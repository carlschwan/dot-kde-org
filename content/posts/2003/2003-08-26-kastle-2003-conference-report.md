---
title: "Kastle 2003: Conference Report"
date:    2003-08-26
authors:
  - "binner"
slug:    kastle-2003-conference-report
comments:
  - subject: "Teambuilder screenshot"
    date: 2003-08-26
    body: "This is a screenshot of the Teambuilder monitor which allows you to monitor the machines on the compile farm: http://ktown.kde.org/~howells/teambuilder.png"
    author: "Chris Howells"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-26
    body: "Teambuilder.. what an ironic name for software that Trolltech refuses to be a team player with."
    author: "Neil Stevens"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-26
    body: "Thanks for not attending Nove Hrady and disturbing all the fun we have without your flames."
    author: "Anonymous"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-26
    body: "But I could have sworn I saw his Rob Kaper euro-counterpart in the photos! :-)"
    author: "Navindra Umanee"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-28
    body: "You even can talk with Rob when he is cooled down - he should be thrown regularly into the lake to prevent overheating."
    author: "Anonymous"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-26
    body: "Matthias Ettrich? Is that you?"
    author: "Neil Stevens"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-26
    body: "Are you really Neil Stevens? Or are you Mosfet? Conspiracy everywhere."
    author: "Anonymous"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-27
    body: "Suck it."
    author: "Chris Howells"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-27
    body: "How insightful.\n\nA few people in KDE deface the front page over hypothetical patents, and a few more respond without an ounce of reason to a company that really does hold KDE's future in its hand slowly releasing more and more software under unfree licenses."
    author: "Neil Stevens"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-27
    body: "Ever heard of the KDE Free Qt foundation?"
    author: "Chris Howells"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-27
    body: "If Qt becomes unfree, then what's stopping you or someone else from forking the last GPL'ed version of it?\n\nSeriously, I see no point in your whining."
    author: "Janne"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-27
    body: "moreover, if Qt becomes unfree, the last version becomes automatically-BSD'd"
    author: "ANON"
  - subject: "Re: Teambuilder screenshot"
    date: 2003-08-28
    body: "Not automatically but by decision of the board of the KDE Free Qt Foundation."
    author: "Anonymous"
  - subject: "Neat picture"
    date: 2003-08-26
    body: "Thanks so much for the group picture. Now I can put a face to the names.\n\nWho did the mapping? Excellent job.\n\nDerek (who is heartened by seeing a few old guys)"
    author: "Derek Kite"
  - subject: "Re: Neat picture"
    date: 2003-08-26
    body: "Coolo did with the recently discovered (by him) KImageMapEditor. Of course, you can use this great app as a Quanta plugin. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Neat picture"
    date: 2003-08-27
    body: "> Coolo did with the recently discovered (by him) KImageMapEditor. Of course, you can use this great app as a Quanta plugin. ;-)\n \n> Andras\n\nYeah and I had to look at the source to find your coordinates... because you have gotten so much sun you look like a shadow. ;-) Now we are probably both the KDE developers with the longest ponytails and darkest tans. When we get together later this year I can tell us apart because you're the skinny one. ;-)\n\nEric"
    author: "Eric Laffoon"
  - subject: "D-BUS"
    date: 2003-08-26
    body: "Jonathan has a nice write-up of the D-BUS talk.  What was the reaction to it?"
    author: "anon"
  - subject: "KDE Developer Map?"
    date: 2003-08-26
    body: "Will the KDE Developer Map be updated under the conference? http://worldwide.kde.org/map/"
    author: "Anonymous"
  - subject: "Re: KDE Developer Map?"
    date: 2003-08-28
    body: "Under? During? After?"
    author: "Anonymous"
  - subject: "Hehe"
    date: 2003-08-26
    body: "Havoc holding that KDE placard... pretty funny.  hehehe."
    author: "ac"
  - subject: "Re: Hehe"
    date: 2003-08-26
    body: "It was actually \"Could you please hold this for me for a moment, I need to get something from my bag?\"  ;)"
    author: "Lubos Lunak"
  - subject: "Pretty Free"
    date: 2003-08-27
    body: ">>>After a long humorous free speech the developers celebrated with free beer, wine, barbecue and were entertained with live music.\n\nFree speech AND free beer huh?"
    author: "AC"
---
On Saturday, the <a href="http://events.kde.org/info/kastle/">KDE Contributors' Conference 2003</a> was officially opened. <a href="http://events.kde.org/info/kastle/conference_program.phtml">Two days full with talks</a> in two tracks followed only being interrupted by meals, <a href="http://developer.kde.org/~coolo/contributors.html">group photo</a> and the obligatory social event on Saturday evening. On Monday there was a PGP keysigning, a press conference was held and in workshops further work was discussed.

<!--break-->
<p><a href="http://www.suse.de/~cs/NoveHrady-Aug-2003/Day3/tn/pict1147.jpg.html">Prof. Dr. Helena Illnerova</a>, president of the <a href="http://www.cas.cz/">Academy of Sciences of the Czech Republic</a>, attended the opening ceremony led by <a href="http://www.suse.de/~cs/NoveHrady-Aug-2003/Day3/tn/pict1150.jpg.html">Dr. Dalibor Stys</a>, director of the <a href="http://www.greentech.cz/ufb/about/index.html">Institute of Physical Biology</a>, University of South Bohemia, and <a href="http://www.suse.de/~cs/NoveHrady-Aug-2003/Day3/tn/pict1151.jpg.html">Kalle Dalheimer</a>, <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> president. Heartly greetings were also submitted from Prof. Dr. Witold Jacak, rector of the <a href="http://www.fhs-hagenberg.ac.at/">Polytechnic University of Upper Austria in Hagenberg</a>, who was unfortunately unable to come. As special guests <a href="http://ktown.kde.org/~charles/novehrady/p8220009.jpg">Havoc Pennington</a> of <a href="http://www.freedesktop.org/">freedesktop.org</a>, <a href="http://www.gnome.org/">GNOME</a> and <a href="http://www.redhat.com/">Red Hat</a> fame and Leon Shiman of <a href="http://www.x.org/">X.org</a> were welcomed.
<p>
The talks attracted varying amounts of visitors depending on the talk type: There were talks summarizing current work and upcoming features like in <a href="http://www.kdevelop.org/">KDevelop</a> and <a href="http://www.kontact.org/">Kontact</a>, talks teaching general basics about design patterns and aspect oriented technologies, presentations of research studies in the usability and KDE developer structure areas, tutorials for advanced KDE programmers and conceptual talks presenting ideas which yet are missing more than a prototype.
Havoc talked in his two talks about <a href="http://www.freedesktop.org/">freedesktop.org</a> and the <a href="http://www.freedesktop.org/software/dbus/">D-BUS message system</a>. Leon presented the <a href="http://www.mediaapplicationserver.net/">MAS Media Application Server</a>.
<p>
The slides of the presentations <a href="ftp://upload.kde.org/incoming/kastle">are collected</a> and will be made available on the <a href="http://events.kde.org/info/kastle/">conference website</a> later. Jonathan Riddel provides <a href="http://www10.informatik.uni-erlangen.de/~simon/n7ywiki/wiki.cgi?action=Browse&id=TalkWriteups">
many talk writeups</a> in the <a href="http://www10.informatik.uni-erlangen.de/~simon/n7ywiki/wiki.cgi?action=Browse&id=NoveHradyWiki">conference wiki</a> for the talks he attended. Additionally several talks were recorded on video and may be made available soon.
<p>
After lunch all developers gathered on the stairs for a <a href="http://developer.kde.org/~coolo/contributors.html">group photo</a>. Don't miss the image map functionality which shows you the name in the status bar if you move the mouse over the head of a contributor! This picture will be featured in the <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">upcoming KDE 3.2 Alpha version</a> in two weeks. A photo of Thursday's smaller group of <a href="http://www.suse.de/~cs/NoveHrady-Aug-2003/Day2/tn/pict1136.jpg.html">attending KDE e.V. members</a> is available as well.

<p>
The social event sponsored by <a href="http://www.trolltech.com/">Trolltech</a> was preceded by a talk by Matthias Ettrich on Qt 4. In summary: Qt 4 will be faster, smaller, more flexible, more productive and easier to learn. Read more details in our <a href="http://dot.kde.org/1061880936/">Qt 4 story</a>. After a long humorous free speech the developers celebrated with free beer, wine, barbecue and were entertained with live music.
<p>
During the conference the possibility existed to buy <a href="http://www.kde.org/stuff/">KDE fan articles</a> like T-shirts and pins. Some of these T-shirts were handed over by the KDE e.V. board as a thank you to our host and a lot of helpers, mostly employees of the university, shepherding us without being paid additionally and in their spare time.
<p>
Monday morning after a PGP keysigning giving everyone the chance to see some embarrassingly old identity card photos, the provided <a href="http://www.hp.com/">Hewlett Packard</a> laptops were given out and were quickly integrated into the already running <a href="http://www.trolltech.com/products/teambuilder/index.html">Teambuilder</a> compilation farm running on the computers contributed by the <a href="http://www.fhs-hagenberg.ac.at/">Polytechnic University of Upper Austria</a>, bringing the total number of hooked-up machines up to 52 and leading to huge compilation speed increases.
<p>
Also on Monday the KDE e.V. issued a press release about KDE and the conference and held a press conference with attending journalists from the Czech Republic, Germany and Brazil. 
<p>
The rest of Monday was reserved for workshops where the topics were <a href="http://kolab.kde.org/">Kolab</a>, <a href="http://www.koffice.org/">KOffice</a>, KDE/Qt integration, <a href="http://usability.kde.org/">Usability</a> and Scripting Applications with DCOP. Some led directly to heavy coding activities while for others additional meetings had to be scheduled later on the same day or within the next days.
<p>
All of the above is documented in detail in eight ever-growing photo albums. You can find the links on the <a href="http://www10.informatik.uni-erlangen.de/~simon/n7ywiki/wiki.cgi?action=Browse&id=PhotoAlbum">PhotoAlbum page</a> of the conference wiki.