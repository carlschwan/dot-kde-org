---
title: "Savanna Says: Kneat Knoppix!"
date:    2003-07-04
authors:
  - "numanee"
slug:    savanna-says-kneat-knoppix
comments:
  - subject: "Savanna"
    date: 2003-07-04
    body: "Who is Savanna? I want to see a picture of her :-)..."
    author: "Anonymous"
  - subject: "Re: Savanna"
    date: 2003-07-04
    body: "Savanna is a very classy, very blonde surfer girl. She's that rare mix of physical culture meets geek chic... looking for the perfect wave by day, chatting up and writing about KDE by night... and dreaming of riding the big one in Hawaii.\n\nI haven't seen a picture of her, but I suspect it could actually be hazardous to geeks in poor cardiovascular condition. Besides I think we should focus on her wonderful contributions to the community. She writes great articles. I'm sure if she is as attractive as she is enjoyable to read we should be more concerned about the drop off in development her picture could cause. ;-)\n\nOh... You ROCK Savanna!"
    author: "Eric Laffoon"
  - subject: "Re: Savanna"
    date: 2003-07-04
    body: "anyway a picture would be nice ;-)"
    author: "Mathieu Chouinard"
  - subject: "Re: Savanna"
    date: 2003-07-04
    body: "But be aware that you might end up with Navindra wearing a wig... ;)\n\nAre you sure you want to see that?"
    author: "Nasty guy"
  - subject: "Re: Savanna"
    date: 2003-07-05
    body: "Why yes, yes I would."
    author: "weird guy"
  - subject: "Re: Savanna"
    date: 2003-07-05
    body: "Eaverytime a girl / woman is discovered in the cumunity the same happens. ;)\nAll geeks make thousends of complements and bag for a picture.\nThe same happened at linuxforen.de.\nI think there are hundereds of men-geeks and 3 or 4 girls, and one very pretty girl (if the pic wasn't fake)."
    author: "panzi"
  - subject: "Re: Savanna"
    date: 2003-07-05
    body: "How come nobody asks for my picture?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Savanna"
    date: 2003-07-05
    body: "Cause now that george fixed KSSL i can get on the sites to download them myself. :)\n\nCheers!\n-ian reinhart geiser\n"
    author: "ian reinhart geiser"
  - subject: "Re: Savanna"
    date: 2003-07-10
    body: "You stupid bunch of wombles....\nNavindra is a man..http://people.kde.org/navindra.html"
    author: "how amusing"
  - subject: "Re: Savanna"
    date: 2003-07-10
    body: "Savanna != Navindra"
    author: "Anonymous"
  - subject: "Re: Savanna"
    date: 2003-07-05
    body: "If this girl looks like the X movie actress Savanah, ooohhhhh, I never forget that deceased delightful woman. Rest in Peace pretty woman."
    author: "Jonh the XX"
  - subject: "knoppix is great"
    date: 2003-07-05
    body: "for showing off KDE/GNU/Linux... I hope promo.kde starts selling Knoppix CD's!\n\nIt's also a great way of installing debian."
    author: "p0p"
  - subject: "speaking of hot chick"
    date: 2003-07-05
    body: "there's a new moderator on the gentoo forum, it's a girl and she's a cutie.\nu can get her pic there:\nhttp://forums.gentoo.org/images/avatars/17882995803ee335db0bc5e.jpg\n\nmaybe we should make a website with pics of all the cuties in the world that like or use foss, how about a miss foss election? the winner could get a whole night with rms or linus lol"
    author: "pat"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-05
    body: "I think it's very splendid that you guys focus on that she's a girl rather than on the excellent work she's doing.  Way to make people feel like part of the community!"
    author: "ac"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-05
    body: "Well, I wanted to learn more about \"Datschge\" and other new active anonymous contributors too."
    author: "Anonymous"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-06
    body: "And who are you?"
    author: "Datschge"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-07
    body: "A non-contributor."
    author: "Anonymous"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-08
    body: "I tend to want to support whatever I like. Is that sufficient as description about me? ;)"
    author: "Datschge"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-08
    body: "If she is pretty (haven\u00b4t checked) the least she can do is carry her burden at least as gracefully as we (ok, I) carry the weight of our (ok, my ;-) .. ermm... homeliness.\n\nHell, I don\u00b4t think I ever heard a Ph.D in maths (and I know a few dozens) who doesn\u00b4t like a compliment about her hair.\n"
    author: "Roberto Alsina"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-10
    body: "She's new, we can't verify what she's done because she's new.  Good try on trying to score brownie points with the hot chick though.  Oh, but do not fear once she has gained some experience she will be looked upon with a keener eye."
    author: "3lue9"
  - subject: "Re: speaking of hot chick"
    date: 2003-07-05
    body: "Wow, you guys are really immature, aren't you?"
    author: "lit"
  - subject: "Re: speaking of hot chick"
    date: 2003-11-21
    body: "She's not hot.  Ewww.  sorry for the truth.  Pass the cookies please.  :)"
    author: "stinky crack"
  - subject: "KDE is not for everyone"
    date: 2003-07-05
    body: "I don't use KDE, but I do try to keep up with the new features.  I have installed Libranet on a few of my friends system. I showed them IceWM, as it was Default, KDE as it is more user based and GNOME.  Out of those 3, none of my friens have chosen KDE, or even IceWM.  I beleive that GNOME gives a better feel then what kde does.  QT takes forever to load and it reminds me too much of windows."
    author: "Dan"
  - subject: "Re: KDE is not for everyone"
    date: 2003-07-05
    body: "I have 4 friends (1 more than u!), and none of them use GNOME because it has no features and the buttons are on the wrong side!  They all use KDE because it looks so pretty with Keramik and Crystal, it's fast, featureful and stable."
    author: "anon"
  - subject: "Re: KDE is not for everyone (Trolls aren't either)"
    date: 2003-07-05
    body: "Anyone who thinks comparing IceWM with KDE and Gome makes sense is hardly an expert on software, not knowing the difference between a window manager and a desktop environment. Pop quiz - name the KDE and Gnome window managers?\n\n> I beleive that GNOME gives a better feel then what kde does.\nDoes that mean interface elements and are we talking 1.4 or 2.x components? Certainly you can't be talking about how they have had to really push developers to adhere to HIG elements KDE doesn't worry about much because they don't have the consistent framework KDE does.\n\n> QT takes forever to load\nHow would you know that? Qt is loaded when you see the KDE boot splash. Since KDE can easily run from one minor version to another without crashing who cares if it takes a minute longer than a simple windwo manager?\n\n> and it reminds me too much of windows.\nReally? Is it the rock solid stability or the theming? Because if you like depressing browns, foot prints and monkeys you can change the look. However it's a strange comment considering that the team that started Gnome are former M$ employees and to this day embrace and admire some of their technical work very much. Trying to do anything on windoze or Gnome drives me crazy very quickly. KDE has a lot of well thought out finishing touches.\n\nIf you're not trolling then at least try to develop some informed opinions. It's sad to see people foisting this level of ignorance on their friends."
    author: "Eric Laffoon"
  - subject: "Re: KDE is not for everyone (Trolls aren't either)"
    date: 2003-07-06
    body: "I think he likes the attractive gray colours that GNOME and Windows are forever shoving into users' faces. MS tried to copy Aqua with Luna but it couldn't resist its favourite drabby gray. GNOME loves gray too."
    author: "Antiphon"
  - subject: "Re: KDE is not for everyone"
    date: 2003-07-05
    body: "> QT takes forever to load\n\nUnless you are profiling Qt, you are just guessing at the startup time.  There's no place where KDE pops up a message saying \"hey, I've loaded Qt, so now I'll load the rest of KDE!\"  From a user's POV, the startup time of KDE and QT are inseparable.\n\n\n> it reminds me too much of windows\n\nNow this is just plain ignorant.  Qt and KDE are themable.  Some of the themes look like Microsoft Windows.  Most don't.  If you don't like Microsoft Windows, then don't use any of those particular themes.\n"
    author: "Jim"
  - subject: "YEAH! YEAH"
    date: 2003-07-05
    body: "Let's start another flame war!! :-)"
    author: "Yemu"
  - subject: "Knoppix is a great Debian installer"
    date: 2003-07-05
    body: "I've had nightmares getting Debian installed with the official ISOs, but the Knoppix hd install makes it a breeze (as long as you understand HD partitioning). Once installed its the usual apt-get stuff to get it updated and things are simple from then on.\n\nMind you, whether Debian is better than other distros is a more contentious issue - I'm keeping it alongside Mandrake and SuSE rather than replacing them - but it's certainly worth a try."
    author: "Rich"
  - subject: "The same with morphix"
    date: 2003-07-05
    body: "I had the same experience with Morphix, some kind of Knoppix relative. Downloading the KDE version (480 Mb) burning. in the cd, and a speedy working kde desktop with only openoffice missing to be fully functional. I didn't have any boot-up problems, nor bugs identified. I did not try to copy it on my harddisk though. \n\nAs I download with ISDN, and experienced problems burning full 700 mb discs, and knoppix updates nearly daily, which makes slow download difficult, I tried Morphix instead, with good results. \n\n"
    author: "Geert"
  - subject: "Knoppix or Morhics"
    date: 2003-07-05
    body: "To show off to friends, maybe Morphics would be better. It's based on Knoppix, but is more modular... There is the base part and a choice of several 'modules': so-called main modules that'll give you KDE3.1,Gnome2.2 or, get this, 500 MB of opensource games with Q3A and UT2003 demos to boot. (using IceWM), and mini modules that one can burn on small CDs and/or use as resque CDs. It is 'cleaner' than Knoppix as Knoppix shoehorns as much Gnome and KDE and other stuff in as possible.\nThe site also contains lots of info to build your own modules (multimedia, programming, 3D,...)\nLat months Linux Format has Morhics on it's bootable DVD with all main and mini modules included!!! Grab 'em while they last!\n "
    author: "UglyMike"
  - subject: "Boot problems"
    date: 2003-07-08
    body: "I also had problems with a burned Knoppix, but discovered it was mainly\nthe CD and the CD drive in my Laptop that caused the problems.\n\nFor the bootup time I showed it once to my father (knoppix with KDE 2.2)\nwho has SuSE installed, to show him that it simply starts from CD.\nAsked for comment when KDE was there, he said: \"oh its faster than SuSE\".\nWhich was surprising for me because I thought the compressed file system would be slower than HD.\n"
    author: "Harald"
  - subject: "If you don't have Broadband"
    date: 2003-07-08
    body: "Try http://www.almostfreelinux.com who sells all knds of distros.\nIncluding a few you may not have heard of."
    author: "chattnos"
  - subject: "so sad."
    date: 2003-07-08
    body: "Great artical.  Embarrassing \"what's she look like!\" responses.  Unbelievable.  I will return to the site for the articles.  I will skip the comments section in the future.  What a bunch of CREEPY sad responses."
    author: "it doesn't matter"
  - subject: "Bad article, worse comments."
    date: 2003-07-09
    body: "When reading the article, I was not at all impressed with the quality of writing, and upon getting to the comments, I became even more annoyed.  As soon as a bunch of geeks see that a girl is interacting with them at all, they immediatley herald it as the greatest thing to ever happen and in the same instant completley shut down any reasoning they have.\n\nThis article is very badly written: she is repetitive, doesn't offer nearly the amount of substance one would expect from such a long article, and makes her self out to not understand what's going on. \"Apparently that is what the Checksum file is for\": she could have at least researched checksumming and given a quick overview of what it actually does; after all, she is complaining that the FAQ glosses over the issue.\n\nFolks, get the blood back to the correct hemisphere of the body, and start using your brain.  After reading the 85 million comments from desperate, anti-social geeks who wanted nothing more than to see a picture of the author, I went and quickly read her other articles, and much to my non-surprise, they're quite similar.\n\nDon't applaud someone simply because they're of the of opposite sex, even in such a female-challenged community.  When you do, you immediatley label yourself  as nothing more the stero-typical (geek|nerd|dweeb).\n\nAnd stop letting \"Savanna\" write articles, it's bringing down the quality of the entire site!"
    author: "Anon"
  - subject: "Re: Bad article, worse comments."
    date: 2003-07-09
    body: "Go away troll.  You can't even spell, yet you think you are high and mighty enough to criticize a valued contributor to the community.  Not everyone is a geek expert like you, accept it.  She told you more than once yet somehow the point didn't get through to you?\n\nSo do your research on how to properly spell \"immediatley\" and  \"completley\" then come back.  Also, inform yourself on what a hemisphere is.  Hint: It is \"sphere\" prefixed with \"hemi\".  It is certainly not how you describe those parts of the body."
    author: "anon"
  - subject: "Re: Bad article, worse comments."
    date: 2003-07-09
    body: "If you are more intelligent than all of us, why didn't you write the article?\nPlease be generous with us, and show us the light...\n\n"
    author: "Spock (a poor mortal)"
  - subject: "Re: Bad article, worse comments."
    date: 2003-09-23
    body: "I'm a geek and proud of it. \nHer articles are more directed to the less geek audience. I know what you're criticizing because I see it myself, but keep in mind the audience. She's showing people who are curious about Linux and KDE yet still new at it, what this wonderful OS can do. You're the one who qualifies as a dweeb in this case if you fail to see that. I couldn't care less if she was a man who had 3 sex changes so now resides in the grey area between male and female. What matters is that her articles are well written when it comes to teaching those who are new to KDE about the neat things it can do, without scaring people away with the technical jargon geeks like me are so fond of. Looking forward to seeing some more constructive criticism instead of just random people bashing. \n\nPS: some might have realized this whole paragraphe only states one point 2-3 times over. This was intentional, with goal of getting it to sink in."
    author: "Flip"
  - subject: "knoppix 3.3"
    date: 2003-07-09
    body: "by the way.....\n\nanyone know when knoppix 3.3 should be release ?\n\n"
    author: "somekool"
  - subject: "Re: knoppix 3.3"
    date: 2003-07-09
    body: "If Knoppix DVD-edition is called Knoppix 3.3 then tomorrow."
    author: "Anonymous"
  - subject: "Re: knoppix 3.3"
    date: 2003-09-23
    body: "There appears to be a v3.3 at www.suprnova.org.  You'll a bittorrents client however.  You can find it under the \"apps\" section listed as \n\"04:26 Knoppix 3.3 English version 700 Mb 57 430 Good anonymous link\"\n\n--ha1o "
    author: "ha1o"
  - subject: "re: Bad article, worse comments"
    date: 2003-07-10
    body: "Unfortunately, I have to agree with you.  Knoppix is great and it's good to see a favorable review by a non-technical type. It would have been better if it had included such simple things as the version used, the hardware installed on, and the type of errors encountered. I have booted and installed older versions with few if any errors.  I'm currently doing a test install of 3.2 on some old hardware - the bootup was without incident and the only install errors so far are a whole bunch of cp errors in /usr/share/doc, which probably means these files were stripped from the CD while leaving the directory structure intact.\n\nAs far as Debian is concerned, I was one of the very early users and I have revisited it several times since.  It is a good, stable distribution and my hat is off to all of the faithful developers that create it.  However, it is not cutting edge and it still needs a product such as Knoppix to shield the user from the horendous install / update procedure.  "
    author: "stretch"
  - subject: "Knoppix and Bitstream Fonts"
    date: 2003-07-11
    body: "Please don't flame me, I'm totally new to this.  I had tried Knoppix a long time ago and liked it except the \"unprofessional\" look of the fonts.  It may have to do with antialiasing.  My understanding was that the new free Bitstream fonts (which are included in the latest distro) would solve this.  But it hasn't.  Maybe they are not enabled by default?\n\nThe desktop fonts look fine but if you do things in OpenOffice or other apps, they look bad.  The antialiasing and font spacing look bad.\n\nIs there a way around this?\n\nJHM"
    author: "James"
  - subject: "Re: Knoppix and Bitstream Fonts"
    date: 2005-04-29
    body: "This is an old comment, But I'l Answer anyway,\n\nGoto the Knoppix settings in control room (or whaterver it's called), got fonts and click Anti-Aliasing.\n\nProblem solved."
    author: "yy"
  - subject: "Knoppix is finished? Erm ..."
    date: 2003-10-14
    body: "I got here late. I guess. I clicked on the link:\n\nhttp://www.knoppix.com/\n\nAnd got some kinda warning:\n\nClosed because of \"Software-Patents\"\nIn the next few days, the European Parliament will decide about the legalisation and adoption of so-called \"software patents\" in Europe, which are already used by large companies in other countries to put competitors out of business. This can lead to the termination of many software projects such as KNOPPIX, at least within Europe, because the holders of the over 30,000 already granted \"software patents\" (currently without a legal foundation) can claim exclusive rights and collect license fees for trivial things like \"progress bars\", \"mouseclicks on online order forms\", \"scrolling within a window\" and similar. That way, software developers will have to pay the \"software-patentholders\" for using these features, even in their own, completely self-developed applications, which can completely stall the development of innovative software for small and medium companies. Apart from this, the expense for patent inquiries and legal assistence is high, for even trying to find out if the self-developed software is possibly violating \"software-patents\", if you want to continue to market your software. Contrary to real patents, \"software-patents\" are, in the current draft, monopolization of business ideas and methods, even without any tangible technical implementation. \n\nMore about the current major problem at http://swpat.ffii.org/index.en.html\n\nAnyone can clarify what exactly does this mean? End of Knoppix? I thought this was protected under GNU or GPL. *shrugs*"
    author: "Kosmos"
---
In the third article of what is turning into a regular column, <i><a href="/search?subject=Savanna%20Says&op=articles">Savanna Says</a></i>:  Kneat <a href="http://www.knoppix.net/">Knoppix</a>!  You should really read this if you had wanted to try or demo KDE before but you couldn't -- because you thought it required a full Linux or Unix installation.
<!--break-->
<p><hr>
<h2>Kneat Knoppix!</h2>
<i>by <a href="mailto:savanna@kdenews.org">Savanna</a></i>

<p>

After <a href="/1056640088/">my last article</a> about Kontact, a lot of people were wondering: <i>"Is 
Linux/GNU/KDE good for me?"</i> -- regular users that is, coders don't seem to 
have to ask -- and then they wrote to me asking that very thing.

<p>

Surprise!

<p>

Well, I have two answers for you today:

<ol>
<li> Yes.</li>
<li> Try it out for yourself!</li>
</ol>

<p>

Now, let's clarify those positions. 

<p>

Number 1: Yes! KDE is ready for regular users. At least, I think
so. In fact, I am planning some exploratory articles on that very same
subject in the very near future. Now, however, I just want to jot this
little review down and get it on digital paper.

<p>

Number 2: Try it out for yourself! Great news! With <a
href="http://www.knoppix.net/">Knoppix</a>, you can do it and you can
do it easily. This is what this article is about: Knoppix. It's a
great thing and it is making waves all over the world. Let me give you
a quick user's rundown:

<p>

When I started with Linux a while ago (or GNU/Linux), it was a little
complicated to install. I'm using Debian Linux on this desktop and it
is sort of a patchwork build of a system which was mostly maintained
with some serious learning at times when I did something wrong, and
some serious corrections at times from the good coders in #debian-kde
(whom I have mentioned in previous articles). Now, I went with Linux
knowing I would probably have to learn a little bit more about
computers than I was used to.  Remember: I'm a regular user.

<p>

Okay... you're with me, right? Regular user, Debian Linux install with KDE. 
Tinkering needed to learn some new things. Got it?

<p>

Okay.

<p>

Not anymore.

<p>

You see, the one thing that kept me back from telling all my friends and 
family that they could switch was that I wasn't sure that they wanted to do 
what I had to do: learn a little more about computers to make some things 
work and install. Now, however, they don't have to.

<p>

Enter Knoppix.

<p>

What is Knoppix? Well, Knoppix is, in a sense, one of the last links in the 
regular users puzzle. Knoppix is a CD on which you'll find a Debian Linux 
kernel and one of the latest KDE3 desktop systems. It's about 700 megs and 
fits exactly on one CD.

<p>

And it is awesome.

<p>

Why? Well, let me tell you something: When I tell people that if they want to 
try Linux, they can just install it and try it, that's a pretty hard sell. 
Most people hear "install" and their eyes sort of glaze over and they start 
to think <i>"Why bother?"</i> And remember the tinkering part? Well, tinkering 
happens at all phases with a distro that you don't buy targeting new users 
(like SuSE, Mandrake, etc...). The hardest part is telling people that they 
have to install to see what it is like. Even with an easy install that 
auto-boots into KDE, people don't want to have to install or tinker to try 
anything out first. But with Knoppix... you don't have to do that.

<p>

All you have to do is put it in your CD drive, boot up, and presto! you've got 
a Linux system -- and a beautiful KDE3 desktop -- running all from your CD 
drive. No install, no weird lines of code...  try it out and you will see how 
beautiful KDE and Linux are, and you won't even have to get your hands dirty 
or look under the hood.

<p>

And you don't have to commit unless you are truly impressed and decide to 
switch.

<p>

This is the best part: you can download it free, burn it, and carry it
around with you to show your friends, family, etc... it will boot on
any PC and won't even touch their hard drive. Demo it anywhere. If
they like it, give it to them -- you can <i>install</i> Knoppix from
the CD to your hard drive if you want to switch, and spread the good
word.

<p>

Isn't that fantastic?

<p>

Before Knoppix, you had to tell people to install things. Now you
don't have to. Talk about a real revolution. People are talking about
Knoppix handout drives to spread the word. People are carrying a few
of the CD's in their bags or briefcases or even their purse to show
others how great KDE and Linux are! Mass singing is breaking out and
peace is befalling the earth as everyone joins hands and...

<p>

Well, that last part I made up but you get the idea. Knoppix really
<i>is</i> that cool.

<p>

So, you're asking me <i>"Okay... what's up with this? How easy is it
to burn and all that?"</i>

<p>

Well, you just go to their site: <a
href="http://www.knoppix.net">http://www.knoppix.net/</a> (This is the
English site) and you <a href="http://www.knoppix.net/docs/">read the FAQ</a> -- which I found to be a little
geeky but fairly general enough that <i>some</i> users might
understand what they are talking about (something that they should
work on to be a little simpler to understand, I think, but it is a
nice start!).

<p>

From there, you find out where to download the 700 meg ISO CD
image. Now... a few things with this:

<p>

<ol>
<li>Most people don't know what an ISO is, but I believe that they
explain it well enough.</li>
<li>It's a 700 meg download so broadband is a must -- unless you don't mind 
waiting a week to get it.</li>
<li>They do have CD's for sale which they can send you for the
non-broadband or techie users out there who can't make heads or tail
of their FAQ.</li>
<li>They go into checking the ISO file after the download with the checksum 
file -- something which most users will sort of glaze over. I know I did 
originally and needed some help from my friendly IRC people in
#debian-kde.</li>
<li>They <i>do</i> explain how to burn the CD with Nero -- which is
important. Most Window users get a PC with Nero or another kind of
software and they seem to take the time to explain how to do this,
even though they say that it is outside of the scope of the FAQ at
large. This is important as many people do not know how to burn an ISO
to be bootable etc...</li>
</ol>

<p>

After you burn, you're basically done.  I did have to download it twice, 
however (bit of a pain). What happened is that the first image didn't 
download well or something so I had to download it again. Apparently that is 
what the Checksum file is for: to make sure that the file you downloaded is 
in working condition. To make a long story short: Getting it and burning it 
was a little on the techie side. Needs to be improved, but very good start 
overall.

<p>

Now for the good part.

<p>

Turn off your PC, leave the Knoppix CD in the CD tray, and boot up.

<p>

Okay, at this point you should know that Knoppix is new and still has some 
bugs in it. Don't worry! It won't touch your HD at all. I'm just talking 
about booting-up bugs. It kept dumping me in the middle of the boot up and 
giving me weird errors on the screen and it would freeze. Don't worry: just 
reboot. It'll work fine eventually. It took me 3 tries and it kept giving me 
errors every now and then, but finally, I was in the desktop!

<p>

There it was. Right on my Windows PC that I use for backups, I had a KDE 
desktop. And I didn't even boot off of the hard drives.

<p>

All the main programs are crammed in there beautifully, and the icon set is 
really pretty. The only thing I wasn't fond of was the background. It looked 
too "H4x0r" Matrix/techie for my tastes. If I could suggest something, I 
would suggest a little tamer background now for regular users.

<p>

How did it handle? It handled beautifully. I was using a Linux/KDE system from 
my CD drive.

<p>

Nice.

<p>

Any bad points? Not that I can think of at the top of my head. (Anyway, I'm 
trying to get people to try this because it really won't hurt, is fairly 
simple, and fun to use.)

<p>

Now, if only I had seen this a year ago. Of course, it wasn't really around 
back then, at least in that form.

<p>

So, what did I do next? I decided to keep it. I had some instructions
on how to install after I had booted up. I typed in: Control-Alt-F1
and got into console mode, typed in "knx-hdinstall" and it guided me
through a <i>very</i> easy to install process. I just chose which
partitions I wanted to use (not very friendly as of yet but it'll get
there), put in my IP numbers etc... and away we go.

<p>

Again, it was a little buggy so it failed two times before it got it on the 
third. Again, weird messages on the screen and froze up. What did I do? I 
just hit the reboot button and started over. A bit tedious but I can deal. 
That has happened to me with a Windows installation before so I wasn't that 
worried about it. And anyway, by this point, I was already well into the 
process of erasing the hard drive and rewriting over it, so it didn't concern 
me in the least.

<p>

Finally, it worked! It installed, booted, went directly into KDE3 and
I suddenly have a fully functional Knoppix system on my backup machine
and it looks great. I love it. I totally love it. In fact, I keep the
CD in my purse and bring it around with me to show people. It's little
geeky me, but it's great and people <i>are</i> wowed. This is Linux
like they never knew existed. I'm dispelling the "Command Line Mode
Only" picture with this one CD and I'm doing a great job of it.

<p>

Now you can too.

<p>

So try it! Give it a whirl. Either buy the CD (very cheap) or just download 
and burn it yourself. At worst, you blow a CD or two trying. But in no time 
flat you will have an awesome CD that is changing the world of computing.

<p>

Thank you team Knoppix. I give you a big thumbs up.
