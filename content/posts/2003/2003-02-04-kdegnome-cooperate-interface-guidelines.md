---
title: "KDE/GNOME To Cooperate On Interface Guidelines"
date:    2003-02-04
authors:
  - "aseigo"
slug:    kdegnome-cooperate-interface-guidelines
comments:
  - subject: "i hope..."
    date: 2003-02-04
    body: "... we start to see collaboration before releasing something, not not vice-versa as happens today.\nA common menu hierachy should have be made sinse kde  and gnome 1.0, but here we are..."
    author: "Iuri Fiedoruk"
  - subject: "Re: i hope..."
    date: 2003-02-05
    body: "Why didn't YOU do it?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: i hope..."
    date: 2003-02-05
    body: "Studing to enter computer science, working 8 hours a day... no C/C++/QT knowledge (only php/gtk-php).\nUnhapply don't have the time and knowledge.... yet."
    author: "Iuri Fiedoruk"
  - subject: "Re: i hope..."
    date: 2003-02-05
    body: "So, others should spend their time coding so you don? have to, but you get to decide what is more urgently needed?\n\nDude, this is not a democracy, you know."
    author: "A.nonymous"
  - subject: "Re: i hope..."
    date: 2003-02-05
    body: "Hey hey hey, calm down! I wans't demanding.\nYou know:\nI hope != I WANT\n\nI just think it's a good idea to chat a bit with other people to enter a consence about how things could be, but no one is forced to do it, even micro$oft that is highly critized for not using standarts is forced to do."
    author: "Iuri Fiedoruk"
  - subject: "bullshit"
    date: 2003-02-08
    body: "To the guy who is asking the other to code..\n\nI notice u ppl have a tendency to tell other ppl to code the minute they have a suggestion or a wish. This is crap.\n\nfirstly not everyone is a programmer, or has the time. They try to contibute by asking for certain features. And If those features are good then when implemented kde could be  better of. \n\nYes i know that programmers feel irritated when some non programmer comes and makes rude demands or trolls but this was no troll as far as i could see, he was making a wish. To grouse him for that is the worst injustice u could do, unless u plan to keep kde away from the end user and only with us programmers. and plus   this is being used as an excuse for not doing something.\n\nBuddy what i am trying to tell u is END USERS MATTER without them there is no product. And this is a democracy, whatever u say. Kde aspires to go into the corporate world, so this becomes even more important. \n\nAnd when u r trying to compete with someone like M$ all these small thing s count. You gotta be more than perfect.\n\n\n\n\n\n"
    author: "Bharat"
  - subject: "Nice stuff"
    date: 2003-02-04
    body: "This is good news and just highlights the mutual respect the KDE and GNOME developers share for each other. After news like this the \"desktop war\" doesn't seem as serious anymore. :)\n"
    author: "Chakie"
  - subject: "How times have chaged"
    date: 2003-02-04
    body: "Interesting isn't it? When Gnome had the developmental momentum and KDE was rather lagging, there was no interest in cooperation, instead the \"Gnome camp\" spent a great deal of time flaming KDE and QT. Now KDE is develomentally ahead of Gnome(by a very considerable margin)they want to play nice."
    author: "Mr Magoo"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "You make this whole issue sound like \"the big bullies now beg for mercy\".\n\nFirst of all, there is no \"Gnome camp\" that \"spent a great deal of time flaming KDE and QT\". Those people are anti-KDE people, not GNOME people. Most of them are probably 14 year-old Slashbots who flame KDE for the sake of flaming, and don't know what they're talking about.\nSimilarly, there is no \"KDE camp that flames GNOME\", those are anti-GNOME people, not KDE people.\nAnd the GNOME and KDE developers themselve don't give a ***** about the rediculous childish flamewars that anti-GNOME and anti-KDE people are making.\n\nSecondly, KDE development is not \"ahead\" of GNOME: KDE develops in a different direction than GNOME. They both target different kinds of users (\"average user\" is a big word) and they both go a different direction. KDE is not \"better\" than GNOME, nor is GNOME \"better\" than KDE: they are just different. Just like KDE is not \"better\" than WindowMaker or GNOME not \"better\" than XFCE.\nKDE has more features, that's great. But some people want simplicity instead. Wether GNOME or KDE or WindowMaker or XFCE or twm is \"better\" depends on your personal taste; there is no universal answer.\n\nThird, this is a *good* thing. There are lots and lots of GNOME and KDE apps. If we can make them look and behave more like each other, then that can only be a good thing.\n\nAnd last: there is no GNOME vs KDE war. Unless you *want* one, of course."
    author: "Hongli Lai"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "> Most of them are probably 14 year-old Slashbots\n\nGood guess"
    author: "KDE User"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "IIRC, Miguel de Icaza is hardly much younger than myself, and I was 14 very long ago. Yet, Miguel did spend an inordinate amount of time whining, flaming and FUDding back then (he even spent a week or so on #kde doing GNOME whining). And I did my fair share of GNOME flaming, too!\n\nI know, I know, now we all must get along, it is old history, not news, etc, but let's not pretend stuff didn't happen."
    author: "Roberto Alsina"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "But we shouldn't let the past in our way either. Miguel has grown up now. Why don't we concentrate on *now* instead of stupid pointless flamewars in the past?\nThe last thing I want is more \"the Linux community is fragmented!\"-trolls."
    author: "Hongli Lai"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "No he has not.  I can assure you.  Miguel was heard this week by me and several others saying 'Qt is tainted'.  That's right, Qt a GPL'd library is 'tainted' according to Miguel.  Blows my mind that the founder of one of the largest GNU projects has said a GPL'd library was 'tainted'. "
    author: "Adam Treat"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "I just want to add that I have no problems with GNOME or GNOME hackers and Miguel's comments should not be regarded as representative of the whole."
    author: "Adam Treat"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Heard where?  In what context?"
    author: "Navindra Umanee"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "on irc - we had cut 'n pasted the conversation and were ready to send it to RMS for his comments - but you know :P\n\nTroy Unrau\ntroy@kde.org\ntroy on irc.kde.org"
    author: "Troy Unrau"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "<miguel> Sorry, but Qt is tainted as far as am concerned\n<miguel> no, I dont\n<jon> how is Qt tainted?\n<manyoso> it's tainted??\n<miguel> Qt is GPL\n<miguel> Can not be used for proprietary/commercial apps without paying\n\nI like Miguel as a person, but I have a big time problem with this.  He is *very* wrong here and this smacks of Microsoft's 'GPL is Viral' FUD and is much worse coming from one of the leaders of the Free Software movement.\n\nCheers,\n\nAdam"
    author: "Adam Treat"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Why was Miguel talking about Qt?"
    author: "Navindra Umanee"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Some people were discussing Mozilla and KHTML when Miguel said KHTML should be rewritten in C# and stripped of the Qt stuff."
    author: "Adam Treat"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Different people have differing opinions on whether a GUI toolkit should be LGPL (GNOME) or GPL (Qt) so just grow up and get over it :)"
    author: "keith"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Sure.  I can understand that.  If Miguel has changed his stance regarding Free Software then that is for him to decide.  However, the statement that Qt is 'tainted' because it is licensed under the GPL is antithetical to the Free Software philosophy and I strongly disagree.  It is important to note that Miguel is a leader of the Free Software movement and founded GNOME specifically because KDE was not Free.  Given the history and context I find this statement particulary egregious."
    author: "Adam Treat"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Understand that even RMS agrees that GNOME should be LGPL.  Miguel has not changed his stance.\n\nThe 'tainted' quote you refer to only regards the issue that a software developer has to pay Troll Tech if they use Qt in a commercial product.  Some consider that a GUI toolkit is an operating system service, like \"glibc\" ... therefore should be licensed LGPL.  Others think differently.  Just accept this differing opinion."
    author: "keith"
  - subject: "Why you shouldn't use the Library GPL..."
    date: 2003-02-05
    body: "... for your next library.\n\nThis is directly from the GNU's mouth! http://www.gnu.org/philosophy/why-not-lgpl.html\n\nMiguel is the founder of GNOME. Remember what that G stood for. GNU. For Miguel to say that GPL libraries are tainted is like RMS saying GPL is tainted. A mighty shock. Miguel has come a long way from being a 'Champion of Free Software'. Now it seems he is the 'Champion of Proprietry software'."
    author: "ac"
  - subject: "Re: Why you shouldn't use the Library GPL..."
    date: 2003-02-05
    body: "Come on ... \"tainted\" is just an English word that means isn't pure.  I believe Miguel was referring to Troll Tech's mixture of using a combined Commercial/GPL license.  For most GPL licensed software this commercial option does not exist ... hence pure.\n\nRegarding your next library and the use of GPL or LGPL ... quote from the page you referenced, http://www.gnu.org/philosophy/why-not-lgpl.html ... \"depends on the details of the situation\".\n\nThe main point here supporting GPL over LGPL ... \"Using the ordinary GPL for a library gives free software developers an advantage over proprietary developers: a library that they can use, while proprietary developers cannot use it\" does not hold water for the Qt Toolkit because a proprietary developer can simply buy a get-out-of-jail pass from Troll Tech.\n\nAnd the main point here supporting LGPL over GPL ... \"when a free library's features are readily available for proprietary software through other alternative libraries\" seems to apply to GUI Toolkits ... a proprietary developer can either buy a QT Toolkit license form Troll Tech or use an alternative toolkit like GTK.\n\nI think the GNU page you referenced supports the idea that common operating system libraries should be licensed LGPL and this has been GNU's practice with their own projects (e.g. glibc).  If you disagree with GNU then that's your opinion.\n\nBut please stop spreading misinformation.  Please accept that different people have different ideas and different companies have different business models.  Time will sort out the details and that's what is good about free software."
    author: "keith"
  - subject: "Re: Why you shouldn't use the Library GPL..."
    date: 2003-02-05
    body: "You seem to be missing the point of that passage, and the point I was trying to make. The FSF recommends GPL for libraries because if the libraries offer something new, then proprietry software developers will be encouraged to GPL their software to take advantage of it.\n\nNow in the case of Qt (which is a great toolkit), there is an expensive get out clause (note that RMS doesn't mind this revenue generating method, but does oppose selling proprietry software to make money i.e. Ximian connector), but the money they get from that goes back into making Qt better. Plus, the cost will naturally force proprietry software developers to at least consider opening their code.\n\nHowever, in the case of Gtk+, there is no pressure for proprietry software to be opened. Now you could say that it offers nothing and is crap, therefore what is the point in having it GPL. But the fact is that it *is* better than motif and perhaps some other commerical toolkits, so by being GPL it would benefit free software by forcing some developers to Free their code. Shouldn't the GNU desktop should be a force for freeing code, and not be a magnet for proprietry software to come over to linux? Being based on an LGPL library this will never happen."
    author: "ac"
  - subject: "Re: Why you shouldn't use the Library GPL..."
    date: 2003-02-06
    body: "Whoa there ... the FSF does not recommend the GPL for all libraries.  You seem to be missing the point of the document.  RMS recommends using the LGPL in some instances.  He even states that the LGPL should be used for common libraries.  Furthermore the FSF uses the LGPL for their own library projects like glibc.  RMS agrees with using the LGPL for GTK and GNOME libraries.  Your seem to be saying the LGPL is evil and the GPL should be used in all cases.  If you're such a GPL zealot, more so than RMS and the FSF,  how come you don't take issue with any of the FSF projects that use the LGPL, like glibc, or many of the KDE libraries that use the LGPL. \n\nRegarding RMS, he wants all software to be free.  He's the same as Christ who wants everyone \"to treat your neighbor as yourself\".  However, in the real world, we are all sinners in RMS's eyes ... Troll Tech and Ximian included ... they are both sinful proprietary software companies even though they both repent and provide significant support for free software projects ... I just know the devil ... MS.\n\nHow's that for software religion ... now I'm outa here ... because there's no point in discussing religion with someone who can't fathom the real world :)"
    author: "keith"
  - subject: "Re: Why you shouldn't use the Library GPL..."
    date: 2003-02-06
    body: "Actually you'd be surprised, my views disagree with the FSF on many fronts, especially on licences :) However, I do believe that since GNOME is *the* GNU desktop, it should be a vehicle for promoting Free software, not proprietary software. Otherwise it waters down the reputation/power of the FSF in its arguments to promote Free software, since people can just point to GNOME and say \"look, you say a great advantage of GNOME is that you can build proprietary applications with it...\""
    author: "ac"
  - subject: "Re: Why you shouldn't use the Library GPL..."
    date: 2003-02-05
    body: "Really, this is just a storm in a tea cup. Miguel is just voicing many people's preference for the LGPL licence for common libraries such as this. Hardly worth getting your knickers in a knot over it."
    author: "TN"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Stripped of Qt and rewritten in Ximian Mono / Microsoft .NET?\n\n*lol*  This guy is corporate material indeed!\n"
    author: "Navindra Umanee"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Well, I like many Gnome developers, but Miguel does never play fair.\n\"KDE has no future\" and things like that do not need to be said. I'm not going to say anything like that about Gnome, although I prefer KDE. He seems to be the only Gnome developer who doesn't respect KDE and KDE developers. Hopefully I\n'm wrong.\n\nMaybe Miguel has to do it for marketing reasons. His income pretty much depends on Gnome and Gnome applications."
    author: "Peter"
  - subject: "Re: How times have chaged"
    date: 2003-02-06
    body: "The only thing \"tainted\" is Miguel from way back when he interviewed for Microsoft and didnt get the job. Ever since then he keeps droning on and on about how great the technology coming out of Redmond is.\n\nI would approach the foaming bullshit pouring out of his mouth with extreme caution."
    author: "Joe Sixpack"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Well, I stopped flaming GNOME a while ago, except on special occasions, too ;-)\n\nSure, Miguel has grown up now. Forgiving is cool, even though he never apologized (in order to get him out of #kde, I had to go troll #gnome, and he didn't even understood why what he did was wrong). But forgetting is stupid. \n"
    author: "Roberto Alsina"
  - subject: "Re: It's Not Completely Untrue"
    date: 2003-02-04
    body: "I beg to differ. GNOME's #1 supporter and funder was RedHat. RedHat has always been partial to GNOME - it's evident by it being the default desktop on 7.x and Bluecurve looking like all GTK in 8.0. Concidently, the netblock FreeDesktop.org is on is registered to RedHat Linux. So as argumenative as he may sound, it's a lot of validity to his statement. He's right, they only wanted the integrated after GNOME is lagging a little behind. But what do you expect, it's RedHat !! Don't shoot the messenger though..."
    author: "chillin"
  - subject: "Re: It's Not Completely Untrue"
    date: 2003-02-04
    body: "Bluecurve looks more like KDE than GNOME. GNOME's icons are duller. GNOME 2.0's menu bar is at at the top, and the task bar is a 1x4 grid of workspaces, not 2x2 like KDE. If you want to know what GNOME's default is, take a look at Mandrake 9.0. They mostly preserve it."
    author: "Noah Body"
  - subject: "Re: It's Not Completely Untrue"
    date: 2003-02-05
    body: "gnome apps have the drab look of Soviet era apartment\nblocks."
    author: "kman"
  - subject: "Re: It's Not Completely Untrue"
    date: 2003-02-06
    body: "And KDE looks like a childs fischer-price play set. \n\nHmm isnt it fun slingin mud instead of actually doing something useful with your life?\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: It's Not Completely Untrue"
    date: 2003-02-05
    body: "That's one take. Another take is that freedesktop.org is coming up on 3 years old, and is trying to get KDE and GNOME developers moving together a little bit at a time as opportunity presents itself. Going back to 3 years ago, I don't think KDE was really so far ahead[1], but there freedesktop was anyway, trying to work towards interoperability with other desktops.\n\n[1] Nor do I think it is now, GNOME apologist that I am, but I digresss."
    author: "Big G"
  - subject: "Re: It's Not Completely Untrue"
    date: 2003-02-06
    body: "PLEASE. FreeDesktop.org is evil because their web server runs RedHat?? Man you need to get out more. \n\nCongratulation on the release of 3.1 btw, come round and congratulate Gnome on 2.2.\n\nCheers, and good will toward desktops!\nRyan"
    author: "Ryan"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "Quit spreading this FUD!  KDE has always been ahead in development momentum.  You should also know that a leading Gnome developer is still spreading lies about Qt."
    author: "anonymous"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "*LOL*\n\ngo home! and stop whining! This is so stupid. Let developers make their job and please shut up!\nGNOME people aren't spreading lies about Qt, because they aren't simply interested in doing that."
    author: "chrisime"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "and the sky is green too!"
    author: "anonymous"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "This was a comparison of GPL vs. LGPL GUI toolkits.  Don't miss represent."
    author: "keith"
  - subject: "Re: How times have chaged"
    date: 2003-02-04
    body: "Miguel de Icaza is.  He's the founder of GNOME in case you haven't heard.  According to Miguel, 'Qt is tainted'."
    author: "Adam Treat"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "\nMiquel and Nat (ximain ceo)met at Microsoft where Nat was working and\nMiquel was applying for a job.\nI don't think Miquel ever got over not getting the\njob for Bill. Maybe that explains his love of C#\na Microsoft dominated language.\n\nrewrite khmtl in c# , indeed.\nmaybe evolution should be rewritten in qt.\n\nStill this cooperation effort is a great thing.\nI guess I should hold my toungue but that rewrite\nkhmtl got me really steamed."
    author: "kannister"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: " This was a comparison of GPL vs. LGPL GUI toolkits. Don't miss represent."
    author: "keith"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Let's not forget the slanderous reputation about qt not being free or \"for Linux\", just because it was released under a software license other than the GPL. The result of that was a whole army of \"Anti-KDE\" idiots who believed that nonsense."
    author: "chillin"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Man, reading this is *scary*. Gnome zealots may have an inferiority complex, but you guys have developed a real serious persecution complex and an over-developed sense of self importance! You guys are much scarier than Gnome zealots! I guess there are all kinds of loonies out there...\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "> Man, reading this is *scary*. Gnome zealots may have an inferiority complex,\n> but you guys have developed a real serious persecution complex and an\n> over-developed sense of self importance! You guys are much scarier than Gnome \n> zealots! I guess there are all kinds of loonies out there...\n\n  Even though KDE is very far ahead of GNOME IMO (and I'm a GNOME user myself,\n  always has been) I have to agree that some KDE people are a little paranoid:\n  they even have a site called kdemyths (or somesuch) that, well, is a little\n  strange to me. ;) Come on guys, grow up a little a bit, not everybody are\n  after you. :)"
    author: "ignorant ;)"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "Well, all this propaganda about \"paranoid\" KDE developers and KDE users is actually getting propagated by people like you...  let me guess you're all GNOME users right? :-P"
    author: "Navindra Umanee"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "> Well, all this propaganda about \"paranoid\" KDE developers and KDE\n> users is actually getting propagated by people like you... let me\n> guess you're all GNOME users right? :-P\n\n  The site kdemiths speaks for itself."
    author: "ignorant ;)"
  - subject: "Re: How times have chaged"
    date: 2003-02-05
    body: "there are a lot of misconceptions out there regarding KDE. such things happen. there was no good summary of information that addressed the most common misconceptions, so i put one together and called it \"KDE Myths\" because that is exactly what it addresses. \n\nif people are free to spread innacuracies, i'm free to spread accuracy. it has nothing to do with paranoia or zealotry but a desire to have accuracte information in the hands of those who care.\n\nif you can show me how this is a wrong thing, please do. "
    author: "Aaron J. Seigo"
  - subject: "Button order"
    date: 2003-02-04
    body: "I guess there will be quite a lot of discussion regarding button order (Ok-Cancel versus Cancel-Ok)..."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "These shouldn't be any. Gnome 2's default is DUMB. It makes no sense whatsoever to lead people clicking on the wrong button:\na) for the first few months of migration to your architecture\nb) whenever they use something else\nIt is too great a sin to change button ordering in a minor revision too. That is a big fuck-up, it will be hard to clean. But whatever it takes, the burden is Gnome devels'."
    author: "goosnargh"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "When your last architecture is mac there is no \"clicking on the wrong button\" problem.  Mac has made this choice for a reason, so has gnome.  Whether or not it's the correct alternative is another question.\n\nIt should be solved between kde and gnome. I don't care if kde or gnome \"wins\" this.  As long as it's the same between them."
    author: "coward"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "So, basically, it is right for some 4% of possible users, and wrong for about 92% of them.\n"
    author: "Roberto Alsina"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "> So, basically, it is right for some 4% of possible users, and wrong for about \n> 92% of them.\n\nWrong.  The button order can be right, or it can be wrong; it can't\nbe right for some users, and wrong for others.\n\nBasically, Apple put a lot of research into determining the correct\norder for buttons in a dialog.  MS made the order different in Windows\nin order to avoid being sued by Apple, even though their button order\nwas _wrong_ and decreased usability.  KDE and Gnome1 copied MSW.\nFor Gnome2, the Gnome developers finally got around to reading some\nliterature on HCI, and fixed their button order.  The KDE developers\nshould do the same.\n\nThis isn't about Gnome vs. KDE -- this is about good UI design vs\nbad UI design."
    author: "pseuonymous coward"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "> Wrong. The button order can be right, or it can be wrong; it can't\n>  be right for some users, and wrong for others.\n \nWrong, of course it can.\n\nFor example, if you are using a right-to-left locale, all of Apple's reasons for their button ordering work the other way.\n\nSo, what you may be trying to say is that they can not be right or wrong FOR THE REASON I GAVE. Which is an entirely different thing.\n\nBut since I replied to a post saying that if you came from a mac there was no \"pressing wrong button problem\", I only stated the obvious, that for about 23 times more people, THERE IS a \"pressing the wrong button problem\".\n\nAs for MS changing the button order to avoid a lawsuit... well, I find it quite farfetched. Do you have a reference?"
    author: "Roberto Alsina"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "A lot of this strikes me as rather trivial. It could be argued that it is good to have the buttons appear in different locations to prevent against habituation - so when a \"You have unsaved data. Do you want to save it?\" dialog box appears, the user doesn't automatically click on the \"discard\" or \"no\" or whatever button with no further thought and realise only a moment later that they have lost their data. Let's face it, losing data is a bit more of a hinderance to productivity than having to find the right button, and users will *always* make mistakes.\n\nWith the buttons in different places, users of both KDE and Gnome apps will have to look for the correct button, reducing the chance of not saving their data before quitting an application. \n\nHaving said all that, I think consistency between Gnome and KDE is a good thing, just so that both DE's get a fair chance when common inconsistencies in proprietary systems are cheerfully ignored. Remember that in hci (or chi!), most findings are counter-intuitive.\n"
    author: "User of both KDE & Gnome"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "[quote]\nA lot of this strikes me as rather trivial. It could be argued that it is good to have the buttons appear in different locations to prevent against habituation\n[/quote]\n\n*lol*, right on!  \n\nAnother cool thing would be to maintain consistent button order but switch them around from time to time -- just for the heck of training new brain pathways.\n\nThen of course, we could have *random* button orders for the truly adventurous."
    author: "Navindra Umanee"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "Sounds like whats happens when you play with WinZip..."
    author: "ne..."
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "<I>\"For example, if you are using a right-to-left locale, all of Apple's reasons for their button ordering work the other way.\"</I>\n\nGTK+ 2.0 has this nice little feature of swapping the horizontal layout of all widgets for right-to-left locales."
    author: "Hongli Lai"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: ">  GTK+ 2.0 has this nice little feature of swapping the horizontal layout of all widgets for right-to-left locales.\n\nAs do Qt and KDE (try kwrite --reverse, for example)."
    author: "fault"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "I didn't new that, kwrite looks very strange, and strangely cool, this way :)\nThanks for the tip."
    author: "Iuri Fiedoruk"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "Wow, that looks so strange.  If you'll excuse me, I have some new brain pathways to train now...\n\n"
    author: "Jay H."
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "But is it automatic? Does the layout of all QT/KDE apps automatically get reversed when run under a certain locale?"
    author: "Hongli Lai"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "Basically, yes, depepndent on the currently selected language -- in  the language packs in kde-i18n, one of the things the translators specify is whether to use RTL or LTR ordering.\n"
    author: "SadEagle"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "it would be rather useless if it didn't, hm?\n\nhehe.. \"let's put in this massive BiDi framework, but the not let it be turned on desktop-wide as a policy decision\".. there is a method call that tells the program wether it is in LTR or RTL mode, and all the widgets/layouts listen to this.\n\neven things like the window decorations get flipped... the keramik window deco actually flips the pixmaps so things look proper... it's really quite nice and been available for a while."
    author: "Aaron J. Seigo"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "In that case you've answered the question about which button order is best. You should use the Gnome/Mac order since that will be the most natural for both LTR and RTL locations."
    author: "Trevor"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "Uh, no. That is one factor. Another factor is what is not surprising the user.\n\nSince KDE has used the current button order for about 4 years, and everyone that uses Windows has used that order for 9 years, and both together are:\n\n100% of current KDE users\n95% of possible future KDE users\n\nit only makes sense that in order to change the button ordering, the change would have to provide a measurable improvement in the user experience that is larger than the annoyance it would produce.\n\nSince no such measurings seem to be available, I see no reason to mess with status quo."
    author: "Roberto Alsina"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "But, using this logic, you should also stick with the double click system that Windows uses."
    author: "Trevor"
  - subject: "Re: Button order"
    date: 2003-02-06
    body: "Windows has both single and double click in the same interface. An inconsistency. KDE can improve that by removing one of the options (double click). There already is consistency with button ordering and the benefits of switching are slim to none, while introducing an 'getting used to period' of a number of weeks."
    author: "ac"
  - subject: "Re: Button order"
    date: 2003-02-06
    body: "KDE has removed nothing.  Using double clicks for icons is still an option within KDE and has been for a long time.  There's probably no reason why button order couldn't be the same.\n\nOnly one thing, though...  Why is it the only time the freedesktop.org project gets any work done on it, is when both groups are shamed by some event.  This last one was almost certainly because of the RedHat 8.0 Bluecurve controversy.  Is everyone so isolated in their own little Desktop Environment world that they are oblivious to everyone else?"
    author: "Anonymous Coward"
  - subject: "Re: Button order"
    date: 2003-02-06
    body: "Of course not, because I think single-click does provide a measurable improvement."
    author: "Roberto Alsina"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: ">You should use the Gnome/Mac order since that will be the most natural for both LTR and RTL locations.\n\nNo, that's according to some hand waving by Mac lovers. Changing button order would increase productivity by a factor of zero and would just annoy people for a time. I think people say it's better because when they finally get used to it, they are happy."
    author: "ac"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "The button order probably doesn't have anything to do with locale (if that means language settings, BiDi or not). It has to do with whether the user is left handed or right handed. As a right-handed user, I tend to quickly fill up an options/prefs dialog and click OK. This means pulling the mouse downwards towards me. It tends to send the cursor towards bottom right, which is the right place for the OK button for right-handed users. For potentially dangerous options, it should be otherwise. This is totally the other way round for left-handed users.\n--\nJust my opinion"
    author: "Rithvik"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "KDE team doesn't read so much litterature. They perhaps just listen to their users ? \nI think it is much better than reading thousands of bok explaining that your eyes is more quick to look on top than on bottom, that the mouse is better on the bottom right, that moving mouse from left to right is more easy than bottom to top,...\n\nI use KDE and use Gnome2 too (I have two machine with last versions of them in each one) and I am always confused by the order of the Gnome buttons :( Probably because I read text from left to right and so the left button is the first I read. So for me it is much better to put \"OK\" on the left.\n\nIf KDE adopt Gnome order I hope there will be a way to put them back in the Ok-Cancel order :)"
    author: "Shift"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "Just listening to the user is not the answer either. You should do *both*: listen to the user *and* read litterature.\nReasons to read litterature too:\n1) The user doesn't always know what he wants.\n2) What the user *thinks* he want may not be what he really wants or the perfect solution. Humans are not aware of everything.\n3) There are some things that can be done better, but the user is not (always) aware of them.\n\nAnd the GNOME button layout is a bit confusing the first few weeks. But having used it for a while now, it really isn't that horrible."
    author: "Hongli Lai"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "and so those few weeks of annoyance resulted in having an interface decision that is livable? i'd rather avoid those few weeks if it isn't going to give me any real benefits. changes that don't provide real and noticeable improvement in usability but come at great retraining costs are hardly worth it, IMHO."
    author: "Aaron J. Seigo"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "And you think those who decide what the user wants are right yes ? They could be as wrong as you - by saying the stuff in point 2)\n\nNo offense but 'Never change a running System' and KDE run's perfectly so far the wide acceptance of users aknowledge this."
    author: "oGALAXYo"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "They are not always right but neither are the users. For that reason, you should listen to users AND read litterature, make an idea and discuss it with others. ONLY listening to users or ONLY reading litterature are both bad.\n\nSurely you already realize this, right?"
    author: "Hongli Lai"
  - subject: "Re: Button order"
    date: 2003-02-10
    body: "sure. i at least certainly do, but the button ordering example is a\nterrible one. using gnome2 default settings slowed me down, therefore\ni switched back to kde. you think windows users are likely to go along\nwith it?, no way.\n\nkde is about pleasing users _and_ showing them the right direction,\nbut going against the grain _isn't_ always the right thing and mac os\nmay has gotten some things right but this is just wrong. \n\nthe okay button is the most commonly used button of the two and the \nmouse user in general centers on the middle of the dialog therefore \nswapping the button positions will increase mouse movement and \nmake it thus very likely that the user will have much further to move.\nthere are far better ways of solving the rtl problem than making \neveryone else suffer for it.\n\nAlex"
    author: "alex"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "> Probably because I read text from left to right and so the left button is the first I read. So for me it is much better to put \"OK\" on the left.\n\nYes, all is said here, it is the natural answer, so it is the good choice... excepted in some countries where people read from right to left......\n"
    author: "Alain"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "Besides my other comments to the original parent of this thread, I think it is potentially dangerous to put the OK button on the default spot since doing so leads to accidental pressing of it. For users who do not know how to undo changes to a document, for example, this can cause great unintended harm.\n\nSuch individuals would be more likely to erase documents, etc. \n\nRemove last year to reply..."
    author: "antiphon"
  - subject: "Re: Button order"
    date: 2003-10-24
    body: "I've even heard the argument, that the eye will travel from the top-left to the bottom-right (in left2right reading countries) and mainly focus at the corners, and that you should therefor place the most frequently used button in the bottom right corner.\n\nI have to disagree to this point, because once i see a row of buttons, i read this like text - that is left to right. This tendency totally overrules the \"bottom right corner\"-effect unless you've gotten used to having the unusual Cancel-OK order.\n\nThis is why i firmly believe that even people who are not used to computers at all, will look at the leftmost button first. And thus the OK button should be the leftmost button, due to it being the most frequently used one.\n\nThat's why i fully agree with Shift and Alain on this matter!"
    author: "JoaCHIP"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "You're forgetting that things exist in the context. And the context is that most KDE users are used to the current layout, so switching it around will hurt existing users, since it'll break their habbits. The difference in usability has to be tremendously big to justify any change like that.\n\nOh, and is the Apple research published (and peer-reviewed) someplace?\n"
    author: "SadEagle"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "> Oh, and is the Apple research published (and peer-reviewed) someplace?\n \ni wish. AFAIK almost nobody openly publishes their usability research. sort of like when nobody published their source code in the software development world. \n\noptimistically speaking, i think we may be seeing the start of \"Open Source\" usability. this may well have similar impacts on the usability industry that \"Open Source\" coding has had on the writing of source code."
    author: "Aaron J. Seigo"
  - subject: "Order"
    date: 2003-02-04
    body: "BREAKTHROUGH RESEARCH REVEALS PROPER ORDER OF BUTTONS\n\nI just finished a $2.5 million 3 year R&D project. The final result is that the \"OK\" button be placed on the bottom left of the window, and the \"CANCEL\" button on the bottom right.\n\nThis conclusion should be adopted by all Open Source dekstop projects because it costed 2.5 million dollars and lasted 3 years. If you disagree with this conclusion, you do not know enough about UI design to make a valid argument against this conclusion. This is the One True Way to put buttons on a window.\n\n[/ihopeyouknowimmakingfunofyourassforbeingsofuckingclosedmindedandabrainwashedappledrone]\n\nThank you for your time."
    author: "anonymous"
  - subject: "Re: Button order"
    date: 2005-11-27
    body: "Of course there can be a button order which is right for me and wrong for you.\n\nWhatever button order I'm used to is right for me, and will of course be wrong for a long-term mac user.\n\nThe Gnome button order is very wrong for me, because I'm forced to use WinXP at college, and have a dual-boot system at home (which means I have an option to boot Win2K3, if I want to.. I don't).\n\nHaving to switch between two very different systems are very problematic, and annoying, which means lower productivity and a lot of frustration.\n\nHowever, this is because I'm used to another button order. Anyway, changing button order should be easy to do. Besides that, the eye is in the lower left corner, when you get to the buttons, and not in the lower right corner. The eyes go from upper left to upper right, and from upper right to lower left, and from lower left to lower right....\n So button order should be (from left to right in a LTR locale, like european languages): Yes No Cancel (unless you're coming from Mac-worldm and are used to the mirrored button order - in that case: Stick to that one)."
    author: "Kristian Poul Herkild"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "\nAnd when that's finished we can start the discussion on whether OK is a sensible label, or whether it should reflect what action will be taken by clicking the button (ie label the buttons Print/Cancel rather OK/Cancel)\n\n\"Print/Cancel\" is clearly superior but many developers seem not to realise this"
    author: "cbcbcb"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "i don't know off the top of my head what the gnome guidelines say, but the KDE guidelines do recommend descriptive labels vs \"OK\" / \"Cancel\"\n\nhttp://developer.kde.org/documentation/standards/kde/style/dialogs/simple.html"
    author: "Aaron J. Seigo"
  - subject: "Re: Button order"
    date: 2003-02-04
    body: "Hmm. As a lot of people are pointing out, the button order thing is a thorny one, with some irreconcileable demands. On the one hand, there is a smallish but clear benefit to having the Apple/Gnome button order; on the other hand, people do _not_ want to relearn stuff, even when they ultimately benefit. A classic example of this is the Dvorak/qwerty issue. Granted, the relearning for using Dvorak is many times larger than for button order, but then, so are the potential benefits.\n\nWhat Gnome and KDE (and every sensible desktop) do get right, on the other hand, is \"make the save choice the default\". I honestly never reflect on varying button order as I work on different desktops. Instead I rely on being able to just choose the marked one (or just press return) when I don't want something to happen, and choose the other (with mouse or TAB) when I do. As long as that principle is obeyed by developers, the order is no longer truly significant.\n\nAnd don't forget that a different desktop looks and acts different in many ways, giving a clear contextual difference to a user, who can pretty easily learn different behaviors depending on the context/desktop. Having, as someone here suggested, a common desktop setting for stuff like button order, single/double click and so on would be great. Everybody wins.\n\n/Janne"
    author: "Janne"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "I've seen some research which claims the Dvorak layout does not improve speed. Others say it does... Sounds like the button question to me.\n\nRemove last year to reply..."
    author: "antiphon"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "I've seen those too, and all of positive research too.\n\nIn the end, it took actually trying the layout for over a month solid to realise how much better it really is (at least for me).  I'm not going back to Querty ;)\n\nDvorak was certainly biased in his research to prove the validity of his design,  but the work he put into the design itself is top-notch imho."
    author: "Hamish Rodda"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "  A classic example of this is the Dvorak/qwerty issue. Granted, the relearning for using Dvorak is many times larger than for button order, but then, so are the potential benefits.\n \nAnd do you think you would benefit from switching from Qwerty to Dvorak everytime you switch from a KDE app to a Gnome app ?"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "What's funny, though, is that you can't tab between buttons on MacOS X. \n\nUsability, anyone?"
    author: "antiphon"
  - subject: "Re: Button order"
    date: 2003-02-05
    body: "Unfortunately, the research on this is still a little bit thin, either way, IMHO.\n\nHowever, I believe it was wrong for GNOME to switch the button order in the 2.x series after already having done the opposite in 1.x.\n\nThis is bad consistency. It's like Microsoft suddenly deciding to move the Close button over to the left side (which happens to be where I like it ;). Mass demonstrations would follow.\n\nOnly if you are willing to significantly overhaul the UI is such a change as modifying the button order justified.\n\nRemove the previous year to email me."
    author: "antiphon"
  - subject: "\"ok\" and \"cancel\" and the likes are evil!"
    date: 2003-02-05
    body: "Buttons should make sense in the context they are used. \"ok\" and \"cancel\" are doing nothing so they should be renamed to whatever fits into the context. After this is done it won't really matter anymore in what order the buttons are since the user will be quicker at understanding the purpose of the buttons instead having to combine the abstract thinking of \"ok\" and \"cancel\" with the context related feeling of doing something dangerous. This feeling overcomes the user when he's unsure about something, and abstract \"ok\" and \"cancel\" buttons only give a feeling of security if they stay at the same place since the place actually implies which of both is more dangerous to use.\n\n(blah blah and so on and so on)"
    author: "Datschge"
  - subject: "Re: \"ok\" and \"cancel\" and the likes are evil!"
    date: 2003-02-06
    body: "I really think that this idea is overdone. There are disadvantages to having OK and Cancel, but there are also advantages. The disadvantage is that the buttons don't give you contextual info as to what exactly will be done. However, this should normally be done by the dialog as a whole. \n\nThe advantage, OTOH, is that \"OK\" and \"Cancel\" carry a lot of information because the user has seen them so many times before. Imagine if we _never_ used \"OK\" and \"cancel\". Then the user would have to read the dialog buttons and think about their meaning every time that we wanted to confirm or cancel a dialog! For example: \n\nPrint this file? [OK] [Cancel]\n\nwould become \n\nPrint this file? [Print] [Don't print]\n\nand \n\nConfigure toolbars.....\n[OK] [Apply] [Cancel]\n\nbecomes\n\nConfigure toolbars.....\n[Reconfigure and close] [Reconfigure] [Don't reconfigure]\n\nI really don't feel that these examples are adding anything. And they would take away the valuable association the user currently has: \"OK\" means confirm my choices in this dialog; \"apply\" means apply my choices without closing; \"Cancel\" means close this dialog without applying my choices. Even though I don't think \"OK/apply/cancel\" are very good choices of words (\"Cancel\" doesn't cancel once you have hit apply), this still works quite well across many contexts.\n\nDave\n"
    author: "dave"
  - subject: "GNOE & KDE"
    date: 2003-02-04
    body: "I just hope this development will be accepted in both communities. I is a Good Thing to have the choice between two desktops. but it's a bad thing if the choice of desktop rules the choice of applications. The ultimate goal would be component embedding between GNOME, KDE, and other Apps. Can we have gnumeric shets in OpenOffice text documents in KPresenter presentations, please?\n\nCheers"
    author: "silversun"
  - subject: "Re: GNOE & KDE"
    date: 2003-02-04
    body: "NO"
    author: "anonymous"
  - subject: "Re: GNOE & KDE"
    date: 2003-02-04
    body: "Only over my segfaulted body!\nBeing able to exchange documents correctly - any time ! \nCommon file formats - heaven!\nIdiocity-hooks for Gnumeric, GNOME-Office (wazzat?!), OO and KOffice - \nare you nuts?!"
    author: "sushi"
  - subject: "So..."
    date: 2003-02-04
    body: "Does this mean KDE will adopt double-clicking or that GNOME will adopt single-clicking?\n\nDoes it mean that KDE will switch around the button ordering on the dialog box or that GNOME will?\n\nReally, I'm curious...  How will those sorts of issues be resolved exactly?  It would really suck for KDE to change its default behaviour...  :-)\n"
    author: "Navindra Umanee"
  - subject: "Re: So..."
    date: 2003-02-04
    body: "It will mean, that the groups will cooperate and discuss these issues in the future.\n\nIn the mean time, the differences will be documented. It will be easy to see what both HIG have in coimmon and what is different. Then application programmers can choose which one they follow."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "The only possible disadvantage to single-click is the problem of selecting individual files. Yes, one can simply hold a modifier key and click but the beginning user is not going to know this.\n\nKDE should enable the auto selection of icons by default."
    author: "antiphon"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "Selection of a single file is a largely useless action, performed very rarely.\n\nLaunching (or however it is called nowadays) is the action most commonly performed, and the most useful.\n\nWhy bind the common and useful action to the hard-to-do, unintuitive double-click?\nIt would be like binding \"save\" to crtl-alt-shift-s and \"search in all documents in the disk\" to ctrl-s.\n\nIMHO, it would even be a better idea (although still bad) to use double-click to select ;-)"
    author: "Roberto Alsina"
  - subject: "You misread me"
    date: 2003-02-06
    body: "I think you misread my comment. I am a supporter of single-click. However, under our current setup, auto selection is not enabled, making it harder for a user to pick an icon w/o holding down a modifier key.\n\nI personally don't like it but since most newbies would benefit, KDE should enable auto select by default."
    author: "antiphon"
  - subject: "Re: You misread me"
    date: 2003-02-06
    body: "Oh, but why would anyone want to select ONE file?"
    author: "Roberto Alsina"
  - subject: "Re: So..."
    date: 2003-02-06
    body: "Independently on what you like, activation <b>should</b> be done with\na double click because this is how Windows does it and 99% people are\nused to Windows. Whether you like it or find it an Evil Conspiracy\nfrom Redmond is irrelevant."
    author: "KDE User"
  - subject: "Re: So..."
    date: 2003-02-07
    body: "I will abstain from answering in the same tone you used, just because I'm a happy guy.\n\nPerhaps you are oblivious to the concept of having two factors in a decision. In this case, there is one factor: familiarity, and there is another one, \"rightness\".\n\nBy familiarity, of course, we mean that if the user is familiar with double-click, he wants to keep on using it.\n\nBy \"rightness\", that double-click has problems which single-click lacks.\n\nNow, familiarity here is, for example, less of a factor than in the case of button ordering, since most users use interfaces with single-click=launch every day, in the form of webpages, not to mention single-click being the KDE default since ever.\n\nSo, unlike the case of button ordering, we would not alienate our current user base by keeping single-click, but by changing, and it could be argued that the alienating factor of users coming from windows is diminished to some point.\n\nOn the other hand, the \"rightness\" factor for single-clicking is pretty obvious. I never saw someone fail to single-click, but I see them fail to double-click often. I see people single-click on icons and wait. Making single and double clicks do the same action erases any source of confusion. Have you ever seen a user double click a link in a webpage? I haven't (I know amaya uses double-click to open a link, but wea re talking sane software here).\n\nSo, this \"rightness\" factor seems to be clear, even if unmeasured so far. \n\nNow, the whole decision about switching to double-click can be defined thus:\n\nIs the alienating factor involved on switching now to double-click larger than the alientating factor involved on keeping single-click, plus the rightness factor of keeping single click?\n\nThat is the whole decision, and so far, we can only argue one side or the other based on personal guesses, informed or not.\n\nNow, where did conspiracies enter? Nowhere. And I had made no mention of conspiracies in my previous posts.\n\nSo, I suggest you stop arguing with voices in your head and start arguing with actual people, me included. \n\nThis post presents, in a rational and comprehensible manner, the factors involved in the problem. Now hopefully it can be discussed properly. The problem with your post is that you take one side of the equation and declare it larger without even, apparently, understanding there is another side.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: So..."
    date: 2003-06-10
    body: "Well, this whole thing is very interesting. Keep in mind that I'm not some psycho, sitting at home and brewing government conspiricies, but one's gotta wonder, where did double click originate? My theory is that the government, which branch i don't know and don't care cause it's inconsequential, the \"damage\" is done, took interest in the home computer buisness shortly after it had built the frist internet like system. Shortly after this the \"PC\" or personal computer, which, might I add, is an acronym that the common person doesn't know what it stands for, came out on the market. Yes, so anyways, the government then influenced what we all know as the mouse. Gates, b4 he was mega sucessful recieved funding from the government to do research on the mouse. when they finally decided that it would be a sucessful thing to add to computers the government decided to mess with it. I mean really, think about it; who, in their right mind, would name a computer product after a vermin that is hated by the majority of the population? so anyways, the double click was born. I think that the government was just trying to see if they could get people to do something completely un-necessary. Might I note that this boarders on brain-washing. So that's what i think, tell me what ou think of my idea."
    author: "Gnome"
  - subject: "Re: So..."
    date: 2003-11-25
    body: "your whole theory on the double click makes sence, but did you really do all your research because it sounds like you are shalowly educated on what you are talking about. i do beleave that Gates is involved in the government, obviousley. but does he even know? i dont know there is just to much to think about when talking about the government and conspiricies, it is better to just not think about it because YOU WILL GO INSAINE."
    author: "sarah"
  - subject: "Re: So..."
    date: 2003-02-04
    body: "It means that one should think about those issues.\n\nIt must be noted that you can pretty much change the button order in KDE with a one line patch :) In fact it is literally a matter of changing a 0 into a 1.\n\nHey, this makes a great contest, whoever posts the above patch first wins a virtual bottle of Beerenburg (tm)\n\nCheers,\nWaldo\n\n"
    author: "Waldo Bastian"
  - subject: "Re: So..."
    date: 2003-02-04
    body: "Aha, so it should be run-time dependent.  \n\nWhen running in KDE, all apps should use a certain button order, as expected by the user.  When running in GNOME, all apps should use the expected order as well.\n\nGood call."
    author: "Navindra Umanee"
  - subject: "Re: So..."
    date: 2003-02-04
    body: "It would be even nicer if the button order were configurable via the UI in both desktops. After all it's already possible to switch between single-click and double-click in KDE."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Here's the patch"
    date: 2003-02-04
    body: "Do I win?\n\n--- kdelibs/kdeui/kdialogbase.cpp.orig       2003-01-29 13:07:26.000000000 -0500\n+++ kdelibs/kdeui/kdialogbase.cpp    2003-02-04 13:24:11.000000000 -0500\n@@ -62,7 +62,7 @@\n   {\n     box = 0;\n     mask = 0;\n-    style = 0;\n+    style = 1;\n   }\n\n   KPushButton *append( int key, const KGuiItem &item );"
    author: "Ravi"
  - subject: "Re: So..."
    date: 2003-02-04
    body: "And the winner is..... Ravikiran Rajagopal!\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: So..."
    date: 2003-02-04
    body: "Except even parts of kdelibs don't use KDialogBase.\n"
    author: "SadEagle"
  - subject: "Re: So..."
    date: 2003-02-04
    body: "What about using the native (running desktop manager) dialogs? For example, the kde printing dialog. Why not have the apps call a 'system' routine that brings up the native one?\n\nThis would simplify things enormously. For a 'best of breed' desktop', one would probably run applications from all the different desktop managers. \n\nI think this alone would create a much easier to use system.\n\nWhy is the mac held up as the paragon of ease of use? I have found it frustrating and counterintuitive. For example, I spent 10 minutes trying to find a terminal window with no success. Easy to use? Bah!\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "> Does this mean KDE will adopt double-clicking or that GNOME will\n> adopt single-clicking?\n\n  My god, this is so ridiculous. I use single click\n  in GNOME, which has been available since Nautilus\n  started being GNOME's file manager.\n\n  Anyway, KDE doesn't own single click, nor GNOME\n  double click.\n\n  The whole thing about single/double click is ridiculous."
    author: "ignorant ;)"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "It's all about the *default* values.  Nobody cares about clever hax0rs like you who can change the defaults, because most people don't. \n\nDouble-clicking is something that's pushed by Red Hat and is the default in GNOME.  Single-clicking is the default in KDE."
    author: "Navindra Umanee"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "> It's all about the *default* values. Nobody cares about\n> clever hax0rs like you who can change the defaults,\n> because most people don't.\n\n  Where did I say I\u00b4m a \"clever hacker\"  ?\n  Don\u00b4t be ridiculos, putting words on\n  someone else\u00b4s mouth.\n\n>\n> Double-clicking is something that's pushed by Red Hat\n> and is the default in GNOME. Single-clicking is the\n> default in KDE.\n\n  Distributors are free to do what they want\n  with free sofware, as long as they comply\n  with the licenses used (GPL, etc).\n\n  If \"single click\" (wow!) is so important to\n  the KDE community, please, make an ammendment\n  to the KDE licenses saying so, and then you wont\u00b4t\n  need to be whining about it anymore.\n"
    author: "ignorant ;)"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "I take back the assumption that you possess intelligence.  Apologies...  now go troll somewhere else."
    author: "Navindra Umanee"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "You raise a very good point. On the one hand, Red Hat and its associates want the OK button in the bottom right based on \"usability research.\" Yet most usability research DOES show that double-click is a skill that is quite difficult (and even impossible for some) to aquire. Yet, GNOME uses double-click. Makes no sense. Sounds more like aping Apple (pun not intended). \n\nReally, folks, Apple is not THE source of all good design. The Dock is a piece of crap compared to the replacements one can find on the Internet.\n\n\nRemove last year to reply..."
    author: "antiphon"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "It does? I'm running stock garnome, not tweaked single vs double click at all, and everything is single click."
    author: "Mike Hearn"
  - subject: "Re: So..."
    date: 2003-02-06
    body: "Interesting to hear that. I haven't been able to get GNOME to compile recently so I haven't been keeping track.\n\nCan anyone else confirm if this was changed in GNOME2.2?"
    author: "antiphon"
  - subject: "Re: So..."
    date: 2003-02-05
    body: "i thought that you could change kde to support double clicking obviously kpersonaliser was lying ;-) - what would be the point if all wm's acted the same by default?"
    author: "Anonymous Coward"
  - subject: "huzzah"
    date: 2003-02-04
    body: "it's really encouraging to see the acceptance of this project here, on the GNOME discussion boards and even more importantly on the open-hci list itself.\n\nit is very nice when people like Moritz, Waldo, Navindra, etc... understand what people are trying to accomplish. i was very disheartened by the noise on slashdot (we're trying to get a correction statement posted by them, btw), but when it comes to those actually involved it is very, very encouraging!\n\nfor everyone who is excited about this, please don't expect results immediately. give the project time to find its feet and actually get some work done. usability related discoveries are VERY slow going in my experience, often orders of magnitude slower than the actual coding required."
    author: "Aaron J. Seigo"
  - subject: "Is this a good idea at all?"
    date: 2003-02-04
    body: "There have been enough problems with bad communication between the HCI people (some of whom have a clue, and some of whom...) and the developers. Do we really want to compound it by adding in a whole new bunch of HCI people who know nothing about the framework they're talking about? There are certainly areas where cooperation will help, but don't think this is cost-free exercise. It should be noted that I have seen similar problems in the communication between the Gnome HCI team and the Gnome developers, so it is by no means unique to KDE.\n\nAnother major issue, is avoiding designing a set of guidelines that simply lead to the lowest common denominator. This would be stupid as they would simply be ignored.\n\nOn balance, I tend to think this has the potential to be an unmitigated disaster. I hope to be proved wrong, but I have a lot of doubts.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "all things have the potential to be an unmitigated disaster. so we need to recognize the risks and try and avoid it.\n\ni don't think ANYone thinks this is a \"cost free\" excersize. it takes effort; if it was cost free it would've been done ages ago. but this isn't an excersize in throwing \"a bunch of HCI people into the mix who know nothing about the framework they're talking about\". this is more like an online version of a conference where the topic is usability. we'll discuss usability from our own perspectives and frames of reference and draft commonly accessable documents reflecting that.\n\nas for creating a \"lowest common denominator\" document, i think what you stated is a truism and quite obvious. we've already covered this issue and understand that we all prefer (documented) divergence in our guidelines over stupid comprimises. open-hci is grounded in reality, not pie-in-the-sky dreaming.\n\nas for communication between KDE \"HCI people\" and KDE developers, i'm actually quite happy at how well it has gone. there have been very few problems and a lot of progress made. nothing is perfect, but i think there has been a great effort/results ratio. that ratio is improving as we learn how to make things work. remember that many of the people on kde-usability were KDE developers first and *then* joined the usability discussions. this is a key issue.\n\nthe GNOME HCI efforts have apparently been a little less effective, but that seems to be because the GNOME HCI people aren't GNOME developers, which is where kde-usability is quite different. on the other hand, they have more HCI professionals and greater access to usability labs and testing. hopefully both teams will be able to benefit from the other's strengths.\n\nnote that already there are offers for usability reports from actual labs that have been posted. we are expecting the first such set of material within the next few weeks. this is very, very encouraging."
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "I haven't noticed much of a tendency to CC maintainers when people start discussing their apps on kde-usability, so I don't think you can say there aren't communication problems\n"
    author: "SadEagle"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "did i say there aren't communication problems, or did i say that it isn't perfect and that there have been problems but that the rate of such problems has been happily low in light of the results? i apologize if i tried to phrase it in an upbeat and optimistic manner.\n\nyou're right that often the maintainers aren't brought into discussions right at the start.  it usually takes a lot of discussion, revision and time to get to a place where there is something worth looking at by the maintainer. it was decided that it would only annoy, discourage and/or consume the time of a maintainer if they sat through the discussion from the very start. \n\nthere is a policy of bringing in the maintainers once there is something resembling usefulness as most of us respect the time of volunteer developers. unfortunately some times this step has been skipped (either inadvertently or due to lack of experience), but kde-usability tries not to make that the case. \n\nif you say, \"but the maintainer might have important input to offer right from the beginning\" i'd suggest that if they did they'd have done so ... there are those who do just that, after all."
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "> it was decided that it would only annoy, discourage and/or consume the time of\n> a maintainer if they sat through the discussion from the very start. \n\nMaybe so, however at least let them know there *is* a discussion, that way they have the opportunity to take part in it.\n\n> i'd suggest that if they did they'd have done so ... \n\nAgain, that's fine if they have the opportunity. If you don't let them know there is a discussion though, they are denied that.\n\nRich.\nps. In theory this could even be done by a bot which monitored the number of times an app is mentioned.\n\n\n\n "
    author: "Richard Moore"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "> Maybe so, however at least let them know there *is* a discussion, that way they have\n> the opportunity to take part in it.\n \nfair enough. i'll let every developer know whenever there is a discussion concerning their code is being discussed (not mentioned, but discussed; i don't want to annoy people w/this after all). we will see how it goes.\n\nas a related aside, several times after having been informed that there is discussion going on in kde-usability i've been told \"cool. well, let me know how it turns out.\" \n\n> In theory this could even be done by a bot which monitored the number of times an > app is mentioned.\n \nheh... the suggestion of a true software developer! =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: ">  fair enough. i'll let every developer know whenever there is a discussion\n> concerning their code is being discussed (not mentioned, but discussed; i don't\n> want to annoy people w/this after all). we will see how it goes.\n\nThanks\n\n> i've been told \"cool. well, let me know how it turns out.\" \n \nI can understand that attitude, but I think a lot of developers are interested in the disucussion, as well as the result.\n\nRich.\n\n\n "
    author: "Richard Moore"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "I have to agree with Rich on this, which I always felt put me in good company. ;-)\n\nIt would be very frustrating to have a laundry list presented to me on usability of Quanta when it's a closed discussion. In particular it would be frustrating because I use the program every day, sometimes all day and key design decisions have been made on the basis of expediting professional development. It would be more than a bit upsetting to have someone who is not actually coming from that level of use to offer suggestions, particularly if they ended up being in some way detrimental to our design decisions.\n\nWhile I would like the program to be as usable as possible it could be useful to be able to make the case as to why some design decisions were made. That might make for a more useful outcome."
    author: "Eric Laffoon"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "\"closed discussion\". ok, let's not start spreading FUD here. last i checked, kde-usability is open for subscription and archived. if you aren't sub'd to kde-devel and someone mentions quanta, does that make it a \"closed discussion\"? look at the threads on kde-usability where the maintainers/programmers are involved in the discussions. often these discussions continue AFTER a usability report is finished. this is not something that is discouraged, but encouraged. \n\na usability report is not a final destination, it is a step in a process. by itself it is just text and largel useless. to be useful it needs action taken, which includes further discovery and discussion as necessary as well as coding effort. a usability report is not the end of the line, it's somewhere in the middle.\n\noften being very close to the program is a problem and it can (and often does) prevent one from seeing usability issues. a full history of a program isn't necessary to understand the usability of it, either. use cases, common guidelines and user testing are as well. do you need to know the whole history of a program to make a patch against a functionality bug? finally, suggestions are just that: suggestions. not edicts. not \"you must now do this. NOW!\".\n\nit really surprises me how uptight some developers are about others discussing and forming opinions on their work without them be right there to hold everyone's hand.\n\nit also really surprises me how some developers who will gladly accept a patch fixing a functionality bug as a sign of usage and support will shun a usability patch as an insult if they weren't in it from the begining.\n\nperhaps it's because some developers lack a concept of the process involved. would it help if a document explaining how it works was drafted up to alleviate worry about \"closed discussions\" and other non-issues?"
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "Let me start off by saying that I have a great deal of respect for you and appreciate what you're doing. Having said that I can say that you seem to have taken what I said out of context and then used it as a platform to make some less than flattering observations about  \"some developers\" of which I am not entirely sure I'm supposed to fit it. Perhaps a little more diplomatic caution might safeguard that \"respect equity\". ;-)\n\nFirst off by closed discussions I did not mean, closed as in shut out. That would be absurd. This is after all open source and one would have to be a bit touched not to know the lists were open and reactionary to make such a statement. I can't even begin to think of responding to the absurdity of mentioning FUD here so I will pass on that. By closed I meant the entire process of review and forming an opinion by many had begun and ended and moved on by the time a developer knew about it. It only takes a little reading the news to see how opinions can take on a life of their own with mob mentality in the Linux community. While I have a great deal of respect for the people involved with KDE I also recognize the dynamics of mental and communication processes.\n\nWhile these discussions may be open I have several lists I already subscribe to. I also am self employed and run a business (actually two) and manage a project that can easily add 20-30 hours to a work week that can easily consume my waking hours. I don't know why this is so hard to grasp... I don't have time to subscribe to every list that might at some time discuss my project! You would know this and more if you subscribed to our list. ;-)\n\nI feel your other statements are quite unfair too regarding uptight developers and a supposed perceived need for a history of a program. My concerns are that I want to be a member of the community with a good reputation for being a team player while I also want to be sure key areas of my vision for our project are preserved. My thinking was that usability implies use. What may appear more usable on a cursory examination may or may not stand up under heavy use. If I were observing the discussion I could provide input in such cases that might affect the recommendation and this could in turn affect further parts of the review for all we know. It is after all reasonable to assume that interface changes would have an affect on usage. Hopefully a good one, but if elements of use of the program were not understood it is logical to assume the recommended changes could be detrimental to the program objectives. That would place me in the position of having to take time to defend my thinking... It seems rational to tomake the case, should one need to be made, early on.\n\nI've not been aware of any usability studies of our program but I would find them interesting reading. We have already made usability changes to Quanta based on user feedback. I believe my attitude would be very similar to what I've read of yours regarding such things as single click/double click, a point I happen to be very much in agreement with you on. In fact I have overseen the interface design of Quanta for some time and our critera are as follows:\n1) Interface operations MUST allow for maximal user throughput. Efficiency is rule one.\n2) Interface operations must provide the highest level of control that can be reduced to the most simple presentation.\n3) Standard interface conventions are the goal.\n4) Any new interface feature is juried on whether it brings significant benefit or adds unneeded complexity. Quanta is *very* user extensible.\n5) All design operations must allow maximum control to the user and must not unduly constrict their usage WRT behavior or focus.\n\nAs far as the process goes, I think I can sum it up like this. It would be good if the process brought out good and useful ideas and was relatively efficient so as not to take away from other areas. It would not be so good if the process resulted in a number of suggestions that were in contradiction to our interface criteria and we ended up looking like we were unresponsive to our users for not accepting them. We must also remember that we are also dealing with a large user base now that is accustomed to how things are and that has lots of fun scenarios as well.\n\nI look forward to the advancement of our common cause and I ask that in the future you give me a little more benefit of the doubt."
    author: "Eric Laffoon"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "> By closed I meant the entire process of review and forming an opinion by many\n> had begun and ended and moved on by the time a developer knew about it. It\n\nas i noted, usability reports are not the end of the process, but somewhere in the middle of the process. a usability report is not a finished item of work, it is a living document that requires input from the stakeholders and ongoing review and revision.\n\nan example is the kopete usability report i'm currently maintaining. some of the issues raised were dropped or modified after further discussion with the developers involved. some issues have been addressed. the usability report needs to be updated in a timely fashion to reflect that.\n\nso no, it is not a closed discussion. \n\n> only takes a little reading the news to see how opinions can take on a life of their\n> own with mob mentality in the Linux community. While I have a great deal of\n> respect for the people involved with KDE I also recognize the dynamics of mental\n> and communication processes.\n \nwhich is why i'm concerned when developers start saying things like \"the usability process results reflect closed conversations\". we already deal with enough resistence from various people when it comes to usability issues (though it is getting better and better all the time) without encouraging those behaviours.\n\nas for my statements regarding \"some developers\", those are reflections of actual experiences. it is a vocal and difficult to deal with minority of developers whose attitude usually can be summed up by \"don't change things. users should be smarter. i like it the way it is. usability studies and published research don't match my instincts.\"\n\n> but if elements of use of the program were not understood it is logical to assume\n> the recommended changes could be detrimental to the program objectives. That\n> would place me in the position of having to take time to defend my thinking... It\n> seems rational to tomake the case, should one need to be made, early on.\n\ncan we discover the \"elements of use\" of a program as well as an author?  this can be accomplished by observing people using it for the first time as well as those using it for the 100th time. i do understand and appreciate your position of authority when it comes to how the program is (supposed to be) used, since you probably are more intimate with it than anyone else. it may occur over the course of exploring Quanta that i come up with the exact same use cases and patterns you have while developing it; or one of us may have missed something.\n\nstarting with fresh eyes allows one to explore without the prejudice of prior knowledge, and that often leads to new perspectives that are equally valid to those currently held.\n\nit also takes time for me in concert with others looking at the program being examined to arrive at useful conclusions. those useful conclusions may arrive at the same place you, the author, already have. which is good as that likely means those patterns should be reinforced and noted for further application elsewhere. \n\nyou say you have limited time to describe your thinking (\"defend\" implies a confrontation that does not exist) and i respect that. which is why i usually don't bother with dragging the maintainer into threads that span several weeks or with the day-to-day status of user testing that i'm doing. the maintainer gets a work-in-progress report once it's at a point that their input will be the most efficacious. it's much like presenting a patch once you've thought through the implementation of it, compiled it and debugged it locally. do you prefer to see half-baked concepts or fully thought out concepts?\n\nperhaps ask the kopete developers if having the usability report delivered once i had a completed draft to show them resulted in lots of wasted time? actual experience trumps theory, so i think we should look at actual experience.\n\nalso remember that action resulting from a usability report takes place only after consultation with the maintainer (if they are interested and can be found). i know of a couple cases where unfortunately this didn't happen and it resulted in bad things and i think everyone on kde-usability learned from it.\n\nas for your 5 point list, i agree with each and every one of them. it is great when developers put such care and thought into their interfaces. this is happening more and more and is a great thing.\n\n> It would not be so good if the process resulted in a number of suggestions that\n> were in contradiction to our interface criteria and we ended up looking like we\n> were unresponsive to our users for not accepting them. \n\nagreed. so the question is: does this actually happen? i think most usability work to date has been in agreement with rather than at odds with the design of the software in question. and you are free to ignore anything you wish, just as with any development suggestion/patch. this is not user -> developer communication, but developer <-> developer communication.\n\na usability report isn't the result of a popular opinion poll, but the result of examination from an HCI perspective that is often (if not always) coupled with user testing. this means taking the technical implications into consideration, knowing that it may be up to the reported to provide patches for some items, etc..\n\n> We must also remember that we are also dealing with a large user base now that\n> is accustomed to how things are and that has lots of fun scenarios as well.\n \nthen it's good thing that we take that into consideration when examining usability and trying to improve it. i'd LOVE to see the bookmarks menu in KFD go away, but the user base that relies on it's specific use pattern is too large and valuable.\n\nas i said, i think a lot of the reservations some developers have regarding the process are a result of not understanding the process we take. and that's understandable as i've never actually formally communicated that process. and that probably adds to the fear, uncertainty and doubt that some developers hold towards usability efforts.\n\nwhether or not \"some developers\" includes you, i don't know. i think it is best if each is left to examine themself."
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "> > By closed I meant the entire process of review and forming an opinion by many\n>  > had begun and ended and moved on by the time a developer knew about it. It\n \n>  as i noted, usability reports are not the end of the process, but somewhere in the \n> middle of the process. a usability report is not a finished item of work, it is a living \n> document that requires input from the stakeholders and ongoing review and revision.\n \nObviously that would make a difference. Not having been involved in the process and given the record of some projects ebb and flow of energy it is not unreasonable in the absence of other information to assume a report would be issued, findings made and that's it. If it can't be closed then it would not fit either definition. As sensible as it seemed it appears by your definition I'm in error. If that is the case I apologize.\n\n> which is why i'm concerned when developers start saying things like \"the usability\n> process results reflect closed conversations\". we already deal with enough resistence\n> from various people when it comes to usability issues (though it is getting better and\n> better all the time) without encouraging those behaviours.\n \nObviously you caught me in an assumption, which was also based on comments from Rich. It certainly appears to me that there may have been some assumptions in both directions looking at some of the generalizations used.\n\n> > but if elements of use of the program were not understood it is logical to assume\n>  > the recommended changes could be detrimental to the program objectives. That\n>  > would place me in the position of having to take time to defend my thinking... It\n>  > seems rational to tomake the case, should one need to be made, early on.\n \n>  can we discover the \"elements of use\" of a program as well as an author? this can\n>  be accomplished by observing people using it for the first time as well as those using\n> it for the 100th time.\n\nThat's a good question. We try to push the envelope in capabilities and frankly I am not sure how much is usability or how much is just wanting to fall into familiar patterns and avoid what is new. I've yet to see a review of Quanta that did not miss features primarily because the reviewer did not get in depth. Does that mean they are not clear? I doubt that as users often mention those features. I suspect it is more that people don't actually use a number of features until they really get to the point where a need pushes them there. Features like templates, scoped toolbars, programmable actions and such. I think we've done a fairly good job here but the truth is I think what is needed would be more definitive examples and samples of use to get people to think into higher possibilities... something we are looking to do. How would usability fare in these less used areas? It's a good question. a better one is if it would make a difference. Of course some areas we just have not gotten to optimizing too but that doesn't mean suggestions would not be interesting.\n\nSo in reality you could put a newbie in front of Quanta and make an assessment and come up with lots of data. However one of my larger concerns is with implementation of some of the newer more powerful features that only an activly involved entrenched web developer would need or explore. I'd hate to have the process miss points. If anything when your feature set grows there is little you can do to avoid people missing somethinng that is right in front of their face.\n\nAll I want is good data. In fact one of my challenges is having a feature set large enough that I need more people developing with our CVS version and feeding back. It is only getting more challenging and time consuming... so rather than being uninterested i'm interested in thorough and complete data and useful suggestions. We're only human.\n\n> starting with fresh eyes allows one to explore without the prejudice of prior\n> knowledge, and that often leads to new perspectives that are equally valid to those\n> currently held.\n \nCertainly, but I want to be sure a representative user spectrum is covered. As we expand our features and scripting and markup languages this becomes more and more difficult. \n\n> it also takes time for me in concert with others looking at the program being\n> examined to arrive at useful conclusions. those useful conclusions may arrive at the\n> same place you, the author, already have. which is good as that likely means those\n> patterns should be reinforced and noted for further application elsewhere. \n \nThat would be be an honor I'm sure, but I chose KDE because it was apparent that both architecture and interface design considerations were easily among the very best I'd ever seen. That and the people are why i'm so proudto be affiliated with this group.\n\n>  you say you have limited time to describe your thinking (\"defend\" implies a\n> confrontation that does not exist) and i respect that. which is why i usually don't\n> bother with dragging the maintainer into threads that span several weeks or with the\n> day-to-day status of user testing that i'm doing. the maintainer gets a\n> work-in-progress report once it's at a point that their input will be the most efficacious. \n\nIn light of what you've revealed about the process that seems to make more sense. however to take it a step further I would think you would still contact a developer first and ask if they have areas of concern where they want to be sure adequate focus is given. In my case I think it is possible some exhange here might be beneficial and also help to expedite your process. If it is a two way process then this seems logical.\n\n>  as for your 5 point list, i agree with each and every one of them. it is great when\n> developers put such care and thought into their interfaces. this is happening more\n> and more and is a great thing.\n\nThank you, but frankly if not for that core thinking I wouldn't even be bothering to produce a program. My objective is to create the first killer app on KDE that has web developers coming en masse. Given that it's hard to know everyone in the community and follow everything I hardly expect you'd know that. However given that perspective you would tend to expect I wanted to make any rational improvement possible... that or I was delusional. ;-)\n\n> > It would not be so good if the process resulted in a number of suggestions that\n>  > were in contradiction to our interface criteria and we ended up looking like we\n>  > were unresponsive to our users for not accepting them. \n \n>  agreed. so the question is: does this actually happen? i think most usability work to\n> date has been in agreement with rather than at odds with the design of the software\n> in question. and you are free to ignore anything you wish, just as with any\n> development suggestion/patch. this is not user -> developer communication, but\n> developer <-> developer communication.\n \nAgain this just supports the fact that I want to get through the \"hoops of process\" looking like a great guy without internally conflicting myself. Unfortunately I'm operating in a vacuum until I step in and find out if it's a dentist chair or somethig a little more fun.\n\n>  as i said, i think a lot of the reservations some developers have regarding the\n> process are a result of not understanding the process we take.\n\nI suggest you convert that from a hypothetical to an absolute. ;-) I would suggest the \"usability\" of your usability program would be better served by a brief description of the process which you can make available, perhaps including testimonials if you think they are good. When the currency of open source is the pride in what you do there is an obvious, but not insurmountable, element of diplomacy in your work.\n\n>  whether or not \"some developers\" includes you, i don't know. i think it is best if each\n> is left to examine themself.\n \nClearly I was not the developers from past experiences. Being a pragmatic business owner in my 40s with substantial marketing experience makes me an odd duck among developers. Many of the people I greatly admire are young enough to be my son. Given that my sole objective all along has been to create the best of class application I would maintain the \"some developer\" comments fell short of your usual high standards. Usually when I begin generalizing at people I take a break get some air and then do the diplomatic thing. Of course I can be cantankerous the rest of the time. ;-) \n\nHopefully what you will take away from this is that I am not \"some developers\", I probably think very much like you in terms of usability and I am totally committed to excellence. I am probably as committed and accessible as anyone heading up an app/package in KDE. I'm certainly aware of the immense benefits KDE provides me in my effort. \n\nThanks for your efforts and addressing my concerns."
    author: "Eric Laffoon"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "I am pretty sure you know pretty well how hard it is to keep track of all development lists -- after all you've edited the KC KDE.  And thus surely, you can't expect everyone to be subscribed to kde-usability (and now also open-hci !); and if they aren't, how will they be aware of the discussion from the beginning until they are told of it? \n\n\n"
    author: "SadEagle"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "i don't expect them to be aware of the discussion on kde-usability. \n\nit's simply a fact that some developers don't have much interest in examining the usability of their interface or don't have enough time to look into it too deeply. those that do often discuss it either on the respective -devel list or in a usability forum. and that's what i meant: those who care to spend a lot of time on this topic do. those who don't, don't. i don't expect me sending emails to each maintainer is going to change that very much. it may help alert those who do find it interesting and do have the time to get involved, though."
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "I think you're mistaken Aaron.  On the contrary,the Gnome HCI developers have been extremely effective.  They've managed to streamline common GNOME interfaces and are respected by the GNOME hackers.  In fact, increasing numbers of GNOME projects ask for UI guidance during their development process in order to make life easier for _users_."
    author: "NoneForYou"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "hey, i'm just repeating what i've been told by GNOME HCI folk themselves. you may note that i didn't say they were innefective, nor are the less effective in all things. i note they have more HCI professionals and that i see that as a benefit. rather, the approach of \"engineers write code, HCI-interested people document and provide feedback\" is different than the approach of \"developers write code, developers examine and provide patches\" and that the latter seems to result in changes more quickly.\n\ni'm aware that many GNOME developers are interested in and are taking an active part in usability issues, as are Free Software developers in general it seems. and this is Good. open-hci wouldn't exist without that sentiment.\n\ni also noted that i think both groups can learn from and take advantage of the strengths of the others.\n\n(starting to get off-topic)\n\nso is my writing that bad, or are people just that uptight about these topics? and is this why so little has been done usability-wise in the Free Software world?\n\n(fully diverging)\n\nsomeone pops up with eye candy feature #40020, or yet another app feature and people ooh-and-ah. but try working on refining what exists rather than slapping down new code and see how much more difficult and unrewarding it is due to the people issues involved."
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "'creating a \"lowest common denominator\" document, i think what you stated is a truism and quite obvious' - you may think that, but looking at the people on the list, aren't they the same ones who broke KDE on RedHat? I'm told they also removed a lot of good UI features from the RH KDE in the name of consistency. This is hardly a good start.\n\n'as for communication between KDE \"HCI people\" and KDE developers, i'm actually quite happy at how well it has gone.', as you know the people on the UI list have managed to infuriate me a number of times (and have consistently failed to look at the discussions of the kde-look list that preceeded kde-usability) so I am a lot less happy than you are. That said, the signal to noise ratio coming out of the list has improved, so perhaps there is hope.\n\n'the GNOME HCI efforts have apparently been a little less effective, but that seems to be because the GNOME HCI people aren't GNOME developers'. I agree they have been less effective, but I think you've got the wrong reason. KDE has made a lot of progress in it's UI design by writing the libraries in such a way as to make the default behaviour correct - for example XMLGUI, KStdAction, KDialog and to a lesser extent KDialogBase. This approach has made it easy for developers to do 'the right thing' and has made things work such that doing the wrong thing is more work. The Gnome framework I AFAIK has not been designed this way.\n\nI remain skeptical, but I have joined the new list to see what happens.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "> you may think that, but looking at the people on the list, aren't they the same ones\n> who broke KDE on RedHat? I'm told they also removed a lot of good UI features\n> from the RH KDE in the name of consistency. This is hardly a good start.\n \nwaldo and i broke KDE on RH? *snicker* seriously though, some of the GNOME people on that list aren't in RH's employ and had literaly nothing to do with KDE anywhere, let alone in RH. some people on the list don't have involvement with either project. yes, there are people on that list who are directly responsible for KDE in RH. but rather than seeing that as a negative thing, i see it as a potential means to help shorten the gap between their decision making and KDE's best interests. \n\n> as you know the people on the UI list have managed to infuriate me a number of\n> times\n\nlet me reassure you that it goes both ways. i've run into some very uncooperative and uncaring developers. so shit happens. that's what beer, music and other such things are for. =)\n\n> That said, the signal to noise ratio coming out of the list has improved, so perhaps\n> there is hope.\n \ni'm glad that this trend is apparent to others besides myself... it means i may not be as completely loopy as some might think ;-)\n\n> KDE has made a lot of progress in it's UI design by writing the libraries in such a way\n> as to make the default behaviour correct\n>The Gnome framework I AFAIK has not been designed this way.\n \nagreed 110%. i think that at all the people who have worked on KDE's framework over the last 5 years deserve a HUGE amount of respect for how well they designed things. it can be, and is being made, better in all sorts of ways, but the foundation is a solid one and there is a LOT that is very good about it. this is a slightly different issue than what i was refering to, however."
    author: "Aaron J. Seigo"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-04
    body: "> i've run into some very uncooperative and uncaring developers.\n\nI think one reason for this is likely to be that the developers are often brought into the discussions much too late. If they were included earlier, I think the majority would be happy to work with the UI list. At the moment though, things don't seem to work that way.\n\n> it means i may not be as completely loopy as some might think ;-)\n\nNo, no don't take one isolated example to mean that! :-)\n\n> the foundation is a solid one and there is a LOT that is very good about it. this is\n> a slightly different issue than what i was refering to, however.\n\nWhat I was trying to say was that I think designing the framework to make common standards the defaults was why we reached the current (fairly high IMHO) level of standardisation on accepted UI designs. I think *that* has been the crucial factor in making UI design work effective.\n\nRich.\n\n\n"
    author: "Richard Moore"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "I think it's a very good idea. My suggestions for an agenda:\n1) Common agreement as to mimetype data and where they're stored\n2) Creation of uniform theme plugin structure, just like the Netscape standard can be used by Motif, Qt, GTK. All pixmap themes must be destroyed, or at the very least, a uniform widget set agreed upon.\n3) Agreement on what icon theme format. SVG must become standard.\n4) Move to single-click with automatic individual icon selection. This is more important than OK/Cancel debates. The research clearly indicates superiority of single-click.\n5) Display of mountable devices. KDE still is buggy WRT this. Nautilus has a better implementation. Mod-click desktop, choose device you want. KDE should copy.\n6) In-place menu editing.\n\nWould KDE users be willing to \"trade\" single-click in GNOME for OK in the bottom right in KDE?"
    author: "antiphon"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-05
    body: "1) Out of scope for an HCI group.\n\n2) Out of scope for an HCI group (and wouldn't work very well anyway).\n\n3) Why /must/ SVG become standard? I see some advantages to it, but for small icons + low powered machines I think bitmaps will be a lot more efficient.\n\n4) We already use single click. We don't use automatic activation though (and personally I hate it).\n\n5) What on earth are you talking about?\n\n6) I'm still not convinced this is the best way to handle menu editting, but at least this is in scope.\n\nRegarding your last point, you seem to have totally misunderstood the idea. It is not a forum for horse trading UI elements (and if it becomes one then it has failed IMHO).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-06
    body: "1&2: As far as the mimetype stuff goes, while it isn't exclusively (or even mainly) the province of an HCI group, a common infrastructure must be fitted into the UI so that a user can modify mimetypes in both environments.\n\n3: I agree that low-powered machines should not use SVG. But for anyone running about a Pentium II 300, it should be default. There is no reason why we cannot ask the user (or administrator) to specify which to use. Perhaps one theme for low-powered devices can be created but it is too much of a hassle for artists to have to create PNGs for five different sizes.\n\n4: _We_ use single-click but GNOME (last time I checked which was a while ago) is still on double-click. This should be changed.\n\n5: You don't know what I mean by mountable devices? I am talking about the function where user is able to mount any device he has permissions to according to the systems fstab. Right now, it is a lot more convenient to mount stuff in GNOME than in KDE since all mounted volumes show up on the Nautilus desktop. Unmounted volumes can be mounted simply by right-clicking on the desktop and choosing it from a submenu. The KDE way is not as intuitive.\n\n6: Granted not everyone likes in-place, however, it should be there as an option for quick editing and for those accustomed to it in other operating environments.\n\n7: I agree that autoselect is irritating. (I never use it myself.) However, the newbie user will NEVER figure out that she needs to hold down a modifier key and click an icon to select it w/o activating it. The power user can disable it but it is very nice for the beginner."
    author: "antiphon"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-06
    body: "7. Why would a user need to select a single icon?\n\nAnd he can also select it by boxing it."
    author: "Roberto Alsina"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-10
    body: "Do you realize how unituitive this is? How many beginners would ever think of this concept on their own? I'd wager it's pretty close to zero.\n\nKonqy is set by default to hover links in pseudo-selection fashion. It should do the same w/icons."
    author: "antiphon"
  - subject: "Re: Is this a good idea at all?"
    date: 2003-02-10
    body: "Uh... about as many as would think on their own that double-click launches things?\n\nPlease read the message in context. I simply mentioned ANOTHER way the users can select icons. The important part, of course was \"why would a user want to select ONE icon?\", which apparently noone has an answer for.\n\nI don't understand what \"hover links in pseudo-selection fashion\" means at all, so I will abstain from commenting on it."
    author: "Roberto Alsina"
  - subject: "Where have I heard this before?"
    date: 2003-02-04
    body: "Have we not heard this before ?.....past attempts at co-operation haven't lead to anything significant for the end-user   what'll make this one any different ?"
    author: "anonymous"
  - subject: "Re: Where have I heard this before?"
    date: 2003-02-04
    body: "have led to anything significant? you mean like the .desktop standard? or XDND? or the systray standard? or icon themes? or NETWM? or X clipboard standardization? or......\n\nthat said, what will make open-hci a success (rather than simply different) are the concerted efforts of those involved, awareness of reality and a general level of competence coupled with realistic expectations.\n\ndon't expect GNOME and KDE to share a single 100% identical HIG. that's unrealistic and not the goal. our ambitions for colocation, documentation and collberation are, IMO, realistic and achievable.\n\nplease don't sell open-hci short before we even get a chance to start. thank you."
    author: "Aaron J. Seigo"
  - subject: "Re: Where have I heard this before?"
    date: 2003-02-05
    body: "Speacking of the .desktop standard.  Is there anyway to have relitive paths for the Icon part.  I keep images of the covers of my cds in the same dir as the oggs.  When I move the album it gets messed up as to where the image is.  It would be much easier if I could do something like Icon=cover.png instead of Icon=/path/to/album/cover.png"
    author: "theorz"
  - subject: "File Chooser Dialog"
    date: 2003-02-04
    body: "The File Chooser Dialog is my favorite part of KDE, silly as\nthat sounds.   So my hope is that GNOME will do something\nabout their awful dialog and learn from KDE what a user friendly\ndialog looks like."
    author: "TomL"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "Yup .. another thing KDE stole from Windows .. albeit they have made it better"
    author: "Ho hum"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "Actually, when I wrote the first version I based it more on the Motif dialog. I fixed the obvious flaws, then made the various views optional. It is true though, that the default configuration these days is very similar to the windows dialog.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "Actually we had a submission here a while ago about all the things Microsoft's newer interfaces are stealing from KDE/GNOME...  it's more two-way than you think."
    author: "Navindra Umanee"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "I'm with you in this point. One can't even found a HOME button on certain Gnome apps."
    author: "honor"
  - subject: "KDE usability is a joke"
    date: 2003-02-04
    body: "As I seem to recall (I'm using RH7.3 KDE, but I'm almost sure this hasn't been changed) the KDE file dialogs use  folder icons yet for actions that manipulate folders they use the word \"directory\". You want to create a new folder, but there is no \"new folder\" button, there is only a \"new directory\" button. I want to create a folder, but dammit, all I can find is  a button to create a new phone book. And if I want to create a new phone book in a  file dialog and hit the \"new directory\" button, no dice. All I get is a new folder. \n \n\nKeep terminology and metaphor consistant. Avoid jargon. It's stuff any HCI student is taught within the first several weeks of class. It's something apple has done since 1984, using the word \"Folder\" in their earliest file dialogs on a machine with a 9\" black and white screen and several thousand times less processor cycles and ram than we have today. Yet to this day KDE is still blowing it on this simple thing that is so easy to change. Sure, most geeky people have no problem using the word \"Directory\", but then you have to ask yourself what a design for geeks is doing in a desktop environment targeted at end-users. \n\n\nI even approached the KDE usability group about this lack of consistancy and adherence to the most basic of UI design principles. They considered this not very important and blew me off. If the people entrusted with making KDE usable can't even grasp basic usability principles, then the KDE usability project is more or less worthless. \n\nI'm no fan of what GNOME has done, and they've made some really big missteps with their usability program. But at least they have the common sense to match the folder metaphor with the folder terminology by calling things represented by folder icons \"folder\". If KDE won't get their act together by fixing stupid, simple stuff like this, GNOME is probably better off not adopting any KDE designs. \n\n\n\n"
    author: "Ilan Volow"
  - subject: "Re: KDE usability is a joke"
    date: 2003-02-04
    body: "you were not \"blown off\". you asked for a fork to take place, while less radical and more effective methods were suggested. you talked about folders vs directories, and you were pointed in the direction of the definitions that are used for each which. this was not \"blowing off\" but discussion.\n\nhttp://lists.kde.org/?l=kde-usability&m=102650798104375&w=2\nhttp://lists.kde.org/?l=kde-usability&m=102589236801573&w=2\n\nbtw, the challenge we face w/directories and folders is the conflict between commonly accepted icons (usually of a folder) and the underlying system (which refers to \"folders\" in the filesystem as directories). this has been discussed quite a bit by the documentation team it seems...\n\nthat said, there are much bigger issues in KDE that need addressing that will result in bigger strides forward than quibbling over when to use folder vs directory, UI vs system consistency, etc... we (or least I) have limited time to deal with all of these things. so i have to prioritize where to spend the few hours a week i have on these things (right now i'm posting between writing code at work). if i had all week to spend on usability coding i might just take the time to grep the code for every use of the word Directory and replace it with the word Folders, including the documentation. this is not a trivial task, and would result in only marginal improvement.\n\nin other words, i need to try and obsess about the right things. "
    author: "Aaron J. Seigo"
  - subject: "Re: KDE usability is a joke"
    date: 2003-02-05
    body: "I did not ask for a fork to take place. I asked for a bug to be fixed. And here I was thinking that open source was about quick bug fixes. I mean, I keep hearing about linux kernel security holes getting patched in hours, but for some reason I keep seeing really simple-to-fix usability problems on both major linux desktops taking years to get corrected, if ever.\n\nThe challenge you (and by \"you\" I don't personally mean you; I mean the entire group of people developing linux desktop stuff, KDE, GNOME, etc) are facing is that you are still carrying around 30 years of command-line unix geek baggage that you refuse to get rid of, and this is polluting efforts to produce decent user interfaces for people who think graphically and don't want to twiddle about with their computers for 16 hours a day. \n\nFollow the link below. Pay extremely careful attention to #2. I suggest the KDE documentation department put down their Neil Stevenson essays for just a moment and read it. \n\nhttp://www.useit.com/papers/heuristic/heuristic_list.html \n\n\nIf the issue about the terminology correction is mainly time, I have no problem downloading the entire KDE source tree from cvs and doing the purging of \"Directory\" and the replaceing with \"Folder\" for you. I've been doing a lot of grepping and replacing as of lately. Yes, I am being serious about this. \n"
    author: "Ilan Volow"
  - subject: "Re: KDE usability is a joke"
    date: 2003-02-05
    body: "> are facing is that you are still carrying around 30 years of command-line unix geek\n> baggage that you refuse to get rid of, and this is polluting efforts to produce decent\n> user interfaces for people who think graphically and don't want to twiddle about with\n> their computers for 16 hours a day. \n \nKDE has little say in the matter of what resides beneath the UI. this is not an excuse, it is a reality. that said, UNIX geek \"baggage\" isn't all bad although some of it is. people just tend to notice the bad before the good, and there are no blanket statements. ;-)\n\n> http://www.useit.com/papers/heuristic/heuristic_list.html \n\nyes, this is a good list...  \n \n> If the issue about the terminology correction is mainly time, I have no problem \n> downloading the entire KDE source tree from cvs and doing the purging of\n> \"Directory\"  and the replaceing with \"Folder\" for you. I've been doing a lot of \n> grepping and replacing as of lately. Yes, I am being serious about this. \n\ncool.. of course, be sure not to change it where it should be \"Directory\" (yes, i like to state the obvious ;) ... perhaps if you started w/kdelibs and kdebase so we can see what it looks like and how well it works and elicit comment and testing before moving on to the rest of the code base?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE usability is a joke"
    date: 2003-02-05
    body: "well, here is my suggestion:\nappend the generic name to apps in the menus.\nXandros and the noob distros just rename it with\nthe generic name, ie pan would become newsreader.\nWhy not have both, ie Pan Newsreader.\nTrivial , I know but evey little bit helps."
    author: "kannon"
  - subject: "Re: KDE usability is a joke"
    date: 2003-02-05
    body: "We do that already, since 3.0\nBu default too, IIRC\n"
    author: "Sad Eagle"
  - subject: "Re: KDE usability is a joke"
    date: 2003-02-05
    body: "> this has been discussed quite a bit by the documentation team it seems...\n \nYes. And the consensus was about as described in the postings you quoted. Problem was: Nothing ever came of it. The proof reading team somehow couldn't bring itself to take the thing to core-devel and then finally start to make the use of the terms consistent in the existing files.\n\nRegards,\n\nThomas"
    author: "Thomas Diehl"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "I think its awfull.. but much more functional than gnomes at the moment.\nThe problem with it is that there are too many things a user can click on and it just looks clothered (hope its the right term in english). \n\nI dont understand some of the design decisions. Sometimes reading through the mailing lists it looks like the user in mind is the \"secretary\" in the office or grandma or whatever kind of \"newbie\" then things like the File Chooser show up. This dialog is Ok for me and people at these forums but d'ont forget that we are power users.\nNewbies- /ordianry people dont even know how to right-click an understand the context menu. They dont even know how to minimize/maximize or even move a window. Some of these people have been using computers for years. I experience these people daily doint Windows support.. so please dont make a copy of Windows dialogs.. get inspired by the Mac.. it's easyer to understand for everybody"
    author: "J And"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "firstly... have you had a chance to look at the file dialog in 3.1? eugenia from osopinion.com did quite a bit of nitpicking on the dialog in 3.0 and much of her work translated into changes being made for 3.1. it's still probably far from perfect, but hopefully closer.\n\nsecondly... can you offer specific examples of what real problems real users run into? not knowing how to bring up a context menu is irrelevant in the file dialog if it isn't something they need to do to use it, for example. real use cases are *very* valuable, however, and can result in corresponding action. \n\nnote the use of the word \"real\". guessing, hand waving and assuming are things i can do all by myself quite well ;-) actually knowing what real users do is not as easy. i observe real life usage fairly often and that helps me notice problems; if others can do the same (even if it is self-observation) and provide feedback based on that it is always welcome as a treasured resource. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-05
    body: "Firstly:\nYes i doubledchecked right now on both 3.1RC6 and 3.1. I find it very good for my needs but I remenber that i was confused when i first saw it. My main point is that there are too many things you can click on thus making it confusing for a newbie. Its all Ok for people like us but would be overwealming for the kind of people i have to deal with daily. Unfortunattely i don't have i better solution i can present :-) - i know this stuff is pretty hard to do.\n\nSecond:\nThe file dialog is just one example of a complex action/dialog that can confuse certain kinds of users. My experience is that people run into problems doing stuff that we find ordinary. Examples could the previous mentioned simple actions like righcliking or moving Windows or just clicking on the \"+\" sign in Device Manager (I support users on different Windows platforms). This stuff that we take for granted can be very difficult for an ordinary user.  Sometimes even doublecliking is a problem for some. Imagine that these users have a more or less cluthered interface in front of them and they become compeltely confused.. there is simply to much information around for them to grasp.\n\nMy sugestion would be to keep the interfaces as simple as possible. An example could be kmail.. there are 14 icons and 9 menu entries in the default configuration - too many for  \"grandma\" to handle. I think icons for  \"check mail\" , \"new message\" and \"reply\" would do it - a power user could easily add the remaining icons/options later. \n\nBTW..Knode is not version 1 yet.. but why is  the font dialog in properties not equal the one in Kmail? I feel it is equally important to be consistent.. maybe programs that do not stricly follow KDE standards should not be distributed with the official KDE. Maybe there should be a usability team that could - developers should be forbidden there - veto a release or inclusion of a program if it didnt follow standards. Yeah.. i need to be spanked for writting that ;-)\n"
    author: "J And"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-05
    body: "> The file dialog is just one example of a complex action/dialog that can confuse\n> certain kinds of users. My experience is that people run into problems doing stuff\n> that we find ordinary\n\ni understand that as i get to regularly interface with sales people and management as part of my day-to-day work. it's the specifics that are needed: WHAT is an obstacle and HOW can it be made easier. not in the abstract, but in the specific.\n\n> An example could be kmail.. there are 14 icons and 9 menu entries in the default\n> configuration - too many for \"grandma\" to handle.\n\nwe also want it to be fun for the user somewhere between you and grandman, too, but you are right that kmail's auxilary interface (everything outside the central widget area) is overly and unecessarily complex. it isn't the only app like that, though. we have a LOT of usability work to do ...\n\n> Maybe there should be a usability team that could - developers should be forbidden\n> there - veto a release or inclusion of a program if it didnt follow standards.\n\napplications have been disallowed from KDE due to non-standards compliance. removing an app that has a large following is not an easy thing, though. better is to fix it. that is one of the aims of kde-usability. of course that requires developers; so banning them is probably counterproductive. =)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Most user are not total newbies"
    date: 2003-02-05
    body: "Remember that now, in the present, most users aren't complete newbies.\nSo, you must do something for the majoritie of users, which are not developers, but can deal with somewhat complex file dialog IF they are well documented.\nThe newbies are not going to use a computer without going to an introduction curse, because most (if not all) of them are not technically-minded people, and tend to be \"afraid\" of complex technology. I think that you can't sacrifice the \"medium user\" capabilities to help them, because you will create a greater problem (e.g.: you have an e-mail app which has only the buttons \"new mail\", \"reply\", and \"check mail\", you will end helping the people who have trainers, but confusing the most of user because they can't found a way to view the contact list, and they wont configure their toolbars because this is a difficult task. Perhaps, the most important example is the very frequent question \"where is my hard disk, my c:?!!\", with a scared face, because most users know that a pc has a hard disk, it's one of the most valuable assets in their offices and they can't found it. Most users came with a cultural background in computers, and you must accept this).\n "
    author: "Andrea"
  - subject: "Re: Most user are not total newbies"
    date: 2003-02-05
    body: "When faced with KDE for the first time, no matter how much people say it's a Windows clone, it is intimidating because everything is new. However experienced you are at this point it is much easier to learn an application where by default it is simple and usable and by configuration can be made more powerful.\n\nThe usability team sure do accept that many people have a background in computers,  but this doesn't mean much. I know plenty about cars, but I would rather sit in a new vehicle faced by a simple set of controls than something that allows me to adjust the oil pressure, tyre pressure, spoiler height etc. Maybe this is an excessive example but the point is sound. Simplicity and usability are important to everyone.\n\nIf kmail only had three toolbar buttons I'd be pleased. I only use check, new and reply, and it would be simple enough for even \"grandma\" to understand and use.\n\nIt's not about sacrificing power anyway, it's about creating defaults that are a happy settlement between usability and usefulness. I tend towards usability since power users are usually happy to muck about in settings dialogs."
    author: "MxCl"
  - subject: "What we need is user levels"
    date: 2003-02-05
    body: "The personalization wizard should offer the user to tell what type of experience he/she has with computers. A beginner would be given a simplified box.\n\nIntermediate to advanced should get the one that we have now.\n"
    author: "antiphon"
  - subject: "Re: What we need is user levels"
    date: 2003-02-07
    body: "This is the best suggestion so far.\n\nEven as a power user, I still only use a small subset of ALL of the features in any given application, so even I would want a grandma simple interface where I can add the few icons that I would need to extend the interface.\n\nKonqueror has far too many buttons that can be clicked on. Look at the Safari interface .. damn it looks good, clean and usable :) \n\nPlease consider this suggestion."
    author: "Agreed"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "All I know is that I can do what I need to do faster\nand more easily with the KDE file dialog than with\nany other that I've ever seen - and I like the looks\nof it, too.\n\nMy goodness, the Open Office file dialog makes me ill.\nAnd the gnome one isn't much better, if at all.  \nI don't hate gnome, but a file dialog is one of\nmy hot points.  I know it's silly."
    author: "TomL"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-04
    body: "it's not silly at all. the file dialog is likely the most visible link between an application and the file system. users probably see it more often than any other single interactive dialog on the desktop. it's very important."
    author: "Aaron J. Seigo"
  - subject: "Re: File Chooser Dialog"
    date: 2003-02-05
    body: "I too also like it and its far better than Gnomes current one.. \nThe problem is that if we want KDE to be used by complete newbies or non-power-users then we have to think about their needs and problems. Trying to look at it from these users perspective then the dialog is far to complex and powerfull. Maybe i can come up with a better idea some day :-)\n"
    author: "J And"
  - subject: "Button Labels"
    date: 2003-02-04
    body: "I hope that the guidelines and the code will say the same \nthing, and that the guidelines will not degrade any further.\n\nFor example, the Apple UI guidelines make it very clear that\ndialog box buttons should not say \"yes\" and \"no\", if\npossible, but should instead contain descriptive verbs, \ne.g. \"save\", \"discard\".\n\nWhen last I checked the Gnome guidelines, it was hard to get \nthat interpretation.  Some Gnome developers have claimed that \nthe guidelines actually forbid descriptive verbs on buttons,\nwhile others say that any conflicts between the Apple and Gnome\nguidelines are bugs in the Gnome document.  I haven't read the \nKDE guidelines.\n\nThis isn't academic; in the version of Evolution I use,\nyou have to read carefully to figure out whether a message \nbeing composed will be discarded or saved.  Gnumeric behaves \nsimilarly.  Did the KDE guidelines get this right?"
    author: "Nathan Myers"
  - subject: "Re: Button Labels"
    date: 2003-02-04
    body: "http://developer.kde.org/documentation/standards/kde/style/dialogs/simple.html\n "
    author: "Aaron J. Seigo"
  - subject: "Re: Button Labels"
    date: 2003-02-05
    body: "Apropos simpe dialogs example. In windows 9x when a user reinstalled their network then a user was presented with a dialog something like this \" Windows has discovered that there is a newer file installed than the one youre trying to install. Should Windows keep the current file  -- YES _ NO\". Most people - including me - didnt read this long boring text so we just cliked yes when the proper answer should be no in order to get a working system. Specially problematic  when the user clicked NO to the next file etc. So a better dialog could be \"Overwrite file\"...\"Keep file\"... and of course a shorter text ."
    author: "J And"
  - subject: "Re: Button Labels"
    date: 2003-02-05
    body: "Wonder of wonders, KDE got it right (at least in the guidelines).\nFurthermore, the guidelines are wondrously clear, unambiguous,\nand prescriptive.  I hope the programs actually do what the \nguidelines say.\n\nIn any case, judging by this sample, the Gnome guidelines document\ncould benefit by wholesale cut-and-pasting from the corresponding \nKDE text.  Presumably the license to that text is less restrictive\nthan for Apple's similarly clear text."
    author: "Nathan Myers"
  - subject: "Re: Button Labels"
    date: 2003-02-05
    body: "Cut the FUD please:\n\nFrom the GNOME HIG:\n\n# Label all buttons with imperative verbs, using header capitalization. For example, Save, Sort or Update Now. Provide an access key in the label that allows the user to directly activate the button from the keyboard.\n\nSorry, but the KDE style guide is nowhere near as complete or useful as the GNOME one. I'm hoping this effort will help unify them so we all benefit, especially KDE. Last time I checked, the style guide (guides?) weren't even linked to from usability.kde.org, or if they are I couldn't find them, only a list of \"issues\" with it."
    author: "Mike Hearn"
  - subject: "Re: Button Labels"
    date: 2003-02-05
    body: "It's a fact that I got responses from Gnome developers claiming\nthat descriptive verbs were forbidden.  It's a fact that in the last\nversion of the Gnome UI guidelines I looked at (last summer, most\nlikely) I couldn't find the text you quote.  It's a fact that some \nof the most heavily-used Gnome programs (including Evolution and\nGnumeric) ignore the guideline you quote.\n\nNothing improves without problems being identified.  Identifying\nspecific problems is nearly the opposite of FUD.  FUD depends on\nvague descriptions of supposedly unfixable problems.  Gnome's\n(and KDE's) UI problems are both real and eminently fixable.\n"
    author: "Nathan Myers"
  - subject: "Re: Button Labels"
    date: 2003-02-05
    body: "It's a fact that \"gnome developers\" is far too vague a group to be useful. Do you class the developers of GnomeMeeting as gnome developers? GStreamer? Gimp?\n\nIt's also a fact that both Evo and Gnumeric stable were released before the HIG was finalized. In fact, the next versions of both of these (i have tried gnumeric and seen screenies of evo) are being brought into compliance with the HIG at the same time as being ported to GNOME2. This isn't \"ignoring\" the HIG - the changes the HIG gives aren't simply a matter of flipping a few bits, stuff like instant apply, borderless frames, and usable icons need to be designed into the app.\n\nIn general, if you decide to criticize a document, go ahead, but please make sure you've actually read the latest version before pointing out flaws in it. What I've read is version 1, so if you read a previous version by definition it wasn't considered finished."
    author: "Mike Hearn"
  - subject: "Re: Button Labels"
    date: 2003-02-06
    body: "Funny, the guy who spent all his free time bashing Konqueror/KHTML/Safari speaks of FUD."
    author: "ac"
  - subject: "Re: Button Labels"
    date: 2003-02-06
    body: "Jeez. I didn't bash Konqueror, nor KHTML, nor Safari. I was bashing peoples attitude towards the whole affair. It was purely personal opinion, with some economic arguments thrown in to justify that opinion. It could hardly be called FUD. This was pointing out some inaccuracies in the post."
    author: "Mike Hearn"
  - subject: "Gnome zealot"
    date: 2003-02-06
    body: "You produced a tremendous amount of FUD to bash Apple, khtml and kde to further your own agenda: Apple should have chosen gecko over khtml. Yes, I especially liked the way you said here that Apple shouldn't have chosen khtml because they write proprietary software, not to be trusted etc, but over at osnews you said they should have chosen gecko because khtml sucked. What a two faced hypocrite; but then this is the expected behaviour of rabid gnome zealots. "
    author: "ac"
  - subject: "FUD"
    date: 2003-02-06
    body: "Not to mention the FUD you have been spreading on slashdot.org about startup notification, when GNOME 2.2 simply implemented a spec largely created by a major KDE developer.\n\nDIE, YOU TROLL."
    author: "ac"
  - subject: "Re: FUD"
    date: 2003-02-06
    body: "This behavior is really sad and inappropriate. It is quite frankly humiliating for the human race to hear such juvenile pettiness, \"Startup Notification FUD\", \"Your not human like me, your a Witc^H^H^H^HTroll! Burn!\". Unbelievable. I thank god that mindless biggots like yourself arent the ones who any power of any sort. I cannot think of words to describe how disappointed I am with people like yourself right now.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: FUD"
    date: 2003-02-06
    body: "I agree.  Here is the full link to his pettiness, forever archived for all to see: http://slashdot.org/comments.pl?sid=52921&cid=5234195\n\nHe is refuted by RedHat/GNOME hacker hp in same thread."
    author: "ac"
  - subject: "Re: FUD"
    date: 2003-02-10
    body: "Leave Mike alone, he's a very decent guy. You on the other hand seem like a complete fool.\n\nI have the utmost respect for Mike, he knows what he's talking about. I haven't read these threads you speal of but I've no doubt you're blowing them out of proportion.\n\nAlso you have nothing to say in all the posts in which you bash him in this thread, why don't you just say nothing at all? What a waste of space you've created.\n\nFool."
    author: "MxCl"
  - subject: "Experience from a KDE newbie"
    date: 2003-02-05
    body: "Hi, I have been using KDE for a couple weeks now from the introduction of my nerdular boyfriend so I think I can offer a fresh perspective.  I normally use windows, so I chose the windows settings from the \"Desktop Settings Wizard.\"\n\nFirst impression: KDE looks pretty.  I especially like the long round circular things at the top of every program that says what it is.\n\nSecond impression: Icons and menus are confusing to me.\n\nVery few of the icons make sense to me.  I clicked on the little pencil/paper icon that looks like its on a book, and it minimizes everything and shows the background.  Doh!, that's the \"show desktop\" operation, says a little tooltip later.  (Great, now I can move the mouse over the other ones to see what they are.)\n\nThe icon for a \"shell\" doesn't convey to much to me.  Actually, I thought the shell might be some french fries at first. :)  The weirdly tilted E for email seems odd to me.  The web browser button looked like some kinda gear on a planet to me also -- was later told that maybe its some ship steering-wheel thingy to mean \"navigation.\"  Maybe I'm just strange and my brain is trained for windows, but I'd think these could be better.\n\nThe help button as a life-preserver is OK though.  But the actual help is a bit..overwhelming.  And the start menu or whatever you guys call it..  oh my god!  There is so much stuff in there, and everything is named Kthis and Kthat, Kblahblahblah.  The only thing I like is that whatever I ran last it gets put up at the top, so I can start it right away when logging in next time.\n\nAlso, cut/copy/paste does not always work between all KDE programs.  Sometimes only CTRL+C will work and other times CTRL+INS will work and not the other.  Argh!  (I checked that they were set in Control Center too.) My windows key also does not do anything, or any of my internet buttons and this keyboard is like 3-4 years old.\n\nI see some people arguing about button order and dialog stuff.  Whats the big deal?  I'm not a programmer, but can't you just make that a setting in the Desktop Wizard Settings program?\n\nOne thing that bugs me with dialogs is that when they give you the choice between, say, \"Yes,\" \"No,\" \"Cancel,\" you cannot just press one of the keys they underline as you can in windows.  In windows I could just press Y or N or C to do any of the above.  I also think when the application asks me \"Do you want to save it?\"  The answer  that comes to me is Yes or No. I don't think Discard.  Discard what?  Discard this dialog? Discard my typing? Huh? The same thing goes for Cancel, which is in windows too but it never made sense to me either."
    author: "Anon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "I forgot to mention. When trying to adjust the size of a window, it seems hard as hell to get your mouse cursor in just the right spot so that you can drag it.  That one really bugs me."
    author: "Anon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "Try pressing ALT and right mouse button while moving the mouse. It's pretty convinient to resize windows that way. "
    author: "j"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "first, welcome to kde =)\n\ni agree with some of your icon observations; some are not as obvious as they should be. as for the web browser icon, does a lower case \"e\" with a swoosh on it convey \"web browser\" any better? probably not, but you learned what that icon meant.\n\nthe kmenu (start menu) is very big because KDE comes with so much software. not all of the application names are very helpful, to be certain. the 'k' prefix is there to make sure that the names are unique on your system. there are a few people working on various solutions to these challenges.\n\nas for the cut/copy/paste issues, which version of KDE are you using? these issues were supposed to have been worked out with the KDE3 series; outstanding problems should be considered bugs. which apps weren't working properly with cut 'n paste?\n\nregarding making things like button ordering configurable, yes we can do that. the question is should we. things get too configurable quickly, which has implications for general usability, learnability and tech support. this isn't to say \"no, we should not make widget option X configurable\", but that there are a lot of issues to weigh.\n\nto use the keyboard to \"press\" buttons in dialogs, use Alt and the letter than is underlined in the button. e.g. Alt-o will select \"Ok\" and Alt-c will select cancel.\n\nit's interesting that you said \"Discard what? .. Discard my typing?\" because that's exactly what it means. you wondered if it means to discard the dialog as well, but just as you applied \"yes\" and \"no\" to the question in the dialog the button names apply to the question as well. usability studies have shown this to be generally more effective than generic \"yes\" and \"no\".\n\nthanks for the observations ... you raised some interesting and valid points. i'd be interested in more such information from newer users such as yourself as they provide useful data points.\n\n\"nerdular\". heh."
    author: "Aaron J. Seigo"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "I'm with her about the yes-no question. I started to use every day OpenOffice, and I found their way to ask what to do with my document so confusing that I have to experiment with a \"trash document\".\nIt ask me \"The document x.swd has been modified. What do you want to do? Save-Reject-CAncel\". I didn't understand the difference between reject and cancel.  I would prefer the easier version \"Do you want to save the changes to x.swd? yes-no-cancel\". This is more familiar, shorter, and easier.\nI don't agree about using specific verbs, not always.\n"
    author: "Andrea"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "Hey, thanks for the welcome.\n\nAbout the e-swoosh thing..  I don't find that one intuitive either.  With the tool tips I didn't find it a big deal, just thought I'd point it out.\n\nI'm using KDE 3.1 and often when I try to copy something in Konqueror and paste it somewhere else, it does not work.  Sometimes none of the keyboard shortcuts work and I have to highlight what I want to copy and then go to the pull-down menu and select \"copy\" with the mouse (and the \"copy\" selection will even say  \"CTRL+C\" next to it and not work).  Sorry I don't remember exactly what I was doing when it happened.\n\nAbout the button ordering thing..  the way things are now, I always have to read what they say because of a lack in consistency in the questions they ask or in what the default pre-selected button is.\n\nFor pressing dialogs when they are right out at you, it seems kind of weird to have to press ALT to me.  It asks you a question, and there are only a limited number of things you can do -- why bother needing to press ALT?\n\nAs for the usability studies, I am suspicious of that.  Whatever the case, in the context I mentioned it I still think it's confusing.\n\nAlso with the \"nerdular\" thing, I was just being silly; no offense intended.\n\nAnother thing I also forgot ot mention..  It took me awhile to get the fonts to look just right.  I had to go look at what the settings were on a windows computer and copy them over into konqueror and then fiddle with the sizes.  I think people who use their computer more casually would have probably have given up right there."
    author: "Anon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "About the copy and paste thing - yeah that one got me too. You see that little clipboard icon in the bottom corner ? That's KClipboard, the clipboard manager. It's purpose is so you can have numerous things \"copied\" at once. Click on it, and you'll probally see the last 5 or so things you copied. However, if that is not to your liking, you can always disable it, and KDE will copy and paste just as any other OS would (i.e. Windows).\n\nAs far as button order or fonts, well what exactly is \"just right\" ? I think people all too often confuse being 'right' with the Microsoft stardard pratice or way of doing something :)\n\nAnyways, Happy computing, and welcome to Linux..."
    author: "chillin"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Um well I guess it's hard to explain, but basically some fonts looked really crummy.  Not because of the font themselves mind you, just how they looked in KDE.  And things seemed to be sized wrong all the time.  I have also just tried the Opera browser and it has its fonts/sizes \"just right.\"\n\nRight now I have them all looking quite nice, except when courrier new is italicized, some letters are deformed.  I have no idea why, and of course don't have the technical expertise to figure it out.\n\nI searched the web a bit and lots of people recommended anti-aliasing your fonts.. so I tried that.. but they looked worse!  Maybe thats just my opinion though, I don't know, but I think that only fonts that are really big looked better with that option turned on.  Seems that getting your fonts looking great is some kind of black art.\n"
    author: "Anon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Fonts ceased to be an issue with Linux about 2 weeks ago, so I'm not surprised you had poor results. Basically a new font system has been designed and is now being integrated. Redhat 8 was the first, and generally has rather spiffy fonts, especially if you steal ones from Microsoft (corefonts.sf.net) :) Now we have our own fonts donated by Bitstream as well, so some pretty fonts with decent font configuration combined with high quality antialiasing means that hopefully within about 6 months all distros will have this one licked."
    author: "Mike Hearn"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "First, let me just say that I agree with you that the icons are sometimes non-descriptive, the problem is that most icons aren't that descriptive. A few are, and those are good. You took the example of the browser icon. Hint: While 'e' doesn't say much to me, it's still an icon.\n\nSecond, the button issue. What is most descriptive given that a question haven't been asked?: 'Save' or 'Yes'. What about these two: 'Quit' or 'Yes'. What about these two: 'Discard' or 'Yes'.\n\nThe problem really is that most people don't really read the question. The _idea_ is that most people shouldn't have to. \n\nFurthermore, whenever a dialog pops up and gives you a choice, it's about an action waiting to happend. 'Cancel' really is descriptive and intuitive. It cancels the action."
    author: "Simon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "The user has two phases in their relationship with the app:\n1- He/she try to understand it. Then, he usually read everything with a lot of attention, trying to understand what the program will do.\n2- He/she knows the program. Therefore, she/he won't read the question. She/he will expect that the app work in an stardard fashion, always \"yes\" to confirm the action, always \"cancel\" to \"go back, escape and keep safe\", always \"no\" to be the most dangerous option.\n\nI found confusing the use of specific verbs in some contexts. It can lead to confusion, and it broke the standard.\n\n"
    author: "Andrea"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "> Second impression: Icons and menus are confusing to me.\n\nYes, this is a known problem. Try Redhat 8 - the default BlueCurve desktop drops a lot of application branding to make things more obvious, so \"Evolution\" becomes \"Email program\", with an icon of a letter etc. That's an attempt to make things easier for people like you. Note to KDE fans: please don't flame me for this advice (the default desktop on redhat8 is gnome).\n\n> The icon for a \"shell\" doesn't convey to much to me.\n\nBy default both KDE and GNOME provide quick access to a terminal/shell, because unfortunately they are still needed often for Linux. Again, Redhat 8 doesn't have such a button in a very prominant place.\n\n> Maybe I'm just strange and my brain is trained for windows, but I'd think these could be better.\n\nAgreed. Redhat have the right idea here. Having said that, I switched from the BlueCurve icons to the defaults, I thought they were prettier.\n\n> And the start menu or whatever you guys call it..\n\nAgain, a known issue. Redhat 8 goes a long way to addressing this, the apps are renamed (at least in the menus) to reflect what they do. KDE could do well to adopt a similar policy (gnome has gone part of the way there, but not fully).\n\n> One thing that bugs me with dialogs ...\n\nThat's a bug in the app, all buttons should have accelerator keys. Hopefully this OpenHCI effort will make the open source community more aware of such things.\n\n> I see some people arguing about button order and dialog stuff. Whats the big deal?\n\nBasically, the GNOME/Apple button ordering is faster once you get used to it, because buttons are in logical and consistant places, so you don't have to stop and think about clicking on them so much. It may sound insignificant, but really it's little details like this which make an environment that much more pleasant to use.\n\nHowever, Windows (and KDE) do it differently. Some people are very much used to that, and feel that the change breaks their habits, which frustrates them and slows them down.\n\nI think it seems to be a personal thing. I move between gnome2 and windows all the time, every day, and never notice it. I hardly noticed it when moving from KDE3 to GNOME2, I was surprised to find out what a lot of arguing it caused.\n\n> can't you just make that a setting in the Desktop Wizard Settings program?\n\nIn short, that's not a good idea. Most users don't alter the defaults except in a very minor way (wallpapers etc). This stuff comes from actually watching users and doing proper research. So, if you make the default something you know to be worse simply to avoid pissing off a vocal segment (the minority??) most users lose, and they don't even know it.\n\nAlso, resolving every argument with \"well make it a preference\" led to serious nastyness in terms of usability. GNOME 1.4 was the ultimate in this respect, there were for instance 4 kinds of clock applet. 4 clocks! And they were named:\n\nClock\nAnother Clock\nAfterStep Clock\nJDBC Binary Clock\n\nObviously that'd confuse the hell out of anybody who was new, regardless of how good with computers they were. The control centers became massive and bloated, and it was impossible to find what you want. For GNOME2, the environment was completely stripped down to the bare bones, in effect UI wise they started again. Now they introduce each feature, each setting with a lot of discussion - is this needed, or is it just working around a deeper problem etc. KDE are doing this as well but not quite so dramatically, in KDE3.1 the control center was cleaned up a bit, but it's still IMO too complex. On the other hand, gnome2 is very light on features now...... well, I still prefer it.\n\n> I also think when the application asks me \"Do you want to save it?\" .....\n\nWell, it's this kind of thing that leads to such huge arguments. One side says \"look what the users say\" and another side says \"users don't know what's best for them\". In this case, there is quite a bit of research that shows people are faster when buttons have descriptive labels rather than Yes or No, because it's harder to be confused by slightly ambigous questions, and so you can process the dialog faster.\n\nI should think another area of contention will be instant apply prefs (settings) - in gnome2 when you change a setting, it normally takes effect instantly, no need to press apply or OK. So prefs dialogs just have \"Close\", not \"OK, Apply, Cancel\". I'm expecting flamewars over that too."
    author: "Mike Hearn"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "\"BlueCurve desktop drops a lot of application branding to make things more obvious, so \"Evolution\" becomes \"Email program\", with an icon of a letter etc. That's an attempt to make things easier for people like you.\"\n\nThat goes too far and is unnecessary. It is too general.\nThe answer is \"Evolution Email\",\"Konqueror Browser\" etc.\n\nespecially if these apps are in a well named folder such as Internet,\nMail it makes comprehension very easy. Newbies don't need that much\nhand holding\n\n"
    author: "kannon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "Nah, I don't think so. In particular \"Konqueror Browser\" would be a bad name, people are used to seeing \"Foo Browser\" to mean something that browses foo, like hardware browser, web browser etc. What is a Konqueror?\n\nThere's not really any need to put the names in, especially given that many open source programs have dumb names: kvirc, kscd, kate? Gnome is bad at this as well, although getting a bit better. Programs will get their branding and name recognition from the about box, and other such places."
    author: "Mike Hearn"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-05
    body: "Do you really think a menu with 5 \"Text Editor\" entries would be a good idea? "
    author: "SadEagle"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "That's the reason you only have one text editor, and you choose the best."
    author: "Mike Hearn"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Good luck doing that. There are 3 text editors in KDE alone, and each has a loyal following - even kedit. And not to forget vi and emacs.\n\n"
    author: "SadEagle"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Text editors was probably a bad idea. Most users don't need something like vi, emacs or kate though, unless they specifically request them from the package manager. So I'd go for kedit, i think that's the simplest.\n\nMost apps don't have same kind of following text editors do though"
    author: "Mike Hearn"
  - subject: "Kedit"
    date: 2003-02-06
    body: "Kedit doesn't have dynamic word wrap so newbies should stay away."
    author: "antiphon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "-That's the reason you only have one text editor, and you choose the best.\n\nAha! But which one is the best? I prefer Vim, but I also sometimes use kwrite, others prefer emacs, Xemacs, gedit, NEdit....though any of the aforementioned are a huge improvemnt over the wimpy Windows notepad. And it's not just editors, I use both Knode and Mozilla for news groups, I have different groups set up on each, and each has some feature the other lacks. I use Konqueror and FileRunner for file  management now that I've switched to KDE on Red hat 8, before that I used FileRunner and and XFtree with XFce (I've never liked either Nautilus or the previous Gmc file browser, though Nautilus has much improved lately) because each one has particular strengths and weaknesses.\n\nDiffent apps performing similar functions exist for a reason, I'd like to have a sportscar, a pickup truck, a limo and a Range Rover in my garage but I can't afford ti and it would be a huge wast of resources, but having several file managers, text editors, mail or news clients on my system costs me next to nothing, each meets a particular needs better than another, just as different cars do. "
    author: "Ned Collins"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "What is the problem with the way KDE has it now?\n\nKonqueror (Web Browser)\nMozilla (Web Browser)\n\nI think most people would understand that."
    author: "ac"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Perhaps our friend is not using a KDE 3.x version?\n\nThe parenthetical names were not standard until 3.0"
    author: "antiphon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Dunno. I don't recall seeing them in the KDE3 that shipped with SuSE, but I last used that in September, so my memory might be faulty."
    author: "Mike Hearn"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-18
    body: "Sorry, I am late to the party. Maybe someone will see this. I think the menu situation in Redhat is comparatively better than in Mandrake.\n\nMost people don't think 'Internet Browser' when they see Network. Strictly speaking, since you can have a Network without the Internet, you should not hide Browser icons in the Network menu. Too unintuitive. Have 'Internet' for all stuff like web browsers, and 'Network Configuration' or something like that. I dno not know if this is so much a Kde issue or a Mandrake one, but I think it needs resolving. Redhat's beta is already showing signs of further integrating th menu properly."
    author: "Maynard Kuona"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Yeah! And you have the complete newbie having to ask \"'how can I put this on 'Word Processor' or 'How I create a bookmark on Web Browser\". \nI think you must start to putting only 'THE BEST', trying to judge that on usability terms, and give some identifier name.  \n"
    author: "Andrea"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "I agree totally. I think a big part of the problem is the lazyness of newbies. They spend years learning how to use Windows, yet they think Linux should be 5 times as easy to learn to use, or the interface should be set up exactly like Windows 98 just because they're used to using Windows. Linux should not have to over compensate for people who have Microsoft tendancies, as it is its own operating system with its own idenity, not another release from Microsoft. Put forth a little effort people :)"
    author: "chillin"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Umm, who was saying it should have to over compensate?  I really don't think my experience is far from what a typical person would encounter.  I've spent hours trying to get the same kind of functionality I've learned to expect from windows and macs.  Ideally, nobody should have to do that.  They shouldn't be told that they should just \"put forth a little more effort\" and divert blame to them either.\n"
    author: "Anon"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "I don't think his inital response, nor my reply to it was targeted directly at you. Having said that, it takes way more than hours to learn an OS, regardless of which one you use. Windows 9.x, Windows NT, Linux, Unix, MacOS X, BeOS... whichever one you use - no OS can be learned from the ground up in a number of hours. Ususally it takes a week or 2 minimium. So neither should Linux be expected to."
    author: "chillin"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "It seems that using that trash of a desktop enviroment 'BlueCurve' is your soloution for everything. I use KDE 3.0.4, and every version after 3.0.2 had all the icons labeled. It says Konqueror Web Browser, KMail (Mail Client), and so on. So what exactly is the complaint here ? Because Windows sure as hell doesn't label every single icon/menu in the OS."
    author: "chillin"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "It so happens BlueCurve addresses many of the issues this person had, including, but not limited to:\n\na) Menu clutter\nb) Icons being confusing\nc) Menu naming.\n\nLast time I used KDE was the version that came with SuSE 8, but presumably the original poster used a fairly recent version, and was still confused. My point was that these issues are recognised and some people are doing something about them. If KDE are too, great, I'm just not aware of that."
    author: "Mike Hearn"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "Yes, you are very ignorant and still intent on spreading FUD.  I have read you on slashdot.org."
    author: "ac"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "And you are even more ignorant. Heck, you're even a dirty old pervert! Don't deny it, I've seen your goatse.cx posts on slashdot.org!"
    author: "Me"
  - subject: "Re: Experience from a KDE newbie"
    date: 2003-02-06
    body: "LOL! Yeah he is pretty immature, isnt he?\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Very Encouraging"
    date: 2003-02-05
    body: "This is excellent news. The GNOME usability team has done a very good job at creating a professional usability culture for GNOME from which KDE can only benefit."
    author: "Grumpy"
  - subject: "Re: Very Encouraging"
    date: 2003-02-05
    body: "I have to fully agree with you.\nI'm a KDE guy but working with Gnome 2 at work I'm impressed. The funny thing is even if it doesn't have much of new features compared to Gnome 1.4 it definitely makes me more productive because of the way they arranged it. \nKDE should take a lesson from Gnome that Less Is More when designing the GUI. With Gnome 1.4 & KDE you cant see the forest for the trees. \nJust look at Corel Draw, to many rarely used features makes the important hard to get at. Look at Adobe products, just enough to get you by and invent new ways to combine them to create new as you go along. I'm not saying KDE should reduce the features, but they should not be cranked in everywhere in the GUI because it's the latest fad of the month.\n"
    author: "Fredrik C"
  - subject: "A word to KDE"
    date: 2003-02-05
    body: "Please...I understand that this is possibly so people won't be confused switching from desktop to desktop, applications will look more native no matter what desktop environment you use, etc....but I have been using KDE for a LONG time...1.0 or before.  I just checked out the gnome 2.2 screenshots and IN MY OPINION (notice that before you flame) Gnome is just plain ugly.  I've never liked the way gnome looks.  \n\nSo, while you work in this venture PLEASE PLEASE do not change the look of KDE.  There are many of us totally satisfied with the way KDE looks and I don't want to see it become just another gnome.  \n\nThank you. "
    author: "Michael Staggs"
  - subject: "Re: A word to KDE"
    date: 2003-02-06
    body: "opinionated prick.\n"
    author: "ac"
  - subject: "Re: A word to KDE"
    date: 2003-02-08
    body: "so what are you?\n\nTony"
    author: "Tony"
  - subject: "Re: A word to KDE"
    date: 2003-02-06
    body: "It's not about looks but about usability."
    author: "Grumpy"
  - subject: "Summary"
    date: 2003-02-07
    body: "About Button order, double click, etc:\n\nCould this not be a documented divergence between the desktops?  Better yet, it would be nice if GNOME apps running in KDE would use KDE button ordering and single clicking (configurable of course), and KDE apps used GNOME button order and double click in GNOME.\n\nAbout file dialog boxes:\n\nYes KDE has a nice one, yes GNOME knows theirs is not, yes theirs will be better when the new GTK comes out.  No need to flame the GNOME people on this one or overly flatter ourselves with how nice the KDE one is.\n\nCompoment embedding:\n\nI like the suggestion that embedding kspread and gnumeric in OO should be a feature.  I think that having a common file format between Abiword, KOffice and OpenOffice would be more valuable for this though.  You could use any spreadsheet app to make a spreadsheet readable and embedable by OpenOffice this way.  I know there was discussion going on between the different office suite developers about maybe moving towards the OpenOffice file format, but I don't know how much development is happening there.\n\nI'd still like to see the ability to embed any X application inside another though, so that we could have KDE and GNOME software as plugins to Mozilla, as well as GNOME and KDE stuff embedding in one another.  I'm not sure how possible any of this is and how much work if any has been done.  (Didn't there used to be a way to make any X application a KPart?)\n\nEverything else:\n\nSadly much of the rest of the discussion is dwelling on past communication problems and arguments between KDE and GNOME or between HCI and the desktop developers.  Flaming GNOME or the HCI guys accomplished nothing.  Not moving forward because the past isn't rosey doesn't get you anywhere.  I say we should wish this new initiative good luck and be as helpful as possible.  Cooperation between GNOME and KDE (and maybe/hopefully Apple someday too) on usability is a good idea."
    author: "Anonymous Monkey"
  - subject: "Re: Summary"
    date: 2003-02-08
    body: "Very well said. I disagree on only one point though: apple can go to hell..."
    author: "chillin"
  - subject: "Standardized TTFs?"
    date: 2003-02-09
    body: "A few weeks back the folks at Bitstream were kind enough to make their Vera truetype fonts available for use to the Gnome Foundation in Linux/Open-source Distros. I would assume KDE can use them too.\n\nWould it be plausible to make Bitstream Vera the default (truetype) font for KDE/Gnome?  They look much nicer at \"typical\" resolutions (1024x768 or so) than the Adobe fonts that are usually packaged.  Myself, I have no problem copying over the TTFs from my Win partition and doing the ttmkfont trick, but many Linux newbies don't know this, and it would be helpful for them to have good-looking TTF's right away.  Just a suggestion.\n"
    author: "PRR"
  - subject: "Excellent"
    date: 2003-02-09
    body: "This is wonderful news, and not a day too early. Thanks for your contributions on this very important subject."
    author: "EUtopian"
  - subject: "KDE and Gnome usability"
    date: 2005-10-01
    body: " I think it would be great if KDE, Gnome and other desktops, window managers, or what ever, agreed with the same file formats and some general guidelines. I don't think we should agree on everything, but we should have good sense and some agreement on important things, as usability is concerned. Defining what are those important things should be the first task.\n\n Anyway, deciding common formats should be very important ( themes, for instance ) - it's very annoying to see very small letters and a radically different theme when using a KDE applications on Gnome environment, and vice-versa.\n\n Fanatics never admit they are fanatic; they only say what they believe, and close their ears and eyes when we say something not pleasant to them. That way their protected ideas will soon or later stagnat and die, while others learn from mistakes. That said, don't flame; flame won't help anyone. If you think you have a better ideas, tell them and listen and learn from others; if everyone do this, KDE, Gnome and everything else would be better.\n\nRegards;\n   pedroac."
    author: "Pedro Amaral Couto"
  - subject: "Re: KDE and Gnome usability"
    date: 2005-10-01
    body: "> I think it would be great if KDE, Gnome and other desktops, window managers, \n> or what ever, agreed with the same file formats and some general guidelines.\n\nThis is already happening.  Examples: \nhttp://www.newplanetsoftware.com/xdnd/\nhttp://www.freedesktop.org/wiki/Standards_2fdesktop_2dentry_2dspec\nhttp://www.freedesktop.org/wiki/Standards_2fsystemtray_2dspec\nhttp://www.ramendik.ru/docs/trashspec.html\n\nHere's a list of specs with varying degrees of acceptance:\nhttp://www.freedesktop.org/wiki/Standards\n\n\n> deciding common formats should be very important ( themes, for instance )\n\nFor themes this is hardly possible as themes are more than just a collection of colour settings and a few icons.  They use code that has to be compiled and that is dependent on the used toolkit (GTK+ or the GUI part of Qt, respectively).\n\n\n"
    author: "cm"
  - subject: "Re: KDE and Gnome usability"
    date: 2005-10-01
    body: "> Fanatics never admit they are fanatic\n\nIndeed, and as such I don't expect you to admit that you are a 'unify everything' fanatic."
    author: "ac"
---
As recently announced, an effort has been started for closer cooperation between the KDE and GNOME <a href="http://usability.kde.org/">usability</a> teams. The effort was announced in a message sent to the
<a href="https://listman.redhat.com/mailman/listinfo/open-hci/"> open-hci@freedesktop.org
</a> mailinglist that was created for this purpose.

<!--break-->
<p><a href="http://lists.kde.org/?l=kde-usability&m=104406681532701&w=2">Original announcement</a> by Aaron J. Seigo:</p>
<blockquote>
<p> Seth Nickell (GNOME Usability Project), Havoc Pennington (Free Desktop, GNOME), and JP Schnapper-Casteras (Free Desktop Accessibility Working Group) and myself have been discussing the possibility of co-locating the KDE and GNOME Human Interface Guides (HIGs). </p>
<p>The plan as discussed thus far is to have the two documents co-inhabit one XML
document. Within this document, each HIG will have its own sections as
appropriate and will remain available for separate viewing. The goal is to
have one URL (on <a href="http://www.FreeDesktop.org">www.FreeDesktop.org</a>) and one document for developers to go
to for KDE and GNOME Human Interface Guidelines.  We hope this site can
eventually house guidelines for multiple desktops and graphical toolkits.</p>

<p>The easier we can make it for developers to discover and follow such 
guidelines the better it will be for Open Source desktops in general. Since
KDE apps are often run on GNOME and vice versa, developers should be able to
easily reference the guidelines for all the desktops they expect their app to
be run on.</p>

<p>Having a shared document will also allow us to start looking at commonalities
between the documents and perhaps create common chapters or sections on basic
guidelines and lessons that are desktop and toolkit-independent (e.g.,
accessibility and internationalization tips, general usability principles).</p>

<p>It will take some work to merge the documents, create a web site, and raise
awareness about the site for developers and people working on other non-KDE
non-GNOME HIGs. If you wish to join us in these efforts, please subscribe to
the open-hci@freedesktop.org email list via the web interface at:</p>

  <a href="https://listman.redhat.com/mailman/listinfo/open-hci/">https://listman.redhat.com/mailman/listinfo/open-hci/</a></p>

<p>Best wishes to everyone!</p>
</blockquote>