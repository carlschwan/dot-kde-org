---
title: "Desktop Linux Conference: KDE Report"
date:    2003-11-12
authors:
  - "gstaikos"
slug:    desktop-linux-conference-kde-report
comments:
  - subject: "SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-11
    body: "Letter of the SUSE CEO to the KDE developers: <A href=\"http://lists.kde.org/?l=kde-core-devel&m=106855804831790&w=2\">kde-core-devel</a>"
    author: "Anonymous"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-11
    body: "Did you that Quote: \"E.g. why do we not open Linux for Apple's Mac OS desktop?\"\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-12
    body: "He has said that already in a previous interview (before Novel acquired SUSE) as long-term dream (5-6 years?). But do you really think that will happen?"
    author: "Anonymous"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-12
    body: "Hi,\n\nI don't think it would make any kind of sense. Not for Apple, not for Suse/Novell except one party could buy out the other. Can it now?\n\nBut then I question the seriosity of the support for KDE. I understand that Novell bought SUSE for Yast and Server not for KDE. \n\nYours, Kay"
    author: "Debian User"
  - subject: "What does that mean?"
    date: 2003-11-12
    body: "Does he mean he wants the Mac OS desktop ported to Linux? Porting Linux to Mac OS would certainly be silly."
    author: "Ian Monroe"
  - subject: "Re: What does that mean?required in bold"
    date: 2003-12-26
    body: "I'm trying to order some scrapbooking things,and it won't let me get passed the message,\"Required fields are in bold\" \nCould you please tell me what this means.                                                                                                              Thank you,  "
    author: "Dee"
  - subject: "Re: SUSE LINUX will continue to strongly support K"
    date: 2003-11-11
    body: "Sounds like:\nKDE is great and I love it. But the Ximian guys will\ntake care of the desktop stuff from now on.\nBad news for KDE users I fear..."
    author: "OI"
  - subject: "Re: SUSE LINUX will continue to strongly support K"
    date: 2003-11-12
    body: "I think you misinterprete something here. To me it reads differently - otherwise he wouldn't write \"Novell, we will ALSO enable our \ncustomers to use GNOME with the same convenience and comfort KDE offers to me \nand all SUSE employees today.\".\nThis pretty much means that GNOME will stay an alternative.\n\n"
    author: "anon"
  - subject: "LOL"
    date: 2003-11-12
    body: "He just basically said KDE is better than GNOME but they will try to make GNOME as good as KDE.\n\nI don't see how that will happen. if that happens it means that KDE would no longer be better than GNOME because right now anyway you package GNOME, like in Fedora or like in Ximian Desktop it isn't better than KDE. If oyu take an apple and put it in 50 different packages it will still be an apple and you can't make it an organge. by this its almost like hes suggesting to stop KDE development and wait for GNOME."
    author: "Mike"
  - subject: "Re: LOL"
    date: 2003-11-12
    body: "Something like that.\nAnd that's why that \"we will strongly support KDE\"\nseems pretty much like...bullshit."
    author: "OI"
  - subject: "Re: SUSE LINUX will continue to strongly support K"
    date: 2003-11-12
    body: "To me it sounds like that the current amount of \"strong\" support will be kept (but there will be no 40 Indian programmers hired to work on it additionally)."
    author: "Anonymous"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-12
    body: "Pls, don\u00b4t get this wrong, I bought SuSE to support KDE, as I thought they do the best for this wonderful project. But looking at the latest development, I\u00b4m not really sure, if they continue theire way... Doesn\u00b4t matter what they write in theire letters... Can anyone tell me, which distro is supporting KDE strongly with developers or even money, so I will swith to this distro;)?\nThx"
    author: "philipp"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-12
    body: "If you want to support KDE (and only KDE) I guess the best way is supporting KDE directly, see http://www.kde.org/support/ ;)"
    author: "Datschge"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-12
    body: "What really bugs me with this is that *there is no organization in the world*, big or small, that in the end of the day will keep two internally competing units. They just MUST streamline the operations, or otherwise they are stupid. Everything else said seems like a lie - but the big question is, which way to go. Obviously they don't know that either."
    author: "jmk"
  - subject: "Re: SUSE LINUX will continue to strongly support K"
    date: 2003-11-12
    body: "The beauty of Open Source is that the least they have to do is install a stock KDE which will be good. \n\nThe isuue is they may want an environment they can control better, and which they can give their customers without any hidden costs. There are hidden costs associated with Qt. Yes, their customers can use Qt Free, but it just means too much explaining to do. If they do that, I can understand where they are coming from, like I understand where Redhat is coming from when they prefer GTK to Qt. If their customers decide on Qt, its neither better nor worse for Redhat. They may have to make such a decision, and they may not have to if it makes financial sense to support the 2 environments."
    author: "Maynard"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2003-11-12
    body: "Personally I was very impressed by the mail by Richard Seibt to kde-core-devel. How many times did the CEO of such a big linux company post to a _developer_-mailinglist to assure his support? Obviously SuSE must have been contacted by a lot of customers to make that happen.\n\nAnd I think the content is pretty much positive as well: I worked for SuSE for 3 years as a KDE developer and I know that most people who work at R&D at SuSE like KDE - many of them even started to use Linux because of it. So while I have always been sure that KDE at SuSE is something the R&D department can't live without I haven't been completely sure in regard to their (current) management.\n\nTherefore I it's quite a relieve for me to see that in the first 30% of his mail SuSE's CEO characterizes KDE as the user chosen de facto standard in Europe and asserts that there would be no corporate/consumer desktop without KDE today. \nOf course he has got to take Gnome into that picture as well, so there's no surprise that he tells how Gnome will fit into the picture at SuSE in the future. It's no surprise that Ximian will take over this part. Interesting point is that he uses the word \"also\" for the Gnome support - so it's still targeted as an alternative and obviously not as a default.  \nIn the last third of the mail he says that SuSE is commited to deliver what the customer asks for. As he already mentioned in the first part customers - especially in Europe ask for KDE. So for the current foreseeable future they will very very probably stay with KDE.\n\nOf course nobody can guarantee that it will stay this way. But IMHO as long as KDE developers listen closely to their users we can make sure that SuSE's customers continue to ask for KDE as the default.\n\nI think it's pretty much telling that although Redhat defaults to Gnome (well actually in the current releases not so much anymore), KDE still enjoys such a high acceptance in the US - So many many people there are obviously not very much satisfied with Redhat's default and change it to something that makes more sense to them. Compare this just to SuSE in Europe where switching to Gnome would be easier to find out and easier to do: Still on the last LWCE in Frankfurt about 90% of all desktops showed KDE (the remaining ones mostly MS Windows). And this is pretty much consistent with my own experience with SuSE customers.    \n\nSo IMHO there isn't too much to worry about - just much work to do: We have to continue to make sure that KDE will stay the best desktop environment around and people Have a lot of fun with it :-)\n  "
    author: "Torsten Rahn"
  - subject: "Re: SUSE LINUX will continue to strongly support KDE"
    date: 2004-03-17
    body: "Yeah, right on. I am a first class Win-to-SuSe-mover, normally operating in the embedded market, and like this kind of support and attitude from vendors.\n\nBest regards\nMark \nThe Netherlands\n"
    author: "Mark Dijkman"
  - subject: "Does this mean?"
    date: 2003-11-11
    body: "\"Novell is really stressing the \"ISV support\" point. It's as though they're taking a lesson from Microsoft - give away inferior product to destroy the competition and then provide expensive peripheral applications and vendor lock-in. Ironic that they took direct shots at Red Hat for apparently trying to do this.\"\n\n+\n\n\"The real target for most businesses (IBM and others) is thin clients and hybrid configurations. Ironically KDE is best suited to this, especially with Kiosk mode (something people were demanding at the conference, and apparently NovellXimianSuse will provide a proprietary Ximian desktop solution for).\"\n\nSUSE will no longer back KDE in the long run? Slowly GNOME is replacing KDE in their distribution?\n\nI think this is a bad idea for them. They have invested quite a bit into KDE, their employees know KDE best and their customers buy SUSE for KDE, and SUSE has had a bad track record with GNOME.\n\nFurthermore, there is room for two desktops, both DKE and GNOME emphasize on different things and both are better at some things than the other. I thought that SUSE would continue to have KDE as the default, but would just work on the GNOME option to bring it to the same level. It sounds like, this is not what is happening and I am very dissapointed in SUSE for selling out like this if my fears are true.\n\nHaving GNOME become the single major force on the corporate desktop is bad for Linux and and it means less choice for consumers as well as reduced innovation. I fear for the future of SUSE, and KDE, but I am hoping that SUSE will put its faith in KDE as it has in the past and that they have done this to bring LINUX forward not just for the money."
    author: "Alex"
  - subject: "read the letter from Richard"
    date: 2003-11-11
    body: "If he truly means what he is saying and it is not just good pr for SUSE so that their customers wouldn't be upset, than I am happy. But, i have my doubt about the truth of what he is saying, surely you can understand, after all Novll has purchased Ximian and Ximian Desktop 2 has destroyed my KDE on both redhat and SUSE when I installed it. Nothing worked in it anymore. I do not like it that Ximian sababtoges KDE when you install it, this is the same kidn of thing Microsoft did to  Wordperfect."
    author: "Alex"
  - subject: "Re: Does this mean?"
    date: 2003-11-11
    body: "I'm not sure, honestly. Novell did announce that they are making Kiosk-like support for Evolution (and I believe via support in XD but I'm not entirely sure).  They said that the client would be free but the server would probably be proprietary.\n\nI only tell it like I heard it, and I won't make assumptions about what that means.\n\nDon't read all of this as much as \"Gnome\" but as \"Ximian Desktop\"."
    author: "George Staikos"
  - subject: "But, it is GNOME "
    date: 2003-11-12
    body: "No matter how I spin it, XD is still GNOME, just in a nicer package its kind of like Staroffice vs Openoffice."
    author: "Alex"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-11
    body: "> OpenOffice.org wants to work with us. We need to work with them.\n\nI'm looking forward to OpenOffice.org \"Q\" having a GUI toolkit abstraction.\n\n> Government regulated/approved accessibility is no longer optional\n\nThere much depends on Qt 4's accessibility support. Still some months away.\n\n> We need to be involved with the Desktop Linux Consortium.\n\nKDE is member of the Desktop Linux Consortium.\n\n> We need to continue our work with freedesktop.org. The X server enhancements\n\nI hear that already patches for this exists eg for ksnapshot taking screenshots with mouse pointer.\n\n> Let's get involved with Fedora.\n\nSounds like something for the \"KDE for Red Hat Linux\" project."
    author: "Anonymous"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-11
    body: "> > We need to be involved with the Desktop Linux Consortium.\n> KDE is member of the Desktop Linux Consortium.\n\n  Being involved and being a member are completely different things.\n\n\n"
    author: "George Staikos"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-11
    body: "Isn't Fedora the future for RedHat? The distro that had KDE in it just because it had to be included? Why would someone want to work with Fedora then? It's a US distro, ergo it's a Gnome distro. I'm stuck with a RedHat 9 at work, and the KDE there was just plain bad, I had to konstruct a newer and unbroken KDE just to be able to work comfortably."
    author: "Chakie"
  - subject: "Exactly"
    date: 2003-11-11
    body: "This is the reason we should work on Fedora, just to get their crippled KDE up to spec. So people like you won't need to konstruct a new KDE when they are forced to use it and so that people everywhere who use the distro won't think that the KDE shipped with Fedora is really as good as KDE gets. Redhat has a larg emarketshare, especially in the US, it is no coincidence that KDE's PR and acceptance is lower in US and that Redhat and GNOME's marketshare is higher in the US.\n\nWe can change that."
    author: "Alex"
  - subject: "Re: Exactly"
    date: 2003-11-11
    body: "It is already been worked on:<br>\n<a href=\"http://sourceforge.net/projects/kde-redhat/\">http://sourceforge.net/projects/kde-redhat/</a>\n\n"
    author: "Richard"
  - subject: "Re: Exactly"
    date: 2003-11-12
    body: "As I understand it, kde-redhat.sf.net is a part of the Fedora project, or at least closely linked with it. The kde-redhat page is full of references to Fedora."
    author: "A. Nonymous"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-11
    body: "Well, Redhat tried, and it didn't sell. They basically abandoned the whole thing, and spawned Fedora.\n\nGive them a chance to learn from their mistakes.\n\nWhat is interesting is that the successful desktop distributions use KDE.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: ">What is interesting is that the successful desktop distributions use KDE.\n \nYeah, I also find that very interesting. When I first started using Linux one year ago, I came in with no biases, and decided to see which I would choose between Gnome and KDE.\n\nInterestingly enough, I started with Gnome because out of the box I thought it looked better, but got frustrated with using it within a day or two, so I switched to KDE, and I never looked back from there. Six months later I removed Windows from my hard drive completely.\n\nKDE is the only way to go on the desktop"
    author: "lrandall"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "Just because a distribution is American doesn't mean it's a GNOME distribution.  I find that the KDE support in Gentoo, for example, is excellent.  I'm sure the same is true for distributions such as Debian.  As far as Fedora is concerned, the fact that it is the future of Red Hat is exactly the reason why KDE should work with Fedora.  If the Fedora Project is indicating that they're trying to drop their policy of crippling KDE, then I think KDE should give them all the help they need.  Remember, we need Fedora more than they need KDE if we're trying to gain mindshare and desktops."
    author: "Michael Pyne"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-13
    body: "--Remember, we need Fedora more than they need KDE if we're trying to gain mindshare and desktops.\n\nI disagree.  Mindshare -- yes.  To capture desktops they need KDE as much as KDE needs them!  "
    author: "Ross Baker"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-13
    body: "\"we need Fedora more than they need KDE if we're trying to gain mindshare and desktops\"\n\nWho or what is Fedora? =P\n\nSeriously though, Fedora, being a successor to Red Hat but barely publically mentioned by them at all, is not (yet? never?) in a position where it could claim to be helpful for any project simply due to the fact it's a very new brand only insiders (aka geeks) really know about. The fact that Fedora, in contrary to the former Red Hat Linux, won't be made available as boxed software to the more general public is further weakening the potential goal of bringing Linux desktop to the average joe."
    author: "Datschge"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "RH's version of KDE was indeed crappy, but Fedora != RedHat. "
    author: "fault"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "Still crappy IMO, lots of features they had on by default in GNOME were off in KDE. Sure, I could turn them on, but that defies the point. For example, an easy Samba browser, recent documents etc were default in GNOME in KDE I had to do it themselves, but these are just small issues there were things that pisse dme off more."
    author: "Mike"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "Fedora as of today is still 9x% RedHat - or where are the independent developers? And even then the \"steering committee\" deciding about if and how to include KDE consists of 100% RedHat employees."
    author: "Anonymous"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "The KDE support in Fedora is completely different from Red Hat's, really.\n\nSo, where are the independent developers? Doing the KDE packages."
    author: "Roberto Alsina"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "Oh really?  Have you installed Fedora?  What does the KDE look like? A stock distribution or still BlueCurve?\n\nI'd be very surprised if it wasn't in tune with Red Hat's offering...  I guess Fedora is pretty much a fork of Red Hat!"
    author: "Navindra Umanee"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "I have no idea how a stock fedora looks like.\n\nHowever, I've been using Fedora with the kde-redhat source in my apt since almost 6 months ago, and it is pretty much a stock KDE from CVS, without\nthe dependency mess, with many extras (PyKDE, yay! :-), and yes, with bluecurve as default.\n\nOr at least it was the default 6 months ago, IIRC."
    author: "Roberto Alsina"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-13
    body: "Fedora is slightly worse in terms of KDE support.  Instead of just using the session menu to choose KDE as your default, you now have to go back to the system menu - find the desktop switch option (under more system settings I believe) and then reboot).  Also, still all gnome speed buttons on the bottom taskbar under KDE.  And no k3b CD Writer etc. "
    author: "sage"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-13
    body: "That's not what I would call a slight issue.  That's what I would call a major step back."
    author: "anon"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "What is this KDE GNOME \"part of the world\" talk?\nPutting some kind of nationalistic stamp on a distro\nis really ridiculous.\n\nDon't you understand? Gnome is an _international_\ndesktop. KDE is an _international_ desktop.\n\nRidiculous."
    author: "OI"
  - subject: "Re: Desktop Linux Conference: KDE Report"
    date: 2003-11-12
    body: "Of course that's true and all, but for various reasons, KDE seems to be more popular in Europe ( http://www.kde-look.org/poll/index.php?poll=30 ) than in North America (because of RedHat), and vice versa (because of every other non-commercial distro)\n\nI'm an American who uses KDE :-)"
    author: "anon"
  - subject: "yes"
    date: 2003-11-11
    body: "I have to agree with pretty much everything there.\nEspecially on backing the freedesktop.org xserver.  Have you guys seen this:\nhttp://freedesktop.org/~keithp/screenshots/"
    author: "theorz"
  - subject: "Re: yes"
    date: 2003-11-11
    body: "Looks promising.\nTHX for the link."
    author: "Mike"
  - subject: "Re: yes"
    date: 2003-11-12
    body: "Does anyone know if there are any KDE developers working on supporting Keith's new  extensions?  I know that they may not be finished yet but it would be a great thing to see KDE support things like true alpha blending as soon as the capablities are in the X server."
    author: "kinema"
  - subject: "Re: yes"
    date: 2003-11-12
    body: "Fredrik Hoeglung is following this X server development very close."
    author: "Anonymous"
  - subject: "Re: WOW"
    date: 2003-11-12
    body: "This transperant/SVG overlays look gorgeous!\n\nI dream of ksvg doing all the icons like this eventually ;-)"
    author: "cies"
  - subject: "XPCOM plugins"
    date: 2003-11-11
    body: "So what was the problem with XPCOM plugins?"
    author: "David Gerard"
  - subject: "Re: XPCOM plugins"
    date: 2003-11-11
    body: "It's extremely complicated for other browsers to implement.  It would be like standardizing on KParts and having, ex Mozilla, implement all their plugins like that.  It's just not practical."
    author: "George Staikos"
  - subject: "KDE Needs A Corporate Champion"
    date: 2003-11-12
    body: "I think KDE really needs a big champion.  I would love IBM to get involved and go with KDE for their desktop platform of choice.  Alternatively, I think KDE needs to see the rise of a corporate champion that will take stock KDE and market it.  Xandros, Lycoris, Lindows all use KDE, but none of them really fit in with the KDE community.  We need a symbiotic relationship with a major corporate backer like GNOME has/had with Ximian/Novell that will push stock (or near stock) KDE exclusively.  No GNOME components.\n\nAlso, I understand that KOffice will be using the OpenOffice formats in an upcoming release.  Will we then be able to use the OpenOffice import/export filters for Word?  If so, then I think we should still ride the KOffice horse for all it is worth."
    author: "manyoso"
  - subject: "Re: KDE Needs A Corporate Champion"
    date: 2003-11-12
    body: "I think the report says IBM will be going with Novell/Ximian..."
    author: "anon"
  - subject: "Re: KDE Needs A Corporate Champion"
    date: 2003-11-13
    body: "No, it doesn't say that."
    author: "anon"
  - subject: "Re: KDE Needs A Corporate Champion"
    date: 2003-11-12
    body: "yes business needs MS office compatibility. IMHO a key succes factor."
    author: "ferdinand"
  - subject: "Re: KDE Needs A Corporate Champion"
    date: 2003-11-14
    body: "What's wrong with a small champion? kittyhooch.com supports Quanta Plus sponsoring Andras Mantia full time and makes a difference. Maybe if a few more KDE users would get their cats snockered with the best we could support KOffice too. ;-)\n\nWhile we're at it... the Linux kernel got big with a lot of small supporters and now is crucial to a number of businesses and enjoys many large supporters. Who's to say that when KDE is as old as Linux that won't be the same case? Remember it's the cummulative strength of small supporters that convinces large supporters they can't beat it so join it."
    author: "Eric Laffoon"
  - subject: "Xandros"
    date: 2003-11-12
    body: "Xandros uses KDE, but:\n\nXandros web browser: Mozilla\nXandros Mail client: Evolution\nXandros Office suite: OOo\n\nIf they're going for a simple, integrated desktop, I would think they'd want to use KDE apps as well...\n\nAlso, it sounds like they've added lots of neat technology like hardware detection, fast user switching, non-destructive NTFS partitioning, config gui's (for wifi network, dual-head display, etc)...are they sharing any of this with the community, or is it proprietary?  (not necessarily complaining either way, just curious)"
    author: "LMCBoy"
  - subject: "Speaking od XANDROS"
    date: 2003-11-13
    body: "Xandros 2 was just released and it sips with KDE 3.1.4, but I do wish they would ditch XFM, it is better than Konqueror for many taks but keeping it better is a huge taks considering how many people work on Konqueror."
    author: "Mike"
  - subject: "Maybe too late?"
    date: 2003-11-12
    body: "\"Our PR is worse in North America than I had originally imagined. Many people use KDE, few know what it is, and I suspect some think they're using other things even.\"\n\nIt's not KDE's PR! The only one of the big player (really) supporting KDE was/is SuSE. Everyone else was using GTK/Gnome. \n\n\"OpenOffice.org wants to work with us. We need to work with them. Let's get going folks!\"\n\nReally? Q's roadmap says something about Gnome integration!\n\n\"Government regulated/approved accessibility is no longer optional. If we don't do it and do it now, we might as well give up hope of seeing KDE accepted into government and corporate institutions.\"\n\nEveryone said KDE's accessibility features are way ahead of Gnome's. So how come this? Just curious...\n\n\"We need to come up with an alternative plugin scheme to Mozilla's XP based plugins that can be used across all Linux browsers, and perhaps on other platforms too. Browser plugins are a mess.\"\n\nThat's not specific to KDE, plugins are generally a mess under Linux\n\n\"The Linux Desktop, according to business types, is Linux+Desktop+Mozilla+OpenOffice. We need to integrate better with OpenOffice, and we need to better educate people about Konqueror.\"\n\nThere's Mozilla, Firebird and some other Browser that are really good. So why use konqueror? I also use KDE but it never came to my mind to use konqueror when having the above mentioned alternatives. Why trying people eduacate about konqueror when most if them even recognize Mozilla/Firebird (under Win)\n\n\"The real target for most businesses (IBM and others) is thin clients and hybrid configurations. Ironically KDE is best suited to this, especially with Kiosk mode (something people were demanding at the conference, and apparently NovellXimianSuse will provide a proprietary Ximian desktop solution for)\"\n\nEvolution may possibly be not better than kmail but it's way more \"business ready\"\n\n\"Businesses want a corporate partner to buy their free desktop from. Go figure.\"\n\nHmmm... I'm thinking of RedHat/Fedora, IBM, Sun an many more others. No one I know uses SuSE/KDE in their business\n\n\"We need to be involved with the Desktop Linux Consortium. We need help from everyone to speak about KDE, and additionally about Linux on the desktop in general.\"\n\nAlready on the way AFAIK!\n\n\"Let's get involved with Fedora. They want our support. There is mutual benefit to be had.\"\n\nWhy not a 2 years ago? RedHat pi**ed on KDE as we all know, so why didn't the KDE team built *working* packages for RedHat's distro?\n\n\"Linux on the desktop is a 4-6 million machine pie right now. That's 1-1.5% of the market. There's only one direction to go from here. We need to make sure that KDE remains competitive and retains our majority portion of that pie.\"\n\nBusiness ready? OK, stop implementing useless features, stop the bloatware, make KDE rock solid! Implement features needed in everydays business, not gimmicks for geeks! That's the way to succeed IMHO!\n\n\nMaybe you guessed it, I am a Gnome user, but until 3.1 I would have died for KDE. But now I miss some kind of professionality, ease of use. The main goal of KDE now seems to be \"more configuration options\" and not \"business orientation\".\n\n\n\nAnyway, KDE overall is a nice piece of work\n\nAnyway\n"
    author: "scsimodo"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: "> It's not KDE's PR! The only one of the big player (really) supporting KDE was/is SuSE. Everyone else was using GTK/Gnome. \n\nHmm.. really? SUSE is the major distro in Europe- they use KDE. Connectiva is the major distro in South America - they use KDE. Turbolinux is the major distro in Asia - they use KDE. I really don't know about Africa or Austrialia, but that leaves the other major continent: North America. RedHat is the major distro in North America, and they use GNOME. \n\nThankfully, Fedora is a community project, and they have seemed to be much more opened up about KDE than RedHat was. Notice that KDE support in other community-based distros is *very good* (Debian and Gentoo).. hell, you even have KDE developers maintaining the packages for these community distros (Ralf Nolden of kdevelop fame, for Debian, and Caleb Tennis, also of kdevelop fame, for Gentoo)"
    author: "anon"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: "Remember Lycoris, Lindows and Xandros all North American and all target the desktop unlike Redhat.  Guess what they run?  KDE..."
    author: "ac"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: "Don't forget Mandrake and Knoppix, which use KDE by default I think and are still rated #1 and #3 on distrowatch."
    author: "Dominic"
  - subject: "Re: Maybe too late?"
    date: 2003-11-14
    body: "RedFlag, the leader distro of China, uses only KDE as its desktop environment. It shipped more than 1 million desktop copies one year and occupied about 30% of Linux desktop market. "
    author: "Astronomy"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: "I only browsed through it, but something out of your stupid rant really stood out:\n\n\"Maybe you guessed it, I am a Gnome user, but until 3.1 I would have died for KDE. But now I miss some kind of professionality, ease of use. The main goal of KDE now seems to be \"more configuration options\" and not \"business orientation\".\"\n\nWhere the fuck did you get this from? All signs point the opposite way, have you paid any attention to KDE development since 3.1? They have been working hard to make it easier to sue, removed a lot of context menu bloat and many features aimed for businesses mostly.\n\nDon't just spit out the propaganda that the GNOME camp throws at its cluelss users. Think. Try. Say something smart."
    author: "Mike"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: "The easier to sue part is only for SCO in particular."
    author: "George Staikos"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: ":))"
    author: "anon"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: "> \"The Linux Desktop, according to business types, is Linux+Desktop+Mozilla\n> +OpenOffice. We need to integrate better with OpenOffice, and we need to\n> better educate people about Konqueror.\"\n \n> There's Mozilla, Firebird and some other Browser that are really good. So\n> why use konqueror? I also use KDE but it never came to my mind to use\n> konqueror when having the above mentioned alternatives. Why trying people\n> eduacate about konqueror when most if them even recognize Mozilla/Firebird\n> (under Win)\n \nWhy educate people about Konqueror?\nTo show them an alternative to Mozilla. If they still prefer Mozilla, fine. But they should at least know that there's an alternative which probably much better suits there needs.\n\nWhy use Konqueror?\nBecause it starts much faster than Mozilla. Because it renders faster than Mozilla. Because it's highly integrated with KDE. That's why a KDE user should use Konqueror.\n\nBTW, people who come from Windows don't recognize Mozilla/Firebird. They recognize IE.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Maybe too late?"
    date: 2003-11-14
    body: "It's a good idea to let people know that Konqueror exists.  It's a bad idea to make them feel like they need to use Konqueror if they're using KDE.\n\nIt's the nature of Linux for things to be mixed and matched and for a variety of configurations to exist.  That enables a healthy ecosystem.\n\nI prefer KDE as a desktop environment and use many of the included programs.  But there are also programs that I prefer using over the KDE equivalents, such as:\n* Mozilla\n* GVim\n* wvdial\n\nMy point is that this diversity is good and necessary.  We don't want to reduce ourselves to using an impersonal integrated, homogenous environment like Windows and Mac users do.  Ditto about standardizing a desktop environment."
    author: "Keith Bowes"
  - subject: "Re: Maybe too late?"
    date: 2003-11-14
    body: "What is wrong with KVim?"
    author: "Anonymous"
  - subject: "Re: Maybe too late?"
    date: 2003-11-14
    body: "I hope you know yourself that what you were writing there is pretty funny. The more audible audience nearly always complains about the lack of consistency and integration, ignoring the fact that with KDE they'd get a pretty complete desktop environment where consistency and integration is no issue at all. Instead they do make excessive use of diversity and then keep complaining about the lack of consistency and integration as if it isn't available or it's others' fault. *shrugs*"
    author: "Datschge"
  - subject: "Accessibility requirement [was Re: Maybe too late?"
    date: 2003-11-12
    body: "scsimodo said:\n\n> \"Government regulated/approved accessibility is no longer optional. If we don't >do it and do it now, we might as well give up hope of seeing KDE accepted into >government and corporate institutions.\"\n>\n>Everyone said KDE's accessibility features are way ahead of Gnome's. So how come >this? Just curious...\n\nNot sure who you've been talking to ;-)  Or perhaps you accidentally inverted the comparison above?  I would be interested in hearing more details about the accessibility requirements, i.e. who's saying it and why.  I do agree that this is becoming a very important requirement.\n\nKDE accessibility has come a long way in the past few months, but it mostly provides a few utilities that are helpful for certain limited disabilities.  That's a long way from true platform-level support for theming, low vision, and assistive technologies. ( http://developer.gnome.org/projects/gap/AT )\nTrue accessibility means that blind users, users who cannot use a keyboard at all, and users with other serious disabilities can use the complete desktop; KDE still doesn't have any support for this level of accessibility (though it's on the roadmap).  The plan for KDE accessibility's assistive technology support is to support and interoperate with the 'GNOME' accessibility APIs, but this KDE/Qt work hasn't begin yet.\n\nOn the other hand GNOME has been adding such support since its 2.0 release, and now includes bundled assistive technologies which work with an ever-increasing number of core GNOME applications.  This isn't meant as a flame in any way, it's just that GNOME started work on platform accessibility well in advance of the KDE accessibility effort.  However with concerted effort and focussed work I don't see why KDE can't leverage and reuse much of the GNOME accessibility work; there's really no reason IMO why an assistive technology needs to be tied to one plaform or the other, provided the same services and interfaces exist regardless of which 'desktop' the user is running or toolkit the developer is using.\n\nbest regards,\n\nBill \nArchitect and Technical Lead\nGNOME Accessibility Project"
    author: "Bill Haneman"
  - subject: "Re: Accessibility requirement [was Re: Maybe too late?"
    date: 2003-11-12
    body: "Sounds right.  I've done some digging around and ATK support is considered a MUST-HAVE for Qt4 by Trolltech and accessibility support for KDE4 is considered VITAL by some important KDE developers.\n\nBill, since you have a nice setup over at Sun Microsystems, would you guys be willing to help us test the code, etc?  I don't imagine it will be easy for our developers to do so without proper hardware support...  most of us just don't have those special resources.  \n\nWith Sun sponsorship, a big hurdle would be removed."
    author: "Navindra Umanee"
  - subject: "Re: Accessibility requirement [was Re: Maybe too l"
    date: 2003-11-14
    body: "actually we don't have a lot of hardware either, in our main development site for accessibility.  Fortunately it's not required, since the gnopernicus screen reader provides a \"braille monitor\" which is a software braille display, and everything else used by gnopernicus is software-based.\n\nThe hardware required for testing GOK is also fairly straightforward, in that most GOK features can easily be tested with a USB trackball or similar device.  It's worth having a head-pointing device (Tracker USB) which costs about $1000, but not essential to testing."
    author: "Bill Haneman"
  - subject: "Re: Accessibility requirement [was Re: Maybe too late?"
    date: 2003-11-12
    body: "> KDE accessibility has come a long way in the past few months, but it mostly provides a few utilities that are helpful for certain limited disabilities. That's a long way from true platform-level support for theming, low vision, and assistive technologies.\n\n\nWell said. There are many improvements in KDE 3.2, including improved support for people with low vision. But most things still need to be done, e.g. support for blind users.\n\n\n> This isn't meant as a flame in any way, it's just that GNOME started work on platform accessibility well in advance of the KDE accessibility effort.\n\n\nIndeed.\n\nBTW, Bill Haneman attended our first IRC meeting to help us getting started last year.\n\n\n> However with concerted effort and focussed work I don't see why KDE can't leverage and reuse much of the GNOME accessibility work; there's really no reason IMO why an assistive technology needs to be tied to one plaform or the other, provided the same services and interfaces exist regardless of which 'desktop' the user is running or toolkit the developer is using.\n\n\nThe current ATK implementation has several ties with GNOME, but this can easiliy be changed, as Bill and other involved people affirmed.\n\nOlaf (Co-maintainer of the KDE Accessibility Project)"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Maybe too late?"
    date: 2003-11-12
    body: "> Everyone said KDE's accessibility features are way ahead of Gnome's. So how come this? Just curious...\n\nMaybe you mean usability, not accessibility?\n\nThe KDE Accessibility Project is still very young - about a year old by now.\n\nWe soon released some small utilities, but the really crucial work is still far ahead. Fortunately, Trolltech promised to support ATK in the next version of Qt. GNOME's ATK is currently supported by Java and GTK2, so real interoperability is possible. And by reusing existing solutions, we hopefully can catch up quickly.\n\nOlaf (Co-maintainer of the KDE Accessibility Project)"
    author: "Olaf Jan Schmidt"
  - subject: "Open Office Integration"
    date: 2003-11-12
    body: "I am not sure why OpenOffice has to be integrated into KDE.  It seems that if OpenOffice supports GNOME it should automatically plug right in to the KDE desktop - at least this is what I understand the goals of FreeDesktop.org are.\n\nAm I wrong in this?"
    author: "Chris Parker"
  - subject: "Re: Open Office Integration"
    date: 2003-11-12
    body: "Yep.  Open Office does not support GNOME."
    author: "anon"
  - subject: "Re: Open Office Integration"
    date: 2003-11-14
    body: "Sun is pushing for better platform integration for OpenOffice in the roadmap. In Sun's case that means better integration with GNOME, but they are open for KDE support as well, that is, if someone is going to write the code for that.\n<p>\nCuckooo is an attempt to do just that, see <a href=\"http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/\">http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/</a> There was some talk about it becoming an OpenOffice incubator project but I'm not sure what the status is of that.\n<p>\nIf you are interested in helping out with this you may want to drop a <a href=\"http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/index.php?page=contact\">mail</a>.\n\n\n"
    author: "Waldo Bastian"
  - subject: "Love hearing about this"
    date: 2003-11-12
    body: "George, thanks a lot for your work. I think you're filling a gap right now, and you're doing it with an apparent enthusiasm that's truely inspiring. Rock on, KDE!"
    author: "Eike Hein"
  - subject: "Re: Love hearing about this"
    date: 2003-11-12
    body: "Yeah,\n\nGeorge Staikos is one of the people who wait a long time to start working on something, but then do it brilliantly. I will except kwintv, I am really not aware why that is so hard to do.\n\nThere are many great and probably not *very* time consuming things that get postponed to the \"next release\". I figure most KDE developers have jobs, and even though when their skills are applied tremendous results are produced, it also depends on their personal interest. And there are great features that wait a long time to complete, while they have taken shorter in other projects.\n\nStill, KDE has an extremely mature development. The hardcore base is never overlooked and that is what gives the overall sense of stability and cohesiveness. \n\nUnfortunately (or not so), those with a lot of time in their hands, choose GNOME."
    author: "kontakt"
  - subject: "Re: Love hearing about this"
    date: 2003-11-12
    body: "Thanks guys :)\n\nRegarding KWinTV, we had some parts working quite well, but it's quite a huge task and I have much more important things to deal with at the moment.  There are too many major areas of KDE that need work and KWinTV is not what I consider critical.  Also I haven't had a working TV tuner here in months so it's difficult to work on the rewrite. :-)  Dirk Ziegelmeier is working very hard on kwintv now and has joined maintenance and lead development.  There are only a few major bugs left and we can start to ship stable releases.  Unfortunately one of them is quite huge (lacking a usable V4L driver), so it will take some time to sort out."
    author: "George Staikos"
  - subject: "KDE Status "
    date: 2003-11-12
    body: "\"Businesses want a corporate partner...\"\nWell, yes, they want a stable product line that they can trust, won't disappear tomorrow, and someone to turn to when problems occur.  Now actually KDE provides a pretty stable product and a steady flow of updates which is good but there's not even a basic user's forum - Where can you post questions?  (and I don't mean developer's mailing lists) Who do you turn to when problems arise?  Some KDE newsgroup forums and FAQ's should be high on the list.  Hopefully the KDE team leaders can get some major corporate partners to sponsor and lend corporate credibility to KDE.\n\n\"Fedora..\" - There should be a packager for all KDE stuff for Fedora unless of course, all of us previous RH users keep switching to Debian, in which case make sure all packages get into Debian and get to Testing asap.\n\nWhere are the Developer Tools?  People seem to overlook the significance of MS putting a big emphasis on having excellent developer tools for Windows.  How does the corporate world move to KDE without great developer tools.  KDevelop3 has great promise but has been in alpha for a long, long time and still is not available for RH, Fedora, etc  What does leave for developers?  "
    author: "sage"
  - subject: "Re: KDE Status "
    date: 2003-11-12
    body: "> but there's not even a basic user's forum - Where can you post questions?\n<br><br>\n<a href=\"http://kde-forum.org\">http://kde-forum.org</a>, this shows that you never looked for it eg at http://kde.org\n<br><br>\n> \"Fedora..\" - There should be a packager for all KDE stuff for Fedora\n<br><br>\n<a href=\"http://kde-redhat.sourceforge.net\">http://kde-redhat.sourceforge.net</a>\n<br><br>\n> Where are the Developer Tools?\n<br><br>\nModules kdesdk and kdevelop\n\n"
    author: "Anonymous"
  - subject: "Re: KDE Status "
    date: 2003-11-12
    body: "just my 2 cent:\nthe admins in corporate networks *hate* \"a steady flow of updates\". in a reasonably large installation, there are just too many people who have their head full of stuff they call work, and juggling rpms is not notmally part of that scenario.\nwhen you want to sell, remeber how you want to buy...\n\nmats"
    author: "Mats"
  - subject: "Re: KDE Status "
    date: 2003-11-12
    body: "> Where can you post questions? (and I don't mean developer's mailing lists)\n<br><br>\n<a href=\"http://www.kde.org/mailinglists/#user\">http://www.kde.org/mailinglists/#user</a>\n\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: KDE Status "
    date: 2003-11-12
    body: "> Where can you post questions?<br>\n> Who do you turn to when problems arise?\n<br><br>\nSearch no more: <a href=\"http://www.kde-forum.org/\">http://www.kde-forum.org/</a>\n"
    author: "Eike Hein"
  - subject: "Re: KDE Status "
    date: 2003-11-12
    body: "Okay, I stand corrected - there is a rather extensive forum.  And it's my fault for not finding it BUT as a long time KDE user I didn't know about it and roughly judging from the number of posts (compared to many other linux  newsgroups), I'm apparently not the only one, soooo, what that means it might simply need to be better publicized etc.  Maybe we need to encourage more of us users to write up stories and how-tos about how we make use of the many features throughout the KDE project and get them published in popular sites such as user groups, distribution install how-tos, pclinuxonline, linux journal, linux forum (which does have many excellent how-tos such as GIMP) etc.  btw we are one of those unheard of US KDE users :)\n\nre steady stream - sure, admins want to minimize updates, but what I meant was that KDE is active, constantly improving etc - if there's a major problem, it's bound to be fixed shortly.  It's much better to choose when to update, than not to get updates.  In short, you can count on it for your business - and we do! - We depend on the desktop, Konqueror browser, KMail, etc every day and it has worked well (with the exception of KPilot which completely screwed up our data but I think that has gotten fixed in 3.2)  And I think the lastest KDE is pretty well supported in most of the Distro releases.  But if a sys admin has to go before the exec committee, they need to explain who is behind the product they want to use and what guarantee of support are they going to get.  \n"
    author: "sage"
  - subject: "Re: KDE Status "
    date: 2003-11-12
    body: "I didn't know about the forums until now, either.\n\nAnd I've been wishing for something like that."
    author: "TomL"
  - subject: "Good Points..."
    date: 2003-11-12
    body: "Could the progress made in these points maybe put on a seperate page like the 3.2 features and 3.2 status list, so it doesn't get forgotten next week. It would give a good overview of the direction KDE develops and how it succeeds."
    author: "furanku"
  - subject: "Who really needs Kiosk Mode"
    date: 2003-11-12
    body: "This is NOT a flame, please bear with me here. I need to know why Kiosk Mode would be important to businesses. I thought locking down a computer in business meant iinstalling only the required software, and making sure no other software can be installed. since a computer is usually assigned to one person, is ther really a need to lock down computers much further. PLease educate me? :)"
    author: "Maynard"
  - subject: "Re: Who really needs Kiosk Mode"
    date: 2003-11-12
    body: "hi\n\nvalid question. a particular company may want consistency across desktops.it might require everyone to have the same wallpaper for example and not waste time tweaking stuff\nregards\nrahul"
    author: "rahulsundaram"
  - subject: "Re: Who really needs Kiosk Mode"
    date: 2003-11-12
    body: "One simple example: You want people at a conference to be able to browse the web, but not to close Konqueror. Without the kiosk code this is not possible."
    author: "simple"
  - subject: "Re: Who really needs Kiosk Mode"
    date: 2003-11-12
    body: "Its to reduce service-effort.\nMost pwople are not as familiar to computers as you might be - if you allow them to tweak theyr desktop they might end up in a mess, helpless to fix their ill settings. thats a problem companys want to avoid."
    author: "Heiko"
  - subject: "Re: Who really needs Kiosk Mode"
    date: 2003-11-13
    body: "Kiosk mode is very important to two kinds of business use. The first and most obvious use is when a business makes a terminal available to the public as in a mall or an airport. In this case, the terminal's user needs to be restricted to do only the usually single function that the business wants to offer the public. It wouldn't do to have a kiosk that prints maps at a car rental agency reconfigured to get email and play frozen bubble by some twit with too much time on their hands, there by preventing the next person from printing their map.\n\nThe second business use is to control the user's desktop and reduce support costs. Companies spend thousands of dollars per year per user on support costs and many of the problems are self inflicted by the user. Countless hours are wasted explaining to users how to get back the icons that they lost (moved off the edge of the screen) or reconfiguring their themes so they no longer have black text on a black background making it impossible to read. \n\nThere are liability issues too. If you look at http://www.kde-look.org you will find a wide variety of themes and wallpapers that people enjoy on their desktop. But, many of those themes are inappropriate for business use. A company can be slapped with a sexual harrassment suit if you display some naked anime as your destop wallpaper, costing the company thousands or millions of dollars. \n\nThere are also issues with people installing their own software on company systems. This can cause all sorts of problems such as licensing, system instability, viruses/trojans/worms and also wasting company time. They aren't happy to pay you to play frozen bubble. It costs them money when you download some app off the web that clobbers a library and now the corporate app won't run and support spends half the day fixing it. Businesses and support departments greatly prefer to lock down the desktop so that the user can change nothing at all and can do only what their business function is with the computer.\n\nIndeed Kiosk mode is very important to business and managing it centrally for the entire enterprise is also critical. This is what Microsoft's Group Policy Objects does and also what Novell ZenWorks does plus a whole lot more. And, almost every company with more than 75 desktops is unwilling to run their enterprise without these features.\n"
    author: "Anonymous"
  - subject: "Gerd"
    date: 2003-11-13
    body: "Perhaps SOT Office may become a KDE OpenOffice as they didn't define their market nice. Waht makes this finnish package so special. I would like to use my KDe buttons in OO as well as the OO GUI looks outdated and broken. I would like to see OO as a Kpart...\n\nSame applies to Mozilla Composer, now NVU."
    author: "Andr\u00e9"
  - subject: "Importance of OO and Mozilla."
    date: 2003-11-14
    body: "Imagine you have a company. You want to install linux because you don't want to be forced to follow Ms decisions, you want to spend less in licenses and try to minimice virus-related troubles. \nYou have legacy hardware which does not have a linux proper driver, and legacy software, difficult or impossible to run on Wine. \nSo, you decide to make a partial migration:\n- Some servers, work on linux. \n- Most desktops, migrate to Mozilla and Open Office on Windows.\n- Work on legacy apps to do them multiplataform, replace them or make them run on linux by some kind of emulation. Same thing on legacy hardware.\n\nSo you have a mixed environment, and open source multiplataform apps are key. In conclusion, doesn't matter if KOffice-Konqueror is better in Linux, the important thing is that they are not as good on Windows.\nThat's why integration and work with them is important: they (Mozilla+OO) are the door througth which users came to Linux + KDE on desktop.\n\n"
    author: "Andrea"
  - subject: "Re: Importance of OO and Mozilla."
    date: 2003-11-14
    body: "No, it is more a \"let's try\" thing. If it works, fine. If not, you will immidiately switch back,\n\n- usability\n\n- stability\n\n- key applications for a good first impression (birthday card printer :-)\n\n- plattform unification\n\n- \"stable plattform\", rpm for all distros. You can run a windows programm from 95 on WinXP, but you shouldn't install a suse7.3 rpm under suse 8.2. And there is one package for each distribution. this may make sense for system software but not for applications."
    author: "Gerd"
  - subject: "> Let's get involved with Fedora."
    date: 2003-11-15
    body: "It appears to me that the question is whether the Fedora project wants to support KDE.\n\nThe current Fedora disto is basically just a retread of RedHat 9.0 with some updates.\n\nIf they are going to work with us, they are going to have to be willing to have a stock KDE distro rather than the crippled one the RedHat was using.\n\nThe RPMs on the Fedora site are new and they look like a proper set of KDE RPMs but I don't know what is in them -- at least they aren't sliced and diced like the ones for RedHat 8.0.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "UI abstraction"
    date: 2003-11-15
    body: "<a href=\"http://ui.openoffice.org/\">http://ui.openoffice.org/</a>\n\n"
    author: "Heini"
  - subject: "IBMs Desktop strategy"
    date: 2003-11-16
    body: "Just found this (don't know, if anyone else post this)\nIBMs Desktop strategy:\nhttp://www.desktoplinux.com/files/article003/sld018.html\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
---
On Monday, thanks to the amazing help of <a href="http://linuxtoday.com/search.php3?author=Jill:Ratkevic">Jill Ratkevic</a>, <a href="http://perens.com/">Bruce Perens</a> and <a href="http://www.xandros.com/">Xandros</a>, I was able to attend the <a href="http://butrain.bu.edu/itp/desklinux.asp">Desktop Linux Conference</a> to represent and <a href="http://www.staikos.net/information.php">speak about</a> <a href="http://www.kde.org/">KDE</a>.  I followed a talk by Xandros, another KDE based product, and their demonstration was an impressive example of how KDE is being used to do great things.  My talk focussed on what KDE is, what it provides, and where we're going with 3.2.  It's standard for KDE developers, but important for those who are looking to move to Linux on the desktop.  It was a long day!
<!--break-->
<p>
There was an excellent turnout for the event, and the facilities were ideal.  The talk schedule did change quite a bit, both in speakers and in timing.  My talk was scheduled for 3:15 but didn't start until approximately 4:00.  At that time everyone, including myself, was tired but we made the best of things. 
</p>
<p>
I spent much of the day listening to other talks and talking to an uncountable number of people about Linux and KDE.  I could write forever but it would bore you and take too much of my time, so here are some key points I learned:
</p>
<ul>
<li>We <b>still</b> haven't educated people enough regarding <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php">FreeQt</a>.  People who should be well educated about these things were still trying to tell me that if Trolltech is acquired, KDE would end up being a proprietary platform.</li>
<li>Our PR is worse in North America than I had originally imagined. Many people use KDE, few know what it is, and I suspect some think they're using other things even.</li>
<li><A href="http://www.openoffice.org/">OpenOffice.org</a> wants to work with us.  We need to work with them.  Let's get going folks!</li>
<li>Government regulated/approved <a href="http://accessibility.kde.org/">accessibility</a> is no longer optional.  If we don't do it and do it now, we might as well give up hope of seeing KDE accepted into government and corporate institutions.</li>
<li>We need to come up with an alternative plugin scheme to Mozilla's XP based plugins that can be used across all Linux browsers, and perhaps on other platforms too.  Browser plugins are a mess.</li>
<li>The Linux Desktop, according to business types, is Linux+Desktop+Mozilla+OpenOffice.  We need to integrate better with OpenOffice, and we need to better educate people about Konqueror.</li>
<li>The real target for most businesses (IBM and others) is thin clients and hybrid configurations.  Ironically KDE is best suited to this, especially with <a hef="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/README.kiosk?rev=1.18.2.11&content-type=text/x-cvsweb-markup">Kiosk mode</a> (something people were demanding at the conference, and apparently NovellXimianSuse will provide a proprietary Ximian desktop solution for).</li>
<li>Businesses want a corporate partner to buy their free desktop from.  Go figure.</li>
<li>Novell is really stressing the "ISV support" point.  It's as though they're taking a lesson from Microsoft - give away inferior product to destroy the competition and then provide expensive peripheral applications and vendor lock-in. Ironic that they took direct shots at Red Hat for apparently trying to do this.</li>
<li>We need to be involved with the Desktop Linux Consortium.  We need help from everyone to speak about KDE, and additionally about Linux on the desktop in general.</li>
<li>We need to continue our work with <a href="http://www.freedesktop.org/">freedesktop.org</a>.  The X server enhancements coming from there will be very good too.</li>
<li>Let's get involved with <a href="http://fedora.redhat.com/">Fedora</a>.  They want our support.  There is mutual benefit to be had.</li>
<li>Linux on the desktop is a 4-6 million machine pie right now.  That's 1-1.5% of the market.  There's only one direction to go from here.  We need to make sure that KDE remains competitive and retains our majority portion of that pie.</li>
<li>There are some great case studies out there, and we need more of them.</li>
</ul>
<p>
This is a summary of what I learned and saw at the conference.  Of course it's not a complete picture but I think it illustrates what the direction is for Linux on the desktop, what we need to do, and what's possible in the future.
</p>