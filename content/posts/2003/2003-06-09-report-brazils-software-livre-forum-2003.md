---
title: "Report from Brazil's Software Livre Forum 2003"
date:    2003-06-09
authors:
  - "numanee"
slug:    report-brazils-software-livre-forum-2003
comments:
  - subject: "IMO"
    date: 2003-06-09
    body: "As someone who hopes to see KDE spread, this is exactly what I like to hear - you did an excellent job of \"correctly\" presenting quality software.  I.E., you let KDE stand on its merits.  \n\nCongradulations, and thanks for taking the high road.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: IMO"
    date: 2003-06-10
    body: "yes, i must say i agree that the best way to gain ppl's support is not to go and bash gnome/windows etc (although i do do that a fair amount =) but to just show them the superiority of kde, and let them see it for themself. well done, it seems that you did a really great job. long live kde"
    author: "lrandall"
  - subject: "ANTI-KDE"
    date: 2003-06-10
    body: "I don't understand this Anti-KDe attitude. What's wrong with KDE? You don't have to use it. IceWM serves as well .-) gnome also works although I don't like to use it.\n\nPresent your KDE and show what you can do with it... nice idea. \n\nGideon and Quanta may be too special software."
    author: "Gerd"
  - subject: "Re: ANTI-KDE"
    date: 2003-06-10
    body: "There's no Anti-KDE attitude..\nThere's the problem of GNU letters..\nRichard Stalmman delays too much to give their bless to KDE, and all damage to deployment was done before.\nSo, they know KDE, but at this time they prefer and use Gnome. The efforts will be duplicated to achieve the same trust.\n"
    author: "Helio Chissini de Castro"
  - subject: "Miguel de Icaza"
    date: 2003-06-10
    body: "Is he really that much of a prick?\nAlmost makes me reluctant to check out Gnome anymore.\n\n"
    author: "zank"
  - subject: "Re: Miguel de Icaza"
    date: 2003-06-10
    body: "I'm sure Miguel is a nice guy and all, but he did pretty much betray KDE. Back in 1996, he was almost fanatic about KDE. In the comp.os.* newsgroups back then, he said a lot of positive things about KDE and backed it wholeheartedly (remember that he was the developer of mc, and as a result, he was already well known among free software circles. ) After learning about then-license of Qt, he completely switched views, said things that were in DIRECT contradiction of what he said months before that about KDE. He literaly went from being one of KDE's biggest supporters to one of it's biggest enemies overnight. Later on, he talked about making a desktop that was based on the Scheme (!!) programming language. This eventually became GNOME. \n\nI think he's cooled down over the years, but there is probably still some bad blood between him and KDE project as a whole. I dunno. I pretty much took a break in free software cold turkey in the KDE vs. GNOME \"argument\"'s formative years of 1999-2001, so I have no idea."
    author: "fled"
  - subject: "Re: Miguel de Icaza"
    date: 2003-06-10
    body: "Maybe not.\nBut at this conference they gain a status of a star, so when you are a star, you can act like a rock star from 80's or a humble star as John \"MadDog\" Hall ( which i hear from friends that he  playing Santa Claus to a little girl in airport ).\nThe moment do the person.\nI can speak some good things about Miguel. They really KNOWS how to sell  their product and actually IS A GREAT speeches man. It's a great thing. evry geek knows how dificult is speak to the masses ( above 50o guys ).\nBut, despites Miguel itself, you must use GNOME sometimes as well..\nWe never know if teh green color is beatifull before know blue or red color.\nSo, you really can say what's better if you really know the other side"
    author: "Helio Chissini de Castro"
  - subject: "Re: Miguel de Icaza"
    date: 2003-06-10
    body: "The short reply is NO. As far as i know him, he's as far of being a megalomaniac god as Maddog is. Or the guy writing the article has never had the opportunity to share a region of the space with RMS. I don't use neither Gnome nor KDE, but my wife and my eldest son use KDE and find it comfortable, albeit somewhat clumsy. I have seen the new version of Gnome and discussed it with Miguel from an agnostic's standpoint, and think it's worth a try."
    author: "Cinabrium"
  - subject: "Re: Miguel de Icaza"
    date: 2003-06-10
    body: "Wha?  I can only let Miguel's reputation speak for itself, but Maddog?  Him I do know.  He's an endearing old Unix geek.  :-)  Heck, he's bordering on gooby at times.  He's kind of the Grandpa to the Linux community.  And yes, the Santa Claus stories are true.  :-)"
    author: "Scott Wheeler"
  - subject: "We had KDE here last year"
    date: 2003-06-10
    body: "In last year's Free Software Forum, KDE's Ralf Noldem was here in Porto Alegre, and all machines had KDE running by default. To me it was apparent that GNOME was chosen this year because Miguel de Icaza was here.\n\nSpeaking of Miguel de Icaza, I talked briefly to him and he seemed to be a nice guy. I also took the oportunity to hand him a GoboLinux CD :)\n\n--\nHisham Muhammad\nhttp://gobolinux.org"
    author: "Hisham Muhammad"
  - subject: "Re: We had KDE here last year"
    date: 2003-06-10
    body: "A have to agree at some point with you, but we're talking not just about the conference, but about all Brazilian Government choices.\nPolitics is a bit more delicated than just installed desktops.\n"
    author: "Helio Chissini de Castro"
  - subject: "Nice"
    date: 2003-06-10
    body: "I like this kind of stuff =) hope to see more of it in the future =) \n\nIt's odd that Miguel would be arrogant to Helio, ater all he said KDE was bette rin some areas than GNOME.\n\nI do think that Helio should of gotten a good 1 GHZ+ 512 DDR laptop, 300 MGZ just isnt a good demonstration machine.\n\nAlso, i've seen Thiago check lots of my bug reports just minutes after I submit them, hes really fast and a great help to the KDE community as I'm sure Helio is too.\n\nWhat bothers me a little though is that I don't think really have a KDE god, a leader that is synnonymous with KDE like Miguel and havoc are with GNOME. Matthias or Aaron maybe I don't really know, there need to be more developer interviews to make KDE devs really popular so everyone knows and doesen't think KDE has no direction.\n\n"
    author: "Alex"
  - subject: "Re: Nice"
    date: 2003-06-10
    body: ">What bothers me a little though is that I don't think really have a KDE god,\n\nFor me, I find it reassuring. \"Gods\" are easier to dislike or attack,\nor FUD about. Not so long ago, everyone advocating Linux heard at\none point a question like \"what happens if Linus is run over by a bus, or\njust gets bored of Linux?\" from the skeptics.\n\n>I do think that Helio should of gotten a good 1 GHZ+ 512 DDR laptop, 300 MGZ\n> just isnt a good demonstration machine.\n\nRemember, he made a point of proving KDE works also on older machines.\n(Helio' demo machine is actually twice as fast as what I have at home,\nwhich does run KDE, but a bit slowly. I really wish KDE gurus could optimize\nthings a bit.)\n"
    author: "eru"
  - subject: "Re: Nice"
    date: 2003-06-10
    body: "WOW I can't imagine running KDE on something that slow, I thought 400 MGZ was the minimum.\n\nAnyway, i thought since he was teh \"demo dude\" that he would get a blazing fast machine to show KDE at its best."
    author: "Alex"
  - subject: "Re: Nice"
    date: 2003-06-15
    body: "I run KDE 3.1.2 on a pentium 233 MMX with 64 RAM.\nIf I had more ram it would run fine, it dosen't need that much processing power.\nThat's why I can't understand people buying machines with high processors and less than 512 ram... makes no sense!"
    author: "protoman"
  - subject: "Re: Nice"
    date: 2003-06-10
    body: ">>What bothers me a little though is that I don't think really have a KDE god, a leader that is synnonymous with KDE like Miguel and havoc are with GNOME.<<\n\nHmm.. and what happens to Gnome when Ximian runs out of money, or the god decides to spent the rest of his live with Mono, or Apple hires him?\n\n"
    author: "AC"
  - subject: "Re: Nice"
    date: 2003-06-10
    body: "Ximian does not do the lion's share of GNOME development, so whilst they would be sorely missed if they went out of business, it wouldn't destroy GNOME. Not by a long shot. Additionally, Miguel *is* spending his life with Mono. He is no longer involved in GNOME at a technical level at all. :-)"
    author: "jdub"
  - subject: "Re: Nice"
    date: 2003-06-10
    body: "Shouldn't you release the overdue next GNOME snapshot and update Garnome instead of reading the dot? :-)"
    author: "Anonymous"
  - subject: "Re: Nice"
    date: 2003-06-10
    body: "> What bothers me a little though is that I don't think really have a KDE god, a \n> leader that is synnonymous with KDE like Miguel and havoc are with GNOME.\n\nMaybe not, but we are still supporting David Faure for president."
    author: "Sergio Garcia"
  - subject: "KDE, why not?"
    date: 2003-06-10
    body: "Well, I am a brazilian member of the Debian project (we are few here, still), and also a indirect member of the government effort for free software.  Not all of us are GNOME heads, rest assured. Most of us will just go with whatever works.\n\nWe have been deploying KDE as the default choice in Campinas/SP/Brazil in the municipal government.  Whe have been working hard to get knoopix/kurumin widespread, and that's KDE as well...  and we are doing that because KDE is marginally better than GNOME right now. Technical merits only, you see. And the l18n quality rates very high in importance.\n\nBut if KDE wants to get Brazil, it needs a very damn good team for pt_BR l18n. So, I can only hope you get more Brazilians into KDE.\n\nOne suggestion I make is for the pt_BR KDE devels to approach one of the public institutions, through CIPSGA (www.cipsga.org.br), to try to get funding for the betterment of KDE's l18n.  \n\nThis is NOT impossible. Some of us know the value of really good l18n people (please apply the elitist and bastardly hight quality requirements you think Debian people have to this definition, btw), and are in management positions, and we will try to get funding for you, if you show up at our door.\n\nDirect email to me won't get you a job, btw. But do try to contact CIPSGA, and send in your resum\u00e9 and a sample of your l18n work."
    author: "Henrique M. Holschuh"
  - subject: "Re: KDE, why not?"
    date: 2003-06-12
    body: "Excuse me, but the Brazilian i18n is one of the top ranked i18n teams at this point. This is remarkable when you realize most of the job is being done by just a couple of _very_ dedicated people. The head of the translation team in Brazil has been doing this job in _very_ unconfortable hours due to her demanding work load. If you want to help, please contact Lisiane Sztotlz (lisiane@conectiva.com.br).\n\nThey _do_ need help, though. Especially as documentation is concerned. A lot of people comes in to do message translation, but the docs are mainly untranslated."
    author: "Roberto Teixeira"
  - subject: "Wow."
    date: 2003-06-13
    body: "I remember talking to you, but maybe you did not understand what I said.  Because I can not picture myself saying `Mono is seven times better than .NET'.  That is the first hint that you might have missed more than a few packets when we talked.\n\nIn fact, I *might* have said \"Mono is on average 70% of the speed of .NET today\", but claiming that Mono is better than .NET today would be a colossal stretch of the word \"seven\".  More like \"point seven\" ;-)\n\nI do not remember the details, but I think you might have been the dude that showed me a screen and said something like `Isnt this better than Gnome', and I said `I dont think so, Gnome is a lot more usable'.   So your representation of what I said if fairly inaccurrate.  \n\nI also dont think this is about being a \"god\" or not, but I do remember that you were slightly antagonistic towards me (if you are that dude from the VIP room).  I said at some point `The important thing is that we both work on free software'.  You might have been the Quanta dude, I dont know.  I dont remember.  It was an intense week.\n\nIf you get a trasncript of my talk, you might realize that I probably spoke about different things than you thought ;-)\n\nMiguel"
    author: "Miguel de Icaza"
  - subject: "possible misunderstanding"
    date: 2003-06-13
    body: "I'm curious: At the convention, did you guys speak together in English, Portuguese, or Spanish?  And based on that, could there maybe have been a little bit of a language barrier?  Mistranslation is always a good source of misquotes."
    author: "Ben Stevens"
  - subject: "Re: possible misunderstanding"
    date: 2003-06-13
    body: "It is very possible that this was the case, because I honestly had nothing to wine from being mean to people.\n\nI do not remember, but most people I met did not speak english, so I typically talked in slow spanish, and they talked back to me in portuguese, portuguese with spanish or english.\n\nI have to say, I loved the conference.  It was a very community-oriented conference of *users*, so unlike a trade show, everyone in the conference behaved like they were old friends.  \n\nIn one hand: the conference was very large (I do not know the numbers), but they had multiple tracks, and the place was huge, and everywhere you went, it was crowded.   And they had a section for community sites, that was fascinating: local groups sharing code, sharing ideas, sharing jokes.\n\nPart of what made this conference awesome was that Marcelo (the organizer) has tried to make not only a conference about free software, but has worked to get other social movements involved, so there was a strong presence from other activist groups that might benefit from free software.  \n\nSo unlike other conferences, I would say that the \"spirirt\" of the World Social Forum (which was held for three years in Porto Alegre) was present in the conference.  Ideas revolving not only around technology, but around social change, and the motto of \"Another world is possible\" being transpired everywhere.\n\nThe kind of energy I witnessed here is hard to find in trade shows today;   The LinuxExpo in Paris and the HispaLinux congress in Madrid are probably the closest to the community feeling they got here.  But as I said, the organizers had a people-networking mindset, which was absolutely awesome.\n\nLove,\nMiguel."
    author: "Miguel de Icaza"
---
<a href="mailto:helio@conectiva.com.br">Helio Chissini de Castro</a> of <a href="http://www.conectiva.com.br/">Conectiva</a> recently gave a KDE speech at <a
href="http://www.softwarelivre.org/forum2003/">Rio Grande do Sul in Brazil</a> and was kind enough to write up his experiences for the rest of us.  In recent years, Rio Grande do Sul has become a GNU and consequently GNOME stronghold, so prepare yourself for an entertaining if not graphic read!  <b>Update: 06/13 20:20</b> by <a href="mailto:navindra@kde.org">N</a>:  Only fair to point out <a href="http://dot.kde.org/1055177326/1055480495/">Miguel's comments</a> on this article.
<!--break-->
<p><hr>
<h2>Report: Forum Software Livre 2003</h2>
<i>by <a href="mailto:helio@conectiva.com.br">Helio Chissini de Castro</a></i> 
<p>
I'm just back from my arduous journey (closed airports, endless
delays, no food, and so on...) to the <a
href="http://www.softwarelivre.org/forum2003/">Open Software
Conference</a> at Rio Grande do Sul, where I did a speech on the
Corporate Desktop and KDE.  
<p>
First, a little explanation on what this conference represents to
Brazil itself.  Since the collapse of Comdex Brazil and Fenasoft, this
conference has become the major computing conference in Brazil, and in
the last two years has gained strong political backing, ever since
Brazilian government made a serious turn towards Open Source Software.
The conference happens every year in Porto Alegre, Rio Grande do Sul,
a strong GNU/Linux nest, and extremely GNOME-centric...  Just so that
you know what I am up against, let me just mention that the guy who
leads the local Rio Grande government software project and now
promoted to spearhead the government's open source effort for the
whole of Brazil, Mr Marcelo Branco, is a "Just GNOME" kind of guy.
<p>
So, after 4 hours of delay in my flight, I arrive at Porto Alegre and
barely have time to grab a bite before hauling off to the
conference. 
<p>
Today, the last day, is considered "GNOME Day" since Mr Icaza is
scheduled to do two speeches in the main room (seating 600).  The
first speech started just an hour before I arrived.  Not a single seat
is empty.  Miguel de Icaza is kind of a mythical God here.  He really
is a star to this people.
<p>
Once I obtain my ID card, I head off to the VIP room accessible to all
the speech guys.  I turn on my notebook, and start to read my mail...
and to my surprise Mr Icaza sits beside me and does the same thing. I
try, I really do try to exchange a few courteous words with him, but
you must understand how hard it is speak to a God, especially when the
guy <i>really</i> acts as one...
<p>
For a few moments I'm feeling a bit unpleased with everything, despite
the good treatment given to me by the staff, but after the harsh talk
with Miguel ("KDE is not interesting", "GNOME will win in the next
iteration", "Mono is seven times better than .NET", yada, yada, yada),
it is time to confront the real treat -- government guy Mr Marcelo
Branco.  Although I barely chat with him, I begin to realise that to
push our beloved KDE in, I will need to take a different
approach... on hand demonstrations.
<p>
I am equipped with a small and old notebook, a P2-300 Mhz with 64Mb,
with a brand new <A href="http://www.conectiva.com.br/">Conectiva
Linux</a> installed, if only to prove to everyone that is indeed
possible run our distro, and most of all, KDE on a simple machine.  To
show off new features, I also packaged material from CVS HEAD
4/06/2003, including <a href="http://www.kdevelop.org/">Gideon</a> and
<a href="http://quanta.sf.net/">Quanta</a>.
<p>
When I arrive at my speech room, imagine my surprise that almost all
of the 100 seats are taken, and only a handful leave during the speech
itself.  Feeling the pressure of responsibility, I hope for the best.
One thing is for sure, with the many rabid GNOME supporters in the
audience, I have to tread carefully by just demonstrating KDE itself,
and avoiding any direct comparisons.  Thank God, everything goes all
right, with nothing crashing during the hour long speech -- despite
some slowness due to lack of memory.  Surprisely, I even get some of
the crowd to participate in the speech.
<p>
I take care to mention which steps we (Brazil) will need to take to
deploy KDE in our government both at the beginning and the end of my
speech.  At the beginning when introducing myself, I also note that
there are only three Brazilian KDE developers (myself, Roberto and
Thiago), and mention that we have a lot of difficult reaching the
developers here for help. It seems I really touch some guys at that
moment, but the "piece de resistance" comes at the end of the question
period.
<p>
A corporate guy at the speech asks me this question: "The government
has stated at this conference that 2004 will be the year of Open
Source Software in Brazil. I want to know if KDE will be there at this
time."  This was the cue that I was waiting for.  My response is
frank and direct: We are three and some translators, we want to be
there but we really need help.  We are willing to make the effort to
walk with this moment, but if we don't have more local help, it will
be difficult...  
<p>
Afterwards, many people came to me to ask more direct questions about
my speech, but I was especially pleased to overhear a lot of people
talking positively about my speech across the conference showroom
floors.  I believe that we may have gained at least one or two new
developers.  My satisfaction will be complete when the news of KDE
reaches the Government guy's ears...  And all this accomplished
without even using any of the usual anti-Windows/GNOME tactics that
some people resort to!

