---
title: "KDE-CVS-Digest for January 17, 2003"
date:    2003-01-17
authors:
  - "dkite"
slug:    kde-cvs-digest-january-17-2003
comments:
  - subject: "Hoping for 3.1 soon."
    date: 2003-01-17
    body: "\"\"\"It has been a very busy week. 3.1 will probably be released at the end of the week. Very likely the announcement will come hours after this digest is published.\"\"\"\n\nHopefully this is true. Is there any reason it was delayed for so long after the completion of the security audit?"
    author: "Bob"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "Ups, another RC?\n\nftp://master.kde.org/pub/kde/unstable/"
    author: "Adrian"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "no, first: RC6.1.1\nafter which: RC6.1.2\n\nthen the real release canidates will go into pre alpha beta states,\nwhere the numbering is: RCpab7.2 (where 'pab' is pre alpha beta)\n\nthen the final release canidates (7) and then KDE 3.1\n"
    author: "IR"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "The RC7-directory has been removed again. I don't know if this is a good or bad sign :-)\n\nKind regards,\nPeter"
    author: "Peter"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "these guys are messing with our heads!"
    author: "IR"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "k . wierd .. i just checked and it is there.\nJust can't get read access cause its been turned off.\n\ni guess its wait and see day.\n\nlol"
    author: "mike"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "RC7 has been there for a few days now... I never noticed it dissappear."
    author: "Vic"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "The KDE-usability team has initiated an experiment to figure out how many Release Canidates are tolerated before users start to group and voluntary invade KDE-base."
    author: "IR"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "Well if they'll count to ten I'll start compiling myself."
    author: "Adrian"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-18
    body: "IR, your posts are so funny :)"
    author: "JC"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "Just after the security audit, RC6 has been released.\nLook at :\nhttp://developer.kde.org/development-versions/kde-3.1-release-plan.html\n\nKde team is right in time."
    author: "JC"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "Right on time? 3.1's been frozen for non-trivial patches since September. What hideous changes took place which caused the release to be delayed so long?\n\nI don't see any information anywhere giving the reasons for rejecting each successive release candidate. Is there any, or is it all done on the whim of the release coordinator? If so, what guarantee is there that there will not be 15 more release candidates, leading to 3.1 coming out after 3.2?"
    author: "Bob"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "um... it's freaking open source software Bob.  write your own 3.1 or let the experts do it.  hideous changes to halt the release?  i'm sure each rc has been documented somewhere as to why it didn't go final, but i don't care to really look. i'm waiting for 3.1 as much as anybody else, even installed rc6 recently on my gentoo box.  \n\nbob indeed."
    author: " expresso"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: ">giving the reasons for rejecting\n\nLook in the bug database.\n\nThere have been a huge number of critical bugs fixed in the last two weeks. One I reported was fixed two days ago, which caused a crash. In RC5, konq crashed once and a while for me, rc6 is fine.\n\nRemember, rc3 was to be final, rc4 was a release. Then the security audit, which produced rc5, rc6 was to be final, which I think may be the case. (plus a few last minute fixes).\n\nIn other words, all would have been well without the security problems.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "I can also tell you it's a matter of compilation issues.  For example, once the packages are made if one of the \"packagers\" gets a compile error then there may be some delay in fixing it and the packages are remade."
    author: "Caleb Tennis"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "On time?  The final was promised for three days ago!\n"
    author: "Sam"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "No, It was not. It was clearly stated that three days ago was a day to package and decide whether this is going to be final, and release a few days later if so."
    author: "radix"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-18
    body: "no i believe it was scheduled in december... o well.  better to releace 800 RCs than release unfinished software ..chough...microsoft...chough..."
    author: "standsolid"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: ">Is there any reason it was delayed for so long after the completion of the security audit?\n\nChristmas holidays?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-17
    body: "i vote for:\n1) security\n2) features\n3) bugs"
    author: "Carlos"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-18
    body: "I vote for:\n\n1) Bugs\n2) Security\n3) Non-trivial performance improvements (like say applications opening >=10% faster)\n4) Features\n\nNow there are some things I'd promote #4 to #2 for like MS Word and MS Excel documents being read perfectly, but not many.  I'm not saying that features are not important, I'm just saying they're #4 on my scale."
    author: "Super PET Troll"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-18
    body: "Don't hurry.  This is not commersial software. There are no marketing department that forces us to release the sofware before it's ready. The RC6 is starting to  look good but if we can wait just a little longer to include e.g. the Apple bug fixes to khtml it will be worth the wait."
    author: "Uno Engborg"
  - subject: "Re: Hoping for 3.1 soon."
    date: 2003-01-18
    body: "`not quite working' is a good reason.\n\nOf course, some help from Apple got it delayed a bit: it makes no sense releasing 3.1 when\nApple just gave you a bunch of trivial fixes for konqueror that you can apply right away\n(but still need testing).\n\nI've seen lots of fixes go in after RC5, RC6. And RC7 is at its second packaging iteration.\n\nLooking very good so far.\n\nNot 100% perfect, but then, nothing ever is !"
    author: "Marc Espie"
  - subject: "Thank you and Request"
    date: 2003-01-17
    body: "\nThank you for this excellent review of what's going on. Now, we know for sure that people are still working on KOffice filters.\n\nAnd the presentation is nice.\n\nJust one request. Would it be possible to have new features and security fixes listed before bug fixing ? I find the two first more interesting than the later.\n\n\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: "Yeah the filters are still getting better.  Personally I have been hacking on the KSpread formulas to make them more compliant with Excell  (and more importantly QuatroPro because it seems to be used in a few more bank apps still, at least the ones still using OS/2).   \n\nAlso If anyone is interested a while ago I started to write a KSpread interface to GSL but lost steam.  Im looking for people who want to help or take over the task.  90% of it is just takeing KSpread argument lists and converting them to C types.  I did all of the easy stuff like the foundation and support code.  Currently it wraps enough to solve a simple PDE, and that is what I originally needed.  I think it would be  a nice feature to KSpread, but lack the time or energy to take it to completion.\n\nCheers\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: "But the OpenCalc export filter is not really complete yet, is it? Seems like OpenCalc is a little bit confused, reads but doesn't show more than one table."
    author: "Peter"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: "Yes - it was just the very first version. I'll commit some enhancements this weekend. For correct number/date/time display you'll have to wait a little bit longer..."
    author: "Norbert"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: ">have new features and security fixes listed before bug fixing\n\nAny other opinions on this? \n\nDerek (vote early and often)"
    author: "Derek Kite"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: "Well, I think this would be a good idea, too - but only if it does not cause too much work for you...\n\n-Arondylos"
    author: "Arondylos"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: ">cause too much work for you...\n\nNone at all.\n\nDerek\n \n"
    author: "Derek Kite"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: "Derek,\n\nI personally believe, bug fixing deserves the credits to come first. It's a loss less unsexy and harder work at many occasions, but more rewarding in the end.\n\nSince your publication has also an impact on development directions, maybe, I hope you choose to put bug fixes first still. It also has this \"I can do that too\" appeal to make people join, or not?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Thank you and Request"
    date: 2003-01-18
    body: "> your publication has also an impact on development directions\n\nI doubt that very much.\n\nThe discussion here probably has more of an impact.\n\nI agree though on the importance of the bug fixes. When I started, the development freeze was on, so there were few new features. Maybe that is why the bug fixes came first.\n\nHmmm. (noise of gears cranking) Security will be first, if there are any. Maybe I'll put the feature part second once the release is out. Then when the freeze for 3.2 comes into effect, the bugfixes take on more importance, and I'll put them second. Is that ok with everyone?\n\nDerek"
    author: "dkite"
  - subject: "Re: Thank you and Request"
    date: 2003-01-19
    body: "That'd be perfect imo. =)"
    author: "Datschge"
  - subject: "Re: Thank you and Request"
    date: 2003-01-17
    body: "I also skip the bugs page, bugs are boring (unless I am affected by one, but then I can watch the bugzilla entry).\n\n"
    author: "Tim Jansen"
  - subject: "What is missing in KDE3.x ..."
    date: 2003-01-17
    body: "The open supported formats of XML (on conf files), the SVG (on the Icons) \nand the new Vfolder suport is JUST GREAT ... but when we will see this formats with XSL, Xlink and SMIL on Konqueror ... is Safari working (or will be) on this ... Konqueror is becoming the perfect \"Interface\" for everything, Web,\nFiles Manageament, Movies, Network (with the LISA integration), etc ...\nIs there any group working on this standart formats to implement in konqueror ?\n\nGreat Job KDE Team ... keep the good work please ...\n"
    author: "Robalinho"
  - subject: "Re: What is missing in KDE3.x ..."
    date: 2003-01-18
    body: "Just out of interest, why should KDE3.x need XML config files?"
    author: "mabinogi"
  - subject: "Re: What is missing in KDE3.x ..."
    date: 2003-01-19
    body: "Not only XML configig files.\n\nThink about XML Document that may be applied against XSLT stylesheets or even CSS. Both isn't working yet. So, there is a need to do something on it.\n\nHowever, the source reflects some very minor code about XML in that way.\n\nGreets, trapni"
    author: "Christian Parpart"
  - subject: "Konqueror Apple Merges...."
    date: 2003-01-17
    body: "Can anyone report on the speed increases of Konqueror after the Safari merges???"
    author: "ac"
  - subject: "Re: Konqueror Apple Merges...."
    date: 2003-01-17
    body: ">speed increases of Konqueror after the Safari merges???\n\nI don't think the performance work from Safari has been merged. Mostly bug fixes.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Konqueror Apple Merges...."
    date: 2003-01-17
    body: "Most of the non-trivial stuff will be merged into KDE 3.1.1, and the REALLY non-trivial stuff into KDE 3.2.\n\n\n-g"
    author: "ga"
  - subject: "Re: Konqueror Apple Merges...."
    date: 2003-01-18
    body: "Will Kongi speed up also as being a file manager, or will these improvements be for only html rendering?"
    author: "thelightning"
  - subject: "Re: Konqueror Apple Merges...."
    date: 2003-01-19
    body: "Apple took khtml not konqueror as a whole, so their improvements will only affect khtml."
    author: "Datschge"
  - subject: "Konqueror and Safari"
    date: 2003-01-20
    body: "Konqueror's UserAgent string goes like this(*):\nMozilla/5.0 (compatible; Konqueror/2.1.1; X11)\n\nSafari's UA:\nMozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/48 (like Gecko) Safari/48\n\nThere is nothing common between the UA strings of both these browsers, even if kHTML is the core of both, and unique to them (so 'Mozilla/5.0' doesn't count). Nothing to tell that they use the same rendering engine: kHTML.\n\nWeb designers and engineers are all looking towards kHTML via Safari and these guys (even the ones representing w3c) are so arrogant (Look at Mark Pilgrim's remarks or the comments at http://www.mozillazine.org/weblogs/hyatt/. They often smack of arrogance, not ignorance. Even if Hyatt suggests using @-konq tags, the others suggest -safari- tags etc. which will be spacific only to safari, as if kHTML is the intellectual property of Apple Computer). While some are going on about web standards, most would do what they always did, sniff around for User Agent strings:-\n\nNo doubt they would check for terms like Safari or AppleWebKit, terms that should belong only to Safari or other potential MacOSX browsers based on the soon-to-come AppleWebKit. Web designers will not bother about Konqueror, so the result will be that Websites designed specifically for Safari (by sniffing out User Agent strings (the easiest method, AFAIK)) will work for Safari, but reject Konqueror, even if they both use kHTML, and kHTML was designed for konqy in the first place. They are already ranting about this on all the weblogs i've seen (ranting about bugs, browser specific tags etc for safari, and they don't/won't bother about konqueror). Please, we cannot have this.\n\nThe KDE/konq/khtml developers should suggest/pressurize Apple/safari devs to include something like kHTML/3.1 in their UAs from the start, and do something similar in Konqueror itself. They should also do this for all browsers based on kHTML that may arise (Abrowser on ATheOS and [unnamed potential] win32 kHTML browsers). Apple has already announced publicly about kHTML in safari, so the web designers would know about it. It wouldn't hurt them to put kHTML/[version number] in their UA string (hopefully developed under the same codebase; and as kHTML is more accepted on the web, drop the 'like gecko' string)\n\nIn an ideal world all websites would adhere to standards and not sniff around for UA's etc. but this is not an ideal world. Some people are already hinting about it, and they are looking to support Safari and AppleWebKit, not all kHTML browsers. Just as Mozilla has been insisting that all its clones mention Gecko, KDE must insist that kHTML must be present in UAs of all kHTML browsers, including Konqueror itself. If the website engineer insists on sniffing UAs to detect Safari, he should look out for kHTML. The time to act is now, when the iron is hot. It will be too late afterwards...\n\nThanks.\n\nPS:\n(*) I don't exactly know the current Konqueror UA string as I'm not using konqueror or kde (I lost my painfully downloaded kde 3.0.2 packages and dependencies (I'm on lousy dialup) and linux install in a crash, and i'm stuck with windows. I especially miss kmail (#)). I picked it up from a slashdot post which quoted the string. Hope it's relevant as far as the message is concerned.\n(#) I'm sick of OE insisting to 'completing download of current message' even when the download refuses to move and I cancel it. The power to selectively download or delete POP3 messages from server is a boon to dialup users and only kmail has it sine some time now. Mailwasher + OE is a very poor substitute. I couldn't help showing kmail off\nto my doze-using friends (apart from the eye-candy, of course:). great feature.\n--\nR. S. Vinekar"
    author: "Rithvik"
  - subject: "Re: Konqueror and Safari"
    date: 2003-01-21
    body: "> I don't exactly know the current Konqueror UA string as\n> I'm not using konqueror or kde\n\nMy UA string is:\n\nMozilla/5.0 (compatible; Konqueror/3; Linux)\n\nKonqueror lets you choose which parameters to include (OS name, OS version, platform, machine type, language setting).  If you don't like those options, you can select from a list of the popular UA strings, including Internet Explorer, Mozilla, Lynx, WGet, and Opera.  It would not surprise me at all if Safari's UA string ends up in that list.  You can also set the UA string on a site-by-site basis.\n\nUnfortunately, Konqueror won't let you write your own string.  You might be able to type a string into the kio_httprc file, but I'm too lazy to try it right now.\n\nIf it really bothers you, you could contact the Safari and Konqueror developers and recommend that they both put \"khtml\" in their UA strings."
    author: "Jiffy"
  - subject: "Re: Konqueror and Safari"
    date: 2003-01-21
    body: "http://lists.kde.org/?l=kde-cvs&m=104304156331709&w=2"
    author: "Anonymous"
  - subject: "Re: Konqueror and Safari"
    date: 2003-01-24
    body: "Thanks for the link. I really couldn't understand what the list message means exactly. Anyway, even Apple seems to have sniffer for browser UA to detect the browser, whether safari or not:\n \nhttp://developer.apple.com/internet/javascript/sniffer.html\n(funny there is no mention of Konqueror's UA on this site)\n\nI wouldn't be mistaken if in the future there will be websites which work on Safari but reject Konqueror unless Konqy includes strings \"like AppleWebKit or Safari\" in the UA string. Sure it is very easy to change the UA to anything we want, but I would really like to see Konqy work as good as Safari by default (they should, of course, if the render code in the release versions of the browsers are in sync, shouldn't they?), which includes a unique UA string (unique to all browsers that share the same guts: KHTML) that is accepted very well.\nI really feel that safari is including the \"like Gecko\" string only in its beta phase so that it doesn't pose problems while testing. They may remove it later.\n"
    author: "Rithvik"
  - subject: "Re: Konqueror and Safari"
    date: 2003-03-23
    body: "You'll be happy to know that in the latest seeded build (only available to apple developers, not to the public yet), they've changed the UA string to:\n\nUser Agent: Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-us) AppleWebKit/67 (KHTML, like Gecko) Safari/67\n\nWoohoo!  Now they've just got to release it =)\n\nGratz to Apple -- first tabs, now the UA String.  They are actually *listening* to us! =)"
    author: "Jack Kennedy"
  - subject: "NVIDIA Crashes"
    date: 2003-01-24
    body: "Is there any news on the lockups w/ the newest NVIDIA drivers v4190 and KDE 3.1RC3+ and QT 3.1.1?  As far as I can tell, the problem is in QT w/ image loading.  But the whole system locks, can use Sysrq to reboot though."
    author: "The Iconoclast"
---
This week's <A HREF="http://members.shaw.ca/dkite/jan172003.html">issue of KDE-CVS-Digest</A> contains news about <A HREF="http://www.freedesktop.org/standards/menu/draft/menu-spec/menu-spec.html">
VFolder</A> support in KDE, more Apple updates in <A HREF="http://www.konqueror.org/">Konqueror</A>, and security fixes.  Also in this issue: commits to <a href="http://www.kontact.org/">Kaplan</a> are flowing in, the <A HREF="http://kmail.kde.org">KMail</A> merges from the different branches are finally coming together and much more.  Enjoy.

<!--break-->
