---
title: "Kolab 1.0 Released"
date:    2003-07-26
authors:
  - "dmolkentin"
slug:    kolab-10-released
comments:
  - subject: "Download Links"
    date: 2003-07-25
    body: "Here some URLs:\n<br><br>\nDownload:<br>\n<a href=\"http://www.erfrakon.de/projects/kolab/download/kde-kolab-client-1.0.1/src/\">\nhttp://www.erfrakon.de/projects/kolab/download/kde-kolab-client-1.0.1/src/</a><br>\n<a href=\"http://www.erfrakon.de/projects/kolab/download/kolab-server-1.0/\">\nhttp://www.erfrakon.de/projects/kolab/download/kolab-server-1.0/</a><br>\n<a href=\"\nhttp://www.erfrakon.de/projects/kolab/download/kde-kolab-client-1.0.0/testing/bin/SuSE-8.2/RPMS/\">\nhttp://www.erfrakon.de/projects/kolab/download/kde-kolab-client-1.0.0/testing/bin/SuSE-8.2/RPMS/</a><br>\n<a href=\"http://www.erfrakon.de/projects/kolab/download/kolab-server-1.0/bin/Debian-3.0/\">\nhttp://www.erfrakon.de/projects/kolab/download/kolab-server-1.0/bin/Debian-3.0/</a><br>\n<br>\nDocumentation:<br>\n<a href=\"http://www.erfrakon.de/projects/kolab/documentation/administration/index.html\">\nhttp://www.erfrakon.de/projects/kolab/documentation/administration/index.html</a><br>\n<a href=\"http://www.erfrakon.de/projects/kolab/documentation/architecture/index.html\">\nhttp://www.erfrakon.de/projects/kolab/documentation/architecture/index.html</a><br>\n<a href=\"http://www.erfrakon.de/projects/kolab/documentation/technical/index.html\">\nhttp://www.erfrakon.de/projects/kolab/documentation/technical/index.html</a><br>\n<br>\nScreenshots:<br>\n<a href=\"http://kolab.kde.org/images/shot-kde-client-calendar1.png\">\nhttp://kolab.kde.org/images/shot-kde-client-calendar1.png</a><br>\n<a href=\"http://kolab.kde.org/images/shot-kde-client-edit-contact1.png\">\nhttp://kolab.kde.org/images/shot-kde-client-edit-contact1.png</a><br>\n<a href=\"http://kolab.kde.org/images/shot-kde-client-edit-event1.png\">\nhttp://kolab.kde.org/images/shot-kde-client-edit-event1.png</a><br>\n<a href=\"http://kolab.kde.org/images/shot-kde-client-edit-task1.png\">\nhttp://kolab.kde.org/images/shot-kde-client-edit-task1.png</a><br>\n<a href=\"http://kolab.kde.org/images/shot-kde-client-notes1.png\">\nhttp://kolab.kde.org/images/shot-kde-client-notes1.png</a><br>\n<a href=\"http://kolab.kde.org/images/shot-web-admin-users1.png\">\nhttp://kolab.kde.org/images/shot-web-admin-users1.png</a><br>\n<a href=\"http://kolab.kde.org/images/shot-web-services1.png\">\nhttp://kolab.kde.org/images/shot-web-services1.png</a><br>\n<a href=\"http://kolab.kde.org/images/shot-web-user-settings1.png\">\nhttp://kolab.kde.org/images/shot-web-user-settings1.png</a><br>\n"
    author: "Martin Konold"
  - subject: "Debain Client Binaries?!"
    date: 2003-07-26
    body: "Hi,\n\nis there any estimate when and how Debian Client Binaries will be availbable?\n\nCheers,\nThorsten"
    author: "Thorsten"
  - subject: "Re: Debain Client Binaries?!"
    date: 2003-07-26
    body: "Yes, we already made Debian client binaries (Some minor testing still required). You may expect them very soon (I think on Tuesday next week or so) on the download site. They are made for Debian Woody and KDE 3.1.1.\n\nI expect that the Debian binaries are very well tested.\n\nRegards,\n--martin"
    author: "Martin Konold"
  - subject: "Re: Debain Client Binaries?!"
    date: 2003-07-26
    body: "3.1.1 ???!\n\nCurrent KDE ist 3.1.2\n\n\nCheers,\nThorsten"
    author: "Thorsten"
  - subject: "Re: Debain Client Binaries?!"
    date: 2003-07-29
    body: "No it's 3.1.3 ;-P"
    author: "Olivier"
  - subject: "Data Store?"
    date: 2003-07-26
    body: "Beautiful!\n\nNow, although it's Client/Server, the offline portion seems to indicate that your client contact info, etc. is available disconnected.  Is it in the KDE contact database, or will there be a new data repository for this information.  I.e., how well does it integrate with KDE?  Will kpilot work with it out of the box (I'm getting a new PDA on Monday :) )?\n\nThe Disconnected IMAP means that it's going on my laptop asap.  No more messy mailsync scripts and local imap servers to get everything working.  VERY nice."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Data Store?"
    date: 2003-07-26
    body: "The Kolab \"database\" is primarily on IMAP. This means that in the disconnected mode the data incl. emails, contacts, notes and tasks  is reused from the local disconnected IMAP Cache.\n\nAll modified and new data gets syncronized to the server as soon as the client goes online.\n\nYes, it does integrate well with the rest of KDE. The KDE Kolab client and the future Kontact are 100% native KDE.\n\nYes, synchronizing with the Palm Pilot works. Synchronizing with other gadgets will hopefully work asa kitchensync is done.\n\nRegards,\n--martin"
    author: "Martin Konold"
  - subject: "Yes!"
    date: 2003-07-26
    body: "Excellent. Now, I'm really looking forward to 3.2"
    author: "Mystilleef"
  - subject: "Kontact?"
    date: 2003-07-26
    body: "Could someone explain the connection/difference between Kolab and Kontakt? They seem to be doing more or less the same thing, yet they are two separate projects."
    author: "Janne"
  - subject: "Re: Kontact?"
    date: 2003-07-26
    body: "Did you read the story? \"Kontact, official successor of the KDE Kolab Client\""
    author: "Anonymous"
  - subject: "Re: Kontact?"
    date: 2003-07-26
    body: "I read the announcement and didn't see a mention of that."
    author: "Janne"
  - subject: "Re: Kontact?"
    date: 2003-07-26
    body: "http://kontact.kde.org/faq/, first question :)"
    author: "Daniel Molkentin"
  - subject: "Re: Kontact?"
    date: 2003-07-26
    body: "Well I'll be damned. It would be a good thing to actually read the FAQ's eh :)? Thanks for the info"
    author: "Janne"
  - subject: "Evolution..."
    date: 2003-07-26
    body: "Could anybody with Kolab-insight give a comment on this ?\n\nhttp://lists.ximian.com/archives/public/evolution/2002-September/021397.html\n\nWhy was this strange solution vCard-in-mail-filder chosen? There must be a reason, no?\n\nCheers, Peter."
    author: "Peter.Q"
  - subject: "Re: Evolution..."
    date: 2003-07-26
    body: "While I had nothing to do with this, it should be very obvious. Contacts are something that should be shared via server (if user wants), but should also be on the client when offline. Finally, when the clinet reconnects, the client/server should re-sync.\nLDAP does not offer a nice way (unless you use replication which requires you to add and maintain a ldap on the client; hard to do on a palm or zaurus ) to handle the sync between server and client, but imap does this by design.\nGotta admit that I think it is strange solution, \nbut after thinking about it for a bit, it is actually not a bad one."
    author: "a.c."
  - subject: "Re: Evolution..."
    date: 2003-07-26
    body: "The more that I think about it, the solution was an excellent choice. They are re-using tried and true code/protocol from OSS world. To go down the LDAP approach would require building quit a bit more code to emulate local storage. Then, there would have to be a redevelopment of the sync on top of LDAP.\nThis is simply more bugs in the making as well as more code to maintain.\nNice job, folks."
    author: "a.c."
  - subject: "Re: Evolution..."
    date: 2003-07-26
    body: "huh? what in IMAP is geared towards disconnected operation? nothing. Well, I guess UIDs help, but other than that there is nothing in the protocol that is for supporting disconnected operation.\n\nok, so lets pretend it is UIDs that we need. Does LDAP not have UIDs? (I don't know, not really familiar with LDAP but my guess is that they must? and if not, why not a schema that had a UID field?)\n\nalso note that an IMAP server can decide to re-label all the messages with a new UID (this is why you have to keep track of the UIDVALIDITY).\n\nignoring that, if there is 1 vcard per email on the imap server and one client changes the contact info of a contact, they'd have to upload a new message and delete the old one. If a second client was in disconnected state and also updated the contact, when he/she goes back online... what now? OOPS. Not so grand a thing, now, is it?\n"
    author: "imap guy"
  - subject: "Re: Evolution..."
    date: 2003-07-27
    body: "> what in IMAP is geared towards disconnected operation?\n\nIMAP4.1 is designed from the grounds up to support disconnected operation very well\n\n> Does LDAP not have UIDs?\n\nNo, LDAP has no functionality comparable to UIDs. LDAP was originally a protocol to enable access from tcp/ip clients to the OSI X.500 directory servers. This access is meant to be executed online by nature. LDAP is definetly not designed for disconnected use in anything else than read-only configurations.\n\n> also note that an IMAP server can decide to re-label all the messages with a new UID\n> (this is why you have to keep track of the UIDVALIDITY)\n\nThe UIDVALIDITY is _only_ changed when deleting a folder and then later creating a folder with the same name again. It is only natural that in this scenario all messages have to be fetched again. From a semantic point of view this new folder has nothing in common with the original folder except that they share the same name. So why it is a problem?\n\n> if there is 1 vcard per email on the imap server and one client changes the contact info\n> of a contact, they'd have to upload a new message and delete the old one.\n\nYes!\n\n> If a second client was in disconnected state and also updated the contact, when\n> he/she goes back online... what now? OOPS. Not so grand a thing, now, is it?\n \nThis is a traditional conflict, which of course can always happen with disconnected mode. After all disconnected always means incoherent if more than one client is doing manipulations to messages in a single folder.\n\nWith kolab we detect this situation and then prompt the user to decide which which entry to keep or alternativly \"duplicate\" the very similar messages.\n\nI don't consider this semantic a design flaw!\n\n"
    author: "Martin Konold"
  - subject: "Re: Evolution..."
    date: 2003-07-27
    body: "From\n\n> http://lists.ximian.com/archives/public/evolution/2002-September/021397.html\n> \"This won't work with evolution (and besides, i think it's a horrible,\n> horrible idea, especially when they're building a client that already\n> supports LDAP.)\"\n\nLDAP is well suited to corporate wide central managed data. E.g. with Kolab we already use LDAP where ever suitable (Kolab configuration data, credentials, Kolab accounts, central Kolab user address books, centrally maintained external addresses,...)\n\nWith the integrated KAddressbook kparts the Kolab user may then use the LDAP to look up interesting data which then gets merged into his _private_ Contacts folder. By doing so the user gets enabled to change the address entry for his personal needs (e.g. add confidential information and comments to the addressbook entry). In addition he also gains offline capablilities for free. \n\nLater we can use ACL enabled shared imap folders to make contacts (contacts are richter than LDAP entries) accessable within workgroups.\n\n"
    author: "Martin Konold"
  - subject: "Now it's time for the German Government"
    date: 2003-07-26
    body: "to stop indirect funding and create the Open Source Foundation they announced in the Election campaign. It would be very nice to get further projects done by public money."
    author: "Gerd"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-26
    body: "Where did they announce it?\n"
    author: "Tim Jansen"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-26
    body: "If the community doesn't make it itself, it's likely useless. We all see how well non-funded development works with KDE. Enough companies have incentive to contribute to KDE.\n\nLets just point of Nautilus. Funded with 17 million USD. Useless to many.\n\nThe Kolab project is an interesting case of a successful Free Software business. Their benefit it savings in development costs, maintaince costs, where as the software is designed to be reusable in other settings.\n\nI would like to see the state hire e.g. companies to put KDE to specified desktop usage, in line with what is needed by the state agencies and institutions. It should be cheaper than buying and applying other stuff (otherwise time is not yet ready, but will be later).\n\nThis would e.g. bring a useful clipboard. GNU/Linux as Bundestag Desktop system was canceled, because you cannot copy&paste from Browser to Office without loosing all formating and images. This is a terrible shortcoming of Linux desktop systems as of now.\n\nComposing office documents from all kinds of other sources is the daily job of Bundestag members (and others too), but so far, no quick and dirty ways to create something of communicative exchange value.\n\nA call for tender for Bundestag Desktop systems may one day end of with a KDE system enhancement done by a commercial company. That will be a good day. \n\nIf it must be funded explicitely, I am against it. Funding means paying more than it's worth. Believe me, a well working system is worth a lot. Certainly enough to make companies a good time improving KDE to suit Call for Tenders of institutions, companies all over the world.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-27
    body: "> If the community doesn't make it itself, it's likely useless. We all see how well non-funded development works with KDE. Enough companies have incentive to contribute to KDE.\n \nActually if you look at the KDE CVS Digest, the previous article, Derek points out how many people are currently funding KDE development. Admittedly not many, but I'm one and I sponsor Andras Mantia to work on Quanta. I would say I'm in the community though so I'm not sure exactly how it all fits with your point.\n\n> Lets just point of Nautilus. Funded with 17 million USD. Useless to many.\n\nFor whatever one's opinion was of them, and frankly their PR department annoyed me by making it sound like the second coming discovering the first Linux desktop, they basically were in the dot com bubble. VC money was flowing and they were going to make a business, although they couldn't say exactly how, but since they invented the GUI at Apple (I know you're going to say Xerox did but that doesn't read as well) they were cool to give money to. Aside from the fact that I'm scratching my head that nobody gave me a few million dollars to party with they clearly were bringing a commercial development model to Linux. I mean they had something like 17 usability people for one file manager. Nearly as many usability people as programmers and in a Linux Magazine article they could not be nailed down on exactly what their business model was. You're not going to see that repeated any time soon because millions of dollars don't usually squirt out like that for no better reason.\n \n> The Kolab project is an interesting case of a successful Free Software business. Their benefit it savings in development costs, maintaince costs, where as the software is designed to be reusable in other settings.\n\nThis is a rational and duplicatable example. It needs to happen in Koffice.\n \n> If it must be funded explicitely, I am against it. Funding means paying more than it's worth. Believe me, a well working system is worth a lot. \n\nI'm not sure how to read this. I think if multiple entities fund development it's cheap, or if a large client based entity does. A lone guy like me is going to pay more than it costs to buy shrink wrap (thanks for making me feel better) but it's still worth it if you get what you want. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-28
    body: "Hello Eric,\n\nyou and what you do with Quanta is certainly special. Special in my appreciation as well. From what I see, you make sure, your money is well directed and not wasted. \n\nThis is not how state funding works.\n\nBut Free Software is of enough benefit to society to make IBM, Oracle, HP, Sun and all big players fund what is needed as infrastructure. So why should a government step in there and order things it does not need?\n\nLet the governments come to use Office, Project Management, CAD and other kinds of design tools, Web Design, etc. on Free Software. What do you think this will bring to enhance Free Software? If only for some strange feature gain to Apache that was not mainline before, but needed in a custom software for government...\n\nOnce we cross that line, Free Software takes off. And I am sure it will :-)\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-27
    body: "> Lets just point of Nautilus. Funded with 17 million USD. Useless to many.\n\n13 million\nBut that wasn't just for Nautilus.\nThat was to run an entire company of 100 people\nAnd run other projects, so lets not get carried away here."
    author: "john"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-28
    body: "their one and only product was Nautilus however. "
    author: "anon"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-28
    body: "\"their one and only product was Nautilus however.\"\n\nwrong ... nautilus was the client for all their services - which sucked up 1/2 or more of their burn BTW (that, plus the fancy offices). AOL's client isn't their \"sole product\" and Nautilus was an \"mini-AOL\" style play ... using massive amounts of VC money (in oversupply at that stage)."
    author: "OSX_KDE_Interpollination"
  - subject: "Re: Now it's time for the German Government"
    date: 2003-07-28
    body: "What services???"
    author: "ac"
  - subject: "KDE switching?"
    date: 2003-07-27
    body: "Just out of curiousity: Will the KDE-Project switch to Kolab for internal Mail and colaboration?\n\nCheers,\nThorsten"
    author: "Thorsten"
  - subject: "Re: KDE switching?"
    date: 2003-07-27
    body: "Switch from what?"
    author: "Thorsten Schnebeck"
  - subject: "Re: KDE switching?"
    date: 2003-07-27
    body: "I am willing to setup a central Kolab Server for the KDE community provided I get a suitable server machine sponsered. \n\nThe security design of Kolab allows for internet usage without the need for vpn or other explicit tunnels.\n\nBasically I have to offer excellent bandwidth (100 Mbit + 1 GBit Backbone) and maintainance.\n\nI think that a really big and heavily used central KDE Kolab server would be a nice testbed in order to check if the server really scales as well as assumed.\n"
    author: "Martin Konold"
  - subject: "Re: KDE switching?"
    date: 2003-07-28
    body: "For those of us that are not (directly) involved on KDE development, but are on other free software projects, I think a really really interesting idea would be to have a (big, I guess, Sourceforge?) Kolab server for hosting free software developers vcards and calendaries. Imagine being able (using Kontact) to take a look at XYZProject maintainer calendar, invite JXCProject developer to an appoinment on IRC to discuss a patch, taking a look at QWERTYProject TODO list, or just losing some time looking at 100 random developers cards (the projects they're involved, their countries or just their photographs :) ). I think this is a really interesting idea, althought it would probably need a big big server (but, again, this could be the definitive test to see if Kolab really scales)."
    author: "Chony"
  - subject: "Re: KDE switching?"
    date: 2003-07-28
    body: "Great idea!\n\nContacts and Calendaring for all of KDE-devel :-)"
    author: "OSX_KDE_Interpollination"
  - subject: "Re: KDE switching?"
    date: 2003-07-29
    body: "\"I am willing to setup a central Kolab Server for the KDE community provided I get a suitable server machine sponsered.\"\n\nYes, please do this - I think it would help tremendously with adoption!\n \n"
    author: "steve"
  - subject: "Re: KDE switching?"
    date: 2003-07-30
    body: "What kind of server machines would be suitable in your experience? \nIt should be possible to organize a respective donation since the Kolab/Kontact combo will surely become one of the most interesting single part of the upcoming KDE 3.2 release for the press and commercial adoption."
    author: "Datschge"
  - subject: "Re: KDE switching?"
    date: 2003-07-30
    body: "It basically depends on the number of users / concurrent users to expect."
    author: "Martin Konold"
  - subject: "MS Access Server + Kolab client ?"
    date: 2003-07-27
    body: "Is it possible to use an ms access server\nwith the kolab client - and how ?\n\nAt my job I shall use outlook client  + access server.\nI do almost everything with linux and I do not want to \nswitch between the Linux +WindowNT. \n\nMy first way the stay there at one OS is to use \ncrossover office at linux with the outlook2000 \nclient.\nIs there any other way ?"
    author: "ebartels"
  - subject: "Re: MS Access Server + Kolab client ?"
    date: 2003-07-27
    body: "Kontact will (hopefully) support Exchange >=2000 in its initial release (KDE 3.2). If you urgently want it, feel free to help us :)\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "OT: Toolkits"
    date: 2003-07-28
    body: "Why isn't the \"Dot\" reporting on freshmeat's post? QT is currently getting savaged by alot of critics currently (more than normal), surely the KDE people have something to say about this?"
    author: "sigma"
  - subject: "Re: OT: Toolkits"
    date: 2003-07-28
    body: "Why don't you post a link then? or even better - submit it?"
    author: "Joergen Ramskov"
  - subject: "Re: OT: Toolkits"
    date: 2003-07-28
    body: "If you need to know it: http://freshmeat.net/articles/view/928/"
    author: "Anonymous"
  - subject: "Re: OT: Toolkits"
    date: 2003-07-28
    body: "Because this isn't a \"lead-the-trolls-to-the-next-flamefest\" site?"
    author: "Anonymous"
  - subject: "Re: OT: Toolkits"
    date: 2003-07-28
    body: "I am not a \"KDE people\", but what do you want to say to this? The article is the authors opinion, and does not contain and wrong information. What do you want to say to guys who flame because of the license? They are right, it is GPL and not LGPL. I do not really care, but if they do, I can not change it. Beat them with better software, not by wasting your time in flamewars that ain't worth it.\n\n"
    author: "AC"
  - subject: "Re: OT: Toolkits"
    date: 2003-07-28
    body: "I think you get the point :)\n\nIm waiting for the day when I see the following on slushdot \"What? you mean with OSS I'm allowd to write something better?  As in do it myself?  Whoah, no way I'm going back to Windows where I just use what I'm givin, its easier to complain and not do anything when I don't have a choice.\"\n\nYes virginia, you to can write Open Source Software.  Personally I dont care if there are a million forks of something.  The best one will rise to the top, and the crap will sit on sourceforge for the rest of eternity...\n\nJust my 2c...\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: OT: Toolkits"
    date: 2003-07-28
    body: "Free Qt does allow you to develop commercial apps unlike what the author claims in the first sentence or two."
    author: "ac"
  - subject: "Re: OT: Toolkits"
    date: 2003-07-29
    body: "Do you think those who are savaging QT would listen to anything anybody said?\n\nDon't waste your breath.\n\nDerek"
    author: "Derek Kite"
  - subject: "Kroupware-kmail ruined my incoming mail"
    date: 2003-07-29
    body: "\nMy server uses wu-imapd, and my mbox (i.e. Incoming) totally destroyed by the kroupware-kmail.  be carefull.\n"
    author: "SHiFT"
  - subject: "Re: Kroupware-kmail ruined my incoming mail"
    date: 2003-07-29
    body: "do not understand anything....  now appeared again? \nmiracles ;-) \n"
    author: "SHiFT"
  - subject: "unforgettable experience"
    date: 2003-07-29
    body: "\n\nLooks like this kroupware mail client was forked off a really old version of Kmail -- i had absolutely the same problems with it, as i had before kde 3.0. \nWell -- if new kdenetwork and kdepim will have all the features that kroupware has -- it will be great, but, it looks like the current version of kroupware-client was not synced with latest version of kmail. the only option for IMAP it has is so called \"disconnected imap\" and it has \"use subscribed folders\" flag always grayed -- as a result, when i tried to open my IMAP account (from server running wu-imapd) -- i got listing of my whole home directory. ;-) \n\nthis is a really good piece of software -- looks like it is going to be a really big competitor to MsOutlook/Exchange, but what if it stands for a separate industrial-use product, they should merge with latest Kmail improvements as well -- so far for everyday work have to switch to the recent kde-3.1.3 \n\n\n  "
    author: "SHiFT"
  - subject: "Re: unforgettable experience"
    date: 2003-07-30
    body: ">> when i tried to open my IMAP account (from server running wu-imapd) -- i got \n>> listing of my whole home directory.\n\nWell, this is what happens when you connect Outlook/Outlook Express to a stock UW-IMAP server ... so if they're trying to be compatible with Outlook/Outlook Express, they have succeeded.\n\nUse a real IMAP server, or patch your UW to not use $HOME as it's mail directory."
    author: "ranger"
---
After a long phase of testing and bugfixing, <a href="http://kolab.kde.org/">Kolab</a>, the result of the <a href="http://www.kroupware.org/">Kroupware</a> project, has finally been <a href="http://www.kroupware.org/news/pr-20030725.html">announced</a>. Kolab is a groupware solution that offers email, <a href="http://kolab.kde.org/images/shot-kde-client-calendar1.png">group calendaring</a>, <a href="http://kolab.kde.org/images/shot-kde-client-notes1.png">notes</a> and <a href="http://kolab.kde.org/images/shot-kde-client-edit-task1.png">tasks</a>. Kolab Server is available as 1.0, the KDE Kolab Client is provided in an improved 1.0.1 version. Additionally, the Kolab Server can also be used with Microsoft Outlook&trade;. This makes it an ideal tool for migrations to the GNU/Linux platform.



<!--break-->
<p>
The KDE Kolab Client is based on well-known KDE components such as <a href="http://kmail.kde.org/">KMail</a>, <a href="http://www.kaddressbook.org/">KAddressbook</a>, <a href="http://www.korganizer.org/">KOrganizer</a> and <a href="http://pim.kde.org/components/kpilot.php">KPilot</a> and integrates them. One of the most interesting features is the disconnected IMAP mode, which makes the Kolab Client offline capable. All Kolab Client developments are being merged within KDE CVS and scheduled as new features for KDE 3.2 right along with <a href="http://www.kontact.org/">Kontact</a>, official successor of the KDE Kolab Client.
<p>
The Kroupware project was started in September 2002 as contract work by the three companies <a href="http://www.erfrakon.com/">Erfrakon</a>, <a href="http://www.intevation.net/">Intevation</a> and <a href="http://www.klaralvdalens-datakonsult.se">Klar&auml;lvdalens Datakonsult</a> and is now being continued by an emerged community on <a href="http://kolab.kde.org/">kolab.kde.org</a>. First successes include an initial adoption of the <a href="http://www.horde.org/turba/">Horde Turba</a> web contact manager and a growing <a href="http://kolab.kde.org/howtos.html">collection of HOWTOs</a>.


