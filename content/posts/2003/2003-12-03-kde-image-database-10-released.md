---
title: "KDE Image Database 1.0 Released"
date:    2003-12-03
authors:
  - "jpedersen"
slug:    kde-image-database-10-released
comments:
  - subject: "Modifying meta info on multiple files in konqueror"
    date: 2003-12-03
    body: "I tend to dislike using dot.kde.org as a bug.kde.org but this seems just so relevant...\n\nIn konqueror, you can right click on an image, and add meta info for description.  This is really cool to use to store info about the picture.\nHowever I can't select multiple pictures, and edit the meta info for all of them at once - I can't think of a nice way to do this.\n\n"
    author: "JohnFlux"
  - subject: "Re: Modifying meta info on multiple files in konqueror"
    date: 2003-12-03
    body: "Reply to my own post...  just wanted to clarify that this was in response to:\n\n>>It's easy to index your images -- \"Label these 10 images as pictures of my girlfriend.\" \n\nAnother quick point:\n\n>>Within a few seconds you can find a given image you have in mind -- \"Locate that image of my girlfriend from the holiday on Mallorca in 1998\", or \"Get me that photo with both my girlfriend and my father.\" \n\nThis seems again like something konqueror (or friends) should be able to do.\nPerhaps it already can - search for files for text in meta info?\n\nThe gnome storage stuff seems perfect for this kind of thing..\n\n\nP.s. I'm not knocking the app - it looks cool :)\n\n"
    author: "JohnFlux"
  - subject: "Re: Modifying meta info on multiple files in konqueror"
    date: 2003-12-03
    body: "fire up the Find File util (or go under the Tools menu in Konqueror) and on the Contents tabs there is a metainfo search option. it works rather well. for images, I select All Images from the file type drop down menu and then go crazy with the metainfo search =) this may be new in 3.2, i don't remember what it looked like in 3.1 anymore"
    author: "Aaron J. Seigo"
  - subject: "Re: Modifying meta info on multiple files in konqueror"
    date: 2003-12-03
    body: "But not for PNG-images. They don't have a meta-field..."
    author: "Andreas Pietzowski"
  - subject: "EXIF?"
    date: 2003-12-03
    body: "It does not support EXIF information and so it is useless for me. :(\n\nAs a hobbyist photographer, *what I need to switch my photo db on Linux* is something like this:\n\n>>>\n\"( http://www.iview-multimedia.com/products/mediapro/index.html ) iView MediaPro</A> is essential for creative and media professionals who need to organize, view, annotate, print, backup and repurpose media, as well as automate workflows. \nWith iView MediaPro, you can make the most of your media to create flexible slide show presentations, QuickTime movies, theme-based web pages, print multi-image reports, and custom PDF layouts. You can also enhance the color, brightness and other visual characteristics of photographs. \n\nVersion 2.0 comes with its new cross-platform Catalog Reader for free distribution. Share iView catalogs and slide shows with clients, colleagues, friends and families who do not own an iView program. \n\nOrganizing your media into catalogs allows you to instantly search, annotate and classify media with unlimited criteria across large volumes of mounted or unmounted media files using the drag-and-drop organizer. MediaPro supports IPTC, EXIF and QuickTime annotation standards, user defined annotations and128 file formats, including popular image, audio/video, and digital camera raw formats. It also converts media into a variety of formats and supports AppleScript. \n\nCreates media catalogs containing thumbnails and annotations that can be viewed even when the original files are no longer on a mounted drive. iView MediaPro allows you to import photos direct from a digital camera and burn media onto a CD-ROM or backup media to a removable volume.\"\n<<<\n\nAnd of course, I need 16bit per channel support on Gimp. And then I can switch on Unix/Linux with all my gear. But such support ain't there yet for pros or for serious hobbyists."
    author: "Photographer"
  - subject: "Re: EXIF?"
    date: 2003-12-03
    body: "That's a good point.  There is an \"exif\" command line utility for Linux/Unix so it should be *very* easy for Jesper to add this to KimDaBa in the next release.  Stay tuned!"
    author: "anonymous"
  - subject: "Re: EXIF?"
    date: 2003-12-03
    body: "And indeed the next version will have EXIF support.\nI had a hard time discarding it for version 1.0, but I had been working on it for almost a year, without a single release, so I had to cut somewhere.\n\nI've already started working on what will be version 1.1 (no 1-year development period again).\n\n1.1 will likely be out just after next holiday - which will be XMas ;-)"
    author: "Jesper Pedersen"
  - subject: "Re: EXIF?"
    date: 2003-12-03
    body: "> As a hobbyist photographer, *what I need to switch my photo db on Linux* is something like this:\n\nWow! That's a lot of \"hobbyist\" features. What do you bet they had more than one guy working on it for a year? ;-) Let poor Jesper have a little glory before pointing out all that is not in his initial release. ;-) Actually I looked at a previous version of KimDaBa and it is very cool, though it's obvious it doesn't have some features that could be useful at this time. It is a very slick application that will be useful to me.\n\n> And of course, I need 16bit per channel support on Gimp. And then I can switch on Unix/Linux with all my gear. But such support ain't there yet for pros or for serious hobbyists.\n\nWhy don't you have a look at Cinipaint? It's good enough for films like \"2 Fast 2 Furious\", \"Scooby-Doo\", \"Harry Potter\", \"Stuart Little\" and more. It's also GPL'd and of Gimp heritage. While you're at it if you don't mind shelling out the bucks and want to produce movies there's other cool software like Maya, but it's about the same price as a new car to set up that Linux gear. ;-)\n\nClearly adding a lot of other features like you mentioned would take some work but there are image gallery creation programs like Kallery by Andras Mantia and other programs that do tasks similar to things you mentioned. To actually make them useful in an integrated set up would take some work and use of DCOP, perhaps kparts and could even be made to interact with programs like Kommander or KJSEmbed. So while it may not be an easy rival to the software you mentioned it's closer than you may think. One important factor is it doesn't have to all live in one application if the various programs can interact well."
    author: "Eric Laffoon"
  - subject: "Re: EXIF?"
    date: 2003-12-03
    body: ">Why don't you have a look at Cineipaint?\n\nCinepaint does have 16bit support but is not as good as the new Gimp 1.3.x for photo retouching and editing. But the Gimp again, doesn't have 16bit. It is a bad cycle."
    author: "Photographer"
  - subject: "Re: EXIF?--Cinepaint was FILM GIMP"
    date: 2003-12-03
    body: "Film Gimp has 16 bit support.\n\n\"..The extended dynamic range of CinePaint appeals not just to 35mm cinematographers, but to 35mm still photographers as well. Still photographers can think of CinePaint as having many more F-stops of range, of being capable of capturing much more subtle nuances of color in a vast blue sky for instance. CinePaint handles 8-bit, 16-bit linear, and 16-bit float images.\"\n\nhttp://cinepaint.sourceforge.net/docs/features.html\n\nI must be missing by what support it doesn't have that you need."
    author: "Marc J. Driftmeyer"
  - subject: "Re: EXIF?--Cinepaint was FILM GIMP"
    date: 2003-12-03
    body: "You did not read my reply before you reply!\n\nI said that CinePaint DOES have 16bit support, but is not suitable for photo work as much as Gimp 1.3x is. They have removed a number of Gimp features in order to make it more suitable for movies instead. Gimp 1.3x is way better for photography work, but it does not have 16bit support."
    author: "Photographer"
  - subject: "Re: EXIF?--Cinepaint was FILM GIMP"
    date: 2003-12-03
    body: "1. If you really care for your pictures you would never, ever, use jpeg, so exif should be an non-issue...\n2. If you don't care to spend money on a Linux retouche app with 16bit support, check Amazon Sweet16 ( http://www.ifx.com - _very_ expensive), or Photogenics HDR ( http://www.idruna.com - not as expensive as Photoshop)."
    author: "LaNcom"
  - subject: "Re: EXIF?--Cinepaint was FILM GIMP"
    date: 2003-12-05
    body: "Do you know how to get Nikon Coolpix 2100/2500/3100 to take pictures without saving them in JPG format?"
    author: "anon"
  - subject: "Re: EXIF?--Cinepaint was FILM GIMP"
    date: 2004-10-04
    body: "EXIF can be used with TIFF images as well as JPEG. (But anyhow I disagree with the assertion that if one cares \"never, ever\" use JPEG. One may care, but there sometimes have to be trade-offs between file size and quality)."
    author: "Tim Middleton"
  - subject: "Coolness"
    date: 2003-12-03
    body: "I've always wondered what this bugger did.  Hope to see more apps here  :)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Coolness"
    date: 2003-12-03
    body: "Ian!\nHow did you manage to talk to me for more than an hour in total at the Kastle without getting a demo ;-)"
    author: "Jesper Pedersen"
  - subject: "Fixed Properties"
    date: 2003-12-03
    body: "> KimDaba offers to describe images with a number of properties. These includes date, persons on image, location of image, plus a keyword field\n\nThis sounds only useful for people taking photographs of persons at locations with no need for more than one own property. Why not make this full configurable or possible to add/search for unlimited property|content pairs?"
    author: "Anonymous"
  - subject: "Re: Fixed Properties"
    date: 2003-12-03
    body: "Hey mate, did you expect me to include a complete feature description in this short announcement?\n\nOf course you can configure the categories to your hearts content. So you may include categories for pet, things, buildings, sex toys, you name it ;-)\n\nCheers\nJesper."
    author: "Jesper Pedersen"
  - subject: "This sounds great"
    date: 2003-12-03
    body: "After Mosfet's PixiePlus ended up dying with him, I thought I would never have a good image manager again, this sounds like it will fill its role for me =)"
    author: "Mario"
  - subject: "Re: This sounds great"
    date: 2003-12-03
    body: "Mosfet died?"
    author: "Khtml bug #1"
  - subject: "Mosfet?"
    date: 2003-12-03
    body: "Really, where is Mosfet??\n\nbtw this app looks nice. Maybe some functionality could also be in Konqueror, and maybe even using the underlying filesystem's extended attributes (like reiser 4). Then you could burn your collections on CD and browse and search them just as easy."
    author: "wilbert"
  - subject: "Re: Mosfet?"
    date: 2003-12-03
    body: "Mosfet comes and goes, for this is his way.  He'll be back at some point.  There are a lot of \"disappearing hackers\" in Open Source.  KDE has a number of guys that show up for a while and then mysteriously disappear again to later resurface.  I think Mosfet is just a bit louder about it.  :-)"
    author: "Scott Wheeler"
  - subject: "KimDaBa"
    date: 2003-12-03
    body: ">>It's easy to index your images -- \"Label these 10 images...\" etc\n\nThis is exactly what I've been looking for! Kewl!\n thanks x 10e6!"
    author: "cypherz"
  - subject: "F-Spot comparison?"
    date: 2003-12-03
    body: "How is this app compares to GTK#'s upcoming F-SPOT, feature-wise?\nMore info here: http://perazzoli.org/blog.php"
    author: "Photographer"
  - subject: "Its been dotkded"
    date: 2003-12-03
    body: "Downloading at about 2 kilobyte/sec, just like in the bad old days. I'm off to bed now, I'll mirror it on my university space when I get up.\n\nFrom the screen shots, it looks interesting. Currently I have all my photos in chronological order in folders based on when I downloaded them. I imagine thats just going to get more unwieldy the longer I own the digital camera though."
    author: "Ian Monroe"
  - subject: "Re: Its been dotkded"
    date: 2003-12-03
    body: "Indeed it has ;-)\nI've never seen my 512K line so hot!\n\nAll please be patient, I'm moving as we speak, so things so be much better in less than an hour ;-)"
    author: "Jesper Pedersen"
  - subject: "Re: Its been dotkded"
    date: 2003-12-03
    body: "http://www2.truman.edu/~iam504/kimdaba-1.0.tar.bz2"
    author: "Ian Monroe"
  - subject: "Re: Its been dotkded"
    date: 2003-12-03
    body: "Its at ktown now, os there should be more enought bandwidth now ;-)\n\nThanks though."
    author: "Jesper Pedersen"
  - subject: "Looks very cool !"
    date: 2003-12-03
    body: "Can't wait to try it !"
    author: "quarus"
  - subject: "GREAT!"
    date: 2003-12-03
    body: "Just about 4 weeks ago I discussed with a friend about such an app and our needs.\nI'll give it a try asap, because there are about 14'000 Digitalfotos on my disk, and I can order it how I want - it's a chaos..."
    author: "Dani"
  - subject: "Re: GREAT!"
    date: 2003-12-03
    body: "Dani.\nPersonally I have approax 3.500 images managed by KimDaBa, and I do indeed expect that it scales to a much larger number, afterall that is the whole idea with KimDaBa.\n\nSo please get in contact with me once you have started using KimDaBa, so we can profile any bottlenecks that might be left.\n"
    author: "Jesper Pedersen"
  - subject: "Re: GREAT!"
    date: 2003-12-03
    body: "I think a db(mysql) backend could be good.\nThis should be relative easy with kioslaves, not?\nI'm not a KDE Developer... (only java+php)\n\nAnd if the data's in a db, it would be possible\nto build a webfrontent via php.\n\nI'll get in contact with you when KimDaBa is up and running..\nOh, and yes, exif extraction would be very cool.\n\"search all images width flash enabled\" ;-)\n\"search all images with exposure time greater than 1 sec\"\n\nare wishlist and buglist on the web accessible?"
    author: "Dani"
  - subject: "Re: GREAT!"
    date: 2003-12-03
    body: "I did consider putting it into a db from the beginning, but decided to see how a plain file would do, so far the loading time of the 3500 images is not significant. If it should turn out to get to slow I'll consider write db support.\n\nAs long as its fast enough a plain XML file is much easier to maintain than a db.\n\nYou may still write php code that access the XML file.\n\nwishlist, buglist: see bugs.kde.org."
    author: "Jesper Pedersen"
  - subject: "Re: GREAT!"
    date: 2003-12-03
    body: "For projects where it's just a little bit too big for a file, and too small for a database, it might be worth having a look at sqlite.\n\nbtw, it's not entirely clear.  Do you use the meta information in the actual images at all?\n(I often store the descriptions of my photos in there)"
    author: "sqlite"
  - subject: "Re: GREAT!"
    date: 2003-12-03
    body: "Are you referring to the EXIF info? No I dont use that currently, but next version will include support for it."
    author: "Jesper Pedersen"
  - subject: "Re: GREAT!"
    date: 2003-12-03
    body: "/me checks in konqueror..\n\nAh yeah it is exif - that makes other comments make more sense now.. thanks.\n\nBtw when I right click on a png, it doesn't offer me a chance to add a comment or anything about the image.  But I thought png allowed arbitrary key-value pairs?\nIs this just a case of the gui not supporting it yet, or that it isn't EXIF, or something?\n"
    author: "sqlite"
  - subject: "PNG"
    date: 2003-12-04
    body: "PNG doesn't have EXIF (its a standard limited to JPEG I think). Konqueror shows comments on the PNG on the Meta-Info tab but doesn't have a way to add any. "
    author: "Ian Monroe"
  - subject: "Seems very good so..."
    date: 2003-12-03
    body: "What about adding this app to the future KDE 3.3 ?\nThere is no app like this in the official release and it would be a good improvement."
    author: "LC"
  - subject: "Re: Seems very good so..."
    date: 2003-12-03
    body: "Getting applications into the KDE release doesn't seem to be that easy, and I'm for sure no politician (Dont use this agains me if I ever choose to go into politics ;-)\n\nI did ask some people at Kastle if there was interest in getting KimDaBa in there, but people wasn't exactly exited, so lets see.\n\nFeel free to add presure whereever you find it appropriate if you want it in there. I'd be happy to support whatever actions technically needed to get it in there."
    author: "Jesper Pedersen"
  - subject: "Re: Seems very good so..."
    date: 2003-12-03
    body: "Personaly I don't see any compelling reasons why KimDaBa should go into the main release of KDE. But it's a perfect app to put under the extragear umbrella. And with some planning there are no problems to time your releases to the main KDE ones. And then you don't have to play with politics, just bribe the release dude to add a couple of lines and a link in the release anoncement.  "
    author: "Morty"
  - subject: "Re: Seems very good so..."
    date: 2003-12-03
    body: "Of course, you already know that it *is* under the extragear umbrella, right?! ;-) \n\n(See http://extragear.kde.org/apps/kimdaba.php)"
    author: "anonymous"
  - subject: "imgSeek"
    date: 2003-12-03
    body: "If you add some imgSeek.sf.net search features, I will be happy. ;-)"
    author: "Tobias"
  - subject: "Re: imgSeek"
    date: 2003-12-03
    body: "I'm wondering if you are capable of drawing your girlfriend so well that it finds her rather than your mother ;-)\n\nNice idea, and I've put it on my wish list, but not exactly at the top.\n\nThanks\nJesper."
    author: "Jesper Pedersen"
  - subject: "Re: imgSeek"
    date: 2003-12-04
    body: "HejJesper\nIt would certainly be a good idea to be able to group lots og images automagically  like imgSeek does. Please  give it  a try -  the  algorithm  used  is  described somewhere  at imgseek.sf.net and if i  remenber  correctly not to difficult to implement - the result is a hash value.\n"
    author: "J. And"
  - subject: "Brings order into chaos"
    date: 2003-12-03
    body: "Wow! This thingy is cool!\n\nI'm currently hooked on Windoze but am planning to convert to Linux on XMas. One of the several pieces of data that i will have to \"convert\" is a moderately large collection of digicam images, which i previously looked at with a horribly stupid digicam browsing tool plus a simple folder structure. KimDaBa gives me categories and free associations to introduce some order into the chaos ;-))\n\nI will *definitely* use it around christmas, expect feedback from me then"
    author: "Robert"
  - subject: "Gallery Remote Protocol support??"
    date: 2003-12-03
    body: "Will KimDaBa support the remote protocol of Gallery in a next version? ( http://tinyurl.com/xjli )"
    author: "Sander"
  - subject: "Re: Gallery Remote Protocol support??"
    date: 2003-12-03
    body: "The next release (for January) is already stuffed with other features I want in, so no - not unless someone else joins the project and implements it (hint hint)\n\nI've bookmarked your page, so maybe someday. If the database you refer to is capable of storing all the data KimDaBa has, then it sounds like a good idea to me."
    author: "Jesper Pedersen"
  - subject: "KDE already has Kmrml"
    date: 2003-12-03
    body: "There is an application included in KDE since a long time (however I never had luck to get it work) called Kmrml. Is a client for GIFT, a image indexing server with manages to clasificate images similar to an example proposed by the user; it uses an xml based protocol so it can be used in a distributed way over internet. I think it would be interesting to make both applications work together (a possible backend for KimDaBa??). However I have no the time to work on this :-(\n\n"
    author: "John the anonymous"
  - subject: "digikam and KimDaBa"
    date: 2003-12-03
    body: "I have tested both programs and digikam 0.6.0pre is sooo sweet especially with its plugin mechanism. But KimDaBa has the far better features to organize my pictures.\n\n<dream>\nKimDaBa as a digikam-plugin!\n</dream>\n\nWorking with a good digicam (EOS300d) and Linux is quite ok. When somebody wants to work on a kde imaging program _please_ use 16bit RGB and colormanagement. You need this today! Scanner and RAW formats use more than 8 bit per channel. There is only CinePaint today for Linux so there is a deep need for a good (=KDE) image manipulation program.\n\nAnd don't underestimate the size of a picture.\nKuickshow can't show\nhttp://www.tawbaware.com/maxlyons/gigapixel_strip.jpg\n(40786x100 pixel)\n(gimp works)\n\nOh, and a kde-tool (like PTassembler or better than Hugin) for the panotools would also a nice addon ;-) \n\nBye\n\n  Thorsten  "
    author: "Thorsten Schnebeck"
  - subject: "Re: digikam and KimDaBa"
    date: 2003-12-05
    body: "Warning: this post contains shameless self-promotion :->\n\n> Kuickshow can't show\n> http://www.tawbaware.com/maxlyons/gigapixel_strip.jpg\n> (40786x100 pixel)\n\nGwenview can view it.\nHave a look at it: http://gwenview.sf.net"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Looks fascinating!"
    date: 2003-12-03
    body: "Downloading it right now. I've got (quick check) 1987 photos from my camera in a single directory, and Konqueror's thumbnails were getting a bit unwieldy. This looks like a perfect tool for sorting stuff properly, without lots of complicated directories clouding the issue.\n\nLooks like it's time to get compiling...\n"
    author: "Adam Foster"
  - subject: "Anyone else seeing compilation errors?"
    date: 2003-12-04
    body: "Does anyone else see linker errors for the CVS version \nin kdeextragear-2?  kimdaba hasn't been building here\nfor quite some time, but I didn't bother because I thought\n\"It's still in development and will eventually be fixed...\".\n\nNow that it's released but with the error still there \nI think I might have some problems that are specific \nto my installation.  \n\nThere are several errors like this. \nimageconfigui.moc.o(.gnu.linkonce.d._ZTV13ImageConfigUI+0x20): \nundefined reference to `ImageConfigUI::~ImageConfigUI [in-charge]()'\n\nSo, does the CVS version build for all of you who have tried? \n\n\n"
    author: "cm"
  - subject: "Re: Anyone else seeing compilation errors?"
    date: 2003-12-06
    body: "Yes I got several build errors from the csv version.\n(Why would a programmer put code into csv for the world to view that did not even compile?)\n\nSo I backed off the the released version.\n\nOnly to fine that it requires unsermake, which to the best of my knowledge no distro has yet included.  So I started searching for that, and realized it was\na fools errand to start whacking my system to compile this thing.\n\nI'd like to use this software, but two hours of downloading and building\ngive me the impression its not ready for beta testing yet."
    author: "jsa"
  - subject: "Re: Anyone else seeing compilation errors?"
    date: 2003-12-07
    body: "The CVS version compiles just fine for me, but if you do not use KDE Head you might be in trouble with it. (I'll backport it to 3.1.x when releasing)\n\nRegarding unsermake, it was indeed not my intend that the release should contain that code, but as I use unsermake myself, obviously that got into the release.\nThis does however not mean that you need unsermake! so if unsermake is required for you, it is either a bug in unsermake or a time stamps problems on your system.\n\nRegarding the software not being ready for beta testing: Please be reasonable now, any release will have problem on some system, and obviously you were the unlucking one. But starting shouting out that it is not even ready for beta testing, is at best not fair."
    author: "Jesper Pedersen"
  - subject: "Re: Anyone else seeing compilation errors?"
    date: 2003-12-08
    body: "> The CVS version compiles just fine for me, but if you do not use KDE Head \n> you might be in trouble with it. (I'll backport it to 3.1.x when releasing)\n\nWell, I do have CVS HEAD, both of KDE and kimdaba.  And the rest of \nthe KDE source compiles, so I don't think my system is broken.\n\nShould I report a bug somewhere with the complete error output?  \nIf so, where? I haven't found it as category at bugs.kde.org...\n\n"
    author: "cm"
  - subject: "Re: Anyone else seeing compilation errors?"
    date: 2003-12-08
    body: "Yes, please do send me the error message blackie@kde.org\nKimDaBa does actually exists in bugs.kde.org."
    author: "Jesper Pedersen"
  - subject: "Re: Anyone else seeing compilation errors?"
    date: 2003-12-08
    body: "For the record: I sent the email and Jesper Pedersen\nsuggested to remove the complete directory and \ndo a new checkout to cleanup stale files. \nThat did the trick.  \n(The standard \"make clean\" I do regularly hadn't helped.)\n\nThanks to him for the quick and competent help. \n\n"
    author: "cm"
  - subject: "Re: Anyone else seeing compilation errors?"
    date: 2003-12-08
    body: "Well you are probably right when you say that I was being a bit\nunfair.  It was late, I was frustrated, and I appologixe.\n\nLets just say its obviously not yet time for ME to be\nbeta testing since it won't build on my machine.  Not the CSV or\nthe released version.\n\nI note that others are having problems with the \nusermake issue (on the mailing list).  To them you suggested\n(as you did to me) that there is a time stamp problem, and on\nthe mailing list you said they should check their system clock.\n\nMy clock is set with ntp to one of the US Government clocks\nso I'm pretty sure thats not the problem.\n\nWhy would a time stamp cause the make process to try to\ninvoke usermake when usermake is not even installed on my\nsystem.  (Indeed, SuSE has never included it).  My system\nis SuSE 8.2, running a late smp kernel, and kde 3.1.4 and\nreciently updated source tree for same.\n\nI also note that after running ./configure  and waiting for that\nto get done, (successfully) I start make, and it goes thru configure\nagain.  That may have been your intent, but I've never seen it with\nother packages I've built from source.\n"
    author: "jsa"
  - subject: "Re: Anyone else seeing compilation errors?"
    date: 2003-12-08
    body: "As I said, it was not intended to release with unsermake enabled.\n\nI'm currently at a company meeting where we work 18 hours a day, so please allow me till Wednesday, where I'll release a 1.0.1 without unsermake, and with bugfixes for two crashes reported.\n\nCheers\nJesper"
    author: "Jesper Pedersen"
  - subject: "Lossless jpeg rotation? "
    date: 2003-12-04
    body: "Looks pretty cool. One feature that I must have in such a program is the ability to quickly select and (losslessly) rotate pictures. Reading through the documentation I saw a picture that suggests you can rotate for viewing, but it wasn't clear if this rotated the actual picture. \n\nLooks like a cool App! I'll download and play with it tonight.\n\nJohn\n"
    author: "Anony Guy"
  - subject: "Re: Lossless jpeg rotation? "
    date: 2003-12-05
    body: "No modifications are done to the images at all.\nYou can store in the database that the image should be rotated, but it will never be written to the actual image.\n\nThats a feature not a bug ;-)"
    author: "Jesper Pedersen"
  - subject: "Re: Lossless jpeg rotation? "
    date: 2003-12-05
    body: "Yeah, I understand where you are coming from...not quite the intended use of your tool.\n\n\nDoes anyone know of a good KDE tool for doing this sort of thing? Right now the best thing I've found is some clunky X program (maybe GTK based? maybe not) that mandrake includes.\n"
    author: "Anony Guy"
  - subject: "Re: Lossless jpeg rotation? "
    date: 2003-12-05
    body: "Maybe the Gwenview image viewer can be what you're looking for.\n"
    author: "Lubos Lunak"
  - subject: "Re: Lossless jpeg rotation? "
    date: 2003-12-05
    body: "digikam 0.6.0-CVS has a lossless-JPEG-plugin\n\nhttp://digikam.sourceforge.net/plugins.html#p8\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
---
After exactly one year of coding, several months of bothering people with
demos, and 2 long holidays (also used for coding), I've finally gotten my act
together enough to make <a href="http://ktown.kde.org/kimdaba/">a public release of KimDaBa</a>.  If you have a large pile of digital images and need a sane solution for managing them, KimDaBa could well be the answer to your prayers.
<!--break-->
<p>When I got my first digital camera a year ago, several image managers
already existed, so I had a hard time choosing the right one. It was especially
hard because I had serious doubts that any of the existing applications would
scale up very well, up to say, tens of thousands of images with hundreds of
new images coming in each month.</p>

<p>Taking my ignorance to the limit,  I decided to develop the desired
application myself.  Suffice it to say, if you have a large pile of digital images, and cannot answer yes to
all of these points, then be sure to take a look at  <a
href="http://ktown.kde.org/kimdaba/">KimDaBa</a>:
<ul>
  <li>It's easy to index your images -- <i>"Label these 10 images as pictures of my girlfriend."</i></li>
  <li>Within a few seconds you can find a given image you have in mind --
<i>"Locate that image of my girlfriend from the holiday on Mallorca
in 1998"</i>, or <i>"Get me that photo with both my girlfriend and my
father."</i></li>
  <li>You have all the tools needed to set up a slide show of a given
subset of your images.</li>
  <li>When browsing through your images, it's easy to switch category -- <i>One
moment you are looking at images of your girlfriend, the next you are
looking at images from your trip to Mallorca.</i></li>
  <li>It's easy to make your images available on the net in different
resolutions -- <i>Make it easy for your mother on a 32Kb modem to see
them in the right resolution, and for your brother on a 2Mb line to get
images in a resolution high enough for printing.</i></li>
</ul>
</p>