---
title: "KOffice 1.3 Beta 4 Released"
date:    2003-09-25
authors:
  - "binner"
slug:    koffice-13-beta-4-released
comments:
  - subject: "rtf"
    date: 2003-09-25
    body: "A lot of work on the rtf filters . gj."
    author: "anon"
  - subject: "Re: rtf"
    date: 2003-09-26
    body: "Well, I consider it to be the priority, as RTF (1.5) is a possible exchange format with many word processors. So any new feature here is good for many people.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Tereza rocks! ..."
    date: 2003-09-25
    body: "...but why this name? (I'll opt to name the final one \"Sylvia\" then. Coolo, any chance?)\n\nAnyway, I have found a few real gems in the pre-Tereza builds.\n\nThanks a lot to all those who contributed!"
    author: "Kurt Pfeifle"
  - subject: "Qt-3.2.1"
    date: 2003-09-26
    body: "Has anyone managed to build this with Qt-3.2.1.\n\nI consistently get this error:\n\nundefined reference to `QString::setNum(long long, int)\n\nwhen it links and the command contains:\n\n-L/usr/local/qt-3.2.1/lib\n\n???\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Qt-3.2.1"
    date: 2003-09-27
    body: "This is a subtle problem.\n\nGoogle came up with the same thing with Scribus:\n\nhttp://nashi.altmuehlnet.de/pipermail/scribus/2003-September/002103.html\n\nI thought that I had rebuilt/relinked KDE atainst Qt-3.2.1, but somehow it didn't work -- I found that there were still KDE library files linked against Qt-3.1.2.  KDE didn't complaint about this, it appeared to work fine.  But, KOffice would not build against the mixed up libraries.\n\nAfter relinking KDE (again) against Qt-3.2.1, KOffice now appears to be building OK.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "KOffice development"
    date: 2003-09-26
    body: "Please somebody enlighten me... The news I read after Nove Hrady sounded like there was going to be a complete rewrite of some core KOffice components alongside with the switch to the OO.o file format. Is work on this already going on? How is Beta4 related to this?\n\nBut I'd like to rant a bit about KSpread:\n\nLast time I checked KSpread I was totally disappointed. I had a big .csv file (about 20 x 50.000) and OO.o Calc was so damn slow, I thought I'd give KSpread as a more lightweight alternative a try. Well, KSpread was not slow... after 5 minutes or so it had completed loading the file and then nearely freezed. I can't wait 20 seconds for a simple scrolling... (1 GHz Athlon, 256MB). But I did not give up, xkilled Kspread and opened a smaller .csv file (20 x 5.000) just to draw some simple charts. It was merely impossible to draw a chart from x, y1, y2, y3... data. The charting component of KOffice just does not fit well into KSpread. I think I'll give Gnumeric a try and see if it's better at dealing with large spreadsheets and drawing charts...\n\nFor now I ended up using some bash-scripting and Gnuplot.\n\nBtw.: Excel works perfectly. It takes big files without a burp. It's responsive. Its charting capabilities are 5 times better than the ones from OO.o Calc and there's just no comparison to KSpread/KChart..."
    author: "Thomas"
  - subject: "Re: KOffice development"
    date: 2003-09-26
    body: "> The news I read after Nove Hrady sounded like there was going to be a complete rewrite of some core KOffice components alongside with the switch to the OO.o file format. Is work on this already going on? How is Beta4 related to this?\n\nRead the news again, it was about \"future of KOffice after the next major release\" - in other words about KOffice 1.4/2.0.\n "
    author: "Anonymous"
  - subject: "Re: KOffice development"
    date: 2003-09-26
    body: "Then help development. Whining and complaining about things people do in their free time is always easy...\nThe KSpread problem you mention is a design problem from the very beginning. Read the KSpread future dot news to more info - this should be fixed with the next KOffice - unless everybody just complains but nobody starts working..."
    author: "somebody"
  - subject: "Re: KOffice development"
    date: 2003-09-26
    body: "The major changes planned at Nove Hrady are for KOffice 1.4, as KOffice 1.3 should already been out, if there was not such a manpower problem. (RC 1 delayed for the second time now.)\n\nSo no, no work to switch on the OO formats is being done and, no, Beta 4 has not any change of that kind.\n\nHave a nice day!\n\nPS.: of course, volunteers are welcomed!"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice development"
    date: 2003-09-26
    body: "sorry for the harsh rant... I'm very busy finishing my exams right now. So no spare time left for other things like looking at KSpread sources..."
    author: "Thomas"
  - subject: "Re: KOffice development"
    date: 2003-09-26
    body: "You're right that csv, if anything, should work well."
    author: "anonymous"
  - subject: "Just to get it right"
    date: 2003-09-28
    body: "After compiling Beta 4 I'm especially impressed with KWord\nKudos to the developers.... KWord is really cool, and finally handling tables is working!\nGreat! Thank you, guys!\nIs there a howto available about scripting KOffice? Does KOffice use KJSEmbed?"
    author: "Thomas"
  - subject: "Re: Just to get it right"
    date: 2003-09-29
    body: "As far as I know there is no document about scripting KOffice.\n\nKOffice can be scripted by DCOP. So you can use your prefered language to script it.\n\nAs for KJSEmbed, KOffice do not use it. (Kexi uses QSA.) However if you can use DCOP with it, you can script KOffice with it. (Sorry, but I do not know more of KJSEmbed than its name.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
On September 25th 2003, the KDE Project released the <a href="http://www.koffice.org/releases/1.3beta4-release.php">fourth beta version</a> of <a href="http://www.koffice.org/">KOffice 1.3</a>. This release sports tons of bugfixes made during the <a href="http://events.kde.org/info/kastle/">KDE developers' conference</a> in Nov&eacute; Hrady, Czech Republic (see <a href="http://dot.kde.org/1061919133/">KOffice Developers' Meeting Report</a>). It is the last beta in the 1.3 series according to the revised <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">KOffice 1.3 release schedule</a> which plans the final release now for November 12, 2003. Read more in the <a href="http://www.koffice.org/releases/1.3beta4-release.php">KOffice 1.3 Beta 4 release notes</a> and in the <a href="http://www.koffice.org/announcements/changelog-1.3beta4.php">KOffice 1.3 Beta 4 changelog</a>. For now you can only <a href="http://download.kde.org/unstable/koffice-1.2.93/src/">grab the source</a>. Binaries, if any will be provided, will appear <a href="http://download.kde.org/unstable/koffice-1.2.93/">one directory above</a>.
 

 
<!--break-->
