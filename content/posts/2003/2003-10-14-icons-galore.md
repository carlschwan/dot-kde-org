---
title: "Icons Galore "
date:    2003-10-14
authors:
  - "Alex"
slug:    icons-galore
comments:
  - subject: "my favorite - Metrope"
    date: 2003-10-14
    body: "Not finished, but shows real promise and doesn't look like all the other themes. Perhaps a little BeOSy, but that is a good thing. :)\n\nhttp://www.kde-look.org/content/show.php?content=3968"
    author: "anonon"
  - subject: "Impressive"
    date: 2003-10-14
    body: "... all of them ! Kudos to those guys, as the graphic artists get way too few respect for their hard work !"
    author: "DarkDust"
  - subject: "What software tools do these artists use?"
    date: 2003-10-14
    body: "The only GPL tools that I have seen for SVG icon making \nare Sodipod and Karbon, neither of which seem to be \nsufficiently developed yet to be stable enough.\n\nI need to make some icons of my own at some point but \nhave not yet worked out which software is the most\nappropriate. Any advice?\n"
    author: "nic"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-14
    body: "I think Sodipodi is better then Karbon14. Are there at the moment any developers working on Karbon14?"
    author: "MaX"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-14
    body: "Yeah, right now the karbon developers are working on large architectural stuff (kpainter), than can be reused in other apps (they already have in ksvg)\n\nI think karbon in the next major version of koffice (after 1.3), will be much more feature complete. \n\n"
    author: "anon"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-14
    body: "I am prety sure that Everaldo and Tackat are useing Adobe Illustrator. Adobe and Macromedia Programs are stillt very good even if they are not Open Source. Unfortunatly they are very expensive too.\n"
    author: "Calle"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-15
    body: "Graphic software and music software is a real problem on Linux. So I really wonder why the commercial programs are not ported.\n\nFor instance: I think there is a strong need for a KIMP program. However nobody started this project yet. The solution to the problem is the OSS development which is not driven by the needs of users but by the capabilities of programmers. So commmercial software for users is needed."
    author: "Gerd"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-15
    body: "Hi,\n\nwhy do you believe that there is such a strong need for \"KIMP\"? The fact that nobody is investing time just to make it use Qt buttons/dialogs instead of GTK ones is prove enough that it doesn't change GIMP usabilty one single bit.\n\nGimp runs fine under KDE, to my knowledge outdoes Photoshop in many aspects and it a very old software project. GTK is founded on Gimp's own toolkit. Gimp had good enough interfaces long before KDE began.\n\nAs long as I can use Gimp under KDE, WTF is the problem with it? I really don't understand why you people want others to waste time on porting Gnome/GTK software to KDE. I rather suggest to make GTK, QT and KDE comply with freedesktop standards and then it doesn't matter much anymore.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-15
    body: "There are quite a few efforts to bring paint programs to KDE, whether you like it or not.  Unfortunately for you, I don't think there's anything you can do to stop the demand and desire."
    author: "ac"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-17
    body: "I agree, that would be a waste of time. I would much rather work on imrpoving the GIMP rather than making it use pretty buttons, which can be done through themes."
    author: "Alex"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-15
    body: "\n1. install Microsoft Windows, \n\n2. install Adobe Illustrator for Microsoft Windows, \n\n3. copy design & style of icons from numerous MacOSX icon sites,\n\n4. and you have a chance to get your icon set into KDE 3.3.\n\nCheers"
    author: "wygiwyg"
  - subject: "Re: What software tools do these artists use?"
    date: 2003-10-17
    body: "You dont have to install Microsoft Windows. Illustrator and Photoshop run quite well with Wine."
    author: "Calle"
  - subject: "Gnome-KDE-icon themes"
    date: 2003-10-14
    body: "Wasn't KDe- ang Gnome- icon-themes supposed to be merged? As far as I remember, they implemented some common standard, but they never standardized the names of icons that are the same in KDE and Gnome. This means that both KDE- and Gnome-iconthemes show up in KDE 3.2CVS when choosing icons, but choosing one of the Gnome-themes doesn't really work. Shouldn't that be fixed for 3.2 final?"
    author: "Johannes Wilm"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-10-14
    body: "Yeah, the gnome project switched over to a kde-format of icon themes around gnome 2.2, but the projects never agreed on common names. "
    author: "anon"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-10-14
    body: ">Yeah, the gnome project switched over to a kde-format of icon themes around\n>gnome 2.2, but the projects never agreed on common names\n\nso what does that mean???\n1. they switched over...\n2. no common names...\n\nthat sounds like there is no possibility to use both icon-sets (kde/gnome) in each environments???"
    author: "Foobar"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-10-14
    body: "It seems to me that mimetype.png and appname.png (where appname is a match of the actual binary or script) would be best.  Use lots of symbolic links, and make mimetypes \"fall back\" to the main category.  I.e., image.png, image-jpeg.png and image-bmp.png.  If a image-gif file is read, display image.png.\n\nIs it just me, or does KDE's icon layout come from BeOS?  Maybe it's just at a glance - I haven't used BeOS much, and I'm guessing its layout from the icon archives I've downloaded."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-10-14
    body: "Well as far as I remember the problem was just figuring out which icons corresponded to oneanother (e.G. the icon with the \"go-left\"-icon, the \"stop loading\"-icon, etc.). But a list convering the icons must already exist for the Gorilla-developers who make icon-themes for both KDE and Gnome.\nSo why not just take that list, and switch to using the Gnome-names in KDE? There shouldn't be much pride connected with those names, and as Gnome doesn't seem to have been able to handle the switch, let KDE do it. In exchange one could maybe ask Gnome to do something else that helps standardization."
    author: "Johannes Wilm"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-10-14
    body: "Why just make symlinks?  KDE shouldn't break backwards compatibility with all the icon sets."
    author: "ac"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-10-14
    body: "Well, I believe KDe3.2 breaks compatibility with window decorations anyway, and what about widgets that weren't present in KDE before 3.2?\nAnd as it should be easz to make a small script that runs through an icon theme and renames all icons in a matter of milli-seconds, it shouldn't be too hard to update all current icon themes to run on KDE3.2 .\nOr am I mistaken?\nBut I do agree, symlinks should be employed imemdiatly! In fact, one should put them in all icon themes that are to be put into KDE3.2 and the Gnome-developers should be asked to do the same thng with the next x.x.1-update of their icon sets. Right now it just looks broken when one can choose anmong 5-10 icon themes of which half seem to be broken just because they belong to the other DE. The most helpful tool would probablz be somethng like a small python script and a list of icon-names that correspond which easily make an icon-theme that is both KDE- and Gnome-compatible out of anz KDE- or Gnome-icon-theme."
    author: "Johannes Wilm"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-10-14
    body: "Johannes: when can we expect this python script and some desktop\nscreenshots from you, showing that symlinking works?\n"
    author: "Richard Bos"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-11-22
    body: "A user called Chroma who is working on a Gnome port of the Crystal SVG set has created such a Python script unknowingly of this thread.  The script can be used to port any KDE icon theme to Gnome.  It is still alpha/beta but easily usable in it's current state.  A new thread will be created soley for the Python script, but for now the following link goes to the thread for the Gnome Crystal SVG project.\n\nhttp://www.linuxcult.com/forum/showthread.php?s=&postid=15443"
    author: "Exdaix"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-11-22
    body: "Be sure to contribute it to freedesktop.org."
    author: "Datschge"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2003-11-22
    body: "When it's polished over (within a few days), no problem.  We'd be glad to do that."
    author: "Exdaix"
  - subject: "Re: Gnome-KDE-icon themes"
    date: 2007-12-03
    body: "FLOB - no such script - dead link"
    author: "yehrite"
  - subject: "if you use KDE CVS..."
    date: 2003-10-14
    body: "...and want to install Crystal SVG 1.0, I found that I had to modify the index.desktop file.  The name of the theme was set to \"Crystal SVG beta1\", which collides with the default icon theme from CVS.  So installing the new icon theme makes kconfig list \"Crystal SVG beta1\" twice, but selecting either of those activates the \"old\" theme from CVS.  There's no way to activate the new theme.  I changed the file \"~/.kde/share/icons/crystal/index.dektop\" so the name of the new set is \"Crystal SVG 1.0\", and now it works (and look fantastic!).  I suppose I could have just installed the new theme as root in $KDEDIR/share/icons/.\n\nJust a heads up in case others have the same issue."
    author: "LMCBoy"
  - subject: "SVG? I can only find the PNG's..."
    date: 2003-10-14
    body: "...is there a distribution in SVG out there?\n\n(so i can fiddle around with it bit in illustrator)\n\na link, anybody?"
    author: "cies"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-14
    body: "look in the scalable directory"
    author: "anon"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-14
    body: "The \"Crystal SVG\" tarball from www.linuxcult.com \n\nhttp://linuxcult.com/forum/crystal/Crystal_SVG.tar.gz\n\ndoes not include a scalable directory. It has svg in its name, but does not contain any svg icons (just png).. As this is the \"official\" location to download the Crystal SVG icons... maybe there's a separate svg-version available?"
    author: "Thomas"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-15
    body: "YOU'NOT RIGHT!\n\nthere is _NOT_A_SINGLE_SVG_FILE in the file you're pointing at...\n\nI would really like to download the SVG files!\nBut i can't find 'm (when i do i post a link here ;-)\n\nBTW are the Crystal SVG icons under (L)GPL?? "
    author: "cies"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-15
    body: "kdelibs/pics/crystalsvg/*.svgz and yes."
    author: "ac"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-16
    body: "http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/pics/crystalsvg/\n\nwebcvs gives 'some' errors... downloading cvs-tools now."
    author: "cies"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-16
    body: "Dear AC,\n\nthanks for the link (kdelibs/pics/crystalsvg/*.svgz) but\nthere are only a few (~100) svg icons in there... not the few hundred that i had expected.\n\nI wonder if it other icons of the crystalsvg theme are: 1) not in svg-format (yet) of 2) somewhere in else...\n\ncheers,\ncies."
    author: "cies"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-16
    body: "> BTW are the Crystal SVG icons under (L)GPL??\n\nyes, as with anything in kdelibs. it's LGPL.\n\n> (when i do i post a link here ;-)\n\nftp://ftp.kde.org/pub/kde/unstable/3.1.92/src/kdelibs-3.1.92.tar.bz2\n\nCVS has the newest files. but it's in there in the pics directory. They are distributed as compressed svg files (svgz)"
    author: "anon"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2003-10-16
    body: "Thanks for the link anon!\n\nbut in this file: ftp://ftp.kde.org/pub/kde/unstable/3.1.92/src/kdelibs-3.1.92.tar.bz2, i find only a small amount of svgz files (~100)... Im still looking for all of them (i was told it consists of more then 400, here: http://www.linuxcult.com/?m=show&id=49)\n\nif i download the \"Crystal SVG\" icon theme (on this site: http://www.linuxcult.com/?m=show&id=49) i get only PNG files (but definitly consists of more than 400).\n\nWhat to do (i gues i'll be emailing everaldo then....)\n\ncheers,\ncies.\n\n \n"
    author: "cies"
  - subject: "Re: SVG? I can only find the PNG's..."
    date: 2006-12-26
    body: "Crystal svg is a very incomplete theme and it contains a lot of icons from the original crystal icon set that dont have svg source\n\nAlso a lot of the links from this page are dead, could someone post a new link to the svg source."
    author: "RossyMiles"
  - subject: "Hmm"
    date: 2003-10-14
    body: "Will Crystal SVG be included as the default icon theme in KDE 3.2?"
    author: "Eike Hein"
  - subject: "Re: Hmm"
    date: 2003-10-14
    body: "yes"
    author: "anon"
  - subject: "Noia"
    date: 2003-10-14
    body: "The Noia theme is becomeing extremely popular and  I do find it somewhat attractive. But, I can't shake the thought of Dr. Seuss when I see the Noia icons.\n\n\nI do not like green eggs and ham. For, Sam I am."
    author: "Isa Secret"
  - subject: "Icon Licence"
    date: 2003-10-14
    body: "I tried to find the licence of icons used by KDE but could not find it. It seems that it is allowed to use them for GPLed software but it is unclear to me whether they can be used by commercial software.\n\nCan anybody help me what the matter is or point me somewhere where I can find the licence under which the icons can be copied, changed and distributed?\n\nThanks\nGuenther"
    author: "guenther.palfinger"
  - subject: "Re: Icon Licence"
    date: 2003-10-14
    body: "> I tried to find the licence of icons used by KDE but could not find it.\n\n__everything__ shipped by KDE in kdelibs is LGPL. It's a requirement for being in kdelibs. So, at least crystal and hicolor are LGPL.\n\nOh yeah, I beleive noia is LGPL too.\n\nYou can use the above in commercial apps. "
    author: "anon"
  - subject: "Re: Icon Licence"
    date: 2003-10-14
    body: "Thank you very much. I didn't know that they are shipped in kdelibs (I assumed it was kdeartworks).\n\nG\u00fcnther"
    author: "guenther.palfinger"
  - subject: "Sorry"
    date: 2003-10-14
    body: "\"Norbalin released version 2 of the Umicons set extending it to over 700 icons, as well as a number of fixes here and there.\"\n\nI meant to say over 2,500 icons!"
    author: "Alex"
  - subject: "wow !!!"
    date: 2003-10-15
    body: "So beautiful....\n\nnever notified those (2,3,4)\nI will sure download all three of them as soon as I get home... (2,3,4)\n\nSomekool\n\ngood job people...\n\n"
    author: "somekool"
  - subject: "konqueror spinning %&$\"! replacement"
    date: 2003-10-15
    body: "indeed,i love crystal.\n\nbut this %&$\"! spinning thing in the konqueror is ugly!\n\nthis one is much better!\n\nhttp://www.kde-look.org/content/show.php?content=2140\n\njust copy the kde.png in /usr/kde/3.1/share/icons/crystal [your crystal folder]/22x22/actions\n\nit is autoscaled by konqueror.\n\nhave fun.\n\ngunnar\n "
    author: "gunnar"
  - subject: "Re: konqueror spinning %&$\"! replacement"
    date: 2003-10-15
    body: "thanks! much better than the default. You've more tips like that?"
    author: "Thomas"
  - subject: "Re: konqueror spinning %&$\"! replacement"
    date: 2003-10-16
    body: "I like Amibug's a LOT better too."
    author: "Alex"
  - subject: "Suse specific icons..."
    date: 2003-10-16
    body: "Regarding Crystal SVG...\n\nI was curious as to why there are give or take 87 Suse icons in the package, shouldn't this be distro-neutral rather than all the suse stuff?\n\nbtw crystal SVG is very beautiful, all i need now is a bigger monitor :)"
    author: "CMF"
  - subject: "Re: Suse specific icons..."
    date: 2003-10-16
    body: "Because Crystal development is sponsored by and used for SUSE?"
    author: "Fred"
  - subject: "Re: Suse specific icons..."
    date: 2003-10-16
    body: "Yeah, i realise that, but do the Suse icons need to be in there? They will only get used by suse users (which will include them in the distro)... i assume the version released with KDE 3.2 will not contain the suse icons."
    author: "CMF"
  - subject: "Re: Suse specific icons..."
    date: 2003-10-16
    body: "You're complaining because SuSE is releasing its artwork under Open Source?  Not even Ximian does that.  You have to charge big bucks for the privilege."
    author: "ac"
  - subject: "Re: Suse specific icons..."
    date: 2006-03-04
    body: "Jeez, Novell just can't do anything right, can they. People complain if they don't make things open source, people complain if they do. (apparently.) \nIf you don't like them, don't use them. Enough said."
    author: "STuPiDiCuS"
  - subject: "How about new Quanta icons"
    date: 2003-10-16
    body: "I keep waiting for one of our great professional looking icon developers to turn their attention to Quanta. If it's in one of the many packages sorry I missed it. I load up KDE 3.2 CVS and I have kwrite and kcalc on my kicker panel... they have nice new icons. Actually Quanta's looks closer now than it did on 3.1, but I still don't know what it takes to get some of the talented effort focused on us. We're part of KDE, but I've been begging and pleading where artists hang out and all I get is a pile of splash screens.\n\nWe need a new application icon and a pile of general use icons for toolbars. We're better developers than icon artists. I don't know when our icon gets to look decent on kicker though..."
    author: "Eric Laffoon"
  - subject: "I still really like Lo-color"
    date: 2003-10-17
    body: "I really liked the Lo-color icon theme. It doesn't seem to be complete in newer KDE releases though :(\nDoes anyone know it will be included in KDE 3.2 or if anything similar will be included?"
    author: "Z_God"
  - subject: "Re: I still really like Lo-color"
    date: 2003-10-18
    body: "> Does anyone know it will be included in KDE 3.2\n\nYup, it's still there. Too bad the artists are having enough trouble keeping crystal and hicolor complete enough to work on completing locolor. As always, help is welcome."
    author: "anon"
  - subject: "Icons"
    date: 2003-11-05
    body: "Can anyone please give me Icons for Works and Money 2004/2003?"
    author: "Norm"
  - subject: "Icons"
    date: 2006-03-03
    body: "Has anyone checked out www.titanicons.com?  Just wondering who they are, they seem to have some cool icons.\n\nYoshi"
    author: "Yoshi "
---
In the last few weeks fans of users the K Desktop Environment have been treated to a shipload of spectacular <a href="http://www.kde-look.org/index.php?xcontentmode=iconsall">icon sets</a> from well known and talented artists. Go ahead, liven up your desktop, there is bound to be a style that's right for you!
<!--break-->
<p>
Here are a few of the many new or updated icon sets over the last couple of weeks.
<br></br>
1. <a href="http://www.kde-look.org/content/show.php?content=8341">Crystal SVG </a>
<br></br>
Everaldo has impressed us all once again with the SVG (scalable) version of his icons. But, don't let the name fool you, Crystal SVG and Crystal GT differ in a lot more than merely file formats, Crystal SVG cleans up and adds many new icons giving it a much cleaner feel.
<br></br>
2. <a href="http://www.kde-look.org/content/show.php?content=3883">Noia 1.0</a>
<br></br>
Carlitus updated his Noia icon set as well, now with many cleanups and quite a few new icons too. In the future, he plans to release the source of the icons so the set can grow at a faster rate and  variants of Noia in natural and warm styles.
<br></br>
3. <a href="http://www.kde-look.org/content/show.php?content=7214">Umicons 2.0  </a>
<br></br>
Norbalin released version 2 of the Umicons set extending it to over 700 icons, as well as a number of fixes here and there. 
<br></br>
4. <a href="http://www.kde-look.org/content/show.php?content=5358">Nuvola 0.2.5 </a>
<br></br>
Nuvola is a very attractive SVG (scalable) icon set created by Dave (which is also the maker of Lush, Sky, and Desktop) and inspired by Crystal.
<br></br>
5. <a href="http://www.kde-look.org/content/show.php?content=7264">Korilla 1.3  </a>
<br></br>
Korilla is a rework for KDE of Jimmac's popular Gorilla theme with blue colors. This port has been done mostly by SystemX.
<br></br>
Please check out all of these fantastic icon sets, they are all worth a look and quite comprehensive. Also, something to look forward to if you still didn't quite find your style  is Slick 1.5, Amibug has gotten back in the game and is working diligently on Slick 1.5.