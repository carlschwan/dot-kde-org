---
title: "KDE on FreeBSD: Alan Eldridge (1961-2003)"
date:    2003-06-09
authors:
  - "afawcett"
slug:    kde-freebsd-alan-eldridge-1961-2003
comments:
  - subject: ":("
    date: 2003-06-09
    body: ":("
    author: "John Tapsell"
  - subject: ":("
    date: 2003-06-09
    body: "So sad :("
    author: "Joergen Ramskov"
  - subject: "All respect"
    date: 2003-06-09
    body: "All respect for the good work."
    author: "Debian User"
  - subject: ":("
    date: 2003-06-09
    body: ":("
    author: "Marc Tespe"
  - subject: ":("
    date: 2003-06-09
    body: "I'm shocked-- I've been using the KDE freebsd packages for years, and I've talked to Alan a few times over IRC. It's quite sad how something as virtual as irc can translate to real life. \n\nCondolences to his family and his friends. I'm sure he'll be missed among KDE circles."
    author: "lit"
  - subject: ":("
    date: 2003-06-09
    body: "Sad news :(. Well, at least he did something very worthwhile in his lifetime - he contributed to a part of the global, combined knowledge pool of mankind: open-source software. I think that deserves a lot of respect, especially in these times. Thanks, Alan. You'll be missed."
    author: "Eike Hein"
  - subject: "R.I.P."
    date: 2003-06-09
    body: "It's sad to read this. I only exchanged two or three mails regarding Quanta's inclusion in FreeBSD, so I don't really know him. But he was very receptive to other's problems, prompt on repsonse, helpful, so I can only say that he was a good man. And as I read in his signature, he was very young. R.I.P. Alan.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: R.I.P."
    date: 2003-06-10
    body: "I too was saddened to read this and struck by the fact that he was several years younger than me. Such news hits me that much harder since I was so ill a few weeks ago that my doctor had told me my immune system was on the virge of collapse and I was facing a 20% mortality rate. I've long believed that how you are remembered and by how many is the true measure of a successful life... but ever since I lost mom I've been painfully aware that nothing can compensate for the sorrow of losing a loved one before they have reached a ripe old age. My condolences and best wishes to Alan's family and friends."
    author: "Eric Laffoon"
  - subject: ":( "
    date: 2003-06-09
    body: "While i didnt know him personally, his work will be missed. My wishes go out to his family."
    author: "Ziggy"
  - subject: "my sympathy"
    date: 2003-06-10
    body: "i never had a chance to speak to alan, but i wish to offer my condolences to his family at this sad time. from others comments he seemed to be a great guy, and i'm sure he will be missed by many."
    author: "lrandall"
  - subject: "Is there anybody else in the denver area..."
    date: 2003-06-10
    body: "who is unemployed and a coder of *nix?\nPlease send me your resume.\nI am aware of some positions starting to open up.\nI only wish that I had heard of alan's plight earlier.\n\nrraabATwindbournetech.com\n\n"
    author: "a.c."
  - subject: ":("
    date: 2003-06-10
    body: "nur die besten sterben jung..."
    author: ":("
  - subject: "an unremitting disease like and unlike any other"
    date: 2003-06-10
    body: "This is such a terrible tragedy. I wish I could have met you Alan.\n\nI just read the \"In Memoriam\" page. I was wondering if any of you know that he was bipolar or if he and his family did for that matter. I did not know of the man until today so I suppose I can't say this for certain, but after reading the comments on that page I can tell you that if what I read was _any_ indication whatsoever of his person, than he was bipolar (or bipolar 2, slight difference). The comments about him and the story of his last year or two fit the pattern of Bipolar Disorder perfectly. I do not see such a classic case often. It is telling how many mentions of his \"acerbic\" personality and how he could at times be an asshole (my wording) there were but also how the same people would talk about how wonderful he was in a good mood or just not being a jerk. I think if I were to die tomorrow and such a page was put up it would look much the same. There are plenty other examples of this sort of thing on that page; in fact, everything I read on it pointed towards bipolarity. I am not a Pdoc (Short-hand for psychiatrist or psychologist among people who have them; usually the prescriber of meds (psychiatrist) is the \"Pdoc\" and the one that talks to you about life (psychologist) is the \"counselor\" but it can be used to refer to both. Yeah, I speak four languages: English, German, hacker/geek, and CRAZY.) so take this as you will, but I've got some experience in the manner; I'd put money on this one. If anyone knows different, speak up.\n\nSome of you wondered how he was able to bring himself to commit suicide. As a fellow sufferer of this devastating mental illness I can only force a sad smile, put my head in my hands and think, \"Oh, you have noooo idea...\" I read somewhere (read: I forget my source and it was probably online somewhere, so get your salt out) that we have a suicide rate five times higher than people with unipolar depression. Some of you also reported shock and just didn't understand how someone could strongly consider suicide much less carry it out. Reading what was said about him I am not shocked, just very saddened (and a bit frightened). _I_ understand. Sometimes it makes sense; in a severe episode reasoning (emotionally, not for something like coding, which is probably a reason he took on so many thorny projects, you've no idea how therapeutic delving into something absolutely horrific that requires all your focus and logical thinking can be to a logical thinker) is the first thing to go.\n\nBipolar Disorder is often not caught in time because it is under diagnosed and misdiagnosed frequently (usually as depression, HUGE difference, especially for medication). I won't go into the myriad reasons for this but I do think that because it can end so tragically that more people should read up on the disease and learn to recognize the signs. I'm going to include a link at the bottom of this for anyone who is bipolar or gets depressed but has another side to their mood imbalances (you might be bipolar, find out), it's a site run by bipolars (as we sometimes refer to ourselves) for bipolars and offers support and help among other things. It's been really good for me just to be able to talk with people who go through the same mental hell. If anyone else would like info on BP, just google for it or mental illnesses in general (if you're interested or curious _you_ ought to be able to find the motivation to do so :), that link is for one of us whom, during an episode, could not do so :(, something like this is a perfect trigger for an episode btw). I was lucky, they were literally seconds away from putting me on an anti-depressant that would have eventually rendered me completely insane, doomed to a permanent and untreatable manic episode (wouldn't have taken all that long, but not right away). Then someone brought up the possibility of BP, prescription was delayed, an evaluation was done over the next three weeks and sure enough, I was BP. Now I am seeing both a counselor and a Pdoc and am medicated, I'm not sure how long I would have lasted if this hadn't been caught. Please try not to let anyone else you know slip through the cracks like this; knowledge is power, arm yourself.\n\nThe site I mentioned for bipolars like myself is:\n\n<HR>http://bipolarworld.net</HR>\n\nIt has a message board for support and a lot of bipolar writers have columns there about life with the disease. You are not alone. We are not alone. Please let Alan Eldridge be a warning to you. AlanE, I feel your pain, wish it could have been different. My deepest condolences go out to the Eldridge family.\n\nFor the record: any spelling errors in this are due to the fact that I'm typing with one hand because I broke the other one in a rage over _losing a silly GAME_, just thought it was an interesting coincidence."
    author: "someone who _does_ understand"
  - subject: "Re: an unremitting disease like and unlike any other"
    date: 2003-06-10
    body: "Oops, what the hell did I do to that link? My bad guys, go there anyway if you need to."
    author: "someone who _does_ understand"
  - subject: "Re: an unremitting disease like and unlike any other"
    date: 2003-06-10
    body: "Don't worry, you got the message across.  Thanks."
    author: "anon"
  - subject: "Re: Bipolar disorder"
    date: 2003-06-11
    body: "Alan, you are not alone.\nI lost my brother to Bipolar Disorder in 1981.\nHe took his own life.  He was 21 and an wonderful artist.\n\nIt took me many years to understand it, not being a sufferer myself,\nbut now I regret that Paul was not diagnosed and treated, although\nI could not have helped him myself, I would give anything to have him back.\n\nI understand that there are meds and therapies that can help now, and more\nrecently I have met sufferers who have been helped.\n\nTo all who suffer severe mood swings out there, please take the time\nto research this and find out what is going on with you.\n\nYou owe it to your family as well as to yourself.\nThe incidence of death is higher than you think, and the likelyhood\nof suicide is great.  Don't kid yourself that you can deal with it.\nIt's an illness, and while there is not yet a cure, there is help.\nIt's not your fault.\nYou don't have to suffer, and you don't have to go it alone.\n\nHow I would love for Paul to see my life now.\nPaul, Alan, wherever you are, we miss you.\n\n"
    author: "Reindeer"
---
It is with sorrow that I have to inform you of the death of Alan Eldridge this weekend.
Alan, or 'alane' as he was known within the Open Source community, was a core member of the team which packages <a href="http://freebsd.kde.org/">KDE for FreeBSD</a>, and a respected member of the Open Source community outside of KDE.

Those of us who worked with him on KDE for FreeBSD have put together a page to honour the work he did with us, and our memories of him. You can see it 
<a href="http://freebsd.kde.org/memoriam/alane.php">at the In Memoriam page</a>.

He certainly won't be forgotten.
<!--break-->
