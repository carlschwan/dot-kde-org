---
title: "ContentPeople: Review of Quanta Plus"
date:    2003-06-11
authors:
  - "numanee"
slug:    contentpeople-review-quanta-plus
comments:
  - subject: "LAMP"
    date: 2003-06-10
    body: "LAMP plus Quanta is a great combination indeed. \n\nI personally use vim for web scripting though. It's a habit you know-- I've been using vim since 1995. \n\nHowever, I think Quanta will be a huge potential force in converting people from Windows, especially once a split code/WYSIWYG mode is stabilized and put in."
    author: "lit"
  - subject: "Re: LAMP"
    date: 2003-06-10
    body: "Even if you have used vim for so long you might want to look at Quanta from time to time. We pack so much into it that I occasionally get emails form long time vim users telling me they were quite surprised to adopt Quanta. Also it is possible we may be able to add the kvim editor plugin by 3.2.\n\nWhen we do have Visual Page Layout finished it will be exceptional because it will be able to do more than just HTML and will have some very interesting features. Right now our attribute editor is coming along nicely and it works in text or VPL. However while we are doing this we are also greatly extending the programatic features including parsing not only PHP functions and variables but classes and objects too. We have parsing now for linked pages and in PHP it respects scoping of variables to reduce noise. Since this is all done within our DTD management structure this means if someone wants to help add Java or Javascript (somebody was supposed to be working on that) they will inherit the functionality.\n\nSo I hope when people can draw their pages in Quanta and everyone gets excited about it that they don't forget that we've been working as hard or harder on what makes Quanta so good already. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: LAMP"
    date: 2003-06-11
    body: "You do know that you can swap out the Editor in Quanta and use the VIM object instead, right??"
    author: "Phoenix"
  - subject: "Re: LAMP"
    date: 2003-06-11
    body: "> You do know that you can swap out the Editor in Quanta and use the VIM object instead, right??\n\nWho is this directed to? If you're asking the developers to do this the answer is that previously the high reliance on Kate made it difficult. Now it is approaching where it would be much easier, though it would still sacrifice a number of features jut to get this editor. Plugin editors will arrive sometime.\n\nOTOH if you're talking to users and you have patched the code yourself and are running it then you need to get in touch with Andras or me so that we can look at making this available to others. We have a lot of other stuff to do and little incentive but if someone did this, especially if they could send a diff against CVS HEAD that would be very cool indeed. We have had a few requests for it."
    author: "Eric Laffoon"
  - subject: "Re: LAMP"
    date: 2005-05-11
    body: "If someone knows about it, could tell me please.\nI refuse to leave vim. It's confortable for me.\nBut Quanta is nice.\n\nIf i could edit on quanta with vim, I ll be more happy than dog with two tails.\n\nThank u."
    author: "Javi"
  - subject: "Re: LAMP"
    date: 2006-03-24
    body: "Recently I was able to test out Linux and see what it had to offer.  Linux seemed as buggy and crash prone as Windows, but I think that had more to do with the cheap hardware it was running on than the OS.  Linux simply did not like certain things like shared video memeory or my DVD burner.  Quanta Plus was really the only killer app that I could not find an equivalent in the Windows world (that I needed, anyway).  \n\nI might just have to install something that allows me to run Linux apps on Windows, since I have to have a Windows box for some of the things I do.  I might even set-up and old beater computer with Linux just so I can use Quanta Plus.  Too bad there isn't a Windows version."
    author: "Scott M. Stolz"
  - subject: "Editing on the server"
    date: 2003-06-10
    body: "Will it ever be possible to edit on the server? This is the one thing that keeps me using QuantaGold (whose development has all but halted). I work primarily in Zope-based sites (like the Dot!), and like it easier to edit files directly on the server, using either FTP or WebDAV. KDE can see my Zope server fine, so all that would be necessary is simply exposing those KIO components. This would be great.\n\nThanks!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Editing on the server"
    date: 2003-06-10
    body: "Im not sure what version of Quanta you are using... maby pre-kde port?  Older?  We have had full kio from day one.  I use it to maintain my remote sourceforge site about once a week.\n\nGranted, this may or may not be documented, but I guess Im just too used to kio.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Editing on the server"
    date: 2003-06-11
    body: "> Im not sure what version of Quanta you are using... maby pre-kde port? Older? We have had full kio from day one. \n\nPre-KDE? Quanta began life as a KDE app in KDE 1. However in KDE 1 there was no kio so we didn't really have it. With the advent of KDE 2 we did for file operations but because of the extensive use of QString instead of Kurl in the tree the fullest exploitation of kio came at Quanta 3.1. That is when remote projects became fully functional but remote file edits have been since KDE 2.\n\n> Granted, this may or may not be documented, but I guess Im just too used to kio.\n \nEasy for you to say. You're a hot shot developer. ;-) How much beer would you like to find me any official explanation of KDE features on line that explain kio since the 2.0 release? This is why I think there should be a drop down on the file dialog to change file:/ to fish://. For most of KDE it's no big deal... but my project is a web development tool. By far the most requested feature, biggest complaint and most frequent email has to do with why we don't have FTP management or remote file access liike such and such windoze program... \n\nMaybe I should bring it up on a list. It's enough to drive a maintainer crazy. I even put it on our feature request page NOT to request it. If there is one thing KDE has done a terrible job of it is in informing people about kio, one of the best things in KDE. The last official documentation I can find is the KDE 2.0 release announcement. :-("
    author: "Eric Laffoon"
  - subject: "Re: Editing on the server"
    date: 2003-06-11
    body: "Agreed with pretty much everything you said. A lot of people I've found using KDE have no clue about kio and it's power. They might have heard about things like kiofish in konqueror, but they have no clue that it works globally in open/save dialog boxes.\n\nPerhaps a solution to this would be a file selector in apps such as kate and quanta that has a combobox with various paths of different protocols in top/bottom. The combobox could have a label attached that says something like \"Viewing:\". The default should say \"Local\" instead of \"file://\""
    author: "fault"
  - subject: "Re: Editing on the server"
    date: 2003-06-12
    body: "I do translations for KDE, so I remembered various kio-functions \nfrom translating something. I absolutely could not find it any place and\nended up asking on the i18n list, and eventually found the info in kinfocenter.\nThis may have changed, but at the time kinfocenter was really well hidden\nwithout a link from khelpcenter. "
    author: "Erik Pedersen"
  - subject: "Re: Editing on the server"
    date: 2003-06-11
    body: "> Will it ever be possible to edit on the server?\n\nI typically respond to all my emails and often quickly with a great deal of congeniality. On occasion I respond as if I'm slightly touched. Two things elicit these atypical responses. One is asking if we could do something \"like windoze program such and such\" as if I'm going to go out and buy a copy of every program out there when I run Linux exclusively. The other is people asking why we don't have or when we will have services related to kio that we've had since version 2.0 in the end of 2000.\n\nWe have attempted to litter your path of information search with one of KDE's best, and certainly their least promoted feature. We have it in our docs in the file section, in the FAQ in the doc section of Quanta, regularly posted on our mailing list, on our feature request page... but still people ask when we'll have features we've had for years. In my opinion the KDE file dialog ought to have a protocol drop down box so that people know it is even there!\n\nHere it is... one more time... To open a file on the server first decide if you want to use ftp, fish, sftp, smb or whatever and then remember file operations are transparent on KDE. Public ftp access is ftp://domain but you need a user name and password for private access. ftp://user@domain/path/file does it. You can enter all but the file in the directory drop down and get your files listed in the dialog. When you enter this a dialog will pop up asking for the password. You can skip the dialog with ftp://user:passwd@domain. Given this functionality it is ludicrous that we would build an FTP manager when you can just enter this in konqueror and have transparent file operations.\n\nQuanta does this one better in our newest versions. You can create a remote project and specify in the project settings what protocol to use to access it with. Then when you open your project it will list all the remote files directly in your project tree.\n\nIf you have been using QuantaGold because of it's FTP features you have been taken in by the PR that went around them having to replace what was built in to KDE already that Quanta Plus has has used all along. As we also incur real costs, and I hope provide more, I hope you will consider giving us equal support. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Editing on the server"
    date: 2003-06-11
    body: "Ahh, I tried to do this, but because of the way Zope stores files (in an object database, not on a filesystem), Quanta complains that it doesn't know how to handle the files. This is from the project listing. Now if I instead try to open files through the \"Open\" dialog, they come up fine (using either FTP or WebDAV). If this can be fixed (and I'll try to assist in this), you've definately got me as a \"customer\". I did scan the docs, but I guess I missed that FAQ entry (which didn't really answer my question). Great work, nonetheless.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Editing on the server"
    date: 2003-06-11
    body: "If the files are in an object database then kio needs a protocol to read them. If you can open them with ftp then the whole thing is very curious since the project tree can be set up to use ftp. So this explanation doesn't seem to make sense as it is.\n\nAnother feature BTW is the ability to set new top directories in the file tree. Typically it shows root and home. You can add a directory to it via RMB in CVS. Earlier versions of Quanta can be set up by eting $KDEHOME/share/config/quantarc. There is a line that looks like this...\n\nTop folders=file:/,file:/home/eric\n\nAdd a comma to the end and follow it wiith ftp://user@domain/path. Now you will have additional directories in your file tree, local and remote."
    author: "Eric Laffoon"
  - subject: "Re: Editing on the server"
    date: 2003-06-11
    body: "Although the files are stored in an OD, it is exposed through HTTP, FTP or WebDAV interfaces. It actually works quite well, as I can open and save a KOffice file transparently using WebDAV and Zope. That file tree modification is probably exactly what I need, however, as I really don't need the project management features (though they are nice). I'll let ya know how it works out.\n\nThanks again!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Editing on the server"
    date: 2003-06-23
    body: "I'm experiencing the same problems as described above. Settin up a new project in Quanta+ (v3.1.2) using a remote-connection to Zope over WebDAV works quiet well, new folders and files are created in the Zope-Database. Only when  i try to open a file in the project-tree i get a message (\"unknown type\") telling me that this type of file cannot be opend in quanta and whether i want to use some other program to open it. Choosing another editor or even another quanta session to open the file works, though. Opening the files directly (without a project) works too. Looks like a small bug...?\n\nThanks!\n\nMarcel\n\n\n"
    author: "Marcel Hoetter"
  - subject: "Re: Editing on the server"
    date: 2003-06-13
    body: "Whoa, had no idea that was in there, especially as I use Quanta under Gnome.  But, all of that functionality's in the libraries so, it works there as well.  That's going to change the way I do a lot of things.  I should probably uh, read the docs one of these days :-)"
    author: "Quag7"
  - subject: "Re: Editing on the server"
    date: 2003-06-13
    body: "Heh heh... wait until you find Kommander... ;-)\n\nQuanta has lots of hidden tidbits. The upcoming preview release will allow you to prcess templates with actions, the new action dialog will make assigning kestrokes easy as seeing where actions are used, the attribute editor will jump right out at you and the aut completion updates and off page parsing will be truly impressive. We are always working on something."
    author: "Eric Laffoon"
  - subject: "Php debugging"
    date: 2003-06-10
    body: "In the review, it is indicated that php scripts can be debugged in Qianta Plus. Has anyone successfully used this feature? Any help will be much appreciated."
    author: "Richard"
  - subject: "Re: Php debugging"
    date: 2003-06-11
    body: "The built in debugging code works pretty good with PHP 3. However it's way behind for PHP 4. The makers of PHPed I think, the big windoze/Linux package with apache and PHP built in, began sponsorship of the debugger and since then they get the up to date code and old code is publicly available. We could buy support but I'm not going to do that. You can do some things with console output. You might want to ask Jono exactly what he was doing.\n\nWe've considered several things for PHP debugging. One is to have a setting that will automatically add echo and or var_dump statements to the code before sending it to preview. This is sort of a lite version. The other is we are thinking of adding support for Gubed. (If you're curious that's debug backwards) You can find it here. http://sourceforge.net/projects/gubed/ Actually it would be helpful for people to help test this so we can bring it up to speed before adding it. We would integrate a plugin interface.\n\nBTW it should be noted that official PHP debugging code was dropped with the 4.0 release. However you can achieve a lot of benefit in Quanta just by switcing to the struct tab and selecting View As PHP from the context menu."
    author: "Eric Laffoon"
  - subject: "Re: Php debugging"
    date: 2005-04-22
    body: "a tutorial \"Debugging PHP scripts with Quanta Plus and Gubed PHP Debugger\" can be found here:\n\nhttp://www.very-clever.com/quanta-gubed-debugging.php"
    author: "tom"
  - subject: "extend it to allow jsp editting"
    date: 2003-06-11
    body: "or even trivialise the process of dtd importing for xml editting and I would use it like a shot. As it is I don't use php and have monkey's with dreamweaver to do html.\n\nI'm sure it's great though, ;-)."
    author: "caoilte"
  - subject: "Re: extend it to allow jsp editting"
    date: 2003-06-11
    body: "We have some code done starting the process of DTD importing and we will be working on schemas too. The author of that code has been indisposed and we're hoping he comes back to finish it as his DTD knowlege is very good. However I keep putting out the message and it's really difficult to get across. What we have here is free community developed software and lots of people who just use it are so proud... The community aspect means that the person you see in the mirror makes some small contribution. After all it's a small price to pay for something great for free and if nobody did it the software wouldn't be there.\n\nIt is very easy to add a language to Quanta. We have made it a matter of writing an rc file and editing XML tag files. We even have a tagxml DTD in Quanta to make it easy to edit. We've done this so that users can add languages. Our W3C DTD guy can add a new HTML dialect in a matter of hours. We generated the PHP tag files with a Perl script from the documentation. It's not difficult and there is some small amount of documentation in the tags area of our source. However we take anyone wishing to add a language and add them to our developers mailing list so they have access to answers and so we can make any adjustments that might be required to our parser.\n\nCurrently we are coding on the parser optimizing it for speed and on Visual Page Layout having just finished a frames editor. How many people do JSP? If just one of them steps forward in the community spirit willing to give up their evenings for maybe a week we will fully support JSP. My belief is that if someone who uses it is testing it then it will be better than if we just take a stab without real world tests. Also I would not like to abandon the C++ development of other features to learn and test new languages.\n\nI am very interested in working with people who want support for Java, JSP, Perl and variants like Mason, Zope, ASP and others. Supposedly ColdFusion and Javascript are being worked on but I've not seen the results for some time."
    author: "Eric Laffoon"
  - subject: "Re: extend it to allow jsp editting"
    date: 2003-08-15
    body: "I currently use Dreamweaver MX for editing JSP files. I would be willing test any enhancements that you make in support of JSP. I also have another person in my group who would also like to test it."
    author: "David"
  - subject: "Re: extend it to allow jsp editting"
    date: 2003-09-02
    body: "I haven't used Quanta much, and currently write JSPs for my webapps in Emacs which can be a little featureless.\nA visual page layout tool would be useful, but JSPs can contain many types of custom tags which are generally used instead of the normal ones (i.e. a JSTL input tag instead of an html input tag), so I think that there would have to be integration with a servlet container to generate the tag output."
    author: "Peter Joanes"
  - subject: "Re: extend it to allow jsp editting"
    date: 2004-08-20
    body: "Looks like this thread has died.  However, I'm am using JSP on opensource other things.( linux, tomcat, postgresql ).  Just finished a web site www.chumsnet.com.  Notice the visual mis-alignment.  I definitely want a good HTML editor in Linux.  The custom tags of JSP should not be an issue.  All tag things in JSP are done with XML.  As long as Quanta can recognize the JSP tags and ignore them, things will be just fine.  And there is no avoidance of invoking the JSP/Servlet container.  But Quanta needs not worry about it.  Quanta only has to provide a button to invoke the scripts( in Ant or Make ) to compile the JSPs, and another button to invoke the web page under development.  The beauty of the new Tomcat container is that it can automatically detect new WAR files, and rebuild the webapps.    \n\nThe displayed web page( or generated HTML code ) can then obtain the HTML source from the IExplorer( or interface api from other browsers ) interface, and Quanta should then do a diff between this generated source and that of the invoking JSP.  Highlight the difference, and this will make it much easier for the user to edit the JSP.  It won't be WYSIWYG.  But, good enough for intermediate to experienced developers.  Even expensive JBuilder relies on having a Tomcat engine, and it is not WYSIWYG( but close )."
    author: "Charles Tse"
---
The <a href="http://www.contentpeople.co.uk/">ContentPeople</a> are currently featuring <a href="http://www.contentpeople.co.uk/issue3/quantaplus.php">a review of Quanta Plus</a> by <a href="http://www.jonobacon.org/">Jono Bacon</a>.  <i>"Quanta manages to balance a full array of features, without useless bloat. I found very few features within the environment that I didn't use regularly. To me, this is evidence of how careful the developers are to keep Quanta Plus lean, rather than flashy. "</i>  As you may recall <a href="http://dot.kde.org/1050969840/">we recently featured</a> an interview with the folk behind Quanta.  <a href="http://quanta.sf.net/">Quanta</a> is the kind of application that makes KDE look good, so keep <a href="http://quanta.sourceforge.net/main1.php?actfile=donate">those donations</a> coming.
<!--break-->
