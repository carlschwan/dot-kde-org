---
title: "KDE-CVS-Digest for September 5, 2003"
date:    2003-09-06
authors:
  - "dkite"
slug:    kde-cvs-digest-september-5-2003
comments:
  - subject: "http://"
    date: 2003-09-07
    body: "The link to freedesktop.org is broken, looks like a forgotten http:// in the A tag.  Thanks for another informative CVS-Digest!"
    author: "Spy Hunter"
  - subject: "Re: http://"
    date: 2003-09-07
    body: "and also few other links on the page."
    author: "naon"
  - subject: "Re: http://"
    date: 2003-09-07
    body: "Fixed. Somehow the semicolon was encoded.\n\nDerek"
    author: "Derek Kite"
  - subject: "the year 2003: Autorun"
    date: 2003-09-07
    body: "..so in the year 2003, KDE got Autorun for when people insert media (cd, dvd etc)? What's next, people can change the desktop resolution on the fly ala windows? Sorry to troll, but sometimes ;)"
    author: "ac"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "..yes in the cvs now people can change the resolution on the fly...\nwhat's the problem?\nYou are not forced to do it..."
    author: "Giovanni"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "there is no problem. only, its the year 2003 and 'already' KDE can do it. it was meant to sound sarcastic. Still, its great that its possible now. But I bet its still restricted to what X allows - which still imposes the X setup hell."
    author: "ac"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "not exacly..you just have to select the \"apply on kde startup\" option.\n:)"
    author: "Giovanni"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "The person that added this option gets the \"most-useless-option-of-the-year-2003\" - award!\n\n\n(seriously, what was this person thinking? of course I want to apply those settings to my next session..)\n"
    author: "wup"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "I can think of situations where I would temporary change my resolution were it possible. Think of playing movies on a somewhat slow system. Scaling up to fullscreen can be hard on CPU or GPU, but if you change the resolution to that of the movie, that's no longer needed."
    author: "Andr\u00e9 Somers"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "Yes, but you could do that for many years now. What's new is being able to change the size of the desktop, and not the resolution on the screen. And for fullscreen changing the resolution is mostly sufficient. \n"
    author: "Sad Eagle"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "And in addition to Sad Eagles comment;\n\nSome video playing programs can change the res themselves so you wouldn't need an option like that.\n\nAnd for other tasks where you'd need to change resolutions you can use the tray switcher (can't remember the actual name atm), as it would be insane to logout/in just to get the right resolution for running a program.\n\nSo, the option is useless.\nIt can be compared with Windows where you have to reboot for every task.. (installing new software -> reboot; installing new driver -> reboot; etc)\n\nIsn't that one of the reasons we all switched to GNU/Linux?\nBecause it isn't such a ridicilous OS that has to reboot every once in a while?\n"
    author: "wup"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "Why does one change resolution on the fly? I've never needed to in X. The highest resolution the display will do is usually best. I read somewhere that it was a matter of flaws within Windows that made it a necessary option. I haven't used Windows for years, so could someone enlighten me?\n\nDerek (ctrl-alt-+ and ctrl-alt-- works if your X is set up for it)"
    author: "Derek Kite"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "You change resolution on the fly usually when you do full-screen presentations with a laptop, for instance."
    author: "ac"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "You could do that before.  Games and presentations or whatever could always change the resolution whenever they wanted. (I don't know if KPresenter had that feature, but it was possible)  The only thing that is different now is that you can change the *size of the desktop* on the fly.  Before, the desktop would always stay the same resolution, and if it was too big to fit on the screen it would scroll with the mouse pointer.  That's the only real difference.  Actually, games should continue to use the old way to change the resolution, because changing the size of the desktop whenever a game starts is really annoying.  On Windows that leads to messing up your icon positions and window sizes all the time."
    author: "Spy Hunter"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-08
    body: "No, you couldn't really do it.  For example when giving a presentation with Acrobat or like you say KPresenter full-screen.   Ctl-Alt-+/- just don't cut it here.  Games and stuff are SUID root and do DGA stuff.  And besides you might still want your desktop in full at that resolution when eg giving demos."
    author: "ac"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "..and games can use it as well\n\n(Doom3 is coming.. you ain't going to play that game in 1600x1200 right? the monsters would be way too scary! ;) (I mean, not because the monsters are scary, but because the models look edgy and the textures blurred at such high resolutions))"
    author: "wup"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-07
    body: "Well, I usually am in 1600x1200, and I have some porn videos and images that are not terribly high quality and are small. I put my monitor all the way to 800x600 so I can see them better. \n\nSo yeah, many people view porn with their computers, so I'd consider this a good real life issue. "
    author: "anon"
  - subject: "Re: the year 2003: Autorun"
    date: 2003-09-08
    body: ">>Why does one change resolution on the fly?\n\nIronicly, I use it because there are still dialogs in several KDE programs which are totaly unusable in 800x600 mode."
    author: "ac"
  - subject: "Autorun is stolen from SCO!"
    date: 2003-09-09
    body: "I remember being able to do this (KAutorun) in SCO's old Linux distro(Caldera at the time)like 4 years ago. You would stick in a CD and up would pop up Konqueror, or some other application.  IBM probably stole that c0d3 too!  Then injected it into KDE....and now everybody is pretending its brand new!!! Those evil suits, them!\n\nActually, one question.  How will this work with Mandrake's Supermount?  The whole mounting this is already a little funny as it is, considering disks are kinda always mounted.  (So the mount/unmount options seems strange when using Mandrake).  However, I will take Supermount anyday until something better comes along."
    author: "Henry"
  - subject: "Unstable Tree"
    date: 2003-09-07
    body: "The digest mentions it, but I would like to still comment on it.\n\nThis week the tree was and is so unstable, that I had to switch to the Mandrake Stock KDE, since the main applications Konqueror and KMail were seriously broken. As of yesterday, Konqueror runs into some heavy processor work on forms and KMail crashes KLauncher and itself when entering a new E-mail address in the recipient line.\n\nI have run KDE HEAD since over a year now, but this week just beats any unstability I have seen. The problem is of course vigirous hacking without testing. I think KDE should make use of the eXtreme programming concept and write unit and functional tests for their code. I think particularly functional tests could be fairly easily accomplished using DCOP. We have used this concept in the Zope 3 development and it works very well. For about 250k lines of code (no documentation, interfaces or tests) we have over 7000 unit tests. We only have 70 functional tests, which will hopefully change in the next months. This has two huge advantages: It discourages excited hacking (i.e. it makes you think more about your code) and it also gurantees bug free code, if you do the unit tests right. Often unit tests take about 1-2 times as long to write as the code itself, but it all pays off in the long run. For example, we have people producing production sites with Zope 3 even before the first beta is out!\n\nJust some thoughts here... I hope next week will be a better week for the KDE HEAD."
    author: "Stephan Richter"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: ">>I think KDE should make use of the eXtreme programming concept and write unit and functional tests for their code.<<\n\nThis is not practical for GUI applications and some other things, like rendering. There is no way of testing whether something is correct. At best you can compare it to older results (for instance check rendering results by saving a bitmap of older runs and compare it), but then you usually need to rewrite the tests very frequently.\n"
    author: "Tim Jansen"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "This is unfortunately a sorry excuse I give myself from time to time. Zope 3 has a good amount of GUI code as well, and functional tests work really well for us in these cases. Also note that I already offered a practical approach by saying that DCOP could be used for functional tests. Note that you could even write your tests in Python if you use DCOP, which would save a lot of time!\n\nOn the other hand, I believe that you can write unit tests for many low-level libraries in KDE. I bet you there are many libraries that do not deal with anything that comes close to be the GUI."
    author: "Stephan Richter"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "This assumes that you can do more with DCOP than execute a few trivial features and are also able to check results. Some apps allow this, most don't. And how do you want to check dialogs, or drag&drop? Usually the rare features are those that cause the most problems, because they are less frequently used.\n\nLow-level libs are a different issue, of course.\n"
    author: "Tim Jansen"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "Have you seen Mozilla's smoketests? They aren't fully automated like Unit tests, but they are nicely structured. khtml already has a few regression tests, but this idea can be extended. "
    author: "anon"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "What exactly does unit testing have to do with unavoidable short-term regressions that have to have when something is being heavily reworked, and has new code that is not completely finished yet, and is further entirely untestable automatically?  "
    author: "Sad Eagle"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "Unit tests avoid having temporarily (which can be a long time) broken code, since noone is allowed to make a checkin until all tests pass. Also, if code is not finished it either (a) should not break other code or (b) work enough not to break other code. If you want to collaborate, you can always use a branch to do the work, which we frequently do when doing major restructuring."
    author: "Stephan Richter"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "Nonsense. The difference is in checkin policy, not the tests. And a branch for development -- well, what do you think HEAD is for? I am sorry, but this is just silly. HEAD is exactly for getting the work done, and unless the changes risk making the entire desktop unusable, there is no point in branching, that just wastes development time. People who try to use HEAD for anything but developing KDE are IMHO just digging their own holes. \n\nNow, I am not saying that tests aren't good -- to the contrary -- however if you can come up with a way of testing stuff like this you're a much smarter man than me. \n"
    author: "Sad Eagle"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "I disagree.. if there is something that is going to give long term unstability within HEAD, then seperate branches should be created. This is how it currently has worked so far. Things like KWIN_III, MAKE_IT_COOL, KATE_GOES_BIDI, etc.. all have worked good in the 3.1->3.2 cycle thus far. "
    author: "anon"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "Isn't it a case of changes in some library that breaks applications? It takes a while for the fixes to filter up. There were a few instances of attempted bug fixes that were quickly reverted due to other problems showing up. Some developers commit works in progress, which makes for instability.\n\nI'll agree though, it's quite unstable. There was a spot two or three months ago where it was similar.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "> It takes a while for the fixes to filter up.\n\nWell, if you have unit tests and functional tests these problems due to API changes immediately show up when running the tests!\n\n> Some developers commit works in progress, which makes for instability.\n \nWell, then they can do this on a branch, which does not have to be stable at any given time or pass all the tests."
    author: "Stephan Richter"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "> Well, then they can do this on a branch, which does not have to be stable at any given time or pass all the tests.\n\nAnd that is exactly why we have HEAD."
    author: "Sho"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "As Sad Eagle so eloquently stated, this is maybe a difference in commit policy. HEAD is not expected to be stable except maybe preceeding a release. There are branches in the KDE cvs repository. KWin is branched currently for a rewrite. But branches have a tendancy to get out of hand if they remain so for long, and can result in a rather difficult and convoluted merge.\n\nFor example, the kcontrol stuff that has changed in the last while. What would be required is a branch that almost contains the whole of the KDE repository, since it affected so many different applications. And while it's branched, the other development would either have to halt or be ported to both, or wait and create a merge nightmare. It's possible, but a whole lot more work for the developers.\n\nSo we have a week or so of bloody fingers and all that.\n\nDerek (who hoped that highlighting the state of head wouldn't create unreasonable expectations)"
    author: "Derek Kite"
  - subject: "Re: Unstable Tree"
    date: 2003-09-08
    body: "Hello Derek,\n\ndon't worry too much, I think it's Ok. The reason that HEAD is unstable should be well know, a freeze is coming. Anything that is in now, will make it.\n\nMy personal expectation is that everything will stablilze rather quick after the freeze.\n\nAs to regression tests, well, if somebody created some and then kdelibs had \"make test\", fine. I would not bother dealing with people demaning that you or others create them. I would ask them to create some if they care. Otherwise HEAD perfectly functions for KDE development of everybody doing core work I guess. The others are on stable points doing apps for the most time and do not care at all.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Unstable Tree"
    date: 2003-09-07
    body: "> This week the tree was and is so unstable, that I had to switch to the Mandrake Stock KDE, since the main applications Konqueror and KMail were seriously broken. As of yesterday, Konqueror runs into some heavy processor work on forms and KMail crashes KLauncher and itself when entering a new E-mail address in the recipient line.\n\nDon't worry.. it's because of a week of sleepless coding at N7H :)\n\nI expect it to stabalize after bug reports from 3.1a1 flood in."
    author: "anon"
  - subject: "Re: Unstable Tree"
    date: 2003-09-08
    body: "for the kmail issue: delete your installed libkabc*so* files, cvs up libkabc and re-make/install it.. this breakage was discovered and fixed almost immediately, and then announced on the devel lists.\n\nas for the CVS instability, here's the easy solution: don't update when there are problems. ;-) i know that sounds a bit obvoius and \"too simplistic to work\" but it works for me just fine... it does mean knowing what's going on in CVS, though... otherwise, you just have to live with code that will occassionally give you a stubbed toe...\n\nbut to suggest that a bunch of effort should go into reforming how CVS commits are allowed because a couple of applications in HEAD, the unstable development branch, become unstable for a short period of time isn't what i'd consider prudent. the issues get worked out (this isn't the first time this has happened in HEAD, as i'm sure you can imagine), and everyone comes out happy.\n\nunit tests are great, however. if you'd like to start creating and maintaing them, i'm sure we'd be happy to put them in CVS where they can help the project out.\n\nback to the topic of this week's KDE Traffic... i \"really\" liked how i was (not) quoted as being against a mandatory branch for Kastle coding, and then the next sentence suggests that's why HEAD is unstable! heh. oh well.. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Unstable Tree"
    date: 2003-09-08
    body: "You sound like James Richard Tyrer: Not being involved in development or understanding how/why it works but picking up the slightest temporary fault to make propaganda for something which misses any proof that it would work with and for KDE development as salutary solution on a way to guaranteed flawlessness. Sorry, KDE doesn't need such people."
    author: "Anonymous"
  - subject: "Re: Unstable Tree"
    date: 2003-09-08
    body: "\nI've always been interested in the 'unit testing' concept. Could you point us to a project with source code for both program and regression base where this is applied?\n\nThanks,\nTom\n"
    author: "Tom Verbeure"
  - subject: "Autorun"
    date: 2003-09-07
    body: "I could have sworn old versions of Mandrake/KDE have had Autorun-support for a long time."
    author: "ac"
  - subject: "Re: Autorun"
    date: 2003-09-07
    body: "Yes, there was an autorun addon for a long time. At least I use it since over a year!"
    author: "Stephan Richter"
  - subject: "Re: Autorun"
    date: 2003-09-07
    body: "I think even KDE1 might have had something."
    author: "ac"
  - subject: "KOffice tables"
    date: 2003-09-08
    body: "Did they fix them?"
    author: "Ax"
  - subject: "Re: KOffice tables"
    date: 2003-09-08
    body: "If you mean the tables in KWord, then the answer is: no, sorry, not enough manpower available!\n\n(The only change is that the tables have now tabulator navigation.)\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Megacommit in Quanta "
    date: 2003-09-08
    body: "I'm wondering what the megacommit in Quanta is (kxsldbgpart now in quanta). Anyone care to explain. TIA."
    author: "John Herdy"
  - subject: "Re: Megacommit in Quanta "
    date: 2003-09-08
    body: "It sounds like a debugger for XSL stylesheets.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "3.2 Work-in-Progress Knoppix ISOs?"
    date: 2003-09-09
    body: "What about that project to create monthly Knoppix ISOs with KDE HEAD? Still alive?"
    author: "Sho"
---
In this <a href="http://members.shaw.ca/dkite/sep52003.html">week's CVS-Digest:</a> <A href="http://uml.sourceforge.net/index.php">Umbrello</A> now has a document model code generator.
Changes in menus and tab configuration in <A href="http://www.konqueror.org/">Konqueror</A>.
Optimizations in KConfig, <A href="http://kmail.kde.org/">KMail</A> and Konqueror. Drag and drop fixes
in <A href="http://pim.kde.org/components/korganizer.php">KOrganizer</A>.
Bug fixes in <A href="http://www.koffice.org/">KOffice</A> and
<A href="http://kopete.kde.org/">Kopete</A>.
<!--break-->
