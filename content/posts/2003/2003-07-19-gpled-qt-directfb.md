---
title: "GPL'ed Qt on DirectFB!"
date:    2003-07-19
authors:
  - "mmaurizio"
slug:    gpled-qt-directfb
comments:
  - subject: "Great..."
    date: 2003-07-20
    body: "At least!\nThis is the beginning....XFree,react or die!"
    author: "Anonymous"
  - subject: "Re: Great..."
    date: 2003-07-21
    body: "ROTFL :)"
    author: "Lucifer"
  - subject: "Re: Great..."
    date: 2003-07-21
    body: "I don't see DirectFB going anywhere soon. Hardware support is still not nearly as good as XFree86. And any modern windowing system better be network transparent."
    author: "Ac"
  - subject: "Re: Great..."
    date: 2003-07-21
    body: "You mean like XDirectFB? Or are you assuming that the misc X network protocols cannot be implemented without an X server that also directly accesses hardware?"
    author: "LV"
  - subject: "Re: Great..."
    date: 2003-07-21
    body: "No, he doesn't mean XDirectFB.  Since XDirectFB is a layer on top of DirectFB, the hardware support is just as terrible.  As in, you basically need a matrox card to get it to work (and those of us with more obscure cards like savages are of source SOL and always will be).\n"
    author: "Andrew"
  - subject: "Re: Great..."
    date: 2003-07-22
    body: "I think LV was addressing the network transparency point.\n\nAs for drivers, the lack of good accelerated drivers frustrates me too. I have an NVidia card, and it works pretty nicely under DirectFB, just isn't accelerated. I haven't been able to try a Savage card, but their status page ( http://www.directfb.org/modules.xml ) shows them as being \"70%\" supported, whatever that means (whereas NVidia only has 10%). Probably now that there are open source Savage drivers that will improve."
    author: "Rakko"
  - subject: "Re: Great..."
    date: 2003-07-22
    body: "he said \"And any modern windowing system better be network transparent\". I was just mentioning that this network transparency doesnt need to be mated to the system like it is in XFree86. XDirectFB allows for most XF86 supported protocols to be used.\n\nAs for hardware support, it could use some work. I also mention the retardedness of the 2.6/2.5 framebuffer in another post. It is truely horrid..."
    author: "LV"
  - subject: "Re: Great..."
    date: 2003-07-21
    body: "I somehow doubt QT on DirectFB will kill XFree.\n\n(Especially with linux 2.6 soonish to be stable and released and DirectFB not working under it. But this isnt a DirectFB issue, the linux 2.5 and 2.6 framebuffers are retarded and have had their balls removed.)\n\nDirectFB in itself is not a windowing system. Basic windowing controls have been hacked on recently(?) and involve meta keys, and work has been done with the (non-working for me) multi-server core that allows you to run more than one DirectFB app.\n\nYes. You heard right. DirectFB normally limits you to one app at a time unless you patch your kernel with fusion MPI support and even then multi-server support is far from gurenteed to work.\n\nDirectFB isnt aiming to be X here. Still, this is a beautiful thing to hear of. Especially for embedded linux developers who have the rom/ram space to burn I would think. Or console enthusiasts who want to not start X when they want to use a gui web browser quick quick (that isnt uglyish like links).\n\nI'm still waiting for a 1.0 release of XDirectFB. the rc4 worked for me but rc5 is horrible. *sigh* XDirectFB feels so nice and smooth when it works properly. Too bad it doesnt have xv or gl support yet. Most people think XDirectFB and say \"why do I want translucency, this is a toy\" when the XDFB X server has other perks than just this. bah."
    author: "LV"
  - subject: "What is DirectFB?"
    date: 2003-07-20
    body: "I've visited the website but can't figure it out really.\nIs it something like Microsoft's DirectX? \nThe website says something about hardware.\nBut is it realistic that graphic cards will support this?\nI don't think they will create graphic cards which run on Linux/Un*x\nonly.\n"
    author: "Martin"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-20
    body: "directFB can use OpenGL for hardware acceleration. Most graphic cards support OpenGL. they only problem is the lack of drivers."
    author: "Jasper"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-20
    body: "So can X. DirectFB is just an abstraction layer over the framebuffer device, but is normally slower (yes! slower!) than XFree, unless you use matrox G series of cards."
    author: "Mike Hearn"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-20
    body: "It is slower. Now!"
    author: "Somebody"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "No, compared to XFree86 (_without_ using a driver with support for hardware acceleration), DirectFB is way faster.\n\nThe bad thing is that DirectFB does only have a few drivers available and there's no (good) NVIDIA driver :(\n\nSo yes, on NVIDIA cards (and many others, but I'm just picking a good example) it's slower, but you'll still notice that DirectFB is faster than XFree86 (everything feels more smooth/fluent, like Windows.. or even better actually)\n\n/me hopes everyone will switch to DFB in the next 2 years.. (including NVIDIA please)\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Come on, that's completely unrealistic. Who will port all those thousands of applications and write dozens of drivers? And why should anyone do it? Because there are a few good drivers for outdated cards? It's unlikely that DFB offers any useful advantages. But it definitely has disadvantages, like the lack of network transparency. \n\n"
    author: "Arno Nym"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "> Who will port all those thousands of applications\n\nRather, porting toolkits such as Qt, gtk, and Motif is better. \n\nKDE itself has some X11-specific code, but various people are working on free-ing KDE from it (first the kdenox stuff, then the QWS stuff, and then the port of KDE to Qt/Mac for MacOSX without X11)\n\nDrivers are a good point however. In terms of network transparency, I think things like VNC/rfb/rdc/timbuktu/citrix are the wave of the future, not X-type network transparency. "
    author: "fault"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Are you sure? While the X protocol might be far away from being perfect it's still a lot more usable than any vnc I've tried. Especially on fast LAN connections. Working with an X session is close to working locally. VNC doesn't even come close. \n\nWhat about just exporting single applications. I don't think something like that will be possible without much weirdness like rendering the app virtually on the server and transfering the image. Otherwise you are close to X again.\n\nI think DirectFB is a great idea and it is useful for many things, especially for embedded devices etc. It would be a tragic loss for UNIX/Linux to get rid of X11 though. "
    author: "dazk"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: ">> It would be a tragic loss for UNIX/Linux to get rid of X11 though.\n\nIt's GNU/Linux, not UNIX/Linux.\nAnd we all know (you do now that, right?) that \"GNU's Not Unix\".\n\nX11 belongs to the UNIX platform, not GNU.\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Last time I checked, not all software on my computer was part of the GNU project, so, it's not GNU/Linux, it's \"Linux and associated open-source software\"."
    author: "Static"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Last time I checked, not all software on my computer was part of the Linux kernel.\n\nSo, we shouldn't call it Linux either.\n\nPerhaps the change the name of the OS should be changed into GLKGQGXFB?\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: ":) I think he meant Unix AND Linux  (Unix AND GNU/Linux)"
    author: "uga"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "I may be stupid, but I'm not th\u00e1t stupid ;)\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "That's definitely the way I read it. I commonly use the term \"UNIX and Linux\", so seeing \"UNIX/Linux\" meant the same to me."
    author: "David Johnson"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "It would be easier to make the XFree scheduler smarter, and the toolkits faster, as that is often what causes perceived slowness on screen. The actual speed of the underlying graphics system is rarely a problem."
    author: "Mike Hearn"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: ">> It would be easier to make the XFree scheduler smarter,\n\nPerhaps it's easier coding-wise, but with the current 3 or 4 active people behind the project (doing 2 bugfixes a day and not adding needed features) I don't see that happening between now and the next 10 years.\n\n\n>> and the toolkits faster\n\nThe toolkits are already faster (and especially smoother) when using DirectFB.\n\n\n>> as that is often what causes perceived slowness on screen\n\nTrue, but the fact that it's easier to make slow and unresponsive programs using XFree86 (quite hard to use API - even toolkits are making many faults) than using DirectFB is also something to think about...\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-22
    body: "http://xcb.wiki.cs.pdx.edu/ is trying to make a Xlib replacement with lower latency."
    author: "Vincent"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "The problem is not the X protocol, but XFree.\nIt is a hungry beast, nothing else.\nIt should be completely *rewriten* to have less latency.\nMany parts(hopefully drivers) can be reused, but the core is rotten.\nThere are 2 kinds of speed, throughput and latency.\nFor most users, latency (reaction speed) is more important than throughput.\nBeOS and QNX both are capable of having a responsive (multithreaded) userland graphics system, so why can't unix and linux?\n"
    author: "Leon Timmermans"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "I'm pretty sure neither BeOS nor QNX actually have a fully multithreaded graphics layer, as at some point you need to have a mutex around the framebuffer. Maybe parts are multithreaded, I dunno.\n\nAnyway, the main problem with XFree is like I said a matter of scheduling and asynchronity, especially with respect to window managers/apps. Like the Linux kernels responsiveness can be improved with VM/scheduler tweaks, so can XFree."
    author: "Mike Hearn"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: ">> Who will port all those thousands of applications and write dozens of drivers?\n\nDo you really use any of those old apps?\n(I certainly don't, I'm only using cli, gtk/gnome and qt/kde apps)\n\nAnyway, should be possible to write some wrapper for those apps.\n(XFree86 may suck, but it does have a stable ABI so that shouldn't be the problem I think)\n\n\n>> And why should anyone do it?\n\nTo get rid of that old cruft called XFree86 and the (mostly) backseat developers behind the project.\n\n\n>> But it definitely has disadvantages, like the lack of network transparency.\n\n95% of the users don't need and/or don't use that feature, why should the other 5% dictate those users what they should use and make them use software that's suffering from bitrot?\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "What I don't get is why all the Windows users want Network Transparency and Microsoft is trying to put something similar in its windowing system, and all us linux users have it and want to get rid of it ??  I think you people are crazy.  I use network transparency every day.  If you actually tried it you would too.  It's nice being at work on a linux box and being able to open your home email client over the internet.  Its not the network transparency that makes X slow.  There is now need to get rid of it. "
    author: "line72"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Perhaps I'm not geeky enough, but usually when I leave home I switch down the power to my system..\n\n\n>> It's nice being at work on a linux box and being able to open your home email client over the internet. Its not the network transparency that makes X slow.\n\nThere are 3 options for that:\n\n- Redirect your email to an address at work\n- Use webmail\n- Don't view any email from your home address at all, you're at work to work, not to view your private email\n\nThe former two are faster than opening a connection to your home box and you'll even save some money by powering down your home system when you're away.\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "I'm not just talking about email.  It could be anything.  I'm just saying its a nice feature to have and it shouldn't be removed."
    author: "line72"
  - subject: "Re: What is DirectFB?"
    date: 2003-08-01
    body: "Network transparency is pretty useful for thin clients."
    author: "Dooglio"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "*Many* KDE apps, especially those that are system near or use fancy graphics, use Xlib directly. I suspect that with other toolkits it's not different.\n\n>> 95% of the users don't need and/or don't use that feature, why should the other 5% dictate those users what they should use and make them use software that's suffering from bitrot?<<\n\nBecause everybody uses a different 95%. That's why software that does not have 100% will not succeed.\n\n\n "
    author: "AC"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "The GTK+ DirectFB port has existed for ages, you can run most gnome/gtk applications with a simple recompile."
    author: "Anonymous Coward"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "They *don't* suffer from network transparency. On a modern desktop system, the overhead produced by a well-designed and optimized network transparent windowing system is perfectly acceptable."
    author: "Ac"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "It's not acceptable if you don't have any use for it.\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "And what are you giong to do with that extra-kilobyte?\n"
    author: "AC"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Filling it with 1024 characters and 2048 nibbles..\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "No, you aren't.\n\nNowadays characters are 16-bit, so you can't put 1024 of them into a kilobyte. Not to mention that AND a pile of nibbles!\n\nThe days of characters==bytes passed about two, three years ago."
    author: "Roberto Alsina"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-22
    body: "Not to mention that a kilobyte is only a 1000 bytes, you would need a kibibyte (KiB) to store 1024 bytes.\n\nOn the topic of network transparancy, I (for one) would rather live without alpha blending (transparancy) - althought it's on its way in XFree86 - than live without newtork transparancy (although I rarely use it).\n\nBTW to those who think that X11 causes poor performance, try comparing AccelleratedX with XFree86 and then decide whether it is X11's or XFree86's fault.\n\nBTW(2) WRT to the directfb screenshots, is directfb responsible for the transparency of Anna Falchi's top?  If it is I might choose the alpha blending over network transparency :-) "
    author: "nonamenobody"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-22
    body: "\"Kibibyte\" is the silliest name I ever heard."
    author: "CE"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "\"95% of the users don't need and/or don't use that feature, why should the other 5% dictate those users what they should use and make them use software that's suffering from bitrot?\"\n\n1) 95% of users don't need and/or don't use KDE, but some version of Windows instead. Maybe we should just kill off the KDE project and all use Windows.\n\n2) 95% of the time I drive, there are only one or two occupants in the automobile, which is a complete waste of the back seat. 95% of the time I drive, the trunk is empty, which is a complete waste of space. I would be much more efficient if I drove a motorcycle instead. Of course, it would then be damned inconvenient when I have more than one extra passenger, damned inconvenient when I need to go shopping for something that won't strap on the back, and damned invconvenient when I get a flat tire and realize that all my emergency tools were in the trunk of the car I sold because it was such a waste.\n\n3) Perhaps 95% of Linux users don't need network transparency, but I will safely assert that 25% to 50% of UNIX users indeed use it on a regular basis. Maybe your vision of vision of a workstation is to be some sort of Xbox clone, used in the home by a single user to play games. But most of us use workstations in a multiuser networked environment to get our work done. A workstation without network transparency would make that work a heck of a lot harder to get done.\n\n4) XFree86 is not suffering from bitrot. More people use network transparency than use Xinerama. But I don't see any hue and cry to get rid of Xinerama (or other multihead support).\n\n5) A million people have already said this, but you haven't been listening. It seems you're attention has been wavering. So get this through your head, NETWORK TRANSPARENCY DOESN'T COST YOU ANYTHING IF YOU DON'T USE IT!"
    author: "David Johnson"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-22
    body: "Actually, network transparency DOES cost  because it still has to pass through X's network layer.  If you're running on a local machine, it doesn't cost *as much* as X over IP over ethernet, but calls to the X libraries still have to be converted to the X protocol, and passed though the network layer, transmitted over a Unix socket, recieved by the X server, and decoded.\n\nThere must be a simpler way of doing this."
    author: "Anon"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-22
    body: "You always need some protocol between the applications and the process/drivers that is responsible for drawing, unless you want to give the applications direct access to the graphics hardware. And even if they had direct access you would need some protocol for locking (because affordable hardware doesn't allow more than one driver to use it at a time). \n\nThere are, of course, different ways to implement this protocol. You can pass messages over unix domain sockets, which is the fastest IPC mechanism for messages on Linux, and what X11 does. Or you can put the driver into the kernel and define a few dozens ioctls. They are just another form of protocol. But whatever you do, you need some protocol."
    author: "AC"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-25
    body: "NVIDIA has said that their Windows/*BSD/Linux drivers share 95% of the code. Thus it wouldn't  be a big deal for them to make hardware accelerated drivers for DirectFB."
    author: "."
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "<I>\"No, compared to XFree86 (_without_ using a driver with support for hardware acceleration), DirectFB is way faster.\"</I>\n\nDoes it surprise you that DirectFB with hardware acceleration is faster than XFree86 without hardware acceleration?\n\nAnd smooth does not always equal fast. Have you ever benchmarked raw drawing speed? How many 640x480 can DirectFB and XFree86 blit per second?"
    author: "Ac"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: ">> Does it surprise you that DirectFB with hardware acceleration is faster than XFree86 without hardware acceleration?\n\nNot really.\n\n\n>> And smooth does not always equal fast.\n\nVery true.\n\n\n>> Have you ever benchmarked raw drawing speed? How many 640x480 can DirectFB and XFree86 blit per second?\n\nPlease tell me, what's the use of having a fast graphical subsystem if it doesn't feel smooth? (which most users perceive as being slow)\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Argh, pressed the reply button too quickly |:(\n\n\n>> Does it surprise you that DirectFB with hardware acceleration is faster than XFree86 without hardware acceleration?\n\nNot really, but I meant having hardware acceleration disabled for _both_ XFree86 and DirectFB.\n\nDirectFB is faster in that case as well.\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Because then at least you can still fix things to make it look smooth. If the raw drawing speed is low then graphics intensive apps like games will hit you hard."
    author: "Ac"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Games are usually using complete different API's..\n\nAnd it won't be fixed, you know that as good as I do.\n(core team, yadda yadda)\n"
    author: "pluub"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "Here's the scoop:\n\nXFree86 currently accesses your hardware directly, essentially bypassing the kernel.  Historically, this is because Linux, BSD, and other unix systems never had their own video drivers.\n\nHowever, as Linux modernized, it now has video drivers, otherwise known as the \"Linux Framebuffer.\"  The idea is really cool.  You can insmod / rmmod a video driver, just like any other Linux driver, and the video device is /dev/fb0 (and so on) just like any other device.  Unfortunately, the Linux Framebuffer is severely underused, and so many cards are unsupported and the drivers are not very optimized.  In contrast, XFree86's drivers are a lot more mature.\n\nDirectFB is an API that layers over the Linux Framebuffer.  It's sorta like DirectX, in that there is a HAL/HEL system (if your card doesn't support a certain feature, for instance such as alphablending, DirectFB will do it in software for you).  However, DirectFB is not intended for just games, like DirectX often is reserved for.\n\nOh, and before anyone says that DirectFB is an X replacement, let me remind you:\n* XFree86 = graphics layer + 100 other important things\n* DirectFB = graphics layer\n\nPersonally, I'd just like to see DirectFB used as a replacement for the XFree86 graphics layer.  In fact, you can already do this with XDirectFB (it's a port of XFree86 to DirectFB)."
    author: "Justin"
  - subject: "Re: What is DirectFB?"
    date: 2003-07-21
    body: "You're right. Correct me if I'm wrong but it seems that with XDirectFB you get all the benefits of DirectFB without loosing the benefits of X."
    author: "grouch"
  - subject: "Anyone fancy porting Konq/Embedded?"
    date: 2003-07-21
    body: "It should be possible (possibly even easy) to port Konqueror embedded to this platform. Anyone up to the job?\n\nRich.\n"
    author: "Richard Moore"
  - subject: "What's the difference"
    date: 2003-07-21
    body: "What's the difference between this and QT/Embedded? I thought QT/Embedded was Qt ported to the frame buffer and that Konqueror runs on QT/E already."
    author: "cbcbcb"
  - subject: "Re: What's the difference"
    date: 2003-07-21
    body: "It is very similar.  Qt/Embedded accesses your /dev/fb device directly, while the Qt in this news article uses the DirectFB API.\n\nGoing through DirectFB is a more sensible approach, as it provides software fallback for all operations."
    author: "Justin"
  - subject: "Re: What's the difference"
    date: 2003-07-21
    body: "Qt/E drivers also provide fallbacks for operations not available in hardware.\n\nAlthough I am still happy to see Qt on DirectFB, just for different reasons."
    author: "Ian."
  - subject: "Re: What's the difference"
    date: 2003-09-03
    body: "I believe with DirectFB you can run multiple framebuffer apps at once. QT/E takes over whole screen and input device so no framebuffer app can coexist on the same screen. QT/E also typically includes its own window manager and launcher."
    author: "F"
  - subject: "Re: What's the difference"
    date: 2003-09-03
    body: "Yes, you need a process with root permissions to coordinate them and a special kernel module for communication between them.\n\n"
    author: "AC"
  - subject: "Licensing concerns"
    date: 2003-07-21
    body: "> A lot of things still have to be implemented, so please don't expect \n> to be able to run Konqueror on DirectFB (yet).\n\nThe KDE libraries are licensed under the LGPL.  We can run LGPL'd code on top of QT because of the Q Public License.  Is it legal to run LGPL'd libraries on top of a GPL'd toolkit?"
    author: "Burrhus"
  - subject: "Re: Licensing concerns"
    date: 2003-07-21
    body: "Yes, the LGPL and GPL are fully compatible, just like the GPL and BSD licenses are. \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Licensing concerns"
    date: 2003-07-21
    body: "> Yes, the LGPL and GPL are fully compatible, just like the\n> GPL and BSD licenses are. \n \nI thought the compatibility only went one direction.  You could run GPL code on top of LGPL, but not the other way around.\n\nhttp://www.gnu.org/licenses/gpl-faq.html#IfLibraryIsGPL\n\nWhat is to stop someone from creating an LGPL interface to GPL'd code, and then extending the system with proprietary code?"
    author: "Burrhus"
  - subject: "Re: Licensing concerns"
    date: 2003-07-21
    body: "To the best of my knowledge, the most restrictive \"compatible\" license sets the course for the rest. That is, while you can run LGPL'ed software on a GPL'ed library, you can't take advantage of the extra \"freedoms\" of the LGPL. \n\nSo if you try to extend an LGPL program that interfaces to GPL'ed code, you must avoid violating the terms of the GPL. *I think.*"
    author: "Timothy R. Butler"
  - subject: "Re: Licensing concerns"
    date: 2003-07-21
    body: "> So if you try to extend an LGPL program that\n> interfaces to GPL'ed code, you must avoid violating\n> the terms of the GPL. *I think.*\n\nBy that logic, all closed source KDE applications will suddenly violate the GPL as soon as KDE is ported to a GPL-only QT.  That is naturally absurd.\n\nThe whole point of the LGPL (originally the LIBRARY General Public License), was to avoid the licensing issues with GPL'd libraries."
    author: "Burrhus"
  - subject: "Re: Licensing concerns"
    date: 2003-07-21
    body: "Well, you don't run QPL, but not GPL, compatible applications on a GPL-only Qt. That is not absurd in any way, shape, or form, if you really think about it. You can't expect closed source KDE apps to somehow be compatible with GPL'ed Qt just because kdelibs are LGPL'ed. The higher level LGPL can't force its will on the lower level GPL'ed library. \n \nThus, proprietary apps like theKompany's Kapital will need to remain on Qt/X11. \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "fresco?"
    date: 2003-07-21
    body: "what's about fresco? i think this could be a X replacement (full hardware accellerated AND full network transparent):\nhttp://fresco.org/"
    author: "panzi"
  - subject: "Re: fresco?"
    date: 2003-07-21
    body: "DirectFB isnt an X replacement. fresco aims to be, DirectFB doesnt. and fresco can run on top of DirectFB, as fresco isnt concerned about direct hardware access. The most supported fresco interface is GGI... the general graphics interface. That would make GGI the graphics layer which is similar to how DirectFB would be the graphics layer, except that GGI is just an interface and is itself a layer on top of another layer (like SDL, XFree, or even.... DirectFB).\n\nfresco does not talk to hardware directly.\n\nDirectFB isnt a windowing system but JUST the graphics layer (and mouse handling, etc) that talks to hardware.\n\nXFree86 is a windowing system and also accesses hardware directly.\n\nXDirectFB is a port of XFree86 to use DirectFB for it's hardware access.\n\nQT is a toolkit that draws widgets (essentially, but does other things).\n\nI hope this explains why fresco has nothing to do with this article at all. :)\n"
    author: "LV"
  - subject: "Anna Falchi"
    date: 2003-07-22
    body: "Is directfb responsible for the alpha-blending on Anna Falchi's top (in the screenshots)?  If so, it must be some very clever software."
    author: "nonamenobody"
  - subject: "You have no clue regarding DirectFB..."
    date: 2003-07-23
    body: "Introducing it should be noted, that DirectFB is a project of a german company called \"Convergence\" (http://www.convergence.de), who has several projects besides \"DirectFB\" in their pipline which include:\n\nthe DVB (Digital Video Broadcast, the standard for digital television in the EU) driver for Linux on the first available cards (based on a reference design by Technotrend)\n\ndietlibc - a \"thin\" libc implementation\n\nDirectFB - a hardware accelerated framebuffer (http://www.directfb.org)\n\nand a reference implmenentation of the MHP (Multimedia Home Platform) for Linux, (which itself uses DVB as transport-mechanism).\n\nThere also exist other projects, having mainly to do with MPEG2 encoding and replay (DVB as well as DVD are MPEG2 based).\n\nFrom these projects, all hosted on http://www.linuxtv.org, and from reading the companies website one should get a clearer picture.\n\nThis stuff is not being developed to replace X11 anytime soon. Instead it is an essential part to provide STB projects with a high-level graphics engine and assorted media frameworks. This stuff is not about your average \"desktop\", it is about Digital Convergence devices. Toucscreens, TV screens and VGA displays of course. Portable, STB, AV devices, not desktop-PCs. And as such the gfx-card manufacturers are more or less interested. Maybe the best example is the nearly complete support for the TVIA CyberPro 5xxx. The CyberPro is a powerfull (I have been told) graphics engine that gets used in STB (http://www.allwell.tv uses them, i.e.) with Video, TV and PiP support and some other \"goodies\", that you will require when using the TV.\n\nAnd this is where \"translucency\" is not a playfull feature but a very good thing. One may think about OSDs (on screen displays) and similare. So it is a framebuffer with a mutlimedia/entertainment bias.\n\nSo all these ppl here who are standing up the front with their swords raised and their flags having a nice 95% banner should try to look at the world a bit more differenciated. The fact, that now QT, formerly GTK+ and parts of XFree got ported is just a win for all of us, especially who use DirectFB as part of their HTPC system (check out http://byzgl.sourceforge.net as an example distro) and want to do more than running some spare apps. Additionally it is good to see, that software like MPlayer supports DirectFB natively.\n\nIf DirectFB progresses and gets some better hardware support (blame the manufacturers for closing their drivers) not much would be needed in order to create a powerful embedded (sort of) MultiMedia engine on top of Linux.\n\nAll that I personally miss (I did not buy a G550DH by accident when building my MediaServer...) is a port of Mozilla. There is even two MPlayer plugins for Moz. Use your fantasy !"
    author: ".jon"
---
The first, very-alpha, release of the Qt library on 
<a href=http://www.directfb.org>DirectFB</a> is now available for testing! A lot of things still have to be
implemented, so please don't expect to be able to run Konqueror on DirectFB 
(yet). You can find <a href="http://linuz.sns.it/~monge/qt-directfb/story.html">screenshots</a> and <a href="http://linuz.sns.it/~monge/qt-directfb/download/">a link</a> to the source <a href=http://linuz.sns.it/~monge/qt-directfb/>here</a>.
<!--break-->
