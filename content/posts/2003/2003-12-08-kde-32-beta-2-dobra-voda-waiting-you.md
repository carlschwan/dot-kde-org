---
title: "KDE 3.2 Beta 2 \"Dobra Voda\" is Waiting For You"
date:    2003-12-08
authors:
  - "coolo"
slug:    kde-32-beta-2-dobra-voda-waiting-you
comments:
  - subject: "Great release"
    date: 2003-12-08
    body: "I've been running Beta 2 all day, feels quite snappy and stable."
    author: "Haakon Nilsen"
  - subject: "Re: Great release"
    date: 2003-12-08
    body: "Yeah it's less buggy than beta1.\nI still have 2 annoying bugs but it will be fixed before final. It's ok for now, it's a beta ;)"
    author: "JC"
  - subject: "Re: Great release"
    date: 2003-12-08
    body: "Which are? (Just curious).\n"
    author: "Src"
  - subject: "Re: Great release"
    date: 2003-12-08
    body: "After opening a html file in konqueror, when I want to browse the directory, it doesn't display anymore my files and directories but the 1st html file.\n"
    author: "JC"
  - subject: "Re: Great release"
    date: 2003-12-08
    body: "Hi. I don't think that's a bug.\n\nI've seen mentioned in the usability mailing lists that the \"home\" button now means \"home dir\" in \"file manager profile\" and \"home page\" in \"web browser profile\". That is a good thing.\n"
    author: "Src"
  - subject: "Re: Great release"
    date: 2003-12-08
    body: "that's the reason why up until now i don't set a homepage in konqueror. good to hear it's now changed."
    author: "jasper"
  - subject: "Re: Great release"
    date: 2003-12-09
    body: "You may set your home page to \"file:///home/user\" and it should do the trick, don't it?"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Great release"
    date: 2003-12-10
    body: "yes..and when im in browser profile konqueror goes to my home dir.\nif i set home to www.kde.org, konqueror goes to this site if i am\nin file management profile.\n\nbut as the message i replied to said, there is now a distiction bet. your home dir and home page in 3.2."
    author: "jasper"
  - subject: "Re: Great release "
    date: 2003-12-11
    body: "Yes, yes, yes ..Bingo :-) Finally!"
    author: "Been waiting"
  - subject: "Re: Great release"
    date: 2003-12-08
    body: "Uncheck \"View/Use index.html\""
    author: "Anonymous"
  - subject: "Re: Great release"
    date: 2003-12-11
    body: "3.1 is annoyingly slow to boot. Has it been improved in 3.1.94 (aka. 3.2 beta)?"
    author: "Syed Mamun Raihan"
  - subject: "Re: Great release"
    date: 2003-12-14
    body: "I have to agree, it is a great release!  I myself have read the threads about some horror stories that other SuSE 9.0 users have had installing or using the binaries.  I for one have only had 2 problems.\n\n1) kdm was a bit wonky til I configured it again\n2) konqueror : webbrowsing profile loaded the libkrpmview plugin that made it crash. Found that it was part of the SuSE \"branding\" package.  So I removed it and now the installation runs beautifully.\n\nThanks! \nPS: Love that new bouncy icon, and the removal of the frame around the systray.  Nice touch.."
    author: "soffcore"
  - subject: "Re: Great release"
    date: 2003-12-26
    body: "Overall I'm very happy with this new KDE-beta. Faster than 3.1.3 and seems cleaner, but I have noticed a couple issues with KsCD and Konq. \n\nIf I'm playing a Audio-CD and open Kong in file manager mode-the CD stops playing, will not resume, and KsCD looses the CD Track-Artist Info. This seems to be associated with the Audio-CD portion of Kong (auto-loading the CD info/files in Ogg and other various file formats).\n\nThe only sure method of regaining CD track-info is to shut down KsCD-eject CD and start over, which to say is annoying.\n\nThis \"test-system\" is a VectorLinux4 single CD base system ie-slackware with required libs-support programs added to meet requirements for each KDE \"config-file\" depends. Custom kernel 2.4.22 using base VL4 config but added Athlon CPU,  patched with Supermount-ng 1.2.10, and Nvidia 4496 driver. \n\nComplied KDE from source files using QT 3.2.1 or 3.1.2. I dont have the system in front of me to verifiy which version of QT but only one version is/was ever installed. The compile time was about 25hrs including QT/extra support libs/programs on an AMD-XP1800 with 512M-I'm not sure I would do it again.\n\nJust curious if anyone else has experianced this same issue? Otherwise nice job!\nNow if I could just get KOffice to open Word2000 Docs.\n\nRobert\n\n"
    author: "Robert Inglis"
  - subject: "Re: Great release"
    date: 2004-06-08
    body: "I am using KDE on Fedora Core release 2, which I presume is a stable version, but am having the same problem as described above with Kscd and Konqueror. When I open a konqueror window I get cd activity. If I am playing an audio cd at the time, it jams the cd player. i don't have to exit Kscd entirely, but I do have to start at the beginning again. Which is very annoying ;) Can anyone tell me how to fix this?\n\nIf it helps, my cd drive is /dev/hdc, which is linked in /dev/cdrom.\n\nthanks a million, Matt"
    author: "Matthew East"
  - subject: "konqueror kills kscd"
    date: 2004-10-17
    body: "I have this problem also (fedora core 2).\n\nIf I have an audio CD playing using kscd and start konqueror\nthe CD stops playing and control can only be taken back by\nquitting konqueror, ejecting the CD and restarting kscd.\n\nThis can be avoided by closing down the left hand panel which contains\na tree like representation of the audio CD player.\n\nThis problem is really really irritating - any fix? I presume there\nis something trivial to be done but cannot find it.\n\nSA"
    author: "SA"
  - subject: "Knoppix Live CD for Beta 2"
    date: 2003-12-08
    body: "Does anyone have experience remastering a Knoppix Live CD?  How hard would it be to put together a Live CD ISO for these beta (and other) releases.  Presumably, it would have all KDE packages installed."
    author: "manyoso"
  - subject: "Re: Knoppix Live CD for Beta 2"
    date: 2003-12-08
    body: "try morphix.\nIt is modular knoppix. You can add your own modules to the base system and then generate a cd-iso.http://morphix.sourceforge.net/modules/news/"
    author: "bla"
  - subject: "Re: Knoppix Live CD for Beta 2"
    date: 2004-02-10
    body: "I wanna creat my own knoppix live-cd, but not via remastering. Under my debian, I build a New Debian Sid system, and installed all the knoppix-debs from http://developer.linuxtag.org/knoppix/i386/ . \nThen I created the KNOPPIX filesystem, and then the myknoppix.iso image. \nBut when I test the iso in vmWare, it said, \"Can not find the KNOPPIX filesystem\", and dropped me to a very limitted shell. \n\nMy question is, after I installed the knoppix-debs from http://developer.linuxtag.org/knoppix/i386/ , what do I have to do to prepare for the KNOPPIX filesystem? \n\nThanks and regards!"
    author: "Hiweed Leng"
  - subject: "Re: Knoppix Live CD for Beta 2"
    date: 2003-12-08
    body: "I'm pretty sure you should be able to just mount the cd image on your harddrive rw, fakeroot into it, and install/remove packages as normal. Note, this is assuming you're running Debian. If not, you'd need to find dpkg and apt-get somehow, and manage to set them up and such."
    author: "John Hughes"
  - subject: "Re: Knoppix Live CD for Beta 2"
    date: 2003-12-09
    body: "With the sole exception that you can mount the iso9660 filesystem only 'ro'. At least on my system."
    author: "Carsten Menke"
  - subject: "Re: Knoppix Live CD for Beta 2"
    date: 2003-12-09
    body: "Mount it, copy everything into another folder, chroot, install/uninstall.\n\nNow, the problems here would be (I may be wrong):\n\n* Knoppix is not really all in the ISO image, because some of it is inside of a file that is mounted as a loopback, other stuff in a ramdisk.\n\n* Knoppix is not really 100% a Debian system, it lacks some stuff, has some extras.\n\n* You still would need to fix the syslinux booting stuff to make your changed image boot properly."
    author: "Roberto Alsina"
  - subject: "Re: Knoppix Live CD for Beta 2"
    date: 2003-12-10
    body: "The scripts for building knoppix are freely available.  The main tricky bit is the compressed loopback."
    author: "Corrin"
  - subject: "Don't bother - Use Slax!"
    date: 2004-01-02
    body: "Slax http://www.slax.org is a LiveCD based on Slackware.  The New Year release features KDE 3.2 beta2, KOffice 1.3 beta2 and just for fun KDE games!\n\nI'm using it to post this comment and view this webpage right now!"
    author: "evangineer"
  - subject: "Re: Knoppix Live CD for Beta 2"
    date: 2006-07-04
    body: "I have used backtrack....a  baby of auditor and Whax....all based on Knoppix.... looks  nice so you guys should try it out"
    author: "Wilberforce Wafula"
  - subject: "SuSE Packages"
    date: 2003-12-08
    body: "I tried the suse packages of the 3.1.93 version, and nearly everything was wrong with them.\n\nCould anyone tell me if this is the case with the 3.1.94 packages, and, how I can install them without dependency conflicts?"
    author: "Mark"
  - subject: "Re: SuSE Packages"
    date: 2003-12-08
    body: "I'd also like to hear about SuSE binaries (particulary SuSE 8.2).\nAny particular problems? How stable is it for everyday work? \n"
    author: "Src"
  - subject: "Re: SuSE Packages"
    date: 2003-12-08
    body: "I have just installed the 8.2 packages here and they are actually very good. I had no trouble installing them, and they run just fine, much much better than 3.1.93. I had almost no dependency conflicts when installing them."
    author: "Stephan"
  - subject: "Re: SuSE Packages"
    date: 2003-12-09
    body: "May I ask where you found libvcard.so? kdelibs throws up this dependency for other packages and I can't find this library(?) anywhere on the web. "
    author: "Gerry"
  - subject: "Re: SuSE Packages"
    date: 2003-12-09
    body: "According to my apt kio slave, it's in kdelibs."
    author: "Max Howell"
  - subject: "Re: SuSE Packages"
    date: 2003-12-09
    body: "I had the same problem but i did this \n\nln -s /opt/kde3/lib/libvcard.so.0.0.0 /opt/kde3/lib/libvcard.so\n\nand then ran \n\nldconfig\n\nbut later i did\n \nfind / -name libvcard.so 2> /dev/null\n\nand found copies of that lib in\n\n/opt/OpenOffice.org/program/components/libvcard.so\n/opt/mozilla/lib/components/libvcard.so\n\ngo figure"
    author: "infyrno"
  - subject: "Re: SuSE Packages"
    date: 2003-12-11
    body: "libvcard.so.0 is provided by the kdelibs3 in SuSE 9.0's KDE-3.1.4 packages.  However, it does not seem to exist in the KDE 3.2beta2 kdelibs3."
    author: "nonamenobody"
  - subject: "Re: SuSE Packages"
    date: 2003-12-09
    body: "Everything (almost) still seems to go wrong with the SuSE packages for 9.0.\n1. Konqueror crashes imediately\n2. Sound is accompanied by a VERY loud background noise\n3. Windows cannot be moved or resized \n4. the kmix window looks horrible\n5. ditto for the kdm window\n\nI didn't try much more  "
    author: "Malcolm Agnew"
  - subject: "Re: SuSE Packages"
    date: 2003-12-09
    body: "\nThe \"horrible sound\" is caused by your mixer settings, go into the \"input\" tab and deselect all the input sources.  (The sound card is sampling its own output and that is causing a feedback loop.)"
    author: "stunji"
  - subject: "Re: SuSE Packages"
    date: 2003-12-10
    body: "Regarding Nr.2, I found that I had to disable some FM18## setting in the Advanced options of kmix to get rid off a terrible background noise on an Ensoniq 1371 with ALSA on SuSE-9.0.\n\nI have no idea what the cause was and why this setting helped against this noise.\n\nI can  not reproduce any of your other complaints. Probably a problem in front of the screen.... :-)"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: SuSE Packages"
    date: 2003-12-10
    body: ">Regarding Nr.2, I found that I had to disable some FM18## setting in the >Advanced options of kmix to get rid off a terrible background noise on an >Ensoniq 1371 with ALSA on SuSE-9.0.\nWhere do you find the \"Advanced options\" of kmix. In kcontrol I could find\nnothing relating to FM18##. I have this terrible background noise even after deinstalling beta 2 (and de/reinstalling the sound card).\n\nMaybe the other problem is me :-) but it's hard to see how I could break as much as was broken. Even beta 1 was a little better (but still hopeless) than beta 2.\n\n1. as root\n % init 3\n2. download all the *.rpms\n2.\n % cd ~malcolm\n % ls -ld .kde*\n .kde\n .kde3 -> .kde\n % rm .kde3\n % mv .kde .kde-save\n3. Install the rpm's\n4.\n % init 5\n5. test"
    author: "Malcolm Agnew"
  - subject: "Re: SuSE Packages"
    date: 2003-12-10
    body: "If Konqueror is always crashing when used as a Web Browser but works okay when used as a file manager, the problem is with krpmview which is supplied by SuSE as part of kdebase3-SuSE.\n\nI removed this application and its associated files and  now Konqueror works fine (I'm using it to submit this on SuSE 8.2 using the SuSE 3.1.94 rpms."
    author: "GeorgeM"
  - subject: "Re: SuSE Packages"
    date: 2003-12-10
    body: "No - konqueror crashes before I can use it as a file manager.\nEven worse is that the window manager is almost useless.\n "
    author: "Malcolm Agnew"
  - subject: "Re: SuSE Packages"
    date: 2003-12-10
    body: "I discovered this too.  In fact, it looks like you lose all the SuSE integration when you upgrade KDE in this manner.  I'm new to SuSE-- do you suppose that they'll release official RPMs when 3.2 becomes stable?  Or will I have to go back to Slackware?"
    author: "stunji"
  - subject: "Re: SuSE Packages"
    date: 2003-12-11
    body: "They have in the past."
    author: "infyrno"
  - subject: "Re: SuSE Packages"
    date: 2003-12-11
    body: "Good catch!\n\nI only removed\n\n/opt/kde3/lib/libkrpmview.la\n/opt/kde3/lib/libkrpmview.so\n\nand konqui runs likes a charm.\n\nSuSE 9.0 with 3.1.94.\n\nGreetings,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: SuSE Packages"
    date: 2003-12-11
    body: "thx. this fixes konqueror as webbrowser as well. any1 else who things suse's quality went to hell in general?"
    author: "Juergen"
  - subject: "Re: SuSE Packages"
    date: 2003-12-11
    body: "*thinks"
    author: "Juergen"
  - subject: "Re: SuSE Packages"
    date: 2003-12-15
    body: "I think that is a pretty bold statement concidering this is only a beta 2 release. I think that SuSE's products have been getting steadily better, as always there are a few shortcomings, but fewer so than with other distros that I have tried. Why don't we wait for official .0 rpms from SuSE before making such harsh comments"
    author: "Steve"
  - subject: "Re: SuSE Packages"
    date: 2004-01-20
    body: "I removed those packages and now Konqueror works again with SuSE 8.2 KDE 3.1.94.  I find it odd that simple removal of two files makes it work again.  Hopefully it didn't break anything else, but hell, I'm glad I have browsing again!"
    author: "Justin"
  - subject: "Re: SuSE Packages gpgagent problem"
    date: 2003-12-19
    body: "I have installed kde 3.1.94 from Suse rpm (from kde.org) on SuSE 8.2 installed on clear system (there are some differences with respect to updating system)\nkde looks OK but konquror - but this is explained by somother replays.\nOne more problem I have noticed is related to gpg integration with kde.\nkde starts with message gpgagent not running.  \nAny one knows solution?\nregards\nMarek"
    author: "Marek Chmielowski"
  - subject: "SuSE binaries"
    date: 2003-12-08
    body: "Has someone already installed the binaries from SuSE? I had some trouble with it last time (Beta 1), so I decided to wait until the final release is out. But now... I wonder if it will run this time, can't wait ;-)"
    author: "koelschejung"
  - subject: "Re: SuSE binaries"
    date: 2003-12-08
    body: "Compared to the first beta for SuSE 8.2 the second beta is really, really a big jump forward. Give it a try!"
    author: "Ojay"
  - subject: "Re: SuSE binaries"
    date: 2003-12-08
    body: "Oh god! I got a lot of conflicts. I don't know where to start. It looks like every package causes a conflict :-("
    author: "Dirk"
  - subject: "Re: SuSE binaries"
    date: 2003-12-09
    body: "Not nice. If you have all the SuSE packages (plus the new QT libs and the appropriate KDE-i18n package for your language) then simply install all of them in one rush: \"rpm -Uhv --force --nodeps *.rpm\" and cross your fingers. Because you install everything you don't need to care about missing dependencies (but only if you have a previous KDE version installed which already handled third party dependencies)."
    author: "Ojay"
  - subject: "Re: SuSE binaries"
    date: 2003-12-09
    body: "And of course, don't bother complaining if you screw up your system this way.\n"
    author: "Zelva"
  - subject: "Re: SuSE binaries"
    date: 2003-12-09
    body: "The only conflicts I saw were for libtag.so (apparently not included on SuSE 9 CDs) and a package called kdenetwork-SuSE.  I just installed with --force --nodeps and things seem to work OK, just a few problems:\n\nSuseplugger no longer manages my device icons\nKonqueror segfaults in webbrowsing mode\nWidgets sometimes flicker / comboboxes set their values randomly\n\nI wouldn't recommend installing these packages at work, but for home they're fun to play with.  Give it a shot.  You might also want to move your .kde/ dir out of the way, this is probably causing some of my problems."
    author: "stunji"
  - subject: "Re: SuSE binaries"
    date: 2003-12-09
    body: "\"--force --nodeps\" is a good source for problems."
    author: "Anonymous"
  - subject: "Re: SuSE binaries"
    date: 2003-12-09
    body: "Sure. It will work this way or it won't work at all. Most probably it will work because you install everything. If it does not work, then the packager made mistakes and produced really buggy binary packages. In this particular case the binaries should be considered to be useless. Don't use them...."
    author: "Ojay"
  - subject: "Re: SuSE binaries"
    date: 2003-12-11
    body: "Or simply, the new version has a requirement the older one didn't have, and it's not provided by these RPMs. That is a perfectly reasonable problem, and it doesn't mean the binaries are in any way buggy.\n\nUsing --force --nodeps is only good if you know WHY you can ignore missing dependencies (for example, if you have a library built from source in some other path, and litle else)"
    author: "Roberto Alsina"
  - subject: "Re: SuSE binaries"
    date: 2003-12-10
    body: "to stop Konqueror crashing in web browsing, Remove the application krpmview and its associated files which are supplied by SuSE as part of kdebase3-SuSE.\n\nThis has been logged as bug number 61042, details at http://bugs.kde.org/show_bug.cgi?id=61042"
    author: "GeorgeM"
  - subject: "Re: SuSE binaries"
    date: 2003-12-10
    body: "Aha so that was the problem. I saw a conflict between kdebase3 and kdebase-suse, so I removed kdebase-suse and it worked fine. \n\nCheers,\n\nJ"
    author: "Jeroen"
  - subject: "Re: SuSE binaries"
    date: 2003-12-14
    body: "Thank you! I've been frustrated for the past 4 days because the -only- problem I'm having with beta2 was konqueror segfaulting every so often. That has certainly cleared it up."
    author: "John E. Martin"
  - subject: "Re: SuSE binaries"
    date: 2003-12-12
    body: "Use Kpackage, and select all of the .rpm's. \nThen, install marked. "
    author: "Rich Jones"
  - subject: "Re: SuSE binaries"
    date: 2003-12-13
    body: "Running SuSE 9.0.\n\nDoes anyone else get this problem?:\n\nUpon login of a new user (created after installing 3.1.94), I immediately get a dialog with TitleBar: \"Sorry - KDesktop\" with content: \"KDEInit could not launch 'default_desktop_aligning': Could not find 'default_desktop_aligning' executable.\n\n[OK]\"\n\nI also have a problem with fonts in the Konsole application.  When I open a Konsole, or when I specifically choose the Linux font for the Konsole, I get an error dialog with TitleBar: \"Error - Konsole\" and content: \"Font `-misc-console-medium-r-normal--16-160-72-72-c-80-iso10646-1' not found.\nCheck README.linux.console for help.\" \n\n[OK]\"\n\nBut I have no file README.linux.console anywhere on my filesystem.  Any thoughts?\n\nI did the following in upgrading:\n1. dl suse ix86 rpms for 9.0\n2. init 3\n3. login as root\n4. rpm -Fvh (or -Uvh) kde3.....rpm (one at a time, in such a manner as to resolve all dependency problems before successful install---thus, very few times did I need to use --force --nodeps and only where it seemed clear that some other package would be providing the necessary requirements)\n4a. As a part of this step, I also had to first do some rpm --erase packageX.rpm where there were conflicts (in some cases, my SuSE 9.0 insisted on =3.1.4 package versions rather than >=3.1.4 versions)\n5. Ran YaST from the console, looked at the summary of dependency problems (many were reported, but only one that I hadn't noticed forcing from the command line), found one for libtag, therefore, I did:\n6. rpm -Uvh taglib-0.95-5.i586.rpm\n7. Used console YaST to create a new user (thinking to get an entirely new .kde directory upon first KDE startup and not risk damage to some existing user's .kde directory)\n8. init 5 (I got a different kdm background from before, but it still looks nice---noticed 2 or 3 system users (privoxy, some others) listed in the kdm user list as well as regular users)\n9. Logged in as the new user and got the Error Dialog listed above.\n\nJust about everything else seems to work fine.  I did find the Konqueror bug and fixed it (or rather, did the workaround listed in the bug report), but of course that removes the handly konqueror plugin for looking at rpm package contents---hopefully this will be fixed soon.\n\nI had few or none of the other problems mentioned here and in similar threads.  I thought there might have been an issue with my suseplugger, (my CD-ROM and DVD-ROM don't show up on the new user's desktop), but that really seems to be related to the error dialog listed above.  If I do any clicking on the desktop surface itself, the error dialog is brought to the front and the focus is assigned there.  When I ran suseplugger by hand, it let me integrate the optical drives (though I didn't get links on the desktop).\n\nFWIW, this whole setup is with a Matrox Millenium G450 and running X in dual-head mode with one monitor at 1600x1200 and the other at 1024x768 with xinerama---not a trivial arrangement, but KDE 3.2 Beta 2 seemed to do ok with it.  My sound works, panel works, I'm posting this in konqueror, xmms works, quanta 3.2 beta 2 starts up, kdevelop 3.0.0b2 starts up (though SuSE YaST reports many dependency problems with it, I'm guessing that the dependency problems reported are more problems with packaging than actual lacks of some requirement---else, would it start up so nicely?), kommander starts up.  Not sure what else to test.\n\nOh... one last question.  I find no link in menu for kcontrol.  I have to run it from the command line (though when I do, it seems to work fine).\n\nMany thanks to the KDE developers!  Nice work!\n\n-Kevin"
    author: "Kevin"
  - subject: "Re: SuSE binaries"
    date: 2003-12-16
    body: "I fixed the \"default_desktop_aligning\" problem by extracting it from the original SuSE 9 kdebase package:\n\ncp [path to /suse/i586/kdebase3-3.1.4-52.i586.rpm] into a temp directory-not into your /opt/kde3!\ncd into to the temp dir and:\nrpm2cpio kdebase3-3.1.4-52.i586.rpm | cpio -i --make-directories\nNext, delve to opt/kde3/bin in your new set of kdebase directories and cp default_desktop_aligning to /opt/kde3/bin\n\ndelete the temp dir.  Logout and login.\n\nThe konsole font problem, I just picked Normal, then Save as default and the message stopped.  Problem with font aliasing??  \n\n-Craig\n"
    author: "Craig"
  - subject: "SUSE KDE3.2 BETA KONQUEROR FIX"
    date: 2003-12-10
    body: "the fix is:\nrm /opt/kde3/share/apps/khtml/kpartplugins/krpmview.rc\n\nwell ... you won't see rpms in konqueror ... but its a fix :D"
    author: "Lolaine"
  - subject: "Really fast and stable!"
    date: 2003-12-08
    body: "Wow. I tried the 3.2beta1 release before and I found always the same annoying bugs I also found before in 3.1.x. To my large surprise up to now all these annoying bugs (plus the additional ones introduced in the first beta) are gone. For me it looks like as if 3.2beta2 is even better than 3.1.4! Good work! \n\nEven more remarkable for me is the speed of the second beta (it feels at least 30% faster) compared to both 3.1.4 and 3.2beta1. Where does this really large speed momentum come from? "
    author: "Ojay"
  - subject: "Re: Really fast and stable!"
    date: 2003-12-08
    body: "If you still have bugs, you can fill a report on bugs.kde.org :)"
    author: "JC"
  - subject: "Re: Really fast and stable!"
    date: 2003-12-08
    body: "well, he said that the bugs are actually GONE!\n\ngood to hear all this positive advancements!"
    author: "joe p"
  - subject: "Re: Really fast and stable!"
    date: 2003-12-09
    body: "'Where does this really large speed momentum come from?'\n\nFresh install :)"
    author: "wygiwyg"
  - subject: "Re: Really fast and stable!"
    date: 2003-12-09
    body: "Okay. After one more day giving it a try I have to say that my first impressions were the right ones. Ah and by the way I am using the SUSE 8.2 binaries. \n\nNevertheless, I found two severe bugs that were also present in the first beta: \n- after entering and highlighting some text in webforms, pressed keys no longer get accepted. After some mouse actions on the highlighted text it might be that keys pressed on the keyboard get accepted again.\n- KNode tries to receive new messages from a newsserver but refuses to do so showing the error message: \"Connection not possible. Work in progress.\" This  seems to indicate incompleted work.\n\n"
    author: "Ojay"
  - subject: "Re: Really fast and stable!"
    date: 2003-12-11
    body: "Had the same knode bug you were suffering from.\n\n\n\nHad to do some re-emerge under gentoo since some gcc, it didn't accept one of my use variables. However after the re-emerge knode works perfectly (even in kontact).\n\n\n\nWhat I'm saying, it might be a compile bug in the binaries, it doesn't have to be in the source.\n"
    author: "Jord"
  - subject: "KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "Did anyone try to run KDE 3.2 Beta 2 and XFree86 4.4.0 RC1 together. I installed them here but KDE just doesn't want to start :(. GNOME works fine though."
    author: "JLP"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "are you using binary packages ?\nif so, I guess that there are linked to the old XFree libraries. You might want to recompile everything."
    author: "JC"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "Yup binaries for both. Is there so much difference in 4.4.0 that everything needs to be recompiled? Ah well it looks like I will have to wait on this machine :("
    author: "JLP"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "Try compiling only kdebase and kdelibs against your new X, and install the rest as binaries. Might work."
    author: "Anonymous Coward"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "Xlibs should be backwards-compatible, so there should be no need to recompile apps using older Xlibs in order to run with newer ones. This looks like a bug, but it's hard to say where and whose without knowing details.\n\n"
    author: "Lubos Lunak"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "Yeah sounds logical. Then we would have to compile everything just because of upgrading XFree86. And considering that all other WMs work just fine then I guess KDE should also work fine. I guess I will have to file a bug, Would it be best to report it here, at XFree86 or maybe even both?"
    author: "JLP"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-21
    body: "i'm using beta2 with xfree8t rc1 of 4.4.0\n\neverything is working ok, the only problem is, if i use the kde-option: \"restore previous session\" my kde hangs after restarting, cpu time is up to 99%. after a few minutes, my xserver freezes. i think it could easily be a bug, but i'm not shure yet if it's successional with the new xfree86. \n\nthe only solution i found until now is to remove the directory KDEHOME/share/config/session. if i do so, my kde starts up fine, without any components of the last session, shurely.\n\noh yes, and top said the 99% of cpu time comes from tree or four kdeinit processes wich uses each about 30%.\n\ngreeings"
    author: "tizz"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "This is what I get when I try to run KDE:\n\nstartkde: Starting up...\nLoading required GL library /usr/lib/libGL.so.1\nkdeinit: Launched DCOPServer, pid = 3684 result = 0\nDCOP: register 'anonymous-3684' -> number of clients is now 1\nDCOP: unregister 'anonymous-3684'\nkdeinit: Launched KLauncher, pid = 3688 result = 0\nDCOP: register 'klauncher' -> number of clients is now 1\nDCOP: unregister 'klauncher'\nDCOP: register 'klauncher' -> number of clients is now 1\nQPixmap: Cannot create a QPixmap when no GUI is being used\nQPixmap: Cannot create a QPixmap when no GUI is being used\nDCOP: new daemon klauncher\nkdeinit: Launched KRandrInitHack, pid = 3689 result = 0\nkdeinit: Launched KDED, pid = 3690 result = 0\nDCOP: register 'kded' -> number of clients is now 1\nDCOP: unregister 'kded'\nDCOP: register 'kded' -> number of clients is now 1\nLoading required GL library /usr/lib/libGL.so.1\nDCOP: register 'anonymous-3690' -> number of clients is now 2\nDCOP aborting call from 'anonymous-3690' to 'kded'\nDCOP: unregister 'kded'\nkded: ERROR: Communication problem with kded, it probably crashed.\nDCOP: unregister 'anonymous-3690'\nkdeinit: PID 3690 terminated.\nkdeinit: opened connection to :0.0\nkdeinit: Launched 'kcminit', pid = 3692 result = 0\nLoading required GL library /usr/lib/libGL.so.1\nkdeinit: PID 3692 terminated.\nkdeinit: Launched 'knotify', pid = 3693 result = 0\nDCOP: register 'knotify' -> number of clients is now 1\nLoading required GL library /usr/lib/libGL.so.1\nDCOP: register 'anonymous-3693' -> number of clients is now 2\nDCOP aborting call from 'anonymous-3693' to 'knotify'\nDCOP: unregister 'knotify'\nERROR: Communication problem with knotify, it probably crashed.\nDCOP: unregister 'anonymous-3693'\nkdeinit: PID 3693 terminated.\nkdeinit: Got KWRAPPER 'ksmserver' from socket.\nWARNING: NAME not specified in initial startup message\nkdeinit: PID 3696 terminated.\nLoading required GL library /usr/lib/libGL.so.1\nstartkde: Shutting down...\nkdeinit: terminate KDE.\nklauncher: Exiting on signal 1\nDCOP: unregister 'klauncher'\nstartkde: Running shutdown scripts...\nstartkde: Done."
    author: "JLP"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-08
    body: "Analysis of log:\nBoth kded and knotify crash and probably ksmserver as well.\n\nThe \"Loading required GL library /usr/lib/libGL.so.1\" looks suspicious, GL tends to cause heaps of problems."
    author: "Waldo Bastian"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2004-01-04
    body: "You *might* want to clean out all your old DCOP sockets and ICE files.\n \nI had similar problems when I upgraded X once with one of the 3.0 series of KDE  --  on my system the .DCOP files in the ${HOME} directory and the .ICE files appeared to be tied to something specific in X. ...\n  It cause the KDE startup to hang for a while and then die horribly .. leaving X hung.\n\n   -- Just a possibility to be explored."
    author: "AJFT"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-09
    body: "I have kde cvs from yesterday running on X-4.3.99.016.\nrunning on a radeon 9200se , really good."
    author: "wb"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2003-12-27
    body: "Not RC1 but I just installed RC2 last night over 4.3.0. It seems that by default 4.4.0 installs its base xinitrc-overwriting my existing xinitrc link.\n\nKde would start right up after making a minor adjustment by (re)moving the base xinitrc installed by 4.4.0 and creating a new xinitrc link to point to my initrc.kdm script. Before that, X would use XDM.\n\nRobert\n\n"
    author: "Robert Inglis"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2004-01-09
    body: "I have the same problem. I have had some problems with my Intel i845 graphics card at work. If I leave the computer alone for 5 min X crashes, so I decided to try to upgrade X to 4.4.0 RC2. After installing the binaries, it started the fwm window manager. After lynxing around on the net I finally learned that I had to change the .xinitrc file to exec startkde\nX starts, but then quits with the following messages on screen:\n\nXFree86 Version 4.3.99.902 (4.4.0 RC 2)\nRelease Date: 18 December 2003\nX Protocol Version 11, Revision 0, Release 6.6\nBuild Operating System: Linux 2.4.18-14 i686 [ELF] \nCurrent Operating System: Linux xxx 2.4.22-10mdk #1 Thu Sep 18 12:30:58 CEST 2003 i686\nBuild Date: 20 December 2003\nChangelog Date: 19 December 2003\n\tBefore reporting problems, check http://www.XFree86.Org/\n\tto make sure that you have the latest version.\nModule Loader present\nMarkers: (--) probed, (**) from config file, (==) default setting,\n\t(++) from command line, (!!) notice, (II) informational,\n\t(WW) warning, (EE) error, (NI) not implemented, (??) unknown.\n(==) Log file: \"/var/log/XFree86.0.log\", Time: Fri Jan  9 16:05:04 2004\n(==) Using config file: \"/etc/X11/XF86Config-4\"\nSymbol ShadowFBInit from module /usr/X11R6/lib/modules/drivers/i810_drv.o is unresolved!\nCould not init font path element /usr/X11R6/lib/X11/fonts/CID/, removing from list!\nstartkde: Starting up...\nLoading required GL library /usr/lib/libGL.so.1\nLoading required GL library /usr/lib/libGL.so.1\nDCOP aborting call from 'anonymous-3380' to 'kded'\nERROR: Communication problem with kded, it probably crashed.\nLoading required GL library /usr/lib/libGL.so.1\nLoading required GL library /usr/lib/libGL.so.1\nDCOP aborting call from 'anonymous-3383' to 'knotify'\nERROR: Communication problem with knotify, it probably crashed.\nLoading required GL library /usr/lib/libGL.so.1\nstartkde: Shutting down...\nKLauncher: Exiting on signal 1\nstartkde: Running shutdown scripts...\nstartkde: Done.\n\n\nwaiting for X server to shut down \n\n\n\n\nMy .xsession-errors file gives the following information:\n\nstartkde: Starting up...\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nkbuildsycoca running...\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nmcop warning: user defined signal handler found for SIG_PIPE, overriding\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nmcop warning: user defined signal handler found for SIG_PIPE, overriding\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nkonsole: cannot chown /dev/pts/0.\nReason: Operation not permitted\nkonsole_grantpty: determined a strange device name `/dev/ptmx'.\nkonsole: chownpty failed for device /dev/pts/0::/dev/pts/0.\n       : This means the session can be eavesdroped.\n       : Make sure konsole_grantpty is installed in\n       : /usr/bin/ and setuid root.                                              \nWARNING: KDE detected X Error: BadDrawable (invalid Pixmap or Window parameter)\\ \\x09\n  Major opcode:  \\x0e\nkonsole_grantpty: determined a strange device name `/dev/ptmx'.\nQClipboard::setData: Cannot set X11 selection owner for PRIMARY\nstartkde: Shutting down...\nKLauncher: Exiting on signal 1\nstartkde: Running shutdown scripts...\nstartkde: Done.\n\n\nI was looking through all the startup scripts (there are quite a lot of them in different places...) until I realised I could try startgnome instead, and lo and behold, it starts fine.\n\nI'm currently looking for the initrc.kdm file another poster mentioned, so I can get KDE to work again, but no luck finding it yet. Also, I'm now pretty sure that its the graphics card drivers that caused my original problems, its the procedure LpWaitRing() in the driver which causes the crash after 5 min with the following message in the logs:\nDec  9 15:16:08 lars kernel: [drm:i830_wait_ring] *ERROR* space: 131048 wanted 131064\nDec  9 15:16:08 lars kernel: [drm:i830_wait_ring] *ERROR* lockup\n\n\n"
    author: "Lars Westergren"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2004-04-26
    body: "I've got the same problem with Intel845/855\nDid you find answer??\n\n"
    author: "Martin"
  - subject: "Re: KDE 3.2 Beta 2 + XFree86 4.4.0 RC1"
    date: 2004-05-23
    body: "I have the same Problem, too \nAcer Travelmate 243LC\n\n:-(("
    author: "Aberhatschi"
  - subject: "Anyone know if redhat fedora packages work on RH9?"
    date: 2003-12-08
    body: "Title says it all. I'd appreciate the response.\nThanks\nCPH"
    author: "cph"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2003-12-08
    body: "I tried on an up to date RH 9 system.  Got the following errors/dependency problems:\n\nerror: Failed dependencies:\n        libdb_cxx-4.1.so is needed by kdeaddons-3.1.94-0.2\n        kdeaddons = %{epoch}:3.1.94-0.2 is needed by kdeaddons-atlantikdesigner-3.1.94-0.2\n        libsensors.so.2 is needed by kdebase-3.1.94-0.2\n        libsmbclient.so.0 is needed by kdebase-3.1.94-0.2\n        libieee1284.so.3 is needed by kdegraphics-3.1.94-0.1\n        libiw.so.26 is needed by kdenetwork-3.1.94-0.1\n        libdb-4.1.so is needed by kdesdk-3.1.94-0.1\n        libdb-4.1.so is needed by kdevelop-3.0.0b2-0.2\n        libMagick-5.5.6-Q16.so.0 is needed by koffice-1.2.94-0.2\n        libqassistantclient.so.1 is needed by (installed) PyQt-3.5-5\n"
    author: "Ian"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2003-12-09
    body: "They work flawlessly here. Just satisfy the dependencies and it flies like an angel!\nCheers\nQuart"
    author: "Quart"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2003-12-11
    body: "I tried:\n\n rpm -i kdebase-3.1.94-0.2.i386.rpm kdelibs-3.1.94-0.2.i386.rpm arts-1.1.94-0.1.i386.rpm qt-3.2.3-0.2.i386.rpm redhat-artwork-0.88-2.i386.rpm\n\nI got:\n\n  libsensors.so.2 is needed by kdebase-3.1.94-0.2\n  libsmbclient.so.0 is needed by kdebase-3.1.94-0.2\n\nI have lm_sensors installed which provides libsensors.so.1 only, and I don't know where to find the libsmbclient - it is not in the samba-client package.\n\nMichael\n"
    author: "Michael Will"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2004-01-05
    body: "Hi.\n\ntry using rpmfind.net. Here is waht I got, when i searched for: \"libsmbclient.so.0\":\n\n\"http://rpmfind.net/linux/rpm2html/search.php?query=libsmbclient.so.0\"\n\nI would try to use the packages for \"iRed Hat Beta Workstation taroon for i386\"\n\nhere's the link to the package:\n\n\"ftp://rpmfind.net/linux/redhat/beta/taroon/en/ws/i386/RedHat/RPMS/samba-common-3.0.0-2beta3.3E.i386.rpm\"\n\nCheers!"
    author: "Christoph"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2004-02-07
    body: "Tell us how you satisfied the dependencies and like what I have done. At the beginning there were a lot of dependencies missing and start thinking and tried a lot of ways and it kind resolved one but poped out others. So I went to upgrade my sistem using the apt-get upgrade (this is redhat9). I used these two repository\n\n# Red Hat Linux 9\nrpm http://ayo.freshrpms.net redhat/9/i386 os updates freshrpms\n#rpm-src http://ayo.freshrpms.net redhat/9/i386 os updates freshrpms\n                                                                                    \n# Dag Apt Repository for Red Hat 9\nrpm http://apt.sw.be redhat/9/en/i386 dag\n\nCould be some other repository but I couldn't locate. The dag one took a lot of the work and this are the last one that couldn't been done\n\n        libdb_cxx-4.1.so is needed by kdeaddons-3.2.0-0.1\n        libsensors.so.2 is needed by kdebase-3.2.0-0.1\n        libsmbclient.so.0 is needed by kdebase-3.2.0-0.1\n        libieee1284.so.3 is needed by kdegraphics-3.2.0-0.1\n        libdb-4.1.so is needed by kdesdk-3.2.0-0.1\n        libmysqlclient.so.10 is needed by qt-MySQL-3.2.3-0.2\n\nLets us know how could this dependencies be satisfy.\n\nThank you for sharing.\n\nRosendo from Todos Santos, Guatemala\n"
    author: "Qman Txun"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2004-02-04
    body: "I (as well) am trying to install KDE 3.2 on RedHat 9. There is no such package (understandably, since RedHat 9 is \"dead\"). I tried the Fedora packages and found they depend on later versions of some system libraries, specifically\nlibsensors.so.2 and libsmbclient.so.0 are needed by kdebase-3.2.0-0.1\n\nlibsmbclient was pointed to in this thread. libsensors is found by rpmfind but only for a Mandrake distribution (and will that work???)\n\nBut by now I'm getting a \"should I be doing this\" feeling. Let me rephrase the question: should Fedora packages be used on RedHat 9? Is there a better alternative (SuSE 9 packages perhaps) for getting KDE 3.2 on RedHat 9?\n\nAnd is it reasonable to assume that RedHat 9 packages will not be forthcoming?\n\nThanks for any wisdom or anything close ;-)\n\nMichael Kairys\nSL Corporation\n"
    author: "Michael Kairys"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2004-02-05
    body: "On IRC chat #kde (Freenode) someone was advocating using yum to get the proper packages for resolving this problem. YMMV"
    author: "jcwinnie"
  - subject: "Re: Anyone know if redhat fedora packages work on "
    date: 2004-07-03
    body: "I found a source for libsmbclient.so.0 at http://samba.org/~jerry/RPMS/samba/RedHat/RPMS/i386/9.0/ and that seems to work.  As for the other dependencies, I'm still working on it.  Some day, I will be able to run Apollon!"
    author: "Lyleonline"
  - subject: "can't wait to test it :)"
    date: 2003-12-08
    body: "i tested the beta1 release on my suse 8.2 installation, and it was impressive.\nnow i can't wait to test the beta2 for suse 9. :)"
    author: "cl"
  - subject: "Where is Gideon?"
    date: 2003-12-08
    body: "kde.org says, that Gideon/Kdevelop is released too?\n"
    author: "anonymous"
  - subject: "Re: Where is Gideon?"
    date: 2003-12-08
    body: "right here ... it is not included in the KDE-sources\n\nhttp://www.kdevelop.org/index.html?filename=download.html"
    author: "S\u00f8ren Holm"
  - subject: "Re: Where is Gideon?"
    date: 2003-12-08
    body: "The mirror I looked at has it. Try another?"
    author: "teatime"
  - subject: "Dependency problems on SuSE 9.0"
    date: 2003-12-08
    body: "The beta 2 release appears to only have a few rpm's in it, far fewer than the beta 1 -- I assume that to install beta 2, I have to have beta 1 already installed?\n\nI hit a bunch of dependency problems when I tried to install either beta by downloading all the rpm's and running 'rpm -Fvh *.rpm' as the readme says.  It's missing libartsbuilder.so.0, libartsgui.so.0, libartsmidi_idl.so.0, kdelibs3, libvcard.so.0, libgnokii.so.0, etc. etc.  Some of these are supposed to be provided by some of the beta rpm's themselves, and others should be installed already in SuSE 9.0, so I don't understand why they can't be found.\n\n"
    author: "Brian Kendig"
  - subject: "Re: Dependency problems on SuSE 9.0"
    date: 2003-12-08
    body: ">far fewer than the beta 1\nnope, as many packages as beta 1 (leave out the debug-rpms)\nyou don't need to have beta 1 installed.\n\n>'rpm -Fvh *.rpm'\ntry without the -F option. i installed _all_ packages available for my arch\n(suse 9.0 / ix86), after that i installed the noarch pkg. works for me at least...\ngood luck!"
    author: "cl"
  - subject: "Re: Dependency problems on SuSE 9.0"
    date: 2003-12-09
    body: "some of the mirrors don't have all the files ... try the KDE FTP =("
    author: "TuxCane"
  - subject: "Re: Dependency problems on SuSE 9.0"
    date: 2003-12-09
    body: "Even I don't find the artsbuilder package... for SuSE 9.0"
    author: "Mathi"
  - subject: "Re: Dependency problems on SuSE 9.0"
    date: 2003-12-10
    body: "A good thing is to look in the supplementary directory on any ftp that has the SuSE distribution.\n\nAnd dont use --nodeps!! Solve the conflicts, otherwise U end up with a broken dist!\n\n/DiCkE"
    author: "DiCkE"
  - subject: "Mmmmm...."
    date: 2003-12-08
    body: "Once Konstruct gets updated I'll burn a few cycles and compile this baby. :) I've loved beta1 although it has had a few rough edges, will be interesting to see what has changed. Konqueror especially seems to be improved a lot based on the CVS-Digests."
    author: "Chakie"
  - subject: "Not updated?"
    date: 2003-12-08
    body: "From http://developer.kde.org/build/konstruct/ : \"Unstable Version - This version installs the current unstable development releases of KDE and applications. Currently it gives a complete KDE 3.2 Beta 2 installation.\""
    author: "Anonymous"
  - subject: "Where to get debian binaries?"
    date: 2003-12-08
    body: "Is there any place with \"bleeding edge\" kde packages for debian?\nI always used the CVS builds on http://opendoorsoftware.com/cgi/http.pl?p=kdecvs but the last build is from Nov 5 AFAIK even pre beta1 not exactly what I'd call bleeding edge =)"
    author: "l/p: anonymous/e-mail"
  - subject: "Re: Where to get debian binaries?"
    date: 2003-12-09
    body: "I've been wondering the same thing! I was using the CVS apt sources, but once they stopped updating I just went and compiled it from CVS for myself. Not quite as fast, is it? Well, for now that will have to suffice. Perhaps you could try getting in touch with the guy who was generating those packages and see what the status is. Probably he's just busy with final exams or something ;-)"
    author: "deaththrasher"
  - subject: "It looks like KMail doesn't like Kontact"
    date: 2003-12-08
    body: "I was running kde 3.1.4+kontact and then upgraded to Dobra Voda(btw why is it named good water.....) and tried to run KMail well that didn't go very well because Kontact was using dimap and when I ran Kmail it couldn't read all the stuff that Kontact put in there.... I thought that Kontact synced with Kmail's sources. Should I put this on the buglist of kmail or Kontact?"
    author: "Boris Kurktchiev"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-08
    body: "AFAIK, Dobra Voda is a Checz brand for mineral water. They probably had that a lot at Nove Hradi :-)"
    author: "Andr\u00e9 Somers"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-09
    body: "Following that logic, 3.2 final must be named Budvar?\n"
    author: "Rob Kaper"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-09
    body: "No, final would be Plzensky Pradsroj."
    author: "Jaana"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-09
    body: "Mmmm... Budvar"
    author: "Z"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-10
    body: "Certainly Budvar :)) I'm bit of a patriot so nothing else is an option for me (I live in Ceske Budejovice (or Budweis for german-lovers)) :)))"
    author: "dragon"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-09
    body: "Yeah, and that brand is named after the village of the same name, which is right next to Nove Hrady."
    author: "Lukas Tinkl"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-09
    body: "i would say that \"Dobra Voda\" has strong association with \"vodka\" for us :)) "
    author: "somebody_from_russia"
  - subject: "Re: It looks like KMail doesn't like Kontact"
    date: 2003-12-09
    body: "Hmm, it seems that we forgot to think about an upgrade path for people who tried Kontact 0.2. I'd say you should file your bug report for the Mail component of Kontact.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "An official KDE screenshots script?"
    date: 2003-12-08
    body: "Whenever a new build/release of KDE gets thrown into the wild, there are always requests for screenshots. Some screenshots will be posted, at random places, of random windows, using 'random' styles etc. \n \nWouldn't it be possible to create some nice script which make all kinds of magic DCOP calls and opens a 'standard' list of apps, snapshots each, if possible selects options in apps etc, snapshots those. \n \nThis way people could simply run that script, create a huge amount of usefull common screenshots, even using different styles/icons with ease.Hopefully this idea is somewhat new, here goes.. \n \nWhenever a new build/release of KDE gets thrown into the wild, there are always requests for screenshots. Some screenshots will be posted, at random places, of random windows, using 'random' styles etc. \n \nWouldn't it be possible to create some nice script which make all kinds of magic DCOP calls and opens a 'standard' list of apps, snapshots each, if possible selects options in apps etc, snapshots those. \n \nThis way people could simply run that script, create a huge amount of usefull common screenshots, even using different styles/icons with ease."
    author: "ac"
  - subject: "Re: An official KDE screenshots script?"
    date: 2003-12-09
    body: "Did somebody ask for random screenshots? :-)\n\nhttp://www.tomchance.org.uk/photos/kde3.2_beta2\n\nIf anybody wants me to take shots of particular things, just ask and I'll see if I can oblige."
    author: "Tom"
  - subject: "Re: An official KDE screenshots script?"
    date: 2003-12-09
    body: "More screenshots at http://perso.wanadoo.fr/shift/KDE3.2/"
    author: "Anonymous"
  - subject: "Re: An official KDE screenshots script?"
    date: 2003-12-09
    body: "please : KDE 3.2 screenshots with plastik theme.\n\nsorry for my poor english"
    author: "pat"
  - subject: "Re: An official KDE screenshots script?"
    date: 2003-12-09
    body: "The links above only contain screenies from kde using the plastik theme"
    author: "cies"
  - subject: "Re: An official KDE screenshots script?"
    date: 2003-12-09
    body: "What about a screenshot of non-trasnparent panels, I want to see how the pager looks like. The pager in the transparent panel looks great but what about ordinary panels?"
    author: "nobody"
  - subject: "fastest way to reach a large number of testers..."
    date: 2003-12-08
    body: "- make it available to debian, so everyone can try it with a simple apt-get.\n\nSeriously.  If debian users can expect to install/uninstall this as easily as other packages, why wouldn't they try it out?  It'd be worth the effort of providing debs, at least :)"
    author: "Lee"
  - subject: "Re: fastest way to reach a large number of testers..."
    date: 2003-12-08
    body: "KDE doesn't create binary packages, only sources.\nSo if Debian users want to use kde 3.2 beta2, they should provide the binary .deb versions themself, just like the other distributions..\n"
    author: "Rinse"
  - subject: "Re: fastest way to reach a large number of testers"
    date: 2003-12-08
    body: "See http://opendoorsoftware.com/cgi/http.pl?p=kdecvs\n\nOrth's kdecvs debs are highly recommended\n\nor if you use gentoo (like I have recently), try:\n\nhttp://iamlarryboy.dyndns.org/\n\n(for binary builds.. portage already has beta2 for a few days in terms of a source build)"
    author: "anon"
  - subject: "Re: fastest way to reach a large number of testers"
    date: 2003-12-09
    body: "yea gentoo!! I don't know how they do it, but stuff goes up on portage right away every time. And if it's not on portage, someone has written an ebuild for it."
    author: "Ryan"
  - subject: "Compiling"
    date: 2003-12-08
    body: "Alas, it would have to come out right as I finishedcompiling the first beta.  Questions: 1) When I compile these packages, what level of debug symbols should I enable ... no, yes, or full? 2) Is there anyway to separate the debug symbols from the code so they only have to be loaded when there is a crash? 3) How much speedup is gained by using anti-debugging features like -fomit-frame-pointer and -frename-registers?"
    author: "Luke Sandell"
  - subject: "Re: Compiling"
    date: 2003-12-08
    body: "4) How do I use the .xdelta patches?"
    author: "Luke Sandell"
  - subject: "Re: Compiling"
    date: 2003-12-09
    body: "Hi,\n\n\n\nYou have to use the xdelta tools, which you can find at http://xdelta.sourceforge.net.\n\n\n\nAfter that, imagine that you have kdebase-3.1.93.tar.bz2 in the current dir:\n\n\n\n$ bzip2 -d kdebase-3.1.93.tar.bz2\n\n$ xdelta patch kdebase-3.1.93-3.1.94.tar.xdelta\n\n$ bzip2 -9 kdebase-3.1.94.tar\n\n\n\nThe you'll have the new source code ready for compiling :)\n\n\n\nRegards."
    author: "Miguel"
  - subject: "Re: Compiling"
    date: 2003-12-09
    body: "ad 1) If you experience crashes then the resulting backtraces are often not very useful unless you compiled with --enable-debug=yes or full. So for a beta I recommend using yes (if you have enough free disk space). If you don't have that much disk space you should at least compile kdelibs with debug symbols. Then you can re-compile everything else with debug symbols if you need them (for reporting a crash).\n\nad 2) AFAIK the debug symbols are not loaded into memory unless they are needed, i.e. only when the app crashed. So there's just a little speed penalty but no memory penalty when you run binaries with debug symbols.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Compiling"
    date: 2003-12-11
    body: "Be careful with these compile flags...\n\nI'm using Gentoo and had to disable one, since I wasn't able to upgrade libc / gcc (can't remember) had to do an awful lot of recompiling to get things fixed.\n\nTime you gain: might be enough to blink your eyes, but I guess you won't notice, but might be able to meassure ;-)"
    author: "Jord"
  - subject: "Dobra Voda"
    date: 2003-12-09
    body: "Well, the lemon water was tolerable, but I don't think there's anything in the world that makes me want to drink beer more than Dobra Voda.  Beer was quite refreshing in comparison."
    author: "George Staikos"
  - subject: "Re: Dobra Voda"
    date: 2003-12-09
    body: "Maybe next release should be called \"Dobro Pivo\"?"
    author: "keber"
  - subject: "Re: Dobra Voda"
    date: 2003-12-09
    body: "actually \"Dobre pivo\", or simply \"Plzen dvanactka\" :)"
    author: "greetings from czech republic"
  - subject: "Re: Dobra Voda"
    date: 2003-12-09
    body: "it depends on which language you speak.\n\nDobra voda doesn't mean \"good water\" only in czech, you know?\n\nit's the same in slovene, where \"good beer\" is spelled Dobro pivo ;)\n\ni was pretty sure that there'd be someone who'd notice the \"good water\" in this beta :D\n\nbtw: where did the idea for this name come from? ...is there a place in the Chech republic called Dobra Voda (where the KDE team met?)"
    author: "Matija \"hook\" Suklje"
  - subject: "Re: Dobra Voda"
    date: 2003-12-09
    body: "there's a post on this topic couple of lines below this one. basically yes, although only god knows what those geeks had on their mind when choosing this release name :)"
    author: "greetings from czech republic"
  - subject: "Re: Dobra Voda"
    date: 2003-12-10
    body: "yea, i've seen it after i posted the question ...but i like the name :D ...it's even better then Rudi :I"
    author: "Matija \"hook\" Suklje"
  - subject: "where can I find Mandrake binary?"
    date: 2003-12-09
    body: "I really want to give it a try, but do not know where can I get a binary for my MDK 9.2"
    author: "Paul"
  - subject: "Re: where can I find Mandrake binary?"
    date: 2003-12-09
    body: "I don't know if beta2 is there, but fairly recent version of cvs are in cooker.\nIt seems to be updated every few days at least."
    author: "JohnFlux"
  - subject: "Re: where can I find Mandrake binary?"
    date: 2003-12-09
    body: "Have you tried cooker?\n\nMaybe you should stop by http://plf.zarb.org/~nanardon/ and add a cooker repository"
    author: "Mario"
  - subject: "Super Nice"
    date: 2003-12-09
    body: "I just successfully installed 3.1.94 on SUSE 9.0 ... very nice! I'm totally blown away. 3.1.4 didn't make me too happy but most of my earlier complaints have been resolved.\n\nWhat happened to KMail though? How can I activate a different splash screen? (And who the hell thought to include the XP one?!? Isn't KDE light years beyond fisher price?) Lastly, what about window decorations? There's only a handful selectable and I can't install new ones.\n\nGo KDE3.2!"
    author: "TuxCane"
  - subject: "Re: Super Nice"
    date: 2003-12-09
    body: "Make sure you install kdeartwork .. that has more styles/window decorations.. I really like the new Plastik one.\n\nUnfortunately, the window decorations for 3.1 will not direclty port to 3.2. There is a knifty window decoration for 3.2 at \n\nhttp://www.kde-look.org/content/show.php?content=8841\n\nThis does compile with 3.2 and looks pretty nice as well.\n\nMost styles for 3.1 should compile and work well with 3.2 (based on my experience).\n\ncheers\nOsho"
    author: "Anonymous"
  - subject: "Who installed it successfully on SuSE 9.0?"
    date: 2003-12-09
    body: "Hi!\n\nI would like to give this beta a try. Did anybody install it already on SuSE 9.0? How? I tried installing the first 3.2 beta, but it locked up my system and I needed to reinstall the distribution!\n\nThanks."
    author: "melster"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-09
    body: "I downloaded the rpms, removed the old KDE rpms from init 3, and installed the new ones with rpm -ivh *.rpm. I tested first with rpm --test -i *.rpm.\n\nWhere did it lock? Why reinstall the whole thing? If you lost the use of KDE, why not just use yast from the command line to reinstall KDE 3.1.4? Sounds like you just made more work for yourself to me! KDE is not the whole distro..\n\nGood luck next time!"
    author: "Max Howell"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-09
    body: "Ok, I've done exactly this:\n\n<as root>: init 3\nremoved all kde packages\nrpm -ihv *.rpm\n(lots of dependency complaints)\nrpm -ihv --nodeps *.rpm\n\ninit 5\n(old xdm starts)\n\nNo luck, no KDE, my system is starting fvwm2 instead of KDE!!!\n\nWhat's next?\n"
    author: "melster"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-09
    body: "Install kdm? Then you can choose which wm/de to start. If kdm isn't starting and is installed look up how to set it to start. can't help you there.\n\nOtherwise try startkde from init 3, I think that works, can't remember."
    author: "Max Howell"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-09
    body: "> What's next?\n\nA broken system due to the use of \"--nodeps\" ?"
    author: "Andy Goossens"
  - subject: "Newbie-rule (nodeps = nicht f\u00fcr Deppen)"
    date: 2003-12-10
    body: "Absolutely. I think anyone recommending --nodeps should be shot. \n\nNEWBIE RULE: NEVER USE --nodeps.\nuntil you know what you are doing and can fix your broken system manually afterwards. But then you are not a Newbie anymoe\n\nEven if a vendor recommends this, don't do it. Seriously. Contact your vendor for packages that are not broken or pick a new distributor.\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-10
    body: "mmm set kdm at variable DISPLAYMANAGER in /etc/sysconfig/displaymanager and run SuSEconfig... work for me.\n\nDISPLAYMANAGER=\"kdm\""
    author: "deabru"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-09
    body: "I successfully installed 3.1.94 on my SUSE 9.0 machine without problems ... i just used KPackage to update all the KDE packages that i had installed (first arts, then libs, then the rest as mentioned in the install FAQ)\n\nKDE3.2b2 is up and running fine ... "
    author: "TuxCane"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-10
    body: "Let me get this right, You upgraded to beta2 right from within KDE?! Were You already running beta1?\nI tried this once with beta1 and ended up having a broken KDE and installed all KDE 3.1.4 rpms from the dvd again and beta1 via konstruct."
    author: "JJ"
  - subject: "Re: Who installed it successfully on SuSE 9.0?"
    date: 2003-12-11
    body: "There's an install FAQ?  Where?  I've looked and looked...\n\nI'm still getting dozens of dependency errors when I try to install this on my vanilla SuSE 9.0 system.  Lots of libarts* errors, though I have arts installed and I don't know what other arts packages I need.  libvcard.so.0, even though I have kdelibs3 installed.  It also tells me \"kdebase3-SuSE <= 9.0 conflicts with kdebase3-3.1.94-1\", but I don't know where to get a new kdebase3-SuSE from.\n\n"
    author: "Brian Kendig"
  - subject: "kdemultimedia couldn't create artsmoduleseffects.h"
    date: 2003-12-09
    body: "I compiled beta1 successfully on the same machine that I'm trying now to compile beta2.  The problem is that a bunch of headers, one of which is artsmoduleseffects.h, never get generated while compiling kdemultimedia.  All the other packages compiled, except multimedia.\n\nIs there anything I'm missing?\n\nThanks,\nIvanK."
    author: "chepati"
  - subject: "Re: kdemultimedia couldn't create artsmoduleseffects.h"
    date: 2003-12-09
    body: "Don't use --enable-final for kdemultimedia or apply <a href=\"http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/konstruct/kde/kdemultimedia/files/fix-final.diff?rev=1.1&content-type=text/plain\">fix-final.diff</a> before ./configure\n"
    author: "binner"
  - subject: "Re: kdemultimedia couldn't create artsmoduleseffects.h"
    date: 2003-12-09
    body: "Thanks, that  did it!\n\nIvanK."
    author: "chepati"
  - subject: "Dobra Voda?"
    date: 2003-12-09
    body: "Why was kde 3.2 beta 2 named  \"Dobra voda\"?"
    author: "jerk"
  - subject: "Re: Dobra Voda?"
    date: 2003-12-09
    body: "http://www.radio.cz/en/article/47099: \"Just a few kilometers away from Nove Hrady lies Dobra Voda, translated into English as Good Water. It is a place of pilgrimage, known for its (you guessed it) good water. But if you are thinking of the bottled water sold around the country bearing the same name, it is not from the same place. In fact, there are over fifteen places called Dobra Voda around the country and many get this peaceful little village confused with the nearby \"bottled water\" town of Ceske Budejovice.\"\n"
    author: "Anonymous"
  - subject: "The Fedora Version"
    date: 2003-12-09
    body: "I have KDE 3.2 Beta2 installed in Fedora Core 1.0, I used\nthe RPM's available.\nI have a problem with the response when clicking on a program in the\ntastbar. Lets say I want to maximize Konqueror, I click on the icon in\nthe taskbar, no respone. I click once more, it opens. I want to minimize, click\n2 times, no response, the third time it minimizes.\n\nAlso, clicking on the K in the left corner can give a hangup where I \nneed to press Escape to come out of it.\n\nAnyone seen this ?\n\nMartin."
    author: "Martin A"
  - subject: "Re: The Fedora Version"
    date: 2003-12-09
    body: "had the same prob with beta1 and fedora, especially when using firebird.\nit seems that the klipper app did steal the focus somehow...\ni disabled klipper and now it works ok.\n\nhope this helps,\nlars"
    author: "tf"
  - subject: "Re: The Fedora Version"
    date: 2003-12-09
    body: "Thanks for answering. I didn't know Klipper\nwas involved. I had actions disabled, and after\nenabling actions it is much better. I can't close\nKlipper because there's no respons when trying to\nconfigure.\n\nMartin."
    author: "Martin A"
  - subject: "Re: The Fedora Version"
    date: 2003-12-10
    body: "Cool! I had the exact same problem, it is working now. \n\n\n\nRight-click on the panel (outside klipper) -> remove -> mini-app -> klipper"
    author: "Roger Soares"
  - subject: "Re: The Fedora Version"
    date: 2003-12-21
    body: "have da same problem with my selfcompiled version on debian... without klipper, it works for me too... but what if i need klipper ?!"
    author: "tizz"
  - subject: "Re: The Fedora Version -> And other versions too!"
    date: 2003-12-11
    body: "I have had the same problem as you guys but I am running on Gentoo from compiled sources!  I'm glad that at least a few othe people have seen this bug, is it formally submitted to the KDE developers yet?"
    author: "anonymous"
  - subject: "changelog??"
    date: 2003-12-09
    body: "what are the differences between beta1 and beta2 ?"
    author: "HelloWorld"
  - subject: "Re: changelog??"
    date: 2003-12-09
    body: "Read through http://lists.kde.org/?l=kde-cvs&r=1&w=2 or read the weekly CVS Digest for summaries."
    author: "Datschge"
  - subject: "Mandrake?"
    date: 2003-12-09
    body: "\nWhy aren't there binaries for Mandrake?\n\nI'd like to try the betas, but compiling from source is way over my head.\n\nI suppose there's a good reason.\nSomebody knows and wants to share?\n\n\nCheers,\n\nCarlos Cesar"
    author: "Carlos Cesar"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "There are binaries in Mandrake Cooker."
    author: "Stephen Douglas"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "> Why aren't there binaries for Mandrake?\n\nKDE doesn't make binaries, it only provides source. Ask Mandrake to make RPMs.\n\nBTW, Beta2 RPMs are in Cooker."
    author: "Andy Goossens"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "\nI was asking Why?\n\nI know, ask Mandrake. Thanks a lot.\n\n\nCheers,\n\nCarlos Cesar"
    author: "Carlos Cesar"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "Both Andy and myself have told you - there are binaries in Mandrake Cooker.\n\nYou're welcome."
    author: "Stephen Douglas"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "so where are they, I can find them in an mandrake cooker dir... only kde 3.1.4, or am I doing something wrong?!?!?"
    author: "superstoned"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "\nStephen (and Andy):\n\nThanks for answering, but it seems to be some disconnection between us.\n\nYou've answered WHERE. I was asking WHY.\n\nTo make my question clearer:\n\nI know that KDE doesn't do binaries, but IIRC, until MDK 8.2 there were links for binaries in the KDE.org site. I guess that meant MDK made binaries.\nSince MDK9.0 there are no more.\n\nI was under the impression that Mandrake was one of the most (if not THE most) KDE friendly distro.\nSo, does anybody knows WHY Mandrake banned the KDE betas to the unfriendly Cooker?\n\n\n\n\nTIA,\n\nCarlos Cesar"
    author: "Carlos Cesar"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "Cooker becomes the next Mandrake release (version 9.3 or 10.0, it's still too early to decide). So it makes sense to put the KDE 3.2 beta's in there.\n\nBut Mandrake doesn't support running this beta on a 'stable' Mandrake version. Supporting it with binaries causes a *lot* work for the packagers. Users might flood their mailing lists describing bugs and solving those just isn't worth the effort.\n\nIf you want to try KDE 3.2 beta2 on your Mandrake system, use CVS, or switch to Cooker. Or you might use third-party RPMs from packagers who have the time to support them."
    author: "Andy Goossens"
  - subject: "Re: Mandrake?"
    date: 2003-12-09
    body: "All nice and dandy, makes sense. TAL.\n\n\nBut MDK hasn't even released RPMS for 3.1.4 FINAL...\n\nAsk Mandrake!\nExcept that  lately, they've become adepts to bullshitting and PR answers.\n\n\nGuess I'll never find out.\n\n\nCheers,\nCarlos Cesar\n\n"
    author: "Carlos Cesar"
  - subject: "Re: Mandrake?"
    date: 2003-12-10
    body: "Umm, I was recently running 3.1.4 and am now running 3.2 beta 2, all installed from cooker rpms (except a few bits and pieces I'm hacking on)\n\nWhat is the problem? Mandrake putting beta packages in their unstable distro? Wow, the shame."
    author: "Stephen Douglas"
  - subject: "KMail"
    date: 2003-12-09
    body: "in which package is kmail? or did we completely get rid of this app? thanks for the great job! i love 3.2!!!!"
    author: "Anonymous"
  - subject: "Re: KMail"
    date: 2003-12-09
    body: "kmail is in kdepim and not in kdenetwork anymore."
    author: "JC"
  - subject: "Re: KMail"
    date: 2003-12-09
    body: "in kdepim"
    author: "Fred"
  - subject: "Damn, it's good"
    date: 2003-12-09
    body: "Snappy as hell and Konqueror only gets better.\nI think it's gonna be killer once it's ready.\n"
    author: "frappa"
  - subject: "Re: Damn, it's good"
    date: 2003-12-09
    body: "Konqueror NEVER gets ready and therefore won't be usable as a web browser."
    author: "kdeuser"
  - subject: "Re: Damn, it's good"
    date: 2003-12-14
    body: "come on, Konqi kicks all other browser's asses already, and will only get better...\n\nI mean, firebird can only *look at konqi from a long distance*, can't even dream about the features konqi offers (the tabbed browsing is so much better, for example, ever tried to drag'n'drop a firebird tab? move it? dublicate it? detatch it? let it open after current *active* tab? HAHAHAHA you will have to look for all those silly plugins, install them, then you can hope firebird does it... HOPE...\n\nthats JUST the tabbed browsing (not talkin' bout identificaton-change, autorefresh, etc etc etc)\n\nand Konqi is faster\n\nand integrates better in KDE\n\nanything else???\n\n(yep, I love my konqi, really frustrating to work under XP with firebird now, I feel handicapped)"
    author: "superstoned"
  - subject: "Re: Damn, it's good"
    date: 2003-12-14
    body: "> (yep, I love my konqi, really frustrating to work under XP with\n> firebird now, I feel handicapped)\n\nInstall Cygwin and KDE-Cygwin and you'll have Konqueror in Windows.\n\nAt least it is supposed to work (I don't use windows, so I haven't tried)."
    author: "Henrique Pinto"
  - subject: "I cannot start kde"
    date: 2003-12-09
    body: "After compiling (i18n-de) with kontruct and setting the right Paths, I realized that there is no kdm or startkde in /bin.\n\nWhat I've made wrong? "
    author: "gensk"
  - subject: "Re: I cannot start kde"
    date: 2003-12-09
    body: "Search in $prefix/bin with prefix where you installed KDE to (Konstruct default is ~/kde3.2-beta2)."
    author: "Anonymous"
  - subject: "Re: I cannot start kde"
    date: 2003-12-09
    body: "I have to precise: I should call it \"~/kde3.2-beta2/bin\" instead of \"/bin\"\nThat's in ~/kde3.2-beta2/bin\n\n<BLOCKQUOTE>\n\nfile:/root/kde3.2-beta2/bin/artsc-config\nfile:/root/kde3.2-beta2/bin/artscat\nfile:/root/kde3.2-beta2/bin/artsd\nfile:/root/kde3.2-beta2/bin/artsdsp\nfile:/root/kde3.2-beta2/bin/artsmessage\nfile:/root/kde3.2-beta2/bin/artsplay\nfile:/root/kde3.2-beta2/bin/artsrec\nfile:/root/kde3.2-beta2/bin/artsshell\nfile:/root/kde3.2-beta2/bin/artswrapper\nfile:/root/kde3.2-beta2/bin/checkXML\nfile:/root/kde3.2-beta2/bin/cupsdconf\nfile:/root/kde3.2-beta2/bin/cupsdoprint\nfile:/root/kde3.2-beta2/bin/dcop\nfile:/root/kde3.2-beta2/bin/dcopclient\nfile:/root/kde3.2-beta2/bin/dcopfind\nfile:/root/kde3.2-beta2/bin/dcopidl\nfile:/root/kde3.2-beta2/bin/dcopidl2cpp\nfile:/root/kde3.2-beta2/bin/dcopidlng\nfile:/root/kde3.2-beta2/bin/dcopobject\nfile:/root/kde3.2-beta2/bin/dcopquit\nfile:/root/kde3.2-beta2/bin/dcopref\nfile:/root/kde3.2-beta2/bin/dcopserver\nfile:/root/kde3.2-beta2/bin/dcopserver_shutdown\nfile:/root/kde3.2-beta2/bin/dcopstart\nfile:/root/kde3.2-beta2/bin/glib-genmarshal\nfile:/root/kde3.2-beta2/bin/glib-gettextize\nfile:/root/kde3.2-beta2/bin/glib-mkenums\nfile:/root/kde3.2-beta2/bin/gobject-query\nfile:/root/kde3.2-beta2/bin/imagetops\nfile:/root/kde3.2-beta2/bin/kab2kabc\nfile:/root/kde3.2-beta2/bin/kaddprinterwizard\nfile:/root/kde3.2-beta2/bin/kbuildsycoca\nfile:/root/kde3.2-beta2/bin/kconf_update\nfile:/root/kde3.2-beta2/bin/kconfig_compiler\nfile:/root/kde3.2-beta2/bin/kcookiejar\nfile:/root/kde3.2-beta2/bin/kdb2html\nfile:/root/kde3.2-beta2/bin/kde-config\nfile:/root/kde3.2-beta2/bin/kde-menu\nfile:/root/kde3.2-beta2/bin/kded\nfile:/root/kde3.2-beta2/bin/kdeinit\nfile:/root/kde3.2-beta2/bin/kdeinit_shutdown\nfile:/root/kde3.2-beta2/bin/kdeinit_wrapper\nfile:/root/kde3.2-beta2/bin/kdesu_stub\nfile:/root/kde3.2-beta2/bin/kdontchangethehostname\nfile:/root/kde3.2-beta2/bin/kfile\nfile:/root/kde3.2-beta2/bin/kfmexec\nfile:/root/kde3.2-beta2/bin/kgrantpty\nfile:/root/kde3.2-beta2/bin/kimage_concat\nfile:/root/kde3.2-beta2/bin/kinstalltheme\nfile:/root/kde3.2-beta2/bin/kio_http_cache_cleaner\nfile:/root/kde3.2-beta2/bin/kio_uiserver\nfile:/root/kde3.2-beta2/bin/kioexec\nfile:/root/kde3.2-beta2/bin/kioslave\nfile:/root/kde3.2-beta2/bin/klauncher\nfile:/root/kde3.2-beta2/bin/kmailservice\nfile:/root/kde3.2-beta2/bin/kpac_dhcp_helper\nfile:/root/kde3.2-beta2/bin/ksendbugmail\nfile:/root/kde3.2-beta2/bin/kshell\nfile:/root/kde3.2-beta2/bin/ksvgtopng\nfile:/root/kde3.2-beta2/bin/ktelnetservice\nfile:/root/kde3.2-beta2/bin/kwrapper\nfile:/root/kde3.2-beta2/bin/libart2-config\nfile:/root/kde3.2-beta2/bin/lnusertemp\nfile:/root/kde3.2-beta2/bin/make_driver_db_cups\nfile:/root/kde3.2-beta2/bin/make_driver_db_lpr\nfile:/root/kde3.2-beta2/bin/mcopidl\nfile:/root/kde3.2-beta2/bin/meinproc\nfile:/root/kde3.2-beta2/bin/pkg-config\nfile:/root/kde3.2-beta2/bin/preparetips\nfile:/root/kde3.2-beta2/bin/xml2-config\nfile:/root/kde3.2-beta2/bin/xmlcatalog\nfile:/root/kde3.2-beta2/bin/xmllint\nfile:/root/kde3.2-beta2/bin/xslt-config\nfile:/root/kde3.2-beta2/bin/xsltproc\n\n<BLOCKQUOTE>"
    author: "gensk"
  - subject: "Re: I cannot start kde"
    date: 2003-12-09
    body: "How about installing kde/kdebase? :-) i18n/kde-i18n-de doesn't require or pull in kdebase."
    author: "Anonymous"
  - subject: "Re: I cannot start kde"
    date: 2004-07-08
    body: "I compiled using the Slackware Slackbuilds but there is no startkde, and i find / -name startkde and found nothing. Somthing is up. im going to try recompiling (i erased my kdebase directory so im crying atm)"
    author: "Sandman1"
  - subject: "two words"
    date: 2003-12-09
    body: "love it !\n\nthanks to all the hard working kde developers :)\nlars"
    author: "tf"
  - subject: "Everaldo's Kids Icons"
    date: 2003-12-09
    body: "Are they in this beta? I heard they entered kdeartwork, but i don't know if that's before or after tagging...\n\nMaybe now the apple \"lickable video game\" fans got a reason to switch"
    author: "Renato Sousa"
  - subject: "Re: Everaldo's Kids Icons"
    date: 2003-12-09
    body: "No, they went in after tagging."
    author: "Anonymous"
  - subject: "Bunch of errors while using konstruct"
    date: 2003-12-09
    body: "I tried to build beta2 with konstruct. Beta1 works 100% fine (and fast!), but I tried beta2 with a different user, to not mess up something.\nKonstruct throws out a lot of errors, but kde still runs, mostly.\n-when krusader is fetched, the md5 seems to be wrong. I have no proxies (neither ftp nor http) in between, so either the stored m5 or the archive is bad\n-kdeaddons/noatun-plugins fails, \"slang\" is missing according to ld, but ldd tells, it's there\n-compile of newpg gives lot of \"undeclared\" errors in maperror.c (therefor kmail-crypto fails)\n-kdebindings/dcopper1 tries to write to /usr/lib/perl5/site_perl/5.8.1/i586-linux-thread.multi\nI already run cvs up -dP and as already told, beta1 works without any problems."
    author: "Andy"
  - subject: "Re: Bunch of errors while using konstruct"
    date: 2003-12-10
    body: "Ok, the last cvs update already fixed the krusader md5 problem.\nReviewing the logs I noticed kdirstat is not completed due to some strange docbook error. newpg still suffers from dozens of missing declarations, dirmngr seems to have a pointer problem. the german kde-i18n gives some errors but works as far as I can tell.\n"
    author: "Andy"
  - subject: "Re: Bunch of errors while using konstruct"
    date: 2003-12-10
    body: "krusader and kdirstat are fixed in the current Konstruct version. The kmail-crypto target should work again for a clean installation (otherwise run \"make clean\" in libs/newpg, libs/dirmngr, libs/ksba and libs/libgcrypt and remove any traces of *ksba* and *gcrypt* in the installation directory). The other two problems have to be fixed in the tarballs, please file reports for them at bugs.kde.org."
    author: "binner"
  - subject: "Re: Bunch of errors while using konstruct"
    date: 2003-12-10
    body: "Yeah, the fixed do their work. kdirstat took two runs but works now. I already reported on bugs.kde.org but yet no reply."
    author: "Andy"
  - subject: "Re: Bunch of errors while using konstruct"
    date: 2003-12-17
    body: "Well, i did a complete re-konstruct yesterday and the mentioned problems (newgp, errors in german i18n) are still there. Until now noone answered my bugs.kde.org submits, although I received mails from people suffering from the same problems.\nSadly, I don't have any c++ programming skills, so I can't fix these myself.\nI noticed sound doesn't work either, but I guess this is a simple file access rights thing."
    author: "Andy"
  - subject: "KDE 3.2 (Beta 2) - a nice \"version\" BUT"
    date: 2003-12-09
    body: "It's not possible to start konqueror .. it crashes before \nI can see the window :-( "
    author: "zeth"
  - subject: "Re: KDE 3.2 (Beta 2) - a nice \"version\" BUT"
    date: 2003-12-09
    body: "Using RPMs?\nUsing \"--nodeps\" and/or \"--force\" arguments to install those? -> Don't!"
    author: "Andy Goossens"
  - subject: "Re: KDE 3.2 (Beta 2) - a nice \"version\" BUT"
    date: 2003-12-10
    body: "ok, I will try"
    author: "zeth"
  - subject: "Re: KDE 3.2 (Beta 2) - a nice \"version\" BUT"
    date: 2003-12-11
    body: "Hi,\n    Are you by chance using a SuSE disto?\n\nIf so, see http://bugs.kde.org/show_bug.cgi?id=68547"
    author: "Sean Clarke"
  - subject: "KMail Notification"
    date: 2003-12-09
    body: "Does KMail, in KDE 3.2b2, have support for task bar and/or system tray notification of new messages?\n\n--Dave"
    author: "Dave M"
  - subject: "Re: KMail Notification"
    date: 2003-12-09
    body: "yes"
    author: "anon"
  - subject: "Where did kmidi go?"
    date: 2003-12-09
    body: "Is kmidi a standard component? I know it was included in the Fedora Core 1 KDE packages, but it seems to have disappeared in the upgrade to KDE 3.2 beta 2. As far as I can tell I've installed all the beta 2 .rpms.\n\nPlease don't tell me that kmidi's been removed from KDE!\n"
    author: "Jon"
  - subject: "Re: Where did kmidi go?"
    date: 2003-12-09
    body: "see http://lists.kde.org/?l=kde-core-devel&m=106105958324438&w=2"
    author: "anon"
  - subject: "Re: Where did kmidi go?"
    date: 2003-12-10
    body: "That's very annoying -- the whole reason I want to use KMidi rather than KMid is that I don't have a sequencer, and KMidi was (through timidity) a software synth. Now I have no way to play MIDI files.\n"
    author: "Jon"
  - subject: "Re: Where did kmidi go?"
    date: 2003-12-15
    body: "As a workaround, XMMS has a midi plugin that plays through some old version of timidity (comes as part of the plugin)."
    author: "jaquadro"
  - subject: "Disappointing as ever."
    date: 2003-12-09
    body: "- Konqueror unusable with 100s of bugs in khtml\n- Help system still unusable\n- The joke KMenuEdit still exists and will exist next years, no challenge even for win 98 in user comfort etc.\n- KSysguard with all it's dozens of useless features still exists. \n- Performance poor as ever - but it's funny that konqueror now starts faster than  a simple Hello-World-KDE-Application. One of the smaller problems of KDE, but  annoying. Windows stays the only desktop which not forces the user to wait for simple tasks (Compare Taskmgr.exe and ksysguard, it's like using a 3 Ghz Pentium 4 and a 100 Mhz Pentium I)\n\nAfter years of hope, it's time to admit that the KDE-Project seems to fail finally. It's no real challenge for Microsoft. Shame, cause Windows has also it's problems, but nothing in contrast to KDE.\nI would suggest fixing at least one single application to escape this unsatisfying state: Konqueror. After 6(?) years of KDE konqueror isn't nearly finished. That must be solved. \nNonsense applications like kmenuedit or ksysguard should be banned from the project. \nKDE should get win 98-like customizable menus. \nThat's way more important than to add permanently new useless features or to concentrate on incomplete themes or changing the icon sets."
    author: "kdeuser"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: "TROLL WARNING! dont feed the troll above"
    author: "TrollKiller"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: "Actually, about the menu editing he has a point..."
    author: "Lars"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: "Yep. I guess if you fling shit at random, you're bound to hit something eventually."
    author: "Rayiner"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: "lol"
    author: "JC"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: "> Konqueror unusable with 100s of bugs in khtml\n\nMore like a thousand known bugs in khtml. And eight thousand known bugs in Gecko. I have no idea how many in IE, Microsoft doesn't maintain a public bug database :-) \n\nRendering engines are hard to make. It takes years. \n\nSee David Hyatt's most excellent post about that at http://weblogs.mozillazine.org/hyatt/archives/2003_11.html#004361 ..\n\n\n> The joke KMenuEdit still exists and will exist next years,\n\nThat would likely require changes within Qt. That's why it hasn't happened yet. "
    author: "anon"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: "Maybe, but khtml ist the buggiest rendering enginge by far."
    author: "kdeuser"
  - subject: "..."
    date: 2003-12-10
    body: "why isn't konqueror called KONQUEROR-ALPHA-0.7.0.91.2 or something like that?\nWould be more realistic.\n\n"
    author: "kdeuser"
  - subject: "Re: ..."
    date: 2003-12-10
    body: "Why don't you call yourself TROLL?\n\n"
    author: "Tim Gollnik"
  - subject: "Re: ..."
    date: 2003-12-11
    body: "[quote]\nwhy isn't konqueror called KONQUEROR-ALPHA-0.7.0.91.2 or something like that?\nWould be more realistic.\n[/quote]\n\nWell probably for the same reason that winblows and explorer are not called explorer-ALPHA-0.0.1a\nand\nwindows-ALPHA-000.00.0000.-1a\n\nTrolling does not do anyone any good.\n\nRegards"
    author: "michaeln"
  - subject: "Re: ..."
    date: 2003-12-11
    body: "Explorer is ready to use for web browsing, firebird is, mozilla is, etc..., konqueror isn't. \n"
    author: "kdeuser"
  - subject: "Re: ..."
    date: 2003-12-13
    body: "That's a vague assertion, but can you back it up? The main reason IMHO why people don't like Konqueror is that it doesn't \"feel\" like a web browser.Netscape, IE, Mozilla all basically have the same menus; Konqueror is more like a universal protocol client, and should stay that way.\n\nYes, Internet Explorer is ready for Web browsing, but that's about it. Every version is equipped with a FTP browser that - well, doesn't work and usually freezes your computer with its annoying little swinging flashlight.  It's like they put the GUI and FTP code in the same thread. Konqueror never freezes on FTP."
    author: "Luke Sandell"
  - subject: "Re: ..."
    date: 2003-12-15
    body: "Explorer is ready for web browsing??? Since when??? Check the facts.\n\nExplorer has been around for 8 years and still doesn't have proper PNG support. It can't do alpha channels properly AT ALL.\n\nExplorer has been around for 8 years and still doesn't have proper CSS support. It doesn't even fully implement CSS2!!! www.w3c.org/Style/ doesn't even render half properly.\n\nExplorer has been around for 8 years and still doesn't have proper DOM support. They don't even do DOM Level 1 let alone DOM 2.\n\nExplorer has been around for 8 years and is still plagued with security issues left and right.\n\nKHTML and Gecko both have full proper PNG support.\n\nKHTML and Gecko both have near full proper CSS2 support, with the exception of some CSS bugs in KHTML.\n\nKHTML and Gecko both have full proper DOM Level 1 support, with much of DOM Level 2.\n\nKHTML has no non-trivial security issues in the bugs database.\n\nLooks like Explorer is the browser that's \"not ready\". And all these problems have existed for at least 2 years since version 6 was released, and they all remain unresolved.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: ..."
    date: 2003-12-27
    body: "Unfortunatly, what KHTML can do, is not particularly useful at the moment. And in my opinion priorities are wrong if effort is going into these features instead of getting KHTML to work with most websites. I have lost count of the number of times i've had to switch to netscape to look at a website, the nvidia website displays wrongly when you download drivers, i check the websites for where i'm going skiing and both www.skifrance.fr and www.avoriaz.com the menus fail to work. Even the download page for flash player won't display (http://www.macromedia.com/shockwave/download/alternates/).\n\nIts a shame, but theres no point in trying to pretend its usable, because for what ever reasons (granted maybe because internet explorer is more popular people design their pages to work with it), I can't use it to browse the web.\n\nMartin"
    author: "Martin"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: ":-)\n\nHahaha\n\nlol\n\nI'm soo disappointed..\n\n:-)\n\n"
    author: "Tim Gollnik"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-10
    body: "Every time I install kde, I customize the menu with kmenuedit. If you don't like this application, just don't use it!\nTwo years ago, on my PIII 866Mhz, konqueror was slow. It took 1.3 sec. to start. Now, with preloading disabled, it takes 0.3 sec. Now I am very happy.\n\nBye bye.\n\nP.S. If you hate kde, if you like windows, just use Windows!!"
    author: "Nekkar"
  - subject: "ksysguard"
    date: 2003-12-11
    body: "Interesting that you compare taskmgr to ksysguard. It has been literally years since I have done Windows, so .... . Ksysguard is a loosly coupled app that can look over other systems. In addition, it is easy to extend it. \n\nIs taskmgr something that is distributed and easy to extend?\nYou have indicated that it has speed, but MS is notorious in their taking of shortcuts for speed and monopoly."
    author: "a.c."
  - subject: "Re: ksysguard"
    date: 2003-12-11
    body: "Taskmgr does its job like ksysguard. Taskmgr is extendable, not overloaded, but has no network capabilities. That's not really necessary. \nTaskmgr is only a click away, ksysguard is a click away and the usual 2 seconds, like kfind, etc... For slow users no problem at all."
    author: "kdeuser"
  - subject: "Re: ksysguard"
    date: 2003-12-13
    body: "Point 1: KSysGuard is zero clicks away. Try CTRL-ESC.\n\nPoint 2: Task Manager takes a second or two to come up on my system."
    author: "Luke Sandell"
  - subject: "Re: Windows better than KDE"
    date: 2003-12-11
    body: "Windows better than KDE? I find it interesting that a windoze user actually finds his way into the KDE3.2 beta message board.\n\nAs to Konqueror ... I agree, I find its way of handling the web inferior to the Mozilla project. On the other hand, I wouldn't want a different file manager. The right click menu in Konqueror beat everything MS has to offer in their version. The fast access to Ark and the \"Copy To\" and \"Move To\" shortcuts are simple features that have come in handy almost every day. If you feel Windows offers a better way of handling things, then stick with Windoze but don't tell other people that their stuff is \"useless\" if they, in fact, can make great use out of it.\n\nAs to KSysguard ... I really don't see your point. Back in my windoze days whenever i needed taskmgr to shut an app down that crashed it itself crashed. I'd rather stick with ksysguard which i don't even have to look at since my apps don't crash anymore.\n\nAs to KMenuEdit ... I agree, it's a silly solution, although not a silly program. It's a powerful program that gets the job done - what's useless about that? I do miss the right click available on windoze in the KMenu, but to be honest, I don't use that particular feature often enough to make me run windoze again. It's merely a small convenience.\n\nThe greatest thing about KDE is though that we can all post to a message board like this and speak our mind and tell the developers what we would like to see changed or added to KDE. Can you do that with windoze?"
    author: "Anonymous"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-12
    body: "I'm sure that you have already decided to help KDE-project to fix any annoyances you have. What was that? You are just sitting there on your fat ass, doing nothing but whine when group of people spend their time to give you kick-ass software for free? That's what I thought...\n\nSeriously, I find your comment to be moronic and pointless. If you don't like KDE, why not start your own desktop-project? Hell, you can even fork KDE if you want to. No-one is forcing you to use KDE; there are other alternatives out there. But I bet that you are not going to do a damn thing, you are just going to sit there in your moms basement and whine."
    author: "Janne"
  - subject: "Re: Disappointing as ever."
    date: 2004-01-08
    body: "You spoke from my heart!"
    author: "Zsigabiga"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-16
    body: "Just my two cents. I hae been trying to customize my taskbar wih windows XP. I can say in one word... sucks. I have not yet seen a help that actually answers my question. Troubleshooting is great too;  have a problem with this; oh well dd u download latest driver; yeah; doesnot solve your problem; call tech support for the hw maker; yes no clues; sorry can't help you. It is a joke.I rather do the same thing in kde than xp sh.. Next time u go around slinging words,remember Microsoft is no longer supporting 98 so bzz off..."
    author: "Anon"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-16
    body: "At least when you ask ksysguard to kill an application it actually does it..."
    author: "Chris Hope"
  - subject: "Re: Disappointing as ever."
    date: 2003-12-29
    body: "Hey, kdeuser!\n\nDid you ever try to validate MS's web page to a w3c validator?\nHow about Nvidia's?\n\nOk. \nSo now we can agree, that pages that do not adhere to common standards might not show the same way in all browsers.\n\nNow.\nI am a web designer and curently I use Opera and Konqueror to validate, weather I screwed up in CSS. A typical page with all it's markup takes only a few K, validates beautifully for xhtml 1.1 and looks exactly the same in Konqueror, Mozilla and Opera.\n\nIE you cry?\nWell, I am spending 90% of time trying to use alphaImageLoader SHIT (failing randomly) and screwing about with nested shit not respecting parrents etc....\nIE is not even alpha quality. It's a random hack (and not in Linux sense, where you can use random hacks on production machinery for years without a reboot).\n\nOne thing, where IE excells at:\nIt makes an excellent use of Microsoft's EULA, whereby you get the right not to sue the owner of the software (Microsoft), which YOU payed for, because there is no guarantee that the particular software you payed for is actually fit to do anything. The EULA gives a meaning to IE. Without EULA, IE has no purpose. Couple IE to EULA and it's like: \"Come here, Idiot customer, pay me for something which I am not going to give you and I guarantee it is not necessary for it to do anything in particular. If this software does something, you can not sue me.\"\n\nAnd yes. There are no significant bugs in MS software. Should your software stop preforming for ay reason you probbably do not know how to use it (Bill Gates, 1993)"
    author: "mrT"
  - subject: "Knoppix or other Live CD so we can try?"
    date: 2003-12-09
    body: "Does anyone know of a Knoppix development release or other LiveCD distro that has this on it so us peons can pop it in and give it a whirl without the risk of busting anything too bad on our work installations?\n\nThanks!"
    author: "rsk"
  - subject: "The answer is SLAX!"
    date: 2004-01-02
    body: "As I commented in another post.  The SLAX LiveCD contains beta2 releases of KDE 3.2 and KOffice 1.3.  I'm using it right now to post this.\n\nhttp://www.slax.org\n"
    author: "evangineer"
  - subject: "Konqueror Problem"
    date: 2003-12-10
    body: "I had beta 1 installed, but now that i got beta2 the html part of konqueror doesnt work. When i try to go to a website (or local html file) it crashes with a Signal 6 error. Whats the deal here?\n\nOh, heres the terminal output...\n\nkparts: loadPlugins found desktopfile for khtml:\nkparts: load plugin autorefresh\nkparts: loadPlugins found desktopfile for khtml:\nkparts: load plugin Crashes\nkparts: loadPlugins found desktopfile for khtml:\nkparts: load plugin khtml_kget\nkparts: loadPlugins found desktopfile for khtml:\nkparts: loadPlugins found desktopfile for khtml:\nkparts: load plugin khtmlsettingsplugin\nkparts: loadPlugins found desktopfile for khtml:\nkparts: load plugin krpmview\nkdecore (KLibLoader): library libkrpmview.la not found under 'module' but under 'lib'\nkonqueror: factory.cpp:79: virtual QObject* KParts::Factory::createObject(QObject*, const char*, const char*, const QStringList&): Assertion `!parent || parent->isWidgetType()' failed.\njames@linux:~>"
    author: "James"
  - subject: "Re: Konqueror Problem"
    date: 2003-12-10
    body: "See bug details at http://bugs.kde.org/show_bug.cgi?id=61042"
    author: "GeorgeM"
  - subject: "Compile Problems?"
    date: 2003-12-10
    body: "I am having difficulty getting this release to compile on a RedHat 7.1 system...  In particular, I am having trouble with gcc, which gives me the following error halfway through compiling arts:\n\n/afs/.cs.cmu.edu/misc/egcs/@sys/archive/libexec/gcc-3.3/lib/./libstdc++.so: undefined reference to `_Unwind_Resume_or_Rethrow@GCC_3.3'\n\nI gather this is a gcc bug which I am hitting.  Does anyone know precisely which versions of gcc are able to compile this release successfully?"
    author: "Chris Colohan"
  - subject: "Re: Compile Problems?"
    date: 2003-12-10
    body: "I compiled kde-3.2b1 and kde-3.2b2 successfully on a RedHat 7.3 system (gcc-2.96-113). The only problems were with -O2 optimization for kdelibs and --enable-final for arts and some other packages (info from http://bugs.kde.org)."
    author: "Hubert Hoffmann"
  - subject: "Re: Compile Problems?"
    date: 2003-12-11
    body: "I have had a similar problem with my slackware-9.1.  Basically the system comes with gcc-3.2.3, but I added a second compiler gcc-3.3.2.  If you have a similar setup, try to use the compiler as provided by your original distribution, or else make sure your gcc installation is sane (try reinstall)."
    author: "Pedro Sam"
  - subject: "Kopete Broken"
    date: 2003-12-10
    body: "Well,  I'm happy to say that Konqueror now starts as fast or faster than IE on my 300 MHz. Unfortunately, it's the only application that starts any faster. Prelinking my binaries caused no noticable speedup. (Remember, the GNU runtime linker once served as the scapegoat of the Qt/KDE slow startup problem, now what is it?) \n\nUnfortunately, Kopete is broken. I try to start it and am asked to enter my KWallet password. I don't really get why I would need to enter more passwords on when I am already logged in as the user in question (or for that matter, have a separate password for KWallet).  At any rate, Kopete stashes drops a dead icon in the tray icon and that's it."
    author: "Luke Sandell"
  - subject: "Kwallet"
    date: 2003-12-10
    body: "yes. Kwallet is annoying.\nanyway to turn it off ?"
    author: "anonymous"
  - subject: "Re: Kwallet"
    date: 2003-12-10
    body: "kwallet seems quite useful, but I wish it would just work for the whole session after you input the form password once like in every other browser, i.e, mozilla, opera, IE, etc... especially's Mozilla's PSM (personal security manager)\n\nkwallet is pretty nice, but it needs to be more friendly to the needs of most users, who don't give two more that two shits about protecting form data more than is needed."
    author: "anon"
  - subject: "Re: Kwallet"
    date: 2003-12-12
    body: "Did you ever think to configure it?  You can make it stay open for the entire session once it's open, you can disable it, and much more.  The first use of KWallet gives you a wizard to configure it, and it provides suitable defaults if you choose to enable it.  If that's not enough, there's a KCM.  Try it."
    author: "George Staikos"
  - subject: "Re: Kwallet"
    date: 2005-12-02
    body: "The first time Kwallet pops up should be with an install wizard which carefully explains what to do and how to use it. At present it pops up and asks for a password ... a password for what? the next time it pops up and asks for a password again ... again, for what.\n\nI install Linux (mainly SuSE)for people who do not understand English very well, if at all, and Kwallet is a major nuisance as it usually ends up in people not being able to get their mail resulting in bad opinions of Linux because of one badly behaved app.\n\nThe way Kwallet pops up reminds one of Gates' idiot paper clip and is in danger of becoming just as disliked in this neck of the woods. Turning it off after the fact is too late as mail and other apps all require you to enter paroles every time they are needed again resulting in bad rap for Linux in general.\n\nLet's face, the way it works at present is a pain in the okole and it needs to be cleaned up. It may be readily apparent to the genius who wrote it but it not so readily apparent to the rest of us.\n\nThank you for your attention,\n\nJanis"
    author: "Janis Klava"
  - subject: "Re: Kwallet"
    date: 2004-03-13
    body: "I *do* like KWallet, but for the life of me I can't figure out how to configure it, under 3.2.  Help!"
    author: "CodeMonkey"
  - subject: "Re: Kwallet"
    date: 2004-03-13
    body: "i was very confused too, until i figured to apt-get install kwalletmanager.\n\nit's a shame that kwallet is in kdebase, but kwalletmanager isn't. "
    author: "anon"
  - subject: "Re: Kwallet"
    date: 2004-03-13
    body: "Doh!  Thanks ... :)"
    author: "CodeMonkey"
  - subject: "Re: Kopete Broken"
    date: 2003-12-10
    body: "aha! just had to delete my old kopeterc"
    author: "Luke Sandell"
  - subject: "Re: Kopete Broken"
    date: 2003-12-10
    body: ">  Prelinking my binaries caused no noticable speedup.\n\nPerhaps because prelinking wrt KDE and Qt apps is broken for the vast majority of installations (and has been for a while) until qt 3.3b1 (or equivalent snapshots from TrollTech). This is because of Qt's dependencies on libGL, which is usually compiled non-PIC. That breaks prelink. libGL is optional in qt 3.3b1.\n\nSo yes, you can still blame the linker :-D"
    author: "anon"
  - subject: "Re: Kopete Broken"
    date: 2003-12-10
    body: "No, I applied Leon Bottou's <a href=\"http://lists.kde.org/?l=kde-optimize&m=107007239305615&w=2\">patch</a> and ldd shows that my Qt is no longer links to libGL. So no, I can't still blame the linker. At any rate, linking only takes a fraction of a second even for KDE apps.\n"
    author: "Luke Sandell"
  - subject: "Re: Kopete Broken"
    date: 2003-12-22
    body: "Weren't there some dynamic linker enhancements coming in 2.6?  I thought that the old crunky dylinker was still present in 2.4..."
    author: "Charles"
  - subject: "Package Installation Order?"
    date: 2003-12-10
    body: "I'm not a big fan of \"package kde-blahblahblah is needed by kde-moreblahblahblah\" when issuing manual rpm commands.  What order should these packages be installed in to minimize this annoyance.  Yes, I know this is trivial but I am a lazy, lazy man.  Better yet, are there any Fedora apt/yum/up2date/etc repositories with beta2?  Mmmmm, automatic dependency resolution...[*]\n\n  \nLester Bangs.\n\n*Typing that last sentence officially made my transition to complete and utter nerd complete."
    author: "LesBangs"
  - subject: "Re: Package Installation Order?"
    date: 2003-12-10
    body: "rpm -U kde-*.rpm"
    author: "Anonymous"
  - subject: "Re: Package Installation Order?"
    date: 2003-12-18
    body: "rpm -U kde-*.rpm\n\nI would have loved for the above command to have worked in Fedora Core 1, but I still get dependancy errors.  Yes, I'm a bit new to this, and would love to check out this release of KDE."
    author: "Robert Bobert"
  - subject: "Re: Package Installation Order?"
    date: 2004-01-10
    body: "place all the RPMs in a directory and run\nrpm -Uvh --force --replacepkgs *rpm"
    author: "Nicola Di Nisio"
  - subject: "Re: Package Installation Order?"
    date: 2004-04-17
    body: "rpm -Uvh --nodeps --force --replacepkgs *rpm is better."
    author: "Jim Cahill"
  - subject: "Re: Package Installation Order?"
    date: 2004-04-17
    body: "But please don't come complaining if using --nodeps --force breaks your install.\n"
    author: "Zelva"
  - subject: "Is Juk included in KDE 3.2 beta 2?"
    date: 2003-12-10
    body: "Does Juk 2.0 (or thereabouts) come with the 3.2 beta 2, or does it have to be obtained separately?\n\nThanks."
    author: "Andrew"
  - subject: "Re: Is Juk included in KDE 3.2 beta 2?"
    date: 2003-12-10
    body: "It is included in the kdemultimedia package."
    author: "anon"
  - subject: "SUSE RPM's require libtag.so"
    date: 2003-12-10
    body: "Does anyone know what libtag is and where I can get it?  I don't mind compiling it and creating my own RPM for it, but I can't even find it.  I did find libtaged.sourceforge.net, but when compiled it doesn't produce any libtag.so files, just libtaged.* files.  \n\nI searched, using Yast, all of the SUSE rpms from the install and it doesn't contain any reference to libtags either.\n\nI would feel a whole lot better about installing this if I can get this dependancy resolved (especially since there is now a fix for Konqueror as well).\n\nThanks for any help you can offer.\n\nDan"
    author: "Dan Freed"
  - subject: "Re: SUSE RPM's require libtag.so"
    date: 2003-12-10
    body: "It's in a package called taglib - it's available in KDE extragear. I checked on rpmfind and there's a SUSE rpm for it, although I have no idea if it's the right version (I'm not a SUSE user)"
    author: "Stephen Douglas"
  - subject: "Re: SUSE RPM's require libtag.so"
    date: 2003-12-10
    body: "Great thanks a bunch.\n\nIn case anyone else is looking, I found a SUSE 9 package at:\n\nftp://ftp.univie.ac.at/systems/linux/suse/i386/supplementary/KDE/update_for_9.0/base/taglib-0.95-5.i586.rpm\n\n"
    author: "Dan Freed"
  - subject: "Re: SUSE RPM's require libtag.so"
    date: 2004-01-19
    body: "This is a newer file, the on on the previous post did not work. It just pruked error messages and hanged.\n\nrpm -i taglib-0.95-5.i586.rpm\nerror: open of <!DOCTYPE failed: No such file or directory\nerror: open of HTML failed: No such file or directory\nerror: open of PUBLIC failed: No such file or directory\n\nUse this one instead.\n\nftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_9.0/base/taglib-0.96-0.i586.rpm"
    author: "infyrno"
  - subject: "Where can I configure audiocd:/ ?"
    date: 2003-12-10
    body: "Where can I configure audiocd:/ ? I don't find the panel to configure it !"
    author: "capit. igloo"
  - subject: "Re: Where can I configure audiocd:/ ?"
    date: 2003-12-11
    body: "Try \"kcmshell audiocd\""
    author: "Peter"
  - subject: "Re: Where can I configure audiocd:/ ?"
    date: 2003-12-11
    body: "Thanks."
    author: "capit. igloo"
  - subject: "da best"
    date: 2003-12-10
    body: "kde3.2b2 is da best. Its khotkeys is best I ever seen :o) Some bugs but it is ok. Also performance is highly better than before.\n\nI compiled it for myself. Lots of features I dreamt to code to kde3.2 are there.\n\nPeople in enterprises should also think about it. Windows is not so good.\n\nThanks to all!!!"
    author: "stromek"
  - subject: "Konstruct cannot be downloaded?"
    date: 2003-12-10
    body: "When I try to download konstruct from \nhttp://developer.kde.org/build/konstruct/unstable/konstruct-unstable.tar.bz2\n\nI get:\n\nForbidden\nYou don't have permission to access /build/konstruct/unstable/konstruct-unstable.tar.bz2 on this server.\n\nWhat's wrong?\nEggert"
    author: "Eggert"
  - subject: "Re: Konstruct cannot be downloaded?"
    date: 2003-12-11
    body: "It works for me this moment."
    author: "Anonymous"
  - subject: "Re: Konstruct cannot be downloaded?"
    date: 2003-12-12
    body: "Now it works here too."
    author: "Eggert"
  - subject: "KDE 3.2 Broke my ALSA sound"
    date: 2003-12-11
    body: "I am running Gentoo with a vanilla 3.4.23 kernel. My system, is a Gigabyte GA-7vt600 motherboard, an AMD XP2100+ prfocessor and a via sound chip. I compiled the KDE 3.2 beta 2 with konstruct. After I ran it, my sound quit working. \n\nI had the same problem with beta 1, and had to rebuild my alsa drivers before the system would run again.  I should have known better."
    author: "John cox"
  - subject: "Re: KDE 3.2 Broke my ALSA sound - same here?"
    date: 2003-12-11
    body: "Similar experience: Mandrake 9.2, Epox 8KRA2+ board (KT600/VT8237, too). Installed KDE 3.1.93 from Cooker, eventually ALSA died. Since 2.4, I switched to Gnome, so apparently the bug was dormant and only triggered when I launched a KDE session for another user a bit later. I cannot precisely reconstruct the events, but it looks VERY similar to what the above poster described.\n\nRich\n"
    author: "Rich"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2003-12-11
    body: "Running SUSE 9.0 and installed KDE3.2b2 from available rpms .... kdemultimedia packages wine about libartsmodules, libgui, libnoatun, and a few other dependencies that apparently aren't met (although the required files were included in kdemultimedia in 3.1)\n\nno sound working for me either at this point. i don't know if the problems are the same as everyone else's in this posting."
    author: "Anonymous"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2003-12-13
    body: "I have a similar problem. (Suse 8.2, kde 3.2 beta 2)\nI can hear sound, but its extremly quiet. KMix and Alsamix work.\n\nbtw. xmms crashs, if i want to play anything."
    author: "atomas"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2003-12-21
    body: "xmms doesn't work with arts-1.1.9x and later\n\nthere is a problem with the running glib-version (new arts uses glib-2.0, xmms does it with glib-1.2)\n\nto only way to use xmms with kde-3.2 is to compile it yourself and use arts-version 1.1.4 or minor (kdelibs and the following configure-scripts look for arts-1.1.x so there is no dependence-problem, other if you use precompiled pkg's).\n\n\n"
    author: "tizz"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-01-01
    body: "To solve the problem You should run as root an alsamixer command in the terminal window, go to the \"External Amplifier Power Down\" item and press M button. That's all."
    author: "Alexander Malashenko"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-06-04
    body: "I had similar problems.  I deleted my SB Audigy card from Yast.  Went to a konsole session as root.  Typed 'Yast sound add'.  That put the card back, and it test okay.  Still no CD player sound.  Went back to konsole and typed 'alsamixer'.  I had to find the 'Audigy CD' setting.  Maxed that to 100%.  Escaped to save and exit.  Works fine now."
    author: "Mike"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2003-12-13
    body: "I hate posting a me too; but ME TOO!\n\nI have too systems: a Pentium 4 (sorry don't have the specs to hand) and an Athlon with a CMI8738 6ch-MX on-motherboard sound system. The P4 works absolutely fine, but the KDE3.2B2 broke my Athlon :(\n\nI installed KDE3.2b2 at work over the first Beta over KDE 3.1.4 - all from the Suse packages. At home 3.2b2 went in over 3.1.4.\n\nAny suggestions for how to fix it?"
    author: "Andrew"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2003-12-13
    body: "BTW, when I run KMix from a console, I get this output. Looks like something bad is going on with the ALSA mixer. Can anyone tell me what this might mean?\n\nkmix: Trying Alsa 0.9x Device hw:0\nkmix: Trying Alsa 0.9x Device hw:1\nkmix: ERROR: Alsa mixer cannot be found.\nPlease check that the soundcard is installed and the\nsoundcard driver is loaded.\nkmix:\nkmix: Trying Alsa 0.9x Device hw:1\nkmix: ERROR: Alsa mixer cannot be found.\nPlease check that the soundcard is installed and the\nsoundcard driver is loaded.\nkmix:\nkmix: Sound drivers supported: ALSA0.9 + OSS\nSound drivers used: ALSA0.9\nkmix: Mixer number: 0 Name: C-Media PCI CMI8738\nkmix: Inserted mixer 0:C-Media PCI CMI8738\n"
    author: "Andrew"
  - subject: "Re: KDE 3.2 Broke my ALSA sound : fixed for me"
    date: 2003-12-20
    body: "Same problem here.\n\nI'm running kde 3.2 beta 2 on mkd 9.2.\n\nIn kcontrol -> sound system -> hardware ; I've set \"Open Sound System\" instead of \"Autodetect\". Reboot. That's it. Sound is back.\n\nHope this helps / quark"
    author: "quark"
  - subject: "Re: KDE 3.2 Broke my ALSA sound : fixed for me"
    date: 2003-12-21
    body: "Unfortunately I had already done this and it didn't work. Since then, I've completely reinstalled ALSA; removed then reinstalled the soundcard in YaST; sound came back but the sound quality was *terrible*.\n\nThen I put an old soundcard into the computer, told Suse to use that one, and everything is back and beautiful.\n\nThis seems to imply an issue with either the ASUS chipset or the soundcard (CMI8738) somehow breaks ALSA/KDE3.2"
    author: "Andrew"
  - subject: "Re: KDE 3.2 Broke my ALSA sound : fixed for me"
    date: 2004-01-02
    body: "I also got terrible sound quality using ALSA/KDE3.2 and CMI8738... It sounds like everything is way too amplified, it used work beautifully using either OSS och via gstreamer/alsa."
    author: "Mattias"
  - subject: "Re: KDE 3.2 Broke my ALSA sound : fixed for me"
    date: 2004-03-24
    body: "I don't have any other choices, just Autodetect."
    author: "Kent"
  - subject: "Re: KDE 3.2 Broke my ALSA sound : fixed for me"
    date: 2004-03-24
    body: "I don't have any other choices, just Autodetect.\n\nAnd one other thing, I can play music CDs and MP3 files with no problem."
    author: "Kent"
  - subject: "Same problem"
    date: 2004-01-19
    body: " I experience the same problem.\nAnd I wonder why there is no one to test the program before the release \neven for such a basic error.\n"
    author: "Pingkai"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-07-21
    body: "ME TOO!\n\nMy old K6 with Sound Blaster works well but the new box with CMI8738 on motherboard sounds like crap using ALSA.\n\nDoes anybody know what kernel modules need to be installed to make the CMI8738 work without ALSA?"
    author: "El Chupacabra"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2003-12-26
    body: "I had the problem with ALSA sound also (XMMS, noatun etc.). To solve the problem I removed ALSA from my Fedora Core and recompiled the all KDE packages. Then I compiled and installed ALSA. Everything works fine now. As far as I understand in this case the sound works through alsa-oss library.\nBy the way the problem didn't appear on my RedHat9 system."
    author: "Alexander Malashenko"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-01-24
    body: "Same happened to me.\n\nMy system has also Gigabyte motherboard and AMD2600, via sound chip (via83xx module), system gentoo 1.4 with gentoo kernel 2.4.20.\n\nI shut down xdm, alsasound and upgraded to alsa-(drivers, utils, oss)-0.9.8.\n\nAfter that I unmuted the usual PCM, CDROM, Master etc, and also the VIA DXS # channels.\n\nNow sound is back to kde 3.2-rc1"
    author: "Xiqui"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-02-06
    body: "Ok, im glad to hear that my system wasnt the only one doing this. Fortunatly for you guys I **HAVE A FIX**, well more of a workaround.  \n\nFirst here is the problem.  If you go into alsamixer and then scroll *all* the way to the right, you should see 4 meters that say \"VIA DXS\".  These meters need to be all the way up to 100% in order for sound to work, dont ask me why they just do. So set them all to 100% then run 'alsactl store' and then 'alsactl restore' for good measure.  But thats not where the problem stops. Unfortunatly when you start KDE 3.2, for some reason or another those 4 meters will be set to all 0%.  This is why your sound doenst work.  Why KDE screws with those meters I dont know but it does.  \n\nNow the fix.  The /etc/asound.state file you made with 'alsactl store' should contain the necessary info to put those four \"VIA DXS\" meters all the way to 100%.  So what you need to do is create a little script that runs 'alsactl restore'.  For those of you who dont know how to do this, just copy the provided script below as \"alsarestore.sh\", make sure to \"chmod +x alsarestore.sh\" and toss that into ~/.kde/Autostart.  Now when you start up in KDE that script will run and your sound should now work.  \n\nFor those KDE developers out there, I would appeciate it if you could fix this the right way, in which I mean make it so that KDE doesnt put those four meters down to 0% in the first place.  Regards\n\n-Sean\n\n-----Start of Script-------\n#!/bin/bash\nalsactl restore\n-----end of script---------\n"
    author: "SiegeX"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-02-06
    body: "Ok, im glad to hear that my system wasnt the only one doing this. Fortunatly for you guys I **HAVE A FIX**, well more of a workaround.  \n\nFirst here is the problem.  If you go into alsamixer and then scroll *all* the way to the right, you should see 4 meters that say \"VIA DXS\".  These meters need to be all the way up to 100% in order for sound to work, dont ask me why they just do. So set them all to 100% then run 'alsactl store' and then 'alsactl restore' for good measure.  But thats not where the problem stops. Unfortunatly when you start KDE 3.2, for some reason or another those 4 meters will be set to all 0%.  This is why your sound doenst work.  Why KDE screws with those meters I dont know but it does.  \n\nNow the fix.  The /etc/asound.state file you made with 'alsactl store' should contain the necessary info to put those four \"VIA DXS\" meters all the way to 100%.  So what you need to do is create a little script that runs 'alsactl restore'.  For those of you who dont know how to do this, just copy the provided script below as \"alsarestore.sh\", make sure to \"chmod +x alsarestore.sh\" and toss that into ~/.kde/Autostart.  Now when you start up in KDE that script will run and your sound should now work.  \n\nFor those KDE developers out there, I would appeciate it if you could fix this the right way, in which I mean make it so that KDE doesnt put those four meters down to 0% in the first place.  Regards\n\n-Sean\n\n-----Start of Script-------\n#!/bin/bash\nalsactl restore\n-----end of script---------\n"
    author: "SiegeX"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-02-09
    body: "Hey, \n\nDoes anyone know if there was any work done on this problem between the beta release and the final release Feb 3?\nI would like to know if it has been fixed or not.\n\nThanks"
    author: "Rich"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-02-10
    body: "Hi,\nThis issue seems to be VIA-specific.\nThe autostart workaround works, but you have to explicitly define the path to alsactl like (if you're using a distribution that sends alsactl to sbin like Slackware)\n/sbin/alsactl\n(if you don't have /sbin in your regular user's path, which is the probable case)\n\nI hope there will be a solution other than a workaround soon.\n\nThe final release also has this problem."
    author: "Dogac Senol"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-02-11
    body: "I have an SBlive and it had the same issue of no sound after upping to 3.2.\nI used Webmin to restart Alsa.\nI opened Kmix and pushed PCM all the way up and AC97 all the way down.\nNo crappy sound (it was nasty before) and at regular volume.\n"
    author: "bendu"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-02-18
    body: "3.2 broke my audigy, i had to turn off \"Audigy A\" (Audigy Analog/Digital Output Jack) in alsa-mixer to use analog mode, realy dont know why kde messed with that"
    author: "Roger"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-05-25
    body: "by Roger on Wednesday 18/Feb/2004, @03:17\n\n\"3.2 broke my audigy, i had to turn off \"Audigy A\" (Audigy Analog/Digital Output Jack) in alsa-mixer to use analog mode, realy dont know why kde messed with that\"\n\nThis one fixed my Athlon XP/Audigy/VIA chipset based computer with Fedora Core 2"
    author: "Thank YOU!"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-03-08
    body: "Well, I've had the same problem as you all after emerge'ing KDE 3.2. My sound card is an Ensoniq 1371 based Creative SB 128. Motherboard is VIA 133 based, though i guess it doesn't matter much.\n\nThe default behaviour for KDE upon startup is to load mixer settings for the sound system (ALSA in the case being). It happened that these mixer settings are quite dubious : all volumes at zero and no channel muted.\nTurned out the loud circuit noise/hiss I experienced was caused by the 'IEC 958' channel not being muted. YMMV.\n\nFix :\n\n1) Fire up alsamixer and use the arrow keys to adjust level settings. Use M to mute a channel (MM means a channel is muted). In particular, mute the 'IEC 958' channel.\n2) With root privilege, run 'alsactl store' and 'alsactl restore' for good measure.\n3) In KDE, open the 'Control Center', 'Sound & Multimedia' category, 'Mixer' pane.\n4) Uncheck 'Load volumes on login' !\n5) Exit KDE\n6) Start KDE and enjoy !"
    author: "Matthieu Boyer"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-03-30
    body: "I am using the following system:\n- SuSE 8.1 with Kernel 2.4.21-198 (SuSE-deflt) and with alsa 1.0.3b\n- SiS7012 PCI Audio Accelerator (rev a0) together with module snd-intel8x0 as snd-card-0\n\nAfter updating from KDE 3.1.4 to 3.2.1 I had the same problem as many others (I guess): no sound at all.\nIt was crazy because also after downgrading to KDE 3.1.4 it stayed the same. \n\nThe following worked for me:\na) The hint of Matthieu ;-)\nb) Start KDE as normal user and run kmix to uncheck the 'IEC958 Capture Monitore' button in the extended section. \n[No alsamixer or else was neccessary  or unchecking the control 'Load volumes on login' in kcontrol's mixer pane. My volume settings were ok.] \nWhile unchecking I run aplay /usr/share/sounds/alsa/test.wav and sound immediately started.\n\nAlternatively you can set the is_muted flag in the config file ~/.kde/share/config/kmixctrlrc, e.g.\n~~~~~snip~~~~~~\n[MixerSiS SI7012.Dev15]\nis_muted=1\nis_recsrc=0\nname=IEC958 Capture Monitor\nvolumeL=0\nvolumeR=0\n~~~~~snip~~~~~~\n\nI am not aware of the details (i.e. problem of either alsa or kde) but it worked so far.\n\nMathias."
    author: "Mathias Monse"
  - subject: "Jackpot!!!"
    date: 2004-07-28
    body: "Thanks Mathieu!!!\n\nAfter spending two days googling, trying all sorts of things, reinstalling my whole system, having the sound broken again, finally this worked!!! What a relief!\n\nJust for completeness... I am using Gentoo on a MSI Neo2 mobo with integrated sound. Sound used to work until I installed the kdemultimedia package and rebooted the computer. From then on, no sound at all. So the problem (at least for my system) really seems to be with those IEC 958 channels - I simply muted all of them following Mathieu's instructions.\n\nKlaus"
    author: "Klaus"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-08-30
    body: "Thanks very much for the solution Matthieu. I now have 2 machines up & running with perfect sound. I searched high & low for tips on this, I think a few people are having this problem too. The 'alsactl store' and 'alsactl restore' fixed an earsplitting tone that emanated from my old Intel 166MMX with SB Live 5.1/Slackware 10 on booting, and the 'uncheck load volumes on login' fixed the hiss/distortion in KDE on my GA-7DXR+ with onboard Creative CT5880/Slackware 10. Cheers!"
    author: "aquamarine"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-05-08
    body: "Tried to follow most of the fixes here and none worked and I really don't like the prospect of having to restart the system after each to try them properly......... *sigh*.. why is this a problem with KDE 3.2 does anyone know?"
    author: "Xanas"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-08-02
    body: "I had the same problem. Set volumes in yast, sound working fine on the Audigy. Then all volume settings lost on reboot. Simple fix for me was the /etc/init.d/boot.local file, adding the alsa ctl command. In my case \"alsactl restore 0\" did the trick."
    author: "steve"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2004-08-06
    body: "I'm running Linux 2.6.7 with compiled in ALSA (onboard CMI sound). Everything worked fine, until I installed kdemultimedia-3.2.3.\n\nBefore this even kde did fine noise, after this at first nothing, switching the control-center to \"ALSA architecture\" or \"OSS\" lead to real cruel sound-quality and at this point I can't hear anything more.\n\nNothing took effect till yet, there seems to be something really weird in kdemultimedia..."
    author: "Mabuse"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2006-05-06
    body: "Same problem here in the year 2006. I am running Ubuntu Dapper Drakeand tried kde once, to see how it looks. After that my sound was off. The tips above did not work for me, so i am asking the more general question:\nWhat non-windowmanager-related config files does the kde windowmanager write to and how do i undo all those changes, preferably without starting kde again?\nThanks!"
    author: "steve"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2006-08-15
    body: "I had the same problem. It got fixed when I added the KDE user to the audio group. "
    author: "delyan"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2006-12-02
    body: "Thanks, yet another 2006 confirmation that only a user/group change is needed for new systems. \nAm on Ubuntu 6.06.1 LTS, kde 3.5.2 and running mythtv.  \n\nSound stopped working and the mixer window would be completely empty.  Not sure why that happened but one more symptom was a red x and a \"mixer cannot be found\" tooltip on the sound icon on the panel.\n\nReinstall alsa did not help.\nsudo apt-get remove alsa\nsudo apt-get install alsa\n\nAdding the user to the audio group and restart kde solved the issue.\nsudo usermod -Gaudio mythtv\nsudo /etc/init.d/kdm restart"
    author: "Amir"
  - subject: "Re: KDE 3.2 Broke my ALSA sound"
    date: 2006-12-12
    body: "Found the problem, in an earlier attempt to setup the dvd for mythtv, I have likely executed the command:\n\n   sudo usermod -Gcdrom mythtv\n\nhowever usermod without \"-a\" would wipe all existing groups, which in turn broke the sound (and video capturing ...).  \"-a\" means append and should likely be on by default to avoid such mistakes, by users.\n\"-a\" is surely in the man page for usermod -G.  Though it is possible the avarage impatient user would not get to that last sentence."
    author: "Amir"
  - subject: "Change icon distance at konqueror"
    date: 2003-12-11
    body: "Does KDE-3.2 support changing distance between icons at konqueror? One thing I notice between Nautilus and Konqueror is that space between icons at Nautilus is wider (only 5 icons at one row, konqueror has 7 icons at one row at 1024 x 768), so it is easier for me to read or look for spesific content.\n\nI search through konqueror options but still not get it.\nI think this is one of usability issue.\n\nThanks."
    author: "Adi Wibowo"
  - subject: "Re: Change icon distance at konqueror"
    date: 2003-12-11
    body: "> Does KDE-3.2 support changing distance between icons at konqueror? \n\nNot that I know of.. I don't think anybody has requested it either. \n\nhit up bugs.kde.org and file a wishlist bug :)\n\nI would like it too."
    author: "anon"
  - subject: "Re: Change icon distance at konqueror"
    date: 2003-12-11
    body: "If you 'feature request it' and put the bug number in this thread, I will vote add my vote."
    author: "ac"
  - subject: "Konqui: How to use button 6+7 for back and forth?"
    date: 2003-12-11
    body: "imwheel doesn't do it any longer on SuSE 9.0.\n\nBTW Button 6+7 do horizontal window scrolling fine.\n\tWhat about button 8 (MX700)? Would be cool for \"app switching\".\n\nThanks,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Konqui: How to use button 6+7 for back and forth?"
    date: 2003-12-11
    body: "How did ever managed to utilize buttons 6 and 7 for anything usefull? \n(my mouse is mx500 -- same as mx700, only corded)"
    author: "SHiFT"
  - subject: "Re: Konqui: How to use button 6+7 for back and forth?"
    date: 2003-12-11
    body: "You'd better reserve the 8th one for mouse gestures (KControl->KDE components->KHotKeys). And I'd actually like to know if it really works with button > 5 :).\n"
    author: "Lubos Lunak"
  - subject: "Re: Konqui: How to use button 6+7 for back and forth?"
    date: 2003-12-11
    body: "OK, let's make a \"deal\"...;-)\n\nYes, it _works_ with 6 or 7.\nPlayed somewhat with KHotKeys before...\nBut I had to erase the default gestures.\nAfter I did my own it _is_ working with the \"middle button\" (2), 6 or 7.\nI'm testing with this now:\n\nSection \"InputDevice\"\n  Driver       \"mouse\"\n  Identifier   \"Mouse[1]\"\n  Option       \"ButtonNumber\" \"8\"\n  Option       \"Buttons\" \"8\"\n  Option       \"Device\" \"/dev/mouse\"\n  Option       \"InputFashion\" \"Mouse\"\n  Option       \"Name\" \"PS/2-Mouse;ExplorerPS/2\"\n  Option       \"Protocol\" \"ExplorerPS/2\"\n  Option       \"ZAxisMapping\" \"6 7\"\nEndSection\n\nWhat's the difference between \"ButtonNumber\" and \"Buttons\"?\n\nNow, please tell me how I can change the 6/7 function from \"LEFT|RIGHT\"\nto \"ALT+LEFT|RIGHT\"...;-)\n\nThe eight button do NOT work with XFree86 4.3.99.14 (XFree86 CVS/DRI CVS) 8-(\nNo response with X's \"xev\".\n\nAny glue?\n\nGreetings,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Canb't make kde-3.1.94 work with QT-3.2.x"
    date: 2003-12-11
    body: "All my attempts to run the kde-3.1.4 configure script has failed since it just does not detect the libqt-mt of QT-3.2.x !! BTW, the library qt-mt is very much there. Any help is welcome."
    author: "hapless user"
  - subject: "Re: Canb't make kde-3.1.94 work with QT-3.2.x"
    date: 2003-12-13
    body: "It worked for me. Have you set the QTDIR environment variable in your profile? And have you put the qt library path in LD_LIBRARY_PATH and the qt executable path in your PATH?"
    author: "Luke Sandell"
  - subject: "Re: Canb't make kde-3.1.94 work with QT-3.2.x"
    date: 2003-12-15
    body: "I had the same problem. \nUpon 3 days of investigation I found that isue actually with freetype library. If /usr/X11R6/lib and /usr/local contains different versions of freetype, qt-3.2.3 linkes with unresolved references.\nSynchronise freetype versions (as noted is freetype's UPGRADE.UNX file) and recompile qt.\n\nHope it helps :)\n\n(And sorry for bad english)"
    author: "deadkitten"
  - subject: "Re: Canb't make kde-3.1.94 work with QT-3.2.x"
    date: 2003-12-15
    body: "deadkitten,\n\tI discovered the exact same problem. My qt was compiled with freetype-2.0.8 but is linked with a freetype lib of different version (2.1.4) intentionally. While qt/kde apps run just fine with this, compiling is a problem."
    author: "hapless user"
  - subject: "No kdebase-suse?"
    date: 2003-12-11
    body: "I see that KDE 3.2 is not compatible with kdebase-suse <= 9.0.  Can I force kdebase3 to install even though it wants a newer kdebase-suse, and will that still work?  If I uninstall kdebase-suse, what will that break on my SuSE 9.0 system?  Is there an updated kdebase-suse available?\n\n"
    author: "Brian Kendig"
  - subject: "Re: No kdebase-suse?"
    date: 2003-12-12
    body: "i installed kde3.2b2 successfully with suse 9 ... i just forced the packages to install even when kdebase-suse complained ... no problems whatsoever ... but don't blame me if you do have any problems ;)"
    author: "Anonymous"
  - subject: "Upgraded okay, but panel is completely empty"
    date: 2003-12-12
    body: "I finally satisfied all the dependency issues with SuSE 9.0, and got KDE 3.2b2 installed.  But now my panel is completely empty -- no Kicker menu, no clock or battery monitor, no list of open windows, no nothing but a grey bar with an arrow at one end to let me show or hide it.\n\nI think I remember seeing a report of this a while ago, and how to fix it, but now I'm not able to find that info again... isn't there just a file to delete?\n\n"
    author: "Brian Kendig"
  - subject: "XMMS No Longer Working?"
    date: 2003-12-13
    body: "I just installed this new beta, and now XMMS using the libartsout.so plugin no longer works.  Any thoughts or ideas?\n\nLaura"
    author: "Laura"
  - subject: "Re: XMMS No Longer Working?"
    date: 2003-12-14
    body: "In the same boat with no paddle here.\n\nI initially thought that it just needed a recompile with the new artsd libs, but that didn't help at all.\n\nAnytime I try and play a file in XMMS with the lib-artsd, XMMS will hang for about 10-15 secconds, and then exit."
    author: "John E. Martin"
  - subject: "Re: XMMS No Longer Working?"
    date: 2003-12-21
    body: "compile your kde with arts-1.1.4, xmms doesn't work with arts-1.1.9x or newer, it is just IMPOSSIBLE, read it at the xmms.org forum."
    author: "tizz"
  - subject: "Missing features"
    date: 2003-12-13
    body: "I downloaded and compiled Konqueror 3.1.94 and noticed it was missing some\nof the nicer features previously seen in 3.1.4. Notably, the ability to\n\"Archive\" web pages, the ability to view the document tree, and the ability\nto quickly switch browser identification from the menubar. Anyone know what\nhappened? My 3.1.4 packages were from Yoper.\n\nLuke"
    author: "Luke Sandell"
  - subject: "Re: Missing features"
    date: 2003-12-13
    body: "You have to install the kdeaddons package."
    author: "Anonymous"
  - subject: "KDE locale problem"
    date: 2003-12-13
    body: "I am using Mandrake 9.2, and just installed a copy of KDE 3.2 beta from cooker, but I got a locale problem. Originally the locale of  old KDE 3.1.3 and Gnome 2.4 were both using zh_CN.GB2312, but now KDE is using UTF-8, I did checked my locale setting, it is zh_CN,\nLANG=zh_CN\nLC_CTYPE=\"zh_CN.GB2312\"\nLC_NUMERIC=\"zh_CN.GB2312\"\nLC_TIME=\"zh_CN.GB2312\"\nLC_COLLATE=\"zh_CN.GB2312\"\nLC_MONETARY=\"zh_CN.GB2312\"\nLC_MESSAGES=\"zh_CN.GB2312\"\nLC_PAPER=\"zh_CN.GB2312\"\nLC_NAME=\"zh_CN.GB2312\"\nLC_ADDRESS=\"zh_CN.GB2312\"\nLC_TELEPHONE=\"zh_CN.GB2312\"\nLC_MEASUREMENT=\"zh_CN.GB2312\"\nLC_IDENTIFICATION=\"zh_CN.GB2312\"\nLC_ALL=zh_CN.GB2312\n=====================================\nWhere can I set the locale of KDE back to GB2312?  thanks for any help \n"
    author: "Paul"
  - subject: "\"Dobra Voda\""
    date: 2003-12-14
    body: "Hi; ppl;\n\nI just wanna say it's cool to se a release name in some other language than english; and that doesn't have to do with some bird, galaxies or some strange flashy SF name ... For those unlucky who doesn't speak Croatins; \"Dobra Voda\" means \"Good Water\". :)\n\nCiaos"
    author: "a. m."
  - subject: "Kmail attachment (zip, bz...) gives crash"
    date: 2003-12-15
    body: "Hi,\nInstalled beta2, and it feels fast.\nBut kmail crashes when i want to attach a zip-file or other stuffed file.\nOther people have the same problem?"
    author: "Frank Mutsaers"
  - subject: "KDE 3.2 Beta 2"
    date: 2003-12-16
    body: "Just installed it,\nlooks great, love the transparent taskbar!!! :)\nsmooth as, really a lot faster!\nim using it on fedora core 1,\ni would assume that thoes rpms will work on redhat 9 & maby 8\nBUT there will be a few more dependancys you will have to get as well!\nwww.rpmfind.net\n\ngood luck\nnice work KDE guys!\n\nsorry for the spelling.\nbai"
    author: "Sam"
  - subject: "Panel missing in FC1"
    date: 2003-12-16
    body: "Well, it sure is fast, and seems stable enough, but I do have a couple issues with the FC1 RPMs.\n\nMy system is Fedora Core 1 (stock) running 3.14. Installed pretty much everything bar the devel packages, quanta and koffice. I got one dependency error message for libvcard.so.0 (existing quanta install), so I backed that up, installed via 'rpm -Uvh *.rpm --nodeps' and manually restored libvcard.so.0 to its original location. My issues are thus:\n\n1> I don't get a panel. My desktop loads fine, it even restored my previous session without difficulty, but no panel, no how. I can edit the panel options in kcontrol, but can't get a panel to pop. When I run 'kicker' from a command prompt as user, I get a silent failure - no bash output, no process, no panel. When i run 'kicker' as root, I get a default panel (not my customised one), but anything I run from that panel has root privileges, so that's not gonna fly. The file /usr/share/config/kickerrc shows the specs of the root panel; /home/magpie/.kde/share/config/kickerrc shows the settings I made for my user panel. They seem correct. Any ideas on how to restore the correct panel?\n\n2> My monospace font is bold by default. I'll cope :)\n\nCheers, \nM"
    author: "Magpie"
  - subject: "Re: Panel missing in FC1"
    date: 2003-12-16
    body: "<i>My system is Fedora Core 1 (stock) running 3.14. </i>\n\nI should say <i>was</i> running 3.14. It's Dobra Voda now...\n\nM"
    author: "Magpie"
  - subject: "Re: Panel missing in FC1"
    date: 2003-12-22
    body: "try to run \"kdeinit kicker\" and look at the output. I had the same problem when updating kde; there was a broken panel applet which caused kicker to crash (did you use kbinaryclock :)"
    author: "hoirkman"
  - subject: "Re: Panel missing in FC1"
    date: 2004-01-07
    body: "Hi,\n\nYou can remove ~/.kde/share/config/kickerrc and then start kicker from command line. It will run kde panel with default settings so you will need to customize it again.\n\nRegards\n\nPawel"
    author: "pawel"
  - subject: "umount  problem"
    date: 2003-12-16
    body: "Does anyone have problem with unmount dvdroms or cdroms? There are 2 dvdroms in my computer. I compiled and run \"Dobra Voda\" on Fedora and ASPLinux 9 (the same as Red Hat 9) and had got problem on both. It takes a long uncertain time to wait until dvdroms can be unmounted. Until that time I get messages \"umount: /mnt/cdrom: device is busy\",  \"Eject /dev/cdrom failed!\" \"The program kdeeject %v' can't be found\" (although kdeeject script exists in /usr/bin). The message depends on a chosen item from the menu that appears on a right click of a mouse on dvdrom device icon. With KDE-3.1.1/3.1.4 everything works fine.\nDoes anybody knows how to solve the problem?\nThanks in advance."
    author: "Alexander Malashenko"
  - subject: "Splash screen broken"
    date: 2003-12-21
    body: "I couldn't find any release notes for beta2 and I looked in bugs for this.\n\nI upgraded from Fedora Core 1's KDE3.1.4 to beta2 and the Splash screen looks weird (big blue blinking squares). In Control center the Splash Screen page looks kindof blank as well.\n\nIs this a known bug?\n\nbtw. great release! Love on first sight. Especially fond of the \"Preloading\" feature under KDE performance - Konquerer is there instantly when I click the icon.. Kapow!!! and Plastic is beautiful... I could go on.... drool.."
    author: "Erik"
  - subject: "Re: Splash screen broken"
    date: 2003-12-21
    body: "You should install this package: ftp://ftp.kde.org/pub/kde/unstable/3.1.94/RedHat/Fedora/noarch/fedora-logos-1.1.20.2-1.noarch.rpm"
    author: "Alexander Malashenko"
  - subject: "Re: Splash screen broken"
    date: 2003-12-22
    body: "Thanks.. That did the trick...\n\nHave you gotten other splashscreens installed as well? It swallows whatever I throw at it, without updating the list of splashscreens."
    author: "Erik"
  - subject: "Re: Splash screen broken"
    date: 2003-12-22
    body: "Seems like most of the Splashes on kdelook.org are missing a Theme.rc file in the archive, that's why they're not showing in the control center.\n\nCopying and adapting the Theme.rc from /usr/share/apps/ksplash/Themes/BlueCurve to  my .kde/share/apps/ksplash/Themes/xxx cusom installed splashes seem to fix everything.\n\nThe Theme.rc must be new with 3.2?\n"
    author: "Erik"
  - subject: "Re: Splash screen broken"
    date: 2003-12-22
    body: "I can see all my themes fine, but for some reason the Apply button is always grayed out no matter what I do.\n\n-B"
    author: "Blair"
  - subject: "Re: Splash screen broken"
    date: 2003-12-23
    body: "After some poking around I'm not able to Apply a different splash... I'm in the exact same spot as you.\n\nStill stuck with the standard Fedora splash screen.\n"
    author: "Erik"
  - subject: "Re: Splash screen broken"
    date: 2004-01-21
    body: "I'm using SuSE 8.2 and updated to KDE 3.1.92 and I also am unable to update my splash screen.  The \"apply\" button, as above noted, is greyed-out.  Has anyone found out how to fix this?  I really don't like the splash screen that is on there now..."
    author: "Justin"
  - subject: "HOW TO?  For Linux Newbies"
    date: 2003-12-23
    body: "Ok, I downloaded all the *rpm's for Red Hat9.  How do I install these, it keeps saying dependencies errors.\n\nIs there an order?\nPlease help"
    author: "Jason"
  - subject: "What depencies has the dobra voda beta?"
    date: 2003-12-25
    body: "I can't find a list of libs on which this beta depends. I am on a LFS-system running KDE 3.1.4 at the moment. Are there any libs I should install before compiling the beta so I get all features?\n\nCiao"
    author: "Norbert Moendjen"
  - subject: "yet another compile error (yace)"
    date: 2003-12-25
    body: "Hi all,\n\n\n\nI'm a bit eager to compile the beta2 with konstruct. Everything works fine, except compiling the kdebase. kdebase did not compile kfm (and maybe some other stuff). - So I compiled kdebase \"by hand\". The result of it is always the same: \n\nmake[3]: Entering directory `/mnt/hdb6/new/konstruct/kde/kdebase/work/kdebase-3.1.94/kate/utils'\n\n/bin/sh ../../libtool --silent --mode=link --tag=CXX g++  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -O2 -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION -D_GNU_SOURCE    -o libkateutils.la -rpath /opt/kde3.2-beta2/lib -L/usr/X11R6/lib -L/opt/kde3.2-beta2/lib  -no-undefined -Wl,--no-undefined -Wl,--allow-shlib-undefined dockviewbase.lo messageview.lo listboxview.lo -lkdeui -lkdecore -lkio\n\ncollect2: ld terminated with signal 11 [Segmentation fault]\n\nmake[3]: *** [libkateutils.la] Fehler 1\n\nmake[3]: Leaving directory `/mnt/hdb6/new/konstruct/kde/kdebase/work/kdebase-3.1.94/kate/utils'\n\nmake[2]: *** [all-recursive] Fehler 1\n\nmake[2]: Leaving directory `/mnt/hdb6/new/konstruct/kde/kdebase/work/kdebase-3.1.94/kate'\n\nmake[1]: *** [all-recursive] Fehler 1\n\nmake[1]: Leaving directory `/mnt/hdb6/new/konstruct/kde/kdebase/work/kdebase-3.1.94'\n\nmake: *** [all] Fehler 2\n\n\n\nAny help is welcome.\n\nMerry christmas, \n\nNero"
    author: "nero2005"
  - subject: "Re: yet another compile error (yace)"
    date: 2003-12-26
    body: "> Everything works fine, except compiling the kdebase. kdebase did not compile kfm\n\nThe reason may be that there is no kfm in kdebase. :-)\n\n> collect2: ld terminated with signal 11 [Segmentation fault]\n\nThis shouldn't happen. Try to upgrade to a newer version of ld."
    author: "Anonymous"
  - subject: "Re: yet another compile error (yace)"
    date: 2003-12-27
    body: "collect2: ld terminated with signal 11 [Segmentation fault] is most the times an hardware defect. Overheating or bad rams. If it's overheating the just type make a second time after your cpu has cooled down.\n\nCiao Nobbe"
    author: "Norbert Moendjen"
  - subject: "Re: yet another compile error (yace)"
    date: 2003-12-28
    body: "That's it! Overheating of AMD-CPU and a bad (though the most recent) version of binutils incl. \"ld\" causes the error. Thanks. But how can I avoid the error of ld?\nMeanwhile I watch out for kfm :)\n\nNero"
    author: "nero2005"
  - subject: "3 Packages won't compile"
    date: 2003-12-27
    body: "Hello,\n\nkdebindings doesnt compile kdejave error is\nmake[4]: Entering directory `/usr/src/kde/kdebindings-3.1.94/kdejava/koala/kdejava'\n/bin/sh ../../../libtool --silent --mode=link --tag=CXX g++  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -DNDEBUG -DNO_DEBUG -O2 -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -march=athlon-xp -m3dnow -msse -mfpmath=sse -mmmx -O3 -pipe -fforce-addr -fomit-frame-pointer -funroll-loops -frerun-cse-after-loop -frerun-loop-opt -falign-functions=4 -maccumulate-outgoing-args -ffast-math -fprefetch-loop-arrays -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common    -o libkdejava.la -rpath /opt/kde/lib -L/usr/X11R6/lib -L/opt/qt/lib -L/opt/kde/lib  -no-undefined -Wl,--no-undefined -Wl,--allow-shlib-undefined -version-info 1:0:0  libkdejava_la.all_cpp.lo  ../../../qtjava/javalib/qtjava/libqtjava.la -lkjs -lkscript -lkhtml -lkmid -lkspell -lkio\n.libs/libkdejava_la.all_cpp.o: In function `Java_org_kde_koala_MouseReleaseEvent_newMouseReleaseEvent__Lorg_kde_qt_QMouseEvent_2IILjava_lang_String_2Ljava_lang_String_2Lorg_kde_koala_DOMNode_2J':\n.libs/libkdejava_la.all_cpp.o(.text+0x6bc2e): undefined reference to `khtml::MouseReleaseEvent::s_strMouseReleaseEvent'\n.....\n.libs/libkdejava_la.all_cpp.o(.gnu.linkonce.t._ZN5khtml14MouseMoveEventD0Ev+0x29): undefined reference to `khtml::MouseEvent::~MouseEvent [not-in-charge]()'\n.libs/libkdejava_la.all_cpp.o(.gnu.linkonce.t._ZN5khtml14MouseMoveEventD1Ev+0x25): more undefined references to `khtml::MouseEvent::~MouseEvent [not-in-charge]()' follow\ncollect2: ld returned 1 exit status\nmake[4]: *** [libkdejava.la] Error 1\nmake[4]: Leaving directory `/usr/src/kde/kdebindings-3.1.94/kdejava/koala/kdejava'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/usr/src/kde/kdebindings-3.1.94/kdejava/koala'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/usr/src/kde/kdebindings-3.1.94/kdejava'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/usr/src/kde/kdebindings-3.1.94'\nmake: *** [all] Error 2\n------------------------------------------------------------------------\nkdesdk won't compile the error is\nIn file included from /usr/include/FlexLexer.h:47,\n                 from pofiles.cc:241:\n/usr/include/c++/3.2.1/backward/iostream.h:36: using declaration `istream'\n   introduced ambiguous type `istream'\npofiles.cc:401:5: warning: \"YY_STACK_USED\" is not defined\npofiles.cc: In member function `virtual int GettextBaseFlexLexer::yylex()':\npofiles.cc:683: cannot convert `std::istream*' to `istream*' in assignment\npofiles.cc: In member function `void\n   GettextBaseFlexLexer::yy_load_buffer_state()':\npofiles.cc:1216: cannot convert `istream*' to `std::istream*' in assignment\npofiles.cc: In member function `void\n   GettextBaseFlexLexer::yy_init_buffer(yy_buffer_state*, std::istream*)':\npofiles.cc:1267: cannot convert `std::istream*' to `istream*' in assignment\npofiles.cc:1458:5: warning: \"YY_MAIN\" is not defined\nmake[4]: *** [pofiles.lo] Error 1\nmake[4]: Leaving directory `/usr/src/kde/kdesdk-3.1.94/kbabel/common/libgettext'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/usr/src/kde/kdesdk-3.1.94/kbabel/common'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/usr/src/kde/kdesdk-3.1.94/kbabel'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/usr/src/kde/kdesdk-3.1.94'\nmake: *** [all] Error 2\n---------------------------------------------------------------------\nkdedevelop won't configure I am still on test\n\nAny help would be nice. Everything else is running fine and I like the beta\n\nCiao Nobbe"
    author: "Norbert Moendjen"
  - subject: "Re: 3 Packages won't compile"
    date: 2006-08-24
    body: "cd to kbabel's parent dir (where your \"making\") and run\n\nsed -i 's/class istream;/#include <istream.h>/' ./kbabel/common/libgettext/pofiles.cc && make\n\n\nhth\n\n"
    author: "Ulrich van Brakel"
  - subject: "Wow!!! It's great!"
    date: 2003-12-28
    body: "Hi, I've installed on my Slack 9.1 box. Are you sure it's a \"beta\"?! Pretty stable, good-looking and fast. It goes very well with the Slack!!\n\nThank you very much! I'm anxious about the final version (3.2). I think it will be a state-of-art desktop, better than everyone else.\n"
    author: "Paulo Igor"
  - subject: "Re: Wow!!! It's great!"
    date: 2004-01-05
    body: "Does anyone have problems with AA fonts in kde 3.2?  I'm running slack 9.1 btw."
    author: "borgware"
  - subject: "how to make new folder in konq detailed list view?"
    date: 2003-12-30
    body: "I can find no way to make a new folder in konqueror when using the detailed list view.   No matter where I right click, I get a context menu for a file with no 'create new' choice.  I have to switch to icon view to be able to do that.\n\nThis sucks, and is different from how it was in konqueror 3.1.x.  In 3.1.4, you could right click on any column but the name column and get a generic menu (including 'create new' and 'paste')\n\nNow, you cannot right click to paste a file.  You can do this in the menu of course (still lost functionality imho) but it is impossible to create a new folder.  It was like this in beta 1, and it is like this in beta 2.\n\nIs it this way on purpose?"
    author: "Jason W"
  - subject: "Re: how to make new folder in konq detailed list view?"
    date: 2003-12-30
    body: "Resize either your columns or your window and right click after the last column."
    author: "Anonymous"
  - subject: "Re: how to make new folder in konq detailed list view?"
    date: 2003-12-30
    body: "heh that works all right.  If I fullscreen konq and squish a couple columns down, it works.  Thanks\n\nI still think it's less than optimal, especially compared with how it was.  Something like this was reported as a bug for 3.1.1  The reporter was told he could click in any column but the name column (he was clicking in the blank part of the name column), so no bug.  Now, you can't even do that.  Can't click in any column.  Ugh.  What's the reasoning behind this?  \n\nOr since it's likely to be this way forever, can anyone give me a clue about where in the code the change was made?  Could be a fun learning experience :D"
    author: "Jason W"
  - subject: "Some problems in the begining. "
    date: 2004-01-04
    body: "Hi i had some problems with my libfam.so and libfam.so.0 . But it wasn't a problem.  I just copyed the old files from my other computer. \n\nI think that the KDE team have done a great work. \nI have some minder problems but i think it will be shalled sooner or later. \nOne problem i have is whit wine. I cant play Counter-stike in KDE. But other games like Warcraft works. \n\nI awate for the coming programs that will shall some minter problems. \n\nGreat job KDE . \n\n/ Teo \n\nHope you understand what i mean. Have a littel bit problems with the spelling in English. "
    author: "Teodor Engqvist "
  - subject: "Not Cool :("
    date: 2004-01-13
    body: "Well after upgrading from KDE3.1.1 to KDE3.1.4, i just couldn't help but rush off to kde 3. But, in order to do that, I had to upgrade a lot of other packages. That includes a newer Qt and glib. Glib worked fine, and qt installed like a breeze, but, now kde wont compile, it spits out a syntax error!\n\n\": undefined reference to `QGList::erase(QLNode*)'\"\n\"undefined reference to `QString::arg(long long, int, int) const'\"\n\nNow being that I use Qt in all my development, i know QString shoudl work.. So I checked out the documentation for Qt 3.3.0-beta1 (thats the only one i could download), and found the followign problems:\n\n\"QList Class Reference (obsolete)\n The QList class has been renamed QPtrList in Qt 3.0.\"\n\nAnd for QString, I coudln't find a match for the arguments KDE tries to pass to the QString class...\n\nWhats going on?!"
    author: "Dreq"
  - subject: "Re: Not Cool :("
    date: 2004-01-13
    body: "Note: going to http://doc.trolltech.com/3.3/qglist.html takes you to QList, and Qglist is nowhere to be found in the 3.3 documentation.."
    author: "Dreq"
  - subject: "Disapearing taskbar"
    date: 2004-01-13
    body: "Hello, \ni've just istalled new KDE on Fedora, everything is fine, but after some time taskbar disapears and (autohide is turned off), i'm new in Linux, so maybe i'v missed sth obvious.\nBest regards\n\nTrikster"
    author: "Trikster"
  - subject: "KDE panel crash - Kicker?"
    date: 2004-01-14
    body: "(no debugging symbols found)...Using host libthread_db library \"/lib/tls/libthread_db.so.1\".\n(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...[Thread debugging using libthread_db enabled]\n[New Thread -1085017216 (LWP 1196)]\n\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...0x00ad0c32 in _dl_sysinfo_int80 ()\n   from /lib/ld-linux.so.2\n#0  0x00ad0c32 in _dl_sysinfo_int80 () from /lib/ld-linux.so.2\n#1  0x00d59a03 in __waitpid_nocancel () from /lib/tls/libpthread.so.0\n#2  0x00944da1 in KCrash::defaultCrashHandler(int) ()\n   from /usr/lib/libkdecore.so.4"
    author: "Trikster"
  - subject: "kicker crash "
    date: 2004-01-15
    body: "[root@localhost root]# kdeinit kicker\nkdeinit: Shutting down running client.\n---------------------------------\nIt looks like dcopserver is already running. If you are sure\nthat it is not already running, remove /root/.DCOPserver_localhost.localdomain__0\nand start dcopserver again.\n---------------------------------\n \nQPixmap: Cannot create a QPixmap when no GUI is being used\nQPixmap: Cannot create a QPixmap when no GUI is being used\nKDE Daemon (kded) already running.\nQPixmap: Cannot create a QPixmap when no GUI is being used\nQPixmap: Cannot create a QPixmap when no GUI is being used\nkbuildsycoca running...\nReusing existing ksycoca\n[root@localhost root]# QWidget (unnamed): deleted while being painted\nQPaintDevice: Cannot destroy paint device that is being painted\nQWidget (unnamed): deleted while being painted\nQPaintDevice: Cannot destroy paint device that is being painted\nX Error: RenderBadPicture (invalid Picture parameter) 184\n  Major opcode:  157\n  Minor opcode:  6\n  Resource id:  0x800103\nX Error: RenderBadPicture (invalid Picture parameter) 184\n  Major opcode:  157\n  Minor opcode:  6\n  Resource id:  0x800103\nX Error: RenderBadPicture (invalid Picture parameter) 184\n  Major opcode:  157\n  Minor opcode:  5\n  Resource id:  0x800103\n \n\n"
    author: "Trikster"
  - subject: "good work but."
    date: 2004-01-19
    body: "But I have to say the media component in 3.2 beta is a total crap.\nNot even usable. And a lot of basic errors."
    author: "netghost"
  - subject: "kwallet"
    date: 2004-05-03
    body: "how do i start kwallet manager?"
    author: "pvamp"
---
Our <a href="http://www.kde.org/announcements/announce-3.2beta1.php">first KDE 3.2 Beta "Rudi"</a> was a huge success and resulted in over 2000 resolved bugs. As the code still has some rough edges, we decided to go for another beta. So here it is: <i>Dobra Voda</i>. Please continue reporting problems you see with it, your testing is much appreciated. You can download Dobra Voda at <a href="http://download.kde.org/unstable/3.1.94/">http://download.kde.org/unstable/3.1.94</a>. Currently we have binaries for Fedora, Slackware and SUSE, besides the sources.







<!--break-->
