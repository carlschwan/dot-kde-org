---
title: "KDevelop: New Qt Based Smartphone with KDevelop SDK"
date:    2003-12-17
authors:
  - "gstaikos"
slug:    kdevelop-new-qt-based-smartphone-kdevelop-sdk
comments:
  - subject: "Cool!"
    date: 2003-12-17
    body: "Wanna get one of those :)"
    author: "Stormy"
  - subject: "Qt Free Edition, Not Qt Embedded"
    date: 2003-12-17
    body: "\nDuring my visit to SoftExpo 2003 in Seoul this month I met the GUI developer team for this device (and of course got to play with it for a while).  A very ambitious and talented group.\n\nOne of the interesting things I learned is that the device uses Qt Free Edition / X11 rather than Qt Embedded.\n\nIt also has a very nice MPEG3 video player.\n\nIf the device does well in China we may very well see a US / European version . . ."
    author: "Dre"
  - subject: "Re: Qt Free Edition, Not Qt Embedded"
    date: 2003-12-17
    body: "Updated the article... There is a Qt Free Edition / Embedded, btw."
    author: "Navindra Umanee"
  - subject: "Re: Qt Free Edition, Not Qt Embedded"
    date: 2003-12-17
    body: "Thank you for your admiration.\n\nNow we've to work again without any vacation\nfor next product and food.\n\nI'm sorry for our poor documentation and insufficient materials."
    author: "Seo Young-Jin - MIZI member"
  - subject: "Re: Qt Free Edition, Not Qt Embedded"
    date: 2003-12-17
    body: "damn good!\n\nI want this phone!\n\nHappy XMAS ;)\ntrapni."
    author: "Christian Parpart"
  - subject: "Re: Qt Free Edition, Not Qt Embedded"
    date: 2003-12-17
    body: "Your work looks stunning and beautiful.  Thanks!\n\nI agree, I want this phone.  :-)"
    author: "KDE User"
  - subject: "Re: Qt Free Edition, Not Qt Embedded"
    date: 2003-12-18
    body: "Wow, that looks nice.   I've sure been waiting for something like this."
    author: "TomL"
  - subject: "KDevelop 3 is really shaping up!"
    date: 2003-12-17
    body: "KDevelop 3 is really getting damn sweet.  Everyone please get those last minute bugs into bugs.kde.org!\n\n"
    author: "Ian Reinhart Geiser "
  - subject: "Re: KDevelop 3 is really shaping up!"
    date: 2003-12-17
    body: "Seeing as it's so good, and I agree, it really is, PLEASE PLEASE PLEASE can we get it released some day so we can actually use it! :)\n\nDavid"
    author: "David Pye"
  - subject: "Re: KDevelop 3 is really shaping up!"
    date: 2003-12-18
    body: "Just use Beta 2 or CVS."
    author: "Anonymous"
  - subject: "I want a Phone"
    date: 2003-12-17
    body: "Not a PDA... i have also played with the specially designed China Unicom phone (Samsung, CDMA) and I am not as impressed. It is big and cumbersome and it doesn't have video features. Maybe it can do more once they decide to use Qt/Embedded instead of suffering with X11.\n\nMore interesting though, A-760 is going pretty well for the GSM market; still not a tri-mode phone but coming next year.\n\nHowever, Linux is rockin!! now, there are three phones in the market in China and more to come next year for sure.\n\nHas anyone seen a Qtopia phone yet? I think this is next step from Trolltech."
    author: "Phone Geek"
---
A <a href="http://linuxdevices.com/articles/AT4481058519.html">new Linux based smartphone</a> by Samsung was announced recently.  The SCH-i519 phone runs a <a href="http://www.mizi.com/en/prod/embed/embedded_smart.htm">Linux distribution</a> produced by <a href="http://www.mizi.com/">Mizi Research Inc</a>.  The user interface is based on Qt, and according to their web site, <a href="http://www.mizi.com/en/prod/embed/images/structure_diagram.gif">the SDK they provide</a> is based on <a href="http://www.kdevelop.org/">KDevelop</a>!  Congratulations to <a href="http://www.trolltech.com/">Trolltech</a> for yet another phone using Qt, and to the KDevelop team for producing the best IDE available on Linux!

<!--break-->
