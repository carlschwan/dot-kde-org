---
title: "KDE wins Linux Journal's Readers' Choice Award 2003"
date:    2003-10-10
authors:
  - "binner"
slug:    kde-wins-linux-journals-readers-choice-award-2003
comments:
  - subject: "Although no real surprise"
    date: 2003-10-09
    body: "\"Pride goes before a fall\" ....\n \n Favorite Web Browser: MOZILLA  - good choice, but I like Konqui more\n Favorite E-mail Client: EVOLUTION - can't understand this one, it's a beast\n Favorite Linux Game: FROZEN BUBBLE  :-D.... gooood one\n Favorite Graphics Program: THE GIMP - definitively\n Favorite Instant-Messaging Client: GAIM  - to goad on kopete...\n Favorite Programming Language: C++  interesting... most KDE developers can surely agree on this one, but isn't it supposed to be plain C with most of the linux folks?\n Favorite Office Program: OPENOFFICE.ORG - for the lack of better alternatives\n Favorite Linux Web Site: SLASHDOT -ah, no ....  dot.kde.org !\n Favorite Desktop Environment: KDE - 'nough said\n\nall in all : congrats to all the people helping to make KDE better with each release and who made KDE what it is today. Way to go! But we won't run out of room for improvements anytime soon I guess... As competition is a good thing, maybe one day we'll see Konqueror as the 1\u00b0 Browser.... or kopete displacing gaim... who knows... But as most of the winners deserved to win (me thinks) it won't be easy"
    author: "Thomas"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "What I really can't understant is vim as the best editor... bleartht! (no offense, just my taste), while kate is so nice :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "I love kate, an I am using it most of the time. But vi is handy when you\nhave no X for one reason or the other :-)"
    author: "MandrakeUser"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "Maybe we should start making a diference between user's choices and desktop choices...\nBecaus vi, vim and emacs are really powerful editors, noone can denie it, more powerful than kate, but if a common user takes a look at the award winning aplications then, he'll find desperate with those text editors while he'll find confortable with kate, kwrite, and whatever gnome has.\nI think vi, vim and emacs shouldn't be in the same category as kwrite and kate as nobody expects Tex/LaText in the same category as KWord or OpenOffice Writter (well, maybe some people does)."
    author: "Pupeno"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "Agreed ! There should be a category for \"console based\" and a category for \"graphical\" text editors. Good point. "
    author: "MandrakeUser"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: ">There should be a category for \"console based\" and a category for \"graphical\" text editors.\n\nVim is both!\n\nAs for kate, I don't know, a long time ago, I felt that NEdit was much better, but it was a long time ago, I've not compared both lately.\n"
    author: "RenoX"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "Then that's what emacs is for!\n\n</troll>"
    author: "Stephen Douglas"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "It's all what you used to. I've tried to get into kate/kwrite, but I still find XEmacs to be more to my liking, even for non-programming files, simply because I am used to it. I suspect vim people are the same way (though why they would use vim instead of elvis is still unfathomable to me)."
    author: "David Johnson"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "I think most KDE developers are not exactly \"C++ is number 1\" guys. I expect almost everyone has a language he likes best (even if it's a different one for each :-)\n\nHowever, C++ is the easiest way to get your code to other people, sadly."
    author: "Roberto Alsina"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "Evolution is not a beast. It actually has very good memory useage, like 17MB or so. Not bad for something its size and ability. Mozilla is a hog though. easily above 50MB whenever I use it. But I still use it. But I have 512MB so no complaining. I am usually using about 178MB of memory usually, with Mozilla running so no problem. Too bad Openoffice won against Gnumeric though. Gnumeric is much better than oocalc, but being a whole suite, I suppose it somewhat deserves the prize."
    author: "Maynard"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "> Gnumeric is much better than oocalc, but being a whole suite, I suppose it somewhat deserves the prize.\n\nI agree on OO.calc being suboptimal (in fact there have been several moments when I was at war with OO.calc for its unresponsive-ness and slowness with large chunks of data), but Gnumeric better than OO.calc? If I am sometimes at war with OO.calc, using Gnumeric would be like shooting myself in the foot...."
    author: "Thomas"
  - subject: "Re: Although no real surprise"
    date: 2003-10-10
    body: "I like gnumeric but I was surprised to see it cannot manage a table with several thousand records while oocalc had no problem. \n\n"
    author: "SorinM"
  - subject: "Re: Although no real surprise"
    date: 2003-10-11
    body: "Quanta?\n\nMozilla? --> Firebird is the better choice in mozusability. Mozilla also includes a useful wysiwyg website composer, hope quata will fill the gap.\n\nKdevelop?\n\nK3B?\n\nKontact?\n\nGIMP --> I am still waiting for a Paintdhop like KIMP!\n\nEvolution -- Outlook rip off. there are many good working email clients.\n\nOpen Office? Lacks of KDE integration. Too slow."
    author: "Gerd"
  - subject: "Re: Although no real surprise"
    date: 2003-10-12
    body: "I want KIMP too! a full good oldfashioned ripoff of GIMP.. sadly thats not very easy from a programmers point of view (from what i hear)\n\n/kidcat"
    author: "kidcat"
  - subject: "Just the DE"
    date: 2003-10-10
    body: "KDE's the preferred desktop, but applications that are preferred are decidedly not KDE apps.  I really have to agree.  I do use Konqueror, in very large part because of the web shortcuts like gg:, but I also use gaim and xmms.  Maybe it's in part because I can get them to act \"unix-y\", such as using C-a to go to the beginning of a line.  In addition, the gtk file requester actually uses tab-completion.  It may suck a lot in other respects, but that hardly matters to me if tab-completion doesn't work.  I like the look of KDE's file requester, especially with the \"speedbar\" or whatever it's called, but I absolutely hate how tabbing goes to the next widget instead of does completion.  I can understand newbies needing tab to do widget changing, but at least having the option of tab complete would make the experience so much nicer for experienced unix hackers.\n\nIn addition, these non KDE apps seem to \"just work\".  The KDE equivalents, for one reason or another, rarely seem to just be written to be used.  One big beef is that all these KDE programs want arts to be running.  Well, I don't want to run it.  I have zero need for it, but these programs want yet another process running on my system that I don't need.\n\nI hate sounding like I'm getting down on KDE developers, but the only KDE apps I use on a regular basis, apart from things like panel applets, are Konqueror, konsole (from KDE 2, because KDE 3's konsole is broken in that it always has a frame at the top and bottom, and I really only use konsole because rxvt doesn't hide the cursor when you type), and on occasion knode and kmail, though I use slrn and mutt for my primary news and mail accounts.\n\nI can't really put it into words, but a lot of KDE's programs just don't feel like they are finished products.  What I've listed above is certainly not comprehensive so a point-by-point rebuttal isn't going to solve everything.  I want to run kopete, I want to run whatever the media player du jour is, but I won't if they're not the best.\n\nI know this'll be taken as KDE bashing, but it's not.  I love a lot of what KDE provides, but that doesn't mean it can't be better."
    author: "KDE User"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "I agree that not having Ctrl-A is a big pain.  You can easily change it in the KDE setup by setting your keyboard bindings to Unix but you have to change each time on every single KDE system you use.  Very much a pain.  It makes no sense when Ctrl-E works as expected.  KDE has Tab completion too.  Just set your keybindings to the Unix scheme.\n\nIn any case, making Ctrl-A select all is really quite retarded but there's no going back.\n\nMozilla vs Konqueror is no contest.  Konqueror wins hands down.  Mozilla is such a bloated buggy pig, it's not funny.  I work in a lab and I've seen many people burnt by Mozilla when for some reason it goes beserk and uses up all the quota.  Konqueror rules all the way.  Well the only problem is that the tabs suck ass compared to Mozilla and I'm using HEAD....  they change in size randomly and move around and flicker quite annoyingly.\n\nI haven't the slightest clue what your problem with Konsole 3 is.  It rocks.  It's great.  It's the best.\n\nI agree about XMMS.  It sucks, but it sucks much less than the KDE alternatives so far."
    author: "ac"
  - subject: "Totally disagree!"
    date: 2003-10-10
    body: "Mozilla Firebird and Opera 7.2 beat Konqueror in quality of rendering, user interface, features, and many other areas. If it isn't to your liking, you ccan always choose from the hundreds of easily installed extensions available for it: http://texturizer.net/firebird/extensions/\n\nI'm missing more thana  dozen things in Konueror. While it is a good web browser for average users, for power users like me, it is lacking too many things and I just can't stand the bloated, unresponsive and poorly thought out UI.\n\nHere are just a few of the bugs I submited about it: \n\n- http://bugs.kde.org/show_bug.cgi?id=62736\n- http://bugs.kde.org/show_bug.cgi?id=62680\n- http://bugs.kde.org/show_bug.cgi?id=63870\n- http://bugs.kde.org/show_bug.cgi?id=53772\n- http://bugs.kde.org/show_bug.cgi?id=62675\n- http://bugs.kde.org/show_bug.cgi?id=59023\n- http://bugs.kde.org/show_bug.cgi?id=51277\n- http://bugs.kde.org/show_bug.cgi?id=52884 (can't believe such a basic feature was omitted)\n- http://bugs.kde.org/show_bug.cgi?id=62678\n- http://bugs.kde.org/show_bug.cgi?id=62679 (makes navigation panel useless for me)\n- http://bugs.kde.org/show_bug.cgi?id=59789\n\nKonqueror is a great browser, but just not for power users. I stil use it a lot, but mostly as a file manager and because of it's unparalleled integration which is something I lack in Firebird. That's what you get for being cross platform ;)\n\nI am speaking solely about Konqueror 3.1.4, I have never used Konqueror 3.2, but i will definitely check it out in 3.2."
    author: "Alex"
  - subject: "Re: Totally disagree!"
    date: 2003-10-10
    body: "> I am speaking solely about Konqueror 3.1.4, I have never used Konqueror 3.2, but i will definitely check it out in 3.2.\n\nAs well as you should. When I was running 3.1, I used Firebird too, but once the alphas/betas came out, I switched back to Konqueror after a very long break from using it as my primary browser (last time I used it was before Mozilla 0.8.. around Mozilla M16 I think, when Mozilla still sucked balls. )"
    author: "anon"
  - subject: "Re: Totally disagree!"
    date: 2003-10-10
    body: "Konqueror is an open project: help out if you can.\nI'm totally happy with the konqueror and none of the bugs you filed really bother me.\nI have filed just one bug report because of tab handling (in Firebird the double click to open new tabs is just better)\n\nI have 2 or 3 websites that are not rendered as good as in Mozilla or IE but I can live with that. JavaScript seems also to be very slow, but I'm young I have enough time...\n\nI use CVS and at least the menus are much better so give it a try.\n\nI would also love to see extensions like you have in Firebird, but most of them are more toys than really useful (the note sidebar would be absolutely great, but I can't imagine this will ever make it into konqueror)"
    author: "why not you?"
  - subject: "Re: Totally disagree!"
    date: 2003-10-10
    body: ">  - http://bugs.kde.org/show_bug.cgi?id=52884 (can't believe such a basic feature was omitted)\n\nWell I can't believe you describe that as a basic feature. It's a wishlist for one thing, it's something not many people would use for another and correct me if I'm wrong but it hasn't been in Mozilla for *that* long."
    author: "Stephen Douglas"
  - subject: "Ok"
    date: 2003-10-11
    body: "Yeah, I guess bookmark searching is not a basic feature. I'm jsut spoiled I guess, I've known it to be in mozilla since 0.9 though. Your right, though its not a basic feature, I mean after all I.E, the browser used by over 95% of internet users doesn't have it, but thean again we're talking about I.E. here...\n\nSitll, I think that bookmark searching is as important as searching your file system, I use it every week. I am very happy that it is now in Konqueror ;)"
    author: "Alex"
  - subject: "Re: Ok"
    date: 2003-10-11
    body: "If you organize your bookmarks, there is no need for a search-function.\n\n\"create new folder\" exists!"
    author: "name"
  - subject: "Re: Ok"
    date: 2003-10-11
    body: "Maybe for your simple web browsing eneds. Maybe you don't have over 150 bookmarked pages. For me Mozilla easily allows searching with all kinds of criteria asily, try Mozilla 1.4.1 and see what I mean, their search functions are amazing. In fact it can even allow you to have dynamic bookmarks which means that bookmarks always go to the right folder.\n\nFor example I have set up that anything with linux as keyword, title, url or description automatically goes to my \"General Linux\" folder. too bad msot people don't know half of Mozilla's features and too bad these advanced searching options are not yet in Firebird.\n\nReally, fi Konqueror developers want to know what I mean by the advanced searching options through out Mozilla and its very useful features which Konqueror si very far behind in, try it! I'm sure most of you have, but jsut saying NO on principle because you are not building mozilla is outright ignorance."
    author: "Alex"
  - subject: "Re: Totally disagree!"
    date: 2003-10-10
    body: "Are you saying that the konqueror developers, who use Konqueror exclusively, are not power users then?  If so, I would think you are also capable of writing a browser in your existing spare time.\n\nEveryone who considers himself a \"power user\" has different views on what should be there, and has different needs.  You are no exception to this rule."
    author: "George Staikos"
  - subject: "Re: Totally disagree!"
    date: 2003-10-11
    body: "I don't know, probably they are, but that is not necessary. Just like if you build race cars it doesen't mean that you will be racing at 400 KM/H.\n\nI am not even close to capable of writting something 1/100th as complex as Konqueror, but I am learning a bit of C++. Anyway, this really has little to do with being a power user.\n\nI know the standards for power user features are differnt for everyone, but for ME searching bookmarks is a power user feature that is very useful."
    author: "Alex"
  - subject: "Re: Totally disagree!"
    date: 2003-10-10
    body: "http://bugs.kde.org/show_bug.cgi?id=62675\n\nThe easiest way to fix this is to turn on word-wrap icon text in the konqueror appearance configuration."
    author: "Dragen"
  - subject: "Re: Totally disagree!"
    date: 2003-10-10
    body: "That's not a fix, that's a workaround."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Totally disagree!"
    date: 2003-10-10
    body: "Please don't get me started about Opera. I don't use Konqueror, but if Opera is better then Konqueror is pretty useless. Don't get me wrong, I like opera, but on some sites, it doesn't render properly, in fact, it renders terribly wrongly. Like having to scroll across to read one line which is supposed to be a paragraph. Opera has its good points, but rendering is not its best attribute. I am sure Konqueror is much better than Opera."
    author: "Maynard"
  - subject: "Re: Totally disagree!"
    date: 2003-10-11
    body: "Opera 7.2 is very good, solely as a browser and if you don't count integration it renders faster, and it seems to have more features. It is slower to start, but still quite fast."
    author: "Alex"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: ">  flicker quite annoyingly.\n\nyou should try the qt-copy patches to fix this.. they are qt problems, not konq problems."
    author: "anon"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "\"Mozilla vs Konqueror is no contest. Konqueror wins hands down. Mozilla is such a bloated buggy pig, it's not funny.\"\n\nWhich version was that? Mozilla 0.1a1? While I really like Konqueror for moderately fast-and-friendly filesystem browsing (although it could be better) and some Web browsing, I think it's hard to truthfully claim that Konqueror is as comprehensive as Mozilla for standards support. Now if you'd said \"IE vs Konqueror\" I'd possibly go along with you. ;-)\n\nI think the comments were pretty fair. KSpread is pretty good, but the other office and productivity applications seem pretty unstable. I've barely started using KMail, but I think it does the job, even though there's also some room for improvement. I like the look of KOrganizer and the way there's a KPart for Konqueror that embeds the calendar for iCalendar files. But many applications do seem as if they're in a continual feature race, as opposed to stability consolidation."
    author: "The Badger"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "Mozilla is *way* slower than Konq on my Athlon classic800.. they are both about same on my AthlonXP 2200+\n\nFirebird is a bit faster than Moz but takes a much longer time to load than Konq"
    author: "anon"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "Can you explain which standards please?  What is missing from KHTML that Mozilla implements?  Specifics, not and handwaving in the direction of \"standards\".  Thank you.\n\nOh and I can start IE 6 inside Crossover Office on my Athlon PC faster than Mozilla or Firebird starts up.  Konqueror beats both of them and Opera (which is pretty snappy to start up).  I timed them all, and I did subjective tests, both consistent in results.  I did the same tests with similar results on Mac OS X (using MacIE, Safari, Mozilla, Firebird, etc) and PPC Linux.\n\nI am sure there are people out there with optimized Mozilla builds and broken KDE installations who see Mozilla start faster too.  \n"
    author: "George Staikos"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "Well I can tell you one CSS tag it ignores off the top of my head.  This is from 3.1.4 mind you, so I do not know if it had been corrected in 3.2.\n\nI know for a fact that min-height does not work.  Works in Mozilla and Opera.  Not in IE though, suprised?\n\n\nsmeat!"
    author: "SMEAT!"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "And what if I can provide HTML+CSS that khtml renders correctly but no other browser does?  Does that mean that only khtml is standards compliant then?  One example means nothing.  Ten examples mean nothing.  What matters is the entire spec and all the interactions between different parts of the specs and different specs."
    author: "George Staikos"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "I agree.. while almost all modern browsers implement all of CSS1, for example, all browsers have quirks in their implementations. IE has quirks, Mozilla has quirks, KHTML has quirks, opera has quirks. \n\nIn terms of CSS2 and 3 support, Opera 7.2 supports the greatest amount of the standard. Mozilla 1.4 has the most complete XHTML support. \n\nHowever, in reality, only things that IE support are actually used on the web. Very few websites are written for the w3c dom even. "
    author: "anon"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "Well is that with the KDE/Konqueror libs preloaded or not?  Of course with all those libs preloaded by kdeinit, Konqueror will start fast."
    author: "ac"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "Beleive me, if I could preload Moz in someway without having to install galeon or Epiphany, I would. However, it's only supported in Windows. Grr.r."
    author: "anon"
  - subject: "Re: Just the DE"
    date: 2003-10-11
    body: "Well, I've mostly been happy with the CSS support in Konqueror, so perhaps my standards claims are overstated. If so, I apologise if I've misrepresented the state of the art in that respect, especially since Konqueror's support for selectors and pseudo selectors (if that's the right term) seem pretty evolved.\n\nOn the subject of start-up time, whilst Konqueror is clearly faster in that area, I think emphasis on such measures are misleading. Firebird is supposedly leaner and faster than Mozilla, but the rendering performance of Mozilla has, in my experience, been better than Firebird on the same system. I usually only start my browser once in any given computing session, so like those classic arguments around fast boot-up times, I don't find it compelling to switch browser to shave a few seconds off an infrequent activity (start-up) to make a frequent activity (page viewing) slower.\n\nAnyway, apologies to any unintentionally offended parties and the obligatory kudos to the KDE team for giving Mozilla some competition."
    author: "The Badger"
  - subject: "Re: Just the DE"
    date: 2003-10-11
    body: "The problem with konsole 3 is that when you go fullscreen, there are bars at the top and bottom of the screen still, even when you select no frame.  Very annoying, and konsole 2 didn't do it, thus I use konsole 2."
    author: "KDE User"
  - subject: "Re: Just the DE"
    date: 2003-10-11
    body: "It's a Qt problem: http://lists.kde.org/?l=konsole-devel&m=105464244808844&w=2"
    author: "Anonymous"
  - subject: "Re: Just the DE"
    date: 2003-10-10
    body: "> KDE's the preferred desktop, but applications that are preferred are\n> decidedly not KDE apps. I really have to agree. I do use Konqueror, in very\n> large part because of the web shortcuts like gg:, but I also use gaim and\n> xmms. Maybe it's in part because I can get them to act \"unix-y\", such as\n> using C-a to go to the beginning of a line.\n\nHuh. That's actually why I use KDE these days. GTK 1.x used to act very \"unix-y\", but they really messed up 2.x... Most applications use ctrl+a for \"select all\", and some won't even let you reassign the shortcuts, because the old way (pointing at a menu item and pressing a key) could \"confuse\" new users.\nAnd GTK2 apps keep stealing the focus for no reason, at least while running under Window Maker.\n\nAnd Kopete is really quite good, at least for ICQ, which is what I use. Earlier versions had some quirks, but 0.7.x works very well."
    author: "aoeu"
  - subject: "Re: Just the DE"
    date: 2003-10-11
    body: "yeah!  Gaim (gtk+2 version) keeps stealing the focus everytime I do anything with anything in the System Tray applet, and I'm using KDE!  :o("
    author: "David Walser"
  - subject: "Thanks to all our users for using and voting"
    date: 2003-10-10
    body: "I`d like to thank to all KDE developers for their excellent work :)\nKeep it up!\n\nGreets, Marlo"
    author: "Marlo"
  - subject: "surprise"
    date: 2003-10-10
    body: "This year's awards are real surprise for the KDE users, it sounds like a little decline compared to the previous year when in the same time I hoped for rise in at least one more category (web browser) - instead KMail loses to Evolution (I guess it's because of the delay of kroupware). You see what I mean previous year kde has Best DE and Mail client, now just the DE - and it was definitely possible to be Best DE, Best Mail, Best Browser and Best IM. I hope that with the next release (3.2) and may be for the next year (when kde will be probably 3.3 or 3.4) it will gain back some positions and occupy a new positions (also btw they never mention Best IDE - where only Eclipse could be a fair competition to KDevelop).\nGnome beats this year not as DE but with apps (be careful, this is possible to lead to that Gnome can beat KDE as DE - improve the apps!) they Gnome/GTK have now strong presence with this apps: GAIM, Evolution, GIMP, XMMS as also the OpenOffice.org allies are in.\nThe conclusion from this year is that no releases - means users decline:\nlast minor release was beg this year http://www.kde.org/announcements/announce-3.1.php\nand last major release was before more than 1.5 y and still noone talks for next major release http://www.kde.org/announcements/announce-3.0.php\n\nAnyway, there is some light in the tunnel:\nhttp://dot.kde.org/1061919133/\nhttp://dot.kde.org/1061312927/\nas a way to improve the Office situation with KDE by using the strength of the defacto-standart OO.o and also may be someone could think of a KIMP port of the GIMP and then may be then KDE can became a desktop leader for the cheap linux boxes (having in mind that kroupware, kopete and noatun have already replaced their Gnome/GTK competitions at the same time next year)."
    author: "Anton Velev"
  - subject: "Re: surprise"
    date: 2003-10-10
    body: "> and last major release was before more than 1.5 y and still noone talks for next major release\n\nYou're a number fetishist (\"issue report count is too high!\", \"version numbers increases to slow!\")?\nI can send you patch to rename your \"KDE 3.1\" to \"KDE 4\" if you need it."
    author: "Anonymous"
  - subject: "Re: surprise"
    date: 2003-10-10
    body: "Hi,\n\nthe apps don't matter, developer mindshare does. KDE obviously has more of this than ever before. The fact that KDE can allow itself large time frame between releases to achieve great advances speaks for itself. \n\nBesides, mentioning KDE 3.0 as a major release is laughable. The version number reflected the QT requirement and incompatible changes/cleanups only. The major versions were rather 1, 2.0, 2.2, 3.1 and in future 3.2. I don't yet expect KDE 4 will bring more than 3.2 -> 3.3 except QT4 porting, but we shall see. \n\nYours, Kay\n\n\n\n"
    author: "Debian User"
  - subject: "Re: surprise"
    date: 2003-10-10
    body: "From 2.x to 3.x there were a number of key improvements which is also expected to happen from 3.x to 4.x, not talking about the framework itself but also about the overall feeling and functionality of the apps. Similar applies for 3.0 to 3.1.\nThe current example is that if 3.2 is released this means that a number of improvements will be included, the most valuable is the overall stability, safari, kroupware and kopete which are a key factors to not lose users to gnome and gnome apps. If the user had a konqueror with the quality of the safari would not vote for mozilla, if user had a kroupware ready and stable would not vote for evolution, if the user had a functional kopete would not vote for gaim.\nI am not counting the version numbers but the known (at least right now we all know what comes with 3.2) and unknown average number of improvements that rises the user base and kde popularity for the cheap linux box users."
    author: "Anton Velev"
  - subject: "Re: surprise"
    date: 2003-10-10
    body: "There are so many exiting Gnome Apps that are crappy too. Like Abiword, a nice idea, very fast, just like Word 97, but it lacks function, import/export filter and quality."
    author: "Magna charta"
  - subject: "Re: surprise"
    date: 2003-10-11
    body: "Hi,\n\nAbiWord is a bad example. I used it when KWord was not yet good enough. It is cross platform. GTK is only one of them, so what does make it a Gnome app other than Gnome decision? Like OOo was some time suddenly also a Gnome app with no use of GTK even...\n\nThe only real apps that stand out are Evolution, XChat and Gnumeric. But it's good they do. After all, Kontact, Kopete and KSpread need something non-MS to be better than. At a not too distant future, MS-products may be too expensive to improve further when nobody wants to pay for it.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: surprise"
    date: 2003-10-11
    body: "XChat is cross-plattform too and has no GNOME dependencies in my installed version here. And I fail to see a \"--enable-gnome\" parameter or alike in its \"configure\". Of what consists an optional GNOME dependency, new features?"
    author: "Anonymous"
  - subject: "Re: surprise"
    date: 2003-10-11
    body: "Hi,\n\nindeed my point. The number doesn't matter at all.\n\nI find so far: \n\nnew kwin -> big focus improvement (no more stealing of the focus)\nnew kontact (what you call Kroupware) -> very big desktop improvement, biggest in a long time for me\nnew kopete -> instant messaging that works (not that i use it, but deserves credits)\nnew kcontrol -> usability fixes without end, support for switching resolutions in X, better style support, droped obsolete theming, added a lot of nice things\nnew khtml -> better standards support and speed\n\nI am using it (alpha2, konstruct-ed) for production on a home box and at work. The only regressions I have is that kwin focus management which has bugs to alpha2 still and that Konqueror won't authorize on a customer site with password. (Have to use Mozilla for that site now).\n\nIt feels like a whole new world. It deserves a major number anyway.\n\nAnd of note regarding apps. Some commercial companies went over to use QT instead of KDE. I hope users demand KDE integration at some point in the future. Be that by QT supporting freedesktop.org where necessary or by plugin KDE into QT at runtime...\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: surprise"
    date: 2003-10-14
    body: "Did you file a bugreport for the authentication problem? If not, please do. If so, which one?"
    author: "Waldo Bastian"
  - subject: "KIMP"
    date: 2003-10-11
    body: "Paint programs are the little pain with linux. GIMP was nice but we saw very little improvement through the last few years. The GUI is outdated. I dOn't say I want windows style. I want it to be customized like it is possible with Kdevelop, SDI, mdi, ecc., we an use them if we want.\n\nTuxpaint is far superior to Kpaint :-)\n\nI gues we need an easy Paintshop like KDE Frontend for GIMP. This has high priority for me. I would donate much money if I saw progress with it. "
    author: "Heini"
  - subject: "Re: KIMP"
    date: 2003-10-11
    body: "Have a look at Krita in KOffice.\n\nhttp://koffice.kde.org/krita/\n\nIt's still in the early stages, but I can use it to replace everything I used the GIMP for. Plus, its KDE intergrated and Python Scripting can be used (none of this perl crap ^_^)."
    author: "Chris Smith"
  - subject: "Re: KIMP"
    date: 2003-10-11
    body: "I geuss you were usign the GIMp for the wrong purpose, these programs have completely different functions.\n\nI also don't think we should waste resources on an always outdated KDE front end to the GIMP."
    author: "Alex"
  - subject: "Re: KIMP"
    date: 2003-10-11
    body: "The current GIMP versions which will lead to GIMP 2.0 scheduled for this year have many new features and improved GUI. And GIMP is still a GTK only application."
    author: "Anonymous"
  - subject: "few surprises, but props to the KDE team"
    date: 2003-10-10
    body: "It's very easy to agree with those results and shouldn't come as a surprise that GNOME apps beat KDE ones hands down.\n\nKopete is still a joke. Evo is slower than kmail but so so much more useful in an enterprise environment. Moz may well be bloated but renders many many more pages better. It also handles tabs better, cookies MUCH better, blocks popups sensibly and I say all this as someone who uses konqueror anyway. \n\nRoll on the 3.2 apps, but they've got some catching up to do. (and plaudits for best DE. KDE is very intuitive, fast and lovely)"
    author: "anon"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-10
    body: "Mozilla is not a Gnome application..."
    author: "Murphy"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "Evolution is not a GNOME application..."
    author: "Alex"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "Wrong and right: It isn't released as part of GNOME releases but uses GNOME libraries."
    author: "Anonymous"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "Quite a few people think, if a program uses GTK, it is part of GNOME."
    author: "Albert Liu"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "many wrongs don't make a right\n\n"
    author: "Alex"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "And this happens while there are examples of programs who decided to stay GTK-only (e.g. Gimp), switched back from GNOME to GTK only (like Pan) or even start to offer KDE file and print dialogs use as an option (Sodipodi)."
    author: "Anonymous"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "/me wonders if we are going to start seeing major GTK/GNOME apps jump over the fence and join the Qt/KDE camp even if they continue to code in straight C."
    author: "anonymous"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "No need to jump over the fence. Using KDE's print and filedialog would be a wonderful improvement for a non-QT/KDE app like e.g. the gimp..."
    author: "Thomas"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "On the other hand, many gnome-libs related functionality is being ported over to gtk. With gtk 2.4 and gnome 2.6, it'll be elimiated all together. gtk 2.4 will also have some optional dependencies on gnome related libraries. For example, the new file selector will use gnome-vfs if it's installed. Basically, in the future, it'll be gnome apps==gtk apps."
    author: "fault"
  - subject: "Re: few surprises, but props to the KDE team"
    date: 2003-10-11
    body: "> Basically, in the future, it'll be gnome apps==gtk apps.\n\nI don't think that it will go so far, think of GConf, Orbit and Bonobo."
    author: "Anonymous"
  - subject: "oh yess!"
    date: 2003-10-11
    body: "My Favorite Linux Game: Return To Castle Wolfenstein - Enemy Territory\n\ndisarm the dynamite!!"
    author: "ac"
---
<a href="http://www.linuxjournal.com/">Linux Journal</a> announced today the winners of the <a href="http://pr.linuxjournal.com/article.php?sid=785">Ninth Annual Readers' Choice Awards</a>. Although no real surprise, KDE continued its <a href="http://www.kde.org/awards/">strong series since 1999</a> and won once again in the category "Favorite Desktop Environment". Thanks to all our users for using and voting for KDE.
<!--break-->
