---
title: "KDE Traffic #68 is Out"
date:    2003-11-17
authors:
  - "hpinto"
slug:    kde-traffic-68-out
comments:
  - subject: "thank you"
    date: 2003-11-17
    body: "I really enjoyed this issue.  It was quite entertaining to read about all the KDE developments and the friendly banter that goes back and forth between the developers.  Thanks for keeping KDE Traffic alive!"
    author: "anon"
  - subject: "Suse's Response"
    date: 2003-11-17
    body: "I wonder how he is going to reconcile it based on Nat's Slashdot posting which all but says that they will try to kill KDE. Which of company is going to win? Ximian or Suse?"
    author: "a.c."
  - subject: "Re: Suse's Response"
    date: 2003-11-17
    body: "I think its too early to tell... Remember Novell is the same company that brought us WordPerfect Office, and Unixware...  I think we need to wait 2 years to see the body count.  Then we can speculate their strategy.\n\nIts not over until the check is in the bank man.\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Suse's Response"
    date: 2003-11-17
    body: "http://dot.kde.org/1069090495\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Suse's Response"
    date: 2003-11-17
    body: "huuuummmm. I must be plain stupid.\nWhat does Rekall (an app) going GPL have to do with the possibility of Novell (Ximian) trying to kill off KDE?"
    author: "a.c."
  - subject: "Re: Suse's Response"
    date: 2003-11-17
    body: "Wrong thread?"
    author: "Thorsten Schnebeck"
  - subject: "Re: Suse's Response"
    date: 2003-11-18
    body: "Yes, your link is wrong."
    author: "CE"
  - subject: "Re: Suse's Response"
    date: 2003-11-18
    body: "Oh, I should better use ctrl+c than MMB :o)\nNext try:\nhttp://dot.kde.org/1068570005/1068982411/"
    author: "Thorsten Schnebeck"
  - subject: "Re: Suse's Response"
    date: 2003-11-21
    body: "The problem is that the IBM presentation was removed as \n\"is has not been approved for publication\"...\n\nUnderstand it at your will ;)"
    author: "Michel Nolard"
  - subject: "Re: Suse's Response"
    date: 2003-11-17
    body: "Do you have a link to the Slashdot posting?"
    author: "AC"
  - subject: "Re: Suse's Response"
    date: 2003-11-18
    body: "http://slashdot.org/comments.pl?sid=84723&cid=7393279\n\n\nKinda of nasty, if you ask me. I feel like I am listening to another Bill Gates speech from 1991 when he says that Windows will be secured, easy to install, low cost, and will play nice with all."
    author: "a.c."
  - subject: "Re: Suse's Response"
    date: 2003-11-18
    body: "Damn, SuSE has been doing such a great job with KDE.\nI really don't like the sound of \"we will still support kde\".\n\nAnyway I'll wait and see, then decide wether to stick with it or not.\n"
    author: "Src"
  - subject: "Re: Suse's Response"
    date: 2003-11-18
    body: "Common guys... It's not _that_ surprising to see the founder of Ximian making those kind of posts. Gnome is what he live on. And KDE is his competitor."
    author: "OI"
  - subject: "Re: Suse's Response"
    date: 2003-11-19
    body: "I don't see this as either Ximian or SuSE winning.  Clearly, what will happen is that they will be integrated.  This integration could be either good or bad for KDE.\n\nSuSE could become a GNOME/Ximian distro that also offers the stock KDE.  This would not be good because they would no longer be promoting KDE -- just make the RPMs of whatever we release and put it in the distro.  OTOH, I guess that we wish that that was what RedHat did. :-D\n\nOr, they could integrate some of the Ximian stuff into KDE (Evolution and OO).\n\nBut, we shouldn't wait around to see what happens.  With what is happening, I think that we need to take steps to promote KDE.  This probably means that an RPM based distro is needed.  This should be a basically stock distro -- all enhancements to Linux should be KDE based in such a distro.  \n\nThis should be quite easy to do since the RedHat installer is freely available and if we want a stock Linux distro all that is needed is a packaged distro based mostly on Linux From Scratch and/or Fedora RPMs.\n\nI emphasize that KDE needs its own distro because KDE includes the enhancements which various distros have developed.  So, KDE will always be in conflict with distros that have their own (possibly proprietary) enhancements to Linux because KDE also includes these.\n\nCommercial support would help -- would the Kompany be interested?  But, we could probably just have the ISO on line and get one of the CD sellers to distribute it.\n\nWe also need to address larger software issues rather than just make better widgets.\n\n1.  Will we soon have something that is interoperable with Evolution and M$?\n\n2.  Have we considered applying for a Flash license and directly incorporating Flash support into Konqueror (with a KPart) rather than using a plug-in?\n\nhttp://www.macromedia.com/software/flashplayer/licensing/sourcecode/\n\n3.  Is KDE doing anything with the Helix plug-in project?\n\nhttps://www.helixcommunity.org/\n\n4.  Will Karbon14 be able to import/export Flash files?\n\nhttp://www.macromedia.com/software/flash/open/licensing/fileformat/\n\nThere is already some OSS code base for this.  Search on: \"SWF\" at SourceForge.\n\n5.  GNOME integration is needed.  But this can be one way.  If you install a GNOME application in a KDE based Linux system, it should be added to the menu, icon and MIME system without any human intervention.  If this isn't currently possible then something like: KAppFinder (a wizard) is needed.\n\n6.  Our native solutions need to be able to play streaming Audio/Video from web sites.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: Suse's Response"
    date: 2003-11-19
    body: "1) Kontact? Yeah, I know it is not ready yet, but it will soon have this feature.\n\n5) Huh? Isn't it like this already? I have an entry for file-roller in my menu, and I'm sure I haven't added it manually (note to self: why do I have file-roller installed?)"
    author: "Henrique Pinto"
  - subject: "Re: Suse's Response"
    date: 2003-11-19
    body: "> 5) Huh? Isn't it like this already? I have an entry for file-roller in my menu, \n> and I'm sure I haven't added it manually (note to self: why do I have \n> file-roller installed?)\n\nI really don't know how much progress has been made.  I have three GNOME applications installed and they put the 'Desktop' file in two places:\n\n{prefix}/share/gnome/apps/Applications\n{prefix}/share/applications/gedit.desktop\n{prefix}/share/applications//gnumeric.desktop\n\nThe first one is compatible -- a link does it.\n\nAnd I assume that 2 & 3 are for the VFS -- they contain: \"Categories\".  I don't know how that works in KDE yet because the menu in KDE-3.2Beta1 is a mess -- half of the stuff is in: \"Unknown\".\n\nThe icon issue seems to be solved.  KDE now looks in: {prefix}/share/pixmap, so a link is all that is needed if you use a different prefix.\n\nAnd, I don't know how GNOME handles MIME types.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Suse's Response"
    date: 2003-11-19
    body: "Yes I tried the KDE-3.2Beta1 and the VFS Menu works.  A link and KDE finds the GNOME icons.\n\nHowever, there was no file association for Gnumeric.  There is a MIME type (the one that comes with KAppFinder) but no association.\n\nHOWEVER!  That \"vanity\" background in KFM is very very unusable.\n\nPerhaps it only gives you a sever headache in 2 minutes if you have astigmatism (I do :-)).\n\nI guess that this is just part of this syndrome that developers don't seem to consider that their favorite eye candy is detrimental to usability.\n\nAnd there is yet another icon on the Konqueror toolbar: FSViewPart.  Was this put there because the authors of the application thought it was a real cool application? or did some one consider usability?\n\n--\nJRT\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Suse's Response"
    date: 2003-11-19
    body: "> Was this put there because the authors of the application thought it was a real cool application?\n\nDid you consider that any kpart for Konqueror appears there as reason?\n "
    author: "Anonymous"
  - subject: "Re: Suse's Response"
    date: 2003-11-20
    body: "> Did you consider that any kpart for Konqueror appears there as reason?\n\nNo, I didn't.  What I considered was that only commonly used icons should be there.  There is a limited amount of space and you have to make objective design decisions.\n\n--\nJRT\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Suse's Response"
    date: 2003-11-19
    body: "0. KDE Distribution --> Knoppix\n1. Evolution/M$ --> Kontakt/Kolab\n5. GNOME Apps --> XDG menus (mime system is in the making AFAIK)\n6. Audio/Video --> Doesn't Kaffeine or KPlayer support this?"
    author: "cloose"
  - subject: "Re: Suse's Response"
    date: 2003-11-20
    body: ">0. KDE Distribution --> Knoppix\n\nThis bootable CD distro is too specialized to be considered a KDE distro.  We need something to replace RedHat.\n\n1. Evolution/M$ --> Kontakt/Kolab\n\nYes as soon as we ship 1.0 this should do it.\n\n>5. GNOME Apps --> XDG menus (mime system is in the making AFAIK)\n\nYes the menues work in KDE-3.2 -- well at least as well as the KDE ones.  But, AND is AND and the MIME types and file associations need to work as well.\n\n> 6. Audio/Video --> Doesn't Kaffeine or KPlayer support this?\n\nIt is the: \"Embeded Media Player\" that needs a little work.  I can download and play a QuickTime movie trailer with Kaboodle and the Xine library (x86 only as you still need the actual Windows DLLs).  Three cheers for the Xine people.  However, they won't play in the \"Embeded Media Player\".  The plugin (or KPart) does load but nothing happens.  Just a small detail that needs to be finished.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Suse's Response"
    date: 2003-11-20
    body: ">>>It is the: \"Embeded Media Player\" that needs a little work. I can download and play a QuickTime movie trailer with Kaboodle and the Xine library (x86 only as you still need the actual Windows DLLs). Three cheers for the Xine people. However, they won't play in the \"Embeded Media Player\". The plugin (or KPart) does load but nothing happens. Just a small detail that needs to be finished.<<<\n\nkmplayer works reasonably well embedded in konqueror, and the mplayer project apparently have managed to decode QuickTime Sorenson3 natively (no windows dll needed).\n \n"
    author: "anonon"
  - subject: "Re: Suse's Response"
    date: 2003-11-20
    body: "There was some reason that I couldn't install MPlayer.  Video card support perhaps?\n\nActually Xine does Sorenson quite well.  It is the *sound* that requires the DLLs.  Go figure!\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Suse's Response"
    date: 2003-11-20
    body: "> Yes the menues work in KDE-3.2 -- well at least as well as the KDE ones. But, AND is AND and the MIME types and file associations need to work as well.\n\nIt was too late for KDE 3.2 to support the fdo MIME spec, but 3.3 should."
    author: "anon"
  - subject: "Re: Suse's Response"
    date: 2006-07-16
    body: "Hey....What's up with Suse media player...why doesn't it play the video from CNN.com? It requires windows media player. Aren't there any substitutes?\n\nIn fact, none of my video works properly!\n\nCan someone help me?\n\n"
    author: "Robert DeSutter"
  - subject: "Re: Suse's Response"
    date: 2003-11-20
    body: "Suse has a market that requests KDE. They cannot sell Gnome. the customer counts and Suse is the bigger player than ximian. I would rather ask for a better kde support of ximian."
    author: "gerd"
  - subject: "Toolbar situation"
    date: 2003-11-17
    body: "Its nice to see some attention being paid to the toolbar situation. IMHO, its one of the principle reasons GNOME apps still look more polished than KDE apps. First the context menus, now this. Its just great.\n\nOT: I'm a little depressed about the whole Novell thing. Maybe I'm just paranoid, but I don't want KDE to become another example of superior technology being killed off by business interests. "
    author: "Rayiner H."
  - subject: "Re: Toolbar situation"
    date: 2003-11-17
    body: "I agree, it can seem a bit disconcerting.  To paraphrase a few others re similiar situations;  as long as there is one developer willing to work on KDE, it cannot be killed.  \n\nRemember, KDE and Gnome both were started by the community, not by any corporation.  "
    author: "Paul"
  - subject: "Re: Toolbar situation"
    date: 2003-11-18
    body: "And in KDE's case it still is a community project."
    author: "cloose"
  - subject: "Re: Toolbar situation"
    date: 2003-11-18
    body: "Aren't they both?"
    author: "Paul"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "Well, apparently Gnome got a community of commercial players so I guess you are right. ;o)"
    author: "Datschge"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "That might be true.\n\nIMHO SuSE's influence on KDE isn't as big as same people think. The number of KDE developers working for SuSE is really tiny compared to the total amount of KDE developers. So if SuSE really decides to pull their(!) plug on KDE I don't think that you would notice it much (development speed wise). \n\nExcept for all the thousands of SuSE customers complaining. :-)\n"
    author: "anonymous"
  - subject: "Re: Toolbar situation"
    date: 2003-11-17
    body: "Using a current CVS version of KDE I like the new composer toolbar.\nBut I still see the crypto-module selector and as a user of a permanent internet connection there is my standard change: I replace \"save into outbox\" with \"save as draft\".\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "Re: Toolbar situation"
    date: 2003-11-17
    body: "Both KDE and GNOME are in their infancy in terms of usage. Only time to tell if one will prevail over the other, or if something else will replace both of them (most likely). Neither one is going to disappear within five years though, probably."
    author: "anon"
  - subject: "True, HOWEVER"
    date: 2003-11-18
    body: "Small babysteps that are towards GNOME, and alrger leaps, such as 1 million new chinese desktops powered by GNOME will eventually lead to a foothold in the Linux Desktop as strong as Microsoft's in the OS market.\n\nThese small steps are what count the most, exactly because both projects are in their infancy. If KDE had 500 million desktops and GNOME had 550 million, it would hardly make a big differnece, however now it does. \n\nKDE is partly to blame, KD Eis not aggressive about getting corporate support, donations etc. They expect companies will flock to them, not vice versa and when you are a pretty new project, it won't happen that way.\n\nGNOME has always been very encouraging of corporate participation and donations, much more than KDE, they have been much more assertive on these issues and it shows. GNOME has been working hard to become busienss ready and also governement ready by making their desktop much more usable and ACCESSIBLE. \n\nTheir license DEFINITELY helps, it is a large part of why they are being supported by more companies than KDE is. KDE really needs to identify with a large company, to build name recognition and trust. "
    author: "Mike"
  - subject: "Re: True, HOWEVER"
    date: 2003-11-18
    body: "... Red Flag Linux uses KDE because of the better internationalisation and management facilities:\n\nhttp://www.redflag-linux.com/source/Documents/redflag/Desktop32e.pdf\n\nThe same is true in Taiwan, Korea, Singapore, India and most other contries with developing open source policies.\n\nCommercial support is nice, but in the long term, Linux tends to see the better product winning out, which admittedly is surprising in such a capitalist culture, but there it is.\n\nHonestly, I expect SuSE to keep on using KDE as their primary desktop environment, despite the Novell acquisition. Moreover, SuSE are finally becoming a challenger to RH in north america, where there was no one else before. That is a good thing. With Novell's corporate tools and HP and Gateway's distribution with new machines, the future is looking very bright for SuSE and the KDE. Add onto that the fact that RH has just abdicated the desktop market, and that Sun are still making a hash of their Gnome-based desktop and I am not so worried about corporate-driven Gnome dominance anymore.\n\nKDE 3.2 should be more than ample for most desktops, achieving parity with Windows and OSX in terms of functionality, intuitiveness and ease of use, which is a great thing. Add to that the fact that Konqy is now very good, and that with Safari, we really do have a good alternative to IE now with a large installed base, and I am hopeful for open standards and the future of the web, too, especially with MS now not shipping IE 7 until 2006, with a majority of Windows users only having access to it by 2009.\n\nSufficed to say, I'm excited and eager to see what happens when we have Kernel 2.6 and 3.0, with Reiser4 and other additions. I'm also eager to see what happens for QT4 and KDE 4, where we get a chance to really push the boat out and add features beyond achieving parity with existing offerings! If all MS have for Longhorn are pretty window effects, WinFS meta-data searching, DRM and \"better\" security, then we stand on the threshold of an incredible change in the computing industry. We have 6 years, effectively, to develop new and compelling features. 6 years to beat Microsoft's crappy new features and establish new ways of thinking, working and using computers!\n\nAnd I'll stop now, because I really am excited and impatient to see what the future holds."
    author: "Dawnrider"
  - subject: "Not True, ya ijit"
    date: 2003-11-19
    body: "God, are you full of it:\n\n\"KDE is partly to blame, KD Eis not aggressive about getting corporate support, donations etc. They expect companies will flock to them, not vice versa and when you are a pretty new project, it won't happen that way.\n\nGNOME has always been very encouraging of corporate participation and donations, much more than KDE, they have been much more assertive on these issues and it shows. GNOME has been working hard to become busienss ready and also governement ready by making their desktop much more usable and ACCESSIBLE.\"\n\nHow does corporate partnerships have ANYTHING to do with usability? And who said a word about \"flocking\"? Please, learn the language of logic before you try to partake in an adult conversation. \n\nThank you."
    author: "Joe"
  - subject: "You first"
    date: 2003-11-20
    body: "\"How does corporate partnerships have ANYTHING to do with usability? \"\n\nUsability is very important for businesses because it = increased productivity. The business doesen't care if you can customize your panel and look and feel well, as long as the unit runs the applications they need that is all they care about. Everything else is convenience, and often slows down productiviyu. Not because the users can't ignore the extra features, but because they like them, are curious about them, and will spend time fiddling with them sometimes. \n\nAnyway, that isn't really usability, reducing options is not always a good thing to do to increase usability, because after a time you have a very usable product that isn't very useful. Usabiltiy means having features properly organized, with intuitive use and options that are useful.\n\n\"And who said a word about \"flocking\"? Please, learn the language of logic before you try to partake in an adult conversation.\"\n\nSmartass its an opinion. From what I've seen GNOME has been more receptive to business requests, perhaps too receptive. KDE seems to expect that companies will come to them instead of KDE proposing a KDE solution to the company, sure its free but you still need marketing just like any other product.\n\nMy first post was aimed at responding to this\n\n\"Both KDE and GNOME are in their infancy in terms of usage. Only time to tell if one will prevail over the other, or if something else will replace both of them (most likely). Neither one is going to disappear within five years though, probably.\"\n\nand the importance of early adoption. If you haad read my post you would know what it was about.  \n\n\"flock\" was used properly in the sentence and I was just emphasizing tha KDE is partly to blame for this and there are ways in which we could speed up KDE adoption and one way is to change the attitude I think many KDE people have. \n\n\"flock\" simply means to quickly come to in large numbers, I don't know what you don't understand about this simple and commonly used word. It doesen't matter who used it, what matters is the whole sentence and topic not a single word. We don't all have the same vocabulary and we will use different words to illustrate different things. Obviously we are not drones and it doesen't matter if the word was used by someone else or not.\n\n\"Please, learn the language of logic before you try to partake in an adult conversation.\"\n\nThere is nothing illogical about using the word \"flock\" and if you want to counter something counter my ideas not my spelling or grammar. This is only the comments section, nothing to get worked up about, I couldn't care less about proper spelling and grammar here. Besides that, what you have said about \"flock\" about my illogical language use is not even correct. You can't just quote a word and expect people will know the entire idea of what you are saying.\n\nFurthermore, how would you even know who here is an adult or not or who here is even a native english speaker. Think, I would like you to write proper Romanian.\n\nAnd again, who really cares if I don't capitalize \"I\" would you correct me on that too? Who cares, the ideas are important not the presentation, not here. And again please explain how I used \"flock\" in the wrong way.\n"
    author: "Mike"
  - subject: "Re: You first"
    date: 2003-11-20
    body: "\"Who cares, the ideas are important not the presentation, not here.\"\n\nUmmm. pardon me for saying, but wasn't this whole conversation ABOUT usability and presentation? Your comments bored me to the point that I skimmed, but it's amusing all the same. I can tell you love listening to yourself (even with all the errors in logic in grammar) and I wouldn't want to get in the way of the echo."
    author: "Joe Schmoe"
  - subject: "Re: You first"
    date: 2003-11-22
    body: "My comment was all about presentation and usability, I mentioned that it is one of the areas where GNOME is ahead, and the comment described how I think KDE presents itself to businesses. \n\nHow about actually mentioning some of those things and maybe you should learn some respect."
    author: "Mike"
  - subject: "Re: You first"
    date: 2003-11-22
    body: "Hmm.. responding to flamebait-ish comments with equivalent verbal patter isn't the smartest thing to do. But anyways, \n\nusability is important for everyone. It isn't tied to businesses, it isn't tied to home users. It isn't tied to genius. It isn't tied to the mentally retarded. It benefits everyone.\n\nKDE is a open source project that from the start, has been focused on providing use of use to the UNIX desktop. Usability has thus been a quite important goal in the minds of everyone involved with the KDE project. We aren't there yet in a prefectly usable solution, but we are slowly getting there. "
    author: "fault"
  - subject: "Re: Toolbar situation"
    date: 2003-11-18
    body: "It's not hard to adjust Konqueror's toolbars to look good (KDE3.1+):\nhttp://www.kde-look.org/content/show.php?content=8999"
    author: "Asdex"
  - subject: "Re: Toolbar situation"
    date: 2003-11-18
    body: "Mine too!"
    author: "Jeff"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "Mine three! \n\nhttp://www.prism.gatech.edu/~gtg990h/kde_32_screenshots/Konqueror - Slashdot.png\n\nBut it would be nice if they came less cluttered out of box :)\n"
    author: "Rayiner H."
  - subject: "Re: Toolbar situation"
    date: 2003-11-20
    body: "A step truly in the right direction."
    author: "OI"
  - subject: "Re: Toolbar situation"
    date: 2003-11-20
    body: "I've found that the Up button on the left like that confuses new users - for web browsing it's all but useless anyway. In fact, the default layout of toolbars in many KDE apps seems a bit odd - KMail is a good example of odd priorities. I know it only takes a few minutes to get things how I like it, but for many users the defaults are all they see.\n\nMy layout, FWIW:\n\nhttp://benroe.com/images/konq.png\n"
    author: "Ben Roe"
  - subject: "Re: Toolbar situation"
    date: 2003-11-20
    body: "Very nice.  I'm glad you kept the Up button because I find that to be a very important feature.  Just as I find the Zoom buttons to be important."
    author: "anonymous"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "I must have missed something about the toolbars.\n\nWith two clicks, I know have 32x32 icons with text under them.\n\nSo, I don't understand how this is a new feature that needs to be added.\n\nWhat I do agree with is the size of the text labels.  There is no way to edit them and no way to use a font like: NimbusSanL-ReguCond (I mention that because it is a free font but Arial Narrow or Helvetica Narrow is what I really mean).\n\nSo, we need two new features:\n\nAbility to edit the text labels for toolbar icons.\n\nAbility to choose the font for the toolbar icons.\n\nNote: if you have too many icons, you can remove them.  Which icons the tool bars contain is fully configurable.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "> I must have missed something about the toolbars.\n\nProbably. Most of what's being discussed is already possible, the discussion is regarding defaults for the toolbars.\n\nThat means:\n    1) Default Buttons\n    2) Default Icon Size\n    3) Show text or not as default\n    etc.\n"
    author: "Henrique Pinto"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "I don't see changing the defaults as a major improvement.\n\nI have Konqueror set up quite differently than the default -- more like Mozilla -- but I didn't think that I should suggest that the defaults should be changed. \n\nPerhaps I should/they should be.\n\nhttp://home.earthlink.net/~tyrerj/files/konq.png\n\nNote that I prefer the small icons.  After a while, you learn what they mean (and where they are).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "James,\n\nWhy do you think that you *need* to have an opinion here? We weren't all waiting around, hoping that you had your answer soon, or else the conversation couldn't go anywhere. \"Oh boy, I hope James tells us what he thinks soon, this coffee is getting cold.\"\n\nDefaults matter. Defaults are used by people who don't know what \"Defaults\" are, and probably think someone is referring to \"the problems\".\n\nFor me, and most \"average\" people I know (I am not including myself in there, but I know the type), your toolbars are confusing and look like dog poop. There are no labels, so people come down to saying \"Which blob with the blue arrow sends a reply back to grandma?\"\n\nCome on, give me a break. Ditch the lame ass \"Print this frame\" buttons, and ditch your (James') tiny icon schemes (yes, James, we know you're super-elite and know what the buttons mean), but you highlight the very fact that you probably shouldn't be in this conversation. If you know how to change them, then do. If you don't, then you are going to be looking for the best way to know your ass from your elbow. Do you really think 20 different icons are really the best way to confront a new user?\n\nGive me a break.\n\nJoe\n\nTo sum up, ditch everything, go through a trial run with a normal user to see what buttons she needs, then make it REALLY clear how to add new buttons. Keep it clean and \"usable\"."
    author: "Joe Schmoe"
  - subject: "Re: Toolbar situation"
    date: 2003-11-20
    body: "PLEASE! give us all a break.  If you have nothing to contribute, then don't go looking for things to misunderstand so that you can write a useless diatribe denigrating them.\n\nYour comments on my arrangement are irrelevant because they do not address my point.\n\nYou said: \"There are no labels\", but that is the current KDE default.  So, please go insult who ever was responsible for that decision.\n\nMy point was to rearrange the icons so that the commonly used ones were more accessable.  I did not add or remove any icons except for the word: 'Location'.  That is a separate issue.\n\nIf you have an intelligent comment on that, please make it.\n\nNow, there is a legitimate issue buried between the lines in your rant.\n\nIt is the question of novice vs. expert users.  There was a paper on this in Science which I think applies to the GUI of a computer.  There *is* a difference of kind, not degree, between novices and experts.\n\nNovices do need some hand holding to start with.  But, there is a disadvantage to a GUI that has too much hand holding -- this has been a legitimate criticism of the Mac (and the mouse in general) -- that it prevents users from ever taking the step to being expert users.  The result of this is low productivity.\n\nI have no objection to the default being to have \"Text under icons\" on what is the: \"Location\" tool bar on my schema.  But, there are currently two problems with this.  They need to be single words and they need to use a smaller type size (perferably condensed).  Otherwise they take up too much screen space.\n\nPerhaps a better solution would be to move some of the icons from the: \"Main toolbar\" to the \"Extra toolbar\" so it looks like IE 5.5 if you don't have the: \"Extra toolbar\" (which should be the default).\n\n--\nJRT\n\n\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "I *do* think that changing the defaults would be a major improvement. So far, four people in this thread have posted screenshots to how they've modified their defaults for Konq. Clearly, the default layout is not exactly pleasing for a lot of people. Also, it would be nice for the icon-text mode to be usable. Having the text there not only helps new users who don't who what the icons stand for, but also helps non-visual users (like me :) who can never remember that they stand for. True, you can fix all this now, because of KDE's configurable toolbars, but doing that manually to each app is just a pain."
    author: "Rayiner H."
  - subject: "Re: Toolbar situation"
    date: 2003-11-21
    body: "I noticed some similarity in the four posted suggestions.  I admit to copying Mozilla.  I suppose that there are also those that would like to copy IE.\n\nIn any case, I am posting a fifth idea.  Note that this is NOT possible with the current code.  It requires adding an additional tool bar and permanantly attaching the: \"Animated Logo\" to the: \"Menubar\" which shouldn't be major changes.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "> Ability to choose the font for the toolbar icons.\n\nThat's already there for some time, look no further than\nControl Center -> Appearance & Themes -> Fonts -> Toolbar"
    author: "Datschge"
  - subject: "Re: Toolbar situation"
    date: 2003-11-19
    body: "I probably could have gone into more detail about this.\n\nYes, you can set it globally, but this doesn't quite solve the problem with Konqueror.  You need to be able to set it individually for each toolbar.  Specifically, if you choose a small font for under the icons in the main toolbar, it also changes your Bookmark tool bar where you want a larger font.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "What is the bug report #?"
    date: 2003-11-20
    body: "That you've filed on this issue. I'll give it my vote if you keep the description terse. Can't have you rambling on forever."
    author: "Joe Schmoe"
  - subject: "Resetting toolbars to default"
    date: 2003-11-19
    body: "Without having tried KDE 3.2 beta, do the toolbars in 3.2 support resetting the toolbar layout to the default? This would be useful if you've messed up the layout and want to start from scratch."
    author: "Paul Eggleton"
  - subject: "Re: Resetting toolbars to default"
    date: 2003-11-19
    body: "Usually renaming ~/.kde/share/apps/<appname>/<appnameorsomething>.rc does that for me."
    author: "Datschge"
  - subject: "Re: Resetting toolbars to default"
    date: 2003-11-19
    body: "Yeah, that'll work, but it isn't that great from a usability perspective."
    author: "Paul Eggleton"
  - subject: "Re: Resetting toolbars to default"
    date: 2003-11-19
    body: "I thought you were asking whether the technical possibility is there so I answered that way. If your point was that the 'configure toolbars' dialogs should have a 'defaults' button like most other setting dialogs seem to have I even fully agree, but you should also clearly state your point that way then. =P"
    author: "Datschge"
  - subject: "Word xml schema"
    date: 2003-11-20
    body: "This might help the KOffice guys with ms office compatibility.\n\nhttp://isb.oio.dk/info/news/wordml+published.htm"
    author: "OI"
  - subject: "Re: Word xml schema"
    date: 2003-11-20
    body: "Post it to http://www.1dok.org"
    author: "gerd"
  - subject: "Kmail"
    date: 2003-11-20
    body: "KMAIL:\n\n- find message is important\n- a forward button is imho needed"
    author: "gerd"
  - subject: "Re: Kmail"
    date: 2003-11-22
    body: "Do you mean \"kmail's find message dialog is important\" or \"kmail needs ot be able to search for important messages\"? The later will be possible in kde 3.2. And there is a forward button. Even two, one for attachment, one for inline. All you have to do is add it to the toolbar."
    author: "Till Adam"
  - subject: "comment on KDE support"
    date: 2003-11-23
    body: "To quote from the SUSE post:\n\"Today and tomorrow, we must be strongly committed to deliver what our customers ask us for.\"\n\nExactly. And this is why I have sent them an e-mail with regard to their KDE support since I paid them quite a bit of money to have my group switch to SUSE Linux desktop (over Red Hat as the system administrator wanted). Our decision was mainly influenced by SUSE's KDE integration which is (at least in my opinion) leaps and bounds above anything Ximian Gnome has to offer. I don't mind getting the Ximian improvements to Gnome included in SUSE, but if NOVELL/SUSE decides to pour only resources into GNOME, then I may just as well use Red Hat in the future.\n\nAnd since we are just discussing implementing a Linux cluster with 64 dual-processor machines, they may loose out on that contract as well if they decide to retain KDE as their main environment.\n\nI just hope that other customers will also write to them."
    author: "anon"
  - subject: "Re: comment on KDE support"
    date: 2003-11-23
    body: "The second to last paragrpah should have read:\n\nAnd since we are just discussing implementing a Linux cluster with 64 dual-processor machines, they may loose out on that contract as well if they decide to NOT retain KDE as their main environment.\n \n"
    author: "anon"
---
<A HREF="http://kt.zork.net/kde/kde20031116_68.html">KDE Traffic #68 has been released</a>, covering topics ranging from <a href="http://kde.ground.cz/tiki-index.php?page=Toolbar+Review">toolbars</a> (<a href="http://tiki.urbanlizard.com/kmail_toolbar.png">1</a>, <a href="http://tiki.urbanlizard.com/kmail_composer_toolbar.png">2</a>), <a href="http://kmail.kde.org/">KMail</a> and <a href="http://kontact.kde.org/">Kontact</a> fun,  to a change in the <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">KDE 3.2 release schedule</a>.  Enjoy!
<!--break-->
