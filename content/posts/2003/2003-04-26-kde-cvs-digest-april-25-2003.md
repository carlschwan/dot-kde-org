---
title: "KDE-CVS-Digest for April 25, 2003"
date:    2003-04-26
authors:
  - "dkite"
slug:    kde-cvs-digest-april-25-2003
comments:
  - subject: "Thanks Derek !"
    date: 2003-04-26
    body: "First post :)\n\nYesss :)"
    author: "Martin"
  - subject: "The first to say: Thanks Derek !"
    date: 2003-04-26
    body: "Ohhooohoooo\n\nTo be or not to be ... the firs post.\n\n\nOOhohohohhooo no !!!\n\n\nThanks Derek ! No the first, Oh no !!"
    author: "The_one_who_can't_believe_this"
  - subject: "Login to Hotmail?"
    date: 2003-04-26
    body: "Because of having read something related to Hotmail in the digest: Is anyone using Konqueror able to login to Hotmail? I'm taken back to the login page after a few redirects (using cvs version)."
    author: "Steffen"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: "It works for me, but I have changed the browser ID for msn.com and passport.com to \"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)\" i.e. Internet Explorer 6.0 on Windows XP."
    author: "uga"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: "I can login with Konqueror 3.1.1 every week or so to empty out the spam.  The problem is Hotmail sets quite a few cookies.  It sounds like you are blocking some of the passport cookies and MS will not let you go on until you let the cookies through."
    author: "pben"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: "Do not use M$ - Hotmail  !!"
    author: "no M$"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: "Some of us need an account there for several reasons. For example:\n\n1) Use the address to register into sites that you don't trust (i.e. an SPAM e-mail client :)\n2) To continue using msn messenger with kopete.\n3) To have an account that never fills up (configure it to delete all e-mail automatically, just by allowing only the e-mails in an empty addressbook ;-)  useful if someone is annoying you...\n"
    author: "uga"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: "Very good reasons. Worth noting though...\n\n1. There are sites that let you create email addresses for this very purpose. Another option is to setup your own domain. For instance I register with sites by using the site name at my domain (ie amazon.com@mydomain.com) and forward all but my private addresses to spam@mydomain.com so I don't have to wade through a bunch of spam to get to my real messages. Also, that way I can tell if I'm getting spam from someone who shouldn't be sending me spam. \n\n2. I used msnmessenger@mydomain.com to register my messenger account, so I don't think you need a hotmail account to do this.\n"
    author: "me"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: ">I used msnmessenger@mydomain.com to register my messenger account\n\nI thought only hotmail accounts were allowed for the messenger... I'll have a look at it."
    author: "uga"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-29
    body: "think again."
    author: "caoilte"
  - subject: "Re: Login to Hotmail?"
    date: 2003-06-25
    body: "verygood"
    author: "leitao"
  - subject: "Re: Login to Hotmail?"
    date: 2008-07-20
    body: "i cant check my email why can you fix it "
    author: "raymond mcguckin"
  - subject: "Re: Login to Hotmail?"
    date: 2008-07-20
    body: "You are replying to a five year old thread and hope to get an answer? I found you only by chance.\n\nPlease, you do not seem to be a native English speaker. Refer to a KDE group near you, or a mailing list or forums in your language. At this place, nobody will hear/understand you."
    author: "Stefan Majewsky"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: "Once you have registered your hotmail account for msn messenger you don't need to check the mail.  Even when the email account has been stopped messenger continues to work"
    author: "ld6772"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-28
    body: "Heh.. that's a stupid reason not to make Konqueror not work with Hotmail.\n\nMost of us are not so fanatically anti-Microsoft. Microsoft has done a lot of bad things, but good things as well. "
    author: "fault"
  - subject: "Re: Login to Hotmail?"
    date: 2003-04-26
    body: "I have had a similar problem with Netscape's webmail.  The bug # is 51530."
    author: "Greg"
  - subject: "Re: Login to Hotmail?"
    date: 2004-07-22
    body: "barenque"
    author: "arturo barenque morales"
  - subject: "Re: Login to Hotmail?"
    date: 2004-07-30
    body: "hotmail is cool"
    author: "hay"
  - subject: "Re: Login to Hotmail?"
    date: 2004-09-19
    body: "ihave probleme tologin please help me thank your"
    author: "ROGERCHARLES"
  - subject: "Re: Login to Hotmail?"
    date: 2006-08-09
    body: "I m using a network PC in my college.The Server adminstrator has locked the hotmail site n sites like yahoo mail etc..\nwehn i open the page hotmail.com n put in my user name n password n click on sign in then the page keeps on loading n eventually CANNOT FIND SERVER or just \"done\" with a blank white screen appears.My college is using a DSL connection.\nYahoo mail is opened from other sites which hav the option tht u can check ur yahoo mails thru their web...so i wanna ask is there any web which enable me to check my hotmail mail or do u ppl hav any other way to solve my problem.\ni'll b thankful\nBa'bye"
    author: "sana"
  - subject: "Re: Login to Hotmail?"
    date: 2008-09-18
    body: "I'm just here to bitch\n\nI hate that hotmail changed the sign in format so you have to srart all over again each time you sign out and sign in.\n\nBefore I could just change a couple of letters to my sign in and jump accounts.\n\nNow I gotta start all over again\n\n\nits just utter bull shit\n\nit doesn't improve the service or help in any way, it just brings inconvenience and was just change for the sake of change\n\nHotmail sux shit thru its teeth.\n\nTHE MORONS.\n\nPOLICE STATE AMERICAN GANGSTERS.  "
    author: "riki"
  - subject: "Re: Login to Hotmail?"
    date: 2009-01-04
    body: "FHFJHGJ"
    author: "H-AL-NAJJAR@HOTMAIL.COM"
  - subject: "THanks"
    date: 2003-04-26
    body: "For the update"
    author: "Alex"
  - subject: "Kscreensaver halt during password prompt"
    date: 2003-04-26
    body: "Kinda sad to see that reported as a bug, but I guess if cpu-intensive screensavers bog down the older computer than it must be fixed.  Though I wish it could be toggled for older computers.  Oh well, great work guys/gals"
    author: "Brendan Orr"
  - subject: "Thanks Derek!"
    date: 2003-04-26
    body: "Thank you for CVS-digest that always brings happyness in KDE community!"
    author: "Enrico Ros"
  - subject: "It's ready!"
    date: 2003-04-26
    body: "\"This week's CVS digest is ready to enjoy.\"\n\nThat CVS digest guy has always been a party animal. Good to see he's up for it again!! :)\n\nCheap jokes aside, great reading.\n\n \n\n "
    author: "Apollo Creed"
  - subject: "Re: It's ready!"
    date: 2003-04-26
    body: "Hey, nobody invites me to parties. I never remember why though.\n\nDerek"
    author: "Derek Kite"
  - subject: "Beta"
    date: 2003-04-26
    body: "Anyone have any feel for how close KDE is to a 3.2beta1 release?"
    author: "cbcbcb"
  - subject: "Re: Beta"
    date: 2003-04-26
    body: "http://developer.kde.org/development-versions/kde-3.2-release-plan.html\n\nJ.A."
    author: "jadrian"
  - subject: "Re: Beta"
    date: 2003-04-27
    body: "Ah.  Monday Month+1 Day2+14, 2003.  Very helpful ;-b"
    author: "ac"
  - subject: "Re: Beta"
    date: 2003-04-27
    body: "This is because there isn't dates for releases for 3.2.\nYou know, KDE developers where burned and flamed because of long 3.1 delay, so they simply won't give dates this time.\n\nSure a date that was true would be better, but if they can't keep that date, no date is better IMHO."
    author: "Iuri Fiedoruk"
  - subject: "Re: Beta"
    date: 2003-04-27
    body: ">You know, KDE developers where burned and flamed because of long 3.1 delay,\n> so they simply won't give dates this time.\n \n\nNot exactly, I think this message explains it better ;-)\n\nhttp://lists.kde.org/?l=kde-core-devel&m=104694433203080&w=2\n\nAnd it's quite worrying, since the graph tends to go up more than it goes down...\n\n\n\n\n"
    author: "uga"
  - subject: "Re: Beta"
    date: 2003-04-28
    body: "Nah, you should look at the whole picture. The amount of unconfirmed and new bug reports are pretty much stable while the amount of resolved bug reports is rising steadily at a very high rate. See <a href=\"http://bugs.kde.org/reports.cgi?product=-All-&output=show_chart&datasets=NEW%3A&datasets=ASSIGNED%3A&datasets=REOPENED%3A&datasets=UNCONFIRMED%3A&datasets=RESOLVED%3A&banner=1\">long_url</a>\n<p>\nWhat we need to do is thin the huge amount of old bug reports which are idling for a long time now and of which I'm sure the majority is no longer valid anymore (either fixed, wontfix or duplicate)."
    author: "Datschge"
  - subject: "Re: Beta"
    date: 2003-04-27
    body: "Actually I think it might be helpful. \nThere is a Alpha listed there, is it out yet? I think not otherwise the proper date would be listed there. If I'm right, then the Beta won't be out for any time soon...\n\nJ.A."
    author: "jadrian"
  - subject: "Re: Beta"
    date: 2003-04-27
    body: "The question should be: Has anyone a feeling for how close KDE is to a release schedule?"
    author: "Anonymous"
  - subject: "Re: Beta"
    date: 2003-04-27
    body: "The release dude said a while ago that the release date would be set when the features were complete.\n\nWhat is left to be done?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Beta"
    date: 2003-04-27
    body: "Anything red: http://developer.kde.org/development-versions/kde-3.2-features.html"
    author: "Anonymous"
  - subject: "I totally agree witht he new schedule"
    date: 2003-04-27
    body: "KDE 3.2 shuld not be released while we have more tha 1,000 confirmed bugs. And ofcourse 4.0 which I have come to expect to be fantastic and extremely stable and fast should have even fewer bugs =) I will do my best to find bugs and evn fix them once I learn Qt and finish learning C++."
    author: "MArio"
  - subject: "It could be a mistake..."
    date: 2003-04-27
    body: "Compared to GNOME's aggressive release schedule it could really be a mistake to not be focused on a specific release date."
    author: "KDE User"
  - subject: "Re: It could be a mistake..."
    date: 2003-04-27
    body: "They have the correct approach: Release early, release often."
    author: "Anonymous"
  - subject: "Re: It could be a mistake..."
    date: 2003-04-27
    body: "Which aggressive release schedule? They have set a date for the release. It is just almost free of any important new features..."
    author: "AC"
  - subject: "Re: It could be a mistake..."
    date: 2003-04-28
    body: "Huh?  Work on Gnome 2 started just a couple of months after work on KDE 2 did.  Gnome == \"release early, release often\"?  \n\nThe KDE 3.1 release did have major delays, but typically KDE releases are within about a week of projected release dates."
    author: "the grand wazoo"
  - subject: "OT: KDE Web Site"
    date: 2003-04-28
    body: "I just for the first time used the \"settings\" entry on the kde.org web site and set the color scheme to \"classic blue\". WOW. It looks so much more professional than the default color scheme - I must say I am really impressed.\n\nNow, why don't you give first time visitors (who probably won't bother chaning the settings) this wonderful color scheme?\n\nThe default just looks so empty (and not elegant).\n\nJust a thought."
    author: "Anonymous"
  - subject: "Re: OT: KDE Web Site"
    date: 2003-04-29
    body: "There'll be a poll to decide which of the available color schemes will be used as default (feel free to contribute your own ones). Currently a competition to include new custom images and signs is running (see http://dot.kde.org/1048968963/ )."
    author: "Datschge"
---
<a href="http://www.konqueror.org/features/browser.php">KHTML</a> 
gets table layout fixes. Many KScreensavers bugs have been fixed. <a href="http://www.kdevelop.org">
KDevelop</a>
adds database programming support. Dia, UML and engineering stencils have been added to 
<a href="http://www.thekompany.com/projects/kivio/">Kivio</a>.  And more...  This week's CVS digest is <a href="http://members.shaw.ca/dkite/apr252003.html">ready to enjoy</a>.
<!--break-->
