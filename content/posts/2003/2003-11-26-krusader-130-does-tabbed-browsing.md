---
title: "Krusader 1.30 Does Tabbed-Browsing"
date:    2003-11-26
authors:
  - "serlich"
slug:    krusader-130-does-tabbed-browsing
comments:
  - subject: "krusader != total commander"
    date: 2003-11-25
    body: "I hope for a nearly 100% copy of Total Commander (aka Windows Commander) on Linux.\nUsed it for years on Windows and nothing compares to this very good software.\nI like the integration of file handling: (un-)rar with password, ace, zip, tar, gz, bz2, etc...\nOr picture viewing with IrfanView.\nOr file display (extension is a own tab) and very important, the feature to change the colors independed of the Desktop Colors! I cannot look for a long time on a white background.\nAlso the file/dir selection (keyboard shortcuts) is nearly unique.\n\nPlease, lovely Krusader Developers, make your Krusader behave like Total Commander and I buy your software!! Forget bookmark handling for remote connections and do the user interface first, maybe a \"Total Commander Theme\" ;-)\n\nI think I used Windows so long because of Total Commander ;-)\n\n"
    author: "anonymous"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: "I used Total Commander for a while on windows, and it nowhere near compared to Directory Opus for Windows. \n\nAlthough for pure file management, I'd have to say the classic MacOS finder takes the cake. There is something to say for simplicity."
    author: "fault"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: "This reminds me... Directory Opus version 4 for the Amiga was insanely great, fast, flexible and very easy to use.\n\nThen, with version 5, they tried to make a program that would replace the desktop interface (\"Workbench\") and it was way too complicated.\n\n'Course, I didn't pay for either version so I'm not really in a position to complain.. ;)"
    author: "Apollo Creed"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "total commander is bullshit.\n\n- the internal API is beyond stone age. I wrote three plugins for that tool since some of our users demanded support for our custom archive/file formats.\n\n- the performance of that tool is in most cases slower than the original windows file explorer\n\n- two-panes are for beginners. even the good old \"DirOpus\" 5.x for amiga had more than two. anything else is a step into the wrong direction\n\n- IMHO the look of total commander is a pain, it reminds me of windows 3.x\n\nand last but not least: how cares about windows applications? I don't want windows  applications on my desktop that remind me of MSDOS, \"norton commander\" or \"windows commander\".\n\n\nIn my experience, users of these tools were more unproductive than other users. they did not gain any more experience and stayed at their level. These users did not organize their data, they made themself more work than necessary. In the company where I work, we made file copy operations obsolete. Everything is controlled by style guides (file/directory naming conventions), revision control systems and synchronisation (i.e. rsync). No one needs to copy anymore, everything \"moves\" automatically to the correct locations.\n\n..."
    author: "psxdev"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "- I have never had a need to write plugins for TC. What kind of company uses its custom archive/file formats, when it has had the wisdom to make such a basic operation, as copying, obsolete?\n\n- TC has never crashed on me, unlike Explorer (many times a day). Outperfoms explorer which needs to be restarted every once in a while, or perhaps the whole system needs a reboot.\n\n- If you need more panes than two, use some other tool. Two is enough for me.\n\n- I don't care about the look of TC. I do work with it, not spend hours on gazing its looks.\n\n- I care about windows applications, since I have no choice. Windows is used where I work.\n\n- Maybe I am less productive than other people. I have no means to prove otherwise. And no need to prove it to you..."
    author: "janza"
  - subject: "Re: krusader != total commander"
    date: 2005-02-25
    body: "Hi,\nI use (Windows)Total Commander for some time and it's not true that people using it is less productive than others. I've got difference experience. Since I have started using it, my productivity has risen nearly twice !\nFast directory browsing thru bookmarks is uncomparable to any other style of browsing...especially if it is used to go to remote locations (like FTP and it's concrete subdirectory ! (to see logs for example))\nAnother great option is tabbed browsing (so you can have more then just two panels).\n\nI would really appreciate if Krusader could have ability to work more like TC...:c)\n\nBTW: Sunchronization tools are useful for very limited types of operation, like working with source code or writting some shared documents...but there are many other \"operation\" than have to be done...and they have to done quickly !"
    author: "Rostislav Stribrny"
  - subject: "Re: krusader != total commander"
    date: 2005-11-12
    body: "Let go of the mouse and start using the keyboard. Thats how TC is meant to be used. Anyone whose ever challenged me on how fast you can zip around the filesystem and other routine things you would normally do, normally loses and by a decent margin. I'm always open to suggestion that there is something better out there, but I haven't yet found it (including Opus), so get off the old high-horse and just accept that some things are better than others and some people work better with different tools."
    author: "Jose"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: "I use Total Commander at work and Krusader at home and I could really appreciate if the user interface of Krusader could be configured to be exactly like the UI of Total Commander. As for tabbed browsing: nice, but not very high on my priority list. Remote connections are a nice feature too, but lets face it: its the local files I want to manage with Krusader.\n\nAnyway, nice job you have done so far Krusader developers!"
    author: "janza"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: "If you want it so much, why don't you code it yourself? Or, if you can't do that, how about giving that money to somebody to do it?\n"
    author: "Lubos Lunak"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: ";-)\n\nthat's a nice thought, but we'd settle for bug-reports and useful suggegstion first! it isn't that helpful to say something like \"TC look and feel\", be specifc so we could talk specifically.\n\n"
    author: "Shie Erlich"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: "sorry to disappoint you, but we don't hold the \"total commander ui\" feature very high on the priority list. once upon a time, krusader let you change colors, but we decided that the program should fit into KDE as a desktop and look the same.\nin any case, even if you could change colors and stuff, it would still not look like TC. widget set and all...\n\napart from that, i do think it looks close enough already. what's missing?\n\nbtw: we just found out that tabbed-browsing and bookmark handling a-la krusader was implemented in the new TC version 6.0, so i guess we're doing something right ;-)"
    author: "Shie Erlich"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: "I did not mean Krusader should imitate the looks of TC, but behaviour. Sorry if my message was confusing... Little anoyances that I usually run into are shortcut key differences like: Shift+F6 when trying to rename a file, other example would be selecting files with shift+arrow up/down, in TC I can let go of shift scroll further push shift back down and select some more files. There are a buch of other things I have run into, but don't come into my mind right now. \n\nI know, these are little things and I can live with them, but they keep coming up every once in a while. As someone suggested, I could just code the changes my self, but I think Krusader is not the easiest peace of code I can start practising my KDE/Linux programming, since I have so little experience in it...\n\nStill, considering all the above Krusader is by far the best filemanager for Linux!"
    author: "janza"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "Just came in to my mind: normal mistake I keep doing is pressing Alt+F1/F2 that changes drive in TC. Of course there is no need to changed drive in Linux, but I would like to configure these shorcuts to open bookmarks for left/right pane..."
    author: "janza"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "problem is that some keys used by TC are already taken by KDE"
    author: "Shie Erlich"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "Could Alt+1/2 be used for shortcuts to bookmarks?"
    author: "janza"
  - subject: "Re: krusader != total commander"
    date: 2005-08-30
    body: "Any body have any idea about Total Commander software for RedHat Linux version (RPM). Pl give me some idea about it. I need it.\n\nI am using Total Commander since 2 years in Windows. But Now I am using Linux in my Laptop. So I need it ASAP.\n\nPl help me on this regards.\n\n\nThanks\n\n\nNazmul"
    author: "Nazmul Hoque"
  - subject: "Re: krusader != total commander"
    date: 2003-11-26
    body: "I think you answered to my question ;-)\n\nThe look of the icons are irrelevant, don't need Krusader to look the same as TC. I mean it too: the behaviour\n\nIt's nice to have the keyboard shortcuts, browsing and marking through files/dirs and the behaviour of compressed files from TC. Compressed files, once decompressed in a tempdir, have nearly the same behaviour like normal files/directorys. Also the extension (.mp3, .rar, .ace. bz2...) should be configurable to have an own tab for a better overview.\n\nThe white background is too light for me, sorry I can't do anything. I *always* change the color background of TC to light grey if I install it on a machine at work.\n\nIn combination with winrar and irfanview, TC was the best for me on Windows.\nwinrar context menu allows me to decompress \"here\" or make a directory called like the compressed filename without extension. Great! With Irfanview I pressed F3 for viewing pictures and movies.\n\nI know, all suggestions are not for the wide user range, but exactly this behaviour and integration was the reason to use these programs and not the other 1000 \"normal\" programs.\n\nNow, I \"code\" some shellscripts for the context menu in KDE to have nearly the same behaviour.\n\nI think someone should laugh at me because I have the double-click in KDE activated. Sorry, I used it for years an Amiga & Windows, what's wrong?\n"
    author: "anonymous"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "\"I think someone should laugh at me because I have the double-click in KDE activated. Sorry, I used it for years an Amiga & Windows, what's wrong?\"\n\nThere's nothing wrong with it, that's why that option exists:)\n\nI've become acostumed with single-click in kde, but never understood this idea to disregard the convention on all other major desktops. Was it made to emulate the single-click option that win98 had? I don't know.\n\nWhen I brought my father to windows, I changed his desktop to double-click, since even after years of using the web, my father never could get rid of the double-click behaviour he had known since win95, and still double-clicks links on the web sometimes.\n "
    author: "jukabazooka"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "KDE always uses single click as default for usability reasons, it's more reliable and easier to use for those who have a hard time with timed double clicking for whatever reason."
    author: "Datschge"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "Allways?\n\nI've KDE _allways_ set on dubklik behavior... _Never_ had a problem ;-)"
    author: "cies"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "Single-click behaviour on KDE has a few problems:\n\n- Acidental clicks. Since everybody keeps his finger rested on the left button of the mouse, ocasionally we single click without wanting to, be it a reflex, or whatever. This can cause for acidental apps to be launched, files to opened, buttons to be pressed.\n- Selecting a file is unintuitive. We must drag a selection box around the file, which makes for more work to work with them when comparing with other desktop behaviours.\n\nI know I shouldn't put this here, but it the right places, but it seems to me that this behaviour is so set in stone, that it would do little good. And while an option of double-click exists, I'm happy."
    author: "Luis Silvino"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "I always see this, and I always ask the same thing, and I never get a full answer:\n\nWhy do you want to select ONE file?\n\nSelecting several? Sure, and you do it in KDE just as in windows, using Ctrl-click and shift-click.\n\nBut selecting ONE?"
    author: "Roberto Alsina"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "I don't know. I just know that I do it all the time :)\nI select a file before moving it. I select a file when I want to rename it or view its preferences. I select a file and just then decide to select some more.\n\nThe thing is, when I'm on KDE I feel afraid of using the mouse because just clicking somewhere can trigger a number of potentialy destructive actions. The relative complicatedness of double-click is a protection against doing something wrong and that's why I like it."
    author: "An ony mous"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "Ok, at least we agree that selecting the file to rename it or see its preferences is not necessary, right? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "What kind of potentially destructive actions are you thinking of? Simply opening a file by single clicking is not what I'd call \"destructive\"."
    author: "Datschge"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "How to select a file with single-click:\n\nFirst. The file does not get launched by mouse button down, but on button up. So if you want to drag/drop a file just do as you always do, click on the file and drag, still keeping the button down. Moving the mouse also helps accidental clicking.\n\nSecond. If you want to delete, rename etc, a file: Right-click on the file and choose the appropriate action.\n\nThird. If you want to mark one file, for any reason, without launching it. Press ctrl before selecting the file."
    author: "Dragen"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "I understand how the system works. I use it. I still think it's less unintuitive, less easier to grasp than \nsingle-click=select file\ndouble-click=launch file\n\nThe first anoyance of people I know who start using KDE is this behaviour.\nIt took me a _long_ while to get the complete hang of it, months maybe (probably because I also use windows, and having to switch between behaviours in my head is harder than switching between desktops), a lot longer than when I first started using graphical desktops with win95. And if I remeber, KDE1 didn't work like this. It's obvious to me that no deep study has gone into this, nor any testing. But I'll shut up now, and forever hold my peace about this matter."
    author: "Luis Silvino"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "Just pretend it's a webpage. Howcome nobody complains that links open when you single click them?\n\nAnd yes, I have seen people double-clink on webpage links, but it's pretty unusual. Hell, I have seen people double-click windows quick launchers, which are buttons and thus single-clickable, or toolbars.\n\nDouble-click is evil."
    author: "Roberto Alsina"
  - subject: "Re: krusader != total commander"
    date: 2006-12-22
    body: "Because you never select links! And you also have a back button for when you fuck up. \n\nI can see it now:\n\"Oh yeah, I'm just gonna move these three links so I'll just select'em first. Yeah, and delete this link, and rename this one.\""
    author: "happyhappy"
  - subject: "Re: krusader != total commander"
    date: 2003-11-27
    body: "Yup, I think single click has the potential to be much more user friendly than double click, but the current implementation in KDE has a lot of problems that make it less user friendly than double click mode."
    author: "fault"
  - subject: "Re: krusader != total commander"
    date: 2003-11-28
    body: "Care to elaborate?"
    author: "Datschge"
  - subject: "Re: krusader != total commander"
    date: 2005-12-02
    body: "Ok, I see tons of people having this discussion of how \"difficult\" it is to adapt to the single click style of operation in konqueror. Well all that aside, when im hammering through files in a file manager of sorts, I am (was) constantly using the quick search method of looking for files (ie. click a file and type and it automatically highlights files with the name starting with...) well with the single click style of operation, this is not possible. I dont know about you, but when im diggin through say... 146 files at once, i dont want to be scrolling down doing my abc's (i got $hit to do)... does anybody know how to force konqueror to behave with the double click mode? I've got it set in the global settings but konqueror refuses to play nice with me and now I want its config file to edit(or something) if possible... Any help would be greatly appreciated. Thanks all"
    author: "Novell Newb"
  - subject: "Re: krusader != total commander"
    date: 2004-09-08
    body: "Hi there Shie\n\nWell, I've used TC for a long time (4 years now), and have recently switched to Debian. I think that Krusader is great in comparison with TC, but there's one thing I dearly miss. TC can be configured to jump to a file as you start typing its name, autocompleting as you continue to type. Krusader only recognises the first letter you type, therefore should you have more than one file starting with the same letter, you'd have to repeatedly keep typing the first letter (or start scrolling down) to find the correct instance.\n\nAnyways, keep up the good work."
    author: "Jacques"
  - subject: "Re: krusader != total commander"
    date: 2004-10-01
    body: "The things I miss from TC : \n\n- right-click to mark a file as selected\n- the search function as described above : you get the focus on files as you type the name, and not only on first letter\n- the Alt-F1 / Alt-F2 to change the content of the panes (ok, there is no disk, but there could be bookmarks)\n"
    author: "O."
  - subject: "buy?"
    date: 2003-11-26
    body: "This software is Free. Download away!"
    author: "Royall"
  - subject: "Re: buy?"
    date: 2003-11-26
    body: "Yes, I will buy it. I know it's free and I have installed it ;-)\n\nBut if Krusader will feel like \"total commander\" I will buy Krusader for this (in my eyes) very helpful feature...\n"
    author: "anonymous"
  - subject: "Re: buy?"
    date: 2003-11-26
    body: "is free as in free speech not as in free beer...if people like the software they should contribute with money!!"
    author: "Luis Mendonca"
  - subject: "Re: buy?"
    date: 2003-11-26
    body: "Or better yet, contribute with code, I'd say!"
    author: "Andr\u00e9 Somers"
  - subject: "Re: buy?"
    date: 2003-11-28
    body: "Or both! Or, at the very least, an extremely warm thank you. :-)"
    author: "Jilks"
  - subject: "double click"
    date: 2003-11-26
    body: "krusader is nice, I've used several versions so far, and\ntabbed browsing is really another improvement.\nhowever, I've also always be annoyed by the inevitable\ndouble click to open files, directories...\nwhich doesn't conform to kde standard and is not configurable."
    author: "hoernerfranz"
  - subject: "Re: double click"
    date: 2003-11-26
    body: "> I've also always be annoyed by the inevitable\n> double click to open files, directories...\n\nI hope, Krusader developers will fix this.\nBut IMO mouse isn't important.\n\nSince Norton commander, thousands and millions users like to\nnavigate in filesystem with just 6 buttons - arrow keys, Tab and Enter.\nAll other actions may be called with F1-F10 keys, etc. "
    author: "gray"
  - subject: "Re: double click"
    date: 2003-11-26
    body: "actually, krusader does support using single clicks.\nit's just not the default\n"
    author: "Shie Erlich"
  - subject: "Re: double click"
    date: 2003-11-27
    body: ">actually, krusader does support using single clicks.\n>it's just not the default\n\nuhm,\nwhere can I configure this ?\n(didn't find it so far...)\n"
    author: "hoernerfranz"
  - subject: "Re: double click (but ignore 2nd click)"
    date: 2003-11-26
    body: "Off topic, but I have a question about single-click support in KDE (QT?) in general.\n\nI kind of like single-click, but there are many Mac/Windows users that insist on double-clicking all the time and totally freak out when they end up opening several windows.  This is especially true because of the startup delay for KDE apps on older systems.\n\nMy question: is it not possible in QT to either request doubleclick notifications and ignore them, or check the time delay between 2 single clicks and ignore the second if it's too soon.  It'd be a pain if it required code for every widget that is activated by a single click, but couldn't the toolkit be coded to handle this transparently?\n\nRob"
    author: "Rob"
  - subject: "Re: double click (but ignore 2nd click)"
    date: 2003-11-27
    body: "KDE lets you choose wether to use single of double clicks"
    author: "anonymous"
  - subject: "Re: double click (but ignore 2nd click)"
    date: 2003-11-27
    body: "My guess is that that is not what he meant. If you have KDE configured for single click, he suggested it ignores the second consecutive click within a certain interval. I think that makes sence. I have seen people using my system that were totally confused, but I'm not about to change my system for them. This would be a nice solution that neither gets in my way, nor makes them scared :-)"
    author: "Andr\u00e9 Somers"
  - subject: "Good stuff!"
    date: 2003-11-26
    body: "This is fan-goddamn-tastic. Its like the old XTree program for DOS but about 10x better. I've been using konq for so long I forgot how freaking handy it is to use a program MEANT for file/dir navigation/manipulation.\n\nThanks for the great software! "
    author: "rsk"
  - subject: "smb4k must be in KDE 3.2 please....."
    date: 2003-11-27
    body: "I am very pleased with smb4k, windows smb share browser, we must have it in KDE as \"Samba Network\" or \"MS Network\" or \"Samba Neighboorhood\". KDE have been very poor in dealing with smb shares. the only hope i see is with smb4k very well integrating with konqueror and KDE in general."
    author: "smb4k user"
  - subject: "Re: smb4k must be in KDE 3.2 please....."
    date: 2003-11-27
    body: "Feature freeze for KDE 3.2 was three months ago."
    author: "Anonymous"
  - subject: "Hrm, why?"
    date: 2003-11-28
    body: "Sorry if I don't get the point of this, but what doe sKrusader have that Konqueror doesn't already offer? I can already do a vertical split mode like this in Konqueror by clicking a single button."
    author: "Jason Keirstead"
  - subject: "Re: Hrm, why?"
    date: 2003-11-28
    body: "Easy navigation/commmands without mouse? I also tried Konq in split screen mode, its no good."
    author: "janza"
  - subject: "Re: Hrm, why?"
    date: 2003-11-28
    body: "Konqueror is not Norton Commander-like.\n\nIt's as simple as that, really."
    author: "Rune Kristian Viken"
---
<a href="http://krusader.sourceforge.net/">Krusader</a>, the old school file manager for KDE, now supports tabbed-browsing in the <a href="http://prdownloads.sourceforge.net/krusader/krusader-1.30.tar.gz?download">1.30 release</a>. Each panel can create unlimited tabs, thereby keeping the twin-panel look and feel while allowing you to keep local folders, ftp, ssh open all at once.  <a href="http://krusader.sourceforge.net/img/krusader130.png">Screenshot here.</a> Of course, this release <a href="http://krusader.sourceforge.net/index.php?nav=text.php&note=1.30.notes&path=stable">offers a lot more</a>.  Enjoy!
<!--break-->
