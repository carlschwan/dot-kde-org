---
title: "KDE Traffic #48 by Russell Miller"
date:    2003-04-29
authors:
  - "numanee"
slug:    kde-traffic-48-russell-miller
comments:
  - subject: "First Thanks"
    date: 2003-04-29
    body: "I love to be a KDE user :-))))\nNowadays more than ever.\nThank you for the infos, Mr Miller.\n\nXtian.\n"
    author: "Sangohn Christian"
  - subject: "Thank goodness that"
    date: 2003-04-29
    body: "aKtion is finally dead"
    author: "fault"
  - subject: "Additionally"
    date: 2003-04-29
    body: "KAphorisms and KGeo are now dead too."
    author: "Anonymous"
  - subject: "Re: Additionally"
    date: 2003-04-29
    body: "KGeo looks useful though."
    author: "KDE User"
  - subject: "Re: Additionally"
    date: 2003-04-29
    body: "KGeo is replaced by Kig (http://edu.kde.org/kig/) which can import KGeo save files."
    author: "Anonymous"
  - subject: "Re: Thank goodness that"
    date: 2003-04-29
    body: "Now all KDE needs to do is deal with kplayer, kmplayer, kaboodle, noatun, kscd and kmid all fulfilling various parts of the same role in different ways (have I missed any out?), none of them covering everything, so it just becomes too much effort working out what works right with what.\n<p>\nIt's very annoying as a casual end user having so many apps in the base system that all do almost-the-same-thing, and inability to identify one way of doing the job well is the biggest thing putting me off KDE as my only desktop atm."
    author: "Tom"
  - subject: "Re: Thank goodness that"
    date: 2003-04-29
    body: "> It's very annoying as a casual end user having so many apps in the base system that all do almost-the-same-thing, and inability to identify one way of doing the job well is the biggest thing putting me off KDE as my only desktop atm.\n\nI've heard that having choice does bother some people. Ironcially you will find the same thing with Gnome and desktop neutral apps... those darned developers just keep developing the programs they want without regard to those people enjoying it for free with better ideas... sans action.\n\nUnfortunately in the present case you mention it is more a matter of ever shifting codecs and such. the programs mentioned use various other programs underneath to do the basic work and the proprietary companies like M$ keep changing those codecs to make it difficult for their competition, not to mention using various other means to thwart free software efforts by legal and financial avenues... which seems to work fairly good at convincing some people Linux/KDE/et al. is not ready.\n\nPick the one you like best, contact the auther and ask if they would be encouraged to do the features you want if you sent them a gratuity. After all... your alternatives are often the world of enforced, persistent and repetative gratuities."
    author: "Eric Laffoon"
  - subject: "Re: Thank goodness that"
    date: 2003-04-30
    body: "Absolutely!!!"
    author: "AC"
  - subject: "Re: Thank goodness that"
    date: 2003-04-29
    body: "I use Kscd to play audio CDs. I use KMplayer to play DVDs. I use Noatun to play MP3s and Oggs. They most certainly do not do \"almost-the-same-thing\". That doesn't mean that all these should go their own separate ways, but they should remain distinct applications. But realizing that not every agrees with this way of doing things, these applications should still use common libraries, share plugins (like visualizers), etc. Also one could make an audio \"shell, in the same way that Konqueror is a browser \"shell\". Open up any media type in this shell and don't worry about individual applications, just as you can open up postscript Konqueror without having to worry about KGhostscript. But don't get rid of Kscd and KGhostscript!"
    author: "David Johnson"
  - subject: "Re: Thank goodness that"
    date: 2003-04-30
    body: "> Also one could make an audio \"shell\", in the same way that Konqueror is a browser \"shell\".\n\nAFAIK that's exactly what Noatun is, a sophisticated frontend, with arts and mpeglib being the \"real workhorses\" underneath. So to get Quicktime/DivX/whatever to work reliably with Noatun, support for these would have to be added to the underlying multimedia architekture.\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: Thank goodness that"
    date: 2003-04-30
    body: "Yes. The problem is that there is no framework yet that can fulfill all your multimedia needs. All frameworks have one or two things that they are good at (e.g. playback and simple recording in the mplayer case), but are not usable for other things. This is sigificantly worse than the situation on Windows, where the media framework is pretty complete and usable for allmost all purposes. \nRight now the only contender that's comparable in scope with Windows Media is GStreamer. But they haven't reached that goal in terms of maturity and plugin-completeness yet. \n\nMPlayer G2 is a pretty bad idea, since it will be just another framework that does one or two things. But at least it will be a framework this time, but otherwise it is already obsolete.\n\n"
    author: "AC"
  - subject: "Re: Thank goodness that"
    date: 2003-05-01
    body: "Funny you mention gstreamer, I just finished installing and playing with 0.6.1\n\nI must say with it definitly is not finished, but this is the first release that actually works for me.  I have a lot of video and gst-player now can play around 90% of it (xine can play them all).  This is pretty cool, considering what gstreamer is trying to accomplish.  The rate it is going the next version may be quite usable for most people."
    author: "theorz"
  - subject: "Re: Thank goodness that"
    date: 2003-04-30
    body: "Now it is time to someone to start up project named kmplayer2 or some which would be based on the new improved MPlayer-G2 arch.\n\nGreetings to all kde developers!"
    author: "vlindos"
  - subject: "Obsolete"
    date: 2003-04-29
    body: "KPaint?"
    author: "Bert"
  - subject: "Re: Obsolete"
    date: 2003-04-29
    body: "Obsoleted by what?"
    author: "Anonymous"
  - subject: "Re: Obsolete"
    date: 2003-04-29
    body: "\nI think so, 3 months ago I wrote a patch to add rotating of images. I mailed the patch to the maintainer but he told me he was not maintaining it anymore...\n\nYet kpaint stays usefull if you only have to make small changes to a picture and you don't want to load up something as big as gimp. Not even is it still usefull, it is also very stable.\n\nPieter"
    author: "Pieter"
  - subject: "Re: Obsolete"
    date: 2003-04-29
    body: "Please send the patch to kde-core-devel."
    author: "Daniel Molkentin"
  - subject: "Re: Obsolete"
    date: 2003-04-29
    body: "Send me the patch I will look at it and apply it if there is no maintainer.\n\nOtherwise submit the patch to kde-devel mailing list, sometimes patches are picked up from there and applied.  If all else fails get on IRC and go to #kde-devel and say \"Hey everyone, I have a patch for this app that is being ignored, can someone help me get it applied?\"\n\nAs someone who uses KPaint quite a bit, I would be willing to take that patch and apply it, barring there are no bugs ;)\n\nThanks\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Obsolete"
    date: 2003-04-29
    body: "As you say it's usfull for small tasks. It sometimes looks like some of the older/smaller apps don't have maintainers becouse nobody notices when they dissaper. The other developers fix the few problems they see and there are no other good reason too thouch the code.\n\nTry mailing your patch to one of the mailinglists or attach to a featurerequest/bug in bugzilla. if it's good odds are someboddy with cvs acces will pick it upp and apply it.\n"
    author: "Morty"
  - subject: "Re: Obsolete"
    date: 2003-04-29
    body: "Try harder! At least one (Laurent Montel) of the listed authors in \"About KPaint\" dialog is still a very active KDE developer."
    author: "Anonymous"
  - subject: "Re: Obsolete"
    date: 2003-04-30
    body: "Well, but he is listed in almost every app. And he is also the maintainer of KSpread and KPresenter and ? I can't remember when he added the last time a feature to one of them."
    author: "Peter"
  - subject: "Re: Obsolete"
    date: 2003-04-29
    body: "IIRC I pointed you at another developer who had been working on it (KPaint). I didn't just say \"I'm not maintaining it\". I find it interesting that despite the fact KPaint was my first real C++ program, and was written when KDE had just started people still find it useful. :-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Obsolete"
    date: 2003-04-30
    body: "\nthe patch is against kdegraphics 3.0.x, while kpaint has not changed, the location of the menu xml file and documentation might have changed, so be carefull when applying them. It would be nice if you listed me in the authers section (Pieter Pareit, pieter_dot_pareit_at_skynet_dot_be).\n\nPieter"
    author: "Pieter"
  - subject: "Re: Obsolete"
    date: 2003-04-30
    body: "Listed as author because of one contributed patch?"
    author: "Anonymous"
  - subject: "Re: Obsolete"
    date: 2003-04-30
    body: "\nTalking about being obsolete...\n\nPieter"
    author: "Pieter"
  - subject: "Re: Obsolete"
    date: 2003-04-30
    body: "He wouldn't be the first..."
    author: "AC"
  - subject: "URL"
    date: 2003-04-30
    body: "I seem to remember reading in Kernel Traffic that the www.kerneltraffic.org domain was preferred to kt.zork.net for linking to them."
    author: "anon"
  - subject: "Re: URL"
    date: 2003-04-30
    body: "This is the link everyone is sending me.  Must be something wrong at the source for the link to be reaching me like that.  Look at the LinuxToday links, they all point to kt.zork.net too.\n\nhttp://linuxtoday.com/developer/2003042900926NWCYKN\n"
    author: "Navindra Umanee"
---
Russell covers the latest news on <a href="http://www.slac.com/~mpilone/projects/kde/kmailssh/index.html">KMail SSH tunnelling</a>, <a href="http://www.csh.rit.edu/~benjamin/kautoconfigdialog/">KAutoConfigDialog</a> and more.  Read it here in the <a href="http://kt.zork.net/kde/kde20030426_48.html">latest KDE Traffic</a>.
<!--break-->
