---
title: "KDE at LinuxWorld Expo San Francisco Report"
date:    2003-08-11
authors:
  - "csamuels"
slug:    kde-linuxworld-expo-san-francisco-report
comments:
  - subject: "Nice photo show, thanks. =)"
    date: 2003-08-11
    body: "*looks through them and enjoys the comments*"
    author: "Datschge"
  - subject: "Wow !!"
    date: 2003-08-12
    body: "<  automatically switch virtual desktops periodically \n<  `while [[ true ]]; do dcop kwin KWinInterface nextDesktop ; sleep 3; done`\n\n           I am new to KDE. But if the above thing is really possible, then I am really really impressed. It looks like that KParts/DCOP is the best object model in the world. It brings us back to the UNIX philosophy of doing things, even for GUI programs. Thanks to all people involved in the development of KParts. You rock !!\n"
    author: "Nilesh"
  - subject: "Re: Wow !!"
    date: 2003-08-12
    body: "Gidday,\n\n\n(`while [[ true ]]; do dcop kwin KWinInterface nextDesktop ; sleep 3; done`)\n\nYip, try it !!!\n\nI did and it works brillant!!\n\n\nRegards\nCoomsie :3)"
    author: "Coomsie"
  - subject: "Re: Wow !!"
    date: 2003-08-12
    body: "But why the double square brackets around true?"
    author: "AC"
  - subject: "Re: Wow !!"
    date: 2003-08-12
    body: "yes, type (or paste) exactly this:\n\nwhile [[ true ]]; do dcop kwin KWinInterface nextDesktop ; sleep 1; done"
    author: "ac"
  - subject: "Re: Wow !!"
    date: 2003-08-13
    body: "My point is that the brackets are unnecessary, and probably misguided. Try it without.\n\n\"while [[ false ]]\" and \"while [[ hippo ]]\" do the exact same thing as \"while [[ true ]]\" for me. But a more simple \"while true\" and \"while false\" are opposites."
    author: "AC"
  - subject: "Cool"
    date: 2003-08-12
    body: "Yeah, that is cool, but where do i write it to make it work? "
    author: "Mario"
  - subject: "Re: Cool"
    date: 2003-08-12
    body: "open up a konsole then copy/paste it.  neato!"
    author: "standsolid"
  - subject: "Konqi"
    date: 2003-08-12
    body: "So what's the story of Konqi's last name?\n\nhttp://ktown.kde.org/~charles/lwce/p8060043.jpg"
    author: "AC"
  - subject: "Re: Konqi"
    date: 2003-08-12
    body: "There's no story: Konq(ue|i)rer. Do you see the \"rer\"?  :-)"
    author: "Melchior FRANZ"
  - subject: "Re: Konqi"
    date: 2003-08-12
    body: "It's Konqui Konqueror, but perhaps that would have been to obvious for the exhibition management."
    author: "Anonymous"
  - subject: "Re: Konqi"
    date: 2003-08-12
    body: "Sorry not true.\nIt's not Konqui Konqueror, but Konqi de Konqueror\n\n--\nKonqi"
    author: "Konqi"
  - subject: "Re: Konqi"
    date: 2003-08-12
    body: "Erm... so, Konqueror is your married name? ;-)\n\nOk, for those who have no idea what I said, in spanish-speaking countries, the wife takes the husband's name like this: Juana P\u00e9rez marries Juan L\u00f3pez, and she becomes Juana P\u00e9rez de L\u00f3pez or Juana de L\u00f3pez\n\nAnd I will address possible objections:\n\na) No, I don't know Konqi is german. Never met the reptile.\nb) Apparently dragons have no visible genitalia, so I don't know he's a boy dragon.\nc) No, I am not sure Kate is a girl dragon\nd) Even if Kate was, that doesn't mean that much nowadays.\ne) Godzilla was a girl! (at least in the Hollywood version)\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Konqi"
    date: 2003-08-12
    body: "That's not true in Spain at least. And yes, we also speak spanish :))\n\nSaludos,\n             Xabi."
    author: "Xabier Ochotorena"
  - subject: "Re: Konqi"
    date: 2003-08-12
    body: "Ok, in _some_ spanish speaking countries ;-)\n\nIn others, it would mean Konqi is from a place called Konqueror.\nOr that her mother's maiden name was Konqueror and his parents were pretentious ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Konqi"
    date: 2003-08-14
    body: "Well, Roberto I'm glad you aksed. There seem to be a lot of confusion about my background.\nA lot of people seem to think I'm German but to be honest, I truly originate from Normandy, France of course. My over-over-great-great-great-grandparents were William the Conqueror  Duke of Normandy, King of England who was married to Matilda Baldwin. \n\nWhen Willam died from complications of a nasty battlefield wound in 1087 Matilda had fling going with the Swedish heir to the trown G\u00f8defr\u00f8 who was unfortunately married to the revengeful R\u00e4gnhilda.\nWhen R\u00e4gnhilda found out G\u00f8defr\u00f8 was having an afair with Matilda de Conqueor she was a woman on a mission and out for revenge. She contacted the Trolls, who licensed her a spell KueTee which enabled her to make sure even 11th child born from the line de Conqueror would be a dragon who would have to spend it's live as a mascot and was never to use the name Conqueror again. Hence the name change to de Konqueror. Matilda flee to Germany and ended up in a Kastle on a mountain near the river the Rhein where she still can be heard wispering names on misty nights when the moon is half and fireflies dans around the computerscreens.\n\nSleep tight and be well.\n\n--\nKonqi\n\n\n\n"
    author: "Konqi"
  - subject: "Re: Konqi"
    date: 2004-05-27
    body: "\"G\u00f8defr\u00f8\"? Kalle, is that you? :)"
    author: "David Faure"
  - subject: "Re: Konqi"
    date: 2003-08-12
    body: "As the happy volunteer assigned to go pick up Konqi's badge, I was given a bit of a hassle by the lady in charge of the badges. She wanted Mr. Rer to pick up the badge himself, with photo ID. Fortunately, the gentlemen working right next to her was a KDE fan. \"Oh, I know him! I'll take care of this.\" He then got me the badge and topped it off with some praise for KDE."
    author: "David Johnson"
  - subject: "Konqi The Dragon"
    date: 2003-08-12
    body: "Off Topic: It is strange, I have searched the internet and I'm yet to obtain a wallpaper of Konqi the Dragon. Does anyone know where I can obtain one? One would expect www.kde.org to have a copy of it, or for a copy to ship with kdeartworks. Unfortunately, that's far from it. :-(\n\nBesides my rant, excellent article and representation.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: Konqi The Dragon"
    date: 2003-08-12
    body: "You don't know http://kde-look.org? Visit it, search for \"Konq\" and you can find funny wallpapers:\n \n http://kdelook.org/content/show.php?content=2133\n  http://kdelook.org/content/show.php?content=3856\n  http://kdelook.org/content/show.php?content=2127\n  http://kdelook.org/content/show.php?content=2159\n  http://kdelook.org/content/show.php?content=4066\n\nOr create a wallpaper yourself with the images in kdepromo/graphics/konqui or use the Konqui SDK linked on artist.kde.org"
    author: "Anonymous"
---
In lovely weather-schizophrenic San Francisco, <a href="http://ktown.kde.org/~charles/lwce/">KDE once again had a presence</a> this year. The show itself had the distributions like SuSE, Gentoo, Debian, Redhat. Oddly, SCO was nowhere to be seen. Perhaps due to a lack of product?  Also present were the big-name regulars like IBM, Sun, and HP. Little blood was lost between any Gnome-KDE strife.
<!--break-->
<p>
Users were continuously impressed by KDE, for obvious reasons.  But easily the most often-asked question was "What the latest version is" (3.1.3), and the <a href="http://developer.kde.org/development-versions/kde-3.2-features.html">cool new features</a> in the upcoming KDE 3.2.
</p>
<p>
All exhibitors I've asked also claimed an increased amount of visitors compared to the previous year; possibly due to the reduced obligations to employment.  There also were reports of a 20% increase in early registrations.
</p>
<p>
Some of the highlights in the show include the folks from the <a href="http://www.ilog.com/">ILOG</a> requesting assistance in configuring their demonstration machine to automatically switch virtual desktops periodically (`while [[ true ]]; do dcop kwin KWinInterface nextDesktop ; sleep 3; done`) and KDE claimed victory (by one pip!) in the Gnome-KDE backgammon competition.
</p>
<p>
Photographs are available at <a href="http://ktown.kde.org/~charles/lwce/">ktown</a>.
</p>
<p>
A big thanks again goes to <a href="http://www.suse.com/">SuSE Linux</a> who provided the two computers and a spare table.
</p>