---
title: "KDE Traffic #50 is Out"
date:    2003-05-13
authors:
  - "rmoore"
slug:    kde-traffic-50-out
comments:
  - subject: "First Post!"
    date: 2003-05-13
    body: "Thank you Russell and Juergen!!!"
    author: "fppp"
  - subject: "Re: First Post!"
    date: 2003-05-13
    body: "KDE traffic rocks!!!"
    author: "xox"
  - subject: "Lots of Koffice changes"
    date: 2003-05-13
    body: "Lots of koffice changes this week... I noticed that it said that David Faure said that koffice is moving to the OpenOffice file format.. when is that expected to happen? I think it's a positive step. \n\nAlso, lots of posts by mosfet this week! Is he coming back to KDE development?"
    author: "Pal"
  - subject: "Re: Lots of Koffice changes"
    date: 2003-05-13
    body: "there was no specific date given..looks more like long-term-maybe. Mosfet is working on at least\none kde-project..i think it was about ksplash or the theme/style manager. guess it was the themer.."
    author: "Juergen Appel"
  - subject: "Re: Lots of Koffice changes"
    date: 2003-05-13
    body: ">  I noticed that it said that David Faure said that koffice is moving to the OpenOffice file format..\n\nthat's surely a very very good idea!!!\n\nIn the same time they could use the MS-Office filters from OpenOffice... :-)\n\nOnce I wrote some lines about this idea here: http://www.mind.lu/~yg/office.html"
    author: "Yves Glodt"
  - subject: "Re: Lots of Koffice changes"
    date: 2003-05-13
    body: "Don't forget that kword is frame based, while OpenOffice isn't.\nFor this reason it is not so obvious to move from a koffice DTD to an OpenOffice DTD.\nMost likely the OpenOffice DTD can not hold the frame information.  Perhaps that the\nOpenOffice developers are willing to add the frame information, into their DTD."
    author: "Anonyous"
  - subject: "Re: Lots of Koffice changes"
    date: 2003-05-13
    body: "Word certainly has the concept of a 'box containing text' which is roughly what a frame is. Since OpenOffice is compatible with word, presumably OpenOffice has a similar concept, and perhaps the implementation of this is sufficiently general to support Kword frames."
    author: "cbcbcb"
  - subject: "Re: Lots of Koffice changes"
    date: 2003-05-14
    body: "OpenOffice.org has remarkably good frame support. KWord does take it a bit further with the root object on the page being a frame, but OO.o has many frame-based features KWord doesn't. Not to say that KWord won't get there, though.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Thanks Derek!"
    date: 2003-05-13
    body: "Thank you Derek!"
    author: "Anon"
  - subject: "This joke is funny the first 2-3 times"
    date: 2003-05-13
    body: "BUT NOW ITS JUST STUPID!"
    author: "mario"
  - subject: "Re: Thanks Derek! (troll)"
    date: 2003-05-14
    body: "mod partent up ;)\n\n[yeah, ./ has its (side)effects]"
    author: "stupid anon /.-poster"
  - subject: "Re: Thanks Derek!"
    date: 2003-05-14
    body: "Hey! Dude, why don't you just Kreate a sKript that autoposts 'thanKs derek' on each new artiKle on the dot?!?!\n\nBut of Kourse don't forget to add in the cheKing loop something liKe this:\n<code>\n  if (strpos(subject, \"3.2 Realeased\")) break;\n</code>\n\n\n\n\n\nbottomline: this Kind of Krazy stupid negative effects often happen when the masses expeKt something to happen (3.2 for example) but nothing happens - just a stupid posts that demonstrate a Kind of progress. masses i guess want at least 3.1.2 or something that is better than nothing. so may be we don't have blame this \"stupid\" dudes who post \"thanKs dereK\", and i am sure that if there was a new minor release there would not be such Krazy posts atleast for next 2-3 months. remember the philosophy of bazaar style - release often."
    author: "Anton Velev"
  - subject: "Re: Thanks Derek!"
    date: 2003-05-14
    body: "So you would rather *not* know about what's happening in KDE development?  And you think this applies to the average person reading the Dot?  I don't think that most of the posts indicate this.\n\nAs you probably know the 3.1.x series is just bug fixes -- there's not as much fun in announcing what's going on there.\n\nAs for \"release often\" -- well, it depends on how you define that; I would say that KDE *does* release often relative to most development.  That said, doing stuff takes time; some features take more than 3 months to implement.  There needs to be a suitable gap between releases to allow for this type of work.   "
    author: "Scott Wheeler"
  - subject: "Re: Thanks Derek!"
    date: 2003-05-14
    body: "Shut up, Anton."
    author: "Navindra Umanee"
  - subject: "Trolling"
    date: 2003-05-14
    body: "Why not just delete the post?  I think deleting blatant trolls is the best policy to keep a forum clean and comfortable.  Of course then he'll complain about censorship, but such complaints are to be expected and ignored."
    author: "xox"
  - subject: "Re: Thanks Derek!"
    date: 2003-05-15
    body: "Well, that's a real troll, but never mind.\nGood culture!"
    author: "Anton Velev"
  - subject: "Re: Thanks Derek!"
    date: 2003-05-14
    body: "Can't we just rename our Konqi into Derek now? ;)"
    author: "Datschge"
  - subject: "Re: Thanks Derek!"
    date: 2003-05-15
    body: "Don't delete posts. Sometimes, people migt be unjustly interpreted as trolls. Let us decide who isa  troll and who just offers constructive criticism.\n\nI do think users should bea ble to delete and edit their posts though."
    author: "mario"
  - subject: "Great Update"
    date: 2003-05-14
    body: "I just have one question... when did Yoda start reporting for KDE?\n\n\"Joyful was this received.\"\n\nDangle a participle we do, yes.  :)\n\nMore importantly, the entire KOffice project is one of those I would absolutely love to love.  Getting OpenOffice and PDF in and outta there will be a huge step in that direction.  Still holding out hopes for reasonable HTML support as well.\n\nA fella can dream can't he?"
    author: "Metrol"
  - subject: "One question"
    date: 2003-05-14
    body: "What's up with the top of the panel menu? Isn't it supposed to be showing the last used programs? It just fills up with the 5 first used programs and then those stay there. I don't think it  has ever worked, so why not get rid of it?\n"
    author: "tritone"
  - subject: "Re: One question"
    date: 2003-05-14
    body: "It seems to work for me.\n\nThe other day Gaim moved up a spot, and KBabel reappeared after an absence.  "
    author: "Tim_F"
  - subject: "Re: One question"
    date: 2003-05-14
    body: "The default install, at least on my MDK, it shows the 5 most used programs. If you want the last used programs just change the setting in the panelconfig. Works ewery time for me :-) Personaly I like this better, my most used progs I have as shortcut s on Kicker and some on the desktop."
    author: "Morty"
  - subject: "Re: One question"
    date: 2003-05-14
    body: "I just realized how it works, thanks.\n"
    author: "tritone"
  - subject: "Re: One question"
    date: 2003-05-16
    body: "I changed the option from most used to last used, but the change didn't take place until I logged in again, so that should be fixed.\n"
    author: "tritone"
  - subject: "Re: One question"
    date: 2003-05-14
    body: "OK, mine is set to show the 5 most often used apps, but I don't think it can possibly be working correctly.  It shows \"kaboodle\", \"kdevelop 3.0\", and \"system notifications\".  Every time I log into KDE, I am virtually guaranteed to start KMail, Konqueror and Noatun, so where are they?  \n\nI think it must only count apps launched directly from the KMenu, since I usually use kicker buttons or Alt+F2 to launch apps.\n"
    author: "LMCBoy"
  - subject: "Re: One question"
    date: 2003-05-15
    body: "It counts only apps lauched from K menu. I intend to extend this to Alt+F2 for KDE 3.2."
    author: "binner"
  - subject: "KDE 3.2 Schedule ?"
    date: 2003-05-14
    body: "hi folks, any insight on the 3.2 release schedule ? There is a dummy page at http://developer.kde.org/development-versions/kde-3.2-release-plan.html , but with no useful info at all as for today (they will update it some time I am sure)\n\nThanks !"
    author: "MandrakeUser"
  - subject: "Re: KDE 3.2 Schedule ?"
    date: 2003-05-15
    body: "Read this discussion: http://lists.kde.org/?t=105267003200001&r=1&w=2"
    author: "binner"
  - subject: "Re: KDE 3.2 Schedule ?"
    date: 2003-05-15
    body: "Thanks binner, I read part of it, it really is a long thread. All I could grasp is that most developers feel like there is still some considerable way to go before the first alpha. The current CVS head can corrupt user data, and this is unacceptable. I agree :-)\n\n\nCheers !"
    author: "MandrakeUser"
  - subject: "gcc 3.3"
    date: 2003-05-15
    body: "Can KDE_3_1_BRANCH be compiled with gcc 3.3? I tried with 3.4-cvs a while a go and got a whole lot of errors."
    author: "cbcbcb"
  - subject: "Re: gcc 3.3"
    date: 2003-05-16
    body: "Yes."
    author: "Anonymous"
  - subject: "Re: gcc 3.3"
    date: 2003-05-18
    body: "Possibly with the right gcc options, but Qt definitely won't build with gcc 3.3 and -O3. gcc miscompiles uic, which means that the rest of the build fails. I don't know whether the main Qt libs are usuable or not.\n\nApparently, miscompiling with -O3 isn't a serious bug for gcc.\n"
    author: "mathpup"
  - subject: "Re: gcc 3.3"
    date: 2003-05-31
    body: "But it worked with gcc-3.2.3!\nI compiled kdelibs with -O3 and it crashed! Is this a bug a la \"thread synch problem due to optimization\" or a gcc bug?\n"
    author: "Ruediger Knoerig"
  - subject: "Small remarks."
    date: 2003-05-16
    body: "Thank you for this KDE traffic.\n\nI think this Ingo \"Klcker\" and Ingo =?iso-8859-1?q?Kl=F6cker?= are very much related. Somebody should include URL decoding in the statistics engine.\n\nThe introduction of every summary is:\n[ title ]\n[date][ link to archive ]\n[ editor ]\n[ posters ]\n\nIt would be nice to also include the name of the mailing list on which those thread have been happening.\n\n "
    author: "Philippe Fremy"
---
KDE Traffic #50 <a href="http://www.kerneltraffic.org/kde/kde20030511_50.html">has been released</a>. This month it includes information about improvements to <a href="http://bugs.kde.org/">bugs.kde.org</a>, KWord patches, OpenOffice filters and a forthcoming PDF import filter. In addition it adds coverage of two more <a href="http://lists.kde.org/">mailing lists</a>: kde-pim and kde-devel.
<!--break-->
