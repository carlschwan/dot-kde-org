---
title: "Kastle 2003: KOffice Developers' Meeting Report"
date:    2003-08-27
authors:
  - "ltinkl"
slug:    kastle-2003-koffice-developers-meeting-report
comments:
  - subject: "KJSEmbed hits the big time"
    date: 2003-08-26
    body: "Nice to see you've decided to use KJSEmbed. Feel free to make feature requests.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-26
    body: "Would it be difficult to extend Kjsembed to more than just ECMAscript? \nIs the introspection of QT stuff separated from the KJS binding?  \n\nIE Can we look forward to python scripts in Koffice? ;-)\n\nAnother interesting point would be to expose all these bindings via DCOP for interprocess scripting .. or is this already possible? \n\n"
    author: "rjw"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "It would certainly be possible to write an equivalent for python, but I have no intention of making kjsembed itself language independent. I think the disadvantages of having a mixture of languages used by different scripts out weighs the pretty minimal gain of supporting more than one. After all, once you support python, why not perl, ruby, scheme etc.\n\nIan Geiser is working on DCOP support for kjsembed, I'm not 100% sure how much is working right now.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "it can now crash the remote app :)\n\nthe dcop command is getting through but something is going wrong communicating variant types.  lastly i have started on adding the ability to create dcop interfaces in a kjs script but this code needs to be rethought.\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-29
    body: "I'm very interested in this to see how it may be useful for Kommander, which provides language neutral dialogs with optional scripting. I do my best to follow it, but if you could keep me advised that would be great. As much as I think KJSEmbed is great and a generally good choice for this I think that being able to shell out from an app and run Kommander with DCOP and the option of using any scripting language is worth having too.\n\nAlso congrats to Rich on this and all his hard work."
    author: "Eric Laffoon"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-29
    body: "It seems insane to me to want people to rewrite the scripting language independent parts for any other language they want to use. \n\nThe point is, we are going towards a mini-ide being accessible from KOffice. How much of this is js specific? \n\nThe other thing is, if we are going toward OOo, and want to import MS office files, why would you want to make it harder to support (shock, horror) VB properly? (Maybe via mono, or something). \n"
    author: "rjw"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-29
    body: "Yes, python scripts in KOffice are possible. I already have a \"Prove-Of-Concept\"  plugin for KSpread on my hard disc, but it is outdated and doesn't work with 1.3. Maybe I will look into this again, once I get back to develop on KSpread in mid of September."
    author: "Norbert"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2006-06-07
    body: "If there would be a poll, I would vote for python. :)\nJavaScript is nice, but it's OOP features seem a bit hackish.\nI like python and python is very commonly used."
    author: "panzi"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-26
    body: "The planned features sound well, but using JavacSript just sucks."
    author: "Carlo"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "Why does it suck?\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "I think you're just running up against one of the many python advocates.   Python seems to be very popular amongst KDE users and as far as many are concerned, if it only had python they'd be pretty happy.   Eric Laffoon has had a lot of discussions on the dot about this and his competing vision about scriptability.\n\nNote that I'm a big fan of python too.   It's my favorite language, too.   But I do appreciate the work you are doing on kjsembed."
    author: "TomL"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "I like python too, but i don't think it's the right language to use here. It is a lot larger than KJS and much more complex. It would be slower to load, add considerable memory overhead etc. (bear in mind that most apps already have kjs in memory through kdeinit). It is also harder to lock python down for security (and no bastion is not good enough for this).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-29
    body: "> I think you're just running up against one of the many python advocates. Python seems to be very popular amongst KDE users and as far as many are concerned, if it only had python they'd be pretty happy. Eric Laffoon has had a lot of discussions on the dot about this and his competing vision about scriptability.\n \nAnd please, if you're going to mention me at least through in a few words about what I'm saying. I don't use Python personally. I also don't think my vision competes per se with Rich's. I like to think they are more complementary in application, but I do believe that the greatest level of user support is achieved by a language neutral approach. It will see the widest acceptance and least complaining about languages. It will not however have all the inherent power of KJSEmbed.\n\nThe bottom line is that people don't usually want to learn another language to do something when they are proficient in a different one they prefer. This means some people will avoid using these extentions, and not just the zealots. If you think about it, it's not difficult to imagine having to use a handful of scripting languages with a number of applications. Whatever the benefits of a particular language are it is difficult to outweigh user resistance and saturation of frustration.\n\nFWIW I happen to like Javascript, I think KJSEMbed is cool and I salute Rich in his work. ;-) I'm also working on Kommander as a language neutral solution because I believe it has a somewhat different target audience. (Plus we've had it for some time now.) Choice is good."
    author: "Eric Laffoon"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "You are the wrong person to address it to. But I wish the KOffice developers would rather pick Python. This will give a more feature rich language with a lot of modules to make use of other programs.\n\nBut you can probably tell us if KJSEmbed has an API interface that e.g. KPythonEmbded could also provide.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "I just don't like the language. I admit that I'm not impartial - it's more a personal thing. I just don't see a reason to use it. JavaScript has no advantage to other languages imho."
    author: "Carlo"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "It has the very nice feature of a prototype-based object system, which IMHO is much easier to understand than the class-based object system that is used by all other popular programming languages. It is also the only mainstream scripting-level language which uses a C-like syntax, which is quite nice for many programmers (but unfortunately reduces its mainstream-appeal a little bit).\n"
    author: "Tim Jansen"
  - subject: "Re: KJSEmbed hits the big time"
    date: 2003-08-27
    body: "begin\n  write('prototype-based oo: to be as flexible as a spaghetti?!');\nend;\n\nscnr ;-)\n\n"
    author: "Carlo"
  - subject: "Great ida!"
    date: 2003-08-26
    body: "I am totally for using the OO.org format, it has tremendous advantages.\n\nThe more applications usinte and use the format the stronger we will bea gainst MS's and it also allows greater compatibiltiy with our own appliations and other platforms."
    author: "Alex"
  - subject: "Re: Great ida!"
    date: 2003-08-27
    body: "It fits greatly in the long term world domination scheme, don't it? ;)"
    author: "AC"
  - subject: "Re: Great ida!"
    date: 2003-08-27
    body: "I'm not so happy to hear this. As someone who has done his share of import filter implementation, I've learned that the file format reflects the features of a program. If koffice adopts the OO-format, I'm afraid it might set an upper limit to what features will be possible to implement in koffice. koffice will never be able to go beyond what OO provides, featurewise (at least when it comes to features that require support from the file format).\n\nThe two main problems with doing import filters are:\n1. The importing program lacks features the exporting program has.\n2. The features match, but are implemented differently (the file format often reflects the implementation details).\n\n1 can't be solved, unless the missing features are implemented.\n2 can often be solved, but it is often trickier than expected.\n\nTo those who claim XML solves the problem by being extensible: If the file format is extended to let koffice implement features not present in the original OO-format, both problem 1 and problem 2 above will show up, and you have gained nothing by using the OO-format. (One of the formats I wrote filters for actually was an XML-format, and I had both problems.)\n\nMy suggestion: Keep the koffice format, or change it to suit your needs, but don't depend on others. Make sure everything is properly documented, and write the necessary filters.\n\nI'm afraid the whole file format mess will persist. The problem is not that each program has its own format (that's the way it should be IMHO), but rather that users distribute files in formats that were intended to be used when writing the document. They should instead distribute the files in a format that was created for distribution purposes (e.g. pdf, html, rtf). I realise that this is not always possible since there doesn't exist that kind of format for every application domain.\n\nSorry if I'm ranting ;-)\n\n/Magnus"
    author: "Magnus Johansson"
  - subject: "Re: Great ida!"
    date: 2003-08-27
    body: "\"They should instead distribute the files in a format that was created for distribution purposes (e.g. pdf, html, rtf). I realise that this is not always possible since there doesn't exist that kind of format for every application domain.\"\n\nHello Magnus!\n\nThe problem is that in the real world, users can't possibly always do this. My entire office has years and years of (perhaps gigabytes) of MS Word proposals, hundreds of PowerPoint presentations, and a great deal of MS Excel spreadsheets. There are also a lot of very old WordPerfect documents, but those are from the earlier 90's.\n\nWe have a enormous investment in MS Office, and currently, 100% of the office machines have MS Office loaded in them. When people in our office need to take home, 99% of the time, it's not a problem because they have MS Office at home.\n\nThe problem is for the other (growing) 1%, like me. It's a huge barrier to switching to some alternative OS like Linux without having compatability with existing documents, and being able to effectively communicate with others (which means, the ability to export MS Office docs is important too)\n\nIn fact, I was pretty much a hobbist with Linux until OpenOffice came along. Since then, I've been able to switch to Linux full time at home. I've also been using Koffice for years, and I love it's interface compared to OO's crappy interface, but I couldn't really use it for anything serious. And I still can't until there is a way to natively open MS documents (wether it be through OpenOffice, I don't care..)\n\nTo me, Koffice doesn't really need to have any more features than OpenOffice.. it just needs to provide a better interface than the crappy java OpenOffice interface."
    author: "Luther John"
  - subject: "Re: Great ida!"
    date: 2003-08-27
    body: "\"The problem is for the other (growing) 1%, like me. It's a huge barrier to switching to some alternative OS like Linux without having compatability with existing documents, and being able to effectively communicate with others (which means, the ability to export MS Office docs is important too)\"\n\nI agree with pretty much everything you say, but IMHO the solution is not koffice swithing to the OO-format, but rather work on the import/export filters.\n\nI don't think it's wrong if they base the koffice format upon the OO one, but if they restrict themselves from extending and/or modifying it to suit their needs, they're making a mistake IMO.\n\n(Disclaimer: I haven't looked at either file format, but AFAIK they're both based on XML. I have, however, done similar work in another application domain.)\n\n/Magnus"
    author: "Magnus Johansson"
  - subject: "Re: Great ida!"
    date: 2003-08-28
    body: "David Faure is a member of the OASIS group that works on the standard, so I'm pretty sure he will cater for koffice's needs. :)"
    author: "Peter Simonsson"
  - subject: "Re: Great ida!"
    date: 2003-08-28
    body: "Nobody has said that there will be no private extensions ot the OO format (OASIS being a public extension compared to OO version 1.0) But I think that we need long before we even get to that border.\n\nAs for working on import/export filters, sorry, but when there is not any programmer, you cannot do much. (Volunteers, email to koffice@kde.org please.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Great ida!"
    date: 2003-08-28
    body: "I have some problems to follow.\n\nIf we have decided to change our file formats, it is because we do not want to keep the old ones.\n\nSo I find that it is better to take a good foundation like the OO format and to build on it, instead of creating again private file formats. Sure private extensions (not necessarily from KOffice) will surely exist, they exist too in RTF. But even in such a document, you can read most of it (instead of nothing.)\n\nAs you have written, using XML does not give the solution by itself. Yes, Abiword's file format is not KWord's is not OOWriter's. That is exactly our problem. That is why we try to find a solution.\n\nKOffice just cannot remain an isolated solution, not even (or barely) available on Windows.\n\nDocument the file formats? Writing filters. especially for other applications? Nice, but where do you get the manpower? Someone in AbiWord's bugzilla (#1144) asked if a KOffice developer could help with their KWord filters. What can we answer for now? Only: \"no, sorry, no people!\" (I have not even dared to give this answer yet.)\n\nBy the way: if people want to volunteer, please contact: koffice@kde.org\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: Great ida!"
    date: 2003-08-28
    body: "\"So I find that it is better to take a good foundation like the OO format and to build on it, instead of creating again private file formats. Sure private extensions (not necessarily from KOffice) will surely exist, they exist too in RTF. But even in such a document, you can read most of it (instead of nothing.)\"\n\nFrom the article I got the impression that you wanted to use the OO format as it is, not as a base. If you are not happy with the current format and want to switch anyway, and don't mind adding to the OO format, I agree that you should change.\n \n\"By the way: if people want to volunteer, please contact: koffice@kde.org\"\n\nI probably will at some point, but it will (likely) not be file format related work I volunteer (I've done enough to know that I don't like it :-). Until I find the time needed to actually contribute, I lurk on the mailinglists and share my thoughts on things I've some experience with and hope that it will be useful to someone.\n\nRegards,\nMagnus"
    author: "Magnus Johansson"
  - subject: "Re: Great ida!"
    date: 2003-08-28
    body: "In fact we plan to switch to the OASIS format, which is based on OO. As long as David Faure is part of OASIS, it is better to try to make any extension part of OASIS. That is why I suppose that nobody is talking about private extensions.\n\nThank you to have thought about contributing! If you know so much about file formats, perhaps you could help use with the filter pages ( http://www.koffice.org/filters .) You would need to know about XHTML 1.0, PHP 4 and perhaps XSLT 1.0. So perhaps you could use your knowledge.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Great ida!"
    date: 2004-08-10
    body: "Based on what I've read, OASIS is NOT \"the OpenOffice\" format. OASIS is an independant organization dedicated to creating an open file standard for commonly used business applications. It just happens that OpenOffice.org adopted it first. But I don't think that necessarily places any restrictions on what KOffice will or will not be able to do with the formats. What it does is levels the playing field and places emphasis on chosing, say, a word processor based on what it will do for the user, rather than having to worry about who will be able to read the file type, an idea I'm totally in favor of. This could be the one thing that helps to break the office suite monopoly that Microsoft has such a strangle-hold on. Just remember that in the audio world, the MP3 file type has been embraced completely; no one complains about limitations, and everyone uses which ever recording or playback application they choose. The result is an audio file that can be played back on just about any kind of software or hardware available. I firmly believe the same will happen with office documents with the adoption of the OASIS standard.\n\nAnd that will put KOffice in a decided advantage over OpenOffice, which is huge and relies too heavily on Java.\n\nBravo, KOffice team...keep up the great work."
    author: "Chris Evans"
  - subject: "Re: Great ida!"
    date: 2004-08-10
    body: "Just a little note: the OASIS open office format is based on OO's file format, even if at the end it differs in many details.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Status of Wv2 ?"
    date: 2003-08-26
    body: "I'm wondering what is the status of the new import / export MsWord library that Werner Trobin is working on ? \nImporting seems to work already quite well but is exporting still a distant dream or can we expect at least basic exporting in a non-too-distant future ?\n\nCheers,\nCharles\n\nPS: switching to Oo formats is a very intelligent move"
    author: "Charles de Miramon"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-26
    body: "Well, I dropped the idea of creating the export code. One reason is that it's just an insane amount of work and I'm only one person with a very limited amount of time. The second reason is, that it's possible to express 100% (yes, really 100%, just read the RTF spec on MSDN) of the MS Word features using RTF. Due to that I'd rather help with RTF export than waste my time on direct .doc export.\n\nCiao,\nWerner"
    author: "Werner Trobin"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-26
    body: "Thanks for answering...\n\nIf the RTF export filter is going to be the future path for exporting to MSWord. Could KOffice developers implement the 'Abiword trick' : id est having a .doc exporting filter whih is a rtf file with a .doc extension. I guess, it is rather easy to code and will make life easier to users that don't know what is RTF (or Rich Text Format)and don't want to know.\n\nFocusing on the RTF export filter is definitely the best option. (Power)-users can much more easily create test cases, look the exported rtf code, try to understand where it went wrong and help the developers. Something you can't do as easily with the binary .doc format.\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-27
    body: "RTF as .doc had been discussed in the past but never implemented\n\nHave a nice day!"
    author: "Nicolas GOUTTE"
  - subject: "Abiword Trick RTF renamed to DOC"
    date: 2003-08-28
    body: "I should point out that the \"Abiword Trick\" has good precedent.  \n\nMicrosoft Wordpad does not export Binary .DOC either, it only exports RTF renamed to .doc\n\nI also recally Wordperfect and Microsoft using the .doc file extension fairly arbitrarily for various formats including Text and RTF.  \nThere are so many version of DOC anyway that quite quickly you need a riguorous file detection algorithm.  \n\n\nWhile I am here cheerleading for Abiword I may as well mention that it would be really great if KDE users could take a look at making Abiword blend in better with  KDE, I am reliably informed that Abiword follows the freedesktop.org icon specification and should be able to use KDE icons when run in KDE.  "
    author: "Alan H"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-26
    body: "I never knew you could get OLE streams into an RTF file. How is this done? "
    author: "rjw"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-27
    body: "You can embed an ole stream in a RTF file, for example a Excel datasheet in a Word document with the \\object tag.\n\nIMHO, embedding one application into another in MSOffice or KOffice is a nice technological trick but is a recipe for ugly looking printed documents because the two applications use different typographical engines, or different fonts. It is also a hog on ressources on a slow computer. It is much more useful, to have intelligent cut&paste for example a wizard to transform a KSpread table in a KWord table.\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-27
    body: "This is all very well until you want to edit the embeded object.  For example, making an invoice: I want the spreadsheet for doing all the formulaeic work and the word processor for putting some nice headers and footer and any covering text in.  And then I want that to be a template so I can use it again.  As far as I can tell, intelligent cut and paste just won't cope with that sort of job.  \n\nObviously the option to intelligently remark the is nice to have available but I don't think that should preclude the addition of an embedded facility."
    author: "Andy Parkins"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-27
    body: "Well, RTF-export is nice. However, there don't seem to be many people around understabnding that code. I tried sending different people code to let the rtf-filter handle tables with only 1 line needing to be changed... I got the answer that nobody had the time/knowledge so that tables will be disabled in rtf-export for 1.3.\nI hope that changes some time in the future."
    author: "johannes wilm"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-28
    body: "Can you re-send that one line of code? (Or point to where the patch is on http://list.kde.org .) I am interested about it.\n\nI am sorry but I was out of \"business\" for a few months, so I have missed your patch.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Status of Wv2 ?"
    date: 2003-08-29
    body: "Hi again,\nwell my HD has broken down since then, but the newest patch I send I could find on koffice-devel is at http://lists.kde.org/?l=koffice-devel&m=105756234418255&w=2 . \nThe line that needs to be changed is the one starting with \"FrameData frame =\" ."
    author: "Johanneswilm"
  - subject: "depending on Qt 4 ?"
    date: 2003-08-26
    body: "i.e. the next koffice version (1.3) will be the last which works with KDE 3, and KDE 4 ain't even scheduled yet (AFAIK)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: depending on Qt 4 ?"
    date: 2003-08-26
    body: "I'm sure there will be 1.3.x releases as necessary."
    author: "ac"
  - subject: "Re: depending on Qt 4 ?"
    date: 2003-08-26
    body: "The talk is that Qt 4 will be release middle of next year and that KDE 4 will be the next release after 3.2.  At least that is what I have been hearing today.\n\nStrid..."
    author: "Strider"
  - subject: "Re: depending on Qt 4 ?"
    date: 2003-08-27
    body: "Well, you can see the need in two ways:\n- KDE 4.0 will need a KOffice version, so why not a new one.\n- the new Koffice version will need time to be done (for example file format switching) and could improve due to Qt4 (for example in KoText, the text engine.)\n\nHave a nice day!"
    author: "Nicolas GOUTTE"
  - subject: "QSA and kjsembed"
    date: 2003-08-26
    body: "I wonder what's the difference between QSA and kjsembed.\nI know kjsembed adds some nice features, but else it's compleat the same? Eaven the cpp source? If that's true, why is kjsembed LGPL and QSA GPL?\nI'm confused.\n\n(And when will KDevelop support JavaScript?)"
    author: "panzi"
  - subject: "Re: QSA and kjsembed"
    date: 2003-08-27
    body: "Maybe this can help:\nhttp://xmelegance.org/kjsembed/kjsembed-qsa.html"
    author: "Vajsravana"
  - subject: ":-O"
    date: 2003-08-26
    body: "looks like this has been read: http://www.mind.lu/~yg/office.html\n\n... ;-)\n\nso now we gotta convince Abiword and Gnumeric developers..."
    author: "yg"
  - subject: "Re: :-O"
    date: 2003-08-27
    body: "A little wrong here though. I think Gnumeric beats OOo calc anyday. It is really quite amazing. I do wich they could use the same file format though. That way, they could probably cooperate to use the same export filters too."
    author: "Maynard"
  - subject: "Re: :-O"
    date: 2003-08-27
    body: "Much of what: http://www.mind.lu/~yg/office.html says is simply wrong.\n\nWhat does it mean that you have to \"deal with Microsoft Office (MSO) files\"?  Does this mean that you have to be able to open them in an application so that you can edit them, or does it mean that you just have to read them somehow?\n\nThis is an important distinction.  Does this \"you\" really need import filters or just a way to view the file?  It would appear to me that it would be MUCH simpler to make (or reverse engineer the MS one) a file viewer than to make an import filter.\n\nAnd the printer example is even dumber.  Hasn't he heard of PDF.  Which also means that you don' have to ask somebody to send you a \"weirdo-0.00001%marketshare.xxx format???\" file, you need to ask them to please send you a PDF.\n\n--\nJRT\n\n\n "
    author: "James Richard Tyrer"
  - subject: "Re: :-O"
    date: 2003-08-28
    body: "> And the printer example is even dumber. Hasn't he heard of PDF. Which also means that you don' have to ask somebody to send you a \"weirdo-0.00001%marketshare.xxx format???\" file, you need to ask them to please send you a PDF.\n\nMost windows users don't know how to make pdf's. They are pretty uncommon in the buisness world compared to .doc's. Having MS Office compatability is *wayyyyyyyyyyyyyyy* more important for most users of koffice than having good pdf compatability, although it'd be nice to have both :-)!"
    author: "anon"
  - subject: "Re: :-O"
    date: 2003-08-28
    body: "> Most windows users don't know how to make PDFs\n\nI would go farther than that, I would say that most Windows users don't know what a file format is.  If they did, they would have probably sent you an RTF file.\n\nBut, that doesn't mean that products should be designed based on user stupidity.  It is my belief that Linux should not be dumbed down to accomodate stupid users because such users have no reason to switch to Linux.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: :-O"
    date: 2003-08-28
    body: "> It is my belief that Linux should not be dumbed down to accomodate stupid users because such users have no reason to switch to Linux.\n\nOk, but the world doesn't work like that. I'd get fired if I refused to take MS Office formats and work with them, and demanded non-company-standard file formats. Remember that Linux users represent less than 1 or 2 percent of the desktop."
    author: "anon"
  - subject: "Re: :-O"
    date: 2003-08-28
    body: "I think that you missed the point.\n\nI presume that you mean that at work you use MS Office as your office suite.  This is not the issue since if your company used WordPerfect Office, you would be using WPO file formats.  If you used Foo-Bar Office, you would be using FBO file formats, etc.\n\nThe question is not about documents you are working on, it is about *completed* documents.  Specifically about the bad habit of sending people completed documents in DOC files rather than RTF files (as I think MS originally intended) or converting to PDFs.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: :-O"
    date: 2003-08-28
    body: "> Much of what: http://www.mind.lu/~yg/office.html says is simply wrong.\n\nThat is your opinion.\n\n> What does it mean that you have to \"deal with Microsoft Office (MSO) files\"?\n> Does this mean that you have to be able to open them in an application so that \n> you can edit them, or does it mean that you just have to read them somehow?\n\nEdit them (which includes saving without screwing them) and print them.\n\n> This is an important distinction.\n\nNot in business. Ask your CIO if he would give green light for a new Office suite that can only read MSO files... dunno if he would like it...\n\n> And the printer example is even dumber. Hasn't he heard of PDF. \n\nsure I have.\nI work for a service company, and when a customer sends me a document I am not\nreally in the best position to tell him \"buddy, I need a pdf\".\nCoz I would be the only 1 to tell him \"I cannot handle M$ Office files\".\nAll the other IT companies he deals with don't bother him.\nAnd you know sometimes I get documents I have to edit and send back.\n\n\nI wrote this document http://www.mind.lu/~yg/office.html with users in mind."
    author: "yg"
  - subject: "Re: :-O"
    date: 2003-08-29
    body: "> Edit them (which includes saving without screwing them) and print them.\n\nIIUC, people from outside your business, send you DOC files and you have to edit them.  Is this the norm, or just a small percentage of the cases.\n\nPrinting isn't an issue, a viewer can print if you have the fonts.\n\n>  Ask your CIO if he would give green light for a new Office suite that can\n>  only read MSO files... dunno if he would like it.\n\nNot \"read\" but rather view them.\n\nMy CIO understands that it is a LARGE security problem to exchange DOC files, and I use MS WordViewer to view them.\n\nActually, HOWEVER, it is *reading* DOC files which is the problem, many wordprocessors can write perfect DOC files (because they don't use the cryptic \"features\").\n\n> And you know sometimes I get documents I have to edit and send back.\n\nEdit, or just mark up?\n\nWhat I am saying -- my point -- is that a viewer, like MS's viewer that could view and print (this should include print to PDF and FAX) DOC files would solve many of the issues without the problems of an import filter.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: :-O"
    date: 2003-09-05
    body: "Reading MS Word file is not really a problem. The problem is the automatically executed macros. KWord has not any macro inside documents. So no need to worry about that!\n\n(That is the same reason why some people prefer RTF over native MS Word documents.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: :-O"
    date: 2003-08-28
    body: "Sorry but where do you see that a file viewer would be easier than an import filter? For both you have to \"dissect\" the file format.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: :-O"
    date: 2003-08-29
    body: "The disection is done by a library:\n\nhttp://wvware.sourceforge.net/\n\nTo view the document all you need is sufficent information to \"paint\" the screen.  You do not need any information about the document's structure.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: :-O"
    date: 2003-09-05
    body: "And how can you paint without browsing in the document structure? (I suppose that you want a little more than just the raw text.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "KOffice separated from KDE ?"
    date: 2003-08-27
    body: "KOffice plans to switch to Qt4. But KDE has not planned this yet, as far as I  know. This means that future version of KOffice will be independant of KDE ?\n\nAlso, beware that such huge rewrites have killed more than one project. Gnome has been stopped during two years just to switch to Gnome 2 and the same situation occured during the KDE1 KDE2 switch."
    author: "Philippe Fremy"
  - subject: "Re: KOffice separated from KDE ?"
    date: 2003-08-27
    body: "> \"Also, beware that such huge rewrites have killed more than one project. Gnome has been stopped during two years just to switch to Gnome 2 and the same situation occured during the KDE1 KDE2 switch.\"\n\nBut not for KDE3...\n "
    author: "capit. igloo"
  - subject: "Re: KOffice separated from KDE ?"
    date: 2003-08-27
    body: "I do not thing that it means that KOffice is going its own way. It only menas that Koffice 1.4 will be delayed until KDE 4.0 is out. And I do not think that it is a waste of time, as there is enough bugs and wishes for KOffice.\n\nI do not really understand your comment about the huge re-write. In any case, we do not have any Qt4 (alpha, beta...) right now. So we will have to do the file format switch first and then the Qt4 switch. I still see an opening for a KOffice 1.4 for KDE 3.x if there is really a need.\n\nHave a nice day!"
    author: "Nicolas GOUTTE"
  - subject: "I'm all for it!"
    date: 2003-08-29
    body: "Sure a rewrite might take a while but that's where innovation really comes from, luckily Linux does not have sucha  big user base so we can still afford to break compatibility. One of Microsoft's greatest weaknesses IMO is that they can't break compatibility any time soon.\n\nAlso, IMO the advanatages gained from the KDE 1->2 rewrite or GNOME 1->2 rewrite far out weigh the consequences.\n\nPlease break compatibiltity if it yilds large advantages. KDE 4 is not a point release, you're meant to break compatibiltiy. but please don't break compatibility between X.X and X.X.X releases. However, KDE 4 will be a X release and we need to let 3.x live longer anyway so the time required to make a rewrite will not be a problem."
    author: "Alex"
  - subject: "Re: I'm all for it!"
    date: 2003-08-29
    body: "KDE 4.x will *not* be a rewrite. KDE has kept the same basic architecture since 2.x, and IMHO its a good one. It should be good for at least a couple of more full releases. Stuff that breaks compatibility might be useful, but a full rewrite would just be a waste of perfectly great code. Plus, a large amount of what KDE needs to work on now is not the internal technology (except for maybe replacing aRts!) but UI polish. "
    author: "Rayiner Hashem"
  - subject: "Why not use the OpenOffice MSOffice filters?"
    date: 2003-08-27
    body: "I know it would involve some work getting these filters into a reasonably independent state from OOo, but at least this way you would have both an import and an export filter. I'm sure that the OOo and Abiword people would be interested in this.\nAlso since all of the important office suites for the desktop would be using the same filter code, it can only improve the filter. \n My 0.02 c\n\nCPH"
    author: "CPH"
  - subject: "Re: Why not use the OpenOffice MSOffice filters?"
    date: 2003-08-27
    body: "Sorry, but I doubt that it will be the same code. All three word processors use very different libraries. (See also the many discussions on the koffice mailing list about why we do not take OO's code.)\n\nHowever what can be done is to create libraries to ease the programming of filters. That what has been done for the MS Word import.\n\nHave a nice day!"
    author: "Nicolas GOUTTE"
  - subject: "Re: Why not use the OpenOffice MSOffice filters?"
    date: 2003-08-27
    body: "Hi,\n\nthe switch to the ooo fileformat offers us the possibility to use openoffice as a nice import filter for ms office files. switching our internal file format is a lot of work, but trying to separate the openoffice filters out of openoffice is only time-consuming, and not probale to work out at all.\n\nopenoffice is able to be scripted, and transforming a ms .doc file into a .sxw file is as simple as taking a template document containing a transform-script, executing openoffice with that script file from the command line.\n\nafter that you can simply open that sxw file, either with an openoffice import filter, or in the future you will be able to simply open it as it is then the native format.\n\n(any people interested in hacking on this are very appreciated :-)\n\nHolger"
    author: "Holger Schroeder"
  - subject: "Re: Why not use the OpenOffice MSOffice filters?"
    date: 2003-08-27
    body: "I'm sure everyone would love to do this, but the OOo MSOffice filters are _very_ linked to OOo specific code, and not only that, it's a plain mess to work with (not that it isn't a wonderful piece of technology, but after years of work, and I can imagine only a few people working on it, it's gotten really obfuscated) :)"
    author: "anon"
  - subject: "Re: Why not use the OpenOffice MSOffice filters?"
    date: 2003-09-05
    body: "I am not sure what this forum is for (KDE?) but I do care about openoffice filters.\nJust for your information, there is a little application using openoffice macros to convert files and fired with a command line through openoffice. If you want to check this try at http://oooconv.free.fr/ . It is quite fast (the time for openoffice to open then convert, save and close), but it would be faster if someone can extract the filters code. I do not know if the related Ooo filters code is bound to Ooo specific code and I do not really want to jump into openoffice source code to verify this. Anon, Have you got any links which explain this very precisely or did you verify it yourself? Thanks for replying.\n\nMaiko "
    author: "Maiko"
  - subject: "Re: Why not use the OpenOffice MSOffice filters?"
    date: 2004-12-14
    body: "Can you compare OpenOffice with MSOffice ?"
    author: "Tiep"
  - subject: "Whats up with the KDE.org home page?"
    date: 2003-08-27
    body: "Did anyone notice it yet? http://www.kde.org takes me to a page which protests against software patents.\n\n"
    author: "defaced"
  - subject: "Re: Whats up with the KDE.org home page?"
    date: 2003-08-27
    body: "http://dot.kde.org/1061880936/1061942225/1061976407/"
    author: "whatever"
  - subject: "Will printing in Karbon14 fixed before KOffice 1.3"
    date: 2003-08-27
    body: "I only get \"wire frame\" printings and my nephew is not very excited...;-)\n\nTry printig preview.\n\nThanks for GREAT work!\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "KWord printing"
    date: 2003-08-27
    body: "I see nothing about fixing the 'bassakwards' WYSI-not-WYG printing in KWord.\n\nIs this a priority?  Is work being done on it?\n\nI realize that part of the problem is Qt::qpsprinter, but this has to work!\n\nA wordprocessor that doesn't print correctly is useless for most purposes.\n\nAs was discussed on <koffice-devel@kde.org>, the current system tries to duplicate on paper what is displayed on the screen (it FAILS, but even if it succeded it would not be good) rather than making a very good (but not perfect) lower resolution +/- one pixel representation on the screen of (exactly) what will appear on the paper.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KWord printing"
    date: 2003-08-28
    body: "I suppose that this is a reason of the switch of KOffice 1.4 to Qt 4.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KWord printing"
    date: 2003-08-29
    body: "No.  It isn't necessary to switch to Qt-4 to rewrite the internal representation of the \"painted\" image.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KWord printing"
    date: 2003-08-29
    body: "I have not written that it is necessary.\n\nI have just meant that if KoText can be designed on top of Qt4 instead of being a replacement of the corresponding classes, then it would be easier to fix bugs and to use Qt's improvements (like Indian scripts.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "KWord - horrible Table support"
    date: 2003-08-28
    body: "I have benn using koffice(eg kword and kspread) for some time now for really small things.\n\nkspread is really useful and works well. thumbs up.\n\nkword is very nice for simple \"word\" documents, but the table support is so bead it makes my head split every time i use it :\n\n- You never know what your table looks like if you save your document and load     it again.\n-cells are lost\n-contents of cells is gone\n-you cannot select a table *\n-you cannot move a table around  *\n-you cannot resize a table *\n*=these points work somehow, but its so bad no one will ever be able to use kword tables ;-) you really have to rethink the way tables are handled in kword.\n\nMfg Christoph "
    author: "Christoph"
  - subject: "Article on Slashdot"
    date: 2003-08-28
    body: "This article has been reproduced in Slashdot\nhttp://developers.slashdot.org/article.pl?sid=03/08/27/2223227&mode=nested&tid=106&tid=121&tid=185&tid=189"
    author: "MandrakeUser"
  - subject: "OpenOffice.org file format"
    date: 2003-08-28
    body: "This is excelent news!!! I always wished KOffice was compatible with OpenOffice.\n\nCongratulations to the KOffice team for this decision!!!"
    author: "a"
  - subject: "Re: OpenOffice.org file format"
    date: 2003-09-17
    body: "Yes.  THis is a great move toward a common file format, a popular standard, and dare I say, a unified front against ... well, ya know, theMS."
    author: "mlitty"
  - subject: "Webpage theme"
    date: 2003-08-29
    body: "Could somebody change te default KDE webpage-style.\nThe Yellow-style looks much better."
    author: "Alex"
  - subject: "Vector drawing!"
    date: 2003-09-01
    body: "Without a really useful vector drawing app the office suite is incomplete.  Karbon14 simply isn't an appropriate replacement for kontour.  Users of drawing apps have simple expectations and karbon14 fails to meet the most basic:  the ability to draw simple lines, polylines, arrows (lines with arrow ends), etc.  Such an ability is in ALL other drawing apps on the planet:  OODraw, SODraw, Illustrator, CorelDraw, KONTOUR, Freehand, MacDraw, etc, etc.  The odd man out here is Karbon14 which has replaced lines with sinewaves and nautilus shells (!?).  What POSSIBLE use outside some very specialized need are these \"lines\"?  Useless for 90%+ of those doing drawings for publications, books, or presentations.\n   Kontour was nearly there.  It had almost everything necessary except a few more export/import filters and stability.  Karbon14, nice in theory, is nearly useless in practice.  You will NOT win officesuite converts by providing some bizarro drawing app that is totally at odds, interface and function-wise, from every single other professional drawing app out there.  Either karbon14 needs a lot of work (someone forgot simple lines?) or it is simply not designed to be a widely useful drawing app and should not be part of the koffice suite.  "
    author: "Praedor"
  - subject: "Re: Vector drawing!"
    date: 2003-09-01
    body: "Agreed... I think karbon is the start of something pretty good, but doesn't handle basic shit. "
    author: "anon"
  - subject: "KPlato?"
    date: 2003-09-02
    body: "What about KPlato? Is it dead?"
    author: "lion_fui"
  - subject: "Kivio? "
    date: 2003-09-05
    body: "I didn't see any mention of Kivio, and I surely hope this doesn't mean it is going away.  I am a big user of Visio, and would love to get away from it just for doing network diagrams.  I have tried Kivio in the past (even purchased some extra templates) but it was just not stable enough for my needs.  I don't want to use the TheKompany version if the free version can really move forward.  Is anyone still working on this project ? (and don't simply tell me I should, I am not much of a programmer really, and unfortunately my time to do Linux stuff is very limited). "
    author: "Anthony"
  - subject: "Re: Kivio? "
    date: 2003-09-05
    body: "For the simple reason that the Kivio maintainer, yes there is a new and active one, didn't attend the meeting."
    author: "Anonymous"
---
Quite a few KOffice developers took the opportunity to meet during the <a href="http://events.kde.org/info/kastle/">Kastle event</a> in Nov&eacute; Hrady, and particularly   to discuss the future of
<a href="http://www.koffice.org/">KOffice</a> after the next major release (KOffice 1.3 slated for the end of September 2003).  A summary of the topics discussed follows.
<!--break-->
<h2>Topics discussed</h2>
<h3>File format</h3>
We will switch to the <a href="http://www.oasis-open.org/committees/office/">OASIS</a>
(OpenOffice.org) file format for all the major applications. This has many advantages:
<ul>
  <li>file format shared with the <a href="http://www.openoffice.org/">OpenOffice.org</a> suite, we don't have to
      reinvent the wheel</li>
  <li>we'll be able to drop our OOo import filters and use the export filters
      as a compatibility layer for older KOffice versions documents</li>
  <li>we can actively participate in the standard file format creation in the
      the case of Kexi and Kugar which are applications that don't currently
      exist in OOo</li>
</ul>
This is also a good opportunity to switch the names of our MIME types to the
<tt>vnd.kde...</tt> naming scheme and to drop our KOffice-specific file
extensions. It's again the users who will benefit from this step, e.g. on
Windows it will be possible to open files created with a KOffice application
using the OpenOffice.org suite.

<h3>KOffice libraries</h3>
We intend to break the binary compatibility soon after the 1.3 release and take
this opportunity to clean up and refactor <tt>kofficecore</tt> and
<tt>kofficeui</tt> libraries.

<p>The next KOffice version will depend on <a href="http://dot.kde.org/1061880936/">Qt 4.0</a> which will have a better rich text engine
that we will reuse. Our text library (<tt>kotext</tt>) will be redesigned as
a thin wrapper around this rich text engine and will provide KOffice-specific
functionality only. (David)
</p>

<p>
The scripting library (<tt>koscript</tt>), currently used in
<a href="http://www.koffice.org/kspread">KSpread</a>
only, will be rewritten from scratch to provide a new light-weight formula parser.
(Werner, Ariya Hidayat)
</p>

<h3>Kexi</h3>
It is still unknown whether <a href="http://www.koffice.org/kexi">Kexi</a>
will be released separately later this year. Otherwise it will definitely
be part of the next KOffice version. The problem is that Kexi's core API
isn't stable enough yet so it makes no sense to do a separate Kexi release
which would depend on already obsolete KOffice libraries. (Lucijan)

<h3>KChart</h3>
Big thanks go to Kalle who finally imported KDAB improvements and fixes into
KOffice CVS.

<h3>Scripting, automation</h3>
We decided to use <a href="http://xmelegance.org/kjsembed/">kjsembed</a></tt>
engine as a foundation for the new KOffice internal
scripting engine. It will allow users to write their own wizards, interactive
forms or scripts using JavaScript and Qt designer forms. Work is also being
done on KDevelop side which will provide an IDE these tasks. (Zack)

<h3>Spellchecking</h3>
Current plans are either to rewrite the current API after KOffice 1.3 and KDE
3.2 releases or to reuse the newly born <a href="http://www.abisource.com/enchant/">Enchant</a>
backend neutral spell checking engine. This architecture will have 3 layers:
different backends, a core library and dialogs. (Zack, Laurent Montel)

<h3>Common canvas library</h3>
The plan is to have a common canvas class that KOffice applications could
reuse for their own drawing needs. Currently there exist different
implementations in
<a href="http://www.koffice.org/karbon/">Karbon14</a>,
<a href="http://www.koffice.org/kpresenter/">KPresenter</a> and
<a href="http://svg.kde.org/">KSvg</a>.
The goal is to have a common library so that also other applications
can draw various shapes without the overhead of KParts embedding.
(Rob, Lukas, David)


<h3>Ongoing work</h3>
The KOffice developers are currently busy fixing various issues so that we
can ship a stable release candidate at the end of this month.

<ul>
  <li>fix spell checking (Zack, David)</li>
  <li>checking that language property is being used for different portions of
      text (David)</li>
  <li>fix the character (font) dialog in KWord and KPresenter (David, Lukas)</li>
  <li>continue KPresenter bug hunting (Lukas)</li>
  <li>discussions about future drawing objects and common canvas functionality
      (see above)</li>
  <li>discussion on table-frameset design (KWord developers)</li>
  <li>Kexi hacking (Lucijan, Joseph, Holger)</li>
</ul>

<h2>Developers present at the meeting</h2>
Luk&aacute;&scaron; Tinkl (release manager, KPresenter)<br>
David Faure (KWord, libraries)<br>
Werner Trobin (libraries, filters)<br>
Zack Rusin (filters, spell checking)<br>
Lucijan Busch (Kexi)<br>
Joseph Wenninger (Kexi, Kugar)<br>
Alexander Dymo (Kugar)<br>
Andrea Rizzi (KFormula)<br>
Thomas Zander (KWord)<br>
Rob Buis (Karbon14)<br>
Holger Schroeder (Kexi)
