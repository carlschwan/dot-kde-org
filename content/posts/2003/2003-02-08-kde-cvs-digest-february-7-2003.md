---
title: "KDE-CVS-Digest for February 7, 2003"
date:    2003-02-08
authors:
  - "dkite"
slug:    kde-cvs-digest-february-7-2003
comments:
  - subject: "Screenies?"
    date: 2003-02-08
    body: "Any screenshots of this Keramic/II thing?"
    author: "Space Cowboy"
  - subject: "Re: Screenies?"
    date: 2003-02-08
    body: "I dont quite get those subtle changes on Keramik/II, I guess changes will be more evident with time, but I have the style from cvs, look at http://garaged.homeip.net/screenshots \n\n1 screenshot every hour\n\nI can take snaps at any aplication that you like and post it to that location too (email me).\n\nI still have the same old bug with windows focus, but everything works great.\n\nThanks for the good work KDE team"
    author: "GaRaGeD"
  - subject: "Re: Screenies?"
    date: 2003-02-08
    body: "Could you please make a screenie of a dropdown box on a website? I'm hoping that they've changed them, because in my opinion they are way too big. :("
    author: "Steffen"
  - subject: "Re: Screenies?"
    date: 2003-02-08
    body: "I agree.. Size does matter, but bigger isn't always better.  Please reduce the size of the dropdown boxes in Keramik II, they hurt!"
    author: "Does Size Matter?"
  - subject: "Re: Screenies?"
    date: 2003-02-08
    body: "I'd like to see them as a rectangle, they shouldn't look like buttons (current state in keramik). A simple box with a 1px margin and an arrow at the right side (I'm thinking of the arrows used in all the scrollbars).\n\nShouldn't that be possible? :)"
    author: "Steffen"
  - subject: "Re: Screenies?"
    date: 2003-02-09
    body: "Maybe this helps to understand what I'm basically thinking about: http://websteffen.bei.t-online.de/keramik_dropdown.png"
    author: "Steffen"
  - subject: "Re: Screenies?"
    date: 2003-02-08
    body: "There are 2 snapshots of menus with keramik/II, i still dont see any difference, but I'm not good with details\n\nIf you want I can make more snaps\n\nCheers\nMax"
    author: "GaRaGeD"
  - subject: "Re: Screenies?"
    date: 2003-02-09
    body: "Thanks!\n\nI can't see much difference myself yet, \nbut they say it will change more with time."
    author: "Space Cowboy"
  - subject: "Re: Screenies?"
    date: 2003-02-09
    body: "We just began with small changes.\nThe  idea is a big slim down\n\nCurrently the tabs are made smaller(also more highlight on active)\nThe combos also.\nPlus we made active lineedits have a highlight color\n(qt rsync needed)\nThe lineedits are also more subtile.\n\n\nJust a beginning (5% done)\n\nqwertz\n"
    author: "qwertz"
  - subject: "Re: Screenies?"
    date: 2003-02-09
    body: "This is really great. For years, I despised all those awfully colorful window decorations and widget themes, but Keramik got style - it uses to much space, though. A slim down is exactly what I want. Cool!"
    author: "Eike Hein"
  - subject: "Re: Screenies?"
    date: 2003-02-09
    body: "5% done? Uh-oh.\n"
    author: "Sad Eagle"
  - subject: "Re: Screenies?"
    date: 2003-02-10
    body: "Dont forget our old TODO list:)\nPlus i want to change more things\n\nq\n"
    author: "qwertz"
  - subject: "Re: Screenies?"
    date: 2003-02-10
    body: "now, this is much better, IMHO, those huge widgets did really look nce to me tall!\nBut i think there is still something missing with the tabs, in particular, i ahte th fact that they look like they are \"sitting\" on top of the page they selct (this is a problem mos styles have with tabs, though), there should be a line, or \"base\", that runs all the way under the tab AND the page, so that the tab really look like it belongs to the page it selects. Liquid does it nicely, and it not only loks better, but it also makes the whole interface easier to understand, again IMO.\n\ngood work!\n\nfred\n"
    author: "fred"
  - subject: "Woohoo! ;)"
    date: 2003-02-08
    body: "Well, one can't say it often enough: Thanks you, Derek Kite. Great work, as usual."
    author: "Eike Hein"
  - subject: "OT: Best Style + Window decoration ever !"
    date: 2003-02-08
    body: "it's called qinx, info here:\nhttp://www.usermode.org/code.html\n\nand screenshot here:\n\nhttp://www.usermode.org/code/qinx.jpg\n\nIMO it should be the default theme, for kde, since it is not so shiny as keramik, and\nit looks like something really new "
    author: "ac"
  - subject: "Re: OT: Best Style + Window decoration ever !"
    date: 2003-02-08
    body: "it (purposefully) looks like QNX/Photon...\n\nhttp://www3.baylor.edu/~Steve_Webb/qnx1.jpg\n\nit is a very nice theme, but it isn't original or something new =)"
    author: "Aaron J. Seigo"
  - subject: "Re: OT: Best Style + Window decoration ever !"
    date: 2003-02-08
    body: ">it (purposefully) looks like QNX/Photon...\n>http://www3.baylor.edu/~Steve_Webb/qnx1.jpg\n>it is a very nice theme, but it isn't original or something new =)\n\nright you are. not new, but very lovely ..."
    author: "ac"
  - subject: "Re: OT: Best Style + Window decoration ever !"
    date: 2003-02-09
    body: "It has a clean but boring look, IMHO. And definitely doesn't look \"new\". :)"
    author: "EUtopian"
  - subject: "Drop Shadows for Window?"
    date: 2003-02-08
    body: "This is off topic of this CVS Digest, but it does have relevance to the CVS. Back during the 3.1 development, I remember seeing drop shadows for windows, finding them missing in the final. Does anyone know what happened and why they were eliminated? Is this feature going to be put back into 3.2? Just curious, and thanks to those who provide any information."
    author: "Paul Kucher"
  - subject: "Re: Drop Shadows for Window?"
    date: 2003-02-08
    body: "I guess you think about the menu-drop-shadows. They are there since KDE-3.1. Just go to ControlCenter->Appearnce&Themes->Style->Effects and turn on Menu-drop-shadows...\n\nIt looks quite nice ;-)"
    author: "Andreas Pietzowski"
  - subject: "Re: Drop Shadows for Window?"
    date: 2003-02-09
    body: "Why don't we have Drop Shadows for tool tips? This should be easier to do than adding drop shadows to menus(?)"
    author: "Peter"
  - subject: "Re: Drop Shadows for Window?"
    date: 2003-02-09
    body: "Because you didn't code it yet? ;-)"
    author: "Andreas Pietzowski"
  - subject: "Re: Drop Shadows for Window?"
    date: 2003-02-09
    body: "No, I know about the menu drop shadows and have been using them for a while. I'm talking about window drop shadows that come off the edges of the windows of the applications."
    author: "Paul Kucher"
  - subject: "Re: Drop Shadows for Window?"
    date: 2003-02-09
    body: "Have you seen any screenshots of that window-shadow\u00ad-feature yet? Do you have an URL?"
    author: "Andreas Pietzowski"
  - subject: "Re: Drop Shadows for Window?"
    date: 2003-02-09
    body: "Check this out:\n\nhttp://www.kde-look.org/content/show.php?content=2779\n\nThis was where I saw it."
    author: "Paul Kucher"
  - subject: "Re: Drop Shadows for Window?"
    date: 2004-02-21
    body: "I was using a kde-redhat build of KDE 3.2 for Fedora Core 1 and it had the window drop shadows.  I upgraded to a newer build and they disappeared.  They were very pretty eye candy, but were slow (they disappeared when dragging windows and re-drew when the window was released.)"
    author: "chemist109"
  - subject: "KMail Spell check on the fly"
    date: 2003-02-08
    body: "I saw this menitioned in the digest.  Can anyone elaberate more on it?  Is it something that will/can be incorperated into the core text edit widget that always shows up in konq? "
    author: "Benjamin Meyer"
  - subject: "Re: KMail Spell check on the fly"
    date: 2003-02-09
    body: "Hi,\n\nI committed the as-you-type spell checking for KMail patch.\n\nThis patch is not very tightly coupled to KMail and can also be used to add as-you-type spell checking to html forms in konq. In fact I already have as you type spell checking working in konq locally and it is doing its thing as I type these words. I can send you the patch if you like.\n\nI've been working on this with Scott Wheeler (KSpell author) and we both seem to think as-you-type spell checking in html forms would be a nice innovation for KDE so hopefully it will be incorporated into cvs.\n\nDon."
    author: "Don"
  - subject: "Re: KMail Spell check on the fly"
    date: 2003-02-09
    body: "Is it possible to steal some code from koffice and underline the wrong word? Now its marked with a simple read color (in kmail).\n\nBye\n\n Thorsten\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: KMail Spell check on the fly"
    date: 2003-02-09
    body: "AutoCompletion for common letter phrases (e.g. \"Regards\", \"Mit freundlichen Gruessen\") would also be a nice feature.\n\nTackat"
    author: "Tackat"
  - subject: "Re: KMail Spell check on the fly"
    date: 2003-02-10
    body: "I hope someone has fixed the bugs in KSpell's auto-underline. It's not very polished (at least in 3.1)"
    author: "antiphon"
  - subject: "Re: KMail Spell check on the fly"
    date: 2003-08-12
    body: "Hi Don \nCould you send me the patch for Kmail spell check on the fly. I am looking for that from a long time. \nThanks\nHari\n"
    author: "Hari Gottipati"
  - subject: "Great as always."
    date: 2003-02-09
    body: "Just a not about the dotty article: the link to KDE Pim, http://http://www.kdepim.org/ , obviously can't work. =P\n\nAnd a small request to Derek Kite: how about including some numbers from http://bugs.kde.org/weekly-bug-summary.cgi every week showing the changes done in bugzilla (eg. \"Konqueror: 1320 bugs, 106 opened, 40 closed, 66 more than last week\"). Something like that surely will encourage more people to help out as well. =)"
    author: "Datschge"
  - subject: "Re: Great as always."
    date: 2003-02-09
    body: "Hmm, seems like something odd is going on. \nAt:\nhttp://bugs.kde.org/weekly-bug-summary.cgi?tops=100&days=365\n\nyou can see a summary of the past year.\n\nKonqueror has 1320 bugs. In the past 365 days 3593 bugs were opened, and 1860 were closed, and change of 1733 (3593-1860). Does this mean Konqueror had a negative (1320-1733=-413) bug-count a year ago?\n\nJohan Veenstra"
    author: "Johan Veenstra"
  - subject: "Re: Great as always."
    date: 2003-02-09
    body: "I think that's glitch is due to the fact that bugzilla is not used for one years yet so the statistics reaching that far into the past are broken."
    author: "Datschge"
  - subject: "keramik web"
    date: 2003-02-11
    body: "Hi!\n\nDoes keramik development team has a web page?\n\nBye"
    author: "mathjazz"
  - subject: "keramik web"
    date: 2003-02-11
    body: "Hi!\n\nDoes keramik development team has a web page?\n\nBye"
    author: "mathjazz"
---
This week in the <a href="http://members.shaw.ca/dkite/feb72003.html">latest KDE-CVS-Digest</a>:  Read about the improvement in <a href="http://www.koffice.org/filters/">filters</a> to interoperate with other applications and formats such as Outlook Express, OOImpress, MSWrite, RTF and ApplixGraphics.  Plus, work continues on the <a href="http://pim.kde.org/">KDE PIM</a> project, <a href="http://www.koffice.org/">KOffice</a> <a href="http://www.konqueror.org">Konqueror</a>, <a href="http://kopete.kde.org/">Kopete</a> and many others.
<!--break-->
