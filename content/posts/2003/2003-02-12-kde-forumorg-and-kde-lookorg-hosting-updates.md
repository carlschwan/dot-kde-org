---
title: "KDE-Forum.org and KDE-Look.org Hosting Updates"
date:    2003-02-12
authors:
  - "numanee"
slug:    kde-forumorg-and-kde-lookorg-hosting-updates
comments:
  - subject: "WTG Lindows!"
    date: 2003-02-12
    body: "I for one am very glad to see Lindows giving back to the open source community.  They did some sketchy things early on with respect to the GPL, but they made good on offering their source to their customers.  And now they are supporting a great KDE site.  I have to say, hats off to Lindows!  Thanks for giving back to the community.  It is very appreciated!"
    author: "beergeek"
  - subject: "Re: WTG Lindows!"
    date: 2003-02-12
    body: "i can imagine this question has been asked a thousand times before, so here comes nr. 1001:\n\nIsn't that still violating the gpl if you give out the source only to customers? If it was, I don't think I'd take off my hat to them, no mattter who they sponsor."
    author: "me"
  - subject: "Re: WTG Lindows!"
    date: 2003-02-12
    body: "Well, no, have you actually read the GPL? It requires you to give the source code to anyone who gets the binary code (eg. your customers) and that you must not restrict your customers rights to modify and redistribute the programs under GPL. Looks as if this is fulfilled if they provide the source code to their customers.\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: WTG Lindows!"
    date: 2003-02-12
    body: "Nope, to put it simply you only have to make available the source to the ones who get the binary, in Lindows case their customers."
    author: "Morty"
  - subject: "Re: WTG Lindows!"
    date: 2003-02-12
    body: "Reading carefully the GPL FAQ ( http://www.fsf.org/licenses/gpl-faq.html ), it is clear that giving the source, not to the public, but only to the customer, is perfectly legitimate. But it is also true that when a customer have the sources, he may redistribute them freely, and also whoever copy the GPLed binaries have a right to ask for the sources. No NDAs are permitted.\n\nQuoting the FAQ:\nIf you commercially distribute binaries not accompanied with source code, the GPL says you must provide a written offer to distribute the source code later. When users non-commercially redistribute the binaries they received from you, they must pass along a copy of this written offer. This means that people who did not get the binaries directly from you can still receive copies of the source code, along with the written offer. \nThe reason we require the offer to be valid for any third party is so that people who receive the binaries indirectly in that way can order the source code from you. "
    author: "Vajsravana"
  - subject: "I agree"
    date: 2003-02-12
    body: "They did a wonderful thing, I for one don't know waht I would do without KDE_LOOK.org, the site needs some serious updates, many amny things can be improved, but it is still a great website.\n\nKDE-Forum is also awesome, I wish more people knew about it!"
    author: "Dan"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "I think kde-look should be moderated. A lot of the files uploaded (mainly  wallpapers) are pure and utter crap. Like political propaganda or nude girls. Ok, a girl with large K to hide her shame is probably OK, but I definitely can't see what the WWII Hiroshima bomb has to do with KDE.\n\nInbetween this horsesh*t, some pure gold is hiding. Yeah, you could sort by rating, but I feel there should be some threshold before the stuff is put there at all. \n\nI'm not a fan of censorship but surely there are other arenas for political agitation and pr0n..."
    author: "ac"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "Amen to that. The ratio of complete crap to acceptable-to-good KDE art is really not favorable to the good stuff. Moderation is a must. There's quality art on kde-look but it's too hard to find. \n\nGetting rid of everything that's not directly KDE related (naked ladies, anime trash, and endless color variations of Linux company logos...) would already do a lot to improve overall quality.\n\nYou can't let every 14 year old post his version of the Lord of the Ring Aqua Babe background and at the same time expect art KDE can be proud of. Most of the stuff on kde-look is embarassingly immmature and amateurish. It doesn't reflect well on KDE as a project and a community."
    author: "Mary"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "*cough*\nSearching for Mary results in \"No Entry\"\n*cough*\nA bit harsh for one not contributing, aren't we?\n\nThere are people who like a decent Wallpaper with the distros logo;\nThere are people who like a anime pictures;\nThere are people who like a nudes;\nThere are people who like a political statement;\n\nI'm not a fan of most of this either, but if the rating-system yields that people like what I don't (like the 4000nd blueish color-scheme or KDE written in pink on purple), I think it has the right to be on this site.\n\nYou can always browse kde-look.org with a threshhold of 70% ;-P\n\nregards,\nTom"
    author: "tomte"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "> A bit harsh for one not contributing, aren't we?\n\nA name is just a name. And, yes, I am harsh.\n\n> You can always browse kde-look.org with a threshhold of 70%\n\nNo I can't. That's one of the problems with the rating system. It's not my 70%; it's theirs! The rating system would need to be a personal one. If 75% of those who rate wallpapers on kde-look love crap [and I'm not necessarily saying that they do], setting that threshhold to 70% doesn't help me because I'm still seeing stuff I consider awful.\n\nThe way kde-look operates now doesn't encourage good KDE art because:\n 1) kde-look lets anything through, including crap.\n 2) This creates an outlet for crap and crappy artists.\n 3) Consequently, the number of crappy artists using kde-look increases,\n 4) and the number of votes from crappy artists increases proportionally.\n 5) Crap gets voted up by crappy artists.\n 6) The good stuff disappears; good artists are discoured: end of sad cycle of crap.\n\nI just wish kde-look could promote quality over abundance, plagiarism, and puerile self-expression. I believe the KDE community would be better served that way.\n"
    author: "Mary"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: ">That's one of the problems with the rating system. It's not my 70%; it's theirs! The rating >system would need to be a personal one. If 75% of those who rate wallpapers on kde-look >love crap [and I'm not necessarily saying that they do], setting that threshhold to 70% >doesn't help me because I'm still seeing stuff I consider awful.\n\nAnd moderation would change this fundamental problem HOW?\n\nVoting IS moderation. What is the difference between moderation by the many to the moderation of the few? In both case many people will disagree with the results, but I will prefer democracy to aristocracy.\n\nBut nobody hinders you to set up your own site where editors select the best artwork. I'll be the first in line to visit it. :-)"
    author: "Markus"
  - subject: "Re: I agree"
    date: 2003-02-13
    body: "> And moderation would change this fundamental problem HOW?\n\nIt doesn't. I was merely responding to the suggestion above.\n\n> Voting IS moderation. What is the difference between\n> moderation by the many to the moderation of the few?\n> In both case many people will disagree with the results,\n> but I will prefer democracy to aristocracy.\n\nVoting is not an appropriate form of moderation because it doesn't not define any standards.\n\nKDE and other free software projects are NOT democracies. They are (partial) meritocracies. Your work is reviewed, discussed, accepted and/or rejected all based on standards defined by the projects' leaders (whether informal or not does not matter). Voting would not work there just like it doesn't work for kde-look."
    author: "Mary"
  - subject: "Re: I agree"
    date: 2003-02-13
    body: "Why do you think linux-kernel, the most important ML for linux by far, resisted any attempt of moderation for years now? Because even in a meritocracies there is a need for open market-places where anybody can offer anything they like. This is the precondition that review, discussion, accteptance and rejection can happen at all.\n\nThere has to be a public place where artistic ideas and drafts can flow freely, hence KDE-Look must show all incoming stuff, like lkml distributes any patch and any idea regardless how stupid it may be. (Even bad stuff may contain a great idea or point to a serious problem! Someone else can continue work on it then. Isn't that part of the advantages that \"free\" and \"open\" offer?)"
    author: "Markus"
  - subject: "Re: I agree"
    date: 2003-02-14
    body: "The lkml is a good analogy and you're making a good point. However, though anything can be submitted to the list, not everything makes it into the kernel. There is a moderation process of some sort going on here. Maybe not at the source, but certainly at the destination. To which you could reply that this process is repeated when icons, wallpapers, and themes are chosen to be included in KDE. To which I would reply, moderation is indeed what keeps quality up.\n\nLet's assume then that kde-look is to remain unmoderated. I still think there needs to be a place where quality KDE art can be showcased and where talented KDE artists can work and create without the 'noise' of anime wallpapers. That's probably a role that artists.kde.org could fulfill be but it seems to be 'napping' at the moment.\n\nHaving kde-look around as THE reference for KDE art is bad for the project. Not because kde-look is a bad idea or is run by bad people but because it creates negative aesthetic associations with KDE. There are very talented artists working to make KDE look great. These people need to be given the recognition they deserve by having their work publicly acknowledged as the standard others should strive to emulate. There needs to be a way to say to aspiring amateur KDE artists, \"this is what we want for KDE, submit all the crap you want but you will be judged against these great icon sets, wallpapers, and themes.\""
    author: "Mary"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "s/discoured/discouraged/"
    author: "Mary"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "Well don't look at the Wallpapers if you don't want a wallpaper."
    author: "ac"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "You are right that most of the wallpapers are bad, and you have to look through them to find the good ones. I don't agree that they should start being moderated though, because then they would just reflect the sense of taste of the moderator. Right now, everybody who looks through the wallpapers has a slightly different idea about which ones are the good ones, and the variety assures that whatever your sense of taste, you'll find something. :-)"
    author: "AC"
  - subject: "Re: I agree"
    date: 2003-02-12
    body: "Perhaps, instead of moderation, wallpapers (icon sets, whatever) could be arranged into categories.  So that all the anime stuff could be grouped together, all the nudes go together, etc.  Perhaps a set of checkboxes could let users turn the individual categories \"on or off\" for display in the regular listing.... maybe even set individual rating cutoffs (\"only anime above 80%\", \"no nudes\", \"nature photos above 50%\").\n\nThis, plus the rating system now used, could go a long way towards making it easy to find exactly what one's looking for - or at least, avoid stuff that one doesn't want to find.\n"
    author: "Anonymous Coward"
  - subject: "OT: User switching"
    date: 2003-02-12
    body: "I remember that when I first logged out after updating to 3.1 I saw s.th. along the lines of \"keep logged in and start second x server\" (a la xp user switching). I did not test it at that time and now it does not show up anymore. \n\nDid I just mis-read a message or is that feature really hidden somewhere inside 3.1?\n\nUsing Suse 8.0 RPMs\n"
    author: "Anonymous"
  - subject: "Re: OT: User switching"
    date: 2003-02-14
    body: "Just lock your screen. Next to the passwd entry is a button labeled \"start a new session\". It will open on virtual console 8.\n\nGreets.\n\nAnno."
    author: "Anno"
  - subject: "Re: OT: User switching"
    date: 2003-02-14
    body: "Oh, and it's at the bottom of the K Menu too."
    author: "Anno"
  - subject: "Re: OT: User switching"
    date: 2003-02-14
    body: "Hhm, not for me anymore... Can I check if the correct lock program is being called? (What is it called? It is not klock)\n"
    author: "Anonymous"
  - subject: "Re: OT: User switching"
    date: 2003-02-14
    body: "Ok, found that it is kdesktop_lock. Calling /opt/kde3/kdesktop_lock directly does not show that button either. The file has the correct date (Jan 23) so it is indeed the one from mym kde 3.1 installation. weird.\n\n"
    author: "Anonymous"
  - subject: "kde-forum.it (for Italian KDE Community)"
    date: 2003-02-26
    body: "Hi to all KDE peoples.\nI have the pleasure to announce the birth of http://www.kde-forum.it (I'm waiting for Italian RA), in order to offer to the Italian community a place where to speak about their preferred WM in about our preferred WM in a familiar language and share help, ideas, opinions, etc.\nI am not an expert, so, if someone wants to propose himself in order to moderate some forum writes to me two lines."
    author: "ValeMax"
  - subject: "Re: kde-forum.it (for Italian KDE Community)"
    date: 2003-02-27
    body: "What's a \"RA\"? And \"Unknown host www.kde-forum.it\"."
    author: "Anonymous"
  - subject: "Re: kde-forum.it (for Italian KDE Community)"
    date: 2003-02-27
    body: "RA means Registration Authority http://www.nic.it/RA/index.html, and I wanted to say, I'm waiting they for domain activation, That one is the reason for which you have seen: \"Unknown host www.kde-forum.it\". Try in the next 3/4 days.\nThanks."
    author: "ValeMax"
  - subject: "Re: kde-forum.it (for Italian KDE Community)"
    date: 2003-02-27
    body: "Why do you announce something if it doesn't work (yet)? Wait and if it works submit a big story instead of a little comment."
    author: "Anonymous"
  - subject: "Re: kde-forum.it (for Italian KDE Community)"
    date: 2003-02-27
    body: "Joy of being able to make it is so a lot  that I have not known to wait for submit that story. Why are you, instead,  anonymous and don't publish a nickname and an e-mail address?"
    author: "ValeMax"
---
We're pleased to note that <a href="http://www.kde-forum.org/">KDE-Forum.org</a> is now back online after a rocky start.  A big thanks goes out to <a href="http://pem.levillage.org/">Pierre-Emmanuel Muller</a> and <a href="http://www.cyberbrain.net/">Cyberbrain</a> who are now hosting this site, and of course to <a href="http://www.kde-forum.org/profile.php?mode=viewprofile&u=2">zenok</a>, fan of KDE and creator of KDE-Forum.org for web-based KDE-related discussions.   Please note that  <a href="http://www.cyberbrain.net/">Cyberbrain</a> is also the host of <a href="http://dot.kde.org/">KDE Dot News</a> as well as several other less obvious KDE services and has been single-handedly responsible for virtually trouble free operation and peace of mind since June 2002.  Other KDE-related web forums available are: <a href="http://www.kde-forum.de/">KDE-Forum.de</a> for German speakers and the <a href="http://www.kde-france.org/agora/index.php?site=forums&bn=">KDE-France forums</a> for French speakers. In other good news, <a href="http://www.kde-look.org/news/news.php?id=41">Frank reports</a> that the terrific and invaluable <a href="http://www.kde-look.org/">KDE-Look.org</a> site has found new sponsorship from <a href="http://www.lindows.com/">Lindows.com</a> -- a big thanks goes out to Kevin Carmony, President and CEO of Lindows.com, as well as to everyone else who offered to help the site.  The KDE community should feel proud to have so many friends and willing <a href="http://www.kde.org/thanks.html">sponsors</a>.
<!--break-->
