---
title: "KDE Has Tea With Stallman"
date:    2003-05-02
authors:
  - "p"
slug:    kde-has-tea-stallman
comments:
  - subject: "What's wrong with C++? (nt)"
    date: 2003-05-01
    body: "What's wrong with C++?"
    author: "jmalory"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-01
    body: "I don't know, but maybe if it was called GNU\\C++ then Stallman would have recommend it."
    author: "Ned Flanders"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-03
    body: "Actually is *is* GNU\\C++... well more or less. It's called g++."
    author: "ac"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "I've got great respect for the guy, but he can be a bit off at times. As for what's wrong with C++, there is absolutely nothing wrong with C++. Fine language. However, RMS seems to be a Lisp'er, and they've sometimes got a chip on their shoulder about C++. The arguements boil down to:\n\n1) C++ is complicated. \n2) C++ is not regular (synactically).\n3) C++ doesn't have mathematical purity. \n\nFrom this they derive that C++ is inelegant. Of course, all of the above are true for English (and most other natural langauges as well!) and lot's of people find them far more elegant that mathematical notation...\n\nPS> I have nothing against Lisp. Common Lisp is too hairy (almost as complex as Standard C++) for my taste, but some Lisp-like languages like Scheme are very elegant. It just pisses me off that some people can't realize that different people might not think the same way, and may not like the same languages. Maybe this is just bad personal experiences, but you see a level of arrogance in comp.lang.lisp that you don't see unless you wander into the pure math department at university..."
    author: "Rayiner Hashem"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "Lisp? Elegant? Pure?\n\nHah! There are better, purer functional languages out there (with fewer parenthesis too). All Lisp coders should be forced to use Haskell or Clean for a while just to see what they're missing."
    author: "Matt Walton"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "Matty, you really do need to get out more :)"
    author: "Dawnrider"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "I second the Clean bit. Clean (which is very similar to Haskell) seems like a very elegant language. I've been learning it recently, and I'm very impressed."
    author: "Rayiner Hashem"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "Clean rocks. Its syntax is pure beauty, and you can write very efficient code in it. It even supports mutable data in a much simpler way than haskells monads, without losing the purity of functional programming languages.\n\nI really hope that one day I will find a job doing functional programming. Until then, I am quite happy with C# and java. \n\nC++ is a hack. But the beauty of Qt and KDE is that they use C++ in just the right way. C++ can be used very productively if you do not use every feature whenever it looks nice..."
    author: "Androgynous Howard"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "C++ is definitely inelegant.  For instance, the static keyword means completely different things depending on context.  Or, the syntax for declaring variables is different depending on whether they are function arguments or not.\n\nC++ is an ugly language.  It just so happens that C was just right for the majority of software at the time, and C++ was an easy upgrade path to \"proper\" OO programming.\n\nYou can't use natural languages as an excuse.  We use them in virtually every communication with another person, it takes us at least ten years to master just one, and a lot of people never even master their native language.  Plus, the problem domains for programming and general communication are far different."
    author: "Jim Dabell"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "From a synactical purity point of view, yes, it is inelegant. Nobody is claiming that there aren't lots of synactical warts on C++. However, syntax isn't really that important. Take the following example:\n\ntemplate <int num>\nstruct factorial\n{\n\tenum{ value = num * factorial<n - 1>::value; }\n};\n\ntemplate<>\nstruct factorial<0>\n{\n\tenum{ value = 1 }\n};\n\nNow in Clean (a 'pure' functional language):\n\nfactorial n\n\t| n == 0 = 1\n\t| otherwise = factorial(n-1)\n\nNow, you've got 10 lines of code vs 3. The C++ syntax, is admittedly horrible. It's more horrible than necessary because I've decided to highlight the worst syntax possible (compile-time STL metaprogramming) rather than a more common technique. However, is it really all that different? Most of it is just boiler-plate code. An experienced C++ programmer just mentally learns to filter it out. If you count \"active lines\" (ones that aren't just visual noise) there are 4 in the C++ version, and 3 in the Clean version. Given that C++ isn't even a functional programming language, and doesn't claim to be one, I'd say that's not too shabby. \n\nYou can't use natural languages as an excuse. We use them in virtually every communication with another person, it takes us at least ten years to master just one, and a lot of people never even master their native language. \n>>>>>>\nSure, but a lot of people also master natural language but not mathematical language. The two styles are fundementally different and people who have different thinking patterns find different styles more natural to them. \n\nPlus, the problem domains for programming and general communication are far different.\n>>>>>>>>>>\nThe Smalltalk and Perl folks would disagree that natural language isn't an appropriate basis for a computer language. Perl 6, I think, is going to drive the functional programmers (and the Lisp people, which I've inaccurately grouped with functional programmers for brevity) absolutely mad. A computer language designed by a linguist rather than a mathematician. Fun!\n"
    author: "Rayiner Hashem"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "You can't blame a language when you program a recursive function in such a strange way. May be I am missing something, but what is the point of the way you coded it in C++? What's wrong with:\n\ntemplate <type T>\nT fac( T x ){\n\treturn x==0 ? 1: x * fac( x-1 );\n}\n\nThat's three or four lines of actual code, depending on how you count. Granted the ?: operator doesn't have the elegance of what can be done in Haskel or Clean.\n\nBeside your functional example doesn't work, the last lin should be: \n | otherwise = factorial(n-1)\n \n"
    author: "Spencer"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "Um, I'm on the C++ side here :) I specifically pointed out that I tried to use the worst C++ syntax possible (template syntax) instead of using a regular function. Heck, the template version doesn't even do the same thing (works only at compile time), but that's not the point. My point was that most of the syntax is just visual noise you quickly get used to. The fundemental language underneath is sound.\n\n\nBeside your functional example doesn't work, the last lin should be: \n| otherwise = factorial(n-1)\n>>>>>>>>>\nBit you too, huh?"
    author: "Rayiner Hashem"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "Oh, I just wanted to add something ---\nIf C++ isn't reccomended for GNU software, isn't it ironic that GCC is one of the best C++ compilers available? :)"
    author: "Rayiner Hashem"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "If I do recall, GCC had to become egcs first before that happened.   And egcs won the battle of the forks by sheer technical excellence although it lost its name.  Basically GCC became one of the best C++ compilers over Stallman's dead body.  :-)"
    author: "ac"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-02
    body: "Most of the C++ standardization stuff happened in the 3.x series. I'd presume the same people are working on it now as were working on GCC before the split, but I could be wrong."
    author: "Rayiner Hashem"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-03
    body: "ABI issues. GCC 2.95 vs 3.0 vs 3.1.1 vs 3.2 vs Intel vs Borland etc. It'll take a while before everybody is switched to GCC 3.2.\nBut even then, there's no guarantee that they won't break compatibility again. The GCC team said they'll only break ABI again if it prevents them from compiling standards-compliant C++ code correctly, but there may still be a few of those bugs in g++ left.\nAlso, other C++ compilers don't support the C++ standard equally well.\n\nIn other words: there's nothing wrong with the language. There is something wrong with all the compilers however!"
    author: "AC"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2003-05-03
    body: "Nothing's wrong with C++. Its a great language, performant, powerful and \"well-thought\". \nThe realization of OO-ideas has been done well (better than in Java), but you have the procedural C subset for the lowest abstraction layers too.\nFunctional languages may have their domain in the implementation of algorithm, but the OO paradigm is superb for big programs & program frameworks (see the KDE framework for example), since it provides the necessary grade of abstraction.\n   "
    author: "Ruediger Knoerig"
  - subject: "Re: What's wrong with C++? (nt)"
    date: 2004-04-01
    body: "No, c++ does not have better \"OO-ideas\" than java.  Sun used c++ as a guide for what not to do with java."
    author: "mike"
  - subject: "GNU/Visual Basic"
    date: 2003-05-02
    body: "Let's make Visual Basic the GNU standard!! :p"
    author: "bleh"
  - subject: "Re: GNU/Visual Basic"
    date: 2003-05-03
    body: "Perhaps you prefer\n\nhttp://hbasic.sf.net\n\nhttp://gambas.sf.net"
    author: "Hakenuk"
  - subject: "I liked the report"
    date: 2003-05-02
    body: "but, its too bad Richard Stallman was not more familiar with KDE."
    author: "ALex"
  - subject: "Re: I liked the report"
    date: 2003-05-02
    body: "It is not too bad. Apart from emacs, RMS is not much familiar with any free software. :-)\n\nBut now, he is familiar with KDE."
    author: "Philippe Fremy"
  - subject: "Re: I liked the report"
    date: 2003-05-02
    body: "Did you send him the info about terminal resizing? I wonder if he has updated emacs accordingly."
    author: "Apollo Creed"
  - subject: "Re: I liked the report"
    date: 2003-05-02
    body: "I don't use non-x emacs a lot, but I've never noticed that it does not resize along with Konsole.  I just tested it now with emacs 21.2.1, and it works just fine."
    author: "Haakon Nilsen"
  - subject: "Re: I liked the report"
    date: 2003-05-02
    body: "Agreed, works fine with emacs 21.3"
    author: "Spencer"
  - subject: "Alien"
    date: 2003-05-02
    body: "Come on, face it, he must be an alien. Like Michael Jackson. There's no other way the article can make sense. "
    author: "AC"
  - subject: "Re: Alien"
    date: 2003-05-02
    body: "Stallman is always teetering on the brink of insanity, but manages to make enough sense to build a solid following. Like most preachers, I guess."
    author: "Xirzon"
  - subject: "Re: Alien"
    date: 2003-05-02
    body: "LOL. The interview shows that he has already crossed the border\nto the realm of insanity. I once worked in an old-people's home and\nhe reminds me a lot of the people living there. Once perhaps a genius\nbut now sliding the slippery slope, insisting on marginalia, GNU/Linux, Linux,\nhell what a topic to discuss with the KDE team if you only have a bit of time.\nThe KDE guys seems to play nice just like you do with your Grandma.\nAnd BTW not that I want to sound superficial: Is that he under that tuft of hair?? \nPerhaps he should get a shave soon or he will end up like Reinhold Messner\nhunting yetis in the mountains."
    author: "Martin"
  - subject: "Hey Now"
    date: 2003-05-02
    body: "What's wrong with having a beard these days?"
    author: "Miles Robinson"
  - subject: "Re: Hey Now"
    date: 2004-10-29
    body: "nothing wrong with having a beard, But if you're male, it's weird to have a ponytail!"
    author: "a student of yours"
  - subject: "Re: Hey Now"
    date: 2004-10-29
    body: "It's not a *real* ponytail. It's just his hair. No ponies were harmed."
    author: "Roberto Alsina"
  - subject: "Re: Alien"
    date: 2003-05-04
    body: "What's wrong with hunting yetis?"
    author: "Spark"
  - subject: "Re: Alien"
    date: 2003-05-04
    body: "They're an endangered species?"
    author: "ac"
  - subject: "Re: Alien"
    date: 2003-05-02
    body: "Lets keep religion, race and sexuality apart from everything else shall we not? please?"
    author: "kidcat"
  - subject: "Re: Alien"
    date: 2003-05-02
    body: "I really have trouble being too critical of Richard Stallman.\n\nEvery day I benefit from what he wrote and gave away. I benefit from his ideas and vision.\n\nYes, he is quirky and has missed big trends, maybe shows some egotism. His baby has grown beyond even what he could have imagined it. He is as human as anybody else I know.\n\nSo what can I complain about? He looks funny? Do I lose respect for him because he uses emacs?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Alien"
    date: 2003-05-03
    body: "But after reading this article you get the impression that he is out of this world. He seems to care more about terminology and dogmata than about free software and technology. And when he shows interest in technology, he seems to be stuck in his admittedly powerful but otherwise quite outdated Emacs stuff. \n\nHe did indeed create a vision that is fundamental, and wrote it down in form of a license. He started a software project that's the foundation of most of what we use these days. But he seems to be too far away from what is needed to make this thing mainstream among those who simply do not care for freedom of intellectual property yet.\n"
    author: "AC"
  - subject: "Re: Alien"
    date: 2003-05-03
    body: "I know more people who seem to be out of the world. None of them get flamed as heavily as RMS does."
    author: "Ac"
  - subject: "Re: Alien"
    date: 2003-05-03
    body: "RMS comments of Software patents show that he is an excellent communicator. And of course he is right. But his Gnu/Linux stuff harmed his reputation."
    author: "Hakenuk"
  - subject: "Re: Alien"
    date: 2003-05-03
    body: "No question. I imagine he would be infuriating to work for or with.\n\nBut that applies to alot of brilliant people. Myself included (not brilliant, but hard to work with). Impatient, opinionated, even fixated.\n\nTell me. Do you use graphical based software because it is fundamentally better and more productive, as opposed to the new and good applications need it?\n\nEmacs does precisely what rms wants with very few keystrokes. I assume that is why he still uses it.\n\nAs for the idea that he is too far to bring the whole thing mainstream, I don't know if it matters. The whole foss thing is too big for any one person to represent or control.\n\nDerek"
    author: "Derek Kite"
  - subject: "Emacspart ???"
    date: 2003-05-02
    body: "That's what i've ever been waiting for; Are there now any efforts in realizing that project ? For me vim is the little editor i do all the config-file editing or so with, but i personally preferre emacs to vim if i have to do some coding; gideon and emacs would be the best i could imagine !"
    author: "Franz Keferboeck"
  - subject: "Re: Emacspart ???"
    date: 2003-05-02
    body: "I think there are three reasons this has not been done, first, emacs is huge, you would have to pick which parts you wanted to implement. Second, it is written mostly in LISP, so the translation is not as easy. Third, most of the important characteristics of editing text in emacs can be simulated by setting up matchng key bindings.\n\nIt would be nice to use emacs code indent features though."
    author: "Spencer"
  - subject: "Re: Emacspart ???"
    date: 2003-05-05
    body: "Is there any other text editor with this code indent feature ? This is the only feature which keeps me with emacs."
    author: "Romain"
  - subject: "Re: Emacspart ???"
    date: 2003-05-05
    body: "Many other editors support code indenting. But they all operate different. Overall I like the way emacs works. But it is good to experiment, many editors have code indenting and syntax highlighting similar to emacs."
    author: "Spencer"
  - subject: "KDE and RMS"
    date: 2003-05-02
    body: "(Just some general impressions from the article and the comments above...)\n\nPersonally, I think Richard Stallman (alias RMS) is still one of the few people in this world to have some human, reasonable and rather coherent points of view (more talking about his political ideas than about the programmer, here); his vision of things on the GNU/Linux front might be a little exaggerated sometimes, but nevertheless it's good he's there to stimulate others, I think.\n\nIt's obvious that KDE (and graphical environments in general) isn't his \"natural\" element: this also explains why the tea meeting doesn't seem (looking at the photos) to be so addicting for either part. After all, people there were probably busy with other, more important things, anyway.\n\nAs for Stallman's \"looks\", considering that he's 50, I'd say that he looks quite damn good (at least 10 years younger!). Personally, I like the beard and long hair (having those myself, also): he reminds me a little of Bakunin and other great past thinkers... :-)\n\nP.S.: Of course, the KDE + RMS/FSF relationship seems to be more one based on principles and theory; OTOH, a more practical and \"fruitful\" relationship could be the KDE + Apple one (I'm an OS X user, BTW): I really hope this will develop beyond KHTML/Safari, as OS X (IMHO) needs some of KDE's powerful features - and, conversely, KDE needs a little more of OS X's simplicity of use and elegance."
    author: "Sven"
  - subject: "Re: KDE and RMS"
    date: 2003-05-02
    body: "The tea meeting was very addictive. I do not think he planed to spend that long with us initially. He left in a hurry. \n\n\n"
    author: "Philippe Fremy"
  - subject: "Deceptive?"
    date: 2003-05-03
    body: "It sounded a bit deceptive saying that some KDE guys call it KDE/GNU/Linux.  Even if some do, the world never sees it written like that.  This is the first place I've ever seen it.\n\nRMS probably thinks that some do use it, but in reality the end user never sees it.  Is that really fair to tell him that?"
    author: "Tyreth"
  - subject: "Re: Deceptive?"
    date: 2003-05-03
    body: "Actually I think it should be used for often, because it 'fixes' a common misconception among users. People think that 'Linux' is a platform. It is not, at least for GUI stuff. You can use any server on a \"Linux machine\", if it is compatible with a few standards of the LSB. There is only one de-facto c library and only one de-facto file system layout. But there is no such thing for the desktop. You could write for Linux+X11 or Linux+X11+Freedesktop.org, but this software would not integrate well into a Gnome or KDE desktop. \nKDE+Gnu+Linux is a complete platform, and Gnome+Gnu+Linux is another one. If you target it would will have a pretty integrated system.\n"
    author: "Tim Jansen"
  - subject: "Use ObjectiveC"
    date: 2003-05-03
    body: "or whatever ...\n\nIt's the best ... really"
    author: "KDE Fanatic"
---
After <a href="http://www.kde-france.org/article.php3?id_article=98">Charles' report</a> on Paris Linux Solution 2003, you can now read <a href="http://phil.freehackers.org/linux-solutions-2003.html">my own account</a> of the event. The most interesting part is certainly the <a href="http://phil.freehackers.org/writings/tea-with-stallman.html">tea episode with Stallman</a>, arranged by the <a href="http://www.kde-france.org/">French KDE team</a>.
<!--break-->
