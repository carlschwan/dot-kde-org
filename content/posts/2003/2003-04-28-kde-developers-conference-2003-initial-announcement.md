---
title: "KDE Developers' Conference 2003: Initial Announcement"
date:    2003-04-28
authors:
  - "mettrich"
slug:    kde-developers-conference-2003-initial-announcement
comments:
  - subject: "Money?"
    date: 2003-04-28
    body: "Does kde.org/kde.ev need donations for organizing the conference/sposoring developers? Any way to specifically donate money for the conference?\n"
    author: "Anonymous"
  - subject: "Re: Money?"
    date: 2003-04-28
    body: "That would be a very worthy cause when you consider that it is at these conferences that kickass KDE technologies like DCOP and KParts have been born from."
    author: "KDE User"
  - subject: "Re: Money?"
    date: 2003-04-28
    body: "I quote from conference program, found in the mailingslists archives:\n\"KDE e.V. is solliciting donations from companies and individuals\ntowards this conference. These donations will be used both for running\nthe conference and for bursaries for delegates from other continents\nand/or without income.\"\nSo, I'd say yes to your question.\n\nSee: http://mail.kde.org/pipermail/novehrady/2003-April/000013.html"
    author: "Andr\u00e9 Somers"
  - subject: "KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-28
    body: "Haven't tried actually, that was just to get your attention.\n\nToo bad there aren't more KDE conferences here in the US.  I guess KDE is primarily a European product, and doesn't have too many Americans developing for it.  I didn't say none, I said too many.  Even if I could get the time off of univ. to attend, it would have to be in the US to be affordable for me.  I'm stuck here in MN and I wish something would happen closer to home.  Local LUG meetings are joke so no suggesting them.\n"
    author: "Super PET Troll"
  - subject: "Re: KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-28
    body: "Look at the Troll in the mirror:  Organize your own KDE in America event.  Less talk more walk."
    author: "KDE User"
  - subject: "Re: KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-29
    body: "Hey I did try to organise something but no one was interested.  I even setup a site for it at www.users-of-kde-and-its-developers-of-the-upper-midwest-and-other-parts.org and put up signs along local interstate highways.  No one ever contacted me nor did anyone ever even go to my site once.\n"
    author: "Super PET Troll"
  - subject: "Re: KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-28
    body: "Actually there are a good number of people from the US and Canada working on KDE.  (Side note: it's important here to say North America, since there are probably as many Canadians working on KDE as folks from the US.)  The problem is that in North America they're so spread out.  Basically to gather all of the KDE developers in North America this still means everyone having to get on a plane to go somewhere.  At that point it's only maybe 50% more expensive to just go to Europe, where most of the developers are.  Hopefully we'll have some of the KDE developers from the North America at this event (I don't really count I guess since I moved to Europe. ;-) ).\n\nAs for local LUG meetings being a joke -- well, never underestimate such things.  (a) There are more KDE people lurking about than most expect and (b) this is a great way to get more people involved.  Case in point -- when I was living in western Michigan there were 5 KDE contributers (that I knew of) within 50 miles; we never met because we were too disorganized to get something going."
    author: "Scott Wheeler"
  - subject: "Re: KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-28
    body: "Roundtrip fares to Europe are so cheap these days, why not combine the KDE meeting with a trip through Europe. "
    author: "Anonymous"
  - subject: "Re: KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-28
    body: "Pitty most Americans dont know thier geography better, but hey its forgiven this time. ;)\n\nIn fact there are about 15(probibly more since some dont come on IRC as much) active KDE developers in North America.  Now if your bored someday take out a map of the US and then take out a map of the EU.  You will note that most of it fits in Texas. I think France spills over in to Oklahoma, but hey Americans love to sent people they hate for being inconveinent to Oklahoma anyway (Oh now that is the troll of the century!) The fact of the matter is we are in a much larger area.  \n\nWe have met a few times for hack sessions but it get expensive to travel from Philadelphia to Toronto, to Chicago...etc.  Its actually more expensive for me to get from Philadelphia to St Paul MN than for me to get to Frankfurt Germany right now, so take that for what its worth.\n\nIn short, if _you_ want a KDE developer conference in your neck of the woods, plan one.\n\nMaby if its cheaper for us to all fly to MN well come, but Im doubting it.\n\n(Disclaimer:  A) im American B) I have planned a few smaller events out here and met quite a few other developers at Linux conferences in both Europe and America C) The above is ment to be take with some element of satire, given the worlds current affairs D) I would love to have a conference in North America but until someone has the time to plan it I dont think it will happen.)\n\nJust my 2c and counter troll...\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-28
    body: "Living in the US I have also wanted to change that.  I live in the north east (NJ) and have been building a list of people who are within a 10 hour drive and a month or so ago three of us got together in PA (in somone's basement) for an un-official get together.  Although it was just three of us it was a great start because in turn several more people come out of the woodwork saying they could have come.  We are thinking of having another one (hopefully larger).  Email me if you are interested in coming.  I did notice that in CA the past few years at LWE a number of KDE guys showed up.  Hopefully in NYC next spring we will have a booth and have a mini get together then."
    author: "Benjamin Meyer"
  - subject: "Re: KDE doesnt compile in Visual C++ .NET"
    date: 2003-04-29
    body: "Hey my basement rocks!"
    author: "ian reinhart geiser"
  - subject: "Budweizer rules! "
    date: 2003-04-28
    body: "i wish i could be there! though i am not much experienced in KDE programming. \n\nLuckily -- Budweiser is not produced in here, in Russia, wich means, that we have authentic c drink from Czeske Budejevice -- but infortunately -- that is only \"exort edition\" -- wich means that it is not that fresh as what one could find in Czechia. :-) "
    author: "SHiFT"
  - subject: "Fast, Faster, KDE?"
    date: 2003-04-28
    body: "KDE is still a lot slower than QT is. Maybe it would be a good idea to use this conference to combine some KDE brain-power to find solutions to speed up KDE?"
    author: "Dik Takken"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "What?\nThe speed, the binaries get executed? (hm.. it's acceptable at the moment)\nTo speed up the startup-time (speak: loading libraries and dyn. linking)?\n(well... lot's of work done in this departement... and a bit over-discussed already)\nTo speed up compile time? (Don't think you're thinking of this)\nTo speed up the development process? (nah, not really either...)\n"
    author: "Thomas"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "Especially the startup-time of KDE apps needs more improvement. Of course, its a lot better than it used to be, but I think KDE can still be a lot faster than it is now. For example, starting konqueror in the relatively simple file browser mode still takes about two seconds on my 800 MHz Athlon. When I boot to Win98, the file browser is there instantly when I need it. \n\nOf course, Konqueror has way more features than the windows 98 file browser, but I would not expect all of these features to get loaded every time I 'just want to copy a file'. On the other hand, I am impressed by the short startup time of Konqueror in web-browser mode. It loads a lot faster than Mozilla or even Phoenix. This observation suggests that Konqueror loads and initializes too many built-in features on startup, even when you don't use them. This is why I feel that Konqueror can be much faster than it is now, and maybe other KDE components as well."
    author: "Dik Takken"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: ">When I boot to Win98, the file browser is there instantly when I need it. \n\nYou can enable the konqueror's preloading function for that. Windows does that in order to improve loading speed.\n\n \n"
    author: "uga"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "No, not really. I think people get mislead by the fact that explorer.exe runs the desktop/taskbar as well, but actually the explorer file browser is a bit like Konqueror, all the components are COM objects that connect to the GUI at runtime. They are not preloaded, simple experiments can confirm this (like running it in wine, don't try it at home kids, if you're not careful you'll corrupt your windows drive :o). IE for instance starts very fast as well in Wine, no preloading there.\n\nI used to think MS preloaded loads of stuff. Now I know better... they just know how to write really fast code."
    author: "Mike Hearn"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "You're probably not on cvs then... In KDE 3.2 cvs konq starts as fast as the window (which pops up instantly) can draw. That is absolutely faster than IE. This is with preloading. IE, which preloads in windows, takes about 1 s to start on this same box in WinXP Pro SP1.\n\nThis is on a Dell laptop with P4 1,7 GHz. Speed gets lesser of an issue for every KDE release, responsivness is more important to work on currently IMO (not that it's bad anymore with XF 4.3, randr enabled qt and 2.5.x kernels with preempt and low latency, but still MORE of an issue...)"
    author: "ealm"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "I don't think you understand, I wasn't really talking about the speed of KDE, I was just making the point that Explorer *doesn't* preload stuff and still loads very quickly.\n\nSure, keeping the program in memory all the time but hidden is one way to get speedups (though rather brutally), but IE/Explorer manage to be fast despite not doing this, mainly because things like their implementations of COM/the registry etc are optimized for speed."
    author: "Mike Hearn"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "Yeah I used to think this was the case too, until I actually looked at IE and found out most of it was preloaded COM objects.  The first IE start is slow, after that they are faster.\n\nBasicly the same thing KDE does, only we start with one running.\n\nThe other thing that kills KDE is our prefs system, but that is another story.\n\nWell leave that one to the Armchair Developers on the DOT :)\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "Well this is my point, the COM objects are not preloaded, when explorer.exe is loaded for a new login some objects are loaded, primarily VFS objects (the win32 equivalent of them).\n\nWhen you load Explorer the browser, the given address is resolved into a PIDL (basically an opaque structure defining a path in the VFS system like our URIs except not human readable), and then scary stuff deep inside the shell system activates various objects which construct yet more objects to represent the folder tree. Then the tree view sidebar is constructed (another on demand COM object), then a view is constructed for the currently selected node (my computer, desktop whatever).\n\nThis view then uses OLE to add stuff to the toolbars, menus etc and you are finally presented with an Explorer window. If you then use the location bar to enter a new web address, you'll see it \"morph\" into IE, as the correct view is activated, MSHTML is loaded, the shell doc view (shdocvw.dll) is loaded and changes the toolbar/menus and so on.\n\nIt's a very powerful system, if very complex, confusing and generally undocumented.\n\nSo all of this is load-on-demand. The fact that IE loads faster under Wine that Mozilla does is a rather depressing confirmation of this fact (and you can run traces under windows as well to prove it). There is no preloading, much though I hate to admit it.\n\nObviously the most expensive thing here is activating the objects (registry lookups, mapping and linking the DLLs) and that is all cached, so subsequent startups are much faster."
    author: "Mike Hearn"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "Try this on a windows box: \n\na) open windows explorer.\nb) type a http url\nc) see how slowly it becomes internet explorer\n\nThen compare it to how long it takes to start IE itself\n\nIn my experience, IE loads 10 times faster than the components load into an existing shell! \n\nEither there is some preloading, or _unloading_ components is ungodly slow, which would be very weird."
    author: "Roberto Alsina"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "Actually if you read ATL Internals (ATL == Active Template Library, a templated library for working with COM) and Inside COM, you will see that once a COM Component has been loaded, it remains cached in the internal object store.  So long as any process is still running which has at some point referenced the component type, whether or not it has ->Release()'d the component, it remains cached unless the component is designed to be free'd as soon as all references to it have been relinquished (not the default behaviour).  Even when the processes have ended, the component may not be released until the COM cache manager decides to remove the component from the cache.  This is something Microsoft did in order to speed up the loading of COM components; however, this is also a major contributer to why Windows is such a bloated resource hog: too many components loaded into memory at once just in case they may need to be used at some point."
    author: "Michael Dean"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "A few reasons for that (probably):\n\n1) iexplore.exe shows the window before it starts loading the renderer component\n2) Unloading components IS slow, it's not just a case of freeing the memory, it's handling the negotiation of the UI elements, swapping the toolbars/menubars over, connecting the various event sinks and so on. Plus you have all the usual COM churn.\n3) If you type an address into explorer, normally you'll have the tree open at the side. That adds quite a bit of work.\n\nAnd then there's the whole \"redmond moves in mysterious ways\" thing. Believe me, IE starts very fast in Wine too, no chance to preload things there."
    author: "Mike Hearn"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-05-04
    body: "There is preloading for ie if ie comes with windows (must be win98 and the next ones)\nIf you install IE on win 95, even on a fast computer, it will take time to load and you will see a splashscreen. This splashscreen has been removed when ie was integrated in windows."
    author: "Macolu"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-28
    body: "\"Especially the startup-time of KDE apps needs more improvement.\"\n\nPrelinking is your friend :)"
    author: "Janne"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-29
    body: "Is this a joke? I was told that ld version 2.13.90.0.2 has prelinking built-in already, so I did not need prelinking anymore.. "
    author: "Dik Takken"
  - subject: "Re: Fast, Faster, KDE?"
    date: 2003-04-29
    body: "You were told wrong. Or least it's misleading. First of all, it's not ld (the linker), but ld.so (the dynamic loader). If whoever told you that really meant ld, then it wast most probably about combreloc support, which is also one way to speed up ld.so performance. For prelinking, you need support in ld.so, and you need a tool to perform the actual prelinking."
    author: "L.Lunak"
  - subject: "Wow, first time that this is really close to me..."
    date: 2003-04-28
    body: "Those conferences have ever been too far away to just get there and listen to it (as i am not an active developer... but i'd love to be...). Now this is _really_ close to austria, and as i live in Freistadt it's even better :-)\nLookin' forward to that event!\nGreetings\nSchnoopy"
    author: "Franz Keferboeck"
  - subject: "costs?"
    date: 2003-04-28
    body: "I would really like to attend (if I'm ready with my final project for my university then), but I can't find anyting about the costs. Being on a tight budget, that is an important aspect for me... Does anyone know about that?"
    author: "Andr\u00e9 Somers"
  - subject: "Re: costs?"
    date: 2003-04-29
    body: "We'll provide further and detailed information soon. Part of the beauty of the location are precisely the costs. KDE e.V. will most certainly be able to cover the costs for the speakers, and support other KDE contributors significantly as well. We haven't yet done precise calculations, but accomodation, food and beer prices should fit well into a student's budget.\n\nStay tuned, this was just the initial announcement. We work on providing more information once we have them."
    author: "Matthias Ettrich"
  - subject: "Re: costs?"
    date: 2003-04-30
    body: "Czechia is much more cheaper than Western Europe or USA. $1 is aprox. 28 Kc. Beer (1/2 litre) in a pub 15-25 Kc, loaf of bread 15 Kc, dinner in a restaurant 80-150 Kc, 100 km by bus 80 Kc, etc. If you want to know more just ask."
    author: "Ritchie"
  - subject: "Re: costs?"
    date: 2003-04-30
    body: "From the mailing list: \"Accomodation 150Kc/person/day (5Euro) in the castle, 250Kc/person/day (8,40Euro) in the Monastry. The food is for full pension (Breakfast, Lunch with coffee and cake afterwards, Dinner) 180 Kc/day (6Euro). Additionaly there are a couple of nice/better Appartments in the castle, [..] there the price ranges between 300-500Kc/person/day (10-17Euro). Also there is a couple of pensions and hotels in the town, prices rank around 10 Euro/night.\""
    author: "Anonymous"
  - subject: "Re: costs?"
    date: 2003-05-04
    body: "sorry for advertising, but, if you are in Europe, you should look at Eurolines : it's quite cheap"
    author: "Macolu"
  - subject: "Thx to SuSE"
    date: 2003-04-29
    body: "SuSE is sponsoring the location - SuSE is sponsoring some organizing staff.\nNow I'm pretty sure I did the right thing in buying a SuSE 8.2 box.\n\nciao"
    author: "Birdy"
  - subject: "Re: Thx to SuSE"
    date: 2003-04-29
    body: "They are not sponsoring it in first place, but they do a lot of organization which we should really be thankful for :)"
    author: "Anon"
---
This is the first announcement of the official KDE Developer's
Conference 2003!  The conference kicks off on Friday the 22nd of August and will take place at the Zamek
("Castle") in <a
href="http://trolls.troll.no/ettrich/NoveHrady.pdf"">Nov&eacute; Hrady</a> (PDF) in the southern part of the <A href="http://www.czech.cz/">Czech Republic</a> in Central
Europe.  Compared to previous KDE
conferences, this one will be bigger, in terms of number of
participants, scope and time.  We will start with two days of
technical talks in at least two parallel tracks, and end with an
optional six-days project work and hacking session until the
31st of August.  There will be plenty of time for socializing.
<!--break-->
<p>Quite simply, Nov&eacute; Hrady is a <a
href="http://trolls.troll.no/ettrich/NoveHrady.pdf">stylish
location</a> for a stylish desktop environment -- and very close to
the real <a href=http://www.budvar.cz/"">Budweiser Budvar
Brewery</a>. There are excellent facilities on and around the castle,
including a very fast network connection and cheap
accommodation. Nov&eacute; Hrady cannot easily be reached by public
transportations, but a shuttle service, organised by the boys and
girls of <a href="http://www.suse.cz/">SuSE Prague</a> (contact <a
href="mailto:lukas@kde.org">Lukas Tinkl</a> for scheduling), will be
provided from Prague-Ruzyne airport and Prague Central Station.

<p>Do you have a particular expertise related to KDE programming that
could be useful for your fellow developers? Do you want to present a
particular programming pattern, a tool, a development strategy, or
anything else that could help KDE developers become more productive? Then
consider talking about it at the KDE Developers' Conference.
<p>
We solicit submissions for presentations from, but not restricted to
the following fields. Everything KDE-related will be considered.

<ul>
<li>Qt programming tricks
<li>kdelibs programming tricks
<li>KParts development
<li>KOffice development
<li>aRts development
<li>programming tools
<li>programming patterns and development strategies
<li>project management issues
<li>internationalization
<li>documentation
<li>usability
<li>accessibility
<li>interoperability
<li> ...
</ul>

<p>Technical talks will be scheduled for 45 minutes talk and 15
minutes Q&A. In some cases, it may be possible to occupy two slots for
a total talk time of 90 minutes. Please note this in your abstract if
you require this.  There will also be room for short presentations of
15 minutes each, and submissions for these are very welcome as well.

<p>A final call-for-papers will be sent out soon, as well as information
on registration, speaker incentives and financial compensation. The
purpose of this first announcement is mainly to help you schedule your
summer vacation, so you can dedicate the last week in August to your
favorite software project.

<p>If you have further questions at this point in time, or simply want
to help with organizational issues, <a
href="mailto:novehrady@mail.kde.org">contact us</a> on the <a
href="http://mail.kde.org/mailman/listinfo/novehrady">novehrady
mailing list</a>.

<p>Looking forward to seeing you guys in sunny Czechia,<br>
Matthias