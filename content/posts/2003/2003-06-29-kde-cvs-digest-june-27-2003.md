---
title: "KDE-CVS-Digest for June 27, 2003"
date:    2003-06-29
authors:
  - "dkite"
slug:    kde-cvs-digest-june-27-2003
comments:
  - subject: "Worried"
    date: 2003-06-29
    body: "Phew! You got me worried Derek. I had a really boring Friday waiting for your report. BTW, you need to set your calendar, 27th was 2 days ago ;-)\n\nAnyway, thanks a lot."
    author: "uga"
  - subject: "Re: Worried"
    date: 2003-06-29
    body: "You need to get out more :)\n\nAlternatively, you could fix some of these: http://tinyurl.com/fk4d\n\nJust think, you could be *in* the CVS digest next week!"
    author: "cbcbcb"
  - subject: "Re: Worried"
    date: 2003-06-30
    body: ">You need to get out more :)\n\nSome people need coffee to wake up, some other need  a proper breakfast in the morning... I need a CVS Digest before I can go out ;-)\n\n> Alternatively, you could fix some of these\n\nMaybe after I release the stable version of my app (currently about to release alpha version 0.1 :-) )"
    author: "uga"
  - subject: "Re: Worried"
    date: 2003-06-29
    body: "Same thing for me :)\n\nThe reading of the KDE CVS Digest symbolise the end of my working week and the beginning of the week-end.\n\nAnd now with this 2 days late I am totally perturbated :(\nI will do a bad work next week. I have done some nightmares on saturday about the time compression and the end of it. \n\nDon't do that next week or I'll be mad :)\n\nTanks Derek"
    author: "Shift"
  - subject: "Which ircnet?"
    date: 2003-06-29
    body: "Which ircnetwork has the mentioned #commits channel? \nGenerally speaking, which interesting kde channels exist in which networks?\n\nthanks,\nMike"
    author: "Michael"
  - subject: "Re: Which ircnet?"
    date: 2003-06-29
    body: "irc.freenode.net"
    author: "Albert Astals Cid"
  - subject: "Re: Which ircnet?"
    date: 2003-06-29
    body: "Freenode is the network that has #commits. Either connect to irc.freenode.net or select freenode from your IRC client's server list."
    author: "norman"
  - subject: "Re: Which ircnet?"
    date: 2003-06-29
    body: "Easy mnemonic: irc.kde.org"
    author: "Anonymous"
  - subject: "Re: Which ircnet?"
    date: 2003-06-30
    body: "KDE fan channels may exist in every greater network (e.g. IRCnet) but you will likely only find developers and most users in FreeNode (irc.kde.org alias). A quick scan there gives these KDE-related channels: #debian-kde, #kde, #kde-commits, #kde-devel, #kde-edu, #kde-users, #kde.de, #kdevelop, #koffice, #kontact, #kopete and #qt"
    author: "Anonymous"
  - subject: "BiDi f\u00fcr Kate?"
    date: 2003-06-29
    body: "Heisst das, jetzt brauchen wir bald kein Kedit mehr!!"
    author: "Hein"
  - subject: "Re: BiDi f\u00fcr Kate?"
    date: 2003-06-29
    body: "\nI hope so...\nThis file-eater (http://bugs.kde.org/show_bug.cgi?id=59413) should go away.\nWe have Kwrite and Kate."
    author: "yg"
  - subject: "Re: BiDi f\u00fcr Kate?"
    date: 2003-06-30
    body: "BR59413 was fixed in CVS in since mid-April."
    author: "Aaron J. Seigo"
  - subject: "Re: BiDi f\u00fcr Kate?"
    date: 2003-07-01
    body: "But only in HEAD... So users will see the fix only when KDE 3.2 comes out. :-(\n I have no idea how to fix it in BRANCH without introducing new strings. Maybe an exception should be made in this case, as the bug may lead to data loss.\n\nAndras"
    author: "Andras Mantia"
  - subject: "ohhh nooooooooo pleeease"
    date: 2003-07-01
    body: "I _hate_ the name Kwrite (reminds of which nasty little program?)... please, if it is kept it should be renamed KEdit, it has no fornatting support after all, has it?\n[hope someone hears this hehe]\ncya zero"
    author: "zero08"
  - subject: "Small gripe about KDE-CVS-digest"
    date: 2003-06-30
    body: "Hi!\n\nI love KDE-CVS digest, it's so much better than the various xy-traffic sites.\n\nBut there is a tiny problem which I think should get solved:\n\nIn the overview, you sort it in \"Features - Optimizations - Security - Bugfixes\"\nbut the detailed descriptions are sorted differently (I think Features-Bugfixes-Optimizations-Security), I think they should be sorted just like in the overview.\n\n"
    author: "Roland"
  - subject: "Quiet..."
    date: 2003-06-30
    body: "I wonder why it is so quiet here. 10 comments... Nobody who wants to tell us about the 30 features that KDE needs in order to replace Windows? Or maybe a \"developers should stop adding feature and spend the rest of their life optimizing the code\"? What about merging with Gnome (or, to make it more interesting, with BeOS or Fvwm)? Or at least a bug to vote for? I am so bored...\n\n"
    author: "AC"
  - subject: "Re: Quiet..."
    date: 2003-06-30
    body: "Probably nobody can access this webpage. With the latest qt-copy version update in cvs, khtml's rendering is completely broken ;-)"
    author: "uga"
  - subject: "Re: Quiet..."
    date: 2003-06-30
    body: "How latest is that. I'm running CVS from the 28'th, and everything looks fine :)"
    author: "Rayiner Hashem"
  - subject: "Re: Quiet..."
    date: 2003-06-30
    body: "No problems when pointing to the links? Half resizing fonts...? Are you using the qt-copy from CVS?  I updated on the  27th, and there're reports that even today's (30th) qt-copy snapshot is quite broken, although I haven't tested it (yet). Previously it worked fine.\n\nSee these comments: http://lists.kde.org/?l=kfm-devel&m=105674256705509&w=2\n"
    author: "uga"
  - subject: "Re: Quiet..."
    date: 2003-06-30
    body: "I think most of us are realizing that the only way to bring about all these fantastic changes is to roll up our sleeves and pitch in! Hopefully there is the sound of keyboards tapping away all over the world to improve KDE this very minute!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Quiet..."
    date: 2003-07-01
    body: "i think you fail to realize that users != developers. But without users kde is a \ndevelopers toy. The underling problem when people are bitching (but not clearly) is that confidence in the establishment is lacking. For instance,\nthe default decorations are unusable. While some ppl suggest that its thier\nto impress, but not really be used ( the fuck ), it apitamizes why people dont have the confidence to do anything except suggest that kde developers are not doing enough/the right thing/all the other common complants. \n\ni guess most of that is a rant, but i stand by the first sentence.\n"
    author: "anonymous"
  - subject: "Re: Quiet..."
    date: 2003-07-01
    body: "Obviously users != developers.\n\nBut with an open and free development system like kde, users have the privilege to watch how software is developed. This is useful because more and more we are dependant on software, and we need to know how such things are produced. It gives us realistic expectations, similar to watching sausage being made.\n\nIf you are unsatisfied as to the progress of kde, think it is too slow, remember that this process is probably the most efficient use of scarce resources. With free and open software, if you want a feature, it is literally up to you to make it happen. That could entail purchasing a distro, which supports developers. That could mean bug reporting, testing patches, etc. It could mean directly funding a developer. Or contributing directly in some way, ie. documentation, art, or whatever turns your crank. You'll find that when you are contributing in some meaningful way, others will listen to you. You're ideas will become important.\n\nOr maybe you're suggesting that Dirk drop his work on khtml (and keeping the whole thing building), or Aaron drops his work on usablility. Or Lubos stops working on kwin. Or Ingo drops his very good work on kmail. Just to jump in and satisfy your desires. Good luck.\n\nThere are things that need doing in kde, and aren't being done at the moment. The way things work is someone sees a need and does whatever to get it fixed. Literally. So if there is a glaring lack, jump in and fix it. If you lack skills to do that, learn them. Recruit a friend that has the necessary skills. Your contributions are welcome.\n\nThat is how this process works. If you lack confidence in it, then, hey.\n\nDerek"
    author: "Derek Kite"
  - subject: "Thank you!"
    date: 2003-06-30
    body: "Thanks, don't know waht I would do without my digests!"
    author: "Doina"
  - subject: "Missing commits"
    date: 2003-06-30
    body: "> I must apologize. This week's digest is woefully incomplete.\n\nIsn't it possible to grab the missing parts from http://lists.kde.org or nntp://news.uslinuxtraining.com or nntp://news.gmane.org with a small script/leafnode?"
    author: "Anonymous"
  - subject: "Re: Missing commits"
    date: 2003-06-30
    body: "At lists.kde.org there is a complete log, but in a very difficult format to scroll through and use. news.uslinuxtraining.com doesn't have the complete kde-cvs list. I didn't try the other one.\n\nPart of the problem was lack of time. I started scrolling through lists.kde.org and realized it would take 2-3 hours just to look at the messages. Then a whole lot of time to manually build the links, etc. I just didn't have the time.\n\nWhat happened was my isp was replacing the mail server, and the list server got too many bounces and unsubscribed me. Plus I was out of town for a couple of days. I asked around if someone had the mail log, but again, lack of time to pursue it vigorously.\n\nDerek"
    author: "Derek Kite"
  - subject: "Reveals gruesome ugliness of KDE toolbar icons"
    date: 2003-07-01
    body: "Kopete, like many other applications, reveals how ugly the B,U, and I icons are (of course KOffice suffers from this too).\n\nThat and the numerous *missing* icons on toolbars (the infamous \"white square\") could be fixed by artists with CVS access and ...\n\nSOME LOVE FOR TOOLBARS"
    author: "KDE::Love_Hate"
  - subject: "Re: Reveals gruesome ugliness of KDE toolbar icons"
    date: 2003-07-01
    body: "Yes, but obviously there are no artists who have enough time (or motivation etc) to do this ATM...\n\n"
    author: "AC"
  - subject: "Re: Reveals gruesome ugliness of KDE toolbar icons"
    date: 2003-07-02
    body: "If I had my choice, I'd adopt the icons developed by Jakub Steiner for Ximian viewable at http://jimmac.musichall.cz/. The work he and Tigert do is amazing...have you see their improvements to OpenOffice.org?"
    author: "Eron Lloyd"
---
In <a href="http://members.shaw.ca/dkite/jun272003.html">this week's KDE-CVS-Digest</a>:  Multimedia gets some attention, with fixes to <a href="http://www.kde.org/areas/multimedia/">aRts</a> and artsbuilder. KGhostview now has a
full screen mode. Work starts on a BIDI mode for <a href="http://kate.kde.org/">Kate</a>. <a href="http://cervisia.sourceforge.net/">Cervisia</a>, the GUI frontend for CVS, now has an SSH password 
authentication dialog. <a href="http://kmail.kde.org/">KMail</a> encryption plugins as well as IMAP support is improved. Plus bug fixes and improvements 
in <a href="http://kopete.kde.org">Kopete</a>, <a href="http://www.konqueror.org">KHTML</a>, KWin and many others.
<!--break-->
