---
title: "KDE 3.1: Desktop Sharing in Practice "
date:    2003-02-06
authors:
  - "Phil"
slug:    kde-31-desktop-sharing-practice
comments:
  - subject: "Desktop Sharing needs icons!"
    date: 2003-02-05
    body: "As you can easily see, Desktop Sharing needs better icons. Feel free to create them :)\n(developers who implement the stuff on the TODO list wouldn't hurt either :)\n"
    author: "Tim Jansen"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2003-02-06
    body: "Thanks for the pointer to the \"uninvited connections\" checkbox.  I missed it before when I was looking for a way to share my desktop for my access over the network.  Now I'm serving up the VNC java applet with apache, so I can go to http://mycomputer/login from anywhere and get my desktop!  Neat stuff."
    author: "not me"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2003-02-10
    body: "I might be being thick here, but how do you serve up the java applet with apache?"
    author: "nrp"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2003-02-19
    body: "I am also quite interested about this.  VNC Desktop sharing is nice, but I am the only GNU/Linux box running KDE 3.1, and in fact, almost the only GNU/Linux workstation in the office!  What would be amazing is for the e-mail invitations to give the choice: http based java applet, or local vnc client.  As it is, I suppose I can always customize the e-mail invitation to specify the correct information (ie, point it to the Java applet on my computer) but, I still am not sure how to set it up..."
    author: "Chris"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2003-03-21
    body: "i want to share my desktop on network under win98 in java i need source code can u help me\n"
    author: "loki gupt"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2004-02-19
    body: "i am working on the project for desktop sharing and video conferencing on network under win98,2000 in java i need source code can u help me.\n\n"
    author: "anish k abraham"
  - subject: "need help with video conferencing"
    date: 2005-03-21
    body: "hey there!\nim a student n am working on video conferencing project...\ncan u hel p me...???\nthanks!"
    author: "mikhail"
  - subject: "Re: need help with video conferencing"
    date: 2005-03-21
    body: "I don't think that adding a comment to a news article that's two years old will get you many responses... \n\nYou may want to take a look at http://www.kde-apps.org/content/show.php?content=10395\nThat's the only KDE-related video conferencing project I know of.\n\n"
    author: "cm"
  - subject: "Re: need help with video conferencing"
    date: 2006-05-03
    body: "We are a browser based video & web conferencing company. Our product is very easy to use and cost effective. You can share video, audio, chat AND any show any application you want.\n\nPlease contact me at 877-634-6342 x 101 to get a demonstration of exactly how it works."
    author: "Cee "
  - subject: "Re: need help with video conferencing"
    date: 2006-10-13
    body: "I am also doing project in video conferencing,I am a beginner and need your help so send me the demo(better if it is in VB.NET) "
    author: "sushma karki"
  - subject: "Re: need help with video conferencing"
    date: 2007-05-08
    body: " me too doing project in video conferencing. can u help me with code?"
    author: "anjana"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2005-06-09
    body: "hi!\nFor those who are still trying to prepare Desktop Sharing for distributed Networking on Java Platform, may be i could of some sort of help for them. Feel free to contact me, because i've created one, its a bit slow but it definitely works... and it can even manage more than one system Desktops simultaneously.\n\nBest Regards\nAnish K Abraham"
    author: "Anish K Abraham"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-01-16
    body: "I saw your post o desktop sharing in Java and would like some info. on it if still avaliable or upgraded?"
    author: "william"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-04-07
    body: "i am doing desjktop sharing..if u have some information,help me...."
    author: "anns"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-02-23
    body: "I want to do a project related to desktop sharing. And i have no idea where to start from. So can you help me."
    author: "gulmina"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-04-07
    body: "i too do a project in desktop sharing//// dont have any idea abt dat...if u have some information, send me..."
    author: "anns"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-03-23
    body: "I saw your mail that u have done desktop sharing.I am a fresher.can you help me by  giving some idea about that."
    author: "anju"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-03-23
    body: "Hi Anish,\nIf u have the details of desktop sharing, can u please send to me...\nRegards ,\nLavitha"
    author: "Lavitha"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-04-07
    body: "Hi !\nI dont usually check the replies over here. If you really need help , then try mailing me or join me for a chat at anish_k_abraham@yahoo.com \nI'll be really happy if i could be of any help.\n"
    author: "Anish K Abraham"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2006-05-25
    body: "Hi Anish,\n\nCan you please help me out on how the desktop sharing application can be implemented using Java, like the technology to use at various levels.\n\nThanks & Regards,\nKishore"
    author: "Kishore"
  - subject: "DESKTOP SHARING"
    date: 2006-06-29
    body: "Hi man, I am also doing this project in JAVA. But I still don't know, how to send those frames from Server to Client in RMI. Like we've seen that it's easy to invoke a method on server via interface but How does the Server sends message to Client?"
    author: "Safyan Shah"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2008-05-15
    body: "hey...\nI m doin a proj on video conferencing in java. can i get the source code of it as i m facing problems in codin a bit. It ll help me to understand better"
    author: "prats"
  - subject: "I need help regarding Java code for Desktop sharin"
    date: 2005-10-31
    body: "Hey anish k abraham and gupt,\n\nI too am a student and working on a project which involves Java code for Powerpoint Presentation Sharing between remote computer and Client computers(Almost similar to a Desktop Sharing). Can anyone fo u in the group hel me out?"
    author: "Java_Freak"
  - subject: "Re: I need help regarding Java code for Desktop sharin"
    date: 2008-10-26
    body: "better to use teamviewer application for sharing desktop"
    author: "chetan"
  - subject: "Re: Desktop Sharing needs icons!"
    date: 2003-02-06
    body: "Oh, and I almost forgot:  I want to congratulate you on the attention to usability and user experience you are putting into KDE's desktop sharing programs.  If only every KDE developer cared this much about usability issues!  KDE would become so easy to use a three-year-old could do it :-)"
    author: "not me"
  - subject: "animated demos are cool"
    date: 2003-02-06
    body: "animated demos are very cool. they are a terrific way to show people how the system works, both for PR and for educational purposes. hopefully we'll see more of these demos appear. \n\nthe desktop sharing stuff is awesome, too. one of my clients is using it extensively for tech support purposes and is just loving it.\n\nso Tim: awesome work and thanks for all the code!!\n\ndid i mention how cool that demo was? ;-)"
    author: "Aaron J. Seigo"
  - subject: "swf? wtf?"
    date: 2003-02-06
    body: "Flash? What kind of proprietary junk is that? It doesn't seem to be available for my architecture.\n\nIs there no open source / free software way to do the same thing? What about JavaScript/SVG? Would that be supported in, say Konqueror or Mozilla?"
    author: "Jonas"
  - subject: "Re: swf? wtf?"
    date: 2003-02-06
    body: "The SWF format is open, and there is a free library called library called libflash. It should be able to play the movie. You may need to write a plugin first though. Concerning SVG, afaik there is no free implementation that is mature enough."
    author: "Tim Jansen"
  - subject: "Re: swf? wtf?"
    date: 2003-02-06
    body: "> It should be able to play the movie. You may need to write a plugin first though. \n\nlibflash also has a Netscape plugin. "
    author: "fault"
  - subject: "Re: swf? wtf?"
    date: 2003-02-06
    body: "under http://www.macromedia.com/shockwave/download/alternates/#linux\" you can download the latest flash player player for mozilla(linux) which integrates nicely in konqueror."
    author: "XavierXeon"
  - subject: "Re: swf? wtf?"
    date: 2003-02-06
    body: "You obviously assume that everyone who uses KDE uses Linux. I use FreeBSD. I cannot view Flash movies. I will not entertain any arguments about Linux vs. FreeBSD. \n"
    author: "Muad'Dib"
  - subject: "Re: swf? wtf?"
    date: 2003-02-06
    body: "Quick translation: You use FreeBSD, and you DON'T KNOW how to see flash.\nBecause you actually can."
    author: "Roberto Alsina"
  - subject: "Re: swf? wtf?"
    date: 2003-02-07
    body: "Ohhh yes you can, just not in Konqueror.  A couple of ways...\n\nFirst, you can install /usr/ports/www/flashpluginwrapper that will allow you to view Flash with Macromedia's Linux plug-in within you natively compiled Mozilla.  Works pretty sweet!\n\nSecondly, you can install Netscape 7.0, which is a Linux based port.  I haven't run this one myself, but in theory it should be able to properly embed any and all Linux based plug-ins in the ports tree.\n\nLastly, and most ickily (it's a living language. Deal with it), you can also install the Linux Netscape 4.7x.  Ack, phewy.  Still, the flash plug-in definitely works properly in it.\n\nHeck, with the recent addition of KPlayer to the ports tree you can now even watch Quicktime movies within Konqueror.  Took a little tweaking, but before too long I was watching movie preview right from Apple's web site.\n\nOf course, with all these plug-ins that use Linux binaries that simply can't embed into natively compiled FreeBSD applications their's no good way to get everything working all in one browser just yet.  Now if we could just get some more library wrappers like that flashpluginwrapper us FreeBSD folks would be stylin'!"
    author: "Metrol"
  - subject: "Re: swf? wtf?"
    date: 2003-02-06
    body: "Sorry, no cookie. That is only a binary for Linux/x86 -- I don't want to think about at what speed it would execute under Bochs ;)"
    author: "Jonas"
  - subject: "Re: swf? wtf?"
    date: 2003-02-06
    body: "Whahaha, you suck! Do you homework :P"
    author: "Anonymous Coward"
  - subject: "Window sharing"
    date: 2003-02-06
    body: "I don'&#7787; know if it's possible, but an important feature in desktop sharing is to allow user to see only some programs(windows) like MS netmeeting. I use netmeeting a lot of times per week.\n\nI know that is possible to execute a program an send it's window, mouse and keybord to other X server. It's implemented in the 3.1 solution?\n\nThanks in advance for your great work."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: Window sharing"
    date: 2003-02-06
    body: "No. I havent spent much time thinking about it, but I dont know an easy way to implement it... one of the problems is that the X11 server can only sent screen data that's currently visible.  The only clean ways that I can think off all include writing a specialized X11 server, a X11 proxy or a X11 extension."
    author: "Tim Jansen"
  - subject: "Re: Window sharing"
    date: 2003-02-06
    body: "The X guys say it is a toolkit issue.  They say that with the RandR extension, toolkits have all the information they need to migrate/duplicate a program between X displays.  Maybe support should be added in QT?  I guess that wouldn't be the best practical solution, though, since then only QT programs would be able to be shared; other apps would have to be re-written.  And how would you tell a program to duplicate itself?  Instead of having one program that could migrate others around, every program would have to migrate itself.  An X extension would be better, but it doesn't sound like you'd have the support of major XFree developers."
    author: "not me"
  - subject: "Re: Window sharing"
    date: 2003-02-06
    body: "RandR helps when you want to migrate an app from one X11 server to another (because it can reconfigure the parameters). But it does not help if you want to have the app on two X11 servers simultanously, or to get pixmap of the application's window (which is needed for VNC)."
    author: "Tim Jansen"
  - subject: "Re: Window sharing"
    date: 2003-02-14
    body: "Why not use XVFB and set up user's desktop over there?"
    author: "Andrew"
  - subject: "Re: Window sharing"
    date: 2003-02-07
    body: "This is off-topic really, but would it be possible (in principal even:) to implement a sort of network/pass-through windows display driver. What I mean is, a driver where the calls into it are all passed down to the \"real\" display driver, and also sent off over the network. That way we could avoid all the screen scraping that VNC has to do?\n\nMike"
    author: "mike ta"
  - subject: "Re: Window sharing"
    date: 2003-02-07
    body: "Yes, xf4vnc.sourceforge.net does this (for the screen, not individual windows). There are several ways to do this, but they all require changes in the X11 server or its drivers.\n"
    author: "Tim Jansen"
  - subject: "Re: Window sharing"
    date: 2003-02-07
    body: "Actually, I meant a Windoze driver\nMike"
    author: "mike ta"
  - subject: "Re: Window sharing"
    date: 2003-02-14
    body: "Isn't that exactly what Citrix and WTS do?"
    author: "Andrew"
  - subject: "Security?"
    date: 2003-02-06
    body: "Does the KDE VNC app try to do any ssh tunneling of the VNC connection? It would be very nice if it set this up (semi)automatically for the user."
    author: "derek"
  - subject: "Re: Security?"
    date: 2003-02-06
    body: "IMHO SSH tunneling is a very crude hack, both technically and from the user's perspective (because you need to authenticate twice). A cleaner solution would be to use SSL..."
    author: "Tim Jansen"
  - subject: "Re: Security?"
    date: 2003-02-06
    body: "You don't *have* to authorize twice. you could have krfb bind to only accept local connections, then someone ssh tunnels to the box and it just works. Then using the invitation cookies it would keep out other local users from nabing your session. \n\n\n"
    author: "js"
  - subject: "Re: Security?"
    date: 2003-02-07
    body: "If the VNC server does not require a password, everybody with an account on the machine would be able to join every desktop.\n\nTo solve this you would need some challenge-response technique (for example krfb writes cookie to user's home dir, krdc reads it over ssh and uses the cookie as password). But then the solution becomes more complicated than SSL and not backward compatible with existing vnc servers anymore - thus the only two advantages of ssh would be lost."
    author: "Tim Jansen"
  - subject: "Re: Security?"
    date: 2003-02-14
    body: "Tim,\n\nis it possible (desirable or not) to bind to the loopback interface?\n\nRegards\nLeigh"
    author: "Leigh Upton"
  - subject: "Re: Security?"
    date: 2003-02-19
    body: "ATM kinetd does not support this."
    author: "Tim Jansen"
  - subject: "Re: Security?"
    date: 2003-02-25
    body: "There is definite value in connecting to another desktop on the same host.\n\nThe value in for me would be the seperation of accounts for different purposes: I have an account that I use for work, and a second account I'd like to use for personal stuff. I can log in to both; I can start simultaneous X sessions for both; one can swap between both sessions. By supporting single host connections, one could keep both accounts seperate in the filesystem but in one place on the desktop.\n\nATM, I can VNC from one account to the other but see just a bunch of garbage in the connection window. If loopback support would solve this problem, it would get a vote from me!"
    author: "Alistair Mann"
  - subject: "Re: Security?"
    date: 2003-02-25
    body: "The garbage that you are seeing is a limitation of the X11 driver that you are using. If the screen is not in the framebuffer XGetImage doesn't work. I havent seen a XFree driver that is able to do this. \nAnd for this use case I would rather use Xnest, no need for VNC.\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: Security?"
    date: 2003-02-25
    body: "Tim, thank you so much -- Xnest is exactly what I needed!\n\nThanks also for your work with rdc -- I've found it a vital tool in deal with real VNC (adminning remote Wintel) situations!\n\nAll the best"
    author: "Alistair Mann"
  - subject: "How did you record the demo"
    date: 2003-02-06
    body: "Would like to know what too you used to create demo.  Was impressed with the cursor being captured as well.  \n\nDesktpo sharing looks like a great tool.  It may allow me to drop WebEx which is very expensive\n\nThanks\n"
    author: "Boo"
  - subject: "Re: How did you record the demo"
    date: 2003-02-06
    body: "http://lists.kde.org/?l=kde-promo&m=104430062329339&w=2"
    author: "Anonymous"
  - subject: "Demo Artifacts"
    date: 2003-02-06
    body: "Are the artifacts in the demo due to VNC, the capturing or the conversion to Flash?"
    author: "Anonymous"
  - subject: "Re: Demo Artifacts"
    date: 2003-02-06
    body: "I don't see any artifacts here.. are you using gplflash or macromedia flash?"
    author: "fault"
  - subject: "Re: Demo Artifacts"
    date: 2003-02-06
    body: "It's using /opt/netscape/plugins/libflashplayer.so of SuSE 8.1 here. See the screenshot showing the artifcacts."
    author: "Anonymous"
  - subject: "Re: Demo Artifacts"
    date: 2003-02-06
    body: "swf2vnc has an option '-nommhack' which is a work-around for a problem that you describe - however, when the option is enabled, it doesn't work on those systems that play the movie fine now. (I am using Flash 6, downloaded from Macromedia)"
    author: "Tim Jansen"
  - subject: "Cool!  But what about MY control?"
    date: 2003-02-06
    body: "I think this is awesome.  I do have a couple questions:\n\n1) Is this as bandwidth efficient as tightvnc server?\n\n2) How can I set this up to stay on for more than 1 hour?  I connect via ssh from work and it would be great to use this, however, I don't know of a way to either turn this on via command line (remember I'm in via ssh) and/or leave it on all day and just connect to it from work.\n\nAny opinions/thoughts?\n\nThanks!\n\nJim\nps.  I understand the security implications, but I don't need to be protected via locked out options .. ;)"
    author: "Jim"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2003-02-06
    body: "1. Yes, more or less. It uses the tightvnc encoding, and the compression should be similar to the Windows server (but not as good as the unix/X11 vncserver server, which has far more information about the screen changes). Latency is quite bad because of X11 limitations. This may improve with future XFree versions.\n2. in KControl/Network/DesktopSharing enable \"uninvited connections\" and set a password. The UI is not optimized for this use case though, and I personally prefer a regular vncserver for this use case (because of bad latency and I don't want to be logged in all the time).\n"
    author: "Tim Jansen"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2003-02-06
    body: "1.  uses tight encoding?\nI was under the impression that the vnc implementation in kde-3.1 did *not* include any advanced encoding methods, only the good 'ol classic hextile-n-friends encodings.  Can you confirm the use of tight encoding?"
    author: "Rex Dieter"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2003-02-06
    body: "Yes, definitely :)\n(It is easy to check with krdc in medium quality mode - look at images with many colors and you can see the jpeg artifacts)"
    author: "Tim Jansen"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2003-02-06
    body: "OK,thanks... I had just assumed that the performance problems I had been seeing were related to bandwidth... guess not.  It's just plain slow.  (-:"
    author: "Rex Dieter"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2003-02-06
    body: "You're answer for #2 isn't quite as satsifactory as I'd like. \n\nI had a nice script that would let me say \"remotehost blah.whatever.org\" and it would ssh to the box, setup a ssh tunnel and run krfb. Then it would spawn a local vncviewer that would use the tunnel to securely transmit the data.  It was really slick. \n\nNow I'm not sure how to do the equivlent. There seems to be no way to remotely start krfb for a single session. I'd really rather not have it listening for connections all the time. \n\nAny ideas?\n\nthanks (its a very cool app!)\n\n\n\n\n"
    author: "js"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2003-02-07
    body: "You can enable/disable kded/kinetd using dcop, but I wouldnt recommend this, since it is rather internal (and future krfb versions may have other kinetd modules).\n\nBetter would be to write a command line tool that modifies the configuration (krfb/configuration.cpp) like kcm_krfb does. I accept patches :)\n"
    author: "Tim Jansen"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2003-09-15
    body: "I've been searching all over the net for a way to start krfb from a ssh terminal and this thread is the closer i've got but still can't do it :( . Did someone succeed on this?"
    author: "gonzalo"
  - subject: "Re: Cool!  But what about MY control?"
    date: 2005-04-12
    body: "Here's how I do it (using DCOP as alluded to above):\n\n1) ssh in\n2) make sure your ~/.kde3.4/share/config/kfrbrc (or whatever) allows uninvited connections, and doesn't ask for confirmation for them.\n3) in your ssh session, 'dcop kded kinetd setEnabled \"krfb\" 1'\n4) connect with vnc, krfc, remote desktop etc\n5) 'dcop kded kinetd setEnabled \"krfb\" 0' to stop the service\n\n"
    author: "ant"
  - subject: "What about special keys??"
    date: 2003-02-06
    body: "I've tryed using krfb to connect to another desktop with win2k, but for getting the login screen I've to enter ctr+alt+canc (which is possible using vncviewer) . So, how can I enter this kind of key-combinations in krdf?? I've googled without result and the help does not say nothing about this. So for now i'll continue using vncviewer."
    author: "fredi"
  - subject: "Re: What about special keys??"
    date: 2003-02-06
    body: "In the 3.1 version this is only possible in fullscreen. The CVS version (that will be in 3.2) already has a special-key function in window mode (see the screenshots)."
    author: "Tim Jansen"
  - subject: "Re: What about special keys??"
    date: 2003-02-06
    body: "Good, is it possible to build krdf3.2 with kde3.1 ? "
    author: "fredi"
  - subject: "Re: What about special keys??"
    date: 2003-02-06
    body: "Should be possible, yes. Currently it does not use any new 3.2 features."
    author: "Tim Jansen"
  - subject: "Bandwidth?"
    date: 2003-02-06
    body: "How much bandwidth does this exactly consume? Is it more or less bandwidth than X?"
    author: "Stof"
  - subject: "Re: Bandwidth?"
    date: 2003-02-06
    body: "It is hard to say how much bandwidth it consumes, it takes all bandwidth that is available and adapts the framerate accordingly. In general it is less efficient than X11, especially when you compress X11 using ssh or use lbx. But this depends on the number of pixmaps you transmit using X11. VNC is better at that (if you use a low quality setting and can tolerate the artifacts of JPEG compression).\n"
    author: "Tim Jansen"
  - subject: "Why VNC and not X11?"
    date: 2003-02-06
    body: "So, we have a super-duper-desktop environment running under a network transparent windowing system... so where does VNC fit in? Our windowing system is already networked, so just using programs on other computers should be no fuzz.\n\nAs for connecting to running desktops, that is a bit harder but still no new problem. It is almost a decade old code out there that does this. Why not use that?"
    author: "Jonas"
  - subject: "Re: Why VNC and not X11?"
    date: 2003-02-06
    body: "The main reason for VNC is that it is much easier to implement. It's a small, bitmap-based protocol. \nVNC does not have the capabilities of X11 (e.g. 3D stuff) and it usually consumes more bandwidth. That's why I plan a X11-based system for accessing remote computers (without sharing, one user per desktop). I won't make any promises about the timeframe though. "
    author: "Tim Jansen"
  - subject: "Re: Why VNC and not X11?"
    date: 2003-02-07
    body: "VNC is a remote control applictation; X11 is not:  It's a window system that happens to be network transparent.  2 completely different roles with just a smidge of conceptual overlap, as I understand it."
    author: "Eric Williams"
  - subject: "Re: Why VNC and not X11?"
    date: 2003-02-07
    body: "Yes. VNC would not be able to coordinate several applications that use a common screen, manage windows, let apps do IPC and so on... that would require an additional protocol (or massive extensions).\n\n"
    author: "Tim Jansen"
  - subject: "cambridge (UK) vs cambridge Mass (US)"
    date: 2003-02-07
    body: "ok in a lab in cambridge (UK) they thought that having a low powered machine display whats on the big server hence VNC\n\nok in a lab in cambridge Mass(US) they thought that having a low powered machine display whats on the big server hence X windows\n\nreally the differance after many years is that VNC is a frame grabber and gets the whole desktop and can export MSwindows X11 WinCE Acorn and others while an X can only show an Xserver AFAIK their are no X servers that export the MSwindows desktop let alone a MacOS desktop  \n\nregards\n\nJohn Jones"
    author: "John Jones"
  - subject: "Using Desktop Sharing through a firewall?"
    date: 2003-02-06
    body: "First of all I want to say great job guys.  I love this new feature.\n\nI was hoping someone tell me a way to change the IP address on my invitation.  I am behind a firewall and I need a way to set/type in the correct IP address for my machine.\n"
    author: "Brian"
  - subject: "Re: Using Desktop Sharing through a firewall?"
    date: 2003-02-06
    body: "If you are behind a NAT, getting your IP is quite difficult. But usually it is not even the biggest problem: your firewall will not forward connections to you. Beside manual solutions (looking up the address in the router/firewall, forwarding ports manually), the following developments may solve this problem in the future:\n\n1. the IETF midcom is working on a solution for problems like this. But right now the only result is the STUN protocol, which only works for UDP (VNC requires TCP). STUN also requires a server in the internet.\n2. There is the UP'n'P Internet Gateway Device specification that you can find it at www.upnp.org. If you have a IGD-enabled router (linux-igd.sf.net, some DSL routers and WinXP internet connection sharing), UPnP can be used to acquire the public IP address and forward a TCP port to your computer. IGD has its problems, beside being ugly. For example it does not have any authentication, so anybody in your LAN would be able to reconfigure your firewall. In home networks this shouldn't be a problem though. I looked at it, but it seems to be quite complicated. But if somebody wants to implement it, I will gladly accept the patch :)\n3. If you do the invitation over IM and the remote side is not behind a NAT, it would be possible to create the connection. (Because both sides could negotiate using the IM protocol and then the server creates a connection to the client). But IM invitations are not possible as long as there is no IM infrastructure in KDE."
    author: "Tim Jansen"
  - subject: "Re: Using Desktop Sharing through a firewall?"
    date: 2003-02-06
    body: "Tim,\nI have a simpler question.  I am just using a linksys router where I am just doing IP forwarding and I know what my correct IP address is.  I have been able to connect to my machine using VNC viewer with this address so I know it is correct.\n\nMy Problem:\nWhen I create a personal  invitation the Host(address and display) the IP address is wrong and I just want to be able to hand type/edit in the correct value.\n\nBrian"
    author: "Brian"
  - subject: "Re: Using Desktop Sharing through a firewall?"
    date: 2003-02-06
    body: "The IP address is just a hint for you, so you can tell somebody your address. It is not used for anything else. So if the displayed IP is wrong, just ignore it and tell the invitee the correct one."
    author: "Tim Jansen"
  - subject: "Re: Using Desktop Sharing through a firewall?"
    date: 2003-02-24
    body: "can you tell me what ports you had to forward in order to get this to work? i'm having a difficult time getting VNC to connect through my linksys router, myself.\n\nibbie"
    author: "ibbie"
  - subject: "Re: Using Desktop Sharing through a firewall?"
    date: 2003-02-24
    body: "Hi ibbie,\nOn my machine I needed to forward port 5900 to the machine that was hosting the desktop.  If you want to change the port that host the desktop sharing go into the control panel->Internet & Network->Desktop Sharing.  Choose the network tab and uncheck the automatically select network port."
    author: "Brian"
  - subject: "Re: Using Desktop Sharing through a firewall?"
    date: 2003-05-01
    body: "I setup my Linksys router by opening TCP port 3389. I have no problems using Remote Desktop Sharing. Hope this helps!"
    author: "Thomas"
  - subject: "Problem with kde3.1"
    date: 2003-02-06
    body: "I just installed KDE3.1 three days ago and am having problems with konqueror. It spawns a couple of artsd processes in filemanger profile when it starts even though arts is already running. When I close konqueror the process still remains unless I kill -9 either arts or konqueror. If I kill konqueror only, the arts processes remain and I can't log out until I kill arts. If I restart konqueror without killing arts, konqueror freezes and doesn't close or respond. If on the initial start of konqueror I hold the mouse over a sound file, konqueror freezes too. I installed via rpm's from Mandrake club but since have recompiled arts and kdelibs from scratch and still have the same problem. Other apps such as kaboodle have no problems with arts. It seems to be a problem specific to Konqueror in filemanger profile. If anybody out there has an idea of what to do about this I would be very grateful!"
    author: "Kbeaumont"
  - subject: "Re: Problem with kde3.1"
    date: 2003-02-07
    body: "if you don't need them, try disabling previewing (thumbnails) for sounds and movies ... its flaky here too."
    author: "ik"
  - subject: "VNC invitations for windows"
    date: 2003-02-06
    body: "Is their a windows VNC client that allows invitations compatible with the KDE implimentation.  I would like to set this up to administer my users on win machines."
    author: "Dave"
  - subject: "Re: VNC invitations for windows"
    date: 2003-02-06
    body: "sure.\ncheck http://www.uk.research.att.com/vnc/download.html\n\nnext time try google first before posting, it took me only 1 sec to\nfind the site."
    author: "Mark Hannessen"
  - subject: "Re: VNC invitations for windows"
    date: 2003-02-07
    body: "'think he's axin' 'bout KDE c'patible invitations, yer URL sez nuttin' 'bout dat. I don't know of anything like that. You can, after configuring the server to accept connections, have the user email you the IP after running winipcgf, or go to dyndns.org and get a mywindowsmachinename.dyndns.org name which resolves to the machine's respective dynamic IP. "
    author: "Anon"
  - subject: "Re: VNC invitations for windows"
    date: 2003-02-07
    body: "I was refering to the SLP features of inviting users.\n\nNext time try reading a post before you flame it, but that will take more than 1 sec..."
    author: "Dave"
  - subject: "Re: VNC invitations for windows"
    date: 2003-02-07
    body: "Along this line, does anyone know of a Windows or Mac client which uses SLP to advertize itself, the way krfb does?\n\nAnd is there a way to have krfb do a reverse-name lookup on the client's IP address, so that the hostname shows up in the Address field when browsing (I'm using the 0.7 version, BTW, in case that's been implemented in the KDE 3.1 version)."
    author: "Anonymous"
  - subject: "Re: VNC invitations for windows"
    date: 2003-02-07
    body: "No, krfb is the only server with SLP support ATM. Actually I am happy that way, because I want to change a few things in the service template :)\n(it will be backward compatible though)\n\nYou can announce a service running on another computer with OpenSLP's slpd though. Add the following in your /etc/slp.reg file (replace herry with the name of your computer, and change the description):\n\nservice:remotedesktop.kde:vnc://herry:1;(type=private)%2C(description=Herry),en,65535\nuse default scopes\ntype=private\ndescription=Herry\n\n(Note that the description attribute exists twice)\n"
    author: "Tim Jansen"
  - subject: "VNC is great"
    date: 2003-02-06
    body: "Altough I do not use KDE desktop sharing yet, I like that I am now typing my mail on my other Linux computer from the Windows VNC client here. Monitoring the bandwidth usage, it does use all bandwidth (1 mbit/s) here..."
    author: "Daan"
  - subject: "ni buka jgn tak buka rugi tau"
    date: 2003-02-07
    body: "aku ada kirim desktop"
    author: "Nasry"
  - subject: "Re: ni buka jgn tak buka rugi tau"
    date: 2003-04-18
    body: "Tak meg dit koyu buga. Trupavabi nepo desktop! Gasbud."
    author: "Gasbud"
  - subject: "This is absolutely wonderful!"
    date: 2003-02-08
    body: "I use VNC all the time.  Currently, I use it to abstract my small home network of cheap PCs into one workstation.  I am still using KDE 3.0 on RH 7.3, but when I upgrade to KDE 3.1, I will definitely make use of this!\n\nNow if only sharing files and printers (sambda stuff) was made this easy with KDE."
    author: "Luke"
  - subject: "What is required ?"
    date: 2003-02-09
    body: "In the overview page, nothing is said about the packages that are necessary. I guess that it needs VNC, of course...\nI have the first screenshot, but not the second. VNC is not installed but I have TightVNC installed and it seems similar, as said in the Mdk 9.0 documentation...\nWhat I need to install ? Or perhaps a daemon to activate ??"
    author: "Alain"
  - subject: "Re: What is required ?"
    date: 2003-02-09
    body: "You don't need anything (beside KDE and its requirements), everything is included in the 3.1 packages. There is no daemon to activate, everything is done with kded which is started when you log in.\n"
    author: "Tim Jansen"
  - subject: "Re: What is required ?"
    date: 2003-02-09
    body: "Aaahh... I don't understand, something seems wrong on my install (on my 2 installs...), because in the KDE Control Center, in front of \"Desktop sharing\" I have nothing in the right side... Impossible to have the second screenshot of http://www.tjansen.de/krfb/ So I can only create and manage invitations...\nI have a network with NFS...\n\nBy comparing with the second screenshot, I find that in my KDE Control Center, the entry \"Local Network Browsing\" is missing... Nothing with Lisa..."
    author: "Alain"
  - subject: "Re: What is required ?"
    date: 2004-01-07
    body: "Hi, Tim & others\n\nPerhaps the solution could be to install kdenetwork package, depending on the particular distro.\n\nI have Mandrake 9.2 and Desktop Sharing is not installed by default. I had to add the kdenetwork rpm to get it.\n\nI hope this help."
    author: "Paco Cruz"
  - subject: "Logs"
    date: 2003-02-18
    body: "Is there a way to know which IP's tried to do a remote connection , but failed : eg wrong pwd or if a connection was dropped in any way ?\n\nIs there a log for this ?\n\nrgd,\nPhil"
    author: "Phil"
  - subject: "Accessing KDE without starting it"
    date: 2006-03-26
    body: "Greetings,\nI've been able to access my Fedora Core 5 box from my Windows XP machine using TightVNC. I was successful to do this by having KDE open on my Fedora Box, but I want to have access to that same KDE environment just by having the Fedora box powered on. I need to do this because I don't want to have to connect a monitor + mouse + keyboard to the Linux box.\nIs it possible to do this? If so, what do I need?\nThanks in advance for your help.\nJC"
    author: "Juan"
---
Desktop Sharing is a KDE service that allows you to share your desktop using the RFB protocol, better known as <a href="http://www.uk.research.att.com/vnc/">VNC</a>. This new feature of KDE 3.1 allows a friend or administrator to fix problems on your computer, or you can use it to show your desktop to somebody else at a remote location. It is compatible with all regular VNC / RFB clients. 

Tim Jansen has created <A href="http://www.tjansen.de/krfb/">an overview page</A> where he explains how to set up the Desktop Sharing, together with a nice <A href="http://www.tjansen.de/krfb/demo.html">interactive demo</A>.
<!--break-->
