---
title: "Poll: New Logo for KDE.org"
date:    2003-09-28
authors:
  - "oschmidt"
slug:    poll-new-logo-kdeorg
comments:
  - subject: "Olaf's logo"
    date: 2003-09-28
    body: "Olaf's logo is nice.  I would vote for it but I'm bothered by the \"KDE -\" hanging at the top left of the logo.  It looks bizarre and out of place, a blemish on an otherwise nice and colorful logo.\n\nWhat to do, what to do."
    author: "Anonymous"
  - subject: "Re: Olaf's logo"
    date: 2003-09-28
    body: "> What to do, what to do.\n\nReplace \"KDE\" with \"The\" and everything is fine. I think this is a poll about artwork and not the variable text around it (see women.kde.org and accessiblity.kde.org examples)."
    author: "Anonymous"
  - subject: "Tough choice"
    date: 2003-09-28
    body: "My favorites are:\n\n1. Luciash's logo with Ralf's icons\nher eit jsut seems like it is too light and doesen't provide enough contrast ot the rest of the website, the logo should be something the visitors remember.\n2. Sebastian's logo with Ralf's icons\nSebastian's logo is very good, the biggest problem IMO is that the icons don't fit the logo well.\n3. Ralfs's logo with Ralf's icons\nMy beef with this one is that it seems a bit too overloaded and the part that says \"The KDE Desktop Enviroment\" is way toos trong inr egard to contrast. The other problem is that at the right of the logo it immidieately stops and doesen't blend in smoothly with the white color.  \n\nI will proabbly go with Sebastian's logo and ralf's icons. Hovever, I like ralf's logo better, jsut not the background for his icons.\nAlso, while I want Konqueror to appear on KDE.ORG, I don't think he should eb in the header, but rather next to the \"Conquer your Desktop!\" text."
    author: "Alex"
  - subject: "muddling the waters"
    date: 2003-09-28
    body: "Just to muddle the waters some more, <a href=\"http://static.kdenews.org/mirrors/root66/kdewww-beta-20021112.png\">here's</a> what root66's beta concept for the frontpage was.  In other words, more art on the page than just the logo (or icons). Note that we have the source to the <a href=\"http://static.kdenews.org/mirrors/root66/konqi_teaser.png\">flying Konqi</a>.  :-)\n<p>\nOne step at a time though.  Once we get the logo settled maybe we can add more art."
    author: "Navindra Umanee"
  - subject: "Re: muddling the waters"
    date: 2003-09-28
    body: "drool, that's much better than the current page(s)! :)\n\nAnd Crystal icons fit much better within the page than those icons from this Ralf guy.\n"
    author: "poephoofd"
  - subject: "forgot.."
    date: 2003-09-28
    body: "+100 points"
    author: "poephoofd"
  - subject: "Re: muddling the waters"
    date: 2003-10-02
    body: "Sorry -- I'm not so into it. Like the default KDE color schemes, it's too many light pastel colors for my taste, which makes it tough to read."
    author: "Otter"
  - subject: "Konqui"
    date: 2003-09-28
    body: "The way Konqui is placed next to \"Conquer your desktop!\" is absolutely perfect and that area is exactly like I wanted it."
    author: "Alex"
  - subject: "Re: muddling the waters"
    date: 2003-09-28
    body: "This looks reaallly nice! I'd like vote for this :)"
    author: "Teemu Rytilahti"
  - subject: "Re: muddling the waters"
    date: 2003-09-29
    body: "That's really nice. Although I do think that Dr Konqi is one of the least professional looking parts of KDE. Everything looks great until I log out, when a toy dinosaur pops up."
    author: "James O"
  - subject: "But I like the dragon !"
    date: 2003-09-30
    body: "Everybody is so uptight about \"proffesional look\" - take for example: Mandrake dropped the pretty pinguins in 8 only to put them back due to popular demand in 9.\n\nWhen I showed my girlfriend what Linux can do for her, the major attraction was Konqi, of course. if it wasn't for the fact that we had trouble with her detachable harddrive, she'd be running KDE mostly because of the cuteness of Konqi."
    author: "Guss"
  - subject: "Re: muddling the waters"
    date: 2003-09-29
    body: "Very nice. :D"
    author: "Mikkel Schubert"
  - subject: "kdedevelopers.org broken"
    date: 2003-09-28
    body: "The referenced website (kdedevelopers.org) is broken because it doesn't allow access with enabled Explicit Congestion Notification (ECN) as defined in RFC 3168 :(\n\nWould it be possible to fix this?"
    author: "surver"
  - subject: "Re: kdedevelopers.org broken"
    date: 2003-09-28
    body: "yes yes its known.\nno, no it wont be fixed any time soon.\njust disable ecn for now, since no-one supports it anyway.\n\nsorry\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Herarchical voting"
    date: 2003-09-28
    body: "Hierarchical voting mandatory. Why the heck does everybody offers eliminatory polls?"
    author: "Inorog"
  - subject: "Logo and Icons??"
    date: 2003-09-28
    body: "Why the mix? Extreme example:\n500 Votes - Logo A\n500 Votes - Logo A with Icons\n503 Votes - Logo B\n000 Votes - Logo B with Icons\n\nLogo B wins? \n"
    author: "Sleepless"
  - subject: "Re: Logo and Icons??"
    date: 2003-09-28
    body: "Indeed.  A fair poll should have \"none of the above\" as an option as well."
    author: "anon"
  - subject: "Is it just me?"
    date: 2003-09-28
    body: "I think none of the logos is really nice. They all look too childish and look alike. I would vote for more submissions before voting."
    author: "Vincent"
  - subject: "Re: Is it just me?"
    date: 2003-09-29
    body: "I disagree.\n\nYou had plenty of time for submitting your own artwork.\nWhy haven't you used this time?"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Is it just me?"
    date: 2003-09-29
    body: "Not everyone is an artist, but one is still entitled to think that the current choices suck.\n\nPersonally I liked Olaf's (yours?) artwork.  However, I fully understand that some people want to vote for more submissions."
    author: "arcade"
  - subject: "sebastian logo"
    date: 2003-09-28
    body: "in my opinion the logo is very good as is (or sebastian logo which is very similar) and also the style of the webpage is beautiful as is"
    author: "mart"
  - subject: "Problem with voting"
    date: 2003-09-28
    body: "The very first problem I realize is that there are many similar variations of the crystal logo. On the other hand there is \"Ralf's logo with Ralf's icons\" which looks very different (imho too geeky mixed with the overattempted emphasis on coolness - don't forget that this is meant to be a somewhat corporate website).\nWhile most people might like the crystal version best there might be a poll result like this:\n\n- Sebastian's logo: 18%\n- Olaf's logo: 19%\n- Sebastian's with Ralf's icons: 16%\n- Ralf's logo with Ralf's icons: 20%\n- Other: ...\n\nSo while most people would prefer the crystal logo over Ralf's logo \"Ralf's logo with Ralf's icons\" would have won in this case.\n\nI remember that a similar thing once happened on LinuxWorld when people were allowed to vote for the best desktop. IIRC they had the choice between Ximian Gnome, Gnome and KDE. Gnome might have won if it would have entered the contest with a single entry (both entries added to each other would have resulted in a close win for Gnome). But due to the fact that the votes were distributed on two entries we had luck and KDE won in the end.\n\nAmong the listed entries I think that Olaf's logo looks best.  \nBut imho root66's beta concept looks even better:\n- it doesn't place the konqi (which always looks a little bit childish) in the header but in a very elegant way beneath instead\n- the contrasts and colors are best \n\nTackat\n\n  "
    author: "Tackat"
  - subject: "Re: Problem with voting"
    date: 2003-09-28
    body: "..and most important: it doesn't look like a piece of programmers art.\n\n(like some of the logos in this poll)\n"
    author: "poephoofd"
  - subject: "Re: Problem with voting"
    date: 2003-09-29
    body: "> But due to the fact that the votes were distributed on two entries we had luck and KDE won in the end.\n\nAnd I didn't event get to vote!\n\nWhy where they showing old releases of 3.1 BTW? Why not HEAD?"
    author: "jmk"
  - subject: "Re: Problem with voting"
    date: 2003-09-29
    body: "The organizers of this poll should *really* take note of the above post!"
    author: "anon"
  - subject: "Re: Problem with voting"
    date: 2003-10-01
    body: "What is needed is to use \"approval voting\" where you are allowed to vote for more than one -- vote for all of the ones that you like.  You count the votes the same way.\n\nMore complicated is \"rank voting\" where you can rank them in order.  Picking the winner is more complicated with this method.  The general consensus is that assigning points to rank voting doesn't work.  So a computer needs to determine the winner with multiple elimination rounds -- the \"musical chairs\" method.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Problem with voting"
    date: 2003-09-29
    body: "Please listen to Tackat, the pro artist.\nLusiach's logo is beautiful.\nBut you can hardly distinguish it from the white background.\nOlafs icons are easy to look at and somewhat playful."
    author: "OI"
  - subject: "Just a few impressions on the options."
    date: 2003-09-29
    body: "Let's just say none of these would pass Apple's Human Interface or NeXT Interface Design requirements.\n\nSebastian and Luciash should join ideas and combine their strengths, respectively. Both the K's are oversized and take away from the gear-like Sun emblem.  Neither convey a sense of Desktop Environment.\n\nAre we conquering as in Victory, symbolically? Or are we symbolizing the overtaking of one's Desktop metaphorically within the X11 environment?  Ever thought of conquering X with K in a sort of 3-D effect on the icon? \n\nOh I don't know project an X drop shadow extending behind the K that projects with Lighting effects to symbolize the K conquering X11?\n\nRalph's icons are nice to signify site sections.  I would change the blue theme and/or make a series of table header color options that contrast with those icons and say add some color diversity. For example, the KDE Family could have different colors for each of the 3 persons being symbolically represented.\n\nHotspot conveys Thermal expansion not contraction (ice).\n\nDevelop is too 1 dimensional.  The limiting size of the icons don't give one much room to add enrichment.\n\nDo they have to be square in dimensions?  Our monitors aren't square.  Give it a ratio traditional to most monitor settings for example.\n\n"
    author: "Marc J. Driftmeyer"
  - subject: "choose a non-tech style"
    date: 2003-09-29
    body: "The logos are also flawed in two other respects. First, because of the \"gear\" theme which speaks to technologically minded people. Most users haven't got a fascination for technology and will find this style cold and alienating. A style should convey \"human\" values. The developers doesn't take this a point like this seriously because they have no intention of communicating with the user in the first place, they make this for themselves.  Secondly because the K superimposed on a gear is simply unelegant - a logo should ideally just be a simple form."
    author: "will"
  - subject: "Re: choose a non-tech style"
    date: 2003-09-29
    body: "I wouldn't associate a gear with technology. At least not more than a wheel or a hammer or a flint stone..."
    author: "Jeff Johnson"
  - subject: "Re: choose a non-tech style"
    date: 2003-10-01
    body: "That's an interesting set of observations.  While agree with you about the need for more touchy-feeliness in KDE in general, and especially on the homepage, I think your point that a logo should be simple form is a stronger imperative.  To my mind, the gear is actually a pretty good starting point - it is a simple and universally recognized symbol, and stands for work and (industrial) strength and basic elements that are needed to make things happen.  The more we can get people to think of KDE when they see gears, the more brand association we've achieved.  That leads me to think that we should gradually diminish the size of the K associated with the gear so that the gear becomes the true KDE brand.\n\nTo address the question of touchy-feeliness, the curve in Luciash's logo had a much more organic feel.  The page as it stands is built on squares, to the point of being blocky.  A couple rounded corners and a logo with a watery wave would go a long way towards that.  A combination of the sun-gear (perhaps with the K de-emphasized a little bit) and the wave shape of Luciash's logo, and some rounding of the inset boxes would go a long way to make the KDE homepage warmer."
    author: "Electronic Eric"
  - subject: "Konqess"
    date: 2003-09-29
    body: "I really like the female Konqi.  I honestly don't know Olaf is, but, he/she/it did a great job on that one.  \n\nAs far as teh KDE logo goes, please, can we get away from blue?  OK, OK.  I don't have any better ideas at the momment, but the whole blue thing has to go.  I'm color blind and all, but, the constant blue theme is just nasty.\n\nThis is just my opinion.  Any artists that have developed graphics for KDE *PLEASE* don't take any offence, but damn...\n"
    author: "Xanadu"
  - subject: "Re: Konqess"
    date: 2003-09-29
    body: "> I really like the female Konqi. I honestly don't know Olaf is, but, he/she/it did a great job on that one.\n\nThe Katie drawing is old (did you ever visit http://women.kde.org?) and is done by Agnieszka Czajkowska.\n "
    author: "Anonymous"
  - subject: "issue?"
    date: 2003-09-29
    body: "The logo of KDe is fine. there are other graphics in KDE, like in the Game package that have to be improved. I don't understand this. KDE's current logo is perfect."
    author: "gerd"
  - subject: "Luciash's"
    date: 2003-09-29
    body: "You can not seriously mean to choose this one.\nDo you know how much you stress your eyes with\nthat little contrast?\n\nClearly. Olof's icons are better. Easier to look at,\nthink about people with eye-defects! They won't even\nbe able to recognice Luciash's logo from the background!"
    author: "OI"
  - subject: "Go Konqui!"
    date: 2003-09-30
    body: "Go Olaf!  Konqui is really an advantage for KDE.  It's original and I don't know nobody who don't like it when I show KDE to other people.  It adds a touch of humanism in a very technical thing (a desktop environment is a technical thing for the majority of users)."
    author: "Dominic"
  - subject: "Some ideas from the past:"
    date: 2003-09-30
    body: "I always thought the KDE.org 1.x website was the cleanest out of the three:\n\nthe KDE webpage four and a half years ago, right when kde 1.1 was released : http://web.archive.org/web/19990423162622/http://www.kde.org/index.html"
    author: "anon"
  - subject: "(OT) apps.kde.com"
    date: 2003-09-30
    body: "I know this is off-topic.\n\nAnybody know whats happen to apps.kde.com ?\n\nIs he in vacation ?\n\n"
    author: "Somekool"
  - subject: "Re: (OT) apps.kde.com"
    date: 2003-10-01
    body: "It's back with new applications."
    author: "Anonymous"
  - subject: "the flying konqi"
    date: 2003-10-01
    body: "\n\ndont know if he will be the header but the flying konqi\nis the best.\n\n"
    author: "patrick_darcy"
  - subject: "Anyone know what happene dto mosfet?!"
    date: 2003-10-01
    body: "He made an awesome image program, styles, and a website ;)\n\nBut, i can't seem to see a trace fo him any more."
    author: "jace"
  - subject: "New Logo for KDE.org"
    date: 2003-10-01
    body: "Root66's design is the best I have seen so far"
    author: "Michael Wright"
  - subject: "Re: New Logo for KDE.org"
    date: 2003-10-01
    body: "i dont see Root66 anywhere on kde.org/testing.php\n\n"
    author: "somekool"
  - subject: "Re: New Logo for KDE.org"
    date: 2003-10-01
    body: "Read the rest of the thread here and you'll find it. Tip: CTRL-F lets you search for text on a website..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: New Logo for KDE.org"
    date: 2003-10-02
    body: "Yeah, that design is nice. One big fundemental problem though; KDE is free software, not Open Source."
    author: "mwg"
  - subject: "remove the crocodile"
    date: 2003-10-01
    body: "I would suggest to remove the crocodile (or may be I should call it krokodile) aka konqui - may be it's a dinosaur or mozilla monster whatever but it affects the reputation of kde. Gears as a symbol of kde are good enough btw.\n\nAs for the designs ralf's design looks best from the listed options however i would suggest to remove the black area it looks inconsistent.\n\nIn a conclusion I must say that kde needs a \"image\" for the new release to impress people (as you already did very well with aqua like keramik interface) and of course a reputation of a enterprise environment and professional desktop.\n\nI hope this helps."
    author: "Anton Velev"
  - subject: "Re: remove the crocodile"
    date: 2003-10-01
    body: "\"I would suggest to remove the crocodile (or may be I should call it krokodile) aka konqui - may be it's a dinosaur or mozilla monster whatever but it affects the reputation of kde. Gears as a symbol of kde are good enough btw.\"\n \nI've got mixed feelings about this. The pinguin hasn't hurt linux at all.\nJ.A."
    author: "J.A."
  - subject: "Re: remove the crocodile"
    date: 2004-10-24
    body: "i like this game sssssssooooooo much\n"
    author: "Brenda"
  - subject: "Re: remove the crocodile"
    date: 2003-10-01
    body: "Konqui is a dragon!"
    author: "Anonymous"
---
The KDE Web Team has set up <a href="http://www.kdedevelopers.org/node/view/206">a public poll</a> to determine the <a href="http://www.kde.org/testing.php">new logo</a> for <a href="http://www.kde.org/">KDE.org</a>. The poll is open for two weeks, starting September 28th, 2003.  Please be sure to test the logos with different styles before voting.
<!--break-->
<p>
As frequent readers of this site might remember, the KDE Web Team <a href="http://dot.kde.org/1048968963/">asked for submissions</a> for a new website logo some time ago. A major requirement was that all source materials of the logo should be available, so that it is possible to create custom logos for the kde.org sub-domains in the same style.
<p>
From the 34 submissions received by the KDE Web Team, four logos were selected for this vote. The source for the old logo has been made available as a result of the submission request and is among the four picked logos. Icons for the subsections of kde.org have also been submitted and are open for a vote. 
<p>
The KDE.org website allows visitors to select their prefered colour style in the preferences, so please <a href="http://www.kde.org/testing.php">test the logos</a> with different styles before you <a href="http://www.kdedevelopers.org/node/view/206">vote on the logo</a> you prefer. There will be a second vote on the default color style afterwards.