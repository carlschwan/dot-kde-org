---
title: "Linux Journal: Interview with Matthias Ettrich"
date:    2003-04-30
authors:
  - "binner"
slug:    linux-journal-interview-matthias-ettrich
comments:
  - subject: "cool"
    date: 2003-04-30
    body: "I can't even imagina a better QT than QT3. This tool is really awesome, and it's improvement reflects directly on KDE.\n\nAnyway, first post?"
    author: "protoman"
  - subject: "Thanks"
    date: 2003-04-30
    body: "Thanks Derek, great job ;-)"
    author: "Anon"
  - subject: "RTFP"
    date: 2003-04-30
    body: "man the dot is getting worse than slushdot.  At least there they read the post before responding...\n\nRTFA should be RTFP here :)"
    author: "Anon"
  - subject: "Re: Thanks"
    date: 2003-04-30
    body: "LOL"
    author: "me"
  - subject: "Re: Thanks"
    date: 2003-04-30
    body: "hehe that's funny. DON'T DO IT AGAIN."
    author: "anon"
  - subject: "Re: Thanks"
    date: 2003-05-01
    body: "I think that you who are posting 'thanks derek' - are actually the derek himself\n(or someone who wants to promote or kid with derek)\n\nor even some kind of very stupid bot!\n\nanyway it's funny and it's working! derek is now more popular than his cvs digests!"
    author: "AC"
  - subject: "Re: Thanks"
    date: 2003-05-01
    body: "Will you post \"thanks derek\" on the next article of the dot?"
    author: "AC"
  - subject: "Qt-only KDE applications"
    date: 2003-04-30
    body: "What happened in the end with this proposal of Matthias (regarding KDE as a supported platform of Qt)? http://lists.kde.org/?l=kde-core-devel&m=101170367405607&w=2"
    author: "Anonymous"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-04-30
    body: "I guess nobody had the time/funding to do it."
    author: "AC"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-04-30
    body: "It will be one of the topics on the KDE Developers' Conference 2003 in Nov\u00e9 Hrady this August."
    author: "Datschge"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-04-30
    body: "Someone on this list must have read the dot. :-)"
    author: "Anonymous"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-04-30
    body: "Really?  hmmm...  know I really would like to go.  Anyone know of whom I could ask to be my sponsor?"
    author: "Benjamin Meyer"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-05-01
    body: "QT must include in its toolkit some of the improvements made by the kde team (at least the kdelibs must be merged with qt codebase and bundled with qt).\nSome things that qt may include are:\n\n-KHTML (and use safari on OSX and MSIE on Win (until there is no khtml for Win))\n-KJS (instead of reinventing the wheel and oposing kde with its GPLd QSA)\n-DCOP\n-ARTS\n-KTextEditor component (also other standart KDE componetns that are definitely better than their QT opponents)\n-KDE standart dialogs (as Mattias said file dialog, print dialog) - and on Mac and -Win their own dialogs\n-KDE print system\n-KIOSlaves\n-KParts (and ofcourse may be some of the stable KOffice Kparts)\n-integrate or fund the integration of KDevelop with QT Designer\n-other you suggest\n\nIn a conclusion I'd like to add that QT forces some opposition (with GPLd license) of the standart kde libs and comps (which are LGPL), which is reasanoble as they have to make money by selling licenses. However we know that noone can make commercial app using only the KDE components - so I beleive that they have no reason to just combine them (not really sure about the other licensing issues and of course someone (QT) should fund it).\nSoon or later this is going to happen - else community will free one-by-one the KDE libs and comps from QT GPL usage, as it happened with KHTML/KJS - Apple helped to free them from QT. So if QT does not start integrating KDE libs with it's software, community would bash it out soon or later.\n\nOne thing must be sure! QT should continue its policy of dual licensing to make money (and improve the software) - weither or not ingegrated with KDE, and both projects have interest this to happen - kde apps for reaching the commercial market and QT to not reinvent the wheel and not staying in opposition to KDE.\n\nMattias? What you would say about it?\n\nAnton\n\nPS1: relevant article\nhttp://dot.kde.org/1047362149/1047380146/1047455315/\n\nPS2: actually i think that this is LGPL vs GPL issue"
    author: "Anton Velev"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-05-01
    body: "> -KJS (instead of reinventing the wheel and oposing kde with its GPLd QSA)\n\nQTScript (The scripting language of QSA) implements a newer version of EMCA Scripts (i.e. it supports a real OO paradigm, whereas kjs only knows about OO using a prototype function). The fact that QScript is GPL'ed is not the problem, the problem is that it is considered a binding under certain circumenstances (if you offer full access to a GPL'ed library, otherwise you could circumvent the GPL this way). It would be the same if you used KJSEmbed (which is the kjs alternative to QtScript). It's in no way \"opposing\" kjs, maybe kjsembed at best (which didn't exists when QSA development was started).\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-05-01
    body: "I'm not sure that QSA was started before KJSEmbed actually. It's also worth noting that KJSEmbed supports a number of things that QSA doesn't and vice-versa (though that is changing as I add the QSA features to KJSEmbed).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Qt-only KDE applications"
    date: 2003-05-02
    body: "Even if you are right, why then there are 2 same functionalities instead of having one that is better than both?\nAnd also don't you find a split that happens - mostly KDE apps should use KJS (as KDE invented it, and konqueror will use only KJS forever), but in the same time as far as I know kexi (the new KOffice Access tool) is using QSA and not KJS - so what would be the standart for KDE then?\nAnd is it possible someday to have them merged? I guess that even it is possible to merge the codes it's definitely impossible to merge the licenses (GPL vs LGPL).\n\nAnton"
    author: "Anton Velev"
  - subject: "what about a RAD for Qt/KDE?"
    date: 2003-04-30
    body: "the same: What about a RAD for QT/KDE? i would like to see something similar to Glade for Qt/KDE and integrated into Kdevelop. As we know, this can be compiled for Linux, Windows or Mac (multiplataform). I think this would be the definately step to achieve \"conquer\" one of the best place of programming tools, close to Borland or Microsoft, and knowing that are Open tools. Is my wish list :)"
    author: "anonymous"
  - subject: "Re: what about a RAD for Qt/KDE?"
    date: 2003-04-30
    body: "Qt Designer. Not quite integrated, but it gets the job done.\n"
    author: "Sad Eagle"
  - subject: "Re: what about a RAD for Qt/KDE?"
    date: 2003-04-30
    body: "Ummm have you ever USED visual studio?  Its very disintegrated when it comes to doing anything more advanced than the standard wizards allow.  Maby I have been using MetroWerks way to long, but the second I try to change a library path in MSVC my project is toast.\n\nNow flip over to KDevelop.  its more modular that MSVC, supports Java, Python, Perl, etc..  Qt Designer is external but KDevelop 3.0 will allow you to subclass .ui files and to see members in your class browser.  I cannot even get MSVC to do that with their own damn dialogs.   If you have problems with the concept of something as simple as Qt Designer you need to stay as far from programming as physicly possible.\n\nI know there are script kiddies that want to click and drool together Quake 3 here, but its not going to happen.  Not on windows and not on Linux.  We have support of VERY highlevel languages like Python w/ PyKDE/QT (both work VERY well in KDevelop3)  and very low level languages like assembly.  That too works in KDevelop 3, although at that level a texteditor will do more good.\n\nPlease think before you post.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: what about a RAD for Qt/KDE?"
    date: 2003-04-30
    body: "i do not agree. i think better integration between QT designer and kdevelop is very important, and will cause better and more software. I also think the kdevelop guys would think about it as soon as QT designer becomes modular and embeddable (i don't think it is right now, but i could be wrong)\n"
    author: "ik"
  - subject: "Re: what about a RAD for Qt/KDE?"
    date: 2003-05-01
    body: "Of course integrating is very important. Yes, most of coders ine KDE world are very familiar with KDevelop and Qt Designer. It may be very easy for them to use. But for new programmers (like me) it is not very comfortable. I'm a Delphi programmer and I have really good -and running- projects on Windoz. I don't have time to learn C++, so Delphi will be my programming tool so more. I also like Kylix 3. But with Kylix I cannot make native KDE programs. As a new feature in recent CVS Digest, there was a Delphi support but what about QT Designer integration? I think if KDevelop gets integrated QTD, KDE will get more killing applications. \nThanks to all KDevelop contributors.\nBye!\n\n\n\n"
    author: "thelightning"
  - subject: "Re: RAD for KDE? - Kommander - quick mini"
    date: 2003-05-01
    body: "Kommander is another thing that is useful because it is focused on being a user level RAD for mini apps and dialog extentions of KDE apps. Kommander is based on QT Designer and produces a ui file, using modified KDE widgets, that can be run by it's executor.\n\nKommander is:\n- visual dialog design\n- maniuplates text associated to it's widgets\n- speaks DCOP\n- has embedded shell scripting\n- can call external scripts\n- has a very limited event model\n\nKommander development objectives include\n- more widgets, especially more complex ones\n- data widgets\n- enhanced object model\n- possible inclusion of kjs\n- enhance design features\n\nKommander is useful for quick feature add on prototypes, non compile mini apps, dialog extentions and \"application glue\" for merging KDE apps into seamless operation.\n\nKommander is included with Quanta+ from version 3.1. Anyone interested in helping with the ongoing development please contact me. Kommander needs developer help and should be very useful for all KDE apps."
    author: "Eric Laffoon"
  - subject: "Why C++?"
    date: 2003-04-30
    body: "While reading this _very nice_ interview this thought poped-up:\n\n   Why C++ as native language for KDE and not Objective-C?\n\nIt maybe a ridiculous question, but it is a question that bother me a bit...\n\nCheers,\nCies.\n\n_________________\nDISCLAIMER: I'm no programmer ;-)"
    author: "cies"
  - subject: "Re: Why C++?"
    date: 2003-04-30
    body: "I guess the answer is that Qt is written in C++ and not Objective C. If you go on to ask \"why is Qt written in C++\", the answer would be that \"everyone\" use C++ and it's a standard language for things like this. Objective C may be nice but its not nearly as widespread as C++."
    author: "Apollo Creed"
  - subject: "Re: Why C++?"
    date: 2003-04-30
    body: "Four simple points:\n\n- Back in 1998, ObjectiveC in gcc sucked (apple did a lot of work on it since they use it)\n- Lack of toolkits for ObjectiveC\n- You get the object introspection with C++/Qt as well\n- There are more people that know C++"
    author: "Anon"
  - subject: "Re: Why C++?"
    date: 2003-05-01
    body: "And, AFAIK, it lacks features like templates."
    author: "Tim Jansen"
  - subject: "Re: Why C++?"
    date: 2003-05-01
    body: "And that's a bad thing?!"
    author: "Will Stokes"
  - subject: "Re: Why C++?"
    date: 2003-05-01
    body: "Yes, Templates make your life a lot easier because you implement template classes and template functions once to use it for a whole range of classes."
    author: "Daniel Molkentin"
  - subject: "Re: Why C++?"
    date: 2003-05-01
    body: "In Objective C you do not need Templates for that thanks to the dynamic\nnature of the language's object system.  Besides, Objective C++ exists."
    author: "anonymous"
  - subject: "Re: Why C++?"
    date: 2003-05-01
    body: "So? Qt also adds a dynamic object system to C++. While it might to be as well integrated, there are a lot occasions where templates are just better..."
    author: "Daniel Molkentin"
  - subject: "Re: Why C++?"
    date: 2003-05-02
    body: "Funny...  Someone claims Objective C would have been a bad decision for KDE's (or Qt's) implementation language because it does not have templates. I tell him, that Objective C doesn't need templates and then you say (correctly) that Qt doesn't even use them and add it's own Objective C like dynamic type system.\n\nOh, the irony ;o)"
    author: "anonymous"
  - subject: "Re: Why C++?"
    date: 2003-05-02
    body: "Dynamic polymorphisms and templates are /not/ equivalent. \n"
    author: "S.E."
  - subject: "Re: Why C++?"
    date: 2003-05-01
    body: "That depends on much you like to copy&paste your code...\n"
    author: "Tim Jansen"
  - subject: "Re: Why C++?"
    date: 2003-05-01
    body: "Templates bring two big advantages:\n* generic programming.\n Many algorithm are indifferent to the (numeric) data type. Thus you can write an\n implementation, test it and use it where you have to. A good example is the STL.\n Especially the container classes speed up software development, since you don't\n have to develop e.g. a linear list if you need one. Copy&Past is not a solution, since\n - you have to alter the code - this may introduce subtile errors.\n* template specialization make it possible to select the best implementation/algorithm transparently. (i.e. list<int *> may use a different algorithm than list<int>\n* template libraries like the STL commonly uses unified interfaces, for example: most of the STL container classes can be interchanged.\n* By using the template mechanism to select algorithms depending on data types or to achieve compile-time bindings etc. one can improve the speed dramatically.\nSee the Blitz++ matrix template library for example. Using these techniques they've beaten Fortran77!\nOne example: Use template specialization to select an algorithm dependend on an integer (e.g. filter order for digital filters). Then you can implement different code dependend on the filter order, e.g. N=1..8: hand-written, unrolled loops; 8-16: time-domain convolution with loops, N > 16: FFT-based convolution.   "
    author: "Ruediger Knoerig"
  - subject: "GNOME #1?"
    date: 2003-05-01
    body: "Why did he asked why GNOME is the standard?  Matthias asked if he meant because of SUN.  Is it the general consesus that GNOME is the standard now?  I have been away from Linux/KDE for a while and was suprised to read that.\n"
    author: "Robert Williams"
  - subject: "Re: GNOME #1?"
    date: 2003-05-01
    body: "Some people only know/use RedHat (for patriotic or other reasons) and as RedHat defaults to Gnome they may think of Gnome as THE desktop like of RedHat as THE Linux distribution. But, does it really matter when most other distributions, especially the desktop-oriented ones (Yoper, Mandrake, Knoppix, Lindows, Xandros, Lycoris, Ark) default to KDE?"
    author: "Anonymous"
  - subject: "Re: GNOME #1?"
    date: 2003-05-01
    body: ">But, does it really matter when most other distributions, especially the desktop-oriented.....\n\n Unfortunately: Yes.\n Reason: A totally unreasnobly high amount of the computer professionals who have only used Win23 only \"know\" two things: \n\n1: \"Linux is manufactord by RedHat - everyone else are just knock-offs\"\n2: \"IBM says Linux is good enough for servers - But IBM and M$ are enemies.. so errr..\"\n\n So lots and lots of people doesnt even see the other distroes as \"desktop-oriented\" but only as some kind of paralel-branding.\n I know how horrid it sounds but ive been fighting it endlessly.. and this is LinuxFreindly(TM) Denmark!! I'll probably get a heart-attac if was to discuss this matter with a group of long-term M$ professionals in a large company in say Texas ;-P\n\n Another thing ive noted is that it is actually unwise to tell \"them\" of the wonderfull freedom of choise when it comes to DTE... as many of them replies something like: \"If they can't even agree on a common *desktop*, I wont even imagine how messy everything else must be!!!\"\n\n So yes.. we need RedHat to change the default desktop. But I [do personally in my own mind [etc]] think they will do so on their own accord over the next 1-2 years, as KDE is gaining more developers/testers and allso has a great deal more functionality per line of code then the GNOME project...\n\n Wouldn't it be great if RH would be so kind ad do a RedHat Linux Workstation with KDE and a RedHat Linux Multiserver with GNOME?\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: GNOME #1?"
    date: 2003-05-03
    body: "> But I [do personally in my own mind [etc]] think they will do so on their own\n> accord over the next 1-2 years, as KDE is gaining more developers/testers\n\nOf course they will!\nAnd it has already begun.\nDon't you see the obvious point behind \"BlueCurve\" (hell, it's even in the name!)?\n if KDE ought to become the default RedHat desktop, then it *must* be an american product. Period.\n\nIt's the same logic as movies. Do you think any American has ever heard about \"Open your eyes\" by Amen\u00e1bar? Nope. They saw \"Vanilla Skies\", an American movie featuring Tom Cruise - same story but, please, clean the streets, clean the thoughts, cut the balls, for nobody in God Blessed America should ever know about mankind diversity.\n\nAnd of course, send this POS back to Spain thereafter, because frankly, America ought to be enough for everyone.\n\n\n\n\n "
    author: "Stretch"
  - subject: "Re: GNOME #1?"
    date: 2003-05-01
    body: "The standard would be freedesktop.org which works for standards and interoperability. RH has not focused on desktops until lately and even so people who start on RH (like I did) do not stay there. I tried other things. For one thing I noticed a box of CDs cost twice as much from then as it did other distros. Now I run Gentoo because I hate the RPM treadmill.\n\nAs far as usage goes, it's difficult to get an exact number, but every survey to date that seemed the least bit credible gave KDE a substantial advantage in users. Even in core Gnome developer discussions the consensus is that KDE leads in many areas, in fact dramatically so. Gnome's founder found the computer lab he started Gnome in running KDE on his last visit. KDE is also much better suited to commercial ventures with kiosk mode and some of the modifications being sponsored by the German government. Gnome has also alienated a number of users with recent removals of configuration options too. If a desktop is gaining on Linux it would appear to be KDE... though who really cares who's ahead as long as you have what you want?\n\nIn an ideal world both desktops will flourish and interact. When Gnome picked up it's big partnership with Sun and all everyone said that was it... at the same event they announced it KDE was again overwhelmingly voted the best desktop. In the end it is not really going to be possible to establish a standard by corporate fiat just as M$ will not be able to hold it's dominance forever by it's power. People will continue to choose what they like based on their exposure. The Linux standard should continue to be choice."
    author: "Eric Laffoon"
---
<a href="http://www.linuxjournal.com/">Linux Journal</a> features an <a href="http://www.linuxjournal.com/article.php?sid=6834">interview with Matthias Ettrich</a>, the founder of the KDE desktop environment. Aleksey Dolya interviews Matthias about the process of creating KDE and what he's up to these days. Asked about current popularity of KDE, Matthias answers: <i>"Beyond all expectations, really. We see more and more contributors, more languages, more applications. And we see KDE code move onto other platforms. Recently Apple came up with their new browser for the Macintosh, which they built on top of KHTML and KJS, two of the more important pieces of KDE technology."</i> Matthias is also appearing together with Lars Knoll (and Konqui) in the <a href="http://www.trolltech.com/video/signalsslots.html?cid=2">"Qt3: Signals and Slots"</a> video of Trolltech's video section.
<!--break-->
