---
title: "KDE/Mac Development Needs You"
date:    2003-12-14
authors:
  - "pem"
slug:    kdemac-development-needs-you
comments:
  - subject: "port?"
    date: 2003-12-14
    body: "> Konsole has already been ported, but actually doesn't refresh right.\n\nIt brings me this question: when can one speak of a 'port' ;-)\n\n"
    author: "cies"
  - subject: "Re: port?"
    date: 2003-12-14
    body: "When something significant is changed, meaning to have to write new extra code to support it.\n\nExamples:\n- compiler (from GCC to another)\n- CPU (from i386 to PowerPC, if you have luck, the compiler has already done all the job)\n- OS (from Linux to MacOS)\n- Window environment (X-Window to MS Windows)\n- main libraries (MFC to Qt)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: port?"
    date: 2003-12-15
    body: "I forgot: with CPU changes, you might also get those little versus big endian problems.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "It's not so much that it's not active..."
    date: 2003-12-14
    body: "...but I'm nearly the only person that's been working on it, and I've been active with tons of Fink stuff.  =)\n\nMuch of getting things working with Qt/Mac is a lot of grunt work #ifdef'ing things, so it should be relatively easy for someone else to jump in and help out.  Last I tried a week or two ago, the latest patch in my patch dir pretty much applied cleanly still, but the new kwin bits need updating to build."
    author: "Ranger Rick"
  - subject: "Re: It's not so much that it's not active..."
    date: 2003-12-14
    body: "Just make sure that you and who ever is in your project follow the HIG for the Mac. I mean nothing looks worse on Mac OS than an app that was clearly not developed for the mac (look at fire bird for an example)"
    author: "blah"
  - subject: "Re: It's not so much that it's not active..."
    date: 2003-12-14
    body: "It'll be a long time, if ever, that it matches the HIG.  For one thing, no matter what you do, it's still a UNIX app, not a mac app.  Also, it's governed by what Qt does, so there's only so much you can do in rearranging things to fit apple's guidelines.\n\nFeel free to jump into the code and prove me wrong, though.  ;)"
    author: "Ranger Rick"
  - subject: "Re: It's not so much that it's not active..."
    date: 2003-12-14
    body: "On top of that, the KDE guidelines take precedence anyways.\n\nRick: I joined the list and I'll try to maintain an up-to-date tree when possible.  I don't know how much I can contribute, but as I see bugs that I can fix, I will do so."
    author: "George Staikos"
  - subject: "Re: It's not so much that it's not active..."
    date: 2003-12-14
    body: "That's not the responsibility of the developers doing the ports but rather that of the KDE framework and its flexibility. I'm sure that once KDE is running on OSX people will find ways to adapt KDE to the Apple HIG they like without changing (much, if any) code."
    author: "Datschge"
  - subject: "Why"
    date: 2003-12-14
    body: "I guess I miss the point of this, but why would someone want to port KDE apps to Mac?  Mac appears to have a lot of great apps already, and most mac users would most likely continue using them rather than go to KDE stuff.  Honestly I would rather see anyone that can code coding for the Linux version.\n\nOh well, just because I don't get it doesn't make it uncool.  Good luck."
    author: "sphere"
  - subject: "Re: Why"
    date: 2003-12-14
    body: ">and most mac users would most likely continue using them rather than go to KDE stuff.\n\nGood point. I can not imagine why apple users would want something like koffice, scribus, or knoqueror; They have MS Office, Adobe, and MSIE.\n\nOh wait. MSIE was withdrawn and now they have safari. But safari is based on ??????\n\n\nMS has been threatening to pull Office for quite sometime. I am waiting to see what OO and Koffice do to those threats.\n\nLikewise, Adobe and Quark were slow porting to OSX. Scribus will almost certainly give them reason to rethink their approach to OSX, and the OSS world.\n\n\n>Honestly I would rather see anyone that can code coding for the Linux\n version.\n\nActually, I agree with this, but that is me.\n \n"
    author: "a.c."
  - subject: "Re: Why"
    date: 2003-12-14
    body: "I use OS X now and then, and I'd love to replace that idiotic Finder with Konqueror."
    author: "Boudewijn Rempt"
  - subject: "Re: Why"
    date: 2003-12-14
    body: "I don't use the finder, there are lots of other replacements for, like Pathfinder. I don't know, maybe Konqueror would be one of them too but we first have to be able to try that ;-)\n\n--\nTink\n\n"
    author: "Me"
  - subject: "Re: Why"
    date: 2003-12-14
    body: "Heh, funny :-) I sometimes use KDE, and wish I would be able to replace Konqueror with the Finder :-)"
    author: "hmm"
  - subject: "Re: Why"
    date: 2003-12-15
    body: "Get Real.\n\nOS X's Openstep Finder is designed for Large Scale Networks and the ColumnView is indespensible in such environments.\n\nWhat needs to be added back is the Shelf part of the original Workspace Manager somehow weaved into the Finder that is more than the Left-side Shelf replacement."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Why"
    date: 2003-12-15
    body: "I rather think I am real. Pinch flesh, feels pain, check, yes, I am real.\n\nManaging files with the finder is a pain. It's almost, but not completely a file browser, that column mode doesn't stick, and it doesn't know how to handle network resources all that well. And, anyway, Large Scale Networks may be what the finder was designed for, it's not what it's used on, so that's hardly relevant. And even my mac-zealot collegue prefers Konqueror (even the KDE 2.x konqueror) to the finder..."
    author: "Boudewijn Rempt"
  - subject: "Re: Why"
    date: 2003-12-14
    body: "Hmmm... First read I was thinking this sounds like a way to preserve a dying platform. No, really. Think about it. People discover there are better things out there, so they turn to F/OSS and port it to their fav OS and/or hardware because no one else will help them. And as for the \"MS is threatening Apple users\"... So? Who put them in that position? Seriously, MS stole Apple's GUI and Apple thinks MS is trustable? *shrugs* I don't get it.\n\nOh, and this wasn't ment to be a troll or flame or whatever. I've never used MacOS and have barely touched any Apple hardware. Just thinking aloud. :-)\n\nI am not so sure about people wanting to use Scribus on Mac, though. Maybe the home user, but I think pros will stick w/ Quark for a few more years. (No offense to Scribus developers. I love it! Thanks!)"
    author: "Jilks"
  - subject: "Re: Why"
    date: 2003-12-14
    body: "IANAMU (Mac User), but I play one occaisionally in RL :).\n\nAs to trusting Gates, Apple obviosly trusts Bill as much as I trust rattlesnakes. They can both be trusted to do what is in their best interest when they see a chance. Bill used OSX to support its' position for the trial, but they have slowly been withdrawing its software from Apple (In bill's word \"We will make apple and MS equal, just that MS is to be more equal\"; I suspect that Bill never fully realized where he got that from when he said that nor how appropriate it is). Apple needed MS to get back what they lost, and are trying to break releance on MS. Now apple is a grudging alley to us because they see a bigger threat in MS.\n\nAs to Quark/Adobe/Scribus, many pros are upset with Adobe and Quark and their total lack of good support for Apple (and from what I understand OSS). Quark was started due to Apple users flocking to them, but now Quark appears to want to play seriously in only the MS world. Adobe has the same problem and will almost fall faster than quark as MS takes on both. Here in Colorado, I have turned on several schools to scribus and 2 dropped quark, while the 3'rd dropped Adobe for doing their newspaper. Reason; cost and support. So students will learn how to do their work on scribus/kde/linux. But yes, the large pros will stick with commercial tools for a while longer.\n"
    author: "a.c."
  - subject: "Re: Why"
    date: 2003-12-15
    body: "Visionary Statement:\n\nIf you think Steve isn't already building applications to surpass Quark/Adobe and Microsoft than you really aren't up on what Apple is trying to do.\n\nSimply: If they won't switch to OS X Cocoa we'll deliver superior, compelling applications that leverage open standards and make them leaders in the industry and encourage all third party developers to use Cocoa."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Why"
    date: 2003-12-14
    body: "This is why I used kde apps on mac os X (fink):\n\n-because you can't use the finder to do some serious file management\n-because you can view man pages nicely in konqy\n-because konqy supports ssh and samba\n-because tabbed konsole is great\n-because kdevelop, kate and kwrite are great (mac os X doesn't feature an programming editor, only complete IDEs)\n\nAlex\n"
    author: "aleXXX"
  - subject: "editor"
    date: 2003-12-14
    body: ">mac os X doesn't feature an programming editor, only complete IDEs)\nmacosx may not come with an \"programming editor\" built in (besides vim and emacs) but the excellent BBEdit (http://www.barebones.com/products/bbedit/index.shtml) is is a long time mac-native and mac-only product and regarded by many as the best GUI Editor for programmign languages and HTML."
    author: "Absinth"
  - subject: "Re: editor"
    date: 2003-12-15
    body: "I agree that BBEdit is a very powerful and very nice editor;  However, it is not a free editor as are those that were created for the KDE.  As a Linux user I am used to having several very powerful editors that come with the system for free and I miss that on my Mac."
    author: "vthakr"
  - subject: "Re: Why"
    date: 2003-12-14
    body: "The reason is simple -- KDE apps are more powerful than their OS X counterparts.  The OS X terminal doesn't support tabs.  Safari doesn't support browsing anything other than http, and it never asks you where to save downloaded files.  Mail.app doesn't support true message threading.  *No* Apple application can be used comfortably without the mouse.  In general, OS X apps are implemented the way Apple likes 'em, with moron users in mind, and if you have a different preference, well tough.  KDE apps are implemented with flexibility and power users in mind.\n\nAs somebody who switched from Linux to OS X I'm rooting for this effor to succeed.  I'll be contributing as soon as I have some free time.\n\n-n8"
    author: "n8"
  - subject: "Come on!"
    date: 2003-12-15
    body: "What up with absolutes people! Your personal preferences are no more important or valid than mine or anyone else's. The statement that:\n\"KDE apps are more powerful than their OS X counterparts.\"\nIs not supported simply because an application does not support a feature. (terminal and tabs) or because one company felt that two applications handled a function better than one (safari/finder vs Konker) or even because you like the implementation of threading on one application better than another (mail vs kmail! By the way how is the spam filter that is built into Kmail?). \n\nTo answer the authors question, it's about choice. Without competition, even the mighty Apple will get lazy and even if they stay motivated, they can not make an application that will be all things to all people all the time. So a healthy number of competing applications is a wonderful thing. "
    author: "Doug Petrosky"
  - subject: "Re: Come on!"
    date: 2003-12-16
    body: "\"\"\"\nWhat up with absolutes people! Your personal preferences are no more important or valid than mine or anyone else's.\n\"\"\"\n\nObviously.  That's why I don't feel the need to qualify them with a bunch of pointless noise like \"IMHO\".  I assumed that the fact that my posts are expressions of my opinion would be obvious enough, but maybe that was expecting too much.\n\nThat said, I think it's silly to claim that you can never say that one application is more powerful than another.  Konsole can do everything Terminal.app can do and more.  I think this is a pretty reasonable definition of \"more powerful,\" and it applies to a large number of OS X vs. KDE comparisons."
    author: "n8"
  - subject: "Re: Why"
    date: 2003-12-15
    body: "Reading all these comments I wonder what planet are you guys on?  You really shouldn't knock things that you know nothing about.  If you feel you would like to add some informed information why don't you try getting a Mac and use OS X for a while.  I don't think Quark is doing much on any platform at this point.  It's just a mater of time before Adobe Indesgn takes over that market.  "
    author: "kb"
  - subject: "Re: Why"
    date: 2003-12-15
    body: "Well, I've been a Mac user since 1985.  I love the platform and may be one of those fanatics that Macs are (in)famous for.\n\nHowever, I welcome with excitement any new applications that can run on my Mac.  I recently switched to using pan as my newsreader, from one of the Mac-only Newswatcher variants.  It's great!  No, it doesn't look like a Macintosh app, no it doesn't have the polish of OS X-native app... but that doesn't matter.  It gets the job done better and uses less CPU than my previous program.  Having software choices is fantastic.  Plus, it's giving me exposure to X11 and other ways of computing that I wouldn't normally have access to.\n\nMaybe none of this matters to Linux programmers... I mean, who cares what some Mac owners think?  But for me, I'm ecstatic and appreciative that I can run Unix and Linux software, and I hope that Mac programmers will contribute back to the *nix community.\n\nBy the way, do Linux apps require many changes to run under OS X?  Most of the command line programs I've used have compiled just fine with \"make\"."
    author: "gonif"
  - subject: "Pay"
    date: 2003-12-14
    body: "My personal opinion is that we shall not waste our time with ports to proprietary operating systems. If apple wants KDE apps they shall rather port them."
    author: "Gerd"
  - subject: "Re: Pay"
    date: 2003-12-14
    body: "I agree with you. After all, they have a similar mindset as MS. Worse, they exercise control not only over their operating system, but over their hardware as well."
    author: "Tuxo"
  - subject: "BS"
    date: 2003-12-14
    body: "Wrong.  Darwin (the kernel) is open source (I'm hacking their mouse driver at the moment) and Apple has the most open hardware platform available, all the way down to the firmware.\n"
    author: "n8"
  - subject: "Re: BS"
    date: 2003-12-14
    body: "Well, who cares about whether Darwin, the kernel, is opensource or not? Important is, that the _whole_ operating system is free software. Operating systems are too important to be controlled by a single company.\n\nConcerning the hardware, I have always thought that only Apple is allowed to produce Apple compatible computers. Correct me if I am wrong."
    author: "Tuxo"
  - subject: "Re: BS"
    date: 2003-12-15
    body: "\"\"\"\nWell, who cares about whether Darwin, the kernel, is opensource or not? Important is, that the _whole_ operating system is free software. Operating systems are too important to be controlled by a single company.\n\"\"\"\n\nUm, Darwin and the BSD core (also open-source) *are* the whole operating system.  Check out the OpenDarwin distribution -- it's a complete OS.  The only proprietary parts of OS X are the window system and applications.  We all know that those can easily be replaced with OSS versions, like, say, KDE.  This is one of the reasons for the KDE-Darwin port.\n\nAs for hardware, Apple may be the only ones allowed to produce it, but my point is that they use open standards for most of their hardware, even the firmware.  As a hacker you have a hardware platform you can actually get documentation for, which has always been the main problem holding back Linux.  I believe the OSS community has a real opportunity with Apple hardware -- Linux PPC could potentially be the slickest Linux on the market, with all features working out of the box on your PowerBook or iMac."
    author: "n8"
  - subject: "Re: BS"
    date: 2003-12-15
    body: "> We all know that those can easily be replaced with OSS versions, like, say, KDE. This is one of the reasons for the KDE-Darwin port.\n\nWell, no, why should I use OpenDarwin when I can use Linux on Mac hardware? Linux (I mean the kernel Linux + Gnu tools + KDE etc) has a much larger community behind it than OpenDarwin, so it is much easier to find software that runs on it. Even more important, Linux is protected by the GPL, which allows it to always stay free (free as in freedom), whereas Apple can change their license for OpenDarwin like they please. This allone makes me not use OpenDarwin and this is also the reason why I don't use BSD variants either. In an ideal world, BSD-style licenses would be optimal, as they allow for a maximum of freedom. However, in this world full of sharks I think that GPL is the license that allows to protect the freedom of most people: It protects the developer (your work is not going to become closed source software) and it protects the user as well, as it guarantees the software to be free also in the future.\n\nConcerning the hardware, I applaud Apple for following open standards, but I don't like that a company reserves itself the right to soly produce the matching hardware. That is basically a monopoly. Imagine a world, where Apple was the number one operating system. No other companies would be allowed to produce computers that go with this operating system. That's a nightmare, frankly speaking!\n\n "
    author: "Tuxo"
  - subject: "Re: BS"
    date: 2003-12-16
    body: "If you prefer to use Linux over OpenDarwin because of the size of the community then that's fine -- it's your choice, but your original argument was that KDE shouldn't be ported to OS X because it's a \"proprietary operating system.\"  This is not true (although it *is* true for, say, Solaris, AIX, and other OSes that KDE is currently ported to).  OS X has a proprietary windowing system built on top of an open-source operating system.  \n\nApple cannot change the license for OpenDarwin -- OpenDarwin is an independent project not controlled by Apple.  Apple *could* change the license for future versions of *their* Darwin implementation, but that would just mean OpenDarwin would become a fork.  Please understand that Apple's open-source license (the APSL) has been approved by the OSI.  It's a real open-source OS.\n\nAs for the hardware, again, the OS Darwin is open-source.  There is an x86 port.  You can port it to  68000 or whatever architecture you want because you have the source code.\n\nIn short, I don't care if you don't want to use OS X, but you should at least be aware that it's truly an open-source OS at the most fundamental level.\n"
    author: "n8"
  - subject: "Re: BS"
    date: 2003-12-16
    body: "> If you prefer to use Linux over OpenDarwin because of the size of the community then that's fine -- it's your choice, but your original argument was that KDE shouldn't be ported to OS X because it's a \"proprietary operating system.\"\nYou are right, I deviated a bit here. It was actually not my main point why I would not use OpenDarwin. The main point is the type of license that goes with it. Even though it is a open source kernel, I do not like licences that allow to hijack software, turning open source software into proprietary software.\n\n> OS X has a proprietary windowing system built on top of an open-source operating system. \nWell, there you have it. What is a naked open source kernel worth if it is wrapped under a proprietary windowing system? After all, the latter gives OSX the identity and provides the APIs to graphical applications. If you control these APIs you basically control everything that is worth to an end-user and that's what it is all about. So again, for me, OSX is a proprietary operating system (even though it contains open source technology).\n\n> As for the hardware, again, the OS Darwin is open-source. There is an x86 port. You can port it to 68000 or whatever architecture you want because you have the source code.\nThat's not what I meant. What is important is that Apple controls the manufacturing of computers for their platform. That's what it is all about. OpenDarwin has nothing to do with it. \n\n"
    author: "Tuxo"
  - subject: "Re: BS"
    date: 2003-12-19
    body: "\"\"\"\nEven though it is a open source kernel, I do not like licences that allow to hijack software, turning open source software into proprietary software.\n\"\"\"\nSo then KDE shouldn't be ported to *BSD?  KDE shouldn't support Python?  In principle either one of those could be taken closed-source.  Whatever.  This is not an issue I care about.  Once the code is released under an OSI approved license it is free forever.  I don't care much if somebody can take it and make a proprietary project out of it so long as I still have access to the original code.\n\n\"\"\"\n> OS X has a proprietary windowing system built on top of an open-source operating system. \nWell, there you have it. What is a naked open source kernel worth if it is wrapped under a proprietary windowing system? After all, the latter gives OSX the identity and provides the APIs to graphical applications. If you control these APIs you basically control everything that is worth to an end-user and that's what it is all about. So again, for me, OSX is a proprietary operating system (even though it contains open source technology).\n\"\"\"\nThis is the whole point!  \nDarwin = good OSS kernel\nBSD = good OSS Operating System support apps\nKDE = good OSS desktop & desktop apps\nDarwin + BSD + KDE + any good OSS window system = good graphical OSS Operating System, and you can also run OS X on it when you want to!\n\n\"\"\"\n> As for the hardware, again, the OS Darwin is open-source. There is an x86 port. You can port it to 68000 or whatever architecture you want because you have the source code.\nThat's not what I meant. What is important is that Apple controls the manufacturing of computers for their platform. That's what it is all about. OpenDarwin has nothing to do with it. \n\"\"\"\nApple does not control manufacture of computers based on PPC.  If OpenDarwin on PPC became a massive success then anybody could manufacture a PPC computer that it could run on.  The new Amiga platform, for example, is PPC-based.\n"
    author: "n8"
  - subject: "Re: BS"
    date: 2004-01-02
    body: "Give up. I have figured out a long time ago that there are people who do not want to pay for anything. They want free software, they want free hardware, they take - take - take and take. They decide that open source software is the only way to go, they bitch about anything propritory, and they take some more. Asking them to donate, or support a software project monitarily is to much as it means they have to spend money. 75% of those that are the loudest about open source and free software have never written a line of code. Again, it is human nature to take what is free.\n\nApple great hardware (even though I have had a few problems with my iBook). I have installed every version of linux ppc that has become available. I have subscribed to YellowDog.net and provide a ton of bandwidth to the KDE project (http://webcvs.kde.org , anoncvs.pandmservices.com, http://news.uslinuxtraning.com, nntp://news.uslinuxtraining.com), and more. I spent time as a software packager for Mandrake Software packaging KDE. Unfortunatly after a few days using Linux PPC I am forced back to OS/X.\n\nWhy? It is simple. I work as an HVAC Service tech and need a laptop that works every time, has to be on battery, has to be open and closed (suspend on closed), has to pick up several wireless networks, etc - Apple has built a product that does this - others have not.  I can not try and figure out why Tomcat keeps crashing. I can not try and figure out why Kmail has yet to syncronize imap folders for offline use, I do not have time to figure out why Evolution locks up every 4 hours on ppc, etc. I do not have the time to figure out why CUPS on PPC does not print every time and why the fonts print differently in konqueror than they appear when displayed. I need a product that works and is supported.\n\nOn the other hand, my server runs on YellowDog PPC. OS/X sucks as a server software, it can not run more than 10 days with the traffic I have without major file system corruption. Again, a reality some people can not grasp, I am going to use the software and the OS that works, if I have to pay for it I will do so gladly, as IT WORKS!\n\nI love KDE, I think it is a killer desktop. There are several times a day I wish I could figure out how to turn off Aqua and start X, but I have to look at support, stability, and updates.  It used to be that you could compile KDE and it worked perfectly with any distribution, but each Linux vendor has helped to make their own distribution proprietory. This has been done with location of config files, start up scripts, and menu structures. None of the distros are installing a \"full\" KDE any more, try and find kpackage on Yellowdog. So nothing plays will with anything else any more.\n\nApple has it, the Linux PPC distros do not. Knocking the OS/X distibution and Apple, and closed source is not the answer.  Helping develop something that is stable is the answer.\n\n-Chris"
    author: "Chris Molnar"
  - subject: "Re: Pay"
    date: 2003-12-14
    body: "Whoever will end up working on the port will also make KDE less dependent on the X Window system. Considering how many (imo uninformed) people like to bash X as outdated and unsuable this addition of flexibility should actually be welcome, shouldn't it?"
    author: "Datschge"
  - subject: "KDE already runs fine on Macs"
    date: 2003-12-14
    body: "Just remove that worhless OSX and install yellowdog or debian!  Sure MacOSX is pretty but it lacks the functionality of KDE.  I am glad when anyone can find a programming project they like and want to work on, but (as far as I am concerned) is a waist of time.  \n\nbrockers"
    author: "brockers"
  - subject: "Re: KDE already runs fine on Macs"
    date: 2003-12-14
    body: "How can it lack the functionality when KDE already runs on OSX?  =)\n\nThe difference here is using Qt/Mac instead of Qt/X11 on top of an X server on Mac OS X."
    author: "Ranger Rick"
  - subject: "What's the OS X terminal app?"
    date: 2003-12-15
    body: "Does anybody know what the OS X terminal application being used in the screenshot is (the one behind Konsole)?  I can't stand Apple's Terminal.app and I would really like a tabbed terminal emulator for OS X to tide me over until Konsole is ported..."
    author: "n8"
  - subject: "Re: What's the OS X terminal app?"
    date: 2003-12-15
    body: "That is iTerm.  You can get it here.\n\nhttp://iterm.sourceforge.net/"
    author: "Pres-Gas"
  - subject: "Why port?"
    date: 2003-12-16
    body: "Everybody seems to think that there is something wrong with porting KDE apps to OS X - an OS kernel with a closed GUI... OK maybe, maybe not.\n\nBut the whole reason it is possible is because Qt released Qt/Mac. Now if Qt think the Mac is worth porting their apps to, what do you know, it might just be a \"really good thing\" (TM). They didn't release Qt/Windows notice.\n\nPersonally I use the Mac over any other OS because I'm lazy. I've tried all the others and the Mac interface makes me feel \"comfortable\", hard to describe. Seems at least a couple of people at Trolltech/Qt feel the same.\n\nMore apps on OS X suits me and mine just fine thankyou.\n\nDon't like it, don't buy it. But sure don't knock me because I do like it."
    author: "aqsalter"
  - subject: "Is QT really a multiplatform toolkit?"
    date: 2003-12-16
    body: "This question bugs me for some time now. It started when I first downloaded and installed it on my Mac. While I was using Linux I loved Qt and all apps developed with it so I was one of the first to jump for joy when they released the source to us. Guys from QT say, there is no need to change any code, simple recompile is all you need for application to run on your platform. That was not my experience. I am yet to compile a single application successfully. Most of the time I tried apps that doesn't rely on KDE libraries so they should compile cleanly but they didn't.\nMy conclusion is, even though you have the option to run QT on every major platform you need to write separate apps for each of them. That is not the case with Java which is the only real multiplatform (write once - run everywhere) development tool. "
    author: "Jabba"
  - subject: "Web page for development list not updated?"
    date: 2003-12-16
    body: "It seems that \"The KDE-Darwin Archives\" at http://www.opendarwin.org/pipermail/kde-darwin/ is not updated."
    author: "Loranga"
  - subject: "New screenshot showing native OSX theme usage"
    date: 2004-01-01
    body: "http://www.csh.rit.edu/~benjamin/about/pictures/KDE/osxtheme.png"
    author: "Anonymous"
---
<a href="http://www.trolltech.com/products/qt/mac.html">Qt/Mac</a> has been <a href="http://dot.kde.org/1055852609/">GPLed since Apple's World Wide Developer Conference 2003</a>, which let everybody think that <a href="http://www.koffice.org">KOffice</a> and the whole KDE stuff would quickly become native Aqua applications. However, the <a href="http://www.opendarwin.org/projects/kde-darwin/">KDE on Darwin</a> project is not really active and needs more developers. So if you know C++, Qt or KDE, please <a href="http://www.opendarwin.org/mailman/listinfo/kde-darwin">join our development list</a> and say "Hello".
<a href="http://konsole.kde.org/">Konsole</a> has already been <a href=http://ranger.befunk.com/blog/archives/000197.html>ported</a>, but actually doesn't refresh right.

<!--break-->
