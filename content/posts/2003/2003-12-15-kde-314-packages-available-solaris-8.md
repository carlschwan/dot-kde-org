---
title: "KDE 3.1.4 Packages Available for Solaris 8"
date:    2003-12-15
authors:
  - "wbastian"
slug:    kde-314-packages-available-solaris-8
comments:
  - subject: "great"
    date: 2003-12-15
    body: "I'm not using Solaris anymore but that's great news."
    author: "JC"
  - subject: "Great!"
    date: 2003-12-15
    body: "Cool, great work Stefan!"
    author: "ac"
  - subject: "congrats"
    date: 2003-12-15
    body: ":-) very good work"
    author: "Phemt"
  - subject: "Excellent!!!"
    date: 2003-12-16
    body: "Solaris and KDE make an excellent combination for workstations. Outstanding work!"
    author: "Sc00b"
  - subject: "Thank You everyone!"
    date: 2003-12-19
    body: "For the very kind and supportive comments!\n\n--Stefan\n"
    author: "Stefan Teleman"
  - subject: "Good job."
    date: 2003-12-20
    body: "Trying out the new free Solaris x86 just became feasible ... ;-)"
    author: "Eike Hein"
  - subject: "I don't find kde/stable/3.1.4 in ftp.kde.org"
    date: 2005-07-13
    body: "Hi;\n  I try to download 3.1.4 for my SunSolaris 8 but I don't find it in the server. Please advise where I can download the latet version of KDE for SunSolaris 8. Thank you.\n\nRegards,\nTEH"
    author: "Teh Kok How"
  - subject: "Re: I don't find kde/stable/3.1.4 in ftp.kde.org"
    date: 2005-07-13
    body: "ftp://ftp.kde.org/pub/kde/stable/3.4.1/contrib/Solaris/\nftp://ftp.kde.org/pub/kde/stable/3.4/contrib/Solaris/\nhttp://solaris.kde.org/\n"
    author: "Anonymous"
---
Stefan Teleman
<a href="http://lists.kde.org/?l=kde-solaris&m=107134772817112">
reported today</a>
to the 
<a href="https://mail.kde.org/mailman/listinfo/kde-solaris">
kde-solaris mailinglist</a>
that KDE 3.1.4 packages for Solaris 8 are
<a href="ftp://ftp.kde.org/pub/kde/stable/3.1.4/contrib/Solaris/8">
now available</a>. 
The packages have been build with the Sun Forte 8 compiler. Before rushing off to try the packages it is <i>strongly recommended</i> to read the 
<a href="ftp://ftp.kde.org/pub/kde/stable/3.1.4/contrib/Solaris/8/KDE_INSTALLATION_INSTRUCTIONS">
installation instructions</a> first. If you still encounter problems after you have followed the instructions you are welcome to report them to the kde-solaris mailinglist where Stefan will try to help you.



<!--break-->
<p>
This distribution requires Sun OpenGL 1.3 which is freely available for
<a href="http://wwws.sun.com/software/graphics/opengl/download.html">
download</a> from Sun.
</p>
<p>
The default installation directory trees are:<br>
/opt/qt-3.2.2-32<br>
/opt/kde-3.1.4<br>
/opt/fsw4sun<br>
</p>


