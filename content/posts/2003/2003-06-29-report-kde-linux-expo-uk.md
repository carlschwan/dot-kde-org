---
title: "Report: KDE at the Linux Expo UK"
date:    2003-06-29
authors:
  - "jbacon"
slug:    report-kde-linux-expo-uk
comments:
  - subject: "Right on ... "
    date: 2003-06-29
    body: "\"As is usual at many expo's, there was the usual number of grumblers as well. We had people coming over and complaining that KDE is bloated, that it looks too much like Windows and that they much prefer mwm or some other suitably arcane window manager that was great back in 1995 but not now. Most were constructive in their criticism but some were not. This is a negative aspect of users I feel. Developers connected to all projects work damn hard to get this software working as well as possible and although criticism is good when constructive, some of it was not. They were told to supply patches. ;) \"\n\nGood answer ... \n\n\nGood job Jonathan, Kevin, Shawn, Lee, Phil and .. Jono ! Let us know when those other pictures are online.\n\n\nFab"
    author: "Fab"
  - subject: "Re: Right on ... "
    date: 2003-06-29
    body: "KDE is a fantastic environment for work and play, whilst it is generally true that \"more features = larger file sizes\" KDE is by far more efficient than windows in both size and performance. KDE still has a lot of features missing, but it is only a matter of time, it is the best Open Source environment by far. Keep up the good work guys.\n\nOn to the show... All the big players were there HP, IBM and so on. The .org village had KDE, Linux Professional Inistitute (LPI), Open Office (though they had abandoned the stand whilst I was there), Debian and more. The floor space was a little small for the Linux show, but I suppose the big one is in London. "
    author: "Ian Ventura-Whiting"
  - subject: "Re: Right on ... "
    date: 2003-06-30
    body: "Vocal critics:\n\n\"...they much prefer mwm or some other suitably arcane window manager that was great back in 1995 but not now.\"\n\nWhat? They must only have been six years old back then."
    author: "The Badger"
  - subject: "K Gear Sign"
    date: 2003-06-30
    body: "How did you produce or where did you get the K gear signs from popping on the desk?"
    author: "Anonymous"
  - subject: "Re: K Gear Sign"
    date: 2003-06-30
    body: "Linux Format kindly did them for me. :)"
    author: "Jono Bacon"
---
Last week I <a href="http://dot.kde.org/1056039550/">launched a call</a> for help when the KDE booth at the <a href="http://www.linux-expo.co.uk/">LinuxUser &amp; Developer Expo 2003</a> dropped in my lap. I have <a href="http://www.jonobacon.org/writing/other/kde_linuxexpobirm2003.php">written a report of the events</a> leading up to and happening at the expo. It was a good expo, and KDE fared well there.  Expect more pictures to be online soon.
<!--break-->
