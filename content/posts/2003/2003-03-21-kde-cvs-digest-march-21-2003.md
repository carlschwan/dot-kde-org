---
title: "KDE-CVS-Digest for March 21, 2003"
date:    2003-03-21
authors:
  - "dkite"
slug:    kde-cvs-digest-march-21-2003
comments:
  - subject: "Thanks for the KDE-CVS-Digest..."
    date: 2003-03-21
    body: "Derek.\nI was the first one, ha! :)"
    author: "138"
  - subject: "haven't-missed-a-week-yet"
    date: 2003-03-21
    body: "Thanks so much!  Keep it up!  :)"
    author: "KDE User"
  - subject: "Has someone fixed the window.location bug?"
    date: 2003-03-21
    body: "Version 3.1.1 has seen the resurgence of a nasty kjs bug which prevents the user from hitting the back button to return to a page which had been left through the JavaScript window.location method."
    author: "antiphon"
  - subject: "Re: Has someone fixed the window.location bug?"
    date: 2003-03-21
    body: "Yes, I have filed a bug for it."
    author: "antiphon"
  - subject: "Re: Has someone fixed the window.location bug?"
    date: 2003-03-23
    body: "number?"
    author: "David Faure"
  - subject: "Re: Has someone fixed the window.location bug?"
    date: 2003-03-23
    body: "http://bugs.kde.org/show_bug.cgi?id=56204"
    author: "Antiphon"
  - subject: "Re: Has someone fixed the window.location bug?"
    date: 2003-03-24
    body: "It really helps if you include a URL or a testcase in such reports...\nIf I make my own testcase and it works, I won't know if that's because the bug is fixed or because my testcase is wrong.\n(Well I'll try now, but no guarantee I'll be testing the right thing...)"
    author: "David Faure"
  - subject: "Re: Has someone fixed the window.location bug?"
    date: 2003-03-24
    body: "Based on your CVS commit, it looks like you've tracked down and fixed what I was talking about. I usually make testcases but was in a hurry @ the time."
    author: "Antiphon"
  - subject: "Quanta development"
    date: 2003-03-21
    body: "It's great to see that Quanta is starting to have a WYSIWYG editor mode.\nI currently use Amaya because of that, but it's very buggy and I prefer a KDE/Qt interace.\n\nContinue the good work,\n Ricardo Cruz"
    author: "Ricardo"
  - subject: "Re: Quanta development"
    date: 2003-03-21
    body: "Did you know that Mozilla has a nice WYSIWYG editor?\nI sometimes use it and guess what: the HTML it produces is standards compliant! That's more than you can say of may commercial HTML editors. Of course Amaya should be standards compliant as well."
    author: "Jos"
  - subject: "Re: Quanta development"
    date: 2003-03-22
    body: "I haven't messed much withthe Mozilla editor. I used to use the Netsape one in the past. Let's be real. I don't think anyone considers them an editing tool for serious use. If they were then there wouldn't be a market for the tools that currently dominate web development. And the question arises... compliant to what? Can you set it up to do HTML 4.01 Strict, XHTML 1.0 or XHTML 1.1 with modules? Being compliant is not so useful if I get to decide what you're compliant with and you don't. Quanta will function compliant with the DTD you authorize and as we progress will work with XML and SGML DTDs as well as Schema based work. Simple HTML 4.0 transitional comliance may be okay today but tomorrow it may be a disaster as you look for a new tool. With Quanta it won't matter the DTD. You can adapt it in a few hours max, and soon possibly in minutes by pointing your declaration to the actual DTD and editing a few settings.\n\nAs for Amaya I have to say the interface is maddening. The last I checked it looked as if it might be shifting from the GTK interface being supplemental to being the current development option. I'm uncomfortable with this. Beyond that Amaya does not appear to be very friendly to PHP and other scripting so again it's not really a professional option. In fact I would say the interface is would drive a professional insane because it does not appear to be geared to productive us. Just my opinion.\n\nI would say that the annotation in Amaya is interesting. The program is worth following for where they attempt to address some issues.\n\nI expect Quanta to arrive with visual editing that transcends our accepted understanding of WYSIWYG. That's our goal."
    author: "Eric Laffoon"
  - subject: "Re: Quanta development"
    date: 2003-03-22
    body: "I wonder how you want to implement. I guess you dont want to implement a new HTML renderer with editting capabilities. So what are the options? Logical to me sounds either to extend KHTML, or maybe even keep KHTML as it is and just edit by modifying the DOM model in realtime... not sure whether that is possible..."
    author: "AC"
  - subject: "Re: Quanta development"
    date: 2003-03-22
    body: "Did you read the text in the CVS digest? It may not have exactly stated it but the Kafka part essentially extends khtml so it will benefit from all improvements. The DOM tree will be pulled through Kafka and there will be node pointers to our internal tree. The Kafka part will essentially act as a visual layer that extracts the node and location and feeds it back to our parser where everything is dispatched and controlled from."
    author: "Eric Laffoon"
  - subject: "Re: Quanta development"
    date: 2003-03-22
    body: "There is no tool like Dreamweaver for Linux yet. A Quanta WYSIWYG mode may change that. \n\nHowever Mozilla Composer is a usable editor. However it lacks functionality. It would be nice to desintegrate Mozilla Composer as a stand-alone application. then would be taken more seriiously and still lacking functions would be implemented. At least html-tidy for Word-html shall be integrated to any web editor. "
    author: "Hakenuk"
  - subject: "Re: Quanta development"
    date: 2003-03-22
    body: "> There is no tool like Dreamweaver for Linux yet. A Quanta WYSIWYG mode may change that. \n \nI haven't run Dreamweaver but I've had comparisons from web developers who tell me we're close in some areas and ahead in others. Intelligent DTD driven markup, scoped resources, DTD indexed templates, unlimited programmable scoped actions and visual dialogs that build strings with text associated widgets... plus a WYSIWYG layer that does not hack up your source but gives you full control?\n\nI was really hoping next year Macromedia was telling everyone they wanted Dreamweaver to be like Quanta actually. If community members would pitch in and help us with resource repositories like scripts and templates it would really be emberassing for them.\n\n> However Mozilla Composer is a usable editor. However it lacks functionality. \n\nMy point exactly. It's not it's mission.\n\n> At least html-tidy for Word-html shall be integrated to any web editor. \n\nIn CVS we have a hard coded implementation however I like the Kommander dialog that Robert Nickel came up with a lot better. The snap is attached."
    author: "Eric Laffoon"
  - subject: "Re: Quanta development"
    date: 2003-03-22
    body: "Agreed, Quanta should rock ...\n\nI have to say though the Moz editor is getting better and better ...  the CSS editor is among the best I'd seen (still rough in terms of integration etc. but) tidy and standards compliance checkers etc have been developed using XUL wrappers to the commandline versions.  Moz supports DAV and FTP saves etc. etc.\n\nThere's lots of missign polish from Moz applications (including the x-platformness bits) but given that many apps are quicky hacks whipped up by hobbyists they are quite amazing. I think a major misunderstanding about Moz is the platform part of it ... it's more like Emacs than Vi ... very extensible but big.  Extensible in XML and jscript ... i.e. by your average web page hacker. XULMaker and other such  Moz IDE's are going to push it over the top I think.  Mozilla client applications for all kinds of network services are springing up in companies all over the place.\n\nThat's what makes it awesome - you don't have to be a developper.  I hope that KDE gets as easy ... Whip up an XML UI with a GUI designer ... attach a python script and a few dcop actions and voila ... an app.\n\n\n\n\n"
    author: "Mozzer"
  - subject: "Re: Quanta development"
    date: 2003-03-23
    body: "> That's what makes it awesome - you don't have to be a developper. I hope that KDE gets as easy ... Whip up an XML UI with a GUI designer ... attach a python script and a few dcop actions and voila ... an app.\n \nWake up and smell the coffee. Granted we're still working on our formal announcement for Kommander, but have you actually looked at Quanta? Especially with Kommander? Kommander is included with Quanta since version 3.1.\nQUANTA:\n* DTDs and tag dialogs added via XML\n* Programmable actions can use any script language\n* Custom toolbars can be exchanged by email\n* Any kpart can be plugged in (some aspects being polished)\n* DCOP being extended beyond anything it's doing to include document information and access\n* Projects can center around toolbars and DTDs completely changing it's nature (in fact changing between different documents with different DTDs change it's toolbars and auto-complete)\nKOMMANDER:\n* Visually draw dialogs that can create strings via associated text in widgets\n* Launch applications and scripts with custom strings WITHOUT programming\n* Incorporate shell scripting in the dialog in any widget text area\n* Widgets are DCOP enabled\n* Any KDE widget can be Kommander enabled by inheriting the associated text class\n\nCombine the two of them and you can integrate any KDE application with DCOP, extend Quanta with keystrokes or toolbar buttons to run any dialog interfacing with any command line action or creating any text string. Looking for a new feature? Build it with any programming language you want, a Quanta action and a Kommander dialog. Our development team would be obsolete if not for a few issues that must be done with C++.\n\nAll of this is hardly standing still too. We have laundry lists for improving templates, plugins, parser driven activities and more. If you ask me what you're looking for is in CVS right now and we are working to bring it to a level that enables KDE users to not have to be wishing they could do something somebody else can. Just the opposite. I don't think there is another tool with the ease of quick customization as Kommander."
    author: "Eric Laffoon"
  - subject: "Re: Quanta development"
    date: 2003-03-23
    body: "The Dreamweaver feature I miss most on Linux is not WYSIWYG but the lack of a FTP-Client which have similar features than the Dreamweaver-one (would be a great addon for Quanta):\n- with one click I can connect to the site AND the local and remote-window show me the correct directories.\n- when I upload or download a file it is automatically transferred to the the corresponding directory on the other side. So I am able to copy files from several directories to the other side with one command without worrying about the correct destination directory.\n- Finally \"synchronise\" is a killer-feature. Select the directories to synchronise, click synchronise. Next you get a list of all the changed (based on mtime!), new or deleted files and can deactivate the files you don't want to upload/delete. Then click on OK and get a cup of tea! ;-)\n\nDoes someone know a tool, which can do this things under Linux? The only thing which eases the pain is rsync, but this is not an option for lowcost-webhost-sites.."
    author: "Michael"
  - subject: "Re: Quanta development"
    date: 2003-03-23
    body: "Sounds like you want a file-tree synchronizer, not an FTP client exactly.  It seems like with KDE's IOSlaves, you could implement a generic application that could synchronize file trees on any type of filesystem or protocol.  Wouldn't that be cool?"
    author: "not me"
  - subject: "WYSIWYG & the Freedom of choice"
    date: 2003-03-22
    body: "1. In a modern WYSIWYG editor requirements it appears that the most wanted feature is a correct dealing with the tables and table cells. \nWhat I mean is the ability of a editor visually to merge neighbour cells and fixing the correct values of COLSPAN and ROWSPAN attributes. And it seems that most of the best selling editors under windows (like dreamewaver, homesite, frontpage) do, but they almost do it! By 'almost' I mean is the tricky calculations of cell widths and heights (WIDTH and HEIGHT attrs), so that when you merge cells all sum of all calculations remain correct, both horizontally and vertically. And just because they don't do it correctly it appers that web designers these days need to put this hidden pixel lines for pushing the cell to expand correctly (it's also an old browser problem). So you may guess what happens when you have a table with more 10x10 cells and do some merging some of them later.\nFrom this point of view everybody who had ever did some writing of browser-like, wordwrapping or another dynamic-layout-enabled renderer app may say that it's just a matter of simple calculation to fix this numbers (the COLSPAN, ROWSPAN, WIDTH, HEIGHT attrs of TD), and they may be right in case that the at least the WIDTH of TABLE is fixed. But especially when you need to have some percentage WIDTHs you will really get a trouble and little more calculation logic to implement for those ones that have variable width based on percentages.\nAnd those are a real problems for these days WYSIWYG editors, so that's why it's a most wanted feature. I want to appologise to those who may have experienced with another 'most wanted' features/problems, and/or especially those professional HTML coders that may be know a HTML editor that is capable of doing that correctly, but if you guess another related issue you can add - and may be help quanta team to be poined to. (A kind of end-user response)\n\nAnd all this I am mentioning not because I personally feel that I would ever need this kind of feature but because I think that just extending KHTML and \"editing its DOM without messing up the other html code\" is not enough, because as you may guess current cell (TD) is just another child of the DOM of the current TABLE. So I mean that it needs a little more workaround and at least touching the attributes of surrounding TDs (not only the currently selected/edited cell) and TABLEs especially when changing the calculations of a given cell/row/collumn and/or merging table cells.\n\n\n2. As for the ability of another html tools for the linux operating environment - they are all welcome - both commercial and open source ones, I call that freedom of choice, someone else could call it freedom of the (competing) market. And important is that the standart (in current case HTML standart) is free and available for everyone to use apps or implement it, but in the same time availability of multiple apps that conform with the standart.\nAnd from this point of view the definition of 'free software' could be adapted to 'freedom of software to choose' - a software that conforms with standarts, so that 'free' in relation with 'software' is interpreded as 'freedom of choice' and not as 'free beer' or 'freedom of speech', and any kind of another asumptions of the imterpretation could be ridiculous in our world of free competing market - commercial and capitalist (may be supporters of well known org could opose this in its ambition to dominate the 'free software' definition, while killing the marked in the same time - but remember the standarts need to be free for having the freedom of choise, not the apps).\nEnd user should ever welcome new available app that conforms with the free standarts, as for linux itself having more than 5 WYSIWYG editors:\n* CoffeeCup - excellent commercial WYSIWYG editor\n* Netscape Composer (note 6 and 7 versoins are very good)\n* Amaya, as mentioned upper\n* Homepage Builder for Linux - a commercial good one from IBM\n* Quanta - yet now a new one available from KDE\n... and may be more\nmakes linux a good option for switching to, and as more they became (especially as more commercial ones) linux will gain bigger adoption by companies to use it and to invest their money in it.\nMy personal preference for linux as a working environment is because of the big amount of pre-installed konsole tools and server apps like apache, java, php, mysql, gcc, ssh, perl, etc. (of course all of them are can be installed on windows - but it will take time, and they don't 'feel' natively under alien non-unix environment).... AND of course KDE - I didn't forgot about it, KDE is most integrated desktop for me. Notice the word 'Integrated', it has a very big matter when talking about desktops!\n\nGreetings to all."
    author: "Anton Velev"
  - subject: "Re: WYSIWYG?  Not quite."
    date: 2003-12-04
    body: "Sorry, Quanta is not wysiwyg."
    author: "Not Quanta"
  - subject: "Re: Quanta development"
    date: 2003-03-22
    body: "Mozilla Composer is nice for small tasks, however Amaya is a lot more powerfull. It has support for HTML, XHTML, MathML, SVG and CSS.\nAmaya has three view modes that I like a lot: the WYSIWYG, the structure and the source. All three are interactive with each other.\n\nAlthough Amaya has two big cons: it has an ugly interface and it is buggy has HELL! But give it a try, you can find it at: http://www.w3.org/Amaya/ ."
    author: "Ricardo"
  - subject: "Re: Quanta development"
    date: 2003-03-23
    body: "Note that while Quanta does not yet support MathML and SVG we do support the rest, though some of the XHTML DTDs are only now in CVS. I would also like to point out that we don't support HTML or XHTML but the specific transitional, strict and frames implementations! The simple point is that we can support ANY markup DTD with XML and a description.rc file. Now how does Amaya do with DocBook and XML/XSL? Amaya is good for looking at cutting edge W3C implementations but less inviting for real work. ;-)\n\nAs to the three views of Amaya I invite you to compare the structure view to Quanta's. Especially our CVS version. It's been a little while since I looked at this in Amaya but there was no comparison in my opinion. I would invite you to try it with a PHP file in both too. Last I checked Amaya was a lousy tool for PHP. Quanta simply has not completed it's WYSIWYG mode.\n\nAs far as bugs, Andras Mantia is the Debugulator. Last I checked on KDE Bugzilla Quanta slipped off the KDE top 100 application bug list with less than 4 bugs!\n\nIt seems to me that without the WYSIWYG issue (or if you prefer a dynamic approach with PHP) Quanta is the only really viable production tool of these."
    author: "Eric Laffoon"
  - subject: "Thanks from the Quanta Team"
    date: 2003-03-22
    body: "Derek, as I mentioned I was quite surprised and humbled by the amount of attention you gave Quanta in this round. There's not much I can add except the Kafka part is now being integrated with Quanta. There's a lot of work to do and this is no small task. We appreciate the support of our users and we have heard your requests. It's finally time to take the giant step. I hope we continue to prove worthy of the good will we have received to date. All the best to our users.\n\nEric Laffoon\nQuanta Team Leader"
    author: "Eric Laffoon"
  - subject: "Thanks TO the Quanta Team!"
    date: 2003-03-27
    body: "Let me just add a thanks TO the Quanta team from a humble user.  Quanta just keeps improving and improving!  And one of these days I WILL look at Kommander.  I'd LOVE to be able to pretty much throw out windows completely from my machine here. :)  Quanta WYSIWYG and Kommander scripts to generate GOOD PHP Code based on Database schemes will do that!  I just need to look at the Kommander docs and start working on this at some point! :)"
    author: "Jeff Stuart"
  - subject: "Browsing the Web with Konqueror"
    date: 2003-03-22
    body: "I already posted this to last week's thread, but it was one of the last three messages and pretty much got lost. I'm feeling for this one, so I'll repeat it. Hope I'm not offending anyone.\n\nI have a request for the next KDE CVS digest. Like many other users, I'm very interested in the current state of Konqueror as a web browser. Sure, we see all those Safari merges, but most look like framework fixes, while I don't see any of the rendering engine stuff coder David Hyatt (http://www.mozillazine.org/weblogs/hyatt/) mentions in his blog. Now, when he talks about fixing the rendering of margins at the bottom of <div> block elements or adding some support for CSS3 selectors, that's something I can understand. I understand how it will affect my daily surfing - I want to know if and when it turns up in Konqueror, 'cause I don't own a Mac, I use KDE.\n\nI think it's time for a little overview article. Now, I don't think the devs want to talk about how the relationship with Apple is going, that's probably a bit too delicate and political for the weekly digest, but some information on how we're standing after all these weeks, what is going to be merged and what is not, and maybe even a little roadmap - that'd be really *great*. :)"
    author: "Eike Hein"
  - subject: "Re: Browsing the Web with Konqueror"
    date: 2003-03-22
    body: "i think there were a lot of KJS fixes too (ie javascript). With a few CSS fixes that will mean konqueror will render pretty much damn near perfectly.\n\nNow I'm pining for a decent password manager..."
    author: "caoilte"
  - subject: "Re: Browsing the Web with Konqueror"
    date: 2003-03-24
    body: "> Now I'm pining for a decent password manager...\n\nYea...\n\nBeing the interested non-developer I'd assume there is some kind of widge set regularly used within KDE for password entry fields. This one I imagine to be possible to be extended with a password manager (appearing and residing as key icon in the \"systray\" when used) storing the password encrypted in memory or at a custome place (let's say a USB flash drive). The password manager could be configurable in the regard of how long to keep the password in memory before reading it from disk or requesting it from the user again, and password entry dialogs could offer an additional button saying \"use password manager\" for using/starting it. Stored passwords could be strictly linked to specific users, specific KPart protocols and specific URLs.\n\nWouldn't that all be super useful and super sweet? ;)"
    author: "Datschge"
  - subject: "Re: Browsing the Web with Konqueror"
    date: 2003-03-24
    body: "There's a password manager in the control panel, but I have never worked out what it does."
    author: "caoilte"
  - subject: "Re: Browsing the Web with Konqueror"
    date: 2003-03-27
    body: "Well, that's the basic functionality of the password widge so far. It lets you decide how the password is hidden and how long the password is kept in memory until the user is asked for a password again.\n\nBtw good news, it turned out that we might get what we want in kde 3.2. According to the feature plan at http://developer.kde.org/development-versions/kde-3.2-features.html George Staikos, Dawit Alemayehu, Waldo Bastian and Tim Jansen are \"in progress (yellow)\" of working on \"Libraries: KDE Password Registry and Wallet\". =)"
    author: "Datschge"
  - subject: "Re: Browsing the Web with Konqueror"
    date: 2006-01-11
    body: "We use a great free password manager at work: CyberScrub KeyChain Password Manager http://www.cyberscrub.com/keychain\n\nI copied this from their site:\nManage ALL Passwords with One Phrase. When you log on to KeyChain with your Master Pass Phrase you will have instant access to all of your password protected websites. Select your destination from a special list you have created- then simply \"Click & Go\". It's that easy! Each time you visit a site requiring a user name and password KeyChain auto enters this information and logs you in. It even prompts you to add these passwords to the program if you have not already done so. Never manually fill in credit card details again. Online shopping is a snap because KeyChain automatically enters your selected credit card details, Shipping and Billing address and more. All of your data is secured with strong encryption. Only you have access to the sensitive data within KeyChain. All information, including passwords, credit cards and other data, is protected with strong encryption algorithms. The USB flash drive also synchronizes with your host computer to back up your encrypted password list. This is an important feature should your PC crash or fail. You may also utilize the USB flash drive, if desired, for Dual User Authentication. This requires the user to not only enter the Master Pass Phrase, but also to plug the USB flash drive into their computer. Easy to use, backed award winning CyberScrub Customer Support."
    author: "WSA"
  - subject: "Re: Browsing the Web with Konqueror"
    date: 2006-01-13
    body: "Haha: \n\nSystem Requirements\nWindows 98/Me/NT/2000/XP\n[...]\nCompatible with IE browser\n\n"
    author: "cm"
  - subject: "Kexi (OT!)"
    date: 2003-03-22
    body: "Sorry to be off-topic, but in case anyone (like me) didn't notice it before:\nI just found out that the Koffice.org page has added Kexi together with some\nadditional info and screenshots.\n"
    author: "Martin"
  - subject: "Re: Kexi (OT!)"
    date: 2003-03-22
    body: "Great =)"
    author: "Datschge"
  - subject: "Now this looks tasty *_*"
    date: 2003-03-22
    body: "Kexi used in a productivity environment containing slicKer/Kards framework as replacement for Kicker: http://www.koffice.org/kexi/screenshots/manipulation.png"
    author: "Datschge"
  - subject: "QT and KDE overlap"
    date: 2003-03-22
    body: "One thing that I've seen raised occasionally by the GNOME camp and have also noticed during brief QT changelog glances myself is the quite heavy overlap between QTLIBS and KDELIBS.\n\nI can see how it happen, good work in KDE has been duplicated into QT. I can even see the strong temptation to leave the duplication in (it's probably implemented differently and would be a pain to change etc).\n\nI just wonder what the long term plans are in KDE with respect to newer features in QT (particularly the sweet looking QT scripting). Will KDE libs be slimmed down any to improve startup? What sort of timeframe is there (years, decades, etc)?\n\nThe CVS Digest is excellent! Well Done!"
    author: "caoilte"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-23
    body: "> brief QT changelog glances myself is the quite heavy overlap between QTLIBS and KDELIBS.\n\nOf course, there is a big overlap between gnome-libs and gtk+ with things like libegg. Of course, a near rewrite of gtk in gtk2 (and even gtk1.2) helped remove a lot of duplications (similiar to KDE 1.2 and KDE 2.0 stuff in Qt 2.x...)\n\nI'd expect more duplications to happen, as I expect that like KDE and Qt, gtk3 won't be as different from gtk2 as gtk2 was from gtk1.x, and gnome3 similiarly won't be as different from GNOME 2.4 (equivalent to KDE 2.3 in terms of versioning, I suppose), as GNOME 2.0 was from 1.4.\n\n> I just wonder what the long term plans are in KDE with respect to newer features in QT\n\nAs long as the KDE solutions remain better, KDE probably will use them."
    author: "test"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-23
    body: "> As long as the KDE solutions remain better, KDE probably will use them.\n\nThat's the answer I was looking for, thanks. (oh well)"
    author: "caoilte"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-23
    body: ">> I just wonder what the long term plans are in KDE with respect to newer features in QT (particularly the sweet looking QT scripting). Will KDE libs be slimmed down any to improve startup? What sort of timeframe is there (years, decades, etc)?<<\n \nDon't worry:\nIf somebody is interested in doing this or if somebody has time, it will be done.\n\nQSA is already integrated in Kexi und will be in more applications soon...\n"
    author: "Norbert"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-23
    body: "Is QSA extensible to other scripting languages? Ie \nnot just ecmascript... \n\nand isn't this a bit odd, having both QSA and KJS as ecmascript \nimplementations? \n \nThis is one thing that is very powerful on windows... \nits possible to script everything from vb, perl, or python... \nanything that supports com+ and idispatch. \n\nI'm sure that adding QSA will help a lot of apps modularise, \nbut it would be really great if it were a fully open scripting interface. \n\nI also don't yet fully understand what the disconnect between an \napps dcop interface and its qsa interface will be. \n\nOn another point, I'm hoping that the QT designer will be broken down into \ncomponents, allowing reuse without hacking it up as everybody seems to \nbe doing at the moment ( quanta and kexi if I'm not mistaken. )\n"
    author: "rjw"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-24
    body: "Sure: it would be great if QSA would be open for other scripting languages. But even QSA is not yet released, so stop complaining about that. Be patient. Maybe something like that will come. Who knows. Everything can be done if enough people volunteer. So what about you? \n\nQSA uses the internal interfaces of an application, slots that are internally used, e.g. for changing a QFont object.  Out of the box it cannot access other applications - the application developers have to decide which internal objects are scriptable - DCOP is used for scripting applications from the outside. It can use the public interface of any application (as long as it has an dcop interface)\n\nNobody is hacking up QT Designer (except the developer at Trolltech:-). The QT Designer is extensible and can be used for different purposes, e.g. designing GUIs and writing QSA scripts and debugging them. \nIt seems like you haven't seen/used it before...\n"
    author: "Norbert"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-24
    body: "I don't know what the benefit would be in not speaking about this... it is actually better to talk about these things before they are finalised... criticism can be constructive.\n\nAnyway - does it not seem a bit silly to have to create two \"scripting\" interfaces - DCOP and QSA - for your app to have in and out of process scripting? This is not required if writing a COM application, and that is a competitive architecture. \nSo maybe a bridge can be created? \n\nLook at quanta - they made a messed up embedded QT designer. \nWhy are the relevant bits not integrated in Kdevelop? This is currently very confusing - launch qt designer from kdevelop, its a whole other IDE... this is not half as nice as other IDEs with gui builders. Dare I say it, Visual Studio...  \n\nAll suggesting a less than optimal modularity.. \n"
    author: "rjw"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-24
    body: "You are wrong.  KDE extends Qt (via inheritence) but it does not reimplement anything in Qt unless the code in Qt is truly unusable.  I don't know of any instance of this off-hand other than the networking code.  Our networking system is very different and goes far beyond what Qt provides.\n\nWhy would you ask Gnome people about KDE when they clearly do not use it or develop for it?  If you have a specific issue with duplication or bloat (*specific*), bring it up with KDE developers and it will be addressed.  Don't toss out generic complaints with no backing and expect a response though.\n"
    author: "George Staikos"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-24
    body: "I was merely interested in whether the problem existed. QT3 does do extra  stuff like DB interfacing, and I only expected a response if there was something to say."
    author: "caoilte"
  - subject: "Re: QT and KDE overlap"
    date: 2003-03-24
    body: "Maybe will be beter if the kdelib license was BSD like soo Trolltech can integrate some KDE technologies in QT."
    author: "tof"
  - subject: "any news about aRTs?"
    date: 2003-03-22
    body: "Hi,\n\nIt's quite frustrating these days to get 1000 dr. konqi error messages due to knotify, which relies on aRTs, which seems does not work these days (I'm on latest redhat beta and everything was compiled from CVS 2 days ago)...\n\nThanks,\nHetz\n"
    author: "hetz"
  - subject: "Re: any news about aRTs?"
    date: 2003-03-23
    body: "If you want a stable KDE, RedHat probably isn't the way to go, I'm afraid, at least not from the stock rpms in my experience."
    author: "GldnBlls"
  - subject: "Re: any news about aRTs?"
    date: 2003-03-23
    body: "I really don't want to offend anyone, but I think arts sucks. It works unreliable for me, and the latency is really bad, although i have a fast machine.\n\nI am not one, but I've not heard of one programmer who is a friend of arts.\n\nCould it be arts lacks behind the rest of KDE?"
    author: "me"
  - subject: "Re: any news about aRTs?"
    date: 2003-03-23
    body: "I've never had any problems with arts although I have a slow machine (<1GHz). But I have a sound card which is really good supported by ALSA.\nMaybe this is your problem: the configuration of the sound card."
    author: "Peter"
  - subject: "Not so good at all - peaty !!"
    date: 2003-03-23
    body: "Moving form 3.0.5a at a Red Hat system many config stuff is not working anymore at 3.1.1.\n\nCan not increase the number os virtual Desks, can not config KNewsTicker, etc...\n\nMany Configure options just not responding anymore, it is is ignoring the command, no message, no nothing !!\n\nHope my efforts to downgrade back to 3.0.5a could succeed...\n\nMaybe the next \"stable\"  one  :-(("
    author: "Thomas TS"
  - subject: "Re: Not so good at all - peaty !!"
    date: 2003-03-23
    body: "Troubleshooting 101... if you are experiencing problems other people are not maybe you are mis-diagnosing the cause of the problems. It's not KDE 3.1.1. It's:\n1) bad RPMs\n2) a permissions problem introduced to $KDEHOME\n3) Something odd Red Hat has done\n\nI would not downgrade over this. I'd find the problem. Try a news group, a mailing list or even here. I can tell you that from the information you have given I don't have enough to tell you what the problem is exactly but I'm sure it's pretty obvious if you fish around."
    author: "Eric Laffoon"
  - subject: "Re: Not so good at all - peaty !!"
    date: 2003-03-23
    body: "I read that one user had problems solved after installing redhat-menus*.rpm from noarch/ directory."
    author: "Anonymous"
  - subject: "Re: Not so good at all - peaty !!"
    date: 2003-04-05
    body: "hi, do you know where to find the redhat-menus-0.38-1.noarch.rpm. it says to install it before upgrading to kde 3.1.1. thanks"
    author: "sean"
  - subject: "Re: Not so good at all - peaty !!"
    date: 2003-04-06
    body: "Can't you read? :-) ftp://ftp.kde.org/pub/kde/stable/3.1.1/RedHat/8.0/noarch"
    author: "Anonymous"
  - subject: "Re: Not so good at all - peaty !!"
    date: 2003-03-23
    body: "If you want the genuine KDE, don't use RedHat.\nI recommend SuSE, it is really well done."
    author: "KDE User"
  - subject: "Yay FTP bugfix!"
    date: 2003-03-23
    body: "I think that FTP bug may be one that's been bugging me for a long time.  I didn't report it because I didn't have any way of finding out what was causing it or submitting a testcase, but hopefully it will work now.  Thanks Thiago!"
    author: "not me"
  - subject: "Re: Yay FTP bugfix!"
    date: 2003-03-24
    body: "Ha, is the ftp ioslave working again (reliably) ?\nThat would be wonderful, It did not work for me since a 2.x release (was not so important, most of the time I use sftp anyway...), but it is very good to hear it's working again...\n"
    author: "Thomas"
  - subject: "KWrite & Qt Font bugs: Redhat or KDE or..."
    date: 2003-03-23
    body: "Using RH Phoebe beta3 (kde 3.1) or RH8.0 w/ kde 3.1.1 - \nI have found several bugs with KWrite  1) when the user logs off, leaving kwrite open, and then logs in again, KWrite is blank, 2)on 3.1.1 KWrite seems to automatically set dynamic word wrap on which causes code to sometimes appear screwy and there doesn't seem any to permanently set the dynamic wrap off 3) the really nice left margin vertical bracket lines that appear in 3.1 are not in 3.1.1.  btw I use KWrite as my main text editor and really like the improvements that have been made in it.\n\nFonts that look okay in Qt Designer - when compiled and run look much different (larger and uglier) - suspect this is a RH problem.\n\nI know some feel that since RH messes with KDE that it should be ignored, but in reality, it is the largest Linux distro in the US and frankly is a pretty nice distro to use so any help anyone can provide is greatly appreciated. thks."
    author: "John Taber"
  - subject: "Re: KWrite & Qt Font bugs: Redhat or KDE or..."
    date: 2003-03-23
    body: "Most of the developer effort right now is focused on Kate I think.  I haven't noticed these problems using Kate so maybe switching would solve your problems.  Besides, Kate is really nice with its file list, embedded terminal, and split-window features.  OTOH, maybe these are RH-related problems, in which case the only people who can help you are RedHat.\n\nIt sounds like your problem with word wrap isn't really the bug that turns it on, but the fact that any wrapped lines start at the first column instead of being indented.  AFAIK KWrite/Kate would be the first editor to fix this problem if this was fixed.  Imagine if KWrite/Kate could automatically and intelligently wrap your long lines for you depending on what you were writing (html, C, Perl, etc).  That would be great!"
    author: "not me"
  - subject: "Re: KWrite & Qt Font bugs: Redhat or KDE or..."
    date: 2003-03-24
    body: "Imagine if your request was just about to be added to CVS :)  It's not as intelligent as you might want it, but it's enough.\n\nJust one more bug to squash first..."
    author: "Hamish Rodda"
  - subject: "Re: KWrite & Qt Font bugs: Redhat or KDE or..."
    date: 2003-03-24
    body: "1) known bug (http://bugs.kde.org/show_bug.cgi?id=49270) with known fix, just haven't committed it because it involves a change to kparts.  It'll be in 3.1.2 I'd say.\n\n2) config -> view defaults -> dynamic word wrapping.  BTW as I just posted a nicer indenting is coming for 3.2\n\n3) I don't really understand this one... you mean the folding indicators? F9, and again use the view defaults..."
    author: "Hamish Rodda"
  - subject: "Re: KWrite & Qt Font bugs: Redhat or KDE or..."
    date: 2003-03-25
    body: "Hamish,\nthks for the info - I have switched to Kate as suggested and problem #1) restoring the file seems to work fine in Kate(at least in Phoebe)  btw As a regular KDE user, i still find the bug reporting process too complicated - mail lists are a pain to subscribe to (and I really don't want to read or delete all that) - bugzilla things are too complicated to subscribe to (not really hard but most of us users aren't interested in taking time to do all this stuff).  Newsgroups or websites like this still seem to be the easiest - "
    author: "John Taber"
  - subject: "Re: KWrite & Qt Font bugs: Redhat or KDE or..."
    date: 2003-03-26
    body: "That's fine, but the chances of you getting a solution are much less here.  If you want to make sure you are heard, unfortunately you have to register for bugzilla.\n\nAlternatively there is a news server (see kde.org website) with feeds from the kde mailing lists, you can click reply via email and read/write to mailing lists that way."
    author: "Hamish Rodda"
---
KDE 3.1.1 released, a <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/kafka/">WYSIWYG mode</a> for <a href="http://quanta.sourceforge.net/">Quanta</a>,  many bugfixes 
in <a href="http://kmail.kde.org/">KMail</a>, KWin, Kicker and <a href="http://www.konqueror.org">Konqueror</a>. Read all this and more in the <a href="http://members.shaw.ca/dkite/mar212003.html">latest issue</a> of KDE-CVS-Digest.
<!--break-->
