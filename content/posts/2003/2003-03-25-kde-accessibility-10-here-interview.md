---
title: "KDE Accessibility 1.0 is Here + Interview"
date:    2003-03-25
authors:
  - "falonaj"
slug:    kde-accessibility-10-here-interview
comments:
  - subject: "cool"
    date: 2003-03-25
    body: "Things like kmousetool have really helped me, as I have slight CTS (carpal tunnel syndrome)... \n\nHowever, for true accessability features, an API like AT-SPI or GNOME's ATK is needed.. lets hope trolltech helps the KDE Accessability project with this! "
    author: "lit"
  - subject: "Re: cool"
    date: 2003-03-25
    body: "Read the interview..."
    author: "ac"
  - subject: "Re: cool"
    date: 2003-03-26
    body: "i did.. and?"
    author: "lit"
  - subject: "Re: cool"
    date: 2003-03-26
    body: "<schnipp>\nHow does the KDEAP relate to other Accessibility Projects?\n\nCurrently there are three applications in the kdeaccessibility module. An infrastructure like ATK and AT-SPI from the GNU Accessibility Project (GAP) is yet missing. But we are currently discussing how this can be changed. One possibility (which is the one we currently focus on) would be to write a bridge between Qt and ATK. Qt already has some QAccessible interface which needs to be extended for that.\n\nDuring design and implementation of the infrastructure we will make sure that our solution interoperates with the solutions of the other Accessibility Projects. \n</schnipp>"
    author: "GingerBreadMan"
  - subject: "Screen Keyboard"
    date: 2003-03-25
    body: "Is such thing planned?"
    author: "Anonymous"
  - subject: "Re: Screen Keyboard"
    date: 2003-03-25
    body: "Good question!  I think Qtopia has a screen keyboard, maybe KDE could make use of that since Qtopia is GPL."
    author: "ac"
  - subject: "Re: Screen Keyboard"
    date: 2003-03-25
    body: "We are currently focusing on other areas of accessibility, as there are already a number of screen keyboards available, but having a KDE screen keyboard would definately be nice.\n\nSome people are thinking about starting such a project, but I am not sure how soon this will happen."
    author: "Olaf Jan Schmidt"
  - subject: "Kmouth"
    date: 2003-03-26
    body: "Am I only one who thinks that KMouth needs a new name?"
    author: "Janne"
  - subject: "Re: Kmouth"
    date: 2003-03-26
    body: "No, you are not. Maybe Konversation would be nice, or even KSpeach. KMouth sounds really weird to me. Can the program eat too?"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Kmouth"
    date: 2003-03-26
    body: "Konversation is a IRC client."
    author: "AC"
  - subject: "Re: Kmouth"
    date: 2003-03-26
    body: "Please, enough with the KNames already :)"
    author: "Haakon Nilsen"
  - subject: "Re: Kmouth"
    date: 2003-03-26
    body: "Why do people complain about the KNames?  It's really bad the way you see people using the KNames as a reason to bash KDE.  At the same time those people have nothing to say about the GNames.  It's really puzzling.\n\nI know you're not doing this, I'm talking in general. :-)"
    author: "Navindra Umanee"
  - subject: "Re: Kmouth"
    date: 2003-03-27
    body: "there are more knames than gnames.... that's why.."
    author: "lit"
  - subject: "Re: Kmouth"
    date: 2003-03-27
    body: "Well I guess some people just have to bitch about something. Another reason might be that in some cases it's just plain silly. You might say 'but they are just names', they are not, they are identifiers, they say something about the program.\n\nNames starting with a K can be put in 2 categories (maybe even 4 but the last two categories I'm thinking about aren't used ver much if at all): KNames and Kames.\n\n- KNames, a K is added to indicate the program is part of a KDE package or is written for KDE, examples: KWord, KOrganizer, KMail.\n- Kames: The first letter of a (descriptive) name (ususally a C) is replaced by a K, examples: Klipper, Kompare, Konsole, Konqueror.\n\nIMHO the KNames aren't that bad. The K can be seen as a kind of KDE branding, just like MS Office, MS Outlook etc. Maybe the menu entries should even say: KDE Word, KDE Organizer, KDE Mail etc.\n\nIMHO the Kames are very bad, replacing the C (or another letter) with a K doesn't add anything besides confusion and takes away clarity. The K just looks strange, and some non-native english speakers don't \"get it\" at all. These names should be shot on the spot.\n\nJohan Veenstra\n"
    author: "Johan Veenstra"
  - subject: "Re: Kmouth"
    date: 2003-03-27
    body: "I disagree.\nMost European speakers don't have a problem to understand\nthis at all. The letter \"K\" is always associated with\nthe same phonetic sound throughout Europe. And everyone\nwho understand English at all will know that \"K\" has the\nsame sound as \"C\" (i.e. in O.K., king etc.). \nHere in Germany we always use the \"K\" and we actually\nwrite \"Konsole\" which is absolutely correct German.\nIn fact non-native speakers will be less confused than English speakers\nbecause they are not used to the language and have to \"decipher\"\nthe words separately. The eye of an English speaker however\nknows the \"shape\" of a word. So when reading, the eye jumps\nfrom word to word only guessing the word according to its \"shape\".\nThat's why a native reader will hang a bit while reading wrong-spelled\nwords. That's the reason behind orthography and why it is important\nthat words aren't written to differently: To be able to guess the words \nand read faster. Sorry for the long lesson in language science ;-)\nSo, in a title of a movie, a book or an application it doesn't really\nmatter because there are only few words and it's not necessary to read\nthem faster. From a scientific point of view, there is no reason\nat all to change this.\nNow my personal opinion: \nI think it's good we have some kind of branding. Something that keeps\nthe applications close to each other, just like we have the same\ntoolbar in every application. \"Microsoft Word\", \"Microsoft Paint\",\n\"Microsoft Excel\". Basically they do the same. But the K should IMHO \nnot be an excuse to take boring standard words and only put \na \"K\" in front of them. I like i.e \"Karbon14\". But only major application\nshould have such a name. I think it's not a good idea to call\nthe calculator \"KSynthoxechohexamatic\". That would be simply confusing\nfor a novice and he wouldn't be able to find it in the K-Menu hierarchy.\nJust my 0,02 \u0080\n\n"
    author: "Martin"
  - subject: "Re: Kmouth"
    date: 2003-03-27
    body: "I'm a native english speaker and I have to agree with Johan that \"Kames\" are vile. A ppreciated your lesson on linguistics, but the simple fact is that for me, and many others, \"konversation,\" \"Konsole\" et al are horrible, clunky (sorry, klunky) abominations! It's like being stuck in a lift with someone who answers your questions with puns - very, very annoying!\n\nP.S. I've noticed (and this is very unscientific!) that \"Kames\" (thanks Johan, that's a great word!) tend to annoy german speakers less. I suspect it's as you say - konsole is a word in german, and so looks natural. It does NOT look natural to me!"
    author: "Ed Moyse"
  - subject: "Re: Kmouth"
    date: 2003-03-27
    body: "The Kames (Krita, Konqueror, Konsole, Kompare, Komponist, Katan, Klipper, Kicker, Kapitalist, Kant, Klock, ...) are great! If it is possible to find a Kame that makes sense for an application it should be used. Only if that fails a KName should be used, although there are (were) a few good KNames too (KSpread, KIllustrator). They are uglier when pronounced (like kpat, kpager, kmid, kmix, kcharselect, ...)."
    author: "Erik"
  - subject: "Re: Kmouth"
    date: 2003-03-28
    body: "I hava an idea ! A great name ...\n\nKaralho\n\n\nWhat about ?"
    author: "Marianne C."
  - subject: "Re: Karalho"
    date: 2003-03-28
    body: "> Karalho\n \nThat may be a good name. I don't know. Does it mean anyting in any language? It looks a little difficult to pronounce. I think Karallo would be easier."
    author: "Erik"
  - subject: "Re: Karalho"
    date: 2003-10-11
    body: "\u00e8s mesmo t\u00f3t\u00f3.\n\nOu n\u00e3o tens ou \u00e9 mm mta pequenino... fdx.. \n\nE a parte do \"allo\" dever ser o som q fazes quando o t\u00e1s a engolir.\n\nBorrego do Karalho."
    author: "Kona"
  - subject: "Re: Karalho"
    date: 2003-10-13
    body: "Sorry, I do not understand."
    author: "Erik"
  - subject: "Re: Kmouth"
    date: 2003-04-01
    body: "Muito engrakado!"
    author: "EricE"
  - subject: "Re: Kmouth"
    date: 2003-04-02
    body: "> Karalho\n>\n> What about ?\n\nGood idea. KDE already has a tradition of using four letter in program names (http://kroupware.kde.org), this would well fit in.\n\nOn the other hand, being a man myself, I am strictly against replacing our pride with a computer program, if this is what you intend. Anyway, if you indeed manage to write a \"Karalho\" program that helps women to get pregnant, let us know..."
    author: "Olaf Jan Schmidt"
  - subject: "Re: Kmouth"
    date: 2006-04-03
    body: "I wrote a very long (and, I must admit, very rambled) thing on kde-look.org about Knames being a high contributor to the un-comfortability factor for Windows users new to KDE, and they all told me that this has already been fixed with the \"Descriptions\" in the Kmenu and that new users should just \"Get used to it\".\n\nNow, instead of having a mass grouping of \"Keverything\" down the list, we now have long descriptions to read.  Hurrah?\n\nAbout the 'Branding' thing, I don't see MSinternetexplorer, MSoutlook, MSwinmediaplayer, MSnotepad or MSpaint all bunched up in the MSwinstartmenu on Windows.  =P\n\nNot even Apple names everything with an \"I\" in front of it."
    author: "Nick"
  - subject: "Speech"
    date: 2003-03-26
    body: "So how do I get KMouth to speak to me? I assume I need some kind of backend to do this. Are there free ones I can try out?"
    author: "Trynis"
  - subject: "Re: Speech"
    date: 2003-03-26
    body: "Installing festival will work. (Sorry, no link, but since it is GPL it should be easy to find)"
    author: "Troels"
  - subject: "Re: Speech"
    date: 2003-03-26
    body: "http://www.cstr.ed.ac.uk/projects/festival/"
    author: "Chris Howells"
  - subject: "kmag"
    date: 2003-03-26
    body: "KMagnifier is really cool, I have been looking for something like that for a long while!!!"
    author: "Osho"
  - subject: "Re: kmag"
    date: 2003-03-26
    body: "For a long while? The original KMag existed already in KDE1 times. :-)"
    author: "Anonymous"
  - subject: "Typo?"
    date: 2003-03-26
    body: "Shouldn't that be \"Gunnar Schmidt\" instead of \"Gunnar Schmi Dt\"?"
    author: "Somebody"
  - subject: "Re: Typo?"
    date: 2003-03-27
    body: "No, Gunnar choose to be called \"Gunnar Schmi Dt\"."
    author: "Olaf Jan Schmidt"
  - subject: "Re: Typo?"
    date: 2003-03-27
    body: "Oh..."
    author: "AC"
  - subject: "Re: Typo?"
    date: 2003-03-27
    body: "Hmmm, that should have been a question in the interview.  :-)"
    author: "Navindra Umanee"
  - subject: "Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "Have a look at this to get some motivation for KDE development:\n\nhttp://www.gnome.org/~michael/XimianOOo/img7.html"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "They specifically talk about the solution on Unix and yet they choose the most expensive Qt price possible which covers Mac/Windows on top of Unix.   I'm sure they're also exaggerating the 13 million dollars part.  Trolltech looks willing to negotiate above 20 licenses (from the pricing page).  Clearly they haven't even tried talking to Trolltech.\n\nOn the other hand, if Ximian was using KDE/Qt maybe they wouldn't be\nso horribly horribly HORRIBLY late in their product development?  Who knows how much revenue they killed by being so late."
    author: "Navindra Umanee"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "That thing is really just incredible - it manages to be both horrendously misleading and technically true in the disclaimer-word kind of sense. Notice how they say \"<13 million dollars(?)\".  The number is rounded up quite generously (13 is /not/ how one rounds 12.4), and of course, as you said, skips volume discounts, and that they're buying a version that's 2x as expensive as for a single platform. That's presumably hidden behind the question mark, but certainly the impression provided is a highly misleading one. I am quite disappointed that a developer would write that, that's really unbefitting of the title. Couldn't he come up with at least a /technical/ argument, so not to resort to questionable marketing?\n\nOf course, the issue of 5000 developers is ridiculous. How many organizations have that many full-time, paid programmers?[1] I doubt more then a few have so many, and certainly not on the same team. Windows 2000 had 1400 people working on it (http://www.usenix.org/events/usenix-win2000/invitedtalks/lucovsky_html/sld013.htm), and \nMicrosoft is certainly among the biggest software vendors there are. \nAnnual pay for so many developers, even assuming low ($40,000) just-out-of-college salaries in reasonably inespensive area (not NYC or Sillicon Valley) will be at least around 200 million,\nand that's not counting benefits, computers, other development tools (you need \nsome pretty fancy version control/configuration management software at THAT scale, I think).\nAnd of course, you're not gonna has 5000 green, fresh-out-of-college developers running a project that large -- there would have to be very senior/years of experience software engineers, etc., to do design, and so forth, and those are going to have to be paid a lot more. Then management, very sales/marketing, etc., people, and so forth. I figure with all that stuff, that'd blow up the organization budget to > $600 million. And at that scale, a number that will be (likely quite a bit) less than 6.2 million dollars for a toolkit license seems quite reasonable.  \nOf course, numbers pulled out of context can look scary.\n\nOr do they perhaphs want OSS people to work on software that then will be sold in a proprietary edition? Well, then I doubt that they would get 5000 contributors willing to sign away their rights, either. \n\n[1] For comparison: There are about 200 active contributors to KDE, with most being part time, and that includes non-programmers -- who wouldn't need a Qt license if this was a commercial organization, either.\n"
    author: "Sad Eagle"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "One thing I'll add.  I mentioned lost revenue due to the extreme extreme delays of up-to-date Ximian products.  I forgot to mention the amount of money they had to shelve out to *pay* their developers in that time. And of course, they're explicitly talking about proprietary development here."
    author: "Navindra Umanee"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-28
    body: "Yes, but there is a little difference between Norway with PKB 50000$ per capita  and the Eastern Europe, for example, with PKB 5000$ per capita ( hint: count zeroes ) where the out-of-college developer costs you less than 6000$. Those 4660$ for the Qt licence and 1500$ for upgrades ( every year ) is noticeable sum."
    author: "Tom"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-28
    body: "The 4660$ for the QT licence are for the trio pack (X11/Win/Mac) and its actually 1450$ for one year of support and maintenance. What do you think you have to pay to get professional support for any other toolkit out there?.\n\nAnd your comarision of the price to developer cost in Eastern Europe are fataly flawed, it does not correspond to the real world. In the real world only a small part of SW gets developed in low-cost countries, the major part of SW development takes place in the US/Canada an Western Europe. "
    author: "Morty"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-28
    body: "Is there professional support for GTK available at all?"
    author: "Anonymous"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-28
    body: "I don't want a proffesional support. I just don't want to buy each new version of Qt at a full price."
    author: "Tom"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-28
    body: "You get the bugfixreleases, and you usually don't uppgrade between major releases without good reasons, new features you need etc.\n\nBTW if one finds the cost to high you don't buy the trio pack to all the developers in the team, but use a common platform for all and rather designate a few to do the porting. "
    author: "Morty"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "I wondering for some time about one thing, which is also present in the above web page: \"the Free Edition license applies to the development phase - anything developed without Professional or Enterprise Edition licenses must be released as free/open source software...\".\n\nWell, if I start to develop a GPL'd appilcation and I use the GPL licence for QT, then GPL is the valid license. GPL permits the author to close the software and make it propietary. How can TrollTech take away this right from the author? I think it cannot, and there were already cases when an open source QT/KDE project was closed. I doubt that those developers had a commercial license when they started to _develop_. \n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "Well, what I can read outof this:\n\nFor Gnome it now pays off that they were attracting a big company like SUN. Miguel d.I. did a really good job in the \"get valuable contacts field\"). I fear, that my favourite desktop will wall behind because of lack of support from a big player. Let's face it: We do not have an alternative office application that can compete to OpenOffice. Even if OpenOffice (yeah, it's 'open'...haha) attracts very few hackers from outside of SUN and even if it needs a business meeting from the Gnome-Company and the Sun-Company to disucss further proceedings, this Office-App is widely accepted and used (hell it _is_ the best Office availableby now on Unix/Linux) in the OpenSource world. \n\nIf there will be an Enterprise-Desktop one day on Linux it will surely not be KDE (because of a less good integration with OpenOffice). It will be Gnome."
    author: "Thomas"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "Good integration with the office packet is an important point, no doubt, but not the only one. KDE's strength is the effectiveness development of its development environment which will (<wishful-thinking>)in the next years show its real advantages, as open source applications become more complex.\n\nBeside that, who nows how long Ximian's venture capital will last for both working on various Gnome-related projects and Mono? I can't imagine that their Gnome-stuff will reach break-even in the next years."
    author: "AC"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "Except for the KDE/Qt bashing, I think ximian makes a good point. The integration of OpenOffice.org right now is pretty bad. Both for Gnome and KDE. \n\nNow this integration -is- important to for any environment to become widely accepted. I don't care (just as most of you will be able to manage without it), but most \"average users\" do care. It all boils down to the well-discussed integration topic. All the different styles/widgets/etc used throughout applications like kde/gnome/mozilla/openoffice.org/acrobat reader/etc/etc are just plain confusing to the (again) \"average user\".\n\nBut look at is like this:\nXimian made OO.o start 15 seconds faster, made the icons changeble, improved the user interface font, made printing more intergrated (with cups, i think), recent documents available, etc. Now nothing really gnome specific (they didn't port it to gtk or something), why wouldn't we take advantage of it? It shouldn't be too hard to make the changes they made work with KDE too."
    author: "Siets"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "In practice, GNOME development is a hundred times more closed than KDE development."
    author: "ac"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-27
    body: "Shouldn't come as a shock.  Ximian is headed by Miguel who is the most famous basher and flamer of TrollTech and QT.  Even today he and his cohorts love to bash anything related to TrollTech.  Hence we see the continuous comments about tainted and about TrollTech being owned by Canopy Group.  All FUD People like Owen and Migule don't really think about ethics when it comes to GNOME VS KDE."
    author: "anon"
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-28
    body: "Because \u00a0Monica Lewinsky is a KDE developer. \nShe belongs to Ksuck team.\n\n[8-D....\n\n\n\n"
    author: "Marianne C."
  - subject: "Re: Offtopic: Why KDE is supposed to suck"
    date: 2003-03-28
    body: "Does that mean that the Accessibility team is going to be coming out with KCigar soon?"
    author: "Xanadu"
---
The <a href="http://accessibility.kde.org/">KDE Accessibility Project</a> has released the first stable version of the KDE Accessibility module.

The package currently contains three nifty <a href="http://accessibility.kde.org/aids/">accessibility aids</a> (screenshots and info):
<a href="http://kmag.sourceforge.net/">KMagnifier</a>, a screen magnifier,
<a href="http://www.mousetool.com/">KMouseTool</a>, a program for people who have trouble clicking the mouse, and 
<a href="http://www.schmi-dt.de/kmouth/index.en.html">KMouth</a>, a program that enables people to let their computer speak for them.  The module has been translated into <a href="http://i18n.kde.org/teams/index.php?action=info&team=da">Danish</a>, <a href="http://i18n.kde.org/teams/index.php?action=info&team=de">German</a>, <a href="http://i18n.kde.org/teams/index.php?action=info&team=he">Hebrew</a> and <a href="http://i18n.kde.org/teams/index.php?action=info&team=pt">Portuguese</a> thanks to the hard work of the <a href="http://i18n.kde.org/">i18n teams</a>. An <a href="http://accessibility.kde.org/about/gunnar.php">interview with Gunnar Schmi Dt</a>, the maintainer of the KDE Accessibility module, is also available.
The interview contains detailed information about future plans of the KDE Accessibility Project as well as its relation to other accessibility efforts.
<!--break-->
