---
title: "Interview:  Konqi meets Lindows CEO Michael Robertson"
date:    2003-12-24
authors:
  - "fmous"
slug:    interview-konqi-meets-lindows-ceo-michael-robertson
comments:
  - subject: "Salvation is near!"
    date: 2003-12-23
    body: "<p>\n>For example File-Open dialogs in OpenOffice are different with <br>\n>the one found in Mozilla or the standard KDE \"File-Open\" dialog. <br>\n>We need consistency! We need integration!<br>\n<p>\n\nMichael, keep cool! --- Salvation is near.... ;-) </p>\n<p>\nJust look at these cool preview screenshots.</p>\n<p>\n   Ximian Evolution integration:<br>\n<a href=\"http://www.kde-look.org/content/preview.php?file=8950-1.png\">\nhttp://www.kde-look.org/content/preview.php?file=8950-1.png</a><br>\n<a href=\"http://www.kde-look.org/content/preview.php?file=8950-2.png\">\nhttp://www.kde-look.org/content/preview.php?file=8950-2.png</a></p>\n<p>\n   OpenOffice integration:<br>\n <a href=\"http://www.kde-look.org/content/preview.php?file=8529-1.png\">\nhttp://www.kde-look.org/content/preview.php?file=8529-1.png</a><br>\n <a href=\"http://www.kde-look.org/content/preview.php?file=8529-2.png\">\nhttp://www.kde-look.org/content/preview.php?file=8529-2.png</a><br>\n <a href=\"http://www.kde-look.org/content/preview.php?file=7131-1.png\">\nhttp://www.kde-look.org/content/preview.php?file=7131-1.png</a><br>\n <a href=\"http://www.kde-look.org/content/preview.php?file=7131-2.png\">\nhttp://www.kde-look.org/content/preview.php?file=7131-2.png</a><br>\n <a href=\"http://www.kde-look.org/content/preview.php?file=7131-3.png\">\nhttp://www.kde-look.org/content/preview.php?file=7131-3.png</a></p>\n<p>\n   About the OpenOffice/KDE-\"Incubator\" project:<br>\n<a href=\"http://lists.userlinux.com/pipermail/discuss/2003-December/000886.html\">\nhttp://lists.userlinux.com/pipermail/discuss/2003-December/000886.html</a><br>\n<a href=\"http://dot.kde.org/1071245692/\">\n     http://dot.kde.org/1071245692/</a><br></p>\n<p>\n   Sodipodi integration, working KDEPrint und fileopen-Dialogen:<br>\n     <a href=\"http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/stuff/sodipodi-screenshots/sodipodi_print_preview.png\">sodipodi_print_preview.png</a><br>\n     <a href=\"http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/stuff/sodipodi-screenshots/sodipodi_savefiledialog2.png\">sodipodi_savefiledialog2.png</a><br>\n</p>\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Salvation is near!"
    date: 2003-12-24
    body: "Very promissing indeed!\n\nKevin Carmony\nPresident, Lindows.com, Inc."
    author: "Kevin Carmony"
  - subject: "Re: Salvation is near!"
    date: 2003-12-29
    body: "This looks great! \n\nIt will really advance the KDE desktop experience. \n\n-- MR\n\nCEO\nLindows.com"
    author: "Michael Robertson"
  - subject: "Commercial is a hoot"
    date: 2003-12-23
    body: "That was pretty good. I wish they would advertise on TV though."
    author: "a.c."
  - subject: "Re: Commercial is a hoot"
    date: 2003-12-24
    body: "But how do we who love KDE, win the minds of service providers when they seem to be backing GNOME at least as things look at present? They seem to be \"forcing\" standards by leaning toward GNOME. Look at HP and SUN who are some of the biggest fish in the industry. Note that Novell also was on the GNOME band wagon, before aquring SuSE. I do not think they will jump ship soon. UserLinux also seemed to be gravitating toward GNOME. At least distros that go for GNOME should install [all] the QT libs, so that KDE and associaed apps can work.\n\nCb.."
    author: "Charles"
  - subject: "Re: Commercial is a hoot"
    date: 2003-12-24
    body: "It comes down to code.\n\nAs much as anyone wants, desktop linux is dependant on the two. Neither is going away. There are holes in both DE's, some are filled by the other. The reality is that both DE's exist and have developer and user support, and are both growing and maturing. (and surely all must realize that the two applications that make the desktop even possible belong to neither desktop environments.)\n\nThe successful model for the desktop is Microsoft, and the tendancy is to emulate the zero-sum view of market. They won developer mindshare and marketshare by driving others out of business, essentially forcing anyone who wants to play in the desktop arena to play by their rules. If someone wants to try to win by their rules, good luck. They have 49 billion in the bank. How is Ximian going to drive KDE out of business? I would suggest that the opposite is more likely. Novell and Sun don't own the desktop, and have both been on a dramatic downwards spiral. The frustrating part for the would be monopolists is any improvement in one desktop is usually quickly transferred to the other. And it benefits both. Look at Gaim and Kopete. There are some base libraries that are shared. Both benefit, and can focus on the user experience according to their respective visions.\n\nThe way to gain marketshare is to change the rules. What if there is a thriving competition for ideas and developers? What if there are choices in all the major application functions? What if there is room for many to write and publish useful applications?\n\nWhat if, as the desktop matures, one could use, transparently to the user, any application, any IPC mechanism, any dialog system, any help system, any configuration system, with any desktop environment? What if modularity permits an easy plug-in configuration system into any environment, with modules to fit the many different network/business/user environments? What if dcop or descendants were plugin modules, that someone could use a heavyweight IPC mechanism that fit their usage, others could use a lightweight one? Essentially a standards based desktop, with enormous room for creative extensions. This is a long way off, and much work.\n\nA monopoly in the free software is as bad as in the business end. Look at XFree.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Commercial is a hoot"
    date: 2003-12-24
    body: "\"Essentially a standards based desktop, with enormous room for creative extensions. This is a long way off, and much work.\"\n\nfreedesktop.org is a promising approach to reach that state sometime.\n \n\"A monopoly in the free software is as bad as in the business end. Look at XFree.\"\n\nThe good thing is that XFree is free software, allowing its offspring KDrive by former XFree member Keith Packard to thrive at freedesktop.org/Software/xserver"
    author: "Datschge"
  - subject: "Re: Commercial is a hoot"
    date: 2003-12-24
    body: "This is an US impression. Suse will not switch to Gnome. Linux desktop = KDE desktop. Afaik there is no Gnome desktop market. This may change soon but... KDE 3.2 will be our desktop of choice."
    author: "Andre"
  - subject: "Re: Commercial is a hoot"
    date: 2003-12-24
    body: "you use the word \"seem\" a lot in your message, and i think that says it all.\n\nwe need to keep creating a solid, impressive technology product.\nwe need to build more and better relationships with business, government and not-for-profit organizations around the world.\nwe need to increase awareness of what KDE provides.\n\nas to how we who appreciate KDE can help in that, well... take a look at those three basic ways and ask yourself, \"what can i do to add to one or more of those three efforts?\" it can be as simple and promoting KDE locally to business, or contributing documentation, or helping with enterprise.kde.org, or..... everything helps."
    author: "Aaron J. Seigo"
  - subject: "Re: Commercial is a hoot"
    date: 2003-12-24
    body: "That commercial was awful.\nLooks like it was made in 10 seconds by a 5 year old.\nBe thankful they don't advertise on TV."
    author: "Yellowboy"
  - subject: "Man, that guy makes me sick..."
    date: 2003-12-24
    body: "Can you say opportunist?\n\nI knew you can!"
    author: "AnonnTV"
  - subject: "Re: Man, that guy makes me sick..."
    date: 2003-12-24
    body: "talking about yourself?"
    author: "ealm"
  - subject: "Re: Man, that guy makes me sick..."
    date: 2003-12-24
    body: "Can you say your a dirty troll? Santa going to give to coal this year if your not careful."
    author: "Kraig "
  - subject: "Re: Man, that guy makes me sick..."
    date: 2003-12-25
    body: "Disgusting. Not a single article without trolls.\n"
    author: "trollbuster"
  - subject: "I don't see a market for this clown."
    date: 2003-12-24
    body: "Exactly what does Lindows provide that every other distro doesn't?\nThis guy has PR stunt written all over him, starting with the msfreepc domain."
    author: "frizzo"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "Sure he cares alot about PR. He's doing commercial business you know.\nIt seems he's doing fine too..."
    author: "ealm"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "Did you even read the article?  This \"clown\" as you call him has made 30,000 deployment deals and has Lindows installed on uncountable Wal-Mart PCs.  You don't see a market?  Fool."
    author: "KDE User"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "Well, nobody buys PCs from Wal-Mart over here, I think Wal-Mart got bankrupt in Europe because they were to expensive for our markets. Perhaps he should try Medion. Aldi-Linux, why not? "
    author: "bala"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "first, I believe you are thinking of KMart that went bankrupt in Europe.\nsecond, while people there may not buy PCs from WalMart, a lot of people in North America do when looking for a home or SOHO PC. we've had emails to kde-devel requesting tech support from Lindows/WalMart customers, and i've met a few on IRC."
    author: "Aaron J. Seigo"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-25
    body: "They are called Asda in the UK.  Still around, and definately not bankrupt."
    author: "Jonathan Bryce"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "\"This guy has PR stunt written all over him, starting with the msfreepc domain.\"\n\nYes, but when Microsoft are trying to screw you and your resellers over at every opportunity, you need to be good at turning those attacks into publicity. That way, every move by the monopolist appears to be anticompetitive and you get support (both popular and financial) to keep going. It's a risky strategy, but I'm glad someone is pursuing it."
    author: "The Badger"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "This hard sell marketing stuff makes me nervous. Usually there isn't alot behind it. But in this case, I have to hand it to Robertson. He is going right up against the big guys with the name and lawyers. He says that there isn't hardware, so he has his line of pc's and laptops. He is making a very easy to install and update distribution, without any tie-in, since it is Debian based.\n\nHe has big goals, and I wish him the best. He has learned how to work with the community. Quite impressive.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "The only negative thing I can come up with Lindows is that \"running all users as root\" thing."
    author: "ac"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "hm, as reported on osnews, they seem to have fixed this (sort of) in their latest version, 4.5. it says you are now advised to enter a root passwd during installation."
    author: "mark dufour"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "That has not been true since they introduced Lindows to the public over a year ago. Adding users is part of the config wizard that prompts you at first login. I don't know why people keep thinking this."
    author: "Kraig "
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "CNR, Ailses, Anti-Porn web filtering, easy software purchasing with big selection and good discounts for members, all the drivers available not just open ones, siphone, easy install process, awesome non elitist helpful community, virus scan service thats just a few things off the top of my head."
    author: "Kraig "
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "PR always is/was a weak point of KDE. And Robertson is an expert in good PR. Look at MS or even GNOME - good PR is more important than the quality of a product!"
    author: "Birdy"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "Well, I believe PR was a strength of KDE. Probably there is a transatlantic KDE gap as many KDE developers and supporters are on this side of the Atlantic ocean. But on the other hand the eurocentrism of KDE is a huge advantage as we don't face US legal uncertainty (SCO's inappropriate FUD would be criminal in most parts of Europe, crypto export guidelines, software patents, DMCA).\n\nOn the other hand it is more difficult for us to raise attention in the US or in Slashdottia. Lindows: reversed situation. Lindows only plays a little role in Europe but its PR in the US is wonderful. As the US market is focussed on Gnome because of RedHats committment to Gnome, however, RedHat is not intrested in the desktop market. I don't think (Mr. Robertson may correct me) that there was a real European market for Lindows now. The European desktiop market is already served by Suse and Mandrake that provide more for their customers. Yes, Suse will not drop KDE in favour of Gnome as their market requests it and the market decides, not a brazilian software provider. I guess Novell will get it.\n\nWhat I would like to see is an European office of Lindows that Lindows could spread to our market as well.\n\nAndre"
    author: "andr\u00e9"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: ">What I would like to see is an European office of Lindows that Lindows could spread to our market as well.\n\nMayby such an Office could figure out also, that stuff like \"pay for click'n run - dear customer. This apt-stuff is just too complicated, better be off...\" does not work in europe. Afaik, europe is a harder market for any product since at least west-european users are able to spend more money on IT, including Books and Software... Most of them have DSL, most of them are better informed and more critical about choosing Hard/Soft.\nLindows is reviewed in the german press as the Distro with the worst price-quality ratio: older apps, not even open office included, stuff like blender or ardour? - harr harr.\nALL Distros, that want to be taken serious in e allow anonymous access to FTPservers holding hundreds of fresh apps for free - Michael"
    author: "zettberlin"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "You re right.  Will the customer get it? I don't think so."
    author: "Gerd"
  - subject: "Re: I don't see a market for this clown."
    date: 2003-12-24
    body: "I don't see why you'd have such a reaction to that interview. To me, it shows how positive an influence Free Software is already having on the software industry. Lindows is obviously doing something right in getting the contracts mentioned in the interview, and they are able to deliver this good business model thanks to the base of Free Software upon which they have built their products and services.\n\nFinally software is developing a commons that will benefit everybody except those companies who want to make the basis of the industry a private monopoly."
    author: "Tom"
  - subject: "Lindows Rock!"
    date: 2003-12-24
    body: "That commercial is halairous! Expect it to hit slashdot soon.\n\nI agree with Michael on the integration, though he needs to realize that integration is hard in Open Source projects due to many unfinished, duplicated, and isolated efforts.\n\nMr. Robertson knows bussiness, he supports KDE because it supports his bussiness. It is a simple and wondeful symbiotic relationship.\n"
    author: "RMS"
  - subject: "New Lindows Coup Looming?"
    date: 2003-12-24
    body: " \"This guy has PR stunt written all over him, starting with the msfreepc domain.\"\n\n\nYes -- Michael Robertson knows a lot about successful marketing. You should read his latest \"diary\" entry:\n \n   http://www.lindows.com/lindows_michaelsminutes_archives.php?id=96\n\nSeems he is in talks with a \"1 Billion $US per Year\" company about rolling out Lindows/KDE to them.\n\nAnyway, certainly a very interesting read...."
    author: "Kurt Pfeifle"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-24
    body: "What a load.  I work in a large company.  Believe me when I tell you that when you purchase something from MS via volume licensing (as anyone with more than 5 desktops should do, let alone a $1b company), there are no activations, no CDs, etc...  The installations are usually done via scripted packages without anyone having to do anybody's desktop.\n\nThis guy sounds more and more like a salesman and can lay it on thick."
    author: "rizzo"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-24
    body: "What about a company with 350 employees. MS Win 2000 is installed on about 200 PCs. Yes, updates for the virusscanner are done via local net... Updates of Win2000 are done by hand \"over weekend\" by 3 tech people going from one pc to the next... insert cd....(yes, a CD..!)  klick klick klick... wtf?.... klick klick... Damn I've to reinstall everything on that #!$\u00a7! machine... anybody knows where the SolidWorks CDs are?.... what, no? .... No, not Office.. I've Office, I need SolidWorks...\n\no.k.   you get the picture"
    author: "Thomas"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-28
    body: "Er, Said company should hire someone competent instead of those 3 \"tech\" people then. Even I, as a 90% linux user, knows this can be handled easily from the administrators workstation. (Granted, you have to actually buy some software to help you do it)"
    author: "Troels Tolstrup"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-24
    body: ">> The installations are usually done via scripted packages\n>> without anyone having to do anybody's desktop.\n\nOK, there are well organized companies and there are badly organized companies.\nThere are well organized IT departments and there are badly orgnaized IT departments.\nThere are well funded IT departments and there are badly funded IT departments.\nThere are professionally educated IT admins (who may be excellent in their job or may suck) and there are self-educated IT admins (who may be excellent in their job or may suck)....\n\nBut yes: he *is* a salesman.\n\nPeople like him may be selling the stuff today which gives you a job next year...."
    author: "Kurt Pfeifle"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-24
    body: "What a load!\nBelieve me when I say that you DO have to stick to these CD installaions with acitivations etc.\nI've worked at three big companies doing MS, and two schools... and it was the same everywhere.\n\nThese \"scripts\" you're talking about aren't even used at such a large organization as SonyEricsson. Neither at Deutsche Bank."
    author: "ealm"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-24
    body: "I don't know about Ericsson. \nBut, they scripts definetely in use at Deutsche Bank.  I know, I am employed by them."
    author: "frizzo"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-24
    body: "I was in the meeting.  These guys are very serious about getting 100% off of Microsoft.  \n\nKevin Carmony\nPresident, Lindows.com, Inc."
    author: "Kevin Carmony"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2003-12-25
    body: "Way cool!\n\nI hope to hear more about this in the future."
    author: "TomL"
  - subject: "Re: New Lindows Coup Looming?"
    date: 2005-11-10
    body: "Barry-Wehmiller International Resources (BWIR) is part of the consulting group of Barry-Wehmiller Companies, Inc. a diversified company founded in 1885. With headquarters in St.Louis, Missouri, BWIR offers the combined benefits of the proximity and dependability of a US company with the affordability of distributed operations.  \n\nBWIR has focused on SolidWorks for nearly a decade. We are a SolidWorks Manufacturing Network Partner and a SolidWorks Research Associate. Our\nengineering professionals have logged thousands of hours of experience using all modules of SolidWorks.  Few consulting companies can match our number of\nCertified SolidWorks Professionals. Barry-Wehmiller Companies, Inc. is a major user of SolidWorks with over 250 seats.  \n\nDuring the last few years we have been offering our services to many companies in the US and the UK. Some of the areas that we offer services in SolidWorks include the following: \n\n3D concept design and development\nDetailing and creation of manufacturing drawings\nConversion of 2D manual or electronic drawings to 3D solid models\nPart modeling for 3D parts catalogs\nExploded views of assembly models for ease of manufacturing and use in machine manuals\nModeling for FEA analysis and report generation\nPhoto realistic images of products for marketing using PhotoWorks\nKnowledge-based Engineering\n\n \nPlease do contact us if you are interested and we will visit you or call you to take it further.\n\nThank you\n\nRufus Seemon [rufus.seemon@bwir.com]\nBarry-Wehmiller International Resources\n\nFor further Information, Please visit ....  Our Web Site : www.bwir.com \n"
    author: "Rufus Seemon"
  - subject: "Solidworks Requirements"
    date: 2005-11-10
    body: "Barry-Wehmiller International Resources (BWIR) is part of the consulting group of Barry-Wehmiller Companies, Inc. a diversified company founded in 1885. With headquarters in St.Louis, Missouri, BWIR offers the combined benefits of the proximity and dependability of a US company with the affordability of distributed operations.  \n\nBWIR has focused on SolidWorks for nearly a decade. We are a SolidWorks Manufacturing Network Partner and a SolidWorks Research Associate. Our\nengineering professionals have logged thousands of hours of experience using all modules of SolidWorks.  Few consulting companies can match our number of\nCertified SolidWorks Professionals. Barry-Wehmiller Companies, Inc. is a major user of SolidWorks with over 250 seats.  \n\nDuring the last few years we have been offering our services to many companies in the US and the UK. Some of the areas that we offer services in SolidWorks include the following: \n\n3D concept design and development\nDetailing and creation of manufacturing drawings\nConversion of 2D manual or electronic drawings to 3D solid models\nPart modeling for 3D parts catalogs\nExploded views of assembly models for ease of manufacturing and use in machine manuals\nModeling for FEA analysis and report generation\nPhoto realistic images of products for marketing using PhotoWorks\nKnowledge-based Engineering\n\n \nPlease do contact us if you are interested and we will visit you or call you to take it further.\n\nThank you\n\nRufus Seemon [rufus.seemon@bwir.com]\nBarry-Wehmiller International Resources\n\nFor further Information, Please visit ....  Our Web Site : www.bwir.com \n"
    author: "Rufus Seemon"
  - subject: "Cool interview!"
    date: 2003-12-24
    body: "I throughly enjoyed it, and also lindows 4.5 is the most polished distribution I've used so far. I will see how it stacks up to Xandros Desktop 2 also when I get it in a few days."
    author: "Alex"
  - subject: "How to overcome the integration aspect."
    date: 2003-12-24
    body: "\"We would like to use exclusively KDE applications because that would give us the most consistent interface. For example File-Open dialogs in OpenOffice are different with the one found in Mozilla or the standard KDE \"File-Open\" dialog. We need consistency! We need integration!\"\n\nFor the file-open dialogs isnt there some standards organisations or isn't osdl creating some standards with linux on the desktop. What someone can do is introduce something like a library for all open dialogs, print dialogs and save dialogs. This library would find out which desktop your in and open the dialog needed microsoft has something like this so all the open dialogs seem the same there with the system but linux has several desktop environments so we need something like this to create an integrated feel for all of them so the developers of applications like Mozilla can call that library which on all linux distros would be in the same location and you can even have it so if that particular distro doesn't have it you can include it in the install(although thats another thing linux needs to work on).  All the library would have to do is figure out which desktop it is currently working on and load up that particular dialog if the desktop you are currently working on is unknown it would bring up a dialog built for that system(although you could have a directory structure built that may prevent this i.e. you get a string that says which desktop this is and you call another library (in a specific location with the desktops name appended to the end of it) made to handle this with certain standards in place.) If you think im wrong tell me why."
    author: "Chris Weilacker"
  - subject: "THe Lindows name works"
    date: 2003-12-24
    body: "In a way. About a year ago I was travelling by train from Deventer to Amsterdam when the man in the seat next to me suddenly bent over, and looked closely at the screen of my laptop. He asked me what it was I was running, and I answered, \"Linux\", of course. He then started talking about how he'd like to switch his companies product to Lindows, because that \"was Windows but not from Microsoft, based on that new-fangled Lunix\", and whether I knew if the VB-based app his company sold would run on Lindows. I _tried_ to tell him that if it would, it wouldn't be anything Lindows specific, but the message didn't appear to get through."
    author: "Boudewijn Rempt"
  - subject: "Re: THe Lindows name works"
    date: 2003-12-24
    body: "Correct me but is it possible to get Lindows on the dutch market? I don't think so, well we saw this dutch MS-action PR, but I don't believe they sell products on that market. I have never seen a dutch Lindows PC and in Germany it was even difficult to get a test copy of Lindows for the Linux Magazin-- despite of the marketing bubble. I never saw Lindows in a shop."
    author: "Bert"
  - subject: "Re: THe Lindows name works"
    date: 2003-12-24
    body: "Amsterdam/Ohio ???"
    author: "Andre"
  - subject: "Re: THe Lindows name works"
    date: 2003-12-24
    body: "Amsterdam/the Netherlands :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: THe Lindows name works"
    date: 2004-01-13
    body: "It's probably because MS has threatened them with a lawsuit there... :-)\n\n->>>> www.choicepc.com "
    author: "moniz"
  - subject: "Re: THe Lindows name works"
    date: 2003-12-24
    body: "Given that there are retailers being sued for carrying Lindows CD's, I guess it's available in some shops -- I think the vicinity of Enschede was mentioned. Don't think there are Lindows PC's, and I've never seen the CD's myself, but then, I used to be a SuSE user, now slowly migrating to Debian.\n\nAnyway, this makes this conversation on the train even more remarkable: one year ago, the CEO of a Dutch company selling Windows software had already heard of Lindows and was considering using it as a platform for his security software."
    author: "Boudewijn Rempt"
  - subject: "Re: THe Lindows name works"
    date: 2003-12-24
    body: "Enschede, it's close to the border but very difficult to be reached by train. \n\nOS diffusion: Sell Lindows in a coffee shop to win the european market :-)\n\nDebian/Suse, this is what we run here. Lindows seems to be a less attractive solution. however Mr. Robertson, open a Office in Europe, we like KDE :-)"
    author: "Andre"
  - subject: "Re: THe Lindows name works"
    date: 2003-12-25
    body: "This Dutch company sells pre-installed LindowsOS-machines:\nhttp://www.dvcs.nl/"
    author: "rinse"
  - subject: "Re: THe Lindows name works"
    date: 2003-12-24
    body: "Hbasic?\n\nhttp://hbasic.sf.net\nalso with dotgnu bindings, so when VB.NEt will be implemented ...\n\n:-) "
    author: "Andre"
  - subject: "Click-N-Run"
    date: 2003-12-24
    body: "> All code will be published back to the community (except for Click-N-Run).\t\t\n\nOn what GUI toolkit is Click-N-Run based?"
    author: "Anonymous"
  - subject: "Re: Click-N-Run"
    date: 2003-12-24
    body: "Its a qt application."
    author: "Kraig "
  - subject: "KDE 2.1.2 soon will be released!"
    date: 2003-12-24
    body: "Taken from http://people.kde.org/konqi.html:\n\n~ Is there any unreleased/unrevealed stuff in your pipe? (New applications/technologies for developers, new icon sets/themes for artists, ...)\n\nGlad that you ask. KDE 2.1.2 soon will be released!\n\n\n-------\n\nLink rot? No not on KDE Sir."
    author: "ac"
  - subject: "Re: KDE 2.1.2 soon will be released!"
    date: 2003-12-24
    body: "You do have fun with old interviews, yes?"
    author: "Anonymous"
  - subject: "Re: KDE 2.1.2 soon will be released!"
    date: 2003-12-24
    body: "My favourite pice of old interviews:\n\n\"Ga\u00ebl: Will there be a KEmacs?\n\nMatthias: XEmacs is the editor of choice for many of the KDE developers. In converse, some XEmacs-developers also mentioned interest in a tighter KDE integration. So the future regarding KEmacs looks pretty bright.\"\n\nFrom http://www.linux-center.org/articles/9809/interview.html\nSeptember 1998\n\n:)"
    author: "Random"
  - subject: "KDE 3.0.1"
    date: 2003-12-24
    body: "> We are very happy with the rate of speed at which KDE develops.\n\nAnd I'm not happy with the rate Lindows integrates newer KDE versions. Even the newest Lindows 4.5 seems to be still based on KDE 3.0.1."
    author: "Anonymous"
  - subject: "Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-24
    body: "Total Community Forum Posts\n\nLindows.com - 153,242 posts\nLycoris - 67,144 posts\nMandrake - 45,452 posts\nXandros - 20,179 posts "
    author: "Kraig "
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-24
    body: "Robertson's paid trolls talk to customers in spe. :-)\n\nWell, if Robertson wanted to enter the European market I would apply for a job :-) \nWe could start a campaign that suits our market.\n"
    author: "Andre"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "\"Robertson's paid trolls\"\nThats a strong accusation Mr. Andre any evidence or are you just trolling yourself?"
    author: "Kraig "
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "Have you missed his disclaimer?  The \":-)\"?"
    author: "cm"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-24
    body: "Gentoo - 715,825 posts\n\nI love statistics. :-)"
    author: "cloose"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "Did you think that you could get alway with preading a lie so easily? i just stopped by the http://forums.gentoo.org/  and added them up its very easy anyone can do it. The total was 113,786 not the made up number you just posted. Come on dude stick to the truth."
    author: "Kraig "
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "Keep cool, ok?\n\nStop by http://forums.gentoo.org/ again and scroll down. In the \"Who is online\" box, you will read the following:\n\n\"Our users have posted a total of 716201 articles\"\n\nThat's where he got the number from. That's where I got the number from. If you believe that number to be wrong, you are entitled to connecting the webmaster of gentoo.org. But next time, be a little bit more careful before accusing other people of being liars. It might be seen as malicious behavior."
    author: "Eike Hein"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "All you have to do is add up the posts and you get the total of 113,786 not 716,201. "
    author: "Kraig "
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "How, when the \"Desktop Environments\" forum alone holds 103843 posts and \"Installing Gentoo\" forum another 78517? That's already more than the number YOU claim.\n\nAnd even IF you were right, all the guy did was rely on the number given by gentoo.org at the usual place for a phpBB forum. That certainly wouldn't make him a \"liar\". I wouldn't even call gentoo.org a liar. I give people the benefit of the doubt.\n\nBut that has its limits.\n\nYou, my friend, are either blind, or unwilling/incapable to admit an error, or a troll."
    author: "Eike Hein"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "Installing Gentoo\nHaving non-GUI problems with the Installation Guide? If you're still working your way through it, or just need some info before you start your install, this is the place. All other questions go elsewhere.\nModerator Global Moderators \nPosts\n78517"
    author: "Kraig "
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "Kinda confirms what I just said, eh? :)\n\nWell, let's end the argument here."
    author: "Eike Hein"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "\"contacting\", not connecting :-D.\n\nBTW, just added up the numbers in the \"Posts\" column myself and I do get the number the forum software claims below.\n\nAt this point, I'm /fairly/ convinced Kraig's just a troll."
    author: "Eike Hein"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "I'm sorry i went back and readded and i was wrong i mistakenly was adding up topics and not posts. Gentoo IS a very active forum and seems to have a good community. It must be very hard to install with a HUGE number of 78520 on just that topic."
    author: "Kraig "
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "Well, it's certainly targeting the advanced user; the installation is text-based and involves partitioning and configuring some things manually. However, it's actually a pretty well documented process - and once it's installed, it's very easy to maintain, update and optimize. Personally, I think it's an awesome platform to run KDE on and get the most out of it.\n\nLooks like many others think so, too.\n\nAside from the top-of-the-line package management, Gentoo is a fairly \"pure\" Linux. That makes the Gentoo forums a very good place to get answers even for mostly generic Linux questions. Very often, I find myself doing a search there before asking Google / Google Groups. \n\n"
    author: "Eike Hein"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "> It must be very hard to install with a HUGE number of 78520 on just that topic.\n\nYou were doing good until this line. That's only a fair assumption if a number of factors align. A more fair assertion is that it's hugely popular because what we know for sure is that in the end that will be the deciding factor and people often post before searching. As was pointed out it's not geared toward newbies as it is not a slick push the button install. It offers you the opportunity to compile everything or use a compiled base but you decide what resides on your system and do the basic configuration. This means following instructions and that means errors and questions. I routinely have people contact me with questions about Quanta that tell me they are newbies and running gentoo. I ususlly shake my head as if I just bit a lemon and try to fit those two concepts together before telling them they are not such a newbie if they installed Gentoo.\n\nTypically Gentoo is popular with developers like myself because it's easy to keep updated. However my favorite thing is that I installed it once and that is the end of installing! Updates really work because we don't create a frozen in time hodge podge of RPMs that will break once we start reshuffling the libraries. That is done to sell more CDs. (I don't even use or like RPMs.) Note that on Gentoo it is possible to create a login with options to run KDE versions 2.2, 3.0, 3.1 and CVS while commercial distros can't offer any version compatibility but can seem to add half a gigabyte of stuff you never wanted and several security risk daemons."
    author: "Eric Laffoon"
  - subject: "Re: Stats Show The Lindows.com Community Rocks!"
    date: 2003-12-25
    body: "As you said, Gentoo is not for Newbies. RPms and deb.s for the rest of us.\n\nNot everybody has the leisure to build from sources. Creating rpms is difficult, too, and badly documented. In my point of view automatized compile farms have to be in place. Or: How do i create a Suse rpm with RedHat?"
    author: "Bert"
  - subject: "Easy RPMs 99% of the time"
    date: 2003-12-25
    body: "Just use checkinstall.\n\nI routinely use it for small packages I want to be able to uninstall someday.\nI say 99%, but I haven\u00b4t seen it fail yet on me."
    author: "Roberto Alsina"
---
You might know <a href="http://www.lindows.com/">Lindows</a> as the company behind an aggressively marketed OS based on Linux and KDE.  Indeed, LindowsOS is shipped on certain <A href="http://www.walmart.com/">Wal-Mart</a> PC offerings and the company has won <A href="http://www.newsalert.com/bin/story?StoryId=Cp7XkubWbmZi0Cdu5nZy&Print=1">huge contracts</a> for LindowsOS machine deployments.  You might also know Lindows as a sponsor of the ever popular <a href="http://www.kde-look.org/">KDE-Look.org</a> community site.
Well, Lindows was in the Netherlands on Dec 8th to <A href="http://www.xs4all.nl/~leintje/fotoalbum/lindows-interview/IMG_1677.JPG">show its support</A> in a situation where <A href="http://www.microsoft.com/">a certain large software powerhouse</A> has apparently been <A href="http://www.newsforge.com/os/03/12/01/1845215.shtml">attempting to intimidate</A> local <A href="http://www.xs4all.nl/~leintje/fotoalbum/lindows-interview/IMG_1690.JPG">LindowsOS resellers</A>.  Sure enough, KDE Dot News took the chance <A href="http://www.xs4all.nl/~leintje/fotoalbum/lindows-interview/IMG_1697.JPG">to meet Michael Robertson</A> for
an <A href="http://www.xs4all.nl/~leintje/stuff/MichaelandFab.JPG">interview</A> and chat about <A href="http://www.xs4all.nl/~leintje/fotoalbum/lindows-interview/IMG_1696.JPG">KDE and Lindows</A>. Wait a minute.  Was that <A href="http://people.kde.org/konqi.html">Konqi</A> in one of those <A href="http://www.xs4all.nl/~leintje/fotoalbum/lindows-interview/IMG_1699.JPG">pictures</A>?
<!--break-->
<p><hr>
<h2>KDE Dot News Interviews Lindows CEO Michael Robertson</h2>

<p><strong>What role does KDE play for Lindows?</strong></p>

   <p>How is a 60-person company going to compete? We need allies. There is a very low cost
   development team out there who are helping us. We need KDE to be successful because we are
   very dependant on that project.</p>


<p><strong>What were your reasons for choosing KDE over other available desktops?</strong></p>

   <p>It has a more familiar UI than GNOME. It is more comfortable for a Microsoft user.
   We are very happy with the rate of speed at which KDE develops. I admire the KDE team.</p>


<p><strong>What do you like most about KDE.</strong></p>

   <p>The pace at which the project is moving. Also its desktop friendly features.
   But most importantly we like the community behind KDE.</p>


 <p><strong>What do you like least about KDE.</strong></p>

   <p>We would like to use exclusively KDE applications because that would give us the most
   consistent interface. For example File-Open dialogs in OpenOffice are different with the
   one found in Mozilla or the standard KDE "File-Open" dialog. We need consistency!
   We need integration!</p>

   <p>Mozilla is a richer experience than Konqueror (at least back
   then when we made some choices about which browser to use in LindowsOS).
   There are always going to be other projects. Does the KDE project replicate
   efforts? KDE does not always have the best application. So we need more integration.</p>

  <p><strong>Could you tell us somewhat more about the work that Lindows has done to
   integrate KDE in their products? Has this been a difficult process?</strong></p>

  <p> Not that difficult, it is pretty easy to make changes. We invested some time during our
   3.0 release to implement changes we could easily adapt to newer future KDE versions.
   So this is very generic code. Our technology has to make it easy to incorporate new
   technology.</p>


 <p><strong>How does Lindows support the KDE community?</strong></p>

   <p>We currently support all our open source partners at <A href="http://www.msfreepc.com/">www.msfreepc.com</A>.
   We support KDE in as many ways as we can. All code will be published back to the
   community (except for Click-N-Run). A lot of changes are not feature changes but more
   or less "default features".</p>

   <p>How are we going to help KDE?  We will look at sponsoring projects on a case by case basis.
   We bring marketing to the KDE community, often overlooked by technical people. By building
   marketing channels, building resellers, this will make KDE stronger. Marketing and business
   development are major obstacles preventing widespread KDE adoption. There are definitely
   no technical shortcomings of the KDE desktop. It is not just about technology but from shifting
   style in the business of software. From
   selling packaged software to selling services. 
</p>
<p>
But what is the impact of KDE on the economics?  How is KDE going to impact 90% of the world who use KDE but don't give back to KDE? KDE will
   impact the business of software.  For desktop linux software to gain wider adoption, it needs support of
OEMs, resellers and retailers. The economics have to make sense for
these companies to invest their time, resources and shelf space. There
must be a way for them to generate profits from it. The problem with
saying "all software is free" is that it removes the economic incentive
for these companies to work with desktop linux and they will not support
it.
</p>
<p>
 LindowsOS is going to move
   the industry. We want to move the business to a service model instead of a package model.
   We are basically selling you the delivery of software instead of the different components.</p>

   <p>We are working directly with motherboard and laptop manufacturers to let the LindowsOS
   work with that hardware. I believe it needs some successful software companies based around
   KDE who are willing to make investments with hardware manufacturers.</p>

   <p>Look at KDE in a broader vision. Verifying software is one thing, but "hardware validation" is the
   key. Once the hardware IS validated you also need retailers.</p>


  <p><strong> LindowsOS recently announced a large deployment in Canada, can you tell
   more about that?</strong></p>

  <p> We have deployed 30.000 desktops. These are so called webstations similar to
  Knoppix, which is very easy in maintenance. They were impressed with LindowsOS
   and decided to go with us.</p>


<p><strong>What kind of users is LindowsOS targeting?</strong></p>

   <p>We target the mass market. By that we not only mean the home user but also small
   to medium sized business. Basically anyone who is cost conscious.</p>


 <p> <strong> How do users switching from Microsoft Windows to LindowsOS perceive
   that switch?</strong></p>

<p>We have hands-on experience. All our staff are using our own distribution (we eat our own dogfood). So when
   they are working with LindowsOS we have a good notion of how some former MS users perceive
   LindowsOS.</p>

<p> We have hired several power users who are keeping good contact with the community. These guys
   interface with insiders (like Beta testers) and they get info from these insiders about
   how they perceive LindowsOS.</p>



 <p><strong>Where would you like to see the future of KDE go,
    and what new features would you like to see in future releases?</strong></p>

    <p>We encourage KDE to look at incorporating services in KDE rather than
    standalone applications. We think the future of OS is based around services.
    The lines between operating systems and services will blur. People are using (Internet) services. Those
    services need to be incorporated in the OS. So how does KDE make sure that it
    interacts better than any other desktop environment out there?</p>

<p><strong>Some people have been predicting a Linux breakthrough on the desktop
for some years now. Do you think 2004 is going to be THE year?</strong></p>

<p>I think I have to say "yes" because I run a Linux company. We are missing
    a key thing: "retail distribution"!  When retail distribution happens that will
    be the time things will really take off. I don't know about 2004. I think so yes?</p>



<p><strong>I noticed that the old man Bill featured in
<A href="http://info.lindows.com/LindowsRock/LindowsRock.html">Lindows latest commercial</A>.
  Do you still expect to get a Christmas card from Bill this year?</strong></p>

<p>If I get one, it will have 2 lawsuits stapled to it.</p>