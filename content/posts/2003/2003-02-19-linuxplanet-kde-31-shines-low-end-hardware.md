---
title: "LinuxPlanet: KDE 3.1 Shines on Low End Hardware"
date:    2003-02-19
authors:
  - "Dre"
slug:    linuxplanet-kde-31-shines-low-end-hardware
comments:
  - subject: "This is not old hardware"
    date: 2003-02-19
    body: "I remember when I got my 133Mhz PC, it had 32MB, and that was pretty high end, it was recommended for workstations only! A modern Linux/KDE/OOo setup would crawl on that. Memory is the big problem with modern Linux apps, you start up KDE, Mozilla and OpenOffice on that kind of hardware and you're looking at a painful user-experience"
    author: "Bryan Feeney"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "Yes it is, my PowerCenter 132 was midrange in 1996, and that had 64mb of ram.  Granted the box before that I had was a Quadra 880AV now that was top of the line 10 years ago... 60Mhz with 48mb of ram!\n\nHardware has moved quickly in the last 5 years.  Its amazing to see win2k and KDE run side by side on the same 150Mhz/128mb  box...  Win2k starts up faster than linux, but quickly slows down to a halt, where KDE/Linux takes 4x longer to startup, it runs smoothly once you are going... Really id rather get a cup of coffie while im loggin in, than sit there in frustration waiting for spellcheck to start.\n\nJust my 2c though.\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "> Win2k starts up faster than linux,\n\nNo linux elitism here, but nevertheless i have to say that i have hard time believing this. XP maybe, but not W2k. We've even done some (half) serious benchmarking regarding this between debian, suse, redhat and w2k with their *default* setup. Debian is usually fastest with suse being number two,  but W2k doesn't really shine in bootup times no matter what way you look at it."
    author: "jmk"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "I think you're mistaken on this one dude. Windows 2000 beat my RedHat installation everytime, and win2k has been repeately faster than Windows XP. But like the previous poster said, that's only boot up time. Once started it gradually slows down."
    author: "chillin"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "Actually... Win2K was known for having horrible bootup times.  Even on the fastest systems with lots of tweaking it would take a very long time to load.  \nLuckily, once you got in 2000 you would very rarely need to reboot (certainly less often than my linux machines).  And unlike WinDOS (9x/Me), it would not detiorate the longer the system was up.  Godbless that unix foundation :)\n\nWinXP on the other hand, boots up about 100X faster than any linux, OS X, or win2K install I've ever seen (only winMe could come close.... but we're only talking about real  OS's here)"
    author: "Brandon"
  - subject: "Re: This is not old hardware"
    date: 2003-02-20
    body: "Actually... Win2k has very variable startup times. On my machine it can starts pretty fast (when it does, most of the time it just says \"STOP\"), on my brother-in-law's machine (both 1.8 GHz),  it takes several minutes.\n\n<flame>\nAnyway, I have not seen my Linux crash for more than two years (it was close when my Konqueror claimed 1 GB+ of memory, but it recovered). \nI have crashed WinXP by moving the mouse while loading a DVD (yes, the whole thing went down with a BSOD, not just one application). :-)\nBut your mileage may vary. :-)\n</flame>"
    author: "Ladislav Strojil"
  - subject: "Linux stability"
    date: 2003-02-20
    body: "<i>Anyway, I have not seen my Linux crash for more than two years (it was close when my Konqueror claimed 1 GB+ of memory, but it recovered). \nI have crashed WinXP by moving the mouse while loading a DVD (yes, the whole thing went down with a BSOD, not just one application). :-)\nBut your mileage may vary. :-)</i>\n\nI ran a Linux desktop for over 4 years.  The GUI crashed *all the time*.  X would freeze or die, or some process (zombie Netscape 4 processes usually) would slow the machine down so much I couldn't even ssh in to kill it.\n\nMy last attempt was with Mandrake 9.  It ran for a while but eventually something caused X to fail on startup.  So I said screw it and got a 2GHz WinXP box - my first Windows machine ever.  It's a POS too (slowdowns and 2-minute freezes) but is slightly more tolerable.\n\nI'm as much of a Linux fanboy as the next person.  The kernel and shell interface are as stable as a very hard rock, and although the GUI has made remarkable progress over the past few years, it still has some way to go before it can be considered more reliable than Windows.  Not dissing, just honestly relaying my experience with it."
    author: "phutureboy"
  - subject: "Re: Linux stability"
    date: 2003-02-20
    body: "Linux stability experiences certainly vary. With NVidias X driver, my machine crashed every day. I changed to XFree86 NVidia driver, and the machine never crashes. I guess that's one consequence of the free OS's configurability - you can configure it to be secure or unsecure, stable or unstable. It's up to you."
    author: "ac"
  - subject: "Re: Linux stability"
    date: 2003-02-24
    body: "If you're experiencing repeated crashes and unstabilities, usually your hardware is dying or almost dead. I would bet that that's the case with your linux machine."
    author: "Kuba"
  - subject: "Re: Linux stability"
    date: 2006-03-15
    body: "Yes, I have recently experienced that unfortunate problem.\nI was running Slackware 10.2 with KDE 3.5.1 on my Pentium III 1 GHz box\nwith 256MB RAM, after experiencing trouble with Windows freezing more and more often.  The first couple days were good, but then I started experiencing the same trouble with Linux.. I went through reinstalls, formats, different OS's, many installs of Slack 10.2, Windows 2000, and Windows NT 4, and it just kept getting worse and worse.. Finally I began troubleshooting my box, I was in denial about the hardware. I pulled RAM, gfx card, net card, wifi card, switched the processor with the old Celeron 800MHz that used to inhabit it, nothing worked.  Only explanation is the motherboard >:|  Now I'm running Slack 10.2 on my old AMD-K62 350MHz box (also with 256MB RAM) and yeah, KDE is slow as hell.. not as far as the interface goes, but if I try to play a video, more often than not, the video app crashes. (Noatun won't even open..it'll open and just stop.)  I know this has nothing to do with the OS, it's just that the box is a POS. but it isn't crashing. I've had it set up for two days now, uptime is 2 days, 2:57. And this is running KDE, multiple instances of Firefox, Xchat, Apache, Opera, Konqueror, video apps, XMMS, mpg123, multiple instances of KEdit, and others, mostly just testing the stability, plus running my webserver of course.  \n\nThe conclusion here is, don't knock it until you've tried it on more than one machine.  Like Kuba said, if it's crashing, it's probably your hardware. And if it's slow, well, it's probably your outdated hardware.\n\nI know one thing for certain though.. I've had this AMD since 1998, when I paid $1500 for it, it's been moved multiple times, banged around, dusty as hell, had components removed, replaced, removed again, and replaced again, and it's still running with no problems....Comparing this 8 year old box with the 4 year old PIII I had, which died for no apparent reason, it hadn't been abused in any way NEAR what this box has been, if at all, I'll never buy another Intel product again."
    author: "TRyP"
  - subject: "Re: This is not old hardware"
    date: 2003-02-20
    body: "Linux needing rebooting a lot more than Windows ? That's odd man. One of Linux's major selling points is the lack of need for a reboot. But one thing that is very true: it depends on your hardware - regardlesss of what OS you're running. I don't know why, but NVidia gives Linux hell. However, as the other poster said, some newer hardware makes Windows 2000 boot up slow as hell also. There have also been numerous reports of XP crashes. It really all just depends. Someone who does a lot of work with pc hardware will quickly tell you that some brands are just more problematic than others vender parts. Just comes with the territory. Personally, I have an Intel PIII 550mhz running on an Intel i810 motherboard, and I've never had an ounce of trouble. The integrated Intel graphics card never gave me any problem on Windows 2000 or Linux. The soundcard however (ESS 19xx, maestro 2, Allegro, or whatever the hell else it is known by) is nothing but a pain in the ass. It gives me problems on both Windows and Linux. As I said before, it really just all depends...\n\np.s. Where did you get this information about Windows 2000 having a 'Unix Foundation' ?"
    author: "chillin"
  - subject: "Re: This is not old hardware"
    date: 2003-02-20
    body: "I have a dual 1.5GHz AMD system dualboot KDE/Linux and winxp. The KDE/Linux starts up way faster, AND stays faster. Winxp dumps a BSOD by just logging out. XP can't finish playing a dvd without crashing. Linux: no problem. I have not had a kernel dump on me since version 2.2.6 on an Apple 8600."
    author: "eze"
  - subject: "Re: This is not old hardware"
    date: 2003-04-01
    body: "ive never seen xp NOT crash. try gaming with it sometime. everyone i play with crashes/hangs constantly. im not arguing one way or another just making an observation. im into web design running a 1.33 athlon, nforce2, ti4200 and 256mb pc2700. if win98se chokes one more time im personally walking over to billys house and farting in his general direction. im looking into a newbie linux os now. any suggestion? looking at mandrake and redhat but am worried about hardware not working. xp is out (for now). too expensive and from what ive seen, too nosey."
    author: "rob"
  - subject: "Re: This is not old hardware"
    date: 2003-10-05
    body: "You aren't capable of configuring your xp system properly (software/hardware). I have uptimes of over 4 weeks without any crash and the system is on heavy load most of the times (gfx, games, net, etc.)\n\n"
    author: "unknown"
  - subject: "Re: This is not old hardware"
    date: 2004-01-25
    body: "4 weeks? we have linux servers at work under serious load that have been up over a year. we have a windows nt 4 machine that's been up atleast 6 months too.. of course its at 0% load most of the time.. but we primarily specialize in linux servers so that's what we use.\ngiven that you have half a clue about the os you're using (how to maintain it, set it up, etc), it can be as stable as you need it to be.\n"
    author: "sm0"
  - subject: "Re: This is not old hardware"
    date: 2006-04-21
    body: "I rarely comment on anything, but I must coment on these falsehoods. I think it is ludicrous to say that \"WinXP on the other hand, boots up about 100X faster than any linux\"  and even funnier to say that \"once you got in 2000 you would very rarely need to reboot (certainly less often than my linux machines\".  In fact I am willing to lay $5000 on the line in a bet that BOTH statements are false. \n\nI have worked with first DOS from 1980 and later Windows from 1990 till now.  I have always been unhappy with the boot speed. Alas, if only I did not have to boot so often with MS operating systems, but it seems the longer a mchine is runing in Windows (any flavor XP, 2000 etc.) the less stable and slower it is. Sooner or later some program kills or locks up the whole system, or machine just slows down to a crawl, requiring a reboot. But with Windows that only solves the problem a limited number of times. As time goes by, all Windows systems just run slower and slower. Tweaks do not help, updates do not help. The only way to speed the machine back up is to reload the operating system from scratch, reinstall, not update. Then it runs fast again.\n\nI have tried Linux since 1996, but never switched to it. I always loaded it along side Windows, a dual boot. After these many years  IN January 2006 I finally got sick of the above scenario with MS Windows, especially the unknown slowdowns when some unknown system driver or device took 100% of the CPU, and after all Linux had come a long way in that time. So I re-formatted my hard drive and loaded Mandriva Linux. Not having the crutch of Windows on my hard drive to fall back to if I had any problem with Linux, I had to learn Linux and use it to do my daily computer work.\n\nI admit it took several days to get comfortable with Linux, but I bit the bullet and learned how to use Linux. After all I had no MS Windows on my hard drive.  I will not say the Linux transition was totally error and problem free, but Linux does work, it works quite well. Now after three months on Linux, with many pleasant surprises along the way, I am very happy. I will never go back to Windows.  It does not matter is XP does boot slightly faster than Linux (not 100X), as I do not have to reboot Linux, practically ever. It just runs and runs with no slowdowns. \n\nAND when it is running it is faster than Windows.  Everthing is faster, my DSL line, for example, would never downlaod faster that about 280KPS with Windows, now it does 400-500KPS regularly with Linux. Especially great under Linux is the use of the internet. I could not believe how smooth Linux handles it. I can use my new web page sukzez.com as if it is on my local drive. I can edit files directly using editors on my local machine. \"I\" am surprisied and happy with my results of swwitching to Linux.\n\nBut before I finish this rather long comment, let me end with this note. Under Windows I had Adaware, Spybot, various spyware programs, Zone Alarm Security, firewall, anti-virus etc. I had to clean up my system practically daily, including updating the literally hundreds of security updates from Microsoft. Before reformatting my hard drive ZoneAlarm showed over 439,000 attempts to breach my secrity. NONE of this with Linux. Oh I have the antivirus and firewall programs installed, but they appear to not be needed as a virus has never been found, and the only activity the firewall ever sees is a ping or two.\n\nMy parting comment is this. Windows works, but it has its problems that require regular maintenance, and it COSTS. Linux works too, and it has some problems too, but fewer than Windows, but it is FREE. I think if anybody would take the time to learn how to use Linux, he (or she) would soon come to be very comfortable with it and NEVER go back to Windows.\n\nOh, and guess what?????? For the first time in YEARS, my system is completely legal!!!!\n"
    author: "George Pitts"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "Why start up mozilla when you have Konqueror?\nThere's one thing to save memory on."
    author: "KDE User"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "\nTo use it's very advanced security token \nplugins what KDE lacks?\n\n"
    author: "NetPig"
  - subject: "Re: This is not old hardware"
    date: 2003-02-20
    body: "\"very advanced security token plugins\"? Please elaborate."
    author: "Datschge"
  - subject: "Re: This is not old hardware"
    date: 2003-02-20
    body: "Online banking and some other sites force me to start Mozilla regularly.\nHow much I like Konq, the real world still is Explorer oriented... and a bit Mozilla. I hope Konq will reach a higher point of compatibility with the Mozilla specs in KDE3.2 "
    author: "Thore"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "My 133MHz had 32 MB too. Maybe a KDE light for older computers ...\nRemoving the multimedia & game stuff. (those old computers usually used as servers and basically needs a simple GUI and good configuration apps)\n\n--\nMasato\n\nDevCounter (http://devcounter.berlios.de/) - An open, free & independent developer pool\ncreated to help developers find other developers, help, testers and new project members"
    author: "Masato"
  - subject: "Re: This is not old hardware"
    date: 2003-02-19
    body: "Simple, just don't install kdemultimedia and kdegames, and you've removed the multimedia and game stuff from KDE!"
    author: "not me"
  - subject: "Re: This is not old hardware"
    date: 2003-02-20
    body: "Does the bindings to those categories also disappear, so KDE hasn't running so much at once ??\n\n--\nMasato\n\nDevCounter (http://devcounter.berlios.de/) - An open, free & independent developer pool\ncreated to help developers find other developers, help, testers and new project members"
    author: "Masato"
  - subject: "Re: This is not old hardware"
    date: 2003-02-20
    body: "Yes it is old hardware. It's has just been upgraded with more RAM. It's cheap to add RAM in the machine, so why not do it? Just because it had 32megs several years ago, means that it must have 32megs 'till the end of time? And besides, it was possible to use 128megs on this machine back when it was released.\n\nThe point that \"in year such and such these machines only had 32megs of RAM, so this is not a valid test!\", is irrelevant, since you weren't using KDE3.1 in that year. This is 2003, and it's possible to increase the amount of RAM."
    author: "Janne"
  - subject: "Re: This is not old hardware: but people upgrade "
    date: 2003-02-21
    body: "lots of people had machines like that w/ 32 mb \noriginal memory.\nOne of the first things they did was get some more\nmemory, so actually it _is_ representative of \"older\"\nhardware setup."
    author: "kannister"
  - subject: "Testing"
    date: 2003-02-19
    body: "I know I do all of my testing on a 100Mhz PowerMac 7200 and I found the biggest kicker is RAM.   If I have only 48-64 MB of ram in the box things grind to a halt and kswapd takes over the systems load.  Once you get over 64 things get better, but again things are tolerable until RAM gets short... \n\nThe box only has 96MB of ram in it, but that is enough to use konqi, and koffice.  I can even play mp3s but they will skip when i do something wacky like start up 2 konqis at once.  Also large session restores will kill login time.  I can log in to the box in about 45 seconds without a session restore, kwrited and xmlrpc turned off and most kded services disabled.  I can do it in about 15 with only KWin and kded, so really most of that startup is arts and kicker with kicker probibly being the hog there.\n\nThe cool thing is that from KDE 2.x -> 3.x its gotten faster and more so on low end HW.  I think if we could reduce the RAM usage we could go even further but that will take more of the same auditing that we have been doing in KDE 3.x series.\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "*sniff*"
    date: 2003-02-19
    body: "I remember when I couldn't listen to mp3s and browse the internet at the same time. ahhhhh. "
    author: "Caoilte"
  - subject: "Runs good on new hardware too ;)"
    date: 2003-02-19
    body: "I just upgraded from a Celeron 466 with 256MB to a dual athlon 2200+ with 512MB.  I can report that KDE3.1 screams on the new hardware as well ;)"
    author: "ac"
  - subject: "A lot of people disagree"
    date: 2003-02-19
    body: "See the comments at: http://osnews.com/comment.php?news_id=2841\nStill waaay too many people believe KDE is nowhere near usable.\n\"Konqueror crashes all the time.\"\n\"KDE is bloated.\"\netc. etc."
    author: "Anon"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "Yeah that is the cool thing about trolling, you dont have to know what you are talking about...  One would think that if it was bloated and crashed all the time that the developers of KDE would have to be masochistic morons to use it, or we are all rich weirdos with 3Ghz Athlons and 2Gigs of ram...  Seeing as (speaking for myself here) this isnt the case in either respect im thinking that they are trolling ;)\n\nNo KDE is not perfect, but you know what?  I payed for Win2k and MSVC, and not only will MS not support me when I find bugs in their compiler or OS, but they tell me to deal with it...   and yes virginia win2k does crash IE on my box far more than Konqi goes down... config issue? maby, hardware issue, doubt it, kde works just fine...  either way im thinking there are are more trolls than useful comments anyway...  With KDE i got far more than I payed for already...  \n\njust my 2c\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "You can see the host/nickname of whoever posts there.  There are just one or two dedicated trolls there including those who haven't even used or tried KDE *3.1*.  "
    author: "ac"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-20
    body: "I am only stating that the people who posted comments on that link disagree. I didn't say anything about *my* opinion. If you must mark people down as trolls, mark them down. I didn't give an opinion."
    author: "Stof"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "> \"Konqueror crashes all the time.\"\n\nI have some doubt on their KDE deployment... :-)\nMy konqi works very well...\n\n> \"KDE is bloated.\"\n\nFor this point, I think their almost right.\nI use KDE as my only desktop environment for some times now, and I follow\nits development since its first beta.\n\nI haven't used 3.1 yet, but on an usability aspect I don't think there's a lot\nof differences with 3.0\n\nI must admit that KDE is bloated... there's \"too many\".\nNot too many features, but too many buttons and menu options. In front of \nKonqueror, I think that most Joe Users are lost (hopefully not every Joe User)...\n\nI hope that the usability.kde.org will have more developers, and more issues solved.\n\nIn my opinion (but I'm not release coordinator :-D), KDE 3.2 should be focused more on ease of use, and usability in general, than on features.\n\nP.S: If possible, I will post an article on KDE News, as soon as I've tested 3.1, if it's appropriate.\n\nP.P.S: Sorry for my bad english... needs to be perfected..."
    author: "Er-Vin"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "> I haven't used 3.1 yet, but on an usability aspect I don't think there's a lot\n of differences with 3.0\n \nWell check it out! kcontrol and kicker have become much cleaner. There are massive differences also in many other apps, like kaddressbook.\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "Argh!\n\nWell... I really need DSL...\nBoring to always ask someone else to download for me! :-/\n\nNice to read! I hope it will persist in this way!\n"
    author: "Er-Vin"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "I think an easy improvement of usability would be\nto remove some standard icons from Konqueror.\n\nBut maybe that's SuSE/Mandrake/etc :s problem"
    author: "KDE User"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "> I haven't used 3.1 yet, but on an usability aspect I don't think\n> there's a lot of differences with 3.0\n\n\n- Konqueror has Tabs now\n- Konqueror Windows open much faster\n\nOne of my friends complained about how slow konqueror opens new windows, if you middle click on a link in KDE 3.0. He really was amazed when I upgraded to KDE 3.1. \n"
    author: "Stefan Heimers"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "No-one said you have to install it all.  That's your choice.  All the apps and libraries are cleanly separated in their modules so you can install the portions you like.  However, if we don't provide certain tools, then those who need them, don't get them."
    author: "ac"
  - subject: "Re: A lot of people disagree"
    date: 2003-02-19
    body: "\"too many buttons and menu options\"\n\nThat was one of the reasons why I decided to use KDE instead of Gnome: in Gnome too many options were missing or hidden too damn far away from me to find them. Give me KDE anytime, power to the user! :-)\n"
    author: "Quintesse"
  - subject: "Offtopic question"
    date: 2003-02-19
    body: "Just a question. Is there a default well design media-player\nin KDE now (3.1) ??\n\nThat would be a good skin for noatun I guess.\nLike the windows media player."
    author: "KDE User"
  - subject: "Re: Offtopic question"
    date: 2003-02-19
    body: "For the moment you have the choice between :\nnoatun, kplayer, kmplayer, kxine,.... \n\nI hope noatun could have video enhancement. \nIt has now became my default audio player with the Hayes Playlist"
    author: "Shift"
  - subject: "Re: Offtopic question"
    date: 2003-02-19
    body: "yes, I agree w/ you there. Hayes does rock. I do have problems w/ noatun not restoring when starting KDE. and Hayes is not starting with the last song that was playing when noatun shut down.\n\nWe need to get it added to KDE."
    author: "Echo6"
  - subject: "Re: Offtopic question"
    date: 2003-02-20
    body: "Does kaboodle count? I was positively surprised to see how well it meshes with konqueror in 3.1. (It had been a while since I used KDE)"
    author: "Joeri"
  - subject: "KDE 3.0 on P-166/64MB"
    date: 2003-02-19
    body: "I remember that even KDE 3.0 on P-166/64MB RAM notebook was running pretty fine the start-up time for KDE was about 1.5 minutes. I had SuSE 8.1 instaled there, and it's 1.5GB hdd was formatted for reiserfs. I think that if i had current KDE 3.1 suse packages installed there -- it would be running even faster! \n"
    author: "SHiFT"
  - subject: "KDE on funny hardware"
    date: 2003-02-19
    body: "My main workstation is a PI/133MHz/48MB. It runs KDE 3.1 just fine, but begins swapping like hell as soon as you open some large non-KDE apps, eg. Mozilla. Just having Konqueror, Kmail and Kword open at the same time is no problem.\n\nBefore I had 80MB in that box, I could run as many apps as I liked, but I had to remove part of the memory because it was faulty and caused occasional system crashes.\n\nWell, but now for the real fun: My home server ( http://www.heimers.ch/ ). Its motherboard was one of the more advanced i486 models about eight years ago, already equipped with PCI slots. I put in a 33MHz i486, 32MB of FP-RAM, an UDMA-66 controller and a 7200rpm/40GB IDE disk from IBM.\n\nJust for fun I installed KDE 3.1 on it to see how it copes with it. And it works! I export the display to my desktop, start knode or kmail and wait a few minutes. But when the app finally appears, it is really usable without any problems. If you are patient, you can even start a full KDE desktop on an Xvnc server.\n\nSo my conclusion: KDE needs lots of RAM and creates a lot of disk activity, but CPU usage is not an issue. Qt is well optimized for low cpu usage, but it eats lots of memory. KDE loads many, large  libraries, thus putting heavy load on your hard drive.  \n\nStefan"
    author: "Stefan Heimers"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-19
    body: "Sure, it will trash on the disk somewhat when it's loading, but the main thing that slows KDE (well, anything) down is swapping. Once it's all snug in memory and loaded, it _is_ way fast.\n\nAlso, enabling --enable-final while building and stripping binaries will also help a lot.\n\nI used to be a rabid gnome user, but KDE just rocks my world!"
    author: "coolvibe"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "Stripped binaries and unstripped ones take up the same amount of RAM."
    author: "Tristan Tarrant"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "But unstripped ones take longer to load if your disk is slow."
    author: "Roberto Alsina"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "I doubt it. AFAIK the debugging info is not even touched when loading a binary/library for execution, and therefore stripping doesn't affect anything besides the disk space usage.\n\n"
    author: "L.Lunak"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "Could be. I would expect the drive to have to seek more, but maybe it is not even measurable."
    author: "Roberto Alsina"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "Debug info sections are placed at the end of the file, so unless the file is fragmented (which it shouldn't be after 'make install'), they theoretically really shouldn't affect the execution in any way.\n\n"
    author: "L.Lunak"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "Thanks. Learn something new every day :-)"
    author: "Roberto Alsina"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "Debug info will end up in disk cache. This leaves less cache for real code."
    author: "Stefan Heimers"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-20
    body: "Doubt that, since binaries are largely paged in on demand (which kind of sucks performance wise); and the debug info is in a different section....\n\nThe real difference between debug and release builds is what happens to kdDebug....\n"
    author: "Sad Eagle"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-19
    body: "If you still have the faulty ram, maybe you can use it, at least partly.\n\nhttp://rick.vanrein.org/linux/badram/\n\nI used this on a server with about 100 broken KB on a 256MB module, and it worked just fine (instead of locking up every hour or so ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-19
    body: "Great! I was already hoping for such a solution, but did not find it before. I will try it as soon as I find the time."
    author: "Stefan Heimers"
  - subject: "Re: KDE on funny hardware"
    date: 2003-02-19
    body: "Also check http://www.memtest86.com/ for a memory tester that can give you the proper badram patterns.  It's also a damn fine memory tester, in general."
    author: "Anonymous Coward"
  - subject: "Clicked on OpenOffice Yesterday"
    date: 2003-02-20
    body: "... it's still loading 15 minutes later ..."
    author: "Clicked on OpenOffice Yesterday"
  - subject: "Not true"
    date: 2003-02-20
    body: "| most of the programs, menus, icons and animations seemed to appear almost \n| instantly\n\nNo way. On my old AMD K6-2 300 with 192 MB ram and SCSI disk, it took seconds even for a simple konsole to open.\n\nTHE biggest problem of KDE is slowness. And while it may be largely caused by gcc, that doesn't matter to the end user."
    author: "Erik Hensema"
  - subject: "Re: Not true"
    date: 2003-02-20
    body: "\"THE biggest problem of KDE is slowness\"\nStartup times. Considering the context I think that's what you were refering to, anyway, I'd just like to stress that. Startup times are what really bothers me, when things are running they run pretty fine."
    author: "Sr Stap Timbes"
  - subject: "Yucky icon theme"
    date: 2003-02-20
    body: "I share the author's dislike of the Crystal icons. In addition to looking pretty, shouldn't icons also be clear? They are not merely decorations, after all. I just installed 3.1 and reverted to the classic icons. The new ones are more abstract, fuzzier, and have too much blue among them. It's not as easy to distinguish their meaning: many of them look like bluish blobs. Frankly, I think icons should be more cartoonish: less pretty, but sharper, with more contrast and easier to understand. I hope KDE does not abandon the classic icon theme."
    author: "Chris"
  - subject: "Memory"
    date: 2003-02-21
    body: "It is strange, the memory usage. KDE 3.1 is not that slow in the sense of using the processor, but indeed needs very much memory. I have a P350 with 64 mb ram and enough swap.\nOpening Konsole takes 12 seconds, after clicking \"Settings-Configure Konqueror...\" I have to wait 6 seconds, a new Konqueror window also takes about 3-4 seconds.\nI can remember KDE 1.2, where a new KFM window opened in about 0.5 second...\nBesides that you need at least 128 MB ram to run KDE well, I find it a very complete system. The things I often want to do, is browse the internet, edit my website with FTP and type some text. For that last, I rather use LyX with xdvi (because kdvi can take 50 mb of memory), but for browsing Konqueror is very good, KMail too, kghostview integration in Konqueror is hady for viewing PDF files in webpages, and Kopete is the best IM program for Linux I have ever found. And there is no better website editing tool than KWrite, thanks to VFS support.\nSo after all I think KDE would be better if it used less memory, but I am completely happy with it."
    author: "Daan"
  - subject: "Memory  "
    date: 2003-02-25
    body: "> And there is no better website editing tool than KWrite, thanks to VFS support.\n\nHave you tried quanta yet? ;)\n"
    author: "Lam0r"
  - subject: "Memory  "
    date: 2003-02-25
    body: "> And there is no better website editing tool than KWrite, thanks to VFS support.\n\nHave you tried quanta yet? ;)\n"
    author: "Lam0r"
  - subject: "Most major Linux apps are RAM-hungry"
    date: 2003-02-26
    body: "Whether you run KDE 3.1 or twm, as soon as you fire up Mozilla or OpenOffice on a machine with less than 64MB of RAM, youre going to be sitting there waiting. \n\nKDE can't do anything to reduce the massive RAM requirements of these 3rd party apps, and the KDE developers should be applauded for taking the time and effort to reduce the performance impact of their project on low-end machines.\n\nA lean, mean KDE will give users of low-end hardware more reason to work primarily in the KDE apps (Konq, KWrite, KMail), rather than go for the bloated Mozilla/OpenOffice etc.\n\n\n\n\n"
    author: "Pete Black"
---
In a recent <a href="http://www.linuxplanet.com/linuxplanet/reviews/4676/1/">article</a> featured on <a href="http://www.linuxplanet.com/">LinuxPlanet</a>, senior technology consultant <a href="mailto:rreilly_AT_cfl_dot_rr_dot_com">Rob Reilly</a> ran KDE 3.1 through its paces using a low-end 133MHz PC with 128 MB RAM.  According to the story, despite a number of new features and aesthetic
improvements, KDE 3.1 reverses the general desktop trend of increased
resource usage:  
<em>"Even though KDE took about two and 1/2 minutes to load, most of the programs, menus, icons and animations seemed to appear almost instantly
and ran without a hitch. [. . .] For the average
office or home user, the combination of an older PC and KDE 3.1
would work perfectly well for their needs."</em>
<!--break-->
