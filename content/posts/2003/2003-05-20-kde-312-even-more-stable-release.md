---
title: "KDE 3.1.2: The Even More Stable Release"
date:    2003-05-20
authors:
  - "binner"
slug:    kde-312-even-more-stable-release
comments:
  - subject: "Gentoo"
    date: 2003-05-19
    body: "Or for those of you using Gentoo, just emerge the update... it's alreadly in portage. Nice! :)"
    author: "Apollo Creed"
  - subject: "Re: Gentoo"
    date: 2003-05-19
    body: "Or for those of you using FreeBSD, just cvsup your ports tree and make install!"
    author: "David Johnson"
  - subject: "Re: Gentoo"
    date: 2003-05-19
    body: "Amen to that, my berkeley brother. Binaries are for sissies. :)\n"
    author: "Apollo Creed"
  - subject: "Re: Gentoo"
    date: 2003-05-19
    body: "Nice try, but Gentoo compiles it straight from the source."
    author: "Janne"
  - subject: "Re: Gentoo"
    date: 2003-05-19
    body: "I think Apollo Creed knows this very well :-)"
    author: "cm"
  - subject: "Re: Gentoo"
    date: 2003-05-20
    body: "Apollo Creed died.. Didn't he? :)"
    author: "J\u00f6rgen S"
  - subject: "Re: Gentoo"
    date: 2003-05-20
    body: "Yeah, I was beaten to death by that horrible Clubber Lang. Good thing I'm resurrected as a dot.kde lurker. :)"
    author: "Apollo Creed"
  - subject: "Re: Gentoo"
    date: 2003-05-21
    body: "Ummmm, no. Apollo Creed was killed by Ivan Drago. You can't even remember who killed you ;)?"
    author: "Janne"
  - subject: "Re: Gentoo"
    date: 2003-05-21
    body: "> You can't even remember who killed you ;)?\n\nI image it would be rather tough since the last thing he saw was a big red glove in his face...\n\n:-)\n\n"
    author: "Xanadu"
  - subject: "Re: Gentoo"
    date: 2003-05-19
    body: "Or the very few chosen debian unstable desktop users may\nsimply type the \"apt-get update && apt-get dist-upgrade\" combination\nand get another fine priece of software for 'sid'."
    author: "stupid anon /.-poster"
  - subject: "Re: Gentoo"
    date: 2003-05-20
    body: "Same works for the folks tracking KDE 3.1.x from download.kde.org who run woody.\n\nFor those who don't, add this to /etc/apt/sources.list\ndeb http://download.kde.org/stable/latest/Debian woody main"
    author: "Blue"
  - subject: "Re: Gentoo"
    date: 2003-05-20
    body: "Does it have to download the entire package? It doesn't seem like that great of a change from 3.1.1a, which I downloaded a few days ago (I'm on dialup, so I care not to get 3.1.2). I guess I could patch it manually but ... nah."
    author: "EY"
  - subject: "Re: Gentoo"
    date: 2003-05-23
    body: "Already did! I'm in love with gentoo. :)"
    author: "9th Gate"
  - subject: "Passwords with Konqueror"
    date: 2003-05-19
    body: "Hello,\n\ndoes anybody know when there will be a password manager for konqueror?\nKonqueror is realy great, but I also have to use Mozilla Firebird for managing websites with username/password query.\n\n\nregards,\nOlaf\n"
    author: "Olaf Mueller"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-05-19
    body: "In KDE 3.2."
    author: "Anonymous"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-05-20
    body: "fantastic!\nany chance of a decent popup blocker?"
    author: "anon"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-05-20
    body: "Huh? It already exists. Using the \"Smart Policy\" rocks :)\n(Will block most of the ad-popups while keeping the useful ones)"
    author: "Daniel Molkentin"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-05-20
    body: "I'm not so sure about that.  I have had it block HSBC bank's popup, although that appears to be fixed.  I'd prefer if it was configurable to some degree (like allow all popups from so and so).  I like the cookie manager system better than the current popup manager.\n\nTim Vail"
    author: "Tim Vail"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-05-21
    body: "I must agree on the popup javascript thing, I would much prefer an interface like the cookie\nmanager (ie you can get a dialog where you allow it or not, just for this session or put it on a popup block list).\nhttp://bugs.kde.org/show_bug.cgi?id=30116 seems to be a  wishlist for something like it...\nAlso the http://bugs.kde.org/show_bug.cgi?id=58650 seem to indicate that the\ndeny list for javascript *not just popups* might be out of  whack...\nNote that I think the whole javascript section should be reworked at least I havent got\nit to work as I would expect. I really prefer the Mozilla Browser ie Mozilla Firebird 0.6\nway of doing.\n\nShameless plug:\nPlease vote for this bug in konqueror its been there since 2.2.0\nhttp://bugs.kde.org/show_bug.cgi?id=31121"
    author: "dnm"
  - subject: "Don't worry"
    date: 2003-05-22
    body: "that's nota  plug, its a bug..."
    author: "mario"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-05-22
    body: "In HEAD it's possible to define host-rules for JavaScript popup blocking."
    author: "Anonymous"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-05-28
    body: "And I'd really like to see a similar mechanism for nspluginviewer. Especially Flash is a resource hog like nothing else out there, sometimes you need it but most you don't it would be really cool if you could disable it\n\nI have a script which does that -*cough* killall nspluginviewer *cough*- in the panel but it's hardly elegant"
    author: "l/p: anonymous/e-mail"
  - subject: "Re: Passwords with Konqueror"
    date: 2003-06-15
    body: "I recently compiled the current CVS and it actually has a feature to en-/disable plugins for certain domains. It's not quite as versatile but almost as good =)"
    author: "l/p: anonymous/e-mail"
  - subject: "Slightly offtopic"
    date: 2003-05-19
    body: "It would be great to have integrated file encryption/decryption in KDE. Any project started to be getting this?\n"
    author: "tritone"
  - subject: "Re: Slightly offtopic"
    date: 2003-05-19
    body: "Why don't you have a look at security/encryption in apps.kde.com? There are a number of gpg-based de/encryptors that integrate with konqi."
    author: "Anno"
  - subject: "Re: Slightly offtopic"
    date: 2003-05-19
    body: "I had only looked at freshmeat because I thought most programs were listed  there. At apps.kde most crypto apps seemed abandoned since KDE 2, but I found kgpg that works great. This one should be included in the distribution IMHO.\n"
    author: "tritone"
  - subject: "Re: Slightly offtopic"
    date: 2003-05-19
    body: "kgpg is already in kdeutils CVS for inclusion in KDE 3.2."
    author: "Anonymous"
  - subject: "Re: Slightly offtopic"
    date: 2003-05-19
    body: "Not only started, but in a fully usable state:  KGPG."
    author: "Haakon Nilsen"
  - subject: "Re: Slightly offtopic"
    date: 2003-05-20
    body: "kgpg is in cvs (and is a great app!)\n\nI hope kwallet is also done for 3.2. I think it is an application that many users have been waiting for a long time. Hope it gets done!"
    author: "whee"
  - subject: "khtml changes ?"
    date: 2003-05-19
    body: "\nin the changelog I see no mention about khtml...\n\nWhat is it's state in BRANCH, I mean regarding safari?"
    author: "yg"
  - subject: "Re: khtml changes ?"
    date: 2003-05-19
    body: "Only safe backports."
    author: "Anonymous"
  - subject: "Re: khtml changes ?"
    date: 2003-05-19
    body: "The safari stuff hasn't been backported to 3.1.*\n\nActually, not alot of has gone in other than bug fixes. \n http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/khtml/html/?sortby=date&only_with_tag=KDE_3_1_BRANCH\n\nor for those using cvs  webcvs:kdelibs/khtml\n\nAnother reason to wait for 3.2.\n\nI'd suggest using cvs if you really want it. Stability is reasonable for a development tree. I haven't lost anything yet.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: khtml changes ?"
    date: 2003-05-19
    body: "till next year... \n\nHave you seen the posts in kde-devel list about 3.2?\n\nAt least, we have the CVS.\n\nBR"
    author: "Francisco Gimeno"
  - subject: "Menu Editor?  Executable names in About?"
    date: 2003-05-19
    body: "I've recently installed the SuSE 8.2 distribution featuring KDE 3.1.1.  Of course I had need to access the menu editor to get everything laid out the way I would like.  It has crashed probably a minimum of twice every time I've brought it up.  Maybe I should open up the code and take a look?  It doesn't strike me that this should be that elaborate a piece of code.  Pretty much as complex as a bookmark editor in a browser and there are plenty of those that work.  Any chance any work is planned for this elemental piece of interface configuration software or am I using the wrong utility?  It is the only option I get when right clicking the \"Start Applications\" button on the task bar.\n\nCouple other things.  It would be nice when looking at the \"About\" box in an application if, besides the pronounceable name of the application, the actual executable command was given.  Is the menu editor kmedit, kmenuedit, kmenueditor or what?  Just an example and probably not the hardest to figure out, but it is nice to know the name of the file you are executing.  This comment is certainly not restricted to KDE applications.\n\nFinally, it would be nice on the download page to see a text file describing what is necessary to download when.  I'm using a dial-up and even though I eventually want to get all of the new KDE, I'll probably only pull a few pieces at a time.  Since I want my system to keep working as well as to utilize the new pieces, it would be nice to get everything I need for a working feature in one connection.  (If such a file is already there, I apologize.)\n\nThanks for all the great work,\n\nCraig Daymon"
    author: "Craig Daymon"
  - subject: "Re: Menu Editor?  Executable names in About?"
    date: 2003-05-19
    body: "> Maybe I should open up the code and take a look?\n\nGood idea. But menu editor in HEAD got already some love during implementation of vFolder support.\n\n>  Finally, it would be nice on the download page to see a text file describing what is necessary to download when.\n\nhttp://developer.kde.org/source/"
    author: "Anonymous"
  - subject: "Re: Menu Editor?  Executable names in About?"
    date: 2003-05-20
    body: "Perhaps there was a problem with the packaging. I've never had a problem with the Menu Editor (KDE 3.1.2 on Debian 3.0 here), and my menus have the app name and generic name, e.g.\n\nKCron (Task Scheduler)\nCervisia (CVS Frontend)\nKate (Text Editor)\n\netc etc.\n\nI was never happy with RPM-based distros, perhaps it's time you took Debian for a spin. Check out Knoppix (www.knoppix.org), it's a bootable live CD with KDE based on Debian. Don't use it myself, but I've heard good things about it."
    author: "Blue"
  - subject: "Re: Menu Editor?  Executable names in About?"
    date: 2003-05-20
    body: "Perhaps there is something wrong with my compiled copy then. I use Gentoo (flags: -march=pentium3 -O3) and I have seen kmenuedit crash on me many times, particularly when using drag n-drop for an entry without an icon. I say so about kde 3.1.1 . "
    author: "Rithvik"
  - subject: "Re: Menu Editor?  Executable names in About?"
    date: 2003-05-20
    body: "That is a very accurate description of the most frequent crash experience I have encountered with the menu editor.  I'm not absolutely certain it occurs only when there is no icon associated, but I know that has been the case in some of the crashes.\n\nThe other thing is that moving menu items or deleting them doesn't seem to actually delete them from the original location, only hide them.  This can cause problems and certainly must waste resources.\n\n-Craig Daymon\n"
    author: "Craig Daymon"
  - subject: "Re: Menu Editor?  Executable names in About?"
    date: 2003-05-21
    body: "I don't think it can change the default locations of items in the menu when run non-priviledged (as it usually is). Most program locations are common for all and can probably be manipulated directly only by root (playing around in /usr/kde/3.1/share/applnk , I suppose). So the menu editor doesn't delete them, it overrides (hides) them somehow for the non-priviledged user.\n\nHopefully there is a better solution."
    author: "Rithvik"
  - subject: "Re: Menu Editor?  Executable names in About?"
    date: 2003-05-20
    body: "For having a nice installer which deinstalls XFree4 'cause you wanted a prog with a far dependency to XFree3.3.6? No, thanks. I prefer to have control on what's going on - apt-get-trouble is too much windows-like in its behavior.\n"
    author: "Ruediger Knoerig"
  - subject: "Re: Menu Editor?  Executable names in About?"
    date: 2003-05-20
    body: "that's only for starters... it's not that hard to install something with apt without breaking existing packages. i really like debian (though i myself am using gentoo atm).\n\ntim"
    author: "Tim Adelt"
  - subject: "thanks!"
    date: 2003-05-20
    body: "It fixes a lot fo the bugs that annoy me the most, likee the tab delay for websites that can't be found. Very nice enchancement release, I can't wait for 3.2 when the UI usability will really be improved like GNOME 2.4's ;) I don't mean you have to eliminate any options, just group them mor elogically, currently the context menus in Konqueror for example are a mess, always giving me options I can't use or would not have any need to use in it and many of the options in the control center can be grouped ebtter. I do at the same time think that many of its options are not needed and should be hidden in an advanced mode.\n\n\nBTW: Choice creates stress, funny isn't it. A maid became stressed out when faced witha  choice between 5 detergents instead of 2. hehe.. yet true "
    author: "mario"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "> currently the context menus in Konqueror for example are a mess, always giving me options I can't use or would not have any need to use in it\n\nBy default, there are about 5 options in Konqueror's context menus in CVS... this is pretty good.\n\nHOWEVER, if you install anything past kdebase, you get applications installing a bunch of service menu entries into Konqueror. This is where you start getting 15 menu-long context menus in konqueror. IMHO, an \"actions\" submenu would be quite logical."
    author: "whee"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "coincidentally, a patch puts all the servicemenus into an actions submenu was committed today. huzzah!"
    author: "Aaron J. Seigo"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "i wonder who.... ;-P"
    author: "kidcat"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "> I can't wait for 3.2 when the UI usability will really be improved like GNOME 2.4's ;)\n\nYou should really go back and take a look at KDE 2.0. I recently booted up my celeron-300a for the first time in ages (running slackware 7.0 with KDE 2.0.1.) Well, I was pleasently suprised with the usability of KDE back then. Things generally *felt* a lot less cluttered, perhaps because there were less features. It actually felt somewhat like GNOME 2.2 does. Perhaps in terms of usability/features GNOME 3.x will be like KDE 3.x/GNOME 1.x, and KDE 4.x will be like KDE 2.x and GNOME 2.x :)"
    author: "whee"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "Note that usability can mean two things: \n\nUsability for the average computer user\nUsability for the brand new (or inexperienced) computer users\n\nThe people in the first group are basically your average computer user. They, of course, use MS-Windows(tm), as it has >90% market share. They  are comfortable with Windows and know how to get things done with it.\n\nThe people in the second group will, upon, seeing a Windows desktop, perhaps be daunted by it. This to a point in which they have to call tech support, etc...\n\nCurrently, I think the people in group 1 (most people who are trying out Linux and other major operating systems in which KDE/GNOME runs), will probably be most comfortable with KDE. There are just some paradigms in KDE that are closer to Windows than GNOME is to Windows (Konqueror/Windows Explorer/IE are quite similiar, and for MANY users, would be their most used application.)\n\nCurrently, I think that the people in group 2 will be most happy with gnome2. In most instances, it is just simplier than KDE is. If KDE had something like a kids, and/or an easy mode, it would satisfy these users. \n\nThe third group, and not mentioned thus far, however, is the power user. This is the average user of Linux or most non-Windows/Mac operating systems. For power users, raw features, and configurability win, and so does KDE. I think the single biggest thing in terms of usability for power users is the ability to morph the underlying system to how the user works. I think KDE, with a myriad of options, therefore, offers the best usability for power users."
    author: "fault"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "> Currently, I think that the people in group 2 will be most happy \n> with gnome2. In most instances, it is just simplier than KDE is. \n> If KDE had something like a kids, and/or an easy mode, it would \n\nMe runs for cover. Easy / meduim /advanced mode no no no no ;-) As it seems, this is a bad route to take. Remember Nautilus (ok, it's kde people here but anyway)? It turned out that most, if not all, users switched to advanced mode and it was scraped. Sensable defaults, good and logical grouping options with clear names that explaines what they do feels a whole lot sensable to me. But that's just me :)\n\nRegards,\n\nJ\u00f6rgen"
    author: "J\u00f6rgen"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "I agree... having seperate user level modes is not the way to go. An environment that is tailored for striking a balance between the three is much better. I was not trying to advocate user modes in either case (although it may seem that I was :))"
    author: "fault"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: ">> I think KDE, with a myriad of options, therefore, offers the best usability for power users.\n\nThe current crowded mess of options in all kinds of weird, horizontally stretched and covering a large part of the screen, visually unpleasant looking dialogs (which cannot be themed away with nice themes and icons) that can only be found by spending more than half an hour of expensive time is not what I call for 'power' users and is definitely no good for usability.\n\n(that's one reason why I'm switching on and off between KDE and GNOME, the latter has a way better HIG - is that OpenHCI effort still alive btw? the mailing lists seem to be dead.. :/ )\n"
    author: "nac"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "the openHCI effort will likely continue on in a while. i'm currently overly busy catching up on a backlog of TODOs, and the others involved seem equally busy ATM.\n\ni agree that there are many places in the KDE UI that are suboptimally laid out and designed. those areas should be fixed, and that takes time to do it right. this has little to do with KDE's UI guidelines, however."
    author: "Aaron J. Seigo"
  - subject: "\"Power users\" and configurability"
    date: 2003-05-24
    body: "\"The third group, and not mentioned thus far, however, is the power user. This is the average user of Linux or most non-Windows/Mac operating systems. For power users, raw features, and configurability win [..] I think the single biggest thing in terms of usability for power users is the ability to morph the underlying system to how the user works.\"\n\nThis is not really true. There is a large group of experienced \"expert\" users (I couldn't tell how large or what is the majority) which would prefer to learn a well designed, intuitive and efficient interface instead of trying to make the interface behave as they are used to.\nSome people claim that interfaces are entirely a subjective thing, but that's not really true either. Usually when discussing about it, all comes down to three simple things:\na) Convenience and ease of use (if something can be made easy to understand, there is no need to make it complicated just for the sake of it)\nb) Efficiency (for example how many clicks it takes to execute a certain action)\nc) Visual appearance (nobody likes to work with butt ugly interfaces and clean layouts often help finding what you are looking for)\n\nSometimes if you discuss a certain problem you can come up with a solution that is a perfect balance between all three areas where a preference would force you to decide between one or another. \n\nThe only really subjective thing is what you are used to. This is indeed a personal thing but in general, those people who are able and willing to learn something new, are happier people. ;) Of course it's always a good thing if you can solve something in a way that most people are used to, but trying to please everyone in this way isn't possible unless you make everything infinetely configurable and do no interface innovation at all (which could lead to better interfaces for people who can cope with change).\n\nOf course there is one very valid reason for preferences: Different environments or abilities. For example screen resolution, monitor size, disabilities of the user, etc can all require slightly different settings. It is important that those kind of preferences are easy to find and reach and don't get drown between non-essential configuration.\n\nNeedless to say that I'm one of these people I mentioned at the beginning. Today I'm looking for solutions that \"just work\". When I evaluate the quality of an application, I will first try to find out how it's meant to be used instead of trying to make it behave as I'm used to. If I like how it works by default or can find an obvious way in the preferences to make it work better, then I'll stay with it. If I don't, then I will look for alternatives if possible instead of spending a lot of time trying to transform the UI into something that works well.\n\nSomething that just came to my mind is this article StarDock article about their tenth anniversary. They wrote about the original ObjectDesktop product for OS/2 and how OS/2 was mostly an OS full of functionality and flexibility, without a real plan how a user should work with it. So ObjectDesktop stepped in and \"bundled\" all this functionality into something very powerful and efficient (with a bit of extra functionality). It seems that it was extremely popular.\nMaybe what KDE really needs now would be an effort like ObjectDesktop. :)"
    author: "Spark"
  - subject: "Re: \"Power users\" and configurability"
    date: 2003-05-25
    body: "\"a) Convenience and ease of use (if something can be made easy to understand, there is no need to make it complicated just for the sake of it)\"\n\nAgreed, but please define \"easy to understand\" and keep in mind that what's easy to understand for one can be misunderstood by another person.\n\n\"b) Efficiency (for example how many clicks it takes to execute a certain action)\"\n\nAgreed, but this, of course, also needs to be balanced with the importance of the certain action for a particular user. Now we know users use computers for very different purposes...\n\n\"c) Visual appearance (nobody likes to work with butt ugly interfaces and clean layouts often help finding what you are looking for)\"\n\nBeauty is in the eye of the beholder, so the interface's look better be exchangeable (which it is). I agree a clean and especially logical layouts help users finding what they are looking for, but one shouldn't think to know already what users will look for and purposely hide away the rest.\n\n\"Sometimes if you discuss a certain problem you can come up with a solution that is a perfect balance between all three areas where a preference would force you to decide between one or another.\"\n \nThis is what's commonly called a \"sane default\". Preferences should be kept easily accessible for users just in case a particular sane default isn't sane for that particular user.\n\nSo altogether while agreeing with your way of thinking I'm sorry to disagree with your conclusion, I think you mixed up \"just work\" (as in having sane defaults which can be changed if necessary) with \"just doesn't work like I want ideally\" (as in forced defaults you have to live with)."
    author: "Datschge"
  - subject: "Re: \"Power users\" and configurability"
    date: 2003-05-26
    body: "\"Agreed, but please define \"easy to understand\" and keep in mind that what's easy to understand for one can be misunderstood by another person.\"\n\nLabels are one very good example. If something is nicely labeled, you immediately know what it's supposed to do. It should always be possible to quickly learn what something will do after the user learned the basic principles of the particular interface. \n\n\n\"Agreed, but this, of course, also needs to be balanced with the importance of the certain action for a particular user. Now we know users use computers for very different purposes...\"\n\nApplications should serve a certain purpose and the interface should be optimized for this. There is no point in assuming that the user might want to use this media player for something else than watching videos. That's why specialied applications are great and Emacs is such a usability monster. ;)\n\n\n\"Beauty is in the eye of the beholder, so the interface's look better be exchangeable (which it is). I agree a clean and especially logical layouts help users finding what they are looking for\"\n\nYes, I was just talking about layout here. Sometimes it is difficult to find a good balance between a layout that is easy to understand _and_ looks really nice (not clumsy or anything). Surprisingly, the most logical layouts are often also the most visually appealing ones. Maybe not that surprising.\n\n\n\"but one shouldn't think to know already what users will look for and purposely hide away the rest.\"\n\nThat doesn't quite have anything to do with what I wrote. However, you _have_ to make compromises somehow and if you don't even know what your application is mostly used for, then you have bigger problems than usability. ;)\n\n\n\"This is what's commonly called a \"sane default\".\"\n\nNo, a \"perfect balance\" can be more than a default. It often requires code instead of preferences. One example that always comes to my mind is one of Gaim. In earlier versions, it had a preference for alawys showing the border around buttons or not. Disabling this made buttons in the IM window much nicer but looked completely stupid in dialogs (this was the default though). Enabling this, was exactly the opposite. :) Now this option is gone but buttons in the IM window have no border while dialog buttons have. Much better. Now you could still have this preference somewhere with one more option (like \"automatic\") which is enabled by default, but it's not really important anymore. So sometimes eliminating a tradeoff preference _can_ be a win for everyone. \n\n\n\"Preferences should be kept easily accessible for users just in case a particular sane default isn't sane for that particular user.\"\n\nYou are probably getting at the typical \"the developer should not assume to know best\" flame... :/ This is arguable, if you do your usability homework, you should actually be able to cover all use cases. So if you can do this, there is no point in adding more options and thus increasing complexity and inflexibility. I usually find that if I try to change the behaviour of an application via an option that wasn't meant to be actually used, the result is everything but useful. Case in point: I like to have labels below my icons because it makes them larger click targets and is sometimes helpful. But KDE is not meant to be used like this so when I set this option, it looks extremely horrible with Konqueror and it's \"Increase/Decrease Font Sizes\" buttons, they are simply too long. The other option \"Text alongside icons\" is even worse, especially compared to the \"priority text\" which would only show labels besides important icons. So I went with icons only in the end. You could argue that the solution to this would be to improve text below icons (having small labels and larger tooltips would be optimal) and text alongside icons to act like priority text and I would agree with this but my point is, that the developer should think about this _before_ adding preferences. Having preferences that aren't actually meant to be used and fully supported has rarely turned out to be helpful in my experience.\n\n\n\"So altogether while agreeing with your way of thinking I'm sorry to disagree with your conclusion, I think you mixed up \"just work\" (as in having sane defaults which can be changed if necessary) with \"just doesn't work like I want ideally\" (as in forced defaults you have to live with).\"\n\nI don't have to live with anything, if something is dumb, then I can go to a bugzilla or a mailinglist and complain.. err discuss about it. :) This way developers are forced to actually do something usable. And if there is a really good reason to have a certain functionality that isn't useful for the majority of people, then it can still be made optional.\nPeople really tend to get angry about this \"developer knows best\" attitude but in reality, this is extremely exaggerated.\nAs for my \"conclusion\", I don't know what you are refering to, I just stated what _I_ prefer today so you can hardly disagree with that. :)\nI also never said that this can't work with sane defaults which can be changed. If an application works great out of the box and is easy to configure for the important stuff, then I don't mind it having 3000 more options somewhere hidden where they never get into my way. It just has no additional value for me so I don't mind their absence in Software that is often flamed against but works great for me without all those options (you know what I mean)."
    author: "Spark"
  - subject: "Re: \"Power users\" and configurability"
    date: 2003-05-26
    body: "\"Labels are one very good example. If something is nicely labeled, you immediately know what it's supposed to do. It should always be possible to quickly learn what something will do after the user learned the basic principles of the particular interface.\"\n\nI agree, but then again, what is a \"perfect label\"? One that uses a technical term which is specific enough for everyone to look up and get a good description for regarding it use and behavior, or one that describes the result thus making it superficially easier to understand for everyone but not helping at all as soon as someone is actually looking for the specific technical term?\n \n\"Applications should serve a certain purpose and the interface should be optimized for this.\"\n\nSo far I agree.\n\n\"There is no point in assuming that the user might want to use this media player for something else than watching videos.\"\n\nFunnily enough I'm more used to \"media players\" being able to play music, not videos, so your specialization would leave me scratching my head and drop the application completely. Obviously this is no good either. =P\n\n\"Yes, I was just talking about layout here. Sometimes it is difficult to find a good balance between a layout that is easy to understand _and_ looks really nice (not clumsy or anything). Surprisingly, the most logical layouts are often also the most visually appealing ones. Maybe not that surprising.\"\n\nNo, it's no surprising since it's not easy to achieve. I'd even go so far and call a successful balance between a layout that is easy to understand and has a nice look art. Layout improvements are definitelly needed in KDE. Luckily enough for most KDE applications the layout is separated as .ui file easily editable with QT Designer so it should be possible to create a layout-improving-community. Lemme see whether we can realize something like that. =)\n \n\"However, you _have_ to make compromises somehow and if you don't even know what your application is mostly used for, then you have bigger problems than usability. ;)\"\n\nI disagree, people are always creative about using programs since in the very beginning there's no knownledge about how to use a program, and programs should rather serve users' expectations instead forcing them to go the \"one and only true way\".\n\n\"No, a \"perfect balance\" can be more than a default. It often requires code instead of preferences. One example that always comes to my mind is one of Gaim. In earlier versions, it had a preference for alawys showing the border around buttons or not. Disabling this made buttons in the IM window much nicer but looked completely stupid in dialogs (this was the default though). Enabling this, was exactly the opposite. :) Now this option is gone but buttons in the IM window have no border while dialog buttons have. Much better. Now you could still have this preference somewhere with one more option (like \"automatic\") which is enabled by default, but it's not really important anymore. So sometimes eliminating a tradeoff preference _can_ be a win for everyone.\"\n\nBeing mostly KDE only I never used Gaim, and what you just wrote about it let my hair stay on end since it's a very obvious consistency breaker which I'd never agree with. If a particular corner case looks bad then it's either a layout or a style issue, never an application issue. Obviously this has been \"fixed\" in Gaim itself, but wherever else it will appear again it will look broken again. Lack of consitency is the root of all evil.\n\n\"You are probably getting at the typical \"the developer should not assume to know best\" flame... :/ This is arguable, if you do your usability homework, you should actually be able to cover all use cases. So if you can do this, there is no point in adding more options and thus increasing complexity and inflexibility.\"\n\nFunnily enough an increase of options and thus flexibility is often requested by users, ie. those people who are part of your very use case. And there we get back to the \"the developer should not assume to know best\" flame.\n\n\"I usually find that if I try to change the behaviour of an application via an option that wasn't meant to be actually used, the result is everything but useful.\"\n\nIf you don't use something it by far doesn't mean that nobody uses it. And there we get back to the \"the developer should not assume to know best\" flame. =P\n\n\"Case in point: I like to have labels below my icons because it makes them larger click targets and is sometimes helpful. But KDE is not meant to be used like this so when I set this option, it looks extremely horrible with Konqueror and it's \"Increase/Decrease Font Sizes\" buttons, they are simply too long.\"\n\nYou could switch to Chinese or Japanese, I'm sure the texts will be much shorter then. ;) But since you mention that \"Increase/Decrease Font Sizes\" buttons with text are too long, did you consider simply removing those buttons? (I'm assuming you don't really need them, otherwise you'd be happy about the increase of click area. ;) )\n\n\"The other option \"Text alongside icons\" is even worse, especially compared to the \"priority text\" which would only show labels besides important icons.\"\n\nI don't see a \"priority text\" feature in KDE so far, but this sounds like a very useful feature which should be added sometime (fully optional and customizable of course). =)\n\n\"So I went with icons only in the end. You could argue that the solution to this would be to improve text below icons (having small labels and larger tooltips would be optimal) and text alongside icons to act like priority text and I would agree with this\"\n\nGreat, then let's work on that. =)\n\n\"but my point is, that the developer should think about this _before_ adding preferences.\"\n\nAnd there you miss the point: These preferences are globally available in every single KDE application on a library level, developers don't \"think\" about them but they are automatically added. Every KDE application's toolbar is fully customizable. Applications trying to circumvent this and thus breaking consistency are not allowed into the official KDE packages.\n\n\"Having preferences that aren't actually meant to be used and fully supported has rarely turned out to be helpful in my experience.\"\n \n You are (unknowingly?) showing your ignorance if you are serious about \"preferences not meant to be used\", of course they a meant to be used and they do work and thus are most likely actually used. You not liking them by far doesn't make them useless for everyone else. And there we get back to the \"the developer should not assume to know best\" flame.\n\n\"I don't have to live with anything, if something is dumb, then I can go to a bugzilla or a mailinglist and complain.. err discuss about it. :) This way developers are forced to actually do something usable. And if there is a really good reason to have a certain functionality that isn't useful for the majority of people, then it can still be made optional.\"\n\nI see, so you are saying what developers want but users don't becomes optional while what users want but developers don't gets removed? Do you see my very point here? ;)\n\n\"People really tend to get angry about this \"developer knows best\" attitude but in reality, this is extremely exaggerated.\"\n\nFor KDE I have to agree, for what I know and hear about Gnome I tend to disagree (feel free to educate me about Gnome though).\n\n\"I also never said that this can't work with sane defaults which can be changed. If an application works great out of the box and is easy to configure for the important stuff, then I don't mind it having 3000 more options somewhere hidden where they never get into my way.\"\n\nThis is what KDE tries to archieve while pleasing as much people as possible. Everything else is just unnecessarily provoking forkes which can be deathly considering the general lack of people willing to actually contribute in every project.\n\n\"It just has no additional value for me so I don't mind their absence in Software that is often flamed against but works great for me without all those options (you know what I mean).\"\n\nYes, if you don't use particular options then you surely won't mind their absence either, of course. Just bad that imo it's impossible to significantly narrow down the amount of options in general this way since while a user might only use, let's say, 50% of all options, every single user most likely use a different 50% of all options."
    author: "Datschge"
  - subject: "Re: \"Power users\" and configurability"
    date: 2003-05-27
    body: "<i>I disagree, people are always creative about using programs since in the very beginning there's no knownledge about how to use a program, and programs should rather serve users' expectations instead forcing them to go the \"one and only true way\".</i>\n\nThat's like saying there should be five ways to use a toaster. At the end I rather learn how a toaster is meant to be used than use such a monstrosity of toaster. :)\nI don't want to argue about weither is the \"right\" way because this would be rather pointless now (it's the same old flamewar without any new arguments) so I'll keep it with saying that I much prefer to learn how to use an application the way it is meant to be used (which should be a very efficient and convenient way of course) than using an application which allows me to use it five different ways. This is my personal preference so there is nothing you can disagree with. :)\nThere seems to be more and more a split between those who have no problems to adjust to an application while getting simpler and usually more efficient interfaces and those who prefer to adjust the application because they don't want to change their habits. This split has nothing to with \"advanced\" and \"newbie\" users though, people are split in all groups. \nI have yet to find an application which really excells in both areas without compromising one of them.\n\n\n<i>Being mostly KDE only I never used Gaim, and what you just wrote about it let my hair stay on end since it's a very obvious consistency breaker which I'd never agree with.</i>\n\nNo, it's not. Borderless buttons are used for toolbar items, even in KDE. The buttons which have no border in Gaim are actually toolbar buttons, it's just not always that obvious for an IM client. :) So it's pretty consistent now, I just gave this as an example where choosing a sane default for a preference wouldn't cut it because both options are idiotic and it's much better to fix the problem while getting rid of the preference. If the Gaim developers would have thought harder before adding this preference, they might have found the solution earlier. ;)\nMany options in traditional free software projects are added much too careless, because customizability was always regarded as a good thing and an easy way to please the users, without really considering the disadvantages and consequences.\nThat does not mean that all options are bad!\n\n\n<i>But since you mention that \"Increase/Decrease Font Sizes\" buttons with text are too long, did you consider simply removing those buttons? (I'm assuming you don't really need them, otherwise you'd be happy about the increase of click area. ;) )</i>\n\na) They are useful but it looks silly. :)\nb) If I wouldn't have removed loads of buttons already and be on a huge screen (1600x1200), not all buttons would be visible at once thanks to this large labels.\nc) In fact, I tried to remove them but they are part of either the \"<Merge>\" item or one of the two \"ActionList\" items (don't ask me which). When I select one of them, the help text tells me \"You can move it, but if you remove it you won't be able to re-add it\". Thanks. ;)\nAnother downside of the \"one UI fits all\" interface I assume.\n\n\n<i>And there you miss the point: These preferences are globally available in every single KDE application on a library level, developers don't \"think\" about them but they are automatically added. Every KDE application's toolbar is fully customizable. Applications trying to circumvent this and thus breaking consistency are not allowed into the official KDE packages.</i>\n\nI did not miss the point and I'm fully aware of this. I wasn't suggesting that the application developer should have to decide this but that the customization for the toolbar should be limited to a level that makes it possible for application developers to completely and perfectly support (and test!) all its modes. In the end, all I care for is that it works well, which it doesn't.\n\n\n<i>You are (unknowingly?) showing your ignorance if you are serious about \"preferences not meant to be used\", of course they a meant to be used and they do work and thus are most likely actually used.</i>\n\nYou just aren't getting my point. Ok, \"not meant to be used\" was probably not the right wording. Let's say \"not fully supported and tested\" instead. This will just make the application look unprofessional and buggy, without beeing really useful for people like me who would actually want such a feature. I found that many applications which have a lot less customizability, allow me to make a lot more choices in the end because each and every preference has a reason and works reasonable well. Like the GNOME toolbar which has only three options (icons only, text under icons, priority text) which all work really well (aside from the current problems with different GNOME toolbars, which is an implementation problem) while for the generally more advanced KDE toolbar, the only option that is of real use for me(!) is changing the size of the icons which unfortunately doesn't even seem to be available as a general desktop setting (unless it's well hidden, heck the \"toolbar\" settings are well hidden anyway in Appearance & Themes -> Style -> Miscellaneous =)).\n\n\n<i>\"People really tend to get angry about this \"developer knows best\" attitude but in reality, this is extremely exaggerated.\"\n\nFor KDE I have to agree, for what I know and hear about Gnome I tend to disagree (feel free to educate me about Gnome though).</i>\n\nWhat do you want to be educated about? \n\n\n<i>This is what KDE tries to archieve while pleasing as much people as possible.</i>\n\nYes \"tries\". :) I'm not convinced that it's even possible without pissing off quite a few very loud persons. But if it works out, I would be just as happy about it. \nInitially I just entered this discussion to say that not every advanced user prefers endless customization and that's a fact (me is proof). :) I don't doubt that there are advanced users who do prefer endless customization."
    author: "Spark"
  - subject: "Re: \"Power users\" and configurability"
    date: 2003-05-27
    body: "Oops sorry I wanted to preview first but then forgot about it."
    author: "Spark"
  - subject: "Re: \"Power users\" and configurability"
    date: 2003-05-26
    body: "> Applications should serve a certain purpose and the interface should be optimized for this. There is no point in assuming that the user might want to use this media player for something else than watching videos. That's why specialied applications are great and Emacs is such a usability monster. ;)\n\n\nThe line between Applications and only one interface can be quickly blurred, and has already been blurred through embedding technologies. Case in point: Internet/Windows Explorer and Konqueror. This is why \"view profiles\" in both applications exist, and are switched transparently. A new computer user may perhaps not even know they are using the same application when they browse the web and manage their files. This is good. Computers are a tool, and things should be task-based, imho. Whether or not an tool is implemented as a shared library that embeds in a container or an seperate application should probably not even be known by most end users.\n\n> Labels are one very good example. If something is nicely labeled, you immediately know what it's supposed to do. It should always be possible to quickly learn what something will do after the user learned the basic principles of the particular interface.\n\nI fully agree.. I think that toolbars in KDE applications should be paired down and \"text under icon\" should be enabled by default. In usability studies, Short textual representations of actions have long been shown to be far more readily understood than graphical representations, which in turn is more readily understood than longer text. \n\n>  If an application works great out of the box and is easy to configure for the important stuff, \n\nI think a good solution to this would actually be a \"first run wizard\"-type of dialog that can be rerun/resumed. Hiding options is imho, not the optimal solution. Getting rid of useless options is of course, good. \n\nIn the long run, I think something completely new is needed. I think a task-based preference system for ALL apps would be good, in fact. Supposadly, a tasked based mode for kcontrol is in the works for KDE 3.2... anyone know the progress of that?  "
    author: "fault"
  - subject: "Re: \"Power users\" and configurability"
    date: 2003-05-27
    body: "\"I think a good solution to this would actually be a \"first run wizard\"-type of dialog that can be rerun/resumed. Hiding options is imho, not the optimal solution.\"\n\nWizards may have their place but I don't think that customization is one of them. How should you know what you want or need without using the application for a while first. :) And running a wizard each time you want to change a setting (for whatever reason) doesn't strike me as very elegant. Despite that it would be even less direct modification than dialogs without instant apply. "
    author: "Spark"
  - subject: "Re: thanks!"
    date: 2003-05-20
    body: "Hi gnome-troll!\nReally sad for seeing KDE so far better?\n\nLess is more is the message of those which haven't it!\n"
    author: "Ruediger Knoerig"
  - subject: "No one's trolling"
    date: 2003-05-21
    body: "Why are people here, so defensive when someoene even mentions another DE or OS. If I said I liked many of Nautilus's options better than Konqueor's would you jump on me too? This is logic, different programs have different features and therefore Nautilus si bound to have cool things Konqueror doesen't, like the more intuitive side panel, zoom, emblems,shadowed icon text,more logical menus (at least in stable) etc. Konqueror has many things which nautilus doesen't too, it can function as a browser (not sure if this is really such a good diea for usability though), it can split views, it has tabs, it cana cess more protocols, etc.\n\nLEARN FROM EACH OTHER, PRETENDING THE COMPETITION SHOULD BE IGNORED IS A MISTAK!\n\n\nAlso, why is there a need for Kwrite, it seems like a little dumbed down KAte, which is already a verylight text editor. IMO, Kwrite should be REMOVED from KDE unless there is real resistance, there is no point for it, Kate is not a heavy app, Kwrite just confuses people since it sounds a lot like a word proecesor. IE OO Writter, SO writter, definetely does not sound slike a text editor. \n\nBTW: Does KDE have those awesome drawers like GNOME in CVS? That's the only major things I wish KDE's panel had.\n"
    author: "mario"
  - subject: "Re: No one's trolling"
    date: 2003-05-21
    body: "Don't feed the troll. ;)\n\nAs for Gnome alike drawers, no there aren't really any. Kicker is probably KDE's last relict from KDE 1.x days, possibly Slicker (http://slicker.sf.net/) can become a worthy replacement for it sometime."
    author: "Datschge"
  - subject: "Re: No one's trolling"
    date: 2003-05-21
    body: "1. I seriously doubt that he's trying to troll- just he's bringing in lots of things that have been discussed quite a bit already without perhaps reading past discussions on them. \n\n2. kicker isn't from KDE 1.x.. it's from KDE 2.x. kicker is \n\n3. (Going back to grandparent post) What awesome drawers in gnome-cvs are you talking about? I have gnome-cvs installed, but I haven't seen any drawers that have not been available since GNOME 1.0. Yes, 1.0.\n\nIt would of course be handy to have that in KDE, but I'm not sure if anybody ever used it much. It's cool to swallow whole apps in it though and have it as (the equivalent of) a virtual menu."
    author: "fault"
  - subject: "Re: No one's trolling"
    date: 2003-05-21
    body: "> more intuitive side panel, zoom, emblems,shadowed icon text,more logical menus\n\nFor what it's worth, I prefer konq-cvs' side panel and zooming. Emblems and shadowed icon text are cool, and kudos to anyone that implements them in Konqueror, but I consider them minor things personally.\n\nIn terms of logical menus, do you use CVS kde? If so, an actions submenu was supposed to have been commited yesterday. I have a week old CVS copy, but am compiling a fresh copy now :)\n\n> LEARN FROM EACH OTHER, PRETENDING THE COMPETITION SHOULD BE IGNORED IS A MISTAK!\n\nAnd people routinely do. Neither KDE nor GNOME would have been so great if it were not this fact. \n\nKeep in mind that KDE and GNOME developers may not have the exact same priorities as you. Most of them (especially on the KDE side), are purely volunteer-based and work in the free time. Many of them are busy students or have full time jobs. The developers on both projects tend to listen to people, but they may not always agree nor have the time to code it. If code wrote itself, there would already be emblem support in konq. \n\nSo, what I'm trying to get at is: instead of whining incessently at every other article, you should probably start learning C++ and start coding some of the features you want! This is the best way to do it, and fits in well with Open Source development.\n\n> Also, why is there a need for Kwrite, it seems like a little dumbed down KAte, \n\nPlease refer to kde-core-devel mailing lists a few weeks ago, or the subsequent Kernel KC articles about that. No use beating a dead horse. kwrite, kate, and kedit are here to stay for the time being in KDE. Some of them will of course, be shifted around. Kwrite and kate will no longer both be in kdebase, for example.\n\n"
    author: "tilt"
  - subject: "Re: No one's trolling"
    date: 2003-05-21
    body: "> In terms of logical menus, do you use CVS kde? If so, an actions submenu was supposed to have been commited yesterday. \n\nWhich is even inserted if it contains only one item. :-("
    author: "Anonymous"
  - subject: "Re: No one's trolling"
    date: 2003-05-25
    body: "Which is a good idea considering that this keeps the menu layout consitent."
    author: "Datschge"
  - subject: "About Kicker and Slicker"
    date: 2003-05-21
    body: "The only big problems with kicker IMO are:\n\n1. No drawers like in GNOME\n\n2. Not good enough drag and drop support, for example I can't drag the trash to kicker and use it.\n\n3. backgound image is not rotated when Kicker is moved. to another position on the screen, from bottom to side for example.\n\n4. No fake transparancy. (waiting for Xfre eis probably the best option though or all the work will go to waste).\n\n\n5. No shadows. (waiting for Xfre eis probably the best option though or all the work will go to waste).\n\nI'm sure there are a lot of other things which could be made better or included, but this is waht I rally want the most.\n\nI also think CardDesk from Slicker absolutely rocks! I really hope to see this become part of Kicker. I really don't like anything else about Kicker expet CardDesk, but I love CardDesk, (the idea and possibilities, it is not yet fully implemented) Anyway, my ultimate KDE  panel is Kicker + CardDesk. "
    author: "mario"
  - subject: "Re: About Kicker and Slicker"
    date: 2003-05-22
    body: "> 1. No drawers like in GNOME\n\nDoes anyone really use these? Not many people did when this feature existed in gnome-panel in gnome 1.x. This was one of many features silently removed from gnome-panel in gnome 2.0, but silently, and unhereldly reimplented later.\n\n\n> 2. Not good enough drag and drop support, for example I can't drag the trash to kicker and use it.\n\nTrue. Trash- special button would be great addition to kicker.. perhaps you could write it even! submit to kde-devel when done :)\n\n> 3. backgound image is not rotated when Kicker is moved. to another position on the screen, from bottom to side for example.\n\nhmmmm.. i'll have to take a look at this.\n\n> 4. No fake transparancy. (waiting for Xfre eis probably the best option though or all the work will go to waste).\n\nTransparency support is in cvs already.\n\n\n> 5. No shadows. (waiting for Xfre eis probably the best option though or all the work will go to waste).\n\nThere are patches that do this already. Perhaps one of them will be included in cvs in the future. This is even hackier than Window-shadows, btw. "
    author: "tilt"
  - subject: "Re: About Kicker and Slicker"
    date: 2003-05-22
    body: "Thanks for the info, Well I really like the drawers and emblems in GNOME and I would like that to be in KDE. Sorry, about the trash thing, I don't know Qt, I;m jsut starting to learn C++ though.\n\nAbout the shadows and transparancy, I think its a waste of coding, it will get replaced by Xfree's whenever that comes and it sinconsistent. For example menu shadows will only show up in DE apps, not in gnome apps etc. It would be much better to code this into X."
    author: "mario"
  - subject: "Re: About Kicker and Slicker"
    date: 2003-05-23
    body: "> 1. No drawers like in GNOME\n\nI have to agree with the other person who replied - I don't think these are really used all that much and in fact I think there was a discussion about removing them once.\n\n> 2. Not good enough drag and drop support, for example I can't drag the trash to kicker and use it.\n\nThis is kind of a generic problem with KDE and GNOME. You can't drag trash to the gnome panel either\n\n> 4. No fake transparancy.\n\nThis is kind of a cheap hack in GNOME, it's hardly a killer feature. Getting it to show through to applets and stuff is a PITA. Still, KDE has plenty of cheap eyecandy hacks too so this would be good to have also I guess.\n\n> 5. No shadows.\n\nHmm, no panel that I know of does shadows. I think you've been fooled by people who have wallpapers with shadows \"burned\" on :)\n\nFinally, yeah, Slicker looks cool...."
    author: "Mike Hearn"
  - subject: "lost feature, or ..?"
    date: 2003-05-20
    body: "hi kde-fans,\nactually, the feature that the screensaver doesn't stop while asking for\nthe password is gone now. i noticed, there was a bug-report on that thing.., someone had problems because of a slow computer or something like that.\nfor me if there was no 3d acceleration problem, i could login while screensaver\nwas running. i think, that wasn't a bug, it was a feature, and there should be an option in the screensaver-dialog to select wheather screensaver should stop or keep running during unlocking the screen.\nanybody who share the point of view ?\nthanks,\nmononoke\n"
    author: "mononoke"
  - subject: "Re: lost feature, or ..?"
    date: 2003-05-20
    body: "Personally i dont care one bit if it stops or runs while i type the pw... and yet another setting in the giu for this seems like overkill.. \n\nPerhaps a setting in kscreensaverrc tho :P (there should always be a little gold to dig out in rc-files... just so we can look more l33t the the purely click-o-rama users  ;-)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: lost feature, or ..?"
    date: 2003-05-20
    body: "Not yet another option, thanks."
    author: "Chris Howells"
  - subject: "Re: lost feature, or ..?"
    date: 2003-05-20
    body: "then, maybe you better choose M$-O$.."
    author: "mononoke"
  - subject: "Re: lost feature, or ..?"
    date: 2003-05-21
    body: "Just one question: what do you lose when you're entering a password and the screensaver pauses during that time?"
    author: "Datschge"
  - subject: "Re: lost feature, or ..?"
    date: 2003-05-21
    body: "THE MATRIX screensaver stops while entering password, that's uncool!\n\n:)\n"
    author: "mononoke"
  - subject: "WHO CARES!!"
    date: 2003-05-21
    body: "Honestly, who cares if it stops or not when entering a password, it caueses no sability problems or anything, its just  a waste of coding and YAUF (Yet Another Useless Feature). IMO, it also looks beter if the screensaver is moving."
    author: "mario"
  - subject: "Re: WHO CARES!!"
    date: 2003-05-22
    body: "The screensaver pauses while one is typing since on slower computer it can happen that the screensaver sucks so much CPU power that it's nearly impossible to type at all. Please note that KDE is not used only on high end systems.\n\nI'm glad you don't care that it will pause from now on. =P"
    author: "Datschge"
  - subject: "Than why would you put a screensaver?"
    date: 2003-05-23
    body: "If I had a computer that sucked so damb bad I would use the balck screen option or a very light screen saver."
    author: "micahel"
  - subject: "binary update"
    date: 2003-05-20
    body: "by when we will be able to update our existing kde installation in single part ?\n\n"
    author: "somekool"
  - subject: "Re: binary update"
    date: 2003-05-20
    body: "Could you specify \"in single part\" please?\n\n/kidcat\n--\nhardtoreadbecauseitissolong"
    author: "kidcat"
  - subject: "Duke Nuken"
    date: 2003-05-20
    body: "Duke Nukem is under GPL now.\n\nIs there someone thinking about ... KDukeNukem ?\n\n8-D\n\n3d Games will be great at Kdegames !!!"
    author: "No spam, please"
  - subject: "Re: Duke Nuken"
    date: 2003-05-20
    body: "I reaslise you were probably joking, but.... the engine may be Free, but unfortunately the game data files needed for play aren't.\n\nSuch thing could never be included in KDE."
    author: "Chris Howells"
  - subject: "Re: Duke Nuken"
    date: 2003-05-20
    body: "I've been thinking about that many times. The code for Quake is also under GPL and i think there probably is a huge amount of game content available for free from the gaming community. Shouldn't it be possible to make a good free Quake game from the best content? Or perhaps it has been done already? (Or perhaps the textures used are under some proprietary restriction...)"
    author: "will"
  - subject: "konstruct"
    date: 2003-05-20
    body: "Konstruct constantly fails to download anything. Bug in konstruct? Wget? Mirrors?\n\n< cut >\nProxy request sent, awaiting response... 302 Moved Temporarily\nLocation: http://download.kde.org/download.php?url=stable/3.1.2/src/kdebase-3.1.2.tar.bz2 [following]\nhttp://download.kde.org/download.php?url=stable/3.1.2/src/kdebase-3.1.2.tar.bz2: Redirection cycle detected.\nmake[2]: *** [http//download.kde.org/stable/3.1.2/src/kdebase-3.1.2.tar.bz2] Error 1\n</cut>"
    author: "jmk"
  - subject: "Re: konstruct"
    date: 2003-05-20
    body: "If you're behind a proxy you may have to configure wget for this case (env variable or ~/.wgetrc)."
    author: "Anonymous"
  - subject: "Re: konstruct"
    date: 2003-05-20
    body: "If you copy the link http://download.kde.org/download.php?url=stable/3.1.2/src/kdebase-3.1.2.tar.bz2 into konqi you will see that you can select mirroring.. i just copyied my mirror and edit the file category.mk(?) and replaced the MASTERS_SITE setting.\n\n-ce"
    author: "Christoffer"
  - subject: "Re: konstruct"
    date: 2003-05-20
    body: "Thanks. That helped."
    author: "jmk"
  - subject: "more konstruct problems"
    date: 2003-05-21
    body: "I am having problems with konstruct as well. If I try to compile kmail-crypto, the build fails on kdebase:\n\nchecking for rpath... yes\nchecking for KDE... libraries /opt/kde3/lib, headers /opt/kde3/include\nchecking if UIC has KDE plugins available... configure: error: not found - you \nneed to install kdelibs first.\nmake: *** [configure-work/kdebase-3.1.2/configure] Error 1\n\nSo, it complains I should install kdelibs first, but these allready have been installed! Any clue how to fix this?"
    author: "Andr\u00e9 Somers"
  - subject: "Re: more konstruct problems"
    date: 2003-05-22
    body: "you probably have to make qt designer look for plugins in $PREFIX/lib/kde3/plugins, you can do this with qtconfig ...\n\ngreetings,\nfrank\n"
    author: "ik"
  - subject: "Re: more konstruct problems"
    date: 2003-07-15
    body: "Hi,\ntry this:\n1) mv /path/where/is/uic /path/where/is/uic.old\n2) write a little script like this\n   !# /bin/bash\n   /path/where/is/uic -L /path/where/kde/plugins\n3) name this script uic, make it executable and place it near uic.old\n4) try the build again."
    author: "Emiliano Gulmini"
  - subject: "Re: more konstruct problems"
    date: 2005-01-22
    body: "I know the last comment to this was posted ages ago, but just to say Emiliano Gulmini, your a god. Ive been searching for ages trying to fix that UIC plugin problem and that script/post has shown me what was going wrong, and fixed it! Thankyou."
    author: "brk3"
  - subject: "Re: more konstruct problems"
    date: 2005-01-22
    body: "Why not use qtconfig and add the missing library path via GUI?"
    author: "Anonymous"
  - subject: "Re: more konstruct problems"
    date: 2005-01-23
    body: "for some reason that doesnt work.. also that script solution isnt exactly the answer either, it may make it configure, but the make scipt still cant find the plugins. its so annoying, you'd think kde would at least let you know about these things in readme file or something. maybe theres some enviroment variables or something that need to be set but whatever it is its driving me mad"
    author: "brk3"
  - subject: "Re: more konstruct problems"
    date: 2007-03-02
    body: "Emiliano THANKS!"
    author: "Lis"
  - subject: "Re: more konstruct problems"
    date: 2007-03-03
    body: "I found my failure.\nSimply make shure qt was not coonfigured with -static flag."
    author: "Lis"
  - subject: "OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "I know it is off-topic, but I'm slightly enraged right now. Recently I set up an account at www.kde.com, only to find out that this site doesn't offer me anything I need when I'm registered. Thus I wanted to delete my account. I didn't find an option to do so (which would be an usual option), so I wrote an email to the webmaster.\n\nThis only gave me an quite angry reply (from an well-known KDE person) that I shouldn't use the internet if I wouldn't want to leave traces, and that he has better things to do than deleting accounts. OK, this account is no big problem for me, but nevertheless I rather like not to leave my email address at too many places.\n\nI was thus pretty surprised, especially by the way the reply was formulated - I considered my question a perfectly legitimate one. My question: Is this kde.com really promoting official KDE policy?!?!?"
    author: "FLB"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "I fail to see the problem. Whenever I sign up to some website (using a hotmail-account reserved for this purpose) I don't ever see a possibility to later delete my account. kde.com doesn't seem to be an exception."
    author: "Janne"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "OK, OK... but nevertheless I think it's a valid question. As I seemingly can't even change my email right now to something harmless..."
    author: "FLB"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "\"Is this kde.com really promoting official KDE policy?!?!?\"\n\nKDE is the name of a desktop environment.  Someone (KDE e.V. [1]) has a trademark on it.  kde.org appears to be hosted at Trolltech (the people who make the QT toolkit upon which KDE is built).  I believe the people who run this site have a few policies regarding who gets CVS access, and they have a few policies regarding what you can do with CVS access [2].  Other than that, I don't think KDE has much along the lines of official policy.\n\nCertainly KDE developers all have different opinions on how things should be done.  KDE isn't a company where someone takes the reins and says, \"you will be polite or you're fired!\"\n\nIf you're not happy with that, you can set up your own KDE site (you may have to change the name) where you make the rules.  Don't be surprised if noone visits it.\n\n[1] According to the KDE myths page:\n\nhttp://kdemyths.urbanlizard.com/viewMyth.php?mythID=58\n\nKDE e.V. owns KDE copyrights and resources, and even accepts donations in the name of KDE.\n...\nThe membership of this non-profit organization consists of and is controlled by the KDE developers. It exists only to handle the legal necessities of the project, such as handling donations.\n\n[2] CVS commit policies\n\nhttp://developer.kde.org/policies/commitpolicy.html"
    author: "Mike"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "He was talking about www.kde.com. You're referring to www.kde.org. The two are totally seperate as far as I can see. www.kde.com seems to be operated by a company called MieTerra, who I've never heard of. But then if you look at their press release, a familiar sounding name appears!\n\nwww.kde.org is the official site, not www.kde.com, so I would regard anything from www.kde.com as representing only their own policy, not the official KDE line."
    author: "GldnBlls"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "> My question: Is this kde.com really promoting official KDE policy?!?!?\n\nNot at all.\n\n> This only gave me an quite angry reply that I shouldn't use the internet if I wouldn't want to leave traces\n\nI think he's pretty much correct there. "
    author: "fault"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "I agree with the point of leaving traces.  However, I think that if someone asks for their account deleted, they should at least be treated politely.\n\nOne of the fundamental flaw in many people's mind is that people who manage accounts are always thinking about how to preserve the information, they don't take the time to think about when and how to destroy it.  True security composes of both preserving, and destroying information (among other things), so yes there should be a policy about removing accounts.  Failure to have a policy means that you have not at least thought about it carefully, which is really bad.\n\nTim Vail"
    author: "Tim Vail"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "I don't know why most of the replies state that that's usual behavoir.\nEven if it would be, it's definitely not ok. If I'm building a site which gives users the options to leave their data/register, I'll also provide a function to delete their data.\n\nFor me, it's surprising that kde.com does 1) not provide a function to delete one's account, and 2) one of the developers replies in this way.\n\nSure, no open source developer is forced to do what users demand (\"I need function xy, code it for me\"), but though, when building a routine that registers user input in a database, the least I can do, is to also provide a routine which deletes the stuff. Not to speak about my own reasons, like performance, storage costs, etc.\nWhat also surprises me is that this developer even didn't expect someone to ask his registration to be deleted.\n\nSo to sum it up, I fully agree with the original poster."
    author: "Hoek"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-21
    body: "> 2) one of the developers replies in this way.\n\nA KDE developer replied because kde.com is independently maintained by a *surprise* KDE developer. =P\n\nKde.com is mostly known for apps.kde.com which offers a quite complete library of existing KDE and QT applications. There is a link to that site on kde.org with the disclaimer: \"Application feed provided by APPS.KDE.com, a commercial, independent KDE website.\""
    author: "Datschge"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-20
    body: "kde.com is a commercial website that is independent from the KDE project (kde.org).\n\nCheers,\nWaldo\n"
    author: "Waldo Bastian"
  - subject: "Re: OT: What's this KDE.com website?"
    date: 2003-05-21
    body: "Thanks for clearing that up. I thought so too, but, you never know =)\n\nBTW: Waldo, SuSE is now my favorite distribution, and the only one that can configure and detect my harware RAID correctly. The only hardare problem I have is that my Haupphage tv does not work well."
    author: "mario"
  - subject: "kde.com design"
    date: 2003-05-22
    body: "Well, why does www.kde.com have to look that...un-stylish?"
    author: "KDE User"
  - subject: "Re: kde.com design"
    date: 2003-05-22
    body: "KDE.com isn't affiliated with the KDE project at all. It can look however the owner of the page wants it to, similarly for kde.pl, kde.de. afaik, kde.com hasn't changed much since 1999 or so. \n\nkde.ORG is the only official KDE top level domain."
    author: "tilt"
  - subject: "KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "Ok, I am really pissed that I can't find any really good file manager son linux. Even konqueror does not support basic features I need, there are tons of things that annoy me about it like the bad UI, menu clutter, stability etc. but on the top of my head I can tell you a few things I do not like. \n\n- It doesn't show free space in the selected directory! C'mon!!!\n\n- File copy/move can't resume! I mean, c'mon! I've spent 4 hours copying a file across a slow link when the network goes down momentarily when 98% done. I try to resume and it only asks to overwrite/rename/cancel/skip. (Well, at least it doesn't remove partial files when aborting transfers like M$ Explorer does.)\n\n- You can't access the file transfer queue. I'm copying a few gigs of stuff when I realize that I don't want to copy the next to last item I selected. I either have to abort the whole transfer, re-select the other files and restart the transfer, or I have to sit and wait until the unwanted file starts copying and then abort, remove the partial file, re-select the last file and re-start the transfer. I also can't add files to be transferred to that queue. Thus there is no way to copy files between more than two dirs in one go. Stupid, stupid.\n\n- Select two directories full of files, right-click and choose properties and it'll say \"2 items - 0 files - 2 directories\", and then when you delete those dirs you are happily unaware of your thousands of files being deleted. This probably isn't so very bad once you are aware of this, but I wasn't for a long time and I suspect I've deleted a lot of non-empty dirs that I thought were empty.\n\n- Split a file listing \"Left/Right\" a few times to get several panes, and then try to drag'n'drop a file from one pane to another. Since the target pane is full of files you can't drop the file anywhere because the whole row (of the \"Name\" column) of an existing file will be chosen as target. You actually have to scroll horizontally and then drop the file on another column.\n\n- It caches A LOT, and it doesn't update the cache when you re-visit the directory. The actual filesystem is thus seldom what konqueror shows unless you press \"Refresh\" all the time. In the last few hours it has also removed several files and directories from the file listing cache for no apparent reason. They reappear when pressing F5. (Why can't it start with displaying the cached file listing and do the refresh in the background to ensure it's up-to-date??)\n\n- Selecting dirs with many files really shows the bad underlying design. The file listing isn't updated until the whole directory has been scanned/cached. It isn't even cleared, which gives the impression that the newly selected dir contains exactly the same files as the previously selected dir. It doesn't even give a hint that something is happening! What's even worse is that it doesn't respond to anything while it's scanning the dir. You can't abort it e.g. by trying to select another dir instead. It seems that it doesn't even repaint the window. What's even worse is that it caches the mouse clicks you've done when you tried to change directory while it was so busy, and when it's finished scanning through the dir it responds to those long ago sent mouse clicks! C'mon! Haven't the developers ever seen a book on UI design? (Quite obviously not.)\n\n- The dir tree doesn't stay in sync with the active file listing. E.g. when dragging a file from the file listing and dropping it on another dir in the dir tree the other dir becomes selected while the file listing shows the contents of the first dir.\n\n- The trashcan is a normal directory! This must be among the most stupid design decisions ever made. The trashcan doesn't remember where the file came from. You can't set it to keep only X MB of the last deleted stuff. You can't set it to permanently delete stuff older than N days. You can't even have two files with the same name in it at the same time! (Not even if they originally came from different directories. Not that that should matter.) This is absurd! (Perhaps the trashcan isn't at all part of konqueror...)\n\n- It keeps a lock on something in visited directories. This becomes apparent when you try to unmount something that konqueror has accessed at some point. You have to close the relevant konqueror window for it to release whatever lock it has in the mounted filesystem, so that it can be unmounted.\n\n- The TABs won't shrink after they fill up the whole row, so if you have many TABs you only see a few at a time.\n\n- No emblems like nautilus\n\n- No integrated cd burning like in windows\n\n- extremely slow on directories with thousands of items\n\n- when oading directroies, such as one with pictures it seems to over lay the pictures and generally looks extremely ugly until it finishes loading, it should load smoothly like nautilus\n\n- no zoom like in nautilus\n\n- no differnece in list views for different types of files, for example in music directuries such as oredr by artist album, play time etc. like in Explorer\n\nWhen will Konqueror actually become a good fast featur erich file manager? Is any of this in the works for 3.2? PLEASE IF YOUR GOING TO DO ONE THING FOR 3.@ FIX KONWUEROR!"
    author: "micahel"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "> Ok, I am really pissed that I can't find any really good file manager son linux. Even konqueror does not support basic features I need, there are tons of things that annoy me about it like the bad UI, menu clutter, stability etc. but on the top of my head I can tell you a few things I do not like.\n\nYou really should sumbit bugs/feature requests to http://bugs.kde.org/, and perhaps mailing lists (esp. the kfm-devel one) there are a lot more KDE developers listening there than here. \n\nIMHO, though, I think you're incorrect. I've used quite a lot of file managers, and I like Konqueror second only to a fine file manager called Directory Opus on windows. It's more feature filled than Konqueror, and faster too, especially with large amounts of files. It's great. Check it out at http://www.gpsoft.com.au/.\n\n\n>  The dir tree doesn't stay in sync with the active file listing. E.g. when dragging a file from the file listing and dropping it on another dir in the dir tree the other dir becomes selected while the file listing shows the contents of the first dir.\n\nThis seems like a feature to me.\n\n> - Selecting dirs with many files really shows the bad underlying design. The file listing isn't updated until the whole directory has been scanned/cached. It isn't even cleared, which gives the impression that the newly selected dir contains exactly the same files as the previously selected dir. It doesn't even give a hint that something is happening!\n\nUh... what? When loading a folder with a few thousand mp3's, I see a statusbar here with progress info.\n\n> - no zoom like in nautilus\n\nThe equivalence of zooming has been in Konq since KDE 2.0\n\n> - extremely slow on directories with thousands of items\n\nIt's quite fast for me; however, it does need work. It's not as fast as Directory Opus or Windows Explorer for me. It's certainly faster than Nautilus though.\n\n> - It keeps a lock on something in visited directories\n\nI think it attaches FAMd to it when it's in the directory list.\n\n> - Select two directories full of files, right-click and choose properties and it'll say \"2 items - 0 files - 2 directories\"\n\nQuite true; I think it should scan the directories like Explorer. \n\n> - It caches A LOT, and it doesn't update the cache when you re-visit the directory. \n\nKDE uses famd (as does Nautilus), which watches for file/folder modifications-- looks like it wasn't doing it's job. \n\n> - You can't access the file transfer queue. \n\nHave you tried using kget?\n\n> - It doesn't show free space in the selected directory! C'mon!!!\n\nAgreed. I remember being able to do this in the past. weird."
    author: "tilt"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-23
    body: "> It's certainly faster than Nautilus though.\n\nNo, it's not true. Nautilus is 2-10 times faster than Konqueror in browsing directories over 3000 files on my PIII-700 128mb ram."
    author: "M3taOscura"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-23
    body: "Yes, but with most common usage, I still find konqueror to be much faster.\n\nStill, Nautilus and Konqueror have a way to go before they reach the speed of Directory Opus."
    author: "tilt"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "\"Even konqueror does not support basic features I need, there are tons of things that annoy me about it like the bad UI, menu clutter, stability etc. but on the top of my head I can tell you a few things I do not like....\"\n\nOh no.  We tried so hard.  After years working on a free desktop environment, we do not even have the _basic_ features Micahel (our client) needs.  How could we have let you down like this??  And to make it worse, there are no other file managers out there for you to use either.  I feel bad that we make you use Konwueror.  You know, with it sucking and all.  I personally will not go to work again until I have satisfied your every desire, I'll start on the file transfer queue thing and then work on the Split a file listing.  Unless you want me to work on the Split a file listing first, of course.\n\nI can't speak for anyone else, but I want to thank you for the constructive criticism.  We need more feature requests like these."
    author: "brian"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "It was just a complaint list, there is alot going right with the file manager too. But, why would you want to hear that, you need to know what's wrong with it IMO of course."
    author: "Michael"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "Now did you already look if your complaints are already posted at bugs.kde.org? If so please feel free to vote for all the stuff you complained so much about, and post every complaint which isn't there yet as new report. You don't need to complain again if you prefer to skip that all which I consider a bare minimum of contribution."
    author: "Datschge"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "It was just a complaint list, there is alot going right with the file manager too. But, why would you want to hear that, you need to know what's wrong with it IMO of course."
    author: "Michael"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "I recently installed RH9.0 in the hopes of using it as my new default OS.\n\nAll I can say is, I'll wait for KDE 3.2 + another distribution.\n\nI know that talking about speed in KDE is old news, but this is my honest opinion here ...\n\nAfter wrestling a few days with the OS, I decided to give it a rest by going back to Win2K for some gaming. My reaction was \"Oh my gawd Windows is fast!\" It was as if I had just upgraded my PII 400MHz PC to a P4 2.4GHz machine!! The speed difference between KGL and Win2K is still frighteningly apparent. \n\nWhy am I worried about speed? All the bugs that irritate Michael pale into insignificance when I consider that I actually looked forward to using Konsole instead of using Konqueror for even the simple tasks like moving and deleting files and folders.\n\nLet me not even start about Gnome. The desktop experience was dismal, period.\n\nA pointer for KDE 3.2 .. If the default installation looks like the screenshot of Everaldo's desktop at his website everaldo.com, I believe many more people would be scrambling to get onboard the KGL express. Damn that screenshot looks fine. Granted, I am a linux newbie, but there was simply no way for me to get RH9.0 to look like that. The fonts in the screenshot look exactly like Windows fonts. The anti-aliasing of fonts in RH9.0 made it look better, but nowhere near as crisp as Everaldo's screenshot.\n\n1. When the speed of Konqueror = the speed of Explorer and\n2. when I can get the fonts to look as good as Windows\n(where is to M to R? I will certainly RTFM if there is one to follow) and\n3. when the speed of any Linux DVD player = Any Windows DVD player\n\n... I will happily move camp and take many with me.\n\nRegards\nHope2BKDE3.2User\n\n"
    author: "I'll wait for KDE 3.2"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "Well change your distro.\n\nRH does not really support kde (and not optimized). I'm using Slackware and everything is very very fast. My laptop is a P4 1,8Ghz with 300Mb ram.\nKonqueror is faster than explorer (while loading and borwsing).\nFor dvd player I prefer mplayer over xine and videolan.\n\nAbout the font you can use the GPL bitstream Vera font that is really great.\nAnd your RH Xfree86 conf file is probably not well setup.\n"
    author: "JC"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "Are you referring to Everaldo's screenshot at http://www.everaldo.com/crystal/images/screen2.jpg?\n\nThat looks pretty much like a default KD E 3.0 with KD E3.1's icons (that Everaldo made), and a different window decoration. It'd be cool to have that kind of desktop in one of the choices for kpersonalizer (the app that runs the first time KD E is run)\n\n> 1. When the speed of Konqueror = the speed of Explorer and\n> 2. when I can get the fonts to look as good as Windows\n> (where is to M to R? I will certainly RTFM if there is one to follow) and\n> 3. when the speed of any Linux DVD player = Any Windows DVD player\n\nWell, Konqueror is as fast as Explorer for me for everything but places where there are MANY files/directories. Unfortunatly there, it slows to a crawl.\n\nThe speed of any Linux DVD player, such as xine or mplayer, should be comparable to any Windows DVD player. If it isn't, sounds like a bad configuration by RedHat.\n\nI currently use Gentoo, and KD E flies on it. I've heard a lot of problems with KDE and redhat, so I don't recommend that at all. Of course, I haven't used Redhat since 5.2 personally :)\n\nI recommend Mandrake or SuSE.. they are as easy to install as Redhat (unlike Gentoo... which will probably give best performance, but is hard to install)"
    author: "tilt"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-22
    body: "Hi Tilt\n\nThat is the screenshot I was talking about.\n\nI have never seen such crisp, good looking fonts, like this screenshot. Can someone point out how this was done?\n\nI have looked at many screenies at kde-look and not many people have their setups looking this good. I am referring to the look of the fonts in general, not the specific font used, or do the two go hand-in-hand?\n\nAlso, Konqueror is cleanly laid out, which is great and the ultra-flat look of the windows is stunning. This all adds up to one very professional looking desktop. That is why I recommended this look ---in it's entirety--- as a direction for KDE3.2 to look.\n\nIf you are getting the same speed from Konqueror as Explorer, than my setup must be totally unoptimised. I will try another distribution next time for sure. The crazy thing is, Gnome felt the same. It wasn't as if Gnome was streets ahead of a broken KDE, they felt equally broken."
    author: "Hope2BKDE3.2User"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-27
    body: "Did you try to install true type fonts?"
    author: "Dimitris"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-23
    body: "First off, don't use RH.  I've got Mandrake running on a 366 pentium 2 (kde 3.1), and the speed is comfortable.  Borwser speed (galeon) is faster than IE on a 400 pentium 2 that my SO uses...\n\nDVD playing in linux is a battle of hardware support.  Don't use anything besides mplayer if you want the best performance.  If anything in linux supports hardware acceleration, mplayer can use it...without hardware acceleration, you are using brute force and windows will be faster, because no doubt that company has released a nice, highly tuned, directX driver for windows...\n\nI can play svcd's and divx at normal speed full screen on this old crate, using DGA and by choosing a low-quality \"stretch\" method to fix the aspect ratio...only mplayer lets you choose this amount of detail - you can get mplayer to play anything on any hardware.\n\nThis graphics card doesn't support any special features besides DGA, no xvidix or xv...if I choose to not fix the aspect ratio, then I get a picture that's not quite full screen, but it looks crisp and runs fast...\n\nBTW, I would love (hate) so see XP founder on this old crate with 128 megs of ram...that would be a fair test, no? ;)\n\nBTW, don't use mandrake either ;) I've got debian running on another faster computer, and it rocks.  It's got K3.2.1, and it's faster and uses less ram...how many windows upgrades use LESS resources?  Wonderfull!"
    author: "davros"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-27
    body: "Ummmm, no, it doesn't have KDE 3.2.1 on it. And you give no evidence why someone shouldn't use Mandrake, so don't mind if everyone ignores your comments."
    author: "Joe"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-28
    body: "Well, you didn't ignore my comment ;)\n\nI wasn't proving a point, so I need to provide no evidence...I liked Mandrake enough to use it for several releases...\n\nBut I'm sick of silly errors like SDL blowing up in 9.0 - urpmi is no apt - I FINALLY got transcode installed and it CORE DUMPED out!  Compiling it was a small NIGHTMARE with ZILLIONS of libs needed...Mandrake is better out of the box, for the soccer mom user...but that's about it ;)\n\nWith Debian, I do miss some of Mandrake's control center tools which spoil you but I LOVE being able to install something EASILY (urpmi is FAR from an apt-get)...\n\nEven compiling stuff can be nightmare in Mandrake...with Debian, if I need something, it's just a command away with apt and then I can compile without issue.\n\nThat is worth everything to me.\n\ntootles"
    author: "davros"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-27
    body: "Try Mandrake 9.1. I used to be annoyed at the speed of Konqueror as well, especially start up, in earlier Mandrake releases It used to take 10 seconds to launch, which was unexceptable in my view. It now launches in under 2 seconds. Same hardware, pIII 500, 384Mb memory. Not sure if these are KDE improvements or some Mandrake optimisations, but KDE is quick now, it also seems to start the desktop faster as well. Not as fast as windows 98 mind you on the same hardware (but more stable obviously) but in the same ballpark as Windows 2000.\n\n\nMandrake desktop looks nice. Fonts are antialiased as well. I was starting to be attracted to Redhats bluecurve desktop but The Mandrake desktop is now almost as nice, but uses KDE.\n\n\n\n\n\n\n"
    author: "David O'Connell"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-05-27
    body: ">  Not sure if these are KDE improvements or some Mandrake optimisations,\n\nProbably both.. much of KDE's slowness in startup was due to dynamic linking costs. Prelink has fixed this mostly.\n\n\n> Mandrake desktop looks nice. Fonts are antialiased as well. I was starting to be attracted to Redhats bluecurve desktop but The Mandrake desktop is now almost as nice, but uses KDE.\n\nAgreed... I like mandrake-galaxy better than rh-bluecurve.. although I use gentoo, I use both from time to time."
    author: "fault"
  - subject: "Re: KONQUEROR NEEDS A LOT OF LOVING!!!!!!"
    date: 2003-06-08
    body: "This is an old thread, but for the sake of completeness here goes.\n\nI downloaded Vector Linux (SOHO 3.2) to replace RH9.0 and I am now a happy camper. I would have loved to go the Gentoo route, for speed, portage and being thrown in the deep end with learning Linux, but I only have one PC and compiling from source would make my machine unusable for long periods of time.\n\nI am now still dual-booting between Win2K and Vector Linux, but Vector is the default.\n\nIf I use Arial 10 as my default font on my 1024 x 768 CRT monitor, then the font looks as perfect as Windows :)\n\nKonq. runs fast, so much faster than RH 9.0 that it is now a pleasure to use.\n\nMy system looks as follows:\n\nThe window Deco is OS K, the colours are hand - picked to compliment the window deco, the style is dotNET and the icons are Noia KDE.\n\nTo get around the speed issue of loading applications, this is what I have done:\nI have chosen Desktop 1 to be my work desktop (Office, Graphics etc.), Desktop 2 is Internet, Desktop 3 is Console and Desktop 4 is Config. So when KDE boots, all my favourite applications are booted at the same time, on their respective desktops. Then all I do is switch desktops when I need to do whatever needs to be done. Load time is now Zero as I am simply switching desktops.\n\nThanx for all the help and suggestions."
    author: "Hope2BKDE3.2User"
  - subject: "KDE 3.1.2 Problem (Many)"
    date: 2003-05-22
    body: "I have just finished the installation of KDE 3.1.2. \nFirst of all when I try to compile kdenetwork, kdebase there was a warning during the configuration (this is from kdebase)\n\nconfigure: WARNING: X11/extensions/XKBrules.h: present but cannot be compiled\nconfigure: WARNING: X11/extensions/XKBrules.h: check for missing prerequisite headers?\nconfigure: WARNING: X11/extensions/XKBrules.h: proceeding with the preprocessor's result\n\nAnyway, since that was a warning (hope so), I continued with the make and make install. Everything were OK (!) and now I am using KDE 3.1.2. I have RedHat 8 and I had KDE 3.1.1, which I compiled, without any problem.\n\nI used Konqueror to browse the network and the first web page I visited was www.kde.org. Konqueror stalled, it was not able to show the entire page, but only the portion that fits in the screen. However, if I open another web page (in a new tab) and then go back to the previous web page, I can see the entire web page and use the sidebar (or mouse wheel) to schroll.\n\nAny ideas why this is happening? Could it be because I had these warnings?\n\nThanks\nVasilis\n\n "
    author: "Vasilis"
  - subject: "Re: KDE 3.1.2 Problem (Many)"
    date: 2003-05-23
    body: "I used Konqueror a couple of times and I do not have the problem anymore. "
    author: "Vasilis"
  - subject: "Not trying to plug GNOME..."
    date: 2003-05-23
    body: "But, since we're mentioning a KD erelease. I also wanted to mention taht GnOME 2.3.2 unstable was released.\n\nchangelogs for unstable releases: \n\n2.3.0: http://www.gnomedesktop.org/article.php?sid=1046\n2.3.1: http://www.gnomedesktop.org/article.php?sid=1112\n2.3.2: http://www.gnomedesktop.org/article.php?sid=1127 \n\ntoo bad KDe has no full detailed changelogs, the best stuff sometimes is undocumented =("
    author: "micahel"
  - subject: "Re: Not trying to plug GNOME..."
    date: 2003-05-23
    body: "Wow, a desktop environment using ~/Desktop - where have I seen that before? And all the other trivial changes - for bugs introduced in the previous snapshot."
    author: "Anonymous"
  - subject: "not true"
    date: 2003-05-23
    body: "lost sof those are not just tbugfixes and cahnging the desktop path is somethign quite important. How about you stop trying to make KDE look great at everything and GNOME look like crap, both are good. Lot sof the changelogs do include bugfixes, but also many new features, read it before writting your standard response."
    author: "alex"
  - subject: "Re: not true"
    date: 2003-05-24
    body: ">  How about you stop trying to make KDE look great at everything and GNOME look like crap, both are good. \n\nLook-- most of us don't really care about GNOME. Not that it isn't bad or anything. It's good. However, this is dot.kde.org. If we wanted GNOME news, we would go to gnotices or whatever. Leave this site to KDE specific stuff. Stop posting GNOME release changelogs here (directed at Michael.) Thanks.\n\nI just wish there was user-based moderation here (like in the GNOME news site) so that we could moderate down utterly irrelevent and OFFTOPIC posts like that to oblivian. \n "
    author: "shifte"
  - subject: "Nautilus is faster than Konqueror"
    date: 2003-05-23
    body: "Hi, on my PIII-700 128mb RAM Nautilus is slightly faster than Konqueror when browsing folders over 3000 files (such as Mozilla cache).\nNautilus took less than 8 seconds to read and doing 'file' entire folder, while Konqueror over 20-30 seconds."
    author: "M3taOscura"
  - subject: "Performance cheats"
    date: 2003-05-23
    body: "Hello,\n\nWhen we as KDE users complain about performance we do *NOT* mean the KDE team should engage itself in low disgusting practices of cheating. What I am talking of is the preloading of Konqueror. How low can you go!!\n\nYes, Microsoft is doing it with internet explorer. Yes KDE is compared with Windows. But KDE is better than Windows (well it should be).\n\nDON'T engage yourself in cheating!!\n\nNow get this kode out of KDE a.s.a.p. and go spend your time on real optimizations. I just did did an upgrade to KDE 3.2 and allthough I haven't yet researched wether SuSE or KDE has to be blamed it's slower than ever."
    author: "Daniel Mantione"
  - subject: "Re: Performance cheats"
    date: 2003-05-23
    body: "Exuse me for the typo, I did upgrade to KDE 3.1.2.\n"
    author: "Daniel Mantione"
  - subject: "Re: Performance cheats"
    date: 2003-08-24
    body: "Why don't U fix the performance issues instead of whining"
    author: "coward"
  - subject: "Re: Performance cheats"
    date: 2003-05-23
    body: "Yep, please complain some more about a non-default (unlike in Windows) feature that can be turned on/off easily (unlike in Windows)\n\n<sarcasm>\n\nAlso, at the same time please go and tell the developers of Mozilla, Firebird, Galeon, and Epiphany to remove all offending performance-enhancing (\"cheating\") code from their browsers as well.\n\nIt's about time we crack down on these browser makers for illegally boosting their performance!!!\n\n</sarcasm>"
    author: "shifte"
  - subject: "Re: Performance cheats"
    date: 2003-05-23
    body: "> Yep, please complain some more about a non-default (unlike in Windows) feature that can be turned on/off easily (unlike in Windows)\n\nThe problem is, I don't WANT to spend stacks of time on disabling so called \"features\". Nor do any other sane people. Since we're comparing with Windows, I'm think of those thinking menus, luna-interfaces, irritating popup windows etc. If KDE is going to be like that it will loose me as user.\n\n> Also, at the same time please go and tell the developers of Mozilla, Firebird, Galeon, and Epiphany to remove all offending performance-enhancing (\"cheating\") code from their browsers as well.\n\nThose browsers are so terribly bloated I'm not even going to bother with them. But you are right, I've already shared my displeasure with the OpenOffice developers about autostarting the whole Open Office suite.\n"
    author: "Daniel Mantione"
  - subject: "Re: Performance cheats"
    date: 2003-05-24
    body: "> The problem is, I don't WANT to spend stacks of time on disabling so called \"features\". Nor do any other sane people. Since we're comparing with Windows, I'm think of those thinking menus, luna-interfaces, irritating popup windows etc. If KDE is going to be like that it will loose me as user.\n\nThe thing is that it's not even enabled by default in stock KDE! If a distro enables it by default, there is nothing anyone at dot.kde.org can do about it. Bitch at the distro instead!"
    author: "shifte"
  - subject: "Re: Performance cheats"
    date: 2003-05-24
    body: "Ok, you are right. I did check the default config files before, but apparently SuSE enable it by a source code patch. Shame on them!\n"
    author: "Daniel Mantione"
  - subject: "Re: Performance cheats"
    date: 2003-05-25
    body: "Huh, this may be a stupid question, but how do I turn this feature on/off? I've looked in the KDE control center but didn't find anything about pre-loading...\n\nThanks!"
    author: "Archie Steel"
  - subject: "Re: Performance cheats"
    date: 2003-05-25
    body: "KDE Components/KDE Performance"
    author: "Anonymous"
  - subject: "Re: Performance cheats"
    date: 2003-05-25
    body: "Mmm...that's what I thought, it's not there. Somehow, I mustn't have upgraded that specific part of KDE, or maybe it's texstar's RPM which are not correctly configured. In any case, I do not see this control module at all..."
    author: "Archie Steel"
  - subject: "Re: Performance cheats"
    date: 2003-05-25
    body: "It's new in CVS, IIRC."
    author: "Sad Eagle"
  - subject: "Re: Performance cheats"
    date: 2003-05-26
    body: "Ah, mystery solved...I got confused because the patch got included in the SuSE rpm packages, but not texstar's fine Mandrake ones...oh well, I can wait. I guess I was just wondering if it was me who was a total idiot for not finding the configuration options..."
    author: "Archie Steel"
  - subject: "Re: Performance cheats"
    date: 2003-05-23
    body: "Just disable it for yourself if you think you don't need it. Preloading it can't be that bad considering that 99+% of all KDE user also use Konqueror regulary in some way and will have to start it at some point anyway."
    author: "Datschge"
  - subject: "Re: Performance cheats"
    date: 2003-05-23
    body: "Yeah, we should also preload konsole and kedit, don't you think? You'll need them anyway! Perhaps the file, http, ftp and smb ioslaves too don't you think? And kghostscript wouldn't be bad too.\n\nOf course not!! Preloading software is just bad software engineering and is a poor attempt to hide underlying problems with the software. By preloading Konqueror there is exactly one single little performance problem fixed at the expense of memory usage and startup time.\nOh yeah it can be disabled so we have an excuse for people who actually care about the software running on their computer... People, it's enabled by default!\n\nGet this \"feature\" out of KDE and fix the real performance problems.\n"
    author: "Daniel Mantione"
  - subject: "Re: Performance cheats"
    date: 2003-05-24
    body: "Preloading doesn't really increase startup time when done properly because it happens \"in the background\" once the desktop is loaded already. I don't see the harm and it can be very useful. \nAs a GNOME and Epiphany user, I don't have a problem with KDE doing this, even if it would be enabled by default. They should do whatever they can do to make the experience for their users more \"pleasing\". GNOME is doing practically the same because Nautilus is used to render the background, it's obviously loaded at startup."
    author: "Spark"
  - subject: "Re: Performance cheats"
    date: 2003-05-24
    body: "It's actually not preloading either, more like not-unloading -- i.e. a few instance of Konqueror can be told to hang around on exit, and to popup their windows when a user wants a new instance, instead of starting an entirely new copy. \n"
    author: "Sad Eagle"
  - subject: "Kde 3.1.2 & Slackware 9"
    date: 2003-05-29
    body: "I've noticed that the unofficial binaries are available. Did anyone upgrade to 3.1.2 ? Problems ? KDE apps that do not run anymore after the upgrade ?\n\nThank you for your answers,\nJoseph"
    author: "Joseph"
  - subject: "Very Impressed - New Windows User Convert"
    date: 2003-06-06
    body: "Hi,\n\nI happened to see some screenshots of the new KDE destop in a magazine and was very impressed. I had a spare computer sitting around and thought what the hell, I'll install linux on it so I can try and get this KDE desktop thing and see how it really is. After doing some googling I decided I wanted to try debian (because of the apt-get thing) and a couple of frustrating hours later I finally was able to download and install KDE.\n\nI got to say...I was blown away, it was even better than the screenshots I seen. I suppose I'm just another sheltered windows user (or was anyways!) that was unaware of the alternatives out there. I mean I had heard of linux before but never imagined that it could be as good a desktop solution as it is...but with kde I think it's better than my XP desktop now.\n\nI am very eager to make the transition from windows to linux and I am preaching it to my friends and family now. Now that my eyes are open to the fact that there are alternatives and IMHO they are superior alternatives as well, I want to thank KDE and all the hard working people that make it happen. KDE rocks :-)\n\n-Pat"
    author: "Pat"
---
The <a href="http://www.kde.org/">KDE Project</a> has <a href="http://www.kde.org/announcements/announce-3.1.2.php">released KDE 3.1.2</a>, the second maintenance release of the KDE 3.1 release series. It features more and much improved <a href="http://i18n.kde.org/">translations</a> and many problem corrections. Read the <a href="http://www.kde.org/announcements/changelogs/changelog3_1_1to3_1_2.php">Changelog</a> or jump directly to the <a href="http://www.kde.org/info/3.1.2.php">download links</a>. Those of you who wish to compile from source can use <a href="http://konsole.kde.org/konstruct/">Konstruct</a> for near automatic compilation.
<!--break-->
