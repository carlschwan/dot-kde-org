---
title: "Introducing www.kdedevelopers.org"
date:    2003-07-20
authors:
  - "igeiser"
slug:    introducing-wwwkdedevelopersorg
comments:
  - subject: "Good idea"
    date: 2003-07-20
    body: "I really like this idea.\nI can't wait to read more things in the website.\n\nBtw, where is the RSS link ? :)"
    author: "JC"
  - subject: "Re: Good idea"
    date: 2003-07-20
    body: "whoops\n\thttp://www.kdedevelopers.org/blog/feed\n\nN-Joy\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Good idea"
    date: 2003-07-22
    body: "Great.  If someone sends me a simple script that pulls this file and converts it to something like <a href=\"http://static.kdenews.org/tmp/apps\">this</a>, I'll be happy to put a sidebox on the dot.  Too lazy/tired/busy to do it myself at the moment, but might eventually get to it if noone steps up..."
    author: "Navindra Umanee"
  - subject: "Re: Good idea"
    date: 2003-07-22
    body: "Ill see what i can do.  any reason why you dont do rss?"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Good idea"
    date: 2003-07-22
    body: "Haven't really had a need so far for it, so I haven't looked into it.  apps.kde.com historically provides us with HTML and the kde-look.org stuff is a hobbled together hack that parses the main page of the site..."
    author: "Navindra Umanee"
  - subject: "Re: Good idea"
    date: 2003-07-25
    body: "RIght now, pulling this file includes quite some garbage at the top not related to the RDF feed.  For example, I see entire log entries in this file at the moment:\n\nWell, I still have no time to hack on KDE, (well, Kopete to be more specific) \n\n[...]\n\nBut that's after I'll port KMail and KNode to it, especially that I n\need to work on html export in KWord a little more.<?xml version=\"1.0\" encoding=\"\nutf-8\"?>\n<!DOCTYPE rss [<!ENTITY % HTMLlat1 PUBLIC \"-//W3C//ENTITIES Latin 1 for XHTML//E\nN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml-lat1.ent\">]>\n\n[proper RDF feed continues here]"
    author: "Navindra Umanee"
  - subject: "here it is"
    date: 2003-07-20
    body: "http://www.kdedevelopers.org/node/feed"
    author: "pat"
  - subject: "Re: here it is"
    date: 2003-07-20
    body: "It's too bad that the feed is served with the text/html content type, when it's not html."
    author: "AC"
  - subject: "DCOP Widgets"
    date: 2003-07-20
    body: "http://www.kdedevelopers.org/node/view/30\n\nThis is a really cool idea. :-)"
    author: "AC"
  - subject: "Re: DCOP Widgets"
    date: 2003-07-21
    body: "whats the difference between this one and the already existing qt<->dcop bridge ?\n"
    author: "ik"
  - subject: "Re: DCOP Widgets"
    date: 2003-07-21
    body: "read the link you will see its VERY different.\n(no im not going to explain it because thats what the link is for)\n\nCheers\n-ian reinhart geiser\n"
    author: "ian reinhart geiser"
  - subject: "Re: DCOP Widgets"
    date: 2003-07-22
    body: "ah indeed, i should have read it before commenting ...\nit looks very interesting, another way (besides kommander) to create 'kde shell scripts' (and more). it seems to have some advantages over kommander too (but its less straightforward i guess)."
    author: "ik"
  - subject: "Re: DCOP Widgets"
    date: 2003-07-22
    body: "I'm not an expert, but I'll try to answer anyway.\n\nI think the main difference is that it makes the link bidirectional... Not only can you send messages to qt widgets, but you can receive messages from them too.\n\nSo you make an XML file describing what your widgets are, then you make a shell script describing what they should do... and *poof* you have a mini KDE application.\n\nI guess it's not such a big deal if your favourite language already has a way to call Qt, since then your program could just make the widgets itself, and then of course it could get messages from the widgets. I would guess that the differences are:\n\n1. This new method would work with any scripting language that can read and write from pipes and files... which should be all of them. :-)\n\n2. It doesn't depend on your users having the Qt-from-Perl or whatever module installed (for example).\n\nI think it would be great for building KDE front-ends of command-line utilities, and for things like graphical installers for KDE programs.\n\nThen again, a lot of my post is speculation. :-)"
    author: "AC"
  - subject: "Re: DCOP Widgets"
    date: 2003-07-22
    body: "You are completely correct, but there is one detail that is missing and is not quite bug free.  Basicly that is the fact that you can map most common Qt signals from the UI to events that can be handled at the command line.\n\nThis is the true power.  With KDE containers now in Qt 3.2 we will see things like KJanuswidget in there too.  Imagine all those sysadmins who can now have GUI front ends to all of those one off shell scripts that hold their systems together.  Imagine all of those wacked out perl scripts that now can have a UI so that the users in the main office can use them.\n\nThe goal of this is to allow system admins to make KDE do what \"THEY WANT TO DO\"  without the pain of bindings, and learning C++.  Just draw your UI file in Designer, write a perl/python/shell script that interfaces with the dcop api (that is the same on all languages) and go....  ( you must be a sysadmin or a recovering sysadmin, because you seem to get this concept quite well )\n\nSure its not native, but damn, its fast, and probibly fits the bill for all those one off projects.\n\nThis is cool ;)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "main page"
    date: 2003-07-20
    body: "When I go to the main page at http://www.kdedevelopers.org/, it really should not be empty like that.  It should send me directly to http://www.kdedevelopers.org/blog which seems to be the most interesting content on the site.\n\nBtw, is it just me or are the fonts on this site extremely tiny?"
    author: "Navindra Umanee"
  - subject: "Re: main page"
    date: 2003-07-20
    body: "im still working on this page, I am trying to make it more of an information page and system status messages.  Im still working out a few kinks, but for general journals its pretty good.  \n\nAnother issue, is the anon comments are moderated, and im not sure if thats going to fly, the plus side is it helps protect against trolls, but the minus side is im not sure how many developers are willing to spend time moderating comments.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: main page"
    date: 2003-07-20
    body: "Well the dot gets by without moderation, so I think you should relax it.  Anti-anon moderation practically killed KDEnews in my opinion.\n\nHowever, I do think you should reconsider making the frontpage more than just a status page and put all the exciting content right there (in some form).  Unfortunately, I do understand the use for a frontpage and if you put the main content there then you don't have many options for your system/status stuff..."
    author: "Navindra Umanee"
  - subject: "Re: main page"
    date: 2003-07-21
    body: "I also support unmoderated anonymous comments, the way the dot allow it (the poster can leave a name and an adress if he wishes to). If somebody wanders around and wants to make a remark, having to subscribe is a tedious processus that will kill the envy to contribute quite effecientely in my experience.\n\nOvercontrol has always proven to be bad, be it for code development, blog comments or simply way of life."
    author: "Philippe Fremy"
  - subject: "Re: main page"
    date: 2003-07-21
    body: "no, you miss the point, the goal here is not for user posts.  this is ment for developers.  anon users can post comments, and vote on polls but thats it.\n\novercontrol is a good thing here, since the blogs are a service to KDE developers and an interest to kde users.  If they find a useful nugget then they can post it here.\n\nOtherwise, the main focus is for the developers.\n\nCheers\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: main page"
    date: 2003-07-22
    body: "1. Users also have good idea, even in the development process. It is important that they can post comments. Sometimes, the best idea for a software comes from an external contribution.\n\n2. KDE developers may wander here without being subscribed and should still be able to post comments."
    author: "Philippe Fremy"
  - subject: "Re: main page"
    date: 2003-07-21
    body: "> is the anon comments are moderated, and im not sure if thats going to fly, the plus side is it helps protect against trolls\n\nIn my experience, very few trolls hang around in blog type sites."
    author: "anon"
  - subject: "Blog features"
    date: 2003-07-22
    body: "Do you plan on supporting trackbacks, pings, XML:RPC, RDF tags, blogrolls?\nWould be nice to see at least some of those features, as well as valid HTML. ;-)\n(I don't think & is correctly changed into &amp; within links and I'm personally no fan of using tables for layout)\n\nNice idea though, might be big if you can smoothen it a bit. :-)\n"
    author: "Rob Kaper"
  - subject: "snooze"
    date: 2003-07-22
    body: "WOW, what an exciting blog! :-( The dot barely gets enough traffic to keep people coming back, and now you want to divide it further by adding yet another news site.... which I've noticed you have yet to post any news for. I don't understand why dot readers wouldn't be interested in kde-development, and anyway you don't have enough news to keep us reading anyway, so what's the point of a news site without any news?"
    author: "Jeff"
  - subject: "Re: snooze"
    date: 2003-07-22
    body: "click on blogs in the top"
    author: "anon"
  - subject: "Re: snooze"
    date: 2003-07-22
    body: "> The dot barely gets enough traffic to keep people coming back\n\nI'm looking forward to don't see you again here."
    author: "Anonymous"
  - subject: "Re: snooze"
    date: 2003-07-22
    body: "Wow, you sure are critical...\n\nWhich is it? Either he doesn't \"have enough news to keep us reading anyway\" or he's dividing the dot readership...\n\nI for one am capable of checking both sites. And, while interesting, I'm not sure that the dot is the right place for \"what I worked on today\"-style informal posts by the developers."
    author: "AC"
  - subject: "Re: snooze"
    date: 2003-07-22
    body: "Whoops... you're right about the /blog page.  Yeah, more content!\nI still maintain that merging the sites to have news updated more often than (I'll say on average) about 3 posts per week would be better, imo.\nI guess I can just check another site every so often now."
    author: "Jeff"
  - subject: "Re: snooze"
    date: 2003-07-23
    body: "I think you fail to see what's the difference between a news and a blog site. I (and others) sure don't want to post my personal stuff on a news site but still might include it in my own blog."
    author: "Datschge"
---
Have you ever wondered what the KDE developers were thinking? Have you ever wondered what motivates or annoys KDE developers?  It was these questions that prompted the development of <a href="http://www.kdedevelopers.org/">http://www.kdedevelopers.org/</a>, an <A href="http://www.kdedevelopers.org/blog">online weblog</a> for KDE developers.
<!--break-->
<p>
This weblog is intended to be for KDE developers to journal their thoughts in a community atmosphere. This is not meant to really compete with things like <a href="http://dot.kde.org"/>http://dot.kde.org/</a> or the <a href="http://www.kde-forum.org">http://www.kde-forum.org/</a>. This is meant to be an area exclusively for KDE developers to share their thoughts about KDE or anything else for that matter. It's an incubator for ideas and an area to let the community see what the KDE developers are thinking. This is not a reflection of KDE policy but a reflection of KDE developers own thoughts. This freedom allows developers to think outside the box and not have to worry about policy derailing a discussion in its infancy.
</p><p>
For interested users who want to watch what their favorite developers are up to, we are providing RSS feeds for both the main site and individual blogs. I hope both visitors from outside the KDE community and developers from within the KDE community find this site enjoyable and friendly.</p>