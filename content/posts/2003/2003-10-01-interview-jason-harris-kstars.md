---
title: "Interview with Jason Harris of KStars"
date:    2003-10-01
authors:
  - "jmutlaq"
slug:    interview-jason-harris-kstars
comments:
  - subject: "DOTted??"
    date: 2003-10-01
    body: "Is this the first site down by dotsy (i dont think so)\n\nAnyway google cache shows some ;-)\nhttp://216.239.59.104/search?q=cache:Gt4a49Q06JQJ:www.stsci.edu/+space+telescope+science+institute&hl=en&ie=UTF-8"
    author: "cies"
  - subject: "Re: DOTted??"
    date: 2003-10-01
    body: "heh, not dotted, we had a disk failure.  Should be back at noon EST :)"
    author: "LMCBoy"
  - subject: "Great Interview"
    date: 2003-10-01
    body: "I really hope that someone takes over people.kde.org's series of interviews as well."
    author: "anon"
  - subject: "Re: Great Interview"
    date: 2003-10-01
    body: "I still have hope that Tink will restart it."
    author: "Anonymous"
  - subject: "Re: Great Interview"
    date: 2003-10-01
    body: "great! :)\n\nthey were always entertaining, as far as I could remember. "
    author: "anon"
  - subject: "Very nice interview"
    date: 2003-10-01
    body: "Jasem, that was a very nice interview, very good idea. \n\nJason, you are right, I am surprised. It is the first time I know about \"Astrocalc\", I have learnt it now, when reading the interview. That is a funny coincidence because a couple of months before joining the KStars team, I made a small astronomical calculator (a prototype) to learn how to develop with  C++, Qt and KDE and I called the project  \"astrocalc\"...\n\nI have really enjoyed the interview. I would like to add that Jason has also made in the 3.2 version a big effort to make sure that KStars uses free data (catalogs and images). He has even compiled himself one free catalog by gathering disperse information, because the alternative catalog is only available for non-comercial purposes. \n\nJason, many thanks for KStars, it is a wonderful application."
    author: "Pablo de Vicente"
  - subject: "Re: Very nice interview"
    date: 2003-10-01
    body: "Excellent interview. Thanks Jason and Jasem.\n\nNow I know what I'm reading this winter. Horatio Hornblower. Now I just have to find it...\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Very nice interview"
    date: 2003-10-02
    body: "yes, very nice indeed. Thanks a lot to the whole KStars team for that superb application. This is the leading application of KDE-Edu and it's amazing. Thanks Jas{em, on} and the other people of the team."
    author: "annma"
  - subject: "Re: Very nice interview"
    date: 2003-10-02
    body: "For those who like Horatio Hornblower, may I recommend Patrick O'Brian's Aubrey/Maturin novels? They're kind of a cross between the Hornblower books and Jane Austen -- lots of mizzens and grog, but more character development and whatnot."
    author: "Otter"
  - subject: "For OpenGL, Try COIN"
    date: 2003-10-01
    body: "If you're going to play with OpenGL, unless you are a machocist, don't deal directly with the native low-level API.  Instead use the COIN (http://www.coin3d.org) implementation of SGI's Open Invetor scene graph high-level API.  It's a million times easier to program to, is claimed to have minimal performance hit over raw OpenGL, and best of all has the SoQT library, a bunch of QT widgets for displaying and manipulating the scene graph using QT.\n\n\"The Inventor Mentor\" is THE book on the subject, many unis have a licensed copy available on their intranets.\n\nJohn."
    author: "John"
  - subject: "Re: For OpenGL, Try COIN"
    date: 2003-10-02
    body: "I guess I shouldn't expect to understand SoQt in a day, but is it possible to imbed a GL-enabled widget inside a KMainWindow with SoQt?  From what I read, the program's main function must call SoQt::init(argc, argv), which instantiates the main window widget.  That's no good for a KDE app."
    author: "LMCBoy"
  - subject: "Re: For OpenGL, Try COIN"
    date: 2003-10-19
    body: "Oh, yeah, there are other FOSS scene graphs available, such as OpenSceneGraph (which also has QT bindings) and OpenRM and many others, so hunt around to see what you like..."
    author: "John"
  - subject: "Even the pro:s use KDE."
    date: 2003-10-02
    body: "Isn't that amazing?\nhttp://www.virtutech.com/images/ieee-snapshot-5.png"
    author: "OI"
  - subject: "Terrible interview"
    date: 2003-10-02
    body: "What a terrible interview... I didn't get to find out if he sings in the shower. :-)"
    author: "AC"
  - subject: "KEarth?"
    date: 2003-10-02
    body: "I know this is absolutely not the topic here ... but... would it be possible to have a \"KEarth\" that would be as nice as KStars, but for earth maps?  Would be nice to find location of friends in KAddressBook or locate stores :)  I guess there are public databases of geocoordinates somewhere.\n\nAnother case of the \"just my 2 cents\"!"
    author: "nox"
  - subject: "Re: KEarth?"
    date: 2003-10-02
    body: "There was a really promising app called kartographer that looked great.  It used to live at www.kartographer.org, but that site is no longer responding.  The Wayback Machine shows this as the last version of the page:\nhttp://web.archive.org/web/20030217230837/http://www.kartographer.org/\n\nSo it sounds like the project is stalled.  Too bad, it had a lot of potential.  Maybe interested parties could attempt to contact the author."
    author: "LMCBoy"
  - subject: "Re: KEarth?"
    date: 2003-10-02
    body: "Well <blush>, that would be me.  Thanks for the kind words.  I've been off doing extensive first-hand research into the layout of the earth (i.e 9 months holiday bumming around South America), hence the hiatus.  But now I'm back and re-assesing where things are at, hence my comment above about using COIN for OpenGL work (lesson 1, kids, nothing kills the fun factor faster than wrestling with a bad API). \n\nI do want to work on ths again, but the vision I now have is far less ambitious, really only an educational toy.  For a more Encatra style experience try KFlog which is a glider flight planning tool, but has the entire globe mapped in high detail (read big data files).  There's talk they'll turn their mapping code into a seperate library one day, when they do, I'll be all over it.\n\nI start a new job in a new city next week, and then have to find the cash for a new PC, but once that's sorted I'll try for a port of the current code to COIN and bolt on the CIA World Book and Wikipedia entries.  For a version of the old code in all its failings, visit the website which is back up.  I'll see if I can update the web page a bit, track down a more recent tarball, re-survey the available datasets and toolkits, etc, but with no home and no PC I'm reliant on net cafes for now :-)\n\nJohn."
    author: "John"
  - subject: "Re: KEarth?"
    date: 2003-10-02
    body: "Don't count on a separate KFLog map lib anytime soon from the KFLog team... Developement is a bit slow at the moment, and the focus is not on stuff like this. We are trying to make a better UI, better logger integration and a 3D view first. I am planning to make a separate lib for the embedded version of KFLog and for Cumulus though. It is basicly the same mapengine, only tweaked for higher performance on small processors. The rationale behind that is that it takes less space and development effort for the two programs if shared code is factored out in a lib. However, development is slow there also."
    author: "Andr\u00e9 Somers"
  - subject: "Re: KEarth?"
    date: 2003-10-02
    body: "There are some public databases out there, but they are not nearly as detailed as you suggest. Locating a store based on them is out of the question, unless you know better sources than we (the KFLog team) does. If you do know of sources, please let us know on www.klfog.org!! Some of our data is really outdated..."
    author: "Andr\u00e9 Somers"
  - subject: "Bundled with celestia?"
    date: 2003-10-02
    body: "Maybe Harris should contact celestia (http://www.shatters.net/celestia/) to combine their efforts (with the OpenGL version), as they already have quite impressive OpenGL space simulation/simulator on their hands that use kde libs?\n\nBest thing would be to convince the celestia people to include their work with the kde distribution (IMO never gonna happen, sadly).\n\nSure it also support that silly Windoze variations and the wonderful Mac OS X...\n"
    author: "Nobody"
  - subject: "TIDE"
    date: 2003-10-02
    body: "I would like to have a tide display, Karamba or like that but I don't know where to optain the data. As the water level data is determined by the moon you may probably help me. "
    author: "gerd"
  - subject: "Re: TIDE"
    date: 2003-10-02
    body: "Tides are complicated by the particular local geography of the shoreline, so it isn't nearly as simple as following the Moon's position.  A quick googling turned up XTide, which sounds like what you need:  http://www.flaterco.com/xtide/index.html\n"
    author: "LMCBoy"
---
This month we interview Jason Harris, the original author and current maintainer of <a href="http://edu.kde.org/kstars/">KStars<a>. Jason Harris is a postdoctoral Astronomer at the <a href="http://www.stsci.edu/">Space Telescope Science Institute</a>. He talks about KStars' history, what's coming in KDE 3.2, and the future. Read on to find out more!
<!--break-->
<p><hr>
<h2>Interview with Jason Harris of KStars</h2>
<i>Interviewed by <a href="mailto:mutlaqja@ikarustech.com">Jasem Mutlaq</a></i>
<p>
<p><b>1. When and where were you born?</b>
<p> McHenry, Illinois, USA.  1973. <p>

<b>2. What school did you go to?</b>
<p> McHenry High School! :)  I went to college at the University of Arizona, and went to graduate school at the University of California at Santa Cruz. <p>

<b>3. Where do you live now?</b>
<p>I currently live in Baltimore, Maryland, but if you ask me in a month, I'll say Tucson, Arizona. <p>

<b>4. When and Why did you start KStars?</b>
<p> kstars-0.1 was published at Sourceforge in April 2001, but I think I started coding it on my own a few months before that.<p>  I don't think I ever mentioned this before, but KStars actually started as a windows program that I wrote for fun in 1999 called "Astrocalc".  I used Borland C++ Builder for that.  It was really primitive; kstars-0.1 was basically a port of Astrocalc to KDE.  It's a complete coincidence that the KStars calculator is named "Astrocalculator"; that was Pablo de Vicente's doing, and I don't think he knew about the original Astrocalc until reading this :) <p>

As to why, in general, I just really enjoy using a computer to make something beautiful and complex.  It's definitely my creative outlet.  In 1999, I was on a long flight home from Chile, and I got it in my head that I could write a simple sky-plotting program.  I worked out a lot of the math on paper there on the plane.  When I got home, I tinkered with it for a couple of months, but then kind of lost interest. That was Astrocalc.  It sat on my hard drive for about two years, during which time I finished my Ph.D. and got more and more into Linux.  I noticed that my favorite DE didn't have an astronomy program, and I remembered Astrocalc.  Luckily, I still had the code lying around, so I harvested what I could and called it kstars-0.1. <p>

I was completely amazed at how easy it was to code with Qt and KDE.  As some of you know well by now (hi Coolo!), I am not a great programmer.  I don't know a singleton from a factory, but the API is so great, I don't have to! :)  Anyway, I quickly set my ambitions way beyond anything I could have done with Astrocalc.  The other magic ingredient was that kstars attracted other developers really quickly after it was posted at Sourceforge.  I'm really fortunate to have such a great group of co-developers; KStars wouldn't be nearly as cool as it is without their efforts.<p>

<b>5. What features will be available in KStars in the KDE 3.2 release?</b><p>
One of the coolest new features is your (Jasem's) <a href="http://edu.kde.org/kstars/screens/moontracking.png">telescope control</a> stuff.  People, of course, have often asked about controlling a telescope with Kstars, and my response was always something like: "yeah, that would be great, but...I wouldn't even know where to begin".  Well, it has arrived!  Most popular computerized mounts are already supported, and the integration with the rest of the program is really great.  You can control any number of telescopes, locally or remotely (through ssh, even!).  Jasem has already used KStars to drive a telescope thousands of miles away.  That is just cool. :) <p>

Of course, not everyone has a telescope with a computerized mount (me included).  There's plenty of other stuff in store for 3.2 as well.  The number of stars has increased to 126,000.  We also now include <a href="http://edu.kde.org/kstars/screens/minorplanets.png">2000 asteroids, 400 comets</a>, and the <a href="http://edu.kde.org/kstars/screens/jupiter.png">moons of Jupiter</a>.  You can show a "trail" behind solar system bodies, which trace their path across the sky so you can see retrograde loops and other interesting effects.  We added a number of "Tools" to the program, including a variable-star lightcurve generator, an <a href="http://edu.kde.org/kstars/screens/altvstime.png">altitude-vs-time plotter</a>, a "What's up Tonight?" summary, a solar system viewer, a tool for plotting the positions of Jupiter's moons, and a "script builder" which provides a GUI for generating complex behavior scripts using our extensive set of DCOP functions.<p>

Oh, and we added some command-line arguments for generating an image of the sky without actually launching the GUI.  I was hoping KStars could then be used as one of the programs which dynamically generates your desktop background, but I can't get it to work.  I suspect that KStars just takes too long to make its image, but I don't know for sure.  Anyway, from the command-line it works great. :)<p>

There's a lot of other stuff, but those are the big ones, I think.  I should also mention that we made good  progress in optimizing the sky-rendering and startup time.  Both are 50-100% faster than in 3.1, I would guess.<p>

<b>6. What's missing from KStars that you'd like to implement in future releases?</b><p>
We have lots of great ideas.  One thing I really want to try is OpenGL rendering of the sky.  I know zero about OpenGL programming, but I am going to use the 3.2 freeze to start learning about it.  I also want to make it easier to add non-standard data to the program, through an integrated download/install tool like KNewStuff.  Another thing I've been thinking about is extending the abilities of the Astrocalculator tool to something like a mini-spreadsheet program with specialized functions for crunching astronomical data (useful for building observing lists).  Pablo has already started the groundwork for this; in 3.2, many of the calculator modules now have a "batch mode" that operates on text files.  I'd also like to make some games and quizzes using the DCOP Script Builder.  The ability to script some pretty interesting stuff for classroom use is almost there, I think. <p>

More ideas are always welcome, so file your wishes at <a href="http://bugs.kde.org">bugs.kde.org</a>!<p>

<b>7. What other projects are you involved with?</b> <p>
No other programming projects at the moment.  Last year I made a KDE GUI frontend for the <a href="http://www.30doradus.org/nwn/serverwatch.html">Neverwinter Nights Linux server</a>, but that's finished now.  Well, I did write some code for my thesis which I am still maintaining and improving.  It's just numerical analysis code, 100% fortran-77, probably not of much interest here! :) <p>

<b>8. What are your main research interests in the <a
          href=http://www.stsci.edu">Space Telescope Science Institute</a>?</b> <p>
I study galaxy evolution at the stellar-populations level, meaning I measure properties of individual stars (and star clusters) in nearby galaxies in order to learn something about the history of the galaxy, and how it has been interacting with its neighbors.  So far, I've been focusing on the Large and Small Magellanic Clouds (LMC/SMC), a nearby spiral galaxy (M 83), two dwarf starburst galaxies (NGC 3077 and NGC 5253), and a tiny, isolated dwarf in the Local Group (Sextans B).<p>

<b>9. What do you do in your spare time?</b> <p>
I use Gentoo at home, so I spend every waking moment compiling something or other.  Just kidding. :)  I've got a good group of friends, so we hang out, go out to dinner, see movies, whatever.  I like riding my bike, going down to Washington DC, hiking, etc, etc. <p>

<b>10. How much time do you spend on KDE?</b> <p>
Not too much, 0-4 hours per day, I guess. <p>

<b>11. Where do you see yourself and KDE 5 years from now?</b> <p>
I will hopefully be on the astronomy faculty at some university by then, using KStars and KDE for classroom demos. <p>

<b>12. What's your favorite food?</b> <p>
Drunken Noodles at Thai Landing here in Baltimore.  Hi Charlie! ;) <p>

<b>13. What's your favorite vacation spot?</b> <p>
Easy: Yosemite National Park, California.  Get there. <p>

<b>14. What was the last book your read?</b> <p>
I am reading the Horatio Hornblower series by C. S. Forrester.  Avast!  Before that I read "Paper Daughter" by M. E. Mar.  Oh, and I am halfway through Douglas Hofstadter's magnum opus: "Le Ton Beau de Marot". <p>

<b>15. What was the last movie you saw?</b> <p>
Hmm, on DVD we just watched "High Fidelity" starring John Cusack.  The last theater movie was "Once Upon a Time in Mexico", which was good campy fun, if you can leave your knowledge of physics at home. :) <p>

<b>16. You are having a BBQ in your backyard and you're allowed to invite 3 famous people who would you invite and why?</b> <p>
<ol> <li>Doug Hofstadter.  He's a computer programmer/engineer by trade, but he speaks maybe eight languages and writes books on linguistics, translation, and classical music.  I'd love to chat with him. </li><p>
<li> Douglas Adams (can I make an impossible pick?  maybe my backyard has an infinite improbability drive :).  He'd be tons of fun; I went to see him speak when I was in college but he had to cancel the show, to my lasting regret.</li><p>
<li>Hmm, another Doug?  Nah.  Either Terry Gilliam or Neal Stephenson.</li></ol><p>

<b>17. Name your favorite quote/saying.</b><p>
"Perhaps I'm old and tired, but I always think that the chances of finding out what really is going on are so absurdly remote that the only thing to do is to say hang the sense of it and just keep yourself occupied." (Slartibartfast) <p>

<b>18. Beer or Wine?</b> <p>
mmm...beer! <p>

<b>19. Vi or Emacs?</b> <p>
Emacs, of course.  But then, I use awk instead of perl, so what do I know? ;p <p>

<b>20. <a href="http://www.seds.org/messier/xtra/ngc/lmc.html">Large Magellanic Cloud</a> (LMC) or <a href="http://www.seds.org/messier/xtra/ngc/smc.html">Small Magellanic Cloud</a> (SMC)?</b> <p>
LMC!  My online nick is LMCBoy, and my website is <a href="http://30doradus.org">30doradus.org</a>.  Easy one :)  The SMC is cool too, I just submitted a paper on it. <p>

<i>Hope you enjoyed the interview!</i>