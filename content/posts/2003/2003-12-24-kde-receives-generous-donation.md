---
title: "KDE Receives Generous Donation"
date:    2003-12-24
authors:
  - "wbastian"
slug:    kde-receives-generous-donation
comments:
  - subject: "Quite modest jump between 2002"
    date: 2003-12-24
    body: "$ 7684.23 in donations in year 2003\n$ 1810 in donations in year 2002\n\nThanks to all donors of course.. even small donations go a long way with the e.V"
    author: "anon"
  - subject: "Re: Quite modest jump between 2002"
    date: 2003-12-24
    body: "KDE seems to produce a better desktop for these small amounts than Gnome does. Funny! Now, what is the KDE League for, and why do we need it? Does it still exist? (Yes I know this has been done before, but it had to be asked) We have KDE e V, which is non-profit, the way it should be in my opinion, and we have respected KDE Developers on it. It is also nice that they have announced this donation, and some news on how the money is spent would be good for the community as well. This would generate a snowball effect as more people are comfortable about contributing."
    author: "David"
  - subject: "Re: Quite modest jump between 2002"
    date: 2003-12-24
    body: "Read the \"Some clarification on structure\" section at http://www.kde.org/areas/kde-ev/meetings/2002.php"
    author: "Datschge"
  - subject: "Re: Quite modest jump between 2002"
    date: 2003-12-24
    body: "Perhaps half a year later it is revealed that the anonymous donator was Michael Robertson. More Robertson stuff on KDE == more donations :-9\n\nHope he will come to Europe as a real person. I would like to meet him at CeBit 2004 nex year, Hanover, and listen to him. I just know his letters from his PR department that my Mozilla spam filter catches."
    author: "Andy"
  - subject: "KDE merchandise?"
    date: 2003-12-24
    body: "Does KDE e.V. have a store where you can buy a plush Konqi to support KDE? :-)"
    author: "AC"
  - subject: "Re: KDE merchandise?"
    date: 2003-12-24
    body: "That's a good Question. I'd like to have one of those in my office but I don't know where to get one.\n\nIf you're a Quanta fan kittyhooch.com has Quanta T-Shirts and mouse pads. I'm revising the artwork to show version 3.2 images."
    author: "Eric Laffoon"
  - subject: "Re: KDE merchandise?"
    date: 2003-12-24
    body: "Or rent Eric Laffoon for advertisement. Eric drinks our beer...\n\n\n  "
    author: "Gnometroll"
  - subject: "Re: KDE merchandise?"
    date: 2003-12-25
    body: "> Or rent Eric Laffoon for advertisement. Eric drinks our beer...\n\nWell it had better be good beer. I'm not lending my name to swill after I've become a star with thousands of cat owners. If there's enough Quanta users to make me a recognizable beer spokes person then we have arrived. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: KDE merchandise?"
    date: 2003-12-25
    body: "He probably drinks Treadmill anyway"
    author: "Simon"
  - subject: "Re: KDE merchandise?"
    date: 2003-12-25
    body: "> He probably drinks Treadmill anyway\n\nActually last time I had a beer IIRC was several months ago when I had lunch with my buddy Randal. We like to split a pitcher of the beer of the day and since we live in Oregon it's a microbrew ale. I believe it was a boysenberry hefeweizen. Since my grandfather owned a beer and wine importing business I learned early on the difference between lager, pilsner, ale and bathwater. There are some things not worth consuming... cheap lager, most processed meats, wine with a screw on cap, machine rolled cigars, sugary cola, condiments that ship in large plastic tubs, deep fat fried cheese, cheese that comes in a can... Life is too short to run bad software too. ;-)\n\nThe only treadmill I know is the one I run on in the gym for 20-30 minutes after I finish 45 minutes to an hour of very intense lifting. Unfortunately a good beer is about 15 minutes on the treadmill for me."
    author: "Eric Laffoon"
  - subject: "Quanta Santa"
    date: 2003-12-25
    body: "Eric, your personality strength is revealed when we look at the name of your web development application: Quanta, not Kuanta! A real non-conformist!\n\n:-)\n\nQuanten means coll., pej.  \"foot\" in German\nalso pl of quantum\n\nQuantum = quantity, quanta = quantities? \nengl./latin quanta = pl of quantum\n\nNil, nisi quanta dabis basia, corde moves\n\nor in Markus, 5/19:\net non admisit eum sed ait illi vade in domum tuam ad tuos et adnuntia illis quanta tibi Dominus fecerit et misertus sit tui "
    author: "Andy"
  - subject: "Re: KDE merchandise?"
    date: 2003-12-24
    body: "Not KDE e.V., but the merchant donates a part of the prize to KDE:<br>\n<a href=\"http://lem.freibergnet.de/cgi-bin/fanshop/process/locale/en_US/page/06-001.html\">\nhttp://lem.freibergnet.de/cgi-bin/fanshop/process/locale/en_US/page/06-001.html\n</a><br>\nFor other stuff see\n<a href=\"http://www.kde.org/stuff/\">http://www.kde.org/stuff/</a>\n"
    author: "Anonymous"
  - subject: "Re: KDE merchandise?"
    date: 2003-12-24
    body: "Check <a href=\"http://www.kde.org/stuff/\">http://www.kde.org/stuff/</a> for T-Shirts, shirts, mugs, mousepads - and stuffed Konqis as well.\n"
    author: "Frerich Raabe"
  - subject: "Catch the comment?"
    date: 2003-12-24
    body: "> Kick some UserLinux Ass.\n\nLMAO\n\nOkay! Quanta plus is another application they won't have (and we are also raising funds for a meeting of Eric and Andras in the coming months). We're looking forward to being one of the most requested apps in the \"You mean I can't get ____(Quanta Plus) on UserLinux? Can you recommend a better distro?\" question. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Catch the comment?"
    date: 2003-12-24
    body: "Eric, we *know* what we owe you. \n\nMeeting you would be nice, too. Will you to Fosdem 04 in Brussels? Or are there other EU events where we can meet you?"
    author: "Gala Diener"
  - subject: "Re: Catch the comment?"
    date: 2003-12-25
    body: "> Eric, we *know* what we owe you. \n\nUh-Oh! Hey I've been working out so I'm ready... oh, this must be one of the \"all you can drink\" beer offers. ;-)\n \n> Meeting you would be nice, too. Will you to Fosdem 04 in Brussels? Or are there other EU events where we can meet you?\n\nWhen is it? I have several factors that have changed affecting 2004 as compared to the last two years. For one my business is doing very good now so I can consider the travel expense... but the other is even more fun. Because Quanta is reaching a rather impressive stage in it's development I think there will be interest in presentations of it, as well as joining with other KDE presentations. It's customary to cover travel expense for speakers and as it happens I have a background in training and public speaking and really enjoy it. I'm looking forward to travelling to Europe and other places and meeting Quanta users. I can't think of anything more fun.\n\nI will definitely be at the KDE conference in August. I may have an invite to LinuxTag in July and I'm submitting papers for several PHP events in 2004. I'm also very interested in any events like this that I may not yet be aware of feel free to email me. \n\nIt's possible that I may make 3 or 4 trips to Europe in 2004. Maybe more. If you are on the Quanta user list I will be posting there for any trips I make and if the Dot editors are interested there too. I look forward to meeting you.\n"
    author: "Eric Laffoon"
  - subject: "Re: Catch the comment?"
    date: 2003-12-25
    body: "Cogitate quantis laboribus fundatum imperium, quanta virtute stabilitam libertatem, quanta deorum benignitate auctas exaggeratasque fortunas, una nox paene delerit. \n                                               Cicero\n\nLinuxTag, that's nice..\n\n\n\nFosdem 2004\nhttp://www.fosdem.org/"
    author: "Andy"
  - subject: "Re: Catch the comment?"
    date: 2003-12-26
    body: "Something I've been thinking of for a couple of days now...\n\nUserlinux sounds all, well...., how can I say..., pretty windows 98. And does not include what they actually want to do (heh heh heh).\n\nHow's about creating a Business Unix (Biznix) distribution? Based on Debian, why not, including GTK libraries, naturally, but having KDE as it's desktop, since:\n\n- it is more user friendly\n- it has better customization options for admins\n- it bases on the QT libraries which are cross-platform (did I miss GTKopia?)\n- QT is already supported by killerapp designer Adobe and others (if you are reading this, where is QTShop?).\n- It just looks better. Don't forget users want looks. That is even the reason M$ painted there start button green in XP.\n- QT looks good when you develop for windows as well. GTK solutions on windows always have this: designed for an 'old fashioned system' kinda look. Where QT just looks good. \n- Businesses don't mind paying for software as long as it is supported.\n- Businesses want supported software.\n- QT is.\n\nNuff said.\n\nWhat we need is a main Linux person. One everybody knows. It should be easy to get one, since KDE is the preferred desktop if you look at the linux journal lists.\n\nAny volunteers here?\n\nJord"
    author: "Jord"
  - subject: "Re: Catch the comment?"
    date: 2003-12-26
    body: "volunteers? how about this: there are people already working on this! check out the kde-debian project, for one ... the kde-debian@kde.org list should show up on lists.kde.org shortly, though it's been active for probably a month or so now.."
    author: "Aaron J. Seigo"
  - subject: "wow!"
    date: 2003-12-24
    body: "it's the holiday season, so it seems to be a good time to say this, and this article is about supporting KDE, so it seems to be a good place to say this: \n\nhere's a great big thank-you from me to everyone who contributes to KDE, be it with their blood, sweat and tears or with their pocketbook. i'm looking forward to an AWESOME 2004, and looking back on a thoroughly enjoyable 2003.\n\ngo KDE! =)"
    author: "Aaron J. Seigo"
  - subject: "Penpal"
    date: 2003-12-24
    body: "Your donations system seems to be hidden. Why don't you publish your bank account.  Why don't you use Penpal? Less than 10 000 EUR, this sounds riddiculous to me."
    author: "Gerd"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "You posted because: \n\na) You want to donate 10.000 euros and want to know more?\nb) You have evidence in other direction?\nc) You are amazed how few people give back?\n\nOr...  \n\nd) You are a troll!\n\nHmmmmmmmmmmm....."
    author: "cwoelz"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "> a) You want to donate 10.000 euros and want to know more?\n\nIn that case you are lucky because thanks to the new EU regulations it is now possible to transfer up to 12.500 euros for the price of a domestic transfer.\n\nYour bank can tell you more.\n\n> d) You are a troll!\n\nTsk! No need to be unfriendly."
    author: "Waldo Bastian"
  - subject: "Re: Paypal"
    date: 2003-12-25
    body: "You should definitely set up a Paypal account for donations, and consider also offering subscription/recurring type payments.\n\nThis makes it easy for the many US users who have paypal, and also many non-EU people who have paypal to make donations.\n\nPersonally I try to make giving me money the path of least resistance.\nWith a \"donate $10\" button or a \"donate $ per month\" button it's pretty damn easy. \n\nIt is a very slick system. (until you have problems that is)\n\n"
    author: "Simon"
  - subject: "Re: Paypal"
    date: 2003-12-25
    body: "\nDid you read the story? If so, did you follow the links? If so, did you see this one?\n\n-----> http://www.kde.org/support/support.php <----\n\nIf I am not mistaken, this has a directly accessible paypal form on it, no?\n\nCheers, \nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: Paypal"
    date: 2003-12-27
    body: "I like the idea of recurring payments.. I signed up for the WineX distribution cuz i wanted to play DX games... and they keep charging me $5 every month.. and i keep letting them do it cuz im too lazy to cancel a mere $5 a month... i'd much rather that $5 go to KDE... i already do annual $20 donations (hey at least its something!), and i use KDE 100% of the time (even using KDE on my powerbook laptop as i write this cuz im getting sick of Quartz)"
    author: "dr.gonzo"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "On <a href=\"http://www.kde.org/areas/kde-ev/donations.php\">\nhttp://www.kde.org/areas/kde-ev/donations.php</a> you will also find the bank account details. Since July 2003 the fees for bank-transfers within the EU (Euro-zone) have been dramatically lowered for transactions based on the IBAN number so this is now indeed a viable option as well. \n<p>\nSee <a href=\"http://www.europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.gettxt=gt&doc=MEMO/03/140|0|RAPID&lg=EN\">this FAQ</a> on cross-border money transfers in euros within the EU.\n"
    author: "Waldo Bastian"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "Can I become a member of KDE e.V.? How much is the membership fee per year?"
    author: "Andy"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "If you're a significant KDE contributor you can apply for membership. There is no fee at the moment."
    author: "Anonymous"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "You can also become a \"supporting member\", thereby donating a fee of your preference. Note that supporting members have no voting rights, though."
    author: "Anonymous"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "No, the currently active bylaws don't allow that."
    author: "Anonymous"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "Maybe a club like mandrake-club, but what are the advantages for members? Maybe if the new \"KDE-Distro\" is out. I take two :))"
    author: "Marc Tespe"
  - subject: "Re: Penpal"
    date: 2003-12-24
    body: "A real KDE user club can be a better representative of user interest than an elite corps like the FSF Europe that avoids broad membership. A rather open community , a user club with promoters, users and contributors of KDE can also be a good representative at the European level, e.G. in the case of software patents (the decision is not through yet). No technology focussed hacker2hacker org like the LUGs, but a community, with a social oriented approach."
    author: "Gala Diener"
  - subject: "\"Donation\""
    date: 2003-12-24
    body: "It is nice to see that we are receiving donations.\n\nIt would also be nice if Bill G. would \"donate\" (i.e. Court ordered damages) about $0.5G (half a [US] Billion). :-)\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"Donation\""
    date: 2003-12-24
    body: "Perhaps you could request governmental donations/indirect funding. Gov paid usability tests, migration guides ecc.\n\n\nAnother idea would be to create a kind of GPL ASP Shareware system: Ask users to contribute, either in the form of money or code. Place a link to the contributions site in the KDE download section. "
    author: "possi"
  - subject: "Re: \"Donation\""
    date: 2003-12-25
    body: "> Perhaps you could request governmental donations/indirect funding. Gov paid \n> usability tests, migration guides ecc.\n\nI think that (in the US) Government funding should be limited to the internet.\n\nHowever, we do already have Government paid usability tests -- the procurement process.\n\nSome Government departments are using Open Source Software, does this constitute indirect funding?  \n\nProper Government mandated standards for internet software used in interstate commerce would also help -- perhaps more than actual funding.\n\n--\nJRT  "
    author: "James Richard Tyrer"
  - subject: "Re: \"Donation\""
    date: 2003-12-25
    body: "As Dr. Held, BMI\n\n"
    author: "Andy"
  - subject: "Re: \"Donation\""
    date: 2003-12-25
    body: "A proper support of EU minority language as declared in the Charter of Regional and Minority languages might receive Gov funding as well and makes KDE adoption in Govs easier as a fulfillment of the language diversity requirement gives a competitive advantage.\n\nI once read an article at the EU level where a move to Linux was rejected by an EU official with the argument that too few languages were supported which is in fact not true. But: At the European level you could spread the news about the language diversity KDE has reached -- and the possibilities for languages diversity by FOSS.\n\nAndre, FFII\nhttp://www.ffii.org\n\nContact the responsible secretariat \nor read the text of the charter here:\nhttp://www.coe.int/T/E/Legal_Affairs/Local_and_regional_Democracy/Regional_or_Minority_languages/"
    author: "Andre, FFII"
  - subject: "Re: \"Donation\""
    date: 2003-12-25
    body: ">It would also be nice if Bill G. would \"donate\" \n\nHe did.\n Why do you think that charges were basically dropped."
    author: "a.c."
  - subject: "Abiword"
    date: 2003-12-24
    body: "http://www.abiword.com/information/news/2003/awn175.phtml\n\nFrom Abiword Weekly(!) news:\n\n\"Unique contributors: 61* \npaypal: 66 \ncheck :  3 \ncash  :  1 \n*61: plusse 3 repeat offenders whom we love dearly \n\nContributions: $2311.84 \nLess paypal and other banking fees: $167.64 \"\n\n\nWell, now we know why Userlinux chose Gnome. Gnome is the money path."
    author: "Gnometroll"
  - subject: "Re: Abiword"
    date: 2003-12-25
    body: "Those contributations are not just for the week, but rather overall."
    author: "Rayiner Hashem"
  - subject: "Re: Abiword"
    date: 2003-12-25
    body: "Abiword is not a Gnome-application"
    author: "rinse"
  - subject: "Re: Abiword"
    date: 2003-12-25
    body: "I think the phrase you're searching for it \"Abiword is not just a GNOME-application\"."
    author: "Yellowboy"
  - subject: "Re: Abiword"
    date: 2003-12-25
    body: "You mean just like \"Scribus is not just a KDE-application\"?"
    author: "Datschge"
  - subject: "Re: Abiword"
    date: 2003-12-26
    body: "Well actually it is \"Abiword is just not a GNOME-application\"\nI assume that an application should use Gnome-libraries in order to be a Gnome-application."
    author: "rinse"
  - subject: "Re: Abiword"
    date: 2003-12-26
    body: "I believe if you looked at Abiword, there is an --enable-gnome configure option, that checks for bonobo, the gnome-libs, nautilus, and gnome-print. \n\nIs that enough for you?\nAbiword has many targets, you can build a plain GTK version or a GNOME version."
    author: "Yellowboy"
  - subject: "Re: Abiword"
    date: 2003-12-26
    body: "So its a Linux application that has the possibility\nof integrating with gnome."
    author: "OI"
  - subject: "Re: Abiword"
    date: 2004-01-13
    body: "Eeep!\n\nIt is also a\n\n* QNX \n* Windows\n* MacOS X (ok, port not official yet)\n\napplication.\nI belive it could also be a QT/KDE app if somone ported it..."
    author: "monkeyboy"
  - subject: "Re: Abiword"
    date: 2004-01-29
    body: "You say it checks for bonobo: does it make itself a component and/or a container? Nothing to do with the thread, just a question."
    author: "Roberto Alsina"
  - subject: "Re: Abiword"
    date: 2004-01-29
    body: "why you are so looser?\n\nabiword it's a great application!"
    author: "me"
  - subject: "Thank you!"
    date: 2003-12-25
    body: "Thank you, whoever you are, that is very kind and greately appreciated. I too donate to KDE, but I'm unemployed and so can not donate that much."
    author: "Alex"
  - subject: "Why ?"
    date: 2003-12-25
    body: "Why $ why not ? ?"
    author: "NoName"
  - subject: "Missing the Point"
    date: 2003-12-26
    body: "KDE really doesn't need to change much.  It does not need to 'get organized' any more than it is already.  I see many comments saying 'KDE needs to do this to beat gnome' and such.\n\nKDE has already accomplished so much without such hoopla - why would we want to change a system that is obviously working.\n\nKDE e.V. is a small but important part of what makes KDE happen and contributions there help pay for our most expensive and most required commodity: bandwidth.\n\nIf you have bandwidth and no cash, we'll take that too :)  If you can donate neither -- the developers do appreciate 'thanks' just the same.  Try the #kde* on irc.kde.org, or the mailing lists.\n\nEverytime an article such as this goes up, the politicking starts from the peanut gallery.  Perhaps try to instead contribute in a constructive fashion than the bickering that only degrades the otherwise super-friendly atmosphere that is KDE.\n\nrant/\n\nTroy Unrau\n( read my blog if you're bored.  http://tblog.ath.cx/troy )"
    author: "Troy Unrau"
  - subject: "Re: Missing the Point"
    date: 2003-12-26
    body: "> read my blog if you're bored. http://tblog.ath.cx/troy\n\nhrm. can i read it if i'm not bored? ;-P"
    author: "Aaron J. Seigo"
  - subject: "Re: Missing the Point"
    date: 2003-12-28
    body: "Okay :P"
    author: "Troy Unrau"
---
KDE received a pleasant surprise this week when an anonymous well-wisher
<a href="http://www.kde.org/support/donations.php">donated $1000 to KDE e.V.</a>
On behalf of everyone in the KDE community we would hereby like to thank our generous friend.
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> uses donations to maintain the various KDE servers, to reimburse travel expenses for KDE related activities of KDE developers and for other activities that help to advance the goals of KDE. If you too wish to show your appreciation for the KDE project by making a donation you can use
<a href="http://www.kde.org/areas/kde-ev/donations.php">the donation form on our website</a>. Happy Holidays!
<!--break-->
