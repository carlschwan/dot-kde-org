---
title: "Seven Years of KDE!"
date:    2003-10-20
authors:
  - "thibs"
slug:    seven-years-kde
comments:
  - subject: "Seven Wonderful Years"
    date: 2003-10-20
    body: "Just want to say thanks to all who have made KDE what it is today, an awesome desktop.\n\nMany thanks!"
    author: "jpaqman"
  - subject: "Me too"
    date: 2003-10-20
    body: "Thank you, and for only this long KDE has certaubly grown incredibly, especially recently."
    author: "Patrick"
  - subject: "Second that"
    date: 2003-10-20
    body: "Yay for KDE! Thanks so very much everyone!"
    author: "Rayiner Hashem"
  - subject: "Woohoo!"
    date: 2003-10-20
    body: "Thanks to everyone who's worked on KDE. It's amazing the accomplishments in such a short time!\n\nBTW, where's the cake?"
    author: "Timothy R. Butler"
  - subject: "Great.."
    date: 2003-10-20
    body: "We've come so far, yet have so far to go. \n\nGo KDE!"
    author: "anon"
  - subject: "So great!"
    date: 2003-10-20
    body: "Just do it!"
    author: "Pieere Pi"
  - subject: "InKredible!"
    date: 2003-10-20
    body: "Aye Karamba!\n\nLet's hope for another successful seven years :o)."
    author: "Eike Hein"
  - subject: "w00t"
    date: 2003-10-20
    body: "now i finally now where the K is from...\nthanks kde for all those great years!!\n"
    author: "Mark Hannessen"
  - subject: "Re: w00t"
    date: 2003-10-20
    body: "yeah, so it should actually be cde\nanyway, beginning almost every app with a K is uncool with a C\n\nthanks for kde anyway"
    author: "ac"
  - subject: "Re: w00t"
    date: 2003-10-21
    body: "AFAIK CDE was the common desktop environment and ran on UNIX(TM), so KDE was called the *Kool* Desktop Environment to make fun of this. (I think this is somewhere on KDE myths). As KDE gathered life the Kool was shortened to K since it looked silly. (Personally i prefer Kool.)\n\nP.S. I love the app-naming konvetion :)."
    author: "Konrad D."
  - subject: "Re: w00t"
    date: 2003-10-22
    body: "I guess I'm an `ak` poster around here then ;)"
    author: "ac"
  - subject: "happy birthday and thank you"
    date: 2003-10-20
    body: "i ve just donated 50\u0080 Euro to kde e.v. as an birthday present!!!\n\n"
    author: "gunnar"
  - subject: "Happy Birthday, KDE"
    date: 2003-10-20
    body: "  You all are doing excellent work.  KDE is my favorite desktop of all time.  I even prefer it over Apple's OS X desktop.  Please keep up the great job you are doing."
    author: "David Parks"
  - subject: "Quote "
    date: 2003-10-20
    body: "I like this quote from Matthias Ettrich original Announcment. \n\"I admit the whole thing sounds a bit like fantasy.\"\nLooks like KDE is quite real after 7 Years. \nKeep up the good work!"
    author: "Calle"
  - subject: "Happy Birthday KDE"
    date: 2003-10-20
    body: "Congratulations to all those that have made KDE so amazingly successful !"
    author: "Nassos Koyrendas"
  - subject: "Kool!"
    date: 2003-10-20
    body: "Happy Birthday, KDE! A great effort.\n\nP.S. Can you say \"person-years\"? I know you can!\n"
    author: "Jonathan Smith"
  - subject: "is about to reach version 3.2"
    date: 2003-10-20
    body: "<i>...is about to reach version 3.2....</i> \n\nCan you please tell us when?!\n\nBTW, nice job and thanx!\n;)\n"
    author: "step"
  - subject: "Re: is about to reach version 3.2"
    date: 2003-10-20
    body: "Probably sometime in december. \n\nBeta1 real soon now. "
    author: "anon"
  - subject: "Happy Birthday!"
    date: 2003-10-20
    body: "Happy Birthday!\n\n"
    author: "Tackat"
  - subject: "Gefeliciteerd!"
    date: 2003-10-20
    body: "Thx to all kde ppl, for making a great piece of software!"
    author: "Dion Mes"
  - subject: "Thanks for so much goodies KDE ;)"
    date: 2003-10-20
    body: "Thanks to KDE, integration with file types and other applications becomes a piece of cake!\n\nAnd Konqueror has the most excellent HTML/CSS rendering :--)\n\n\nDarkelve"
    author: "Darkelve"
  - subject: "Re: Thanks for so much goodies KDE ;)"
    date: 2003-10-21
    body: "kake"
    author: "Gerry"
  - subject: "KDE Rules!"
    date: 2003-10-20
    body: "KDE Rules.  It is a superior desktop to anything I have seen (may be with the exception of two: Amiga and Max OSX).  KDE needs more documentation and it is ready to top the world."
    author: "tom"
  - subject: "Re: KDE Rules!"
    date: 2003-10-20
    body: "Happy Birthday!! I was a long time advocate of the \"simple\" Window Managers, but KDE has bacome my all time favourite!! :-)"
    author: "John"
  - subject: "Well what's more to say..."
    date: 2003-10-21
    body: "Happy Birthday! :)"
    author: "jadrian"
  - subject: "this is the path to choose"
    date: 2003-10-21
    body: "...and we will prevail.\nanyways, happy birthday to KDE, this is a great step in history. And I've looked at the post that started it all, well, it quite got what it should. I am using KDE 3.1.4. (for everydaystuff) on my freshly compiled Gentoo together with Gnome 2.4. (adminning) and it is beautiful, hell fast, and it rocks XP right out of the box. Gee, phun came back to computers, thank you KDE guys and have a good 3.2 release :)\nregards, Marcel"
    author: "Marcel Partap"
  - subject: "Awesome Work Guys"
    date: 2003-10-21
    body: "Way to go to the KDE team.\n\nFabulous job."
    author: "Gregory Carter"
  - subject: "Wow...."
    date: 2003-10-21
    body: "Rules! :-)"
    author: "Eugene"
  - subject: "Happy Birthday KDE!"
    date: 2003-10-21
    body: "Many, many thanks to Matthias & KDE contributors for a truly awesome desktop environment that just keeps getting better!  :-)"
    author: "Tom Hayward"
  - subject: "congrats"
    date: 2003-10-21
    body: "Good job guys! Let's see KDE strong for another 7 years...\n\nAnd to think, I use to think GNOME was the best desktop around :P"
    author: "nite"
  - subject: "\u00a1Muchas felicitaciones!!"
    date: 2003-10-21
    body: "\u00a1KDE es mi aplicaci\u00f3n desktop favorita!!\n\n\u00a1Fel\u00edz cumplea\u00f1os, KDE!\n\nHappy birthday!\n\nGratulere med dagen!\n\n\n"
    author: "R.A.P.P."
  - subject: "Cheers!"
    date: 2003-10-22
    body: "cheers!\nhappy burthday!"
    author: "Georgi"
  - subject: "Congratulations!"
    date: 2003-10-22
    body: "I've been using it since 1.0beta2. It sucked back then, but it sucked less than the other desktop environments available. The reason I started using it was that I came from a windows environment and I wanted the basics to behave like in windows. For example, I was used to using ALT+F4 to close a window and I didn't want to learn another shortcut just because some project descided aother shortcut was more logical.\n\nToday KDE is awesome and still provides an easy way for newbies that come from MacOS or Windows to start using Linux and it gives a lot of power to experienced users. \n\nKDE developers: THANK YOU!"
    author: "KW"
  - subject: "Obrigado programadores do kde"
    date: 2003-10-23
    body: "Actualmente na minha casa, toda a gente usa o linux, e o desktop \u00e9 kde. Uma prova viva de que o kde est\u00e1 cada vez mais perto do sonho de se tornar um desktop de massas. Pareb\u00e9ns a todos e muito obrigado!"
    author: "Daniel Silva"
  - subject: "Happy Birthday"
    date: 2003-10-23
    body: "... and a great Kongratulation to the whole developer group AND the faithful and supportive user base. Way to go people! "
    author: "Vajsravana"
  - subject: "Happy Birthday!"
    date: 2003-10-23
    body: "Thanks to everyone who involved in this project, it is simply the best desktop environment. Keep up the good work!"
    author: "Happy KDE User"
  - subject: "thanks"
    date: 2003-10-25
    body: "And the thing that makes me happier is that it's getting better and better with age. Kde 3.2 will be great, and I can't even imagine how good KDE will get when it reaches it's 10th anniversary.\n\nMaybe we can get a special version then? :)"
    author: "protoman"
  - subject: "Re: thanks"
    date: 2003-10-25
    body: "Good Work!"
    author: "Jooram"
  - subject: "Happy birthday,KDE."
    date: 2003-10-25
    body: "A marvelous work.Thanks to all contributors."
    author: "Liu Xiao Gang"
  - subject: "Happy birthday,KDE."
    date: 2003-10-25
    body: "A marvelous work.Thanks to all contributors."
    author: "Liu Xiao Gang"
  - subject: "Great as Miracle!"
    date: 2003-10-28
    body: "Thank you for this great work and thank you for your open minds,\nall people around the world can use KDE equally and it is really wonderfull :)"
    author: "Davoud Seyedin"
  - subject: "Thanks"
    date: 2003-10-29
    body: "I must also throw my two cents into the ring.  To all those who have worked on making KDE as good as it is, THANKS!  You've done a fine job."
    author: "Howard Coles Jr."
  - subject: "Hurra for KDE!!!"
    date: 2003-10-29
    body: "I've been using KDE from 1.x on ...and i have to say it's getting better and better :D\n\nI'd like to thank all the devs (old and new) and express my great love and gratitude towards KDE!!!\n\nKeep up the great work, guys (and gals!)"
    author: "Matija \"hook\" Suklje"
---
In this <a href="http://www.linuxfrench.net/article.php?id_article=1319">article</a> (french), <a href="http://www.linuxfrench.net">LinuxFrench</a> reveals that 7 years ago, October 14th 1996 to be exact, <a href="http://people.kde.org/matthias.html">Matthias Ettrich</a> <a href="http://groups.google.com/groups?q=g:thl2630113795d&dq=&hl=en&lr=&ie=UTF-8&oe=UTF-8&selm=53tkvv%24b4j%40newsserv.zdv.uni-tuebingen.de">announced and detailed a new project</a> that was in search of contributers.  The project was temporarily dubbed the Kool Desktop Environment, later to simply become the K Desktop Environment.  Of course, 7 years later and KDE is about to reach version 3.2, consists of 4.5 millions lines of code and represents the effort of nearly 1400 man-years. So <i>Happy Birthday KDE</i>, and long live Konqi!
<!--break-->
