---
title: "Checking Hotmail with KMail/Gotmail"
date:    2003-06-23
authors:
  - "Fab"
slug:    checking-hotmail-kmailgotmail
comments:
  - subject: "Don't forget hotwayd"
    date: 2003-06-23
    body: "I tried to configure gotmail a couple of months ago and i wasn't that lucky... so i tried hotwayd (http://hotwayd.sourceforge.net/) and it simply rulez :) works great for me with kmail!"
    author: "aPT-DRiNK"
  - subject: "Re: Don't forget hotwayd"
    date: 2003-06-23
    body: "Please share your experiences with us ... I would be more than happy to translate it and do some proofreading\n\nSeriously ... contact me if you want ... \n\nFab\n\n"
    author: "Fab"
  - subject: "Re: Don't forget hotwayd"
    date: 2003-06-23
    body: "The README file that comes with the package explains quite simply how to get the thing working... and also does the installation guide at http://hotwayd.sourceforge.net/install/#inetd\n\nI can't explain it any better, is quite easy. Once you get it working, you have a local pop server running, so kmail asks for mail there... and voil\u00e1 ;)\n\nIf you have any problems with the guide, maybe i can help you... just shout!! :P"
    author: "aPT-DRiNK"
  - subject: "Re: Don't forget hotwayd"
    date: 2004-02-02
    body: "Hi,\n\nI'm a new MAC user and i've been trying to get hotwayd running, but i honestly have no clue as to how to do it? (which type of file do i need? rpm? or something?)could you tell me what programs i need to use? and perhaps the simplest way to do it? or does someone have a .dmg of it? I used to use hotpopper for PC so I wanted something simple like that...\nThanks...\n\n\nKenny."
    author: "Kenny"
  - subject: "Re: Don't forget hotwayd"
    date: 2003-06-28
    body: "I followed your instructions exactly, well almost exactly.  I installed gotmail with 'apt-get install gotmail' :).  Anyway, it worked perfectly with KMail from KDE 3.1.2 (in debian sid).  Took me all of 2min to setup.  Thanks a lot though, now I dont have to deal with logging in and evreything.  I can just use with my Hotmail account like my other regular email accounts (imap and pop).\n"
    author: "SupetPET Troll"
  - subject: "Re: Don't forget hotwayd"
    date: 2003-06-24
    body: "I also use hotway.  In Debian, I just apt-get install.  In other distros, I assume setup is just as easy as installing the package.  It sets itself as a pop3 server, so configuring mail clients is a matter of typing the address of the machine using hotway (i.e. Configure Kmail->Network->Receiving->Add->POP3->host:HotwayBox).  And that is the advantage hotway has over gotmail--install hotway on one box and all computers with pop3-friendly mail clients on your network can use that box to check Hotmail/MSN mail.\n\n(Debian description)\nHotway acts like a pop3 server, but actually goes to hotmail.com\n to retrieve requested mail. It uses Hotmail's undocumented HTTPmail\n interface (as does Outlook(TM)). Works with both hotmail.com and\n msn.com addresses."
    author: "A C"
  - subject: "Re: Don't forget hotwayd"
    date: 2003-06-26
    body: "I just installed hotwayd and it works flawlessly! took me about 5 min. from download to recieving e-mail from the hotmail server. thank you very much for posting the link, I have been wondering how to do this for a while.\n\n\nLance D>"
    author: "madpuppy"
  - subject: "Re: Don't forget hotwayd"
    date: 2004-08-30
    body: "I have a problem with hotwayd on debian. I doesn't mark the messages as read. How can you change it? i have it installed with synaptic"
    author: "Nathan"
  - subject: "Re: Don't forget hotwayd"
    date: 2004-12-20
    body: "Genius, utter genius.  I'm not the most knowledgable Linux user, was expecting it to be a nightmare.  3 minutes, I estimate, and it just works perfectly! :)"
    author: "jon"
  - subject: "Re: Don't forget hotwayd"
    date: 2005-12-28
    body: "hey i cant find an installation for hotwayd and if i do it is in a \".gz\" format and my computer doesnt recognise it. please can you help me out. i am wanting to use it for my hotmail account."
    author: "Ahmed"
  - subject: "Re: Don't forget hotwayd"
    date: 2006-08-04
    body: "I cannot get Hotway working in Thunderbird. How must I do that?\n\nThanks\nJaap"
    author: "Jaap"
  - subject: "Re: Don't forget hotwayd"
    date: 2008-03-06
    body: "Today (6 march 2008) i made a easy install of hotway hotsmtpd with apt-get in debian etch. works great with kmail."
    author: "Luca Cuzzolin"
  - subject: "web mail"
    date: 2003-06-23
    body: "I prefer Linuxmail.org as a web mail provider.\n\nFast, easy registration, but I don't know whether you can fetch your mails."
    author: "Linuxmail"
  - subject: "Re: web mail"
    date: 2003-06-24
    body: "> Fast, easy registration, but I don't know whether you can fetch your mails.\n\nI opened an account, it seems to work well. But you have to pay to use IMAP/POP3 and SMTP (US$ 20 a year). Not really expensive. Any solution alternative to a microsoft solution (like hotmail) is welcome. I do not microsoft products at all, so hotmail is out of the question for me.\n\nPersonally, as a secondary email server (apart from my employer's)  I use gmx.net - Excellent service, German only, but heck, many servers are English only ;-)"
    author: "MandrakeUser"
  - subject: "Re: web mail"
    date: 2003-06-25
    body: "They used to have English and Turkish service as well (www.gmx.co.uk and www.gmx.com.tr), but now those are German as well..."
    author: "Olaf Jan Schmidt"
  - subject: "Re: web mail"
    date: 2003-06-25
    body: "https://www.myrealbox.com/\n10 MB, POP3, IMAP4, SMTP, webmail, - all that over secure transport too, serverside rules, antivirus, n+1 other things I forgot."
    author: "Tar"
  - subject: "Re: web mail"
    date: 2003-06-26
    body: "Thanks for the hint. It looks quite good actually. The only thing I'm concerned about is their uptimes... today I saw that they have an uptime of 10 hours... well, that's not too much I would say. How reliable are they usually?"
    author: "uga"
  - subject: "Articles like this are good..."
    date: 2003-06-23
    body: "But wouldn't it be a good idea to group these on a \"KDE-howto\" site or something?\nThese articles help out people right now, but after a while, people forget about them, so... If they would all be put together, people would have one place for looking for kde-related tut's/howto's"
    author: "mike"
  - subject: "Re: Articles like this are good..."
    date: 2003-06-23
    body: "Yes I guess that's a good idea. I think it will need someone who is willing to put some time in a website like that. But it would definitely be a very good thing to collect all of these howtos ...  "
    author: "Fab"
  - subject: "Re: Articles like this are good..."
    date: 2003-06-24
    body: "All howtos that explain how to do something special with KMail and that we, the KMail developers, are aware of are listed on kmail.kde.org. I will make sure that this nice howto will be added to this list.\n\nBut I have some questions/suggestions:\n1. Why does the mailbox format of the new folder need to be changed to mbox? I don't think that's necessary at all, unless the folder is located on a Windows partition where maildir doesn't work because in maildir format some filenames include colons. In all other cases always maildir should be used because it's much more robust than mbox.\n\n2. With regard to locking, please see http://docs.kde.org/en/3.1/kdenetwork/kmail/faq.html#id2839877. Did you check that FCNTL locking really works? I lost a few messages because FCNTL-locking didn't work with fetchmail. Anyway, as FCNTL is very problematic on NFS mounted devices I recommend using procmail with gotmail because procmail lockfile locking does definitely always work.\n\n3. The precommand \"~/gotmail-0.7.9/gotmail -c\" doesn't look right because according to the man page, the usage information and the actual code (of gotmail 0.7.10) the \"-c\" option is used to specify a different configuration file than the default one and doesn't stand for check."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Articles like this are good..."
    date: 2003-06-24
    body: "Very cool,\n\nI didnt realise you could integrate it inot kmail like that ... i have used it going through my other email address etc .....\n\nQuite right about the -c on gotmail here is the help option with all the args ... see below\n\nCheers\nCoomsie :3)\n\n\n[coomsie@iain coomsie]$ ./.gotmail/gotmail --help\nGotmail v0.7.10    Copyright (C) 2000-2003 Peter Hawkins\nGotmail comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it\nunder certain conditions; see the file COPYING for details.\n \nUsage:\ngotmail [OPTIONS...]\n \nOptions:\n  -?, --help, --usage       Display this screen\n  --version                 Display version information\n  -c, --config-file <file>  Specify config file (default ~/.gotmailrc)\n  -u, --username <name>     Specify your hotmail username (REQUIRED)\n  -p, --password <pass>     Specify your hotmail password (REQUIRED)\n  -d, --domain <domain>     Specify hotmail.com (default) or msn.com\n  --proxy <host:port>       Specify an HTTP proxy to use. Format is\n                              host:port - eg: localhost:3128\n  --proxy-auth <user:pass>  Specify authentification details for the\n                              HTTP proxy.\n  -s, --smtpserver <server> Specify SMTP server. Will not use sendmail\n  -f, --forward <address>   Specify an email address to forward to. If a\n                              forwarding address is not given, messages\n                              will be saved to disk\n  --folders \"folders\"       Only get these folders (list of folders in\n                              quotes, i.e.: \"Inbox, Bulk Mail\")\n  --folder-dir /my/dir      Download messages into this directory\n  --only-new                Only unread messages will be retrieved\n  --mark-read               Messages will be marked as read once downloaded\n  --delete                  Messages will be deleted after download\n  --retry-limit max_tries   Maximum number of attempts to download a message\n  --speed-limit             Throttle back rate of giving messages to sendmail\n  --save-to-login           Save to folder-dir/username for Inbox and\n                              /folder-dir/username-foldername for others\n  --use-procmail            Send all messages only to procmail\n  --procmail-bin <path>     Use this program as procmail (default is\n                              /usr/bin/procmail) (implies --use-procmail)\n  --curl-bin <path>         Specify the path to the cURL program if it's\n                              not in your path.\n  --silent                  Do not print messages\n  -v, --verbose             Verbosely print messages\n  --debug                   Print debug output\n"
    author: "Coomsie"
  - subject: "Questions???"
    date: 2004-11-04
    body: "Helo\n\nHow to can authentification whit username and password the Proxy in the Konqueror Browser"
    author: "geovanni"
  - subject: "Re: Articles like this are good..."
    date: 2003-06-27
    body: "i'm pretty sure that the mbox option is neccesary because that is the format gotmail stores the mail in... not positive on that one though. "
    author: "standsolid"
  - subject: "w00t!!"
    date: 2003-06-24
    body: "W00t! alright! I got this to work flawlessly! Thaxs for a wonderful HOWTO!"
    author: "radioact1ve"
  - subject: "Inbox translation"
    date: 2003-06-24
    body: "It's not mentioned in the howto, but you must use the translated version off \"Inbox\" in the gotmailrc eg: in french it's \"Bo\u00eete de r\u00e9ception\"\nI don't know why they don't use generic folder names.\n.gotmailrc :\nfolders=Bo\u00eete de r\u00e9ception\nKmail configuration (Local mailbox):\n/home/user/hotmail/Bo\u00eete de r\u00e9ception\n"
    author: "Cyril"
  - subject: "Re: Inbox translation"
    date: 2003-06-24
    body: "Yes we noticed this as well, we used a Dutch Hotmail account. But we didn't check the possible \"inbox\" names for French. I'm glad you are mentioning this...\n\nAlso the feedback given on dot.kde.org will result in an updated version of the document. We are looking through some suggestions and errors and will post a corrected version soon.\n\nCheers'\n\nFab"
    author: "Fab"
  - subject: "OT"
    date: 2003-06-24
    body: "Include kaffeine instead noatun in the next release of KDE ...\nhttp://members.chello.at/kaffeine/"
    author: "anonymous ..."
  - subject: "Re: OT"
    date: 2003-06-24
    body: "I agree with you! Seems to have a much better interface for watching films than the default one of Noatun. Similarly, Kplayer (http://kplayer.sourceforge.net) has a nice interface too. In contrast to Kaffeine, which is based on xine, it uses mplayer instead. \nHowever, Noatuns interface is probably better for listening to music at least when using skins (winamp or k-jofol skins)."
    author: "tuxo"
  - subject: "Re: OT"
    date: 2003-06-25
    body: "What's your gripe with noatun? I'd support feature additions, upgrades and modifications but not a replacement. I wish noatun never be replaced but improved upon. I simply love it."
    author: "Mystilleef"
  - subject: "Re: OT"
    date: 2003-06-25
    body: "I personally don't like noatun because:\n\n1. relies on arts.. very strongly in fact. it's basically a shell for arts as konqueror is a shell for kparts. \n\n..and arts only works on my hardware (PPC G4 @ 533) half of the time.. in one KDE release, it works, and in another it doesn't. \n\n2. It doesn't ship with a usable playlist. hayes is a usable playlist. Please include it in noatun!!"
    author: "mif"
  - subject: "Re: OT"
    date: 2003-06-25
    body: "Emmm...you got me all confused. First, noatun ships with a useable playlist and I think it's called Hayes. Secondly, aRts is probably the most powerful sound server/tool available in the *nix sphere. In fact, the Gnome team was thinking about porting it to Gnome. \n\nI don't know what distro or package manager you have on your system, but if you installed and configured aRts properly, it should work 100% of the time like it's supposed to and like it does all the systems I've installed KDE on. \n\nIf noatun didn't ship with a useable playlist, then how on earth do you think users listen to their favorite music files on their file system? :?  3/4 of my multimedia applications rely on aRts, so I'm a little perplexed as to why you see aRts as a hindrance. :?\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: OT"
    date: 2003-06-25
    body: "> What's your gripe with noatun?\nThe interface! Compare e.g. Noatuns \"Excellent\" or the \"Milk-Chocolate\" interfaces with the one of Kaffeine or the one of Kplayer! The latter two are just more usuable than Noatuns interfaces, due to the better placement of the widgets (better layout) as well as nicer widgets (better design).\nFurthermore, Noatun has less features than e.g. Kaffeine (streaming,playlist)."
    author: "tuxo"
  - subject: "fetchyahoo for yahoos"
    date: 2003-06-25
    body: "I do the same with yahoo mail with fetchyahoo which is a perl\nscript.\n\njust put fetchyahoo in precommand.\n\nI do something like rxvt -e fetchyahoo \nso that I can see the progress."
    author: "Lex"
  - subject: "yahoopops for fetching mail from YAHOO !"
    date: 2003-06-25
    body: "This project does a similar thing for Yahoo ! Mail. \n\nhttp://yahoopops.sourceforge.net\n\nWouldn't it be useful if all such project combine and then you just have to write a plug-in for fetching mail from a particular service. Because there are so many of them ..\n\nhotmail.com\nyahoo.com\nusa.net\nindiatimes.com\nrediffmail.com\nsify.com\n...\n...\n...\n"
    author: "Nilesh"
  - subject: "Re: yahoopops for fetching mail from YAHOO !"
    date: 2003-06-25
    body: "wow, great. I use fetchyahoo at the moment, but this seems a worthy try."
    author: "kool"
  - subject: "Gotmail configuration guide"
    date: 2003-07-02
    body: "<a href='http://linux.cudeso.be/linuxdoc/gotmail.php'>http://linux.cudeso.be/linuxdoc/gotmail.php</a>"
    author: "Webby"
  - subject: "Anyone got all these working with hotmail?"
    date: 2003-07-04
    body: "Can anyone get hotmail working with kmail or any client such that it can\n1) sync on deletes\n2) retrieve custom folders (including junk mail)\n3) sync on \"read mail\" status (eg if you read the mail on kmail, it's marked as read on hotmail)\n\nthanks"
    author: "yalag"
  - subject: "IMAP option "
    date: 2004-05-11
    body: "Hello \n\nI installed Hotwayd it works like POP server. I would like to know if gotmail provide IMAP option for hotmail.\n\nThanks "
    author: "Mike"
  - subject: "Re: IMAP option "
    date: 2004-08-21
    body: "sending   -->(pop3 protokol) : pop3hot.com\nreceiving -->(smtp protokol) : mx1.hotmail.com"
    author: "ted"
  - subject: "microshit hotmail"
    date: 2004-08-21
    body: "sending    -->(pop3 protokol) : pop3hot.com\nreceiving  -->(smtp protokol) : mx1.hotmail.com"
    author: "ted"
  - subject: "Configure Kmail for Hotmail !"
    date: 2004-11-17
    body: "Folks, \n\nI am newbie. I am able to use Kmail to access HOTMAIL directly without any other software!\n\nFor Incoming emails:\nSelect : POP3\nHost : pop2hot.com\n{ Feed your username and password }\nExtra > \"Check What the sever supports\"\n\nFor Outgoing emails:\nSelect : SMTP\nHost : hotmail.com\nSecurity > \"Check what the server Supports\"\n\nI didn't use Hotway, or other tools!\n\nAll the best!\n\nRaj."
    author: "Raj"
  - subject: "Re: Configure Kmail for Hotmail !"
    date: 2004-11-17
    body: "Holy crap! It really works, except host for me is pop3hot.com\nNow this is handy, thanks for the information!"
    author: "138"
  - subject: "Re: Configure Kmail for Hotmail !"
    date: 2005-04-14
    body: "salut cherie"
    author: "younes"
  - subject: "Re: Configure Kmail for Hotmail !"
    date: 2004-11-18
    body: "Hi Friends,\n\nPlease Dont use this pop2hot.com configuration to access your HOTMAIL account! \n\n\nThis server attacks on users privacy and without any prior notification. It records users personal information such as password, ID and other info in to their database.\n\nI am sorry for posting that configuration without evaluating to the core.\n\nRaj."
    author: "Raj"
  - subject: "Re: Configure Kmail for Hotmail !"
    date: 2004-11-18
    body: "Hi\n\nI checked whois before I tried and found out that domain wasn't registered to msn(it is registered to some guy in Antwerpen BE), so I kind of figured that they would not care for my privacy.\n \nDid it anyway, why you might ask. Well it is my hotmail account after all ;), I never ever send anything from my hotmail account, I just use it as an address to give to a companies during some registration process where they need to send some key to use a product. Of course I could create new hotmail account everytime I needed one but now that I can read my hotmails with kmail there is no need and no need to change konquerors identification string when checking hotmail.\n\nAnd furthermore, my hotmail account doesn't have my real name or any remotely useful information, to summarize: I use it where I think I am going to get some nice commercial material in an other words, crap. \n\nSo it is handy for me and I didn't know about such a \"service\" before, you don't have to be sorry for me :)."
    author: "138"
  - subject: "Re: Configure Kmail for Hotmail !"
    date: 2006-05-28
    body: "Spot on worked like charm!!! I had to use pop3hot.com as well best and easiest bit of advice I have ever seen!! on extras I clicked none and cleartext went through right away. Thanks"
    author: "TIM"
  - subject: "how to check hotmail thru kmail or gotmail"
    date: 2005-10-05
    body: "can you tell me how i could access hotmail thru gotmail or kmail"
    author: "alison"
  - subject: "Re: how to check hotmail thru kmail or gotmail"
    date: 2007-01-01
    body: "I DO NOT KNOW!!!!!!!!!!!!!!!!!!"
    author: "ELLIE"
  - subject: "Re: how to check hotmail thru kmail or gotmail"
    date: 2007-08-31
    body: "Try gotmail:\n\nhttp://fedoranews.org/mediawiki/index.php/How_to_check_Hotmail_with_KMail"
    author: "SA"
---
At our <a href="http://www.vosberg.be">Belgian SuSE LUG</a>, we have written <a href="http://en.vosberg.be/doc/kde/kmail_gotmail.shtml">a handy bilingual HOWTO</a> (<a href="http://www.vosberg.be/doc/kde/kmail_gotmail.shtml">dutch version</a>) about how you can check your <a href="http://www.hotmail.com/">Hotmail</a> email in <a href="http://kmail.kde.org/">KMail</a> with the help of <a href="http://www.nongnu.org/gotmail/">Gotmail</a>. We are following in the same trail as KDE enthusiast <a href="http://dot.kde.org/1055230511/">Siddhu Warrier</a> by offering our article to dot.kde.org. We believe there is a need for task oriented HOWTOs and we will be translating and writing more of these guides in the near future (<a href="mailto:info@vosberg.be">help us</a>). Enjoy!
<!--break-->
