---
title: "OSNews.com: KDE 3.2-beta2, Towards a Better KDE"
date:    2003-12-15
authors:
  - "JigSaw"
slug:    osnewscom-kde-32-beta2-towards-better-kde
comments:
  - subject: "Good preview"
    date: 2003-12-15
    body: "I think that was a fair preview. Especially the conclusion sums up the situation pretty well."
    author: "Matt"
  - subject: "Did you note..."
    date: 2003-12-15
    body: "... that she didn't post a screenshot with a crash or error dialog as usual? I think that's a good sign."
    author: "Anonymous"
  - subject: "Re: Did you note..."
    date: 2003-12-15
    body: "Thank god she didn't review beta1. Orth's Nov. 5 CVS is the most unstable KDE I've seen since the pre 1.0 betas I got with RedHat 5.2!"
    author: "Rayiner Hashem"
  - subject: "Praising Preview"
    date: 2003-12-15
    body: "Such a good preview from Eugenia is excellent and does indeed suggest KDE has improved in the right directions. I agree with most of her points, however, putting settings under the edit menu has always seemed daft to me, I think the settings menu is very sensible. But otherwise I know exactly what she means, everything starting with configure can be confusing, but what's the solution there? She suggests hiding the \"configure toolbar..\" entry but doesn't that completely oppose good UI design - hiding the functionality?\n\nI don't agree that Keramik is hideous. At the very least it is distinctive. I don't think you can ever appease even 70% of people when it comes to things like widget style.\n\nShe is right about the bouncing cursor needing to be default, and the text shadow also. The text shadow makes the test unreadable when the contrast is too high, however I have found that a 50% gray with white is really nice. I suppose the solution is to find the average colour of the wallpaper and desaturate it a little. If this is done I think the KDE solution is much superior to the usual text shadow effect in terms of making the text more readable. Perhaps this is already known and I missed the relevant discussion.\n\nThoughts?"
    author: "Max Howell"
  - subject: "Re: Praising Preview"
    date: 2003-12-15
    body: "\"I don't agree that Keramik is hideous. At the very least it is distinctive. I don't think you can ever appease even 70% of people when it comes to things like widget style.\"\n\nAsk laptop users if they use Keramik. For me on my laptop it takes too much screenspace ... \n\n\nFab\n \n"
    author: "Fabrice Mous"
  - subject: "Re: Praising Preview"
    date: 2003-12-15
    body: "that's why you can change the windowdecorations to 'laptop', among others. \n\nI've got a monitor running 1200x1600. I like large window decorations. It should take up more screenspace, I think...\n"
    author: "Rich Jones"
  - subject: "Re: Praising Preview"
    date: 2003-12-15
    body: "Having a laptop is not a prerequisite to being able to use wiget styles that take generous amounts of screen space. I have a laptop with a 15.7\" screen running at 1280x1024, for instance. So stop using the excuse that you have a laptop. If you have a small screen that is one thing, but having a laptop is no reason to complain."
    author: "Anonymous"
  - subject: "Re: Praising Preview"
    date: 2003-12-15
    body: "The reviewer is absolutely right when it comes to the \"soft lines\" between toolbars. They would help a lot and - more importantly - simply make KDE look better.\n"
    author: "Anonymous"
  - subject: "Re: Praising Preview"
    date: 2003-12-15
    body: "I think the default theme should be bland. Something that no-one will particularly dislike. \n\nNow by my definition of bland, Plastik is bland. I think it looks great (kudos to the author), but there's no massive in-your-face effects like other themes. Understatment and minimalism are good things. I also like Bluecurve (or QtCurve for purists ;-))\n\nPeople who really love fancy themes will know where to look. Kids on boards always moan about bland themes and want the latest and greatest with translucency and drop-shadows. Personally I think developers should resist this and ship a decent default that the average new user won't mind."
    author: "Bryan Feeney"
  - subject: "Re: Praising Preview"
    date: 2003-12-15
    body: ">putting settings under the edit menu has always seemed daft to me\nYou're not the only one, who never will understand this.\n\n>I don't agree that Keramik is hideous. At the very least it is distinctive.\nOh, it is. Distinctive and hideous."
    author: "Carlo"
  - subject: "Re: Edit Preferences"
    date: 2003-12-16
    body: "I can understand putting settings in the edit menu: You get an English sentence: \"Edit Preferences\". But in other languages this is less obvious, so i like a separate \"Settings\" menu very well. It acts as a good container and is very predictable across different KDE applications."
    author: "wilbert"
  - subject: "Still bias against konqueror i think"
    date: 2003-12-15
    body: "she (Eugenia) jumped to bash konq on the poor rendering of her site, but neglected to note the huge improvements konq has made in its rendering overall, she vagely mentioned speed increases.. but these are huge in konq since the last offical kde release, and deserve more note. I just cant believe that people arnt singing konqs praises like they do for our fat friend mozilla.\n\nshe also lets her personal preference for VERY simple interfaces get in the way of her discovering the beauty of the rest of the system.. she thinks it sucks (how it exists today, she does like kde technologies), and she couldnt be more wrong... try surfing over a pdf in konq.. its just gloriously simple. She completely neglects the user who is accoustomed to the interface (which, dispite the impressions she gives, is very usable) and can harness all the power of KIO, kparts etc. dont get me wrong, i agree with her points on the interface.. but i just cant help but think that she hasnt used it long enough to have a good opinion on the \"deeper\" aspects of kde. "
    author: "Robert Guthrie"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: ":::Still bias against konqueror i think \n\nGive her the benefit of the doubt. Konqueror does have problems."
    author: "Matt"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "Well, she seem to value her own estetical views very much.\nIt is a fact that humans do not have any problems\nfiltering much information. So a \"cluttered\" window does\nnot affect usability much. Of course, there are always extremes\nbut KDE is certainly below the limit.\n\nAnd what's more, KDE is fully themeable and have a Kiosk mode\nso it should be quite easy to change it to ones liking."
    author: "OI"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-16
    body: "<i>It is a fact that humans do not have any problems\nfiltering much information</i>\n\nCare to prove that with a scientific usability research? I think it's bullshit. People get enough information overload already, so overload in the interface only makes it worse. "
    author: "hmm"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-16
    body: "Very true.\n\nPersonally I very much appreciate the many features, options and settings of KDE, but I think they could be pressented a lot better than they are currently.\nMenus are cluttered, too many options are visible by default, grouping seems illogical in places, etc, etc..\nIn my oppinion KDE should deffinately keep all its customizability but should be cleaned up to provide less options in menus, toolbars and config windows by *default*, the stuff still needs to be there and it must be possible to enable it, but a lot of it should be hidden from the average user.. I think a perfect solution would be a trimmed down interface and the abillity to configure the interface to ones liking, but hiding all (or at least most) of that stuff in \"Advanced configuration\" menus/panes/tabs/whatever ..  \nMake it simple and uncluttered by default, but make all the options and whatnot available to the advanced user in some \"advanced options location\" ...\n"
    author: "Jesper Juhl"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "Well, Konqueror is extremly fast but it still fails to render most (most sites I visit anyway) of the pages correctly. It DOES render simple sites like Slashdot, Freshmeat and dot.kde.org correctly but it screws up almost all more advanced sites. Try www.metica.se for example. It renders the page but then clears it?!! I see that kind of stuff all the time.. I still need mozilla for many sites.."
    author: "Tuxie"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "www.metica.se renders fine for my under KDE 3.1.2"
    author: "Bob"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "looks fine here too (kde 314 on SuSE 9.0)"
    author: "cypherz"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "I see the problem as described on kde 3.2beta2. It is not uncommon to encounter pages that do not render well under Konqueror. "
    author: "Jonas"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "I've just been there and it looks OK to me (I don't know how it should look, however). One thing is sure, it didn't clear up as you mentioned.\nMandrake 9.2 (out of the box) here.\nLuck!"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "Both Gabriel and Bob uses KDE older than 3.2 beta 2, and therefore shouldn't comment.\nThat site really IS broken on beta 2 (tried now on two boxes).\n\nI've actually found about, at least, 4-5 sites broken in 3.2 that wasn't so in 3.1.x.\n\nHope it's just temporary bugs though...\nAnd while it - when will we get a functional certificate manager in Konq?\nRight now I need to keep mozilla in order to access my online bank!"
    author: "ealm"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "> That site really IS broken on beta 2 (tried now on two boxes).\n \nIt seems the site sees konqui as ns4. I you change you UA to IE5.5 win98 or something like that it works just fine. So don't blame konqui - blame the programmer of metica.se..."
    author: "tanghus"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "www.metica.se no problem here (kde 3.1.4). And many sites which _were_ problematic are fixed in current CVS."
    author: "wilbert"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "\nComes out as a blank page here (KDE CVS HEAD).\n\n> And many sites which _were_ problematic are fixed in current CVS.\n\nAnd many sites which _were_ fine are now broken in current CVS. :("
    author: "cbcbcb"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "Although the page contains a lot of errors it still renders here.\n"
    author: "Leonscape"
  - subject: "Re: Still bias against konqueror i think"
    date: 2004-01-20
    body: "You mean \"although Konqueror exhibits a lot of errors displaying the page\""
    author: "Trejkaz"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "the problem is in its broken browser detection JS. try switching your user agent setting to IE 5.5 and it renders and works PERFECTLY. even the DHTML menus are flawless. "
    author: "Aaron J. Seigo"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-15
    body: "When I change the Browser ID to \"IE 5.5 on Win2k\" the page renders fine in Konqueror (CVS version, compiled a couple of days ago).  \n\nIt didn't render right at first (rendered, then blanked out), but changing the browser ID caused it to re-render the page, and it stayed up.\n\nI also come across that fairly frequently.  Is it a Konq issue or a server issue?"
    author: "Ed Cates"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-16
    body: "Interesting. It's that \"getflta(ap)\" error. If you search for that on bugs.kde.org, you'll come up with other sites with the same problem.\n\nWhat's interesting is that JavaScript function is not in any of the pages. I suspect a Konqueror bug. But, then again, changing the UserAgent fixes it...\n\nIf someone has a better idea of what causes this (the contents DO get lost! Check your DOM Tree), feel free to post comments on the open bug reports."
    author: "Thiago Macieira"
  - subject: "Re: Still bias against konqueror i think"
    date: 2004-01-20
    body: "It's a JavaScript issue, and a perfectly good example of how JavaScript shouldn't be used."
    author: "Trejkaz"
  - subject: "Re: Still bias against konqueror i think"
    date: 2003-12-17
    body: "While I agree with most of her article, that's what struck me too. Thanks to kparts and the kioslaves collection Konqueror is arguably the nicest piece of software on the planet. A lot of people think in terms of applications while in KDE that compartimentalized view doesn't suffice to really harness the flexibility. \n\nThis kde 3.2 beta is extremely good. Maybe a few config widgets should be moved out of sight but its very configurability and flexibility is what makes KDE so good. "
    author: "Jeroen van Drie"
  - subject: "Re: Still bias against konqueror i think"
    date: 2004-01-27
    body: "I'm working on a project in Suse 9.0.  I see in a lot of places where people say that there is a built in kiosk mode now.  Up to know I have not been able to find it and have been able to find no references to how to set it up and implement it.  Was not even able to find info concerning it on the Suse website.  Any assistance is appreciated.\n\nKeith"
    author: "Anyone know how to implement Kiosk Mode?????"
  - subject: "Re: Still bias against konqueror i think"
    date: 2004-01-27
    body: "http://mail.kde.org/mailman/listinfo/kde-kiosk is the subscription page for the kiosk mailing list. The kiosk readme file is in the kdelibs source package in kdecore/README.kiosk. That should be enough to get started. BTW, if you searched for 'kde kiosk' on google, these two are the first two hits.\n\n"
    author: "Lubos Lunak"
  - subject: "HIG?"
    date: 2003-12-15
    body: "Also why do people always say \"KDE needs a HIG..\"? I find KDE apps are very consistent. It's hard not to right a consistent KDE app, kdelibs are just too good! As long as you use the standard GUI class (KStdAction, KMainWindow, etc.) you can't help but end up with a consistent KDE-styled application.\n\nDo people just say \"use a HIG\" because they would feel better if everyone spent hours over UI design details rather than have the base classes designed so conformance is automatic?\n\nOr am I just glossing over some important details?"
    author: "Max Howell"
  - subject: "Re: HIG?"
    date: 2003-12-15
    body: "We do have documents: see http://developer.kde.org/documentation/library/ui.html\n\nSome of them are a bit out of date perhaps, but we also have the kde usability mailing lists: see http://lists.kde.org/?l=kde-look&r=1&w=2 and similar for other interests (like accessibility).  Most of these lists are far from dead, and many of the participants are active developers.\n\nAnd yes, using the kde libs does half the work for you, but it doesn't do all the work for you.\n\n---\n\nThat said, some of the decisions are truly braindead.  Why the hell is Konquerors Fullscreen mode under settings and not View or Window?  Oh well, at least it's consistently wrong -- it's in the same place in Konsole and Kate.\n\n\nThey are being worked on.  Treatment of tabs is now nearly identical in konq and konsole, as an example, due to a new class in kdelibs designed to unify these things (ksirc and a few others are slower to convert, missed the feature freeze.)\n\nThis seems as though it will be the focus of the post-3.2 KDE (based on my perceptions of conversations in the official irc channels).  As soon as freeze finishes folks."
    author: "Troy Unrau"
  - subject: "Re: HIG?"
    date: 2003-12-15
    body: "> That said, some of the decisions are truly braindead. Why the hell is Konquerors\n> Fullscreen mode under settings and not View or Window? Oh well, at least it's \n> consistently wrong -- it's in the same place in Konsole and Kate.\n\nNah, come on. ;)\nBraindead would be to put it under Bookmarks, to put it under Settings might just not be optimal. "
    author: "dsmith"
  - subject: "Re: HIG?"
    date: 2003-12-17
    body: "I would expect to see it in \"View\""
    author: "Jonathan Bryce"
  - subject: "Re: HIG?"
    date: 2003-12-15
    body: "There are some things you just can't program in, like guidelines that context menus should be context-specific and not too huge. These are the things we need an HIG for."
    author: "Rayiner Hashem"
  - subject: "Re: HIG?"
    date: 2003-12-15
    body: "Good usability is not the same thing as consistent user interface - it includes consistency, but is more than that. Usability is also the very small details that are easy to miss if you are an advanced computer user, or if you are used to a certain UI enough to stop bothering about these details. How things are worded, where things are found, if programs behave like the user would expect. No matter how good KDE libraries are, they can in themselves not replace user interface guidelines. What they should do however, is implement the guidelines where it is possible. Where they can not help, the application programmer needs to be aware of them.\n\nGnome Human Interface Guidelines (http://developer.gnome.org/projects/gup/hig/1.0/) are probably more detailed than would be required by KDE, since the framework can take care of many things mentioned in them. Look at the introduction for example: \"Make Your Application Consistent\" is indeed mentioned there, but so is also \"Design for People\", \"Let Users Know What's Going On\", \"Keep It Simple and Pretty\" and \"Forgive the User\". However good the kdelibs are, they can not deal with all this. \n\n\n\n"
    author: "Claes"
  - subject: "Please, no 'registry'"
    date: 2003-12-15
    body: "Eugenia very much seems to like extremely simple interfaces and hiding options as much as usable. I really don't agree with her. Hiding configuration options into a kind of registry-editor alike tool will really make them usable. There was a very to-the-point comment in the reactions that pointed to the amount of downloads of TweakUI for windows, that is basicly a GUI interface to the many hidden options in the windows registry. Please, stick with putting options in the control center...\nPersonally, I like the configurability of KDE. I can have stuff *my* way. Sensible defaults: sure, but hiding ways to make another choice: please don't. \n\nOne thing I do agree with: Konquerors menu's can be trimmed down. She is right I don't need Cervisia on a webpage."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Please, no 'registry'"
    date: 2003-12-15
    body: "A good alternative to hiding options are presenting \"Advanced\" tabs/buttons/modes in config panels. The user feels that entering this places they are doing \"advanced\" operations, left for \"advanced\" (experienced) people. If you are a newbie then you tend not to visit these places very much."
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Please, no 'registry'"
    date: 2003-12-15
    body: "Well, I almost wonder if it should not be taken one step further.\nOne of the settings should be how to handle the config; should it be novice, intermediate, advanced? Then the config panels should adjust accordingly.\n\nKommander is perfect for this. I have suggested to Eric that Kommander needs to be extended to support kcontrol (I do not have the time). Once he does, I am going to resurect my *nix config project (3 tier config XML that serves to map from system config to a functional interface to a gui interface). I figure that by coming up with a true meta data for describing configs, we can generate Gnome, KDE, WX, distributed, etc. I actually had it going several years ago (should have finished it or submited for others to finish)."
    author: "a.c."
  - subject: "Re: Please, no 'registry'"
    date: 2003-12-15
    body: "Do you really have Cervisia's context menu entries while browsing a webpage? I don't! (and I do have kdesdk installed)"
    author: "anonymous"
  - subject: "Nice"
    date: 2003-12-15
    body: "I liked the review and its conclusion , and second that for users who don't have a ~/.kde/ already (so fresh accounts) Plastik and the bouncing cursor should be default. Also miss info about the huge improvements of Konqueror.\n\nDon't agree however with the \"one editor\" mantra: I definately love KWrite and Kate. Kwrite for normal quick work on single files, and Kate for working on trees of files, programming etc. They use the same underlying technology so they really feel the same whatsoever."
    author: "Wilbert"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: "Mmm. I've thought about one 'KEditor' for a while. It could perhaps change its view depending on what file you opened etc. KWrite and KEdit certainly need to be merged."
    author: "David"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: "This is a bad usability idea.  Each application should have one (configurable) interface.  It should not morph into some other type of application based on something a variable as the data being displayed.  \n\nI also use both KWrite and KEdit, but I use them for different purposes, even with the same file.  For example, when opening a perl script, which editor would you set as default?  For simple scripts, I don't need extensive features, just a quick editor, for more complex scripts, I want to use the syntax highlighting, auto formating, completion, etc. that is available."
    author: "Jon Doud"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: "Why not switch between the two modes with a menu choice?  Like you switch a calculator from simple to scientific mode?"
    author: "Henrik"
  - subject: "Re: Nice"
    date: 2003-12-16
    body: "That's a good idea.\n\nI use both too (mainly Kate). But one thing that bugs me with Kate, and would be made even worse if they were combined, is that if I just want to edit a single file and I do open with Kate it has to open the 30 files I was last editing also.\n\nThe button to change views should be from single file mode to project mode, and there should be a way to have more than one project grouping. When you open a file as part of a project it is remembered for that and when you switch projects the old files are still remembered if you need later. This would be far superior to having actual project files which I think is a pain.\n"
    author: "Dominic Chambers"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: "I've said this before, so I'll just repeat -- switching the default style regularly is just a bad idea.  Can you imagine Mac changing the default look and feel with every minor release?  How about Windows XP looking completely different with each service pack?  Keramik is part of KDE 3's look and feel.  Any decision to change the default should be post-3.2...\n\nThe KDE fanclub changes their mind on styles every 3 weeks, but if you go back to just before 3.1 came out everyone thought Keramik was the coolest thing since sliced bread."
    author: "Scott Wheeler"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: "> The KDE fanclub changes their mind on styles every 3 weeks, but if you go back to just before 3.1 came out everyone thought Keramik was the coolest thing since sliced bread.\n\nThe difference is that not a large amount of people actually TRIED keramik before 3.1 came out, because, they needed uh, CVS or unstable releases. While, since Plastik started out as a seperate style, it was used by a lot of people before inclusion into CVS. \n\nKeramik might look neat to look at, but it feels horrible to *use*. "
    author: "anon"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: "How can Keramik \"feel horrible to use\"? While the taste for different styles may vary a style \"feel horrible to use\" is clearly a matter of your imagination."
    author: "Datschge"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: ">  While the taste for different styles may vary a style \"feel horrible to use\" is clearly a matter of your imagination.\n\nUh, Keramik does indeed feel quite bad for daily usage __for a large amount of people__. The worst thing is it's bulkiness. Plastik doesn't have such problems. "
    author: "fault"
  - subject: "Re: Nice"
    date: 2003-12-17
    body: "Sure, Keramik (iyo) feels bad, and? I also feel quite bad when I am forced to hear (imo) bad music or watched an (imo) bad movie, wasted my time reading an (imo) bad book. It's still all a matter of taste. Why are you trying to prove something you should know you can't prove beyond your own self? Taste never has been universal."
    author: "Datschge"
  - subject: "Re: Nice"
    date: 2003-12-15
    body: "Actually there were tarballs available and SuSE even shipped Keramik with 3.0 with I think SuSE 8.1.\n\nAgain, I mostly chalk this up as everyone getting excited about the new stuff; most normal users really don't care.\n\nThis will be a valid debate for 4.0, but changing the default style twice in a major release is simply a bad idea; like it or not there should be some continuity in a major release."
    author: "Scott Wheeler"
  - subject: "Re: Nice"
    date: 2003-12-16
    body: "Well, actually they do. Transparent windows are now gone, brushed metal in the latest version. I believe that Eugenia mentioned these these changes some time ago."
    author: "Anonymous"
  - subject: "Re: Nice"
    date: 2003-12-16
    body: "> Can you imagine Mac changing the default look and feel with every minor release? \n\nApple actually has with OSX 10.0->10.1->10.2.. with great success too, since they've been making a more and more polished style with less \"in your face\" eyecandy each time. \n\nThe days of lickable buttons are fading, at least until 2005 when longhorn comes out :-)D"
    author: "anon"
  - subject: "Re: KWrite vs Kate vs Kedit ...."
    date: 2003-12-15
    body: "I use/rely on BOTH KWrite and Kate on a regular basis (ie about 18 hours/day) for exactly the reasons above - Eugenia just needs to upgrade to KDE and she would understand:)  (btw the docking file listing in Kate 3.2 seems broken since it overwrites the editing screen and doesn't stay active although the tear-away feature is very nice).\n\nHowever, the number of editors in the menu even struck me as too many (maybe Kommander should be taken out of editors, moved somewhere else) and yes, there seems little reason to have both KWrite and KEdit (I realize the differences - but they could be combined - I always use KWrite even on simple text config files).  "
    author: "john"
  - subject: "Re: KWrite vs Kate vs Kedit ...."
    date: 2003-12-15
    body: "I thought that first, too. But I found out that you have to click on the rectangle (the icon in the middle of the three really tiny icons) in the top right corner of the docking file list. Not very obvious..."
    author: "Steffen"
  - subject: "Totally Agree (ONE EDITOR TO RULE THEM ALL)"
    date: 2003-12-16
    body: "Kate! Kwrite! KEdit! Kate is not representative of its function, Kwrite sounds like a wordprocessor, only Kedit has a name respresentative of its function. Even the names are confusing! Most users, including me only need one and that one is Kate (should be renamed to Kedit)! Kate has a very configurable suer interface and it can do just about everything Kwrite and Kedit can all in a single package. THERE IS NO NEED FOR THE REST,s top trying to pelase the last 5% who want this. This will cause nothing but ambiguity and a detremental effect on useabiltiy and space, if KDE IN THE OFFICIAL DISTRIBUTION can not decide on a single all purpose text editor, this is really a sad day for the project. \n\nReally speed is great in Kate alsoa nd that is not an issue anymore, not for something like a text editor, Kate opens in less than a second here and hardware is getting far faster as is software so there is no need to nitpick on speed so much. By the end of 2004, Intel should havea  3.8 GHZ computer for example and most will have at least 1-2 GHZ."
    author: "Alex"
  - subject: "Re: Totally disagree (NO ONE EDITOR)"
    date: 2003-12-16
    body: "The problem with your one editor is that from the limited perspective you're looking at it with it makes sense. Many people with much greater insight and perspective have looked at it and having all three makes sense. What exactly do you gain in taking away from others and why exactly is it a \"sad day\" when someone who ccouldn't answer dozens of questions the developers had to doesn't agree?\n\nWhat is sad is restricting choices in ways that hurt others... especially when you don't even know who it hurts and how."
    author: "Eric Laffoon"
  - subject: "Re: Totally disagree (NO ONE EDITOR)"
    date: 2003-12-16
    body: "How do you know how much insight I have? Also Eric, I already mentioned why this is a bad idea in my original post. I am not proposing the immidieate removal of the rest, I am jsut proposing that development be focused on one and the others just remain as legacy until KDE 3.3 or 4 when KAte cand o everything they can and more, meaning that they can be removed.\n\nAs for restricting choice. This is not so, I only said that it shouldn't be in the main KDE distribution, these applications will still exist. So if the user feels Kate is inadequate they will just find a better one."
    author: "Alex"
  - subject: "Re: Totally disagree (NO ONE EDITOR)"
    date: 2003-12-17
    body: "You are running into open doors. KEdit is no longer in the kdebase package and is still there only due to the missing bidi support in the other editors, and KWrite and Kate base on the same code so there's is no development time wasted.\n\nSo to be blunt, I wish you much more insight than you are currently showing."
    author: "Datschge"
  - subject: "Re: Totally Agree (ONE EDITOR TO RULE THEM ALL)"
    date: 2003-12-16
    body: "Kate can't do bidi yet. After that KEdit might be dropped. I just had a terrible experience with it: replacing the same text in 2800+ took so much time that I killed it. In Kate it took a fraction of a second. Anyway, I'm happy with Kate and KWrite. :-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Totally Agree (ONE EDITOR TO RULE THEM ALL)"
    date: 2003-12-16
    body: "> and most will have at least 1-2 GHZ\n\nSpoken like someone with cash to burn."
    author: "Tukla"
  - subject: "Misunderstandings"
    date: 2003-12-15
    body: "\"why would I want to zip a web page or use Cervicia with it, is beyond me\"\n\nIs she expectiong an answer? I want to zip a web page to save it to disk and view it later, and use cervisia to admin a page remotely. She doesn't, then criticizes.\n\n\"Konqueror is the Frankenstein of file managers, made of so many Kparts that the end result is just not good\"\n\nThen she's misunderstanding the nature of konqueror: it's made to be a Frankenstein and to hold as many kparts as one wants to throw at it.\n\n\"Another thing I dislike is that \"settings\" menu that most KDE apps have, where they list 3-4 different \"Configure\" options in addition to the \"Configure the application\" option\"\n\nThis comes from above: she misunderstands the nature of KDE and reusable parts. That's intentional, the applications are \"frankensteinized\" and every part has its own life, get used to it.\n\n\"there are too many apps shipping with KDE\"\n\nAnd others complain of too many apps for \"alternative platforms\". Come on, most of them are optional and the distributions pack them separately.\n\nOn the other side:\n\nI agree on the point of making the color of the icon text automatic, if someone can hack it (I can't think how to do it, sounds a bit hard).\n\n\"expanding a few *pixels* the space between words on the application menus\"\n\nI'd advocate a measure on ems, the current about 1em here can be probably improved by the about 4em of gnome (in her example). But I don't mind too much; it's \"good enough for me\".\n\nI couldn't make any sense of her criticism of kcontrol, so I'll skip that commentary."
    author: "Cloaked Penguin"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "I think she clearly has some localized problems.  Her menus are far larger than mine and I install all of KDE from CVS.  I think her biggest problem is that she installed everything that came with her distro and KDE just picked them up and added them to the menu.\n\nI also think that Konqueror is not to blame for services adding to the service menu where it is inappropriate to do so.\n\nKControl might benefit from an \"Advanced Mode\" in the future which activates modules that are initially hidden.  We know that all users will eventually turn this on, but it will look more \"accessible\" to the user at first I think.\n\nOver all the criticisms were not too bad but they were perhaps slightly misdirected or at least the real problem was not fully understood."
    author: "George Staikos"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "I fully agree with an basic and advanced mode for configurations."
    author: "cwoelz"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "As far as I understand, Cervisia is only of interest if you use cvs. I think cervisia is a kind of option that can't be categorized as simple vs advanced - rather it is a specialized option. It does not fit on a simple - advanced scale. "
    author: "Claes"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "Cervisia is part of the kdesdk package (That's K Desktop Evironment Software Development Kit for you) so if your distribution installed it for you complain to your distribution that you don't intend to develop using KDE."
    author: "Datschge"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "Besides I don't know how she managed to get them, but I don't have Cervisia's context menu entries while browsing through the web at all (and I do have kdesdk with cervisia installed). "
    author: "anonymous"
  - subject: "Re: Misunderstandings"
    date: 2003-12-16
    body: "Thats what you get if you are browsing the web in Filemanager mode of the konqui."
    author: "PBY"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "Could Kcontrol be laid out like the Mozilla Firebird setup screen. For those who haven't seen it it's like Outlook. The top 5-6 items could deal with basic things (Desktop Appearance, Application Appearance, Sound, Fonts, Personalisation, Localisation) and the last item could be \"Advanced\" which brings you to a tree where you can pick the appropriate values and edit them.\n\nAs regards context menus, they are getting a bit out of hand (and I've used KDE since 0.99). For one thing, all the XML-Gui extensions for K3B etc. should be put in a sub-menu (e.g. \"Actions\") just under \"Open with...\". I'm using 3.1.4 and it's crazy. Also you've got to wonder about some of the options added to these menus. The desktop _context_ menu should list things to deal with the _Desktop_ but it's got logout/lock buttons (they're on the shagging kicker!) open terminal and run command. None of these have anything to do with the desktop. And for those who bemoan the lack of use, I always click on kicker for loggin out, use Alt-F2 for running stuff, and have a terminal icon on my kicker, all of which is faster and easier than finding a free piece of desktop, right-clicking on it, and selecting the appropriate option in the menu."
    author: "Bryan Feeney"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "> As regards context menus, they are getting a bit out of hand (and I've used KDE since 0.99). For one thing, all the XML-Gui extensions for K3B etc. should be put in a sub-menu (e.g. \"Actions\") just under \"Open with...\". I'm using 3.1.4 and it's crazy\n\nHave you tried 3.2b2? The context menu is a lot better, but not perfect yet. "
    author: "anon"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "\"The desktop _context_ menu... it's got logout/lock buttons...\"\n\nHey, calm down! I use them all the time if I am using the mouse (with keyboard I have logout set to ctrl-alt-d). And each time I am in windows (not too often) I attempt to logout from a right mouse button click; annoying. You do it differently; let other people be."
    author: "Cloaked Penguin"
  - subject: "Re: Misunderstandings"
    date: 2004-01-20
    body: "Damn right.  Also, most other WMs have logout on the desktop, who is KDE to break a fine tradition? ;-)"
    author: "Trejkaz"
  - subject: "Re: Misunderstandings"
    date: 2003-12-15
    body: "Of course I want to be able to lock my desktop, why shouldn't that be in the desktop context menu?"
    author: "Datschge"
  - subject: "Don't make the same mistake as GNOME"
    date: 2003-12-15
    body: "One of the great things about KDE is it's configurability, the possibility to let it do (almost) *exactly* what I want.\n\nI agree with Eugenia on some things (for example I also find Keramik extremely ugly) but unlike her, I admit that it's a personal perference and not some kind of law written in stone.\n\nThere is no best setting for everybody. The defaults should try to satisfy as many people as possible, yes, but please NEVER EVER remove any configuration possibilities. I gladly invest a couple of seconds to find an option which will save me frustration and time FOR MONTHS. - You only configure a feature once, but you use it a lot longer.\n\nAlso, Eugenia is contradicting herself: She sais that computer illiterate users never change the defaults (which is true) but then she sais that the configuration is too complicated for them - If they never change the defaults anyway, they WILL NEVER SEE THE CONFIGURATION PANEL!!!\n\n"
    author: "Roland"
  - subject: "Re: Don't make the same mistake as GNOME"
    date: 2003-12-15
    body: "Eugenia has raised some very valid points but struck a nerve whilst doing it. I think we can summarise her by saying that KDE has three fundamental flaws: Look&Feel, Usability, and Compatibility.\n\nNow I am sure we would all be very happy if further improvements were made in the L&F and Compatibility areas, and Eugenia's comments regarding menu spacing and toolbar separation is the type of stuff that makes Gnome look so much better. But please be careful when messing with the usability demon.\n\nI do wonder whether the Gnome/MacOSX approach is truly better. I certainly don't prefer it, and nor do many others. But, being an advanced user it is not immediately obvious why KDEs way of doing things may not be perfect for everyone.\n\nStill, good UI design comes from following a cycle of behavioural analysis, innovation, and product refinement, and I can think of many areas that could be substantially improved without making huge changes. But Eugenia's idea of splitting program configuration in two parts seems more revolutionary, than evolutionary. What proof is there that this is a good idea. I worked on a large security product that took this approach and we had a hard time making it work, ending up with a product that was inconsistent and even harder to use.\n\nI am sure there is a perfect compromise in there somewhere, but perhaps it will only be found through careful, managed evolution."
    author: "Dominic Chambers"
  - subject: "Re: Don't make the same mistake as GNOME"
    date: 2003-12-15
    body: "This review was horrible, even for Eugenia.  Anyone remember her review of Red Hat 8?  If not, go look it up; if you read it and find yourself nodding your head in agreement, please switch to a more user-friendly operating system; you're not cut out for the world of Linux. In fact, if you agree with Eugenia's opinion about Red Hat's X configuration system, including its inability to deal with Eugenia's oddball monitor and graphics card, go buy a cabin in a remote location and swear off all forms of high technology; you can't handle any of it.\n\nThe entire review is an editorial, and a poorly-written one at that.  And yes, as usual, she's managed to contradict herself.  She complains about things that, by her own admission, most people will never see.  And quite frankly, the bit about using Plastik over Keramik is purely opinion; I use Keramik, and find it to be nice-looking.  The default colorscheme, on the other hand, is horrible in my opinion, as is the window decoration.  Scrollbars could use a bit of work, too.  But that's just my opinion. :D\n\nI agree that KDE shouldn't make the same mistake as GNOME.  I don't recall the name of GNOME's default CD ripper, but the last time I checked, it didn't even offer an option for bitrate.  Is the average user REALLY going to get confused if they get to choose a bitrate?  "
    author: "regeya"
  - subject: "Re: Don't make the same mistake as GNOME"
    date: 2003-12-16
    body: ">I agree that KDE shouldn't make the same mistake as GNOME. I don't recall the\n>name of GNOME's default CD ripper, but the last time I checked, it didn't even \n>offer an option for bitrate. Is the average user REALLY going to get confused if \n>they get to choose a bitrate?\n\nSound Juicer is just not finished yet. Ross Burton says he plans to add that feature, but just hasn't yet. but to answer your question, yes, it will confuse the average user unless it's couched as a quality scale (low, medium, high etc) possibly with the bitrate being show for advanced users, but possibly not. 'cause seriously, who cares?"
    author: "mark"
  - subject: "Re: Don't make the same mistake as GNOME"
    date: 2003-12-18
    body: ">yes, it will confuse the average user unless it's couched as a quality scale (low, medium, high etc)\n\nThat's a terrible idea, IMHO.  What about people who want a semi-predictable filesize?\n\n>possibly with the bitrate being show for advanced users, \n>but possibly not. 'cause seriously, who cares?\n\nThat's an awful lot of assumption."
    author: "Shane Simmons"
  - subject: "Useless review"
    date: 2003-12-15
    body: "No matter if what she says is good or not, her review is useless. I can't trust on somemone that hasn't even checked the facts that she claims:\n\n\" I don't need four text editors in the same submenu (Kate, KWrite, Kedit and Kommander-something)\"\n\nSince when is \"kommander editor\" a _text_ editor? She didn't even bother clicking on anything. That says all.\n\n"
    author: "uga"
  - subject: "Re: Useless review"
    date: 2003-12-15
    body: "Maybe Kommander should add a GenericName to its desktop file to specify what it is. It may not be a text editor but it didn't tell me what it IS instead then. Even the \"About\" box did not say anything other than \"Kommander Editor\".\n\nEditor for what?"
    author: "Waldo Bastian"
  - subject: "Re: Useless review"
    date: 2003-12-15
    body: "True. I didn't notice that fact. One can't easily guess what it does, unless he has used Qt Designer before. "
    author: "uga"
  - subject: "Re: Useless review"
    date: 2003-12-15
    body: "Right. Any suggestion? GUI Script Builder??\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Useless review"
    date: 2003-12-16
    body: "Yes that sounds like a good name to me. \n\n\nFab"
    author: "Fab"
  - subject: "Re: Useless review"
    date: 2003-12-16
    body: "Kommander has reached a level where a moderately capable user can do a fair amount with it, but unfortunately it lacks some polish. With some work it could  become a tool that could enable less experienced users to do much more. So we need to address it, but we also need more developers as Marc has not been able to give it much attention and I'm debating taking the lead coding it. (as if I had time) Anyway I loaded things up and noted it showed up as an editor. I never really thought it belonged there and don't know who put it there. It really should go under development tools.\n\nI would also agree with the critisism of the review. Apparently there are too many programs because she can't open them all and tinker with them a few minutes in the short amount of time she allots to review. After the recent review from a BSD user who did take the time I'm forced to agree that the packages should not be made to fit into her reviewing schedule, but it should be the other way around. No wonder she's obsessed with visuals. "
    author: "Eric Laffoon"
  - subject: "Custom View filters for kcontrol?"
    date: 2003-12-15
    body: "May a kind of User-View options practical?\nlike:\n\tnovice-> less less and simple options\n\tmedium-> opitions like now\n\thardcore-> more (every) options"
    author: "pmarat"
  - subject: "Re: Custom View filters for kcontrol?"
    date: 2003-12-15
    body: "Sure, and then produce 3 versions of the documentation (each with different texts and screenshots), and 3 versions of all KDE web sites (because people would otherwise wonder why the screenshots look so different from their desktop), and different user groups (because a hardcore user can't help a novice - the hardcode user does not know which option the novice has)...."
    author: "AC"
  - subject: "Re: Custom View filters for kcontrol?"
    date: 2003-12-15
    body: "That has been brought up on kde-usability many times and it has been turned down every time. It will just not work because different users are good (and bad) at different things."
    author: "Peter Simonsson"
  - subject: "Re: Custom View filters for kcontrol?"
    date: 2003-12-15
    body: "Well, maybe grouping some of the modules and adding an 'advanced' button could do the trick (it wouldn't reduce functionality, but should look cleaner) eg:\nOne 'Appearance' module, containig 'Fonts', 'Windows Deco', 'Style', 'Colors', 'Background', the first tab would be used to select a style (Plastik for example consists of style, colors and window deco, so a unified setting would work) a background (image or color) and one base font (eg. default all fonts to Vera). Klicking the 'advanced' button would ungray tabs to further customize the settings.\nI think I'll do a mockup and post it (on KDE Bugs/ KDE Look) soon..."
    author: "LaNcom"
  - subject: "Re: Custom View filters for kcontrol?"
    date: 2003-12-15
    body: "and what about 'advanced...' buttons .. impossible too ?"
    author: "ik"
  - subject: "Re: Custom View filters for kcontrol?"
    date: 2003-12-15
    body: "Why then on kprinter do we have a expand and collapse section and a properties button.  If we need to get to the options we can, but for the simple stuff - keep it simple.\n\nSomebody (maybe I will) should compile a list of every option in kcontrol.  The list should be a flat list without hierarchy.  Options should be listed as those that are basic and advanced - voting and marking could possibly done on a wiki style page.  The basic options (probably 1/5 to 1/3 of all) should be put into refactored dialog boxes.  The remaining could either have expand/collapse buttons, an advanced tab, could have their own dialog section, or could even have a simple section similar to the kprinter driver dialog (which is very minimalist).\n\nI'm sure the developers have voted against it.  It is hard and who wants to decide what is basic and what is advanced.  At somepoint it will need to be done.  And if I, who knows relatively little about good C++ programming, have to  grab CVS and submit a patched kcontrol - then maybe I should.  Or maybe the people who developed it originally and understand its internals and feel ownership should do it and get a much better result.  Either way, by the next KDE release 3.3 or 4.0 - somebody will have done it - whether the mailing list agrees or not - it will happen (even if it comes down to me getting off my soapbox and doing something about it myself)."
    author: "PaulSeamons"
  - subject: "Re: Custom View filters for kcontrol?"
    date: 2003-12-15
    body: "I mostly agree with Eugenia whe she says kcontrol is too cluttered, and a simpler control panel should be used instead.\nBut i think a gconf-editor/regedit like solution would be horrible and an almost inusable annoing program.\nOne way could be inserting the basic/advanced button in kcontrol, or shouldnt' be instead better if the settings:/ kio-slave would show fewer and most common items like background or color settings and leave kcontrol as is? The kio slave would be the easy panel and kcontrol the utility fon fine-tuning the desktop and no regedit-like application would be needed, because in some things kcontrol likes regedit a lot(like a tree organization or the search function) but doesn't have that annoying behaviour key/value key/value key/value.\n\nAnd, going a bit off-topic, another important fact is that the kio slave could show either shortcuts to kde configuration modules and proprietary configuration modules added by the distribution. \nLook for example to Mandrake, there is the Kde control center and the Mandrake control center, why two control centers in one single desktop? this is absurd, but the Mandrake configuration tools are written in gtk so at the moment is not possible to integrate them in the kde control center(like Suse did), but the kio slave starts every module in a new window,so this should not be a problem. \nIn fact these tools are yet integrated in the Gnome control center, as entries in their settings vfs, giving more consistency to the desktop."
    author: "mart"
  - subject: "Some good points"
    date: 2003-12-15
    body: "This message is posted from Konqueror in Kde3.2_beta2, and I think KDE3.2 is the best desktop environment I've seen. But, I have to say I agree with many of Eugenia's points. My pet peeves are:\n\n- The context menu in konqueror is nasty\n- The control centre is a nightmare.\n- Default settings are generally horrible. Keramik is ugly and too large. Toolbar layouts are strange and cluttered. The close/maximise/minimise buttons are right next to each other. Why not enable icons on buttons by default?\n\nI'm going to go see if usability.kde.org needs some volunteers.\n"
    author: "Ben Roe"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "Icons on buttons are not on by default because not everyone has a computer as fast as yours.  It costs to add those icons.\n\nNote that the only way to reduce the context menu significantly is to remove functionality that people want and use.  We did try removing entries during development and some features became unusable or the number of complaints far outweighed the number of people supporting it.  Cascading the menu is no real option either as it really only increases the number of options while making them harder to access.  If you really don't like it, you can configure it.  It uses and XML file to define what's in it.  Remove the features you don't want.\n"
    author: "George Staikos"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "I didn't realise button icons were that heavyweight. It's just that they decrease the time to figure out which button does what - it's immediately obvious that green=yes, red=no in general. It seems a good feature for newbies to have.\nI will go edit my menu XML. It would be nice to have a menu type selector or something though. Anyway, I'm going to quit being a whinging user now."
    author: "Ben Roe"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "Those colour associations arn't valid in all cultures!"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "not to mention colour blind people and monochrome screens (still present in the embeded world) ;)"
    author: "oliv"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "If there are computers, most likely there are semaphores too."
    author: "Quique"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "Of course, you have to balance that against the cost of not reducing menu clutter, which is reduced efficiency for the user. I'm not surprised that you got more people complaining about the menu reduction than supporting it --- people who want the feature have a much stronger incentive to contact you than people who don't. The overall response on the 'net, however, has been that most users (with the exception of a vocal minority) seem to support the overal simplifications that have been made in the Konqueror context menu. \n\nAnd yes, you can configure it using an XML file, but that's not a great solution. The XML file doesn't really look much like that the menu looks like when it is rendered. Unless you're familiar with the mechanism, its hard to get the look you want. At least thats what I noticed when editing the Konqueror XML menu. "
    author: "Rayiner Hashem"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "I agree with you Rayiner, I feel this *very* vocal 'uber-geek' minority is driving people away from KDE to GNOME. They may be happy to ghetto-ize themselves, but I think it would be a shame for the project. Personally, I have already moved over to GNOME, precisely because it is so much simpler to understand for the people in our organisation who are largely moving over from Windows. GNOME is catching up look-wise recently as well.\n\nSo, yeah I'm behind Eugenia 100%."
    author: "Tom Holte"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "\"I feel this *very* vocal 'uber-geek' minority is driving people away from KDE to GNOME.\"\n\n\nYeah, right. That's why KDE is leading over GNOME in every poll I saw, right?\n\nEverytime somebody talks about \"vocal minorities\" you can just substitute everything he said with \"I just made it all up and I don't have proof for anything\".\n\nFACT is that KDE is a lot more successful than Gnome at actual users. GNOME is more successful in finding companies (like Sun) pushing it.\n\nA cynic might say that KDE-bashers want KDE to \"simplify\" only because they want it to fail and fall behind GNOME....\n\n"
    author: "Roland"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "More importantly, are we supposed to write code for people who don't provide any input and only exist in theory, or the people who get involved and vocalize what they want?  It seems to me that the vocal minority are the people who are claiming that we are listening only to the vocal minority."
    author: "George Staikos"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "Let's use the Konqueror context changes as an example. A few people here on kdelook complained strongly about them. Yet, for every review and preview of KDE 3.2 I've seen, people seem to love the changes. There are definatey *real* users out there who like the simplifications. Its a fact of life, however, is that people are more likely to vocalize disapproval than approval. I know when Kwin III got all wonky in beta1, I submitted a bug report. Yet, when it was fixed in beta2, I didn't send an e-mail praising the fix. I was happy and appreciative, but I saw no reason to vocalize it. \n\nThere is, potentially, a problem here. Its hard to judge what the users want. It comes down to: do the KDE developers agree that simplification is needed, or don't they? After all, these are their applications, no? There is no reason to be heavy-handed like the GNOME developers were, but if KDE-Usability put up a page saying \"we want simplifications; send patches here,\" I think you'd see a lot more involvement from the segment of the KDE user community that wants to streamline the interface. "
    author: "Rayiner Hashem"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: ">More importantly, are we supposed to write code for people who don't provide any\n>input and only exist in theory, or the people who get involved and vocalize what\n>they want ?\n\nLet me vocalize a bit then:\n\nI'am a real user.\n\nI like the KDE framework (although I never contributed a line to KDE) far more than GNOME. I like the speed of KDE. Iwould like to use KDE full time.\n\nBut I am using GNOME to write this just because of better usability. That pretty much says it all."
    author: "Bilou"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "Pay someone to set up KDE the way Gnome works, it's perfectly possible to turn KDE into a Gnome clone just by changing setting and without touching any code, making it look the same, and with all the menu entries removed etc. But don't expect it to become the default KDE."
    author: "Datschge"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "> it's perfectly possible to turn KDE into a Gnome clone just by changing setting and without touching any code\n\nNo, it's not. Seriously, I'll PayPal you $1000 if you can set up a KDE desktop, with KDE apps, that after a minute or two of normal usage I can't easily distinguish from GNOME. Hell, I'd be enormously surprised if you could even take a single KDE screenshot that looks convincingly like GNOME (and I mean real GNOME, not some fugly Geramik version)."
    author: "damiam"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "The look and feel wasn't the point. Bilou statet that he can't use KDE due to usability, and I state it's easy to bring down and limit KDE to the level of GNOME. This has nothing to do with whether you can distinguish it from GNOME after using it or after looking at a plain screenshot."
    author: "Datschge"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "Look and feel is a large part of usability. Look at Eugenia's screenshot and comments on Gnumeric and KSpread. There are a lot of little details that GNOME gets right and KDE doesn't. (Full Screen under the Settings menu in Konsole? What were they smoking?) A lot of this sort of stuff can't be fixed just by editing configuration options."
    author: "damiam"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "You should choose better examples than the ones you pick up since it's indeed possibly to fully customize the menu, it's a simple xml file for each app which contains the menu and toolbar structure. Apps' menu structure just have no GUI to be user editable since the KDE HIG states that a menu's purpose is to offer an alternate way to access all functions an app offers.\n\nAnd regarding Full Screen under the Settings menu I agree that it's suboptimal. But I personally wouldn't put it in any other menu but instead make it part of the WM (reasoning being that I don't want the app to decide whether I can run it fullscreen or not)."
    author: "Datschge"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "That was only an example of the poor design choices that have been made, not a concrete \"Fullescreen in the settings menu is THE problem\" statement. I don't have a Linux box in front of me at the moment. When I get home, if it matters so much to you, I'll find a better example.\n\nMaking fullscreen part of the WM is in interesting idea, but I don't see how it'd work. Generally apps designed to run fullscreen (media players, terminals, office products, web browsers, etc.) have a completely different UI layout in their fullscreen incarnations. Since that would already have to be hardcoded into the app, there's not a lot of point in making the actual fullscreen switch a WM option."
    author: "damiam"
  - subject: "Re: Some good points"
    date: 2003-12-17
    body: "Yes, please get better examples. Input of any kind is always nice to have.\n\nAnd I don't see any real problem with fullscreen being a part of the WM. Windows with fixed geometry can't have fullscreen, but the WM, having to handle that after all, knows about that. The UI layout, with respect to usability, shouldn't be radically different between a normal window and fullscreen, surprising the user never had been good usability."
    author: "Datschge"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "Even though many of us are completely happy with things as they are, KDE does have (minor) usability issues. If that were not the case we would not be hearing the things we are hearing. But what is hapenning, as it often does, is that some of these views are not being acknowledged, and a polarization is occuring.\n\nThose of who adore KDE because it lets us set it up they way we want it, have the most to lose by letting KDE be tampered with; when the evangelizers point to MacOSX and Gnome as their champions, we start to get really scared, as though they were recommending castration!\n\nBut the KDE developers are a very intelligent bunch. I am convinced that if, despite any reservations, the KDE maintainers can take the message to heart, then the result will be a good one. Even though we might not yet be ready to admit it, a future KDE that is simpler to use, and as powerful, if not more powerful, will be something we will embrace once we can see that no permanent damage was done.\n\nGeorge mentions the issues with slimming down menus/context menus. That is a really tricky area! I followed some of the conversations on the Bugzilla lists, and almost went mad that people were advocating the removal of the options I use all the time. Of course, by the same token, I would happyily agree to the removal of everything you need most, provided I don't need it! There is a pattern emerging here: what I wan't to see, is not necesarilly what you want to see.\n\nWin2K introduced the concept of hiding rarely used options. Provided options are not moved around, and the complete menu can be viewed again with ease, there are no negative usabilty issues with that approach. Although care needs to be taken with the algorithm used in the decision making process, this little bit of statistics is AI at it's easiest. If I use an option then I am going to want to see it, and if I don't, then I won't. No XML editing required.\n\nCould we not move slowly through these issues by taking simple non-invasive approaches like these?"
    author: "Dominic Chambers"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "As a KDE user since 2.x (I wouldn't dream of defecting to GNOME, I can't stand what they've done with 2.x) I want KDE simplified to enhance usability. The clutter on the toolbar and context menus makes them useless to me, because it forces me to scan through the toolbar to find what I want. If I'm going to be scanning anyway, I might as well just use the main menu, where at least things are spelled with with words and things are hierarchically arranged. \n\nOn my desktop, I have probably tweeked every single feature in KControl. Yet, I wouldn't mind some of the more esoteric stuff broken off into a seperate \"advanced tweekers\" app, if that would get more users and support for KDE. \n\nSimplification doesn't have to mean a loss of power. Done properly, it just means that power is easier and faster to get to."
    author: "Rayiner Hashem"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "\"The clutter on the toolbar and context menus makes them useless to me, because it forces me to scan through the toolbar to find what I want.\"\n\nFirst, it's pretty extremist to call something \"useless\" just because of some unused icons, secondly, the whole point of icons is to recognize them easily and once you found the wanted icon you shouldn't have any troubles finding it again if it is in the same place, no matter how many icons are around it, thirdly you are arguing against your own point: EXACTLY BECAUSE THE DEFAULTS ARE NOT PERFECT FOR EVERYBODY, we need customizability. Don't like the toolbar? Configure it! If some self-proclaimed \"usability expert\" would say that toolbar-editing is only for the \"vocal minority\" and remove the ability to edit toolbars, you would be screwed, so be careful what you wish for.\n\nI never said that the defaults can't be improved. I just say that under no circumstances remove configuration settings.\n\n\"On my desktop, I have probably tweeked every single feature in KControl. Yet, I wouldn't mind some of the more esoteric stuff broken off into a seperate \"advanced tweekers\" app, if that would get more users and support for KDE.\"\n\nSome points:\n\n-) How would that make anything easier? If you don't find your configuration setting in app A you have to look at app B or vice versa, it's getting more complex and NOT easier.\n\n-) Different users have different fields of expertise. I am a programmer, but I have no doubt that an average secretary would need many more settings in KWord than I. So who's the \"advanced user\"? Please, get it in your head: There is no such thing as \"perfect setting for everybody\" and there are also no \"experience levels\". Every user is different. There is no \"average user\" and finding settings that \"only average users will need\" is impossible.\n\n-) Getting help is getting harder. Novice users will of course ask advanced users, who will of course not know what options are in the novice settings and what aren't. Yet another layer of complexity and confusion.\n\n-) The whole issue is overblown anyway. I just need to set settings *ONCE*, but I use applications *EVERY DAY FOR YEARS*. Streamlining applications AND ADDING MORE CONFIGURATION SETTINGS TO DO SO is much more important than streamlining configuration.\n\nI have helped a lot of users with Linux and that's what the real problems are:\n\n-) They just fear the unknown. They are willing to spend days, even weeks in solving some Windows problem, but will give up on the slightest Linux problem. KDE/Linux just needs more visibility and success stories. Marketing to put it in other words.\n\n-) Hardware problems like \"my USB printer doesn't work\" or \"my PocketPC doesn't synchronize\".\n\n-) And of course the biggest problem is software compatibility. Every Windows users has a lot of applications he doesn't want to lose. If you really want to get masses of Windows users to KDE, integrate Wine into it. Make sure Wine-apps are automatically added to the K-menu and make sure they work.\n\n\n\n\n\n \n"
    author: "Roland"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "First, it's pretty extremist to call something \"useless\" just because of some unused icons\n>>>>>>>>>>>\nNot really. What is the point of the toolbar? The toolbar is there to allow you to quickly access actions without having to go to the menu. If it is just as quick for me to go to the menu as to wade through a huge toolbar full of extraneous icons, then it is completely useless. Its even more important for the context menus, where the icons are too small to really be useful as a guide. In that case, a menu with 18 items (like the original Konqueror one) is nearly useless (at least its still context-sensitive), because you have to linearly scan through all those items to use it.\n\nsecondly, the whole point of icons is to recognize them easily and once you found the wanted icon you shouldn't have any troubles finding it again if it is in the same place, no matter how many icons are around it\n>>>>>>>>>>>>>\nThen you must have a better memory than I do. Its easy to keep it all in your head when there are less than a dozen entries, but when you get Microsoft Office style monster toolbars, there is little you can do but search through them linearly.\n\nDon't like the toolbar? Configure it! \n>>>>>>>>>>>>>\nI do. I spent hours getting every single KDE app I use regularly to have sane toolbars. I don't think other people should have to go through that though. I assert that the way I have my toolbars configured is more appropriate for the majority of users than KDE's default ones. I could be wrong, but the fact that toolbar minimalism is a part of GNOME's, Microsoft's, and Apple's HIGs lend support for my argument.\n\nIf some self-proclaimed \"usability expert\" would say that toolbar-editing is only for the \"vocal minority\" and remove the ability to edit toolbars, you would be screwed, so be careful what you wish for.\n>>>>>>>>>>>>>>\nNobody is saying that we should listen to self-proclaimed usability experts. However, it would be wise to take into account some basic, well-understood, non-controversial GUI design research. This is where GNOME went wrong. Instead of smartly applying basic principles, they went hog-wild and tried to apply a single simplistic ideology (less is better) all over the system, even where it wasn't needed. We don't want to go the way of GNOME, but that shouldn't stop us from taking advantage of GUI design knowledge when its appropriate.\n\nI never said that the defaults can't be improved. I just say that under no circumstances remove configuration settings.\n>>>>>>>>>\nI'm not saying we should remove configuration settings. Maybe move some KControl stuff to a seperate utility, but not remove them. I use pretty much every KControl feature. I'd hate it if they were removed. But I can put up with starting a seperate program if it'll make the mainstream happy.\n\nHow would that make anything easier? If you don't find your configuration setting in app A you have to look at app B or vice versa, it's getting more complex and NOT easier.\n>>>>>>>>>>>\nApp A would have well-chosen defaults. It would be for users who didn't even know there was an App B. If they couldn't find the feature they were looking for in there, they'd just think it didn't exist. App B would be for advanced users. It really wouldn't be a problem of hunting between the two apps, because App A would only have a few options, and App B would have everything else. This mechanism works surprisingly well for MacOS X and Windows.\n\nDifferent users have different fields of expertise. I am a programmer, but I have no doubt that an average secretary would need many more settings in KWord than I. \n>>>>>>>>>>>>>>\nI'm just talking about KControl. The basic desktop needs to be accessible to everybody, which is why it is worth it to go to a little extra effort to make something like KControl more accessible. Its not worth it for something like KWord.\n\nand finding settings that \"only average users will need\" is impossible.\n>>>>>>>>>>>>>>\nNot really. First, there are average users. Most people using Windows machines don't do anything with them but launch games, run Office, check email, and surf the internet. Microsoft (and Apple) design the defaults for those users. They don't seem to be having any problems finding those settings, because its really not hard. And you're really in no position to claim otherwise, because they (as in Microsoft, Apple, and even GNOME) have done research on what 'average users' need and you haven't."
    author: "Rayiner Hashem"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "<i>Yeah, right. That's why KDE is leading over GNOME in every poll I saw, right?</i>\n\nAnd do you believe that situation is going to continue?\n\n<i>Everytime somebody talks about \"vocal minorities\" you can just substitute everything he said with \"I just made it all up and I don't have proof for anything\".</i>\n\nOr you could substitute, \"I have a problem here but everyone says it's not a problem.\"\n\n<i>FACT is that KDE is a lot more successful than Gnome at actual users. GNOME is more successful in finding companies (like Sun) pushing it.</i>\n\nWell, KDE is a lot older, and has had a lot more time to attract users, I will give you that much.\n\n<i>A cynic might say that KDE-bashers want KDE to \"simplify\" only because they want it to fail and fall behind GNOME....</i>\n\nPlease don't think I'm a KDE-basher. My first linux desktop was actually KDE, and I had been using it since the 2.x days. In those days it was so much better than GNOME, but I see GNOME catching up quickly, <i>especially</i> in the area of user friendliness.\n\nNow I realise this is a hollow talk, \"get up and do something about it, I hear you say\". I know I should but I have neither the skills, nor the time, nor (and this is the most important thing) the patience to argue with people who seem unwilling to see (what I see as...) a broader desktop world out there. \n\nMaybe you're right and I'm wrong, maybe I'm right and you're wrong, only time will tell, but *please* listen to us, if only for your own sake."
    author: "Tom Holte"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "> Well, KDE is a lot older, and has had a lot more time to attract users,\n> I will give you that much.\n\nAccording to the history books, GNOME is 76 months old, whereas KDE is 86 months.\nMakes a difference of 13 %, hardly 'a lot'."
    author: "Bernd Gehrmann"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "Well, at least I have put up some evidence supporting my point:\n\n- MS Office is extremely bloated, yet popular among low-skill-level users\n- OpenOffice is also extremely bloated, yet more popular than KOffice or Abiword on Linux.\n- Computing hardware is becoming \"bloated\". Nowadays a lot computers come with serial, parallel, PS2, Firewire, USB1+2, LAN, several types of card-readers, a \"game port\", audio ports and loads of other ports, often even a redundant VGA-port. In comparison, the so-called \"legacy-free\" PCs and motherboards almost all were unpopular. Bloated won again.\n- All distributions are \"bloated\" and include millions of features which won't get used in some particular installation, yet almost nobody runs a streamlined \"Linux from scratch\" installation.\n\nUsers love features (= bloat).\n\nI made several points illustrating that. If you could come up with just one example of an \"unbloated\" application/product triumphing over an already established app because of it's lack of bloat, you would have some credibility, but like that it's just claims and hot air.\n"
    author: "Roland"
  - subject: "Re: Some good points"
    date: 2003-12-18
    body: "Okay firstly, I assume:\n\nbloated == features / features you don't use\n\nWhich makes 'bloat' at least 50% subjective.\n\nTo a few of your points:\n\n1) I'm sure you would accept that MS Office's market share is not purely based upon its feature set.\n\n2) I agree at most 50%. OpenOffice has more features (useful to you or not) than KOffice or Abiword.\n\nBut I feel you are missing the point. I far as I can tell (and speaking for myself,) the people here are not arguing for removal of features, but for simplification of the *default interface*.\n\nIn terms of proof, just look at the default toolbars for an Office XP application compared to a KOffice application. The <i>initial setup</i> for Office XP is far less intimidating to new users, simply because they are not immediately innundated with buttons. As they become more familiar with the application, they can add toolbars."
    author: "Tom Holte"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "as a non-kde and non-gnome user (E0.16+Rox filer are fine for me, my computer is \"only\" 600 MHz), I can say that KDE made more good decisions to convince me to use its software and environment that Gnome in the past 2 years.\nMany very useful features were removed from GTK and Gnome in the name of \"HIG\" (Holy Integrist Guideline?), and drove me away from Gnome (think about Metacity which has absolutely no functionality for managing windows as the most obvious example of bad decision). If only KDE had a lighter look as gnome... (yes \"look\": colour scheme, icons, toolkit appearance). And if only I could DnD from Konqueror to Gimp 1.3.x. (ok, I have tp check with the newest Mandrake if this works now)"
    author: "oliv"
  - subject: "Re: Some good points"
    date: 2003-12-17
    body: ">And if only I could DnD from Konqueror to Gimp 1.3.x. (ok, I have tp check with the newest Mandrake if this works now)\n\nJesus, it finally works!!!! I can close the old bug report about DnD not working between Gnome and KDE :)\n"
    author: "oliv"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "It seems like you have just designed yourself a well defined task: write an XML GUI file editor for KDE.  Then you would have a GUI menu editor that would make it easy to customize your menu.  You have to realize that everyone draws their line in a different place and it's impossible to make everyone happy.  We do provide the tools for everyone to make himself happy though."
    author: "George Staikos"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "> Icons on buttons are not on by default because not everyone has a computer as fast as yours. It costs to add those icons.\n\nWhat, should icons be removed from toolbars as well? It only costs as much in toolbars as in buttons."
    author: "anon"
  - subject: "Re: Some good points"
    date: 2003-12-15
    body: "No cookie for you.  Pushbuttons, ex. on messageboxes, are created and destroyed regularly throughout application execution.  Toolbars are created only once at application startup.  Furthermore toolbars only show icons by default, not text and icons."
    author: "George Staikos"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "I like cascading menus, as long as there is only one submenu."
    author: "Quique"
  - subject: "Re: Some good points"
    date: 2003-12-16
    body: "There's one thing I don't quite get: are the M$ \"rarely used menu items\" (the \"v\" at the bottom of a menu) really such a bad idea? Why on earth not have a powerful context menu and just expose whatever is frequently used? That way if you want to do something less-than-usual, you just expand it. Doesn't take too much time, does it?\n\nKuba"
    author: "Kuba"
  - subject: "Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "Everything is subjective, and most of her suggestions would drive me nuts.\n\nI think the KDE folks are surely capable of figuring out what needs to be done on their own.   I am immensely happy with kde, and while there are a few small issue for me, I find that KDE is head and shoulders above any other environment, including Mac OS X.  I just don't want it ruined by following well meaning suggestions such as hiding configuration options.\n\nAbout the only thing I liked in her review is the spacing suggestions and Plastik as the default.  _maybe_ the lines around the toolbar."
    author: "TomL"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "Most KDE users might now be able, but will KDE users in the future, who might have a lower skill-level?\n\nKDE definately is cluttered in places where it doesn't really buy you anything, just loses you efficiency and ease-of-use. And I say that as a full-time KDE user. Would anyone, for example, mind the advanced options in KControl being moved out to a seperate app like in Microsoft Powertoys? GNOME clearly goes too far in the direction of minimalism (hell, even MacOS isn't that sparse!) but KDE needs to move more towards the center as well."
    author: "Rayiner Hashem"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "\"Most KDE users might now be able, but will KDE users in the future, who might have a lower skill-level?\"\n\nSo let me get that straight what you want:\n\nYou want to piss off existing users and developers to please non-existing \"future\" users who *might* like a dumbed down interface?\n\nWell I think that sums the stupidity up pretty well.\n\n\"KDE definately is cluttered in places where it doesn't really buy you anything, just loses you efficiency and ease-of-use.\"\n\nThat's nonsense, simply because you usually change settings only *once*, but you use an application for *years*. So if you (oh my god!) need an extra 20 seconds to find some setting it's 20 seconds lost. If you lose 10 seconds every day because some smartass thought that the \"average user\" doesn't need that setting and (re)moved it, you lose an hour in the first year - and you get frustrated.\n\n\"Would anyone, for example, mind the advanced options in KControl being moved out to a seperate app like in Microsoft Powertoys?\"\n\nI would.\n\n"
    author: "Roland"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "Hear! Hear!\n\nI would be pissed off too if \"advanced\" settings (advanced for what? for whom?) would be moved to a different application. If people complain they can't find the options now, how are they supposed to find them if you split them over several applications?!"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "Hold on. Nobody is talking about splitting things up over several applications. There would be a seperate \"advanced\" tweeking application for power-user features, like Microsoft's PowerToys. Its not several applications, but one.\n\nLook: I take KDE's configurability to the extreme. I use nearly every option in KControl, all the apps I use regularly have had their toolbars reconfigured, etc. Hell, I regularly patch my themes to change things I don't like! Yet, I still can't understand why anybody would have trouble installing and using a seperate application for advanced tweeking."
    author: "Rayiner Hashem"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "Last time I looked, powertoys was a separate application from the normal Microsoft configuration (however, it has been awhile ^_^). I really don't see the point of splitting options that belong together into separate applications. Sure, split them over different tabs, even use an Advanced button if you must, but please keep the options together. I don't want to have to look in two applications to find the option I need, because I don't know if the author decided it's an advanced option or not."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "Its probably not as bad as you think. Under the two-application scheme, KControl would have only the most simple and obvious options: changing fonts, changing themes, changing resolutions, etc. The advanced app would have everything else. "
    author: "Rayiner Hashem"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-17
    body: "You, distributions and everyone else are free to create as many option tweaking tools or levels as they like, you don't even need to be able to program to be able to offer customized solutions, just use kcmshell and add the components you want. If you don't know what components are available search for kcm_* in KDE's library folder.\n\nExample (me using KDE 3.1.4 here right now, so components my have changed their name): You only want access to the desktop background picture, icon and keyboard setting, execute following command (or better create it as icon):\nkcmshell background icons keyboard"
    author: "Datschge"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "Very good, Roland!  You worded this perfectly, and the point is right on!"
    author: "George Staikos"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "You want to piss off existing users and developers to please non-existing \"future\" users who *might* like a dumbed down interface?\n>>>>>>>>\nNobody is talking about dumbing down the interface. I am not, repeat, am NOT, suggesting we go down the GNOME path. I am, however, saying that we should simplify the interface in places that would make things easier and more efficient for everybody. Its not too different from what KDE is doing already. Does anybody really remember, or miss, the three or four options that were removed in 3.2's KHTML menu? If those changes are acceptable to the KDE community, I think that there are a lot more changes like that which could be made that would be equally acceptable.\n\nThat's nonsense, simply because you usually change settings only *once*, but you use an application for *years*. \n>>>>>>>>>>>>>\nIn this respect, I'm not talking so much about settings, but extraneous toolbar and context menu items. These slow you down every time you use the menu or toolbar. Since these are configurable, if it really helps your workflow to have them, you can always add them, right? And by your own logic, the extra 20 seconds it takes you to add those options back is nothing compared to the efficiency gain over using that application for years.\n\nI would.\n>>>>>>>>>>>>>\nWhy?"
    author: "Rayiner Hashem"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "\"Nobody is talking about dumbing down the interface. I am not, repeat, am NOT, suggesting we go down the GNOME path. I am, however, saying that we should simplify the interface in places that would make things easier and more efficient for everybody.\"\n\nIf you argue about tweaking the defaults, I agree.\n\nIf you talk about removing/splitting/seperating options, I strongly disagree.\n\nMS Office is probably the most bloated application in existence, yet nobody can claim that it isn't widely used. Users want features, features, features. Every  user wants different features and in the end the only way to satisfy all users is to have a bloated app.\n\n\"Why?\" [a seperate kcontrol would be a bad idea]\n\nBecause:\n\n-) I love to scan kcontrol for new configuration settings and new features. I often found interesting features that way which I would have never found if they were hidden.\n\n-) Searching for configuration options in seperate apps is more complicated than searching in a single application, it's just that simple.\n\n-) It would not make KDE more attractive, to the contrary, it would needlessly add confusion and frustration.\n\nIt's not fair to compare KDE with Windows. We are not preinstalled on almost every desktop computer. We don't have any serious gaming library. However we can compare KDE with Gnome, Blackbox, FVWM, etc. And KDE is the *MOST SUCCESSFUL OF ALL* so KDE must be doing something right.\n\n\n"
    author: "Roland"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-15
    body: "If you argue about tweaking the defaults, I agree.\n>>>>>>>\nI'm thinking bigger than tweeking, but nothing fundemental. Basically, I'd like to see the stuff that was done from 3.1 to 3.2 (especially to Konqueror) be applied to the rest of the system. \n\nMS Office is probably the most bloated application in existence, yet nobody can claim that it isn't widely used. \n>>>>>>>>>\nI don't think anybody *wants* to use MS Office. We can do better than that.\n\n-) I love to scan kcontrol for new configuration settings and new features. I often found interesting features that way which I would have never found if they were hidden.\n>>>>>>>>>>>\nSo? Nobody is saying to hide them. They'd just be moved to a seperate app, and you'd scan that. New features wouldn't generally get added to the basic KControl.\n\n-) Searching for configuration options in seperate apps is more complicated than searching in a single application, it's just that simple.\n>>>>>>>>>>>>>\nYou have to consider the two types of users at work here. New users would just use the basic app. They'd not even be aware that the other app existed. Advanced users would already know the functionality of the basic app, because it would have only basic options and wouldn't change often, and would know that everything else was in the advanced app.\n\n-) It would not make KDE more attractive, to the contrary, it would needlessly add confusion and frustration.\n>>>>>>>>>>>\nYou can't be sure of that. Indeed, experience indicates that it has worked fine for Windows and MacOS.\n\nIt's not fair to compare KDE with Windows.\n>>>>>>>>>>>\nYou're right, its not. We're beyond Windows. But Windows isn't a good target. Its GUI was never very good. It still isn't. Microsoft Office's interface epitomizes the whole \"toolbar/context menu clutter\" problem. Our goal should be to match the ease (and efficiency) of use of GUIs that people actually liked, like MacOS (Classic and to a lesser extent X), BeOS, NeXT, etc. "
    author: "Rayiner Hashem"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-16
    body: "Drop it. This idea of separating config options stinks. It _is_ more complicated. I think everyone can agree that KDE usability can be hugely improved without pissing anyone off. This _would_ piss people off, including me. Since there is so much low hanging fruit, why even bother persisting with these other half-baked ideas? Until the simpler changes are made we can not truly determine the best way to proceed forwards.\n\nThere are four areas where I can imagine things could be improved in an incremental stepwise manner:\n\n  1. Solve the context menu issue (I like the Win2K solution of hiding items\n     that are never used anyway -- an adaptive inteligent system).\n  2. Perform usability analysis and figure out what should go where (toolbars\n     and app menus). See where problems occur and optimize the out-of-the-box\n     settings for the mythical average user.\n  3. Improve the settings panels in various apps and control center so that\n     they are better worded/organized.\n  4. Provide task oriented How Tos to complement the application manuals.\n     Something more advanced then this is a window, based on areas were users\n     actually get lost.\n\nIf there was a real will to do it, all of that could be achieved for KDE 3.3, and then, and only then, should we ask is we need to go any futher.\n\nJust my penny's worth.\n"
    author: "Dominic Chambers"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-16
    body: "I dunno how much low-hanging fruit there is. The developers made some pretty basic and obvious changes to the KHTML context menus, and there was an outcry over it. I think almost any simplification you do to KDE is going to meet with a lot of resistence."
    author: "Rayiner Hashem"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-16
    body: "I think there's lots. Context menus should certainly only show items when they are contextually relevant, and that isn't always the case with KDE 3.1. I made a lot of noise too, because people were proposing the removal of items I use all the time, so I can understand that.\n\nBut, and I have made this point twice already in this forum, and am feeling like a naughty repeat poster, they could very easily hide items that are never/rarely used (statistically speaking) like Win2K does for the Start Menu. That is a good idea that solves the problem, harms nobody, and is definitely low hanging fruit.\n\nThe same technique could be used for the KDE menu to solve the too many text editors dilema. Other possibilities include cleaning up some of the setings menus. Some are almost incomprehensible (e.g. some of KMail's dialogs) and many could be better worded or organised.\n\nMeasures like this are simple, if not technically, then politically, since they do no harm to anyone. Since they can be made before more drastic measures are taken, and since we will not no how much further we need to go until then, that is what we should do first. That, in my opinion, is the low hanging fruit.\n"
    author: "Dominic Chambers"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-17
    body: "> 1. Solve the context menu issue (I like the Win2K solution of hiding items\n> that are never used anyway -- an adaptive inteligent system).\nWell... if they ever decide to implement this, I really hope it can be turned off. I absolutely loathe this feature in windows (the few times I'm forced to work with it). Really. If I click on a menu, I'd actually like to see that menu. I find it confusing that the menu doesn't seems to be complete."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Please ignore most of her suggestions!"
    date: 2003-12-16
    body: "why not just have the \"basic\" controls as the first few controls in kcontrol? or perhaps stick them in a seperate tree that defaults to being unfolded? the rest of the options can be in their normal places, with the tyree folded up so there is no visible clutter for basic users.\n\nanother idea would be to make the kcontrol folders actually a kcontrol module themselves that contains the basic, most used options for everything in that section. that way you have the basic controls a singlwe click away, and by clicking on the icon for that module again, it unfolds and shows the rest of the modules.\n\njust a few ideas"
    author: "ben klopfenstein"
  - subject: "Kontrol center"
    date: 2003-12-15
    body: "I agree with some of the criticisms. Having used KDE 3.1.2 for a while, I felt the Kontrol center was not presented properly. I haven't used KDE 3.2 since my comp is down. \n\nThere should be a distinction between Superuser options (those that require the root password) and User options. They should be kept separate. For example the Login manager option (kdm settings) clearly doesn't belong to a non-priviledged user's option. What does he get from the option apart from a disabled dialog box (and an admin mode button)? Sure some are a bit difficult to decide, like font installer, which belongs to both places?!?. That could be decided, but the point is that it removes a lot of clutter from that one Kontrol center app, and we have a clear reason to differentiate them.\n\nOverall, it give place for expansion of the Kontrol center. If someone writes a Kontrol center app to, say, set up hardware, we know where it should belong. On the other hand if someone writes a working integrated theme app (not the 3.1.x legacy crap, hope that's done with), we know where that belongs too. The Information on hardware etc. and perhaps kuser should be in superuser Kontrol center, while user password, user information etc should be in the user Kontrol center. \n\n>Konqueror's context menu is a mess, why would I want to zip a web page or use Cervicia with it, is beyond me\n\nI agree with that. The developer's viewpoint shows. If we want new people and non-developers to use KDE, that option should be shunted somewhere else. Zipping (or 'war'ing a web page) makes sense as an option, but I doubt the context menu means 'war'ing the web page.\n\nI would like to hear if some of these changes are already in the works (as it so often is:-) Thanks."
    author: "Rithvik"
  - subject: "Zipping(warring) the file."
    date: 2003-12-15
    body: "Personally, I make heavy use of this. But it belongs in Location/Save, not as tool option."
    author: "a.c."
  - subject: "The part I liked"
    date: 2003-12-15
    body: "\"But it is so damned easy to develop for this platform that some people seem to... over-develop.\"\n\nI dont know if its true, but I like the sound of that. Maybe KDE should use it to atract developers"
    author: "Mario"
  - subject: "Re: The part I liked"
    date: 2003-12-15
    body: "No sure about over-develop, but compared to anything else, ( Windows, Gnome, WxWindows ) It is the easiest.\n\n"
    author: "Leonscape"
  - subject: "She is 100% correct on the analysis."
    date: 2003-12-15
    body: "She comes pretty close on the prescription as well.\n\nAfter reading her last review of the latest Gnome (which was very positive) I was somewhat interested to see what she would say about KDE.  Most of here previous articles reviewing KDE were scathing.  I kind of expected the same for this.  However, that is not what I read.\n\nMy synopsis of her article is that she does actually like KDE and would love it if it could symplify the last front of configuration.  I didn't think I would ever hear her say something as good and as positive about KDE as this article does.\n\nI agree completely with her assessment of Plastic.  It is a beautiful theme.  I agree that we shouldn't change styles mid major release.  I also know though, that I used keramik for 3 days before going back to Mandrake Galaxy (I do still use the keramik window decoration).  I will use Plastic once I switch to 3.2 full time.\n\nI agree that having four text editors from the same desktop in the same menu is probably too much, although having two is not unreasonable.  Quick poll - how many people use the menus to access the text editor, how many use a konsole to start them, how many use \"Alt-F2 kwrite\" to start them, how many have a dedicated global key shortcut?.  The same developers who care that we have four text editors probably don't use the menus to start them.\n\nI agree that we need better configuration dialogs.  Understand though, that when I first load a desktop environment, the first thing I do is go through every configuration option I can find to see what range of options are available.  Same with applications such as konqueror, kopete, and any other application (usually I use an application for a few moments before diving in to see what extra options are available).  I love the number of options that KDE has.  I don't really want them to go away.  However, I would like it if they were a little bit easier to follow for the simple options.  KDE is at the point especially with 3.2 that I would install it on my mom's computer.  My immediate family has used it for sometime - but it is usable enough for my mother, dad, fill in any name here.  That is until they need to change a configuration item.  Beginners really need to have only the pertinent options presented in a simple and concise manner.  Period (ok - maybe not period - but I would offer a challenge to find any research other than personal observation that would suggest that showing more items at a time is easier to grasp than showing only a few items).  I don't know the best way to keep all of the advanced items - we shouldn't get rid of them.  But we really should only present the default simple options to the new user - even if that new user uses the configuration only once in awhile.\n\nI agree that there shouldn't be so many options under the \"Settings\" menu (Configure shortcuts, Configure toolbars, Configure konqueror, Configure spell checker).  The options seem redundant, even if they aren't.  They probably ought to be merged into one \"Settings\" sub menu item like she suggested.  With shortcuts, toolbars, and spellchecker listed as components in the konqueror configuration dialog.  I often choose the wrong \"Configure\" sub menu item and I'm sure many other people would agree that they have choosen the wrong item at least once.\n\nFinally, I agree on the context menus.  I like that they have gotten better.  However, some options could use better explanation and could be collapsed.  For example, I love Quanta - but I don't think it should be on the main context menu.  The \"Preview In\" submenu is a little bit misnamed and the options in preview (\"Embedded Advanced Text Editor\" and \"Embedded Vim Component\") are a little bit over the top for most users.  It would be much better to have an \"Open with\" sub menu that contained quanta, and the different editors.  I like the gnome idea for the \"Open\" items on context menus (Show the main mime handler and then a submenu of other handlers - Although if you are using the application that is the main mime handler to get the context menu - you probably shouldn't show that application in the context menu).\n\nAny way - I've rambled on.  I think she is 100% correct.  She seemed to have nothing but praise for the latest work and the speed and the framework and seemed to just be screaming to finally fix one of the last perceived sore spots of the KDE desktop (configuration is such a sore spot that any 3 developers would give 5 opinions about how to do it)."
    author: "PaulSeamons"
  - subject: "And next time I'll work on my spelling"
    date: 2003-12-15
    body: "Sigh..."
    author: "PaulSeamons"
  - subject: "Re: And next time I'll work on my spelling"
    date: 2003-12-15
    body: "In 3.2? right-click the input and select check spelling... from the context menu."
    author: "Leonscape"
  - subject: "Re: And next time I'll work on my spelling"
    date: 2003-12-16
    body: "+5, Insightful."
    author: "Bill"
  - subject: "underlying technology"
    date: 2003-12-15
    body: "Euigenia concludes that it's easier to use KDE's good application framework and polish the apps, than it is to extend Gnome's framework to KDE's levels. \n\nIt made me kinda optimistic to see her give KDE that recognition. I certainly didn't expect it!"
    author: "Apollo Creed"
  - subject: "Excuse me, but..."
    date: 2003-12-15
    body: "It may sound like cursing in the middle of a church, but... why exactly do people want KDE to be usable/understandable/acceptable for any Joe User? I see a lot of comments here and on other places that people thing that Joe User won't be able to understand all the options, that he will be scared away, etc. So what? If Joe isn't willing to spend some time learning KDE, or feels happier using GNOME or even windows, why exactly do we care? Isn't there a place for a DE that fits the needs of 'advanced' users who actually *like* this so-called 'clutter'? \n\nI think in the end, it's not Joe User who helps to move KDE forward for us all. It is the advanced users who do, who take their time filing usefull bugreports, or even fix problems themselves. It is they who come up with suggestions for new applications or features that help the current userbase. Really, can anybody explain to me what exactly is the benefit of KDE conquering Joe Users desktop?"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-15
    body: "True.  Joe User will not help to advance anything directly.  But I would imagine that most of the people using Linux are hoping for the day when a multitude of Joe Users who are using Linux will have enough mass to bring about a slew of software for Linux.  Yes, we can continue to write software to fill all of our itches and avoid all proprietary systems like the plague, but at some time we will want to use a niche program by a company writing software for that niche - and we won't want to take the weeks or possibly months or possibly years to develop it ourselves (as an example I would really love to have some Garmin GPS software that works under Linux).\n\nWe can write companies, we can lobby.  We can yell at them.  We can ignore them.  Companies using a non-free software model, who still write useful software that we may actually want to use, will not listen until there is enough demand.  That demand can be inspired by a host of Joe Users.  Joe by himself is not very loud.\n\nThis would be the proverbial Chicken and Egg problem of applications for Linux."
    author: "PaulSeamons"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-15
    body: "Well, I do understand your argument, but I don't think that the lure of commercial niche software somewhere on the horizon is worth sacraficing features ('bloat' as others call it) people use _now_. I think KDE has enough critical mass as it is, and is still gaining momentum without these sacrafices.\n\nAs for working GPS software: what are you looking for? I'm currently working on a Garmin plugin for KFLog (http://www.kflog.org)..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-15
    body: "Very cool on the KFLog stuff.  I had downloaded and worked with it a bit.  Very nice.  However the maps don't always have as great a degree of detail that I can get in the Garmin maps.  I know there are ways to digitize maps but they aren't exactly user friendly.  Then, trying to get the map into a garmin format and downloaded to the GPS is even more difficult.  I wish Garmin was a bit more open in this regard.\n\nOn the other note.  I am not calling for the removal of features (Especially not in any of my posts in this topic).  But the configuration of KDE needs to be made easier - however it is done.  Why does it need to be made easier?  Why not - and what would it hurt?  Is there a maximum level we take it to?  Certainly.  Do I have much room to talk about KDE development?  Not much as I haven't contributed to the code base as of yet - but some because I have some experience using it."
    author: "PaulSeamons"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-15
    body: "I'm glad you like KFLog. The maps are as detailed as the sourcemaps we can find... Unfortunatly, especially the maps on roads, railroads, cities, etc, tend to be very old and inaccurate :-( If you know of any good source of data, please let us know...\nI don't know how to get the maps into the Garmin. I'm sorry. It's not in the interface specs I have at my disposal.\n\n"
    author: "Andr\u00e9 Somers"
  - subject: "Another reason..."
    date: 2003-12-15
    body: "If all we are doing this for is for ourselves, aren't we a sorry bunch."
    author: "PaulSeamons"
  - subject: "Re: Another reason..."
    date: 2003-12-15
    body: "Uh... not at all.\n\nAre you a sorry individual if you sell something trying to earn money for yourself?\n\nAre you a sorry individual if you have a hobby that is \"just for yourself\"?\n\nYou are not just wrong, you are incredibly, what-a-heckishly wrong."
    author: "Roberto Alsina"
  - subject: "Re: Another reason..."
    date: 2003-12-15
    body: "Do you have a family?\nYes? You're helping to supply their livelihood.\nNo? Do you plan on having a family?\nYes? You're preparing for them.\nNo? Are you just trying to survive?\nYes? I'm sorry for you.\nNo? I'm sorry for you.\nWanting to earn money for myself is not bad as a means.  But as an ends it is pretty hollow.\n\nOn the topic of a hobby.  I agree that there are some things that we may do that appear to be only for ourselves that bring us levels of satisfaction - such as coin collecting, or painting, or hiking, or developing software.  But if I spend my life in that hobby, or put time away into it, or cherish it, or all three, and not ever share it with another person - I would evaluate that as being pretty sorry.  However, the chance of a hobby ever being truly just for yourself is pretty slim - as even your posterity or the history books will look on your life and will remember that hobby and will see it with either admiration or pity.  And if the hobby isn't self fulfilling or is destructive in nature - well I'm sorry for that too.\n\nDoing something extra for the simple sake of doing something right may be thought to only serve self, but really is of benefit to humanity.  In effect, anything good you do - whether intentional or not will affect somebody else.  Any thing bad, likewise.\n\nDo I do things or have hobbies that are \"just for myself?\"  Of course.  But the more I look at those hobbies and the missed time I could have been with a loved one - the sorrier I am.\n\nThe application of the term \"sorry\" is open for interpretation.  Am I offbase?  Probably, but I have found ways to avoid being sorry later on."
    author: "PaulSeamons"
  - subject: "Re: Another reason..."
    date: 2003-12-16
    body: "Do I have a family? Girlfriend, she makes about the same money I do.\n\nI find your post stupid and smarmy. All touchy-feely new-agey and bland.\n\nI feel sorry for you if you really are like that."
    author: "Roberto Alsina"
  - subject: "Re: Another reason..."
    date: 2003-12-16
    body: "I'm often stupid and I'm sure I'm often smarmy.  When I am old and sitting on my porch with my wife and the kids have moved away and I have a moment, I imagine I'll reflect on how smarmy I've been - but I probably won't be sorry."
    author: "PaulSeamons"
  - subject: "Re: Another reason..."
    date: 2003-12-16
    body: "The sole thought of devolving into that line of thought makes me want to become a Soylent Green advertiser.\n\nThere are many many many selfish people who are married, and they will be healthier and have better porches than you."
    author: "Roberto Alsina"
  - subject: "Re: Another reason..."
    date: 2003-12-16
    body: "And if I worry about the grass on the other side of the fence - well - once again I'm a pretty sorry guy - which is what this little thread started with anyway."
    author: "PaulSeamons"
  - subject: "Re: Another reason..."
    date: 2003-12-16
    body: "> But if I spend my life in that hobby ... and not ever share it with another\n> person - I would evaluate that as being pretty sorry.\n\nBut KDE is open for all to share.  It's not a coin collection that is kept in a safe and is never seen by anyone but the collector.  Thousands of people who have never contributed to the project -- myself included -- have benefitted from it. Just because IBM and Sun aren't trying to cram a customized KDE down our throats doesn't make KDE a failure.\n"
    author: "Tukla"
  - subject: "Bingo!"
    date: 2003-12-16
    body: "Thats what I was saying."
    author: "PaulSeamons"
  - subject: "Re: Bingo!"
    date: 2003-12-17
    body: "The fact that is't shared doesn't mean it's done for the people you share it with."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Another reason..."
    date: 2003-12-15
    body: "No we are not, even if we were doing it all for ourselves. For my part, I'm doing it for people who'd like to contribute to the project themselves too. I think a lot of developers do. Didn't you notice the way developers are regulary demotivated by the demands from Joe User types? I did. I fear that will only get worse once the masses move in. I don't think the masses are ready to understand what Open Source is all about. I fear that may lay too big a strain on some projects, including KDE."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Another reason..."
    date: 2003-12-15
    body: "And what did you do for me today? =)"
    author: "Datschge"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-15
    body: "I hate putting it in terms of Joe User, because its really not so simple. Sure, configuration clutter might be a \"Joe User\" issue, because you only configure once. But what about menu and toolbar clutter? That's not only confusing Joe, but its also slowing down Vladamir the uber-user. That's why a more streamlined KDE (to a point!) is better for everyone."
    author: "Rayiner Hashem"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-16
    body: "Of course there are optimisations to be achieved, ways of interacting with KDE and it's applications to be streamlined, etc. I wasn't saying that KDE is finished or something like that, nor that we should not work on it or strive to be better.\n\nI was just wondering why some people are arguing that we should dumb-down KDE, hide options from the configurations panel, etc., just to please a Joe User. I know it can't be seen so black and white, but I was trying to raise a real question. The only argument I've seen so far, is that a larger userbase might attract companies that might supply niche products some users would like to use. For me, that simply isn't enough reason."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-16
    body: "Take a look at http://www.google.com/press/zeitgeist.html \"Operating Systems Used to Access Google - Sept. 2003\". Assuming that it is good measure of market share of Linux based workstations (and why it shouldn't be?), when we move just 1% up, we would get twice as much users and supposedly much more software developers. There are many Linux projects which are severely understaffed (LyX, Debian, etc.).\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: Excuse me, but..."
    date: 2003-12-16
    body: "Currently, the vast majority of non-linux users aren't developers. While you are right that attracting more developers would be good, I don't see how attracting Joe User type of users is going to help here. I think it would sooner be counter-productive, as developers would need to spend more time in support and/or could be scared away by user demands from people who don't understand how OSS works."
    author: "Andr\u00e9 Somers"
  - subject: "Configuration options and many applications"
    date: 2003-12-15
    body: "Compared to the amount of options in unix command line untilities, I have had no problem at all with KDE.\n\nI am desktop oriented person and I enjoy Unix/Linux much more nowdays, because\nI do not need to remember all those illogical and cryptic options that many\nold command line utilities suffer (I have also other things to remember).\nIt is much easier to find an option through menus and if there are many of them those can be hidden in separate configuration menus or dialogs if it is a problem, I also like that I can configure the look and feel just I like.\n\nAnd I hope that there will be plenty well supported applications, There are still many I miss, one does not have to install those that one does not like.\nKeramik in my opinion is more nice looking than plastik.\n\nT Jari\n"
    author: "jari s\u00f6derholm"
  - subject: "The really essential options and the rest"
    date: 2003-12-15
    body: "No doubt, the Control Center holds an enormous amounts of options. For a moment, lets take a look at what options in there that are most essential <i>to be able to</i> change. \n\nMy list of the most essential options are:\n\nCountry / Language (and here I mean chosing exactly country and language, and not details such as decimal symbol and negative sign for current locale)\nKeyboard layout\nDate & Time\nMouse (Changing button mapping and pointer acceleration, not much more)\nPrinters\n\nI may have missed some, but I find that the vast amount of options do not belong to the \"most essential\" category. Since KDE in itself does not configure all aspects of the OS, there are not so many really essential settings there. Many, many settings are of the power-user flavor though. Would it perhaps be a good idea to design an alternative, much simpler control center with only very basic options? I suspect it is both simpler and less likely would lead to flamewars. This alternative control center should be \"mom-proof\". \n\nHow would such a control center look? Lets not at the moment argue whether it is a good idea to have two control centers. Let's just pretend it is a good idea. How would it look? What would it contain? What are the really essential options, how can good defaults be destilled down to a minimal set of settings, for those who don't mess with settings unless there is an emergency?\n\n\n"
    author: "Claes"
  - subject: "Re: The really essential options and the rest"
    date: 2003-12-16
    body: "> How would such a control center look?\n\nActually, it would look just the same because the Control Center is just a container for KDE Control Modules.\n\nYou could have a Basic and an Advanced Control Center and the Basic one would not have all of the modules available in it.  But, to do what you appear to want, it would be necessary to have two versions of the KCMs as well.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "new startup notifications"
    date: 2003-12-15
    body: "ok, i just loaded kde 3.1.94 (cvs >=20031206) and one of the most irritating things from the windows world has crept into kde, and i can't figure out how to turn it off! \n\nwhenever a new window opens up, the taskbar flashes the new windows entry 3 times in blue before just staying there in blue until you click on it. i hate this. i have had to remove the taskbar until it can be fixed. if anyone can help me turn this %$#@*& off i would appreciate it immensely."
    author: "Elliott Martin"
  - subject: "Re: new startup notifications"
    date: 2003-12-16
    body: "I think you need to go to the KDE Control Center then select: Desktop->Window Behavior, then select the Advanced tab and adjust the \"Focus Stealing Prevention Level:\" to a lower setting.\n\nHave fun."
    author: "Giles"
  - subject: "Re: new startup notifications"
    date: 2003-12-16
    body: "that's really a shame that you have to disable focus stealing prevention to get rid of the annoying flashing in the taskbar...maybe whoever coded that never used windows 98."
    author: "Elliott Martin"
  - subject: "Re: new startup notifications"
    date: 2003-12-17
    body: "What do those setting do?\n\nI don't quite know what to make of \"extreme\", \"high\", \"low\"."
    author: "TomL"
  - subject: "Re: new startup notifications"
    date: 2003-12-19
    body: "if you press shift f1 and click on the text there it will give you some more information....not that it does much..."
    author: "Elliott Martin"
  - subject: "Fair review for the most part"
    date: 2003-12-15
    body: "hese are the kind of articles I love readinga t OSNEWS.COM. Criticisms with constructive suggestions, and many of the things mentioned would probably not be ramarked by anyone but Eugenia. Such as teh menu and toolbar separation, msot would just say it's cluttered and leave it at that. I agree with just about al of Eugenia's points, but I do wishs he went intoa lot more details regarding teh new features in this release, there are hundreds of new important features.\n\nAlso, I don't think Kcontrol is that bad, it does have too many options ins ome areas, but waht is more problematic is that they are not as well organized as they should be. Lots should be combined, sucha s Keyboards and Mice. But overall I think it is a very good ocntrol center and just needs a bit more polishing. If you don't wan to change the defaults you don't need to go in it.\n\nALSO remember that unliek GNOME it has a very easy to use search tool. There is a find tab in which you can type what you're looking for and quickly find it. It works great even for obscure options. I often use it because it is faster that way.\n\nGood job to Eugenia and the KDE team, especially considering that there is still over a month until release."
    author: "Alex"
  - subject: "Re: Fair review for the most part"
    date: 2003-12-16
    body: "Why would you want to combine the keyboard and mouse configuration? They are two different beasts, why try to merge the configuration of the two? I think it would make the configuration even harder...\n\nWhat I do think is that we should try think something up to link the different types of keyboard configurations together somehow. The current devision makes sence, but is confusing at the same time. Why do I need to configure keyboard repeat and numlock status in 'Peripherals' and keyboard layout and shortcuts in 'Regional & Accessebility'? Somehow, I'd expect keyboard configuration at one place. Having said that, I'd also expect Regional settings to be grouped like it is now... That won't be trivial to solve, I guess."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Fair review for the most part"
    date: 2003-12-16
    body: "What I don't really understand is the Panel configuration in the Control Center. You have a Panels item both under the Appearance & Themes and Desktop and a Taskbar under Desktop. If you choose Configure Panel from the context menu you get a dialog will all of the above in one place. Confusing? ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Usability \"improvements\"?"
    date: 2003-12-16
    body: "> Currently, the user has to literally search on panels after panels where the \n> \"show text shadow\" option is hidden (FYI, is on a panel after you clicked \n> \"advanced\" on the background image panel)\n\nI presume that who ever made this change thought that they were making an improvement.  As, did whoever removed: \"Edit File Type ...\" from the Konqueror context menu and replaced it with a tiny unlabeled icon.\n\nAnother \"improvement\": you now have to go to a second level menu to: \"Mount\" or \"Eject\" a CD-ROM while redundant options and options that simply don't work (with a CD-ROM) remain on the first level.\n\nAm I missing something here?  Yes, I know, I don't understand Free Software -- he who codes decides, etc..  But, to me it is starting to look like this \"he\" isn't making very good decisions -- that design decisions need to be made before the code is changed.\n\nAnd yes, I brought this up on: 'kde-usability' -- an \"arrogant developer\" was nice enough to tell me how wrong I was without actually citing anything that had any validity.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Usability \"improvements\"?"
    date: 2003-12-17
    body: "I have to agree with this. Hiding the unmount in an extra layer is positively silly. Is that a result of useability expertise?\n\n"
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Quick question...."
    date: 2003-12-16
    body: "I'd just like to add a question (to my comments above). When I first came to Linux, around 1999 I guess, I was checking out the two DE's and I came to the conclusion that KDE was the average-user-friendly desktop whereas GNOME was the desktop for the more technically minded. \n\nIt's strange how in the intervening four years that situation seems to have totally switched around, was this purely because GNOME got more commercial backing? Was it a conscious choice on behalf of the core developers?"
    author: "Tom Holte"
  - subject: "she's right"
    date: 2003-12-16
    body: "I use KDE for the last 5 years and it seems to me that it has grown to a point where there are just too many configuration options to keep them organised in the way they are know.\nI guess the KDE Control Centre is getting out of control.\nOn the other hand keeping the functionality (and displaying it to the user) seems to be importand as well so there seems to be a need for two layers of configuration options.\n\n\n"
    author: "Alex"
  - subject: "How does OS/X handle it??"
    date: 2003-12-16
    body: "A simple konfig menu to not freak out newbies with all the rest of the advanced options hidden away.\n\nJust copy that ..."
    author: "More-OSX_KDE_Interpollination"
  - subject: "Advanced/Simple"
    date: 2003-12-17
    body: "I think the key to the configuration dialog complexity is to simply have an 'Advanced' option that expands out to show more features within the one dialog, Preferably integrating them into the layout to avoid duplication of catagories. The advanced options should also be destinctive, maybe a slightly different background color. and the desktop settings wizard can simply ask you if you want it on simple or advanced by default. \n\nKonqueror menus do need a good cleaning up. I think The Window menu could just about be gotten rid of. The Help menu should be moved to the far right (I sware it used to be in the old days). The go menu has alot of redundant cr*p in it such as applications autostart and templates.  The toolbars should also be shrunk down, theyre a joke.. I mean print, print frame.. cut copy paste, 2 icons for view modes. Also reload and stop should alternate as they do in Opera as there is no need for both of them at once.\n\nJust my 2c"
    author: "Bug"
  - subject: "My suggestions:"
    date: 2003-12-17
    body: "For example: http://img.osnews.com/img/5410/kde1.png\n\nGnumerics toolbar DOES look better than on Kspread. If Kspread had those lines between the icons here and there it would look alot better. Also, it would help organize the icons in to groups according to their function. I don't have KDE install at my disposal right now (my Linux-install is hosed and waiting for re-install), but if the situation is similar in other KDE-apps, they should be modified accordingly as well.\n\nAnd the menus ARE too close together. There is plenty of room in the menubar, why cramp the menus so close together?\n\nNow, regarding the clutter (like I said, I haven't used KDE in a while, and I haven't tried the latest betas). That can be divided in to three sub-categories: Toolbars, context-menus and configuration-options. Let's go through them one by one:\n\nToolbars. Now, there's not much KDE could do in regards do third-party KDE-apps. But something could be done when it comes to the core KDE-apps. We should take a long and hard look at each app and get rid of the bloat. What could be done for example, is something similar to the \"Actions\"-button in the new Mac OS X Finder: Group the less used actions under a one button. Pushing that button shows a menu where those actions are listed. Of course, what's on that menu would be user-configurable. If we could move 4-6 actions that now have separate buttons in to that one menu, we could seriously reduce the clutter in the toolbars.\n\nKonqueror must get special attention in this regard, since it is THE KDE-app. Does the layout of the toolbar change when user switches profiles? Some actions that are relevant in filemanagement, may not be relevant in web-surfing. Having only relevant actions displayed in the toolbar would help. Now, it might be difficult to implement with tabs. If we have separate filemanagement-window and browsing-window, it wouldn't be difficult. But if they are in different tabs, it might be confusing to the user if the toolbar changes when they switch between tabs. I don't have an easy solution for this.\n\nContext-menus: KDE is getting better, but work remains to be done. We should REALLY take a look at what's needed and what's now. When I right click on a directory, what options should I get? I'm thinking of:\n\nMove\nCopy\nLink (think shortcuts here)\nPaste\nDelete\n----------\nActions =====> (less used options here, in a sub-menu. Also, options added by additional apps)\nCreate ISO-image\nCompress\netc.\n----------\nProperties\n\nThat's it. Nothing more, nothing less. How about a file?\n\nMove\nCopy\nLink\nDelete\nOpen With...\n----------\nActions ====> (See above. Also, these could change according to filetype)\nCompress\nBurn to CD\nSet as wallpaper (if a image-file)\netc.\n----------\nProperties\n\nHow about the desktop?\n\nAdd ====>\nDirectory\nLink to application\nFile\netc. </add>\nPaste\n---------\nArrange icons\nUnclutter desktop\n---------\nRun command\n---------\nConfigure\n\nThat's it. It might be feasible to put \"Actions\"-menu in the desktop-menu as well.\n\nConfiguration-options: This is a double-edged sword. Obviously we don't want to limit the tweakability of KDE. What we need to do is to re-arrange the control-cetner and consolidate, consolidate and consolidate some more. I can't really offer constructive opinions here, since it would require me to have up-to-date KDE installed. I will return to this subject after I have re-installed my Linux (that is, if people are interested in what I have to say)."
    author: "Janne"
  - subject: "Re: My suggestions:"
    date: 2003-12-17
    body: "Why arn't Compress and Burn to CD in your Actions menu? "
    author: "Andr\u00e9 Somers"
  - subject: "Re: My suggestions:"
    date: 2003-12-17
    body: "they are. I'm sorry if that wasn't clear in the original message"
    author: "Janne"
---
KDE 3.2 Beta 2 was released last week for general testing and OSNews <A HREF="http://www.osnews.com/story.php?news_id=5410">offers a preview</a> of what is to expect from it early next year upon its release. The article mentions KDE's new features (faster loading times, Konqueror's Service Menus, Kontact, KPDF, Plastik theme etc), the problems that still plague it (cluttered KMenu and Konqueror menus, too many disorganized control center modules) and some constructive suggestions on how to get over the bloat without losing the functionality.


<!--break-->
