---
title: "Help KDE: Clean Up bugs.kde.org"
date:    2003-10-27
authors:
  - "cniehaus"
slug:    help-kde-clean-bugskdeorg
comments:
  - subject: "Help via CPU idle-cycle donations?"
    date: 2003-10-26
    body: "Could KDE integrate some sort of grid computing software for people willing to donate extra CPU cycles for for example source compiling rounds?\n\nUsing similar techs as distributed.net, seti@home, folding@home, XTremWeb, ssic-linux and similar interface should not be a big software R&D deal by using software like mpich2 with distcc variation?!?\n\nOh well, just a thought (perhaps for the kde-4.0 release cycle). ;-)\n"
    author: "Nobody"
  - subject: "Re: Help via CPU idle-cycle donations?"
    date: 2003-10-26
    body: "you can have mine too.\n\nan AMD XP2200 with ( very soon ) 40 kbps up and 100 kbps down sitting on his ass with 95% cpu idle.\n"
    author: "Mark Hannessen"
  - subject: "Re: Help via CPU idle-cycle donations?"
    date: 2003-10-27
    body: "Sorry, you can't have mine.  XScreensaver uses up all my idle CPU time ;-)\n\nJWZ just came out with some cool new hacks too!  XScreensaver is great."
    author: "Spy Hunter"
  - subject: "Re: Help via CPU idle-cycle donations?"
    date: 2003-10-27
    body: "While at developer conferences Troll Tech's Teambuilder is invariably used, at other times unfortunately the lack of bandwidth (the Teambuilder documentation suggests that 10Mbit ethernet may struggle) and latency  would more then likely slow down complilation rather than speed it up."
    author: "Chris Howells"
  - subject: "Re: Help via CPU idle-cycle donations?"
    date: 2003-10-27
    body: "I agree, but only because teambuilder&ccdist are too primitive: if they would combine distributed caching of compiled executables (not .o files, but the whole libraries and programs) with a way to analyse all input (compiler options, checksums of all used sources, header and libraries) it would work. Simply because libkdecore looks the same on all Suse 9.0 installations, as long as the sources don't change and people are using the same options.\nBut this is no possible with make...\n"
    author: "AC"
  - subject: "Re: Help via CPU idle-cycle donations?"
    date: 2003-10-27
    body: "Obviously the source code changes if developers work on the code so your suggestion doesn't make much sense, does it?\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "YEah, there are a lot of invalid bugs"
    date: 2003-10-26
    body: "I will close as amny as I know that are no longer vallid for 3.1, but I don't have a way to run head.\n\nI have SuSE 8.2. I guess I could try Gentoo though."
    author: "Alex"
  - subject: "Re: YEah, there are a lot of invalid bugs"
    date: 2003-10-26
    body: "Close them if they are reported for an older version than 3.1.x. It's unlikely that the same error will show up again in 3.2."
    author: "Anonymous"
  - subject: "Re: YEah, there are a lot of invalid bugs"
    date: 2003-10-27
    body: "> but I don't have a way to run head.\n\n/me thinks beta1 might be packaged.."
    author: "anon"
  - subject: "Re: YEah, there are a lot of invalid bugs"
    date: 2003-10-27
    body: "If you don't want to wait for Beta1 ( I suggest you don't), get konstruct-unstable and with a little tweaking, you'd be building from HEAD in no time."
    author: "Guss"
  - subject: "Re: YEah, there are a lot of invalid bugs"
    date: 2003-10-27
    body: "With daily snapshot tarballs? That requires some bunch of tweaking/manual interaction."
    author: "Anonymous"
  - subject: "Re: YEah, there are a lot of invalid bugs"
    date: 2003-10-27
    body: "With `cvs up`. and not that much tweaking. \nin gar.lib.mk, after configure-%/configure I added:\n@pushd $* && cvs -q up -d -rHEAD && make -f Makefile.cvs && popd\n\nugly, crufy and convulated ? I guess - but it works. if you want a new CVS snapshot you simply remove your cookies and rebuild."
    author: "Guss"
  - subject: "how long should it take me?"
    date: 2003-10-28
    body: "I want to maybe run gentoo on a system to test KDE 3.2's bugs\n\nHow hard do you think taht is and how many hours should it take on a 2 ghz system?"
    author: "Alex"
  - subject: "Re: how long should it take me?"
    date: 2003-10-28
    body: "I run KDE 3.2 CVS on Gentoo. On a 2GHz P4, ccache'd builds of kdelibs and kdebase take a couple of hours. Usually, I compile overnight, and all the major bits (network, PIM, utils) finish in that time. One thing to remember, however, is that some packages may not compile. So you don't want to do \"emerge kde\" because that way, if kdenetwork fails to compile, nothing afterwards will be compiled. What you want to do is do \"emerge arts ; emerge kdelibs; emerge kdebase; ...\" seperately."
    author: "Rayiner Hashem"
  - subject: "Re: how long should it take me?"
    date: 2003-10-28
    body: "One thing to mention is that gentoo's portage handles distcc and ccache automatically if installed. The CVS ebuilds are available from http://dev.gentoo.org/~caleb/kde-cvs.html ..  maintained by Caleb Tennis of kdevelop fame.  They work like magic. "
    author: "anon"
  - subject: "take this"
    date: 2003-10-28
    body: "How is this ? :\n\n- install ab debian from knoppix\n- use Orth's Debs zu use KDE-CVS\n\nshould only take 1 hour or so , depends on your\nline speed.\n\n\ngreetings chris."
    author: "chris"
  - subject: "Re: kdenetwork failing on 3.2.0 Beta and Gentoo"
    date: 2003-11-17
    body: "Has anyone else had an issue trying to emerge kdenetwork 3.2.0 beta on Gentoo?  Mine fails during the install when approaching the kwireless stuff.  It is an upgrade from 3.2.0 alpha.  "
    author: "Marcus Whitney"
  - subject: "Re: YEah, there are a lot of invalid bugs"
    date: 2003-10-28
    body: "SuSE users can find snapshot rpms here, sometimes the directory contains only some rpms, if the build failed, but it changes every day:\n\nftp://ftp.suse.com/pub/people/adrian/"
    author: "Micha"
  - subject: "Yes, I have tried to clos bugs, but more is needed"
    date: 2003-10-26
    body: "I have tried some bug busting, but I am not sure of the proper criteria.\n\nI was closing them if I couldn't confirm them on the current STABLE branch release from CVS.  Is this OK?\n\nI think that we must consider dumping some of the old bugs.  Should bugs for the 2.x.y branch that are over a year old just be purged from the system with a message sent to the reporter that if the problem still exists on 3.x.y that thsy should write a new report.\n\nAlso, what happens to the bug reports on HEAD when the first Beta of the new branch is tagged?  Haven't they become irrelevant to some extent.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Yes, I have tried to clos bugs, but more is needed"
    date: 2003-10-27
    body: "> I was closing them if I couldn't confirm them on the current STABLE branch release from CVS. Is this OK?\n\nIf the report was not filed against HEAD/newer version it should be fine of course.\n\n> what happens to the bug reports on HEAD when the first Beta of the new branch is tagged? Haven't they become irrelevant to some extent.\n \nNot irrelevant, bugs don't disappear just because HEAD was tagged. A tag is just a label, no branch. There will be no new branch (then stable KDE_3_2_BRANCH) until after the KDE 3.2 release."
    author: "Anonymous"
  - subject: "Re: Yes, I have tried to clos bugs, but more is ne"
    date: 2003-10-28
    body: "The question is:\n\n> What happens to the bug reports on HEAD when the first Beta of the new branch is tagged?\n\nWhile you may have a point that this time point in question is when the  KDE_3_2_BRANCH is added, it doesn't anwswer the question of what should be done with them.\n\nThe question is whether development is being done on HEAD that is not part of the 3.2 BETA.  If that has happened then the branches have diverged and it is time for the \"QA department\" to do something with the bug reports for HEAD.\n\nThese bug reports need to diverge as well.  The ones that are fixed in HEAD prior to the divengence should be reduced to a list for regression testing and the ones that are not fixed should be updated for the current HEAD.\n\nPerhaps there needs to be a way to tag the bugs as well.\n\nNOTE: If you have better ideas about how to deal with this mountain of bugs, please post them!\n\n--\nJRT\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Yes, I have tried to clos bugs, but more is ne"
    date: 2003-10-30
    body: "There is no regresion testing done with the bug database.\n\nBugs marked as RESOLVED will be definitively closed when KDE 3.2 is out. That is how Bugzilla works. (Well with Bugzilla, normally you are supposed to have a QA team to mark them as VERFIED, but we have not any QA team, so no VERIFIED status in KDE.)\n\nAnd again: the branch is only created *shortly* (hours, days) before KDE 3.2 is released.\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: Yes, I have tried to clos bugs, but more is needed"
    date: 2003-10-27
    body: "There aren't enough 2.x bugs in our database to even argue about them.\nThere are many more for 3.0 and 3.1 and many of them are still valid."
    author: "coolo"
  - subject: "Re: Yes, I have tried to clos bugs, but more is ne"
    date: 2003-10-28
    body: "The question remains, should the fact that a bug is for 2.x.y be sufficent reason for closing it and asking the reporter to resubmit it if it is still a problem in 3.1.4?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Yes, I have tried to clos bugs, but more is ne"
    date: 2003-10-28
    body: "If you can't reproduce a 2.x.y bug with 3.1.4 or later then close it (as WORKSFORME) and ask the reporter to re-open it in case he can still reproduce the problem with 3.1.4 or later."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Yes, I have tried to clos bugs, but more is ne"
    date: 2003-10-30
    body: "If the bug can be reproduced, it remains valid.\n\nIf you can add new data from never version for the bug, that is fine too.\n\nHowever, if you have such a bug, that you cannot reproduce him, that someone asked the reporter a question and that the reporter has *not* answered for many months (6 being the thumb rule), then you can mark the bug as RESOLVED/INVALID.\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: Help KDE: Clean Up bugs.kde.org"
    date: 2003-10-27
    body: "Hi all,\n\nJust now I started konstruct (cd meta/everything 8-) on LFS 4.x based system.\n\nSoon you may hear from me too!!.\n\nUntil then,\n\nBadri"
    author: "Badri Pillai"
  - subject: "Re: Help KDE: Clean Up bugs.kde.org"
    date: 2003-10-27
    body: "Not a good day to do so because \"the first Beta version is near\"."
    author: "Anonymous"
  - subject: "Check your own bug reports!"
    date: 2003-10-27
    body: "And what is completely missing in there, check your own bug reports after an update to a newer version! Nobody can better tell if the same bug still occurs for the original reporter than himself."
    author: "Anonymous"
  - subject: "mail a list of open bugs to the reporter"
    date: 2003-10-28
    body: "Hi!\nIMHO the bug reporter s the most competent one to judge if a bug is resolved.\n\nSo it would be a good idea to send a list of open bugs to the reporter to ask/confirm if the bug has been resolved. Some intelligence could be used to mark the bug as solved if the reporter just clicks a link in it's mail without urging him to open an account.\n\n\n"
    author: "ferdinand"
  - subject: "Re: mail a list of open bugs to the reporter"
    date: 2003-10-29
    body: "Since bugs.kde.org is using bugzilla all reporters actually need to have an account already to be able to write a bug/wish report at all. So all one need to do to contact a reporter is commenting to a bug/wish report, the reporter should get the respective message as email (unless s/he changed the address meanwhile). So the best thing you can do is picking an old bug you can't reproduce and add a message to it asking the report to close it.\n\nSadly in many cases the reporter doesn't answer so you'll need someone else to close it (eg. contact me at datschge at gmx de). Also there are old reports which were created when bugs.kde.org didn't use bugzilla yet (about 2900 right now, see http://tinyurl.com/ss8q) so those reporter might not have an account yet.\n\nAn effective way of searching for old reports which are probably resolved in KDE already is searching for reports which didn't have any kind of activity (no new messages, no votes, no changes of any kind) for a very long time. For example http://tinyurl.com/ssfe is a list of currently about 1600 reports which weren't touched at all since one year ago."
    author: "Datschge"
  - subject: "Re: mail a list of open bugs to the reporter"
    date: 2003-10-29
    body: "> http://tinyurl.com/ssfe is a list of currently about 1600 reports which weren't touched at all since one year ago.\n\nOf which \"only\" 359 are bug reports and the rest wishes which likely nobody other than the reporter needs."
    author: "Anonymous"
  - subject: "Re: mail a list of open bugs to the reporter"
    date: 2003-10-29
    body: "That's a great reason to keep them in the database, not?"
    author: "Datschge"
  - subject: "Re: mail a list of open bugs to the reporter"
    date: 2003-10-30
    body: "No new message does not mean that the bug is fixed. For example many bugs for KWord are still valid but have not had messages for months.\n\nIt is another case when a question was asked to the reporter and that many months later, there is not any answer. Such bugs should be marked as RESOLVED/INVALID. (If you have not enough rights to do that, just add a message proposing to mark the bug as RESOLVED/INVALID. It would give the reporter a last change to answer.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: mail a list of open bugs to the reporter"
    date: 2003-10-30
    body: "Better WONTFIX (because of missing information)."
    author: "Anonymous"
  - subject: "Re: mail a list of open bugs to the reporter"
    date: 2003-11-01
    body: "No, the report is invalid, as you cannot work with it.\n\nWONTFIX would be when you understood the report (even with very few data) but do not want to change anything.\n\nAn example for WONTFIX: the file from application XYZ cannot be read. The user has not attached the document but the developer knows that XYZ produces buggy files anyway, so he do not care.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "on the subject of bugs"
    date: 2003-10-28
    body: "\ndoes anyone else see funny link behaviour when hovering over links in Konqueror CVS at the moment:\nhttp://www.drobe.co.uk/ and http://news.bbc.co.uk/ exhibit this behaviour.\n\n"
    author: "cbcbcb"
  - subject: "Would someone please fix bug # 39693"
    date: 2003-10-29
    body: "Would someone please fix bug #39693.  Its been around for ages and its one of the most annoying."
    author: "Mike"
  - subject: "Re: Would someone please fix bug # 39693"
    date: 2003-10-31
    body: "I support this suggestion strongly! :)"
    author: "Markus"
  - subject: "COMDEX, Around the corner!!!!"
    date: 2003-10-29
    body: "O'Reilly is working with COMDEX to organize an Open Source Innovation Area on the COMDEX Exhibit Floor. They nominated 21 projects and they'd like you to help them select the six projects they'll send to COMDEX. The winning projects will be recognized by COMDEX and they'll invite a leader from the project to come to COMDEX and run demos on the show floor. This will give Open Source projects an opportunity to go where only commercial software vendors have gone before.\n\nVote for  the projects you think are most essential to OSS \nhttp://www.oreillynet.com/contest/comdex/\n\nHere are the current standings:\n\n1. php MyAdmin (2793)  \n2. Eclipse (252)\n3. TightVNC (216)\n4. Plone (65)\n5. MPlayer 39\n6. OpenOffice.org (19)  \n6. GNOME (19)\n6. KDE (19)\n\nVery suprising, I expected KDE and GNOME to be at the forefront of the race, not  lagging in last place.\n\nBe sure to vote for  the projects most essential to OSS, the contest will end soon.\n\nKDE ALL THE WAY!!! =) =) =) \nI would love to see a beta of KDE 3.2 demonstrated at COMDEX. From now on, I will vote every day KDE and only KDE so that KDE can actually get ahead. If I vote for  OO.org, KDE, and GNOME it is as if I haven't voted at all, because their all getting the same benefit.\n\n"
    author: "Alex"
  - subject: "Re: COMDEX, Around the corner!!!!"
    date: 2003-10-29
    body: "Wow, that must be messed up or something. I've personally voted for KDE about ten times in the last ten days. "
    author: "anon"
  - subject: "Re: COMDEX, Around the corner!!!!"
    date: 2003-10-29
    body: "> Here are the current standings:  6. KDE (19)\n\nYou're referring to http://www.osdir.com/Downloads-req-viewdownloaddetails-lid-124-ttitle-KDE.html? The voting (or better rating) of the linked download directory entries has nothing to do with the COMDEX context."
    author: "Anonymous"
  - subject: "cannot view link"
    date: 2003-10-29
    body: "In the links for HELP KDE: Clean Up Bugs.kde.org... I cannot view the manual\nhttp://kde.ground.cz/tiki-index.php?page=Bug+Triage\n\nIt states I don't have acces"
    author: "Onno"
  - subject: "Re: cannot view link"
    date: 2003-10-29
    body: "I have same problem. Maybe the concern overloaded server capacity :-)"
    author: "BoodOk"
  - subject: "Re: cannot view link"
    date: 2003-10-29
    body: "The server seems to be misconfigured right now. http://tinyurl.com/svdg is a link to a google cache of the Bug Triage page."
    author: "Datschge"
  - subject: "Does kernel matter"
    date: 2003-10-31
    body: "Does it matter if I am using 2.4 or 2.6 test KDE to verify bugs?"
    author: "Alex"
  - subject: "Re: Does kernel matter"
    date: 2003-10-31
    body: "nope, kde isn't linux dependant anyways =)\n"
    author: "anon"
  - subject: "Re: Does kernel matter"
    date: 2003-11-01
    body: "No, except if the bug might be due to Linux's behaviour.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
As <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">KDE 3.2 is approaching</a> and the first Beta version is near, more and more people are testing it. Therefore, a lot of new bugs are appearing in <a href = "http://bugs.kde.org/">KDE's bugtracking system</a>. While this is of course a good thing, it is much easier for the developers if all the reported bugs are (still) valid and precise enough. Everyone with a current version of KDE is able to do this cleanup-work, coding-knowledge is not needed. Think of it this way: The less time the developers spend on fixing bug reports, the more time they can spend on fixing the bugs themselves! So if you wish to help KDE, consider <a href="http://wiki.kdenews.org/tiki-index.php?page=Bug+Triage">reading through this manual</a> posted on the <a href="http://wiki.kdenews.org/">KDE Community Wiki Site</a> and do your part in making KDE 3.2 the best KDE ever!

<!--break-->
