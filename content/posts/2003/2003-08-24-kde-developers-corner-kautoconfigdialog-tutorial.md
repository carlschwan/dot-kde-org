---
title: "KDE Developer's Corner: KAutoConfigDialog Tutorial"
date:    2003-08-24
authors:
  - "bmeyer"
slug:    kde-developers-corner-kautoconfigdialog-tutorial
comments:
  - subject: "Very Nice"
    date: 2003-08-24
    body: "Seems extremely interesting. I look forward to trying it out for some of my apps come KDE 3.2 :)\n\nConfigure dialogs aren't half tedious."
    author: "MxCl"
  - subject: "CVS digest alluded to this . . ."
    date: 2003-08-24
    body: "From what I saw it reminds me of the windows registry editor :)\n\nThis is not a flame, just an observation. It's cool though."
    author: "Turd Ferguson"
  - subject: "Re: CVS digest alluded to this . . ."
    date: 2003-08-24
    body: "You're thinking of the KConf editor in CVS digest.  This is something completely different.  Yes, KConf is similar to GConf and the windows registry editor, but KControl is not going away.  This however, is a new configure widget for developers.  It really doesn't offer anything new to the end user besides consistancy, but it DOES make it much easier for developers to write configuration options."
    author: "manyoso"
  - subject: "Can't wait to use it!"
    date: 2003-08-24
    body: "Thanks for this great tutorial. I cannot wait until I can use it in my own applications as it will really reduce their code size and therefore my work. \nKDE is really the coolest development framework available! This new class rocks!\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "links"
    date: 2003-08-24
    body: "it is bad to link to the same site twice within the same paragraph! You're doing that very often, please don't.\n\nthanks!"
    author: "me"
  - subject: "Re: links"
    date: 2003-08-24
    body: "I think this linking is ok because it is to 2 seperate things: the specific documentation and the general developer-site. Sure, they are on the same site, but clearly two seperate things.\nSuppose you only link the developer-site, then the user has to find the specific documentation the topic was talking about.\nSuppose you only link the documentation, then the user may not be aware that this documentation is part of a larger site about KDE-developer-information.\nGiving links to both is clearly an asset."
    author: "robrecht"
  - subject: "Re: links"
    date: 2003-08-24
    body: "Well... the sneak peak and the tutorial links both reference the exact same URL... This _is_ double linking IMHO."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Re: links"
    date: 2003-08-25
    body: ">> Well... the sneak peak and the tutorial links both reference the exact same URL... This _is_ double linking IMHO.\n\nThere are only two links, \"sneak peek at KAutoConfigDialog\" and \"KDE Developer's Corner\".  The two links have different URL's.\n\nWhat is this 'tutorial' link that you refer to?"
    author: "nonamenobody"
  - subject: "Re: Re: links"
    date: 2003-08-25
    body: "At the moment I replied, the last word of the article, 'tutorial', was a link too, refering to the same URL as the sneak peak link."
    author: "Andr\u00e9 Somers"
  - subject: "Chess engine"
    date: 2003-08-24
    body: "Will a KDE chess board be included in KDE 3.2 games package?\n\nI also get the impression that the graphics of the game package soemtimes look a little bit inconsistent. Will a Crystal look also be applied to the Game graphics, like card sets ecc.?\n\n"
    author: "Wurzelgeist"
  - subject: "Re: Chess engine"
    date: 2003-08-24
    body: "If someone writes it :)\n"
    author: "AC"
  - subject: "Re: Chess engine"
    date: 2003-08-24
    body: "Are you kidding?!  Check out Knights... it's only the coolest chess software out there.\n\nhttp://knights.sourceforge.net/\n\nWrite to the developers and ask them to put it in KDE's CVS."
    author: "manyoso"
  - subject: "HOLY SHIT THIS ROCKS!!!"
    date: 2003-08-24
    body: "I just downloaded version 0.6 and this is by far the best chess game on Linux! This just has to go in CVS!!! It rocks!\n\nTry it out yourself: http://sourceforge.net/project/showfiles.php?group_id=31461&release_id=160924\n\nSure it's nowher enear as good as Chessmaster 9000, but its still awesome!\n"
    author: "Alex"
  - subject: "Re: HOLY SHIT THIS ROCKS!!!"
    date: 2003-08-24
    body: "And this \"slibo\" chess program?  "
    author: "Heini"
  - subject: "Re: HOLY SHIT THIS ROCKS!!!"
    date: 2003-08-24
    body: "I think Knights is much further along.  Knights really is an amazing program."
    author: "manyoso"
  - subject: "Re: HOLY SHIT THIS ROCKS!!!"
    date: 2003-08-25
    body: "Bah, they really should merge the two programs."
    author: "Alex"
  - subject: "Awesome!!"
    date: 2003-08-24
    body: "This program rocks, jsut tried it now and I downloaded the themepack too, it's absolutely stunning!\n\nThe default, Winter or Blue Marble board theme  with Nuts and Bolts, Penguins, Staunton Wood, or Symmetraced look sooo beautiful and it works great too.\n\nOnly problem is using the crafty engine if I put the computer 3 bars more than weaker it starts lagging a bit. And that the computer difficulty setting is kind of obscure, it should be next to the computer opponet option, I shouldn't have to go to human and than computer to change the difficulty."
    author: "Mario"
  - subject: "'tabs'"
    date: 2003-08-25
    body: "While this sounds nice, I was scared when I saw that screenshot: http://developer.kde.org/documentation/tutorials/kautoconfigdialog/configure.png\n\nIs that terrible 'sidebar' with icons ever going to go away from KDE?\n(or can it be changed to just be a row of tabs instead? without the (often non-Crystalized) icons?)\n"
    author: "whatever"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "IMHO a solution like in Controlcenter - View \"Icon\" oder \"Tree\" would do it."
    author: "Ferdinand"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "No, that makes it a lot less user friendly.\n\nJust having tabs (or at least the choice to have them instead) would be nice..\n"
    author: "whatever"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "Actually using KAutoConfigDialog a developer shouldn't normally specify which layout format to use.  Thus every app that has multiple pages currently uses the sidebar format.  But of course to change that one would only have to make a changed in one place and every single application would move.  This makes for a much more consistant user interface.  Currently that change is in the code, but I was thinking of down the line to making it a global kde configuration option.\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "Thanks for this very useful class. This will reduce some serious amount of work and I think, using it as a way to make the layout format globaly configurable, is a pretty good idea, too."
    author: "anonymous"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "I don't know anybody else who feels that way about the sidebar icons. I much prefer them to tabs myself and it makes KDE stand out from other Desktop Environments which is also a good thing."
    author: "MxCl"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "There are many other ways to make KDE stand out from other DE's:\n\n1) faster/est widget toolkit (which Qt already is)\n2) good programming interface (much better than Gtk/Gnome, but can still be greatly improved to be somewhat more professional)\n3) consistent desktop (unfortunately, KDE is not the most consistent DE anymore, but it looks like things are improving lately)\n4) having one option to rule them all, and not 20 options for doing the same in 20 different programs (KDE programs suffer badly from this, but looks like it's improving as well lately)\n5) supporting all needed features to use GNU/Linux without touching a terminal (no DE on Linux does that atm)\n\n(about that last one, reallly.. try using Linux/KDE for a month without using a terminal/console at all, it won't work)\n\n\nAnd if you want icons, that's good, as long as I can have my tabs :)\n(using one _global_ desktop options of course, and not 40 options)\n"
    author: "whatever"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "Oh and of course:\n\n6) best programs (Bake Oven rulez, KMPlayer is cool, KDevelop rocks, etc)\n"
    author: "whatever"
  - subject: "Re: 'tabs'"
    date: 2003-08-25
    body: "\"supporting all needed features to use GNU/Linux without touching a terminal\"\n\nNext time you have to open up a terminal, stop and look at WHY you're opening it up. It will be one of two reasons: 1) system adminstration; or 2) accessing the full power and flexibility of the OS.\n\nIn terms of system administration, if you don't want to touch the command line, do what thousands of companies across the world do, even with Windows: hire a sysadmin. Although it is possible to put sysadmin functionality in the desktop, the question I have to ask is \"why?\" And what good would a KDE frontend to the sendmail configuration do you if you run postfix instead? I'll backtrack a bit. Since with a stand-alone home system, you HAVE to be your own sysadmin, there are some things that should be on the desktop. The prime example is package management. But most of these things are already covered.\n\nThe second reason was to access the full power and flexibility of the OS. This is something you never want to take away from the user. I dual boot WinXP and FreeBSD, and everytime I use Windows I feel like I have my hands tied behind my back. There are things you can do with the command line in UNIX that are simply unimaginable in Windows. Need to change a link in each of ten thousand html files? One or two lines in a terminal using grep and sed are all that you need, versus several hours of opening up each file one by one and editing manually."
    author: "David Johnson"
---
For developers waiting for all of the new goodies that KDE 3.2 will offer, here is a <a href="http://developer.kde.org/documentation/tutorials/kautoconfigdialog/index.html">sneak peek at KAutoConfigDialog</a>, in the form of a new tutorial added to the <a href="http://developer.kde.org/">KDE Developer's Corner</a>.  KAutoConfigDialog is a class that allows a developer to easily create a configuration dialog for an application.  It automatically synchronizes GUI widget values with values in the configuration file by matching keys and widgets of the same name.  KAutoConfigDialog also manages the buttons in a normal configuration dialog along with many other features -- reducing duplicated code and manual work.  For full details and screenshots check out the tutorial!
<!--break-->
