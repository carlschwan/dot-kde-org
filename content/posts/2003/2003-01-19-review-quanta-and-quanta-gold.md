---
title: "Review of Quanta+ and Quanta Gold  "
date:    2003-01-19
authors:
  - "binner"
slug:    review-quanta-and-quanta-gold
comments:
  - subject: "Good stuff"
    date: 2003-01-19
    body: "I'm using v3.0 with rc6. There are a few minor quirks that bug me, but the application is very useful. \n\nQuirks: Editor doesn't get focus if I switch to another virtual desktop and back. Seems to lose project settings on shutdown, especially if kde shutdown. The file structure takes some getting used to. Oh, and bookmarks would be nice to better navigate a large file.\n\nPlus: If you highlight a word, hit the anchor toolbutton, highlight becomes text in anchor. Gotta say I love the select to copy, mmb to paste in kde. Autocompletion is cool, needs a little polish yet.\n\nNice to see the project using kate as editor. Both apps have been worked on and improved.\n\nThe article says the documentation comes only with the non-free one. It is all there from Gentoo. Quite handy.\n\nMy biggest complaint is my monitor is too small.\n\nDerek "
    author: "Derek Kite"
  - subject: "Re: Good stuff"
    date: 2003-01-19
    body: "3.0 with rc6?? Why do not use the version which is shipped with RC6?? I think the bugs you've mentioned are not present in that version...\n\nAndras"
    author: "Andras Mantia"
  - subject: "Quanta rocks...."
    date: 2003-01-19
    body: "\n\nUnfortunately theKompany bases everything on QT, to get sales from Windows, which is a reasonable thing to do, but makes their products appear as alien as Motif stuff on a customized KDE. Either should QT hook better into KDE, using their classes if they are present, or this will remain.\n\nSo I think, Quanta Gold is not really an option. I really like Quanta Plus. In the old days, I created webpages with it and am pleased about the easy to use interface and KDE alike thing.\n\nI like the tree-like document structure that is clickable. And I LOVE the preview thing, this was once, what got me into MS Frontpage.\n\nQuanta just does the tricks I need better than I need. What else can I say, not being a web professional?\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: Quanta rocks...."
    date: 2003-01-20
    body: "As far as the appearance difference goes, simply run the qtconfig binary in your qt directory (eg, /usr/lib/qt3/bin/qtconfig) and change the QT style to whatever you're using in KDE. All of my QT apps look like Keramik, for example. This doesn't solve the file selector dialog issue, but at least everyone is using the same widgets.\n\nIMHO, KDE should do this automatically.... or at least have yet-another-checkbox for it :)"
    author: "Matthew Kay"
  - subject: "Re: Quanta rocks...."
    date: 2003-01-20
    body: "Hmm? That's supposed to happen automatically.. The one reason for it not happening I can think of is if you're using KDE3.0.x with Qt3.1.x. Which isn't really supported... "
    author: "Sad Eagle"
  - subject: "It does happen"
    date: 2003-01-21
    body: "Yeah, my Opera is using my KDE style without me doing anything.\nAlthough it's alot better, it'd be really cool if Qt apps could\nknow what platform they were running on and attempt to use the\nsystem file dialogs before defaulting to the Qt version...or what\nwould be easier is if KDE hijacked Qt calls with it's own calls\n(i.e. if KDE shipped a modified Qt library that would call KDE file\ndialogs instead of Qt file dialogs)\n\nI know it sounds pathetic to be basing things on the look of the\ndialogs and icons, but I just can't get accustomed to using Opera\nor theKompany's Studio.  Konqueror and KDevelop feel so good."
    author: "Enrique"
  - subject: "Another \"review\" of Quanta+"
    date: 2003-01-19
    body: "Hello,\n\nQuanta+ ist the current \"application of the month\" on the German KDE website, http://www.kde.de/appmonth\n\nThanks to Eric and Andras for joining us for the interview - and keep up the good work! :-)\n\nKlaus\n"
    author: "Klaus Staerk"
  - subject: "Re: Another \"review\" of Quanta+"
    date: 2003-01-19
    body: "Why did you stop to upload english versions of interviews if they were conducted in English language?"
    author: "Anonymous"
  - subject: "Re: Another \"review\" of Quanta+"
    date: 2003-01-19
    body: "Someone care to translate it in English?"
    author: "John Herdy"
  - subject: "Re: Another \"review\" of Quanta+"
    date: 2003-01-19
    body: "Hello,\n\nthere's no need to translate it into english as the interview was originally in english language. I asked Andreas Diekmann (he held the interview) to provide the dot-editors a simple HTML-version of the english interview in order to offer it here on the dot, okay? :-)\n\nRegards,\n\nKlaus\n\n "
    author: "Klaus Staerk"
  - subject: "Re: Another \"review\" of Quanta+"
    date: 2003-01-20
    body: "Great, thanks, can't wait to see the english version on the dot."
    author: "John Herdy"
  - subject: "Re: Another \"review\" of Quanta+"
    date: 2003-01-22
    body: "Of course the interview was conducted in English language. We translated it in German for our \"application of the month\" site (http://www.kde.de/appmonth). I just sent the original version to the editors of KDE.NEWS so I'm sure that the English version will be available soon. Sorry for the delay..."
    author: "Andreas C. Diekmann"
  - subject: "I can't help but wonder"
    date: 2003-01-19
    body: "I see we (Quanta Plus) were reviewed with version 3.0 PR1 because this is what Mandrake chose to ship. This release was a \"let's just get something out for KDE 3 users\" package that was plagued with many problems and lacking many features. It is interesting that we still review so well, however we have been sitting on a 3.1 release waiting for KDE. In the process it has benefitted from speed increases and backporting of some feature improvements. It is much better for those using PHP.\n\nIt is interesting that in the comparison they state that we don't have CVS but we have integrated Cervisia since 3.0 final. Let me repeat... reviewing a pre-release when a final has been our for many months doesn't make sense to me. I'm not surprised when reviewers miss the scoping of project resources, but the FTP feature is a non starter. KDE has it built in so why should we waste effort making a new one. Our upload dialog in 3.0 (note I'm talking a real release) has enough improvements for me to start using it. I had not used the old one because it made no allowance for for the upload list getting mixed up. Now I can reset all.\n\nEvaluating how spell check performed in a pre-release? We have so many features to keep track of I can't remember what was when any more but our releases are the best. As they mentioned word wrap I should note that 3.1 on KDE 3.1 will have soft wrap, one of our most requested features. Also, line numbering can be turned on by default in the final release. Now maybe we need to make it an application default for future reviews.\n\nOur templating is also good for more than pages. It does code snippets and even can be used to link common binary files. It is also scoped gloabal/local/project.\n\nI can't believe the author is asking for an integrated FTP client in Quanta Plus. Excuse me. Can we try some research before writing? *ALL* KDE dialogs inherit kio which gives them FTP, SFTP, Fish, SMB or whatever is on your system. Just enter ftp://user@domain/path in the top dropdown to get a file listing or complete the file in the bottom dropdown for the file. (This is in our 3.0 docs and the welcome docs.) This also works in konqueror so you can drag and drop and manage files transparently like they were on your system. This is why we won't be adding a lame attempt to duplicate what is already done so brilliantly by KDE. In our 3.1 release, due out any day, you will be able to use your home machine to run a project on your work machine and upload it to the server. All of it will look transparent on your system. This was achieved by a total rewrite of much of the original code and a big part of 3.1 was debugging this.\n\nIn noting the features available in the commercial editor it should be noted that we have docs available for HTML, PHP, Javascript and PHP. These docs work with context sensitive help.\n\nI am very happy to have been reviewed well, but as an open source project we have a hurdle with servicing our user base. I answer a lot of email explaining things people missed, correcting erroneous presumptions and trying to explain what can actually be done. Open source is truly amazing because you can actually talk to the people that build the project. I want to talk to reviewers because they can help the project by exposing our feature set rather than a cursory, load it up and look around, review. Reviewers can enrich users experience and make project developers lives easier by doing their due dilligence.\n\nOnce again I'm reading a review where the author did not contact me... even though my email address is in every about dialog. If he could not figure out how to get line numbers on by default he could have done what a number of users have done... write me. When you consider that by answering his question I could answer it for everyone reading the review it seems almost deriliction to leave me to do it by email or talk back. The same goes for FTP. I'm also sure that they have the latest release from theKompany as they said that anti-aliasing was coming. I would have gladly gotten out the release I'm posting to freshmeat today to the author.\n\nWe have been working very hard to provide a tool for the community that is in no way second best and in the best tradition of GPL offers the ultimate freedom. I can only continue to hope and dream that someday GPL'd programs will receive the same traditional courtesy that proprietary apps receive, to be pre contacted and to offer useful input to the author. Looking at the site this article was posted I see a number of posts already revolving around confusion on just the points I made. Fortuantely some users are clearing things up."
    author: "Eric Laffoon"
  - subject: "Re: I can't help but wonder"
    date: 2003-01-20
    body: "Relax a bit. :-)  \n\nIt's free publicity.  Sometimes things move very fast in the Open Source world for everyone to keep track."
    author: "ac"
  - subject: "Re: I can't help but wonder"
    date: 2003-01-20
    body: "I'm with you Eric. \nMost features asked in the review are already in quanta 3.1.\nSome as line numbers in side of text came from kate/kwrite (I have to say that kate improvements helped quanta a lot too). This is the beauty about quanta being a KDE app, when KDE gets better, quanta gets better too.\n\nI had stopped using Quanta+ for a period, using kate, because it wasn't very good, but I'm back with Quanta+ 3.1. It's fast, efficient, very configurable and helps a lot my PHP coding.\n\nAll in all I just want to thank for your (and all Quanta+ team) that are helping out for FREE, to OPEN SOURCE!\nI hope from deep on my heart you guys have all health and money you deserves for helping people with your software. :)\n\n(I think this is the 9th time I thanks you, and now that I got into Computer Cience I can at least learn basic C functions and C++ objects, so I can start helping out with some code). PS: I don't get tired of thanking ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: I can't help but wonder"
    date: 2003-01-20
    body: " ftp://user@domain/path\n\nwhat do you do when the \"user\" name includes an obligatory @ because of some IPS?\nJust out of curiosity... I love quanta."
    author: "jaysaysay"
  - subject: "Re: I can't help but wonder"
    date: 2003-01-20
    body: "You should be able to URL encode the @ sign... @ is ascii character 64, which is hex 40, so you'd use %40 in place of @. For example:\n\nftp://user%40somewhere@domain/path\n\nThis should work from anything that supports ftp URLs, including KDE, but I haven't tried it. :-)"
    author: "AC"
  - subject: "Re: I can't help but wonder"
    date: 2003-01-20
    body: "Replying to myself, ha ha...\n\nYou can also just use ftp://domain/path and if that host doesn't have an anonymous FTP account, KDE will pop up a box allowing you to type both the name and the password. That method is a little slower, but probably easier. :-)"
    author: "AC"
  - subject: "Re: I can't help but wonder"
    date: 2003-01-21
    body: "both dont work in my case.\ni still continue to use gftp which is good."
    author: "jaysaysay"
  - subject: "Re: I can't help but wonder"
    date: 2003-01-30
    body: "You are correct. The Konqueror FAQ has an entry for this BTW.\nhttp://www.konqueror.org/faq.html\n\nHmm, why is there only Plain  Text in the Encoding combobox, but there is still a line at the bottom saying \"Allowed HTML:\" ... Let's add a \"dot.kde.org\" area on http://bugs.kde.org :))"
    author: "David Faure"
  - subject: "We weren't notified either"
    date: 2003-01-20
    body: "I had no idea this review was taking place, and the author also used an old version of QuantaGold.  It would be nice if would be reviewers would approach the author first to verify if they have the most current, and to return with questions when they aren't sure about something.  While it was nice to be reviewed, I think both QG and Q+ were done a disservice because the review was done without talking to the authors.\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Quanta+ is great."
    date: 2003-01-20
    body: "I try to use it whereever possible.  Unfortunately there's one major feature lacking that prevents me from always using it, lack of unicode support in outputted HTML file.\n\nI like to use UTF-8 as the encoding on my pages, but Quanta doesn't seem to support it.  Only Mozilla's composer seems to be able to do it, and i just don't like it very much.\n\nIt'd be nice if Quanta could add support for this, and i'd definitely drop everything else to use it."
    author: "rh"
  - subject: "Re: Quanta+ is great."
    date: 2003-01-21
    body: "Hmm, the Open File and the Save File dialog both have a combo box in the upper right corner which allows you to set the encoding which should be used when loading/saving the document.\n\nIsn't that what you want?\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Quanta+ is great."
    date: 2003-01-21
    body: "Hmmm, so it is.  I never noticed it before, silly me. :)\n\nWell that was the last real problem i had with Quanta, guess i'll be using it as my default editor from now on.\n\nAn XHTML1.1 DTD would be nice though. ;)"
    author: "RH"
  - subject: "Re: Quanta+ is great."
    date: 2003-01-22
    body: "Go ahead and create it! ;-) (And send it to us.)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Quanta+ is great."
    date: 2003-01-24
    body: "Quanta Gold supports selection of input/ouptut encodings ( including UTF-8 )."
    author: "Dmitry Poplavsky"
---
<a href="http://newsforge.com/">Newsforge</a>  is featuring <A href="http://newsforge.com/article.pl?sid=03/01/13/1148231&mode=thread&tid=23">a side-by-side comparison</a> of the free GPL <a href="http://quanta.sourceforge.net/">Quanta+</a> and the  commercial <a href="http://thekompany.com/products/quanta/">Quanta Gold</a> from <a href="http://thekompany.com/">theKompany</a> HTML editors.
Both projects were born from the same code-base but have gradually diverged in presentation and features over time. This review, which gives a nice overview praising features and listing shortcomings, concludes with <i>"If you use Quanta Plus or Quanta Gold you can't help but be pleased"</i> as verdict.
<!--break-->
