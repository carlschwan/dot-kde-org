---
title: "KDE::Enterprise wants your KDE Success Story"
date:    2003-12-09
authors:
  - "wbastian"
slug:    kdeenterprise-wants-your-kde-success-story
comments:
  - subject: "bubble.kde.org"
    date: 2003-12-08
    body: "You should check the urls. At least XTServer Networks seems to be a lesser success story."
    author: "Carlo"
  - subject: "Re: bubble.kde.org"
    date: 2003-12-09
    body: "I have thrown out the ones with non-working websites.\n\nWill disable the link altogether when no website was given."
    author: "Waldo Bastian"
  - subject: "Largo (Update)"
    date: 2003-12-09
    body: "Oh, I discovered the old Largo story on the website.\nHere is an update:\n\nhttp://makeashorterlink.com/?S312211C6\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Rumour has it (I remember reading) that Largo is transitioning to a Gnome based environment now (Think Ximian was involved).\n\nIf I'm right, we probably don't want to trumpet the Largo 'success story' for KDE... :/\n\nDavid"
    author: "David"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "The rumors (look at who is spreading them) want to tell about a decision which has not been made (yet). Read the above given link before replying!"
    author: "Anonymous"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Yeap, but whatever they choose they are already using plenty on Gnome apps.\nKDE needs to work *fast* on some weak points.\nJust two observations\n\n- Office Suite\nI'm all for developing KOffice, but right now OpenOffice.org is a much safer bet for most people. If it ends up beeing percieved as a Gnome app it can really favour them. OpenOffice integration with KDE is really important, and work on this front needs to be done, fast!\nOpenOffice developers seem to be interested too which is a good thing.\n(See http://dot.kde.org/1068570005/).\n\n- Forgotten Domains\nEverybody's focusing on the desktop. Yes there is a big market out there!\nMeanwhile both Desktops Environments are underestimating what has been one of their biggest supporters, the scientific community. Linux is all over the Academic world already, that is where many people get in touch with linux for the first time. Still look at the state of the art in this domain (OSS-wise):\n- Plotting data - GnuPlot,\n- Numerical Computation System - Octave,\n- Computer Algebra Systems - maxima, yacas, scilab(?)\netc,\n\nAll of this are either text only or have terrible GUI frontends. \nIt's bad for OpenSource in general, it's bad for Linux/*nix (to make things worst most proprietary versions have much better windows versions (if not only)), and it would really be a wise move from any of the DEs to bet in  this areas.\n"
    author: "Src"
  - subject: "Scientific Community"
    date: 2003-12-09
    body: "You don't know Kst, KStars or Kalamaris, or?"
    author: "Anonymous"
  - subject: "Re: Scientific Community"
    date: 2003-12-09
    body: "I know all three of them.\n"
    author: "Src"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Shockingly enough OOo is not a Gnome application as some really wish you would belive.  Its based on an older toolkit developed for cross platform apps.  About the extent of Gnomeness is a quickstarter, some icons and a few mime types.  Really we could have the same thing in KDE, but no-one seems to have wanted to do it.\n\nGnome has beaten KDE at one game, they tend to work very hard at making non-Gnome apps look gnome enough to ship. KDE's demand for consistency and tight integration has a point, but I think industry has shown us they want functionality over it work in a smooth integrated manner.  "
    author: "Ian Reinhart Geiser"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "I think you're missing the point. Of course OOo is not a GNOME app (I mean, gee, ever tried coding against the GNOME API? Now try to imagine coding something as big as OOo against it!). However, if the GNOME team succeeds in having people believe it is, KDE is as good as dead. This is worrisome; I hope the Cuckooo project (http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/) will find new contributors rapidly -- it is a killer app in the strongest sense of the term."
    author: "Anonymous Coward"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "I think what's most important of OOo over Koffice is MS Office compatability. That's pretty much the only reason I don't use koffice (in 1.3, koffice is starting to be a lot less raw that it used to be)\n\nI personally don't think koffice 1.0 and 1.1 should have ever been released. I think those versions turned a lot of people to koffice. 1.3 has fixed a lot of faults that previous versions had, and would work for 90% of people if it just had better MS Office compatability. \n\nNote that Ximian's version of OOo actually defaults to MS Office file formats."
    author: "anon"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "I know many people (eng and sci students) who do not use OpenOffice.Org Calc because you cannot get the equations of calculated regressions like in Excel. KSpread (KChart) cannot even do any kind of regressions yet.\nI hope it improves, it is a promissing office suite, but it's going to take a while, and I don't think we can afford to wait.\n\nKDE integration should be on it's way and already mentioned here. \nhttp://tools.openoffice.org/releases/q-concept.html\n\nHave you seen this point in the City Of Largo article:\nhttp://makeashorterlink.com/?S312211C6\n\"Automation - Openoffice.org (Moving to tighter GNOME integration).\"\n\nIt does make a diference."
    author: "Src"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "I've tried using Excel, but swore to avoid it after trying to plot a simple histogram. KSpread may be even worse, but the simple fact is that using office programs for Sci/Eng work is stupid.\n\nUse Matlab, Mathematica, R (r-project.org), Gnuplot or whatever, but let the business types keep their tools - they're definitely not designed for scientific work."
    author: "Anonymous Coward"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "I second that! I use gnuplot. \nUse spreadsheet apps for business diagrams or other simple cases... but don't try to use them for scientific work. Some time ago I had a discussion about Gnumeric being suited for sci work or not. Gnumeric may have better accuracy than excel (due to internas), but I have huge matrices over here (measurement data) and Execl, OO.calc, Gnumeric and KSpread all fail miserably in handling large amounts of data... (excel and OO.calc are somewhat ok, but Gnumeric or KSpread are completely unuseable)"
    author: "Thomas"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Again, depends a lot on the work you are doing. \nThey are completely different kinds of tools. Because they overlap to some (small) extent many people uses them the wrong way. And that goes both ways. If most of your work is typical spreadsheet stuff that's what you should use.\n\nAnyway, since you brought up sci/eng applications (no I do not classify a spreadsheet as one, and no I am not contradicting myself), you might wanna read the second part of my very first comment on this thread.\nhttp://dot.kde.org/1070917908/1070928701/1070968874/1070970514/1070974433/\n"
    author: "Src"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Could u file a bug report for the problem u had with gnumeric. I am sure Jody would definitely be concerned since his aim is to lick excel in every aspect."
    author: "Bharat"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "\"I've tried using Excel, but swore to avoid it after trying to plot a simple histogram. KSpread may be even worse, but the simple fact is that using office programs for Sci/Eng work is stupid.\"\n\nDepends on what you are doing. Sometimes in typical spredsheet work you need to calculate regressions. \nIn those cases the tools you mention are not really appropriate for most of the job, so using them only for calculating the regressions is an unecessary pain and using them for the whole job would just be dumb (because they'd be inadequate).\n"
    author: "Src"
  - subject: "Optimization"
    date: 2003-12-09
    body: "It's pretty weird.\nBut when it comes to linear programming optimization,\nExcel is pretty good and handy. wouldn't it be\npossible to have a \"Solver\" for Linux/Koffice?"
    author: "OI"
  - subject: "Re: Optimization"
    date: 2004-03-25
    body: "Gnumeric has a great solver that works with lpsolve and gplk as backends.\nI've had a same problem crash in excel and work weel in gnumeric. Hopefully they'll have that in kspread.\nBK"
    author: "Trovador"
  - subject: "Re: Optimization"
    date: 2005-01-09
    body: "There already exist good \"solver\" software for linux.\n\nIt is used by OO already in LPSolver. Miht come for other linux software too."
    author: "KDE User"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Let them make people believe that OOo (or Mozilla or any GTK app of your choice) is part of Gnome, the belief alone won't make integration. And the very fact that OOo (and any other non-Gnome \"Gnome\" app) integrates poorly might very well strongly backfire sometime, especially when tools like Kiosk framework and similar are becoming more popular. So as for KDE I prefer no official integration over any kind of incomplete integration any day."
    author: "Datschge"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "> Let them make people believe that OOo (or Mozilla or any GTK app of your choice) is part of Gnome, the belief alone won't make integration. \n\nYes, but it's a big selling point for Gnome (especially Ximian Desktop), and is probably why Largo is evaluating it in the first place. "
    author: "anon"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "KDE already has a lot of big selling points to offer even without picking OOo, one just need to play sales people to actually sell them. Being able to do marketing right first seems to be Gnome's biggest selling point atm."
    author: "Datschge"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "(...) the belief alone won't make integration.\n\nNo this will:\nhttp://www.gnomedesktop.org/article.php?sid=1464\nhttp://tools.openoffice.org/releases/q-concept.html\nhttp://ooo.ximian.com/\n\nBelief is important yes, and it has been working pretty fine. \nBut in this case it's pretty much a fact that work is beeing done towards integration. Lots of work."
    author: "Src"
  - subject: "Re: Largo (Update)"
    date: 2003-12-10
    body: "You are apparently good at rehashing marketing. I only hope you are not mixing up \"will\" with \"is\". Largo should not decide now regarding features they can only check out at an undefined time in the future. Creating \"selling points\" based on \"forward looking statements\" is one thing a particular big player is making effective use of. I wouldn't take that as a compliment if anyone else gets attributed this way as well. Adding value free catch phrases \"lots of work\" (OOo apparently needs it >=P) doesn't help either.\n\nIf some KDE person where to create \"selling points\" based on \"forward looking statements\" he could easily refer to how OOo is already embedable as KPart using Cuckooo, how any given icon set can be used in OOo using kol's script at kde-look.org, how one developer is working on replacing the VCL GUI layer with Qt implementations etc. Toss in a \"But in this case it's pretty much a fact that work is beeing done towards integration. Lots of work.\" blanket statement and tell everyone to be happy. But on the KDE side nothing such will happen, and I hope you know why. When you know anything about KDE's frameworks, it's quite apparent that actually integrating the OOo beast of aged source code into KDE's beast of framework would create more work than improving the already ideally integrated KOffice instead. And thus in this case \"lots of work\" is rather something to avoid."
    author: "Datschge"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "There is a QuickStarter for KDE.\nhttp://segfaultskde.berlios.de/index.php?content=oooqs\n\nAnyway they're planning much tighter integration for OpenOffice.Org 2.\nIn fact both the OpenOffice.Org and Ximian teams seem to be working on that.\n\nFrom http://www.gnomedesktop.org/article.php?sid=1464\n\"This makes very interesting reading: http://tools.openoffice.org/releases/q-concept.html. The document basically outlines the plan for OpenOffice.org 2.0. A significant portion of the document deals with GNOME integration. From a quick read, it seems that there is some overlap with what the Ximian guys are currently doing.\""
    author: "Src"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Also check this.\nhttp://ooo.ximian.com/"
    author: "Src"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "I am also really interested in scientific apps for KDE. There is koctacve ( \nhttp://bubben.homelinux.net/~matti/koctave/) but I don't know how good it is. A program that I also use quite often is xmgrace. It is a very nice program, but it has a terrible interface. It would be awesome to have a kde/qt version of it. But I don't know how hard it would be. Probably quite hard."
    author: "Michael Thaler"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "It's nice.\nI appreciate the authors effort but that's it, nice.\nWe need real apps, quality ones. You know Quanta, KDevelop, Koffice? That kind of development for Scientific applications. I'm pretty sure the Octave, Maxima and Yacas teams would appreciate having one of the major Desktop Envs actively cooperating with them and creating nice GUI frontends.\n\nThere is a real void for that kind of applications out there.\n"
    author: "Src"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "There has been QMatPlot or KMatplot, very promising, but dead by now... sad this is.."
    author: "Thomas"
  - subject: "Re: Largo (Update)"
    date: 2004-03-17
    body: "I agree with you that xmgrace has a terrible interface. I use it almost daily\nand curse it :). \nI am quite interested in porting xmgrace to kde/qt version and planning to give it a shot during my summer break. \nIt appears like xmgrace developers themselves do not have any plan to do it themselves.Are you aware of any work done in this area? \n"
    author: "Manish Jain"
  - subject: "Scientific app frontend"
    date: 2003-12-09
    body: "kdenonbeta/frontman is such an effort. However, development on it will pick up post kde-3.2 since 3.2 will be the first kdelibs release with MDI functionality which is crucial to such an app. For screenies of the current status see\n<A HREF=\"http://www.eleceng.ohio-state.edu/~ravi/kde/frontman.html\">the Frontman homepage</A>.\n"
    author: "Ravi"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "\nmaybe supporting/developing OOo QT intergration would be an alternative..."
    author: "ac"
  - subject: "Re: Largo (Update)"
    date: 2003-12-12
    body: "First of all, I really like KDE and I don't use GNOME. But I don't use KOffice.\nIt's really a waste of time and effort. Lack of MSOffice support is a big negative for koffice. Also it's not stable. OOo is really good but a bit slow and has an ugly interface. When I read http://people.redhat.com/dcbw/  \nI was excited. To KOffice developers: you can use your time and effort to KDE or other useful apps. Maybe KDE NWF http://kde.openoffice.org/ .\nThanks"
    author: "A Linux user"
  - subject: "Re: Largo (Update)"
    date: 2003-12-09
    body: "Largo is currently using KDE 2.2 and is evaluating whether they will upgrade to KDE 3.1 or XD2, they are expected to make that decision early next year.\n\nSo far they have been very content with KDE 2.2 for the last two and half years so I think that qualifies as success and something that KDE can be proud of regardless of what they chose next."
    author: "Waldo Bastian"
  - subject: "Hi Waldo"
    date: 2003-12-09
    body: "You should check out Mandrake's Business Cases site... It's got lots of good stuff on, and companies are usually using KDE under Mandrake when they deploy:\n\nhttp://www.mandrakebizcases.com/"
    author: "Dawnrider"
---
After gathering a bit of dust due to some server changes
<a href="http://enterprise.kde.org">KDE::Enterprise</a> has recently received a face lift and is now fully operational again. To freshen up the contents we
would like to invite you to share your 
<a href="http://enterprise.kde.org/bizcase/">KDE Success Stories</a> with us.
Has your company made a succesful switch to KDE?
<a href="http://enterprise.kde.org/bizcase/addcase.php">Tell us about it!</a>

<!--break-->
<p>
Companies or free-lance professionals that offer KDE consulting, support, development or other KDE related services are also invited to
<a href="http://enterprise.kde.org/bizdir/addbiz.php">register</a> themselves
in the <a href="https://enterprise.kde.org/bizdir/">KDE Business Directory</a>.
</p>
