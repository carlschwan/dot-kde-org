---
title: "Report: KDE at Linux Bangalore 2003"
date:    2003-12-15
authors:
  - "taj"
slug:    report-kde-linux-bangalore-2003
comments:
  - subject: "KDE in Fedora"
    date: 2003-12-15
    body: "Yes, KDE is much better supported in Fedora, from my own perspective. I'm now using KDE a lot. \n\nBefore Fedora, I use only Gnome for there are two serious problems of KDE on RedHat:\n\n1. KDE's locale setting is conflict with GDM's locale choice at login. \n2. KWin has a insufferable delay when you try to moving a toplevel window, if the window is the only window opening at the moment and it's newly opened and never moved.\n\nNow all these two problems are gone. KDE(3.1.4) just runs happily on Fedora. So, I decided to use KDE more -- Replace GDM with KDM. Now, when I log out, KDE ask me whether I want to log out or shutdown the computer or reboot the computer, instead of just letting me log out when GDM was used. It's Great!\n\nBut soon I find another problem: KDE's locale setting(I mean the locale setting in personal setting, in KDE Control Center) doesn't have any effect. Whether I choose Chinese locale or not, it always uses the default system locale (USA english on my system). With GDM, I can choose Chinese locale at login and KDE will respect the choice. But KDM doesn't have locale choice. Perhaps Fedora modifies KDE to eliminate the locale choosing confliction with GDM?\n\nAny way, KDE is rich and fast and much much improved recently. Thanks for you all to bring us such a great desktop to the world.\n\n\n"
    author: "me"
  - subject: "Re: KDE in Fedora"
    date: 2003-12-15
    body: "Naj,\nThe 3.2 b2 packages for Fedora are working well for me - it's very stable.  The opening KDE startup splash screen is messed up but once you are past that, it is smooth sailing.  Some small glitches but nothing to prevent you from using it on a regular basis.  \nJohn\n"
    author: "John Taber"
  - subject: "Re: KDE in Fedora"
    date: 2003-12-15
    body: "No no no, sure it's a scary picture -- but it's acctualy just the KDE DEvelopers at kastle."
    author: "standsolid"
  - subject: "Re: KDE in Fedora"
    date: 2003-12-15
    body: "it'll be replaced before 3.2 comes out\n\nsee http://bugs.kde.org/show_bug.cgi?id=68045 =)"
    author: "anon"
  - subject: "Re: KDE in Fedora"
    date: 2003-12-15
    body: "Do you mean the official Fedora binaries or\nthe binaries from KDE-RedHat project? I was previously \nusing KDE from KDE-RedHat project on RedHat 9, and it \nwas working absolutely fine for me compared to the RedHat's \nown stripped KDE.  Do the Fedora people continue to sabotage \nKDE taking on the RedHat tradition ? But I think, the KDE-RedHat \nproject has not yet released binaries of KDE 3.2 b2; correct \nme if I am wrong.  \n\nThanks a lot to the KDE-RedHat people, or else I would \nhave been forced to move away (painfully, of course)from RedHat \nwhich I had been using for long and which, as an OS, I like \nfor many other reasons.\n\nAsokan"
    author: "Asokan"
  - subject: "Re: KDE in Fedora"
    date: 2003-12-16
    body: "I mean the official Fedora binaries. \n\nLast night I changed display manager back to GDM because the locale setting problem. But GDM just forgot how to remember any user's last session type (kde or gnome) this time. So, fairly speaking, using KDE on Fedora is still a little promatic, if the official binaries is used. Although not all the problems are caused by KDE. Another annoying thing is if you ever used Gnome under the same user account, there will be two \"Start Here\" icon on desktop.\n\nHow about make GDM and KDE under the same standard umbrella? If so, even if GDM is used, when loging out KDE, I could still choose shutdown or reboot the computer.\n"
    author: "me"
  - subject: "Re: KDE in Fedora"
    date: 2004-09-04
    body: "Hi,\n\nI am having some issue with loading translated mo files (KDE) into Fedora core 2. Do you have any suggestion as to what do do in order to be able see translate GUIs in any kDE app.\n\nHere is what I did:\n\nAfter installed mo files (translated Lao KDE message files) under /usr/share/locale/lo then I configured Lao support under Control Center-Regional and Accessibility-Country/Region and Language-Laos. I then selected Appearance and Themes-Fonts, under Menus I select Lao OT font. Applied both changes. Opened Kwrite, but Lao GUI/Interface wasn't take into affect. I also tried with conqueror.\n\nAnything else I need to do?\n\nThanks in advance,\n\nSak"
    author: "Anousak"
  - subject: "Thanks taj!"
    date: 2003-12-15
    body: "for representing the KDE project at linux-bangalore. \n\nYour comments wrt KDE in India, are unfortunatly, dead on. The lack of proper Indic language support in Qt until 3.2 was pretty much a show stopper for KDE usage and localization in India until now. Perhaps that's why only RedHat has attempted to break in to the potentialy very large Linux market until now, as they are primarily a GNOME-based distro.\n\nKDE 3.2 and Qt 3.2 should see a rapid increase of KDE localization in various Indic lanaguages, and thus, a much greater prenetration of KDE within India. "
    author: "anon"
  - subject: "Re: Thanks taj!"
    date: 2003-12-19
    body: "Well Hindi translations are on its way, We should be having a decent KDE Hindi desktop by 3.2 release :)\n\nSome screenshots.\nhttp://www.indlinux.org/hindi/kde/konqpage.jpg\nhttp://www.indlinux.org/hindi/kde/konqdialog.jpg\nhttp://www.indlinux.org/hindi/kde/konqmenu.jpg\n\nKarunakar\nKDE Hindi coordinator\n"
    author: "karunakar"
  - subject: "Re: Thanks taj!"
    date: 2003-12-19
    body: "Looks great. =)"
    author: "Datschge"
  - subject: "KDE in Debian"
    date: 2003-12-15
    body: "Taj Wrote:\n>Linux is synonymous with Red Hat simply because they seem to be the only >distribution to have put serious effort into this market so far. Almost all the >demo machines at the conf were running it, and most people's KDE experiences seem >to be from Red Hat too\n\nThis is very much true. Redhat is distro famous with any/all newbies to linux.\nPartly because ,the local Computer Magazines which introduced Linux, flooded with RH variants only. Plus kudos to Anaconda and other RH works which make it user friendly ( Fedora follows).\n\nI am still yet run KDE over a Debian. :(\n\n\n"
    author: "senthil"
  - subject: "Re: KDE in Debian"
    date: 2003-12-15
    body: "Too correct. \nI started out on the linux experience using PCQuest Magazine (in 1998) and it was RH 5.0. I also came across KDE on this system. Out of curiosity I found binaries called kde 0.98pre or something and tried it and was hooked. Until recently ( I had no network connection), I thought RH was the only distro worth using. Now I hate using RH . I use Gentoo instead.\n\nMost people here still think Linux is RedHat. It is taught in schools and classes as \"Linux 8.0\" (RedHat omitted) and even advertised as such. This has fuelled a wrong impression. If I tell my friends to try Linux and KDE, they say \"we've used it and its crap\". And they always talk of RedHat as \"Linux 8.0\" or \"Linux 9.0\". Who's going to tell them even \"Linux 3.0\" is a long way off?\n\nOn the other hand, my father was totally computer illiterate. Now he uses the computer and doesn't know how to use Windows (that's the secretary's job in office). At home he is very happy with using KDE. So I concluded that KDE is an excellent DE for newbies not spoilt by Windows.\n\nThanks a lot for the desktop, and I'm really glad it is making inroads in India. Hopefully the large number of VC++ guys in India will give a hand to Qt/KDE and switch over. Currently only people who prefer C associate it with linux and are linux programmers in India (AFAIK). They associate C with linux and C++ with Windows. Hopefully that will change.."
    author: "Rithvik"
  - subject: "Re: KDE in Debian"
    date: 2003-12-15
    body: "Just to add to this I too got started on linux using PCQuest and they shipped redhat with their magazine (expect at the very start they shipped with two slackware releases before starting with redhat 5.0)\n\nThe magazine always carried many article on KDE (I have used KDE from the magazine dating back to .98pre release I think). So I think anyone using linux from PCQuest   should be well versed with KDE even tough it's a redhat based distribution. They also updated KDE packages quite regularly on their monthly CD. back at the time having dial up connection this was really a only source for me to update KDE"
    author: "YBP"
  - subject: "Re: KDE in Debian"
    date: 2003-12-16
    body: "Well, one of the reasons why KDE was more prominent in PCQ's version of Redhat Linux was that we (the team that used to put together PCQL) were quite happy with KDE (which was much more stable thatn Gnome in those days), and used it by choice. That sort of \"leaked\" into the work, so that many of the articles (and distros) were very KDE oriented.\n\nWhile I did do a detour via WindowMaker for some time (as my 5 year old notebook started creaking under KDE's weight), I have been using KDE again for the past year, and if I were to do a PCQL distro again (I haven't since 2001), I am sure you would see the \"bias\" again. ;-)\n\nBTW - this is no comment at all on Gnome's usability - it is just that I cannot use both desktops at the same time and *had* to make a choice, so went for the \"tried and trusted\".\n"
    author: "Atul Chitnis"
  - subject: "Joke in first talk's synopsis :-)"
    date: 2003-12-15
    body: "\"The war for Linux on the desktop has been won.\""
    author: "Anonymous"
  - subject: "Re: Joke in first talk's synopsis :-)"
    date: 2003-12-19
    body: "joke?\n"
    author: "taj"
  - subject: "KDE for me too :)"
    date: 2003-12-15
    body: "Hi Taj, \n\nThough I was almost sold out on switching back to GNOME after the LB/03 conference,I came back, installed FC1 and reverted back to KDE. I like the usability of KDE better.\n\nSwati\n"
    author: "Swati Sani"
  - subject: "Re: KDE for me too :)"
    date: 2003-12-15
    body: "Anybody know of any retail outlet in India from where \nFedora core 1 can be purchased on CD ?\n\nI cannot download it due to poor bandwidth.  Any suggession \nwill be greatly appreciated.\n\nAsokan"
    author: "Asokan"
  - subject: "Re: KDE for me too :)"
    date: 2003-12-15
    body: "Asokan, I have cds of FC1 with me plus CVS KDE Sources and some more cool stuff. If there is enough interest we can start replicating them and begin circulation. Let the revolution begin! :)\n\nParag"
    author: "Parag"
  - subject: "Re: KDE for me too :)"
    date: 2003-12-17
    body: "OK Parag, I will contact you separately  \nthrough email.  \n\nThanks a lot for your suggession."
    author: "Asokan"
  - subject: "Re: KDE for me too :)"
    date: 2003-12-18
    body: "And yes - No business intent here. Just spreading some joy thru KDE!"
    author: "Parag"
  - subject: "Re: KDE for me too :)"
    date: 2003-12-17
    body: "I can help you out in getting the Fedora CDs if you are in Bangalore. ( No business please) just the cost of media or not even that if you give me media.\n\n\nSagar,\nBangalore"
    author: "Sagar"
  - subject: "Re: KDE for me too :)"
    date: 2004-04-13
    body: "Hi Sagar\n  Could you help me out in getting some free open source OS CD's. I will give you the media.\n\nRegards\nYogi"
    author: "Yogi"
  - subject: "Re: KDE for me too :)"
    date: 2005-09-16
    body: "Hi,\n\nI want to install the Can you help me in getting a Fedora core CD please."
    author: "Rethish"
  - subject: "Re: KDE for me too :)"
    date: 2006-04-11
    body: "Hey any of you got fedora 5 CDs (distribution release). I would thankful if you can gimme those to burn the CDs."
    author: "prashanth"
  - subject: "Re: KDE for me too :)"
    date: 2003-12-19
    body: "Hi Swati,\n\nHappy to hear that. I still owe you and Dr Tarique a KDE CD, I just forgot at the conf.\nSo if you come to Delhi... ;)"
    author: "taj"
  - subject: "Re: KDE for me too :)"
    date: 2003-12-22
    body: "Hi Taj,\n\nLets see when we come to delhi again, last I visited delhi was 10 months ago after a gap of 12 years :(\n\nLets hope this time its sooner.\n\nSwati"
    author: "Swati Sani"
  - subject: "Translation"
    date: 2003-12-15
    body: "Its great to hear about the translation being done. Even though I only speak English, their are a lot of people in the UK whose native tongue comes from India. So having KDE in Hindi is a great thing, especially in persuading schools about Linux.\n\nIs there anyone working on Punjabi?"
    author: "Leonscape"
  - subject: "Re: Translation"
    date: 2003-12-19
    body: "Unfortunately there doesn't seem to be a Punjabi/Gurmukhi translation team yet. I'll try\nto convince some of the linux users in Punjab to get working on this.\n\n"
    author: "taj"
  - subject: "Re: Translation"
    date: 2004-01-21
    body: "Count me in ;-)"
    author: "aps"
  - subject: "Re: Translation for KDE in Panjabi"
    date: 2004-03-24
    body: "we are working on translating gnome 2.6 and lots of work has been done. our work can be checked at developer.gnome.org/projects/gtp/status/gnome-2.6/index.html and can be downloaded from our site www.geocities.com/punlinux . We are starting to work on KDE soon, for this we need startup help. \n"
    author: "Amanpreet Singh Alam"
---
Last week I represented our illustrious desktop environment project at
the big <a href="http://www.linux-bangalore.org/2003/">Linux Bangalore</a>
conference in, well, Bangalore. This pleasant city is located in India's
southern state of Karnataka and hosts -- perhaps as a side-effect of it
being the computing capital of the nation -- the largest and most active
free software community in the country.
<!--break-->
<p>
I flew to Bangalore from Delhi on the morning of the first with the clever
and amiable Naba Kumar, whose only major failing is that he's a GNOME guy
(developer of Anjuta; I professed a plan to get him to hack on KDevelop
instead but that has not yet reached fruition). By the evening of that
day I had set up the larger-than-expected KDE booth (I was expecting
more like a KDE stall) with two provided computers and my laptop running
a post-Rudi CVS of KDE and KOffice.
<p>
The conference began on the second, during which I did my first talk
(on KDE 3.2) and spent the rest of the day at the booth talking
about KDE and answering questions. Since I had last attended the <a
href="http://www.linux-bangalore.org/2001/">first LB</a> in 2001, two
changes were immediately apparent - the conference had grown immensely,
and the composition of attendees had diversified from interested tech
students to IT professionals and government agencies actually using
free software in their daily operations. A <i>lot</i> of people dropped by the
booth and had a chat or played around with KDE, and their knowledge of
KDE was highly varied. 
<p>
One thing I've noticed is that in India, Linux is synonymous with Red
Hat simply because they seem to be the only distribution to have put
serious effort into this market so far. Almost all the demo machines
at the conf were running it, and most people's KDE experiences seem to
be from Red Hat too. Not such a great thing, especially as downloading
software off the net is hard due to the low availability of broadband. I
hadn't really anticipated this, so at the conf itself I spent quite a bit
of time burning and giving away CDs with whatever KDE sources happened
to be on my laptop at the time.  It's going to be very important to get
good KDE packages onto the next few Fedora releases, as its quite hard
to even find other distributions for sale.
<p>
I must admit my KDE 3.2 talk was not particularly inspired, I blame the
flu and some unrehearsed demos. There were a lot of good questions though
so at least they were listening. My <i>Rapid Applications with KDE</i> talk
on the second day went a lot better (I got a bit more sleep and swallowed
a lot of pills that have likely nullified my stomach lining). It became
Rapidly Clear through a show of hands that most people there weren't
much aware of how KDE works, so most of the talk was spent explaining
KDE internals, DCOP, Qt Designer etc.
<p>
The folks representing the <a href="http://www.indlinux.org/">IndLinux</a>
project were at a booth close by and were demonstrating their l10n efforts
for Indian languages. Qt only added decent support for Indian scripts in
2.3, so while these guys had some translation work already done for KDE
in Hindi, it took a day or so for them to get it set up. By the second day
of the conf they had some cool Hindi and Kannada demos at their booth and
helped set it up on one of our machines too. They had been waiting for the
3.2 string freeze, so translation work should pick up serious steam soon.
<p>
Overall, an eye-opener of a trip for me personally, and a fairly useful
one for KDE. It was really great to meet so many free software folks and
there was definitely a spark in the community that wasn't there two years
ago. Mark my words -- there is going to be a sudden and large influx of
Indian names in free software development within the next six months.
<p>
I returned to Delhi a few days after the conf ended, fitting in a bit of
wandering around Bangalore and a day trip to Mysore with the inspiring
<a href="http://gnumonks.org/">Harald Welte</a>.
<p>
I'll end with a note of thanks: my brother Giriraj for preparation and
printing of booth artwork, the handful of volunteers who helped out at
the booth totally off the cuff, the IndLinux and Mahiti folks for cool
demos and the offer of machines, and the conf organisers for their good
humour and help beyond the call of reasonable duty -- I hope to be back
next year.
