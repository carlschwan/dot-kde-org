---
title: "KDE-CVS-Digest for October 17, 2003"
date:    2003-10-18
authors:
  - "dkite"
slug:    kde-cvs-digest-october-17-2003
comments:
  - subject: "Thanks"
    date: 2003-10-18
    body: "Derek"
    author: "bugix"
  - subject: "Please include full kolab-support for kontact"
    date: 2003-10-18
    body: "It's very very important! Please merge the kroupware-branch!\n\nThanks!"
    author: "TomTom"
  - subject: "Re: Please include full kolab-support for kontact"
    date: 2003-10-18
    body: "Agreed. I would be stuck with a patched KDE 3.1 as well if the Kroupware branch won't be included in KDE 3.2."
    author: "Arend jr."
  - subject: "Thanks Derek"
    date: 2003-10-18
    body: "As always an interesting read.\n\nBug 61617 - tabs resize (shrink) when clicked on while loading\n\nAh! I _almost_ got used to it ;-)\nBut nice to know that it's gone. Come to think of it - it's been quite\nannoying...\nBTW (OT):\nFor quite some time now I'm trying to figure out why Noatun interrupts\nplaying a song for a second or so after half a minute of playing. After that\nit plays the song till the end without any further interruption. XMMS via\nArts doesn't do that. I played with the Arts settings anyway, tried\nhdparm -d1 /dev/hda\nbut to no avail. This occurs on all PCs I've installed KDE, at work\nas well as at home. Is it a know bug (couldn't find any) or have I overlooked\nsome setting? I'm using KDE 3.1.2"
    author: "Mike"
  - subject: "another one bites the dust"
    date: 2003-10-18
    body: "Hooray!  They fixed the khtml bug with the dashed lines around visited links not being erased correctly!  I love reading about my \"pet peeve\" bugs being swatted :-)"
    author: "Spy Hunter"
  - subject: "Layer support for KHTML?"
    date: 2003-10-18
    body: "I spotted this:\n\n> Added very basic support for <ilayer>, much like <layer>, as a generic block element.\n\nIs it really worth supporting obsolete, proprietary elements like that?  As far as I know, the only other browser to implement <layer> elements is Netscape 4.x.  It's an buggy anachronism that's probably better off forgotten about."
    author: "Jim Dabell"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-18
    body: "Yeah, this could just bloat the engine, nobody uses those anymore since 99% of people's browsers are incompatible with them. But, hey its still better to have a wider support."
    author: "Alex"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-18
    body: "I'm not really concerned with bloat of that nature, I'm more concerned with the KDE maintainers:\n\na) Having to know about the element type in the first place,\n\nb) Having to figure out how to treat them,\n\nc) Having to find/write testcases to check that things work properly, and\n\nd) Having to maintain the code in the future.\n\nRemember, just because one developer implemented it, it doesn't mean that other KHTML developers know all about the element type.  When a bug report comes in about it or they need to modify the code for some other reason, the work has to go through the small number of developers that know about this odd element type, rather than the large number of developers that know about proper HTML.\n  It's a maintenance burden that offers virtually no benefit (how many websites rely on these element types, considering only Netscape 4.x supports it?  Not many, I'd bet).\n"
    author: "Jim Dabell"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-18
    body: "Hi,\n\nI guess Apple did it. And if so, why not?\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-19
    body: "I think I was pretty clear on why not: it takes up developer resources for no gain.\n"
    author: "Jim Dabell"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-19
    body: "So somebody did it for fun. \n\nGo ahead, demand punishment, demand removal, how do you qualify to?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-19
    body: "Er, *what*?  I said I don't think it's a good idea.  I didn't say that somebody should be punished and I'm not demanding anything.  All other things being equal, the less code in an application, the better.  I qualify to judge whether it's a good idea or not as I am very familiar with HTML and common HTML user-agents.\n"
    author: "Jim Dabell"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-19
    body: "> Er, *what*? I said I don't think it's a good idea.\n\nActually you said a little more than that, or at least very emphatically.\n\n> I didn't say that somebody should be punished and I'm not demanding anything.\n\nSo this isn't a demand, just a rant? ;-)\n\n> All other things being equal, the less code in an application, the better.\n\nThen I guess all other things weren't equal. FWIW it was David Faure who put it in. He's not exactly a developer one would suggest is wandering around searching for a clue. He was a key architect on konqueror and kword and kparts are his baby. He also has been sponsored full time or half time on KDE for years.\n\n> I qualify to judge whether it's a good idea or not as I am very familiar with HTML and common HTML user-agents.\n\n...and that means exactly what? You don't qualify to judge because you are in no position to mediate. You qualify to have an opinion based on some information which is a different matter entirely. As the saying goes, \"everybody has an opinion...\". In a general context your opinion might have some value, if David has sought it. However if you read the commit it says he did it so that the release manager could view one of his favorite sites. Now... knowing what open source is, if developers who in fact do take responsibility for the code can't have some indulgence that facilitates their using that code in thier day to day development because a user who is not actually doing the development doesn't think it's a good idea... well... why bother. (Besides, this does support more sites that users who have no idea what W3C compliance means are asking for. Are we taking a stand against that because it's not W3C?)\n\nSo that explains the tone in response to the tone of your post. I'm not going to argue whether an obscure tag should be supported just as you should not argue whether respected developers will be able to maintain thier application. You're certainly free to say you think it's hooey, but it would be nice to eventually come down to some support for the developers being able to enjoy their code too."
    author: "Eric Laffoon"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-20
    body: "> Actually you said a little more than that,\n\nI explained my reasoning, sure.\n\n\n> or at least very emphatically.\n\nThe only emphatic bit I see is:\n\n\"It's an buggy anachronism that's probably better off forgotten about.\"\n\n...and that's in relation to the <ilayer> element, so I can hardly see why people should be upset on behalf of the KHTML developers, and I don't see how I could be mistaken for calling for punishment or demanding anything.\n\n\n> So this isn't a demand, just a rant? ;-)\n\nYes :)\n\n\n> > All other things being equal, the less code in an application, the better.\n> \n> Then I guess all other things weren't equal. FWIW it was David Faure who put it in. He's not exactly a developer one would suggest is wandering around searching for a clue.\n\nOf course not.  But that doesn't mean he isn't immune to bad decisions or necessarily well-informed about the various usage patterns of every element he comes across either.\n\n> > I qualify to judge whether it's a good idea or not as I am very familiar with HTML and common HTML user-agents.\n> \n> ...and that means exactly what?\n\nIt means that I know what I'm talking about when I say things like:\n\n\"As far as I know, the only other browser to implement <layer> elements is Netscape 4.x. It's an buggy anachronism that's probably better off forgotten about.\"\n\n> You don't qualify to judge because you are in no position to mediate. You qualify to have an opinion based on some information which is a different matter entirely.\n\nSo you are saying I overstepped my bounds when I assumed it was meant to be a proper implementation rather than a special-case workaround for one of the developers?\n\n> However if you read the commit it says he did it so that the release manager could view one of his favorite sites.\n\nWouldn't it have been better to fix the website in question?  It would help the website, and it would mean *zero* code to maintain for KHTML.\n\n> (Besides, this does support more sites that users who have no idea what W3C compliance means are asking for. Are we taking a stand against that because it's not W3C?)\n\nThat's the second time somebody has mistaken my position as being a W3C cheerleader.  Will you read what I wrote please?  The issue is that any website relying on layers will only work in Netscape 4.x [1], and, consequently, it's so rare to find a website relying on layers that it's hard to justify implementing it.  Furthermore, as it's not part of any standard, and there are already standard ways of doing it, the number of websites I expect to use layers in the future hovers somewhere between zero and one.\n\nSo yeah, if the release manager wants some code in to handle a website he likes, who am I to argue?  But you can't seriously tell me that the problem was fixed in the right place, can you?\n\n[1] If Bugzilla entries are anything to go by, they sniff for Mozilla to serve it better code.  I don't have time to look into it now, but it seems to me that all they'd have to do is update their sniffing code to check for Konqueror as well (it's a horrible implementation anyway, but extending it to one more browser is the least intrusive fix).\n\nAccording to <URL:http://www.mozilla.org/docs/web-developer/upgrade_2.html#layer>, Mozilla doesn't do *any* special-case handling for layers.  It's possible the partial implementation of <layer> and <ilayer> confused the sniffer, and so ripping out <layer> instead of putting in <ilayer> would have fixed the problem and brought KHTML closer to Mozilla behaviour."
    author: "Jim Dabell"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-18
    body: "> (how many websites rely on these element types, considering only Netscape 4.x supports it? Not many, I'd bet).\n\nUsing that argument, we should remove support for the w3c dom too, eh?"
    author: "anon"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-19
    body: "No because the DOM might be used in the future, this si unlikely to be used in the future.\n\n\n"
    author: "Alex"
  - subject: "Re: Layer support for KHTML?"
    date: 2003-10-19
    body: "> Using that argument, we should remove support for the w3c dom too, eh?\n\nNo.  Did you read my argument?\n\na) Knowing about the element type is a lot more probable since it is a standard element type,\n\nb) Developers already know how to treat them since they are defined in a public specification,\n\nc) Testcases for the HTML/XHTML specifications are already available,\n\nd) developers already have to maintain standard HTML code, and\n\ne) lots of websites already use the DOM - virtually no websites use the <layer> element.\n\nBasically, any argument for supporting the <layer> element doesn't have a leg to stand on, and comparing it to the DOM is ludicrous.\n"
    author: "Jim Dabell"
  - subject: "titles..."
    date: 2003-10-18
    body: "the new titles in kcontrol for each of the subsections are good in theory. however the icons are a blurred mess... will this mean svg icons are around the corner? ;P"
    author: "not_registered"
  - subject: "Re: titles..."
    date: 2003-10-19
    body: "I need to create 22x22 versions for some of those icons. They are currently scaled up from the 16x16 version and hence the blur."
    author: "Nadeem Hasan"
  - subject: "kmail and spamassassin"
    date: 2003-10-18
    body: "I use kmail from kde cvs of 10th october and it freezes while checking my spammassassin filters even though the it dod. is that a known bug or is it just me? maybe it's been already fix. \n\nthanx\n\npat"
    author: "Pat"
  - subject: "Re: kmail and spamassassin"
    date: 2003-10-18
    body: "sorry I meant it freezes but it doesn't crash, i just have to wait a couple douzen of seconds before it gets back to normal, the filters works great though :)"
    author: "Pat"
  - subject: "Re: kmail and spamassassin"
    date: 2003-10-18
    body: "Just use spamc instead of spamassassin."
    author: "Asdex"
  - subject: "Re: kmail and spamassassin"
    date: 2003-10-18
    body: "It's caused by KMail executing filters in the same thread as the GUI. I had this problem about a year ago. Work is being done to rectify this I think, though I don't know when it will be completed."
    author: "Chris Howells"
  - subject: "Re: kmail and spamassassin"
    date: 2003-10-18
    body: "Right now...\n\n-------------------------------\nDate: Today 21:17:21\nCVS commit by tilladam: \n\nAdd Mirko's threadweaver to libkdepim until after 3.2 when it will likely\ngo into kdecore so we can use it in kmail for async filtering and async\ngpg operations.\n-------------------------------\n\nNote this is just the lib, but the kmail implementation will follow soon. Thanks to Mirko, Don, Till and the others\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: kmail and spamassassin"
    date: 2003-10-18
    body: "Ooooh sweet :)"
    author: "Chris Howells"
  - subject: "Re: kmail and spamassassin"
    date: 2003-10-19
    body: "Thank you so very much!\nthis is why I like kde so much :)"
    author: "Pat"
  - subject: "Mouse Gestures"
    date: 2003-10-18
    body: "Does anyone know how to set up the gestures, so that one can close a tab with a gesture?\n\nClosing tabs can be done through the older hotkeys, but only with keyboard shortcuts. Would be nice if that interface provided a way for providing gestures instead of keyboard shortcuts."
    author: "radix"
  - subject: "Re: Mouse Gestures"
    date: 2003-10-18
    body: "Yeah, I wish there was a Gesture->DCOP Call action in khotkeys2.. but you can do it with a Gesture->Keyboard action in khotkeys2 in recent CVS. Just make sure the window contains \"Konqueror\" or whatever other browser you use (keyboard input works with non-KDE apps as well)"
    author: "anon"
  - subject: "Re: Mouse Gestures"
    date: 2003-10-18
    body: "But there is a gesture->dcop action, the question is, what dcop action is needed to close the active konqueror tab in the active konqueror window. I couldn't do it."
    author: "radix"
  - subject: "Re: Mouse Gestures"
    date: 2003-10-20
    body: "You can create Gesture->DCOP action if you choose the generic type and create it yourself. Also, there is kdebase/khotkeys/khotkeysrc, if you use that as your config file, you'll get demonstrated most of the features. I guess creating gesture for closing a tab based on the demo gestures that are there shouldn't be difficult."
    author: "L.Lunak"
  - subject: "Cool, Thanks for the info!"
    date: 2003-10-18
    body: "May I see a screenshot of the new style dialog?"
    author: "Alex"
  - subject: "Re: Cool, Thanks for the info!"
    date: 2003-10-18
    body: "It's just a \"Configure...\" button beside the style combobox which opens a small options dialog window."
    author: "Anonymous"
  - subject: "New Version of Plastick Style!"
    date: 2003-10-18
    body: "Ceebx has just released a new version of his Plastick style: http://www.kde-look.org/content/show.php?content=7559\n\nOne of my pet peeves regarding every KDE style is that there is no cool animated progress bar. Is there some way to have an animated progress bar in Plastick, like in Keramik (with patch) and OS X?\n\nSomething just like this: http://www.kde-look.org/content/show.php?content=6345\n\nBTW: Did the above project make it into Keramik CVS, and if it didn't do you know why not? I really think it should."
    author: "Alex"
  - subject: "Re: New Version of Plastick Style!"
    date: 2003-10-18
    body: "Plastik rocks!\n\nif only I could have a deb for unstable :-("
    author: "ac"
  - subject: "Re: New Version of Plastick Style!"
    date: 2003-10-18
    body: "It's already in unstable."
    author: "anon"
  - subject: "Re: New Version of Plastick Style!"
    date: 2003-10-18
    body: "Hmm, Keramik as of the latest CVS update (last night) has animated progress bars."
    author: "Rayiner Hashem"
  - subject: "Re: New Version of Plastick Style!"
    date: 2003-10-18
    body: "COOL! I didn't think it did since the project on looky said it wasn't in CVS yet. What about Plastick, I haven't tried the CVS version."
    author: "Alex"
  - subject: "Re: New Version of Plastick Style!"
    date: 2003-10-19
    body: "Nope."
    author: "Rayiner Hashem"
  - subject: "Konqueror Sucks"
    date: 2003-10-19
    body: "I'm using Konqueror 3.1.4 on KDE 3.1.4.  My konqueror window\nis over 800 pixels wide.  Yet this web page, hosted at KDE,\nrenders wider than the browser.  If I want to read a line of\ntext, I have to scroll the window sideways.  That makes\nKonqueror effectively unusable.  Despite claims to the \ncontrary, this bug was not fixed in KDE 3.1.4.  The sad\nthing is, KDE developers simply don't care about this\nproblem."
    author: "Konqueror Sucks"
  - subject: "Re: Konqueror Sucks"
    date: 2003-10-19
    body: "Umm, ok, I _could_ try to match the tone of your posting but I \nrather prefer not to. Though I'm not a developer:\nPlease go to bugs.kde.org and look how many open bugs are listed there.\nThen go look at the bugs most voted for.\nNow imagine someone would post a message like you for everyone\nimprtant bug which is posted there. And there are a lots of bugs over\nthere. I don't want to write that old: \"Go fix it yourself\" stuff\nbecause I consider that equally stupid. But a bit more understanding\nfor unfixed bugs would definitively help here. I'm not saying that you\nshouldn't campaing for \"your\" bugs here or that you shouldn't say out loud\nwhat bothers you about KDE. But to claim that KDE developers \"don't care\"\nis just that - a claim. The difference to M$ is here: You paid no money\nfor KDE you get it all for free. There is no one you can blame for it\nif sth. works not as expected. And if you consider all that - honestly -\nKDE as a whole is a pretty good piece of software isn't it? So if you\nthink this bug is so bad that you can't use Konq without it being fixed\n- so well -  you can tell is here - you can be annoyed - you can use Mozilla\nfor the time being -  but please don't claim that developers are not \ninterested in fixing ANY bug which just isn't true. Otherwise KDE wouldn't \nbe where it is now.\n\n\n"
    author: "Mike"
  - subject: "Re: Konqueror Sucks"
    date: 2003-10-19
    body: "Don't know if that's correct english... but: Don't cast pearls before swine...\n\nAnyway.. meanwhile I set the size of my konqu window to < 800 px in width and the line-length   and page-width were correctly re-adjusted. It just resulted in a lot of word-wrapping like it's supposed to do... (KDE 3.1.3)... So what is this all about?"
    author: "Thomas"
  - subject: "Re: Konqueror Sucks"
    date: 2003-10-19
    body: "please dont feed the troll"
    author: "anon"
  - subject: "Re: Konqueror Sucks"
    date: 2003-10-19
    body: "Well, perhaps you are right. But I decided to post anyway but to\npost an answer that not leads into endless discussions and sth. which \nmight be good to read for a wider audience because we all can sometimes be annoyed by bugs... He can reply whatever he wants now, I'm outta here...\n\n"
    author: "Mike"
  - subject: "Re: Konqueror Sucks"
    date: 2003-10-19
    body: "What URL? What distribution? What package? Nobody is going to help you without some additional informations as it's no general problem."
    author: "Anonymous"
  - subject: "Re: Konqueror Sucks"
    date: 2003-10-19
    body: "Developers do care. It's fixed in the CVS."
    author: "Joachim"
  - subject: "Re: Konqueror Sucks - some soppy fanmail"
    date: 2003-10-19
    body: "I'm a user, I can't begin to tell you how good using KDE makes me feel.  It's good kit, looks amazing, adds KStars, (don't know what it's supposed to be for but I enjoy playing with it) - it mystifies me when people moan.\n\nFTIW\n\n"
    author: "Gerry"
  - subject: "Subversion"
    date: 2003-10-19
    body: "Subversion looks nice.. im glad KDevelop is now compatible :)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Subversion"
    date: 2003-10-19
    body: "I tried to compile kio_svn against the current debian svn-libs today to try out the svn support.\n \nIt didn't work. \n\nSeems that kio_svn does not support subversion 0.31 (=current) yet.\nUnfortunatly I was unable to find any hints on which svn-Version should work.\n\n-- \nkillefiz"
    author: "killefiz"
  - subject: "Re: Subversion"
    date: 2003-10-20
    body: "But as usual Subversion is not GPL, but has a BSD-like license...\n\nHow about supporting also GPL'ed revision control system like GNU's arch [http://www.gnu.org/software/gnu-arch/]?\n\nMaybe a feature for kde-3.3?\n"
    author: "Nobody"
---
In this week's <a href="http://members.shaw.ca/dkite/oct172003.html">CVS-Digest</a>:  Disconnected IMAP fixed in <a href="http://kmail.kde.org/">KMail</a>. KHTML now supports jng image format. <a href="http://www.kdevelop.org/">KDevelop</a> has a <a href="http://subversion.tigris.org/">Subversion</a> plugin. KDE has global settings for mouse gestures. <a href="http://kopete.kde.org/">Kopete</a> has new &quot;Away&quot; and plugin configuration dialogs. KControl has a new style configuration dialog. Plus many bugfixes in <a href="http://kmail.kde.org/">KMail</a> and KHTML.
<!--break-->
