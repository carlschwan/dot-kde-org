---
title: "e-scribe.com: FreeBSD, KDE and Me"
date:    2003-12-03
authors:
  - "agroot"
slug:    e-scribecom-freebsd-kde-and-me
comments:
  - subject: "Nice review!"
    date: 2003-12-02
    body: "A review that hits the nail on the head. Finally, we can see how a 'normal' user enjoys Konquerors' features - I don't think he would ever care about Konqueror being 'too bloated', it just has all the features he wanted to have on MacOS X!"
    author: "User"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "Maybe the rarity of this event is caused by the fact that there are very few things to enjoy in KDE, as very few things work in a proper, expected way? And when one complains, he is faced with a flood of accusations of being prejudiced, anti-open-source, stupid, etc. etc., not to mention \"if you don't like it, fix it yourself!\" comments."
    author: "Nobody"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "\"...there are very few things to enjoy in KDE...\"\n\nWhy are you reading the dot if there is almost nothing good in KDE?  You seem to waste your time here!  Nothing stops you from using another desktop environment."
    author: "Dominic"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "I think the rarity is more caused by KDE's first impression. KDE doesn't present a great first impression, in comparison to Mac OS X or even GNOME. It takes somebody using the system for a significant amount of time to notice the depth of KDE. Unfortunately, most of the reviewers never get past the \"so many icons everywhere!\" stage."
    author: "Rayiner"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "> Unfortunately, most of the reviewers never get past the \"so many icons everywhere!\" stage.\n\nIf *most* of the reviewers (ie. voluntary and willing to learn people) cannot, then maybe they have a point here.\n\nEnhancement in usability would be my first wish for KDE4."
    author: "bilou"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "These are unsubstantiated comments about non-existing reviewers!"
    author: "anonymous"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "That's the problem. Most reviewers are not voluntary and willing. This guy, for example, was. He gave it a good few weeks. Most reviewers look at it for a couple of days and dismiss it."
    author: "Rayiner H."
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "And what exactly is wrong, usability wise? \n\nIt's easy to say that usability sucks, but comments like that don't help at all. We need to know what is wrong now how we can improve on that!"
    author: "Andr\u00e9 Somers"
  - subject: "Too many icons ..."
    date: 2003-12-03
    body: "\nnot only that ... too many icons that are fuzzy, of differing contrast and color depth, that are too small, not internationalized and just plain bad ....\n\neg:\n\nhttp://e-scribe.com/osx/freebsd-kde-and-me/images/paste.png\n\nNow compare to:\n\nhttp://jimmac.musichall.cz/ikony.php3   (sharp)\n\nTigerT became famous and a became a pretty good artist into the bargain while working on eraly gnome icons for Ximian.  To help KDE someone should hire a crack team of designers pay them for 25% time over a year to create a massive 5000 icon collection in SVG PNG and full original XCF (or whateve they use).  In various sizes themes and colours. (make say 5 solid i.e. 20-25000 pieces of art).\n\nThe artists could become famous and they would need to make the artwork REALLY GOOD.  These could last for years - there's no need for KDE to constantly change it's icon set!!\n\n\nOh yeah ... work over the UI and **reduce the clutter** !!\n"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Too many icons ..."
    date: 2003-12-03
    body: "> not only that ... too many icons that are fuzzy, of differing contrast and color depth, that are too small, not internationalized and just plain bad ....\n\nLet's see.. the KDE 2.x icons (hicolor) were made SPECIFICALLY with usability in mind (e.g, there were borders around everything). Some people didn't like that so we went to Crystal in 3.1, which looked more modern, but is perhaps worse usability speaking. \n\nI like the new version of Crystal that's going to be included with KDE 3.2. It has a good mix of usability and good looks. See http://www.kde-look.org/content/preview.php?file=8341-2.jpg"
    author: "anon"
  - subject: "Re: Too many icons ..."
    date: 2003-12-03
    body: "> Oh yeah ... work over the UI and **reduce the clutter** !!\n\nspecifically what things don't you like?"
    author: "anon"
  - subject: "Re: Too many icons ..."
    date: 2003-12-04
    body: "From the two examples you linked I prefer the former since the latter are too detailed (busy) for their size and also too dark, but I guess that wasn't your point...?"
    author: "Datschge"
  - subject: "Low usability icons [was: Too many icons ...]"
    date: 2003-12-06
    body: "There is nothing wrong with most of the original (unthemed) icons which are now called KDEClassic.\n\nOTOH, the CrystalSVG icons do have usability problems.  They are harder to visualize because they have reduced contrast and higher average brightness (open them in an app that will show a brightness histogram) -- and if you consider classical information theory, they contain less information (a 2D FFT should at least be indicative of this if not sufficent to prove it).\n\nI realize that lots of people think that the CrystalSVG icons are 'real cool' to the point that they seem to think that the original unthemed (now KDEClassic) icons should be removed from applications.  I am at a total loss to understand why.\n\nThe simple truth is that from every viewpoint except 'coolness' that the KDEClassic style icons are better.  I would hope that at the minimum that the developers reconsider and at least give all users the option to choose which icon style that they wish to use: KDEClassic or CrystalSVG.\n\nAnd, yes, the GNOME icons do look better than the KDEClassic icons, but I have found that this is only when displayed at larger sizes -- GNOME icons are normally 48x48.  When reduced to smaller sizes they don't look as good as the KDEClassic icons.  This is because the GNOME icons are more realistic and that results in this trade off -- they get fuzzy when they are reduced to smaller pixel dimensions.  This is the way it is and there is no fix for it except to not use dynamic icon resizing.  Perhaps SVG icons will help some.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "Many things unfortunately :-/\nSubscribe to the usability mailing list for details."
    author: "Src"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "I really, really, really like KDE (I don't know how to stress it more :), and thank endlessly to the developers.\n\nAlthough, I do have a couple of things to point in regard to usability. Coming from a Windows world (as most of the new Linux users) I am deeply accustomed to the following two behaviours that are not present in KDE.\n\n1) When a window is maximized I expect that \"crashing\" the pointer to the top-right corner of the screen (I mean: moving it instintively to that point: without even looking) and clicking, activates the close window button ('x'). I really enjoy the window decorations/themes in KDE and think are way better and more fun than the classic Windows ones, but I feel I've lost some important functionality. Now I have to pay attention to where the cursor is when I want to close a maximized window.\n\n2) The other one is really similar, but with another function. The vertical scroll bars (typically in Konkeror, but present in almost all apps).\nI expect the system to let me just \"crash\" the mouse pointer into the right side of the screen and be able to click-drag up or down to scroll, without leaving the eyes from what I'm reading. Again this isn't working in KDE, so I have to visualy search the scroll bar to be able to scroll the page. Then you have to re focus your eyes back on the line you where reading (which usually is now not where you left it, since you already scrolled unadvertedly a bit). So eyes suffer, concentration is disturbed.\n\nThey are two really little things to implement, but they end getting in the way constantly, so you easily come to hate them. I don't pretend the window decoration buttons to cover these areas visually, but just the functionality to be there.\n\nI would also like to see 'x' buttons in each Konqueror tab, but peerhaps this is already included in 3.2 (I've never tested it myself, I'm just a user) and by much not as important as the fist ones.\n\nLong live KDE!!!\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "Exactly my feelings about the KDE GUI. The idea of being able to 'crash' your mouse somewhere to do things even has a name - Fitt's Law. The easiest targets to hit on the screen are: the pixel under the mouse pointer, the corners and the edges. MS Windows used to violate it badly  - for example, the start button used to be one pixel in from the edge of the screen.\n\nFortunately, things are much better in KDE3.2. The close button extends to the corner of the title bar and the menu bar at the top of the screen in MacOS style (much better from a Fitt's law point of view) is much improved. The scrollbars are still not at the edge of the screen in maximised windows, though, which is really annoying.\nA team of people who went through all of KDE looking for these kinds of problems would be a great idea IMHO. I'd even help out a bit if I had time after my own project."
    author: "Ben Roe"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "This behaviour is very simple to get, \nSetting->control center->desktop->window behaviour->moving\nand deselect allow moving and resizing of maximised windows.\n\nVoila, the border disappears and scroll bars are at the limits of the screen.\n\nIts strange in that I've found the usability of KDE to be fine. a lot easier in fact than Windows. Windows simply frustrates me now, I find it completely unusable."
    author: "Leonscape"
  - subject: "Re: Nice review!"
    date: 2003-12-04
    body: "Sorry, but what you suggest is the first thing I tried months ago, with no result.\nDid you try it yourself with Keramik?\n\nGreetings,\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Nice review!"
    date: 2003-12-04
    body: "Yep, Its what I'm using right now.\n\nStrange that. Although it may be a 3.2 thing?"
    author: "Leonscape"
  - subject: "Re: Nice review!"
    date: 2003-12-04
    body: "It certainly maybe as I'm using 3.1.x\nGreat! :)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Nice review!"
    date: 2003-12-04
    body: "That's how I normally run. My install of kde3.2 using either Keramik or Plastik doesn't have the scroll bars as the edge of the screen despite having \"moving and resizing of maximised windows\" disabled. Not quite sure why that should be.\n\nAnd yes, Windows usability is horrible compared to KDE."
    author: "Ben Roe"
  - subject: "Re: Nice review!"
    date: 2003-12-04
    body: ">>A team of people who went through all of KDE looking for these kinds of problems would be a great idea IMHO.\n\nShouldn't this be an obvious job for http://usability.kde.org/ ?"
    author: "ac"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "You're concerns are already addressed, you just need to choose a window decoration that does this. There are several in KDE 3.2 (and a few for 3.1) that remove the window borders when maximizing the window. This allows you to \"crash\" the mouse to the corner and hit the close button, or \"crash\" to the right and hit the scroll bar.\n\np.s. Why does the close button have to be on the upper right? Why not the upper left, far away from the other buttons?"
    author: "David Johnson"
  - subject: "Re: Nice review!"
    date: 2003-12-03
    body: "So, perhaps what I should ask is the default Keramik theme to behave this way, and that an advice to consider this behaviour would be included in a 'style guide' for creating new themes/window decorations for KDE. So 9 out of 10 new users should get what he/she expected from KDE in this respect.\n\nAlso, I consider the proposed behaviour don't affect the people that don't want/need it, because it's somewhat hidden and currently these areas of the screen aren't used for anything else. So everybody wins. This is good!\n\nGreetings,\nGabriel\n\nP.D. Speaking of Keramik, I'd like the windows' bottom corners to be somewhat rounded too in 3.2. Not as much as the upper corners are, but also not as hard as it is now. Any info about that? Otherwise it's an incredibly nice theme, and I love it.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Nice review!"
    date: 2003-12-04
    body: "Hmm, i use KDE, and it's great, really great. I'm no KDE-coder, although i understand a lot of programming. But i use KDE for my daily work. And i'm deeply impressed, every time. So every time people talk about missing usability, it's sooooo far away from reality, i find myself asking if they ever used KDE or even any other GUI. It's sooo simple to switch from any DE to KDE, you have to bother about nothing.\n\nThere's nothing \"bloated\", nothing \"complicated\", nothing \"only for power users or technicians\". I simply do not understand what this \"discussion\" is about.\n\nI believe it's only to hurt people, who are VERY dedicated in making a REALLY GOOD JOB.\n\nDon't you have other things to do than to despise other's work?\n\nThink about it\n\nTim\n\n\n"
    author: "Tim Gollnik"
  - subject: "What I liked most...."
    date: 2003-12-02
    body: "Nice user report...<br>\nWhat I liked most (believe it or not... ;-) :\n<p>\n\"One specific commonality worth mentioning may surprise Quartz-proud OS X users: Both allow the creation of PDFs from any application's print dialog. KDE's implementation is arguably better, since it gives much more fine-grained control over PDF output.\"\n<p>\nAnd he proofs it by showing this screenshot: <a href=\"http://e-scribe.com/osx/freebsd-kde-and-me/images/pdf1.png\">pdf1.png</a> ..."
    author: "Kurt Pfeifle"
  - subject: "Great!"
    date: 2003-12-03
    body: "Not fluffy like most of these \"reviews,\" and written by someone who actually *used* KDE for some period of time. \n\nHe hits the exact right points --- his assessment of the strengths and weaknesses of KDE jibe precisely with what I've noticed using KDE/Linux as my desktop OS. "
    author: "Rayiner"
  - subject: "On the work needed on apps"
    date: 2003-12-03
    body: "He mentioned that some KDE applications need a lot of work to be brought up to the standard of, for example, Konqueror. Trying to badger individuals with no particular obligations to do this is pointless, of course, so perhaps the KDE Project could request help on these from distributors or companies that develop KDE, and particularly those who might use those applications?\n\nI'm not so sure removing them from KDE would help of course..."
    author: "Tom"
  - subject: "FreeBSD the wrong choice"
    date: 2003-12-03
    body: "Most of his complaints seems to be usability issues with FreeBSD, not KDE.\nMaybe he should have tried something that actually tries to be integrated and user friendly, like Mandrake or SuSe.\n"
    author: "frappa"
  - subject: "Re: FreeBSD the wrong choice"
    date: 2003-12-03
    body: "Which complaints? FreeBSD uses KDE exactly as it comes from kde.org. This is a review of KDE, not a review of KDE + some distro specific stuff like YaST. BTW, have you ever tried SuSE 8.2 or 9  on a 333MHz Pentium II?"
    author: "Thomas"
  - subject: "Re: FreeBSD the wrong choice"
    date: 2003-12-03
    body: "So he compares KDE to a complete OS and critizes things that KDE doesn't have control over, like software and hardware installation.\nBtw how well does OS X run on a G3 333? Not too well from what I have heard.\n"
    author: "frappa"
  - subject: "Re: FreeBSD the wrong choice"
    date: 2003-12-03
    body: "His complaints don't seem to be \"usability\" issues so much as integration issues. Unlike Mandrake and SuSE (which are still far from 100% integrated), FreeBSD prefers to ship XFree86 and KDE \"out of the box\" without tweaks or additions. There are advantages and disadvantages to this. For the newbie, this could be a large issue. But one of the basic philosophies of FreeBSD is that no one remains a newbie forever. Sometimes it's better to focus on the intermediate and advanced users.\n\nI really liked this article for one simple reason: the author stuck with it long enough to learn the complete system. I'm tired of reading reviews where the author gave up after one day, and recommends using their predetermined distro instead. If this article were instead written by the average OSNews reviewer, they would have recommended sticking with OSX in the second paragraph and spent the next two pages focusing on drawbacks of KDE and FreeBSD. That wouldn't have done anyone any good. But instead we see a very well balanced review that does not gloss over the bad points, and is not afraid to highlight the good."
    author: "David Johnson"
  - subject: "Re: FreeBSD the wrong choice"
    date: 2003-12-05
    body: "FreeBSD is the best choice for him. Let's see, he is accustomed to using BSD based OS-X and he had used FreeBSD in the past for servers. He has a dated, lower end computer and FreeBSD is a lot more \"lower end\" friendly than most linux distros. If he tried to use mandrake 9.1, he would have simply failed to have any satisfaction from the GUI. Perhaps his hardware would have been more readly recognized, but it wouldn't perform. He wouldn't have used enough to recognize KDE's strengths."
    author: "goo.."
  - subject: "Has QTDESIGNER Become OUTDATED?"
    date: 2003-12-03
    body: "\"With glade you layout interfaces, instead of painting them. You place widgets by creating container widgets like tables which hold the \"real\" widgets. The advantage of this method is scalable interfaces. What do interfaces consist of? Text, icons and widgets. If you use svg icons you have completely scalable interfaces with Gnome *now*. E.g. you rarely have to worry if text in less concise languages than English will fit on your widgets. Gtk will take care of it and dynamically resize the widgets at runtime. Users with bad eye-sight or running higher resolutions than normal can choose large systems fonts. Pixel-oriented interface design is primitive and a thing of the past.\"\n\nThis sounds very beneficial to me, is it really true that in QtDesigner a widget is not scalable automatically? It sounds like it is very outdated if this is true, in fact what are its benefits if this is true?"
    author: "Mario"
  - subject: "GNUstep will bury you all!!"
    date: 2003-12-03
    body: "http://www.gnustep.it/Renaissance/Screenshots.html\n\nautomatically scales widgets taking into account LOCALE etc etc. "
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: GNUstep will bury you all!!"
    date: 2003-12-03
    body: "So does Qt designer. Nice troll, but doesn't everyone have an application that creates XML for interfaces these days?\n\nhttp://doc.trolltech.com/3.2/designer-manual-16.html\n\nYou can even evaluate those at runtime if you want.That's part of what KJSEmbed can do for you: Click some forms. glue it with JavaScript, be done. No need to compile it :)\n\nCheers,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "COOL! "
    date: 2003-12-03
    body: "It seems that this is nothing unique and Qt can do all of this too.\n\nHere are some more responses to the same question from another forum:\n\n\"Also, Qt comes with default widget spacing, avoiding\nhaving to specify it in \"a million places\" when coding\na UI.\n\n  When it comes to fitting non-Enlish text and i18n.\nHave a quick look at Qt Linquist. You do _not_ need to\nbe a programmer to be a Qt translator.\"\n\nand\n\n\"It seems to me what you describe looks pretty like the use of QLayout and derivatives...\nWhatever it looks like, Gtk *is* pixel-based. Have a look at classes : QVBox, QGrid, QLayout,\nQHBoxLayout, and so on... and of course you can use them in designer.\""
    author: "Mario"
  - subject: "Re: COOL! "
    date: 2003-12-03
    body: "All just goes to show you really should stop selling your GNOME propaganda over here. :-)"
    author: "anonymous"
  - subject: "Re: Has QTDESIGNER Become OUTDATED?"
    date: 2003-12-03
    body: "I'm not sure, whether you are a KDE user, who wants Gnome to look bad, or you are serious about your question. Anyway, i will answer: Qt supports this for _years_, since it's 1.x days."
    author: "Rischwa"
  - subject: "Re: Has QTDESIGNER Become OUTDATED?"
    date: 2003-12-03
    body: "> This sounds very beneficial to me, is it really true that in QtDesigner a widget is not scalable automatically? It sounds like it is very outdated if this is true, in fact what are its benefits if this is true?\n\nErm, that has been a feature of Qt's layout system since before gtk existed. =)"
    author: "anon"
  - subject: "Re: Has QTDESIGNER Become OUTDATED?"
    date: 2003-12-03
    body: "This idea of designing a window like this was implemented in the original InterViews C++ toolkit and I'm sure the Amiga Intuition tool kit did it as well ( you could definitely smoothly rescale ). QT can do this as well. It's WIN32 and VB that brought in this idea of exact layout out of everything so you can force people to upgrade when they get a larger display.\n\n"
    author: "Nick"
  - subject: "Re: Has QTDESIGNER Become OUTDATED?"
    date: 2003-12-04
    body: "LOL, does WIn32 and MFC, and VB stil use that old technology with an exact layout?"
    author: "Mario"
  - subject: "Re: Has QTDESIGNER Become OUTDATED?"
    date: 2003-12-04
    body: "Hell, even Win.Forms (the new MS UI toolkit for .NET) still doesn't have a layout manager! I think its slated for Longhorn or something."
    author: "Rayiner H."
  - subject: "ROFL"
    date: 2003-12-04
    body: "WTF? Why, this seems to be an essential feature, are there any major disadvantages to it or is just Microsoft lazy?"
    author: "Mario"
  - subject: "Re: ROFL"
    date: 2003-12-04
    body: "The problem is that a lot of GUI designers have never learned to think in the different way that a packed layout requires. They treat GUI design like a bitmap paint package when in fact a better parallel is a vector drawing package.\n\nIt's true it requires a bit more effort to learn but once you've mastered it you can design your screen once and then forget about it. No matter what resolution it is run on.\n\nVisual Studio was never the best development platform for Windows, even if we ignore the poor complers. \n\nLet's just hope people like Herb Sutter, www.gotw.ca, who now work for them can improve it a bit.\n\n"
    author: "Nick"
  - subject: "Tips site?"
    date: 2003-12-03
    body: "For my part I got a few tips about things I hadn't noticed before (pdf printing, for instance). Is there a site/page (at kde.org or elsewhere) with KDE tips?"
    author: "Flipflop"
  - subject: "Re: Tips site?"
    date: 2003-12-03
    body: "KDE's wiki maybe? http://kde.ground.cz/"
    author: "anonymous"
  - subject: "2 points"
    date: 2003-12-06
    body: "The reviewer confirms two points I have made previously:\n\n1.  KDE needs to be commercial quality.\n\n    a.  KOffice should still have a version of < 1.0\n\n2.  KDE needs to provide more OS configuration front ends.\n\nThe second point will always put us at odds with all distros, leading to the logical result that we must have our own distro.  Our experience with RedHat should prove this point.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: 2 points"
    date: 2003-12-06
    body: "2 KXConfig, the mouse theme installer - it's really gettin' better in 3.2. I really like 3.2, I guess if Paul has tried the beta, he'd be far more impressed. I tried 3.1.4 a few days ago, and damn, I cant work with it anymore... esp Konqueror has been so much improved that the 'old' one is almost useless ;-)\n\nkeep up the good work, guys - I'm sure 3.2 will be shokking for the world..."
    author: "Superstoned"
---
When a computer crash drives men to do desperate things, they do strange things. They might even install KDE and like it so much they just have to write it up.  That's exactly <A HREF="http://e-scribe.com/osx/freebsd-kde-and-me/">what Paul Bissex has done</A>.  As a Mac OS X user he finds plenty to like about KDE/FreeBSD and has the screenshots to prove it.  He even says something nice about <A href="http://pim.kde.org/components/kpilot.php">KPilot</a>, which is a big relief to me.
<!--break-->
