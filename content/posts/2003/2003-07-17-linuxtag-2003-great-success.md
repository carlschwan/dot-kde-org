---
title: "LinuxTag 2003: A Great Success"
date:    2003-07-17
authors:
  - "dmolkentin"
slug:    linuxtag-2003-great-success
comments:
  - subject: "KDE Meeting?"
    date: 2003-07-17
    body: "Oh it would be so awesome to have a native OpenH323 app bundled with KDE!\n\nCurrently I'm using GNOME Meeting, which is not too bad, but the interface could certainly use KDE'fying...  not just look'n'feel wise, but annoying stuff you expect to work like better autocompletion in the callto:// bar, the chat window being more resizable, sound notifications not working (lack of aRts support).\n\nAnyway, a decent KDE version would just plain rock!\n\nPS Those Ark Linux system tools look pretty wicked too."
    author: "Navindra Umanee"
  - subject: "Re: KDE Meeting?"
    date: 2003-07-17
    body: "As usual there is already some code out there.  Unfortenately the current\ndevelopers have no time to continue working on it. \n\nInfo at:\nhttp://sourceforge.net/projects/konference/\nhttp://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/konference/konference/\n\nRichard"
    author: "Richard"
  - subject: "Re: KDE Meeting?"
    date: 2003-07-17
    body: "Hi Navindra!\n\nI had a reason for not adding a link to the Conference Software as it does not have a homepage yet. It's not Konference but a new project by Natasha Sainty from arklinux.\n\nSee http://www.arklinux.org/cgi-bin/cvsweb.cgi/video_conference/"
    author: "Daniel Molkentin"
  - subject: "maybe the presentations are online?"
    date: 2003-07-17
    body: "are the presentations (ppt, kpresenter, OO, png, html) online?\ncould be a nice read...\n\nemmanuel"
    author: "emmanuel"
  - subject: "Re: maybe the presentations are online?"
    date: 2003-07-17
    body: "No, at least not yet"
    author: "Arno Nym"
  - subject: "Re: maybe the presentations are online?"
    date: 2003-07-17
    body: "David Faure's debugging presentation sounds very interesting - an online version would be really nice for some of us far away.  thks."
    author: "Sage"
  - subject: "Re: maybe the presentations are online?"
    date: 2003-07-17
    body: "It was really interesting. But I'm not sure if the slides without the presented tests of valgrind and expecially kcachegrind are helpfull.\nFafnirx"
    author: "Fafnirx"
  - subject: "Re: maybe the presentations are online?"
    date: 2003-07-17
    body: "I agree, it was absolutely interesting (this is just for teaser: http://ariya.pandu.org/gallery/linuxtag/davidfaure.jpg).\n\nThe night after, I rushed to download the latest valgrind, cachegrind, and kcachegrind and jumped to start grindifying kspread.\n"
    author: "Ariya"
  - subject: "Re: maybe the presentations are online?"
    date: 2003-07-18
    body: "That is one hell of a large presentation screen!"
    author: "anon"
  - subject: "Re: maybe the presentations are online?"
    date: 2003-07-18
    body: "I told you it's just a teaser :-P"
    author: "Ariya"
  - subject: "printing.kde.org"
    date: 2003-07-17
    body: "In case you got a 404: The link to print.kde.org should point to printer.kde.org instead."
    author: "Olaf Jan Schmidt"
  - subject: "Re: printing.kde.org"
    date: 2003-07-17
    body: "OK, you got the right thing in the subject, but the wrong thing in the body.\n\nIt's printing.kde.org :)\n\nChris Howells\n(printing.kde.org Webmaster)"
    author: "Chris Howells"
  - subject: "More pictures ..."
    date: 2003-07-17
    body: "More pictures (with comments) are available at http://www.kde.de/events/linuxtag2003/"
    author: "Klaus Staerk"
  - subject: "article fixes"
    date: 2003-07-17
    body: "link fixes etc, applied.  thanks."
    author: "Navindra Umanee"
  - subject: "Video Conference - not Konference"
    date: 2003-07-17
    body: "All, we don't have a web site for the Video Conference project/app yet. Konference, which is linked in the article (http://konference.sf.net/) is a different project.\n\n- Video Conference may be viewed in CVS at:\n\n  http://www.arklinux.org/cgi-bin/cvsweb.cgi/video_conference/\n\n- Packages are available currently for Ark Linux (requires pwlib and openh323)\n- We'll try to get a web page up soon. :-)\n- You can find the Ark Linux team, including the main author of Video Conference, on freenode #arklinux and find out more about Ark Linux at:\n\n  http://www.arklinux.org/\n"
    author: "David Sainty"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "Uhm.... I downloaded the version from CVS, but I was unable to build it (PVideoOutputDeviceRGB is not defined anywhere here). Should those of us not using ark report bugs somewhere?\n\n"
    author: "uga"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "The RPM from current seems to work on Mandrake if you use --nodeps, as long as you have the dependencies for gnome meeting already installed."
    author: "Navindra Umanee"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "Thanks! I'll give it a try that way. Although I'd prefer to be able to compile it, since I use kde from HEAD usually for coding."
    author: "uga"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "PVideoOutputDeviceRGB should be somewhere in the OpenH323/Plib packages (as it starts with a P). Probably you have the wrong version..."
    author: "Arno Nym"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "I found only \"PVideoOutputDevice\" (without RGB) which is defined in the pwlib (v1.4.7) headers. I tried to replace it with this class name, so one file compiled, but I got several errors later on, related to virtual functions.\n\nIt seems that the latest version for pwlib out there is 1.5.0. I'll try to see if it works with that version."
    author: "uga"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "Yeap! its there :-) ptlib/videoio.h from Pwlib 1.5. Thanks very much for the hint."
    author: "uga"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "I converted the SRPM and tried to compile videoconference under Debian unstable.\nThen I installed libpt[-dev] and libh323[-dev] but it still fails to compile. I guess videoconference does not like my gcc 3.3.1 :-(\n\nIn file included from vcgrabber.h:40,\n                 from videoconference_part.cpp:45:\nvcdevice.h:54: error: syntax error before `{' token\nvcdevice.h:55: error: virtual outside class declaration\nvcdevice.h:55: error: non-member function `const char* GetClass(unsigned int)'\n   cannot have `const' method qualifier\nvcdevice.h: In function `const char* GetClass(unsigned int)':\nvcdevice.h:55: error: `PVideoOutputDeviceRGB' undeclared (first use this\n   function)\nvcdevice.h:55: error: (Each undeclared identifier is reported only once for\n   each function it appears in.)\nvcdevice.h:55: error: syntax error before `::' token\nvcdevice.h:55: error: incomplete type 'VCDevice' cannot be used to name a scope\nvcdevice.h: At global scope:\nvcdevice.h:55: error: virtual outside class declaration\nvcdevice.h:55: error: non-member function `BOOL IsClass(const char*)' cannot\n   have `const' method qualifier\nvcdevice.h: In function `BOOL IsClass(const char*)':\nvcdevice.h:55: error: incomplete type 'VCDevice' cannot be used to name a scope\nvcdevice.h: At global scope:\nvcdevice.h:55: error: virtual outside class declaration\nvcdevice.h:55: error: non-member function `BOOL IsDescendant(const char*)'\n   cannot have `const' method qualifier\nvcdevice.h: In function `BOOL IsDescendant(const char*)':\nvcdevice.h:55: error: incomplete type 'VCDevice' cannot be used to name a scope\nvcdevice.h:55: error: syntax error before `::' token\nvcdevice.h:55: warning: no return statement in function returning non-void\nvcdevice.h: At global scope:\nvcdevice.h:55: error: syntax error before `(' token\nvcdevice.h:60: error: syntax error before `*' token\nvcdevice.h:61: error: syntax error before `int'\nvcdevice.h:62: error: destructors must be member functions\nvcdevice.h:65: error: virtual outside class declaration\nvcdevice.h:66: error: virtual outside class declaration\nvcdevice.h: In function `BOOL IsOpen()':\nvcdevice.h:66: error: `GetFrameWidth' undeclared (first use this function)\nvcdevice.h: At global scope:\nvcdevice.h:67: error: virtual outside class declaration\nvcdevice.h:67: error: non-member function `PStringList GetDeviceNames()' cannot\n   have `const' method qualifier\nvcdevice.h:68: error: virtual outside class declaration\nvcdevice.h:70: error: syntax error before `private'\nvcdevice.h:91: error: syntax error before `}' token\n/usr/include/ptlib/pdirect.h:458: warning: inline function `static BOOL\n   PDirectory::Remove(const PString&)' used but never defined\nmake[2]: *** [videoconference_part.lo] Error 1\nmake[2]: Leaving directory `/tmp/video_conference-0.2/src'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/tmp/video_conference-0.2'\nmake: *** [all] Error 2\n"
    author: "Henning"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-17
    body: "> vcdevice.h:54: error: syntax error before `{' token\n\nYour problem is the same as mine. In that line it's trying to inherit from class PVideoOutputDeviceRGB which doesn't exist in your headers (pwlib 1.4.x probably?)  That's why you get the error.\n\n If you install version 1.5 of pwlib it _should_ compile fine.\n\n\n\n"
    author: "uga"
  - subject: "Re: Video Conference - not Konference"
    date: 2003-07-28
    body: "Congratulations for the port of GnomeMeeting! Are there screenshots?\nThat will show the KDE superiority again!"
    author: "None"
  - subject: "Are there transcripts?"
    date: 2003-07-17
    body: "Anywhere? I'd like to read the Kolab presentations.\n\nDerek"
    author: "Derek Kite"
  - subject: "Add me to the request list ;)"
    date: 2003-07-17
    body: "I'd like to read more about all those mentioned speeches as well."
    author: "Datschge"
  - subject: "Nice roundup :)"
    date: 2003-07-17
    body: "Well... Now I wish I could be there :)"
    author: "Stormy"
  - subject: "Dinner? You had dinner?"
    date: 2003-07-17
    body: "Wow! Having dinner implies that you actually found a restaurant in Karlsruhe?\n\nI couldn't find a sensible one at all -- not even after having driven an hour through the more urban parts of the city in search of a pizzeria. No, truth be told, there were two in the south-west of the city, but both were closed, one from 15:00 to 18:00, and the other one from 14:30 to 17:30. What in the world is wrong with restaurants in Germany? And those that are open are self-service (the vegetarian one near the centre of the town was not that bad, however)!"
    author: "Sinuhe"
  - subject: "Re: Dinner? You had dinner?"
    date: 2003-07-18
    body: "Well, to be honest: there are hundreds of restaurants in Karlsruhe (I used to live there for many years) and many good ones. I wonder who you asked to find one...\n"
    author: "Norbert"
  - subject: "Re: Dinner? You had dinner?"
    date: 2003-07-18
    body: "> not even after having driven an hour \n\nThat's your mistake right there: If you had *walked* through the pedestrian zone that covers most of the inner city, you'ld have had trouble *not* finding a place to eat."
    author: "Anno v. Heimburg"
  - subject: "Kolab announcement?"
    date: 2003-07-18
    body: "Kolab server and client are now 1.0.\nHmm, time for an announcement on all sites (dot, kde.org, kolab.org)?\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "Typo"
    date: 2003-07-18
    body: "The sentence \"there was lots of socialization\" should read \"There was lots of socializing.\"\n\nSocialization refers to the process whereby an invididual is made accustomed to his/her society. It also means the act of making a sector of the economy government-run."
    author: "Bob"
  - subject: "Re: Typo"
    date: 2003-07-18
    body: "Maybe the sentence was right. It is a bunch of computer programmers we're talking about here :-}\n\nDerek"
    author: "Derek Kite"
  - subject: "fli4l-Gallery from LinuxDay"
    date: 2003-07-23
    body: "On http://www.fli4l.de/gallery/lt2003 you can see the fli4l-Gallery from LinuxDay in Karlsruhe.\n\nCarsten"
    author: "Carsten"
---
As always, the KDE Team participated in this year's <a href="http://www.linuxtag.org">LinuxTag</a> which took place in Karlsruhe, Germany. LinuxTag is a platform for Free Software projects and Linux related companies with an exhibition area and a wide range of talks. With more than 19,500 visitors, this year's LinuxTag has become the largest Linux event in the world. Even away from the KDE booth, there was a lot of KDE
stuff to see. (<b>Update:</b> Pictures of <a href="http://www.suse.de/~cs/LinuxTag-2003/images.html">KDE/GNOME dinner</a> added.)
<!--break-->
<p>
As usual, the <a href="http://www.jowenn.at/kde/LT2003/p7110165.jpg">KDE booth</a> in the exhibition area was crowded. There, both KDE 3.1 and CVS HEAD were shown on four machines. As a special feature, KDE was shown on an Opteron which was made available by AMD. There were almost no problems getting it to compile, and it worked great. Many people dropped by to see the latest developments and were fascinated by the whole range of promising new features and applications such as <a href="http://www.kontact.org">Kontact</a>, KDE's future Groupware suite and <a href="http://www.koffice.org/kexi">Kexi</a>, a database management system for office users. Many users also used the opportunity to talk directly to the developers in order to provide feedback and suggestions. KDE developers used the hacking area to jointly develop new ideas and hack on KDE. 
<p>
There was a lot of socialization. A lot of new project participants had a chance to meet some of the other developers. LinuxTag's Social Event on Friday was big fun this time and a good opportunity to talk (and have a beer!) with members of other projects or companies.
Thursday night, KDE and Debian guys had dinner together. On Saturday, Gnome and Debian folks joined us for a dinner organized by KDE's Ralf Nolden. All those events were really enjoyed by the participants. Yet that did of course not stop people from unpacking their laptops at some point and starting to hack. It's a passion.
<p>
At the booth of the German <a href="http://www.bsi.bund.de">Federal Office for Information Security (BSI)</a>, <a href="http://www.intevation.net/">Intevation</a> and the KDE PIM developers Ingo Kl&ouml;cker, Marc Mutz, Tobias K&ouml;nig and Daniel Molkentin <a href="http://www.tr.debian.net/gallery/lt-ka-2003/akb">presented</a> <a href="http://kolab.kde.org"> Kolab</a> which had its first official release there. Again, the feedback from the visitors, among them a lot of IT decision makers from companies and government agencies was very positive. So expect Kolab to hit your desktop at work very soon! <a href="http://www.credativ.de">Credativ</a> showed a KDE desktop on Debian basis, also running Kolab.  The "Office Productivity Booth" demo'ed <a href="http://printing.kde.org/">KDE's printing capabilities</a> in action.
<p>
A lot of KDE related talks took place at the conference area. In <a href="http://www.linuxtag.org/2003/de/conferences/talk.xsp?id=23">his speech</a>, Ralf Nolden presented KDE and gave an overview of new features in KDE 3.2. Bo Thorsen, who was kindly assisted by Martin Konold, <a href="http://www.linuxtag.org/2003/de/conferences/talk.xsp?id=49">gave a talk</a> on Kolab which was very well received. Bernhard Reiter from the FSF Europe <a href="http://www.linuxtag.org/2003/de/conferences/talk.xsp?id=95">reported</a> on his experience with Kroupware and &Auml;gypten. Andreas Brand from the University of Frankfurt gave an <a href="http://www.linuxtag.org/2003/de/conferences/talk.xsp?id=137">interesting overview</a> of KDE's social structure which he analyzed by studying the community and conducting interviews at the last LinuxTag. David Faure presented the power of <a href="http://developer.kde.org/~sewardj/">Valgrind</a> and <a href="http://kcachegrind.sourceforge.net/">KCacheGrind</a> in his talk about effective software debugging.
<p>
We found many companies running KDE, among them giants such as HP and SAP. At the booth of <a href="http://www.arklinux.org">Ark Linux</a>, KDE hacker Bernhard "Bero" Rosenkr&auml;nzer demonstrated a Linux distribution with impressive seamless integration of system management tools within KDE as well as an <a href="http://www.arklinux.org/cgi-bin/cvsweb.cgi/video_conference/">OpenH323 
based video conferencing software</a> for KDE based on <a href="http://www.gnomemeeting.org">GnomeMeeting</a>.
<p>
LinuxTag once again proved to be a very good platform for a mix of hacker gathering and representation with a unique atmosphere. We all look forward to meet you at LinuxTag 2004!
<p>
The inevitable photo series:
<ul>
<li><a href="http://www.kde.de/events/linuxtag2003/index.php">Konrad Rosenbaum's photos (at KDE.de)</a></li>
<li><a href="http://www.jowenn.at/kde/LT2003/">Joseph Wenniger's photos</a></li>
<li><a href="http://ktown.kde.org/~tackat/LinuxTag2003/">Tackat's photos</a></li>
<li><a href="http://www.tjansen.de/blog/2003_07_06_archive.html#105805042480656469">Tim Jansen's photos</a></li>
<li><a href="http://www.tr.debian.net/gallery/lt-ka-2003">Andreas M&uuml;ller's photo gallery</a></li>
<li><a href="http://ariya.pandu.org/gallery/linuxtag/">Ariya Hidayat's photo gallery</a></li>
<li><a href="http://www.suse.de/~cs/LinuxTag-2003/images.html">Chris Schlaeger's photo gallery, including GNOME/KDE dinner</a></li>
</ul>