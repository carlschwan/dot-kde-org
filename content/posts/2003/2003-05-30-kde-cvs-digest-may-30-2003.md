---
title: "KDE-CVS-Digest for May 30, 2003"
date:    2003-05-30
authors:
  - "dkite"
slug:    kde-cvs-digest-may-30-2003
comments:
  - subject: "Good work"
    date: 2003-05-30
    body: "I always look forward to the weekly cvs digest.  As always great work (a little early this week too....)"
    author: "Avery"
  - subject: "Tab Widgets"
    date: 2003-05-30
    body: "Of course the KTabWidget is new in kdelibs and only used in Konqueror now. And it doesn't require Qt 3.2 as whole HEAD shouldn't require it yet."
    author: "binner"
  - subject: "Re: Tab Widgets"
    date: 2003-05-30
    body: "are there any screenshots of the new tab widgets someplace?"
    author: "anon"
  - subject: "Re: Tab Widgets"
    date: 2003-05-30
    body: "This screenshot shows how Konqueror/HEAD uses it. Missing is the mouse pointer between the tooltip and the close button shown on icon hover."
    author: "binner"
  - subject: "Re: Tab Widgets"
    date: 2003-05-31
    body: "What about new icons for opening/closing tabs? The current ones are not that beautiful in my opinion."
    author: "tuxo"
  - subject: "Re: Tab Widgets"
    date: 2003-05-31
    body: "How about drawing some alternative icons? It looks like many icon sets are still missing these (any many other) icons."
    author: "Datschge"
  - subject: "Re: Tab Widgets"
    date: 2003-06-02
    body: "thanks\n\n\nxxx"
    author: "anon"
  - subject: "thanks Derek !"
    date: 2003-05-30
    body: "Your CVS Digest is always interesting !"
    author: "quarus"
  - subject: "Screen shots?"
    date: 2003-05-30
    body: "Anybody have screen shots of new konqueror tab widget?\nDoes this implementation use the color indication of the page status (loading, ...)?  How can you open a link into a new tab while you keep viewing the current? (I can't wait for the 3.2 publication :-) )\n\nThanks a lot!\n"
    author: "Torpedo"
  - subject: "Re: Screen shots?"
    date: 2003-05-30
    body: "For screenshot see above. The tab widget allows colored tabs and Konqueror uses it for page status indication. And KDE 3.1 already had Ctrl+left mouse button, an general option and right mouse button menu entry to open new tabs in background."
    author: "binner"
  - subject: "Re: Screen shots?"
    date: 2003-06-01
    body: "Does it's API allow for the active tab to be bold, to distinguish it from the rest? That would be quite helpful.\n\nCheers,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Screen shots?"
    date: 2003-06-01
    body: "No. This sounds more like a style issue."
    author: "binner"
  - subject: "Re: Screen shots?"
    date: 2003-06-01
    body: "the QStyle widget theme in current use can make active tabs bold. Whether this looks nice is really dependent on the style."
    author: "lit"
  - subject: "Re: Screen shots?"
    date: 2003-06-01
    body: "FWIW: I like the current black/red/blue (depending on the state) tab titles a lot!"
    author: "Melchior FRANZ"
  - subject: "Thanks!"
    date: 2003-05-30
    body: "Even if we're already accustomed with the great KDE-CVS-Digest i've to thank you for bringing us always the bleending edge of kde development!\nI can't wait anymore, I need to read it! Thank you!!\n  enrico"
    author: "Enrico Ros"
  - subject: "ktabwidget && kmdi"
    date: 2003-05-30
    body: "1. Is kmdi going to be ported to use ktabwidget so that apps like kdevelop and kate can use it's features? Is konsole going to be ported to kmdi or ktabwidget solely? Is konqueror going to be ported to kmdi? I'm confused. \n\n2. The close button on tabs in Konq is sweet, and a long overdue addition (props to whoever worked on it..), but is there any way to use Mozilla-like behavior, as in close button in the edge? I find this far more space saving and less annoying than the galeon-like close buttons currently in CVS... A lot of people who undoubatly switch BACK to konq in KDE 3.2 (gotta love a better tab implementation), from browsers like Mozilla, Firebird (Phoenix), and Epiphany, will probably love a setting to switch it to the side. Only current Galeon users will like it, but galeon is fast moving to a state if irrelevence, as it was snubbed for inclusion into GNOME 2.4 in favor of Epiphany (which btw, has a sweet bookmarks system that any konq developers should try.. could be made into a kioslave, *hint* *hint*)\n\nAnyway, great work as usual in CVS kde developers, kde 3.2 is shaping up to be a great release. Oh yeah, thanks as usual Derek!"
    author: "phluid"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "Konsole is perfect the way it is.  Please don't change nothing."
    author: "anon"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "it would be nice to be able to \"spawn\" new shells using ctrl+shift+n just like spawning new tabs in konqy."
    author: "Mark Hannessen"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "Use Ctrl+Alt+n (since KDE 2.2) or \"Settings/Configure Shortcuts...\". :-)"
    author: "binner"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "thats really cool !! thanks a lot.\n\nbut isn't it confusing to have two different key combinations for two things that are so similair? i tried ctrl+shift+n naturaly in konsole because konqy did it that way, when ctrl+shift+n didn't work in konsole i thought it wasn't inplented and didn't bother searching. i probably should have looked a little better, but i bet there are more people who made that mistake.\n"
    author: "Mark Hannessen"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "I agree, it should be consistent. That's why I submited this whishlist report:\nhttp://bugs.kde.org/show_bug.cgi?id=53855\n\nPlease vote for it.\nThanks\n"
    author: "jadrian"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "Konsole was there first. :-) Its shortcuts were chosen to don't clash with common shell shortcuts. Konqueror chose others because its shortcuts conflicted e.g. with KLineEdits in web pages. It's not easy to find common shortcuts. I changed Konsole to default to Ctrl+Shift+N as alternate shortcut for \"New Session\"."
    author: "binner"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "But konsole already does that. Doesn't it?\n\n*switch-to-konsole* - *press ctrl-alt-n* - *getting new shell*\n\nYes, it does.\n\nOr it might be a special custumisation i did myself.\n"
    author: "Matthias"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "I plan to change Konsole to use KTabWidget. A toolbar button group is a wrong GUI representation for multiple sessions."
    author: "binner"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "Argh.  That would  be quite awful.  It looks and works great the way it is, especially with the bar at the bottom and with text beside icons. For an idea of how bad normal tabs would look try gnome-terminal. \n\nPlease get consensus and check with the maintainers before making this big change. :("
    author: "anon"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "I agree! The buttons look beautiful.... frankly, using tabs would be ugly (especially in keramik, but that's just a gripe I have with keramik's tab widget)\n\nNo tabs in konsole! PLEASE!"
    author: "optikSmoke"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-01
    body: "Try Keramik/II for slightly different tabs or use a different style."
    author: "binner"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-01
    body: "I use Mandrake Galaxy at the moment..... I haven't found a perfect style as of yet, but some of the nice little touches in Galaxy allow me to ignore some of the little things I don't like."
    author: "optikSmoke"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-01
    body: "same here :-)"
    author: "Andre Lourenco"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "> especially with the bar at the bottom and with text beside icons.\n\nWhat makes you believe that this will go away?\n\n> Please get consensus and check with the maintainers before making this big change. :(\n\nI convinced at least myself already. :-)"
    author: "binner"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "Ah no, please don't use tabs instead of buttons, it's perfect the way it is now! Gnome terminal is ugly!"
    author: "tuxo"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "Another reason why tabs is not a good idead is that with terminals, the focus is always on the bottom (where the cursor is). Now with tabs on top a la konqueror or gnome-terminal, one has to switch focus all the time when changing a terminal. With the current button-based implementation, it is possible to have the button bar on the bottom, so one doesn't have to move the eyes up and down while switching."
    author: "tuxo"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "You can move ktabwidget (and qtabwidget) in top and bottom, and hopefully someone will extend it to rotate to left and right too like kmultitabbar can (which is the widget used in konqueror's sidebar, afaik)\n\nPerhaps the konsole tabbar should be set to bottom by default."
    author: "lit"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "> For an idea of how bad normal tabs would look try gnome-terminal. \n\nYes, do try gnome-terminal in fact. Tabs work quite great for this purpose.  Konsole's buttons and tabs are the same metaphor.. having konsole use buttons instead of tabs has been a long-running UI consistancy bug since KDE 2.0 (konsole in KDE 1.2 didn't have MDI, did it? did kvt?) that simply needs to be fixed sooner or later. It's actually possible now that we have a tab widget that is as powerful as Konsole's buttons+a toolbar were. Back in Qt 2.x, I remember qtabwidget being pretty hard to extend, not sure when it got fixed :) I'm sure when konsole was original developed, it would have used a tab widget if there was one sufficient for it at that time.\n\nI'm personally looking forward to colored tab highlights ala konqueror in konsole (the current notfications, etc...)"
    author: "fault"
  - subject: "Please keep the buttons"
    date: 2003-05-31
    body: "I'm going to join the chorus and say I like Konsole the way it is.  When working in a terminal, the command line is almost always at the bottom of the screen.  It makes sense to place the tabs/buttons close by.  \n\nI took a look at gnome-terminal, and it just doesn't feel right.  I usually switch terminals using shift-left and shift-right.  I like the fact that I can watch the buttons change right below my command line.  This is one of my favorite features of Konsole, making it my terminal emulator of choice.\n\nI also like the \"new\" button on the left-hand side.  If you were to switch to tabs, the next step would be to add a toolbar for the \"new\" button.  Frankly, that would just add clutter.\n\nYou might notice that Gideon also uses buttons instead of tabs.  They work quite well.\n\nBy the way, Konsole lets you move the toolbar to the top.  Does anyone actually do that?"
    author: "Mike"
  - subject: "Re: Please keep the buttons"
    date: 2003-05-31
    body: "> I'm going to join the chorus and say I like Konsole the way it is. When working in a terminal, the command line is almost always at the bottom of the screen. It makes sense to place the tabs/buttons close by.\n\nUhm, ktabwidget supports being at the top *and* at the bottom. I assume that it'll be put at the bottom by default in konsole.\n\n> If you were to switch to tabs, the next step would be to add a toolbar for the \"new\" button\n\nHave you seen ktabwidget within Konqueror? The new button is within the tabbar. You could probably put a close button on other side too.\n\n> I like the fact that I can watch the buttons change right below my command line.\n\nIt would probably pretty easy to let the tabs accept that kind of focus.. If it can't already."
    author: "fault"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "NO PLEASE!!! Konsole is OK the way it is now. DONT USE TABS IN KONSOLE!\n"
    author: "nbensa"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "Your arguments are really convincing. Wait - what arguments? It would help if people would read the other comments before whining about losing a bottom bar and other things. :-)"
    author: "binner"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-01
    body: "Tabs aren't only OK, they are a requirement. Buttons just don't make sense for switching konsole instances. That's completely inconsistent. People will soon forget the buttons, once they see the new (very well done!) tabs at the bottom of their konsole window. (Back oriented people are free and strongly encouraged to fork konsole and maintain their very own implementation. :-)"
    author: "Melchior FRANZ"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-02
    body: "Yes, I think its better to use ktabwidget, wasn't it its target?!. Long life to code reusing...\n"
    author: "Torpedo"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-02
    body: "IMHO, first keramik's bulky buttons and tabs should be flattenned a bit before this change. They take much more space than the buttons when ->setFlat(true);\n\nWhen I code, I usually hate to put keramik's buttons just for that, so I *always* use the flat option in my dialogs. I need to learn coding qt's styles asap...."
    author: "uga"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-03
    body: "Keramik 2 addresses this, anyway you could always use a different style"
    author: "MxCl"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-02
    body: "If Keramik's tabs where flat that would look nice, but it just doesn't look right for me with bulky buttons on the bottom. Is there a ->setFlat() option for tabs? That would be nice. Otherwise I'll have start making my own patched konsole ;-)"
    author: "uga"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-02
    body: "> there a ->setFlat() option for tabs?\n\n\nYes, there is in fact.. However, it really depends on the style maker to implement such a varient tab. Most KDE and third-party styles don't include it.  TrollTech's do.\n\nBTW, KeramikII's tabs are less bulky than Keramik3.1's tabs."
    author: "till"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-02
    body: ">> there a ->setFlat() option for tabs?\n> Yes, there is in fact..\n\nHuh?\n"
    author: "SadEagle"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-02
    body: "ugh.. too much coffee.. i was referring to qwidget::setTabShape(QTabWidget::triangular) "
    author: "till"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-03
    body: "I'm I the only one who works in Konsole without the scroll bar, the menu bar, the status bar, the tool bar and in full screen all the time? I'm I the only one who sees all those bars, icons and widgets as distractions? I'm the only one who see KDE's interface getting cluttered and uglier as the day passes by? I'm I the only sick brother around? Even Konqueror's Keramic style tabs look bulky, bulgy, cluttered, unnatural and just plain annoying. Isn't it about time KDE's user interface designers and engineers started incorporating the simple, sleek and slim fundamentals into their design philosophy. Visible tabs on a CLI is just blatantly inexcusable. Even if you are to incorporate them, please design them with simplicity and style in mind and make them less conspicuous or less distracting. \n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-03
    body: "> Even Konqueror's Keramic style tabs look bulky, bulgy, cluttered, unnatural and just plain annoying. \n\nwait for KeramikII in KDE 3.2 :).. less bulky tabs\n\n> Visible tabs on a CLI is just blatantly inexcusable.\n\n\nA nice balance might to be to hide the tabbar in konsole (like it is in Konq), when only tab is in use.\n\nThis has been a main feature of konsole since KDE 2.0. It's one of the main reasons people uses Konsole versus something like Eterm or aterm. When gnome-terminal did not have it, people screamed until it did. \n\nThis brings up a good point though-- a lot of people use KDE for it's features. This of course doesn't mean KDE developers should not focus on slimness and sleekness (which were main features of KDE 1.2...) \n\nI do agree that KDE is quite a bit more cluttered than it used to be.. Especially things like the control center. Of course, on going work (http://usability.kde.org/activity/completed/panelconfig.php) is always going on in that sector.\n"
    author: "till"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-03
    body: "autohiding would rock"
    author: "lyp"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-03
    body: "What status bar?"
    author: "Anon"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-03
    body: "I don't get it.  Juts turn off all the options you don't like and use the light widget style.  The sleek, no-clutter CLI that you describe is then right there in front of you, so what's the problem?  "
    author: "LMCBoy"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-30
    body: "Look at the screenshot by binner.  There is an X on the side like mozilla.  The x in the tabs don't waste any space anyway since they just take the place of the favicon on hover."
    author: "anon"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "unfortunately i find the new close tab method annoying at times, since it sometimes accidently press on the icon of the website on the tabwidget to select it (which closes it of course) and crashes konqueror coincidently too :)\n\nit looks ugly anyway so i would rather close them using right click -> close tab :)\nalso the red color text on the tabs when there is some loading looks a little out of place with the colour scheme. i dont know what you could do about that though...\n\nanyway cvs continues to be very stable and nice so keep up the excellent work people!\n\nps: keramikII has some nice little additions "
    author: "not_regisitered"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "Instead of the red, maybe steal the animation from Mozilla?"
    author: "anon"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "While hiding the site icon? And how to display the \"unread\" state (without hiding the site icon)? In my opinion it's better to display both states with colors rather one with an animation and the other with color."
    author: "binner"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "overlay?\n"
    author: "Sad Eagle"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-05-31
    body: "While often a great idea I don't think overlay makes much sense here, the size of a favicon is imo way too small for this purpose, and additionally it doesn't help anyway if the favicon's look itself is not know yet to the user."
    author: "Datschge"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-01
    body: "Well, the little star from new window/tab/file icons may fit. I am not sure the icon loader handles overlays this small, though. \n"
    author: "Sad Eagle"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-01
    body: "Using colours for this is almost inherently flawed.  What if someone uses a different colour scheme?  Even if it's configurable it's too much bother. What if someone is colour blind?"
    author: "anon"
  - subject: "Re: ktabwidget && kmdi"
    date: 2003-06-01
    body: "It will be either dropped, configurable or use color scheme colors like the newest version. And AFAIK color blind people can differ black, red (appears as brown to red/green blind) and blue."
    author: "binner"
  - subject: "What happened to mouse gestures?"
    date: 2003-05-30
    body: "Khotkey 2 is still not merged in. Is it so buggy that people gave up?"
    author: "Gesture lover"
  - subject: "Re: What happened to mouse gestures?"
    date: 2003-05-31
    body: "You please do it, please, please, please."
    author: "Another gesture lover"
  - subject: "Re: What happened to mouse gestures?"
    date: 2003-05-31
    body: "> Is it so buggy that people gave up?\n\nBy no means... CVS is still a work in progress."
    author: "trill"
  - subject: "Will KDE add something like the eclipse tab widget"
    date: 2003-05-30
    body: "When eclipse has to deal with a number of tabs, it adds a sort of combo box on the side \nwhich allows the user to pick a particular tab by name, which is a lot more fun that trying\nto go \"left, left, left, oops too far, right, right\".   It also has a previous and next tab capability.\n\nI'd love to see this kind of thing for konqueror."
    author: "TomL"
  - subject: "Re: Will KDE add something like the eclipse tab widget"
    date: 2003-05-31
    body: "Konqueror already has previous/next tab functionality.  I have no idea what you talking about the eclipse though."
    author: "anon"
  - subject: "Re: Will KDE add something like the eclipse tab widget"
    date: 2003-06-02
    body: "Eclipse has previous and next tab capability in the toolbar, in a nice convenient place.\nBelieve me, eclipse does their tabs in a very, very nice way."
    author: "TomL"
  - subject: "Re: Will KDE add something like the eclipse tab wi"
    date: 2003-06-02
    body: "How about screenshots and more detailed descriptions of its behavior?"
    author: "Datschge"
  - subject: "WOW,  Lot sof changes in KDE this week!"
    date: 2003-05-30
    body: "Thanks derek Kite and Kde developers!!! \n\nAlso, I think the Eclipse idea is pretty cool, amybe you should add it as a wishlist item  to add as a prefference.\n\nSpeaking of tab features, here are some cool ideas at KDE-LOOK.org: http://www.kde-look.org/content/show.php?content=6319 its rough, but if at least #6 would be implemented that would be awesome."
    author: "Alex"
  - subject: "Re: WOW,  Lot sof changes in KDE this week!"
    date: 2003-05-31
    body: "> http://www.kde-look.org/content/show.php?content=6319\n\nPretty good ideas! I think all of them are pretty much possible with the new tab widget.. I just hope that there are enough developers free enough to implement them all. "
    author: "lit"
  - subject: "Re: WOW,  Lot sof changes in KDE this week!"
    date: 2003-05-31
    body: "> http://www.kde-look.org/content/show.php?content=6319\n\nPretty good ideas! I think all of them are pretty much possible with the new tab widget.. I just hope that there are enough developers free enough to implement them all. "
    author: "lit"
  - subject: "Question"
    date: 2003-05-31
    body: "I read that when you hover a close buttona ppears directly on the tab. This seems very bad usability wise, what if I click to select a tab where the close button is supposed to appear? Wouldn't that close the tab. I don't think i want that, especially since there is already the mozilla way to close tabs. Is there a way to disable it?\n\nAlso, can you edit the tab toolbar likea  normal toolbar so i can for example remove the add new tab and close tab button if i want and just use the extra toolbar in Konqueror. god, i really hope this is possible. I ahte having diifferent buttons taht do the same thing."
    author: "Alex"
  - subject: "Re: Question"
    date: 2003-05-31
    body: "> I read that when you hover a close buttona ppears directly on the tab. This seems very bad usability wise, what if I click to select a tab where the close button is supposed to appear? Wouldn't that close the tab.  \n\nI haven't tried it out personally yet, but the way I beleive it works is that the close button only pops up after a \"hover time\".. that is, a delay. This is to avoid mistaken clicks. \n\nBtw, is middle button closing ever going to be implemented? Or has it already? It's the thing I miss most from Moz's tabs.\n\n> Is there a way to disable it?\n\nSee http://lists.kde.org/?t=105426566900002&r=1&w=2\n\nApparently no GUI option yet. "
    author: "fault"
  - subject: "Re: Question"
    date: 2003-05-31
    body: "> I beleive it works is that the close button only pops up after a \"hover time\".. that is, a delay.\n\nIt didn't have a delay initially but now has for trial.\n\n> is middle button closing ever going to be implemented? Or has it already?\n\nNo, see http://bugs.kde.org/show_bug.cgi?id=59172. Middle button is pasting under Linux/Unix and that is what it does in Konqueror and Mozilla's Linux/Unix versions too."
    author: "binner"
  - subject: "Re: Question1.  Close with middle click"
    date: 2003-11-01
    body: "No. In mozilla-firebird for windows the default behavior is to close tabs\nwith the middle button. To make it work in GNU/Linux just do the following:\n\n1.  Open Firebird, type \"about:config\" as the url.\n2.  Search for the option \"middlemouse.contentLoadURL\", set it to `false'\n3. DONE!\n\nI miss this in konqueror, I really really really miss it. Hope there is/will be a \nway to configure that.\n\nheavyvoid"
    author: "heavyVoid"
  - subject: "what about this??"
    date: 2003-05-31
    body: "\nWhat about fixing this _annoying_ flickering-toolbar bug...???\n\nhttp://bugs.kde.org/show_bug.cgi?id=39234\n\n\nThanks voting for it!"
    author: "yg"
  - subject: "Re: what about this??"
    date: 2003-05-31
    body: "\n\nand I've got more:\n\nhttp://bugs.kde.org/show_bug.cgi?id=57189\nhttp://bugs.kde.org/show_bug.cgi?id=58040\nhttp://bugs.kde.org/show_bug.cgi?id=59113\n\n\nplease vote :-)"
    author: "yg"
  - subject: "Re: what about this??"
    date: 2003-05-31
    body: "I beleive the second is already fixed in CVS.. can anyone confirm?"
    author: "lit"
  - subject: "Re: what about this??"
    date: 2003-05-31
    body: "I beleive the second is already fixed in CVS.. can anyone confirm?"
    author: "lit"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "I have KDE 3.1.2.\n\n#57189 on my 400MHz K-6 III, it is running in Konqueror with 3 other tabs and using:\n\n20 to 30% of CPU.\n\nI'm also compiling the updates on KDE_3_1_BRANCH on a console. \n\nI have no problems.\n\nShould I assume that it is fixed and close it WFM?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "the image loading bugs are the ones that bother me mostly, i saw mosfet was working on atleast the .gif issue ... right ?\ni have 512 meg ram but my konqueror regulary gets OOM killed when i load big .jpg's or .gifs.\nthe kview part has the same problem (i use it instead of the standard image part because of zoom functionality), and especially when zooming i get OOM kills (guess because the pixmap is scaled in memory instead of just repainted)\n"
    author: "ik"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "OK, I'll vote for that because it is a general problem which is more annoying on a medium speed system 400MHz K-6 III.\n\nFlickering is the result of unneeded screen redraws.  This issue should receive more attention -- eliminating them not only eliminates this anoying problem but should result in an increase in performance.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "Where did you get the idea that the flickering is a result of an unneeded redraw? It is a needed redraw, the flickering always occures when you change the view. When changing the view you also change the kpart/embedded KDE app which again has to update the toolbar with its own set of merged icons. Just open eg. a web site and a calendar file like at http://www.apple.com/ical/library/ in separate tabs, switch between them and see what merged toolbar buttons are and how \"unneeded\" the redraw actually is.\n\nMy suggested fix (as mentioned in the report) is delaying the redraw instead showing it in \"real time\" for avoiding the flickering."
    author: "Datschge"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "Excuse my imprecise English.\n\nI should have said redundant redraw.\n\nDo you also feel that redundant redraw is good?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "Well, code could be added to compare the formerly merged toolbar buttons with the newly merged toolbar buttons and only do a redraw when they actually differ, but I don't know whether this is actually improving anything. The flickering would then only appear when the old and new buttons differ. I tend to think that this additional code is worse that just doing the redraw in the end since the redraw flickering issue has to be fixed elsewhere anyway."
    author: "Datschge"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "The flicker isn't a unnecessary redraw that occurs I think-- it's just the erasing of the background several times I beleive."
    author: "lit"
  - subject: "Re: what about this??"
    date: 2003-06-01
    body: "Redundant redraw is a general problem in many (if not all) desktops.\n\nHowever, I note that simply redraws something won't cause flicker.\n\nIt is necessary to first erase it and then redraw it to cause flicker.  Clearly, code that does this could use a little improvement.\n\nAnd, yes this bug should be moved to the code that actually draws the icons.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: what about this??"
    date: 2003-06-02
    body: "Sure, but that would only help when the buttons dont change... cleaner solutions would include a) speeding up the redraw, there is no reason why it has to flicker or b) wait/hope for a backbuffered, Quartz Extreme-like X11solution which should make it easy to hide the flicker."
    author: "AC"
  - subject: "Re: what about this??"
    date: 2003-06-02
    body: "I don't think that speeding up the redraw will remove the flickering since the flickering is afaics a result of showing in real time how the dynamical old toolbar buttons are getting unloaded and the new ones merged in."
    author: "Datschge"
  - subject: "Re: what about this??"
    date: 2003-06-02
    body: "Yes, but who says that it is neccessary to redraw the toolbar after unloading the old kpart? That may be a Qt-related problem (when you remove a widget the parent will be redrawn automatically), but still a solvable problem... QWidget would need a \"dont redraw unless you get an exposure message\" flag.\n\n\n\n"
    author: "AC"
  - subject: "Re: what about this??"
    date: 2003-06-02
    body: "I think the quickest way to do this to use QWidget's (whatever toolbar ptr's)->setUpdatesEnabled(false) before switching views, and setUpdatesEnabled(true) after the toolbar has been merged in. Hope that the merging code uses QWidget::update and not QWidget::repaint."
    author: "till"
  - subject: "Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "Can anyone tell me if the new KTabWidget will allow custom buttons to be added next to the New/Close buttons? If not, then there's no point in my using them."
    author: "Troy Corbin Jr."
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "You only need QTabWidget of Qt 3.2: http://doc.trolltech.com/3.2/qtabwidget.html#setCornerWidget"
    author: "binner"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "Thanks for the link, it was helpful, but unfortunatly it shows that no, I can not add an extra ( third ) widget at this time. ( Sorry, I should have explained that better in the first post )"
    author: "Troy Corbin Jr."
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "Ever heard of nested QWidgets?"
    author: "binner"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "Ever heard of being polite?"
    author: "Troy Corbin Jr."
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "Ever heard about looking at the source or asking politely before ranting?"
    author: "anon"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "How primitive!"
    author: "Somebody"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "*sigh* And we wonder why the KDE community is considered obtuse. Obviously Binner didn't think my question impolite, else he wouldn't have answered. I even said \"Thanks\" and \"it was helpful\" in my initial reply. This was not polite? I apologise. Perhaps you, in your trolling, anonymous wisdom can enlighten me, nay, the entire community on polite conversation?\n\nOf course I've heard of looking at the source. When you were in school, did your teacher tell you to research every question you wanted to ask her? The point is that if you don't want to help, then DON'T. The world will still revolve around it's axis without your dull wit. Unless, of course, you're just trolling for a reaction, in which case I humbly suggest you go play on Slashdot."
    author: "Troy Corbin Jr."
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "Ehem, I beg you to differ. Binner gave you a hint how you can add \"add an extra ( third ) widget at this time\", by his short answer it should be obvious that it's obviously possible to have \"nested QWidgets\" which would allow you to do what you want. But that time you didn't say \"Thanks\" and \"it was helpful\" but \"Ever heard of being polite?\" so don't be surprised about similar answers with a similar tone."
    author: "Datschge"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-02
    body: "\"Ever heard...\" IS impolite."
    author: "Somebody"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-02
    body: "Thank you. I'm glad *somebody* understands. Heh, get it? Somebody? ;-)"
    author: "Troy Corbin Jr."
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-02
    body: "No, it isn't. Starting a fight whenever someone *thinks* the other one was being impolite is helping noone and thus pointless and a waste of time. And as far as this thread is concerned \"Ever heard of nested QWidgets?\" was actually informative while \"Ever heard of being polite?\" was not only useless but counterproductive (unless it was intended as a troll and a try at starting a flame war which I hope it wasn't).\n\nNow to you, Troy Corbin Jr., I think you should stop writing your negative minded posts and relax. In your first post while asking for information you unnecessarily also wrote \"If not, then there's no point in my using them.\", something which would usually lead to flames, but Binner wisely ignored it. Then the next post you say thanks \"but unfortunatly it shows that no, I can not add an extra ( third ) widget at this time.\" showing that you didn't understand Binners post while unnecessarily implying that it's Binner who is wrong instead asking how exactly one can do it. You smartly concluding that since you didn't get it it's impossible to do, bleh. Then in your third post you ignore the hint completely and instead, again unnecessarily, assume he's not helping you and not being polite. This now finally lead to a little flame which funnily enough was again filled with hints. But in your forth post you again decided to ignore them all and instead, unnecessarily, making a big speech about \"KDE community considered obtuse\", \"everyone impolite while only I appologise\", personal insults like \"The world will still revolve around it's axis without your dull wit.\" and \"go play on Slashdot.\" You even said \"Of course I've heard of looking at the source.\" while obviously not having considered actually doing just that once.\n\nYou and your unwillingless to keep your own personal issues away from your posts was the problem, the whole thread here would be perfectly fine and polite if you weren't there to bark at every single point where you with your personal social background thought of them as insults."
    author: "Datschge"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-02
    body: "But it really sounds like \"Ever..., you idiot!\"."
    author: "Somebody"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-02
    body: "I'm sorry that we can't agree on this, Datschge. I won't further pollute these forums with what has become a very offtopic thread. However, if anyone is honestly interested in my reply to Datschge's post, I'll be happy to send it through email.\n\nHave a great day all! :-)"
    author: "Troy Corbin Jr."
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-03
    body: "I think the problem here is you thought binner was being impolite, while I can think of many instances where saying what he said would be nothing but polite and helpful. If he'd added a smilie you wouldn't have been confused.\n\nIt's just a case of misunderstanding the tone because it's the written word only.\n\nNow Datschge was right, you shouldn't react so aggressively. You can't tell what tone he meant, and he was being helpful to you."
    author: "MxCl"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "> *sigh* And we wonder why the KDE community is considered obtuse.\n\nI think it's probably the fact that there isn't, unfortnatly, much moderation here.. (besides for editors like navrinda, but they are probably busy).. it'd be nice to have either ip masks posted with posts, or accounts needed to post."
    author: "lit"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-02
    body: "Considering the traffic here I consider the boards here one of the most friendly one existing. Be aware that unlike on many other sites you can actually see every message posted without any \"-1\" or \"troll\" messages hidden away by default.\n\nThe removal of messages is seldom here, and I hope it can stay this way. But I think Navrinda should put a suggestion like \"stay friendly, then others will be friendly to you as well\" on every single post page. ; )"
    author: "Datschge"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "word.. http://lxr.kde.org. learn to use it, love it, and copy+paste large chunks of code from it :=======)\n\nof course, grandparent post was hardly a \"rant\""
    author: "lit"
  - subject: "Re: Custom Buttons in KTabWidget"
    date: 2003-06-01
    body: "That didn't have to be sarcastical. It could have been a simple honest question, you don't know what tone he used so you should of just assumed the best.\n\nAlso, anon is his nick name here and he is not a troll and posts quite often here. "
    author: "Alex"
  - subject: "KDE Standard utils"
    date: 2003-06-02
    body: "Will there be some standard utilities in KDE.\nI mean. I have Suse linux 8.2 and there are different\napplications for video, music etc.\nAre there any standard KDE applications??"
    author: "KDE User"
  - subject: "Re: KDE Standard utils"
    date: 2003-06-02
    body: "Noatun is the standard music/video player of KDE. However,there is Kaboodle as a light-weight alternative to just watch a film, listen to a piece of music, without having to deal with a playlist and lots of controls (although I find that an integrated volume slider is really missing in Kaboodle).\nI haven't used Suse in ages (am using Debian now) and maybe they added other players such as xmms or mplayer to the menu. Other desktop-oriented Linux distributions on the other hand just put one application per task (e.g. Lycoris).\nThe reason why there are multiple programs at disposition in Linux is due to that people have different tastes (interface-wise) and also because different players have different capabilities. So Linux offers you more than the one-size-fits-it-all approach of M$. It's all about choice. If you don't like some of the applications you find pre-installed with Suse, de-install them.\n\nTuxo"
    author: "tuxo"
  - subject: "Re: KDE Standard utils"
    date: 2003-06-03
    body: "xmms and (g)mplayer are automatically added to the kmenu through kappfinder... \n\nkde 3.2 will have a new music application: JuK.. it takes more of a itunes approach to things, but is not a itunes clone (unlike similiar apps like rythymbox for gnome)\n\nkde 4.0 will hopefully see artsd replaced.. imho, it's the weakest link in KDE.. i've had KDE in 3 systems, and in two of them (both of which were ppc based), arts didn't work. On the third, arts did work, but playing a song took 10% more CPU usage than xmms using either alsa or OSS directly (12% versus 2%), AND it did weird things with software mixing when my hardware was perfectly capable of doing it."
    author: "till"
  - subject: "Close button closes wrong tab..."
    date: 2003-06-02
    body: "Anyone else notice that the close button closes the currently viewed tab, not the tab where the button appeared?  This is really annoying."
    author: "anon"
  - subject: "Re: Close button closes wrong tab..."
    date: 2003-06-02
    body: "The almost usual answer: your CVS copy is too old."
    author: "binner"
  - subject: "Re: Close button closes wrong tab..."
    date: 2003-06-03
    body: "hmm, so compiling twice a week isn't often enough... I should get a new computer....\n"
    author: "michael"
  - subject: "Back-ported to 3.1.* ?"
    date: 2003-06-03
    body: "Hi All\n\nAnyone knows if the konqui tab improvements will be backported to the 3.1 branch ? I would be nice, but OTOH KDE devs seem to wisely have chosen to try to keep the 3.1.* releases amost strictly as stabilty (bug-fix) ones ...\n\nThanks :-)"
    author: "MandrakeUser"
  - subject: "Re: Back-ported to 3.1.* ?"
    date: 2003-06-03
    body: "I think bug fixes such as clearing the window upon tab changed will be backported. The new tab widget will not be--- KDE 3.2 will rely on Qt 3.2 eventually and the tabwidget has the most potential under Qt 3.2 (due to new stuff in QTabWidget)"
    author: "till"
  - subject: "Re: Back-ported to 3.1.* ?"
    date: 2003-06-03
    body: "Sounds like it, thank you Till !"
    author: "NewMandrakeUser"
  - subject: "Shrink tabs instead of < > buttons ?"
    date: 2003-06-03
    body: "I wondered if tabs are going to be like in mozilla, i.e they shrink when the tabbar is full instead of the two < > buttons with wich you can toggle tabs."
    author: "peter"
  - subject: "Re: Shrink tabs instead of < > buttons ?"
    date: 2003-06-03
    body: "This was implemented in HEAD during the last days too."
    author: "binner"
---
This week, <a href="http://dot.kde.org/1054309615/1054314560/1054316816/1054318401/konqui-head.png">new tab widgets</a> are in <a href="http://www.konqueror.org">Konqueror</a>, news on <a href="http://www.csh.rit.edu/~benjamin/about/phpquickgallery/dir.php?gallery=Programs/KAudioCreator/">KAudioCreator</a>, <a href="http://www.geocities.com/gigafalk/qextmdi.htm">MDI support</a> goes into KDE, and more functions and
templates are added to <a href="http://www.koffice.org/kspread">KSpread</a>.  Also,  many bug fixes have been made to KMail, Konqueror, and KWin. Read it all in the <a href="http://members.shaw.ca/dkite/may302003.html">latest KDE-CVS-Digest</a>.
<!--break-->
