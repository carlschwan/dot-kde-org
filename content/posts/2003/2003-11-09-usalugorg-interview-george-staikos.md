---
title: "usalug.org: Interview with George Staikos"
date:    2003-11-09
authors:
  - "binner"
slug:    usalugorg-interview-george-staikos
comments:
  - subject: "A window manager"
    date: 2003-11-09
    body: "Is KDE just a Window Manager?"
    author: "OI"
  - subject: "Re: A window manager"
    date: 2003-11-09
    body: "No, read the interview answers."
    author: "Anonymous"
  - subject: "Sad"
    date: 2003-11-09
    body: "It is so sad that US citizens don't seem to like KDE. Probably ximians anti-qt propaganda raised some fruits or RedHat's KDE policy. KDE is a mature alternative and the apllications are good, too. However, i ve got the impression that KOffice diminishes the KDE impression. Program corps like Kpaint. And KDE still has no good OpenOffice integration, although scripts can be used to add the KDe icon set to KDE. Perhaps SOT Office may specialize on KDe integration, I suppose this was a big market niche."
    author: "Matze"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "This is an interview published by the USA Linux Users Group.  They are quite liberal with their praise for KDE.  What's sad about that?  \n\nThe problem is more that KDE does not get enough exposure and promo in North America -- people are less aware of it and sometimes don't even know it exists.  When they are made aware of the existence of KDE (say through being installed on 200 desktops or more at University), I can very happily report that they are just as happy with the desktop as you are...\n\nTo the prankstar on interbusiness.it:  Stop it please.  Very funny.  :-) "
    author: "Navindra Umanee"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "> Program corps like Kpaint\n\nHence kdenonbeta/kolourpaint is under development"
    author: "Anon"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "> It is so sad that US citizens don't seem to like KDE\nIt is so sad that they confuse it with a Window Manager :P\n\n> RedHat's KDE policy\nWell, they're out of the picture now since they don't make free linux distros\nanymore.  Their KDE support was hopeless anyway.\n\n> KOffice diminishes the KDE impression\nWhy?  It works as long as you don't try to save to any other format than the\nnative ones.\n\n> And KDE still has no good OpenOffice integration\nKWord has working OO filters.  KOffice 1.4 will use OO formats natively.\n"
    author: "Anon"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "Redhat has a free distro called fedora in fedora.redhat.com. You may be unsupportive of the non existant kde policy of redhat but look before you leap"
    author: "rahulsundaram"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "Although fedora is a community project, nearly all maintainers/contributors are RedHat employees. I don't expect fedora to have any sorta change in KDE policy than RedHat did overnight."
    author: "anon"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "> It is so sad that US citizens don't seem to like KDE. \n\nPlenty of people here in the US like KDE. What's a shame is that more Windows users don't use KGX (kde/gnu/X Windows)"
    author: "anon"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "The control center has improved, still it is not organized very well. Improve Usability and the desktop is ours."
    author: "Heini"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "OFF-Topic remark:\n\nhttp://usability.kde.org/activity/usabilityreports/\n\nIs this a dead project, I don't see any usability reports submitted."
    author: "Heini"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "That project was there before the kde-usability mailing list was opened, where most usability discussion happens."
    author: "anon"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "http://www.datanamic.com/download/index.html\n\nWill Kexi be a mysql tool like deZign? I find it very strange that the best mysql tools are commercial programs for Windows while linux programs don't semm to sell. There is not even a port! Perhaps because nobody can keep track of distri and versions variety. Nobody wants to build x packages. KDE has to provide a unified running plattform. "
    author: "Gerd"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "Try:\n\n  http://www.fabforce.net/dbdesigner4\n  \nYou may have problems as you need libpng.so.2 which some (most?) current distros don't use. It also seems to have a few other bugs, but worth a try.\n\nIts a kylix proggie, so feels more KDE than Gnome."
    author: "Hmmm"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "Like deZign? Yeah, we plan to go with Kexi similar way. Kexi as MySQL tool? Not especially MySQL-devoted/dependent tool but really more generic in design, thus usable for more db engines. Who likes MySQL more than other engines may want to add some special plugins - this will be possible \"soon\".\n\nYes, 3rd-party software packaging on linux looks like not solved problem, especially ok high-integrated environments like KDE and Gnome. This can be a main reason that win32-related-companies won't port theirs software EVEN if they use Qt (!), like Adobe Photoshop Album."
    author: "Jaros&#322;aw Staniek"
  - subject: "Re: Sad"
    date: 2003-11-10
    body: "There is http://www.ksqlshell.de/ - german only by now, but he asks for help."
    author: "Carlo"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "GIMP 1.2x is no photoshop or paintshop replacement and is getting outdated. Gimp 2.0 will be nice but is second best and of course no KDe program. Why don't they open a penpal account or a gimp foundation. I would like to pay for a proper Linux \"paintshop\" because I need it. KPaint and similar approaches are nice toys, even tuxpaint provides you with more options.. MosfetPaint seems to be discontinued. And I also don't need 20 non-mature vector graphic programs."
    author: "Gerd"
  - subject: "Re: Sad"
    date: 2003-11-09
    body: "\"And I also don't need 20 non-mature vector graphic programs.\"\n\nThat's how it often begins. Remember how it was with cdburning programs? \n\nJ.A."
    author: "jadrian"
  - subject: "Re: Sad"
    date: 2003-11-10
    body: "there is still no cd burning program that can fully compete to windows tools - not even k3b. neither in function nor ui"
    author: "Carlo"
  - subject: "Re: Sad"
    date: 2003-11-12
    body: "I must be a weirdo.  I'm a US citizen and love KDE.  I don't\ngive Gnome the light of day (except to see how they're doing).\n\nSeriously, I think it is all because of Red Hat.  I understand\nbusinesses wanting redhat because it's reliable like a Ford F-150....\nbut we like KDE because it's great, like a Corvette.  I guess it's\nlike growing up....sure my dad liked Corvettes, but he wasn't\ngonna haul bails of hay in a Corvette for the cows...you know\nwhat I mean?  (BTW, I'm from Texas, USA)\n\nHowever, KDE has really matured and *is* reliable(above statements\nreflect general sentiments....not really mine).  I think the only\nthing holding it back is that businesses are using RedHat in the US\nand that people are scared of its youthful glow (for some reason,\npeople think something has to be old and ugly to be reliable).\n\nAs mentioned in other posts, if IBM or some big company backed\nKDE in its pure form, Linux would just take over everything."
    author: "Nonya"
  - subject: "Re: Sad"
    date: 2003-11-14
    body: "I'm a US citizen, in fact I'm the guy who interviewed George...and I love KDE!\n\n-=mUnky=-"
    author: "munky"
  - subject: "Re: Sad"
    date: 2003-11-14
    body: "> And KDE still has no good OpenOffice integration, ...\n\nOK, I not much heavy user of Office programs, but it seems that KOffice getting very close to be enough. So I guess, that I do not care much\nabout integration with OO and I have it just as an intermediary solution before I will use KOffice completely. And M$-Office formats are pretty good for interchange, aren't they? :-)\n\nBest,\n\nMatej"
    author: "Matej Cepl"
  - subject: "KOffice"
    date: 2003-11-09
    body: "I think kde has many good application like kmail (the best email client i know) or konqueror. The main problem area is koffice's stability."
    author: "fireball"
  - subject: "I hate Mandrake, Redhat,"
    date: 2003-11-09
    body: "They have made my beautiful KDE a shit by adding their crappy menus and icons...\n\nI WANT DEFAULT KDE LOOK AND FEEL, \n\nDoes \"debian\" or \"Suse\" has the default beauty of KDE, style, wallpapers, splash screen, icons, keybindings etc?\n\nI want a distro which allows default KDE looks... please suggest. thanks."
    author: "KDE user"
  - subject: "Re: I hate Mandrake, Redhat,"
    date: 2003-11-09
    body: "SuSE messes around with the styles and things as well. Debian doesn't. You might want to look into slackware or gentoo as well, which are very good distros and leave KDE the way it's supposed to be."
    author: "Traktopel"
  - subject: "Re: I hate Mandrake, Redhat,"
    date: 2003-11-14
    body: "Debian definitely doesn't munge anything -- KDE is KDE is KDE.\nThe only problem is that if you are used to speed of inovations of Suse/Mandrake/RedHat you have to use testing distribution (or external package repositories, which is what I do), but I have switched to Debian two years ago and I have never looked back for minute. Go for it.\n\nMatej"
    author: "Matej Cepl"
  - subject: "KDE's \"innovations\""
    date: 2003-11-09
    body: "If KDE is not a Window$ clone, where is it REALLY different? And please, don't tell me about the application menu that can be put on the top of the screen like Ma\u00a2O$ because it is completely irrevelent. There must be a feature in KDE that haven't been made just to create a desktop environement \"as good as the others\" for Linux.\n\nPlease, prove me wrong.\n"
    author: "interbusiness.it"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-09
    body: "Nothing is innovative, in real life, you are copying styles of other people, you use \"words\" copied from your parents, peers, newspapers, etc. if you call yourself innovative create your own language and speak with yourself. So, You are just a copy of the people you've met or read about... in this sense, you are no unique.\n\nMS is a very good example of copycat. it copies all new technologies into its OS and call its own innovation. Like terminal services (RDP) copied from citrix. \n\nKDE provides familiar GUI (Popular existing Desktop Environment like of CDE, MS, MAC os) to all UNIX. And this is innovation... ONE Easy GUI for all UNIX Flavors. KDE has (and will have) all best things of all the Desktop Environments.\n\nRemember NT stands for New Technology! why? because NT4 could be installed on different CPU architectures. MS called it New Technology, huh! when unix was running on multivendor machines, since 70s.\n\nif you want to see a real clone of MS Windows, visit http://www.xpde.com\n\n"
    author: "KDE user"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-09
    body: "how about the fact that KDE is network transparent out of the box?"
    author: "Larry"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-09
    body: "gnome is a clone of a kde is a clone of windows is a clone of mac is a clone of amiga is a clone next is a clone of parc\n\nand nothing wrong with that"
    author: "anon"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-09
    body: "Watching the screen you can tell KDE is a 'good looking desktop', like many others.. But have you ever seen the internals ?\n Do you know you can replace virtually everything in KDE? We can make a 3D reactive desktop, kicker can be repalaced with a 'personal information collector and application launcher' (or by slicker at instance).\n Have you ever seen kio? network transparency or others? try to put \"system:/\" in your konqueror's location and see what happens.\n Try to develop a KDE application and see that with 2 lines of code you can add a fully working system tray icon to your application.\nYou can't even imagine what's to come and how far is kde from others.\nIs this enough \"interbusiness.it\" ? If not, please let me tell you that this is just the beginning.\n.Ciao."
    author: "koral"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-10
    body: "<rant>\n\nThe features are there, but their availability and instructions on how to use them seems fragmented or at least not well presented.\n\nI think it also is a matter of attrackting developers as purely a seperate interest group.\n\nBack in the days when I crawled around in a snakepit, I used to code for MFC and there was/is this site: www.codeproject.com which was realy attractive to visit as a MFC developer.\n\nThere is no such attractive place for KDE APPLICATION developers to group togheter, unless it is some boring newsgroup or sloppy looking web site I am affraid. It's more a matter of stumbling accross bits and pieces of information in posts like these or newsgroups that a potential KDE developer can learn.\n\nPerhaps a 'codeproject' site for KDE developers can attrackt more KDE developers. By developers for developers so to speak?\n\n</rant>"
    author: "ac"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-10
    body: "Why not http://developer.kde.org/ as information source (which includes http://developer.kde.org/documentation/ and especially http://developer.kde.org/documentation/library/cvs-api/ ) and http://www.kdedevelopers.org/ for blogging?"
    author: "Datschge"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-10
    body: "All your examples lack the community following existing in the www.codeproject.com example I gave.  \n  \nYour examples are more or less static examples which do not separate KDE application development from KDE \"the desktop\" development.  \n  \nFor example, I create some nice framework KDE code or widget hack for my app and think others might be able to use it as well. On codeproject you could group this into a logical place in that one site. Other visitors could visit other developers KDE app codebases/widget examples etc etc.  \n  \nJust take a look at www.codeproject.com and observe it is one site whereas you mention at least four sites which are fragmented and are in my opinion not even comming close to showing the power of KDE to developers.  \n  \nAnd that KDE power is -so much greater-, better representation of that power is likely to attract more developers, because I really believe many developers out their are not fully aware of KDE's power for developers. The information is too fragmented. \n \nAlmost all stuff on codeproject is based on 'closed' source widgets, toolkits, proprietary etc. etc and yet all the examples and tutorials are wide open and have a large community following. The question then is, why couldn't KDE have something similar?"
    author: "ac"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-10
    body: "Very easy to explain this situation. See that banner on codeproject? Refresh a few times to see different banners. This site is sponsored by Microsoft. They also resell MS products. KDE is not commercial, it can't sponsor sites or offer them resale oportunities. On codeproject you can also see banners for commercial tools used for Windows programming. There are no commercial tools for KDE programming, commercial Linux tools are either generic C++ or Java tools or avoid using KDE (Kylix). \nYou see, building good sites is all about sponsorship. This is about the best you can get without strong sponsors such as MS."
    author: "Anonymous"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-10
    body: "Try http://www.kdedevelopers.net (thanks to Ian).\n"
    author: "Ravi"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-11
    body: "kdedevelopers.net is only for people working on KDE istelf, not for people writing standalone applications for KDE."
    author: "Stephen Douglas"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-12
    body: "*looks at himself and scratches his head*"
    author: "Datschge"
  - subject: "*COUGH COUGH*"
    date: 2003-11-12
    body: "\"All your examples lack the community following existing in the www.codeproject.com example I gave.\"\n\nYou must be joking... so if www.codeproject.com has such a better community following, where are the consequently superior programs and libraries (superior to KDE with its \"non-community\" \"non-following\")?"
    author: "Datschge"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-09
    body: "When compared to Windows (though not necessarily other X window managers), KDE is miles ahead in simple things like window management. Honestly, I feel crippled using Windows machines after having used KDE for years. Why? Little things: good virtual desktops, horizontal/vertical window maximizing, middle-clicking to send a window to the back, the ability to give a window focus without bringing it to the top, easy resizing (alt+right mouse allows you to resize a window by clicking anywhere on it), easy moving (alt+left mouse allows you to move a window by clicking anywhere on it), window shading with mouse hovering, and the list goes on. KDE has had most (all?) of these features for years. Windows is truly far behind when it comes to simple convenience features on the desktop. This is just window management, too; the list gets longer as you compare kicker to Windows' \"start bar\" or konqueror to IE."
    author: "Matthew Kay"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-10
    body: ">>middle-clicking to send a window to the back\n\nTHANKS FOR THAT GREAT TIP!!\n\nI cant believe I have to learn this great feature this late on a KDE forum..\n\nThat middle click think is really a time saver!"
    author: "ac"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-11
    body: "Heh, in that vein, here's a related one I discovred only a few days ago: you can alt-middle-click anywhere on a window to toggle it \"bring to top\"/\"send to bottom\". I find this particularly useful because the click event isn't actually sent to the window, so you can easily bring a window to the top that you can only see part of, without accidentally clicking on something important in the window.\n\nThere's always a little gem left somewhere in KDE :)."
    author: "Matthew Kay"
  - subject: "Re: KDE's \"innovations\""
    date: 2003-11-10
    body: "Nothing is terribly innovative these days. Consider the changes (in the UI) made between Win98 and WinXP. What's really changed? Things look different, because of Luna, but that's just visual. KDE itself isn't much older than WinXP, and consider all that has changed since then. \n\nKDE's two biggest strengths:\n\n1) Extremely powerful framework. Stuff like DCOP, KIO, KParts, and the other KDE libraries allow a desktop that's supremely well-integrated, and joyful to develop for. Neither WinXP nor OS X offer that level of integration, or that level of power.\n\n2) Extreme customizability. KDE can be molded to the user's preference. It can behave like Windows, or like OS X, or anything inbetween. You might consider a MacOS-style menu irrelevent, but its a manifestation of this overall principle. Personally, I find it invaluable. My main computer is a laptop with a 15\" LCD, and I can save a lot of space by setting up the desktop just-so. \n\nIn addition to these two points, you've got all the other features people here have mentioned."
    author: "Rayiner H."
  - subject: "OT: kmix in kde3.2_beta"
    date: 2003-11-09
    body: "What do people think about the new kmix behavior:\n\n Left button now shows/hides main window. \n Middle button shows volume control. \n \nsee also  http://bugs.kde.org/show_bug.cgi?id=54711\nThanks!"
    author: "Anonymous"
  - subject: "Re: OT: kmix in kde3.2_beta"
    date: 2003-11-09
    body: "KMix now finally follows the KDE HIG rules for systray entries, the previous behavior didn't follow it and thus was inconsistent with other systray entries, see http://developer.kde.org/documentation/standards/kde/style/basics/systray.html"
    author: "Datschge"
  - subject: "KDE and Linux on the desktop."
    date: 2003-11-10
    body: "Well, as you can probably tell, I am from the USA, and just so you know, my favorite desktop enviroment is KDE. I rarely use anything else unless my machine is too old and slow to support it. KDE is a truely wonderful thing, and it makes Linux ready for the desktop of the normal user, IMHO of course :)\n\nWhile many people don't agree with me on that issue, I have helped many new users that can use Linux very productively, mostly because of KDE. I have help many a user convert from that other os, KDE has made it much easier for those new to Linux to switch. I've also had the pleasure of introducing Linux to someone who has never ran a computer before, KDE is on his desktop, and he's having alot of fun using his computer now. \n\nMany thanks to George Staikos for his time, and to all the developers who work on KDE. Your work is appreciated, yes, even in the US :) \n\nBy the way, usalug.org has MANY users that aren't from the US. Several from the UK and Canada, and other parts of the world also.\n\nThanks again,\n\nSincerely,\n\nDave Crouse\nSite Administrator\nhttp://www.usalug.org"
    author: "crouse"
  - subject: "My 2 cents"
    date: 2003-11-10
    body: "Hm, just a thought: One reason for Gnome's popularity is that Miguel and Nat have done a good job promoting it. KDE really needs a stronger PR effort and some \"faces\". That's why I enjoy hearing of the existence of interviews likes this one. "
    author: "Eike Hein"
  - subject: "very nice"
    date: 2003-11-10
    body: "great interview, IMHO! both the questions and answers were enjoyable.. it was a quick, easy read with some great content... kudos to USALUG and George!"
    author: "Aaron J. Seigo"
---
The <a href="http://usalug.org/">USA Linux Users Group<a> features an <a href="http://www.usalug.org/index.php?view=14">interview with George Staikos</a>, restless KDE core developer and promoter. They are interested in what to expect in the upcoming release 3.2 of "the industry's leading Window Manager". Further topics include the default look and settings of KDE, what makes KDE successful, how it appeals to new users and of course the all time favorite question as to whether Linux and KDE are ready for end-users.
<!--break-->
