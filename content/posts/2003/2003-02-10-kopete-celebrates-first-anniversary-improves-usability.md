---
title: "Kopete Celebrates First Anniversary, Improves Usability"
date:    2003-02-10
authors:
  - "Dre"
slug:    kopete-celebrates-first-anniversary-improves-usability
comments:
  - subject: "basic bugs"
    date: 2003-02-10
    body: "0.5 shipped with some inexcusably fundamental errors (eg empty messages instead of msn user is typing), that sent me straight back to gaim. Here's hoping the last GTK app on my desktop is eclipse after I've tried this."
    author: "Caoilte"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "This is beta software. Note the version number. 0.5. Fairly fundamental errors are not exactly unexpected."
    author: "Chris  Howells"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "If the world were perfect, I would be in *total* agreement with you.\n\nBut it's not.  A lot of software projects in the OSS/FS/License-of-the-Week world seem to have a real problem with releasing software with a version number at or above one. \n\nExamples:\n\n\t- The stable version of GAIM is 0.59\n\t- The stable version of WindowMaker is 0.80.2\n\t- K3b, generally considered stable, is at 0.8\n\t- The last stable version of Mosfet's Liquid is 0.9.5\n\t- Qtella, stable Gnutella app, is at 0.6.1\n\n\nIn many cases you wont see a 'stable' release of an application, ever.  Fortunately, this has gotten a lot better in recent years than it used to be.  However, pointing at the version number and calling it 'pre-release' is not an ample reason to shoot down criticism for what really are pretty major bugs.\n\nTelling the person to read information on the site, wherein it prerelease status is explained, though, *is* an adequate reason. \n\t\n\n\t"
    author: "Sam"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "The version number has everything to do with the current capabilities of the software. Version 1 should be stable and have the necessary features according to the developers complete. Anything less, i.e. 0.5 can be broken and incomplete in many ways."
    author: "ac"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "Again, if this were actually true across all developers, I would be able to accept that as a blanket statement.  But, since once again this is not a perfect world, this isn't actually how things work.\n\nYou're absolutely right in that version 1 *should* include all of the functionality and features that the developers feel the application needs.  However, that's not true.   This is borne out if you take a look at most of the projects I listed, especially in the case of WindowMaker."
    author: "Sam"
  - subject: "Re: basic bugs"
    date: 2003-02-11
    body: "Version numbers are meaniless unless they are defined. Traditionally a component isn't deemed stable until 1.x release, but many software components are versioned differently. Without knowing how the software is versioned you can't make a blanket statement about it's declared stability or compatibility with other components soley on the number attached to it."
    author: "me"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "They've put some incredibly nifty features in (ie tabbing, transparency etc - i just tried it, very nice) but still haven't fixed some of the basic usability problems that make an app actually <i>usable</i>. Whatever the version number (shouldn't you be making that comment on /. ?) the basic features should be got right before the fancy stuff.\n\nFrankly I'm surprised you didn't tell me it was opensource software so I should submit a fix. I filed a bug, though, and will have to go back to gaim for the moment."
    author: "Caoilte"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "> Note the version number. 0.5.\n\nAnd that has to do with...?"
    author: "lit"
  - subject: "Re: basic bugs"
    date: 2003-02-11
    body: "In that case, I disagree. 0.5 was advertised as \"stable\", and was pretty disappointing. Another major regression was that AIM messages were printed as plain text rather than HTML, rendering kopete pretty unusable as an AIM client. Looking in the code, the function to process the HTML in AIM messages had been commented out with a note saying \"do we still need this ?\".\n\nThe problem was immediately reported, and while there really should have been an intermediate 0.5.1 bug-fixing release, there wasn't one.\n\nI also work on a fairly large KDE project, I know it's hard and we've had some releases with really stupid bugs, but Kopete 0.5 really was a disappointment, and so was its subsequent management (or lack thereof)."
    author: "Guillaume Laurent"
  - subject: "Re: basic bugs"
    date: 2003-02-14
    body: "then go get use some developers that are not on vacation all the time and hand some brain over to others (the commented code was mine and I never found out WHO goddamn commented it out) :P"
    author: "mETz"
  - subject: "Re: basic bugs"
    date: 2003-02-14
    body: "cvs annotate is your friend :-)."
    author: "Guillaume Laurent"
  - subject: "Re: basic bugs"
    date: 2003-02-14
    body: "My friend does not tell me how to use him (i.e. I have no clue about annotate) ;)"
    author: "mETz"
  - subject: "Re: basic bugs"
    date: 2003-02-14
    body: "Beyond the obvious \"RFTM\", cervisia nicely wraps it (look up the log of a file, there's an annotate button)."
    author: "Guillaume Laurent"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "0.6 though should be really bug-free and stable."
    author: "KDE User"
  - subject: "Re: basic bugs"
    date: 2003-02-11
    body: "But it isn't. It crashes every time I change between IM plugins."
    author: "Eeli Kaikkonen"
  - subject: "Re: basic bugs"
    date: 2003-02-14
    body: "Noatun has a similar problem and still there's no component being pointed out as guilty (the crash goes quite deep into kde libs and qt itself).\nAlso, why do you have to load/unload plugins all the time, isn't that something you only do once and then just go on using the application?"
    author: "mETz"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "kopete is fantastic !!!\nMultiple protocols in one GUI is more than needed...\nso everybody should congratulate.\n\nYou sound as if you haven't coded one line of GPL source yet.\nI don't like people who only know how to critizise my source.\nAnd I think kopete-developer feel the same.\nSo: \nSay a nice word first before talking about bugs...or send a patch!"
    author: "Jens Henrik"
  - subject: "Re: basic bugs"
    date: 2003-02-12
    body: "I *have* coded GPL source. Does GPL mean its better or something?  If someone's code is buggy beyond belief (which I'm not saying Kopete is...I use Gaim) then its buggy beyond belief, and the devs should be told.  If the bugs are really, really horrible, then saying \"this is crap\" seems reasonable."
    author: "whoever"
  - subject: "Re: basic bugs"
    date: 2003-02-14
    body: "> If the bugs are really, really horrible, then saying \"this is crap\" seems reasonable.\nIt all depends on the \"tone\", just being rude saying \"it sucks\" is not what I call motivation."
    author: "mETz"
  - subject: "Re: basic bugs"
    date: 2003-02-10
    body: "> inexcusably fundamental errors\n\nINEXCUSABLE??? How on earth could they be?\nSince Kopete is totally free!!\n\ngrr"
    author: "Mandrake"
  - subject: "Re: basic bugs"
    date: 2003-02-11
    body: "I would say they were inexcusable because they made kopete unusable.\n\nie, it was in theory a good IM client with excellent kde integration but all the wonderful features in the world couldn't make up for the fact that it crashed every twenty minutes.\n\nie, the bugs it contained couldn't be excused on the grounds that the featureset was rich because they made it impossible to appreciate the excellent hard work that was put into it.\n\nie, inexcusable because the developers must have known that they were running before they could walk.\n\nDon't let corporations hear you using \"free\" as an excuse for bug-ridden software. Is that the \"excuse\" you want for them not using kde?"
    author: "Caoilte"
  - subject: "Re: basic bugs"
    date: 2003-02-11
    body: "Yeah, 0.5 was complete crap. I used it for 5 minutes, until I closed the main window (which should just hide it, but keep it running, showing the docked icon) and it crashed.\n\n0.6 is much more stable. I've just switched over from Psi. It certainly has room for improvement, but it's usable enough for me."
    author: "Blue"
  - subject: "Re: basic bugs"
    date: 2003-02-11
    body: "i've been using it since yesterday, and sadly it's crashed five or six times (that's a lot less than it used to) after windows were closed.\n\n:(. it's back to gaim for me."
    author: "Caoilte"
  - subject: "Kopete improvements..."
    date: 2003-02-10
    body: "At the moment, I use gaim for my AIM needs.  I'd be more than happy to switch to a KDE client (if only for the ability to dock it so the main window is unmapped)...\n\nBasically, what I need from my client:\n*The ability to use familiar keybindings (ctrl-w to delete a word, ctrl-u to delete a line, etc).  I can't get along with a client that doesn't let me do this.  No, ctrl-home won't cut it.  Only consistency with other apps (including minicli, mind you) will.\n*Hitting enter should cause the message to be sent.  Not ctrl-enter, not clicking send, but hitting enter.\n*Signed off clients should not be listed at all.  I don't want to see them greyed out, I want them to be non-existent.\n*Adding/removing buddies should be as easy as in gaim: tabs switch between online buddies and an edit tab.  Extremely handy (I don't want to have to go through a config process, or a bunch of menus just to add someone).\n*I need to be able to specify autologin *from the commandline*.  With gaim you can do gaim --login=screenname and it will automatically log that person in.  Just having auto-login isn't enough; I don't want to be auto logged in unless I specifically say so, and I want the ability to choose which screen name gets auto logged in.\n\nThese features, at least, are keeping me with gaim.  It does everything in a simple, straight-forward, and non dumbed-down method that I like.  That's my only complaint with some KDE apps; they try too hard to be \"newbie friendly\", forgetting that some of us who use KDE actually know what we're doing."
    author: "KDE User"
  - subject: "Re: Kopete improvements..."
    date: 2003-02-10
    body: "1) Not sure how easy this would be, but global editor configuration should make this easy.\n2) Simple. In a conversation window, go to Settings->Configure Shortcuts. Send message, \"Return\" instead of the default (yeah, I think the default is stupid too, but it's easy to change).\n3) Ctrl+V in the buddylist window. Or click on the looking-glass icon. Again, stupid default, but easily changed in less than 5 seconds.\n4) Different developer mindsets. I honestly don't know what the Kopete guys are thinking with the add-a-buddy wizard, but I'd rather have something a bit more straightforward myself. Still, it's one of the few remaining useless pieces of the app.\n5) This one's slightly more difficult. Talk to Duncan or Nick about it in IRC or send an email to the mailinglists.\n\nMy personal beef with Kopete is that I'd like to be able to have multiple accounts per protocol, but I can't currently. This is annoying. That, and it crashes/immediately disconnects with sufficiently large buddylists (in excess of 110 buddies) on OSCAR. But these are small details, and I've got confidence that they'll get fixed, so I've moved from gaim (hopefully forever).\n\n-clee"
    author: "clee"
  - subject: "Re: Kopete improvements..."
    date: 2003-02-10
    body: "> (if only for the ability to dock it so the main window is unmapped)...\n\nGAIM does this. Just enable the \"dock\" plugin in the GAIM preferences and it will put an icon in the kde status dock. Clicking the dock icon toggles the buddy list visibility. And right-clicking it allows you to set options without having to map the main window. YMMV, but it works for me with GAIM 0.60 and KDE 3.1\n\n"
    author: "Max Watson"
  - subject: "Re: Kopete improvements..."
    date: 2003-02-10
    body: "> At the moment, I use gaim for my AIM needs. I'd be more than happy to switch\n> to a KDE client (if only for the ability to dock it so the main window is\n> unmapped)...\n\nYou can dock *any* application by using the ksystraycmd tool that is included in kdebase. ksystraycmd --help for more details.\n\nRich.\n\n"
    author: "Richard Moore"
  - subject: "Re: Kopete improvements..."
    date: 2003-02-10
    body: "Thanks for the tip.... I didn't know about this one.   Makes my life MUCH easier!"
    author: "Chris"
  - subject: "Re: Kopete improvements..."
    date: 2003-02-10
    body: "You're welcome. There's not much point in writing these things if people don't use them. You might also want to play with kstart (a similar tool for customising window decor) if you get bored. :-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Kopete improvements..."
    date: 2003-02-10
    body: "Right click on the Kopete icon in the system tray, select minimize. It's the exact same way for KMess as well. I think the guy is right, it's more of a KDE build in feature, so why code it into your program ?"
    author: "chillin"
  - subject: "Re: Kopete improvements..."
    date: 2003-02-10
    body: "* Kopete is a KDE application, and as such uses all KDE standard edit shortcuts as specified in KControl.\n* Again, Ctrl+Enter is the default because it is a defacto KDE standard for \"Send Message\", as used by KMail, KNode. If you want to set this to just plain enter it is as simple as Settings->Configure Shortcuts, as with all KDE applications.\n* As stated below, click the button in the toolbar. This preference is saved.\n* This is a matter of preference. Personally, I prefer the wizard.\n* This feature may be added, it seems reasonable."
    author: "Brunes"
  - subject: "Re: Kopete improvements..."
    date: 2003-08-11
    body: "I did not know how to set enter instead of control enter in KDE apps.  That is a very helpful and useful thing to know.  Now that I can use return to send messages, kopete is my default IM client.  THank you very much."
    author: "Mark"
  - subject: "Re: Kopete improvements..."
    date: 2004-06-25
    body: "Ah, when i set settings->shortcuts to change for sending a message from clt+enter to enter, it doesn't take affect.  Enter remains as new line and clt+enter remains as send message.  It ignores that setting (kopete 0.8.1)"
    author: "matt"
  - subject: "Re: Kopete improvements..."
    date: 2004-08-25
    body: "yeah for me too... even setting <enter> as the shortcut for send it still doesnt work... and have to send by <ctrl-enter>"
    author: "librano"
  - subject: "Re: Kopete improvements..."
    date: 2004-09-29
    body: "Same in 0.09.  Needs to get fixed!"
    author: "kgonzales"
  - subject: "Re: Kopete improvements..."
    date: 2004-09-29
    body: "Same in 0.90.  Needs to get fixed!"
    author: "kgonzales"
  - subject: "kdenetwork"
    date: 2003-02-10
    body: "wouldn't it be a good time for kopete to move over to kdenetwork? "
    author: "Bausi"
  - subject: "Re: kdenetwork"
    date: 2003-02-10
    body: "Or kdepim."
    author: "ac"
  - subject: "Re: kdenetwork"
    date: 2003-02-10
    body: "perhaps once it stops crashing doing fairly rudemantary things.\n\nI *really* hope it stablizes before KDE 3.2 tho."
    author: "lit"
  - subject: "Re: kdenetwork"
    date: 2003-02-10
    body: "I use the 0.6 Kopete on Mandrake with\nMSN and ICQ protocols. It never crashes.\n\nWhat are you doing with it?"
    author: "Kde User"
  - subject: "Re: kdenetwork"
    date: 2003-02-11
    body: "Look at the attached SS:\n\nI'm using it with AIM (oscar) only.. after a few hours, it keeps on using 95-99% CPU usage. No idea what's causing it, since this is hours after not doing ANYTHING with the app.. the app is still usable (but slow). I have no way to reproduce it even other than just letting it run, heh.\n\nThis is with 0.6, with KDE 3.1, and .. I had other major stablity problems with 0.5, but they seem to be fixed.\n\nI love kopete (especially the interface), but it's not there yet for me :(\n\nAlso, it should have a wizard type of thing to select what protocols the person wants to use. "
    author: "lit"
  - subject: "Re: kdenetwork"
    date: 2003-02-11
    body: ">  I'm using it with AIM (oscar) only.. after a few hours, it keeps on using \n> 95-99% CPU usage. No idea what's causing it, since this is hours after not \n> doing ANYTHING with the app.. the app is still usable (but slow). I have no \n> way to reproduce it even other than just letting it run, heh.\n \nTry disabling the \"Connection Status\" plugin. I had the same problems (with ICQ tho) when I used it."
    author: "Carsten Pfeiffer"
  - subject: "Re: kdenetwork"
    date: 2003-02-12
    body: "Thanks, but I didn't even have this plugin enabled in the first place :("
    author: "lit"
  - subject: "Re: kdenetwork"
    date: 2003-02-14
    body: "Also try disabling one of the two plugins to find out which one is guilty so we get an idea of where to search for that bug.\nThings like\n\"it hangs\"\n\"it sucks\"\n\"it crashes\"\netc. won't give us the opportunity to fix bugs."
    author: "mETz"
  - subject: "Re: kdenetwork"
    date: 2003-02-11
    body: "there are many chrashes whenn you use IRC"
    author: "vanHell"
  - subject: "Re: kdenetwork"
    date: 2003-02-10
    body: "It will be in kdenetwork for kde 3.2"
    author: "Spencer"
  - subject: "Re: kdenetwork"
    date: 2003-02-11
    body: "Yes fine, but if it will be in kdenetwork for KDE 3.2, why is it still in kdenonbeta?"
    author: "Philipp"
  - subject: "Translucent Usability"
    date: 2003-02-10
    body: "\"translucent windows\" + \"usability improvements\" = unconvincing."
    author: "Grumpy"
  - subject: "Re: Translucent Usability"
    date: 2003-02-10
    body: "Quite a lot of work went into API restructuring to allow better maintainability in future versions (and lay the groundwork for multiple accounts per protocol and KDE address book integration), but as that makes little sense to most users, I guess Dre left it out from on purpose."
    author: "Martijn Klingens"
  - subject: "Re: Translucent Usability"
    date: 2003-02-10
    body: "I have no doubts that a lot of work went into this new version of Kopete. My post merely alluded to the incongruity of those two phrases being used together."
    author: "Grumpy"
  - subject: "Re: Translucent Usability"
    date: 2003-02-10
    body: "dict:orthogonal"
    author: "Aaron J. Seigo"
  - subject: "Re: Translucent Usability"
    date: 2003-02-10
    body: "Not culturally."
    author: "Grumpy"
  - subject: "Re: Translucent Usability"
    date: 2003-02-11
    body: "ah. well, i suppose that just makes me practical and boring then. could be worse, i suppose. i could be grumpy. ;-)\n\n(spoiler: yes, that was meant in a light hearted manner, said with a devious smile and a pint raised high)"
    author: "Aaron J. Seigo"
  - subject: "Re: Translucent Usability"
    date: 2003-02-12
    body: "I thought I was the practical and boring one? Aaron, what's going on?\n\n\nI'm definitely still grumpy. But I was born that way."
    author: "Grumpy"
  - subject: "Re: Translucent Usability"
    date: 2003-02-10
    body: "Why would it be? The translucent windows are\nturned off by default.\n\nThere's only need for some little switch\nsomewhere on the \"advanced\" page."
    author: "Kde User"
  - subject: "Re: Translucent Usability"
    date: 2003-02-10
    body: "\n    Actually, an IM client (and possibly an audio player application) are about the only two applications where translucent windows (or on screen display interfaces) are actually useful.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Translucent Usability"
    date: 2004-01-18
    body: "This is simply your list so far.  I can think of a couple of other interesting uses for translucent windows, mainly in the realm of building visuals out of layered applications.  Translucent windows lets one use the desktop as a canvas.  Draw an image from a web camera.  Now layer some text over that, perhaps using a browser window.  Now drawn an animation over that.\n"
    author: "heironymouscoward"
  - subject: "Jabber progress?"
    date: 2003-02-10
    body: "I couldn't find in the release documentation whether the jabber plugin supports group chat yet? That is the only thing keeping me from using Kopete instead of PSI."
    author: "Joe"
  - subject: "Re: Jabber progress?"
    date: 2003-02-10
    body: "If you compile from source, and have libpsi installed, you should get the Jabber plugin."
    author: "Spencer"
  - subject: "Re: Jabber progress?"
    date: 2003-02-10
    body: "That doesn't answer my question though. I know it has Jabber support. I am asking specifically if it has the IRC-style Jabber multi-user group chat functionality that is in PSI. "
    author: "Joe"
  - subject: "congrats!"
    date: 2003-02-10
    body: "Nice work so far on kopete, I'm looking forward to trying out 0.6!\n\nCongratulations,\nJason"
    author: "LMCBoy"
  - subject: "Unstable"
    date: 2003-02-10
    body: "I have tried both versions 0.5 and the CVS (Early Feb) for a week each.  The interface is very attractive and so are the features.  Unfortunately I did have to switch back to gaim due to the unstable nature.  I got random crashes when I received AIM messages (Not ICQ & MSN messages) and when I log onto ICQ/AIM at certain times.\n\nAlthough thumbs up so far.  Once the bugs gets sorted out, Kopete will blow all the other Multi-Protocol IM's out of the water (I can just feel it).\n\nAn option I would really like to see in future versions of kopete is to override the html of the received message.  Say for example, you receive a message from a certain *cough* female *cough* that sends messages with huge bright purple fonts with a black background."
    author: "Aleks Ivic"
  - subject: "Re: Unstable"
    date: 2003-02-10
    body: ">An option I would really like to see in future versions of kopete is to override the html of the >received message. Say for example, you receive a message from a certain *cough* female >*cough* that sends messages with huge bright purple fonts with a black background.\n\nYou can already do this. The message formatting is just done by HTML, all you have to do is remove the special formatting tags for user sent background color and font color under the Chat Appearance config. We do need better docs for these options though."
    author: "Brunes"
  - subject: "My opinin"
    date: 2003-02-10
    body: "Why exists so many projects with a same function in KDE ?\nsuch as: kinkatta, kyim, kmess, kopete, konverse, kwinpopup ............. ,psi ..\nWhy not work only in the kopete and make better ?"
    author: "anonymous .."
  - subject: "Re: My opinin"
    date: 2003-02-10
    body: "Why are there so many messages in this board?\nWhy not work only in one and make it better? :)"
    author: "Vajsravana"
  - subject: "Re: My opinin"
    date: 2003-02-11
    body: "ROTFL!"
    author: "jd"
  - subject: "Re: My opinin"
    date: 2003-02-10
    body: "All of those other programs existed before Kopete.  Even so, consider that the other programs might perform their native function better than an all-in-one app?  Btw, Konverse is dead.  Also, there is some collaboration:  for instance, Kopete and Psi share the same Jabber code.\n\nAlso, some are not KDE-only:  Psi and Kinkatta both run on other platforms.\n\nDon't forget LightHawk:  (site seems to be down)\nhttp://freekde.org/neil/lighthawk/\n\nTo each his own, I guess."
    author: "Justin"
  - subject: "Re: My opinin"
    date: 2003-02-17
    body: "and there are discussions between kyim and kopete about the yahoo plugin that a lot of people seem to be missing... so hopefully both projects will benefit of that work.\nbut i'll always prefer kyim to kopete (or any other all-in-one), because yahoo has a lot of specificities (sp?) that i don't, and probably can't find in all-in-one apps (email warning, profile, specific smileys, IMvironments, Buzzer, status notification, members lookup, audio, video, whatever, etc.) Certainly everything is not implemented is kyim as well, but some of these \"extra-features\" are not, and cannot be found in all-in-one apps, because they are not possible to \"meta-describe\".\noh, and Kinkatta and KYIM also share common problems: very few developers that have a very rich personal life (to the detriment of their KDE life ;-)), so i'm not very sure that their help would be very interesting for Kopete (both projects are going veeery slow for a while)."
    author: "loopkin"
  - subject: "Re: My opinin"
    date: 2003-02-11
    body: "Maybe people like to have a *choice*? Why use Linux and KDE? Let's all use windows and make it better!"
    author: "Andr\u00e9"
  - subject: "Theme from screenshots"
    date: 2003-02-10
    body: "Does anyone know which KDE theme is being used on the 0.6 screenshots?  Looks kinda QNXish but I don't think it's identical to the QNX themes currently on kde-look.  Any ideas?"
    author: "balerion"
  - subject: "Re: Theme from screenshots"
    date: 2003-02-10
    body: "widgets: keramik\nwindow deco: web"
    author: "Aaron J. Seigo"
  - subject: "Re: Theme from screenshots"
    date: 2003-02-10
    body: "http://kopete.kde.org/img/screenshots/0.6/chatwindow1.png\nhttp://kopete.kde.org/img/screenshots/0.6/icq-prefs.png\n\nThe 0.6 screenshots are definately not keramic.  I'm pretty sure the window decoration is .NET, but I'm not sure about the widget theme..."
    author: "balerion"
  - subject: "Re: Theme from screenshots"
    date: 2003-02-10
    body: "Looks like QNiX to me."
    author: "fault"
  - subject: "Re: Theme from screenshots"
    date: 2003-02-11
    body: "erf... yes, helps if i scroll down far enough ;-) ... it does look like the qnx styles avail on kde-look, but the tabs look different from either one.. i'm stupmed =)\n\nthe window deco in those shots is quartz."
    author: "Aaron J. Seigo"
  - subject: "Re: Theme from screenshots"
    date: 2003-02-14
    body: "that's qinx pre0.5 (I'm visually debugging it with the author, that's why I use it) and quartz window deco. The color-scheme is a tweaked keramik-emerald."
    author: "mETz"
  - subject: "Yahoo plugin please!"
    date: 2003-02-10
    body: "Hi guys,\n\nPlease provide a Yahoo plugin fast!!\n\nThanks\nSarang"
    author: "Sarang"
  - subject: "Re: Yahoo plugin please!"
    date: 2003-02-11
    body: "I'm also visiting the site pretty often hoping i would see \"yahoo\" word on the page, but ... until then i'm stick with gaim ... :|\n"
    author: "Costin"
  - subject: "Re: Yahoo plugin please!"
    date: 2003-02-12
    body: "Yes, plz. \n\nI'm locked in gaim till then :)\n\nAnyway the app's great!"
    author: "Juan Dom\u00ednguez"
  - subject: "Re: Yahoo plugin please!"
    date: 2003-02-14
    body: "Why not use the jabber client and use a yahoo transport?\n\nor use Psi for that matter?"
    author: "Andrew"
  - subject: "Re: Yahoo plugin please!"
    date: 2003-02-28
    body: "I am looking forward to the yahoo plugin too.\nI used to use gaim and then moved to everybuddy as gaim didnot support file transfers. i am pretty happy with everybuddy....but i like kopete better. I will move to this as soon as the yahoo module comes out.\n\nI havenot been able to connnect to the jabber as yet with kopete as yet. Wondering if there is a bug."
    author: "V Leher"
  - subject: "Re: Yahoo plugin please!"
    date: 2003-04-29
    body: "Has it been create or not? I am still using Gaim, but I would like to move over to kopete. Please let me know if anyone has managed to get Yahoo working on Kopete"
    author: "LinuxLover"
  - subject: "Re: Yahoo plugin please!"
    date: 2005-11-04
    body: "hey! please can u provide me with the yahoo plug in\nthanks.\nsarah"
    author: "sarah haile"
  - subject: "Re: Yahoo plugin please!"
    date: 2005-11-04
    body: "The current kopete version has a yahoo plugin.  (I haven't tested it though, as I'm not using Yahoo IM...)\n\n"
    author: "cm"
  - subject: "Re: Yahoo plugin please!"
    date: 2006-06-08
    body: "is there any plugings for yahoo messenger , like in msn messenger please post here , and developers please hurry yahoo needs something fresh ect..... example something like stuff plug '\n"
    author: "mr p[hillipssssssss"
  - subject: "plugin for invisible status"
    date: 2007-01-04
    body: "let me shae a cool peice a software ( Y! plugin ) with you.. \nit tells you who is offine and who is just pretending to be so..\ngr8 s/w to know who avoids you :(\nhttp://www.tricks4fun.com/2006/11/29/yahoo-messenger-invisible-status/"
    author: "david__cc"
  - subject: "Still not usable, back to SIM."
    date: 2003-02-10
    body: "Well I tried the new version but it still\ncrashes on exit. I use only the ICQ plug-in.\nOnly one plug-in, starting and closing \nshould work without any crash. And it crashes\non all 5 machines I tested it on (all SuSE 8.1,\nsome immediately after a fresh install).\nThis is an application which usually isn't\nclosed, so this wouldn't be a problem. The\nnext \"bug\" (or is it a feature?), however,\nmakes all this really annoying:\nIf the Kopete window is open and you click\nclose, I would expect that Kopete minimizes\nto the panel. Instead it closed completely -\nand crashes. The only way to minimize Kopete\nto the Panel seems to be to click on the \nPanel icon again - not very user friendly.\nSo, it's the small things that keep me from\nusing this app. I appreciate all the work though\nand I'm patiently using SIM until Kopete works\nproperly ;-)\n\n"
    author: "Jan"
  - subject: "Re: Still not usable, back to SIM."
    date: 2003-02-11
    body: "I actually haven't tried kopete at all, but the same discussion about close came up about gnomeicu. The conclusion? Having the \"X\" minimize the application to the systemtray is inconsistant with all other applications. Minimize could maybe do it, but that's also somewhat inconsistant. So why does people excpect it to minimize to the systemtray if close is pressed? Simple answer: The so called UI from AOL callad ICQ (AIM problably does the same thing, I have never tried it though...). The ICQ-UI have more flawes than, than, than well I honestly don't know ;-).\n\nMy point: keep the close button for *really* closing apps.\n\n\nRegards,\n\nJ\u00f6rgen"
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: Still not usable, back to SIM."
    date: 2003-02-12
    body: "Er, I'm sorry. You press the \"close\" button and expect the program to minimise?\n\nSorry, I know that windows programs do this.. but try this: Speak loudly and listen to your words \"I am pressing the CLOSE button to make the program MINIMISE\"\n\nDoesn't anything strike you odd? Maybe the program is working fine, it's just not used right? ;)"
    author: "Googol"
  - subject: "Re: Still not usable, back to SIM."
    date: 2003-02-14
    body: "When you hit the close button it should close (not minimise) the *window* - not the whole application in an application with more than whan UI element.  If the application has a docked item on the tray this should be uneffected by the opening or clsoing of windows.\n"
    author: "Adrian Bool"
  - subject: "Re: Still not usable, back to SIM."
    date: 2003-02-17
    body: "When I click close, I expect to close the window. I don't agree the idea of docking, but most people expect the docking (my sister and brother for example).\n\nHow can we be free of windows programs?\nEverybody wants windows' behaviour in Unix/Linux, but noboby says \"hey, windows is wrong, my kde does it well\".\n\nluisdlr."
    author: "luisdlr"
  - subject: "Buddy Icons"
    date: 2003-02-10
    body: "As far as I know, there is no way to enable a buddy icon, or see other users icons with AIM. I know this may seem silly to some, but just having a little icon gives the person you are talking to some personality. This is one feature that GAIM has that I wish Kopete would adopt. Besides this good work guys! This is much much improved over the last release."
    author: "Resist"
  - subject: "The best improvement to Kopete would be..."
    date: 2003-02-10
    body: "... to change its name.\n\nKopete seems to have a brilliant future, but its name sounds too much like Muppet with a \"K\" instead of the \"M\" :-)\n\nIf someone change the name Kopete, please (oh please!) don't give it another name beginning with a \"K\".  If all Windows apps were beginning with a \"W\", we would have WOffice, WMessenger, WNotepad, maybe W.Bush, etc...\n"
    author: "Ned Flanders"
  - subject: "Re: The best improvement to Kopete would be..."
    date: 2003-02-11
    body: "> If someone change the name Kopete, please (oh please!) don't give it another name beginning with a \"K\". If all Windows apps were beginning with a \"W\", we would have WOffice, WMessenger, WNotepad, maybe W.Bush, etc...\n \nEver heard of Microsoft Office, Microsoft Internet Explorer, Microsoft Windows? k is just a branding thing, as is most company's name, etc...\n\nGNOME does it (a bit less than KDE, although still quite a lot), Xfree86 does (even more than KDE).. everyone does it :>\n\nAlthough I agree, it's more creative to have unique names :)"
    author: "fault"
  - subject: "Re: The best improvement to Kopete would be..."
    date: 2003-02-11
    body: "Are you the same fault who disliked the name Kroupware? (see http://dot.kde.org/1032748629/ ).\n\nIf you are, I think you can understand why I don't like the Kopete name...:-) But I must admit that Kopete is way better than Kroupware (remembers the Foresight suggestion?)"
    author: "Ned Flanders"
  - subject: "KDE's  lack of elegancy"
    date: 2003-02-12
    body: "Oh - I wish that KDE would drop the K naming convention. The problem is that the marjority of the decision takers are engeneers with poor taste, so I don't see that happening any time soon -or ever. The developer centric nature of KDE means that it lacks a fundamental \"elegancy\" as Bart Decrem said. It will probably never change, so I have lost some interest in KDE myself. \n\nMy conclusion is that such changes will probably must take place at the distribution level. Problem is that haven't been that good either - but I'm a big fan of Xandros, so I rest my hope on that distribution. There are signs that the others will follow.\n\nTo summarize KDE needs IMO to do four things to gain a certain elegancy:\n\n1. Drop the K naming convention\n\n2. Clean and redesign Kicker\n\n3. Drop the cogs-and-wheels style-elements and replace with something that speaks to the values of non-engeneers\n\n4. General cleanup - less options, less elements etc.\n\n\n\n\n\n\n "
    author: "will"
  - subject: "Re: The best improvement to Kopete would be..."
    date: 2003-02-11
    body: "If you were chilean, you could inderstand it's name\nright duncan? ;)\n"
    author: "Matias Fernandez"
  - subject: "Re: The best improvement to Kopete would be..."
    date: 2003-02-11
    body: "No problemo!"
    author: "Ned Flanders"
  - subject: "Buggy Mandrake 9 rpm"
    date: 2003-02-11
    body: "\nHi,\n\nthe version inside the mandrake 9 rpm states:\nVersion 0.58 (cvs 20030131)\n\nand it crashes quite alot, especially when trying to move contacts\nto the top-level directory.\n\nAnd, I really like the fact that the sounds come in ogg format, but\nkde3.1 seems being unable to play them... except when using ogg123\nas external player\n\nI stay with 0.5 for now"
    author: "ac"
  - subject: "Re: Buggy Mandrake 9 rpm"
    date: 2003-02-13
    body: "To be honest, it's not Kopete's fault. Kopete has been valgrind'ed and tested for over a year now, and 0.6 is no exception. The only place I could blame such problems for is in kdelibs. There are a couple known critical bugs in 3.0.x and one critical bug in 3.1. HEAD is fine, although that doesn't help the users much.\n\nPlease don't whine about how KDE is unstable. KDE is just now becoming mature and its API is not being re-written every release. Many important bugs are being fixed every release, and it's important that users keep up-to-date with KDE when also keeping up-to-date with Kopete, at least until kdelibs becomes a little less buggy and Kopete stabilizes API-wise."
    author: "Nick Betcher"
---
The <a href="http://kopete.kde.org/">Kopete Project</a> celebrated its
first anniversary yesterday by
<a href="http://kopete.kde.org/index.php?page=newsstory&news=Kopete_releases_version_0.6">announcing</a>
the release of Kopete 0.6.  Kopete is KDE's all-purpose, modular and
extensible chat client, which currently supports the MSN Messenger, ICQ,
AIM OSCAR, Jabber and IRC protocols.  A sampling of the great new features
includes sophisticated (HTML) text rendering, signing / encrypting chats
and sending SMS messages to mobile phones, as well as chat tabs, translucent
windows and web presence notification.  In addition, the Kopete developers
made a number of usability improvements based on
<a href="http://usability.kde.org/activity/completed/">usability studies</a>
conducted by the <a href="http://usability.kde.org/">KDE Usability Project</a>.
Congratulations to all involved!
<!--break-->
