---
title: "KDE 3.1: eWeek Review, More RPMs"
date:    2003-02-11
authors:
  - "Dre"
slug:    kde-31-eweek-review-more-rpms
comments:
  - subject: "Growing attention of media"
    date: 2003-02-11
    body: "The last year's article about KDE 3.0 was only one page by same author, this has three. Also there is a pleased separate review of Konstruct at <a href=\"http://www.eweek.com/article2/0,3959,879509,00.asp\">http://www.eweek.com/article2/0,3959,879509,00.asp</a>."
    author: "Anonymous"
  - subject: "OT: Knode"
    date: 2003-02-11
    body: "Yes KDE is developing fast. \nI just wonder what is happening with Knode. Just compare how much Knode and Kmail improved from KDE3.0 to KDE3.1.\n\n"
    author: "Me"
  - subject: "Re: OT: Knode"
    date: 2003-02-11
    body: "KNode just is developed slower. http://developer.kde.org/development-versions/kde-3.2-features.html shows planned features and it seems yenc read support was forgotten. Not that I would think that a offline mode and better killscoring of indiviual users wouldn't be a nice addition."
    author: "Anonymous"
  - subject: "Re: OT: Knode"
    date: 2003-02-11
    body: "\"KNode just is developed slower.\"\nWell that's exactly what I was saying. \n\n\n\n"
    author: "Me"
  - subject: "Re: OT: Knode"
    date: 2003-02-11
    body: "Sorry, I thought you doubted that it's developed anymore at all."
    author: "Anonymous"
  - subject: "Re: OT: Knode"
    date: 2003-02-11
    body: "Actually yENC support is in cvs and works smoothly"
    author: "radix"
  - subject: "Re: OT: Knode"
    date: 2003-02-13
    body: "Then put it on the 3.2 feature page."
    author: "N.N."
  - subject: "Re: OT: Knode"
    date: 2003-02-11
    body: "Dam, the only gnome app I still use is pan.  Hmmm... one of these days I am going to get off my lazy ass and help out."
    author: "theorz"
  - subject: "Re: OT: Knode"
    date: 2003-02-12
    body: "pan is no Gnome application anymore with version 0.12."
    author: "Anonymous"
  - subject: "Debian ?"
    date: 2003-02-11
    body: "There is no complete debian package for sid ? \n- ftp.kde.org just offer them for woody \n- lot of missing in official (kmail .. ) \n\n??? "
    author: "DebianOwner"
  - subject: "Re: Debian ?"
    date: 2003-02-11
    body: "deb http://people.debian.org/~ccheney/kde-3.1.0-1 ./\ndeb http://people.debian.org/~bab/kde3.1 ./\n\nUse these addresses until the Debian FTP masters have allowed them all to go into sid."
    author: "Anonymous"
  - subject: "Re: Debian ?"
    date: 2003-02-11
    body: "Plus \n\ndeb http://people.debian.org/~schepler/  ./kde3.1/\n\nfor the all-important kdegames :)  I think all we're missing now is kdevelop.\n\nFor those who don't know, new packages have to be processed manually by the FTP admins so they can take a little time to enter the archive.  Once they are in, updates should be fairly rapid. kdebase and kdelibs have already seen a couple of updates.\n\n-- Jaldhar"
    author: "jaldhar"
  - subject: "Re: Debian ?"
    date: 2003-02-11
    body: "excellent!  I now have noatun.  But none of these three sites have kdenetwork.  Is that up anywhere?"
    author: "another debian user"
  - subject: "Re: Debian ?"
    date: 2003-02-12
    body: "deb http://people.debian.org/~ccheney/kde-other ./"
    author: "Gunter Ohrner"
  - subject: "Problems here..."
    date: 2003-02-13
    body: "I installed kdenetwork from that source and kmail won't start. (error msg: \"kmail: relocation error: kmail: undefined symbol: _ZN8DwString4nposE\") Is that a shared library problem?? Does anyone know how to fix it? Baring that, how can uninstall all of the packages that were installed when I typed \"apt-get install kdenetwork\"? Removing kdenetwork itself doesn't seem to remove those packages.\n\nThanks"
    author: "Jim Chabot"
  - subject: "Re: Problems here..."
    date: 2003-02-13
    body: "> kmail won't start\n\nI didn't try these packages yet, so I cannot help there, sorry.\n\nHowever:\n\n> how can uninstall all of the packages that were installed when I typed\n> \"apt-get install kdenetwork\"\n\nYou definitely want to take a look at aptitude. apt-get itself cannot do that, only with the help of deb-foster and/or deborphan. However, aptitude can, much more easily than with apt-get, deborphan and deb-foster and it's very handy in various other, different ways.\n\nFor me, aptitude definitely is a \"must have\" to manage a Debian installation. It features a command line interface and an ncurses based, menu controlled UI with mouse support. Both are equally powerful, you can choose one depending on your preferences. I personally like the ncurses interface very much.\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: Problems here..."
    date: 2003-02-14
    body: "\"kmail: undefined symbol: _ZN8DwString4nposE\")\"\n\nPurge libmimelib1 and then reinstall from unstable.\n"
    author: "anonymous"
  - subject: "Re: Problems here..."
    date: 2003-02-15
    body: "Thanks, Gunter and anonymous. I found a new set of debs that work before I read this, but I went ahead and purged libmimelib1 anyway to avoid potential future problems. As for aptitude, it is quite nice. Thanks for the tip."
    author: "Jim Chabot"
  - subject: "Re: Problems here..."
    date: 2003-05-21
    body: "10x... That help me move 1 more function from Windows to Linux (-:"
    author: "Lior Kaplan"
  - subject: "Re: Problems here..."
    date: 2003-10-03
    body: "I've tried to install KDE 3.1.4 on Sid. Apt-get had removed large number of packages and stuck. I decided to return to 3.1.3, but after I did kmail didn't work (\"kmail: relocation error: kmail: undefined symbol\"). Thanks to your admice it's back to the living."
    author: "Kuba"
  - subject: "Re: Problems here..."
    date: 2004-02-06
    body: "Add one more note of thanks for the tip on this one - during the process of fixing my horribly confused testing/unstable machine I got this error - would have had no way of knowing how to fix it. "
    author: "jvarner"
  - subject: "Re: Problems here..."
    date: 2004-02-17
    body: "Worked for me too.  I was stuck without my trusty kmail for ten days!\n\nNow the question is: how the hell would I make out what to do?  I mean with libmimelib1, without reading this thread?  I've been trying many things...  including downgrade a lot of stuff, without success.  What was the clue that should have pointed me at libmimelib1, and that so obviously I've been missing?\n\nShow a man how to fish...\n"
    author: "andrew"
  - subject: "RH 8.0 rpms"
    date: 2003-02-11
    body: "Yes, finally RH8.0 rpms, good work from the KDE for RedHat Linux guys.\n\nThe only problem for me is that one certain member from my household is playing CounterStrike, and I don't want to ruin his game :(\n\nWell I guess I can wait for a couple of hours.....\n\n"
    author: "Johan Veenstra"
  - subject: "Re: RH 8.0 rpms"
    date: 2003-02-11
    body: "Somehow I doubt that CounterStrike depends on KDE."
    author: "Anonymous"
  - subject: "Re: RH 8.0 rpms"
    date: 2003-02-11
    body: "I think it depends on the internet connection though. =P"
    author: "Datschge"
  - subject: "Official RedHat 8.0 rpms?"
    date: 2003-02-11
    body: "I clicked on the \"mirrors\" link.  In the RedHat 8 directory there was a README saying that official RPM's would arrive on Feb. 15.  Anyone who can confirm this?  "
    author: "Rimmer"
  - subject: "Re: Official RedHat 8.0 rpms?"
    date: 2003-02-14
    body: "Hi, any reply to this ?\n"
    author: "Bicatu"
  - subject: "Re: Official RedHat 8.0 rpms?"
    date: 2003-02-16
    body: "Yea, where the f..* are they ? Now it's the 16."
    author: "Martin"
  - subject: "Re: RH 8.0 rpms freetype2 support?"
    date: 2003-02-18
    body: "Before starting a great(for a 56k modem) download, are the qt compiled with freetype2(the RH hacked freetype I mean) support(like one article I've found on kde-look)?\n\n "
    author: "ra1n"
  - subject: "ldap support"
    date: 2003-02-11
    body: "I was trying to use the 3.1-adressbooks' ldap-support and failed. I entered the server name, port and base dn.\n\nEntering something in the incremental search field did not work. In the help-file, they talk about a 'magnifying-glass-over-a-book'-icon in the toolbar, which I do not have.\n\nYesterday, I was told it's because I don't have the sql-io-slave installed, but i thought sql and ldap are quite unrelated?!\n\nSo, did anyone get that to work? If yes, how? Also, I have only around 200 entries in the directory, can I have my address-book automatically filled with these entries? I don't want to have to search for them every time.\n\nthanks,\n   ben"
    author: "ben"
  - subject: "Re: ldap support"
    date: 2003-02-12
    body: "I tried this earlier today and had some success.  After configuring kaddressbook to use one or more ldap servers, you need to click one of the toolbar icons.  For me, I believe it was the right most icon.  I'd be more specific, but my test machine is at work.  If you don't have this icon, as you've said, you should try configuring your toolbar and adding the icon.  The incremental search does not search the ldap servers.  I find this feature very cool, but does anbody know if it's possible to get KMail to see the LDAP contacts?  Currently, I have to add the LDAP contacts to my local address book for them to show up in KMail."
    author: "Matt"
  - subject: "Re: ldap support"
    date: 2003-02-12
    body: "Is it possible to get the old way to work of kadressbook without ldap ?\nI don't need ldap for my contact :( I am the only user in my box and it is stupid to waste time learning ldap, configuruing it,... just to store thousand of contact."
    author: "Shift"
  - subject: "Re: ldap support"
    date: 2003-02-12
    body: "kaddressbook doesn't require ldap. it defaults to a local text file with options for things like LDAP or a binary file (dunno if that last one made it into 3.1 or not off-hand though)"
    author: "Aaron J. Seigo"
  - subject: "Re: ldap support"
    date: 2003-08-01
    body: "I've the same problem... There isn't a 'magnifying-glass-over-a-book'...\n\nthanks.\n\nAntenore."
    author: "Antenore"
  - subject: "Too much GUI config support?"
    date: 2003-02-11
    body: "From the article summary:\n    (+) Tabbed browsing in Konqueror; handy desktop sharing feature; interface enhancements.\n    (-) Multitude of configuration options can prove confusing.\n\n\nStrange how only a short while ago reviewers complained that Linux GUI config tools had too little coverage...\n"
    author: "Andy Marchewka"
  - subject: "Re: Too much GUI config support?"
    date: 2003-02-11
    body: "I don't think the criticism was for the level of support, but rather for the way it's organized.  I think he's right on the ball with that criticism too.  Why are Window Behavior and Window Decorations not in the same submenu?  Why are Style, Theme Manager, and Window Decorations separate items (they all sound like the same thing)?  What is the Desktop submenu for anyway -- I thought KDE *was* the desktop.  Why is there a Behavior panel and a Window Behavior panel?\n\nI'm mostly playing devil's advocate here -- I've learned to navigate the control center after years of experience.  For a newbie, though, I can't imagine it makes any sense at all.  Heck, it doesn't make any sense at all to *me*, I just know where things are now.  I think it might have improved slightly since 3.0 but it's still a godawful mess.\n\nAlso, maybe it's OT but I don't see the resolution control panel that was mentioned in the review.  Any hints as to where I can find it?"
    author: "n8gray"
  - subject: "Re: Too much GUI config support?"
    date: 2003-02-11
    body: "And may I add, \n- Has anyone ever used:\n(right click on desktop)->Create New->Html File\n\n- (Right Click Folder)->Open Terminal Here. \n\n- (Right Click Konqueror Background)->Edit File Type?\n\nMe\n"
    author: "Me"
  - subject: "Re: Too much GUI config support?"
    date: 2003-02-12
    body: "Right Click -> Edit File Type? \n  Yes, after updating to 3.1 for some reason some of my file associations got lost so that some files e.g. the trash were opened with kuickshow - that's when I used that option.\n"
    author: "Anonymous"
  - subject: "Re: Too much GUI config support?"
    date: 2003-02-13
    body: "No, I said right click on konqueror background! The context menu!\nThat alows you to change the type of the directory you're at... now have you ever used *that*!?\n\nSee what I mean, not clicking a file and chosing change type,clicking in the background of konqueror and choosing change type.\n\nMe\n\n\n"
    author: "Me"
  - subject: "Re: Too much GUI config support?"
    date: 2003-02-12
    body: "i've used all three. the Crew New subdir doesn't get in the way IMHO, but the other two belong elsewhere (namely in an Add Ons submenu and in the file properties dialog respectively)"
    author: "Aaron J. Seigo"
  - subject: "MODERATE PARENT UP!!"
    date: 2003-02-12
    body: ".. oops wrong forum ... anyway yes.  Think about how to simplify simplify simplify KDE.  The KControl needs some serious love and making over from a UI and HID specialist.\n\nAnd yest it's nice to have nice file picker widget and all (compared to Gnome - ugh!) but, uhh, folks - KDE's filepicker is OUT OF CONTROL. Thje filepicker itself is turning into a miniature application.  I think it good to pay attention to MacOS/NeXT etc. less crap = faster and less bugs. \n\nThink asthetically too ...."
    author: "NONInstaller-of-KDE"
  - subject: "Re: MODERATE PARENT UP!!"
    date: 2003-02-12
    body: "> less crap = faster and less bugs\nwhere are the bugs in the KDE file picker?\nI don't know of any and I love the dialog really: it's beautiful und funcional. And also fast - the windows file picker is worse."
    author: "Peter"
  - subject: "Re: MODERATE PARENT UP!!"
    date: 2003-02-13
    body: "No! Wrong way IMHO! The fine thing in KDE is that it's _not_ oversimplified. If you want a \"simple\" OS/GUI choose Windows, MacOS or Unix/twm or fvwm2 maybe.\n\nI can't see what's wrong with KDE's filepicker. Yes, any file picker _is_ (and should be considered) a miniature application. The KDE folks are the first to do it right since Peter Norton... (yes, I know mc ;). They give me the opportunity to do things more than one way, and being a Perl geek I consider this to be a good thing.\n\nSame goes with the KDE control center. I can't hear those complaints \"oh boy, that's too much gadgets here, I really can't find the one I am looking for...\" (ok, we'll add it for you ;). Thinking that you can reduce the \"searching\" by removing gadgets while maintaining configurability is thinking too simple... (pun intended ;).\n\nKDE means Unix, Unix means power, power needs control, control needs a center. Simple isn't it? ;)\n\nAestethics is something one can't discuss about, so I leave it at saying that KDE is the desktop environment with the best functionality : sexiness ratio I know of (and thrust me, I know many). I also like Gnome, and WindowMaker for example, but both of them spend much on looks while not reaching the usability of KDE for me. They are (and traditionally were) better when it comes to themeing and such. But that's not on the top of my wishlist anyway...\n\nThe good news is, there _are_ simpler (in your sense of simplicity) things out there. But for myself, I want my KDE to stay as \"gadgety\" as it is.\n\nCheers,\nFlexx"
    author: "Alexander Wessel"
  - subject: "Re: MODERATE PARENT UP!!"
    date: 2003-02-17
    body: "This is interesting to say the least. I use both KDE and Gnome. I would have thought KDE did much more/(cared more)  about the 'look' than Gnome. Not to say Gnome is bad. I like it better than KDE in terms of the look.(Hate  Windows). I lke its functionality though. I totally agree that the situation is not good. Its too difficult to figure out what to configure to do what. Here is a proposal for you people who do the developing. I liek the side bar in the control center, but I do not think it should have sub menus. Just the categories like, 'Desktop Look', 'Sound server', Kernel Tuning' and so on. The other submenus should be tabs across the top of the Control Center window. No more submenus after that. Maybe pop up dialogs. If it can't fit into that scheme then something wrong.\n\nDo aggregate some of these options.\n\nDo not have 3.1 yet though, using Redhat. Will get down to getting it soon."
    author: "Maynard Kuona"
  - subject: "Re: Too much GUI config support?"
    date: 2003-02-12
    body: "Cramming too much into each panel would be way too confusing.  KDE (and probably *most* things open source) offers a lot of flexibility, which means a good deal of detail that can be configured under each category.  And I don't think the community would have it any other way -- I certainly like my apps to look and behave \"my way\".  So each config category has to be fairly specific.  I'm really happy that KDE takes care of so much of it.\n\nI use M$ at work, so I certainly haven't seen a better navigation model to make suggestions from.  [Does anybody know where the \"Console\" applet from NT's control panel is under Win2K?  It's amazing that a screen buffer is not allocated to each window by default.]\n\nIf you've got suggestions for improving the Config navigation, I'm sure the folks at usability.kde.org would love to get into a dialogue.\n\nPersonally, I'm really happy with the KDE Control Centre's \"Search\" function.  I use that whenever I don't already know which panel holds an option that I want, and it's never let me down.  Very powerful and innovative, in my opinion.\n"
    author: "Andy Marchewka"
  - subject: "Re: Too much GUI config support?"
    date: 2003-02-16
    body: "how about a simple mouse over \"tool tip\" to give\na terse 2-4 work ( or maybe even a sentenc) description\nof what the apps in the menu are."
    author: "kannister"
  - subject: "mandrake"
    date: 2003-02-11
    body: "has anyone tried the test rpms for mandrake 9.0 ?"
    author: "Andreas"
  - subject: "Re: mandrake"
    date: 2003-02-11
    body: "Yes they don't work."
    author: "KDE User"
  - subject: "Re: mandrake"
    date: 2003-02-11
    body: "Well, it seems to differ somewhat. I know that some people have had problems, but I have three installations that work. Two desktops that are more or less rock steady, and one laptop with a few problems. \nSo, while not perfect, they are definitely worth a try."
    author: "G\u00f6ran Jartin"
  - subject: "Re: mandrake"
    date: 2003-02-11
    body: "I'm using them right now, and I'd say they \"mostly\" work, with one glaring exception.  For some reason, KMail doesn't seem to do mail checking except on startup.  Interval checking doesn't do anything, nor does checking mailboxes manually.  Go figure."
    author: "Kyle Haight"
  - subject: "Re: mandrake"
    date: 2003-02-11
    body: "I mean. You can't install them on Mandrake 9.0\nThere are dependency problems."
    author: "KDE User"
  - subject: "Re: mandrake"
    date: 2003-02-12
    body: "You're hopefully not using the Cooker-RPMs? They won't work,and Mandrake advises not to use them on 9.0 because you can destroy your whole installation... they have their testing version of kde3.1 for Mandrake 9.0, but at the moment, its only available to club members. About a week ago, I read at their club pages that it would be released to the public in about 2 or 3 weeks or so. wait and see..."
    author: "wiseguy"
  - subject: "Re: mandrake"
    date: 2003-02-12
    body: "What XFree86 version should work with Mandrake 9.0 KDE3.1 rpms? Is XFree 4.2.x that comes with the Mandrake 9.0 without updates works with KDE3.1?"
    author: "NS"
  - subject: "Re: mandrake"
    date: 2003-02-12
    body: "The MDK 9.1 beta (3) are for a system very different from the ones for 9.0. Do _not_ use them in a production system (do not play with probabilities...). \nGlibc is != and you have to upgrade lots of _experimental_ RPMs to satisfy the dependencies .\n\nI am using texstar RPMs and so far so good. Nice work! \nUse them instead or just become a Mandrake Club Member.\n"
    author: "simul"
  - subject: "Re: mandrake"
    date: 2003-02-13
    body: "I just installed texstar RPMs on my mdk 9.0, konqueror works as browser, but the file manager segfaults at start: any idea why I have this problem?"
    author: "mart"
  - subject: "Re: mandrake"
    date: 2003-02-13
    body: "update: i had do switch off al tumbnails preview and \"folder icon refletting content\" in order to get the file manager working, but it still crashes when i chose \"open terminal here\" from the context menu of a folder.\nIs it a problem of that packages? Had anyone encountred this problem?"
    author: "mart"
  - subject: "Re: mandrake"
    date: 2003-02-12
    body: "9.0 are available at mandrakeclub. (give a few \u0080\u0080\u0080 for getting more rpms and to see mandrake next year.\nIf you don't want / can't, don't use cooker's or 9.1beta' rpms, but *texstar*. they work greatly."
    author: "Mooby"
  - subject: "Re: mandrake"
    date: 2003-02-12
    body: "Has anybody seen RPMS for KDevelop/Cervisia for Mandrake 9.0 ?"
    author: "Alex"
  - subject: "Re: mandrake"
    date: 2003-02-16
    body: "mandrake kde3.1 for community?  NO,NO,NO so go to Tex's KDE's they work (even without art-devel)\n\nFor all major versions there is a kde3.1 (at tex's even athlon ones!!!!)\n\n\nEurope for a war free world!"
    author: "ruud koendering"
  - subject: "Re: mandrake"
    date: 2003-02-17
    body: "Yes, I have tried Texstar rpms. They work... but,  I did not like the default configuration, I had to force kdebase3 becasue of krootwarning, I could not launch konqueror, maybe I wrong but I did not see Xft2 in Texstar rpms.\n\nThen... I tried garmone builds scripts and kde 3.1 works great. Try garmone if you have decent machine, if you have an old one... you should not use kde 3.1.\n\nluisdlr."
    author: "luisdlr"
  - subject: "Mandrake 8.2"
    date: 2003-02-13
    body: "Can I use Mandrake 9.0 RPM for Mandrake 8.2?\n\nThanks."
    author: "Lozzi"
  - subject: "Redhat kde 3.1 rpms"
    date: 2003-02-14
    body: "I can only see KDE 3.0.5a rpms for Redhat where are the 3.1 rpms?"
    author: "pieter philipse"
  - subject: "Re: Redhat kde 3.1 rpms"
    date: 2003-02-14
    body: "You can get redhat rpms from rawhide\n\n"
    author: "Nick Jones"
  - subject: "Re: Redhat kde 3.1 rpms"
    date: 2003-02-16
    body: "Thanks a lot!\n\nI found the rpms here:\n\nhttp://mirrors.sunsite.dk/redhat/rawhide/i386/RedHat/RPMS/"
    author: "Pieter Philipse"
  - subject: "Re: Redhat kde 3.1 rpms"
    date: 2003-02-18
    body: "I don't think those will install on an RH8 system unless you do a rebuild of the SRPMS..."
    author: "Brian Wright"
  - subject: "ASP Linux 7.3 rpms on RedHat 7.2/7.3"
    date: 2003-02-21
    body: "Works, but I recommend to rebuild from ASP Linux source rpms. In my case at least arts from binary rpm had some problems."
    author: "checat"
---
<a href="http://www.eweek.com/">eWeek</a>'s
<a href="mailto:jason_brooks@ziffdavis.com">Jason Brooks</a> has
<a href="http://www.eweek.com/article2/0,3959,879450,00.asp">tested and
reviewed</a> some
<a href="http://promo.kde.org/3.1/feature_guide.php">new features</a> of
<a href="http://www.kde.org/announcements/announce-3.1.html">KDE 3.1</a>,
which he calls
&quot;<em>Linux's best hope for becoming a viable desktop
contender</em>&quot;.  In the time since the KDE 3.1 release more binary
packages have also become available, please check the
<a href="http://download.kde.org/">mirrors</a>.  We have also been informed
that KDE 3.1 rpms for Linux Mandrake 9.0 are available for
<a href="http://www.linux-mandrake.com/en/club/">Mandrake Club</a> members;
others might check Texstar's
<a href="ftp://ftp.ibiblio.org/pub/Linux/distributions/contrib/texstar/linux/distributions/mandrake/9.0/KDE-3.1/">test
packages</a>.  For Red Hat Linux,
KDE 3.1 rpms are available
courtesy of the <a href="http://kde-redhat.sourceforge.net/">KDE for RedHat
Linux</a> project for <a href="ftp://apt.unl.edu/apt/7.3/RPMS.kde3-test/">RH 7.3</a> and <a href="ftp://apt.unl.edu/apt/8.0/RPMS/">RH 8.0</a>; alternatively the
<a href="http://download.kde.org/stable/3.1/ASPLinux/7.3/">ASP Linux 7.3</a>
rpms are said also to work for Red Hat 7.3.

<strong>Update on Wednesday 12/Feb/2003, @01:39</strong>:  <a href="mailto:G.Ohrner@post.rwth-aachen.de">Gunter Ohrner</a> has written
in with the news that official Debian SID (unstable) packages are now available
for KDE 3.1.
<!--break-->
