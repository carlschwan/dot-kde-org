---
title: "KDE-CVS-Digest for August 8, 2003"
date:    2003-08-09
authors:
  - "dkite"
slug:    kde-cvs-digest-august-8-2003
comments:
  - subject: "Thanks for the digest"
    date: 2003-08-09
    body: "It's very detailed, even more so than usual! There's some great stuff too in Juk Kontact etc."
    author: "Alex"
  - subject: "Nitpick on CSS"
    date: 2003-08-09
    body: "Does it bother anyone else that there is CSS rule body{margin: 0px 0px 0px 0px;}?\n\nIMHO even the default (on Mozilla) 8px margin for body would look more easy for the eye.\n\nOther than that little thing the digest is very much appreciated."
    author: "Tar"
  - subject: "Re: Nitpick on CSS"
    date: 2003-08-09
    body: "CSS bothers me in general.  HTML 3.x rules!"
    author: "ac"
  - subject: "Re: Nitpick on CSS"
    date: 2003-08-09
    body: "<flame>HTML 3.x is for wussies ;]</flame>\n\nActually the problem is that there is a typo in cvsdig.css on line 96\n td.fill (\nshould be:\n td.fill {\n\nDerek, you're not using MSIE to preview the digest now are you? ;P *kidding*"
    author: "Tar"
  - subject: "Re: Nitpick on CSS"
    date: 2003-08-09
    body: "yeah!"
    author: "David Walser"
  - subject: "Re: Nitpick on CSS"
    date: 2003-08-10
    body: "What is the problem exactly? I fixed the typo in the css.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Nitpick on CSS"
    date: 2003-08-10
    body: "This little typo was the root of all evil, now everything is shown as it should, thanks :)"
    author: "Tar"
  - subject: "Knode :-/"
    date: 2003-08-09
    body: "Am I the only one who is disapointed with Knodes development.\nI am of course very grateful to all that have worked in this application, and I do think it is quite good, but just compare it's developement with kmails - compare for instance the evolution of both apps, since 3.0.x to 3.1.x! Knode is the same, it was ported, bugfixed and not much happened besides that, Kmail really evolved.\n\nI understand that there are much less developers in Knode, and there might not be so much interest in that app (which is a shame), but if that is so, why not try to use more Kmail stuff in Knode? Development would benifit and Look and Feel with be more consistent between both apps which would be a good thing (it happens so many times to select a piece of text and \"follow up to ng\" and then recall that is a kmail feature).\n\nSnx"
    author: "Snx"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "I think you answered your question yourself. The numbers of KMail users and developers is higher by an order of magnitude. "
    author: "Tim Jansen"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "What question? I didn't ask *why* the development is slow did I? I'd really like to see some comments on what I wrote.\n\nSnx"
    author: "Snx"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "If you care so much about KNode, why don't you help?\n\nMy personal opinion of KNode is that it's good enough. As part of the Kontact GUI, it's likely to be better. That work is going to push it certainly. It will also make me read more news likely. Separate programs for mail and news are a pain in that field. I loved the DOS program XP back then :-)\n\nI have had it that I had to remove the configuration, because something changed into tabs and I couldn't get to the old layout. Very strange.\n\nA unified GUI is likely when Kontact is release. But at this point, we have yet to see a first release of Kontact to the wide community. So it's probably going to take a while.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "I recompiled kdepim and KNode is strangely NOT in Kontact"
    author: "kontakt"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "You have to manually compile the knode subdir in kdepim/kontact/plugins. I haven't yet included it in the default compilation as I'm still working on some issues which should hopefully be resolved today. After that you have to enable the plugin itself in the kontact properties."
    author: "Zack Rusin"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "I'm not sure what you're talking about.\n1) We are gradually exporting general classes from KMail to either libkdepim or libkdenetwork. If you click \"reply by email\" in KNode you'll notice that the address selection widget in both KNode and KMail is the same. The on-the-fly spellchecking classes are also the same. I am working on a common composer for both...\n2) We will merge both applications at some point, but don't hold your breath.\n3) Saying that KNode should have some X functionality of KMail is almost like saying that it should have some Y functionality that can be found in Evolution. The code bases for both are dramatically different. When a class involving any mime manipulation from KMail is to be used in KNode it has to be completely rewritten - KNode uses libkmime, KMail still mimelib."
    author: "Zack Rusin"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "\"I'm not sure what you're talking about.\"\nI'm not sure what you're talking about when you say you don't know what I'm talking about... honest...\n\n\"1) We are gradually exporting general classes from KMail to either libkdepim or libkdenetwork. If you click \"reply by email\" in KNode you'll notice that the address selection widget in both KNode and KMail is the same. The on-the-fly spellchecking classes are also the same. I am working on a common composer for both...\"\nGreat!\n\n\"2) We will merge both applications at some point, but don't hold your breath.\"\nOk. \n\n\"3) Saying that KNode should have some X functionality of KMail is almost like saying that it should have some Y functionality that can be found in Evolution. The code bases for both are dramatically different. When a class involving any mime manipulation from KMail is to be used in KNode it has to be completely rewritten - KNode uses libkmime, KMail still mimelib.\"\n\nI know it is that way now. I know they started as separate apps so each one did things their own way. Now one of them is evolving faster while the other is falling behind, so IMO it's the right time to stop duplicating efforts and take advantage of what has been and will be done in Kmail. \n\nAnd don't take me wrong, I do like knode and appreciate the effort of those who work on it.\nIt's just bad to have two apps with so many common points behaving so differently, and seeing how much better knode would be if it used some of kmails stuff...\n\nFrom what I've understood from your answers in 1. you're taking the appropriate steps to make kmail code easily available to other apps, and they are behing used in knode as you export them. If that's it I'm glad to hear about it! And thanks!\n\nSnx"
    author: "Snx"
  - subject: "Re: Knode :-/"
    date: 2003-08-09
    body: "We know all that. We know exactly what has to be done, we're lacking developers who would like to do it. The problem is not that it never occurred to us that the two duplicate most of each other code, the problem is to make sure it doesn't happen. If you can't write code, start reporting problems to bugs.kde.org and in a more specific manner than \"merge KMail with KNode since KMail is evolving faster\" since that actually helps."
    author: "Zack Rusin"
  - subject: "Re: Knode :-/"
    date: 2003-08-10
    body: "Once again Zack, thanks for your answers, they were enlightening. \nI'd like to say though, that I really don't understand some attitudes towards my post.\nI've re-read it again, and it seems a very simple, straightforward, and respectful post. \n\nI asked if it was only my impression that knode was falling behind (I could be wrong, some could not agree, could be something going on that I was missing), and I shared that thought about the merge kmail stuff on it to know what you all thought about it... It was not even a \"You should do this\" kind of post, just a \"Why not do this?\" one. Seemed like a good idea to me and I wanted to know the opinion of more experienced people. Just curious that's all.\n\nI cannot see the relation between what I've done and bug reports. Seriously. Should I ask what I did at bugs.kde.org? Should I not ask this kind of stuff at all? Why?\nJust for the record I do report bugs/whishes, I query them and try to reproduce them, I try to clean old already fixed bugs, and vote for annoying ones. I know that that helps and my post doesn't, so what? I didn't expect it too. Is there really any problem with it? \n\nOh well, maybe it was me who just woke up on the wrong side of the bed.\nKeep up the good work!\n\nSnx"
    author: "Snx"
  - subject: "Re: Knode :-/"
    date: 2003-08-10
    body: "I just have 1 wish for knode..\n\nmultiple-part attachment decoding (yeah, with all those pics, music, binaries...)\n\nI love knode, but when it comes to multiple-part attachment, I have to go somewhere else - which is a pitty...\n\nHetz"
    author: "Hetz Ben Hamo"
  - subject: "3.2"
    date: 2003-08-10
    body: "Wow, I'm really impressed by all the development going on in kde and I'm really looking forward to running kde 3.2!\n\nEspecially since I saw the screenshot at mosfet.org today!"
    author: "Ernst Persson"
  - subject: "Re: 3.2"
    date: 2003-08-10
    body: "will not be out there soon, i bet that it will not be ready until 01/01/04"
    author: "A"
  - subject: "Re: 3.2"
    date: 2003-08-11
    body: "speaking from a developer's perspective, Jan 1 2004 feels pretty soon ;-)"
    author: "Aaron J. Seigo"
  - subject: "KMail threading"
    date: 2003-08-11
    body: "Is it possible to sort threads accorting to the latest mail in thread?\nSomething like mailman mail archive: If someone writes a new mail for an older thread this thread gets the latest in the archive.\n\nBye\n\n Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: KMail threading"
    date: 2003-08-11
    body: "See http://bugs.kde.org/show_bug.cgi?id=32400"
    author: "Datschge"
  - subject: "Re: KMail threading"
    date: 2003-08-12
    body: "votes+=20 ;-)"
    author: "Thorsten Schnebeck"
---
In <a href="http://members.shaw.ca/dkite/aug82003.html">this week's Digest</a>: The <A href="http://kopete.kde.org/">Kopete</A> developers release a new version of the messaging client.
<A href="http://kmail.kde.org/">KMail</A> message threading is improved.
<A href="http://knode.sourceforge.net/">KNode</A>, a news reader, is integrated into <A href="http://www.kontact.org/">Kontact</A>.
<A href="http://korganizer.kde.org/">KOrganizer</A> printing gets improved.
<A href="http://www.koffice.org/">KOffice</A> uses the new version of <A href="http://sourceforge.net/projects/wvware/">wvWare</A> for MS Word import.
<A href="http://www.slackorama.net/cgi-bin/content.pl?juk">JuK</A> playlists are improved. Plus many bugfixes.
<!--break-->
