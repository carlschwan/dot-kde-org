---
title: "Konqi on Windows XP?"
date:    2003-07-09
authors:
  - "kpfeifle"
slug:    konqi-windows-xp
comments:
  - subject: "Windows ?"
    date: 2003-07-09
    body: ".\nWhat Mr. Gates thinks about this ?? 8-D\n\nThis could be a great experience. I think this is the only way to crack your box and see the infamous \"blue screen\" when you are playing whit KDE.\n."
    author: "Brazilian Boy"
  - subject: "Bit of a misleading title"
    date: 2003-07-09
    body: "Running Konq from XP is nice, but the \"on\" word in the title is a bit misleading.. when a person skims the headers he/she could think this was a port or hack instead of a remote application server. This won't exactly help anyone to run KDE applications on XP with a dual boot machie. :)\n\nThat said, cool initiative. I hope they win the contest. Does Nomachine support dialback? That's one of the most important features my dad's company requires for RAS (MS Word compability being the other in their field of work, unfortunately).\n"
    author: "Rob Kaper"
  - subject: "Re: Bit of an misleading title"
    date: 2003-07-09
    body: "> the \"on\" word in the title is a bit misleading..\n\n...but the question mark remained (despite of the editing by Nav which changed\na bit of the meaning... )"
    author: "Kurt Pfeifle"
  - subject: "Re: Bit of an misleading title"
    date: 2003-07-09
    body: "Hey, how do you know it was me? I'm not the only active editor on this site... :-)\n\nActually it was in this case, and I added the \"?\" where there was none at all before.  \n\nSorry if I changed the meaning of anything, I was trying to make it clearer and concise and I'm guessing that in the process of removing ambiguity perhaps I introduced an error?\n"
    author: "Navindra Umanee"
  - subject: "Marvelous Quanta Plus"
    date: 2003-07-09
    body: "Wow! Now we're marvelous! I don't know how many people can relate... I just have my head buried pushing along thinking what I'd like Quanta+ to be and then I see it mentioned so often. Every time I still say to myself \"Hey! That's my program! We've been noticed.\" I guess application wise I'd have to say we are really becoming a standout, but maybe the realization hasn't quite caught up to me.\n\nI'd like to say we look \"mahvelous\" but I noted a hideous typo and the top image is still squeezed and overlaps in this shot even at that screen size... Grrr! It never ends. And BTW seeing Quanta on XP is utterly weird for me as I run Linux only.\n\nNo Machine looks like an incredible accomplishment. Then again so is KDE and XP users could use the upgrade. ;) Thanks Kurt for mentioning Quanta+... even though I did have to experience the shock of seeing it run on XP. :-O"
    author: "Eric Laffoon"
  - subject: "Commercial X-Server"
    date: 2003-07-09
    body: "So this is a commercial pricy X-Server with free clients and compressed transmission (like compressed and encrypted SSH tunnel)? Why was this posted here!? \n\nAs mentioned the story title is misleading and the only relation to KDE misses time zone information."
    author: "Anonymous"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: "i think the relevancy is due to the fact that they are using bits of KDE code (e.g. for their customized window manager thing) and apparently lean towards running KDE sessions themselves.\n\nas for why their software is interesting: it uses remarkably little bandwidth providing for high quality sessions even over the public Internet, it has management tools (including on the client side), supports several different OSes on the client-side with minimal hassle to set it up, supports RDP and X11 and provides security benefits such as no XDM, no non-NX logins, etc... since their licensing is per server rather than per user, their product is also quite cheap compared to other offerings in this same market.\n\nthis is indeed a very interesting product and one that could help adoption of KDE in corporate settings."
    author: "Aaron J. Seigo"
  - subject: "They're using Kwin"
    date: 2003-07-12
    body: "That's why! =)\n\nAnd, yes it is awesome. Also, its not so pricey if you get the personal server and hey you can view your desktop everywhre even ona  ps2!"
    author: "Ale"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: "I find it interesting, and the price is pretty attractive for business use.\n"
    author: "Simon"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: "Did you all see they provide the source code under GPL licence? Seems like this technology can be integrated in the standard XFree86 distribution! Wow!"
    author: "Dik Takken"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: "> Seems like this technology can be integrated in the standard XFree86 distribution!\n \nYes it can. It is all GPL. It is up to XFree86.\n\nAlso, KDE has been invited to write its own frontend to the NX server and\nclient technology (as have GNOME and LTSP)."
    author: "Kurt Pfeifle"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: "Not if the source really is GPL. GPL'd stuff can't go into XFree86 mainstream due to license incompatibilites. A fork is possible however."
    author: "Rich"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: "All the core compression libraries developed by NoMachine are GPL.\nNoMachine doesnt use different and improved libraries for their commercial\noffering (unlike CrossOver). \n\nKDE has been invited to develop their own client frontend as well as the an\nNX-based server, with the help of NoMachine.  The same invitation has been\ngoing out to GNOME and LTSP."
    author: "Kurt Pfeifle"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: ">>NoMachine doesnt use different and improved libraries for their commercial offering (unlike CrossOver). <<\n\nCurrently they do. The commercial version is at 1.2.2 (with many improvements, according to the release notes), the free version is 1.2.1. \n \n"
    author: "Anonymous"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-09
    body: "(Which does not mean that it is bad or something, but they obviously also have a commercial interest)"
    author: "Anonymous"
  - subject: "Re: Commercial X-Server"
    date: 2003-07-11
    body: "Version 1.2.2 has been released as source code too. There was a mishap which\nprevented the correct version to be displayed on the website. Actually the link\nnamed 1.2.1 pointed in fact to the download version 1.2.2. This is fixed now.\n\nThanks for noticing."
    author: "Kurt Pfeifle"
  - subject: "Shameless..."
    date: 2003-07-09
    body: "waste of resources making this work, if you ask me..."
    author: "Anonymous George"
  - subject: "umm"
    date: 2003-07-09
    body: "why is this news.  I've been running konqi on xp for a while now just using cgywin.  Its not that hard.  Just modify the x startup with an option -noroot or something like that, then `konqueror&`"
    author: "ciasa"
  - subject: "Waste of time"
    date: 2003-07-09
    body: "People should run KDE on top of XP if at all. Doing it remote is no news at all.\n\nYou can btw. just use cywin from Redhat on your XP and run KDE locally or use the X-Server to run it remote. Free and better mayhaps.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Waste of time"
    date: 2003-07-09
    body: "> Doing it remote is no news at all.\n\nNo it isnt, you are right. The news is the excellent compression technology.\nThe news is that it runs even across a modem. The news is that we are \nrunning 100 KDE sessions in parallel on one server.\n\n\n \n"
    author: "Kurt Pfeifle"
  - subject: "what's this??"
    date: 2003-07-09
    body: "What a ridicolous post! Running remote apps through X11 is nothing new, it was there before Linux and even before KDE. What NoMachine/NX does isn't described at all.\n\nThere was some talk about NX technology on the XFree lists a while ago, which was quite interesting. At the time I thought it was some proxy technology. Sounds like a hard business idea since X11 is being phased out since it is very picky on network latency as compared to RFB and other simple stuff (Sun used something similar on their Rays). Their technology may be good if they have cured this problem, but how many will really use it?\n\nNow it sounds like NX is some commercial X server which is an even worse idea. There are already XFree on Windows. The market for proprietary ones is tougher than ever. And remember what's a server in an X11 network -- that's right, the terminal! That really kills thin computing with expensive X servers.\n\nI would hope NoMachine turns out to be good guys and helps the X people design a much better protocol for X12 and making it fully multimedia capable. Would this be hoping too much?\n\nIt'd be great if someone from NoMachine could tell their side of the story here ..."
    author: "jb"
  - subject: "Re: what's this??"
    date: 2003-07-09
    body: "> What NoMachine/NX does isn't described at all.\n\nIt is not described in detail. \n\nIt does compression, it does caching, it does encryption, it does differential\ntransfers and it uses a proxy on both sides (client and server)\n\n> I would hope NoMachine turns out to be good guys and helps the X people design a much better protocol for X12 and making it fully multimedia capable. Would this be hoping too much?\n \nNo, the hope is valid. NoMachine have donated all their core library code under\nthe GPL. (They *could* have used a different license...) KDE and GNOME are\ninvited to use the libraries to develop their own, competing (and Free)\n alternative."
    author: "Kurt Pfeifle"
  - subject: "Re: what's this??"
    date: 2003-07-09
    body: "To be honest I'm terribly pleased about this. It appears that the XServer for Windows is basically based on the XFree86/Cygwin sources [based solely on the presence of cygwin1.dll] but takes up a lot less space than a minimal Cygwin install.\n\nAt my university the public workstations (Novell/Win2k based) only have a crapola XServer (Exceed) and having this small Xserver that fits in my homespace is excellent. The Xserver itself is totally compatible with plain-old X over SSH.\n\nCombined with PuTTY (or even some hacking of the client software) it provides an excellent, small, replacement for my previous Remote Access solution."
    author: "Rich"
  - subject: "Re: what's this??"
    date: 2003-07-09
    body: "Are you sure that the source of the server has been released by NM? Cannot find it (and they are not required to, it's not GPL)."
    author: "Anonymous"
  - subject: "Great :)"
    date: 2003-07-09
    body: "This is really great :o I tried the NoMachine TestDrive server: great performance on a slow connection. I think I'm going to set up a trial personal server at home, this is way and way much better than vnc.\nI would really love it if krfb (http://www.tjansen.de/krfb/) would get NX support. From what I understand, at least the client support is possible with the GPLed NX libs. And what about server support? Are those libs also GPLed?\nGreetings, Niek."
    author: "Niek"
  - subject: "Re: Great :)"
    date: 2003-07-09
    body: "- Desktop Sharing Server support for a protocol that is not bitmap-based would be a huge amount of work and require large changes in the X11 server. \n- AFAICS the sources for the NX server (which does not share the screen but creates new X11 sessions BTW) have not been released, only the client.\n\nMy current plan for the client is to add support for plain X11 over ssh first, and later for NX's nxproxy compression and their improved Xnest variant. This construct will not be able to connect to the NX servers directly (which I don't really care about, since there is only a proprietary implementation), but it will be able to connect to every system that has the NX server installed.\nBut plans can change, especially since I am not done with looking through the NX code and  the documentation is pretty.. well, non-existent.\n"
    author: "Tim Jansen"
  - subject: "NX is great but they aren't the first..."
    date: 2003-07-09
    body: "When I read the article I remembered that I once read about a similar solution for running X11 compressed over a small bandwith connection.\n\nSo I searched and found two (completely free and older) projects:\n\nThe first DXPC (Differential X Protocol Compressor) seems to be more similar to NX (or NX to DXPC ;) ?):\n\nhttp://www.vigor.nu/dxpc/\n\nThe second one LBX-Proxy is a part of XFree86:\n\nhttp://ftp.xfree86.org/pub/X.Org/pub/R6.6/lbxproxy/\n\nA small comparison between both (and an explanation of LBX-Proxy) can be found at the end of an old HOWTO (last updated 1997) I found at The Linux Documentation Project:\n\nhttp://www.tldp.org/HOWTO/mini/LBX.html\n\nSo I think a side by side comparison (compression rates etc.) between all three would be quite interesting.\n\nOf course NX seems to go much beyond the to other projects since it also can compress VNC, integrating sound etc.\n\nPerhapes it would be the best to take the free available compression libraries from NX combining them with one of the projects above and make a free NX-compatible solution.\n\nAnd perhapes integrating this solution in Krfb would be easier and more attractive since it is a completely free solution then."
    author: "Daniel Arnold"
  - subject: "Re: NX is great but they aren't the first..."
    date: 2003-07-09
    body: "NX's origin is DXPC, it seems to be based on the mlview-dxpc variant. The old mlview site also forwards to www.nomachine.org now.\n\nLBX is not really comparable, NX does much more.\n\n"
    author: "Tim Jansen"
  - subject: "Re: NX is great but they aren't the first..."
    date: 2003-07-10
    body: "I searched quite a while the net to find a copy of mlview-dxpc since almost all links refer to the now forwarded (to nomachine) mlview-dxpc page.\nThe last version I could find was v0.159-MS1 (from march of 2001):\n\nhttp://freshmeat.net/projects/mlview-dxpc\n\nDirect download link:\n\nhttp://freshmeat.net/redir/mlview-dxpc/13579/url_tgz/mlview-dxpc-v0.159-MS1.tar.gz\n\nA project also concerning with streaming sound over dxpc can be found at the end of:\n\nhttp://www.edu.uni-klu.ac.at/~mkropfbe/xray.html\n\nBy the way does anybody know a good ftp (or Archie) search engine in the rank of Google)? I miss the days of AskSina. :-("
    author: "Daniel Arnold"
  - subject: "Re: NX is great but they aren't the first..."
    date: 2003-07-10
    body: "www.filewatcher.org ?"
    author: "Anonymous"
  - subject: "Re: NX is great but they aren't the first..."
    date: 2003-07-10
    body: "The above mentioned URL http://www.edu.uni-klu.ac.at/~mkropfbe/xray.html provides much more as I thought.\n\nIt seems to be a further project called X-Ray (now also comercial) with the same goals as NX.\n\nIn his master thesis ( http://www.edu.uni-klu.ac.at/~mkropfbe/papers/X-Ray_thesis.ps.gz ) you can get a detailed analysis of the current solutions (VNC, etc) and a detailed description of his X11-based solution and much more (like ideas and solutions to sound and video forwarding). I think this paper can be much valuable for building up a free NX(-like) solution.\n\nHe also forked Xnest and called this new programm Xray (which he released with sources to ne net according to his paper) and compressed the stream over mlview-dxpc.\n\nUnluckily he has removed the program xray from the net (but says on his page that you can write him a mail if you are interested) and I couldn't find the sources or binaries anywhere. :-("
    author: "Daniel Arnold"
  - subject: "Re: NX is great but they aren't the first..."
    date: 2003-07-10
    body: "The difference between Xray and NX seems to be that Xray has persistent sessions. So when you disconnect from Xray the session does not end automatically, but you can reconnect later. This is also possible with, but not with NX. The NX guys seem to work on it though."
    author: "Tim Jansen"
  - subject: "Re: NX is great but they aren't the first..."
    date: 2003-07-11
    body: "mlview was indeed the predecessor of NX. NX has the same lead programmer as mlview...."
    author: "Kurt Pfeifle"
  - subject: "open source"
    date: 2003-07-09
    body: "only parts of the client where released open source.\n( and the client was already free as in beer )\n\nif i would have to guess then this is a tactical move.\nmake your ( already free ) client open source and hope it gets into as many distro's as possible and then...\n\ngain popularity.. and money !!!\n\nbut hey... that's just my conspiricy theory."
    author: "Mark Hannessen"
  - subject: "Re: open source"
    date: 2003-07-09
    body: "The most important parts, the nxcomp library and nxagent, have been released. What they did not release is their servers and clients (there is a compatible command line client now though). This is a somewhat strange situation, since this almost forces free software developers to create an incompatible implementation based on their technology.\n\nAFAICS I can not write a krdc backend for NX without some changes in the server (because I need to embed the remote window). While at the same time my motivation for writing a client for their protocol is pretty low, since I can't use it anyway. And their protocol is also pretty bad for systems without NX which may be understandable (they want to sell servers, they don't care for systems without NX) but also makes me work on an alternative solution."
    author: "Tim Jansen"
  - subject: "Re: open source"
    date: 2003-07-10
    body: "This post by an NX employee makes an OSS based NX-ifying progress of any X server without buying their complete solution sound very easy:\nhttp://www.xfree86.org/pipermail/forum/2003-March/000667.html"
    author: "Datschge"
  - subject: "Re: open source"
    date: 2003-07-10
    body: "Creating a new solution that uses their compression technology is easy. But then you can not use their server (which is also responsible for things like coordinating users, managing samba and so on).\n\n"
    author: "Tim Jansen"
  - subject: "Re: open source"
    date: 2003-07-10
    body: "Quote by Gian two post above the one I posted before:\n\"> What other stuff have you developed to be included in NX that is not under GPL?\n\nThe top level software. Everything you can do with NX Client and NX Server can be done by hand, using OSS components. We count on users being lazy.\"\n\nI see now what you mean. Nevertheless would it be possible to start with a comparatively little subset of the features offered by NX? Since Krfb as of now doesn't include many of NX's features anyway just ignoring them as for now wouldn't be a problem for current Krfb users while they still can make use of the main advantage of NX, vastly improved screen performance over slow connections."
    author: "Datschge"
  - subject: "Re: open source"
    date: 2003-07-10
    body: "Not krfb (the server), because that will only work with bitmap-oriented protocols like VNC. But it is possible to add X11 with NX compression as additional backend to krdc (the client). krdc will then create a new session when you connect."
    author: "Tim Jansen"
  - subject: "Re: open source"
    date: 2003-07-11
    body: "> But then you can not use their server\n\nHuh? What do you mean to express by this?\n\n\"Use\" their server by accessing it from a Free client should be possible.\nWrite a Free server which is accessible by a Free client as well as the NX client\nshould also be possible.\n\nThey say that there might be further developments, and that the version\n2.0 might break the backward compatibility to 1.x.x. protocol and compression, \nbut they are intending to also document and release it to.... I have no reason to\nnot trust them on this...."
    author: "Kurt Pfeifle"
  - subject: "Re: open source"
    date: 2003-07-11
    body: "It is possible to write a client that talks to their server, but, as you said, then I would have to implement a free server. Which would be a lot of work and also a lot of reverse engineering, since there doesn't seem to be a clearly defined protocol. At least for unix-systems there are simpler solutions, and I also have a few issues with their architecture (from what I understand after staring at their code for a few hours, correct me if I am wrong):\n\n- they currently seem to create a special user called 'nx' on the ssh server. If you login with that user (no password, uses only a DSA key that's part of nxclient, so everybody knows it) via ssh you enter a special shell that allows you to do the real login. This requires a lot of trust in sshd and the nx server, much more than with a direct authentication of the user via ssh. Everybody can log in on your computer and access the NX shell. And the shell probably runs as root or has root-like privileges, as it can login other users.\n\n because they use their own authentication mechanism, plain text password or \nMD5 over encrypted ssh connections, it is not possible to use other ssh authentication mechanisms like Kerberos. Plain text passwords and MD5 hashes require that you fully trust the server you log in or use a separate password for each computer. This is quite bad for many remote administration use cases when you want to connect to computers that you don't fully trust, like many desktop machines and especially notebooks\n\n- it only works when you have the NX server installed on the system. A system that would use a plain ssh login could use fallback options that work even on systems without NX\n\n\nThey obviously have reasons to do it this way: the server should also work on non-Unix systems like Windows.\n\n\nThe alternative architecture that I tend to implement works a little bit like fish. Login via ssh, install a perl/shell script on the server and start it. The script would examine the environment, check which DEs (KDE, maybe Gnome) are available, whether NX libraries are installed and so on. It sends this information via STDIN to the client, which may let the user specify options, \nand after that the script starts nxagent or Xnest with the selected DE. Beside backward compatibility, security and better authentication this has the advantage that it is more realistic for KDE 3.2.\n"
    author: "Tim Jansen"
  - subject: "Re: open source"
    date: 2003-07-12
    body: "> Everybody can log in on your computer and access\n> the NX shell. And the shell probably runs as root or\n> has root-like privileges, as it can login other users.\n\nnxserver, that's the nx shell, doesn't run with root\nprivileges and you don't have any way to break in as\nroot if not by knowing the root password.\n\nA per-server NX private key is created at the time the\nNX server is installed. The public nx user doesn't need\nto run as root as it logs to the node (usually the same\nmachine where NX server is running) using the public\nkey. The public key is \"trusted\" by all the NX-enabled\nusers on the server.\n\nBy breaking into the nxserver as user \"nx\", you can\neventually run a shell as any user that is NX-enabled.\nOn the other hand, this doesn't give in any case root\naccess to the machine and let you just run as an\nunprivileged user. By the way, nxserver will never\nallow you to run sessions as user root.\n\n> because they use their own authentication mechanism,\n> plain text password or MD5 over encrypted ssh\n> connections, it is not possible to use other ssh\n> authentication mechanisms like Kerberos.\n\nWe don't -presently- support other authentication\nmechanisms. This doesn't mean that is \"not possible\"\nto implement alternative methods and maintain\ncompatibility with the default one. Infact this is\nwhat we plan to do.\n\n> Plain text passwords and MD5 hashes require that\n> you fully trust the server you log in or use a\n> separate password for each computer.\n\nA NX login offers the same security offered by SSH.\nIf the key of the server is changed, user is warned\nand the connection is aborted. Obviously you presently\nneed to have a separate password for each computer,\nthat's not that bad in 99% of the cases. Future\nversions will be able to offer single sign-on logins,\nKerberos and authentication based on smart cards. This\nwill be treated as an option, as requires that the NX\nuser that logs in to a machine is the same real user\nthat runs the session. This is not what we want in\nall the cases, as we need to support \"guest\" or\n\"anonymous\" sessions, the way is possible with HTTP.\n\n> The alternative architecture that I tend to implement\n> works a little bit like fish. Login via ssh, install\n> a perl/shell script on the server and start it.\n\nI don't see much differences, except that you run\neverything as the \"real\" user. So you cannot manage\nthe running sessions, the processes, the mounts and\nthe other sysrtem resources except if you run\nsomething else as root (that I wouldn't consider\nmore secure) or otherwise implement a mechanism to\nhave a \"limited privileges\" user, like the nx user,\nthat can access someone else's accounts. This is\nexactly what we do.\n\n> that it is more realistic for KDE 3.2.\n\nMay you please explain this point better?\n\nI was at the KDE booth at LinuxTag, together with\nother NX developers, both today and yesterday. I spoke\nwith Andreas Pour and others. I looked for you, as I\nwas told you were at LinuxTag. I think the best is that,\nif you like, we sit down and have a chance to exchange\nsome opinions. This will save a lot of time spent on\n\"reverse engineering\" things that are not really\nintended to remain obscure.\n\n/Gian Filippo Pinzari."
    author: "Gian Filippo Pinzari"
  - subject: "Re: open source"
    date: 2003-07-12
    body: "> nxserver, that's the nx shell, doesn't run with root\n> privileges and you don't have any way to break in as\n> root if not by knowing the root password.\n\nBut how can it log-in users with knowing only the MD5 hash of the password? You need at least some setuid component.\n\n> A per-server NX private key is created at the time the\n> NX server is installed.\n\nHow does that help? It will allow the server to authenticate to the users (which is also important, especially with plain text passwords), but what prevents somebody who does not know a valid account from getting through the sshd login process and accessing the nxserver shell?\n\nMy issue is that a user without credentials can do relatively complex operations with sshd and also nxserver, which would require a much higher level of trust in both sshd and nxserver than a regular ssh login.\n\n\n> Obviously you presently need to have a separate password for each computer,\n> that's not that bad in 99% of the cases. \n\nHmm.. you are probably thinking about different use cases than I do. One of my major use cases is an administrator managing workstations, with a (NX) server running on each. \n\n\n> I don't see much differences, except that you run everything as the \"real\" user. So you cannot\n> manage the running sessions, the processes, the mounts and the other sysrtem resources\n> except if you run something else as root (that I wouldn't consider more secure) or otherwise\n> implement a mechanism to have a \"limited privileges\" user, like the nx user, that can access\n> someone else's accounts.\n\nIf the authenticated user would be able to access a server via unix domain sockets or a similar mechanism it can do the same things (on Unix systems, as I said for Windows/Cross-platform that would be more complicated). The difference is that the amount of code that you need to trust would be much lower, and you would get all ssh authentication mechanisms, including password challenge-response, public key authentication and kerberos, for free. I see this feature as something that should be possible by default on every single desktop machine, including desktops. Any potential vulnerability should be avoided, and the ssh login is perfect because it does not increase the risk.\n\n \n> > that it is more realistic for KDE 3.2.\n>  May you please explain this point better?\n\nTime constraints. I have quite a lot of work to do for 3.2, and the direct login mechanism would be the easiest (because fish makes it really easy to execute commands on remote machines, I just need to write a GUI and scripts for the server-side). But that would not exclude the possibility of adding support for the NX server as well, there's just not much time.\n\n \n> I was at the KDE booth at LinuxTag, together with other NX developers, both today and\n> yesterday. I spoke with Andreas Pour and others. I looked for you, as I was told you were at\n> LinuxTag.\n\nSorry, I only visited LT today.\n\n"
    author: "Tim Jansen"
  - subject: "Re: open source"
    date: 2003-07-13
    body: "> But how can it log-in users with knowing only the MD5\n> hash of the password? You need at least some setuid\n> component.\n\nI tried to explain this in the previous post. Sorry if\nI was unclear. NX server sets the nx user's public key in\nthe trusted keys of the added account, so, once the real\nuser is authenticated, nx user logs to the node without the\nneed to use the real system password. This doesn't prevent\nNX to client and server to use a different authentication\nmethod, infact we already have a preliminary version that\njust relies on SSH daemon and logs users with any method\nsupported by SSH. In this case, anyway, nxserver is still\nexecuted as user nx, as this (unprivileged) user is the\none that has control of resources assigned to running\nsessions.\n\n> How does that help? [...], but what prevents somebody\n> who does not know a valid account from getting through\n> the sshd login process and accessing the nxserver shell?\n\nNothing. Is this a problem? What prevents a user to run\na httpd process on a Apache web server? It just a matter\nof writing a secure nxserver (the way you need a secure\nSSHD or HTTP or IRC or FTP or whatever server).\n\n> My issue is that a user without credentials can do\n> relatively complex operations with sshd and also\n> nxserver\n\nSorry, I don't understand this. It executes nxserver.\nWhat nxserver does is to check the password by means of\nany of the supported mechanisms (MD5 password or any auth\nmethod supported by SSHD+PAM) and if login fails it exits.\nIt's exectly the same you get by running HTTP (as user\nnobody) over SSL.\n\n> Hmm.. you are probably thinking about different use\n> cases than I do. One of my major use cases is an administrator\n> managing workstations, with a (NX) server running on each.\n\nI think at the millions users of any Linux computer anywhere\nin the world. The case somebody sets up up a single-logon\ndomain is not that widespread. On the other hand there is no\nproblem to handle this with current architecture. You just\nignore a feature (the possibility to have separation between\nsystem accounts and NX accounts) to use another feature (the\npossibility to identify any system user as a NX user).\n\n> and you would get all ssh authentication mechanisms,\n> including password challenge-response, public key\n> authentication and kerberos, for free.\n\nAs I explained, the only difference is that NX server does\nmuch more han what you prospect and we need to run services\nas nx user if we want to control the server machine without\nrunning monitors and daemons as root.\n\n> Time constraints. I have quite a lot of work to do for\n> 3.2, and the direct login mechanism would be the easiest\n> [...]. But that would not exclude the possibility of\n> adding support for the NX server as well [...].\n\nI think that your plan is absolutely OK and I agree that\nwe can have better interoperability in future :-). I just wanted \nto make clear some technical details. Please don't hesitate\nto write to me if you have any question or any issue to \ndiscuss.\n\n/Gian Filippo Pinzari."
    author: "Gian Filippo Pinzari"
  - subject: "Re: open source"
    date: 2003-07-13
    body: ">>but what prevents somebody\n>> who does not know a valid account from getting through\n>> the sshd login process and accessing the nxserver shell?\n> Nothing. Is this a problem? What prevents a user to run\n> a httpd process on a Apache web server? It just a matter\n> of writing a secure nxserver.\n\nYes, but that's why newer distributions do not enable httpd by default anymore. To reduce the amount of network-accessible functionality that may contain security leaks. BTW it is not only about nxserver, also sshd. When sshd has an exploit that can only be used by authenticated users, the attacker would gain root rights. The initial authentication is only a subset of sshd, so allowing untrusted users to authenticate increases the risk. Not a significant risk for a server which also runs services like httpd and which can be assumed to be administered by someone who will update when there are exploits etc. But quite a risk for a desktop machine run by a casual user.\n\n> As I explained, the only difference is that NX server does\n> much more han what you prospect and we need to run services\n> as nx user if we want to control the server machine without\n> running monitors and daemons as root.\n\nOn Unix you could have the same capabilities when users would login using ssh and then start a setuid executable that runs as the nx user.\n\n \n\n"
    author: "Tim Jansen"
  - subject: "Re: open source"
    date: 2003-07-14
    body: "> But quite a risk for a desktop machine run by a casual user.\n\nI agree that running SSHD on a computer can be considered\na security risk, but we are still speaking about enabling\nremote logins to a desktop machine. As an experienced Unix\nuser, I would tend to trust SSHD more than a new login\ndaemon written from scratch.\n\n> start a setuid executable that runs as the nx user\n\nThis is what we do when using SSHD/PAM auth methods. In this\ncase you end up trusting the user even more as, for example,\nyou cannot force a logged NX user to run a restricted shell.\nFurthermore, this still doesn't solve the problem of having\nguest logins.\n"
    author: "Gian Filippo Pinzari"
  - subject: "Re: open source"
    date: 2004-04-01
    body: "sir\niwant to know how much linux security is full proof.\ncan some one login the server as root without changing the root password.\nif so then please tell me all the means and preventives also.\n\nvivek."
    author: "vivek"
  - subject: "Re: open source"
    date: 2003-07-11
    body: "No conspiration theory needed. NoMachine spelled out in public what their intention is\nand how they imagine to make a living from their development. Any person with the\nwillingness and power to read it may find it f.e. here:\n\nhttp://lists.kde.org/?l=kde-promo&m=104889637120424&w=2"
    author: "Kurt Pfeifle"
  - subject: "This isn't anything new."
    date: 2003-07-14
    body: "There have been commercial X-Servers for windows for a long time.  At my univ (Univ of Minnesota) I did exactly this on Win2K.  I had/have debian at home with KDE3 on it.  I would use the x server that the univ had on its windows machines and run KMail from home.  It was fairly slow even though I have a 1.5/384 ASDL at home and the univ has something <b>FAR</b> better than that, but usable.\n\nHere is what I would do.  Run the non-commercial SSH client at school to SSH to my home debian machine -- with X tunneling on.  Then I would run the xwin server on the univ's win2k machine.  Then I would go back to the ssh client at run what ever like kmail and about a 30-45sec later a window would open up and then another 15sec or so it woul finish drawing the window and I could use kmail.  Those times were also highly dependent on which kde theme and icons I was using.\n"
    author: "SupetPET Troll"
  - subject: "commercial ML-View"
    date: 2003-07-15
    body: "After reading the NoMachine pages and seeing that they are based in Italy, it suddenly all came clear to me.. These are the guys that made ML-View which was influenced by dxpc ( http://www.vigor.nu/dxpc/ ), which itself was influenced by LBX.\n\nIt sure would have been simpler if they just said that NX is an X-protocol compressor!\n\nJust wondering if this also does session management, like the SUNray or X-Ray ( http://www.edu.uni-klu.ac.at/~mkropfbe/xray.html ) ?!\n"
    author: "Anonymous"
  - subject: "Re: commercial ML-View"
    date: 2003-07-15
    body: "Uh, they do say that."
    author: "Datschge"
---
Want to run Konqi from a Windows XP desktop? Want to use KDE's marvellous <a href="http://quanta.sourceforge.net/">Quanta Plus</a> Web editor from inside Windows?  Not that you particularly like XP -- it's just that you have no choice.  Well, <a href="http://www.nomachine.com/">with NX</a> now you can.  I've compiled <A HREF="http://www.danka.de/printpro/NX.html">some 
background info</A> on how to achieve this, including some nice and revealing screenshots:
<A HREF="http://www.danka.de/printpro/NX-konqi-in-WinXP-1.png">Konqi on WinXP 1</A>, 
<A HREF="http://www.danka.de/printpro/NX-konqi-in-WinXP-2.png">Konqi on WinXP 2</A>, 
<A HREF="http://www.danka.de/printpro/NX-quanta-in-WinXP-1.png">Quanta on WinXP</A>, 
<A HREF="http://www.danka.de/printpro/800x600-KDE-session-on-WinXP1280x1024.png">KDE Session on WinXP</A>.  
<!--break-->
<p>
KDE supporters of the "Office Productivity" booth at <a href="http://dot.kde.org/1057427072/">LinuxTag</a> are preparing
to show off this very cool application of remote X.  It uses a new, 
highly efficient compression technology that allows one to run a full KDE session (or single application) remotely and display locally even over a 9600 baud modem link. A KDE 
startup may transfer as little as 35K across the wire, while a remote 
"vanilla" X session would use as much as 3.5M.
<p>
<a href="http://www.nomachine.com/">NoMachine</a> are also trying to gain recognition in the <a href="http://www.guinnessworldrecords.com/">"Guiness Book of World Records"</a> by having more than 100 users concurrently connected to their application
server, each running a full KDE desktop session with KOffice. </p>
<p>
The server will be named "desktop.linuxtag.org" and will be officially online on Sunday 13th from 13:00 
to 14:00, and also available for training and testing on Friday the 11th.</p>
<p>
As an aside, running GNOME on this server is also possible.  Perhaps there
will be 50 GNOME/OpenOffice.org and 50 KDE/KOffice sessions in parallel, joining
forces to win the record against <a href="http://www.citrix.com/">Citrix
MetaFrame</a>...  Stay tuned!</p>