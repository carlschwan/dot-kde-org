---
title: "Trolltech Releases Qt 3.2"
date:    2003-07-24
authors:
  - "jblomo"
slug:    trolltech-releases-qt-32
comments:
  - subject: "Truetype Font antialiasing"
    date: 2003-07-23
    body: "I am experiencing some problems with my SuSE 8.2 freetype lib as it seems it is not possible to get antialiasing to work with truetype fonts (at least it works with type 1 fonts). Anybody here who has similar problems (and possibly a solution)?"
    author: "Thomas"
  - subject: "Re: Truetype Font antialiasing"
    date: 2003-07-24
    body: "Please post your support question to the: kde-linux list and we will try to help you.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Motif migration"
    date: 2003-07-23
    body: "So QT 3.2 will it make easiert to migrate old motif programs? Any experiences?"
    author: "Hein"
  - subject: "KDE 3.1 with Qt 3.2"
    date: 2003-07-24
    body: "What is the nature of those problems?  I thought Qt 3.x was supposed to be backwards compatible.  Sounds like a serious bug."
    author: "ac"
  - subject: "PS font names"
    date: 2003-07-24
    body: "The important question is, does this fix the problems with font names?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "So..."
    date: 2003-07-24
    body: "Are developers interested in fixing KDE 3.1.x problems with QT 3.2, or are bug reports on this not welcome?"
    author: "cbcbcb"
  - subject: "QSplashScreen"
    date: 2003-07-24
    body: "they add a \"Splashscreen\" widget? wtf? so no KDE/QT applications have had splashscreens before this NEW feature? Sounds like bloat to me.\n\n"
    author: "autumn"
  - subject: "Re: QSplashScreen"
    date: 2003-07-24
    body: "Well, it means that all apps don't need to roll their own splash implementation, thus reducing bloat."
    author: "Chakie"
  - subject: "Re: QSplashScreen"
    date: 2003-07-24
    body: "The fact that KDE/Qt applications had to roll their own SplashScreen widgets is evidence that TT's customers wanted a SplashScreen widget in Qt.."
    author: "fault"
  - subject: "Re: QSplashScreen"
    date: 2003-07-24
    body: "It's a trivial implementation. It doesn't cause any bloat. It is essentially a bare widget that displays a pixmap and starts a timer. At most it has 20 lines of real code."
    author: "David Johnson"
  - subject: "Re: QSplashScreen"
    date: 2003-07-28
    body: "It's better than a timed splash screen. This new splash widget displays automatically until the application is opened. Thus, no timer. It's automatic. Now, instead of a crappy timed splash screen in 20 lines, you can have a fully auto splash screen in 3 lines."
    author: "zeroskill"
  - subject: "Re: QSplashScreen"
    date: 2003-07-25
    body: "It's the opposite of bloat. Before applications that wanted a splash screen had to write one from scratch. Now applications can use QSplashScreen (or KDE developers the soon to be KSplashScreen) therefore reducing code duplication."
    author: "Chris Howells"
  - subject: "about that font rendering...."
    date: 2003-07-24
    body: "How does this new font rendering engine fit in with freetype2, Xft, etc for X ?  \n\nAre these things complementary? mutually exclusive?\n\nCan someone gimme a clue, please?"
    author: "and large"
  - subject: "Re: about that font rendering...."
    date: 2003-07-24
    body: "I think it still uses freetype, Xft ... as usual... the only thing you could probably notice is that fonts are actually BIGGER while webbrowsing. Well, at least they are much bigger in the qt-copy verson I have right now."
    author: "uga"
  - subject: "Re: about that font rendering...."
    date: 2003-07-25
    body: "Qt now recognises that fontconfig/Xft2 can artificially italicise fonts, so italics can now be selected for any font from font selection dialogues. :)\n\n... At least I _think_ that's a new Qt feature. Not sure how long it's been there."
    author: "Deciare"
  - subject: "Qt getting aheat of KDE"
    date: 2003-07-26
    body: "Is KDE goign to lose pace with Qt, since 3.2 was released now and KDE still has at least 5 months until 3.2 will be released and by then there wil almost be a new version of Qt. IT would really suck if KDE couldn't keep pace with Qt like in teh past where the releases were almsot simultaneous."
    author: "somebody"
  - subject: "Re: Qt getting aheat of KDE"
    date: 2003-07-26
    body: "Since when and why should there be a necessity to release KDE simultaneously to a new Qt version (which doesn't even fix known problems and thus forces KDE developers to work around them in KDE libs instead)?"
    author: "Datschge"
---
The newest iteration of <a href="http://www.trolltech.com/products/qt/">Qt</a> has been released today.  See the <a href="http://www.trolltech.com/newsroom/announcements/00000135.html">announcement</a> on <a href="http://www.trolltech.com">Trolltech's site</a>.  User desktop improvements include a <i>"completely re-written, faster font rendering engine."</i>  Developer improvements include new QSplashScreen and QToolBox objects.  Looks fun!
<!--break-->
<p>
KDE developers will be pleased to hear that Qt 3.2 is available from the qt-copy CVS module. It is expected that Qt 3.2 will be 
<a href="http://lists.kde.org/?l=kde-core-devel&m=105897722031627&w=2">
required</a> for KDE's main development branch (CVS HEAD) in the near future. It should be noted that 3.1.x versions of KDE experience some problems with Qt 3.2 and that for that reason this combination is not recommended.