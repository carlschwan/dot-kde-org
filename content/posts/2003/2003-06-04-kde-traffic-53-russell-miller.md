---
title: "KDE Traffic #53 by Russell Miller"
date:    2003-06-04
authors:
  - "Juergen"
slug:    kde-traffic-53-russell-miller
comments:
  - subject: "he he !!"
    date: 2003-06-03
    body: "mera number aa gaya :-)"
    author: "nilesh"
  - subject: "For the rest of you ..."
    date: 2003-06-03
    body: "Okay .. that meant that finally its my turn ... :-)"
    author: "nilesh"
  - subject: "Re: For the rest of you ..."
    date: 2003-06-04
    body: "Sorry dude.  I got first post, and Juergen got second.\n\nYou're number three!  You're number three!\n\n:P"
    author: "Russell Miller"
  - subject: "Re: For the rest of you ..."
    date: 2003-06-04
    body: "Ummmmmm\n\nNumber one, number two, number three....\n\nThis is a cryptographic comunication ... \n\ndid someone have the gpg=key?"
    author: "LAWYER KAMAL"
  - subject: "Re: For the rest of you ..."
    date: 2003-06-04
    body: "If we use the latest qt build, will you need to translate this :-) KDE 3.2 will support hindi as well ... ;-)"
    author: "PaintedReality"
  - subject: "kde 4.0"
    date: 2003-06-04
    body: "First, thanks Russell Miller.. I really enjoy your writing style in this one.. makes reading so much text a breeze to read..\n\nSecond, a quote from the article:\n\n\"Stephan Kulow replied that he wants to wait for Nove Hrady because he's not even sure if he'll want to scrap 3.2 and go right to 4.0 (!?!).\"\n\nWow. Isn't release of kde 4.0 conditional of when qt 4.0 comes out? I expect a lot of architecutral changes to occur in kde 4.0, such as scrapping aRts (hopefully!).\n\nA bug count of 2300 is alarming, but briefly looking at bugs.kde.org, it seems that most of them are old konqueror or khtml bugs.. Hmmpph.. can anyone tell me how to get a number?"
    author: "rotary"
  - subject: "Re: kde 4.0"
    date: 2003-06-04
    body: "You're welcome.  Being professional doesn't have to mean being stuffy, that's my take on things.  You should have seen me while I was still attending church - I knew exactly which buttons to press to make people very uncomfortable without quite crossing the line.  It was quite entertaining.  And if you get a chance to visit NW Iowa this summer, I'm accompanying for the local community theater - I also try to put that philosophy into that as well.\n\nReplies to that comment revised the count down to around 1,000 or so.  That's still a lot.  And since the criteria for judging what a real \"bug\" is varies from person to person, I don't think that question will be answered satisfactorily for quite a while.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: kde 4.0"
    date: 2003-06-04
    body: "You should have seen me while I was still attending church - I knew exactly which buttons to press to make people very uncomfortable without quite crossing the line.\n\nAnd would that be the button to turn on the heater and off the AC in the middle of summer :)"
    author: "a.c."
  - subject: "Re: kde 4.0"
    date: 2003-06-04
    body: "No, the ministers had already taken care of *that* little detail.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: kde 4.0"
    date: 2003-06-04
    body: "btw, i don't think kde is soo arts-dependant, you can do an OK job of configuring an arts-free KDE yourself, with JuK, kmplayer, configuring knotify to do \"play foo.wav\" instead of playing trough arts, and disabling the startup of artsd at kde startup ...\n"
    author: "ik"
  - subject: "Re: kde 4.0"
    date: 2003-06-04
    body: "You still have to install arts, so KDE is still dependant on it, even if you can disable it."
    author: "epoch"
  - subject: "Re: kde 4.0"
    date: 2003-06-06
    body: "I always compile from sources and remove artsd from the installed dir after everything has been installed correcty."
    author: "Thomas Zander"
  - subject: "Nove Hrady?"
    date: 2003-06-04
    body: "sorry for the dumb question, but wtf is Nove Hrady?"
    author: "N3wb1e"
  - subject: "Re: Nove Hrady?"
    date: 2003-06-04
    body: "Nove Hrady is a town in the Czech Republic where the KDE Developer's Conference 2003 will take place: http://dot.kde.org/1051497356/"
    author: "Anonymous"
  - subject: "Searching for old reports"
    date: 2003-06-04
    body: "If you want to get a number of reports idling for a long time go to the home page of bugs.kde.org, scroll down to the \"open reports idling...\" thingy, set it to eg. \"from one quarter to forever\", click \"show\" and you'll get all open reports idling for more than four months (atm it results in 5430 reports including wishes, many of them are surely duplicates or simply outdated).\n\nWe really need an organized bug squadron to eliminate all useless reports ensuring that bugs.kde.org gets even more efficient."
    author: "Datschge"
  - subject: "BTW"
    date: 2003-06-04
    body: "Good to see mosfet getting more and more involved in kde development. I remember as one of the most vocal KDE developers way way back when in the debian-legal mailing list ;)\n\nmosfet vs. knightbrd (I believe this was the nick.. I forget exactly) flame throwings were always entertaining."
    author: "rotary"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "There was an entertaining flamewar on the list - that, oddly enough, didn't involve mosfet - recently.  I didn't cover it because I didn't deem the subject interesting enough to justify covering such an inflammatory post.  You might want to look back in the archives for a couple of weeks - I believe the post was about a person who wanted to \"program\", but not \"code\".  It also spilled out into the dot.\n\nThe ironic thing was that, mostly, those who were best qualified to refute most of his points were experienced enough to know not to reply.  (I'm painting with a broad brush here, so please no one take offense)\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "Uhm, we had the same here on dotty as well and I think it got resolved sufficiently. I don't think we need a review of it, it's past now."
    author: "Datschge"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "Please distinguish my postings here from an extension of kde-traffic.  I speak for myself and not for kde-traffic unless I'm actually speaking directly about kde-traffic.  If that makes sense.\n\nI guess, IOW, think of anything I say here as inside the \"editorialize\" tag.  There's a reason I didn't report that flamewar on kde-traffic.\n\nHowever, this does give me an idea for kde-traffic.  I'll have to mull it over for a while and then get feedback, it's just off-the-wall enough that it might ruffle a few feathers.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: BTW"
    date: 2003-06-05
    body: "Are you thinking of some entertaining soap opera style for kde-traffic? A la \"White trash battling it out in public, throwing chairs at each other. X crying about Y's other women. ...\" perhaps? ;)"
    author: "Datschge"
  - subject: "Re: BTW"
    date: 2003-06-05
    body: "Nothing quite so tacky.  Just something like a \"hall of shame\" that flamewar participants would get their names up on.  But even that is a little \"out there\", and would require a great deal of thought before implementing.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: BTW"
    date: 2003-06-06
    body: "> There was an entertaining flamewar on the list\n\nI am glad to see that someone else found it entertaining. :-D\n\nI think that there is a difference between inflammatory and provocative.  I intended the original porting to be proactive, to provoke a discussion.\n\nI was disappointed that it didn't.  If you will take the trouble to review it carefully, you will see that it mostly degenerated into flaming about side issues, and there were no answers to my actual ideas and/or suggestions.\n\n> I believe the post was about a person who wanted to \"program\", but not \"code\". It also spilled out into the dot.\n \nI am also disappointed that you failed to understand the point I was trying to make about how I could contribute.  I said that, and say again, that I can help with the design/engineering even though I don't feel that at the present time I can contribute code.\n\nSince you have provided an e-mail address, I will send you a private mail about this.\n\n--\nJRT "
    author: "James Richard Tyrer"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "Mosfet isn't really much involved in KDE development nowadays. Seems he only appears when he has a chance to make it into the news like this time again."
    author: "Anonymous"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "Seems some people are jealous of him."
    author: "Anonymous"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "You can be active in development without doing any coding."
    author: "Erik Hensema"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "He could very well be doing KDE development without being part of the core team.  Mosfet has made some cool KDE hacks and some of them are not as well-known as they should be.  Pixie was freaking cool man but nobody distributes it anymore."
    author: "anon"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "> Pixie was freaking cool man but nobody distributes it anymore.\n\nHuh? I think SuSe has packages for Pixie... am I wrong? A friend was upgrading to 8.2 yesterday, and I think I saw this package."
    author: "uga"
  - subject: "My coding stuff"
    date: 2003-06-04
    body: "Yep, Suse and Debian have official packages of PixiePlus, FreeBSD has a package that I believe is official, and there are unofficial packages for dists like Mandrake. Of course, PixiePlus is actively developed and you can download it from mosfet.org, too ;-) I'm going to try to more heavily promote the next release, it kicks butt.\n\nAs a matter of fact I've done a ton of coding recently, mostly concerning PixiePlus. The next version will be Beta1 in preparation for a 1.0 release. Since I haven't updated my page in awhile I'll attach the ChangeLog so far. Lots of work going on and I'm not finished yet :)"
    author: "Mosfet"
  - subject: "Re: My coding stuff"
    date: 2003-06-05
    body: "There is an port of PixiePlus for FreeBSD that's as \"official\" as any port can be. It works quite well. Keep up the good work."
    author: "David Johnson"
  - subject: "Re: My coding stuff"
    date: 2003-06-05
    body: "What about MosfetPaint? I saw the screenshots on your site but no downloads. Is it not better to allow downloads and then get user's response about bugs?"
    author: "Anon"
  - subject: "Re: My coding stuff"
    date: 2003-06-05
    body: "MosfetPaint won't be ready for public consumption until after PixiePlus 1.0. I don't have time to seriously work on both at the same time. I usually don't release code until I feel it is at least somewhat usable, and MosfetPaint isn't yet."
    author: "Mosfet"
  - subject: "Re: BTW"
    date: 2003-06-04
    body: "Good to hear.  Mandrake 9.1 doesn't have it although maybe the power pack does."
    author: "anon"
  - subject: "Re: BTW"
    date: 2003-06-05
    body: "PixiePlus is in my SuSE 8.1, so it should be in 8.2 as well.\nI like it."
    author: "reihal"
  - subject: "I will definetely try Pixie"
    date: 2003-06-05
    body: "It seems to be quite good even though Gthumb has been great so far, I still want to see Pixie beta 1. I mgiht even switch ;)\n\nAlso, how's liquid going? Are you thinking of implementing flashing buttons like in OS X, that's IMO what's really missing from taht theme. Flashing buttons to match the HPL window decoration which on hover makes buttons flash would be great."
    author: "Mario"
  - subject: "Re: I will definetely try Pixie"
    date: 2003-06-05
    body: "I played around with that on mouse hover using threads and found it to be rather buggy. You can get away with a non-threaded version for the window manager because since the window manager is separate from the application it doesn't get bogged down when the application is busy. The widgets are a different story: if the application is busy and not updating the GUI the buttons won't flash or will do so sporadically, which looks funny.\n\nThe next Liquid release will mostly focus on the window manager. That's really the only part that doesn't work like I feel it should: there is a problem with large titlebar font sizes and the buttons are not configurable."
    author: "Mosfet"
  - subject: "Personally..."
    date: 2003-06-04
    body: "Id rather wait a year for the next release if it ment it was more stable, faster, and took less memory. (Yes, all of these things are currently happening in CVS now.) And yes i know the longer we keep CVS open the more features we get, and the longer we freeze the more branches we get... but hey...  im running KDE 3.1.2 now on a P166 w/48mb of ram, and its running pretty well... Takes less memory than 3.0 did anyway.\n\nThrow in time to get Qt 3.2.1 out the door and we have a recipie for a solid KDE release.\n\nJust my 2c I know its no so simple, but we are so far ahead of Gnome, lets take our time and to it right.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "> Just my 2c I know its no so simple, but we are so far ahead of Gnome, lets take our time and to it right.\n\nKDE and GNOME should really focus on pulling users from windows, macos, etc.. not each other. These days, most people who use KDE will use KDE, and most people who use GNOME use GNOME.\n\nBut KDE is not that far ahead of GNOME in everything anymore. I do think that in terms of features and technology, KDE is still way ahead of KDE. But on the other hand, I doubt that anybody at all will argue that usability in KDE is way behind that of GNOME. \n\nAnd based on your personal philosophy, it sounds like moving up KDE 3.2 would be a good thing in the face of XD2 (coming out REAL soon.. see http://www.osnews.com/story.php?news_id=3705), and GNOME 2.4 (coming out Sept I beleive..) Ximian Desktop2 looks to be real polished-- something KDE has not been lacking since KDE 2.2 or so, imho.. it's all about the feature creep :)"
    author: "rotary"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "> But KDE is not that far ahead of GNOME in everything anymore....\n\nhahaha! have you tried to modify a shortcut which you placed on your desktop with GNOME\n(e.g. change the executable it launches)\n\nwell bingo... That is not possible with a current GNOME (the one from RH9 e.g.)\n\nand I swear you thats no joke !\n\n...another thing that really bothered my is that you _cannot_ turn of the window animation\nthat occurs while maximizine/minimizing.\n\n... and have you seen the file-open dialog? I wonder why the can (or want) not improve it before\nGNOME 2.6 (!!!) now they are at 2.2.X\n\nanyway... we have the choice, and mine is called KDE"
    author: "yg"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "> and have you seen the file-open dialog? I wonder why the can (or want) not improve it before GNOME 2.6 (!!!) now they are at 2.2.X\n\nActually, gnome 2.3.2 (unstable branch), has a new file open dialog.. Ximian Desktop 2.0, coming out next week, also has a new file open dialog (as did Ximian Desktop 1.4)\n\n> hahaha! have you tried to modify a shortcut which you placed on your desktop with GNOME. well bingo... That is not possible with a current GNOME (the one from RH9 e.g.)\n\nYeah, I beleive that wasn't possible in gnome 2.2. Of course, it wasn't possible with classic MacOS either, which gnome reminds me of (nostalgically.. I was a die-hard Mac user until 1998 or so.)\n\nIt is possible in the gnome running on my system (2.3.2), which will eventually be 2.4. \n\n> ...another thing that really bothered my is that you _cannot_ turn of the window animation that occurs while maximizine/minimizing.\n\n*shrug*.. there is probably some obscure gconf key somewhere. an website documenting these things in gnome would be good. Some people talk and talk about how culturred kcontrol is (which it is), but don't realize that it's as hard to find something like this in gconf. \n\n> anyway... we have the choice, and mine is called KDE\n\nYup. I use KDE 99% of the time, but gnome is still a nice desktop environment. In some ways it's ahead of KDE, and in other ways, it's quite behind. I think the two desktops have gone in different focuses now, which might actually be a good thing. In the end, choice is a very good thing."
    author: "fault"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "The Differrence is:\n\nGnome Developers do what they want...\nKde Developers do what the user wishes...\n\n"
    author: "andreas"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "No... not true... Developers on each side do what they want...\nimo the gnome-camp has to deal a lot more about this 'how to strip down the UI to make it useable for dumb users'-stuff (because of the involved companies) than the kde-camp does. MS has to deal with a lot of dumb users, too... That's why they need to pay their developers some money, otherwise nobody would like to program on a crippled interface like Windows ;-)\n"
    author: "Thomas"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "But (yeah, the famous \"but\" ;) another important fact is: KDE's bug tracker has a voting system, and high voted bugs and wishes actually get pretty much attention by developers. Gnome's bugzilla got that feature taken out so there is no way people can easily move the attention to certain bugs except crowding the report message boards which tend to be counterproductive most of the time, decreasing the efficiency of the whole system afaics."
    author: "Datschge"
  - subject: "Re: Personally..."
    date: 2003-10-14
    body: "Last time I checked, GNOME's Bugzilla had a voting system. That was yesterday, I think."
    author: "Matthias Warkus"
  - subject: "Re: Personally..."
    date: 2003-10-14
    body: "yes, but in june it didn't =("
    author: "anon"
  - subject: "Re: Personally..."
    date: 2003-10-15
    body: "So how do you search for votes with http://bugzilla.gnome.org/query.cgi?"
    author: "Anonymous"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "> hahaha! have you tried to modify a shortcut which you placed\n> on your desktop with GNOME  (e.g. change the executable it launches)\n \nHave you tried modifying a shortcut you've placed on the panel in KDE? (this used to have the same problem, can't check just now as I'm using windowmaker nowadays)"
    author: "cbcbcb"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "Yeap did it yeasterday, and worked fine.\n\nOff Topic:\nBy the way, I did it because I couldn't run more than one kfmclient... when I clicked the browser or filemanager icon on kicker a snd time I would get nothing. It worked fine on day, suddenly it stoped working :-/\nChanged it to konqueror --profile ... and it worked... very weird.\n(SuSE 8.2, kde 3.1.1)\nI'll probably bug report it, but have no idea how to gather info on this one. Ran it in konsole and all I got was an error msg saying kdeinit couldn't launch kfmclient."
    author: "jadrian"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "Dont make me come to your house and force you to report a bug!\nI DONT CARE if its a packaging bug, or a human error bug, or if its whatever. We have a bug system for a reason.  Worst case the maintainer wont get to it BUT its noted.  Its recorded. People wont suffer.  The worst thing you can do to KDE is to not report bugs and just complain about them here.   Nothing sucks more than finding out my shiny new KDE barfs on some users strange system, especially because I only test the systems that my clients use.  \n\nPlease please, file those bug reports. \n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "No worries, you can search my nick in the the kde bugs database. I report, comment and vote on bugs. \nJust wondering how I could get more info on this kind of stuff before reporting it... and thinking out loud here in the forum. If I don't manage to get more info about it, I'll just end up posting what I have on bugs.kde.org."
    author: "jadrian"
  - subject: "Re: Personally..."
    date: 2003-10-14
    body: "\"... and have you seen the file-open dialog? I wonder why the can (or want) not improve it before GNOME 2.6 (!!!) now they are at 2.2.X\"\n\nYou see, it took a hell of a lot of infrastructure work to get to a file selector that will work both with and without GNOME-VFS in place to support both GNOME applications and GTK+ applications. Furthermore, the thing should be slick UI-wise (no horizontal scrolling, yuck) and it should appeal to Joe Slashdot who has been clamouring for a new file selector for about two years now.\nI think KDE took the easy way out -- KDE's and Qt's file selectors are not the same AFAIK.\nOh, but then you work with a widget set that is much more third-party than ours, so you probably can't avoid that sort of problem."
    author: "Matthias Warkus"
  - subject: "Re: Personally..."
    date: 2003-10-14
    body: "> Oh, but then you work with a widget set that is much more third-party than ours, so you probably can't avoid that sort of problem.\n\nYup.. at various times in history, kde and qt's themeing wasn't even the same. "
    author: "anon"
  - subject: "Re: Personally..."
    date: 2003-06-04
    body: "Hey... I can respect the notion: KDE can't ship a \"stable\" release and have it fall flat on its face... we all want continuous improvement, no doubt about it.\n\nBut I have to agree with Binner/ Kainhofer:\n   added time != improved reliabilty\n\nKeep the scheme of recent release-schedules(post KDE-2) of fairly regular/frequent releases. I don't really agree with the dev-releases because that's just CVS no matter how you look at it.  Just wrap up the changes sometime soon (maybe after Nove Hrady) and announce a feature freeze.\n\nI know everybody wants to get in their new wiz-bang features, but every few months (definitely less than a year) the code needs a good clean up and then released to the users.  More time before a release just allows more time for CVS to become totally different from the previous release... it doesn't make it any more stable. (From my viewpoint anyway)."
    author: "Jeff"
  - subject: "Usability sucks in KDE comapred to GNOME"
    date: 2003-06-05
    body: "Oh come on anyone who thinks KDE is easier to use than gNOME is kidding themselves. In terms of usabiltiy GNOME wins hands down, and usabiltiy isn't just for dumb users it is for everyone even smart users like havin a usable desktop, this does not mean taking away a lot fo features, it just means reimplemented them in a easier to sue way. IE: DE's clock.\n\nKDE is really far behind in usability and without something as good as GNOME's HIG (written by SUN) anda  QA team taht needs to validate KDE applications ebfore shipment things won't improve. \n\nUsers want more feautres, but they would much rather have an easy and antural desktop. "
    author: "Mario"
  - subject: "Re: Usability sucks in KDE comapred to GNOME"
    date: 2003-06-05
    body: "Your text's readability sucks as well. ;)"
    author: "Datschge"
  - subject: "Re: Usability sucks in KDE comapred to GNOME"
    date: 2003-06-05
    body: "> Users want more feautres, but they would much rather have an easy and antural desktop.\n\nThat's not true at all. People (who matter-- non-computer \"newbies\") usually want features, not usability. If this weren't the case, Apple would have 90% market share, WordPerfect would be the most popular wordprocessor, Lotus 1-2-3 would be the most popular spreadsheet app, etc.. \n\nAnyway, most users screamed for features from the time before KDE 1.0 to around KDE 3.0. There were less usability concerns back in KDE 2.x and 1.x because there was less configurability.\n\nIn fact, back then, a lot of people said that KDE was less tweakable than GNOME 1.2 and 1.4. KDE 2.0-KDE 2.2 probably were actually. \n\n> KDE is really far behind in usability and without something as good as GNOME's HIG (written by SUN) anda QA team taht needs to validate KDE applications ebfore shipment things won't improve.\n\nNah.. KDE 2.x proved that things could remain usable than a rabid usability team that continues to strip out or leave out perfectly usable programs, as is the case currently in the GNOME world. This might be fine in GNOME development, but there are simply more volunteers in the KDE side of things than vice versa, and I don't think turning down people's work is quite healthy for KDE right now."
    author: "trill"
  - subject: "Re: Usability sucks in KDE comapred to GNOME"
    date: 2003-06-05
    body: "> KDE is really far behind in usability and without something as good as GNOME's HIG (written by SUN) anda QA team taht needs to validate KDE applications ebfore shipment things won't improve. \n \nTalk about a poorly formed cheering section. KDE had design guidlines when GNOME was making a big deal that they were going to have them and asked KDE to sign on. Not only that developing on KDE automatically supports this where as GNOME developers *have* to be asked to do this because of architectural differences. Of course SUN is known for being a powerhouse in desktop market share. Now KDE needs a QA team? How about advertising, public relations and cost accounting? The thousands of users on CVS as well as the millions on official releases using the bug tracking system affords it's own QA. However I'm wondering how a sentence started on usability and gravitated towards QA which is not directly related.\n\nI think the crime of all this propaganda from the GNOME camp on misguided usability ideas is that it first assumes that usability is a \"one size fits all\" proposition. If that is what open source is now about then we really do need the advertising and accounting departments to sell that BS. The next one is that fewer options make any difference at all to the vast majority who never even look for them if there are good defaults. Finally there is the fact that for experienced users who know what they want, not allowing them to do what they want does not enhance their user experience.\n\nJust because someone subscribes to an invasive perspective that tosses out software contributions in favor of rewrites because of \"too many options\" does not mean that such a narrow view has any merit for those people who hold the belief that flexibility and ease are not mutually exclusive. In case you're missing the point there is a difference in serving a button down world of lazy sysadmins and real people who actually enjoy a menu instead of the old SNL \"cheesburger, cheesburger, cheesburger, cheesburger, cheesburger\"."
    author: "Eric Laffoon"
  - subject: "Re: Usability sucks in KDE comapred to GNOME"
    date: 2003-06-05
    body: "You are absolutely right.   Glad to see such perspicacity."
    author: "TomL"
  - subject: "Re: Usability sucks in KDE comapred to GNOME"
    date: 2003-06-06
    body: "Gnome believes that less options mean better usability; and better defaults are good for more users.\nWhile these points are very valid, I know very very few people that acutally can and want to work with a Gnome desktop. Most people just get bored and reboot to Windows.\n\nI can't tell you why; but Gnome is not as good as you claim"
    author: "Thomas Zander"
  - subject: "Less Features = more Usability!?"
    date: 2003-06-06
    body: "Well, yes in the literal sense, but than again would you want applications where you can do only the simplest of tasks like in WordPad? I doubt you would and I would not want to have such dumbed down programs myself, I believe ther eshould be a good balance and msot features should be kept uness they are compeletely bogus like KDE's background program which only takes a screenshot ofa  website you point it to and refreshes the backgroudn every so often without eltting you navigate the website at all.\n\nHowever, what I do want is to have all the feature rich applications in KDE like Konqueror, the control center, Kdevelop etc. completely undergo a UI check in which the usability of these applications can be improved through contructive criticism and work from the developers. As it is, many of KDE's applications are very powerful yet not as easy to use as they should be. For example as many users have complained, Konqueror's context menus are just TERRIBLY stupid and will often display irrelevant options and miss the most obvious ones like \"copy\", the \"actions\" menu should improve usability a little though.\n\nI know power users don't care much about usabiltiy even though they do like better designe dUIS, btu teh thing is that normal suers wh aren't linux fans and just want to try it will quickly be turned off. Especiallly by the isntallation fo programs, I know I was, I do hope autopackage.org will fix this issue plaguing all Linux's.\n\nAnyway, i am very pleased to see that some UI sisues are being fixed, like the KDE clock for example. =) This si a nice start.\n\nIt would be great if KDE would design quality UI guidelines like GNOME's hig though.\n\n\n"
    author: "Mario"
  - subject: "Re: Less Features = more Usability!?"
    date: 2003-06-06
    body: "> It would be great if KDE would design quality UI guidelines like GNOME's hig though.\n\nKDE has already had these guidelines for over four years. Unfortunatly, the problem is that \n\n> However, what I do want is to have all the feature rich applications in KDE like Konqueror, the control center, Kdevelop etc. completely undergo a UI check in which the usability of these applications can be improved through contructive criticism and work from the developers.\n\nBtw, it helps if users contribute by giving mockups of how things should look. Qt Designer really makes this easy.\n\n> For example as many users have complained, Konqueror's context menus are just TERRIBLY stupid and will often display irrelevant options and miss the most obvious ones like \"copy\", the \"actions\" menu should improve usability a little though.\n\nAnd as MANY other users have said, it's been fixed in kde-cvs.. now, Konq's and Nautilus's are comparable in terms of items. Nautilus still doesn't have an actions submenu, while Konq does, but on the other hand, Konq doesn't have a \"Open in\" submenu, while Nautilus does. \n\n> KDE's background program which only takes a screenshot ofa website you point it to and refreshes the backgroudn every so often without eltting you navigate the website at all.\n\nIt's not meant to-- it's mainly for displaying one page things like satellite images and such-- at least that's what I used to use it for. It's not really completely useless. Will most people use it? Probably not though."
    author: "till"
  - subject: "Thanks Russel"
    date: 2003-06-04
    body: "I tend to agree with Mosfet on this one. KDE needs to have some kind fo regular realeases like GNOME has snapshots with well documented changes and a few screenshtos and other goodies,\n\nDevelopers should each have a list of what they have done for KDE so the changelogs could easily be compiled.\n\nWhat I think would be nice to help make this happen is for anyone to be able to click a poster or reporter's name and see a littleinformation about the person and their positiion not jsut their e-mail. For example\n\nname\npersonal information (e-mail, interests, lcoation etc.)\nbugs reported in last 7 days\n-\n-\n-\n-\n-\n-\n-\nview all bugs reported by ----\n\ncomments by -- from last 7 days\n- (links)\n-\n-\n-\n-\n-\n-\n-\nview all comments by --\n\npatches by --\n\nyou get the idea.\n\nI think taht would really be an improvement"
    author: "Alex"
  - subject: "Re: Thanks Russel"
    date: 2003-06-04
    body: ">KDE needs to have some kind fo regular realeases like GNOME has snapshots\n\nWait... not so fast. I personally hate the way GNOME releases code, so I'd rather not take them as a good example. Why do I say that? Easy: Distros would start putting code that is \"the one that works\".\u00e7\n\nIn next versions we could have either KDE 3.1.92, or 3.1.93.4, or whatever, if they see any problem in 3.2 or they just cannot wait until 3.2, and that would be a mess.\n\nAs an example, do you actually know *anyone* that knows what version of Gnome they are using?\n\nI only have rpm versions of Gnome, but my system says:\n\nlibgnome2_0   -> version 2.2.0.1\nlibgnome-vfs0 -> version 1.0.5\nlibgnome32     -> version 1.4.2\nlibgnomeui2_0 -> version 2.3.0\nlibgnome2      ->  version 2.2.0.1\n\nSo... am I using 2.2.? 2.3.0? 2.2.0.1? I don't care, but I would care about my kde version.\n\n"
    author: "uga"
  - subject: "Re: Thanks Russel"
    date: 2003-06-04
    body: "bugs.kde.org's query interface and http://lists.kde.org/?l=kde-cvs&w=2&r=1&s=cvsuserid&q=b are your friends."
    author: "Anonymous"
  - subject: "Re: Thanks Russel"
    date: 2003-06-04
    body: "Good idea regarding your suggestions for bugs.kde.org. I'll look what feasible ways realizing something of that kind are there."
    author: "Datschge"
  - subject: "Re: Thanks "
    date: 2003-06-05
    body: "I can't wait tos ee what you come up with =)\n\nAlso thanks for the tip .... annonymous ?!"
    author: "Mario"
  - subject: "Please Please Don't Wait Until KDE 4.0!"
    date: 2003-06-04
    body: "One of the biggest things KDE fans are waiting for, in regards to KDE 3.2, is the new stuff in khtml from Safari.   While I understand that a lot of really, really great ideas are going to come out of the conference coming up, I don't think you should let the hormones and insight completely take hold of the release cycle.\n\nA 4.0 release is something that would take a significantly longer period of time.   It might be six months.  It might be three.   It might be a year.   We won't know, and only the developers will have any idea.    With an incremental release such as 3.2, we have reasonable expectations that a release will come out sooner rather than later.\n\nKonqueror, and by extention KDE have a lot to gain by using the momentum from Safari.   Having to tell users that yes, the changes from Safari will come back into Konq, but then having to tell them that it won't be for a long time will have a worse effect than never having the help from Apple at all.   It's all about higher expectations.\n\nAbout promoters:  Promoters are not magical people who can open up your brain while you sleep and download a new list of features that are in CVS.   Nor should people who are primarily promoters be required to read every line of code in the KDE project; that's what developers are for.   Honestly, if developers want the promoters to spread the word about new features, developers are going to have to meet them half way and write in depth and accurate changelogs.\n\nI'm not even an official promoter and I know it would help *me* when I extoll the virutes of the KDE project."
    author: "vosque"
  - subject: "Re: Please Please Don't Wait Until KDE 4.0!"
    date: 2003-06-04
    body: "Promoters should be able to organize themselves and give the developers the time to actually improve KDE instead forcing them to spend time doing something nearly everyone else could do as well when taking enough time.\n\nI myself am not a developer, I consider myself a promoter. ;)"
    author: "Datschge"
  - subject: "Re: Please Please Don't Wait Until KDE 4.0!"
    date: 2003-06-04
    body: "There's two substatements implicit in your statement:\n\n1) That writing comprehensible changelogs is beyond the responsibilities of a coder.\n\nPersonally, I feel that is about the same thing as saying that coders should spend time coding and not documenting their code.   Changelogs explaining what exactly it was that they added is part and parcel documentation.   It could very well be that these changlogs are already in there somewhere, at which point it becomes a technical problem.\n\n2)  Project Promoters are an organized force, just like developers.\n\nWhile there is a kde-promo team,  they aren't the only ones doing the promoting.   The folks sending out promotion info to companies may be an organized force, but the KDE guys I meet with at the pub on Thursdays certainly aren't.   Nor should they be, because then it starts to feel like astroturf.\n\nI personally don't have the resources to get involved with kde-promo officially, though I certainly talk about KDE wherever it's appropriate.  I've made a fair number of converts.   More documentation about what's coming would certainly help.   If KDE is actually interested in the number of people it has using it[1], they shouldn't discount this sort of help.\n\nLa la la, I'm (most recently) a project manager.   I think it shows. :)\n\n\n[1]:  It could very well be that the KDE people don't really care how many people are using their desktop.   If that's the overall belief, I can dig it.   However, if that really were true, what's the point of having a kde-promo group in the first place?  Surely it's not just to bat away the GNOME trolls. :)\n\n\n\n"
    author: "vosque"
  - subject: "Re: Please Please Don't Wait Until KDE 4.0!"
    date: 2003-06-05
    body: "1) The the most extensive documentation of changes in KDE possible is the KDE CVS list, Derek Kite (a single person) shows week for week how useful it is for documenting changes in KDE. Now think about two additional components improving on this: firstly tell developers to add more descriptive text to their CVS commits, secondly let some people dig through the KDE CVS list, Derek's weekly summaries and personally check out the latest CVS head. Can only developers do that? Imo not at all. Is it hard to do? Imo not at all. So where's the problem? =P\n\n2) We shouldn't think about any group as organized force in the sense that it's a closed strictly organized force (most of the time it just works mutually), what we should do is encourage everyone to do something about something as soon as she/he feels like it instead giving others the impression that contributing is very hard and others should do it, then having them begging for this and that. I'm glad you already noticed one doesn't need a promo force to convert a \"fair number\" of people. ;)\n\nAt http://datschge.gmxhome.de/support.html I've created a one-stop page showing all ways anyone can support KDE. I'm considering adding a section for changelog creation as soon as a respective website/mailing list exist (I think a wiki site would be great for this purpose, anyone willing to donate one? ;)."
    author: "Datschge"
  - subject: "Re: Please Please Don't Wait Until KDE 4.0!"
    date: 2003-06-05
    body: "1. very nice list on that website.. needs to go somewhere on kde.org imho :)\n\n> I'm considering adding a section for changelog creation as soon as a respective website/mailing list exist (I think a wiki site would be great for this purpose, anyone willing to donate one? ;).\n\nPerhaps set something up on developer.kde.org?"
    author: "till"
  - subject: "What I really want is not features."
    date: 2003-06-04
    body: "I just want a slick looking itnerface which si almsot there, only these issue sned to be fixed: http://www.kde-look.org/content/show.php?content=3910\n\nAnd most importantly, I want an intuitive user friendly, and consistent desktop. KDE greately need to imporve usability, many of its base applications such as the clock with 5 tabs http://bugs.kde.org/show_bug.cgi?id=58775 or KDE's bulky find tool also with far too many options shown at once. Even Konqueror is a mess when it comes to logical usability, often listing dozens of options in its context menu which are completley irrelevant to the user's actions and yet are still present and not even grayed out.\n\nFor a good example check out the find tool in GNOME 2.2 , as powerful as KDE's yet infinetely easier to use. Try it out yourself. "
    author: "Mario"
  - subject: "Re: What I really want is not features."
    date: 2003-06-04
    body: "Well, if you really want dumbed down user interface which still manages to look confusing and definitely not sleek, then maybe you should use GNOME?\n\nI installed MDK9.1 to one of my computers yesterday and while I was compiling KDE from CVS (didn't install MDK KDE packages), I tested Gnome 2.2 that was included with MDK.\n\nFirst I tried to configure panel, there were only very few options and the configuration dialog looked like it was designed by some six year old in playschool. Then I tried to configure the clock in the panel. Same thing happened. Not configurable at all and configure dialog still looked clumsy. Great indeed.\n\nI certainly don't want that to happen to KDE.  "
    author: "Janne Kylli\u00f6"
  - subject: "Re: What I really want is not features."
    date: 2003-06-04
    body: "I hope KDE never ever try the same way that gnome go.\nI heard from more people asking :\"Where can i configure this and this feature\" then the \"Configure Center\" is confusing me.\nI don't want a second \"Windows Regiestry\" where you must search for this and this feature.\n\nSorry for my bad english *g*"
    author: "andreas"
  - subject: "Re: What I really want is not features."
    date: 2003-06-05
    body: "I think we need to be balanced about this. I have no problem with KDE staying fully configurable. I don't really buy the \"configurability confuses users\" argument. But KDE clearly could be greatly polished without losing a whole lot of functionality. \n\nFirst of all, toolbars need to be rethought. Many KDE apps come with 3(!) toolbars. Very Microsoft-ish. In comparison, most GNOME apps start with a single toolbar. The default toolbars should only come with the functions that absolutely need quick access. For example, in my Konqueror setup, I have all of 4 buttons in my single toolbar. \n\nSecond, right click menus need to be cleaned up. Right-click menus are supposed to be context-specific, not general purpose! You should not have to actually read the menu like a list to find out what action to invoke.  There needs to be some intelligence applied to right-click menus. For example, the right-click menu for images in Konqueror (as of KDE 3.1) has 21 items. Almost all of them are useless. \n\nFirst, there are navigation items. If I wanted to navigate, I would click on the nagivation buttons on the toolbar, or use a shortcut key!\n\nThen there are three items for opening the image in various tabs and new windows. If those are absolutely necessary, they should probably be moved to a submenu. \n\nThen there is a non-functional copy item. \n\nThis is one item that really *should* work, but doesn't! \n\nThen there is a \"add to bookmarks\" item. How often do you add an image to the bookmarks list?\n\nThen there is an \"open with\" menu. Okay, this one can stay. \n\nThen there is a copy-to menu. This can also stay.\n\nThen there are save link as and save image as items. How often to you save the link to an image that you can't copy the image URL and save it manually? And the save-image item is redundant given the copy-to item. Having both to do very slightly different things is very wasteful. \n\nThen there is a select-all item. How do you select all of an image? Well, this item doesn't. It selects all of the text on the page. Isn't an RC menu supposed to be context specific? \n\nNext copy image location and copy link location. Okay, these are necessary. \n\nThen, stop animations. If this really needs to be accessed quickly, it should be a button on the toolbar instead of cluttering up every right click menu. \n\nThen view image. Don't we already have an open-with? \n\nThen, view source, view document information, security, and encoding. What in god's name do these have to do with the image in question?\n\nMost egregious of all, there is no properties menu. Konqueror as a file manager has a properties menu, and the image is just a remote file, so it should have a properties menu! And if you had a properties menu, you could move less often used items like image location to that. \n\nOkay, let's take a look at what this menu could be:\n\n- Open\n\t- In new window\n\t- In new tab\n\t- In background tab\n\t- With viewer\n- Copy\n- Save\n\t- Image As\n\t- Link As\t\n- Properties\n\nNow, the menu is reduced from 21 top-level entries to 4. You don't loose much important functionality, and make the menu finally useful for what it was originally meant to do --- provide quick, no-think access to context-specific features. Big win for speed of use. Best of all, if you allow these menus to be configured, the enterprising user can always put in exact what he uses most often. \n\nNow don't get me wrong --- I love KDE. It's my only desktop. I use KDE's features extensively, to the point where I have it 100% tweeked to work exactly the way I need it to. They'll take away my configurability over my cold dead body. But I don't see any reason not to at least make the default setup more clean, for the sake of efficiency if not ease of use."
    author: "Rayiner Hashem"
  - subject: "I AGREE 1000%!"
    date: 2003-06-05
    body: "That is waht I was saying in my posts all along. KDE does not need to abandon a lot of configurability, only a few options.\n\n\nWhat it does need to do is have smart context menus, better layout of features and ebtter documented features, Users shouldn't even need to see options that are irrelevant or can not be used in the context menu."
    author: "Mario"
  - subject: "Re: What I really want is not features."
    date: 2003-06-05
    body: "I use save link as because kde has io slaves. I can go to another app and give it that url and work with it and then save it back to the server using something like webdav or sftp. Also when I wan to tell a friend about an image and I email them about it I send a link not the image."
    author: "kosh"
  - subject: "Re: What I really want is not features."
    date: 2003-06-05
    body: "Right on!\nSome other things: The throbber on konqueror. I've used it to spawn new windows since kde 1.x and found it very useful. Unlike the throbbers in other browsers, this one doesn't take you to netscape.com or mozilla.org or kde.org. It has a cool function, but this has become a little redundant with tabs coming in the feature set. I rarely, if ever, use it now. It could have some other neat functions like:\n1) The Home button, unlike other browsers, takes you to ~. Of course it is needed in the file manager, but there is place for the Home page too. The throbber could be a place. This way, at least it will behave like other browsers. This could be configurable though.\n2) The throbber indicates activity. How about a stop function during animation and reload during no animation? That would decrease clutter in toolbar, getting rid of two buttons. Sure there are some problems associated with a stop/reload combined button, or so I've heard, but it sounds good to me."
    author: "Rithvik"
  - subject: "Re: What I really want is not features."
    date: 2003-06-05
    body: "Look at KDE CVS head, some of your suggestions are already implemented there afaik."
    author: "Datschge"
  - subject: "Re: What I really want is not features."
    date: 2003-06-05
    body: ">  Look at KDE CVS head, some of your suggestions are already implemented there afaik.\n\nIndeed.. Konq's context menus have already been cleaned up quite a bit-- however, it's toolbars still have not (only apps that should have more than one toolbar in KDE is probably kdevelop and koffice apps.. nothing else should, especially konq)"
    author: "trill"
  - subject: "MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2003-06-04
    body: "Don't be ungrateful for the multitude configurability. That is what seperates KDE from the rest. The power it gives to its users. How many Desktop Environments offer as much configurability as KDE does? None. The fact that I can get KDE looking like MAC in a minute, windows in another, gnome with a few clicks, icewm if I wanted, fluxbox if I tried, UNIX if I felt like it, says wonders about KDE. It is built for the user in mind. It respects our choices, our uniqueness and our differences. It empowers us. See if you'd get that if all those configuration options are removed? Point is, you wouldn't.\n\nThe Woes\nThe KDE KERAMIC Default style is clunky, bulky, bulgy, unnatural and just plan horrible. 'Sexy, Simple, Sleek, Slim, Speed', what ever happened to that interface design philosopy? That was all kde was about until keramic reared it's ugly, slow, unresponsive, memory choking, annoying head in. All that glitters is not Gold, you can achieve much powerful, fast and eloquent user interface designs if we learn to keep it simple, like the latest release of Gnome is doing.\n\nOutside the above, KDE is great and it remains the best! And whoever said GNOME is more functional that KDE most be smoking something, stones perhaps. The only GNOME oriented program I'm jealous of is GIMP. Which reminds me, is there a KIMP package around?"
    author: "Mystilleef"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2003-06-04
    body: "Yes, keramik style looks very clumsy and outdated. I personally prefer dotNet nowadays."
    author: "Janne Kylli\u00f6"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2003-06-04
    body: "Maybe we need a vote on the slickest style out there for kde ?\n\nMy first choice is the \"Alloy\"-style:\nhttp://www.kde-look.org/content/show.php?content=6390\n"
    author: "Thomas"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2003-06-04
    body: "Agreed! Ilike the smooth toolbars and most widgets of Keramik/Keramik II, but buttons are horrific. I wish buttons were smoother/thiner. Something like in the mozilla theme \"Pinball\": http://www.deskmod.com/?state=view&skin_id=9363"
    author: "uga"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2003-06-04
    body: "Someone need to post screenshots of the new Keramik in CVS head, it's much better imo."
    author: "Datschge"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2003-06-05
    body: "I support Kde's look, it makes KDE stand out and most people think it's a nice looking style, myself included. It's not for everyone, but it can be easily changed. The most important thing is people will see a new install of KDE and instantly recognise KDE because of Keramik.\n\nI'd be most upset if Keramik II wasn't adopted as the default style for KDE."
    author: "MxCl"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2003-06-10
    body: "\"The KDE KERAMIC Default style is clunky, bulky, bulgy, unnatural and just plan horrible.\"\n\nI agree. I'm using KDE 3.1.2 with KDE 2 style/icons\n\n\"The only GNOME oriented program I'm jealous of is GIMP.\"\n\nI'm using Abiword 1.9.1 instead of KWord 1.2.1. The RTF import and export filters work better IMHO.\n"
    author: "maxoub"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2004-05-18
    body: "I agree with the poster who says it is what makes KDE what it is. Keramic II,is what should be the new default. Keramic has become what XP's deafault Luna or Mac OSX's Aqua is. It sets it apart from the rest. As for Gnome,it too is great. Using Gnomes graphic login manager,you can change the login screen as much as you can in XP,but for free. And it is the closets thing to looking exactly like Mac OSX when you add the right skin/icons. KDE always retains that clunky,boring Windows95/98 login. Even when another is added,you get that KDE or KDM login manager. Sure,you can add a different wallpaper to it,but it's still boring compared to Gnomes graphic login screens. Once KDE gets rid of the boring login screen,and uses Keramic II,I'll love it."
    author: "KeramicKoffee"
  - subject: "Re: MORE CONFIG, MORE CHOICES, MORE POWER!"
    date: 2004-05-19
    body: "kdm is slated to have gdm theme support in kde 3.3.. see kdenonbeta/kdmthemes in CVS"
    author: "anon"
  - subject: "Konqueror"
    date: 2003-06-04
    body: "When will we get autoreload feature in Konqueror ?\nWaiting, waiting....\n"
    author: "Nux"
  - subject: "Re: Konqueror"
    date: 2003-06-04
    body: "What is autoreload ?\n\nReagrds,\nDawit A."
    author: "Dawit A."
  - subject: "Re: Konqueror"
    date: 2003-06-05
    body: "Autoreload? www.faceoff.com/nhl any game. As the game is played, the score and stats are updated. In konqueror. I use it all the time. It is responsible for most spelling and grammar and html mistakes in kde-cvs-digest.\n\nGo Anaheim.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Konqueror"
    date: 2003-06-05
    body: "Assuming you mean the same thing as I think you do, the answer is... you don't have to wait, just install the Konq plugin I've attached to this post. :-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Konqueror"
    date: 2003-06-05
    body: "*LOL*\n\nI love it..."
    author: "anon"
  - subject: "Re: Konqueror"
    date: 2003-06-06
    body: "An updated version of this plugin is now in cvs in kdeaddons/konq-plugins/autorefresh, enjoy.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "What the hell is it?!! "
    date: 2003-06-06
    body: "Is it automatic refresh of the page afte ra givn amount of time, is it that fun game or .... AHH!"
    author: "Mario"
  - subject: "feature freeze"
    date: 2003-06-04
    body: "I agree that a feature freeze would be good.\nAs mentioned bugs are not fun to work on and doing a feature freeze would force devs to work on them.\n\nI always thought that once the all items in the todo list for 3.2 were green we would get our release\n\nI'm also wondering if a lot of apps shouldn't be sent of to kde apps( eventhough the interface is but ugly in my opinion. they should remove the frames...).\nBy having so many apps it seems you add so many more bugs and delays...\n\nI keep imagining a software item in the control panel where I could select extra components from the KDE project. For exemple in kde multimedia i could remove noatun and select kmplayer and juk... \nBut then I think of so many reasons not to do it :-)\n\n"
    author: "Yann Bouan"
  - subject: "I agree with MOSFET.  Release development-releases"
    date: 2003-06-04
    body: "Quite frankly - I couldn't agree more with Mosfet.  Pulling things from CVS all the time is a pain in the ass if you just want to check things out. Compiling everything every few days is a pain in the ass.\n\nIf KDE could prepare/release an \"in progress\" release once every month - with the only \"requirement\" that it compiles on x86 - then I'm sure some distros would start shipping these in-progress releases.\n\nFurthermore, more people would be inclined to test it.  I know I would.  I don't care if a few features are broken.  \n\nStable releases are well and good.  Don't change the way you release them -- but \"along the way\" releases _REALLY_ should be made.  The way it is now is quite frankly sucky in that aspect."
    author: "arcade"
  - subject: "Re: I agree with MOSFET.  Release development-releases"
    date: 2003-06-04
    body: "Here comes a shameless plug:\n\nGentoo lets me have a stable kde and cvs kde available and working, selectable on login just like selecting gnome or kde.\n\nIf people want an unstable release of kde, then maybe it's up to them to chose a distro that will make it easy for them.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: I agree with MOSFET.  Release development-releases"
    date: 2003-06-04
    body: "What's the easiest way to do that? I want to do it on my Gentoo machine too. :)"
    author: "Apollo Creed"
  - subject: "Re: I agree with MOSFET.  Release development-rele"
    date: 2003-06-04
    body: "Get the ebuilds from http://cvs.gentoo.org/~danarmak/kde-cvs.html\n\nDetailed instructions are there.. it's pretty damn cool too-- let's you have both KDE 3.1 and KDE-cvs within portage. It even lets you pick where you want to put the cvs'd code in, and if you have a KDE cvs account, it even let's you use that, along with ordinary anon-cvs. \n\nThese packages with ccache making getting and maintaining a recent copy of kde-cvs extremely easy within gentoo. It even let's you have qt-copy, qt 3.2-b and qt 3.1 installed at the same time :-)"
    author: "fault"
  - subject: "Re: I agree with MOSFET.  Release development-rele"
    date: 2003-06-04
    body: "Thanks for the pointer! This is just too cool. :)"
    author: "Apollo Creed"
  - subject: "Re: I agree with MOSFET.  Release development-rele"
    date: 2003-06-05
    body: ">qt-copy... installed at the same time...\n\nHow does that work? How to set it up?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: I agree with MOSFET.  Release development-rele"
    date: 2003-06-04
    body: "The thing is that kde project only releases source-- KDE is supported in so many platforms that it's impossible to make packages for every single one. So, in every stable KDE release, third party packagers are given the source early (typically a week ahead or so), to package it. It's a bit of a burden for these packagers to have to package development releases. \n\nIn the gnome side, it works because you have companies like Ximian who employ people to package stuff (such as the dozen different linux flavors supported by redcarpet..) Companies that sponser KDE development, such as SuSE, typically are only concerened with their distro only (and rightly so, why would SuSE start releasing redhat or Solaris packages? :))"
    author: "fault"
  - subject: "Re: I agree with MOSFET.  Release development-rele"
    date: 2003-06-04
    body: "For those who really need to live on the edge, grab yourself a debian unstable then read: <a href=http://opendoorsoftware.com/cgi/http.pl?p=kdecvs;frame=1>KDE CVS HEAD Debian packages</a>\n\n"
    author: "Andy Parkins"
  - subject: "No, please..."
    date: 2003-06-04
    body: "This would mean: making sure it compiles, branching, freezing and pre-packaging, checking it's even coeherent and starting again. Perhaps it could took just some hours or even some minutes a month, but these are hours or minutes much better spent coding and bugfixing!!\nMaybe if anyone offered just to do this, but again who would benefit?\nOn users' side, more users who don't understand programming enough to configure/make coud install some crashing unstable realeas (what a deal!)\nOn developers' side, coders would drown under a lot of quite untrackable bugreports due to the proliferating of different versions, subversions and snapshots.\nJust my 2eurocents."
    author: "Vajsravana"
  - subject: "KDE Releases More Often?"
    date: 2003-06-04
    body: "As a Solaris administrator I'd prefer less frequent releases. KDE isn't as ready for Solaris as it is for Linux which invariably means I have to spend more time compiling the latest release.\n\nFurthermore, there is no good upgrade path. New version means a new directory tree. That means I have to deal with hardcoded file paths in some of the preference files. And then I have to figure out all the changes I made and reimplement those, such as what I moved out of the global autostart, which screensavers I disabled, etc.\n\nAs far as I am concerened the word \"upgrade\" is a misnomer. \"Migrate\" would be more correct and it would be really nice to see a migration tool created to handle this.\n"
    author: "Russ LeBar"
  - subject: "KDE Release Schedule"
    date: 2003-06-04
    body: "Regarding the recent threads of discussion regarding the release schedule of KDE 3.2\n\nFor the most part, I am just an observer. I have tried Linux and KDE and do thoroughly believe that it will be the future and certainly is deserving of use within many workplaces.\n\nMy comments about the release schedule are: KDE may be getting too big to release all at once.\n\nLet me explain. There are many parts of KDE that should be bundled together and released together but there are other parts that could be separate. I remember when Windows was a pretty basic operating system (environment) but with each new release, it has more and more features that were copied or acquired from other developers. With regard to KDE, I think that perhaps for version 4, there should be a separation of certain parts of it, each with their own release schedule. For example, games, KDEPIM, KMail/Kontact and perhaps some other \"packages\" could be split off and released separately.\n\nIn splitting off some of these items, it may enable the KDE core to move more quickly from release to release. Furthermore, it will enable the split off packages to achieve autonomy from KDE and release on their own and not have to wait for the rest of KDE to reach release level. A spin-off from independent releases is that with different packages being released at different times, it will give the appearance that KDE is just-a-hopping with a new release of something or other every couple of weeks. Of course, when the next major release is being prepared, presumably because of major underlying changes, each project may be timed to have a release at the same time.\n\nPlease excuse any ignorance that I may have revealed about my knowledge of KDE: I am just putting in my 2cents.\n\nJules\n"
    author: "Jules"
  - subject: "New directions for KDE"
    date: 2003-06-04
    body: "Thoughts on what's missing, needed and new directions on KDE\n\nI have been reading a discussion on which direction the KDE or new features the KDE should move in on. I choose KDE over the other window managers for one simple reason; better integration. I can see two directions for KDE to move on in; better office integration, hence kontact being the major application on that. KDE lacks something like Evolution; KMail is a very nice tool but has to be integrated with address book, organizer. That will complete the package for a business or a home user enabling it to reach all those commonly accessed tools from one place.\n\nAlthough KDE is still mature, KDE control panel is still too much convoluted. Certain things have to be compacted into one. There is an option for fonts, colors, icons, window decoration, style, themes. A theme should be composed of icons, colors, window decoration + style.  Panels is an unnecessary feature, I doubt many people use it hence should be hidden. I think a couple of months should be devoted to usability study and see what is most commonly used and pushed to the front so that a novice user can gain the most by spending the least time with the control panel.\n\nAlso, integration in konqueror should be in the forefront. Especially the kmplayer should be enhanced as much as possible giving the user a polished feel rather than a clunky working plugin. Right now, I am happy with kmplayer a lot but more can be done.\n\nI would like to thank many developers for putting so much of their time to bring such a package together. Many thanks...\n"
    author: "Ender Wiggin"
  - subject: "Re: New directions for KDE"
    date: 2003-06-05
    body: "Note that kmplayer was original just a demo app for a khtml plugin with javascript bindings, as a kde alternative to nspluginviewer.\nAll that was added later was for enabling mm devices I have at home. And the broadcasting was added so that I can watch tv at diner :-)\nWhat you call clunky look, is of course a personal taste. I just don't like big gui elements.\nBut, quite frankly, I don't believe kmplayer will live after one or two releases. Arts with xine can play as well as mplayer (and maybe even better for real streams) and it's fully intergrated into kde. As soon as apps get released capable of playing from all the sources kmplayer does (qtvision comes into my mind, if it weren't in beta forever, and/or noatun/kaboodle), I stop working on kmplayer."
    author: "Koos"
  - subject: "Re: New directions for KDE"
    date: 2003-06-05
    body: "Panels is definitely very useful. I have a multimonitor setup and being able to set some better settings for the panels under that situation is very nice. I don't like the idea of hiding options since in the end it just makes systems harder to use.\n\nOverall kde is still the only environment I have used that actually works well  with xinerama,"
    author: "kosh"
  - subject: "Re: New directions for KDE"
    date: 2003-06-06
    body: "But if you don't like the panel, aren't there options to hide it? Nothing is visible on my desktop except for my wallpaper. If you think the panel is too cluttered, you can remove and add certain items as you wish, and even configure the behaviour or properties of each item. I did just that. Again, this is the power KDE allows its users, the choice to metamorphosize your desktop into what you want. With regards to KMAIL, I think Kmail should remain what it is,an email client. KDE comes with an address book and an organiser, why would anyone want to further clutter a program when it can run faster and better while it is lean. Not to mention it's more manageable. \n\nTake a look at Microsoft Outlook. Half of the problem encountered by that email client doesn't stem from the email client itself, it stems from redundant and unnecessary addons such as organisers, schedulers, alarms, address books etc. More integration means a slower app, more bugs, and the possbility of a latent security hole. Not something we want in an email client. \n\nLike you, I do agree and hope kmplayer is better integrated into Konqueror and becomes the default media/video player. Aktion and kaboodle are just pathetic. They are an embarrasment to KDE's caliber. They should thrown in the trash can and lost there forever! I think we should spend much more time integrating (k)mplayer, which is much better than Xine, into KDE/Konqueror. If properly implemented, a part of KDE's multimedia woes are over.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: New Direction to KDE"
    date: 2003-06-04
    body: "There is a mailing-list dedicated to KDE usability, why don't you look at the archives ? There was some discussion about themes and I have started to develop a new control center module... Just wait a few weeks and it will be ready."
    author: "Benoit W."
  - subject: "Re: New Direction to KDE"
    date: 2003-06-04
    body: "> new control center module... Just wait a few weeks and it will be ready.\n\naaaahhh! tasty! :-P Willing to see kcontrol broken again ;-)"
    author: "uga"
  - subject: "Re: New Direction to KDE"
    date: 2003-06-04
    body: "???"
    author: "Benoit W."
  - subject: "Re: New Direction to KDE"
    date: 2003-06-04
    body: "Sorry if I offended you or my bad english was uncomprehensive. :-S\n\nWhat I meant is that whenever new features are added to kcontrol something breaks (for example, it's not long that I found kcontrol crashing when I used big icons instead of the small icon tree).\n\nAnyway, I like to see kcontrol broken sometimes. That means somebody is improving it :-)"
    author: "uga"
  - subject: "Re: New Direction to KDE"
    date: 2003-06-04
    body: "The kcontrol shell shouldn't break when some kcmodules are getting tweaked or exchanged, both are pretty independent from each other. Most likely you are talking about compile or packaging problems."
    author: "Datschge"
  - subject: "Re: New Direction to KDE"
    date: 2003-06-04
    body: ">The kcontrol shell shouldn't break when some kcmodules are getting tweaked or >exchanged,\n\nErrr... when I said that \"it breaks\" I didn't mean it broke the compilation or linkage. I meant it breaks the program, i.e., crashes. Obviously, if a module is not dealing properly with memory and segfaults, the whole kcontrol will crash. Well, at least I never saw a module crashing itself and leaving kcontrol still working. Maybe that all those bugs were due to kcontrol then.\n\n>Most likely you are talking about compile or packaging problems.\n\nNo, I'm talking about a recent CVS Head version. That's an upgrade of qt-copy +kdelibs+kdebase on the 24/May/2003 around 1:15 am. ldd reports no reallocation problems, and all apps run fine, but kcontrol has crashes.\n\nI open kcontrol, Icon View mode. No problem. Click System Administration, no problem. Appearance->Theme Manager crashes. The same shortcut *doesn't* crash in tree-view.\n\nSo it seems that there's actually a connection between kcontrol and its modules, since it crashes only with certain modules, and icon view mode in kcontrol.\n"
    author: "uga"
  - subject: "Suggested compromise"
    date: 2003-06-05
    body: "How about something as simple as: feature freeze the last 5 days of a month, coinciding with a big bug-fixing party (real or virtual), and release whatever turns out on the 1st of the next month as a preview release.\n\nThis would allow developers to keep tacking on features all the way until the official feature freeze, but have a dedicated time in which to say \"okay, let's clean up what we have.\" The result doesn't have to be perfect or totally devoid of grave bugs to be an improvement.\n\nApologize if this has already been mentioned but I didn't see it.\n"
    author: "Ken Arnold"
  - subject: "Re: Suggested compromise"
    date: 2003-06-05
    body: "Nice idea, but I don't think it will work, simply because most contributors to KDE do the work they do when they feel like doing it, not when some schedule tells them to do it."
    author: "Datschge"
  - subject: "Re: Suggested compromise"
    date: 2003-06-05
    body: "I think this happens naturally. A bunch of new features will  go in, then everyone will scramble to make it work. Then a new wave, etc.\n\nIt isn't documented, or planned, but happens.\n\nderek"
    author: "Derek Kite"
  - subject: "Re: Suggested compromise"
    date: 2003-06-06
    body: "Bug fixes vs. Features, Features vs. Bug fixes.  And then a false compromise.  :-|\n\nI do not see this as an either or proposition.\n\nI posted this idea before and nobody liked it but:\n\nThe bug fixes and new features should not both be done on the same branch!\n\nBug fixes should be done on a feature frozen branch (i.e. the current release) and new features should be developed in their own space separate from the bug fixes.\n\nAs I said before, new features should accommodate bug fixes no the other way around as it is now done.\n\nWith this paradigm shift: \n\n1.\tSome time would be saved because it would not be necessary to: \"back port\" bug fixes.\n\n2.\tSome additional time would be spent because bug fixes would probably break some new features.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Suggested compromise"
    date: 2003-06-06
    body: "James,\n\nIf I remember correctly, you were the author of the post that I referred to up at top, that I've been avoiding commenting on because it was frankly not really to anyone's benefit for me to do that.  And I'll avoid commenting on that specific post at this point, but I think I would like to make some general observations that may help you in your interactions with the KDE people.\n\nTreat them like people.  If they don't like what you posted, give the reasons that they give serious consideration, and respond to the points as if they've made a valid argument, *even if they haven't*.  Take their point of view seriously, even if you think it's the stupidest thing you've ever heard.  Don't look down on them because they may have less of a formal education of less \"experience\" or what not.  Personally, I don't have a college degree, but I have quite a few years of programming and administration experience under my belt (close to nine at this point), but you'll notice this is really the first time I've mentioned it other than by way of introducing myself.  A lot of these guys are young and just starting out in programming - don't begrudge them the satisfaction of working on code that they believe in.\n\nI would like to preface this with the second point with the caveat that I have been disabled myself and know what it is to want to contribute something - anything - but not being able to.  It isn't a pleasant experience.  But the general culture of open source software tends to be \"put up or shut up\".  You either put in what scratches an itch,ask nicely for someone else to do it (chances are they will if they think it's a good idea), or suffer in silence.  The people who are writing the code - for free, I might add, and in some cases at a great personal sacrifice to their lives, such as they may be - aren't interested in someone who hasn't proven their mettle coming in and telling them how to do it.  Even if they may be more experienced.  This is a meritocracy.\n\nI know that you may think that you have answers and that no one is listening to you.  Step back for a moment and think about *why*.  Is it your approach?  Is it just that you haven't proven yourself?\n\nI applaud your wanting to contribute to the KDE project, in whatever way that you can.  I do think that your approach leaves something to be desired.  Have a *conversation* with these people - and treat them as equals - and I bet you get much farther than you would otherwise.  I have a philosophy in life, myself - even a PhD, even though it may help you to get a better, higher paying job than you would otherwise, still only means that you have proven your ability to bullshit a review board.  Even someone with multple doctorates has to use the bathroom twice a day.\n\nHope this helps.  And if you're not the guy I'm thinking of, sorry.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: Suggested compromise"
    date: 2003-06-06
    body: "I thank you for your thoughtful (and somewhat long) comments.\n\nYes, I plead guilty of making somewhat sarcastic and terse remarks to people that anonymously posted what I thought were inane comments.\n\nYes, some of what I said were \"inside comments\", intended as humor, but probably not take that way by those that didn't get the joke.\n\n> [They] aren't interested in someone who hasn't proven their mettle coming in \n> and telling them how to do it. Even if they may be more experienced. This is \n> a meritocracy.\n\nAnd, I had this strange idea that my ideas might be considered on their merit. :-)\n\nI remain frustrated that there was no actual discussion of any of the actual idea.  Or, did I miss something?\n\nThe only response to my original posting on kde-devel was that I was told -- to coin a phrase -- that it was the stupidest thing they'd ever heard.\n\nI have put the idea out in what I think should be a good forum.  We have had the comments of several users supporting my basic premise that fixing bugs is more important to users than new features.  So, it would be nice to discuss the idea rather than discussing me.\n\nWhat do you think about it?\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: Suggested compromise"
    date: 2003-06-06
    body: "I'm well known for the amount of verbiage that I can spew.  I'm one of those (and probably thankfully) rare people for which each word has a meaning, and different words that are close in meaning can still have subtle shades of meaning that most people don't pick up on or care about.  So in an effort to give the most precise meaning possible to my words, I end up using a lot of them.  *shrug*.  I've grown used to it.  To me it is somewhat similar to a fine musical performance or elegant computer code - just in language.\n\nI think that we have slightly different meanings for the word \"meritocracy\".  Perhaps I used the wrong word in this case.  The way I meant it was \"by he merit of what you produce\" rather then \"by the merit of your ideas\".  Ideas are wonderful but without implementation, they are just ideas, and it's impossible to force volunteers to implement ideas, no matter how well thought our they may be.  There are several programs that I would like to see, myself, but I don't even bother asking for them, as I realize that if I don't code them myself they will not get coded.  That's regardless of how useful they would be.\n\nAnyway, what do I think of your proposal.  I'm not sure what I think of it.  I am a developer on one level, yes, but I haven't the experience with large projects that most of the other KDE developers to - my experience tends to be as a \"lone ranger\" type coder.  So I really can't give you any constructive feedback.  As you can probably tell, I feel my strengths lie somewhat more in the \"people\" area, and fortunately, I've found a way to contribute using that without seriously rankling people in the process :-)  I know what I want in KDE, I can help put it there if I feel strongly enough about it, but I'm not particularly interested in all of the guts of project management.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: Suggested compromise"
    date: 2003-06-06
    body: "> I think that we have slightly different meanings for the word \n> \"meritocracy\". Perhaps I used the wrong word in this case.\n\nI was just making a facetious remark.  Your meaning is basically correct as far as M-W is concerned:\n\n1 : a system in which the talented are chosen and moved ahead on the basis of their achievement\n\n2 : leadership selected on the basis of intellectual criteria\n\nThe problem with this is that this definition doesn't rule out the achievement of a college education.  \n\nSo, the meritocratic system you describe would be a special subset of a meritocracy for which there is probably not a specific word.\n\nMy point here is that if someone says that my college education doesn't matter because this is a 'meritocracy' they are saying something that may make sense to them, but strictly speaking they are actually contradicting themselves.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Suggested compromise"
    date: 2003-06-06
    body: "You are probably correct.\n\nI propose we coin the term \"codocracy\" :-)\n\n--Russell"
    author: "Russell Miller"
  - subject: "kde traffic 53"
    date: 2003-06-09
    body: "In my office environment, we work daily with most of the Outlook features. A switch to KDE-Linux is only possible if the promised kroupware integration and Exchange comptitbility is there.\n\nIn short: forget about success on our deskop until 3.2 is there and stable. It seems to me urgent and important. A feature freeze seems to me necessary. If there are problems with kroupware, as this is a priority for success, they should be sorted out before new gadgets are added. \n\nPlease thanks for all the wonderfull improvements and staility added since 2.0. \n\nGeert"
    author: "Geert "
---
A slick <a href="http://kt.zork.net/kde/kde20030602_53.html">KDE Traffic #53</a> is out.  This one features a single long-standing topic, <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">the release cycle of KDE 3.2</a>. Read all about it at <a href="http://kt.zork.net/kde/latest.html">the usual location</a>.
<!--break-->
