---
title: "New Mailing List \"kdepim-users\""
date:    2003-12-14
authors:
  - "kstaerk"
slug:    new-mailing-list-kdepim-users
comments:
  - subject: "New page \"third party software\" "
    date: 2003-12-14
    body: "Also new as of today is <a href=\"http://pim.kde.org/components/third_party.php\">http://pim.kde.org/components/third_party.php</a>"
    author: "Anonymous"
  - subject: "Re: New kde-pim mailing-list"
    date: 2003-12-14
    body: "I'm not sure if this is a good idea - \nthere is e.g. kde-linux around where users discuss all kinds of\nkde-apps including kmail etc., and this list has not so much traffic.\nso why further divide the audience into one more list with a subtopic ?\n"
    author: "hoernerfranz"
  - subject: "Re: New kde-pim mailing-list"
    date: 2003-12-14
    body: "The list is taking over for the kmail list which was getting both user and developer mails.  The kmail list is now replaced with kmail-devel and the kpim list will receive the user emails."
    author: "anon"
  - subject: "Re: New kde-pim mailing-list"
    date: 2003-12-15
    body: "I claim that a mailing list which has a less general topic than \"All of KDE\" will attract more people who are willing to help. For example most KDE PIM developers are already subscribed to kdepim-users and will help with answering KDE PIM related questions. Most of those developers are not subscribed to the general KDE user mailing list (for one or the other reason). I browsed the KDE user mailing lists (kde, kde-linux and kde-de) for a few months, but in the end it did simply cost too much time to filter out all the irrelevant questions that I couldn't or didn't want to answer, so that I had to stop reading those mailing lists. With kdepim-users I only have to browse one mailing list and a higher percentage of the questions will be KMail-related."
    author: "Ingo Kl\u00f6cker"
  - subject: "What I would post..."
    date: 2003-12-14
    body: "... is wether IMAP filtering is supposed to already work in beta 2? I want to move messages into IMAP folders with filters, but I can seemingly can only choose local ones.\n\nI remember reading about it in digest, anyone know the status?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: What I would post..."
    date: 2003-12-15
    body: "why should everything be built into kmail, making it fat and bloated ?\njust use what is already there -\nfetchmail/procmail !"
    author: "hoernerfranz"
  - subject: "Re: What I would post..."
    date: 2003-12-15
    body: "With normal (online) IMAP: Neither client-side filtering nor IMAP folders as target for filters will be in KDE 3.2.\n\nWith the new disconnected (offline) IMAP: All incoming mail in your INBOX (of the disc. IMAP account) will be filtered by KMail. I currently can't check whether it's possible to select folders on the disc. IMAP account as target for filters."
    author: "Ingo Kl\u00f6cker"
---
<a href="http://pim.kde.org/">pim.kde.org</a> <a href="http://pim.kde.org/news/news-2003.php#28">reports</a> that a <a href="http://pim.kde.org/contact/user_mailinglist.php">new mailing list kdepim-users</a> for discussion about the usage of the KDE PIM applications (<a href="http://kontact.kde.org/">Kontact</a>, <a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a>, <a href="http://kmail.kde.org/">KMail</a>, <a href="http://korganizer.kde.org/">KOrganizer</a> and everything else in the kdepim CVS module) has been created. Users who want to help with user support for the KDE PIM applications are very welcome to the <a href="https://mail.kde.org/mailman/listinfo/kdepim-users">new mailinglist</a>. Your help is very much appreciated by the developers.

<!--break-->
