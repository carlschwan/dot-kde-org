---
title: "Aaron J. Seigo: Adding WhatsThis Help To KDE Applications"
date:    2003-08-18
authors:
  - "aseigo"
slug:    aaron-j-seigo-adding-whatsthis-help-kde-applications
comments:
  - subject: "Congratulations"
    date: 2003-08-18
    body: "Congratulations for a great tutorial Aaron!\n\nCheers,\nThorsten"
    author: "Thorsten"
  - subject: "Non-Programmer's Guide to Participating in "
    date: 2003-08-18
    body: "Useful!\n\nWhy doesn't KDE provide a Wiki structure for documentation and a distributed framework for documentation translation (\"grid translation\").\n\nnon-programmer contributors don't know CVS and don't want to learn how to handle it. they will probably be even unable to use ftp and understand the docbook-format"
    author: "Gerd"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-18
    body: "And programmers are ungifted in documentation issues.\n\nSi! A framwork for easy contributions is needed. However Aaron's article again teaches how to use CVS ecc. in the programmers way (\"To get started, visit the KDE anonymous CVS documentation\"). Cough! Programmers will never learn it. \n\nContributors don't want to learn how a programmer likes to work. This documentation is only usable for those who like to become a programmer."
    author: "Kai"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-18
    body: "Consider the ability to follow directions to use CVS as a test of your ability to contribute to the project.\n\nIt isn't really that hard, but it requires a little thought and understanding of how the project works.\n\nIf a person is willing to make the effort, invest a little time and energy into learning the process, then they may be trusted with some responsability.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-19
    body: "Not ALL programmers do bad documenting, most. I do think however you are right about Aeron's article. As far as I know, via KDE one can easely start using CVS with a GUI.\n\nBe proud of this CVS GUI and promote that instead of command line instructions (in this case)."
    author: "AC"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-19
    body: "i did mention cervisia in the article. the command line stuff is very simple, however, and i didn't want to go into a huge tangent on how to set up cervisia and use it properly. if you want to provide such a patch to the article complete with screenshots, please do so and i'll add it and give you credit.\n\np.s. while i like the 1950's-era aerodynamic vehicle of the future ring that \"Aeron\" has to it, my name is actually Aaron ;-)\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-20
    body: "Sorry about that name.. I actually think Aeron is from a Final Fantasy game? Anyway, ironicly, I also use the command prompt myself and wouldnt even know how to use the GUI at this moment, so I wouldnt be much help Aaron!"
    author: "AC"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-20
    body: "No, Aeron is the famous Hermann-Miller chair, the symbol for DotComs that spent too much of their VC's money and thus were doomed from the beginning."
    author: "AC"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-20
    body: "yeah, but DAMN those chairs are awesome to sit in! especially when all you do most of the day is sit. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-22
    body: "What is so hard to set up in Cervisia? I think a quick guide to set it up and use would be appreciate by non programmers. I'm a programmer, but I also appreciate a graphical CVS client. We should promote Cervisia. KDE is a desktop environment after all, not (only) a place from where you start an xterm and do the rest of things. ;-) Translators also tend to use KBabel, web page writers Quanta (hopefully) and so on. Of course, there are old habbits and favourite apps, many KDE core developers may use non-KDE apps for everyday work, but in any case we should promote KDE software whenever it is available, and especially if it's good enough.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Non-Programmer's Guide to Participating in "
    date: 2003-08-19
    body: "> (\"To get started, visit the KDE anonymous CVS documentation\")\n\nyes, and then i go through it step-by-step so you can cut'n'paste your way through it. you don't really need to visit the KDE anoncvs docu, because it's all there in the article! but if you are curious, you *can* visit the documentation for more information.\n\nsee, this article isn't meant to be a \"Slobbering Idiot's Guide To Placing Random Text Into KDE\" it's meant to be something more on par with a high school level tutorial. if it was possible to do these things with no effort and no thought whatsoever, i wouldn't've had to write a tutorial in the first place, now would I? think about it.\n\nto be honest, any half-motivated, computer literate 12 year old could follow that article and produce results.\n\nnow here's the part where i rant maniacally (as opposed to the above;):\n\nit's the \"suport\" of people like you that makes venturing out into the public eye so very difficult for me to handle. btw, where's your amazingly useful and artfully written article for me to read and learn the Tao of writing from? and no, half-baked criticisms on web boards most definitely do not count."
    author: "Aaron J. Seigo"
  - subject: "This helps me help!"
    date: 2003-08-18
    body: "Thanks for that! I was looking into means to contribute \"What's this?\" bits and pieces anyway. So this is definitely s.th. I will look into. To have a *complete*, and I *mean* complete, 100%, working \"What's this?\" for *all* apps will bring KDE    much closer to a viable corporate-used desktop system than anything else...\n\nCheer,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Wow, a great effort"
    date: 2003-08-18
    body: "It is an impressive tutorial, and it makes sence to me as a programmer.. but i dont see anyone putting in that much effort each..  there is alot of stuff there that a small webscript can smplify (the 80 char's a line stuff for instance)\n\ni agree with the other posters, and think that putting this effort into a web script to collect these strings, and a system to float the good ones to the top so programmers can enter them into CVS would be a better use of time..\n\nActually.. i take back some of what i said.. Parts of this tutorial are not misguided at all.., and incombination with a webscript idea could provide a really good soution i think...\n\nsorry if some of this dosnt make sence, ive just felt the flu coming on, and im about to go lie down."
    author: "Robert Guthrie"
  - subject: "Help?"
    date: 2003-08-18
    body: "Good stuff, altough perhaps still a bit too much from a programmers perspective, like said above.\nStill a good try though.\n\n>> It tends to be more verbose than a tooltip, but more succinct and specific than a full-blown help manual.\n\nI actually never ever use that silly help button that unnecessarily adds bloat to every dialog.\n\nIf I want to know what a specific UI part does, it makes no sense to browse through a complete manual (I already forgot what I was looking for when I finally found the text that explains what the UI does).\n\nIf only that button could be removed from the dialogs.. /me would be a happy KDE user.\n"
    author: "konqi"
  - subject: "Re: Help?"
    date: 2003-08-18
    body: "> If only that button could be removed from the dialogs\n\nlook in the window decorations control panel. you can customize which buttons are shown for many (most?) kde window decorations. just remove the WhatsThis help button and voila, you're a happy KDE user. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Help?"
    date: 2003-08-18
    body: "Actually, I rather meant those 'Help' buttons in the dialogs, not window decorations, like in the following screenshot: http://urbanlizard.com/~aseigo/whatsthis_tutorial/whatsthis_example_large.png\n\nBut thanks, didn't realize it was possible to remove them from the win decs, so I'm already a tiny bit more happy KDE user now :)\n"
    author: "konqi"
  - subject: "Don't remove the help!"
    date: 2003-08-23
    body: "Please no!! The Help is only in the UI when there is no menu to put it in, I agree it could be put in the context menu instead, but most KDE applications already do this and changing would be too much work, besides I don't need the help every time in my context menu."
    author: "Mario"
  - subject: "programmer oriented?"
    date: 2003-08-18
    body: "so, several people have noted that they think it's still all too programmer oriented. well, it's not going to get much clearer than what's there. WhatsThis help is embedded RIGHT IN the application. it isn't some external file. so you need to enter it RIGHT IN the application's source code. fortunately we have UI files to blunt that pain. =)\n\nas for using CVS, it's not very hard. all the commands are right there to copy 'n paste, and there's even a GUI for it, as noted in one of the Tips. CVS is simply the only practical way to make patches and have the latest code. the effort required to get CVS going to do WhatsThis is minimal compared to the effort of actually writing the WhatsThis help.\n\nas for watching how you format your lines when editting source code files, well, that's just an unfortunate detail. if wrapping at 80 characters is beyond you, or you can't be bothered to try even a little bit, stick to the UI files where it's all handled for you.\n\nas for using a webscript, i'll just repeat this: it's embedded in the applications!\n\nthere is a UI-files-from-CVS web-based browser in the works, but it isn't ready for prime time yet. once that is done, the you can grab UI files using your web browsing and edit them from there. but this still doesn't help with making the patches or ensuring they are up-to-date when you finish.\n\nhonestly, if it's all too much effort for you, that's cool. don't do anything. just continue to be a user that takes what is made for you. i'm not trying to convert the entire world into being KDE contributors. but there ARE people out there who want to get involved, and these sorts of things are exactly what they have been asking for.\n\nof course, i'd be happy to see a tutorial showing how to do WhatsThis that meets the \"doesn't suck because it wasn't written by a programmer\" standards. ;-)\n\nalso note that some of the future tutorials in this series will not have anything to do with the source code... future topics may include bug triage, icons and other art, documentation writing, translating, user support, advocacy... bug triage is likely the next one, actually..."
    author: "Aaron J. Seigo"
  - subject: "Re: programmer oriented?"
    date: 2003-08-18
    body: "> as for using a webscript, i'll just repeat this: it's embedded in the applications!\n \nHow about a webscript that automatically emails a mailing list.. that way non-programmers won't have to mess with the hard to use software that is CVS"
    author: "anon"
  - subject: "Re: programmer oriented?"
    date: 2003-08-18
    body: ">  How about a webscript that automatically emails a mailing list.. that way non-programmers won't have to mess with the hard to use software that is CVS\n\nHow exactly is CVS hard? Remembering commands, options and switches? Okay, but conceptually it's easy. Check out a repository (or if you're starting one check it in) then to get the latest update and to send your changes commit. To add files you add. \n\nKDE has Cervisia in the kdesdk module and it makes CVS so easy it's not even funny. Before anyone tells me \"but you're a developer\" you should know I was not doing application development when I discovered CVS and using it to submit artwork or tag files to Quanta scared me. Then I discovered Cervisia and my world changed...\n\nIt takes about two commands to set up a local repository. Suddenly I began putting web development projects, notes, spreadsheets and everything in a CVS repository. Why? Because it's incredibly easy with Cervisia. I showed my wife and she started putting journal notes in CVS. CVS is not only dead easy with Cervisia, but there isn't any silly web interface that has the functionality, unless it hooks up to a CVS server. the odds are excellent that something you are doing now would benefit from your own CVS. It's not just developers that need to manage files with versioning.\n\nLog in once, open Cervisia and point and click, review changed files, diff changes so you can understand what was done... Cervisia and KCompare are great tools that are easy to use... much easier than complaining CVS is hard. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: programmer oriented?"
    date: 2003-08-18
    body: "********\n\nso, several people have noted that they think it's still all too programmer oriented. well, it's not going to get much clearer than what's there. WhatsThis help is embedded RIGHT IN the application. it isn't some external file. so you need to enter it RIGHT IN the application's source code. fortunately we have UI files to blunt that pain. =)\n\n********\n\nYup, that's pretty much the bottom line. Editing Designer files is relatively easy -- deciding which hand-created object needs to have the What's This? tag added to it is going to require the novice to follow the C++ code at least superficially. God help them when they run into objects created as a QWidget instead of a QWidget *. ;-)\n\nStill, there's a large contingent that spends a ton of time messing with source even if they don't code. This will be a good learning experience for them and the Qt class hierarchy is so elegant, it's a great way to start."
    author: "Otter"
  - subject: "German Translation"
    date: 2003-08-18
    body: "Is it planned to translate this tutorial ?\nIt would very nice !\nOr is there a way that people don`t speak english to help the kde projekt?\n"
    author: "German"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "KDE's source code is written in \"US English\". From there it gets translated to numerous languages, including German. So to add tooltips you will need to\nbe able to write English reasonably well. If you don't, you will still be able to help by translating the english tooltips to e.g. German if you are at least able to read English reasonably well.\n<p>\nIf you can't read English you can still help KDE by promoting KDE in your own language, e.g. by presenting it at expositions or trade shows or by answering KDE questions on language specific mailinglists.\n<p>\nIf you want to help with translations you can contact the team coordinator for the language you are interested in. See\n<a href=\"http://i18n.kde.org/teams\">http://i18n.kde.org/teams</a> for a list of all translation teams and \n<a href=\"http://i18n.kde.org/teams/index.php?action=info&team=de\">http://i18n.kde.org/teams/index.php?action=info&team=de</a> for information about the German translation team.\n<p>\nCheers,<br>\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "KDE's source code is written in \"US English\"\n\nTypisch deutsch. Sobald ein einziger Spanier, der deutsch kann, im Raum ist, sprechen alle Englisch. Ich sch\u00e4tze, wenn jeder seinen Kram in seiner Sprache gepflegt h\u00e4tte und dann \u00fcbersetzt wird, w\u00e4ren wir schon weiter. Ich kann in deutsch in der Zeit wo ich auf Englisch was formuliere f\u00fcnf Texte schreiben. Wenn man dann einen ins Englische \u00fcbersetzt, sind es immer noch 4 mehr...\n\naber so wie es jetzt ist, hinkt wohl eher die deutsche KDE-Seite hinterher. \n\nNaja. Sicherlich hat der zum Teil traurige Zustand der Doku auch damit zu tun, dass f\u00fcr viele Entwickler US-Englisch nicht die Muttersprache ist. Da fasst man sich dann halt lieber kurz."
    author: "Wurzelgeist"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "Bitte? *\n\n\n\n\n* I don't speak Bratwurst-language\n"
    author: "Das Koole Desktop?"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "Most Germans would write \"der koole Desktop\". ;-)\nI don't know why it is used as male in German, perhaps because of \"der Schreibtisch\"."
    author: "CE"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "Like I said, I really don't speak German :)\n"
    author: "schnitzelkopf@dashei\u00dfePost.com"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "No jo, souhlasim. Od zitrka zacinam psat vsechno v cestine. Vsak vy si s tim nejak poradite."
    author: "L.Lunak"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "\u00d0\u00af \u00d0\u00b4\u00d1\u0083\u00d0\u00bc\u00d0\u00b0\u00d1\u008e \u00d1\u0087\u00d1\u0082\u00d0\u00be \u00d0\u00b2\u00d1\u0081\u00d1\u0091-\u00d1\u0082\u00d0\u00b0\u00d0\u00ba\u00d0\u00b8 \u00d0\u00b5\u00d1\u0081\u00d1\u0082\u00d1\u008c \u00d1\u0081\u00d0\u00bc\u00d1\u008b\u00d1\u0081\u00d0\u00bb \u00d0\u00bf\u00d0\u00b5\u00d1\u0080\u00d0\u00b5\u00d0\u00b9\u00d1\u0082\u00d0\u00b8 \u00d0\u00bd\u00d0\u00b0 \u00d0\u00a0\u00d1\u0083\u00d1\u0081\u00d1\u0081\u00d0\u00ba\u00d0\u00b8\u00d0\u00b9 -- \u00d0\u00b2\u00d0\u00b5\u00d0\u00b4\u00d1\u008c \u00d0\u00bd\u00d0\u00b5\u00d0\u00bb\u00d1\u008c\u00d0\u00b7\u00d1\u008f \u00d0\u00b6\u00d0\u00b5 \u00d0\u00be\u00d0\u00b6\u00d0\u00b8\u00d0\u00b4\u00d0\u00b0\u00d1\u0082\u00d1\u008c \u00d1\u0087\u00d1\u0082\u00d0\u00be \u00d0\u00bb\u00d1\u008e\u00d0\u00b4\u00d0\u00b8 \u00d0\u00b1\u00d1\u0083\u00d0\u00b4\u00d1\u0083\u00d1\u0082 \u00d0\u00bf\u00d0\u00be\u00d0\u00bb\u00d1\u008c\u00d0\u00b7\u00d0\u00be\u00d0\u00b2\u00d0\u00b0\u00d1\u0082\u00d1\u008c\u00d1\u0081\u00d1\u008f \u00d0\u00b8\u00d0\u00bd\u00d0\u00be\u00d1\u0081\u00d1\u0082\u00d1\u0080\u00d0\u00b0\u00d0\u00bd\u00d0\u00bd\u00d1\u008b\u00d0\u00bc \u00d0\u00b0\u00d0\u00bb\u00d1\u0084\u00d0\u00b0\u00d0\u00b2\u00d0\u00b8\u00d1\u0082\u00d0\u00be\u00d0\u00bc."
    author: "SadEagle"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "The above is utf8, BTW\n"
    author: "SadEagle"
  - subject: "Re: German Translation"
    date: 2003-08-19
    body: "001001001001101001001001001011001001111011001010010011010010\n011101101001000010011110100100100111010100100100111011001001\n001110100100100100100110100100100100101101100100111101100101\n001001101001001110110100100001001111010010010011101010010010\n011101100100100111010010010010010011010010010010010110110010\n001010010011010010011101101001000010011110100100100111010010\n010010011010010010010001001001001110110010010011101001001001\n0010011010010010010010110110010011110110010100100110100"
    author: "AC"
  - subject: "Re: German Translation"
    date: 2003-08-19
    body: "You are a shame for all anonymous users (and also for Alan Cox)."
    author: "AC"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "When I see that I think it's good that Russian uses cyrillic letters."
    author: "CE"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "that's not russian"
    author: "ac"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "Did I state that?\nBut it's a slawonian language.\nAnd I think these are more readable with cyrillic letters."
    author: "CE"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "\u00e4rgere dich blo\u00df nicht allzusehr dr\u00fcber.  Ich bin damals teils zu KDE gekommen, weil ich meine Deutschkenntnisse nicht verlieren wollte, und dachte ich konnte den tapferen KDE-leuten helfen, ihr Projekt ins Englische zu ubersetzen.  Ich kam mir ziemlich dumm vor, als ich mich bei der i18n Mailinglist meldete und fand, da\u00df beinah alles schon auf US-English lief.  \n\nKurz zusammengefasst: Du darfst dich in deine Strings beliebig schlecht ausdrucken, es gibt immer welche, die beim Korrigieren froh werden.\n\nWill\n(IRC: Bille)"
    author: "Will Stephenson"
  - subject: "Re: German Translation"
    date: 2003-08-18
    body: "The problem is that many people around the world are working on the same applications. It is not realistic to use more than one language.\nEnglish is the language that every serious programmer can speak - and has to speak, because most documentation, API references, specifications etc are not available in any other language. \nEven if the programmer feels more comfortable writing in his native language (and I do), it would exclude so many people from contributing that this would easily outweight any disadvantage in the documentation.\n\nAnd, BTW, I don't think that there are many people capable to translate from German or any other non-English language to their native language. So it would not even work for the documentation.\n\n"
    author: "Tim Jansen"
  - subject: "Great Article!"
    date: 2003-08-18
    body: "This is a ridiculously useful article that has already had some broad appeal to non-programmers.  Many non-programmers I know want to _do_ development but don't feel like they are actually part of the development process by writing howto's and making websites.  This gives users a way to actually submit patches, make diffs, and get credit for code changes.. \n\nThis little tutorial may do more to help out KDE than the last day or two of bug fixes.  Thanks for adding hundreds to the pool of possible KDE developers.  \n\nStrid..."
    author: "Strider"
  - subject: "Re: Great Article!"
    date: 2003-08-18
    body: "yes, great article. I often meet in IRC users that are willing to learn to compile from source for whatever reason. It's not much more difficult to buid KDE from cvs and with this neat tutorial, non-developers will be able to give something to KDE. The What'sThis help is very important and as developers, we tend to forget that. I encourage people to follow the instructions in the tutorial and to be part of KDE. On IRC (#kde, #kde-devel) you will always find some help if you are stuck.\n\n\nAt your keyboard, 3, 2, 1...ready... go!"
    author: "annma"
  - subject: "kde webpage"
    date: 2003-08-18
    body: "wouldn't people like it to be as slick as gnome.org\nor at least as es.kde.org"
    author: "KDE User"
  - subject: "Re: kde webpage"
    date: 2003-08-18
    body: "The GNOME frontpage is cool, but the inner pages are a bit lackluster. The KDE frontpage is lackluster, but it's inner content pages are cool. I think the KDE front page should probably put less content in the front page and try to get it to fit into one page length (like the yahoo of olden times).. it seems a bit confusing."
    author: "anon"
  - subject: "Re: kde webpage"
    date: 2003-08-18
    body: "The kde frontpage just need a little more graphics, less blockness, maybe more colour.\n\nBut the content is good and fresh!"
    author: "ac"
  - subject: "Re: kde webpage"
    date: 2003-08-20
    body: "yeah, they are just starting the site redesign as we speak, they started with the front page but havnt gotten to most of the rest of the site."
    author: "dude"
  - subject: "Re: kde webpage"
    date: 2003-08-18
    body: "how is this related to the WhatsThis article?"
    author: "Aaron J. Seigo"
  - subject: "Re: kde webpage"
    date: 2003-08-18
    body: "> how is this related to the WhatsThis article?\n\nYou don't know? ;-) It's how they got here... and their own little forum that will subsume all discussions here to be the most popular talkback on your excellent story, hereafter referred to as \"the KDE Front Page story\".\n\nOkay... scratch one user concentrating long enough to finish the tutorial. Next up, first person shooters... ;-)\n\nBTW I skimmed it real quick. Excellent stuff, which I'd expect. I'll read it again later when I can savor it.\n\nCheers"
    author: "Eric Laffoon"
  - subject: "Re: kde webpage"
    date: 2003-08-18
    body: "Obviously you haven't visited the dot recently! :)"
    author: "anon"
  - subject: "Re: kde webpage"
    date: 2003-08-19
    body: "well, there's so much stuff on the webpage so it's hard to know\n\"what things are\". but maybe \"what's this\" popups on the webpage\nwould be too much."
    author: "kde user"
  - subject: "Thanks for this great article/howto"
    date: 2003-08-20
    body: "See title, just want to show my appreciation.\n"
    author: "John Herdy"
  - subject: "Thanks for this great article/howto"
    date: 2003-08-20
    body: "See title, just want to show my appreciation.\n"
    author: "John Herdy"
  - subject: "GREAT GUIDE!"
    date: 2003-08-23
    body: "This is one area where KD Eis clearly lacking and now anybody can add more helpful What'sThisHelp to KDE with a bit of work.\n\nThank you!\n\nBTW: While this wish http://bugs.kde.org/show_bug.cgi?id=59859 would not be encessary if every part of KDE's inteface had a What'sThis tooltip, that probably won't happen so we still need a way to make the user know wher ethe tooltips are. For example when he clicks the tooltip icon and he hovers over widgets with tooltips, his mouse cursor should change so he would know that there is a tooltip there.\n\nVote for this bug: http://bugs.kde.org/show_bug.cgi?id=59859 and maybe Trolltech will fix it."
    author: "Mario"
---
<a href="http://urbanlizard.com/~aseigo/whatsthis_tutorial/"><em>Adding WhatsThis Help To KDE Applications</em></a> is the first installment in the <em>Non-Programmer's Guide to Participating in KDE</em> tutorial series. This series is designed to aid those who would like to participate in the KDE project, but for one reason or another can't do so by contributing source code. Fortunately, there are many tasks in KDE that don't involve writing code, and many of them don't require much investment in the way of time, either. Adding WhatsThis help in KDE is one such task.
<!--break-->
<p>WhatsThis help is essentially an on-demand, extended tooltip associated with a specific widget. It tends to be more verbose than a tooltip, but more succinct and specific than a full-blown help manual. To learn how to help improve KDE by adding WhatsThis entries where they are missing, check out <a href="http://urbanlizard.com/~aseigo/whatsthis_tutorial/">the tutorial</a>!