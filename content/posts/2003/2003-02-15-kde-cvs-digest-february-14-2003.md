---
title: "KDE-CVS-Digest for February 14, 2003"
date:    2003-02-15
authors:
  - "dkite"
slug:    kde-cvs-digest-february-14-2003
comments:
  - subject: "We luv ya. ;)"
    date: 2003-02-15
    body: "Thanks, Derek. Good work, as usual.\n\nAnything new on Keramik II? Anyone? :)"
    author: "Eike Hein"
  - subject: "Re: We luv ya. ;)"
    date: 2003-02-15
    body: "Keramik II Screenshots:\nhttp://garaged.homeip.net/screenshots/\n\n--\n\n\nDevCounter (http://devcounter.berlios.de/) - An open, free & independent developer pool\ncreated to help developers find other developers, help, testers and new project members."
    author: "Masato"
  - subject: "Re: We luv ya. ;)"
    date: 2003-02-16
    body: "What is different in keramik II ?"
    author: "pnut"
  - subject: "Re: We luv ya. ;)"
    date: 2003-02-20
    body: "For example the current active tab is drawn so that it differs more from the inactive ones."
    author: "Anonymous"
  - subject: "Mistake"
    date: 2003-02-15
    body: "Need to change 7 by 14 here :\nhttp://members.shaw.ca/dkite/index.html \n\nThanks a lot for this KDE CVS review. Now I can dream of what my KDE will be ;)"
    author: "Shift"
  - subject: "Re: Mistake"
    date: 2003-02-16
    body: "Fixed. Thanks.\n\nYes. 3.2 will be very good. Frankly, I'm amazed at the pace of development in so many different areas.\n\nDerek"
    author: "Derek Kite"
  - subject: "Shouldn't spellchecking be global?"
    date: 2003-02-15
    body: "> Zack Rusin committed a change to kdepim/kmail\n\n> Read an article from some dude whose\n> biggest complain about KMail was that\n> the spellchecker doesn't recognize the\n> word KMail so make it recognize KMail\n> and a couple of other key KDE components.\n\nShouldn't this go into KSpell rather than KMail?"
    author: "Jiffy"
  - subject: "Re: Shouldn't spellchecking be global?"
    date: 2003-02-16
    body: "I noticed that too.  Why would KMail be a valid word in KMail but not KWord?  The commit notes may have been inaccurate, but this certainly seems kindof wrong."
    author: "ac"
  - subject: "Re: Shouldn't spellchecking be global?"
    date: 2003-02-16
    body: "According to the diffs, it definately went into KMail.  Judging by the code, the fix was obviously a quick hack.  A more appropriate solution would have KSpell read KDE related words out of a text file.  The file could be readily expanded and translated to other languages.\n\nAnother idea might be to parse all the internationalization strings in KDE, and put every single word into the spell checker's personalized word list.  But then you would end up with all of the translators' typos and spelling errors.\n"
    author: "Jiffy"
  - subject: "Re: Shouldn't spellchecking be global?"
    date: 2003-02-16
    body: "You may want to read this:\nhttp://lists.kde.org/?l=kde-cvs&m=104466564420683&w=2\n\nThat address the same question.\n"
    author: "Sad Eagle"
  - subject: "syncml"
    date: 2003-02-16
    body: "a little off-topic, but I'm wondering why kde (still) doesn't support syncing the address-book with syncml-devices.\n\nThere are quite a lot of devices already that support this open and free standard (like the SonyEricsson T68i and the P800), so I'm really wondering whether nobody has scratched their itch yet and written some software for that.\n\nAlso, how do Qtopia-Devices sync? The zaurus for example, does it use syncml or did trolltech come up with another new way on synchronizing contacts?\n\nSomeone in kde must have thought about this before, are there any documents or threads on a mailinglist about that? IIRC, even the kroupware project is only thinking about adding palm-sync support."
    author: "anonymous"
  - subject: "Re: syncml"
    date: 2003-02-16
    body: "You will see a syncing app in KDE 3.2. It is called KitchenSync. The OPIE-\nproject released a new version some days ago. You can help us by\ntesting this tarball:\n\nhttp://www.handhelds.org/~zecke/kitchensync_alpha3.tar.gz\n\nYou'll find a HOWTO here:\nhttp://handhelds.org/~zecke/kitchensync.html\n\nFor SyncML: There might be patent-issues with the implementation. The\nSyncML-group is not answering to questions concerning the free\nimplemention. \n\n__\nCarsten Niehaus\nProject OPIE- the Open Palmtop Integrated Environment\nhttp://opie.handhelds.org | http://www.opie.info (german)\nIRC: irc.freenode.net #opie #opie.de"
    author: "Carsten Niehaus"
  - subject: "Re: syncml"
    date: 2003-02-16
    body: "Thanks for your answer, Carsten!\n\nYou didn't mention it in your reply, but the screenshot on the howto-page implies that kitchensync can currently only sync with QTopia and OPIE-Devices. Is that true? If yes, I can't really test it, since I currently only have one WinCE-2.11 device and 'almost' one syncML-Device (the SE P800 once it is available, probably next week)."
    author: "anonymous"
  - subject: "Re: syncml"
    date: 2003-02-16
    body: "I'm currently updating the HOWTO. In fact, I commited about 5 minutes ago.\nKitchenSync is a \"general-purpose\"-application and is NOT limited to \nKDE-apps on the Desktop-side and NOT limited to OPIE-apps (or\nZaurus/Sharp-apps). You only need a so called konnector (you could\nsay: a filter) to sync with other apps. We are currently working on syncing with\nKAddressBook/KOrganizer and Evolution. The more help we get the more\nfilters will be written.\n\n-- \nCarsten Niehaus\nProject OPIE- the Open Palmtop Integrated Environment\nhttp://opie.handhelds.org | http://www.opie.info (german)\nIRC: irc.freenode.net #opie #opie.de"
    author: "Carsten Niehaus"
  - subject: "Re: syncml"
    date: 2003-03-06
    body: "Well i think there is a syncml client called multisync. can be found on multisync.sf.net not really shure though. At least they are implementing some p800 support.\nMaybe asking them how they did that might be a solution to that problem. \nbtw. they are trying to write a opie sync. plugin and could need some help with that..(but that is quiet OT here).\n\n"
    author: "jk"
  - subject: "Re: syncml & IPR/Licensing"
    date: 2003-10-14
    body: "Hi,\n\nFrom the information on the SyncML home pages on OpenAlliance (http://www.openmobilealliance.org/syncml/) it seems that there is no problem in using the current SyncML standard. There is only a reservation that they do not restict the alliance members in making IP of surrounding issues.\n\n\n-------------- quote:\n10) What are the SyncML licensing terms?\n\nSyncMLmakes the reference code for the SyncML framework available to everyone. Vendors can download this code and use it in their implementations. \n\n11) Can I resell or distribute the SyncML protocol?\n\nThe SyncML initiative encourages companies to market products that incorporate the SyncML Specification.\n\n-------------------- end quote \n\nhttp://www.openmobilealliance.org/syncml/introduction.html\n\nregards birger...."
    author: "Birger Kollstrand"
  - subject: "Kdevelop"
    date: 2003-02-16
    body: "In alpha3 when I compile something and hit an error, it just dumps all the info to the message window at the bottom, and stuff flies by.  Thats kind of annoying.  Why can't it just stop at the first error, and move your cursor in the editor where the error is?  (Is there a way?)\n\nAlso, judging by the forums -- where there are scores of messages with 0 responses, it seems like there isn't much of an active community.  I also get the impression that most KDE developers themselves don't actually use Kdevelop.  Is that true?  Whatever the case, as someone new to KDE/QT stuff I'm finding it rather frustrating to figure out what to use, where to go, best tools to use etc.  Anyone got any advice for someone who knows there way around C, C++ and programming in general but not with Linux/QT/KDE inparticular? :)\n"
    author: "Anonymous"
  - subject: "Re: Kdevelop"
    date: 2003-02-16
    body: "1) Turn on Program -> Options -> Make -> abort on first error\n2) Press F4 to go to each error / warning\n\nNot many kde developers use kdevelop because they're already used to vim/emacs/whatever.  Have a look at kdesdk, there are useful development tools / scripts there."
    author: "Hamish Rodda"
  - subject: "Re: Kdevelop"
    date: 2003-02-16
    body: "That kind of works, but the cursor isn't adjusted, and more scroll is outputed than necessary.\n\nI don't suppose you know if there is any kind of \"Using emacs as a development environment\" sort of thing out there?  A big plus if it includes KDE specific info."
    author: "Anonymous"
  - subject: "Re: Kdevelop"
    date: 2003-02-16
    body: "If the cursor isn't adjusted you might be hitting the bug where opening a new file sometimes doesn't get the right cursor location due to timings (a kate fix is in the works).\n\nkdesdk includes scripts for use with emacs under kdesdk/scripts.  That's all I know...\n\n"
    author: "Hamish Rodda"
  - subject: "Re: Kdevelop"
    date: 2003-02-16
    body: "Thanks!"
    author: "Anonymous"
  - subject: "Re: Kdevelop"
    date: 2003-02-16
    body: "Are they installed if I have executed \"make install\" in kdesdk? How do I use them?"
    author: "Erik"
  - subject: "Re: Kdevelop"
    date: 2003-02-18
    body: "Ask on irc.freenode.net, #kde-devel  -- I really don't know the answer, because Kate does everything I need."
    author: "Hamish Rodda"
  - subject: "konqueror"
    date: 2003-02-16
    body: "I'm getting very excited by the pace that konqueror is improving.\nNevertheless, I'm disappointed that konqueror (even in 3.1) still has issues rendering many common web sites (eg. espn.com)...rather, I'm disappointed that it won't render them like mozilla, or IE...\nIs it a konq philosophy to not render certain pages as the site designer intended it to be rendered, just because perhaps the designer is not properly following standards.... or is it a case where the konq team has not gotten around to making certain sites conform to mozilla/IE rendering.. or perhaps do I have the konq settings screwed up (I am using it on a red hat machine)\nI'd love to hear from someone who knows more than I.\n "
    author: "pnut"
  - subject: "Re: konqueror"
    date: 2003-02-16
    body: "I don't know what you want. The site you mentioned renders just fine here (I don't see any graphical glitch). If you think there's something wrong contribute yourself something to fix it (eg. by posting a bug report at bugs.kde.org and describing what you actually don't like. Just saying you don't like Konqueror doesn't render like Mozilla/IE makes people wonder why you use Konqueror at all) otherwise you'll be ignored as a possible troll."
    author: "Datschge"
  - subject: "Re: konqueror"
    date: 2003-02-16
    body: "You are right, I wasn't descriptive...\nactually konqueror handles the top espn.com page just right.  But some major subsections, for example : http://sports.espn.go.com/nhl/scoreboard , don't render as the site designer intended (compare konqueror to mozilla on this page)\nI'd prefer konqueror because of its integration with the desktop, but I'd like it more if it could render my favorite site the way its designers intended it to.  \nIt wouldn't be that big of a deal, but for the fact that it's one of the most visited sites on the web.  Also because of that fact, I certainly would not be the first to mention these espn specific problems to konqueror developers.\n\nActually...there are prob pages that konqueror handles better than mozilla and IE...So I'll prob just be patient with the progress, and assume that this issue will be handled with the safari merges..."
    author: "pnut"
  - subject: "Re: konqueror"
    date: 2003-02-16
    body: "> actually konqueror handles the top espn.com page just right. But some major subsections, for example : http://sports.espn.go.com/nhl/scoreboard , don't render as the site designer intended (compare konqueror to mozilla on this page)\n\nPerhaps that is because the site designer is a bit of a putz? I took a look at this based on your comment and it did drop the header on top of the menu, however I can tell you it renders fine now. This is not the fault of konqueror, but of the moronic Javascript that only looks for IE, Netscape or Mac. They select style sheets with Javascript for the browser to compensate for minor differences in rendering. A grand idea to sell a more expensive package to ESPN with for a few pixels here and there. However the idiots forgot to leave a fallback style sheet for unrecognized browsers.  Their code is commented US patent pending though. I'n't that special?\n\nUsually these errors are checking for compatability and checking browsers instead of if they reconize DOM. If they must use Javascript they should check to see if the browser reconizes DOM and if it does issue the script. That leaves you with Netscape, DOM browsers and category 3, unable to render. \n\nKonqueror has some tools from kdeaddons. From Konq's menu select Tools>Change Browser Identification and select probably about anything else and it should work. Konqueror is fully capable of rendering here. Why don't you write them. I don't have the Javascipt in front of me but I know you can ask a browser if it recognizes DOM.\n\nBTW browser identification can be set per site. Don't change it indescriminately as it will skew site logs and look like there is less of Konqueror out there. Webmasters should get smart and check for DOM recogntion. Feel free to contact them when they don't."
    author: "Eric Laffoon"
  - subject: "Re: konqueror"
    date: 2003-02-19
    body: "Eric,\n\nCould you check out one of the two bugs that are open for rendering issues with KHTML?  There has been a bug in KHTML since the 3.1rcX days that has forced me to not want to use konqueror.  The bugs can be found here:\n\nhttp://bugs.kde.org/show_bug.cgi?id=52993\nhttp://bugs.kde.org/show_bug.cgi?id=54695\n\nThank you very much.\n\n\nBret.\n"
    author: "Bret Baptist"
  - subject: "Re: konqueror"
    date: 2003-02-16
    body: "This is a msn site (seee slashdot for opera/msn problem).\nIf you change the browser configuration to IE konqueror renders the page fine, which makes me believe that you get a bad style sheet if you identify yourself as konqueror user."
    author: "XavierXeon"
  - subject: "Re: konqueror"
    date: 2003-02-16
    body: "excellent! this is a solution for the moment"
    author: "pnut "
  - subject: "Re: konqueror"
    date: 2003-02-16
    body: "actually, it is not a real solution. This will simply encourage M$ to continue its attrocious behaviour, and furthermore will make you show up as an IE in web statistics, furthering the cause of those who say that they don't need to make their websites standard compliant."
    author: "anon"
  - subject: "Re: konqueror"
    date: 2003-02-16
    body: "Some pages (including NBA Scorecard) have a \"report bug\" link at the bottom of the page. While that link itself does not work in Konqueror you can use \n\n  http://dynamic.espn.go.com/espn/bugs\n\nto report bugs with that web site.\n\n\n  "
    author: "Anonymous"
  - subject: "Re: konqueror"
    date: 2003-02-18
    body: "\"Is it a konq philosophy to not render certain pages as the site designer intended it to be rendered?\"\n\nYes. And it should be the philosophy of EVERY browser to do the same. The web is not a page oriented word processor. Everyone has a different screen resolution, monitor size, browswer window size, custom stylesheets, color depth, etc. You simply cannot make a webpage display identically for every user.\n\nKonqueror cannot guess how the designers want their pages to be displayed. So instead it uses HTML and CSS to figure it out. If the designer is not using proper standard HTML and CSS, then there's no way for konqueror to figure out what is wanted. Mozilla can handle it for one reason and one reason only: they emulate the b0rken and standards trashing Internet Exploder."
    author: "David Johnson"
  - subject: "Konqueror"
    date: 2003-02-16
    body: "Cool, new XFree86 features, I suppose that means customizable cursors, finally.\nI'm quite tired of those ugly black-and-white cursor. ;-(\nKonqueror has really grown up to the best file-manager I know.\nI like the fact that you can use tabs not only for webpages\nbut for file-management as well. This is really a step ahead in comparison\nto other file-management-programs (you know which...)\nThe only major bug left is the fact that you cannot open two Konqueror\nwindows and use cut/copy and paste to move/copy a file between them.\nI wonder why this still doesn't work in 3.1... Well, perhaps nobody\nelse uses Konqueror like this. But I guess it should work, because\nit works within the same Konqueror window."
    author: "Damon"
  - subject: "Re: Konqueror"
    date: 2003-02-16
    body: "Mmmmmm. It works for me. I'm using Gentoo with kde 3.1 and works between two independent konqi windows and between two tabs in the same konqi window. Maybe a vendor bug ?\n\n"
    author: "x"
  - subject: "Re: Konqueror"
    date: 2003-02-16
    body: "Are you sure you have a real 3.1 and not one of the RCs? A bug with the same symptoms was fixed on 2002/12/08; and this works fine for me in HEAD.\n"
    author: "Sad Eagle"
  - subject: "Re: Konqueror"
    date: 2003-02-17
    body: "Well, thanks to all who answered.\nI was wondering why it worked for all of you\nbut not on even one PC I installed KDE 3.1 on.\nNow, I found out that it doesn't work only\nif Konqueror is in the \"detailed folder view\" (or\nwhatever it is called in the English version). If I\nuse \"icon view\" I can copy and paste.\nUnfortunately I set this to be my default view.\nSo, I guess this bug will be fixed someday. At\nleast I can switch to a different view temporarily\nas a workaround.\n\n\n"
    author: "Damon"
  - subject: "Re: Konqueror"
    date: 2003-02-18
    body: "Hmm, seems to work fine with the detail list view for me, too.\nCould you please describe exactly what you're doing, step-by-step, and what's going wrong? \n"
    author: "Sad Eagle"
  - subject: "Re: Konqueror"
    date: 2003-02-18
    body: "Strange, well I try to describe this in full detail.\n\nI have a Home icon in the taskbar\nwhich runs:\n\nkfmclient openProfile filemanagement\n\nI click on this Icon, Konqueror runs maximized and in\ndetailed list view in my main home directory (I saved this\nto be the default setting)\nI click on this Icon again. A second Konqueror window\nopens and becomes active. The first Konq window is\nbehind and only visible in the taskbar.\nI right click on a file in my home directory and\nselect \"Copy\" from the menu.\nI click on the first Konq window in my taskbar. The first\nwindow becomes active. I right click on an empty space (not on a file)\nwithin the detailed list view and... \n\nThe context menu opens but \"Paste\" is disabled/grey!\n\nDoes nobody else experience this? The rest of KDE 3.1 works perfectly fine.\nBut this does not work. Not in KDE 3.0 either. Not with any standard SuSE 8.1\ninstallation on lots of PCs in our company I tried it.\n"
    author: "Damon"
  - subject: "Re: Konqueror"
    date: 2003-02-17
    body: "Works here, too. Using Debian 3.0r1 (woody) KDE 3.1 packages.\n\nIt does seem a bit silly to do this, since you can just drag-n-drop the files if you have two windows open. I guess if you're running at a low resolution or want to cut/copy/paste between windows on different desktops it makes sense, though."
    author: "Blue"
  - subject: "memory leak in kicker"
    date: 2003-02-17
    body: "Do the KDE developers plan to fix annoing memory leak in kicker (http://bugs.kde.org/show_bug.cgi?id=49215 )?"
    author: "Andrey V. Panov"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-17
    body: "Vote it up on bugs.kde.org...  but im betting this is not a leak in kicker but in an applet, since i have yet to ever see it.\n\ncheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "But I use only applets distributed with KDE. Did anybody among KDE developers run KDE for days and check its memory usage?"
    author: "Andrey V. Panov"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "No because it crashes ... just like Gnome har har har ....\n\nMaybe when these projects get to the point where the developers think they look OK they can start going for stability (a la Emacs) and performance (a la BSD file system)\n\nI remember running one instance of Emacs (lisp remember - the memore use grows naturally) for almost 2 years.  I also remember a most visited and used FS/OS in the world at freebsd.org with ISO image FTP downloads and \"make world\"'s over and over again ....\n\nWould that that sort of quality make its way to the desktop (and also attract top notch grafists and designers to beautify it). How many more years must we wait?"
    author: "NONInstaller-of-KDE"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "How long before you contribute?"
    author: "Me"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "Hey non-installer ... stop waiting now:\n\nhttp://www.apple.com/macosx/\n\nAs for Emacs-like stability in a DE well uhh yeah that's the goal ... with KDE as version 3.1 and Emacs at 21.3 (**21.3!!) there's a reason Emacs is \"stable\".\n\nBTW ... in case you hadn't noticed KDE is only at version 3.1 and can almost (I said *almost*) do everything Emacs can do !! ;-)\n\nPlus KDE is scriptable via dcops and python (and others) ... Emacs limits you to Lisp (e-lisp at that). So there!! :-D\n\n"
    author: "NONWaiter"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "> Maybe when these projects get to the point where the developers think they look OK they can start going for stability (a la Emacs) and performance (a la BSD file system)\n \nMaybe you could stop trolling and talk out of an orafice that has teeth? I'm happy to use Quanta+ as an example. A crash in CVS HEAD is rare and in proper setups release versions don't. Memory leaks have been relentlessly chased down and our Parser was a performance problem. In 3.0 it was slow, in 3.1 it was faster and in 3.2 it's finally near complete. Operations that could take several seconds on a 10,000 line file now are in milliseconds. In fact the entire file can be parsed between keystrokes in most cases. We are working on the final node localization optimizations. All of this has been done with a barage or requests for WYSIWYG and more which this was needed for first. We are not waiting to complete our feature set to do things right as you suggst and we have produced less features to be architectureally excellent. These speed optimizations have been done while watching CPU cycles and keeping them to a minimum. We make no excuses for quality and speed. we don't have to.\n\nIn short, if you speak from an interface nearer to your brain you are likely to make more sense. More politely, if you do your due dilligence you will see how erroneous your ASSumptions are. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "I have a few clients open, via X or remote VNC.  The longest running is 3 weeks, running 3.0.4.  The shortest is running head from 3 days ago.  I am sure the problem exists but probibly only in your unique config.\n\nMy advice after looking at the bug report is to try to provide some more information, maby look for duplicate problems, and then maby see if you can provide what exactly is leaking memory.\n\nI only run KDE applets myself, plus quite a few i wrote for myself.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "> I have a few clients open, via X or remote VNC. The longest running is 3 weeks, running 3.0.4\n\nLinux marvin 2.4.19-4GB #1 Fri Sep 13 13:14:56 UTC 2002 i586 unknown\n 10:27am  up 114 days, 53 min, 5 users,  load average: 0.04, 0.06, 0.01\n\nBox is constantly running KDE. During that time KDE has been restarted few times only due to it being updated (now running suse-packaged 3.1). Zarro crashes, no oddities, everything just works like a trains WC. "
    author: "jmk"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-18
    body: "Can you check the memory consumption of kicker? Is it approximately constant or growing with time?"
    author: "Andrey V. Panov"
  - subject: "Re: memory leak in kicker"
    date: 2003-02-19
    body: "There are no exotic things in my configuration. The panel have only minipager, taskbar, clock and K button. The leak is seen for a long time, certainly since KDE 3.0. I have tried various configuration options before compilation, Slackware`s packages."
    author: "Andrey V. Panov"
  - subject: "Re: memory leak in kicker"
    date: 2003-07-17
    body: "kicker's memory usage grows over time.\nI either have to restart or kill kicker and let kdeinit start it back up\n\nIt takes up 14.4mb on startup, it was taking 68mb before I killed it.  I don't know why a panel would need to take up so much memory."
    author: "David Davis"
  - subject: "Re: memory leak in kicker"
    date: 2003-07-18
    body: "14.4 mb on startup isn't bad at all considering how much of that is likely shared. leaks causing it to go to 68mb is bad however. is there any special usage of it that causes this? I've been valgrind'ing kicker for a week now, and I've caught one leak so far, but it caused a loss of ~300k or so for 45 mins time."
    author: "anon"
  - subject: "Re: memory leak in kicker"
    date: 2003-08-10
    body: "I get a HUGE memory leak in kicker in Redhat 9.\n\nIt's been one day since I last killed it, and it's using almost a gigabyte!\n\nThe only applets I am running is Klipper and Clock \n\nSurely others have noticed this?  \n\n[craig@localhost script]$ ps axv | grep kde\n 1995 ?        S      0:00   1799   588  3559  984  0.0 /bin/sh /usr/bin/startkde\n 2098 ?        S      0:00    236    31 25692 7856  0.7 kdeinit: Running...\n 2101 ?        S      0:00    101    31 27400 8264  0.8 kdeinit: dcopserver --nosid\n 2104 ?        S      0:00    325    31 28568 9312  0.9 kdeinit: klauncher\n 2106 ?        S      4:07    800    31 29756 10440  1.0 kdeinit: kded\n 2127 ?        S      0:00    846    31 34288 10092  0.9 kdeinit: knotify\n 2130 ?        S      0:00    735    31 29532 10156  0.9 kdeinit: ksmserver\n 2131 ?        S      0:16   1147    31 30576 12096  1.1 kdeinit: kwin -session 117f000001000105549695300000045610000_1059381616_912699\n 2133 ?        S      0:33   1470    31 31220 10120  0.9 kdeinit: kdesktop\n 2135 ?        S      1:53   1488    31 2685844 563264 54.5 kdeinit: kicker\n 2136 ?        S      0:00    130    31 26060 8660  0.8 kdeinit: kio_file file /tmp/ksocket-craig/klauncherg3IRkc.slave-socket /tmp/ksocket-craig/kdesktopN8xY1a.slave-socket\n 2143 ?        S      0:00    848    31 30192 10388  1.0 kdeinit: kwrited\n 2149 ?        S      0:04    729    31 29620 10136  0.9 kdeinit: kaccess -session 117f000001000105551438800000045610044_1059381616_906129\n 2153 ?        S     13:08   1503    31 34500 15184  1.4 kdeinit: konsole\n19955 ?        S      0:18   2374    31 37200 15088  1.4 kdeinit: konqueror --silent\n 2214 pts/6    S      0:00    164    70  3521  648  0.0 grep kde\n[craig@localhost script]$ free\n             total       used       free     shared    buffers     cached\nMem:       1032680    1019848      12832          0     135580     130616\n-/+ buffers/cache:     753652     279028\nSwap:      1028120     291184     736936\n[craig@localhost script]$ kill 2135\n[craig@localhost script]$ free\n             total       used       free     shared    buffers     cached\nMem:       1032680     439760     592920          0     135504     134492\n-/+ buffers/cache:     169764     862916\nSwap:      1028120      52520     975600"
    author: "Craig O'Shannessy"
  - subject: "Re: memory leak in kicker"
    date: 2004-04-18
    body: "There is indeed a memory leak in kicker.  On my RH 9 system, if it is allowed to get out of hand, it can bog my whole system down with swapping, forcing a reboot (!)\n\nThe leak seems to be a function of how much you use the launcher.  Killing kicker and restarting it effects a cleanup.\n\nI created a simple script to kill -s SIGINT the kicker process and run up kicker again immediately.  It works.\n\nTry \n\n  kill -s SIGINT `ps --no-headers --format=%p -C kicker`\n  kicker\n\nPut this in cron to run nightly, or even hourly, depending on requirements.\n\nOf course this assumes you have KDE up constantly."
    author: "L. Wright"
  - subject: "Re: memory leak in kicker"
    date: 2004-04-18
    body: "PS.  The above is just a hack to demonstrate that killing and relaunching kicker frees up the memory kicker was holding.  I don't suggest you try and put that script into a cron job as is!  Also note:  allowing multiple kickers to run at the same time is definitely not recommended. :|"
    author: "L. Wright"
  - subject: "Re: memory leak in kicker"
    date: 2006-12-01
    body: "well im using kicker on SuSe FActory and when i start kde it uses all cpu all i do is send signall SIGSTOP and its ok no killking."
    author: "migero"
  - subject: "Branches"
    date: 2003-02-18
    body: "Is there a simple way to find out which branches exist in the CVS, e.g. KDE_3_1_RELEASE, HEAD, etc.? I can't find a list anywhere."
    author: "Meretrix"
  - subject: "Re: Branches"
    date: 2003-02-18
    body: "Go to say http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/ and look on the bottom of the page. There is a dropdown list of the branches.\n\nIf you do a cvs log (check the syntax) the log file lists the branches and revision numbers.\n\nDerek"
    author: "dkite"
  - subject: "Re: Branches"
    date: 2003-02-18
    body: "You can go to http://webcvs.kde.org. Then select a module and have a look at the combo box at the end of the page.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "KDE ain't ready for the desktop..."
    date: 2003-02-18
    body: "...until everybody is happy and people stop complaining about it. See also the comments at:\nhttp://osnews.com/comment.php?news_id=2841\n\nI'll quote a few things:\n[quote]\nI have a hard time trusting the aesthetic sense of\nsome linux enthusiasts. These are the same people who\nwere oohing and aahingsaying over Gnome desktops\nback whey they were butt ugly.\nAnd they sure as hell weren't nicer looking than\nW95. despite this being an article of faith among many\nWindows looks has always been the most trivial of my\nconcerns with Microsoft.\n[/quote]\n\n-- and --\n\n[quote]\nI've tried KDE on my Pentium 2 266, 96 Meg RAM (and a 1 yr old 20 gig hard drive) and it's painfully slow - starting kde takes forever, and launching relatively lightweight apps takes at least 15 seconds. Basically it's too slow to be useable. However i've found gnome 2 to be significantly faster - both when starting up, and when launching apps. I'm not going to have a go at KDE for requiring a reasonably fast machine to rnu comfortably, it was never meant to be a lightweight desktop, i just won't use it on machines any slower than a pentium 3.\n[/quote]\n\n-- and --\n\n[quote]\nBy Eugenia (IP: ---.client.attbi.com) - Posted on 2003-02-18 00:01:09 Konqueror is the most buggy part of KDE. Maybe because it is the most complex one. Point is, it does not always work as expected, in fact it has right-click menus for items that don't make sense to have specific options, and in general is *buggy*, crashy and bloated.\n\nAs for KDE overall, while we seen it adding a lot of new features lately, it REMAINS unpolished, bloated and without a direction: It includes system utilities for some things, and not for others. It includes certain user-level applications, but not everything. KDE has an identity crisis and leaving a few preferences to the distribution while handling others through KDE is just weird. And inconsistent.\n\nMore over, the KDE 3.1 Kontrol Center is the Center Of All Laughs. I have never seen a more bloated preference panel in my life, having more about 30-40 leaves in the tree excluding the number of tabs for each leaf/option! Soon you forget where is where. It is just unusable. Terrible.\n\nOne year ago I had Gnome out of the horizon and considered KDE the only leading Linux DE. One year later, and *because* of the Red Hat involvement on Gnome 2, I actually tend to see Gnome2 having a better face today. Gnome2 still sucks, but for a DE, is simpler and more predictable than KDE. KDE tries to do everything, adds apps to the mix and instead of focus to the usability and how to clean up their chaotic existance, they add more to the mix. A shame.\n[/quote]\n\n\nLooks like there's still a lot of work left to be done. People still don't take Linux or KDE seriously. There's still waaaay too much critism about KDE and Linux."
    author: "Anon"
  - subject: "Re: KDE ain't ready for the desktop..."
    date: 2003-02-18
    body: "*yawn*\nAsk yourself whether all those comments stopped people from using Windows.\n\nAs for me I'm using KDE on a AMD K6-2 300mHz with 224MB RAM. Program startup is slower but everything else is faster than under Windows on the same computer (can't play Ogg and Divx under Windows without heavy skips, stuff like that). A desktop's look is the biggest non-issue I've ever heard about: if you don't like it, change it. Tastes are different in different times and between different persons so it's the worst part to rate a desktop after. Usability is the only valid concern, but it's also the part where both Gnome and KDE sped up a lot as of late.\n\nCriticism is fine as long as it is constructive. Sadly many people seem to ignore this and prefer badmouthing to contributing. And with community projects like KDE contributing does make a difference."
    author: "Datschge"
  - subject: "Re: KDE ain't ready for the desktop..."
    date: 2003-02-18
    body: "3.1 is dramatically faster and more finished than any previous release. To emulate an earlier version of kde, I have to run an emerge -u world in the background. Then it feels just like 3.0.\n\nSeriously. The pace of development is so quick in the linux desktop (both kde and gnome) that any opinion is out of date quickly.\n\nWhen I hear a complaint, the first question is \"what distribution and version\".\n\nMore importantly, I have yet to request permission to use any of the linux desktops. Seems to me one has to with XP, or it doesn't work very long.\n\nDerek"
    author: "dkite"
  - subject: "Re: KDE ain't ready for the desktop..."
    date: 2003-02-18
    body: "Well, I've used Windows before at home and in my company.\nFirst, I switched from Windows in my company, a few weeks later\nat home. I didn't know anything about Linux or KDE so I jumped\nin at the deep end. I was never interested in Linux and a big\nsceptic. But at work and at home my Windows XP crashed often even \nafter installing on a fresh formatted HD. The task bar \nvanished, I got a virus just by *viewing* an email and I thought:\n\"OK, it's bad, but computers are like this\". In my company we\nused Linux only on servers. Nobody had any experience of using\nLinux as a regular desktop OS. My colleagues switched our \nservers from Windows to Linux after lots of strange crashes, hangs\nand other problems with our mail server. One day, I just started Windows\nand was editing a text in NotePad(!) all of a sudden my computer rebooted.\nThat was the last time I saw Windows on the screen of my computer.\nI took our SuSE 8.1 CD and installed it not knowing if I might regret\nthis step. After 20 minutes KDE was installed. To my amazement all\nhardware was detected correctly. I needed some time to get used to all\nthe new concepts like different Users, no EXE files, and I needed some\ntime to find out which application is best for my purposes.\nBut what I really noticed right from the start is the speed and stability.\nI can run my computer for days, opening and closing applications, installing\nnew applications without ever having to restart my computer. And my computer\n(1 GhZ, 128 MB RAM) runs so fast I never thought it could be possible.\nI never hear my hard disk working for minutes when I start an application.\nTo all sceptics: Try this under Windows and Linux.\nCopy a file over the network with Explorer/Konqueror.\nWhile it is copying start Word/KWord. While Word is starting and\nthe files are copying, start Notepad/KWrite and start writing some\ntext. In Linux/KDE all this is no problem. In Windows, you can be\nlucky if Word starts after minutes and heavy disk activity. In the\nmean time your computer becomes dead slow. So I found out why\nLinux applications do not have a splash screen. They do not need\none: You can run the application and keep on working until it is started.\nSo if you are not into computer Games which are mostly for Windows only:\nYes KDE is ready for the desktop and I wonder why people keep saying\nits to slow? Well, I wanted to point out here that I experienced\nthe opposite to be the case.\n\n"
    author: "Damon"
  - subject: "Re: KDE ain't ready for the desktop..."
    date: 2003-02-19
    body: "> One day, I just started Windows\nand was editing a text in NotePad(!) all of a sudden my computer rebooted.\n\nThings like that usually point to Hardware-Problems, ie. defect or incompatible hardware. Of course, it might also be due to bad drivers, configuring some hardware in ways, that they do conflict with each others.\n\nYet, it seems Linux is even in this regard more \"stable\" than Windows.\n\nRegards,\nHarald Henkel"
    author: "Harald Henkel"
  - subject: "Re: KDE ain't ready for the desktop..."
    date: 2003-02-20
    body: "Well, I can only say:\nI installed Windows with autodetected hardware & drivers.\nI installed Linux with autodetected hardware & drivers.\nWindows crashes. Linux does not.\nIt's as simple as that.\nHardware problems? I doubt it!\n\n\n"
    author: "Damon"
---
This week, in the <a href="http://members.shaw.ca/dkite/feb142003.html">latest KDE-CVS-Digest</a> read about the many improvements in the development tools. In <a href="http://www.kdevelop.org/">KDevelop</a>, 
work continues on code completion and new code templates. 
<a href="http://quanta.sourceforge.net/">Quanta</a> gets ktips and finishing polishes. 
<a href="http://kate.kde.org/">Kate</a>, <a href="http://cervisia.sourceforge.net/">Cervisia</a>, <A href="http://i18n.kde.org/tools/kbabel/">KBabel</a> and <a href="http://uml.sourceforge.net/">Umbrello</a> continue to get better. 
Support for new <a href="http://www.xfree86.org/">XFree86</a> features are being implemented. 
And nothing like a <a href="http://www.fosdem.org/">gathering of developers</a> (<a href="http://dot.kde.org/1044844022/">report</a>) to
improve the games. 
<!--break-->
