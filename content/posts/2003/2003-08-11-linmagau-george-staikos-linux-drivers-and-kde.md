---
title: "LinMagAu: George Staikos on Linux Drivers and KDE"
date:    2003-08-11
authors:
  - "ineogy"
slug:    linmagau-george-staikos-linux-drivers-and-kde
comments:
  - subject: "\"Tainted\" drivers"
    date: 2003-08-11
    body: "I'm very happy to have the NVidia \"tainted\" drivers in my system. They're very easy to install and work just excellent and NVidia updates them frequently. I can play most games in WineX just as good as Windows. Good luck with the \"open\" ATI drivers.\n\nIf you don't like NVidia's (or any other company) closed source policy, develop your own open source drivers.\n"
    author: "Slovin"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "The problem is that if we let lock-in software trickle into essential parts of the operating system we will end up with something like Windows. The main reason I am using Linux is however that is free in the sense of free speech. Operating system are at the heart of information processing and should not be controlled by companies but by user and developper communities."
    author: "Tuxo"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "And if you use closed drivers, get support elsewhere.\n\nNVidia lost a sale for it's mb chipsets recently due to their insistence on not making available their hardware specs. Plus the fact that I would have had to purchase a video card on top of the motherboard.\n\nNo, I'm not going to load a proprietary module for the video, and ethernet hardware components. Forget it.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "NVidia lost my money too. Unfortunately Matrox does not do graphic cards for laptops so I choosed a laptop with a ATI card instead of one a little chiper with a NVidia card. And it seems that lately ATI is supporting the people that do the OSS drivers for their card. BTW, the cad works fine with 3d accel under linux.\n"
    author: "fredi"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-13
    body: "Let me get this straight: You refused to buy NVIDIA-card, but instead bought a card from company that cares even less than NVIDIA about Linux? At least NV provides good drivers for Linux (although closed source), whereas Ati provides jack shit. Yes, they \"support\" the OS-drivers, but I don't think they support them any more than NV suppoert the guys making the nv-drivers (OS NVIDIA-drivers). it's more like Ati expects OS-guys to do the work of writing drivers for them, whereas NVIDIA provides users with drivers that they themselves made.\n\nWhen it comes to Linux-support, I rank NVIDIA ALOT higher than Ati!"
    author: "Janne"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-16
    body: "I disagree.  I do not expect from the manufacturer to provide drivers for my operating system; for whatever reason, I might want to switch to hurd or netbsd tomorrow.  But I _do_ expect them to release the specs for their hardware, so that anyone capable can step up and write a driver.  Unless they change their policy, I will never buy an nvidia card in the future."
    author: "pavos"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-18
    body: "Sorry, but you're quite wrong.  ATI regularly submits patches to the Xfree86 project, however due to the lunbering hiarchial development of Xfree86, it takes FOREVER for these patches to get applied and tested.  That's one of the reasons why what's-his-face is developing his own fork called Xouvert."
    author: "Nick"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-18
    body: "Hi there, I'm the interviewer in question.\n\nWhether or not NVidia's drivers are more stable or ATI's are (my NVidia drivers aren't especially stable), the area we're looking at it closed source drivers and open source drivers and their effect on OS workings in general. Most of you (hopefully) would have read about the beginnings of the GNU project when Richard Stallman had problems with his printer drivers and he asked for the source, but didn't receive it.\n\nThis is where everything has pretty much grown from and to forget that and simply look at it in terms of current driver releases and company standings is rather shortsighted. Every few years a major change happens in the computer industry, look at 3DFX for example, going 3DFX was seen as \"the way to go\" for quite a while but that changed, didn't it (please don't get into OT arguments about 3DFX, it's just an example)?\n\nWhat I'm really trying to say is look at the big picture and try to remember what OSS is about and what has happened in the Windows world in terms of drivers and software with its closed source world."
    author: "John Knight"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "If you use your computer just to play games with winex, is probably better to remain with your propietary nvidia drivers. If you don't care about free software aswell, then you would be better with windows. I have a computer with NVIDIA graphics card and use the propietary drivers, but I'm not so \"happy\", neither the drivers are sooo good as you're telling. I wish NVIDIA could release their drivers, and we are asking them for it. In long term, it would be better for them."
    author: "Lucas"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "Oh I do a lot more than just WineX. After all, I could just play the games easily in Windows if I wanted to. The point is that I do _care_ about free software, but not to the point where any propietary \"stuff\" become automatically evil.\n\nSo far, the propietary solution offered by NVidia works for me. If there is an open source driver that matches NVidia's driver performance then I'd certainly use it.\n"
    author: "Slovin"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "Not a good comment up there ...\nIt would be possible to develop an open source driver at least as good as nvidia's proprietary ones, only if they could release the specs. The nv driver is indeed good for 2D, but doesn't work with 3D ...\nStick with your \"tainted kernel\" if you like, but don't brag about it, since it's not \"free\" (as in free speech of course). That is also the reason why I encode in ogg/vorbis format instead of mp3 (The quality of ogg is better also ...). See the open format is actually better ? If nvidia would open there specs, you could probably win some fps as well, so encourage it :o)"
    author: "Julien"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "Obviously they write closed-source drivers because they do not want to release their specs. So if they release their specs, you would have open source drivers that are at least as good as the proprietary ones :)\n\n"
    author: "AC"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "I (and other people I have spoken to) have found that nVidia's drivers can decrease system reliability and cause many other seemingly-random problems. Since they care closed-source, only nVidia can fix them, and they don't seem to be doing that. My guess is that the resouces nVidia are dedicating to Linux driver development is very small (which is understandable when given the small user base), so they have no hope of testing and debugging them across a wide range of configurations.\n\n\"If you don't like NVidia's (or any other company) closed source policy, develop your own open source drivers.\"\n\nWhat do you think the nv driver is? nVidia is deliberately being uncooperative with nv developers, so it is difficult for them to develop even a good 2D driver. I realise that nVidia need to protect their (and their licensers') IP, but it would be nice if they at least helped the nv developers to create a good 2D-only driver.\n\nBinary-only drivers also make software support more difficult. I know that the MPlayer developers would like to improve nVidia support, but they are unable to do so because of this."
    author: "Yama"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "I tried nVidia closed drivers and it was wonderful in speed inprovement terms but my system become a bit crazy: some unusual time to time system hangs, harddisk spinning without stop... Of course, I got rid of them.\n\nWithout doubt, I prefer open drivers, so when I needed a new MoBo I went for a VIA based one (even if I use Windows, even if nForce2 is a bit faster than KT400)."
    author: "Juan M. Caravaca"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "That nVidia can't open source their drivers is natural and justified, I beleive but I don't think they are as innocent when they are not releasing the specks. Consider the scenario where open source driver matches the performance on most games except those frequently benchmarked, or worse still, on every game but benchmarks. "
    author: "random guy"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "Well.. this is a very semplicistic comment about topic. Nvidia can develop the same driver with the same identical features using free software model and community support. Free software is \"live\" software! Remember this."
    author: "Peek"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "I was very happy with the closed source drivers of Lexmark. They offered very good printing capabilities, much better then I experienced with OSS drivers.\nBut when I wanted to use CUPS as printing subsystem, i could not, since the lexmark drivers are only available for LPRNG, and they won't create newer drivers for my printer. So I could not take advantage of all the features offered by kprinter. \nNow I got a HP-printer, which uses an OSS-driver created by the community with support of HP. This printer prints in Linux as good as the Lexmark printer, but since it is OSS, I can use this one with whatever printing subsystem my Linux-distro offers.\n\nI can imagine the same with NVidia drivers, if NVidia stops updating their drivers, You will get stuck with an older version of Xfree, wich will become obsolete and/or incompatible over the years.\n\nRinse"
    author: "rinse"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "If nVidia stops updating their drivers, you'll take the nv drivers. "
    author: "CE"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "Yep, but like the lexmark drivers, the OSS versions are not as good as the closed source versions.\nYou can use OSS drivers for the lexmark, but they don't even print half as good as the propietary drivers of Lexmark. With HP no problem, the OSS-drivers are (allmost) as good as the proietary (Windows) drivers.\nAFAIK the nv-drivers aren't even near as good in supporting the NVidia cards as Nvidia's own losed source drivers.\nSo moving to the nv-driver is a major step down. "
    author: "rinse"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "But what ist the solution if nVidia doesn't release the specifications/sources?\nBuying ATI? Never again!"
    author: "CE"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "Hmmm... preview button..."
    author: "CE"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "The nv (OSS) drivers do not support 3D, no TV output, no DVI output, no multihead, no DVD acceleration... you could as well use generic VGA drivers :)\n"
    author: "AC"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "That's not true (for TV output at least).  I use the \"nv\" driver (because as has been said elsewhere the binary only driver is incredibly unstable) and have no trouble switching tv output on thanks to a superb utility called nvtv (which is in debian stable).  What is more, it's better than nvidia's own tv mode as it allows you to set overscan so that you don't get huge black borders when watching dvds etc."
    author: "Andy Parkins"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "The NVIDIA driver uses the \"TVOverScan\" option for overscan."
    author: "AC"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "Nice try, but unfortunately NVidia's drivers have been broken for a long time.  1.0.4496 is the first release that I can successfully use TwinView and Multi-Head configuration both without crashing while attempting to switch VC's. In an Open driver, very likely someone would have fixed this a long time ago and NVidia would have saved a lot of face (instead of their forums looking like a coordinated chorus of \"my machine hangs\".  \n\nYour quaint \"write your own\" response shows how little you know about the industry.  \"Writing one's own\" requires that the interfaces to the hardware be revealed, which seems to be part of the trouble with opening up the driver code in the first place.\nIf the vendor opens the code, we can help.  If not, it's not worth screwing a monkey trying to take advantage of enhancements they won't allow us information on.\n"
    author: "r4780y"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-11
    body: "The requirement of NVidia drivers is a myth, at least for the games that I play.  Any game that uses strictly 2D graphics (ie DirectDraw, but not Direct3D) only needs the presence of the xv extension.  The nv driver implements xv and I'm able to play Hearts of Iron at nearly 100% speed!  \n\nIn terms of NVidia's drivers, they screwed up the font rendering on my system.  How's that related?  I dunno, but switching to nv made all my fonts look good again."
    author: "Craig Howard"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-12
    body: "There are still 2D games? Who would want to play a 2d game (beside card and board games, and even they have a 3d field today..)?"
    author: "AC"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-12
    body: "I would, if it still has the gameplay of the older games. 3D games today suck (only a few exceptions); they focus ONLY on being as 3D as possible and not on being fun to play.\n\nPlease give me some more Metal Slug or Day of the Tentacle!\n\nSome companies are trying to revive old games by giving them a 3D look, but they fail *everytime*. Look at lucasarts, they've just canned their 3D version of Full Throttled. Suckers :D\n\nI bet a new high res (800x600, or even higher) 2D version of Full Throttle would sell really well, if it still has the same level of fun as the first one."
    author: "Richard Stellingwerff"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-12
    body: "Sure, everything was better in the old days.\n\nThat's only because you remember maybe 200 2D games and forgot the 5000 bad ones...\n\n\n\n"
    author: "AC"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-18
    body: "In the old days, there were only 200 games!  Seriously.  Those \"5000 bad games\" were the result of the commercial explosion of the gaming industry, as they tried to find the right formula for developing \"modern, cool games\".  Most \"modern, cool games\" are *quite* formulaic.  Besides, there are some games which are best left 2d.  Take Go for example.  Go already has infinite possibilities.  Why add another dimension of infinite possibilities?  It would only strip the beautiful simplicity of the game.  On the other hand, a random player who doesn't understand the game, would no doubt be dazzled by a holographic, perfectly rendered, explosions-when-a-piece-is-captured, \"modernized\" version.  Would it add a nanogram to the gameplay?  Only for those which ADHD.  Their is an old saying, that says the wisest of men are those who learn to be content with what they have.  That is to say, they learn to appreciate the beauty of simplicity, rather then perpetually seeking novel stimulation."
    author: "Nick"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-12
    body: "I dunno, maybe something like SimCity 4 or Commandos.  There are still games that fake it and look 3D, but the engine has a 2D core.  And what makes a game good is at least 49% design, 49% execution and 2% graphics.  It's a shame more people don't realize this and just go for the eye candy."
    author: "Craig Howard"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-12
    body: "You can't compare that, would you want to watch Pulp Fiction (or any other movie) if it were a cartoon? \n"
    author: "AC"
  - subject: "Re: \"Tainted\" drivers"
    date: 2003-08-19
    body: "Yes.  It's called anime.  Your cultural bias is overwhelming."
    author: "Nick"
  - subject: "Re: \"Tainted\" drivers"
    date: 2005-04-12
    body: "We would develop our own if there was documentation to work from.  Also, there are a lot more operating systems out there than just Windows & Linux.\n\nI think nvidia's argument that they are protecting their IP is bogus.  First, if their competitors are always copying models of their hardware that they have already put on the market their competition is going to constantly find themselves behind. Second, providing documentation on the register set and even a rudimentry programming interface doesn't give that much away about the design of the hardware.  Sure it indicates some elements of the architecture but it's hardly handing over the blueprints.\n\nI regret to say until this policy is changed I will not purchase nvidia video adapters; additonally I recommend to anyone who I discuss the topic with that they don't purchase them either.  It's just not acceptable."
    author: "Jim Worth"
  - subject: "NVidia is doing well these days."
    date: 2005-10-06
    body: "It looks like it's been about two years since the original post on all of this, and to be honest I believe the Linux community convinced NVidia to put more effort into their drivers. It was no more than a year ago when the whole 2.6 / 2.4 kernels became an issue for NVidia, then from what I can tell they finally hired some developers that actually knew Linux.\n\nI've been running my GeForce 6800 GT on Gentoo Linux with the latest drivers from NVidia for the past year straight without ANY problems. I play HL2 (Through Cedega), Doom III, WoW (Through cvs-wine), UT2K4, and several others. I also have every single possible acceleration for the card enabled:\n\n$ cat /proc/driver/nvidia/agp/status\nStatus:          Enabled\nDriver:          AGPGART\nAGP Rate:        8x\nFast Writes:     Enabled\nSBA:             Enabled\n\n$ cat /etc/X11/xorg.conf | grep Accel\nOption      \"RenderAccel\"           \"true\"\n\nI also have the \"composite\" and \"AllowGLXWithComposite\" extensions enabled for xorg. All without any problems ever for the past year.\n\nIf there was any card out there that I would recommend it would be an NVidia, they do amazing work that is unbeatable. I'm a software developer and have written a number of 3D demos, and 3D engines, this board does not fail me under Linux or Windows-- In fact I generally get better FPS under Linux (+4 fps depending).\n\nThat's just my experience though, maybe I lucked out with my hardware combination choice or something."
    author: "Caleb Gray"
  - subject: "Re: NVidia is doing well these days."
    date: 2006-03-08
    body: "Fair enough.  NVidia cards have been my cards of choice for a couple of years as well.\n\nThe problem with closed source drivers still remains - there are no guarantees as to how long they will continue to support the drivers.  This has bitten me in a big way today when my Geforce 5200 failed.\n\nI've ordered a replacement card which should arrive this week (hopefully) but in the mean time I went back to my previous card - a GeForce 2 GTS Pro.  As I already had the NVidia drivers installed and working it should have been a simple case of swapping the cards out.\n\nNo joy.  Xorg wouldn't have it.\n\nNever mind, re-install the driver.  This is when you discover something a bit worrying - you get an error message that the card is not supported by the latest drivers - and a pointer to the NVidia site to download an older version.\n\nIn fact - you've got to roll back to version 6629 to have support for this card.\n\nAll of these cards are no longer supported: TNT2, TNT2 Pro, TNT2 Ultra, TNT2 Model 64 (M64), TNT2 Model 64 (M64) Pro, Vanta, Vanta LT, GeForce 256, GeForce DDR, GeForce2 GTS, GeForce2 Pro, GeForce2 Ti, GeForce2 Ultra, GeForce2 MX Integrated graphics, Quadro, Quadro2 Pro, Quadro2 EX.\n\nThis wouldn't be a problem - apart from the fact that the unsupported driver which does support these cards doesn't work on the newer versions of the 2.6 kernel.\n\nThis leaves you with the joyous task of searching for a third party patches for the driver which may (or may not) work.  The ones I've tried so far don't work.\n\nWhich leave me stuck with the NV driver and no 3d support.  At least till my new card arrives."
    author: "DanO"
  - subject: "Not an interview about KDE"
    date: 2003-08-11
    body: "The introduction quotes all KDE parts of this interview, the other parts are not KDE-related. Not really newsworthy here."
    author: "Anonymous"
  - subject: "Re: Not an interview about KDE"
    date: 2003-08-11
    body: "I agree."
    author: "George Staikos"
---
<a href="http://www.linmagau.org/modules.php?op=modload&name=Sections&file=index&req=viewarticle&artid=246&page=1">In an interview</a> over at <a href="http://www.linmagau.org/">LinMagAu</a>, George Staikos talks with John Knight about the state of video and audio drivers for Linux, particularly the question of open vs closed drivers and when more hardware manufacturers will come around to supporting OSS development. <i>"In KDE we try to make the software completely driver and hardware independent, so adding driver features would work against that goal. We do add special features for hardware from time to time though. KDE 3.2 will include support for special hardware keys such as brightness/volume/etc. on certain laptop models, for instance."</i>
<!--break-->
