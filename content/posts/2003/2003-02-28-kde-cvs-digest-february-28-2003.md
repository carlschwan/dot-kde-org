---
title: "KDE-CVS-Digest for February 28, 2003"
date:    2003-02-28
authors:
  - "dkite"
slug:    kde-cvs-digest-february-28-2003
comments:
  - subject: "Great job"
    date: 2003-02-28
    body: "I saw nobody did it yet, so... Great job Derek!"
    author: "ArendJr"
  - subject: "Re: Great job"
    date: 2003-02-28
    body: "btw, I missed my commits this week on KRDC, which I found were quite useful since my RDP backend is no longer crashing (as far as I know ;). So if anyone wants to test connecting to Windows Terminal Servers, go ahead :)"
    author: "ArendJr"
  - subject: "KDevelop Templates"
    date: 2003-02-28
    body: "Hey in case anyone is wondering, KDevelop templates are trivial to make.  What Im looking for now are example plugins or test programs that developers would like to see as templates.\n\nIf there is interest from other developers to see templates for your applications plugins feel free to email me an example program and I will see what I can do.\n\nThe Kate plugin took me about 2 hours to do... mainly because I was talking with someone on IRC at the time I was doing it.  \n\nLastly these templates are not limited to KDE, we can handle Java, python, php4 and C apps too.  So if you have a php framework or a C framework you would like to see supported please send me a sample and I can put it in my queue.\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: KDevelop Templates"
    date: 2003-02-28
    body: "I'd like to see KFile plugins supported as a template ;-) \n\nSince I've written a screensaver, kfile plugins, konqueror plugins and kate plugins, its all easier with support in the IDE.\n\nThere are a couple in kdesdk for example."
    author: "George Russell"
  - subject: "Re: KDevelop Templates"
    date: 2003-02-28
    body: "well i wrote a screen saver template some time ago.  I actually dislike kapptemplate excessively hence why I do the KDevelop templates ;)\n\nNow if I only get arround to porting KAppWizard to run standalone outside of KDevelop... ah but that is when I have time in my next life...\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: KDevelop Templates"
    date: 2007-05-11
    body: "Hi,\n\nThis is raghav. Any one of you help me how to develop sample Kfile plugins using KDE IDE. if you any template already , can you please send it."
    author: "Raghavendra Boregowda"
  - subject: "Re: KDevelop Templates"
    date: 2007-05-11
    body: "Can you please send me a kfile plugins that had written??"
    author: "Raghavendra Boregowda"
  - subject: "Re: KDevelop Templates"
    date: 2003-02-28
    body: "if people are taking requests, lets have more python stuff :)"
    author: "vod"
  - subject: "Re: KDevelop Templates"
    date: 2003-02-28
    body: "Send me the example programs and Ill make the templates :)\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: KDevelop Templates"
    date: 2004-03-29
    body: "QTE templates not Qtopia just QTE. If you are still taking requests"
    author: "Mowgli"
  - subject: "Keep doing good work pls"
    date: 2003-02-28
    body: "Thank you Derek!\n"
    author: "Guest"
  - subject: "VFolders-based menu sounds great."
    date: 2003-02-28
    body: "This is a great idea and I am looking forward to it. This has been a problem in KDE/Gnome interoperability that has lasted long enough. Right now, Mandrake Linux implements the management of the menus via this horrible program called Menudrake. First, it's written in GTK and messes up all of the settings I make with KMenuedit. Also, they have completely messed up the path to KMenuedit. I hope that this works well and really takes off. Hopefully it will be the end of Menudrake at least! Perhaps KMenuedit will soon just be Menuedit, and used for Gnome menu config as well! Thanks for support of standardizing desktop environment settings like these."
    author: "Paul Kucher"
  - subject: "Re: VFolders-based menu sounds great."
    date: 2003-02-28
    body: "When I was using mandrake they started with a Debian program that was supposed to keep menus consistent across desktops. This was a big thing for them in spite of the fact I suspect people only switch desktops when first trying Linux out. There was the ability to disable menudrake in 8.0 or 8.1. I got tired of the upgrade merry go round and just got Gentoo. I was building KDE anyway to see what KDE was actually like before it got those \"nice\" features like Menudrake. ;-)\n\nVFolders sounds like it will finally eliminate some of the bad hacks out there and facilitate better interoperation."
    author: "Eric Laffoon"
  - subject: "Re: VFolders-based menu sounds great."
    date: 2003-03-02
    body: "VFolders look very useful.  I can hardly wait."
    author: "nonamenobody"
  - subject: "You got me -Quanta news"
    date: 2003-02-28
    body: "Hey Derek, I'm pretty excited about the new templates we got... but I can't find any news following the story head. Don't get me wrong, I appreciate being mentioned but I still hope as project lead I would know about such stuff. ;-)\n\nI also don't recall any mention of my commit of tagxml completion. The XML based tag language that Quanta uses to define tags for auto-complete, structure view and tag dialogs is the tagxml DTD. I thought someone doing a DTD update would have submitted this. So I finished it myself. This makes it a lot easier to develop new DTD definitions in Quanta for tag dialects or script languages. In fact a new XML/SGML dialect or scripting language can be added by:\n1) building a definition.rc file (review our other DTD folders)\n2) building the tag files using Quanta's tagxml DTD\n3) building any toolbars you want for it in the user gui (you can even copy other toolbars in the toolbar folder, load them in quanta and edit them)\n4) modify the Makefile.am files to include the new files/directories and make install\n\nFull functionality is achieved from just this. Heck, we'll modify the make files if you send us the other files. You can directly install them in $KDEHOME/apps/quanta to test them.\n\nAnother item that i have not seen is that our parsing is now orders of magnitude faster. \n\nI guess with so much to look at we need to keep sending heads up. ;-) \n\nWhat day is the break for gathering all information?"
    author: "Eric Laffoon"
  - subject: "Re: You got me -Quanta news"
    date: 2003-03-01
    body: "Thursday evening, pacific time. I usually do the rough draft wednesday evening, which is the cutoff for the cvs log. Announcements or news gets in until thursday. Waldo sent the bit about vfolders thursday, but his note about the number of developers missed, until next week.\n\nI also got a note from Marc Mutz about some major bug fixes in kmail. I missed them as well.\n\nDerek\n\n"
    author: "Derek Kite"
---
This week we see more large merges from <a href="http://www.apple.com/safari/">Safari</a>,  the switch to <a href="http://www.freedesktop.org/standards/menu/draft/menu-spec/menu-spec.html">VFolder-based</a> KDE menus,  the <a href="http://www.koffice.org/">KOffice</a> developers improve 
the filters and add useful templates, <a href="http://www.kdevelop.org/">KDevelop</a> 
and <a href="http://quanta.sourceforge.net/">Quanta</a> receive new templates and yet more.  Get your  <a href="http://members.shaw.ca/dkite/feb282003.html">CVS fix here</a>.
<!--break-->
