---
title: "Krusader Project Needs Developers"
date:    2003-05-23
authors:
  - "serlich"
slug:    krusader-project-needs-developers
comments:
  - subject: "Konqueror"
    date: 2003-05-22
    body: "I prefer Settings/Load View Profile/Midnight Commander."
    author: "Konqfan"
  - subject: "Re: Konqueror"
    date: 2003-05-22
    body: "Can someone tell us what the differences are?\nWhat does this have that konqueror doesn't?\n\n"
    author: "John Tapsell"
  - subject: "Re: Konqueror"
    date: 2003-05-23
    body: "krusader is a twin-panel filemanager. it looks like midnight commander with a GUI, or something like windows commander. it's a whole different way of managing files, much different from the explorer-style konqueror (to which i salute also!)"
    author: "Shie Erlich"
  - subject: "Re: Konqueror"
    date: 2003-05-23
    body: "I still don't understand.\nKonqueror can do split-panel, if that is what you mean..\nGo to Settings->Load View Profile-> Midnight Commander.\nAnd you can change whether you want to see the files as big icons, or lists of files..\n\nWhat does krusader offer over this?\nI think I might just have to play with it to understand.  \n"
    author: "John Tapsell"
  - subject: "Re: Konqueror"
    date: 2003-05-23
    body: " There's a difference between split-panel and twin-panel. If you split a view in Konqy, you get two (more or less) independent views (and the embedded konsole is yet another independent view). At most, you can lock them together, making them use the same URL. But the twin panels in Krusader/MC/whatever work together, and the command line also works with the active panel.\n\n How do you quickly copy some files from one dir to another in Konqy? In both cases, you view the two directories in two views, but in Krusader, you select the files, hit F5, and hit Enter, it's done. I don't use Konqy for file management, but I doubt you'll achieve the same without mouse fiddling, because a) there's no such keyboard shortcut, b) how would it know to which view to copy?"
    author: "L.Lunak"
  - subject: "Re: Konqueror"
    date: 2003-05-23
    body: "In konqueror you would select the files in one pane, them drag them across to the other.  Hardly a big deal :)\n\nI can't say i'm totally convinced here :)\n\nBut I'll trust that it is better in some ways..\n\n"
    author: "John Tapsell"
  - subject: "Re: Konqueror"
    date: 2003-05-23
    body: "ad a) I doubt that it would be hard to add an action \"Copy to other view\" to Konqueror.\nad b) If there is only one other view which accepts files (which would be the case in split-panel mode in Konqueror) then the answer to your question is obvious. And if there are more views then there could be some kind of popup which would allow the user to select the target view.\n\nNeither a) nor b) are convincing arguments for Krusader (except that they are currently not implemented in Konqueror). All functionality which the article mentions, i.e. extensive archive handling, mounted filesystems support, ftp, is also possible with Konqueror. I do not doubt that Krusader has some features that Konqueror doesn't have (yet). But I don't think that it makes much sense to continue this unnecessary duplication of code. It would be much wiser to add a Mignight Commander mode (not just a profile) to Konqueror than to keep developing Krusader as standalone application (it could still be made available as standalone application even if it's made a KPart). Just have a look at the following mostly high-priority items from the TODO list:\n- embedded tree view\n- Tabbed browsing\n- use KDE's date format \n- integration with KGET (dl. manager)\n- a new embedded SMB browser\n- New Bookmarks Manager (multiple wishes, e.g. profiles)\n- embedded quick view/thumbnail viewer\n\nAll of this is already implemented more or less in Konqueror. In my opinion it would be a waste of precious developer man power (it's not as if we would have too much developers) to duplicate all these features in Krusader just because the handling of Krusader and Konqueror is slightly different.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Konqueror"
    date: 2003-05-23
    body: "first, there is no code duplication. when krusader got started (almost 3 years ago), we had to implement things that were not available in KDE. as time went by, some things got implemented in kdelibs (and you wouldn't call that duplication, would you?). once we see that something we did, is implemented in the libs, we drop our code (unless there is a really good reason), and use kde's. this allows us to use a maintained code, avoid duplication and inherit every bug-fix that kde developers do. \n\nsecond, regarding the todo list:\n- integration with KGET (dl. manager): not a high priority, but among the LOWEST\n- embedded tree view: we plan to use kde's as-is, and again, not a high priority\n- use KDE's date format: simply a bug-fix\n- a new embedded SMB browser: i'm using kde at work, on a big windows-centric\n  lan. maybe it's just me, but i've yet to see a samba browser that works. \n  note that i know about komba, and already had talks with its author.\n- New Bookmarks Manager: again, integrate kde's which is much better\n- embedded quick view/thumbnail viewer: relates to a gui redesign issue that i\n  won't get into.\n- Tabbed browsing: not the same as konqueror's. twin-panel tabbed browsing \n  relates to having multiple view on the left side, and multiple on the right,\n  while still keeping the left ones and the right ones separated. completely\n  different.\n\nlast, notice that a twin-panel filemanager IS a different conecpt. if we'll take\na windows equivalent, you might say that using windows commander is the same as\nopening to instances of explorer, and placing them side by side. you could drag\nfiles between them, delete files etc. obviously, this is not the case.\n\n"
    author: "Shie Erlich"
  - subject: "Re: Konqueror"
    date: 2003-05-23
    body: "Thanks for the clarification. I'm glad that my concern about code duplication is unjustified.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Konqueror"
    date: 2003-05-25
    body: "I think the problem here are not the features. Just some people (Like myself) are used to the good old norton commander for DOS. I just do not like the explorer type file managers. In the windows world I use the Total Commander (former windows commander, his Billness asked them to change the name, after several years). When I use KDE, I use Krusader. \n\nMy point is, it's irrelevant that other file manager do the same, we just like Krusader more than others. Unfortunately, I'm a C++ 'disabled' person (ASM and some C) so I cannot help.\n\nRegards,\n\nJose\n"
    author: "Jose"
  - subject: "Re: Konqueror"
    date: 2003-05-27
    body: "> a new embedded SMB browser: i'm using kde at work, on a big windows-centric\n> lan. maybe it's just me, but i've yet to see a samba browser that works. \n> note that i know about komba, and already had talks with its author.\n\nEver try lan://?\n\nIt uses lisa, and lets you browse numerous useful services on a lan, like smb, ftp, http and fish."
    author: "goat"
  - subject: "Re: Konqueror"
    date: 2003-05-28
    body: "just tried it now, and the most i get from it, is a list\nof IPs on my network. i click one, select SMB and get nothing.\ni suspect it has something to do with workgroup, since when i mount\nmy share using smbmount, i have to specify my workgroup.\nanyway, listing IPs is not what i meant when i said smb browser.\n"
    author: "Shie Erlich"
  - subject: "Re: Konqueror - there *is* such a keyboard shortcu"
    date: 2003-05-23
    body: "In Konqueror, you select the files, hit F7, hit Enter, and it's done (F8 is move, F10 mkdir -- come on, you could have looked in to the edit menu ;-) . There is also support for ctrl-(+), ctrl-(-)  for wild card selection.  \n\nAs for the target pane: it asks for a confirmation of the directory when you do it, and with 2 views the destination is not ambiguous, is it?\n\n"
    author: "Sad Eagle"
  - subject: "Re: Konqueror"
    date: 2003-05-24
    body: "copying files in konqy: select the files, press F7 for copy, F8 for move\nif there are exactly two views open, the selected files will be copied/moved to the other view, if there is only one or more than two views open, a dialog will appear were you can select the destination.\nF10 is for mkdir\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Konqueror"
    date: 2003-05-24
    body: "... I should have scrolled down the complete thread brefore replying...\n\nSorry\nAlex"
    author: "aleXXX"
  - subject: "Re: Konqueror"
    date: 2003-05-24
    body: "Speaking of which, as soon as I load a profile, it kills all my current tabs.\nCouldn't the profile perhaps  be made an additional tab? \nIts not as if the new profile cannot work with tabs: Once in the new profile u can create  tabs again. \n\n  But it does seem like a bug that all my current tabs are thrown away."
    author: "KUser"
  - subject: "Re: Konqueror"
    date: 2003-05-24
    body: "Or atleast if thats difficult to code, may I suggest a warning box which says:\n\n\nWarning: all your current tabs will disappear.\n     [x] Do not  ask me again\n[Continue anyway] [Open in new window][Cancel]\n\n\nver:  KDE 3.1.1a\nThanks for a great product btw."
    author: "KUser"
  - subject: "Merge"
    date: 2003-05-22
    body: "What about a merger with Konqueror? It will become THE filemanager!"
    author: "Marc Tespe"
  - subject: "Re: Merge"
    date: 2003-05-22
    body: "\"What about a merger with Konqueror? It will become THE filemanager!\"\n\nI agree 100%. First of all: it is the developers' decision. If they want to go their way I fully respect it. But I still think it would be nice if they decided to work on the file management aspect of Konqui (which already is excellent). A merger would be a first step :-)\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: Merge"
    date: 2003-05-22
    body: "Konqueror is imho a Internet Explorer Clone, not a filemanager. The kitchensink full of bugs included."
    author: "Carlo"
  - subject: "Re: Merge"
    date: 2003-05-23
    body: "> I agree 100%. First of all: it is the developers' decision.\n\nNot only that, but krusader actually predates Konqueror. I've always loved krusader... I haven't used it in a while, but it'd be a shame for it not to have more developers. It fills a certain niche (light file manager) that Konqueror can't fill, imho. "
    author: "fault"
  - subject: "kpart"
    date: 2003-05-23
    body: "Instead of merging it with Konqueror why not make a kpart out of it?  This way it could be embedded in anything including Konqueror?"
    author: "Adam"
  - subject: "Re: Merge"
    date: 2003-05-24
    body: "Actually konqy can do almost anything, I don't see a way how something could be \"merged\". All what would have to be done for konqy would be maybe some more UI tweaks, konqy *can* do everything.\n-> open two views (left/right), change between them using CTRL+Tab, press CTRL+E to execute a shell command in the current view, press F7/F8 tp copy move files, F10 for mkdir, press Shift+Delete to delete files, press enter to view it.\nPress CTRL+\"+\" to select files, CTRL+\"-\" to deselect files, CTRL+\"*\" to toggle the selection of all files. Press F2 to rename a file.\n\nWhat is missing ?\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Challenge for students of computer science"
    date: 2003-05-22
    body: "Asking for help is an excellent iniciative.\nI can't think of a best project for a student than developing free software. It is a challenge (most of open source projects have really high standards of quality) and meaningfull to society. \nAnd even better when you have an open invitation."
    author: "cwoelz"
  - subject: "Looking for developers ?"
    date: 2003-05-23
    body: "Try DevCounter:\n\nDevCounter ( http://devcounter.berlios.de/ )\nAn open, free & independent developer pool\ncreated to help developers find other developers, help,\ntesters and new project members. "
    author: "Masato"
  - subject: "Krusader != Konqueror"
    date: 2003-05-23
    body: "Although I prefer konqueror I must say that krusader is a wonderful program with a very intuitive interface.  I find that new users (new as in new to computers, not new as in \"just came from windows) have a much easier time understanding how Krusader works.  Its a great project that I do not want to see disappear.  \n\nStrid..."
    author: "Strider"
  - subject: "Thanks b.t.w."
    date: 2003-05-23
    body: "Hey, this is a very nice filemanager and I would hate for it to disappear.\nI just installed it and tested it and I love it.  Konqui is much to heavy for me as a filemanager. Anyway, thanks to the developers. I'm a student myself, with no little time to spare (unfortunately), and I realize you guys must have put a lot of work in this project.\nI hope that you will find a couple of extra developers."
    author: "dwt"
  - subject: "Question"
    date: 2003-05-23
    body: "Does the FTP/network functionality use it's own code or the KIO support for these things?"
    author: "Luke"
  - subject: "Re: Question"
    date: 2003-05-23
    body: "krusader uses kio, and thereby has (almost) all\nthat konqueror has to offer (btw, thanks ;-)\n"
    author: "Shie Erlich"
  - subject: "Heads up from me!"
    date: 2003-05-23
    body: "On windows I used Servant Salamander all the time, which is a Norton-clone. The thing I love about this, is that you can do everything with the keyboard. While I like Konqueror as a browser, I never use it for file management as it get's too much in the way (same with Explorer).\n\nfor reference:\n  http://www.altap.cz/salam_en/index.html"
    author: "Anon Bastard"
  - subject: "Start selling the app"
    date: 2003-05-24
    body: "Your app is a good one. Very good! It's super light compared to the konqueror.\n\nHey, why don't you start selling your app? Then you will find a \"payed\" time for it and even more you can make a business!\n\nJust start selling the newer version, and when there is released a more recent the previous one becames free.\n\nFor example if you are now at 1.11 let start charging for 1.20,\nwhen you make 1.21, 1.22 etc they will be chargable too, and after making 1.30 - 1.20 becames free\n\nYou know nothing is free and the biggest fans would pay for the most recent version! And yes NOTHING is free, people should learn to pay for every single line written! You've spent a hundreds of hours for developing the app and noone from the \"community\" will pay your time.\n\nBtw you also can start at later point to increase the delta between the free and payed version and someday only 1.8, having 3.0 in the same time for money! Or even just make the app entirely commercial.\n\nJust be careful because we all know what happened to the old Quanta team that started selling it, but in the same time the \"free geeks\" continued the production of the free Quanta.\n\nMay be you have to think about changing the license to non RMS-GPL copyleft one, so making sure that you will protect your OWN intelectual property from the FORKERS."
    author: "Anton Velev"
  - subject: "Start supporting the developers"
    date: 2003-05-25
    body: "Pay the developers to continue your much liked apps, and tell your friends to do the same. There's no reason why to close a source just for being able to sell an application. Also developers keep their own intelectual property even with GPL while by going commercial you are exactly provocing the forking behavior you naively think to be able to avoid."
    author: "Datschge"
  - subject: "Krusader and Open Source"
    date: 2003-05-24
    body: "Krusader is a fine product.  The ecology of open source suggests that those bits of software that are useful to a large audience of users and developers survive.  I'm guessing that Krusader has a small audience and will eventually whither away.  It's a real shame for the guys that have poured their personal time and effort into this product and not have it succeed.  It would appear that Konqueror has been a better match to the needs of the largest audience (for now)."
    author: "Josh"
  - subject: "Re: Krusader and Open Source"
    date: 2003-05-24
    body: "trying to be objective here... krusader does not appeal to the konqueror-using audience. same as windows commander doesn't work for explorer-using guys. same as mc and nautilus. it's simply a different program for different people. i do think, however, that krusader does have an audience and seems to have a dedicated user base. i'd like to think that as long as there are people who needs an alternative over the explorer-like file manager, krusader will be there for them ;-)\n\nbtw: if you'd look outside KDE, you'll see a lot more programs which manage files differently than konqueror."
    author: "Shie Erlich"
  - subject: "Re: Krusader and Open Source"
    date: 2003-05-24
    body: "No, not at all.  Sometimes useful programs die too.  Like KIllustrator which was pretty damn good in the KDE1 days.\n\nIt is not fair to compare Konqueror to Krusader because Krusader does not get as big a distribution as Konqueror does.  KDE really needs to find a way to promote these \"outside\" apps more to distribution vendors, especially good ones like Krusader."
    author: "ac"
  - subject: "Re: Krusader and Open Source"
    date: 2003-05-24
    body: "well, in my old mandrake days, i noticed that krusader came on mandrake cds (although on the last one ;-). i think it's included with debian also, and i know about suse and redhat builds. obviously, gentoo (which i'm using today) has an ebuild. problem is, and not only with krusader, that a lot of good kde applications get no attention (or almost no attention) - unless someone is good enough to post something on the dot on their behalf. the problem, as i see it, is that users have a hard time finding out about kde programs that don't come in a one of the kde packages. as i remember, a lot of gnome people say that kde is nice, but gnome has more applications. regardless if this claim is truth or not, it wouldn't be such a bad idea to let the people know that kde has some apps to offer.\n"
    author: "Shie Erlich"
  - subject: "Re: Krusader and Open Source"
    date: 2003-05-25
    body: "> KDE really needs to find a way to promote these \"outside\" apps more to distribution vendors, especially good ones like Krusader.\n\nAnswer to this is kdeextragear. It'd be awesome if krusader was developed within kde-cvs in kdeextragear. They could still of course, release seperate versions from time to time on sourceforge. "
    author: "tr1p"
  - subject: "Does anyone else's KDE run slow?"
    date: 2003-05-28
    body: "I have a SuSE 8.2 installation with an Intel 2.56Ghz chip and a gig of memory, and my KDE installation is SLOW. For example, KEdit takes over 30 seconds to load and any of the SuSE config applets takes 1-3 minutes to load and display. What are some of the reasons for this?"
    author: "The Stallion"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2003-05-28
    body: "It can be the DMA and other I/O params. Ever tried hdparm?\n"
    author: "ac"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2003-05-28
    body: "I think you have a screwed up /etc/hosts... KDE itself takes about six seconds to load here on my p4 2.4ghz. Most apps, like kedit, popup within a second or two.. exceptions include Mozilla, and OpenOffice. I'm using gentoo with prelink, GCC 3.3, linux 2.5.70-mm1, nptl."
    author: "ti"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2003-08-26
    body: "hell yes it is slow.  Slower than Winblows and that is bad.  KDE should be scrapped."
    author: "ter"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2003-08-26
    body: "what's slow about Windows?\n\nBoth Windows(2k) and KDE are pretty fast on my P3 (Katami)/500"
    author: "ANON"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2003-10-18
    body: "Well I guess you might be right, and that is my point.  All the Linux people say how Windows sucks etc, but KDE sucks really really really bad.  IT IS SSSSSLLLLLOOOOOWWWWWW."
    author: "ter"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2003-10-18
    body: "Hmm.. KDE is not slow. Modern versions of KDE are roughly comparable to XP in speed. "
    author: "anon"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2003-11-01
    body: "I have SUSE 7-1 or 7-2 I would have to look.\n\nIs that a modern version?"
    author: "TER"
  - subject: "Re: Does anyone else's KDE run slow?"
    date: 2008-04-24
    body: "Actually IT IS SLOW and it is not comparable to xp in speed. I'm a linux enthusiast but when's something is just not right it isn't and kde (Any version btw) is just slow. \nI think it's got to do with the opengl which is not part of kde. Rather kde uses some other software graphics accelerator I think it's xfgl or something named like that. \nHowever, anyone knows how to fix this? "
    author: "Tony B"
  - subject: "Nice... It's now my main file manager"
    date: 2003-05-28
    body: "Before this, I used emelfm (http://emelfm.sourceforge.net/) for file management.  Emelfm does some things better than Krusader, but Krusader also does some things that Emelfm doesn't do or doesn't do as well.  In addition, although Emelfm a nice, clean program, but I like having a small, working Qt/KDE equivalent that fits in with my KDE desktop a little better. (Konqueror doesn't seem to be very stable on my computer, it's bigger than I want for file management, and I can never remember how to get the two panel mode to work correctly.)\n\nAll in all, I think I like what I've seen so far of Krusader.  Keep up the good work! (or find the help needed to do so) :)"
    author: "Dhraakellian"
  - subject: "Re: Nice... It's now my main file manager"
    date: 2003-05-28
    body: "wait till you see what we're doing now ... \n;-)\n"
    author: "Shie Erlich"
  - subject: "How long till we see Marking in Konqueror?"
    date: 2003-05-29
    body: "I've just downloaded Krusader and am getting acquainted with it.  It looks like Krusader lets you mark groups of files (maybe even save and load lists of marked files).   So how long until we see a tagging facility in Konqueror's file manager?\nIt might be more general, like the 'emblem' feature in Nautilus, so that you could tag a file into several groups at once.   \n\nI organize my FAQs, school files, and drawings and it would be MUCH easier to move things around if they're tagged -- otherwise, if I slip up on the mouse and make a stray click, I lose my whole selected list. \n\n\nRoey"
    author: "Roey Katz"
  - subject: "Re: How long till we see Marking in Konqueror?"
    date: 2003-05-29
    body: "krusader DOES allow you to save preset selections."
    author: "Shie Erlich"
  - subject: "Re: How long till we see Marking in Konqueror?"
    date: 2003-05-30
    body: "Thanks!  I have been waiting for something like that for some time.  Now only if Konqueror supported it...\n\n\n\nRoey"
    author: "Roey Katz"
  - subject: "Re: How long till we see Marking in Konqueror?"
    date: 2003-06-02
    body: "On what level are preset selections saved (ie. is that feature easily reusable by other (KDE) apps)?"
    author: "Datschge"
  - subject: "Amiga Directory Opus Magellan II"
    date: 2003-06-04
    body: "Was the single best file manager I've ever used, represented the pinnacle of years of continuous refinement of high-volume-file-management user interfaces  that took place before the Amiga died.  \n\nApparently, the company that made it now make a windows explorer.exe shell replacement with the same name, but I haven't used it.\n\nPeople dissatisfied with konqueror or krusader might want to check it out for ideas on how to solve UI problems of mixing the best of both.  It combined the best features of twin-pane managers and finder-style managers - the key trick was windows could be designated a source or destination.\n\nYes, the look is dated (90s-Amiga) - the feel is not:\nhttp://www.gpsoft.com.au/DOpusMagellan.gif\n\n"
    author: "Anonymous Cowherd"
---
<a href="http://krusader.sourceforge.net">Krusader</a> (<a href="http://krusader.sourceforge.net/index.php?nav=scr.php&s=4">screenshot</a>) is a twin-panel file manager for KDE, patterned after old-school managers like Midnight Commander and Norton Commander. It features basically all your file-management needs, plus extensive archive handling, mounted filesystems support, ftp and much much more.  So far, the project has been developed by two developers, whose time is now not enough to continue the rapid pace of development. If you're a developer and you're interested in Krusader, we <a href="https://sourceforge.net/tracker/?atid=560746&group_id=6488&func=browse">need your help</a>!
<!--break-->
<p>
Since we're not students anymore, our day jobs prevent us from dedicating the same time into Krusader as we did in the past. We've come to understand that although we have many plans and ideas for Krusader, it will take us a long time to complete them. Development will slow down (as it has for the last 5 months), and new versions will come out more slowly. This is obviously not a desirable situation.
<p>
Basically, we need more developers on the project, so that when we have a "down-time", development will not stop. Our <a href="https://sourceforge.net/tracker/?atid=560746&group_id=6488&func=browse">TODO list</a> for the upcoming versions is
on-line on the website, and everyone can have a look, and decide if it is interesting enough to join. If you are a developer and want to join, please send an email to the <a href="mailto:krusader@users.sourceforge.net">team</a>, and write a few words about who you are, what you'd like to do for the project, and what you've done in the past regarding programming.
<p>
Thanks for your time!