---
title: "Interview with Jeroen Wijnhout of Kile"
date:    2003-10-03
authors:
  - "binner"
slug:    interview-jeroen-wijnhout-kile
comments:
  - subject: "Some more questions"
    date: 2003-10-03
    body: "Kile is a very nice program. I'll probably not use it since I've grown too accustomed to vim, but I can see that for many people kile can be really useful. The fact that data plotting is integrated is a nice plus. Kile looks a lot like WinEdt, a program for windows that makes editing LaTeX text easier by providing convenient buttons for quickly accessing commands.\n\nI'd like to ask some more questions that are not directly KDE related but related to scientific text editing.\n\n- When editing text for LaTeX there a number of tools already. These tools can be placed in two categories: WYSIWY[MG] and plain text editors with easy buttons. In the first category one has LyX and TeXmacs, the second, one has vi, emacs, kate etc. What is the unique selling point of kile for being chosen as a LaTeX editor?\n\n- Do you think LaTeX will be replaced by an XML based language soon? Do you think it would be desirable, since the LaTeX syntax is so inconvenient in several places (\\alpha, \\beta are pre-unicode relics, adding spaces with ~ is silly and a space after a command is ignored).\n\n- Will you implement a possibilty for unicode symbols to be shown as unicode symbols instead of \\alpha commands? This would be a real benefit. I'm using a vim plugin for this but it's not really perfect (you need a unicode aware terminal or [kg]vim).\n\n- What do you think of data plotting solutions under linux at the moment?\n\n\n\n\n"
    author: "Jos"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "> - What do you think of data plotting solutions under linux at the moment?\n \nI second that.... \nThat's what bugs me most at the moment. Excel may completely unsuitable for plotting graphs, but so many people are using it even for scientific work, because it's just sooo easy to use. Excel is far more responsive than OO.calc and has the nice option to put a chart on a single sheet (what OO.calc is missing...)\n\n I've been using QMatplot (very nice program) for some time now, but it's main (and only?) developer just disappeared without a word or so and was never seen again... so QMatplot seems to be a dead end by now. Gnuplot is what I use most at the moment. It's a working horse. it just works and you can feed it with very large amounts of data. It eats matrices of 5000 x 5000 points without a burp... and output is good quality as long as you don't want to have colors in the game... How do you get a colored 3D-surface plot with gnuplot using colour scale for the heights?\n\nYeah... I know there'S OpenDX (http://www.opendx.org/) but that's just like breaking a fly on the wheel..."
    author: "Thomas"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "IMO, what we really need is a really good GUI frontend to open source Numerical and Algebraic Computing Systems (octave, maxima, yacas).\nI know there are some frontends (even kde ones) around, but I stress the \"really good\". It would be a killer application and IMO it is so important as a providing a good office suite.\n\nIf, on one hand, there are more typical potencial office users than NAS/CAS users, the potential users of NAS/CAS are still many and easier to reach because there are no open source alternatives and they are an easier public (more tech).\n\nAgain I stress that I'm talking *very good* GUI, not just another simple frontend.\n\nJ.A.\n"
    author: "jadrian"
  - subject: "Re: Some more questions"
    date: 2003-10-06
    body: "Actually, I did start writing a unified front end for Yacas, Octave, Macaulay2 and Singular. You can find it under kdenonbeta/frontman in KDE CVS. The development on it is temporarily halted until the kmdi API in kdelibs stabilizes since the current interface based on KJanusWidget is not extensible. The idea is to create something similar to KDevelop, except that it is geared towards interpreted languages.\n\nOne wish I have is for khtml to display MathML. I tried to write support for it but it is very hard to extend khtml that way. Ideally, I would like for khtml to load a library on demand when it sees an embedded objects of certain types such as MathML, SVG, etc. This way one can operate on the DOM tree easily.\n\nOn QMatPlot: I sent the author several bugfixes for the widget library, but I did not receive any response. In case anyone is interested in the bugfixes, please email me privately. Note that I fix bugs on in the widget library and not in the app itself. If anyone wants to take over maintainership of the application, I'd be glad to lend a hand with widget library part.\n\nRegards,\nRavi\n"
    author: "Ravi"
  - subject: "Re: Some more questions"
    date: 2003-10-11
    body: "I only saw your answer now :)\nIt would be great to have such an unified front end available!\nAbout MathML, in case you're interested there is already a whishlist report:\nhttp://bugs.kde.org/show_bug.cgi?id=30526\n\nJ.A."
    author: "jadrian"
  - subject: "Re: plotting solutions?"
    date: 2003-10-03
    body: "I've been working on a scientific visualisation program for the past year that I hope will become part of a new KDE module (KDE Engineering). It is currently called KVisualiser and is a combined high level plotting program and low level scientific visualiser, using the VTK library for graphics.\n\nIf anyone is interested, I should be making an initial announcement in a week or so. I have started a website at kvisualiser.free.fr, and there is information there on the design and the code that I have written so far. I am (of course!) looking for collaborators, and I think this is going to end up being a biggish project. It currently stands at about 20k loc and there is a long way to go yet. I'd appreciate any comments."
    author: "nic"
  - subject: "Re: plotting solutions?"
    date: 2003-10-03
    body: "It seems you've already invested a fair amount of work into it. I'm looking forward to the announcement and the source-release..."
    author: "Thomas"
  - subject: "Re: plotting solutions?"
    date: 2003-10-03
    body: "> I've been working on a scientific visualisation program for the past year that I hope will become part of a new KDE module (KDE Engineering)\n\nHow about finishing and releasing it stand-alone first? And a module is for more than one program and if there is a need for it."
    author: "Anonymous"
  - subject: "Re: plotting solutions?"
    date: 2003-10-03
    body: "> How about finishing and releasing it stand-alone first?\n\nAgreed.\n\n> And a module is for more than one program and if there is a need for it.\n\nNo, some rather big programs can get their own modules. \n\nperhaps kdeedu for this project though.\n"
    author: "anon"
  - subject: "Re: plotting solutions?"
    date: 2003-10-03
    body: "Well, the idea behind KDE Engineering would be to have a set of complementary applications. KV would just be the visualisation component for any application that needs scientific visualisation. Other existing applications would also link in, for example KDevelop could be used for the programmig environment component, etc..\n"
    author: "nic"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "> How do you get a colored 3D-surface plot with gnuplot using colour\n> scale for the heights?\n\nWith the pm3d plotting mode, see\n\nhttp://www.sci.muni.cz/~mikulik/gnuplot.html"
    author: "AC"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "hurray... drool.... Now if I just knew that 2 weeks ago it would have saved me a lot of trouble.."
    author: "Thomas"
  - subject: "Plotting on *NIX"
    date: 2003-10-03
    body: "Hi\n\nI am plotting quite a lot data and am using xmGrace:\n\nhttp://plasma-gate.weizmann.ac.il/Grace/\n\nThe GUI _sucks_ but the app itself _rules_. And \nin case somebody has to much spare time: xmGrace is GUI-independent,\nyou could simply kick Motiv or whatever it is and write a nice\nQt-Frontend for it."
    author: "Carsten Niehaus"
  - subject: "Re: Plotting on *NIX"
    date: 2003-10-05
    body: "Yes, I also did a lot of plotting using xmgr. Leaves with approx. 1 million open windows, but gives great results, nice graphs and can do things like digital filters, FFT, histograms, numerical integration and other stuff.\n\nI'd recommend the \"old\" version of xmgr (where the development has stalled), instead of the new one, the last time I tried it was quite buggy and the UI was \"different\", but not significantly better.\n\nA KDE frontend would be a nice thing :-)\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Some more questions"
    date: 2007-07-26
    body: "Hi, Does any know how to plot two x-axis with one y-axis?..for e.g., if i have one set of y values and one set of x values....one can plot these values in xmgrace...but in addition i need to multiply all x-values by a number and then want to plot it in the same plot...i.e., two x-axis (one on top and other in bottom) and one y-axis..\nthanks\nraj"
    author: "Raj"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "This might be a good future alternative to LaTeX:\n\nhttp://tbookdtd.sourceforge.net/\n\nIt's a DTD for scientific texts that allows for figures and math.\n"
    author: "Jos"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "This might be a good future alternative to LaTeX\n\nand if I did not overread something important it's merely an additional abstraction layer to LaTeX which itself is already a layer for TeX... \n\nIt's main advantage may be direct html output. Of course you can use latex2html for that purpose, though I don't know if tbook may convert formulas to mathml (latex2html will output pngs to display equations and math symbols)\n"
    author: "Thomas"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "Formulas in tbook _are_ mathml. So tbook is not just an abstraction of latex, it's a document type for scientific texts. These texts can be converted to LaTeX, but that's just one of the possibilities.\n"
    author: "Jos"
  - subject: "Re: Some more questions"
    date: 2003-10-08
    body: "It may be in future, but what does one use to edit stuff written in tbook?\n(A text editor is sufficient for document production in LaTeX and an augmented editor such as WinEdt makes things that much easier.  The same cannot be said of tbook since you can't edit mathematical notation in any efficient manner (drag and drop with the mouse is not efficient, nor acceptable in the face of having LaTeX as an alternative.)\n\nJohn."
    author: "John Allsup"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "Not the author but I'll comment anyway :)\n\n\" - When editing text for LaTeX there a number of tools already. These tools can be placed in two categories: WYSIWY[MG] and plain text editors with easy buttons. In the first category one has LyX and TeXmacs, the second, one has vi, emacs, kate etc. What is the unique selling point of kile for being chosen as a LaTeX editor?\"\n \nI'm an xemacs user :) anyway I know that for many people it would be better to have a native KDE application for editing LaTeX. It's a matter of look and feel consistency, integration capabilites, and using kde specific features like working via ssh! I really miss that in Xemacs.\n\n\" - Do you think LaTeX will be replaced by an XML based language soon? Do you think it would be desirable, since the LaTeX syntax is so inconvenient in several places (\\alpha, \\beta are pre-unicode relics, adding spaces with ~ is silly and a space after a command is ignored).\"\n\nI don't see how both these aspects are related.\nIn the beginning you also couldn't use any isolatin characters in LaTeX, some editors made the task easier for you by accepting isolatin chars and converting the buffer to the equivalent ASCII LaTeX codes, now by importing the appropriate package you may use themdirectly. I hope that in the future I might use unicode with LaTeX too.\n\n\" - Will you implement a possibilty for unicode symbols to be shown as unicode symbols instead of \\alpha commands? This would be a real benefit. I'm using a vim plugin for this but it's not really perfect (you need a unicode aware terminal or [kg]vim).\"\n\nAgree, this is not a request for kile though, but for kate.\nI've made such a whishlist report (58340). Details here:\nhttp://bugs.kde.org/show_bug.cgi?id=58340\n\n\nJ.A."
    author: "jadrian"
  - subject: "Re: Some more questions"
    date: 2003-10-03
    body: "The vim plugin I use (and wrote) is available here. It has a useful list of characters and a description (in the code) of how the codes may be replaced reversibly, which is important.\n\nhttp://www.vandenoever.info/software/vim/\n"
    author: "Jos"
  - subject: "Re: Some more questions"
    date: 2003-10-04
    body: "Very nice. Mind if I add your url to the whishlist report?"
    author: "jadrian"
  - subject: "Re: Some more questions"
    date: 2003-10-04
    body: "Of course not! I'm honoured. :-)"
    author: "Jos"
  - subject: "Re: Some more questions"
    date: 2003-10-08
    body: "Regarding \\alpha, etc.  What is probably wanted is an easy way to get between\ne.g. ASCII* only and unicode representation (round trip capable at that.)\nThe reason being that it can be inconvenient to need to take your hands off the keyboard in order to insert symbols:  for example, I can type \\alpha in the time it takes to get my right hand off the keyboard to the mouse (before I've done any hunting around to find the right symbol.)  \n\n* When I say ASCII, I really mean just what can be input using the keyboard, and using as little inconvenient EscapeMetaAltControlShift stuff to get out the stuff used most commonly (e.g. greek characters, math symbols, etc.)\n\nSo far as an XML language is concerned, the problem is input.  You don't want to be using a text editor to typeset large technical documents using XML, unless the editor takes care of it for you.  The XML stuff should require less typing of \\commands\\ than \\LaTeX\\ does (which is IMO too much.)  Then e.g. MathML is impossible to input quickly at present (compare the time with that required to type the LaTeX input for an equation: e.g. with a few simple macros, \nsomething like \\set{x\\stc x\\not\\in x} is possible.  The ability to write macros like this is a great 'programmer accelerator' for using LaTeX, and this should be present in whatever follows LaTeX (else like e.g. MS equation editor, or KOffice's equivalent, inputting maths will be too cumbersome.) In short, the move to a _convenient_ XML language will necessarily preclude the possibility of editing documents using a text editor, and this in turn will make editing applications more complicated, more or less requiring an IDE of some sort for practical usage.  This will, of course, hinder making it widespread.\n\nA lot of thinking needs to go into not just the document format of an XML replacement for LaTeX, but the entire workflow of document production from editing to printing to web-publishing, and have usability issues thought through properly.  This will not be a quick thing, nor will it be any less complicated than what we have with Latex, though its implementation will probably suck a little less (p.s. yes, I have written and modified LaTeX packages, and delved around in the internals in order to get a few things done.)\n\nJohn.\n"
    author: "John Allsup"
  - subject: "Kile is great!"
    date: 2003-10-03
    body: "I'm a newbie.  When I realized I was actually capable of creating my own pdfs, I was ecstatic.  I only recently learned how much more was possible with pdfs created with free software when using LaTeX.  Compared to the TeX tools, using Ghostscript just seems lame (I could be wrong -- this is just based on my limited investigations).\n\nKile has been a joy to use to mark up text that I then used pdftex and hyperref to create pdfs with hyperlinked table of contents... something that I never would have thought possible without buying an expensive product from Adobe!\n"
    author: "Mike"
  - subject: "Re: Kile is great!"
    date: 2003-10-04
    body: ">Compared to the TeX tools, using Ghostscript just seems lame to the TeX >tools, using Ghostscript just seems lame\n\nThe normal latex compiler actually generates  a dvi file and it's ghostscript that converts it to .ps and .pdf :) Ghostscript is not that lame\n"
    author: "notMe"
  - subject: "Re: Kile is great!"
    date: 2003-10-07
    body: "Can Ghostscript produce pdfs with hyperlinks and/or fillable form fields?"
    author: "Mike"
  - subject: "Re: Kile is great!"
    date: 2003-10-08
    body: "Ghostcript can produce pdfs with hyperlinks when using hyperref package properly."
    author: "Pato"
  - subject: "Re: Kile is great!"
    date: 2004-12-30
    body: "How can you use hyperref package? How is it use? Can you send me a code or suggestion on how to incorporate hyperlinks in PDF? Thank you."
    author: "Alex"
  - subject: "Textpad"
    date: 2003-10-03
    body: "I am a admirer of the editor program textpad under Windows that is a slim but powerful shareware text editor, i use it as default under windows. So I like it very much that Kile becomes more and more closer to Textpad in every version. But a execution of a compiler or java sdk is still better in textpad implemented.\n\nHowever I wonder why there are three editors under KDW when kile already serves me best.  "
    author: "Heini"
  - subject: "Re: Textpad"
    date: 2003-10-03
    body: "Are you sure you mean kile and not kate?\n\nkile=powerful latex editor\nkate= powerful editor"
    author: "anon"
  - subject: "Re: Textpad"
    date: 2003-10-03
    body: "If you're speaking of kate (and not of kile):\n\nkedit is gone in newer versions of KD_E_.... it's obsolete\nkwrite and kate use the same editor component, but kwrite follows the 'just a basic editor' approach, whereas kate is more and more becoming a lightweight IDE..."
    author: "Thomas"
  - subject: "Re: Textpad"
    date: 2003-10-03
    body: "> kedit is gone in newer versions of KD_E_.... it's obsolete\n\nHu? It's still there in current HEAD."
    author: "Anonymous"
  - subject: "Re: Textpad"
    date: 2003-10-04
    body: "> kedit is gone in newer versions of KD_E_..\n\nkedit is very much alive, mainly because kate/kwrite doesn't support BiDi"
    author: "anon"
  - subject: "Knoppix"
    date: 2003-10-03
    body: "I tried it under Knoppix, but it didn't work, got error messages I could'nt resolve.\n\nI also miss a kind of \"rapid action\" options, I think Kile shall be delievered with a few sample texts in order to explore the tool. It makes it easier to start as a beginner or to check whether your program is wrong configured OR you just didn't get it, made mistakes ecc.\n\n"
    author: "Wurzelgeist"
  - subject: "....."
    date: 2003-10-04
    body: "Are you planning add a interface for use kile with others plot programs? like \nhttp://fung-calc.sourceforge.net or http://kmatplot.sourceforge.net"
    author: "anonymous"
  - subject: "Thank you"
    date: 2003-10-04
    body: "I just wanted to say thanks Jeroen and other developers involved with kile. I used it to write my thesis and it saved me hours of headaches going through the steep learning curve of latex (which was well worth it). I am even more grateful in that it allowed me to stop using ms word which is a nightmare program to try an write a thesis in. So thanks very much. Here are some other things I would like to see in kile:\n\n- A nice user friendly frontend to the makebst program that allows you to easily define your bibliography style.\n- A package browser that will show a list of available packages on ctan with a short description next to each, lets you do searches and then automtically downloads and includes selected packages into your document.\n- A decent table and long table editor\n- A document options dialog that lets you manage common properties like line spacine, in text citation format, document font size, page numbering format, and the fancyheader options.\n\nMany thanks for a brilliant program!"
    author: "TimLinux"
  - subject: "Re: Thank you"
    date: 2003-10-04
    body: ">- A nice user friendly frontend to the makebst program that allows you to easily define your bibliography style.\nPlease file a wish for that on http://bugs.kde.org/\n\n> - A package browser that will show a list of available packages on ctan with a short description next to each, lets you do searches and then automtically downloads and includes selected packages into your document.\nYou mean like this wish? http://bugs.kde.org/show_bug.cgi?id=65360\n\n>- A decent table and long table editor\nPlease file a wish for that on http://bugs.kde.org/\n\n>- A document options dialog that lets you manage common properties like line spacine, in text citation format, document font size, page numbering format, and the fancyheader options.\nPlease file a wish for that on http://bugs.kde.org/\n\nThese are all good ideas (IMHO), but they will be lost if they are not inserted in the bugsystem.\n"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Thank you"
    date: 2003-10-04
    body: "Thanks - I have added them to the wishlist."
    author: "TimLinux"
---
Jeroen Wijnhout, the current maintainer of the TeX/LaTeX editor and GnuPlot frontend <a href="http://kile.sourceforge.net/">Kile</a> was interviewed for <a href="http://www.kde.de/">kde.de</a>'s monthly series "<a href="http://www.kde.de/appmonth/2003/kile/">Application of the Month</a>". The interview was conducted in English by Andreas C. Diekmann who kindly provided us with this English transcript. Read on for Jeroen's personal background, how he got involved and the upcoming features of Kile 1.6.
<!--break-->
<p><hr>
<p><b>How did you get involved in this project? What's your job in this project?</b></p>

<p>After using Kile for a while, I decided that I would like to implement some features and fix some bugs.  Therefore I contacted Pascal Brachet, the developer of Kile at that time, and offered him my help.  It turned out that he wanted to stop the development of Kile. So I got more than I bargained for, the project was in my hands now. Since Pascal used to work alone, I had to take over all his jobs, which means that I'm project manager and developer at the same time. But, many developers of the KDE team have helped me since then. However, if anybody is interested in taking over some of the project management tasks I would be very pleased.</p>

<p><b>What was/is your motivation to work in such a project?</b></p>

<p>Well, programming is alot of fun and even more so if you can work on a program that is used by many people all over the world. Editing TeX/LaTeX source files can be a pain sometimes and, since I love KDE so much, it would be a pity if there wouldn't be a tool available for KDE.</p>

<p><b>As I could see the previous developer was Pascal Brachet. Who else should be mentioned when we talk about Kile?</b></p>

<p>The list is pretty long. There are translators, packagers and a few people who provide patches. Very helpful are some of the more experienced developers of the KDE team, such as Laurent Montel. They guard over the quality of the code.</p>

<p><b>I think the next version will be 1.6. Which new feature or changes may we 
expect?</b></p>

<p>From the technical point of view, the difference will be huge. Many parts of the program are rewritten. But the most important change will be that the editor for Kile will be the same editor as is used for Kate. This editor is much more powerful than the current editor of Kile: flexible syntax highlighting, block selection, editing commands, to name just a few. A modest form of project management, a feature much wanted by the users, is also planned for version 1.6.</p>

<p><b>Is Kile sponsored in any way?</b></p>

<p>Kile is hosted on sourceforge.net. Sourceforge offers a location for a website, a download area, mailinglists and much more. The source code of Kile is located on a KDE CVS server. We do not receive any direct financial support, though.</p>

<p><b>Working with Kile requires knowledge in TeX/LaTeX thus it's not as easy to use as KWord for example. What would you say to convince someone to produce his documents with Kile? :-)</b></p>

<p>Certainly, it requires some time to learn TeX/LaTeX. But compared to KWord, for example, TeX/LaTeX is much more powerful. Some users do not need the power of TeX/LaTeX, they are invited to use KWord. If you do need these powerful tools, you want to make your life as easy as possible, by using tools that form a userfriendly shell around the command-line programs you need to prepare a 
document with TeX/LaTeX. Kile offers such a shell, but it also helps you editing the TeX-source files. For example, it offers a way to insert LaTeX commands very easily, jump to lines with an error in it or view a graphical representation of the structure of your document (sections, chapters etc.).</p>

<p><b>What are your favorite tools under KDE (apart from Kile ;-) )?</b></p>

<p>Apart for the development tools, such as KDevelop & Cervisia, I am fond of KMail and KOrganizer, they help me a great deal to manage the Kileproject.</p>

<p><b>Kile is published under the GPL and also another example that great software doesn't have to be expensive and that it even could be offered for free. What do you think about Open Source?</b></p>

<p>Since I come from an academic background, it is very natural for me to share my results with others. Scientists are used to sharing their results, openly and freely. It allows other scientists to build upon the results of others and make progress more quickly. This is similar to the open source environment, sharing your code with others allows the community to develop software more efficiently. In other words, I think open software is a very valuable asset. It prevents developer from having to reinvent the wheel every time they start a new project.</p>

<p><b>Do you have a "vision" or wishes for the "Desktop Of the Future"?</b></p>

<p>Ideally, in the future desktops will be based on "Open Standards", not neccessarily open software, although that would be great. Suppose for example that the document formats for MS Office and KOffice would all be based on the same open standard. Users could then share their documents without any problems, it wouldn't matter if you are using KDE and the other person is working on a Windows box. There is a role for the government here, if they would be the first to use software based on open standards, the rest might follow. Fortunately several European governments are starting to support open 
standards.</p>

<p><b>How does the hard- and software look like you're using?</b></p>

<p>I'm using SuSE 8.1 with the 2.4.20 kernel on a AMD Athlon XP 1800+. SuSE is great, because if comes with so many packages, just everything you need (and much more). For my work Mathematica is very helpful, but this program is not free and can be quite expensive if you don't work for a university.</p>

<p><b>Tell us a little bit about yourself. What do you do for a living? How do you spend your free time (if there's any ;-) )?</b></p>

<p>Well, I'm 26 years old and live in a small town nearby Amsterdam. The University of Amsterdam was so kind to offer me a PhD position a few years ago in the String Theory group of the Institute for Theoretical Physics. This leaves me little free time, and lately most of my free time was spent on the development of Kile. But if I'm not working on Kile I like to spend my free time on playing the guitar and cycling.</p>