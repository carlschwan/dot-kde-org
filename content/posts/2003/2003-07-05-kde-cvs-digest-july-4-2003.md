---
title: "KDE-CVS-Digest for July 4, 2003"
date:    2003-07-05
authors:
  - "dkite"
slug:    kde-cvs-digest-july-4-2003
comments:
  - subject: "Yeahhhh"
    date: 2003-07-05
    body: "Brazilian boy, the first this time.\n\nThanks Derek, great job.!!!"
    author: "Brazil Boy"
  - subject: "Re: Yeahhhh"
    date: 2003-07-05
    body: "What is the weather like in Brazil at the moment?"
    author: "foo"
  - subject: "Re: Yeahhhh"
    date: 2003-07-05
    body: "Quite foamy, or so I heard."
    author: "Haakon Nilsen"
  - subject: "Re: Yeahhhh"
    date: 2003-07-06
    body: "Sunny days !! After a long, long, long storm, where megacorporates cartel's was dealling the cards, we are looking for sunny aftertimes.\n\nThe softcorporates lost their control. Brasilian government is Free Software Partener now.\n\nThe biocorporates is lossing their control. GM frankenfood is under fire, voices in the wild talks about a great moratorium. The ilegal crops are constrained.\n\nThe warcorporates lost their \"filet mignon\". The militar expenses was frozen and international war trade was amputated.\n\nThe venoncorporates is lossing their control. Organic agriculture is increasing more than 100% each year.\n\nThis mean that we will be under troubled times with Mr. Oilcorporate Ambassador. We are living under the comercial retaliation phantom !!\n"
    author: "Jonh the XX"
  - subject: "KSVG commit"
    date: 2003-07-05
    body: "Oops, you got one of the KSVG commits wrong. I think its not by Benjamin Reed, but by Adrian Page. Other than that I am glad ksvg gets mentioned :)\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: KSVG commit"
    date: 2003-07-05
    body: "Fixed. I thought I had fixed that in the script, but I guess not.\n\nDerek"
    author: "Derek Kite"
  - subject: "Menu applet"
    date: 2003-07-05
    body: "The menu applet is so awesome! Put it in a small child kicker panel, and you can have the menubars in all apps autohide. I've been using the kmenubar applet in KDE 3.1 for awhile, but it had some bugs, and required patching kdelibs. I'm glad that it made it into KDE proper. However, in my CVS copy of KDE (dated June 28) the menu-at-top behavior is totally borked. First of all whenever Konqueror's menubar is turned off (with, say CTRL-M, as well as with the MacOS menubar option) the world \"Location:\" will get printed on whatever buttons happen to be on the far lest of t he menubar. Further, while the menubar applet works, when autohide is enabled, parts of the menubar (the parts with the text) will stay behind when the menubar is hidden. Anybody running CVS have the same problem?"
    author: "Rayiner Hashem"
  - subject: "Re: Menu applet"
    date: 2003-07-05
    body: "I had the \"Location:\" problem with it.. seems like a bug. I just put the Location item into one of the hidden toolbars and it seemed to work fine however. "
    author: "lit"
  - subject: "Re: Menu applet"
    date: 2003-07-05
    body: "John Firebaugh fixed that in CVS last night, I think (or so; time is kind of confusing when you have different timezones). \n"
    author: "Sad Eagle"
  - subject: "Re: Menu applet"
    date: 2004-01-18
    body: "Where can i download this applet?"
    author: "Electric"
  - subject: "Summary Feedback"
    date: 2003-07-05
    body: "Hi Derek!\n\nI'd like to chime in on the \"Thank You\" chorus as well. I really do appreciate your efforts with these digests.\n\nI'm in the middle of a large project at work that involves making our web pages work better with screen readers. As a result, I've become aware of some best practices that can really help those using assistive browsers have a better experience with the web. I thought I would point out one item in your summary that, with a minor change, would make the page work much better for these users.\n\nOne of the common features of screen readers is to display a list of links on the page. The screen reader will read the text between the <a></a> tags to the user. This means that the text between those tags is much more useful if it fully describes where the link takes the user.\n\nMost of the links in the summary are just fine. However, you're last link is just read as \"here\" to the users. This doesn't really help them understand what the links does. If you changed the link text to say something like \"Read the digest here\" it wouldn't be much effort, but it would make a big difference and would be very much appreciated.\n\nThanks again for the digests! I'm glad I could make even a small contribution.\n\nBest regards,\n\nDavid"
    author: "David Joham"
---
This week in <a href="http://members.shaw.ca/dkite/july42003.html">KDE-CVS-Digest</a>: News about a new patch collection in qt-copy module, the <a href="http://developer.apple.com/darwin/">Darwin</a> port of KDE and
<a href="http://quanta.sourceforge.net">Quanta</a>. Optimizations in <a href="http://svg.kde.org/">KSVG</a>, listview and iconview modes.
And the usual bugfixes and improvements throughout. Read it <a href="http://members.shaw.ca/dkite/july42003.html">here.</a>
<!--break-->
