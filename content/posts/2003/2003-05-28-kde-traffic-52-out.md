---
title: "KDE Traffic #52 is Out"
date:    2003-05-28
authors:
  - "rmiller"
slug:    kde-traffic-52-out
comments:
  - subject: "Broken link"
    date: 2003-05-28
    body: "The link in the introduction should read:\nhttp://kde-gossip.kicks-ass.net/\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Broken link"
    date: 2003-05-28
    body: "Sorry, I'll verify that and change it tonight."
    author: "Russell Miller"
  - subject: "Components"
    date: 2003-05-28
    body: "I wonder when KDE will come in components.\nLike kdebase-kpanel.rpm\nThat would be great.\nI heard MS is trying to do it for their next windows version..."
    author: "KDE User"
  - subject: "Re: Components"
    date: 2003-05-28
    body: "1. What would be your benefit? 0.20 $ hard disk space? Not sure whether that is worth the trouble and the large number of bugs that something like this would introduce (there would be hundreds of packages for KDE, and it would be impossible to test all possible combinations for dependencies). Some distributions have somewhat smaller packages than KDE provides though. And usually also a few bugs.\n\n2. I dont think that Microsoft's componentization would offer a more fine-grained selection than today's Linux distributions. They only do it so they can compete with the customizability of Linux, and they will rather have less components than more.\n\n3. What has this to do with the Kernel Traffic?"
    author: "AC"
  - subject: "Re: Components"
    date: 2003-05-28
    body: "That is one thing I don't understand about rpms and all other linux packaging systems. When I installed stuff on windows I usually got one package, and during the installation I could choose to or not install certain components, or simply go with the default.\n\nTake KDE games for instance. I don't want to have to install them all when I just want a couple of them! SuSE provides one package for each kind of games, arcade/boardgames/etc... What if I want just one of each?! But I don't want one rpm for each game, obviously!\n\n"
    author: "ST"
  - subject: "Re: Components"
    date: 2003-05-28
    body: "Yes, for games and apps it may make more sense. But not for most things in kdebase. And I doubt that you can install Windows without the start bar."
    author: "AC"
  - subject: "Re: Components"
    date: 2003-05-28
    body: "\"Yes, for games and apps it may make more sense. But not for most things in kdebase.\"\nSo what?\nmetarpm -i kdebase.mrpm = rpm -i kdebase.rpm\n\n\"And I doubt that you can install Windows without the start bar.\"\nSo do I, and your point is? \n\nmetarpm -i -nosub cardgames kde-game.mrpm \nmetarpm -i -onlysub arcade  kde-game.mrpm \nmetarpm -i -onlysub arcade cardgames.kpoker kde-game.mrpm\n"
    author: "ST"
  - subject: "Re: Components"
    date: 2003-05-28
    body: "Yeah, gee, good plan. THAT wouldn't make anything more complicated. Just suck it up and have all of the games or none. 73mb total in CVS right now. \n\nYou KNOW that this would prompt some other complainer to say \"I wish I could get all of the games in ONE package. That would make everything so nice. FOSS is so COMPLICATED!\"\n\nLet's just stick to what we're doing. If you don't want the games, delete them. Otherwise, pick up a keyboard and help us."
    author: "Joe"
  - subject: "Re: Components"
    date: 2003-05-28
    body: "\"Yeah, gee, good plan. THAT wouldn't make anything more complicated. Just suck it up and have all of the games or none. 73mb total in CVS right now.\"\n\nSorry maybe I wasn't very clear.\nSuSE 7.3 (which I'm using) only has a kdegames rpm package with all the games, (a bad choice in deed), and I based my example on that. I wasn't trying to \"say put them all in one package\". \nSuSE 8.2 does split it into smaller packages one for each kind of games, and I think that is better. I would still like to be able not to install all the games of each package and just select the ones I want. \n\n\n\"You KNOW that this would prompt some other complainer to say \"I wish I could get all of the games in ONE package. That would make everything so nice. FOSS is so COMPLICATED!\"\"\n \nYou see, it's not about put more or less stuff into a package. I'm talking about giving the user (admin) more options when installing stuff, optional 'sub-packages' seems usefull. So acording to my example, there would be the same packages that there are now, and a \"metarpm -i some.mrpm\" would do the same as \"rpm -i some.rpm\", so I cannot se how would that be more complicated to the end user (admin).\n\nThis doesn't apply to KDE stuff only, I'm talking about packaging system in general.\nSeems a simple and useful concept. If you don't agree, could you tell me why?\n\n"
    author: "ST"
  - subject: "Re: Components"
    date: 2003-05-29
    body: "You're not a developer, are you?\n\nI can tell, because of your assumption that the addition of extra and more extra would somehow be \"Better\".\n\nRPM is not really written to be modular for simple users. It just isn't. If a developer wants to split up the kdegames package, hey more power to her. BUT, unless you are willing to do the work, there just doesn't seem to be a great need for this. Besides the fact that most people seem more than happy to use it the way it is. If you don't like it, compile it from source and edit the Makefile to exclude the extra 20mb you are hoping to save.\n\nAll of this is of course, ignoring the fact that KDE has nothing to do with RPM packaging, so the debate is completely useless."
    author: "Joe"
  - subject: "Re: Components"
    date: 2003-05-29
    body: "\"You're not a developer, are you?\"\nI do program (a lot) but I wouldn't call myself a developer. \n\n\"RPM is not really written to be modular for simple users. It just isn't.\" \nThat is what I'm saying isn't it. RPM isn't modular, (for users, admins, whatever, rpm isn't modular). \n\n\"If a developer wants to split up the kdegames package, hey more power to her. BUT, unless you are willing to do the work, there just doesn't seem to be a great need for this. Besides the fact that most people seem more than happy to use it the way it is.\"\nNow you're assuming stuff. I know *lot's of people* who are not happy to use it the way it is.\n\n\"If you don't like it, compile it from source and edit the Makefile to exclude the extra 20mb you are hoping to save.\"\nGuess what, some of those users don't know how to do that, others know but since they're problem is lack of hard disk space this is not a solution, others simply don't  think it is worth the effort. \n \n\"All of this is of course, ignoring the fact that KDE has nothing to do with RPM packaging, so the debate is completely useless.\"\nI agree this is not the appropriate place. "
    author: "ST"
  - subject: "Re: Components"
    date: 2003-05-29
    body: "Don't give them strange ideas, otherwise for the next version, people will have to buy a separate package for $200 if they want to have a taskbar or start-menu ;-)"
    author: "uga"
  - subject: "Re: Components"
    date: 2003-05-29
    body: "> I don't want to have to install them all when I just want a couple of them\n\nThis is completely offtopic here...this is a SuSE packaging wishlist, not a KDE wishlist.. Try switching to a distro that splits KDE up into many packages, such as Debian, if you really want this."
    author: "ti"
  - subject: "Re: Components"
    date: 2003-05-30
    body: "This is offtopic in deed, but it has nothing to do with the way things are packaged.\n"
    author: "ST"
  - subject: "Re: Components"
    date: 2003-06-03
    body: "then why does debian provide this package \nsplitting yet your ass sucking sucky distribution \ndoes not?\n\nAlex"
    author: "lyp"
  - subject: "Re: Components"
    date: 2003-05-28
    body: "This is not something that the KDE Project is repsonsible for, it is the responsibility of the  packagers at the distribution. They can split up KDE into whatever packages they feel appropriate.\n\nDebian uses this approach."
    author: "Chris Howells"
  - subject: "Re: Components"
    date: 2003-05-29
    body: "> I wonder when KDE will come in components.\n\nwhen you switch to Debian :-)"
    author: "jaldhar"
  - subject: "Re: Components"
    date: 2003-05-29
    body: "> I wonder when KDE will come in components.\n\nIn subpackages from the KDE project? never. It's just too much of a burden for KDE release dudes. Perhaps if 50 more people stepped up to help in packaging. If you wish to help, email either the kde-devel or kde-core-devel mailing lists (second one preferably)\n\nIt's the distribution's job to split the big KDE packages into smaller ones, if they chose to. Debian does this, for example. You can apt-get install konqueror in it."
    author: "ti"
  - subject: "OpenSSL"
    date: 2003-05-28
    body: "I'm surprised at the mini-controversy over OpenSSL. The editor's note even suggests that it is non-free software! Huh? It's probably not GPL compatible on systems were it is not a component of the base system, but it definitely is Free Software.\n\nThe solution is obvious, and was the last one mentioned. Keep the Deb^H^H^H legalists happy by putting a disclaimer in the KMail license. I'll be sorely pissed if I have to install GnuTLS to use KMail when there's a perfectly good (and currently superior) OpenSSL as part of my base operating system."
    author: "David Johnson"
  - subject: "Re: OpenSSL"
    date: 2003-05-28
    body: "um , things aren't always clear cut.\n\nI wouldn't be surprised if they can't put that exception in, since they use all the qt widgets, and kde libraries, etc. So those wouldn't allow the linking, even if kmail does.\n\n"
    author: "JohnFlux"
  - subject: "Re: OpenSSL"
    date: 2003-05-29
    body: "Since Qt is under the QPL (along with the GPL), and kdelibs are under the LGPL, there is no problem in that regard. GPL-incompatibility is of concern only to GPL applications. So all you need are for the KMail developers to agree to a trivial disclaimer (which they have already implicitly agreed to by linking to OpenSSL to being with) and all the brouhaha goes away."
    author: "David Johnson"
  - subject: "Re: OpenSSL"
    date: 2003-05-30
    body: "Sorry, but your claim that we had already implicitly agreed to anything with regard to OpenSSL is wrong. KMail currently does _not_ link to OpenSSL. Stefan Rompf wrote a S/MIME crypto plugin that links to OpenSSL. But this plugin is not part of the official KDE release. Of course one can load the plugin with KMail. But that's not the point. Anyone can write a plugin with a GPL-incompatible license for KMail (as for any other GPL program that has support for plugins). But the mere existance of such a plugin doesn't automatically imply that we have to change the license of KMail. OTOH, if such a plugin would be part of the official KDE release this would probably change the situation."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: OpenSSL"
    date: 2003-05-28
    body: "The one fact that was *not* in contention in the thread that I covered was that openssl is not compatible with GPL software.  I profess ignorance as to the details and everyone could be wrong, but those were the facts that I was going by.  So, my comments about openssl being non-free were based on what I gleaned from the thread.\n\nIf someone can provide an explanation proving that that is incorrect I will modify the editorial to put that fact into circulation."
    author: "Russell Miller"
  - subject: "Re: OpenSSL"
    date: 2003-05-29
    body: "GPL compatibility is not the same thing as Free Software. There are several Free Software but not GPL-compatible licenses, including the MPL and QPL. The FSF has not addressed the free or non-free status of the openssl license. Since it is a minor variation of the Apache License, which the FSF lists as Free but GPL incompatible, I would make a strong assumption that the openssl license will wind up in the same category."
    author: "David Johnson"
  - subject: "Re: OpenSSL"
    date: 2003-05-29
    body: "> So, my comments about openssl being non-free were based on what I gleaned from the thread.\n\nOpenSSL is completely open and free software, but it's license isn't GPL compatible.. it is, however, usable with pretty much any other open/free license, such as LGPL, BSD, etc.. kdelibs, which is LGPL, links to openssl quite fine. \n\n\nThe conflicting conditions from the OpenSSL license are:\n\n\"* 3. All advertising materials mentioning features or use of this\n *    software must display the following acknowledgment:\n *    \"This product includes software developed by the OpenSSL Project\n *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)\n* 6. Redistributions of any form whatsoever must retain the following\n *    acknowledgment:\n *    \"This product includes software developed by the OpenSSL Project\n *    for use in the OpenSSL Toolkit (http://www.openssl.org/)\"\n\nThis conflicts with this in the GPL:\n\n\"6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\"\n"
    author: "ti"
  - subject: "dialogs.."
    date: 2003-05-29
    body: ">> http://www.tjansen.de/other/file_properties.png\n\nOnce again, _why_ aren't those dialogs in KDE designed by some person(s) who actually know what they're doing?\nMost of you guys are great programmers, but you're not great designers (as in visual design, the software design rocks).\n\nThe file properties dialog in the above screenshot wouldn't even fit on a 800x600 resolution, it's huge!, don't you guys realize there are still people out there working using that res?\n(and even on a res of 1024x768 it's way too large)\n\nAnyway, could the author pretty please try to use tabs instead of such a big dialog? :)\n(or perhaps some way to reuse those widgets instead of duplicating them 4 times, or whatever..)\n\nI really wish KDE would have some QA team to validate those dialogs (and window layouts)..\nIt's a shame that software which is technically soooo nice and built on a great toolkit, is so unusable like this.\n"
    author: "nac"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: "Nac, why not just select a different window style in Appearance & Themes? Works great for me.\n\nEven better would be people testing and modifying these themes on lesser resolution screens. (But, hark! Why isn't this happening already? Apparently not enough demand OR the people with smaller screen space are happy with the way things are OR ...)\n\nPlease, if you want to help with design, learn how to code a little and join the team. That's more constructive than posting here."
    author: "Chai"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: ">> why not just select a different window style in Appearance & Themes?\n\nDoesn't really make the dialog(s) much smaller though.\n\n>> better would be people testing and modifying these themes on lesser resolution screens.\n\nAgreed.\n\n>> if you want to help with design, learn how to code a little and join the team.\n\nCoding isn't the problem, free time is (unfortunately)."
    author: "nac"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: "Beside the style, you should also use smaller fonts. As you can see on the screenshot a simple checkbox with the text \"Only owner can change rename and delete directory content\" takes 340 pixels. Then put that into a frame and a tab, add some borders and use window decorations, and you got 435 pixels width.\n"
    author: "Tim Jansen"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: "I thought this too when I first saw it :-)\nThis screenshot is just a convenient summary of four different looks the dialog would display, according to the type of file. It's not at all the real dialog."
    author: "Aur\u00e9lien"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: "I just noticed that as well, and I'm terribly sorry for not looking further than my nose :)\n\nBut my point is still valid, most dialogs are too large and not designed correctly.\n"
    author: "nac"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: "1. This isn't a real dialog... it's a mockup of the four different ways the real dialog can look.\n\n2. Perhaps you should start a team that does QA work and tests dialogs.. you don't need much programming experience for this-- a lot of dialogs today in KDE are made in Qt designer, which means any reasonably experienced non-programmer can start making dialogs."
    author: "ti"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: "YES, I AGREE\n\nThis happened to me sometimes. Please, make the dialogs box something about 400x500, thats enough.\n\n"
    author: "vb"
  - subject: "Re: dialogs.."
    date: 2003-05-29
    body: ">>Once again, _why_ aren't those dialogs in KDE designed by some person(s) who actually know what they're doing?<<\n\nThanks for your trust :)\nI have uploaded a series of screenshots from the actual patch that should make it clearer. Same URL..\n\n\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: dialogs.."
    date: 2003-06-03
    body: "would be lovely to have a usability team that made quick decisions\nand came up with a good concensus, unfortunately i'm just not seeing\nthat and in most cases the good is the only thing ever talks.\n\nAlex"
    author: "lyp"
---
Get <a href="http://kt.zork.net/kde/kde20030525_52.html">KDE Traffic #52</a> at <a href="http://kt.zork.net/kde/latest.html">the usual place</a>. This week read the newsflash,  about the <a href="http://developer.kde.org/policies/commitpolicy.html">KDE CVS Commit Policy</a>, <a href="http://www.tjansen.de/other/file_properties.png">file permissions</a> and more.  
<!--break-->
