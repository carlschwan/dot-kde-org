---
title: "K3b 0.10: DVD Burning for KDE"
date:    2003-10-14
authors:
  - "strueg"
slug:    k3b-010-dvd-burning-kde
comments:
  - subject: "K3b = Best CD Burning App on Linux"
    date: 2003-10-14
    body: "Thanks Sebastian, DVD support was really the last thing I needed from K3b, I also have to say that by making K3b you have torn down one of the greatest barriers Linux has."
    author: "Alex"
  - subject: "Re: K3b = Best CD Burning App on Linux"
    date: 2003-10-14
    body: "I couldn't agree more!!\n\nk3b is THE burning program.  Keep up the great work!"
    author: "NickT"
  - subject: "Best CD Burning App on Linux"
    date: 2007-01-16
    body: "From my experience, it is not very stable.\nI think the problem is in QT library.\nAre there any other good CD burning programs ?"
    author: "Sergey"
  - subject: "k3b is great.. "
    date: 2003-10-14
    body: "I know a lot of people who use Linux but not KDE but use k3b. It's simply the best. \n\nIt's a wonder, because two years ago, there was a hodgepodge of different burning apps for KDE-> koncd, kreatecd, k3b, cdbakeoven, etc..\n\nNow, k3b has clearly outdone everything else, because of Sebastian's hard and continuous work. k3b is great! \n\nHope it moves from kdeextragear to kdeutils for kde 3.3/4 =)"
    author: "anon"
  - subject: "Re: k3b is great.. "
    date: 2003-10-14
    body: "Heck, move it in for 3.2.  Today k3b is easily at a quality level that makes it ready for a general user.  "
    author: "theorz"
  - subject: "Re: k3b is great.. "
    date: 2003-10-16
    body: "I seccond that.. put it in 3.2 guys!"
    author: "kidcat"
  - subject: "K3b rocks"
    date: 2003-10-14
    body: "I couldn't agree more with the above. K3b works like a charm and is a pleasure to use. Thanks :)"
    author: "Paul Eggleton"
  - subject: "k3b really rocks"
    date: 2003-10-14
    body: "OK - I know this is getting repetetive, but I also need to say it:\n\nk3b is a really, really great app. Bot functionality and user interface are of a very high class.\n\nThx a lot!\n\nJohan"
    author: "Johan Abrahamsson"
  - subject: "Yes it rocks, but..."
    date: 2003-10-14
    body: "Please, be less verbose by default, a dumb user like me is allways offended by the way burning is showed : lines explaining the way cdrecord is going should be hidden by default, with an expert button to show them.\nBecause I get warnings like \"I/O error occured, but it may not be a problem\", that just keep K3B away from being a real allday use application."
    author: "Jos\u00e9"
  - subject: "Re: Yes it rocks, but..."
    date: 2003-10-14
    body: "I agree, normal users don't usually want to be \"that\" informed. :)\n\nA progress bar with just basic information should take care of most users expectations.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Yes it rocks, but..."
    date: 2003-10-15
    body: "Offended?  Reminds me of Lrrr from Futurama:  \"We are CONFUSED and INFURIATED!!\" ;)  There's truth in humor."
    author: "Haakon Nilsen"
  - subject: "K3B"
    date: 2003-10-14
    body: "When I switched from Windows, K3B was just beginning to be good.\nI used CD BakeOven a bit but it had lots of problems. Audio CD were\nbroken afterwards and so on. Then I switched to K3B and I liked it \na lot more but it had lots of bugs so I kept CD BakeOven just in case.\nThat's not necessary anymore: K3B is easier than on Windows IMO.\nI have never had any problems with this. A big Thank You from me, too.\nOne improvement perhaps: When I switched from Windows I wanted\nto burn CD's right from the start. When I ran K3B Setup the page about\nusers really confused me a lot. I first thought I had to add \"root\" by clicking\non \"add user\" because the text said \"need to be run as root\". I know how\nit its meant of course right now, but perhaps it could be changed to something\neasier to understand for novices like:\n\nK3B uses auxiliary applications to burn a CD.Running these auxiliary \napplications require root privliges by default. So  by default only root\ncan burn a CD. K3bSetup recommends that you create a dedicated CD burning user group and add all users who want to burn CD's to that group. K3bSetup will\nthen change permissions to all auxiliary applications automatically so that\nthey are accessible for this group without root permissions. No other\npermissions than CD burning will result from being a member of that group.\nAll users in the list will afterwards be able to use K3B without ever having to\nenter a root password.\n\n\n\n"
    author: "Mike"
  - subject: "Re: root access and such K3B"
    date: 2004-05-13
    body: "Is there, or where is a \"How-to\" for the root/user/group question? This seems to start the answer, but not completly. I see the same question in many forms but few answers."
    author: "Jepjr"
  - subject: "Stupid question"
    date: 2003-10-14
    body: "What does K3b mean?"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "Did you read the story? \"the name does not really lead to anything\""
    author: "Anonymous"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "Oups, then that really was a stupid question :-)"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "Well he said it doesn't mean anything, so we'll believe him :)\n\nWhen I first discovered this piece of software, I too wondered what it could mean and my final guess was: \"Ku Klux Klan Burner\". Knowing how the KKK loves to burn other people's houses, it would make sense.\n\nSo, please tell me I'm being totally parano\u00efd and you're not one of the KKK's fans ! Please ! Or tell me that you want to burn the KKK members :)"
    author: "Julien Olivier"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "There is no Ku Klux Klan in Germany."
    author: "Anonymous"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "Yet, there might well be KKK fans anywhere in the world, sadly..."
    author: "Julien Olivier"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "lol\n\nNot really.... it's 3 bs: bbb. And it's way too annoying to tell what it stands for.... maybe you will get it yourself. :)\n"
    author: "Sebastian Trueg"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "K3b\n\n\nKDE big bad burner?"
    author: "Emmanuel Mwangi"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "that's not it.... but it's so much better... so I will probably go for this in the future... ;)\n"
    author: "Sebastian Trueg"
  - subject: "Re: Stupid question"
    date: 2003-10-16
    body: "3b = Bored Beyond Belief?\nBecause you were so bored that you just HAD to write a kewl program?\n"
    author: "kidcat"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "I know something like this in French, it's called the \"rule of the three B\", and it's not really elegant :-) Do I think about the right thing?"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "no. :)"
    author: "Sebastian Trueg"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "billen, borst en buik?\n\nThat's duch :-)"
    author: "me"
  - subject: "Re: Stupid question"
    date: 2003-10-14
    body: "Bastis Burning Bastard ? ;-)\n\n(as Basti is sometimes the nick form of Sebastian)"
    author: "Philipp"
  - subject: "Re: Stupid question"
    date: 2003-10-16
    body: "Well my guess is 'burn baby burn'! If the bs stand for something non-english, then I have absolutely no idea."
    author: "Joel Carr"
  - subject: "Re: Stupid question"
    date: 2003-10-16
    body: "grmff.... you got me.... ;)\n"
    author: "Sebastian Trueg"
  - subject: "Re: Stupid question"
    date: 2004-11-05
    body: "Got you because it's burn baby burn, or because it's not English?"
    author: "Anonymous"
  - subject: "Re: Stupid question"
    date: 2004-11-05
    body: "it is english. :)"
    author: "Sebastian Trueg"
  - subject: "Re: Stupid question"
    date: 2003-10-15
    body: "Well I always figured it stood for \"KDE 3 Burner\", but I guess this question has been answered already."
    author: "Paul Eggleton"
  - subject: "Re: Stupid question"
    date: 2003-10-15
    body: "Until I read this story I thought it might be KdE, d and E written backwards :)\n"
    author: "Kevin Krammer"
  - subject: "What about DVD-RAM? Would be cool for backup."
    date: 2003-10-14
    body: "Any thoughts?\n\nGreetings,\n      Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: What about DVD-RAM? Would be cool for backup."
    date: 2003-10-14
    body: "There is no need for that:\nJust try \"mkfs.ext2 /dev/dvd\" and \"mount /dev/dvd /mnt -o noatime\" (noatime to minimize writing on disk) and use that thing like a normal hard disk:\nYou can then use konqueror or rsync or whatever to copy data on that disk. \nAfter that \"umount /mnt\" and you are done.\nOf course you could also create an iso9660 file system, but that's boring ;-)"
    author: "Alexander Elbs"
  - subject: "Re: What about DVD-RAM? Would be cool for backup."
    date: 2003-10-15
    body: "I know that. But I ask about such things like MR (Mt. Rainier standard).\nWould be cool for migrations.\n\nThanks,\n   Dieter\n\nBTW I didn't used ext2 for ages...;-)"
    author: "Dieter N\u00fctzel"
  - subject: "Re: What about DVD-RAM? Would be cool for backup."
    date: 2004-01-28
    body: "I think what we are looking for is the ability to do full system backups/restores via k3d maybe alowing us to make bootable recovery dvd's as well all from a few clicks in k3d? (enabling some kind of archive compression as well?)\n\nThanks\n\nDave"
    author: "David Stanton"
  - subject: "Well I don't exactly have a CD burner..."
    date: 2003-10-14
    body: "But from the list of features and screenshots it looks better than software sold at shops!"
    author: "Tom"
  - subject: "Re: Well I don't exactly have a CD burner..."
    date: 2003-12-18
    body: "       Single parent three kids.Can't even provide christmas.Not looking for hand me down,just trying to be honest.If I had a cd burner I would copy what you want me to copy.\n"
    author: "Shonta Harrelson"
  - subject: "k3b surpass Nero and is the superNero on linux"
    date: 2003-10-14
    body: "yeah, now k3b is the greatest CD ( and now DVD ) buring prog I see.\nNow, if only we could have CD cover ... at least for audio CD ... sniff\nDo we have a good one under KDE ?"
    author: "Dark_Schneider"
  - subject: "Kover? "
    date: 2003-10-14
    body: "Try a serch on appsy. I tried Kover back on KDE 2.x.\nIt was nice, but I could newer get it to print anything \nbetter than 300dpi, so I gave it up :-). Will try it next \ntime I need to print a cover. "
    author: "Morty"
  - subject: "Re: Kover? "
    date: 2003-10-16
    body: "The kover is the best I have personally used, but there is also KCDLabel (check out http://apps.kde.com/) that works too...\n\nHopefully k3b has _integrated_ CD/DVD cover editor someday, and is included to the base kde distro!\n"
    author: "Nobody"
  - subject: "Re: k3b surpass Nero and is the superNero on linux"
    date: 2003-10-16
    body: "I'll add my vote in for this.\n\nThere is Kover which does the job, but is buggy and I'm not sure if it's under active developement.  I don't think it would be that hard for some friendly soul to clean it up and integrate it in.\n\nAny takers???\n\nJohn."
    author: "John"
  - subject: "Re: k3b surpass Nero and is the superNero on linux"
    date: 2003-10-16
    body: "Well, I had some discussion with the maintainer about making the DVD-cover (movie-size case) and to move to use kde.org's cvs...\n\nDidn't hear back from him about it after sending the idea and contact email addresses, so who knows, maybe it's still in the works?\n\nBut as all other major CD/DVD burners do have integrated label generator k3b should have one too!\n\n\nBy the way, is there any reason(s) why k3b isn't in any of the feature-plans, not even for kde-3.3?!?\n\nCould there be an voting system for kde-3.3/4.0 to see what packages people would like to see to move from kdenonbeta/kdeextragear(s) to the actual kde distribution?\n"
    author: "Nobody"
  - subject: "Re: k3b surpass Nero and is the superNero on linux"
    date: 2003-10-17
    body: "nero remain the best !!"
    author: "steve "
  - subject: "Re: k3b surpass Nero and is the superNero on linux"
    date: 2003-10-17
    body: "Nero is excellent, just like Easy CD & DVD Creator, but iDVD has been the best I have personally used!\n\nBut on the Linux side, k3b is IMHO the best compared to others usually included on distributions like CDBakeOven, Gnome Toaster, XCDRoast and so on...\n"
    author: "Nobody"
  - subject: "K3b and 2.6.x kernels"
    date: 2003-10-14
    body: "Well it wont even compile because of cdrom.h but even workarounding it , it wont even recognize and cdrom/cdrw device. Please fix this Sebastian!"
    author: "Cartman"
  - subject: "Re: K3b and 2.6.x kernels"
    date: 2003-10-15
    body: "This weekend I succesfully burned 4 DVDs with K3b from CVS compiled and running on 2.6.0-test4. However, /usr/src/linux still points to the kernel my distro came with, rather than the kernel I'm running right now. I also had to force some modules to be loaded (ide-scsi and sr_mod, I think)."
    author: "mth"
  - subject: "Re: K3b and 2.6.x kernels"
    date: 2003-10-15
    body: "Well first you do not use 2.6.x headers but I do. Second you wont be able to use ide-scsi for cd burning in 2.6.x because its broken and noone yet fixed it."
    author: "Cartman"
  - subject: "Re: K3b and 2.6.x kernels"
    date: 2003-10-16
    body: "I'm using k3b 0.9 (and recently 0.10) with 2.6.0-test6 and cdrtools-2.01_alpha18 (and without SCSI emulation - my CDRW is an ATAPI IDE unit). Works better than ever! :)\n\nNow, to get USB working..."
    author: "anonon"
  - subject: "Re: K3b and 2.6.x kernels"
    date: 2003-10-22
    body: "Any tips on how you did this?"
    author: "budgee"
  - subject: "Re: K3b and 2.6.x kernels"
    date: 2004-10-20
    body: "Hello.\nI'm begining to learn about KDE and Linux in general. I've just install SuSE 9.0 in my home machine and I have some problems whith the modem (Intel Data Fax Voice), audio (integrated) and the cd-rw unit (internal Polaroid BurnMax52) configuration. Particulary, when I start K3b 0.10 an error window appears warning me that my cd-rw is an ATAPI device, and that this K3b does not have a cdrdao version that supports this interface. I understand that I can run an SCSI emulator (because that is what the error window sugests me to do), but I don't know haw to do this. Can you please tell me what to do?"
    author: "Zaita"
  - subject: "setup suggestion"
    date: 2003-10-14
    body: "First of all, THANK YOU for k3b. It became my cdburn app of choice too. An let me also say thanks to the author of XCDRoast, which did a good job until k3b matured. \n\nAnyways, a humble suggestions: is it possible to achieve a smoother integration of k3bsetup and k3b ?. It gets confusing at times. May be borrow the ideas from kcontrol, where you can enter \"administrator mode\" by providing root password, whithin the MAIN application (k3b in this case). Also, the error messages could be worked out a bit. I once had a user in my machine not able to read. The error message was: \"could not find cdrecord or cdrao\" or something similar. There was a suggestion to run k3bsetup. The problem in the end was that the user was not included in the list of users able to use k3b. This was really confusing. \n\nWell, as I said, great work. Keep it going please !. Cheers :-)\n\n"
    author: "MandrakeUser"
  - subject: "Version 0.9 --> 0.1?"
    date: 2003-10-14
    body: "Please excuse my ignorance, but is there a typo?  How can we go from version 0.9 to version 0.1?  (This is not intended to be flamebait.)"
    author: "Anon"
  - subject: "Re: Version 0.9 --> 0.1?"
    date: 2003-10-14
    body: "from 0.9->0.10\n\ndecimal system: 9 to 10\n\nmaybe he should have just called it 1.0, but perhaps not time for that yet =)_"
    author: "anon"
  - subject: "Re: Version 0.9 --> 0.1?"
    date: 2003-10-15
    body: "So it should be called 0.10, because 0.1 is confusing."
    author: "Julio"
  - subject: "Re: Version 0.9 --> 0.1?"
    date: 2003-10-15
    body: "If you could tell us where it's called \"0.1\" then we could fix it."
    author: "Anonymous"
  - subject: "Re: Version 0.9 --> 0.1?"
    date: 2003-10-16
    body: "I guess the point is, in decimal notation, the numbers 0.10 , 0.1 and 0.100 are all the same, equal to 1/10. Of course, software versioning is a different thing, but still to the eye, 0.10 looks like 0.1 and gets confusing. I also prefer 0.8 -> 0.9 -> 0.91 -> 0.92 ... until 1.0 , with minor releases (such as 0.92.1) if needed inbetween. "
    author: "MandrakeUser"
  - subject: "Re: Version 0.9 --> 0.1?"
    date: 2003-10-15
    body: "0.9->0.10? I always thought it was 0.09 to 0.10.\n\nPerhaps 0.91 would be better.\n\nbye!"
    author: "Sendo"
  - subject: "Re: Version 0.9 --> 0.1?"
    date: 2003-10-18
    body: "I am the author of the original question, although not of any of the other posts in the thread.  I'm apologize for the hard feelings it seems to have caused.  I use K3B on my Gentoo box and absolute love it!  Thanks very much to the developers!"
    author: "anon"
  - subject: "KDE 3.2"
    date: 2003-10-14
    body: "Sebastian, since K3b seems to be such a killer app, any word on what needs to be done before it becomes part of the official KDE release?\n\nAny chance it could get into KDE 3.2?"
    author: "Navindra Umanee"
  - subject: "Re: KDE 3.2"
    date: 2003-10-14
    body: "<feature_freeze_rant>\nIt's not in the feature plan and thus it can't go into the official release before 3.3 even if the author wanted it to. It's in kdeextragear and that has it's reasons I guess. Why do all people want everything and the kitchensink in the base modules?\n</feature_freeze_rant>"
    author: "Daniel Molkentin"
  - subject: "Re: KDE 3.2"
    date: 2003-10-14
    body: "a cd burner app is certainly more pertinant to being in a base module than much of the stuff is in modules like kdeutils\n\nbut kde 3.3 sounds great if seb. wants it =)"
    author: "anon"
  - subject: "Re: KDE 3.2"
    date: 2003-10-14
    body: "Maybe 3.2.1 then...\n\nIt would be awesome to get it in the KDE distribution so that vendors pick it up.  That way a killer app on KDE will get wide distribution on Linux/Unix and become a killer app on a wider scale.  \n\nAnother widely-used killer app that uses the KDE framework would certainly be a good thing for KDE."
    author: "Navindra Umanee"
  - subject: "Re: KDE 3.2"
    date: 2003-10-14
    body: "Btw, the reason I'm interested is that I'm tired of hearing that KDE doesn't have any good apps...  If K3b is so good, then it deserves the chance to get enough exposure so that it can compete in such things as the Linux Journal Readers' Choice Awards...\n\nI'm not sure what kdeextragear is all about, but it doesn't seem to be working as well as GNOME Fifth Toe which has actual releases and announcements."
    author: "Navindra Umanee"
  - subject: "Re: KDE 3.2"
    date: 2003-10-14
    body: "I think many distributions already picked it up. (SuSE and Debian for sure, probably more ...)"
    author: "Oliver Bausinger"
  - subject: "Re: KDE 3.2"
    date: 2003-10-14
    body: "SuSE 9.0 has k3b 0.10, Mandrake 9.2 has 0.9 and RedHat has no k3b as it has no LyX and no Scribus."
    author: "Anonymous"
  - subject: "Re: KDE 3.2"
    date: 2003-10-15
    body: "Red Hat 9.0 accounts for about 83 computers in the McGill computer science lab, and FreeBSD with KDE 3.1 accounts for about another 83.  Of course these both have KDE installed and most of the stuff from kdemultimedia as well...  but K3b is certainly not there (all machines have CD burners)."
    author: "Navindra Umanee"
  - subject: "Re: KDE 3.2"
    date: 2004-01-02
    body: "I hate to say this, but Red Hat really can't be considered a \"normal\" Linux distro.  They went out on some unusual Microsoft-like limb and decided to do things their own way and disregaurded some things that have been standard in the Linux community for some time.  There is really no surprise that K3b is absent.  "
    author: "master alabaster"
  - subject: "Re: KDE 3.2"
    date: 2003-10-15
    body: "kdeextragear cannot be \"released\", it is just a home for different packages, each following its own release cycle.\n\nI think its exactly the place for applications like K3B.\nIt allows maintainers to access to KDE resources, e.g translators, while still allowing to have shorter release cycles than main KDE.\n\nBeing in KDE CVS makes them visible enough to distro packagers. I am sure they will package kdeextragear packages whenever there is a KDE release or a new release of their respective distro."
    author: "Kevin Krammer"
  - subject: "Re: KDE 3.2"
    date: 2003-10-14
    body: "3.2.1 will not contain any new features and more than ever applications compared to 3.2.0 as it's a bugfix and no feature release."
    author: "Anonymous"
  - subject: "Better than anything...."
    date: 2003-10-14
    body: "K3B is better than any commercial piece of burning software, including the venerable Nero. I was quite shocked when I first used it."
    author: "David"
  - subject: "Re: Better than anything...."
    date: 2003-10-15
    body: "While K3b is undoubtely the best on Linux it IS STILL NOT as good as Nero 6, but it is more intuitive."
    author: "Alex"
  - subject: "Re: Better than anything...."
    date: 2003-10-15
    body: "Hmm.. I like k3b better than nero now-days.. I wouldn't have said that even a few months ago BTW. k3b-cvs (i guess 0.10 is same) is amazing though."
    author: "anon"
  - subject: "Re: Better than anything...."
    date: 2003-10-15
    body: "Nero 6 is total pants. The previous versions of Nero were far better."
    author: "David"
  - subject: "First DVD"
    date: 2003-10-14
    body: "I just burned my first DVD in Linux. I've been waiting this new version for a long time since I had to use Windows to burn DVDs until now. K3B is totally awesome!"
    author: "janism"
  - subject: "Doesn't compile on Debian Sid !"
    date: 2003-10-14
    body: "I get this on my Debian sid with kernel 2.6.0-test6 !\nPlease solve this problem.\n\nThanks for your work !\n\nIn file included from /usr/include/linux/cdrom.h:14,\n from ../../../../src/core/device/k3bscsicommand.h:20,\n from ../../../../src/core/device/k3bdevice.cpp:23:\n/usr/include/asm/byteorder.h:38: error: syntax error before `(' token\n/usr/include/asm/byteorder.h:42: error: '__u64' is used as a type, but is not\n defined as a type.\n/usr/include/asm/byteorder.h:43: error: syntax error before `}' token\n/usr/include/asm/byteorder.h:44: error: syntax error before `.' token\n/usr/include/asm/byteorder.h:50: error: syntax error before `.' token\n/usr/include/asm/byteorder.h:51: error: syntax error before `.' token\n/usr/include/asm/byteorder.h:52: error: syntax error before `:' token\nIn file included from /usr/include/linux/byteorder/little_endian.h:11,\n from /usr/include/asm/byteorder.h:65,\n from /usr/include/linux/cdrom.h:14,\n from ../../../../src/core/device/k3bscsicommand.h:20,\n from ../../../../src/core/device/k3bdevice.cpp:23:\n/usr/include/linux/byteorder/swab.h:199: error: syntax error before `(' token\n/usr/include/linux/byteorder/swab.h:209: error: syntax error before `(' token\n/usr/include/linux/byteorder/swab.h:213: error: `__u64' was not declared in\n this scope\n/usr/include/linux/byteorder/swab.h:213: error: `addr' was not declared in this\n scope\n/usr/include/linux/byteorder/swab.h:214: error: variable or field `__swab64s'\n declared void\n/usr/include/linux/byteorder/swab.h:214: error: `__swab64s' declared as an\n `inline' variable\n/usr/include/linux/byteorder/swab.h:214: error: syntax error before `{' token\nmake[5]: *** [k3bdevice.lo] Erreur 1"
    author: "Flam"
  - subject: "Re: Doesn't compile on Debian Sid !"
    date: 2003-10-15
    body: "I have got the same compilation problem on Slackware 9.1 (Linux-2.4.22, gcc-3.2.3)."
    author: "Andrey V. Panov"
  - subject: "Re: Doesn't compile on Debian Sid !"
    date: 2003-10-18
    body: "Same problem compiling in Slackware 9.1"
    author: "Mrdengue"
  - subject: "Re: Doesn't compile on Debian Sid !"
    date: 2003-10-18
    body: "same on Slack9.1"
    author: "nordle"
  - subject: "Re: Doesn't compile on Debian Sid !"
    date: 2003-10-24
    body: "Same her on SID"
    author: "aprodigy"
  - subject: "Re: Doesn't compile on Debian Sid !"
    date: 2003-11-08
    body: "fixed in HEAD now."
    author: "Dirk S."
  - subject: "Re: Doesn't compile on Debian Sid !"
    date: 2004-03-17
    body: "HMMM... I just got the current HEAD of K3b... and it the problem looks very similar....\n\nLFS 5.0 & BLFS (and ofcourse modified for latest stuff).\n\nKernel: 2.6.4\ngcc 3.3.3\nKDE (CVS ... about a week old)\nK3B (newest HEAD)\n\n\nAttached is the output... \n\n\n\n"
    author: "Saad Malik"
  - subject: "Re: Doesn't compile on Debian Sid !"
    date: 2004-03-28
    body: "doesn't do it on slack 9.1\nbyteorder/swab.h\nasm/byteorder.h\nlinux/cdrom.h\nlinux/tcp.h\nlinux/if_ppp.h\n\nare all messed up. anybody know of a patch?\n"
    author: "name773"
  - subject: "FreeBSD support"
    date: 2003-10-15
    body: "It would be nice if k3b could run freebsd, and use burncd as well as cdrecord to burn CDs (burncd is used to burn cds on ide burners, unlike linux you don't need scsi emulation on freebsd)."
    author: "Smacker"
  - subject: "Re: FreeBSD support"
    date: 2003-10-15
    body: "Use a \"current\" Linux, then.\n2.6.x do not need ide-scsi anylonger.\n\nTry cdrecord on FreeBSD or maybe port the dvd+rw-tools.\nhttp://fy.chalmers.se/~appro/linux/DVD+RW/\n\nGreetings,\n      Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Downloads not avaible"
    date: 2003-10-15
    body: "It might be a sourceforge problem, but the 0.10 source files are not avaible on any of there mirrors. Any other download locations?\n\nSteinchen"
    author: "Steinchen"
---
Well, the <a href="http://k3b.sourceforge.net/cgi-bin/index.pl/news2">basic news</a> is quite short: <em>K3b is now able to burn DVDs.</em> But this would be a little too short. ;)  So for all of you who do not know <a href="http://www.k3b.org/">what K3b is</a> (the name does not really mean anything): K3b is (was) a CD burning application for KDE (<a href="http://k3b.sourceforge.net/cgi-bin/index.pl/screenshots">screenshots</a>). It uses <a href="http://www.fokus.gmd.de/research/cc/glone/employees/joerg.schilling/private/cdrecord.html">cdrecord</a> and <a href="http://cdrdao.sourceforge.net/">cdrdao</a> for actually burning the CDs but implements its own methods for all CD information retrieval functionality.
<!--break-->
<p>Now that this has been clarified I present the list of DVD related features in the new K3b version 0.10:
<ul>
<li>DVD-R(W) and DVD+R(W) support through the <a href="http://fy.chalmers.se/~appro/linux/DVD+RW/">dvd+rw-tools</a> by Andy Polyakov which is released under the GPL (yes, no strange security keys or patched cdrecord version for DVD burning anymore).</li>
<li>Support for all DVD-R(W) writing modes: <em>DAO</em>, <em>Incremental Sequential</em> (used for multisession), and
<em>Restricted overwrite</em> (use a DVD-RW just like a DVD+RW)</li>
<li>Automatic burning speed selection based on the media's capabilities.</li>
<li>DVD-R and DVD+R multisession burning (be aware that most DVD-ROM drives are not able to read multiple sessions)</li>
<li>DVD copy. No VideoDVD shrinking yet. That means you may not copy a DVD-9 (most VideoDVDs) to a DVD-4 (4,3 GB DVD
media). But this is planned and will be implemented in one of the next versions unless some legal stuff stops us.
Perhaps someone with a better legal background could help me here.... ;)</li>
<li>DVD-RW and DVD+RW formatting which can also be done automatically before burning.</li>
</ul>
</p>
<p>Before I get too many mails about this: There is no VideoDVD project <em>yet</em> but it is planned. I am just not
sure yet how to realize it from the users point of view.
</p>
<p>So if you own a DVD burner please check out the new <a href="http://www.k3b.org/">K3b Homepage</a> and get the latest
version (as of this writing it's 0.10).</p>