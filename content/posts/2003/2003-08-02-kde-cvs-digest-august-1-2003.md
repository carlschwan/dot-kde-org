---
title: "KDE-CVS-Digest for August 1, 2003"
date:    2003-08-02
authors:
  - "dkite"
slug:    kde-cvs-digest-august-1-2003
comments:
  - subject: "great"
    date: 2003-08-02
    body: "thank you!"
    author: "IR"
  - subject: "Object Pascal"
    date: 2003-08-02
    body: "Ruby bindings are cool. I'd also really like to see Object Pascal bindings for KDE. Any other Pascal fans out there?"
    author: "py"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "No, sorry :)"
    author: "Martin"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "Me! I like Pascal a lot more than C/C++.\nIt takes more time to write \"begin\" and \"end\" and \"then\",\nbut it produces a lot more readable source code IMO.\nI hate all those brackets in C."
    author: "Jan"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "how about:\n\n#define begin {\n#define end }\n#define then"
    author: "Johan Veenstra"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "No! Use real Pascal or write C++ in usual style. If one makes C++-code look too much like Pascal, one might forget that one is working with C++, not Pascal, and overlook the more subtle differences. And your simple example does not work with \"end if;\", \"end loop;\" and \"end <function or procedure name>;\""
    author: "Erik"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "Never seen \"end if\" and \"end loop\", must be relatively new syntax. Besides the defines weren't meant to make it look more like Pascal, but to make Jans code more readable to Jan.\n\nIf the dislike for a certain language can be solved by a couple of simple defines, then so be it. Anyway you get used to those { and } in no time at all. If the block structures aren't clear enough use bigger indents."
    author: "Johan Veenstra"
  - subject: "Re: Object Pascal"
    date: 2003-08-03
    body: "> Never seen \"end if\" and \"end loop\".\n\nSorry, it is Ada.\n\n\n> If the dislike for a certain language can be solved by a couple of simple defines, then so be it.\n\nI am sure that it would seldom work. It would obviously not work for Ada. Even if it would be possible to define \"end if\" and \"end loop\" to \"}\", it would not work, because the compiler would not check wether \"end if\" really matches \"if\" and \"end loop\" really matches \"loop\". Compiler checks are essential. And what if someone else will read the code? Someone who is used to the normal style of the used language? Better stick to the normal style.\n\n\n> If the block structures aren't clear enough use bigger indents.\n\nAlways use the horizontal tab character to represent a level of indentation. Then whoever reads the code can set the indentation as big as he wants by adjusting the tab width.\n\nBut the problem is not really the size of the indent. It is that a \"}\" does not show what is ending. Many coders would like to see directly wether it is a \"if\", \"loop\", procedure/function (and the name of it) or just a block. The \"begin\" is often far above the top of the screen."
    author: "Erik"
  - subject: "Re: Object Pascal"
    date: 2003-08-03
    body: "To get around that problem, I always try to add a small comment behind a } if the opening { is far away, to signify what it is that it is closing:\n\nif (OK) {\n....//more than a couple of lines of code here.\n} //if (OK)\n\nMakes the code more readable, IMO."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Object Pascal"
    date: 2003-08-03
    body: "> if (OK) {\n>  ....//more than a couple of lines of code here.\n> } //if (OK)\n>  \n>  Makes the code more readable, IMO.\n\nAs long as the comment is correct, but it is bad because the compiler does not check it. (I have coded like that in LISP.) After a few changes you may get\n\nif (OK) {\n  // ...\n  while (a) {\n    // ...\n  } //if (OK)\n}\n\nand the compiler accepts it. That's why Ada is superior in this aspect."
    author: "Erik"
  - subject: "Re: Object Pascal"
    date: 2003-08-03
    body: "Sure, that's possible. Still, it may help you. The new codefolding features are a help too, by the way. It will make the mistake obvious. The mistake you outline above could have been avoided if proper indentation would have been used... Still, I agree that the compiler can't check the code on this level."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Object Pascal"
    date: 2003-08-03
    body: "> The mistake you outline above could have been avoided if proper indentation would have\n> been used.\n\nMaybe, but no guarantee."
    author: "Erik"
  - subject: "Re: Object Pascal"
    date: 2006-04-29
    body: "> if (OK) {\n> ....//more than a couple of lines of code here.\n> } //if (OK)\n\nLooks ugly to me.."
    author: "L505"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "Object Pascal was my favourite language before I started to work on KDE/QT. Now I don't miss it anymore. :-) But I believe it would be good to see language bindings as there are many Pascal developers out there and they would enjoy it. (Yes, I know about Kylix, altough I didn't really work on it.) As far as I know Gideon has Pascal support, so it would be great if Gideon could be used to develop complete KDE application in Pascal. Correct me if it's already possible. \n\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "Me too! Object Pascal is my favorite and mainly language."
    author: "Jose Thadeu Cavalcante"
  - subject: "Re: Object Pascal"
    date: 2003-08-02
    body: "Here, here! :)\nObject pascal would be very nice.\nI would also really love to see Haskell bindings!! There an haskell GUI taskforce trying to come up with a standard GUI library for the language: http://haskell.org/pipermail/gui/\nIt would be a nice chance to have KDE as the standard Linux/*nix GUI for a language that is becoming quite popular :)\n\nSnx"
    author: "Snx"
  - subject: "Buy yourself kylix"
    date: 2003-08-04
    body: "Hi,\nGood that you like pascal. Why don't you buy yourself one kylix license, it's much cheaper compared to QT, moreover you receive a professional environment to use instead of using amateur GPL tools. Commercial made ones (vs free ones) will always be better.\nOn the other hand QT license is very expensive because of the GPL tirany. You guess what I mean - companies license under GPL and force you to:\n1) pay a lot of money\n2) never sell your app\nResist the GPL tirany and get yourself free GPL. Then for a regular free market competitive money you will get something real that you can use for producing commercial apps, and will never be afraid about selling your app, and that if GPL was used a freak could take your code and make a fork as it happend to quanta vs quanta-gold.\n\nThere is something broken in RMS thoughts and possible 100% GPL society. Everyone must realise that we are living in free market, and capitalist economy. It is this business to function if you are forced to open the code or letting it free.\nThere is a belief that companies like QT that force GPL tirany can survive, I would say that this is not exactly true. Following a trends of growing share of GPL apps will lead to always free stuff and one will never sell an app and not buying a commercial licence.\nExample (quanta vs quanta-gold):\nQuanta is GPL, it's free, and will not be sold, someone pays for development of quanta and it get's improved, one day it will became good app and a web designer will not consider buying some - this eats market share from commercial apps.\nQuanta Gold is Commercial app, for building it thekompany uses commercial QT license. If the free quanta gets much improved, people will not buy quanta gold and the quanta gold project will die. Then eventually thekompany will not renew one QT license - QT will lose one license.\nSimilar trends will happen to all comm vs gpl apps. Someday TT will not have enough power to maintain it's GPL lib and will let net freaks to fork it and make amateur GPL soft. QT will get bad. Even if this freaks write good soft TT will definitely die, as a whole commecrial QT industry as we have now.\nContinuing this trends you will find how one by one companies that \"beleive\" that can abuse GPL tirany will die (nobody realises that).\n\nPeople must sign a resistance against GPL and GPL tirany. Use real commercial apps/libs or BSD ones that not let you to GPL your code, or letting it free.\n\nI know, i know. You will definitely classify me as troll or M$ fan, but look at the reality, and please land to the real world. Think how could a GPL economy function, and think about one line code contributed to GPL soft how much lines of code from commercial soft die. About M$ - they are just a market winner at this moment, hating or not the monopoly will not help distorting one and switching to something GPL will not change the situation on the commercial market.\nI think that only Apple shows the right way - they demonstrate how it is possible to compete with M$ without GPLing something. They compete with M$ in the commercial market and successfully sell their product which is better than M$. Moreover they successfully reuse the real free technologies like KHTML and FreeBSD which are free of GPL.\n\nMaking something as GPL is:\n1) egoism (when opposing to BSD) because you do it for not letting someone to find a good commercial purpose of the code\n2) tirany (when selling a commercial licence) because you tell people always GPL or pay us a lot of money\n3) market destructive (when competing with commercial apps) because each line of GPL code probably eliminates 10 lines of commercial code (you are destroying the business, you kill some companies that make money)\n4) not Free (when you try to make soft) because it chains you with GPL and you are FORCED to GPL your code too\n\nIf GPL market share continues its growth something bad will happen soon. You can oppose it by not using GPL apps, not contributing them, not paying GPL tiran companies, letting something to be freeware without opening the code, buying alternative product to the monopoly product(s) (buy mac, buy solaris, buy c++ builder or delphy, buy staroffice, buy oracle, etc - give money to the commercial competition of the monopoly - they will improve and they know how to destroy the monoploy if they have finantial power).\n\nGPL monopoly is bigger danger than M$ monopoly. M$ realises that living in a commercial world they must maintain competitive marketing environment that will maintain their position. They are already letting some .Net components to be made by outside companies and also offering them on their site, a GPL monopoly will lead to absolutely non-existant commercial opportunity - in current capitalist free market economy of our world it will destroy a whole industry.\n\nFreedom. Some net individuals as RMS abuse this word in order to psychologically deformate the community in order to apply a their wrong ideas. GPL does definitely not mean Freedom - it means GPL tirany, GPL chaining, GPL egoism. Freedom is the FREEDOM OF CHIOICE, FREE MARKET, FREEWARE apps (no open code), BSD. The last one is free by the real means of freedom (along with the X license and Apache) because it gives you the Freedom to do whatever you want. About the FREEDOM of CHIOICE you definitely lose it when there is GPL monopoly - ask yourself how many HTML editor apps you have for KDE (where is your choice?)? Or how could a FREE MARKET exist in a commercial competitive economy where the quality must be the key, commercial app cannot coexist with GPL apps or environment, moreover it cannot survive.\n\nToo many thoughts were expressed, you have enough food for your brain, and I hope articles like this will help the rising anti-GPL resistance.\n\nBtw does anyone knows an entirely free of GPL but opensource OS or distribution? (I know about BSD flavors but i mean no (L)GPL apps at all - to do everything you can do with gnu/linux but with all BSD alternatives, for example GCC is a must-be-replaced in a BSD distro)."
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Are you being paid by Micro$oft ???"
    author: "Coen"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "I am not being paid by M$. But I guess you are not being paid by RMS too."
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Okay, Microsoft might be our enemy, bot no need to insult them."
    author: "ac"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "There's one tiny problem. Free software exists, and it is making progress all the time. Feel free to work on commercial (or BSD) software if you like. But other people are creating a huge GPL code base which makes re-using code easy and cheap. Writing a competitive piece of software is getting more expensive every day. 20 years ago a good assembler programmer was able to write a competitive word processor in a year or less. Today companies like Microsoft are putting hundreds or even thousands of man years into a word processor. The only way to compete with them is to invest a *huge* amount of money into the word processor, and live from selling licenses.\n\nThe free software alternative to adapt existing code to your (or your customer's) needs. You only get paid for the work that you do, not for the investment that you did to create the product. Sure, you can not get as rich from working on free software as you would from selling licenses. But on the other hand the risk of investing the money up-front before you sold a single license is also huge. \n\nThe whole point of free software, from a vendors perspective, is that it breaks the old cycle invest-money/sell-licenses. Instead you sell just the work that you do, on a per-hour basis. As the money that you need to invest (and thus the risk) is getting larger, free software becomes the only solution that is commercially viable to create projects in dimensions that you need for most mainstream products.\n\nAnd there is, of course, the buyers perspective. The buyer does not pay for licensing, only for the improvements that he ordered and for services around the product. Often (but not always) this is cheaper. The buyer does not depends on a single vendor who maintains a monopoly over the product. The buyer is able to adapt the product to his own needs. If you need a change in a commercial product *immediately* you are usually lost, unless you pay a obscene amount of money to the vendor. Free software does not have these risks, which is why it is more attractive, especially for the corporate buyer.\n"
    author: "AC"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "You have an imporant visions about the costs of a particular piece of software from buyer's point of view.\nBuyer definitely wins when everything is free, but there is no perspective then for this business. And buyers would understand that.\n\nWhat about the seller perspective? How about the QT model GPL tirany that forces you to may a lot of money or go GPL?\n\nAbout the acheivments you meantion, they are available with other licenses like BSD that does not require you to open the source. Also if you buy a commercial one you also have (even much better) from of reuse.\n\nSomething to not forget is that for quality maintenance and support of a particular piece of soft/lib there is a need of solid company and team behind it. GPL involves a defragmentation of the business leading to smaller groups or individuals which are not capable to maintain the quality."
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Well, if you listened to RMS, you sure know.\n\n1. Not all software is sold, most software is custom software.\n\nThis means, that for the largest part of software (like 80%) there is no market, but use. Companies pay for implementations and of course receive the source code and all rights all the time.\n\nThey save a lot building on GPL software. That's the society value of Free Software anyway.\n\nYou can also make a living that way, writing custom software. I do.\n\n2. Copyright is a right not a tyrany\n\nYou know, it's their code. Like your code is your code. How can you question their right to licence under whatever conditions they choose, when you want to use the same right to make money?\n\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "\n> You can also make a living that way, writing custom software. I do.\n\nIt's interesting how exactly do you live? (how you pay your rent, taxes, goods, stuff, hardware, software - you guess if you work full time for free you will die)\n\n> How can you question their right to licence under whatever conditions they\n> choose, when you want to use the same right to make money?\n\nI don't actually want to make any money from GPL soft because I beleive it's does not meet some quality criterias. What I envision as problem is that there is a global problem comming from GPL that destroys the software making as a enterprise and business, and turns it as a hobby. Not to mention that limited competition decreases user's opportunity for freedom of choice, and decreases the quality.\n\nYou need a lesson how does the free market economy works."
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "1. Do you understand the term \"custom software\"?\n\nIt means that I write specialized software that only one customer is possibly interested in. You never see this software, but it exists and it is the largest part of the jobs and of course software at all.\n\nThe customer pays for our ability to write software and we benefit from improved infrastructure.\n\n2. Quality and hobby\n\nYou may need to accept that the free market economy lets Free Software win. I am usually quite optimistic, it will.\n\nAs to the quality, funny how I find that Free Software will overall improve software quality. Our software largely benefits from using better infrastructure. We can provide better value to the customer at lower costs.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "OK, again, be more real, land to real world - answere your self the question how do you make your earnings?\n(what amount of the money you earn you earn them from the your gpl code and what you earn as hired for a commercial softwre??)\n"
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-05
    body: "I earn money, not with GPLed software, but with the system we build which is migrating to GPLed software. Linux, MySQL, GNU and Python to name the most important ones.\n\nOur licence is more free than GPL in the sense that there is no licence, but ownership.\n\nThe client lives well from using the software.\n\nI guess, you are a student worried that you won't find a job later on. And you seek to blame GPL software replacing commercial software for that. Maybe that's even true. But it's only a few jobs anyway. Think yourself, how many have work from e.g. MS Word (100 developers) and how many have (still) work from puting MS Office into custom applications by scripting it in Visual Basic.\n\nYou don't understand the market you are talking of. The commercial software is inferior infrastructure. It has high costs in both getting it and living with its marketing oriented constraints.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Don't feed the troll."
    author: "CE"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Running out of the reality will not make the world better"
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Beeing evil won't either do it.\n\nNobody forces you to use the GPL.\nIf you want to use code of somebody, he may tell how to use this code.\nWhy should there be a problem with that?"
    author: "CE"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "The seller's perspective is that free software will become, sooner or later, an economic neccessity. Why do you think did Apple take an LGPL'd HTML engine? Because it was not economically viable for them to create a HTML engine from the scratch. Software is slowly hitting a tipping point where the cost of producing proprietary software from the scratch does not justify the possible revenues. The GPL is a way out, because it allows you to build on existing software. \n\nAnd of course there is a perspective for business. Free software needs to be improved and enhanced, and someone needs to pay for it. It's unlikely that everything will be done by volunteers who are working for free. So eventually people will have to pay for the advancement of the software. Probably not as much as for license-based software, but also by the users. Either directly (because they want some extra feature) or indirectly (because they hire a company that lives from supporting a product, and needs this product to stay competitive).\n\nPractice showed that BSD does not work, because the people are not contributing back, at least not as much as with GPL. BSD has a short-time benefit for those who want to sell it as proprietary software, but the long-term disadvantage that because of this less code is contributed to the BSD-licensed part. This is the reason why FreeBSD&friends are slowly falling behind Linux. BSDs have a lot to catch up if they still want to run on the next generation of hardware...\n\nIf khtml would not be LGPLd, and thus all HTML engines would be under GPL, it's likely that Apple would have created a browser under GPL - that would be their only solution for loosing their dependecy on Microsoft's IE. But because there was a code base with less restrictions they just have to release the HTML renderer. If KHTML had been under BSD, I bet that they would not have release anything.\n"
    author: "AC"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Then the options are:\n1) GPL tirany (with duallicensing)\n2) no way to sell it (with GPL only - everyone can fork)\nBoth options are not acceptable"
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "With the GPL you don't sell software, you sell improvements.\n"
    author: "AC"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "Of course you can sell it.   I have bought lots of free software, most notably RedHat Linux.\n\nOf course, I'm not really buying the software, but the packaging and support.\n\nI am perfectly willing to buy more and more free software on that basis.\n\nAs to your assertions that you need a company to assure quality...  I find that the best software I use is free software.   Python, gcc, xemacs, the gnu toolchain, linux, KDE, etc."
    author: "TomL"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "This days there is a danger comming for the software making businesses. There are two growing monsters attacking from two opposite different fronts. Some are being defeated, assimilated or join the one empire, another join the other empire, die or decline.\n\nLet me explain:\nGo to tucows and you will find a real power of the free market, a lots of companies competing, and providing a big variety of apps. End user benetits because can choose, and moving power of what Adam Smith invented drives the natural diversity and evolution of this business.\nWhat is happening today and what are the two epmires.\n1) M$ Empire. With each next release we see abuse of its monopoly to adopt other products and destroy other businesses. As you guess there is IE defeated NS, there is MediaPlayer which fights winamp, MSNMessenger which fights ICQ, AIM. You don't need anymore apps like WinZip with XP, you don't need wingate since W2K, Java is now being attacked with .Net weapon, they expand on server market, they just buy some companies and applying their expansion. Step by step with each next release user declines one or another app, and slowly moving to full MS solution.\nMoreover as user gets more chained to the OS the prices are being risen.\n2) GPL Empire. As opposed to M$ with army of millions blind hackers around the world they build an ideal weapon for non-commercial environment. Some companies in order to survive decide to abuse some GPL software, or to release one. On other hand enthusiasts start small groups and establish variety of OSS projects ( http://sf.net ). What happens is that the big variety of companies making commercial apps dissapear - join GPL, die or decline because of their inability to sell. Market is shortened for them because out there are GPL apps entirely for free, their natural driving power that comes from the free market is lost.\n(guess what happens for example with industry like carmaking if for some reason someone starts giving cars for free?)\n\nLittle by little from two sides this industry is declining because of the two empires eating from both sides.\n\nWhat will happen if everything becames GPL? Yes the life will continue, just there will be no more big companies, but only a smaller service oriented groups and individuals. This in terms of economics is defragmenting and decline of that economy and not an improvement.\n\nAnother good example i can give about the effect of GPL is this:\n1) one guy works for 3d commecial softare making company and as a hobby in his free time he works on GPL wordprocessing app\n2) another guy friend of the first one works for a company that makes commercial wordprocessing app in his free time as a hobby makes a GPL 3d software\nPossible conlusion is that something bad happens and tomorrow both this guys lose their jobs because the GPL 3d soft eats the market share of the commercial one and the GPL wordprocessor wins the market of the commercial wordprocessor.\nThose guys then have no other option except to establish an opensource projects around their GPL apps and their own companies around them but will never be possible to make the same money as they did while working for a company.\nOf course this is very simple and artificially improved impossible case but in general right now commercial apps lose market share because of gpl alternatives.\n"
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: ">>What will happen if everything becames GPL? Yes the life will continue, just there will be no more big companies, but only a smaller service oriented groups and individuals. This in terms of economics is defragmenting and decline of that economy and not an improvement.<<\n\nThis is not true.\n1. Companies like IBM are not exactly small. All huge consulting companies can (and will) compete in this market, just like smaller companies...\n2. It will restore true competition. Today you have monopolies and oligopolies in most software categories. This is a somewhat natural condition, as in software it does not make sense to have more than 2-3 products per category. Markets around products, like books and training, usually can't support more than 2-3 products, and it becomes harder to hire knowledgable workers when the market is fragmented. This is why they consolidate and monopolies are built. With the effect that innovation stops and buyers pay too much for the product. Databases are probably the best example. \nFree software creates competition, because an unlimited number of companies compete for services for the same product. This is like markets were intended to be.\n \n>>Those guys then have no other option except to establish an opensource projects around their GPL apps and their own companies around them but will never be possible to make the same money as they did while working for a company.<<\n\nIf the people previously had a quasi-monopoly in their niche and made much more than what they invested for creating the software, yes. But then the buyers also paid too much and innovation usually stopped. The smartest strategy for a monopolist is to improve the product only so much that there is a sufficient reason for the customers to buy upgrades. The formular is that the extra-value for the customer must be at least as high as the upgrading costs. It does not pay off to do more, that would be more costs without any additional revenues. \nBut if you have competition, there's the potential that the extra-value is much higher than the actual costs. That's because the price is not bound to the value, but to the amount of work. \n"
    author: "AC"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "> 1. Companies like IBM are not exactly small. All huge consulting companies can (and will) compete in this market, just like smaller companies...\n\nThat's right, but not exactly. Companies like IBM, Sun, HP survive because they are also hardware vendors. Yes hardware vendors will contuinue to exist.\n\n> 2. It will restore true competition. Today you have monopolies and oligopolies in most software categories....\n\nYes, that's what I meant. Sercvices companies will be created a smaller ones, that serve manintenance, support and customization but the bigger solid companies that create and innovate cannot reach to this stage. May be only big ones that are also hardware vendors too will continue to centralise serious efforts for innovations.\n\n"
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: ">>Companies like IBM, Sun, HP survive because they are also hardware vendors. Yes hardware vendors will contuinue to exist.<<\n\nIBM is primarily a provider of services and consulting, not hardware. Just like Accenture, CSC and all the others... HP is also trying to get into the services business. The hardware margins are shrinking all the time, and commodity hardware like x86 is closing the gap to extremely expensive high-end stuff. \n\n\n\n \n"
    author: "AC"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "It's one of the biggest fears of people that big companies rule their life.\nSo everybody should be keen on using OSS."
    author: "CE"
  - subject: "Disillusioned"
    date: 2003-08-04
    body: "You don't really understand even what you are saying. And really there are not millions of hackers working on OSS. In fact it was less than 25,000 from the last statistics.\n\nWhat you constantly keep saying is that there is no way to make money from GPL software and this is not true. The software industry is turning into services and support and this is the best way to make money from it. You don't pay for the software, you pay for the solution, integrating that software into your business. I am not an expert on this, but if you study the industry and read a few books you will notice that the GPL is reviving the indsutry.\n\nIn addition it is impossible for everything to be GPL, there is a space for both commercial companies and GPLD ones. If everything was GPL it also means that the market has adapted and that the GPL is profitable.\n\nI also think your example is flawed. Companies don't lose market share over night. For example OO.org has existed for quite some time and MS's office is not goign away any time soon. The people working on GPL software are often even hired  to continue their work on the project or jsut hired because they have shown their skills.\n\nYou really don't understand where the costs in the software industry are and how it works. This isnt the place for misfounded trolls like you. Stupidity is like a virus so i will cease talking to you and hope you don't infect anyone."
    author: "Mike"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-05
    body: "As you are talking about economics, put it this way, GPL forces companies producing crapy closed source code or code taken from BSD'style licensed software tweaked (thay say \"enhanced\") to be incompatible with the original (read: kerberos, microsoft) to lower their prices or adapt to the free software's way. This because as you sayed there are good and free software replacement for the crappy closed source and expensive ones. Well, here you seem to think that this will destroy the softare houses witch enjoy profits from crappy software and treating users like apples or pears. So the IT industry will suffer lose in employment and profit following your reasoning. I think indeed that the money not spended from costumers in crappy, expensive and closed source software will return in the IT industry by other means, more manpower for data centers, more programs to coustomise for the costumer needs, at the point where the distinction from programmers and in house employed system administrators / integrators becomes less noticeable. And the inccentives to use the GPL become more and more attractive because of code reuse.\nAn analogy, your reasoning is like saying that researching other energetic resources than petroleum does harm becouse the poor petroleum companies will suffer loose and will lower the number of employes. Think about this.\n\nAs a side note, those poor software houses witch need the GPL'ed software for free but dont want to release the code, you think are acting right to the developers that maked this possible for free?"
    author: "fredi"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "I tried kylix. Nice product. I actually own a version of Delphi for windows.\n\nOne problem though. It doesn't fit with the rest of my system. I can't fix bugs in it or the base libraries. If I want to distribute something I write, I have to distribute libraries that the user probably has on their machine already, maybe even a better version.\n\nIt's not free or open. You are worried about how to make money from software. That is an important issue, I think people should be able to eat. My experience with proprietary software has been universally negative. Yes it works. But there always has been some frustration or limitation due to the vendor. I am not willing to pay for something that causes me frustration or limits me fundamentally. So, no sale. I'm not parting with my money under those conditions.\n\nRemember windows ME? It was the product of a non competitive market. It didn't matter what MS put out, there wasn't any other choice available. MS has competition now. They finally have a stable server. The proprietary software model naturally produces monopolies. It is a fundamentally unhealthy model. You should be thanking the free software world for providing a healthy balance that benefits everyone.\n\nAnd if you think that 80%+ margins are the only profitable model for software development, welcome to the real world.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "I agree that monolpoly is a problem and i am against it, but totally moving to GPL is not the right way too.\n\nYou have the perfect environment for innovation and development of this business only when there are multiple vendors that compete on the free market. Putting a software on the market that is entirely for no money is unfair cometition and destroys the market."
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "All that is happening is the commoditizing of software. All that will happen is MS won't be able to gather 6 billion in cash per quarter, just 4 billion. And what that means is W2000 web server is 300 bucks instead of 2000. This is good.\n\nThere is an economy building around free software that will grow it's own market. When MS and Netscape gave browsers away, they grew the internet market.\n\nIf there hadn't been linux available, the small ISP's that built the internet wouldn't have been able to afford Sun hardware and software. The industry grew as a result of Linux. The industry also contributed to Linux.\n\nThere will be a natural price / profit margin level that will be reached in a free competitive market.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-05
    body: "> If there hadn't been linux available, the small ISP's that built the internet \n> wouldn't have been able to afford Sun hardware and software. The industry grew as\n> a result of Linux. The industry also contributed to Linux.\n\nSmall ISP does not rely on UNIX immitation. They rely on strong UNIX power of the BSD and for Free. It's FreeBSD, and there is no GNU or GPL involved.\n\nAnother softwares that small ISPs rely are things like perl, php, apache which are not GPL at all."
    author: "A"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-05
    body: "No GNU involved? One has to wonder e.g. with what compiler the programs are compiled. ;-)"
    author: "Anonymous"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-04
    body: "It only destroys the market for software licenses, which become free. It does not destroy the markets for improving the software and offering services. Actually it frees these markets from an monopoly."
    author: "AC"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-05
    body: "\nDo you understand the waste it is to ask for money for goods that have no cost like e.g. KDE?\n\n1. The item is not put to use to increase productivity where it could have been.\n\n2. The item is reinvented over and over.\n\nAs a society you don't want it any different but that these 2 things don't occur. All obstacles to productivity have always been elimated. Why should it this time be different?\n\nAnd of course, society will prosper even more after it. And if you will, I as custom software writer can feel it already. In the past, we had only the infrastructure we could buy. That was not all what would have been useful. But now we can just plug in some Free Software and our product is better.\n\nOur client works better and that even saves lives. :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "One question..."
    date: 2003-08-04
    body: "Just curious, from which corner are you coming? You don't sound like you know much about KDE, FOSS, GPL etc. at all, which is a pity since you're posting on the wrong website."
    author: "Datschge"
  - subject: "Re: One question..."
    date: 2003-08-04
    body: "Good Question.\n\nIt's wrong assumption.\n\nI will just tell you this and you will know:\n\"Is UNIX ready for Desktop?\"\n(remember this old KDE slogan?)\n\nNow I can say UNIX is ready for desktop. It's acutally much better!\nCheck OSX from apple, build from FreeBSD. No GPL involved.\n\nKDE is still the same nice and getting improved every day. Last i have is 3.1 and I am using it since 1.1.2. Something that kde really needs to do for surviving is getting more commercial apps on it. For acheiving this it should get free from GPL (QT is the only limitation that makes it impossible to build commercial KDE apps).\n\nI am not one of this people, but I see that many tend to switch to GTK because there are no GPL limitations, KDE has GPL limitation and it's QT.\n\nKDE will get on top if companies start making commercial apps for it.\n\nCurrent market facts show that Macs are 4-5% of market share which makes them biggest UNIX desktop. Linux is around 1-2% where kde has around 60-70%. Kde has supperier technology compared to gnome, but gpl limitation stops the companies to build kde apps - else they have to pay a lot of money to QT.\n\n\nBtw as i read carefully, apple freed KHTML from GPL - remember their version is now QT independent, may be it's about a time that kde forces more competitive environment for with pushing it's own kit or QT to decrease the licensing costs.\n\n\nIf you don't beleive me that QT prices are too high just check what borland offers with their kylix."
    author: "A"
  - subject: "Re: One question..."
    date: 2003-08-04
    body: "Erm... I'm pretty sure that when Apple first started work with KHTML, KHTML was already independant from Qt."
    author: "Jilks"
  - subject: "Re: One question..."
    date: 2003-08-04
    body: "Wrong.\n"
    author: "SadEagle"
  - subject: "Re: One question..."
    date: 2003-08-04
    body: "> I am not one of this people, but I see that \n> many tend to switch to GTK because\n> there are no GPL limitations, KDE has GPL\n> limitation and it's QT.\n\nSo you like GTK because it is a good mix of GPL and LGPL? So its not about GPL tyranny then, its about libraries not being LGPL when they should be? Stop ranting about Linux and GPL then, calm down dude. Focus on the real issues. (BTW, if open software doesn't exist under a license people like, those people when often write their own software to get around the limitation)"
    author: "Russ Dill"
  - subject: "Re: One question..."
    date: 2003-08-04
    body: "Furthermore this isn't actually the right place to discuss this issue.\nHe should go to Slashdot.org."
    author: "CE"
  - subject: "Re: Buy yourself kylix"
    date: 2003-08-05
    body: ">  Quanta is GPL, it's free, and will not be sold, someone pays for development of quanta and it get's improved, one day it will became good app and a web designer will not consider buying some - this eats market share from commercial apps.\n\nI'm not at all honored to be name in this diatribe. BTW according to this I'm \"someone\". One day Quanta *will* become a good app? Any chance you will switch your creme rinse with gasoline and take up chain smoking? Or am I too late with that suggestion? Quanta Plus *IS* a very good app used by a lot of professional web developers. As far as eating market share from commercial apps... well if we do that then I think it's obvious that they should not have prduced such crappy product that a hand full of guys could have produced better software because a good web developer can easily pay for the software they need with a days work so price is not really an issue... Then again a lot of people not living in industrialized luxury can't and they don't need to either.\n\n> Quanta Gold is Commercial app, for building it thekompany uses commercial QT license. If the free quanta gets much improved, people will not buy quanta gold and the quanta gold project will die. Then eventually thekompany will not renew one QT license \n\nThey use Qt for a lot of other projects and will continue so that's not an issue. Also not an issue is the number of emails I get saying the GPL'd program is preferred. I've also seen reviews to this effect. In all fairness both programs have things the other doesn't but Quanta Plus has distinct advantages expressly because of the GPL and the code we can reuse. So my point here really is that you're using examples incorrectly which clearly demonstrate that you have no idea what you're talking about.\n\nNow on to another topic... business. How many times has your hame been in the incorporation papers or at the top of a DBA? How many times have you been the guy people come to asking for money where you make the decisions? Business is not a spectator sport. Owning one (ore several) gives you new perspective. Let's talk real property. How many times can you sell a piece of real estate? It can have esssentially one entity owning it at a time. Someone here said that 80% of all software is this type, however you seem focused on software as a product. It's not! It's a tool. Software as a product is the lottery. You can sell it again and again and again... there is no real production cost past the design stage. If you're coding for somebody doing shrink wrap software look elsewhere for work because there's plenty of it. If you're looking to hit the jackpot with the magic program then understand something... greed killed the industry. It's a walking corpse waiting to be buried. \n\nYou want to talk capitalism? Captialism is rendering a service or product for a reasonable price and expanding on that model... but intellectual property, patents run amok and shrink wrap lottery mentality have people in high tech running around looking for that \"magic combination\" where they have one idea which they apply for government protection of and sit back to enjoy the massive revenue as everyone who ever touches their idea is forced to pay and pay. This isn't captalisim! It's feudalism revisited where economic and information serfdoms are formed and the haves milk the essence of the have nots. Let's face it. You don't pull this deal off without backing so once you have one of these \"great ideas\" it takes a fat wallet to do it so you sell it to the predatory companies who no longer care about an equitable business model and only care about returns mandated by laws set forth when economics supplemented your farming and trading rather than ran mega corporations.\n\nToday small business people have no choice but to pursue an ethical model because people shun dishonesty, but some businesses have a choice because they can afford to mandate their products and services without option. Some would point to M$ but you could point to RIAA and MPAA members too as obvious participants in callusion and price fixing where there is no economic competition and the common man is having their money siphoned away for absurd profit margins.\n\nCommercial software is failing as a business model because it's long since proven it is not about meeting the needs of customers near so much as creating wealth in an inequitable fashion. Soon it will become clear to businesses that they can participate in a collaborative manner to produce all the software tools they could ever want way cheaper than they can buy them and it will be impossible to sell shrink wrap software (unless you've established a solid niche with a tool at a reasonable price like Trolltech has with Qt - but I guess you are upset that they can make money off of what you make money off of too - another irrational perspective because if you're making money you can affort the captial investment... that's capitalism.)\n\nBTW the GPL protects not only users by insuring the code will be there but as compared to BSD it protects me from you taking my program, modifying it and selling it without ever sending me a dime. You call that freedom. I call it a free lunch. I'm not buying yours. RMS may be quirky and out there some days, but at the core he's dead on. I can also sell web work while giving my tool away. It's free... though anyone is welcome to send me money in appreciation. ;-)"
    author: "Eric Laffoon"
  - subject: "LOL!"
    date: 2003-08-04
    body: "Is this a joke, it better be, nobody in the right mind would say such a load of shit.\n\nNot only is that the poorest english I've seen in a long while it is also ridiculously misinformed, he can't even remember the name of the company's name, calling Qt a company.\n\n'There is a belief that companies like QT that force GPL tirany can survive, I would say that this is not exactly true. Following a trends of growing share of GPL apps will lead to always free stuff and one will never sell an app and not buying a commercial licence.\"\n\n\nHe also does nto understand the way TT's dual licensing works. TT does not force you to make your software GPL as the misinformed A(sshole) suggests. All TT says is if you don't want to make GPL software and you want to sell it, than they should be rewarded for their work too. \n\nIn addition thee price is not so high for a company for what you get. TT is a startup and they have to charge thi price because they live off of innovation. The price was always the same, even before TT had a GPLd Qt. The GPL did not eat into tehir business anyway because programmers which write free software would not pay a company so much to do so and would instead use something else such as GTK+. The dual licensing as TT said has done nothing but help them, they now have better brand recognition and KDE to show just how good their product is. In addition, TT has increased their profits each year.\n\nAnother point from the A(sshole) is that you can't sell apps which are GPL. This is simply not true, the GPL does not say this and besides you do not need to choose the GPL as your license of choice. You only neeed to select it if you are already using GPLd code which automatically counters his other point that GPL soft is of poor quality. If it is of such poor quality why would a company use it and furthermore why would he see it as a threat?\n\nThe A(sshole) also says that people will fork Qt and therefore it will become of poor quality. Again another illogical point from his side. A fork is separate from the original Qt and so no matter how bad the fork was Qt would remain the same. IMO if Qt is forked it would not make it worse at all, it would mean that TT was not giving its user base what it wanted and so a fork was necessary.\n\nThe poor misinformed A(sshole) also jumps to MS's side and calls them \"just a market winner\". How naieve to believe that this si all ti is. Perhaps he has never read these two articles:\n\nhttp://www.lindows.com/lindows_michaelsminutes_archives.php?id=65\nhttp://microsuck.com/content/whatsbad.shtml\n\nAnother misguided example was the Quanta plus vs Quanta Gold arguement. The A(sshole) seems to think taht Quanta Plus came from Quanta Gold, while it is actually the other way around. TK hired people working on Quanta Plus. And than Eric Laffoon did not want the project to go to waste and hired Andras. Quanta Plus constantly presures Quanta Gold to be better, and so its a win for everyone. \n\nHe also says that Apple is successfully competing against MS by using a commercial license. HOW!? MS has a whopping 2.3% market share, you call that sucessfully competing!\n\nHe also seems to like the BSD license which I think is good for the short run but terrible for the long run. The GPL allows companies to use the code which many people might have worked on and donated to OSS and not give anything back to the project. This is good in the short run because many companies will use your code, but bad in the long run for the project. It only benefits companies.\n\nHe also says again that if something is GPL, it is always GPL or you have to pay someone a lot of money. But he doesen't realize that in the pure commercial licesnes it is always pay a lot of money and there is no other choice.\n\nCompanies that make money can also be companies based on the GPL and other companies will just need to compete.\n\n\"4) not Free (when you try to make soft) because it chains you with GPL and you are FORCED to GPL your code too\"\n\nIt is free if it is GPL and also it doe snot force you to GPL your program unless you decided to use GPL code. So fi you don't like the GPL don't use it. You can use GPL programs and not have to GPL your code btw. For example if I make a program in kdevelop it does not need to be GPL.\n\nH also calls the GPL a monopoly. THE GPL is a license and it can't be a monopoly unless it controls most of the market share which it does not by any measure.\n\nThis is just a anti GPL misinformed asshole who clearly has not read any books explaining the way OSS works or even the way software in general works.\n\nSorry about not quoting everything I am responding to and not fixing my spelling/structure errors. But, it simply is too much time wasted on one idiot. I only responde dto his 1st post, since he si pretty much repeating the same shit. "
    author: "Alex"
  - subject: "kwallet"
    date: 2003-08-02
    body: "A new little toy to play with :-)"
    author: "uga"
  - subject: "Re: kwallet"
    date: 2003-08-02
    body: "More than a toy, kwallet will finally let me manage passwords so I don't have to keep typing them into dialogues every time I revisit a web page in a new session. Finally!"
    author: "Tom"
  - subject: "Re: kwallet"
    date: 2003-08-02
    body: "Please note: kwallet is enabled for compilation, but not integrated into anything in KDE yet.  Integration will happen over the month of August.  Any wallets that are created at this point are subject to be summarily broken in half, or more, so don't use it for anything other than personal entertainment right now.\n\nThat being said, I would appreciate feedback on the direction the GUI is taking (kwalletmanager), and perhaps the client API (kwallet.h).\n\n"
    author: "George Staikos"
  - subject: "Re: kwallet"
    date: 2003-08-04
    body: "Is it just me or does kwalletmanager not have a visible system tray icon?  I'm running kwallet (3.2.0-0+cvs20030801+orth) on Debian Sid with XF86 4.3 and KDE CVS HEAD (orth's debs)."
    author: "anonymous"
  - subject: "Re: kwallet"
    date: 2003-08-04
    body: "The tray icon was not done yet.  I have a wonderfully ugly one in CVS right now, not installed yet.  I'll put a Makefile in tomorrow night.\n\nI really need someone with artistic talent to make ~6-7 icons for this, if anyone is interested."
    author: "George Staikos"
  - subject: "Re: kwallet"
    date: 2003-08-04
    body: "Stupid question: why does it need to have a tray icon? That just wastes screen space and sound like it's hard to use (who will be able to understand the relation of password in Konqueror and some symbol in the system tray?)."
    author: "Jeff Johnson"
  - subject: "Re: kwallet"
    date: 2003-08-04
    body: "It is used to notify the user that a wallet is unlocked and in use.  It's very important IMO.  If I get enough options together to justify a kcm, I'll make the tray icon optional."
    author: "George Staikos"
  - subject: "Re: kwallet"
    date: 2003-08-05
    body: "Unlocked? Does that mean that I need to enter a master password?\nWhat about people like me (and the majority of the population), who never use passwords? All my passwords in Mozilla are unprotected, I even have all my passwords list in a text file...\n\n"
    author: "Jeff Johnson"
  - subject: "Re: kwallet"
    date: 2003-08-06
    body: "Does that mean you run as root, auto login included etc.? You know everyone here will tell you that this all is discouraged due to security reasons? ;)"
    author: "Datschge"
  - subject: "Re: kwallet"
    date: 2003-08-06
    body: "No, I have one passwort for my user account (and a trivial-to-guess&type password for my root account). But I don't want more passwords. They do not make sense anyway, if somebody hacked my account he can easily install a key sniffer, or replace the application that reads them with a trojan horse, or... it's pseudo-security."
    author: "Jeff Johnson"
  - subject: "Re: kwallet"
    date: 2004-08-18
    body: "Jeff,\n\nDon't be a fool.  Passwords are not pseudo security.  It is not so easy to hack a well protected system, no matter how good a hacker you are.  Remember that the best hacker will not be on a personal system that is unimportant.  Only the mass of hacker wanna-be's would try to hack YOUR system, unless you work for our so-called President or someone else important.  Otherwise, your computer is not the target of some super hacker.  With that in mind, if you have your firewall up and configured properly, you are running with other security measures in place, and you use secure passwords (easiest with a password manager) then you will not be hacked successfully.\n\nAlso, remember that the passwords should be secure passwords.  This means you should NOT store them in a text file, but in an encrypted file.  You should NOT use words that can be found in the dictionary and passwords related to your personal information (i.e. social security number, birth date, name, place of birth, wife's name, wife's birth date, even your dog's name, etc...).  They should be a combination of upper and lower case letters as well as contain some none alpha-numeric symbols.  They should also be more than 8 letters in length, but some programs and websites limit passwords to 5-8 letters.  With these things as your guidelines you should be pretty much hacker proof on your passwords.  If you ignore these very commonly accepted practices after they have been explained to you then God help you.\n\nR.E. Craig"
    author: "R.E. Craig"
  - subject: "Re: kwallet"
    date: 2003-08-03
    body: "I called a toy, just because I'd like to \"play\" with it in my apps :-) Not because I consider it a toy. It's something everyone has been waiting for loooooong time "
    author: "uga"
  - subject: "Re: kwallet"
    date: 2003-08-04
    body: "Yea, I've been waiting on something like this for unix.  I really hope it'll include everything -- web passwords, ssh, gpg, remote filesystem passwords, etc.  That would rock.  Even better, if it syncs with gnu keyring on my palm :D"
    author: "Lee"
  - subject: "Re: kwallet"
    date: 2003-08-04
    body: "It will do web passwords, and all KIO related items.  I have no provisions in place for handling gpg, for instance.  It is possible to do this, but I'll leave that up to the kgpg developers to deal with.  KWallet is quite generic and really can do anything - it's effectively an encrypted binary data store with serialisation and a fancy GUI.  If you want to synchronize with devices, again, you will need to write the code to do it but it's really not hard.  I'll be fully documenting things approximately around the time of my talk in Nove Hrady."
    author: "George Staikos"
  - subject: "Re: kwallet"
    date: 2003-08-04
    body: "That sounds great.  It would be nice to hear that there'll be a guideline which makes apps USE it, but either way, I think they probably will.  KDE's usually pretty good with integration :)"
    author: "Lee"
  - subject: "Re: kwallet    forgoten password"
    date: 2004-03-01
    body: "Please, helpme.\nI forgot the kwallet password.\nIs the root able to change it? I still have the root password.\n\nI have been looking for one solution on the net.  Nothing found yet.\n\nWere can i ask about one solution?"
    author: "annonimous"
  - subject: "Re: kwallet    forgoten password"
    date: 2004-03-01
    body: "No. If root would be able to read them then it would be an design flaw."
    author: "Anonymous"
  - subject: "Re: kwallet"
    date: 2004-03-01
    body: "I don't want to read then, i want to delete then. \n\nI want to be at starting point.  Is it possible?"
    author: "annonimous"
  - subject: "Re: kwallet"
    date: 2005-08-25
    body: "Just create a new wallet from the wallet manager, and set it to use as default.  that is what I did, the dumbass that forgot the password as soon as I typed it in..."
    author: "Dale"
  - subject: "Nice Stats!"
    date: 2003-08-02
    body: "Great feature."
    author: "J"
  - subject: "Translations"
    date: 2003-08-02
    body: "What happened to the Swedish translation?  They have consistently maintained the most complete translation.  Now they aren't even on the list.\n\nhttp://i18n.kde.org/stats/gui/HEAD/index.php"
    author: "Burrhus"
  - subject: "Re: Translations"
    date: 2003-08-02
    body: "1. That link points to the HEAD translations, which one day will be KDE 3.2. Few translation teams have started working on HEAD.\n\n2. The statistics list seems to have been cut off right below 'Russian'. All teams behind Russian in the alphabet are left out, not just Swedish. (The i18n.kde.org server has been down for a few days. I guess it's not quite up to speed again yet.)\n\n3. The 3.1.x statistics page is complete, and looks really impressive for Swedish. I wouldn't worry yet ;-)\n\nhttp://i18n.kde.org/stats/gui/KDE_3_1_BRANCH/index.php"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Translations"
    date: 2003-08-04
    body: "It looks like that all i18n teams and statistics are now restored."
    author: "Anonymous"
  - subject: "Re: Translations"
    date: 2003-08-04
    body: "I thought everyone in Sweden spoke perfect English :) As do everyone in Denmark, Norway, Slovenia..."
    author: "bobbob"
  - subject: "Thank you for teh updates Derek!"
    date: 2003-08-02
    body: "Almost scared me, thought there wouldn't be any CVS Digest for Friday =( glad to see I was wrong! =)\n\nI like the new features included now and I'm glad to see some work on the UI though I wish tehre was more and I also like the speedups especially for PIM and Artsd. Great stuff =)"
    author: "Ale"
  - subject: "kget trouble on dial-up networks"
    date: 2003-08-03
    body: "I've got trouble with the kget of kdenetworks-3.1.3. If I make a \"save target as\" nothing happens. If I do it again I get the message \"The location is still being saved\" and kget pops up, with the download stopped. It starts if I click the \"play\" button. What's wrong?"
    author: "Ruediger Knoerig"
  - subject: "Wallet"
    date: 2003-08-04
    body: "Does that mean some malicous app can access all my FTP accounts because\nthe passwords are stored in the wallet? I hope not. \nIt would be really cool if it worked like this:\n\n1. You enter ftp://www.example.com in Konq\n2. Some dialogue opens which asks if you want to \"spend\" your wallet information\non that domain. This dialogue is protected in a way that makes it impossible\nto automatically circumvent it.\n\nThis would make it quite safe because I'd get suspicious if some dialogue requesting\naccess to ftp accounts I'm currently not working on popped up all of a sudden.\nOptionally you might enable that you always have to enter your root password\nto \"spend\" a password. At least I wouldn't have to remember all the various passwords\nat once.\n\n\n\n"
    author: "Jan"
  - subject: "Re: Wallet"
    date: 2003-08-04
    body: "If you have a malicous app on your computer, running with your account, it can get every piece of data on your computer. Period. \n\nEvery file. Every keystroke. The attacker can see everything on your screen. Everything.\n\nIf you would have to enter the root password, the only difference would be that the attacker would also learn your root password. But the value of root accounts on desktop machines is completely overrated, all important data is stored with the user's account. Root is only useful for an attacker if the machine is used by several people, or for using your computer for some distributed DOS attacks that require root permissions. "
    author: "AC"
  - subject: "More info about OBEX kio-slave?"
    date: 2003-08-04
    body: "Does anyone know where to find further info about the new OBEX kio-slave?"
    author: "Sami Ky\u00f6stil\u00e4"
  - subject: "Re: More info about OBEX kio-slave?"
    date: 2003-12-12
    body: "I tried it from CVS/kdenonbeta today.\n\nBut I have a SE T68i and had no success. Get and put with openobex ftp works on the command line.\n\nFor the KIO stuff I will try a little bit more this weekend and then I also test with a T610"
    author: "Thomas Vollmer"
---
In <a href="http://members.shaw.ca/dkite/aug12003.html">this week's CVS Digest:</a> QtRuby, <A href="http://www.ruby-lang.org/en/">Ruby</A> bindings for Qt are now in the kdebindings module.
<a href="http://www.kdevelop.org/">KDevelop</a> has a new class browser.
An <a href="http://www.ravioli.pasta.cs.uit.no/open-obex/">OBEX</a> kio-slave has been added. KWallet is enabled for compilation and testing. Plus KWin improvements, lots of work on <a href="http://pim.kde.org/components/kpilot.php">KPilot</a> conduits and many bugfixes.
<!--break-->
