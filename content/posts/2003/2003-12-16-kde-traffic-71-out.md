---
title: "KDE Traffic #71 is Out"
date:    2003-12-16
authors:
  - "hpinto"
slug:    kde-traffic-71-out
comments:
  - subject: "First post!"
    date: 2003-12-16
    body: "Weeee, first post!\n\nThank you, for another great report. "
    author: "Swoosh"
  - subject: "Thanks"
    date: 2003-12-16
    body: "Thanks again, I love reading your KDE Traffic!!"
    author: "nekkar"
  - subject: "GwenView/ShowImg fusion"
    date: 2003-12-16
    body: "  Since several days, there's a discussion about KDE advanced image viewers:\n''merging Gwenview and ShowImg'', and I do not understand what could be the\nadvantage in such an operation? To make a long story short, here are my\ncomments:\n\n1) Everybody says that the goal is to obtain a more complete image viewer\nby merging their both features. But looking ahead, these two softwares\nare not as complementary as said: they offer some identical things but\nnot presented in the same way. \n  \n2) The point of view of Gwenview is to be ``minimal'', I mean it uses\nexternal progs to achieve several features while ShowImg has them\nbuilt-in.\n \n  This dissension about the conception of the progs by itself leads to a question:\nwhich architecture would be used? Moreover, there's a drawback in using\nexternal progs: you have to install them, configure them, and know how to\nuse them. Lastly, which software should be used to start this joint development?\nGwenView, ShowImg, or from scratch? In this last case, when will\nthis new viewer be ready to use?\n\n3) Finally what features GwenView/ShowImg are missing to be complete? It\nwill certainly be easier to add these features than to start a third viewer.\n\n4) To my mind, it's a good thing that users can choose their own viewer:\neverybody has different needs, some prefer GwenView and other ShowImg according\nto their 'feeling'."
    author: "Guillaume Duhamel"
  - subject: "Re: GwenView/ShowImg fusion"
    date: 2003-12-16
    body: "It would be good to add image manipulation features. Another development direction could be a kind of flash alternative."
    author: "huh"
  - subject: "Re: GwenView/ShowImg fusion"
    date: 2003-12-16
    body: "I am still looking for an image viewer/manipulator that would allow me to:\n-rotate (multiple) images (in one go), and keep the EXIF info\n-crop images\n-adjust gamma\n\nOther than that, I don't need anything besides nice and fast viewing. The purpose of all this is of course to work with digital photo's. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: GwenView/ShowImg fusion"
    date: 2003-12-16
    body: "Gwenview does #1, although multiple images support is still rough right now. See this link for more info: http://bugs.kde.org/show_bug.cgi?id=65467."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: GwenView/ShowImg fusion"
    date: 2003-12-17
    body: "``-rotate (multiple) images (in one go), and keep the EXIF info''\n  ShowImg does! (tools-concert-rotate)\n\n``-crop images''\n  ShowImg doesn't...\n\n``-adjust gamma''\n  ShowImg doesn't do that exactly, but you can normalise/egalise the displayed image by using 'Display-Effect-Normalise/egalise'\n"
    author: "Richard Groult (ShowImg author)"
  - subject: "Re: GwenView/ShowImg fusion"
    date: 2003-12-16
    body: "Although not just an image viewer, the CVS version of digikam is awesome. Anyone wanting to easily manage a small or large number of digital pictures will be very happy with digikam when it is finally released. Check out the list of features.\n \nhttp://digikam.sourceforge.net/about.html\n\n"
    author: "Jarefri"
  - subject: "Re: GwenView/ShowImg fusion - what about imgSeek"
    date: 2003-12-18
    body: "I switched to linux slightly more than half a year ago and I have decided on KDE against gnome. Before that I was using ACDSee on Windows and naturally I was looking for something similar, and preferable a KDE application. I've tried the latest version of gthumb (a gnome image viewer), showimg(0.81), gwenview(1.0), imgSeek(0.82) and of course kuickshow and kview. \n \nI was sad to say that kuickshow was too primitive; showimg crush and freeze all the time (especially after deleting files from within showimg); therefore I have been using gthumb until gwenview-1.0pre4 where external tools are usable. \n \nI now use gwenview-1.0 mainly because it does not freeze up like showimg. I have krename configured as an external tools so that I can do series rename. And I use imgSeek as a standalone application for finding duplicate images as I cannot find a good way to integrate it with gwenview. \n \nBetween showimg and gwenview, I preper showimg for its better interface (double click on image to view it full screen, arrow keys to page through images etc) and  completeness in of feature - it have built in series renamer, and deplicate image finder. I personally do not care about image editiing feature for an image viewer as I can always edit images in dedicated editing software like gimp. However, lets not forget about imgSeek. In terms of image seeking and duplicate image finding capabilities imgSeek cannot be beaten. \n \nIf the image viewer applications were to be merge into one, I would choose the interface of showimg, the stability of gwenview and the duplicate image and image seeking features of imgSeek. I still like to use krename as an external tool for renaming images as it offers the most comprehensive renaming features. "
    author: "Polar Bear"
  - subject: "Re: GwenView/ShowImg fusion - what about imgSeek"
    date: 2003-12-18
    body: " Showimg is complete and efficient. External progs can be more efficient for some features but using them can slow down the operations. I use Showimg's features because it's fast and intuitive."
    author: "Benoist Gaston"
  - subject: "Re: GwenView/ShowImg fusion - what about imgSeek"
    date: 2003-12-18
    body: "Using external programs can slow down operations because you have to fork that is true. But on the other hand using external programs means less features in the main program, less code, so less memory used, and we all know that the speed of an application mainly depends on the free memory available.\nI think it's a good idea to use external programs so the code of your application is smaller and the program is easier to debug and has more chance to be more stable. Your also have the advantage of using all the new features of the external progs without having to recode all of them. I think all developpers should keep there applications \"simple and stupid\" (kiss)"
    author: "Nitra"
  - subject: "Re: GwenView/ShowImg fusion"
    date: 2003-12-18
    body: "Gwenview is too minimalistic. It lack many features that an image viewer should have. Namely,\n1.\tbatch rename\n2.\tbatch image type conversion\n3.\tduplication image finder\n\nUsing external tools may not be the ideal way. External tools may not integrate well with the image viewer. e.g. using krename with gwenview is not without problems. Try renaming \n\n\timage_a to image_b and later rename\n\timage_c to image_a\n\nyou will realise that gwenview reuse the original thumbnail for image_a. i.e. the thumbnails no long reflect the content of the underlining image. This problem can only be rectified by the lengthy process of regenerating all the thumbnails."
    author: "vav"
  - subject: "Re: GwenView/ShowImg fusion"
    date: 2003-12-29
    body: "Hi,\n\nI'm now using showimg about two years to manage pictures from my digital camera \n( currently more than 1500 and growing more and more). In the last month \nmultiple other picture viewers are enhanced (kview, pixie, kuickshow, gwenview, ...) \nand I had inspected them but I'm currently continue to use showimg. \nO.K. that's up to now.\n\nIn the past I'have found showimg are nearly complete program to display\nand manage my pictures. I.e. it is good to rename and rotate images. Of course, \nadding some external infos could be more simple. But these features are the\nmost important for me. The other programs mentioned above are still \nnot so comfortable for these jobs.\n\nWhat I don't need: extensive image manipulation routines. The new features \nare nice to see but it is not strictly required to extend them beeing general \npurpose image manipulation programs. \nStarting another program like gimp does this job very vell.\nSome improvements: removing red eyes would be nice \n(I don't know if this is possible).\nAnd creating an HTML image galery like konqueror would be also nice \n(e.g. starting the konqueror dialog).\n\nSupporting multiple stored view e.g. a full view to manage the images, \na minimal view (only movement buttons/ title / EXIF,text info ) for presentation,\na quick overview mode (like HTML mini icons) seems to be a good idea. \nIsn't this implemented by gewnview?\n\nComming back to merging: If the authors get ready to merge the programs \nthis would be nice - if the underlying concepts are compatible and the \nresult becomes a more complete thing. I think this \nis not the case when following a strictly minimal approach.\n"
    author: "Reiner Nix"
  - subject: "Is there an archive?"
    date: 2003-12-16
    body: "Hi,\n\nI pretty enjoy the KDE Traffic nevertheless I have missed the last one. Today I found out that the archive is not available. Does somebody know an archive?\n\n"
    author: "Anonnymos"
  - subject: "Re: Is there an archive?"
    date: 2003-12-16
    body: "http://kt.zork.net/kde/kde20031207_70.html\n\nI'll ask Zack what's wrong with the archive... Thanks for pointing this out."
    author: "Peter Rockai"
  - subject: "Thank you"
    date: 2003-12-16
    body: "And I like your idea, having interviews in KDE Traffic. That is a good move."
    author: "Alex"
  - subject: "Quanta and KIMageMapEditor"
    date: 2003-12-17
    body: "We're in the news again... I keep forgetting we're in KDE traffic now but our editors do a great job of sifting out the pearls.\n\nKImageMapEditor will be released with the next version of Quanta BE, date TBA. It also may be getting some fun new features. Stay tuned, and enjoy!"
    author: "Eric Laffoon"
  - subject: "I recommend using GQview :-)"
    date: 2003-12-18
    body: "I have used GQview since 1998, and I never find a true  counterpart instead of it.\n\nIt is simple, fast, understandable,  first introduced picture comparing (imho) etc.\n\nI must say that, KDE world could not present such a good picture viewer these day.....\n\nIt is funny for me: I use KDE desktop day-to-day, but I use Mozilla for browsing and email, mplayer for video, xmms  for audio, gqview and gimp for pictures, OpenOffice for documetum handling. It is so boring to fight each other, when the most used applications are not KDE based.\n\nSome notable exception:  Kghostview is better then Acrobat Reader's linux port, k3b is a beautiful CD/DVD writing application, audiocd://  camera://  (iosalves in general) and KDE printing system  are very well functional features. Kopete is very exciting and promising for me too.\n\nIs there a general Multimedia Framework in KDE?  As a system administrator -- who must deal with a lot of users --, I can say that, the most requested features are seamless movie/DVD viewing, listening music, editing home video and audio, playing with webcameras, send messages via microphone to friends, data exchange with mobile  phones etc.\n\nAnd I sense that, some basic configuration tool is missed by the average user (host name, network settings, basic firewall). There are very mature config tools (printing, font management, login options, keyboard, mouse) in KDE, but lack some basic tools (ie: basic\ntools for the average users). Typical question:  how can I add my linux machine to a Windows domain from KDE, and how can I map a windows share to my linux machine... KDE_SAMBA integration should reach a higher level IMHO. The real world is a mixed Linux/Windows environment, so it is important to ease the use of KDE in a Windows domain (the typical environment in hundreds of medium and large sized corporations).\n\nWhile KDE  can't work together Windows domains and Exchange servers (I mean: without a hitch), it has a much smaller chance to get used in the (mixed) corporate desktop. The real world is that, we can't stop using Windows servers from today to tomorrow, so IT managers and  decision makers will not adopt KDE, if it does not work with Windows (server) stuff.\n\nSo the debate about picture viewer A  B  or C is funny and marginal.  \n\nThe truth is simple: Concentrated effort is needed to conquer the desktop!\n\n"
    author: "N\u00c9METH Bal\u00e1zs"
---
Lots of news await you in the just-released KDE  Traffic. If you're interested in hearing about KDE performance, Quanta, KOffice, KDevelop, image viewers and more, <a href="http://kt.zork.net/kde/kde20031214_71.html">check it out</a>.



<!--break-->
