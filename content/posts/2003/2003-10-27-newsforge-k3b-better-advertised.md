---
title: "NewsForge: K3b: Better Than Advertised"
date:    2003-10-27
authors:
  - "Morty"
slug:    newsforge-k3b-better-advertised
comments:
  - subject: "Great article about great application"
    date: 2003-10-26
    body: "Agreed that k3b is the coolest CD/DVD burning software for the Linux, but why KDE doesn't try to get it integrated to the distribution (even kde-3.3 feature plan doesn't mention k3b) as that kind of software _should_ be part of any modern desktop environment...\n\nAnd as all desktops should atleast have an option for DTP, scribus fits the bill perfectly.\n\nOther funtionalities that IMHO should be part of all desktop environments are IP-based phone and radio softwares (like kphone and kderadiostation).\n\nIs KDE-people just waiting for someone to bring their software in via KDE CVS's kdeextragear->kdenonbeta->distro as the only way, and that KDE developers don't try to get critical pieces of functionality by approaching them directly?\n"
    author: "Nobody"
  - subject: "Re: Great article about great application"
    date: 2003-10-26
    body: "Don't expect the KDE 3.3/4 feature plan to be near completion, at the moment it only lists the stuff which didn't make it into KDE 3.2."
    author: "Anonymous"
  - subject: "Re: Great article about great application"
    date: 2003-10-26
    body: "Umm, I _guess_ (I'm not a developer) the problem is as follows:\nIf some app belongs to the KDE project is is required to\n1) Be a KDE app (not so obvious, see below)\n2) Be continually maintained within the CVS\n\nTo 1):\nScribus (as far as I know) is currently only a QT app and the\nquestion is whether the author is willing to invest the extra\nwork to port it to KDE. K3B is however a KDE app but even if \nit's based on KDE libs still there are requirements for true \nKDE-project apps. They all have to comply to certain standards \n(help, buttons etc). It would be quite difficult to do that\nagainst the will of the autor(s) because there is still 2):\nIf it is done against the will of the author (i.e. the project\nis forked) than there would have to be a maintainer how continually\nimproves the CVS-project in the future.\n\nAnyway, I agree with you that KDE-developers certainly should\ntry to _actively_ win the maintainers of those projects (K3B, Scribus)\nfor the KDE project (don't know if they already did) instead of just\nwaiting for them to knock on the door. This is the true strength of\nopen source in my opinion: Imagine that you get your favorite desktop\nwith professional CD burning and DTP for free. Now show me the DTP app\nwhich comes bundles with M$ ;-)\nOf course you can get them now for free, too, but the common perception\nwould be different if all is already contained. I really do hope\nthat K3B will finally be in the KDE main CVS and Scribus a part of KOffice\nbut its to the maintainers of these projects to decide.\n"
    author: "Mike"
  - subject: "Re: Great article about great application"
    date: 2003-10-26
    body: ">  I agree with you that KDE-developers certainly should try to _actively_ win the maintainers of those projects (K3B, Scribus) for the KDE project (don't know if they already did) instead of just waiting for them to knock on the door.\n\nK3b is hosted and developed in KDE CVS already, gets bugfixes from other KDE developers, translations from the KDE translation team, has a webspace (http://extragear.kde.org/apps/k3b.php, even if it's not its main) and so on."
    author: "Anonymous"
  - subject: "Re: Great article about great application"
    date: 2003-10-27
    body: "I'm sure that most distributions will create packages of the apps in kdeextragear. No need to be part of the main packages. \n\nIt even has advantages to be not part of the main packages, because otherwise you have to follow KDE's release plan. This can hinder the app developer.\n"
    author: "cloose"
  - subject: "Re: Great article about great application"
    date: 2003-10-27
    body: "There is afaik no reason why an application cannot be developed and released (in/from an own version branch) in the main CVS modules. You only have to adhere to the freezes of the HEAD and KDE_x_x_BRANCH branches."
    author: "Anonymous"
  - subject: "Re: Great article about great application"
    date: 2003-10-27
    body: "> You only have to adhere to the freezes of the HEAD and KDE_x_x_BRANCH branches. \n\nExactly! That's what I meant. The release schedule of KDE might not fit the schedule of the app developer.\n\nIn kde-extragear, he can make up his own release plan and still has most of the benefits like better translation support, help from other developers, higher visibility of the project, etc.."
    author: "cloose"
  - subject: "Re: Great article about great application"
    date: 2003-10-28
    body: "What is the point in adding every good application into the distribution?\nSince I switched to gentoo, I keep wondering why there is so much bloat in KDE itself. There are a lot of great programs in the distribution, but there is also such incredible redundancy and lots of programs I won't ever think about using - and still I have to compile them just to get one small application from that package.\n\nIt's the task of the OS Distribution (or maybe a Meta KDE Distro) to install all applications you want."
    author: "Justin Heesemann"
  - subject: "Re: Great article about great application"
    date: 2003-10-29
    body: "The point of KDE CVS is moving together all more or less important applications in one source tree so redundant code can be avoided more effectively by cooperatively working on shared libraries instead. The point of offering only few big packages is to allow package managers to quickly create complete up to date KDE packages without having to worry about potentially hundreds of packages with additional dependencies which are avoided with the current bigger packages split up into specific purposes. Furthermore (as a side product) big packages containing one popular app allows KDE to promote the other included apps. Splitting up the packages for the users to be able to choose from is not KDE's job, it's the task of distributions offering KDE packages. Complain to your distribution if they don't offer you that service.\n\nWithin the official KDE packages there's no redundancy, and Extragear's purpose is (among other purposes) offering redundant but possibly useful programs the advantage of being in KDE's CVS as well."
    author: "Datschge"
  - subject: "Re: Great article about great application"
    date: 2003-10-29
    body: "> Within the official KDE packages there's no redundancy\n\nThere is: Think of editors, image viewers, PDF viewer, ..."
    author: "Anonymous"
  - subject: "Re: Great article about great application"
    date: 2003-10-29
    body: "Technically they aren't."
    author: "Datschge"
  - subject: "Re: Great article about great application"
    date: 2003-11-04
    body: "Exactly. There is redundancy in say, Mandrake, but not in the KDE core distribution its self. On the other hand, Mandrake gives you a choice of all those apps and all the window managers. Maybe it should be the distro guys job to clean up."
    author: "Joe"
  - subject: "Re: Great article about great application"
    date: 2003-10-29
    body: "ever heard of DO_NOT_COMPILE flags? You need not compile things you don't need. "
    author: "fault"
  - subject: "Re: Great article about great application"
    date: 2003-10-29
    body: "I _think_ that k3b is not in official kde releases, because it haves a lot of linux especific code. Remember that KDE should work in linux, BSD, etc."
    author: "Alex Exojo"
---
In <a href="http://www.newsforge.com/article.pl?sid=03/10/20/1340254&mode=thread&tid=82">this article</a>, <a href="http://www.newsforge.com/">NewsForge</a>'s Joe Barr takes <a href="http://www.k3b.org/">CD/DVD creator K3b</a> for <a href="http://k3b.sourceforge.net/cgi-bin/index.pl/screenshots">a spin</a> and likes  it. <i>"K3b does more than burn audio, ISO, and data CDs. It can clone CDs and it handles DVDs, too. And it can make bootable multimedia CDs by using <a href="http://movix.sourceforge.net/">eMovix</a> as well. It's a really great application. I rank it up there with <a href="http://ahnews.music.salford.ac.uk/scribus/about.html">Scribus</a> in terms of being the one of the best (new to me) open source programs I've seen."</i> So be sure to check out K3b if you aren't satisfied with your current CD/DVD burner application.
<!--break-->
