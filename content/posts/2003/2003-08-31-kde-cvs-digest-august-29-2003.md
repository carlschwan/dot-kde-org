---
title: "KDE-CVS-Digest for August 29, 2003"
date:    2003-08-31
authors:
  - "dkite"
slug:    kde-cvs-digest-august-29-2003
comments:
  - subject: "The CVS digest n7y edition part 1..."
    date: 2003-08-30
    body: "Thanks. ;)"
    author: "Datschge"
  - subject: "Wee!"
    date: 2003-08-30
    body: "Thank you, almost scared me there, though there wouldn't be any this week :)"
    author: "Kitty"
  - subject: "Thanks Derek"
    date: 2003-08-31
    body: "Wow, IPv6 and SLP will be so cool."
    author: "Braden MacDonald"
  - subject: "I agree"
    date: 2003-08-31
    body: "I must say I agree with the editorial comments on neal's commit. \nAnd if everyone would start adding their local army forces to about boxes, there wouldn't be an end to it.. "
    author: "Pieter Bonne"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Yup, me too.\n\nOtherwise I'd like to add credits for my dog, pussy and guinny pig.\n"
    author: "wup"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "I hope he'll change his stance about this.. i'm actually quite surprised he apparently doesn't understand why it can't be done.. i value his contributions (and Hayes in particular) very much.."
    author: "Pieter Bonne"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "All my code will continue to be maintained, so if you like what I write, you're not losing anything.\n\nhttp://www.hakubi.us/kit/ (KDE 3 release already made and announced on http://freshmeat.net/)\n\nhttp://www.hakubi.us/hayes/ (KDE 3 releases made for some time already, and announcements to be made on freshmeat from now on)\n\nhttp://www.hakubi.us/megami/ (new release of the KDE 3 code coming soon, watch freshmeat)\n\nhttp://www.hakubi.us/kaboodle/ (status undetermined, if kdemultimedia maintains this anyway, I'll rename my fork)"
    author: "Neil Stevens"
  - subject: "Re: I agree"
    date: 2003-09-04
    body: "Neil,\n\n I actuall agree with you. It's your software, and if you want to credit the US military in it, that's your right. (And I agree w/ your sentiments about the US military too, and I'm not an american).\n\nCheers,\nSheldon."
    author: "Tormak"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Full ACK!\nNice to see that this project can't be taken over by some nutty freak.\nI immediately removed all his apps from my hard disk. Good riddance!\n\nAlternative slogans (don't worry -  I do not plan to commit them to CVS):\n(c) US Army - Turning dictatorships into civil wars world wide\nUSA (TM) - Feeding obese citizens by starving out the impoverished\n\nNeil is gone, others are waiting in the queue, unfortunately.\nNot that I'm especially fond of lots of things going on here in the EU,\nbut how the hell can one be so completely uncritical of:\nLarge proportions of the population imprisoned?\nThe world's highest per-capita energy consumption?\nDeath penalty for innocent black people?\nNo penalty whatsoever for racist police officers commiting crimes taped on video?\n\nThere has always been lots of critizism about the USA, but I rememeber only \n10 years ago, America was still considered the land of the free, the\nland of infinite opportunites, sth. desireable over here among large parts\nof the population. After the Iraq war the shine has worn off competely.\nNow they are just an ugly squid grabbing for more and more of the\nworld's resources for its obese population at the cost of people living elsewhere.\nI'm quite disappointed...\n"
    author: "Jan"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "This isn't about Neil's ideas. An anti-american comment was removed from CVS a while ago. For the very same reason, it isn't the place for it. Nor is this forum the place for your comments.Take your politics somewhere else. If you have some code to contribute, fine.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Of course it's about ideas.  Rather than keep KDE country-neutral and politics-free, KDE was made to take stands on pending EU laws.\n\nAnd don't talk to me about patents hurting KDE until KDE cuts off all support and contact with software patenters like IBM, Red Hat, and Apple."
    author: "Neil Stevens"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Do you really not see a difference here? I'm sure that if there was a patent protest targetting North America, the response would have been the same from KDE.\n\nWhat does it matter that this protest happened to be about Europe? Do you hate Europe or something?\n\nPatents affect KDE, so they would care about any large patent protest. Armed forces do not directly affect KDE."
    author: "AC"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "> Nice to see that this project can't be taken over by some nutty freak.\nI immediately removed all his apps from my hard disk. Good riddance!\n\nSorry, but this is not the appropriate place for blatantly anti-American (or any kind) of politics. Thanks. Okay. Bye."
    author: "User"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "You are the crossing the line here, with the above post."
    author: "Navindra Umanee"
  - subject: "Re: I agree"
    date: 2003-09-04
    body: "Dont veil your threats. If you plan to sanction people who post political views on a forum site (as opposed to KDE application credits), then you should atleast have the guts to state what the line is and what you will do if they cross it. This isnt the first time Ive seen you pass empty threats like theyre wind."
    author: "Ryan"
  - subject: "Re: I agree"
    date: 2003-09-04
    body: "Sure, email me if you want to be banned.  I'll be glad to take care of it, since I'm sure you can find elsewhere to play the idiot."
    author: "Navindra Umanee"
  - subject: "Re: I agree"
    date: 2003-09-11
    body: "<!--\n198.53.237.38 - - [11/Sep/2003:18:08:49 +0200] \"POST /1062275899/1062283645/1062285376/1062351676/1062693170/1062694830/1063296521 HTTP/1.1\" 200 7020 \"http://dot.kde.org/1062275899/1062283645/1062285376/1062351676/1062693170/1062694830/\" \"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.4) Gecko/20030701 Epiphany/0.8.2\"\n\nI think you missed the part where I criticize you for making empty threats like someone gives a rats ass WTF you think.\n-->"
    author: "Ryan  "
  - subject: "Re: I agree"
    date: 2003-09-04
    body: "I do not find Neil's comment to be political, if taken in the proper context.\n\nThis context it the winning of WWII and the defeat of the USSR (the government) in the Cold War.  Does anyone disagree with this?\n\nOn the other hand your comments *are* political.  And I note that I somewhat agree with some of them, but you go too far.\n\nOn the other hand, I do not think such comments are appropriate in an application.  But, OTOH, a patriotic wallpaper would be a good idea.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: I agree"
    date: 2003-09-08
    body: "Why don't you worms spend more time on reducing the memory footprint of kdeinit!"
    author: "Jojo"
  - subject: "Re: I agree"
    date: 2003-09-08
    body: "kdeinit's memory usage is around 86k.. it's just a wrapper"
    author: "anon"
  - subject: "Re: I agree"
    date: 2003-09-09
    body: "> This context it the winning of WWII and the defeat of the USSR (the government) in the Cold War. Does anyone disagree with this?\n\nI do! Because the USSR actually won WWII. The Russians would have won, with or without US support. The US on the other hand could not have done it without the USSR. So therefore we should credit Stalin! No? \n\n> On the other hand, I do not think such comments are appropriate in an application. But, OTOH, a patriotic wallpaper would be a good idea.\n\nNo, it wouldn't! KDE is an international project. Any given patriotic wallpaper will with certainty stir anger in another country. In some countries, most notably the KDE-centric Germany and Austria, the very concept of patriotism is despised. A couple of years ago, a german politician proclaimed: \"I'm proud to be german!\". He got tarred and feathered for that."
    author: "dth"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Well, the question is not about, that it is not allowed to add some entities as contributors list -- for example, if i wrote some application, I would gladly add my cat to contributors list for maintaing  my good mood :-) -- and i would be sure that it would not offend any1's feelings -- since my cat influences only my mood, by letting me to caress it -- i am not forcing every one else to do it or like it -- and my good mood is the key success factor in writing applications :-) -- so i just stated it source   \n\n\nIf the US army was the finansial sponsor of those projects -- that would matter. But the author of that comit stated, that the US army did \"Preserving the freedom that made this software possible\". That is an arguable political sentence, which can offend someone's feeling about US and their army. For example, I am quite sure, that US army preserved none of my freedoms, and that on the contrary, whole my life i tought that our (Russian) army is preserving my freedoms and rights against potential threats, including US as the grates possible one, all those numerous terrorists, and so on -- i was tought that if Soviet Union did not have nukes by the end of 40s, there would be no such country already by the 50s.\n\nI know that other people may have other oppinion -- there are people here from Poland, Hungary or Chech Reoublic, for whom the Soviet Army the symbol of communist's dictature in their countries, rather than the symbol of freedom from the Hitler's invasion (reminder: it was the Soviet army which liberated the eastern europe, while US&UK were liberating the western part) -- and IT IS their right to think so -- i have no right to force them thinking in another way!     \n\nOK. now imagine, what a big flame can start right now about each of statements i've made -- that is why things like 'thaks to US army for \"Preserving the freedom that made this software possible\"' should not be in credit lists of about boxes of internationally-distibutable software. \n"
    author: "SHiFT"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "From a certain standpoint, your cat and the US Army are identical in the credits.  They are both cited by the coder as secondary contributors.\n\nRealisically, the credit should have been removed and a strict \"primary contributors only\" policy stated, i.e., no spouses, pets, fathers, or other inspirational or supporting groups or individuals should be allowed.\n\nIf the US Army contributes code, artwork or translation, then they should be credited.  If not, then no.  If the German government does the same, they should be credited.  If not, then no.  If the Presbyterian church contributes code, the same applies.\n\nSlapping down one person's inspiration (Neal's US Army) and not another (your cat[1]) is not depoliticizing the KDE codebase - it is politicizing it.  One person might be inspired by God, another by RMS (some would claim 'what's the difference :P ).  Unless either has actually written the code, then they shouldn't get credit.  If they did, they SHOULD be able to, no matter how much somebody thinks it may be \"wrong\".  An equal playing field for contributors, and equal credit.\n\n[1] I get the idea your cat is a fictional example, but let me use it for this too :) "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "The thought about only adding direct/active contributors to a contributions list also crossed my mind :) \nI think it would solve this problem and maybe others to make it a policy indeed, and help keeping people objective. "
    author: "Pieter Bonne"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "I agree the statement should not be added for the reasons you are giving in your post.. i also think the bottomline is that (at least kde) shouldn't contain political texts, because \n1) politics have no place in this project and \n2) flamewars (about politics :p) have no place in this project.\n\nBut as I stated in the reply on my post, I'm also very concerned about why Neil made this statement and more importantly, why he almost begged for a discussion about this by moving his contributions from kde cvs..\n\nSo bottomline for me is, let's stop flaming, the relevant points have been made, and get back to work! (and hope (hopefully not in vain..) Neil changes his mind because of something we said here..)"
    author: "Pieter Bonne"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "> And if everyone would start adding their local army forces to about boxes, there wouldn't be an end to it..\n\nI think you're misunderstanding Neil's commit. He was simply protesting the political comment made on kde.org about patents. \n\nSee relevent links to Neil's blog ( http://the-amazing.us/simplyamerican/2172/ ) and also Rob Kaper's blog ( http://capzilla.net/blog/2171/ )\n\nIt's a shame this thing blew up so big. Perhaps a small link to the protest or a dot.kde.org editorial would have been good, but removing kde.org for a day was not. "
    author: "anon"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Um.  Reading http://the-amazing.us/simplyamerican/2172/ and http://the-amazing.us/simplyamerican/ only convinces me more that some people are nuts..."
    author: "ac"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Being for freedom and for softwate patents isn't really possible...\nIt seems they don't mind that."
    author: "CE"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "> but removing kde.org for a day was not.\n\nKDE was not alone in this; it was a community-wide effort. It lasted just one day, not exactly a horrific disruption of service.\n\nBut most importantly, software patents are probably the biggest threat to Free / Open Source software today. They happen to threaten KDE directly. We can stick our heads in the sand until it is too late, or we can say something and do something about it now in the hopes that the scenarios we all hope will never happen don't ever happen.\n\nThis wasn't about taking up some random political stance, but about ensuring KDE and other Free Software projects can survive. Most of the KDE developers happened to understand and agree with the action taken."
    author: "Aaron J. Seigo"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "I still find it very disturbing he removed all his contributions because they contained a political statement. He apparently wanted them to contain it very badly, which I don't understand. Firstly, it's obvious kde shouldn't contain political stuff because it's an international project. Secondly, the us army it hardly an active contributor (but there's no rule against that in principle) and thirdly I disagree with the statement itself. I doubt that what americans are doing right now will reduce terrorism."
    author: "Pieter Bonne"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "> I still find it very disturbing he removed all his contributions because they contained a political statement. He apparently wanted them to contain it very badly, which I don't understand.\n\nNo, he didn't remove his code because they wouldn't let him keep the ridiculous credit line in his about box.  He removed his code because of what he perceived as a double-standard in KDE commit policies, which he highlighted by putting a ridiculous credit line in his apps.  This is standard procedure for Neil, if you follow the lists.  \n\nTo wit: Someone does A.  Neil says, well if they can do A, then surely I can do ridiculous-and-obliquely-related thing B!  Everyone says: uh, no.  Neil says why NOT?!  What follows next is a lot of sound and fury, signifying nothing."
    author: "ac"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "So essentially it was a childish tantrum?"
    author: "not ac"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "I gather (without having been through all of this in detail) that the following occured:\n\nSomebody spoke for KDE saying that Software Patents were wrong, and that KDE opposed them (it may have even been said in the CVS tree... I'm not sure).\n\nNeil said \"That's political, and should not be in KDE\".\n\nThe consensus was \"No, we *all* believe it (even though Neil was objecting), so it's okay. Besides, who cares about politics\".\n\nWhereupon, Neil, who is, I believe, a US Army veteran, posted credit to the US Army for his code - an inspirational credit.\n\nSuddenly, it became a case of \"well, politics against software patents is Goodthink, and supporting an army is Badthink\".\n\nIMO, both should be either fully allowed (i.e., KDE is a group of varied contributors with personalities), or neither should be (i.e., KDE is a simply codebase and no more).\n\nIf anybody can correct me or fill in the details, please feel free.  I've read here and there and tried to keep an open mind to both sides."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Your summary seems accurate to me.\n\nOne correction:  I have never served in the Army."
    author: "Neil Stevens"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "> The consensus was \"No, we *all* believe it (even though Neil was objecting), \n> so it's okay. Besides, who cares about politics\".\n\nno, the consensus was that enough people agreed with the change to do so, and that the change was directly related to KDE's future existence. there were others besides Neil who disagreed with the change, and they made their voice heard. but KDE isn't a project that requires 100% agreement by everyone to do something, as that would make ANY decision impossible to reach (too many people in the project). so things are done that not everyone agrees with (sometimes that includes me, so i'm not being hypocritical or unsympathetic here).\n\nbut most who said they didn't agree made their case in a civil and socially acceptable manner, a path that Neil decided not to follow. and it wasn't the first time, either. this, as one one might imagine, causes problems as it unfortunately escalated the issue into the stratosphere.\n \n> Suddenly, it became a case of \"well, politics against software patents is Goodthink,\n> and supporting an army is Badthink\".\n\nit wasn't about \"Goodthink\" and all the 1984-Orwellien-esque implications the use of that phrase has. it was about using common sense and arriving at what was generally considered to be a majority consensus on a relevant topic.\n\nor do you think KDE is a conspiracy of fools?"
    author: "Aaron J. Seigo"
  - subject: "Re: I agree"
    date: 2003-09-01
    body: ":: no, the consensus was that enough people agreed with the change to do so, and that the change was directly related to KDE's future existence\n\nI did not read the change that applied to software patents; I do not know the circumstances of the agreement.  Perhaps my characterization of it as \"Goodthink vs. Badthink\" was connotative of 1984, but it was not intended to be.  It was (and is) intended to indicate the group 'feel' on the issue (ignoring vocal individuals in either direction).  \n\n:: or do you think KDE is a conspiracy of fools?\n\nI do not think KDE is a conspiracy, nor fools.  Nor do I think that this was handled poorly.  In fact, I went to great lengths to try an portray an evenhanded version of what I skimmed out of the lists.  Basically, my only point in the whole issue was that the best thing to do to move forward is to agree on a way of handling credits.  I then put forward the (common sense, likely already in the minds of most people) idea of 'primary contributors only'.\n\nIncidently, I don't think Neil was out of line either.  He was not in agreement with the majority, he spoke his mind, was overruled, and he walked away.  He still has a good set of code on his website, I get the feeling he will continue to update it as he sees fit, and life goes on.  Since it was specifically CVS commits that were the sticking point, it makes sense that he retract his code from CVS so he can take it in the direction he wishes.\n\nAt the end of the day, Hayes is still around, still maintained, and the author is happy.  It's a bit less centralized, but that's okay... things out of KDE CVS get the advantage of being able to be a bit more edgy (in features or goals... or simply credits, apparantly in this case).  KDE's still working fine, everything compiles, and the KDE core is free from any strings that might cause bad publicity or bad feelings on the part of some users or press (and I am making a common sense guess that that's a large portion of the reason people objected).  So, KDE's a bit less edgy, a bit more stable, and a bit more palatable to users all over the world.\n\nEverybody wins, at the slight cost of a bit more work downloading.  KDE went by the group dynamic, and Neil chose to stick to his individual choice, and went off on his own.  That's what volunteering is all about."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: I agree"
    date: 2003-09-01
    body: "Yes but if everybody starts acting like this then soon it will be a problem.\nThere is something that I'd really like to understand. \nDid Neil acted that way only because he felt this was mixing KDE with politics or did the subject itself (patents in europe) had anything to do with it?\n\nIf it is the 1st one, then what if a worldwide law, one that allowed companies not to respect the GPL, was going to be voted? Would he still think KDE shouldn't protest? Sorry, I might be wrong, but I find it really hard to believe that anyone in KDE thinks so. \n\nAnd if I'm right, then his problem is the subject itself. Patents in Europe.\nI could speculate about the whys but I prefer not to. What matters here is that if that is the problem, the subject, then he simply doesn't agree with some position assumed by the KDE team in this case (not really the behaviour in it's essence), so IMO it is childish to do all that fuss. Like it has been said before, you cannot expect everybody to agree 100% on everything in every matter in such a big groups.\n\nDnx"
    author: "Dnx"
  - subject: "Re: I agree"
    date: 2003-09-02
    body: "> you cannot expect everybody to agree 100% on everything in every matter in such a big groups.\n\nSounds like decision by democtratic voting."
    author: "Alex"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "Some days ago there has been a thread here on the dot about a chinese distribution including kde as its first-choice desktop...Of course, the question was raised what to do if there would be commits from a communist party(yes i know, it's _not_ communism over there in china). Different political views can easily break a community like ours (or others in the OpenSource universe). \n\nStill there are European and US companies acting and working together with staff from around the globe... Heck, how many US companies have dependencies in china, how many European companies?.... I doubt they all agree on their political views, still they manage to work together (in most cases very friendly)... In big companies acting worldwide there's always this rule: \n\nNo Politics.\n\nIf these people can work together despite their different political background OpenSource projects can do this too...."
    author: "Thomas"
  - subject: "Re: I agree"
    date: 2003-08-31
    body: "No politics, but KDE cannot ignore the threat to itself caused by patents on software."
    author: "CE"
  - subject: "For the love  of software!!"
    date: 2003-08-31
    body: "Don't fuck up the credits! \n\nOnly list people who contributed one fo these things: http://kde.org/support/\n\nMoral support, encouragement this REALLY shouldn't go here or we will have a very long list wth many people that have no relation to the project.\n\nAND POLITIICS DEFINITeLY SHOULDN'T BE HERE!"
    author: "Kitty"
  - subject: "Re: I agree"
    date: 2003-09-01
    body: "I've just uninstalled any past and present versions of his software. I will not be implicitly supporting the army of a FASCIST STATE that invades countries to *steal their oil* and murder their civilians. The USA and its redneck blind patriot supporters disgust me.\nIt's good software, and I'm said to see it go from my system, but people like RMS and Linux Torvalds have taught me that righteous principle can sometimes be more important than functionality.\n\nRT"
    author: "Rat Tank"
  - subject: "Re: I agree"
    date: 2003-09-01
    body: "Well, I think that this message sums it all up. Not only software patants can be a threat to KDE, also political views are a threat.\n\nPerhaps we have to let the OS advocates do their job of pushing europe and the world to the abolishment of software patents, and let the coders do their coding.\n\nI wonder if it's wise to include in *a* release of KDE in *every* about box a message against software patents ?  (this app may be abolished by ...)\n\n"
    author: "David L"
  - subject: "Re: I agree - Dump DARPA software!"
    date: 2003-09-03
    body: "> I've just uninstalled any past and present versions of his software.\n\nDon't forget Quanta Plus. I head that project and am a US Navy veteran so you have plenty of reason to hate me too.\n\n> I will not be implicitly supporting the army of a FASCIST STATE that invades countries to *steal their oil* and murder their civilians.\n\nInteresting. Did you support an intervention in the genocide in Bosnia or was that an invasion too? Did any state refusing to back the UN resolution have any multi billion dollar trade deals with Saddam at stake? Now that the Americans are going to have to spend over a hundred billion dollars rebuilding Iraq to steal a few billion dollars worth of oil we also know that they are stupid, right? I mean how much oil do they produce per year and how much can be diverted past any oversight so that they will not have to pay the going rate per barrel back to Iraq?\n\n> The USA and its redneck blind patriot supporters disgust me.\n\nOkay, what if they're NOT blind, grew their hair long to cover their red neck and are involved in open source projects? never mind. Your statement was about patriotic Americans without regard to whether that is inclusive of OSS developers.\n\n>  It's good software, and I'm said to see it go from my system, but people like RMS and Linux Torvalds have taught me that righteous principle can sometimes be more important than functionality.\n \nBut they live in America! Stallman even modelled a software declaration of independence after the US Declaration of Independence. Don't forget DARPA. DARPA is the Defense Advanced Research Agency responsible for the initial development of the internet and helping the W3C get started so we need to get you off the internet right away! Internet standards are managed by the W3C and I have provided the credit page below that is all the excuse you need.\nhttp://www.w3.org/Consortium/Prospectus/DARPA.html\n\nThere you have it. Neil Stevens credited the US Army and you removed his software. DARPA is part of the US military and is credited with both technology and standards as well as funding for the creation of the internet... and an ongoing involvement.  If you're not a hypocrite disconnect! If you're disconnecting now let me be the first to say.. Buh-bye!"
    author: "Eric Laffoon"
  - subject: "economic reasons"
    date: 2003-09-04
    body: "> I mean how much oil do they produce per year and how much can be diverted past any oversight so that they will not have to pay the going rate per barrel back to Iraq?\n\nPre 1990-production was slightly above 1 billion barrels per year. That's definitely less than the current costs of the allies, but not exactly peanuts either. Iraq has the second largest proven oil-resources in the world. Having at least some control over those resources is definitely interesting, esp. if you think Saudi-Arabia (largest oil-resources in the world) to be a potentially unreliable producer in the mid-term.\nNow, saying oil was the one and only reason the allies invaded Iraq is ridiculous. Yet, I do think, it's part of the difference between e.g. North Korea (who admit to be running a military nuclear program) and Iraq (who have been found to have (illegally) kept the option of one day re-instating their military nuclear program). Yet other reasons make the difference between e.g. Pakistan and Iraq. Few of those are perfectly moralic.\nThat said, whoever thinks, \"Old Europe\" was opposing the war for solely moral reasons must be blind. The division lines corresponded remarkably well to who could be expected to gain or lose economic influence in Iraq due to the war. Personally, I happen to think France and Germany chose the \"right\" position in opposing this war. But they definitely did so for the wrong reasons.\nPersonally, I do think, the US and GB administrations were acting imorally. Saying they're generally more imoral than any other ambitious power (such as Germany and France no doubt are) is stupid and biased, however.\nCondemming the US as a nation, therefore, makes no sense at all. But not only recent experience has shown that you should _always_ be suspicious of somebody who likes power enough to run for president in any country of the world. And in particular there's quite often reason to question the motivation for their politics. The true division lines are generally between economic interests, not between nations.\nNot like any of this was related to KDE...\n"
    author: "nobody"
  - subject: "Re: I agree - Dump DARPA software!"
    date: 2003-09-04
    body: "Eric,\n\nAs a proud american, I thank you for your service to our country, and I will continue to use quanta to grow my website business.\n\nAlso, I didn't hear any complaints when the US decided to spend some money and resources in Liberia to stop genocide.  And how convienent is it that so many trade deals existed with \"Saddam\" from our \"allies\"?  I wish the media would report about that a bit more.\n\nthanks.."
    author: "anonymous coward"
  - subject: "Re: I agree - Dump DARPA software!"
    date: 2003-09-04
    body: "Yes, the killed children in Iraq thank the US for their help.\n\nThere is a difference, the US don't need to lie for reasons in helping Liberians."
    author: "CE"
  - subject: "Re: I agree - Dump DARPA software!"
    date: 2003-09-04
    body: "Read that and understand it:\n\nhttp://dot.kde.org/1062275899/1062574189/1062697157/"
    author: "CE"
  - subject: "Re: I agree - Dump DARPA software!"
    date: 2003-09-04
    body: "I don't think that modern politicians are really interested in the spirit of US Declaration of Independence.\n\nPerhaps it would be better not to answer such comments.\n\nPS: Remember, several people said that the war against Iraq would be stupid.\nThe US made no friends with it.\nI'm very sure that there would have been better solutions.\nI also believe that the reasons weren't directly economical, but power and Christian fanaticism."
    author: "CE"
  - subject: "Re: I agree - Dump DARPA software!"
    date: 2003-09-05
    body: "Hmm. Eric, whilst I agree that the parent post you replied to was inflammatory, and frankly racist, I think you've gone a little over the top.\n\nMost people who object to the US's (& Britain's) invasion of Iraq do not object to the US in general (at least most people I speak to don't), so listing the good things the US has done in the past, and the good people who live there, is not strictly relevant. This is a new president, and a new government, and it's THEIR actions that a large segment of the world (including me by the way) objects to.\n\nAlso, I believe that most of the US companies involved in re-building have said that they won't make a profit, but will take enough money from oil to cover their losses. This is an extremely important point (if it's true - can't find a link right now). \n\nFinally, comparisons with Bosnia aren't fair. The invasion of Iraq was NOT primarily about saving the lives of the citizens of Iraq (it was part of the war on terror remember, and the - completely unsubstantiated - suggestion was that Iraq was a threat to the US)  and was not carried out with the backing of most of the world. Effectively the current US government has said \"we know best, and the laws we hold the rest of the world to don't apply to us* \" It's this attitude that is so upsetting, and frankly frightening.\n\nMy natural inclination is to back america ... I have american cousins, and many, many american friends. San Francisco is one of the great cities I've been to. I love the place ... Bearing all that in mind, it should worry americans that someone like me now considers the US the greatest current threat to world peace. Not through overt malice, but more a sort of collosal arrogance that only one point of view is valid, and by resorting to force far too easily.\n\nEd\n\n* I guess you'll object to this statement, but imagine that Russia invaded a neighbour to stop \"terrorism\" and did so without the backing of the security council. The US would surely object? Also, remember that there has been a big push towards an international court of war crimes, which has been totally derailed by the US's insistence that this is fine and dandy, as long as US troops are immune from presecution!"
    author: "Ed Moyse"
  - subject: "crystal icons"
    date: 2003-08-31
    body: "Ah, finally some REAL nice crystalized icons for those arrow thingies!\n\nThank you Everaldo, I wish there were more artists like you working on KDE and other Linux programs.\n\nYou rock!\n"
    author: "wup"
  - subject: "If you sue KDE, you GOTTA GET THIS!"
    date: 2003-09-02
    body: "http://www.kde-look.org/content/show.php?content=6240&vote=good&tan=77050039\n\nI think KDE should actually install these when it installs ebcause these are AWESOME!"
    author: "Alex"
  - subject: "Re: If you sue KDE, you GOTTA GET THIS!"
    date: 2003-09-03
    body: ">If you sue KDE\n\nYou listening, McBride??  ;)\n"
    author: "LMCBoy"
  - subject: "ROFL"
    date: 2003-09-04
    body: "meant to say use ;p"
    author: "Alex"
  - subject: "slides"
    date: 2003-08-31
    body: "Please provide slides in PDF or HTML (rather than kpr or kwd!).  :-)"
    author: "ac"
  - subject: "Kate goes KMDI?"
    date: 2003-08-31
    body: "I thought it was a KDE design rule that MDI wasn't used, and yet it seems Kate has \"gone KMDI\". Does anyone know exactly how this has been implemented? Personally I think using tabs is the best solution, most coherent with the rest of KDE and other editors. One think I like about KDE is its rejection of MDI."
    author: "Tom"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-08-31
    body: "Tabs are one kind of MDI.\n\nAnd yes, MDI is now allowed in KDE, though not in the sense many people know it. You won't see \"windows inside windows\" if you dislike it, tabs are the default MDI mode. Because of that, you'll barely note any difference, since lots of KDE apps are already \"MDI like\" (consider Konqueror, Kate, Konsole...)\n\nThere's now a library named KMDI in kdelibs to take care of all MDI stuff."
    author: "Henrique Pinto"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-08-31
    body: "MDI isn't as \"evil\" as it used to be.\nFortunately a lot more sanity has come to this issue.\nOne of the arguments against MDI was that you\ncannot spread your toolboxes and stuff over multiple desktops\nand freely arrange them. This is no longer true as you\ncan undock windows from a modern MDI app and spread them\nall over your desktops.\nTo sum it up you still can:\n1) Run multiple instances each with one document (SDI)\n2) Undock windows and spread them over your desktop\n3) Use tabs instead of windows\n\nAnd of course you can use it like \"evil\" Windows-style MDI ;-)\nBut you don't have to. So I think this shouldn't be such\na controversial issue anymore. Sorry to all those who have lost\ntheir relatives in this war against MDI. It's over, I'm afraid.\n\n\n\n"
    author: "Jan"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-08-31
    body: "The good thing about the KMDI classes are that they can be automagically switch from SDI (different kate windows), controlled MDI (e.g, tabs or kate's old interface), real MDI (uh, MS style MDI), and IDEAL-MODE (a combination of controlled MDI and real MDI :)))\n\nThe default is controlled MDI, but can be set by user."
    author: "anon"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-08-31
    body: "IDEAL mode is absolutely awesome! Its wonderful for programs like KDevelop. OSS doesn't innovate --- yeah right!"
    author: "Rayiner Hashem"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-08-31
    body: "Fully agreed. I can't wait for gideon to be released with KDE 3.2 too :)\n\n(also kate is now becoming more and more of a lite-IDE, which is nice too.. )"
    author: "anon"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-09-01
    body: "Actually I'm pretty sure that the IDEAL mode was copied from commercial software. It was a while ago that I actively read Kdevelop mailing lists, but I think it came from some java IDE or something? Anyway, I'm just nit-picking. ;-)"
    author: "Ed Moyse"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-09-02
    body: "Maybe, but I was more referring to bulding it into the central MDI system, thus making it available for all MDI apps."
    author: "Rayiner Hashem"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-09-02
    body: "It comes from IntellJ IDEA, afaik. "
    author: "anon"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-08-31
    body: "It seems that KDE is on the wrong path and if that wont stop it will go down the drain sometimes. Why add such bloat just because you can ?"
    author: "anon"
  - subject: "Re: Kate goes KMDI?"
    date: 2003-08-31
    body: "s/just because you can/just because large numbers of our users need and\\/or want it/\n\nthe KMDI stuff should actually reduce bloat since individual apps won't have to implement their own MDI, and it should help usability by providing (as) sane (as can possibly be) standard implementations..."
    author: "Aaron J. Seigo"
  - subject: "us army"
    date: 2003-08-31
    body: "George Staikos, thank you for standing up against this and deleting those comments!\n\nI find that comment very offensive (or, well, stupid) and it's obvious that they would have caused a LOT of discussion.\n\nPlease make sure that religion & politics stay out of KDE, it can't be any good."
    author: "me"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "Damn right.\n\nThat Neil guy is a nut.\n\nhttp://www.hakubi.us/ridge/\n\nWhat's at the above link has gotta be the saddest piece of software I've ever seen!"
    author: "foo"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "ROTFL! If it wasn't such a sad issue."
    author: "Jan"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "But isn't one of the virtues of an apolitical, tolerant, collaborative project like KDE that its contributors are NOT hounded based on their beliefs?  \n\nFor example, we all know ESR's attitude towards guns, yet this belief of his ought not to play a role in how we receive his ideas."
    author: "Dan"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "Well, yes.\nBut, if a developer adds \"Thanks to the us army ... liberty, justice,.. blah...\" to a KDE app, it's no longer apolitical.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "No one complained about Neil's (and Capzilla's) 'The Amazing US' blog, even if it probably does not represent the majority of KDE contributors. \n\nPeople complained when he stated in his opinion in his apps, and quite obviously did so only to annoy the people who disagreed with him.\n"
    author: "Tim Jansen"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "Actually, Christoph Cullmann whined to KDE e.V. about Rob Kaper's comments on his personal site."
    author: "Neil Stevens"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "But it seems that Rob was mistaken, while the action of somebody else was serious."
    author: "CE"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "and it was quickly retorted that, while the blog entry in question was highly offensive to many, it isn't the e.V.'s place to intervene in such matters. it was sorted out between the people involved to a quick and satisfactory (for them) end."
    author: "Aaron J. Seigo"
  - subject: "ESR"
    date: 2003-08-31
    body: "ESR has \"attitudes\" on several other issues beyond guns, which IMHO *should* play a role in how we receive his ideas. Have you ever read his blog ?\n\nTry this :\n\nhttp://armedndangerous.blogspot.com/2002_09_15_armedndangerous_archive.html#81815163\n\nfor a start."
    author: "Guillaume Laurent"
  - subject: "Re: ESR"
    date: 2003-08-31
    body: "That's very very sad.\nI wonder who are the barbarian, if i see such narrow-minded things."
    author: "CE"
  - subject: "Re: ESR"
    date: 2003-08-31
    body: "Well, ESR solution to everything involves a gun.\n\nPossible situation:\n\nHey, that nail caught my girlfriend's silk stockings?\n\nESR answer:\n\nHey, let me use my revolver's stock to make the nail's head even with the wooden surface.\n\nAnd just for kicks:\n\nSimpsons situation:\n\nHey, my basketball is on the roof!\n\nHere, Lisa, let me handle it! _BANG_\n"
    author: "Roberto Alsina"
  - subject: "Re: ESR"
    date: 2003-09-01
    body: "Read the second one. The guy is a complete wacko! As a Muslim, and Indian, and a Westerner* I'm offended on all three counts. But whatever. I love Open Source and respect his contribution to it. In the end, you have to make up your own mind, and realize that while some people will agree with you on some things, nobody will agree with you on everything. If you agree with something, don't stop supporting it just because its supporters have a different agenda on other issues. \n\n*> The weird background is a result of being Bangladeshi, living in the United States since age 5, and attending English schools since preschool."
    author: "Rayiner Hashem"
  - subject: "Re: ESR"
    date: 2003-09-01
    body: "I certainly won't stop supporting Open Source because of him, but I don't recognize him as a community leader."
    author: "Guillaume Laurent"
  - subject: "Re: ESR"
    date: 2003-09-01
    body: "Well, ESR's contribution to free software is fetchmail.\n\nAnd fetchmail sucks. A lot.\n\nUnless we count sophomoric essays as contributions, of course."
    author: "Roberto Alsina"
  - subject: "Re: ESR"
    date: 2003-09-02
    body: "he also wrote the kernel configuration system used in linux 1.2 to 2.4.. "
    author: "anon"
  - subject: "Re: ESR"
    date: 2003-09-02
    body: "No, he didn't.\n\nHere is ESR's entry in the kernel CREDITS file:\n\n\nN: Eric S. Raymond\nE: esr@thyrsus.com\nW: http://www.ccil.org/~esr/home.html\nD: ncurses library co-maintainer\nD: terminfo master file maintainer\nD: Distributions HOWTO editor\nD: Instigator, FHS standard\nD: Keeper of the Jargon File and curator of the Retrocomputing Museum\nD: Author, Emacs VC and GUD modes\nS: 22 South Warren Avenue\nS: Malvern, Pennsylvania 19355\nS: USA\n\nAs you can see, no mention of the kernel config system.\n\nNo idea how current this is, but I doubt it is pre-1.2 ;-)\nHad he done what you said he did, he would make sure it was mentioned everywhere. Of course, I may be wrong.\n\nHe did write a kernel config tool (CML2), which wasn't all that good:\n\nhttp://kt.zork.net/kernel-traffic/kt20030413_213.html#8\nhttp://kt.zork.net/kernel-traffic/kt20020520_167.html#3\n\nIt was never included in a production kernel, and eventually dropped:\n\nhttp://kt.zork.net/kernel-traffic/kt20020617_171.html#1\n\nHe did throw a tantrum about it, though.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: ESR"
    date: 2003-09-03
    body: "> Well, ESR's contribution to free software is fetchmail.\n \n> And fetchmail sucks. A lot.\n \n> Unless we count sophomoric essays as contributions, of course.\n\nYou might have a point, except ironically it's \"The Cathederal and the Bazaar\" that had one of the biggest impacts on free software of anything, unless you count the phrase \"open source\". If you read \"Under the Radar\" about Bob Young and the early days of Red Hat you see where he managed to have a positive impact on Intel and a number of other companies that made a big difference in the acceptance and growth of Linux in critical ways, as well as important corporate involvements. He cites \"The Cathederal and the Bazaar\" as the big factor that drove him forward. I've seen a number of project leaders make the same claim.\n\nSo while ESR may not have contributed substantial code, and you may not be all the impressed with his essays, at least one of them played a pivital role in shaping the thinking of a number of people who advanced OSS."
    author: "Eric Laffoon"
  - subject: "Re: ESR"
    date: 2003-09-03
    body: "The Cathedral and the Bazaar is his best essay, and I would grade it as adequate, if a bit pretentious (all that amateur socio-anthropo-whateverologist stuff).\n\nIt has, however, a few defects that don't let me be that generous:\n\na) It has been post-facto made into an essay about free software. It isn't. It contrasts gcc and (fetchmail|linux kernel). ESR is not the one who did this, of course. So he is not to blame. But it hurts the essay.\n\nb) It has aged really badly. Basically, it took the two examples the author saw, and ran with it, generalizing like crazy. Nowadays we can all see numerous (much more than at that early time) schemes for free software development CatB didn't even consider (KDE is one, Quanta is another ;-)\n\nConsider for example, a leaderless group like KDE. You won't find anything like it even imagined in the essay. My guess is that ESR's ego wouldn't let him imagine such a thing could exist.\n\nAgain, one can hardly blame him for not imagining what would happen. Futurology  will always be better done in the future. But it does mean the essay is antiquated.\n\nc) It's written in annoying egocentrical style. A quote:\n\n\"It's not a coincidence that I'm an energetic extrovert who enjoys working a crowd and has some of the delivery and instincts of a stand-up comic.\"\n\nd) Consequences are not a reflection of the quality of the work.\n\nColumbus embarked on his first trip because he had read a work that miscalculated the earth's size by 30% (Plotinus?). Had he read the (obviously better) number by Erathostenes, he wouldn't have done it, since a trip that long (think, Atlantic+Pacific+a little and not in the shortest path) was not practical at the time.\n\nDoes it mean that the bad measurement was good? No, it only means Columbus got lucky.\n\nIn this case, it's just that an almost-adequate essay impressed a young man to put his money in the wrong place (remember what business plans Red Hat had? ;-) and much later, by luck (IPO at the right time) and work and whatever, after changing direction A LOT, the company became semi-profitable (with a sunk investment of what? 50M?).\n\nDoes that mean CatB was any good as inspiration for corporate involvement? Hell no.\n"
    author: "Roberto Alsina"
  - subject: "I totally agree! THANKS STAIKOS!"
    date: 2003-08-31
    body: "I don't think hes a nut or anything, people have different opinions. However, Id on't think KDE should have any political or religious affilations lsited in it's software, it can offend many people.\n\nIn addition changelogs should list only DIRECT contributors to the project, otherwise it will confuse developers trying to get the hold of people working on the project and users.\n\nOnly people with these contributions should be listed \nhttp://kde.org/support\n\nEveryoen else does not belong in the application's change log. Just imagine if I tahnke dmy local supermarket for giving me acess to such great and cheap food, my friends for supporting me, starbuccks for helping me stay awake, air so I can breathe, my teachers who have taught me programming, my favorite authrs for inspiring me, my friend because he introduced me to Linux and KDE ETC. THEY HAVE NO PLACE HERE!\n\nI HOPE KDE DEVELOPERS WILL UNDERSTAND THIS AND CONTINUE THE TRADITION OF SENSIBLE CHNAGELOGS!!!"
    author: "Kitty"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "agreed.. I'm American and I haven't ever payed attention to the \"warning level\" (expect maybe..erm, the months after Sept11 after we bombed the snot out of the Taliban), and neither have most Americans. "
    author: "too lazy"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "Oh my, this guy is real nuts.  Read his blog:\nhttp://the-amazing.us/simplyamerican/"
    author: "em"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "Neil seems a little obsessed with \"anti-americanism\", while constantly branding non-americans as \"them\". Is Neil \"anti-everyone-else\"? Seems like it to me.\n\nI think America is a great country and I'm a Brit. However I don't approve of everything they do, but since when does anyone do everything right?\n\nPlease don't reply to this post, I don't want to start a thread."
    author: "MxCl"
  - subject: "Re: us army"
    date: 2003-08-31
    body: "Well, many of today's \"Anti-Americanists\" admire many aspects of the US.\nBut many other aspects are simply not acceptable, if you believe in freedom, democracy, civil rights ...\nThere are enough Americans who have that \"Anti-American\" attitude, even if you're not accepting Mr. Moore."
    author: "CE"
  - subject: "Re: us army"
    date: 2003-09-01
    body: "Neil seems a little obsessed with \"anti-americanism\", while constantly branding non-americans as \"them\". Is Neil \"anti-everyone-else\"? Seems like it to me.\n \nKind of. \nBut don't blame neil entirely. W. started this US vs. Them crap. Unfortunatly, the US is simply the hardline republican party. It has carried over to some people that is fanatism equal to, if not exceeding, OBL/Al Qaeida level and that far exceeds what you see in the Linux/anti-microsoft camp.\nI suspect that if our forefathers were alive today, many of them would be talking about, or even calling for, a revolution against this government. "
    author: "ac"
  - subject: "oops"
    date: 2003-08-31
    body: "Seems paranoid. A bit scary actually."
    author: "oops"
  - subject: "Re: oops"
    date: 2003-09-02
    body: "It's a commentary. Often it's \"read news\", \"post\" with few steps in between. SA should not be taken as a manual towards life.\n\nYes, there are many conservative, controversial, offensive and questionable posts on there. For example, I hate Neil's constant whining about the Dodgers and I bet he could do without my comments on the Giants himself. :-)\n"
    author: "Rob Kaper"
  - subject: "patents"
    date: 2003-08-31
    body: "I am ashamed that KDE.org didn't participate in protesting against software patents in Europe.\n\nWhy? Who could be more affected than you? What could be more detrimental to you?"
    author: "patents"
  - subject: "Re: patents"
    date: 2003-08-31
    body: "> I am ashamed that KDE.org didn't participate in protesting against software patents in Europe.\n\nIt sure did. "
    author: "anon"
  - subject: "Re: patents"
    date: 2003-08-31
    body: "It did, jackass.  Go be ashamed elsewhere."
    author: "ac"
  - subject: "Re: patents"
    date: 2003-08-31
    body: "like on slashdot for example.."
    author: "AC"
  - subject: "Re: patents"
    date: 2003-08-31
    body: "Some of us are disappointed that KDE *did* move away from a no-politics policy to participate in that European political protest."
    author: "Neil Stevens"
  - subject: "Re: patents"
    date: 2003-08-31
    body: "It was KDE's own interest to participate, since KDE and other OSS is facing problems because of software patents.\n\nThis is *no* move away from a no-politics policy."
    author: "CE"
  - subject: "Re: patents"
    date: 2003-09-01
    body: "kde is directly affected by _software_ patents regardless what country is going to introduce _software_ patents... These patents are _not_ about iraq, not about the Taliban nor about the u.s. army.... \n\nagain: about _software_ (that's what kde is all about)\n\nconclusion: kde is _not_ about iraq/the taliban/old europe/insert any appropriate term here to start endless political discussions. It's simply about software. Keep it that way..."
    author: "Thomas"
  - subject: "ignore neil"
    date: 2003-09-02
    body: "Ignore Neil, sadly he's blinded by right-wing politics."
    author: "Anonymous Coward"
  - subject: "Re: patents"
    date: 2003-09-01
    body: "Sorry, I can't remember any posting from you which made sense.\n\nAlex\n"
    author: "aleXXX"
  - subject: "NEIL, here is WHY I  DISAGREE"
    date: 2003-09-01
    body: "\nI understand what you're saying, but I think that is\nquite different. \n\nMore than 600 websites of which many include important\nOSS projects such as GNOME.ORG have participated in\nthis protest against patents in the EU.\n\nFurthermore, this patent law will significantly affect\nthe OSS world and therefore it is of great interest\nfor KDE, GNOME, GIMP.ORG, Autopackage.org etc. to see\nthis law get shot down.\n\nThe US army does not affect OS software in any major\nway and there is nothing the user can do anyway. KDE\ncalled for action to actually make a difference, if\nthey were to be passive  saying something like \"The\nnew European patent law sucks ass!\" and that was the\nentire message, not asking people to try and shoot\ndown the law than I'm sure there would have been a lot\nof negative reaction. Since saying this does not do\nanything about the problem.\n\nBut, the biggest difference is again, that your thanks\nto the army was actually in the credits of KDE\nsoftware while the US army didn't actually contribute\nanything to your projects. It\u0092s the same as if I listed\nmy programming teacher, my parents for giving me a home, \nthe local police for keeping me safe, the fire department for \nkeeping me safe also, GCC team for giving me a great \ncompiler for my software etc.  at the credits of my software,\nwhile he didn't directly have anything to do with it.\nOn the other hand this new law will impact OSS so it\nis of much interest to everyone. It would not offend\nKDE users or developers since if they are using KDE\nthey obviously don't want it to go away and this law\nwould hurt it.\n\nAlso, the protest only lasted a day (the website was\nalso still accessible through another address) and it\nwas a website not actual KDE software. \n\nAnd as many others have mentioned, it was KDE's own interest to participate in the protest, since KDE and other OSS projects are facing problems because of abusive software patents. KDE would have participated in a demonstration like this regardless of what country would have wanted to introduced this law.\n\nIt is just like businesses will support laws that will make their profits higher.\n\nThis is *no* move away from a no-politics policy.\n\nYour comment about the army was actually part of KDE\nsoftware and so if I installed the VS version of your\nsoftware I would have that comment for the entire time\nI had that software installed. \n\n"
    author: "Alex"
  - subject: "Re: patents"
    date: 2003-09-02
    body: "Neil: You are an idiot and your software sucks real bad. Take a look a kboodle. Did you know many of us have uninstalled that piece of shit? It only wastes space on people harddrives. WE DON'T NEED YOU, WE DON'T NEED US ARMY. WE DON'T NEED YOUR COUNTRY. "
    author: "Yo"
  - subject: "Re: patents"
    date: 2003-09-02
    body: "AND WE NEED NO PATENTS"
    author: "Mark Hannessen"
  - subject: "Re: patents"
    date: 2003-09-02
    body: "AND WE NEED NO FARKING FLAMEWARS! GROW UP!"
    author: "Mikkel Schubert"
  - subject: "Re: patents"
    date: 2003-09-06
    body: "No, but you would be far beinhind without the US, besides your trolling makes nos ense, the issue was never whether the US was needed or the US army.\n\nIt's just about KDE's policy on politics. It's obvious politiics should not be here, because there are so many immature trolls just waiting to speak outa nd many people may take offense."
    author: "Alex"
  - subject: "Re: patents"
    date: 2003-09-03
    body: "Neil, it's actual concessus that you're a moron ... I'm a right winger too in Canada but I think you confuse USA foreign policies with FREEDOM to do OUR job as SOFTWARE DEVELOPER. so try to learn the difference between those 2 please."
    author: "Mathieu Chouinard"
  - subject: "A huge week for KDE cvs..."
    date: 2003-08-31
    body: "With the removal of Neil's CVS privaleges, I hope that some of his ideas don't die, although I disagreed with 95% of what he said. He brought an unique perspective to KDE developers. He was perhaps the most ultra-conservative (well, I guess he was probably ultra-libraterian) USA-patrotic KDE developer, heh. \n\nOh well. bye neil :() I thank you for hayes. I don't think you for kinkatta/kaim never being in cvs compared to the bad client that's called kit. (good that kopete is replacing that piece of shit)\n\n"
    author: "anon"
  - subject: "Re: A huge week for KDE cvs..."
    date: 2003-08-31
    body: "Well JuK is in CVS now, so don't worry about Hayes.  I think Kaboodle is forked since it is actually needed."
    author: "ac"
  - subject: "Re: A huge week for KDE cvs..."
    date: 2003-08-31
    body: "I just hope neil finishes his \"Space Indenters\" game. Extremely elite. \n\n(See http://www.hakubi.us/spaceindenters/spaceindenters.png)"
    author: "anon"
  - subject: "Re: A huge week for KDE cvs..."
    date: 2003-08-31
    body: "i think so too.. some of his perspective had an ultimately good influence on the kde community at times.. too bad freekde.org had to stop.. I didn't even know until today he was so patriotic about the US!"
    author: "Pieter Bonne"
  - subject: "Re: A huge week for KDE cvs..."
    date: 2003-08-31
    body: "Yeap, me neither. And I really think it shouldn't matter.\nI talked to him many times in #kde-users and saw him help many people there, he seemed like a cool guy. Such a mess over something like this is really stupid, it's just not worth it. I hope things calm down soon..."
    author: "Sleepless"
  - subject: "Re: A huge week for KDE cvs..."
    date: 2003-08-31
    body: "I'm not quitting software development, or even KDE software development.  I quit cvs.kde.org development because of inconsistent policy enforcement.\n\nIf you like Hayes, keep using it same as ever: http://www.hakubi.us/hayes/"
    author: "Neil Stevens"
  - subject: "That's good to hear!"
    date: 2003-08-31
    body: "What do you mean inconsistency, ar eoyu saying thet KDE developers didn't remove European political affilations in KDE software? If so, than that's not fair at all, but just because some other politics sneaked their wayinto KD does not make adding mroe right IMO.\n\nJust like if one robber doesen't get caught stealing from the bank it doesen't make it right for anotehr robber to steal."
    author: "dan"
  - subject: "Re: That's good to hear!"
    date: 2003-08-31
    body: "not at all... there was a pro-Europe msg committed to CVS at one point in the past and it was met with the same response and was reverted."
    author: "Aaron J. Seigo"
  - subject: "Re: That's good to hear!"
    date: 2003-09-01
    body: "I think he means that if KDE opposes software-patents (something that actually has something to do with KDE!) it means that it's OK to put in propaganda-messages about the glorious US Army (the protectors of freedom, saviors of mankind!) in to KDE as well. I mean, US Army is just as relevant to KDE as software-patents are, right?\n\nIn case you can't tell, I'm being sarcastic."
    author: "Janne"
  - subject: "That's good to hear!"
    date: 2003-08-31
    body: "What do you mean inconsistency, ar eoyu saying thet KDE developers didn't remove European political affilations in KDE software? If so, than that's not fair at all, but just because some other politics sneaked their wayinto KD does not make adding mroe right IMO.\n\nJust like if one robber doesen't get caught stealing from the bank it doesen't make it right for another robber to steal. Also, why quit CVS, I'm sure this political mess wil soon e worked out completely."
    author: "dan"
  - subject: "Re: A huge week for KDE cvs..."
    date: 2003-09-01
    body: "Hello,\n\nright wing activist and Free Software, oh man....\n\nOf course they must get confused. They believe they can force others to accept credits for non-Freedom forces. \n\nThe US army has been prominent in keeping freedom from people. Since America is the land of DCMA, software patents, anybody can ruin/scare you through sueing, SCO, etc.... well, who would think it is Free?\n\nAnyway, Free Software is used in China as well. Free Software helps to get society more free. Maybe best where it's worst.\n\nAll my disrespect for instrumenting KDE to gain some cheap attention to your non-issue.\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Plastik"
    date: 2003-08-31
    body: "It's great that plastik was added to CVS.. was a variation of knifty added too? \n\nIf you haven't seen/tried plastik yet, goto http://www.kde-look.org/content/show.php?content=7559\n\nVery nice looking. hope they add thinkeramik and perhaps alloy too!\n\nPlastik is a very good candidate for being KDE 4.0's new default widget set. The current one isn't used by very many ppl (it' aqua-like, but it's 100x bulkier than aqua)"
    author: "User"
  - subject: "Re: Plastik"
    date: 2003-08-31
    body: "I can see some \"Keramik should not be the default theme\" posts coming. I hope not since this topic has been thrashed out all over the mailing lists. I don't get the fuss myself. Everyone I show KDE to says, \"Wow! That's a fab interface,\" (regarding Keramik). Look at me fueling the fire.\n\nPlastick's very nice though."
    author: "MxCl"
  - subject: "Re: Plastik"
    date: 2003-08-31
    body: "Everyone I've showed Keramik to loves it in first site, but if I install KDE on their computers, I often see them switching to some other style within a few weeks of being comfortable.\n\nBut I think keramik should still be default. It's alright if most users outgrow Keramik in a few weeks. "
    author: "anon"
  - subject: "Re: Plastik"
    date: 2003-08-31
    body: "Plastik isn't very good as a default theme. It doesn't scale very well with resolution, and is rather low-contrast (like Mandrake Galaxy). As a result, its almost unusable on my 1600x1200 15\" LCD. Similarly high res screens (1450x1050 is another common res for 14\" and 15\" screens) are actually rather common on laptops, and its important to take into account that laptops are outselling desktops these days. "
    author: "Rayiner Hashem"
  - subject: "Re: Plastik"
    date: 2003-08-31
    body: "Not only that, but the removal of the contrast slider in CVS, it hurts themes like plastik. "
    author: "anon"
  - subject: "WHAT THE FUCK!!!!!!"
    date: 2003-08-31
    body: "Why did they remove the contrast slider, taht makes no sense I loved and used that thing for msot of my themes! WHYYYY!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\nNOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO! MEAW MEAW MEA!!!!!!!!!NOOOOOOOOOOO!"
    author: "Kitty"
  - subject: "Re: WHAT THE FUCK!!!!!!"
    date: 2003-09-03
    body: "gets some milk for kitty.. calm down, its ok.\n\n"
    author: "joost"
  - subject: "But why?! PLEASE EXPLAIN"
    date: 2003-09-06
    body: "Why would they remove the contrast slider, that is a very very useful feature to me, I always set my contrast to the highest, it makes themes more usable and take sinto account people with disabilitiies which need very high or low contrasts."
    author: "Alex"
  - subject: "Re: Plastik"
    date: 2003-09-03
    body: "I love alloy, too.   It's very, very nice.   I use the commercial alloy l&f for my java projects\nand it's just gorgeous, so I was very happy to see an alloy theme for kde.   I'd love to see it\nas part of standard kde, if possible."
    author: "TomL"
  - subject: "Konqueror in windows in N7Y"
    date: 2003-08-31
    body: "**wet dreams** at this picture: http://www.blackie.dk/Images/kastle/digcam000769.jpg"
    author: "too lazy"
  - subject: "Re: Konqueror in windows in N7Y"
    date: 2003-08-31
    body: "It's Konqueror running on a remote PC using the (so much hated) X-Windows network transparency. The X-server namely displays an X in the systray.\n\nIt's not really that difficult, have tried it at home with the Cygwin X server - running Konqueror on the desktop and displaying it on the laptop with Windows."
    author: "Daan"
  - subject: "Re: Konqueror in windows in N7Y"
    date: 2003-09-01
    body: "You're wrong, no remote PC is involved. It's KDE running on Cygwin."
    author: "Anonymous"
  - subject: "That nut called NEIL..."
    date: 2003-08-31
    body: "<i>Please don't post messages with titles or content that say things like \"that nut called NEIL..\" here again.  They are not welcome.  I'm sure you can find better ways of expressing yourself.</i>\n<p>\n<a href=\"mailto:navindra@kde.org\">-N.</a>\n<br>------ ------ ------ ------- --------<Br>\npart of a post to core-devel of nut-neil:\n<p>\n> But it is.  You can't have free software without freedom at home, and<br>\n> I couldn't have written any of those apps without the US Army's<Br>\n> protection.<Br>\n<p>\nI read this 10 times and still can't believe one can tell such a shit.\n<p>\nMaybe that nut should try to get into the us army, instead of bothering\nkde people and users\n<p>\n\n\nand btw, kaboodle sucked big time, I'm glad it's gone\n"
    author: "ac"
  - subject: "Re: That nut called NEIL..."
    date: 2003-08-31
    body: "What's sad about this guy is that he really doesn't seem to understand that KDE is not the place for such controversial political statements."
    author: "poor neil"
  - subject: "Re: That nut called NEIL..."
    date: 2003-08-31
    body: "Yes he does, he understands that perfectly well.  He added the credit line as a direct reaction to the fact that the KDE.org site was taken down for the Software Patents protest, which he viewed as a similarly political act.\n\nWhat he doesn't understand are the differences between the webpage change and his About box changes, that make the former acceptable, and the latter ridiculous.\n"
    author: "ac"
  - subject: "Re: That nut called NEIL..."
    date: 2003-08-31
    body: "umh... maybe the difference is that the protest against patents (yes, it's about patents in Europe not in the US, not in another part of the world...) was a community-wide effort... Homepages like that of the gimp took part in this protest, cause all OpenSource projects may be somehow affected by a stupid legislative about software patents (plz note that it's about _software_ patents, nothing about iraq, the taliban or international terrorism...) regardless of a specific continent (even if it \"only\" affects Europe). \n\nI think that kde.org would would take part in a similar effort from let's say south american OpenSource contributors if they would face a similar threat (in terms of free software)...\n\nWhereas any OpenSource project has to avoid politics and social statements (... that's what politcal parties are for.... )\nProjects like kde are about developing better software (and that's what we can all agree on)"
    author: "Thomas"
  - subject: "Re: That nut called NEIL..."
    date: 2003-08-31
    body: "\nfor me neil corresponds to the typical american as some think of them in europe...\n\n- he certainly likes \"spray cheese\"\n- he thinks Michael Moore must be shut up and go to jail forever\n- he thinks GWB is great, honest, competent and elected\n- he had no strange feeling when he read \"united we stand, in god we trust\"\n- he is perfectly fine with the following sentence (about us people)\n  \"How can anyboy hate us? we are the greatest people in the world!\"\n- he knows that the us army, day by day fights against evil terrorists in iraq, as shown on cnn every day.\n\n\nmy advice: Neil, go GNOME"
    author: "ac"
  - subject: "Re: That nut called NEIL..."
    date: 2003-08-31
    body: "Please take your political comments elsewhere.\n\nThe issue isn't having political views. Everyone has them. But there are times and places for expressing them.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: That nut called NEIL..."
    date: 2003-08-31
    body: "It isn't only an political issue."
    author: "CE"
  - subject: "Patent protest very different"
    date: 2003-08-31
    body: "Well, abotu teh patent thing: http://news.com.com/2100-1012_3-5068007.html?tag=fd_top\n\nOver 700 websites participated in the protest and the outcome of this ruling directly affects FSS, the US army doesen't."
    author: "Kitty"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-01
    body: "first: Gnome has (like kde) a lot of contributors from all over the world (e.g. from spain and from france). Don't think the Gnome camp will welcome any obtrusions of political opinions...\nsecond: please discuss politics on a forum with a topic like \"foreign policies / diplomacy / affairs / whatever\""
    author: "Thomas"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "Read the-amazing.us.\nNeil is everything, except intelligent and sensible.\nThe good thing of that site is that it show the madness behind it.\n\nHe doesn't understand why the Europeans don't wanted to support an injust war.\nHe is a supporter of murder.\n\nI don't want to talk about politics here, but it is a good proof that he is very unfair to people with a different position."
    author: "CE"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-01
    body: "I'm confused, I didn't know Gnome was American & KDE was European. I was going to upgade to Mandrake 9.2 when it comes out & load KDE desktop only(I've been using Redhat9/Gnome), the reason I'm confused is I'm not from USA or Europe I'm from Australia is there a desktop for the South Pacific area with no political smell to it? By the way the reason I'm going back to KDE is the Kmail its the best Mail box there is. Cheers Jim"
    author: "Jim"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "> I'm confused, I didn't know Gnome was American & KDE was European\n\nyeah, interesting ... not even KDE or Gnome know it..."
    author: "Thomas"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-01
    body: "- he knows that the us army, day by day fights against evil terrorists in iraq, as shown on cnn every day.\n \n \nah no. Neil will not be watching cnn. Instead he will be watching \"Fair and Balanced\" Fox news ran by ex-reagan people."
    author: "ac"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "There is no big fair and balanced news channel in the US.\n\nWatch BBC!\n\nPS: This war wasn't a just war becuae the Iraq was evil.\nThere is still no proof of a connection to Al-Quaida and there won't be one."
    author: "CE"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "Hate to have a political discussion here on kde.org, but BBC as fair and balanced as Fox and CNN are. I get all of them, and all of them are biased in one way or the other."
    author: "anon"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: ">There is no big fair and balanced news channel in the US.\n\nIMHO, CNN is probably one of the more F&B of any major news station in the world, but I would tend to agree that BBC is probably better at when dealing with American issues.\n\n>Watch BBC!\n \nIf you are implying that BBC is \"fair and balanced\", then you are way off in left field. No station can truely be fair and balanced. There will be biases built in from the reporter to the editor to the owner. \nWhen I use to work as an EMT running ambulances in a small city (Ft. Collins, CO USA), I was shocked at what was reported in the news vs. what we would see. Several times, I saw accidents that were totally reported in wild fashions in the paper. Likewise, I saw a true gang hit that made 1 sentance in the paper even though it was huge news (the city was trying to deny that gangs were a problem for Ft. Collins).\n\nPersonally, I have watched BBC and do like that they tend to be more \"F&B\" when dealing with America. But, IMHO, the BBC tends to be more than slightly biased when dealing with Europe/UK or European/UK vs America. \n\n>There is still no proof of a connection to Al-Quaida and there won't be one.\n\nYou must have me confused with somebody else. But I find it funny that this is even an issue. [O|U]BL hated Sadaam more than he hated America. I laugh when I hear ppl even insinuate that relationship. The hatred there is very intense."
    author: "ac"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "I didn't state that BBC isn't biased. ;-)\n\n> You must have me confused with somebody else.\n\nIt wasn't answer to anything you have written, but I read the-amazing.us before."
    author: "CE"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-04
    body: ">>[O|U]BL hated Sadaam more than he hated America. I laugh when I hear ppl even insinuate that relationship. The hatred there is very intense.<<\n\nAre you completely off your rocker?  The planes on 9/11 must have flown into the oil fields in Iraq instead of WTC and Pentagon.  Reality check!!!"
    author: "frizzo"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-04
    body: "Why? It worked perfectly.\n\nWith the attack the end of Saddam's regime was caused."
    author: "CE"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-04
    body: "I thought Europeans were open-minded to other's viewpoints...at least that's what you've been telling conservatives for many years, right?\n\nI consider myself a typical american, so I'll respond to your points:\n\n- he certainly likes \"spray cheese\"\n\nno, I don't.\n\n- he thinks Michael Moore must be shut up and go to jail forever\n\nno, he has an absolute right to speak his mind and say whatever he wants.  However, he does not have a right to an audience and I have a right to not listen to him at all.\n\n- he thinks GWB is great, honest, competent and elected\n\nyeah, i do.  compared to clinton?  even more.\n\n- he had no strange feeling when he read \"united we stand, in god we trust\"\n\nI do a little bit, just because I wouldn't want muslim stuff pushed onto me.\n\n- he is perfectly fine with the following sentence (about us people)\n\"How can anyboy hate us? we are the greatest people in the world!\"\n\nI don't think we are the greatest people, but I think we are the greatest country.  I know why people hate us.  Most other countries are jealous of our country, because their socialist government couldn't outpace capitalism.\n\n- he knows that the us army, day by day fights against evil terrorists in iraq, as shown on cnn every day.\n\nI know that, but because I have a friend in Iraq, and I know they are not there just to steal the oil."
    author: "anonymous coward"
  - subject: "the USA way"
    date: 2003-09-09
    body: "Well, one could debate all the things you DID agree to.\n\nI dunno what 'spray cheese' means, so I'll refrain from giving a comment on that.\n\n1)Michel Moore: well, no1 would dispute that people have the right not to go see anyone or anything. However, if a person doesn't want to see something, they usually have a reason. In Michel Moore's case, it probably will have something to do with his sarcastic view on certain aspects of USA-life. Such a way of getting critique is often hard to swallow for those that are patriotic-shallow-minded. If anybody would make the same sort of criticism in my copuntry, I would welcome it. So, while it's the audiance right to go/listen or not, it's also a clear sign how close-minded they are; the level is equal to that of being able to accept, or at least doing the trouble of hearing what other people have to say.\n\nOn the other hand, you could just not like people with big noses or something, so it's for yourself to make out how valid my remark is.\n\n2)GWB: well, it's in the eye of the beholder, I guess. I would argue that he certainly doesn't seem honest and competent. I dunno about great; it depends on how you define it. If somebody is great by manipulating the people, for instance, that term might apply. But for 'honest': a mere history of seeing his 'oil'-dealings before he got president indicates that being honest is not his strong point. As for competent...well, he's competent enough to start a war on grounds that were flimsy and untrue at best... so maybe one could argue he is competent too, yes.\nIn any case, he was not elected (in a democratic way), if one accepts that a person which the majority of the people voted for, should become their elected representative. Of course, if you use the premise that it's not necessary to have the majority of the voters, then you could call it 'elected'. But in that view, it could be argued that Sadam got elected too.\n\n3)It's not a ne necessity to have the hand of God above your head, for not wanting to be shoved muslim-faith onto you. In fact, if fear for that is the main reason why you want to 'trust in god', then it's a sad thing. Also, I never understood the reasoning of the 'God with us'. If God  was truelly with the USA, why did he let the suicide bombers destroy the twin-towers? Couldn't an almighty god find another way of getting a message through? And what about the people in Iraq? I'm sure they praise and ask for protection of God too; does God chooses sides? In EVERY war BOTH parties always claimed god was on their side...well, you know what? If god would exist, he would get very tired of that. Sentences like 'God is with us' are arrogant and only a sign of self-rightious people who think that they are right, and the rest is wrong...or maybe they don't think (for themselves) at all.\n\n4)Heh. 'We are not the greatest people, but we live in the greatest country'... doesn't that seem like, a litle odd? What do you mean, then, by 'greatest country'?  That it's the biggest land-area, it has the highest mountains, the grass is always greener? I don't think so. Isn't it the people that make a country great? So, in effect, you are saying the people are the greatest, but indirectly. Yet you stated you aren't. It's a fine contradiction. And, once more it shows a distressing amount of arogance and little kids-fighting: 'You are saying that because you are jealous!' Yeah, right. I don't think any country in europe (and many others in the world) would want to be the USA, dude. What most USA-dudes seem to miss, is that we don't like the USA, not because of what it is, but because of what it does. It's not jealousy of the country, it's anger of the way you behave towards the rest of the world. But you guys never seem to realise that. It's like: 'but what is good for the USA, is good for the world'. You simply can not fathom that other people  have other views of how to deal with the world, and if it doesn't suit the USA, you are one of the 'bad' guys. The comparison between the roman empire and the usa is not farfetched: both were world powers, and both regarded the rest of the world as or babaric, or as vasal-states. And what Rome decided, was the only thing that mattered.\n\nIt seems that in 2000 years, people have changed little.\n\n5)Your last remark is the funniest: 'not just to steal oil'...not JUST to steal oil? So you concede stealing their oil was partly the reason, at least, then? ;-) Yeah, and the other reason was the weapons of mass-destruction: which are nowhere to be found. And the AL-quada connection; which even the CIA themselves say there is not a shred of evidence for that. O yeah, and Saddam was a brutal dictator...while true, if that would be a reason to invade a country, one should invade at least a dozen countries more in that regio alone. But, strangely enough, the USA seems to have no trouble in actively supporting brutal dictators, as long as it suits them. The moment they don't serve them anymore, however, then suddenly their former allies become vile dictators who must be removed from power. Right.\n\n\nWith so little self(country)-criticism, it's no wonder you don't like what Moore says."
    author: "ReBornToDieTwice"
  - subject: "Re: the USA way"
    date: 2003-09-09
    body: ">>It's not jealousy of the country, it's anger of the way you behave towards the rest of the world.<<\n\nThe jealousy is also limited, for instance, by the economic conditions in the USA. It's hard to believe for europeans that there are western countries where people need to work two fulltime (minimum-wage) jobs just to support their kids, and don't even have a health insurance.. it's part of the concept that some people suffer and some people are extremely rich, but in the eyes of many the US are too extreme, and not only in this aspect.\nThere is, of course, jealousy for specific things, like the US military. But in general I just feel sorry for the people.\n"
    author: "Jeff Johnson"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "\"I couldn't have written any of those apps without the US Army's protection.\" I was wondering, when you chaps/ladies write apps for KDE in Europe does the European army protect you? I didn't know it was that dangerous writing software. I know this is the wrong place to say it buy my 2cents worth USA/Bush got into the war to settle an old score or for oil or any other wrong reason, but at the end of the day the people will be better off with out a dictator. So I hope for the people of Iraq they have a free and safe future. I personally would be interested in any one from Iraq informing me if they are happy or do feel better off. I've never been to USA, lets face it Captilistic countries offer Dugs, greed etc, but I would have all of that because it does offer a special thing call freedom & I do think that is worth fighting for. Regards Jim"
    author: "Jim"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "> Capitalistic countries ... offer a special thing call freedom\n\nNot so fast pal. You might need to rethink a little bit.\nCapitalism does not imply freedom, if it means that\nyou can pay for restricting the freedom of others,\nthat is, buy others freedom.\n\nExample:\n\nIn some countries, everyone is free to walk through forests,\ndo camping, enjoy the land, feel free.\nIn other countries. The owner of that land might put a fence\naround the whole area, or kill you for trespassing.\n\nWhich one is more capitalistic? Which one is more free?"
    author: "Olof"
  - subject: "Re: That nut called NEIL..."
    date: 2003-09-02
    body: "You're right I choose my words wrong, I should have said, with all the wrong things about democratic countries they do have one good thing & that is freedom of speech as a posed to what Iran had, they had a dictator. And please don't get me wrong I'm not a blind idiot to thinks Bush went in to Iraq to save the people from a dictator, but I am glad to see that they will have freedom of speech and a way of life at last & that's got to be better than what they had. My views on software is, file formats should be open source, companies should sell there products due to quality not because they have some closed format, I know people like to rave about big bad Microsoft, but the big cost to companies is all the other companies selling closed software, (what is the saying binary= golden hadcuffs). I'm also for distributions making profits on suppling a good service. Regards Jim"
    author: "Jim"
  - subject: "mosfet"
    date: 2003-08-31
    body: "Where did he go, I can't seeem to be able to aces shis website for a couple of weeks and I don't see him anywhere, anyone have any information about him?\n\nreward $100 USD\nALIVE"
    author: "Alex"
  - subject: "Re: mosfet"
    date: 2003-08-31
    body: "He announced that he would moving his website to an other server.\n\nYou can access http://www.mosfet.org/liquid.html, but not index.html."
    author: "CE"
  - subject: "Re: mosfet"
    date: 2003-09-01
    body: "Yeah, but his mosfet.arklinux.org is down too, what happened? Anyone, got his e-mail so I can ask him?"
    author: "Alex"
  - subject: "When protesting against patents..."
    date: 2003-09-01
    body: "...we should have also linked to http://www.hakubi.us/amazon/ as a good absurd example how software patents can affect KDE. Just so you know, that patent is not only valid in the US but has already been granted in the EU as well even though their was no justification for it law wise, the possible upcoming EU patent law would give just that. That's why KDE among many other software projects (which often enough did so supporting their European contributors even if they were a minority) needed to protest against it.\n\nI respect Neil for his ability to give a different point of view when people expect it the least, I just wish he'd not be that shortsighted and pigheaded in choosing what, when and how to do so."
    author: "Datschge"
  - subject: "Software patents"
    date: 2003-09-01
    body: "For the KDE project, protesting software patents are entirely consistent with remaining apolitical. For the KDE project, software patents aren't a political matter, but a matter of self interest. Thus, its no different from commercial companies lobbying governments when particular laws could have a big impact on them."
    author: "Rayiner Hashem"
  - subject: "I agree"
    date: 2003-09-02
    body: "To NEIL:\n\nI understand what you're saying, but I think that is\nquite different.\n\nMore than 600 websites of which many include important\nOSS projects such as GNOME.ORG have participated in\nthis protest against patents in the EU.\n\nFurthermore, this patent law will significantly affect\nthe OSS world and therefore it is of great interest\nfor KDE, GNOME, GIMP.ORG, Autopackage.org etc. to see\nthis law get shot down.\n\nThe US army does not affect OS software in any major\nway and there is nothing the user can do anyway. KDE\ncalled for action to actually make a difference, if\nthey were to be passive saying something like \"The\nnew European patent law sucks ass!\" and that was the\nentire message, not asking people to try and shoot\ndown the law than I'm sure there would have been a lot\nof negative reaction. Since saying this does not do\nanything about the problem.\n\nBut, the biggest difference is again, that your thanks\nto the army was actually in the credits of KDE\nsoftware while the US army didn't actually contribute\nanything to your projects. It\u0092s the same as if I listed\nmy programming teacher, my parents for giving me a home,\nthe local police for keeping me safe, the fire department for\nkeeping me safe also, GCC team for giving me a great\ncompiler for my software etc. at the credits of my software,\nwhile he didn't directly have anything to do with it.\nOn the other hand this new law will impact OSS so it\nis of much interest to everyone. It would not offend\nKDE users or developers since if they are using KDE\nthey obviously don't want it to go away and this law\nwould hurt it.\n\nAlso, the protest only lasted a day (the website was\nalso still accessible through another address) and it\nwas a website not actual KDE software.\n\nAnd as many others have mentioned, it was KDE's own \ninterest to participate in the protest, since KDE and \nother OSS projects are facing problems because of abusive \nsoftware patents. KDE would have participated in a \ndemonstration like this regardless of what country would \nhave wanted to introduced this law.\n\nIt is just like businesses will support laws that will\nmake their profits higher.\n\nThis is *no* move away from a no-politics policy.\n\nYour comment about the army was actually part of KDE\nsoftware and so if I installed the VS version of your\nsoftware I would have that comment for the entire time\nI had that software installed. "
    author: "Alex"
  - subject: "ok, maybe it's just me..."
    date: 2003-09-01
    body: "ok, maybe it's just that i am thik-headed or something, but i can't seem to figure out what's wrong with software patents... someone please enlighten me as I am really confused on this issue and why it's such a big deal... i mean, if some wants to protect what they invented, what's wrong with it?\n\neh, maybe it's newbism..."
    author: ":-)"
  - subject: "Re: ok, maybe it's just me..."
    date: 2003-09-01
    body: "afaik it's not in general against patents... (I still think that we should rethink that \"intellectual property\" stuff, esp. in fields like the human dna)\n\no\n_software_ patents tend to be very \"cloudy\" and have numerous side-effects on other not directly related parts of the IT. I think the reason may be the character of a software patent as it's more an attempt to protect a general \"idea\" than an actual solution to a specific problem (at least that's the case for a lot of already existing software patents) \n-> this is different from e.g. engineering patents where it's almost always possible to come up with a different solution to a problem without violating existing patents.\n\no\nthe evolution of software used to be faster than e.g. in mechanical engineering or other creative classes.... the actual proposal for a new legislative (in Europe) does not take this into account. The life-span of a software patent seems to be too long (this would hinder future developments in this field or at least introduce an inaccaptable delay)\n\nnote: this is _my_ point of view. Try google for (unbiased) information on this topic...\n"
    author: "Thomas"
  - subject: "Re: ok, maybe it's just me..."
    date: 2003-09-01
    body: "The problem is not the patent in it self, but the patent bureau.\n\nTo get a patent, your new invention must reach a certain level\nof \"new-value\". That is, it must be something not too small\ninventionwise.\n\nSo when a patent bureau gives Amazon.com a patent for\n1-click-shopping they argue that this \"invention\" has a sufficient\nlevel of \"new-value\" which it, of course, does not.\nBut how could the bureau have known? They were probably\nnot software engineers.\n\nNow, deciding that \"pure computer programs\" can not be patented\nclearly reduces the risk of this mistake by bureaus.\nHowever, linking the computer program to something else,\nsome machine etc, might be sufficient for reaching the \"new-value\"\nlevel. So computer programs are not totally banned.\n\nThe \"not pure computer programs\" clause is about to be removed."
    author: "patents"
  - subject: "Re: ok, maybe it's just me..."
    date: 2003-09-01
    body: "I don't know how software patents (i.e. patents on logic, thinking, etc.) and freedom get together.\nThat's why they are bad.\n\nI would also state that logic (and because of that software) is prefigured by itself, it cannot be invented.\n\nAnd furthermore, the code itself is protected by copyright."
    author: "CE"
  - subject: "Re: ok, maybe it's just me..."
    date: 2003-09-01
    body: "A software application is just like a painting.\n\nYou have the tools make it.\n\nYou cannot patent the tools nor the result.\n\nBut you have the copyright."
    author: "patents"
  - subject: "Re: ok, maybe it's just me..."
    date: 2003-09-03
    body: "The problem with software patents is that not a software (the specific implementation itself) is patented, this would rather be a case for the copyright. However, the method that the software implements is being patented.\n\nThus, for a filecopy program you will file a patent for what it does - copying files. The patent is then also narrowed down on the specific method you implement but usually everybody tries to make their patents as broad as possible.\n\nNow imagine patents would pass for all programs that exist, it would in the future hardly allow you to even access your computer without paying for it."
    author: "tigloo"
  - subject: "kwintv"
    date: 2003-09-01
    body: "What about KWinTv? Is this application on anybody's screen? I'd like to be able to use a neat kde app instead of the old xawtv... "
    author: "Markus Heller"
  - subject: "Re: kwintv"
    date: 2003-09-01
    body: "I'm using it right now, because XawTV for some reason crashes my box, and zapping (a gtk app) sucks.\n\nActually, I am using qtvision, the latest rewrite. Seems to work just fine."
    author: "Roberto Alsina"
  - subject: "Re: kwintv"
    date: 2003-09-02
    body: "There are a few GUI bugs I'm aware of (especially with ATI cards) but as my TV card is broken, I can't fix them. :-(\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: kwintv"
    date: 2003-09-02
    body: "If they are in bugs.kde.org and they are rather simple stuff, I can take a look. If they aren't there, just email me.\n\nI am learning the thing anyway, because I am developing a security camera applciation for a client, and I am basically just multiplying qtvision by 4 :-)"
    author: "Roberto Alsina"
  - subject: "Re: kwintv"
    date: 2003-09-01
    body: "Unfortunatly, George Staikos is busy working with so many parts of KDE (everything from SSL stuff to kwallet), that I suspect he is too busy to work on kwintv atm. "
    author: "anon"
  - subject: "Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-02
    body: "See for yourself:\n\nhttp://the-amazing.us/simplyamerican/2172/\n\nSo, because KDE-folks do not like it when people use the project as a tool for political proganda, it gets labeled as \"anti-american\"? Mr. Stevens should have the right to put his propaganda in to KDE, otherwise KDE is anti-amercan?\n\nAlso note how he comments that it's OK to me \"European political statements\" in KDE (well, the website actually)? I wouldn't call the patent-protest an \"European\" protest, but rather a software-protest that goes beyond national borders. It just happens to be relevant in Europe right now.\n\nProtesting against software-patents does not mean that you could also use the project to further your own unrelated propaganda.\n\nNeil Stevens: Just what is your malfunction (apart from being a flag-waving jingoist)? I find it rather pathetic that when you are denied the right to use KDE as a vessel for your political BS, you start whining how you are an innocent victom of an anti-american witch-hunt or something. \n\nWhat do you suggest KDE to do in order not to be \"anti-american\"? Fill the codebase with comments that glorify USA and it's armed-forces, is that it?\n\nSeriously, I find your whining to be sad and pathetic."
    author: "Janne"
  - subject: "Re: Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-02
    body: "Neil just follows George W. Bush directive:\nThose that are not with us are against us.\nKDE - The Desktop Of Evil\n"
    author: "tomten"
  - subject: "Re: Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-02
    body: "Ok, before I've said good things about Neil, but now really this is too much... I honestly thought that nobody with half a brain could actually be this stupid. It's really scary, the guy has some serious problems.\n"
    author: "Sleepless"
  - subject: "Re: Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-02
    body: "> KDE should be renamed OEDE: Old Europe Desktop Environment \n\nHehe. Quite funny. :-D"
    author: "Funny"
  - subject: "Re: Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-03
    body: "How old is this Neil? Surely he is still in his teens. His behavior is more likely related to some kind of personality disorder. It is no use discussing any of his topics, since no logic or reasoning can be applied to it."
    author: "personality disorder"
  - subject: "Re: Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-05
    body: "You mean like most people on this site who bash him? Neil did what he did to make a *Statement* about what he saw as a double standard. That's it. It's his right to be able to make such a statement. It doesn't mean anyone has to agree with the statement though. As an actual contributor to free software, (no some user who is getting a free software lunch), I believe that if you want to use software that I have spent hours, days, etc of my time working on, you can accept it in the manner I write and present it and use it for free. Or, fork it and fix it if you don't like it. But don't come whining to me about not liking who or what I put in the contributors section.\n\nSheldon."
    author: "Tormak"
  - subject: "Re: Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-05
    body: "Oh do grow up. Stop rationalizing a try looking at where he is coming from. Maybe he has a point. Maybe he is misunderstanding the situation. If you don't like what he puts in his software then don't use it. But if you do use it, shut up."
    author: "Tormak"
  - subject: "Re: Neil Stevens: \"KDE is anti-american!\""
    date: 2003-09-08
    body: "\"Oh do grow up.\"\n\nI think it's Neil who should \"grow up\". I wasn't the one who wrote comments on how KDE is this and that, or how Neil is \"anti-Europe\". But Neil did. He thinks that since KDE does not praise USA (it doesn't praise Europe either), it must be anti-USA. Like I said, Neil needs to grow up.\n\n\"Stop rationalizing a try looking at where he is coming from. Maybe he has a point.\"\n\nThat KDE should not protest against software-patents (espesially if the protest takes place in, shock and horror, Europe!)? After all, software-patents have NOTHING to vdo with KDE, whereas glorious US Army has EVERYTHING to do with KDE, right?\n\nNo, he has no point. He has exactly ZERO point.\n\n\"Maybe he is misunderstanding the situation\"\n\nHe sure is. And I'm not surprised. The best thing I remember him from is his constant whining on how bad things are in KDE. With friend like him, who needs enemies?\n\n\"If you don't like what he puts in his software then don't use it. But if you do use it, shut up.\"\n\nObviously I'm not the only one who disliked his comments, since the comments were removed? And, being childish as he is, he removed his software from CVS."
    author: "Janne"
  - subject: "More information about CrystalSVG?"
    date: 2003-09-02
    body: "This is very intresting: \"More new CrystalSVG icons.\"\n\nDoes anyone know anything more about this / where can i find info about the new icons; where can i read more about new CrystalSVG? "
    author: "Magnus"
  - subject: "Enduring bigotry and ignorance "
    date: 2003-09-03
    body: "I'm writing this to all the thoughtless anonymous cowards out there who think there is a place for intolerance in a project like KDE. By now most people have some idea of the incident with Neil Stevens, though obviously many didn't take the time to figure out what it was really about. I don't want to argue it. I disagree with Neil on this and I feel his extreme tendencies are abrasive, however I respect him and his opinion. While I would not be found agreeing with him on this I also would not dismiss things he says out of hand. Neil seems to think there is an anti-American sentiment with KDE but I don't see that among developers. He does seem to bring it out in anonymous posters though.\n\nOver the last two days I've tried a number of times to type responses to several posts that I felt were particularly offensive. The problem is that I believe it is best to continue the honorable practice of not using my position as a project leader in KDE as a platform for devisive ideologies that are not directly relevent to the project. What upsets me is that I am in fact a politically moderate to conservative American, a US Navy veteran and a patriot who very much loves my country. I also detest the practice of parroting misleading sound bites with the arrogance of moral superiority. Of course \"political correctness\" grants it's practitioners that anyone not in agreement is a morally inferior barbarian. I happen to enjoy a good debate with people I can respect and who will respect me, regardless of how much we disagree on. That is because I'm civilized. However anyone who calls me a flag waving jingoist to my face should first consider their ability to sustain equal or greater abuse.\n\nThe big problem with Neil's protest is that he chose to use a political reference which is inappropriate, and I don't care to discuss motivations or particulars. What I would like to say is that everyone who used this as an opportunity to state their dislike for America, or the even more rediculous assertion that those of us who live here and have any national pride are \"pathetic\" or otherwise mentally defective, are in fact hypocrites! Anyone who seriously hopes to maintain any high moral ground would, by definition, NOT engage in the behavior they are critisizing. As someone who tests everything he hears to see if it rings true I witnessed a number of statements which I could spend an inordinate amount of time thoroughly trashing in a reasonable debate... but reasonable people don't make unreasonable statements and therefore it is irrational to expect people making emotional statements to actually be impressed by a reasonable argument. \n\nI would like to appeal the the better nature of KDE users and ask people to stop multiplying offenses by seeing how much they can offend others in turn. It's beyond unattractive. I'd especially like to suggest an end to hypocracy and some degree of humility. If we want to make the Dot an open forum let's at least show a little decency. If nothing else it will help you look to be more gracious after someone shreds your assertions like a mouse in a cuisinart and hands you your head. The misuse of the the term fascist to describe the US demonstrates the complete lack of any internalized awareness of what fascism is and is akin to confusing the NAACP with the KKK. Having talked with people who lived in fascist regimes, like Pol Pot which was known as the killing fields, or contemplating 30 million dead in Europe in the 1940s, it hardly seems like a word to use carelessly.\n\nI get a lot of really positive feedback from the community. I don't care what nationality anyone is who wishes to be involved in our project, but I do expect they will, and should, be proud of where they live. If I honestly thought that most of the people who used Quanta would be hostile towards me if I was only an American, and not the Quanta project leader, I would seriously question my commitments. Not because of nationality, but because of irrational bigotry. To succeed as a project we must be willing to see past the differences of our leaders and embrace each other across national borders. For those people hostile towards ANY nationality (or anyone being proud of their nation and heritage) I invite you to either grow up and get a mature attitude or go somewhere else and stop causing trouble. I would like to think a little more highly of people posting on the Dot."
    author: "Eric Laffoon"
  - subject: "Re: Enduring bigotry and ignorance "
    date: 2003-09-03
    body: "I've read through most of the threads in this article and I get the impression that there seems to be a consensus to refuse _any_ political statements (here on the dot and of course in about-dialogs in kde) e.g. I've found two posts here which can be characterized as anti-american, but in both cases there are numerous replies rejecting these political statements along with a call to the (political motivated) poster to go somewhere else.\n\nThe majority seems to be intelligent and sensible enough to be make kde \"fortified\" against political infiltration regardless if it's from from right or from left or from pro- or anti-americanism, pro-europe or anti-europe... etc. (this is not contradictory to the fact that most _do_ have a political opinion they stand up for, but they may choose a political forum to make them heard)\n\np.s.: I agree with you that you can only accept others the way they are if you accept yourself (including your nationality ... in a good and a bad sense)"
    author: "Thomas"
  - subject: "Re: Enduring bigotry and ignorance "
    date: 2003-09-03
    body: ">>>I'm writing this to all the thoughtless anonymous cowards out there\n\nAre you talking to me? ;)"
    author: "AC"
  - subject: "Re: Enduring bigotry and ignorance "
    date: 2003-09-03
    body: "I haven't seen any anti-american posts here, but then again, I haven't read all messages that have been posted about this.\n\nNow, some of us (Europeans and Americans alike) disagree with some US actions/policies. But that does not make any of us \"anti-american\". Disagreeing with some country does not make that person \"anti-thatcountry\".\n\nMy comments about this situation (as seen few posts above) are not anti-US IMO. They are about using KDE as a tool for political propaganda. Yes, I would have labeled them as \"propaganda\" and \"political BS\" even if they had been anti-USA or pro-Europe. Propaganda is propaganda, regardless of it's source or intentions.\n\nNeil could say that KDE is \"anti-USA\" if there were anti-USA comments in KDE. But there aren't any. He says that KDE is anti-USA because KDE does not carry pro-USA comments. By that same logic, KDE is anti-China, anti-Europe, Anti-Mars, Anti-India, Anti-Antarctica etc. etc. since there are no glorifying comments about those either.\n\nIt doesn't really matter that is it about lack of pro-USA comments or because he was denied the right to put those comments there. Such political comments are not allowed, period. To my knowledge, someone had put some pro-Europe comments in KDE at some point in the pat and they were removed as soon as they were noticed (Of course, Neil forgot to mention that tidbit in his whining).\n\nNow, protesting against software-patents is COMPLETELY different from praising some countrys army. Software-patents can have direct and severe consequences (spelling?) on KDE, so protesting against them is the right thing to do. US Army (or any other army for that matter) has not such effect on KDE, why should the be mentioned in KDE?\n\nAnd besides, are there any comments about software-patents in KDE itself? The protest involved the website, nothing more (to my knowledge)"
    author: "Janne"
  - subject: "Re: Enduring bigotry and ignorance "
    date: 2003-09-03
    body: "Janne you are smart person. i agree with you\n\nYou can discuss and discuss forever BUT.. its quite simple, theres no room for political shit in KDE software. and thats why the guy is kicked outta cvs access.\n\nof course he can have his pro US army about box, but it wont be in merged with KDE.. (sucky software should be removed anyway kabboodle or whatever)\nmakes sense to me\n\n\nCya"
    author: "joost"
  - subject: "Re: Enduring bigotry and ignorance "
    date: 2003-09-04
    body: "Actually, he wasn't kicked out. The offensive lines were removed in the code. He disliked that and other comments (don't know what they were) from others, and removed the applications himself. He was asked if he still wanted cvs access and he replied why would I.\n\nThanks Eric for your comments.\n\nDerek"
    author: "Derek Kite"
  - subject: "nations"
    date: 2003-09-03
    body: "> but I do expect they will, and should, be proud of where they live.\n\nActually, I generally hope, people don't attach their pride to stuff which they had little influence on (such as which country to be born in). It's quite natural to attach (positive) emotions to whatever you call home. Promoting pride to one's nation however may easily lead to lack of differentiated reflection on that nation's politics - especially whenever it comes to conflicts between nations. I'm very *happy* I live in the country I was born in (among other things, 'cause it's a rich, privileged and (comparatively) democratic country) and would not leave it unless given _very_ good reasons. But I certainly don't feel proud for that.\n\nAnyway, essentially I agree with your post. More than just \"two or three\" posts (as others believe to have counted) are anti-american. Some very strongly so, others not quite as openly hostile. Many definitely beyond the point of simply disapproving of certain politics (which are still not related to KDE), but rather expressing a difuse aversion against the US as a whole.\n\nI'm highly disappointed to see that (Old) Europe vs. USA thing being brought up again and again. This time, it was Neil who dragged this entirely unrelated conflict into the discussion - and loads of people here jumped to it. Please everybody: Get a grip on yourselves. It's understandable that emotions rise high, when talking about war (on Iraq), and my own opinion on that is rather strong and emotional, too. Me too, I don't much like the current American goverment for a variety of reasons (and yeah, neither could I think of a single European government, that I'd really positively like). But let's keep those discussions separate.\n\nI'm not saying, discusssions on the Dot should not be allowed to stray to topics not directly related to software. I'm not even saying, this wasn't a forum to cirticize people for opinions unrelated to software and voiced in personal blogs. (Those things don't really belong here, but should not be banned either). But please: When doing so - leave your nation at home, and don't expect me to bring mine either. Even when talking about foreign politics: Just don't condemn entire nations or even \"everything\" a certain goverment is doing. That's not only rediculous, but also bound to be offensive.\n\nWell, what to expect in a forum, where statements to the effect of \"Everything about GNOME (including the user base) is bad, while everything about KDE is good\" keep coming up? Let's keep trying, though...\n\nThomas"
    author: "Thomas"
  - subject: "Anti-American posts"
    date: 2003-09-04
    body: "> More than just \"two or three\" posts (as others believe to have\n> counted) are anti-american.\n\nCount them.  Four or five at most.  So long as your definition of \"anti-american\" does not include criticism of the US government, of course."
    author: "em"
  - subject: "Re: Anti-American posts"
    date: 2003-09-05
    body: "Sure, it depends on the definition. I won't even try to give one. But I do sometimes wonder, why people are so eager very to voice their criticism of the US (government). That does not necessarily mean, I disagree with the criticism itself, actually I think many of those criticisms to be quite valid for themselves. I just get suspisicious when stuff like that starts turning up in completely inappropriate places.\nNow, as mentioned, I don't think all posts in a forum like this need to be on topic. And this time, it was Neil, who gave a rather obvious pointer to US foreign politics. So it's not really surprising, some people state their opinion on Iraq. I do wonder, though, how e.g. complaints about racism in the US got into this.\nBTW, I recently read a rather interesting side-note, to the effect, that of all countries with racist tendencies (which sadly means of absolutely all countries in the world), the US are the only one, where a member of a discriminated against minority can make it to Secretary of State. It does make you think, no? And so - to pick a random but fairly obvious example - why have I never read anything about Danish immigration politics on the Dot?\nMaybe, you're even right, and no single post (or well, at least only few posts) can by itself be judged obviously anti-American. But the frequency with which critical statements about the US are voiced in this forum does seem suspiciously high to me. It seems to be symptomatic of a broad sentiment. Here, I do see, how US-citizens (even if they may agree with some of the criticisms themselves, and even if they may not tend to high levels of patriotism) will quite rightfully feel offended.\n"
    author: "Thomas"
  - subject: "Re: Anti-American posts"
    date: 2003-09-05
    body: "Sometimes it bugs me, that I've choosen my simple name (which is indeed a very common one) as my nick... I think it simply was luck, that nobody else was using it until now... Thomas, welcome to\nhttp://dot.kde.org/search?author=Thomas  \n;-) hope you feel comfortable... at least I can agree on what you've written"
    author: "Thomas"
  - subject: "Re: Anti-American posts"
    date: 2003-09-05
    body: "Well, I didn't think about that until after posting. Guess I'll change my nick to Thomas2. Hope that will reduce the confusion."
    author: "Thomas2"
  - subject: "Re: Anti-American posts"
    date: 2003-09-05
    body: "> And so - to pick a random but fairly obvious example - why have I never read\n> anything about Danish immigration politics on the Dot?\n\nI think the answer should be quite evident.  I know nothing about Danish immigration policies, and I expect that this is the case of most posters to the Dot.  On the other hand, I can't avoid being aware of US politics, and thus having an opinion on them.  They're on TV, they're on the papers, they're on Slashdot and they're on the Dot.  They get to affect many things I do, even KDE.  Definitely not the case of Danish policies.\n\n> But the frequency with which critical statements about the US are voiced in\n> this forum does seem suspiciously high to me.\n\nI don't think critical statements are more *frequent* (though maybe more numerous, in this forum) than supportive statements.  Pro-US government posters are contributing to this noise as much as anti-US government posters.  As long as a certain civility is observed, I don't believe this is entirely bad, either.  At least it makes people aware of the existence of different views on US politics, that might not be so easy to get especially for people inside the US."
    author: "em"
  - subject: "Re: Anti-American posts"
    date: 2003-09-06
    body: "As an American, I can say I agree wholeheartedly that many Americans just simply don't know about the real nature of the debate outside the US.  From what you hear on CNN and American network TV, the only motive for opposing US policies is an attempt by Western European powers to create some sort of counterbalance to the US superpower.  I don't think most Americans know that the invasion of Iraq violated the UN Charter (i.e. international law) and if informed, I don't think they'd care.  Americans see no similarity at all between Iraq's invasion of Kuwait a decade ago, China's invasion of Tibet, and the recent US invasion of Iraq.  It's hard for non-Americans to imagine someone being able to miss such an obvious parallel, but believe me, it's true.\n\nIt's terribly difficult for the average American to get unbiased news on Iraq.  The best many hope to do is get two equal but opposite biases (i.e. Fox News and Al Jazeera) and try to piece together reality halfway in between.  I appreciate that many Europeans take the time to challenge Americans on their policy of invading countries we don't like, but I also hope that Europeans refrain from seeming too superior.  Any nation with a permanent seat on the Security Council can violate international law with impunity, and in fact they do, fairly regularly.\n\nJust because the US is currently the biggest danger to democracy and the self-determination of nations does not mean everyone else is pure as the driven snow.  Accept your own faults, but please, PLEASE, continue to point out ours whenever we seem to be blind to them (which is most of the time).\n\nThank you."
    author: "ac"
  - subject: "Re: Anti-American posts"
    date: 2004-05-05
    body: "I would like to find definitions of \"Anti-American tendencies\".. anyone?"
    author: "J"
  - subject: "Re: Enduring bigotry and ignorance "
    date: 2003-09-04
    body: "thank you... i felt the same thing but u said it said so much better than i could of"
    author: ":-)"
  - subject: "Your right!"
    date: 2003-09-04
    body: "People should leave their politics at home when on the dot at least, this isn't the place for it and I too think we needto be more united as a communtiy and set our regional differences behind us.\n\nIn addition a fantstic improvement would be a registration system for the dot so that only people who actually have something to say will post. Most trolls only want to waste a minute or two falming and they would not want to bother to register, besides people would always see their names and therefore would learn to ignore them, unelss they are really insistent and constantly create new names which clearly shows that they don't have a life.\n\nPlease think about it, it would help the community untie, keep trolls away and have fewer, but more meaningful posts. Imagine if you found that the houses next to you had a new person living in them every day, it's kind of hard to make friends and establish connections that way, registering would help us get to know each other.\n\n\nPlease consider it. \n\nBTW: eric, can I see a preview of the new KittyHooch website you were working on?"
    author: "Alex"
  - subject: "Re: Enduring bigotry and ignorance "
    date: 2003-09-04
    body: "> but I do expect they will, and should, be proud of where they live.\n\nYou will find some of us are actually opposed to nationalism.  I happen to believe nationalism is just a tool used by egocentric politicians to increase popular support and discourage criticism for their actions.  And sadly you can see it works.  People abroad are opposed to invading Iraq?  They're ANTI-AMERICAN and should be ignored or even hated.\n\nHumanity should be facing the challenges of our future together, and instead we lose our energy in stupid infighting for the only benefit of power-hungry politicians who will never care about people like us.  Neil probably has much more in common with me than with American \"aristocrats\" like Bush or Cheney, and yet he hates me because I am European and I oppose their actions.  That is what nationalism and \"pride of where you live\" brings you."
    author: "em"
  - subject: "Excellent!"
    date: 2003-09-05
    body: "Eric,\n\n  Excellent response. You stated succinctly and rationally exactly what needed to be said. Kudos."
    author: "Tormak"
  - subject: "Online forums thrive on this stuff!"
    date: 2003-09-10
    body: "Flamewars, ahh, what would  asite be without them, well I'll tell you what it won't be, it won't get 5 times the comments and 10 times the hits, that's for sure. ;)\n\nFlamewars are nice, they keep us busy from real wars and preoccupied when there is nothing to do, it's like watchinga  soap opera ;) I like that the Dot did not close this thread."
    author: "Mario"
---
In <a href="http://members.shaw.ca/dkite/aug292003.html">this week's Digest</a>:  It's been a very busy week. Some new applications: <a href="http://www.tjansen.de/knot/">Knot</a>, 
a service location server, Kickme, a lightweight DCOP messenger and event viewer,
an LDAP kioslave, <a href="http://kwifimanager.sourceforge.net/">KWiFiManager</a>, for monitoring wireless cards, 
the new <a href="http://www.kde-look.org/content/show.php?content=7559">Plastik</a> widget style, 
an SNMP plugin for <a href="http://apps.kde.com/na/2/info/id/1359">KSim</a>. 
<a href="http://www.kde.org/areas/multimedia/">aRts</a> adds 
<a href="http://www.mediaapplicationserver.net">Media Application Server</a> output support. 
<a href="http://www.kdevelop.org">KDevelop</a> adds Haskell and Mozilla XUL language support. 
<a href="http://edu.kde.org/kstars">KStars</a> supports electrical telescope focusers. Security fixes for KDM.
<a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a> has phone calling support. 
Spell checking support in <a href="http://www.koffice.org">KOffice</a> is complete, and improved elsewhere. The
KitchenSync plugin is added to <a href="http://www.kontact.org">Kontact</a>. 
<a href="http://pim.kde.org/components/korganizer.php">KOrganizer</a> printing system is improved. 
Safari render_layer code is ported to KHTML.
More new CrystalSVG icons. Improvements for running KDE on laptops. And a huge number of bugfixes.
<!--break-->
