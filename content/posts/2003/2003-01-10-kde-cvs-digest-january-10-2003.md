---
title: "KDE-CVS-Digest for January 10, 2003"
date:    2003-01-10
authors:
  - "dkite"
slug:    kde-cvs-digest-january-10-2003
comments:
  - subject: "again"
    date: 2003-01-09
    body: "the new format looks very nice, Derek.  Great job as always."
    author: "Caleb Tennis"
  - subject: "Re: again"
    date: 2003-01-10
    body: "AWESOME job Derek... I can't thank you enough for these summaries."
    author: "David Nielsen"
  - subject: "HTML breakage"
    date: 2003-01-10
    body: "Whoops... The page doesn't render properly in Moz 1.0. It gets cut off halfway through the Konqueror bug fixes by a (malformed?) HTML comment.\n\nRenders fine in Konq tho (of course)."
    author: "AC"
  - subject: "Re: HTML breakage"
    date: 2003-01-10
    body: "works for me with mozilla 1.2.1"
    author: "AC"
  - subject: "Re: HTML breakage"
    date: 2003-01-10
    body: "I guess I should upgrade. Still, those HTML comments should be escaped with &lt; and &gt;. They are in one place, but not another:\n\n<p class=\"line\">Dirk Mueller committed a change to kdelibs/khtml/html</p>\n<pre>handle &lt;!--&gt; and &lt;!---&gt;\nCCMAIL<a href=\"http://bugs.kde.org/show_bug.cgi?id=34302\">34302</a>-done@bugs.kde.org\nBackported to KDE_3_1_BRANCHhandle <!--> and <!---> (backport)\n\n3.1rc4 Konq cuts the word \"and\" out in the last line, at least."
    author: "AC"
  - subject: "Re: HTML breakage"
    date: 2003-01-10
    body: "Does it get screwed up after ....\n\nDirk Mueller committed a change to kdelibs/khtml/ecma\n \ndon't crash\nDiff\n\nDirk Mueller committed a change to kdelibs/khtml/html\n\nthen goes wonky? If so, this is what is there...\n\n <pre>make &lt;select&gt;&lt;option&gt;    text&lt;/select&gt; work....\n\nThis is somewhere in the middle of the konq. bug section. \n\nCould be the early mozilla got confused with the escape characters.\n\nDerek\n(I fixed the email address. speaking of escape characters....)"
    author: "Derek Kite"
  - subject: "Re: HTML breakage"
    date: 2003-01-10
    body: "Found it. Fixed it. I didn't escape the second instance of the comment.\n\nShould be ok now.\n\nAck. Another one. <file>. FIxed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: HTML breakage"
    date: 2003-01-10
    body: "Thanks. I love the CVS digest, BTW. :-)"
    author: "AC"
  - subject: "Coding style question"
    date: 2003-01-10
    body: "Why do they use !(x == y) instead of x != y for example in http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/khtml/rendering/render_style.cpp.diff?r1=1.54&r2=1.55&f=h\n?  (And why does the dot prevent me from posting html?)"
    author: "em"
  - subject: "Re: Coding style question"
    date: 2003-01-10
    body: "Re html posts, the Dot has had them disabled for a couple years now, after I reported an exploit in their forum software (someone had used the hole to change all links on this site to some place unmentionable).  This problem has either never been solved, or the admins don't want to risk it again."
    author: "Justin"
  - subject: "Re: Coding style question"
    date: 2003-01-10
    body: "That is unfortunate, because it leads to posting URLs as text (instead of hyperlinks) which in turn breaks the page layout in a most annoying way."
    author: "em"
  - subject: "Re: Coding style question"
    date: 2003-01-10
    body: "I don't have any faith in any the HTML parsing code I've seen.  It would definitely be nice though, so I'll probably give it a try (this weekend...).\n"
    author: "Navindra Umanee"
  - subject: "Re: Coding style question"
    date: 2003-01-11
    body: "In konqueror, everything is an hyperlink!\n\nSelect the URL, then middle-click, IIRC. That should open the URL."
    author: "Roberto Alsina"
  - subject: "Re: Coding style question"
    date: 2003-01-10
    body: "Probably to stress that a specific operator (==) is to be used. You can define your own == and != operators for objects, although != will be implicitly mapped to !(==) if it has not been defined. If you have large hierarchies of classes, you tend to lose track of which operator was defined in what class..."
    author: "me"
  - subject: "Re: Coding style question"
    date: 2003-01-10
    body: "In my experience != is NOT implicitly mapped to !(operator==).\nSo for your own classes, you need to define both ... or to use !(a==b), which is probably why khtml does the latter."
    author: "David Faure"
  - subject: "Re: Coding style question"
    date: 2003-01-10
    body: "Actually IIRC the reason for using !(a==b) is that libstdc++v2 (shipped with gcc-2.95) wasn't namespace clean and defined a public template operator!=, which took its arguments using references. And since some data members in KHTML classes are bitfields, and one cannot take pointers(i.e. also references) to bitfields, this caused compile errors(the libstdc++ operator was used instead of compiled generating a \"normal\" one)."
    author: "L.Lunak"
  - subject: "Safari changes"
    date: 2003-01-10
    body: "The KDE-VCS-digest mentions that some of the Safari changes are incorporated into Konqueror from KDE_3_1_BRANCH. It is planned to incorporate all the Safari changes to Konqueror before releasing KDE3.1. Would be a good idea, I think, even though this may postpone KDE3.1 a little further."
    author: "Michael"
  - subject: "Re: Safari changes"
    date: 2003-01-10
    body: "Each changeset will be judged independantly. Some are trivial to oversee and yet have an impact on the performance, those are likely just commited.\n\nEverything that is a harder to oversee change, but makes sense will be commited to HEAD only, probably backported later, when it proved working good enough, for a 3.1.1.\n\nAnd everything else, that is architectural, will see only HEAD and 3.2\n\nThat's the natural way to do it and I guess, the KDE people will only do it better.\n\nRegards, Kay\n"
    author: "Debian User"
  - subject: "Re: Safari changes"
    date: 2003-01-10
    body: ">incorporate all the Safari changes to Konqueror before releasing KDE3.1\n\nSome are going in. The easy ones. And the crash fixes. The developers are impressed with the work and using as much as possible as quickly as possible. Someone even told Dirk to go to bed :-)\n\nIIRC the Apple people were working from a 3.04 base. So there is a divergence in the two trees which will take a while to sort out.\n\nI doubt if 3.1 will be delayed any further.\n\nDerek (IMHO)"
    author: "Derek Kite"
  - subject: "Apple changes.."
    date: 2003-01-10
    body: "Is there anyone working on intergrating the Apple changes to khtml?\nI assume Apple won't do it,why should they ;)"
    author: "anon"
  - subject: "Re: Apple changes.."
    date: 2003-01-10
    body: "Have you read the other comments? :)\n(yes)"
    author: "AC"
  - subject: "Re: Apple changes.."
    date: 2003-01-10
    body: "To me it came as a surprise to learn that Apple\nwould not only provide it, but also help merge\nit.\n\nAfter all, the name is Apple.\n\nBut if you think of it. While keeping it of course\nsecret to gain an edge over others (who? would\nSun even try to learn from them?!), after it was\nreveiled, it all comes down to cost.\n\nAnd maintaining a branch costs more than\nincorporating the tested KDE releases with\nall merges.\n\nSo they  can focus on the UI. And that's what\nApple believes they can do best.\n\nSeems Ok to me, Kay\n\n\n"
    author: "Debian User"
  - subject: "Hi mommy! ;-)"
    date: 2003-01-10
    body: "Heh, it's nice to see your own commit showing up in KDE CVS digest.\n\nOne small remark though: my commit originally contained the text \"<quote>Vladimir is really fast in finding and fixing reported bugs</quote>\". It looks like those pseudo-tags were lost. Oh, and linebreaks made my commit more understandable :-)\n\nYou know, last week I was thinking it would have been great if one of my commits did ever show up in the CVS digest. If only all my dreams could become reality... ;-)"
    author: "Andy Goossens"
  - subject: "Re: Hi mommy! ;-)"
    date: 2003-01-10
    body: "> originally contained the text \"<quote>Vladimir...\n\nI guess I took them out too quickly. I'll put them back.\n\nFunny I've never run into this until now. Maybe a plot to keep me on my toes.\n\nDerek"
    author: "Derek Kite"
  - subject: "Very useful"
    date: 2003-01-11
    body: "Derek: there's so few comments here, I just want to make sure you know that's because it's so comprehensive and so useful.  Wonderful job!"
    author: "Dan K."
  - subject: "Great job...Again!"
    date: 2003-01-13
    body: "Hey Derek, just another post of praise here.  Thanks so much for the hard work.  I really enjoy the weekly updates!\n"
    author: "beergeek"
---
The latest KDE-CVS-Digest is <A HREF="http://members.shaw.ca/dkite/jan102003.html">now available</A>. This issue features news about Apple using khtml in Safari and the subsequent merging of Apple's contributions, support for <a href="http://www.rdesktop.org/">RDP</a> in the KDE Remote Desktop Client (krdc) and numerous bug fixes and new features. Enjoy!
<!--break-->
