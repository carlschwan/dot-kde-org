---
title: "KDE Traffic #58 is Out"
date:    2003-07-16
authors:
  - "rmiller"
slug:    kde-traffic-58-out
comments:
  - subject: "Kittens?"
    date: 2003-07-16
    body: "We've got the hooch! (Kitty Hooch that is... yes, the only cat intoxicant to support a KDE project) ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Kittens?"
    date: 2003-07-16
    body: "Eric, something totally offtopic, but i installed quanta from cvs, with --prefix=/usr --with-kafkapart (or something similar) yesterday, yet i didn't have a wysiwyg frame anywhere ;o) How come? :o)  Thanks! </OT> "
    author: "Verwilst"
  - subject: "Re: Kittens?"
    date: 2003-07-17
    body: "> Eric, something totally offtopic, but i installed quanta from cvs, with --prefix=/usr --with-kafkapart (or something similar) yesterday, yet i didn't have a wysiwyg frame anywhere ;o) How come?\n\nWell, since this has so many factors I have a high percentage chance of blowing it... here goes...\n1) You aren't going to get anywhere installing Quanta with kakfa enabled unless you are running KDE CVS HEAD because kafka is now dependent on new code in KHTML.\n2) Kafka doesn't actually get it's own frame per se, it takes the same location as the text editor when activated. Maybe you meant that.\n3) Nicolas gets pretty excited when he starts coding and then throws out a few hundred lines of code after thinking about it. ;-) The entire interface has been re thought and there are several other people working on it. Currently what it was is becoming several parts: a public interface in KHTML; the Kafka part; the syncronization code to integrate it with Quanta and access Quanta's feature set.\n\nFor the next few weeks you should feel happy if it runs as code is in the process of replacing old code and some is going to be stripped out."
    author: "Eric Laffoon"
  - subject: "Re: Kittens?"
    date: 2003-07-17
    body: ":o) Okido, thanks :o) It compiled for me but nothing kafka-related appeared in Quanta ;o) I'm running KDE 3.1.2, so i'll hold on for 3.2alpha to arrive ;o)\nThanks!"
    author: "Verwilst"
  - subject: "KDE Traffic lateness"
    date: 2003-07-16
    body: "> Better late than never, I suppose. I'd tell you why it was late, but I\n> suspect you wouldn't believe me if I told you. Let's just say I had a very\n> good reason, and leave it at that. \n\nLet me guess... you had a date with Natalie Portman, right? :-)"
    author: "em"
  - subject: "Re: KDE Traffic lateness"
    date: 2003-07-17
    body: "> Let me guess... you had a date with Natalie Portman, right? :-)\n\nNope, Natalie and Britney (Spears) were with me :-P\n\n"
    author: "nbensa"
  - subject: "Re: KDE Traffic lateness"
    date: 2003-07-17
    body: "Nothing that banal. :-)"
    author: "Russell Miller"
  - subject: "kafka"
    date: 2003-07-16
    body: "Hi,\n\nI have the same problem, I get kafka to work standalone, but included in quanta, it's not working:)\n\neven OT\n\nbr Philipp"
    author: "Philipp"
  - subject: "Re: kafka"
    date: 2003-07-17
    body: "> I have the same problem, I get kafka to work standalone, but included in quanta, it's not working:)\n\nI'm going to guess you have KDE CVS HEAD installed. See my above answer. I haven't even tried it standalone for ages. My last discussion on Kafka as stand alone centered around getting everything ready for KDE 3.2 prior to the freezes. At this time Nicolas indicated he was focused on that and that Kafka would be incomplete as a stand alone program after his work. Granted it would be much farther ahead than ever before and KHTML would have the code to write to the DOM nodes in it. So it would be a substantial start for someone wanting to integrate WYSIWYG into a KDE program, but the tagging engine and all the years of development that went into making Quanta what it is will not be getting duplicated, at least by Nicolas, this year."
    author: "Eric Laffoon"
  - subject: "brockenboring is like superstring theory..."
    date: 2003-07-16
    body: "...they both make my head hurt.  Anyone care to give a non-obfuscated explanation of what it is, for those of us who don't necessarily know the ins-and-outs of kdeinit, and who wouldn't know unsermake if it bit us in the butt?"
    author: "LMCBoy"
  - subject: "OT: pls vote for this bug"
    date: 2003-07-16
    body: "\nhttp://bugs.kde.org/show_bug.cgi?id=45570\n\nit's about khtml not supporting the \"border-collapse\" css-property in tables"
    author: "yg"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "if i may suggest, the best way to make voting on bugs.kde.org absolutely worthless is to recruit people wholesale to vote for various random bugs by posting them off-topic to places such as theDot. such campaigning distorts the statistical relevance inherent in the process. while you may achieve a surge in votes for your pet bug, you'll be doing a disservice to all the other bugs that have garnered votes \"the hard way\" even though those votes are probably much more relevant/important. to \"even\" the game, everyone would then have to promote their own pet bug for voting, which would not only bring everything back into parity (meaning you gain nothing) but it would also piss off a lot of people who would just like to read theDot and other such resources without having to skip over such OT postings. some of those people are developers who can and do actually fix bugs.\n\ntherefore i'd suggest that instead of helping your cause, you're hurting it along with all the other bugs in b.k.o. as well as the general quality of theDot. \n\nor maybe i'm just crotchety today."
    author: "Aaron J. Seigo"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: ">  therefore i'd suggest that instead of helping your cause, you're hurting it along with all the other bugs in b.k.o. as well as the general quality of theDot. \n\nI agree. As usual your main argument was very well stated.\n \n> or maybe i'm just crotchety today.\n \nI'm sorry Aaron, this is the only part I don't get. It's the \"or\" that gets me. It could just be my usability philosophy I apply to Quanta seeping in... but can't you be both right *and* crotchety today? ;-)"
    author: "Eric Laffoon"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-21
    body: "It's a good thing he didn't say XOR then\n\n"
    author: "JohnFlux"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "Sorry, Aaron. I disagree here.\nI'm following the whole voting thing for some years now in Mozilla.\nMozilla's core developers often did care next to nothing about\nwhat has been voted for and what not. They keep claiming that other\nimportant bugs wouldn't be fixed otherwise. But did that justify that\nsomething like proper integration of a new splash screen stayed open\nfor years though lots of alternative splash screens were available??\nIMO the community is an important factor in Open Source software. \nAnd bug-voting is an important part of this Open Source community. \nVoting in general is an important part of a democrary. We always know\nwe are a small individual in a large country but we know nevertheless\nthat we can influence what's going on at least a bit with our vote.\nSome arguments coming up with bug-voting are in striking contrast\nto the democrary principle of voting. In the real world this would mean:\n1) Polticians should better ignore elections. Other important things like\npainting the mountains in a different color wouldn't otherwise be done\nthough all _politicians_ agree this is important though nobody votes for it.\n2) No party or group should really do any advertising. This only distracts\nfrom people recognizing what the really pressing problems are.\n\nI know people work on KDE for free. And they do what they like. That's\nOK with me of course. BUT: If there is a voting mechanism you will have to\naccept that it works like voting usually does with all its advantages and\ndisadvantages. And sometimes programmers should really take a second and\nask themselves twice why a bug has so many votes. Not because an Open Source\ndeveloper MUST do what the votes say, but because the voting system in general\n(not a single vote by itself) leads to a better \"society\" i.e. to a better\nKDE in this case. If he comes to the conclusion that he really doesn't want\nto do it he is of course free to do so, but I sometimes have the impression\nthat voting or complaining about bugs by people who cannot program\nthemselves might seem like some intrusion from the outside world. If someone\nthinks it's so important that he is campaigning for it - why not?\n\n\n\n\n"
    author: "Jan"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "With all due respect, Aaron is not saying that voting should be ignored; far from it. Developers do look at the list on a regular basis.\n\nWhat he is saying, is that the voting, if don't campaign for their bug, will give a good impression of what bugs are currently most important to the community. Campaigning for more votes for a bug effectively gives it undue votes compared to the importance.\n\nThis means that when developers go to fix something, when they start at the top of the list, they end up fixing bugs which the community does not find important. The Community is then dissenfranchised and KDE suffers from not having the most essential/irritating bugs fixed first.\n\nUltimately however, voting without bias will allow the most critical bugs to float to the top, because by definition, critical means that it causes problems and annoys people the most.\n\nFor the process to be truly effective it needs to exist without people campaigning to hike the votes for minor bugs (if it was major, it would aquire votes on its own merits)."
    author: "Dawnrider"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "My view on this as the Cervisia maintainer...\n\n1) Mozilla != KDE. KDE developers *do* care about votes and they change there to-do list accordingly. But campaigning for a bug IMHO destroys the trust in those vote numbers because the ratio to the other bugs is out of balance.\n\n2) This vote recruitment couldn't be more OT. The bug doesn't even have anything to do with the content of the KDE Traffic. Also theDot is the wrong place for this.\n\n3) IMHO this is getting as annoying as the campaigning of political parties. If I want my pet app to improve, I go to b.k.o and search the bug list for interesting entries and vote for them. I don't need any campaigning for this.\n\nChristian"
    author: "cloose"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "Imo bugs.kde.org already has the most apparent and easiest to use voting interface of any bug tracking system. I'm somewhat surprised that you compare it to Mozilla's set up of Bugzilla which, as you rightly pointed out, by far doesn't emphasise voting as much as bugs.kde.org does. If you still see weak points regarding voting at bugs.kde.org, by all means let me know about them. Thanks."
    author: "Datschge"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "I don't understand how anyone can possibly disagree here without just being really detached from a number of realities. The comparison to elections and advertising is truly astonishing given that campaign reform and an attempt to end undue influence returning to a \"one person, one vote\" ideal has been at the forefront of politics for years. Certainly if someone is campaigning for office they need to get their message out, but then if someone is in office it is a republic, not a democracy. See your dictionary. This is another set of criteria.  Regardless, there is an issue of \"buying votes\" and I defy you to show me anyone who enjoys the political ads during election years. We accept it as part of the process.\n\nBugs are a different process and the voting is more democratic. Quite simply you vote for what is important to you. If it does not affect me, and if I am not the maintainer, I don't care. I can't. I have my own bugs and wishes to fix. Aaron's point was exactly correct. Once the escalation of campaigning begins we no longer find the voting representing your experience or concerns, but the effectiveness of someone's PR efforts. Clearly those bugs without promotion now need their own promoter. Enter the hoarde of sleezy bug promoters and take everything you read everywhere and liberally mix in large quantities of off topic \"support my bug\" advertising... now make sure your bug gets the most votes and do whatever it takes. openbugvoters.org will raise up to make sure their bugs will have the most votes! Their portfolio will be filled with honorable mentions of extremely popular obscure and minor bugs in the hall of fame...\n\nIt kind of drives home why OT is short for off topic and open source is about community. I can't for the life of me understand how anyone could see OT bug vote solicitation as good for the community. If votes don't reflect real user experience and are there to create noise then what sane community could tolerate them? The integrity of the system is only there if you vote your own votes."
    author: "Eric Laffoon"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "Then developers should also stop to say \"I will only implement this if it gets 1000 vote points\". That's what provokes campaigning."
    author: "Anonymous"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "There is only one developer who did it. And of course if a developer for an app \nencourages that,\nthat's different. And in that case, it ended up an utter disaster, resulting in \na meaningless long thread with probably around 100 messages, noisifing both kde-bugs-dist\nand kmail lists considerably. People also need to understand that:\n1. Not all developers pay attention to votes.\n2. Being pushy and noisy may not pay. Volunteers are under no obligation \nto deal with all reports, and when reporters act in a condescending or demanding \nmanner, produce too much noise, etc., many people would rather go spend their \ntime helping someone else. In particular when that someone else sounds like a person\nwhose bug report is really paining them. \n3. Extra noise hurts everyone. Contributors, in particular volunteers, have very\nlimited time. Watching over all bug report activity is  *very* time consuming, \nand only a small number of developers do it. When people popularize particular \nbug reports, and encourage non-technical traffic on them (me toos, etc.) they \nmake it much harder for people to deal with bug reports directly and promptly. \n"
    author: "SadEagle"
  - subject: "Re: OT: pls vote for this bug"
    date: 2003-07-17
    body: "Mozilla got it in Feb 2002 - http://bugzilla.mozilla.org/show_bug.cgi?id=41262"
    author: "Tar"
  - subject: "Knopdex"
    date: 2003-07-17
    body: "As long as it contains a very clear warning, I don't think it will cause a lot of problems.\n\nI know I would try it, just to get a look at how KDE is progressing :)"
    author: "Joergen Ramskov"
---
<a href="http://kt.zork.net/kde/kde20030716_58.html">KDE Traffic #58</a> has been released, at long last.  Covers discussions on artsbuilder, brockenboring, and more.  And, oh yes, lots and lots of cute, tiny, fuzzy kittens.  Get it at <a href="http://kt.zork.net/kde/latest.html">the usual place</a>.
<!--break-->
