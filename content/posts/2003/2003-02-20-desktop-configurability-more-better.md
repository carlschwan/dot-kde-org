---
title: "Desktop Configurability: Is More Better?"
date:    2003-02-20
authors:
  - "Dre"
slug:    desktop-configurability-more-better
comments:
  - subject: "do both !"
    date: 2003-02-20
    body: "\nwhy not do both ?\n\njust have a configuration option named \"expert\" which would allow\nyou to tune your desktop up to the tiniest details when activated,\nand which would let just the main options visibles when deactivated\n\n\nthat being said if I'd be forced to choose between the two\ni'd choose \"more is better\""
    author: "anonymous coward"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "[++++++++++++++]\n\nVery good idea"
    author: "Shift"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "Ack!\n\nMake the interface simple for novices but leave the option to configure everything for expert users. If the normal kcontrol modules become too crowded, add s.th. like Win PowerToys (is that what it was called) but include it in the KDE default installation. This way novice users can use KPersonalizer/KControl for standard options and experts can play with all possible settings in an extra module.\n"
    author: "Anonymous"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "I would go for the microsoft way:\n\nA KControl panel, with only minimal day-to-day configuration options and a KExpertUserControl, where you could do all the dirty and superfluous configuration.\n\nThe only problem is the interoperability of the two pannels... "
    author: "zelegans"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "The big problem is, what exactly are \"day-to-day configuration options\"? This whole \"less is better\" meme fails to grasp the fact that you don't configure stuff on a daily basis. I've touched the Windows control panel twice in the last year. I touch the KDE control panel only to change styles and backgrounds.\n\nThe stuff that is used most often should be easier to access and understand, such as changing your background and styles. But don't hide away the other stuff. If you think the current control panel is difficult, consider how much worse it will be with TWO control panels! If your distro absolutely has to have a dumbed down interface, then write a dumb control center for that distro, without forcing it on the rest of the world."
    author: "David Johnson"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "Yes, I understand your point. \n\nI don't want a dumbed down interface, I want an interface that is minimal and elegant. \n\nFor example, there are at least 3 locations in kcontrol that set font properties. It is cool and (I must admit, sometimes usefull) that you have this much flexibility, but is it necessary and convenient for 90% of potencial KDE users? I don't know. \n\nI'm not advocating that KDE should be a \"one button washing machine\" like the mac, I'm just trying to make the point that things should be kept as simple and coherent as reasonably possible. \n\nIf, to keep both camps happy, a PowerKControl and a LameKControl have to coexist... well, I have no final position on the issue ;)\n\n\n\n\n"
    author: "zelegans"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "Hey, I just wrote KNewbieControl, especially for you:\n\n$ kcmshell LookNFeel/{background,kwindecoration,style,colors,fonts,screensaver}\n\nm.   :-)"
    author: "Melchior FRANZ"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "*ROTFLOL*\n\nBravo!"
    author: "anon"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "Actually, it's pretty cool that this sort of thing is possible. Fancy writing a quicky tutorial about the options? We can add it to the dot tutorial series.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "> writing a quicky tutorial about the options\n\nI'm afraid there isn't much to say about the options. Just do a \"kcmshell --list\". Of course, the whole idea can easily be used in a *.desktop file that can be put anywhere in /home/grandma/.kde/share/applnk so that it appears in the menu:\n\n\n[Desktop Entry]\nExec=/bin/sh -c \"kcmshell -caption KNewbieControl LookNFeel/{background,kwindecoration,style,colors,fonts,screensaver}\"\nIcon=kcontrol\nType=Application\nName=Newbie Control Center\n\nPS: /bin/sh is only used to make use of shell globbing. :-)"
    author: "Melchior FRANZ"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "Impressive!!!!\n\n:))"
    author: "zelegans"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "Dude, you just made my day !\n\nKudos to the KDE developers for such an awesomely elegant thing."
    author: "Anonymous George"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "Nice!\n\nClearly there are a lot of nice things that is possible to do just with at single command line. This need to be documented somewhere.. Such things opens up the possibilites for system administrators to make life easier for their users... \n\n/Mats"
    author: "Mats"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "They tried this in GNOME (Nautilus) with user levels and registry-editors (UGH) but I think decided it didn't work and nobody liked it.  Why not do like the Mac and put Advanced options in a separate Tab?  KDE does that already I think.  I like the way KDE is already anyway."
    author: "ac"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "Why would anybody want that?\n\nThere is no such thing as a general \"novice\". Some people are novices in one area and experts in others.\n\nI'm a programmer and have used computers for over 10 years intensively. Yet I'm sure that some secretary who won't touch computers unless forced will need more configuration options in KWord than me.\n\nBut in that \"user level\" scheme the secretary would be classified as \"novice\" and would only get a tiny subset of configuration options. So the secretary can either switch to \"expert\"-mode, live with workarounds because she can't find the config options or not use KDE at all.\n\nForget \"user levels\". It's a stupid idea.\n\n"
    author: "Roland"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "The problem is that every user has a different idea of what the 'essential' configuration items are. For instance an old-style X user will go mad until\nhe sets the focus-follows-mouse policy, but a windows user won't even know\nwhat that means.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: do both !"
    date: 2003-02-20
    body: "Well, if you guys don't think that people will agree to what the essential config items are,. maybe what we need is a way to let everyone configure which items should be essential and which are \"advanced\" config options... \n\n...and that way we would have one more thing to tinker with and configure!!!!\n\n;)\n\nHonestly, I like it the way it is, some config pages could be a bit more polished, but other than that it's fine with me. It's KDE, not gnome, not Windows, or anything else... let it be what it is...\n\nCheers from Uruguay, South America"
    author: "Phantom"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "LOL!  Instead of deciding whether or not KDE should be configurable, let's just put in an option to make it configurable or not!\n\nI hope you see the irony in this suggestion :-)\n\nMyself, I think that KDE is too configurable already.  However, there is no going back.  I think KDE should continue in the direction it is going, and continue to add options.  When it gets too complex, a new project may decide to simplify KDE and pare it down to the bare minimum.  With all the great code that is being written for KDE, such a simplified system would be able to include lots of cool functionality but focus on simplifying the interface as much as possible.  Wouldn't that be cool?"
    author: "not me"
  - subject: "Do NOT do both !"
    date: 2003-02-21
    body: "Please, do not make different user levels!\nA good interface will allow a user to quickly learn the interface by heart so that the user doesn't have to think about _how_ to do something, only about _what_ to do. It sould be like driving a car, you never have to think about how to stop, turn or accelerate once you are an experienced driver.\n\nIf you add different user levels you get to the situation where, once you have learned how everything works it all suddenly changes and you have to start all over. Instead you should be able to continually refine your skills (until you become Schumacher) according to the car analogy). The user levels idea has also been discussed and rejected by the KDE usability project:\nhttp://usability.kde.org/activity/recent/userlevels.php\n\nPick up a few books on UI (Nielsen, Norman and Raskin comes to mind) to find out more.\n\n\n"
    author: "Anon"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "This would be the right way!"
    author: "Dirk"
  - subject: "Re: do both !"
    date: 2003-02-21
    body: "I totally agree on the point of an \"expert mode\", I have thougt about it before.\nYou should be able to cross a box in the Control Center that activates the \"simple mode\", and then after logging in again all advanced options should be gone, for example also Konquerors \"View DOM tree\" or \"Use index.html\" as no novice user will ever use this. Another idea is to hide \"Shred file\" when viewing a webpage, and more like this.\nIn the Control Center, this can hide the advanced anti-alias settings, the advanced focus policy (you only then have a checkbox \"Focus follows mouse\"), additionally to hiding advanced modules like editing of mime types and socket timeouts.\nAnd if a user really feels it needs to configure this-or-that, he will probably have the knowledge to uncheck \"Expert Mode\" and press Apply."
    author: "Daan"
  - subject: "Totally agree"
    date: 2003-02-20
    body: "I totally agree with this.\n\nI want configuration everywhere :)\n\nWhen I look at the Windows exemple. A big part of the persons I know and who has used Windows for years just used the default options of Windows. So when they need to work on a DOS shell the write all the path and all the name of the commands. I show them the hidden option in regedit for getting auto-completion  and they say \"Woooow !! I don't know this option. It could have save me much time in the past\". After that they all want to get more tweak on their system and modified regedit database... but on day a bad option has crashed their system.\nThe conclusion is that :\n- Hidden options can be usefuls for everyone so don't put all in a regedit-like database ( gconf-editor,...),\n- If you create a regedit like software, take much time to know if this option has its place on it, and protect the way to put bad data via this software\n- Prefer the way KDE do now : \"Advanced options\" button on the configuration windows,\n- Don't take users for dummies or you will get dummies users :)\n- ...\n\nPlease, don't follow the Gnome Guideline. Now Gnome2 unfortunnately sucks a lot regarding to Gnome1.4 :("
    author: "Shift"
  - subject: "Re: Totally agree"
    date: 2003-02-20
    body: "Full ACK !!!!!"
    author: "star-flight"
  - subject: "Re: Totally agree"
    date: 2003-02-21
    body: "Firstly, please, GConf is NOT like Regedit. Regedit is a mass af unintelligible keys. GConf is very understandable and wuite verbose. Stuff like, Nautilus theme, etc.\n\nThe point is it is a bad idea to make a user interface too configurable. Make it good so that configuring it is minor. Things that should be configured are font sizes and maybe colors and in the extreme Window decorations. OS X is not the most configurable GUI in the world but people love it. Follow that instead of the more is more thing. It only creates an unnecessarily complex DE without improving the experience.\n\nIf you don't like the Gnome paradigm, don't flame it and diss it. Get KDE and be happy with it. Some of us keep both and like different things about the 2."
    author: "Maynard"
  - subject: "Re: Totally agree"
    date: 2003-02-21
    body: "I agree fully. GConf is nothing like the Windows registry. It may be centralised (which is a good thing), but it is fully text-based (XML). It is not very different from the KDE config file system, but it is far more structured and consistent. Because it is centralised, it is far easier to manage settings, either by hand (via a humble text editor), or via a config tool like the gconf-editor.\n\nThe beauty of the GNOME system is that you are able to put only the most important options in the graphical prefs menus, without limiting the overall configurability of the app. This should be sufficient for most users. If you want to tinker, you can access the GConf database directly. This isn't very difficult to do (it's far easier than editing the Windows Registry), and hopefully it will discourage complete newbies from wrecking their systems (people who deliberately launch the gconf-editor should know what they are getting themselves into).\n\nI am not saying that GNOME should be inflexible and spartan. I am simply saying that there needs to be a balance between this and a bewildering array of options."
    author: "Yama"
  - subject: "Who is the target?"
    date: 2003-02-20
    body: "I agree with mosfet. \n\nThe target for removing options is some phb somewhere who wants to dictate what his users do. Let them use the kiosk tools then. Leave the options to those who want them.\n\nOr some \"usability study\" that takes a new user, who is confused by the options. Is the answer to take options away? Or set a reasonable default, let the commonly changed options be quick and easy to access, while leaving the fine tuning in an expert section.\n\nIt may be that the organisation of the options could be better. Bubble help (is that what it's called?) on the options and categories would be nice.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Who is the target?"
    date: 2003-02-20
    body: "<i>The target for removing options is some phb somewhere who wants to dictate what his users do. Let them use the kiosk tools then. Leave the options to those who want them.</i>\n\nNo, too many options also hurt the users who search for an option. Obviously it is easier to find an option if there are only 10, than to find it among 10 million other options... the categorization can help somewhat, but where do you, for example, find the 'smart window open' policy in Konqueror? Behaviour or JavaScript?\n \n"
    author: "Tim Jansen"
  - subject: "Re: Who is the target?"
    date: 2003-02-21
    body: "Your point is very astute wrt Behavio(u)r. IMHO it shouldn't be in the Konqueror dialog box at all. Almost anything could fit under that rubric."
    author: "Antiphon"
  - subject: "Re: Who is the target?"
    date: 2003-02-21
    body: ">where do you, for example, find the 'smart window open' policy in Konqueror? Behaviour or JavaScript?\n \nI've never had to look for it. The default is reasonable. Just a minute, that isn't true. I had the occasion to frequent a site (which was necessary for my work) that required pop-up windows enabled. So I had to search for the configuration option. It wasn't too hard to find.\n\nThe original article that mosfet was responding to made the point that many options are work-arounds for faulty code, ie. disable animations to make computer usable.\n\nHow many of the kde configuration options are of that nature?\n\nHow many speed complaints would disappear if the default configuration didn't have all the animations and stuff that take up cpu cycles?\n\nDerek "
    author: "Derek Kite"
  - subject: "Re: Who is the target?"
    date: 2003-02-21
    body: "> Obviously it is easier to find an option if there are only 10, than to find it among 10 million other options...\n\n\nAnd what if you need the option that was removed? Will be much more difficult to find it :-)"
    author: "anonymous"
  - subject: "Re: Who is the target?"
    date: 2003-02-22
    body: "Exactly so. This is exactly what many people forget. =)\nJust like Mosfet says in his second article: not the amount of settings matters, the organization of them does.\nAnyway most programs have context sensitive settings which already reduce the amount of settings to search through drastically, so what's the matter?"
    author: "Datschge"
  - subject: "Expert mode"
    date: 2003-02-20
    body: "I agree with the general trend expressed so far:  two much information gets to be confusing to the average user. A clean solution for KDE would be to have an overall \"expert mode\" that you can switch on or off. And add to the kdelibs a flag to let developers determine what menues/options are general and which are \"advanced\". A user running in \"normal mode\" only sees a few basic options/menues, and the \"expert mode\" user sees everything.\n\nThe other crucial issue is to set sensible DEFAULT users. Most computer users NEVER ever tweak. \n\nCheers !"
    author: "NewMandrakeUser"
  - subject: "Re: Expert mode"
    date: 2003-02-20
    body: "Crucial issues is good DEFAULTs, yes.\n\nBut hide completely options unless in overall Expert mode, no.  Leave it all there but organize it so that the non-expert options are first and the expert options are in Advanced tab."
    author: "ac"
  - subject: "Re: Expert mode"
    date: 2003-02-22
    body: "KControl IS the expert mode already, use context sensitive configuration available in many programs as well as on the desktop and the kicker and you have a limited, context related set of settings.\n\nAnd it's not like KControl is jumping into the face of those who never ever tweak. =P"
    author: "Datschge"
  - subject: "Re: Expert mode"
    date: 2004-02-03
    body: "hi there\ni m looking for a linux programmer for my project\nthat project is basically a KDE program\n\nMy client wants to have aprogarm with the same features\nlike kDE but with my own skin\nso that will be fairly easy for anyone with KDE understanding\njust make a few changes in KDE regarding the skin and look\nand makeover and my project will be ready\ni shall provide the graphics and u have to install those\ngraphics in the kde to make it a look that i m upto.\n\ncan anybodyt help me with that\nplease quote me about that.\n\nthansk\n"
    author: "PHP Lamp"
  - subject: "The \"one true way\""
    date: 2003-02-20
    body: "I believe in \"one true way\"! IMO most usability issues have a one true way that will make things usable for the majority of people. If KDE could come set with defaults that make it usable for the majority of users than it will be a fabulous desktop environment.\n\nI also agree with Mosfet that one of KDE's strengths is it's configurability. HOWEVER! Average users find the sheer number of options in KControl opressive and intimidating. Even I was a little scared at first and I still find it a little hard to find options sometimes, I know I found them once, but I can't for he life of me find them now!\n\nMy point is, perhaps a good solution is to have a slightly less full COPY of the controlcenter (as well as the current one) that has the options an average user wants to change, basically an equivalent of dekstop properties in Windows with a few more sections such as accessability and region settings. The tabs in controlcenter-lite should be EXACTLY the same as the ones in controlcenter, there's just less of them. If they're not the same then you're asking people to learn two different configuration systems and that is not a usable solution.\n\nSorry if this has been suggested before. I know people are working on a registry editor type system for advanced options for KDE, but having read Mosfet's essay I realise I don't agree with that line of thinking. Nobody wants to ruin KDE, we just want to make it more usable for the majority. Maybe what I suggested would work, what do others think?\n\n\"You can productively use KDE without ever knowing about how you can configure, for example, menu drop shadows or window decorations.\" - This is true and why we should keep the control center full of options, most people will probably never stray there and be happy to use an environment that is usable by default. But, everyone wants to change their Windows style every now and again."
    author: "MxCl"
  - subject: "Re: The \"one true way\""
    date: 2003-02-20
    body: ">I also agree with Mosfet that one of KDE's strengths is it's configurability. >HOWEVER! Average users find the sheer number of options in KControl opressive >and intimidating. Even I was a little scared at first and I still find it a >little hard to find options sometimes, I know I found them once, but I can't >for he life of me find them now!\n\nThe solution is to beef up the keywords and the search panel. In Windows XP, I find searching for options works quite well in their help system. In an ideal world, I would type proxy in the search tab and get a first item defining what is a proxy and several items for modifying your proxy. In KDE, you have to click on the proxy module and then on help to get a basic definition of what is proxy. All the work of the documenter is more or less hidden. \n\nThere is much room for improvement in KDE to offer to the user information in a hierarchical way : General to particular, simple to complex, short to long. It would be nice if some developper work on that for 3.2 with the help of the usability team."
    author: "Charles de Miramon"
  - subject: "Re: The \"one true way\""
    date: 2003-02-20
    body: "Well, it'd help if people actually used the search tab too -- for some reason it seems like people always seem to not notice it, while it can help find the relevant things quickly much of the time. "
    author: "SadEagle"
  - subject: "Re: The \"one true way\""
    date: 2003-02-21
    body: "1. You need to know the search panel. Most people do not\n2. You need to know search terms. A newbie who does not know the right terminology will not find it."
    author: "AC"
  - subject: "Re: The \"one true way\""
    date: 2003-02-20
    body: "> I believe in \"one true way\"!\n\nIf there is a \"one true way\", and KDE miraculously find out what is the \"one true way\" for everything, the \"one true way\" will just be the default.\n\nSo the defaults are perfect and nobody will ever care about how \"bloated\" the configuration is because nobody will ever see it.\n\nSo why can't all the self-proclaimed \"usability - dumb down everything - experts\" go away and bother somebody else?\n\n"
    author: "Roland"
  - subject: "Re: The \"one true way\""
    date: 2003-02-21
    body: "Well done for not reading my post and reiterating my points!\n\n> So the defaults are perfect and nobody will ever care about how \"bloated\" the\n> configuration is because nobody will ever see it.\n\nAnd nobody will ever change their wallpaper then? Be intelligent about this.\n\n> So why can't all the self-proclaimed \"usability - dumb down everything -\n> experts\" go away and bother somebody else?\n\nWe could do without opinions like that on the project. I want the project to succeed, don't you?"
    author: "MxCl"
  - subject: "Re: The \"one true way\""
    date: 2003-02-22
    body: "Right click on your desktop and choose configure, isn't that easy enough?"
    author: "Datschge"
  - subject: "more options for the masses"
    date: 2003-02-20
    body: "I totally agree with mosfet, \nconfigurability is what makes using kde fun, because if you're disliking a certain aspect of kde you can change it. It's like really being in charge of your session. "
    author: "doujin"
  - subject: "Few things in KDE I'd like to be configurable"
    date: 2003-02-20
    body: "(they are not right now)\n\n1) I'd prefer, for example, to have to possibility to choose in what folder\nthe hot plugable devices/removable media/etc links should be created. I\ndon not like having desktop full of icons, but I'd love to provide my\nusers a kind of \"My computer\". By default KDE create such things in\n/usr/share/apps/kdesktop/Desktop (on my Mandrake), but I would be happy to\nbe able to choose an alternate directory.\n\n2) I'd prefer to be able to have a plug-in mechanism for file folder\nviews. Or at least ot be able to write my own templates for some folders,\nso that folder can be presented in a way like \"My Computer\" in XP does.\n\n3) I'd like to be more \"standard\" icons for folders, like \"bin folder\" icon, \"photos folder icon\", \"development\", etc. They are in different icon themes, but they are inconsistent accross different icon themes.\n\n4) I'ld like to have badges for folders or files like in Nautilus.\n\n5) I'd like that \"Home URL\" be different between filemanagement profile and web profile. In file namagement I should get my home, in web I should be my home page. Not quite the same thing.\n\nThx"
    author: "Socrate"
  - subject: "Re: Few things in KDE I'd like to be configurable"
    date: 2003-02-20
    body: "> '2) I'd prefer to be able to have a plug-in mechanism for file folder\nviews.\n\nthat exists :)"
    author: "fault"
  - subject: "Re: Few things in KDE I'd like to be configurable"
    date: 2003-02-20
    body: "There alread is a plugin mechanism for file forder views -- that's how the directory views in Konqueror work (and how the Cervisia directory works) since forever; these are just KParts for the inode/directory mimetype.\n"
    author: "SadEagle"
  - subject: "Re: Few things in KDE I'd like to be configurable"
    date: 2003-02-24
    body: "Well, perhaps you're looking for an \"Emblems\" feature, instead of icons for each folder type (becomes unwieldly to make icons for each occasion and type of contents)"
    author: "Rudd-O DragonFear"
  - subject: "it isnt configurabilty that makes kde hard to use"
    date: 2003-02-20
    body: "I dont think its the number of options that is the problem with the configurability of kde, its the interfaces that are used that makes it difficult and confusing to use.  Having konqueror configuration options in the central configuration panel, scrollable side bars, etc make it difficult for me to use, even though I have grown somewhat accustomed to the use of it.  I also find that they dont try to make the options on the sidebars of the configuration menus from easiest and most commonly configured to most complicated.  Everything just seems here and there and until that is changed I think there will continue to be problems, and lessening the options a user has wont eliminate them."
    author: "bonito"
  - subject: "Minimalists need configurability!"
    date: 2003-02-20
    body: "Let GNOME go whatever way they've chosen, but one of the reasons why I ended up using great KDE instead of GNOME is THAT I CAN SWITCH OFF FLASHY STUFF I DON'T LIKE. I think KDE is well ahead in terms of user-friendlyness or usability, it allows newbies not to bother reading all the configurational stuff and start using KDE right away and gives them a chance to figure out what feature they might need and what annoys them. So does KDE give the expert the opportunity to configure everything in a way that performance ( i mean the user's ) is at max.\nIf KDE was not providing this possibility, I'd probably use wm2 ;)\n\nHonestly, I think this is the one and only way to follow! Minimalism does not mean to restrict!! KDE developers can be proud of the path they chosen so far!"
    author: "Udo"
  - subject: "flashy stuff in gnome?"
    date: 2003-02-21
    body: "you\u00b4ve got to be kidding me. the reason I use gnome is that I don\u00b4t think its GUI is as intrustive and \"fancy\""
    author: "mithril"
  - subject: "average users"
    date: 2003-02-20
    body: "Whenever something like this gets posted, everybody talks about what \"average users\" want, and how \"average users\" work.  If you look at these statements, most of them are really condescending, implying that this mythical \"average user\" is a drooling moron.  Come on, you guys!  The average KDE user is not an idiot. \n\nEven if it were true that the average user was such a computer illiterate, why on *Earth* would we want to target our awesome hot-rod desktop to such a demographic?  Let's keep making a desktop that *we* like to use; otherwise, what's the point?\n\nI am not at all opposed to making KDE more accessible to non-experts; I want aunt Tilly to use KDE as much as the next guy.  However, I don't buy that aunt Tilly is the average user.  \n\nSo, yes, maybe having an optional non-expert mode that hides complexity is a good idea, if less experienced users feel overwhelmed.  Let's not lose what makes KDE great in the process, however.  "
    author: "LMCBoy"
  - subject: "Re: average users"
    date: 2003-02-20
    body: "There is a bigger problem, too, I think: \n\nan average user doesn't exist.\n"
    author: "SadEagle"
  - subject: "Usability Reports"
    date: 2003-02-21
    body: "Read some usability reports like this one:\n\nhttp://developer.gnome.org/projects/gup/ut1_report/customization_tasks.html\n\nIt's not that average users are stupid, it's just when you're confronted with something new it's very easy to fint it hard to do things.\n\nUsability is about making something instantly usable for the majority, we're all experienced with KDE and consequently find it easy to use. Don't forget that."
    author: "MxCl"
  - subject: "Please, NO"
    date: 2003-02-20
    body: "I was a hardcore GNOME user until the release of 2.0.  What a disappointment!  ALL of my favorite configuration options were gone.  I would hate to see the same thing happen to KDE.\n\nOn another note; why the big fuss?  GNOME is still a good desktop for people who like things simple, KDE is a great desktop for people who've been around the block a few times and know exactly what they want.  Since you can still use GNOME apps within KDE and vice-versa, I don't see any reason why the two projects can't coexist, serving different kinds of users."
    author: "Stunji"
  - subject: "Re: Please, NO"
    date: 2003-02-20
    body: "> GNOME is still a good desktop for people who like things simple,\n> KDE is a great desktop for people who've been around the block\n> a few times and know exactly what they want. \n\nQuite the opposite. KDE is used chiefly by newbies and chosen by popular distributions because it appeals to newbies. It looks good (or flashy depending on your taste), it looks powerful (because of all the options it offers) and yet generally follows the conventions set by that other popular desktop OS.\n\nGnome is actually trying hard to create something that is usable, powerful and original. Few people understand that."
    author: "Grumpy"
  - subject: "Re: Please, NO"
    date: 2003-02-20
    body: "KDE is working on being powerful and original and usable.  Few trolls understand that."
    author: "anon"
  - subject: "Re: Please, NO"
    date: 2003-02-20
    body: "Don't make it a KDE vs. Gnome issue. This is not what I meant."
    author: "Grumpy"
  - subject: "Re: Please, NO"
    date: 2003-02-22
    body: "You don't even notice that he just reworded your last sentence..."
    author: "Datschge"
  - subject: "Re: Please, NO"
    date: 2003-02-20
    body: "> KDE is used chiefly by newbies [...]\n\nYes, sure. That must be why Linus uses it. Let me cite:\n\n\"... while running my normal set of applications (X, KDE etc).\"  -- Linus TORVALDS\n\nm.  ;-)"
    author: "Melchior FRANZ"
  - subject: "Another Thing"
    date: 2003-02-20
    body: "RedHat tried to make KDE like GNOME in RedHat 8.0.  Total disaster.\n\nIts STUPID to make both KDE and GNOME the same. Whats the POINT?\n"
    author: "ac"
  - subject: "Re: Another Thing"
    date: 2003-02-21
    body: "Correction. Redhat made KDE and GNOME look the same. If you really look at it, they actually made GNOME mroe like KDE. (The desktop switchers, the panel at the bottom. That is definitely a KDE arrangement)"
    author: "Maynard"
  - subject: "Re: Another Thing"
    date: 2003-02-21
    body: "Correction. Desktop switchers and panel at the bottom are neither a KDE or Gnome arrangement., happily ! You arrange as you want...\nWhat is better in KDE than Gnome is some parameters like \"Maximise when double clicking in the title bar\"... Here, for me, it makes a difference...\nAs for the Redhat desktop, I don't see where is a KDE arrangement without Konqueror, Kmail and some other things... Although you may arrange many things..."
    author: "Alain"
  - subject: "Re: Another Thing"
    date: 2003-02-21
    body: "Correction. \"Maximise when double clicking in the title bar\" is very easy to _configure_ in Gnome 2.2\n\nSo both KDE and Gnome are good, joy! :)\n"
    author: "bleh"
  - subject: "Configurability is OK if it is documented"
    date: 2003-02-20
    body: "During the recent Solutions Linux show, I heard no user complaining about over-configurability of KDE. I think most of them use Mandrake's default and are happy with it.\n\nAll in all, you don't change the configuration of your computer very often and if you have to spend a little time finding the right option it is a minor nuisance.\n\nOn the other hand, a lot of options are not documented. For example (I'm talking here about 3.0 but I think it has not changed in 3.1), if you look at the spellchecking configuration module, you can change your encoding but in the help page there is no explanation of what is encoding and how to make sense of acronyms like ISO-8859-1. \n\nMaybe for 3.2, we could name a dictator to apply a simple rule : if your configuration option is not documented and explained in a language devoid of jargon, it will be obliterated...\n\n\n\n\n"
    author: "Charles de Miramon"
  - subject: "Re: Configurability is OK if it is documented"
    date: 2003-02-20
    body: "Good rule.  :)"
    author: "ac"
  - subject: "Very close to what I was planning to say"
    date: 2003-02-20
    body: "The over-configurabiity problem stems from lack of adequate documentation. If the user reads JUST the name and figures she doesn't want to change it, no meaningful amount of options would be a problem. If she indeed wants to change it, a document reading requirement is not too much to ask. Ofcourse the document should exist and be understandable.\n\nAlso, note \"meaningful amount of options\" phrase. I don't think it is a problem now, but too many options may be a problem in the future. One should be able to read all kde options in less than half an hour or so. Out of box infinite configurability shouldn't be a goal.\n"
    author: "goosnargh"
  - subject: "Re: Configurability is OK if it is documented"
    date: 2003-02-20
    body: "> During the recent Solutions Linux show...\n\nRight. Motivated Linux users, early adopters, hackers, CS students have not problems using KDE.\n\nWhat about the rest?"
    author: "Grumpy"
  - subject: "Not only geeks are using KDE"
    date: 2003-02-21
    body: "No, they are also senior citizens in France that use KDE. If they come to an IT show, it means that they are interested in computers and trying new things but they are certainly not Unix experts.\n\n"
    author: "Charles de Miramon"
  - subject: "A good UI..."
    date: 2003-02-20
    body: "Do we have to decide between a comfortable and easy-to-use interface\nand lots of functions? I don't think so. For an absolute beginner\nlike my Mum it doesn't really matter how complicated i.e KControl is\nor what menu options show up if you right click on the taskbar.\nShe does not click there. She does not start KControl. She does not\neven know KControl exists. And if she knew it, she would not change\nsettings labeled \"Advanced - Internal Server Settings - DHCP host\" or\nsomething like that. She is a novice, but she can *read*. It would\nbe confusing if all those options are forced on the user. But they aren't.\nSo: There might be a discussion about simplifying or restructuring some\ndialog windows. There might be a discussion about preventing more advanced\npreferences from being changed accidentally. Even additional help texts\nin KControl could be possible. But a discussion about whether\nall those functions should exist is rather ridiculous. KWord can do more\nthan KWrite. Do we have to reduce the functionality because KWord is\ntoo complicated? No. And: Even novice users sometimes have fun finding out\nor learning about some \"hidden\" function. My mum now changes the\nbackground herself after I showed her KControl. And furthermore the ability\nto customize can even help novice users: For example, I put\nlarger icons in the taskbar and put the taskbar to the top, because\nmy Mum (don't ask me why ;-) ) can see it better there. Well she starts her\ncomputer, opens KWord, writes something, prints it, shuts KDE down. She\nhas learned it and she wouldn't even notice if some configuration option\nis missing. A more advanced user might be interested in changing i.e.\nthe system sounds. But such a person knows probably that he has to \"research\"\na bit to find out how it works. If the function is not buggy, well documented,\nand not too hidden - it is fair that he takes some minutes to find out\nabout all the options available concering that function. Perhaps she/he\ndiscovers even some new aspect/option which he likes. And the professional\nuser - well we don't have to talk about that -  he knows about all the\nbells and whistles and will adjust everything to his liking.\nSo I think KDE's way up to now is also the way to go in the future:\nMaximum functions - Maximum simplicity\nThis is no contradiction.\nIt's possible. It works. And it's even A Good Thing (TM) to do it.\nPlease keep innovations and customizability in KDE in the future.\nEveryone probably uses only a small portion of it. But everyone in\na different way. And that's what it's all about, isn't it?\n(Sorry folks for the long posting, but I just felt this has to be said...)\n\n"
    author: "Martin"
  - subject: "Re: A good UI..."
    date: 2003-02-20
    body: "BTW, I just discovered: KControl has a Search Tab. If you want\nto change the background -  search for \"background\" and there you are.\nIt's as easy as that. Haven't seen that before. Perhaps there should\nbe a user-friendly Welcome page with pictures and simple informations\ninstead of the system information when you start KControl. This can lead to\nthe most import options just like on a webpage. For example:\n\nWelcome to KControl!\n\nHave you ever got bored of your wallpaper? Just change it! Find out\nhow it works _here_ (Link)\n\n(bla, bla)\n\nPerhaps a tip of the day. Well you get the idea. And please don't\nmake an Expert and Normal mode. This only hides the fact that the\nExpert mode has such a horrible UI that this is necessary. And this\nis even bad for the \"experts\"."
    author: "Martin"
  - subject: "Re: A good UI..."
    date: 2003-02-24
    body: "To me the issue is the following .. If the options were arranged intelligently, there would be no need for a search option :)\n\nMy solution follows what some people have said already:\n\n1. Configure all options for one particular item in one place.\n2. Place most often changed options first, then more obscure options second etc.\n3. Use clear English devoid of as much jargon as possible when labelling the options. To me, even -- Configure the Desktop sound 'dangerous'. Ofcourse, sometimes it is impossible to remove the jargon, but where possible, make it clearly understandable.\n\nFor the record, a centralised XML based config system + editor doesn't sound like a bad idea. I wish that could be implemented system wide throughout Linux.\nNo more cryptic learning of old style txt file formats in the /etc directory. That would be excellent.\n\nThank you."
    author: "3.1rOXtHEcASBAH"
  - subject: "Yes, More *IS* better!"
    date: 2003-02-20
    body: "I couldn't agree more on this topic with Mosfet.\n\nWhat we need is good defaults. The below-average complete moron (yes, I do consider somebody who gets \"confused\" with the KDE-control center a moron. And below average, not average at all.) will just use the default anyway and will never even try to customize it anyway.\n\nAnd as far as I can see, KDE already has good defaults. There might be some discussion about this or that detail, but in general it's not bad.\n\nThe whole \"KDE/Linux needs to copy Windows\" misses completely the point. Technically, KDE/Linux is already much better than Windows, even as a GUI. Where KDE/Linux sucks is marketing and running Win32 software.\n\nWe need computer maker preinstalling KDE/Linux, we need advertizing - and we need to support the huge Win32-software library.\n\nIf you really don't need what to do to attract more people, just integrate Wine into KDE. That will do 10 times more to KDE's usage than any dumbing down. (Actually, dumbing down would lead to losing many users, IMO)\n\nA well integrated Wine - one which creates the entries in the start-menu correctly, one that works reasonably well, maybe even one that would use native KDE-widgets and even more importantly the tray - that would really do a lot for KDE/Linux' acceptance.\n"
    author: "Roland"
  - subject: "Configuration is the way to go..."
    date: 2003-02-20
    body: "I think the best idea I've seen posted on this thus far is for a check-box or option somewhere to allow for either advanced or basic configuration options.  The problem I have with having things such as 'Advanced' option tabs is that it makes it more work for those who actually want to configure.  Instead of having all of the options in front of you, your forced to change a few options, the switch tabs to switch more.  With the checkbox option, you have the option of having everything in front of you or not.  One quick setting, and you're set whether you want the advanced configuration or not.\n\nThat's all I have to say."
    author: "Jesusfish"
  - subject: "Re: Configuration is the way to go..."
    date: 2003-02-21
    body: "Try implementing that! Nightmare.\n\nHow do you combine two sepearate tabs into one and still make it all legible! You could stack everything vertically but that's just the same as tabbing it."
    author: "MxCl"
  - subject: "Organization of configuration options"
    date: 2003-02-20
    body: "I also like that KDE offers that many configuration options. However, I'm not completely happy with how things are organized right now. (although KDE 3.1 has made a step in the right direction.) What about the following principles for dialog design:\n\n- All configuration options relating to a particular subject should be accessible in one place. (I don't like to look in different places to find e.g. a particular network setting.)\n\n- The most common configuration options should go first. Less frequently used settings go on an \"Advanced...\" window / tab.\n\n- Avoid overloaded pages / menus. If dialogs / menus are too crowded it's very time-consuming to find the option one is interested in. Rather, I'd like to see the options organized in a hierarchical manner (sub-menus, tabbed dialogs).\n\nEspecially the \"Look'n Feel\" options are quite confusing as they are now (themes, styles, window decorations, icon themes...). I'd like a theme manager that offers a few basic themes (e.g. Liquid, Keramik...) that include ALL look'n feel settings. Customizing the individual design elements should be possible through theme manager sub-items or tabbed configuration pages.\n"
    author: "Lars Kastrup"
  - subject: "More, more, more"
    date: 2003-02-20
    body: "The only thing better than configurability is more configurability.\n\nThe first thing I do when I use KDE on a new account is go into kcontrol and start changing things like crazy.  I love KDE to death, in large part because it lets me set up my desktop exactly how I like it (well, almost; I have to hack the source a bit).  I like having a Motif like window manager with a close button on the left, and minimize and maximize on the right.  I like my \"save\" dialog to have the buttons _I_ want in the speedbar.\n\nIf need be, kcontrol can have a \"dumb user\" and a \"smart user\" option.  I know that no one agrees on what is essential, but I figure if someone has enough gumption to change an option, they shouldn't be so stupid that they can't ignore stuff that doesn't apply to them.  I don't use, say, the address book, but that doesn't mean I get confused when I see that it's an option in the control center."
    author: "KDE User"
  - subject: "Re: More, more, more"
    date: 2003-02-21
    body: "amen to that..."
    author: "ne..."
  - subject: "Configurability is mandatory"
    date: 2003-02-20
    body: "You have to allow configurability in a desktop that is designed for slow computers, fast computers, Linux, Solaris, HP-UX, AIX, Tru64, FreeBSD, OpenBSD, and more.  The desktop, in our sense of the word, also happens to include a plethora of applications.  These need configuration options too.  In Windows, you have your configuration options in each application, custom designed.  And quite honestly, windows is not all that easy to configure.  it's just that everyone uses it so everyone knows where to go.\n\nOn MacOS X, I must say, I wish there were more configuration options.  However it does have a comparable number to KDE.\n\nAs American contact for KDE, I have only received email from people saying \"please keep KDE very configurable; I don't like what the new Gnome setup does to configurability\".\n\nI think the important thing to do here is put the main and common configuration options \"up front\", in the first tab or near the centre or top of the configuration module.  Hunting for options is bad, but making few options available so that the hunting is easier is the wrong approach.  For instance, have a look at what some people consider the \"mess\" of the KDE cryptography control module.  How many times have you ever had to go beyond the first tab, or even the first few settings?   I'm guessing that most people never go past that first tab at all.  However, I do often get requests for even more configuration options!  The ones that are there now do get used, and it would be horrible to have to deprive people of that functionality.\n\nOne thing I am definitely not against is having option levels - Basic, Advanced, whatever.  I think this could be easily automated in KControl so that modules or tabs would only show up if the proper configurability level is set.  That being said, I don't see it necessary and have no intentions of implementing this personally. :)\n"
    author: "George Staikos"
  - subject: "Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "Mosfet is dead wrong on this one.\n\n> As a matter of fact, many people are now setting up KDE for things like their families\n> and rarely do they complain about things like too many configuration options.\n\nWould we hear their complaints? Provided that they could articulate their confusion with the interface, to whom would they report it? To KDE hackers who would explain to them that they are not trying hard enough and that they do not understand the \"power\" of KDE? Preposterous!\n\n> The only people who like less configuration options are those who believe their\n> way of doing thing is \"the right way\". Everyone else is, well, screwed.\n\nNo. They are those who see people using computers everyday, who see people blaming themselves for mistakes they haven't made but that some sloppy or uneducated designer/programmer has made. They are those who see people overwhelmed by complicated computer lingo and an impossibly large set of options to understand. They are those who see people giving up on their task because arrogant computer jerks have decided that if they could understand something one way everybody else should too.\n\nCleaning up the ui to reduce the number of options offered up-front doesn't mean that every one else is \"screwed.\" It means that the motivated few, the hackers and the wannabes, can figure out for themselves how to tweak KDE to look or act exactly the way the want. Regular users can move on and actually use KDE.\n\n> KDE does not make such presumptions. \n\nKDE makes plenty of presumptions. Not the least of which is that it should offer a graphical interface to almost every single configurable bit in its system.\n\n> KDE is about making the Linux desktop operate how *you* want it to work,\n> not how anyone else feels you should work.\n\nThis doesn't make any sense at all. KDE is designed by people who make conscious decisions about what should or should not be part of KDE or what is or isn't worth being implemented. So it is about how someone else feels you should work. The huge problem here is that most of these decisions and most of the assumptions around those decisions are taken by people who are not educated enough on the subject of usability (but who are willing to learn) or by people, like Mosfet, who refuse to believe that the vast majority of computer users cannot deal with the level of complexity he is able to deal with. That's good for Mosfet but bad for KDE!\n\nIf we want KDE to succeed, we must stop thinking like macho computer hackers or immature wannabes. Mosfet's editorial is an unfortunate example of the kind of attitude that's dragging KDE down on the usability front. Making KDE simpler to use is nothing to be afraid of. It's a good thing and big egos shouldn't stand in the way of it. Embrace simplicity!"
    author: "Grumpy"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "Normally, I don't respond to trolls, but anyway:\n\n\"Would we hear their complaints? Provided that they could articulate their confusion with the interface, to whom would they report it? To KDE hackers who would explain to them that they are not trying hard enough and that they do not understand the \"power\" of KDE? Preposterous!\"\n\nIf you would actually use KDE you would have noticed that EVERY KDE appliaction has a bugreport option in the help menu. So we would see those complaints.\n\nThe only ones complaining are KDE-bashers, not real users.\n\n\"Cleaning up the ui to reduce the number of options offered up-front doesn't mean that every one else is \"screwed.\" It means that the motivated few, the hackers and the wannabes, can figure out for themselves how to tweak KDE to look or act exactly the way the want. Regular users can move on and actually use KDE.\"\n\nWhat pile of nonsense.\n\nI'm a programmer. But I sure as hell don't want to dig into configfiles for every feature I want (removed). How would you even know that you can remove/add a feature if there is no option for it?\n \n\"KDE makes plenty of presumptions. Not the least of which is that it should offer a graphical interface to almost every single configurable bit in its system.\"\n\nWow, the KDE/Linux-bashers normally complain (wrongly) that there isn't a GUI for every config option, now the bashers complain that there is.\n\nNo way KDE-developers do it will be the right way. The bashers/trolls will NEVER stop bashing. The best thing to do is to ignore them.\n\n\"KDE is designed by people who make conscious decisions about what should or should not be part of KDE or what is or isn't worth being implemented. So it is about how someone else feels you should work.\"\n\nWrong, because there are so many people working on KDE, almost every even semi-popular feature gets implemented. (And that's a good thing)\n\n\"The huge problem here is that most of these decisions and most of the assumptions around those decisions are taken by people who are not educated enough on the subject of usability (but who are willing to learn) \"\n\nSo summarized, we shall put \"Grumpy\" in charge of all the design decisions. Or who else? Then we have one perfect default. To make sure nobody ever changes the default, we remove all configuration options, then everybody will be happy.\n\nIf you never want to change anything, why not just remove kcontrol at all?\n\n\"If we want KDE to succeed, we must stop thinking like macho computer hackers or immature wannabes.\"\n\nIn terms of making a damn fine desktop, KDE has already succeeded.\n\nIn terms of market penetration, most people use whatever is preloaded on their computers. End of story.\n\n\"Making KDE simpler to use is nothing to be afraid of.\"\n\nRemoving configuration functionality isn't making KDE simpler to use. To the contrary: All are forced to use the supposed-one-size-fits-all defaults.\n\nAgain: If the default is so great, why are you complaining? If the default is so great there is no need to every fire up kcontrol and change anything, so any bloat wouldn't matter at all.\n\n"
    author: "Roland"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "> If you would actually use KDE you would have noticed that EVERY KDE appliaction \n> has a bugreport option in the help menu. So we would see those complaints.\n\nI do use KDE and I'm sad to see that, because I believe KDE could be better or different, you assume that I'm trolling.\n\nI write what I write because I do care about KDE.\n\nNow, do you seriously believe that regular KDE users, non techies, your grandma, people who might use KDE in an office environment and not know what it is, who writes it, etc..., do you seriously believe that these people will file bug reports?\n\n> almost every even semi-popular feature gets implemented\n\nBut popular doesn't mean usable.\n\n> In terms of making a damn fine desktop, KDE has already succeeded.\n\nSure. KDE is a damn fine desktop now. But can it grow and be comfortably used by non-techies? That's the question.\n "
    author: "Grumpy"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "\"I do use KDE and I'm sad to see that, because I believe KDE could be better or different, you assume that I'm trolling.\"\n\nA few comments above you say that GNOME is so great. So what do you use? Don't say both, because that's a) nonsense and b) contradicting with your opinion that one-size-fits-all.\n\n\"Now, do you seriously believe that regular KDE users, non techies, your grandma, people who might use KDE in an office environment and not know what it is, who writes it, etc..., do you seriously believe that these people will file bug reports?\"\n\nYes. Depends on how pressing the problem is and how motivated the reporter is. But skill-level is not the obscacle, anybody can write a bugreport in KDE if he wants to. Of course many people are simply not interested or think their bugreports won't get noticed, but that's not KDE's fault.\n\n\"But popular doesn't mean usable.\"\n\nWell, that's your problem.\n\nDespite requesting KDE-designers to \"get off their high horse\" and dumb down everything, you are really the most arrogant person I've met for quite some time.\n\nFirst, you say that all normal users are dumb. Too dumb to get around in kcontrol, too dumb to file a bug. (Do you really need this kind of arrogance to feel smart? Maybe, just maybe average intelligent people are a little bit smarter than you want to believe.)\n\nThen you say that \"popular doesn't mean usable\" really meaning that only what YOU THINK is \"usable\". Well, I tell you something: POPULAR *IS* USABLE. If it weren't usable it weren't popular. Quite as simple as that. Pretty arrogant to say it isn't useless just because you don't want it.\n\nIf somebody wants some feature, don't take it away from him. What is your problem with that principle?\n\n\"Sure. KDE is a damn fine desktop now. But can it grow and be comfortably used by non-techies? That's the question.\"\n\nIt can. Right now. And it is.\n\n\"non-techies\" already can handle the mess of unorganized directory with randomly (read ordered by alphabet, not function) thrown in help-tools in MS Windows. Anybody who can handle that has no problems with coping with an tree-like organized and centralized tool like kcontrol.\n\n\"non-techies\" ignored MacOS and used DOS for over 10 years. \"non-techies\" would still use DOS with all the IRQ-conflicts, config.sys and autoexec.bat problems and memory restrictions if it were preinstalled on their computer and would be the only way to run the newest games.\n\nI have helped quite a lot of people use KDE and Linux and there were only 2 real problems: Missing software (\"but I want to run xy\") and hardware problems. Desktop \"usability\" was *NEVER* a problem. Never. You heard me? Never. Quite to the opposite. A couple were quite impressed that they could move windows with Alt-LMB and resize with Alt-RMB.\n\nWith hardware problems becoming quite rare, only missing seamless and actually working Win32 support is slowing mass-adoption of Linux. Still, in 2003 Linux will make huge inroads as many organizations and governments are adopting it.\n\n\n\n\n \n"
    author: "Roland"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "> First, you say that all normal users are dumb.\n\nI never said that nor do I think it. I know however that people are often afraid of using their computer, afraid of making mistakes, worried about looking stupid and that they will blame themselves easily for things which aren't their fault.\n\n> POPULAR *IS* USABLE. If it weren't usable it weren't popular. \n\nYou got it backwards. Usable is popular, yes. But what is popular with the development crowd is not necessarily usable. That's what I meant. Read back the thread. \n\n> [I]n 2003 Linux will make huge inroads as many organizations and governments\n> are adopting it.\n\nAll very true. And again, don't think that because I'm criticizing some aspects of KDE I don't like it or don't like Linux. Quite the opposite. Now, think about what you just said. All these people, in private organizations in governments... is KDE really developed with them in mind? I believe that it isn't and I believe that Mosfet's editorial is the perfect illustration as to why.\n\nAs you pointed out, non-techies have been using non-usable technology for years. But is that your excuse for KDE's potential usability shortcomings? That we're just as bad as the other guys? You can't be serious!\n\nRoland, calm down. I'm not anti-KDE, I'm not anti-Linux. But it incenses me to see people like Mosfet defending in two mediocre, undocumented and unresearched editorials these backwards positions when some people are trying, the hard way, to make KDE more usable."
    author: "Grumpy"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "> I never said that nor do I think it. I know however that people are often afraid of using their computer, afraid of making mistakes, worried about looking stupid and that they will blame themselves easily for things which aren't their fault.\n\nOk, then they need to be helped.  Removing access to useful preferences for KDE users does not help them.  Your solution does nYou got it backwards.\n\n> Usable is popular, yes. But what is popular with the development crowd is not necessarily usable. That's what I meant. Read back the thread. \not fit the problem.\n\nIf people are using KDE it is because they *choose* to do so.  Similarly, if they are using a preference it is because they *choose* to do so.  If the option/preference is popular that is a de facto reason to keep it.  Why would you not keep it?  So you can actively and willfully piss off your current users?\n\n> All very true. And again, don't think that because I'm criticizing some aspects of KDE I don't like it or don't like Linux. Quite the opposite. Now, think about what you just said. All these people, in private organizations in governments... is KDE really developed with them in mind? I believe that it isn't and I believe that Mosfet's editorial is the perfect illustration as to why.\n\nGovernments and organizations can lock down the desktop all they want with the kiosk mode.  KDE has a working solution and it does not entail stripping KDE of all the powerful/usefull features like they did with GNOME.\n\n> As you pointed out, non-techies have been using non-usable technology for years. But is that your excuse for KDE's potential usability shortcomings? That we're just as bad as the other guys? You can't be serious!\n\nWhat usability shortcomings?  Point to a specific.  You keep beating the same drum and it does not hold any water!  KDE is very usable and ...  that is why it is the leading desktop enviroment for Free Systems. \n\nBoth of Mosfet's articles have laid bare the ridiculous idea that you should remove popular working preferences which pisses off your current user base just so you can satisfy the self-appointed usability experts and some hypothetical 'future/average' user."
    author: "ac"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "> I never said that nor do I think it. I know however that people are often afraid of using their computer, afraid of making mistakes, worried about looking stupid and that they will blame themselves easily for things which aren't their fault.\n\nOk, then they need to be helped.  Removing access to useful preferences for KDE users does not help them.  Your solution does nYou got it backwards.\n\n> Usable is popular, yes. But what is popular with the development crowd is not necessarily usable. That's what I meant. Read back the thread. \not fit the problem.\n\nIf people are using KDE it is because they *choose* to do so.  Similarly, if they are using a preference it is because they *choose* to do so.  If the option/preference is popular that is a de facto reason to keep it.  Why would you not keep it?  So you can actively and willfully piss off your current users?\n\n> All very true. And again, don't think that because I'm criticizing some aspects of KDE I don't like it or don't like Linux. Quite the opposite. Now, think about what you just said. All these people, in private organizations in governments... is KDE really developed with them in mind? I believe that it isn't and I believe that Mosfet's editorial is the perfect illustration as to why.\n\nGovernments and organizations can lock down the desktop all they want with the kiosk mode.  KDE has a working solution and it does not entail stripping KDE of all the powerful/usefull features like they did with GNOME.\n\n> As you pointed out, non-techies have been using non-usable technology for years. But is that your excuse for KDE's potential usability shortcomings? That we're just as bad as the other guys? You can't be serious!\n\nWhat usability shortcomings?  Point to a specific.  You keep beating the same drum and it does not hold any water!  KDE is very usable and ...  that is why it is the leading desktop enviroment for Free Systems. \n\nBoth of Mosfet's articles have laid bare the ridiculous idea that you should remove popular working preferences which pisses off your current user base just so you can satisfy the self-appointed usability experts and some hypothetical 'future/average' user."
    author: "ac"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "> I know however that people are often afraid of using their computer, afraid of making mistakes, worried about looking stupid and that they will blame themselves easily for things which aren't their fault.\n\nYou're never going to solve that problem. Quite simply, a general purpose computer is a complicated piece of kit. You can put as many pretty buttons and flashy lights on it as you like, it's still going to be complicated. I know how to start my car, all I do is turn the key. Doesn't mean I have any understanding of what goes on beneath the bonnet. Making it even easier to start my car won't solve that problem. You can certainly make things appear simple on the surface, but that won't magically make a computer easier to understand. It just means the complicated parts are hidden."
    author: "Psiren"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "> You can certainly make things appear simple on the surface, but that won't\n> magically make a computer easier to understand. It just means the complicated\n> parts are hidden.\n\nYou're absolutely right. And what Mosfet is advocating, directly or indirectly, is that the complicated parts should remain visible because HE can deal with it. Of course he doesn't say that explicitely but that is what this kind of discourse amounts too. Your example is well chosen. People have worked at making your car easy to start because they knew it was better for them and their user. It's a state of mind, it's knowledge, and it's hard work. And Mosfet's editorial goes against all of this which I find very very unfortunate."
    author: "Grumpy"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "I understand where you're coming from, but I think the complicated bits should remain available for those who want them. An advanced tab or whatever is fine, but don't remove them altogether. I bet a home mechanic would be pretty annoyed if he wasn't able to tinker with the insides of his car because someone else figured he wouldn't want to, so they put the bits inside an inaccessible box."
    author: "Psiren"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-20
    body: "> but I think the complicated bits should remain available for those who want them.\n\nAbsolutely. I never meant to imply that they should be unavailable."
    author: "Grumpy"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-21
    body: "> Absolutely. I never meant to imply that they should be unavailable.\n\nA couple of messages above you say that KDE *wrongly* assumes that everything should be configurable per GUI and that the hackers and wannabes shall find out themselves how to change the configuration. (= edit config files themselves)\n\n"
    author: "Roland"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-21
    body: "<i>You got it backwards. Usable is popular, yes. But what is popular with the development crowd is not necessarily usable. That's what I meant. Read back the thread.</i>\n<p>\nAnd it's still nonsense. \"Usable\" is a prerequesite for being \"popular\".\n<p>\n<i>As you pointed out, non-techies have been using non-usable technology for years. But is that your excuse for KDE's potential usability shortcomings? That we're just as bad as the other guys? You can't be serious!</i>\n<p>\nNo of course not.\n<p>\nThe point was (which you seem to ignore) that for MASS ADOPTION, M-A-R-K-E-T-I-N-G (as in preinstalled) and software compatibility (as in Win32 compatibility) is much more effective than usability.\n<p>\nKDE can be the best desktop usability-wise (and in my opinion it *is*. Yes I did try MacOSX, too.) and still not dominate the market.\n<p>\nYour whole point is built on \"if we don't change KDE ... KDE won't be mass-accepted\" - which is just plain wrong. KDE doesn't need to change at all. We need to give organizations time to get used to it, need more marketing, more apps, more drivers for Linux.\n<p>\nP.S.: Why does a GNOME user like you even care? If there is only \"one true way\" and GNOME offers that, why do you bother us KDE-users?"
    author: "Roland"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-21
    body: "> Why does a GNOME user like you even care?\n\nAs I indicated earlier, I use KDE exclusively. This is not a Gnome vs. KDE post, despite what you may think. Such personal invectives only make you sound desperate which does not help your argumentation.\n\nI've said what I had to say. We could go on forever like that. You seem to be stuck on this idea that my goal here is to put KDE and KDE developers down when it isn't. Nothing I can write could convince you that I am in fact well intentioned, so why bother? How can we have a constructive conversation in these conditions?\n\nI'll just leave you with this and won't reply to anything anymore. You and I can deal with KDE the way it is now because we are motivated users. But I know from experience that to the overwhelming majority of computer users (not current KDE users!), the combination KDE+Linux/Unix is not usable enough _yet_. I know this for a fact because it is my job to know so.\n\nKDE's reach is growing everyday, which is great. But it also means that the proportion of motivated users to non-motivated users will change and that soon people like you and me could be the minority. What happens then?\n\nThis, btw, is what happened with Windows. It was designed by techies who had little regards for their users. 20 years later, Microsoft is still paying the price for its shortsightedness and playing catch with usability.\n\nNow please don't tell me that you're going to call me a Windows troll. ;-)"
    author: "Grumpy"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid."
    date: 2003-02-21
    body: "Are you trolling or really that dense?\n\n>  > As a matter of fact, many people are now setting up KDE for things like their families\n> > and rarely do they complain about things like too many configuration options.\n \n> Would we hear their complaints? Provided that they could articulate their confusion with the interface, to whom would they report it? To KDE hackers...\n \nIs this question rhetorical? I guess I'm a KDE hacker as I lead a project. I set up friends and relatives. I help them. They like it. They rarely mess with configuration but it's really no more confusing than windoze in my experience. Especially since KDE does not attempt to obscure what it's doing. You want to talk left out in the cold? Give a computer newbie a copy of W2K and let them configure a fixed IP.\n\n> > The only people who like less configuration options are those who believe their\n> > way of doing thing is \"the right way\". Everyone else is, well, screwed.\n \n> No. They are those who see people using computers everyday, who see people blaming themselves for mistakes they haven't made but that some sloppy or uneducated designer/programmer has made. They are those who see people overwhelmed by complicated computer lingo and an impossibly large set of options to understand. They are those who see people giving up on their task because arrogant computer jerks have decided that if they could understand something one way everybody else should too.\n \nI could not come up with a better example of a twisted and spiteful hatred from one who receives free software to the anonymous developers... however I'm not anonymous as anyone who's ever emailed me for help can attest. Grumpy is. I see one candidate for a \"KDE for dummies\" book. \n\nAs a basic rule people may come to a computer not understanding they have to make a connection to the internet to get email. A friend of mine had that confusion. You either learn or you don't get your email. Likewise if a setting is important to you then you can resource many web sites, email lists or newsgroups if need be. If it is important you will do it and if not you won't much care. In any event you have a greater likelihood of success in a dialog than an rc file.\n\n> > KDE does not make such presumptions. \n \n> KDE makes plenty of presumptions. Not the least of which is that it should offer a graphical interface to almost every single configurable bit in its system.\n \nAnd the really big question that any idiot should see clearly is this. Is it going to be easier to configure that if we leave everything in files that must be manually edited then putting it in configuration dialogs? If this is presumption it is certainly in the favor of users. The greatest presumption is always the one that restricts your freedom!\n\n> KDE is designed by people who make conscious decisions about what should or should not be part of KDE or what is or isn't worth being implemented. So it is about how someone else feels you should work. \n\nThis is quite a leap. Making a decision to make a program more user configurable is deciding to exercise less, not more control over how you work. This is a quantum leap of moronic dimensions.\n\n> If we want KDE to succeed, we must stop thinking like macho computer hackers or immature wannabes.\n\nI have to assume that's the royal we... or you have a mouse in your pocket. First KDE is a very big success already. Second, I contribute a substantial amount of time and money toward making Quanta succeed and it's getting a lot of attention. Third, you have no identity. Who are you? What is your interest and what are you actually doing besides whining? If there is any area I feel I need to exercise restratint it is when I'm lifing the heavy load and listening to the guy on the side of the road doing nothing except pontificating on how I should be handling my load. This anonymous poster's contribution is to antagonize developers with inaccurate generalizations and broad sweeping prejudice. I see nothing factual or practical here.\n\n> Making KDE simpler to use is nothing to be afraid of. It's a good thing and big egos shouldn't stand in the way of it. Embrace simplicity!\n\nAgain, using Quanta as an example, we have worked to make it efficient and productive to use as well as to make it maximally flexible to adapt to your work styles. We have worked to make it work with multiple DTDs and user defined DTDs. We have focused on making it a tool professional would use while being accessable for newbies. The fact is everything streaming down the net or coming from the W3C is *not* simple. You want brain dead you can hack up your HTML with M$ Flunk Page.\n\nThe success of KDE it tied to configurablity to the degree that it supports the professionals who need the services to compute as they want to. If 5 minutes of looking though every option to set it and use it for a few years is a bad trade off get an admin to set you up with kiosk mode. We can make it so simple you can't even find your configuration. That's 'cause it's flexible.\n\nAlso, one reason I chose to develop on KDE aside from the excellent architicture is that fact that everyone I met in the developer community was a gentleman and the lists clearly indicated there was not an issue with ego. The very fact that there is no titular hed of KDE speaks to this as well as anything. It's easy to spew garbage but you should be accountable for your words if you expect credibility.\n\nLinux and KDE are about freedom. If your vision is about a singular mediocrity I'm not developing for it. Besides it will always be too simple or complex or long or short or something for critics. I listen to my users. They tell me we're doing good by them."
    author: "Eric Laffoon"
  - subject: "Re: Mosfet is wrong. Mosfet is afraid.  -- WHY?"
    date: 2003-02-22
    body: "WOW. I cannot fathom where on earth you managed to dredge up so much emotion. Hes talking about good design, not who your mother sleeps with... *take* *deep* *breath*.\n\nSomehow you have deftly managed to turn a debate about Usability into a matter of the deepest importance to Freedom Of Choice (tm). Please Im very interested in why both yourself and Roland ( and mosfet ) have taken this so personally? You state that you are open to listening to users and implementing their preference requests, but when Grumpy speak his mind, he is only a whiner from the sideline? Why do you feel antagonized? I didnt read Grumpys statements as antagonistic, at worst they are misguided but constructive. What do you have to lose if some options get set to sane defaults, and their config tools move from what should be a end-user GUI ( Kcontrol ) to a sys admin GUI ( gconf / kioskmode / ... )? If you want to *only* cater to \"W3C professionals\" then why are you arguing with him about end-user preferences?\n\nMy bachelors is in psychology and my masters work is in Human-Computer Interaction, and I hate to break it to you sir, but Havoc is Right (tm) and mosfet is Wrong (tm). I am not talking about imposing *MY* ideas on others, I am talking about decades of ongoing research into how randomly chose people TRY to use computers, and designing interfaces around them. These STUDIES show that excessive ( note that is an subjective word ) options confuse users, not because they are stupid but because they arent as literate as you or I. Their minds havent built sufficient abtraction to template their tasks against. This has nothing to do with inteligence, and everytthing to do with the way humans think and learn. Please read a book on HCI before you flame opinions you dont understand. While you may have put your friends and family in front a KDE ( and indeed so have I ), and they can click on the konqueror icon, DOESNT mean they know how to use the desktop. Try one of the experiments I do all the time: take a Statistcially Signifigant Random Sample of people ( not including friends and family pls ), record their ( computer ) background information, and have them do a series of real world, humanistic tasks, such as: change the time, download and display/edit a file, email that same file to Bob, ( change a static IP ), etc. This is REAL WORK people must do every day, and based on your post above, the results shoudl astound you.  People are very complex, and although they are not stupid, nor unwilling to learn, they will undoubtedly have problems with KDE -- they have problems with Windows so they should!\n\nTo tell the truth, I hope KDE stays highly configurable, this is an excellent opportunity for KDE and GNOME to differentiate themselves, keep the genetic stock different, and we can wait and see which ideas bubble to the fore.\n\nCheers,\nRyan\n\n"
    author: "Ryan"
  - subject: "AMEN BROTHER!!!"
    date: 2003-02-21
    body: "You've really hit the nail on the head there. The computer industry has done a great job of raising users to be fearful and to accept thier difficulties when using a computer as being thier own dumb fault. Time for everyone to grow up. \n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Less is Better -- Not"
    date: 2003-02-20
    body: "Less is Better, only if you have poorly chosen your defaults.\n\nIf the average user needs to make more than a few changes to be productive, the defaults have been poorly chosen.  That may be the case for Gnome.  I know that Gnome never comes close to working for me right out of the box.\n\nBut, I believe that in general, the defaults have been well chosen in KDE."
    author: "Gary Krueger"
  - subject: "Re: Less is Better -- Not"
    date: 2003-02-21
    body: "Well, what's about the average default? When I got to KDE the first time I heard about the focus follows mouse once before and that's available in KDE - one of the reasons why I got it in the first time. And I was a novice.\nHeck, leave it as it is, I *LOVE* this many features and if you really think teh average dumb user can't handle it then better make a SimpleKConfig but please leave every simple Options.\n\nBeside, I think the current hirarchic structure is pretty fast to understand, apart from that there are different items for the widgets, icons and alltogether, couldn't you make it in to a subtree?\n\nKind regards,\n Micha"
    author: "micha"
  - subject: "New, updated commentary on mosfet.org"
    date: 2003-02-20
    body: "Since many people asked for me to write a more direct rebuttal to Havoc's \"free software UI\" paper I've written another commentary that specifically addresses this. Havoc's paper is generally considered to be the one that started the \"less-is-more\" debate, and my response goes more in depth about where I feel he is right and where I think he's wrong. It gives a lot more detail on my position.\n<p>\nYou can check it out here:\n<p>\n<a href=\"http://www.mosfet.org/free-software-rebuttal.html\">free-software-rebuttal.html</a>"
    author: "Mosfet"
  - subject: "Re: New, updated commentary on mosfet.org"
    date: 2003-02-21
    body: "Mosfet, thanks for posting this. I've heard for some time good things about Havoc, his work ethic and approach. However when I saw in his position at RH he reported the single click as a bug I wondered how \"even handed\" he was. Clearly your analysis here of his document is even handed if not generous showing he has some good points. However some of the points that he makes are truly obtuse.\n\n- Stating things he doesn't think can be done or done well which are being done well in KDE leaves a dark question.\n- Sacking the default window manager becuase it has too many configurations... I think this point must have gone by many Gnomers complaining.\n- Citing programs non-GUI console configuration an argument against GUI configuration requires a double take to see if he's joking. This is *THE* argument for GUI configuration as opposed to remembering a zillion keyboard commands.\n- Falling just short of contempt for users, suggesting they should not be given what they want to get their work done but what he decides to give them.\n\nMy take after looking through this is that it's really frightening to think that freedom could so easily slip from free software. Thank God for choice and KDE! I'm beginning to suspect where some of the contempt users have for developers came from now when I read \"shut up, we know best\" thinking like this. I have seen comments that Gnome has sold out on talk backs and thought that they were the usual case of people talking out of their hat. After looking at some of his quotes here one wonders if he really doesn't know a GUI from a command line or is towing the line from a company saying \"make it simpler or else\".\n\nI think there is a common thread here. Several years back when Gnome picked up all the corporate support many of their advocates were calling for the single desktop and saying it had been decided. I've been philosophically opposed to this even though I've never much cared for Gnome. Choice is good. Now the call is for the dumber desktop and shut up becasue \"we know best\". The thread is... suspect anyone who is against your freedoms. \n\nI think I can sum up the philosophical difference. Havoc seems to think freedom is too messy and smart people can sort out what you need. KDE seems to prove that freedom can be organized but is first and foremost cherished by the smart people who are our users.\n\nI haven't followed Gnome much. I've had other things to do. I have to say that for their loyal user base to wait over a year to cheer for anti aliased fonts and other things we've enjoyed all that time only to be poked in the eye with removed versatitlity is a sad thing.\n\nOT I once went to work for a company that had created so many successful people they bought the company from the founders... then they started teaching what they thought *should have been taught* to be successful instead of what they in fact *did* to be successful. In a few years the company that survived the better part of a century was dead.\n\nMosfet is right... you make people happy by making them happy. Period! Users are NOT stupid!"
    author: "Eric Laffoon"
  - subject: "Sweet Irony."
    date: 2003-02-22
    body: "http://developer.kde.org/documentation/design/ui/summary.html\n\nAvoid rampant customisation.\nCustomisation has the effect of delegating part of the interface design to the user. They may or may not be qualified to do this. Users are not usability experts; we are. If a user can, by a few judicious choices, really improve the interface, we probably have done a poor job. Most users, if given a reasonable interface, just want to get their jobs done.\n"
    author: "Soup"
  - subject: "Re: Sweet Irony."
    date: 2003-02-22
    body: "keyword: rampant"
    author: "ac"
  - subject: "Summary of this thread so far"
    date: 2003-02-20
    body: "Summarized we have\n\n- a lot of real KDE-users crying not to take away any configurability. I also fall into that category. Actually one also described how his mother doesn't have any problems with kcontrol at all.\n- a few KDE-users saing that there could be a dumbed-down version for somebody else and a full version for themselves.\n- one GNOME-fan and KDE-basher who sais that KDE is too complicated and needs to remove configurability.\n\nListen to actual users, not bashers. Bashers will never be happy, any effort to satisfy them is pointless and wasted energy.\n\n*Not a single person on this thread demanded fewer options for themselves.*\n\nIt's always the hypothetical \"average user\".\n\n"
    author: "Roland"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-20
    body: "> Listen to actual users, not bashers.\n\nBy all means, do. And you will see that they disagree with Mosfet."
    author: "Grumpy"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-20
    body: "> By all means, do. And you will see that they disagree with Mosfet.\n\nYeah?\n\nOn this thread alone there are half a dozen requests for *MORE* configurability.\n\nCan you come up with a single example request for less configurability?\nNo, I don't want some request for some anonymous, hypothetical, mysterical \"average\" user, I want some real user complaining about too many options and he would like less FOR HIMSELF.\n\n"
    author: "Roland"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-20
    body: "> On this thread alone there are half a dozen requests for *MORE* configurability.\n\nBut people who read, and post on, KDE.news are not necessarily representative of current or future KDE users."
    author: "Grumpy"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-20
    body: "If this isn't the place to find KDE users then could you please direct me to the correct forum?\n\n... and do not talk of 'usability' labs and the self-apointed 'usability-experts' as they really have nothing to do with KDE.\n\nThis is *our* desktop and is made for the people that are contributing on this forum.  We will not and should not sacrifice our current users and there opinions/preferences for some hypothetical 'average user' that may or may not exist now or in the future.\n\nGrumpy: I've seen you all over this article disagreeing with Mosfet and saying how KDE has too many preferences ... well which preference would you like to see taken away from you?  Point to them, please.  Why don't you assemble all of them (must be a big list by the number of posts where you've been exhorting the KDE developers to take away preferences) and send them to the list and we can discuss specifics.\n\nSo, which preferences exactly do you want us to take away from you so you don't get so damn confused??"
    author: "ac"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-20
    body: "> If this isn't the place to find KDE users then could you please direct me to the \n> correct forum?\n> ... and do not talk of 'usability' labs and the self-apointed 'usability-experts' as\n> they really have nothing to do with KDE.\n\nTake private organizations and governement users, the same users Roland was referring to earlier. These are the people who just use whatever computer their employers are providing them with. They go back home at night and think about something other than their desktop environment. They do not know or care about what KDE is or stands for, unlike you and me. They don't have to. Unfortunately, I cannot point you to the correct forum for them because it doesn't exist. They are anonymous users and, believe it or not, they are the majority. I work with them everyday.\n\nThe only way for their voice to be heard is to have usability labs and \"usability experts\" work on KDE's usability problems. I don't understand what you mean when you say that usability experts \"have nothing to do with KDE.\" They have everything to do with KDE. Why dismiss them?\n\n> This is *our* desktop and is made for the people that are contributing on this forum.\n\nYes and no. When KDE is released to the public, it also becomes everybody's desktop. You seem frustrated by the idea that KDE might escape your control. But if you want KDE to grow, you have to accept that it'll be used by people different from you. It's inevitable. You can't wish KDE well and rejoice when it is deployed at yet another company and at the same time want it to be yours only.\n\nPerhaps you don't want KDE to be used by many. If that's your opinion, that's fine, I respect it. But if it isn't, your position is untenable."
    author: "Grumpy"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-21
    body: "> Perhaps you don't want KDE to be used by many. If that's your opinion, that's fine, I respect it. But if it isn't, your position is untenable.\n\nI really wonder what I'll lose first in this thread: My temper or my mind.\n\nOK, now again, what you are ignoring, Grumpy:\n\nLet's assume that you finally find out the all-perfect configuration for KDE.\n\nIf we make that the default, how can anybody be confused by configuration options when everybody is already happy with the defaults?\n\nGrumpy, your whole chain of argumentation is flawed from the very beginning.\n\n"
    author: "Roland"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-22
    body: "Since you want configurability, you must have been very angry that your \"option-full\" file selector was \"dumbed down\" from this version:\n\n http://img.osnews.com/img/1347/dialog.png\n\nTo the current one ;-).\nOr maybe you guys are just so thick-skulled that you take any one intending to give constructive criticizm as a troll.\n\nOur beloved KDE project will do alot better without people like you.  This place has become a religious meeting place where people leave their brains out at the door and once inside, begin chanting the slogans and speeking in tongues they don't understand.  \n\nAlmost every known fallacy in the book is excercised here. \n\nSee: \n\nhttp://www.nizkor.org/features/fallacies \n\nto learn how to make your point without resorting to fallacies, and to evaluate your opinions before making them public -- if you really care about the KDE project.  "
    author: "Soup"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-22
    body: "The old version admittedly looks better.  It makes sense to have Home on the toolbar and in the context Menu.\n\nWhat you fail to understand however is that Usability and Configurability are not mutually exclusive!\n\nThink about it."
    author: "ac"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-22
    body: "And what you fail to understand is that \"less\" configurability does not mean \"less\" usability, nor does it mean \"no\" configurability. And while we are at it, \"more\" configurability does not mean \"more\" usability.\n\nAssuming you aggree with the above statement, think about these statements:\n\n\"less is more\"\n\"more is more\"\n\nWhat do they mean? Have you actually understood what they mean before categorically accepting one as true or the other as false? Can you justify your choice objectively?  I haven't seen a single post here that is objective.  Failing to find ammunition, everyone has translated the statements to the following:\n\n\"none is more\"\n\"all is more\"\n\nWhich are easier to argue for/against.  So the KDE guys can easily flame the GNOME guys for having \"no\" configurability, and the GNOME guys can flame the KDE guys for having \"all\" possible options in one's face.  But when you look at the real situation, deep down they agree with each other.\n\n"
    author: "Soup"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-22
    body: "You're talking a lot of fluffy nonsense, my friend.  Get a life."
    author: "ac"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-22
    body: "There you go. Another fallacy:\n\nAd Hominem Abusive\nhttp://www.nizkor.org/features/fallacies/personal-attack.html\n"
    author: "Soup"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-23
    body: "There you go. Another fallacy Soup:\n\npot calling the kettle black\nhttp://potcallingkettleblack/features/soup/isabigidiot.html"
    author: "ac"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-21
    body: ">But people who read, and post on, KDE.news are not necessarily representative of current or future KDE users.\n\nOn what do you base that claim?"
    author: "Roland"
  - subject: "Re: Summary of this thread so far"
    date: 2003-02-20
    body: "This article and the response makes quite clear that the majority of KDE users agree with Mosfet.  Quit trolling."
    author: "ac"
  - subject: "You have to have both"
    date: 2003-02-20
    body: "Hasn't Eugenia Loli-Queru has touched upon this? \nThe problem is not \"too much\" but rather \"where do you find it\".\nYou have to arrange the menus in such a way that they are simple to deal with for the newbies (and those who don't bother) and still leave room for the advanced (or exerimental) users a few clicks down the menu tree."
    author: "G\u00f6ran Jartin"
  - subject: "Re: You have to have both"
    date: 2003-02-21
    body: "Yup, she has. She replied to Mosfet on pclinuxonline with a link to Havoc's great article about \"less is more\" (http://www106.pair.com/rhp/free-software-ui.html), which contradicts Mosfet's opinions with some great arguments, but Mosfet didn't reply at all with any new arguments!"
    author: "Jean-Pierre"
  - subject: "Reread the article ;-)"
    date: 2003-02-21
    body: "I wrote a whole new commentary that directly addresses Havoc's paper. I don't get into debates with Eugenia because she spends way too much time trolling both KDE and me in particular >:)"
    author: "Mosfet"
  - subject: "Re: Reread the article ;-)"
    date: 2003-02-21
    body: "I think you're right about Eugenia's trolling, but I was after a rather specific point that I think she's been making (if not, I'll make the point myself, and let's start from there :-) )\n\nIt's about the structure of the configurability. I, personally want to be able to configure almost everything on my desktop. I want to be able to choose the mp3 player that I think has the nicest interface, or to use the imageviewer of my choice in Konqueror.\nI don't want the \"GUI police\" to tell me what I need and what I don't need, whether I should singleclick or doubleclick.\nIn a few words: I want to adapt the UI to myself, not the other way around.\n\nBut, at the same time, the instruments to adjust the UI shouldn't be spread all over the place. \nInstead of inventing the perfect UI (which will never exist, anyway since people are different) efforts should be made to standardize the tools that people need to perfect their own UIs, and to make them easy to find.\n\nOf course, most people will never use anything but the standard settings. But for those who want - or need - something else, it should be easy to find out how. "
    author: "Goran J"
  - subject: "KDE start menu should support Drag and Drop"
    date: 2003-02-20
    body: "Is there something fundamentally difficult in implementing Drag and Drop for the KDE Start Menu? This is one feature I really miss. I always try to drag icons from the desktop into the menu or to reorganize the start menu using Drag and Drop, but obviously this does not yet work. I know that a program called kmenuedit or something exists, but I do not like it.\n\nAbout the amount of configuration options: The configuration options should be better organized, but they should not be totally left out. There should be a way to lock them down, but the kiosk functionality already does this. \n\nregards\n  \nA.H. "
    author: "Androgynous Howard"
  - subject: "Re: KDE start menu should support Drag and Drop"
    date: 2003-02-20
    body: "Totaly agree with you on the drag 'n drop issue... I think it is not essential, but a nice touch and one more step toward ease of use.\n\nCheers..."
    author: "Phantom"
  - subject: "Re: KDE start menu should support Drag and Drop"
    date: 2003-02-22
    body: "I actually hate that behavior in Windows..."
    author: "Datschge"
  - subject: "Re: KDE start menu should support Drag and Drop"
    date: 2003-02-22
    body: "I agree that there should be some way to \"lock down\" the Drag and Drop behavior. As with most drag and drop stuff, it is much too easy to accidentally change the GUI. I have seen it about a thousand times that some newbie user had changed his office or windows toolbars by accident using DND, and was utterly confused afterwards. So *every* drag and drop feature should have some kind of lock. This should in fact be a common GUI element. \n\nBut if you know how to use it, reorganizing a menu using drag and drop is much faster than using the menu editor. Reorganising the k menu using kmenuedit is so cumbersome that I do not bother with it. I just use a command line. But of course that is not an option for a newbie user.\n\nregards,\n\nA.H."
    author: "Androgynous Howard"
  - subject: "Re: KDE start menu should support Drag and Drop"
    date: 2003-02-25
    body: "What about holding down a key (say Ctrl) and then drag stuff? Is that a good enough of a lock for you?\n\n(PS: oh, and I like my KDE configurable up the hilt as well, I'm no newbie, having mucked with X and several flavors of *nix for several years (2 digits :) in my life, and I agree fully with mosfet)\n\n(PPS: I like (and agree) with Mosfet's rebuttal to Havoc's paper)\n\n(PPPS: I love KDE. Really. I friggin LOVE it.)"
    author: "coolvibe"
  - subject: "Defaults are for those who don't configure"
    date: 2003-02-20
    body: "More configuration is better, and I believe that defaults should be chosen with great care to eliminate the need to configure as much as possible. The people who need a simplified user interface will probably spend all of their time learning the default scheme instead of figuring out how to change things, that's why a good default scheme is important. People who do like to change things will want to change whatever they don't like. If you put restrictions on options, you will be deciding what should be changed and what should not. KDE is not an interface. It is an environment that is conducive to building an interface that is suitable for the user. Gnome does not offer this. I tried Gnome 2 recently and found its control panel very lacking."
    author: "Paul Kucher"
  - subject: "the main KDE-usability issue:"
    date: 2003-02-20
    body: "the configuration options overbloating is always a problem of big projects. Different people have different preferrences, and developers usually do their best to meet such configurability demands -- but extreme configurability usually appears to ba problem for newbies. \n\none of the solutions is to introduce 'configurability level' like is was done in some gnome applications or in xine, where user could adjust ammount of options available for changing. I have a strong believe that such approach is not a good idea, because it leads to confusion with self-estimation, and it tends to mess things up in users' head. For example -- the famous MS-Office 2000 menus, which showed only freqently accessed options (later it appeared in Windows ME and Windows 2000) -- when i teached students the basics of computer use, this feature was very confusing to them. \n\ni think that the more appropriate solution will be like it is done currently in KDE or in OpenOffice or in Mozilla, especially after those numerous usability reworks.  All three systems introduce somewhat comprehensive configurability implemented in GUI, which is well-organized and categorized. And the rest of the most advanced, morst complex and most novell options are left for text files. \n\nfor example -- i really appretiate the fact that mozilla have very-well documented text-files configs, and also supply lots of usefull examples (such as UserContent.css) -- i think that KDE should do it as well -- KDE has extremely great configurability potential, but most of it is nullified by the lack of documentation and samples on those text-configuration-files in .kde.\n\nexample -- give me a point in the KDE manuals on how to add new RBM entries for sertain file types, like \"uncompress here\" entry for archives. Or any hints about how to use user's custom CSS's in konqueror for blocking ads?  Or any hints in User's manual to XML-gui files, which user could modify to adjust the contorls layout and look&feel in his favorite applications? \n\n\n"
    author: "SHiFT"
  - subject: "Re: the main KDE-usability issue:"
    date: 2003-02-20
    body: "> give me a point in the KDE manuals on how to add new RBM entries for\n> sertain file types, like \"uncompress here\" entry for archives.\n\nVery good tutorial by Aaron Seigo here: http://developer.kde.org/documentation/tutorials/dot/servicemenus.html"
    author: "Grumpy"
  - subject: "More is always better"
    date: 2003-02-20
    body: "I have to agree with Mosfet also. Every time I install KDE i touch almost every part of Kcontrol. More, sometimes I have several users on the same machine with different desktop configuration corresponding to what I am doing. \n\nIt seems that every comparison between Windows and Linux from a long time starts with \"my grandma\", \"my mother\", \"Joe the user\" and so forth. Users are NOT a bunch a stupid people. Some of them may be uninterested or not knowledgeable enough but that can change in time (and will change if there are reasons). \n\nWhat would be interesting would be to be able to have multiple \"full\" configurations. I would like to change immediately almost everything according to several previous setups. Say I am working on my 21 inch monitor. A set of fonts, colors (CRT's are not the same, some color scheme is better that another one') and so forts. But maybe I am working directly from my 800x600 laptop. Everything changes then. Color scheme (tft is different) fonts, smaller panel, smaller icons and so forth.\n\nSorin M"
    author: "Sorin M"
  - subject: "Configuration levels"
    date: 2003-02-21
    body: "I think it would be a great idea for each setting in KDE to be assigned a configuration level. Maybe from 1 to 10. By default, only options in level 3 or lower might be shown. At the top of the Control Center, maybe in the menu area if Qt allows it, there could be a simple slider from Simple to Expert which would change the viewed configuration level. The problems with this system would be when certain sections have only one option at a level. In that case, it might be combined with other similar sections though how that would be decided, I'm not sure. Perhaps make an internal (not seen by the user) tree with more levels of similar sections or something..."
    author: "Luke-Jr"
  - subject: "Please read the updated article!"
    date: 2003-02-21
    body: "I went a lot more in depth with the new commentary and it addresses a lot of the things said here!"
    author: "Mosfet"
  - subject: "Configuration is one thing .... UI is another"
    date: 2003-02-21
    body: "Apple has a very configurable desktop ... the UI for configuring it is sane however.  Anyone who has honestly studied the interaction of average users with  the KDE control centre will have to admit. The UI is insane, complex, inconsistent, non-intuitive and merely papered over by a \"help system\" to try to explain how it works - it shouldn't require as much explanation or a many icons.  You don't have to take a HID course to understand this.  \n\nIt's the configuration UI and UI of various configuation elements that is the problem - not \"configurability\" per se .... \n\nXP has improved over KDE seems deadset on being as confusing as XP plus a little more confusion for good measure.  That's why people keep refering to Mac's and to \"uncluttered\" UI .... because KDE like XP is whacky ..."
    author: "Clicked on OpenOffice Yesterday"
  - subject: "Re: Configuration is one thing .... UI is another"
    date: 2003-02-21
    body: "> Apple has a very configurable desktop \n\nWrong, MacOSX didn't allow me to turn off Minimize/maximize animations, which piss me off.\n\nAnd that's just one example. No multiple desktops, poor configurability of the dock and no panel applets are others.\n\n\n"
    author: "Roland"
  - subject: "Re: Configuration is one thing .... UI is another"
    date: 2003-02-21
    body: "Strange, I have a friend who did exactly this. Sorry, I can't tell you how but I think it had something to do with speed or so"
    author: "micha"
  - subject: "How about hierarchy and inheritance?"
    date: 2003-02-21
    body: "I'd like to be able to configure a general configuration page like Look, where I'm able to configure \"Flat V2\" \"Redmond XP\" etc., Background and Double-/Single-Click. When I want to configure more precisely I can create my own profile (starting with an already existing one) which consists of Desktop, Taskbar, Mouse Behaviour, Window Decoration ... which again have more general profiles by default ...\nIt then would be great, when every subpart of that configuration could be delivered as seperate software module for download from websites like kde-look.org.\n\nA general setup configured in KControl could be inherited by individual application (like for example a localized keyboard layout for KWord). The configuration of those applications should again be hierarchical and every subpoint should have the option \"KDE default\" to modify only the parts wanted.\n\nAlso I think KControl shouldn't get too bloated. I personaly don't like the idea of an M$ Control Panel for everything. Stuff like system time, font management, network configuration, management of personal information (email), system information ... doesn't belong into the same application, but in seperate tools. WebBrowsing is of relevance only for Konqueror and thus should be in the Konqueror settings page only.\n\nGenerally it'd be great being able to configure GTK/GNOME and QT/KDE stuff in the same interface ... ok, ok :D\n\nAs a last point, I agree that the look of KDE configuration dialogs should be homogenous. For example, the panel on the left side in Konqueror 3.1 I find confusing."
    author: "Peter Shaw"
  - subject: "Configuration is best"
    date: 2003-02-21
    body: "KDE is better to me because it can highly be personalized, some where like 30 times more than Gnome, KEEP IT!!!! and IMPROVE IT!!!!! Those two things will make KDE users stay with KDE"
    author: "Bryan"
  - subject: "Finally KDE and GNOME are really different...."
    date: 2003-02-21
    body: "\nI'm sick of the endless debate, however, personally I think it's nice that KDE and GNOME have something that really distinguishes them from each other.  They are taking very different approaches.  I don't think most people are going to choose their desktop based on what toolkit it's based on or what is the primary language used to develop the apps or what themes are available.  What's really going to make a difference is how pleasant the interface is to use.  Is it a joy to use or a bitch?  The next couple years will be telling ones..."
    author: "Jamin Gray"
  - subject: "Quality, Not Quantity"
    date: 2003-02-21
    body: "I agree with Mosfet, and have this to add.\n\nIt all boils down to this:\n\nJust having MORE configuration options is not better.\nHaving BETTER configuration options is better.\n\n(I feel almost ashamed that I have to say this explicitly; it's a well-known fact.)\n\nMore configuration options will confuse more than the average user; it will confuse power users and developers also. (I swear I saw one option for the taskbar twice in kcontrol, but I can't find it again; there are a bunch of options there, and more than one way to categorize some of them. I wouldn't call the taskbar config a particularly bad example; in fact, it has improved a lot.)\n\nBetter configuration options, especially better layout thereof, will make common options easier to find and all users feel more in control.\n\nI strongly disagree with the notion of a global switch between simple and expert modes. I always hit Custom in Windows installers and Expert on any nontrivial application, because what I think is simple and what the application developer thinks is simple can be very different things. Moreover, the Expert path is nearly always more tested, since that's what the developers use.\n\nIdeally, configuration options should follow these goals:\n\n1. The defaults should produce a reasonable compromise between bare-bones simplicity and advanced, cluttered, and mega-eye-candy. The \"average user\" should find the overall experienced produced by the defaults to be stylish but not distracting, commanding but not confusing, and above all, functional. There shouldn't be any setting that an \"in the know\" user immediately sets upon configuring a system for someone else.\n\n2. The first options that a user finds should be the most pertinent to the task at hand. This may be accomplished through context menus, \"tips\", category-level options, \"themes\", etc.\n\n3. Categorization should reflect increasing level of detail, grouped by where a user is most likely to look, not necessarily by what it controls on a programming perspective. Subcategories and tabs should be used only where the division is obvious to the user. If there is more than one grouping that makes sense for a particular option, a hyperlink may be employed to direct the user to where the option is actually located.\n\n4. All options should include contextual help for their overall purpose and the specific effects of each setting, as well as how different options interact. Consider \"themes\" when options interact in significant and unobvious ways.\n\nKDE generally seems to be moving towards these and other goals. I'm sure that Usability Professionals and User Interface Designers could and already have put similar goals in better words than mine.\n\nFor whatever it's worth, I've deliberately set some options to more closely mimic the behavior of Windows, even when the default KDE setting seems technically or usably superior. Having general \"themes\" in kpersonalizer can help when you know what you want, but I suggest a slightly more formal and complete way to deal with the \"theme\" concept. Within the kcontrol framework, there could be a global list of the defined sets of options, with predefined and user-definable \"themes\" that group their settings. For each set, the theme most closely matching the current settings can be selected, and the differences between the current settings and the theme settings can be listed. That way, I could see concisely where my settings differ from the KDE defaults or the Windows-alike \"theme\", and make a better decision on which options I really like better.\n\nKen\n"
    author: "Ken Arnold"
  - subject: "Re: Quality, Not Quantity"
    date: 2003-02-21
    body: "Going back and reading the last several comments before mine... I agree with most of them. Especially, \"profile\" (from 2 (?) comments up) is a much better word than \"theme\" for what I was trying to say in the parent comment.\n\nKen\n"
    author: "Ken Arnold"
  - subject: "Re: Quality, Not Quantity"
    date: 2003-02-21
    body: "Furthermore, a global switch only leads to this:\n\nOn the phone...\nNovice: Hi, can you help me? I want to change my background!\nGeek  : Sure, click on the \"K\" in the lower left corner,\n        then click on \"Start\" the KDE Control Center.\nNovice: OK. The control center window seems to be open.\nGeek:   Now click on \"Appearance\"\nNovice: I don't see the word \"Appearance\"\nGeek:   ???\n\nIt takes lots of time to find out that the novice is in simple\nmode and the geek is in advanced mode. In the simple mode the\ntree has different and less categories.\nThis also happens if something is described in a book, in a\ncomputer magazine and so on. \"Hey, I'm using 3.1 but where is that option?\"\nGuess this would lead to lots of confusion. Not really a step towards\nan easier user interface especially for beginners.\n"
    author: "Stephan"
  - subject: "Re: Quality, Not Quantity"
    date: 2003-02-23
    body: "I thought if you were walking someone through a process you literally did it as well to make sure you are doing the same thing so that you see exactly what he sees and actually do, or emulate what you are telling him/her to do."
    author: "Maynard"
  - subject: "Advanced users are users too!"
    date: 2003-02-21
    body: "Mosfet is definitely right here\n\nThe day you take my configurable KDE away from me because some 'Expert' decided that some hypothetical 'casual user' might not want configurability, is the day you pry it from my cold, dead hands.\n\nAdvanced users are users too!, and I've been a KDE user since before the first alpha release, because I've always been able to make it suit me, even when the developers have decided that the defaults should be different.\n"
    author: "Stuart Herring"
  - subject: "Re: Advanced users are users too!"
    date: 2003-02-25
    body: "Exactly. And once you customized your KDE the way you like, you hardy touch the settings anymore.\n\nThe usability-nazi goons have an argument that's flawed at the foundation. The worse thing is that the GNOME developers are listening to them, and pissing off lots of GNOME users in the process. Oh well, more users for KDE I guess... Maybe GNOME 3 will be hella configurable again, at least, I _hope_ for the GNOME project that they will see the fundamental error in their ways eventually.\n\nIf you want _less_ configurability, KDE-kiosk mode is just plain cool. I use it for internet cafe terminals and it's really easy to set up. \n\nI salute the KDE team for a job very well done and hope they will never take kcontrol (and it's well sorted cornucopia of opions) away! "
    author: "coolvibe"
  - subject: "Slashdot.."
    date: 2003-02-21
    body: "..is where you go for intelligent opinion:\n\n\"The people who make the most noise are not your \"userbase\". They are merely people who are making a lot of noise. You've always got to remember the silent majority who are just using the software, not bitching at the developers. That means you shouldn't add a feature just because a lot of noisy people want it, you should evaluate a feature based on the arguments for and against. Obviously, if it seems to be a popular feature take that into consideration, but the validity of the change should always come first.\" [http://slashdot.org/comments.pl?sid=54594&cid=5351119]\n\nIf we want to make KDE a success we must think of the majority.\n\n"
    author: "MxCl"
  - subject: "Re: Slashdot.."
    date: 2003-02-22
    body: "\"but the validity of the *change* should always come first\"\nExactly. Don't remove features. Please add features. Only change them if it's an actual improvement. Ready. Thanks for giving me a nice quote which contradict some of your previous posts. =)"
    author: "Datschge"
  - subject: "UI design is a NOT a matter of opinions"
    date: 2003-02-21
    body: "It is always good to see debate on UI on the forums. What worries me, however, is that most people here seem to think that UI design is a matter of opionion. It is not. Or at least it shouldn't be. In most cases it should be a science, with measurable results. One example of this is reading speed with different foreground/background colors and fonts. The combination that gives one user the fastest reading speed will almost certainly be best for most other users too. I challenge you to find a single user that does not read black text on white background most quickly (measure results with a stopwatch). It is, sadly, not uncommon to still find people who have changed their preferences to red text on a green background. Which leads us to the point that Mosfet misses the point when he says \"Believe me, there is no lack of feedback from users about what they want\". \n\nUsers are not UI experts and they do not usually know what is best for them fom a UI perspective. I suspect that Mosfet also has not studied much UI engineering and is basing his opinions on personal preferences alone. Whould you trust a coding advice from someone who has never picked up a book about programming? Probably not. Then why is it that people assume that they can be experts on UI design without som serious studying? "
    author: "Anonymous"
  - subject: "Re: UI design is a NOT a matter of opinions"
    date: 2003-02-21
    body: "> Then why is it that people assume that they can be experts on UI design without som serious studying? \n\nBecause everyone is a user.\n\nUsers may not necesarily know what's 'best' for them, but they do know what they like, and they're not necesarily rational about it.\n\nObviously UI studies are important to make sure that in the general case, the UI is more accessable, but it is aways wrong to make the assumption that what is right for the general case is therefore right for every case.\n\nWhat matters is sensible defaults, that's where the UI expertise is important, but don't take away the ability to change the defaults if a user doesn't fit into the mold in every respect, and I think that is the point that Mosfet was trying to make.\n\n"
    author: "Stuart Herring"
  - subject: "Re: UI design is a NOT a matter of opinions"
    date: 2003-02-22
    body: "Except that he wrongfully assumed that Havoc implied taking out all configurability."
    author: "Soup"
  - subject: "Re: UI design is a NOT a matter of opinions"
    date: 2003-02-22
    body: "It's what happened to GNOME isn't it?"
    author: "ac"
  - subject: "Re: UI design is a NOT a matter of opinions"
    date: 2003-02-23
    body: "Do you even know what GNOME is? Have you ever used it? Do you know what you are talking about?\n\nI can't help but wonder."
    author: "Soup"
  - subject: "Re: UI design is a NOT a matter of opinions"
    date: 2003-02-23
    body: "are you as big a freakin moron in real life as you portray here?  have you ever tried not being a moron?"
    author: "anon"
  - subject: "Exactly, since UI design is a matter of taste."
    date: 2003-02-22
    body: "A user will feel good using a computer when he feels good doing so. Some do so using MacOS X, others don't, some do so using Win XP, other don't, some do so using Gnome, others don't. This applies for all GUI's and also for KDE, regardless what opinion, taste or feelings \"UI experts\" have."
    author: "Datschge"
  - subject: "Theme Manager"
    date: 2003-02-21
    body: "I think the GUI for the Theme Manager desperately needs an overhaul. I would like the Theme Manager to be a simple interface to the Colors, Style and Window Decoration pages. For example it should be possible to change to Mosfet Liquid style without using two different dialogs (Window Decoration and Style). And the separate \"Mosfet Liquid\" page is also very confusing. Yesterday I saw some user who had installed liquid via rpm and could not figure out how to select it because he thought the relevant page was the separate \"Mosfet Liquid\" page."
    author: "Androgynous Howard"
  - subject: "Re: Theme Manager"
    date: 2003-02-21
    body: "Exactly!\n\nAll this Colors Window decorations and style and other blaah is way too dispersed unlike in Windows since 95."
    author: "Void"
  - subject: "Re: Theme Manager"
    date: 2003-02-22
    body: "You mean they should do it like how it is currently done in GNOME?"
    author: "Soup"
  - subject: "Re: Theme Manager"
    date: 2003-02-22
    body: "You can do it in KDE already.  GNOME has nothing to do with it."
    author: "ac"
  - subject: "Re: Theme Manager"
    date: 2003-02-22
    body: "Looks like this is becoming Mosfet next project. =)"
    author: "Datschge"
  - subject: "KDE should have lots of config options"
    date: 2003-02-21
    body: "This story has been posted on Slashdot. If you read all the comments there (and here), you can only conclude 1 thing: _there is no universal answer!_\nToo many people disagree with each other. 50% wants a minimalist desktop, while the other 50% wants a highly configurable one.\n\nIf people *want* a miminalist, less-is-more desktop, then they'll use GNOME 2 instead. That's why KDE should be the opposite and have lots of config options instead. KDE should be an alternative to GNOME (and Windows and MacOS X and....), so people can have a choice."
    author: "Stof"
  - subject: "Re: KDE should have lots of config options"
    date: 2003-02-21
    body: "Again with the choice thing!  Users do not know if they want a choice ... it is up to us to decide for them whether they should have a choice because they are not Desktop Environment developers.\n\nBy giving the users a choice of desktop you are throwing out years of research on Desktop Environments sponsored by O.T.W.R.C.D.E.S.E. (One True Way Research Council of Desktop Environment Self-appointed Experts).\n\nWe should be kind to the user so that he doesn't have to suffer through making a choice of Desktops that might be confusing for the poor stupid fool.  By making the choice for him he will be happy and can have something that Just Works^TM.\n\nSo you see, the whole concept of 'choice' should be thrown out the window and everyone should just use the Desktop Environment that is dictated by OTWRCDESE since it is the real scientific answer.  After years of research the OTWRCDESE has determined that the command line with absolutely no preferences is the preferred Desktop Environment for everyone.\n\nHave a nice day and happy computing :-)"
    author: "anon"
  - subject: "Re: KDE should have lots of config options"
    date: 2003-02-22
    body: "You have committed several mistakes in reasoning:\n\nless configurability does not mean no configurability.\nA novice user is not stupid.\nless choice does not mean no choice.\nYou are allowing your self-centerdness to cloud your judgment.  You think only about what you want and not what would be good for the majority of people."
    author: "Soup"
  - subject: "Re: KDE should have lots of config options"
    date: 2003-02-23
    body: "You have committed several mistakes in thinking:\n\nno one said squat about configurability.\nyou are stupid.\nno friggin choice for you.\nyou are a pretentious bastard for thinking 'papa knows best'.\nYou are not papa."
    author: "anon"
  - subject: "no!"
    date: 2003-02-21
    body: "No, GNOME should be an alternative to KDE because KDE was here first and is the most advanced of the two."
    author: "ace"
  - subject: "Re: no!"
    date: 2003-02-22
    body: "Really. \"I was here first.\" Man thats mature. \"My desktop can beat up yours!\" Seesh."
    author: "Ryan"
  - subject: "Not just a desktop matter."
    date: 2003-02-21
    body: "First: I would not like to reduce the configuration options.\n\nIt is very important to make things easier for the user.\n\nScenario: Configuration of the language used.\n\nIf I set the language in KDE why dont that have effekt when I login through the console / telnet / ssh?  Why does XFree have its own language settings and KDE its own,  and so on.. \n\nI would like to be able to set the language and the next time i login XDM / KDE / GNOME / ssh  / rlogin / telnet / console  I would like to have my prefered language.\n\nThere are numerous other examples like this. If we want better usability this is one BIG issue. I would like to reduce the number of places I have to configure to achieve the same thing in different domains.\n\nOne way of achieve this is to create a library for setting/reading this kind of information that could be used by KDE / GNOME / console apps.\n\n/Mats\n\n"
    author: "Mats"
  - subject: "As a parent..."
    date: 2003-02-21
    body: "I have a 3 year-old, give me the ability to do the following to her account:\n\n- Prevent desktop icons from being deleted, moved, or \"right-clicked\".\n- Permit a desktop icon to be clicked once to fire off the program.\n- Prevent \"start menu\" items to be moved, clipped, pinned, or deleted.\n- Prevent applets to be moved/added to the status/control bar.\n- Prevent commands to be \"run\" from the menu.\n- Prevent \"right-cliking\" anywhere including any task bar items.\n- Prevent screen saving while she is logged on.\n- Prevent screen locking while she is logged on.\n- Prevent any key-sequence/function keys to be active.\n- Prevent cut&paster anywhere.\n- Prevent any Terminal to be used.\n- A few more that I can't think of.\n\nAnd the clincher: I have to be able to disable this mode and re-enable it via a complex key-sequence.  \n\nSo, I guess the concept of User Desktop Profile Templates that an admin can apply to new accounts, would be what I'm looking for too.\n\n:-)\n\nPat"
    author: "Anonymous"
  - subject: "Re: As a parent..."
    date: 2003-02-21
    body: "You know what?  I would keep your 3-year old as far from the computer and TV as possible.  Really.  :-)\n\nSomeone else might want to talk about where the KDE Kiosk mode fits into all this."
    author: "Navindra Umanee"
  - subject: "Re: As a parent..."
    date: 2003-02-22
    body: "Maybe you want to let her use \"BOB\". See http://toastytech.com/guis/bob.html\n\nFor the rest you can also use WindowMaker, remove all icons, add those for the Potato Guy etc. and lock them."
    author: "Daan"
  - subject: "Re: As a parent..."
    date: 2003-02-23
    body: "This is easy, remove the power cord whenever she is near.  Infact just keep her well away, unless you particularly a sticky keyboard and juice in your motherboard."
    author: "Anonymous"
  - subject: "Re: As a parent..."
    date: 2005-09-23
    body: "1. For lockin of panel u may visit the below site. This will help u to solve ur first 5 queries.\n\nhttp://enterprise.kde.org/articles/korporatedesktop3.php\n\n2. For disabling left/middle/right mouse button u can diable in the configure desktop  program, so that it won't work on the desktop. This will help u to solve ur 6th query.\n\n3. The rest of the querys can be done by disabling function keys, and will let u know as soon as i find it out, b'coz i'm still working on it.\n\nbye!"
    author: "Vivek Biswas"
  - subject: "Opinion"
    date: 2003-02-22
    body: "I think some Usability work has to be done. there are too many ways to configure your desktop. I don't know KDE 3.1 yet but the control center was confusing."
    author: "Hakenuk"
  - subject: "Now we know what KDE really means..."
    date: 2003-02-22
    body: "Konfigurable Desktop Environment, joy! ;)\n\nPS: And I like it this way, thank you. =)"
    author: "Datschge"
  - subject: "Simplicity...Food for thought"
    date: 2003-02-22
    body: "I don't want to jump in with another \"this is what I want in a desktop\" post. Instead I want to make an observation. Of all the people I've met with computers, most are compete novices. They have some Compaq they bought at Sears or whatever, and they know just enough about Windows to check their email. Many are running 16 or 256 bit color at 640x480 because they don't know they can change it, and their systems are way out of date and full of security holes (they're probably hosting a half dozen trojans without knowing it too). They go to work, where their limited computer knowledge allows them to use Word, Outlook and Access. And, they consider themselves successful computer users. These are not the people who will be represented in this discussion, but the environment that reaches them will be the de facto standard Linux desktop. When this happens, the Linux power-user will suddenly find himself in the minority, but it must happen for free software to really hit critical mass.\n\nIt's aggrivating enough under Windows to have some app ditch the standard interface in favor of some pretty home-brew gadget set. Although you can completely re-skin Windows, only a very small percentage of users do it (although Microsoft seems to like to mix it up without warning, could the XP scheme be any uglier?). It just doesn't promote efficient work. What KDE should have is a simple, functional (and attractive) interface that is the installed standard. If no one touches any settings, it should be no more complex than the interfaces Microsoft and Apple have spent millions researching & developing. \n\nI don't think it's the settings and features that are bad, it's the idea that consistency across Linux desktops is oppressive and evil. Eventually, a casual user must know that his functional knowledge of a platform will serve him well across the majority of the installed base."
    author: "Bryan Edewaard"
  - subject: "do both - - Like SWAT"
    date: 2003-02-22
    body: "SWAT interface for SAMBA has a nice approach for this...you can elect to just access the main congifuration page, which is a few items that set the configuration.\n\nOr, you can choose the \"full\" parameters page, and spend hours just trying to understand the options...this would be the expert view."
    author: "Beldin"
  - subject: "More is better"
    date: 2003-02-23
    body: "I use KDE daily and am more than astonished with almost every aspect of it.\nI also agree with Mosfet, the day configuration options start dissapearing is indeed a dark one, when someone is forcing \"their way\" upon you.\n\nWhat I'd like to see configuration options for kcontrol itself, ie. allowing you to \"hide\" your choice of menus etc. (not to protect them, but to make them less easily accessible for the earlier mentioned 3yr old etc)\nBut, this should never be default behaviour, when we start piling up stuff in an \"advanced mode\" then you will have 2 defaults which fit noone, a newbie user will always want to change certain \"advanced\" features, and the advanced mode will start to fill with more dangerous and mostly useless options.\n\nMy opinion is: nothing should be hidden from the user by default, but a more experienced user should be able to hide certain options from them and make relevant options more visible.\nIf I want windows I will use windows, right now I am more than happy with KDE and its extensive configurability and so are everyone else I've talked to, newbie or pro.\n\nSame goes for you lot who seem to want windows/gnome; pipe down or use something else."
    author: "Funklord"
  - subject: "good article"
    date: 2003-03-03
    body: "good article by mosfet, which i agree with. And good comments above - seems to be a 50/50 divide between more configurability versus a cleaner UI.\n\nThe newly introduced Kiosk mode for KDE sounds promising, especially for locking down features so that your resident 5 year old doesn't mess up your desktop.\n\nHowever, i agree with an above poster about Theme Management - it's quite frankly, a mess and too scattered around the place. You've got fonts, colors, icon sets, styles, Window decorations , Mosfet's Liquid, blah blah blah.\n\nA single theme control center would be VERY useful for easily configuring the look of your desktop.\n"
    author: "bopeye"
---
One of the oft-recurring debates on KDE mailing lists is, how configurable should
the KDE desktop be?  With recent indications that GNOME seems to be heading
in the "less is better" direction, independent KDE developer
<a href="http://www.mosfet.org/">Mosfet</a> has written an <a href="http://www.mosfet.org/configurability.html">editorial</a> (<a href="http://www.mosfet.org/free-software-rebuttal.html">related article</a>)
urging why KDE should not follow suit.  Anyone else have an opinion on this?
&lt;grin&gt;


<!--break-->
