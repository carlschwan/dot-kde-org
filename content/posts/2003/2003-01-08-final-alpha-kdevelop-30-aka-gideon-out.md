---
title: "Final Alpha of KDevelop 3.0 (aka Gideon) is out"
date:    2003-01-08
authors:
  - "ctennis"
slug:    final-alpha-kdevelop-30-aka-gideon-out
comments:
  - subject: "Gideon problems"
    date: 2003-01-08
    body: "First let me say what a brilliant looking piece of software this is. I really like the 'ideal' interface which keeps all the periphery windows & trees nicely out the way until they are needed.\n\nI am having a couple of problems though. The first (and biggest :-( ) is that I cant get the kde simple application wizard to make a project that wil actually compile. I just get a message saying \n\nmake: *** [install-recursive] Error 1\u00a0\nmake: Target `install' not remade because of errors.\n *** Exited with status: 2 ***\n\nI also get an error that says /home/myhome/myproject/src/myproject/ is a directory.\n\nI am still a relative newcomer to c++ so I struggle to sort out compile errors when things go awry. In the old kdevelop (2.1x) , the project wizard used to run and build first time without problems.\n\nThe other thing I am curious about is how to get code completion working? Is it still only for your own classes or can it do qt etc classes too?\n\nThanks for a brilliant software package!"
    author: "Tim 'the linux junkie'"
  - subject: "Re: Gideon problems"
    date: 2003-01-09
    body: "Same here -- I'd love to give it a good workout but have run into all sorts of problems with basic tasks like project generation, configuring and building and modifying the toolbars. I've used KDevelop for years, so it's not like I'm unfamiliar with how things are supposed to work.\n\nNo, I haven't filed bug reports (yet) because I've been running CVS, but now that they're moving towards beta I'll definitely let them know. Besides, I need to file the wishlist report asking for the return of KDE-Mini!"
    author: "Otter"
  - subject: "Re: Gideon problems"
    date: 2003-01-09
    body: "Yes, unfortunately we recognize that the toolbars are a mess.  I'm hoping we get it cleared up by the next beta release.  If you have any bug reports, by all means file them or post them to the mailinglist.\n\nHey, we could use someone to fix project generation.  Volunteer? :)"
    author: "Caleb Tennis"
  - subject: "Re: Gideon problems"
    date: 2003-01-09
    body: "Do you have more error information for your error?  I think it should give a little more information about what the cause was\n\nAs for the : /home/myhome/myproject/src/myproject being a directory - did it create /src/myproject?  I believe that the binary target is called \"myproject\" if that's also your project name, so it's probably not able to create that binary since a directory is in the way.  Perhaps removing that directory will help."
    author: "Caleb Tennis"
  - subject: "I miss some of 2.x's features"
    date: 2003-01-09
    body: "3.0 doesn't have some of the features 2.x had and I can only hope they'll make their way back into the program.  Namely:\n1. The \"KDE Mini\" project choice is gone.  Trying to remove stuff from 3.0's \"Application Framework\" project to get it to a mini-like state has given me problems with \"make\" and removed files.\n2. There's no \"Switch header/source\" choice in the editor anymore.\n3. The \"New Class\" isn't integrated with 3.0's class manager or file manager like it used to be.  And I can't see how to use \"New Class\" to put a class in a subdirectory.\n\n\nAlso the file manager seems to be neutered in favor of the automake manager when it comes to adding and removing files, which I'm willing to concede as a design choice and something I may just have to get used to, along with the way system libraries are added.  Actually, in making sure the points above were sound, I found out how to do some of the things that I was unsure of how to do before when attempting to switch over.  I may use 3.0 primarily from now on.\n"
    author: "Mike K. Bennett"
  - subject: "Re: I miss some of 2.x's features"
    date: 2003-01-09
    body: "* F12 is still there but when you use kate it will be mapped to \"Enable Wordwrap\", so just set that action to another key and you can switch source/header again. I already complained in the kate mailing-list, it will be reverted to shift-F12 so that F12 can be used for switching again.\n\n* yes, I also miss KDE-Mini :) It's on my TODO to add."
    author: "HarryF"
  - subject: "Re: I miss some of 2.x's features"
    date: 2003-01-09
    body: "Is there any documentation on adding new template types? I have a properties dialog plugin I've written for a new dot tutorial I'm working on that could make a decent template.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: I miss some of 2.x's features"
    date: 2003-01-09
    body: "no documentation, the templates are in parts/appwizard"
    author: "HarryF"
  - subject: "Re: I miss some of 2.x's features"
    date: 2003-01-09
    body: "First I must say I really like the new KDevelop, i also miss features especially the number three in the list above (New Class) and that one is really annoying ! "
    author: "Bj\u00f6rn Sahlstr\u00f6m"
  - subject: "Re: I miss some of 2.x's features"
    date: 2003-11-05
    body: "3) Is solved by :\n\n- opening the Automake manager and clicking on the directory that you want to add to.\n- then in the lower window, right click on the library\n  - select 'Make Target Active'.\n\nI know I'm a little late but these posts always seem to show up in Google.\n\n\nAlex\nhttp://www.alexharford.com"
    author: "Alex Harford"
  - subject: "Some bugs"
    date: 2003-01-09
    body: "See bug report http://bugs.kde.org/show_bug.cgi?id=52788. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "finally.."
    date: 2003-01-09
    body: "I can tick the 'start in external terminal' in project run options.  Was this ever possible with the previous generation (2.1) ? I never realized if it were there. Did try all sorts of hacks - even saying that run it with konsole -e <program>, but no.."
    author: "jmk"
  - subject: "Little Too Early, Methinks"
    date: 2003-01-09
    body: "Wouldn't it make more sense for people to wait until Gideon is in beta before recommending that people try it out?\n\nAlpha-quality software is usually so bleeding-edge that people puncture themselves just be touching it!"
    author: "Anonymous"
  - subject: "Re: Little Too Early, Methinks"
    date: 2003-01-09
    body: "Even if I reported 19 bugs some hours ago, I think Alpha 3 is usable, and people (mostly experienced ones) should try out, and report bugs, help with patches, so a beta will be even more stable, and may arrive faster. BTW, I would recommend instead of Alpha3 the CVS HEAD, and watch the commits for kdevelop, so you need when you should upgrade. ;-)\n\nAndras\n\nPS: I'm using Gideon for Quanta Plus developement since Alpha2, and altough sometimes I'm angry on it, in the majority of the cases I think it's already better than KDevelop 2.1. Just fix the reported bugs..."
    author: "Andras Mantia"
  - subject: "Re: Little Too Early, Methinks -not"
    date: 2003-01-09
    body: "Stability wise its realy a beta (as moast other projects would have labeld it), but\nsince they still add losts of features the correct term is alpha. Besides me thinks\nnearly all the developers use Gideon to develop Gideon, wich is a great motivation to keep the core features stable. Far less of \"I know there are lots of bugs, but\nI just have to add these nifty features first...\" witch haunt lots of other projects.\n\n"
    author: "Morty"
  - subject: "talking about alphas, betas and bugs :)"
    date: 2003-01-09
    body: "So true, so true.\nActually i think both the KDE, Koffice and Kdevelop projects should let this sink in.. i mean.. features are cool.. but when a project has become as big as this one, with a userbase that is so large, the programmers need to develop a bit more deciplin when it comes to just this. I know that bughunting is no way near as much fun as incorporating the latest ultra-spiffy features, but most [production oriented] users tend to evaluate software like this (just repeating what everyone knows) :\n\na: ***Stability***\nb: **Performance**\nc: ... features?\n\n So.. *major* kudos to those who spend most of their time hunting bugs!!!\n\nSurgestion: \n Could we have like a \"tag-wall\" where bughunters could pit their \"stats\"? Id love to have a http place where i could see what kinds of bugs the warious hunters prefer to hunt so i can hint \"my favorite hunter\" to the bugs that annoys me the most ;) Or a bunch of silly graphs which shows both what individuals are doing (aka The Penis Enlargement Graph) and which sections of code that has been fixed the more (aka The Group Hunter Graph).. and other stuff that would encurage this fine art/sport of BugHunting(R).\n\n If such a place was in fact made awailable, i would *gladly* donate a Dual Celeron 400@500 (including lownoise whather-cooling kit) to reward \"The Bughunter Of the Month\" or equivalent. (Others join in! Hardware, Cash, those sexy KDE-mechandises, anything good!)\n \n In other words.. lets make it as at attractive as possible to be a \"full time\" bughunter... lets give them some more status and glory. If we are good enough at it we might even attract bughunters from other projects where they only get their humble names mentioned in the cvs changelog.\n\nThats just my two cent..\n\n/kidcat"
    author: "Anders Juel Jensen"
  - subject: "Re: talking about alphas, betas and bugs :)"
    date: 2003-01-09
    body: "From this you can see what Caleb Tennis have been dooing the last month :-)\n\nhttp://bugs.kde.org/weekly-bug-summary.cgi?tops=30&days=31\n\n"
    author: "Morty"
  - subject: "Re: talking about alphas, betas and bugs :)"
    date: 2003-01-09
    body: "Just watch the bugzilla archive, I think Caleb should be pretty high in closing bugs, followed by me and falk. The number of closed bugs is not that easy to determine, I spent two days fixing a weird dangling pointer problem in the classview which caused sporadic crashes, while other bugs are fixed in a matter of minutes."
    author: "HarryF"
  - subject: "Re: talking about alphas, betas and bugs :)"
    date: 2003-01-09
    body: "\"I know that bughunting is no way near as much fun as incorporating the latest ultra-spiffy features, but most [production oriented] users tend to evaluate software like this (just repeating what everyone knows) :\n\na: ***Stability***\nb: **Performance**\nc: ... features? ......\"\n\nLike Windows ... ? :-) I don't think so."
    author: "Mauricio Strello"
  - subject: "Thank you"
    date: 2003-01-09
    body: "I bow my head in deep admiration for the great people who have put this together. Thank you! There must have been many a man hour inside this thing. (I have not used this before, and not found an IDE i like since Borlands but it looks like that may have to change.)"
    author: "Jonas"
  - subject: ".spec file creation"
    date: 2003-01-09
    body: "Is it now possible to automatically create .spec files with KDevelop for projects (in order to create RPM package of the project)? \n"
    author: "Bojan"
  - subject: "Re: .spec file creation"
    date: 2003-01-09
    body: "Submit a wish on bugs.kde.org :)\n\ncheers,\nFrans"
    author: "Frans Englich"
  - subject: "Documentation"
    date: 2003-01-09
    body: "Hi\nI've already posted a feature request, but here goes anyway. I've been using KDevelop sinec kde 1.x and love it. My favorite aspect, though, is the automake management: in which I can make sub projects which are compiled as libraries, have their own makefiles and as such, can install their headers or whatever to wherever I want. I can do this all by right clicking on the file tree. This is amazing -- also critical for some of my work (which involves many home brewed sub libraries, plugins, etc) -- but I can't figure out how to do this in gideon. Seriously! I've tried hard.\n\nIs it possible at all? I imagine it would be, the program would be hopelessly crippled without it. So what I'm asking for, is either:\n\n - a tutorial for migrating kdevelop 2.x users.\n - a general purpose set of documentation. Not c/c++ docs, but \"how to use this big, honking, featureful and not entirely obvious-how-to-use program\".\n\nI'm not griping here -- gideon looks like it's shaping up. But the fact is, I *understand* kdevelop 2.x, and try as I may, I don't understand gideon. Sure, I've made projects in gideon, and even done some development. But basic usage is one thing ; serious, fulltime usage doing complicated things is another altogether. That's a wall I've hit with alpha 1, alpha 2, and I imagine alpha 3 when I give it a whirl. Every time, after wasting an evening, I sigh and go back to 2.x; after all I have code to write!\n\n"
    author: "Shamyl Zakariya"
  - subject: "Re: Documentation"
    date: 2003-01-10
    body: "...I have to agree."
    author: "zyzstar"
  - subject: "Re: Documentation"
    date: 2003-05-04
    body: "Thats EXACTLY how I feel.  I've tried alpha3 and alpha4a.  The projects compiles fine when I first created.  Then I add one subproject and it fails to compile."
    author: "Brad"
  - subject: "Is there a mdk 9 binary available ?"
    date: 2003-01-10
    body: "I cannot get kdevelop 3 alpha to compile. I cannot install the other packages. I'd really like to try it."
    author: "vm"
  - subject: "looks nice but"
    date: 2003-01-12
    body: "its always crashing when i want to create a new project or open an existing one ... \n\n\n... konstruct is great!"
    author: "guest"
  - subject: "Problem /solution in kdevelop 3.0a3 for suse 8.2"
    date: 2003-05-18
    body: "In parts/cppsuport/cppsupport.h\n\nyou must change #include <strstream.h> with <strstream>\n\nsuse 8.2 \n\n\n"
    author: "Diego Saravia"
---
The KDevelop team is proud to announce the third and final alpha release of <a href="http://www.kdevelop.org">KDevelop 3.0</a>.

Sources can be downloaded <a href="http://download.kde.org/unstable/kdevelop-3.0-alpha3/src">here</a> and binary packages will be available soon from 
<a href="http://download.kde.org/unstable/kdevelop-3.0-alpha3/">here</a>.
Since the previous alpha release almost all known crashes have been eliminated, many bugs have been fixed, and an integrated <a href="http://developer.kde.org/~sewardj/">valgrind</a> part has been added.  All users of earlier versions of Gideon are encouraged to upgrade, and KDevelop 2.1 users are also encouraged to try Gideon out.
<!--break-->
