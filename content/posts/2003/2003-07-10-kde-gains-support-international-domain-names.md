---
title: "KDE Gains Support for International Domain Names"
date:    2003-07-10
authors:
  - "tmacieira"
slug:    kde-gains-support-international-domain-names
comments:
  - subject: "Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-10
    body: "kah-d\u00e4h.\u00e4h ;-)\n\nKidding aside. It's good to see new technology implemented that fast\nin KDE. Open source proves once again to be where the innovation is.\nIMO the Greeks will profit the most from that within the Union. A German\nor french or swedish word can be read without those special letters\nand we've got used to it anyway from crosswords and awful support for\nour language for years on various electronic devices. But people which language\nhas completely different letters will certainly be glad they can at last have\nnative domain names. And chinese people can now put a whole story\nin the URL bar... So cheers to them. Keep up the good work, KDE team.\n"
    author: "Jan"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-10
    body: "Actually, french people do not like very much not using accents...\nso I imagine from now I will have to do horrible things with my keyboard to access   french websites ;)"
    author: "ParideP"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-14
    body: "> ... I imagine from now I will have to do horrible things with my keyboard \n> to access french websites ;)\n\nThat was also my first thought... I think this could become a real problem \nfor domains in more exotic languages. \n\nAnd I think it'll be quite some source of confusion because \nthere may be a version with and without accents or \neven mixed cases that do not necessarily belong to the same \nperson."
    author: "cm"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-16
    body: "Heya,\n\nFor what it's worth, I'm French but I don't like using accents for Net purposes. I think it just makes things more confusing. *g* Hopefully the use of international scripts won't make things difficult for people with a different keyboard though..."
    author: "Random Ribbit Person"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-10
    body: "How is a non chinese person going to visit a chinese website. all those strange characters are not on my keyboard. granted, i can't read chinese let alone write it but the internet should be open to everyone"
    author: "Mark Hannessen"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-10
    body: "Use a program for chinese input.  For example, xcin and fcitx accept input in PinYin.\n"
    author: "Robert Klein"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-10
    body: "you can have two domain for a single server : a full-ASCII domain name, and an international domain name."
    author: "capit. igloo"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-11
    body: "Just type in the unicode character... code. Alt+00+code_number. "
    author: "Anonymous"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-11
    body: "Oh wait... does the Alt trick even work in unix!?!? I hope I haven't just made an ass of myself. "
    author: "Anonymous"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2003-07-11
    body: "It doesn't (at least for me), would be a nice feature request tho. ;)"
    author: "Datschge"
  - subject: "Re: Hevorragend! Ich reserviere schon mal..."
    date: 2004-12-20
    body: "and how is a chinese person supposed to visit a website written in ascii - i.e. american encoding? is it a bit much to expect every computer user on planet earth to learn the roman alphabet?\n\nokay, so their keyboards probably have ascii letters written on them anyway, and some chinese input methods actually rely on a knowledge of roman letter phonetics to work, but even this is a symptom of past narrow-mindedness on the part of the dominant american technology makers. technological development seems to be shifting to the far east - let's hope the chinese developers are more thoughtful or else we'll all have to learn mandarin pretty soon!\n\nthe vast majority of websites visited in the world are clicked-through hyperlinks rather than typed in anyway, and even if it does have to be typed in, there'll probably be an ascii alternative domain name."
    author: "David Wilkinson"
  - subject: "Support throughout KDE?"
    date: 2003-07-10
    body: "And how is the support for these new URL's in other KDE applications? I mean, it's nice that the website can be reached, but can I also reach it's webmaster using KMail? Can I read their newsgroups in KNode? \nWhat kind of changes does this need?\n"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Support throughout KDE?"
    date: 2003-07-10
    body: "As for the SMTP kioslave, that one should be IDNA-ready for months now.\nNote, however, that IDNA is not about localized mail addresses. Those are currently in the prcesss of being discussed in the IETF. Search for \"IMAA\".\n\nTo answer your question: Yes, you can send mail to webmaster@<funny domain name>.org, but not to <funny local>@normaldomain.org.\n\nMarc"
    author: "Marc Mutz"
  - subject: "Re: Support throughout KDE?"
    date: 2003-07-10
    body: "That depends if the application doesn't do anything funny with its URLs. The support is inside KURL and our DNS lookup functions. As long as the application doesn't try to do the lookup by itself and as long as it keeps the URL in Unicode, there should be no problems.\n\nOf course, some protocols like SMTP and HTTP require the hostname being accessed to be transmitted over the wire. That's a protocol-specific implementation and has to be checked on case-by-case basis. HTTP and SMTP send the Punycode-encoded hostname, but other protocols could just send it in UTF-8 if they felt like doing so.\n\nAnd while kio_smtp has been made working and I believe the other ioslaves are good to go, I'm not sure about KMail itself. I mean, it wouldn't stop you, but it's certainly nicer to see the properly decoded domain name in the KMail/KNode panes than the ugly Punycode domains."
    author: "Thiago Macieira"
  - subject: "ie"
    date: 2003-07-10
    body: "it seems ie supports this, too, at least mine does.\n\npart from that, is there a place where you can already register .com and .de-domains with those special characters?"
    author: "me"
  - subject: "Re: ie"
    date: 2003-07-10
    body: "Currently you can't register such .de domains, see http://www.denic.de/doc/faq/domainregistrierung.en.html#r0011"
    author: "Olaf Jan Schmidt"
  - subject: "libidn"
    date: 2003-07-10
    body: "I was pleasently surprised that KDE has only a run-time dependency on libidn for this feature."
    author: "Anonymous"
  - subject: "xslt"
    date: 2003-07-10
    body: "Will khtml support xslt? Its actually coming into use on a few sites now. Its not going to go away.... \n\nAlso, will ksvg be integrated into khtml ( by a plugin if DOM handlers can be dynamically registered ? ) for inline svg? "
    author: "rjw"
  - subject: "Cool but..."
    date: 2003-07-10
    body: "If your site isn't only for the audience of your country, most people will have problem to get on your site (I'm talking about win-users that don't even know what is a character map).\n\nBut anyway I think it's nice because most of navigation those days is by links (and not like the old days when I wrote urls on my journal) so it should not be a big problem, and for brazilian sites it would be a welcome news being able to create urls like: http://www.a\u00e7\u00e3o.com.br/\n\nReguards."
    author: "Iuri Fiedoruk"
  - subject: "international"
    date: 2003-07-10
    body: "What I also like is what FFII.org does with internationalization. \n\nindex.en.html\nindex.de.html \n\nand so on. This is very convinient and I would prefer a web browser standard that automatically picks up a page in the correct language, so you don't need script or additional tools."
    author: "Bert"
  - subject: "Already exists!"
    date: 2003-07-10
    body: "Ever since 1.0, HTTP has had the Accept-Language header. The idea is that when a browser requests a file, it transmits this header with the language codes its user likes in order of preference, such as \"Accept-Language: fr, de, en-au\". It's then up to the server to decide what to do with the codes, and sadly most people don't configure their servers to pay attention to the header. (That's why you might't have heard of it).\n\nInternet Explorer and Mozilla have supported this for many years, and both have sections in their preferences where you specify the languages you want and their order. As I recall, Konqi has supported this for ages as well (but OTOH I forget where the pref is for selecting your languages)."
    author: "azza-bazoo"
  - subject: "Re: Already exists!"
    date: 2003-07-10
    body: "IIRC, it just uses the fallback sequence defined in the KDE-wide language settings.\n"
    author: "SadEagle"
  - subject: "Re: international"
    date: 2004-12-20
    body: "wouldn't it be better if everything was just encoded in UTF8?"
    author: "David Wilkinson"
---
Konqueror and the KDE base libraries in CVS now support domain names written with names outside the usual strict 7-bit ASCII letters. This means that <a href="http://www.verisign.com/nds/naming/idn/">one can now register</a> and access domain names written in proper letters for almost all languages in the planet, not just English.   <a href="http://www.konqueror.org/">Konqueror</a> is among the first browsers to support this new technology, developed in cooperation with <a href="http://www.verisign.com/">VeriSign</a> which has also been cooperating with the Safari and Mozilla teams (<a href="http://devedge.netscape.com/viewsource/2003/idn/">Mozilla IDN announcement and explanation</a>). 
<!--break-->
<p>The support was added into our base libraries several months ago, but last bugs were trimmed out only about a month ago.   In the mean time, the drafts provided to us have been made official Internet standards (RFCs) and domain name registrars are starting to sell domain names encoded in other languages.  
<p>
Although we have done basic tests with Konqueror and other base applications and the code seems to work fine, we are still expecting bugs to surface and intend to squash them quickly.  It's my expectation that we will fix everything by the time KDE 3.2 is released.

<p>The support currently requires the <a href="http://www.gnu.org/software/libidn/">GNU IDN Library</a> to be installed on your system. If that is so and you're using CVS HEAD, you can test the support at the following addresses:

<ol>
<li><a href="http://www.nunames.nu/eu-lang-test.htm">Examples of domains in the Latin 1 set</a></li>
<li><a href="http://www.nunames.nu/lldemo/default.htm">Examples of other language domains</a></li>
</ol>

Please note that KDE only supports the standards-compliant Punycode encoding. The IDN testbed led by VeriSign used another encoding (RACE) which has since been deprecated. Konqueror will therefore not work on those domains.