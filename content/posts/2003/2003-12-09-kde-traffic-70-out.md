---
title: "KDE Traffic #70 is Out"
date:    2003-12-09
authors:
  - "hpinto"
slug:    kde-traffic-70-out
comments:
  - subject: "KOffice 1.3"
    date: 2003-12-08
    body: "What happened to its release? It's still not tagged, another month delay?"
    author: "Anonymous"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-08
    body: "I heard that there were some critical bugs that needed fixing."
    author: "Navindra Umanee"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "To my mind a proper OpenOffice integration/ui separation would be more fruitful. There are scripts to change the icons of OpenOffice. More can be done to fix its horrible UI. \n\nSame apllies to KDE's paint program. Take a image viewer  and add plugins, this works better than the all in one approach. I don't think we will see a KDE-image program in the near future.\n\nKoffice as we know it will never make it."
    author: "Andr\u00e9"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Go for it!  Meanwhile the rest of us want to use snappy non-bloated software."
    author: "ac"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "> Koffice as we know it will never make it.\n\nIt might take years more, but it will =)\n\nI personally use OOo about 99% of the currently, mostly because koffice doesn't do everything I need. But i sure HATE _using_ OOo.  It's not a joy to work with. But it works, and that's what I need. Koffice 1.3 is quite improved, hopefully 2.0 will be kickass!"
    author: "anon"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "What KOffice needs the most is more developers. Much code for what it needs can be taken of other free software, it just needs to be \"ported\". For example, there is a lot of functions it can borow from Gnumeric. The same for the filters, code can serve as the format documentation.\n\nThere's too few people working in it if you think about how important an office suite is, but they're doing an amazing job. Just try a recent version and forget about the filters, you'll see what I mean. Maybe with the 3.2 release, when a *lot* of bugs are fixed and features implemented, more developers will dedicate some time to KOffice."
    author: "Penna"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Yes, we need more developers. (David Faure has written once about 30 developers.)\n\nHowever I do not think that taking them from KDE is good. I think that there is enough work in KDE too, especially when it will be Qt4/KDE4 time (at that time KOffice 1.4 will need plenty of work too.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-10
    body: "Perhaps it might be worth contacting Lycoris, Xandros, Lindows and other KDE using distros and ask them about developing the desktop they are using?"
    author: "David"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Perhaps the method abiword uses is good, a weekly digest about the latest improvements and the option to donate via penpal. However, Abiword is an example of buggy OSS. I never saw a stable release. Perhaps this originates from the amount of developers.\n\nMy opinion: Create small pieces and plug them in. Look how KDE-Look helped to improve KDEs appearance. What's really needed is a community."
    author: "Andy"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Not only KOffice would benefit from more developers, see this KDE Wiki page for more examples: http://kde.ground.cz/tiki-index.php?page=Developers+Wanted"
    author: "Anonymous"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "\n> I don't think we will see a KDE-image program in the near future.\n\nHave you seen kdenonbeta/kolourpaint? :)\n"
    author: "Clarence Dang"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Thank you for the encouragement! :-(\n\nAs for paint programs, well, two are coming:\n- Kolourpaint (as Clarence Dang has already pointed out)\n- Krita (in KOffice, but not for 1.3.)\n\nAs for a KDE integration of OO, well, I do not share your point of view that it would be easier than to make KOffice better.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-10
    body: "I'm a long time koffice user a watch its development with great excitement.\nKOffice _is_ the right way for KDE - but it is also a very long way.\nToday I can do most things with KOffice today and this is KSpread, KPresenter and KWord. When I need MS-Office compatibility OOo is not as good as some people tell you. If a doc is a little bit more complex than usual the im- or exporter make nasty errors.\nBut when I do not need this kind of compatibility I prefer KOffice: its integrated, small and fast. OK, it needs some important features for business usage (programable functions, a script language for all apps (kjs(?), set form mode, database connectivity, better printing support (kword->pdf generator from scribus?))\n\nThe main office apps need some time. Did you all remember the time 5 years ago, when Torben and Reggie published the first versions of kword?\n\nGo  KOffice!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "> Same apllies to KDE's paint program. Take a image viewer and add plugins, this works\n> better than the all in one approach. I don't think we will see a KDE-image program in \n> the near future.\n \nUm... You've never tried to create a paint application, have you? Trust me on this, this cock won't fight. \n\nAnyway, Krita now can paint using gimp brushes again, and in glorious technicolor, too. Krita's architecture is pretty solid by now, maybe buggy in parts, but nothing that's really wrong. It even works fine on powerpc. All that needs to be done is polish the user interface and create the necessary tools. And maybe plugins for image effects, too. (One tip: don't try to make Krita work with the Gimp's plugins. That's a dead end that's been tried before.)"
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Open Office, for what it does from a user perspective, does a reasonably good job. However, I've tried to look over the code and it's structure and it is pretty terrible - and that comes from someone who is not a massively experienced programmer. At least KDE code seems pretty interchangeable from what I've seen. I think it will take some major work to do anything with Open Office (i.e. port it to other toolkits etc.) and integrate it with different environments. Linus Torvalds, well-rounded guy that he is (in an intellectual sense!), has already said that it is pretty terrible.\n\nI wish people would stop this talk regarding Open Office being used in other environments (\"Why don't you just reuse?\" questions), because it has never been designed, and probably never will be, to do this from the ground up. This is not as easy as it might appear at a casual glance. It would be lovely if we could have Open Office plugins, nicely separated, so different UIs and apps could reuse them. Alas, this looks as if it will never be the case. The much vaunted GTK/Gnome integration is a pipe-dream, promoted by many, because Open Office has gone, and is going, too far down another path. I also hate the way that bit and bobs such as language support, like Java, is effectively being stuck on to the existing codebase with double-sided stick tape, rather than thinking about the project as a whole. When people try and get Open Office to work with more external bits of software and environments in the future they're going to have some pretty severe problems.\n\nThe Ximian port looks as if it is merely some ad-hoc hacking to get it to work with GTK and the Gnome facilities and infrastructure. Get a new version of Open Office and the whole process starts again. This is hardly reuse and is not really what Open Source software should be about.\n\nAs far as I'm concerned KOffice has arrived. I like KWord, and KSpread is quite a bit better then the OO Calc. Touches such as those nice blue triangles in the bottom corner of cells to let me know that the contents of those cells are the result of functions has told me instantly where the real innovation lies. This might sound silly, but it really is useful when you are lost in a spreadsheet full of cells and functions. For more complex stuff there may be some obstacles, but for everyday 'spreadsheeting' it really shines.\n\nAs far as integration goes, I think the Oasis forum is the place for this to be done. There is already the makings of a good standard file format, and hopefully there will be, or is, dialogue on how components can be embedded in documents that do not assume any particular technology or language. An XML description framework for this may be a good idea, I don't know.\n\nIntegration through standards rather than what people consider to be 'standard technology' (think MS Office) is the way forward."
    author: "David"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "As the one who coded that blue-triangles-on-the-corner (the buzzword for that is formula indicator), I'd rather say that it's really not pure innovation. That was sort of feature request, after Quattro Pro. But, nevertheless, it still pleases me if people like it :-P"
    author: "Ariya Hidayat"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-10
    body: "\"(the buzzword for that is formula indicator)\"\n\nThat's the one!\n\n\"I'd rather say that it's really not pure innovation.\"\n\nDon't be too harsh. It is small things like this that make the difference. I know spreadsheets have done this sort of thing before, but reusing good not immediately-obvious-ideas is great."
    author: "David"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Have you seen the roadmap of OpenOffice 2.0 (http://tools.openoffice.org/releases/q-concept.html)\n\nI think that the real integration with gnome is going to happen there."
    author: "Santiago"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-10
    body: "Making Open Office a real Gnome app is going to take forever. The changes are basically the stuff Ximian has done now, nothing more. I don't see scripting available for Miguel's baby, Mono, I don't see real hard-core support for Bonobo etc. The direction certain people want to take Gnome, coupled with others, makes Gnome very vulnerable especially considering the 'Gnome board' approach that they have."
    author: "David"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Yes, bug 66142 ( http://bugs.kde.org/show_bug.cgi?id=66142 ) makes that KOffice cannot be used with KDE 3.1.4 on Qt 3.2.x. (The bug unfortunately happens for KOffice 1.2.x too.)\n\nSo either somebody (finally) finds a fix for this bug or we have to wait on KDE 3.2 final, whatever would come first.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Qt 3.2.x was IIRC never the recommended version for KDE 3.1.x. And if happens with KOffice 1.2 too where would be the degradation when KOffice 1.3 would be released?"
    author: "Anonymous"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "As far as I know, KDE 3.1.4 was released because people wanted it to work on Qt 3.2.x. Also bug fixes are done to Qt 3.2.x, not to Qt 3.1.x anymore. That is why more and more people prefer using Qt 3.2.x.\n\nAs for what degradation, well, when KOffice 1.2 was released, it worked. If we would release KOffice 1.3 today, it would not. That is the degradation.\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "> So either somebody (finally) finds a fix for this bug or we have to wait on KDE 3.2 final, whatever would come first.\n\nUntil then I suggest to create a KOFFICE_1_3_BRANCH and tag KOFFICE_1_3_0_RELEASE later within and start with KOffice 2.0 in HEAD today. Or is there any other reason to delay the start of KOffice 2.0 development over two months other than this single bug?"
    author: "Anonymous"
  - subject: "Re: KOffice 1.3"
    date: 2003-12-09
    body: "Well KOffice 1.3 has still plenty of bugs, so the Christmas/New Year period can well be used to fix a few of them.\n\nThen next year we can start with KOffice 1.4. (When next year is another question...)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "thanks henrique"
    date: 2003-12-08
    body: "That was a good Traffic, thank you.  :)\n\nThe sysadmin link is very interesting!\n"
    author: "ac"
  - subject: "apps.kde.com"
    date: 2003-12-09
    body: "We really need this website, it is central for many KDE users like me and contains just about every Qt/KDE application there is, it is very useful.\n\nThe site also had ratings which were quite helpful. If it isn't coming back up, we definitely need a new one. Maybe one that is a lot better, one like download.com that allows comments too and also hopefully all kinds of ordering like by date, downloads, rating, size, category, KDE version, etc."
    author: "Alex"
  - subject: "Re: apps.kde.com"
    date: 2003-12-09
    body: "Yes, I agree that the site is needed. If it really won't come back, then we desperately need a replacement. Unfortunately, I don't have enough resources/time to create one myself. If someone can provide us a server for hosting and if someone wants to start a replacement site (apps.kde.org?), I'll be happy to help, but I can't do that alone."
    author: "Henrique Pinto"
  - subject: "Re: apps.kde.com"
    date: 2003-12-09
    body: "Isn't it a bit too early to talk about replacement? I mean, it's been offline for what, two weeks? I don't know the situation - maybe the guy is on vacation or something? Imagine coming back and your work has been made obsolete. Before we can think about *replacing* apps.kde.com, let's find out why it's down?"
    author: "Eike Hein"
  - subject: "Re: apps.kde.com"
    date: 2003-12-09
    body: "No, it is not vacation. The whole kde.com domain is unreachable (including http://www.konqueror.org )\n\nSee the archive ( http://lists.kde.org ) of the kde-www mailing list to read a little more about it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: apps.kde.com"
    date: 2003-12-13
    body: "See http://lists.kde.org/?l=kde-www&m=107099152930002&w=2"
    author: "Nicolas Goutte"
  - subject: "Re: apps.kde.com"
    date: 2003-12-11
    body: "I observed that when I go to kde.com a page by \"Domain Discover - Domain Registration Service\" appears.\nDoes this mean that the previous owners lost their domain?"
    author: "Joachim"
  - subject: "Re: apps.kde.com"
    date: 2003-12-11
    body: "No."
    author: "Anonymous"
  - subject: "Feature Requests?"
    date: 2003-12-09
    body: "I couldn't believe what that guy wrote about Ingo. And a feature request for HTML mail?!!! What's the world coming to?"
    author: "David"
  - subject: "Re: Feature Requests?"
    date: 2003-12-09
    body: "Perhaps a Unified Wiki Language standard and supprt :-)"
    author: "Andy"
  - subject: "Re: Feature Requests?"
    date: 2003-12-10
    body: "That would be better."
    author: "David"
---
We have just released KDE Traffic #70, with lots of news for you. Topics include a new release of <a href="http://kolab.kde.org">Kolab</a>, <a href="http://kmail.kde.org/">KMail</a> performance improvements, <a href="http://www.koffice.org/">KOffice</a> toolbar icons (again!), <a href="http://www.kde.org/areas/sysadmin/">administering KDE</a>, apps.kde.com and more. Check it out at <a href="http://www.kerneltraffic.org/kde/kde20031207_70.html">the usual place</a>.
<!--break-->
