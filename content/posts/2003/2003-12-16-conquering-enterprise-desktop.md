---
title: "Conquering the Enterprise Desktop"
date:    2003-12-16
authors:
  - "numanee"
slug:    conquering-enterprise-desktop
comments:
  - subject: "Debian Desktop Project"
    date: 2003-12-15
    body: "How does this relate to the Debian Desktop Project (http://wiki.debian.net/index.cgi?DebianDesktop)? Is there a cooporation?"
    author: "Anonymous"
  - subject: "Re: Debian Desktop Project"
    date: 2003-12-15
    body: "Thanks for pointing that out.  We're still having contacts flooding in, but yes this is definitely one project we will want to collaborate with."
    author: "Navindra Umanee"
  - subject: "Damn!"
    date: 2003-12-15
    body: "Would that just not be the coolest thing ever? A Debian-based distro with KDE-based configuration tools, and good commercial backing?\n\nThis certainly makes me feel a whole lot better about the whole Novell/Sun/GNOME thing. At least, it seems that KDE is willing to put up a fight in that market space.\n\nI wish all the luck to this project and UserLinux as a whole. These are really best-of-breed technologies, and it would be great if they became a significant force in the enterprise market."
    author: "Rayiner Hashem"
  - subject: "Re: Damn!"
    date: 2003-12-16
    body: "I'm glad to see some *positive* response on the KDE side to the UserLinux initiative.  UserLinux has chosen to focus their limited resources on a set of \"core\" applications, and KDE/Qt can be a nice add-on for those who want the ultimate best.  This is similar to the situation with Python and TK.  TK is no match for Qt, but it meets the needs of the Python project, lightweight and no restrictions on licensing.  Anyone with a big GUI project in Python will move quickly to Qt.\n\nUnix is at another crossroads, similar to what happened in the 80s.  I watched in astonishment as the different minor variations of Unix focused on their petty little war, while Microsoft took the entire market.   Let's not repeat that mistake as we go this last mile to the end user.  We need the best possible cooperation between KDE and UserLinux.  Stop the personal attacks.  Give users a set of tools we can *easily* install in Debian / UserLinux / Redhat / Whatever.  Go that extra mile to make sure all package dependencies are resolved.  Try it on some real users.\n\n-- Dave\n"
    author: "David MacQuigg"
  - subject: "GtkQt"
    date: 2003-12-15
    body: "A first step (hack? ;) has been made: the GtkQt and the QtPixmap engine for Gtk by Craig Drummond. I use it, and it works very well. You can change matching Gtk themes, fonts and colors from the KDE Control Center.\n\nhttp://kde-look.org/content/show.php?content=6954"
    author: "cwoelz"
  - subject: "Re: GtkQt"
    date: 2003-12-15
    body: "Unless I'm completely mistaken... work is being conducted that would allow GTK to draw its style directly from the KDE/Qt style... thus obviating the need for two separate themes (Keramik and Geramik) that look alike.  With this engine all you would need is Keramik.  Stay tuned."
    author: "manyoso"
  - subject: "Re: GtkQt"
    date: 2003-12-15
    body: "Well, _this_ is interesting! I would love that! I only hope that GTK apps do not get too slow!"
    author: "cwoelz"
  - subject: "Re: GtkQt"
    date: 2003-12-15
    body: "matching themes is a trivial and hardly useful step. this is probably why it's been done so much: it's easy and inconsequential.\n\nwhat is much harder, but much more useful, is allowing apps written to other toolkits to be able to use KDE's file dialog, network-transparent IO Slaves, etc... that's the sort of stuff that is being worked on. and when i use the word \"worked on\" i mean \"code being written\"."
    author: "Aaron J. Seigo"
  - subject: "Re: GtkQt"
    date: 2003-12-16
    body: "Sodipodi can be compiled with KDE support, and if so it uses the KDE file and printing dialogs."
    author: "Josep"
  - subject: "Re: GtkQt"
    date: 2003-12-16
    body: "Which is incredible easy and is looking nice on your desktop"
    author: "Fab"
  - subject: "Re: GtkQt"
    date: 2003-12-17
    body: "But what we need is to be able to install a theme as one package and have it theme everything.\n\nAt the moment, I need install maybe three packages, then go into kcontrol and choose the them, run gnome-control-centre and choose it again there, and do the same for the likes of xmms."
    author: "Jonathan Bryce"
  - subject: "What is # 2?"
    date: 2003-12-15
    body: "What is the admin tool? any info on it? I am curious as to the approach taken on it. \nthanx."
    author: "a.c."
  - subject: "Re: What is # 2?"
    date: 2003-12-15
    body: "admin tool_s_, and we'll all just have to wait until the authors are ready. KDE has never really been about announcing vapourware, and the wait won't be that long."
    author: "Aaron J. Seigo"
  - subject: "Re: What is # 2?"
    date: 2003-12-15
    body: "Hi all,\n\nI just want to pop my head up and mention that I've been working on some administration tools too. (also because I'm sick of the current state of affiars wrt distros).\n\nhttp://www.simonzone.com/software/guidance/\n\nThe user&group admin is pretty complete, also the Service level prog. I've been bevering away on a fstab admin prog lately.\n\nAaron and the other people involved, it would be nice if you people could let me know what your plans are in this area. I'm not interesting in duplicating any effort in this area, and I'd like to work something out. :-)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: What is # 2?"
    date: 2003-12-15
    body: "Simon,\n\nthat looks like a really great beginning! \n\nI agree about the essential need for stuff like that. We should stop to be \nat the prey of distro's (semi-)proprietary or half-baked and inconsistent \ntools which are slow to use. \n\nToo long we did let them go away with their say about \"keeping\nthe admin tools in our re-alm since it is too complicated for KDE to\nthink of all the distro-specific stuff\". \n\nHeck, I can't even set the IP address with a KDE tool!\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: What is # 2?"
    date: 2003-12-16
    body: "Wow! Looks fantastic! I've always wanted some KDE specific, and non-distro branded, config tools. Everyone says SuSE is \"seemlessly\" integrated with KControl - but this is rubbish. SuSE's tools are in their own section - why? And have the stupid \"YaST\", and gecko logo - hardly integrated, as they look nothing like the other modules. For a complete desktop OS (KDE + Unix/Linux) everything should be consistent, both UI wise and functionality wise.\n\nIs their any chance these, and Guarddog, could be converted to KControl modules?"
    author: "Craig"
  - subject: "Re: What is # 2?"
    date: 2003-12-16
    body: "If there was sufficient interest to take the stuff that I've written and integrate it all into a complete desktop, then yeah there is a very big chance that it could be converted into KControl modules. :-) I'm certainly not against it although I do prefer ArkLinux's way of organising a control center over the current KDE Control Center, but that's a different story. :)\n\nI just want complete and polished desktop too. That's my top concern.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: What is # 2?"
    date: 2003-12-18
    body: "Well I'd love to have them integrated. I agree Ark's Mission Control looks nice - my only concern with that is if the HTML pages are static, the problem would then be if you install a new control it won't show up!\n\nMaking your tools into KControl's would allow them to be used in both scenarios.\n\nAnd I share you concern about a \"complete\" desktop - *none* of the current distros offer this. There's allways KDE's control settings, and then the distro tools. Which doesn't give the impresion of an integrated OS."
    author: "Craig"
  - subject: "Re: What is # 2?"
    date: 2003-12-16
    body: "Aaron, while I appreciate not wanting vapourware, it is useful to still talk about things (or at least see things).\n Back in KDE 2.2, I developed a 3 tier approach to handleing admin on *nix (as opposed to pure unix, pure linux, pure bsd, etc). I suspect that I have done a bit of work that you may find useful. I was generating GNOME and KDE controls, and had the ability to easily move to a networked tool (think ksysguard). This was also working on many of /etc's files.\n\nIs there a page that has your work on it?\n"
    author: "a.c."
  - subject: "Re: What is # 2?"
    date: 2003-12-16
    body: "the majority of this isn't my work, i just happen to know about a lot of it... the stuff i'm personally working on relates more to thin client management and i'll probably be helping out with some LDAP related things. but it isn't like we'll have to wait long to see the fruits of everyone's labours; it seems January will be the month that a lot of this stuff surfaces in alpha/beta form and/or as design documents.\n\nfeel free to discuss further with me via email this 3-tier system you designed, though. i'm more than interested and would be happy to point people in the right directions to increase collaboration and involvement."
    author: "Aaron J. Seigo"
  - subject: "Re: What is # 2?"
    date: 2003-12-16
    body: "With respect to a 3 tier approach it might be good to talk to the authors of gnome-system-tools to see if they would be interested in that as well. It is intended to ship with Gnome 2.6 in a few months."
    author: "Chris Cheney"
  - subject: "Re: What is # 2?"
    date: 2003-12-17
    body: "There are at least three kde/qt (partly) implementations of frontends using the gnome system tools. I had started one, but never got more than the system chooser running, because of lack of time.The gnome system tool developers' were quite cooperative back than and would have appreciated a full KDE frontend implementation for their backends "
    author: "Joseph Wenninger"
  - subject: "KDE-Distro"
    date: 2003-12-15
    body: "Will this be a kind of KDE-Distro ??"
    author: "Marc Tespe"
  - subject: "Re: KDE-Distro"
    date: 2003-12-15
    body: "IMO: no and yes. when complete it will be a distribution of software that leverages KDE to its full potential (including raising the level of that potential), but it won't be \"the operating system of the KDE project\". KDE supports many, many different OSes, and this doesn't change that in the least. personally, i hope that this process will result in four things:\n\n1. show what KDE can be like, to its full potential\n\n2. provide a platform for third parties looking to build upon Linux/KDE for their clients whose needs aren't served by SUSE, Mandrake, Red Hat, etc... it turns out that there are more people in that category than i first thought.\n\n3. provide a case study for how the KDE project can work with the SUSEs and Mandrakes of the world more directly. right now that relationship is often quite passive, and that may not be the best thing for KDE or the vendor. the question is HOW to do it in such a way that it works; this may be a first forray into that realm.\n\n4. through a combination of 1-3, grow KDE's user base."
    author: "Aaron J. Seigo"
  - subject: "kioslave for apt"
    date: 2003-12-15
    body: "Check this out: http://kde-look.org/content/show.php?content=8966  It is a kioslave for apt that performs queries on the apt-cache from withing konqui..."
    author: "manyoso"
  - subject: "The key is collaboration"
    date: 2003-12-15
    body: "This is great news to hear. But it's really important to stress that only collaboration between KDE and GNOME will lead to success. It's that simple: \n\n1) No user will understand why he can't set the gimp's font from the kde control center.\n2) Users will be confused if half of their applications has OK/Cancel and the other half has Cancel/OK as button order.\n3) My KDE app has to dock into GNOME Panel's systray.\n4) ... much more \nAnd everything vice versa.\n\nIf both desktops collaborate, better solutions can be achieved in shorter time. (There is enough market share to steal from MS :-))\n"
    author: "Oliver Bausinger"
  - subject: "Re: The key is collaboration"
    date: 2003-12-15
    body: "That is the whole point."
    author: "cwoelz"
  - subject: "Re: The key is collaboration"
    date: 2003-12-17
    body: "Isn't docking to the systray standardised between kde and gnome already? I seem to remember gaim docking into my kde systray, before I started using kopete."
    author: "Joeri Sebrechts"
  - subject: "New Sponsor: LinuxMagic Inc"
    date: 2003-12-15
    body: "I'm sure they won't mind me quoting part of their email.  :)\n<p>\n<i>We, at <a href=\"http://www.linuxmagic.com/\">LinuxMagic Inc</a>, have adopted the KDE and Debian based strategies for\nall our LinuxMagic OS and product developments. Our LinuxMagic Workplace\nproducts especially show the alignment of our organization, and our beliefs\nin the statements expressed in your article entilted \"Conquering the\nEnterprise Desktop\"</i>\n<p>\n<i>Using Thin Client technology, KDE Desktop, Debian based systems, OpenOffice,\nour product is specifically designed to statisfy the requirements of the\ntypical office, whether small or enterprise scale..</i>\n<p>\n\nWoo!  Things are moving fast.  Juanjo has joined and already whipped up some new PyQt code.  Eyal whipped up some Qt designer interface examples....  keep them coming folks!"
    author: "Navindra Umanee"
  - subject: "some notes on produktivIT as well"
    date: 2003-12-15
    body: "<A href=\"http://www.produktivit.com\">produktivIT</a>:\n<p>\nTheir plans for contributions include:\n<p>\nIMAP Mail server configuration (Cyrus first)<br>\nSMTP Mail server configuration (Postfix first)<br>\nHTTP server configuration (Apache first)<br>\nSMB server configuration (the kdesamba-plugin project being currently dead)<br>\nProxy server configuration (Squid first)<br>\nSystem Authentication configuration (PAM first)<Br>\nUser Management extended to LDAP, Samba & SASL<br>\n"
    author: "Navindra Umanee"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-15
    body: "KDE has a ControlCenter and a InfoCenter. I think it\u00b4s time for a ServerCenter :)"
    author: "Marc Tespe"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-15
    body: "Please no! Splitting up the Control Center into two apps is a BAD IDEA. Reason: Joe user wants to change a setting in their mail server. Do they open kservcenter? No, they open kcontrol since it's familiar to them as the single opint of configuration ala MS Windows. It may make sense to you as a UNIX admin, but makes no sense to the MSCE who's been told that his boss wants the server room MS free, now."
    author: "Gary Greene"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-16
    body: "Joe user does not need kservcenter, if it needs a root password. He only needs the control center. The joe admin may need it, but keeping it separately makes sense. Hopefully he has enough sense to know what to use where if he wants to be an admin. The kservcenter could be called \"advanced settings\" or something.\n\nBTW why would a joe USER be allowed to change the settings of the mail server? That's the admin's job. Maybe if there are any allowable changes, these changes should be accessible through kcontrol.\n\nTo me at least it does make GOOD sense. This is not MS Windows.\n"
    author: "Rithvik"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-16
    body: "You are falling into the old UNIX mindset that all systems that have servers are only run by admins. If we follow this logic, we don't install KDE on the server, nor do we even install X11."
    author: "Gary Greene"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-17
    body: "This is not just useful for servers, it is useful for workstations also, the type that windows today is used in most offices today. Please don't fool yourself into believing that only windows-like workstations exist in this world. Windows workstations give a hell of a nightmare for security in administration, especially windows 9x. \n\nUsers frequently do something to their systems and it stops working (BSOD, viruses, corrupt registry or deleting other peoples files) and these actions are often not deliberate. I don't know how many times a win admin has to reinstall win98 on their workstations due to ignorant users. This is because they have access to system utilities that should normally be restricted (any one can download Xsetup and play with it). KDE should cater to those workstations which attempt to make the admin (and users) life better, where most of these problems don't crop up so often (unless the user is determined to cause trouble). \n\nSuch systems are more often setup than home pcs (which I assume is your idea of the general system). Besides, the topic is on the Enterprise Desktop.\n  \nUsers shouldn't be shown dangerous options. Leaving it around is not dangerous, since it requires root piviledges anyway, but it makes the common control center redundant. Shunt it. keep it out of the way (not inaccessible though) maybe in another similar app. Like I said once before (http://dot.kde.org/1071469854/1071499344/), presenting a user with a Login manager settings box which is entirely disabled (except for an admin mode button) makes this option totally redundant here for most users.  \n\nThey should keep it in a separate kservcenter or whatever which has most options with root priviledges. Since there is a clear reason to differentiate these options, and we can be pretty sure they are going to increase, we must catagorise and make place for them.\n\n"
    author: "Rithvik"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-17
    body: "Then let the admin hide those settings via kiosk mode or maybe, I dunno, kmenuedit? The problem is that you are thinking with the mindset that KDE will or should only be used in a workstation business type of environment. The idea of splitting this stuff out for home desktop systems is not very helpful for those of us trying to break *NIX into the home PC market. I understand the issues your speaking of 'cause for my day job, I'm an employee of the IT support department at Grand Valley State University. I see the kind of stuff that happens because Windows is too insecure. Does this detour GVSU from using Windows based systems? No.\n\nWhen competing for a market, sometimes breaking paradigms is good, however in the case of usability, people don't care how logical the system is as long as it's something they're comfortable. This is why I cannot get GVSU to use a *NIX based KDE solution for the labs, because the ease of use isn't there yet according to the department after doing an analysis of the environment."
    author: "Gary Greene"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-18
    body: "Since you mentioned kiosk mode, this is exactly what is useful in a workstation environment. \n\nWhere I am studying, the systems have all windows 98 (newer ones have XP). The only reasons they use Windows is that a large number of plugins and client softwares for bioinformatics (my subject) are available on it (not that there are no linux versions -- they have bought windows versions and windows too). There are some tools which have no clients on UNIX systems (esp. some homegrown ones). \n\nSplitting the Kcontrol center is simply a better way of organising the ever-growing kcontrol center. It is already unweildy now with a large number of options. At least some property should be chosen which can be used to categorise options, and I can think of no better. If it doesn't agree with you, I'm not going to try convincing you. Hopefully someone else is reading these comments too.\n\nTry imagining the Kcontrol center with all options -- with everything for tweaking the OS -- from hardware to themes to server options. Mail settings would hold Client settings and Server settings. As a non-priviledged user you start kcontrol. How would you feel if half the controls are disabled, and are all strewn across the applet? Maybe you are brilliant, but any other person would get pissed off seeing all these options, including the disabled ones lying around mixed with the ones that 'work'. Imagine him trying to figure out what is where, and which one is 'working'. \n\nHell, even a single-user system doesn't run root all the time. There is a clear distinction between the two types of settings. If you really don't want to see them separate, keep them as two icons in a Control Panel folder, like windows (Local settings and Global settings -- perhaps)."
    author: "Rithvik"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-16
    body: "Windows has the Control Panel and the Microsoft Management Console. What do you mean?"
    author: "David"
  - subject: "Re: some notes on produktivIT as well"
    date: 2003-12-16
    body: "It may have both, however, MMC is accesable from the Control Panel, sure the MSCE may load mmc.exe, however the most of the time, they'll just browse to the Control Panel for the Administrative Tools. Moving obvious comfiguration tools from the Control Center to a seperate app is poor usability and adds an inconsistancy to KDE. Mac OSX Server also has it's server tools accessable in it's control center, so please let's follow good paradigms in usability."
    author: "Gary Greene"
  - subject: "Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-15
    body: "HERE is his oh-so-well written rebuttal: http://www.userlinux.com/GUI.html\n\nSomeone mentioned to me who his secret \"1-million-$-a-year\" backers might be:\nSun, and maybe HP...\n\nSun is feeling increasingly uneasy to have underneath their mis-named \nJavaDesktop a SuSE-engine which now belongs to Novell! Not something they'd \nhave expected to happen when they signed the deal with SuSE.\n\nAnd Sun is quite experienced to utilize \"the community\" for their efforts (see\ntheir Blackdown/Java behaviour). Now, Bruce P., who are your backers? We want\nto know _before_ we start coding for you!\n\n"
    author: "Anonymous BruiseFan"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-15
    body: "Strange reply he made there.\nObviosly, it is better to have the possibility to develop with QT, since it has some very interesting features. So if GTK apps will be able to work well in KDE, then what's the problem?"
    author: "OI"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "I don't think that's any rebuttal to this. Indeed, I would say this may be a rebuttal to _his_ writing. His comments do not reflect even have the minimum hint of what's mentioned in this article.\n\nHe really condemns one of the desktops. KDE has a better option that may please even the Novell/SuSE team: lets make them work together. If they really want to do it, they have no better choice than this really."
    author: "uga"
  - subject: "Bruce lies for the money"
    date: 2003-12-16
    body: "I think it has become clear to anyone who has been on the userlinux mailinglist recently that Bruce lies and tries to enforce his agenda by dictatorial means\n\n** Bruce Perens doesn't reveal the names of his sponsors which seem to be clouded in secrecy. Who would ever work on a project where you don't know who the instructing party is? \n\n** Bruce Perens doesn't reveal the requirements specification which obviously does exist and which ties the 1 Mio Dollar payment to using Gnome instead of KDE for example. If you browse the archives you can clearly see that the decisions which were made on the list are mostly not related to the discussion which happened on the list.\n\n** Bruce Perens claims that the 1 Mio budget is is tied to using Gtk - while on the other hand he claims that he has no financial interest in Gtk (his claim of having a financial interest concerning the Qt book just sounds like hypocrisy to me!)\n\n** On one hand Bruce insists that the reason for excluding the Qt toolkit (and therefore KDE) is justified by the decision that only *ONE* toolkit should be used in UserLinux, while on the other hand he has already approved *THREE* other toolkits for UserLinux!\n\n** In spite of the fact that the majority of the vocal list has already clearly stated that KDE is much better in technical terms, much better suited as a development platform and much better in terms of cost (as the time to develop a Qt based product is lower and support/quality is much higher) he spreads a deliberate misrepresentation.\n\nObviously someone has a financial interest to drive his agenda and tries to muzzle others under the \"banner of freedom\"."
    author: "Anonymous George"
  - subject: "Re: Bruce lies for the money"
    date: 2003-12-16
    body: "Yes; that seems ridiculous to me.  $1Mil USD for Gtk+ development--but Qt is bad because of the financial cost for proprietary development (which would likely, no doubt, be for profit)?\n\nBruce is alright and all, but sometimes he leaves me scratching my head. ;-D"
    author: "Shane Simmons"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "Bruce needs to either be honest about his reasoning or admit to his intellectual difficulties. His \"developmental\" argument flies in the face of reason and is an open insult to free software and the common man. Who are these \"imaginary developers\" that will be hamstrung by license costs? This scenario only plays out when you are planning on taking a lot of free development efforts and turn around and sell the product after adding some value. That means that the finished product isn't free software and the development was contributed largely by free developers. This is the only model where the license issue comes up and it is the Microsoft whine we all know as \"we can't steal your software.. Wah!\"\n\nLet's look at reality. There are several factors to consider developing an application. Will it be distributed or is it vertical market software? What are the skill sets of the developers and how much developer resources do you have? What is your financial standing to accomplish this? The idea that a little guy writes a fully polished desktop application by himself in his spare time and the fee will break him is irrational. It takes man years to write an application from scratch unless you buy a lot of pre-assembled code and widgets as many developers make a business around. If your program is going to make enough money to turn a profit and $1500 makes a difference you have a funny concept of profit. The most expensive part of development is by far the man hours and you're going to need a lot of them... We're talking hundreds of thousands of dollars in billable hours! Check the last Quanta BE release in sloccount.\n===\nTotal Physical Source Lines of Code (SLOC)                = 149,389\nDevelopment Effort Estimate, Person-Years (Person-Months) = 38.38 (460.52)\n (Basic COCOMO model, Person-Months = 2.4 * (KSLOC**1.05))\nSchedule Estimate, Years (Months)                         = 2.14 (25.70)\n (Basic COCOMO model, Months = 2.5 * (person-months**0.38))\nEstimated Average Number of Developers (Effort/Schedule)  = 17.92\nTotal Estimated Cost to Develop                           = $ 5,184,133\n (average salary = $56,286/year, overhead = 2.40).\nSLOCCount is Open Source Software/Free Software, licensed under the FSF GPL.\nPlease credit this data as \"generated using David A. Wheeler's 'SLOCCount'.\"\n===\nFiguring licenses for each of the theoretical 18 developers above at no discount for volume that's 0.5% of the development cost. You also need to sell over 100,000 copies at $50 net just to break even, which should the popularity of free software. You could make more money packaging and training. Bruce is advocating the wooly mammoth we're slaying.\n\n1) However similar KDE and GNOME are in use and appearance there are architectural differences. Most application designers are more oriented to C++ which would make KDE an obvious choice for them. Bruce has decided to remove this.\n2) Reviewing the tools, lines of code to accomplish various tasks and docs, the question of efficiency comes up. As illustrated above the logical question of cost comes down to evaluating whether your people would realize an increase in efficiency. You would have to assert *zero* efficiency gains on Qt/KDE to make this argument hold water!\n3) If you're selling software for a profit then bringing it to market sooner also significantly affects yor cash flow so you can't afford not to examine this question in the real world unless you have more money than you know what to do with.\n4) If you're developing an application that is not going to be distributed but used internally the license issue is a moot point.\n5) Whatever desktop is being used to put forward it's pretty face the smartest thing to do to support users is to give them what they want. This means that from a user perspective people will want to run Quanta and telling corporate shops to \"go to hell we don't do Qt\" is just not good business. Only a company with a Microsoft mentality would treat their customer like that.\n\nClearly what Bruce offers is a red herring unless he is involved with a group who wants coders to work for free so they can resell their work. This could be done with a dual license agreement, but I would rather consider what made F/OSS work in the first place. If a company has a lot of money, they are going to develop an application and go with the old guard model of selling it for a profit instead of selling service or value added packaging then I don't see why they should not pay for the software that goes into it. Why the hell is Bruce beating us over the head about 1% software profits instead of promoting free software development? Is market penetration and branding more important than principles?\n\nLet me be clear... if you intend to charge me for your software you had better get my permission and compensate me before taking my software to do it with. You ought to pay for your software profits. What happened to the principles that made F/OSS grow?\n\nBruce is either suffering a lapse of ethics or reason and he ought to know better. I don't know what's in this for him but I would not sell my principles for any amount of money. Somebody said he had an investor? Could be Sun, but for all we know it's M$. Come clean Bruce. Enough double talk. Who's your sugar daddy? Why are you really making a bad business move to spite a desktop?\n\nI thought in the open source community you had a position based on your code and your adherence to principle. Can someone explain to me why Bruce's 15 minutes of fame aren't up yet? I honestly don't want people to think he is somehow representing me instead of some corporate cabal that won't even step into the light of day. I'm going to enjoy producing the software people ask him about that he is trying not to support."
    author: "Eric Laffoon"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "> What happened to the principles that made F/OSS grow?\n\nPeople seem to think that, as soon as big corporations can take free software and use it to sell their own products without giving back to the open source community, then free software will succeed.\n\nI definitely don't believe this. Privately owned companies will eventually do _anything_ to trash the competetiors (eg free software), that's a force to take into account. And that's why the LGPL is dangerous. The \"we support open source\" thing only makes sence at some point in time, not anymore when your competitor is an open source project."
    author: "OI"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "I had a good long chuckle when the 'chosen' software was announced by Perens and group. It's not that they don't have the right to chose. But the customers and users definitely do. Hasn't every distribution, including Debian, tried to chose a desktop environment? Haven't they all tried to say this or that is the best MTA, or DB? and what happens? someone who has a different preference, for very good reasons, packages another. And another. And another.\n\nThe distros accept this because they have no choice. Users want choice, for the very good reason that each package provides strong and weak points.\n\nThe *nix desktops are still immature. KDE has it's strengths and weaknesses, Gnome/Evolution/Mozilla/Openoffice do too. To try to select now, before they are complete is, being generous, amusing. To think a committee can select and put together 'the right way' is plain stupid.\n\nDerek (who wonders if the vaunted and courted enterprise developer can use the OO editor or spreadsheet as a part in their application?)"
    author: "Derek Kite"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "If you don't like the idea, why care?\n\nAs you said yourself people like to have choices. This project sounds like the best choice for me if it delivers.\nSo what do you want to achieve - take away this choice from people like me?\n\nYou say people like to be able to choose desktop. Then how come Windows and MacOS are so successful?\nAnd how come the OS with most desktop choices has the smallest market space among the big desktop players?"
    author: "ealm"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "Probibly because its been arround the shortest :)"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "Indeed, MacOS has had twenty+ years to maturate and Windows has had \nnearly that long. You don't build up large user bases overnight people! Linux has done fine considering it's desktop offerings are only six years old at the most (KDE)\n\n"
    author: "anon"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "I don't care. I'm amused.\n\nFrankly, the only advantage I see, other than the server stuff, is it's free. As a desktop it is a hack. Three widget libraries. KDE gets badmouthed as bloated, but have you ran OO?\n\nThe linux server applications are solid, stable, reliable. There is an extremely rich field to chose from. Because of that, it is gaining market share. The desktops are nowhere near that.\n\nThe vertical apps that I imagine this group is trying to attract are usually available in Windows. They use the rich features that the MS software stack contains, ie. using MSWord for text, Access for DB, etc. As much as I dislike MS, they have a compelling platform. So how does the desktop offerings here match that?\n\nAs I said, it is far too early to try to settle on a desktop that rules us all.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "kio-slaves in kde make it VERY convenient to work with web based apps. I have done devel for a lot of systems and so far kde has been the most pleasurable system to develop with and for. It is so nice when you can upload files between servers by just taking any url that kde recognizes and sticking it in the file upload form control of a web site. Or that every app on the system can work with webdavs and sftp transparently. \n\nMy company does zope based web apps and all of the stuff we do is under the GPL I have no problems with Qt being GPL and it does not bother my company at all. We have also installed computers running kde for customers as a secure way to access certain parts of their sites and so far they have all liked it a lot. \n\nOverall for any GUI app I would use KDE as my primary gui platform, zope as my app server and python as the language to bind it all together. These solutions can easily compete with microsoft offerings since we have beaten microsoft offerings more then a few times. The solutions work faster, more reliably, more securely and they are easier to adapat to changing needs.\n\nWilliam Heymann\nhttp://webme-eng.com"
    author: "kosh"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-17
    body: "I've no particular problem with choosing Gnome as the desktop, even if KDE is much better.\n\nWhat I don't understand is why anything writen with QT should be banned from the distro regardless of its merit.\n\nThere are plenty of KDE distros around, but they realise that the Gimp is much better than anything KDE can offer at the moment, so they include that.  Gnome's instant messaging clients have traditionally been much better, so they include them - Kopete is catching up quite nicely though.  OpenOffice.org is much better than KOffice if you want something you can actually use as an MS Office replacement now.  KOffice needs a lot more work, but it is built on better foundations.\n\nLikewise, Quanta Plus is much better than anything Gnome can offer, so is K3B, so is Kolab/Kontact, so, no doubt are many other things that I take for granted on KDE."
    author: "Jonathan Bryce"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "Come on. Enough is enough.\n\nI think people have blown this whole conflict way out of proportion. Even though I am a huge KDE fan and use it whenever I can, I cannot help but see that the behavior of a number of kde fans both here and on the UserLinux mailing list has been downright disgusting. I hope that it is over now, but if not, it's high time it stops.\n\nJust because someone expresses a different opinion than yours doesn't mean he deserves your scorn. Just because someone leads a project in a direction you think is foolhardy doesn't mean that he should be subjected to ridicule. And just because someone expresses a few poor (or poorly worded) arguments about a controversial subject doesn't mean you should lose all respect for him and treat him like a vile enemy.\n\nThese conspiracy theories and allusions to mental deficiency are unjust and border on cruelty. The mockery and attacks on his moral character are just plain  wrong. Even if Bruce Perens is absolutely and completely wrong about this, he would still deserve more respect than that. \n\nIt is a remarkable phenomenon, but it seems ever to be true, that the one thing that any otherwise intelligent and reasonable person can do that will always invariably drive everyone around him into a bloodthirsty rage is to simply refuse to argue about an issue that others find important. It has happened again and again in nearly every messageboard or mailing list I've ever been a part of. This is what Bruce has done here. But there are times when it is a very good thing to refuse to argue about an issue. Sometimes there's really nothing to argue about. Sometimes arguing about something just takes up too much time. Othertimes you just have to not argue in order to keep your sanity.  IRL if you are in a crowded room where everyone is shouting and raving at each other and at you, you don't try to argue. You either leave the room, or do something to calm down or get rid of the people who are causing all the raucus. But if you stand there and keep saying \"I refuse to argue\" you are likely to get lynched. That's the way of the world.\n\nIn my opinion, Bruce probably should have simply stopped discussing the issue. Then those who thought he was wrong could have peacefully left and progressed to their own projects and plans. But each time he came back and gave the smallest hint of an argument without fully explaining his reasoning, and basically saying \"I'm not going to argue with you seriously,\" he caused others to become even more enraged at his failure to engage them directly. The fault lies in both sides. Bruce should either have seriously taken up the argument or never posted anything on the issue again. Eventually things would have died down if he did the later, and maybe he and others would have reached some kind of accord had he done the former (although at the expense of a great deal of time). But then, the goading and attacking done by those responding didn't help to encourage that kind of response, nor did the development of irrational prejudice and hatred toward him.\n\nAnyway, there's been a sort of chain reaction here that has lead everyone to expend far too much energy and reason on this than it was ever really worth. In the end this will be a very small blip in history, barely worth mentioning. \n\n"
    author: "a disturbed kde fan"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "> In the end this will be a very small blip in history, barely worth mentioning. \n\nExcept that several good things came out of it for KDE including tighter integration and polish with the Debian base OS, better suitability for the enterprise in general, and maybe more.  :P\n\nFocus on the positive.  We're already moving ahead with our *constructive* proposal.  :-)"
    author: "Navindra Umanee"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-17
    body: "Hmmm. Is this like Andrew Morton posting a wrong patch on LKML to generate discussion?\n\nThe mind boggles.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-16
    body: "> And just because someone expresses a few poor (or poorly worded) arguments \n> about a controversial subject doesn't mean you should lose all respect for \n> him and treat him like a vile enemy.</I>\n\nI've reread the discussion as it happened in the archives. \n\n--> First the UserLinux leader said that he wanted cooperation from \"the     community\". \n\n--> He also said that the project will be guided by \"meritocracy\" (meaning if you do work and give code you will have more of a say). \n\n--> Then he opens the discussion. But he does hardly participate in the debate. \n\n--> After a few days he comes back and declares his decision to *exclude* KDE and Qt and not support it. And he says he has taken the decision \"by fiat\". \n\n--> He compares himself to Linus. (AFAIK Linus does participate in all debates where he later may \"take the decision\" on his own. And Linus *explains* his decisions....)\n\nThe debate might have been passionate at times -- but KDE/Qt supporters *never* argued for being the *exclusive* desktop default system. They always wanted to include GNOME apps, and suggested steps to achieve that even better. The debate was lead with real *facts*, *figures* and *arguments* (with Bruce P. hardly participating), and hardly any \"flame\".\n\nPlease note, that even all of people favouring GNOME as the default were also wanting to include and integrate KDE apps (where GNOME is lacking a good enough alternative, such as K3B, Quanta and KDEPrint).\n\nBruce \"final\" decision was that KDE and Qt is in effect excluded from UserLinux official support, something which _no-one_  _had_ _even_ _argued_ previously! And he does it on the grounds that he wants to provide a development platform for Independent Software Vendors (ISVs) which is \"free of compensation\" (meaning you don't require to give back anything -- neither code [GPL] nor money [commercial Qt license] -- for their use of Free Software devolopment libraries.\n\nWhy do you then wonder about the fact that people are now speculating who the anonymous investors are? Asking themselves who is part of the \"industry consortium\" who *may* put \"1 million $US per year\" into UserLinux (as Bruce P. indicates in his White Paper)? Asking if it is them may be controlling a hidden agenda behind the UL effort to utilize \"the community\", or at least a willing part of it?\n\nI reserve the right to make up my mind about personalities from their current behaviour, not their past merits...\n\n\n> In the end this will be a very small blip in history, barely worth \n> mentioning.\n\nIndeed. I also believe, that practical work towards the end of a viable Linux Desktop for the Enterprise is much more determining the fate of any project that declares to tackle that gigantic task. In the end this is even more important than even multiple million dollars a year.\n\nPeople will be surprised about KDE's \"ability to deliver\". There is a lot in the pipeline. We will make rethink any sane opponent about his stance by the code (and documentation, usability and polish) we will produce.\n\n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Bruce \"Almighty Perens' Crackdown Reply!"
    date: 2003-12-17
    body: "hummmmm. 1 Million a year? I wonder how much the troll would go for?"
    author: "a.c."
  - subject: "Silly"
    date: 2003-12-16
    body: "Perens is mentioning a bunch of kde-oriented distributions saying they have shown interest in participating in his project. That is not likely to happen.\nI run redhat 9, using nothing of their version kde-version except kdm. I compile kde myself, and I am quite happy with what I end up getting. It is certainly much better than what redhat delivers as KDE. My wish for a desktop would be kde with a few extra programs better integrated, notably Gimp, Mozilla even though I use konqueror, on a rare occasion it is nice to have mozilla, open office. Earlier I would have said gaim as well, but now kopete does the job completely, and my son who used to prefer gaim now exclusively yses kopete, and he is certainly an expert on such matters.\n\nI think the ideal would be to take knoppix and develop it into a full fledged distribution\n\nErik"
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Re: Silly"
    date: 2003-12-16
    body: "I've been participating in many projects during the last few year. As I also teach Linux to newbies, I can say with experience that the recepie for a successful desktop linux is:\n\n- desktop kernel with low-latency and supermount etc\n- software: Debian with KDE + easy admin tools\n- security: no default network services, not even ping is answered by default\n- distribution on a live-cd so that the user can test hardware (and software) compatability before he installs\n- the installation tool should offer an option to automagically shrink an exsisting win-partition and make correct partitions for linux\n\nDid you people test out BeOS a few years ago? It was by far the best desktop OS ever, but it just lacked software. It was easy to install (running a program in windows with automagic partitioning), it booted from zero to desktop in 5-10 seconds and if was superfast..\n"
    author: "Otto Kek\u00e4l\u00e4inen"
  - subject: "About Live-CD"
    date: 2003-12-17
    body: "Two months ago I made a CD-Live with KDE 3.2 Alfa1 \n\nhttp://es.kde.org/modules/news/article.php?storyid=159 (sorry in Spanish)\n\nMaybe, with enough time, I can do the effort to made other with KDE 3.2 Beta 2.\n\nMy intention is to release a CD-Live with KDE 3.2 final as soon as posible (when it's done) to demonstrate the KDE power ;-), and of course with an English version."
    author: "Pedro Jurado Maqueda"
  - subject: "Re: About Live-CD"
    date: 2003-12-17
    body: "Based on what project? Has your work gone upstreams, eg did the \"community\" benefit of the improvments you did?\n\nI see a lot of people doing their own Live-CD's, and everybody has the same goals (making a demo-CD with easy install), but very little is done to unite everybodys efforts..\n\nmaybe we should all start contributing to www.morphix.org which gives the framework for things like this."
    author: "Otto Kek\u00e4l\u00e4inen"
  - subject: "Re: About Live-CD"
    date: 2003-12-17
    body: "The project is metadistros (http://metadistros.hispalinux.es), is a Spanish project with not English translation yet :-(.\n\nThis is how I made, you install a Debian distro, whatever, with your favorite software (KDE 3.2 in my case ;-), then compress the partition where you install the distro like an iso, put it in a directory together a special package named \"calzador\", recompile both in other iso and it's done, ready to burn. \n\nEasier than Knoppix, and you start from a clean Debian install."
    author: "Pedro Jurado Maqueda"
  - subject: "Will Qt ever be Free enough?"
    date: 2003-12-17
    body: "First Qt was free, and they complained it wasn't Free.\nThen it came covered by the QPL, making it Free, and yet they complained it wasn't Free enough.\nNow it comes covered by the GPL, the core license of the _Free_ Software Foundation, and yet, still, they complain it's not Free enough.\n\nWhen will people stop bashing Qt over its license?\n"
    author: "Joeri Sebrechts"
  - subject: "Dead in the Water"
    date: 2003-12-17
    body: "This UserLinux project is dead in the water. It is obviously backed by some strong Gnome and anti-KDE people. No Qt by default, but no MySQL?! That's a complete non-starter. Yer, right... They are talking about creating a whole free/open software stack for groupware, desktop, development etc. That has taken years for commercial software to achieve! And then he wants proprietary software to be developed on top that does not exist today? This will never be sutainable, and it will cost a lot more than the million dollars he is touting.\n\nDream on Bruce. No one will ever use this, and it will never be ready. Some people are very scared of KDE if they mention KDE using distributions."
    author: "David"
---
As part of <a href="http://www.prweb.com/releases/?94719">an exciting new initiative</a> by a group of KDE and Debian developers, a <A href="http://desktop.kdenews.org/strategy.html">strategy has been formulated</a> to conquer the enterprise desktop.  The proposal covers integration and development of standard KDE features as well as developing Debian-specific integration such as <a href="http://www.arklinux.com/">an installer</a> and system tools.  We have a lot of Debian expertise on board, so focussing on a single Debian target instead of a wide variety of platforms makes a lot of sense here.  We hope that similar groups with relevant expertise will form around other efforts such as Fedora and Cooker, where others like SUSE and Ark Linux are already doing an excellent job themselves.  <i>Want something controversial to discuss?</i>  Check out <a href="http://desktop.kdenews.org/strategy.html#gtk">our proposal</a> for integrating <a href="http://www.gtk.org/">GTK+</a> into KDE.
<!--break-->
<p>
This announcement is not as <A href="http://www.userlinux.com/">UserLinux</a> specific as much as it is Debian-focussed.  We welcome collaboration with, endorsement and sponsorship by other entitites.  Just <A href="mailto:kde-debian@kde.org">drop a mail</a> to express your interest.
<p>
And last but not least, a big thank you to all the developers who are already and have been contributing code, effort and wisdom to this.