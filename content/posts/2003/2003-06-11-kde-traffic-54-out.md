---
title: "KDE Traffic #54 is Out"
date:    2003-06-11
authors:
  - "rmiller"
slug:    kde-traffic-54-out
comments:
  - subject: "Bad Link - use the following"
    date: 2003-06-11
    body: "<a href=\"\"http://kt.zork.net/kde/kde20030608_54.html>KDE Traffic #54</a> \n\n<BR> \n<a href=\"http://kt.zork.net/kde/kde20030608_54.html\">KDE Traffic #54</a>\n<br>\nhummmm. what happend to the HTML encoding?<br>\noh well."
    author: "a.c."
  - subject: "Re: Bad Link - use the following"
    date: 2003-06-11
    body: "Thanks.  HTML encoding is at the bottom of the todo list.  :-)"
    author: "Navindra Umanee"
  - subject: "new Kofifce Startup"
    date: 2003-06-11
    body: "IMO, the new Koffice startup is grreat except for the \"create document\" tab which looks entirely flawed and ugly.\n\nThe name description and icon to the right are good, but the way the document is sklected is very poor. here is how i suggest it:\n\n|drop down combo box with options: cards and lables, envelopes, page layout, text orientation|\n\nUnder this are the templates icons each with a distinct icon like the current koffice so you can easily see what it is.\n\n\n"
    author: "Alex"
  - subject: "Re: new Kofifce Startup"
    date: 2003-06-11
    body: "Another thing: users are most likely to work on a recent document. Thus, it makes sence to have that tab as the first tab! Or, as an alternative: remember wich tab was used last, and make sure that tab is raised again the next time KOffice starts up."
    author: "Andr\u00e9 Somers"
  - subject: "Re: new Kofifce Startup"
    date: 2003-06-11
    body: "Right, where recent documents are comprised of both saved documents and the templates used to create new documents.  \n\nI.e., if I open a doc and save it, it is added to the list.  If I create a new doc based on a fax, the fax template should be added to the list.  That way, somebody who creates new faxes (or memos, or reports or letters) all day will have their \"most recently accessed document\" - i.e., a new one - on the list.\n\nTyping quicky and running out the door - hope this makes sense.  I thought of it the other day when I was using KWord."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: new Kofifce Startup"
    date: 2003-06-11
    body: "I don't know why people want to change the current implementation of the Koffice startup selector. I like it much more than the newly proposed one shown in the screenshots. Maybe an option to desactiavate it for those who don't like startup selectors would be of interst, why not.\n\nHowever, I think that much more important for koffice is to get better word/openoffice import/export capabilites. And maybe icons that fit in better with the standard Crystall icons."
    author: "tuxo"
  - subject: "Re: new Kofifce Startup"
    date: 2003-06-12
    body: "I agree - a million times.. what was wrong with the old way?  This seems a whole lot more bloated... a huge icon  window for recent docs? Seems a whole lot simpler with a dropped down menu, first document being at the top.  I dunno... I don't think the three options of, type of document, open document, or recent document, are all too different to have their own tabs.  but again, just my two cent.. and lets get some more opinion on this."
    author: "Jon"
  - subject: "Some ideas:"
    date: 2003-06-11
    body: "1) Don't know if this is already implemented but:\nPlease save the size of this dialog and use it the next time.\nThe small open dialog is really awful.\n2.) Create an option to disable the whole thing and always start with\na blank default page. Most times I just want to write something new down\nand care about page size and all that later. I've searched all over\nkword to disable that dialog but couldn't find anything. Or am I missing\nsomething here?"
    author: "Jan"
  - subject: "Re: Some ideas:"
    date: 2003-06-11
    body: "#2 option would be required IMHO, if for nothing else then to mimic MS Word behavior with this, habbits burned in by MS die hard ;)"
    author: "Tar"
  - subject: "Re: Some ideas:"
    date: 2003-06-15
    body: ">habbits burned in by MS die hard ;)\nYes they do.. and i think it is important that we inplement these little \"freindly faces\" regardles of whatever we personally like/dislike them. After all, the first round of the battle is to make users convert!\n\n/kidcat\n\nNB1: I dont want KDE to look like Win32!\n\nNB2: I must admit that there are a few GUI features in Win32 that i actually like...\n\nNB3: Beware! There is much noise of debuging and ratteling of heavy compilers under the horizont... it is the hordes of PINGUINS and DRAGONS marching under the RISING SUN!!!"
    author: "kidcat"
  - subject: "Re: Some ideas:"
    date: 2003-06-11
    body: "Hmm... need to RTFA first ;)\nI wonder what \"Always start KWord with the selected template\" does on http://linpacker.tuxfamily.org/newconcept1.jpg - that should solve #2, no?"
    author: "Tar"
  - subject: "Re: Some ideas:"
    date: 2003-06-11
    body: "Whole heartedly agree with item 2 - and it should be default - across all of the koffice applications.  Having koffice use the dialogs as the first thing the user sees will generally confuse the user.  If i want to open documents or get a template or look at my recent documents - I can go to the File menu - just like in every other application in the KDE desktop.  If the dialogs are such a good idea - there should be a common framework for them available in all of KDE and everything used to create documents should use it.  It it is not a good idea for some applications - it may be considered that it isn't a good idea for kword or kspread either.  I'm fine having the feature there (in fact I don't have much choice without coding otherwise) - but I'd much prefer to have it turned off and have kword and kspread startup in the same manner as any other word processor/spreadsheet."
    author: "Paul Seamons"
  - subject: "3 items"
    date: 2003-06-11
    body: "Sorry for complaining, but is it useful to release a KDE Traffic for 3 medium-interesting news items? Either collect them until you have more to say, or submit them as separate dot entries...\n\n"
    author: "AC"
  - subject: "Re: 3 items"
    date: 2003-06-11
    body: "A complex question.\n\nStrictly speaking, there are advantages and disadvantages to what you propose, as with anything in life, it is not as clear cut as that.\n\nIt all comes down to what my priorities are.  Would I rather skip a week and save it all up, risking the threads becoming out of date, or would I rather put out something that's a little sparse, but timely?  I think I prefer timely.  If I go to a two week schedule, fine, then I may end up taking your suggestion anyway.  But I'm not going to go skipping issues if there's any way at all I can avoid it, because this is a weekly issue, and it is very important to me that this issue be produced on a schedule for many reasons, including several I choose not to go into at this time.\n\nAlso consider that people are contributing to these issues - I am not the only author, though I am the \"editor\", fwiw.  I don't want to hold their contributions back any longer than I have to.\n\nAs with any other medium, some issues will be more interesting than others.  That is because the source material is not always interesting from one week to the next.  That's the nature of the beast and just something you'll have to deal with.\n\nOf course I could go making up posts.  But that probably wouldn't be appreciated.  Maybe I can write a post where Ellis Whitehead marries Ingo Kl&#246;cker...\n\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: 3 items"
    date: 2003-06-11
    body: "I agree with you, I prefer to keep my self up to date. \n\nI also prefer to read small documents once a week than a big one every two weeks"
    author: "Mario"
  - subject: "Re: 3 items"
    date: 2003-06-11
    body: "I agree w/ above.\n\nEven the statement \"no news at all this week\" tells me something about whats going on (on not) on the lists these days.\n\nI also like the comment of \"active thread on <PROJECT-NAME>... \" because I can then go read the lists and listen in if I'm interested.\n\nThanks a lot for the news... I really like the content & format."
    author: "Jeff"
  - subject: "Re: 3 items"
    date: 2003-06-11
    body: "I try to do that.  But you can also go to the archive link and it will take you directly to the thread.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: 3 items"
    date: 2003-06-11
    body: "Hello Russell,\n\nthank you for the good work. I recommend to do it the way it's fun for you.\n\nThen I want to share my impression too. It sometimes feels disappointing if it's so short, only relevant to one issue.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: 3 items"
    date: 2003-06-11
    body: "I understand.  Maybe I can come up with something fun when there's a slow week.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: 3 items"
    date: 2003-06-12
    body: "Plus, if one misses a week, it may become a habit.\n\nGood job btw. I like it each week. Maybe the developers will talk more if they know it is covered? Or even subscribe to the lists?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: 3 items"
    date: 2003-06-13
    body: "THANK YOU DEREK!!! >:-)"
    author: "Russell Miller"
  - subject: "Re: 3 items"
    date: 2003-06-11
    body: "Well, I don't mind the small updates. Reading something huge like Kernel Traffic can sometimes take a lot of time. The small but frequent KDE Traffics also help to keep the KDE discussions \"alive\". Rather many small discussions regularly than a few big ones infrequently.\n"
    author: "Chakie"
  - subject: "Re: 3 items"
    date: 2003-06-15
    body: " The day (week?) where the KDE project has \"something hot for everyone\" to post every week will indeed be a fine day! Unfortunately we need 25 (or how many) good programmers more to spend 10-20h a week/pr hacker for that to happen.\n\n In the mean time it is still nice to see that the KDE-Machine (TM/R/C) is still running well-greased, and that work gets done even if it is of the more \"trivial\" kind.\n\n So.. Untill the time comes where each and every issue of KDE-Trafic is awesomely-ultra-wiz-bang-cool i guess we just have to digest the \"thin\" ones too (btw.. i've never meet a weekly issue of anything that was always droolsome ;-)\n\n/kidcat"
    author: "kidcat"
  - subject: "Mysterious Thread"
    date: 2003-06-11
    body: "\"There's a thread that shows some promise\" - what is its topic?"
    author: "Anonymous"
  - subject: "Re: Mysterious Thread"
    date: 2003-06-11
    body: "A discussion on the implementation details of splitting kmail from its gui."
    author: "Russell Miller"
  - subject: "Krita"
    date: 2003-06-11
    body: "It is so sad that there is so little development on Krita. Perhaps we need a kind of GPL shareware model. I would pay 100$ for a valuable Photoshop or Paintshop alternative under Linux..."
    author: "Hein"
  - subject: "Re: Krita"
    date: 2003-06-11
    body: "Don't need to pay!\nThere is either Gimp (Free, as un Free Speech) or Corel Photopaint 9 (free, as in free beer).\nOf course you still can send the 100$ to Gimp developers."
    author: "oliv"
  - subject: "Re: Krita"
    date: 2003-06-11
    body: "But Gimp is NOT a KDE/QT Application and it's handling is far away from Photoshop."
    author: "nobody"
  - subject: "Re: Krita"
    date: 2003-06-11
    body: "Do you actually think that Gimp is free has in free beer to develop?!?  Or for that matter Krita or the rest of KOffice.  There is a need for funds in free software too.   Developers need to eat, in fact they need to do more than just eat.  Of course, I'm not saying you should sponsor Krita, but I find comments like \"Don't need to pay!\" disturbing. the \"free\" in free software is not about the actual price of the software... are you telling me that if the Gimp team would charge for their software you would not support them?"
    author: "Patrick Julien"
  - subject: "Re: Krita"
    date: 2003-06-11
    body: "I have to agree with this 100%\n\nIts so sad to see so many people take and take and take from 'free' programs, and never give anything back... The fact is no matter how perfect the world might be with everything being free (a la RMS's view), its not going to happen anytime soon. In the meantime, developing takes time and money... \n\nI'm not able to contribute to projects with code, but I make damned sure that every year I make a few donations to the projects I use the most (gentoo and KDE).. if but a fraction of the users of 'free' programs did the same, we could easily subsidize fulltime development and make leaps and bounds in progress much more so than is happening now with the majority of developers having other fulltime jobs and working on the 'free' stuff in their spare time.\n\nStop being cheap, support the good guys!"
    author: "dr.gonzo"
  - subject: "Re: Krita"
    date: 2003-06-11
    body: "I have to admit that I find something even more frustrating from my experiences developing Krita.  A good portion of free software users see the community has a business, obviously for a single company to develop multiple word processors, image editors, etc. doesn't make any sense.  However, the community is not a single entity, I did not decide to work on Krita so I could compete with Photoshop!  I worked on it because I wanted to learn.  Before Krita, I had never worked with Qt, KDE or anything in KOffice.  I also wanted to learn on the core algorithms that make something like Krita work.  Something that his already done with Gimp.  I swear I'm going to scream if I get another mail about Kimp.  I don't care!  What is wrong with Gimp's GUI anyway?  You can organize it's multiple panes so it looks like Photoshop's no problem.  I'm willing to scratch an itch has much has the next guy, but I'm only willing to scratch on my person.  If you have an itch, leave me out of it!"
    author: "Patrick Julien"
  - subject: "Re: Krita"
    date: 2003-06-11
    body: "Kimp, Kimp, Kimp! ;-P\n\nWell, seriously, you can get used to handling gimp in the way you can get used to handle Photoshop. However, it is not entirely true that you can organize Gimp so as to look and feel like Photoshop. There are still a few problems with the interface of Gimp:\n1) Even though it is possible to dock tools in the newest development version, these docking windows still float around individually. Therefore, a container window is missing that can handle these windows floating windows (e.g. when minimizing the app). Also, wallpaper pictures can be disturbing and interfere visually when manipulating pictures with gimp. A container window would allow to solve this problem as well.\n2) The main toolbox has many buttons - too many per row, there should be only two or three per row in order to quickly access them (this is especially true when you activate toolbar docking, then the size (# buttons per row) of the main toolbar cannot be reduced enough).  Furthermore, toolbar buttons are not grouped, see e.g. Sodipodi for a good example."
    author: "tuxo"
  - subject: "Regarding Kimp..."
    date: 2003-06-11
    body: "There is http://bugzilla.gnome.org/show_bug.cgi?id=71514 which is untouched for nearly one year already stating that Gimp's frontend will be (already is?) separated from its backend in version 1.4. Should be promising for those who like to draft a better UI and link it to the Gimp backend. =)"
    author: "Datschge"
  - subject: "Re: Regarding Kimp..."
    date: 2003-06-12
    body: "Heh, and now this: http://www.kdelook.org/content/show.php?content=6255 ;)"
    author: "Datschge"
  - subject: "Re: Regarding Kimp..."
    date: 2003-06-12
    body: "Interesting, a step into the right direction, I think. However, individual windows are still floating around in the container window and the desktop menu should not appear on each individual image manipulation window, but better be shared among all windows and thus be part of the container window. \nBut then again, it was just a quick hack I guess. Not bad at all!"
    author: "tuxo"
  - subject: "KOffice startup dialog"
    date: 2003-06-11
    body: "Hmm, I tend to like the simplicity of the current dialog...perhaps just a little polish instead? Having a preview pane would be handy, but otherwise this dialog seems a bit bulky (perhaps Keramik increases that perception)."
    author: "Eron Lloyd"
  - subject: "Re: KOffice startup dialog"
    date: 2003-06-11
    body: ">Hmm, I tend to like the simplicity of the current dialog...perhaps just a >little polish instead\nI agree with you, a bit more polish of the current startup dialog and document preview capability would be better thank a bulky dialog."
    author: "tuxo"
  - subject: "Wedding"
    date: 2003-06-11
    body: "How the fsck did a geek like him get a hottie like that?  (j/k - I know it's mad h4x0r skillz that did it)\n\nCongrats on the wedding, I wish I could add some words of advice here, but I'm not married.....yet."
    author: "Lovey"
  - subject: "Re: Wedding"
    date: 2003-06-11
    body: "Heh.  Neither am I, and I hope it stays that way.\n\nHonestly, my opinion of marriage is so low that it took me several tries to come up with an article that wasn't offensive.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: Wedding"
    date: 2003-06-12
    body: "*grin*  Don't worry, your low opinion of marriage was already apparent from earlier.  Seriously, though, your editorial comments often make me laugh. ;)\n\nKeep up the good work.  BTW, I'd prefer staying with the weekly Traffics over every other week.  There's nothing wrong with them being short sometimes, and I personally tend to forget about news articles that don't come out either weekly or monthly...\n\nRegards!\nEllis"
    author: "Ellis Whitehead"
  - subject: "Re: Wedding"
    date: 2003-06-12
    body: "Re the editorial comments: yes, that is what I try to do.  I try to make this fun for me and for the reader.\n\nRe the comments:  I am not trying to cause offense (something that I don't think I did here) but more I am a very outspoken person who has a very difficult time keeping his opinions to himself.  That's why I post here.  Because if I can express my opinions, etc., here, I won't feel as tempted to archive them in the kde-traffic, where they arguably *don't* belong.\n\nI've always felt that I either make good friends or good enemies, but my type of personality doesn't really lend itself to ambivalence.\n\nRe every week - at this point it looks as if the votes are for that.  It's what I'd prefer to do, as well.\n\n--Russell"
    author: "Russell Miller"
---
<a href="http://kt.zork.net/kde/kde20030608_54.html">KDE Traffic #54</a> has been released.  <a href="http://www.koffice.org/">KOffice</a> updates abound, as well as an update on the <a href="http://www.msu.edu/~whiteh12/wedding/wedding.html">continuing saga</a> of Ellis Whitehead.  I wonder if <a href="http://www.msu.edu/~whiteh12/penguin/penguin.html">penguins</a> make good wives.  Anyway, get it at <a href="http://kt.zork.net/kde/latest.html">the usual place</a>.
<!--break-->
