---
title: "QuickRip needs you, you need QuickRip"
date:    2003-06-15
authors:
  - "tchance"
slug:    quickrip-needs-you-you-need-quickrip
comments:
  - subject: "Clipping!"
    date: 2003-06-15
    body: "Please add clipping! I do not care about fps settings or other advanced features, but clipping dramatically improves the movie quality.\n\n(Or add automatic clipping using mplayer -vop cropdetect).\n\n"
    author: "Anonymous"
  - subject: "Re: Clipping!"
    date: 2003-06-15
    body: "I've just changed the code to use cropdetect to detect appropriate cropping attributes, and to then use those to crop the film :)\n\nYou can get the new code by fetching dvd.py from CVS, then overwriting the existing dvd.py with the new one (found in /usr/share/quickrip).\n\nGrab from CVS with your CVS client, or from the web at:\nhttp://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/quickrip/quickrip/dvd.py\n\nThough the web-based CVS viewer seems to be playing up at the moment, so you might have to wait a while for it to update the new version."
    author: "Tom Chance"
  - subject: "XviD & Ogg Vorbis"
    date: 2003-06-15
    body: "Does this support encoding video into XviD format and audio into 5.1 Ogg Vorbis format?"
    author: "JLP"
  - subject: "Re: XviD & Ogg Vorbis"
    date: 2003-06-15
    body: "Both are planned features. If you go to the site and submit a feature request we might be more resolved to implement them ;)"
    author: "Tom Chance"
  - subject: "Re: XviD & Ogg Vorbis"
    date: 2003-06-15
    body: "Ooops, ignore me, I've just noticed that you already have :-)"
    author: "Tom Chance"
  - subject: "Integrate it into K3b maybe"
    date: 2003-06-15
    body: "It would be grea if DVD burning functionality could be integrated into K3b, is tehre such a plan?\n\nAnyway, looks like a good program."
    author: "Mike"
  - subject: "Re: Integrate it into K3b maybe"
    date: 2003-06-15
    body: "DVD writing with dvdrecord is almost finished here (needs some testing, I don't habe a DVD-Writer yet.). DVD+RW writing and video-dvd to follow... :)\n\nCheers,\nSebastian\n"
    author: "Sebastian Trueg"
  - subject: "How about..."
    date: 2003-06-15
    body: "...including it in KDE Extragear? http://extragear.kde.org/"
    author: "Datschge"
  - subject: "Re: How about..."
    date: 2003-06-16
    body: "Yeah I think that's a great idea..."
    author: "lrandall"
  - subject: "Re: How about..."
    date: 2003-06-16
    body: "Well, typically to be put in the KDE CVS repository it should actually be a KDE application, not just a Qt application.  While I did give it a whirl and it's a reasonably nice app for DVD ripping, it doesn't use standard KDE dialogs or menus, doesn't have a DCOP interface, etc.  Note that this doesn't make it a bad app, it just means that it's not a KDE app and as such really (in my opinion) doesn't belong in the CVS repository."
    author: "Scott Wheeler"
  - subject: "Re: How about..."
    date: 2003-06-16
    body: "Well, Tom Chance did write in the text at the top of this page (aka \"article\") that QuickRip currently is only command line and a Qt interface but that he's working on a \"full KDE interface\" which implies that everything you mentioned will be included (I sure hope so). ;)"
    author: "Datschge"
  - subject: "Re: How about..."
    date: 2003-06-16
    body: "I must confess I said that it would be a good idea to add this to cvs based on the function the program performs. I hadn't actually checked whether the program was a KDE app, I just assumed it was. However, if the program was modified to use KDE dialogs, toolbars etc, I think having an app like this in kdemultimedia would be really great..."
    author: "lrandall"
  - subject: "dvd::rip for those who want more power"
    date: 2003-06-16
    body: "QuickRip looks nice if all you want is a simple ripper/encoder. For those who would like more control and functionality, I suggest you take a look at <a href=\"http://www.exit1.org/dvdrip/\">dvd::rip</a>.\n\nYama\n<a href=\"http://www.pclinuxonline.com/\">PCLinuxOnline.com</a>"
    author: "Yama"
  - subject: "Re: dvd::rip for those who want more power"
    date: 2003-06-16
    body: "Why are all these programs written in Python and Perl? I don't like having to struggle around with (snapshots of) language bindings."
    author: "Anonymous"
  - subject: "Re: dvd::rip for those who want more power"
    date: 2003-06-16
    body: "I obviously can't speak for dvd::rip, but for QuickRip, I used Python for several reasons:\n\no I don't know C/C++ ;-)\no It makes coding much faster and easier\no It makes the program less bug prone, and makes fixing bugs eaiser for the clued-up end user\no It's not the sort of app that suffers from the very slight speed loss\n\nAs for having to use snapshots, you only need to if you use a distro which made the awful mistake of shipping incompatable versionf of Qt, KDE and PyQt.\n\nIf you're a C++ hacker, you could always do a C++ rewrite of any of the apps. Making one from python is particularly easy as it's just a matter of translating the code, since the structure is almost identical."
    author: "Tom Chance"
  - subject: "Re: dvd::rip for those who want more power"
    date: 2003-06-16
    body: "i have coded both C++ qt programs and pyqt qt programs, and i really like pyqt.\non most distributions pyqt is difficult to install tho, its a pity. on debian\nyou can do apt-get install python2.2-qt3 i believe.\n\npyqt is really nice, no more waiting for compile and no more weird C++ runtime\nand compiletime errors. it does really matter for smaller applications, but\nit is a bit slower especially on application startup ..\n\nits splendid for fast prototyping: you have a working prototype in no time. \nwhen the app gets bigger, python's dynamic typing can bite you, but then you\ncan switch to c++ if you want (i believe scribus was started as a pyqt project\nand converted to c++ later)\n"
    author: "ik"
  - subject: "Re: dvd::rip for those who want more power"
    date: 2003-06-16
    body: "emerge PyQt... ;-)"
    author: "Carlo"
  - subject: "Re: dvd::rip for those who want more power"
    date: 2003-06-16
    body: "Then encourage your distro to make PyQt and PyKDE a priority. Personally, I'm busting for PyKDE to become better supported so that I can ditch C++ for most of my stuff. It's just amazing how fast (and fun!) things can be done with Python and Qt/KDE...\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "And acidrip"
    date: 2003-06-16
    body: "And (in the spirit of opensource ;-) people might also want to check out Acidrip (http://acidrip.sourceforge.net). It lies somewhere in between QuickRip and dvd::rip in terms of difficulty & power, and works very well for me."
    author: "Tom Chance"
  - subject: "debian packages?"
    date: 2003-06-16
    body: "sounds great.  Where are the debs for ppc?  (hint, hint ;)"
    author: "Lee"
  - subject: "computing power..."
    date: 2003-06-16
    body: "hmmm... it's probably not so easy to pull off, but what would be REALLY cool is a network-ripping feature.  You know: where the app will maybe copy the raw data to hd, and then talk to other instances of quickrip on the lan, sending them all chapters to work on or something, so that they can co-operate to get the rip done quickly.  I'd even settle for something that only worked on a shared filesystem :)\n\nApart from that, I'd want really good priority management (haven't tried it yet; no time this morning) so that I can run it without it bugging me, and maybe some basic and/or smart scheduling system so it can work when I'm not using my machine."
    author: "Lee"
  - subject: "Re: computing power..."
    date: 2003-06-16
    body: "you mean like dvd::rip in cluster mode ..\nwell, i tought one of the objectives of quickrip was to be simple, so maybe\nthis will not be an ambition"
    author: "ik"
  - subject: "Re: computing power..."
    date: 2003-06-16
    body: "hmm... I guess that makes sense.  A simplicity is definitely top of my priority list for a dvd ripper.  Perhaps someday, it'll be easy to use apple's opensourced rendezvous or something to to it automatically, though ;D"
    author: "Lee"
  - subject: "Re: computing power..."
    date: 2003-06-17
    body: "What they could do is simply break up the work over multiple processes.  This will allow it to take advantage of SMP machines, and openmosix can distribute it over many machines.\n\n"
    author: "JohnFlux"
  - subject: "Multiple chapters"
    date: 2003-06-23
    body: "Will it be possible tu easily rip these CDs with the old animes we used to watch as kids ? they have several episodes on one DVD, which are not always on different tracks (some are all on one track, and you have to see the chapters to choose which episode you want to watch)"
    author: "Anonymous"
---
<a href="http://quickrip.sf.net">QuickRip</a> (<a href="http://quickrip.sf.net/screenshots.shtml">screenshots</a>) is a DVD backup utility for GNU/Linux, with a Qt interface as well as a command line interface. It makes ripping DVDs onto your hard drive quick and easy, and so is ideal for those who aren't bothered about framerates, clipping, and other (usually) unnecessary  options. Version 0.7 has just been released, bringing the basic list of features close to completion, but we'd like to see more feature requests, bug reports (or less!) and code submissions before we hit the 1.0 milestone to make QuickRip the best DVD backup utility for KDE.
<!--break-->
<p>
QuickRip is the only DVD backup utility to use the Qt toolkit, as well as offering a command line interface, with full KDE and Gtk/GNOME interfaces in the works. It is unique in this approach, as well as its focus on ease of use. Other notable features include:
<ul>
<li>The choice of one-pass, two-pass and three-pass encoding processes, to choose between speed and quality;</li>
<li>The usual options for changing filesizes, languages (audio &amp; subtitle), aspect ratios, dealing with interlaced DVDs, etc.;</li>
<li>A special PDA mode that makes video files suitable to be viewed on a PDA screen</li>
</ul>
<p>
We'd like to hear users' views on what makes QuickRip good, and what they'd like to see in future releases. We'd also like to get a few more developers on board, to make the code even better, add requested features, and add features of their own. If you'd like to help out, visit the <a href="http://sourceforge.net/projects/quickrip">SourceForge project page</a> and post in the forums, submit feature requests, submit bugs, and help us reach 1.0 to give KDE a really decent DVD backup program.