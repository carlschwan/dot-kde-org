---
title: "KDE 3.1:  Well Worth the Wait"
date:    2003-01-28
authors:
  - "Dre"
slug:    kde-31-well-worth-wait
comments:
  - subject: "First w00t ?"
    date: 2003-01-28
    body: "Aallll righty then.\nStart yer browsers. \n"
    author: "kalpha"
  - subject: "Re: First w00t ?"
    date: 2003-01-28
    body: "You really are pathetic."
    author: "kalpa's mother"
  - subject: "Re: First w00t ?"
    date: 2003-01-28
    body: "thanks mom."
    author: "kalpha"
  - subject: "Last minute Quanta changes..."
    date: 2003-01-28
    body: "We were so excited to be included in the 3.1 release. At first it was going to be very close to our 3.0 so our feature set was small... then the date slipped and we worked on sanitzing even the most obscure bugs and tweaking for speed... then we started back porting some of our 3.2 work... Then when we were thinking in perpetual release candidate mode packages really were set to final instead. So we missed getting the last few changes to 3.1 in the tarball. (Changes that made it to quanta-3.1-kde-3.0) Oops!\n\nThey are in the 3.1 branch and we will diff the package and put up a patch for download for anyone who would like it. Check the Quanta page in the next day or so for the patch. Enjoy KDE 3.1 and thanks to everyone who has made Quanta so popular. We are proud and honored to be included with such great software and great people.\n\nPS Look for an announcement on the big easter egg in KDE 3.1 in a week or so when the dust settles. It's Kommander, the text manipulation/DCOP/scripting visual dialog builder and executor."
    author: "Eric Laffoon"
  - subject: "Re: Last minute Quanta changes..."
    date: 2003-01-28
    body: "Obviously KDE_3_1_BRANCH will be slightly better than what is released now. There is always some delay between making tarballs, getting distributors to create binary packages and allowing mirrors to catch up and the happy release note.\n\nI hope your changes are not critical for Quanta to work properly.. and hey, we need to provide users with a reason to upgrade to 3.1.1 next month. ;-)\n"
    author: "Rob Kaper"
  - subject: "Re: Last minute Quanta changes..."
    date: 2003-01-28
    body: "\"Obviously KDE_3_1_BRANCH will be slightly better than what is released now. \"\n\nThat's what 3.1.1 will be good for :-) The differences aren't too many right now because most annoying bugs have been fixed during the security delay. I think that this is the best .0 release a project could make *ever* :-))\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Last minute Quanta changes..."
    date: 2003-01-28
    body: "Hello Ralf,\n\nmay you just state here what everybody will ask you anyhow. So answer the Debain-KDE FAQ best now ;-)\n\nThanks for your work so far, I'm eger to do the update."
    author: "Philipp"
  - subject: "Re: Last minute Quanta changes..."
    date: 2003-01-28
    body: "noatun is very unstable here. in fact i upgraded from beta 2 to final (suse binaries), changing noatuns skin plungin causes it to crash. always. :("
    author: "unfortunately not"
  - subject: "Re: Last minute Quanta changes..."
    date: 2003-01-28
    body: "> I hope your changes are not critical for Quanta to work properly.. \n\nNah, mostly little things that always become the emails that start \"why didn't you do such and such\" and ironcally a splash screen that I hope will lessen the number of times I read \"dude yer spash screen sucks\". ;-)\n\n> and hey, we need to provide users with a reason to upgrade to 3.1.1 next month. ;-)\n\nNow you're talkin'! We're on it. Ironically that is about the time frame for finishing a number of features we've been working on for PHP support that is second to none."
    author: "Eric Laffoon"
  - subject: "Re: Last minute Quanta changes..."
    date: 2003-01-28
    body: "I found already an i18n bug in the German translation. Should I submit it to quanta.sourceforge.net, to bugs.kde.org or directly to the translators?\n\nStefan\n"
    author: "Stefan Fricke"
  - subject: "Great"
    date: 2003-01-28
    body: "And if your distro hasn't any binaries yet, it looks like Konstruct has been updated :\n<A href=\"http://konsole.kde.org/konstruct/\">http://konsole.kde.org/konstruct/</a>\n<p>\nHappy building !"
    author: "john"
  - subject: "Re: Great"
    date: 2003-01-28
    body: "Gentoo has it too... I was kinda surprised when I read the output of emerge -up world :)"
    author: "coolvibe"
  - subject: "Re: Great"
    date: 2003-01-28
    body: "oh yeah. i'm using gentoo, and this is one of the down points of using such a great system.  i get to wait a day before i can test drive the new KDE (though i was using rc6 on one of my systems). If'n one day they get that distcc working nicely, i would think my network could build a working kde in a few hours maybe.\n\ngo KDE, the rc6 looks nice!"
    author: "mark"
  - subject: "Re: Great"
    date: 2003-01-28
    body: "The KDE3.1 ebuilds are in gentoo since many weeks."
    author: "some1"
  - subject: "Re: Great"
    date: 2003-01-28
    body: "But it wasn't really kde 3.1.  At best, it was rc4 as I remember.  However, 3.1 is now unmasked!  WOOHOO... "
    author: "jstuart"
  - subject: "Re: Great"
    date: 2003-01-28
    body: "debian has had 3.1 rc7 since it was put up on cvs.\ngentoo is great, but you don't need it to run the latest\nsoftware."
    author: "kman"
  - subject: "Konstruct Rocks!"
    date: 2003-01-29
    body: "I'm building KDE 3.1 using Konstruct on my Toshiba laptop right now... very nice indeed!"
    author: "Wheaty"
  - subject: "Impressive"
    date: 2003-01-28
    body: "The new feature list is amazing, and the shots are impressive.\nKDE is really the best desktop, and finally there is no doubt that soon or later sun, redhat and co will adopt kde as the standart desktop!\n\nBut I didn't see kvim integration into kate, kmail and kdevelop, does anyone knows when it will be included, and how I how i could probably configure kde to use kvim as my text control?\n\nGood job KDE! I am waiting for my distro to pack it."
    author: "Anton Velev"
  - subject: "Re: Impressive"
    date: 2003-01-28
    body: "the vimpart is distributed with KDE debs and kvim is distributed for debian from freenux.org site until inclusion into Vim.\n\nthat's still experimental and kmail integration is for kde 3.2.\n\nenjoy,\nMik"
    author: "Mickael Marchand"
  - subject: "Re: Impressive"
    date: 2003-01-28
    body: "I see, so in kde32 we will have vimpart, good.\nAbout the debs - unfortunately my distro is not debian, so they will not work for me.\n\nI will try to find on the net (http://freehackers.org/kvim) how is it possible to compile the vim kpart to be integrated as default text component."
    author: "Anton Velev"
  - subject: "Re: Impressive"
    date: 2003-01-28
    body: "you just need to get the vimpart from the kdeextragear-1 module in KDE's CVS\nand to compile/install it.\n\nthe current version mostly works but it's broken in kdevelop\n\nMik"
    author: "Mickael Marchand"
  - subject: "Re: Impressive"
    date: 2003-01-28
    body: "There's a patch out there for KMail that'll give you the KTextEditor/Vimpart\nintegration right now. In fact, I'm using it myself - editing all my mail with\nvim. Coolness. Also, the vimpart will allow you to embed vim into konqueror,\nand other apps like kdevelop 3 (gideon) and even kwrite!\n\nHere's the steps ( it's possible these may be prettied up a bit and thrown on  the\nweb somewhere ):\n\n#1 - First, get and install KVim:\n\nftp://ftp.uni-kl.de/pub/linux/kde/stable/apps/KDE3.x/utils/kvim-6.1.141.tar.bz2\n\ntar xvjf kvim-6.1.141.tar.bz2\ncd  kvim-6.1.141\n./configure --enable-kde-toolbar --enable-kde-check --with-vim-name=kvim --enable-gui=kde --with-features=huge\nmake\nmake install\n\n\n#2 - Then get and install the vimpart:\n\nexport CVSROOT=:pserver:anonymous@anoncvs.kde.org:/home/kde\ncvs login\n<hit enter at password prompt>\nmkdir source\ncd source\ncvs co kdeextragear-1\ncd kdeextragear-1\nmake -f Makefile.cvs\n./configure\ncd vimpart\nmake\nmake install\n\nFire up Control Center.\nNavigate into KDE Components -> Component Chooser,\nand also into KDE Components -> Vim Component Configuration\n\n\n#3 - Now get the KMail source:\n\nhttp://download.at.kde.org/pub/kde/stable/3.1/src/kdenetwork-3.1.tar.bz2\n\n\nAnd get the kmail-ktexteditor patch here:\n\nhttp://freenux.org/kde/patchs/kmail-ktexteditor-3.1.diff\n\n\n#4 - Finally:\n\ntar xvjf kdenetwork-3.1.tar.bz2\ncd kdenetwork-3.1\ncp kmail-ktexteditor-3.1.diff .\nln -s kmail kmail-3.1\npatch -p0 < kmail-ktexteditor-3.1.diff\n./configure\ncd kmail\nmake\nmake install\n\nFire up your new kmail, then go to Settings -> Configure KMail -> Composer\n\nChoose the Embedded Vim Component\n\nDone!\n\n\n\n\n\n\n"
    author: "Corey"
  - subject: "Re: Impressive"
    date: 2003-06-07
    body: "I've installed 'vimpart' according to the above instructions and have been able to select 'vim' from the component chooser, however I have no 'Vim Component Configuration' so when I try to start it up from kdevelop 3.0 I just get a message stating that the vim component needs to be configured - Anyone any idea what needs to be done to configure this. \nI'm using version 3.1.2 of KDE."
    author: "stv_t"
  - subject: "Re: Impressive"
    date: 2003-12-25
    body: "I have the same problem.  I'm running kdevelop 3.0.0 beta 2, on Gentoo 1.4, and the vimpart from kdeaddons (moved from kdeextragear-1) in cvs.\n\nAny solutions from anyone out there?  I really don't want to have to rebuild the kdelibs, kcontrol, etc. from scratch just to get this to work.  (Ugh!)"
    author: "Joe Khoobyar"
  - subject: "Re: Impressive"
    date: 2004-02-12
    body: "I have the same problem. HELP!"
    author: "Brian Keats"
  - subject: "Re: Impressive"
    date: 2004-01-21
    body: "I try your recipe but had no success building kdenetwork after applaying the kmail-ktexteditor patch. I use Debian and get the sources via apt-get source kmail.\n\nThe error I get was:\n\n/bin/sh ../libtool --silent --mode=link --tag=CXX g++  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -DNDEBUG -DNO_DEBUG -O2 -fno-exceptions -fno-check-new -DQT_NO_COMPAT -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DKDE_NO_COMPAT -DKDE_NO_COMPAT_H    -o kmail -L/usr/X11R6/lib -L/usr/lib   kmail.all_cpp.o  -lkhtml -lkspell ../libkdenetwork/libkdenetwork.la ../mimelib/libmimelib.la -lkabc \nkmail.all_cpp.o(.text+0x37b7e): In function `ComposerPageGeneralTab::ComposerPageGeneralTab[not-in-charge](QWidget*, char const*)':\n: undefined reference to `KTextEditor::EditorChooser::EditorChooser[in-charge](QWidget*, char const*)'\nkmail.all_cpp.o(.text+0x37b97): In function `ComposerPageGeneralTab::ComposerPageGeneralTab[not-in-charge](QWidget*, char const*)':\n: undefined reference to `KTextEditor::EditorChooser::readAppSetting(QString const&)'\nkmail.all_cpp.o(.text+0x3829e): In function `ComposerPageGeneralTab::ComposerPageGeneralTab[in-charge](QWidget*, char const*)':\n: undefined reference to `KTextEditor::EditorChooser::EditorChooser[in-charge](QWidget*, char const*)'\nkmail.all_cpp.o(.text+0x382b7): In function `ComposerPageGeneralTab::ComposerPageGeneralTab[in-charge](QWidget*, char const*)':\n: undefined reference to `KTextEditor::EditorChooser::readAppSetting(QString const&)'\nkmail.all_cpp.o(.text+0x38c94): In function `ComposerPageGeneralTab::apply()':\n: undefined reference to `KTextEditor::EditorChooser::writeAppSetting(QString const&)'\nkmail.all_cpp.o(.text+0xab177): In function `KMEdit::KMEdit[not-in-charge](QWidget*, KMComposeWin*, char const*)':\n: undefined reference to `KTextEditor::EditorChooser::createDocument(QObject*, char const\nand so on ... \n\nDo you have any idea what's the problem? It would be nice i've someone could help me with this, course I *realy* need vim in kmail ;-)\n "
    author: "Markus Hubig"
  - subject: "Re: Impressive"
    date: 2003-01-29
    body: "While KDE really is very impressive (nice work and congratulations), there is really no hope of it becoming the standard desktop for SUN or Red Hat.\n\nBoth are deeply involved in GNOME and GNOME 2.2 really is very nice as well.\n\nThe single biggest problem for SUN is that they have lots of commercial third party developers. SUN does not want to use a platform that someone totally controls commercial development on. If you want to develop commercial applications for KDE you need to purchase a commercial license from Trolltech. SUN does not want to be at Trolltechs mercy.\n\nDo you think Microsoft would ever develop an operating system where their core libraries were owned and controlled by a seperate entity?\n\nWith GNOME, everyone can develop commercial software, even using the GNOME-libraries and not just GTK+. \n\nThere is of course no problems for free software, and still no problem for most developers, the licenses for Qt are not that expensive, but if you ever ask yourself why SUN as a developer of a platform instead of just third party software chose GNOME instead of KDE, one of the answers is pretty clear.\n\nI have to say, KDE looks pretty damn slick, and again congratulations!"
    author: "GauteL"
  - subject: "Re: Impressive"
    date: 2003-01-29
    body: "Actually, Sun has stated in the past that they wished they'd just bought out Trolltech and used Qt since it is far better documented and less kludgy."
    author: "Antiphon"
  - subject: "Re: Impressive"
    date: 2003-01-30
    body: "As long as there is \"commercial support\" like Apple is giving by contributing to KHTML I'm not worried about KDE at all. =)\n\nCheers. ^_^"
    author: "Datschge"
  - subject: "Using Konstruct under Redhat 8?"
    date: 2003-01-28
    body: "I'm sure it will be ages before there are Redhat 8 rpms, so I was thinking of using Konstruct.  Anyone tried this yet?  The instructions on the Konstruct page are very brief... I'm not quite sure how to go about this (other then downloading the source files on my 56k).  Hopefully by the time I'm done downloading someone will have figured the build part out :)"
    author: "Rimmer"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-28
    body: "I'm trying to right now (on RH8).  I've been able to fix most of the compile errors that come up just by installing devel RPMS and playing with ldconfig, but I'm stuck at the moment.\n\nSpecifically, in the kdemultimedia-3.1/noatun/modules/dcopiface directory, it's failing because of references to VideoPlayObject in libartskde.so, and several \"gsl_osc_\" references in libartsmodules.so.  Anyone have any ideas what I can do to fix it?  The error message is below.\n\nThanks!\nCharlie\n\n\n/opt/kde3.1/lib/libartskde.so: undefined reference to `Arts::VideoPlayObject_base::_IID'\n/opt/kde3.1/lib/libartskde.so: undefined reference to `Arts::VideoPlayObject::_Creator()'\n/home/creis/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/arts/modules/.libs/libartsmodules.so: undefined reference to `gsl_osc_config'\n../../../noatun/library/.libs/libnoatun.so: undefined reference to `Arts::VideoPlayObject_base::_fromDynamicCast(Arts::Object const&)'\n/home/creis/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/arts/modules/.libs/libartsmodules.so: undefined reference to `gsl_osc_table_free'\n/home/creis/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/arts/modules/.libs/libartsmodules.so: undefined reference to `gsl_osc_table_create'\n/home/creis/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/arts/modules/.libs/libartsmodules.so: undefined reference to `gsl_osc_process'\n/home/creis/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/arts/modules/.libs/libartsmodules.so: undefined reference to `Arts::StdSynthModule::outputConnectionCount(std::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)'\n/home/creis/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/arts/modules/.libs/libartsmodules.so: undefined reference to `Arts::StdSynthModule::connectionCountChanged()'\n/home/creis/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/arts/modules/.libs/libartsmodules.so: undefined reference to `Arts::StdSynthModule::inputConnectionCount(std::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)'\ncollect2: ld returned 1 exit status"
    author: "Charlie"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-29
    body: "Ah-- good news.  I was using an older version of Konstruct (1/27 instead of 1/28), so I updated and it appears to work fine now.  Can't wait till it finishes!\n\nCharlie"
    author: "Charlie"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-29
    body: "Konstruct works just fine under Redhat 8.  Just follow the instructions.  The question I have is how do you configure QT-COPY to use Redhat's xft2 configuration?  That is all I would need to be a happy camper.\n\nStrid..."
    author: "Strider"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-29
    body: "This worked for me:\n\nFirst apply this patch:\n---------------------------------------------------------------------------------------\nRCS file: /home/kde/qt-copy/config.tests/x11/xfreetype.test,v\nretrieving revision 1.9\ndiff -u -3 -p -r1.9 xfreetype.test\n--- x11/xfreetype.test  17 Dec 2002 14:57:40 -0000      1.9\n+++ x11/xfreetype.test  29 Jan 2003 17:01:29 -0000\n@@ -37,7 +37,7 @@ XDIRS=`sed -n -e '/^QMAKE_LIBDIR_X11[ ]*\n LIBDIRS=\"$IN_LIBDIRS $XDIRS /usr/shlib /usr/lib /lib\"\n F=\n for LIBDIR in $LIBDIRS; do\n-    LIBS=\"Xft2 Xft\"\n+    LIBS=\"Xft2\"\n     for LIB in $LIBS; do\n        FOUND_LIB=`ls ${LIBDIR}/lib${LIB}.* 2>/dev/null`\n        if [ ! -z \"$FOUND_LIB\" ]; then\n---------------------------------------------------------------------------------------\n\nThen do this:\n\nexport QTDIR=$PWD\nexport YACC='byacc -d'\nmake -f Makefile.cvs\n\n/configure -system-zlib -qt-gif -system-libpng -system-libjpeg -plugin-imgfmt-mng -thread -no-stl -no-exceptions  -fast -no-xinerama -prefix /usr/local/qt/ -I/usr/include/Xft2/ -lfontconfig\n\nmake\nmake install\n\n\tP"
    author: "perraw"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-30
    body: "Is this applied to the current qt-copy in head?  Any chance you can give me a pointer on how to apply this patch?\n\nStrid..."
    author: "Strider"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-31
    body: "Hi, i am not able to use this patch, dont know what is wrong by me.\n1. I try to apply the patch against qt-x11-3.1.1 -> the official qt-3.1.1 release from Troltech ( Should i use it only against qt-copy from KDE CVS ?\n2. When i try to apply the patch i get the following error:\npatching file x11/xfreetype.test\npatch: **** malformed patch at line 4: LIBDIRS=\"$IN_LIBDIRS $XDIRS /usr/shlib /usr/lib /lib\"\n\nP.S. TurboLinux 8 KDE 3.1 RPMS can be used in RedHat 8.0 , they worked and there is antialiasing. BUT !: it is not so easy , first u have to build alsa-drivers and alsa-lib if u want to have sound. An there are other minor issues with KDM ....\n"
    author: "Ivaylo Toshev"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-02
    body: "Did you force the install?  Did you remove the previous RH installation before installing?\n\nI've got the TurboLinux 8 RPM's but... I'd hate to hose my system just to get KDE 3.1 (no matter how good KDE is)."
    author: "David Bakody"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-04
    body: "The patch was against qt-copy. It won't be applied to qt-copy because it is just a hack :)\n\nHowever, it works w/o any problems here.\nI removed all RH 8 Qt/KDE stuff prior till installation\n\n\nCheck out qt-copy, kdebase, kdelibs, arts accordning to:\nhttp://developer.kde.org/source/anoncvs.html\n\nThen apply the patch to qt-copy, run the configure line I told you.\n\nOk ?\n\n   P"
    author: "perraw"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-31
    body: "Hmm, the problem for me is I don't seem to have a makefile.cvs :-(\n\nI fetched and installed the sources with konstruct using 'make deep-patch' in meta/kde\n\nI then patched xfreetype.test according to your instructions, but I can't do 'make -f Makefile.cvs', since I don't have the Makefile.cvs.\n\nAlso, I notice that you don't have '-xft' on your configure line - does this still somehow build with Xft support? If I include the '-xft' on the configure line, configure complains that the test for Xft fails ...\n\nGreetings,\n\n   -Chris\n"
    author: "Chris"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-05
    body: "How did you restore your Red Hat Menus?\nThanks"
    author: "tagbo okoli"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-12
    body: "What Instructions are you referring to. Exactly where are they?\n"
    author: "Jim"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-29
    body: "I got it pretty much built with konstruct on Red Hat 8.0.  I had to tweak a Makefile when building some of the GL screensavers (manually added -lGLU, no idea why configure missed that), and I also tweaked a Makefile to not build some of the noatun plugins (they wanted /usr/lib/libesd.la and /usr/lib/libaudiofile.la - again, no idea).  But other than that it's built, and seems to run...\n\nBUT\n\nNo antialiased fonts.  What gives?  AA fonts work fine in the official KDE 3.0.x on Red Hat 8.0, what's changed in 3.1?  Something to do with Qt?\n"
    author: "Des Herriott"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-30
    body: "Try perraw's advice in the reply above.  On mine, I noticed that QT was configuring itself without Xft support, and I found someone's suggestion to add \"-lfontconfig\" to the OWN_CFLAGS variable.  That worked for me, and I think perraw's approach might be a better way to handle it."
    author: "Charlie"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-01-30
    body: "Yup, I think that's working.  I had trouble patching Qt and getting everything to play nicely with Konstruct, so I downloaded and built Qt separately (following perraw's sample command-line), and set the HAVE_QT_3_1_INSTALLED define to TRUE in Konstruct's gar.conf.mk.  I'm guessing there's a way to patch Konstruct to build Qt properly for RH8.0, but I'm not familiar enough with Konstruct to do that myself.\n\nOnly kdebase built so far, but konsole now appears to have antialiased fonts again, so looking good :)\n\nThanks!\n"
    author: "Des Herriott"
  - subject: "Xft support for antialiased fonts"
    date: 2003-01-31
    body: "i've tried to add -lfontconfig to OWN_FLAGS in my gar.conf.mk, it accepts it but then there's still\n...\nXft support ......... no\n..\nin screen print-out...\n\nwhat can be wrong?\n\nthanks, luci"
    author: "luci"
  - subject: "Re: Xft support for antialiased fonts"
    date: 2003-01-31
    body: "Hmm.  Just a guess, but make sure you have both Xft and Xft-devel installed.\n\nFor me:\n\n$ rpm -qa |egrep Xft\nXft-2.0-1\nXft-devel-2.0-1\n\nHope that helps,\nCharlie"
    author: "Charlie"
  - subject: "Re: Xft support for antialiased fonts"
    date: 2003-02-01
    body: "I have both of those installed in RH8 and still get:\n\nXft support ......... no\n\n./configure -xft -continue\n\nstill didn't resolve the problem...\n\n\""
    author: "rh8_user"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-01
    body: "I tried, I failed. Just ended up rm -rf the whole lot."
    author: "Peterservo"
  - subject: "Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-01
    body: "For the benefit of those that are having trouble getting Konstruct to build KDE with XFT support, here are the steps I used . This has worked for me on two machines, but that's hardly a comprehensive QA so YMMV. Please let me know if this does not work for you, or if some of the more knowledgeable folks spot some problems with this process.  I'm installing QT in /opt/qt-3.1.1 and KDE in /opt/kde-3.1, so wherever you see those entries in the process change them to your desired installation location.\n\n1. Download and extract Konstruct\n2. cd konstruct/libs/qt-x11-free\n3. make extract\n4. cd konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/config.tests/x11\n5. I also could not get perraw's patch to work, so I just manually edited xfreetype.test. Change the line:\n   LIBS=\"Xft2 Xft\"\n to\n   LIBS=\"Xft2\"\n   Thanks to perraw for posting this tidbit. \n6. cd konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1\n7. export QTDIR=$PWD\n    export YACC='byacc -d'\n8. ./configure -system-zlib -qt-gif -system-libpng -system-libjpeg -plugin-imgfmt-mng -thread -no-stl -no-exceptions -fast -no-xinerama -prefix /opt/qt-3.1.1 -I/usr/include/Xft2/ -lfontconfig\n9. make install. Go watch two episodes of the Simpsons. \n10. cd to the konstruct dir\n11. edit gar.conf.mk as per the Konstruct README. Be sure to set HAVE_QT_3_1_INSTALLED\n12. export QTDIR=/opt/qt-3.1.1\n      export KDEDIRS=/opt/kde-3.1\n      export LD_LIBRARY_PATH=$QTDIR/lib\n      export PATH=$KDEDIRS/bin:$PATH\n13. unset YACC\n14. cd konstruct/meta/kde\n15.make install. Go watch 10 episodes of the Simpsons.\n16.Edit /etc/ld.so.conf and replace the existing entries for QT and KDE with the new locations. Run 'ldconfig' as root. \n\nDon't forget to edit your .bashrc and add the environment variables set in step 12 for any users that will be using the new KDE.  And by the way, ccache (http://ccache.samba.org) is your friend when you are rebuilding KDE on a weekly basis. "
    author: "GA"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-01
    body: "GA thanks for the step by step. FYI I AM A NEWBIE to linux \n\nI am having a problem after completing step 8.\nI run the 'make install' and get the following error.\n\n\nmake[1]: Entering directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1'\ncd qmake && make\nmake[2]: Entering directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/qmake'\nmake[2]: Nothing to be done for `all'.\nmake[2]: Leaving directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/qmake'\ncd qmake && make install\nmake[2]: Entering directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/qmake'\n[ -d /opt/qt-3.1.1/bin ] || mkdir -p /opt/qt-3.1.1/bin\nmkdir: cannot create directory `/opt/qt-3.1.1': Permission denied\nmake[2]: *** [install] Error 1\nmake[2]: Leaving directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/qmake'\nmake[1]: *** [qmake-install] Error 2\nmake[1]: Leaving directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1'\nmake: *** [install] Error 2\n\n\nam i supposed to run this command from root? \n"
    author: "dbakez"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-01
    body: "Yes, if you want to install it in /opt that is. If you install QT and KDE in your home directory then you can run Konstruct as a normal user. This is actually the default for Konstruct, as it reduces the chance that you will foobar your system."
    author: "GA"
  - subject: "Using Konstruct with Redhat 8.0 : Restoring menus"
    date: 2003-02-05
    body: "Thanks for the instructions! I have been able to compile and start KDE 3.1 on \nRH 8.0. The problem is that many of my menu items have disappeared. Any instructions on how to restore them? I tried to use KAppfinder but it left out a lot of programs.\nThanks in advance"
    author: "tagbo okoli"
  - subject: "Re: Using Konstruct with Redhat 8.0 : Restoring me"
    date: 2003-03-17
    body: "After compiling 4 hours (!) kde 3.1 finally startet but the same problem occured like u desrcibed...any sugestions?\n\n"
    author: "Hinkebein"
  - subject: "Re: Using Konstruct with Redhat 8.0 : Restoring me"
    date: 2003-03-24
    body: "I ended up using the rpms at the kde-redhat sourceforge site"
    author: "Tagbo Okoli"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-06
    body: "I'm pretty new to this too. \n\nI have a problem with step 15. I get\nchecking for Qt... configure: error: Qt (>= Qt 3.1 (20021021)) (library qt-mt) not found. Please check your installation!\nFor more details about this problem, look at the end of config.log.\n\nbut I have libqt-mt.so\n[root@b3hg325sy482a qt-3.1.1]# ls lib/\nlibdesigner.a    libqassistantclient.a    libqt-mt.so.3      libqui.so\nlibdesigner.prl  libqassistantclient.prl  libqt-mt.so.3.1    libqui.so.1\nlibeditor.a      libqt-mt.prl             libqt-mt.so.3.1.1  libqui.so.1.0\nlibeditor.prl    libqt-mt.so              libqui.prl         libqui.so.1.0.0\n[root@b3hg325sy482a qt-3.1.1]# pwd\n/opt/qt-3.1.1\n\n\nThanks\nMarc"
    author: "Marc"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-07
    body: "Hi i get errors saying that ld annot handle the `-l` option  \n\n[root@powerslave root]# ld -l\nld: unrecognized option '-l'\nld: use the --help option for usage information \n\nanyone got any ideas.\n\nregards,\n\nHans"
    author: "Hans Roeffen"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-12
    body: "Hi there,\n\nI'm having trouble with step 9. When I issue the command \"make install\" I obtain the following output:\n\nIn file included from kernel/qtaddons_x11.cpp:25:\nkernel/qt_x11.h:64:22: X11/Xlib.h: No such file or directory\nkernel/qt_x11.h:69:23: X11/Xutil.h: No such file or directory\nkernel/qt_x11.h:70:21: X11/Xos.h: No such file or directory\nkernel/qt_x11.h:71:23: X11/Xatom.h: No such file or directory\nkernel/qt_x11.h:79:34: X11/extensions/shape.h: No such file or directory\nmake[2]: *** [.obj/release-shared-mt/qtaddons_x11.o] Error 1\nmake[2]: Leaving directory `/home/amr/Downloads/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/src'\nmake[1]: *** [sub-src] Error 2\nmake[1]: Leaving directory `/home/amr/Downloads/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1'\nmake: *** [install] Error 2\n\nAll the prior steps are running fine. What can be causing this? I'm using the \"decafeinated\" version of KDE to run this. Is that the problem?\n\nCheers."
    author: "Ferrantepunto"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-20
    body: "Hi,\n\nI solved the problem by installing the \"X-Software Development\" packages. \n\nI haven't payed attention to exactly what packages are included because I used the RedHat 8 Package Manager. It works with only \"X Software Development selected\" and that was fine by me.\n\nGood luck.\n\nKoen Van Impe\nhttp://linux.cudeso.be"
    author: "Koen Van Impe - Cudeso"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-03-28
    body: "For RedHat 7.3 the package is XFree86-devel-4.2.0-8.i386.rpm on CD2, it should be similar on 8.0.  Regards,"
    author: "Steven"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-02-25
    body: "I've got a problem in step 9, on the 'make install' command:\n\n/home/Walker/apps/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/lib/libqt-mt                                          .so: undefined reference to `XineramaIsActive'\n/home/Walker/apps/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/lib/libqt-mt                                          .so: undefined reference to `XineramaQueryScreens'\n/home/Walker/apps/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/lib/libqt-mt                                          .so: undefined reference to `XineramaQueryExtension'\ncollect2: ld returned 1 exit status\nmake[4]: *** [../../../bin/uic] Error 1\nmake[4]: Leaving directory `/home/Walker/apps/konstruct/libs/qt-x11-free/work/qt                                          -x11-free-3.1.1/tools/designer/uic'\nmake[3]: *** [sub-uic] Error 2\nmake[3]: Leaving directory `/home/Walker/apps/konstruct/libs/qt-x11-free/work/qt                                          -x11-free-3.1.1/tools/designer'\nmake[2]: *** [sub-designer] Error 2\nmake[2]: Leaving directory `/home/Walker/apps/konstruct/libs/qt-x11-free/work/qt                                          -x11-free-3.1.1/tools'\nmake[1]: *** [sub-tools] Error 2\nmake[1]: Leaving directory `/home/Walker/apps/konstruct/libs/qt-x11-free/work/qt                                          -x11-free-3.1.1'\nmake: *** [install] Error 2\n\nthen the process stops...I don't know how to go about fixing this. Any clues?\n\nthx,\nwalker\n"
    author: "Walker Sampson"
  - subject: "Re: Using Konstruct with Redhat 8.0 step-by-step"
    date: 2003-09-16
    body: "Well.. There i was looking at the kde docs, and I thought this puppy is worth messing with. Sick of the mix and match look and feels that seems not to offend linux geeks, I thought lets bite the bullet download the source and see whats what with kde..\n\nThe first comment i have is i dont understand if i have qt installed and the sources why i cant just say that, but i can live with the esoteric nature of these things and I downloaded the src and compiled it. And then did it again alla kostructor just to test if you were talking shit or not. This worked fine.\n\nSo with qt set up and ready, i thought okay lets compile this puppy.. You must have 3 hour special simpsons episodes because I satrted in the evening left overnight and finished in the afternoon .. But my pc is old and gui stuff takes a long time to compile , and who am i to complain anyway.. So i left it..  \n\nAnd guess what it failed.. I dont mind this sort of thing, but i dont think konstructor should exist.. Or at least just stop when it fails on something. If i was checking an unstable version out of cvs then i'd spend a few days sorting things out, but anyone who's thinking about this dont bother. \n\nIn fact, despite liking kde, i never want to so much as see a hint of a kapp ever again. I was really excited by the possibility. Konstruct is a lie that you'll just get back \"well it is free\" thrown back in your face.. Its simple just down release this bunch of make files that don't work. Please stop, i dont care if its free... \n\nWhat I have to say is.. \"Konstruct doesn't work, Konstruct doesn't work, Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work,Konstruct doesn't work...\"\n\nAND JUST BLOODY WELL SAY SO!!!!! SHOVE ANY \"ITS FREE\" retorted where the sun don't shine... \n"
    author: "QuicheEatingPunk"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-03
    body: "Okay I was wondering if you guys could have an idea what I'm doing wrong here.\nEverything goes really smoothly for me for the most part then when it's compiling I get these errors.\n\nmake[5]: *** [audio_fifo_out.lo] Error 1\nmake[5]: Leaving directory `/root/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/xine_artsplugin'\nmake[4]: *** [all-recursive] Error 1\nmake[4]: Leaving directory `/root/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1/xine_artsplugin'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/root/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1'\nmake[2]: *** [all] Error 2\nmake[2]: Leaving directory `/root/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1'\nmake[1]: *** [build-work/kdemultimedia-3.1/Makefile] Error 2\nmake[1]: Leaving directory `/root/konstruct/kde/kdemultimedia'\nmake: *** [dep-../../kde/kdemultimedia] Error 2\n\n\nfor some reason xine wont compile properly.  It's really od since I just compiled the lates version of xine myself yesterday."
    author: "Kacp"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-03
    body: "How clever you are to skip the relevant part before what you posted. :-)\n\n> It's really od since I just compiled the lates version of xine myself yesterday.\n\nIt's possible that this is the reason: xine's API is AFAIK still very unstable."
    author: "Anonymous"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-03
    body: "http://lists.kde.org/?l=kde-cvs&m=104431000610795&w=2, I expect it to be backported for KDE 3.1.1."
    author: "Anonymous"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-03
    body: "ahh I see, well I though of that and removed all traces of xine I could find.  I'm about halfway through compiling it again, It should hopefully work now."
    author: "same guy as two post up :)"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-04
    body: "Trying to build..bombs on getting qt..\n\n[root@guiness konstruct]# cd libs/qt-x11-free/\n[root@guiness qt-x11-free]# make extract\n[===== NOW BUILDING:    qt-x11-free-3.1.1       =====]\ninstall -d download\n ==> Grabbing download/qt-x11-free-3.1.1.tar.bz2\n        ==> Trying file//files/qt-x11-free-3.1.1.tar.bz2\nmake[1]: *** [file//files/qt-x11-free-3.1.1.tar.bz2] Error 1\n        ==> Trying file///root/kde3.1-sources/qt-x11-free-3.1.1.tar.bz2\nmake[1]: *** [file///root/kde3.1-sources/qt-x11-free-3.1.1.tar.bz2] Error 1\n        ==> Trying ftp//ftp.trolltech.com/qt/source/qt-x11-free-3.1.1.tar.bz2\n--08:42:56--  ftp://ftp.trolltech.com/qt/source/qt-x11-free-3.1.1.tar.bz2\n           => `download/qt-x11-free-3.1.1.tar.bz2'\nResolving ftp.trolltech.com... done.\nConnecting to ftp.trolltech.com[80.232.38.140]:21... connected.\nLogging in as anonymous ... Logged in!\n==> SYST ... done.    ==> PWD ... done.\n==> TYPE I ... done.  ==> CWD /qt/source ... done.\n==> PASV ... done.    ==> RETR qt-x11-free-3.1.1.tar.bz2 ...\nNo such file `qt-x11-free-3.1.1.tar.bz2'.\n\nmake[1]: *** [ftp//ftp.trolltech.com/qt/source/qt-x11-free-3.1.1.tar.bz2] Error\n1\n*** GAR GAR GAR!  Failed to download download/qt-x11-free-3.1.1.tar.bz2!  GAR GAR GAR! ***\nmake: *** [download/qt-x11-free-3.1.1.tar.bz2] Error 1\n\n>?????\n\nsherd\nat\nmuscletech\ndot\ncom\n\n\n"
    author: "Stuart"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-04
    body: "Such file exists, try again."
    author: "Anonymous"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-04
    body: "Okay Almost everything seems to have went smoothly.\n\nExcept I can't use wallpapers for some reason jpeg/png wallpapers wont work.  Any ideas on how I can remedy this situation without compiling everything again.  I'm pretty sure it's just a problem with kdebase."
    author: "Me"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-05
    body: "I keep getting this error when trying to run make on qt 3.1.1. I edited the xfreetype.test file like the earlier post suggested and if configured ok. I now get this error running make, any suggestions?\n\n\ng++ -c -pipe -fno-exceptions -I/usr/include/Xft2/ -Wall -W -O2 -D_REENTRANT -fPIC -DQT_SHARED -DQT_NO_DEBUG -DQT_THREAD_SUPPORT -DQT_THREAD_SUPPORT -DQT_NO_CUPS -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -DQT_NO_XINERAMA -DQT_NO_XFTFREETYPE -DQT_NO_IMAGEIO_MNG -DQT_NO_IMAGEIO_JPEG -DQT_BUILTIN_GIF_READER=1 -DQT_NO_STYLE_MAC -DQT_NO_STYLE_AQUA -DQT_NO_STYLE_INTERLACE -DQT_NO_STYLE_WINDOWSXP -DQT_NO_STYLE_COMPACT -I/root/progs/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/mkspecs/linux-g++ -I. -I../include -I/usr/X11R6/include -I/usr/X11R6/include -I.moc/release-shared-mt/ -o .obj/release-shared-mt/qpngio.o kernel/qpngio.cpp\nkernel/qpngio.cpp:45:17: png.h: No such file or directory\nkernel/qpngio.cpp:67: `png_structp' was not declared in this scope\nkernel/qpngio.cpp:67: parse error before `,' token\nkernel/qpngio.cpp: In function `void iod_read_fn(...)':\nkernel/qpngio.cpp:69: `png_ptr' undeclared (first use this function)\nkernel/qpngio.cpp:69: (Each undeclared identifier is reported only once for\n   each function it appears in.)\nkernel/qpngio.cpp:69: `png_get_io_ptr' undeclared (first use this function)\nkernel/qpngio.cpp:72: `length' undeclared (first use this function)\nkernel/qpngio.cpp:73: `data' undeclared (first use this function)\nkernel/qpngio.cpp:75: `png_error' undeclared (first use this function)\nkernel/qpngio.cpp: At global scope:\nkernel/qpngio.cpp:84: parse error before `,' token\nkernel/qpngio.cpp:98: parse error before `)' token\nkernel/qpngio.cpp:111: type specifier omitted for parameter `png_structp'\nkernel/qpngio.cpp:111: parse error before `,' token\nkernel/qpngio.cpp: In function `void setup_qt(...)':\nkernel/qpngio.cpp:113: `screen_gamma' undeclared (first use this function)\nkernel/qpngio.cpp:113: `info_ptr' undeclared (first use this function)\nkernel/qpngio.cpp:113: `PNG_INFO_gAMA' undeclared (first use this function)\nkernel/qpngio.cpp:113: `png_get_valid' undeclared (first use this function)\nkernel/qpngio.cpp:115: `png_get_gAMA' undeclared (first use this function)\nkernel/qpngio.cpp:116: `png_set_gamma' undeclared (first use this function)\nkernel/qpngio.cpp:119: `png_uint_32' undeclared (first use this function)\nkernel/qpngio.cpp:119: parse error before `;' token\nkernel/qpngio.cpp:123: `width' undeclared (first use this function)\nkernel/qpngio.cpp:123: `height' undeclared (first use this function)\nkernel/qpngio.cpp:124: `png_get_IHDR' undeclared (first use this function)\nkernel/qpngio.cpp:126: `PNG_COLOR_TYPE_GRAY' undeclared (first use this\n   function)\nkernel/qpngio.cpp:129: `png_set_invert_mono' undeclared (first use this\n   function)\nkernel/qpngio.cpp:130: `png_read_update_info' undeclared (first use this\n   function)\nkernel/qpngio.cpp:131: `image' undeclared (first use this function)\nkernel/qpngio.cpp:136: `png_set_strip_16' undeclared (first use this function)\nkernel/qpngio.cpp:138: `png_set_packing' undeclared (first use this function)\nkernel/qpngio.cpp:146: `PNG_INFO_tRNS' undeclared (first use this function)\nkernel/qpngio.cpp:157: `PNG_COLOR_TYPE_PALETTE' undeclared (first use this\n   function)\nkernel/qpngio.cpp:157: `PNG_INFO_PLTE' undeclared (first use this function)\nkernel/qpngio.cpp:197: `png_set_expand' undeclared (first use this function)\nkernel/qpngio.cpp:199: `PNG_COLOR_TYPE_GRAY_ALPHA' undeclared (first use this\n   function)\nkernel/qpngio.cpp:200: `png_set_gray_to_rgb' undeclared (first use this\n   function)\n\n************TRUNCATED****************\n\nkernel/qpngio.cpp:1079: `png_get_x_offset_pixels' undeclared (first use this\n   function)\nkernel/qpngio.cpp:1080: `png_get_y_offset_pixels' undeclared (first use this\n   function)\nkernel/qpngio.cpp:1090: parse error before `;' token\nkernel/qpngio.cpp: In function `void qt_zlib_compression_hack()':\nkernel/qpngio.cpp:1216: `compress' undeclared (first use this function)\nkernel/qpngio.cpp:1217: `uncompress' undeclared (first use this function)\nmake[2]: *** [.obj/release-shared-mt/qpngio.o] Error 1\nmake[2]: Leaving directory `/root/progs/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/src'\nmake[1]: *** [sub-src] Error 2\nmake[1]: Leaving directory `/root/progs/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1'\nmake: *** [init] Error 2\n\n"
    author: "Chris"
  - subject: "Re: Using Konstruct under Redhat 8?"
    date: 2003-02-06
    body: "> kernel/qpngio.cpp:45:17: png.h: No such file or directory\n\nLook around for a png-devel or alike RPM."
    author: "Anonymous"
  - subject: "Please Help!"
    date: 2003-02-05
    body: "For some reason Jpeg support wont work.  Well it works for apps and konqueror, but for some reason I can't see any back grounds or preview files.\n\nI've tried configuring qt  a few different ways, but to no avail.\nThis is the config I tend to use\n\n./configure -system-zlib -qt-gif -system-libpng -system-libjpeg -plugin-imgfmt-mng -thread -no-stl -no-exceptions -fast -no-xinerama -prefix /opt/qt -I/usr/include/Xft2/ -lfontconfig\n\nand I get this line libjpeg  = system(plugin)\n\nI install it from the /opt/qt directory to /opt/qt-new  then when it's finished I delete the installation director \"/opt/qt\" then rename /opt/qt-new to /opt/qt.   I do this so I don't have to change any path variables ever.\n\nAnyone have any ideas?\n\nOh yeah which package for KDE deals with jpeg support?  kdebase? kdelib? kdearts?"
    author: "Me"
  - subject: "Re: Please Help!"
    date: 2003-02-05
    body: "Okay I'll mention that when I compile KDE I get check if qt needs -ljpeg = no   (or something to this degree)  I'm not sure how relevant this is. "
    author: "Me"
  - subject: "Mandrake 9.0?"
    date: 2003-01-28
    body: "Hi,\ndoes anybody know if Mandrake will be able to release binaries in the next days?\n\nIs anybody else providing binaries for 9.0?\n\nTexstar ? \n\nThanks"
    author: "joe"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-28
    body: "Well binaries are already available for Cooker (STBNA 9.1). I've been running Cooker for a while, and KDE 3.1 RC6, pretty impressive it is too."
    author: "John Allen"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-28
    body: "Me too, but don't forget, that the KDE in Cooker requires the newest XFree86, which is 4.2.99.5 in Cooker and packages like fontconfig and OpenSSL 0.9.7. But I'm sure, those Mandrake guys are working on Packages for MDK 9.0 ;)\n\nBest Regards from Germany,\nPom"
    author: "Mathias Meyer"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-28
    body: "They used to be quicker...\nAnd I think they should, as they are a mainly KDE distribution\n\nBut on the other hand... the KDE developers aren't quick either :-)\n\nThe announced 2 weeks ago the final release within roughly a week...\nAnd while time passes, they don't even post a message to say they are late - again.\n\nKDE is a great project, but timeliness could have a higher priority."
    author: "Lightning"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-29
    body: "I'd imagine they are probably concentrating more on distro building atm, since their number one priority is to survive bankruptcy protection.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-28
    body: "As XFree86-server didnt work for me, I had to find a workaround. You may install just XFree86-libs with the --nodeps option, and all is fine."
    author: "Honor"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-28
    body: "I never thought I'd see the day when Debian packages came out before Mandrake ;)"
    author: "Anand"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-29
    body: "Is anybody else out here having problems with Mandrake?\nI installed the cooker versions and all the required updates.\nNow, X is unstable enough to force reboots. I feel like I have stepped into the land of MS.\nis anybody else having these same issues?"
    author: "a.c."
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-29
    body: "Yes, I've installed packages from Cooker and the result is:\n\nKDE apps don't start from Kicker\nSome modules in Kcontrol don't load\nThere are duplicated toolbar buttons\nThere are duplicated menu items\nSound doesn't work\nKonqueror doesn't remember added bookmarks\nBookmarks > Edit bookmarks crushes Konqueror\n\nShould I continue?"
    author: "anonim"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-30
    body: "Did you delete or move your old .kde directory before using KDE3.1? If the answer is no, that is the cause of your troubles..."
    author: "Luca Beltrame"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-29
    body: "Due to the current financial situation of Mandrake Linux it is doubtful that any RPMs for Mandrake will be available before March 2003. It is also possible that the first RPMs will not be issued until Mandrake Linux 9.1 is ready...\nSorry pals...\n\nFranc"
    author: "francois"
  - subject: "Identify yourself"
    date: 2003-01-29
    body: "First of all, kde3.1 packages have already appeared in cooker, and I bet that if not Mandrake, contributors will build them for 9.0. Secondly, I don't trust your statement. Why? Because of your email, that it's not of mdksoft, of course, instead it points to a domain that is still under construction. I may be wrong, but I'd like explanations."
    author: "Luca Beltrame"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-01-29
    body: "Don't be so sure. MandrakeSoft haven't stopped its work on the distro (9.1 betas recently released). I think Mandrake's RPMS will be available in next few days. As I read on alt.os.linux.mandrake, MandrakeSoft's packager is just finishing KDE 3.1 RPMS for 9.0.\n\nregards\nMarcin"
    author: "mar10"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-02-02
    body: "Sorry to tell you pal, but there have been kde rpms since kde3.1 came out and not just that, there have even been athlon optimized versions of it, but they are not officially available, since the stability of kde 3.1 was not too good we still have to add patches from cvs for some probs, so they will not be release until some issues are fixed, of course you can get some inofficial kde 3.1 rpms, if you really must have them.\n\nSo please first get informed and then try to be a smart....KDe RPMS don't have anything to do with financial situation\n\nUnofficial rpms:  ftp host: linux-phased.ath.cx\nSince mandrakeclub has the testing rpms hosted for i586 architecture I don't provide the i586 anymore."
    author: "Linux-phased"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-02-02
    body: "Is there anywhere we can get i586 packages if we are not members of the mandrakeclub BS?\n"
    author: "me #2"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-02-03
    body: "No I'm sorry not yet, but I expect them to be up on kde mirrrors soon(couple of days), there is one minor bug that has to be fixed, after that they will be available. Expect these packages to be very solid, it's worth waiting for them.\n\nI used to have i586 on my ftp, but removed them due to the great demand, my bandwidth could not handle it."
    author: "Linux-phased"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-02-21
    body: "3 februari a few days, tomorrow it is my birthday (hoera) and finding kde3.1 for mdk9.0 would be the best present to receive"
    author: "ruud koendering"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-02-02
    body: "I need some help..  I used cooker to install xfree86 4.2.99.5 on mandrake 9.0 and now fonts wont render in KDE, and everything looks funny.  I can get into GNOME and everything works fine. But if I try and run a KDE program, or go into Mandrake config I get I mess if the program even starts up..  How can I go back to the older xfree86 so I can fix this mess?  The reason I did this was to install KDE 3.1.. But I'd rather have my system working...  I didn't know xfree86 would mess everything up."
    author: "Heather"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-02-02
    body: "Deinstall all the crap, there are special RPMS for 9.0 out there, the cooker rpms are for the coming 9.1 no way you should use them unless you do a world update.\n\nYou may want to do a fresh install instead of deinstalling and installing everythin manually, that is much easier"
    author: "Linux-phased"
  - subject: "Re: Mandrake 9.0?"
    date: 2003-02-04
    body: "It appears texsstar is compiling these rpms right now.... shouldn't be long."
    author: "me #2"
  - subject: "Windowstyle used?"
    date: 2003-01-28
    body: "I've seen the windowstyle used here several times in screenshots allready. Can anyone tell me what style this is, 'cause it looks rather cool! Wannahave!"
    author: "Andr\u00e9"
  - subject: "Re: Windowstyle used?"
    date: 2003-01-28
    body: "Keramik style"
    author: "JC"
  - subject: "With 3.1 out of the way"
    date: 2003-01-28
    body: "How long till 3.1.1? Are we there yet?"
    author: "zelegans"
  - subject: "Re: With 3.1 out of the way"
    date: 2003-01-28
    body: "I can't help but quote Brian... \"There's not pleasing some people!\" :) I'm sure 3.1.1 (The \"KDE For Workgroups\" release) will be out in around a month's time."
    author: "Haakon Nilsen"
  - subject: "Re: With 3.1 out of the way"
    date: 2003-01-28
    body: "\"KDE For Workgroups\" would of course have to be 3.11. :)\n"
    author: "Chakie"
  - subject: "Re: With 3.1 out of the way"
    date: 2003-01-28
    body: "And after that .. we have KDE forking in to directions,\nKDE 95 and KDE NT.\n\nJust cause\n\n;)\n\nlmao"
    author: "mike"
  - subject: "Re: With 3.1 out of the way"
    date: 2003-01-29
    body: "Yeah, KDE 95 will go to some lengths to hide the fact that it runs on Linux, and KDE NT will actually install it's own kernel underneath everything. ;)"
    author: "ShavenYak"
  - subject: "So it finally happened :-)"
    date: 2003-01-28
    body: "Ok, great stuff. Little more to add. The release of 3.1 marks a milestone as now a lot of Desktop attacking software has a real worthwhile foundation to build on.\n\nI hope the KDE project chooses wisely what to do next. I have all trust :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-28
    body: "Usability!! I know they are working on it, still it's a shame we have to wait at least until 3.2. Konqueror is powerfull in deed, but also a complete mess!\nAnyway great job with KDE 3.1! :)\n\nMe"
    author: "Me"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-28
    body: "konqueror a mess ? HHmmm, did you tried Nautilus ????\nTell us what you don't like.\n\nKonqueror is powerfull and well designed."
    author: "JC"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-28
    body: "I'm still using RC6, but I sort-of agree with the previous poster: konqueror is getting fantastically powerful now but it's very complciated. I know for a fact that my mother would be terrified by the multitude of incomprehensible icons!\n\nDon't get me wrong! I love it (fish:/ is fantastic!!!!) but even on my most KDE-yay! day I'd never argue that Konqueror is a tool designed for newbies ... try sitting a novice down in front of it and watch them struggle."
    author: "Ed Moyse"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-28
    body: "So, uh, hit Settings->Configure Toolbars and remove all the stuff your mom doesn't need.\n\nI suppose you could even create a simplified browsing view profile for her, if you let her use your login."
    author: "Ian Eure"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-28
    body: "i'll have to agree that konqueror is a _REALLY_ nice file manager.  \n\ni can't think of what i don't like, but rather what i would like to have.  perhaps this is already there, in which case just shoot me now.  it would be nice to be albe to have a split screen. on the left side there's a folder browser, on the left middle you see the file view, and on the right there's the inverse of possibly a different set of folders.  i think this would really make moving files around in a large file system much easier.  i find that i have to use two konqueror (or explorer in 2k) windows open to move things from one deep folder to another often far away folder."
    author: "mark"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-28
    body: "Windows->Split View Left/Right\n\n(or ctrl-shift-L)\n\nConsider yourself shot ;-)"
    author: "Ed Moyse"
  - subject: "Kcontrol needs usability audit"
    date: 2003-01-29
    body: "... by a top notch GUI HID team ... \n\nCurrently, well, can anyone say \"WAY TOO MANY CATEGORIES\" ...? Poorly organized too (though the search function helps)."
    author: "KUI YUK"
  - subject: "Re: Kcontrol needs usability audit"
    date: 2003-01-29
    body: "I find things like little broken, ugly and non-lined up icons (close button on sidebar for konqui is an example), configurable toolbars that don't configure properly, etc. more of a hassle.\n\nNeed more \"polishing\"!! ... it's tiny nitpicks like this *UI and correctness* that non-techie users or people's grandmas can comment on.  It would be neat if there was an xprop sort of thing that users could call up and it would take a small screen shot that identified the window/resourc/app and dumped the small screeny and info into an e-mail the user would then fill in according to a template.\n\nLike current bug reports but focussed on UI"
    author: "AC"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-29
    body: "I'll use this at cybcaf\u00e9s but KDE needs to run *well* and *fast* on pII-233 with 128 megs RAM to meet the main criteria I have for my own machines.  Since it doesn't (or on a pIII-500mhz for that matter) I can't use it :-("
    author: "NONInstaller-of-KDE"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-29
    body: "Wrong.  I user it on a Celeron 400 with 128M."
    author: "ac"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-30
    body: "And I use it on an AMD6-2 300mHz with 224MB RAM. =)"
    author: "Datschge"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-30
    body: "I'm the winner: Pentium 233 MMX, 64 MB RAM.\nOr am i the looser ?"
    author: "Henrique"
  - subject: "Re: So it finally happened :-)"
    date: 2003-01-31
    body: "Fist of:\nKDE is a *full featured* desktop enviroment.. kept in analogy with \"that other [non]OS\" the KDE-3.x series matches winXP. And last time i checked nobody in their right mind runs XP on a PII-233. This is why the KDE team is so kind to still release security-patches for the 2.x.x series (\"win98se\").. so ppl with small systems can still get a nice GIU on their free OS.\n\nSeccond of:\nCompile u code yaself if you want it to run fast.. search the net for help, or ask in the apropriate IRC channel. KDE handles hard optimization better then most other things around (except for noatun *shrug*, just re-./configure with lower opts; cd noatun; make all install)\nTrim down the eye-candy to 0.\nChoose a low-color theme.\nDitch that l337 wallpaper.\nUse latest glibc (it hold lots of speedups for C++ apps)\nUse latest gcc (which also has other stuff that we used to \"handhack\" earlier)\nUse latest binutils (cant remember why ;-)\nGo over your startkde and look if there is anything that could be removed.\nAnd last but not least.. recompile ever single thing that you think KDE might issue underneath the surface.\n\nIn other words.. if u want to do the impossible: www.linuxfromscratch.org\nTHAT will give ya all the speed that can possibly be tweaked out of that box.. and if thats still not fast enough.. get an uptodate workstation or go help the gcc/g++ guys making C++ optimizasion even better (or go for 2.x.x).\n\nKind Regards\n/kidcat"
    author: "Anders Juel Jensen"
  - subject: "3.1 & 3.1.xx(CVS 3.1.90) screenies need space ?"
    date: 2003-01-28
    body: "hi\n\ni have the 3.1 screenies (beautiful ones) along with some screenies of the upcomming 3.2 (cvs 3.1.90) with the new features but i dont have webspace to load them up( these files are big prolly take 15-20MB), if any one is willing to host them i can pass them my screenies for uploading.\n\nlet me know (irfan at slingshot.co.nz)\n"
    author: "crazycrusoe"
  - subject: "Re: 3.1 & 3.1.xx(CVS 3.1.90) screenies need space "
    date: 2003-01-28
    body: "How about posting the best of them at kde-look.org?"
    author: "Datschge"
  - subject: "Re: 3.1 & 3.1.xx(CVS 3.1.90) screenies need space ?"
    date: 2003-01-28
    body: "Maybe try getting the size down? Ask yourself if we really need to see an entire 2000x2000 desktop just to see one or two new features?\n"
    author: "Richard"
  - subject: "Re: 3.1 & 3.1.xx(CVS 3.1.90) screenies need space ?"
    date: 2003-01-29
    body: "According to my quick calculations, to have a 15MB file he must be running something like 2640 x 1980 at 24bit. Not counting compression."
    author: "ShavenYak"
  - subject: "Re: 3.1 & 3.1.xx(CVS 3.1.90) screenies need space "
    date: 2003-01-30
    body: "I thought he might have about two screenshots for every single KDE app. =P"
    author: "Datschge"
  - subject: "Just a tip for compilers"
    date: 2003-01-28
    body: "kdelibs wants libart_lgpl, and at least my experience when compiling rc2, it needed such a recent one that I had to grab it from cvs. libart_lgpl is maintained in the GNOME cvs, and I can't seem to find any info on the KDE side on how to check it out, although it is listed as a (recommended) requirement. Now here's how I got it:\n\nexport CVSROOT=':pserver:anonymous@anoncvs.gnome.org:/cvs/gnome'\ncvs login (press enter when asked for password)\ncvs -z3 checkout libart_lgpl\ncd libart_lgpl\n./autogen.sh --prefix=... (+ other arguments usually given to ./configure)\nmake\nmake install\n\nAnd you're set to compile kdelibs. For convenience I also put the snapshot up at http://rasmus.uib.no/~st03069/libart_lgpl.tar.bz2, but then you would of course have to trust me not to trojan it ;)\n"
    author: "Haakon Nilsen"
  - subject: "Re: Just a tip for compilers"
    date: 2003-01-28
    body: "Just can say that libart_lgpl 2.3.11 works fine: http://ftp.gnome.org/pub/gnome/sources/libart_lgpl/2.3/"
    author: "Anonymous"
  - subject: "Re: Just a tip for compilers"
    date: 2003-01-29
    body: "I'm getting the following error in compiling kdelibs under rh 8.0 + libart_lgpl\n(from rawhide). Does anyone else have this problem? \n\n/bin/sh ../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../interfaces -I../.. -I../../kdeprint -I../../interfaces  -I../../interfaces/kregexpeditor -I../../kdefx -I../../kutils -I../../dcop -I../../libltdl -I../../kdecore -I../../kdeui -I../../kio -I../../kio/kio -I../../kio/kfile -I../.. -I/opt/qt-x11-free-3.1.1/include -I/usr/X11R6/include -I/opt/kde_3.1/include   -DQT_THREAD_SUPPORT -O2 -fomit-frame-pointer -march=athlon-xp -D_REENTRANT -I/usr/include/pcre  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -O2 -fomit-frame-pointer -march=athlon-xp -fno-exceptions -fno-check-new  -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_COMPAT  -c -o katefont.lo `test -f 'katefont.cpp' || echo './'`katefont.cpp\n/opt/qt-x11-free-3.1.1/bin/moc katehledit_attrib_skel.h -o katehledit_attrib_skel.moc\nrm -f katehledit_attrib_skel.cpp\necho '#include <klocale.h>' > katehledit_attrib_skel.cpp\n/opt/qt-x11-free-3.1.1/bin/uic -nounload -tr tr2i18n -i katehledit_attrib_skel.h ./katehledit_attrib_skel.ui > katehledit_attrib_skel.cpp.temp ; ret=$?; \\\nsed -e \"s,tr2i18n( \\\"\\\" ),QString::null,g\" katehledit_attrib_skel.cpp.temp | sed -e \"s,tr2i18n( \\\"\\\"\\, \\\"\\\" ),QString::null,g\" | sed -e \"s,image\\([0-9][0-9]*\\)_data,img\\1_katehledit_attrib_skel,g\" >> katehledit_attrib_skel.cpp ;\\\nrm -f katehledit_attrib_skel.cpp.temp ;\\\nif test \"$ret\" = 0; then echo '#include \"katehledit_attrib_skel.moc\"' >> katehledit_attrib_skel.cpp; else rm -f katehledit_attrib_skel.cpp ; exit $ret ; fi\nmake[3]: *** [katehledit_attrib_skel.cpp] Error 139\nmake[3]: Leaving directory `/home/ustun/kde_3.1/kdelibs-3.1/kate/part'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/home/ustun/kde_3.1/kdelibs-3.1/kate'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/home/ustun/kde_3.1/kdelibs-3.1'\nmake: *** [all] Error 2\n"
    author: "Anonymous"
  - subject: "New features"
    date: 2003-01-28
    body: "In the changelog I saw:\n\nSupport for \"Multimedia keys\" on your keyboard\n\nIs it supposed to work?  It doesn't here."
    author: "Another Anon-person"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "hasn't worked for me and there's no obvious place to configure it in the control center, so i'd guess no, not yet.\n\nhotkeys works quite well enough for the moment."
    author: "caoilte"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "I've got a microsoft natural pro w/KDE 3.03, and the multimedia keys work fine--for most apps--but not all.  Kmix, for example will work; just set the key bindings.\n\nMark"
    author: "Mark"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "That's bullshit, I have no multimedia keys on my keyboard!"
    author: "Me"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "I think you have to map them in X first, just like what you would do with the \"Windows\" key.  Not that I know how to do any of that though.\n\n-Justin"
    author: "Justin"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "xev and xmodmap are your friends :)"
    author: "coolvibe"
  - subject: "Re: New features"
    date: 2003-01-31
    body: "Or better, use klineakconfig http://lineak.sourceforge.net"
    author: "Sheldon"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "You need to configure your X correctly for it. In Control Center, go to 'Regional & Accessibility/Keyboard Layout', and select a proper keyboard model there (e.g. the 'Microsoft Internet Keyboard Pro' matches my keyboard). Then KDE should recognize all the multimedia keys (you can also check by running xev, it should print e.g. XF86Back for the back key and not NoSymbol). It's possible that your XFree86 version doesn't support the particular keyboard you have - bad luck, you'd have to upgrade XFree86 then or try to write the map yourself. The keyboard model can be configured in XF86Config too if you don't want to use the kcontrol module."
    author: "L.Lunak"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "except sadly, kde doesn't support my memorex keyboard."
    author: "caoilte"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "It's not KDE, it's an XServer issue. KDE doesn't care about what kind of keyboard you have.\n"
    author: "L.Lunak"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "I've got the same kb, but the keys aren't mapped correctly.  Not sure why.  Wonder whats preventing it from \"just working\"?"
    author: "Anon"
  - subject: "Re: New features"
    date: 2003-01-29
    body: "Can anyone post their xmodmap-file with these multimedia-keys enabled?"
    author: "Andreas Joseph Krogh"
  - subject: "Re: New features"
    date: 2003-01-31
    body: "Seriously <shameless plug> checkout klineakconfig http://lineak.sourceforge.net no need for xev, xmodmap, etc."
    author: "Sheldon"
  - subject: "Re: New features"
    date: 2003-01-28
    body: "> Is it supposed to work? It doesn't here.\n\nIn KControl->Peripherals->Keyboard, i picked\n\nKeyboard Model:  Microsoft Natural Pro\nPrimary Layout:  U.S. English\nPrimary Variant:  pc_universal\n\nStill, it didn't work for me until I picked \"U.S. English\" in the Additional Layouts frame.  I don't know why it made a difference, but now it works great (and that's with KDE 3.0.3)."
    author: "Mikey"
  - subject: "Re: New features"
    date: 2003-01-29
    body: "I've gotten all versions of KDE that I've used to take advantage of the \"special\" keys on my keyboard.  It took a bit of reading and a lot of trying but it works very well now. \n \nI use Mandrake.  Google helped me find the necessary information, but here it is in a nutshell: \n \nOlder versions of KDE couldn't directly recognize the keys that XFree86 assigned to those keys (they had symbols like \"XF86Back\" and such).  Those keys are enabled when you enable keyboard layouts ( KCC | Peripherals | Keyboard ). \n \nAfter enabling the keyboard layouts, I had to find the file with actual symbols that were used for that keybard.  For my Mandrake system, and for my keyboard, the relevant file was  \"/etc/X11/xkb/symbols/inet\".  I had to change all the XF86xxxxx symbols to F13 through F26.  As I understand it, it was Qt (not KDE) that couldn't recognize the XF86xxxxx symbols.  ( I believe this may be the support now available in 3.1 but I'm not sure.) \n \nOnce I have F13 through F26 working (check using xev), KDE was more than happy to assign them to whatever I wished. \n \nQuestions?  :) \n----------------- \n \nAnd on another note, since I saw L.Lunak posted a reply too - I'll assume that it's the Lubos Lunak that maintains khotkeys.  Lubos, I really really love khotkeys, but JEEZ was it hard to find in KDE.  Once I found khotkeysrc, everything was doable, I just wish there was a KCC module for it somewhere. (\"wish\" in the sense that someone besides me codes it :) ) \n \nAlso, it took some doing and a lot of errors before I figured out the safest thing to include after the \"Run=\" in khotkeysrc was a small script to do something, rather than just the path to mozilla, xmms etc.   LOTS of errors in .xsession-errors and wierdness that way, and I have no idea why.  My ~/bin directory  now has a bunch of tiny scripts that just serve to be called by khotkeys to launch programs.  Later, I added a bit of XOSD to the scripts, so that's nice, too. \n \nOnce again, thanks and I love khotkeys! \n \n-----------------"
    author: "PimpDaddyA"
  - subject: "Compiled on 7.3"
    date: 2003-01-28
    body: "Have anyone tried to compile this on a standard RedHat 7.3 box?\nAre any upgrades of compilers or other non-kde packages required?"
    author: "Kevin Rogers"
  - subject: "kde3.1 packages for redhat"
    date: 2003-01-28
    body: "Coming soon at \nhttp://kde-redhat.sourceforge.net/ \n\nStay tuned.\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: kde3.1 packages for redhat"
    date: 2003-01-28
    body: "also for RedHat 8.0 ? ^_^"
    author: "olf"
  - subject: "Re: Compiled on 7.3"
    date: 2003-01-29
    body: "I run RH 7.3.  I've updated my gcc using Redhat's update agent, but that's all.  I installed QT 3.1 too, seperately from KDE 3.1.\n\nAnd I've had no problems so far.  I've sucessfully built arts, libs, base, network and multimedia.  It's taking the normal long time, but should go quickly from here on in."
    author: "Bradley Woodward"
  - subject: "Re: Compiled on 7.3"
    date: 2003-01-29
    body: "What version of gcc are you using?\nHave you upgraded other components than gcc?"
    author: "Kevin Rogers"
  - subject: "Re: Compiled on 7.3"
    date: 2003-01-29
    body: "RH releases bugfix updates to gcc 2.96 via up2date, unless you're subscribing to the \"gcc-3.1 for rh 7.3\" channel, then you also get gcc 3.1 (installed as gcc3, which means that it's installed parallell to gcc. I.e., gcc has version 2.96, and gcc3 has version 3.1.).\n\nEither way, gcc 2.96 should build KDE just fine. I've buit KDE 3.0.x and a few betas of KDE 3.1 with it, and I've had no problems. Gcc 2.96 is _not_ a bad/buggy compiler (for comparison, gcc 3.0 could not build arts correctly)."
    author: "moZer"
  - subject: "Re: Compiled on 7.3 - problem!"
    date: 2003-01-29
    body: "Hi!\n\nI've compiled KDE 3.1 using Konstruct on my Red hat 7.3 box and it all works fine except for a rather weird problem with Konqueror! Take a look at tis image : http://www.navero.co.uk/misc/erro-konqueror.jpg"
    author: "Jaime Fordham"
  - subject: "S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "KDE  3.1 seems to work quite well on a SuSE 8.1 distro. However, one thing still does not work, the S/MIME in KMail. It's still the same old problem, I can not configure the needed plugin because the \"Certificate Manager\" does not start.\nCan anyone confirm this? Do I need to download some extra Aegypten stuff that can not be distributed with the original rpm's?"
    author: "Marcus Reuss"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "which rpms have you downloaded for suse81? those provided by kde or you have found suse prepared ones?"
    author: "Anton Velev"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "The ones provided by KDE for SuSE 8.1"
    author: "Marcus Reuss"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "Binaries are never provided \"by KDE\". The SuSE-binaries found here are packed by SuSE, of course.\n\nGreetings\n\nTim\n"
    author: "Tim Gollnik"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "> The SuSE-binaries found here are packed by SuSE, of course.\n\nAre you sure? Because i checked the suse ftp mirrors and there was nothing about kde3.1 the last kde we can see there is 3.0.4, this is also the latest one we see on their site (check http://www.suse.com/us/private/download/linuks/index.html )"
    author: "Anton Velev"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "Sure: http://www.kde.org/packagepolicy.html"
    author: "Anonymous"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "Look into each of the package information \nabout who and where compiled it,\nand loook at the RPM's changelogs as well. \n"
    author: "SHiFT"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "\n These any good ?\n\n  http://download.at.kde.org/pub/kde/stable/3.1/SuSE/ix86/8.1\n\n  \n  I'm on SuSE 8.1 , but using konstruct right now to refresh my RC6.  \n\n  Great to have choices. Great to have KDE3.1\n\n  Thanks to all who've made it possible."
    author: "KMD"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-28
    body: "KDE 3.1 is also unstable in Debian and have lot of bugs. In kmail folders are invisible and konqueror crash if i try to open page in new tab.\nSettings > Configure shorcuts > Open in new tab > custom \"t\"\nAnd using in internet I push button \"t\" *crash*\nSetting > Configure shortcuts > | Where is item \"Open in new tab \"\nBoth of these problems were in kde3.1rc5 \nI dont' even dare to use font installer because that causes hundreds of kdeinit4 processes to start and they freeze my computer.\n\nSorry my bad english."
    author: "Anonymous"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-29
    body: "The folders are not invisible. The folder column is just extremly narrow. This bug has been fixed several weeks ago."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: S/MIME on SuSE 8.1 (or KDE 3.1 in generall)"
    date: 2003-01-29
    body: ">The folders are not invisible. The folder column is just extremly narrow. This bug \n>has been fixed several weeks ago.\n\nThanks.You were right "
    author: "Anonymous"
  - subject: "Is it real?"
    date: 2003-01-28
    body: "KDE3.1 really got released? Really? Even before DukeNukem Forever?\n\nSeriously, great job. I had already a look at a release candidate and it's really great. A fine improvement over KDE 3.0.\n\nMy personal KDE3.1 tip: Type \"fish:your-ssh-server\" into Konqueror and enjoy a graphical ssh/scp frontend. A killer feature, finally easy network browsing with zero-configuration.\n\n\n"
    author: "Roland"
  - subject: "Re: Is it real?"
    date: 2003-01-28
    body: "Hi,\n\nI cannot say that \"fish:my-ssh-server\" produces anything but \n\nAn error occured while loading fish:my-ssh-server:\nUnknown host \n\n\n\nWent fishing\nmike"
    author: "And thank you for the fish"
  - subject: "Re: Is it real?"
    date: 2003-01-28
    body: "fish://your_server"
    author: "JC"
  - subject: "Re: Is it real?"
    date: 2003-01-28
    body: "This is great! I always hat the scp-command in the shell to copy files from a ssh-host...\n\nNice job developers ;-)"
    author: "Andreas Pietzowski"
  - subject: "Re: Is it real?"
    date: 2003-01-28
    body: "Hey, this works.\n\nBut, not all of it as expected.\n\nThe fish protocol isn't aware of my ssh-config file? \nIt somehow must be, how would I log in without passphrase into the server.\nBut then again, ssh2 allows you to define any name as a Host in .ssh/config with some options you like to change. Like:\n\nHost my_server_modified\n...\nPort 4321\nUser other_user\n...\n\n\nIf all is correct, the command line \"ssh my_server_modified\" works alright, but \"fish://my_server_modified\" fails with \"Cannot connect to localhost\"\n\n(It seems to be the port, that gets messed up)\n\nAm I missing another piece?\n\nmike\n"
    author: "And thank you for the fish"
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "fish://my_server_modified:4321/"
    author: "Cliff"
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "fish://other_user@my_server_modified:4321/\n\nBloody brilliant!"
    author: "ac"
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "Thanks for your efforts. \nSorry my problem seems to last, but I'm sure I'll figure it out someday.\n\n\nhost1 (is not allowed to ssh to server2) \nserver1 (is allowed to ssh to server2)\n\nserver1%  ssh -fNgL otherport:server2:22 server2\n\non the command line I now can:\n\nhost1% ssh -o 'Port otherport' server1\n\nand end up on server2 with no probs.\n\nAgain, the fish-thing can't \"connect to localhost\" or initiates the protocol and \"dies unexpectedly\". \n\nFor all other servers I really love the fish-thing.\n\nbye\nAnd\n\n"
    author: "And thank you for the fish"
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "So,\n\nI have to correct myself, happily :-)\n\nfish://otheruser@server1:otherport/ \n\nworks.\nIt boils down to fish not reading ~/.ssh/config. Don't know whether this is good or not.\nfish is very good though :-)\n\nthanks\nAnd\n\n"
    author: "And thank you for the fish"
  - subject: "Re: Is it real?"
    date: 2003-02-14
    body: "This is the most amazingly amazing thing I have seen for ages. Can you believe that I used to be a die-hard Gnome user (yeuch), but since Gentoo started bundling KDE 3.1, I decided to make the switch.\n\nNow that I know what FISH protocol is for I'm sure I will use it all the time. "
    author: "Salim Fadhley"
  - subject: "Re: Is it real?"
    date: 2003-01-28
    body: "fish://localhost/\nfish://root@localhost/tmp/\nfish://user@localhost/home/user\n\nassuming that you have ssh running."
    author: "a.c."
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "You can also use it in other kde apps.\nI for example edit most of the files on remote machines with kate and fish.\nJust put fish://user@host/path_to_file in file dialog and bookmark it if you want :)\n\nhave fun\nFelix"
    author: "hal"
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "Yes, actually you can use every ioslave in every KDE app.\n\nAdditionally try to setup lanbrowsing, it also supports the fish ioslave.\n(the guided setup has a bug, remove trailing \";\" from /etc/lisarc !, fixed in cvs)\nThen you have your whole lan at your hands without samba :-)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Is it real?"
    date: 2003-01-30
    body: "Is this the current \"lan\" bug-fix?\nRemove all trailling \",\" from /etc/lisarc?\n\nsmb:/your_workgroup/your_server/ works\n\nbut _NOT_\nlan://your_server/ (SMB | FISH | HTTP)\nlan://localhost/your_server/ (SMB | FISH | HTTP)\n\nand _NOT_\nlan://your_server_full_domian/your_server_full_domian/ (SMB | FISH)\nlan://localhost/your_server_full_domian/ (SMB | FISH)\n\nWhat's wrong with the FISH ;-)\n\nGreetings,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Is it real?"
    date: 2003-01-30
    body: "OK, more on this:\n\nWhat I get is this:\n\nlan://localhost/your_server_full_domian/FISH\nlan://localhost/your_server_full_domian/SMB\n\nSome \"strings\" wan't be evaluated?\n\nThanks,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Is it real?"
    date: 2003-01-28
    body: "> My personal KDE3.1 tip: Type \"fish:your-ssh-server\" into Konqueror and enjoy a graphical ssh/scp frontend.\n\nThis is new? I've been using fish since KDE 2.x and I thought it was added to the base packages in 3.0. Not that I'm complaining about the attention.\n\n#1 most exasperating feature request for Quanta: FTP manager. How can we better educate people about KIO? FTP is just another protocol and we're not married to a protocol because to us it's just a means to a file operation. Perhaps a mode selector on file dialogs to prompt users on how to use protocols?"
    author: "Eric Laffoon"
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "I have a way to educate users about fish... allow the developer to setup a project that points to a sourceforge project...  and use fish/quanta to remotely edit webpages transparently.  This would be *very* cool, IMHO :)"
    author: "Adam Treat"
  - subject: "Re: Is it real?"
    date: 2003-01-29
    body: "I wouldn't be too quick to jump on using fish for sftp sessions. When you attempt to connect to a server with lots of subdirectories that are distributed over many computers the fish ioslave will hang that server while it tries to figure out all the autocomplete names...\n\ntry using the sftp:// ioslave instead, it's not as invasive of system resources on the machine it's connecting to."
    author: "Elliott"
  - subject: "wait"
    date: 2003-01-28
    body: "Wait? What wait?"
    author: "IR"
  - subject: "Distributed RPM's don't work under suse 7.3"
    date: 2003-01-28
    body: "After downloading the RPM's and installing under Suse 7.3\nkde hangs while logging in.\n\nIt gets to \"initializing system services\" then stops."
    author: "AM"
  - subject: "Re: Distributed RPM's don't work under suse 7.3"
    date: 2003-01-28
    body: "try cleaning all those tings in /tmp, \n\nand ~/.kde -- (back it up if you need settings from there)\n"
    author: "SHiFT"
  - subject: "Re: Distributed RPM's don't work under suse 7.3"
    date: 2003-01-29
    body: "Disable the fam service, start kde and the restart fam"
    author: "linuxgonz"
  - subject: "Re: Distributed RPM's don't work under suse 7.3"
    date: 2003-01-29
    body: "OK indeed its fam,\nafter turning of fam kde3.1 starts"
    author: "AM"
  - subject: "why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "I wonder why kde 3.1 is released with known bugs, so what the difference between a beta release and a stable release ? I appreciate all the efforts made by the community but I don't appreciate that the a \"stable\" release contains known bugs.\n "
    author: "frederic heem"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "The only important question for you is: How many of these bugs affect you? Did you vote for them already at http://bugs.kde.org? Or do you care about the 1000+ web pages that don't render 100% correctly and which you never visit? Btw, that 4000 is pretty low for such a big project. Compare that to the numbers of the Mozilla, Gnome or OpenOffice.org projects. And Mozilla has no desktop/office, Gnome no office/html render engine and OpenOffice.org no desktop/browser."
    author: "Anonymous"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "Many of them are probably old and inaccurate. If someone got time over he/she could go over those bugs and close those that aren't reproducable any longer..."
    author: "Sam"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "...but he'd probably rather be coding... :-)"
    author: "Andr\u00e9"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "No, I'd rather hunt bugs rather than coding, but I've got many other things to do"
    author: "frederic heem"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "It would be really nice too if you could help closing some bugs that doesn't occur anymore. Its not hard at all.\n\n"
    author: "Sam"
  - subject: "Re: why kde 3.1 is released with more that 4000 bu"
    date: 2003-01-29
    body: ">> It would be really nice too if you could help closing some bugs that doesn't occur anymore.<<\n\nHow does one sign up for it?  Like seriously.  I honestly do feel like a bit of a free-loader since I really like KDE and all the OSS stuffs but I'm not a programmer, I only speak English, and I am not I an artist (color-blind...).  I don't know how to help out (other than money donations, of course).  I really don't have a WHOLE lot of spare time, but when I find myself looking for something to do, it would be nice to be able to \"give back\" a bit to the KDE developers and users.  If I could sit and have a beer or two and help kill a ton of backlog, that would be a Good Thing (TM) IMHO.  I just don't know where to even start with it.\n\nI run Gentoo and I update whenever things are released.   I say that because I'm running the \"real\" KDE sources, not some ditro's modified binaries so I'd beable to to really help out and easily figure out if it's a KDE problem or a distro problem.  \n\nIt's ugly and kinda boring work (just clicking a few things to see if I can make something crash kind of thing...), but it would be nice to give a little back to the folks that made KDE.\n"
    author: "Xanadu"
  - subject: "Re: why kde 3.1 is released with more that 4000 bu"
    date: 2003-01-29
    body: "For the start get an account on http://bugs.kde.org and comment on duplicates, try to reproduce bug reports or ask submitters if ancient reports still apply to current KDE versions."
    author: "Anonymous"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "i am doing this as i don't know c++, etc.\n\nhowever i think the original poster should consider this:\n\nall open source projects have bugs, reported or not\n\nthat means all releases also have bugs, believe it or not\n\nso you should get over with it, stop wit the yada and check yourself?\n\nopen source works, just as well as everything else often\n\n"
    author: "the beast"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "> open source works, just as well as everything else often\n\nfrom my experience OpenSource works better, because\nbugs are reported (many users) and developers are\nworking on those bugs. Sometimes users are even resolving them.\nNot like closed  products.\n\nHow many bugs are in MS Products? For sure at least 100 times more.\nAnd MS dont care about them -- they care about 'features',\nbecause features are selling point and they need $$$$.\n\n>all open source projects have bugs, reported or not\n\nAll projects have bugs... Software is written by people.\nAre you perfect? No. Everyone has his 'bugs', nobody's clean.\n\n"
    author: "Alekibango"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-31
    body: "Touch\u00e9!\n For comparison Win2000 was released with 21.000 *known* bugs.. and as we all know quite a bit of them was what the KDE team would have called by their true name: Showstoppers.\n\n As mentioned above.. Open Source Software tends to work better *becaue* the hole bug-thing is a non-hush (oposite to M$ who calls bugfixes \"sevice-releases\").\n\n I know it sometimes looks oposite because of the \"release early, release often\" policy that rule the OSS world. This is allso why anyone is recommended to NOT use the x nor x.y releases for coporate use.. those should go for x.y.z (read KDE-3.1.x). But this way of adressing bugs gives just what the developers need: users' feedback.\n\nKind Regards\n/kidcat"
    author: "Anders Juel Jensen"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-29
    body: "Well I don't care about the pages I don't visit. \n\nBut it would have bin nice if I could use konqueror to submit forms on certain websites. E.g. post articles on ZNet, moderate posts on slashdot or manage my D-Link 704 firewall.  If that was possible I could save a lot of space by removing Mozilla."
    author: "Uno Engborg"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-29
    body: "> moderate posts on slashdot\n\nGood news!  You can do that since KDE2 when Konqueror first appeared!"
    author: "ac"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-29
    body: "I have a D-Link 713-P, very similar to your vaporators (I mean, 704) and I can manage it from Konq just fine. I've also never had problems with Slashdot, but can't speak for ZDNet as I try not to venture too far into the Belly of the Beast."
    author: "ShavenYak"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-29
    body: "I remember a post on some mailing list claiming that, the at the time, newly released windows 2000 had 64 000 bugs,  And some Microsof defender replied that only 21 000 was serious. So KDE is perhaps not that bad after all.\n\nThere will always be bugs in software, in opensource we can at least try to fix them."
    author: "Uno Engborg"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-29
    body: "I have to admit that it is quite fun to juggle with numbers, but please try to say something relevant here...\n\nA number of bugs isn't everything: you have to know the number of lines in your code, the complexity of it, and you have to make an assessment on the severity of the bugs. Otherwise, all it is, is just a bunch of numbers...\n\nAnd I'm sure that ms tries to fix them too. Perhaps not always as quick, but it is of course an operating system with all difficulties that come with it...\nComparing a desktop environment and an operating system like this is just plain stupid."
    author: "Lightning"
  - subject: "Re: why kde 3.1 is released with more that 4000 bu"
    date: 2003-01-30
    body: "> Comparing a desktop environment and an operating system like this is just plain stupid.\n\nYes, especially when the desktop environment comes with hundreds of applications while the operating system is only a pretty naked operating system. ;)\n\nIn the end it's all relative. Cheers. ^_^"
    author: "Datschge"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "Any piece of software will always contain known bugs (commercial as well as OpenSource). Compared to the size of the project, 4000 bugs is actually a rather low count (especially if someone would get rid of all the old, innacurate and obsolete bugreports). And, most of the open bugs in bugs.kde.org are actually very minor - the number of grave/critical bugs is amazingly low."
    author: "me"
  - subject: "Re: why kde 3.1 is released with more that 4000 bugs ?"
    date: 2003-01-28
    body: "If they didn't release anything until all known bugs were fixed, they would effectively paralyze themselves and nothing new would get done and there would not be any KDE 3.1 right now."
    author: "Another Anon-person"
  - subject: "Release shedule for KDE 3.2?"
    date: 2003-01-28
    body: "There isn't a release schedule on developer.kde.org for KDE 3.2 yet. Could I expect KDE 3.2 in 2003 or would it take longer? Who would be the release coordinator?"
    author: "Steinchen"
  - subject: "Re: Release shedule for KDE 3.2?"
    date: 2003-01-28
    body: "Something between 6 and 10 months. Stephan Kulow"
    author: "Anonymous"
  - subject: "Re: Release shedule for KDE 3.2?"
    date: 2003-01-28
    body: "Good try - but you're not coolo, mr anonymous :)"
    author: "David Faure"
  - subject: "Re: Release shedule for KDE 3.2?"
    date: 2003-01-29
    body: "I think that wasn't a signature though, that was an answer to the previous question. ;P"
    author: "Datschge"
  - subject: "Re: Release shedule for KDE 3.2?"
    date: 2003-01-28
    body: "When a new windows version is released, the day after do you ask when will be the next release ???\n"
    author: "JC"
  - subject: "Re: Release shedule for KDE 3.2?"
    date: 2003-01-28
    body: "> When a new windows version is released, the day after do you ask when will be the next release ???\n\nThe problem is, no one of us actually cares about the next Windows release =P\n\nBut you're right.\nEveryone should enjoy this version and give the developers time to party and relaxation before going back to coding."
    author: "Hoek"
  - subject: "Re: Release shedule for KDE 3.2?"
    date: 2003-01-29
    body: "> When a new windows version is released, the day after do you ask when will\n> be the next release ???\n\nWhy do you post, if you have nothing to tell?\n\nThink that it might be interesting for someone developing an KDE app to have a timeframe..."
    author: "Steinchen"
  - subject: "Re: Release shedule for KDE 3.2?"
    date: 2003-01-30
    body: "Developers can get a good idea of how long it takes until the next release when they participate in mailing lists. After all it's ready when it's ready.\n\nCheers."
    author: "Datschge"
  - subject: "compile problem on debian unstable"
    date: 2003-01-28
    body: "Anyone?\ni try to compile on mi debian/unstable /sid) but i get some error with the first package, arts.\ndpkg-buildpackage tell that i don't have qt-mt but (i am sure!!) i have libt3-mt and libqt3-mt-dev!!!! in detail, i have the last official package for unstable, tagged qt 3.1.1 cvs 20-12-2002 binary and headers (libqt3-*-dev). currently i am using kde3.1rc6 self-compiled whith the same script dpkg-buildpackage... anyone have the same problem? anyone solved it?\n\nfor all coder: very very GREAT work!!!!! the best desktop EVER! simple as windows XP, stable as KDE!"
    author: "simone"
  - subject: "Re: compile problem on debian unstable"
    date: 2003-01-28
    body: "Qt packages on unstable are still compiled with gcc-2.95, but the default compiler is gcc-3.2 now. Since the abi has changed, this will not work. Either wait for a couple of days 'till Qt packages compiled with gcc-3.2 are in unstable or try this:\n\nexport CC=gcc-2.95\n\n(I hope I remember this correctly. Maybe you have to set some more environement variables, you can probably find how to do it with google).\n\n\n"
    author: "Michael"
  - subject: "Re: compile problem on debian unstable"
    date: 2003-01-28
    body: "Qt packages on unstable are still compiled with gcc-2.95, but the default compiler is gcc-3.2 now. Since the abi has changed, this will not work. Either wait for a couple of days 'till Qt packages compiled with gcc-3.2 are in unstable or try this:\n\nexport CC=gcc-2.95\n\n(I hope I remember this correctly. Maybe you have to set some more environement variables, you can probably find how to do it with google).\n\n\n"
    author: "Michael"
  - subject: "Re: compile problem on debian unstable"
    date: 2003-01-28
    body: "i think is true... \nafter my first post, i start to compile qt libray by myself from source.  \nthanks anyway for your answer... :-))))"
    author: "simone"
  - subject: "Re: compile problem on debian unstable"
    date: 2003-01-28
    body: "Hi Simon,\n\nYou verify quickly with \"ldd -r\" if you got it right. ie,\n\nldd -r /usr/local/qt/lib/libqt.3.1.0.so (or whatever the lib is called)\nldd -r /usr/local/kde/bin/kwin \n\nIf there are unresolved symbols it won't work\n\n"
    author: "Sam"
  - subject: "Re: compile problem on debian unstable"
    date: 2003-01-28
    body: "Is anybody knows when KDE 3.1 deb`s will be included in debian/sid (officialy) Thanks !\n"
    author: "ass"
  - subject: "Re: compile problem on debian unstable"
    date: 2003-01-29
    body: "Probably in a few weeks, or less. http://lists.debian.org/debian-kde/2003/debian-kde-200301/msg00920.html"
    author: "not me"
  - subject: "Re: compile problem on debian unstable"
    date: 2003-01-30
    body: "That's great news, thanks! ^_^"
    author: "Datschge"
  - subject: "3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-28
    body: "I downloaded the 3.1 rpms for SuSE8.1 this morning.\nLess than 25 minutes later, it was up and running!!\nNo dependency issues as long as you get the order right and meet the requirements.\n\nGreat Job KDE Team......\n\n\nJust try upgrading to a new version of windows in 25 mins.  \nO.K. I know it's not a directly relevant or very sensible comparison, but........\n\nThanks,\n\nPaul"
    author: "Paul Hands"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-28
    body: "What is the order.If your are a noob you are lost\nCould you post the install order. I've downloded all that stuff, tried to install and run into a lot of dependency probs. Allmost broke my current installation.\nWould you please be so kind and post the install order."
    author: "sw"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-28
    body: "I have SuSE 8.1 and the only thing i did was to download all files into a folder....\n\nThe i typed as root\n\nrpm -Fvh * --nodeeps --force\n\nthen a reboot and everything works fine.. KDE3.1 is realy great!!!!\nIf you don't want to use the --nodeps --force argumens you have to remove pixie_plus and maybe add som libs before installing... you will get error messages that tells you which libs to install....\n"
    author: "KDE"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-29
    body: "I downloaded all the rpms, then logged out of KDE, launched the shell via Ctrl-Alt-F4 and then logged in as root and then types \"init S\";  Then I typed \"rpm -Uvh * --nodeps --force\" once i got back to the shell prompt after all the servers had been unloaded.  Then I typed \"init 5\" and bingo.. all was fantastic.\n\nGreat Work KDE team !!!"
    author: "Ian"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-02-16
    body: "Hey, I have SuSE 8.1 and I just downloaded the rpms and I've done every single thing to get it to work, but I just can't can anyone here explain step by step? what I did was\n1.- rpm -Uvh *\n2.- run SuSEconfig\n\nwhen SuSEconfig was done I got the following msg:\nATTENTION: You have modified /etc/opt/kde3/share/config/kdm//kdmrc.  Leaving it untouched...\nYou can find my version in /etc/opt/kde3/share/config/kdm//kdmrc.SuSEconfig...\n\nThen I rebooted, but nothing really changed, I'm still in KDE 3.0.3\nAny idea of what I can do?\nThank you\n"
    author: "rem7"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-29
    body: "ignoring package dependencies and forcing the install is not exactely a smart thing to do.\nThe packages havedependencies for a reason - if you ignore those dependencies things may install and mostly work, but it'll probably be broken in various unpredictable ways.\n"
    author: "someone"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-29
    body: ">> ignoring package dependencies and forcing the install is not exactely a smart thing to do.<<\n\nI second this.\n\n--force, from time to time (still, not often at all), I can see.  But --nodeps can quickly land you in a world of hurt.  I think the poster \"someone\" is right in saying that the packages have dependencies for a reaon.\n\nI can't speak about SuSE much (since I didn't use it much (I didn't care for it to be honest...)), but MDK's urpmi and Debian's apt will handle all that stuff for you.  Me being on Gentoo now, the same is true.  \n\nemerge -u world\n\nkdegraphics is building now...\n"
    author: "Xanadu"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-30
    body: "I got KDE 3.1 in 25 hours! Compiling in on a p233 takes awhile. Even if it is pointless I like to do it. Makes me feel 31337."
    author: "Moloch"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-01-30
    body: "Try http://apt4rpm.sf.net follow the links, I use it with Redhat 8.0 works beautifully, though Redhat has KDE 3.1 only for for rawhide for the moment."
    author: "khalid"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-02-28
    body: "use --nodeps \"--nodeeps\" is written wrong ;-("
    author: "markus lippert"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-03-02
    body: "Yup, downloaded the packages yesterday, did \nrpm -Uvh * --nodeps --force\nworks like a smooth!\n\nPS: I still claim on it takes less than 25mins ;)"
    author: "Kilian"
  - subject: "Re: 3.04 to 3.1 in less than 25 minutes!!"
    date: 2003-03-02
    body: "Where did you downloaded ther files?  (Official Suse ones?)\n\nMust I believe there where no dependency problems?\n\nPlease clarify i more detail with download address and your system setup.\n\n\nRuud koendering\nSr. Project manager"
    author: "ruud koendering"
  - subject: "Icons"
    date: 2003-01-28
    body: "So many new nice icons, but KMail's inbox, outbox and sent-mail icons never change. Are they hard-coded? Just curious."
    author: "Matt"
  - subject: "Re: Icons"
    date: 2003-01-29
    body: "The icons are not hardcoded. The artists simply didn't create nice new icons although we asked more than once.\n\nBTW, you can report also art-related bugs/wishes via bugs.kde.org.\n\nhttp://bugs.kde.org/show_bug.cgi?id=50946 is about the KMail icons. If you vote for this bug report then the artists will probably do something about it.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Icons"
    date: 2003-01-29
    body: "Does anyone know where the icons for inbox, sent, etc. are located at in $KDEDIR?\n\nI could put some out for everyone."
    author: "Antiphon"
  - subject: "Re: Icons"
    date: 2003-01-30
    body: "as most apps kde in /usr/share/apps\nfor kmail is in .../usr/share/apps/kmail/pics\ni changed mine the first time i puted my eyes in the default ones"
    author: "nuno pinheiro"
  - subject: "Awesome Feature Guide!"
    date: 2003-01-28
    body: "The feature guide is world-class. I really like it. It's complete, easy-to-understand, and attractive.\n\nKeep up the good work."
    author: "JJ"
  - subject: "XINE Multimedia does not work"
    date: 2003-01-28
    body: "Hi,\n\nI installed the suse 8.1 rpm:s and xine0.9.13 but i cant get this new multimedia thing to work.. Does anyone know how to enable for exaple noatun to use the xine-libs?\n\nThis was the most wanted feature I had for 3.1 but no luck :-(\n\nBy the way.. 3.1 just feels so solid!!!.. Best KDE release ever.. try keep all next .0 releases at least the same level... Thank you!!\n\nBye"
    author: "KDE"
  - subject: "Re: XINE Multimedia does not work"
    date: 2003-01-29
    body: "Yes, I very much liked the long releas candidate period. That way we had a chance to test the software thoroughly. A couple of weaks is far to short time to test what you intend to release as a solid product.\n\nSo please have longer feature freeze time, so that bugs really have time to get fixed. This extra time that the security audit gave us for this release was perfect."
    author: "Uno Engborg"
  - subject: "XINE Multimedia does not work"
    date: 2003-01-28
    body: "Hi,\n\nI installed the suse 8.1 rpm:s and xine0.9.13 but i cant get this new multimedia thing to work.. Does anyone know how to enable for exaple noatun to use the xine-libs?\n\nThis was the most wanted feature I had for 3.1 but no luck :-(\n\nBy the way.. 3.1 just feels so solid!!!.. Best KDE release ever.. try keep all next .0 releases at least the same level... Thank you!!\n\nBye"
    author: "KDE"
  - subject: "Re: XINE Multimedia does not work"
    date: 2003-02-03
    body: "I never managed to use the xine bin out of a SuSE release... There is a new xinle-lib 1.0beta4 which installs fine on SuSe 8.1. With this release, you have to use the Xine-ui 0.9.19. Both tarballs can be downloaded from http://xinehq.de/. "
    author: "Daniel"
  - subject: "Fish?"
    date: 2003-01-28
    body: "What's the story with this fish:// stuff?\n\nI thought KDE has had scp:// or sftp:// for a while...? How is it different?"
    author: "AC"
  - subject: "Re: Fish?"
    date: 2003-01-28
    body: "The difference is that it is now part of the standard KDE. In previous releases it was a separate download. FISH by the way is the name of the protocol (also used by some other apps).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Fish?"
    date: 2003-01-28
    body: "sftp:// connects to an FTP server through SSL.  fish:// connects to an SSH server to do basically the same thing, but with a few more features.  If you already have an SSH server, and you can't or don't want to run an FTP server on your machine, fish is great."
    author: "Jiffy"
  - subject: "Re: Fish?"
    date: 2003-01-28
    body: "\nAccording to my memory:\n\nsftp:// is secureftp, a kinda-like-ftp-thing related to ssh\nftps:// is ftp-over-ssl\nfish:// is scp (or rcp)"
    author: "Roberto"
  - subject: "Re: Fish?"
    date: 2003-01-28
    body: "SFTP and SCP both require special support from the server. The way FISH works, IIRC, is by using regular SSH login, and running (shell? perl? don't remeber) scripts on other end. So it's probably slower but more flexible - i.e. it work with just about any SSH server"
    author: "SadEagle"
  - subject: "Re: Fish?"
    date: 2003-01-28
    body: "scp only requires a regular ssh login, and that is what fish uses.\n"
    author: "Roberto"
  - subject: "Re: Fish?"
    date: 2003-01-29
    body: "...and an scp binary on the remote host in your path..."
    author: "Adrian"
  - subject: "Re: Fish?"
    date: 2003-01-29
    body: "Well, yeah, but what ssh doesn't install that? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Fish?"
    date: 2003-01-29
    body: "I posted this comment to somebody above under the heading \"is it real?\", but there is something worth noting before you do it with fish. \n\nIf the server you are connecting to has a large distributed file system, the perl process that is run on the server by fish will go crazy and totally hog the CPU and network resources of the server. I haven't looked at the script but it seems like it's running ls -alR or something, so as my university has over 2000 home directories that are split up over a bunch of physical machines, running fish on it is a terrible thing to do. This happens even if you go straight to the directory you want, ie, fish://server/home/myhome/ will still try to resolve every path name at the /home/* level.\n\nSo basically, be careful using fish, or just use sftp://, it's much more stable and works everywhere fish works (in my experience)."
    author: "Elliott"
  - subject: "Re: Fish?"
    date: 2003-01-30
    body: "Any chance that you or someone else could offer a fix/patch for that problem?"
    author: "Datschge"
  - subject: "Re: Fish?"
    date: 2003-04-16
    body: "try this site http://www.outwar.com/page.php?x=558059"
    author: "fish fixer"
  - subject: "Re: Fish?"
    date: 2003-03-19
    body: "use this tip for fish on my site.\n\n\nhttp://www.outwar.com/page.php?x=558059"
    author: "andrew scott"
  - subject: "Thanks to KDE !"
    date: 2003-01-28
    body: "Just a quick thank you from me. I like KDE although I use Gnome. There are two very good desktop environments and I woudn't like to miss one of them.\n\nGeorge"
    author: "Georg "
  - subject: "Re: Thanks to KDE !"
    date: 2003-01-29
    body: "Thanks for letting us know. I guess many of us don't know if there's still something like Gnome, so good you mentioned it. \n\nNote the irony.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Thanks to KDE !"
    date: 2003-01-30
    body: "Of course everyone knows gnome! It's a very good meta search engine! http://www.gnome.com/ :))))\n\nBookmark it ;)"
    author: "Anton Velev"
  - subject: "Good news.  .  .but"
    date: 2003-01-28
    body: "The screenshots underwhelmed me.  If I understand correctly, actual SVG support is still not a reality with this release and the text looks terrible.  Why not have AA turned on for these shots?  Keramik while original has been around quite some time now and not nearly as slick looking as offerings in the windows world.  Don't get me wrong.  .  .I love KDE.  I just don't think the best foot was placed forward for this release."
    author: "Terry"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-28
    body: "I personally like AA off for all but the largest fonts. As for SVG, what do you expect?  For 3.2 to be released before 3.1?  For the work to magically get done?  Perhaps you can provide the KDE team with a time machine.  :)"
    author: "Another Anon-person"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-28
    body: "The KDE team mentioned SVG icons in it's PR material for the 3.1 release, so my comment was quite justified.  As to your preference for running a non AA desktop, it is exactly that. YOUR preference."
    author: "Terry"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-29
    body: "Support for SVG icons has been in the to-do list for 3.2 for quite some time now. I guess you are mixing that up with the fact that the included Crystal icon theme is originally mostly in SVG already which has been statet at many places."
    author: "Datschge"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-30
    body: "Um, running an AA desktop is *your* preference. Why should one be more relevent than the other?"
    author: "Rayiner Hashem"
  - subject: "Anti-aliasing"
    date: 2003-01-28
    body: "Some people don't actually like AA. Hard to believe eh? It seems AA only became the latest \"fad\" after some Windows release added it. Why?"
    author: "Fooman"
  - subject: "Re: Anti-aliasing"
    date: 2003-01-28
    body: "I think one big factor on liking and not liking AA is the monitor (plus the tons of different FreeType versions, etc.). I personally didn't like AA on my previous monitor, which was a fined-pitched CRT. My new LCD panel, however, has much coarser grain and so AA is a must. \n\nIt's also a matter of fonts -- some fonts render horribly w/o AA, too. "
    author: "SadEagle"
  - subject: "Re: Anti-aliasing"
    date: 2003-01-30
    body: "I agree AA on LCD is really orgasmic, nothing to have with CRT !"
    author: "khalid"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-28
    body: "The crystal-icons-svg package works fine for me in kde3.1-rc6... don't know how it is with 3.1final. will not be able to download+compile before friday :("
    author: "matiz"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-29
    body: "> Why not have AA turned on for these shots? \n\nBecause AA is not good for normal font ranges unless you are on a LCD monitor. Windows mimics this behavior, and it's the right thing to do (tm). Apple's behavior in MacOSX and Be's BeOS don't exactly do it right. "
    author: "met"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-29
    body: "Not true. If you are using ClearType, all sizes are anti-aliased. I think that the idea is a good one, provided that the hinting is less at the smaller sizes.\n\nNote: Since the introduction of Plus! for 95, fonts in Windows were antialiased at sizes >=14 and <=6\n\n"
    author: "Antiphon"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-30
    body: "Well, Win 95 had it build in already at that point, the plus package just offered the fitting setting option for users to actually activate it."
    author: "Datschge"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-30
    body: "Very true. But I doubt that anyone did so until Plus came along.\n\nOT: OS/2 never had antialiasing, did it?"
    author: "Antiphon"
  - subject: "Re: Good news.  .  .but"
    date: 2003-01-30
    body: ">  If you are using ClearType, all sizes are anti-aliased.\n\nWell, obviously, since ClearType is made for, and should be really only used in LCD screens :)"
    author: "met"
  - subject: "Panacopia?"
    date: 2003-01-28
    body: "I love the desktop guide.  It does an excellent job of explaining and illustrating the new features.\n\nOne complaint:  On page 7 in the desktop section, it says \"offers users a panacopia of new goodies\".  Panacopia is not a word, at least not an existing word in English.  Do a google or dictionary.com search if you're in doubt.  While I'm all for creative linguistic expression, \"cornucopia\" might be a better choice here.\n\nSorry about the minor complaint.  Congratulations on KDE 3.1 and the excellent guide."
    author: "Grammar Nazi"
  - subject: "Re: Panacopia?"
    date: 2003-01-28
    body: "Perhaps they meant \"Cornucopia\".\n\n"
    author: "Dr_LHA"
  - subject: "Re: Panacopia?"
    date: 2003-01-28
    body: "...and I should really read your posting fully :-)"
    author: "Dr_LHA"
  - subject: "Re: Panacopia?"
    date: 2003-01-29
    body: "Is it a cornucopia that thinks its a panacea, or a panacea that thinks its a cornucopia?"
    author: "repugnant"
  - subject: "Re: Panacopia?"
    date: 2003-01-29
    body: "<i>One complaint: On page 7 in the desktop section, it says \"offers users a panacopia of new goodies\". Panacopia is not a word, at least not an existing word in English. Do a google or dictionary.com search if you're in doubt. While I'm all for creative linguistic expression, \"cornucopia\" might be a better choice here.</i>\n\nI presume they mean <a href=\"http://www.m-w.com/cgi-bin/dictionary?va=panoply\">panoply</a>\n\n-chris"
    author: "Chris Kuhi"
  - subject: "Re: Panacopia?"
    date: 2003-01-29
    body: "But why?? It's a perfectly cromulent word!"
    author: "taj"
  - subject: "Re: Panacopia?"
    date: 2003-01-29
    body: "That remark is absolutely flibin! :-P"
    author: "Antiphon"
  - subject: "Re: Panacopia?"
    date: 2003-01-31
    body: "Yep, I think cornucopia fits better - More in line with a veritable smorgasbord of eye candy and features..."
    author: "Iestyn Guest"
  - subject: "Re: Panacopia?"
    date: 2005-06-05
    body: "Me thinks that shmoopy best defliberizes the entire bitter diatribe that ye are basing your platform on!\n"
    author: "tony "
  - subject: "What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-01-28
    body: "Just like the title says, what's the best way to install all those packages.\nAny particular order?\nA small guide would help a lot of people.\nThanks."
    author: "TABASCO"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-01-28
    body: "What I would suggest, is wait for SuSE to make it available in the Update Service:\n\nhttp://www.suse.com/us/private/download/linuks/index.html\n\nFor now they only offer 3.0.4, but I hope they will have 3.1 soon. Otherwise this service would be pretty useless... On my 7.3 upgrading to 3.0.4 worked without any problems.\n\nDebian caused a strange problem. 3.0.5a was packed in veerrryyy much packages. One should uninstall (dpkg -r) most of them, like kdcop, kscreensaver and so on. If you get dependency problems, first do \"apt-get install kdebase; dpkg -r kpresenter\" and then also remove \"libarts1-qt\". Then it worked for me.\nAs you see, kpresenter depends on \"libarts1-qt\". However, this conflicts with \"libarts1\" from KDE 3.1. So I cannot use KPresenter anymore :(\n\nOr is there a solution for this?"
    author: "Daan"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-01-29
    body: "It seems to me that SuSE has basically stopped providing anything new in its Update Service. They've missed 3.05 and 3.05a, so what are the chances of 3.1?\nHappy waiting!\n  "
    author: "Sceptic"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-01-29
    body: "kde3.0.5a are included in the suse online update via YOU or apt.\nI expect 3.1 appearing in these services soon.\n\nbye\n0x1"
    author: "And thank you for the fish"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-01-29
    body: "Apt with SuSE? \n\nD."
    author: "Donalbain"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-01-29
    body: "see http://linux01.gwdg.de/apt4rpm/\n\nworks fine!"
    author: "Sceptic"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-01-30
    body: "Agree ! apt4rpm is really a must, after using it for updating redaht I am really satified with it."
    author: "khalid"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-02-05
    body: "Sucessfuly installed on three machines so far - no glitches:\n1. download all KDE3.1 rpm's\n2. go to that directory\n3. rpm -Uvh --replacepkgs\n4. run 'SuSEconfig'\n5. restart X-server\ndone\n\nI had two or three packages initially popping up with dependency issue, but I just removed them (gets reinstalled with 3.1 anyway).\nSo far - loving it!"
    author: "Singidunum"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-02-05
    body: "Will this method work for Suse 7.2"
    author: "Dbakez"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-02-09
    body: "I too have 7.2 and would like to upgrade.\n\nI've tried following the directions above:\n\n1. download all KDE3.1 rpm's\n2. go to that directory\n3. rpm -Uvh --replacepkgs\n4. run 'SuSEconfig'\n5. restart X-server\n\nBut it keeps going back into the KDE 2 desktop.  Please help!\n\nThanks,\nKen\n"
    author: "Ken Beal"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-02-12
    body: "And what about SuSe 8.0? Will it work?"
    author: "zgrande"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-06-24
    body: "Here's what did the trick on my SuSE 8.0 workstation (Updated via Y.O.U.) - told in plain english:\n\n-first downloaded all (see below) the packages ;-) - most of them should be available now on SuSE FTP mirrors...\n-used konsole to type 'su' to install as root\n-typed the following:\n\nrpm -Uvh --replacepkgs  kdeadmin3-3.1.2-4.i386.rpm kdeartwork3-3.1.2-20.i386.rpm kdebase3-3.1.2-13.i386.rpm kdebase3-ksysguardd-3.1.2-13.i386.rpm kdebindings3-3.1-44.i386.rpm kdebindings3-mozilla-3.1-44.i386.rpm kdegraphics3-3.1.2-4.i386.rpm kdelibs3-3.1.2-3.i386.rpm kdemultimedia3-3.1.2-1.i386.rpm kdenetwork3-3.1.2-3.i386.rpm kdepim3-3.1.1-104.i386.rpm kdetoys3-3.1.2-5.i386.rpm kdeutils3-3.1.2-5.i386.rpm arts-1.1.2-4.i386.rpm qt3-3.1.2-37.i386.rpm libart_lgpl-2.3.10-97.i386.rpm qt3-non-mt-3.1.2-36.i386.rpm kdeartwork3-3.1.2-20.i386.rpm kdeaddons3-3.1.2-9.i386.rpm quanta-3.1.2-4.i386.rpm arts-1.1.2-4.i386.rpm  kdegames3-3.1.2-3.i386.rpm\n\n(quanta is optional ;))\n\n-typed 'SuSEconfig'\n-logged out of KDE (to restart)\n-logged in again\n-breathed a sigh of relief as it worked :-B\n"
    author: "Freelancer"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-02-22
    body: "it doesnt work\n\nlinux:/home/seb/rpm # rpm -Uvh --replacepkgs\nrpm: Es wurden keine Pakete f\u00fcr die Installation angegeben\n\n\n"
    author: "maxxi"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-02-22
    body: "rpm -Uvh --replacepkgs *.rpm"
    author: "Anonymous"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-02-22
    body: "it doesnt work\n\nlinux:/home/abc/rpm # rpm -Uvh --replacepkgs\nrpm: Es wurden keine Pakete f\u00fcr die Installation angegeben\n\n\n"
    author: "maxxi"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE"
    date: 2003-03-12
    body: "euhm.. did you put the name of the package after \"--replacepkgs\" ? it seems necessary :)"
    author: "ash"
  - subject: "Re: What's the best way to install KDE 3.1 in SUSE 8.1"
    date: 2003-02-17
    body: "negative- I get a whole list of prerequsite packages\n"
    author: "wayne"
  - subject: "YOU ARE THE BEST! :)"
    date: 2003-01-29
    body: "I just wanna say:\" Congratulations and thanks to all the KDE developers, testers and sponsors!\"\nVoglio solo dire: \"Congratulazioni e grazie a tutti gli sviluppatori, i tester e gli sponsor di KDE!\"\nGiovanni"
    author: "Giovanni"
  - subject: "Switch users"
    date: 2003-01-29
    body: "One feature I would love to see is a switch users feature similar to winxp and xandros. "
    author: "Jarefri"
  - subject: "Re: Switch users"
    date: 2003-01-29
    body: "I'm not familliar to windows XP or Xandros.  But I guess that you want to be able to take over the machine and use your own standard user environment (as if you were logged in yourself) without having the first user to logout.\n\nThis is possible at least if you run Linux and XFree86, just start more Xservers and run kdm or xdm\n\nE.g. \n\nX :1  -query yourkdmserver  vt08\nX :2  -query  yourkdmserver vt09\nX :3  -query yourkdmserver vt10\n\netc...\n\nTo make them available every time you start Linux start them in your /etc/inittab file.\n\n\nThen you can switch between the different logins by using\nctrl-alt F8\nctrl-alt F9\nctrl-alt F10\n\nThe default server is on ctrl-alt F7\n\nThis is also very useful if you have many different machines and want to switch between them quickly without logging out.\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: Switch users"
    date: 2003-01-29
    body: "Note that changing virtual consoles is Linux-specific.\n\nIf you want to script changes, you should use the \"chvt\" command.  Type the URL \"man:chvt\" into Konqueror for more info.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Switch users"
    date: 2003-01-29
    body: "I think you can do it in FreeBSD too, I have not tested though"
    author: "Uno Engborg"
  - subject: "Re: Switch users"
    date: 2003-01-29
    body: "Is this Suse specific?\n\nI have in my K-Menu the entry \"Start new session...\", which indeed places a new login screen on vt08.\n\n\ngood luck\nAnd"
    author: "And thank you for the fish"
  - subject: "Re: Switch users"
    date: 2003-01-31
    body: "Yes it is SuSE specific, it an automated way to do something similar to what Uno_Engborg suggested."
    author: "nonamenobody"
  - subject: "Re: Switch users"
    date: 2006-10-10
    body: "tell me about switch user in fedora linux "
    author: "Sheelu"
  - subject: "Here's how I've done it in Mandrake..."
    date: 2003-02-05
    body: "Not as slick as XP or Xandros...\n\nBut I do it in Mandrake 9.0 this way: \nCtrl-alt-F1           \n   gets you a login.\n   Login the new user\n% startx -- :1  \n\nThat will launch your new x session...(and use 2, 3, 4 for subsequent ones. 0 is the one you started from)\n\nThen use Ctrl-Alt-F7/F8/F9/F10 to rotate through the users, where F7=0, F8=1, etc etc.\n\nDerek\ncid at isoc dot net\n"
    author: "cid"
  - subject: "Re: Here's how I've done it in Mandrake..."
    date: 2006-09-07
    body: "Works on fedora as well... thanks"
    author: "Sujay"
  - subject: "atlast! gimme gimme gimme!"
    date: 2003-01-29
    body: "w00t!\nIt's there.\nIt's all mine!!!!\nmwuhaahaahaaa :D\n\nYeah, I'm going crazy. I've been waiting so long for this =].\n\nThanks a ton KDE team, you are the best.\n\nES"
    author: "EvilSmile"
  - subject: "kwallet?"
    date: 2003-01-29
    body: "Does kwallet actually compile under any situation? If so, what are the prerequisites? The requirements page mentions nothing...\n"
    author: "Richard"
  - subject: "Re: kwallet?"
    date: 2003-01-29
    body: "No, it doesn't compile because it's not finished yet. Maybe for 3.2...."
    author: "Chris  Howells"
  - subject: "Re: kwallet?"
    date: 2003-01-29
    body: "Thanks for the info!"
    author: "Richard"
  - subject: "Re: kwallet?"
    date: 2003-01-30
    body: "For your info, we are working on this more aggressively and it should be done for 3.2.0.\n\n"
    author: "George Staikos"
  - subject: "Gniiiii !!!"
    date: 2003-01-29
    body: "Mom ! mom ! I wan't KDE 3.1 on my RedHat 8.0 !\nbut I don't want the redhatized KDE version ! I want the real KDE(c)(tm) !"
    author: "olf"
  - subject: "Re: Gniiiii !!!"
    date: 2003-01-30
    body: "Use Konstruct, see a previous thread, works fine for people who tried it"
    author: "khalid"
  - subject: "debian packages, kdeartwork?"
    date: 2003-01-29
    body: "I've installed kde3.1 on my sid-debian box from official kde-packages. but there is no kdeartwork packages or I missed something?"
    author: "Konstantin"
  - subject: "Re: debian packages, kdeartwork?"
    date: 2003-01-29
    body: "Fetch Ralf's packages directly from his stie at ktown, you'll find kdeartwork there as well as the new \u00c4gypten stuff for kMail and a wealth of other pre-packages KDE 3 programs for Debian.\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Aououououwww :-P"
    date: 2003-01-29
    body: "Je fumes ! Ou c'est qu'on download les packages pour Mandrake 9 ?\n\nGreat work !"
    author: "Manu"
  - subject: "Re: Aououououwww :-P"
    date: 2003-01-29
    body: "Y en a po :(\n\nEt je ne suis pas sur que Mandrake peut faire grand chose maintenant, etant en cessation de paiements."
    author: "JC"
  - subject: "Re: Aououououwww :-P"
    date: 2003-01-30
    body: "Utilise Konstruct Cf. les messages suivants : \n\nhttp://dot.kde.org/1043732923/1043834917/1043838444/\nhttp://dot.kde.org/1043732923/1043739939/1043857283/ "
    author: "khalid"
  - subject: "Re: Aououououwww :-P"
    date: 2003-01-29
    body: "Le c\u00e9l\u00e8bre Texstar fournit beaucoup de packages pour Mandrake. Il a dit qu'il les fournira quand Mandrake mit KDE 3.1 en Cooker.\n\nRegardez www.pclinuxonline.com en jours suivants.\n\nJe suis d\u00e9sol\u00e9 parce que le fran\u00e7ais n'est pas ma premi\u00e8re langue."
    author: "Antiphon"
  - subject: "Totally satisfied."
    date: 2003-01-29
    body: "Wow.\n\nYesterday i was a RedHat 8.0 user, concirned with (/terrified by) the way RedHat modifies common GNU/Linux standards and KDE 3.0.3 on it's own -design sickening- Bluecurve way. Overall happy with the system. Stable and complete.\n\nKDE 3.1 came out.......\n\nLike tradition....no RedHat RPM's.\n\nLike always... too impatient to compile QT/KDE myself.\n\nToday i am a SuSE 8.1 user. After breaking some SuSE-ish \"user-friendly shell's\", and upgrading to KDE 3.1, i cannot imagine being happy as this with my previous setup. KDE 3.1 just rocks. SuSE has a overal slick design. \n\nFor anyone that has not upgraded to 3.1 yet,\n\nIt's worth it!\n\n\nRegards to the KDE team,\n\nScythe.\n\n"
    author: "Scythe"
  - subject: "Re: Totally satisfied."
    date: 2003-01-29
    body: "This is typical redhat! But there is help if Your computer have a fast CPU and a lot of memory: Download the NEWEST version of konstruct. from \n\nhttp://konsole.kde.org or http://apps.kde.com:\n\nhttp://apps.kde.com/na/2/counter/vid/8740/dld0/konstruct-20030128.tar.bz2\n\nThe new version of konstruct behaves friedly under RH 8.\n\nInstall the multimedia packages from \n\nhttp://psyche.freshrpms.net \n\ninclude the alsa-libs and alsa-kernel!\n\nUnpack konstruct as an ordinary user. DO NOT DO THAT AS ROOT!\n\n$ tar xvfj konstruct-2003XXXX.tar.bz2\n\nMake sure that You are connected with the internet. I hope You have a reliable and good bandwithed line (DSL and so on).\n\nThen go in the directory ~/konstruct/meta/kde and say:\n\n$ make install\n\nThen wait a really long time and look at eventually compile errors.  Tips:\n\nThere is a bug in libxml2. In the files nanohttp.c and nanoftp.c is a following definition:\n\n#DEFINE SOCKLEN_T unsigned int\n\nBut the declarations\n\nSOCKLEN_T len;\n\nfail. Make the quick and dirty change to\n\nunsigned int len;\n\nAnd the compilition go again ... A \"long time\" means 6 - 36 h depending from the performance of Your system. But it's only a slight delay as the waiting for the RH packagers.\n\nI don't believe that RH will release RPMS in the next days. Perhaps I could help You.\n\nMagnar Hirschberger"
    author: "Magnar Hirschberger"
  - subject: "Re: Totally satisfied. Redhat Menus"
    date: 2003-02-05
    body: "How do you restore the Redhat Menus after using konstruct? I have tried KAppfinder but it doesn't \nfind everything?\n\nThanks in advance"
    author: "tagbo okoli"
  - subject: "Re: Totally satisfied. Redhat Menus"
    date: 2003-02-05
    body: "It's impossible. RedHat menus are an (incompatible with anything) source modification"
    author: "SadEagle"
  - subject: "Re: Totally satisfied."
    date: 2003-02-02
    body: "This sounds like a trend.\n\nI, too, am a recent RedHat defector.  I used RedHat exclusively from 4.0 all the way to 8.1beta2, then I said \"to hell with it\" and switched to SuSE 8.1.\n\nThe reason?  KDE.  I plan on paying for 8.2 to show my support (yes, I'm too much of a snob to pay for anything as old as 8.1)"
    author: "ac"
  - subject: "compilation failure on kdelibs"
    date: 2003-01-29
    body: "Got compilation error on kdelibs:\ntook the last QT (3.1.1 or something like that), compiled arts, and then kdelibs.\nHere is the error:\n\nmake[3]: Entering directory `/opt/src/kdelibs-3.1/dcop'\n/bin/sh ../libtool --silent --mode=link --tag=CXX g++  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -O3 -mpentiumpro -fno-exceptions -fno-check-new  -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_COMPAT    -o libDCOP.la.closure libDCOP_la_closure.lo -R /opt/kde3/lib -R /usr/X11R6/lib -version-info 5:0:1 -no-undefined -L/usr/X11R6/lib -L/opt/kde3/lib  dcopstub.lo dcopref.lo dcopobject.lo dcopclient.lo KDE-ICE/libkICE.la -lqt-mt  -lpng -lz -lm -lXext -lX11  -lSM -lICE -lpthread \nlibtool: link: warning: `-version-info' is ignored for programs\n.libs/dcopclient.o: In function `DCOPClient::applicationRegistered(QCString const &)':\n.libs/dcopclient.o(.text+0x8859): undefined reference to `static_QUType_varptr'\n.libs/dcopclient.o(.text+0x8860): undefined reference to `QUType_varptr::set(QUObject *, void const *)'\n.libs/dcopclient.o: In function `DCOPClient::applicationRemoved(QCString const &)':\n.libs/dcopclient.o(.text+0x8979): undefined reference to `static_QUType_varptr'\n.libs/dcopclient.o(.text+0x8980): undefined reference to `QUType_varptr::set(QUObject *, void const *)'\n.libs/dcopclient.o: In function `__static_initialization_and_destruction_0':\n.libs/dcopclient.o(.text+0x8cea): undefined reference to `QMetaObjectCleanUp::QMetaObjectCleanUp(char const *, QMetaObject *(*)(void))'\n.libs/dcopclient.o(.data+0x70): undefined reference to `static_QUType_varptr'\n.libs/dcopclient.o(.data+0x8c): undefined reference to `static_QUType_varptr'\n.libs/dcopclient.o: In function `QPtrList<DCOPObjectProxy>::replace(unsigned int, DCOPObjectProxy const *)':\n.libs/dcopclient.o(.QPtrList<DCOPObjectProxy>::gnu.linkonce.t.replace(unsigned int, DCOPObjectProxy const *)+0x26): undefined reference to `QGList::replaceAt(unsigned int, void *)'\n.libs/dcopclient.o: In function `QPtrList<DCOPClientTransaction>::replace(unsigned int, DCOPClientTransaction const *)':\n.libs/dcopclient.o(.QPtrList<DCOPClientTransaction>::gnu.linkonce.t.replace(unsigned int, DCOPClientTransaction const *)+0x26): undefined reference to `QGList::replaceAt(unsigned int, void *)'\n.libs/dcopclient.o: In function `QPtrList<_IceConn>::replace(unsigned int, _IceConn const *)':\n.libs/dcopclient.o(.QPtrList<_IceConn>::gnu.linkonce.t.replace(unsigned int, _IceConn const *)+0x26): undefined reference to `QGList::replaceAt(unsigned int, void *)'\ncollect2: ld returned 1 exit status\nmake[3]: *** [libDCOP.la.closure] Error 1\nmake[3]: Leaving directory `/opt/src/kdelibs-3.1/dcop'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/opt/src/kdelibs-3.1/dcop'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/opt/src/kdelibs-3.1'\nmake: *** [all] Error 2\n\n\nanyone got a workaround ??"
    author: "Mickael Bailly"
  - subject: "Re: compilation failure on kdelibs"
    date: 2003-01-29
    body: "Try recompiling qt too first. Maby you have an old version of qt or a version that was built with a different compiler?\n\nDid you 'make clean' in kdelibs first ?"
    author: "perraw"
  - subject: "Re: compilation failure on kdelibs"
    date: 2003-01-30
    body: "My QT version is the last QT version out (3.1.1)\nI found the problem:\nthe QT libraries were not given to the compiler\n\ng++ -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -O3 -mpentiumpro -fno-exceptions -fno-check-new -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_COMPAT -o libDCOP.la.closure libDCOP_la_closure.lo -R /opt/kde3/lib -R /usr/X11R6/lib -version-info 5:0:1 -no-undefined -L/usr/X11R6/lib -L/opt/kde3/lib dcopstub.lo dcopref.lo dcopobject.lo dcopclient.lo KDE-ICE/libkICE.la -lqt-mt -lpng -lz -lm -lXext -lX11 -lSM -lICE -lpthread \n\nthere were no reference to \"-L/opt/qt3/lib\"\n\nNow I hope I could compile KDE 3.1!!!\n"
    author: "Mickael BAILLY"
  - subject: "startkde error"
    date: 2003-01-29
    body: "Can anyone help here?\n[root@localhost lib]# startkde\nstartkde: Starting up...\nICE default IO error handler doing an exit(), pid = 2735, errno = 2\nkdeinit: Communication error with launcher. Exiting!\nkdeinit: relocation error: /usr/lib/libXft.so.2: undefined symbol: FcPatternAddI\nteger\nstartkde: Could not start kdeinit. Check your installation.\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nksplash: relocation error: /usr/lib/libXft.so.2: undefined symbol: FcInit\nWarning: connect() failed: : Connection refused\nLoading required GL library /usr/X11R6/lib/libGL.so.1.2\nksmserver: relocation error: /usr/lib/libXft.so.2: undefined symbol: FcInit\nstartkde: Shutting down...\nWarning: connect() failed: : Connection refused\nError: Can't contact kdeinit!\nartsshell: /usr/lib/libasound.so.2: no version information available (required b\n /usr/lib/libartsflow.so.1)\nstartkde: Running shutdown scripts...\nstartkde: Done.\n[root@localhost lib]# "
    author: "anon"
  - subject: "Re: startkde error"
    date: 2003-01-29
    body: "xft not installed or wrong version ?\narts problem ?"
    author: "JC"
  - subject: "Re: startkde error"
    date: 2003-01-29
    body: "Solved ;) Anyway, thanks. That was an XFree problem, old libs remained from the previous version."
    author: "anon"
  - subject: "Re: startkde error"
    date: 2003-04-14
    body: "What was the issue exactly? I have just finished installing XF86 4.3.0 and can't start *anything* right now.\n\njon"
    author: "Jon"
  - subject: "Re: startkde error"
    date: 2003-04-14
    body: "John: Try running fc-cache -f -v as root.\n"
    author: "Sad Eagle"
  - subject: "Re: startkde error"
    date: 2003-01-29
    body: "Appears like an RedHat 8 Problem with the fontconfig Package. Reinstall fontconfig AND fontconfig-devel. I hope You didn't compile KDE 3.1 before ... then You MUST recompile KDE completely ...\n\nMagnar Hirschberger"
    author: "Magnar Hirschberger"
  - subject: "Re: startkde error"
    date: 2003-07-19
    body: "We've got the same problem after upgrading from Mandrake 9 to Mandrake 9.1.\n\nKDE and gnome don't work.\n\nIcewm works great though...\nand so does XFC\n\nwe ran the fc-cache program... and it said success...\n\nbut it still doesn't work.\n\nThis is on a dell laptop with an ati rage mobility"
    author: "Aaron Peterson"
  - subject: "Mandrake RPMS"
    date: 2003-01-29
    body: "Hi\n\nThe KDE 3.1 stuff looks great, the only problem I have is that I am stuck with Mandrake.\n\nI bailed out on Redhat, after 5+ years, mainly because of its KDE policy, tried Debian for 1 year, great for servers, and moved to Mandrake 9 two months ago for better sound/video control. Now due to Mandrake's financial problems, no KDE3.1 rpms till March!\n\nOptions are \n- build my own\n- go back to debian\n- try Suse\n\nAny advice ?\n\nSimon"
    author: "SimonW"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "Debian, Slackware, Gentoo\nor LinuxFromScratch ..."
    author: "JC"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "yes, but LFS and Gentoo have no configuration tools : have you already try to configure X under gentoo ?"
    author: "capit igloo"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "Doesn't webmin work on gentoo?"
    author: "Roberto Alsina"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "webmin is cool, but it doesn't help for configuring XFree86 :\nhttp://www.webmin.com/standard.html"
    author: "capit igloo"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "\nTo configure xfree86, you have xf86config. Old and reliable ;-)\nOr xf86cfg, new and.. well, not THAT reliable.\nOr XF86Setup, which I don't know if it's there anymore.\nOr you can get XConfigurator (I don't know if gentoo has it in portage)"
    author: "Roberto Alsina"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "Or you simply copy some config files from your current Mandrake installation. Edit the paths if necessary, and you're up and running. Did it when I moved from Debian...."
    author: "Ruud"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "XConfigurator is not for redhat only?"
    author: "capit igloo"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "XConfigurator is just a text-mode program that reads /proc, asks a couple of questions and generates a text file. How distr-specific can it be? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "> Gentoo have no configuration tools \n\nit doesn't? I count at least four X configuration tools in gentoo:\n\nkxconfig \nXautoconfig \nXFree86 -configure (aka Xconfigure)\nxeasyconf\n\nThere's probably more.. i just briefely looked at /usr/portage/x11-misc"
    author: "met"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "hum, hum :\n\nXautoconfig -> doesn't compile\n\nXFree86 -configure (aka Xconfigure) -> kill my system\n\nxeasyconf -> I have a wonderful \"Fatal server error\" at the beginning\n\nkxconfig -> where do you see that ebuild ?"
    author: "capit igloo"
  - subject: "Re: Mandrake RPMS"
    date: 2006-09-17
    body: "\nemerge mkxf86config"
    author: "Craig"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-30
    body: "1 word: vim. :-P"
    author: "John Hughes"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "I am sure Texstar is going to upload them soon. Read www.pclinuxonline.com for updates."
    author: "anon"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "Me too!!!! Where are the Mdk RPMs????\n\nI liked the policy Mdk had before...\n\n"
    author: "Andre"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "Perhaps the building process becomes doable using this: http://konsole.kde.org/konstruct/ ?"
    author: "IR"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-29
    body: "Mandrake 9.0, too eager to try kde 3.1 (specifically that kio_fish thing .. way cool lookin') to wait for the Mandrake RPMS, so I tried the konstruct way of doing things.\n\nRan into snags building K3B and pixelmagic, but for the most part was okay - but it looks like there's some stuff missing from my freshly compiled KDE .. most notably, no kmail - which can't be a good sign.\n\nSo I'm waiting for the RPMs, which I'm quite sure will be out well before March.  Here's hoping by week's end.  Texstar will bail us out if Mandrakesoft can't for some reason (praise be!)\n"
    author: "Mick Szucs"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-30
    body: "Well, I have just bailed out of Mandrake.\n\n4 hours to download Suse81, 1 hour to rebuild MySQL, Web services, and get email working again properly, then 25 minutes and KDE3.1 is working.\n\nMagnificent!!\n\nIt will take a few days to get used to Suse, its is a bit different to RH/Mandrake and Debian, but not too different.\n\nSimon"
    author: "SimonW"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-30
    body: "You could do what I did.  I added one of the cooker ftp sites as a source for RPMs and then installed the KDE3.1 packages from there.  It will take care of the many dependency issues that will pop up.  I believe I ended up upgrading 104 packages to get KDE 3.1 running on my Mandrake 9.0 box, but its running and I've got a big pipe in to my computer, so downloading all of the packages wasn't a big deal.\n\nAnd KDE 3.1 rocks!!\n\nPS Attached a screen shot if you don't believe me.. ;-)"
    author: "Jim Erickson"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-30
    body: "Be careful.\nI am running the one from cooker as well and it took me > 8 hours to set-up (just set-up, not download). I should go back to CVS version and be done with it.\nA couple of problems with the mandrake cooker\n1) do not have Xft libs hanging around.\n2) before updateing the X stuff, move to runlevel 3 (I had more problems than I can shake a stick at - took me 5 hours to straighten that up, as it kept rebooting my system. total reboots.  I suggest using the \"XFree86 -configure\" as a working starting point, if you have the same problems. )\n3) apparently, kde did not like my earlier configs in .kde you may wish to move them aside and start from scratch again, if you are having any real probems.\n4) the kdmrc file is different so make sure that a new one is used rather than the old\n/usr/share/config/kdm/kdmrc\n5) unfortantly, I think that mandrake is either out of techies or doing a redhat. part of the keramick is in place, but not all of it. \n\n\nNow, for the pros:\nsome of the items that Mandrake use to hide are back. Hopefully that is not due to the fact that it takes a ton of time for them to hide them.\nkfontinstaller is 1000% better than the gtk stuff.\n"
    author: "a.c."
  - subject: "Re: Mandrake RPMS"
    date: 2003-02-02
    body: "Thanks A.C. I ended up giving up and uninstalling XFree86 and 4.2.99.5 installing 4.2.1. I used red carpet to do it.  And with some minor tweeking I'm about back to where I was before. I had to reinstall openoffice and for some reason, I have to login as root to use my Mandrake control center.  Or I can \"su\" and run it from a terminal.  I've allso noticed some rendering bugs that wheren't there before but they're minor.  At least I have a usable system again.  I've had to switch completly to gnome though.  I hate the thought of having to reconfigure this computer again I have so many hours in it."
    author: "Heather"
  - subject: "Re: Mandrake RPMS"
    date: 2003-01-30
    body: "Be careful.\nI am running the one from cooker as well and it took me > 8 hours to set-up (just set-up, not download). I should go back to CVS version and be done with it.\nA couple of problems with the mandrake cooker\n1) do not have Xft libs hanging around.\n2) before updateing the X stuff, move to runlevel 3 (I had more problems than I can shake a stick at - took me 5 hours to straighten that up, as it kept rebooting my system. total reboots.  I suggest using the \"XFree86 -configure\" as a working starting point, if you have the same problems. )\n3) apparently, kde did not like my earlier configs in .kde you may wish to move them aside and start from scratch again, if you are having any real probems.\n4) the kdmrc file is different so make sure that a new one is used rather than the old\n/usr/share/config/kdm/kdmrc\n5) unfortantly, I think that mandrake is either out of techies or doing a redhat. part of the keramick is in place, but not all of it. \n\n\nNow, for the pros:\nsome of the items that Mandrake use to hide are back. Hopefully that is not due to the fact that it takes a ton of time for them to hide them.\nkfontinstaller is 1000% better than the gtk stuff.\n"
    author: "a.c."
  - subject: "Panel Menu not functioning"
    date: 2003-01-29
    body: "Hmm, strange I wonder what happened to the \"Panel Menu\" since RC6.\nNow it seams that it is impossible to add applets to the panel.\nAnybody else having this problem?\ni"
    author: "Uno Engborg"
  - subject: "Re: Panel Menu not functioning"
    date: 2003-01-29
    body: "Are you using the panel auto hide?  I experienced this with auto hide turned on, once the panel had hidden itself, when it appeared again the panel menu didn't work.\n\nWill"
    author: "Will Stephenson"
  - subject: "Re: Panel Menu not functioning"
    date: 2003-01-29
    body: "If you mean right-click on a free area in the panel and then Add->Applet:\nThis works fine on my system. I have just downloaded RMPS for Suse7.1.\nEven my own applets, originally written for KDE 3.0 work fine after recompilation.\n\n"
    author: "Tom"
  - subject: "Re: Panel Menu not functioning"
    date: 2003-02-02
    body: "It works her to now, I really don't know what I did to make it work\nStrange"
    author: "Uno Engborg"
  - subject: "KDE 3.1 on Redhat 8.0..."
    date: 2003-01-29
    body: "...is available through Rawhide. But it's supposed to be unstable and \"for testing only\". Works quite well for me.\n\nMy KDE says it's \"Release 3.1-0.16 Redhat\".\n\nNOTE: you'll have to download at least 300-400 Mb of packages, because they have newer glibc/gcc in Rawhide than in 8.0.\n\nOf course, if KDE team / someone else will _ever_ build \"real\" KDE and stop shitting about Redhat - i'll gladly install \"real\" packages.\n\nP.S. Redhat made _more_ than anyone else for Linux linux promotion."
    author: "Pavel P."
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-29
    body: "> Of course, if KDE team / someone else will _ever_ build \"real\" KDE\n\nYou can try konstruct.. the KDE project itself only distributes source code.\n\n> P.S. Redhat made _more_ than anyone else for Linux linux promotion.\n\nKDE has nothing to do with Linux other than the fact that it's one of the supported platforms. I for one hope that linux dies, and the BSD's go back to being the dominant UNIX(-like OS). I'm happily using KDE on FreeBSD."
    author: "met"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-29
    body: "> I for one hope that linux dies, and the BSD's go back to being the dominant UNIX(-like OS).\n\nI like FreeBSD as well, but I don't hope Linux dies - actually I'm using both. Why so negative? More users of *NIX like OS'es and more competition between the OS'es is a good thing, surely. Or?\n\n"
    author: "someone"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-30
    body: "Actually not linux, GPL has to die. People should free their software from any gpl infection.\n\nAnd at this moment BSD flavors are closer in this direction."
    author: "Anton Velev"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-30
    body: "\"Actually not linux, GPL has to die. People should free their software from any gpl infection.\n\nAnd at this moment BSD flavors are closer in this direction.\"\n\nI prefer that the whole community benefits from the code. I wouldn't want KDE or anyone else to work as a free R&D-lab for corporations.\n\nIf you don't like GPL, then you don't have to use it! Just don't steal code from GPL-project and you are all clear. GPL only affects you if you use code that's under the GPL. And no-one is forcing you to do so. After all, doesn't the creator of the code have the right to choose his license? Who are you to deny the developers that right? And doesn't the creator of the code have the full right to choose GPL instead of BSD? Maybe he wants his code to remain free, and not to be stolen by some company (like Microsoft has done with some BSD-code)?"
    author: "Janne"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-30
    body: "OK. You have one or some problems with the GPL.\n\nThen You looking for a mailing thread have absolute nothing to do Your problem, turned off thinking ... You wrote one of this kind of useless hate postings - totally OT as a matter of course! The question was here: \"When Redhat will release KDE 3.1 final RPMs?\" and not: \"You are happy with GPL or BSD or Artistic or god knows what for a license?\".\n\nBTW, to the thread leader posting:\n\nThe Redhat KDE policy makes me mad, this appears like a boycott ...\n\nThere are an project on sourceforge.net for KDE on Redhat Linux, but this have only packages for Valhalla (7.3) and not for Psyche (8.0). Really pitty!\n\nPerhaps Konstruct could help! But read this full list; You can read a posting about the problem with qt and Xft2 could avoid font antialiasing ...\n\nMagnar Hirschberger"
    author: "Magnar Hirschberger"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-31
    body: "I installed the binary packages from the kde-redhat project under RH 8.0.  You can find the binaries on: http://sourceforge.net/project/showfiles.php?group_id=65974\n\nThe only thing I had to do is upgrading redhat-artwork if I remember well.  I took 0.59, but the ones you can find on http://apt.unl.edu/7.3/i386/RPMS.kde31-test/ should work too.  They are from the kde-redhat team.\n\nThen I upgraded QT, arts, and all the KDE packages.  And it worked ;-))"
    author: "Frido Roose"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-31
    body: "Hmm, it is working with the compat-libs packages ... i'm really stunned. \n\nI will try that and hope that works for me without damages.\n\nBut the problem isn't solved really.\n\nMagnar Hirschberger"
    author: "Magnar Hirschberger"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-02-01
    body: "Is this binary compatible with KDE 3.0.x, and will mofset liquid work?"
    author: "Brian Wright"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-02-04
    body: "Hi,\n\nthis are the error messages that I get if I try to install kde-3.1 packages in testmode: rpm -i --test kde*.rpm\n\n        SDL >= 1.2.0 is needed by kdeaddons-3.1-1\n        knewsticker >= 3.1 is needed by kdeaddons-3.1-1\n        libartsflow-gcc2.96.so.1 is needed by kdeaddons-3.1-1\n        libartsflow_idl-gcc2.96.so.1 is needed by kdeaddons-3.1-1\n        libkmedia2_idl-gcc2.96.so.1 is needed by kdeaddons-3.1-1\n        libmad.so.0 is needed by kdeaddons-3.1-1\n        libmcop-gcc2.96.so.1 is needed by kdeaddons-3.1-1\n        libqtmcop-gcc2.96.so.1 is needed by kdeaddons-3.1-1\n        libSDL-1.2.so.0 is needed by kdeaddons-3.1-1\n        libsoundserver_idl-gcc2.96.so.1 is needed by kdeaddons-3.1-1\n        libXm.so.1 is needed by kdebase-3.1-1\n        libartsflow-gcc2.96.so.1 is needed by kdebase-3.1-1\n        libartsflow_idl-gcc2.96.so.1 is needed by kdebase-3.1-1\n        libkmedia2_idl-gcc2.96.so.1 is needed by kdebase-3.1-1\n        libmad.so.0 is needed by kdebase-3.1-1\n        libmcop-gcc2.96.so.1 is needed by kdebase-3.1-1\n        libqtmcop-gcc2.96.so.1 is needed by kdebase-3.1-1\n        libsoundserver_idl-gcc2.96.so.1 is needed by kdebase-3.1-1\n        lisa < 3.1 conflicts with kdebase-3.1-1\n        sane-backends >= 1.0.3-10 is needed by kdegraphics-3.1-1\n        libglut.so.3 is needed by kdegraphics-3.1-1\n        libImlib.so.1 is needed by kdegraphics-3.1-1\n        libsane.so.1 is needed by kdegraphics-3.1-1\n        arts >= 8:1.1 is needed by kdelibs-3.1-1\n        libartsflow-gcc2.96.so.1 is needed by kdelibs-3.1-1\n        libartsflow_idl-gcc2.96.so.1 is needed by kdelibs-3.1-1\n        libkmedia2_idl-gcc2.96.so.1 is needed by kdelibs-3.1-1\n        libmad.so.0 is needed by kdelibs-3.1-1\n        libmcop-gcc2.96.so.1 is needed by kdelibs-3.1-1\n        libqtmcop-gcc2.96.so.1 is needed by kdelibs-3.1-1\n        libsoundserver_idl-gcc2.96.so.1 is needed by kdelibs-3.1-1\n        qt-devel >= 3.1.1 is needed by kdelibs-devel-3.1-1\n        arts-devel >= 7:1.0.5a is needed by kdelibs-devel-3.1-1\n        pcre-devel is needed by kdelibs-devel-3.1-1\n        cdparanoia is needed by kdemultimedia-3.1-1\n        lame is needed by kdemultimedia-3.1-1\n        xine is needed by kdemultimedia-3.1-1\n        libartsflow-gcc2.96.so.1 is needed by kdemultimedia-3.1-1\n        libartsflow_idl-gcc2.96.so.1 is needed by kdemultimedia-3.1-1\n        libkmedia2_idl-gcc2.96.so.1 is needed by kdemultimedia-3.1-1\n        libmad.so.0 is needed by kdemultimedia-3.1-1\n        libmcop-gcc2.96.so.1 is needed by kdemultimedia-3.1-1\n        libmp3lame.so.0 is needed by kdemultimedia-3.1-1\n        libqtmcop-gcc2.96.so.1 is needed by kdemultimedia-3.1-1\n        libsoundserver_idl-gcc2.96.so.1 is needed by kdemultimedia-3.1-1\n        pilot-link >= 0.11.3 is needed by kdepim-3.1-1\n        libpisock.so.8 is needed by kdepim-3.1-1\n\nMy installed gcc version is 3.2.7 so what can I do to use the new KDE-Version?\n\nthx in advance\nThomas\n\n"
    author: "Thomas"
  - subject: "Re: KDE 3.3 on Fedora"
    date: 2004-09-13
    body: "I also get this error messages when I try to install kde-3.3 packages in testmode: rpm -i --test kde*.rpm\n\n        fortune-mod is needed by kdetoys-3.3.0-1.0.2.kde\n        graphviz is needed by kdesdk-3.3.0-1.2.2.kde\n        libgnokii.so.2 is needed by kdepim-3.3.0-1.1.2.kde\n        libHalf.so.2 is needed by kdegraphics-3.3.0-1.1.2.kde\n        libHalf.so.2 is needed by kdelibs-3.3.0-2.0.2.kde\n        libidn.so.11 is needed by kdeadmin-3.3.0-1.0.2.kde\n        libidn.so.11 is needed by kdeartwork-3.3.0-1.0.2.kde\n        libidn.so.11 is needed by kdeedu-3.3.0-1.0.2.kde\n        libidn.so.11 is needed by kdegames-3.3.0-1.0.2.kde\n        libidn.so.11 is needed by kdegraphics-3.3.0-1.1.2.kde\n        libidn.so.11 is needed by kdelibs-3.3.0-2.0.2.kde\n        libidn.so.11 is needed by kdenetwork-3.3.0-1.1.2.kde\n        libidn.so.11 is needed by kdetoys-3.3.0-1.0.2.kde\n        libidn.so.11 is needed by kdeutils-3.3.0-1.0.2.kde\n        libIex.so.2 is needed by kdegraphics-3.3.0-1.1.2.kde\n        libIex.so.2 is needed by kdelibs-3.3.0-2.0.2.kde\n        libIlmImf.so.2 is needed by kdegraphics-3.3.0-1.1.2.kde\n        libIlmImf.so.2 is needed by kdelibs-3.3.0-2.0.2.kde\n        libImath.so.2 is needed by kdegraphics-3.3.0-1.1.2.kde\n        libImath.so.2 is needed by kdelibs-3.3.0-2.0.2.kde\n        libjasper-1.701.so.1 is needed by kdelibs-3.3.0-2.0.2.kde\n        liblcms.so.1 is needed by kdegraphics-3.3.0-1.1.2.kde\n        libmad.so.0 is needed by kdemultimedia-3.3.0-1.1.2.kde\n        libmal.so.0 is needed by kdepim-3.3.0-1.1.2.kde\n        libmp3lame.so.0 is needed by kdemultimedia-3.3.0-1.1.2.kde\n        libsamplerate.so.0 is needed by kdemultimedia-3.3.0-1.1.2.kde\n        libsamplerate.so.0(libsamplerate.so.0.0) is needed by kdemultimedia-3.3.0-1.1.2.kde\n        libsdp.so.2 is needed by (installed) gnome-bluetooth-0.4.1-7\n        libsdp.so.2 is needed by (installed) libbtctl-0.3-5.1\n        libslp.so.1 is needed by kdenetwork-3.3.0-1.1.2.kde\n        libtag.so.1 is needed by kdemultimedia-3.3.0-1.1.2.kde\n        libxine.so.1 is needed by kdemultimedia-3.3.0-1.1.2.kde\n        openoffice.org-i18n < 0:1.1.2-3 conflicts with openoffice.org-1.1.2-3.1.2.kde\n        qt-styles is needed by kdeartwork-3.3.0-1.0.2.kde\n        themer >= 0:1.60 is needed by redhat-artwork-0.98-1.0.2.kde\n        themer >= 1.60 is needed by redhat-artwork-0.98-1.0.2.kde\n        themer is needed by kdebase-3.3.0-3.0.2.kde\n        trm is needed by kdemultimedia-3.3.0-1.1.2.kde\n        valgrind >= 0:2.0.0 is needed by kdesdk-3.3.0-1.2.2.kde\n libmad.so.0 is needed by arts-1.3.0-3.0.2.kde\n\nHave you solved this problem ????\n\nthanks in advance\nHenrik"
    author: "Henrik"
  - subject: "Re: KDE 3.3 on Fedora"
    date: 2004-09-13
    body: "That's not a bug, that's doing it the hard way.\n\nPlease read this: \nhttp://www.pycs.net/lateral/stories/31.html\n\nThen use this repository:\nhttp://kde-redhat.sf.net\n\nAnd follow the instructions there.\n\nIt will be 100 times easier."
    author: "Roberto Alsina"
  - subject: "Re: KDE 3.3 on Fedora"
    date: 2004-10-13
    body: "I tried that... got the following\n\n[root@Rutger root]# dir\n20040204_dreamer.wma  Desktop    install.log\nanaconda-ks.cfg       downloads  install.log.syslog\n[root@Rutger root]# cd downloads\n[root@Rutger downloads]# cd apt\n[root@Rutger apt]# dir\napt-0.5.15cnc6-1.1.fc2.fr.i386.rpm\n[root@Rutger apt]# rpm -uvh apt-0.5.15cnc6-1.1.fc2.fr.i386.rpm\n-uvh: unknown option\n[root@Rutger apt]# rpm -Uvh apt-0.5.15cnc6-1.1.fc2.fr.i386.rpm\nwarning: apt-0.5.15cnc6-1.1.fc2.fr.i386.rpm: V3 DSA signature: NOKEY, key ID e42d547b\nPreparing...                ########################################### [100%]\n   1:apt                    ########################################### [100%]\n[root@Rutger apt]# apt-get update\nGet:1 http://ayo.freshrpms.net fedora/linux/2/i386 release [1819B]\nFetched 1819B in 3s (575B/s)\nGet:1 http://ayo.freshrpms.net fedora/linux/2/i386/core pkglist [1729kB]\nGet:2 http://ayo.freshrpms.net fedora/linux/2/i386/core release [151B]\nGet:3 http://ayo.freshrpms.net fedora/linux/2/i386/updates pkglist [1048kB]\nGet:4 http://ayo.freshrpms.net fedora/linux/2/i386/updates release [157B]\nGet:5 http://ayo.freshrpms.net fedora/linux/2/i386/freshrpms pkglist [230kB]\nGet:6 http://ayo.freshrpms.net fedora/linux/2/i386/freshrpms release [161B]\nFetched 3007kB in 33s (90,5kB/s)\nReading Package Lists... Done\nBuilding Dependency Tree... Done\n[root@Rutger apt]# apt-get kde3.3\nE: Invalid operation kde3.3\n[root@Rutger apt]# apt-get kde\nE: Invalid operation kde\n[root@Rutger apt]# apt-get install kde\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nE: Couldn't find package kde\n[root@Rutger apt]# apt-get update\nGet:1 http://ayo.freshrpms.net fedora/linux/2/i386 release [1819B]\nFetched 1819B in 3s (586B/s)\nHit http://ayo.freshrpms.net fedora/linux/2/i386/core pkglist\nHit http://ayo.freshrpms.net fedora/linux/2/i386/core release\nHit http://ayo.freshrpms.net fedora/linux/2/i386/updates pkglist\nHit http://ayo.freshrpms.net fedora/linux/2/i386/updates release\nHit http://ayo.freshrpms.net fedora/linux/2/i386/freshrpms pkglist\nHit http://ayo.freshrpms.net fedora/linux/2/i386/freshrpms release\nReading Package Lists... Done\nBuilding Dependency Tree... Done\n[root@Rutger apt]# apt-get install kde\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nE: Couldn't find package kde\n[root@Rutger apt]#  apt-get install kde themer qt arts kdelibs kdebase gtk+ gtk2 redhat-artwork\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nE: Couldn't find package kde\n[root@Rutger apt]# apt-get install kde3.3\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nE: Couldn't find package kde3.3\n[root@Rutger apt]# #ftp://apt.us.kde-redhat.org/linux/kde-redhat/apt/\n[root@Rutger apt]# ftp://apt.us.kde-redhat.org/linux/kde-redhat/apt/\nbash: ftp://apt.us.kde-redhat.org/linux/kde-redhat/apt/: Onbekend bestand of map\n[root@Rutger apt]# apt-get install kde\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nE: Couldn't find package kde\n\n(You can see that I tried alot..)\n\nIt doesn't work, i have Fedora core 2 and updated it with up2date one hour ago... \n\nPlease can you tell me what i'm doing wrong!\n\nThanking you advance! \n"
    author: "Binky"
  - subject: "Re: KDE 3.3 on Fedora"
    date: 2004-10-13
    body: "Ok, why are you typing stuff like \n\nftp://apt.us.kde-redhat.org/linux/kde-redhat/apt/\n\nin your shell prompt?\n\nIt's clear you don't have the kde-redhat sources in your apt configuration, or it would have shown when you did the update.\n\nJust put something like this in your /etc/apt/sources.list.d/kde-redhat.list\n\nrpm ftp://apt.kde-redhat.org/apt fedora/2  unstable\nrpm ftp://apt.kde-redhat.org/apt fedora/all    unstable\nrpm ftp://apt.kde-redhat.org/apt kde-redhat/2  unstable\nrpm ftp://apt.kde-redhat.org/apt kde-redhat/all    unstable\n\nrpm ftp://apt.kde-redhat.org/apt fedora/2  stable\nrpm ftp://apt.kde-redhat.org/apt fedora/all    stable\nrpm ftp://apt.kde-redhat.org/apt kde-redhat/2  stable\nrpm ftp://apt.kde-redhat.org/apt kde-redhat/all    stable\n\nrpm ftp://apt.kde-redhat.org/apt fedora/2  testing\nrpm ftp://apt.kde-redhat.org/apt fedora/all    testing\nrpm ftp://apt.kde-redhat.org/apt kde-redhat/2  testing\nrpm ftp://apt.kde-redhat.org/apt kde-redhat/all    testing\n\n\nThen do \n\napt-get update\n\napt-get install kde\n"
    author: "Roberto Alsina"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-02-04
    body: "So what was your exact steps in installing KDE 3.1 on Redhat 8.0???\nI keep getting dependency errors. I tried Redhat-artwork 1st, it says I need qt-3.1\nTheodore"
    author: "Theodore"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-02-07
    body: "i had to compile qt 3.1 from source to get things to even start, so far lots of toil and no workin 3.1 yet for me.. i would love to see a step by step that the first guy (whom im willing to call SIR!) on how he got it working..."
    author: "jonathan"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-02-04
    body: "well a better place from where to download the files is :\nftp://mirror.ac.uk/sites/download.sourceforge.net/pub/sourceforge/kde-redhat/\n\nIt is faster and clean has it should be.\n\nby the way I did not Understand why those binaries are not where they should be: THE KDE FTP SITE!!!\n\nsee ya"
    author: "Luca"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-02-04
    body: "Ask the creator of these packages."
    author: "Anonymous"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-03-22
    body: "Simple: 'Couse its creators are not from the KDE team. \n\n[]'s\nMauro"
    author: "Mauro Faccenda"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-03-22
    body: "None of the packages are provided by the KDE team.\n\n"
    author: "Sad Eagle"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-29
    body: "Be careful there :=)\n\nYou need rawhide gcc, kernel, glibc\nI believe the new glibc will bring you the NPTL threading stuff and more.\n\nAll highly unstable :=)\n\nP"
    author: "perraw"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-30
    body: "Konstruct is your friend, see previous thread about Redhat 8.0"
    author: "khalid"
  - subject: "Re: KDE 3.1 on Redhat 8.0..."
    date: 2003-01-30
    body: "I highly respect Redhat work. They do a lot of important development. And if only that they employ Alan Cox, pushed Gnome (we NEED Gnome, because we need something that KDE has to try and be better or learn from even when Windows is long dead) and many things more.\n\nBut promotion is largely irrelevant. What mattered to Linux is the code. So I have the highest respect for those that improve the code,  that make it feasible to promote anything at all about Linux.\n\nThe problem that Redhat likely has with KDE is that it's anti-commercial largely. So much stuff is GPL and not LGPL as with Gnome. But I don't care a tad thing about it, you can do commercial KDE apps, but not commercial KDE system variants that are not GPL, so what?\n\nI believe Redhat is a good thing to have. But I believe society really needs something that IBM, HP, Sun, and all the big engineering firms can participate in and collectively produce the best thing.\n\nSo forgive me, hopefully, the need for Redhat will be a thing of the past in the future ;-)\nBut lets enjoy it why it's so. :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Just gorgeous!!!"
    date: 2003-01-29
    body: "Thanks developers, my Gentoo box finished compiling last night.  I LOVE IT!!! This is wonderful!  Nobody needs a proprietary OS anymore!  Not WinXP, not MacOSX, not anything, except... KDE!!!\n\nThe only thing is, the new Keramik theme shows just how cheap my default Dell monitor is.  I gotta getta a new monitor now!  Maybe one of those super-sharp, super-bright Sony LCDs I saw in CompUSA a few weeks ago..."
    author: "Robert"
  - subject: "Re: Just gorgeous!!!"
    date: 2003-01-30
    body: "oppps, don't forget that Apple are from the good guys, and i beleive that everyone can found the aguments for that..."
    author: "Anton Velev"
  - subject: "Somewhat disappointed..."
    date: 2003-01-30
    body: "I recently upgraded my SuSE 8.1 to KDE 3.1 but\nI am somewhat disappointed. As a web programmer\nI need an editor to edit HTML / PHP web pages.\nQuanta as well as Kate are completely unusable for\nme just because of one simple but IMO major bug in both applications\nwhich exists since KDE 3.0.\nIt is not possible to achieve the following \"function\" known from HomeSite:\n1. Show the editor and a list / tabs of all currently open files _only_\n2. On F9 (or any other simple key combo) the file selector is\n    shown. You can click on as many files as you want and they all\n    open in the background without closing the file selector.\n3. When you press F9 again the file selector closes.\n\nThis is IMO absolutely necessary if you don't want to waste half of your\nscreen when you leave the file selector open all of the time.\nBut this is not possible because of many bugs which all \"work together\"\nto prevent me from doing this in Kate and Quanta.\n\nQuanta:\nI have to close all other tree views until I have only the file tree left.\nNow I associate F9 with \"show/hide Files Tree\". After pressing\nF9 some time and some closing and restarting of Quanta the program\nhas gone completely nuts and the editor window detaches from the main\nwindow rendering the application completely useless (only\ndeletion of quantarc helps here and you lose all your settings)\nWhy isn't there a menu item which hide/shows all except the editing area?\n\nKate has got the following bugs:\n1. You can't make the editing area smaller than a third of the screen height.\n2. Once the file selector has had the focus no shortcut keys are\n    recognized until you click with mouse back in the editing area.\n3. A detached, free floating file selector is reintegrated into the\n    main window after trying to hide and show it again.\n\nThis happens on all distros and on all PCs in our company.\nWell, I like and keep KDE for all other tasks, but at work we'll\nstick to Windows until this is solved.\n"
    author: "Jan"
  - subject: "Re: Somewhat disappointed..."
    date: 2003-01-30
    body: "Those are probably some fairly useful features/bugs, however you really should go to bugs.kde.org to post them instead of here where they will probably be left ignored or probably unread.\n"
    author: "Super PET Troll"
  - subject: "Re: Somewhat disappointed..."
    date: 2003-01-30
    body: "Hello,\n\nthese are certainly useful suggestions. But have you come to realize that Quanta Plus is the first time included in KDE 3.1? It's only now getting more attention, from what I read before here, it's mostly one person driver effort. If more people drive it, it will get better. The release in 3.1 will certainly help that.\n\nStrangely F9 works as you say in Konqueror sidebar. What you are asking for it more like asking Konqueror, Quanta and Kate to share this code for the sidebars of typical apps. Probably, I don't know admittedly, it's just not yet there, that this really good and advanced sidebar code from Konqueror is ready for reuse in other KDE apps. Or it just hasn't happened yet.\n\nChanging software implies other usage styles though. For now you could consider to adapt or wait for future releases. You likely would be an early adopter now.\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: Somewhat disappointed..."
    date: 2003-01-31
    body: ">> the editor window detaches from the main window rendering the application completely useless (only deletion of quantarc helps here and you lose all your settings)\n\nWhilst I agree that the interface of quanta still needs some work, it is not necessary to delete your quantarc to fix this.  There is a little arrow (and I mean *little*, it can easily be over looked) in the top right corner of the panel, click this and it will re-attach itself (still they could do with a menu item for this).\n\n>>I have to close all other tree views until I have only the file tree left.\nNow I associate F9 with \"show/hide Files Tree\".\n\nI agree there should be some other way of doing this.  Closing all the other tree views just so that you can have a keyboard shortcut it much to high a price for me (the DOM tree view is worth it's weight in gold if you ask me).\n\nAs the other post have said, this isn't the best place to complain about this (not if you want something done about it anyway).  I recommned that you visit http://quanta.sourceforge.net/ and take a look at the todo list, then possibly post a request."
    author: "nonamenobody"
  - subject: "Re: Somewhat disappointed..."
    date: 2003-01-31
    body: "Yes, there is a small arrow which can be easily overlooked.\nNo, *nothing* happens when I click on it.\nI know it should, as it works in Kate.\nWell, this seems to be the next bug here.\nI've already sent a message to the developers when Quanta 3.0\nwas released about all of these problems. They basically answered then\nthat they would figure it out someday. After that, a lot \nof *new* features have been integrated in Quanta.\nBut I wonder why they didn't fix such a major bug.\nIn addition a *minimal test-case* document containing\nPHP code, HTML and JavaScript has a totally broken \nsyntax-colouring where each letter has a different colour.\nAll these bugs make Quanta completely unusable. So I wonder\nwhy they implemented all these fancy-features if those\nbasic features just don't work. I could file bugs on all of\nthose problems, but anybody can see these bugs at first glance.\nJust try it out: Open a HTML page which contains some simple PHP\nblocks and some JavaScript blocks. Syntax colouring is completely\nbroken. I really don't want to be unfair. I know there is \na lot of work behind such a project. But sometimes a bit\ngeneral crtiticism (not just filing bugs) is good because it\nshifts the focus back from feature-mania to the main problems.\nWell, just in case, I reposted my original post in addition on\nthe Quanta bug page.\n\n\n\n"
    author: "Jan"
  - subject: "It's in the press"
    date: 2003-01-30
    body: "http://www.idg.se/ArticlePages/200301/29/20030129095351_TST336/20030129095351_TST336.dbp.asp"
    author: "KDE User"
  - subject: "widget flicker"
    date: 2003-01-30
    body: "Does anyone know if there's work being done to reduce widget flicker?\n"
    author: "Praveen"
  - subject: "Re: widget flicker"
    date: 2003-01-30
    body: "Today a change to Qt has been applied which fixed the Drag&Drop-flicker.\n\nhttp://lists.kde.org/?l=kde-cvs&m=104384856526094&w=2"
    author: "Carsten Niehaus"
  - subject: "New konq"
    date: 2003-01-30
    body: "Konqueror really goes from strength to strength. I thought it was the most convenient browser _before_ it had tabs and \"right click goes back\"!\n\nWith the help from Apple, I can't see how Konq can be stopped... "
    author: "ac"
  - subject: "Re: New konq"
    date: 2003-02-02
    body: "Yes it is nice, exept that you can't use it to post articles on slashdot, zdnet and many other places or use it for controlling the web based interface of a D-Link 704.\n\nWhenever you post something on those places it just sits there and waits for the answer forever."
    author: "Uno Engborg"
  - subject: "antialiasing"
    date: 2003-01-30
    body: "Could somebody explain to an idiot like me why antialiasing is not available after I build everything on redhat 8 using konstruct? Is that default in KDE to have AA off or in QT or what is this? If so where can I find clear instructions for idiots how to compile it with AA using konstruct, or even better an example of gar.conf.mk file? \nThanks,\nSerge"
    author: "Serge"
  - subject: "Re: antialiasing"
    date: 2003-01-30
    body: "konstruct looks for libXft.so for aa rendering, but rh8 has libXft2.so. So konstrukt builds qt with AA disabled. Try compiling and installing Xft in a safe location then add its path to the main konstruct Makefile (maybe in configure extra options...). Take a look at qt config output to be sure it worked. Sorry for not being very accurate, I got this problem some RCs ago :)\nciao"
    author: "giaso"
  - subject: "antialiasing in icons (SuSE 7.3)"
    date: 2003-01-30
    body: "Why is there no antialiasing for icons in SuSE 7.3?\nCrystal Icons and Noia look terrible here. Anything preventing SuSE from using that in 7.3? (8.1 looks great).\n"
    author: "Idiot2"
  - subject: "Re: antialiasing in icons (SuSE 7.3)"
    date: 2003-01-30
    body: "AFAIK SuSE 7.3 (as all versions before 8.0), has a version of X (or QT, I don't remember) which does not support antialiasing at all."
    author: "Vajsravana"
  - subject: "Re: antialiasing in icons (SuSE 7.3)"
    date: 2003-01-30
    body: "Well Qt is the one that comes with KDE 3.1 (Qt 3.1.1 I think). \nThe Xserver I have is version 4.1.0... oh well I'll wait until SuSE 8.2 is released.\n"
    author: "Me"
  - subject: "Re: antialiasing"
    date: 2003-01-30
    body: "> konstruct looks for libXft.so for aa rendering\n\nKonstruct doesn't, Qt's \"configure\" does."
    author: "Anonymous"
  - subject: "Problem with Konqueror in KDE 3.1 with Red Hat 7.3"
    date: 2003-01-30
    body: "Hi!\n\nI've compiled KDE 3.1 using Konstruct on my Red hat 7.3 box and it all works fine except for a rather weird problem with Konqueror! Take a look at tis image : http://www.navero.co.uk/misc/erro-konqueror.jpg"
    author: "Jaime Fordham"
  - subject: "Konstructing failed on Mandrake9.0"
    date: 2003-01-30
    body: "I cannot build KDE3.1 using konstruct on my Mandrake 9.0 box. Building stops while making /root/konstruct/kde/kdelibs/work/kdelibs-3.1/kabc/plugins/ldap\nI'm trying to install KDE to /opt/kde The following messages are shown and buiding process stops unexpectedly:\n\n\"libtool: link: warning: library `/usr/lib/libsasl.la' was moved.\nlibtool: link: warning: library `/usr/lib/libsasl.la' was moved.\nlibtool: link: warning: library `/usr/lib/libsasl.la' was moved.\n/bin/sh ../../../libtool --silent --mode=link --tag=CXX g++  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -DNDEBUG -DNO_DEBUG -O2 -I/opt/kde/include -I/usr/lib/qt3/include -I/usr/X11R6/include -L/opt/kde/lib -L/usr/lib/qt3/lib -L/usr/X11R6/lib -I/opt/kde/include -I/usr/lib/qt3/include -I/usr/X11R6/include -L/opt/kde/lib -L/usr/lib/qt3/lib -L/usr/X11R6/lib -I/opt/kde/include -I/usr/lib/qt3/include -I/usr/X11R6/include -L/opt/kde/lib -L/usr/lib/qt3/lib -L/usr/X11R6/lib -O2 -pipe -O2 -pipe -O2 -pipe -fno-exceptions -fno-check-new  -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_COMPAT   -L/opt/kde/lib -L/usr/X11R6/lib -L/usr/lib/qt3/lib -L/opt/kde/lib -L/usr/X11R6/lib -L/usr/lib/qt3/lib -L/opt/kde/lib -L/usr/X11R6/lib -L/usr/lib/qt3/lib  -o kabc_ldap.la -rpath /opt/kde/lib/kde3 -L/usr/X11R6/lib -L/opt/kde/lib  -module -avoid-version -module -no-undefined -R /opt/kde/lib -R /usr/X11R6/lib  -lldap -llber -lresolv kabc_ldap_la.all_cpp.lo  ../../libkabc.la ../../../kdeui/libkdeui.la\nlibtool: link: warning: library `/usr/lib/libsasl.la' was moved.\nlibtool: link: `/usr/lib/libsasl.la' is not a valid libtool archive\nmake[7]: *** [kabc_ldap.la] Error 1\nmake[7]: Leaving directory `/root/konstruct/kde/kdelibs/work/kdelibs-3.1/kabc/plugins/ldap'\nmake[6]: *** [all-recursive] Error 1\nmake[6]: Leaving directory `/root/konstruct/kde/kdelibs/work/kdelibs-3.1/kabc/plugins'\nmake[5]: *** [all-recursive] Error 1\nmake[5]: Leaving directory `/root/konstruct/kde/kdelibs/work/kdelibs-3.1/kabc'\nmake[4]: *** [all-recursive] Error 1\nmake[4]: Leaving directory `/root/konstruct/kde/kdelibs/work/kdelibs-3.1'\nmake[3]: *** [all] Error 2\nmake[3]: Leaving directory `/root/konstruct/kde/kdelibs/work/kdelibs-3.1'\nmake[2]: *** [build-work/kdelibs-3.1/Makefile] Error 2\nmake[2]: Leaving directory `/root/konstruct/kde/kdelibs'\nmake[1]: *** [dep-../../kde/kdelibs] Error 2\nmake[1]: Leaving directory `/root/konstruct/kde/kdebase'\nmake: *** [dep-../../kde/kdebase] Error 2\"\n\"\n[root@manux kde]# ll /usr/lib/libsasl.la\n-rwxr-xr-x    1 root     root          745 sty 17 15:41 /usr/lib/libsasl.la*\n\nI have encountered the same trroubles while building RC6. Anyone knows what might be wrong?\n\nregards,\nMarcin\n"
    author: "mar_10"
  - subject: "Re: Konstructing failed on Mandrake9.0"
    date: 2003-01-30
    body: "You most likely need to update libsasl. Try getting the version that is in Texstar's RPM repository (http://www.pclinuxonline.com)."
    author: "Luca Beltrame"
  - subject: "Re: Konstructing failed on Mandrake9.0"
    date: 2003-01-30
    body: "Yes I know what's wrong. I had the same problem. \n\nuse a text editor (as root) and edit the file /usr/lib/libsasl.la\n\nchange the last line from\n  libdir=''\nto\n  libdir='/usr/lib'\n\nthere seems to be a problem with the libsasl included in Mandrake 9.0 . Happy compiling!"
    author: "Braden MacDonald"
  - subject: "Re: Konstructing failed on Mandrake9.0"
    date: 2003-01-30
    body: "Thanks a lot, building process is still going on but critical part is already done.\nSo the next question is:\nHow to change to new KDE after the compilation is finished?\nI have got 3.1 RC5 installed from RPMS to /usr, new version will be installed into /opt/kde. Is there any easy way to switch to new version without loosing current configuration, menues and other things?\n\nregards\nMarcin"
    author: "mar_10"
  - subject: "Re: Konstructing failed on Mandrake9.0"
    date: 2003-01-31
    body: "Let's see; I hope I can remember how I did it:\n\nCreate a file in /etc/X11/wmsession.d/ called \"20KDE3.1\"\n and put the following in it:\n ____\n NAME=KDE 3.1\n ICON=kde-wmsession.xpm\n DESC=The K Desktop Environment\n EXEC=/opt/kde/bin/startkde\n SCRIPT:\n exec /opt/kde/bin/startkde\n ____ \n (don't include the lines (____).)\n \n And then, create a file in /usr/bin/ called \"startkde3.1\" (use this exact name).\n next, chmod +x /usr/bin/startkde3.1, so it's executable, then edit it and put in the following:\n ____\n #!/bin/sh\n export PATH=/opt/kde/bin/:$PATH\n export PATH=/opt/qt/bin:$PATH\n export KDEDIR=/opt/kde\n export QTDIR=/opt/qt\n /home/(your user name)/bin/startkde\n ____\n (make sure qt paths are right.)\nthen do the following command as root:\nservice dm restart"
    author: "Braden MacDonald"
  - subject: "Problems using Konstruct with Mandrake 9.0"
    date: 2003-01-30
    body: "I'm trying a build in Mandrake 9.0 using Konstruct.  I'm performing a \"make install\" under the meta/everything directory and everything goes fine until kdebase-3.1 where I run into the following error:\n\nchecking for Java... configure: error: jni.h not found under /usr/include/. Use --with-java or --without-java\n\nNow I have Sun's sdk 1.4 installed but it is installed under /usr/local/ and it doesn't put the header files under /usr/include.  I tried copying the header files into /usr/include but that didn't work and I also tried executing the configure command myself with the --with-java=\"/usr/local/j2sdk1.4.1\" but then a bunch of other configure options fail (Qt was the first one).\n\nIs there anyway to pass the --with-java parameter to the \"make install\" command?  The Konstruct web site is pretty thin.  Does anyone else know of a way to get around this?\n\nThanks for the help!"
    author: "Tickleboy"
  - subject: "Re: Problems using Konstruct with Mandrake 9.0"
    date: 2003-01-30
    body: "> Is there anyway to pass the --with-java parameter to the \"make install\" command?\n\nJust change kde/kdebase/Makefile to your liking. Then run \"make buildclean install\"."
    author: "Anonymous"
  - subject: "Re: Problems using Konstruct with Mandrake 9.0"
    date: 2003-04-19
    body: "I too have had the same problem using Konstruct to install KDE3.1.1 on Mandrake 9.1. (I had no problems at all with 3.1.0!). Editing the makefile didn't make one bit of difference. In fact, even passing the without-java argument brought the same error. I don't think I have a syntax error (see attached makefile). Is there a work-around? "
    author: "Cougar"
  - subject: "compiling qtsharp fails"
    date: 2003-01-30
    body: "What do I need to compile qtsharp from kdebase?\n\nI get:\n\nmake[4]: Entering directory `/home/bjorn/download.uk.kde.org/pub/kde/stable/3.1/src/kdebindings-3.1/qtsharp/src/parser'\ncsant -Dcscc=/usr/bin/cscc -C cscc\nBuilding project `Qt# C++ Header Parser'\nBuilding target `all' for project `Qt# C++ Header Parser'\n/usr/bin/cscc -o ./parser.exe -O2 ./parser.cs ./main.cs -L. -llexan\n./parser.cs:17: invalid type specification\n./parser.cs:37: invalid type specification\n....\n\nand a range of errors. This is with Portable.NET 0.4.8\n\nWhat do you guys use? Or can I select not to support csharp? I can't find any options for it in the configure script.\n   _\n/Bjorn.\n"
    author: "user"
  - subject: "Re: compiling qtsharp fails"
    date: 2003-01-30
    body: "You need to update your Portable.NET version.  The latest version is here:\nhttp://dotgnu.org/downloads.html\n\nI would suggest you try the latest cvs version of pnet or you'll have to install mono too.\n\nCheers,\nAdam"
    author: "Adam Treat"
  - subject: "Re: compiling qtsharp fails"
    date: 2003-01-30
    body: "BTW, if you uninstall Portable.NET and mono, the configure script will disable qtsharp from the build."
    author: "Adam Treat"
  - subject: "Thanks"
    date: 2003-01-30
    body: "For the best desktop in the Linuxworld....\n\n\nSo long, and Thanks for all the Fish.\n\n/LGP"
    author: "Lennart G Peterson"
  - subject: "kdm?"
    date: 2003-01-30
    body: "Thanks to konstruct, I have a compiled version of kde3.1 in my ~/kde3.1.  Now, how can I add the new session to kdm? (i am running mdk 8.2). Any help is appreciated.\n\n-kl"
    author: "kola"
  - subject: "Re: kdm?"
    date: 2003-01-30
    body: "Try this:\nCreate a file in /etc/X11/wmsession.d/ called 20KDE3.1\nand put the following in it:\n____\nNAME=KDE 3.1\nICON=kde-wmsession.xpm\nDESC=The K Desktop Environment\nEXEC=/home/(your user name)/bin/startkde\nSCRIPT:\nexec /home/(your user name)/bin/startkde\n____ \n(don't put the lines in it.)\n\nAnd then, create a file in /usr/bin/ called \"startkde3.1\" (must exactly match).\nnext, chmod +x /usr/bin/startkde3.1, so it's executable, then edit it and put in the following:\n____\n#!/bin/sh\nexport PATH=/home/(your user name)/bin/:$PATH\nexport PATH=(path to Qt)/bin:$PATH\nexport KDEDIR=/home/(your user name)/\nexport QTDIR=(path to Qt)\n/home/(your user name)/bin/startkde\n____\n\nThat *should* do it."
    author: "Braden MacDonald"
  - subject: "Thanks!! "
    date: 2003-01-31
    body: " I'll try that\n\n-kl"
    author: "kola"
  - subject: "Re: Thanks!! "
    date: 2003-01-31
    body: "I forgot: to have kdm apply these changes, do the following command as root:\n/usr/sbin/fndSession\n, and then in kdm, choose \"Menu : Restart X Server\"."
    author: "Braden MacDonald"
  - subject: "Working! (without font antialiasing)"
    date: 2003-01-31
    body: "Which works fine with kde3.0.x though :("
    author: "kola"
  - subject: "Konstruct failed on my MDK 8.2 box"
    date: 2003-01-30
    body: "Hi!\n\nThere is a problem with building 3.1 on my machine. After building and installing kdelibs correctly, the ./configure of kdebase gives me the following error message:\n\nchecking if UIC has KDE plugins available... configure: error: not found - you need to install kdelibs first.\n\nThe executed command (taken from config.log) that fails is:\nconfigure:22577: /usr/lib/qt3.1/bin/uic -L /opt/kde3.1/lib/kde3/plugins/designer -nounload -impl actest.h actest.ui > actest.cpp\nconfigure:22580: $? = 0\nconfigure:22595: error: not found - you need to install kdelibs first.\n\nThere is nothing like a actest.ui in the kdebase base directory (should it?). kdelibs-3.1 is successfully installed.\n\nI'm using qt from KDE CVS (version 3.1.1) that I already have compiled some weeks ago (and konstruct with the HAVE_QT_3_1_INSTALLED option).\n\nThank you for any help\n\nDaniel Gruen"
    author: "Daniel Gruen"
  - subject: "Re: Konstruct failed on my MDK 8.2 box"
    date: 2003-02-01
    body: "Probably your gcc/g++ has been configured with\n--enable-__cxa_atexit. Check with gcc -v. Same problem here, I'm\npretty sure that this is the reason. If only my machine would compile\nfaster.\n"
    author: "Ulrich Kunitz"
  - subject: "Re: Konstruct failed on my MDK 8.2 box"
    date: 2003-02-02
    body: "Jep, you're right. Is there a solution or should I wait for mdk rpms?\n\nThank you\n\nDaniel"
    author: "Daniel Gruen"
  - subject: "3.1 compiled successfully for RH8.0"
    date: 2003-01-30
    body: "Just a quick FYI that I successfully used konstruct to download and install 3.1 with no problems at all on my RH 8.0 system. It took a while on a PIII/600 but its up and running."
    author: "Mike Campbell"
  - subject: "Re: 3.1 compiled successfully for RH8.0"
    date: 2003-02-06
    body: "How did you get the Redhat Menus to work in the compiled KDE 3.1. I have used\nKAppfinder but I would rather restore my old menus because a lot of items are \nstill missing\n\nThanks in advance"
    author: "tagbo okoli"
  - subject: "Flash not working"
    date: 2003-01-30
    body: "I am running kde3.1 successfully on a mandrake 9.0 box. The only glitch is that konqueror is not showing flash images. I have already installed macromedia flashplugin and it also showing in the list of available plugins, but whenever i go to a flash website, a message always popsup asking me to download the plugin. \nwhats wrong?? "
    author: "Rajil Saraswat"
  - subject: "Re: Flash not working"
    date: 2003-01-31
    body: "There are two versions of Flash now - one supported natively under Linux, and one not.\n\nThe one which is NOT supported is Flash 6 IIRC(as produced using Flash MX)\n\nTry this site:\n\nwww.theola.dyndns.org - my g/f's page - sorry, but it's the only one I KNOW is in Flash5 and works ok with the flash plugin :)\t\n\nDavid"
    author: "David Pye"
  - subject: "Re: Flash not working"
    date: 2003-01-31
    body: "The Macromedia website lists only flash 6 plugin. I couldnt find anyplace to download flash5, can you give some pointer?\n"
    author: "Rajil Saraswat"
  - subject: "Re: Flash not working"
    date: 2004-08-22
    body: "abe flash light ka bulb fuse ho gaya hoga. Replace it."
    author: "Ruchir Saraswat"
  - subject: "Re: Flash not working"
    date: 2004-10-16
    body: "when i try to use american greeting cards and launch all i see are little white boxes with red x's in them. it wont let me view the cards and videos and when i go to macromedia flash download it says its already installed but i still cant view them what do i do?"
    author: "alyssa"
  - subject: "rh8.0 + konstruct compilation problems"
    date: 2003-01-30
    body: "Hi, I'm trying to compile kde3.1 (using konstruct and cd meta/everything; make install) on red hat 8.0\n\nI started out with the prefix in gar.conf.mk set to /usr (as, I think, Red Hat likes) and got this error when I did make install:\n\nnanohttp.c: In function `xmlNanoHTTPConnectAttempt':\nnanohttp.c:815: `len' undeclared (first use in this function)\nnanohttp.c:815: (Each undeclared identifier is reported only once\nnanohttp.c:815: for each function it appears in.)\nmake[8]: *** [nanohttp.lo] Error 1\n\nI tried resetting the prefix to ~/kde3 and I also tried uncommenting the \nCC = gcc-3.2 line in gar.conf.mk (RH8.0 uses gcc3.2).  Neither change helped.\n\nAnyone got any suggestions?  Thanks!\n\nzo.\n"
    author: "johnzo"
  - subject: "Re: rh8.0 + konstruct compilation problems"
    date: 2003-02-04
    body: "Hi,\n\nI have the same problem. I patched and installed Qt 3.1 as described above successfully. \nWhen I make in meta/kde oder meta/everything I get the same compilation error.\n\nDo you have found a solution?\n\nThanks \nTobias"
    author: "Tobias H\u00fcbner"
  - subject: "Re: rh8.0 + konstruct compilation problems"
    date: 2003-02-05
    body: "I have a workaround the problem: I removed the targets libxml2 and libxslt from lib/Makefile. \nEverything compiled fine without kdegraphics... so I did \"make install -k\". KDE 3.1 works fine,\nantialiased fonts and icons. Only the kdegraphics is missing now.\n\nTobias"
    author: "Tobias H\u00fcbner"
  - subject: "Re: rh8.0 + konstruct compilation problems"
    date: 2003-03-18
    body: "Hello!\n\nI've solved this problem in this way:\nAfter compilation fail (nanohttp.c:815: `len' undeclared (first use in this function) I've modified two files in ~/konstruct/libs/libxml2/work/libxml2-2.4.30 directory: nanohttp.c and nanoftp.c. I've changed 'SOCKLEN_T' phrase occuring before the variable name with 'unsigned int'. 'SOCKLEN_T' appears only once in each file except define section in their beginnings.\nAfter such a modification the compilation process has gone further.\nBut the compilation hasn't finished yet... ;-).\n\ngreetings\nJarek\n"
    author: "Jarek"
  - subject: "Re: rh8.0 + konstruct compilation problems"
    date: 2003-03-20
    body: "And I can confirm that Jarek's solution is working perfectly.\n"
    author: "MK"
  - subject: "Re: rh8.0 + konstruct compilation problems"
    date: 2003-09-07
    body: "Well, that solved it for me. lets see what wll happen now :)\n\nAnd lets hope it\u00b4ll work :)))"
    author: "Henning"
  - subject: "missing icons!"
    date: 2003-01-31
    body: "Hi!\nLeft Mandrake 9 out in the cold (after trying compiling kde3.1).. :( for the benefit of SuSe 8.1. Installed the new kde rpms.. it's working.. as far as I can see..   But I saw when compiling it said \"can't remove folder xxxx, it's not empty\" or something like that.. think it was icon folders and konqueror folder. Is it some way to fix this? Am I missing kde3.1 icons?\n\nthanks for any help!\n/moerten"
    author: "moerten"
  - subject: "Re: missing icons!.. again"
    date: 2003-01-31
    body: "it seems like it's the Yast and suse icons thats been missing.. should be able to pick them from the system though...... don't U think?"
    author: "moerten"
  - subject: "Redhat 8.0 compiled via konstruct"
    date: 2003-01-31
    body: "I got KDE 3.1 to install on RH 8.0 by using konstruct but the icons/fonts are jagged and look like crap. (It looks WAY better on my Libranet system)... Does anyone have a step-by-step on how to get KDE 3.1 to install and look good on a RH 8.0 system?\n\n"
    author: "rh8_user"
  - subject: "Re: Redhat 8.0 compiled via konstruct"
    date: 2003-02-01
    body: "Not sure what the deal is with the icons, but as for fonts: I recompiled the freetype libraries to support bytecode hinting, and then installed a bunch of nicely hinted Windows truetype fonts (trebuchet, verdana, georgia, arial, etc) with the kde font installer.  This gives me a look I like, but YMMV.  Font preferences are amazingly subjective, it seems.\n\nIf you'd like, I'll email you my freetype RPMS, which you can install with rpm -U.\n\nzo."
    author: "johnzo"
  - subject: "Re: Redhat 8.0 compiled via konstruct"
    date: 2003-02-01
    body: "You have probably compiled qt without xft support.\n\nThis is what I did:\n\nTake a look here:\nhttp://promo.kde.org/3.1/requirements.php\nand check that you have all devel packages (and other packages) installed.\n\ngo into /konstruct/libs/qt-x11-free\nrun make patch (this will download and patch qt)\nfollow this advice (qt is in the work directory):\nhttp://lists.trolltech.com/qt-interest/2002-12/thread01047-0.html\nrun make configure and check that it says: xft yes\nrun make install\n\nNow you can install the rest of kde.\n\nHope this helps."
    author: "Hallvor Engen"
  - subject: "Re: Redhat 8.0 compiled via konstruct"
    date: 2003-02-03
    body: "Hi\n\nI did all this. I am sure qt is now compiled with Xft support\nbecause when I execute qtconfig it comes with antialised\nfonts. However, KDE has not antialiased fonts! And, be sure,\nI checked antialising in the control center/appearence/fonts.\nI even compiled kdelibs again, just to make sure (and also to\ninclude libbzip2, which I forgot), but it didn't work.\nWhat can I do now?\n\nThanks\nCarlos"
    author: "oliveira"
  - subject: "Re: Redhat 8.0 compiled via konstruct"
    date: 2003-02-09
    body: "Well, it worked like a charm for me by following the above advice. Are you sure you included the appropriate parameters for \"./configure\" in \"qt-X11..\"? \nI patched qt, wrote the changes to the xfreetype.test; then I added \"-I /usr/include/Xft2 -xft -xrender\" to the \"CONFIGURE_ARGS\" line in \"/konstruct/libs/qt-x11-free/Makefile\". Do a quick(?) \"make install\", and all is well - whence I  did the ordinary install of \"/meta/kde\". "
    author: "Mr T"
  - subject: "Re: Redhat 8.0 compiled via konstruct"
    date: 2003-02-06
    body: "Is there anyway to get the redhat menus to show up? I have used KAppfinder\nbut a lot of applications are still missing?\nThanks in advance"
    author: "tagbo okoli"
  - subject: "great"
    date: 2003-01-31
    body: "Just wanted to add my thanks for all the work put in this release."
    author: "IR"
  - subject: "I had no idea where to post this..."
    date: 2003-02-02
    body: "I need some help.. I used cooker to install xfree86 4.2.99.5 on mandrake 9.0 and now fonts wont render in KDE, and everything looks funny. I can get into GNOME and everything works fine. But if I try and run a KDE program, or go into Mandrake config I get I mess if the program even starts up.. How can I go back to the older xfree86 so I can fix this mess? The reason I did this was to install KDE 3.1.. But I'd rather have my system working... I didn't know xfree86 would mess everything up."
    author: "Heather"
  - subject: "Mandrake 9.0 binaries in MandrakeClub"
    date: 2003-02-02
    body: "Hi all\n\nThe KDE 3.1 binaries for 9.0 are already available for Mandrake Club members. I understand that after a \"testing period\" they will be available for the rest of us. Check it out:\n\nhttp://www.mandrakeclub.com/article.php?sid=388&mode=nocomments\nhttp://groups.google.com/groups?hl=en&lr=&ie=UTF-8&th=bffdf8b80d1bf3d&rnum=2\n\nCheers !"
    author: "NewMandrakeUser"
  - subject: "Re: Mandrake 9.0 binaries in MandrakeClub"
    date: 2003-02-03
    body: "So far the free and open source community...\nIf you want packages, and want them when the software is released, you have to pay...\nIs this the future of mandrake? Taking its users hostage.... First get them with a free OS and then force them to pay if they want to stay up to date?"
    author: "Lightning"
  - subject: "Re: Mandrake 9.0 binaries in MandrakeClub"
    date: 2003-02-03
    body: "Of course no one denies you the right to compile KDE from source and make it avaiable to other people..."
    author: "fredi"
  - subject: "Re: Mandrake 9.0 binaries in MandrakeClub"
    date: 2003-02-03
    body: "Well you should instead of complaining read closer.\n\nI posted a link earlier on to my RPMS, they have been available for anybody since KDE 3.1 was released. Only thing I did now , I removed the i586 because they are available on Club and a lot of people must have found the way to my RPMS since I had at least 50 people getting them, if I look in my ftp logfiles.\n\nYou can still get the athlon rpms from my ftp though and not just that you can also get some other fancy stuff. But as I said earlier you have to look around you and not stay in the middle of the street crying.\n\nBy the way , the only reason Mandrake KDE Team did not put them up on the ftp is, because there were too many bugs in the intial KDE 3.1 release, lots of patches have been included now, after some more tests the KDE packages will be put on the public ftps. "
    author: "Linux-phased"
  - subject: "KDE is very very nice"
    date: 2003-02-04
    body: "I have just finished compiling and installing KDE 3.1 and I am very pleased with it.\n\nI did run into a few problems along the way ( due to being to too eager to just get on compiling stuff and not working out what to do first ), firstly with complications involving my old KDE 3.0 and then with horrible looking icons and fonts which I have solved by re-compiling qt with -xft -xrender flags.\n\nOne problem I haven't solved yet ( again a problem entirley of my own making ) is that I managaged to zap the nice kdm login thing and appear to have reverted to xdm somewhere down the line.\n\nCan anyone tell me how to re-configure X so it uses KDM again ?\n\nAs I have said I am really pleased with 3.1, it looks great and I love the download manager and krecord works ( which it never did on my last KDE )."
    author: "CmdrGravy"
  - subject: "Can't install KDE3.1 in SuSE 8.1"
    date: 2003-02-16
    body: "Hey, I have SuSE 8.1 and I just downloaded the rpms and I've done every single thing to get it to work, but I just can't can anyone here explain step by step? what I did was\n1.- rpm -Uvh *\n2.- run SuSEconfig\n\nwhen SuSEconfig was done I got the following msg:\nATTENTION: You have modified /etc/opt/kde3/share/config/kdm//kdmrc.  Leaving it untouched...\nYou can find my version in /etc/opt/kde3/share/config/kdm//kdmrc.SuSEconfig...\n\nThen I rebooted, but nothing really changed, I'm still in KDE 3.0.3\nAny idea of what I can do?\nThank you"
    author: "rem7"
---
The KDE Project today
<a href="http://www.kde.org/announcements/announce-3.1.html">announced</a>
the release of KDE 3.1, &quot;<em>a major feature upgrade to the third
generation of the most advanced and powerful free desktop for Linux
and other UNIXes.</em>&quot;
While you are busy <a href="http://www.kde.org/info/3.1.html#binary">downloading</a>
the new packages for this fabulous release, we hope you will enjoy the
&uuml;ber-cool (<em>disclaimer</em>:
I wrote it) <a href="http://promo.kde.org/3.1/feature_guide.php">KDE
3.1 Feature Guide</a>, as well as a sortable
<a href="http://promo.kde.org/3.1/requirements.php">KDE 3.1 Requirements</a>
page, both new for this release.  And if that's not enough, you can also
check out the detailed
<a href="http://www.kde.org/announcements/changelogs/changelog3_0_5to3_1.html">ChangeLog</a>.  And - ah yes - there are also <a href="http://promo.kde.org/3.1/screenshots.php">screenshots</a>.  So much to do today . . . .

<!--break-->
