---
title: "Linus Names KDE as Open Source Community He Admires"
date:    2003-07-09
authors:
  - "jremijn"
slug:    linus-names-kde-open-source-community-he-admires
comments:
  - subject: "Heh"
    date: 2003-07-09
    body: "\"Duh Linus is an idiot\"\n\n\n(watch the quotes, read the bug reports, don't all jump on me now, please ;)\n"
    author: "plob"
  - subject: "It must be a mutual admiration society"
    date: 2003-07-09
    body: "I can't think of anything that is at once more delightful and obtuse then a first rate coder and project leader like Linux writing a web page in Quanta. (We weren't OSS icons last I checked... Were we?) Since I know he uses KDE I can't help but wonder what he thinks of Quanta. Maybe some day I can ask. ;-)\n--\nEric Laffoon\nQuanta+ Project Lead"
    author: "Eric Laffoon"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-09
    body: "Maybe he has a cat? ;-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "> Maybe he has a cat? ;-)\n\nROTFL LMAO\n\nThis might finally be a reason to check with him. I'm sure he'd like his cat toys to be as high of quality as his kernel. ;-)\n\nMy cats are also into Linux. Have a look."
    author: "Eric Laffoon"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "I hope that you already know that we all admire Quanta Eric!\nDon't know if Linus already used it, but if he did, the changes are very high!\n\nReguards!"
    author: "Iuri Fiedoruk"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "I want to experience the day on which Eric is not evangelizing Quanta."
    author: "Anonymous"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "He actually isn't evangelizing Quanta, he is evangelizing KDE and using Quanta as a tool to do so. ;)\n\nI personally want to experience the day on which even \"Anonymous\" people start supporting KDE. =P"
    author: "Datschge"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "Wouldn't it be possible to replace kdeinit with Quanta somehow? We may be able to redesign the app with it! ;-)"
    author: "uga"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "Thanks for the attempt at humor. Don't quit your day job. If you are going to exercise your wit start out in a private place so that people can't see how anorexic it is. Then once you can tell good jokes with snappy punch lines and crisp comebacks look for a good amatuer venue. Give it a few years and try again. "
    author: "Eric Laffoon"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "Hey, that's not fair, people write anonymously for a number of reasons. Some may even be KDE contributors who are too lazy to enter their name, do not want to see their boss that they are writing during their working hours or just want to say something controversial without having other people shouting at them."
    author: "Anonymous"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "Full agreement. I'm such a KDE distributor (and did more than Datschge before he complains again). If you can't reply to the content of a comment but need a poster's name to judge it/flame him, then you have a problem."
    author: "Anonymous"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-11
    body: "You two are so humourous."
    author: "Anonymous"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-11
    body: "Datschge may not be a contributor since ages, but he helped the project quite a lot (including Quanta). And the reasons that \"too lazy to enter their name, do not want to see their boss that they are writing during their working hours or just want to say something controversial without having other people shouting at them.\" are funny. \n1. You don't have to enter your name every time.\n2. Your boss will see that you write something during the working hours even if you write as anonymous\n3. If you say something, be sure to really accept criticism and even flame. Don't hide behind the anonymous name. \n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-11
    body: ">  2. Your boss will see that you write something during the working hours even if you write as anonymous\n\nOnly if he knows who an \"Anonymous\" poster was. But the examples given by that poster are only a subset of valid reasons and by far not the complete set.\n\n> 3. If you say something, be sure to really accept criticism and even flame.\n\nYou can criticise/flame \"Anonymous\" posters. Some may even tend to flame an \"Anonymous\" poster even worse (which doesn't speak for them). And it will be usually read by the specific \"Anonymous\" poster but e.g. I don't take it so much to my heart if an \"Anonymous\" posting of me is flamed as a flame which contains my name."
    author: "Anonymous"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "> I want to experience the day on which Eric is not evangelizing Quanta.\n\nDo you want me dead... or are you just too dense to not be where I am every single day? Maybe after all this constructive critsism I can drop the project, tell Andras to look for work, buy Dreamweaver, save thousands of dollars and 100 hours a month. Then we could tell millions of people who do web pages to stay with Windows? Will you please supply your name and email address so I can forward all requests for why we ceased development to you?\n\nI want to experience the day when a guy named Anonymous does *anything* that *anybody* cares about, or at least can utter something that sounds more intellient than a Steve Ballmer Press release. BTW I'm sure Ballmer would agree with you."
    author: "Eric Laffoon"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-10
    body: "> Do you want me dead...\n\nNo. But I'm annoyed how often you try to turn an article into an off-topic Quanta discussion like this one.\n\n> I want to experience the day when a guy named Anonymous does *anything* that *anybody* cares about\n\n\"Anonymous\" is no name. There is more than one guy posting under this notation.\n\n> or at least can utter something that sounds more intellient than a Steve Ballmer Press release.\n\nNo reason to insult a poster, anonymous or not."
    author: "Anonymous"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-11
    body: "You started the insults. Eric does a lot of work for KDE and there is nothing wrong with being pleased with your work. What is your problem that you are so incensed by someone else's happy comments?\n\nYou degraded an otherwise pleasant thread. Well done!"
    author: "MxCl"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-11
    body: "> You degraded an otherwise pleasant thread.\n\nI didn't start to post offtopic. I don't know who more desires for admiration: Eric or Mosfet. And don't read this as critic of their work, they do good stuff but neither Quanta nor Mosfet are the center of the KDE world."
    author: "Anonymous"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-11
    body: "Um.. don't think me an idiot but what is Quanta?"
    author: "Forbiddanlight"
  - subject: "Re: It must be a mutual admiration society"
    date: 2003-07-14
    body: "http://quanta.sourceforge.net/"
    author: "ac"
  - subject: "Quanta, uh"
    date: 2003-07-10
    body: "Am I the only one who dislikes quanta? I much more like the straightforwarded apprach of Bluefish. I only seem to have trouble with quanta."
    author: "Jeroen"
  - subject: "Re: Quanta, uh"
    date: 2003-07-10
    body: "> Am I the only one who dislikes quanta?\n\nLet me see... after careful polling I believe you are. ;-)\n\nIn all seriousness, aside from the fact that I really don't understand what you mean by Bluefish's \"straightforwarded apprach\", what don't you like about Quanta and what troubles have you had with it? (Unless you're trolling.) BTW you can get on our user mailing list any time you want help. If you ever wrote me there's a 99% chance I did everything I could to address any problems you may have had.\n\nNow let's deal with a little reality. Bluefish is a decent tool. However it appears to have focused on surface features where Quanta has focused on laying a solid foundation. The newest versions of Bluefish, last I checked. had different feature sets depending on whether you ran Gnome 2 or 1.4 and I've heard of stability issues with the newer version. Major version upgrades can have problems. However Quanta has had Andras Mantia on it full time for the last year. Our bug listing on the KDE top 100 applications for bugs is way down there and we often have less than 5-10 open bugs at any time. I have a CVS copy open often for a week at a time before updating and I can't recall the last time it crashed. We respond right away to bug reports. The big question is... did you file any? We've also spent months fine tuning internal features you're not going to find in other apps and getting what took several seconds to milliseconds, getting CPU time down to 5% during this and chasing down memory leaks. The bottom line is that Quanta is one of the most stable and fast pieces of software anywhere even though it's packing features others wouldn't dream of adding for speed and complexity fears.\n\nTo be very blunt I have to wonder what you're talking about unless you've been running Red Hat 8 or one of the other distro releases that shipped with our 3.0pr1 release which was a one week disaster rushed out during our conversion from KDE 2 and never remotely meant to be shipped in a distro. (Espeically when 3.0 final was out within 30 days of it.) It *is* possible a Linux distribution gave you a bad impression of Quanta by their unprofessional packaging choices. We shouldn't be blamed for that. Check out a 3.1x release before issuing an opinion.\n\nI realize a tool can't be for everyone and that's fine. I'm currently setting up Quanta to support Movable Type blogging and adding Kommander dialogs to the process along with custom toolbars and support for blog tags. Frankly this could be done by any user because it will require exactly zero lines of C++ and little or no scripting. It's almost entirely point and click, drag and drop and XML. Maybe this is not straight forward? It is capable, flexible, reliable and extensible though. Can you do this in Bluefish?\n\nWe are trying to get a CVS snapshot out now to show off the new things we've done. We're working on Javascript support, WYWIWYG, data widgets, advanced site design concept tools and more. Recently Quanta was referred to on the Dot as marvelous. I don't think I'd be bragging if I said I think it is a marvel. If you have a complaint about it or a problem with it and you didn't write me or file a bug then knocking it here would be pretty lame because we care and we always respond. Ask anyone who's written me.\n\nBTW our change log is here. Would you say we are working on it?\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/quanta/?hideattic=1#dirlist"
    author: "Eric Laffoon"
  - subject: "Re: Quanta, uh"
    date: 2003-07-10
    body: "WYWIWYG?\n\nwhat you WILL is what you get?\n\nis this another step in implementing focus-follows-mind?"
    author: "kHIG"
  - subject: "Re: Quanta, uh"
    date: 2003-07-10
    body: "What You Want is What You (might) Get ;-)"
    author: "Thomas"
  - subject: "Re: Quanta, uh"
    date: 2003-07-10
    body: "> WYWIWYG?\n \n> what you WILL is what you get?\n \n> is this another step in implementing focus-follows-mind?\n\nThis was clearly an accident. We did not mean to reveal this feature until it was ready for release and there was no chance of spies for proprietary tools looking into it. This is the ultimate upgrade, but it still has a few problems.  For one, it will require much greater power consumption for the USB based matter integrator, probably a fusion reactor with an input reservoir of nearly one solar mass. So it could get warm. Also the brain scan unit seems to receive inexplicable commands to create a swimsuit model roughly every six seconds too... It could take some work.\n\nYou may find out VPL (Visual Page Layout) interesting though. I know it's easier to type for me than WYTiWYMTT (What You Type Isn't What You Meant to Type).\n\n;-)"
    author: "Eric Laffoon"
  - subject: "Re: Quanta, uh"
    date: 2003-07-11
    body: "Mmm. Flamewar. May i participate? (btw, what exactly is it about?)"
    author: "Eike Dehling"
  - subject: "Re: Quanta, uh"
    date: 2003-07-11
    body: "out of curiosity, who made up this \"focus-follows-mind\" joke? everyone seems to use it and despite my use of many WMs, i just don't find it funny. no offence to the person who made it up but imho, i think it's rather old, tired and lame."
    author: "Anonymous"
  - subject: "Re: Quanta, uh"
    date: 2003-07-10
    body: "WYWIWYG\n\nCool. I want 1 million dollars and world peace. hummmmm. it does not work. :)\nSeriously though, good tool. I recommend it as one of the good ones.\n\n"
    author: "a.c."
  - subject: "Re: Quanta, uh"
    date: 2003-07-10
    body: "Hey, Quanta is great, almost everybody using *nix knows it.  The open source movement probably needs more convinced people like you.  However, this guy has the right to express his opinion... even if didn't justified it totally.\n\nAnyway, we should talk about Linus and KDE.\n\nKeep up good work!!!"
    author: "Ned Flanders"
  - subject: "Re: Quanta, uh"
    date: 2003-07-11
    body: "THANKS FOR QUANTA!!! I LOVE YOU!!!"
    author: "Anonymous"
  - subject: "Re: Quanta, uh"
    date: 2003-07-10
    body: "Hi,\n\n  Can you elaborate this more? Well, only if you care for Quanta....\n\nAndras"
    author: "Andras Mantia"
  - subject: "Community"
    date: 2003-07-10
    body: "That's the right attitude: Everyone helping to improve KDE is (a small) part of the KDE community - even if [s]he only helps with reporting bugs."
    author: "Anonymous"
  - subject: "Re: Community"
    date: 2003-07-10
    body: "KDE is better organized (with regards to x-functionality like internationalization, mailing lists, community) and has an open framework. However KOffice is the weak part due lack of developers. There still is no sufficient paint program, Kpaint has to be improved in usability and functions, The Gimp is no KDE program.\nKeduca is a unique program, but it shows that you can work faster with an editor.\n\nStill to be improved: KDE Documentation (perhaps a wiki like structure would help to improve it), Usability (esp. Control Center, menu structure, standardisation, default theme), speed, get rid of Kedit (bidi for Kate)."
    author: "gerd"
  - subject: "Wiki Documentation"
    date: 2003-07-11
    body: "I like the Wiki based documentation editing idea."
    author: "anonymous"
  - subject: "deputies"
    date: 2003-07-10
    body: "Interestingly, he doesn't mention Alan Cox or Marcelo Tosatti in his list of `deputies'.  Aren't these guys rather important too?\n\nD"
    author: "Daniel"
  - subject: "Re: deputies"
    date: 2003-07-10
    body: "\nMarcello works entirely with the 2.4 series. Linus hasn't managed the stable series (while a development series was running) since the 2.0 days. Alan has mostly been working with Marcello on 2.4 although he has contributed to the 2.5 IDE layer."
    author: "cbcbcb"
  - subject: "Offtopic but important .."
    date: 2003-07-10
    body: "Hi there ..\nI am Nilesh , a student at Indian Institute of Technology. I have been given a chance to write an article about open-source and similar stuff in an online magazine in India. If you have any suggestions or links to any important articles, please post them here. But after reading the following restrictions carefully.\n\n(1) This article is intended towards IT managers and Govt. officials mainly because much has been written and is being written for individual users but that will make the changes happen slowly,and unfortunately we dont have much time, therefore I decided to target those who have the power to change policies. Remember .. it is NOT a free market right now.\n\n(2) Now, to convince businesses to move to open-source, the article has to be based on facts and not on opinions. This is the reason I have decided to title the article as \"Who Owns Your Data ?\" Hopefully, this will sell better to the executives class.\n\n(3)Similarily, to sell to Govt. officials, we'd like to point out the advantages INDIA as a country will get by migrating to open-source. For example, we can note that China has started developing its own microprocessor chips due to concerns about external control.\n\n(4)We are talking about open-source in general here, not specifically to KDE or linux. So, it will be good if we keep the article general in nature.\n\n(5) And sorry for the poor english ...\n\nThank you , all.\n"
    author: "Nilesh"
  - subject: "Re: Offtopic but important .."
    date: 2003-07-11
    body: "Please join us at http://mail.kde.org/mailman/listinfo/kde-promo, that's the place where we talk and you can ask about these kinds of topics. =)"
    author: "Datschge"
  - subject: "Re: Offtopic but important .."
    date: 2003-07-11
    body: "Ooh wow, IIT! You must be really smart! Heard that's one of the most prestigious tech schools in the world..."
    author: "Anonymous"
  - subject: "The real question:"
    date: 2003-07-13
    body: "If Linus likes KDE so much (and he does), does he use kdevelop? ;)"
    author: "Coolvibe"
---
In a <A href="http://news.com.com/2008-1082_3-1023765.html">recent CNET interview</A>, Linus Torvalds gets asked which open-source software development communities he particularly admires. His answer: <i>"If I'd have to pick two, I'd pick KDE and the GCC group. I often end up clashing with the compiler people, because the kernel ends up having rather strict needs, and I hate how much slower GCC has become over the years. But there's no question that they're doing some good stuff. And I just like the KDE group, and have found it very responsive when I had issues."</i> Nice to hear -- keep on <a href="http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&bugidtype=include&bug_id=&votes=&emailreporter1=1&emailtype1=substring&email1=torvalds&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&order=Reuse+same+sort+as+last+time&cmdtype=doit">reporting bugs</a>, Linus!
<!--break-->
