---
title: "KDE-CVS-Digest for May 2, 2003"
date:    2003-05-03
authors:
  - "dkite"
slug:    kde-cvs-digest-may-2-2003
comments:
  - subject: "Tnx Derek!"
    date: 2003-05-02
    body: "Im the first to say so this time?\n\nyay me!"
    author: "cobaco"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-03
    body: "Can we please stop this \"I was the first to thank Derek\" contest? It's getting old..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-03
    body: "You're right, it's more trendy and fashionable to say that arts and noatun are buggy and sucky :-)"
    author: "Ricardo Galli"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-03
    body: "arts and noatun are good"
    author: "Bob"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-03
    body: "Tell where do you live and which desktop you use.\nMP3 or vorbis streaming? video? not at all, it doesn give a clue to the user that it cannot play nothing from the network. artsd sucking 5% of the CPU doing _nothing_? stupid noatun default playlist, etc. etc. \n\nnoatun is unusable, artsd is buggy."
    author: "gallir"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-03
    body: "Tell where do you live and which desktop you use.\nMP3 or vorbis streaming? video? not at all, it doesn give a clue to the user that it cannot play nothing from the network. artsd sucking 5% of the CPU doing _nothing_? stupid noatun default playlist, etc. etc. \n\nnoatun is unusable, artsd is buggy."
    author: "gallir"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-04
    body: "I'm pretty sure that I listed your conferences in the Balearikus Party with Kaboodle."
    author: "Josep"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-04
    body: "Uep!, not at all! :-)"
    author: "Ricardo Galli"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-03
    body: "Noatun keeps crashing for me, so I tend to pretend it sucks ;) Plus, usability is... horrible.\nBtw, thx derek and please let the noatun devs regain their pride with better software."
    author: "Henri"
  - subject: "Re: Tnx Derek!"
    date: 2003-05-03
    body: "No, sorry."
    author: "Martin"
  - subject: "=)"
    date: 2003-05-02
    body: "Great news, especially the wonderful speed improvement in Karchiver."
    author: "Michael"
  - subject: "Lot of work in KDE-Edu"
    date: 2003-05-03
    body: "These past weeks have seen a lot of cvs commits in the kdeedu module. KStars, Kig, KTouch, KEduca, Kalzium and KHangMan have been greatly improved. If you never tried any of these programs, now it's time to do so and send your comments and suggestions to the KDE-Edu team. And welcome to KBruch, try your skills in calculating fractions!"
    author: "annma"
  - subject: "Re: Lot of work in KDE-Edu"
    date: 2003-05-03
    body: "I didn't mention the language modules to khangman, danish this week.\n\nThe whole lot is quite impressive.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Lot of work in KDE-Edu"
    date: 2003-05-03
    body: "Unfortunately kdeedu hasn't compiled for me since im using kde cvs (at least since two months). Thsi is the error message:\n\nMaking all in khangman\nmake[2]: Entering directory `/var/tmp/portage/kdeedu-5/work/kdeedu/khangman'\nmake[2]: *** No rule to make target `all'.  Stop.\nmake[2]: Leaving directory `/var/tmp/portage/kdeedu-5/work/kdeedu/khangman'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/var/tmp/portage/kdeedu-5/work/kdeedu'\nmake: *** [all] Error 2\n\nDoes anyone have a auggestion?"
    author: "Steffen"
  - subject: "Re: Lot of work in KDE-Edu"
    date: 2003-05-03
    body: "> Does anyone have a auggestion?\n\nYes, you should do a    \nmake -f Makefile.cvs \nand  configure\nagain :)"
    author: "Anonymous"
  - subject: "Re: Lot of work in KDE-Edu"
    date: 2003-05-03
    body: "http://edu.kde.org/kbruch/screenshots.php\n\nThe way the Formula is displayed looks ugly, amateurish. Isn't there the possibility of some interchange with KFormula for formatting?"
    author: "Hakenuk"
  - subject: "Re: Lot of work in KDE-Edu"
    date: 2003-05-04
    body: "KBruch has just been added in cvs so it's still very imperfect. And KBruch work was somehow slowed down because we toyed with the idea of having a KMath program with parts, which could not have been done because of lack of developers. \nI will make sure your idea is taken into account by Sebastien. Thanks for your suggestion!"
    author: "annma"
  - subject: "Re: Lot of work in KDE-Edu"
    date: 2003-05-04
    body: "You are right that it isn't the best way to show the formula. But in any way it is very important, that it is very easy for the user to input the solution. So the 2 edit boxes are very easy to use. And it is really important, that these input boxes align very well to the formula. So if you can show me how to do this by using kformula, please mail me and I will try to add it to the program!\n\nSteinchen"
    author: "Sebastian"
  - subject: "Quanta+Kafka"
    date: 2003-05-03
    body: "Looks like more and work is being done with Quanta's WYSIWYG mode (aka Kafka)... hope that it stablizes for KDE 3.2! It sounds great!"
    author: "lit"
  - subject: "Accessibility?"
    date: 2003-05-03
    body: "Derek, obviously you wanted to cover Gunnar's commit to kmouth as well, since there is a star for a new feature in accessibility, but it is a dead link. Did it get lost somewhere?\n\nOlaf."
    author: "Olaf Jan Schmidt"
  - subject: "Re: Accessibility?"
    date: 2003-05-03
    body: "For all who need to know what's missing behind the dead link:\n\nI have added a first version of word completion to KMouth. This word completion is implemented as a subclass of KCompletion which gives the KCompletion exactly the ten most often used words that start with the last word of the entered string. In order to do so the word completion needs a list of words together with weights (how often they are used). Currently this list is generated at the first start of KMouth by counting the words in the KDE documentation.\n\nDiffs at http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdeaccessibility/ :\n\nkmouth/Makefile.am.diff?r1=1.4&r2=1.5\nkmouth/configwizard.cpp.diff?r1=1.3&r2=1.4\nkmouth/configwizard.h.diff?r1=1.2&r2=1.3\nkmouth/kmouth.cpp.diff?r1=1.7&r2=1.8\nkmouth/kmouth.h.diff?r1=1.2&r2=1.3\nkmouth/phraselist.cpp.diff?r1=1.4&r2=1.5\nkmouth/phraselist.h.diff?r1=1.2&r2=1.3\nkmouth/version.h.diff?r1=1.4&r2=1.5\nkmouth/wordcompletion/Makefile.am?rev=1.1&content-type=text/x-cvsweb-markup\nkmouth/wordcompletion/wordcompletion.cpp?rev=1.1&content-type=text/x-cvsweb-markup\nkmouth/wordcompletion/wordcompletion.h?rev=1.1&content-type=text/x-cvsweb-markup\nkmouth/wordcompletion/wordcompletionwidget.cpp?rev=1.1&content-type=text/x-cvsweb-markup\nkmouth/wordcompletion/wordcompletionwidget.h?rev=1.1&content-type=text/x-cvsweb-markup\nkmouth/wordcompletion/wordlist.cpp?rev=1.1&content-type=text/x-cvsweb-markup\nkmouth/wordcompletion/wordlist.h?rev=1.1&content-type=text/x-cvsweb-markup\nkmouth/wordcompletion/wordlistui.ui?rev=1.1&content-type=text/x-cvsweb-markup\n"
    author: "Gunnar Schmi Dt"
  - subject: "Re: Accessibility?"
    date: 2003-05-03
    body: "Hmm. Indeed it was selected for inclusion. I got a bug in the script somewhere.\n\nThanks. I see than Gunnar has posted a description.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Accessibility?"
    date: 2003-05-03
    body: "I fixed the problem. The update includes the kmouth patch, and Gunnar's comments from here.\n\nNote to self: Accessibility is spelt accessibility.\n\nThanks to all who noticed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Accessibility?"
    date: 2003-05-04
    body: "Thanks, Derek!"
    author: "Olaf Jan Schmidt"
  - subject: "Flicker in KDE"
    date: 2003-05-03
    body: "Hey all,\nKonqueror flickers when you switch tabs; as a matter of fact, lots of KDE apps have flicker. I was wondering if anything was being done about this; I emailed the kde-devel list, but there was only 1 response. I think that the flicker is the only thing GUI-wise that KDE lags behind other major GUIs; if that got fixed, KDE would look a lot better. What are your thoughts?"
    author: "Praveen Srinivasan"
  - subject: "Re: Flicker in KDE"
    date: 2003-05-03
    body: "Well, most people know it. The problem is that few people are working on it :)\n\nIt is also not that easy, since a lot of these problems are in Qt or are caused by interactions with Qt or Qt's API. (The KIconView flickering seen on the desktop is a good example of flickering caused by the Qt API - it's much better in HEAD though, thanks to the optimized icon rendering)\n\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: Flicker in KDE"
    date: 2003-05-03
    body: "Versions of KDE prior to 3.0 didn't flicker, IIRC. I remember thinking that KDE was flicker free, and Gnome certainly wasn't. Wonder what happened..."
    author: "ac"
  - subject: "Re: Flicker in KDE"
    date: 2003-05-03
    body: "\nI agree fully with you  that this flicker really gives a bad impression when you show\noff KDE to somebody.\nIt feels and looks pathetic.\nIt seems all the hackers got used to it and don't care about it anymore :-(\n\nSometimes very little details are very important\nto the overall impression of a software. The same is true for default settings, as\nmost users never change anything.\n\nNot all of us are hackers,\n\nanyway we enjoy KDE"
    author: "KDE User"
  - subject: "Re: Flicker in KDE"
    date: 2003-05-03
    body: "What is this flicker you guys are talking about?  I must be one of those who is too used to it to notice; can anyone describe when exactly this flicker happens?\n\nthanks,\nLMCBoy"
    author: "LMCBoy"
  - subject: "Re: Flicker in KDE"
    date: 2003-05-03
    body: "Well, if I have two tabs open in Konqueror and change between\nthem, the whole window is repainted. Together with the \"delayed tabs\"\nbug this all seems a bit \"shaky\".\n"
    author: "Michael"
  - subject: "Re: Flicker in KDE"
    date: 2003-06-11
    body: "Also, when you do mouseover an a link in Konqueror. Do it repeateadly and the whole screen starts flickering like mad...\n\nI use Opera instead. Too bad, since I like Konqueror's bookmark scheme."
    author: "Fredrik Carl\u00e9n"
  - subject: "Kile/Kite. Please, need stable downloads!"
    date: 2003-05-03
    body: "I was shocked by the disappearing of \"Kile\" (\"KDE Interface to Latex Editor\" or something like that). It's the best Latex editor I used and still use (it's my everyday app right now)\n\nIs there a reason for this name change? and what's worse: is there _anywhere_ that one can download version 1.5????? One needs some guarantees to get a stable version, not just CVS.\n\nThere's no way to download in Brachet's website now, and Kite doesn't have stable package downloads.\n\n"
    author: "uga"
  - subject: "Re: Kile/Kite. Please, need stable downloads!"
    date: 2003-05-03
    body: "http://perso.club-internet.fr/pascal.brachet/kile/kile-1.5.tar.gz"
    author: "Anonymous"
  - subject: "Re: Kile/Kite. Please, need stable downloads!"
    date: 2003-05-03
    body: "Thanks very much :-) At least I can keep the source now."
    author: "uga"
  - subject: "Re: Kile/Kite. Please, need stable downloads!"
    date: 2003-05-05
    body: "Kile got a new life, it's now in kdeextragear. First it was renamed to kite, but then the new maintainer got the permissions to use kile, so he changed it back again... "
    author: "Georg Zigldrum"
  - subject: "KMail smart quoting!"
    date: 2003-05-03
    body: "> Smart splitting of quoted paragraphs.\n\nThanks Ingo!  Can't wait to see this feature in action :)"
    author: "LMCBoy"
  - subject: "Konq to display free space on partition?"
    date: 2003-05-03
    body: "\nHi,\n\nI once reported this as wish, but it got closed :-(\n\nhttp://bugs.kde.org/show_bug.cgi?id=24143\n\nKonqueror gets better and better, and the more I use it the\nmore it annoys me that I cannot see how much free space I have on the partition\nthat I currently browse.\n\nI don't think this would have a performance impact. For sure it is much slower\nto RMB on a file and look there for the free partition space, which btw is an ugly\nworkaround, coz what has free space to do with the properties of a single file ?\n\nwell, anway KDE rocks"
    author: "Yves Glodt"
  - subject: "Re: Konq to display free space on partition?"
    date: 2003-05-04
    body: "Well this is definately a good idea.  I will try to vote for it."
    author: "Ben Atkin"
  - subject: "Kexi"
    date: 2003-05-03
    body: "You should also try out TOra, an oracle admin tool. Very advanced. You can easiely use it for other databases. Hope KExi will become as powerful. Perhaps they shall adopt some TOra code."
    author: "Hakenuk"
  - subject: "Re: Kexi"
    date: 2003-05-03
    body: "http://www.globecom.se/tora/"
    author: "Hakenuk"
  - subject: "Re: Kexi"
    date: 2003-05-04
    body: "hi,\n\n\ttora is definitly a nice tool. but i think it doesn't have much to do with kexi except that the word 'database' is mentioned in both descirptions ;)\nthe aims of these projects are compleatly different.\n\twhile kexi tries to help the user with processing and storing of data unsing databases as medium to access user's data tora administrates these databases. i plan to pull kexi even more in its direction for the next stable and take it away form its 'access' copy state.\n\thowever i don't think kexi and tora are competeing against each other but can be both used to extend each other. so in my eyes there is no special need to merge these two projects. maybe a button in kexi's project menu 'administrate with tora' wich launches tora but not more.\n\tif you really want to compare kexi with another free 'equivalent' you should take knoda.\n\tin any case i think a tool like kexi has definitly more todo in koffice than tora. since administrating a database with the features and complexity of tora is a taks for powerusers not average office users.\n\nlucijan"
    author: "Lucijan Busch"
  - subject: "Re: Kexi"
    date: 2003-05-04
    body: "Not being funny, but is there any particular reason why you decided not to use the Qt SQL drivers? \n\nOn the face of it, it creates a whole lot of duplication and possible incompatibilty/unexpected behaviour. "
    author: "rjw"
  - subject: "Re: Kexi"
    date: 2003-05-04
    body: "I'm not sure why he chose to use them, but in my case I chose them because it provides a common API for all the databases, so one can \"easily\" write an app that supports several databases, just by changing the driver.\n\nThis is, of course, not completely true. The support of Qt SQL drivers is not complete yet, and I tend to use queries too many times to accomplish this. But the idea itself is good IMHO.\n\nI hope they improve the support in the next versions.\n\n>duplication and possible incompatibility/unexpected behaviour.\n\nI had no problems like this. The only problem I found with support for MySQL for example was the handling of BLOB data type, but it's properly documented, and explained how it should be used. Yes, it's difficult, but the behaviour is really predictable (no unexpectancies here)."
    author: "uga"
  - subject: "Re: Kexi"
    date: 2003-05-04
    body: "Sorry, my mistake. I read \"decided to use the ....\" instead of \"decided NOT to use\". My mind was faster than my eyes sampling data :-)"
    author: "uga"
---
This week in the <a href="http://members.shaw.ca/dkite/may22003.html">latest KDE-CVS-Digest</a>: <a href="http://edu.kde.org/kbruch/">KBruch</a> added to <a href="http://edu.kde.org/">KDE-Edu</a>,  fixes to KHelpCenter, scripting interface updates for <a href="http://edu.kde.org/kstars">KStars</a>,
<a href="http://kmail.kde.org">KMail</a> editing 
improvements, the beginnings of <a href="http://www.postgresql.org">PostgreSQL</a> 
support for <a href="http://www.koffice.org/kexi/">Kexi</a>.  And more.
<!--break-->
