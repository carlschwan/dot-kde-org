---
title: "LinuxTag 2003: KSpread Development Roadmap"
date:    2003-07-25
authors:
  - "numanee"
slug:    linuxtag-2003-kspread-development-roadmap
comments:
  - subject: "It's good to see commitment to Kspread"
    date: 2003-07-25
    body: "It's nice to read this. I built version 1.2 and then I tried 1.3 beta. The beta was much faster but for some reason it's been unstable lately, so I built CVS and it's been very frustrating. Save As has taken it all down and I've been renaming autosave files which then save fine. I did do a parallel CVS build and was using qt-copy but I've switched back and I'm rebuilding KDE 3.1.2 and CVS. Maybe things will be more stable. Right now in CVS linking to a cell on another page and doing an \"if\" to compare the text has it comparing accurately if I manually enter the text but not if I paste it. Text comparisons are also case sensitive too, which I don't think users expect, as well as \"==\" being the comparison operator while I don't think \"=\" is an assignment operator. It's okay for me as a programmer, but for joe user...\n\nI really love spreadsheets, and I miss Mesa/2 on OS/2 which I beta tested extensively. There are a few things I find really frustrating on Kspread. I have not found an easy method to create date sequences. I'm trying to recall if I succeeded last time. It's not inuitive, and Kspread seemed intuitive when I first tried it. I also can't figure out how to use cell references to adjust formulas based on the cell or row and I can't seem to find sumproduct to multiply two columns. I know, just like with Quanta, you can only do so much, but for me the problem seems to me that I can't really seem to do work on Kspread without the love/hate aspect. OO Calc has great stuff but it's a dog and dog ugly. I wish Kspread were just a bit better. Also areas it seems to have gone back downhill are in building formulas by point and click. A number of times a click just leaves an error where the operator is there without a cell reference during edits. Clicking across pages seems completely disfunctional using CVS on KDE 3.1x. Also find and replace no longer seems to have the power to look inside cells. I also see no easy way to detect the last cell with data in a range. That would be handy. Other things I miss are the custom features to build combobox lists and scripts... I should say something good. My wife is now a spreadsheet junkie too and other than having to break things down to smaller sheets she has no complaints and does everything under the sun on Kspread. I'm just more demanding. ;-)\n\nWhile we're at it I have a request. It looks like DCOP may be good enough to enable poking data into cells and such. What would be really sweet is the ability to call a shell command with parameters. Then I could easily use Kommander for custom data entry on little spreadhseet apps. It wouldn't take much to get these two to do great things together. I should probably get involved in advanced testing... I just have my hands so full with Quanta, but I really want Kspread to be great. BTW the coolest thing with Mesa/2 was it's dynamic object copy feature. You could create a formula in cell E3 and drag copy down to E15. Then you could edit the formula with a correction in E3 and E4:E15 would change automatically. It was fun helping to spec a lot of interface features there too... "
    author: "Eric Laffoon"
  - subject: "Re: It's good to see commitment to Kspread"
    date: 2003-07-25
    body: "I won't tell you something new, but what about bugs.kde.org wishes so other people can vote?\n"
    author: "AC"
  - subject: "Re: It's good to see commitment to Kspread"
    date: 2003-07-25
    body: "I'm rebuilding everything on my system right now (along with recovering greehouses, getting new product lines together, trying to get a Quanta snapshot out and getting ready to go out of town for the weekend...) As soon as I can confirm these bugs on a clean build I will report and get involved. (I'm just afraid I'll look at the code and have to give up sleeping)"
    author: "Eric Laffoon"
  - subject: "VBA???"
    date: 2003-07-25
    body: "VBA?\n\nI would be very good to support VBA, I guess Mono implements VB.NET. And perhaps also advances Linux basic implementatiosn like Hbasic may help: hbasic.sf.net"
    author: "Gerd"
  - subject: "Re: VBA???"
    date: 2003-07-25
    body: "You want to implement virus support in kspread??"
    author: "renaud"
  - subject: "Re: VBA???"
    date: 2003-07-25
    body: "We will have a JavaScript based scripting language as a plugin (but without many dangerous things, like access to other applications/file system...). Mainly you will be able to do all the things you can do with the GUI."
    author: "Norbert"
  - subject: "Re: VBA???"
    date: 2003-07-25
    body: "Are you using QSA? Will thre be DB access and all Qt features?"
    author: "panzi"
  - subject: "Re: VBA???"
    date: 2003-07-25
    body: "My current plugin on the hard disc uses QSA indeed, but I'm thinking about using kjs instead. \n\nYou will be able to do everything you can do using the KSpread user interface, so you get the database access KSpread currently has."
    author: "Norbert"
  - subject: "Re: VBA???"
    date: 2003-07-25
    body: "Basic is from hell. There's no doubt about that. If you can initiate a shell process then you can run a Kommander dialog which can speak DCOP. So whatever you have available in DCOP is what it could do. This would be very cool because it's here now (in the quanta module) and it doesn't require any actual scripting for simple tasks like a data entry widget. Have you looked at Kommander? It's based on Qt Designer and the concept of associating text with widgets to assemble strings. It can use scripting in any language that runs in the shell and is DCOP enabled. Our objective is to extend it to where it is very user friendly to non programmers. We will also be adding data widgets soon.\n\nIt would be very cool to be able to use visual dialogs for spreadsheet mini applications."
    author: "Eric Laffoon"
  - subject: "Re: VBA???"
    date: 2003-07-25
    body: "It would be nice with a small Kommander tutorial. I see you talking about it a lot, still I don't know exactly what the h*ll it is. ;) A small text to help people to see the light would be good.\n"
    author: "Apollo Creed"
  - subject: "Re: VBA???"
    date: 2003-07-26
    body: "See: http://quanta.sourceforge.net/main2.php?snapfile=snap02\nAlso check out the quanta/quanta/data/scripts directory in the Quanta CVS HEAD (KDE CVS servers) for examples.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: VBA???"
    date: 2003-07-29
    body: "There is a tutorial with examples on the Quanta site with our downloads. In a nutshell it's a graphical dialog builder based on Qt Designer. The twist is that it allows you to \"associate text\" with it's widgets. So a button could gather up text from the widgets in a dialog and assemble a string which it could sent to stdout or via DCOP, as an example of what it can do. It can also script natively in shell scripting (bash) and load and run other scripts like Perl or Python. All of this combines to make it very powerful while being relatively simple compared to language based tools.\n\nI'll attach an example. If you have Quanta 3.1 or greater installed on your system you can run Kommander files with the command \"kmdr-executor [path/]filename\" or edit them with the command \"kmdr-editor [path/]filename\""
    author: "Eric Laffoon"
  - subject: "Great Job Guys"
    date: 2003-07-25
    body: "As weird as it sounds, I prefer KOffice a million times to Open Office. KSpread is  awesome. It has a few quirks to it, but I'm confident in its future and those of other KOffice components. I only wish there are more developers for it, full time ones especially. However, you guys are doing a wonderful job and I am a great fan and a humble user of our priceless baby, KOffice. :-)\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: Great Job Guys"
    date: 2003-07-25
    body: "Thanks lot. We're glad that KSpread could be useful for you.\nWe can't promise, but you can have our words that we always improve it for the future release.\n"
    author: "Ariya"
  - subject: "Re: Great Job Guys"
    date: 2003-07-25
    body: "> I only wish there are more developers for it, full time ones especially.\n\nIt makes a big difference. Having Andras full time on Quanta has enabled us to do things on a level we would never have tried otherwise. If I had the resources I'd love to sponsor someone full time on the Kspread team. I'm reluctant to get involved myself because I already spend way too much time on Quanta. However I'm just one guy who does a little web work and owns a small business with no employees. I really don't understand why other business people can't understand it makes sense to shell out a small portion of their IT budget to sponsor development. If you guys need a business type to sit at the table and arm wrestle a potential sponsor let me know. ;-)"
    author: "Eric Laffoon"
  - subject: "koffice is pointless"
    date: 2003-07-25
    body: "so its finally catching up with gnumeric?"
    author: "printf"
  - subject: "Re: koffice is pointless"
    date: 2003-07-25
    body: "Strange comparison. Gnumeric does presentations? "
    author: "Anonymous"
  - subject: "Gnumeric"
    date: 2003-08-08
    body: "Gnumeric is the gnome-office spreadsheet, not a presentation program."
    author: "Eike Dehling"
  - subject: "Re: Gnumeric"
    date: 2003-08-08
    body: "Correct, so KOffice has at least a point for presentation."
    author: "Anonymous"
  - subject: "Re: koffice is pointless"
    date: 2003-07-29
    body: "I disagree! That's like saying \"choice is pointless\". Personally I can't wait for KOffice to mature. I really like KWord with its WYSIWYG-ness, and the other applications are pretty slick too.\n\nIf you make another choice - and use Gnumeric, or Scalc, or Excel, well that's great. It makes no difference to me. It's YOUR choice! With open source software, very little is sacrificed for you to have choices. Look at KSpread's support for the Scalc file format for example, possible because both are open source. With closed source software, making a choice would usually lock you in to varying degrees, but with open source, you can make one choice which suits you now (to use the more developed Gnumeric for instance), and switch over later (if you like!) when KSpread or KOffice offers something you would like to take advantage of (like presentations, or integration with other apps, or speed, or stability, or how it looks, or whatever.\n\nIs it really such a hard thing to avoid pointless generalizations such as \"koffice is pointless\"?"
    author: "Paul Dorman"
  - subject: "mmmmmm ..."
    date: 2003-07-25
    body: "Development is more fast in Gnumeric ... Why ?"
    author: "anonymous ..."
  - subject: "Re: mmmmmm ..."
    date: 2003-07-25
    body: "gnumeric's original author has stuck with the project for nearly five years..\n\nkspread's haven't.. through a variety of real life issues that occur with open src projects.."
    author: "anon"
  - subject: "Re: mmmmmm ..."
    date: 2003-07-25
    body: "KSpread suffers from the existance of GNumeric.\n\nI suggest GTK apps stop to exist.\n\nYours, Kay\n\nPS: Irony included, guess in which sentence ;-)"
    author: "Debian User"
  - subject: "I like the memory reduction but what about ..."
    date: 2003-07-25
    body: "Yes, ok, but what about bug 57871?\n\nThis seems to be a pretty drastic change or else it would have already been done.  I sure hope they plan on implementing this.  Not being able to select multiple, non-contiguous rows / columns means KSpread is still behind Excel from at least the Windows 98 era (6+ years ago!) ...\n\nIf this functionality interest you but have no KOffice hacking skills (like myself unfortunately) please at least vote for the bug."
    author: "Jesse"
  - subject: "Re: I like the memory reduction but what about ..."
    date: 2003-07-25
    body: "That's on our target, don't worry. But it's just that first we need to restructure internals of KSpread to become much more memory-efficient. Else, nifty features and other advanced stuff would be memory hungry and therefore less useful."
    author: "Ariya"
  - subject: "Chart support"
    date: 2003-07-25
    body: "Anyone know if people are working on improving the chart support. Currently the types of charts is very limited and have no anti-aliasing."
    author: "Vincent"
  - subject: "Re: Chart support"
    date: 2003-07-25
    body: "Anti-aliasing is a canvas problem I guess. Wait for http://www.cairographics.org/, when this is ready it is likely that someone will write an anti-aliased canvas. Right now, with libart being the only solution, it does not make that much sense...\n"
    author: "AC"
  - subject: "Switch to OpenOffice.org file format"
    date: 2003-07-25
    body: "This is, to me, the KILLER feature. I prefer koffice interface by far, but I end up using openoffice (and recommending it to others) when I need to do \"office\" stuff. The reason is compatibility with the m$ monopolic world. \n\nThe sooner koffice switches to openoffice formats and merges whatever they have that openoffice doesn't in their filters, the sooner openoffice will get widely adopted. I for one would like to see both of them coexist :-)"
    author: "MandrakeUser"
  - subject: "Re: Switch to OpenOffice.org file format"
    date: 2003-08-14
    body: "Yeah, I agree, if you switch to OOo file format so much becomes possible.\n\nusing uno, you can setup OOo to be a server, which can accept files from KSpread (in OOo format), and save them in whatever file format you want (that OOo supports). You can avoid reinventing many wheels :)\n\nJ."
    author: "Jonathon"
  - subject: "kSpread"
    date: 2003-08-04
    body: "Hmm, R integration"
    author: "bert"
  - subject: "Re: kSpread"
    date: 2003-08-14
    body: "Fabolous! Kspread with R-project support will be great!. A lot of users in scientific area will be very happy with this support."
    author: "dean"
---
During the <a href="http://dot.kde.org/1058415564/">LinuxTag 2003</a> conference, <a href="http://www.koffice.org/">KOffice</a> hackers Ariya, Norbert and Phillip had a chance to meet and discuss the current state of <a href="http://www.koffice.org/kspread/">KSpread</a> as well as the future direction of the project.  Plans include an Excel export filter as well as switching to the OpenOffice.org Calc format -- read on for the full KSpread Development Roadmap that resulted from the discussions.
<!--break-->
<p>
<hr>
<h2>kspread development roadmap</h2>
<p>Ariya Hidayat, Norbert Andres, Phillip M&uuml;ller<br>
July 12, 2003</p>
<a href="http://ariya.pandu.org/gallery/linuxtag/kspreadhackers.jpg"><img src="http://ariya.pandu.org/gallery/linuxtag/tn_kspreadhackers.jpg"></a>

<p>During LinuxTag 2003, we had a
chance to meet and discuss KSpread developments. This is a
summary of what we have discussed.</p>

<h2>what we have done</h2>

<p>We are now close to the release of KSpread 1.3 (as part of KOffice
1.3). Beta 3 will be soon available, and release date is expected to be
in mid-September. </p>

<p>Since the latest KSpread 1.2, we have done several important things
such as:</p>

<ul>
  <li>styles support</li>
  <li>performance improvement, as well as reduction in memory usage</li>
  <li>addition of many built-in functions</li>
  <li>better support for foreign file formats, including OpenOffice.org
Calc document</li>
  <li>WYSIWYG enhancement, with real zoom and high-resolution printing</li>
  <li>bugfixes and other improvement (templates, conditions,
drag-and-drop, and many others)</li>
</ul>

<p>Until now, KSpread has been maintained by Laurent Montel. Core
developers are Norbert Andres, Ariya Hidayat, Phillip M&uuml;ller, with support
from other active KOffice hackers like David Faure and Luk&aacute;&#353;
Tinkl. Documentation is mainly written by Pamela Robert. Although we do not
have an official QA team, Ferdinand Gassauer is always very helpful
in reporting various annoying bugs.</p>

<h2>where we are going</h2>

<p>Our goals for the next release are:</p>

<ul>
  <li>more efficient memory consumption, necessary for working with
large documents</li>
  <li>full support for OpenOffice.org Calc document format</li>
  <li>near 100% compatibility for Excel formula functions</li>
  <li>bidi support</li>
  <li>better and smarter handling for calculation dependency</li>
  <li>allow support for up to 4 milllions rows </li>
  <li>real-zoom for the chart component</li>
</ul>

<h2>work items</h2>

<p>Following are a few things which will be developed or are already
being worked on.</p>

<ul>
  <li>Macro recorder</li>
  <li>New format storage data structure, necessary to avoid storing
format information in each cell. Cache, tree, and other solutions for
optimization might be required but have to be researched first.</li>
  <li>Format parser to speed up painting routine. In short, all
information for painting a cell can be looked up very fast and thereby
avoid any painting bottleneck.</li>
  <li>Formula engine which will replace KoScript. It will be a
stack-based
interpreter for faster and efficient expression evalution. All built-in
function will need to be modified but then we will have the possibility
for external function definition (say, in Perl). via plug-in. There
will be also support for localized function names.</li>
  <li>Source code clean up, to make it easier for new developers to
jump in and avoid unncesseary clutterness. This will include: adding
more documentation for core classes,
redoing user interface parts into .ui files, moving dialog box code
into dialogs/ sub-directory,&nbsp; introducing namespace (KSpread::Cell
instead of KSpreadCell).</li>
  <li>Revision support</li>
  <li>Excel export filter.</li>
  <li>Faster document saving and loading. One possibility is using
experimental SAX-based document loader (vs the current DOM
approach) which has the advantage that no intermediate step (i.e
constructing DOM tree) is performed.</li>
  <li>Website rework. The current content of KSpread website is already
too ancient. Some new
screenshots (like KSpread using colorful Keramik style, or KSpread
running on HP-UX) will be additional bonus.</li>
  <li>Maintainership. As agreed, for future release Ariya will
co-maintain KSpread with Laurent.</li>
  <li>Switch to OpenOffice.org Calc file format. Somehow this
should be done in a coordinated manner with other KOffice applications.</li>
</ul>

<h2>how you can help</h2>
Since we are relatively busy with other things and KSpread has no
full-time developer, any help will be appreciated. Among others, here
are some things which you might want to have a look:<br>
<ul>
  <li>File bug report to bugs.kde.org. Check first that it has not been
reported already.<br>
  </li>
  <li>Test and report problems with filters, especially Excel filter.<br>
  </li>
  <li>Write and update KSpread documentation. Make sure to contact Pamela first.</li>
  <li>Provide regressions tests for many built-in functions.</li>
</ul>

<h2>epilog</h2>

<p>KSpread still has a future. We love it, and we will always improve it!</p>