---
title: "Usability Study: KDE Suited for Corporate Desktop"
date:    2003-08-13
authors:
  - "dmolkentin"
slug:    usability-study-kde-suited-corporate-desktop
comments:
  - subject: "Das link ist nicht Englisch"
    date: 2003-08-12
    body: "The PDF is in German, please correct that.\n\nI read this on Slashdot and other news sites few days ago and wondered why it didn't show up here.\n\nGrats to the KDE team and its supporters for bringing KDE to what it is now!\n"
    author: "Slovin"
  - subject: "Re: Das link ist nicht Englisch"
    date: 2003-08-12
    body: "Fixed. Funny, we wait so long for the English version and then link the German."
    author: "binner"
  - subject: "Re: Das link ist nicht Englisch"
    date: 2003-08-13
    body: "English translation:\n\n  http://www.relevantive.de/Linux_e.html\n\nSlashdot:\n\n  http://developers.slashdot.org/developers/03/08/12/2224218.shtml?tid=106&tid=121&tid=185&tid=189"
    author: "Kurt Pfeifle"
  - subject: "I agree with the study"
    date: 2003-08-12
    body: "Linux is definetely very usable for most things. But, as it said the interfaces of programs aren't always too consistent, worded in the best way and the system doesen't have enough clear erros to let the user know what the problem was. \n\nOn my priority list, in the top 10 problems with Linux is documentation, for develoeprs and suers, which is clearly lackign and not up to spec.\n\nI'm very happy to see that KDE is focusing on usability and accesibility, not uite as much as GNOME, but still a step in the right direction, fixing the clock and the proposed changes to teh Kmenu are all good examples of this."
    author: "Mario"
  - subject: "Re: I agree with the study"
    date: 2003-08-16
    body: "I agree with the study too. But here are my observations on the subject.\n1) People using home desktop systems, more often than not, install and uninstall software. This is very tough on linux (easy on commandline, not for newbie though). OK this is a wellknown problem....\n\n2) I have observed many people using a combination of the browser and a wordprocessor for storing stuff. They just mark it on the browser and paste it on a wordprocessor (IE and Word mainly). The text appears along with graphics etc. In fact, we were 'taught' that method where I am attending my postgraduate course (most of my colleagues are from life sciences). Many people are used to this and jump to KDE easily (since it is so 'similar' to what they are used to). But this method doesn't work on KDE. It would be good if it worked. It is a very useful feature as far as convenience is concerned.\n\nSince my course concerns bioinformatics, this method is extremely useful to store output of any sort of online analysis on the variety of database systems out there (such as  NCBI or Genbank). A good method to incorporate the same in documents would be appreciated. Linux provides all the goodies for bioinformatics except this convenience."
    author: "Rithvik"
  - subject: "Re: I agree with the study"
    date: 2003-08-16
    body: "<i>1) People using home desktop systems, more often than not, install and uninstall software. This is very tough on linux (easy on commandline, not for newbie though). OK this is a wellknown problem....</i><br>\n<br>\ni dont know whats wrong in using one of the package tools that come with consumer distros (up2date | red-carpet | the mandrake thing | synaptic) arent really hard to handle. of course synaptic is a bit more complicated, but so is debian to the newbie.<br>\n<br>\ni dont see a problem here, as most linux distros come with very large repositories and joe user doesnt want to install some cvs-stuff.<br>\n<br>\n<small>html-encoding is not selectable FIX THIS</small>"
    author: "nuf"
  - subject: "Re: I agree with the study"
    date: 2003-08-18
    body: "Windows software come on CDs with magazines. Very easy to install or uninstall software from there. They seem to have no dependency problems, even without network. Linux apps that come on CD may or may not (more often not) install without searching the net. \n\nThe up2date method you are talking about needs the network. I use Gentoo, so emerge works well (again needs network). As far as the newbie is concerned, he/she will first install from a CD, particularly if the software is too large for the network. This is very difficult for a newbie, especially when cryptic dependency messages pop up."
    author: "Rithvik"
  - subject: "Won't open"
    date: 2003-08-12
    body: "Konqueror gives me this error:\n\n\"Could not open file /tmp/kde-wolf/konquerorsRKCJb.pdf.\"\n\nHowever, i can open the Executive Summary pdf just fine."
    author: "Alex"
  - subject: "Re: Won't open"
    date: 2003-08-12
    body: "Could be related to a recent change to nsplugins that was since reverted.  Try open with -> KGhostView or open with -> acroread instead."
    author: "George Staikos"
  - subject: "Re: Won't open"
    date: 2003-08-13
    body: "KGhostview gives the same error on KDE 3.1.3"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Won't open"
    date: 2003-08-13
    body: "Try xpdf instead. It works for the documents I try. KGhostview and Ghostview usually don't show any text."
    author: "Erik"
  - subject: "Nice one"
    date: 2003-08-13
    body: "I've been really impressed with how KDE has been improving over the years (I started with, um, the beta 2 which came before 1.0), and things like this show just how far it's got. Plus, constructive criticism will be invaluable. How much does Microsoft have to pay for information like this?\n\nMy main gripe with Linux at the moment is that there's too much software, not all of which is finished. There being too many applications can be bewildering, especially when there's three music players, five email programs, etc. I'd rather the distibutions installed a subset with just the basic, polished stuff, albeit not in a Red Hat way - keep with KDE stuff as appropriate, as multiple user interfaces are even more annoying. Clear, Obvious Pointers as to what else is available on the discs are a must.\n\nThe package manager in YaST2 is great in that way - I've often gone looking for random bits of software by typing stuff in the search section. It's like Google, but without any download or compilation times...\n\nWoo. Rambling nonsense. Go me!\n\n\n"
    author: "Adam Foster"
  - subject: "Re: Nice one"
    date: 2003-08-13
    body: "> There being too many applications can be bewildering\n\nThe thing is that Windows itself has *many* more programs than Linux+X11 has.\n\nOften the problem is that the distro at hand installs too many software packages by default. A home desktop-related distro, for example, probably shouldn't install much of the kde admin stuff, and a workplace-related distro shouldn't install kdegames and kdetoys. \n\nI know choice is very good, but I think distros should also pick only one browser, office suite, desktop environment, etc.. to install by default too."
    author: "fault"
  - subject: "Re: Nice one"
    date: 2003-08-13
    body: "> I know choice is very good, but I think distros should also pick only one browser, office suite, desktop environment, etc.. to install by default too.\n\nThe Caldera distribution was like that. It was more usable for beginners."
    author: "andrianarivony"
  - subject: "Re: Nice one"
    date: 2003-08-13
    body: "I've always preferred the distros/systems that came with a fewer number of packages. I've seen more than one newbie bewildered at the huge number of packages available during installation. Currently I'm using FreeBSD, which has over 9000 now, but then again, I'm no longer a newbie. For someone who doesn't know the difference between pico and jed (let alone kedit, kwrite and kate), something basic like Slackware may be more up their alley, despite it's reputation as being \"hard\".\n\nIt would be great if SuSE (as an example) marked a small set of packages as \"stable, finished and recommended\", then kept all the others out of the installation process.\n\nCan you imagine Windows coming on a multi-DVD set containing every available shareware from download.com, which you had to choose from during installation? For the power user that may sound great, but the average Windows user would find it annoying and frustrating. Why do so many Linux distros feel they should do the equivalent?"
    author: "David Johnson"
  - subject: "Re: Nice one"
    date: 2003-08-14
    body: "I think many do that. Redhat AFAIK is repackaged by many to be on one CD. So maybe you should push newbies towards those distros. These will be binary compatible with RH. They instal exactly the same packages."
    author: "Maynard"
  - subject: "Windows centric tasks"
    date: 2003-08-13
    body: "As someone who never used Windows on my system except for WABI, I found the tasks to be worded according to the Windows paradigm.  Specifically the reference to: \"personal folder\".\n\nIIUC the KDE desktop had been setup to have a \"personal folder\" on the desktop which is the: \"Document path\" directory.\n\n[Or, perhaps it looses something in the translation.]\n\nIf there is a usability issue there, and I think that there is, it needs to be addressed in KDE.  As the report says, years of using Windows must be taken into account.\n\nWhen you open an application in KDE, the default directory is either, for KDE applications, the \"Documents path\" or for non-KDE applications the $HOME directory -- some applications such as WP and OO are individually configurable (which they did with OO)..\n\nSince the $HOME directory on a *NIX system gets clogged up with a lot of junk, it would be an improvement if there was also (or instead) a \"User path\" that would be the default directory for all applications that store or load files (and have an environment variable for it).  Perhaps this would just be the \"Documents path\" but I find that a poor choice for the name when used in this context.\n\nSome would like for this to be the \"Desktop\".  If it is configurable, that is a non-issue.\n\nSince this directory would soon fill up, it will need to have subdirectories.  It should, therefore, be possible to chose the default directory for an application in the: \"*.desktop\" file.  It would be a good idea if there were default subdirectories which would apply to certain classes of applications.  Please no cute names like Windows: \"My Music\" but the idea isn't that bad.\n\nI have several including: Documents, Graphics, Music, and DownLoad, but these are in $HOME (rather then in *USR_DIR\"), because that is how *NIX works.\n\nThese directories should also show up on the K menu, (perhaps) the Desktop, and in Konqueror (User on the tool bar next to HOME, and the others in the sidebar).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Link Gives error message"
    date: 2003-08-13
    body: "The pdf link isn't opening in Konqueror or Kghostview. I get the same error the gentleman above posted.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Screenshot from Hell"
    date: 2003-08-13
    body: "\"Core-developer Waldo Bastian already proposed a change (Screenshot) for a usability problem in the K Menu raised in the study.\"\n\nThe screenshot looks worse than the most insane flyout menu madness of Win98\n\nThere are simply TOO MANY menu items.  It's truly a bad UI design."
    author: "OSX_KDE_Interpollination"
  - subject: "Re: Screenshot from Hell"
    date: 2003-08-13
    body: "Perhaps you don't see the changes made.\n\nThis sin't about how amny items there are in the menu, its about separating the items. The silver background with text like \"most used applications\", \"other actions\", and \"applications\" are good ui wise, it helps the suer tell what the items in each category are.\n\nof course it could be amde ebtter ina  lot of ways, but this si stilla n improvement IMO."
    author: "Mike"
  - subject: "Re: Screenshot from Hell"
    date: 2003-08-13
    body: "I don't like it either.  It doesn't make it more usable.\n\nCould we try making it a sub menu like the: Quick Browser.\n\nThis has its pros and cons:\n\nThere would be no question about what it was.\n\nIt would be harder to find.\n\nIt could have more entries without filling the screen height.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Screenshot from Hell"
    date: 2003-08-13
    body: "> There are simply TOO MANY menu items. It's truly a bad UI design.\n\nIt's actually a worse UI design to hide options (in this case, menu items) or nest them too much. Decades of human-computer interface research has shown this. \n\nSo, either split the kmenu into several standalone menus that get their own buttons in kicker (which itself has usability issues), or make it easier for the user to mentally categorize items without hiding them. The latter is what is being down with the new kmenu headers."
    author: "fault"
  - subject: "NOO!"
    date: 2003-08-13
    body: "Not more buttons on Kicker, if you want to split Kmenu you can make it like the START menu in Windows, same diea not implementation, but don't make me have mroe buttons on Kicker.\n\nAnyway, i see nothing wrong with Waldo's screenshot, IMO it is a slight improvement."
    author: "Mario"
  - subject: "Re: NOO!"
    date: 2003-08-13
    body: "The enduser in the study is not a power user and the XP way og doing this is also a problem - anyone with enduser experience (newbies) would tell you that. Waldo's shot seems to be based on XP's menu - from the same folks that gave us the fading-in popup menu (copyed into KDE at one point), and how bright was that?\nEndusers- this from the perspective of someone working at a hotline dealing with windows problems - need to have as few options as possible so that everything is reached with so few clicks as possible. Dont forget that newbies do not understand  the concept of the popup menu, moving windows, single contra double click etc.\nWaldo's suggestion misses the point, of course depending on wich users you want to reach .\n\n"
    author: "J. And"
  - subject: "it is an improvement"
    date: 2003-08-14
    body: "All he does is tell the user what each section is for, so he won't be confused on why the top 5 apps change or anything like that. He doesen't need to have any more clicks, or options, all waldo did was add headings and its a good idea."
    author: "Mario"
  - subject: "Re: Screenshot from Hell"
    date: 2003-08-13
    body: "This will sound trollish, but I think its true:\n\nAs a Linux/KDE OS user and developer myself, I am sure things will 'evolve' once Longhorn showcases its new user interface design and OS developers have something new to copy again. That is, if X doesnt get in the way of competing with the to be replaced windows GDI/GDI+."
    author: "AC"
  - subject: "Re: Screenshot from Hell"
    date: 2003-08-13
    body: "I sincerely hope you are not trying to say you cannot come up with a single original idea. 'Cause if that is the case, we are doomed. Please have a bit more confidence in yourself. I have seen screenshots of previews of Longhorn and all I can say is I'll pass. All it seems to do is waste screen estate. I say this as a reluctant user of WinXP configured to the 'classic windows' look. The default look of XP makes me physically sick, and I do not mean rhetorically."
    author: "ne..."
  - subject: "Re: Screenshot from Hell"
    date: 2003-08-14
    body: "Oh I am confident and I have plenty several practical ideas going on already here. But actually it is people who say they prefer the classic windows look (to fend off physical illness..?) who take away my confidence.\n\n\"We\" are not \"doomed\" as you put it, I just indicated that \"we\" are doomed again to follow UI ideas set out by Microsoft (and Apple)."
    author: "AC"
  - subject: "Save document ... in WORD format, HELLO?!"
    date: 2003-08-13
    body: "Why did they think that this was not Windows specific?\n\nAnd what is the point?\n\nYou can configure OO to use *.doc as the default save format.\n\nOOOOOOOPPPPPPPPPSSSS!  Try again.\n\nI find it quite dificult to save as a STAROFFICE format in M$ Word. :-D\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Rearrange The Menu"
    date: 2003-08-13
    body: "The suggestion by Waldo is good but the most raised issues regarding KMenu still remain. I've always came across people write \"... Too many choice in the start menu of Linux GUI... bla... bla...\"\n\nI would like to suggest that the menu items Development, Edutainment, Games, Graphics, Internet, Multimedia and Office to be group under the submenu of Application. If this proposal is combined with Waldo proposal, there will be less items on the main menu.\n\n\nThanks"
    author: "Mohd Azhar Ariffin"
  - subject: "KDE + OpenOffice"
    date: 2003-08-13
    body: "Well the whole concept of a corporate desktop is pretty cool. But to create an user experience without bumps and potholes, OpenOffice should be integrated into KDE in a better way. At least the FileOpen and PrintFile dialogs should look the same as in KDE.\n\nHaving supported a bunch of \"user champions\" in a very conservative German bank for a while I have the experience that many of them have problems performing relatively basic tasks like \"open a file\", \"organize your files in a hierarchical file systen\", \"find your document and open it\". With some training and endless Q&A they're able to recall the controls by their looks and perform the task. Anything that's different or unknown confuses them (enough the differences between Word and Excel in some parts of the user interface e.g. options dialog)\n\nThey're like a lost dog who is erratically roaming the streets of the town until it has found its home by the smell, looks, noise etc. Having a different look and feel in the Office program is unnecessarily confusing our poor doggy ;-)\n\nIt should be possible to have a KDE consistent look and feel for OOo or develop Koffice to a point where it is competitive. As OOo is Sun- sponsored and Sun is promoting the \"Other Desktop Environment\" that's the task of the KDE folks. \n\nConrad\n\nPS: tell me where I can help ;-)"
    author: "beccon"
  - subject: "Re: KDE + OpenOffice"
    date: 2003-08-13
    body: "> PS: tell me where I can help ;-)\n\nWriting a OOo wrapper for the dialogs would be a start :) Using KDE icons for OOo is possible since OOo 1.1 btw. There is a script on kde-look which brings your favourite kde icon theme to OOo\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: KDE + OpenOffice"
    date: 2003-08-13
    body: "Everything is possible, the problem is to get the funding or to find the volunteers :)\n\n(Personally I would prefer a competitive KOffice, OOs is only a short-term solution as it uses the wrong technology)\n"
    author: "Jeff Johnson"
  - subject: "Re: KDE + OpenOffice"
    date: 2003-08-13
    body: "I agree. A very *cool* improvment would be to have Gnome, KDE, XFCE, WMaker (GNUStep), etc. applications using the same Open / Save dialog according to the desktop environment. \ne.g. when using WindowMaker, saving a file open the Save dialog of WindowMaker.\nNext step would be to add the same possibility in Mozilla, OOo, etc."
    author: "andrianarivony"
  - subject: "KMenu - structure and search function"
    date: 2003-08-13
    body: "The content and structure of K-Menu has been discussed for a long time. \nSome prefer the structure from one distribution or the other. \nBut 2 things are realy important\n- wording: how can a newbe know what k3b or kile or ... is?\n- search function: K-Menu needs a (fuzzy) search as in the help system which brings up a list of possible apps. No matter how good the structure, wording and translations are, some users (propably a function of expertise) will look for other wordings and not recognize the purpose of the application, because they are not familiar with the specialized terms of EDP people. \nIMHO it shouldn't be to difficult to implement the search function of the help system for KMenu.\nBTW Here on my development system I have 600 desktopfiles (= applications)  in /opt/kde3/share/applnk  and hence in my KMenu,  :-D\n\ncu\nferdinand"
    author: "Ferdinand"
  - subject: "Re: KMenu - structure and search function"
    date: 2003-08-14
    body: "Maybe there could be a tool tip shown when you hover over an entry in the menu, displaying the brief description found in the menu item's desktop file. "
    author: "AC"
  - subject: "Re: KMenu - structure and search function"
    date: 2003-08-17
    body: "Why does KDE have menu entries like\n\nk3b (burn cd's)\nkonqueror (webbrowser)\nkonqueror (file manager)\nkvirc (chat program)\n\nOf course, I might not be entirely correct as I use the Dutch KDE, but still, there they are all.\n\nAnd while I am at it: on Windows nobody really complains about \"Word\" or \"Excel\" or \"WinZIP\" or \"Internet Explorer\" or \"Outlook\" or \"Windows\" or \"VNC\" or \"RealPlayer\" or \"QuickTIME\" or \"Safari\" or \"RagTime\" or \"Delphi\" or \"Encarta\" in the start menu.\n\nIn fact, I once wrote an application which automatically started OO Writer, Word or WP if you clicked \"Word Processor\", but nobody I know wanted to use it, they all preferred to click the name of the application."
    author: "Daan"
  - subject: "Re: KMenu - structure and search function"
    date: 2003-08-17
    body: ">>on Windows nobody really complains about \"Word\" or \"Excel\" or \"WinZIP\" or \"Internet Explorer\"...<<\n\nThere's an important difference: Allmost those apps that you list need to be installed separately by the user, so you can assume that the user knows what they are. The few apps that ship with Windows have pretty obvious names. \n\nBut on KDE systems there's usually a huge number of applications preinstalled, and a new user has no idea what they are good for.\n"
    author: "Tim Jansen"
  - subject: "Re: KMenu - structure and search function"
    date: 2004-10-29
    body: "_ KMenu has another glaring hole - where are the Save/Restore Menu Tree commands ? This way when you make changes to your menu tree you can set up checkpoints as you go along and can retreat when you make a mistake. It would also allow those who are setting up a block of workstations to configure them with a common set of menu choices.\n_ Also, when you install a distro with KDE you should have three menu choices : Linux Comprehensive (the current jumble), Windows Comprehensive (jumble by function - e.g. Games -> Arcade -> Tetris -> etc.), and Build-As-U-Go (for those who want lean & mean)."
    author: "Robert Sugg"
---
German-based <a href="http://www.relevantive.de/">relevantive AG</a>, specialized in software and web usability, have released the <a href="http://www.linux-usability.de/download/linux_usability_report_en.pdf">English version</a> (<a href="http://www.linux-usability.de/download/summary_linux_usability.pdf">executive summary</a>) of their <a href="http://www.relevantive.de/Linux_e.html">Linux Usability Report</a> today. The GNU/Linux setup was a SuSE 8.2 Professional with KDE 3.1.2 desktop and OpenOffice.org preconfigured by the experts of <a href="http://www.basyskom.de/">basysKom</a> to resemble a typical corporate setup. The study is based on a broad test conducted with 60 people who had previous Windows knowledge but had never used Windows XP. For comparison, 20 other people were asked to try Windows XP for the first time. Both are
possible migration scenarios as support for Windows NT is being dropped. The study is independent, as it was conducted without a client order.  

<!--break-->
<p>Some quotes from the study: <i>"The usability of Linux as a desktop system has been experienced as nearly equal to Windows XP. A couple of tasks were, in fact, easier and faster to solve on Linux"</i>, <i>"The majority of the test participants enjoyed working with the Linux system [...]".</i> The report also reveals that <i>"80% of the Linux test participants said they would need a week or less in order to feel as competent on the tested computer system as with their current system"</i>. Burning CDs was perceived to be even easier than under Windows XP.</p>
<p>Constructive critique like poor wording of programs and interfaces was raised in the report too. The study will be one of the topics at the <a href="http://events.kde.org/info/kastle/">KDE Contributor Conference</a> where those issues will be addressed. Core-developer Waldo Bastian already <a href="http://lists.kde.org/?l=kde-usability&m=106052316605062&w=2">proposed a change</a> (<a href="http://urbanlizard.com/~aseigo/KDE-menu.png">Screenshot</a>) for a usability problem in the K Menu raised in the study. Comments and discussions on the study are <a href="http://www.linux-usability.de/forum/board.php">welcome</a>.</p>
