---
title: "Unix Review discusses Kopete"
date:    2003-07-11
authors:
  - "binner"
slug:    unix-review-discusses-kopete
comments:
  - subject: "Nice."
    date: 2003-07-11
    body: "I found it to be a nice, unbiased, overview of todays multi-IM clients.\n\n\"Yahoo (more on that shortly), IRC, Windows popup messages, and others including IRC. \"  heh, a little redundancy never hurts ;-)"
    author: "Brendan Orr"
  - subject: "Features"
    date: 2003-07-11
    body: "Kopete is really becoming a great im client, which is really encouraging. As far as those \"most-wanted featueres\" go #5 is already implemented in CVS, I left it open because I want to refactor that code, I'll present a patch for #1 sometime this week and as the links say - we're planning to finish #4 on n7y. Carsten finished #7, Till got #2 working. I think we have people working on the whole top 10 except the PWM-like tabbing in KWin (anyone?), which gives us hope that they all will be there for 3.2."
    author: "Zack Rusin"
  - subject: "Re: Features"
    date: 2003-07-11
    body: "\"remove attachments from mail\", \"find as you type\" and \"Bayesian spam filter\" are being worked on too? Great, if true."
    author: "Anonymous"
  - subject: "Re: Features"
    date: 2003-07-12
    body: "We are talking about an IM client, not a mailclient...\nBTW: the features you're talking about are implemented in KMail (ok, maybe not the first one) in combination with Spamassassin."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Features"
    date: 2003-07-12
    body: "> We are talking about an IM client, not a mailclient...\n\nWrong, read the \"most-wanted features\" link which Zack is refering to.\n\n> BTW: the features you're talking about are implemented in KMail \n\nFunny, the second is a Konqueror feature request. Read before replying!"
    author: "Anonymous"
  - subject: "Re: Features"
    date: 2003-07-12
    body: "*bash and bash and bash...*\n\nWhile I can understand how some people prefer to stay anonymous for whatever reason I'd still be happy to see them not throw away any indication of civilizedness."
    author: "Datschge"
  - subject: "Re: Features"
    date: 2003-07-12
    body: "> Funny, the second is a Konqueror feature request. Read before replying!\n\nActually, it has been suggested that the type ahead feature should be part of the general framework, accessible by any KDE application."
    author: "A C"
  - subject: "Re: Features"
    date: 2003-07-11
    body: "it\u00b4s a pity that the gpg-plugin does not work in suse > 8.1. kopete does not accept the gpg-password of the own private key :-(( or is it a bug outside of kopete, perhaps in gpg ???"
    author: "andreas mueller"
  - subject: "Re: Features"
    date: 2003-07-20
    body: "Comment the agent string in .gpg . Thats all.\n\n"
    author: "Ralph"
  - subject: "Re: Features"
    date: 2003-07-20
    body: "Oh, that sounds great, but what exactly do you mean with teh \"agent string\" in .gpg. Would be great if you could help me out ..."
    author: "Andreas M\u00fcller"
  - subject: "Re: Features"
    date: 2003-07-12
    body: "you relise gaim 'beat kopete in this review? and for good reason...maybe you should investigate why.\n\n>Caff"
    author: "caffeine"
  - subject: "Re: Features"
    date: 2003-07-12
    body: "You realize that Gaim is older?"
    author: "Anonymous"
  - subject: "Re: Features"
    date: 2003-07-13
    body: "yes\n\n>/dev/caff"
    author: "caffeine"
  - subject: "performace with 100 contacts"
    date: 2003-07-11
    body: "Hi!\n\nLast time I tried I believe 0.6a and my problem was that It was really slow with my 100 contacts ... Has that changed? As currently I still use ickle which is much faster than kopete 0.6a.\n\n"
    author: "Robert penz"
  - subject: "Re: performace with 100 contacts"
    date: 2003-07-12
    body: "same problem here. construction and destruction of a contact needs a lot of time apparently (joining a big irc channel, or starting up kopete shows this).\n\ni saw kopete moving from completely unusable (for me) to the almost-perfect-im-client in 4 months time, so i think this problem will be fixed soon..\n"
    author: "ik"
  - subject: "Re: performace with 100 contacts"
    date: 2003-07-13
    body: "> construction and destruction of a contact needs a lot of time apparently\n\nKopete CVS is a lot faster now..."
    author: "Andy Goossens"
  - subject: "Kopete is brilliant"
    date: 2003-07-12
    body: "I love kopete. I used to use windows for web browsing, email and IM. But it always annoyed me that msn signed in on its own however hard you try to stop it. Since I discovered konqueror with tabbed browsing, Kmail and kopete I've started enjoying computing again and I haven't used windows at all. I love kopete particularly because I can have contacts on different networks and I can still use my hotmail account and msn contacts. I can't wait for the yahoo network in 0.7. Only other thing I'd like is an online/offline indicator on the icon in the system tray. Plus I wish my brother could have kopete on his mac cos he's really jealous of it."
    author: "John"
  - subject: "Re: Kopete is brilliant"
    date: 2003-07-12
    body: "i'm using yahoo (cvs) and it works very well here. and another feature i really like is the ability to have meta-contacts (same person has both a yahoo and a msn contact)"
    author: "ik"
  - subject: "Re: Kopete is brilliant"
    date: 2003-07-12
    body: "Huh? Maybe I misunderstand you, but Kopete allready supports meta-contacts."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Kopete is brilliant"
    date: 2003-07-12
    body: "Looks like he knows that already and really likes meta-contacts."
    author: "Datschge"
  - subject: "Re: Kopete is brilliant"
    date: 2003-07-13
    body: "AARGH!!! I really must read posts 2 or 3 times before answering in the future |:(\nSorry."
    author: "Andr\u00e9 Somers"
  - subject: "Kopete"
    date: 2003-07-13
    body: "Kopete is great, I use it all the time for chatting with my friends via ICQ. Its ICQ integration isn't quite complete yet though, and it does crash occasionally even when I'm not doing anything with it. I figure I can live with it... if it continues to crash in 0.7 I'll try to file a proper bug report."
    author: "Paul Eggleton"
  - subject: "Re: Kopete"
    date: 2003-07-14
    body: "I can second this. It crashes when the status of an icq contact changes. But, sadly, not always does this. (It's sad because this is not easy to reproduce)"
    author: "Akos Putz"
  - subject: "what about file transfert?"
    date: 2003-07-13
    body: "what about file transfert? it's one of the greatest im feature especially when chatting with no linux/bsd/<insert your favorite unix flavor> that can't scp you. I know gnuyahoo which uses libyahoo do file transfert on ymssenger and that Licq do it with icq and I guess it must be feasible with msn and the rest. The last time I checked that feature was not working great on the main protocols (yahoo, icq & aim, msn). Has it been fixed in all those four protocols?\n\nother than that, great app :)"
    author: "pat"
  - subject: "Re: what about file transfert?"
    date: 2003-07-25
    body: "Yahoo doesn't do file transfers yet, but it's going to be one of the many things I work on after 0.7 comes out."
    author: "Matt Rogers"
  - subject: "Kopete and it's IRC client"
    date: 2003-07-13
    body: "Kopete is great but its IRC client lacks very important features that users have grown accustomed to in a basic IRC client. I'd also like to see all instant messenger clients tabbed in one window as opposed to different windows scattered all over my desktop. I use IRC the most so I eagerly anticipate ground breaking features in the next release. Once again, good job.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "SIM is the best one"
    date: 2003-07-14
    body: "The SIM-IM (http://sim-icq.sf.net) is the best icq-only im-client for KDE by far.\nIt's not very popular but those who started using it use no other ICQ-Client: all ICQ function, server-based contactlist,good KDE integration, stable and a lot of nice-to-heave features.\n\nDon't know why this client doesn't get more attention.\n"
    author: "devox"
  - subject: "licq is more better than sim"
    date: 2003-07-14
    body: "Licq is more better than sim, It support All icq for windows supports.\nBut kopete is the best.\n"
    author: "cjacker"
---
<a href="http://www.unixreview.com/">Unix Review</a> features a <a href="http://www.unixreview.com/documents/s=8472/sam0306web/">comparative  review</a> of multiple protocol instant messaging clients including <a href="http://gaim.sourceforge.net/">Gaim</a>, <a href="http://www.everybuddy.com/eb-lite/">EveryBuddy</a> <a href="http://ayttm.sourceforge.net/">spin-offs</a> and <a href="http://kopete.kde.org/">Kopete</a>. Even though the author recognizes that Kopete isn't quite finished, he finds it very appealing in <a href="http://kopete.kde.org/index.php?page=screenshots">terms of look</a> and KDE integration. He even tried the current CVS version soon to be released as Kopete 0.7.  And speaking of integration, there are now <a href="http://lists.kde.org/?t=105782910200004&r=1&w=2">plans</a> to implement one of <a href="http://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist&votes=21&order=bugs.votes">the most-wanted features</a>: <a href="http://bugs.kde.org/show_bug.cgi?id=50442">Integration of Kopete with KAddressbook</a>.
<!--break-->
