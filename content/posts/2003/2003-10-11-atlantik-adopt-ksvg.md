---
title: "Atlantik To Adopt KSVG"
date:    2003-10-11
authors:
  - "rkaper"
slug:    atlantik-adopt-ksvg
comments:
  - subject: "Congratulation"
    date: 2003-10-11
    body: "Hi,\n\ncongratulations to this decision. A new kind of freedom will be achieved when this game framework will be open enough to achieve that you can create games without coding (as in programming language use) at all.\n\nThis shows big promise. :-)\n\nSee, Free Software+Open Standard -> Innovation.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Congratulation"
    date: 2003-10-11
    body: "Obviously programming will never completely dissapear. The actual actions on the board still need to be defined somehow, so new games would depend on the amount of support for actions in the engine, which will grow as more games are officially supported or desired actions or options are sent as wishlist items.\n\nFor example the engine would need to offer an option to define whether a turn means rolling dice (Monopoly), moving a token manually (chess), spinning the wheel (Game of Life) or placing a token (Go, Othello, battleship) or even a combination of phases (Risk). SVG won't make any of that easier, but at least the board rendering will be in a documented format, acessible through a DOM..\n\nI still suppose people will need a Designer app to make or edit board games. There are already enough different actions in Monopoly that editing the board config file by hand is not something I'd recommend. But I do it occasionally myself, when I need 35 Free Parking squares to debug Free Parking behavior. ;-)\n"
    author: "Rob Kaper"
  - subject: "Re: Congratulation"
    date: 2003-10-11
    body: "Hi,\n\nmy dream would be that for my child (on its way) it would be possible to create games by composing rules. A designer app would be great to edit the meta data and attach/combine it to the SVG.\n\nI today played some KDE games (and nethack-qt). I think work could be simplied, quality (not that it's bad, to contrary) for all if more common code was used. Score, highscores and their handling are e.g. very different and sometimes absent.\n\nIdeally each game should be possible to run on a server transparently to the game code and so on.\n\nWhat SVG will do is hopefully making up a chance to attract people to use such common code. \n\nBesides, I see Atlantik and try it, but I never get to play. Even when I connect on the internet to servers with users, I can only start new games, but not really (2 players needed). Is this my fault?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Congratulation"
    date: 2003-10-11
    body: "I think there is a highscore widget that most/all kde games use?\n\nCan someone confirm that.."
    author: "JohnFlux"
  - subject: "Re: Congratulation"
    date: 2003-10-12
    body: "KHighScore IIRC.. but there is something indeed. Unfortunately I don't have much use for it, I wouldn't know of a single way to determin highscores for most board games."
    author: "Rob Kaper"
  - subject: "Re: Congratulation"
    date: 2003-10-12
    body: "No, this is not your fault. Old servers sometimes had stale connections still counted as users, this will be fixed in monopd 0.9.0 which distinguishes meta server probes from actual clients. Furthermore, some day I (or someone else) will write a bot so 2 manual players won't be necessary. And, last but not least, once kabc and the messaging clients settle a bit, I intend to add an invitation framework which should make it easier to invite friends to a game.\n\nThe current Designer in kdeaddons works nicely, though."
    author: "Rob Kaper"
  - subject: "Re: Congratulation"
    date: 2003-10-13
    body: "There are other games like this table tennis game where the sever is better integrated, so you don't have to start is seperately but you fist have to deal with a kind of launchpad. i think this is more convenient..."
    author: "gerd"
  - subject: "Re: Congratulation"
    date: 2003-10-12
    body: "Rather than having the rules attached to the SVG, instead have them in a certal server. One approach that I thought about a number of years ago was to use prolog for its inference engine. Way too slow. Instead, I later toyed with mercury which was making great headway until it got .nyeted. But the structure of mercury makes for a great fast rules engine for making logic choices.\nBut, I wrote Rob with this about 2 years ago. A bit ahead of myself.\nI just have to make more time for myself to get OSS done.\n\nBTW, the approach that I suggest would make for a very nice card playing engine as well, or perhaps a multi-level monopoly game where boards would be owned and built up with local region info. Such as a board for US, Europe, China, Russia, etc. Once you win a region, then you can advance to the continet arena for the world domination. This would allow for some intersting AI work to be done as well which would encourage some more at the university level."
    author: "a.c."
  - subject: "Re: Congratulation"
    date: 2003-10-11
    body: "Not to mention that you have a lot of work to do, especially to get all the features done:\n\n\"Adding features like ...a FUFME plug-in for some very wicked trade proposals are ... currently planned...\""
    author: "JohnFlux"
  - subject: "SVG Icons"
    date: 2003-10-11
    body: "This is sort of tangential, but I'd just like to point out how drop-dead gorgeous the new CrystalSVG icon theme is. The latest version (beta1) was just released on kdelook, and its got quite a few visible changes over the CrystalSVG 0.6 that's currently in KDE CVS. Particularly, the Konqueror buttons look nicer.\n\nI just felt like sharing that :)"
    author: "Rayiner Hashem"
  - subject: "Modification"
    date: 2003-10-11
    body: "I would like to have a patentpoly mod: get LZW patent, buy lawyers, go to prison for patent infringement, pay license fees ...."
    author: "Gerd"
  - subject: "Re: Modification"
    date: 2003-10-12
    body: "If you want to stick to -opoly games, Atlantik Designer can already help you out with that. Load the city.conf from the monopd package and start editing. You'll need to run a custom monopd server to play it though.. I suppose it would be nice if clients could just upload custom games to servers, will store that wish in long-term memory. ;)\n"
    author: "Rob Kaper"
  - subject: "Re: Modification"
    date: 2003-10-13
    body: "I thought it would be a \"as\" translation issue. However Rob, you talked about an infrastructure for online games. Can you explain it to the public? what makes Atlantik and other board games so similar? I first thought it was all about Atlantik and its anchestors, but you wrote abouit total different games like checkers. "
    author: "Leena"
  - subject: "Re: Modification"
    date: 2003-10-13
    body: "True, the rules of checkers are completely different. But it shares at least three concepts with Monopoly: turns, tokens and a board with squares.\n\nmonopd currently sees \"roll dice\" as one of the few appropriate actions for a player in his turn, but implementing a \"move token\" action would not be hard to implement. In fact, this is what basically already happens internally when you get a \"advance to Go\" card. Monopd currently doesn't check for existing tokens when a token lands on a square, because that's perfectly fine in Monopoly, but disabling/removing existing tokens (what would happen in chess or ludo) shouldn't be a complex action. Checkers removes tokens differently, but it's still by logical rules. Likewise, Go, Othello and that-virus-game-from-the-7th-Guest also remove tokens (or change ownership/color) when tokens move or new ones are placed.\n\nBut any action that happens is still a result of a very limited set of player options and events for tokens appearing on squares. What I'm trying to do, bit for bit, is to remove all the subtile differences into the engine without exposing them to the client, and just to give the client a list of valid player actions at that point.\n\nNote that it took me quite some time to just get Monopoly in a playable state and to allow for custom Monopoly boards. Any of the specific actions in other board games are likely not implemented and it will take time for each game. But all of the objects they affect already exist. I know very few board games that work with different objects than squares, tokens/pieces that move, cards and turns.\n\nPeople have played Go, chess and checkers with as little as sand and stones, so obviously these games must have a lot in common.\n\nThe online network part is just because I want the engine component to be able to run on servers. I have actually started work on a Qt-port of monopd so it can be embedded in the client, but I'd like to keep dedicated servers around for people who connect to the Internet through NAT. I also hoped it would allow for people to find other players, but it takes some patience at the moment, so you're still better off finding some friends first.\n"
    author: "Rob Kaper"
  - subject: "Increasing dependencies"
    date: 2003-10-12
    body: "So that means that kdegames will start to depend on kdegraphics? :-("
    author: "Anonymous"
  - subject: "Re: Increasing dependencies"
    date: 2003-10-12
    body: "What gave you that idea??\n\n\nRinse"
    author: "rinse"
  - subject: "Re: Increasing dependencies"
    date: 2003-10-13
    body: "Well, KSVG is currently in kdeartwork...\n"
    author: "Rob Kaper"
  - subject: "Re: Increasing dependencies"
    date: 2003-10-13
    body: "No, it isn't. You're sure you know about KSVG when you don't even know in which module it is?"
    author: "Anonymous"
  - subject: "Re: Increasing dependencies"
    date: 2003-10-14
    body: "Note to self: use Preview buttons.. of course it's in kdegraphics, not kdeartwork. By the time 3.3 is nearing, we'll see if that means an extra dependency or whether KSVG is used by anything else (justifying a move to kdelibs) or whether a non-KSVG fallback can be used (for example, keeping the current board as backup).\n"
    author: "Rob Kaper"
  - subject: "Re: Increasing dependencies"
    date: 2003-10-13
    body: "kdegraphics you mean?"
    author: "anon"
  - subject: "What about Kpainter?"
    date: 2003-10-12
    body: "Instead of using KSVG as it, isn't better to try to merge the KSVG with other vectorial developments like Kpainter to get a low level API? In this way we could get a more hierarchical library (kpainterdevice(libart/cairo)->kpainter->kcanvas->DOM+KSVG technologies) and applications like atlantik could use vectorial graphics in a efficient way without non required dependencies. Even a vector oriented desktop (like mac osx)\n\nJust a question :-)\n\nCongratulations to KSVG. Keep doing good work! "
    author: "Joe Anonymous"
  - subject: "Re: What about Kpainter?"
    date: 2003-10-12
    body: "Mac OS X is NOT a vector oriented desktop!"
    author: "MaX"
  - subject: "Re: What about Kpainter?"
    date: 2003-10-13
    body: "No it's not, but just check out how they do their 'Expose'-thingie on the Mac OS X 10.3... Cool, eh?\n\nBy the way, where is OpenGL'ed KDE software?!? Even the apps.kde.com if full of stuff that have more than 90% rating, why not start to add those to the kde-3.3?\n"
    author: "Nobody"
  - subject: "Re: What about Kpainter?"
    date: 2003-10-15
    body: "It's up to individual authors to ask for inclusion in KDE packages."
    author: "Rob Kaper"
---
<a href="http://unixcode.org/atlantik/news/2707/">Atlantik will adopt KSVG</a> to render game boards.  <a href="http://svg.kde.org/">KSVG</a>, KDE's implementation of the <a href="http://www.w3.org/TR/SVG/">Scalable Vector Graphics</a> specification, will be included in KDE 3.2, adding support for a growing technology. KSVG <a href="http://svg.kde.org/preview_kde32.html">has been maturing</a> in the kdenonbeta development module for a long while and the enthusiasm of the developers is spreading to other parts of KDE.  <a href="http://unixcode.org/atlantik/">Atlantik</a>, the leading open-source game client for Monopoly-like board games, is one of the first KDE applications slated to adopt KSVG for more than <a href="http://svgicons.sourceforge.net/">fancy icons</a>.
<!--break-->
<p>
 For a long while, the <a href="http://unixcode.org/atlantik/charter.html">Atlantik development charter</a> has included the wish to support various board games and not just the Monopoly-like board games currently supported. Defining game boards as a vector graphic will bring that goal a lot closer. The application will no longer make assumptions on the board layout based on the original design to support just one game type, instead shapes on the board are limited only to the imagination and skills of SVG artists. It is the goal for KDE 3.3 to convert the existing games to an SVG board while keeping all current behavior intact, yet allowing for far more complex boards in the future.</p>

<p>
One of the possible games that could be supported in the future is battleship. A <a href="http://www.kde.org/kdegames/kbattleship/">KBattleship</a> application is already available for KDE and in fact included in the kdegames module (or package), but the intent to extend Atlantik and support more game types has already been received positively. On the bus back to Frankfurt from the KDE Contributor Conference in Nové Hrady, KBattleship maintainer Niko Zimmerman responded with joy to the proposal that in the future Atlantik might replace the application, because it would relieve him from maintainership. Ironically Niko is also one of the main contributors to KSVG and thus responsible for creating this opportunity in the first place. Obviously replacement of KBattleship remains a long-term goal for Atlantik and replacing existing applications should only happen in KDE when all existing features are supported, but the future of <a href="http://games.kde.org/">KDE Games</a> promises to be exciting.</p>

<p>
The KSVG website states <i>&quot;the real use of KSVG is to provide something similar to the Adobe SVG Viewer on KDE: A plugin for the web browser&quot;</i>. But in the great tradition of general purpose computing, other uses for the technology are already being developed. KDE 3.2 has not even been released yet, but KDE users already have something ready to look forward to in 2004.</p>