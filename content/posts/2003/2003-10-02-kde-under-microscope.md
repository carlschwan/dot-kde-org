---
title: "KDE Under The Microscope"
date:    2003-10-02
authors:
  - "bnavindra"
slug:    kde-under-microscope
comments:
  - subject: "I wish I was in sociology"
    date: 2003-10-03
    body: "This report is very human in the sense that it talks about human interaction and must be insightful for a lot of geeks. To look at their interactions with fellow geeks at this meta-level is very refreshing.\n\nBut also, I wish that I could have typed a report about a 'scientific' subject so easily for a diploma. This report is simply the slightly abstracted vision of the development process by somebody. Quite insightful and helpful in understanding oneself and solving problems. but probably scientifically unrewording.\n\nThanks for you insightful report!"
    author: "Anonymous Yellowbeard"
  - subject: "Re: I wish I was in sociology"
    date: 2003-10-03
    body: "Making something look simple and easy is often a good hallmark of hard work and application.  :-)\n\nThe above is also mentioned to be just a summary of the whole work."
    author: "ac"
  - subject: "Re: I wish I was in sociology"
    date: 2003-10-03
    body: "Plus it's a translation."
    author: "Eike Hein"
  - subject: "Re: I wish I was in sociology"
    date: 2003-10-03
    body: "Well, first Adreas already has his \"Diplom\" -- that's what's indicated by the Dipl. in his title.  A \"Diplom\" in Germany is roughly equivalent to a Masters degree in English speaking countries.  Andreas is a postgrad student.  I hope that you really don't think that this dot story is the chief product of the research he's been doing -- it's a summary.  There's about 50 more pages of results on his site and I would presume more to come.  He's shown up at the major KDE events for the last two years in a couple of countries.\n\nSecond the guy there who it mentioned was working on a paper for his diplom is linked in the story.  It's a reasonably technical 100 page paper (and it's actually in English).\n\nAnd finally, if you wish you were in sociology, why aren't you?  Oh, right -- that was your very clever way to fling an insult.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: I wish I was in sociology"
    date: 2003-10-03
    body: "> And finally, if you wish you were in sociology, why aren't you?\n> Oh, right -- that was your very clever way to fling an insult. ;-)\n\nActually it wasn't, but it was an exageration. I've come to learn that it really can be useful. In this case these reports are useful because they are a sort of management report for an organisation without professional management. In a (properly working) commercial organisation this sort of evaluation can take place quite frequently.\n\nAn added difference is that in OSS the direction of the development is much more of a group process than it is in a commercial company. So it is important that everyone understands how the organisation works."
    author: "Anonymous Yellowbeard"
  - subject: "German translation of Andreas' report"
    date: 2003-10-05
    body: "Hi,\n\na summary of Andreas' report/thesis is available in German at the German KDE website, see http://www.kde.de/news/Paper_KDE_Andreas_Brand.pdf\n\nBest regards,\n\nKlaus"
    author: "Klaus Staerk"
  - subject: "Thanks to the responses"
    date: 2003-10-09
    body: "Hi, \nbig thanks to the translators. I had no time to translate it myself, so if there are any misconceptions, feel free to contact me. \nIn the moment I am working on papers for the scientific community. But there is a big case study which waits to be finished ;-) \nBest wishes\nAndreas Brand"
    author: "Andreas Brand"
---
While the KDE project continues to research and develop the ideal desktop environment, now the KDE community and development processes themselves have been studied and researched by at least two different efforts: <a href="mailto:christian.reinhardt@telfs.com">Christian Reinhardt</a> of <a href="http://www.uibk.ac.at/">University of Innsbruck</a> chose to study KDE for his &quot;<a  href="http://kde.klevahhehd.com/">Collaborative Knowledge Creation in Virtual Communities of Practice</a>&quot; Master's thesis. Christian would be very grateful for feedback in his <a href="http://kde.klevahhehd.com/forum/index.php">discussion forum</a>. Meanwhile, Dipl. Soz. <a href="mailto:A.Brand@em.uni-frankfurt.de">Andreas Brand</a> of <a href="http://www.soz.uni-frankfurt.de/arbeitslehre/">Johann W. v. Goethe University</a> also chose to study KDE in &quot;<a href="http://www.soz.uni-frankfurt.de/arbeitslehre/brand_veroeffentlichungen.htm">The structure, entrance, production, motivation and control in an Open Source project</a>&quot; which he has already presented at this year's <a href="http://www.linuxtag.org/">LinuxTag</a> as well as at the <a href="http://events.kde.org/info/kastle/">KDE Conference</a> (<a href="http://ktown.kde.org/~danimo/events.kde.org/info/kastle/presentations/Presentation_Andreas_Brand_KDE_Nove_Hrady_24-09-2003.pdf">English slides</a> <a href="http://events.kde.org/info/kastle/conference_program.phtml">available</a>). You can read an English summary of his German paper below as translated by the <a href="http://i18n.kde.org/teams/index.php?action=info&team=de">KDE i18n German team</a>.
<!--break-->
<p><hr>
<h2>The structure, entrance, production, motivation and control in an Open Source project</h2>

<p><b>Author:</b> Dipl. Soz. Andreas Brand<br>
Project electronic labourmarkets (PELM) / Projekt elektronische Arbeitsmärkte<br>
Johann W. v. Goethe University / Johann W. v. Goethe Universität<br>
Institute for polytechnic and laboursciences / Institut für Polytechnik und  Arbeitslehre<br>
<b>Email:</b> A.Brand@em.uni-frankfurt.de<br>
<b>Translators:</b> The KDE i18n German Team<br>
<b>Blame:</b> The Dot, for edits
</p>

<h2>Structure</h2>
<h3>Structure of KDE Project</h3>
<p>This Open Source large-scale project is a world wide distributed group of about 800-1000 people (about 800 cvs-accounts, not even counting most translators). Nearly all of the contributors come from central Europe, mainly Germany, France, Great Britain, the Benelux and a few from USA/Canada.</p>
<p>
There are also regional clusters, for example at Nürnberg/Erlangen. This group is really homogeneous. Most of these people are male and between 20 and 30 years old.</p>
<p>A big part of the project members are students, another one employed persons. The greatest number are studying or have studied in the IT sector. Students, trainees, unemployed persons and employed persons from other fields are a minority. There are also a number of scientists and project contributors who are self-employed and offer services around the Open Source Project.</p>

<p>They are mainly working voluntarily, as a hobby, on the common task of creating a free desktop environment. There are different fields of activity in the Open Source community, with the Software developers and the documentation maintainers/translators as the most important ones. The Software developers represent two thirds of the task force, the documentation maintainers/translators about one third and other activities, such as homepage service or graphics/sound creation, only about five percent.</p>

<p>The most important tools are the mailing lists for communication and the source file management systems for the production. The Open Source project consists on the one hand of a few important and central subprojects, on the other hand of a lot of small single-person subprojects.</p>

<h3>Environment and Context of KDE Project</h3>
<p>The environment of the large-scale project consists of other Open Source projects, corporations and end-users. There are conflicts between the environment of the large-scale project and the large-scale project itself. Conflicts between different Open Source projects are rare, but if they appear, they are about the normative basics of the general Open Source community such as the correct handling of licenses. The good relations to other Open Source projects are improved by the use of tools of these projects in the large-scale project.</p>

<p>Many more norm conflicts about the use of the licenses of the large-scale project exist between the large-scale project and corporations where the conflict topics are about taking over, changing and selling parts of the source code, and the reputation through brand endorsements.</p>

<h2>Functionality</h2>

<h3>Joining the Project</h3>
<p>Many people get to know the project after using a Linux distribution, talking with friends and reading articles in computer magazines. At the beginning of the project, many were recruited from mailing lists and Usenet forums. The project exists since 1996 but most the people have joined between 1999 and 2002.</p>

<p>The entry into this community follows after showing interest in helping by sending little source code changes. After multiple contributions the novices get the rights to write to the source file management system. This happens very fast, mostly between 6 to 12 months.</p>

<p>The entry has to be seen as a self-selecting process, in which the self-motivation plays a main role. After the entry there is the possibility to climb up into the inner circle by reputation. The inner circle is a distinguishable group of persons, who have worked well on the project and who have gained reputation and hence social status. The reputation here consists of different elements like experience, continuous performance, friendly and cooperative behavior and visibility. Voluntary exits are caused by acquiring other tasks and responsibilities, such as family issues or a new job, while forced exits happen by violation of important standards, such as deletion of code without agreement or consensus.</p>

<h3>Production, work-distribution and work-consolidation</h3>
<p>There is an operative layer and a strategic decision layer, where the operative is about the actual software creation and the strategic is about legal problems and visions.</p>

<p>Product-creation is the center of the Open Source project. The software development normally follows a process of trial and error where the one, who does the work, decides about it. Usually programming occurs instantly without initial drafts or other tools, although sometimes a possible solution is thought out before while doing sports. Only a minority develops a formal model that is programmed later. Hence, product-creation and work-distribution are about the same.</p>

<p>Working-time is very heterogeneous and variable, between a few minutes to 14 hours a day, i.e. about 0.5 and 90 hours a week. An average of 2 to 3 hours a day is spent for the project. Nearly all the work is done in free time if you don't count those people who are employed by project-friendly companies, such as distributors, to work on the project. Therefore, problems may arise between the participant and his/her family due to the time-consuming nature of the hobby.</p>

<p>Most contributors stated that they work on 1-4 projects simultaneously. The average is 2 projects. The average number of persons in a subproject is 3-4 persons. If you take the maximums, there are 6-8 persons. Sporadically, there are subprojects with more than 10-20 persons. The persons from the inner circle stated that they work on projects with more contributors than the average. Also persons from the inner circle participate in more subprojects than other project members.</p>

<p>Work-distribution is realized as a voluntary takeover of work, as overseen by a project coordinator and  his or her assistants.  During the work-distribution, arrangements are made between the subproject coordinators where trust in the adherence of the arrangements plays a very important role in the Open Source project. The project coordinator is responsible for the quality of his/her own duties.</p>

<p>Decisions about the source code or about the future development of the project are made in consensus. Normally project members have a say in the subproject. But there is a tendency that the project coordinator and persons from the inner circle have a bigger influence than persons who only participate.</p>

<p>The project coordinator has no extra power. Only project contributors from the inner circle enjoy special influence and confidence, because of their reputation. If there are conflicts, mediation is expected. The common work comes together in a release. There is an official release which is coordinated in a release plan. Responsible for this plan and for the assumption of stable programs is the release coordinator, who takes up this special, coordinating position for the inner circle.</p>

<h3>Motivation/Gratification</h3>
<p>We distinguish monetary and non-monetary based motivations. The monetary reasons exist because persons are employed for the work on the Open Source project (comparable to a sponsoring of the project). On the other hand some members get giveaways and are lent state of the art hardware, which resembles the sponsoring of individuals. Some stated that they temporarily got monetary bonuses for certain tasks from project-friendly companies. Some get money for tasks based around the project, such as speeches or articles in magazines.</p>

<p>However, non-monetary based motivation reasons prevail in this project. The contributors' motivation consists of a mix of internal and external elements. Internal motivations are for example the fun of solving interesting problems or the fun of programming. A great majority denoted these reasons as important. External motivation is, among others, oriented to the community and its cooperative culture. For this, you have to consider social recognition (reputation) and mutual help. These motivation reasons were also seen as important by a majority. In this situation conflicts can on the one hand lower the motivation through insults, but they can also raise the motivation through ambition.  Better job opportunities in the future are also a motivation reason.</p>

<p>Personal meetings represent an important motivation source. Persons, who participate longer (inner circle), have met with other project contributors. Those meetings mostly take place at Linux conferences, and more rarely at project-oriented programmer-meetings, such as before major releases or else they take place because of local proximity.</p>

<p>The fact that the project can be considered a hobby, a time-consuming one, can also be seen as a motivation reason. There is a fraction of participants who are generally interested in technique. Two camps can be seen: the first one uses programming to relax, and in the second one, the persons cannot subsequently stand the sight of a computer. Other hobbies of the participants varied widely: reading science fiction or comic books and sports are very popular.</p>

<h3>Control</h3>
<p>Control of the work in the Open Source project is through peer verification of the source code quality. This happens in an operative layer. Quality rises through self-checks with simultaneous use of the software, spot tests by the project coordinators and the reports of end users through a bug tracking system. Thereby a conflict exists between the self-assigned work of the project participants and the end user orientation of the community, which can often be seen in little disputes.</p>

<p>There are three channels for disturbances. Firstly there are conflicts during the communication, i.e. on the mailing lists, but also in the chat system. Here you can find most of the trouble. Disturbances by project contributors are punished verbally, disturbances from outside are normally ignored. Secondly every now and then there are conflicts and problems with the use of source code by free-riders and the GPL license.  Infringing enterprises are usually punished through bad publicity. Thirdly disturbances in the product-creation through the source code management system occur infrequently. These events are punished by restoring the previous source code and possibly restricting the participant's access to the system.</p>

<p>There are several topics, which occur often in conflicts. The most frequent topic consists of conflicts that concern the future development of the large-scale project and the subprojects. The second topic of conflict, which occurs infrequently, involves: Disregarded contributions, refusal of  source code patches, deletion and altering of source code, quality of code/translation/documentation and the type of communication.  Also very rare are conflicts involving work-distribution, incorrect copyright attributions in source code, or exaggerated opinions of one own's performance and position in the project.</p>