---
title: "World's Largest Country Goes for KDE"
date:    2003-08-19
authors:
  - "swheeler"
slug:    worlds-largest-country-goes-kde
comments:
  - subject: "Nice"
    date: 2003-08-19
    body: "This should enumerate the couple of Spanish GNOME computers. :-)"
    author: "Anonymous"
  - subject: "Re: Nice"
    date: 2003-08-19
    body: "KDE has a dictionary too, you know ;)"
    author: "Lee"
  - subject: "Re: Nice"
    date: 2003-08-19
    body: "He/she says that, because of LinEx, a Debian based distribution, which uses GNOME as a basis:\n\nhttp://www.linex.org\n\nLinex is the basis OS for Extremadura, an autonomous community in Spain."
    author: "Alex"
  - subject: "Re: Nice"
    date: 2003-08-20
    body: "You can find strange people all over the world. Hey, some people never learn :-)"
    author: "uga"
  - subject: "Re: Nice"
    date: 2003-08-20
    body: "They chose free software. That shows they learned something, instead of blindly buying more MS software. :-/\n"
    author: "Random coma"
  - subject: "special tool to enter chinese characters"
    date: 2003-08-19
    body: "It seems like they are using a special tool to enter chinese characters... \nShould'nt this be used as an optional add-on for kde? Could be helpful for other asian speaking desktop users, too... (me thinks)\nWell, idnsc (i do not speak chinese) and I do not know how they're getting one of these 27.000 different characters out of a qwerty keyboard..... must be really complicated.\nAnd it would be nice to know, if that RedFlag distribution is interested in collaboration and improving \"our favorite desktop\"..."
    author: "Thomas"
  - subject: "Re: special tool to enter chinese characters"
    date: 2003-08-19
    body: "\nXIM (x input method) clients have been around for years. There are at least dozens of them for many languages.\n\nKDE *still* has some i18n issues left though, Many of these are on the QT side. Maybe this much exposure might nudge QT to do something about it."
    author: "Ken"
  - subject: "Re: special tool to enter chinese characters"
    date: 2003-08-21
    body: "Yes but XIM really sucks for inputting Chinese. I've used redflag 3.2 and they have their own front end to XIM which works better than the other free ones available; but still it is a long way behind entering Chinese on Windows."
    author: "JSC"
  - subject: "Re: special tool to enter chinese characters"
    date: 2003-08-22
    body: "Just out of curiosity: \nhow is it working? How do you enter chinese characters on windows? Do you explain it to me? (I could not find any screenshots on the web)"
    author: "Thomas"
  - subject: "Re: special tool to enter chinese characters"
    date: 2003-08-22
    body: "Its easier to do than it is to explain. I'll describe the most common method in mainland China but you first have to understand a little about the language.\n\nThe written Chinese language is entirely pictorial based (normally referred to as characters), no new news there, each syllable is represented by one character and each character has one or more meanings depending on the context. Some characters can represent more than one syllable. Whole words are either single characters or combinations of characters. \n\nThere are many dialects in Chinese and different dialects pronounce the same characters differently. But they all write the same way (there are some very small differences, for example Cantonese has lots of its own characters), for now we will also ignore the differences between the old form of chinese characters and the new form. The Chinese language is also tonal. That means that one sound such as 'ma' can be pronounced in 5 different ways. \n\nHow do you write a Chinese character on a standard qwerty keyboard? There are two approaches: pronunciation or by breaking down the character into its component parts and mapping these to key combinations.\n\nThe most common Chinese input method in mainland China is 'pinyin' which is based on romanising the sounds of the Chinese language. Using pinyin I can write an entire sentence using western characters: jiu zhe yang (just like this).\n\nWith the old simple pinyin systems you would enter these syllables one at a time and then the computer would pop open a little box and show you all the characters that have this sound. With all the different tones this can be a lot. I always dread having to look up characters for the sound 'ji' there are about 50 or so of them!\n\nSo when you have identified the correct character you select it (either with your mouse, or by pressing a number which appears above each option) and it is transferred to the document yu are writing in.\n\nModern systems are more intelligent and basically work in a similar way to writing on a mobile phone. As you type pinyin the computer tries to make sense of what you are writing and select the correct characters. As you write you can correct its wrong choices.\n\nThe microsoft Chinese input system is very good at getting out of your way and letting you just type - it is very good at guessing what you mean.\n\nI switched to Linux on the desktop a few years ago and I have to admit there are times now when I have to write in Chinese and rather than send an e-mail and suffer the paignful Chinese input system I write a fax by hand.\n\nI hope that gives you a rough idea of how it works."
    author: "JSC"
  - subject: "Re: special tool to enter chinese characters"
    date: 2003-08-22
    body: "There is a mistake in my earlier post.  When I said:\n\n\"Some characters can represent more than one syllable\"\n\nI meant that some characters can represent more than one sound.\n\nSometimes Chinese characters have to be thought of as representing concepts rather than specific words.\n\n"
    author: "JSC"
  - subject: "Re: special tool to enter chinese characters"
    date: 2003-08-22
    body: "Thanks for your detailed answer!"
    author: "Thomas"
  - subject: "Re: special tool to enter chinese characters"
    date: 2006-09-16
    body: "Right... so what's the best way to input chinese chars in KDE?  I want to write to friends in china and right now am writing pinyin...."
    author: "James"
  - subject: "The most popular..."
    date: 2003-08-19
    body: "This has a very interesting and entertaining aspect. Of course I'm very happy about this and I believe it will be a benefit for the people of China and for KDE. However it kind of tweaks me and it would be cool to know their numbers. Certainly China is by far the most populous nation on earth. Anyway when I tell people about open source it's fun to tell them I lead one of the most popular apps in open source and I think it's safe to say the most popular OSS web development tool on Linux... Now it appears that it's possible it could become the most popular web development tool in the world without me even knowing if it is. That's a bit of a mind bender... but really fun! ;-)\n\nSo now we can all say we use what could be the most popular desktop in the world (on some day that we don't know when). Perhaps it would be interesting to get some indication of hardware sales in China and extrapolate the estimated Red Flag installs? It would be entertaining to think of M$ as a US centric niche desktop. ;-) Even better would be to realize proportional contributions from China and India on development efforts. That could produce really exciting results!"
    author: "Eric Laffoon"
  - subject: "Re: The most popular..."
    date: 2003-08-20
    body: "> Now it appears that it's possible it could become the most popular web development tool in the world without me even knowing if it is..\n\nPerhaps in ten years.. but right now, the combined computer usage of North America and Europe far outweighs any other continents combined. There is a great deal of development going on in places like China and India, but it's still, for the most part, hasn't reached the main part of the population."
    author: "fault"
  - subject: "Re: The most popular..."
    date: 2003-08-21
    body: "Sorry to break it to you, but according to distrowatch, RedFlag doesn't come with quanta.  It's only a 1 CD distro, so they obviously have to make some cuts."
    author: "Damien Byrne"
  - subject: "Re: The most popular..."
    date: 2003-08-24
    body: "Don't think it matter to much if it's bundled or not. Quanta isn't a tool for the \"regular guy\" anyway. It's not like every guy out there develop web-pages. But for those who do, Quanta is easy to grab/install, and would probably be the program of choice for KDE-people.\n\nAnd a little note to you Eric: Keep on the good work, I'm sure Quanta will become the most popular webdevelopment-package in China although it doesn't come bundled. I'm from Norway, and have used Quanta for many years now, and I find it to be the most excellent tool I've ever seen for web-development. No matter operation-system."
    author: "Helge \u00d8yvind Hoel"
  - subject: "China?"
    date: 2003-08-19
    body: "To me its a little scarey about China picking it up. Ever notice how close of an ideology Open Source is to Communism?"
    author: "Anonymous"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "Which just goes to show that when we get to the point where you can duplicate physical goods including skyscrapers, bridges, etc. as easily as you can duplicate software communism will finally be practical :)."
    author: "Ideology"
  - subject: "Re: China?"
    date: 2003-08-20
    body: "I just want to point out that China has not been truly communist for more than a decade.  It has been embracing captialism very rapidly, which makes it *fascist*."
    author: "Chris Bordeman"
  - subject: "Re: China?"
    date: 2003-08-20
    body: "Facism and communism are like fire and water. Communism + Capitalism != Facism!\n\nCommunism is both an economic and governing type. Capitalism is only an economic type. Fascism, in many respects, is an ideology of negativism: anti-liberal, *anti-Communist*, anti-democratic, anti-egalitarian, etc. As a political and economic system in Italy, it combined elements of corporatism, totalitarianism, nationalism, and anti-communism.\nIn other words: a communist country cannot turn into a facist country simply by adding an element (capitalism). It needs a revolution and complete flip-over to turn into a facist country.\n"
    author: "Ac"
  - subject: "Re: China?"
    date: 2003-08-20
    body: "???  Communism is simply centralized control on both the political and economic sides, no matter what the underlying ideals are.  Fascism is totalitarian on mostly just the political side.  When a communist state abandons Socialist ideals and decentralizes on the economic side, they are moving toward fascism.  It is absurd to say a communist country *cannot* evolve into a fascist one."
    author: "Chris Bordeman"
  - subject: "Re: China?"
    date: 2003-08-21
    body: "Your definition of Communism is total nonsense.\n\nNot only the fact of centralization is important but most important the underlying idea! Facism is the last state of Capitalism where in both ones 'big profit' is the underlying idea. Communism is an ideal society of Socialism where 'all equal, all free, no exploitation' is the main idea. You see, both ones are the opposite to each other.\n\nP.S.: I hope the manpower of China will reflect itself in contributions/patches/fixes to the KDE sources"
    author: "F@lk"
  - subject: "Re: China?"
    date: 2003-08-21
    body: "Your definition of fascism as only a *right wing* phenomenom is incorrect, and seems designed to hide the natural evolution of Marxism and Socialism to a form of Fascism as is now occuring in China.\n\nFor a quick explanation why, please read this:\n\nhttp://www.mail-archive.com/mutti-l@taklamakan.org/msg01091.html\n\nAnd for a more thorough study:\n\nhttp://www.yale.edu/yup/books/078277.htm\n\nThere tons of articles, research papers, books, studies, etc. documenting and exploring China's move to Fascism, just do a quick google search.  This narrow \"original intentmeans as much as the end results\" response is a left-wing defense mechanism for their precious socialism and everyone should see through it."
    author: "Chris Bordeman"
  - subject: "Re: China?"
    date: 2003-08-22
    body: "I'm no expert, but I would say that China is a (primitively) socialist state, with a communist government.\n\nThe fact that the government is using some capitalist economic policies to increase the living standard and level of development of the country needn't be a bad thing, although people may well disagree as to how far the adoption of these policies will go.\n\nI also believe that for a government to be fascist, it must see complete control as the preferred means of operation.  However, it could be argued that the government in China only exercises its control enough to maintain stability in the country.  For instance, China does have non-communist political parties with which the government works closely.\n\nAddressing the original point, though, I think it is sad that some Open Source enthusiasts (who realise the ability of humans to work cooperatively rather than selfishly) feel the need to post uninformed political condemnations of other countries which share these ideals."
    author: "NoNeed"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "The Open Source ideology is: develop in the most efficient, bug-free manner, which happens to be by opening the source code to encourage more eyes to look at the code and develop it.\n\nYou're thinking of the Free Software ideology...\n\nAnywho, Free Software emphasises the important of the freedom of the individual user, and by extension the \"society\" of users, over the individual programmer(s) who write the code. As RMS recently commented, proprietary software tends to promote division and control. Marx wrote about similar themes, so in a sense they're close, even though what you see in China is about as far from Marxism as any \"communist\" state has managed."
    author: "Tom"
  - subject: "Re: China?"
    date: 2003-08-22
    body: "While it must be convenient to pigeon-hole communism as being a system which goes against freedom, that it is not really what communism is about.  If I may paraphrase the Communist Manifesto:\n\n'The only freedom communism deprives people of is the power to subjugate the labor of others'.\n\nTo put that in practical terms, under communism people would not be able to make money from owning something.  Under capitalism, someone can own a factory and pay the workers there a fraction of the profits.  Thus the owner makes money not from working, but by subjugating the labour of others, i.e. simply by owning something.\n\nThis is similar to the principle behind closed source code.  Someone owns a software patent, or a proprietary but widespread file type, or a near monopoly on desktop OSes, and then they can make money not from producing good products or services, but by abusing their position of control.\n\nOf course, you may be talking about freedom in the sense of civil rights, but I think the universal truth is that all governments will infringe the rights of their citizens if the alternative is the country breaking down.  Look at how the American government has put forward the Patriot Act and uses Guantanemo Bay to protect itself from terrorists.  Previously America used similar laws and strategies to oppose the rise of communism in its own country and abroad, just as countries with communist governments have taken harsh steps to stop the rise of capitalism.\n\nAs for China, I would not call it a communist state, as it is closer to a socialist state with a communist party in government.  As for its distance from the Marxist ideal for a state, I don't think anyone disagrees that it has a long way to go.  I would say, though, that the government is applying Marxist theory in analysing the current state of the country and realising that the level of development is not yet right for the introduction of further socialist economic elements.\n\nIn summary, the political equivalent of the type of freedom in Free Software is the freedom offered by a system where the workers own the means of production."
    author: "NoNeed"
  - subject: "Re: China?"
    date: 2003-08-24
    body: ">'The only freedom communism deprives people \n> of is the power to subjugate the labor of others'.\n\nJust goes to show you how dangerous left-wing idealism really is."
    author: "Chris Bordeman"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "Hey, there's nothing about capitalism or free enterprise that keeps good-hearted individuals, or even governments, from producing free software.  If you're free to choose, you're free to choose not to charge.  Anyway, congratulations KDE team for spreading your good work to China!"
    author: "MH Dixon"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "how is it scary, exactly?"
    author: "Aaron J. Seigo"
  - subject: "Re: China?"
    date: 2003-08-26
    body: "Its scary that the Open Source phenomenon, the source code, is owned by the community. Everything under Communism is owned by the community. According to Marx, people are paid what they need. Then again I hear everyone has a job. Although splitting money equally amoung all classes is a horendous idea. I'm sure the lower class would love it, although people still do carry on different rankings in society.\n\nPutting that into terms of this software initiative, there are people that will never spend their time dedicated working on an Open Source project. How does that help repay those few that do spend their time? People are doing very technical work with a low payoff? Only the Capitalism involved gives monies to the Open Source workers. Competition too is a capitalist attribute, being Open Source or X versus Microsoft. Think of this way, under Communism how many different types of competing government produced toliet paper will you find? How about just one, why compete, that's a capitalist idea!"
    author: "Anonymous"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "There's nothing fundamentally wrong with communism.  It's just a little tough to implement correctly.  The same could be said for capitalism; there are many flaws in its implementation, too.  Yet, if OSS shares the benefits of either of those systems, yet continues to work the way it does, then it's a good thing.  Certainly not something to be afraid of :)"
    author: "Lee"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "> There's nothing fundamentally wrong with communism. It's just a little tough to implement correctly.\n\nYou mean \"theoretically\" wrong with it. In theory it sounds wonderful. It is worth noting that by definition governments can't implement it because they always revert to socialism with a ruling class. I studied communal societies as a young man because of the idealisms. One could argue it had some degree of success in the first century Christian church. It failed the pilgrims and in a book on communes in America over several hundred years I was stunned to find that every single one of them failed. In fact it is safe to say that it is nearly impossible to implement because it restricts any reward incentive, is antithetical to individualism and tends to have an adverse impact on personal responsibility.\n\n> The same could be said for capitalism; there are many flaws in its implementation, too.\n\nOne could argue it's chief flaw is it's inequitable distribution. Certainly the failing of either ideal centers on their practice by greedy and immoral people. Monopolies, government corruption and exactly where to balance the socialist elements of the common good create problems, but of the forms mentioned only capitalism has demonstrable proof of long term success.\n\n> Yet, if OSS shares the benefits of either of those systems, yet continues to work the way it does, then it's a good thing.\n\nThe communist slur on OSS is very wrong. The truth is that it is agnostic to your political views and social ideals. If you believe in the worker's utopian paradise or you think the government has no business in insurance and retirement you will still have something to like in OSS. The only ones who can object are those who wish to run IP like a lottery and work once so they can collect forever... an ideal that is not really of either camp. (Those who want to be paid for what they don't produce because they once produced are lazy gamblers.)\n\nOSS makes sense because:\n1) Financially you can get a tool for less money\n2) Since infrastructure (like highways) is both too expensive and too vital to be produced by individual entities it is managed as a public resource... Operating system and other software is already demonstrating that being entrusted to OSS groups prduces superior results.\n3) By enabling development of better software tools enterprising business types can offer powerful value added services at competative prices with nominal expenses. \n\n> Certainly not something to be afraid of :)\n\nBeing afraid of this couldn't be more obtuse. By delivering open source software to China we in fact deliver our friendship and a message of the values of free software. We are saying \"We want you to have these tools and we hope you will be enriched and they can help you make your life better.\" What could be better than that? This also means more Chinese are connected to the internet. I wish the North Koreans were all connected but they endure total information control. Do you know what they are told? Ask yourself if you think people will be more likely to march off to war with you if they only know they were told you are a murderous hoarde, or if they are using software you gave them freely to use and they have communicated with you by email.\n\nWe should all be very happy that China is moving to get people connected and to improve opportunity for their people. The only thing we should fear is ignorance, and being connected is where the cure lies."
    author: "Eric Laffoon"
  - subject: "Re: China?"
    date: 2003-08-20
    body: "> The communist slur on OSS is very wrong.\n\nEhm. I think this statement is wrong :) \n\n> The truth is that it is agnostic to your political views and social ideals.\n\nWhile I agree with this.\n\nTo make a long story as short as possible (and henceforth wrong like all simplifications of an inherently complex matter) there are two \"layers\" of communism: a social level, and an economical level; and the two are somewhat distinct, even if related. Oversimplifying, at the economical level communism basically means (usually) a sharing of the tools and of the deriving products. Again oversimplifying, at the social level communism means \"no ownership\".\n\nTranslated into software terms, FLOSS *does* result in an essentially economically communistic environment; what it doesn't do is fit the \"social\" communistic point of view, which would rather become something akin to \"no intellectual property (of software)\".\n\nIf some social structure has to be found in the FLOSS, this would rather be, IMO, an anarchic (self-aggregating, self-ruling communities) one. But that's another matter.\n"
    author: "Giuseppe Bilotta"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "China nowadays is less communist than you might think? Market driven policies are leaving a lot of people out to dry, as they can't rely on state-run companies anymore.\n\nI think this has to do more with technological independence (and security to some extent) rather than communism."
    author: "Ken"
  - subject: "Re: China?"
    date: 2003-08-19
    body: "The problem is China is a dictatorship with a more and more capitalistic system.\nIt doesn't match any good meanings of communism, but seems to be engaged to match all bad meanings.\nBut there are enough \"good\" countries not caring about humanity and individual rights, especially in the last years.\n\nYou're right, this decision is about independency and strengthening its own economy.\nSo it isn't surprising that they choose Linux and KDE. (OK, they could also use FreeBSD/NetBSD/OpenBSD, etc.)"
    author: "CE"
  - subject: "Good Point"
    date: 2003-08-20
    body: "It is just like it. Lets see\n1) All actions are dictated from top down like a CEO.\n2) If anybody trys to compete against you, \n\tthen just \"kill\" or inprision them.\n3) If anybody is succeding at making money,\n\tthen take it all away from them unless they are the right people\n\noh wait, This does not sound like OSS, but sounds like ....."
    author: "a.c."
  - subject: "Re: China? FOSS software, capitalism, free markets"
    date: 2003-08-20
    body: "Actually if you look at capitalism it is based on a free market economy(in theory)\nA free market has low barriers to entry and high demand.  In a free market economy the cost of goods and services will trend towards the marginal cost to produce.\n\nWith electronic duplication and internet distribution the marginal cost to produce one unit of software (an office suite for example) is close to zero.  So the cost of for downloading software should be close to zero.\n\nWorldwide internet distribution should be as close to free market economics as possible.  As far as barriers to entry, you can produce software with a single PC.\n\nchuck\n\n\n"
    author: "cmoss"
  - subject: "Re: China?"
    date: 2003-08-20
    body: "Have you seen Chinese economic theories since 1978? hardly communist (they never were, and neither was the soviet union)"
    author: "fault"
  - subject: "Re: China?"
    date: 2003-08-20
    body: "Interesting though, how OSS was invented inside capitalist societies..."
    author: "Chris Bordeman"
  - subject: "Re: Red, Red China? "
    date: 2003-08-20
    body: ">>To me its a little scarey about China picking it up. Ever notice how close of an ideology Open Source is to Communism?<<\nYou never lived in a communist country did you? name a few countries) - has nothing toCommunism - in the form the world has seen it (Russia, China, East Germany to  do with colaboratoring, sharing resources (unless we talk about widespread theft and corruption) and working cooperatively. \n\nCommunism is rather state monopolies - one size fits it all: You have to buy that little, clunky, smelly two-stroke lawn mower engine powered Trabant car or walk because it's the only car you can buy. \n\nIf you want something special, say Banana or Mango fruits for dinner - you get probably laughted upon by the rough shop keeper in the government owned store. (if not they go after you for provokation ;-)  )\n\nDoesn't it rather match the business practices of a big software company from the North east of the United States? (let me see: one size has to fit, it's clunky, it will explode in a spectacular fire ball in the case of the slightest accident as its Trabant counterpart, you get ridiculed if you want something special ;-)  )\n\nWell the Chinese do what they want to do and their government is their business. If many of them choose Linux and KDE then they are our friends, aren't they;-)\n\nConrad"
    author: "beccon"
  - subject: "Re: China?"
    date: 2003-08-20
    body: "Ever notice how far China is from being Free and Open?\n"
    author: "anonon"
  - subject: "Re: China?"
    date: 2003-08-23
    body: "> Ever notice how close of an ideology Open Source is to Communism?\n\nOpen source works.\nCommunism doesn't work.\n\nHow about that for close?"
    author: "Nobby Pseudonym"
  - subject: "China is not the largest country in the world ..."
    date: 2003-08-19
    body: "It is the third largest (after Russia and Canada). It does have the highest population, however."
    author: "Sinuhe"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-19
    body: "So some might say it has the largest population."
    author: "Scyber"
  - subject: "Re: China is not the largest country in the world "
    date: 2004-01-30
    body: "china is actually the 4th largest after Russia, canada, USA and after china comes brazil"
    author: "Re: China is not the largest country in the world "
  - subject: "Re: China is not the largest country in the world "
    date: 2004-01-30
    body: "China\n1 335 840.3\n+1.2%\n\n\nIndia\n1 088 056.2\n+1.9%\n\n\nUnited States of America\n 294 540.1\n+1.0%\n\n\nIndonesia\n 221 777.7\n+1.8%\n\n\nBrazil\n 183 199.6\n+1.9%\n\n\nPakistan\n 157 056.0\n+2.6%\n\n\nNigeria\n 154 491.1\n+2.6%\n\n\nRussia\n 146 743.8\n+0.5%\n\n\nBangladesh\n 141 398.2\n+1.8%"
    author: "ggggggg"
  - subject: "Re: China is not the largest country in the world "
    date: 2004-04-24
    body: "I think you would find China is indeed the 3rd largest country in the world, it is larger than the U.S, not by much i grant you but it's big enough.\n\n\n1 Russia 6.6 million mi2 (17 million km2) \n2 Canada 3.9 million mi2 (9.9 million km2) \n3 China 3.7 million mi2 (9.6 million km2) \n4 United States 3.5 million mi2 (9.1 million km2) \n5 Brazil 3.3 million mi2 (8.5 million km2) \n6 Australia 3 million mi2 (7.6 million km2) \n7 India 1.2 million mi2 (3 million km2) \n8 Argentina 1.1 million mi2 (2.7 million km2) \n9 Kazakhstan 1,050,000 mi2 (2.7 million km2) \n10 Sudan 966,000 mi2 (2.4 million km2) \n\n"
    author: "Rob"
  - subject: "Re: China is not the largest country in the world "
    date: 2004-04-24
    body: "\n\nIs this \"Russia\" figure from a *current* source? \n\nI mean, back when it used to be the USSR it was like that. But nowadays big chunks of the former area have become states in their own rights: like Mongolia, Belorussia, Ukraine....\n\nNo?\n\n"
    author: "anon coward"
  - subject: "Re: China is not the largest country in the world "
    date: 2005-08-21
    body: "Are you into CIA paraganda.\nThe stretigic error, is done by purpose NOT a single typing mistake. It is a necessary part of US comprehesive Anti-China policy. It is also the old method that US HAS BEEN USED FOR AS LONG AS PEOPLE CAN REMEMBER, but the question is asked here is \"Why Now\". Have you checked the pass factbook by CIA??? why changed  NOW( THE TIMING IS very interesting), in fact, the Answers.com(US owned) just speedy up to change its Stats on China from 3rd to 4th in last week. What a kiddish method!!!"
    author: "Garabooo"
  - subject: "Re: China is not the largest country in the world "
    date: 2008-12-04
    body: "Seems you are the one swallowing propaganda. US is the 3rd largest country in the world, this is fact."
    author: "lol"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-19
    body: "Isn't the US slightly larger than China? /look it up/ Yes, 9,629,091 sq km versus 9,596,960 for China. Although China does have more dry land. Or they did; with the Three Rivers Dam up that may no longer be true.\n\nAnyway, this reminds me of my favorite indignant Canadian story -- the National Geographic recently printed a letter from an Canadian complaining, \"A recent article wrote that Russia is the largest country in the world. Admittedly, that's true but you should have added that the second-largest isn't the USA, it's Canada.\""
    author: "Otter"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-20
    body: "I suppose it depends on the information source and the technique used to measure the land ... however mostly it is said, I think, that China is larger than the USA (9 560 980 versus 9 372 614 square kilometres). Mind you, this is actually quoted from a US atlas (Earth Mapbook: Environment Atlas, Interarts/Geosystems, Cambridge, Massachusetts, USA), so I doubt it is a matter of politics ..."
    author: "Sinuhe"
  - subject: "Re: China is not the largest country in the world "
    date: 2006-03-25
    body: "Yeah, I really bet that's a matter of politics... I mean, what's better for politics than saying that you're a huge country... honestly..."
    author: "Yeah"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2003-08-19
    body: "I was referring to population.  In this context it's a more appropriate measurement."
    author: "Scott Wheeler"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2003-08-20
    body: "i wonder how China (or any country, for that mater) ranks by number of desktop computers? or how about deskop computers per capita? hm.. might be interesting to google on that later tonight... =)"
    author: "Aaron J. Seigo"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-20
    body: "Not computer usage, but internet usage:\n\nChina is already #3.. will over take #2 (Japan) soon.. \n\nComputer posession is much less than in China because computers are shared (computer centers, ).. much fewer people own computers than use them :)\n\nhttp://www.c-i-a.com/pr1202.htm"
    author: "anon"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-20
    body: "Shouldn't the title be \"World's Most Populous Country Goes For KDE\", then? I know it doesn't really matter, since the article following elaborates on the title, however in humble opinion, 'largest' is understood as the biggest, particularly when you only see the title (eg on the kde.org website)."
    author: "Sinuhe"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-20
    body: "Err ... I'm sorry, that is not good either, because China is rather large -- countries like the Netherlands and Monaco are populous, on the other hand. How about \"World's Most Numerous Nation Goes For KDE\"?"
    author: "Sinuhe"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-20
    body: "That's discriminating against all non-Han people in China. =P\n"
    author: "Datschge"
  - subject: "Re: China is not the largest country in the world "
    date: 2003-08-20
    body: "Basically \"largest\" is fine.  A country is not just a geographical region but also a collection of persons that are \"citizens\".  In fact as has already become apparent in this thread there are many ways to measure the \"size\" of a country.\n\n\nLet's take a similar example; say that I'm referring to a school class.  Now if I were to say \"this class is larger than that class\" this meaning would vary in context.  If they were confined to a certain room I could be referring to that.  Alternatively I could compell everyone in said class to be weighed and sum these to determine the largest class.  Or, finally I could take the contextually more simple approach and assume it to mean, \"They've got more people in there.\"  ;-)"
    author: "Scott Wheeler"
  - subject: "Thank you king of the obvious"
    date: 2003-08-24
    body: "Brilliant, noone probably realized that before you said it.  Thanks so much!"
    author: "Chris Bordeman"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2003-10-30
    body: "Actually, India has the highest population now"
    author: "Pikman"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-01-08
    body: "uh, nope. China's pop=India's pop+USA's pop (300 mill)"
    author: "anon"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2005-01-22
    body: "You people are so sad...lol"
    author: "The Forum Jumper ;-)"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2005-09-26
    body: "you need help...lol ^__^\n"
    author: "lol"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2003-10-23
    body: "Canada is the largest in terms of land mass.  Used to be Soviet Union."
    author: "Shane"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-01-08
    body: "Size of countries by area:\n\n#1 Russia\n#2 Canada\n#3 USA\n#4 China\n\nSize of countries by Population:\n\n#1 China \n#2 USA\n\nand I think\n#3 Russia\n#4 Canada\n\n\n\n#1 Russia:\ntotal: 17,075,200 sq km \nwater: 79,400 sq km \nland: 16,995,800 sq km  \nArea - comparative:   \napproximately 1.8 times the size of the US  \n\nPopulation:    \n144,526,278 (July 2003 est.)  \n\n~~~~~~~~~~~~~~~~~~~~~~\n#2 Canada:\ntotal: 9,984,670 sq km \nland: 9,093,507 sq km \nwater: 891,163 sq km  \nArea - comparative:   \nsomewhat larger than the US  \n\nPopulation:    \n32,207,113 (July 2003 est.) \n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n#3 USA: \ntotal: 9,629,091 sq km \nland: 9,158,960 sq km \nwater: 470,131 sq km \nnote: includes only the 50 states and District of Columbia  \nArea - comparative:   \nabout half the size of Russia; about three-tenths the size of Africa; about \nhalf the size of South America (or slightly larger than Brazil); slightly \nlarger than China; about two and a half times the size of Western Europe  \n\nPopulation:    \n290,342,554 (July 2003 est.)  \n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n#4 China \ntotal: 9,596,960 sq km \nland: 9,326,410 sq km \nwater: 270,550 sq km  \nArea - comparative:   \nslightly smaller than the US  \n\nPopulation:    \n1,286,975,468 (July 2003 est.)\n\nThe info above is copied from: \nhttp://www.cia.gov/cia/publications/factbook/geos/rs.html  \n"
    author: "Katerina"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-01-08
    body: "> Size of countries by Population:\n> \n> #1 China \n> #2 USA\n> \n> and I think\n> #3 Russia\n> #4 Canada\n> \n#2 India 1,049,700,118 (July 2003 est.)"
    author: "138"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-02-17
    body: "Canada is  not #4 "
    author: "Canadian"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-03-16
    body: "Us landmass is larger than China is when only ALASKA landmass is included. Otherwise China's mainland is broader than US'."
    author: "test"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-03-16
    body: "If you consider China's autonomious regions (like Tibet, Inner Mongolia, Xinjiang, etc..), the US, even without Alaska, is more. "
    author: "anon"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2005-08-21
    body: "NO, That is wrong. plus Taiwan China is larger than USA. The factbook's sudden change is reflecting changing US policy towards China from Cliton eara. The Aim is clear, just to say China only have 26 long range Nuclear weapon hahahaha, if you believe that than have a cuppa. The chilling fact is that China has possessed at least 2000-2500 Long range warheads."
    author: "Garabooo"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2005-10-15
    body: "the usa is the most powerful well respected well fed.....organized wealthy...must I go on??? or will you get all jealous again???????LMAO\n\nDont hate us because you may work for us someday!\n\nTake Care...\nps go get your bird flu shot......or maybe a sars shot....since you guys seem to catch anything you create......\n\nMuch love from the USA"
    author: "Big Bad USA"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-07-29
    body: "Wrong, China IS 3rd biggest in area, cos it's a good bit bigger than Brazil and Brazil is actually bigger than the USA if you exclude Alaska so China is does come 3rd very closely followed by the US"
    author: "Harry"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2004-07-29
    body: "Wrong, China IS 3rd biggest in area, cos it's a good bit bigger than Brazil and Brazil is actually bigger than the USA if you exclude Alaska so China is does come 3rd very closely followed by the US"
    author: "Harry"
  - subject: "Re: China is not the largest country in the world "
    date: 2005-01-14
    body: "This should settle it once and for all\n\nALL INFO SOURCED FROM THE CIA WORLD FACTBOOK OF THE US GOVERNMENT\n___________________________________________\nLARGEST COUNTRIES BY LAND AREA ONLY (NO WATER!)\n\n1) RUSSIAN FEDERATION \n16,995,800 sq km\n\n2) PEOPLE'S REPUBLIC OF CHINA\n9,326,410 sq km\n\n3) UNITED STATES OF AMERICA\n9,161,923 sq km\n\n4) CANADA\nland: 9,093,507 sq km\n____________________________________________\nLargest Countries by TOTAL AREA (WATER+LAND)\n\n1) RUSSIAN FEDERATION\n\ntotal: 17,075,200 sq km\nland: 16,995,800 sq km\nwater: 79,400 sq km\n\n2) CANADA\n\ntotal: 9,984,670 sq km\nland: 9,093,507 sq km\nwater: 891,163 sq km\n\n3) UNITED STATES OF AMERICA\n\ntotal: 9,631,418 sq km\nland: 9,161,923 sq km\nwater: 469,495 sq km\nnote: includes only the 50 states and District of Columbia\n(THIS INCLUDES ALASKA AND HAWAII)\n\n4) PEOPLE'S REPUBLIC OF CHINA\n\ntotal: 9,596,960 sq km\nland: 9,326,410 sq km\nwater: 270,550 sq km\n_________________________________________\nLARGEST COUNTRIES BY POPULATION:\n\n1)CHINA    1,298,847,624 (July 2004 est.)\n2)INDIA    1,065,070,607 (July 2004 est.)\n3)USA        293,027,571 (July 2004 est.)\n4)INDONESIA  238,452,952 (July 2004 est.)\n\nMISC:\nRUSSIA       143,782,338 (July 2004 est.)\nCANADA        32,507,874 (July 2004 est.)\n_________________________________________\n\nSo you can see the idea that Canada is bigger than China and the US is based solely on the fact that it claims sovereignty over more WATER than land! The USA Is only bigger than China if you include Territorial Seas. (having more coastline means more territorial seas can be claimed)"
    author: "Fred"
  - subject: "China is not the largest country in the world "
    date: 2005-05-15
    body: "Some people in this thread have a very fanciful idea of the population of some countries.\n\nThe facts (from mid-2003) are:\nCountries with > 50million people\n   1. China - 1,294,629,555\n   2. India - 1,065,070,607\n   3. United States - 293,027,571\n   4. Indonesia - 238,452,952\n   5. Brazil - 184,101,109\n   6. Pakistan - 153,705,278\n   7. Russia - 144,112,353\n   8. Bangladesh - 141,340,476\n   9. Nigeria - 137,253,133\n  10. Japan - 127,333,002\n  11. Mexico - 104,959,594\n  12. Philippines - 86,241,697\n  13. Vietnam - 82,689,518\n  14. Germany - 82,424,609\n  15. Egypt - 76,117,421\n  16. Iran - 69,018,924\n  17. Turkey - 68,893,918\n  18. Ethiopia - 67,851,281\n  19. Thailand - 64,865,523\n  20. France - 60,424,213\n  21. United Kingdom - 60,270,708\n  22. Dem Rep of Congo - 58,317,930\n  23. Italy - 58,057,477 "
    author: "marshall"
  - subject: "Re: China is not the largest country in the world "
    date: 2005-06-20
    body: "The debate lies within the Great Lakes.  Do you calculate the USA's portion of the Great Lakes in the total square mileage - or do you leave it out since the lakes are shared with Canada?\n\nIf the USA owned the whole of the Great Lakes (the land all the way around them) - there would be no debate.\n\nIf you calculate that the land underneath the U.S. portion of the Great Lakes is as much a part of America as Arkansas - the U.S. is larger than China."
    author: "Dung Heap"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2005-11-10
    body: "China(we say our country \"Zhongguo\", i.e. \"Central Country\",  our Zhongguo had been once the largest country for about 2000 years, but not after Russia grew up and ate us part by part , damned Russia, I hate it"
    author: "Zhongguoren"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2006-01-07
    body: "@Dung Heap:\n\nIn fact, you have to count either land mass or total area(incl. water). I prefer the land mass.\nBy land mass, the US is ranked 3rd.\n\nRussia: 16.995.800 sq. km\nChina: 9.326.410 sq. km\nUS: 9.169.700 sq. km(All territories of the US)\nCanada: 9.093.000 sq. km"
    author: "anonymous"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2006-01-15
    body: "I love Chinese, but they exhibit a level of self-deluding, pointless nationalism that I had never come across before, myself being a member of a liberal \"see things from the other person's point of view\" democracy.  I've lived with a Chinese lady for more than ten years, and am heartily sick of listening to arguments like this, or (for example) that \"it was okay for China to invade Tibet because it's part of China really and the Tibetan monks are all paedophiles\".  She reallt thinks this, and my girlfriend isn't even a mainlander!  She grew up in Taiwan and venomously hates the Beijing Communist regime!  I only found this thread during an argument about whether or not China was the largest country in the world.  She insists that it is, but other countries changed the maps.  As if having political control of more desert than other countries is a matter of national prestige!  Chinese are great fun though; visit the country is you have the opportunity."
    author: "Robin"
  - subject: "Re: China is not the largest country in the world "
    date: 2006-04-03
    body: "What a stupid question, why the hell are you arguing?\n\nIt's a fact that China has the largest population, but not the largest land mass, why discount parts of some countries so that the other country becomes bigger.. you don't just not add Alaska to the USA and say \"China is bigger\", Alaska is a fucking part of the USA, as much as Tasmania is a part of Australia. \n\nso stfu and stop arguing on pointless questions in which the inferior argument is already determined by facts\n\nand what Robin said, Chinese are just self-delusive. "
    author: "Vish"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2006-05-22
    body: "So which one is it then???????"
    author: "Jaz"
  - subject: "Re: China is not the largest country in the world "
    date: 2006-06-19
    body: "OK...I think I'll have to settle this argument once and for all.\n\nThe Largest Countries in terms of land mass (sq km)\n1\tRussia\t        17,075,400 (doesn't include ex Soviet satelite states)\n2\tCanada\t         9,976,140\n3\tUnited States\t 9,629,091\n4\tChina\t         9,596,960\n5\tBrazil\t         8,511,965\n6\tAustralia\t 7,686,850\n7\tIndia\t         3,287,590\n8\tArgentina\t 2,776,890\n9\tKazakhstan\t 2,717,306\n10\tSudan\t         2,505,810\n\nThe Largest Countries in terms of population\n1\tChina\t         1,298,847,624\n2\tIndia\t         1,065,070,607\n3\tUnited States\t   293,027,571\n4\tIndonesia\t   238,452,952\n5\tBrazil\t           184,101,109\n6\tPakistan\t   159,196,336\n7\tRussia\t           143,974,059\n8\tBangladesh\t   141,340,476\n9\tJapan\t           127,333,002\n10\tNigeria\t           125,750,356\n\nIndia will overtake China in population at the current growth by 2025. China 1 child policy has to change because of its plan to be an economic giant so they might be still the biggest countries in terms of population. That remains to be seen.\n\nChina also controls Tibet (1,228,400 Km\u00b2). If you deduct that total, then China is the 5th largest country. Tibet is rightfully an independent country. It was invaded by China and the world did nothing, not even the mighty US of A. The way I see it, the US are bullies. They pick and choose who they fight with. Their biggest victory was in Grenada. They were whipped convincingly by Vietnam and now are getting screwed by Iraq. \"In Bush They Trust\". \n\n"
    author: "Dino"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2006-08-22
    body: "\"The way I see it, the US are bullies. They pick and choose who they fight with. Their biggest victory was in Grenada.\"\n\nOh yeah, were bullies all right. Were the goddamned world's policeman, that's what we really are.\n\nWhen a fight is too tough or requires a long-term commitment, the Europeans (save Britain) run away from it. We are the only country in the world that has the intestinal fortitude to see it through these days. Everyone else has become too comfortable and spoiled to deal with it. You know what? It is much MORE DIFFICULT to fight a war that needs to be fought than to CRITICIZE IT! \n\nWhile our soldiers are getting their hands dirty and are being shot at every day, the comfortable in Europe and our country can only criticize. I am glad I live in a country where it is possible to criticize those who are responsible for giving you freedom to do so!\n\nVietnam was an abberation on the American Military's history. THIS WAR COULD HAVE BEEN WON if we (the citizens) would not have listened to the goddamned biased media! The TET OFFENSIVE was a strategic loss for the Vietcong, the American media turned it into a propaganda victory for the enemy. TRAITORS!\n\nThink about what you consume from the mass-media and look at the bigger issues. If you don't think the media has their own agenda, think again. The Media really believes they are the 4th branch of government in this country and has thought that for some time. The real problem is that they haven't been granted this power by our constitution, they have only been given the power to say what they want, they haven't been given the power to make people believe what they want, but they think that they do. They have gotten to the point of cockiness, though. They really believe you are stupid.\n\nI don't think you are stupid. Hear me out.\n\nBy the way, our greatest victory was over the Kingdom of Great Britain in the Revolutionary war, without that victory we would not have a NATION, our 2nd greatest victory was over Imperial Japan, without that victory, we would all be speaking Japanese or German and eating Sushi or Kraut.\n\nThanks!\n\n"
    author: "Del"
  - subject: "Re: China is not the largest country in the world "
    date: 2007-04-25
    body: "Actually, you would have eventually had your independence from Britain.  Canada got it without fighting.  Maybe all you had to do was to ask nicely. :)\n\nI don't agree with the war in Iraq, but I do tend to agree with what Del says about the Europeans, i.e the other NATO members (excluding the UK).  Canada got into this argument with them, specifically Germany when our guys were losing men in heavy fighting in Afghanistan and their troops were hanging out in the safe zones.\n\nRegards."
    author: "Kevo"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2007-03-19
    body: "ewll actually india is bigger then china if you are compairing populations thought u should know that"
    author: "crazyguy"
  - subject: "Re: China is not the largest country in the world ..."
    date: 2007-09-21
    body: "The United States is bigger than China because of the Great Lakes.  There's a lot of land under that water - and the US owns half of it, (and all of Lake Michigan)."
    author: "Darren"
  - subject: "China and Open Source? Ha!"
    date: 2003-08-20
    body: "China will never follow the GPL!  If you then try to go to court they will fart in  your general direction!  Free Software in a non free country?  maybe it could have some influence, but I don't believe so.  Have a look at\nhttp://tinyurl.com/klq8"
    author: "anymous"
  - subject: "Re: China and Open Source? Ha!"
    date: 2003-08-20
    body: "I did try to raise the issue of China being a notorious abuser of human rights, but my messages got deleted every time I posted about it. This is despite the dot's own policy of not moderating general messages (according to the front page).\n\nI also joked that maybe it was time for KFirewallBuster and KHumanRights. Obviously they didnt' appreciate the joke ;-)\n"
    author: "Tom"
  - subject: "Re: China and Open Source? Ha!"
    date: 2003-08-20
    body: "I guess it's just offtopic. Should we start a thread with the long AI list of human rights violations each time a US-related article appears (I only need to say 'guatanamo bay')? It's not fair to reduce China and especially the Chinese people to the bad actions of their government...\n\n"
    author: "Anonymous"
  - subject: "Re: China and Open Source? Ha!"
    date: 2003-08-20
    body: "Well my original message was pointing out how bad the Chinese government actually is in many respects, to temper people's praise. I think it's important the we don't just start talking about the Chinese government as being incredibly cool, just because they use some FS, especially given their firewall. They obviously don't subscribe to the principles of Free Software - they just want workable solutions that don't tie them to foreign companies.\n"
    author: "Tom"
  - subject: "Re: China and Open Source? Ha!"
    date: 2003-08-20
    body: "Your original message was a meaningless flame.\n"
    author: "anonymous"
  - subject: "Re: China and Open Source? Ha!"
    date: 2003-08-24
    body: "That's EXACTLY right, their only reason to do this is to reduce their dependence on foreigners so they have no leverage when China eventually acts on it's plans for world domination."
    author: "Chris Bordeman"
  - subject: "Re: China and Open Source? Ha!"
    date: 2003-08-20
    body: "At least you can download Red Flag distribution here:\nhttp://www.redflag-linux.com/xiazai/eindex.php"
    author: "kieltux"
  - subject: "Re: China and Open Source? Ha!"
    date: 2003-08-20
    body: "Is this why companies like Oracle are partnering with Redflag in China?\n\nSeems the US is the greater threat to Free Software with companies like SCO around.\n"
    author: "anonymous"
  - subject: "It doesn't see so"
    date: 2003-08-20
    body: "Sad, but RedFlag is an active GNOME Fundation member.\n\nLook at these pages:\nhttp://www.gnome.org/press/releases/redflagjoins.html\nhttp://www.redflag-linux.com/jujiao/enews_view.php?id=1000000011\n\nSo, \"GNOME-Desktop goes China\" (http://www.golem.de/0101/11920.html)\n"
    author: "Rasta"
  - subject: "Re: It doesn't see so"
    date: 2003-08-20
    body: "Lol, press releases from 2001 and their newest release 4.0 last month has (still) KDE as default desktop (http://distrowatch.com/table.php?distribution=Red+Flag)."
    author: "Anonymous"
  - subject: "Re: It doesn't see so"
    date: 2003-09-09
    body: "There isn't anything sad about it. Both KDE and Gnome are great projects, and support is a good thing."
    author: "Ashton"
  - subject: "I've used RedFlag 3.2 and it sucks"
    date: 2003-08-21
    body: "I bought RF3.2 thinking that this could be the distro for my Chinese employees to use. It sucks. Off the top of my head...\n\nBad Points:\n- Default user is root.\n- Interface badly designed (by comparison the leading thai distribution is a s-l-i-c-k KDE desktop), the default desktop is ugly and boring.\n- Sloppy. Using the system you just feel that it is sloppy.\n- Very bare bones. (this could be a good point depending on your point of view) very few apps (and I mean very few), little choice.\n- Try downloading from the site. It is so slow that you can't use it. This is a Chinese solution to a problem. Problem: our software is GPLd and we have to be seen to be making it publicly available but we want don't want freeloaders downloading it, what do we do? Solution: but it on a super slow ftp server that never works.\n\nGood Points:\n- Looks like Windows and works a bit like windows. From my point of view this sucks but I can see that for the digital illiterati of China switching from pirated windows this is probably a good thing. I seem to remember seeing a 'C:' drive in various places and the control panel has a remarkable similarity to windows.\n- Chinese Input Method - better than XCIN (the main free XIM chinese implementation) still buggy and not up to Windows Chinese IM.\n\nOn the whole Linux use and understanding is small here. Its extremely hard to find Linux admins that can actually think - everybody is so used to pushing buttons on Windows until they get the right configuration or re-installing that they haven't learned how to diagnose a problem and work through it. Of course there are exceptions, and if you are one of them my apologies. Our company does have a couple of uber geek Chinese Linux hackers but it took us nearly a year to find them.\n\nhttp://beijinglug.org"
    author: "JSC"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2003-08-21
    body: "Why 3.2?  It looks like 4.0 is out?"
    author: "ac"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2003-08-21
    body: "Ha-Ha. Because when I decided to try Red Flag the current version was 3.2. Version 4.0 was released after I bought 3.2 a few months back. Maybe 4.0 is a big improvement.\n\nI can only hope that 4.0 is a big improvement. The distro that looks good for China is Turbolinux. I only played with it for a few minutes at the recent LinuxWorld in Beijing but it left a good impression. But Turbolinux costs about 10x as much as Red Flag (US$100+ vs US$13)\n"
    author: "JSC"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2003-08-21
    body: "Yea, but you can you install it on multiple desktops?  If so then it is $100/n desktops."
    author: "Anonymous Coward"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2003-08-22
    body: "Good point. Although small and medium sized Chinese companies are used to paying $0 for pirated copies of Windows. $100 sounds like a lot to a Chinese manager who is conditioned to think 'Why should I pay for software?'\n\nSounds funny when I write it down but this is just so true here.\n\nLinux on the desktop in the workplace in China, much like in the west, will start in big organisations and then filter down. "
    author: "JSC"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2003-11-01
    body: "I have just downloaded Redflag Desktop 4.0 from the redflag site. I would have to say it downloaded pretty fast I thought.  Just for peoples reference Version 4.0 is only available for download on their Chinese website. The English one only contains Version 3.2.  Version 4.0 seems OK. Pretty basic though in terms of apps.  My over all comment is a linux version of windows98. This analogy gives you a pretty good idea of the kinds of apps provided and the overall server functionality. I think you have to buy the server version to get all the goodies like apache, etc.  I am now trying to figure out how to use the various Chinese input systems (the whole reason I am trying out this dist.). Would have to say its better than RH in this regard, but not as good as windows (could be I am not using it right). Another thing, it ships with Mozilla 1.3. I thought ver. 1.3 was the Beta version for version 1.4??\n\n"
    author: "Beng"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2004-01-14
    body: "yesterday I installed Redflag 3.2. I have no language proficiency in Chinese. When  I login after installing I am presented with what looks like a Product Key prompt, i.e. three boxes separated by a '-'. My CD came from linuxcd.org. I do not have ANY licensing or product key information. How do I get past this prompt? Many thanks.\ncoder"
    author: "coder"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2003-08-25
    body: "You probably want to try fcitx or miniChinput for inputting chinese. I found both of them quite powerful, at least in pinyin method. \n\n"
    author: "WW"
  - subject: "Re: I've used RedFlag 3.2 and it sucks"
    date: 2003-12-02
    body: "The slowness of the server is likely the general slowness of the Internet in China. It often crawls under the weight of all the viruses and trojans that live on what is essentially a 'worm farm' network run by MCSEs whos sum knowledge of network administration is putting a CD in a drive and clicking 'OK' until no more 'OK' buttons appear. The up-comming generation are a bit more cluey, but the present managers are largely just in their positions from knowing the right people, rather than knowing anything about computer networks.\n\nBTW: China has a communist government in the same way that the US Democrats are democraticaly inclined and the US Republicans haven't long-since sold out to the industrial imperialists. (And the Australian Liberals are liberal, the Labor party supports the workforce and the One Nation party isn't trying to split the country). Belive me, there are likely more comunists in the US today than in China!"
    author: "Glenn Alexander"
  - subject: "Inofficial goals?"
    date: 2003-08-22
    body: "One step closer to world domination. ;-)\nBut there is still hard work to do..."
    author: "CE"
  - subject: "rfmin"
    date: 2003-08-28
    body: "hi, i'm an developer from Redflag Linux. and personally i prefer KDE to GNOME.\nwe built lots of tools based on KDE or plain Qt library. and here is a screenshot for one of our new system management tool (rfmin), which can be found in our 4-series release. it may look like Windows, but it's linux at heart.\n\nunfortunately we dont have an English release yet, but it's surely on our schedule.\n\nyou can contact me or refer to our website (www.redflag-linux.com) for further information.\n\n"
    author: "pczou"
  - subject: "Re: rfmin"
    date: 2003-08-28
    body: "Good to hear from you.  Hope you stick with KDE! :-)"
    author: "ac"
  - subject: "CHINA ? !"
    date: 2005-01-25
    body: "Oh give me a break,china is the worlds largest country and you all know it!\n\n\n(Hopefully)\n\nAnyway..whoever debates this is .... ERM...wronge?\n\n"
    author: "Agricultural"
---
As <a href="http://asia.cnet.com/newstech/applications/0,39001094,39146335,00.htm">reported at CNet Asia</a>, China has announced a new policy mandating homegrown software solutions throughout government agencies.  <a href="http://www.redflag-linux.com/">Redflag Linux</a>, a Linux distribution backed by the Chinese government, seems to be well situated to to fill their OS needs.  And along with Redflag Linux Desktop 3.2 comes none other than our favorite desktop.

<!--break-->
<p>From the <a href="http://www.redflag-linux.com/source/Documents/redflag/Desktop32e.pdf">product sheet</a> for their latest desktop offering: &quot;Redflag Linux Desktop 3.2 introduces the latest and stable KDE desktop environment.  The interface, beautiful and decent, is similar to that of [the] Windows operating system, enabling convenient operation for a green hand.&quot;

<p>While I'm not sure about the &quot;green hand&quot; bit -- it sounds like they're happy with their KDE desktops.  Hopefully we'll see more of the public sector following suit and embracing regional Open Source / Free Software solution providers.