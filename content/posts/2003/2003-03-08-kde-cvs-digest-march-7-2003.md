---
title: "KDE-CVS-Digest for March 7, 2003"
date:    2003-03-08
authors:
  - "dkite"
slug:    kde-cvs-digest-march-7-2003
comments:
  - subject: "KUDOS !!!!!"
    date: 2003-03-08
    body: "It can't be said enough: thanks a lot Derek !!!!!!!!!!!!!!!!!!!"
    author: "John Herdy"
  - subject: "favicon"
    date: 2003-03-08
    body: "I noticed that Derek uses a nice newer KDE 3.1-esque favicon for his site.. I think that www.kde.org and dot.kde.org perhaps should adopt it 8)"
    author: "fault"
  - subject: "Re: favicon"
    date: 2003-03-08
    body: "Yeah, it's nice..."
    author: "Navindra Umanee"
  - subject: "Re: favicon"
    date: 2003-03-08
    body: ">Yeah, it's nice...\n\nAnd ceremoniously swiped from the kde icons. Melchior Franz helped me set it up.\n\nDerek (trying out cvs konqueror)"
    author: "D Kite"
  - subject: "Re: favicon"
    date: 2003-03-09
    body: "Personally I envisaged a Crystalised version of the \"K\" rather than a gear.... I note that dot.kde.org and the cvs-digest are using very slightly different gears at the moment.\n\nI'm happy to change the kde.org favicon.ico to a gear though, but I can't quite decide if I like the dot one or cvs digest one more at the moment."
    author: "Chris Howells"
  - subject: "Re: favicon"
    date: 2003-03-09
    body: "Yeah, the kde.org favicon.ico should probably remain a K...  it's pretty good already though old style."
    author: "Navindra Umanee"
  - subject: "KDE 3.1.1 release schedule?"
    date: 2003-03-08
    body: "Is there a release schedule made for KDE 3.1.1 , 3.1.2, etc.? I'm wondering it merges from Safari will make it into those."
    author: "Bart"
  - subject: "Re: KDE 3.1.1 release schedule?"
    date: 2003-03-08
    body: "KDE 3.1.1 release schedule:\n\nhttp://developer.kde.org/development-versions/kde-3.1-release-plan.html"
    author: "rookkey"
  - subject: "Re: KDE 3.1.1 release schedule?"
    date: 2003-03-08
    body: ">wondering if merges from Safari will make it into those.\n\nDon't think so. The large safari merges from the last 3 weeks weren't backported.\n\nWe'll have to wait for 3.2.\n\nDerek"
    author: "D Kite"
  - subject: "Re: KDE 3.1.1 release schedule?"
    date: 2003-03-09
    body: "You're looking for a changelog, rather than a release schedule.\n\nSee here for the 3.1 to 3.1.1 changelog:\n   http://www.kde.org/announcements/changelogs/changelog3_1to3_1_1.php\n\nFiguring out where the other changelogs are is a simple exercise :)\n"
    author: "Jon"
  - subject: "Thanx Derec"
    date: 2003-03-08
    body: "YEAHHH\nanother cvs-digest. looking forward for it every day!\n"
    author: "gunnar"
  - subject: "just a question."
    date: 2003-03-09
    body: "Out of curiosity, but why don't you use ChangeLogs as any other project ? So everyone could look on their own what's changed. Many ChangeLogs in the kde modules are outdated for years or some projects simply don't have any."
    author: "oGALAXYo"
  - subject: "Re: just a question."
    date: 2003-03-10
    body: "Probably because of the scope of the project...\n\nChangelogs seem to mainly be used by smaller projects, or larger projects that are maintained by a few people.\n\nI've also noticed that a lot of the time, the Changelogs used by other projects, are nothing more than CVS commit messages, in which case, we have that here ;)\n\nUsualy a more accessable global Changelog is assembled in time for the actual release.\n\nBTW, I'm not a KDE developer, so I'm only making guesses from what I've seen over the years"
    author: "Stuart Herring"
  - subject: "Re: just a question."
    date: 2003-03-10
    body: "Becuase IMHO changelogs doesn't make sense if you have a cvs system.\n\nIn a cvs system, you put all your changes into the cvs commit comment, which is the best you can do. Later you will be able to use annotate and can see for each line of code, what was the reason for this line.\n\nSo using a comment in a commit is rather mandatory, if you want to use full cvs functionality.\nThen, when you then want a changelog, just have a look into the comments.\n\nWhy should you then still maintain such a IMHO mostly useless changelog file?\nAre you still interested one year later what was changing months ago whithout any relationship to the code? I doubt this strongly.\n\nWhat we still maintain is a CHANGES file, which contains changes which is interesting for the user and not for the maintainer. This CHANGES file is then used for the remarks on the release.\n\nPhilipp\nKSpread developer"
    author: "Philipp"
  - subject: "Some aplications ..."
    date: 2003-03-10
    body: "Noatun and ksim crash when change preferences, is normal ? For example:\nChange the skin type from kjofol to kaiman crash ... to minimal crash .. in the ksim quit \nthe uptime crash, change some skins crash, most people use gkrellm and xmms ...\nWhats up with this apps ? or use gkrellm and xmms and no more problems ?"
    author: "anonymous ..."
  - subject: "Re: Some aplications ..."
    date: 2003-03-10
    body: "They both work pretty good for me (installed from source code)\nDid you use the right RPM packages, are they known to work, have you looked in bugs.kde.org to see if somebody else reported a similar problem, have you filed a bug report, can you provide a backtrace...\nThis would be more helpful to the developers."
    author: "Norbert"
  - subject: "Crashes"
    date: 2003-03-10
    body: "Entire KDE desktop locks up for me with KDE 3.1 forcing reset button reboot."
    author: "anon"
  - subject: "Re: Crashes"
    date: 2003-03-10
    body: "Don't you have to love those very verbose wanna-be bug reports? :-)"
    author: "Anonymous"
  - subject: "Re: Crashes"
    date: 2003-03-10
    body: "I can't get my e-mail.  Please help.  (joke alert :)"
    author: "Haakon Nilsen"
  - subject: "Re: Crashes"
    date: 2003-03-10
    body: "Are you using a GeoForce?"
    author: "anno"
  - subject: "Re: Crashes"
    date: 2003-03-14
    body: "If so, you shouldn't be having any problems if you use the 3190 drivers.  Linux/X is rock solid with both a Athlon/GeForce2 and a Pentium4/GeForce4."
    author: "repugnant"
---
<a href="http://www.kdevelop.org">KDevelop</a> gets more templates, <a href="http://quanta.sourceforge.net">Quanta</a> 
gets better action toolbars and <a href="http://uml.sourceforge.net">Umbrello</a> gets new code generators.  A <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">dummy</a> KDE 3.2 release schedule. More merges from <a href="http://www.apple.com/safari/">Safari</a> to 
<a href="http://www.konqueror.org/konq-browser.html">KHTML</a>. 
Plus a large number of bug fixes. Read it all in the <a href="http://members.shaw.ca/dkite/mar72003.html">latest KDE-CVS-Digest</a>.
<!--break-->
