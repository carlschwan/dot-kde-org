---
title: "KDE-CVS-Digest for August 22, 2003"
date:    2003-08-23
authors:
  - "dkite"
slug:    kde-cvs-digest-august-22-2003
comments:
  - subject: "Awesome stuff!"
    date: 2003-08-23
    body: "Especially the new Khotkeys! I'v elaways wante dto be able to use mouse gestures and the keyboard for a hotkey or simply a mouse gesture. This will make me a lot more productive.\n\nThe KDialog used by any X application wil certainly make Linux applications other than KDE's look less foreign once developers adopt this.\n\nI'm also glad tos ee Khtml improving steadily, but unfortunately it si still quite a bit behind Apple's version. \n\nKstars is also cool, I'm not really too much into Astronomy, but I lvoe playing around with it and learning some new things from it.\n\nThanks for keeping us up to dater Derek and thanks to all the KDE contributors who have made this project so awesome!"
    author: "Jace"
  - subject: "Re: Awesome stuff!"
    date: 2003-08-23
    body: "To take it even further, the upcoming KDE 3.2 also features spell checking - even in Konqueror ;-)\nMicha"
    author: "affenschlaffe"
  - subject: "Config Editor"
    date: 2003-08-23
    body: "Heh. I can see myself using nothing but the config editor in the future. No more aimless clicking in kcontrol.. ;)"
    author: "anonon"
  - subject: "Konqueror and Safari merged status"
    date: 2003-08-23
    body: "I am always very impressed by the KDE CVS improvement. All seems to be better and better but for weeks now I don't see any \"Safari\" or \"Apple\" merge in the changelog of konqueror (regarding the digest).\n\nCan anybody explain what is the situation between Safari and Konqueror for now ? I have lot's of friends who change their website to use XHTML + CSS (That's great) and they ask me to test their site on Konqueror and they don't ask a Safari user to test it because he believe that the engines are the same (or ask a Safari user believing Konqueror react the same). That is not the case because Safari was based on a old KHTML version so KHTML get new patch and in the other side Apple team improved Safari-KHTML engine ( Dave Hyatt presente lot's of them in his blog http://weblogs.mozillazine.org/hyatt/).\n\nSo now we have two KHTML engine which are able to handle different CSS properties or selectors. When will the patchs of Apple be merged ?\nAnd moreover, why Apple didn't work with KDE team on the same CVS. I think Apple is happy to have  such a free engine but the way they work is not the best because KDE team has not the same team so it is difficult for it to merged all the patch. If a Safari developer or Konqueror read this, can he explain why KDE team and Safari team don't work together ? Is it possible ? If no, why is it impossible ? \n\nThanks a lot for the answer and long life to my favorite browser (konqui).\n\nFranck\n"
    author: "Shift"
  - subject: "Re: Konqueror and Safari merged status"
    date: 2003-08-24
    body: "I am not a developer, much less a khtml developer, so what I say could be inaccurate...\n\nSafari was originally based on a 3.0 era khtml snapshot. Since then, they HAVE merged khtml changes into the safari codebase, and of course many of the safari changes have been merged into the khtml codebase. I remember reading something written by one of the devels that many of the safari changes are being merged into khtml, but just haven't always been noted as such. So yes, the safari changes are getting into khtml, but no, its not quite up to date, but is getting there. It is, of course, a moving target, so naturally we'll never have all the safari changes, just as safari will never have all of our changes"
    author: "lrandall"
  - subject: "Re: Konqueror and Safari merged status"
    date: 2003-08-25
    body: "Yeap. A shame though...\nI was expecting active colaboration and eventualy have one branch actively developed by both teams. "
    author: "Sleepless"
  - subject: "KHTML not as good as Safari's"
    date: 2003-08-23
    body: "I've palyed around with Safari 1.0 http://www.apple.com/safari and on my friends Mac it renders pages better and it seems to me faster than Konqueror so I think Konqueror is really behind Safari's codebase, still Konqueror does render most pages well.\n\nBut, I do have some layout problems ocassionally and in some cases I can't use some pages at all, for example I can't use Netscape Mail with Konqueror.\n\nAnywayy, thanks for the CVS Digest and yeah Khitkeys 2 does sound reallly cool ;)\n\n"
    author: "Jace"
  - subject: "Re: KHTML not as good as Safari's"
    date: 2003-08-23
    body: "Yeah, I found this too.. for example, \n\nhttp://www.anerispress.com/wltsim/ renders perfectly with Safari 1.0, but doesn't with Konqueror-cvs (compiled yesterday) :("
    author: "KDE"
  - subject: "Re: KHTML not as good as Safari's"
    date: 2003-08-24
    body: "That page looks just the same in Konqi and Mozilla from what I can see.  I don't know what it looks like in Safari."
    author: "manyoso"
  - subject: "Re: KHTML not as good as Safari's"
    date: 2003-08-24
    body: "It seems to have changed since it was featured on slashdot (the author probably made it smaller by removing a lot of JavaScript and images)\n\nEarlier today there was a JavaScript menu that worked in Safari and Mozilla, but not in Konq."
    author: "KDE"
  - subject: "Config Editor = Usability nightmare"
    date: 2003-08-24
    body: "There is a big danger that The configuration editor will tempt developers to leave out (or even remove) configuration options in kcontrol and just say \"just use kconf\" to users.\n\nOne of KDE's great advantages is it's customizability. Please keep it that way."
    author: "Roland"
  - subject: "Re: Config Editor = Usability nightmare"
    date: 2003-08-24
    body: "Agreed, though I'm sure this will happen.\n\nDoes anyone happen to now if kconf displays options not included in KControl, and even those not in the config files themselves? There are a lot of settings we don't get to alter in KControl that I'd love to get my hand on, especially if kconf can provide an explanation of the settings."
    author: "Tom"
  - subject: "Re: Config Editor = Usability nightmare"
    date: 2003-08-24
    body: "> There is a big danger that The configuration editor will tempt developers to leave out (or even remove) configuration options in kcontrol and just say \"just use kconf\" to users.\n \nI seriously doubt a developer would want to do that. I don't. However... if we introduce a feature in a minor release during a string freeze we would have two choices... tell you to set it in kconf or a text editor. So far we have had only the text editor option. We can't add to config dialogs during a string freeze. So this could see use under those conditions."
    author: "Eric Laffoon"
  - subject: "Re: Config Editor = Usability nightmare"
    date: 2003-08-24
    body: "If this a rarely used feature, say by 0.001% of the user base and by people who have special needs and the neccessary skills, I would say that this is a good thing.\n\nAn example would be a debugging mode for developers.\n"
    author: "Tim Jansen"
  - subject: "Re: Config Editor = Usability nightmare"
    date: 2003-08-24
    body: "creating a basis and an advanced mode sounds more like a solution to me."
    author: "Mark Hannessen"
  - subject: "Re: Config Editor = Usability nightmare"
    date: 2003-08-24
    body: "Hi Mark...\n\nUnfortunately, that doesn't really work. Having a basic and advanced system poses, first, a most essential problem:\n\n- What is advanced and what is not?\n\nThe line there is never clear, and unfortunately, you get users who are advanced in one area, such as understanding details of fonts, but not others, such as networking.\n\nMoreover, it also causes problems for technical support, since the user may not see the same controls that the tech support person sees.\n\nFinally, it also breaks the user experience, since there are hidden controls, which only appear when the mode is changed. That really does confuse users.\n\nWhat the config editor will allow, is users to have access to the really tiny features to tweak their systems, which, while fun, are not relevent to most people. So in short, we have KControl as the normal options and KConfEditor for really obscure, pedantic stuff."
    author: "Dawnrider"
  - subject: "Re: Config Editor = Usability nightmare"
    date: 2003-08-25
    body: "which inevitably leads to the question\n\nWhat is really obscure, pedantic stuff?\n\nI don't see how this is easier to answer than the basic/advanced question..."
    author: "got one"
  - subject: "Bloated menus"
    date: 2003-08-24
    body: "Can somebody using CVS HEAD tell me, and others who might be interested, how much has been done to tackle the bloated context menus? I recently gave GNOME another go, and just having a clean, simple looking file manager almost made me want to swap, until I noticed all the features I need in KDE.\n\nI'm thinking of things like the 3 open options being in the RMB menu on a web page (better grouped into an \"open\" menu?), \"Encrypt file\" & \"Copy To\" & \"Stop animations\" & similar options going into the proposed \"actions\" submenu, etc.\n\nAlso, have the menus in the Konqueror menubar been simplified at all? They confuse me after years of using KDE, so goodness knows what a new user thinks when they look through them!"
    author: "Tom"
  - subject: "Re: Bloated menus"
    date: 2003-08-24
    body: "That's the main reason, why I'm still using Mozilla. The visible menus entries (menubar & popupmenus) should change dynamically, depending on the visible data. And I want to have them totally configurable, to adjust them to my needs for whatever protocol I use. Even if Konqueror would have the best rendering engine, I wouldn't use it, because the interface sucks. :-("
    author: "Carlo"
  - subject: "Re: Bloated menus"
    date: 2003-08-24
    body: "> should change dynamically, depending on the visible data.\n\nThey are."
    author: "KDE"
  - subject: "Re: Bloated menus"
    date: 2003-08-24
    body: "Oh, yes - didn't noticed it, because I'm so dissatisfied with the konqueror interface that I use it seldom. That being said - I use the functionality via Krusader (and Mozilla) instead. I never liked this explorer/bookmark approach. To me filemanager and browser are two different/seperate things.  \n\nThe other points are still valid: \n1. In general overcrowded mainmenus \n2. The missing configurability of mainmenus and popupmenus. \n\neg.: http/popupmenu - it inludes non-relating entries like 'encrypt'(kgpg), 'create data cd with k3b', and a lot of other entries which relate, but which i never use. In fact the both top sections are useless to me. Selected text, doesn't cause a popupmenu with a copy entry in it, etc..\n"
    author: "Carlo"
  - subject: "Re: Bloated menus"
    date: 2003-08-24
    body: "Yes they are!\nAnd even though they are, the menus are still a huge mess! I find it amazing how messy they are, honest!  :)\n\n- Let's click on the Floppy Icon\nOpen With?\nEncrypt File?\nCreate data CD with K3B?\nEdit file type?\nAdd to bookmarks?\nCopy to public folder?\n\nI'll just stop here... I understand why this happens, but really does any one actually thinks this should appear there? Did anybody ever actually used this? Honest it's just crazy :)\nAlso try clicking in a link and count the entries. Even stuff like the back arrow shouldn't be there. Why would you click a link to go back?\n\nAnyway this is 3.1.3, don't know how things are in CVS.\n\n "
    author: "Sleepless"
  - subject: "That's nothing"
    date: 2003-08-24
    body: "On SUSE 8.2 KDE 3.1.1 I get a whopping 17 context menu entries and that's not counting the submenu entries!!!\n\nWhat annoys me the most about these super duper bloated menus all over KDE is that they often have just about everything EXCEPT what I want.\n\nLet's take Konqueror for example:\nI highlight some text and bring up a context menu, I have 18 freaking entries like Open in Background Tab, Create Data with K3b, Encrypt File, Secuirity, Stop Animations, Back, Forward, Reload, Open With etc. - just about anything you can imagine but not A FREAKING \"COPY\" command, sure there's COPY TO> but that copies the whole page!\n\nNow the funny thing is, when I right click a link there is a simple COPY command, but not when I right click highlighted text?!\n\nMozilla is still ways better in usability and features for now.\n\nIn this case for example the context menu for the text and just an empty color is the same. "
    author: "Alex"
  - subject: "Re: Bloated menus"
    date: 2003-08-24
    body: "I'm using Mozilla mainly because of the more powerful tab features, cleaner more logical interface, bookmakr search/save feature (makes my bookmarks organize themselves), history search feature, powerful pop up blocking, and finally better compatibility."
    author: "Dario"
  - subject: "Re: Bloated menus"
    date: 2003-08-24
    body: "I agree that Konqueror has some problems, like the home page that should be different between the file manager and the web browser.  For an end-user, these two kinds of applications are very different.\n\nHowever, not all parts of the Konqueror interface suck.  Web shortcuts for example are very useful.  In fact, they're the main reason why I almost always use Konqueror.  The cookie management system is also better in Konqi than in any other browser I know."
    author: "Ned Flanders"
  - subject: "Re: Bloated menus"
    date: 2003-08-24
    body: "> For an end-user, these two kinds of applications are very different.\n\nAs an end-user I would wonder, why I have to browse directories. The applications should \"know\", which data belongs to them. \n\nI just don't like the \"explorer\" approach. In the end, why not adding something like konsole://, kword://, rtfm://, ...\n\nIn german exists an expression for this: \"eierlegende Wollmilchsau\". In english: \"ovulating woolmilksow\" - I assume there is a better translation, but I don't know it. The meaning should be clear, I think?!"
    author: "Carlo"
  - subject: "Re: Bloated menus"
    date: 2003-08-25
    body: "Document centric is the big thing, not app centric. \n\nPeople should not need to think about apps, just what 'things' they have. \n\nWhat really needs to happen is better metadata and search. "
    author: "rjw"
  - subject: "Re: Bloated menus"
    date: 2003-08-25
    body: "Normally, I know what I want to do and start the application i need to do the job, instead of opening a directory and searching for a document in a bunch of files. Maybe document centric is the \"big thing\", but it is not the way I work."
    author: "Carlo"
  - subject: "caret navigation?"
    date: 2003-08-24
    body: "quite interesting, how always!\n\n but what the heck does caret navigation support in konqueror mean?\n\nAny suggestions?\n\n >8^) kata"
    author: "katakombi"
  - subject: "Re: caret navigation?"
    date: 2003-08-24
    body: "It has to do with editing rich text format. IE and Mozilla do it. I don't know what the application is.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: caret navigation?"
    date: 2003-08-24
    body: "contentEditable/Caret-Navigation is important for Content Management Systems (CMS).\nCMS (in case you don't know) basically means:\nIn a larger company there can be lots of people who don't know HTML,\nbut they want to change some text or numbers. \nCMS systems let you log in, go to the page and edit your content directly on the\npage. For this to work it's necessary that a browser can show a caret just like in Word.\nIf you can get your hands on a Windows (Ugh!) PC with Internet Explorer (Ugh,Ugh!)\nthen you can try feeding it the following code:\n<html>\n<body>\n<div contentEditable=\"true\">\nThis DIV can be edited on the page. Cool - isn't it?\n</div>\n</body>\n</html>"
    author: "Mike"
  - subject: "Re: caret navigation?"
    date: 2003-08-25
    body: "But it can't. If I place the above mentioned HTML-code in a file and access it with either Konqueror OR Mozilla, neither of them are able to edit the innherHTML."
    author: "Andreas Joseph Krogh"
  - subject: "Re: caret navigation?"
    date: 2003-08-26
    body: "Yeah, caret navigation in khtml is the same thing as in Mozilla: It display a blinking caret with which you can move through the text by the keyboard.\n\nIt does *not* allow you to edit text. So contenteditable won't work. But caret navigation is a prerequisite for that."
    author: "Leo Savernik"
  - subject: "Re: caret navigation?"
    date: 2003-08-26
    body: "OK, so how do I activate it?"
    author: "Andreas Joseph Krogh"
  - subject: "Re: caret navigation?"
    date: 2003-08-27
    body: "There's no means to be activated by the user yet.\n\nIt will be possible by pressing F7, but that patch is still pending.\n"
    author: "Leo Savernik"
  - subject: "Re: caret navigation?"
    date: 2003-08-27
    body: "OK, thanks for the info."
    author: "Andreas Joseph Krogh"
  - subject: "KHotKeys"
    date: 2003-08-24
    body: "I've been using a very old khotkeys with recent KDE releases for a couple huge reasons: It doesn't require a menu entry for a shortcut and thus won't use startup notification for every single keypress, and it allows you to run a command even if previous commands haven't completed (or, at least, it runs them in succession very fast).\n\nThe new khotkeys (as done through kmenuedit) always shows a notification, and is slow in responding.  For a lot of programs, this isn't a big problem.  However, I have mapped F10 and F11 to volume down and volume up.  The old khotkeys does great with this: I can hold down F10, and my volume goes down quickly, without cluttering up my taskbar.  The new khotkeys takes lots of time to run each volume change, and puts tons of notifications in the taskbar.\n\nI have other similar keys, such as F5-F8 for controlling xmms.  My main question, then, is does the new khotkeys work properly, like the old one, or does it have the same semantics as the current one?"
    author: "KDE User"
  - subject: "Re: KHotKeys"
    date: 2003-08-24
    body: "For Volume you can assign keys in KMix. Just right click on a slider and select\n\"Assign keys...\" (Or whatever it is called in the English version)"
    author: "Jan"
  - subject: "Re: KHotKeys"
    date: 2003-08-24
    body: "Thanks for the tip, but I don't have KDE multimedia installed, and I have other keystrokes that I don't want notification on, too.\n\nWorst case I use my own keygrabber that I wrote in conjunction with KDE's, but I'd rather have it all managed through the same place."
    author: "KDE User"
  - subject: "Re: KHotKeys"
    date: 2003-08-24
    body: "You can disable notification by adding X-KDE-StartupNotify=false in the corresponding .desktop file under share/applnk (KMenuEdit in CVS HEAD has checkbox for that, I'm not sure if 3.1 has that too).\nI'll have a look at the problem with the delay with repeated keypresses."
    author: "Lubos Lunak"
  - subject: "Re: KHotKeys"
    date: 2003-08-24
    body: "Ohh, excellent, thank you very much.\n\nAs for the delayed keypress, that was an issue with KDE 3.1's hotkey system.  I'm using your khotkeys snapshot from 20020412 and it works absolutely perfectly.  So if you've ported your code over to KDE 3.2 then I'd imagine it still works properly.  If only the KDE 3.1 code was updated that there might be an issue with delayed presses.\n\nBy the way, thanks for khotkeys, I think I'd probably die without it.  It makes things sooooo much more convenient."
    author: "KDE User"
  - subject: "SuperKaramba in KDE"
    date: 2003-08-24
    body: "Will SuperKaramba be included in 3.2? I think it should as long as more developers work on it to bring it up to a 1.0 release and improve the internals, maybe adopt a more flexible deisgn and XML themes like gDesklets.\n\nThough I love the simplicity of it: \n \n Create a text file called myexample.theme \n \n In it type this: \n \n karamba W=140 H=20 #I am commenting! :p w = width and h = height these define the size of the theme \n text value=\"SK is super easy!! ;)\" #just tells karamba to put SK is super easy!! ;) in the space we just created \n \n Yes, this is really how easy it is and it has full session support and will appear on all desktops etc.! \n\nYet, it needs better documentation, the last API update was .28, for example and again I suggest checking out the gDesklets source also.\n\nI don't really know how to do anything more complicated than a clock or an alarm etc. but it seems pretty easy in general.\n \n"
    author: "Alex"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-24
    body: "no thank you."
    author: "not_registered"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-24
    body: "Isn't it partially redundant?"
    author: "CE"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-24
    body: "how so? i really dont know so educate me."
    author: ":-)"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-24
    body: "Regarding features KDE already has."
    author: "CE"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-25
    body: "No, its not redundant and it will be able to integrate into KDE seamlessly, look at:\nhttp://www.kdedevelopers.org/node/view/124 and  http://www.kdedevelopers.org/node/view/125\n \nCome December,  I am sure Karamba will be ready for primetime and be good enough to get included into KDE! You wouldn't have to use it if you don't want to, it will just be included in extragear or something.\n\nSK is very popular and many people would like it in 3.2."
    author: "Alex"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-25
    body: "But it would be nice to have some features of it in the KDE core parts.\nBut as a whole it's too separate for me."
    author: "CE"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-25
    body: "i'd like to see it to in 3.2"
    author: "Giovanni"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-08-28
    body: "No way, it's missing maturity at least."
    author: "Anonymous"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-09-06
    body: "Hello.  I'm the author and SuperKaramba.  I think it would be great to add it to KDE at some point if people want it.  However, I really don't think it will be mature enough to include by the 3.2 release.\n\nThe program is still developing very rapidly and putting a release in KDE at this point would hurt KDE (unstable program, documentation improving but still lacking) and hurt SuperKaramba (people wouldn't take advantage of new features because they would be stuck with an old version and have no idea how to get a new one)."
    author: "Adam Geitgey"
  - subject: "Re: SuperKaramba in KDE"
    date: 2003-09-07
    body: "SK in kdeutils in kde 4 would be great :-)"
    author: "anon"
  - subject: "Bloated Menus MUST STOP NOW!!!"
    date: 2003-08-24
    body: "Okay, I couldn't take it any more, seeing each KDE version get more and more bloated context menus and slowly turning into a usability monster who's rampage is making more and more KDE applications have bloated, illogical menus.\n\nThis in my eyes is by far the most serious problem with Konqueor. It's like havinga  book who's index is scattered with all kinds of entries from other books and with inaccurate page numbers causing an unbelievable amount of problems.\n\nIn response to this issue I wanted to make the first step in the right direction by submitting a bug report, if you are as disspointed in the way KDE's context menus are going please vote for my bug here: http://tinylink.com/?DMZFXCKyrA and maybe such dire problems will get resolved by 3.2.\n\nIf you want to see more of my wishes and bugs here is my bug list: http://tinylink.com/?rfvbhDinLz \n \nThank you and have a good day ;)"
    author: "Alex"
  - subject: "Re: Bloated Menus MUST STOP NOW!!!"
    date: 2003-08-24
    body: "Hi Alex...\n\nYou really need to take a look at CVS. A lot of work has been done by people in changing the context menus. These should be very evident in 3.2, as will the KControl changes and many other usability tweaks.\n\nYou might like to subscribe to the usability mailing list to keep up to date with these things. There is a signup form at http://lists.kde.org"
    author: "Dawnrider"
  - subject: "BUG REPORT ALREADY EXISTED"
    date: 2003-08-24
    body: "This si good news and I'm aware that an actions menu has been added in 3.2, but I'm not sure what else has been added. I would love to see a screenshot of the context menu that comes up when text is selected.\n\nAnyway, I found out there was already a bug report about this and it has over 300 votes! But it needs even more votes, it needs to have more votes than any other wish because this really is the worst part of KDE and what the gnomers complain   about the most ;p\n\nPlease vote here: http://bugs.kde.org/show_bug.cgi?id=53772\n\nTHANK YOU!"
    author: "Alex"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-24
    body: "Please don't campaign for bug report votes. It is counter-productive. It is essential that users only vote for the bugs that *annoy them*. If they vote for campaigned things, then some bugs will be artifically inflated, leading to them being fixed before bugs which annoy the community more.\n\nTo be honest, Gnome users usually have a go at KControl, the K menu, Kicker and Konqy. Context menus are eventually on their lists, but fairly low. To be frank, fixing/changing any of these things won't make them change over to KDE, and the first priority is improving the user experience for existing KDE users. For example, people complain about the Gnome file selector, and while I accept that it is a dog, fixing it won't get me to change to Gnome. Fixing GTK so that it isn't so hideous to program with would be a good start, but that is a topic for another day. "
    author: "Dawnrider"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "And which would you prefer, more votes for existing bugs or more bug reports about the same bugs? Bugzilla search system is not perfect, you know. I'd open at least 30 new bugs, but it's too much of a burden to prepare proper test cases. However, if I knew that these bugs already exist, I'd vote for them.\n\nThe thing is, this bug was opened in January, and KDE users *are* bothered with it. Developers obviously don't care. The voting system is there exactly for cases like this.\n"
    author: "Anonymous"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "Personally, I'd like more votes for the bugs people find meaningful. The fact is, it is a self-resolving issue. If you really get annoyed by a bug and it means something to you, you'll either vote for it, if the report already exists, or you'll open one yourself. It is really annoys you, you'll take the time to open the report and produce a few screenies or other testcases. If it's really too much of a burden, then it doesn't mean that much to you...\n\nIn January, 3.0 was out and 3.1 was in feature freeze. Changing the menus like this really does mess up the internationalisation strings, and people would be more annoyed it most of the context menus didn't work in 3.1 in their language, if it wasn't English.\n\nDevelopers do care about this stuff... that's my point. They have been working hard on changing the context menus fo 3.2, because users wanted it. There has been a great deal of work on K menu organisation, Konqy menus and KControl for exactly the same reason. Developers really do care and you'll see that in 3.2."
    author: "Dawnrider"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "\"If you really get annoyed by a bug and it means something to you, you'll either vote for it, if the report already exists, or you'll open one yourself. It is really annoys you, you'll take the time to open the report and produce a few screenies or other testcases. If it's really too much of a burden, then it doesn't mean that much to you...\"\n\nRegardless of the rest of the conversaion, this attitude really annoys me. I can understand that there's only so many bugs KDE developers can handle, and that a flood would just make development even more slow. But this attitude just means that you'll only get techies submitting bugs. Unless I'd prodded her, my girlfriend would never have submitted the bugs she had, including some for bugs that really caused her problems, because it looked confusing, and because she didn't see the bug system and think: ah, if I submit a bug report, it will get fixed, hooray!\n\nIf we are to cater for users who aren't used to the Free Software way of doing things, we can't take this attitude that it's good to keep people from filing bugs unless they're *really* bothered.\n\nIn the case of the Konqueror menubar and context menus, I think it's incredibly obvious that there are serious problems. Just look at the menu bar... many of the items from \"View\", \"Tools\" and \"Settings\" could be in any one of those menus for all I know!"
    author: "Tom"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "I think maybe what I said came across in a different way to the way it was intended. \n\nWhat I meant to say, is that without bug campaigning, the system will naturally float to the top the most critical bugs, and the less important bugs will get, by their nature, reported and voted for less, simply because people won't go out of their way for them.\n\nDon't get me wrong, I accept entirely that current bug reporting is a barrier to entry for many non-technical end users, but I also submit that there are technical end users who perform the same roles, or know people such as yourself who can assist them in filing bug reports. Ideally we have a bug reporting system that is perfectly user-friendly and everyone can submit reports and the thing automates screen-grabs, sample cases, etc. But unfortunately, there is a limit on those things. There are users, for example, who don't understand the concept of a mouse. Bugs.kde.org doesn't cater for them. An extreme case, but a very real barrier to entry. Similarly, moderate users who can't grab a screenshot or describe their problem in a useful manner (\"Tables don't work in KWord\") are difficult to deal with too.\n\nIn the long term, a better bug reporting system would be very nice, maybe with automated recording of user actions and users able to attach notes to explain what they wanted to happen. Until that can be brought about, however, there are a great deal of bugs to work through, that, although submitted by technical users, can be representative of general users too. Those are what we have, and we need a balanced bug voting system to understand how critical those bugs are to the community and in what proportion. It's not truly representative of the total user body, but we have to hope that it isn't so far off. Also, at present, fairly significant bugs are being fixed, which I'm sure all users need to see resolved.\n\nAs for Konqy menus and context menus, there is a lot of work going on wrt those, and the results are progressing nicely in CVS. Those will be released in 3.2, as mentioned earlier. Hopefully all users will enjoy the benefits of that work.\n\nTo be honest, if the KDE's most serious problems are that there are context menus with limited ergonomics, I would consider the project to be doing rather nicely. It's a definite move forward from applications not existing/not working/crashing all the time.\n\nThis is what the 3.x series has been all about. 3.0 was definitely a massive leap forward. 3.1 saw a stabilisation of that to a very high degree, with features being rounded off and added to, making them more usable. In feature terms, 3.1 is relatively complete. It will work on anyone's desktop, and applications notwithstanding (I still want a KDE replacement for Photoshop), it is ready for daily use. 3.2 is really a refinement of 3.1, with a number of new features to make life easier and some great additions, such as Kopete, but it is most of all, a usability improvement. Menus are improved, KControl is changed, context menus are redesigned, Konqy's tabbing is improved and hundreds of other tweaks to make life easier and more intuitive for users.\n\nThe developers are working really hard on these things, and really listening to users about many issues, in fact, this is a user-experience focussed release, more than anything else. Wait and see what you think. I think you'll like it."
    author: "Dawnrider"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "Ok, I have here KDE compiled from CVS maybe one week old:\n- There is no significant improvement in RMB menues.\n- There is still no Copy option when selecting a piece of text.\n- Right click on background in file management mode shows 24 options.\n\nYour other points: I entirely agree. This same problem existed before, but I think now is the time to solve it, since other major issues are mostly solved. If I have time I will dive into Konqueror code during the following few weeks and try to at least understand how it works."
    author: "Anonymous"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "Pardon my previous post. I accidentally ran the older version of Konqueror. Right click on background in file management mode gives now only 14 options. This is definitely an improvement."
    author: "Anonymous"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "Any campaigning for votes obviously skews usefulness of votes at all. And our friend\nAlex has been doing so much of that, that for many areas votes are absolutely meaningless,\nbecause they loose any statistical signficance. Just because you can post on 10 websites\nand convince 15 people to vote does not mean it's more important than something\na lot of people /independently/ came to conclusion of. Frankly, I'd like to see \na dot and kde-look policy forbidding campaigning for bugs. \n"
    author: "SadEagle"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "If 15 people voted for a bug, then 15 people are annoyed by that bug. If you think that there is something more important, just post it here so we can see for ourselves.\n\nMaybe we should consider lowering the maximum number of votes one can give, so people would make priorities? Perhaps 20 votes per package and 100 votes total? Or maybe someone could attempt a \"weekly bug digest\" (ugh, sounds disgusting ;) ) where various bug reports would be presented in a non-coder friendly way, so people could consider voting for them?\n\nAnyway, I dislike the ban because a) spammers will just post the link to other forums, such as kdelook or slashdot and b) that way you limit the votes just to coders and people that know how to use bugzilla, and this will skew results away from wishes of regular, non-coding people - and KDE of all OSS projects should try to cater to non-coders. The reason why Alex alone can pump the votes for a bug is because so few people vote for bugs at all!? The most wanted wish has 1200 votes, that's 60 people - most web polls are considered useless if the top voted option has less than 100 votes. So, if you want to prevent skewing, vote! And ask others to vote as well :o)\n"
    author: "Anonymous"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "Do you actually think more votes will help? Isn't it like spam?\n\nThe developers are well aware of the problem. CVS HEAD already has a reduced context menu. No doubt more fine tuning will happen in the months to come. This is one of many usablility issues that are being resolved.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-25
    body: "I don't think that's true. All I did was make some of my bugs known, you don't have to vote for them if you don't want to.\n\nIt's just that without spreading the word nobody will know. Believ eit or not most people who are annoyed by something in KDE wil not bother to report it or search through the bug database.\n\nSome of the things that annoy people the most are never voted on because users don't case to do so, KDE has about 9,200 bugs and wishes!!! How many people do you think will ever bother to look through all of them or even search through them. \n\nNot a lot. this is why if I want any votes I need to spread the word. Also, I've only plugged my bugs on the dot."
    author: "Alex"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-26
    body: "But the developers are the ones that need to be aware of the issue. They are. More votes won't do anything. What is needed are coders to fix the problem. It may not be a simple matter of changing a small thing. There may be some infrastructure needed. Or it may be a matter of someone spending alot of time going through each application. Are you volunteering?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: BUG REPORT ALREADY EXISTED"
    date: 2003-08-26
    body: "I don't think that his intention was to spam the index. It's just that he's trying to help KDE in the best way he can. There are many, many users who are annoyed by this bug and Alex is trying to make developers aware that this is one bug that needs to be fixed.\n\nWhile some have complained about bloated context menus being present in many KDE apps, in fact, the main culprit is Konqueror. And fixing it will not require creating and translating new menu entries, but rather just deleting irrelevant ones.\n\nThere is no reason for a selection CM (to pick but one example) to have options for page navigation (that gets rid of Up, Back, Forward, Reload), window/file operations (Open in new window, tab, background tab, add to bookmarks, Open with, K3b)"
    author: "Bob"
  - subject: "kdialog"
    date: 2003-08-24
    body: "I hope other toolkit systems start using some method like this, this way we can for example, have a kde file dialog in openoffice while running kde, a gnome file open dialog when on gnome, or the own openoffice file dialog when running on other desktop that dosen't provide a file dialog.\n\nThis would be good for users I belive, making apps looks as the user expects (because he is working on kde he expect things to look as kde apps).\nLess confusion I belive.\n\nNext step would be making styles/themes compatible between toolkits."
    author: "Iuri Fiedoruk"
  - subject: "Re: kdialog"
    date: 2003-08-25
    body: "I'm afraid that OpenOffice and Gnome apps tend to implement their own file selectors per application, or re-write the existing ones heavily, which is why the Gnome folk are having a hard time creating a new one for their desktops. The Oo one has to be self-written for cross-platform stability, too, so that can't really change, which is a shame.\n\nAll i all, it isn't an easy problem, I'm afraid :-/"
    author: "Dawnrider"
  - subject: "Re: kdialog"
    date: 2003-08-25
    body: "I believe the new file dialog stuff in Gtk 2.4 allowed other file selectors to be used in Gtk/Gnome programs..\n\n(including the KDE one)\n"
    author: "whatever"
  - subject: "Re: kdialog"
    date: 2003-08-26
    body: "Actually, the OOo version for Windows (and the future OSX-native one) do not use the dialogs used in the X Window System version of OOo. Creating a KDE binding will not be too difficult.\n\nYour point about \"GNOME apps\" is also wrong since one of the criteria for being a GNOME app is that one must use the GNOME dialogs. Gtk programs, on the other hand, do tend to make their own dialogs."
    author: "Antiphon"
  - subject: "Re: kdialog"
    date: 2003-08-26
    body: "This new opening of the KDE file dialogs will be good for those wishing to write multi-platform Qt apps. Now, we're not stuck with the basic Qt boxes and don't have to reinvent the wheel.\n\nI hope that kdeinit won't be run all the way, though. That would sort of defeat the purpose of running non-KDE WMs."
    author: "Bob"
---
In <a href="http://members.shaw.ca/dkite/aug222003.html">this week's KDE-CVS-Digest</a>: <a href="http://edu.kde.org/kstars/">KStars</a> is now using a new free star map and the telescope interface gains some new wizards.
KGhostview (PDF/PostScript viewer) now has a thumbnail preview.
A new KHotKeys is in the works. <a href="http://korganizer.kde.org">KOrganizer</a> is improved with work on drag and drop, alarms and todo lists.The <a href="http://www.automatix.de/~zack/kconfedit.png">KDE Config Editor</a> moves along.  The trash icon is cleaned up. KHTML caret navigation is almost completed. The KDE dialogs can now be used by non-KDE applications. And more.
<!--break-->
