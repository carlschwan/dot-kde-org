---
title: "Sun.com:  KDE 3.x on Sun Solaris"
date:    2003-12-30
authors:
  - "jjuhl"
slug:    suncom-kde-3x-sun-solaris
comments:
  - subject: "Stefan Teleman's packages?"
    date: 2003-12-29
    body: "And where are Stefan Teleman's packages? I've read about these recently somewhere...\n\nWhat's the benefit of these (other than their more recent versions)? Are they \"better packaged\" and have some \"extra KDE flavor\" with them? --  /methinks that it is a big waste of time to package these, if Sun does the job the same...."
    author: "Anonymous Coward"
  - subject: "Re: Stefan Teleman's packages?"
    date: 2003-12-29
    body: "<a href=\"ftp://ftp.kde.org/pub/kde/stable/3.1.4/contrib/Solaris\">\nftp://ftp.kde.org/pub/kde/stable/3.1.4/contrib/Solaris</a>\n"
    author: "JC"
  - subject: "Re: Stefan Teleman's packages?"
    date: 2003-12-30
    body: "Well, Sun's versions tend to lag behind somewhat. I'd go with the KDE-packaged Solaris packages instead of Sun's. I think this article is just for promoting Sun's freeware site.\n\nAnd yes, Solaris runs KDE pretty nice. It certainly blows CDE away any day :)"
    author: "Emiel Kollof"
  - subject: "Re: Stefan Teleman's packages?"
    date: 2003-12-30
    body: "Now, I'm using the Stefan Teleman's KDE 3.1.4 packages and it runs very well, only a few problem with dtlogin (it was not appear like an option ;-), resolve with nake-dtlogin from http://www.tiem.utk.edu/~peek/software/solaris/make-dtlogin/ and at least, I have a modern desktop in my Sun Sparc Ultra 5 like in my Linux box :-)\n\nNow, I'm trying to put the screen with 16 bits depth instead the default 8 bits. It's very dificult for a Solaris newbie like me ;-). I try with several howtos but anyone works. I'm still trying."
    author: "Pedro Jurado Maqueda"
  - subject: "Re: Stefan Teleman's packages?"
    date: 2003-12-30
    body: "if you have an ultra 5 with on board graphics, it can only do 8 bit colour depth.\n\nYou need to get a creator 3D or something to get decent colour depth.\n\nCheers,\n\nPaul"
    author: "Paul Mitcheson"
  - subject: "Re: Stefan Teleman's packages?"
    date: 2003-12-30
    body: "Yes, I have an Ultra 5, with on board graphics?. I don't know, but xdpyinfo gives me this information:\n\nname of display:    :0.0\nversion number:    11.0\nvendor string:    Sun Microsystems, Inc.\nvendor release number:    6410\nmaximum request size:  262140 bytes\nmotion buffer size:  256\nbitmap unit, bit order, padding:    32, MSBFirst, 32\nimage byte order:    MSBFirst\nnumber of supported pixmap formats:    4\nsupported pixmap formats:\n    depth 1, bits_per_pixel 1, scanline_pad 32\n    depth 8, bits_per_pixel 8, scanline_pad 32\n    depth 16, bits_per_pixel 16, scanline_pad 32\n    depth 24, bits_per_pixel 32, scanline_pad 32\n\nThe last four lines confusing me, but the important thing is that I have KDE in my Sun Sparc, so I'm happy enough.\n\nOne last question, is kde-i18n-es package avaliable for Sparc? Thanks and sorry for the off-topic"
    author: "Pedro Jurado Maqueda"
  - subject: "Re: Stefan Teleman's packages?"
    date: 2003-12-30
    body: "Hi!\n\nI believe kde-i18n-es is available in the kde-i18n huge\npackage (please correct me if i'm wrong). I did not upload\nthe whole kde-i18n because it's very big, and it wouldn't\nbe useful to force someone to install the whole thing when\nthey are probably only interested in a few languages anyway.\n\nIt's available for download from the source download site(s).\n\nAbout the video card:\n\ntry running:\n\nm64config -help\n\n(IIRC m64config is the video card config utility on Ultra 5).\ni am not sure if this works with an on-board video card, though.\n\n--Stefan\n\n\n"
    author: "Stefan Teleman"
  - subject: "Great, it's working"
    date: 2003-12-31
    body: "Yes, with the command\n\nm64config -res 1024x768 -depth 24\n\nworks perfectly.\n\nThank you very much :-)"
    author: "Pedro Jurado Maqueda"
  - subject: "Re: Great, it's working"
    date: 2003-12-31
    body: "Pedro,\n\nSorry - I stand corrected.  I was sure that the onboard graphics didn't have enough memory for 43 bit colour.\n\nRegards,\n\nPaul"
    author: "Paul Mitcheson"
  - subject: "Re: Great, it's working"
    date: 2004-01-03
    body: "The first generation of Ultra 5's could only do 8-bit.  They then released versions with the PGX24, which is capable of 24-bit graphics, although only at 1152x900.  Above that, it's 8-bit."
    author: "Tony"
---
Corey Liu has written an article about running <a href="http://dot.kde.org/1071436752/">KDE 3.1 on Solaris</a> based workstations. He covers installing KDE, using KDE and running KDE apps under the CDE environment. The <a href="http://www.sun.com/bigadmin/content/submitted/kde_on_sun.html">article is available</a> over at <a href="http://www.sun.com/">Sun's website</a>. Some screenshots are also included.






<!--break-->
