---
title: "KDE PIM Hackfest Summary"
date:    2003-01-24
authors:
  - "dbinner"
slug:    kde-pim-hackfest-summary
comments:
  - subject: "linuxgonz"
    date: 2003-01-23
    body: "Just great!"
    author: "linuxgonz"
  - subject: "ugly icon"
    date: 2003-01-24
    body: "Is it just me, or is the icon for KAddressBook (the phone receiver) really really ugly? Especially next to those cool new ones like Kontact."
    author: "Artem"
  - subject: "Wait for KDE 3.2? ?"
    date: 2003-01-23
    body: "Hi,\n\nThat sounds a little painfull to have to wait for 3.2 - 3.1 will be final in a few days and knowing the KDE team faithfull bug hunting exercise theres sure to ber 3.1.1-3.1.3 or higher! that gonna be b4 we see 3.2 - Now to tell the the truth I don't think i can wait that long!!!  \n\nSince you say there been CVS commits of the code, i'm going ahead and compiling the CVS version, with these new tools i believe KDE is truely ready for the enterprise!\n\nThank you all for your hard work!! YOU MAKE LINUX GREAT!"
    author: "iCEmAn"
  - subject: "Re: Wait for KDE 3.2? ?"
    date: 2003-01-24
    body: "Linux was already great when it ran GNU software the first time.\nLinux was even greater when it ran Apache.\n\nThis KDE thing is making it perfection to many :-)\n"
    author: "Debian User"
  - subject: "HTTPMail "
    date: 2003-01-23
    body: "Maybe this is the wrong place to mention this! I say an excellent app written by Tom\u00e1s Espeleta on http://hotwayd.sf.net This would be excellent to support in KMail client for Home users who might not have a Kolab Server.\n\nJust a thought....\n\niCE\n~~~"
    author: "iCEmAn"
  - subject: "Re: HTTPMail "
    date: 2003-01-24
    body: "From what I read on http://hotwayd.sf.net resp. on http://people.freenet.de/courierdave/ (the project's current homepage) it should already work with KMail. Simply follow the installation instructions at http://people.freenet.de/courierdave/.\n\nOr did you already try to use it and ran into problems? If yes, then please contact us via kmail@kde.org.\n\nBTW, if you just want to fetch mail from your Hotmail account with KMail then read this:\nhttp://dot.kde.org/1035822985/1035828921/1035906330/\n\nRegards,\nIngo"
    author: "Ingo Kl\u00f6cker"
  - subject: "kmail needs to support signing and encrypting"
    date: 2003-01-24
    body: "kmail will not move over the top until it can ingest PKCS12..etc files to perform signing/encryption/verification of email.  The only program that can do this is netscape, and it does not have PDA support.  So I am stuck using 2 apps.  I am not sure why all linux email apps can use PGP, but none of them use certificates from verisign or any other certificate authority."
    author: "Mark"
  - subject: "Re: kmail needs to support signing and encrypting"
    date: 2003-01-24
    body: "http://www.gnupg.org/aegypten/\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail needs to support signing and encrypting"
    date: 2003-01-24
    body: "really cool.  Will this ever find its way into RPM format?  I would love to try it out !!!  \n\n..Thanks for the info"
    author: "Mark"
  - subject: "Re: kmail needs to support signing and encrypting"
    date: 2003-01-24
    body: "It's part of KMail as of KDE 3.1\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: kmail needs to support signing and encrypting"
    date: 2003-01-27
    body: "That depends on the distributors. There are already Debian packages for all necessary libraries. Whether there will also be RPMs from SuSE, Rethat, Mandrake, ... can only be answered by those companies.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Great!"
    date: 2003-01-24
    body: "Keep doing that good work! KDE 3.2 will be great: KPIM, KHTML improvements, Koffice growing up, ...\n\nBut there is a lot of motivations for the future: an integrated multimedia framework with video (arts allowing things like non-linear video editing, ...), avanced framework for image-composing like Quarkz(maybe XRENDER + Kpainter), avanced programming envirotment (multilanguage develop with Kdevelop + UML editing with Umbrello, ...), fully integration with new internet tecnologies and distributed programming (dotGNU -with Qt# and Kde#- or, why not, konqueror XUL support to get rapid remote-applications within KDE)... A lot of dreams for the future... \n\n"
    author: "Guest"
  - subject: "XUL support"
    date: 2003-01-25
    body: "Neat idea ... all you need for XUL is something that will render it ...\n\nThe neatest thing about mozilla is not that it renders webpages ... it's that mozilla *IS* a webpage.  The whole application is XML/RDF rendered by xpcom .... I wonder if hacking an xpcom kioslave would let KDE render mozilla/XUL applications?!!!\n\nEEEEKK!"
    author: "CoolXUL"
  - subject: "Re: Great!"
    date: 2003-01-25
    body: "Maybe that's what Trolltech wants to do in a future:\n- They have a XML format to represent interfaces (*.ui)\n- They have support for a ECMA like scripting language: QTScript\n- You can render ui files dynamically with an library (libqtui ?)\n- They even have support for a component model like COM\n\nSo, the only remaining thing is to distribute the aplications over HTTP and maybe, fill some small holes. \nWhat I wouldn't like from this model:\n- I think .ui is not as clean as XUL. The box-oriented layout is very useful\n- Styles: skins in XUL are described using CSS, another web related technology. I haven' studied this for Qt, but maybe it's not generic enought\n- What happens with other technologies: XBL, ...\n\nIn the other hand, Qt is a wide used library with interesting features; you could compile your interfaces when speed get critical. \n\nAnyway, I think it's interesting. I just imagine myself designing _remote applications_ in my office with little work (forgive .NET by now) _just like web-designing_ (I wouldn't need prototypes) fully integrated in KDE... Wonderful!\n\n"
    author: "treax"
  - subject: "Check mozdev.org"
    date: 2003-01-26
    body: "there's a few projects there that use XPCOM to \"serve\" up XML/XUL for rendering ... eg. there is an Apache XPCOM module that \"serves\" XUL applications, MySQLXPCOM ... etc."
    author: "CoolXUL"
  - subject: "Re: Great!"
    date: 2003-01-26
    body: "I've actually written some code for this sort of thing - eg. it lets you export QListView's as HTML. I remeber talking to Harvord from Troll Tech about it the linux expo in San Francisco a few years ago. The main difficulty in using this to make apps usable over HTTP is latency. The approach I think most successful is to make a headless port of Qt which can do this stuff natively, and make the rendering code generate HTML with JavaScript in it. The script would handle all the stuff that needs to operate with low latency, then things like pressing an Ok button would connect back to the server.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Dates  better called Calendar"
    date: 2003-01-24
    body: "Perhaps the category \"Dates\" should rather be called \"Calendar\". I for my part have hardly any dates at all   :("
    author: "per"
  - subject: "Re: Dates  better called Calendar"
    date: 2003-01-24
    body: "Thanks - you are right. I changed it right now..."
    author: "Guenter Schwann"
---
On the first weekend of 2003, <a href="http://www.intevation.de">Intevation</a>, along with the other companies participating in the <a href="http://www.kroupware.org">Kroupware</a> project, <a href="http://www.klaralvdalens-datakonsult.se">Klar&auml;lvdalens Datakonsult</a> and <a href="http://www.erfrakon.de">Erfrakon</a> organized and sponsored a <a href="http://pim.kde.org/">KDE PIM</a> <a href="http://pim.kde.org/development/meetings.php">Hackfest</a>  in Osnabr&uuml;ck, Germany. The main purpose was to define a roadmap on the future of personal information management in KDE. Berndhard Reiter from Intevation wrote down a <a href="http://pim.kde.org/development/meetings/20030103/summary_report.php">nice summary</a> (included below) for your convenience. Thanks goes to the companies involved for making this meeting possible, and the <A href="http://pim.kde.org/development/meetings/20030103/group.php">developers involved</a> for their participation.
<!--break-->
<p>This is a short summary report about the 
Kroupware/Kaplan Hackfest that took place 
on the first weekend in 2003 in Osnabrück, Germany.</p>

<blockquote>
<p>The overall goal of the meeting was reached with
significant progress being made towards bringing  better integration between the <a href="http://kroupware.kde.org/howto-kolab-kde-client.html">KDE Kolab client</a>
functionality and the upcoming
architecture of KDE-PIM (mainly associated with the <A href="http://www.kontact.org/">Kaplan</a> approach).
All participants developed a common vision about how to proceed and no major conceptual differences remained.  Some technical issues were resolved right away and consensus was also reached on what needs to be done
in the next steps.</p>
</blockquote>

<p>People started to drop in on Friday and left on Sunday afternoon.  A kickoff meeting started the hackfest on Friday afternoon.
To get everyone on common ground, presentations were given.
After a general overview of the Kroupware project,
more technical introductions to the currently implemented KDE Kolab client
and the Kaplan approach followed:</p>

<blockquote>
<p>At the core of the Kolab architecture stands the standards-based IMAP protocol.
Basically, this protocol is used to save all sorts of information on the server
and allowing offline changes which are later synced.
This capability is not specific or restricted to any particular groupware.
The current KDE client implementation based on the upcoming KDE 3.1
makes <a href="http://kmail.kde.org/">KMail, <a href="http://www.korganizer.org/">KOrganizer</a> and <A href="http://www.kaddressbook.org/">KAddressbook</a> save their information
using disconnected IMAP controlled by KMail.</p>

<p>Kaplan basically tries to provide access to the capability
of the KDE components relating to personal information management.
In this sense, it needs all KParts done cleanly in a way that the
single applications can still work standalone
without being embedded in Kaplan.
Kaplan therefore does not add much logic itself or solve
the problem of interaction. It only forces a clean design and 
usage of common configuration and interaction methods.</p>
</blockquote>

<p>After the problem space was explored,
work was done alternately between 
working groups and meetings for coordination and discussions of new findings. 
As natural for an event like this, 
many successful informal conversations also took place.
In the following you can read a few lines on 
what was archived by the different people on this first January weekend.</p>
<blockquote>
<p>David Faure and Daniel Molkentin analysed DCOP and found
that a better general DCOP service starter is needed
which was subsequently implemented.
DCOP error handling got examined and a <a href="http://pim.kde.org/development/meetings/20030103/implement_dcop.php"> some documentation</a>
on how to properly write clean DCOP functions was produced.
KAddressbook was ported as an example.
It is clear that more general DCOP interfaces need to be defined
so that KMail and KOrganizer can act as a Kolab client together.</p>

<p>Tobias Koenig presented the <a href="http://pim.kde.org/development/meetings/20030103/resource_framework.php">new resource framework</a> to the group.
He made more progress on its implementation and started porting libkabc
to the new framework.</p>

<p>Bo Thorsen and Steffen Hansen worked on various bugfixes
of KMail and KOrganiser code and ported most of KMail/Kroupware to HEAD.
They, together with Karl-Heinz Zimmer, answered many
questions about the code they contributed for the Kroupware project.</p>

<p>Lutz Rogowski made some progress towards using KOrganizer as a KPart
and familiarised himself with Kitchensync.</p>

<p>Cornelius Schumacher developed a concept for the iCalender parsing 
and worked on porting KOrganizer to the new resource framework.</p>

<p>Günter Schwann fixed a KParts bug and started on the "imap"-resource
for KOrganizer.</p>

<p>Ingo Klöcker helped porting KMail/Kroupware to HEAD
and did miscellaneous KMail cleanups.
Marc Mutz worked on KMail, especially the IMAP account code
and prepared and conducted several code merges to HEAD.
Marc also helped to prepare the meeting.</p>
</blockquote>

<p>Having many developers on site gave us the good opportunity
to discuss the long standing questions  whether to move KMail, <a href="http://knode.sf.net/">KNode</a>, Korn and related libraries to 
the kdepim module.  The question was approached with <A href="http://pim.kde.org/development/meetings/20030103/moving_kmail.php">structured discussion</a>.  
In a first step, pro and con arguments were collected.
Everybody had to wait for the second step to give
their opinion on how to weight the arguments.
In the end, consensus was reached and the move was favoured. More details on the results will be reported by
the respective developers on the appropriate mailinglists.</p>

<p><i>This report is brought to you by Bernhard Reiter (of Intevation), who coordinated and moderated the meeting. Travel expenses and the hotel accommodations were paid for by Erfrakon, 
Klar&auml;lvdalens Datakonsult and Intevation, 
the companies behind the Kroupware project.
Bandwidth, drinks and rooms were provided by Intevation.</i></p>

<br />

<p>A lot has happened since then: 

<ul>
<li><A href="http://kmail.kde.org/">KMail, <a href="http://knode.sf.net/">KNode</a>, Korn and related libraries have been moved to the KDE PIM CVS module as planned</li>
<li>Kaplan, now called <a href="http://www.kontact.org">Kontact</a> (<a href="http://trolls.troll.no/sanders/kontact/kontact1.png">four</a> <a href="http://trolls.troll.no/sanders/kontact/kontact2.png">screenshots</a> <a href="http://trolls.troll.no/sanders/kontact/kontact3.png">are</a> <a href="http://trolls.troll.no/sanders/kontact/kontact4.png">available</a>), got an improved mechanism for configuration</li>
<li>The <A href="http://pim.kde.org/development/meetings/20030103/resource_framework.php">resource framework</a> is now completely in place</li>
<li>Much <a href="http://pim.kde.org/development/meetings/20030103/merging_kroupware.php">code has been merged</a> from the <a href="http://www.kroupware.org/">Kroupware</a> Branch</li>
<li>A new improved vCard parser has been written and has been committed to CVS along with some testcases for semi-automatic regression tests</li>
</ul>
</p>
<p>Have a look at the <a href="http://pim.kde.org/">KDE PIM homepage</a> for further <a href="http://pim.kde.org/development/meetings.php">status reports</a>. </p>

<p>Hold your breath for KDE 3.2!</p>