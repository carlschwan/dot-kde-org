---
title: "Your Chance to Ask Trolltech"
date:    2003-07-02
authors:
  - "pfremy"
slug:    your-chance-ask-trolltech
comments:
  - subject: "QT for SWT"
    date: 2003-07-02
    body: "Dear Trolltch, SWT is realy a great GUI Library for JAVA.\nOn Linux the GUI-Connection is done with GTK (or x86/Motif)\n\nWhen will you support IBM with a QT Binding?\n\nMike"
    author: "Q to Trolltech"
  - subject: "Re: QT for SWT"
    date: 2003-07-02
    body: "Good question!  I would love to see Eclipse using Qt instead of Motif/GTK."
    author: "anon"
  - subject: "Re: QT for SWT"
    date: 2003-07-02
    body: "Yeah, me too !!!\nEclipse is simply wonderful, now if it only would support QT ... ;-)"
    author: "GengerBreadMan"
  - subject: "Re: QT for SWT"
    date: 2003-07-02
    body: "That should be great, but I think it's impossible due to licensing issues. SWT is CPL, while QT/X11 is GPL. I think it's only possible when TT will make something like a SWT exception in their license."
    author: "Niek"
  - subject: "Re: QT for SWT"
    date: 2003-07-02
    body: "Yummy,\nEclipse on QT/Embedded would be great!!!\n"
    author: "Klaas van Gend"
  - subject: "Re: QT for SWT"
    date: 2003-07-02
    body: "yep, i dream of doing java development on my palm pilot quite frequently...\n\neclipse and qt are opposite ends of the spectrum. eclipse using swt was designed for native widget binding.  as i understand, qt emulates it's widget rendering.  this would put you back to a netbeans type of application.  that emulation requires MUCH MUCH more overhead."
    author: "Mark Lybarger"
  - subject: "Re: QT for SWT"
    date: 2003-07-07
    body: "I don't see why Qt is less native than Motif or GTK widget libraries.. SWT do bind to native (as in non-Java implemented) widget libraries and that is what makes it so much faster than Swing."
    author: "Morten Moeller"
  - subject: "Re: QT for SWT"
    date: 2003-07-08
    body: "They haven't implemented an SWT port to Qt yet because of licensing problems. Qt's license (at least used to) conflict with IBM's Common Public License."
    author: "Curtis d'Entremont"
  - subject: "Re: QT for SWT"
    date: 2003-07-12
    body: "According to the IBM guys on the eclipse mailling list they already have the Qt bindings done, It just can't be release because of the licensing issues."
    author: "Byron Foster"
  - subject: "Re: QT for SWT"
    date: 2003-07-02
    body: "I think they already did an AWT peer implementation for their Qtopia platform.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: QT for SWT"
    date: 2005-07-15
    body: "Qt...Powerful.\nEclipse...Powerful.\nQt+Eclipse...Painful.\nI found it so hard to work on Eclipse with Qt, maybe I'm a rookie after all."
    author: "Sun"
  - subject: "Re: QT for SWT"
    date: 2007-03-23
    body: "I'd like to see Eclipse with a Qt or (even better) KDE GUI, too!\n\nBTW: Netbeans is faster on my Linux box than Eclipse, but on Windows Eclipse is a lot faster. Seems that the overhead of GTK+ is bigger than the overhead of Swing.\n\nNow, I'd like to use Eclipse on Linux, but the look of it is just annoying in KDE.\n\ngargamel"
    author: "gargamel"
  - subject: "Re: QT for SWT"
    date: 2007-03-23
    body: "Regarding licensing, please, IBM, do listen: GPL rules!\n\nAnd as long as there is GPL'ed Eclipse version with a Qt or KDE GUI I'll always prefer Netbeans. It's also faster and much more efficient in current versions. And: The Netbeans modules all work, whereas the majority of Eclipse plugins just suck.\n\ngargamel"
    author: "gargamel"
  - subject: "Would Trolltech"
    date: 2003-07-02
    body: "ever consider selling Qt to Sun or anyone else? Those who work there tell me that many people are tired of Gtk but are now stuck w/it for now.\n\n2) A lot of people and small companies are interested in Qt but are afraid of developing with the free version and cannot afford the licensing for the commercial version. Will Trolltech do something about this situation?"
    author: "Antiphon"
  - subject: "Unfortunately there is GTK2"
    date: 2003-07-03
    body: "Sun and Macromedia are going to like GTK2 ...\n\nGTK2, especially with c++ and c# wrappers shims and what have you, is really good.  And it's still being developped and improved, cleaned and ported to native Win32 and Carbon.  The advantage of C is it a flexible mess that can be easily ported and optimized :-( And ObjectiveC is the best of all :-(   \n\nQt's *massive* advantage in documentation is gradually being eroded too ...\n\nwhat to do??!!\n"
    author: "qtnice_but"
  - subject: "Re: Unfortunately there is GTK2"
    date: 2003-07-04
    body: "> Sun and Macromedia are going to like GTK2 ...\n\nUhm..? Macromedia uses gtk? That's news to me\n\n>GTK2, especially with c++ and c# wrappers shims and what have you, is really good\n\ngtkmm, for what it's worth, still very much feels like a wrapper unfortunatly.. I agree it has been cleaned up from it's quite messy gtk 1.x days (and is now finally usable), but it doesn't feel like a nice, complete coherent solution like Qt does.\n\n> And it's still being developped and improved, cleaned and ported to native Win32 and Carbon.\n\nWhile there is a fork of gtk to use carbon, the main one still uses x11 unfortunatly :(\n\nLast time I saw, gtk still doesn't integrate as well as Qt does on win32 either. It doesn't use WindowsXP Visual Styles, but rather uses gtk-engines :(\n\n> The advantage of C is it a flexible mess that can be easily ported and optimized\n\ntrue, but it's a myth that C++ can't be as portable. after all, Qt works very nice on both MacOS and Windows, and has for years. gtk might have also, but it still doesn't work well on either one.\n\n> And ObjectiveC is the best of all :-( \n\nI personally gind objective-C quite awkward and less powerful compared to C++. I think a lot of other people do, seeing the relative popularity of C++ compared to obj-C. But hey, if I had learned obj-C before C++, I might feel differently. \n\n> Qt's *massive* advantage in documentation is gradually being eroded too ..\n\nAgreed.. time for qt to use doxygen! (which ironically, uses Qt)\n"
    author: "anon"
  - subject: "world domination"
    date: 2003-07-02
    body: "If I was at the head of Trolltech, I would want to see Qt being used everywhere it made sense, pervasively.  Do you, Eirik, have the same attitude?  If so, what kind of steps do you or would you take to increase the market adoption of Qt?\n\nFor example, are you concerned that a distribution such as Red Hat may not install Qt by default?  Are you concerned when commercial companies such as MandrakeSoft, IBM, Sun, Macromedia choose GTK+ over Qt?  Are you concerned that GTK+ provides functionality such as fully-fledged X11 accessibility that Qt doesn't have? Do you pursue efforts such as the recent CE Linux Forum formed by the industry giants aggressively?\n"
    author: "Navindra Umanee"
  - subject: "Re: world domination"
    date: 2003-07-02
    body: "> Are you concerned that GTK+ provides functionality such as fully-fledged X11 accessibility that Qt doesn't have?\n\nQt supports accessibility for Windows. As accessibility is a condition for selling software to the gouvernment in the USA, how important is it for Qt to have this feature?\n\nAnd how do you evaluate the efford of the KDEAP to write a bridge to the GNU accessibility framework (at-spi) for your business?"
    author: "Olaf Jan Schmidt"
  - subject: "Qt and Windows"
    date: 2003-07-02
    body: "Why is there no GPLed Qt 3 for Windows? The lack of this has stopped a number of my friends from using Qt for their free software projects. "
    author: "Bob"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "Yep, this question popped up in my brain too..."
    author: "cies"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "Please ask this question; I was about to post it myself."
    author: "Paladin128"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "I really want to know this one as well.  I would love to use QT for a project that I am planning (free) but I need database access across platforms with grids, etc and thus want it in the GUI toolkit.  Even GTK+ doesn't have that, so I will be stuck with either Java or WxWindows without a free QT3 for Windows."
    author: "kelly"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "I question this as well.  I've got a project intended mainly for a windows audience that I develop on Linux.  I've kept it hobbling along by mainting backwards compatibility with Qt 2.x and the Qt Non-Commerical Edition.  I'm not necessarily stuck on having a GPL version for windows, but I'd *really* like to see more regular releases of the Non-Commercial Edition.  It would also be great to see these releases made with cygwin/gcc compatibility rather than requiring MSVC.\n\nI can understand the concern about maintaining the revenue stream, and that this is low on TrollTech's priority list, but it would be interesting to hear about it.  Heck, I'll bet they could find lots of people who'd be willing to sign an NDA and manage/maintain these releases so that it wouldn't be a burden on TrollTech.\n\nJust my $.02.\n\nKen"
    author: "Ken Cecka"
  - subject: "Please ask this question"
    date: 2003-07-02
    body: "My vote here :-)\n(disclaimer, i don't run windows, i just wonder if a gpl windoze qt would increase marker share significantly for qt)"
    author: "MandrakeUser"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "This question has already been answered in the past, I think it was in my previous interview. His answer was \"When windows becomes Free Software, we will release Qt as free software for windows\".\n"
    author: "Philippe Fremy"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "... which basically means \"Never.\"  ;)"
    author: "Bojan"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "So when did Mac OSX become Free Software?"
    author: "Sean Pecor"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "MacOSX, as a platform, relies a lot more on free software than Windows does."
    author: "tilt"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "Maybe so, although my understanding is that there is also quite a bit of BSD code in Windows. Obviously MacOSX has _more_ BSD inside, since it is based on BSD, but MacOSX is hardly free. I don't have a bone to pick, I was just challenging the reason given for the lack of a GPL'ed Windows Qt. The obvious reason is that Trolltech's bread and butter is the Windows Qt. \n\nMy uneducated guess about Trolltech releasing a GPL Qt for MacOSX, is that it signals an increasing level of cooperation between Trolltech and Apple in the future. In fact I wouldn't be surprised if Apple buys Trolltech, given the vast amount of KDE applications and libraries built around Qt. Apple would be tapping into a massive amount of \"free\" resources for their platform.\n\nSean."
    author: "Sean Pecor"
  - subject: "Re: Qt and Windows"
    date: 2003-07-03
    body: ">  Obviously MacOSX has _more_ BSD inside, since it is based on BSD, but MacOSX is hardly free.\n\nThat's true, but MacOSX's kernel, Darwin, is completely free. It's directly based on FreeBSD. Not only this, but unlike Microsoft, Apple chose to keep the BSD code within OSX (Darwin), under a BSD license, so public code is available. Microsoft chose to keep their BSD-derived code closed source. \n\nWindows is significantly more closed than OSX as a result.\n\nOf course, I agree that the real reason is that TT's main money comes from Windows."
    author: "pret"
  - subject: "Re: Qt and Windows"
    date: 2003-07-02
    body: "Maybe he could elaborate on why Windows not being free software prevents Trolltech from releasing Qt/Win as GPL.  I don't see the connection at all; I think releasing Qt/Win under the GPL would only benefit Trolltech, free software, and if anything ease migration away from Windows to Linux or Mac OS X."
    author: "em"
  - subject: "Re: Qt and Windows"
    date: 2003-07-03
    body: "But Trolltech doesn't (as a company) have any reason to want you to migrate away from Windows.  They don't sell migration, the sell a cross platform toolkit.  Nor does people migrating to Qt without any motivation to buy a copy help TT.  As much as they might like to say, \"Wow, isn't that neat?  They're using Qt!\" it doesn't pay the bills.\n\nBeing pragmatic, you have to ask what benefit would it be to TT to release Qt for Windows?  \n\nYou also have to realize that most of the places Qt is being used are for internal tools -- in these situations companies can quite easily just use the GPL'ed version and simply never do a release.  Many companies will of course still want support, but TT seems to still be overwhelmingly a \"software\" rather than \"services\" copmpany and as such has to have some mechanism for continuing to sell their software.\n\nIf you're developing on Windows, you should be used to paying quite a bit for software -- those compilers don't come cheap..."
    author: "Scott Wheeler"
  - subject: "Re: Qt and Windows"
    date: 2003-07-05
    body: "\"Being pragmatic, you have to ask what benefit would it be to TT to release Qt for Windows?\"\n\nFreeware using Qt increases Qt visibility.  Windows is, by far, the most common desktop platform and even a small number of well-known Qt-based freeware applications would make Qt much more well known, potentially leading to many more sales.  Think about KDE for Windows for example -- wouldn't it be an excellent showcase of Qt's power to the many Windows developers out there?\n\nWhat I mean is that the rationale for a GPL'd Qt/Windows version is the same as for a Qt/X11 one.  Why the asymmetry?\n\n\"But Trolltech doesn't (as a company) have any reason to want you to migrate away from Windows.\"\n\nThey sell a cross-platform toolkit, so I think a desktop market that is more equally split among the different platforms could help their sales a fair bit, too.\n\n\"If you're developing on Windows, you should be used to paying quite a bit for software -- those compilers don't come cheap...\"\n\nNot true.  gcc with mingw32 and Borland C++ 5.5 are both perfectly good gratis Windows compilers."
    author: "em"
  - subject: "Re: Qt and Windows"
    date: 2003-07-06
    body: "I guess most people on Windows, including developers, don't care about things like toolkits. It's just a no-issue over there, because software on Windows just works (or if it doesn't it's not because of the tookit). With a good toolkit the app looks and behaves like a native Win32/MFC app. And if a Windows developer wants a better, higher level programming development, .NET is a logical choice. Qt is very exotic for a Win32 dev.\n"
    author: "Anonymous"
  - subject: "Re: Qt and Windows"
    date: 2004-03-03
    body: "\"Paying Quite a bit for windows compilors\"\n\nNot likely! Ofcourse theres that dev c++ but for me...nothing less then vs.net pro 2003..and no i didnt pay...my school has a msdn license and we get all these amazingly expensive developing apps and windows xp pro and other versions, media edition if we want, for free...and msdn help thing...and when whidbey comes out we get that to! and longHORN!! ALL FREE!\n\nnow your comment is wrong in that you think because people use windows, they actually spend money on software...which is laughable in every way...*well you know what laughter sounds like :D*"
    author: "User"
  - subject: "QT for .Net"
    date: 2003-07-02
    body: "I've seen no recent activities in the QT# area.\nDoes Trolltech consider the .Net platform as a future target for QT?"
    author: "Klaas van Gend"
  - subject: "QT 4"
    date: 2003-07-02
    body: "As many people are ask when KDE 4.0 will be released, this question would be interesting :) \n\nWhen do you expect to be releasing QT 4.0 and what features do you think should be there by then?"
    author: "uga"
  - subject: "Re: QT 4"
    date: 2003-07-02
    body: "Yes, we want to know!\n\nEspecially >>what features do you think should be there by then?<<"
    author: "cies"
  - subject: "People who can't spell"
    date: 2003-07-02
    body: "Does it annoy you when people spell Qt as 'QT' instead of 'Qt'?"
    author: "anon"
  - subject: "Re: People who can't spell"
    date: 2003-07-02
    body: "I don't think they care much about it, but it seems you do ;-) QT, QT, QT :D"
    author: "uga"
  - subject: "Re: People who can't spell"
    date: 2003-07-02
    body: "I'm a programmer, I can't help it.  It kills me."
    author: "anon"
  - subject: "Re: People who can't spell"
    date: 2003-07-02
    body: "I'm a programmer too (better said, I do program), ... but I can't see the problem. There's nothing in between most their classes that says \"Qt\".  It's only \"Q\" that turns up usually (QWidget, QString, Q...). \n\nAs you can see above I make the mistake sometimes even myself. But that's mainly due to the spellings of GTK, KDE, ... and one ends up capitalizing everything. Anyway, have you noticed that their logo has actually a capital lletter \"T\"? ;-)"
    author: "uga"
  - subject: "Re: People who can't spell"
    date: 2003-07-02
    body: "Do you use Unix?  Cuz everything is case sensitive there, just like it usually is in programming.  \n\nThe problem is also that Qt and QT are not the same thing.  One is a GUI toolkit, the other one is also known as Apple's QuickTime. And have _you_ noticed that they used to be called TrollTech and now they are called Trolltech?  Just goes to show that capitalization matters. :-)"
    author: "anon"
  - subject: "Re: People who can't spell"
    date: 2003-07-02
    body: "Actually, \"Qt\" prefix is used for compatibility classes, those\nthat were removed in some release, but are kept in the attic/ \ndirectory of Qt sources to help port apps (i.e. by adding them \nto app sources). For example, Qt2's QTableView, which was removed\nin Qt3 became available as a QtTableView standalone replacement class. "
    author: "Sad Eagle"
  - subject: "Re: People who can't spell"
    date: 2003-07-02
    body: "If you watch the videos on the site you can hear that the original (marketing) name is QT, while the developers call it 'cute'."
    author: "AC"
  - subject: "Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "I spoke with a windows developer recently who is using MFC to build his applications. I was trying to get him to evaulate Qt, because this would obviously make porting his apps much easier. He did, and didn't like it, for the following reasons:\n\n- Qt isn't \"native\" MS Windows, meaning if you install extension plug-ins that eg. let you enter japanese text into widgets, or extend functionality (eg. password databases that automatically fill out login dialogs etc.) these plug-ins will only find widgets developed with the MFC libraries. MFC apps just \"play nicer\" with the rest of the system, he claims.\n- There are \"lots and lots of MFC extensions\" available for just about everything if you don't want to roll your own. Need a connection to a specific database or a table widget with special properties? Chances are that you can license it for ?200 instead of spending a week rolling your own.\n\nNow I don't claim these claims are right (are they?), but if they are, are you considering to change this? If so, how?\n\nThis, and the fact that you still need to license MS Visual C++ to be able to use Qt seems to be a major problem when trying to convert professional developers."
    author: "Jens"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "I don't think there's much they can do about that. Qt is fundamentally an \"emulating\" toolkit, it does all widgets internally. That means less OS integration than you'd otherwise have because it's not a first tier toolkit (except on Linux, where sometimes it is).\n\nChanging Qt to use native widgets would probably mean mostly rewriting it. Abstraction toolkits often have looser semantics as well, so basically I doubt they'll do that.\n\nOTOH MFC isn't so great really, so like most things you have to balance the pros and cons."
    author: "Mike Hearn"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "so then, fundamentally, it's similar to Swing from the java camp?  i've done limited KDE/Qt development circa kde 2.2, and it was nice to code for.  the trouble is, machines aren't really that powerfull and memory still isn't that cheep for using a emulating toolkit as a desktop (consider a swing desktop?).  netbeans and eclipse are prime examples of using an emulator .vs. native widgets.  eclipse wins hands over foot.  course, that's at the slight expense of minor appearance difference and such on different platforms.\n\ni commonly use a kde 3.x desktop outside of work and find it ice to use, i just wanted to pipe in on the emulator .vs. native topic.  i prefer native when available."
    author: "Mark Lybarger"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "Yes, more or less. I guess it depends on the platform, on MacOS it seems to use their style API.\n\n"
    author: "AC"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "The same for WinXP. And for KDE, of course, the Qt style API is \nthe native API. \n"
    author: "Sad Eagle"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "What makes you think that Qt doing the rendering is slower than Windows (or whatever) doing the rendering? I mean OK, on Window, USER may be using some sorts of undocumented hooks in GDI or something. But on X, there is no \nfundamental difference between supposedly 'emulated' Qt approach, and what GTK+, which SWT uses, does. Yet Swing is slow and SWT is supposedly fast; and surely, the same approach that's nice and fast on X would still be nice on other UI systems? Sounds to me like you're taking a poor implementation, and are making conclusions about a whole class of architectures based on it, which is, well, a good way to reach wrong conclusions.\n"
    author: "Sad Eagle"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-03
    body: "Agreed.. the slowness of Swing isn't because it's not native, but rather the fact that it's slow. \n\nQt is much, much, much faster than Swing on Windows. It uses native themeing in XP. \n\nNote that Microsoft themselves don't always use \"native widgets\". See OfficeXP for example."
    author: "pret"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-03
    body: "As far as most people are concerned, Qt is a native toolkit, as is GTK. Linux is a bit odd in having two, but hey, whatever.\n\nBear in mind there's nothing in \"emulated\" that says \"slow\", despite the name.\n\nSWT vs Swing is just because Java is historically not fast at graphics and Swing is a bit of a dog (though in fact we use it at work and the latest versions are not so bad). In fact Qt is pretty fast (as is GTK relatively speaking, though GTK is hit badly by its usage of XRENDER)."
    author: "Mike Hearn"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2006-05-07
    body: "I think the reason why Swing is so slow is that it is completely run under an interpreted language while SWT hands over all the hard work to the machine code to do.  Why should an emulated toolkit be slower than the native if it uses the same primitive drawing routines?  The only speed/slowness will come from the performance of the creation for the graphics to draw.  "
    author: "David"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2007-05-15
    body: "Java started as an interpreted language, but today it is a bit different story. Leading Java VMs employ JIT (Just-In-Time) compilation. That is, frequently used bytecode is compiled to native code on the fly and cached. This enables many optimizations (like in-lining). So today, Java performance is comparable with native software. I saw benchmarks where Java implementation (running on Sun's JDK 6) had outperformed GCC-compiled counterpart."
    author: "JK"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "> Qt is fundamentally an \"emulating\" toolkit, it does all widgets internally.\n\nOnly partially true on WindowsXP and MacOSX. "
    author: "tilt"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "Did your friend respond to your suggestion that Qt would make porting easier (across *NIX, Win*, Mac, PDAs)?  It seems like all of the arguments for MFC are only for features tightly tied to MS-Windows.\n"
    author: "Andy Marchewka"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "My responses as a Qt windows programmer:\n\n1. Qt can now use and provide ActiveX. There are thousands of ActiveX available so it should answer the need for diversity.\n\n2. Using an external control is always risky:\n    . you don't know the quality of the code you use\n    . usually the code you use that way does not do exactly what you need so you end up spending a lot of time understanding it and customising it. As quick as writing it yourself.\n    . writing yourself the code gives you more control when you want to extend it\n\nIt has happened many times to my team that somebody picks up a control \"hey, this guy did just what we need\" and give after two day of trying to make it work the way they want (when it is not simply \"make it work\"). The quality of internet available code vary greatly from very bad to good."
    author: "Philippe Fremy"
  - subject: "Re: Claim: Qt is an \"emulation\" under MS-Windows?"
    date: 2003-07-02
    body: "> This, and the fact that you still need to license MS Visual C++ to be able to use Qt\n\nNo, only if you want to use Visual C++\n\nI see four other qmake targets in the mkspecs dir of my Qt installation\n\nCheers,\nKevin\n"
    author: "Kevin Krammer"
  - subject: "Accessibility"
    date: 2003-07-02
    body: "Does Trolltech have any accessibility expertise which will lead to improved infrastructure in future Qt versions [keywords: AT-SPI (Assistive Technology Service Provider Interface), laws (http://developer.gnome.org/projects/gap/laws.html)]"
    author: "Anonymous"
  - subject: "Qt frontend and backend"
    date: 2003-07-02
    body: "Qt has become an all-included library no longer limited to doing GUI stuff but also offering backend support to databases etc. While doing so will Qt stay monolithic after 3.x or is a more modular design like separating frontend and backend or supporting extensions through plugins considered?"
    author: "Datschge"
  - subject: "Re: Qt frontend and backend"
    date: 2003-07-02
    body: "Also, will Trolltech create a separate qlib that contains QObject and all other non-GUI utility classes from Qt, just like the Red Hat developers have separated out glib from GTK+?  \n\nThis could be useful when developers want to do development with the Qt framework without having to link in all the GUI stuff."
    author: "anon"
  - subject: "Re: Qt frontend and backend"
    date: 2003-07-02
    body: "Have a look at http://www.uwyn.com/projects/tinyq/"
    author: "Anonymous"
  - subject: "Re: Qt frontend and backend"
    date: 2003-07-02
    body: "TinyQ is nice, but it just took the low-hanging fruits. More complicated things like the event loop or QObject are not included, since that would require relatively complex modifications."
    author: "AC"
  - subject: "Re: Qt frontend and backend"
    date: 2003-07-02
    body: "Yep, good question >>will Qt stay monolithic after 3.x or is a more modular design [...] considered?<<"
    author: "cies"
  - subject: "yet another question"
    date: 2003-07-02
    body: "When can we expect to see QTopia, now based on the Qt 2.x series, move to a more recent version of Qt?"
    author: "Andr\u00e9 Somers"
  - subject: "qt-copy/patches"
    date: 2003-07-02
    body: "How do you feel about KDE developers now collecting/publishing patches in qt-copy/patches CVS module against Qt which didn't get included into Qt or a reaction when sent to qt-bugs@trolltech.com after a reasonable time?"
    author: "Anonymous"
  - subject: "Base64/UUEncode-Support via Qt-Class?"
    date: 2003-07-02
    body: "Hi,\n\nI would like to know if there are any plans to implement some Base64/UUEncode classes for transparent {de,en}coding, perhaps based on QMemArray.\n\nSince there is IMHO no real portable library supporting this and e.g. some of the KDE applications have got their own implementations, it would be really great to have something like that directly built into Qt.\n\nbye\nErik\n"
    author: "Erik"
  - subject: "Re: Base64/UUEncode-Support via Qt-Class?"
    date: 2003-07-02
    body: "Take a look at http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/kmdcodec.h?rev=1.45&content-type=text/x-cvsweb-markup\n\n(KCodecs)\n"
    author: "Sad Eagle"
  - subject: "KDE as Qt Platform"
    date: 2003-07-02
    body: "Does Trolltech have any plans or work in progress for making Qt considering KDE as a platform like Windows (meaning using KDE file dialog and other stuff in Qt programs if KDE is installed) as once proposed by your Mathias Ettrich?"
    author: "Anonymous"
  - subject: "Re: KDE as Qt Platform"
    date: 2003-07-03
    body: "Yes, Very good question. "
    author: "anonymous"
  - subject: "Re: KDE as Qt Platform"
    date: 2003-07-03
    body: "Id like to see \"./configure --build-for-kde\" or something like that :P\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: KDE as Qt Platform"
    date: 2003-07-03
    body: "No! We already had that in Qt 2 and it's the wrong solution. That way, you create a cyclic dependency (Qt requires KDE requires Qt...)\n\nYou need a different approach, which will probably lead us towards the need of a proper component model again. We plan to discuss that in n7y btw.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: KDE as Qt Platform"
    date: 2003-07-04
    body: "ooops.. Brain-Bummer(TM) on my behalf :-/\n\n/kidcat"
    author: "kidcat"
  - subject: "Previous Interviews"
    date: 2003-07-02
    body: "Philippe's previous interview from 2001 can be found at http://dot.kde.org/1001294012/\nAnother interesting one by LinuxPlanet: http://www.linuxplanet.com/linuxplanet/interviews/4673/1/"
    author: "Anonymous"
  - subject: "Re: Previous Interviews"
    date: 2003-07-02
    body: "Interesting.\n\nI want to know if Trolltech has offered QtAWT to Sun..."
    author: "anon"
  - subject: "Rendering API Question"
    date: 2003-07-02
    body: "Do you plan to finally bring Qt on par with all the other rendering APIs out there?\nWriting vector graphics apps isn't really possible (integer coordinates,\nno bezier postscript output, no alpha chanel etc etc)."
    author: "Lenny"
  - subject: "QToolBar"
    date: 2003-07-02
    body: "When will Qt get support for using QToolBar widgets in any widget other than the QMainWindow?\n"
    author: "nac"
  - subject: "Re: QToolBar"
    date: 2003-07-02
    body: "That was reported as a bug ages ago and IIRC we got back a WONTFIX response.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: QToolBar"
    date: 2003-07-02
    body: "Actually, it is possible to do that in some convoluted way with current Qt, at least for widgets a couple levels inside a QMainWindow. The TOra database app does something like that with its toolbars... I had to fix a widget rendering bug @ Keramik with that setup, so I know it exists.. Don't have the code at hand ATM, though, may be I still have my test app at home, though..\n\n"
    author: "Sad Eagle"
  - subject: "Re: QToolBar"
    date: 2003-07-03
    body: ">> at least for widgets a couple levels inside a QMainWindow\n\nYes, that's the whole problem.. it should be possible to use QToolBars in _other_ widgets/windows as well ;)\n"
    author: "nac"
  - subject: "Re: QToolBar"
    date: 2003-07-02
    body: "Actually, it is possible to do that in some convoluted way with current Qt, at least for widgets a couple levels inside a QMainWindow. The TOra database app does something like that with its toolbars... I had to fix a widget rendering bug @ Keramik with that setup, so I know it exists.. Don't have the code at hand ATM, though, may be I still have my test app at home, though..\n\nHmm. Actually, it's not the only app that does that. KFileDialog uses \na (flat) toolbar. So does kcmprintmgr.\n"
    author: "Sad Eagle"
  - subject: "Re: QToolBar"
    date: 2003-07-03
    body: ">> Hmm. Actually, it's not the only app that does that. KFileDialog uses a (flat) toolbar. So does kcmprintmgr.\n\nNot sure, but afaik those are not using a QToolBar."
    author: "nac"
  - subject: "Re: QToolBar"
    date: 2003-07-03
    body: "They're using a KToolBar which inherits off a QToolBar. "
    author: "Sad Eagle"
  - subject: "Re: QToolBar"
    date: 2003-07-03
    body: "As far as I can see from looking at the docs the KToolBar doesn't support any widgets other than Q/KMainWindow either..\n\n(and I still don't understand why KDE people thinK it's necessary to Kreate lots of dupliKate Klasses to Ket that silly K in widget Klass names, but that's another topiK :)\n"
    author: "nac"
  - subject: "Re: QToolBar"
    date: 2003-07-03
    body: "Well, it obviously does, since I told you where it is used. What you need is a constructor\nthat takes a general QWidget*; which in turn calls to  \nhttp://doc.trolltech.com/3.1/qtoolbar.html#QToolBar-2\nwith mainWindow = 0. \n\nAnd if you even bothered to look at the KToolBar and QToolBar docs for more than 5 second you'd see why there is\na KToolBar, since it's pretty obvious (hint: count the methods).\n\n"
    author: "SadEagle"
  - subject: "Re: QToolBar"
    date: 2003-07-03
    body: "> and I still don't understand why KDE people thinK it's necessary to Kreate lots of dupliKate Klasses to Ket that silly K in widget Klass names, but that's another topiK\n\nBecause people don't want to wait for similiar functionality to appear in Qt (if ever?!)"
    author: "lit"
  - subject: "Re: QToolBar"
    date: 2003-07-03
    body: "Well, of course it would break the API/ABI so that WONTFIX is pretty logical.. but perhaps it could be done for Qt4?\n"
    author: "nac"
  - subject: "Qt Use in Commercial Consumer Products"
    date: 2003-07-02
    body: "Are you aware of (and can talk about) known companies writing new consumer products or converting there current ones to Qt other than the recently announced Adobe Photoshop Album? Perhaps thanks to Qt's Motif integration offerings a Acroread Reader or Realplayer version with Qt interface?"
    author: "Anonymous"
  - subject: "C++ will not be there forever..."
    date: 2003-07-02
    body: "My question:\nC++ will not be forever, at least its use will decline as newer languages like Java and C# become more popular. Both languages allow platform independent programming, and even today they come with libraries and toolkits that offer even more functionality than Qt.\nSo what is TrollTech going to do? Will TrollTech try to find new markets? Which?\n"
    author: "AC"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Java and C++ are mostly written in C++, and they probably be using system widget libraries such as Qt."
    author: "Bzzz"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "But these are no significant markets. Usually a company wants to expand. But C++ is a shrinking market.\n"
    author: "AC"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Where do you get your information from? \n\nC++ will be there forever (I even now some programmers that still write COBOL programs) and its usage will even increase. Especially since the new standard will include a much broader standard library (see e.g. http://www.boost.org or http://www.cuj.com/documents/s=8250/cujcexp2106sutter/). Even Microsoft realized that (see http://www.gotw.ca/microsoft/index.htm).\n\nSo don't worry about C++. It's alive and kicking...\n\nChristian"
    author: "cloose"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "I have to agree with this.  Java and C# replacing C++ is just typical hype.  Before C#, it was Java and Delphi.  Before Java, it was Delphi and Visual Basic that people claimed would draw people away from C++.  When C++ became popular (early 90's) people were saying it would totally replace C, so why keep support for C in C++ compilers.  Well, guess what. C is still around and in heavy use today, Visual Basic and Delphi are still around but have diminished in popularity, due to Java and C#.  Cobol and Fortran are still in use today as well, even though most people seem to think they have died.  And there are quite a few people out there who still write their software in assembly language (for the most part, though, they're embedded systems programmers).  Java and C# will likely remain in use primarily in small software (embedded software for cell phones and the like) and for web-based projects.  C and C++ will likely remain the languages of choice for work such as os kernel development and professional application development, as well as embedded systems programming.  In fact, C++ is on the rise in popularity among embedded systems programmers due to EC++, the Embedded C++ standard which was finalized in (I believe) 2001, maybe 2002.  C++ will not die, nor will C.  They'll still be in use by the time we're all retired and grandparents (or great-grandparents even, if we live long enough).  There is far too much code out there written in these languages for people (and especially companies) to just say, \"ah, screw C++, I like (insert choice here) better, let's though out all our old code and start over from scratch in our new favorite language.\""
    author: "Michael Dean"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "\"ah, screw C++, I like (insert choice here) better, let's though out all our old code and start over from scratch in our new favorite language.\"\n\nThat, of course, is not the way it works. Will a significant number of new projects in Qt's core market (desktop and PDA/Phone GUI apps) use C++ in the year 2010? I don't think so. \nIt's quite possible that other projects, like Linux and the BSDs, may use C++ (or C+, like MacOS) then. But they certainly won't use Qt..."
    author: "AC"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "\"Will a significant number of new projects in Qt's core market (desktop and PDA/Phone GUI apps) use C++ in the year 2010?\"\n\nWhy not? Traditionally, embedded developers used C. They laughed at OO. But now the fad is Java in embedded devices, and the OO pariah has gone. Enter C++, with all the low level stuff that embedded developers like, and all the OO goodness of Java. Now that GNU and MS have *finally* decided to implement the C++ standard, I see C++ ready to take off in all markets, including embedded."
    author: "David Johnson"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "The first apps in Java and .Net are already out and support for these languages seem to become a standard feature, so it can be expected that support for them in 7 years will be very good. So why should anyone use a lower-level language?"
    author: "AC"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "Funny, the \"first apps\" for Java were out ten years ago. But the \"first apps\" for C++ were out twenty years ago. Support for Pascal was good seven years ago, and look where it is today.\n\nUse whatever language you want to use, but don't make sweeping pronouncements about what the future will be like, because everyone who has done so in the past has been wrong.\n\nWhy should I use a lower level language like C or a mid level language like C++? Because I'm writing software that uses the lower and mid levels of the hardware. Show me a kernel written in .NET. Show me a GUI written in Java that doesn't call native C routines to perform the real work. My company produces hard realtime medical systems doing a heck of a lot of signal analysis. What language should I use?"
    author: "David Johnson"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Yes, C++ will be there forever, but its use will decline, just like Fortran's and Cobol's market share have gone down from >50% to a few percent in the last 25 years. Boost can't change the fact that C++ will be less productive than the newer languages. (I am not talking about the next year, but the next >=5 years when they have matured).\n\nBTW, Stdlib+Boost is pretty pathetic compared to the breadth that the standard libraries of Java and DotNet offer. There's not even a XML parser or basic network support in the standard C++ libraries. Not to mention stuff like database/SQL, printing, HTTP/FTP, IPC/RPC, WebServices, support for directory servers, security (kerberos etc), cryptography, GUI, 2D rendering & image manipulation, 3D rendering, accessibility... \n\n\n\n\n\n"
    author: "AC"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "But we have Qt that does all this stuff."
    author: "Bojan"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Not all of them, but many. That's why I like Qt. \n\nThere are, however, many features that I would like to have and that are not possible in C++, especially those that are made possible by the VM (reflection, garbage collection, method attributes, secure code execution etc). Right now a number of problems with Java and C# keep me from using them for non-custom software - the VMs still feel immature and I also miss features like templates. But in a few years this may be different...\n\n"
    author: "Tim Jansen"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "moc extends c++ to give you reflection for QObject (that's why it's better than template based signal/slot solutions), there are third party libs that give garbage collection (great circle), not sure what you mean by method attributes maybe similar to Qt properties? secure code I have no answer for."
    author: "Don Sanders"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "Moc is a step in the right direction, but AFAIK there is no public API for invoking slots and you can not call constructors.\n\nAttributes in C# are user-defined markers/tags for classes, methods, member fields etc. They do not generate any code, but are part of the classes meta information. This allows you, for example, to mark fields as persistent/transient for persistence frameworks, specify whether methods are transactional in application servers, mark methods as async/one-way for RPC and so on. \n"
    author: "Tim Jansen"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Okay, as I said, the next version of the C++ standard library will probably offer most things on your list (see http://www.linuxworld.com/linuxworld/lw-2001-02/lw-02-stroustrup.html).\n\nBTW, I don't think that a standard library should offer e.g. a GUI library, because the requirements are just too different. For example, Qt is a very good lib for an modest to bigger size application. At work I created an application that just transfers data from A to B and Qt would be way too heavy for this. So I used Fltk.\n\nAlso if the standard lib of Java  is so great (Swing) why is everybody so interested in SWT?\n\nChristian"
    author: "cloose"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "?\nAccording to the article the next version will not have single item of my list. Have I missed something?\n\nA GUI lib is controversial indeed, but both Java and C#/DotNet have one. And Qt is pretty cross-platform as well, so it can't be impossible (even if the integration of the GUI may be not perfect).\n\nSwing is annoying, indeed, but I said >=5 years. Whatever problems Swing has, I am confident that there will be a much better solution for Java GUIs in 5 years.\n\n"
    author: "Tim Jansen"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Hi Tim, :-)\n\nOkay, *most on your list* was probably an exaggeration, but some points are there:\n\n* database --> persistence\n* GUI\n* basic network support --> platform-independent system facilities\n\nSome of the other points are problematic:\n\n* GUI - would have to support small to very large apps.\n* IPC/RPC - standards come and go.\n(small list: XMLRPC/SOAP/CORBA/DDE/DCE/RMI/DCOP :-) )\n* 3D rendering - Direct3D or OpenGL ??\n\nAbout Swing...even if there will be a much better solution in 5 years, Java would still have to support Swing for compatibility reasons (like AWT now). This way Java will become pretty heavy.\n\nChristian"
    author: "cloose"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Certainly all these features have problems. But each JavaSDK comes with a huge selection of well-integrated and systematically-documented libraries for almost everything that I can imagine. DotNet is similar. For me, as an application developer, the reason why something is not implemented is not important. I just see that for Java the functionality is readily available, and for C++ it is not.\nQt+KDE are the only solution that make the situation somewhat bearable for me, and other problems with Java/C# (performance, bad integration with the system, installation hell, bad toolkits, lack of templates etc) are what keep me using C++. I just don't know for how long.\n\nBTW DotNet has an interesting feature for transport-independent RPC (called Remoting). \n\n"
    author: "Tim Jansen"
  - subject: "Re: C++ will not be there forever..."
    date: 2006-11-14
    body: "People use SWT because\na) they think Swing is too complicated or they are not willing to learn a powerful toolkit\nb) they think that you get worthwhile optimisation from linking into the native peers with native methods.\n\nSwing is great. A 2GHz super scalar pipline machine avereages 1 instruction per clock.  Thats 2billion instructions per sec.  Why would you care if there is a interpreted layer. So, in short, because they are confused."
    author: "Ed"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Stdlib+Boost is pretty pathetic compared to the breadth that the standard libraries of Java and DotNet offer. \n>>>>>>>>>>\nThat's because Java and .NET are platforms, not languages. There are lots of libraries for doing the things you mention available for C++. In most cases, these libraries are superior to the ones included in the Java or .NET platforms, since the developers can usually just concentrate on one project, while the Java and .NET guys have to implement a huge range of libraries. In most cases, the time required to find a good library (hint: Google is your friend) is much less than the time it takes to read all the documentation and learn how to properly use it. You still have to do the latter, so eliminating the former isn't really a big savings."
    author: "Rayiner Hashem"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "C++ allows platform independent programming as well. Actually, it allows BETTER platform independent programming than C#."
    author: "David Johnson"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-02
    body: "Currently maybe, but if Microsoft will really abandon backward compatibility for Win32 in Longhorn and make .Net the primary development platform, it could become hard to write anything in C++ at all."
    author: "AC"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "Writing C++ in a .NET centric world won't be any harder than writing UNIX code in a Win32 centric world. "
    author: "David Johnson"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "I have strong concerns that microsoft will ever use C# or the .net itself for it's apps, as they sure dislike the nice decompile feature of the dotnet environment. they will provide it for their nice advertising of \"we support all platform, we are cross platform, cool, better than sun's java\" but won't kill C/C++ as it's the only way to hide their internals from public (which has been ever the main goal). (yes, there are obfascicators, but that only makes it harder, not that hard as to try to \"decompile\" other code"
    author: "Christoph Cullmann"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "I think it's also a simple matter of noone rewriting something as large as Microsoft's\nproduct set in a totally different programming language. People just don't do that.\nFor a good example of how reusing old code is important, take for example the Motif\nintegration support in Qt -- it's there exactly because people want to reuse old code, \nand even when changing it, because they want to migrate gradually. \n\n\n"
    author: "SadEagle"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "Take a look at Visual Studio.Net,\nYou can use several languages out-of-the-box, among which C++.\n\nIf you want to use C++ for .Net applications, there are a few limits, \ne.g. you have to abstain from using multiple inheritance...\n\nBut M$ does support C++ for .Net."
    author: "Klaas van Gend"
  - subject: "Re: C++ will not be there forever..."
    date: 2003-07-03
    body: "How do you come to your conclusions?\nTake a look at:\nhttp://www.aboutlegacycoding.com/Articles/V/V50201.asp\nThere it is said\nThe Gartner Group reports that: \"COBOL is still the most widely used programming language in business with a tremendous impact on the international economy, and on peoples' daily lives as well.\"\n70 percent of the world's business data is processed by COBOL applications.\n9 out of 10 ATM transactions are processed using COBOL.\n30 billion online COBOL transactions are processed daily.\nCurrent investments in software written in COBOL top $3 trillion.\n492 of the Fortune 500 use COBOL including the entire Fortune 100.\nCobol is from the 50s afaik and C isn't much younger and is widely used. Atm C++ offers advantages over JAVA/C# etc. i don't see them disappearing in future.\nGreets,\nRischwa"
    author: "Rischwa"
  - subject: "WebServices"
    date: 2003-07-02
    body: "In the Qtopia feature list I spotted SOAP among the future features. Will there be SOAP support in the regular Qt as well?"
    author: "ano"
  - subject: "Position on OPIE"
    date: 2003-07-02
    body: "What is Trolltech's position on OPIE, the open-source fork of Qt/Embedded.  OPIE is one of three GUI alternatives (as opposed to GPE, the GTK-based GUI, and \"bare\" X) for the Familiar handheld Linux distribution (http://opie.handhelds.org/  and  http://familiar.handhelds.org/).\n\nIs OPIE considered a welcome effort to accelerate the use of Qt on handhelds, or a rogue knock-off?  Is the Qt/Embedded license compatible with the Familiar distribution (ie., supported under GPL)?\n"
    author: "PDA wanna-be"
  - subject: "Component Model"
    date: 2003-07-02
    body: "1. Are there any plans to introduce a component model? In the past such a project was canceled.\n\n2. You should split your library in several parts, like gui, network ...\n"
    author: "Peter"
  - subject: "Re: Component Model"
    date: 2003-07-03
    body: "2. Ever heard of qt plugins? It's already like this.\n\nQt shouldn't ever be split more than this however; for most companies, a complete solution is needed."
    author: "lit"
  - subject: "QT Desktop for Mac"
    date: 2003-07-02
    body: "I have been enjoying the use of the QT Desktop for linux and syncronizing my Zaurus for some time.  When will the QT Desktop be released for Mac OS X?  I really would love to sync my Zaurus with OS X!! \n\nThanks,\n\n"
    author: "Chris"
  - subject: "Re: QT Desktop for Mac"
    date: 2003-07-02
    body: "It already has been:\nhttp://www.osnews.com/story.php?news_id=3929\n"
    author: "Michael Dean"
  - subject: "Splitting Qt into modules?"
    date: 2003-07-02
    body: "Dear Eirik Eng,\n\nWould it be possible to split the Qt library into smaller modules, like all the non GUI classes in one library, all database classes in one library , all gui classes in one library and so on? I would like to use Qt in a non gui project, and would prefer to link to a smaller library.\nAlso there could be a more flexible pricing sceme. the non-gui classes could go out for cheap for instance, or even be LGPL since that functionality is available in other libraries for free anyway (std::string, STL ...).\n\nMarc "
    author: "Marc"
  - subject: "Translucent forms and others"
    date: 2003-07-02
    body: "Qt, is really a good idea, but i have this suggestions:\n\n1.is extremely important that qt humanize the forms, because you in microsoft windows, and mac os X you can make translucent forms and forms with any shape form(like skins)  \n\n2.Qt forms must accept transparent background, in the widgets such textlabels or buttons,because if you use a custom background your application widgets look very ugly\n\n3.the SQL module really need to be simplified and integrated into qtlibs, because as plugin is a nightmare\n\n4.Qt really need more integration with x, and x extensions\n\n5.Qt and kde, need flexibility on styles and windeco, must be great if you can call from qt or kde a form with another style an windeco\n\n6. qt need rewrite the widgets flags, its a miracle works with that flags\n\nfrom venezuela\nrafael castillo \njrch99@cantv.net\n!sorry my english!"
    author: "Jose Rafael Castillo"
  - subject: "Re: Translucent forms and others"
    date: 2003-07-03
    body: "> 1.is extremely important that qt humanize the forms, because you in microsoft windows, and mac os X you can make translucent forms and forms with any shape form(like skins)\n\nYou can with Qt and X11 already.. you can either do this the Qt way (QWidget flags), or the KDE way (setting NET::Override)\n\n> 2.Qt forms must accept transparent background, in the widgets such textlabels or buttons,because if you use a custom background your application widgets look very ugly\n\nWell, this is ofcourse true for any GUI toolkit. Custom backgrounds usually don't work too well with the rest of the desktop :)e\n\n> 4.Qt really need more integration with x, and x extensions\n\nWhich X Extentions? It's intregrated with pretty much all, and when new ones are written, Qt usually supports em quick. Take for example randr, xft2/fontconfig recently.\n\n> 5.Qt and kde, need flexibility on styles and windeco, must be great if you can call from qt or kde a form with another style an windeco\n\nYou can already with other styles. See qapplication (I think)'s setStyle().. \n\nwhy would you need multiple window decs?\n\n> 6. qt need rewrite the widgets flags, its a miracle works with that flags\n\nwhy so?"
    author: "anon"
  - subject: "Difficulty"
    date: 2003-07-02
    body: "My questions would be \"Which part of Qt is the most diffcult to keep portable\".\n\nI mean is it more difficult to have crossplatform UI, or is it the IO stuff, or the database parts, or maybe containers because of lousy compilers.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Qtopia Dual Question"
    date: 2003-07-02
    body: "My main question is, why does TrollTech keep Qtopia Desktop under a proprietary license while making the (assumedly) much more valuable Qtopia itself a GPL'ed package? Wouldn't it make sense to make Qtopia Desktop Free Software too so that it could provide Qtopia-powered handhelds (such as the great Sharp Zaurus) better integration with non-Windows desktops?\n\nSecondary question: assuming Qtopia moves to Qt 3.0 some day (or 4.0) will TrollTech attempt to implement a Binary Compatiblity Module so that Qtopia powered handhelds can remain legacy friendly like Palm OS-based handhelds?\n\n  Thanks,\n           Tim"
    author: "Timothy R. Butler"
  - subject: "Qt IDE Development Platform"
    date: 2003-07-02
    body: "Does Trolltech have any plans to produce a full-fledged IDE developer platform to compete with Visual C Studio or Borland C++ Builder?  On linux KDevelop is close but not quite sufficient for most companies.  "
    author: "John"
  - subject: "Re: Qt IDE Development Platform"
    date: 2003-07-03
    body: "<another Eclipse dream>\n\nQT support (i.e. QT designer) as a plug-in for Eclipse?\n(The C/C++ version of Eclipse)\n\n</dream>\n"
    author: "Klaas van Gend"
  - subject: "Re: Qt IDE Development Platform"
    date: 2006-12-26
    body: "As far as I know Trolltech as no real plan and I doubt they will spend much time on this because the open source community has already taken it over. Three usable IDEs are already available, all of them have the basics requested for an IDE but each have taken a different approach :\n\n<A HREF=\"http://monkeystudio.sf.net\">Monkey Studio</A> : rushes to high version numbers and, most of the time, is the last to offer new features. However it is, until now,the most downloaded.\n\n<A HREF=\"http://qdevelop.org\">QDevelop (a.k.a QIde)</A> : rushes to new features, sometimes without expanding those already present, which results in a decent list of features but lot of frustration as soon as you want a more in-depth feature (like include file support in pro files or several project openned at the same time). Besides the class browsing and code completion are very slow.\n\n<A HREF=\"http://edyuk.sf.net\">Edyuk (elderly DevQt) </A> : has the most extanded variety of features and is by far the most flexible through it fully pluginized architecture, its perspective system and its extended edititng framework. It is also the only one to offer Assistant integration, Designer integration and a widget designed for .qrc files editing. It also has debugging and the next version will bring class browsing and code completion. One reproach remains : long time elapses between releases.\n\nSidenote : I'm the developer of Edyuk so you'd better try them all because I might not be fully objective."
    author: "fullmetalcoder"
  - subject: "Re: Qt IDE Development Platform"
    date: 2007-08-07
    body: "Try also Handcoder's ide for QT (HiQT) that you can find on qt-apps.org"
    author: "Mattias"
  - subject: "Re: Qt IDE Development Platform"
    date: 2007-08-11
    body: "Hi there, I am the author of the \"Handcoder's IDE\" that Mattias mentioned. Those who like it, please spread the word... and send me encouragement to keep it going!\n\n< 2 months old - solid foundation would you agree?\n\nJM\n"
    author: "Jeremy Magland"
  - subject: "Re: Qt IDE Development Platform"
    date: 2007-12-28
    body: "A new beta version of Monkey Studio is out, you can learn more here : http://www.monkeystudio.org\n\nP@sNox,"
    author: "PasNox"
  - subject: "SSL and other crypto"
    date: 2003-07-02
    body: "Can we expect to see crypto classes included in Qt any time soon?  With so many programs now being developed to interact with web services, etc. things like SSL support are becoming increasingly important.  In addition, functions like MD5, SHA1, AES, DES, 3DES, etc. would be very useful in developing more advanced applications."
    author: "Steve Nakhla"
  - subject: "Re: SSL and other crypto"
    date: 2003-07-04
    body: "I'm working on a project called 'Qt Crypto Architecture' (or QCA), basically a simple wrapper library for crypto stuff.  My initial goal is to support SHA1, MD5, AES, 3DES, SSL/TLS (and X509), and SASL.  The functionality will be provided via plugins.\n\nI'm not sure yet how I want to rig this, but I'm thinking of binding very close to Qt, maybe by storing the plugins in the Qt plugin path (under \"security\").\n\nIf you would like more information, send me an email (justin-qt@NOSPAM.affinix.com)."
    author: "Justin"
  - subject: "Qt and the future"
    date: 2003-07-02
    body: "What steps is Trolltech taking to make Qt relevant for the future?  With Microsoft and other companies building in web services support (i.e. SOAP, UDDI, WSDL, etc.) into their development libraries, where is the support for these functions on Linux using a high-quality library?  Non-existant.  As more and more companies (specifically large, enterprise companies) move to Linux, the need for these features will become even greater.  Is Trolltech currently working on things like web service solutions and other emerging technologies so that Qt can become as advanced as the other big names?"
    author: "Steve Nakhla"
  - subject: "GPL for Windows?"
    date: 2003-07-02
    body: "Will Trolltech ever release a GPL or non-commercial version of Qt 3.x for Windows?  I know that there was a 2.x release some time ago, but nothing for 3.x.  There are a lot of people like me who are just hobby developers and would like to develop for Windows.  Kudos on releasing one for the Mac!"
    author: "Steve Nakhla"
  - subject: "Question #1"
    date: 2003-07-02
    body: "Would you employ Bjarne Stroustrup?"
    author: "Marc"
  - subject: "Re: Question #1"
    date: 2003-07-02
    body: "It's really a great idea.\nAnd Qt will become the new standard lib for C++ ^^\nI have a dream...\n"
    author: "joris"
  - subject: "Re: Question #1"
    date: 2003-07-02
    body: "Stroustrup complains that C++ doesnt have a major supporter like Delphi - Borland, Java - Sun, C# - Microsoft...\nC++ is always in the also supported section. Listen, Trolltech! Employ this guy and become THE C++ company. "
    author: "Marc"
  - subject: "Re: Question #1"
    date: 2003-07-02
    body: "Yes, and we are all with you. Come on Trolls, you are big guys now ;-)\n"
    author: "joris"
  - subject: "Re: Question #1"
    date: 2003-07-02
    body: "Hrmmm.. let's look at the facts..\n\nBorland invented Delphi.\nSun invented Java\nMicrosoft invited C#\n\nTrollTech invents C++?? :)"
    author: "loft"
  - subject: "Re: Question #1"
    date: 2003-07-03
    body: "TrollTech invented Qt!\n\nI have no clue of c++ but can code in Qt just fine :-)"
    author: "John"
  - subject: "Re: Question #1"
    date: 2003-07-03
    body: "No but if they employ him they can say: Someone in our Company invented C++ ;)"
    author: "Marc"
  - subject: "Have time"
    date: 2003-07-02
    body: "Heyyy Philippe, have you really ask all the good questions to Eirik Eng by phone ?\nA\u00ef a\u00ef la facture de t\u00e9l\u00e9phone (phone cost) ;-))\n "
    author: "joris"
  - subject: "QT on MorphOS and AmigaOS"
    date: 2003-07-02
    body: "Would you see a significant chance to get QT on this Systems ?\nHave you already heard about theses OSs ?\nDo you see a chance for QT in forseeable time ?\nkrgds,\nFrank"
    author: "Frank"
  - subject: "Fixing QTable"
    date: 2003-07-03
    body: "As you might know, QTable is horrible broken. Is there any chance to get bugfixes for GPL users?"
    author: "Martin"
  - subject: "Partenership"
    date: 2003-07-03
    body: "Hi Eirik Eng,\n\nI am a independant developper, mainly working on database feature (actually creating a database dedicated to C++/Qt -direct support for C++ class, slot/signal within the database object, automatic generation of databse driven application (query/search/result display/editing/creation) from data description,...). Would Trolltech be interested working with external party for better integration.\nTypically, I miss some festure in QObject that make the support quite difficult. Best would be to modify the meta model to accomodate my feature.\n\nstephane\n"
    author: "stephane PETITHOMME"
  - subject: "Native signals and slots"
    date: 2003-07-03
    body: "Have you had, or are you having any plans for implementing support for your signal/slot syntax directly in gcc and thus avoid the moc? Would be neat..."
    author: "Johan Thelin"
  - subject: "Re: Native signals and slots"
    date: 2003-07-03
    body: "I think this once was an April's fools day joke by Bjarne Stroustroup..."
    author: "Klaas van Gend"
  - subject: "Re: Native signals and slots"
    date: 2003-07-04
    body: "I'd vote for this. I think it would be easier and more efficient to use a template based implementation of signals and slots (similar to libsigc++). Most of the data typing would be done in compile time and it would be quickier. \nI also think it would be better to drop obsoletes classes and use the STL's alternatives (containers, ...)"
    author: "Torpedo"
  - subject: "Re: Native signals and slots"
    date: 2003-07-04
    body: "Please no STL classes. They are a complete mess. Stupid method names (why push_back instead of append?), no class hierarchie, different convention for method and class names... \n"
    author: "Anonymous"
  - subject: "Re: Native signals and slots"
    date: 2003-07-06
    body: "This is complete bs. If the worst thing you can say about a library is that you dont like the names, then you either like the library, or you have *no idea* what your talking about. The STL hierarchy is exactly as it should be, there is no reason, given the current scope of the STL why it should have a Java style hierarchy. As a matter of fact, it seems the only concern you have is that it doesnt *look* like Java. Pretty amature criticisms.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: Native signals and slots"
    date: 2003-07-06
    body: "The naming is not a problem because I don't like it, but because it is not consistent when mixed with Qt/KDE naming schemes.\n\nA Java-like hierarchy has several advantages. The most important is probably that it makes using collections as arguments for APIs much more convenient. In Java you can make a function take a (java.util.)Collection as argument, no matter whether it is actually a Map or a Set or whatever. With STL you have to chose one for the API, and the user must then convert his type to the APIs. Or alternatively, you overload the API function for each available STL collection class...\n"
    author: "Anonymous"
  - subject: "Re: Native signals and slots"
    date: 2003-07-06
    body: "C++ uses genertics, which Java recently incorported into Java 1.5, which allows you to parameterize the containers type at compile time, so long as the passed type supports the used interface, it will work. This treads on some of the same ground as the Java heirarchy, and is exaclty how the STL is designed.\n\ntemplate <typename ACType>\nvoid assoc_container_func( ACType& ac )\n{\n   ac.insert( 5, \"something\" );\n}\n\nWill work nicely for sets or maps, or indeed any \"Associative Container\" as defined in the STL manual. If you do some googling, you can read about how \"STL-like\" ideas based on generics are being incorporated into the next Java 1.5.\n\nAs for the reason why its called push_back instead of append: STL is built upon certain concepts, such as Iterators, and there is a method called back() that returns an Iterator that points to the last element of a Sequence: therefore push_back pushes an item to where back points."
    author: "Ryan"
  - subject: "Re: Native signals and slots"
    date: 2003-07-06
    body: "I know, but unfortunately the use of templates \na) makes formal documentation almost impossible. \"Associative Container\" is not a base class or any other C++ construct, it is a STL-description of classes that share common properties. I think the lack of systematic documentation is one of the worst problems with STL... in the end the only formal C++ description is in the methods implementation\nb) The error text that you get when you use the wrong type is bad. You won't get \"argument 2 is not a Associative Container\", you will get something like \"Cant find insert() in argument 2 in line 23\". Almost completely useless, you can only fix it by reading the APIs documentation.\nc) requires that you implement the function in the header\n\n\nI didn't doubt that the STL designers have a logic explanation for \"push_back\" and every other detail of STL. But it is clearly obvious that is has not been designed  the way it is for didactic reasons, or to make the developer type less. It has been designed for a very special form of aesthetics and purity, that may be nice for cod fetishists, but bad for those who just want to get their work done.\n"
    author: "Anonymous"
  - subject: "Re: Native signals and slots"
    date: 2003-07-07
    body: "In case a, its not necessarily clear that formal documentation of the class hierarchy is any more useful than STL style models. I dont find STL, havin read the documentation, less clear than Java. Certainly taken with case b, no one has ever claimed that C++ doesnt give you a lot of rope to hang yourself with. C++ is harder to use, but still more powerful than Java. However well done software will *always* require the programmer know the API well. Indeed Java seems quite popular with lower rung programmers precisely because it is easier. But C++ and STL are still mainstays of large scale programming. \n\nWith case c, the actual languae doesnt require implementing in the header, this is a hack for working with compilers, like GCC, that dont implement the export keyword. A more acceptable version is simply including the cc file. Either way its a C++/Compiler issues, not STL issue.\n\nC++ aint perfect, but STL is an example of what power and elegance can be accompilshed with the language. Other than that, we should probably just agree to disagree.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: Native signals and slots"
    date: 2003-07-07
    body: "The STL doesn't have a class hierarchy on purpose. You shouldn't use STL containers in your API. You should pass iterator ranges (see the algorithm provided by STL). \n\nThis way you can use your methods with several different STL collection classes like you can with Java. Your methods will be even usable with collection classes that are not part of the STL but which were created by 3rd party developers with the needed STL typedefs.\n\nWell, it seems, as if you don't know much about STL. \n\nChristian"
    author: "cloose"
  - subject: "Re: Native signals and slots"
    date: 2003-07-04
    body: "didn't someone just say you don't even get reflection without MOC.  how does your \"solution\" fix this."
    author: "anon"
  - subject: "Re: Native signals and slots"
    date: 2003-07-04
    body: "You're raising a very good issue. Basically, the moc approach and the libsigc++ \napproach raise the classical static vs. dynamic binding tradeoff. The former  is \nfaster, and is compile-time typechecked. The latter is slower, run-time typechecked -- but also more flexible, since you can do things like accessing \nslots by name at runtime, so you can have two things talk to each other without a base interface class (which may be inconvenient to distribute for some reason), or do things like the Qt/DCOP bridge, or the various bindings, etc. \n(Some people would insist that the former approach is more consistent with the C++ design, but I'd say that the C++ design it to provide multiple methods of programming, so the developer can decide which one is more appropriate to the conitions -- which of course means there is nothing wrong with adding an another approach... but I digress)\n"
    author: "Sad Eagle"
  - subject: "KDE"
    date: 2003-07-03
    body: "Simple question: what do the future of Qt, Trolltech's status as a company, Qt's market penetration internationally, competition with Red Hat and GTK+ and Qt on MacOS X have to do with KDE?\n\nGranted, the future of Qt is interesting for developers. But in general, I don't see why this kind of news belongs here. "
    author: "Rob Kaper"
  - subject: "Re: KDE"
    date: 2003-07-03
    body: "What, don't you know?  KDE is a joint venture of SuSE and Trolltech.  They *own* KDE.  That's why Trolltech marketers feel free to use KDE mailing lists to push their stuff.  That's why SuSE employees get to make radical changes to KDE CVS without a peep of discussion on kde-core-devel.  Trolltech houses www.kde.org.  SuSE houses cvs.kde.org, so KDE lives at their whim.\n\nWhat's that?  You're asking me for proof that SuSE and Trolltech influence KDE policies? That's easy to show.  Let's just check the kde-ev-membership or kde-ev-board archives.. oh, they're not open.  Let's check kde-security.. oh, that's not open.  Let's check kde-packaging... oops.  kde-sysadmin?  Same story.  So you're right, I have no direct evidence, only circumstantial.  But likewise, SuSE and Trolltech defenders can't refute anything becuase of the same lack of evidence.\n\nIn closing, yes, I'm fully prepared to get bashed for daring to criticize the employer/sponsor of KOffice RC Luk\u00e1? Tinkl (hm, kdenews.org maintainers: make the site utf8!!!), Konqueror head David Faure, former KDE RC Waldo Bastian, KMail co-maintainer Don Sanders, KDE founder Matthias Ettrich, etc.  I'm fairly likely to be accused of making personal attacks on them.  But look at it this way:  when these two companies hold so much power in KDE, shouldn't they be held to a higher, not a lower, standard?  I think so.  I also think the case for defending them is pretty weak if the only answer is to call any questioning of them a personal attack."
    author: "Neil Stevens"
  - subject: "Re: KDE"
    date: 2003-07-03
    body: "> Trolltech houses www.kde.org. SuSE houses cvs.kde.org, so KDE lives at their whim.\n \nWhat a FUD. A new web server is no problem and I heard Savannah offered to host KDE CVS. You have access to kde-ev-membership archive, so risk it to be excluded and post your proofs. And for kde-security not being open, what a proof for a SuSE and Trolltech conspiracy!\n\n> I'm fairly likely to be accused of making personal attacks on them.\n\nYou're accused of trolling."
    author: "Anonymous"
  - subject: "Re: KDE"
    date: 2003-07-03
    body: "KDE-security not being open is indeed a big problem. I can understand why it's not a public mailinglist, to avoid black hats from subscribing, but I would say every KDE developer entrusted with a CVS account should be trusted on that list, no?\n\nIf I wanted to exploit a security hole in KDE, I could easily write one. There are several applications in CVS which are barely maintained and a malicious developer could easily sneak in commits that wouldn't be properly reviewed. Closing kde-security means that a special group apparently does not have to trust the majority of developers, while the larger group has no choice but to trust the small one. That's simply not a healthy way to organize matters.\n\nSame for the packaging: some developers want to abandon the idealogy that KDE provides source packages only, hence the weird release schedule these days where, except for those on kde-packaging, noone really knows what versions of the code are released and when. In all honesty, I have not tried subscribing to kde-packaging. I ought to, as I release (and package) stand-alone versions of my software in KDE for users of KDE 3.0 and for practical purposes I'd like them to have the same versions that would be part of a normal KDE release.\n\nAs for unreviewed CVS commits: that happened just once, prior to the KDE 3.0 release. Well, an unfinished Crystal (http://bugs.kde.org/show_bug.cgi?id=51048) was pushed through for 3.1 as well, but apparently I'm the only one who thinks it is ridiculous to make an unfinished look the default.\n\nOn one hand, I can't blame companies like SuSE for employing the top KDE developers. On the other hand, since that is probably the best way to influence KDE development, I also can't blame people for having second thoughts about it.\n\nI'm still hoping that Philipe can concentrate on question relevant for KDE in the interview. If that happens, it will be valuable. If I want Trolltech's company profile, I'll visit their website."
    author: "Rob Kaper"
  - subject: "Re: KDE"
    date: 2003-07-04
    body: "\"trolling.\"  What is that word besides you calling names?\n\nAre you sticking your tongue out at me and pointing your finger at me too?\n\nGo home, Anonymous. Your mommy will give you some milk and cookies, and you'll soon forget all about this."
    author: "Neil Stevens"
  - subject: "Re: KDE"
    date: 2003-07-03
    body: "yawn."
    author: "Derek Kite"
  - subject: "Re: KDE"
    date: 2003-07-06
    body: "what do SuSe and Trolltech have to do with certain mailing lists being members-only? last time i checked, the kde-ev list was for members only because, well, it's a place for members of kde-ev to discuss issues that pertain only to kde-ev. it's a small thing called privacy and discretion, and in this case has nothing to do with SuSe and TT.\n\nas for those two companies employing several key contributors to KDE, you're right. that gives those organizations more say in what occurs simply because they have more individuals working on KDE with their interests in mind. those interests, however, are not at odds with KDE. not only that, but the majority of KDE developers are not employed by either of those companies. any group of developers that got together and decided to do a certain amount of work on KDE's code base would have a proportional say in what code gets written. that's how it works.\n\nit isn't like the release dude is always a SuSe or TT hire, either. that would go a long ways to supporting your theory, but the last release dude (Dirk Mueller) wasn't paid by anyone to work on KDE!\n\nnon-Suse/TT hires often overrule the desires/thought/code of SuSe/TT employees working on KDE. by providing reasonable code with reasonable explanation and discussion, there have been times when their better judgement at that time was overruled. the opposite is true as well. most often, the outcome is something better than either person(s) would've come up with on their own, regardless of who pays their wages.\n\nas for who \"owns\" KDE, well... it's all under free software licenses, now isn't it? and a lot of different people own the copyrights of various pieces. you even \"own\" some of those copyrights, Neil. i'm also not certain what you're referring to when you say \"Trolltech marketers push their stuff on KDE mailing lists\"? perhaps you'd care to clarify.\n\nSuSe and TT are both doing great things for KDE by providing financial and marketing support for the platform. they have not taken any special place in the project beyond that which would be afforded anyone who did what those contributors are or have done.\n\nyou seem to care quite a bit about KDE. i wonder how constructive for KDE your innacurate comments are?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE"
    date: 2003-07-06
    body: "As long as you think Trolltech's interests are perfectly aligned with KDE, and SuSE's are, too, then I guess you would not see these companies' pervasive influence as a threat.  But how can we be sure just how influential these people are in release issues when basic lists like packaging and security are closed off?\n\nYou expect us to take it on faith, that these companies can be trusted to work with KDE, but I think trust needs to be earned.  Both are companies that use or abandon free software licensing at a whim.  That kind of behavior is detrimental to the community, and makes them seem less, rather than more, trustworthy."
    author: "Neil Stevens"
  - subject: "Re: KDE"
    date: 2003-07-06
    body: "it's only partially true that one needs to think that their interests are \"perfectly aligned\" with KDE. there is the counter ballancing effect of all the other developers in the project, as well. being Free software, their changes if determined detrimental, can be avoided.\n\nas for having earned trust, i don't know how much more those who work for them and work on KDE could do to help improve KDE beyond where it is. they have provided art, coding, testing and marketing to date that has been in KDE's interest. what, besides their track record with KDE to date, are you looking for?\n\nas for abandoning Free software licensing, i see both companies as actually having moved closer towards Free software licensing over time. i think it is obvious that both TrollTech and SuSe have more Free software under acceptable licenses in their stables today than ever.\n\nas for the packaging list being closed, i believe there are reasons for that. best to ask those on the list, however. but the term \"closed\" is something of a misnomer, as last i knew it was pretty trivial to get yourself subscribed to that list: you simply had to be involved in the packaging of KDE for an OS.\n\nthe security list being closed is another obvious one. there needs to be a place where security issues can be reported, discussed, fixed and those fixes tested before announced and released to the public. full disclosure is good, but it needs to be accompanied with full fixes too. ;-) those subscribed to the security list are those who are directly involved in ensuring these matters get taken care of in a timely manner with the best results possible for the users. if security flaws were reported and not fixed, i'd be concerned. but KDE seems to be doing quite well when it comes to response times and effectivity to major security issues.\n\ni agree that vigilent and careful attention should be payed to interests that get involved with KDE, be they corporate, communal, or individual. i don't believe in unwarranted boat rocking that only makes participation less attractive to those who have and are doing a great job. =)"
    author: "Aaron J. Seigo"
  - subject: "Whats with qt-ada bindings"
    date: 2003-07-03
    body: "Will there be any ada binding for qt. \nAda is a very powerfull language, and f.e. very big in france.\nFor a project with client + server in Ada we did need a\nGUI. So we had to use the ada gtk bindings. But\nthat was very frustrating at much points.\n\nThere was a project on\nhttp://sourceforge.net/projects/qtada/\nbut its dead since 3 years without\nany  output.\n\nThanks \nEnno"
    author: "ebartels"
  - subject: "Re: Whats with qt-ada bindings"
    date: 2003-07-03
    body: "Actually I've been working in Ada bindings for Qt.  They're in a very early state, though.  Currently you can instance most Qt classes and call most methods, but you can't derive classes, which disallows many possibilities. Also there's no documentation whatsoever and I'm not ready to release the binding generator I'm using to create them.  I'll try to finish the bindings if I've got time.\n\nBy the way, I'm not entirely sure Ada is the best language to program a GUI in :-)."
    author: "Miguel"
  - subject: "Re: Whats with qt-ada bindings"
    date: 2003-07-04
    body: "> Actually I've been working in Ada bindings for Qt\nThats really great. I hope we will hear more from you!\nI there any way to help you?\n\n>By the way, I'm not entirely sure Ada is the best language to program a GUI in :-).\nThats right. But Ada is a great language for\nembedded and safety critical systems. It has the stronges cast checking \ni'v seen, and has much allready inside like tasking.\n"
    author: "ebartels"
  - subject: "Re: Whats with qt-ada bindings"
    date: 2003-07-05
    body: "\"I hope we will hear more from you!\"\n\nI think when (if) the bindings are ready to be released I will announce them to the kde-bindings mailing list, so you should watch it if you're interested.  Don't hold your breath though, it will take probably at least a couple months at the very minimum."
    author: "Miguel"
  - subject: "Real upgrade to the desktop?"
    date: 2003-07-03
    body: "So where is KDE that could be compiled with Qt-Embedded only?\n\nThat way people could stop using the bloatware everyone call xfree86.\n\nAnd for the real upgrade where are these features:\n\n  1. Quartz Extreme-style OpenGL'ed GUI\n  2. Expose-style windowing\n  3. Fast user swiths (as waiting for multithreaded xfree86 is just stupid)\n  4. Other eye-candy from Mac OS X... ;-)\n\nSo ok, I'm very influenced by the Mac OS X 10.2 (and 10.3 preview), and I think that should be the goal for the Qt/KDE-4.0 as it's the only really cool OS around at the moment and why put your goals any lower?\n\n\nPs. How's cooping with Apple coming along (even possibility)?\n\n"
    author: "Joe Average"
  - subject: "Re: Real upgrade to the desktop?"
    date: 2003-07-03
    body: ">Ps. How's cooping with Apple coming along (even possibility)?\n\nLet me see... no comments about their patent on the bytecode interpreter of the freetype thingy, still suing people that try to imitate any of their styles in their (free, for non-profit) projects,... to say short: \"I'll take as much of yours, but I won't give you back anything if I can\"\n "
    author: "uga"
  - subject: "DirectFb support"
    date: 2003-07-03
    body: "Will Qt ever gain support for DirectFb, so one could run a fast desktop without the need of X11? It would make things like transparency much easier, and seing GTK allready been ported, it would be very nice to have Qt ported as well so we can run KDE on DirectFb."
    author: "Andr\u00e9 Somers"
  - subject: "Re: DirectFb support"
    date: 2003-07-03
    body: "Why do you think that adding maybe 5% of extra-functionality to XFree is more difficult than to write something that does maybe 70% of what XFree does?\n"
    author: "Anonymous"
  - subject: "Re: DirectFb support"
    date: 2003-07-03
    body: "Because it can save you a lot of overhead you don't need for most desktop systems. I mean: how many desktop users are actually running applications on other computers who's output they redirect? Not many, I'll warrant. The architecture of DirectFB is different from that of X, making graphics for the desktops and stuff like hardware acceleration much easier."
    author: "Andr\u00e9 Somers"
  - subject: "Re: DirectFb support"
    date: 2003-07-03
    body: "The majority of users runs applications with different permissions. Usually with root and their own user on the same screen. My guestimate of the effort of running X11 also over TCP instead of only named pipes would be 100 lines of code (out of > 1 million). And even the multi-user capability should be far below 1% of the overall code...\n\n\n\n\n\n\n\n\n"
    author: "Anonymous"
  - subject: "Usability"
    date: 2003-07-03
    body: "1. The kde usability project did some work on QT designer. However it was complained that nobody of the developers recognised it.\n\n2. What is the position of Trolltech about the planned introduction of EU software patents? \nhttp://swpat.ffii.org\n\n3. Will QT be integrated to the .NET implementation of Mono\n\n4. QT 4 planned features."
    author: "Bert"
  - subject: "Qt changes"
    date: 2003-07-04
    body: "1. Will you switch to Xr in Qt 4 or ever? \nGTK+ is planning to use it for cross platform rendering. Maybe its not developed enough to say atm. \n\n2. Resolution independence - \nWill Qt4 have any moves towards helping developers handle ultra high res devices - ie digital paper.\nThose 12 pixel scroll bars will be hard to pick out at 1200 dpi. \n\n3. Qt Designer modularisation - \nWill Qt designer be split up into components so that the relevant parts can reasonably be embedded in Kdevelop and the KOffice products - ( this is needed to match MSOffice.) - and not bring along the sicko MDI baggage... \n\n4. QSA. \nIs the licence interpretation for QSA incredibly restrictive? If I made eg KSpread have QSA scripting architecture, and someone made a QDialog using the exposed Qt APIs, this would place their entire spreadsheet under the GPL. This is clearly entirely unacceptable to many large organisations who love to throw spreadsheets at each other with big NDAs attached. Is it your position that a QT scripting architecture can never be non viral GPL to documents? Ie is it illegal to view HTML forms in Konqueror, which end up calling Qt widgets? I assume so... this is a very important question.. \n\nAlso, will the QSA support other languages than ecmascript? ie python.. also, why not use KJS? \n\n5. Do high profile, corporate open source apps going to GTK+ (Eclipse, Mozilla, ) purely for the LGPL ever make you wonder about your licencing? Do you ever wish IBM or Apple would buy you and LGPL Qt so you could *really* take over the world with your superior technology? \n\n6. How much work do Trolltech put into X11 performance? Do you ever contribute to XFree86? \n\nCheers, Rob"
    author: "rjw"
  - subject: "SCO's Canopy Group also a Trolltech investor"
    date: 2003-07-05
    body: "This is a very pertinent question even though it is political and not technical.  I really am curious what TT thinks, although I doubt you can get a candid answer in a public interview.\n\nEveryone should know about the SCO suit against IBM which is spreading heaps of FUD about Linux and free software more generally.  Besides nebulous contract claims in the suit against IBM, they have very actively claimed that Linux is tainted by code or concepts that belong to SCO.  It all seems to be a desperate attempt to extort money by a company whose products are now totally uncompetitive.  The major power behind SCO is a technology investment firm called the Canopy Group.  This firm is clearly behind SCO's recent strategy and has worked to arrange legal council and make other management decisions.\n\nFinally the connection to Trolltech: The Canopy Group and SCO own a 5.7% stake in TT.  This is not a particularly large portion of a company that is 64.7% employee-owned.  What influence does the Canopy Group have on management of TT?  What kind of privileged information are they entitled to as investors?  Are any other owners of TT uncomfortable with the Canopy Group's position in the SCO case?  Would it even be possible to buy out the Canopy Group's stake?  Any discussions about that?"
    author: "Morris Q"
  - subject: "Re: SCO's Canopy Group also a Trolltech investor"
    date: 2003-09-03
    body: "I'm too concerned about the origin of TrollTech money resources. What happens if the canopy takes control over TT an then say \"KDE is our IP\"?.\nIf this is just my mind flying away i wanna be sure not to be sue by SCO.\nBy the way I'm from M\u00e9xico and the Law here permit us to sue SCO upon a\n\"Organized Gang Operation\" = \"Crimen Organizado\" style with blackmailing customers\nbefore they pay a \"nonexistent\" service invoice.\n"
    author: "Lito Steel"
  - subject: "Re: SCO's Canopy Group also a Trolltech investor"
    date: 2003-09-03
    body: "Canopy does not own TT."
    author: "ac"
  - subject: "Re: SCO's Canopy Group also a Trolltech investor"
    date: 2003-09-03
    body: "How should they? Unlike Linux there is no unreleased Qt source code that KDE developers could 'steal' code from.\n\nTrolltech could only stop the development of the free qt edition, but that's what the Free Qt Foundation is for. The GPL'd Qt version would then become BSD licensed."
    author: "Jeff Johnson"
  - subject: "Re: SCO's Canopy Group also a Trolltech investor"
    date: 2003-09-03
    body: "TT is a private company. They can control who invests in them. I don't think they are too concerned with Canopy/SCO's 5.5% investment in TT."
    author: "anon"
  - subject: "Qt Interview"
    date: 2003-07-08
    body: "does the conversation gonna be recorded ?\n\nI think we would like to heard this conversation by our-self.\n\n"
    author: "somekool"
  - subject: "Re: Qt Interview"
    date: 2003-07-09
    body: "it is going to be recoreded but it is going to be very probably in french :-)\n"
    author: "Philippe Fremy"
  - subject: "When?"
    date: 2003-07-12
    body: "When will ther be better integration with KDE?"
    author: "Ale"
  - subject: "Huh"
    date: 2003-08-01
    body: "This article is already 1 month old, and I've not seen a interview yet. Is it coming soon?"
    author: "Niek"
---
Thanks to <a href="http://www.kde-france.org">KDE France</a>, I have the chance to conduct a phone interview with Trolltech CEO Eirik Eng. If you have questions on <a href="http://www.trolltech.com/products/qt/">Qt</a> and <a href="http://www.trolltech.com/">Trolltech</a>, now is the time to ask! I will harvest questions from <a href="http://www.linuxfr.org">LinuxFr</a>, <a href="http://www.kde-france.org">KDE France</a> and of course here.  A possible set of topics includes:  the future of Qt, Trolltech's status as a company, Qt's market penetration internationally, competition with Red Hat and GTK+,  Qt on MacOS X (ed: <a href="http://www.osnews.com/story.php?news_id=3929">recent OSNews article</a>).  If you have any other ideas, please be quick to voice them as the interview time is imminent.
<!--break-->
