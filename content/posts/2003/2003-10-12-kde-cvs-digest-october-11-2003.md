---
title: "KDE-CVS-Digest for October 11, 2003"
date:    2003-10-12
authors:
  - "dkite"
slug:    kde-cvs-digest-october-11-2003
comments:
  - subject: "Woohoow! Another KDE CVS digest! :p"
    date: 2003-10-12
    body: "I was waiting for this to appear! Thanks! ;o)\nFirst reply, jaj! :p"
    author: "Verwilst"
  - subject: "Thank you"
    date: 2003-10-12
    body: "Thanks for the updates, jsuta  few questions though.\n\n1. Is Amarok in KDE and how is it differnt from, kaboodle, noatun, Juk, and other KDe players?\n\n2. Is KolourPaint better than other apps like Kpaint or Krita?\n\n3. For people runnign CVS, does the KDE desktop finally align icons to grid and not by the text length? This is my biggest beefs with the Kdesktop."
    author: "Alex"
  - subject: "Re: Thank you"
    date: 2003-10-12
    body: "Look at amarok's web site and try it, to answer your first question.\n\nhttp://amarok.sourceforge.net/\n\nIt aims to be as simple as possible in terms of its interface, to let people manage simple playlists with drag and drop and play those files. It looks a bit like a cross between Noatun and Xmms."
    author: "Tom"
  - subject: "Re: Thank you"
    date: 2003-10-12
    body: "As written in the CVS commit comment, KolourPaint is not supposed to replace Krita. As for KPaint, may be, only the future will tell. \n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Thank you"
    date: 2003-10-12
    body: "Hi all !\n\nI wander if there is no licensing problem with KolourPaint : it is released under a BSD style license. But, if I am not wrong, the KDELibs are licensed under the LGPL.\n\nIs it allowed to downgrade the license this way ? Or at least, this is not allowed to use the original BSD license, only the modified one, I think.\n\nBy the way, I want to tell that I think that it is a very good idea to have a program which can compete with The Gimp on the simplicity front (not the functionnalities, obviously) and with Krita and friends, which are heavy since they rely on the KOffice libs. Although I would have preferred that an already existing project like KPaint had been improved instead. I know there is a lot of work to do with it to achieve a good concurrent to Microsoft's Paint, but it is worth of it as it is already included in the official KDE releases.\n\n--\nChucky"
    author: "Michel Nolard"
  - subject: "Re: Thank you"
    date: 2003-10-12
    body: "Just some small comments:\n- krita does not really depend on KOffice libs. It would be pretty easy to remove all dependecies on KOffice libs. But then, is this usefull for a big app like Krita?\n- I worked a little bit on kpaint and no, it does not make sense to use the kpaint codebase. It does not even use the document - view coding paradigma. I tried kolourpaint and it looks very nice. Undo/redo is great. I definitely think this should be the future kpaint. It looks very promising and has nearly all the functionality a basic painting app should have. And I am sure it is pretty easy to code things like a star tool etc. (which I did for kpaint to get a better understanding of the code).\n\nIt is really time that we get some good painting apps for KDE. It's a pitty that there is no Kimp. On the other hand, Gimp 1.3 looks pretty cool and it is probably quite a lot of work to code something like this. But the base is there with krita. It is just a matter on how many people want to work on krita. It is not very useful from an end user perspective (but try to create a new document, go to layers->insert image into layer and insert to images. Then go to layers->properties and change the opacity of the upper layer to 50%. Looks really cool. The code works, just painting tools are missing;-("
    author: "Michael Thaler"
  - subject: "Re: Thank you"
    date: 2003-10-12
    body: "> But, if I am not wrong, the KDELibs are licensed under the LGPL.\n\nSince when did a painting app become a core KDE library?"
    author: "anon"
  - subject: "Re: Thank you"
    date: 2003-10-12
    body: "As for number 3,\n\nOn my KDE 3.1.2 desktop :\n\nright-click on desktop > icons > align to grid"
    author: "blah"
  - subject: "Fake, crappy align to grid"
    date: 2003-10-12
    body: "The grid is not true, it is jsut defined by text, and icons are spaced unevenly, there are lot sof problems with that.\n\nTry Windows XP or GNOME 2.4 to see what I mean."
    author: "Alex"
  - subject: "Re: Fake, crappy align to grid"
    date: 2003-10-14
    body: "Looks even here, given that the text wraps."
    author: "David Faure"
  - subject: "Re: Fake, crappy align to grid"
    date: 2004-09-02
    body: "When the icon size is 48 there is a lot of space between icons."
    author: "soda"
  - subject: "KolourPaint info"
    date: 2003-10-12
    body: "Does anyone have more information on KolourPaint? I'd be interested to know more about what kind of features the author(s) plan(s) to implement in this program. One feature I'd love to see would be a simple plugins framework that allows plugins to be written in various languages (C, Python and Perl being the obvious choices) to allow people to extend it into the realms of PaintShop Pro without impacting on the core philosophy of the app, which seems to be simplicity.\n\nOne thing I liked about PaintShop Pro, and that my girlfriend and others who aren't interested in The GIMP's advanced features miss, is the simplicity of PaintShop Pro coupled with the ability to do some funky things (like colourise)."
    author: "Tom"
  - subject: "Re: KolourPaint info"
    date: 2003-10-12
    body: "Paint shop pro is NOT a paint program, it is more like Photoshop.\n\nA paint program would be Corel Draw, Macromedia Freehand, or Adobe Illustrator"
    author: "Alex"
  - subject: "Re: KolourPaint info"
    date: 2003-10-12
    body: "I think you're confusing painting and drawing.  Programs that edit pixels, such as Photoshop, KPaint, and the Gimp are painting programs.\n\nPrograms that edit vectors, like Corel Draw, Illustrator, and Karbon14 are drawing programs."
    author: "Burrhus"
  - subject: "Re: KolourPaint info"
    date: 2003-10-12
    body: "I would personally consider that PaintShop Pro is in the league were we want to put Krita. (For me PSP has also advanced features like Gimp. (I am not trying to count which has more features or more filters or so on, especially as I have never used any PSP after version 6.))\n\nKoulourPaint is just supposed to be a simple paint program, not one with multi-layers, multi-colour-models, multi-... \n\nAs for bindings, this is definitively a Krita thing, as all KOffice applications are supposed to be usable with DCOP. (I suppose that right now, Krita is not DCOP-aware yet.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KolourPaint info"
    date: 2003-10-14
    body: "> Does anyone have more information on KolourPaint?\nSee either: http://tinyurl.com/qv53\nor: http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/kolourpaint/README\n\nAlternatively, feel free to email me about anything related to KolourPaint.\n\n> I'd be interested to know more about what kind of features the author(s) \n> plan(s) to implement in this program. \nSee either: http://tinyurl.com/qv4v\nor: http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/kolourpaint/TODO\n\n> One feature I'd love to see would be a simple plugins framework ...\n> to allow people to extend it into the realms of PaintShop Pro without \n> impacting  on the core philosophy of the app, which seems to be simplicity.\nSupporting plugins would be a great idea!  I think I'll code it after 1.0.\nNot quite sure, ATM, how this will work though (pass the plugins QPixmap's?).\n\n"
    author: "Clarence Dang"
  - subject: "Juk and Ogg files"
    date: 2003-10-12
    body: "Is any one else having trouble with ogg files and juk or is it just me?\nSlightly more on topic Yippee for pmu support in Klaptopdaemon."
    author: "Sean"
  - subject: "Re: Juk and Ogg files"
    date: 2003-10-12
    body: "Ogg in Juk: http://bugs.kde.org/show_bug.cgi?id=64024"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "kate"
    date: 2003-10-12
    body: "i wonder whether the code completion stuff in kdevelop could be turned into a plugin for kate because i would find that most cool."
    author: "not_registered"
  - subject: "Re: kate"
    date: 2003-10-12
    body: "kate is a plugin of kdevelop.\n\nIf you love kate so much use kdevelop. THERE IS NO DIFFERENCE!\n\nAnd you get build tool  management, documentation browser, cvs integration AND code completion for free!\n\nHave fun"
    author: "Amilcar Lucas"
  - subject: "Re: kate"
    date: 2003-10-13
    body: "i would say there was quite a difference ;P"
    author: "not_registered"
  - subject: "KolourPaint"
    date: 2003-10-12
    body: "Can someone please post a screenshot from KolourPaint."
    author: "MaX"
  - subject: "Re: KolourPaint"
    date: 2003-10-13
    body: "Heres one. Its very similar to MS paint in terms of look."
    author: "norman."
  - subject: "Re: KolourPaint"
    date: 2003-10-14
    body: "And here's another one: http://kolourpaint.sourceforge.net/screenshot1.png\n\nYou can sweep your Elliptical Selections by holding down Shift :)\n"
    author: "Clarence Dang"
  - subject: "Krita"
    date: 2003-10-12
    body: "Does krita compile in current state ?  I would give it a try if it is possible."
    author: "LC"
  - subject: "Re: Krita"
    date: 2003-10-13
    body: "It compiles fine, but isn't usable for anybody. \n\nIt has a pretty advanced gimp-like architecture, but very little user tools are implemented. "
    author: "anon"
  - subject: "Re: Krita"
    date: 2003-10-13
    body: "TuxPaint will beat it out!"
    author: "Leena"
  - subject: "KDM still broken???"
    date: 2003-10-13
    body: "This is killing me... KDM is still broken... I'm on Gentoo running fresh CVS builds and KDM will come up but cannot start a users session (will just keep the background on and only the cursor will show after kdm attempts to log the user in)\n\nAny ideas? Or workarounds?\n\n-Mike"
    author: "proto"
  - subject: "SOLVED - Re: KDM still broken???"
    date: 2003-10-13
    body: "simply use:\n\ngenkdmconf --no-old\n\nAnd its good to go!\n\n-Mike"
    author: "proto"
  - subject: "Kartoffen"
    date: 2003-10-13
    body: "--\"\"The problem is that probably the DB is only in english, so kartoffen would mean nothing'. \"\"--\n\nBut, what's the meaning of \"kartoffen\"..??"
    author: "Ingo Gonzales"
  - subject: "Re: Kartoffen"
    date: 2003-10-13
    body: "> But, what's the meaning of \"kartoffen\"..??\n\nI guess, that it should have been Kartoffeln, which is the german word for \"potatoes\". At least, it would make sense, as it is was written in a Mail about KRecipes.\n\nBenjamin."
    author: "Benjamin Traut"
  - subject: "Re: Kartoffen"
    date: 2003-10-13
    body: "Sure, that's what I meant.  Argh, I must review my german ;) I even wrote it in lowercase! \n\nThe actual problem is that if you want to search for  the property table of the ingredient, you could just easily look for \"apple\" in english and get its properties, but guess how that would work if I wrote \"pom\", \"sagar\" or \"manzana\" instead.\n\nWell, time will tell what we can do about it..."
    author: "uga"
  - subject: "KolourPaint: Finally!"
    date: 2003-10-13
    body: "I've been looking for an app like this for a long time: something easy to use, but has basic funtionality like flip, rotate, zoom, undo/redo, etc.  Sadly, I've even been deperate enough to run mspaint in Wine just to get something like this in the past.\n\n\n-Karl\n"
    author: "Karl Garrison"
  - subject: "Plans for GPL'ed security packages?"
    date: 2003-10-15
    body: "Are there any plans for moving away from non-GPL security packages like OpenSSH and OpenSSL to GPL'ed versions like:\n\nopenssh -> lsh [http://www.lysator.liu.se/~nisse/lsh/]\nopenssl -> gnutls [http://www.gnu.org/software/gnutls/]\n\nIf I recall correctly there were some discussion about them in the past, and were just wondering how is the situation with them?\n\nIs there plans for stuffing that viki to kdeaccessibility? IMHO it should already be on the kde-3.2!\n\nBut where is software for braille-terminal users? Like brltty [http://www.mielke.cc/brltty/] only difference being that it should be for the Qt/KDE... ;-)"
    author: "Nobody"
---
In <a href="http://members.shaw.ca/dkite/oct112003.html">this week's KDE-CVS-Digest</a>:
CSS and other bugfixes in <a href="http://www.konqueror.org/">Konqueror</a>. <a href="http://amarok.sourceforge.net/">Amarok</a> gets a DCOP interface. Two new applications: Viki, a visual keyboard, and <a href="http://sourceforge.net/projects/kolourpaint/">KolourPaint</a>, yet
another paint program. Klaptopdaemon adds PMU support, and now compiles on your S/390.
<!--break-->
