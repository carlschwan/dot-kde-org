---
title: "KDE-CVS-Digest for July 18, 2003"
date:    2003-07-18
authors:
  - "dkite"
slug:    kde-cvs-digest-july-18-2003
comments:
  - subject: "Derek"
    date: 2003-07-18
    body: "Who is Derek? I want to see a picture of him ;-)...\n "
    author: "anon"
  - subject: "Yay!"
    date: 2003-07-18
    body: "I got second post!  Yay!!!"
    author: "That guy!  But who is that guy?"
  - subject: "Derek, don't ask about Quanta"
    date: 2003-07-18
    body: "Hi Derek,\nI thought I'd post this here instead of in an email... don't ask about Quanta. I've done little but discuss stuff on the list this week, bring in some people and try to keep on top of what's happening. So if you ask all I will likely remember is that I need a vacation. ;-)\n\nOkay, a little. KFileReplace is in our CVS for development and test purposes though it should be submitted to kdeutils once all the fixes are in. If you ever wanted to do find and replace on the next level this app kills! We're about done with ColdFusion support and in work on Zope and Javascript. We're adding a bunch of XML tools as well as new templates and I'm setting up extensive Movable Type Blogging support. I did promise Wil Wheaton it would be out... when did I say? Better late than never. Marc Britton just added a small rich text widget this morning for this.\n\nI have a whole buch more stuff I've been working on I need to put in CVS. We can't let Andras have all the commits. ;-) Please email me next week for an update.\n\nIf anyone would like to fly me someplace tropical (or possibly to Nove Hrady) let me know. I can't seem to work for all the Quanta stuff. I may as well either relax or code. ;-)"
    author: "Eric Laffoon"
  - subject: "Qt Script for Applications"
    date: 2003-07-18
    body: "Kig Python scripting support? wouldn't it be better if KDE apolications would start using Qt Script for Applications??"
    author: "Yoni"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-19
    body: "Why not dropping Qt Script in favor of Python? Python would be a good choice and the world doesn't need another VBA. *creeps away*"
    author: "Carlo"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-19
    body: "? \n\nQt Script uses JavaScript. If it would use Python it would be as \"another VBA\" as JavaScript is."
    author: "Tim Jansen"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-19
    body: "Why not Hbasic?\n\nhttp://hbasic.sf.net\n\n\n:-)"
    author: "Gerd"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-19
    body: "You want to know what the problem with scripting applications is? It's that you want me to use your script language when I want to use mine. I may want to use bash scripting, Perl or have a little fun with PHP since it can now script (as of 4.3) easily in the shell. Not only that there is also DCOP, and frankly, since I haven't played much with Python and have with Javascript I really prefer what I know. No offense to Python.\n\nFor this reason we've been adding DCOP hooks to Quanta along with some direct program access and supporting the best scripting language ever. (Fill in your favorite here: _____) We've also done the same thing with Kommander, making it initially scriptless by using text associations, plugging in shell scripting and now adding script containers so your scripts can live in the same file.\n\nEvery time I see an app falling back on promoting one language prejudicially instead of using DCOP and allowing the user to choose their language it annoys me. When I want to script something I don't want to crack the books, I want to get it done and move on. This is exactly why these features rarely reach their potential... That and how absolutely hideously terrible M$ basic is at inconsistent behavior and being designed from the beginning to write structurally bad code. Gates and co. woudln't know good abstraction if it bit them on the ass!"
    author: "Eric Laffoon"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-19
    body: ">You want to know what the problem with scripting applications is? It's that you >want me to use your script language when I want to use mine.\n\nSounds like you're a fan of the concept of .NET ;-)\n\nSure, having language independent interfaces should be the goal of KDE in general. On the other hand, it would be good to have a \"default\" scripting language. And then you have to decide which: And Python would be a damn good choice, because it's a very powerful language, easy to learn and there exist proper bindings to KDE yet. -> http://www.riverbankcomputing.co.uk/pykde/index.php\n\nHave a look at eric3 -> http://www.die-offenbachs.de/detlev/eric3.html \nIt's written in Python based on Qt."
    author: "Carlo"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-19
    body: "> >You want to know what the problem with scripting applications is? It's that you\n> >want me to use your script language when I want to use mine.\n \n> Sounds like you're a fan of the concept of .NET ;-)\n \nNot particularly, but I think the CLR is an interesting concept. Nonetheless it couldn't be farther from what I'm talking about. I'm sorry to say I believe you are missing it and getting ready to wave the flag for your side again.\n\n> Sure, having language independent interfaces should be the goal of KDE in general. On the other hand, it would be good to have a \"default\" scripting language. And then you have to decide which:\n\nSo when you go someplace, does one of you open the door for the other one of you? Your first two statements are utterly contradictory and you proceed to pick your language. If this is your \"feel felt found\" approach it needs work. Trust me. You are proving exactly my point. It's like going on slashdot and saying \"I like KDE because Gnome sucks\". Next you wonder why the flame war started. Actually it's even worse because it's like saying \"you should have to use KDE (or Gnome) instead of the other if you run Linux\". It eliminates a choice. I like choice.\n\nLet's try this one more time for those in the back of the room with the paper airplanes and puzzled looks... IF (stay with me here...) If an application is built to be scriptable the designer must make descisions about the means of scripting. Those decisions are going to expose portions of his program to the user to enable them to accomplish tasks. This gives a programmer two concerns.\n1) Adding useful scripting features to the interface\n2) Preventing the user from accessing things that could be dangerous by accessing areas of the program where crashes could be caused and adding extreme complexity.\n\nSo, stay with me. Once again, the developer must do something to build an interface. Optimally they would love to do just #1. Eric uses PyKDE build on PyQt which is essentially designed for small application design with a graphical interface. It's mission focus has not been primarily scripting extentions so #2 above comes into play big time.\n\nStay with me. I wanted to set up this point thoroughly. Here it is. If the developer instead chooses a solid implementation of DCOP and/or other direct interfaces tht can use stdout to communicate then it's a whole new ball game. You can use Eric to build Python scripted dialog extentions to an app. Somebody else can use QSA or KJSEmbed. I can use Kommander and make it avaialbe for users who choose to use as little scripting as possible... and we can all be happy.\n\nYour approach is going to make a lot of people unhappy and a lot of developers will never standardize on it because they prefer another language. Beyond that it will mean that a huge amount of good thinking and useful options and alternatives will be thrown out just because somebody wanted to shove their preferences down everybody's throat.\n\nAs I pointed out, there is no real express programming benefit to tying a program to a single language solution. It may even be more work!  It is also detrimental to the adoption of the program. That is why we didn't do it with Quanta or Kommander.\n\nBTW We don't even have any .NET support in Quanta and you might know it's a web application. What I'm a fan of is freedom of choice.  kparts, DCOP and good design easily provide that without a CLR. I have an application to prove it, Quanta, in base KDE. Have a look at our Configure>Actions and Kommander."
    author: "Eric Laffoon"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-19
    body: "Hello Eric,\n\nI see the value of your point and I agree that for high-level steering of an application, it's probably the best way to go.\n\nHowever, some applications with scripting support (I'm thinking of electronics design tools since that's my habitat...) are passing tons of data between the application - scripting language interface (hundreds of megabytes). In this case, I'm afraid that using DCOP is not a valid option, although I've never tested this in practice.\n\nIn addition, I will never write an application that 100% linked to a single environment (like KDE), so I use Qt instead which gives me a wide choice of platforms to work with. I'm not sure that DCOP can easily be ported to Windows.\n\nTom\n"
    author: "Tom Verbeure"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-21
    body: ">  I see the value of your point and I agree that for high-level steering of an application, it's probably the best way to go.\n\nFinally! Somebody gets it.\n \n> However, some applications with scripting support (I'm thinking of electronics design tools since that's my habitat...) are passing tons of data between the application - scripting language interface (hundreds of megabytes). In this case, I'm afraid that using DCOP is not a valid option, although I've never tested this in practice.\n\nI would have to check the current state of DCOP but this would not be a viable choice in my recollection. Those seem like absurd amounts of data to be shuffling around with scripting too.\n \n> In addition, I will never write an application that 100% linked to a single environment (like KDE), so I use Qt instead which gives me a wide choice of platforms to work with. I'm not sure that DCOP can easily be ported to Windows.\n \nDCOP is based on X11's ICE. So I think it is usable on the BSOD platform via Cygwin and it should work on OS/X with Fink. That would allow you to use KDE as opposed to just QT assuming those systems were so equiped. It may not be the best option at this time but native ports are in work.\n\nYou may want to try temp files because I can't remember if you can used named pipes on windoze. Under such circumstances as cross platform data exchange you may be stuck with whatever a script language can offer to manage the inconsistancies of the platform artchitectures.\n\nBTW DCOP is not limited in this scenario. If you can access your interface via stdout you are in the game. DCOP calls can be made from stdout and can instruct the application to do whatever the programmer wanted like read or write files, open pipes, etc..."
    author: "Eric Laffoon"
  - subject: "Re: Qt Script for Applications"
    date: 2003-07-22
    body: ">Not particularly, but I think the CLR is an interesting concept. Nonetheless it couldn't be farther from what I'm talking about. I'm sorry to say I believe you are missing it and getting ready to wave the flag for your side again.\n\nI don't think I missed your point. I asked this \"question\" just to get an impression about your thoughts and testing your response. And I surely don't want to discuss the pros & cons of CLI/CLR here.\n\n>So when you go someplace, does one of you open the door for the other one of you? \n\nYes and no. I want it language independent and I would prefer a single sample implementation in one scripting language. It's not that I say it should cover whole KDE. By now, DCOP / KDE Bindings are poorly documented. See it more as a wish for more documentation. That's what I want - better docs and a sample implementation. It's not that I say - do it Python - I say - do it in one a language that is easy to learn and powerful. And do it in a single one, because for the unexperienced user it's easier to learn one language, instead having to deal with a lot of samples in different languages and for the devs it's easier to maintain one sample implementation.\n\n>So, stay with me. \n\nI did. And you're right. But a lot of people, who don't want to dig deep into Qt/KDE  will never use the bindings to other languages, if there isn't more documentation; And I prefer sample implementations as documentation."
    author: "Carlo"
  - subject: "Great improvements"
    date: 2003-07-18
    body: "Derek and Malchior,\n\nI really like the new statistics section. As I watch the kde-cvs regulary, this is the section that gives me the most interesting information. Thank you!\n\nAndras\n\nPS: Who is Gaute Hvoslef Kvalnes with so much code commited? ;-) "
    author: "Andras Mantia"
  - subject: "Re: Great improvements"
    date: 2003-07-18
    body: "> PS: Who is Gaute Hvoslef Kvalnes with so much code commited? ;-)\n\nNot code, translations. He is the coordinator of the Norwegian Nynorsk team."
    author: "Anonymous"
  - subject: "Re: Great improvements"
    date: 2003-07-18
    body: "I see. Anyway, good work! :-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Great improvements"
    date: 2003-07-18
    body: "It's okay Andras. You're not getting smoked that bad. ;-) You rock, and now you will no longer be the secret weapon of KDE with thes stats glaring out.\n\nBTW Derek, I must agree. The stats are totally awesome.\n\nIf I might suggest again, you might consider TOC linking your page. This would make it easier to point people to specific stuff in the Digest. In fact I've got some cool stuff I could share with you. Drop me a line if I can help there."
    author: "Eric Laffoon"
  - subject: "statistics"
    date: 2003-07-18
    body: "That thingy is cool... How / where do you get it from?"
    author: "Stormy"
  - subject: "Re: statistics"
    date: 2003-07-19
    body: "Some from bugs.kde.org, summary page. Others from the i18n page.\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/www/areas/cvs-digest/tools/ gives a list of the various scripts used to generate the digest.\n\nThe kde-cvs mailing list sends emails for each commit. They are sorted using kmail filters. The script goes through each email, grabs the pertinent information, then spits out the table that you see.\n\nMelchior Franz did the table design with the indicator bars. I showed him a table that I had done, which was ugly. He took it, and sent back the layout that you see. I think it looks very good.\n\nI would like to add a few links, such as some bio information on the developers, links to the modules, links to the bugs.kde.org queries. Here is some info on one famous and productive developer: http://www.dirk-mueller.com/html_e/!start.htm\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: statistics"
    date: 2003-07-19
    body: "This does not look like the same person as [http://people.kde.org/dirk.html]."
    author: "Erik"
  - subject: "Re: statistics"
    date: 2003-07-19
    body: "www.dirk-mueller.com is *not* the homepage of the famous KDE-Developer."
    author: "Me"
  - subject: "Re: statistics"
    date: 2003-07-19
    body: "Well, I had a good laugh... wasn't it intended as a joke?"
    author: "Datschge"
  - subject: "Re: statistics"
    date: 2003-07-20
    body: "You mean that isn't the Dirk Mueller we know? There has to be someone who does fun and exciting things on the weekend.\n\nDerek"
    author: "Derek Kite"
  - subject: "konqueror & multi-session cd's :("
    date: 2003-07-19
    body: "how about konqueror now?\nin my KDE3.1.2 i can only see the data of the \"1st session\".\nand in K3B it is not possible to add a new session or to view all sessions on the \"unclosed\" CD. really bad to start windows just to get data from an unclosed Multisession CD."
    author: "mononoke"
  - subject: "Re: konqueror & multi-session cd's :("
    date: 2003-07-19
    body: "... file a bug report... \n\nK3B ne version is out, check the changelogs."
    author: "Gerd"
  - subject: "KDE scripting with DCOP and Gambas"
    date: 2003-07-19
    body: "For those who are interested, my own language Gambas can automate any KDE application via DCOP.\n\nhttp://gambas/sourceforge.net\n\nI made a little example named KateBrowser to show the abilities of DCOP. \n\nKateBrowser is a little program that popup a top-only window with a button. When you click on the button, a popup menu shows every C function in the currently opened C/C++ source file in Kate. When you select a function in this menu, the cursor goes to its beginning.\n\nI made that because I wanted this function in Kate !\n\nI had only one problem: it seems that the Kate DCOP interface is not well designed, or not terminated. The method to move the cursor does not update the screen, so I had to hide then show the window to force an update.\n\nI made another example that change the background picture regularly. But this is useless, as you can already do that with the KDE control center !\n\nDCOP is very powerful. KDE applications need just offer more DCOP interfaces, and these interfaces should have a better designed. The Konqueror one or the KDesktop one seem to be well designed for example.\n\n"
    author: "Beno\u00eet"
  - subject: "Re: KDE scripting with DCOP and Gambas"
    date: 2003-07-20
    body: "> DCOP is very powerful. KDE applications need just offer more DCOP interfaces, and these interfaces should have a better designed. The Konqueror one or the KDesktop one seem to be well designed for example.\n\nAgreed. We haven't seen the full power of DCOP in use yet... \n\nI hope you filed a bug for kate btw :)"
    author: "anon"
---
<a href="http://members.shaw.ca/dkite/july182003.html">This week's digest</a> features: <a href="http://edu.kde.org/kig/">Kig</a> Python scripting support,
<a href="http://www.slac.com/pilone/kpilot_home/">KPilot</a> Palm generic db viewer, an action menu in
<a href="http://konqueror.org/">Konqueror</a> to print
files, DVD burning in <a href="http://k3b.sourceforge.net/">K3b</a>, RDP support completed in krdc (KDE Remote Desktop Connection) and an httpmail
protocol ioslave. Plus many <a href="http://www.arts-project.org/">aRts</a> bugfixes besides 
<a href="http://www.kdevelop.org/">KDevelop</a> and
<a href="http://quanta.sourceforge.net/">Quanta</a> fixes
and improvements.
<!--break-->
