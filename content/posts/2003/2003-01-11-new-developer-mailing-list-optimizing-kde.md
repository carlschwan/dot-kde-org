---
title: "New Developer Mailing List for Optimizing KDE"
date:    2003-01-11
authors:
  - "bmeyer"
slug:    new-developer-mailing-list-optimizing-kde
comments:
  - subject: "A better name for that would be kde-performance"
    date: 2003-01-11
    body: "optimize is not a good name."
    author: "Someone"
  - subject: "Re: A better name for that would be kde-performance"
    date: 2003-01-11
    body: "It's for developers actively optimizing KDE, not for users discussing performance."
    author: "dc"
  - subject: "and the freebsd equivalent"
    date: 2003-01-11
    body: "check that sysctl hw.ata.ata_dma = 1\nyou also want to enable hw.ata.wc\nand if you have a drive that supports it\nhw.ata.tags"
    author: "mark"
  - subject: "great :)"
    date: 2003-01-11
    body: "At least now they are thinking about the optimization subject.\nYou see, I think taking a lot of time for kde start or 3~5 seconds for a browser open os ok, but not for a text editor or a file manager, those two are too essential that should open in less than a second."
    author: "protoman"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "You can say what you want, but KDE developers have been optimizing KDE - all the time! - since the days KDE 2.0 betas.\n\nEvery release of KDE has gotten faster.\n\nThe traitor so to speak is the toolchain. This is the compiler and linker on GNU/BSD systems, gcc and ld, which were designed to work well for C. And they do very. C++ as used by KDE project, has long been kind of a step-child. Somebody remember how people even forked the GNU gcc, to egcs, which in the end became gcc 2.95, because C++ development  just didn't make significant advances?\n\nThe current gcc 3.2.x only now achieve a stable C++ binary interface (something C had for more than 10 years), called ABI btw, and begin do a half-decent optimization for C++ code only now. So gcc, probably is on the way.\n\nWhat still sucks badly, is the linker and C++. You see, with all the inheritance stuff, C++ is doing a lot of replacement of pointers to member functions. These are the so called relocations. For every method your base class has, you need every derived classes objects to take special care, reserve memory and all, wether it uses that method or not.\n\nThese relocations, being the exception with C, have been implemented in a way that must be slow. And doing many of them, application startup gets very slow. So if you look at the page for optimization hints, the best hints are the \"don't start\" kind...\n\nI personally am very Ok with these slow times, because I don't terminate KDE very often and not their programs. Then, Konqueror is e.g. very fast. See the Safari adoption success of their engine.\n\nSo what we really someday want, is a toolset that handles code in a way optimized for how C++ does things. We want to locate the points, where user experience could be snappier. And of course, have more threading. One thing BeOS did right, was having a thread per window, per menu, per UI element. Probably KDE 4.0 material....\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: ">  want to locate the points, where user experience could be snappier.\n> And of course, have more threading.\n\nIf you have too many threads, you have a lot of context switches, which takes time and makes cpu cache less efficient. Mplayer for example does not use threading in order to optimize performance. See http://mplayer.lgb.hu/index-en.html.\n\nSo only use threads / create new processes where it is necessary.\n\nThreads are useful to keep the user interface responsive while there is some background activity (eg. loading/saving a file, playing a sound,...), but don't put every little thing in it's own thread just because threading is cool.\n\nThreading is not our great saviour. We don't need more threading, we need threading in the right places."
    author: "Stefan Heimers"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "The overhead of context switches in Linux is approaching 0 quickly, although of course, it will not reach it. I have to admit, I wouldn't know about FreeBSD e.g., but a Linux 2.6 will be more than good enough at it I guess.\n\nAnd as to MPlayer example. What if you have 2 CPUs ? How are you going to take advantage of faster encoding, decoding, if you don't use threads?\n\nDeviding work into independant tasks that are then merged, is the key of optimization for multiple CPUs. \n\nBut that's KDE offtopic, not? This will happen in some library anyway.\n\nBut just like kicker is not part of kdesktop, you want to have ways to make everything work all the time. The bad is probably the synchronisation and locking nightmare you can get into.\n\nI personally want to be able to use a programs menu, no matter if it's doing a long taking repaint, (auto)saving, doing some calculations, whatever. The menu should have  a defined state, be accessible by me as a user and allow me to do all useful things.\n\nBut lets see, I have strong enough faith in those that did KDE 3.1, that they will know better than me anyway.\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "You didn't read the mplayer text, did you?\n\n- MPlayer don't need two cpus, it works just fine with one.\n- Nobody (except some geeks) has two or more cpus in his/her desktop computer anyway.\n- If you have two cpus, fine! You will be glad the other is free for some other applications or you can have two mplayers run simultaneously ;-)\n\nFor KDE it makes sense to have multiple threads, so you will take advantage of having two cpus. As I previously stated, threads are good in some circumstances, but should not be used in excess.\n\nContext switching is a non-issue on very fast computers, but not everybody can afford one of these. My server still runs at 33MHz (i486), my fastest computer is an iBook at 300MHz, and I don't intend buying one of those energy wasting 2GHz Intel or AMD microwave ovens anytime soon;-) .\n\n\nBTW, KDE 3.x is great fun in an Xvnc on the poor old 486 ;-) It is slow, but not as slow as you might think. If you are patient, you could even do some meaningful work with it."
    author: "Stefan Heimers"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "Hi,\n\nI really didn't read it. But I know it well. mplayer runs my home cinema on a Celeron 500 that \"normally\" can't do the trick. I don't think a Dual system would improve it.\n\nBut my example was about encoding. You can never be fast enough with that, can you? And KDE can be faster (more responsive) on SMP systems, if the \"active\" application is run on several CPUs simultaneously. Esp. if you assume that this system has CPU loads of CPU number * 100%.\n\nBut maybe somebody needs to prove that, I am not all so certain, it's really like this for the load situation.\n\nContext switches introduce latencies. With bad threading support of older Linux, those hurt so badly, that Java gets unusable under Linux. Not that this is a particular loss by itself, but it's the technicque, threads, that is limited.\n\nPerformance is a non-issue on fast computers, right. So don't optimize?? Luckily, most people care about efficient use of their computer, not their time. So KDE will get optimized to run 5% faster in some nobody really cares situations, day by day :-)\n\nAs for VNC: I did it via 14k upstream DSL. You need to low-color everything, avoid repaints with animations, etc... and you often won't notice it.\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "This is a great example that putting the guilty on just one side is easy, but not true sometimes.\nOne of the major kde developers (sorry, can't remember the name) once said in a interview that he wanted the linker problem to get fixed just for kde developers don't hide themselves behind that escuse.\n\nI've heard that gcc3 would fix the linker problem, then it was going to be 3.1, now not even 3.2 satisfacts kde programmers :(\n\nAnyway, I'm not saying that kde isn't getting better since 2.0, but 2.0 was SO SLOW that this isn't enought. They just should organize optimization in programs. You see, most programmers don't even know how to optimize well a program, that's why I think KDE wasn't taking much care about it, and now they are starting.\n\nYes, kde is faster now, but if it was good no one would complain about speed. Just compare gnome and kde.\n\nAnyway, I'm not saying kde tem is bad or something like that, just they tought features should come first, then speed. Some users thing they could wait more 6 months for a new release in exange of more speed ;)\n\n"
    author: "protoman"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "i think you got a few things right in your posting, and a few things not-so-right =)\n\nthe truth is that the gcc toolchain does have problems, and this is known by everyone including those on the gcc devel team. this doesn't help matters, and it's frustrating to know that good speed improvements could be had if a tool you depend on was better (or even half-way decent).\n\non the other hand, there is a lot of room for optimization in KDE. such things are getting more attention now probably because there are so many features in. a rule in programming is to get it right first, then optimize it second. now that the base libraries in KDE are featureful and quite stable the business of optimizing them can and has begun.\n\nthat said, when you say \"compare gnome and kde\", what exactly are you comparing? start up times or run time tasks? the truly meaningful comparison is when it comes to run time tasks (displaying web pages, editting large text files, listing directories, filtering email, searching in files, changing fonts in a document, etc.), since not only are those things fully in the hands of the programmers working on the apps but they are what you spend 99% of the time on your computer doing. keep track of how much time you spend starting apps versus using them.\n\nso i'm very happy to see all sorts of run-time optimizations going into KDE since those have the most impact on my experience as a user."
    author: "Aaron J. Seigo"
  - subject: "Re: great :)"
    date: 2003-01-12
    body: "And I agree with you :)\n\nAbout gnome/kde, I was comparing startup times and resources usage.\n\nAnd there is one thing that few people say that I think is a important factor in kde speed, maybe the biggest one: QT.\nEven some small QT apps I've done are kind of slow."
    author: "protoman"
  - subject: "Re: great :)"
    date: 2003-01-12
    body: "And there is one thing that few people say that I think is a important factor in kde speed, maybe the biggest one: Qt\n\nBut I don't think Qt itself is slow at all... just check out things like Opera.. "
    author: "m@n"
  - subject: "Re: great :)"
    date: 2003-01-12
    body: "See also Qt/Windows.  I have a P4 1.6GHz system with Gentoo Linux and gcc 3.2, and a P2 400MHz with Windows XP and MSVC++ 6.  I can compile my Qt application on the Windows box in half the time it takes to compile it on Linux.  My program also starts faster on Windows.\n\nI don't think Qt is slow.  I place blame on the underlying system (g++/ld.so)."
    author: "Justin"
  - subject: "Re: great :)"
    date: 2003-01-13
    body: "Well don't worry ... by the time gcc gets to version 4.0 it will beat or equal MScompilers ...\n\nOh wait ... by  then it will be 2030 and everything C++ will be obsolete "
    author: "C++ Lover ..."
  - subject: "The real culprit is C++ switch to ObjectiveC"
    date: 2003-01-13
    body: "\nWant a language that is a fast as C, cleaner and *more* object oriented that C++, *simple*, simple, simple sytax allows you to \"drop to C\"easy as pie and is th basis of MacOS/X ...  NeXT with the smallest number of engineers of any company (fewer than Be) produced a break through GUI for machines that ran at 16mhz - using ObjectiveC\n\nThe only reason Apple uses C++ is because their gcc allows mixing of obj and c++ together in one project and because there are crap loads of C++ libraries out there.  But there are craploads of Java (and python!) libraries out there too ... and gcc can compile that too ...\n\nThe way to fix slowness of C++ is to eliminate it almost everywhere.  KDE developers are enormously productive (thanks to Qt) but for a look at what 4-6 coders part time can accomplish with ObjectiveC take a look at GNUStep.  After 4 years the GNUStep libraries are nearly complete and the GUI is now starting. Soon therel be standard widgets galore and a portable GUI/development framework.  At that point it will take only 6-12 GNUStep coders to reproduce a KDE quality of environment - and it will only take about 6 months  ... With Mac coders using gcc and GNUStep in droves it will soon have a core development group closer to the size of KDE .\n\nThen it will rule ... because people - it is FAST.  GNUStep coded apps truly are useable on a pentium 100.\n\nSincerely,\n\nObjc Willrule\n\nps: Insert cool \"Switch\" advert here to convince C++ coderz of their folly ...\n\npps: because C++ sux and everyone knows it!"
    author: "ObjectiveC Willrule"
  - subject: "Re: The real culprit is C++ switch to ObjectiveC"
    date: 2003-01-14
    body: "Oh god, Mac people are so creepy. First, Objective C is slower than C++. It's compilers aren't as good, and it's more dynamic nature incurs more runtime overhead. Second, those who deride C++ do so because they don't understand it. Sure it has its share of faults, but so does every language, and C++ has far fewer faults than many other languages. Overall, C++ is an extremely lightweight, fast, and most importantly, *expressive* language. It blends performance and clean design more than most other contemporary languages. In doing so, it makes certain compromises and incurs a good deal of complexity. But hey, English is an order of magnitude more complex than even C++ and people speak that passably! \n\nPS> I've been using C++ as my primary language for more than six years. It wasn't until I had been using the language for a couple of years that I learned its full power. Nobody claims C++ is easy. Still, I get irritated at all those people who've used C++ for six months and start spouting (\"C++ sux, <insert fad language here> rulez!\"). Now, if you're an experienced C++ coder, and still think it sucks, well, that's a defensible difference in opinion. Otherwise, you're just spouting.\n\nPPS> Language wars are stupid. In reality, each language has its strengths and weaknesses. Everyone should know and use at least several. I happen to like C++ best, believe it is a good language, and believe that a statement like \"C++ sux\" is just plain stupid, but I don't claim it is the be all end-all of languages. "
    author: "Rayiner Hashem"
  - subject: "Re: The real culprit is C++ switch to ObjectiveC"
    date: 2005-02-24
    body: "Claiming that Objective C is slower than C++, or that you'd simply have to be ignorant to C++ to \"deride\" it, is making two incredibly general (and very likely not to be true) statements (I think you should probably insert the word \"usually\"). I've personally professionaly programmed in C++ for over seven years, and I have to say quite frankly that I have a 'lot' of grievances with it. It isn't that C++ is difficult to learn, or really even conceptually difficult to deal with--- it's actually very simple compared to many other languages (conceptually). It's that for the deficiency of concepts it really can express, the syntax is just terrible. Yes, it's powerful, yes, there is a large existing source base to be used, but it's, honestly, pretty damn ugly.\n\n   Stating that C++ has far fewer faults than 'many' other programming languages may be a true statement, but Objective C is not one of those languages (And probably no other modernly used language save perl and VB). Objective C is a true extension of C, and it isn't inherantly any more 'dynamic' than C++. In C++ you can create non-virtual objects, which technically are no different than sets of methods taking structures as parameters. If an object can't be extended, can't act polymorphically, there really isn't much of a reason for it to be an object to begin with. Objective C can use a majority of the C compiler optimizations, so, I'm really not quite certain how the compiler performance between Objective C, C, and C++ could be much different (This is bordering on the argument people used against using C++ instead of C; the differences really are insignificant).\n\n    You're right about one thing, language \"wars\" are stupid. Honestly comparing two programming languages, however, is not. C++ definitely doesn't suck, but Objective C is a very viable alternative."
    author: "Jason Thomas"
  - subject: "Re: The real culprit is C++ switch to ObjectiveC"
    date: 2003-05-09
    body: "A prof of mine at Stanford, some time ago, said:\n\nThis was a time before the opensource / GNU movement.  When you pick a\ncommercial company for your dev tools...\n\nlong ago there was a language war:\ncamp a: Brad Cox and his nobel Obj-C, simple & elegant & pure hybrid,\n  takes the elegance of smalltalk and hooks up C.\ncamp b: AT&T and its C++, layers onto C.\n\n[from lang elegance, & pure-OO, Obj-C is the winner.]\n[from a \"money - bet your biz on it, C++ will win\" + it can\n get C-like speed]\n If you had a company [dont count NeXT now] you would bet on a\n single company's compiler, a 10-person like company, or the might\n of AT&T?  You have to pick something that you will use for the next 10 years?]\nHis summary: its not contest. C++ will win.\n\nHey C++ was designed w/ certain constraints: (a) it had to be a superset,\n(b) it has to have speed, as its primary customer was the \"Phone code\",\nyou know the stuff that services your long distance calls & its running\non, at the time, 1mip PDP or AT&T mini-computers.\n\nEvery lang has +s and -s.  C++ is faster [machine wise].  Obj-C might\nbe more efficient human wise.  Heck a language like Self might\nbe even faster, people wise.  [prototype based object langs might be\neasier than class based ones you see in the mainstream.]\n\nThe reason Obj-C is viable now is that GNU-c can do C/Obj-C/C++.\n"
    author: "tom popovich"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "gcc3 fixed gcc's part of it - not enough, but the binaries code it wrote was prelinkable\nnot sure what binutils version fixed their part, but it's ready. - the things it links are prelinkable\nglibc2.3 fixed glibc's part of it - ld.so knows how to load a prelinked binary if it finds one\n\nso the remaining piece is the 'prelink' tool itself, so that these prelinkable binaries can become prelinked binaries. It is approaching a useable stage now... if you want to download a snapshot, be my guest. I'm working with it some. It's not 100% stable, but I haven't nuked myself too bad yet.\n\nlinking is *every bit* as bad of an issue as kde developers make it sound like. fully 50% of the cpu time during a kde login (on my system, probably closer to 30% on yours - I have some local work not yet committed in fontconfig that really helps speed that up, raising the percentages for everything else) is spent, in the linker, before reaching main().\n\nWe could have frozen feature work while we waited for the improved toolchain to be ready, but then we'd still have kde2.0 now instead of 3.1, and the prelinker wouldn't be any closer to ready. "
    author: "Kevin Puetz"
  - subject: "The linker problem is utterly irrelevant"
    date: 2003-01-11
    body: "The linker problem is utterly irrelevant\n\nIt takes my laptop 1 second to execute kedit --help. It takes 7 seconds to load kedit and put the window up.\n\nThe linking stage is the same, and linking clearly takes at most 1/7th (~14%) of the time required to start kedit. Therefore 6/7ths (~86%) of the time is spent doing something else.\n\nWould any KDE developers explain where this idea that the linking time dominates KDE start up time when my experiments clearly demonstrate that this is not the case on my system.\n\nA bit of profiling suggests that it spends nearly 50% of the startup time doing malloc(!)"
    author: "cbcbcb"
  - subject: "Re: The linker problem is utterly irrelevant"
    date: 2003-01-11
    body: "the linking stage is not the same, ld.so is a lazy linker resolving symbols only as they are needed. So you need to resolve more symbols when you start the whole app.\n\nif you're seeing that much malloc though... are you kdelibs built with --enable-fast-malloc=full?"
    author: "Kevin Puetz"
  - subject: "Re: The linker problem is utterly irrelevant"
    date: 2003-01-11
    body: "Instead of \"time kedit --help\", try \"time LD_BIND_NOW=1 kedit --help\".\n\nOn my machine \"kedit --help\" takes 0.551s without LD_BIND_NOW, and 0.946s with it, almost double.\n\nRunning kedit so that it pops up onto the screen without LD_BIND_NOW takes 1.757s, and with it 2.041s. Those numbers are both higher than the true values though, because I wait for kedit to pop up on the screen and hit alt-f4 as fast as I can.\n\nSo it looks to me like almost half the startup time is taken up by the linker."
    author: "AC"
  - subject: "Re: The linker problem is utterly irrelevant"
    date: 2003-01-12
    body: "well i'm currently playing around with a glibc.2.3-based prelinked gentoo system. i can tell you prelink makes a _huge_ difference.\nit takes 9s to start x and kde from the console. when i start konq with kdeinit_wrapper konqueror it takes ~0s. (thats on a athlon xp 1800)\nsurprisingly it's a lot slower to start apps from kicker. but i read of a patch in HEAD that should fix that. (im currently using 3.1rc5)"
    author: "tschortsch"
  - subject: "Re: The linker problem is utterly irrelevant"
    date: 2003-01-12
    body: "> It takes my laptop 1 second to execute kedit --help. It takes 7 seconds to load kedit and put the window up.\n\nHave you heard about linking is actually done? Not *all* symbols are resolved at once."
    author: "m@n"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "Actually, threading on Linux is extremely cheap. At this point, encouraging more extensive threading would probably improve performance more than hurt it."
    author: "Rayiner Hashem"
  - subject: "Re: great :)"
    date: 2003-01-11
    body: "Threading can never improve performance on a uni-processor system (at least 95% of all systems running kde). Multi-threading programs use the processor cache less efficent.\n\nThreads often make programs more complicated, and so bugs are harder to find.\n"
    author: "itsch"
  - subject: "Re: great :)"
    date: 2003-01-12
    body: "Only in the land of the theoretical.  In the real world where we live, threading can yield enourmous improvements just by the sheer fact that it can make some problems easier to solve with our intellectual-resource-constrained noggins.\n"
    author: "Another Anon-person"
  - subject: "Re: great :)"
    date: 2003-01-12
    body: "Try a preemptable Kernel (2.4.20+patch, e.g. -ck or 2.5.xx) without \"heavy threading\" and be surprised. Next step would be the glibc-2.3.1 with the new NGPT.\n\nGreetings,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: great :)"
    date: 2003-01-14
    body: "Threading almost always improves user percieved performance. There are two options for good user performance: do events and I/O async, or break them out into threads. Most developers are too lazy to do the former (because it's freaking HARD!) so unless they use threads, they end up doing massive computations and I/O while screwing user speed. See most Linux software for blatent examples (and compare them to a well threaded base of software like BeOS's). Besides, with HyperThreading and cheap SMP becoming more prevalent, who cares about a (very minor, with today's or even yesterday's systems) in the uniprocessor case."
    author: "Rayiner Hashem"
  - subject: "Re: great :)"
    date: 2003-01-13
    body: "You're right that threading is not necessarily CPU-time efficient, but it is user-time efficient.\n\nI really hate each time I do a slow operation and that the whole application freeze instead of just the part who is doing the work.\n\nAn example with Mozilla: tabs opening, lets suppose I open a new tab on a busy website, the tab \"freeze\" which is ok but the whole browser freeze too, I can't instantly go back to an active tab..\nFrom the user point of view, it feels really slow, a browser can render pages \"instaneously\", if it has these kind of user interface issues, it will be always felt as slow by users..\n\nI hope that Konqueror with tabs won't have these kind of issue.\n\nBeOS was really good for this, it felt smooth: no blocking, a window could freeze because it was doing something complicated, but you could instantly swith to another window without any slowdown felt both in Windows or X-Windows (KDE or Gnome).\nOh and BeOS booted really fast!\nWhile it doesn't matter really, it gives user a good feeling about the OS, end-user feels is much more important than benchmark numbers IMHO.\n"
    author: "renoX"
  - subject: "Re: great :)"
    date: 2003-01-14
    body: "In X-Windows windows don't freeze, event if application hangs, you can switch to another window or even resize the window. If you feel slowdowns when switching apps it means there isn't enough RAM on your computer."
    author: "Tot"
  - subject: "Re: great :)"
    date: 2003-01-14
    body: "> If you feel slowdowns when switching apps it means there isn't\n> enough RAM on your computer.\n\nOr your apps use too much RAM... :)\n\nFor example, should a web browser really use 60MB of RAM? That is an awful lot. What does Konqueror do with it all?\n"
    author: "cbcbcb"
  - subject: "Re: great :)"
    date: 2003-01-16
    body: "Nothing 'cause it's mostly shared:\n\nnuetzel  13680  2.4  2.5 109268 26688 ?      S    02:29   0:09 kdeinit: konqueror --silent\n\n/home/nuetzel> pmap 13680\nkdeinit: konqueror --silent(13680)\n08048000 (32 KB)       r-xp (08:08 417105)   /opt/kde3/bin/kdeinit\n08050000 (8 KB)        rw-p (08:08 417105)   /opt/kde3/bin/kdeinit\n08052000 (6940 KB)     rwxp (00:00 0)\n40000000 (76 KB)       r-xp (08:03 9424)     /lib/ld-2.2.4.so\n40013000 (4 KB)        rw-p (08:03 9424)     /lib/ld-2.2.4.so\n40014000 (4 KB)        rw-p (00:00 0)\n40015000 (188 KB)      r-xp (08:08 307406)   /opt/kde3/lib/libkparts.so.2.1.0\n40044000 (12 KB)       rw-p (08:08 307406)   /opt/kde3/lib/libkparts.so.2.1.0\n\n[snip]\n\n46312000 (152 KB)      r--p (08:07 1731481)  /usr/X11R6/lib/X11/fonts/truetype/verdanai.ttf\n46338000 (272 KB)      r--p (08:07 1731455)  /usr/X11R6/lib/X11/fonts/truetype/arial.ttf\n4637c000 (280 KB)      r--p (08:07 1731486)  /usr/X11R6/lib/X11/fonts/truetype/arialbd.ttf\n463c2000 (56 KB)       r-xp (08:08 475465)   /opt/kde3/lib/kde3/libkuickplugin.so\n463d0000 (8 KB)        rw-p (08:08 475465)   /opt/kde3/lib/kde3/libkuickplugin.so\nbffeb000 (84 KB)       rwxp (00:00 0)\nmapped:   109268 KB writable/private: 9324 KB shared: 73812 KB\n\n=> 9,3 MB (own) + 73,8 (shared)\n\nSo what is your problem?\n\nRegards,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: great :)"
    date: 2003-01-16
    body: "\nThanks for the hint about the pmap tool.\n\n> So what is your problem?\n\nFirstly, I don't run KDE on my laptop (it's too slow), I run konqueror under Windowmaker, so (most of) the 'shared' memory isn't really shared. You still have to account for the shared memory - it isn't free.\n\nSecondly, I have a 30MB private anonymous mapping in the current konqueror process which has been running for some time. My experience is that konqueror gradually uses more and more memory but I haven't been able to reproduce it under test conditions (although general web browsing always triggers it). Eventually the Konqueror process grows big enough that it hits swap and everything grinds to a halt and I have to restart it.\n\nA newly started konqueror doesn't have this huge mapping. I guess this is a bug in Konqueror/KDE's memory management somewhere.\n\n"
    author: "cbcbcb"
  - subject: "Re: great :)"
    date: 2003-01-16
    body: "Yes we just found and fixed some cycling reference counting problems in the Javascript support, which led to memory leaks.\nHopefully KDE-3.1 will have those fixes."
    author: "David Faure"
  - subject: "Re: great :)"
    date: 2003-01-16
    body: "\nAre the fixes on the 3.1 branch currently, or just on the head?\n\nShould KDE just move to using libgc and become fully garbage collected? This should reduce the number of memory leaks"
    author: "cbcbcb"
  - subject: "Re: great :)"
    date: 2003-01-16
    body: "Just backported to 3.1 branch.\n\nFully garbage collected? Surely not. It's the garbage-collection nature of Javascript which introduces those leaks (when you end up with a cyclic reference chain, nothing gets deleted anymore). This is definitely NOT a solution for the rest of KDE. Plain simple \"delete foobar\" is much easier to do and debug than garbage-collection with refcounts and possibly cyclic ref chains."
    author: "David Faure"
  - subject: "garbage collection"
    date: 2003-01-16
    body: "\nI'm talking about real garbage collection, not reference counting! libgc implements a generational mark and sweep collector, so it works properly when you've got cycles. Refcounts also have horrible performance implications.\n\nAnyway, looking at the kdecore/malloc stuff it looks pretty trivial to add new memory allocation strategies so you might get a patch when I get round to it."
    author: "cbcbcb"
  - subject: "Re: garbage collection"
    date: 2003-01-16
    body: "Ah yes. The reason I'm talking about refcounting for kjs, is because it has a DOM-like API, so it needs both refcounting _and_ garbage collection with mark & sweep. The dependencies with the DOM nodes (which are refcounted) is what makes the problem a bit difficult.\n\nAbout a generic malloc implementation that can garbage collect: hmm - how will it know the dependencies between the objects, i.e. which one should mark which other one?\n"
    author: "David Faure"
  - subject: "Re: garbage collection"
    date: 2003-01-17
    body: "\nI'm not sure why you need refcounting and mark & sweep - surely if you have the correct set of roots then the mark & sweep strategy will do everything? (I've not looked at the kjs code so I could be missing something)\n\nThe homepage for the libgc collector is at http://www.hpl.hp.com/personal/Hans_Boehm/gc/ Most distributions should have a libgc package.\n\nEssentially, it's a conservative collector, so if a malloc-ed region contains a value which appears to be a pointer (ie it has a value within the bounds of the heap and there is an object on the heap at that location) then it is treated as a reference. The roots of the collection are contained in the static data area and the stack.\n\n"
    author: "cbcbcb"
  - subject: "Re: garbage collection"
    date: 2003-01-17
    body: "We need refcounting because as I said the KJS objects (the only ones which are garbage collected) have dependencies on the DOM objects (which are not), and vice-versa.\nSince the DOM objects are not known to the garbage collector, they will never be asked to mark(), therefore the related KJS objects will be destroyed at the wrong time.\n\nAnyway I'm not very expert on fully mark&sweep solutions (this one was implemented by Harri and the refcounting was added by Peter) - I can describe what kjs does and why, but not whether something else could have worked.\n\n\nHmm, the idea of a collector trying to guess pointers from raw memory sounds like a good opportunity for strange behaviours to me :) Ok, it must be quite rare that binary data (pixmaps, strings, etc.) contains the same 4-byte sequence that those matching precise locations where data was allocated, but when this happens, strange things will happen. You see, the main reason we can't easily switch to a GC, is that in C++, destructors can have code. And they often do. We rely on object A doing this and that when being destroyed. If it's not destroyed anymore, then stuff will break. Moving stuff from the destructor to a \"destroy()\" method changes nothing to this - if it's not called, bugs will happen. Therefore a GC wouldn't help. Fixing the code does :)"
    author: "David Faure"
  - subject: "Perl?"
    date: 2003-04-21
    body: "I may be wrong, but doesn't Perl use reference counters? Perl seems pretty darn fast to me, too. If Perl can do it quickly, who can't a JavaScript implementation do it quickly as well?"
    author: "Jonathan Miller"
  - subject: "Re: great :)"
    date: 2003-01-16
    body: "In short: Which KDE/Konqueror version?\n\nWould you \"all\" be so kind to send full system specs and software versions if you have some trouble?\n\nThanks,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "kde-optimize -- give me a break"
    date: 2003-01-11
    body: "KDE is written in C++. While this is not necessarily a problem, it can be when Visual Basic reject programmers (which the KDE project is overrun with) do not know enough to avoid important pitfalls that plague C++ software projects; KDE suffers badly from stupid use of autoincrementing operators and iteration with C++ objects and masses of unnecessary allocations and deallocations of memory -- two of the most comon problems in C++ software.\n\nPerhaps the most cretinous of all problems is blaming the extremely slow startup times of KDE apps on GCC. The GNOME 1.x releases were hardly svelt (2.x fixes many of these issues), but GNOME is a fashion cat-walk superwaif when compared to KDE's 500lb fat-momma cheese-burger scoffing trailer trash.\n\nOne need only look at the fuss over ugly KDE hacks (such as prelinking) used to bandage up the design and coding flaws in the decrepit KDE architecture to see the truth."
    author: "GNOME user"
  - subject: "Re: kde-optimize -- give me a break"
    date: 2003-01-11
    body: "Word for word the same on wrong facts based flame as <a href=\"http://dot.kde.org/1040965751/1041019223/1041474015/\">last week</a>. You were corrected, you didn't learn. Go away!"
    author: "Anonymous"
  - subject: "Re: kde-optimize -- give me a break"
    date: 2003-01-13
    body: "Not only that, but he spelled svelte wrong both times!  :-)"
    author: "Andrew"
  - subject: "Re: kde-optimize -- give me a break"
    date: 2003-01-11
    body: "Yeah! And C64 Basic loads really fast on an 1MHz machine. It's written in assembly. Let's use that!!"
    author: "ac"
  - subject: "Re: kde-optimize -- give me a break"
    date: 2003-01-12
    body: "LOL!! Very funny. \nThis troll is quite amusing. :))"
    author: "CPH"
  - subject: "Re: kde-optimize -- give me a break"
    date: 2003-01-12
    body: "<stupid flame here>\n\nIt's good to see all the lamers still living in 1990 come out of the woodwork. KDE runs fine on my Athlon XP 2200+. Gnome does too. I know GNOME 3.0 and KDE 4.0 may not run as well, but that's fine... i'll upgrade. KDE 3.0 and GNOME 2.0 don't run with any acceptable speed on my pII 233 router box (which was once my main system).. but KDE 1.0 and GNOME 1.0-1.2 did (and GNOME 1.4 without the hog that is Slow^H^H^H^HNautilus) "
    author: "yep"
  - subject: "Intel compiler"
    date: 2003-01-11
    body: "Anyone tried compiling Qt and KDE with the Intel compiler?"
    author: "Johnny Andersson"
  - subject: "Re: Intel compiler"
    date: 2003-01-12
    body: "Problem is not so much the compiler, it's the linker which would still be the same old slow one resolving relocations at startup...\n\nThe Intel compiler is furthermore not free and all. I suggest, we wait for DFA in gcc releases, then the Intel compiler should theoretically be overtaken, or not? The good thing about gcc will then be, Celeron, Athlon, i486 optimizations automatically near optimal, because a pipeline description will allow to simulate for best code/data flows...\n\nThat's advanced stuff... nobody say that gcc is not the best work ever. Just not for C++ ;-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Intel compiler"
    date: 2003-01-12
    body: "I don't think so.  GCC doesn't even have a fully baked graph-based register allocator yet, let alone implementing some of the recent advances in compiler research that icc only recently has employed.  GCC sadly still misses a lot of optimization opportunities that it can do but doesn't (just look in the bug database).  The fact is they just don't have the sheer number of people and intellectual resources working on GCC as do many commercial groups.\n\nAnd I don't see that changing any time soon with the GCC project having so many unnecessary artifical barriers to entry (read: FSF copyright assignment before code is accepted into the tree, having the codebase written in C, etc.)\n\nOn the bright side, GCC is probably one of the most \"correct\" compilers out there for (at least) C and C++."
    author: "Another Anon-person"
  - subject: "Re: Intel compiler"
    date: 2003-01-12
    body: "> On the bright side, GCC is probably one of the most \"correct\" compilers out there for (at least) C and C++.\n\nOh yeah (gcc3):\n\n#include <cstdarg>\nclass A {\npublic:\n  virtual int foo ( unsigned long, ... ) = 0;\n};\nclass B : virtual public A {\npublic:\n  int foo ( unsigned long a, ... ) { return 0; };\n};\nint main ( void ) {\n  B b;\n  return 0;\n}"
    author: "jmk"
  - subject: "Re: Intel compiler"
    date: 2003-01-12
    body: "yup, that doesn't work.  Still, g++ is better than msvc and borland IME.\n(btw, that specific sort of bug with varargs is fixed in 3.3)\n"
    author: "Another Anon-person"
  - subject: "Re: Intel compiler"
    date: 2003-01-12
    body: "http://www.trolltech.com/developer/compilers/intel.html\n\nQT3  works beautifully with icc.  Much of KDE on the other hand won't build with ICC, mainly due to a few unsupported GNU extensions like statement-expressions."
    author: "Another Anon-person"
  - subject: "Re: Intel compiler"
    date: 2003-01-21
    body: "As someone who has actually run C++  benchmarks between\nGCC and Intel C, I can tell it would not help at\nall with KDE. Intel is good in floating point number\ncrunching, but not at all better for integer and pointer\nhandling code, which is what a GUI mostly contains.\n"
    author: "MacroRodent"
  - subject: "Re: Intel compiler"
    date: 2006-03-19
    body: "Hi!\nNow (2006-03-19) QT 4.1.1 (latest from trolltech) was compiled \"out-of-box\" and working without problem via Intel's icc-9.0.030 (latest from Intel).\n\n\nGentoo 2006.0, Intel 920, EM64t\n\nWith best whishes - Yuriy Dmitriev."
    author: "Yuriy Dmitriev"
  - subject: "Open Source == Open Design"
    date: 2003-01-12
    body: "Is 'just' a fairly abstract 'KDE-optimize' mailing list a thorough enough solution to an ever recuring issue of performance? For in the case of a KDE project, a developer cannot quickly drop in and review what the most relevant performance/optimization issues of a given project are. The source is very open, but most current project seem not very open about their code design. \n \nThus note that 'optimizing' might also count for inefficient organization of 'KDE-usability' or 'KDE-look', as I believe is also relevant. Not all KDE projects evolve around code, but all of them are based on an organization to which you could link efficiency/performance/progress rate. \n \n'We' are already open-source, you might as well be open about your bottlenecks and code design for potential new developers. The 'performance bottleneck' of a mere KDE-optimize mailing list comes from the fact that you might not know a project by it's design in the first place. In the end you might find all your research was useless as your possible solution or approach was already considered/implemented. \n \nAny serious attempt to optimize part of an application involves getting to read allot of the code and get a feel of the data flow, whereas it often might be usefull for the developers of a project (already aware of the current bottlenecks) to be public about this and mentioning design (diagrams) on their site or perhaps on a central 'KDE code design' site where developers can browse through to find relevant information about the project they are potentially interested in for helping. \n \nSo for developers it would be good to be able to browse per project what the bottlenecks are for each project and thus quickly browse a bit trough the relevant code. This way some more or less obvious 'slow' syntax can more quickly be resolved, or even be backed up by suggestions of improved datastructures for a given project. \n \nConcluding this rant - each KDE project's code-leader can already improve optimize performance one step ahead by being more open about their project it's design approach. Detailed layouts of code designs lower the entrance bariere for potential new developers of interest so they can better review your project and decide wether to join, suggest radical new designs or offer mere syntax improvements.\n"
    author: "IR"
  - subject: "Re: Open Source == Open Design"
    date: 2003-01-13
    body: "IR said: \n> blah blah blah\n\ntalk is cheap. \n\nAll you say is very theorical and could just apply to any projects. It looks like you never take the time to look at what KDE has been doing regarding speed improvements. I don't want to sound agressive, but do you think you are just the smart guy which will teach the KDE developers how to work ?\n\nPlease take the time to learn more about KDE before posting this kind of stuff. For you information:\n\n1. Optimisation has always been in KDE developers mind. While there is no formal benchmark or process yet, developers regularly check the speed of the desktop and see where it could be generally improved. Browse kde-core-devel, you'll see benchmarks periodically.\n\nSo we do care about speed. I remember the KDE 2.0 release, which used a two month delay to get fantastic speed improvements at that time (tweaking with startup and kconfig).\n\nThe recent improvements of the Gnu toolchain regarding relocation and prelinking have been motivated by analysis perform by KDE developers or by people interested in improving KDE speed.\n\nSo please don't talk like we suddenly discovered that we could think about improving speed. It just wasn't as publicitely done as it may become now.\n\n\n2. \"Thus note that 'optimizing' might also count for inefficient organization\"\n\nThis is why we are creating kde-optimise, to better cordinate any effort in improving KDE.\n\n\n3. \"In the end you might find all your research was useless as your possible solution or approach was already considered/ implemented.\"\n\nWe are not doing research but analysis and improvements. Big difference in approach. We don't analyse and present design for the fun of it, and just realise it does not apply. KDE, the projects and the developers have always been very pragmatic. If we analyse something, there will be a useful result, don't worry.\n\n\n4. \"Any serious attempt to optimize part of an application involves getting to read allot of the code and get a feel of the data flow\"\n\nAnd what do you think developer are doing everyday ? Check any development mailing-list and you will find people that are very aware of all KDE code (2.6 millions lines of code). Do you think main developers do not know their code or their data flow in their application ? What kind of developers would they be then ?\n\nThere also are plenty of generic tips to improve the speed of an application, are at least to avoid some bad constructs (using sytstem thinking sh is quick to start (while you are in fact starting bash) for example).\n\n\n\n5. \"This way some more or less obvious 'slow' syntax can more quickly be resolved, or even be backed up by suggestions of improved datastructures for a given project.\"\n\nThis is happening everyday.\n\n\n\nSo all in all, the only interesting thing in your post is to make design of applications more public. A good idea in itself, but you don't need so much blah blah to present it.\n\nAlso note that you learn a lot by reading source code. Although it requires an initial inestement before contributing, I am sure it helps to raise the level of contributors.\n\n\n\n\n\n\n\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Open Source == Open Design"
    date: 2003-01-13
    body: ">>>All you say is very theorical and could just apply to any projects. \n \nThat was intended and not in a dictative form. \n \n>>>It looks like you never take the time to look at what KDE has been doing regarding speed improvements. \n \nFirst lets agree there are those projects we can regard as core projects and side projects. And I admit not knowing allot about the KDE (core) design, neither do I want to pretend to do. Neither am I able to other than to read allot of code only to learn about already known code designs (which can be argued a waste of time on my part, since diagrams or what not would have made that process allot more efficient). \n \n>>> I don't want to sound agressive, but do you think you are just the smart guy which will teach the KDE developers how to work ? \n \nNo, do you think I am that stupid Philippe? I merely attempt to advise those developers who have so called 'side' projects to be more open about their application design such as to lower the barrier of entrance for overall most KDE side projects (or potential BETA branch projects). Obviously at some point you DO need to read allot of code, but it can be avoided to some extend. Its not a bad habit to be open about your code design and this does apply more to side projects then KDE's core as I fully respect is likely the case. Actually, I consider the KDE-core at an entire different design level since it is already a long ongoing project, unlike some newer projects. \n \n>>>1. Optimisation has always been in KDE developers mind. While there is no formal benchmark or process yet, developers regularly check the speed of the desktop and see where it could be generally improved. Browse kde-core-devel, you'll see benchmarks periodically. \n \nYou seem to talk about 'the KDE developers mind'. Now I very well expect there to a 'common' KDE developers mind for core KDE parts.I believe however that for some coders it wouldn't be a bad habit to be more open about their design and lower the entrance bariere to potential new KDE killer-apps. \n \n>>>So we do care about speed. I remember the KDE 2.0 release, which used a two month delay to get fantastic speed improvements at that time (tweaking with startup and kconfig). \n \nI never argue *you* don't care for speed nor do I even dare to call my judgement over the performance of a project such as KDE-core. \n \n>>>So please don't talk like we suddenly discovered that we could think about improving speed. It just wasn't as publicitely done as it may become now. \n \nPlease don't feel so attacked. You do agree that not all (side) projects are well designed and run be 'we'? Some people might want to be more open about their designs? I am not dictating anything, just wanting to give advise as I think it might be advise to some people? \n \n>>>4. \"Any serious attempt to optimize part of an application involves getting to read allot of the code and get a feel of the data flow\" \n>>>And what do you think developer are doing everyday ? Check any development mailing-list and you will find people that are very aware of all KDE code (2.6 millions lines of code). Do you think main developers do not know their code or their data flow in their application ? What kind of developers would they be then ? \n \nThat's my point. Not ALL projects get this kind of extensive reviewing and most have a 'high' barrier for review at all. Off-course developers should know their data flow. And wouldn't it be great if they mentioned this somewhere so would be new developers can more quickly join a (side) KDE project? At least, that's my opinion. \n \n>>>So all in all, the only interesting thing in your post is to make design of applications more public. A good idea in itself, but you don't need so much blah blah to present it. \n \n...'blah blah' \n \n>>>Also note that you learn a lot by reading source code. Although it requires an initial inestement before contributing, I am sure it helps to raise the level of contributors. \n \n...yes obviously. Still, potentially you might get more contributions for more projects if they are more open about general design approach. Plus its nice for any developer to 'quickly' learn a bit how KDE stuff ticks without extensive research. Obviously before any contribution more in-depth review is required! \n \nKDE is a rather old project, so its very likely allot things have already been discussed so many newbies developers (like me) are likely to post old ideas/suggestions 'again' and again sounding dictating in the process, all the while just trying to help. \n"
    author: "IR"
  - subject: "Re: Open Source == Open Design"
    date: 2003-01-13
    body: "\nSorry, I was a bit verbose about speed issues because many seem to believe that KDE developers are not aware of optimisation issues, or never thought about optimising KDE.\n\nSo basically you suggest to have more open designs for less mainstream applications. I say it is a good idea. Ideally, every kde app would have a sourceforge like project page to discuss its development and expalin its design.\n\nYou can find this kind of design answer by simply asking on a mailing-list. Not as friendly as reading a design document, but still very efficient.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Open Source == Open Design"
    date: 2003-01-13
    body: "I am relatively new regarding programming for Linux and using Linux in general. When Mandrake 9.0 came out, I switched 100% from windows to Linux (now using Gentoo). Open source is such an enormous powerfull entity in itself. Again and again I am  also am amazed by KDE abilities and potential abilities. Not to mention how volunteers organize themselves in such a professional/effective way. I guess you can say that Windows blinds users but also allot of developers sadly. \n \nWhat I personally found over time is that it can be argued there is a KDE user barrier, there is also a developers barrier. Its just that I can see much potential in open standards/code the more I am involved in utilizing/developing these, the more I sometimesfeel sorry not to see developers being open about their designs. Because its finaly these designs which are also out in the open. \n \nSome time ago I read an extensive article about the Reiser file system and through it the developers gave me a look into how it works also related to its datastructures. This allowed me to learn allot more about its design without having to read trough allot of abstract code. I think this is great because more developers can anticipate and more quickly react on it this way. \n \nIts also just nice for any technically minded KDE person to see better how KDE ticks. Perhaps its like an automobile. Its engine is openly available, but nice layouts of its design and how it works is pleasing also to non mechanics."
    author: "IR"
  - subject: "Re: Open Source == Open Design"
    date: 2003-01-13
    body: "You have a good deal of information on\nhttp://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html\n\nBut I understand what you mean. This is open source, you must help yourself."
    author: "Philippe Fremy"
  - subject: "Tips: Upgrade to KDE3.1??"
    date: 2003-01-12
    body: "Let's see..\nKDE becomes more slow each release. Ok, I have an old PII with 64MB, but\nthat isn't the issue.\nI installed a Linux with KDE 2.1 for a friend in its still older Pentium 233, and it's yet faster than my KDE 3.\nI discussed this with Aaron Seigo and its KDE3 on 64MB computer mith, and we didn't agree. \nKonqueror is a special concern. It LEAKS MEMORY. After an hour or so, it eats all the computer resources at the point you must kill it or throw yourself thru a window.\nI hope KDE becomes better, but I do not believe it becomes faster in newer versions."
    author: "Julio Cesar Gazquez"
  - subject: "Re: Tips: Upgrade to KDE3.1??"
    date: 2003-01-12
    body: "Upgrade the system.. seriously. I think it's too much of a drain for programmers to on one hand add new features and support ancient hardware. It held back Apple for a long time (supporting 68k hardware).. it has always dogged Microsoft (until XP). Of course, those two, who are certainly the major players in the desktop market now-days don't care much for old hardware such as Pentium 233's... try running OSX or XP on something like that (or for that matter, try running win2k and MacOS 9.2 on it.. it'll be slow, but workable.. sorta like KDE is). If KDE (and Linux) wants to grab more market share, it must continue adding features. This is what the mass-consumer wants. This is how, for example, Microsoft Office became predominant (after years of WordPerfect/Lotus 123).. Microsoft just added all of the comptetitors' features in their products.\n\nI suggest you use something like fluxbox on it."
    author: "yep"
  - subject: "Re: Tips: Upgrade to KDE3.1??"
    date: 2003-01-13
    body: "Well, some things have definitely become slower with KDE 3, konsole, some things in kmail, but e.g. kmail in 3.1. is much faster than in 3.0.\nNevertheless, 3.1 runs usable on my old K6/200 128 MB.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Tips: Upgrade to KDE3.1??"
    date: 2003-01-13
    body: "Don't forget konqueror!\nIn KDE3.1 it's really faster! "
    author: "Giovanni"
  - subject: "Re: Tips: Upgrade to KDE3.1??"
    date: 2003-01-14
    body: "It isn't that KDE is slower with each release, it is that it gets bigger with each release.  It now takes 128 MB (192 MB is better) to run it.  If you have less memory, Linux becomes slow because of the constant VM swaps."
    author: "James Richard Tyrer"
  - subject: "Re: Tips: Upgrade to KDE3.1??"
    date: 2003-01-15
    body: "\"New KDE release out\" doesn't mean you have to\nrun it in your, say, 266/64MB system.\n\nFor example KDE just flies on Athlon 1.6/256MB.\n\nAnd I've been using KDE 3.01 on Celeron/500/196MB\njust fine. \n\nWhat exactly most people expect when talking \nabout speed?\n\nSure, Gnome loads faster, but this is mostly\nbecause it has 1/4 of the functionality."
    author: "DOD"
  - subject: "Re: Tips: Upgrade to KDE3.1??"
    date: 2003-01-16
    body: "\nIf you use a slower computer there are some things which take quite a long time - enough so that you feel held up waiting for it to respond. For example - opening new tabs on Konqueror in 3.1 is slow on my laptop (takes a second or so) or opening the Konqueror configuration window which causes the computer to sit there hammering away at the disk for several seconds.\n\nAlso, switching between desktops feels noticably slower in KDE compared to other window managers even when running exactly the same applications - for this reason I run Windowmaker on my laptop even though I don't like it :)\n\nI would really like to see KDE use less memory - it seems to fill most of my 128MB laptop, which means that slows down if I start using the memory for anything else. However, the KDE developers are getting a handle on the memory leaks in the 3.1 release candidates.\n\nI did some profiling of the delay in opening new tabs, but I didn't get very far as the debian QT package does not contain symbols so the only results I got was that 90% of the time was spent somewhere in QT. I'm going to do some more serious work once the Debian transition to gcc 3.2.x has progressed a little more.\n\nOf course, there's the standard complaint about slow application start up times. Prelink improves matters a bit (and overall KDE start up an incredible amount), but there are still stability problems with it (at least Kword is less stable, but it might not be the latest version)\n\nIn summary, I feel KDE is just generally a bit sluggish and unresponsive in certain places."
    author: "cbcbcb"
  - subject: "GCC is written in C by C coders "
    date: 2003-01-13
    body: "who don't really care about java or c++ ... gcc is a compiler for systems level programming ... i.e. Unix kernels (and a shrinking bit of NT W2K XP internals) ... C is like fortran it won't  go away and since the difference between writing a kernel and compiler is less than it is between writing a GUI and compiler, theres no way GCC maintainers will ever make gcc/g++ and linkers optimal for C++.\n\nI pin my hopes on Trolltech releasing a free/commercial compiler so linux/bsd GUI's can be properly compiled and linked ...."
    author: "C++ Lover ..."
  - subject: "Re: GCC is written in C by C coders "
    date: 2003-01-13
    body: "You should take a look at the following article:\n\nhttp://osnews.com/comment.php?news_id=2534\n\nSeems like Apple isn't only contributing code to KHTML.. :)\n"
    author: "re"
---
This past week a new mailing list <A href="http://lists.kde.org/?l=kde-core-devel&m=104202724410140&w=2">was created</a> called <a href="http://mail.kde.org/mailman/listinfo/kde-optimize">kde-optimize</a> (<a href="http://lists.kde.org/?l=kde-optimize&r=1&w=2">archive</a>).  The list is for <i>developers</i> who are willing to <i>actively work</i> on optimizing KDE or KDE applications, those who have profiled KDE or those who have the knowledge to help others optimize KDE.  Two initial documents have already been created as part of this list.  The first one is to teach users how they can <a href="http://dforce.sh.cvut.cz/~seli/download/tips.html">improve the performance of their system</a>. The second one is to give developers a <a href="http://www.csh.rit.edu/~benjamin/articles/optimize.php">starting point</a> when looking to optimize applications.
<!--break-->
