---
title: "KOffice 1.3 Beta 3 Released"
date:    2003-08-15
authors:
  - "binner"
slug:    koffice-13-beta-3-released
comments:
  - subject: "Hehe"
    date: 2003-08-14
    body: "The dot almost always seems to be a beta behind ;p\n\nThe title should say Beta 3. The same error wa made last time, when beta 2 was out the Dot said it was beta 1. But, ti's no problem, it happens and you can tellf rom teha rticle what's going on.\n\nHowever,  please correct the error."
    author: "Mario"
  - subject: "Re: Hehe"
    date: 2003-08-14
    body: "Yeah, I'm lazy and concentrated too much on the links in the body. :-)"
    author: "binner"
  - subject: "Yeah so am I"
    date: 2003-08-14
    body: "Just look at all the errors in my post."
    author: "Mario"
  - subject: "Karbon14"
    date: 2003-08-15
    body: "Just wondering what the current status of Karbon14 is.\nIs it being worked on or should I just learn Sodipodi?\n(This is not a troll)\nAnyone who has used both care to comment?  Other than desktop integration what does it do that sodipodi doesn't?  \n\nthanks"
    author: "ac"
  - subject: "Re: Karbon14"
    date: 2003-08-15
    body: "sodipodi is currently quite more \"done\" than karbon14 is. This is because in the KDE vector-graphics app department, there was a move from a pretty mature app (killustrator to karbon14..)\n\nkarbon will also mature in the future, probably when SVG support matures. then we'll have a nice kpainter class :)"
    author: "anon"
  - subject: "Re: Karbon14"
    date: 2003-08-15
    body: "Hi,\n\n>Just wondering what the current status of Karbon14 is.\n>Is it being worked on or should I just learn Sodipodi?\n\nIts not being worked on right now by me, as I am working on ksvg and some framework stuff. Nevertheless its still in active development, work will continue on it soon. And a lot of the work on ksvg is benefitting karbon, as both cover a lot of similar ground.\n\n>Anyone who has used both care to comment? Other than desktop integration what >does it do that sodipodi doesn't? \n\nSure, it has a different feature set. It aims to be a lot like Adobe Illustrator, so it can do some similar tricks like flattening paths, rounding corners, text-along-a-path etc. Please give it a go :)\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Icon Contest"
    date: 2003-08-15
    body: "What ever came of the icon contest? Do these Betas include the winner?"
    author: "Turd Ferguson"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "That would be quite nice indeed. KOffice really needs better icons.\n"
    author: "Jan"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "Sadly enough, the were no contestants so there was no winner :-( "
    author: "Lukas Tinkl"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "http://lists.kde.org/?l=koffice-devel&m=105178354428607&w=2"
    author: "Chris Howells"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "How about using the wonderful icons developed by Ximian for OpenOffice.org? I doubt you'll find a more complete and high-quality set elsewhere...\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "Ah no, please no! These icons really don't fit with the beautiful crystal icons of KDE. Actually, apart from their hig, what I probably most dislike about Gnome is its default icons and color scheme."
    author: "Tuxo"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "While I will agree that the Crystal icons are nice, they are nowhere near as complete and consistent as the Ximian OpenOffice.org ones. Have you even seen them? Take a look: http://jimmac.musichall.cz/i.php3?ikony=77. And besides, Crystal is poorly incomplete, especially in KOffice. They don't scale well to 16x16 sizes, and don't contrast well in the UI (especially for people like me with poorer vision). I've been experimenting with modifying KDE to use the default GNOME stock icons, as they fit the demands of a modern desktop: consistent, complete, and contrasting. Consistent because they do adhere to a specific palette and style, complete because Jimmac and Tigert are extraordinary artists and have produced icons for just about anything you'd need, and contrasting because the stand out much more than the soft colored, soft-edged Crystal icons. I would never want to criticize the wonderful contributions of Everaldo and Tacket, but we are in dire need of a better icon situation, and cooperating with GNOME could lead to another Free Desktop standard: the same icons!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "Well, I know that the Gnome icons are maybe more complete. Their look however is a matter of taste. I don't like their look at all. I would rather see the Crystal icons completed than using Gnome icons. Actually,there is a script at kdelook that allows you to replace the default icons of OpenOffice: \"OpenOffice.org toolbar icon themes\". This theme is not yet complete, but allows to fit OpenOffice much better into KDE than is the case with just using Gnome icons everywhere.\nSo no, I would really not like to see KDE look like Gnome. Really not. The colours of Gnome (also its icons) have an army-style charme and are cheerless due to their dull colours."
    author: "Tuxo"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "But the question is, who will complete the Crystal iconset? Opinions aside, the Ximian OpenOffice.org set is complete *now* whereas Crystal is not. Therefore, it may be in the best interest of the users to provide a solution now instead of waiting for some time in the future (which by then of course, they may be changed back). I don't see any convincing point not to use them other than the fact you don't like them (you can always install a Crystal icon theme). They are consistent, complete, contrasting enough, free, and available now. Without a comparative alternative, I don't see what the problem is. The icons are for OpenOffice.org, not GNOME, just because they are from the same artist doesn't mean they should deserve a negative bias. Your association of GNOME's icon style with an army (which army?) is interesting, nonetheless.\n\nCheers,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "guess it's because of all the brownish colours mixed with a touch of ocher. Reminds me of camouflage suits, too.... well, just a matter of taste..."
    author: "Thomas"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "My association of Gnome's icons with an army (no specific army) is as Thomas mentioned before due to their choice of brownish and pale colours. Can't help it, it's just the feeling I get.\nBut aside from that, I don't think it is a good idea to mix gnome and crystal icons - this would be really inconistent. Also with the default crystal icons used in the KDE environment, koffice would look alien when using all-gnome or a mix of gnome style icons, as abiword or openoffice do now. One thing I like about koffice is, appart form its speed and frame-orientation, the perfect integration into the KDE environment. Like konqueror, this browser perfectly fits into KDE and that's why I use konqueror and not mozilla, even so the latter is still a slightly better browser when it comes to displaying web pages correctly."
    author: "Tuxo"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "Interesting association. I prefer it because it uses more \"Earth tones\" (my perspective), which armies tend to use because it helps them blend in with the surroundings. Nonetheless, speaking about completeness is key, and again the question is who will complete the Crystal icon set. Unfortunately, KDE as a whole already is wildly inconsistant, as the Crystal icons (now the defaults) do not replace even half of the old HiColor defaults, so you have a big mixture already. Also, many distributions feel they need to add artistic touch and change icons (Mandrake, Redhat), producing more inconsistancy through the desktop, not just KOffice. I'd rather have inconsistant icons than buttons and menu items without icons at all (the current situation). That's what this boils down to, and needs to be discussed. If you have any concerns, come join us in koffice-devel and kde-usability.\n\nHave a good weekend,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "I agree with you that the mixture of HiColor and Crystal icons in KDE is not a pleasant situation and something has to be done about it. Like you, I also prefer to have iconistant icons than none at all.\nCurrently I am finishing up my thesis, so I should actually not be over here discussing, but writing up my thesis ;-) Once I am finished, I hope to join in not just with words!\nTill then, have a nice weekend too!"
    author: "Tuxo"
  - subject: "Re: Icon Contest"
    date: 2003-08-16
    body: "I generally agree with you, but did you actually look at the open office icons mentioned? I think they look quite neutral, and most of them might has well have been from hicolor.\n\nI don't think it is a question of either or though, those icons that fit in with the rest of the koffice icons, yet are better than the currently existing ones, could simply be imported. The rest could be ignored.\n\nThen if crystal icons are ever created for koffice, then they could all be replaced. This would fix the situation now, while not having to mindlessly replace the icons that are actually working just fine."
    author: "Troels"
  - subject: "Re: Icon Contest"
    date: 2003-08-16
    body: "You are right, it doesn't have to be an either or case, icons that fit in could be replaced. However, I am afraid that it will lead to one of those famous permanent provisional (and sub-optimal) arrangements."
    author: "Tuxo"
  - subject: "Re: Icon Contest"
    date: 2005-11-11
    body: "You are missing the point.  He is not asking you if you like the Gnome icons he is saying that he likes them and wants to know how to use them in KDE.  If you know the answer to that then answer, don't however tell him that he shouldn't want it just because you don't.\n\nFor what it is worth I also would like to find an easy way to use Gnome icons and themes in KDE.   My desktop runs KDE because there are some things that I don't like about Gnome and some things that I really like in KDE,  but I do like the Gnome icons and themes."
    author: "Jon"
  - subject: "Re: Icon Contest"
    date: 2003-08-16
    body: "BTW there is a script that you can use in order to change the Icons of OO to Crystal."
    author: "Gerd"
  - subject: "Re: Icon Contest"
    date: 2003-08-15
    body: "So what are those lovely icons in the latest Mandrake betas?  Huge improvement on the old ones, my vote goes with them..."
    author: "John"
  - subject: "Compiling error when compiling kudesigner_lib"
    date: 2003-08-15
    body: "I get the following:\nmycolorcombo.cpp:83: error: `createStandardPalette' undeclared (first use this\n   function)\n\nAnd then a lot of other errors.\nDoes anyone get the same errors?"
    author: "Jarl E. Gjessing"
  - subject: "Re: Compiling error when compiling kudesigner_lib"
    date: 2003-08-15
    body: "Never mind!\nI think it happened because I used to many optimation flags.\nWhen I did a configure --prefix=... only then it worked."
    author: "Jarl E. Gjessing"
  - subject: "KWord Tables"
    date: 2003-08-15
    body: "Am I the only person whose KWord shows really strange behaviour when using tables? (no redraws, frequent crashes etc)"
    author: "Arno Nym"
  - subject: "Re: KWord Tables"
    date: 2003-08-15
    body: "KWord does not do tables, it just simulates them. It does not work well ;)"
    author: "Anders"
  - subject: "Re: KWord Tables"
    date: 2003-08-15
    body: "No I too.  I abandoned Kword just for \nthis reason. To use ttables was horrible in 1.2\nDon't know if it has improved in 1.3 beta.\n"
    author: "Asokan"
  - subject: "Re: KWord Tables"
    date: 2003-08-15
    body: "No, it didn't (did not try beta 3 though)."
    author: "Jeff Johnson"
  - subject: "Re: KWord Tables"
    date: 2003-08-15
    body: "I tried KOffice 1.3 beta 2 but did return to OpenOffice for this very reason"
    author: "Jan"
  - subject: "Picking up speed."
    date: 2003-08-15
    body: "I've been pleased to see KOffice seems to have been picking up a bit of speed recenly in terms of development. Kexi looks set to be great, it's nice to see someone back working on Kivio, and KSpread looks to be making good progress too. I know there was a shortage of developers at one point, is this still a problem?\n\nThe only app which still seems stuck is krita, which is a shame. Is any work being done on it? The website makes it sound fairly impressive, but it hasn't been released with KOffice for a while. \n\nAnyway, excellent work KOffice developers!"
    author: "David"
  - subject: "Photoshop"
    date: 2003-08-15
    body: "There is Photoshop, NP."
    author: "Photoshop"
  - subject: "Re: Photoshop"
    date: 2003-08-15
    body: "and The Gimp"
    author: "MaX"
  - subject: "Re: Photoshop"
    date: 2003-08-17
    body: "Gimp is G"
    author: "Anton Velev"
  - subject: "Re: Picking up speed."
    date: 2003-08-15
    body: "Looking at the KOffice apps, they could need more developers :)\n\nThere's definitely progress, but many features feel fragile (even in KWord), there are crashes from time to time and the apps lack polishment."
    author: "Arno Nym"
  - subject: "Re: Picking up speed."
    date: 2003-08-15
    body: "Yes, they need more developers.  Feel free to join in at any time :)\n/me spares the rants of people who complain about the completeness of a free poduct without contributing anything..."
    author: "ian reinhart geiser"
  - subject: "Re: Picking up speed."
    date: 2003-08-15
    body: "How can you know whether an anonymous person contributes to other projects?"
    author: "Arno Nym"
  - subject: "Re: Picking up speed."
    date: 2003-08-15
    body: "Personally I dont care... This is OSS, talk is cheap, when I see a patch from you Ill apply it.\n\nCheers\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "KDevelop 3"
    date: 2003-08-15
    body: "yeah, not quite on topic, but does anyone know what's going on with KDevelop 3? They've been working on it for very long time now, and I thought that they were at least nearing a beta release stage. Last news on the site is from May.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: KDevelop 3"
    date: 2003-08-16
    body: "You can see from the CVS activity boxes on their homepage that development is going strong.  As for homepage updates, I suspect most developers can't be bothered, they'd rather code.\n\nLast time I tried Gideon was alpha4, and it was pretty stable and quite usable then.  I think KDevelop 3 will be a fantastic release, and well worth the wait."
    author: "Haakon Nilsen"
  - subject: "Re: KDevelop 3"
    date: 2003-08-16
    body: "KDevelop 3 is going to be released at the same time as KDE 3.2 so it's using the same release plan"
    author: "Peter Simonsson"
  - subject: "Re: KDevelop 3"
    date: 2003-08-16
    body: "Right now I'm using some week- or two old CVS build of Galeon - \nand it's been absolutely superior. I'm quite picky about my DevEnv, and right now the only thing that bugs me off slightly is Gideon's inability to reopen man pages on startup. Other than that - it absolutely rocks. \n"
    author: "Yarick"
  - subject: "Re: KDevelop 3"
    date: 2003-08-16
    body: "YEah the new GAleon is far better than the old one and yeah KDevelop is going to rock.\n\nBut is it really as good as Visaul C++ .net or 6?"
    author: "Dan"
  - subject: "Re: KDevelop 3"
    date: 2003-08-16
    body: "You are absolutely right! The new Galeon is a much better web browser than Kdevelop ever used to be (scnr) ;-)"
    author: "Tuxo"
  - subject: "Great work!"
    date: 2003-08-18
    body: "I must say that KOffice looks really promising.\nWith it's integration, speed. Great work guys."
    author: "KDE User"
  - subject: "Re: Great work!"
    date: 2003-08-18
    body: "Kivio looks quite good also. Judging from the screenshots.\nIs there any application that can generate\nUML-diagrams from cpp-sourcecode?"
    author: "KDE User"
  - subject: "Re: Great work!"
    date: 2003-08-19
    body: "The problem is, that it has been looking 'promising' for already more than two years or so."
    author: "Ronald"
  - subject: "Re: Great work!"
    date: 2003-08-19
    body: "True, so you better help out."
    author: "Datschge"
---
 On August 14th 2003, the KDE Project released the <a href="http://www.koffice.org/releases/1.3beta3-release.php">third beta version</a> of <a href="http://www.koffice.org/">KOffice 1.3</a>. It brings a lot of bugfixes and a couple of new features compared to KOffice 1.3 Beta 2. This release is the last beta in the 1.3 series. There will be only <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">one more release candidate</a> and the final version is expected to be released in September after the <a href="http://events.kde.org/info/kastle/">KDE Contributor Conference</a> during which hopefully many of the remaining bugs will be fixed. Read more in the <a href="http://www.koffice.org/releases/1.3beta3-release.php">KOffice 1.3 Beta 3 release notes</a> and in the detailed <a href="http://www.koffice.org/announcements/changelog-1.3beta3.php">KOffice 1.3 Beta 3 changelog</a>. <a href="http://download.kde.org/unstable/koffice-1.2.92/">Binary packages</a> are expected to be available soon, for now you can only <a href="http://download.kde.org/unstable/koffice-1.2.92/src/">grab the source</a>.
 
<!--break-->
