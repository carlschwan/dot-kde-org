---
title: "KDE Traffic #59 is Out"
date:    2003-07-23
authors:
  - "rmiller"
slug:    kde-traffic-59-out
comments:
  - subject: "*sigh*"
    date: 2003-07-22
    body: "That was a slow and somewhat boring KT with less than no content - only one thing was even KDE related... *sighs*\n\nAnyway, can't complain too hard, I don't want the job *grins*\n\nTroy"
    author: "Troy Unrau"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "That kroupware dig is hilarious.  :-)"
    author: "ac"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "KDE development is neither slow nor boring. Either too few mailing lists are covered or too many threads are skipped compared to previous glorious Kerner Cousin KDE times."
    author: "Anonymous"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "Feel free to cover things you find interesting yourself.  Send them to me in proper XML format and I'll include them if they pass my editorial muster.\n\nSo far Juergen Appel and Fabrice Maus have been the only contributors since I started this - and their contributions were/are appreciated.\n\nOtherwise, quit complaining.  I only cover threads that I consider interesting, that's part of the job.  If there aren't any, well, that's the way the cookie crumbles.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "If there aren't any or not enough skip KDE Traffic publication for this week and don't redistribute spam sent to the mailing-lists. That's the ultimate low point of KDE Traffic!"
    author: "Anonymous"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "I'm sorry you feel that way.\n\nI thought picking apart the spam would be entertaining.  I knew some people wouldn't agree, that's part of trying something controvertial.  I may not do it again, but I don't regret the experiment.\n\nHowever, it's your choice whether to read KDE traffic.  I do this on a volunteer basis, ie, I don't get paid for this, I do this because I use KDE, like KDE, and want to contribute to the KDE project.  If you think you can do a better job, why did you not step up to the plate when no one was doing it?  It's always a lot easier to sit on the sidelines and snipe than it is to step up to the plate and actually do something about it.\n\nTo the rest of you, please don't misinterpret this to be against constructive criticism.  I really am listening to you, and if the feedback is too negative, I will consider today's experiment a failure.  But, please, be civil about it.  Calling this the \"low point\" of KDE Traffic and talking about how far it's fallen from its \"glory days\" really doesn't do a whole lot to give me incentive to continue this task.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "Ignore 'em Russell. Keep doing what you're doing. This is the only reason I check this site so regularly. Good writing."
    author: "Joe"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "I, for one, would prefer a late but contentful KDE traffic compared to your last few weeks. You cover too little stuff. When I read kernel traffic or wine traffic (I don't read GNUe) I'm satisfied. When I read KDE traffic, I feel I need to read all mailing lists myself if I want to get a sense of what is going on. I don't have time for THAT. The dilemma is that if one can fix/help you with KDE traffic, he doesn't need KDEt in the first place. So the scratching an itch idea doesn't really work."
    author: "nusuth"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "That's not completely true of course. You can help out by covering one or two of the lists you monitor. If a couple of others do that too for the lists _they_ monitor, all lists will be covered for everybody in the end. The argument you give goes for Russell himself too: he doesn't need KDE traffic for himself, but maintains it so others can use it. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "<I>The argument you give goes for Russell himself too: he doesn't need KDE traffic for himself, but maintains it so others can use it.</I><P> Yes, it does. Perhaps the question is why doesn't other *traffic suffer from the same problem and what can be done to imitate their success on KDE traffic. If Russell makes a case of more submitters to other *traffics, I can volunteer for monitoring and mining for interesting threads (but not for writing summaries) on a list or two. "
    author: "nusuth"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "Oh please... I love KDE Traffic, and I don't care at all wether it is thin some weeks or if Russel hasn't had time to do it. I understand that it is something he does for us all and for fun, and so should you. Shut up or help out.\n\nI actually thought the spam comment was pretty fun. :)"
    author: "Chakie"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "Thanks for that :)\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "Russel, I really like your KT, since I don't have time enough for keep in touch with the kde development and KT gives me the opportunity to feel inside the KDE community without spending much time.\nReally thanks,\n Ricardo"
    author: "Ricardo Cruz"
  - subject: "Thanks!"
    date: 2003-07-23
    body: "I don't usually post, but I wanted to say Thanks for taking the time to do these.  I think doing a fun one every now and again is perfectly fine.  The first question I have for people complaining is \"where are your submissions?\".  Its easy to complain, but that isn't what this is about.. so if you want more coverage of KFooBar or something, write up an article and send it over.  Its something called being constructive, and not just whining.\n\nThanks again!"
    author: "elemur"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "In fact the comments to the spam were quite funny."
    author: "Stefan Heimers"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "You may find the usability list interesting.\n\nI'd be happy to help if you could tell me how to do \"proper XML format\" and what kind of things would \"pass your editorial muster\".\n\n--\n\tHenrique Pinto\n\tstampede at lagoaminas dot com dot br"
    author: "Henrique Pinto"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "Take a look at one of the documents in the kckde module in CVS.  One article goes in between the section tags.  Please fill in as much info as is appropriate and remove blank attributes.\n\nAs far as passing editorial muster, I tend to grant large amounts of leeway to contributors - especially for those lists that I don't subscribe to or cover.  A successful contribution will have correct grammar and spelling (except for the quotes, of course), be informative, and pertain directly to a topic on the list.  I may make exceptions to that on a case by case basis.  What you wish to cover is strictly a matter of what is interesting to you - I will rarely if ever reject a contribution based on content as long as it's an accurate reporting of something that happened on the list (and within the bounds of good taste ;).\n\nOh, and please don't cover flamewars.  Unless they are *really* good.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "It's a choice - a newsletter with one entry or a newsletter with three entries, two of them being silly.  Considering I'm a silly kind of person and I don't think that this project should take itself too seriously (see my editorial <a href=\"http://www.duskglow.com/phpBB2/viewtopic.php?t=54\">here</a>) I see nothing wrong with going a little bonkers every now and then.  Rest assured that if there is something going on that I find interesting it will get covered.  Also, as I mentioned elsewhere, feel free to contribute your own stories.  Generally I defer a lot to the judgement of contributors, even if I don't find it interesting myself, that doesn't mean that I won't put it in the newsletter."
    author: "Russell Miller"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "> It's a choice - a newsletter with one entry or a newsletter with three entries, two of them being silly.\n\nI choose no newsletter at all for this week."
    author: "Anonymous"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "That's your choice whether to read it or not.  However, as far as publishing goes, that was my choice, and I stick by it.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: *sigh*"
    date: 2003-07-22
    body: "Thanks Russ,\n\n\n   We went through this exercise once before: some prefer to wait and get lumped news, and some prefer frequent news whether or not its full of exciting changes.\n\n   I'm glad you keep us regularly updated... whether there's news or not. Thanks for keeping it all in good humor too.  Good stuff.\n-jf"
    author: "Jeff"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "Full ACK. It's always nice to have something to read.\nSometimes it's fun sometimes it's useful and sometimes both.\nAnd if it's nothing of all that, well, I come back next time...\nIt's for free and to the folks who are saying \"Don't waste my\ntime with your free stuff\": Just don't read it. If you are afraid you\nmight miss something useful: Well, that's your problem. You might\nstart your own Kernel traffic with UsefulNewsOnly (TM).\n"
    author: "Martin"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "Let me add myself to the list of thankers (now, there wouldn't be a word like this, would there? ;-)\n\nKeep up the good work.\n\nPS: It's summer after all, so those disgruntled people should get out and take a cold bath in the sea ..."
    author: "Stefan Moebius"
  - subject: "Re: *sigh*"
    date: 2003-07-23
    body: "The musicman was covered well and very interesting. I want to know when it's going to be merged :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Quanta was so slow I barely had time to read this"
    date: 2003-07-22
    body: "If your feed is so slow then add the Quanta list. We always have something interesting going on. This last week I could hardly get anything else done and often woke up to 30 emails on various topics. We're making all kinds of changes... actually I'm a little surprised Fabrice isn't covering it.\n\nBTW those topics included new XML tools, DockBook support issues, a Kommander rich text editor for blog tools, Javascript support issues, new file issues with VPL and VPL design criteria review, Kommander script objects and a lot more. Perhaps not interesting... I don't know. But it was not slow by any means in our neck of the woods."
    author: "Eric Laffoon"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-22
    body: "Interesting thought.  I'll consider it.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-23
    body: "Eric,\n\njust one thing interesting for me (as it's for KSpread the same):\n\nDo the scripts generated with Kommander need to be GPL as well?\nI mean they are somehow bound to QT and therefore they must be GPL as well, right?\n\nPhilipp"
    author: "Philipp"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-23
    body: "Interesting question, and I'm not a license expert, so correct me if I'm wrong, but the GPL is not enforced on the result document created with a GPL'd app. You're KWord documents are not enfored to be released under GPL, the code written in KDevelop isn't either, so I beliebe the UI files created with Desginer or Kommander can be of any license. \n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-25
    body: "You are correct about the Word document, in case it doesn't contain any script.\nSame for KSpread, as long as we don't contain scripts, the file is not bound to a license.\n\nBut with scripts it's getting harder.\n\nFor KWord this issue is not that important, as not a lot of text documents do need to contain scripts. But for a spreadsheet app scriptability is rather a must.\n\nAs the script interpreter is with QT a GPL'ed interpreter, the script must be GPL as well? That's the issue.\n\nPhilipp"
    author: "Philipp"
  - subject: "Re: Quanta was so slow I barely had time to read t"
    date: 2003-07-23
    body: "Kommander generates (what sums up to be) a text file. That text isn't really bound to Qt, as any (knowledgable) person could make a program to read the files and\noutput whatever they like. This is much like using, say, emacs to write something.\nThe resulting file isn't required to be GPL'd.  "
    author: "Jilks"
  - subject: "Re: Quanta was so slow I barely had time to read t"
    date: 2003-07-24
    body: "yeah, but after all when you create a qt program you also create 'textfiles' (cpp and h files really) that can be linked to any library that is QT-compatible ...\n\n(not really arguing, just thinking aloud)\n"
    author: "ik"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-23
    body: "Hmm, a question I've never thought.\nIMO no, for the same reason that you can compile non GPL programs with gcc."
    author: "fredi"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-25
    body: "GCC is not comparable, as it is only the compiler. The compiler is not bound to the resulting binary. The binary is bound to eg. libc which is LGPL.\n\nNow Kommander is here the interpreter and the interpreter is bound to QT which is GPL -  so does it affect it?\nIf you eg. read the license FAQ for QTScript then trolltech states that the script must be GPL as well.\n\nComparable is here also Java and GPL'ed programms. The FSF says rather no - AFAIR."
    author: "Philipp"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-23
    body: "Eric,\n\nwhy not make a Quanta Weekly News? I would read it and like to see it posted here as well.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Quanta was so slow I barely had time to read this"
    date: 2003-07-23
    body: "That's a good question. Actually I'm looking for people to help with docs, tutorials and articles. I've thought along this line and because we have so much going on this might be a good idea. However I don't want to take away from any of the other digests out there so I'd welcom feedback on that. If anyone is interested in getting involved let me know."
    author: "Eric Laffoon"
  - subject: "musicman install"
    date: 2003-07-23
    body: "I've just compiled musicman successfully on my gentoo box but I can't see it on konqueror :(\nhow can I get it to work?"
    author: "pat"
  - subject: "Re: musicman install"
    date: 2003-07-23
    body: "a guess: you have compiled it with the default --prefix=/usr/local, and your /usr/local is not part of $KDEDIRS ? \n"
    author: "ik"
  - subject: "Re: musicman install"
    date: 2003-07-23
    body: "I didn't use --prefix, it's all in my /apps/khtml/kpartplugins and /usr/kde/lib with all the other kde libs"
    author: "pat"
  - subject: "Re: musicman install"
    date: 2003-07-23
    body: "From the screenshot it seems that you get a sidebar pane with musicman, you don't, it is only available in the right click context menu in the iconview pane.\n\nIf that was your assumption like mine that is :) otherwise apologies."
    author: "MxCl"
  - subject: "Re: musicman install"
    date: 2003-07-24
    body: "I just checked protage and it's not there, so I have to assume you did it \"manually\".  If so, what you need to do is (in my case, I don't know what version of KDE you're running):\n\n--prefix /usr/kde/3.1/\n\nWhen you compile and then it will show up and act as it should and all that stuff."
    author: "Xanadu"
  - subject: "Re: musicman install"
    date: 2003-07-25
    body: "I've compiled with and without --prefix but the result was the same. I've noticed that the plugins for konqui are in /usr/kde/3.1/lib/kde3/ so I tried to move libmusicman.* there but with no success :("
    author: "Michele"
  - subject: "Re: musicman install"
    date: 2003-07-25
    body: "It's not the prefix but the makefile has some errors.\n(see thread http://lists.kde.org/?l=kde-devel&m=105780050905118&w=2)\n\nFor example you have to install the .desktop file by hand into $KDEDIR/share/services/.\n\nChristian"
    author: "cloose"
  - subject: "Re: musicman install"
    date: 2003-07-25
    body: "woooooah! Now it works! Thank you :))"
    author: "Michele"
  - subject: "50% off notebook pc"
    date: 2003-07-28
    body: "You call 128MB of RAM decent???  Maybe a year ago.  I would call 256 MB decent.  I'm looking for a new notebook myself in the next couple months, and I won't settle for less than 512MB, ideally 1 GB (then again, I also have 2 GB of RAM on my desktop, so I guess that says a little something about my thoughts on RAM).\nBut seriously, Those few times I boot into WindowsXP, the POS is using over 260 megs of RAM just sitting there idle, no apps running.  I don't know if this has anything to do with my 2gb of ram tho, it seems like it uses more ram for everything when you have more system ram to start with...\n\nIn any case, lots of ram is important.  Otherwise, how can you keep 14 apps running at once without slowing anything down like I'm doing right now?"
    author: "Michael Dean"
---
<a href="http://kt.zork.net/kde/kde20030722_59.html">KDE Traffic #59</a> is out.  As it has been a slow news week, we cover a couple of fun things, as well as the <a href="http://avi.alkalay.net/software/musicman/">MusicMan plugin</a> for Konqueror.  Get it at <a href="http://kt.zork.net/kde/latest.html">the usual place</a>.
<!--break-->
