---
title: "Siddhu Warrier: Setting up Samba"
date:    2003-06-10
authors:
  - "numanee"
slug:    siddhu-warrier-setting-samba
comments:
  - subject: "hmmm..."
    date: 2003-06-10
    body: "Setting up a Samba server eh? If there's any software that you really don't need on a Samba server, it's X and KDE. \"caveat emptor\" alright - you've installed too much."
    author: "foo"
  - subject: "Re: hmmm..."
    date: 2003-06-10
    body: "Correct, if the samba-server is on a separate computer.\nBut in most environments I work, the Samba-server is one of the desktop computers in the network.\n\nRinse"
    author: "rinse"
  - subject: "Re: hmmm..."
    date: 2003-06-10
    body: "And to add to that, I believe that if you want access to *all* the desktop Linux boxes from Windows, you need to install a Samba server on each and every one of them."
    author: "Navindra Umanee"
  - subject: "Re: hmmm..."
    date: 2003-06-10
    body: "Indeed, if you want others to view/use some file/printer shares on your desktop, you'll need to run a samba-server on it, right?"
    author: "cies"
  - subject: "Re: hmmm..."
    date: 2003-06-10
    body: "No,\n\nin case of Win <> Win9x you only need cups. Samba can add here additional functionality, but for basic printing Samba is not needed."
    author: "Philipp"
  - subject: "Re: hmmm..."
    date: 2003-06-23
    body: "> in case of Win <> Win9x you only need cups.\n\nfor printing on the network and allowing others to do so at your computer+printer...\n\nBut for reading/modifing files from the 'shared folder' of your collegue? You need smbmount, right? And for sharing files on the network? You'll need samba (smbd), isn't it?"
    author: "cies"
  - subject: "Re: hmmm..."
    date: 2003-06-10
    body: "Calm down, people!!  You need to configure the Samba server in order to use the Samba *CLIENT*, which is normal for a desktop machine to run."
    author: "stunji"
  - subject: "KSambaPlugin"
    date: 2003-06-10
    body: "I have no idea what any of that had to do with KDE.  He used a KDE text editor to edit the config file.  Wow!  He used Konqueror to change the permissions of a file.  Ooo!  Everything else was done at the command line.\n<p>\nThere is a project called KSambaPlugin that offers a much better solution.\n<p>\n<a href=\"http://apps.kde.com/na/2/info/id/1715\">http://apps.kde.com/na/2/info/id/1715</a>\n<p>\nUnfortunately, version 0.4b didn't work for me on Gentoo Linux.  I had to edit the config file by hand.  Version 0.4.2 is out, but I haven't tried it."
    author: "Mike"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-10
    body: "Well, the fact is that KDE is part of a much larger system.  That's where the whole KGX thing comes from.  Sometimes even a KDE user has to look beyond the confines of KDE to get something done.  Otherwise, this was a more or less experimental feature and will hopefully be informative and useful to at least some KDE users."
    author: "Navindra Umanee"
  - subject: "Re: Article"
    date: 2003-06-10
    body: "Well, at least he wrote something useful.\nI can imagine this might come in handy to someone. Even if it's not that kde related. He's not that experienced with linux (he says so himself).\nSo give him a break. Nevertheless I thought this was a decent article for newbies. It provides step-by-step and not too much detailed info (which could be confusing).\n\n"
    author: "dwt"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-10
    body: ">I have no idea what any of that had to do with KDE.\n\nOne obvious answer: how many KDE newbies know how to connect to their local networks using KDE? How many know about \"lisa\"? How many know that there are things like \"fish://\"? Very few, only those that have people helping around. Not all KDE users have 10 years of experience programming kernel modules ;-)\n\nNot only I think this is indeed KDE related. I really wish this kind of manuals were included somewhere in the KDE website. Don't think of KDE as a set of applications based on Qt. Think of it as a Working Environment.\n\nHow about more manuals like this?: How can I edit music files? How can I watch DVD's in kde? How can I set-up my e-mail? How can I write CD's? How can I .... I'm sure KDE newbies would love them. \n"
    author: "uga"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-11
    body: ">I have no idea what any of that had to do with KDE.\n \nI have had several versions of Mandrake on my machine and always used the KDE  configuration, meaning I have nothing to compare it to. I don't know if it is signifigant, but I threw Window out when it was \"Windows for 386\" - 12 / 15 years ago. My question is how do I tell what is Linux, what is Mandrake, or what is KDE or is that really important except to tell the more experienced what I am using ?"
    author: "BillM"
  - subject: "Re: KSambaPlugin"
    date: 2004-02-03
    body: "I am a linux newbie.  I need Samba to connect my WinXP to my Fedora Core 1 linux machine (have you ever heard of mythtv?  I want to share the files b/t upstairs and downstairs computers).  Anyway, since I use KDE, I looked at www.kde.org to see postings/tutorials about Samba.  FWIW, I agree with the 'How do I xxxx on KDE' approach.  \n\nIf you want linux to be a more pervasive OS, you have to lower the barrier to entry.  Flexibility and an anything-but-MicroSoft attitude is not enough to entice the masses.  \n\nWhat's lisa, fish, QT?  Got me."
    author: "GKL_Linux"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-10
    body: "To the previous posting, I think I should add this: Why not put this kind of stuff better in KHelpCenter? \n\nMy \"helpcenter\" seems to be explaining how to use KDE for those that have no experience with GUIs: \"A Window! What now?\", \"Using the Taskbar\".\n\nMy mom didn't ask me such questions ever. She only asked my how to read e-mail, how to write letters, how to print... that was it! People learn using taskbars intuitively in my experience.\n\nEvery new KDE user that I \"recruited\" had their questions related to multimedia stuff. Most know their way either in windows, mac, gnome... Then, they want to know about networking and e-mail, and last, how to install new software. I think those help areas are not really covered in the help of kde.\n\nThey don't want to know anything about areas covered as: MIME Types, applets,... and what does \"kioslave\" mean for a newbie??????\n\n\n"
    author: "uga"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-10
    body: "uga,\n\nFollow this thread on the kde-doc-english mailinglist. \nhttp://lists.kde.org/?l=kde-doc-english&m=105249217001130&w=2\n\nAlso I have (re)written/translated several taskoriented howtos in dutch. I plan to translate them soon to english. I need some help from people who can proofread en correct my english. *hint*\n\nAnyway .. there is enough stuff on the internet but it is mostly a bit outdated. Just find a howto and make it up-to-date with current KDE version. \n\nYes anybody can do this  :o) \n\n\nFab"
    author: "Fab"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-10
    body: "uhmmm... that sounds interesting proposal.... but, I'm not a native english speaker, so I'd rather not correct anyone's english ;-)\nI'll have a look what I can do. Thanks.\n\tUnai"
    author: "uga"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-11
    body: "hey i'd love to edit those.  I am a native speaker, and although not a very good one, i think i can still be of help.  a good way to tset your howtos is give them to a n00b and let them see if they understand them :)"
    author: "standsolid"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-11
    body: "standsolid,\n\nPlease contact me by mail if you want to help. \n\ngrtz'\n\n\nFab"
    author: "Fab"
  - subject: "Re: KSambaPlugin"
    date: 2003-06-12
    body: "<i>I have no idea what any of that had to do with KDE.</i>\n\nMy question exactly!\n\nHe also uses mount (with no indication that you need to be root to use mount) when normal users may have to use smbmount, but he could use komba2 or kio_smb to access the share, and setup lisa to browse the network!\n\nWhy anyone would suggest running ICS on a Windows machine to get DHCP when you can have a firewalled ICS/DHCP box with any linux distro beats me.\n\n<i>Unfortunately, version 0.4b didn't work for me on Gentoo Linux. I had to edit the config file by hand. Version 0.4.2 is out, but I haven't tried it.</i>\n\n0.4.2 works fine, and is in contribs for Mandrake 9.1.\n\nMandrake 9.2 should have Windows network browsing in KDE working out-the-box ..."
    author: "ranger"
  - subject: "Having to edit text files?"
    date: 2003-06-10
    body: "This brings up a bigger point is why you have to edit text files in the first place. Shouldn't the samba sharing be built into Konq? Shouldn't a user just be able to right click on a folder and select 'sharing' like in Windows and it be automatically shared?\n\nJust some thoughts on usability.\n\n/b"
    author: "Glen Barnes"
  - subject: "Re: Having to edit text files?"
    date: 2003-06-10
    body: ">Shouldn't a user just be able to right click on a folder and select 'sharing' like in >Windows\n\nHave you tried to do it? Mine does:\n\nRight click, Share (or Properties)->Local Net Sharing"
    author: "uga"
  - subject: "Re: Having to edit text files?"
    date: 2003-06-10
    body: "\"Local Net Sharing\" is too limited: ksambaplugin is your friend, right click on a map, then share, then go to the samba tab, and fill in everything want ( users, guests, rwx. it has everything windows does, and more )"
    author: "Mark Hannessen"
  - subject: "Re: Having to edit text files?"
    date: 2003-06-10
    body: "I just saw the project page in sourceforge. It looks nice. Is this anywhere in the kde cvs? Is it going to be merged into 3.2?"
    author: "uga"
  - subject: "Re: Having to edit text files?"
    date: 2003-06-11
    body: "Hi, \nI'm one of the (two) developers of KSambaPlugin.\nIt is in kdenonbeta and called kcm_sambaconf.\nCurrently it is not ment to go into 3.2.\nHowever, I think it would be possible to put it\ninto 3.2. The main problem now is the translation.\nIt is not ready to be translated as it doesn't\nhandle translated Samba parameters correctly.\nBut if the majority wishes to have it in 3.2\nI will do my best to correct these bugs.\n\nGreetings,\n\nJan Schaefer"
    author: "Jan Schaefer"
  - subject: "Re: Having to edit text files?"
    date: 2003-06-11
    body: "I'm one of yours !\nI would love to have this in 3.2 : it's a great piece of software.\nBye\n"
    author: "Hundge"
  - subject: "Re: Having to edit text files?"
    date: 2003-06-11
    body: "Who doesn't want it in 3.2? He must be using Windows! ;-)\n\nGood work! Thanks to both of you.\n\n\tUnai"
    author: "uga"
  - subject: "Re: Having to edit text files?"
    date: 2003-07-01
    body: "I have SuSE 8.2 and its not working. For non root user even if i say share file it dont do so. When in control center Internet and Network -> File Sharing i select \"allow users to share files\" and apply changes and then restart kcontrol i see its still \"do not allow users to share files\" inspite of fact that i allowed users. This seems to be some bug please look in this. \n\nPlease include it in 3.2 also as its good and very user friendly."
    author: "Nilesh Bansal"
  - subject: "what's up with LISa?"
    date: 2003-06-10
    body: "Can anybody tell me how this fits into the picture of LAN connectivity?\n\nDoes LISa rely on other libs/demons?\n\nThanks in advance..."
    author: "cies"
  - subject: "Re: what's up with LISa?"
    date: 2003-06-10
    body: "Not at all. Lisa is a network of port scanners, each running as a daemon. Lisa itself does not depend onfurther daemons for scanning. The only relation is that a Lisa daemon may discover a SMB server, like Samba."
    author: "AC"
  - subject: "Re: what's up with LISa?"
    date: 2003-06-11
    body: "Uhm, LISa only scans a network of hosts discovering TCPIP services running (not only Windows shares).\n\nBTW, I don't like LISa for newbies. It's hard to setup, it's hard understand their purpose, etc. There are a new SMB browser called Smb4K (I suppose SMB for KDE) in apps.kde.com.\nIt really works! Maybe will be a good idea to incorporate this code in the smb:/ kioslave.\n\nRegards."
    author: "Mauricio Strello"
  - subject: "hmmm"
    date: 2003-06-10
    body: "Tried it.  Didn't work for me.  but my machine doesn't have 'mksmbpasswd.sh' or anything like it.  Or the command 'service'.  I guess thats RH specific.  It is a good idea, though.  Things like setting up samba are useful, and not always straightforward.  Try man smb.conf some day... its enormous!  Who's gonna read all that?  Any other ideas?"
    author: "Bill"
  - subject: "Re: hmmm"
    date: 2003-06-10
    body: "What distro are you using?\nI have it here also in mandrake. The file is provided by the package samba-server-2.2.8a-5mdk\n\n\n\n"
    author: "uga"
  - subject: "Re: hmmm"
    date: 2003-06-11
    body: "Slack 9.0"
    author: "Bill"
  - subject: "Re: hmmm"
    date: 2003-06-11
    body: "This might help you. This is the contents of the script (I hope it's shown properly formated)\n\n==========================================\n#!/bin/sh\nawk 'BEGIN {FS=\":\"\n        printf(\"#\\n# SMB password file.\\n#\\n\")\n        }\n{ printf( \"%s:%s:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nXX:[U          ]:LCT-00000000:%s\\n\", $1, $3, $5) }\n'\n===========================================\n"
    author: "uga"
  - subject: "On a somewhat related note...."
    date: 2003-06-11
    body: "Does anyone know how to get KDE to prompt for samba passwords?\nI can add an entry for a samba share in fstab, and it automagically appears on my KDE desktop, and opening it runs the mounting helper. Because I'm a paranoid little man, I don't want to put the password for the samba share in the fstab and would like KDE to prompt me for a password. Does anyone know if this is even possible with KDE at the moment? I remember seeing it somewhere, although it could have been the medication talking."
    author: "Hiren"
  - subject: "Use pam_mount instead"
    date: 2003-06-12
    body: "Sure, it's a bit more work to setup, but works great.\n\nAvailabe in Mandrake contrib since about 9.0. It will mount shares you specify in a config file (with macros/tokens allowing configs to apply to all users) with the password you log in with."
    author: "ranger"
  - subject: "Constructive Comment on possible missing step"
    date: 2003-06-11
    body: "Thanks for the informative article. I read it closely and either a step #7 is missing or things are mis-labeled after step #6."
    author: "JJ Hurley"
  - subject: "Linux Client to NT Server Domain & DHCP"
    date: 2003-11-19
    body: "You mentioned getting the IP from the NT server. What, if any,\ndifference is there if the IP is being assigned by a separate \nDHCP server, such as a Netgear RT-311? \n\nMany gateway routers do provide suh services and I find it is \none less item the server has to worry about. And of course, \nbecause it is a M$ product, one less chance to fudge up.\n\nI have one Linux box, called Ironsmith, that I would like to \nattach to an existing home NT domain network. The current \nNT home network has been in place for almost three years.\n\nStin\n"
    author: "Stin"
  - subject: "Thanks!"
    date: 2003-12-11
    body: "This document has really helped setting up printing for my RedHat box. Thanks a lot; everything else was just crap. This is the type of documentation that I love. Not difficult, not sickeningly easy; just right. Thanks again.\n\nCpt. Guido\n"
    author: "Captain Guido"
  - subject: "Failed to modify password entry for user ????"
    date: 2004-11-18
    body: "I followed your instructions, but I get the error Failed to modify password entry for user ?????.\n\nAlso - this file \"mksmbpasswd.sh\" does not exist."
    author: "Beauford"
  - subject: "Re: Failed to modify password entry for user ????"
    date: 2004-12-07
    body: "this is very urgent plz send me a better solution"
    author: "hari"
  - subject: "Re: Failed to modify password entry for user ????"
    date: 2007-03-05
    body: "I found that the error \"Failed to modify password entry for user\" occured when I didn't have a matching system user of that same name.\n\nFix: Create a system user with something like:\n\nadduser myuser\n\nSee if this solves the issue for you.\n\n"
    author: "yumsus"
  - subject: "Re: Failed to modify password entry for user ????"
    date: 2007-12-02
    body: "gracias\n"
    author: "magally"
  - subject: "Re: Failed to modify password entry for user ????"
    date: 2007-12-21
    body: "Cheers that worked great :)"
    author: "marc"
  - subject: "Re: Failed to modify password entry for user ????"
    date: 2008-07-06
    body: "Chama te la comiste, bien por ti gracias me sacaste de un gran apuro\nsaludos"
    author: "Lester"
  - subject: "permissions on files"
    date: 2005-03-03
    body: "I have mounted a Windows share on my Linux box.\nI was root when I did it.\n(either smbmount or mount command)\n\nCan I let my users have access to these files?\nEverything seems to be readonly for everyone\nbut root.\nSince I dont use Root normally,\nI can only copy the files to my Linux box, edit them,\nthen su to root to copy them back to the windows box.\n\nI've tried to chmod the directory, but \nthat doesnt change anything.\n\nIs there a better way to do this?\n\nThanks\n"
    author: "Jabmist"
  - subject: "Perfect!"
    date: 2005-07-17
    body: "THANK YOU!! This was exactly what I needed. Before I found this tutorial, I was floundering -- Linux could get to Windows shares, but Windows couldn't access anything on Linux. Thanks for not assuming I have any clue what I'm doing, yet writing instructions I could follow. ;-)"
    author: "kalico"
---
KDE enthusiast <a href="mailto:siddhuwarrier@hotmail.com">Siddhu Warrier</a> has kindly contributed <a href="http://static.kdenews.org/content/siddhuwarrier/20030610/">a write-up of his experiences</a> with setting up a <a href="http://www.samba.org/samba/samba.html">Samba server</a> using Linux and KDE.  For those of you who don't know what Samba is, the bottom line is that with a little black magic, you can conveniently access files, directories and printers on a Linux machine from a Windows machine and vice-versa all over the network.  Intrigued?  Pull up your sleeves and <a href="http://static.kdenews.org/content/siddhuwarrier/20030610/">check out the article</a>.  Thank you, Siddhu, and to the rest of you: caveat emptor!
<!--break-->
