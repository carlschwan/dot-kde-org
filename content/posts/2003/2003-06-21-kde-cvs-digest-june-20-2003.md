---
title: "KDE-CVS-Digest for June 20, 2003"
date:    2003-06-21
authors:
  - "dkite"
slug:    kde-cvs-digest-june-20-2003
comments:
  - subject: "K3b vs Nero..."
    date: 2003-06-21
    body: "Hello,\n\nVery soon K3b will be slapping Nero's bald head in a mocking manner. It's a joy to see young apps mature in functionality, power and stability. It's synonymous to watching a young weak seedling grow into a formidable oak tree. Good job! :-)\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-21
    body: "what I don't like about K3b is that their logo bar in the middle takes up half of my 800*600 screen. Hope they'll remove that it's really annoying.\nother than that, great app!"
    author: "bob"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-21
    body: "Any reason why you chose to use such a small resolution?\nBesides that I agree that the logo bar should be possible to hide optionally for those who like to hide it for whatever reason, there's no wish report for it at bugs.kde.org so far though so one should create one."
    author: "Datschge"
  - subject: "800x600"
    date: 2003-06-21
    body: "I've old monitor. It works good, but it can display screen up to 1280x1024, but on refresh rate 60 Hz. So I select 800x600x85 Hz. What's wrong? IMHO KDE will use hardware resources in reasonable way. (In other words, why I need to upgrade so often?)"
    author: "gray"
  - subject: "Re: 800x600"
    date: 2003-06-21
    body: "Well you can probably run at 912x684 at around 80Hz, which should still be confortable, and be big enough for any KDE app's fat UI to not be a problem."
    author: "David Walser"
  - subject: "Re: 800x600"
    date: 2003-06-22
    body: "Just btw the cvs version of K3B allows you to turn the logo bar off. It was a welcome option for me, as it will be to many others it seems. And yeah, K3B is great. When I used Windows I used Nero, and when I first started using Linux I was very unhappy till I found K3B, which is everything I ever wanted in a burning program."
    author: "lrandall"
  - subject: "Re: 800x600"
    date: 2003-06-22
    body: "Just go to settings->show document header, and disable it. you may want to turn off the media player as well."
    author: "Peter Backlund"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-22
    body: "Actually, many people in poor countries still use 800x600 resolution. And even college students that can't pay themselves a new monitor do.\n\nSo I think it's still too early to consider 800x600 outdated, and a smaller logo should be considered. Plus I happen to hate big logos taking so much screen space :("
    author: "Pawn"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-22
    body: "it shouldn't matter WHAT resolution you run.  GUI layout should be dynamic.  And huge logos have no place in the middle of an app, anyway; that's what about boxes, docs, and websites are for."
    author: "Lee"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-30
    body: "Hear, hear!\n\nThe resolution I use is MY choice, and not the app's designer.\n\nK3b Setup is the worst offender I've ever seen.\n\nPlease, use moderation with logos.\n\n\nCheers,\n\nCarlos Cesar"
    author: "ccp"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-22
    body: "> Actually, many people in poor countries still\n> use 800x600 resolution\n\nIn rich countries too, for example my old iBook here in Switzerland ;-)"
    author: "Stefan Heimers"
  - subject: "Re: K3b vs Nero..."
    date: 2005-03-13
    body: "Hey, I buy a new $3000 desktop every 1-2 years, but I see myself staying at 800x600 forever.  Maybe it's because I've been computing in the 80x25-char console interface for a decade, but for me it's a matter of principle: you shouldn't need higher rez except for quality games (which I'm not into).  When you spend 16 hours a day reading / writing text, higher resolution only increases eye strain."
    author: "Alex"
  - subject: "Re: K3b vs Nero..."
    date: 2005-03-13
    body: "So you still use a 8-pins dot-matrix printer for printouts, too? Not a snazzy, lovely 600dpi laserprinter? Because if the size of the glyph stays the same, the number of dots that make up the glyph are important.\n\nMe, I've got a 1600x1200 15\" panel on my laptop -- 133 dpi is nice, no laser printer quality, but every glyph looks printed compared to any screen I've used before."
    author: "Boudewijn"
  - subject: "Re: K3b vs Nero..."
    date: 2005-03-14
    body: "Where do you get glyphs that are big enough?"
    author: "ca"
  - subject: "Re: K3b vs Nero..."
    date: 2005-03-14
    body: "As long as you don't lie to X11 by forcing it to use a wrong dpi setting, everything works out of the box. A 10pt font will have the right dimensions no matter what, if only the dpi setting is correct."
    author: "Boudewijn Rempt"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-23
    body: "\nThose using old laptops can't increase the screen resolution. My laptop is 3 and a half years old and does 1024x768, but when I bought it many similar laptops were limited to 800x600.\n\nAlso, I find VNC (or Xnest) sessions can be more comfortable with a screen size of 800x600 because it fits within a 1024x768 screen."
    author: "cbcbcb"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-23
    body: "Almost ontopic: Any ideas as of how to convert a Nero .nrg to something you can burn under 'nix? Preferably using k3b, of course.\n\nAlso, by the way: Why does k3b have to be installed in a specific directory (namely, the one kde is installed in) to work? This is mention in the last faq, but it doesn' t say why it is so. I would like to have k3b in /usr/local/k3b, and that doesn't work."
    author: "mr. Anonymous Coward"
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-25
    body: ".nrg is in ISO format. Just rename the file to .iso and it'll work."
    author: "Rayiner Hashem"
  - subject: "Re: K3b vs Nero..."
    date: 2004-08-29
    body: "No it's wrong !!!!"
    author: "czara1"
  - subject: "Re: K3b vs Nero..."
    date: 2004-09-21
    body: "Convert NRG to ISO with nrg2iso from http://gregory.kokanosky.free.fr/v4/linux/nrg2iso.en.html\n\n"
    author: "Sander Jonkers"
  - subject: "Re: K3b vs Nero..."
    date: 2006-10-25
    body: "Thanks... it's also in kubuntu (and friends), so an apt-get install nrg2iso works fine."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: K3b vs Nero..."
    date: 2003-06-23
    body: ">>Very soon K3b will be slapping Nero's bald head in a mocking manner\n\nFirst of _all_, people have issues with a 800x600 resolution issue here and you are talking about \"slapping Nero's bald head\".\n\nyeah - very soon now.."
    author: "AC"
  - subject: "Missing commits / DIGEST ?"
    date: 2003-06-21
    body: "Some people were complaining, again, that major features were missing in this report. Maybe it would be a good idea to have a keyword in the commit message like DIGEST?\n"
    author: "Tim Jansen"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-21
    body: "Drop me a line if there is something I missed. It definitely is possible.\n\nAnother factor this week was my ISP mail server was borked for most of thursday, so there was at least a day's worth of commits that didn't even get looked at. I think there were 250 messages from kde-cvs when the mail started flowing again. They will be included in next week.\n\nAgain, complain vociferously if something is missing. Please.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-22
    body: "Derek,\n\nwhen you add something to the Digest, could you add a small changelog to the top or the bottom of the page, or highlight new entries with a small icon in the table of contents and in the entries itself? Or post details in the comments section? Reason: I already read it and don't want to read everything again just to find out  what's new in there. Yes, I'm that lazy! :)\n\nWell - just a suggestion. :)"
    author: "Eike Hein"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-22
    body: "You lost me there. Do you mean from when I upload the draft version to when I announce the finished one?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-22
    body: "Nope, sorry :). I'm referring to the exchange between you and Tim Jansen. According to Tim, the intitial publication of this weeks Digest lacked coverage about some neat things - you encouraged him to point 'em out to you so you'd be able to add them. For me as reader, that means there's a strong possibility that I missed something because I read an earlier revision. So maybe there's need for an in-document-changelog?"
    author: "Eike Hein"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-22
    body: "I won't add items to an exising digest. If there are things I missed, they will be included in the next week. The only things I change are errors or mis-attributions.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-22
    body: "Ah :). Imaginary problem solved, then. Thanks!"
    author: "Eike Hein"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-22
    body: "Derek, \n\nin lieu of a DIGEST CVS keyword, can we just CCMAIL you in the log if we think something is worthy of the digest?\n\nregrads,\nJason"
    author: "LMCBoy"
  - subject: "Re: Missing commits / DIGEST ?"
    date: 2003-06-22
    body: "Good idea. It should catch my attention :-}\n\nDerek"
    author: "Derek Kite"
  - subject: "Whoo!"
    date: 2003-06-21
    body: "Thanks a lot dude! I thought there wouldn't be any for this week and I was dissapointed and now I just woke up and found one here =) THANK YOU!"
    author: "Alex"
  - subject: "Cool stuff!"
    date: 2003-06-21
    body: "I'm especially glad that K3b will get DVD support, that will rock!\n\nTHough its dissapointing to know taht Konqueror is now a whopping 10% slower at displaying JPEGS (the most widely used image format on the entire web!!!). THIS IS A MAJOR SLOWDOWN, I hope it will become faster.\n\nHonestyly I couldn't even notice the problem in JPEG rendering, it looked the same in Mozilla and Konqueror to me.\n\nCheck out the bug report yourself. \nhttp://bugs.kde.org/show_bug.cgi?id=60000"
    author: "Mario"
  - subject: "Re: Cool stuff!"
    date: 2003-06-21
    body: "It doesn't matter Mario, you probably wouldn't notice the difference in rendering time. Anyway the bottleneck is the network performance.\n\nCheck out the code, see if you can improve it."
    author: "MxCl"
  - subject: "Re: Cool stuff!"
    date: 2003-06-21
    body: "Sorry, i don't know how to code. But, how could I not notice it, 10% sounds like a big decrease in image rendering."
    author: "Mario"
  - subject: "Re: Cool stuff!"
    date: 2003-06-22
    body: "10% is inconsequential if the rendering time is 12ms. I have no idea how fast JPEGs render, but I'm sure someone more knowledgable would confirm that it's fast enough not to worry about.\n\nThanks,\n\nMax"
    author: "MxCl"
  - subject: "Re: Cool stuff!"
    date: 2003-06-22
    body: "using konqueror from cvs (kde 3.1.90),i just tried the link u provided on your bug report\nhttp://www.emutalk.net/showthread.php?s=&threadid=12952 and I can tell u it's displaying the jpeg at normal speed as far as I can tell it was just as fast as mozilla.\n\ncheers,\npat"
    author: "pat"
  - subject: "Suggestion"
    date: 2003-06-21
    body: "Would this be easy to do http://bugs.kde.org/show_bug.cgi?id=60202 it seems so easy and just makes so much sense. I really hope it will make it in 3.2.\n\nBe sure to check out other wishlist/bugs of mine if you agree with me.\nhttp://tinyurl.com/ef3m"
    author: "Alex"
  - subject: "Re: Suggestion"
    date: 2003-06-21
    body: "Alex, I see you just keep doing it: please stop posting long links here."
    author: "Datschge"
  - subject: "Re: Suggestion"
    date: 2003-06-21
    body: "AOL, http://tinyurl.com"
    author: "Anonymous"
  - subject: "Re: Suggestion"
    date: 2003-06-21
    body: "I am amazed everyday at what some people can come up with! That's a great idea.\n\nCheck out the new url! http://tinyurl.com/ef3m =)"
    author: "Alex"
  - subject: "Re: Suggestion"
    date: 2003-06-22
    body: "Why? I'm just curious; what's the problem with \"long links\"?\n"
    author: "ac"
  - subject: "Re: Suggestion"
    date: 2003-06-22
    body: "they make the width of the page really wide, so you have to scroll back and forth to read every post...it's pretty annoying."
    author: "LMCBoy"
  - subject: "BC for KDE libraries"
    date: 2003-06-21
    body: "BC (Binary Compatability) for libraries has already been achieved with another operating system that was very popular a few years back, namely the Amiga Operating System. The Amiga's OS used a technique of using a jump table at the beginning of a library of which stayed constant with new routines being added to the end of the table.\n\nThis would be a great idea for the KDE libs, it would require breaking BC one more time, but after that no more."
    author: "Ian Ventura-Whiting"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-22
    body: "Do you have any links for information about this? What are the penalties for using such a system?"
    author: "MxCl"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-22
    body: "The only problem was when some of the demo crews and some games decided to hit the amiga's hardware directly or use some \"internal use\" library code instead of using the published routines.\n\nAll programs that were written correctly (useing the published library routines) worked on all versions of the operating system. The people who developed the libraries could add extra (optional) parameters to library functions and were able to extend a libraries functionality by adding new routines that were linked at the end of the library jump table."
    author: "Ian Ventura-Whiting"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-22
    body: "It was great, but it was before C++ came along.\n\nFor languages like C, it's perfect. Windows used to do it the same way, btw....\n\nBut KDE is exporting C++ interfaces, which have each their own virtual tables...\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-22
    body: "Such a thing is already done in KDE with the wonderful powers of ELF.\n\nWhere Amiga adds a \"jump table at the beginning of the library\", ELF makes a relocation table.  Hence, KDE can add new functions without trouble.  The problem is in fact that the size of structures when we add virtual methods.\n\nLet me describe:\n\nC++:\nclass Foo\n{\n\tvirtual void setValue(int c);\n};\n\nSame thing in C:\ntypedef struct Foo\n{\n\tvoid (*setValue)(Foo *, int);\n}\n\nso, now, sizeof(Foo) is equal to sizeof(void*), in the case of C; C++ may not necessarily add the virtual-table inline with the class object itself (so you might not get too much excitement by testing this out).\n\nThis means, that when you do the following: \nC++:\nFoo * foo = new Foo;\n\nand in C:\nFoo *foo = (Foo*)malloc(sizeof(Foo));\n\nThe compiler can identify the sizeof the class Foo at compile time (sizeof(void*)), so it actually writes\nFoo * foo = (Foo*)malloc(4); (in a 32 bit process)\n\nWhen someone adds another virtual to class Foo in libfoo.so, the sizeof(Foo) increases by a notch.  That's no big deal, the problem when an application that uses class Foo, it calls malloc(4), even though it should be calling malloc(8).\n"
    author: "Charles Samuels"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-22
    body: "That is the \"fragile base class\" problem which comes up whenever you use gnu C++ to create shared libraries. BeOS solved this problem by just adding some spare virtual functions to classes that they wanted to extend in the future. It is a hack, but it works to preserve binary compatibility.\n\nAnd by the way, adding virtual functions does not increase the size of the object, it merely increases the size of the virtual method table. You can have an object with thousands of virtual methods that uses just a few bytes per instance."
    author: "Androgynous Howard"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-22
    body: "Very interesting idea. KDE could use that as well.\nCurrently KDE is using the virtual_hook() methods in all classes that can be used to make non-virtual methods behave like a virtual...\n\n"
    author: "Tim Jansen"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-22
    body: "As  Androgynous Howard already described, it doesn't work like this, since it really look more like this (in a single inheritance case, let's not talk about MI :-) ):\n\nstruct Foo\n{\n\tFoo_vtable* vptr;\n        int data1;\n\tint data2;\n\t//..      \n}\n\nstruct Foo_vtable\n{\n\tint base_offset;\n\tsomeFunctionPtr1 typeID;\n\tsomeFunctionPtr2 virtualFunc1; \n\tsomeFunctionPtr2 virtualFunc2;\n\t...\n}\n\nAdding more virtuals becomes a problem when there is a class that inherited off\nthe base and added a new one. The traditional way of doing that would be for\nthe child to create a virtual table that has additional slots at the end. If the \nparent puts something else in these locations, funny stuff happens, of course..\n\nThe size issue of course exists if you add more data members. \nBut it can also exist even when the allocation size of the object doesn't change.\n\nConsider this:\n\nstruct Base \n{\n\tchar v1;\n\tchar v2;\n\tchar v3;\n};\n\nHere, sizeof(Base) == 4 on most platforms due to alignment/padding. So can you safely add a char v4, since it'd still allocate 4 bytes? Nope.\n\nIf you had a  \nstruct Child: public Base\n{\n\tchar c1;\n};\n\nMany C++ implementations (I believe including g++-3.x) would pack the \nnew value into the padding of the parent class, so making the base class use it\nis not a very good idea...\n\nAnyway, the point of all this: \nMost (all?) C++ ABIs are designed for performance, at the expense of making binary compatibility difficult. Therefore there are generally no easy answers -- \njust a lot of discipline plus some tricks... \n"
    author: "Sad Eagle"
  - subject: "Re: BC for KDE libraries"
    date: 2003-06-24
    body: "Since KDE is C++, and C++ doesn't really allow this (because of virtual function tables, etc), it would be difficult to do this.  But, another language, objective-C, does this quite nicely, using messages for doing method calls.  The way a message works is through a proxy function, which just browses the object's message table, and calls the right method.  It's pretty cool, just check it out at apple's site.  IMHO it's much better than C++."
    author: "somebody"
  - subject: "KDE.ORG Design Adoption"
    date: 2003-06-21
    body: "I remember reading a few months ago that the design adopted by KDE.org will replace the design which many KDE websites like: \n\nhttp://www.kde.org/areas/people/\nhttp://i18n.kde.org/\nhttp://artist.kde.org/\nhttp://dot.kde.org/\nhttp://events.kde.org/\nhttp://kdemyths.urbanlizard.com/\nhttp://www.koffice.org/\nhttp://www.kdevelop.org/\nhttp://printing.kde.org/\nhttp://women.kde.org/\nhttp://developer.kde.org/\nhttp://www.kdeleague.org/\nhttp://enterprise.kde.org/\nhttp://promo.kde.org/\n\ndo not us and have not even adopted the color-scheme. This is not even the complete list of website snot using the new design, it is a shame to see such inconsistency so many months after the new design became ffective and I am wondering if there si stilla  plan to bring a level of consistency between the different designs KDE.org sub-sites have adopted.\n\nI really hope this issue is taken seriously because it is really hurting KDE's image as a consistent desktop. People will think that if the sites are this inconsistent so must be the desktop.\n\nThanks,\n   I hope this issue will get resolved quickly =)\n\nBTW: I will help, but I am leaving for 2 months starting tommarow so I can't help yet."
    author: "Alex"
  - subject: "Re: KDE.ORG Design Adoption"
    date: 2003-06-22
    body: "http://women.kde.org has its own design and will not be ported to the new one.\nFor the other sites you list, very often webmasters are also developers and they cannot find time to port the website. I guess the dot has always had a style different from www.kde.org\nI suggest that when you are available again (in 2 months), you cvs co the www module, you subscribe to the kde-www mailing list and you port those websites locally. Then you can make a tarball and send it to someone from the mailing list.\n:-)\nIn my opinion, events and promo must be the first to be ported."
    author: "annma"
  - subject: "ui/qt/kde question"
    date: 2003-06-22
    body: "I don't know anything about the structure of the kde libraries or qt, and so I really don't understand bugs like this one:\n\n>Christian Loose committed a change to kdesdk/cervisia\n\n>Implement BR 59644: Change key shortcuts for cvs add and\n>cvs remove to Insert and Delete. Now you can use the plus and\n>minus keys to fold/unfold the tree view like in most other KDE\n>applications.\n\nShouldn't such controls like a treeview and their behaviour be part of kdelibs/kdebase, shared by all kde applications and only be overridden when necessary?\n\nAllows this signal/slot mechanism no inheritance or so? To me (having no clue about qt) it looks like a sample of bad design. Please enlighten me! :-)"
    author: "Carlo"
  - subject: "Re: ui/qt/kde question"
    date: 2003-06-22
    body: "The +/- keys are indeed shared between all apps using the treeview control. The problem was that in cervisia, + and - were overridden to perform other functions, and therefore could not be used to expand and collapse the tree. These other functions were now remapped to the Ins/Del keys, so that +/- are again available for expanding/collapsing the tree."
    author: "Roie"
  - subject: "Re: ui/qt/kde question"
    date: 2003-06-22
    body: "Thanks for the explanation. I had a second look at the diff and have to concede that this was a pretty dumb question. It was too early in the morning, I suppose.\n"
    author: "Carlo"
  - subject: "Binary Clock Screenshot"
    date: 2003-06-22
    body: "http://www.csh.rit.edu/~benjamin/binaryclock.png"
    author: "Benjamin Meyer"
---
This week in <a href="http://members.shaw.ca/dkite/jun202003.html">KDE-CVS-Digest</a>: CD burning application <a href="http://k3b.sourceforge.net/">K3b</a> begins to gain DVD writing functionality, continued fixes and improvements
to KWin, more on the binary compatibility debate, bug fixes, and more.  Enjoy.
<!--break-->
