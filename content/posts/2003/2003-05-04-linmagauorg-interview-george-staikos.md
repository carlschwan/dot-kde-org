---
title: "linmagau.org: Interview with George Staikos"
date:    2003-05-04
authors:
  - "binner"
slug:    linmagauorg-interview-george-staikos
comments:
  - subject: "Mac OS X"
    date: 2003-05-03
    body: "I don't know why he so focussed on OS X. I prefer KDe. Installtion of software is still a problem as you need packages for every single distribution and architecture. So this is an issue that will be solved in the next two years. As software becomes more stable and usable I don't get why Mac OS X was superior as there is very little professional MAc OS X software."
    author: "Hakenuk"
  - subject: "Re: Mac OS X"
    date: 2003-05-04
    body: "I don't know if you have used OS X or not, but it is quite impressive.  KDE can learn and has learned much from OS X.  I think it's perhaps a better place to learn from than Windows too.  Everyone builds on eachothers' work in the computer desktop world though.  Completely new ideas are not the most common thing."
    author: "George Staikos"
  - subject: "K3B"
    date: 2003-05-04
    body: "\nImpressive is K3B. I was astonished at my first time with K3B.\nKMail is wonderful. KDE have a pretty face and great features.\nWhen KOffice and Konqueror take up all this \"fell and look\" .... poor Mac OS X.\nBut it is a long time yet !!!\n\nDevelopment !! Development !! Development !! \n"
    author: "v.b."
  - subject: "Re: K3B"
    date: 2003-05-04
    body: " I do think Konqueror is great the way it is now.\n Although, I do agree with the KOffice part.\n\n I think MacOSX is better than KDE/Linux in the following aspects:\n- software installation;\n- ease of use of removable devices.\n\nps: sorry my English!"
    author: "Ricardo"
  - subject: "Commit Superkaramba to CVS and KDE == MacOSX"
    date: 2003-05-04
    body: "... well almost.  Few users could tell the difference.  Especially with Xsvg coming and support for alpha transparency 3D etc.\n"
    author: "KDE Fanatic"
  - subject: "Re: Mac OS X"
    date: 2003-05-04
    body: ">>>Installtion of software is still a problem as you need packages for every single distribution and architecture. So this is an issue that will be solved in the next two years.\n\nReally? How?"
    author: "IR"
  - subject: "Re: Mac OS X"
    date: 2003-05-04
    body: "While I am not sure what the author is talking about, autopackage.org looks like it could be a good solution for installation itself. To achieve compatibility for KDE apps, KDE would have to set up rules for creating binary compatible libraries that distributions must follow, if they want to support KDE binaries."
    author: "Tim Jansen"
  - subject: "Re: Mac OS X"
    date: 2003-05-04
    body: "I would have to say that this problem will only get worse.\n\nThe LSB (&FHS) will not fix the problem because it is controlled by those that are making the problem worse.\n\nIf anyone has any idea how to fix the problem, you can (PLEASE) join the discussion on:\n\n\tdlc-discuss@desktoplinuxconsortium.org\n\nNote the complete lack of postings there except for those telling me why I am wrong.  I chose not to answer some of them because I felt that the matter was hopeless.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Mac OS X "
    date: 2003-05-04
    body: "> as there is very little professional MAc OS X software.\n\nOh Really? Compared to what, Windows?\n\nBecause OS X has loads of professional software for: \n\na) Niche markets such as video, music, graphics and dtp \n(even Quark is finally out).\nb) Corporate desktop use (it also runs Office, remember).\nc) Server use (Apache, Samba, J2EE etc).\n\nAnd on top of that it can run almost all legacy UNIX apps,\n(there are even two large porting projects with thousands of\nports) and running GTK or QT apps is just a matter of having \nthe appropriate libraries and doing some porting.\n\nCompared to a UNIX/KDE system, Mac OS X has a LOT more\nsoftware.\n"
    author: "Space Cowboy"
  - subject: "Re: Mac OS X"
    date: 2003-05-04
    body: "... BTW, OS X also runs KDE 3.1.1 quite well, thanks to the talented people of the Fink Project (the most \"mature\" porting project, OpenDarwin being a close second): see also\n\nhttp://fink.sourceforge.net/news/kde.php\n\nAnyway,  KDE is (IMHO) still rather more advanced from the features point of view - for example, in apps such as Konqueror, etc., or even in \"simple\" things like contextual menus and multiple desktops, - but especially in its excellent \"KParts\" modular components architecture (the closest OS X equivalent one can think of is probably the \"Services\" concept).\n\nKDE and OS X have a lot to (positively and constructively) learn from each other, indeed! :-)"
    author: "Sven"
  - subject: "Re: Mac OS X"
    date: 2004-02-12
    body: "No prof software out there?  What a*e u on?  Space Cowboy, Rivardoand Sven have it right.  As for KDE, I tried it on several ppc distros.  Talk about failing to do the simplist things that are taken for granted on the mac platforms for yrs now (for half a decade at least now?). ie Drag and drop, as well as copy and paste.  Fonts on YD were horendous (mdk was noticeably better but still not up to par) as well as not printing to my Epson printer, nor syncing with my Palm!  Mandrake-ppc had system stalls to which one had to install benh kernel by hand 'cause there was not alternative install.  Why then provide the benh kernel in the first place?!  Coupled with pc users that are experimenting with ppcs that can't even tell me how to run fsck (or get oto single user mode 'cause their way of doing it doesn't work [this goes both for yd and mdk-ppc]) on the ppc platform.  Their responce when they found that their instructions don't work?  Google it documented.  PPC platform largely isn't.  And don't tell me to use the man commands as these are only meant as cheat sheets, not full blown instructions!  I left that community, I'm happy to say (save my drive from all the crashes and the resulting damage).\n\n  As for installing software, please Linux has a long way to go.  Mandrake-ppc wouldn't iniciate a newly installed client unless I first unintalled the previous v. (something not too unheard of) but the new version would not even show up under drakupdate after intalling the new version (the previous version would).  This from a distro that is supposed to be a desktop user friendly gui distro?  I know the way Apple would do it, followig their logical way of doing things, boths versions would show up as being capable of being updated, regardless if there is an update or not.  Yes I know, unix clones are not gui systems (and thus this is not their forte) but we're comparing a gui interface (KDE) to OSX's Aqua aren't we.  And this is the point, OSX is built from the ground up with the user experience in mind (gui priority [have you read about how Apple meetings start?  They start the meetings by speaking about user experience;read <http://seattlepi.nwsource.com/business/158677_msftmac30.html>]) and unix clones are not, nor where they ever about user experience.  Gui interfaces on unix \"clones\" ('cause that's what all these systems are clones of unix, they are not unix, it doesn't exist anymore) come closer to being an after thought.  I've read as much from Linux reviews written by Linux users recently.  There are complaints about the gui utlities being barely desirable due to the inability to have the flexibility that command line offers and that reporting of errors are insufficient to be of use, etc.  Get out there and read. And use the other system you think you don't like before you comment (on a friend's comp if oyu don't want to buy?).  I don't believe for a sec you Apple bashers have used OSX (Panther is the best of the lot).  A lot of *nix user complaints about OSX come from sheer ignorance such as people intalling OS X with a unix filing system (huge mistake on Jaguar!!). \n\nOn the whole, yes Safari leaves somethings to be desired and i think I prefer Konqueror (although one is based on the other) but Apple has done a good job intergrating Address book and java scripts that can be written to compensate for many desible features."
    author: "chascon"
  - subject: "Re: Mac OS X"
    date: 2004-02-12
    body: "By the way, dispite that all  the above experiences may not be attributable exclusively to KDE, by en large I selectively used KDE on the vaious linux ppc distros mentioed above.  This may have contributed the problems or not, your choice to make for yourself."
    author: "Chascon"
  - subject: "Why does it break"
    date: 2003-05-03
    body: "Good interview. I feel old (at 45).\n\nInteresting comment on needing gurus to fix when broken. Does OSX break? Debian stable never does, unstable rarely does. Gentoo suffers from 'emergus interruptus' from time to time, but I'm on the bleeding edge there. I've never experienced linux and kde breaking from use.\n\nI guess new hardware, new software installation is the difficulty. Again debian has the new software down pat. \n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Why does it break"
    date: 2003-05-04
    body: "I don't think I've ever seen a system that doesn't break.  Don't forget that people make mistakes, software has bugs, etc."
    author: "George Staikos"
  - subject: "Re: Why does it break"
    date: 2003-05-04
    body: "I think if I would quite messing w/ mine that it stay more stable, but I get bored and have to try something new. That leads to fixing what I broke. Now if I get the support for the software I need to run on my laptop, I will leave that one alone and get some work done."
    author: "Echo6"
  - subject: "Re: Why does it break"
    date: 2003-05-04
    body: "Various versions of KDE have been able to misbehave or break if you get crap in your .kde directory. I think this can happen if you run several KDE sessions at the same time, or you switch between different versions. (It is important to be able to do this - my home directory at work is accessible on all machines, and they have various different versions of RedHat and KDE and I need to be able to use any of them)\n\nDebian absolutely does NOT have installation of new software sorted. If you use stable, then there is no new software made available by debian, and unofficial packages can obstruct/break upgrading to unstable or the next stable. Ordinary users cannot install .debs. Installing 3rd party software affects the whole system - If I install, say, a Sun JVM I want to be able to say \"install this deb and satisfy dependancies from my configured apt repositories\". AFAIK I can't do this currently. Further, I don't (necessarily) want this package to interfere with the system. Perhaps I don't want it to be in root's path, or I don't want it to conflict with Kaffe (invoked as java on my system). Really, I want software not packaged with the system to live in somewhere like /opt/package/ with a per-user configurable system to alter $PATH and $LD_LIBRARY_PATH at login.\n\nThis isn't really debian specific, I use redhat at work and I'd like to be able to install s/w on it without having to ask sysadmin to do it for me."
    author: "cbcbcb"
  - subject: "Re: Why does it break"
    date: 2003-05-05
    body: "I've seen Linux break, but that was only because WinXP tried to hose one of my ext3 partitions that was accidentally labled ntfs in the partition table. Fixing the problem was simply a matter of booting from a rescue disk and typing fsck. On the other hand, WinXP goes into fits and refuses to boot even if you just move an installation from one machine to another. And WinXP is nearly impossible to repair. The recovery console has a very limited set of tools (unlike my Gentoo bootdisk which is fully networked and has web access :) and there's only a small chance that it will actually fix things without requiring a reinstall. I don't know how the situation is with OS X, since I've never used it, but one of the things that makes Windows easier (the opacity of the internals to the end user) also makes it really hard to fix."
    author: "Rayiner Hashem"
  - subject: "George Staikos"
    date: 2003-05-04
    body: "In my opinion George is one of the most capable and productive KDE developers. If your entity thinks about supporting KDE, please consider sponsoring George to work more than in his spare-time or even pay him to work full-time on KDE."
    author: "Anonymous"
  - subject: "Usability"
    date: 2003-05-04
    body: "\"I think KDE is far more usable than Windows 98 SE. I think Linux is far less so, mostly due to applications.\"\n\nSo how usable is KDE running on Linux??"
    author: "Ac"
  - subject: "Re: Usability"
    date: 2003-05-04
    body: "My point is that the GUI itself is much more usable in KDE, but the entire package consists of more than just a GUI.  It consists of device drivers, installation, hardware support, packaging, and more.  I think Linux still has a way to go with that."
    author: "George Staikos"
  - subject: "KwinTV"
    date: 2003-05-04
    body: "Is Kwintv (new version) planned to be integrated into kde ?\nOr does the final version will be released separetely ?\nFor now, it seems this new rewrite is only available in CVS."
    author: "LC"
  - subject: "Re: KwinTV"
    date: 2003-05-04
    body: "It will be released as another alpha, beta, etc package as it reaches those milestones, and then the plan is to put it in kdemultimedia.\n\nThe problem is that development has slowed due to requirements elsewhere.  We also need to rewrite the V4L plugin again because the current one just doesn't work well.  There is a short but difficult list of bugs that need to be fixed before we can consider a new release.\n\n"
    author: "George Staikos"
  - subject: "So...."
    date: 2003-05-04
    body: "Get that 3.2 release finished already! :p "
    author: "Joergen Ramskov"
  - subject: "re: Mac OSX"
    date: 2003-05-05
    body: "The thing to note is that despite George's enthusiastic\nthumbs up for OSX he is still working on KDE.\nThis is something quite different from those who say,: \"Linux?\nfuggedaboutit already and just get a Mac.\"\n\nFinally considering Apple has complete control of their much smaller\nuniverse of hardware and a single gui, it had damn well better\nbe pretty polished.\nAnd never forget part of this brave new world of Mac OSX is now\nbuilt upon KDE technology:khmtl.\n\nPersonally it is about a lot more than the tech side.\nOpen Source is for some about a lot more than whether it just\nworks."
    author: "kannister"
  - subject: "KDE INTEGRATION NEEDS TO BE ACHIEVED"
    date: 2003-05-06
    body: "Trolltech is a great group of motivated bright individuals and I'm sure they can come up witha  way to integrate KDE and Qt, this is wha I really don't like about Qt! It is as integrated with KDE as a GTK application!!! Infact, Qt is far better integrated with OS X, and Windows than with Linux.\n\nMy second gripe is that Trolltech does not assist in the development of Python Qt and offers no official support for it. Python Qt is by far the best solution for most applications, but few people know. Its as if Qt was build for it because Python is more object oriented as Qt is, it is easy, multiplatform, and yet very powerful. PyQt + Qt Designer is the ultimate RAD tool."
    author: "Alex"
---
<a href="http://www.linmagau.org/index.php">linmagau.org</a> features an <a href="http://www.linmagau.org/modules.php?op=modload&name=Sections&file=index&req=viewarticle&artid=89">interview with George Staikos</a> (<a href="http://www.linmagau.org/modules.php?op=modload&name=Sections&file=index&req=printpage&artid=89">print version</a>) about Qt and the future of KDE and Linux/Unix desktops in general. George contributes to <a href="http://kmail.kde.org/">KMail</a>, <a href="http://konqueror.org/">Konqueror</a> (SSL, Netscape plugins, <a href="http://dot.kde.org/1049415292/">web sidebar</a>), Xinerama support and <a href="http://kwintv.org/">KWinTV</a>. Other interview topics include usability, hardware driver support, the <a href="http://developer.kde.org/development-versions/kde-3.2-features.html">feature set of KDE 3.2</a> including better laptop support and a new wallet system planned by George, and KDE's hardware requirements. In short, <a href="http://www.linmagau.org/modules.php?op=modload&name=Sections&file=index&req=viewarticle&artid=89&page=1">worth a read</a>.
<!--break-->
