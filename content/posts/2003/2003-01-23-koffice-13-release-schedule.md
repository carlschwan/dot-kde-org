---
title: "KOffice 1.3 Release Schedule"
date:    2003-01-23
authors:
  - "binner"
slug:    koffice-13-release-schedule
comments:
  - subject: "Go KFormula"
    date: 2003-01-23
    body: "I'm happy to see KFormula is still alive/resurrected. Free software is doing pretty well on the scientific desktop. Formula support is very important for further growth in that area.\n\nGood luck on the MathML spec!"
    author: "Jos"
  - subject: "Re: Go KFormula"
    date: 2003-01-24
    body: "I hope it has support for chemical reaction schemes (nice arrows for chemical equilibria et al).\n\nAnother thing missing for a scientific office desktop is a bibliography management system. Something like pybib or endnote that can get references directly from web sources, such as http://www.pubmed.org. \n\nI heard, some time ago, there was a project on an application of this type but it's been so long since i heard anything from it..."
    author: "zelegans"
  - subject: "Kexi!"
    date: 2003-01-23
    body: "This project looks awesome. I've been looking for a GOOD front-end to popular databases. Does anyone know if if will be able to connect to remote databases or if it will be limited to those on the local machine?"
    author: "Vic"
  - subject: "Re: Kexi!"
    date: 2003-01-23
    body: "Kexi uses an internal database or can be conntected to an external database.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Kexi!"
    date: 2003-01-23
    body: "it looks in it's extreme infancy to me.\n\ncheckout <a href=\"http://www.thekompany.com/products/rekall/\">ReKall</a> for something looking a bit more mature.\n\nI'm just umming and erring over whether to buy it myself. And If it comes with windows support (i think so) i think my boss will pay! (I'm looking at a front end for remote db admin too)."
    author: "caoilte"
  - subject: "Re: Kexi!"
    date: 2003-01-23
    body: "hi,\n\ndid you only check out the screenshots on kexi's homepage? they are a bit old, i'll need to update them. well i have to admit that it is still in a early stage, but hopefully we get it working till koffice's release.\nbtw, i'd never start with kexi if rekalm wouldn't be dead, but it is :(\n\nhowever i would be very happy if you could post me whishes about it's features and it's priority. if i will get some feedbacks like that i know where to set priorities\n\nthank you\nlucijan\n"
    author: "lucijan"
  - subject: "Re: Kexi!"
    date: 2003-01-24
    body: "how is rekall dead? that's a great shame. the idea in principle is great.\n\nand sorry to pour scorn on your app. i just had a look at the feature list, and it doesn't look like much more than a direct database editor at the moment. i couldn't let someone who doesn't understand databases use it.\n\nI don't use MS Office, but a friend showed me some of the (albeit UGLY) cross-office VB scripting he can do to generate pretty forms, spreadsheets, graphs, word docs and presentations from a database. if KOffice, and specifically a db forms frontend for it offered that sort of scripting with python, ruby, java etc - it would be getting somewhere new and useful."
    author: "caoilte"
  - subject: "Re: Kexi!"
    date: 2003-01-24
    body: "Rekall is alive and well and actively developed, a new release is in beta3 right now, we have plug ins for IBM's DB2 as well as for ODBC coming out in the next week and Oracle is coming along nicely.  Rekall is running as a KDE native app, or a Qt app on both Linux and Windows.  A Mac OS X port will be coming either with the 1.1.1 release coming next week or the 2.0 release in a month.\n\nThe Rekall dataaccess engine is also being leveraged into some of our other products.  And if you missed it, Rekall is also available on the Sharp Zaurus as tkcRekall.\n\nFeel free to check it all out at:\n\nwww.thekompany.com/products/rekall\nwww.thekompany.com/embedded/tkcrekall"
    author: "Shawn Gordon"
  - subject: "Re: Kexi!"
    date: 2003-01-25
    body: "hi,\n\noh, yes it exists :) last time i took a look it seemd like beeing dead, anyway i think the userinterface is a bit hard to work with and not intuitive. well i like some ideas which i am considering of including into kexi (no metter if it already happens in it's first version :)\nbut so far as i see the licence isn't really the one which should be taken for including into koffice ;)\nwasn't it gpl before? anyway, now i see i should update the discribtion and make some new screenshots. btw. kexi will be/is definitly more than just another mysql frontend (i hope so), we got gui queries working and forms are already going into the right direction psn will fix table creation and for scripting we think about taking QSA which shoudn't be very hard to implement. however i hope that doesn't sound like a message against rekall.\n\nlucijan busch\n (kexi maintainer & developer)\n"
    author: "lucijan"
  - subject: "Re: Kexi!"
    date: 2003-01-27
    body: "ooooh QSA. i stand corrected, that would be worth it.\n\nGood Luck! I look forward to trying koffice1.3"
    author: "caoilte"
  - subject: "Re: Kexi!"
    date: 2005-01-07
    body: "Rekall - dead - never. I have just uploaded Rekall V2.3.2 beta1 for the Mac to our web site. As far as features are concern we add quite a lot of new stuff. For example, we have added a new set of Wizards which generate \"starter\" applications, a web interface, which allows you to publish your application to a web server, drivers for SQLite, Sybase/MS SQL Server, and Informix and much more. For more information, please visit http://www.totalrekall.co.uk or http://www.rekallrevealed.org"
    author: "deanjx"
  - subject: "Re: Kexi!"
    date: 2003-01-23
    body: "How about knoda?  It looks like it has potential."
    author: "Trevor "
  - subject: "Hooray!"
    date: 2003-01-23
    body: "KOffice is such a fantastic effort, I think this small but able team deserves three cheers for partaking on such a ambitious but very needed project. I have in front of me now a stack of StarOffice 6 books, that I'm planning on using to train my staff of 30+ on soon, so I know SO pretty well. As good as it may be (and it has come a long way), nothing beats the look, feel, and integration of a native KDE office suite. Just as KHTML defeated Gecko on the MacOS X desktop, KOffice will grow to be a strong contender in the UNIX desktop world. A universal document format (based on the StarOffice XML formats) will help to get us there.\n\nKeep up the great work!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Kivio?"
    date: 2003-01-23
    body: "What is happening to kivio and a project planner? Any news on either?"
    author: "a.c."
  - subject: "Re: Kivio?"
    date: 2003-01-23
    body: "Kivio is rewritten for the KOffice architecture and there is (again) talk about a Dia stencil importer.\nAbout project planner, there is no competition to MrProject. KPlato is still stuck in the design phase."
    author: "Anonymous"
  - subject: "Re: Kivio?"
    date: 2003-01-23
    body: "Ummm maby you should read the source, it already has Dia stencil support.  \n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Kivio?"
    date: 2003-01-25
    body: "rewritten for KOffice?  We wrote it for KOffice originally, years ago."
    author: "Shawn Gordon"
  - subject: "Re: Kivio?"
    date: 2003-01-25
    body: "http://lists.kde.org/?l=kde-cvs&m=104265577208254&w=2"
    author: "Anonymous"
  - subject: "Fine, now what about KDE?"
    date: 2003-01-23
    body: "According to the release plan -- http://developer.kde.org/development-versions/kde-3.1-release-plan.html -- we will get the decision about KDE 3.1 final on Monday January 13th, 2003. I don't such a decision anywhere (and yes, after comments the last time I posted this, I *have* searched the mailing lists). The last thing I can find is the release coordinator saying that he liked the look of RC7 (which also isn't mentioned in the release plan)."
    author: "Bob"
  - subject: "Re: Fine, now what about KDE?"
    date: 2003-01-23
    body: "I gave up on a \"release\" and just got the source from CVS KDE_3_1_BRANCH... 3.1 is awesome, by the way :)"
    author: "Vic"
  - subject: "Re: Fine, now what about KDE?"
    date: 2003-01-23
    body: "Hi!\n\nWell, then they did/do need a few more days time to get everything in place and to build the packages.I don't see any problems with that. If you do not want to wait any longer i would go and grab the sources via cvs.\n\nAndy"
    author: "Andy"
  - subject: "Re: Fine, now what about KDE?"
    date: 2003-01-23
    body: "I'm perfectly happy to compile the code myself. What I'm not happy with is the lack of communication with ... well, anybody, on the part of the release coordinator."
    author: "Bob"
  - subject: "What about Krita ?"
    date: 2003-01-23
    body: "It appears someone is actually working on it.\nWill it be part of the release ? It would be a real lack for kde if not..."
    author: "LC"
  - subject: "Re: What about Krita ?"
    date: 2003-01-23
    body: "Its maintainer and almost single developer, Patrick Julien, has not reacted to the release schedule yet. So we don't know how mature he expect it to be at KOffice 1.3 release time."
    author: "Anonymous"
  - subject: "Functionality"
    date: 2003-01-23
    body: "Hello koffice team,\n\nit's great to see that development on suite is going on and still not dead,\nbecause the best desktop still needs it's own native best office suite.\n\nThere are 2 things that still worry me for making sure that one day this suite\nwill be the best one:\n\n1. The raster drawing app. What happened with krita (formerly krayon), seems\nthat this one will never be released?\n\n2. OpenOffice integration. As we know openoffice.org is mature suite and very\nwidespread one, however until kde has at least like it or even better one we\ncould use it's success and knowhow in order to promote the kde native office\nsuite. I am talking about integration as making koffice kparts for the various\nopenoffice components (not just embeding them but even making them to use kde's\nmenu and toolbar system, and may be even more to use the native kde/qt\nwidgets). This will at least help in the beginning for former\nMS/Star/OpenOffice users to convert, and those components could be finally\nreplaced when koffice has mature and stable ones (I think that kword is good\nexamlpe).\nSuch of integration can probably consinst in folowing stages:\n2.1 plugger kpart integration (or xkpart whatever) - the easyest way to embed\nwhatever xapp\n2.2 make integrated openoffice kparts to use kde's menu and toolbar system\n2.3 start replacing basic widgets (scrolls, panels, buttons, ..) with kde/qt\nwidgets in order to reflect to users' configuration settings of look and feel\nand at least to have kde look and feel\n2.4 make this components to support koffice native file formats\n2.5 replace core classes with kde foundation classes (may be before this point\nis reached kde will have it's own mature and stable office components, because\nyou guess it could be very ugly such of refactoring)\n\nI would even CONTRIBUTE if koffice opens it's openoffice.org integration\nsubproject, what is important in the begining is to check the LGPL license that\nsun composed because as I read in their faq there are some limitations.\n\nIs there anybody who is aware with their license, is this really possible and\nis there planned same/similar koffice subproject.\n\nGreetings, Anton Velev"
    author: "Anton Velev"
  - subject: "Re: Functionality"
    date: 2003-01-23
    body: "I agree.  I would fully support OOo being ported to Qt, but porting that project to any new toolkit is simply not feasible.  It's a project of over 6 million lines of code.  It's not impossible, but it's more work than it's worth, apparently.\n"
    author: "Sam"
  - subject: "Re: Functionality"
    date: 2003-01-23
    body: "> I would even CONTRIBUTE if koffice opens it's openoffice.org integration\n> subproject, what is important in the begining is to check the LGPL license that\n> Sun composed because as I read in their faq there are some limitations.\n\nWell, you can help here: import and export filters from/to OpenOffice.org are already under work (in KOffice CVS) and will become part of KOffice 1.3. As for the licence issue: you are not right, please read again http://www.openoffice.org/license.html, we are compatible.\n "
    author: "Lukas Tinkl"
  - subject: "Re: Functionality"
    date: 2003-01-24
    body: "> Well, you can help here: import and export filters from/to OpenOffice.org are\n> already under work (in KOffice CVS) and will become part of KOffice 1.3. As\n> for the licence issue: you are not right, please read again\n> http://www.openoffice.org/license.html, we are compatible.\n\nwell, that's not exactly what I wanted to say, I think that koffice kpart of openoffice components would be the fastest way to bring industry standart working functionality for koffice. However the step of compatibility is important and part of both strategies (1st strategy - port openoffice comps to kde, 2nd stratrgy making koffice comps to support openoffice formats). It seems that in both cases i really need to research how compatible are the native document object models of both projects.\n\nI can start with kword - openoffice writer, now can someone tell me how to get the fresh cvs of openoffice and koffice working with my kdevelop ide?\n\nAnd also another question, formerly i read in the dot something about kxpart or xkpart which was intended to turn every xapp to kpart, is there anyone that can tell me how it works?\n\nLukas, i will contact you by email meanime."
    author: "Anton Velev"
  - subject: "Krita / Kexi"
    date: 2003-01-23
    body: "Whooohooo, Kexi, finally a lightweight database\nlike Access. This was badly needed. Perhaps\nit could import/export mdb's one day? There is some OS-project\nabout that going on (cant rememeber the address\nright now, though).\nKWord definitely needs better RTF import/export.\nI could live without DOC-export but without good\nRTF export there's no way to get my documents\n\"out\" to our clients. RTF is complicated but text-based\nand documented so there's hope.\nI hope Krita's eventually getting somewhere.\nThe Gimp is one of the worst UI-sins which\nhas remained on my desktop. I hardly use\nit though, but Photoshop 5.5 on Wine which\nruns OK but has got some bugs or PhotoPaint 9\nwhich has a horribly slow file open/save dialog.\nSomething with Gimp functions but without \n1000s of windows spread all over my desktop.\nEverybody asks me why I don't use the GIMP.\nIsn't there anybody else out there who finds\nthe UI of this app horrible?\nWell, big thanks anyway for all those KDE/Koffice developers\nhave achieved so far - a user-friendly OpenSource desktop.\n"
    author: "Jan"
  - subject: "Re: Krita / Kexi"
    date: 2003-01-23
    body: "> Kexi, finally a lightweight database like Access.\n\nKexi is NOT a database. Nor that I would say that Access is a real database either. :-)"
    author: "Anonymous"
  - subject: "Re: Krita / Kexi"
    date: 2003-01-23
    body: "It do have it's own embedded database backend (cql) though.\n\n// Peter Simonsson, Kexi Developer"
    author: "Peter Simonsson"
  - subject: "Re: Krita / Kexi"
    date: 2003-01-24
    body: "Have you thought about using libgda (look for it under the GNOME Office materials) for the backend library connecting to the various databases?  libgda is interface agnostic; so it should be easy to put up a QT/KDE frontend.\n\nAdvantages to using libgda:\n*It's actively maintained by the GNOME people and the Ximian Mono people.\n\n*There are beta plug-ins for MySQL, Postgresql, Firebird, Oracle, .mdb (MSAccess) and probably others.\n\n*It's front-end agnostic.  libgda does not use a GTK/GNOME front-end (that's what libgnomedb is for).\n\n*More cooperation between KDE and GNOME.\n\nDisadvantages:\n*libgda is written in C using GLib.  OOP C may not seem elegant to a C++ programmer, but it works and can be easily called from C++.  Other programs in KDE link to GLib, I've heard, so this might not be so bad.\n\n*libgda is not a native KDE library.  Therefore, it might not fit in to the overarching KDE scheme.\n\n*[I'm probably missing a few more here.]\n\nAt any rate, give libgda a look if you haven't already.  It's shaping up quite nicely and can provide a good general method of accessing different database systems.  It's also nice to see more code-sharing between the two desktop projects.  Either way, good luck with your application!"
    author: "aigiskos"
  - subject: "Re: Krita / Kexi"
    date: 2003-01-24
    body: "Well it's pretty easy to wrap GObject code into C++, the gtkmm people do that all the time, there are automated tools for it. They produce pretty good bindings, though they aren't based on Qt (for instance they use std::string instead of QString etc)."
    author: "Mike Hearn"
  - subject: "Re: Krita / Kexi"
    date: 2003-01-24
    body: "what makes a database a \"real\" database to you?  i think you're thinking of RDBMS, which would still include access as it implements the R, the DB, and the MS.  a text file can be a database, but the user has to be the R, and MS. \n\nnow if you start throwing in ACID test, then you start throwing out lots of competition.  if you need redundancy built in for 99.999% uptime, the you're throwing out lots of others.  you're left with oracle maybe one or two others.\n\nGo KOffice! If I could only get nice support for my printers, but that's another project all together (Lexmark Z53, and Lexmark 4039).\n\n"
    author: " expresso"
  - subject: "Re: Krita / Kexi"
    date: 2003-01-23
    body: "Here is one !! I too agree that the Gimp is a UI headache. I do a great deal graphics design, for the web mostly (using MM Fireworks MX) and also some print graphics (have used Corel PhotoPaint 8) however I intend to persevere with the Gimp. \n\nAlthough the UI really desperately needs a whole rethink and redesign no one can ignore the immense power of the beast, it is awesome."
    author: "Paul"
  - subject: "Re: Krita / Kexi"
    date: 2003-01-24
    body: "Hmm, is Kexi just a standalone application or are KWord and Kalc (or whatever it's called) able to actually make use of it. In OpenOffice.org, it's possible to call up a DataSources slider. Create a query, create a document which has 'fields' and have OpenOffice.org fill in your document. This is still a tad buggy, and only works with adabas or ODBC, but if it'd work more correctly I'd love it for pretty invoice generation!\n"
    author: "Mathieu Kooiman"
  - subject: "Re: Krita / Kexi"
    date: 2003-03-28
    body: "Oh yes!  I agree with you completely.  I have often asked myself when the GIMP developers are going to revamp their interface.  No matter what distro or theme you use, it #1 looks ugly, and #2 is highly unintuitive.  They could do MUCH better.  If any GIMP developer reads this, I mean this as constructive criticism; you are free to email me with questions."
    author: "Charles Hastings"
  - subject: "Re: Krita / Kexi"
    date: 2004-01-03
    body: "Since a while I'm trying to find out if open source will meet my needs for my small office. Today I was looking for an alternative for Acces,for Linux of course, and I found Kexi. Wow. I use simple databases: \n.tables and relations between them, \n.forms to fill them, \n.queries to make summaries,\n.forms to display them,\n.and a menu to work with it.\nAcces gives me just enough tools to make and use them, and I hope Kexi will give me the same. I'm not a big programmar, but what I saw on the screenshots gives me hope. Thanks a lot. I'm looking forward to it."
    author: "JeanJacque"
  - subject: "Small error on the release plan"
    date: 2003-01-23
    body: "In the FAQ-section at the bottom:\n\nWhich version of Qt is required?\nQt 3.1.1 or later (3.1 recommended)\n\nSurely it should read:\nQt 3.1 or later (3.1.1 recommended)\n\n\nSorry for being such a nitpick. :)"
    author: "J\u00f6rgen S"
  - subject: "KChart needs more features!"
    date: 2003-01-24
    body: "Still missing the feature in KChart that would allow to put\nthe \"X-Axis\" data in one column and \"Y-Axis\" data in another,\nthen make some \"quasi-scientific\" plots (a'la gnuplot). I'd love\nto have the possibility to embed the kchart plots instead of\nembeding the graphics prepared using other tools."
    author: "piters"
  - subject: "KSpread and KPresenter"
    date: 2003-01-24
    body: "I hope opening foreign documents in KSpread will work in this release.  Opening Corel Quattro Pro documents in KOffice 1.2.1 would only make KSpread crash.  A bug on this was already reported.  Also, Corel Presentations shows should be incorporated in this release as well since Not everyone uses MS PowerPoint.  Support for Lotus SmartSuite and Sun StarOffice documents should be considered as well since Lotus is the other major competitor to MS Office and StarOffice is the most popular office suite for Linux at this moment.  A database component for KOffice might be useful as well."
    author: "Webgraph"
  - subject: "Re: KSpread and KPresenter"
    date: 2003-01-24
    body: "Don't cry, stand up and code. I'm learning C++ to help KDE, what about you?"
    author: "star-flight"
  - subject: "Keep going with yuor fantastic work. Don't stop!!"
    date: 2003-05-22
    body: "KDE and KOffice are remarkable. Sure the various programs need to be developed a lot more, but that will happen. KOffice has immense potential. Look to WordPerfect for some ideas on how to improve KWord - look at the features in WordPerfect that are way ahead of MS Word: reveal codes; delay codes; find and replace codes; dynamic property bar; etc. Likewise with Quattro Pro and Presentations. KOffice is only up to version 1.3 and is already remarkable. You are doing what massive corporations have refused to do. Don't ever give up!!!!  Everything you do is appreciated by an increasing community of users!!"
    author: "Laurence Field"
  - subject: "Keep going with yuor fantastic work. Don't stop!!"
    date: 2003-05-22
    body: "KDE and KOffice are remarkable. Sure the various programs need to be developed a lot more, but that will happen. KOffice has immense potential. Look to WordPerfect for some ideas on how to improve KWord - look at the features in WordPerfect that are way ahead of MS Word: reveal codes; delay codes; find and replace codes; dynamic property bar; etc. Likewise with Quattro Pro and Presentations. KOffice is only up to version 1.3 and is already remarkable. You are doing what massive corporations have refused to do. Don't ever give up!!!!  Everything you do is appreciated by an increasing community of users!!"
    author: "Laurence Field"
  - subject: "KWord"
    date: 2003-05-23
    body: "Citation tools, like those found in Endnotes and Oberon's Citation etc, would be extrememly useful in KWord and would set it apart from programs like WordPerfect and MSWord. Now that Kexi is integrated into the KOffice suite, can it be the database for such a feature? An integrated citation tool as well as the usual indexing and table of contents/figures/authorities etc tools would be fabulous. "
    author: "Laurence Field"
---
<a href="mailto:lukas.tinkl@suse.cz">Luk&aacute;&scaron; Tinkl</a>, the new <a href="http://www.koffice.org">KOffice</a> release coordinator, has posted the first version of the <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">KOffice 1.3 release schedule</a> and asked <a href="http://lists.kde.org/?l=koffice-devel&m=104237472518063&w=2">for feedback</a>. According to it the release cycle begins with Beta 1 in April and ends with the final release in early September 2003. Among the many <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">targeted features</a> are hyphenation support for <a href="http://www.koffice.org/kword/">KWord</a> and <a href="http://www.koffice.org/kpresenter/">Presenter</a>, over 100 new formulas for <a href="http://www.koffice.org/kspread/">KSpread</a> and much improved filters (<a href="http://www.koffice.org/filters/status.phtml">development status</a>). <a href="http://www.koffice.org/karbon/">Karbon14</a> will finally replace Kontour. <a href="http://luci.bux.at/projects/kexi/">Kexi</a>, a database frontend and administration tool, will join. Kugar and Kugar Designer are redesigned for the <a href="http://www.koffice.org/developer/">KOffice architecture</a>. And last but not least, both <a href="http://www.koffice.org/kformula/">KFormula</a> and <a href="http://lists.kde.org/?l=koffice-devel&m=104307204525299&w=2">Kivio</a> have found new maintainers.
<!--break-->
