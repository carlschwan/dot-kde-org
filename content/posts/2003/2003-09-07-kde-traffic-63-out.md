---
title: "KDE Traffic #63 is Out"
date:    2003-09-07
authors:
  - "binner"
slug:    kde-traffic-63-out
comments:
  - subject: "Context menus"
    date: 2003-09-07
    body: "Context menus are a pretty big problem in KDE. They are far too bloated, and suffer from attempting to reproduce the main menu in the context menu. Context menus, toolbars, and the main menu all serve the same purpose (link clickable areas to actions) but have *very* different optimizations.\n\nMain Menu --- Should contain a comprehensive listing of application functionality. The hierarchical system is optimal for searching through such a relatively large amount of information.\n\nToolbars --- Should contain often used actions that are easy to depict graphically. If something doesn't really have a good iconic representation, it probably doesn't belong in the toolbar. The whole point of the toolbar is so the user can quickly navigate to an option via a mere glance. Having 3 layers of toolbars with dozens of icons makes the toolbar utterly useless for that purpose. The pain of the default KDE toolbar setups (::cough:: Kivio) is tempered somewhat by the fact that they are editable. This is one of the many places where copying Microsoft's GUI design (specifically MS Office) is *not* a good idea :)\n\nContext menus --- Should contain commonly used actions relavent to the current object under the cursor. Like toolbars, context menus are most effective when the user does not need to read them to activate them. When a list is less than about 7 items long, it is very easy for the brain to memorize it. As a result, when dealing with short context menus, the user can quickly choose the option he wants without reading any of the other entries. However, when the context menu becomes very large, users have to linearly search through the contents, which destroys the utility of having a shortcut system in the first place. If the user has to work so hard to find the action he wants, he might as well go to the main menu, which is at least organized in a way that makes it easy to navigate through the large list of actions. Also, anchoring is important. Very commonly used items should appear in similar places in all menus. This is one thing Microsoft got right: having \"properties\" be at the bottom of every context menu is a huge boost for such a commonly used entry.\n\nNow, interestingly, its the most high profile KDE apps (Konqueror, KMail, KOffice) that have some of the worst toolbar and context menu layouts. In comparison, apps like Kate, JuK, KNode, etc, have rather good toolbar and context menu layouts. A lot of the Konqueror context menus are wholly unnecessary. Would anyone shed a tear if \"Stop Animations\" was left out of the default menu? Or \"View Document Source?\" Or \"Set Encoding?\" Or \"Security\" (which really should be a status bar icon!) That \"Set  Encoding\" is in the default context menu, and cut/copy/paste is nonsensical. Cut/copy/paste requires two actions --- selecting text, and issueing the command. If the context menu provides cut/copy/paste, the user can keep his mouse in one general location. However, \"Set Encoding\" only requires one action, so requiring the user to go to the main menu for it isn't such a loss. Even if you use both features the same amount (and I am really curious to see if anyone uses \"Set Encoding\" all that often!) its still more efficient to give the context-menu space to cut/copy/paste. "
    author: "Rayiner Hashem"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "<i>The pain of the default KDE toolbar setups (::cough:: Kivio) is tempered somewhat by the fact that they are editable. This is one of the many places where copying Microsoft's GUI design (specifically MS Office) is *not* a good idea :)</i>\n\nAnd why is this not a good idea? If you don't want to edit, then don't. This is a good thing because it allows people, who know what they want, get what they want, and leaves everybody else with the default. If I don't need 3 zillion buttons on a toolbar, then I'm happy to remove them. I'd be angry if I couldn't. Although, the toolbars should be locked down by default so that they can't be moved. How many times have one not missed a button on a toolbar in MS Office, only to displace the main menu? :)\n\n\n<i>Would anyone shed a tear if ... Or \"View Document Source?\"</i>\n\nYep, I would. :)"
    author: "J\u00f6rgen S"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "View document source is nice because it lets you view the source of a single frame. I think if you use the view menu version, it gives you the source of the frameset."
    author: "AC"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "I meant that having a huge number of large toolbars is not a good idea, not that making them editable is a good idea ;)"
    author: "Rayiner Hashem"
  - subject: "Re: Context menus"
    date: 2003-09-08
    body: "-----------------------------------------------\n...How many times have one not missed a button on a toolbar in MS Office, only to displace the main menu?...\n----------------------------------------------\nlol this reminds me of a time, a lday came into the tech bench i work at all fussed up because \"we lost her fonts\" after installing windows XP. \n\nafter explaining to her how fonts work, and how an upgrade to a new version of windows wouldn't kill her fonts she agreed to have another go at it, and if she couldn't figure it out, she'd brign the computer in.  sure enough, two hours later she is in all up in a fuss (again) with her comptuer this time.<br><br>\ni started up Microsoft Word and typed out \"Do the fonts work?\"  i then proceed to select all and go to format> fonts.  i show the lady all the fonts -- that are still there -- and she says \"well were aren't they right there?  it just says normal and headline!\"\n\nmmm office is good..."
    author: "standsolid"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "I certainly use View Document Source a lot and find it extremely useful. Taking that off would slow me down a lot while working. Also the newer added view document information is very useful and I use that a fair bit also. I do web apps and konqueror is what I develop the stuff on. "
    author: "kosh"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "You can always go to View -> Document Source.\n\n"
    author: "Tom"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "not if there are frames..."
    author: "ac"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "In that case there's \nView -> View frame source\nJust click inside the frame you want to examine\nbefore you select the menu item..\n\nBTW: I bound keys to the two actions.\n"
    author: "cm"
  - subject: "Re: Context menus"
    date: 2003-09-08
    body: "View Document Source is the perfect example of something that SHOULD be in the context menu. "
    author: "ac"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-07
    body: "> That \"Set Encoding\" is in the default context menu,\n> and cut/copy/paste is nonsensical. Cut/copy/paste\n> requires two actions --- selecting text, and issueing\n> the command. If the context menu provides\n> cut/copy/paste, the user can keep his mouse in one\n> general location.\n\nCopy and paste dont't require the user to move his mouse to the menu.\nMaybe you don't know, but as soon as you select something, it\nis copied to the clipboard. There is *no* need for clicking \"copy\".\nAnd if you want to paste something, just press the middle mouse button.\n\nIt's stupid to annoy most users with a context menu crowded by \"copy\"\nand \"paste\", only because someone thinks that some newbies don't know\nwhy their mouse has three buttons.\n\n\n> However, \"Set Encoding\" only requires one action,\n> so requiring the user to go to the main menu for\n> it isn't such a loss.\n\nThere are frames. And if you want to change the encoding of a frame,\nthe menu doesn't help.\n\n> Even if you use both features the same amount (and I am really curious to see\n> if anyone uses \"Set Encoding\" all that often!) its still more efficient to\n> give the context-menu space to cut/copy/paste. \n\nEither you have a strange configured Klipper or there is *no* need\nfor copy or paste context menu entries. Only \"cut\" could be usefull,\nbut <ctrl><x> should be faster anyway."
    author: "Asdex"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-07
    body: "> It's stupid to annoy most users with a context menu crowded by \"copy\"\n and \"paste\", only because someone thinks that some newbies don't know\n why their mouse has three buttons.\n \n My mouse has 2 buttons...\n"
    author: "Julio"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-07
    body: "Mine too... :-)\n\nI really like the way of selecting text with my mouse and right-clicking the selection to get access to copy/paste commands."
    author: "Fran\u00e7ois MASUREL"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-07
    body: "click on both at the same time.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-08
    body: "If you have enabled the Emulate3Buttons mode in you X config..."
    author: "cm"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-07
    body: "Well, according to all the specs and to what KDE is doing (or planning to do), Ctrl-X or MenuCopy is *NOT* the same thing as copy with mouse left-drag."
    author: "Navindra Umanee"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-07
    body: "> It's stupid to annoy most users with a context menu crowded by \"copy\" and\n> \"paste\", only because someone thinks that some newbies don't know\n> why their mouse has three buttons.\n\nYou may think it stupid, but most people expect these options because the rest of KDE consistently presents them when the user selects a block of text. Not having them there is bad usability.\n\nAnyway, it was already suggested that they don't have to be there always. There's no reson to have paste in most cases (bar INPUT blocks), and you only need to add Copy when text is actually selected. Cut is only required in INPUT blocks also. Following these rules would hardly bloat the context menu at all.\n\nAs I recall a kfm-devel guy said he'd implement what I described."
    author: "Max Howell"
  - subject: "Keep KDE consistent"
    date: 2003-09-07
    body: "I agree with Max if you guys think it's not sueful, while I think it's extrememly sueful at least add it for consistency and remove all those other useless options.\n\nIf this bug:http://bugs.kde.org/show_bug.cgi?id=53772  has over 400 votes it means lots of users want it and so the people saying suers don't want it must be uninformed.\n\nKDE's context menus inclu just about \n everything except an option to make you breakfast when you actually should only \n see a few entries that are commonly used. The menus are meant to include everything!\n \n What annoys me the most about these super duper bloated menus all over KDE is \n that they often have just about everything EXCEPT what I want. \n \n For example, here are the context menu entries that come up when I highlight \n some text and right click: \n \n Up \n Back \n Forward \n Reload \n Open in New Window \n Open in Background Tab \n Open in New Tab \n Add to Bookmarks \n Open With \n Encrypt FIle \n Create Data with K3b ... \n Copy To -> \n Select All \n Stop Animations \n View Document Source \n Viw Document Information \n Secuirity ... \n Set Encoding -> \n \n In addition, most of these entries do nothintg to the text. For example \"Copy To \n ->\" copies the whole page and K3b sets up the whole page for burning and \n \"Encrypt File\" would encrypt teh whole page and Open in New Tab opens the entire \n page in another tab etc. \n \n Now the funny thing is, that even with 18 context menu entries (not counting the \n hundreds of submenu entries) a simple COPY command was left out!! Another odd \n thing is that right clicking on a link will yield a simple COPY command but not \n on highlighted text!!? \n \n Wha the user actually needed to see when right clicking on highlighted text \n should have been something simple like in Mozilla: \n \n Copy \n Web Search for \"selected text\" \n View Selection Source \n \n But, I'm not really suprised that Konqueror included 15 useless and confusing \n entries when doing something as simple as highlgihting text, after all right \n clicking on empty part of the page will give you the EXACT same menu.not exactly \n context sensitive menus... \n \n The worst part is that such bloated context menus are all over KDE, for example \n clicking a floppy will yield 17 entries, albeit less than when right clicking on \n text, but still a usability disaster. \n \n I really hope that such things will get cleaned up before 3.2 this is enough to \n confuse advanced users, let alone newbies, this is perhaps the biggest problem \n with KDE. \n \n The CVS \"Actions\" menu is good but that still leaves 16 entries for right \n clicking highlighted text! \n \n \n "
    author: "Alex"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "I blame IE. IE has 15 menu entries too. There is a reason that people copy Apple and not Microsoft :)"
    author: "Rayiner Hashem"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "Fully agreed. MacOSX has five context menu items for all files, four for directories, and three for drives and most folders.\n\nIt has help, open, open with, get info, and copy \"file\" for files. \nIt has help, open, get info, and Copy \"folder\" for directories. \nIt has an eject item for drives."
    author: "anon"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "And camino in MacOSX has 6 in it's context menu (sorry, but I can't check safari.. it's crashing for me right now, GO GO STABLE UNIX-POWERERD MACOSX!!!!!!!!!!!!!!!)"
    author: "anon"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "A default installation of IE6 has 4 entries when non-editable text is highlighted.\n\nCopy\nPaste\nSelect All\nPrint\n\nI like the Select All option, but Print? That is as bonehead as KDE as it tries to print the entire page. Gee, let me think now .. One click on the print icon, or two-clicks on the context-sensitive menu?\n\nYes there are 15 entries when you are right-clicking on an empty part of the page, but the irrelevant options are greyed out.\n\nRoger."
    author: "Roger"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "Souns pretty consistant with KDE.\n\nKonqueror cvs has 14 when clicking on a file, Nautilus-2.2.2 has 15.\n\nFirebird has 10 in empty spaces in a webpage. Konq has 15."
    author: "anon"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "I completely agree with all that you have said.\n\nThe reality is the following: KDE is developed by geeks. Geeks do what pleases them. They don't give a rat's ass about the average user. KDE is predominantly a volunteer project. The ever-present banter is: Write the code yourself, pay someone to do it or learn C++(English translation: piss off).\n\nI can bet anything on the following ... Even If someone paid a usability expert to fix KDE, the CVS changes would not be merged because of a few elitist childish developers that feel that there way is better because they are actually creating the code. \"It's my way or the highway\".\n\nWhen geeks make feature suggestions/wish lists for other geeks, then it looks like they are listening to the community by implementing those features. Now, a \"newbie request\" like this is made and the answer is:\n1. Learn a new key shortcut or 2. Learn how to use your computer, your brainless cretin. \n\nI consider myself a power-user and not even I use Ctrl-C/P because I prefer using the mouse. Does that make me a brainless newbie? No, I simply have a different preference. I am so used to being able to highlight text without any action being taken (from my time in Windows). In my mind there is no logical connection between highlighting text and instantly having it copied for me. Guess what, it is called \"highlighting\" for a reason! So that is stands out more than the surrounding text. It is not called highlightcopying of text. Maybe KDE needs it's own elitist jargon.\n\nWhat this community has to realise is that KDE has now become large enough where issues like these have to be taken into account. Hundreds of millions of people are used to this behaviour from other OSs. It makes sense. It is logical, however it is not being addressed. \n\nCan someone tell me, how the hell does Up, Down, Forward and Reload relate to highlighted text? There is no logical connection. That is infinitely more broken than having one extra entry that people actually need. Please show me the community outcry (not just a wish list, but an outcry on the same level as we are experiencing now) asking for \"Create Data with K3b ... \". But that option is available.\n\nIf you want KDE is reach much higher levels of success, the \"clueless newbie\" has to be taken as being as important as the \"power user\".\n\nOne thing to remember is this; MS and Apple have spent more money on usability than KDE will ever see. Maybe, just maybe, you can swallow your pride and admit that there are things we can learn from them.\n\nWhy am I so passionate about this? I love KDE and I want to see it succeed. I really do hope that someone as glaring as this will be fixed before KDE 3.2.\n\nAlex, you are probably correct when you infer that context sensitive menus just don't work. Can a core KDE developer come clean and at least tell us that they can't fix this problem because context sensitive menus are simply broken in KDE. That would explain why they have chosen to jam as many options as possible in any given \"context sensitive\" menu in an attempt to cater for as many users as possible.\n\nI hope that this is not the case.\n\nRoger."
    author: "Roger"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: ">>The reality is the following: KDE is developed by geeks. Geeks do what pleases them. They don't give a rat's ass about the average user.<<\n\nThis is a unfair generalization.There are different people (and companies) involved with different agendas.\n\n>>KDE is predominantly a volunteer project. The ever-present banter is: Write the code yourself, pay someone to do it or learn C++<<\n\nThis is the economic model of free software: you can get the existing code for free, but all changes need to be paid for. In the end the software can still be cheaper than commercial software (because there is no monopoly that can dictate the price for new features), but you can not expect that people work for free.\n\n>>I can bet anything on the following ... Even If someone paid a usability expert to fix KDE, the CVS changes would not be merged because of a few elitist childish developers that feel that there way is better because they are actually creating the code. <<\n\nIf someone lets a usability designer redesign an application without even asking the maintainer first, or a discussion of the changes, then this can certainly happen. While not everything may be newbie-friendly, most UIs have not been designed by a random generator. KDE does offer features for advanced users, and if you just remove them for usability reasons without appropriate replacement, you probaby won't make many new friends. The reason is that those advanced users are currently paying (with work or by buying distributions) for a large chunk of the development costs.\n\n>>I am so used to being able to highlight text without any action being taken (from my time in Windows). In my mind there is no logical connection between highlighting text and instantly having it copied for me. Guess what, it is called \"highlighting\" for a reason!<<\n\nAnd because you, and admittedly many other millions of non-KDE-users, are used to this model it needs to be used on all computers for eternity? Even if the other model is much faster, especially for those who prefer use the mouse only (like you said you do yourself)?\n\n\n\n\n"
    author: "AC"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "I really should proof read my comments, calm done and have a cup of coffee before responding :)\n\n>> If someone lets a usability designer redesign an application ...\n\nI'm not talking about application redesign here, as what you said is clearly true. I am talking about creating a KDE HIG, using the existing Apple and Gnome HIGS as a reference point and then creating our own unique, logical, clear and workable solution for a consistent desktop experience. This would naturally cover the aspects being discussed regarding content-sensitive menus.\n\nThere is one comment further down where the user has to change screen resolutions to see the full contents of more than one dialog box. A clear guideline in the HIG with respect to this would help in avoiding this type of issue. The HIG could state that dialogs should be completely visible on 640px by 480px resolutions, for instance. In this way, you are not going to inconvenience anyone with regards to having to change resolutions to see the dialog box. If all the options can't fit onto one dialog screen, use tabs. Apple has shown that computer newbies -get- tabs first time around. Sure there is more onus on the application designer to get this right, but in turn it will show the level of professionalism within KDE.\n\nIf you don't want to use tabs, but you want to keep all 4000 options on one screen, look at the Advanced Tab under Internet Options under IE6. That screen comprises of a vertically scrollable list of checkboxes and radio buttons. That is a decent solution. I certainly don't subscribe to the fixed dialog sizes in Windows as that penalises me if I have a large monitor. Most dialogs should be resizable and each dialog should remember it's last size and window position, etc.\n\nOne completely bonehead aspect of Windows is the non-adherence to the location of the Options or preferences for an application. Is it Tools -> Options or View -> Options or File -> Options or Edit -> Preferences or Options -> Properties or\u0085? There certainly is a need for adherence to a set of standards so that KDE can avoid this from happening.\n\n>> And because you, and admittedly many other millions ...\n\nI am all for changing the way a model works if it improves the situation. The BeOS way of navigating an entire directory structure from a drive context sensitive menu is a perfect example of -forward- evolution. With this in place within KDE, Konqueror would not have to be opened anywhere near as often. The current change in the copy-paste model does not improve the situation in my eyes. Here is an example. You are busy typing in a text editor that does not support spell checking. You copy all your text into another editor that does support it. You now want to replace the text in the first editor with the updated text. In your model, you are forced to go and clear the text in the first editor first, before going back to the second one and copying the text so that you can paste it back into the first editor. My logic says, I am currently in text editor # 2; I must copy my work here and move to text editor #1. I then switch to text editor #1. Crash (you cannot select the text in the first editor at this point). Think of a way to work around this. Go to bottom of document and paste. Be careful not to accidentally highlight any text in the process. Go to top of document and delete original text. Be very careful not to delete updated text at the same time. So your model forces me to change the way I work. A pc is supposed to work the other way around. The windows solution to my example: text editor #1, select all + copy, go to text editor #2, select all + delete, paste.\n\nNo model should be set in stone, but change for the sake of change and being different isn't the way to do it.\n\nAnother example. There is an IE6 skin, which gives you tabbed browsing. Their solution to closing tabs is very direct, simply right-click and close or double-middle click to close. It is brain-dead easy. Simply bind your middle mouse button to double-click and the tab gets closed with a single middle button click anywhere on the tab. There is no close button per tab, or close button on the end of the toolbar, or hover mouse before close button appears, etc. Now that, in my opinion, is a good model.\n\nKDE still has no solution to single button clicking to open a folder + easy and obvious ability to select a folder without opening it (I don't care about all the reasons why people should not be doing this. People are doing this, including me). My above example can be used to solve this problem. Bind the double-click action to the middle mouse button. Now you have the left mouse button as file/folder selector and middle mouse button to open. It works. \n\nIn my day-to-day pc use, I have not found a better solution."
    author: "Roger"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: ">>I'm not talking about application redesign here, as what you said is clearly true. I am talking about creating a KDE HIG...<<\n\nStyle guide fixes are done all the time, but there are not enough people working on it.\n\n>>In your model, you are forced to go and clear the text in the first editor first, before going back to the second one and copying the text so that you can paste it back into the first editor.<<\n\nNo one claimed that KDE should not support Cut&Paste, or that it can not be useful in *complex* situation like the one you described. But 99% of all use cases are much more simple. Select a text, insert it into another document. Takes exactly 2 clicks, one for the selection and the second to paste. Or if you see a link in some application: select it, find a Konqueror window, click in the Konqueror window with the middle mouse button -> Konqueror goes to the URL. The usability of the X11 selection model is clearly superior in the majority of cases. And for the rest there is still Cut&Paste.\n\n>>No model should be set in stone, but change for the sake of change and being different isn't the way to do it.<<\n\nI believe the X11 Selection model is older than Cut&Paste.\n\n\n>>KDE still has no solution to single button clicking to open a folder + easy and obvious ability to select a folder without opening it (I don't care about all the reasons why people should not be doing this. People are doing this, including me). My above example can be used to solve this problem. Bind the double-click action to the middle mouse button. Now you have the left mouse button as file/folder selector and middle mouse button to open. It works.<<\n\nWhy do you want to select a folder without opening it or doing any other operation? You may be used to this way of operation from Windows, but it does not make sense and is pretty inconsequent, since in a web browser you need no double click to select something.\nIf you want the context menu, just right-click it. If you want to drag&drop it, just drag it. What's the reason for selecting it without doing anything? \n\nBTW there are two ways to select the folder: 1. use the rubber band, or 2. press shift.\n\n\n\n"
    author: "AC"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "> I believe the X11 Selection model is older than Cut&Paste.\n\nNope, Apple has had this kind of copy+paste forever. Since the Apple Lisa (before the Mac) in fact (1983)"
    author: "anon"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-09
    body: "I think the Xerox star (1981) cut and paste was more similar to the x-windows(1984) behaviour. It had a seperate, dedicated, cut/paste button on the keyboard if my internet sources can be relied on :).\nhttp://www.digibarn.com/friends/curbow/star/retrospect/\nThe only difference is that X-windows moved the paste button to the mouse.\n\nI do not know how the Xerox parc (1974), which introdused the cut/paste paradigm, operated its cut/paste. Maybe someone else do, but this happened before I got my first computer, but I remember me drooling on the Lisa... \n\nHaving said that, the year different cut/past paradigms was introduced should probably not be a important factor when choosing cut/paste behaviour. For me, personally, the cut/paste behaviour is the main reason I use KDE, it makes my cut and paste work like a breeze. At least when I use KDE-programs :). And a part of that is the excelent clipboard tool (klipper), I think that people not liking cut/paste in KDE is not using klipper good enough, or use non-KDE programs.\n\nHowever, if I could change one thing with cut/paste behaviour in KDE: I would have made the middle button paste on button_up. On button_down (+ a short pause with the button down) I would have the clipboard history pop up at the cursor, and saving me that trip across the screen. I do see why this is not easy implemented, but maybe one day. (Yes, I know of ALT+CTRL+V, but there are two differences, you find them ;)) "
    author: "Dragen"
  - subject: "Re: Keep KDE consistent"
    date: 2004-07-11
    body: "640x480 is in KDE's style.\n\nHowever developers cannot do everything especially if there are already working at their limit.\n\nFor example KOffice has this problem. I have even marked the bug report as \"Junior Job\" but no volunteer yet. ( http://bugs.kde.org/show_bug.cgi?id=76757 )\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "It's not that bad..."
    date: 2003-09-08
    body: ">The ever-present banter is: Write the code yourself, pay someone to do it or learn C++(English translation: piss off)\n\nI see a lot of usabillity issues, that users point developers on, getting solved. Believe me this boated context menus issue in konq will get solve the same way ;-)\n\n> One thing to remember is this; MS and Apple have spent more money on usability than KDE will ever\n\ntrue, but in fact the KDE crowd is looking carefully at other desktops (as i'm making up from a lot of posts).\n\nrelax, and enjoy..."
    author: "cies"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "Even though I agree with you about the context menus, I think that this is rather unfair to the KDE devs. Many KDE developers do take usability very seriously, both for power users and regular users. In fact, the KDE developers realized a long time ago that a usable desktop is usable for either kind of user.\n\nps. The toolbar/context menu debate should not be framed in terms of power/newbie user. I don't qualify as a newbie. I have no desktop icons, use hotkeys for everything, and spend most of my time in Vi inside Konsole. My objection to bloated toolbars and context menus is not that its confusing or difficult, but that its inefficient. If you are browsing one-handed (which is often the case when you're just surfing) you don't want to have to reach over to the keyboard to hit CTRL-C to copy text. If you want to find a particular item in a context menu or toolbar, you want your muscle memory to pick the correct item instantly, you don't want your brain to have to linearly search the thing. "
    author: "Rayiner Hashem"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "> Even If someone paid a usability expert to fix KDE, the CVS changes would not be merged because of a few elitist childish developers that feel that there way is better because they are actually creating the code. \"It's my way or the highway\".\n\nNot really.. do you ever pay attention to the kde-usability list? The reality is that there simply isn't enough people working on KDE. If someone were hire a usability expert, it would be greatly accepted."
    author: "anon"
  - subject: "Re: Keep KDE consistent"
    date: 2003-09-08
    body: "The problem of the usability list is that it is a mainly pie-in-the-sky list. When developers ask for a small advice for the feature they are currently implementing they will be almost ignored. But when somebody dreams of a large change, dozens of people join dreaming."
    author: "Arno Nym"
  - subject: "Re: Keep KDE consistent"
    date: 2004-07-11
    body: "If I would \"give a rat's ass about the average user\", I would not develop open source software.\n\nIt is such commentaries that make flee some developers, seeing the gratitude of some. (Personally I am past that, my phase of wanting to quit due to such comments was a few months ago.)\n\nHave a nice day nevertheless!"
    author: "Nicolas Goutte"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-07
    body: "I like Copy/Paste better than the X way with the middle mouse button. The reason is because not every time I highlight something do I want to copy/paste it and it is annoying when some other text than I planned ends up showing up using the middle mouse button.\n\nI also often like to use both. Ina ddiition many users do not want to memorize shortcuts."
    author: "Alex"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-08
    body: ">Copy and paste dont't require the user to move his mouse to the menu.\n>Maybe you don't know, but as soon as you select something, it\n>is copied to the clipboard. There is *no* need for clicking \"copy\".\n>And if you want to paste something, just press the middle mouse button.\n\nNot necessarily, not if \"Seperate clipboard and selection\" option is set."
    author: "Rene Horn"
  - subject: "Re: copy/paste superfluous"
    date: 2003-09-17
    body: "i still think cut/copy should be available in the context menu, but _only_ if text/somthing else is selected."
    author: "ben"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "So true.. menus are terrible in some KDE applications.\n\nEver noticed how the KWrite (including the part so it shows up in KDevelop as well) edit menu contains no separators at all?\nNot really good for usability.. (it's much harder to find some item in this menu without having a good overview with separators in it)\n\nSpeaking of separators, why are these removed from some KDE applications?\nIt should be a function of the theme to 'remove' them or not, not the app.\n"
    author: "wup"
  - subject: "EVERYONE"
    date: 2003-09-07
    body: "If you don't like the insensitive context menus check out this wish list: http://bugs.kde.org/show_bug.cgi?id=53772 and don't forget to vote!\n\nIf you think that Konqueror as a web browser and Konqueror as afile manager aren't different enough frome ach other and that View Profile could be the answer to this, thus making it a more usable app check this out: \n\nhttp://bugs.kde.org/show_bug.cgi?id=62679\nhttp://bugs.kde.org/show_bug.cgi?id=62678\n\nOkay enough plugging for one day :)"
    author: "Alex"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "> That \"Set Encoding\" is in the default context menu,\n\n\nI dunno what languages you speak, but this is really important in Japanese where there is multiple encodings for one different scripts in one language and thus you need to switch between them often. "
    author: "anon"
  - subject: "Re: Context menus"
    date: 2003-09-07
    body: "The same for Russian.\n"
    author: "Sad Eagle"
  - subject: "Re: Context menus"
    date: 2003-09-08
    body: "Well then the answer is not to clutter menus with options that are unnecessary to a large number of KDE users, the answer is to only display the item when it is necessary or useful."
    author: "Spy Hunter"
  - subject: "Re: Context menus"
    date: 2003-09-08
    body: "I thought Konqueror had an option to auto-select the encoding? Anyway, most of the internet is in languages that have only one encoding, and I'd argue that its preferable to make only people who visit certain foreign-language websites memorize a shortcut key than to make *all* users memorize a shortcut key. Ideally, though, the menu would be configurable so a user could add this at to their specific menu."
    author: "Rayiner Hashem"
  - subject: "Set Encoding"
    date: 2003-09-09
    body: "\"Set Encoding\" cannot be removed from the context menu, since the encoding is frame-specific and the ability to switch a specific frame's encoding is very important (and sorely missed in Mozilla).\n\nThen again, most Americans won't shed a tear if the rest of the (non-English-speaking) world couldn't tak anymore."
    author: "Toastie"
  - subject: "Re: Set Encoding"
    date: 2003-09-09
    body: "> Then again, most Americans won't shed a tear if the rest of the (non-English-speaking) world couldn't tak anymore.\n\nWow, you couldn't resist could you? Please take your trolls elsewhere. The grandparent poster perhaps may not even have been American or primarily English speaking. So fuck off, there isn't an American conspiracy to get you asshat."
    author: "anon"
  - subject: "Re: Context menus"
    date: 2003-09-10
    body: "BTW: Wouldn't it be a great idea to implement something like the F12 button in (at least Windows' opera), where a context menu shows up, in which you can very quickly change such options as:\n\nopen popups always/never/on request\nenable/disable java/javascript/cookies\n..\n\nHere you could also put in options for setting the encoding or \"stop animations\"\n?"
    author: "MaxAuthority"
  - subject: "litlle adress mistake"
    date: 2003-09-07
    body: "the link about xdg http://www.freedesktop.org/standards/menu-spec/0.5-onehtml/ pointed to wrong adress."
    author: "maor"
  - subject: "Re: litlle adress mistake"
    date: 2003-09-07
    body: "You're sure that you're replying to the correct story? :-)"
    author: "Anonymous"
  - subject: "links"
    date: 2003-09-07
    body: "linking to the same document twice within two consecutive sentences is bad syle. but i almost have lost hope that you'll ever understand.\n\nthanks for kde-traffic, though!"
    author: "me"
  - subject: "Re: links"
    date: 2003-09-07
    body: "Where is there a link to the same document twice?  You know, you can easily check this in Konqueror.  Hover your mouse over the link and you can see the URL in the statusbar."
    author: "ac"
  - subject: "Re: links"
    date: 2003-09-07
    body: "There was a duplicate link which is now gone while the public moaning will stay forever."
    author: "binner"
  - subject: "Re: links"
    date: 2003-09-09
    body: "Who was that little barb directed to?  Binner or myself?"
    author: "Russell Miller"
  - subject: "Re: links"
    date: 2003-09-09
    body: "That person was directing it against the Dot."
    author: "ac"
  - subject: "Windowlist/kicker usability"
    date: 2003-09-07
    body: "I posted to kde-devel some prelimary comments on my work to make an exclusiveSqueeze type function for KStringHandler which could be used in situations where you're likely to get identical strings if you csqueeze them (eg the WindowLists), but I got no responses at the time.\n\nI've still got the work and plan to finish it at some point, but as the enthusiasm seemed to have disappeared I've gone back to developing my other current pet-project.\n\nNice to see that thread featured in KDE-traffic though, thanks Russell/Stephen, whoever did it this week :)"
    author: "Max Howell"
  - subject: "Re: Windowlist/kicker usability"
    date: 2003-09-07
    body: "KDE Traffic is as always done by Russell, I only wrote the two sentences of the dot story and published it."
    author: "binner"
  - subject: "HELP OUT KDE!"
    date: 2003-09-08
    body: "http://bugs.kde.org/show_bug.cgi?id=63868\n\nNo, it's not really related to the topic, but it is related to everything about KDE, this website, KDE Traffice, the CVS Digests etc."
    author: "Alex"
  - subject: "Re: HELP OUT KDE!"
    date: 2003-09-08
    body: "Help while others are trying to actively destroy it :/. \n\nTried latest redhat beta (severn) yesterday, and few remarks:\n- No single KDE application or library gets installed by default\n- All KDE applications are hidden behind 'more applications' tabs\n- *ONE* KDE application started properly from menus after install (konsole)\n- Some KDE applications do start from the command line, such as konqueror, but do not work a single bit. In konqueror, 'configure konqueror' menu never appears no matter what you do\n\nHow broken can you actually get it?! I tested it roughly for 10 minutes before giving up with it."
    author: "foo"
  - subject: "Re: HELP OUT KDE!"
    date: 2003-09-08
    body: "\"How broken can you actually get it?!\"\n\nA beta? Who knows ;)\n"
    author: "Random coma"
  - subject: "Re: HELP OUT KDE!"
    date: 2003-09-09
    body: "Oh stop whining. If RedHat want to dump KDE, let them; it's their right to do whatever they want with it, within the terms of the software licenses. And as someone else said - it's a beta.\n\nRedHat won't \"destroy KDE\", as you put it, because every other distribution uses KDE, and many favour it over GNOME. Do you hear GNOME developers whining about SuSE's adoption of KDE?\n\nThe best response would simply be to look at why RedHat favours GNOME, ask if KDE needs to work on areas where GNOME is considered better by RedHat, and then try to make KDE a better alternative.\n\n"
    author: "Tom"
  - subject: "Re: HELP OUT KDE!"
    date: 2003-09-09
    body: "Your not being objective.  He clearly listed the things Red Hat did to KDE but your just kneejerking around."
    author: "ac"
  - subject: "Re: HELP OUT KDE!"
    date: 2003-09-09
    body: "> The best response would simply be to look at why RedHat favours GNOME, ask if KDE needs to work on areas where GNOME is considered better by RedHat, and then try to make KDE a better alternative.\n \nYou think such a reason exists other than politics? If it wasn't for RH, we would already have that unified desktop.\n "
    author: "foo"
  - subject: "Re: HELP OUT KDE!"
    date: 2003-09-10
    body: "How is it Redhat's fault that Trolltech did not originally want to have Qt under a GPL License. The only thing that forced them to do that was money. They knew if they did not give in, GNOME would take over and the opportunity on Linux would be lost. So maybe even KDE proponents should thank Redhat because they had the power to make Qt change their licensing. If it had not been for Redhat, there may not have been free Qt."
    author: "Maynard Kuona"
  - subject: "Re: HELP OUT KDE!"
    date: 2003-09-10
    body: "What Redhat wants to do is provide a platform for which developers can choose whether to make proprietary software, or open source, without their decision having to be affected by other licensing issues.\n\nStandardising on Qt forces their customers, or developers to either license Qt, or just make open source software. GTK avoids the issue of extra money being needed to get off the ground. If for some reason someone thinks Qt is better, and wants to make proprietary apps, then they can license it and do it there anyway, Redhat does ship Qt and KDElibs. Redhat also has more freedom to influence GTK development than they could Qt, since someone else owns Qt."
    author: "Maynard"
  - subject: "About, the SuSE arguement"
    date: 2003-09-10
    body: "YEs, SuSE choses KDE as the default, but GNOME Is still good jsut like it is Mandrake. In RH they completely mess it up. Same goes with Ximian. If you install XD 2 you ruin your KDE< it won't start sometimes or you can't open any apps, it's as if it were sabatoged."
    author: "Alex"
  - subject: "Re: HELP OUT KDE!"
    date: 2004-07-11
    body: "got a question..can u see windows files on linux aka \"KDE\"..if so ..someone please tell me how..i can't figure it out"
    author: "kewl joe"
  - subject: "Konqueror and tabs"
    date: 2003-09-09
    body: "My problem is that I want to use tabs when I middle-click on links in a html document but I want to get a new window if I click on a folder locally.\nIs there a way to configure Konqueror like that?\n"
    author: "anonymous"
  - subject: "Re: Konqueror and tabs"
    date: 2003-09-11
    body: "No, did you file a wishlist entry at http://bugs.kde.org already?"
    author: "Anonymous"
---
<a href="http://kt.zork.net/kde/kde20030901_63.html">KDE Traffic #63</a> was released this week with news about cookie problems, discussion about Cut and Copy entries in the context menu of Konqueror, usability of the Kicker window list, the proposed move of kpdf from kdenonbeta to kdegraphics and more. Read the full report at the <a href="http://kt.zork.net/kde/archives.html">KDE Traffic site</a>.
<!--break-->
