---
title: "eWEEK: Alpha Code Shows a Strong KDE 3.2 Ahead"
date:    2003-09-24
authors:
  - "binner"
slug:    eweek-alpha-code-shows-strong-kde-32-ahead
comments:
  - subject: "I agree"
    date: 2003-09-24
    body: "KD 3.2 will be GRET!!\n\nJust look at all the features it is planned to have: http://developer.kde.org/development-versions/kde-3.2-features.html"
    author: "Mario"
  - subject: "Re: I agree"
    date: 2003-09-24
    body: "I'd be very happy to see more progress on consistency between desktops. I'm not an advanced KDE developer, but I'll give my two cents anyway :)\n\nGoal:\n\nApplications regardless of the toolkit / widget libraries (GTK, QT, KDE) shoud look like and act like others.\n\nFreedesktop.org has made drafts and standards for desktop, and more of these standards should be in KDE. I think that has been done with menus, but should be done in icons and themes also.\n\nTo achieve this, we need:\n\n1. common configuration between desktops, which defines:\n    - mouse settings, like does single click open, doubleclick timeout etc.\n    - fonts\n    - colors\n    - widget theme\n    - icon theme\n\n2. a way to tell all applications, that settings have changed, and they need to reload them (Fam, dcop or something)\n\nIf these would be done, I could change these settings in KDE Control Center or in Gnome Control center, and all applications would look and act the same.\n\nEleknader\n\nP.S. I wish there was a way to replace GTK filedialog with KDE one ;) Probably some of Gnome users would like to replace KDE one with the one they use in GTK apps.\n"
    author: "Eleknader"
  - subject: "Re: I agree"
    date: 2003-09-24
    body: ">> 1. common configuration between desktops, which defines:\n>> - mouse settings, like does single click open, doubleclick timeout etc.\n\nI'd like to see that in KDE as well..\n(I've got double click enabled but there are programs that force me to use single click, like the CC)\n"
    author: "poephoofd"
  - subject: "Single click should *ignore* doubleclicks"
    date: 2003-09-24
    body: "I personally like being able to launch stuff with a single click, but there are enough barely computer-literate Windows types that think you have to doubleclick everything everywhere.\n\nAt very least, KDE should *ignore* the second click if it falls within the doubleclick timeout rather than launch a second copy of whatever is being clicked.  Actually, this should probably be handled by QT.  If a 2nd click comes within the doubleclick interval, a doubleclick signal should be generated, not a second click signal.  The app is free to ignore the doubleclicks.\n\nLaunch feedback makes it less likely that a savvy user will click twice, but those reflex-doubleclickers out there totally freak when they get multiple windows opening."
    author: "Rob"
  - subject: "Re: Single click should *ignore* doubleclicks"
    date: 2003-09-24
    body: "For the record, I'm not a computer-literate Windows type of user.\n\nI just happen to like double clicking.\n"
    author: "poephoofd"
  - subject: "Re: Single click should *ignore* doubleclicks"
    date: 2003-09-25
    body: "Actually, that is a great idea. Every so often, I am forced to help somebody on Windows. When that happens and I come back to heaven, It takes a bit of time for me to get back to single-click. It would be nice to have an ignore double click in a number of areas. (desktop, kicker, taskbar, etc)."
    author: "a.c."
  - subject: "Re: Single click should *ignore* doubleclicks"
    date: 2003-12-21
    body: "I'm a hard-core windows user and I always set my machines to single-click activation.  It tool me forever to find out how to make Gnome do the same."
    author: "WindowsUser"
  - subject: "Re: I agree"
    date: 2003-09-24
    body: ">>P.S. I wish there was a way to replace GTK filedialog with KDE one ;) \n\nSodipodi 0.32 can do this, you have to compile Sodipodi with --enable-kde or something similar, I wish that others non-kde apps such OpenOffice.org could do the same, which is very pleasent for a KDE user."
    author: "Josep"
  - subject: "Re: I agree"
    date: 2003-09-24
    body: "Gosh you are boring.\nWe're talking about the next KDE and that's all you can say?"
    author: "OI"
  - subject: "Re: I agree"
    date: 2003-09-24
    body: "\"<I>pplications regardless of the toolkit / widget libraries (GTK, QT, KDE) shoud look like and act like others.\"\n\n\"Freedesktop.org has made drafts and standards for desktop, and more of these standards should be in KDE. I think that has been done with menus, but should be done in icons and themes also.</I>\"\n\nThere is another, more pragmatic way to achieve what you wish for.\nStop using non KDE apps, and port missing ones to KDE.  Are there really\nthat many reasons to use a non KDE app instead of its KDE counterpart?\n\nThe only case where I can see this justified is with Open Office, just\nbecause it has better MS import/export filters right now."
    author: "pureKDE"
  - subject: "Re: I agree"
    date: 2003-09-24
    body: "I perfer Mozilla/Firebird to Konqueror, and so do many other people using KDE ( http://apps.kde.com/rf/2/latest?sid=6827216a18a65bd8937d3966bffdd7c6 ) \n\nBut other than that, 3.2 has helped me remove more gtk apps.. I went from xmms->juk (noatun still sucks), gmplayer -> kplayer (noatun/arts playback still sucks), and gaim->kopete (kit sucked ass)"
    author: "anon"
  - subject: "Re: I agree"
    date: 2003-09-25
    body: "Each user has their own reasons. I'm not talking about just me.\n\nSome people need Gimp, some Grip etc.\n\nYes, it would be the best have them as native KDE apps, but these applications are developing all the time. Porting would be a waste of time compared to fixing look / feel issues.\n\nEleknader\n\n\n"
    author: "Eleknader"
  - subject: "Re: I agree"
    date: 2003-09-29
    body: "Your pragmatic way to achieve things would eventually lead me tu use Windows."
    author: "Moises"
  - subject: "RPM version of KDE 3.2 for Mandrake"
    date: 2003-09-24
    body: "I hope that KDE will oficially release n RPM compiled version supporting Mandrake 9.x."
    author: "christoph m"
  - subject: "Re: RPM version of KDE 3.2 for Mandrake"
    date: 2003-09-24
    body: "KDE doesn't release binary packages: http://www.kde.org/download/packagepolicy.php"
    author: "Anonymous"
  - subject: "Re: RPM version of KDE 3.2 for Mandrake"
    date: 2003-11-04
    body: "zo for mandrake 9.2 there are no package for kde .3.2 beta ?"
    author: "coolcabel"
  - subject: "Re: RPM version of KDE 3.2 for Mandrake"
    date: 2004-02-10
    body: "There are RPMS, build from Cooker Soures:\nftp://mandrake.redbox.cz/people/bluehawk/kde32-92"
    author: "Torsten"
  - subject: "Re: RPM version of KDE 3.2 for Mandrake"
    date: 2004-02-15
    body: "ok, rpms are great but i don't know what to do with them.  I'm a real newb to this whole linux thing...how can i get KDE 3.2 setup on a Mandrake 9.2 (i686 laptop)\n\nthanks in advance, you all rawk!!!"
    author: "appleman"
  - subject: "Re: RPM version of KDE 3.2 for Mandrake"
    date: 2004-02-24
    body: "You can use the cooker rpm's, install with rpm -Uvh *.rpm and solve the dependecies.\n\nPCLinux"
    author: "r.koendering"
  - subject: "Sounds good to me..."
    date: 2003-09-24
    body: "But I do think that it shouldn't be any rush to get KDE 3.2 out before new year... It's better to polish it until it really shines =)\nI'm looking forward to this new release and as always, I'm astonished by the skill on how this great project is running."
    author: "Dave"
  - subject: "Completely agree!"
    date: 2003-09-24
    body: "KDE 3.2 should not be rushed and I would actually prefer a 2004 release, so that KDE will seem even more recent. It's a lot better for marketing IMO and makes the user feel he is using the absolute state of the art technology.\n\nIn addition, as every poll has shown the majority of KDE users get KDE from their distros and as you know distos won't ship until mid April 2004 earliest, so as long as it's not postponed past late February everythign is great.\n\n\nCurrently KDE 3.2 is shaping up to be a great release, but according to the feature plan http://developer.kde.org/development-versions/kde-3.2-features.html just a little less than half of the planned features have been implemented. This si why I think post poning is necessary, and also let's heep up a tradition which GNOME has recently adopted too, quality vs features.  "
    author: "mario"
  - subject: "Re: Completely agree!"
    date: 2003-09-24
    body: "If it's better than KDE 3.1 it should be released. It will have less bugs and better quality, better features and a lot of optimizations in it.\n\nThe feature plan obviously will not be completely implemented. It's no drama to postpone more to 3.3 it will come.\n\nThe marketing aspect is no point at all. It will be released when it's ready like always. It appears to be very ready. Some kwin work, some Kontact polishment and some bug fixing and it will be ready.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Features"
    date: 2003-09-24
    body: "If you install 3.2 from CVS you will probably see most of the features on the plan are already implemented, realy a case of developers beeing to busy coding to uppdate the plan :-)\n"
    author: "Morty"
  - subject: "Re: Features"
    date: 2003-09-24
    body: "Does anybody know if this korganizer feature got implemented\n\n\"pinning of any mime type to an appointment or TODO\" ?\n\nIt would mean a big improvement and would push korganizer one (important) step further to the enterprise level... Could somebody with an uptodate cvs head or alpha1 take a look wether it's implemented or not (and post a reply)?"
    author: "Thomas"
  - subject: "Re: Sounds good to me..."
    date: 2003-09-24
    body: "KDE 3.2 should be out as fast as possible, if only because of how faster it launches than KDE 3.1.  Watching KDE load is a shame compared to GNOME on Redhat 9."
    author: "ac"
  - subject: "Re: Sounds good to me..."
    date: 2003-09-24
    body: "Actually I couldn't care less of how fast KDE loads. I load it at most once a month... :) Of course, it might make a difference if you shutdown the machine all the time.\n"
    author: "Chakie"
  - subject: "Re: Sounds good to me..."
    date: 2003-09-24
    body: "Some of us use laptops. Some of us dual-boot. Some of us like to install new kernels. And some of just just prefer to shut down the machine when we are not using it. Saying \"just don't shut it down\" is not helping. It's just avoiding the issue."
    author: "Janne"
  - subject: "Re: Sounds good to me..."
    date: 2003-09-24
    body: "With an OS that would suspend well enough, we would not have the problem. You would suspend then turn off. Install new kernel, continue userspace (I know, likely a dream). Suspend then boot something else.\n\nSuspend should be the normal way of turning off IMO. It just isn't because the OS support is hard to do. But I expect GNU/Linux to be able to after 2.6.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Sounds good to me..."
    date: 2003-09-24
    body: "I already said it, but I guess it would have been more appropriate to write it here.\nMultiple users? At my house I have 3 accounts (me, my sis, my mother). I'd say that most people with a home computer do that, no? So even if it's always on they still have to login to their account.\n\nJ.A.\n\n"
    author: "J.A."
  - subject: "Re: Sounds good to me..."
    date: 2003-09-25
    body: "Hi,\n\nI have solved that by using two X sessions and switching between them. I understand it doesn't scale well for several users. With recent kernel changes that share pages among processes better (i.e. running KDE 2 times needs only 1 time memory for the binaries) it could scale though.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Sounds good to me..."
    date: 2003-09-24
    body: "And some of us actually have more than one user in the machine.\n\nJ.A."
    author: "J.A."
  - subject: "Very impressed"
    date: 2003-09-24
    body: "One of the coolest things about the latest CVS is how much smoother the UI feels. Right after kwin_III was integrated, window resizing in KDE became *extremely* smooth. With my laptop's button-pointer, resizing even a complex Konqueror window is as smooth as doing it with IE in XP. I don't know if the speedup is because kwin-III rocks, or because Konqueror rocks, but it makes a very nice little improvement in how smooth the UI feels.\n\nPS> Oddly, resizing isn't as smooth with my USB external mouse. Its much more choppy than the last CVS version (just prior to kwin_III integration) that I  used. I have a feeling, though, that the difference might be due to the fact that I used the previous version on kernel 2.4, and the latest one in kernel 2.6"
    author: "Rayiner Hashem"
  - subject: "Re: Very impressed"
    date: 2003-09-24
    body: "Does it use the rubberbanding you were talking about like metacity?"
    author: "mario"
  - subject: "Re: Very impressed"
    date: 2003-09-24
    body: "Hmm, I was using rubber-banding as a pejorative to describe bad resize behavior. Anyway, kwin_III doesn't appear to be doing any synchronization, because in some complex cases, the canvas will lag behind the window frame. There just seems to be some good ol' performance optimizations in the codebase. "
    author: "Rayiner Hashem"
  - subject: "Re: Very impressed"
    date: 2003-09-24
    body: "Yes, different kernels can have a large effect on your mouse movements.  The kernel mouse device handling has been changed.  The new driver changed certain things such as the mouse update rate and movement speed, at least on my machine.  Also, the interactivity improvements in the kernel can have an effect on resizing speed.  You should try both versions of KDE on the same kernel before you pronounce one better than the other."
    author: "Spy Hunter"
  - subject: "Konqueror"
    date: 2003-09-24
    body: "Where can I get the particular features expected in Konqueror? I'm specifically interested in knowing whether tabs will be able to resize [if many] just like in Mozilla, and also whether the menu has been improved so that it's actually a context menu depending on what has been selected.\n\nThanx..\n\nCb.."
    author: "Charles"
  - subject: "Re: Konqueror"
    date: 2003-09-24
    body: "You could try reading the article ;-)\n\nYes, tabs now resize.  The context menus have been simplified somewhat, but I don't know if their context-sensitivity has been improved."
    author: "Spy Hunter"
  - subject: "Re: Konqueror"
    date: 2003-09-24
    body: "> but I don't know if their context-sensitivity has been improved\n\nIndeed, it has been improved quite a bit. The only context insensitive items are now the navigational items in html pages appearing on the context menus of links (there is a patch to fix this, but it breaks on pages like kde.org, where there are links to the same page)\n\nIt requires more infastructure improvements to fix this. "
    author: "anon"
  - subject: "Will kmail filtering be improved ?"
    date: 2003-09-24
    body: "Hello all,\n\nwel,l kde 3.2 seems to be very promising. I really like everything on this desktop except taking my mails. I have been obliged to use a spam filter (spamassassin) and now kmail is just not useable any more when I take my mails. It takes so much time to filter all my messages and I cannot use kmail for anything during the filtering process. \nEven not for writing a new message or reading my old mails !\n\nWill filtering be multi-threaded and performed in background in the next release ???\n(Please say yes ! :) )\n\nkde coders, you are wonderful ! \n:)"
    author: "SocialEcologist"
  - subject: "Re: Will kmail filtering be improved ?"
    date: 2003-09-24
    body: "If it's that bad, you are probably using the spamassassin script directly. This is very slow, try using spamd/spamc instead. This isn't exactly blazingly fast either, but alot better.\n\nSee http://kmail.kde.org/tools.html for guidelines."
    author: "anonon"
  - subject: "Re: Will kmail filtering be improved ?"
    date: 2003-09-24
    body: "As a first step the pipe-through filter action will be made non-blocking. A proof-of-concept is already working. But the real solution requires some larger architectural changes in the filter manager. Hopefully it will be ready by the time KDE 3.2 is frozen for release.\n\nFor now you should\na) use the spamd/spamc combination which is about 5-10 times faster than using spamassassin directly\nb) you should not pipe all incoming messages through spamassassin but only messages that are probably spam (like messages which don't come from friends or from mailing-lists you are subscribed to).\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Will kmail filtering be improved ?"
    date: 2003-09-24
    body: "Hello again,\n\nI already use spamd and spamc but it remains really slow. \n\nI just use this filter for incoming messages that haven't been filtered yet, i.e messages that do not come from a mailing list. But in your reply you mention to filter messages based on my friends' address. I wrote a small bash script that checks if a mail has been sent by someone who has an entry in my kaddressbook and I use this script to filter friend messages. Is there any better solution around ?\n\nA non blocking kmail would be sweet ... :)\n\nThx for your replies."
    author: "SocialEcologist"
  - subject: "Re: Will kmail filtering be improved ?"
    date: 2003-09-24
    body: "Can you post this script? I'd much appreciate it as I have exactly the same problem you have."
    author: "GeekBoy"
  - subject: "Re: Will kmail filtering be improved ?"
    date: 2003-09-24
    body: "I use QSF (quick spam filter).\nThere's even a guide for setting up under kmail.\n\nhttp://www.softwaredesign.co.uk/Information.SpamFilters.html#qsf\n\nCombine that with pre-run filters for any mailing lists you're on and that should be fast enough."
    author: "caoilte"
  - subject: "Re: Will kmail filtering be improved ?"
    date: 2003-09-25
    body: "If you have your own mail server, the very very best is to just filter it before you get it, like this: http://www.pycs.net/lateral/stories/9.html\n\nEven if it's just your own box, maybe it's still worthwhile."
    author: "Roberto Alsina"
  - subject: "What about IMAP filters?"
    date: 2003-09-25
    body: "I mean, are there any plans for implementing filters that work with IMAP and not only POP? The reason is that in kmail my POP email gets filtered through spamassassin, while IMAP doesn't..."
    author: "Luca Beltrame"
  - subject: "Superficial"
    date: 2003-09-24
    body: "The review is very superficial. It probably took them less than an hour to perform it. Basically, they have launched every application to see if they work and that's it.\n"
    author: "Philippe Fremy"
  - subject: "screenshots??"
    date: 2003-09-24
    body: "I would love to see some screenshots of the different things in 3.2!!!\n\n"
    author: "LD"
  - subject: "Re: screenshots??"
    date: 2003-09-24
    body: "The screenshot section of kde-look.org may be your friend... Get your screenshot galore today with lots of kde cvs, alpha 3.2... screenies\n\ne.g.\nhttp://www.kdelook.org/content/show.php?content=7546\nhttp://www.kdelook.org/content/show.php?content=7919\nhttp://www.kdelook.org/content/show.php?content=7855\nhttp://www.kdelook.org/content/show.php?content=7625\n\nYou won't find any out-of-the box screenies but heavily themed and modified kde3.2 deskops from other people. But you still get an impression..."
    author: "Thomas"
  - subject: "Re: screenshots??"
    date: 2003-09-24
    body: "A request for the screen shooter would be to take pix of sections.\nIt would be nice if the users should be able to select via rubber banding what they are interested in taking a pix of.\n\nKeep up the good work."
    author: "a.c."
  - subject: "Re: screenshots??"
    date: 2003-09-24
    body: "That's already been implemented.\n"
    author: "Sad Eagle"
  - subject: "Re: screenshots??"
    date: 2003-09-24
    body: ">That's already been implemented.\n\nAnd it rocks too! :)\n\nksnapshot is great."
    author: "anonon"
  - subject: "Re: screenshots??"
    date: 2003-09-25
    body: "That was added to the new 3.2 stuff?\nIf so, cool.\nIt sure is not in 3.1"
    author: "a.c."
  - subject: "Re: screenshots??"
    date: 2003-09-25
    body: "yes, it's in 3.2. not in 3.1."
    author: "anon"
  - subject: "Why not this?"
    date: 2003-09-24
    body: "I have no exchange to check it with, but...\n\nhttp://freshmeat.net/projects/outlookgrabber/?topic_id=31%2C28\n\nAbout:\n Outlook grabber is a Perl script that allows you to download mail from a Microsoft Outlook Web Access server and put it in your mailbox. Its functionality resembles that of Ximian Connector, but this script can be used in combination with any mail program. \n\nCan't this be integrated into kmail like the hotmail downloading script is?\n\nMaybe even see if these things are installed and adda  couple of options to the account wizard? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Why not this?"
    date: 2003-09-25
    body: "The problem, as I see it, is that AFAIK, no active kmail contributor actually uses Exchange. "
    author: "anon"
  - subject: "Re: Why not this?"
    date: 2003-09-25
    body: "Of course the simple solution is:\n\nONE of those who say \"kmail needs Exchange access\" has to lend a hand ;-)\n\nIf noone does, then maybe KMail didn't need exchange support so badly."
    author: "Roberto Alsina"
---
<a href="http://www.eweek.com/">eWEEK</a>'s senior analyst Jason Brooks installed the <a href="http://dot.kde.org/1063222993/">first KDE 3.2 alpha release</a> from source to <a href="http://www.eweek.com/article2/0,4149,1277046,00.asp">review its current state</a>. Although he obviously missed the <a href="http://www.kde.org/info/requirements/3.1.php">KDE requirements</a> and ended with a build without Xft2 support, he was impressed: <i>"KDE 3.2 impressed us with improvements to its <a href="http://www.konqueror.org/">Konqueror</a> Web browser and file manager and with the addition of a handful of new applications, including <a href="http://kopete.kde.org/">Kopete</a>, a multiprotocol instant messaging client."</i> He also had a look at <a href="http://kontact.org/">Kontact</a>, new wireless networking tools, the improved <a href="http://www.tjansen.de/krfb/">remote desktop connection</a> and the <a href="http://accessibility.kde.org/">accessiblity tools</a>. Some crashes he encountered may be already solved in the second alpha expected to be released this week.
<!--break-->
