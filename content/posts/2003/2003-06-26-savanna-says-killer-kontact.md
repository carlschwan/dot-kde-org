---
title: "Savanna Says: Killer Kontact"
date:    2003-06-26
authors:
  - "numanee"
slug:    savanna-says-killer-kontact
comments:
  - subject: "Screenshots, Screenshots"
    date: 2003-06-26
    body: "It would be great if articles like that could have embedded screenshots of the app. Beside that, nice article."
    author: "AC"
  - subject: "Re: Screenshots, Screenshots"
    date: 2003-06-26
    body: "Just follow the link \"preview\" just up here."
    author: "ViRgiLiO"
  - subject: "Re: Screenshots, Screenshots"
    date: 2003-06-26
    body: "I know where to find screenshots, but I said 'embedded screenshots'."
    author: "AC"
  - subject: "Great article !"
    date: 2003-06-26
    body: "Thank you Savanna. I am a long time linux/unix user, for so long that I never actually used outlook (i have been been MS free for almost ten years). Then I started using kmail, and by the time evolution (the current linux killer mail app) came out, I was already hooked to kmail. And I also heard there were plans to integrate the PIM stuff on KDE. So I decided to wait.\n\nLong story short: I loved your article, it is very pedagogical, it explains in simple but clear words what is all this integration about. Now I can't see the time kde 3.2 is out :-)\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Emailing appointments from Korganizer ?"
    date: 2003-06-26
    body: "Is it going to be possible to schedule, say, a meeting with other people, and have korganizer automatically email a reminder to everyone involved  a couple hours before the meeting ? \n\nOr add a doctor appointment and have an email sent to mysef an hour in advance ? \n\nLast time I checked (a year or more ago) this was still not working, it would be great. It is the only thing I miss from my former solaris workstation :-)\n\nThanks in advance !\n\n"
    author: "MandrakeUser"
  - subject: "Re: Emailing appointments from Korganizer ?"
    date: 2003-06-26
    body: "It may require a little more effort but these things can be done with DCOP and both Kmail and Korganizer have triggering capabilites. You can use kdcop to explore the DCOP interfaces. Just have a script set up and and have the app run a program. It's less than emminently user friendly but it is something that can be done (as oppsed to can't be done) and with the right scripts can become very reusable.\n\nFor even more fun with integration you can try Kommander which allows you to construct strings simply by associating text with widgets in it's dialogs. Kommander also speaks DCOP, runs bash scripting natively and can call scripts in any language. You can find snapshots of the editor and some created dialogs at http://quanta.sourceforge.net/main2.php?snapfile=snap02 and docs at http://sourceforge.net/project/showfiles.php?group_id=4113. We will be adding more functionality to Kommander soon including script containers and a script repository to make this easier for users.\n\nKommander is the easiest visual dialog creation tool ever because it can create useful dialogs without a single line of code and these dialogs can expose and exchange information and manage nearly every aspect of a KDE application as long as they good DCOP support. This makes Kommander the ultimate user desktop \"application glue\". Kommander is part of the Quanta package developed from QT Designer to make Quanta and other KDE apps user extensible. Enjoy!"
    author: "Eric Laffoon"
  - subject: "Re: Emailing appointments from Korganizer ?"
    date: 2003-06-26
    body: "Thanks a lot for the tip Eric. I will give Kommander a shot whenever I get some time to do it. I still have the feeling that this funcitonality is going to be implemented (I either submitted or read a wishlist bug report some time ago on this topic). I thought it would be one of the features of the PIM changes in 3.2, I'll probably ask around or submit a wishlist again, unless you tell me you are sure it is NOT being implemented. \n\nBut in any case it is great to know about Kommander. Cheers !"
    author: "MandrakeUser"
  - subject: "Re: Emailing appointments from Korganizer ?"
    date: 2003-06-26
    body: "> Thanks a lot for the tip Eric.\n\nYou're welcome.\n\n> I will give Kommander a shot whenever I get some time to do it.\n\nRemember that it will likely seem utterly strange and incomprehensible at first looking at widget text association. So look at the docs and examples. Somewhere in there it generally hits you in a flash because it's so simple. Genius always is simple and we were inspired by Terek Zsolt's Kaptain software. Kommander benefits from visual design though among other things. Once you see what it can do you will realize it can integrate into any KDE application and solve many feature request issue as well as help to personalize and customize your desktop.\n\n> I'll probably ask around or submit a wishlist again, unless you tell me you are sure it is NOT being implemented. \n \nLook through KDE's Bugzilla. I am too busy with Quanta and Kommander to know all that is happening on kdepim. ;-)\n\nCheers"
    author: "Eric Laffoon"
  - subject: "Re: Emailing appointments from Korganizer ?"
    date: 2003-06-26
    body: "> Thanks a lot for the tip Eric.\nAgain. korganizer automatically email a reminder to everyone involved is a functionality I would like to have.\n\n> we were inspired by Terek Zsolt's Kaptain software\nI love Kaptain. It's this sort of very clever little big tools."
    author: "andrianarivony"
  - subject: "Re: Emailing appointments from Korganizer ?"
    date: 2003-06-27
    body: ">  I love Kaptain. It's this sort of very clever little big tools.\n\nObviously I do too and currently it has a few features Kommander doesn't, but when looking at doing some enhancements to Kapatin we decided to build Kommander instead. The similarities are many including text associations. The differences are instead of writing it in a grammer you draw it in a stripped and modified QT Designer. Kommander also can contain scripting and makes inclusion of new KDE widgets very easy for developers. We plan to have data aware widgets soon. It also is DCOP enabled. Both can live update (as of version 0.7 of Kaptain) and Kaptain has built in support for sockets. Both are very good tools. I think Kommander is a little more focused on end user ease of use though."
    author: "Eric Laffoon"
  - subject: "Re: Emailing appointments from Korganizer ?"
    date: 2003-06-27
    body: "An easy way to do someting like this is to run a skript like the following:\n\n#!/bin/bash\necho \"any body text\" | mail -s \"subject text\" email@address.com,email2@...\n\nI also tried to type in this command directly, but commands with arguments seem not to be accepted.\n\nRegards\nGuenther\n"
    author: "guenther.palfinger"
  - subject: "Re: Emailing appointments from Korganizer ?"
    date: 2003-06-27
    body: "Thank you Gunther !\n\nYes, it is possible to script these kind of things, but then of course, the whole idea of KDE is to provide a graphical, user-friendly environment :-) \n\nI'll check bugs.kde.org - Thanks again !"
    author: "MandrakeUser"
  - subject: "Features of Operas enw email client"
    date: 2003-06-26
    body: "I use KMail as my main email client, but I did have a brief play with the new email bit that comes with Opera.  There are some features in there i'd *love* to see in a KDE system.\n\nThe way it automatically groups messages into multiple access points depending on things like who they're from and what labels you place on it are excellent..."
    author: "Kevin Hughes"
  - subject: "Re: Features of Operas enw email client"
    date: 2003-06-26
    body: "\"Search folders\" aka \"virtual folders\" are already working in KMail/HEAD."
    author: "Anonymous"
  - subject: "Exchange Support"
    date: 2003-06-26
    body: "The biggest problem I have with not using Windows is the lack of Exchange support. Fortunately this only affects Calendar, but it is still a pain. The outlook plugin in KOrganizer doesn't work because the Exchange server at my work doesn't have webdav enabled.\n\nAre there any plans to create native exchange support for korganizer/kontact? If not, what's the best way to convince my IT to turn on webdav, keeping in mind that they'll never understand why I don't use Windows instead?"
    author: "David Johnson"
  - subject: "Re: Exchange Support"
    date: 2003-06-26
    body: "That's not really possible. Even ximian's outlook connector only speaks webdav. Talk to your admin.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Impact of strike..."
    date: 2003-06-26
    body: ">> Suddenly, KDE will go from being a desktop user system to an integrated business groupware system as well.\n\nSuddenly means when 3.2 comes out or what ;-)\n\nI wonder what will happen if this Kontact/kolab killer duo strikes. It will be soon (but after 3.2 is released) I guess...\nRead this and forecast for your self:\nhttp://www.hindustantimes.com/news/181_288709,0003.htm\n\nThey will probably strike hard... I hope big coms/orgs who use this on full scale will contribute money/developers to the team (or team\"s\": kolab, postfix, openldap, etc.) -- so it might become the apache (vs. IIS) scenario of the furure, *grin*.\n\nAnd this will only be the first of a serie... Once KDE desktops get used in companies, Qt will gain momentum in general. More commercial software will get built on Qt -- portability will be higher on the priority list.\n\n<silly>\nI think adobe should move to Qt! That will rock ;-)\n</silly>\n\nWho's hopeing with me?"
    author: "cies"
  - subject: "Re: Impact of strike..."
    date: 2003-06-26
    body: "Adobe already uses Qt for new products, see Photoshop Album."
    author: "Anonymous"
  - subject: "What about Aethera?"
    date: 2003-06-26
    body: "Kontact is not KDE's first groupware client by any means. Aethera has been in the works for years. Why start another one when there is already a project out there?"
    author: "Steve Hunt"
  - subject: "Re: What about Aethera?"
    date: 2003-06-26
    body: "That's because Aethera is terrible.  It's very, very slow.  Closed source.  Last time I used it (admittedly close to a year ago) it, it wouldn't import my old email inboxes correctly (from Eudora).  And it's a Qt app (which has be ported from Qt2->Qt3).  Not that the last is a bad thing, it just means its not going to behave like the rest of your KDE apps.\n\nAll in all, Aethera, while having a *much* cooler name, is probably not the best solution.  Not sure if Kontact is or not, but you've gotta admit that its impressive how far they've come in such a short time.  Aethera's been under development since before January 2001."
    author: "Bill"
  - subject: "Aethera IS open source"
    date: 2003-07-10
    body: "Bill wrote:\n\n> Closed source.\n\nNope, Aethera is GPLv2, incorporating some MIT/X-licensed code from the antecedant Magellan project from which it was forked.\n\nWhat you're probably thinking about is theKompany's proprietary optional plug-ins for Aethera, most (all?) of which are proprietary.\n\nRick Moen\nrick@linuxmafia.com"
    author: "Rick Moen"
  - subject: "Confirmed:  Aethera is GPLv2"
    date: 2003-07-10
    body: "Because I keep seeing people, including KDE core developers, claim that Aethera isn't open source, I've done my best to [re-]investigate, this evening.  I wanted to get to the bottom of the matter, because I maintain http://linuxmafia.com/~rick/linux-info/applications-muas.html (a list of 117 known MUAs for Linux, classified by licence category and individually described), and try to keep it accurate.\n\nIt looks like theKompany has been at times slow in getting source tarballs out in public, e.g., with their current RC2 release, available as binaries only via a SourceForge site.  However, such annoyances aside, the core application is indeed open source under GPLv2.  You need only have a look at the contents of http://ftp.kde.com/Office/Groupware/Aethera/aethera-2.0.tar.gz .\n\nAs I mentioned in my other posting (earlier), it IS true that theKompany would like to sell you various optional Aethera plug-ins (such as Jabber support) under proprietary licensing, but Aethera is functional without them.\n\nPersonally, I'm much, much more enthusiastic about the Kolab server (and its Kontact client), as full-blown groupware using real open standards has been a big hole in the Linux world for a long time.  Open Source Application Foundation also intends to fill this hole eventually with a long-overdue implementation of the IETF CAP and RFC-2445 \"iCalendar\" protocols for scheduling servers, but only after the Chandler PIM is done, so that's probably going to take a while.  But either of these combinations will be a major win.\n\nI try to track developments in that area at:  http://linuxmafia.com/~rick/linux-info/groupware\n\nRick Moen\nrick@linuxmafia.com"
    author: "Rick Moen"
  - subject: "Re: What about Aethera?"
    date: 2003-06-26
    body: "> Why start another one when there is already a project out there?\n\nAside from the reasons already gives this is pretty grossly missing the facts. I'll grant you that it is good to see if you can possibly join another open source project before spinning off another, but as mentioned Aethera's not open source. IIRC it started that way and the question could have been asked of it. The point of my first staement is that in fact Aethera set about recreating much of the functionality of several KDE apps while making use of some in reusable kparts. Kontact is making the progress so quickly exactly because it's *not* starting over but building on existing KDE apps. In fact Kontact is first of all a shell for four KDE apps as mentioned. \n\nIn addition IIRC Aethera doesn't have a server solution, or at least an attractive one. I forget the name but there is another KDE email app using a free server that also falls into this category, though I'm not sure it's been developed much since KDE 2. Substantial improvements for group usage is being contributed to KDE in the German contract for these improvements. The Kolab server will also result meaning that there will be a free server replacement for M$ Exchange that runs on Linux and serves both outlook and Kontact which will then interact on a level not previously possible. This will be huge!\n\nIn any way you care to look at it Aethera wan't even close as a choice. "
    author: "Eric Laffoon"
  - subject: "What about Infusion"
    date: 2003-06-26
    body: "> I forget the name but there is another KDE email app using a free server \n> that also falls into this category, though I'm not sure it's been developed\n> much since KDE 2\n\nIt's called Infusion http://www.shadowcom.net/Software/infusion/ \nInfusion was using the Citadel/UX server http://uncensored.citadel.org/citadel/  \n\n\"Citadel can send mail between local users, send and receive internet mail, and make mail available to internet users. This brilliant arrangement means that, as far as mail is concerned, any diehard Mutt user such as myself can happily coexist on a Citadel/UX network with Infusion users\" (Navindra Umanee in \"Focus on infusion\" http://dot.kde.org/992627943/ )\n\nInfusion was promising. Unfortunatly it seem that no one join the developper to help him.\nSee also \"More on Citadel\" http://dot.kde.org/992627943/992676648/\n"
    author: "andrianarivony"
  - subject: "Re: What about Aethera?"
    date: 2003-06-27
    body: "They're really quite different approaches -- Kontact is basically about reusing mature parts of KDE and wrapping them up and plugging them into a single application.  All of the code from those projects is shared in Kontact and needn't be maintained separately.\n\nTo put things in perspective, if you sum the parts of KDE PIM that make up Kontact:  KMail, KOrganizer, KNotes, KAddressbook, libkdenetwork, libkdepim -- and finally the glue between them, Kontact, it's around 200,000 lines of code.  Kontact is about 4,000 -- 2% -- of that total (i.e. slightly larger than KNotes).\n\nSo really, if you want to talk about duplication of effort and reuing what's available -- that's exactly what Kontact does.  Aethera, on the other hand, rather than being a small wrapper around these existing apps, must duplicate all that they do."
    author: "Scott Wheeler"
  - subject: "serial letters?"
    date: 2003-06-26
    body: "will kontact make it possible to use kaddressbook-entries for serial letters (in koffice or openoffice)? this would also be anything \"what KDE needs, and needs badly\", if you think of the corporate desktop. "
    author: "kde-user"
  - subject: "Re: serial letters?"
    date: 2003-06-27
    body: "Well, that's all abstract from Kontact. Ask the KOffice devels wether they could integrate libkabc as datasource for mailmerge (not sure this works yet).\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "The killer feature of Kontact"
    date: 2003-06-27
    body: "Last month, I made a big party to celebrate my birthday, and emailed a lot of friends inviting them. A lot of messages bounced. Invalid old addresses!\n\nSome of them bounced because I had three or four different entries in my kaddressbook for each of them. It's very easy to add a new address, but to delete an old one I have to: 1) open kaddressbook 2) search for the name 3) delete the ivalid email. Result: I _never_ delete invalid entries from my contact list.\n\nI'd like an integrated mail/address book software to autmatically delete the invalid entries from my contact list. \n\nHow to implement it? Simple: use VERP. The idea is that each message sent out will have a unique envelope sender, and you codify the destination address in it (e.g., if user@example.com send a msg to paulo@destiny.com, the envelope sender would be user+paulo=destiny.com@example.com). If the message bounce to user, it's easy to parse the failed address. \n\nFailed addresses would be marked as bounced, and not be used for automatically completions, maybe moved to a different category, and deleted after some bounces. \n\nSure it all depends if the SMTP server of the sender allows these kind of messages. To check it I'd send a verp message to myself.\n\n"
    author: "Paulo Eduarod Neves"
  - subject: "Re: The killer feature of Kontact"
    date: 2003-06-27
    body: "That sounds pretty much like a killer feature for spammers who want to get rid of invalid email addresses. Anyway, the dot is the wrong place for feature wishes. Use http://bugs.kde.org instead.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: The killer feature of Kontact"
    date: 2003-06-28
    body: "If spammers used this feature to clean up their address lists, wouldn't that make using KDE's 'bounce' feature become a good way to delist yourself? The spammer wouldn't know the difference between a real bounce and an anger-bounce."
    author: "Glenn Alexander"
  - subject: "Nice article"
    date: 2003-06-27
    body: "Nice article I got a \"goose skin\" just from reading it ;-). I fully understand the excitement as I am using kontact already on a everyday basis. It rocks - would be small understatement.\n\nThe KDE TEAM and all their partners are doing a superb job which is impossible to describe in a few words. CUPS is integrated into kdeprint. Samba can be accesed drectly from within konqueror... oh boy if that isnt news to get excited than I dont know what ;-)\n\nBest regards\n\nNils Valentin\nTokyo/Japan\nnils(at)knowd.co.jp\n"
    author: "Nils Valentin"
  - subject: "Microsoft Mail"
    date: 2003-06-27
    body: "I work in a small business environment (5-10 workstations) on a mixed W98/2000 environment. For our internal needs the Microsoft postoffice, free in W95 absent but instalable in W2000 (best using the files form W95) is more than sufficient for all outlook networking (internal mail, calender, tasks). Will this be supported in any way? \n\nIt is more a file-organisation than a server. \n\nIts no fuss, low maintanance. \n\nGood work PIM. Without this functionnallity there is no chance for a linux desktop in our business. "
    author: "Geert"
  - subject: "Re: Microsoft Mail"
    date: 2003-06-28
    body: "\"outlook networking\"? Do you mean you use an Exchange server, or how is your network set up? In case you have an Exchange server enable WebDAV (if it isn't already), then you'll be able to access it with every KDE app using KDE's kio slave \"webdav:\"."
    author: "Datschge"
  - subject: "would it kill you to use Evolution"
    date: 2003-06-27
    body: "KDE has great integration, fantastic actually, but I can't get any work done in it because everything is slightly broken, including Kmail. Function is foremost--I don't need a fancy KDE file-picker that abstracts the file system in a windows-like way (my least favorite part of KDE), my only concession is to sym-link \"desktop\" to \".gnome-desktop\". That's why I switched to gnome over a year ago. No, it's not as slick, but stuff actually [mostly] works as advertised, including Ximian Evolution.\n\nKDE reminds me of those tricked out honda civics the kids drive around here--too much fluff--does an economy car really need 18\" rims....\n\nEvolution is here and it works beautifully right now, the gnome2 version just came out and it's speedy and gorgeous. I applaud the work of the KDE developers, but I simply don't understand the KDE-centric end users bemoaning the lack of a PIM. Don't cry for shortage of trunk space when you've bought the Miata.\n\nI use a few KDE apps that are superior, including k3b (the slickest cd burning tool out there), and don't feel like they spoil my pristine gnome desktop.\n\nA better solution is to integrate Evolution into the KDE desktop better. qt and gtk can get along just fine.\n\nI'm looking forward to seeing Kontact become stable and wonderful and huggable and cute. Just don't tell me that it's going to change the viability of the popular acceptance of the linux desktop.\n"
    author: "Max Otterland"
  - subject: "Re: would it kill you to use Evolution"
    date: 2003-06-28
    body: "I don't know if you are aware of that, but you are completely missing the point. Kontact hasn't been started just for adding yet another Outlook clone PIM but first and foremost as a feature complete KDE PIM client for the Kloab server infrastructure (http://kolab.kde.org/), offering an open source Kolab/Kontact counterpart for the commercial Exchange/Outlook combo. Evolution, Mozilla Mail and all the other mail/PIM applications are open and very welcomed to integrate support for accessing the Kolab server, but so far there are only the Kolab client and soon Kontact available as clients."
    author: "Datschge"
  - subject: "what's your problem?"
    date: 2003-06-28
    body: "Why do you want to deny others the right to use and enjoy Kontact?   How is that your problem?  Let the KDE folks develop the apps they want, who are you to decide what they do?\n\nIf you would have read the article anyway, you would have seen that the author was *already* using Evolution, but was excited at what Kontact could offer."
    author: "anon"
  - subject: "Re: would it kill you to use Evolution"
    date: 2003-06-28
    body: "Evolution major downside is that it certainly isn't a \"end-to-end\" solution. It's certainly a good client, but will not ever be accepted to the commercial world because it doesn't come with a good server. Kontact and it's kollab subproject is a much more complete solution that actually has a chance to be accepted to the mainstream. "
    author: "mif"
  - subject: "Re: would it kill you to use Evolution"
    date: 2003-06-28
    body: "> I don't need a fancy KDE file-picker that abstracts the file system in a windows-like way (my least favorite part of KDE)\n\nIndeed, this is basically the most hated parts of gtk happens to be it's archaic file selector. KDE's is much, much, much better. It helps me get things done in a much more productive manner. \n\n> would it kill you to use Evolution\n\nUsing this logic, perhaps GNOME (and subsequent development of 90% of GNOME's apps) shouldn't have been created in the first place. The individual parts of kontact are mature and very well tested. kmail has been around since 1997. korganizer has been around since 1997. knotes has been around since 1998. \n\nkparts is a wonderfully mature framework that makes it easy to wrap these things into one app. Why not provide existing kmail/korganizer/knotes users with more features, such as integration that kontact provides?\n\nThe fact is that neither GNOME nor KDE are going away anytime soon. Perhaps both desktops cater to different groups of users these days. Not sure. I loved GNOME 1.2 and 1.4, but I find GNOME 2.x downright horribly *annoying* to use. I didn't  like KDE 2.x as much as KDE 3.x either."
    author: "lit"
  - subject: "Re: would it kill you to use Evolution"
    date: 2003-07-02
    body: "Hi Max (and others ;-)\n\nI think Evolution is \n\na) to heavy\nb) doesnt have good import filters\nc) reminds me to much of Outlook\nd) mostly commercial\n\nwhich made me think twice. The best I found was kmail. Plus it fully integrates into KDE. Make a \"top\" and compare the resources Evolution is requiring and kmail is using. Evolution didnt add any import filters during the last 2 years (not a single one !!). They just update the design and bugfixes. While at this time Evolution was grafically the most advanced, it didnt develop much further from my point of view. Kmail (as well as the whole KDE) was constantly improved and new features added. I think that definitely kmail (and KDE) is the choice to make. \n\nf.e Which Desktop environment had first the feature to start a new X-window session from within an X-windows session ? ( the answer is KDE)\n\nWhy does RedHat 9 come with a installed konqueror on a gnome desktop ? \n(its a KDE application) - because it rocks !!\n\nIf you are discussing that you dont need the PIM than dont use it - as simple as that ;-)\nI believe KDE (including kmail) is the best Desktop Environment around for a normal desktop users.\n\nThe whole KDE project made a big step forward from KDE 3.1.\nNothing which I would be able to mention about the Gnome desktop that would be even close to this.\n\nThats my $0.02. You might want to flame me for this (its just my personal opinion).\n\nBest regards\n\nNils Valentin\nTokyo/Japan\n\n"
    author: "Nils Valentin"
  - subject: "Re: would it kill you to use Evolution"
    date: 2003-10-12
    body: "He he, I wrote this slightly as an agitator, and because I'd been using the rc2 of drake 9.1 where kde was a mess. Since then I've upgraded to the latest KDE from texstar and am using almost exclusively KDE apps INCLUDING Kroupware, which I love!. I do miss the html composition component in Evolution and the little cute weather/headline page, but Kroupware is simply more elegant and friendly.\n\nSince my last response, I've also discovered IRC and use it with Kopete. I've found that the KDE folks are just the nicest about and super helpful! That's how I discovered Kroupware. I logged on the the #kmail channel and asked how I could get support for offline IMAP, and a friendly developer hooked me up.\n\nPlease forgive me! :P"
    author: "Max Otterland"
  - subject: "HTML WYSIWYG Editor"
    date: 2003-06-28
    body: "What KMail really needs is an HTML WYSIWYG editor.  It is extremely useful to format email with bold and italic text.  It is handy to use blockquotes, lists, and tables.  These tools help people communicate more effectively."
    author: "Burrhus"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-28
    body: "Hopefully Quanta's future HTML WYSIWYG editor can be embedded as a kpart, eventually ;-)\n\nhttp://mail.kde.org/pipermail/quanta/2003-March/000187.html"
    author: "cies"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-29
    body: "Now I understand why KDE development is so inefficient. It's because we don't write HTML messages. <b>Thanks for the tip!</b>\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-29
    body: "I agree very much with this. I regularely send a photo news letter to my students and it looks very nice in Outlook/Outlook express where the photo's show in the text in the right place. As attachenments the whole newsletter loses it's charm.\n\nSo please allow HTML or if security is a problem a restricted HTML allowing Tabels, photo's, fonts etc.\n\nDada K."
    author: "Dada"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-29
    body: "No way.\n\nHTML and e-mail just don't mix. Kmail doesn't need a WYSIWYG editor at all. Mail is plain text, and doesn't need formatting."
    author: "Coolvibe"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-30
    body: "> HTML and e-mail just don't mix. \n\nHrmm.  A significant portion of the Windows/Mac/Webmail using-population (perhaps 95%+ of computer users) use HTML-based mail.\n\nHTML mail has a stigma with geeks and advanced computer users. This doesn't mean email and HTML don't mix however.\n\n(btw, I certainly do hate HTML mail, but this isn't a good enough reason to keep WSYIWYG editing out of kmail if anyone decided to do it..)\n\n> Mail is plain text,\n\nI think you're completely misunderstanding what the concept of MIME is."
    author: "fault"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-30
    body: "MIME is basically a hack to allow non-ASCII text and data in a media which is limited to transferring ASCII text."
    author: "Datschge"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-30
    body: "MIME is hardly a hack. It was created for email specfically (perhaps the \"hackiness\" of MIME comes when it's used in places other than mail) and is well documented. It has been a defacto standard for the last ten years-- a period of time where the internet has had explosive growth. MIME has indeed helped in this growth, as things like email would have been less flexible. ASCII-only email was only common place before MIME existed. Almost everyone who has used email has used the fruits of MIME, such as sending/receiving documents or images through attachments, or sending/receiving html mail."
    author: "fault"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-06-30
    body: "Emails as we still use them today were never intended to break the 7bit ASCII barrier. MIME as a rule set itself is nice and dandy, but using it as an excuse to blow up the size of the outdated email format whenever 8bit data occures is very cheap. A successor to the classic email format capable of containing 8bit data should have been introduced, the currently used solution is not good design, it *is* a hack."
    author: "Datschge"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-07-01
    body: "> Mail is plain text, and doesn't need formatting.\n\nAbsolutely. We're much better off with all these immensely convenient and fully standardized conventions like prefixing lines with '>' and thus screwing the word wrapping, putting words between '*' or '_', and other state of the art techniques. "
    author: "Guillaume Laurent"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-07-01
    body: "Well, IIRC, KMail has all sorts of fancy code to make sure the prefixing doesn't \nmess up the line wrapping :-)\n"
    author: "SadEagle"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-07-01
    body: "And this was obviously very interesting code to write, wasn't it ? Totally worth of all the time spent on it.\n\nBTW, in my experience, it's still very easy to mess up worp wrapping in KMail."
    author: "Guillaume Laurent"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-07-01
    body: "Well, I don't know how much fun kmail people had to that, but I brought this up\nexactly because I think this supports your point. Not sure that HTML e-mail makes\nit any better; but it's clearly the case that plaintext doesn't cary enough semantic\ninfo on quoting in nice form...\n"
    author: "SadEagle"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-07-01
    body: "I'd say that HTML kept within \"reasonable\" use is still better than what we use now, which is really, really pathetic, but we're so used to it we don't realize that anymore."
    author: "Guillaume Laurent"
  - subject: "Re: HTML WYSIWYG Editor"
    date: 2003-07-02
    body: "If you guys can get a thousand votes on Bug#4202, then within one month a patch that implements HTML WYSIWYG support in the KMail Composer will be sent to the KMail list. This patch will cover basic rich text styling like font face, font size, bold/italic/underline, and color.\n\nLooking at kontact.org/votes I see there's already 844 votes, up from 400 last week which is really impressive, well done.\n\nI did say I'd only count votes from kde.org, but never mind that, as long as no one cheats (creates multiple voting accounts for one person) then I don't mind where the votes come from.\n\nDon Sanders.\n"
    author: "Don Sanders"
  - subject: "Mixed Feelings"
    date: 2003-06-28
    body: "The integration of KMail, KAddressbook, KOrganizer and KNotes into one application,Kontact, has spiralled mixed feelings in me. On one hand, I commend the developers of this immense project for providing an application that will, undoubtedly, serve thousands of users; on the other hand, I cringe at how much of a resource hug Kontact will be on a frugal system. All things being equal, the larger your program is, the more system resources it consumes(in particular RAM), the more sluggish it becomes, and the more irresponsible and selfish it is to other applications and tools seeking the systems attention.\n\nMicrosoft Outlook, one of Kontacts competitors, is a clasic example of the message I'm trying to convey. I'd be disgusted if Kontact becomes a Microsoft Outlook clone. It is reduntantly bloated, ridiculously slow, irresponsible with respect to system resources and unpredictable courtesy of countless bugs. I hope a lot of fundamental planning, designing and thought has gone into this integration. I hope the codes are streamlined and efficient. And I hope to God, the developers were not inspired by Microsoft Outlook. No offense, but people should be imprisoned for writing programs like Microsoft Outlook. Yes, it sitll gives me nightmares.\n\nI wish the developers the best and pray they make Kontack the ideal, model or standard, of how a groupware should be designed and implemented. And thank God for KDE's interprocess communication, via dcop, kparts and the likes, without it Kontact will indeed be a herculian undertaking I would shun a million miles away.\n\nRegards,\n\nMystilleef "
    author: "Mystilleef"
  - subject: "Re: Mixed Feelings"
    date: 2003-06-28
    body: "> The integration of KMail, KAddressbook, KOrganizer and KNotes into one\n> application,Kontact, has spiralled mixed feelings in me.\n\nWe're sorry. Hope you unwind again soon. ;-)\n\nMarc\n(Sorry, couldn't resist)"
    author: "Marc Mutz"
  - subject: "Re: Mixed Feelings"
    date: 2003-06-28
    body: "Perhaps, after using Kontact my feelings will untwine, for better or for worse. :lol:\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: Mixed Feelings"
    date: 2003-06-28
    body: "I guess your worries are unjustified in this case. Kontact is mostly a shell combining a couple of existing mature KDE apps and tools for offering an Outlook alike PIM experience. The resulting usage of system resources shouldn't be much more than the sum of system resources used by the single apps embedded in Kontact (in particular KMail, KAddressbook, KOrganizer and KNotes). In case you can't run those single apps all *at the same time* on your computer due to the lack of system resources you'll neither be able to run Kontact."
    author: "Datschge"
  - subject: "Re: Mixed Feelings"
    date: 2003-06-28
    body: "Hello Gents,\n\nI have never at any point in time had the need to run all four applications at the same time, which is the underlying logic behind my rant. The important question is have you? I'm positive a significant number of users don't. And even if they do, it is should be on rare occassions. It doesn't make sense to run four programs as one when you don't need all of them running at once. \n\nFurthermore, the issue is not whether or not my system can or cannot run KDE or any of its applications for that matter. My argument is that conglomerating a set of applications into one giant tool makes the application sluggish, larger and resource hungry. Basic coding dictates that small applications run faster and vice-versa. I acknowledge there other factors affecting the speed of a code.  But, I'd hate to see KDE evolve into a bloated piece of junk called Microsoft Windows. Ever wondered why Microsoft Windows is so sluggish compared to KDE/*nix?  \n\nI'd hate the see the developers make the same mistake the Microsoft Outlook developers did. For this reason I don't see how my worries are unjustified.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: Mixed Feelings"
    date: 2003-06-29
    body: "> I have never at any point in time had the need to run all four applications at the same time\n\nThis expands to: \"I have never really used a client-server groupware solution\" :) \n\nYou need mails to communicate, a calendar to manage meetings (which you negotiate via email using the groupware solution). You need to put a bunch contacts somewhere, of which you can select some and say \"I want to have a meeting with those guys\". The groupware solution should try to find an optimal timeframe (and this is really just one usecase).\n\nConclusion: The whole concept only makes sense if the parts art tight and seamlessly integrated. The art is to keep the components stand-alone on demand, while achieving this high amount of integration.\n\nThis is not an easy goal and it will take time to get it right, but we are on the track and I think it's the right one.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Mixed Feelings"
    date: 2003-06-29
    body: "Well, your worries are unjustified because you obviously have no interest in what Kontact offers. And Kontact won't replace KMail, KAddressbook, KOrganizer and KNotes but \"only\" rely on them for all features. If you don't want to use Kontact you are still free to use any of the four other apps, nothing changes in that regard. A high degree of shared code is what KDE excels at, code bloat always has been actively discouraged within this framework."
    author: "Datschge"
  - subject: "Re: Mixed Feelings"
    date: 2003-06-28
    body: "You'll be able to use the individual tools, of course. "
    author: "lit"
  - subject: "the screenshots"
    date: 2003-06-28
    body: "sigh, the screenshots should of been taken with AA turned on. Pretty lame."
    author: "anonymous"
  - subject: "Re: the screenshots"
    date: 2003-06-28
    body: "Why? I think most people who view these screenshots will be using CRT monitors. With the font sizes in use in the screenshot, it's a well proven fact that antialiasing will actually be detremental to readability of text on the eye. This is why, for example, Microsoft doesn't only antialiases larger sized fonts and smaller sized fonts, unless ClearType is on (which is meant for LCD's).. Apple made virtually all font sized be antialiased in OSX because most of the hardware capable of running OSX will used some sort of LCD monitor (except for the eMac, of course)"
    author: "lit"
  - subject: "Views"
    date: 2003-06-29
    body: "Thanks for this article. I have lot a of different contacts who send me emails to which I reply. Now is it possible that in the KAddressbook component (or somwhere else in Kontact)I can get a list of mails that I received from (or sent to) a perticular email address, for example by right-clicking on an address? This would greatly ease the communication between many parties. This is a function I first saw in ACT2000 on Windows.\nAnybody knows whether this will be possible in Kontact?\n\nPat"
    author: "Pat"
  - subject: "Re: Views"
    date: 2003-06-29
    body: "There is no such entry in the rmb menu so far but technically this feature should be very easy to add and I agree it would be very useful, please open a wish report for it at bugs.kde.org so your suggestion won't get lost."
    author: "Datschge"
  - subject: "Re: Views"
    date: 2003-06-29
    body: "Done. Just supplied the wish report and it got filed as bug 60496. :-)\nPatrick"
    author: "Patrick"
  - subject: "Re: Views"
    date: 2003-07-01
    body: "Kmail for 3.2 has a search function that would create a folder, which would contain for as your example, all the emails from a certain person.\n\nDerek"
    author: "Derek Kite"
  - subject: "Well-written, but incomplete..."
    date: 2003-08-31
    body: "Says many good things about Kontact, which isn't finished yet... when there are already two FINISHED projects out there that do basically the same thing:\n\nFirst, there's Aethera, which is made by TheKompany... which is freely available, but TheKompany makes their money off MS Exchange plugins, etc.\n\nhttp://www.thekompany.com/projects/aethera/\n\nSecond, there's Kroupware, which is also complete. It does nearly 100% what Kontact is setting out to do -- integrate \"enhanced\" versions of Kmail, Korganizer, etc into an integrated Outlook-style interface.  And it's _finished_.  What's strange is the author mentioned Kolab (which is the optional server-side piece of the Kroupware umbrella of stuff), but didn't mention the Kroupware client????   How could you mention Kolab as a server-side component to Kontact, an unfinished client, without giving props to Kroupware?  Unbelievable.\n\nI suggest everyone look at this before holding your breath for Kontact.\n\nhttp://kolab.kroupware.org/\n\nThe glaring oversights here ruin an otherwise decent article."
    author: "Dylan Carlson"
---
After  her <a href="http://dot.kde.org/1055452455/">review of JuK</a>, Savanna is back with a unique perspective on the importance of <a href="http://kontact.kde.org/">Kontact</a> (<a href="http://wgess16.dyndns.org/~tobias/kde/kontact/index.html">preview</a>) to KDE's success.  Read on for an entertaining write up of this up and coming app.
<!--break-->
<p><hr>
<h2>The Killer Kontact</h2>
<i>by <a href="mailto:savanna@kdenews.org">Savanna</a></i>
<p>

"The Killer App" is a term that even a simple end-user like myself has
heard about. It usually means "success" for a platform and an
Operating System.

<p>

Success with KDE is a very important thing, as I see it. "Killer App"
for me conveys the idea that KDE has "made it" into the real world where
people can use it as a tool to actually facilitate every day functions.

<p>

One of the huge reasons I switched from Microsoft to Linux around a
year ago was because <a
href="http://www.microsoft.com/office/outlook/default.asp">Outlook</a>
was eating all of my mail. This would happen on average every three to
six months, and there was simply nothing that I could do about it. The
classic "format and reinstall" solution had become such a feared
process for me that I simply didn't want to have anything to do with
computers any longer.

<p>

Then, Linux came into my life. Linux, KDE, and <a
href="http://kmail.kde.org/">KMail</a> to run it on. Three things
which ultimately converted me from wanting to throw my computer off of
the Empire State Building in slow motion for a Dave Letterman video,
to never wanting to leave my computer because I actually use it
productively all the time without fear of ever losing my data again.

<p>

KMail is a beautiful email program. It was, however, very different
from Outlook. It wasn't groupware, and it only deals with email. If I
wanted my address list to come up in a more complete fashion than just
a click-list, I would have to open <a
href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a>. For
my calendar, <a href="http://korganizer.kde.org/">KOrganizer</a> would
do the trick. I need to jot down some notes? Well, naturally, one
turns to <a href="http://pim.kde.org/components/knotes.php">KNotes</a>.

<p>

As you can see, this is an entirely different philosophy from Outlook
which incorporates everything into one single program. I had to relearn
to use different applications for different things - a sort of Linux
purist philosophy which I became accustomed to and even fond of over
time.

<p>

The idea which I loved behind KMail and all the others is their apparent
integration into the desktop of KDE. This gave me the illusion that my
entire desktop was like a big Outlook program, but (naturally)
infinitely better. (And don't flame me for comparing KDE to Outlook -
I'm only using it as an example. I wouldn't wipe my feet on Outlook
anymore while KDE is, naturally, the holy of holies =) )

<p>

However, there were some things I did miss from Outlook. And since I
use two email accounts (one for business and one for personal) I would
separate them by using KMail for one and <a
href="http://www.ximian.com/products/evolution/">Evolution</a> for
another. This wasn't done because KMail couldn't handle the
functionality of having multiple accounts, but simply because I had
the option with Linux of doing so rather easily.

<p>

Now, as everyone knows, Evolution is very much like Outlook. In fact, it
is disturbingly like Outlook. But there was something which reminded me
of the things which I did admire about Outlook.

<p>

Outlook is one of Microsoft's "Killer Apps", and rightly so. It is
great, for instance, to be able to instantly click on an integrated
icon in a program and have a selection of all your email addresses
without having to pull up a separate program for it. I did miss that
feature.  And when somebody emails me an appointment, it was nice in
Outlook to simply to add it into the calendar with a single click,
rather than having to copy/paste the contents of the email from one
program into another, like I do with KMail and KOrganizer.

<p>

But now...now, I will have the option of doing both. And what's better
yet, I will have the option of doing both with my KDE PIM applications.

<p>

Enter: <a href="http://www.kontact.org/">Kontact</a>.

<p>

Kontact is a new program which is currently in development. Kontact is a
PIM integrator. Kontact gives you the options which I simply adore and,
when it is finished, will be another "Killer App" for KDE.

<p>

I pull up Kontact and I get four programs housed in a shell: KMail,
KOrganizer, KAddressBook, and KNotes -- the standard Outlook shell
(only much nicer because I use <a
href="http://www.mosfet.org/">Mosfet</a> <a
href="http://www.mosfet.org/liquid.html">Liquid</a> and Keramik with
<a href="http://www.kde-look.org/content/show.php?content=3883">Noia
Icons</a>, and nothing in Outlook looks this good).

<p>

On the left side, you have your basic icons to switch between functions,
and the right side contains the application you are currently focused
on. You also will have a summary/daily preview with weather and all the
lovely overviews you get in Evolution and Outlook which make sorting out
your day so much easier.

<p>

Now, as I said, it is in development and that means it isn't even <a
href="http://wgess16.dyndns.org/~tobias/kde/kontact/index.html">ready
for a preview</a> yet. Kontact is being updated every single day by a
dedicated team of developers which I had the good fortune of
interviewing online.

<p>

And while it isn't ready for a preview yet, I can see great things for
it, and KDE.

<p>

Kontact isn't Outlook, however. While interviewing the Developers for
Kontact, I was told that they will be using other PIM suites as a
model but that they expect to do many new things. So don't expect an
Outlook model -- and I for one am grateful for that. For instance,
KNotes should remain on your desktop whether or not Kontact is running
(Outlook cannot do this). Other features such as this will make it
much more powerful than the Microsoft equivalent.

<p>

Kontact is what KDE needs, and needs badly. An integrated PIM with all
functions talking to each other is an invaluable piece of software for
any desktop in a business environment at the very least. Most people
don't understand why it is a great idea to be able to run several
programs separately and they want every program to do everything for
them. I'm not that silly, but I do see their point of view: I want the
functionality of Outlook coupled with the power of KDE desktop and
separate applications.

<p>

The Kontact Devs tell me that it is being planned for release with <a
href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">KDE
3.2</a>.  For now, you can use Evolution if you really want that
Outlook functionality, or you can be like me and mostly use KMail and
your desktop as the real PIM. But I suggest you check out Kontact when
it does come out. It will be something important for the KDE and Linux
community as a whole. Even in development, it looks simple and
friendly to use.

<p>

But here is one of the most important aspects of Kontact: with the <A
href="http://kolab.kde.org/">Kolab</a> server application, Kontact
will be out there to rival and challenge the Outlook/Exchange server
model. This is a "Killer Business App" which KDE has been in need for
so long now. In fact, this is an incredibly big step for the KDE
community in the business world. With Kolab's client functionality
integrated into Kontact, businesses will have a perfect groupware
software package to link all of their computers together, and at a
dramatically reduced cost than buying licenses for every Microsoft
Outlook and Exchange server computer which they need to use. Now, they
will be able to get that functionality integrated and packaged in the
best of desktops: KDE with Kontact. Suddenly, KDE will go from being a
desktop user system to an integrated business groupware system as
well.

<p>

So give it a few months time and get ready for something new on the
horizon; something which other Outlook users will go "wow" at; something
which will put KDE on the map even more; and something which everyone
will be given the option to use in the best of the spirit of Linux and
KDE.

<p>

Watch out for Kontact.