---
title: "KDE-CVS-Digest for April 18, 2003"
date:    2003-04-19
authors:
  - "dkite"
slug:    kde-cvs-digest-april-18-2003
comments:
  - subject: "Where to mister?"
    date: 2003-04-18
    body: "I think you forgot the link to where the digest actually is... :)\n\nhttp://members.shaw.ca/dkite/apr182003.html"
    author: "Robert Schouwenburg"
  - subject: "Re: Where to mister?"
    date: 2003-04-18
    body: "Uh, weird.  My fault...."
    author: "Navindra Umanee"
  - subject: "oblig"
    date: 2003-04-18
    body: "thanks Derek!"
    author: "thr0d ps1t"
  - subject: "Keep up the good work"
    date: 2003-04-18
    body: "Thx derek keep up the good work!"
    author: "Willy"
  - subject: "Yup, Thanks"
    date: 2003-04-18
    body: "I really like the way KDE keeps its users up to date on the way development is going? I Didn't see any real UI changes though, wasn't 3.2 supposed to significantly clean up the UI?"
    author: "Mario"
  - subject: "Re: Yup, Thanks"
    date: 2003-04-18
    body: "I don't know what you mean by 'clean up the UI', but every week there are incremental fixes and improvements. \n\nMy daughter runs 3.1.1, and I was surprised how slow it seemed compared to the up to date from cvs that I run. \n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Yup, Thanks"
    date: 2003-04-19
    body: ">I Didn't see any real UI changes though,  wasn't 3.2 supposed to significantly clean up the UI?\n\nNot really \"clean up the UI\" but improving the usability (that's really not the same, mind you ;). The usability team is already for some time building up a database of usability concerns in the current KDE and suggestions how to solve them ideally. What's lacking so far is a front end for the usability.kde.org website so those suggestions are actually accessible for eveeryone.\n\n*considering to volunteer himself soon if nothing changes*"
    author: "Datschge"
  - subject: "Konq"
    date: 2003-04-18
    body: "> Safari fixes to v68 (current is v73) merged.\n\nWohoo!"
    author: "Eike Hein"
  - subject: "Re: Konq"
    date: 2003-04-19
    body: "Do we have any idea what things in particular are coming from Safari? I know there are meant to be a lot of Javascript/html engine improvements, and some page rendering speed improvements, which will be nice just to see things rendered a little better, but are there any other notable things? It'd be really nice to see a breakdown of the sort of things we're getting from Safari..."
    author: "Tom"
  - subject: "Re: Konq"
    date: 2003-04-19
    body: "Hi,\n\nI can really recommend reading David Hyatt's blog, available here: http://www.mozillazine.org/weblogs/hyatt/\n\nHe's talking quite a lot about improvements made to the KHTML rendering engine over at Apple, especially last month. Go through it. Very informative."
    author: "Eike Hein"
  - subject: "Re: Konq"
    date: 2003-04-19
    body: "In my mind it would be a very nice idea if some interested KDE developers wrote BLOGs and kde.org would offer a link page to all of them.\n\nThorsten"
    author: "Thorsten"
  - subject: "Re: Konq"
    date: 2003-04-19
    body: "Try http://www.advogato.org/proj/KDE/"
    author: "Anonymous"
  - subject: "Re: Konq"
    date: 2003-04-19
    body: "Look at the source code links in the digest.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Konq"
    date: 2003-04-19
    body: "CSS fixes are in table fixes still missing afaik... but CSS support on head rocks now!"
    author: "cartman"
  - subject: "Re: Konq -> VERY OFFTOPIC"
    date: 2003-04-19
    body: "www.tomshardware.com looks realy bad in konqueror (3.1.\"who knows\" from mandrake 9.1 updates) "
    author: "Andre"
  - subject: "Tom is right !!"
    date: 2003-04-20
    body: "\nThe Konqueror will be a good browser at the future, not today.\n\nMozilla is my default browser, sometimes I use Konqueror. I am shure, at a near future Konqueror will be an excellent browser, but not today.\n\nA boy, from a Lan House near my home, have a bad idea about Linux because at his school they use Linux with KDE by default. He think is a beautiful and stable software set, but he don't like the Konqueror. \n\nHe use M$ WinX with IE at his 40 machines at Lan House. This machines (Atlhon XP with GeForce and DDR 333 RAM) are instable and the littles 12/13 years old lammers/hackers sometimes detonate one.\n\nI told \"Install Linux at one machine\", but he remember the Konqueror and just say NO !!!\n\nI hope when the Safari upgrades return to KDE team, and a few more development, Konqueror will become the browser.\n\n"
    author: "Tom's friend"
  - subject: "Thanks a lot for keeping us informed"
    date: 2003-04-19
    body: "We appreciate it =)"
    author: "Mario"
  - subject: "Page colours"
    date: 2003-04-19
    body: "It looks like the KDE CVS digest page sets the background colour to white, but doesn't set the default text colour. So if your default browser colours are white text on a black background, you get white text on a white background, which is hard to read."
    author: "AC"
  - subject: "Re: Page colours"
    date: 2003-04-19
    body: "Derek, please use Konqueror > Tools > Validate Web Page > Validate CSS next time. ;)"
    author: "Datschge"
  - subject: "Re: Page colours"
    date: 2003-04-19
    body: "Try it now. I fixed the css. The foreground and background are now set.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Page colours"
    date: 2003-04-19
    body: "Thank you. :-)"
    author: "AC"
  - subject: "Konqueror tabs"
    date: 2003-04-19
    body: "Great to hear the tab code getting a lot of focus. Does anyone know if there is a plan to allow for the currently viewed tab to have bold text for the title to help make it stand out more? That would certainly help from a UI point of view...\n\nThanks for all the hard work--computing is fun with KDE!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-19
    body: "Personally, I'd love to have fix-width tabs spanning multiple rows a la Opera.   Currently, if you're on the right-most tab, and the first tab is out of view, and you click on a tab before the right-most tab,  then the tab you clicked on will become the right-most tab.  In order to get to the last tab, you'd have to click the '>' arrow a few times.  Can't the whole view just stay put??  I find it *really* annoying when I'm trying to jump back and forth between a few documents.  Granted, I could detach what I need into new windows, but then the screen gets cluttered.\n\nUsing Opera's tab model, all the tabs are kept in view.\n\nRoey\n\n-- I've decided that I procrastinate too much, but I'm going to change that in a week or so."
    author: "Roey Katz"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-20
    body: "I agree.  Something needs to be done about the way tabs behave.  The \"Click on a tab and have it move out from under you\" is quite annoying.  Phoenix (now Firebird) has fixed width tabs but only on one level.  If all the tabs can't fit in the area it starts shrinking them to all fit on one row.  Either with one row or multiple rows of tabs I think the \"<>\" buttons should go."
    author: "beergeek"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-20
    body: "I agree 100%. That is the only reason I switched to mozilla for browsing. I personally don't mind if I can't read the titles of the tabs if I have 20 tabs open at the same time. When I'm searching a tab, I just click a tab and usually know if the one I want is before or after it and then use the keyboard to find the one I'm looking for. But having to scroll through 20 tabs with the keyboard is a pain, and so is placing the mouse on that litte widget and clicking 15 times. \n\nBtw, is there an \"official\" statement about the \"middle-click pastes in url bar and try to browse to it, no matter what it is\" bug ? I have reported it ages ago, but nothing seems to happen, so I'm starting to wonder if that is considered to be a feature. It should be simple to disable that, but I'm not familiar with kde programming, so I can't do it myself.\n\nOther than that, great work KDE team, you've made my desktop rock."
    author: "Marko"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-20
    body: "i agree, the < > tabs thingie sucks. It should behave like mozilla or opera tabs."
    author: "peter"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-20
    body: "What \"middle-click pastes in url bar and try to browse to it, no matter what it is\" bug are you referring to? It works fine here and I'm using it all the time. Selecting URL and surfing to them by mmb clicking as well as selecting text and searching for it by mmb clicking rocks. =)"
    author: "Datschge"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-21
    body: "I think he's describing the behavior that when you middle-click in the browser window, any selected text gets pasted to the URL location widget.  IMHO, this should only happen if the URL widget has input focus.  If I middle click in the browser window, nothing should happen.  This happens to me pretty frequently, because I use my mouse's scroll wheel to scroll webpages, and it's a bit sticky, so I often click the MMB accidentally..."
    author: "LMCBoy"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-21
    body: "Well, as I already wrote above I actually like this feature a lot and don't want to miss it anymore. And \"because I use my mouse's scroll wheel to scroll webpages, and it's a bit sticky, so I often click the MMB accidentally\" sounds like a hardware related problem. \n\nBut I agree that MMB paste should only work in widgets which already have the input focus (while retaining the feature being able to scroll anything with the wheel without having to have the focus first). Removing the whole feature altogether (whether making it optional or not) would imo be a heavy cripple though."
    author: "Datschge"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-21
    body: "Don't get me wrong, I love MMB to paste URLs, but I just think it should only happen if the URL widget has input focus, not when the MMB click happens in the webpage.  "
    author: "LMCBoy"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-21
    body: "\"but I just think it should only happen if the URL widget has input focus, not when the MMB click happens in the webpage.\"\n\nThis sounds like a completely nonsensical solution to me. I mean mmb click into the location text field should paste the clipboard text into that field, not load it. And making mmb url paste in webpages work only when the location text field got the focus is yet another way to effectively irritate most users who won't understand why this feature works in one and doesn't in another case. They won't understand that simply since there is no logical or visible link why it should behave that way."
    author: "Datschge"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-22
    body: "I would love to be able to disable the mmb paste behavior.  When I have a url in the \npaste buffer and I want to drop it into the location bar, I just hit the little x next to\nlocation to clear out the bar, paste it into the (now empty) location area,  and click the \ngo button to the right of the location bar... very simple and no keyboard needed!  I am \nconstantly missing the clear-the-location-bar x button anytime i have to use a browser \nother than konq.\n\nBut I often accidentally tap my middle button while mouse wheel scrolling, and half\nthe time my browser ends up in google with some person text from an email \nin the search bar... I'm hoping some day I don't inadvertantly send them a recently\ncut'n'pasted password like that.\n"
    author: "sneakers"
  - subject: "Re: Konqueror tabs"
    date: 2003-04-22
    body: "Just curious (really curious tho): what mice are you all using where you can hit mmb so easily while \"wheeling\" around?"
    author: "Datschge"
  - subject: "Re: Konqueror tabs"
    date: 2004-01-21
    body: "it really annoys me when the wheel itself is depressed it acts as a middle button click, so wheeling it normally results in all sorts of misbehaviour (commands pasted into shell windows, confusing browsing in wrong direction, etc).\n\nHow can i turn it off!!!  please!!!!"
    author: "i'm using a microsoft mouse"
  - subject: "Re: Konqueror tabs"
    date: 2004-01-22
    body: "Here's another vote for being able to toggle this misfeature.  There are a number of ways in which it could be improved that would make it less annoying.\n\nFor one, if it would recognise that the thing to be pasted was actually a URL and not do anything if it isn't, that'd be great.  Pasting only into the location bar would be a vast improvement, but just being able to turn off the \"browse to whatever junk gets accidentally pasted into a konquerer window\" would be the absolute best."
    author: "Sean Russell"
  - subject: "Re: Konqueror tabs"
    date: 2004-01-22
    body: "> For one, if it would recognise that the thing to be pasted was actually a URL and not do anything if it isn't, that'd be great. \n\ndone in 3.2. also it asks the first time and lets you turn it off."
    author: "anon"
  - subject: "Re: Konqueror tabs"
    date: 2004-06-20
    body: ">done in 3.2. also it asks the first time and lets you turn it off.\n\nHOW do you (permanently) turn it off? \nI've managed to kill it in mozilla and firefox using the about:config page (middlemouse.contentLoadURL: false) but konqueror pops up a message \"Do you really want to search the internet for...\" where I have to hit cancel. \n\nI've read some people like this feature, but my mouse-wheel sometimes clicks a middle-click while I'm scrolling up, harmless in most apps but abruptly sending me to some random page in konqueror. I really want to disable this feature. \n\n(Rather than this middle-click, I'd prefer a context-menu over selected-text to search or paste it in the Location bar.)"
    author: "Jerry"
  - subject: "Was: Konqueror tabs - Middle mouse button disable!"
    date: 2006-09-21
    body: "Help!  How on earth can I disable the middle mouse button for every window?\nTHere seems to be an swer in here but I haven't seen it.\n"
    author: "Steve Shaffer"
  - subject: "Re: Was: Konqueror tabs - Middle mouse button disable!"
    date: 2008-04-29
    body: "I hope Steve already found a solution, hehe, since this is more than a year and a half old comment, but for those like me who stumble upon this discussion trying to turn this thing off, here's how you do it:\n\nSettings > Configure Konqueror > Web Behaviour and then under \"Mouse Behaviour\" unselect \"Middle click opens URL in selection\"\n\nThis behaviour completely turned me off Konqueror before because I assumed it's a feature rather than a bug..."
    author: "Dom Delimar"
  - subject: "Re: Konqueror tabs"
    date: 2004-08-17
    body: "If you normally use the mmb to scroll (with the directional arrows that appear upon mmb clicking), this is a let-down.  You have to spend an extra .5 sec to find a place to click (mmb) that wont load anything, and then find a place again when you want to return to normal scrolling (no directional arrows that appear upon mmb clicking).  Is there an answer to the original question?  Of course some peeps like it and some don't, that wasnt the question though.  I, too, would like to know if it can be completely disabled."
    author: "Ryan"
  - subject: "Re: Konqueror tabs"
    date: 2005-12-08
    body: "This is definitly a feature not a bug.  I often find my self browsing the web way too seriously, and only looking for info I want.  Accidental middle clicks (generally from sloppyness when trying to hover over an imput form box) combined with the fact reglar text becomes google \"I'm feeling lucky\" searches have to the strangest sites.  It helps remind me that the web is more than email, news blogging, computer help fourms and academic journals (all I generally use it for).   Although, on a couple rare occasions, I've acidently stumbled on to the fix or journal article I needed....\n\n-Jason"
    author: "Jason"
  - subject: "Middle click URL"
    date: 2003-04-21
    body: "What are you talking about?! I love the feature! \nsometimes  URLS are not hyperlinked and it is really painful to copy, clear out the current location, paste the new one and then hit enter... \n\tNow, All I have to do is highlight the url and middle click for it to be loaded!"
    author: "AC"
  - subject: "Re: Konqueror tabs"
    date: 2003-05-23
    body: "The > issue in konqueror is very frustrating.  I too would like to see resizing somehow enabled.  In addition to this it would be really nice to be able to open all the bookmarks in a particular bookmark folder as a group of tabs.  Phoenix can do this nicely.  I believe Opera can as well, but I'm not sure."
    author: "Adam York"
  - subject: "DockBar improvements"
    date: 2003-04-22
    body: "A lot of bugs have been fixed for DockBar (the kicker extension). If you are looking for another way to make kde look like WindowMaker, have a look at http://www.avenheim.online.fr/deskbox\nNote that the goal is not only to make it possible to use dock apps, but to display desktop icons as boxes (but it is also possible to use WindowMaker dock apps...)."
    author: "Benoit W."
  - subject: "kwin docking"
    date: 2003-04-23
    body: "This means I can choose to put a window in dock (with licq, lmule, kget, etc) instead of taskbar of I'm getting it wrong?\n(long file ago I sent a bug asking for this in bugs.kde.org, nothing until now...)"
    author: "Iuri Fiedoruk"
---
This week's <A href="http://members.shaw.ca/dkite/apr182003.html
 ">KDE-CVS-Digest is out</a>.  We can now do bash scripting in <a href="http://www.kdevelop.org/">KDevelop</a>. 
KGhostview gets command line switches and 
some bug fixes. The infamous <a href="http://www.konqueror.org/">Konqueror</a> tab delay has been fixed again, while <a href="http://www.apple.com/safari">Safari</a> 
fixes up to v68 (current is v73) have been merged.  Kicker docking and KWin crash bugs have been fixed.  And much more...  Enjoy!
<!--break-->
