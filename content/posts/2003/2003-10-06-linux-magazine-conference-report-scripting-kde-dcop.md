---
title: "Linux Magazine: Conference Report + Scripting KDE with DCOP"
date:    2003-10-06
authors:
  - "binner"
slug:    linux-magazine-conference-report-scripting-kde-dcop
comments:
  - subject: "That's all right, but..."
    date: 2003-10-06
    body: "...what's the picture of the guy with the weird shirt and the iron about????"
    author: "Roberto Alsina"
  - subject: "Re: That's all right, but..."
    date: 2003-10-06
    body: "I guess my \"pale geek with long hair\" look wasn't quite exciting enough to make it.  I would have even been willing to hold an appliance on request though as I'm currently inbetween (cough) irons it would have had to have been a coffee grinder or something.  :-)"
    author: "Scott Wheeler"
  - subject: "Re: That's all right, but..."
    date: 2003-10-06
    body: "I understand your position, since I have not owned an iron in... well, I have never owned an iron. I suggest in your curriculum you include a copy of this article and say it came from \"irons monthly\".\n"
    author: "Roberto Alsina"
  - subject: "Re: That's all right, but..."
    date: 2003-10-06
    body: "But what do you do when your ties get crumpled? ;)"
    author: "Navindra Umanee"
  - subject: "Re: That's all right, but..."
    date: 2003-10-06
    body: "If I gave away my sister at her wedding without a tie, I think I can get through life without ties :-)\n\nHere's some ideas:\n\na) T-shirts, Mao shirts, polo shirts, other shirts\nb) neck braces\nc) Borrow a tie\ne) buy a new tie if I really really really need one\n\nI've never tried b) yet, and only got to e) once.\n\nThen I had to beg someone to tie my tie for me, because I have forgotten how it's done :-) But that was like two years ago.\n"
    author: "Roberto Alsina"
  - subject: "Re: That's all right, but..."
    date: 2003-10-06
    body: "Yep.  I had this crappy job when I was very young where ties were compulsory, but otherwise I'm as anti-tie as they get and I've since never worn a tie for any other social or work reasons... \n\n> Then I had to beg someone to tie my tie for me, because I have forgotten how it's done\n\nDo you know of any ties that don't require tying?  Like, elastic or buttoned or something?  That could come in useful for emergencies.\n\nBtw, I don't have an iron either... :)"
    author: "Navindra Umanee"
  - subject: "Re: That's all right, but..."
    date: 2003-10-07
    body: "He is cute, half Matrix look, and that jeopard couch must be confortable per fare l'amore."
    author: "Gigi, il gaio"
  - subject: "Re: That's all right, but..."
    date: 2003-10-08
    body: "Actually, change him out for konqi. \nMight make for an interesting pix."
    author: "a.c."
  - subject: "The DCOP aritlce rocks!"
    date: 2003-10-07
    body: "I learned a lot ;)"
    author: "Alex"
  - subject: "Re: The DCOP aritlce rocks!"
    date: 2003-10-07
    body: "DCOP is not *half* as famous as it should be.  People talk all the time about Bonobo and never use it.  People never talk about DCOP but DCOP is used to do *magic*.  Magic, I tells ya."
    author: "ac"
  - subject: "Re: The DCOP aritlce rocks!"
    date: 2003-10-07
    body: "bonobo is not the same thing as dcop anyways. it's more like gnome's equivalent of kparts. gnome uses CORBA/ORBiT instead of dcop. there is no equiv of dcop's CLI or GUI frontends however. "
    author: "anon"
  - subject: "Re: The DCOP aritlce rocks!"
    date: 2003-10-07
    body: "Yes, a famous saying goes like \"Any sufficiently advanced technology is indistinguishable from a really cool DCOP script.\""
    author: "Haakon Nilsen"
  - subject: "Re: The DCOP aritlce rocks!"
    date: 2003-10-07
    body: "Does anyone know how I can get the window that is currently in focus?  I want to grab the web page that a person is viewing when they do.  Then look up the associated rdf file for the web page, and provide a list of the rdf links for that web page in a small app.  This is for people who find it hard to move their hands.  They could navigate a web page like slashdot by saying the title number (e.g.    1. 'title 1..'   2. 'title 2...'   and so on)\n\n"
    author: "JohnFlux"
  - subject: "Re: The DCOP aritlce rocks!"
    date: 2003-10-07
    body: "Well, here's a bash script to get the currently active Konqueror.  What you're talking about becomes non-trivial, but this might get you going.  Sorry for the lack of formatting -- the Dot's text input doesn't allow formatting characters (i.e. &nbsp;)\n\n== BEGIN SCRIPT==\n#!/bin/bash\n\nfor i in `dcop | grep konqueror` ; do\n\tfor j in `dcop $i | grep mainwindow` ; do\n\t\tif [ `dcop $i $j isActiveWindow` == \"true\" ] ; then\n\t\t\techo $i is the currently active Konqueror.\n\t\t\texit\n\t\tfi\n\tdone\ndone\necho There is no currently active Konqueror."
    author: "Scott Wheeler"
  - subject: "Re: The DCOP aritlce rocks!"
    date: 2003-10-07
    body: "Thanks, but my konqueror doesn't know the function isActiveWindow\n$ dcop konqueror-2885 konqueror-mainwindow#1 isActiveWindow\nno such function\nKde 3.1.3\n\nThis is what I get from \n$ dcop konqueror-2885 konqueror-mainwindow#1\n\nQCStringList interfaces()\nQCStringList functions()\nvoid openURL(QString url)\nint viewCount()\nint activeViewsCount()\nDCOPRef currentView()\nDCOPRef currentPart()\nDCOPRef action(QCString name)\nQCStringList actions()\nQMap<QCString,DCOPRef> actionMap()"
    author: "Dee"
  - subject: "Macro recorder"
    date: 2003-10-07
    body: "I think for newbies, it will be nice if all KDE applications has a macro recorder that can generate DCOP scripts.\n\nBut maybe it will be duplicated of what QSA can offer?"
    author: "tofu"
  - subject: "Re: Macro recorder"
    date: 2003-10-08
    body: "I have this horrifying vision of someone naming it kASS.\n\nThe Mac zealots in the audience will get the joke."
    author: "Shane Simmons"
  - subject: "put content of file into clipboard"
    date: 2003-10-07
    body: "dcop klipper klipper setClipboardContents \"`cat file_you_want_to_copy`\"\n\nnice article "
    author: "anonymous coward"
  - subject: "Re: put content of file into clipboard"
    date: 2003-10-07
    body: "Tried that, oh, say with a file with more than one line?  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: put content of file into clipboard"
    date: 2003-10-07
    body: "Hi,\n\nI use that regularily that way I believe at least :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: put content of file into clipboard"
    date: 2003-10-07
    body: "yes. tried it again with a file which has 213 lines. works fine"
    author: "anonymous coward"
  - subject: "Re: put content of file into clipboard"
    date: 2003-10-08
    body: "Ah, doesn't work with all shells (I use tcsh.), but it does seem to work with bash (after one test)."
    author: "Scott Wheeler"
  - subject: "Re: put content of file into clipboard"
    date: 2003-10-08
    body: "Hi,\n\nif it works with POSIX shells, it's good enough. The csh family is a bastard anyway ;-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "did anyone look at the doc summary of the pdf?"
    date: 2003-10-08
    body: "It really urks me that so many Linux related pdfs are written using non-Linux tools.  Are we production quality or not?\n\n"
    author: "Mike"
  - subject: "Re: did anyone look at the doc summary of the pdf?"
    date: 2003-10-08
    body: "Hi,\n\nwhen we stoped using Linux to proof it could be used, we showed it was ready for production.\n\nHonestly, why have to use Linux for relatively simple tasks like that?!\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: did anyone look at the doc summary of the pdf?"
    date: 2003-10-09
    body: "Are you saying that the only reason to use Linux is to \"prove\" something?  I'm sure you're not, but that's easy to read into your statement.  We use Linux because we prefer it over the alternatives for various reasons, and the reason some alternative was used for the PDF has probably got to do, as was suggested, with workflow processes or something other instead."
    author: "Haakon Nilsen"
  - subject: "Re: did anyone look at the doc summary of the pdf?"
    date: 2003-10-09
    body: "Hi,\n\nEnglish makes it hard to recognize conditional things some time. I of course meant \"when we would stop\" which can be expressed as \"when we stoped\". But I also wanted to express that we stoped actually already.\n\nNobody needs to use Linux over other platforms to prove it can do something the other can. I can. But we are still free to take work flow preferences.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: did anyone look at the doc summary of the pdf?"
    date: 2003-10-09
    body: "In this particular case, the answer is simple: workflow. The pdfs are generated out of the DTP program we use, and as for today there is no such thing as a production ready Linux DTP program suitable for magazines. We hope, Scribus <http://web2.altmuehlnet.de/fschmid/> will get there one day, but for the moment we have to stick to MacOS for this task. (FYI: This publisher is a MS free zone, everything apart from layout related tasks is done using Linux :)  "
    author: "Patricia Jung"
  - subject: "Re: did anyone look at the doc summary of the pdf?"
    date: 2003-10-09
    body: "For DTP, I guess I can understand it.  However, I see a lot of other Linux related pdfs that could have easily been written in OOo or LaTeX if the time had been put into it.\n\nHere we are telling the world how great our software is, and then we don't even use it.\n"
    author: "Mike"
  - subject: "Re: did anyone look at the doc summary of the pdf?"
    date: 2003-10-09
    body: ">>However, I see a lot of other Linux related pdfs that could have easily been written in OOo or LaTeX if the time had been put into it.<<\n\nThe problem is not that it is impossible. The problem is that it is too difficult and uncomfortable, especially LaTex. But also OOo is, while certainly not bad for its costs, much worse than MS Word. \nMany people use the tools that make them most productive and do not use free software just for the sake of it.\n "
    author: "Jeff Johnson"
  - subject: "OpenOffice.org?"
    date: 2003-10-09
    body: "I can't see your case for MS Word being better then OOWriter. For almost all uses many people comfortable with both programs prefer the latter. Only people who don't really know and use OOwriter, such as you, would claim that it is \"much worse\".\n\nAs we are talking about PDF creation: This is impossible to do using MSWord. Microsoft software can't do it. (I know distiller and Freepdf, but those are not part of MSWord, you might as well use CrossOver and kprinter....) \nIn OpenOffice it is one click!"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: did anyone look at the doc summary of the pdf?"
    date: 2003-10-09
    body: "Sorry for telling the truth, but we're definitely not. 'Linux' stuff works, but it can not compete. At least if you compare the price of the commercial offerings."
    author: "Jeff Johnson"
  - subject: "A bit of documentation..."
    date: 2003-10-10
    body: "on DCOP calls woudn't hurt...\n\nFor example, ksmserver->ksmserver->logout(int, int, int) does not tell me what each int means... The first time I tried this it promptly rebooted the ltsp server, while there were people logged in...\n"
    author: "Vodka"
  - subject: "Re: A bit of documentation..."
    date: 2004-05-05
    body: "The first int is the parameter, which turns interactivity on or off. If it is 0, the ksmserver does not ask if logout command should be executed or not. If the first param is != 0 the ksmserver asks.\nThe second param is the logout command. 0 means 'logout' from currentkde session. 1 means 'reboot of system and 2 means shutdown of system.\nThe third param is still unknown for me. Maybe it is some kind of timer, but I am not sure. I will look through the sources (if I find them) and try to find out what this third param means.\nI hope that that was a little bit of help for you."
    author: "PinguinPfleger"
  - subject: "kdebindings for Mandrake 9.1?"
    date: 2003-10-10
    body: "Bit late posting, but does anyone know if there's a kdebindings binary rpm floating around for Mandrake 9.1, without forking out for Mandrake club? I realise KDE has nothing to do with building binary releases - just curious if anyone could help - Rpmfind doesn't seem to find anything."
    author: "Frustrated"
  - subject: "dcop and nmblookup?"
    date: 2003-10-13
    body: "I wonder if a perl script could be written to \"discover\" a local samba network and automatically create links in the kfmclient (kde file manager) for the shares?  Similar to the way the old Corel file manager \"self discovered\" and configured itself for windows networking.\n\nI really think that would be a good thing."
    author: "james"
  - subject: "Re: dcop and nmblookup?"
    date: 2003-11-10
    body: "What's wrong with lisa? Shares are automatically in the \"network\" tab, as long as your samba's working."
    author: "Rube"
---
In its <a href="http://www.linux-magazine.com/issue/36">November 2003 issue</a>, <a href="http://www.linux-magazine.com/">Linux Magazine</a> publishes several KDE related articles of which two are also available online in PDF format. The first, titled "<a href="http://www.linux-magazine.com/issue/36/KDE_Conference_Kastle.pdf">There's an aim called 3.2</a>" looks back on the <a href="http://events.kde.org/info/kastle/">KDE Contributor Conference</a> and asks again several developers what they really accomplished during the hack fest. In "<a href="http://www.linux-magazine.com/issue/36/KDE_Scripting_DCOP.pdf">Boost your efficiency</a>" Scott Wheeler describes how you can easily write scripts which use <a href="http://developer.kde.org/documentation/library/kdeqt/dcop.html">DCOP</a> to control your KDE desktop and help you save much work.
<!--break-->
