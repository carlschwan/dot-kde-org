---
title: "Want KDE at COMDEX?  Then Vote!"
date:    2003-10-19
authors:
  - "ee."
slug:    want-kde-comdex-then-vote
comments:
  - subject: "Voted!"
    date: 2003-10-19
    body: "I have'nt seen a \"results\" link.... So no possibility to see who's ahead so far..."
    author: "Thomas"
  - subject: "Re: Voted!"
    date: 2003-10-19
    body: "i think thats a good thing.. so on-one gets the funny idea to mess with it, like we once saw on looky where someone keept smashinsh Daniel's Liquid style.. (it was fixed later tho)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Voted!"
    date: 2003-10-19
    body: "Where is Daniel anyway, he said he would relase a new version of PixiePlus with many improvements months ago and than suddenly he disappeared.\n\nI hope he's just on a hiatus, we need him."
    author: "Patrick"
  - subject: "Re: Voted!"
    date: 2003-10-20
    body: "Who needs an unreliable diva?"
    author: "Anonymous"
  - subject: "Vote running until Oct 31"
    date: 2003-10-19
    body: "Why is an account allowed to vote once per day?"
    author: "Anonymous"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-19
    body: "I haven't been there, but I'm guessing it's to subject you to their online ads?"
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-19
    body: "They do run ads for their own books (thus no inbound revenue), but they are static as far as I can tell... they run the same ad for several days in a row.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-19
    body: "We definitely want KDE to be one of the winners in any case!"
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-19
    body: "We sure do! That would be SUPER to give everyone the impression that Linux desktops are ugly as hell and sub-win 95 standard.\n\nOn the other hand, if any of you KDE uber zealots voted for Gnome, then fingers crossed that the PREMIER *nix desktop will be represented."
    author: "Gil"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-19
    body: "I agree, I hope KDE will be presented too.\n\nBTW: KDE can look very good.\n\nJsut check out how customized this guy's KDE 3.1 is: http://kde-look.org/content/show.php?content=7397\n\nReally, with themes like Plastik and Keramik I much prefer KDE's look to GNOME.\n\nBut, your right, GNOME does have its better parts in look n feel, which I listed here: http://bugs.kde.org/show_bug.cgi?id=58944\n\nAnyway, you don't really want to know the facts, you just want to troll."
    author: "Alex"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "std disclaimer: please don't feed the troll\n\nthis guy probably surfed over from the gnome desktop news article about the same thing (http://gnomedesktop.org/article.php?sid=1405&mode=thread&order=0&thold=1)"
    author: "anon"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Did you notice that the GNOME desktop article *didn't* encourage people to vote repeatedly day after day."
    author: "anon2"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "So? The poll says you can do this. \n\n"
    author: "anon"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Legalising the common KDE crowd's behaviour of vote stuffing any poll?\n\nNo, it's not something to encourage... I don't care if the site says it's ok.\n"
    author: "anon2"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Did you notice that GNOME posted the COMDEX article first?"
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Who is complaing about *anyone* posting it? Good grief... you really are a bunch of thickos on this site aren't you?\n\nIt's the blatant call for script-kiddies and zealots to ballot stuff that is typically dishonest KDE behaviour.\n"
    author: "anon2"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "\"Please vote for no more than three projects. In the interest of fairness you may only vote once per day. To keep track we ask that you please log in to your O'Reilly Network account.\"\n\nThis is O'Reilly policy, you Gnomedope.  Go bitch to Tim.  If you don't like the poll or OReilly, don't vote.  It's that easy.\n\nAnd what about the headstart that GNOME got by posting the poll first?  THAT'S CHEATING!  *eyeroll*"
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "HEADSTART? Are you insane? As I've already pointed out, the gnome-desktop story did not ask people to repeatedly vote as many times as possible. You, apparently, seem unable to see this.\n\nI don't care what O'Reilly policy is... it's the repeated calls for ballot-stuffing by dot.kde.org that's at issue -- it's been going on for years. KDE apparently, can't rely on normal people to vote, you must call upon your fearsome clans of drooling fanatics to swing things in your favour.\n"
    author: "anon2"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "> it's the repeated calls for ballot-stuffing by dot.kde.org that's at issue -- it's been going on for years.\n\nAny example except of [the Linux Journal Reader's Choice 2003] where gnomedesktop.org's call had to be compensated?"
    author: "Anonymous"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Usually the pattern is that GNOME posts it first and then KDE posts it.  GNOME usually gets the headstart/stuffing/trolls on most polls."
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "And the GNOME policy for years has been to send trolls over to dot.kde.org.  It's because the GNOME project has been faltering and has nothing to show for itself but hoardes of paid trolls going around spreading anti-KDE FUD, lies, propaganda.\n\nAs I told you, if you don't care about O'Reilly, then don't support their poll and don't vote.   O'Reilly asks you to vote fair by voting once a day. Yet your easy-bake-oven head can't seem to grasp this. "
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "GNOME policy. ROFL. This little session is paying for my car tax. The fact is, every story on slashdot is overrun with KDE flamebots -- and the little demonstration earlier this year with KDE bluecurve was ample proof that KDE is staff and supported by moron flamers.\n\nYawn... let's repeat the O'Reilly thing again -- for the hard of thinking majority on dot.kde.org. Did you see GNOME desktop encouraging its users to vote early and often? No... it's something that happens on this fanatical stinkhole of a website a lot. Go get 'em boys... zealots ho!\n\n"
    author: "anon2"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Uh-huh.  Your presence here is proof of your lies.  Go to GNOMEDESKTOP.ORG and count the number of KDE trolls there.  Then come back here and count the number of GNOME trolls here.   Hooray, GNOME won another trophy in the number of trolls they have..."
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Another interesting thing I noticed.  Your IP is in the same range as an open proxy abused by our famous \"wiggle\" troll last year.  Perhaps a \"welcome back\" is due.  I guess you noticed we lifted the bans.  Time to reinstate it perhaps and make you the second ever ban in 6 months?  Maybe, maybe not.\n\nThat's how fanatical we can get when we come across trolls and abusers of our resources...  It's great isn't it.  Go Konqi!\n\n195.92.168.163-182: proxy used by wiggle troll 2002/08/17 (webcache*.cache.pol.co.uk) "
    author: "Navindra Umanee"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "I guess after <a href=\"http://gnomedesktop.org/comments.php?op=showreply&tid=18569&sid=1405&pid=18532&mode=thread&order=0&thold=-1#18569\">seeing this</a> [1], there shouldn't be any qualms left.  :-)\n<p>\n[1] Admitting (the obvious that) you are a troll/abuser of KDE Dot News on GnomeDesktop.org and identifying yourself by IP."
    author: "Navindra Umanee"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Hahaha.  You haven't seen the Sun Java GNOME Desktop, have you?  So much for the PREMIER UNIX desktop.  It might be the PREMIER UGLY desktop tho.  Hahaha, get it?"
    author: "ac"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Yeah, that theme is pretty damn ugly."
    author: "Patrick"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "Sheeeit.... that's almost KDE-level ugliness. If they work on it for a bit, they may even be able to match the depths of the KDE default theme (shudder).\n"
    author: "Franz"
  - subject: "Re: Vote running until Oct 31"
    date: 2003-10-20
    body: "For *real* ugly check out Red Hat bluecurve!  Man now that's butt ugliness from the Win95 days!"
    author: "ac"
  - subject: "fairplay"
    date: 2003-10-20
    body: "Do you really think that what you are suggesting is fair?\nJust listen to yourself \"You can vote once per day, so make it part of your daily routine!\", it's a shame for the Dot to approve and issue such article. And I feel like losing my time to read about such dirty tricks.\nI didn't voted not because I don't see my favorite OSS project FreeBSD or that didn't want helping KDE to get there, but I didn't because this stupid article that demonstrates your morality and ethics and destroys the image KDE is building seven years now.\nKDE will definitely be there because it deserves - you happy now??\nBut for future may be it is questionable will it be or not if trends of losing image and popularity because of bad public view like this article continue.\n\nIf there is admin here please delete or edit this shame from the dot.\n\nAnd finally I'd like to say thanks to the hardworking free developers of KDE who made KHTML and Safari possible."
    author: "Anton Velev"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "Apparently every single voter needs a O'Reilly Network account so cheating will need some ugly work I hope nobody is willing to do for this purpose (ie. I wouldn't really worry about that)."
    author: "Datschge"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "Anton,\n\tIf you had read the article, the author of the vote/poll basically recommends voting once a day, but no more. So no, this is not bad to encourage that ppl vote once a day. Nobody encourages others to vote more that what was requested. Personaly,I do not see how this destroy's the KDE image? In what way was it wrong to obey the rules of the road for this vote? \nAs to deserving to be there, that is probably not true. More often than not, deserving has nothing to do with anything. If so, then BSD would have been used more years ago. Apple would have replaced MS on the desktop years ago, and Linux would now be replacing both on the desktop. MS should have taught all that you do not get what you buy (or deserve)."
    author: "a.c."
  - subject: "Re: fairplay"
    date: 2003-10-21
    body: "\"Personaly,I do not see how this destroy's the KDE image?\"\n\nPosting such material on the #1 KDE public media (the Dot) is destroying the image (since it engourages the unfairplay). I checked already yesterday what does gnome says about it and they also encourage people to vote but does not publicly say that people should post multiple votes.\n"
    author: "Anton Velev"
  - subject: "Re: fairplay"
    date: 2003-10-23
    body: "It's not unfair play! Its the rules of the poll! If they didn't want people to vote more than once, they wouldn't let them! The rules are probably structured like that to make sure that a small but dedicated fan base can still have an impact."
    author: "Rayiner Hashem"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: ">  If there is admin here please delete or edit this shame from the dot.\n\nHmm.. it's been posted a lot of other places as well. I count gnomedesktop.org, dot.kde.org, the OOo mailing lists so far, perhaps more. "
    author: "anon"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "And gnomedesktop.org posted it first. Who wants to see there only Evolution, Gaim, Gimp, GNOME, GnuCash and OpenOffice.org?"
    author: "Anonymous"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "As the author of that phrase, I wrote it because the creator of the poll says you can vote once per day, so I'd imagine he's looking to factor that into the results of the poll.  Since he's tracking each vote by a user account, it's obviously intentional that he is letting people vote once a day.\n\nMy impression is that they are looking for the projects with the strongest base of users.  \"Strong\" base of users can be a small group of devoted people advocating their project or a large group of people that have mild feelings.  Remember - this is about advocacy, and they have already chosen 21 projects that they feel could be there.  By putting it to an online vote, they are even making the selection as open as possible - turning it over to the Open Source and Free Software community as best they can."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: fairplay"
    date: 2003-10-21
    body: "\"You can vote once per day, so make it part of your daily routine!\"\nagain - this sounds like a bad joke on the KDE's #1 public media which purpose is to help building the good image.\nIf you really wanted to suggest such cheat (or call it as you wish) you could add it as anonymous reader after you posted the article but it's really bad to feed such stuff on this board. After all don't forget that this is not the only site that publishes your material, it's also feeded through RSS to many other sites and news tickers as well.\nTalking about advocacy, this post will really play it's good role for helping KDE to get there but as basic trend of curving the public image of kde in this direction will really play as bad advocacy for a longer term."
    author: "Anton Velev"
  - subject: "Re: fairplay"
    date: 2003-10-21
    body: ":: If you really wanted to suggest such cheat (or call it as you wish) you could add it as anonymous reader\n\nAnton Velev:  I want you to reply to this one question:  Why is it bad when the sponsors of the poll suggest that people vote that way?  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "> KDE will definitely be there because it deserves - you happy now??\n\nYou made a donation enabling KDE to have a booth there without O'Reilly's help?"
    author: "Anonymous"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "Man you are such an idiot and liar.  Go back to GNOME and vote for all the unfair GNOME projects in the poll when there is only one KDE option to choose from then.\n\nJust in case you haven't understood:  YOU ARE COMPLETELY WRONG.  This is not cheating.  This is HOW THE POLL IS SUPPOSED TO BE."
    author: "ac"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "*Go back to GNOME and vote for all the unfair GNOME projects in the poll when there is only one KDE option to choose from then.\n\nThey were obviously shortlisted in terms of quality. The simple fact is, most KDE apps are eye-candy plus alpha-level features... with a version 2.1 label slapped on to make it look \"mature\".\n"
    author: "anon2"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "Oh right. This is why the butt-ugly XMMS filedialog is so /mature/ and has such a /usable/ non-useless-eyecandy GUI.  *laugh*  Wait, mature must also explain why Evolution is such a bloated and crashy pig? And also why the Gimp has such a stupid unusable GUI?  How about the *wonderful* integration of GNOME Office (TM)?  Man what a joke."
    author: "ac"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "XMMS isn't a GNOME project -- and it is currently being ported (independantly) to GTK2.\n\nEvolution crashy? Perhaps you have faulty RAM, or you messed up when you compiled it. I've been using Ximian builds of evolution since it was alpha (when it crashed a lot, ooh, 2.5 years ago I think), since then it hasn't crashed once... not once, and unlike you and the loudmouthed KDE zealots, I use my mail client in all kinds of different environments, not just simple POP3 to my ISP to pick up the electric-letters from my fellow l33t d00ds.\n\nGNOME office? Gnumeric is, bar none, the best spreadsheet on Unix. Let me know when Kspread can do more than add up a shopping list."
    author: "anon2"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "> Gnumeric is, bar none, the best spreadsheet on Unix.\n\nCrap! (had to say this!) You have never really tried to use it, do you? And if you've been using it, than for no more than adding up a shopping list. The truth is, that it's just not able to handle files larger than 100 cols x 1000 rows (even with 512mb of ram). OO.calc is crap too, but it's lightyears better than Gnumeric! Gnumeric and Kspread are both neither mature apps nor ready for real-world usage. Do'nt claim something else... it's just not true."
    author: "Thomas"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "Hey, the shopping list crack was mine. It seems KDE copying is not just confined to the software world. As for Gnumeric, I've been using it for analysing experimental data (along with a few other packages) for 6 months now. Best spreadsheet I've ever used -- and I've tried most. It even comes recommended for the quality of its numerical accuracy.\n"
    author: "anon2"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "> analysing experimental data\n\nfunny, that's what I tried to use gnumeric (v 1.2) for... so _who_ is copying here?.. Whatever...\n\"recommended for the quality of its numerical accuracy\" plz read that again and let's both have a good laugh... You're sure you know what you're talking about? Wake up, man! you're free to use OpenOffice. It's there.. it's usable, though a bit slow sometimes (OOs slowness brought me to the stupid idea of giving Gnumeric a try). OO may be slow,  but at least you get your work done... "
    author: "Thomas"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "*funny, that's what I tried to use gnumeric (v 1.2) for... so _who_ is copying here?\n\nAnd I'm supposed to know that how, exactly? I would have to know it to copy it... fool. \n\nAs for the numerical accuracy -- apparently, you don't understand even the basics of this. Many spreadsheets have numerical routines of shockingly bad quality outside of the level required for simple accounting (Excel is notorious for this, as a simple google will show you). Gnumeric has been praised and recommended for this. You sound like a 15 year old who thinks if you put in 2+2 and get out 4, it means that your spreadsheet is T0P KWALITEE.\n\nAs for the size issue: Gnueric supprots enormous sheets with ease. The standard build is compiled with options to limit it to the size allowed by Excel, for compatiblity reasons. \n\nYou, along with a great many KDE loons, seem to compile GNOME software yourself and don't have the first idea how to do it properly -- possibly this is deliberate, or you simply lie about your experiences. May I suggest making the effort to get a packaged version, and save yourself some grief.\n"
    author: "anon2"
  - subject: "Re: fairplay"
    date: 2003-10-20
    body: "And I'm supposed to know that how, exactly? I would have to know it to copy it... fool\n\nIf you don't get that sentence, simply don't feel in need to answer to it...\n\n> Excel is notorious for this\n\n... Yeah... Excel _was_ notorious for this. btw. Excel e.g. is a mature app, easy to use, eats large chunks of data and feels responsive even with very large files. Neither Gnumeric nor Kspread nor even OO.calc live up to the level of excel...\nAnd you're right I did not know that special compatibility-option for Gnumeric. But that's not changing anything in my opinion, that Gnumeric is overrated by you and the guy who told you to use Gnumeric for scientific work..."
    author: "Thomas"
  - subject: "Re: fairplay"
    date: 2003-10-21
    body: "> And you're right I did not know that special compatibility-option for Gnumeric. But that's not changing anything in my opinion, that Gnumeric is overrated by you and the guy who told you to use Gnumeric for scientific work...\n\nUh, well, if the guy worked with it half a year in production environment i.e. \"at work\" while you haven't passed the hurdle of compiling it right, well, who's better in the situation to make a reasonable point about Gnumeric's quality and accuracy?\nAnd yeah, you may be right Excel *is* a mature app, but it's worstcoded, bloated + speedhacked. And it's far to expensive anyways, just because they have bound mankind to the Office formats in time.\nIt's a simple thing, OSS development seems to me a bit faster at the moment then M$ dev, properly because the 5000 something coding guys @ M$ there constantly stumble over all that old ugly code.. So it'll all just outdevelop M$. They are struggling with Longhorn now (didn't get that beta with that neat 3D stuff in there ready in time to dev.conf., HAHA) and I don't want to ever. \nAnd the good thing really is to know, ultimately, M$ cannot win.\nregards"
    author: "Marcel Partap"
  - subject: "Re: fairplay"
    date: 2003-10-21
    body: "The option I did not know of limits the number of columns to that of older excel versions (which was the size of a word if i recall correctly). So how does this effect my judgement of Gnumeric (provided that I had to deal with ASCII-tables up to 20000 rows)? Not at all...\nAnd plz come down that high horse regarding Microsoft software... Guess we can all agree on MS software not giving us the freedom we'd like to have... But the worst thing you can do is to underestimate Microsofts capabilities. Ms can't be doing everything wrong... \n\nfrom my experience it's like\n\n       excel > oo.calc > > > gnumeric > kspread\n\nwith a _large_ gap between oo.calc and gnumeric\n\nregards"
    author: "Thomas"
  - subject: "Re: fairplay"
    date: 2003-10-23
    body: "That option was not quite the point, you said you had to deal with one table and that didn't work out for you and that was it with your Gnumeric experience. But that other guy has used it a lot, so he is in a better position to judge. That was my point. If this came out of a misperception, well, correct mine.\nAnd, ok, M$ cannot do _everything_ wrong , you're right on that. If you look at it from an economical and marketing point they even have done especially well competing on the market, crushing most of their opponents to dust or pushing them into niches. But if you look at their Code (which I have never done except for DOS 6.22 and that's not quite comparable to modern C/++) it should be real ugly. I mean, it feels that way using the software. You can _feel_ it is not done properly coded. Regarding software designing/engineering they really failed big time. No good structuring, if at all. No wonder they have so many leaks and so much trouble getting Longhorn to run with all that old code in there. I would consider f.e. KDE's design process to be much more efficient then M$'s (imho). The \"less trouble thingie\" is quite a good indicator of that.\nIt's time for M$ to take a dive, they've had their time. They've made computers \"more popular and easier to use\", although I guess f.e. Apple would have done better at that. They should just go and die, and if they do not get a grip on other markets fast enough (which I fear they might have achieved), they sure will. Better for mankind in mine opinion. Remember the only thing why M$ is doing OS is your money. They wanna pluck you. Well, in OSS the reason to code an OS is to make a working OS, which seems to result in less marketing and more developing.\nI think Bill Gates always wanted to get rich. Little nerd as he was, red-haired, bullied in school, wanted to make everybody use his OS on his desktop - and earn him quite a buck.\noh btw I have used all the apps but not extensively so I myself can't really say anything bout that, will stop chitchating now, smoke a bowl and go to bed.\nregards, Marcel"
    author: "Marcel Partap"
  - subject: "Re: fairplay"
    date: 2003-11-10
    body: "Guys, you're funny as hell. Here, take xl, scalc, gnumeric and whatnot, fill a whole list with =rand() and check how long and how much memory it takes. Save and check file size. Post your results here."
    author: "Templar"
  - subject: "Damned..."
    date: 2003-10-20
    body: "> They were obviously shortlisted in terms of quality. The simple fact is, most KDE apps are\n> eye-candy plus alpha-level features... with a version 2.1 label slapped on to make it look \n> \"mature\".\n\nOh, damned... you got us. So we can't fool you...\nWell, than you are of course free to go back and use the better Gnome-only apps. \n\nAt least the illusion is nearly perfect isn't it....? Let's see what happens when people realize that all CDs they believe to have burned with K3B are actually still blank (but the \"write to CD\"-progress bar is really nifty, isn't it)?\n\nOr when they find out that Konqueror is not displaying websites but random .png images from the local fs (which the so-called khtml-developers made previously from screenshots of glory mozilla)\n\nBut the best trick still is kmail... You barely notice that it actually can't retrieve any mails but presents you snippets from it's large random-text database... And now they made a PIM-application... I can tell you, kontact is even trickier.."
    author: "Thomas"
  - subject: "Re: Damned..."
    date: 2003-10-20
    body: "*ROTFLOL* :-D"
    author: "ac"
  - subject: "Illusions ..."
    date: 2003-10-20
    body: "Of course KDE's applications are pretty lame. Still KDE's technical foundation is highly sophisticated.\nTake KDevelop e.g.: The user doesn't notice that internally KDevelop adds five years to the system date, and connects to the respective future Anjuta-version using a highly sophisticated timewarp-kioslave.\nSo actually the whole visual output of KDevelop is generated by an Anjuta-process running five years ahead in the future. Pretty evil, huh? ;-)"
    author: "Doc Brown"
---
<a href="http://www.comdex.com/lasvegas2003/">COMDEX</a> is one of the largest computer conventions in the United States. <a href="http://www.oreillynet.com/">O'Reilly & Associates</a>, publishers and Open Source advocates, have stuck a <a href="http://www.oreillynet.com/pub/wlg/3895">deal with COMDEX to sponsor six open projects</a> to appear among the commercial vendors and demonstrate their product.  KDE is one of the 21 projects selected as possibilities, and there is an <a href="http://www.oreillynet.com/contest/comdex/">online poll currently running</a> to select the six choices. You can vote once per day, so make it part of your daily routine!
<!--break-->
