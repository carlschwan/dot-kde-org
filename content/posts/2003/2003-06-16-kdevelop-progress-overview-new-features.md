---
title: "KDevelop Progress: Overview of New Features"
date:    2003-06-16
authors:
  - "hrodda"
slug:    kdevelop-progress-overview-new-features
comments:
  - subject: "great work!"
    date: 2003-06-16
    body: "Kdevelop is becoming a ultra awesome and very professional environment. Thanks kdevelop developers! there is nothing else quite like kdevelop for linux (except for ajunta, which is nowhere near as powerful as gideon is)"
    author: "bjo"
  - subject: "Re: great work!"
    date: 2003-06-17
    body: "Documentation howecver is crap..."
    author: "Gerd"
  - subject: "Scripting languages"
    date: 2003-06-16
    body: "I'm a python developper and \"support for additional programming and scripting languages\" (like python) is great !! I was using kate, but I missed an autocompleter (which I found with Scite). By the way, Boa-Constructor is good ide too, but really too buggy :(\n\nI have great hope with gideon !"
    author: "Philippe"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "AFAIK both the class view and code completion works.  The only thing really missing is code folding, but that is because we use kate, and its folding engine has problems with the python method of scoping.\n\nThis was just a quick update, I think php, and python support are both quite old, and I think are pretty stable.\n\nLanguage support stands at:\nPerl, Python (PyQt), Ruby, Fortran, Bash shell, Pascal, PHP, Java, C++, and ADA\n\nIn C/C++ we have GTK and GNOME support, along with Qtopia, and gameboy advance\nIn Java we have ANT support and SuperWaba support...\n\nMan, its easier just to list the stuff we dont support ;)\n(Actually docs is where we need help now, so if you like to write/translate come on over and give us a hand!)\n\nCheers\n\t-ian reinhart geiser\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "> Man, its easier just to list the stuff we dont support ;)\n\nSure? I miss support for Tcl, ECMA/JavaScript, Objective C, VisualBasic, Pike, Guile, Scheme, Lisp, Emacs Lisp, Prolog, Algol, Logo, Natural, C#, StarBasic, DOS Batch, ML, Modula-2, Modula-3, Oberon, C+, Managed C++, C with classes, OPL, QuakeScript, ActionScript, Lingo, INTERCAL, Forth and the good old COBOL.\n"
    author: "AC"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "Supported or not, I won't miss them for a second! ;-)"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "Got objC, C# i think is in development, I tried Forth, but gave up when i found it wasnt really useful in an IDE, C with classes?!  you mean structs with function pointers?  never heard of that one...  TCL, sorta works, but again, has problems with ITCL...\n\nSo really unless you want to code in something like brainfuck, or RPL, KDevelop will do what you want...\n\nNow if your pet language is missing and at least has a function like structure, then fell free to drop me an email, and I can show you how to add you pet language support.  The reason I gave up on Forth, was, it seemed overly difficult to get variable scope for autocompleting... now, if someone actually knows how to do this feel free to pick up where I left off :)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "C with classes was the predecessor to C++.\n\nBTW, i wasnt serious about all this. And I forgot B and BCPL. Shame on me.\n"
    author: "AC"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "Gave me a good laugh, fo' sure."
    author: "anon"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "Cobol? Waht about this KOBOL of THEKOMPANY? Cobol would be very nice. there are free compilers.\n\n\nGNU ADA? "
    author: "Bert"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "As far as I know that is not a free/OSS project.\n\nIf you want to implement Cobol support feel free to drop me a line and I can walk you through it...   Its mostly an afternoon of playing with regexps...\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "http://tiny-cobol.sourceforge.net/\nhttp://cobolforgcc.sourceforge.net/cobolforgcc.html\n\n\nKobol is the founding planet of mankind. All Thirteen Tribes of man originated on Kobol, and they themselves founded other colonies, some close by to the Colonies, some light years away. \nhttp://staff.bus.bton.ac.uk/fesg/bsg/colonials/kobol.html"
    author: "Andr\u00e9"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "From TheKompany's website\n\nKOBOL = KOmpany Business Oriented Language\n\n:-)\n"
    author: "Andr\u00e9"
  - subject: "Lispy langs, other fuctionals, Designer. "
    date: 2003-06-17
    body: "Its a shame no lispy languages are supported as of yet. \n\nLuckily they are all similar syntactically, shouldn't be hard to do all of them easily once the basics are there.. . \n\nOther languages that I actually do use are XSLT[1],\nHaskell, \nObjcaml, \nR ( S+ really, stats language). \n\nWith all of these langs in Kdevelop, it'd be interesting to look at some interplay with language integration systems - SWIG, Corba, Mono, maybe even SOAP/XML-RPC - eg auto or semi auto coding proxies between languages ... hard to know how this would pan out, but worth some thought. \n\nAnother thing I'd love to see is the QT Designer properly componentised and embeddable in Kdevelop. \n\n[1] an evil semi-functional language in XML form, seemingly the only corporately acceptable fuctional language. Shame that you can wrap something in sicko XML, make it worse than every one of its competitors, and *thats* the one that catches on..... "
    author: "rjw"
  - subject: "Re: Lispy langs, other fuctionals, Designer. "
    date: 2003-06-17
    body: "For R it would be better to have a SPSS like frontend.\nPerhaps a ripped off spreadsheet with added functioanlity."
    author: "Andr\u00e9"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "\"gameboy advance\"? :-) ( I can't really be sure if this is a joke or not... you can expect whatever from the gideon developers! ;-)"
    author: "uga"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "No joke man! \n\nWe have templates and full support, all you need to get is the cross compiler.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "!!! \n\nWell... only one word: amazing. I'll have to see those templates in the next update, although I don't expect to use such a thing anytime soon :-)  \n\nThe only thing I expect soon to get my hands dirty with, is Qt#... otherwise Gideon's c++ plus Qt is more than enough for my daily use.\n\n\n\n"
    author: "uga"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "and Dreamcast, inform6 and other interactive fiction langagues are in the work."
    author: "Mathieu Chouinard"
  - subject: "Re: Scripting languages"
    date: 2003-06-16
    body: "> Perl, Python (PyQt), Ruby, Fortran, Bash shell, Pascal, PHP, Java, C++, and ADA\n \nDo you mean Ada? If so, note that it is spelled \"Ada\" and is not an acronym, see [http://www.adapower.com/lab/adafaq/5.html]."
    author: "Erik"
  - subject: "Re: Scripting languages"
    date: 2003-06-17
    body: "What's up with Cg? (C for Graphics from nvidia)\nshouldn't be that hard, but also shouldn't be that nessecery"
    author: "panzi"
  - subject: "Re: Scripting languages"
    date: 2003-06-17
    body: "The kate folding engine also has problems with latex methods of scoping.\n\nSee: http://bugs.kde.org/show_bug.cgi?id=54199\n\n It would need to include many different ideas: \n 1) the most common delimiters-couple based: ( ), { }, [ ], begin{*} end {*}, if fi, etc... \n nestability defined as an attribute \n 2) indentation based (think python) \n 3) \"new-marker\" based, with hierarchy (more complicated): \n -Used for html, latex, etc.. \n -Folding at a level of the hierarchy fold everything under (much like nested \n delimiters-couple) \n\nThe result would be something like this with a hierarchy where \npart > section > subsection\n\n - \\part{a} \n - \\section{b} \n - \\subsection{c} \n - \\section{d} \n \n - \\part{a} \n + \\section{b} \n - \\section{d} \n \n + \\part{a} \n \n \n "
    author: "Fran\u00e7ois Desloges"
  - subject: "Re: Scripting languages"
    date: 2003-06-17
    body: "Any help on improving kate part in that aspects are very welcome btw. ;)"
    author: "Christoph Cullmann"
  - subject: "Re: Scripting languages"
    date: 2003-06-19
    body: "Well I've gone as far as I could in  http://bugs.kde.org/show_bug.cgi?id=54199 \n\nThe basic idea is that in the context tag of the DTD, we need something\nlike may Regions that ends at once i.e. an attribute like:\n\n endRegion=\"Part,Section,Paragraph\"<br>\n\nAlso, in a document to be code-folded, if one or many effective endRegion strings are  found on the same line of a beginRegion string, the desired result would be for the  precedent code to fold up to but not including this line. Once again, look at\nhttp://bugs.kde.org/show_bug.cgi?id=54199  for more details.\n\nSorry, I have neither the time budget nor the computer budget to throw myself in the development of Kate :-(\n\n\n \n"
    author: "Fran\u00e7ois Desloges"
  - subject: "Integration"
    date: 2003-06-16
    body: "You know what? In the last few months I've seen a lot of work going on in the 'integration' dep. Once stand-alone apps are integrated into major big projects.... We've seen single applications like kmail, korganizer,... maturing over a long period of development. Now integrated into an even bigger project... kontact\nKCachegrind is now integrated into KDevelop... KDevelop uses Kate as it's Editor 'component'... It's simply amazing how all these 'components' fit to each other... It's like watching the construction of a big building...\nI think KDE really shines in terms of  'code reusability' and 'component architecture'.\n...'nough praise for now.."
    author: "Thomas"
  - subject: "Go, go, go..."
    date: 2003-06-16
    body: "Completely agreed and 100% supported. Go ahead, try and use Gideon. Aside of some small annoyances I think you will be amazed. Personally I like it very much. :-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Go, go, go..."
    date: 2003-06-16
    body: "Please report those small annoyances at http://bugs.kde.org!\n\nThese bugs all get posted to the devel list and are discussed/fixed.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Go, go, go..."
    date: 2003-06-17
    body: "> Please report those small annoyances at http://bugs.kde.org!\n \n[chuckling]\n\nMaybe you didn't read the signature line... Andras not only reports those bugs but he's fixxed some including one that had been a mysterious annoyance to developers for something like two years. Andras also keeps Quanta at the bottom or off of the list of the top 100 KDE applications for bugs on Bugzilla. ;-)\n\nCheers,\nEric"
    author: "Eric Laffoon"
  - subject: "Button bar"
    date: 2003-06-16
    body: "I haven't been able to figure out so far: Where / How do I add\nuser defined icons for the toolbar of my application. Do\nI have to copy them in some special folder? Where in the XML-GUI\nfile to I choose which icon file should be used? Hour of reading\nthrough the QT and KDE documentation and I haven't been able to find\na solution. This is really frustraing, because this is the last thing I would\nneed for writing some basic (but complying to the standards) KDE application."
    author: "Jan"
  - subject: "Re: Button bar"
    date: 2003-06-16
    body: "http://developer.kde.org/documentation/library/kdeqt/kde3arch/iconloader.html \nexplains where and how to install additional icons.\n\nIcons are a KAction propertie. You specify the icon when calling the KAction constructor or with KAction::setIcon. I don't think you can override a KAction icon in the XML-GUI file.\n\n"
    author: "Chris"
  - subject: "Re: Button bar"
    date: 2003-06-16
    body: "Thanks! I wonder why I haven't found that page before...\nI will try this out as soon as possible.\n"
    author: "Jan"
  - subject: "Great Job Kdevelop team."
    date: 2003-06-16
    body: "Just wanted to drop a line of support to everyone associated with Gideon.  KDevelop is quickly becoming the best IDE available, in any environment .  The amount of work you guys have completed along with the quality of that work is just astounding.  Thanks for all of it and keep up the good work.  I use Gideon almost every day and yet I consistently find ways in which it makes my work easier and easier.\n\nStrid..."
    author: "Strider"
  - subject: "Good Job !!"
    date: 2003-06-16
    body: "\n\n\tIts great to hear that KDevelop is increasing its features, with the integration for previewing ui files in Designer it gets a nice feauture :), and i hope it improves also in the tabbing area where it still lacks just a little :(....still for many years kdevelop has been the leading linux IDE and sure it'll continue to be !!!\n\n\n\tCheers...\n"
    author: "Rodolfo"
  - subject: "I  l o v e  i t  ! !"
    date: 2003-06-16
    body: "Normally, I would piss and complain about the UI..\n(small example: those terrible splitter-window-like-bevels at the sidebars are even drawn when the theme doesn't do that for normal splitter windows)\n\nBut I'll have to make an exception for this awesome IDE, I just can't whine about it.\nI've been using it for one full week now (the cvs version), and it made my life much easier compared to the old multi-terminal/code/make/execute/cli-command way of doing things.\n\nJust a few questions though..\n\n- Does the latest cvs version already generate that database for auto completion and stuff automatically?\n  (have to do that using r++ currently, or use the older ac stuff..)\n- Will there be some intelli sense -like feature some day? (ie. lingering over a variable should give a tooltip with the type of the var)\n- Is there some way to decrease the popup/response time of the auto completion dialog?\n  (takes too long to popup, I'm typing too fast :)\n\nAnd this one should actually be posted @ kde-zilla, but I'm a bit lazy atm so here goes:\n\n- There's currently a tab that shows the programmer all kinds of typing mistakes and all that, which is done by compiling in the background.\n  Could that be changed into something more visual?\n  (eg. Use those red understriping lines from the well known word processors to show off typos)\n"
    author: "nac"
  - subject: "Re: I  l o v e  i t  ! !"
    date: 2003-06-16
    body: ">- There's currently a tab that shows the programmer all kinds of typing mistakes and all that, which is done by compiling in the background.\n>Could that be changed into something more visual?\nTry pressing F6, or enable \"show iconborder\" in the editor settings ;).\n\nYours, \nRischwa"
    author: "Rischwa"
  - subject: "Re: I  l o v e  i t  ! !"
    date: 2003-06-16
    body: "Doesn't that icon border only show stuff after doing a build?\n\n(what I meant seems to be done in real time, which would be more useful with those red underlined words)\n"
    author: "nac"
  - subject: "Re: I  l o v e  i t  ! !"
    date: 2003-06-16
    body: "If you would have pressed F6 for testing, before posting, you would have known better ;)\n\nYours, \nRischwa"
    author: "Rischwa"
  - subject: "Re: I  l o v e  i t  ! !"
    date: 2003-06-17
    body: "Didn't have the needed stuff running ;)\n\nI still think red lines would be more visible though, but this is already better than what MSVC6/7 and Anjuta do, so.. :)\n"
    author: "nac"
  - subject: "Re: I  l o v e  i t  ! !"
    date: 2003-06-22
    body: "I feel those red lines really would be useful, perhaps in addition to the icons bar because the icon is globally oriented to the whole line.\nYet I do not want my sources being cluttered by all red lines. Showing red lines on demand, e.g. when clicking the icon, would save some time however, especially in complex circumstances.\nOne could even go further down on this idea, providing a chain of dependend errors by clicking any of those red lines...\nWell, this is utopia for now..."
    author: "Bippo"
  - subject: "QTdesinger"
    date: 2003-06-16
    body: "Will the work on usability list be incorporated into qt designer?\n\nThe programmers seemed to show a lack of interest for changes.\n\n\nSupport of Pascal? GnuPascal or FreePascal?\n\n\n"
    author: "Bert"
  - subject: "Re: QTdesinger"
    date: 2003-06-16
    body: ">Will the work on usability list be incorporated into qt designer?\n No it will not,thats TT's project\n\n>Support of Pascal? GnuPascal or FreePascal?\nIts in there.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: QTdesinger"
    date: 2003-06-17
    body: " > Support of Pascal? GnuPascal or FreePascal?\n \n It is already available (for now without class browser and code completion but they will be available till the 3.0 release).\nFor free pascal there are some project templates (including gtk app) and compiler options dialog - i've implemented all known options but if you find missing one fell free to bug me ;).\nGnuPascal should work but there are no compiler options dialog - i can't get a usable installation of gpc.\n "
    author: "Alexander Dymo"
  - subject: "pyQt Development"
    date: 2003-06-16
    body: "What is the best PyQt IDE?\n\nAnd which is better, KDevelop or Eric 3 (http://www.die-offenbachs.de/detlev/eric3.html) for PyQt development?"
    author: "Alex"
  - subject: "Re: pyQt Development"
    date: 2003-06-17
    body: "Hrm... thats a tough call.\nEric has a debugger and code folding... Other than that they are equal.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: pyQt Development"
    date: 2003-06-17
    body: "Any estimate on when those features might be incorporated into Kdevelop. or maybe tehy can be added as plugins, just take the code from Eric."
    author: "Alex"
  - subject: "Re: pyQt Development"
    date: 2003-06-17
    body: "Wing (http://www.wingide.com/) has code folding, a debugger, code completion with a parser (that will complete imported modules and do other nice things). Unfortunately from a hobbyist perspective, it is commercial.\n\nWhat may be of more interest is that the parsing part of Wing is open source, so it may be feasible to add it to e.g. Gideon.\n\nI use PyQt in a commercial environment (yes, really!) and thus far Wing's code completion support puts it above other IDEs for me. It doesn't integrate the documentation etc. but I don't mind running the assistant \"on the side\"."
    author: "Faeltir"
  - subject: "wxWindows?"
    date: 2003-06-17
    body: "Great news! I loved KDevelop before, and Gideon is even better! Can't wait to get a new CVS Snapshot running... :-)\n\nI have one question though:\nWould it be difficult to add some nice and easy to use wxWindows templates??? How would/could this be done?\n"
    author: "Shyru"
  - subject: "Re: wxWindows?"
    date: 2003-06-17
    body: "Take a look at parts/appwizard/cpphello to see a template example"
    author: "Amilcar Lucas"
  - subject: "what about kbasic?"
    date: 2003-06-17
    body: "I know this is off topic but the screenshots looks really great and it's been pretty quite lately, is it dead or what? it is said to be released this summer http://www.kbasic.org/1/home.php3\nany news about it and will it be supported by kdevelop or maybe integrated?\n\nthanx\nbob"
    author: "bob"
  - subject: "Re: what about kbasic?"
    date: 2003-06-17
    body: "kbasic has been around for a very, very long time, as long as perhaps kdevelop  has been around-- however, it's been marred by periods of deep and long inactivity. The eight months has been such for the kbasic project. \n\nYou might want to try Gambas or hbasic though. Both are actively maintained and nice Basic (VB-type) environments. I haven't tried either of them out, but based on screenshots, they look pretty damn cool."
    author: "fault"
  - subject: "simply amazed"
    date: 2003-06-17
    body: "I'm so impressed by KDevelop3 that I've written a poem :)\n\n-------\nFather says to son while writing on Emacs\nthat all modern IDEs are fscking prone to bugs\n\"They are also bloated, can I say it better?\nbugs and features always come together!\"\n\n\"Don't disturb me Father in kde-api lecture\nGideon is stable thanks to plugin architecture!\"\n-------\n\nWaiting for beta!"
    author: "chris_k"
  - subject: "refactoring ala Eclipse?"
    date: 2003-06-17
    body: "Can anyone familiar with both Eclipse and KDevelop(Gideon)\ncomment on how KDevelop stacks up with regard to support\nfor refactoring?\n\nMore generally (forgive me - I'm still using XEmacs), how does\nKDevelop for Java compare to Eclipse, and how does Eclipse\nfor C++ compare to KDevelop? "
    author: "Steve"
  - subject: "Re: refactoring ala Eclipse?"
    date: 2003-06-17
    body: "Eclipse has the best refactoring tools that I've ever seen in an IDE. Such functiontionalty isn't there in kdevelop yet, but I guess someone could create a plugin for it. It would be non-trivial to make one that compares to Eclipse's refactoring tools however; they are extremely intelligent. \n\nFor Java development, I generally prefer Eclipse (because of good refactoring support and junit support).. For C++, I generally prefer kdevelop (kdevelop has historically been a C++-only IDE, and as such, it has great support for it)"
    author: "moro"
  - subject: "Re: refactoring ala Eclipse?"
    date: 2003-06-28
    body: "Hi\n\n>Eclipse has the best refactoring tools that I've ever seen in an IDE.\n\nIntelliJ Idea has the best refactoring tools. The IDE is much more powerfull than Eclipse.\n\n- Jens"
    author: "Jens"
  - subject: "Nice"
    date: 2003-06-17
    body: "I've been running KDevelop Beta 4 since it came out (beta 3 was too buggy), and I'm very impressed with all the features and functionality. It appears to be a completley full featured, professional grade IDE. I only have 2 problems with it though:\n\n1) After highlighting some text, you have to either hit ctrl+c or go to the menubar just to copy it, as opposed to the traditional option on the right click menu\n\n2) Sometimes, when you're writing a quick/small/test program, it's way too much overhead to have to create an entire project, and KDevelop doesn't support single file compilation. In those situations, I either use Anjuta or just fire up KWrite and compile the program from the command line. Maybe a lightweight or 'quick' mode for KDevelop should be implemented ?\n\nAnyway, these both are just minor things, and I usually just try to work arround them (because I feel KDevelop is worth it:)"
    author: "chillin"
  - subject: "Website"
    date: 2003-06-17
    body: "WOW, the Kdevelop website is ancient. Its in desperate need for updates. I mean come on, even the features page is ancient:\n\n\"KDevelop 1.1 even incudes KDE 2/Qt 2 frameworks for those developers that want to stay on the bleeding edge of KDE development.\" \n\nYes, KDE 2 is definetely bleeding edge.\n\nand on the Graphics page, there shouldn't be 3 screenshots sections, jsut make 1 and have a \"next ->\" link.\n\nEven the press is ancient, Linux Tag 2001?\n\nThe software used on the site is also old, phorum, taht thing totally sucks when you have something like PHPBB and you alrady have a space on KDE-Forum.org here:\nhttp://kde-forum.org/viewforum.php?f=11 there should be a link.\n\nLinks page is out dated too, listing mosfet's site as a KDE 2 development site and even listing broken links. \n\nThe documentation is ancient too, the suer manual's last update was in 1999.\n\nReally this is pathetic and it happens like this to a lot of KDE projects! Even Konqueror's page has the last news item in 2002 while so many exciting things have happened to it. In addition KDE websites were supposed to adopt the new design, but many flagship software websites ave not been updated like Koffice's website.\n\nSeriously the KD Eproject websites are rarely updated, the dot and kde.org seem to be in the minority of websites being updated regularly. THe documentation also sucks.\n\nI would be happy to help make the KDevlop website better by providing a new design and updating whatever content I can if you want."
    author: "Alex"
  - subject: "Re: Website"
    date: 2003-06-17
    body: "Why not go and fix it then? Developers generally don't care about websites because they either 1). don't care about the website much or 2). rather code.\n\nIt often takes non-Developers to do such things, and unfortunatly, nobody has stepped up for kdevelop. Ajunta actually does have someone who updates the website (and is not a developer).. \n\nSimiliar situation for documentation. Doc-writers were extremely active during the KDE 2.x era, and have seemed to be much more busy lately. My suggestion is to submit bugreports when you see outdated docs, and if possible, send a patch. Often for things like this, bug reports crop up, but there is nobody to fix it :(\n\n"
    author: "Papa"
  - subject: "Yes"
    date: 2003-06-17
    body: "I want to try to adapt the Kdevelop website to the KDE.org design if I can.\n\nHowever, I will be gone for about a month and a half so I won't be able to do much except this week.\n\nBTW: Why aren't there as amny active doc writters WE NEED THEM! ;)"
    author: "Alex"
  - subject: "Re: Yes"
    date: 2003-06-17
    body: "Could you please drop a line to kdevelop-devel@kdevelop.org ?\nThe people there can tell you how you can help.\n\nThanks"
    author: "Amilcar Lucas"
  - subject: "Anjuta even has a better website"
    date: 2003-06-17
    body: "and I heard from people her ethat Gideon is a lot better.  We need a website to show the quality of the program, currently the website for Kdevelop will just turn people off.\n\nCompare for yourself: \nhttp://anjuta.sourceforge.net\nhttp://www.kdevelop.org\n\nYes, your right, the Kdevelop website does not stand comparrison."
    author: "Alex"
  - subject: "Re: Anjuta even has a better website"
    date: 2003-06-17
    body: "There are actually still sites making use of frames? o.O"
    author: "Datschge"
  - subject: "Re: Anjuta even has a better website"
    date: 2003-06-17
    body: "Frames are crap. Something like Zope's standard headers and footers make a much better job, and make things much more expandable."
    author: "David"
  - subject: "Re: Anjuta even has a better website"
    date: 2003-06-17
    body: "Yes, and they are used wisely for that website."
    author: "Alex"
  - subject: "Emacs keybindings"
    date: 2003-06-17
    body: "Is it possible to get Gideon to understand Emacs keybindings, or rather, Unix keybindings? The C-c, C-v and C-x thingies are from the Windows world, there are so any of us that are used to the Unix/Emacs way of doing stuff. I've used Gideon a fair deal and like it a lot. It has a few bugs but those seem to get fixed pretty fast, but the keybindings irritate me every time I use it. I always use the Emacs ones, and they wreak havoc inside Gideon. :)"
    author: "Chakie"
  - subject: "Re: Emacs keybindings"
    date: 2003-06-17
    body: "Actually, I've had a similar thought to this numerous times, but never got around to posting a message about it (too busy coding).  Why can't KDevelop be given a feature where you can select between a number of different predefined keybindings, and/or create your own, and be able to share them on the KDevelop website (in case you create a custom one which matches an editor whose keybindings you're used to using, and want to contribute it for others who are used to those same keybindings)?  I think that would make life a lot easier for anyone who is switching KDevelop, or just plain prefers a different set of keybindings."
    author: "Mike"
  - subject: "Developing libraries."
    date: 2003-06-17
    body: "Gideon's support for developing and compiling libraries needs a lot of improvement.  This is especially important on a unix system, since putting the underlying mechanisms in a library, then writing a front end is a fundamental concept of unix development.  In it's current state, you have to completely rework the structure of subprojects and targets to create a project format that makes library development efficient and pleasant."
    author: "Sean Fraley"
  - subject: "Re: Developing libraries."
    date: 2003-06-17
    body: "I'm a KDevelop developer and user, I use it to code linux libraries.\n\nI don't see your point. Can you elaborate?\n\nThanks"
    author: "Amilcar Lucas"
  - subject: "Re: Developing libraries."
    date: 2003-06-18
    body: "I don't think any of the choices for C++ project types works well as is for developing a library, or at least not one specifically for KDE and KDevelop.  It's little things. The target for the src subproject being set to a program.  No subproject set up for testing the library.  I think that there needs to be a project type that you can select for creating generic libraries in C or C++.  It should have the src subproject set to build a libtool library, and have a seperate subproject for testing the library.  At current, with a little reworking of subdirectories and targets, I can get things set up pretty well, but it would be nice to have a project type that did this for me."
    author: "Sean Fraley"
  - subject: "Valgrind"
    date: 2003-06-17
    body: "This is slightly OT, but can someone explain to me why we have KCachegrind but nobody seems to be interested in integrating Valgrind itself (well, more specifically, the memcheck \"skin\") into a GUI/IDE? Surely finding and fixing memory bugs is more important that improving performance...\n\nOf course, this is not to detract from the recent KDevelop/Gideon work, which is excellent :)"
    author: "Paul Eggleton"
  - subject: "Re: Valgrind"
    date: 2003-06-17
    body: "Because valgrind is useful without a GUI. You just run you programm on the command line and get some output when valgrind detects something unusual. But cachegrind, OTOH, is almost unusable without some visualization. There's a strong need for visualization, and that's why people have the incentive to do it.\n"
    author: "AC"
  - subject: "Re: Valgrind"
    date: 2003-06-17
    body: "Yes, valgrind is already integrated."
    author: "Amilcar Lucas"
  - subject: "important things i believe need to be done"
    date: 2003-06-17
    body: "dark schema for the text editor (ie black background that doesnt affect the syntax highlighting readability) and syntax highlighting improvements (i find vim is the best i have seen).\n\nvariable/class/etc name completion popup in text editor is kind of cool (like anjuta does) although at times it can piss you off.\n\nthese things arent exactly directly problems of kdevelop but there use kwrite so they inherit them."
    author: "not_regisitered"
  - subject: "XUL support"
    date: 2003-06-17
    body: "Please note that KDevelop now supports Mozilla XUL files in the same way it supports Qt Designer .ui files, so it can be used as a Mozilla IDE :) \nA screenshot showing KaXulPart interface in Konqueror is here:  http://www.automatix.de/~zack/kaxulpart.png and get the code from kdenonbeta/kaxul ."
    author: "Zack Rusin"
  - subject: "Re: XUL support"
    date: 2003-06-27
    body: "this is great and exactly what i am looking for. which version of kdevelop ( or kde version ) supports this ?"
    author: "jef peeraer"
  - subject: "Re: XUL support"
    date: 2003-06-27
    body: "can you explain a little bit more. do i create the xul files with qt-designer ? "
    author: "jef peeraer"
---
The CVS version of <a href="http://www.kdevelop.org/">KDevelop</a> (a.k.a.
"Gideon") continues to improve, both stability-wise and in the feature
department.  Several new features of interest include the ability to easily
<a
href="http://yoyo.cc.monash.edu.au/~meddie/patches/screenshots/debugging2.png">attach the debugger</a> to crashed applications inside KDevelop by <a
href="http://yoyo.cc.monash.edu.au/~meddie/patches/screenshots/drkonqi.png">clicking on a
button</a> in Dr. Konqi; the beginnings of integration of <a
href="http://kcachegrind.sourceforge.net/">KCachegrind</a> (<a
href="http://www.di.unipi.it/~raggi/calltree2.png">screenshot</a>) by Roberto Raggi, the programmer behind Gideon's solid C++ support; Qt
Designer <a
href="http://www.di.unipi.it/~raggi/uiview.png">file preview support</a> (.ui) in the file selector; and <a href="http://geiseri.myip.org/~geiseri/pictures/computers/kdevelop-bash-support.png">support
for additional programming and scripting languages</a> such as bash.

Rumors of support for Palm Pilot<sup>TM</sup> development also abound...
<!--break-->
