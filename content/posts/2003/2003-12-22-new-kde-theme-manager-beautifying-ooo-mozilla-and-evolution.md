---
title: "New KDE Theme Manager + Beautifying OOo, Mozilla and Evolution"
date:    2003-12-22
authors:
  - "numanee"
slug:    new-kde-theme-manager-beautifying-ooo-mozilla-and-evolution
comments:
  - subject: "No ugly icons anymore - hurray!"
    date: 2003-12-22
    body: "Just three days ago a friend told me, that OpenOffice may do all his tasks, but the icons are so ugly (outdated), that he wouldn't use it... \nAnd I'm happy too, as OpenOffice (and Mozilla) isn't looking as a contaminant anymore.\nI think this is (part of) what KDE currently needs to be accepted as a really good Desktop."
    author: "Birdy"
  - subject: "Re: No ugly icons anymore - hurray!"
    date: 2003-12-23
    body: "I don't know what kind of a person that wouldn't use a product because the icons are not 'pretty'.\n\nSure we all love eye-candy, but this is a childish behavior. If an application provides me with what I need, I'll use it.\n"
    author: "RMS"
  - subject: "Re: No ugly icons anymore - hurray!"
    date: 2003-12-23
    body: "Most people wouldn't drive an ugly car even though it may get them where they needed to go.  I definitely wouldn't.  So why is software such a far stretch?  It's not, really."
    author: "Jay"
  - subject: "Re: No ugly icons anymore - hurray!"
    date: 2003-12-23
    body: "This is because the car is a public display and people wants \"public\" things to be look as good as possible. \n\nI don't go around showing people how my OO looks like.\n"
    author: "RMS"
  - subject: "Re: No ugly icons anymore - hurray!"
    date: 2003-12-23
    body: "He's a guy using (mostly) Windows and as far as I know \"just copied\" MS Office. So OO and MSO are equal cheap, but MSO is looking more beautiful. It's the situation that so many people face..."
    author: "Birdy"
  - subject: "Keramikzilla is great"
    date: 2003-12-22
    body: "These newfound integration efforts are kick-ass!  I still use Mozilla for some things, and Keramikzilla makes it stand less out (even though I use Plastik).  I do wish, though, that it would be \"smart\" and pick up my custom KDE color settings, but that is probably not possible with Mozilla themes, I don't know.  \n\nI was pretty amazed when I tried Sodipodi some months ago and it opened the KDE file selector, and now I see there's even more KDE stuff there, and people on the KDE lists are discussing how to make GTK apps use the KDE file dialog when running in KDE.  Bodes well!"
    author: "Haakon Nilsen"
  - subject: "Re: Keramikzilla is great"
    date: 2003-12-22
    body: "Mozilla themes are mainly done using CSS style sheets. So, to make Mozilla follow the KDE color settings, it would be needed some kind of script or program that patches the style sheets and zips them again into a jar file. But that's not the main problem. The main problem is that some things are made with images (like the scrollbars and the buttons), and that script would need to be capable of loading the images, recolouring them and saving them. So, that's a lot of work and Keramizilla should be more a metatheme than a real theme if you want to do that.\n\nRight now I'm working to make Keramikzilla's icons follow the same directory structure as KDE, so changing the icons will be as easy as copying /usr/share/icons/<yourtheme> into the right Keramikzilla directory. I want to make different versions of Keramikzilla: one with small icons (like in Konqueror), another with the Noia icon set, another with Crystal SVG instead of Crystal GT and so.\n\nFor that, I'm thinking about starting a project to make Mozilla / Firebird themes that match the KDE look, something like \"Mozilla KDEization\" or so. Konqueror is great, I also use it, but there're still some pages Konqueror can't render properly, and Mozilla has a better CSS 2 support. I've also heard about KMozilla, but have never seen it working. Also, maybe you go to your work or your university practices and you can't use Konqueror. At least you will be able to use Keramikzilla, won't you? That's why I decided to make Keramikzilla and am thinking about starting that project. Any volunteers? ;)"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Keramikzilla is great"
    date: 2003-12-22
    body: "Great work! Perhaps you write down in your blog who you did it7 what you did, so others can follow you. Will it be necessary to create a Mozilla theme for each KDe theme? and what about KDE/GNOME Installation? When the Mozilla theme was switched to KDe style it will appear in same fashion when Gnoem is started, hmm?"
    author: "Matze"
  - subject: "Re: Keramikzilla is great"
    date: 2003-12-22
    body: "Maybe it is possible to just use the Mozilla rendering engine once it is properly split out from the rest of Mozilla. The Mozilla people are currently working on just that. (Oops I realize that sounds a bit like Galeon and Epiphany.) That would get you the Gecko renderer with a more KDE like interface :) I use both konqueror (well safari) and Mozilla, I love them both. Gecko is a little more advanced in some standards though."
    author: "thijs"
  - subject: "Re: Keramikzilla is great"
    date: 2003-12-22
    body: "That would be kool, but right now the only thing I know how to do is to make Mozilla themes. I prefer to work on that instead of doing nothing. ;) And anyway, Mozilla isn't just a browser. It's also a mail client (the one I use, BTW), an address book, a Javascript debugger, an IRC client... Mozilla's addons are great, and I don't think you would be able to use them if you just take the rendering engine (Gecko) and put it into Konqueror or another KDE GUI. But if you just have a Mozilla theme that matches your KDE look, you'll feel fine with Mozilla although it doesn't use the KDE libs/Qt."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Keramikzilla is great"
    date: 2003-12-22
    body: "Mozilla themes would continue being independent from KDE/Gnome. So it doesn't matter if you change the KDE style or the Gnome one, the Mozilla theme will continue being the same.\n\nWell, not that independent from Gnome because Mozilla uses GTK. But at least in Keramikzilla, I want to make it independent from the GTK widgets. This way if you change the GTK theme it won't affect Keramikzilla. Right now, the only remaining widget to make independent from GTK is the tab bar.\n\nBy the way, I've finished Keramikzilla 0.7 for Mozilla 1.3 and 1.4. I have to make the port to Mozilla 1.5 and I'll upload it in half an hour or so. :)"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Keramikzilla is great"
    date: 2003-12-23
    body: "Firebird on Windows can use native widgets.  I don't know exactly how it's done, but I'm pretty sure it doesn't use images and stuff.  It uses Windows functions to draw the widgets directly, just like any other Windows program.  You should look into that code, because it could probably be adapted to draw with QT instead."
    author: "Spy Hunter"
  - subject: "Re: Keramikzilla is great"
    date: 2003-12-28
    body: "It's pretty easy, you just have to set the property \"-moz-appearance\" to the name of the widget like, for example:\n\nbutton {     // The name of the XUL widget\n  -moz-appearance: button;     // The name of the native widget\n}\n\nThat's what the \"classic\" theme does. But Mozilla uses GTK and I don't know any way to make it use Qt. So you depend on GTK themes that match the Qt ones and for themes like Plastik there aren't any match. Also, it would make your theme platform-dependent and it would only work on Linux. That's enought, I know, but it would be much better if the theme is platform-independent. You kill two birds with one shoot if you just make a theme that matches the KDE style directly without depending on native GTK widgets. Also, it would be easier to install"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "One ICOn"
    date: 2003-12-22
    body: "One Icon of the OO set seem to be missing. Only a complete icon set can be fruitful. But how do we get whether an icon set is complete. Is therea KDe standard of standard icons that have to be included as minimum."
    author: "Matze"
  - subject: "Theme Manager"
    date: 2003-12-22
    body: "How about integrating the Theme manager and the appearance Preferences into one panel, so that it could also be used to save all appearance settings. \nThis could also improve usability\n\nJust look at the way it was done in Mac OS 9 (i could provide you with screenshots if you like)"
    author: "Okona"
  - subject: "The New Theme Manager"
    date: 2003-12-22
    body: "Lukas, \n\nSeeing as the theme-manager handles what appears to be the entire look-n-feel branch of the control center, is it intended to make the position of the theme manager clear in the module hierarchy with the redesign?\n\nI envisage making the individual components of look-n-feel available as a sub branch off of the theme-manager (when in tree view). But this is off the top of my head, I acknowledge this isn't possible with the current control center, and wouldn't work in the other control center view mode.\n\nBut something like that seems to make sense. Dunnit? :-)\n\nIMO doing it windows style, where the individual modules are separate dialogs that you open via big buttons in the theme manager (labeled: colors, advanced, etc.) isn't great, and also doesn't fit in with the control center's current style. Also I think leaving it as is (theme manager and rest in the same branch) is sub-optimal since it doesn't make it clear that the theme-manager handles the settings for many of the other modules."
    author: "Max Howell"
  - subject: "Re: The New Theme Manager"
    date: 2003-12-22
    body: "That's why I think it would make sense to have the theme manager as a separate application. For example, settings such as the background cannot be hidden in an advanced dialog of the theme manager.\nThe control center could be simplified however in unifying kcms related to the look'n feel of applications into a single kcm (colors-style-windeco). I have started doing that, you can mail me if you are interested."
    author: "Benoit W."
  - subject: "Re: The New Theme Manager"
    date: 2003-12-22
    body: "Being a KCM, it can be easily run independently using kcmshell. This would have the same effect as having it as a separate app."
    author: "Nadeem"
  - subject: "Re: The New Theme Manager"
    date: 2003-12-22
    body: "Not exactly. Of course it is still possible to \"run\" it as if it were a separate application. But what I meant is that it should not be included in the control center. Otherwise, the theme manager will handle settings which belong to other modules. This duplication is confusing and was one the reasons why the old theme manager has been removed. A solution would be to have a kind of hierarchy in the \"Appearance\" part of the control center as the theme manager handles settings which belong to the modules of this section. But I do not really know how feasible this would be.\n\nHaving all settings related to look'n feel hidden behind it would oversimplify the configuration: selecting a new theme will change your desktop settings (background, panel appearance) as well as application settings (style, colors, windeco) and other settings (sound notifications?). Most of the time, you only want to change your background picture that's why I don't think it should be something hidden behind a theme. A theme manager should be a convenient way to import/export settings (for example in kde-look.org) but is not what should be used to modify some parts of your settings. I am not sure for example that there is a reason why people who like a given style would like the same background. "
    author: "Benoit W."
  - subject: "Re: The New Theme Manager"
    date: 2003-12-22
    body: "the current theme manager is broken. We shall rather think about a simplified Control Center and a Professional control center for finetuning.\nthe simplified Control Center could just be limited on theme selection while with the professional control center you can finetune it."
    author: "Andr\u00e9, FFII"
  - subject: "Re: The New Theme Manager"
    date: 2003-12-22
    body: "Exactly what I'm doing in my Theme Manager - it aims at being very simple and provide just selecting, creating and deleting themes. One can always fine tune the settings afterwards in the respective kcm modules."
    author: "Lukas Tinkl"
  - subject: "Re: The New Theme Manager"
    date: 2003-12-22
    body: "Good question - I've been asking myself the same one as well :) I'm still unsure how the whole LookNFeel branch will look like but my intent is to provide the user with a simple tool to set the overall visual appearance in one module and fine tune it if necessary. "
    author: "Lukas Tinkl"
  - subject: "Theme Manager"
    date: 2003-12-22
    body: "The theme manager looks absolutely fantastic already. If KDE can harnass its power, while at the same time providing simplicity where it is required, it will knock seven bells out of anything else - and I include Windows in that assessment. Don't be fooled. Achieving this is extremely and unbelievably difficult, and Ximian has only got 'usability' by taking a chainsaw to the desktop environment. This is not the way forward.\n\nIt also amuses me that many people compare a completely free desktop, KDE, to a commercialised Gnome - Ximian Desktop. In the long-run I think this will help KDE immensely. What was that about GNU/Free Software again?\n\nNo wonder Ximian are trying to pressure everyone into dropping KDE!\n"
    author: "David"
  - subject: "Re: Theme Manager"
    date: 2003-12-22
    body: ">\n>\nAchieving this is extremely and unbelievably difficult, and Ximian has only got 'usability' by taking a chainsaw to the desktop environment. This is not the way forward.\n>\n>\n\nI too believe it is very difficult, more than people imagine. However, it will probably even more difficult if one isn't prepared to use a chainsaw - doing two contradictory things at the same time lessens the chance of success, so the key thing is actually just to accept it as the way to go forward. I would create a much more productive mindset, because then the task can become \"what can we cut today\" rather than \"what can save today\".  \n\nI think options are overrated anyway. If the default ones are choosen wisely, one will get accustomed to their loss...\n\n"
    author: "will"
  - subject: "Re: Theme Manager"
    date: 2003-12-22
    body: "> If the default ones are choosen wisely, one will get \n> accustomed to their loss\n\nif you cut of your legs, you will eventually get accustomed to their loss. most places are wheel-chair accessable these days, after all."
    author: "Aaron J. Seigo"
  - subject: "Re: Theme Manager"
    date: 2003-12-23
    body: "\"I too believe it is very difficult, more than people imagine. However, it will probably even more difficult if one isn't prepared to use a chainsaw - doing two contradictory things at the same time lessens the chance of success, so the key thing is actually just to accept it as the way to go forward.\"\n\nI quite agree. It's getting that balance right :)."
    author: "David"
  - subject: "Re: Theme Manager"
    date: 2003-12-22
    body: "it's not so much difficult, as it is just simply time consuming. which isn't to say that it's obvious how to get everything \"just so\", but implementing things so they are \"just so\" tends to take the bulk of the time. it's easy to make simple systems, or complex systems that rely completely on the user for direction ... but writing software that is even somewhat non-simple that has some \"smarts\" built in, such that \"the right thing\" usually happens in response to the user's actions is time consuming. \n\nobviously, trimming out silly micro-configurations that are truly unecessary is a good thing; trimming out configurations that mask bugs is a very good thing; moving the truly esoteric or technical out of the main GUI can be an OK approach when used with good measure... but even then we are left with a large number of possibilities."
    author: "Aaron J. Seigo"
  - subject: "Re: Theme Manager"
    date: 2003-12-23
    body: "Ximian belongs to Novell and Novell's new child SUSE is focussed on KDE. The pressure from Suse's market will rather force ximian to support qt than suse to adopt Gnome."
    author: "maga"
  - subject: "sweet"
    date: 2003-12-22
    body: "the theme manager looks pretty sweet =) integrate the detailed style panels into it in a non-obtrusive way and we'll be golden .. great work =)"
    author: "Aaron J. Seigo"
  - subject: "Re: sweet"
    date: 2003-12-23
    body: "Yeah, I've been thinking along the same lines but still don't know how to do a good UI of the detail sections. We'll see, meanwhile I'm importing my code into kdenonbeta in a few minutes."
    author: "Lukas Tinkl"
  - subject: "What I would like to see"
    date: 2003-12-23
    body: "is something that can apply a KDE theme, and the matching GTK theme at the same time.  Also, if you could click on a link in kde-look.org and have it apply the theme for you, that would be nice."
    author: "Jonathan Bryce"
  - subject: "Re: What I would like to see"
    date: 2003-12-23
    body: "Perhaps freedesktop forgot theme engine config unification"
    author: "maga"
  - subject: "KDE 3.3"
    date: 2003-12-23
    body: "Wird Hbasic in KDE 3.3 integriert?"
    author: "bala"
  - subject: "Re: KDE 3.3"
    date: 2003-12-23
    body: "No. From where do you got this idea?"
    author: "Anonymous"
  - subject: "Re: KDE 3.3"
    date: 2003-12-23
    body: "Hum? Don't know.\n\nI was told that Kbasic, the basic targeted to become a KDE Basic, was dead and Hbasic (http://hbasic.sf.net) was to fill the gap (version 0.99g). I think Gambas is somehow similar but less mature. \n\nHowever both seem to be qt based but no fully integrated KDE application yet. "
    author: "bala"
  - subject: "Re: KDE 3.3"
    date: 2003-12-23
    body: "No such integration is planned for KDE 3.3 as far as I know, but a few enthousiastic people and some days off can make all the difference of course ;-)"
    author: "Waldo Bastian"
  - subject: "Re: KDE 3.3"
    date: 2004-01-02
    body: "I'd say Gambas and HBasic are kind of neck and neck as far as maturity goes.  Though Gambas is only at 0.80, the current version is very much a release candidate for 1.0.  (Being in charge of generating its documentation, I have to say I'm the primary holdup at this point ;) ) \n\nBoth are doing some really innovative things lately though (like Gambas with its new VB compatibility component and HBasic with its ability to write subroutines in Perl) so I hope they both find a niche.  If there's a lot of press coverage when one or both of them reaches 1.0, it could really provide a shot in the arm as far as corporate acceptance of KDE goes (assuming people grasp the connection between Qt and KDE.)  The GNOME guys don't seem quite as interested in the VB type of software and there is A LOT of it out there in the real enterprise.\n"
    author: "raindog"
  - subject: "Mail staus"
    date: 2003-12-23
    body: "Is there an evolution or Kmail view (a kind of karamba applet9 that shows an overview about your mail status?"
    author: "bala"
  - subject: "Re: Mail staus"
    date: 2003-12-23
    body: "see kmail/kontact in kde 3.2.."
    author: "anon"
  - subject: "theme manager"
    date: 2003-12-23
    body: "As I recall the GNOMEs were trying to standardize their theme format on FDo.  Are the new KDE theme packages compatible with GNOME themes?"
    author: "anonymous"
  - subject: "Re: theme manager"
    date: 2003-12-23
    body: "No, as I haven't seen a standard yet - my plan is to indeed standardize the format on FDO once it's finished and stabilized."
    author: "Lukas Tinkl"
  - subject: "Feature request "
    date: 2003-12-23
    body: "Feature request for the theme manager.\n\nPlease make Themes use a special Extension for Filenames.\nlike .kdetheme or so... and than i can invoke the theme-editor by \ndouble klicking on the icon , oder with the right mouse button choose some actions like install , preview , or remove (if its installed) .\n\n\nwhat about this ?\n\n\nchris"
    author: "chris"
  - subject: "Re: Feature request "
    date: 2003-12-31
    body: "Already done, it uses \"kth\" as the extension. Did you actually check before asking? :)"
    author: "Lukas Tinkl"
  - subject: "Re: Feature request "
    date: 2004-01-01
    body: "all stuff i downloaded from kdelook.org had tar.gz extensions, so i thought it was not implemented.\n\n"
    author: "chris"
---
Datschge pointed me at some <a href="http://www.kde-look.org/usermanager/search.php?username=kol">interesting work by Rohit "kol" Kaul</a> on <a href="http://www.kde-look.org/">KDE-Look.org</a>.  Complementing the <A href="http://kde.openoffice.org/">recent efforts</a> at <A href="http://www.koffice.org/">KDE'fying</a> <A href="http://www.openoffice.org/">OpenOffice.org</a> is <a href="http://www.kde-look.org/content/show.php?content=7131">an icon theme generator</a> as well as an <a href="http://www.kde-look.org/content/show.php?content=8529">accompanying static theme</a>.  The screenshots are very interesting, if anything.  In addition, kol has done <A href="http://www.kde-look.org/content/show.php?content=8950">the same work</a> on <a href="http://www.ximian.com/products/evolution/">Evolution</a> in a nice little showdown with <a href="http://www.kontact.org/">Kontact</a>. Also, who needs <a href="http://www.kde-look.org/content/show.php?content=8685">Keramikzilla</a> when we got <A href="http://www.konqueror.org/">Konqueror</a>?  But there you go...  we got it all.  By the way, you might want to browse <a href="http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/stuff/sodipodi-screenshots/">a few more screenshots</a> of <a href="http://www.sodipodi.com/">Sodipodi</a> flaunting <a href="http://dot.kde.org/1071748404/">KDE integration</a> -- some <A href="http://www.koffice.org/karbon/">Karbon</a> shots in there too.  The pieces of the <A href="http://desktop.kdenews.org/strategy.html#gtk">integration and assimilation</a> puzzle are falling into place quite nicely!

<!--break-->
<p>
Finally, the news you have all been waiting for:  <A href="http://developer.kde.org/development-versions/kde-3.3-features.html">KDE 3.3</a> will feature a brand new and long-awaited theme manager.  Lukas Tinkl has <a href="http://ktown.kde.org/~lukas/kthememanager/">a nice little preview page</a> complete with screenshots and an initial version 0.1 available for download.   
<p>
Your suggestions and input are crucial, since Lukas is looking forward to adding new features to the KDE Theme Manager while aiming to keep the UI as simple as possible.  Also note that this is a completely new take on the quick-and-dirty legacy theme manager that was present in KDE 2.x and therefore does not preserve backwards compatibility.   
