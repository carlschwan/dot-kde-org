---
title: "KDE-CVS-Digest for May 16, 2003"
date:    2003-05-17
authors:
  - "dkite"
slug:    kde-cvs-digest-may-16-2003
comments:
  - subject: "Thanks Derek !"
    date: 2003-05-17
    body: ":-)"
    author: "Fran\u00e7ois MASUREL"
  - subject: "Kroupware/Kontact"
    date: 2003-05-17
    body: "Thanks Derek. It's good to see some more about Kroupware/Kontact. I haven't heard about it in a while."
    author: "Paul Kucher"
  - subject: "Re: Kroupware/Kontact"
    date: 2003-05-19
    body: "I *hate* the names kroupware and kontact.  They're horrible!\n\nI'm guessing these names have an infamous future ahead of them."
    author: "Tyreth"
  - subject: "Coool thanks!"
    date: 2003-05-17
    body: "But, I don't understand something on the mobile tool, why would it only display cameras supported by Gphoto2. Why not USB ones too? Also waht's the point of gphoto? ALl cameras now seem to be USB and Linux supports that, why is there a need for such an application."
    author: "mario"
  - subject: "Re: Coool thanks!"
    date: 2003-05-17
    body: "Gphoto supports USB cameras as well. I know, I just bought one today.\n\nThere is more to supporting a piece of hardware than saying it supports USB. There is a protocol over the top of USB that needs to be handled. Gphoto makes the various usb camera protocols, plus firewire ones, serial and parallel ones, etc have exactly the same interface so they can all be dealt with in the same way."
    author: "GldnBlls"
  - subject: "Re: Coool thanks!"
    date: 2003-05-17
    body: "How do you access the data? Do you have to mount the device, and read it?\n\nFrom what I understand, you could read and write a camera, palm, zaurus, ipaq and cell phones the same way. Kpilot currently imports and exports addresses and todo's, etc. into kontact. What if there is a device similar to the palm, but different? One would have to write, run, maintain, etc an application similar to kpilot. With this framework, one would have to write the synchronization routines, and translate the data from and to the kde data structures. Much less work, and the common tasks would be more likely maintained.\n\nIt is another level of abstraction.\n\nDerek\n\n"
    author: "Derek Kite"
  - subject: "Re: Coool thanks!"
    date: 2003-05-17
    body: "USB is a physical transport method, not a wire protocol unto itself. This is why drivers in top of USB are needed, such as gphoto for cameras, HID for keyboards, and mousedev for mice. "
    author: "lit"
  - subject: "Kate?"
    date: 2003-05-17
    body: "With Kate in KDE 3.1 I got problems to dock the file selction inside Kate. \nI also don't get what the mayor difference of Kwirte and Kate was."
    author: "Hakenuk"
  - subject: "Re: Kate?"
    date: 2003-05-17
    body: "If you undocked the file lis selection in Kate you can redock is by dragging the second header line (the one including those tiny dock and close buttons) back into the main GUI.\n\nKwrite is SDI (single document interface) while Kate is MDI (multi documents interface). I hope Kate will be obsoleted by a MDI-enabling Kwin sometime at which point Kwin will be able to allow MDI in any SDI app depending whatever the user prefers."
    author: "Datschge"
  - subject: "Re: Kate?"
    date: 2003-05-17
    body: "It would actually be much better to have MDI within the apps themselves. Not only does it save memory, but it allows better integration of KDE apps with non-kwin window managers. Please no more EWMH-extentions/hacks!"
    author: "lit"
  - subject: "Re: Kate?"
    date: 2003-05-18
    body: "Uh? What leads you to think that MDI in every single app is saving memory compared to a global solution (which would additionally ensure consistent MDI and allow the integration of non-KDE apps)? Besides that there are plans to make the resulting WM based MDI mode an open NET WM standard which could be easily taken over by any other WM. And it won't come before KDE 3.3/4.0 anyway."
    author: "Datschge"
  - subject: "kolab"
    date: 2003-05-17
    body: "I've been at CeBit 2003 and visited the Kolab-Booth, where Martin Konold explained its concept and showed some people how it worked.\n\nWhen I asked him about the release-date of 1.0, he said \"very soon\". Since \"very soon\" can mean anything between tomorrow and a couple years, I asked him when 1.0 will be released :)\n\n\"About two weeks after the CeBit\", he said.\n\nWell, that is quite some time ago, and you really don't hear too much about kolab anymore. So, does anybody know what has happened?"
    author: "me"
  - subject: "Re: kolab"
    date: 2003-05-17
    body: "Kolab (which is the server) is indeed pretty much ready. You can get the current release candidate 1 at the known place (being http://kroupware.kde.org/howto-kolab.html). I think the main reason they didn't declare it a 1.0 so far is the lack of a ready to use Kolab client (which is what Kontact will be, a stable but feature incomplete version is available at http://kontact.org/latest.html)."
    author: "Datschge"
  - subject: "Re: kolab"
    date: 2003-05-17
    body: "This is wrong. The Kolab Client is also available at kolab.kde.org. It's basically a groupware enanbled kmail which integrates a special version of korganizer. This is the official \"counterpart\" to the Kolab Server. Yet this is not the cleanest way of course and also the reason why the final target is Kontact, which will have a cleaner and more generic design.\n\nJust to clarify again: Kolab Client != Kontact.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: kolab"
    date: 2003-05-18
    body: "I stand corrected, thanks for the info. =)"
    author: "Datschge"
  - subject: "KOffice"
    date: 2003-05-17
    body: "KOffice 1.3 will really be interesting to try.\nWhy don't they name it Koffice 2? Seems like they are\nsqueezing out lots of bugs, and 1.2.1 is already quite good."
    author: "KDe User"
  - subject: "Re: KOffice"
    date: 2003-05-17
    body: "Why should we, just because some marketing reasons?\n\nI really think 1.2 was a bigger step than 1.3 is.\nSo I'm fine with 1.3."
    author: "Philipp"
  - subject: "Re: KOffice"
    date: 2003-05-17
    body: "As I have already answered such a question, please look at:\nhttp://lists.kde.org/?l=koffice-devel&m=104256641027895&w=2\n\nHave a nice day/evening/night!"
    author: "Nicolas GOUTTE"
  - subject: "Re: KOffice"
    date: 2003-05-19
    body: "Watched too much Truman-Story? ;-)"
    author: "Michael"
  - subject: "Thanks Russell and Juergen!!"
    date: 2003-05-18
    body: "Thank you Russell Miller and Juergen Appel !\n"
    author: "thr0d ps1t"
  - subject: "test"
    date: 2003-05-31
    body: "im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         "
    author: "test"
  - subject: "test"
    date: 2003-05-31
    body: "im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         \n\n\n\nim sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         \nsalut\ncomment\nca\nva\noui\n\noui\ntest\ndesole\nmerci\nim sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         im sorry to post this here. but i really wonder about PRE messages on this site, because i have problem with mine. feel free to erase this message as soon as you read it :) thanks....         "
    author: "test"
---
In <a href="http://members.shaw.ca/dkite/may162003.html">this week's issue</a> of KDE-CVS-Digest, read about the beginnings of a mobile device framework and the reworking of <a href="http://kdepim.kde.org/">KMail</a> 
groupware functionality.   Also, bug fixes in <a href="http://kate.kde.org/">Kate</a>, <a href="http://printing.kde.org/">KDE Print</a>, 
<a href="http://www.konqueror.org">Konqueror</a>, KWin, <a href="http://www.koffice.org/kspread">KSpread</a>, 
<a href="http://kopete.kde.org/">Kopete</a> and many others.
<!--break-->
