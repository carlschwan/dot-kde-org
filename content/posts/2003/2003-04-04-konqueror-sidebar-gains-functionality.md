---
title: "Konqueror Sidebar Gains Functionality"
date:    2003-04-04
authors:
  - "numanee"
slug:    konqueror-sidebar-gains-functionality
comments:
  - subject: "Vertical text"
    date: 2003-04-04
    body: "That looks cool. How come when KDE writes text in a vertical (rotated 90 degrees) manner, it isn't pretty and anti-aliased like normal text is? The Kate sidebars are the same way..."
    author: "AC"
  - subject: "Re: Vertical text"
    date: 2003-04-04
    body: "It's the same code actually.  Konqueror shares that widget with kate.  You should ask Joseph Wenninger, the author of KMultiTabBar.\n"
    author: "George Staikos"
  - subject: "Re: Vertical text"
    date: 2003-04-04
    body: "And sub-pixel-anti-aliasing must be disabled automatically because\nit relys on the physical left-to-right RGB order of the pixels on an LCD screen.\nOtherwise people with sub-pixel-aliasing on would see coloured edges on each\nletter. Does X support vertical anti-aliasing at all?\n\n"
    author: "Martin"
  - subject: "Re: Vertical text"
    date: 2003-04-04
    body: "Current antialiasing settings allow RGB, BGR and vertical versions of those two (VRGB, VBGR)- I see no problem in dettecting this setting and make fontconfig/Xft render text with apropriate setting at any case when text is to be drawn rotated 90 degrees. Anyway INAFXD (I am not a fontconfig/Xft developer)"
    author: "Willy"
  - subject: "Re: Vertical text"
    date: 2003-04-04
    body: "Just take a look at Gideon's IDEAl interface. The vertical text is anti-aliased.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Vertical text"
    date: 2003-04-28
    body: "no way, go with .vrml like this lol"
    author: "warm"
  - subject: "Re: Vertical text"
    date: 2003-04-04
    body: "I don't think the vertical tab is very user-friendly at all.  Space-efficient, sure, but quite cryptic at first blush.  Probably too much functionality there by default."
    author: "Navindra Umanee"
  - subject: "Re: Vertical text"
    date: 2003-04-04
    body: "I agree. Actually, I've always disliked konqueror's sidebar because of the sidewaysness of it, but more because of the appearance the sidewaysness creates. The huge thick bar separating the sidebar from the rest of konqueror is, IMHO, absolutely fugly. It sticks out like a sore thumb, and makes what otherwise is a beautiful application (when the sidebar is off) look cludgey and archaic. Don't get me wrong, though -- I still prefer konqueror over any other browser, I just don't like the sidebar's appearance."
    author: "optikSmoke"
  - subject: "Re: Vertical text"
    date: 2003-04-04
    body: "> The huge thick bar separating the sidebar from the rest of konqueror is, IMHO, absolutely fugly.\n\nI'd word that a little more diplomatically (aka Blixed). ;)\n\nBesides that I have to agree, Konqueror's \"Navigation Panel\" is well on the way delivering direct access solutions to every problem you did (and didn't =P) think of. This as a feature on its own is very welcome, but I see less and less use for it in one program's window where it most of the time just takes away space for the content while still not having enough space to be really useful itself (aka crowded). Add to this that it might well be open in multiple of your multiple Konqueror windows and you can't resist calling it a bloath of view space.\n\nSo my proposal would be: Considering that the \"Navigation Panel\" is more an more becoming a system tool rather than a sidebar for Konqueror, how about making it independent of Konqueror and show it as a part of the desktop's left (or right) border instead? Then you can access all your content and services all the time, and Konqueror can focus on itself again. =)\n\n(Should I log that as wish at buggy?)"
    author: "Datschge"
  - subject: "Re: Vertical text"
    date: 2003-04-05
    body: ">The huge thick bar separating the sidebar from the rest of konqueror is, IMHO, >absolutely fugly\n\nIf you meant \"ugly\", I completely agree ;-)\n\nI wonder if this things is themeable. it looks horrible indeed. That's why I never enable the sidebar. It's like some disturbing bug sticking to the side of konqueror :-("
    author: "uga"
  - subject: "fugly = f***ing ugly (EOM)"
    date: 2003-04-05
    body: " "
    author: "pottymouth"
  - subject: "Re: fugly = f***ing ugly (EOM)"
    date: 2003-04-05
    body: "Ooops, ... thanks ;-)"
    author: "uga"
  - subject: "Re: Vertical text"
    date: 2003-04-06
    body: "just change the location of the buttons to the left, remove the border and use highlighting instread, like in idea/gideon/etc."
    author: "cylab"
  - subject: "Cool"
    date: 2003-04-04
    body: "That is nice, it could be used for a number of things depending on how functional it is.  Eg, directory specific scripts and links/buttons, information on directories not normally provided, etc.\n\nPresuming that we can make our own html/whatever based sidebar entries."
    author: "Tyreth"
  - subject: "Re: Cool"
    date: 2003-04-04
    body: "You can make your own, but they have to behave in the way that Netscape 6/Mozilla sidebar entries do.  Note that we don't support XUL.  Also, form POSTs don't work yet in Konqi due to a KHTML limitation.  I hope to work around that shortly.\n\nIf a web sidebar entry doesn't suit your needs, you can always make a konq sidebar plugin with C++ too.  It's easy to write a plugin.  Have a look at the source code for the web sidebar model for an example.\n"
    author: "George Staikos"
  - subject: "Standard devices dynamically added to sidebar"
    date: 2003-04-04
    body: "Wouldn't it be of advantage to the KDE desktop to have some default default devices listed in the sidebar, like Floppy, CD-Rom, NFS mounts, samba shares, ...?\nOf course, dynamically added as KDE detects these devices at startup.\nThis way one would not have to know where Distribution put their mount points.\n(SuSE in /media , Mandrake in /mnt ... )\n\nJust a suggestion.\n\nNorbert\n"
    author: "norbert"
  - subject: "Re: Standard devices dynamically added to sidebar"
    date: 2003-04-04
    body: "It is implimented at least in mine.  :-)  I have no idea when they added that fuctionablility, but I know its in it.  Here is a quick screenshot from mine: <a href=\"http://www.geocities.com/bass_46563/sidebar.html\">sidebar.html</a>\n<p>\nOtherwise, I just updated my cvs and can't wait to recompile!!!"
    author: "Wait no longer!"
  - subject: "Re: Standard devices dynamically added to sidebar"
    date: 2003-04-04
    body: "that's not what I meant. I thought of icons showing floppy, cdrom, etc directly on the sidebar or maybe even as part of the directory structure.\nOr could i get to confusing?\nI simply believe that people don't think of floppy drives as a service. It shold rather be part of your filesystem. Some newer distribution already took on that idea and used a similar directory layout as known from windows explorer.\nWhat do you think? Couldn't it improve proctivity?"
    author: "norbert"
  - subject: "Re: Standard devices dynamically added to sidebar"
    date: 2003-04-04
    body: "Look at my question and to my answert to it below.\nYes it is possible to do it, and yes it's very easy :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Standard devices dynamically added to sidebar"
    date: 2003-04-04
    body: "Ah I see, that would be a nice feature to have in the sidebar.\n\nP.s. sorry about the name \"Wait No Longer!\" I rushed through the post and ment to put it in the  \"Title\" field. :P"
    author: "Brendan Orr"
  - subject: "Re: Standard devices dynamically added to sidebar"
    date: 2003-04-07
    body: "Nice suggestion; I do keep icons for removable media on my desktop, would be nice to have them in the sidebar like that as well.\n\nOn a slightly related note - has anyone gotten konqs FS mount feature to recognize and correctly handle encrypted filesystems? Say I have /mnt/secret in my fstab, mapped to an encrypted loopback filesystem. When mounting it from the console, I'm asked for a passphrase before the filesystem is decrypted and mounted to /mnt/secret. When setting up a .desktop file for it, it seems to just hang indefinitely when double-clicked, presumably waiting for the \"mount\" call to finish, while \"mount\" is waiting for a passphrase...\n\nA passphrase dialog would be nice =)"
    author: "Erik G."
  - subject: "Very nice!"
    date: 2003-04-04
    body: "I'm just developing a web based bookmark sidebar to store your bookmarks on a server and access them from every sidebar enabled browser on earth. (Mozilla, Opera, and now Konqueror). - So I was just waiting for this to come! Great work!\nI have just one question though: is target=\"_content\" on a-tags usable, as in Mozilla? If not, how would I reproduce the same functionality?\n\nCan't await to use it daily!"
    author: "Shyru"
  - subject: "Re: Very nice!"
    date: 2003-04-04
    body: "Yes, we use the _content hack too...  (check http://dot.kde.org/static/bar )"
    author: "Navindra Umanee"
  - subject: "Re: Very nice!"
    date: 2003-04-04
    body: "> Yes, we use the _content hack\ninteresting, so konqeror bars will be compatible with NS and IE bars, however there is another cool hack to add - the _search hack. In one site i don't actually remember which was it I found that when you run searches the search results are displaying in the search side bar (MSIE), it was done with target=\"_search\" or window.open(\"_search\", \"url.com\", \"width=160\"). A very cool feature to have a search sidebar in konqui too which opens up on demand from sites."
    author: "Anton Velev"
  - subject: "Re: Very nice!"
    date: 2003-04-07
    body: "Have a look at ol'bookmarks (olbookmarks.sourceforge.net), it is an online bookmark manager and it supports Mozilla sidebar. Maybe you could adapt it to Konqueror..."
    author: "Federico Cozzi"
  - subject: "Neat..but..."
    date: 2003-04-04
    body: "Wouldn't it be cool if there was a button that appeared when you have multiple tabs open that let you close the tab in the front like Mozilla does? Something like isn't as sexy as this but it's very useful. :)\n\nThis looks neat though...I'm not knocking it. Konq is really coming along quite nicely.\n"
    author: "Ben Rosenberg"
  - subject: "Yup"
    date: 2003-04-04
    body: "Yes I too think this would be cool. I would also point out that you can add buttons to the toolbar from Settings->Configure Toolbar, which I find convenient."
    author: "richard"
  - subject: "Re: Yup"
    date: 2003-04-05
    body: "That button actually exists. You can add it to your toolbar by simple right clicking on the toolbar and selecting \"configure toolbars\".\nSee the attached image for how the close tab and even the new tab buttons look alike.\n\n- Holle"
    author: "Holle"
  - subject: "cool feature"
    date: 2003-04-04
    body: "Hah, this is simply too cool, Just adding all types of news to my Konqueror Sidepanel now (running CVS) but I like to suggest some sort of Grouping functionality would make it easier to differ between normal Bookmarks, Home, Printer and the Sidepanel news thingies. Maybe a separator would do the trick to distinguish and separate them."
    author: "oGALAXYo"
  - subject: "Re: cool feature"
    date: 2003-04-04
    body: "Either that, or a complete redesign.  The sidebar definitely needs some organization...  it's too overloaded."
    author: "Navindra Umanee"
  - subject: "Re: cool feature"
    date: 2003-04-04
    body: "Yes, some things in the side bar just don't make any sense...\nJust click on the history tab for instance. Now you want to clear the history, what do you do? Right click it you say... so lets see... what do we have here...\n\nOpen in new window\nOpen in background tab\nOpen in new tab\n----------------------------\nUndo\nCut\nCopy\n----------------------------\nRename\nMove to Trash\nDelete \nAdd to bookmarks\n----------------------------\nOpen with\nPreview with Embedded Advanced Editor\n----------------------------\nCopy to\nMove to\n\nUsability Hell huh? So how do you clear the history? Right click *one of the entries*.\n\nNew window\n----------------\nRemove Entry\nClear History\n----------------\nSort\n----------------\nPreferencies\n\nOk so there it is, you get the generic history options as well as the specific history entry ones, by right clicking a specific entry... \n\n(Yes I think I'll bug report this...)\njadrian\n\n\n"
    author: "jadrian"
  - subject: "Nice feature "
    date: 2003-04-04
    body: "Couldn't it be implemented in KMail as well? Look at Evolution with its rdf-feeds. Would be really cool to have it in KMail or Kontact ...\n\nAnyway .. cool feature \n\nCheers'\n\nFab"
    author: "Fab"
  - subject: "Re: Nice feature "
    date: 2003-04-04
    body: "What has an email client to do with a news feed? Adding it to Kontact would probably be okay but KMail will always be an email client and no bloated all-in-one app.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Nice feature "
    date: 2003-04-04
    body: "you are propably right about KMail, it is more a feature for Kontact. But I found this feature in Evolution and I think it justifies to have it there. \n\nThink of it, a lot of people fire up their mailclient and also their browser. One for checking their mail and the other for checking headlines like The Dot :-) \n\nSo why not combine those 2 things? Yes I find this a nice feature to have although not needed.\n\ngrtz'\n\nFab"
    author: "Fab"
  - subject: "Re: Nice feature "
    date: 2003-04-05
    body: "If that's going to be anywhere, it should be in Kontact. Are Konqueror plugins usable outside konqi, like KParts are?"
    author: "Blue"
  - subject: "But why use it?"
    date: 2003-04-04
    body: "... for IMHO running knewsticker on the panel eats up much less valuable desktop space?\n\n\nyea,\nblame me for using a 15\" tft screen anyways ;)\n\n\nbye,\n    [L]"
    author: "[Lemmy]"
  - subject: "Re: But why use it?"
    date: 2003-04-04
    body: "This is why we are provideing Konqi side panels and even a generic DCOPRSS service so you could use thinkings like a screen saver or karamba to display the data.  The reason why we provided an RSS service is so that you can use a central area do access all of your RSS data.  Imagine a poor webserver if you had 10 konqi sidebars open.  We solved that problem by only forwarding a query when its needed.\n\nAlso Frerich Raabe and I just finished up an XMLRPC service wrapper so that you can search and add feeds from syndic8.com transparently.  Again becuse we use a dcop service we minimise the hit on a remote service/webserver because we can do things like cacheing and limiting.\n\nDCOPRSS is something cool becuase anyone can have what they want with minimal effort and code.  Even from bash:\n\n#!/bin/sh\necho \"Articles:\"\nDOCS=`dcop rssservice RSSService list`\nfor DOC in $DOCS\ndo\n\tDOCREF=`dcop rssservice RSSService document \"$DOC\"`\n\tTITLE=`dcop $DOCREF title`\n\tCNT=`dcop $DOCREF count`\n\techo $TITLE - $CNT\n\techo \"------------------------------------\"\n\twhile let \"CNT >0\"\n\tdo\n\t\tlet \"CNT=CNT-1\"\n\t\tART=`dcop $DOCREF article $CNT`\n\t\tTEXT=`dcop $ART title`\n\t\techo \"$CNT\t$TEXT\"\n\tdone\ndone\n\nWill output:\n[6.63}-{geiseri@rae:~/kdebuild/cvs/kdenonbeta/dcopservices/rss> sh test.sh\nArticles:\nKDE Dot News - 10\n------------------------------------\n9       Web Shortcut Goodness: KDE Support for Feedster\n8       KDE 3.1.1: It's Not Odd at All!\n7       KDE-CVS-Digest for March 21, 2003\n6       XFree86 and KDE\n5       KDE Accessibility 1.0 is Here + Interview\n4       NewsFactor: The Suite Strategy of Konquering the Desktop\n3       KDE-CVS-Digest for March 28, 2003\n2       Competition: Design a New Logo for KDE.org\n1       George Staikos: A Quick Cost Analysis of Qt vs GTK\n0       Konqueror Sidebar Gains Functionality\nfreshmeat.net - 10\n------------------------------------\n9       crip 3.4\n8       Open Application Server 0.1\n7       hdup 1.6.9 (Stable)\n6       swsusp 2.4 Beta19-20 (2.4 Development)\n5       swsusp 2.5 Beta19-20 (2.5 Development)\n4       Jay's Iptables Firewall 0.9.94 (Development)\n3       web2ldap 0.11.17\n2       MailScanner 4.14\n1       Perl HL7 Toolkit 0.63\n0       JTAG Tools 0.3.2\n\nCool Huh?\n\nCheers \n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: But why use it?"
    date: 2003-04-04
    body: "The sidebar definitely does not use much space.  It hides away or restores at a single click!  Actually I personally don't use the newsticker anymore because of the amount of panel space that is lost, and having an extension bar is painful in Xinerama.\n\nSpace is really not an issue."
    author: "George Staikos"
  - subject: "CNN.com guidance"
    date: 2003-04-04
    body: "Where does one find that CNN Sidepanel news ? I was searching on CNN.com for various minutes now and wasn't able to find it. Someone guide me!"
    author: "A. A."
  - subject: "Re: CNN.com guidance"
    date: 2003-04-04
    body: "You can find it here:  <a href=\"http://www.cnn.com/browsers/ns6/\">http://www.cnn.com/browsers/ns6/</a>"
    author: "Navindra Umanee"
  - subject: "Re: CNN.com guidance"
    date: 2003-04-04
    body: "Thank you very much!"
    author: "A.A."
  - subject: "Re: CNN.com guidance"
    date: 2003-04-04
    body: "If I add the cnn sidebar, and then click on the added sidebar konqueror crashes on me. Did you see this, too?"
    author: "Michael Jahn"
  - subject: "Re: CNN.com guidance"
    date: 2003-04-04
    body: "Make sure you have a clean installation.  If you do, then file a bug report with the backtrace (with debugging enabled!) please.\n"
    author: "George Staikos"
  - subject: "Re: CNN.com guidance"
    date: 2003-04-04
    body: "Nothing crashes here."
    author: "A. A."
  - subject: "Re: CNN.com guidance"
    date: 2005-01-24
    body: "I was watching Carlos interview with the actress on Sunday on CNN, and was impressed with his's interveiwing with Cresflo Dollar of Atlanta, Ga. I felt both the interveiwing with all the guest were faberlous. I am a new author of a new book entitled \"Divorce Before Marriage\" found on Amazon.com. And Barnes and Nobles, and Xlibris.com. I heard the guest who is an Mexican speak about divorce. Well, I feel my book is a new tool that will help millions to gather a wealth of understanding about what is meant to know the standards predetermined for a life of happiness. My wife and I have been married for 36 years after dating for only six months. We have found adults childrens, and eight grand-children. I think Carols report is gonna be a great success. He has the skills, and God given talent to move forward in the cereer planned for him. I want to know what to do about an interview with Him concerning this new book that maybe can help millions suffering especially in the African communities? The book attacks many issues that comes up in our daily lives, such as sex, divorce, alternative relationships,dating etc....You may cantact me by e-mail or call me after 5pm daily.\nSincerely,\nRev.D.W.Jordan/author."
    author: "D.W. Jordan"
  - subject: "Functionality overlapping ?"
    date: 2003-04-04
    body: "I'm not sure I see the point of having this RSS-like side panel for news sites (the first feature described above). It doesn't seem to offer any advantage over RSS..."
    author: "Guillaume Laurent"
  - subject: "Re: Functionality overlapping ?"
    date: 2003-04-04
    body: "Indeed, there is a functionality overlap, although the HTML one is more flexible (it can be any HTML) and user-friendly (click button.. tab gets added) than the RDF  one.  On the flip side, RDF works everywhere.\n\nI think there are plans to integrate the two applets.  George can probably give more details here."
    author: "Navindra Umanee"
  - subject: "Re: Functionality overlapping ?"
    date: 2003-04-04
    body: "This feature was originally implemented by Mozilla and Netscape 6, and as Navindra says, it's really easy to use.  Just click and it adds to the sidebar.  The sites out there have supported it for a long time now.  If you don't want to use it, you'll never know it's there.  If you do, you'll be quite happy.  Personally, I'm addicted to it already. :)"
    author: "George Staikos"
  - subject: "Just say no to clutter"
    date: 2003-04-04
    body: "People are cheering this as a 'Cool' feature, but adding all cool features of the month to the main GUI is just a bad UI design. Please have it turned off by default."
    author: "Fredrik C"
  - subject: "Re: Just say no to clutter"
    date: 2003-04-04
    body: "1) It's in the sidebar not the main GUI.\n\n2) It's not there unless you use it.\n\n3) Go KDE!!!  Go Konqi!!!"
    author: "Anonymous"
  - subject: "Re: Just say no to clutter"
    date: 2003-04-04
    body: "> Please have it turned off by default.\n\nYou don't even notice a difference between before and after. I think you should read the Headliner and the comment of this Topic again and look at the screenshots about what it does because I assume you are a bit missinformed. You are the person on your system and you decide wether you want to add these Sidepanel news or not it doesn't add stuff automatically. You have to click on the 'Add to my Sidepanel' (or how they things are labeled) on the Webpages and you get promted with a dialog wether you wish to add it or not."
    author: "oGALAXYo"
  - subject: "Re: Just say no to clutter"
    date: 2003-04-05
    body: "If you want to remove something from your sidebar, just right-click it and click 'Remove' - just as if it was a panel button. If you are a KDE user it should not be very difficult to figure this out. Besides, the sidebar doesn't have very many buttons.\n\nNow, I just realized that the right-click-and-remove trick does not work on toolbars - the toolbar menu only lets me use the Configure Toolbars dialog . . .\n\nThe other thing, the more features there are in KDE, the longer it takes for me to compile it from source!"
    author: "ThreeFarthingStone"
  - subject: "Re: Just say no to clutter"
    date: 2003-04-05
    body: "GCC got slower at compiling C++, I think that's what you're experiencing."
    author: "Datschge"
  - subject: "good, but..."
    date: 2003-04-04
    body: "I wish that instead of doing things like that, they could return the classical toolbar option. I miss that A LOT.\nSomeone have a idea how I can build a similar by myself? I can add a directory entry, but it will only show directories on sidebar, not things as .desktop files from services, for example."
    author: "Iuri Fiedoruk"
  - subject: "Re: good, but..."
    date: 2003-04-04
    body: "Yes, just found how to do ;)\nI will work a bit on it and release later (maybe this weekend) as a howto in kde-look."
    author: "Iuri Fiedoruk"
  - subject: "Re: good, but..."
    date: 2003-04-04
    body: "What's the classical toolbar option?"
    author: "Navindra Umanee"
  - subject: "Re: good, but..."
    date: 2003-04-04
    body: "Something like this... (see the attached image)"
    author: "Iuri Fiedoruk"
  - subject: "Re: good, but..."
    date: 2003-04-04
    body: "I'm not sure what the difference is.  You can put the tabs on the left (see RMB->Show Tabs Left).  The only change is that the tab name is now displayed.  I guess that could be made a setting as well, but I think it's quite an advantage."
    author: "George Staikos"
  - subject: "Re: good, but..."
    date: 2003-04-04
    body: "The point is having multiple entries on just one entry...\n\nYou know, instead of using root directory in the sidebar items you click in one of the items on the sidebar :) I know, a bit confusing...\n\nI think the best way to explain it is: it's more like the explorer drivers in sidebar approach. Nothing like: OH MY GOD!, but more: I like this way better ;)"
    author: "Iuri Fiedoruk"
  - subject: "popup"
    date: 2003-04-04
    body: "We all love popups right?  Can't live without those, myself.\n<p>\nAnyhow, the point is, people using browsers without sidebar support can still go to the <a href=\"/configure\">configure</a> page and click the icon thingy, but instead of a sidebar entry you'll get a separate little window that you can pretend is your own personal sidebar.  :-)"
    author: "Navindra Umanee"
  - subject: "feature request"
    date: 2003-04-05
    body: "I haven't had chance to look at this yet, but it would be nice if you could configure it to check your history and remove entries you have already looked at."
    author: "dpash"
  - subject: "Re: feature request"
    date: 2003-04-05
    body: "Visited links are usually colored differently already."
    author: "Datschge"
---
<A href="http://www.staikos.net/">George Staikos</a> recently added a fun new feature to <a href="http://www.konqueror.org/">Konqueror</a> that  sidebar lovers everywhere should appreciate.  Those of you running Konqueror from CVS (or a recent enough Mozilla) can go to the Dot's new <a href="/configure">configure page</a>  to add a nifty KDE Dot News side panel to their browser.  The rest of you, while waiting for KDE 3.2, can view some screenshots illustrating this feature on: <a href="http://static.kdenews.org/mirrors/pmax/sidebar3.png">KDE Dot News</a> (<a href="http://static.kdenews.org/mirrors/kde.mcamen.de/misc/sidebar2.png">shot2</a>), <a href="http://static.kdenews.org/mirrors/kde.mcamen.de/misc/sidebar1.png">Gnome Desktop News</a>, <a href="http://static.kdenews.org/mirrors/kde.mcamen.de/misc/sidebar3.png">KDE and GNOME both</a>, <a href="http://static.kdenews.org/mirrors/pmax/sidebar1.png">CNN.com</a> and <a href="http://static.kdenews.org/mirrors/pmax/sidebar4.png">National Geographic</a>.  Those of you who think this feature is neat will also enjoy the upcoming <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/librss/rss-faq.html?rev=1.1.1.1&content-type=text/html">RSS</a> sidebar news applet (<a href="http://static.kdenews.org/mirrors/kde.geiseri.com/sidebarnews.png">shot1</a>, <a href="http://static.kdenews.org/mirrors/kde.mcamen.de/misc/sidebar4.png">shot2</a>) by <a href="http://kde.mcamen.de/">Marcus Camen</a>, <a href="http://kde.geiseri.com/">Ian Reinhart Geiser</a> (<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/dcopservices/rss/">RSS DCOP service</a>) and <a href="http://www.student.uni-oldenburg.de/frerich.raabe/">Frerich Raabe</a> (<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/librss/">librss</a>).  This plugin, currently available from <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/konqsidebar_plugins/konqsidebarnews/">kdenonbeta</a>, should work with any existing RDF/RSS news source. Thanks to George, Marcus and Ian for supplying the screenshots, on top of these very cool hacks.
<!--break-->
