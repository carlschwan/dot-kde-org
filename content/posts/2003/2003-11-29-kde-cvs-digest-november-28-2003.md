---
title: "KDE-CVS-Digest for November 28, 2003"
date:    2003-11-29
authors:
  - "dkite"
slug:    kde-cvs-digest-november-28-2003
comments:
  - subject: "Thanks Derek"
    date: 2003-11-29
    body: "And since it is Thanksgiving time - not only thanks for this issue but for a whole year of your most appreciated writings."
    author: "burki"
  - subject: "Release schedule?"
    date: 2003-11-29
    body: "\nThis document\nhttp://developer.kde.org/development-versions/kde-3.2-release-plan.html\nstates that -rc1 should be prepared on January 18 2004\n\nIs it really necessary to wait soooo long? Things look like a release could\nbe made for end of the year, in terms of cvs activity really focusing\non fixes only\n\na kde user"
    author: "ac"
  - subject: "Re: Release schedule?"
    date: 2003-11-29
    body: "KDE3.2Beta 1 does well for me - why not releasing a Beta-2 as a christmas/advent present?"
    author: "Ruediger Knoerig"
  - subject: "Re: Release schedule?"
    date: 2003-11-29
    body: "Read the mentioned release plan, there will be a second Beta real soon."
    author: "Anonymous"
  - subject: "Re: Release schedule?"
    date: 2003-11-29
    body: "Personally, I'd rather wait a little bit longer and get a super-stable, extra-polished 3.2. I'm sure the release wasn't pushed back without reason; Stephan will know what he's doing. "
    author: "Eike Hein"
  - subject: "Re: Release schedule?"
    date: 2003-11-29
    body: "This is because of Christmas holidays that make it pretty much impossible to do a release.  We hit the exact same thing last year.  Because the release process takes a couple of weeks basically if you don't have it out by December 15th you won't have it out (at least) until January 15th.\n\nIt takes time for testing, tagging, packaging and so on.  A lot of the guys involved are vacation, having their lives dictated by family and whatnot."
    author: "Scott Wheeler"
  - subject: "Re: Release schedule?"
    date: 2003-11-29
    body: "selfish developers - what's more important, me having a kde3.2 release, or them having a christmas?\n\n/me ducks"
    author: "JohnFlux"
  - subject: "Re: Release schedule?"
    date: 2003-11-29
    body: "BANG!!!\n\n\n\nshit, missed him - the basterd ducked on time...\n\n;)\n\n\n\nme for myself - I'd love to have 3.2 NOW but I also prefer it to be stable... \n\nSo I'll just use the beta's, when I feel like complaining about bugs I say \"hey, its just a beta, wait for the stable\" and if I feel like complaining about the 3.2 still not being there I say \"hey, there are beta's which are quite good!\"..."
    author: "Superstoned"
  - subject: "Re: Release schedule?"
    date: 2003-11-29
    body: "The release dude needs vacation time too =)"
    author: "anon"
  - subject: "No, you don't"
    date: 2003-11-29
    body: "If you think KDE works fine at the end of the year than by all means download it and use it nobody's stopping you and keep in mind that no distributions like SUSE, Redhat, Mandrake etc. will ship with 3.2 until March even if it were released today so it really makes little difference, most people won't get it until much later.\n\nBut, again, if you feel it is ready at the end of the year than get it."
    author: "Alex"
  - subject: "Re: No, you don't"
    date: 2003-11-29
    body: "If it was released today distributions would package it. So that \"makes little difference\" parte is not quite right. But I do agree with most of what you say.\n\n"
    author: "s.d."
  - subject: "Re: No, you don't"
    date: 2003-11-29
    body: "yeah but they have to holiday too. Either way we're not getting it till the new year. "
    author: "c"
  - subject: "No they would not"
    date: 2003-11-29
    body: "If it was released today they would not package it, they would not delay their schedules for it. You know how it goes, you can never relly on an accurate release date, thats why none of the distributions waited for KDE 3.2 in December. \n\nIn addition if it were released today they would still have to test it and integrate it which would take at least an extra month and it is definitely not ready today, that is even ahead of the originial schedule.\n\nI like this schedule very much."
    author: "Alex"
  - subject: "Re: No, you don't"
    date: 2003-12-01
    body: "There are 3.2beta1 binary packages for at least Mandrake, SuSE and Debian. Alex's right."
    author: "Anonymous"
  - subject: "Re: No, you don't"
    date: 2003-12-01
    body: "On ftp.kde.org are also official packages for Conectiva and RedHat/Fedora."
    author: "Anonymous"
  - subject: "Re: Release schedule?"
    date: 2003-11-30
    body: "I use the current CVS, and it *really* isn't ready. Its still very unpolished in places (missing icons), and Konqueror is still very unstable. A Feb. release is definately a good idea."
    author: "Rayiner Hashem"
  - subject: "rosegarden"
    date: 2003-11-29
    body: "Hmmm, Mandrake 9.1 doesn't seem to have Rosegarden unfortunately.  What does it take for a distribution to pick up a great app these days?  :-)"
    author: "Navindra Umanee"
  - subject: "Re: rosegarden"
    date: 2003-11-30
    body: "Rosegarden is in Mandrake contribs.\nIf you've got urpmi set up properly, urpmi rosegarden finds it just fine."
    author: "Stephen Douglas"
  - subject: "KDEvelop vs BorlandC++BuilderX"
    date: 2003-11-29
    body: "I just recently tried Borland C++ Builder X and I am very impressed with it, I like it even more than KDEvelop 2.1.5. BCX is an extremely well done multi platform IDE with an excellent compiler and set of tools.\n<p>\nThere is one feature in it that I don't think I would ever go back to another IDE without! \n<p>\nIn BCX it doesen't matter how crappy my code looks, for example it can look like this:\n<p>\n_______________________________________\n<pre>\n#include <iostream>\nusing namespace std;\n\n void menu()\n {\n    int choice = 0;\n   cout << \"\\n[1] Solve a Linear System\\n\";\n       cout < < \"[2] Quit\\n\\n\"; cout << \"Enter choice (1-2): \";\ncin >> choice;  switch ( choice )\n{\n    case 1:      cout << \"not available yet\\n\";    break;\n\n       case 2:\n       cout << \"not available yet, sorry\\n\";\n            break;\n\n    default:\n              cout <  < \"\\nPlease select either choice 1 or 2.\\n\";\n       menu() ;\n}\n\n      }\n</pre>\n__________________________________________\n<p>\nAll I have to do is go to Edit -> Format All and it will be very readable and formatted to perfection. Observe the result after just clicking that option.\n<p>\n__________________________________________\n<pre>\n#include <iostream>\nusing namespace std;\n\nvoid menu()\n{\n  int choice = 0;\n  cout << \"\\n[1] Solve a Linear System\\n\";\n  cout < < \"[2] Quit\\n\\n\"; cout << \"Enter choice (1-2): \";\n  cin >> choice;\n  switch ( choice )\n  {\n    case 1:\n      cout << \"not available yet\\n\";\n    break;\n\n    case 2:\n      cout << \"not available yet, sorry\\n\";\n    break;\n\n    default:\n      cout < < \"\\nPlease select either choice 1 or 2.\\n\";\n      menu();\n  }\n\n}\n</pre>\n____________________________________________\n<p>\nIt saves me hours when I try to fix my code up and its just so simple it doesen't even ask any questions (though I could customize the feature) and it will format anything you throw at it, regardless of its crazy formatting perfectly. my class requires that my code be formatted very well and its saved me a LOT of time.\n<p>\nBottomline, will KDEvelop 3 have such an amazing feature? I have only tried 2.1.5. If it doesen't than I guess I will be sticking with BCX Personal (which is free and not a trial, but you can't make commercial software with it).\n<p>\nIf it does, tahn I will most likely use KDEvelop 3."
    author: "Alex"
  - subject: "To see what I mean. (Sorry)"
    date: 2003-11-29
    body: "For some reason the first messy code snap and the second clean one look the same unless you click view in which you can see the difference.\n\nSo click the \"view\" link right next to reply to see the difference."
    author: "Alex"
  - subject: "Re: KDEvelop vs BorlandC++BuilderX"
    date: 2003-11-29
    body: "Emacs has done that since ages.  There are Unix filters that do the same thing."
    author: "anonymous"
  - subject: "Cool!"
    date: 2003-11-29
    body: "But I don't like Emacs as muchas a IDE like KDEVELOP or BCX. Does KDEvelop 3 have that? \n\nThe feature is awesome and if the unix implementation as you say there are has this than that is really really great =) Now I am much more productive thanks to it, no more spacing just right , all I need to do is code than when i feel its getting too sloppy just \"format all\" and instantly everything looks great =)"
    author: "Alex"
  - subject: "Re: Cool!"
    date: 2003-11-29
    body: "I don't know about KDevelop but Emacs helps you format your code *as you work*.  This is much better than coding ugly and then reformatting later although it lets you do that too."
    author: "anonymous"
  - subject: "Re: Cool!"
    date: 2003-11-30
    body: "Check out AStyle (http://astyle.sf.net), it worked wonders with my assignment the other day, where I was jumping between different editors on different platforms, and every single one had its own idea of how to autoformat..."
    author: "Mikkel"
  - subject: "Re: Cool!"
    date: 2003-11-30
    body: "Latest KDevelop has this feature. Tools->Reformat source. Very useful when you remove outer loops and things like that. \n"
    author: "Apollo Creed"
  - subject: "Re: Cool!"
    date: 2006-04-18
    body: "You look like girls!if you know programing(not a  magazine tutorial) it would be enought with vim!"
    author: "Jhon"
  - subject: "Re: KDEvelop vs BorlandC++BuilderX"
    date: 2003-11-30
    body: "Yes Kdevelop does have this feature. its under the tools menu. Along with lots of other useful stuff.\n\nYou can apply it to just selected text too. The code style stuff is excellent and allows you to format the code how you like.\n\nI use it a lot, I contribute code to a project that uses a style very different from what I like. So I write the way I want to with code styled the way I like it, change a few options and reformat the code and commit it, and change it right back."
    author: "Leonscape"
  - subject: "Very cool!"
    date: 2003-11-30
    body: "That's exactly what I wanted, another question, since somebody said that emacs has a mode which autoformats as you write, does Kdevelop have this too?"
    author: "Alex"
  - subject: "Re: Very cool!"
    date: 2003-11-30
    body: "There is the automatic indentation. But I don't think I've tried that yet. One little correction the source formatter is under the edit menu :)\n\nThe difference between two and three is huge. Its a seriously impressive piece of kit. Better than and other IDE I've used."
    author: "Leonscape"
  - subject: "Rosegarden"
    date: 2003-12-01
    body: "Rosegarden looks nice, it atergets the professional market. However I prefer home user tools like musicbox."
    author: "gerd"
---
In <a href="http://members.shaw.ca/dkite/nov282003.html">this week's KDE-CVS-Digest</a>:
KHTML regressions and font handling fixed. 
<a href="http://amarok.sourceforge.net/">amaroK</a>, another media player, now has a resume feature, and can play streams. A new release of <a href="http://www.all-day-breakfast.com/rosegarden/
">Rosegarden</a>, a powerful application aimed at composers, musicians, music students and recording environments. Plus many bugfixes in all applications.
<!--break-->
