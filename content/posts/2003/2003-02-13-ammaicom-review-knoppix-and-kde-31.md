---
title: "Ammai.com: Review of Knoppix and KDE 3.1 "
date:    2003-02-13
authors:
  - "Datschge"
slug:    ammaicom-review-knoppix-and-kde-31
comments:
  - subject: "nice review"
    date: 2003-02-12
    body: "hi,\nproud to use my linux/kde every day!!!!!!\n\ni send Vinod this email. ;-)\n\n\nhi Vinod,\n\ngreat review. yes, knoppix and kde 3.1 really rocks!\n\nhere are some nice hinds:\n1. getting the processcontrol quicker: just hit: ctrl+esc\n2. or kill and application: ctrl-alt-esc and klick (right klick kills the skull) ;-)\n3. in konquror try out the splitt windows it rocks\n4. or do an fish://users@yourdomain u can even do it in quanta: loading up with full encryption (ssl). best with splitt windows (u need an ssl shell for that)\n5. or do tabbed browsing with konquorer (shift+ctr+n or so...)\n6. or do shift+up/down arrow in konqueror....\n\n...and much much more.\n...i stop here, because you havent asked ;-)\n\ngreetings\ngunnar"
    author: "gunnar"
  - subject: "I want more hints!"
    date: 2003-02-12
    body: "I'm a KDE user, but didn't know the shift + up or down trick in konqueror. Any other great tips you could give?\n\nIf I had time, I would start a KDE tips & tricks list .......\n\nRaph"
    author: "raphinou"
  - subject: "Re: I want more hints!"
    date: 2003-02-12
    body: "...\n7. type in konquorer: gg:knoppix or php:session\n8. right click at panel -> add -> applet -> public webserver ...gooo (or some other applet)\n9. click at the clock (at the right)\n10. run an application (kwrite) as root? alt+F2 and then kdesu kwrite \n\n...and so many more.\n\ngreetings\ngunnar"
    author: "gunnar"
  - subject: "Re: I want more hints!"
    date: 2003-02-12
    body: "sorry for posting this twice.\n\nanother nice tutotial: http://www.trylinuxsd.com/\n\n-gunnar"
    author: "gunnar"
  - subject: "sftp instead of fish"
    date: 2003-02-13
    body: "Everbody speaks so well of fish:// , but KDE has had sftp:// since longer. It uses the SFTP protocol which is implemented in (almost) all SSH v2-implementations (in the default installs of GNU lsh, OpenSSH and SSH.com) and, abobe all, properly standardized. So prefer sftp:// if it is available on your systems!"
    author: "Jonas"
  - subject: "Re: sftp instead of fish"
    date: 2003-02-13
    body: "I always thought 'fish' and 'sftp' were pretty much synonymous... but then,\nwhat do I know?  Isn't fish using the SSH protocol as well?  If not, where are the\ndifferences?  I think many KDE users (not just me) would like to know...\n\nTIA\n\nKLaus"
    author: "Kavau"
  - subject: "Re: sftp instead of fish"
    date: 2003-02-14
    body: "fish works even on systems that don't allow or can't do sftp (crazy admins, old ssh, etc). otherwise they're pretty much the same functionality wise. fish puts a perl script in your home dir (.fishsrv.pl), while sftp doesn't require that. instead it requires a ssh2 server that has sftp support turned on."
    author: "Aaron J. Seigo"
  - subject: "Re: sftp instead of fish"
    date: 2003-02-14
    body: "Additionally, or, more precise, because of these differences, in my experience sftp has a much better performance on low-end servers and does not load their CPU that much.\n\nIn contrast an advantage of fish is that it does support symlinks transparently while sftp does not, at least not with my current server configuration which is pretty much Debian Woody default concerning the sshd.\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: nice review"
    date: 2003-02-13
    body: "> 6. or do shift+up/down arrow in konqueror....\n \nThat's really neat! The only problem I have is that it takes a considerable \namount of hand-eye coordination to stop the text from scrolling once you've got \nit started! Or is there a convenient trick to do so? \n\n\n"
    author: "Kavau"
  - subject: "Re: nice review"
    date: 2003-02-13
    body: "Take your hand off shift, then press an arrow key."
    author: "Anon"
  - subject: "Re: nice review"
    date: 2003-02-14
    body: "Just press Ctrl alone.\n"
    author: "L.Lunak"
  - subject: "Re: nice review"
    date: 2003-02-14
    body: "Ahh, that makes this feature almost usable again. But still: shortly after the feature was introduced, kthml reacted promptly when I de-/accelerated the autoscroll speed via subsequent\nshift-cur-up/down hits. This got broken somewhen, and now there are several seconds long lags that pretty much defeat the purpose of autoscrolling. I do not use it much since then. (No, I didn't file a bug report yet. :-)"
    author: "Melchior FRANZ"
  - subject: "FYI"
    date: 2003-02-14
    body: "To those using the official Knoppix (which still uses KDE 3.0.4) be aware that the fish protocol is not KDE 3.0x and thus is not in the official Knoppix version.\n\n"
    author: "Antiphon"
  - subject: "Re: nice review"
    date: 2003-03-28
    body: "I try 6 diff. Linux distribution and I thik Knoppix is the best (but try Peanut Linux anyway)"
    author: "Jacek"
  - subject: "A really must have "
    date: 2003-02-12
    body: "This is the knoppix version i was dreaming of !\n\nAll my thanks again and again to Klaus Knopper (this the best linux-promotion-tool i know) and to the whole KDE team.\n\nMike"
    author: "Maillequeule"
  - subject: "german computer magazin with Knoppix"
    date: 2003-02-12
    body: "Hello,\n\nthis weeks edition of the german computermagazin c't ships with a knoppix CD including KDE3.0.4 and OpenOffice. ( www.heise.de/ct )\nTheir aim is that the readers can make up there own mind wether Linux is ready for the desktop or not.\n\nThe c't is one of the best selling computer magazins in germany with over 380000 reader per issue.\n\nI think this is a great boost for Linux and KDE.\nThe Knoppix guys did a really good job!\n\nI think I will change from my old SuSE7.1 to Knoppix.\n\nPeter"
    author: "Peter"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-12
    body: "maybe you should consider http://www.gentoo.org?\n-gunnar"
    author: "gunnar"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "Maybe you should lay off the fanboism.\n\nI have been using linux since the beginning. Who has six months to compile a basic system?"
    author: "yup"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "> > maybe you should consider http://www.gentoo.org?\n> Maybe you should lay off the fanboism.\n \nFanboism? Maybe you should give up coining new and more annoying words?\n\n> I have been using linux since the beginning. Who has six months to compile a basic system?\n\nWe're talking since the first kernel appeared? I guess you would have been compiling it then. Six months for a basic system? Talk about FUD! I've installed Gentoo on several systems now and love it. It is not for the faint of heart but compared to the packaged distros I've used it is very light, extremely fast and totally honors the developer's design. I also don't have to stick a CD in the drive to update ever again, have preemptible multi-tasking on my system and never have to deal with RPM dependency hell again. I've also gotten programs running on my system that would not install with RPM or build from source on other distros. I find XFree86 upgrades to be cake and can get the latest PHP installed with ease. There is a lot to be said for Gentoo... not the least ot which is that it returns to the *source* part of open source.\n\nFor all this I got each basic system up with X and KDE in a matter of hours because of the new option to have large programs prebuilt A full rebuild of KDE along with X, a new gcc, base system files and other programs took a day and a half on my wife's Athlon 700 with 256 MB of RAM. It's much faster on my 1900+ with 500 MB. I have not even started using distcc because of optimizations.\n\nBTW the best part of my wife's update is that she did it because she can type \"emerge -u world\". Want to talk about how many clinics we can find on how to do this with RPM? Have you ever killed RPM? I don't know where your \"beginning\" was with Linux but it does not appear to be very tolerant or informed. If you have broadband and a reasonably new system Gentoo can be very attractive as well as save you time and anxiety over 2-4 upgrades a year. For a growing number of people this is something to get excited about.\n\n</enthusiast mode>"
    author: "Eric Laffoon"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "APT lets users not have to worry about dependencies and not have to waste bandwidth and CPU cycles to download and build from source. It also lets one build from source the packages for which that is absolutely necessary. At the moment, it seems like a far more reasonable tool for most users."
    author: "dmp"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "\"APT lets users not have to worry about dependencies and not have to waste bandwidth and CPU cycles to download and build from source.\"\n\nGentoo takes care of dependencies. And you dont seem to understand: Gentoo-users don't use Gentoo despite the compiling. Many do it BECAUSE of the compiling. I prefer to compile my software from source.\n\nBesides, Gentoo is alot more up-to-date that Debian is (Xfree 4.2 was only 9 months late in Debian, KDE3 is still not in Sid).\n\nNow I admit, Gentoo is not for everybody. It's not trying to be."
    author: "Janne"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "But KDE 3.1 is in Debian sid."
    author: "Tristan Tarrant"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "Really? So, where is KMail 3.1 or KOrganizer 3.1?"
    author: "Anonymous"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "It is? To be honest, I haven't checked recently since I'm using Gentoo more and more. You could say that KDE3's absence from Debian was one thing that drove me to Gentoo. If they FINALLY have KDE3 in Sid, good for them! Too bad they are about half a year late!"
    author: "Janne"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-16
    body: "How can they be a year and a half late, when kde3.1\njust came out a month ago?\n\nDebian has had all the rcs of kde 3.1 all along.\nThey weren't in sid ,but in packages that were in\nauxillary package repositories.\nAlthough they are called unofficial packages , they\nwere by the debian kde packagers themselves.\nYou just had to add the new location  to your sources.list.\n\nDebian for the most part, can be as uptodate as you want it to.\n\nAs for Gentoo, I have nothing against it, ( tried it once) but \nemerges don't always work either."
    author: "kannister."
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-14
    body: "> APT lets users not have to worry about dependencies and not have to waste bandwidth and CPU cycles to download and build from source.\n\nI know about APT, though I have not used it. Conversely it is possible to retrieve prebuilt programs thorugh Portage too. I used it for Open Office after I read about the resourses a compile hogged. However the argument about CPU cycles is specious. Most people are hardly ever using any of their CPU unless they are compiling or rendering. I leave my system on at night and I sure don't notice the difference. What I do notice is the blazing speed compiling with CFLAGS=\"-march=athlon-xp -pipe -O3\". I've heard that Debian was a faster distro. When I first loaded Gentoo it was several orders of magnitude faster than I had been with the same system running Mandrake. I also like being up to date with the latest gcc.\n\n> It also lets one build from source the packages for which that is absolutely necessary. Again, it is \"open source\" not \"free binaries\". ;-)\n\nThe Gentoo perspective is just the reverse... installing pre-compiled programs when absolutely necessary.\n\n> At the moment, it seems like a far more reasonable tool for most users.\n\nI've had people write me with user questions that were running Gentoo where the questions made me wonder how much they knew about basic Linux. Conversely I've heard that initial install on Debian is not nearly as easy as Mandrake and SuSE. For the moment I'd say neither distro is near so ready for the average computer user and Knoppix is the clear winner for painless review. (granted it's from Debian but that's not the point is it.) \n\nIf I had to bet between Debian and Gentoo which one would capture the hearts of more people computing in the next few years I'd say the momentum goes to Gentoo and the advances they're making are impressive. As broadband connections and faster CPUs proliferate the adventurous are migrating en masse to Gentoo. Nothing against Debian because it's a very good distro but Gentoo is a remarkable success story with a lot of momentum that strikes a chord with many users tired of RPM. I'd run either before tying myself to another RPM based distro. RPM could have been good but it has become a disaster. I could go on and on about how and why I dislike it."
    author: "Eric Laffoon"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: ">A full rebuild of KDE along with X, a new gcc, base system files and other\n>programs took a day and a half on my wife's Athlon 700 with 256 MB of RAM.\n\nSometimes I wonder where the advantage of Linux - to run fast on older hardware - has gone. KDE needs lot of ressources, compiling it on AMD K6 based systems takes ages.\nWould be nice if the developer would sometimes remember people with smaller ressources and try to optimize their applications instead of overboarding them with features which cannot be switched off to work with acceptabe speed on older PCs."
    author: "Andreas"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "Sorry, but if compiling takes ages on your machine then you should install binaries instead of compiling yourself.\nFurthermore, additionally to adding new features (which you, the users, ask for) we are always trying to optimize our applications. Most features which cost a lot of cpu cycles can be switched off.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: ">> Sometimes I wonder where the advantage of Linux - to run fast on older hardware - has gone. \n>> KDE needs lot of ressources, compiling it on AMD K6 based systems takes ages.\n\nOf course compiling takes ages! It's a lot of high quality code! \n\nAs for the speed, KDE 3.1 is Fast. Faster than 2.2.2 even, and I installed it on my P150 mhz laptop, assuming it would choke and die. But it runs, not like lightening, but quite fast, and very useable.\n\n>> Would be nice if the developer would sometimes remember people with smaller ressources \n>> and try to optimize their applications instead of overboarding them with features which cannot \n>> be switched off to work with acceptabe speed on older PCs.\n\nI think they have with kde.3 1... \nas with gentoo, there is no advange to apt, if you want easy updates, getting things installed easyly there is apt. If you really want to compile kde there is konstruct, however, compiling KDE is no trivial matter, I used the great app konstruct, but still didn't manage to compile all parts of kde."
    author: "Eadz"
  - subject: "On Debian vs. Gentoo"
    date: 2003-02-13
    body: "Please, please... I like Gentoo (I'm running it on the machine I use to type this) but please stop being the fanboy. apt has a much more advanced dependency scheme than Portage. (Ever tried upgrading OpenSSL to latest unstable on your Gentoo box? Guess that's why it isn't upgraded by default yet...)\n\napt is easily the best tool around -- for installing binaries, and no good at all for auto-compiling source packages which Portage is very good at. Use the best tool for the best job! \n\nThe Debian project rocks but is slow-moving. The Gentoo project is more dynamic, less chatter and more bleeding-edge. But they have much fewer packages and less mature tools."
    author: "Jonas"
  - subject: "Re: On Debian vs. Gentoo"
    date: 2003-05-22
    body: "A great deal of the Debian community are a bunch of stuck up *&!!# who think the are l33t just because they use Debian GNU/Linux. I used Debian on my old PC for a while, but when I bought a new one I figured it was time for a change (+ I was too lazy to do research to get Debian running on my new PC). I have to say I like Gentoo much more than I like Debian. It's like Gentoo folks are much nicer and more willing to help. Indeed Gentoo has fewer packages, but Gentoo is very new. Anyway when Debian \"ruled\" it was state of the art, now... Well let's hope they come up with someting revolutionary fast."
    author: "Well..."
  - subject: "Re: On Debian vs. Gentoo"
    date: 2003-07-29
    body: "I run 100% ~x86 on my box, including OpenSSL, without a glitch.\n\nMaturity comes with age, and as with many a fine wine, one must let it age gracefully; but lets not forget to dust away the cobwebs Debian."
    author: "Martin"
  - subject: "Re: On Debian vs. Gentoo"
    date: 2004-02-20
    body: "I wrote a whole piece on it, feel free to check it out (the comments are disabled for now but should appear again soon)\n\nhttp://www.tomvergote.be/writings/Linux/Debian-Gentoo-production-environment.html"
    author: "tom"
  - subject: "Re: On Debian vs. Gentoo"
    date: 2005-04-17
    body: "Any distribution has pros and cons, and they all should keep their focus on delivering an OS that serves the purpose. Debian is a self-indulging community that delivers in three flavours: stable, testing and unstable, being buzz words for  \"very obsolete\", \"obsolete\" and \"unusable\". At this time, an up-to-date gnu/linux ought to include the following major projects:\n\nXfree 4.5\nxpdf 3\nteTeX 3\nOpenOffice 2\nPostgreSQL 8\nmozilla's firefox and thunderbird 1.0.2\n- support the wxwindows development libraries!!!\n- use postscript and open-type fonts only (have you ever seen how good KDE looks with commercial postscript fonts? If you do, you will understand what I am saying.)\n\nIn addition to them, there should be a new collaborative project to make a live iso image, regardless of the specific flavour of linux distribution. Enough with    clutter installations: just flip in the DVD, reboot and get productive. If you want, you can install the system in the hard disk and be happier. This is what I would like to have, here and now, but unable to find anywhere.\n\n\n"
    author: "Bob Hunter"
  - subject: "Re: On Debian vs. Gentoo"
    date: 2006-06-16
    body: "Man i just surfed by, and have to remark painfully, what kind of strange people you KDE folks are... man.. jsus not one factually or technical argue \n\nOk, sorry\nQ: what distro?\nA: \"Have you seen how nice kde looks with commercial ps fonts\"\n\nman\njesus....\n"
    author: "Heck"
  - subject: "Re: On Debian vs. Gentoo"
    date: 2006-06-16
    body: "> man jesus....\n\nNo manual entry for jesus....\n"
    author: "cm"
  - subject: "Re: On Debian vs. Gentoo"
    date: 2006-12-07
    body: "hahaha. mod +1\n\n\nJESUS(2)                   World Kernel Manual                   JESUS(2)\n\n\nNAME\n\n     jesus - just expedite solutions using source\n\nSYNOPSIS\n\n     jesus [-INRI] [-p prayers] [-c confessions] [-b bleefs] [soul...] \n\nDESCRIPTION\n\n     For each sinner that invokes jesus using some combination options -p (pray) -c (confess) and -b (bleef) with argument [soul], jesus performs an audit of all flags and scrubs all set sin flags. Note: jesus rarely sends to stndout, and only usually gives indirect evidence it has done anything.\n\nDIAGNOSTICS\n\n     Extremely hard to debug using strace as jesus code is extremely well optimized for speed of execution. \n\nEXAMPLES\n\n     \"jesus --save me\" - Called with gnu option --save, jesus reads soul file [me] and writes directly to buffer [heaven].  \n\nCONTRIBUTORS\n\n     J, E, D, & P: J codes Jehovah for God. E stands for the author who coded Elohim for God, D stands for the Deuteronomic author and P stands for the priestly code. Gnu version contributers Mark@bibble.org, Matthew@bibble.org, Luke@bibble.org and John@bibble.org\n\nSEE ALSO\n\n     father(1), holyghost(3), bibble (7), bibblepro(7)\n\n"
    author: "compustretch"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "Ok, I was being a jerk. I'm sorry.\n\nMy early days were not that early as to be playing with early kernels and such, but were early like  Yggdrasil-Linux-CD early.\n\nAnyhoo. I have installed Gentoo on a G4 workstation at work. it took quite a long time. A week, and that is no FUD :)\n\nI appreciate the flexibility of which you speak, and boy am I craving it. But I also like a workstation that works. One that I dont have to fuss with, as those days are kinda over for me. Call it laziness, age, or being plain spoiled. Who knows?\n\nI dont mind messing around. And truth be told, I found Gentoo pretty easy and straightforward to get install. Much like the ease of Slackware.\n\nI am tempted to try gentoo on my p4 as my woes with redhats messing with kde (KDEDIR? Whats that?) are about through.\n\nSo these are my two questions:\n\nOne: I hear a lot of people end up breaking things with updates. That portage sometimes causes more problems then its worth. Simple updates can smoke something working a short time back. Witness on the forums the issue with the updated kdelibs for 3.1.\n\nDo you have that experience? Have you had that? Is this just people who are update happy?\n\nAlso, Fonts. I want nice fonts. Know any good toots on getting smooth, pretty fonts going? Thats another common complaint on the gentoo forums. I have some tricks I have learned over the years, but frankly, I do want it to just work. so I can work.\n\nAgain. Sorry for being the ass. Thanks for calling me on it. I am quitting smoking."
    author: "yup"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-15
    body: "> Again. Sorry for being the ass. Thanks for calling me on it. I am quitting smoking.\n\nMore power to ya, brother.  I can't seem to do it. I'm 29 and have been smoking for almost 20 years...  :-\\\n\n(not regularly until I was about 14 or so, but still...)"
    author: "Xanadu"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-18
    body: "its killing me man.... :( \n\n\nnot smoking that is."
    author: "yup"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "\"Who has six months to compile a basic system?\"\n\nSix months? Are you running Linux on old 386 :)? I'm currently running Gentoo on an old 266MHz P2-laptop (it's either 233 or 266Mhz, I'm not sure) with 128megs of RAM. It's definitely NOT state of the art.\n\nI started the installation one evening (from State 1, which means that EVERYTHING is compiled). I reached bootstrap-phase (where it compiles GCC and Glibc) in that evening. I left it compiling and went to bed. \n\nWhen I woke up, it was finished. I then progressed to next stage (when it compiles everything else). I left it compiling and went to work. \n\nWhen I got back home, it was finished. I then installed Xfree and Fluxbox (the machine is short on resources, so Fluxbox was the best choice). I left it compiling (Xfree is big) and by next morning it was finished. I really didn't lose any time (since the machine was compiling in times when I wouldn't be using machine in the first place).\n\nI would say that on a modern machine, compiling the basic system takes maybe 2-3 hours. And besides, people who use Gentoo, know about the compiling. It's not like they use Gentoo despite the compiling, thy use Gentoo BECAUSE of the compiling (of course, there are other benefits as well).\n\nAnd yes: Gentoo IS great ;)!"
    author: "Janne"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "This c't edition has a much, much better setup than any other distribution I have seen so far: e.g. type ifconfig and hit <tab> and you get the list of possible devices... \nWhy can't every distri configure the bash like the c't guys did?"
    author: "Norbert"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "> Why can't every distri configure the bash like the c't guys did?\n\nBecause people will rather complain than switch distros?"
    author: "Roland"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "Sound like c't has been using the newest bash (2.05b). Don't worry within no time at all, all the distro's will be using it."
    author: "Johan Veenstra"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "What's so great about the newest bash?  \n\nEveryone should be using tcsh anyway.  Bash is just plain annoying. :-P"
    author: "Navindra Umanee"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: ">> Sound like c't has been using the newest bash (2.05b). Don't worry within no time \n>> at all, all the distro's will be using it.\n\nDebian has had it for over a year ( maybe longer ) .. so I don't know what you mean when you say \"no time at all\"."
    author: "Eadz"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "You can also use the (new )seperate bash-completition package, witch is split off from bash CVS HEAD right now (although 2.05 had it)..."
    author: "fault"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "Debian/unstable already has this feature:\nType \"mount <tab>\" and you get the list of moutable devices (or mount points)\nThey even configured bash to do completion for apt:\nType \"apt-get i<tab>\" then \"install\" appears; then type kernel-i<tab> and you get alist of all the available kernel-image packages. Of course this work for every deebian package.\nEven for ssh/scp:\nType ssh a<tab> and you get a list of all hostnames beginning with a"
    author: "Sangohn Christian"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-13
    body: "I see.\n\nIn tcsh, Alt-P usually does the trick since it cycles through the history of past usage."
    author: "Navindra Umanee"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-16
    body: "I have a debian/unstable and my bash version is 2.05b-5 but I don't have this feature. How can I turn it on?\n\nMichael"
    author: "michael"
  - subject: "Re: german computer magazin with Knoppix"
    date: 2003-02-17
    body: "In /etc/bash.bashrc uncomment the lines loading bash_completion.\n\nCheers,\nKevin\n"
    author: "Kevin Krammer"
  - subject: "Simply install bash completion"
    date: 2003-02-13
    body: "YOu can find it here, it is easily installed wherever you use bash.\nhttp://www.caliban.org/bash/index.shtml#completion\n\nI agree it is a shame that SuSE-8.1 did not ship with it.\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "Quanta Plus - favorite app! ;-)"
    date: 2003-02-12
    body: "I thought I'd take a look. He is very positive on Quanta Plus! This is exciting to see such praise. Ya gotta love it. Best of all we are not resting and Quanta is just getting better. Our current CVS opens quanta with a handfull of project files orders of magnitude faster than ever before and we are continuing to extend our feature set."
    author: "Eric Laffoon"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "About Quanta Plus.. I am confused. Perhaps I misread this, but I thought Quanta Plus is now included in the KDE release. After updating KDE on Gentoo it didnt come installed. So do I need to emerge this as a seperate package? Thanks just in case."
    author: "IR"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "It IS part of KDE 3.1. See: http://kde.org/announcements/announce-3.1.html. Some distros may have not noticed it yet, as it's in a separate tarball and has it's own tagging in CVS (just like arts), but it is released together with KDE, not separately like KOffice and KDevelop.\n\nAndras\n "
    author: "Andras Mantia"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "emerge quanta quanta-docs\n\nWorks here.\n\nMy daughter is writing a blog, so needed some html help. I showed her a couple of very basic things, such as finding a page you like and looking at the code, where to find help in Quanta, a few basic tags, etc. Now she had done up a very nice looking page. No wyswyg, no problem. A very impressive application.\n\nDerek "
    author: "Derek Kite"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "Quanta Plus is the bomb. I've had countless conversations more or less exactly like this:\n\nGuy: Hey, did you check out the latest Quanta?\nMe: Yes! It's fantastic, isn't it?\nGuy: Are you kidding me? It's [profanity] unbelievable!\n... etc :)\n"
    author: "Haakon Nilsen"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "Quanta Plus i amazing!\n\nBut there is two things that buggs me...\n\n1. When i open a file from Konqueror Quanta doesn't check if i already have Quanta open... so it opens upp anotherone...\n\n2. Some files that i can open fine when i open them with Quanta from Konqueror doesn't open from the panel i Quanta... he complains about it being binary but it is a html file.\n\nIf someone can fix these things i would owe him/her my life :)"
    author: "isNaN"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "1. Would be easy - just requires making it a KUniqueApplication.\n2. How odd!\n\nRich\n"
    author: "Richard Moore"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "1. How do i do that?\n2. Well... yea..."
    author: "isNaN"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "\"1. Would be easy - just requires making it a KUniqueApplication.\"\n\nThere is a --unique switch. Just use it in your .desktop file.\n\n\"2. How odd!\"\n\nAnd if I tell you that some distros return an odd mimetype for *.php? Madrake 9 is one of them. Quanta uses mimetypes to find out if a files is binary or text. If it's under the text node, it's a text, otherwise it's a binary. On some distros the php mimetype is only under application. You should create a new mimetype under text for php files and Quanta won't complain anymore.\n\nAndras\n \n\n"
    author: "Andras Mantia"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-14
    body: "Hey didn't I read...\n> If someone can fix these things i would owe him/her my life :)\n \nSo you  know we'll settle for much smaller gratuities. ;)\n\nI made the decision to make the default not to go with a unique application because there are times I have a project open and want to open another project or just handle a file quickly. As I just committed KTips to CVS I'm going to write a tip for this. Thanks!\n\nBTW this is another reason why Quanta may be the best candidate for KTips... <slaps forehead>"
    author: "Eric Laffoon"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-14
    body: "> So you know we'll settle for much smaller gratuities. ;)\n\nhe he... i'll just say thank you very much then because your tips worked great! :)\n\nAnd i have to say that i love the open source community! Whenever i have a problem i just go to a forum and ask and people are eager to help me! This is truly amazing!"
    author: "isNaN"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-16
    body: ">And i have to say that i love the open source\n>community! Whenever i have a problem i just go\n>to a forum and ask and people are eager to help me!\n\nNot just \"people\", but often the actual coders or project managers themselves (I'm not implying they're not people ;). Try that with the gazillion dollar multi galactical faceless heart-as-dark-as-coal software corporations.\n"
    author: "Haakon Nilsen"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-05-08
    body: "My favorite app too! The mime type change did it. Thanks!"
    author: "Herman Tolentino"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "Last I checked (Mdk91b3) Quanta+ didn't have:\nCtrl+C == Ctrl+Ins\nCtrl+V == Shift+Ins\nCtrl+X == Shift+Del\n\nFor a guy trying to ditch Win32 & Editplus this is a must :|"
    author: "Void"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "It is your default text editor part that handles this, which is probably Kate.  This works just fine (cvs head I admit) with the shortcuts you've mentioned.  Anyway, it should all be configurable, check the shortcuts dialog."
    author: "Hamish Rodda"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-13
    body: "Like he said 'Except for WYSIWYG, this tool has got pretty much everything, including toolbars with buttons to different HTML elements.'\n\nWhat happened to kafka anyway?"
    author: "rsv"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-14
    body: "> Like he said 'Except for WYSIWYG, this tool has got pretty much everything, including toolbars with buttons to different HTML elements.'\n\nAnd don't forget you can make more toolbars and buttons to your heart's content... including ones that live in a project and even load them with groups of files in project views. Vinod told me Quanta had him on a path Linux in an email exchange we had. I consider his praise no small thing for a guy running DreamWeaver Mx. To think I still have people trying to compare it to Homesite and not realizing it already does more. ;)\n \n> What happened to kafka anyway?\n\nKafka seems to have fizzled since Jono Bacon left for other projects. Jono has remarkable organizing and documentation skills. I miss seeing more of him. Kafka was focused at being a \"light and easy\" HTML editor. However as I have long said you have to be able to do PHP, XML and things not yet dreamed or you are just working with nostalgia. Since I took over the helm I have steered Quanta to being a *serious* professional tool that still accomodates newbies. Traditional WYSIWYG tools have seroius baggage WRT XML and especially pre-process scripting. Hard coded visual markup editors invite eternal hack rewrites that forever lag the bleeding edge and fail to address more productive programattic approaches. That's what you get from your marketing department design sessions, not Quanta.\n\nFor all the nice things said about us I hope someday to hear this a little more, that we impacted the Linux/KDE community by enabling it to move ahead of the rest of the web development community... not hamstrung it by leaving it with a hard coded HTML only editor that left people switching from us instead of to us.\n\nAndras and I are currently in the early exploration of WYSIWYG options. Next we can look at a timeline and when we might be able to get it delivered. If we had more help and some of our initial investigations show promise it might be as soon as KDE 3.2. I hope that it can be in for 3.3. That means that we are looking to deliver WYSIWYG *this year*. Our foundational key elements are nearly in place. We still need to do more with XML DTDs and Schemas and set up rule sets so that users can tune WYSIWYG performance per DTD/Schema. No FlunkPage hacks! We also want to use our parsing to interpolate between the rendered page, underlying markup and base scripting so that WYSIWYG will actually benefit PHP, Javascript and other languages underneath the markup. I don't think anybody else has this.\n\nI joined this project originally to help with feature set and interface design. I had neither a clue nor any great credentials just a few months earlier. Along the way I had to teach myself C++ and sponsor developers to keep our quality and activity constant and high. I also fell in love with free software. To go to the next level we need people doing templates, assisting with XML, user interaction and more. So let me appeal to everyone... why settle for good. We want to be the killer app for web development to secure an open internet once and for all. Imagine thousands of web developers migrating to KDE this year! If you can't get excited about this you're not a true geek. ;) Anyone with a will can help. Most of the development of Quanta, even in the last year, has been done by a handful of people. Imagine the difference a few more people could make."
    author: "Eric Laffoon"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-14
    body: "This is quote from an anonymous forum poster:\n[quote]\nthe only valid argument that developers of non-WYSIWYG html editors seem to use to justify producing an essentially obsolete product -- \"our software makes cleaner code\" -- doesn't make sense to me. what exactly are you afraid of? poor load times? there isn't that much difference between a 3k file and a 6k file, sorry to say. standards compliance? how about doing a little testing. i'm sorry it doesn't cut it.\n\nclearly the producers of this software (as well as bluefish, quanta, and whatever else) are html purists who don't actually want anyone to use their software. i already *have* a text editor. i don't need another one. i need software that will actually push creativity and design aesthetic forward for the linux platform as a whole. i know it's asking a lot and i'm not in a position to do much about it (i have zero free time to learn how to develop applications) but it just seems that there is a niche to be filled and nobody is even bothering to fill it.\n\nplease, can somebody show me BEAUTIFUL website that was produced in screem (or bluefish or quanta). i love minimal design as much as the next guy but try getting creative while you're wading through a mile of table tags.\n[/quote]\n\n\nIt was modded +4 Insightful. What are your comments about this?"
    author: "Stof"
  - subject: "Re: Quanta Plus - favorite app! ;-)"
    date: 2003-02-14
    body: "> i love minimal design as much as the next guy but try getting creative while \n> you're wading through a mile of table tags\n\nAny, I mean all of the visual layout programs I have ever used, be it html, even an old direct digital control hvac graphic screen designer, required access to the 'code' to get the layout where I wanted it.\n\nAfter I learned the 'code', it was quicker and easier to layout in text. Scripts, shortcuts to automate the repetitive stuff, things like that helped greatly. But moving elements around with a mouse is the most frustrating and literally painful thing I have ever done on a computer. I shied away from applications that couldn't be edited directly for that reason.\n\nThen again, I'm strange.\n\nDerek"
    author: "dkite"
  - subject: "wiki?"
    date: 2003-02-13
    body: "There have been some many suggestions on this list that are quite useful to many. Perhaps a wiki isn't such a bad idea for bundling these.\n\nGiven a nice frontend it might help lower the entry barrier by giving beginners a nice introduction and possibility to write down their experiences.\n"
    author: "Patrick Smits"
  - subject: "My favourite line"
    date: 2003-02-13
    body: "\"Screenshot showing KWrite. This was surely better than Notepad, ...\"\n\n:-)"
    author: "Kavau"
  - subject: "Re: My favourite line"
    date: 2003-02-14
    body: "Yes, I enjoyed that one too. Next we'll be told that Konsole\nis at least as good as MS-DOS.\n"
    author: "Liam O'Toole"
  - subject: "App descriptions in KDE menus"
    date: 2003-02-13
    body: "Aaron, this one goes for you :)\n\nHow do you guys think we could implement some kind of descriptions for all the inventive names we come up with for applications?\n\nThey can be pretty confusing for first time users. \n\nThanks,"
    author: "exa"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-14
    body: "already done. put a GenericName item in your .desktop file and turn on \"Name (Description)\" or \"Description (Name)\" format in the panel config panel (Layout -> Menus). i don't know if this is the default setting though (i'm not at home to check right now) ... if it isn't, it should be =)"
    author: "Aaron J. Seigo"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-14
    body: "wow!\n\nthanks for the info Aaron, keep up the good work!\n\n"
    author: "exa"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-15
    body: "Someone needs to explain this quickly to the guys at Red Hat.\n\nFor example, the \"Network Configuration Wizard\" looks like this if you enable it:\n\nNetwork COnfiguration Wizard (Network Configuration Wizard)\n\nWhich is:\n\na) Wrong\nb) Useless\nc) Waaaaaaay too wide ;-)\n\nThis happens for pretty much every non-KDE app in RH 8.0\u00b4s redhat-menu packages."
    author: "Roberto Alsina"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-15
    body: "No, I think it has to be explained to the KDE folks.  :-)\n\nPutting these parentheses in the menu is ridiculous..."
    author: "Navindra Umanee"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-14
    body: "Already implemented :-)\n\nKControl Desktop group, Panels control panel, Menu tab, choose \"Menu item format: Description (name)\"\n\nGo KDE usability team!"
    author: "not me"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-14
    body: "No offense but my menus looks absolutely ridiculous now:\n\nK->Games->\nArcade Game (KAsteroids)\nArcade Game  (KBounce)\nArcade Game  (KFoulEggs)\n...\n\nI kid you not...  Can't we at least have descriptive descriptions? :-)\n\nMaybe the default menu format should be \"Menu item format: Name Description\"."
    author: "Navindra Umanee"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-14
    body: "(btw i believe the above menu format is the default since i don't recall changing anything myself but I could be wrong)"
    author: "Navindra Umanee"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-14
    body: "this is exactly the sort of project that is perfect for someone who isn't a coder ... there are a number of such jobs available around KDE, actually..."
    author: "Aaron J. Seigo"
  - subject: "Re: App descriptions in KDE menus"
    date: 2003-02-14
    body: "I think putting parentheses in the menu in the first place is a visual and consequently usability mistake.  Why not get it right the first time, removing the need for such clutter?\n\nExamples: \"Konqueror Web Browser\", \"Evolution Mail Client\"\n"
    author: "Navindra Umanee"
  - subject: "Quanta in Knoppix 3.1 ?"
    date: 2003-02-13
    body: "OK, after reading this, I tried to find Quanta in the Knoppix 3.1\ndistro, but failed. Can someone give me a clue as to where it's\nhidden ? Doesn't seem to be hanging off any off the menus, AFAICS.\n"
    author: "sjc"
  - subject: "Re: Quanta in Knoppix 3.1 ?"
    date: 2003-02-14
    body: "Another magic key combo: Press Alt+F2 and type the program name. \n\n(Hey Aaron, how about extending address/run/command interfaces to not only check bins and existing kio slaves but also the full names and descriptions used in kmenu? Or extending it with features Launchbar for OSX (http://www.obdev.at/products/launchbar/) seem to have as well? ;) )"
    author: "Datschge"
  - subject: "patch"
    date: 2003-02-14
    body: "it is still case sensitive though, which sucks. perhaps i'll work on that next.\n\non the plus side it takes translations into consideration because KService rocks. hurrah.\n\nas for the more complex pattern matching, that'll have to wait for another day unless i suddenly have lots of free time on my hands. but that would likely mean getting hit by a bus or kidnapped by aliens (who have a computer i can hack KDE on), which would suck in its own way."
    author: "Aaron J. Seigo"
  - subject: "Re: patch"
    date: 2003-02-14
    body: "ok. apparently you aren't allowed to preview a posting if you want to attach a file. or at least you need to attach it last, otherwise it doesn't stick. stupid web boards."
    author: "Aaron J. Seigo"
  - subject: "KDE Wiki"
    date: 2003-02-14
    body: "Hi everyone,\n\nI am the author of the KDE review posted on Ammai.com. I have set up a Wiki for all users using KDE. It will contain tips and tricks, things that all beginners should know and other important information.\n\nPlease contribute to this Wiki by going straight to http://www.ammai.com/phpwiki and adding tips and tricks as well as any other information that KDE users will find helpful. There is also a suggestion box for new ideas for the Wiki.\n\nPlease tell all your friends to contribute as well.\nThank you and have fun,\nVinod.\n\n"
    author: "Vinod"
  - subject: "Re: KDE Wiki"
    date: 2003-02-15
    body: "Very good, it's already in KTips.\nThere are so many different sites for KDE/help KDE/Tips and Tricks/Screenshots. It's time to merge all information on kde.org. This is the place for KDE!"
    author: "star-flight"
  - subject: "Re: KDE Wiki"
    date: 2003-02-18
    body: "I couldn't agree more, however I like the initiative ..."
    author: "Patrick Smits"
  - subject: "Kix with KDE 3.1"
    date: 2003-02-19
    body: "Hey Knoppix is relly cool and usefull and now with KDE 3.1 it's great.\n\nI am the developer of Kix the 8cm/minicd Knoppix Edition and the new release with a complete KDE 3.1 is coming! That's right! A complete Linux with KDE and much apps on only 8cms :)!\n\nMore infos will be on http://tukansoft.de maybe this week. \n\nThanks,\nzenok\n\n(thanks for the great article of my/the new community: http://kde-forum.org)"
    author: "zenok"
---
After the <a href="http://dot.kde.org/1043732923/">public release of KDE 3.1</a> it was only a matter of time to see a <a href="http://knoppix.com">Knoppix</a> based Live CD using this version. The <a href="http://lists.kde.org/?l=kde-promo">KDE Promo team</a> worked on several custom versions, one of which was then used at <a href="http://dot.kde.org/1044216380/">Solutions Linux in Paris</a>. The forum for Knoppix customizations, <a href="http://www.knoppix.net/">Knoppix.net</a>, then went ahead and offered a custom <a href="http://www.knoppix.net/docs/index.php/KnoppixKDE">Knoppix KDE edition</a> containing KDE 3.1 and many more KDE applications. <a href="mailto:vignesh@ammai.com">Vinod Kumar</a>, one of the editors of <a href="http://ammai.com/">Ammai.com</a> and also a Microsoft Certified Professional in VB 6.0 Desktop Applications, chose the above mentioned Knoppix KDE edition for <a href="http://ammai.com/modules.php?op=modload&name=Reviews&file=index&req=showcontent&id=2&page=1">trying Linux for the first time ever</a>. He gives 4.5 out of 5 and concludes: "<i>Being the first time I used Linux, I was highly satisfied. (...) the entire experience was wonderful. (...) What is even more amazing is that because of Knoppix, Linux is closer to anyone who wants to try it risk-free. I plan on using Knoppix more and more until I get completely familiar with it, then I will most likely install Linux on my hard drive.</i>"
<!--break-->
