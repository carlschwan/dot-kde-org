---
title: "KDE 3.1 Packages Available for AIX"
date:    2003-10-14
authors:
  - "gstaikos"
slug:    kde-31-packages-available-aix
comments:
  - subject: "Too late!"
    date: 2003-10-14
    body: "Arrghhhh! My AIX license has been revoced!"
    author: "David"
  - subject: "Not Linux compatible?!"
    date: 2003-10-14
    body: "Hi,\n\ncan you explain in what regions the AIX 5L is not Linux compatible? In my understanding the new Linux compatability of AIX should have solved everything, but seemingly not?\n\nThanks, Kay"
    author: "Debian User"
  - subject: "Re: Not Linux compatible?!"
    date: 2003-10-15
    body: "Just having a quick browse of the patches it looks like most of the fixes are either for a) interfacing with userland tools or b) non-GCC fixes, i.e. using the AIX cc. Some of the items in b) appear to be fairly valid cleanups (like checking whether TRUE if #ifdef'd before #define-ing it again)"
    author: "Rich"
  - subject: "Re: Not Linux compatible?!"
    date: 2003-10-16
    body: "Yes lots of them are valid cleanups, but a few work around some unfortunate compiler bugs.  Testcases have been sent to IBM and hopefully we'll get fixes before KDE 3.2.0 comes out.  Otherwise we're going to have to keep a couple of patches outside of CVS still.  It really is only a couple though.  Most of the arts, kdelibs and kdebase patches are already in CVS-HEAD.  I'm working on some higher level modules now."
    author: "George Staikos"
  - subject: "interesting..."
    date: 2003-10-14
    body: "This article is interesting and I classify it as \"non-gcc KDE\" series as this one for example: http://dot.kde.org/1064340634/\n\nWhat is interesting for me is what are the other compilers/platforms that KDE is compilable - other than gnu. For now I know it's possible on sun's and ibm's compilers may be it could also possible soon to compile with microsoft's and borland's compilers on Windows having cigwin and winqt or also with the compilers from intel or symantec.\nAlso btw are there other compiles except gcc that are opensource under UNIX environment that are possible to compile KDE?"
    author: "Anton Velev"
  - subject: "Re: interesting..."
    date: 2003-10-15
    body: "Yeah it is already potentially possible, e.g. KDE libs, KOffice and notably - Kexi - compiles with msvc 6, although sources are not mergef with kde cvs ;)\n\nhttp://www.iidea.pl/~js/qkw/\n\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: interesting..."
    date: 2003-10-16
    body: "Nice touch Jaroslaw!\nI am figuring out that your effort makes it possible an exchange of user experience with a much larger and stronger user group - the windows user. Having the KDE apps available under windows will be a real challenge for making them competitive on alien platform that has virtually unlimited cometition."
    author: "Anton Velev"
  - subject: "Re: interesting..."
    date: 2003-10-16
    body: "Yeah, interesting aspect, Anton. That also means additional challenge for KDE libs, when we are entering on win32 developers area."
    author: "Jaroslaw Staniek"
  - subject: "bcc"
    date: 2003-10-16
    body: "Btw will your port compile with bcc (the borland's compiler)?"
    author: "Anton Velev"
  - subject: "Re: bcc"
    date: 2003-10-16
    body: "Yes, especially since Qt 3.2.2 bcc is supported also for win9x. I think, this would require just few options for qmake. The same for gcc for win32. Only potential problem can be with name mangling incompatibilities between compilers: since with KDE we tend to use so many libraries, it may be difficult e.g. to run msvc-compiled app using bcc-compiled library... Hope I am wrong, alhough even using the same build system for development (what I am doing) there is so messy situations when compiling and linking on win32..."
    author: "Jaroslaw Staniek"
  - subject: "Re: bcc"
    date: 2003-10-18
    body: "Thanks for the info I will give it a try when I have time."
    author: "Anton Velev"
  - subject: "Re: interesting..."
    date: 2003-10-18
    body: "What about Intels Compiler?"
    author: "Gerd"
  - subject: "Why not gcc ?"
    date: 2003-10-14
    body: "I am wondering. Why are they not using gcc and the GNU toolchain ? Performance reasons or something more profound ? Thanx"
    author: "MandrakeUser"
  - subject: "Re: Why not gcc ?"
    date: 2003-10-15
    body: "Well, IBM compilers should generate far more efficient code for their RS6000/Power3/Power4 CPUs. AFAIK AIX5l is 64bit O.S. ... and being the CPU a RISC processor, the compiler has strong influence on the performance."
    author: "Pato"
  - subject: "Re: Why not gcc ?"
    date: 2003-10-15
    body: "Thanks Pato, that's sort of what I was aiming at. BTW, having KDE compile with several compilers (and merging the necessary changes in) should help clean up code and get a more standard product (as long as the changes are not of the form \"IFDEF AIX whatever\", that is, there is no conditional patching, just a more generic code is written). "
    author: "MandrakeUser"
  - subject: "Re: Why not gcc ?"
    date: 2003-10-28
    body: "I cannot agree more ;-)"
    author: "Pato"
  - subject: "Where are ASPLinux 9 KDE 3.1.4 packages ?"
    date: 2003-10-16
    body: "Where are ASPLinux 9 KDE 3.1.4 packages ? It was there in the ftp but not present in any mirror. "
    author: "AspLinux User"
---
<a href="http://www.staikos.net/">Staikos Computing Services Inc.</a> is <a href="http://www.staikos.net/news/getarticle.php?id=200310140002">proud to announce</a> the immediate availability of KDE 3.1.4 packages for AIX 5L! Many thanks go to Reza Arbab and <a href="http://www.ibm.com/">IBM</a>, without whom this could not have been possible. Be sure to <a href="http://www.staikos.net/kde/aix/aixfaq.php">visit the FAQ</a> for more information.  All source patches are <a href="http://download.kde.org/stable/3.1.4/AIX">available on the download site</a> and we are working hard to integrate them into KDE CVS for KDE 3.2. You can download the packages from <a href="http://download.kde.org/">your nearest KDE FTP mirror</a>.
<!--break-->
