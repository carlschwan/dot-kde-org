---
title: "KDE Tips & Tricks in Mandrake 9.0"
date:    2003-01-07
authors:
  - "plavigna"
slug:    kde-tips-tricks-mandrake-90
comments:
  - subject: "Myf favorite trick?"
    date: 2003-01-07
    body: "Previewing a sound file by passing the mouse over it in Konqueror has got to be my  favorite!"
    author: "DaSheister"
  - subject: "Re: Myf favorite trick?"
    date: 2003-01-08
    body: "I love it too.\n\nUnfortunately bug #45673 http://bugs.kde.org/show_bug.cgi?id=45673 drives me nuts when it is turned on.  Maybe it is just because I have a messy clutter of open windows all the time, but I am constantly triggering previews that don't stop until I wave my mouse back over the correct window.  I usually have to turn it on and ass I need it.\n\nBut this is definitly a killer feature.  When I am picking songs for a cd I can just hover, drag, and drop."
    author: "theorz"
  - subject: "And why are not these tricks documented?"
    date: 2003-01-07
    body: "IMHO, all these tricks should be documented - just so that the developers themselves be aware of what exists and what may be unintentionally damaged when doing changes. There are a lot of usable features in KDE not even all developers know about - such as drag & drop to taskbar button to bring the window upmost. This function was dropped when \"window grouping\" was introduced, and re-inserted later."
    author: "Alex"
  - subject: "Re: And why are not these tricks documented?"
    date: 2003-01-07
    body: "Yeah, every app should come with an extensive list of use cases of expected functionality that volunteers can check. Compiling something like that for even a single app is quite a bit of work, though.\n\nMarc"
    author: "Marc Mutz"
  - subject: "Re: And why are not these tricks documented?"
    date: 2003-01-07
    body: "Why didn't you document them?\n\nThe answer from you might relate to why others didn't do it.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: And why are not these tricks documented?"
    date: 2003-01-08
    body: "Well, probably because Marc already does a LOT for KDE. The chance is\nhigh that you use his code every day (at least I do) and he has already\nwritten a lot docu."
    author: "Carsten"
  - subject: "Re: And why are not these tricks documented?"
    date: 2003-01-09
    body: "Debian User replies to Alex not to Marc.\nDoes Alex do also a lot of coding in KDE? Maybe.\n\nHave fun...\nSeven"
    author: "Seven"
  - subject: "One example of other needed tips&tricks"
    date: 2003-01-07
    body: "in SuSE 8.1 i have an mplayer entry in Right-Button-Menu of the DVD icon on my Desktop. I just wonder, how can i add xine there, for example?\n\n"
    author: "SHiFT"
  - subject: "Re: One example of other needed tips&tricks"
    date: 2003-01-07
    body: "Right click on the file, and choose Edit File Type."
    author: "LukeyBoy"
  - subject: "Re: One example of other needed tips&tricks"
    date: 2003-01-07
    body: "\nat work so I don't have KDE in front of me, but roughly, you need to add a viewer for the appropriate mimetype, which is inode/blockdevice unless there's something more DVD specific - you'll know you've found it when you find mplayer as the handler for it :)\n\n"
    author: "cbcbcb"
  - subject: "my favourite is"
    date: 2003-01-07
    body: "auto-scrolling in konqueror ( Shift + Down starts the web page\nscrolling slowly i.e. about reading speed )."
    author: "CPH"
  - subject: "Re: my favourite is"
    date: 2003-01-07
    body: "You can increase the speed and reverse the direction too. :)"
    author: "anon"
  - subject: "Re: my favourite is"
    date: 2003-01-07
    body: "In which version of Konqueror? KDE 3.0.4 dosn't seem to have this feature.\n\nSarang"
    author: "Sarang"
  - subject: "Re: my favourite is"
    date: 2003-01-07
    body: "It's a new feature of KDE 3.1. :-)"
    author: "Anonymous"
  - subject: "Re: my favourite is"
    date: 2003-01-08
    body: "thats a good one!"
    author: "gunnar"
  - subject: "Re: my favourite is"
    date: 2003-01-08
    body: "yes, that's good. and it goes along well with holding down the shift key and scrolling the mouse wheel up or down to adjust the zoom level. when reading lengthy pages, i'll often zoom to the right size and then start it autoscrolling..."
    author: "Aaron J. Seigo"
  - subject: "Re: my favourite is"
    date: 2003-01-08
    body: "And CRTL stops it, or you can arrow in the opposite direction until it slows down to a stop.\n\nBut very cool, in IE I would click on the mouse wheel for that little circle with the arrows in it, postion the cursor close to the bottom of the circle and I could have it scroll slowly that way. But this is much easier to use.\n\nAnd now, playing with this text box, shift-up or shift-down highlights a line. CRTL-K deletes a line, but it doesn't copy the line the way pico/nano does. I wouldn't suggest this as an idea since then I would probably get the ever present \"code it yourself\" message."
    author: "Ian Monroe"
  - subject: "Re: my favourite is"
    date: 2003-01-08
    body: "well, this isn't a \"code it yourself\" message, but how about a \"report it yourself\" message? skip on over to http://bugs.kde.org and enter it as a wishlist item..."
    author: "Aaron J. Seigo"
  - subject: "GTK Yuck"
    date: 2003-01-07
    body: "We should only push distos that are KDE only. These gtk based distros are just poorly done and inconsistent. Think Lycoris and Xandros and Lindows. No gtk configs and all KDE. Now if we could just get these KDE only distros to get off there butts and start KDE development :) Maybe an email campaign is in order or a web petition or something.\n-- \nCraig Black ICQ# 103920729\nYahoo IM blackfam972\n-- "
    author: "Kraig"
  - subject: "Re: GTK Yuck"
    date: 2003-01-08
    body: "you narrow-minded small human!\n...\nI guess you're not a programmer, right... Yeah, I thought so.\nTry to do something constructive first, before you bitch on other people's blood, sweat, and tears.\n"
    author: "dwt"
  - subject: "Re: GTK Yuck"
    date: 2003-01-08
    body: "Don't think any KDE developer wastes a second to think about bashing other peoples projects (and it's good this way), and even I as a user don't believe that you are right."
    author: "Thomas"
  - subject: "substring completion"
    date: 2003-01-08
    body: "Only remember part of a URL you recently visited? All you know is it had \"kde\" somewhere? Give focus to the location bar and press Ctrl-T. It will list all the urls in the history with \"kde\" in them.\n\nWell, probably it will launch konsole for you, you first have to fiddle with the shortcuts to make this work, as the \"Terminal\" shortcut conflicts with it :}"
    author: "Carsten Pfeiffer"
  - subject: "tip or trick for KDE not to ignore /etc/profile?"
    date: 2003-01-08
    body: "In Mandrake 9 (and maybe even as early as 8), when you start KDE none of the environment variable stuff handled in /etc/profile is done. So, system variables like PATH aren't set correctly as they are under Gnome or with virtual terminals. Does anyone know the tip or trick to fix this? It's *so* annoying!"
    author: "Leston Buell"
  - subject: "Re: tip or trick for KDE not to ignore /etc/profile?"
    date: 2003-01-08
    body: "Try to change the first line of startkde to \"#!/bin/sh --login\". Not that I think it's a great idea."
    author: "Anonymous"
  - subject: "Another Vote"
    date: 2003-01-08
    body: "Don't forget to vote here too!  Everybody at GNOME has already.\n\nhttp://www.linuxformat.co.uk/modules.php?op=modload&name=Awards&file=index"
    author: "anon"
  - subject: "Enhanced web browsing"
    date: 2003-01-12
    body: "is my favorite. It gives much of my work environment at my fingertips. Google, Rpmfind and my own shortcuts for the intranet wikis, cvs, commonly used documentation etc. Five taps on the keyboard and it's there."
    author: "Robin Rosenberg"
  - subject: "Can you help me; please !!!"
    date: 2003-03-27
    body: "Thank you for having helped me to use GUI effects !!!\nNow, I would like the icons of my KBar to appear in 72 x 72, and make them grow to 128 x 128 with my mouse (like Mac OS X...). Can you help me ? Because 48 x 48 is really too small.\nThanks you so much !!!"
    author: "Christophe"
---
Two <a href="http://www.trylinuxSD.com/kde1/">new</a>
<a href="http://www.trylinuxSD.com/kde2/">chapters</a> have been released on the <a href="http://www.trylinuxSD.com/">trylinuxSD.com</a> website titled "KDE Tips & Tricks in Mandrake 9.0" which offer some KDE3 tips that may not be so obvious to new users. Although the pages are geared toward Mandrake Linux 9.0, much of the content should also apply to anyone using KDE. Please note: The site makes extensive use of screenshots, so dialup users be warned.


<!--break-->
<p>
<a href="http://www.trylinuxSD.com/kde1/">Part 1:</a>
<ul>
<li>    Using 'Split View'; Displaying/Sorting files & folders</li>
<li>    View Profiles; Konqueror Shortcuts</li>
<li>    Konqueror for the Web & FTP; Bookmarks</li>
<li>    Fixing default fonts in 9.0; Speeding up KDE</li>
<li>    KDE Shortcuts; Improving the desktop with anti-aliased fonts</li>
<li>    Installing TrueType and Postscript fonts with DrakFont and KDE's Font Installer</li>
    </ul>

<a href="http://www.trylinuxSD.com/kde2/">Part 2:</a>
<ul>
<li>    Improving the appearance of KDE with desktop wallpaper, transparent menu, styles, themes & icons</li>
<li>    Menu Shortcuts; Applets</li>
<li>    KDEaddons; 'Sharing' Applet</li>
<li>    'Edutainment' Package; Killing Troublesome Apps</li>
<li>    Using the Konsole; the Power of 'root'</li>
<li>    Introduction to KDE System Administration Utilities; Save Session</li>
    </ul>

What are some of your favorite KDE tips & tricks?


