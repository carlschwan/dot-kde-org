---
title: "KDE-CVS-Digest for April 4, 2003"
date:    2003-04-05
authors:
  - "dkite"
slug:    kde-cvs-digest-april-4-2003
comments:
  - subject: "1st Post"
    date: 2003-04-05
    body: "Just so 138 doesn't get the 1st post again :)\n\nMaybe 138 found the life he was looking for and that's why I am now the first poster... :/\n\nOh, and btw, thanks Derek. "
    author: "JC"
  - subject: "YAYYAYYAYAYAYAY"
    date: 2003-04-05
    body: "Wow.. I think this was the most hidden jewel that I've found in Derek's digests so far: \n\n\nZack Rusin committed a change to kdebase/konqueror\n\nAnd that fixes 47814 (which is #1 on the list of the most hated bugs). I'm not\nsure whether we should have some kind of special page with a sign that says \"loading\"\nor just an empty page like we'll have now.\n\nRefer to Bug 47814 - tab open delays when it can't reach website..\n\n\n\nNow, I think I can actually use Konqueror a my web browser again instead of Phoenix :)\n\n\n"
    author: "wewt"
  - subject: "Re: YAYYAYYAYAYAYAY"
    date: 2003-04-05
    body: "Yahooo!  Go Zack!"
    author: "anon"
  - subject: "Re: YAYYAYYAYAYAYAY"
    date: 2003-04-05
    body: "Even better: If you read the comments on that bug at bugs.kde.org, you'll find that the annoying habit of konqueror to repaint the entire browser window when new tabs are created also seems to have been fixed. Yay!"
    author: "optikSmoke"
  - subject: "Does that *really* work?"
    date: 2003-04-05
    body: "\"Support for running kwifi via a simple antenna (...) Grab an old mouse and rip off the chord (...) \"\n\nO wait, wait, wait.... are you telling me that one can build a wireless network in such a simple way? Does this thing even work?\n\nThat was 1st of April... should I believe it?"
    author: "uga"
  - subject: "Re: Does that *really* work?"
    date: 2003-04-05
    body: "LOL - that one's a gem!"
    author: "anon"
  - subject: "Re: Does that *really* work?"
    date: 2003-04-05
    body: "It's a shame that it was a joke though. Who needs Centrino with one of these? ;-)"
    author: "uga"
  - subject: "What happened to Safari merges?"
    date: 2003-04-05
    body: "What happened to Apple merges? We can't see anything in CVS Digest about this for three or four weeks. Is it stuck? \nAlso Kopete is very silent. \nI'm afraid my grandson will use the Safari-improved Konqueror \"the Browser\".\n"
    author: "thelihgtning"
  - subject: "Open up the informations"
    date: 2003-04-05
    body: "We really need someone reporting from the private khtml/kjs/konqueror/webcore/safari mailing list..."
    author: "Datschge"
  - subject: "Re: What happened to Safari merges?"
    date: 2003-04-05
    body: "Remember, this is a 'digest', hopefully a representative sample of the commits. There were 1600 or so this last week, and the digest shows less than 100. Kopete continues to be developed. I hope to have one of the developers do a write-up for us. They usually generate 80 or so commits a week.\n\n>What happened to Apple merges\n\nI think what we are seeing is the ebb and flow of the development process. A month or so ago the large changes from  safari were merged. Look at the bug reports for konqueror. The developers are probably focussing on them. \n\nDerek\n"
    author: "Derek Kite"
  - subject: "Re: What happened to Safari merges?"
    date: 2003-04-07
    body: "I think a lot of people are over estimating the scale and impact of the Safari changes....\n\nMost of them are probably fairly boring little bugfixes.\n\nThat's not to say that I think the Safari changes are irrelevant, but just that I think they'll be indistinguishable from the normal ongoing improvement of khtml.\n\nI don't think you'll ever wake up one moring and suddenly have a 'Safari enhanced' Konqeror...\n\nAfterall, even if the changes were significant, they're only to khtml...and possibly kjs"
    author: "Stuart Herring"
  - subject: "Re: What happened to Safari merges?"
    date: 2003-04-07
    body: "Improving KHTML and KJS is pretty important because a good browser is so essential in todays networked world. The Safari development will certainly speed up the development proces."
    author: "Joergen Ramskov"
  - subject: "Re: What happened to Safari merges?"
    date: 2003-04-07
    body: "Well, it's a first involvement of a rather big and very popular IT company in a fully (L)GPL'ed project so the impact of this decision shouldn't be under estimated, especially considering that it might encourage other companies to take over some particular code and contribute back improvements instead doing something proprietary on their own. Considering this it's a little sad that this all isn't happening in an open fashion like we were used to before."
    author: "Datschge"
  - subject: "Bug"
    date: 2003-04-05
    body: "\"programs are runned from home directory instead of program directory\"\nThat's also a bug in Konqueror. When you click i.e. on a shell script, it\nexecutes in the home directory which is pretty confusing and annoying.\nWorkaround:\nUse\ncd `dirname $0`\nas the first line of a script.\n"
    author: "Martin"
  - subject: "Re: Bug"
    date: 2003-04-06
    body: "May that cause any security related problems?"
    author: "Hein"
  - subject: "kcmdhcpd"
    date: 2003-04-05
    body: "New website and screenshots can be found here\n\nhttp://www.linux-lovers.be/index.php?show=kcmdhcpd\n"
    author: "Willy De la Court"
  - subject: "Re: kcmdhcpd"
    date: 2007-03-20
    body: "Where on earth can I report bugs for this tool, the README says bug.kde.org but I see no reference to this particular tool in the drop down list.  all information on this tool appears to be a little sparse."
    author: "DM"
  - subject: "Whoa, 29105 is crushed"
    date: 2003-04-05
    body: "Bug 29105 http://bugs.kde.org/show_bug.cgi?id=29105 is fixed, now you can press the WinKey and K-Menu pops up, just like in Windows, wohoooo :)\n\nI just hope it could be poped up above the K button but that's way minor compared to having the WinKey->K-Menu functionality.\n\nGreets to Ellis Whitehead"
    author: "Tar"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-05
    body: "I posted http://bugs.kde.org/show_bug.cgi?id=56882 since to my surprise I didn't find any bug report related to Kmenu not popping up at the K button (it has been mentioned on mailing lists before but obviously noone really cared that much so far). Please consider voting for it (and promote the ability to vote for wishes/bugs in general since it's a good way to find out what's wanted/popular)."
    author: "Datschge"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "http://bugs.kde.org/show_bug.cgi?id=49601\nhttp://bugs.kde.org/show_bug.cgi?id=50510\n\nPerhaps"
    author: "Tar"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-07
    body: "Thanks."
    author: "Datschge"
  - subject: "KdeKeyboard"
    date: 2003-04-05
    body: "Hopefuly, some day thinkgeek.com or getdigital.de will offer keyboards with\nKDE key instead of Win key ;)"
    author: "Anton Velev"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-06
    body: "I have one with a Corel key :-)"
    author: "Roberto Alsina"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-06
    body: "I think it would be even more important (before a KMenu key ;-) to have support for all those \"multimedia keys\" especially found in laptops and some multimedia keyboards. It's a shame that there seems to be no standard on how to access this stuff...."
    author: "uga"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-06
    body: "I think khotkeys has some stuff on that, but I may be remembering wrong."
    author: "Roberto Alsina"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-06
    body: "Not really. From what I know at least, khotkeys works only with normal key combinations (shift, ctrl, alt, altGr...). I'm referring to special keys like \"MailReader\", \"WebBrowser\"... which are present in most laptops now. They don't seem to be linked to any strange key combination, so I can't make use of them.\n\nI found a service in kde called \"kmilo\" (KDE Special Key Notifier), but I don't know how it works or how to configure it..."
    author: "uga"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-06
    body: "Here's what you want: \"Internet Keyboards and KDE\"\n<p>\nNot terribly simple, though.\n<p>\n<a href=\"http://www.warpedsystems.sk.ca/modules.php?op=modload&name=News&file=article&sid=475\">[warpedsystems.sk.ca]</a>\n"
    author: "Roberto Alsina"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-06
    body: "Thanks for the link, but I had tried that already. The keys that I have in my case don't seem to be mapped as normal keys, since xev doesn't show anything when I press them. Anyway, thanks a lot."
    author: "uga"
  - subject: "Sounds like you have a thinkpad"
    date: 2003-04-07
    body: "So you might want to check out the thinkpad button project,works for me...\n\nhttp://savannah.nongnu.org/projects/tpb/"
    author: "Moritz Moeller-HErrmann"
  - subject: "Re: Sounds like you have a thinkpad"
    date: 2003-04-07
    body: "It's not a thinkpad. It's a Compal N-30N3 series laptop which is sold by plenty of brands ( See for example http://www.singular.org/~gary/laptop/), but it could be a similar problem. I'll have a look at that project. Thanks."
    author: "uga"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-07
    body: "&lt;shamless plug&gt;<br>\n<a href=\"http://lineak.sourceforge.net\">lineak</a> (<a href=\"http://lineak.sourceforge.net/screenshots/klineakconfig-0.5pre2.png\">screenshot</a>)<br>\n&lt;/shameless plug&gt;<br>\n"
    author: "Sheldon"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-07
    body: "Looks cool. How about moving it into KDE CVS and making it a part of its Control Center?"
    author: "Datschge"
  - subject: "Re: KdeKeyboard"
    date: 2003-04-07
    body: "KDE does have support for those keys. All you need to do is to configure your keyboard model correctly (e.g. in the KDE keyboard kcontrol module, select something else than pc101).\n"
    author: "L.Lunak"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "> I just hope it could be poped up above the K button\n\nDoesn't it? It does for me. CVS from 05 Apr 2003 01:40 GMT approximately.\n\nI just press the \"Win\" key, and the menu pops up right on top of the K Menu. Move around the button, and it will follow."
    author: "uga"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "> Doesn't it? It does for me. CVS from 05 Apr 2003 01:40 GMT approximately.\n> \n> I just press the \"Win\" key, and the menu pops up right on top of the\n> K Menu. Move around the button, and it will follow.\n\nEhmm, I can't confirm that now but if it really works like that then let me utter another WHOAAA :)\n\nThumbs up for KDE devs :)\n\nCan anyone else confirm that or am I just dreaming?\n\nMinor annoyances of Win32 junkies are being crushed, 3.2 is looking sweeter every day :)"
    author: "Tar"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "Yes, but how can I navigate this menu without using the mouse mouse?\n"
    author: "cm"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "s/mouse mouse/mouse/"
    author: "cm"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "It doesn't seem to be working with keyboard cursors right now. I press [up] or [down] keys, and the KMenu gets the focus temporally, but then if I press again, the app right below is the one that has the focus....."
    author: "uga"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "You can type in first letter of the app/menu name to select it.  i.e. Alt-F1, A Konq, <enter> selects Applications, Konqueror, and runs it\n"
    author: "SadEagle"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "What version of KDE do you have? It doesn't work for me.At least in the cvs version, there's no \"konqueror\" under \"applications\" Konqueror is in \"Networking\"\n\nAlso, if I press the Win Key, I get the menu. Then for example if I want \"Graphics\" I press G, but I get \"Games\" instead, and no chance to move it anywhere else. It may be fixed yesterday or today?"
    author: "uga"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "Argh, sorry, I meant Networking. As to the other part: if you press G and get games, just press \"r\" to move to _Gr_aphics. It'll enter the menu as soon as you type enough chars to diambiguate. "
    author: "SadEagle"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "mmmmmhh... it doesn't work here. It maybe a new bug. Thanks anyway."
    author: "uga"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "I sure hope the old behavior is still possible.\nMandrake fixed this with a simple script which adresses a bogus function to the windows keys. It's only a matter then of having kde make the menu pop up with this function wich is done easily in kcontrol.\nThat way it's very simple to get the windows key back as a meta by commenting out the proper line in the mandrake script. Which is important to me because i use it to move my windows around, because i need that alt + mouse does not move the windows, because alt + mouse in gimp makes the selections move without moving their content wich is very important to obtain certain results. "
    author: "jay say say"
  - subject: "Re: Whoa, 29105 is crushed"
    date: 2003-04-06
    body: "And maybe it's time for a shameless plug on a bug 51891 ;)\nhttp://bugs.kde.org/show_bug.cgi?id=51891\n\"redmond theme maximized shows top/bottom border\"\n\nAgain annoys the shit out of Win32 converters :/"
    author: "Tar"
  - subject: "KcmDhcpd needs a wizard"
    date: 2003-04-06
    body: "KcmDhcpd looks nice, but it would be nicer with a wizard, for fast configuration the DHCP-server."
    author: "Markus Gans"
  - subject: "CVS"
    date: 2003-04-07
    body: "i think i'm gonna compile kde from CVS... Dangerous isn't it?"
    author: "Anonymous Coward"
  - subject: "Re: CVS"
    date: 2003-04-07
    body: "Not really.  Just make sure you don't overwrite the current KDE\n\nI usually compile it as:\n\nmake -f Makefile.cvs\n./configure --with-qt-dir=/usr/local/kdecvs/qt-copy --prefix=/usr/local/kdecvs\nmake\nmake install\n\nThis assumes you have a version of qt-copy installed in /usr/local/kdecvs/qt-copy, and will install kde in /usr/local/kdecvs\n\n\nThis way, you can have two versions of kde installed in the system. Your distro's one, probably in /usr, and yours under /usr/local. You can choose which one to use in KDM.\n\nI almost forgot. It's better not to mix your current KDE directory and the CVS one. Just use $KDEHOME=$HOME/.kdecvs for example."
    author: "uga"
  - subject: "Re: CVS"
    date: 2003-04-07
    body: "I remember something about a configure option kdebase package, \n\n--disable-kdm\n\nor something like that. Otherwise you'd might mess up your stable KDM (KDE Display Manager)"
    author: "Gert-Jan van der Heiden"
  - subject: "Re: CVS"
    date: 2003-04-07
    body: "? I've had no problem like that. I have two versions of kdm. One that belongs to the system, one that belongs to the CVS. It doesn't touch the system's one. But it's true that I had to touch a bit the CVS one so it worked."
    author: "uga"
  - subject: "Re: CVS"
    date: 2003-04-07
    body: ">>>i think i'm gonna compile kde from CVS... Dangerous isn't it?\n\nyeah, living on the edge"
    author: "yeah"
---
Featured <a href="http://members.shaw.ca/dkite/apr42003.html">this week</a> in KDE-CVS-Digest:  Continuous improvements to the development tools, with <a href="http://quanta.sourceforge.net">Quanta</a>,
<a href="http://kate.kde.org">Kate</a> and <a href="http://www.kdevelop.org">KDevelop</a> getting optimizations and bug fixes, and dcoppython and kjsembed in <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/">kdebindings</a> getting some love.
Also covered, new and improved filters in <a href="http://www.koffice.org/">KOffice</a>, the large rewrite of <a href="http://edu.kde.org/kig/">Kig</a>
is complete, work on new themes and theme engine, info from the new <a href="http://www.linux-lovers.be/index.php?show=kcmdhcpd
">KcmDhcpd</a> maintainer (<A href="http://www.linux-lovers.be/index.php?show=screen_kcmdhcpd">screenshots</a>), and an exciting <A href="http://lists.kde.org/?l=kde-cvs&m=104916444618418&w=2">new feature</a> in kwireless. Plus, news on the kernel concepts KDE t-shirts (<a href="http://www.kernelconcepts.de/products/kde/index-en.shtml">pictures</a>, <a href="http://events.kde.org/registration/register.phtml">order information</a>) and more.  Get it all <a href="http://members.shaw.ca/dkite/apr42003.html">right here</a> folks.
<!--break-->
