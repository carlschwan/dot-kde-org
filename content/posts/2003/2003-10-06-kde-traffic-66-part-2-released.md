---
title: "KDE Traffic #66 Part 2 Released"
date:    2003-10-06
authors:
  - "rmiller"
slug:    kde-traffic-66-part-2-released
comments:
  - subject: "About keyboard shortcuts"
    date: 2003-10-05
    body: "I would really appreciate having the same keyboard shortcuts in KMail and KNode out of the box... "
    author: "Boudewijn Rempt"
  - subject: "weirdos"
    date: 2003-10-06
    body: "If you're going to name the KDE Traffic's part 1 and part 2, at least you should give them the same issue number!"
    author: "ac"
  - subject: "Re: weirdos"
    date: 2003-10-06
    body: "Can't.  Software won't let me.  At least this way, people will know that it's the same one, just in two parts (with two issues).\n\nAnd you're sitting there complaining about issue numbers?  Who's the weirdo? :)"
    author: "Russell Miller"
  - subject: "Re: weirdos"
    date: 2003-10-06
    body: "> And you're sitting there complaining about issue numbers? Who's the weirdo? :)\n\nLOL. Full ACK."
    author: "Mike"
  - subject: "Nice work"
    date: 2003-10-06
    body: "Hey Russel,\njust want to say that i really enjoy your KDE traffic.\n\nThanks man!\n"
    author: "Mike"
  - subject: "Uga?"
    date: 2003-10-06
    body: "> And for being late, here's the icing on the cake (uga).\n\nRussell, you speak Hebrew? ;)"
    author: "Anonymous"
  - subject: "Re: Uga?"
    date: 2003-10-06
    body: "Nope.  Uga is the maintainer of krecipes.  I don't know his real name off the top of my head."
    author: "Russell Miller"
  - subject: "Re: Uga?"
    date: 2003-10-06
    body: "Ha. What a funny coincidence that \"Uga\" is Hebrew for \"cake\"...\nFits nice with the recipes...\n"
    author: "Anonymous"
  - subject: "Re: Uga?"
    date: 2003-10-06
    body: "Nah, don't believe him. He must know Hebrew.... otherwise why would he even mention such a weird word? ;)\n\n\tuga\n\n"
    author: "uga"
  - subject: "Kpovmodeller"
    date: 2003-10-06
    body: "I saw another intresting japanese renderer project: Lucille\nhttp://web.sfc.keio.ac.jp/~syoyo/lucille/index.html\n\nLooks very amazing."
    author: "Wurzelgeist"
  - subject: "Re: Kpovmodeller"
    date: 2003-10-08
    body: "Uh, wow, these screenshots look really impressive!\n\nWe have plans for object and renderer plugins but have not enough active developers in the project at the moment.\n\nSo support for renderers like Renderman or Lucille is not the item with the highest priority on our TODO list."
    author: "Andreas Zehender"
  - subject: "@Michael Thaler"
    date: 2003-10-06
    body: "Fill tool for Kpaint:\n\nSomehow tuxpaint is superior to KPaint... ;-)\nBut also the fill option for Tuxpaint looks like an ugly hack... "
    author: "Magna charta"
  - subject: "Broken link"
    date: 2003-10-06
    body: "The \"Archive Link\" of the fifth thread (KPaint) is broken.\n\nAs a bonus nitpick, the word is \"consensus\", not \"concensus\" (appears twice)."
    author: "em"
  - subject: "storage"
    date: 2003-10-06
    body: "If a storage like ioslave for kde is made, please work with the gnome people.  Put together a library for it, and make it a freedesktop.org project.  The last thing I need is to have my files restricted to one desktop."
    author: "theorz"
  - subject: "Re: storage"
    date: 2003-10-06
    body: "> If a storage like ioslave for kde is made, please work with the gnome\n> people. Put together a library for it, and make it a freedesktop.org\n> project. The last thing I need is to have my files restricted to one\n> desktop.\n\nfull ack!"
    author: "ac"
  - subject: "Re: storage"
    date: 2003-10-06
    body: "I don't understand why people reply \"full ack!\".\n\nIsn't that coming from the abbreviation of \"acknowledged\"? If yes, wouldn't the message of those two words be \"fully acknowledged!\"? Wouldn't that mean that - in this case ac - has fully acknowledged (= read and understood?) what was said?\n\nWell, I don't care. Where would we be if everyone posted that he has read and understood some text?\n\nBut I think that ac meant to say \"I completely agree with you (and I want others to know about that)\". Well, then please say that!\n\nAm I the only one who finds \"full ack\" weird, inappropriate and superflouos? Do only german teenagers say that or is it common in i.e. the states, too?\n\nThis is not meant to offend anyone, I'm truly interested in your opinion!"
    author: "me"
  - subject: "Re: storage"
    date: 2003-10-06
    body: "Aw, me, don't get so down; the kids are all right.  See here for the Jargon File definition of ACK:\n\nhttp://catb.org/esr/jargon/html/A/ACK.html\n\nSo, yes, using ack to signify agreement is a bit of a stretch, even from the original jargon usage; but then, language is fluid, it evolves.  This is what the term has come to mean.  \n\nI have no idea if \"Ack\" is popular in the US (or in Germany for that matter); I have only heard the term in the placeless expanse of the internet ;)"
    author: "LMCBoy"
  - subject: "Re: storage"
    date: 2003-10-07
    body: "I agree with this.  After reading up on GNOME Storage, I've got to say that this would be great!  What, KDE can work with Windows nicely but not GNOME?  Reminds me of BSD zealots; Windows is OK, OS X is insanely great and Linux is crap.\n\nPlease don't tell me I'll have to run a Linux box and use Reiser4 just to get these benefits.  Let's work to make either KDE work with GNOME Storage, or heck, make a KDE server that's compatible with Storage and beats the pants off of the GNOME system. ;-D\n\nNo, I'm not a developer.  Please don't flame me for not being one, or not willing to just \"do it yourself if you want it.\""
    author: "Shane Simmons"
  - subject: "Re: storage"
    date: 2003-10-08
    body: "If you are not a developer yourself, maybe you are rich enough to pay someone else to do it for you? ;-)"
    author: "Andr\u00e9 Somers"
---
<a href="http://kt.zork.net/kde/kde20031004_66.html">KDE Traffic #66 Part 2</a> has now been released on zork.net.  (The <a href="http://www.duskglow.com/kt/">backup site</a> is still available, but is now intended for experimentation and mirroring.)  This traffic contains news on <a href="http://www.kpovmodeler.org/">KPovModeler</a>, the kdesupport module, <a href="http://www.konqueror.org/">Konqueror</a> (what issue would be complete without it?), <a href="http://kmail.kde.org/">KMail</a>, KPaint and last but not least, giant pink fluffy bunnies.  OK, nix the bunnies, but it's still a decent issue.  Look for issue #67 to be released shortly.
<!--break-->
