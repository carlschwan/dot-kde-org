---
title: "linmagau.org: FLOSS in Iran"
date:    2003-12-28
authors:
  - "elektroschock"
slug:    linmagauorg-floss-iran
comments:
  - subject: "I need a mirror :)"
    date: 2003-12-28
    body: "That's strange to see a reverse taskbar with the clock on the left and menu on the right :)\nI wonder why it's not the same with the icons bar ? Lol\n\nBut it's nice to see a Farsi translation with already 50%."
    author: "JC"
  - subject: "Re: I need a mirror :)"
    date: 2003-12-31
    body: "with a notebook you wouldn't have this problem my friend. Go global! But must be a swatch-nb (sand-sealed) :))))\nhahahaha"
    author: "jo\u00e3o"
  - subject: "Re: I need a mirror :)"
    date: 2005-01-14
    body: "how do you get the toolbar to do that? i would very much like to know.\n"
    author: "nick"
  - subject: "??"
    date: 2003-12-28
    body: "the people of bam will be full of joy ....."
    author: "jo"
  - subject: "!"
    date: 2003-12-28
    body: "no need to be sarcastic... (considering the situation in the city of Bam)\n\nhttp://www.icrc.org/web/eng/siteeng0.nsf/f_don?openform"
    author: "Thomas"
  - subject: "Re: ??"
    date: 2003-12-28
    body: "Well, I don't think this sarcasm is appropriate.\nSome regions of the earth are unfortunately quite \nfrequently stricken by natural and/or often man-made\ndesasters. Just inform yourself how many people\nare dying of AIDS in Africa each month. Does that mean\nwe shouldn't release news about KDE or any other technical\nstuff about Africa anymore?? I guess not.\nIn fact technical advances are the only hope for such countries.\nWhy do you think aren't there 20,000 deaths whenever California is hit by\nsuch an earthquake? \nSo it's nice to see that there is also technological progress in such\ncountries and having an OS which supports their native language is\ncertainly quite important. IMO OSS is much more suited to adapt to\n\"minority\" languages cause the market in those countries is still\ntoo small to be _comprehensively_ supported for a commercial company like M$.\nKDE in Farsi (or whatever it was called) is certainly a small step but\nanyway - this is a KDE news site. If you want to have news about the\nearthquake (which is perfectly OK with me, mind you) just look elsewhere.\n\nNow, my personal impression of those screenshots and arabic scripts in \ngeneral: When I see chinese or japanese scripts I always think: This\nis difficult to learn but I can make out different symbols: A box, a line,\netc. Arabic scripts look the hell of confusing to me!!! Did you ever\nsee them on TV? Gee, you always see a wavery line with dots strangely put\naround them. You can't even see where a letter starts or ends. Combined\nwith a low res device like a TV set I sometimes doubt you can actually read\nanything ;-). Oh well, as long as the people concerned can read this...\n\nPS (warning! biased and political! don't read any further if you like Bush):\nIsn't it strikingly ironic that Bush's axis of evil has so quickly spun\naround to just an axis of pity? Probably the money spent on Iraq warfare\ncould have been of much more use over there now. So in the end it's all\nabout the money and not the people in those regions. No need to pretend\notherwise. Still it's nice to see that lots of people over here send money\nand technical support down there anyway - be it privately or else.\nI only hope the Arabic people will recognize this and learn to differentiate.\n"
    author: "Mike"
  - subject: "Re: ??"
    date: 2003-12-28
    body: "Well, the article says they probably MAY not support the market because of export regulations. And there is no Win-Farsi. I know many persian friends, who are advanced computer users. Computers are on the rise in Persia and it is no \"developing country\" but an old civilisation. KDE now has a small competetive advantage."
    author: "gerd"
  - subject: "Re: Africa"
    date: 2003-12-28
    body: "Well, thanks for that reply. Having lived in the west for almost 15 years now, I find that the media and the general public always refer to Africa as one entity. Though this is true as a continent, Africa has more than 50 countries. There are countries where AIDS is almost an non issue. I know this is hard to believe but it's true. Countries like Niger have no AIDS cases to talk about. Incidentally, I do not hear people group people from Canada/US as people from North Americ, or China/Vietnam, Laos as ASIA. I think it would be better to begin refering to countries individually instead of puting forward statements that equate a tragedy in one African country as a tragedy to the whole continent. As far as KDE is concerned, I am supprised about how far it's gone. This keeps me wondering how coders implement features in Arabic script as the photo shows. Keep up the good work guys. My yearly donation is coming soon.\n\nCb.."
    author: "Charles"
  - subject: "Re: Africa"
    date: 2003-12-28
    body: "That's right. Africa is no common ground. And it will be an important issue to get them involved in open source, support their languages (KDe does very well) and help them to combat the diffusion of the former colonial languages because of modern technology. KDe performs very well in the support of native languages."
    author: "Bertram"
  - subject: "Re: Africa"
    date: 2003-12-31
    body: "wake up\nyour backyard is more common ground than anything in Africa\ngo to live there to know what you 're speaking about"
    author: "jo\u00e3o"
  - subject: "Re: Africa"
    date: 2003-12-29
    body: "For what it's worth, I hear Asian people that live in the US always talk about people from China, et al as Asian. I'm guessing that this is not the case within those various countries."
    author: "Jim Jones Freaky"
  - subject: "Re: Africa"
    date: 2003-12-29
    body: "My girlfriend's grandmother is Japanese and she thinks all white people come from \"America\". I think its cute however. :)\n\nI agree with the main point, people from different regions are different and we should make a minimal effort to recognize them as such."
    author: "Ryan"
  - subject: "Re: Africa"
    date: 2003-12-31
    body: "I thought all black people are coming from america at least their soul\nthat's cute too I think"
    author: "jo\u00e3o"
  - subject: "Re: Africa"
    date: 2003-12-29
    body: "\"We spent a lot of time talking about Africa, as we should. Africa is a nation that suffers from incredible disease.\" \n- June 14, 2001, Press Conference "
    author: "gwb"
  - subject: "Re: ??"
    date: 2003-12-28
    body: "I don't have a gripe with interfering to improve lives. But if that's what Iraq was about then why not Burundi and Rwanda. Some don't know this - try google for Tutsi and Hutu. 1 mill people. My gripe with Iraq is the spins, the hype, the lies, the hysteria. It's about reelection, oil, beating up somebody for 911, rebuilding contracts, showoff. And that Saddam is a ruthless dictator as many others is just a convenient excuse. It was an opportunity that could not be passed by. War is not the right word. Neither is interference. Assault is better. But ugly. But in the end it will improve lives in Iraq.\n"
    author: "Claus"
  - subject: "Re: ??"
    date: 2003-12-31
    body: "what you are speaking about? Antropology via google ... yeaah you was the first on the moon or one of them which believe no-one was there, am I right???"
    author: "jo\u00e3o"
  - subject: "Re: ??"
    date: 2003-12-29
    body: "<I>Arabic scripts look the hell of confusing to me!!! Did you ever\nsee them on TV? Gee, you always see a wavery line with dots strangely put\naround them. You can't even see where a letter starts or ends.</I><P>\n\nIt's really just a matter of getting used to. Arabic (and other languages that use similar alphabet) scripture is cursive, so letters (most of them) join with those before and after. This means that letters are written slightly differently based on their position in the word. You tell the beginning of a word from the end of the one before it by observing how a letter is written, and the amount of space left between the letters. Farsi differs slightly from Arabic, and in general has more dots and accents.<P>\n\nKeep in mind that to an Arab with limited exposure to English (not very common these days) will observe cursive English scripture in the exact same bewilderment that you observe Arabic.\n"
    author: "z00z"
  - subject: "Re: ??"
    date: 2003-12-29
    body: "Ah! Oh! Well, dot.kde.org is always good to learn something new\nnot only about KDE. Thanks for your comment.\n"
    author: "Mike"
  - subject: "Re: ??"
    date: 2003-12-31
    body: "especial your last words  ... may be ok but the difference is that they think who writes so is stupid and has has lots of money in his pocket to take and never ever will even be curious about learning writing or reading or thinking so ... so until YOU do as he aspects he is your friend but look like it change when YOU need something\n"
    author: "jo\u00e3o"
  - subject: "Re: ??"
    date: 2003-12-29
    body: "FYI:\n(Iran is not an Arab country.  The citizens of Iran are actually Persian.  Although they use the arabic characters in their writing, farsi is actually not an arabic language.)\n\nThanks for your post.\n\n"
    author: "Anonymous"
  - subject: "Re: ??"
    date: 2003-12-28
    body: "Yes bad timing."
    author: "Kraig "
  - subject: "Re: ??"
    date: 2003-12-28
    body: "Few comments from my side:\n\n- First of all thanks to Elektro Schock for posting this news here. We wish you all the Best for your project.\n\n- The article was written by Aryan Ameri and me. It is a mistake that only my name is given. [Editor: corrected since, sorry]\n\n- Many people have died in Bam, which is a really sad event. Iran is hit by earthquakes quite often and if you have seen Kiarostami's \"Life and nothing else\", which is about the last major earthquake in Northern Iran, then you will know that it is in the Best Old-Iranian tradition (my interpretation) to continue with life and not to let sadness and grief weaken your spirit. Life continues. \n\n- Last but not least we would like to once again thank all KDE people for this wonderfull desktop. We are excited for KDE 3.2\n"
    author: "Arash Zeini"
  - subject: "Re: ??"
    date: 2003-12-31
    body: "this is a question:\n\nthe farsiKDE have the visual environment or Not?\n\nThanks\n"
    author: "Zahra HB"
  - subject: "Re: ??"
    date: 2004-01-02
    body: "Yes, sure! That's what this whole thing is about: A Graphical User Interface.\nGet a Shabdix CD and try it out!"
    author: "Arash Zeini"
  - subject: "KDE for muslims"
    date: 2003-12-28
    body: "I think Muslim option will be also implemented in KDE: prayer reminder, muslim calender."
    author: "Hein"
  - subject: "Re: KDE for muslims"
    date: 2003-12-28
    body: "The Hijri calander is fully supported in kde 3.2"
    author: "anon"
  - subject: "Re: KDE for muslims"
    date: 2003-12-28
    body: "Hmmm when they did a review of bibletime here some time ago you should have heard the howls from the secularists. They were sooooo afraid KDE would be \"tainted\" with religion. "
    author: "Kraig "
  - subject: "Re: KDE for muslims"
    date: 2003-12-29
    body: "KDE should always remain religion and politics free. Those who want to write religous apps using KDE are free to do so.\n"
    author: "Slovin"
  - subject: "Re: KDE for muslims"
    date: 2003-12-29
    body: "Why not just support any and all religions directly?  The solution to religious intolerance isn't pretending that they don't exist."
    author: "bleaurgh"
  - subject: "Re: KDE for muslims"
    date: 2003-12-29
    body: "KDE doesn't have to pretend religions don't exist, it just has to remain completely and totally neutral to all of them.  Supporting all religions equally is impossible, and even if KDE tried, then people would be mad about *that* too, because they are convinced that their religion is better than the others and deserves special treatment.  It's the nature of the beast.  So not including religious applications in KDE is the way to go.  That doesn't mean that KDE users or developers are being deprived of anything.  Anyone is free to write or download religious KDE applications.  They just don't belong in the KDE project itself.  Popular religious holidays should probably be in the KDE calendar, but that should be the extent of KDE's religious content."
    author: "Spy Hunter"
  - subject: "Re: KDE for muslims"
    date: 2003-12-29
    body: "Well, the western calendar ist christian! \n\nanno domini\n\nMuslims and chinese use a different calendar. Moon calendar ecc.\n\nthis has to be implemented just like different time zones and is not related to religion. And a prayer reminder may be a gimmick just as the \"tea\" panel applet."
    author: "andre"
  - subject: "Re: KDE for muslims"
    date: 2003-12-31
    body: "Just for the record:\n\nNot all muslims use the same calender. Iran does not use the Hijri calender (which is based on moon), it uses Jalali calender (based on Sun), which is actualy a very precise calender and doesn't have the mistakes of both, Georgian calnder, and moon-based calenders.\n\nAlso, I know of some Muslim countries (like Jordan) which actualy use the Georgian  calender. Same with Turkey.\n\nSo, \"Muslims use a different calender\" is incorrect. There are many muslims countries, and they use different calendering systems."
    author: "Aryan Ameri"
  - subject: "Re: KDE for muslims"
    date: 2003-12-29
    body: "The Muslim prayer reminder makes me a bit uncomfortable (and I am, in theory, a Muslim) but I see nothing wrong with including the calander. The calander is not any more religious in nature than the Gregorian one, and is simply an alternative calander. Kinda like the binary clock :)\n"
    author: "Rayiner Hashem"
  - subject: "Re: KDE for muslims"
    date: 2003-12-30
    body: "Isn't the calendar included already? I don't know what calendar system one would use, but my KDE provides many different calendar systems (namely Gregorian, Hebrew, Jalali and Hijri)."
    author: "Henrique Pinto"
  - subject: "Re: KDE for muslims"
    date: 2003-12-31
    body: "a muslim in theory - yeahh that is great stuff\nif you do your computer things like this you ever come to the same point: let's try again ....\nAre you or are you not? Do you know or do you believe?"
    author: "jo\u00e3o"
  - subject: "Re: KDE for muslims"
    date: 2006-02-16
    body: "Thats just calendar :)"
    author: "semsudin"
  - subject: "bidi"
    date: 2003-12-28
    body: "farsi is written in different direction. Would'nt it be useful to mirror the menu panel as well? Or are persians used to the menus of programs. Perhaps it would be suitable to place the file dialogue to the right of an application. And the kicker to the right...."
    author: "Hein"
  - subject: "Re: bidi"
    date: 2003-12-28
    body: "Oh, Snapshot 4, I see"
    author: "Hein"
  - subject: "esrever"
    date: 2003-12-28
    body: "Why is everything reverse in the screenshots?"
    author: "anon"
  - subject: "Re: esrever"
    date: 2003-12-28
    body: "Because Farsi is a Right to Left Language."
    author: "Leonscape"
  - subject: "Re: esrever"
    date: 2003-12-28
    body: "That's weird though because, sure, you'd expect the text to go from right to left, but not everything else."
    author: "anon"
  - subject: "Re: esrever"
    date: 2003-12-28
    body: "Well, if you read right to left, it makes sense that asymetric widgets would be reversed. Left to right, or right to left, isn't just the way you read text. Its a lot more like handedness, a lot of other things are oriented differently because of it. Consider, for example, that pixel numbers are counted from left to right and from top to bottom. Why? Because that's how we read text in left to right languages. Why are resize handles on the bottom and right? Because pages in left to right languages have their origin in the top left, so it makes sense to have the resize handle opposite to that anchor. "
    author: "Rayiner Hashem"
  - subject: "Re: esrever"
    date: 2003-12-28
    body: "Wow! I think I will tell my persian friends from Hanover about this Farsi - KDE. They use Win XP, but I believe there is no Farsi version."
    author: "Gerd"
  - subject: "Re: esrever"
    date: 2003-12-31
    body: "xp is farsi by design"
    author: "jo\u00e3o"
  - subject: "Zeini"
    date: 2003-12-28
    body: "\"It is not a secret anymore that FLOSS is gaining momentum all over the world. We witness an international move and acceptance of FLOSS in the private as well as in the public sector.\"\n\nI believe this is true. I participated in the UN world Summit and I was surprised about the large amount of FLOSS supporters."
    author: "andre"
  - subject: "Re: Zeini"
    date: 2003-12-28
    body: "Yep, I think that FLOSS can, will and should play an important role in the whole ICT for development discussion going on everywhere."
    author: "Arash Zeini"
  - subject: "the dot on slashdot again..."
    date: 2003-12-29
    body: "again: http://slashdot.org/search.pl?query=dot.kde.org"
    author: "cies"
  - subject: "yesyesyes"
    date: 2003-12-31
    body: "but only 25 (used by 5 each) of this 100 trillion have a computer\nalso most of 10% of population is able to read and write\nso you don't need farsi you need muppets language\nstill good you're not a profit org :( you 're money never comes back :)\nand take care when you make service exchange, when you come home there is only sand in the box :))))\nhahahaha\n"
    author: "jo\u00e3o"
---
In a <a href="http://articles.linmagau.org/modules.php?op=modload&name=News&file=article&sid=395&mode=thread&order=0&thold=0">background article</a> Aryan Ameri and <a href="http://www.kde.org/areas/people/arash.html">
Arash Zeini</a> (KDE Farsi) explain the benefits of FLOSS (Free Libre Open Source Software) for Iran and developing countries. For a trade show <a href="http://knoppix.org/">Knoppix 3.1</a> was modified in order to demonstrate <a href="http://www.farsikde.org/">KDE in Farsi language</a>. Overall an estimated 100 million people speak this language. With the release of KDE 3.1 in January 2003, Farsi became an official language in KDE. 
KDE <a href="http://articles.linmagau.org/images/news/eleven/snapshot2.png">looks</a> <a href="http://articles.linmagau.org/images/news/eleven/snapshot3.png">very</a> <a href="http://articles.linmagau.org/images/news/eleven/snapshot4.png">interesting</a> in Persian.


<!--break-->
