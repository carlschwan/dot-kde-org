---
title: "Competition: Design a New Logo for KDE.org"
date:    2003-03-30
authors:
  - "jbainbridge"
slug:    competition-design-new-logo-kdeorg
comments:
  - subject: "The Free Art License"
    date: 2003-03-30
    body: "Shouldn't the license be more permissive? Andreas Pour argued some time ago that KDE artwork should not be GPLed, since commercial products run on KDE. They may want to use the artwork. For that reason an art-equivalent of the LGPL should be used, for the artwork. Why then use a more strict license for the main image? "
    author: "Ante Wessels"
  - subject: "Re: The Free Art License"
    date: 2003-03-30
    body: "Is there any other license you would recommend then? The Free Art license sounds fairly permissive to me although I'm no licensing expert. \n\nWe tried to include all GPL related licenses to cover artwork but if there is a suitable one we are unaware of, we will of course consider permitting it. :)"
    author: "Jason Bainbridge"
  - subject: "Re: The Free Art License"
    date: 2003-03-30
    body: "The Free Art License seems to be the most suited for this king of artwork, since it's an adapted GPL for Arts : free and copyleft.\n\nThe problem with the use of the word \"creator\" in the license is due to a bad translation, it should be fixed soon.\n"
    author: "N\u00ffco"
  - subject: "Re: The Free Art License"
    date: 2003-04-01
    body: "As Andreas Pour pointed out, the GPL is not permissive enough. So, all the\nlook alikes are too. Problem is that commercial packages should be able to\nuse the libraries and images. KDE is a platform, anything can be run on it.\nMost libs are LGPLed. BSD can work too.\n\nThere is a policy about the licenses that can be used.\nhttp://developer.kde.org/documentation/licensing/policy.html\n\n\"\"\n3) Source files that are part of a library in kdelibs must be licensed \nunder the terms of any of the following licenses:\n\n  *) LGPL as listed in kdelibs/COPYING.LIB\n  *) BSD license as listed below.\n  *) X11 license as listed below.\n\"\"\n\nWhile images are no libs, they are in the same situation. So, only these three\nare useable. Using other licenses makes a less clear situation. They go \nagainst the policy. The policy mentiones four more licenses:\n\n\"\"\n4) Any other source files must be licensed under the terms of one of the\nlicenses listed under 3) or any of the following licenses:\n\n  *) GPL as listed in kdelibs/COPYING\n  *) QPL as listed below.\n  *) MIT license as listed below.\n  *) Artistic license as listed below.\n\"\"\n\nThese 7 are all the licenses that are possible to use in KDE. The other GPL \nrelated licenses that are mentioned as possible are not useable. In this case\nthe first 3 are the ones to use.\n\nThe svg icons use the LGPL with an add on to the license notice. See:\n\n********************************************************************\n\n\n\n\nThis copyright and license notice covers the images in this directory.\nNote the license notice contains an add-on.\n\n********************************************************************************\nKDE vector icons. Crystal theme vector icons with handoptimized pngs for small\nicons.\nCopyright (C) 2002 KDE Artists\n\nThis library is free software; you can redistribute it and/or\nmodify it under the terms of the GNU Lesser General Public\nLicense as published by the Free Software Foundation,\nversion 2.1 of the License.\n\nThis library is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\nLesser General Public License for more details.\n\nYou should have received a copy of the GNU Lesser General Public\nLicense along with this library; if not, write to the Free Software\nFoundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\n    **** NOTE THIS ADD-ON ****\nThe GNU Lesser General Public License or LGPL is written for software libraries\nin the first place. We expressly want the LGPL to be valid for this artwork\nlibrary too.\n\nKDE vector icons is a special kind of software library, it is an\nartwork library, it's elements can be used in a Graphical User Interface, or\nGUI.\nSource code, for this library means:\n - for vectors svg;\n - for pixels, if applicable, the multi-layered formats xcf or psd, or\notherwise png.\nThe LGPL in some sections obliges you to make the files carry notices. With\nimages this is in some cases impossible or hardly usefull. In this library, the\nvector elements carry short notices, the pixel elements do not; a longer notice\nis placed at a prominent place in the directory containing the elements. You may\nfollow this practice.\nThe exception in section 6 of the GNU Lesser General Public License covers\nthe use of elements of this art library in a GUI.\n\nkde-artists@kde.org"
    author: "Ante Wessels"
  - subject: "Re: The Free Art License"
    date: 2003-04-01
    body: "Firstly remember we are talking about an images for the sole purposes of using as logos on kde.org and it's sub-domains, the only reason we put any mention of licenses there, was so the winner couldn't suddenly deny us to use the image at any stage. I'm also not going to get into a debate as to what licenses KDE libraries and what not should use as that is not my area of expertise.\n\nNow I could with someone's assistance go through and create our own version of the LGPL like was done for the SVG icons, but is that even worthwhile for just a website logo? Do things have to be this political? I personally don't care what license the logo is under as long as it ensures we can use it and modify it.\n\nAfter the new design went live one of the main complaints was about the logo that was used, so we in good spirit have organised a competition for the commnity to provide a new logo so I am hoping things like licensing isn't going to be a major barrier...\n\nJ"
    author: "Jason Bainbridge"
  - subject: "Re: The Free Art License"
    date: 2003-04-02
    body: "If you comply with the policy, the possibilities are most broad, with the least chance on problems. The most simple thing to do is comply with the policy. Allow LGPL, BSD and X11. Don't worry about an add-on."
    author: "Ante Wessels"
  - subject: "Thanks Derek"
    date: 2003-03-30
    body: "Yes, great job Derek...\n\nOoooooops, wrong article, wrong time :-)"
    author: "Ricardo Galli"
  - subject: "Re: Thanks Derek"
    date: 2003-03-30
    body: "Great! :-)"
    author: "Fabi"
  - subject: "does anyone care?"
    date: 2003-03-30
    body: "All you people who have been complaining about the KDE.org art and look, where are you?  All you people posting mockups on kde-look.org where are you?  It's your chance to help KDE look better...\n\nDoes anyone *intend* to participate in this contest?  Can someone post a link to this contest on KDE-Look.org?  I tried to register but it didn't work."
    author: "Navindra Umanee"
  - subject: "Re: does anyone care?"
    date: 2003-03-30
    body: "Done.\n\nJ.A."
    author: "jadrian"
  - subject: "Re: does anyone care?"
    date: 2003-03-31
    body: "Put Konqi back on the page, thats all it needs.\n"
    author: "reihal"
  - subject: "Re: does anyone care?"
    date: 2003-03-31
    body: "Waiting for your submission... or submissions inspired from your post... :-)"
    author: "Navindra Umanee"
  - subject: "No - please drop the gear motive!"
    date: 2003-03-31
    body: "My heart made a jump when I saw the call for a new logo. In my opinion KDE is held back by a set of developer centric preferences which goes straight against aestitic choices made from a pure design-craft perspective.\n\nTwo such elements are:\n\n* the K-naming convention, which results in incredibly ugly and clumsy names. The convention is evidently purely made because of the need to signal group identity -   an objective which in this case is in direct conflict with the objective of making elegant and attractive application names. If only KDE had been named ADE instead, the situation would have been som much better.\n\n* The technological gear-based unified style. This style speaks well to developers, who are reminded of their central values and signals communality whith fellow engeneers. However, it is really badly suited as a style for non-techological end users (in other words: \"users\"). An important visual element like this speak to the values of the the users - it should give pleasant associations, transmit a feeling of \"can-do\" empowerment or instill a sense of security and familiarity. Use smiling faces, flower-motives or elegant cartoons or whatever - BUT NOT GEARS. It is cold, alienating and uninviting and associates with an industrial environment. You can almost smell the diesel when you open KDE.\n\nThis is important because such dominant form elements puts a severe limit to how good the default KDE-style can get.\n\nThe programmers of KDE can, I feel, respect the work and demands of a good craft. Design and  user communication is a serious and important craft as well. Out of respect for this craft, can't you let go of the narrow developer aestetic antics and do the right thing?\n\nSo please, *please*, use this opportunity let go of the gear motive and replace it with something better.\n\n"
    author: "will"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-03-31
    body: "Wow!  You've obviously thought about this quite a bit.  I guess is boils down to the fact that managers don't tell developers how to write code, so developers shouldn't tell designers how to design visual elements."
    author: "anonymous"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-03-31
    body: "It's not that I don't like the gear as is the case for will, but I also think that it would be better not to require that the new logo is based on the gear. I understand the fear that a completely new logo with no aspects common to the old one might confuse people or generally just be a bad marketing move. Still I think that a less restrictive contest might yield more interesting results, from which KDE could possible benefit in the long run."
    author: "Matthijs Sypkens Smit"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-03-31
    body: "Sorry to ask, but would you suggest Gnome to drop the foot when in search for a new weblogo? FreeBSD to drop the deamon or Linux to drop the penguin? or debian to drop the spiral? ;)\n\nThe contest asks on _variations_ of the gear, not to use exactly _this_ gear. \n\nAll the contest asks, is \n\na) the known KDE \"logo\", is used in some variation (there are many gears out there ;). \nWhat use is a logo on a webpage wich is totaly unrecognizable? \nb) the text is usable on non-english pages (KDE is translated in approx. 50 languages)\nc) the size of the logo should be usable on a webpage.\nd) \"Ideally\" the source should  be available to make the logo usable on subpages.\ne) we need a license wich allows KDE to use, redistribute and modify the image. \n\nWhen you come up with a logo without the gear wich is looking nice and still recognizable as KDE at the first glance I am sure it will not being turned down. ;)\n\nPeople, the last logo lasted for _years_. When you want fame with your friends, an easy way to show your possible employer how good you design or simply what you have learned in all this arts lessons. Here is your chance. \n\nI am looking forward to every entry.\n\n\tphysos"
    author: "Rainer Endres"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-03
    body: "> Sorry to ask, but would you suggest Gnome to drop the foot when in search for a new weblogo?\nNo, because the foot is userfriendly, I think, I don't care that much.\n\n> Linux to drop the penguin?\nThe penguin is cute and everybody loves it\n\n> debian to drop the spiral? ;)\nno idea, I never understood debianers.\n\n> FreeBSD to drop the deamon\nI left this to the end since is the most intrested.\nIf FreeBSD tried to achive a user friendly desktop, if it tried to reach users, instead of geeks and administrators. Yes, I would recomend it to drop the daemon because a daemon (no matter how nice it seems) isn't user friendly. Now, think of the market FreeBSD is getting, the logo it has and the market KDE is looking for (I know, market is the wrong word since this is not comercially based, but I didn't know a better world).\n\nI totally agree with the other post, the K in the names end up being very weird... I don't imagine John User telling Veronica Newbie \"Try k-organizer, it's a great application and if you have a palm pilot you can use k-pilot to syncronize it, it even syncronize to k-notes and k-alarm, something is on the radio...\" radio: \"last news, the world has be k-invaded\". People doesn't even know sometimes, that they're running KDE.\n\nThanks.\n "
    author: "Pupeno"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-03
    body: "i know you put a \";)\" for debian.  debian changed its logo completely a couple years ago because they wanted to be inclusive of the hurd.  the spiral simply won the vote.  there wouldn't be anything wrong with kde doing the same thing.  if the gear won, then we'd stay with the gear.  a new logo might take some getting used-to but it shouldn't be a problem."
    author: "PunkFag"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-03-31
    body: "You have some very strong points and you've thought about this quite a bit.  How about channeling these ideas into a new logo without the gear.  If it is good enough then perhaps you can convince others :-)\n\nPS The K naming convention is likely here to stay unless a real alternative exists (I don't see one) and the benefit is larger than the cost of renaming and rebranding all of the official KDE apps ;)"
    author: "anon"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-03-31
    body: "Replace it with what?\n\nAny suggestions? ;)\n\nSmiling faces? Like the overstyled pictures on every company website out there? \nFlower motives? This is something to think about, but again I get an image of photographs,\nnot logo. \n\nI am not an artist or designer (yes I think there is a difference ;) not in persons but in the outcome (discussions about this only in private mail please ;)), so I do not have sufficient skills to come up with something myself in a decent timeframe. :(\n\nYour input shows me, that the gears are an itch sufficient to spend time on. And from the fact I tend to agree with the gears being too technical to some extent, I invite you to enter your own suggestion in the contest. \n\nPlease bear in mind, the gear with the K is very recognizable on fairs and in print. It is there for quite some time now. I think we should not give it up too easily. People recognize our booth on fairs instantly because there is the gear somewhere (same as the Gnome foot, I can not make up my mind if it is friendly or tech though ;) \n\nI think we are very open to suggestions, but the gear is something which is not going away easily IMHO. ;) \n\nThanks for your input\n\t\n\tphysos"
    author: "Rainer Endres"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-01
    body: "Another point of course is that creating a completely new logo always implies the danger of creating something that is too similar to something that is already existant. Personally I'm glad that since 1998 (that's the year the current version of the KDE logo was used for the very first time) I haven't seen any logo which looked too similar.\n\nTorsten Rahn\n\n "
    author: "Torsten Rahn"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-03-31
    body: "\"the K-naming convention, which results in incredibly ugly and clumsy names\"\n\nAs opposed to Gnome, GTK+, GNU, GCC, Grub, Galeon, GIMP, Gnumeric, etc. Or XFree86, XEmacs, xbiff, xscreensaver, xclock. xeyes, xnabm xmms, etc.\n\n\"If only KDE had been named ADE instead, the situation would have been som much better.\"\n\nOh yeah! Writeade, Conqade, Officeade, Lemonade, Gingerade, etc.\n\n\"Use smiling faces, flower-motives or elegant cartoons or whatever - BUT NOT GEARS.\"\n\nSounds like someone has issues they need to work through. But at least it's not a smelly foot with a toe missing!\n"
    author: "David Johnson"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-01
    body: "The gears symbolize stability, responsiveness, steadiness, reliability, evolution, modularity and precision - It's what good software and KDE is all about (\"It just works\")..\nIn comparison most smily faces / flower logos are rather dumb, naive and not very unique.\nAlso keep in mind that it's easy to create stuff which is offensive in some cultures (a good example is the foot which is a clearly offensive symbol in some cultures).\n\n"
    author: "Anon"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-01
    body: "I'm partial to the colourful spinning beachball that Apple uses.\n\nReason being?  Everytime I go to launch KDE on my iMac, under OS X.4, that's all I see for several minutes. :-)\n\n(Ducking for cover)"
    author: "A.C."
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-01
    body: "How about a gear or gears with personalities.. A little softer with some kind of faces or maybe arms (like m&m's) working together to get things done..\n\nI wish I could do something like that, but I have no artistic talent :(  \n"
    author: "anonymous"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-01
    body: "i find that flower idea quite interesting, cause a flower can quite look like a gear. think about it :)\n\n-thomas"
    author: "thomas"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-01
    body: "What about the dinosaur you can stil see in the logo of the german kde site? I really like him ;D"
    author: "Heinrich Wendel"
  - subject: "Re: No - please drop the gear motive!"
    date: 2003-04-02
    body: "Konqi is a dragon, and he is cute indeed, just like Katie (http://women.kde.org).\n\nAnyway, I really hope there will be submissions that include Konqi as well.\n\n"
    author: "Olaf Jan Schmidt"
  - subject: "How about.."
    date: 2003-04-01
    body: "How about a left footprint, four toes?"
    author: "IR"
  - subject: "No please"
    date: 2003-04-01
    body: "\nWhat's wrong with the new KDE logo on kde.org site? You have small versions of default crystal icons. Use them.\n\nThe main problem with kde.org is that the site is boring. And it is boring because it is text based. And all text on the site has similar sizes. \n\nWhen I open kde.org site I don't know what it is all about. You create software, don't you. Wright it with big letters: WE CREATE SOFTWARE. With nice antialiased fonts. And put a picture of your software or a big logo there. Add a section with small pictures which announces a new piece of software (and change these pictures every few days as the new applications arrive - it would give some life to the site - if you only change the text the site looks always the same).\n\nOne of the things totally wrong on kde.org site is KDE-forum logo. It is bigger than the kde.org logo (and visually suggests that kde-forum is more important than kde.org), it has the same main element in its logo as kde.org (wrong), and the fonts are different (wrong). The background picture of both logos should be the same (it would give consistency).\n\n\nBTW, you have excellent gears on dot.kde.org.\n\nantialias"
    author: "antialias"
  - subject: "Re: No please"
    date: 2003-04-02
    body: "   What's wrong with the new KDE logo on kde.org site?\n\nThe problem is that we do not have the source of the logo, so it's impossible to create other versions for all the kde.org subsites.\n\n\n   You have small versions of default crystal icons. Use them.\n\nGood idea. Would you like to work it out and submit it?\n\n \n   One of the things totally wrong on kde.org site is KDE-forum logo. It is bigger\n   than the kde.org logo (and visually suggests that kde-forum is more important\n   than kde.org), it has the same main element in its logo as kde.org (wrong), and\n   the fonts are different (wrong). The background picture of both logos should be\n   the same (it would give consistency).\n\nThat's why we have a contest for a new logo. After we have chosen the new logo, KDE-forum can decide whether they would like to use similar fonts, but that's up to them."
    author: "Olaf Jan Schmidt"
  - subject: "Let sunshine in"
    date: 2003-04-01
    body: "Has anybody ever seen that the KDE-Gear on KDE.org somehow looks like a sun ?\n\nI not a designer , but I think its a good idea..."
    author: "Arven"
  - subject: "At designers: Please take a look at a real gear!!"
    date: 2003-04-04
    body: "The current KDE-logo must be the creation of people who never saw a gear in real live. The logo tries to appeal people with technicial interests. But it completely upsets people with mechanical knowledge. For them it looks like incompetent people are working here.\n\nPlease have a look at http://www.warpax.com/grinder/grinder.html to know what I mean.\n\nBtw. I like the little green dragon. He gives KDE a more personal identification. I don't understand why kde.org changed the layout to such boring pagelayout. Looks so frosty and impersonal. brrrr. I hope kde.de give the dragon asyl forever.\n\n\n"
    author: "Peter Liscovius"
  - subject: "Re: At designers: Please take a look at a real gear!!"
    date: 2003-04-05
    body: "I do not quite comprehend this issue against the gear. I like it very, very much.\n\nOne thing I just like to add to your contest is that there are people in this world for every tastes. Every. Do not try to say your view (for the ones who are claiming against the gear or whatever) is the correct one. There is no such thing in this world as only one view of beauty.\n\nAccording to the background of each individual, he will get a different response to what he sees. \n\nPlease, please remember that this logo will be seen by lots of people in the whole world. \n\nBTW, I do not like logos to much childish (e.g. Konqui and others like it ). But this is just my taste. I do comprehend that there are people liking them. \n\nJust to finish, I'm a mere translator, work on Linux and uses KDE. I've always like the gear. I do not program or develop anything. I just like it. It's different. \n\nThank you all for your work on KDE.  :D"
    author: "Henrique Maia"
  - subject: "Logos"
    date: 2003-04-11
    body: "I think we have to work cause the officials logos are very good.\nLet's go finding a new one.\nFrom chven France Lyon"
    author: "Julien Chevalier"
---
The KDE Web Team would like to announce a competition to design a new logo for the <a href="http://www.kde.org/">KDE.org</a> website.   The prize for the competition is the honour and satisfaction of having your work displayed on <a href="http://kde.org">KDE.org</a> and its subdomains, with full credit given for each use of the image.  The competition will run over the course of a month and closes on April 30th, 2003. Artists are further encouraged to contribute other artwork they feel would improve the look of the website such as icons for the various sidebar sections (namely: Inform, Download, Communicate, Develop, Explore, Search, Hotspot and KDE Family). <B>(Updated)</b>
<!--break-->
<p>The requirements for the logo competition are as follows:</p>

<ul>
<li>The logo should contain a variation of the KDE gear (such as <a href="http://www.kde.org/stuff/images/kdelogo_svg.png">kdelogo_svg.png</a> or <a href="http://static.kdenews.org/dot.kde.org/TopicImages/official.gif">official.gif</a>). </li>
<li>The text of the logo should be limited to "K Desktop Environment" or "KDE" as it is envisioned the image will be used on sites with languages other than English.</li>
<li>The logo should be no more than 400 by 80 pixels in size and be in PNG format. 
<li>Ideally, the logo should be easily modifiable in order to accommodate the various <a href="http://www.kde.org/">KDE.org</a> subdomains (such as the <a href="http://usability.kde.org/">Usability</a> and <a href="http://pim.kde.org/">PIM</a> sites).   Therefore, it is a requirement that the source file  be available -- preferably in The Gimp's XCF format although Photoshop's PSD format is also acceptable.   Any fonts used for the image will need to be made available.</li>

<li>All images will need to be licensed under one of the following licenses: GNU General Public License, GNU Free Documentation License, The Design Science License or The Free Art License as <a href="http://www.gnu.org/philosophy/license-list.html#OtherLicenses">detailed here</a>.</li>
</ul>
<p>To enter the competition, simply email your image in PNG format (under 40kb please) to <a href="mailto:webmaster@kde.org">webmaster@kde.org</a>.  
<p>
Once the competition has closed, the KDE Web Team will review all entries and make a shortlist of the best entries received. The authors of the chosen entries will then be asked to provide the source image file and fonts used for their design.  A public poll consisting of the chosen entries will then be run over the course of 2 weeks and the winner will be announced here and notified by email!</p>

<p>If you have any further queries about the competition please email <a href="mailto:webmaster@kde.org">webmaster@kde.org</a>.</p>
<p>
<b>Update: 04/02 04:52</b> by <a href="mailto:navindra@kde.org">N</a>: The SVG, AI or PSD source for the <A href="/1048968963/logos.png">sunshine and blue</a> variants of the crystal KDE logos is available if required.