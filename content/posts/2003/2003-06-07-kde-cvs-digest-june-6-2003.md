---
title: "KDE-CVS-Digest for June 6, 2003"
date:    2003-06-07
authors:
  - "dkite"
slug:    kde-cvs-digest-june-6-2003
comments:
  - subject: "Ahhh.....first one"
    date: 2003-06-06
    body: "Finaly...."
    author: "Nadeem Hasan"
  - subject: "Re: Ahhh.....first one"
    date: 2003-06-06
    body: "I guess the clock diversion worked ;-). Admit it, it was you who staged the whole thing ;-)\n"
    author: "Sad Eagle"
  - subject: "Re: Ahhh.....first one"
    date: 2003-06-06
    body: "No it was #commits on irc :)\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Ahhh.....first one"
    date: 2003-06-06
    body: "du bist nicht allein!\n"
    author: "Slovin"
  - subject: "kspell is broken"
    date: 2003-06-06
    body: "I've noticed that KSpell is completely screwed up in the latest CVS ..unfortunately no info in the digest to whom we have to be so grateful for that.  "
    author: "MiniMe"
  - subject: "Re: kspell is broken"
    date: 2003-06-06
    body: "second to this happend to me in the last two three weeks just when i got used to use it ."
    author: "maor"
  - subject: "Re: kspell is broken"
    date: 2003-06-06
    body: "Step 1. Find a bug\nStep 2. Go to bugs.kde.org\nStep 3. Search for your bug\nStep 4. If its there vote it up, if not add it.\nStep 5. Repeat until KDE rocks\n\nThis is optimal even if the bug goes away in CVS it boots tracks problems and resolutions.  It also make sure we dont find a million bugs the day after release.  You guys using CVS should have bugs.kde.org as your home page! \n\nWorst case senario you will be boosting some of our bug fixes stats!\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: kspell is broken"
    date: 2003-06-07
    body: "If you're running HEAD (the main development trunk of KDE's CVS) then you really can't complain about things being broken; in fact I guarantee that they often will be.  That's why it's not released.  ;-)\n\nAnd I think this is sometimes misunderstood, but HEAD is not a toy or something just to see what's coming in the future -- if you're running HEAD you should (a) be willing to find and fix things when they're broken and annoy you, (b) be willing to file and follow up bug reports or at the very minimum (c) keep quiet about what annoys you if you're not willing to help to fix it.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: kspell is broken"
    date: 2003-06-08
    body: "Well I'd agree with you If I'd complain about something fress/new application   but if it is a well working code in the past what is the point o screw it up.\n\n"
    author: "MiniMe"
  - subject: "Re: kspell is broken"
    date: 2003-06-10
    body: "\nPeople add features, which (sometimes) make KDE better, but they also sometimes break things. The original poster was right. If you're running HEAD, it is certain to have bugs, and sometimes not even to compile. It's up to you to help fix that, by filing well-written bug reports at http://bugs.kde.org/."
    author: "David Hugh-Jones"
  - subject: "highlights (for me)"
    date: 2003-06-06
    body: "> Stephan Binner committed a change to kdebase/konqueror\n> Bug 48393: Dynamic Tab Resizing in Konqueror (wishlist)\n> Sometimes the tabs don't shrink enough, any idea?\n\nWhee! Moz-like dynamic tab resizing? nice.\n\n>Waldo Bastian committed a change to kdelibs/kdeui\n> Deactive layout while we wait for the layout to get rebuild. (Less flicker)\n\nThis is removal of the toolbar flicker when switching views in Konq-- yay!"
    author: "lit"
  - subject: "Not too good"
    date: 2003-06-07
    body: "Dynamic Tab Resizing isn't as good as in mozilla, as the commit says and the flicker has NOT been eliminated +( just reduced."
    author: "Mario"
  - subject: "Re: Not too good"
    date: 2003-06-07
    body: "Why? Did you try? There were more commits for it than only the initial one."
    author: "binner"
  - subject: "Re: Not too good"
    date: 2003-06-08
    body: "Of course he didn't try it."
    author: "anon"
  - subject: "No, I did not try, I just read the commits"
    date: 2003-06-08
    body: "And that's what they said."
    author: "Mario"
  - subject: "Re: No, I did not try, I just read the commits"
    date: 2003-06-08
    body: "The commits listed in the digest are about 7-10% of the totals. I would say about half the total commits are finishing touches, minor though important corrections. Maintenance type stuff. Translations, web page maintenance, etc. Of the other half, developers typically do multiple commits for the same feature they are working on. Work in progress. I those situations, I select one or two that have the best representative comments.\n\nSo, what you read in the digest is only a small part of the action. Hopefully it gives an idea of what is happening, but it is not everything, nor is it meant to be.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: No, I did not try, I just read the commits"
    date: 2003-06-09
    body: "It's good to try before you speak :-)\n\n(the dynamic tab resizing in HEAD is good, btw)"
    author: "AnGuS"
  - subject: "Thanks!"
    date: 2003-06-07
    body: "Thanks for the updates Derek!"
    author: "Alex"
  - subject: "OT: What is currently really bugging me:"
    date: 2003-06-07
    body: "Sorry to ask but:\nI think I can remember that some time ago when I saved a document\nvia FTP KIO, it was uploaded immediately back. Now, the document\nis saved locally and only when I close it, I'm asked if I want to\nupload it back. How can I get back the original behaviour? Or do\nI remember incorrectly and it was like that all the time? \n"
    author: "Jan"
  - subject: "Re: OT: What is currently really bugging me:"
    date: 2003-06-08
    body: "I think the way it has always worked is:\n(a) If you're using a KDE application and you save, it uploads immediately\n(b) If you're using a non-KDE application it waits until you exit and then asks\n\nSo what app are you using to edit the document?"
    author: "AC"
  - subject: "Re: OT: What is currently really bugging me:"
    date: 2003-06-08
    body: "I'm using Kwrite"
    author: "Jan"
  - subject: "Re: OT: What is currently really bugging me:"
    date: 2003-06-09
    body: "I can confirm this. Sadly, if I do Alt+F2 and type www.kde.org, first the page is saved into my disk and then loaded by konqueror. This is really annoying, since the images are not cached, and therefore only the text is shown.\n\nI just tested this. The CVS version is from Friday night (night in UK ;-), so this might be fixed by now."
    author: "uga"
  - subject: "Re: OT: What is currently really bugging me:"
    date: 2003-06-09
    body: "I found an easy fix though: I wiped out all my $HOME/.kdecvs/share/applnk directory, and as smooth as usual."
    author: "uga"
  - subject: "Did anybody tried to compile HEAD on FreeBSD re..."
    date: 2003-06-08
    body: "Did anybody tried to compile HEAD on FreeBSD recently?\nIt requires automake17, but it's port is broken, when I installed it manually and start compilation it breaks after some time (after configure in middle of the compilation of kdelibs) with error \nlibtool: cannot find library `'\nDid somebody have solution about ?\n\nps. I am killing for FreeBSD KDE CVS binaries - anybody know something different than fruitsalate.com ?\n"
    author: "Vlindos"
  - subject: "Re: Did anybody tried to compile HEAD on FreeBSD re..."
    date: 2003-06-08
    body: "kdelibs doesn't compile at all right this moment.\n\nDerek"
    author: "Derek Kite"
  - subject: "Regular builds?"
    date: 2003-06-08
    body: "I'd love to see Red Hat 8/9-compatible RPM releases of KDE's bleeding-edge branch available for download on a regular basis, similar to Mozilla's nightly binary build system. Maybe twice a month? That'd be really neat for us more adventurous KDE followers. Or is anyone already distributing such snapshots? Of course, I'd love to see KDE.org do it - I trust KDE.org; with any other source I'd have to wonder about malicious modifications to the source."
    author: "Eike Hein"
  - subject: "Re: Regular builds?"
    date: 2003-06-08
    body: "Why use RedHat with kde? since RedHat seem to have a great history of introducing bugs etc just for kde."
    author: "Kde User"
  - subject: "Re: Regular builds?"
    date: 2003-06-08
    body: "Maybe you should switch to a more ... KDE friendly distribution first. I mean, redhat has a history of screwing up KDE. Read http://www.mosfet.org/noredhat.html from Mosfet's site and see why."
    author: "Andr\u00e9"
  - subject: "Re: Regular builds?"
    date: 2003-06-08
    body: "Switch to Gentoo or Debian and look at either http://cvs.gentoo.org/~danarmak/kde-cvs.html or http://opendoorsoftware.com/cgi/http.pl?p=kdecvs , that's pretty much all you get and need."
    author: "Datschge"
  - subject: "Re: Regular builds?"
    date: 2003-06-08
    body: "Well, I could easily build KDE on RedHat on my own, too - of course, Gentoo's Portage greatly simplifies the task. I'm aware of that. Still, I'm specifically asking for binary releases because of the long build times. As far as I know, RedHat 8/9 are LSB certified, which pretty much means a RH8/9 compatible RPM release should work for serveral other distros, too, right? \n\nBTW, I know RedHat's not the most KDE-friendly distribution. Getting arts/Noatun to play back MP3s was quite an ordeal. However, I understand the reasons, and apart from that, RedHat is a fine, highly polished distribution. (Still, I had a lot of fun with my first Gentoo installation - at the time, I was just new to Linux and learned a great deal about it during the installation process. Gentoo + Google taught me what packages I really need and for what reasons, while a RedHat/SuSE is quite overwhelming for a newbie.)\n"
    author: "Eike Hein"
  - subject: "Re: Regular builds?"
    date: 2003-06-08
    body: "I think requests for binaries for commercial distributions should go to the commercial distributions, it should be their service for which they get money and reputation. In any case KDE itself never offered binaries anyway (see http://www.kde.org/download/packagepolicy.php )."
    author: "Datschge"
  - subject: "Re: Regular builds?"
    date: 2003-06-09
    body: "Somewhat unfortunate, but I can see why :)."
    author: "Eike Hein"
  - subject: "Re: Regular builds?"
    date: 2003-06-08
    body: "Another very KDE friendly distro is SuSE.  If you want to drop the $70 at a CompUSA or other common software store for the Professional version, you get a full desktop with commercial DVD authoring software and other goodies.  Good stuff."
    author: "Evan \"JabberWokky\" E."
  - subject: "Try Gentoo!"
    date: 2003-06-09
    body: "Of all the distros I've tried, I happened to install and use KDE on Gentoo without  a single problem. I suggest anyone reads more about it. www.gentoo.org"
    author: "Mystilleef"
---
This <a href="http://members.shaw.ca/dkite/jun62003.html">week in
KDE-CVS-Digest</a>: Improvements and bugfixes to the new tab code used
in <a href="http://www.konqueror.org">Konqueror</a> and now <a
href="http://quanta.sourceforge.net/">Quanta Plus</a>, numerous
usability enhancements in KMail including easier keyboard shortcut
editing and flickering toolbars fixed, <a
href="http://printing.kde.org/">KDE Print</a> gets a new PPD parser,
<a href="http://edu.kde.org/kstars">KStars</a> hardware interface is
extended, and more.  Enjoy.
<!--break-->
