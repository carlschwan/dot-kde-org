---
title: "J'ai deux amours: Paris et KDE"
date:    2003-02-03
authors:
  - "cmiramon"
slug:    jai-deux-amours-paris-et-kde
comments:
  - subject: "hrmmm - that site is quite funny"
    date: 2003-02-02
    body: "I must say - copinedegeek.com is quite amusing :)\n\n/me looks around for his girlfriend :)\n\nTroy Unrau\ntroy@kde.org"
    author: "Troy Unrau"
  - subject: "Knoppix Fork?"
    date: 2003-02-03
    body: "Where can I download this KDE 3.1 Knoppix fork/branch??"
    author: "Luke-Jr"
  - subject: "Re: Knoppix Fork?"
    date: 2003-02-04
    body: "For the moment, there is no place where to put this iso image (it has been finished this afternoon, and we are burning cds at this moment).\nThis version is a special french version (default language for the display and the keyboard).\nBut we are working for an international version."
    author: "gerard"
  - subject: "Re: Knoppix Fork?"
    date: 2003-02-04
    body: "I was planning to create a similar Knoppix fork using the latest CVS version of KDE. It purpose would be having an easily accessible bleeding edge KDE system for making surveys and discussing usability, accessibility and optimization in cooperation with the respective KDE mailinglists. Would be great if it's possible to base this effort on your already done work. =)"
    author: "Datschge"
  - subject: "Where is GLS geek's site ?"
    date: 2003-02-03
    body: "What about my gay geek boyfriend, strong like an AMD Athlon XP, stable like kernel 2.4.20, beatifull like KDE 3.1 and with a great tool like GNU toolset.\n\nGreat idea, www.glslinux.org.anywhere_in_the_word"
    author: "GLS Penguin"
---
<a href="http://www.kde-france.org/">KDE-France</a> will be holding a booth at <a href="http://linuxsolutions.fr/">Solutions Linux</a>,
a Linux Show in Paris open from the 4th to the 6th 
of February.
David Faure will give a conference Wednesday afternoon about KDE for the end 
user. The KDE-France team will demonstrate KDE 3.1, sell KDE Goodies and a 
brand new KDE Demo CD based on <a href="http://www.knoppix.net">Knoppix</a>. We will also 
try to enlist everybody in our KDE-France association. French developers 
will also demonstrate on the booth their Qt / KDE applications.

We look forward to meeting our French speaking userbase and welcome all Europeans 
who want to share their love of KDE and spend a romantic week-end in Paris. 
Indeed, if you are looking for romance, close to KDE's booth you'll find <a href="http://www.copinedegeek.com">The Grand Union of French Geeks' Girlfriends</a> with practical advice on becoming a clean, 
healthy, talkative and lovable geek.
<!--break-->
