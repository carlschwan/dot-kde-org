---
title: "KOffice Icon Contest"
date:    2003-02-20
authors:
  - "aseigo"
slug:    koffice-icon-contest
comments:
  - subject: "sodipodi"
    date: 2003-02-20
    body: "If we used sodipodi to make the icons, would we be automatically disqualified?\n"
    author: "LMCBoy"
  - subject: "Re: sodipodi"
    date: 2003-02-20
    body: "If sodipodi has onerous licensing that impacts the license and rights of use of the icons that you create with it, then you better be careful.  Otherwise you must make sure you comply with the above rules."
    author: "anon"
  - subject: "Re: sodipodi"
    date: 2003-02-20
    body: "sodipodi is Free software, GPL v2..."
    author: "LMCBoy"
  - subject: "Re: sodipodi"
    date: 2003-02-20
    body: "Does that mean the icons you create with it are automatically GPL?"
    author: "anon"
  - subject: "Re: sodipodi"
    date: 2003-02-20
    body: "The GPL restricts *code* derived from GPL software, not *documents* created with GPL software.  I can license art I create with sodipodi, the GIMP, karbon, kontour, etc. however I please.\n\nI wasn't asking about the licensing; I was referring to this line:\n\n\"Artists should use KOffice 1.2.1 or later (e.g. KOffice from CVS) in creating their submissions.\"\n\nwhich seems to imply that they want you to use KOffice programs to create the icons, but maybe I am reading it wrong..."
    author: "LMCBoy"
  - subject: "Re: sodipodi"
    date: 2003-02-20
    body: "Oh dear no, I doubt that's what it means.  It means that you should create icons targeted at KOffice 1.2.1 or later."
    author: "anon"
  - subject: "Re: sodipodi"
    date: 2003-02-20
    body: "Yeah, that makes much more sense.   Good luck to all participants!  :)"
    author: "LMCBoy"
  - subject: "Re: sodipodi"
    date: 2003-02-20
    body: "It says \"*in* creating their submissions\" not \"*for* creating their submissions\",\nso I guess this means: You should make all Icons necessary for the functions\nof KOffice 1.2.1. i.E. If there is no save button in KOffice 1.0 but a\nsave button in KOffice 1.2.1 then you will have to make an Icon for the save\nbutton. "
    author: "Martin"
  - subject: "Re: sodipodi"
    date: 2003-02-21
    body: "LMCBoy said:\n\"The GPL restricts *code* derived from GPL software, not *documents* created with GPL software.\"\n\nNo, this is incorrect.  The GNU GPL derives all its power from copyright law.  The GNU GPL cannot leverage powers beyond what copyright law grants.  The reason the GPL doesn't set terms on the licensing of one's Sodipodi drawing is because it cannot.  This is explained in <URL:http://www.gnu.org/licenses/gpl-faq.html#GPLOutput>.\n\nI have no access to the HTML encoding, so please forgive the plain text URL."
    author: "J.B. Nicholson-Owens"
  - subject: "Re: sodipodi"
    date: 2003-02-23
    body: "Hmm, as far as I can tell, you just repeated what I said: the GPL restricts code derived from GPL'd software, not documents created with GPL'd software.  What part of that are you saying is incorrect?"
    author: "LMCBoy"
  - subject: "Re: sodipodi"
    date: 2003-02-23
    body: "When I first read what you wrote, I think I misinterpreted it.  I thought you meant something else by \"code\" and \"documents\".  Reading it differently now, I see that you could have meant derivative works by \"code\", and output from a program by \"documents\".  If this distinction is what you were indicating then we agree on this point and indeed this was mostly repetition.  My apologies for the misinterpretation."
    author: "J.B. Nicholson-Owens"
  - subject: "Derivative works by using a program's output?"
    date: 2003-02-21
    body: "\"anon\" asked:\n\"Does that mean the icons you create with it [sodipodi, a GNU GPL-covered vector drawing application] are automatically GPL?\"\n\nNo.  See http://www.gnu.org/licenses/gpl-faq.html#GPLOutput for the FSF's take on this.  You control the licensing of your drawings made with Sodipodi (or any other drawing program, for that matter).\n\nIn short, copyright law doesn't grant the power you're getting at.  There is one rare exception, but even in that case it is easy to work around and claim all copyright power to the work you made with the program."
    author: "J.B. Nicholson-Owens"
  - subject: "SVG tools update???"
    date: 2003-02-21
    body: "Speaking of SVG tools (sodipodi).  Why is it that when KOffice is discussed it always omits Kontour and Karbon?  Are they not part of KOffice?  Are they being worked on?  Where does one find out this info, I would think that the dot would talk about it.\n\nSince the Kontour and Karbon websites are rarely updated it seems that development is slow...."
    author: "ac"
  - subject: "Re: SVG tools update???"
    date: 2003-02-21
    body: "Kontour is dead, Karbon will be part of KOffice 1.3. Blame its authors for not updating the website tho -- they're busy with coding :)"
    author: "Lukas Tinkl"
  - subject: "Re: SVG tools update???"
    date: 2003-02-21
    body: "I can't believe KIllustrator is dead.  It used to be so good!"
    author: "anon"
  - subject: "Re: SVG tools update???"
    date: 2003-02-21
    body: "There are not many people working on Karbon14. But it seems to me they all work on Kexi too and prefer to do that right now. So instead Kexi development is really fast now. If somebody wants to help out I'm sure they would appreciate it."
    author: "Peter"
  - subject: "Re: SVG tools update???"
    date: 2003-02-21
    body: "Yes, any help is appreciated :)\n\nCheers,\n\nRob."
    author: "Rob"
  - subject: "Re: SVG tools update???"
    date: 2003-02-21
    body: "I've just updated koffice.org to remove the Kontour pages, and added a tiny bit of info about Karbon. Thanks for pointing out."
    author: "Chris  Howells"
  - subject: "Re: sodipodi"
    date: 2003-02-21
    body: "No sodipodi does not have any license restrictions on artwork created by it."
    author: "ismail donmez"
  - subject: "Re: sodipodi"
    date: 2003-02-22
    body: "it's funny....laugh\n\nam I the only one who got the gnome vs kde joke?"
    author: "steven"
  - subject: "Re: sodipodi"
    date: 2003-02-23
    body: "really, it wasn't meant as a joke; I just misunderstood the part where the article said \"you should use koffice-1.3 in making your icons\".  This just struck me as an odd requirement, but I now understand that they meant \"in selecting the set of icons to create, use recent KOffice as a guide\" (see other thread)."
    author: "LMCBoy"
  - subject: "For the love of god..."
    date: 2003-02-20
    body: "...please someone come up with anti-aliased Bold and Italic buttons."
    author: "Frank Rizzo"
  - subject: "Re: For the love of god..."
    date: 2003-02-20
    body: "Let me correct you:\n\nFor the love of GNOME.. \n\n(That was what you were trying to say, right?)\n"
    author: "ynnuf"
  - subject: "Re: For the love of god..."
    date: 2003-02-21
    body: "I notice this too ... it makes me close Kword/koffice and startup OpenOffice I'm willinng to bear 10 minute startup times and massive swapping just to avoid those glaring horrid Koffice icons!!!\n\nIOW This contest is gonna add some beauty to the wonderful Koffice Suite. Go Artists!!"
    author: "Clicked on OpenOffice Yesterday"
  - subject: "Re: For the love of god..."
    date: 2007-10-07
    body: "WOW!! How lazy are you?  Geeze!"
    author: "Frank"
  - subject: "Great idea, but how about stencil sets for kivio ?"
    date: 2003-02-20
    body: "KOffice is certainly the next area that requires a big effort to reach a status where Office users feel at ease with it and consider it stable/usable. It seems to be making quite a lot of progress lately, which is just great! Icons can only help people feel good using these applications, so this is certainly a good idea.\n\nThere is, however, something that I'd qualify as even more important: Kivio has been there for quite a while, and the code seems to be mature enough. The only thing that prevents anybody from using it seriously, though, is that it is delivered with a ridiculous number of stencil sets.\nI personally don't want to have to buy stencil sets from TheKompany, but I'd agree to spend some time in creating one or 2 sets (easy to do, since it is a simple XML format).\nI once came accross an open-source project that allows to turn xfig libraries into kivio stencils (http://sourceforge.net/projects/xfig2sml/). It does much of the initial job, the only problem being that there are no connection points, but this is fairly easy to add.\nIf anybody had the means of organizing work so that volunteers take care of one or two sets each, I think we could turn kivio into something very usable very quickly."
    author: "brisco"
  - subject: "Re: Great idea, but how about stencil sets for kiv"
    date: 2003-02-20
    body: "> means of organizing work\n\nHow about the koffice-devel mailing list?\n\nstart a thread :)"
    author: "fault"
  - subject: "Re: Great idea, but how about stencil sets for kiv"
    date: 2003-02-21
    body: "Like this one? http://lists.kde.org/?l=koffice&m=102267883117074&w=2"
    author: "Anonymous"
  - subject: "Re: Great idea, but how about stencil sets for kivio ?"
    date: 2003-02-21
    body: "It would be fantastic if you could provide additional stencils for Kivio. The KOffice mailing lists will have an open ear for that :-)"
    author: "Norbert"
  - subject: "Re: Great idea, but how about stencil sets for kivio ?"
    date: 2003-02-21
    body: "One of the best secrets of KOffice 1.2.1 is that Kivio has a Dia stencil import filter. Two drawbacks: It's told to only work with 75% of the Dia shapes and you can only work with one collection active at one time. For me it was as simply as \"cd $KDEDIR/share/apps/kivio/stencils;mkdir Dia;cd Dia;ln -s /opt/gnome/share/dia/shapes/* .\" to get them offered to me at next Kivio start."
    author: "Anonymous"
  - subject: "Scribus"
    date: 2003-02-21
    body: "Has it ever been discussed if Scribus should be part of KOffice?\nIt's a great DTP program! I've used it a bit and it is really a killer application!\nhttp://web2.altmuehlnet.de/fschmid/"
    author: "Tuxie"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "use it *a lot* and reort back :-)"
    author: "Clicked on OpenOffice Yesterday"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "I have used it a bit more than a bit... and I can assure you it's progressed a lot. The PDF output quality is really good compared to those of koffice. One of the main problems I find actually in koffice is the way they embed the fonts... TTF fonts look like crap on Acrobat Reader... "
    author: "Myself"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "Actually, they look great and are the standard fonts\nincluded in PDF docs in Windows and Mac worlds, along\nwith OT fonts."
    author: "Hmmm"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "You mean you are able really to do this ???:\n\n1) Write a document in kword using Comic Sans\n2) Export To PDF embedding fonts\n3) Open in Acrobat Reader and say: it looks nice! \n\nI tried... and the fonts are embedded as Type 3 (see in Acrobat Reader font properties)... which looks ugly in Acrobat. Yeah, I know it looks great in kghostview... but not everyone in this world uses KDE. And you call it a \"publishing tool\"...\n\nNow, afaik, the only solution is to embed those fonts as Type1. OpenOffice and Scribus do it properly. Kword can't seem to deal with it. Why not import Scribus' code?\n\nPlease let me know if I said something wrong above there. I'm not an expert in fonts and PS/PDF format.\n"
    author: "Myself"
  - subject: "No you are correct ..."
    date: 2003-02-22
    body: "KDE has a really great API for supporting printers (CUPS PDF-export PDF-email etc.) the only problem is there's not really one app out there that can actually print nice output to said framework ...\n\nFor all intents and purpose KOffice DOES NOT FUNCTION for printing.  It's thje largest \"blocker\" preventing the app from \"breaking out\" into big time use like OpenOffice (which doesn't work to well for interactive use but prints and imports/exports nicely).\n\nKDE and Linux printing in general SUCKS HORRIBLY.  We've made it look good, not crash, functional e-mail and browsers ... even some decent stabs/starts at office apps. What's missing is huge though:\n\n- good quality printing ... postscript is still patented and ghostscript is weak version of that ... (no easy screen to print equivalence unless you have postscript in the display too).\n- a good native well suported multi-media format and framework for supporting it anbd plugins for crap commercial closed media (ASF, WMV, MP3 etc ...) ARTs is getting old\n\n\nGnome does a bit better job on th output but has a crappy API/framework for allowing apps to print to devices .. KDE's is best bar none on that ..."
    author: "Feature vs. Function"
  - subject: "Re: No you are correct ..."
    date: 2003-02-22
    body: "Errm no *you* are wrong ;-)\n\nOr there's something wrong with your set up ... but you are wrong on certain points.\n\nCUPs rox ... LPRng is good too but everything is gravitating towards supporting CUPS and using it as a print interface to various other services (e-mail, DAV uploads, PDF and XML-FO rendering etc.) Magnificent!  This is one are where Unix is WAY OUT IN THE LEAD re: windows ... Apple is right along side us ;-)\n\nFor the output onscreen/onpaper well, yes, we are weaker  ... and certain Postscript technologes are patented - but not for long (a few key patents expire 2005 I think), and besides, much like X where XFree is starting to beat out commercial variants ... ghostscript is simply the best and most excellent postscript rendering environment out there in many ways.  And \"Postscript in the display\" is not dead.... see sourceforge for various GS extensions for X.\n\nRe: your setup:\n\nCheck if print previews in various applications are working and give you something that looks good.  If print preview looks crappy then there's something broken with ghostscript/Xft/your KDE font setup.  \nIf print preview looks good and printer output is crappy then it's your print drivers (Foomatic, GimpPrint etc.)  Be sure ghostscript, print drivers, cups and all your fonts are as new and as extensive as possible!! Install everything!!\n\nHere are the things that are wrong or need work:\n\n- seamless screenfont/printfont display.  We mix Type1, TrueType, Post/ghost, etc. it would be nice if Xft and fontconfig were as great a fix for printing as they are for X display fonts.  Unix/ X really has grown up to be like commercial graphics power house OSes and apps re: *display* (AA, Xrandr, etc.) but for printing we have a radically separate subsystem.\n\nI guess some might contrue this as an advantage ... (separate teams and tasks) but while it's possible to take say a Thai, or Indic font (TrueType) dump it into my ~/fonts/ dir and have beautiful Thai document display (assuming I've got encoding working right etc.) it is essentially impossible to have such a document display and print well automagically and out of the box ... i.e. useres can't deal with this ... printing and fonts is still a sysadmin type problem.\n\n- Print speed.  Since there's no tight integration of display and printing even printing a small simple document causes applications to lock up at times (while they wait for \"background printing\" to happen) and enormous processing power is used even just to print simple text.  This is a reall pain if you are doing print previews on your laptop on battery power!! \n\n- multi-media formats: yes bad support but not our fault! plus there are these fixes\n\n* ALSA (linux only though)\n* gstreamer (nicer if ported to Net|FreeBSD/Darwin etc.)\n* Helixcommunity\n* OGG/Vorbis (yeah baby!!)\n\nAnyway multi-media is outg of scope here since I'm talking about printing :-D\n\nI agree with everyone about the KOffice icons (especially the printer icon that pops up while the document is prepocessing/printing)."
    author: "KDE Fanatic"
  - subject: "Re: Scribus"
    date: 2003-02-25
    body: "It appears to me that the Qt PostScript driver simply doesn't work.\n\nI tried your experiment.  I'm not quite sure what OpenOffice did, but KOffice has this error message in the PS file:\n\n\t% No embeddable font for ComicSansMs found\n\nSuggestion: then don't embed it.\n\nWhy does it embed the fonts?  Or, at least, were do you turn embedding on and off?\n\nThat is really a rhetorical question.  There is a box to uncheck in qtconfig but it doesn't appear to help the situation.  And, in any case, there is no reason for fonts to be embeded if you will be printing the PS file on the same system.\n\nAnd the experiment resulted in substituting Helvetica for Comic Sans MS.  And then when I made a PDF, the margins were screwed up.\n\nRepeat: PostScript driver is BROKEN.\n\nSolution for embidding a TrueType font is supposed to be a scalable Type 42 font.  Perhaps this font doesn't allow embedding, but that is a problem for GhostScript.  The solution for the PostScript dirver is simply not to embeded it and list it as a system supplied font.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "Scribus has really made astonishing progress.\nIt definitively should be a part of KOffice!\nPerhaps some UI adjustments will have to be made\nin order to make it similar to the other KOffice\napplications.\nIf some KOffice developers are reading this:\nPlease get in contact with the developers of Scribus.\nThis would further improve the power of KOffice.\nImagine this:\nBitmap Editor, Vector Editor, DTP, Word-Processing, \nSpreadsheet, Database, integrated PDF Generator, ... all \nfor free. All cooperating with each other. All with a similar \nUI, similar Shortcut-Keys,.. This is not too far away.\nNow M$ Office, Adobe, try to match this.\n"
    author: "Stephan"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "It's not really the task of the Office team to try to adopt other projects. \nThis is still the task of the owner of the product, because it must be in the interest of the programmer to be part of the project.\n\nSo if the Scribus author would like to join, I'm pretty much sure we will let him do so.\nOTOH, Scribus is not a KDE application yet, it's a native QT app. So it would be a harder change for Scribus to fit into KOffice.\n\nI personally don't think it's useful to have apps in one office suite which don't ingtegrate, just to tell that we have more apps in our suite than blabla.\nThe apps in a suite must fit together, otherwise it's not a suite."
    author: "Philipp"
  - subject: "Re: Scribus"
    date: 2003-05-06
    body: "As you know, Quark XPress is very expensive. Sribus may be mature enough for newspapers, writers and printers to migrate to GNU/Linux. People are tired to pay taxes to Quark/Microsoft/Adobe/Whatever. We need more freedom.\n\nNo matter if Scribus is a Qt application only. The history of GNU software shows that you sometimes have to make exceptions. The only matter is flexibility.\n\nCome on: your team will never be able to release a software like Scribus in KOffice. So why hide and pretend it is not compatible. The simple fact that scribus is free software, makes it a potential candidate for integration into KOffice."
    author: "Jean-Michel POURE"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "What I would suggest is an \"Scribus output plugin\" for kword... the PDF generation would improve greatly. I just wonder how difficult would this be to implement. Then, if Scribus was integrated, one could in one-go do: kwd->scd->pdf"
    author: "Myself"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "How about just finding out what makes the pdf output of Scribus so great and fixing KDE's output to match.  That way all KDE applications would benefit, since making PDFs is part of the print system and avaliable in every app.\n\nKWord is supposed to be able to do DTP tasks, with its frame layout features.  It would be kind of silly to have both KWord and Scribus in KOffice at the same time."
    author: "not me"
  - subject: "Re: Scribus"
    date: 2003-02-21
    body: "The trick seems to be that KOffice uses QT in order to generate the postcript files, which has certain limitations while embedding fonts. There was some long discusson about it in the kde-devel threads ... and the solution doesn't seem to be easy (too much work to be done).\n\nNow... KWord is supposed to be working as Desktop Publishing tool, but... what about \"retouching\" or improving documents? I'm speaking about Acrobat(Distiller, writer,.. whatever you call it) features such as adding notes or small touches to pdf files (really useful for researchers), hyperlinks, or even importing pdf files.\n\nI doubt that with the current kwd file structure every single PDF trick can be represented. This means that it would be easy to convert kwd->pdf, but not the other way around: pdf->kwd.\n\nScribus has the advantage that is oriented specifically to work with PDF's, so the document format represents every single detail (supposedly).\n\nNow... merge Scribus + Kword in something like, KwordCribus ;-) and... oh man, ... I need it."
    author: "Myself"
  - subject: "Another Thing"
    date: 2003-02-21
    body: "Please design a new Konsole icon.  The blue is wrong and bad for usability.  I always get confused when I alt-tab and don't know what window to switch to because all the icons look the same!  And I don't see the selection anyway because it's yellow on gray!"
    author: "anon"
  - subject: "Re: Another Thing"
    date: 2003-02-22
    body: "I agree completely! Why must all icons be blue now? I can no longer distinguish between Konqueror and Konsole. Why can't Konqueror be green-blue and Konsole black like it used to be.\n\nIs it so that everyone can feel how it is to be color-blind?"
    author: "Erik"
  - subject: "kexi?"
    date: 2003-02-21
    body: "why is kexi not part of the content?"
    author: "hans hermann"
  - subject: "Re: kexi?"
    date: 2003-02-21
    body: "Because we are not sure if it will be a part of Koffice by 1.3 yet. And it now uses standart koffice icons ( load/save etc ) ."
    author: "ismail donmez"
  - subject: "Re: kexi?"
    date: 2003-02-21
    body: "hi\n\nwell, i'm quite sure that kexi will be a part of koffice 1.3 but the main reason is, that we have got our \"own\" artist (Kristof Borrey known from iKons) who creates all icons we need :)\n\ne.g. there is a svg icon avaible under http://luci.bux.at/kexi.svg all other kexi-only icons are created by him as well. the rest are usual koffice icons.\n\nand i think that kristof has very good skills.\n\nlucijan"
    author: "lucijan"
  - subject: "Format & Style"
    date: 2003-02-21
    body: "'The preferred icon format is SVG...'\n\nAnd that means I should use Adobe Image Ready which is used by Crystal icon team? Mo way. Because of Adobe (and its attack on KDE because of the name of the KOffice applications) and because I don't use Windows/Mac.\nSodipodi & other open source apps are not ready.\n\n'The preferred icon style is Crystal...'\nWhy this contest at all?\nYou have Conectiva/Everaldo Crystal Icon set. Its style is Crystal. Which is BTW default. Use it. Authors just have to finish office icons. And there are 6-7 in the team. It shouldn't be a problem.\n\n.....................\n\nP.S. IMHO Koffice icons should not be created in Crystal style because of the usability issues. Take a look at AbiWord and see what a really proffessional office icons look like. \n\nCheers,\n\nantialias\n\n"
    author: "Antialias"
  - subject: "Re: Format & Style"
    date: 2003-02-21
    body: "> And that means I should use Adobe Image Ready which is used by Crystal icon team?\n\nSodipodi is completely ready to create such icons.. I'm not sure why you said it's not.. what are it's missing features? I've created quite complicated things in Sodipodi before.\n\nThat being said, Adobe Image Ready is still probably the best tool for the job, if you have it, have windows/macosx, and are still willing to use Adobe products (heh... virtually all artists have to... adobe does create very nice products)"
    author: "fault"
  - subject: "Re: Format & Style"
    date: 2003-02-22
    body: "'Sodipodi is completely ready to create such icons..'\n\nNo, it is not. I've tried every and each release (compiled) and always had strange crushes when I wanted to save my work. And you can imagine how frustrated one can be when spending a lot of time to create an icon and it get lost because the application crushes when you want to save you work.\n\n'Adobe Image Ready is still probably the best tool for the job...'\n\nProbably.\n\n'if you have it, have windows/macosx, and are still willing to use Adobe products'\n\nI don't have it (Adobe Image Ready), I don't have windows/macosx anymore. I'm actually waiting for Carbon to get ready and I hope it will be ready very soon."
    author: "Antialias"
  - subject: "Re: Format & Style"
    date: 2003-03-16
    body: "sorry \nbut crystal is wonderfull\n\nif you don't like crystal because you don't create a theme of icons better?  \nI am saddened in seeing people like you criticizing the good work done in crystal icons"
    author: "Dirceu "
  - subject: "KOffice 1.3"
    date: 2003-02-22
    body: "Why doesn't KOffice 1.3 integrate TOra rather than Kexi? "
    author: "Hakenuk"
  - subject: "Re: KOffice 1.3"
    date: 2003-02-22
    body: "because:\n\n1) koffice know about TOra while including kexi\n2) nobady contacted the koffice team and requested a include of tora\n3) TOra aims at database-power users only, kexi is done for more trivial purposes e.g. personal and small buissines\n4) kexi is based on the koffice-framework\n\nthat's it :)\n\nlucijan"
    author: "lucijan"
  - subject: "XML Support"
    date: 2003-02-24
    body: "Hi all!\n\nDoes KOffice support XML? That is, what is the structure of files KOffice genrates? Because, XML would give us readibility, portability, flexibility, etc...\n\nSincerely!"
    author: ".coder"
  - subject: "Re: XML Support"
    date: 2003-02-24
    body: "KOffice files for current versions are .zip's of a few XML files. (Older versions used tar.gz's of XML files)\n"
    author: "Sad Eagle"
  - subject: "developers"
    date: 2003-02-24
    body: "Call for developers.\n\n\nYou all know KOffice needs more developers. If you can do something you can help bringing koffice on par with MS Office or Staroffice.\n\nI would code if i had time sorry :-( :( :-)\n\n\ngo go go\n"
    author: "chris"
  - subject: "Re: developers"
    date: 2003-03-07
    body: "You don't need that much time: there are many very small things to do.\nEvery small piece helps"
    author: "Norbert"
  - subject: "Pretty icons?"
    date: 2003-02-24
    body: "I'd rather see stability from KOffice, myself. Here are some things which could be improved:\n\nThe spreadsheet: it isn't as slow as it was when loading files, but it still crashes when doing basic stuff such as copying and pasting. Fortunately, it saves recovery files as it goes.\n\nThe word processor: try importing a 50 page RTF file, and there's no need to call OpenOffice.org slow any more - you can easily load OpenOffice.org, load the RTF file, probably even read it yourself, save it as something else, and quit before KWrite has done anything.\n\nNow, I use KSpread for various things, but what KOffice really needs is more focus on stability and reliability before people get over-interested in tarting it up. Myself, I'd even contribute to the development if I didn't have a huge list of other things that I'd also be interested in contributing to. Meanwhile, I don't think it's incomprehensible that KDE developers get a bad reputation, *despite* their hard work in producing something as useful as KDE is, because it's the flashy, clever stuff that seems to get developer/community attention rather than the more noble objective of being able to trust your data to various applications.\n\nAs it is, I *just* *about* trust my data to KSpread, but it would be nice to trust KSpread just a bit more."
    author: "GuestX"
  - subject: "Re: Pretty icons?"
    date: 2003-02-24
    body: "Suggestions for stability improvments are great.  Maybe they are having this contest so that the developers can concentrate on that and have people who like doing stuff like making icons do them.\n\nI think this is a great idea.  They letting the developer of KOffice work on the code and asking for people with talents in graphics design to make the icons."
    author: "Randy"
  - subject: "Re: Pretty icons?"
    date: 2003-02-26
    body: "Yes, let's hope that the essentials get a bit more attention. After all, an office suite can look really good, but it's useless if it crashes, loses data and behaves badly. Still, KOffice has come on a lot over the past year or so."
    author: "GuestX"
  - subject: "'Please try to keep posts on topic'"
    date: 2003-02-25
    body: "\n1. How about stencil sets for Kivio? & thread\n\n2. Has it ever been discussed if Scribus should be part of KOffice? & thread\n\n3. KDE and Linux printing in general SUCKS HORRIBLY. & thread\n\n4. Why is kexi not part of the content? & thread\n\n5. Why doesn't KOffice 1.3 integrate TOra rather than Kexi? & thread\n\n6. Does KOffice support XML? & thread\n\n7. I'd rather see stability from KOffice, myself. Here are some things which could be improved... & thread\n\n--------------------------------------------\n\nI thought we were discussing Koffice Icon Contests here, but 85 % of postings are off topic."
    author: "Antialias"
  - subject: "Re: 'Please try to keep posts on topic'"
    date: 2003-02-25
    body: ">  I thought we were discussing Koffice Icon Contests here, but 85 % of postings are off topic.\n\nVery true, please use http://kde-forum.org/ for postings not related to news stories."
    author: "Anonymous"
---
The <a href="http://www.koffice.org/">KOffice</a> <a href="http://www.koffice.org/developers.phtml">developers</a> have been making outstanding progress towards their goal of creating a useful, powerful and reliable KDE office suite. But whereas the technology
in KOffice has been steadily improving, its visual appearance has not been keeping pace. To address this issue, the KOffice development team is pleased to announce the KOffice Icon Contest. This contest is being held to ensure that <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">KOffice 1.3</a> not only <i>works</i> better but also <i>looks</i> better  than any previous release.
<!--break-->
<h3>Aim of the KOffice Icon Contest</h3>

<p>One of the goals of KOffice 1.3 is to deliver an improved user interface that compliments the progress being made by the KOffice developers in the code. Icons and related artwork are a vital part of that interface improvement.</p>

<h3>Entering The Contest</h3>

<p>The KOffice Icon Contest is open to everyone. All submissions from individuals or teams of artists that meet the guidelines for the contest are welcome.</p>

<p>To participate, the following  materials should be sent as a single submission to the contest organizer <a href="mailto:voidcartman@yahoo.com">Ismail Donmez (voidcartman@yahoo.com)</a>
and the <a href="mailto:koffice@kde.org">KOffice users list (koffice@kde.org)</a>:

<ol>
<li>A single screenshot showing all of the icons in the submission. The individual icons in the screenshot should be either 22x22 or 32x32 pixels in size.</li>
<li>An FTP or Web URL from which the icons can be downloaded. Please do not email the icons themselves!</li>
</ol>
</p>

<h3>The Icons</h3>

<p>The icons should be provided in a single compressed archive containing icons for each of the following KOffice applications:

<ul>
<li> KOShell (Koffice work area)</li>
<li> KWord (Word processor)</li>
<li> KSpread (Spreadsheet)</li>
<li> KPresenter (Presentation program)</li>
<li> Krita (Bitmap graphic editor)</li>
<li> Karbon14 (Vector graphic (SVG) editor)</li>
<li> KFormula (Mathematical formula editor)</li>
<li> Kugar & Kugar Designer (Report creator)</li>
<li> Kivio (Flow chart and diagram editor)</li>
</ul>

<p>The icons for each application should include:

<ul>
<li>an application icon which will appear in the KMenu and elsewhere</li>
<li>toolbar and menu icons</li>
<li>icons for use in dialogs such as the new document and configuration dialogs</li>
</ul></p>

<p>All icons must be supplied in both 22x22 and 32x32 sizes. The application icons must also be supplied in 48x48, 64x64 and 128x128 sizes. The preferred icon format is SVG though PNG is also acceptable. The preferred icon style is Crystal SVG.</p>

<p>Artists should use KOffice 1.2.1 or later (e.g. KOffice from CVS) in creating their submissions.  You may further wish to review the old <a href="http://artist.kde.org/introduction.html">KDE Icon Style Guide</a> although portions of it have now been obsoleted with the adoption of Crystal SVG.</p>

<h3>Licensing of Submissions</h3>

<p>Just as with the official KDE icons, all icons submitted to the contest must be licensed under the LGPL. All submitted icons must also belong to the artist(s) who submit them. Icons belonging any other 3rd party will not be accepted.</p>

<h3>Contest Timeline</h3>

<p>The KOffice Icon Contest will be open for submissions for two (2) months starting February 20, 2003. At the end of those two months all of the submissions will be examined by KOffice developers and users and the winning submission will be announced by the KOffice development team a week later.</p>

<h3>The Prize</h3>

<p>The award for winning the KOffice Icon Contest is to have your icons distributed along with KOffice as its official icons and an acknowledgement of contribution in all of the KOffice programs.</p>