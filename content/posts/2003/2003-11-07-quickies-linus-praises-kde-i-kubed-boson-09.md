---
title: "Quickies: Linus Praises KDE, I-Kubed, Boson 0.9"
date:    2003-11-07
authors:
  - "numanee"
slug:    quickies-linus-praises-kde-i-kubed-boson-09
comments:
  - subject: "thanks linus"
    date: 2003-11-07
    body: "yup, nice that linus likes it.\nbut dispite linus not able to find new onces i still find plenty of bugs to be crushed. (especially konqy)\n\nabout the structure and such. yes!!! kde rules absolutely here\nit really exploits the benifits of the object orientated nature and i found it too be insanely flexible. great work there!\n\nand with new projects as great as kopete and kontact comming into the next stable KDE i see a very bright future for KDE.\n\nkeep it going guyes!\nKDE is great work!"
    author: "Mark Hannessen"
  - subject: "not that much bugs in 3.1.4"
    date: 2003-11-07
    body: "I'd be nice if Linux whould start using CVS and help fixing bugs ;-)"
    author: "Superstoned"
  - subject: "Re: not that much bugs in 3.1.4"
    date: 2003-11-07
    body: "no, its better to us all, seeing linus working at his domain! he should (and does) his spent his time on doing the kernel-dev stuff.\n\n"
    author: "gunnar"
  - subject: "Stupid"
    date: 2003-11-07
    body: "Well, I'm in front of a W2000 box, and I found this kubed guy blocked IE requests because not being \"standards compliant\". Does he thinks he win anything doing this?\n\nWell, I guess I'll see that tonight in home, in my own computer, as I won't download Bloatzilla nor Opera just because he wants Windows people feel his frustration when using www...\n\nBTW, I had a Linux box I mantained here, with KDE, but it died 2 weeks ago. Our 2 another Linuxes are GUI-less servers...\n"
    author: "Julio G\u00e1zquez"
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "Don't be mad of him - as a linux kernel developer he's in permanent touch with microsofts attitude to standards. And I can tell you - if you're trying to implement a something-standard you WILL hate people fussing around with the specification - especially with the goal to \"take over\" the standard."
    author: "Ruediger Knoerig"
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "No, it's a pretty stupid thing to do.  And he's no linux kernel developer.  He barely knows what's going to be in 2.6, and is playing around with precompiled Mandrake kernel packages."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "I guess it's just his way of saying 'this is what it's like when you have a needlessly IE only page.'  Call it what you like: stupid, immature, whatever.  It's his page, and if he doesn't want to be 'diplomatic' with respect to supporting users, that's his decision.  \n\nPersonally, I liked the way it reminded me that I'd forgot to switch Opera back to saying it was Opera ;-)"
    author: "John Allsup"
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "no, it's not a pretty stupid thing to do, it's a pretty smart thing to do. does the word Protest have any particular meaning to you? If it does think of this as a protest, if it doesn't i think you should redefine just your standards for stupidity."
    author: "illogic-al"
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "people here already said it but I'm not a kernel developer. \nand yes i do play around with precompiled kernels. As a matter of fact i dont even compile my own kernel anymore. So what if i save a few megs of space, i've got gigs more left :D"
    author: "illogic-al"
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "Well, it worked here with W2K and Firebird. It's not THAT big of a install you know. And you would be doing your part in breaking the IE-hegemony."
    author: "Janne"
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "Ever used an internet cafe? Not much chance of installing other browsers here..."
    author: "V79"
  - subject: "Re: Stupid"
    date: 2003-11-08
    body: "The one we have here around the corner has Linux with KDE and Mozilla. =P\nAnd 'easy everything' here allows you to do whatever you want to the Windows system, after a time out the system just get hard resetted, data gets wiped out (virtual drive I guess) and freshly rebooted (that's probably the most sane way under Windows *shrug*).\nHowever internet cafes always remind me how much of a time saver tabs really are."
    author: "Datschge"
  - subject: "Re: Stupid"
    date: 2003-11-07
    body: "Julio, i'm not trying to win anything. The 'IE blocker\" as i like to call is the for two reasons. One is it's kinda satyrical humor, poking fun at those IE only pages people put up.\nMore than that though it's a protest against IE and its shitty (yes, i said shitty) CSS support. If the page rendered properly in IE then I'd have no problems letting IE users in. I, however, refuse to 'fix' my page just for them (or you) if they insist on using that browser. if you don't want to download bloatzilla, or opera by all means don't. You have that option (just as I have the option to not allow IE browsers into my site until they get proper css support)."
    author: "illogic-al"
  - subject: "Re: Stupid"
    date: 2003-11-09
    body: "And people have the choice not to come to your site....\nI'm using Konqueror, no way to see the site correctly. I now know IE won't be better, so I'll simply forget that this site does exist...\n\nNice way of using the web for sharing informations.... :("
    author: "Rol"
  - subject: "Re: Stupid"
    date: 2003-11-10
    body: "> I'm using Konqueror, no way to see the site correctly.\n\nI'm using Konqueror, two, and everything shows up ok. What's the matter? If you can't see the last line on his 3.2 review just reduce the font size for a moment.\n\nIf you wanna protest, illogic-al, why don't you allow IE access to your page *without* fixing it? Thus you don't have extra work and it is some kind of protest, too.\n\nAlso, Julio, why not installing Opera? It's fast and lean. At work, I must use IE for some three sites, but mostly I use Opera (which even is my default browser on Windows). I don't know why people tolerate IE anymore. It's too clunky!\n"
    author: "Gr8teful"
  - subject: "Re: Stupid"
    date: 2003-11-10
    body: "I meant I'm using Konqueror, TOO... sorry, not a native English speaker... :-(\n"
    author: "Gr8teful"
  - subject: "Re: Stupid"
    date: 2003-11-15
    body: "sorry about that, I've been messing around with the site for a while so I did probably break it for a while there :-/"
    author: "illogic-al"
  - subject: "Re: Stupid"
    date: 2003-11-10
    body: "It's really no different than those multitude of sites that do the same except bring you to a rather blank page stating \"This site best experienced using Internet Explorer.  Download it for free!\"\n\nDoesn't make it any less stupid, though.  He's just got lots of company."
    author: "CK"
  - subject: "Boson looks awesome!"
    date: 2003-11-07
    body: "I installed Boson with apt-get, and gave it a try.\n\nAbsolutely great game! It still needs developing, but it's definetly coming along nice.\n\nSeems like there is another game I can show my son :)\n\nEleknader"
    author: "Eleknader"
  - subject: "Niederdeutsch"
    date: 2003-11-07
    body: "Will KDE 3.2 get a translation to niederdeutsch/plattdeutsch/lower German - ISO language code nds? I heard about a translation project in the news... \n\nAny details?"
    author: "Gerd"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "I've heared of a translation to the klingon language as well, so just go for it and create a KDE-i18n-package with all the german dialects on your own - KDE is opensource - everybody can take part and bring useless (but funny) stuff ;-)\n\nSCNR\nPietz\n"
    author: "Andreas Pietzowski"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "I don't share the view that a translation to a minority language (not a DIALECT or a FUN language) was \"useless\". However it would be nonsense to start a project on my own (I can't do that alone) if there already was work in progress. So..."
    author: "Gerd"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "Is plattdeutsch really a language?!? I thought it's an german dialect spoken in the north. In which country do they speak niederdeutsch or plattdeutsch? Is the bavarian style of talking and writing a language, too?\n\nThanks for a proper answer\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "it is a independent language, no dialect. In the european language charter it is recognized as a REgional language. It was spoken in 1/3 of historical Germany and menonite minorities in paraguay. A lower German dialect Luebsch was official language of the Hanse trade association that controlled large parts of the baltic sea. However due German national state building (in the late 19. century) it was suppressed and left to \"rural people\". same happens in the third world today with many languages that are dropped due to urbanisation in favour of the former colonial language.\n\nSee Wikipedia! There is even a platt wikipedia.\n\nit would be a competitive advantage if KDE could fullfill the European Language Charter. You should know that an administrative body is obliged to answer in a reguinal language."
    author: "Hans Janssen"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "http://nds.wikipedia.org/wiki.cgi?Plattd\u00fc\u00fctsch\n\nor\n\nhttp://www.wikipedia.org/wiki/Low_Saxon_language\n(currently the server is unavailable)\n\nWikipedia op platt\nhttp://nds.wikipedia.org/wiki.cgi?HomePage"
    author: "Onno"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "Jo! "
    author: "Andre"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "Low Saxon (Low German) was recognized as a legitimate language only recently. Prior to that, the dialects of Northern Germany were regarded as belonging to German, and the dialects of the Eastern Netherlands were regarded as belonging to Dutch. Because of centuries of official and educational neglect and suppression, the language has been unable to develop a standard dialect and a standard orthography, even though there is a notable written Low Saxon tradition. There are several spelling conventions, based on German principles in Germany and based on Dutch principles in the Netherlands, and few writers strictly adhered to any of them. So far, spelling systems have been devised and developed mostly by activists and writers with insufficient familiarity with phonological principles, and most Low Saxon writers have been trying to write their home dialect \"phonetically.\" This disunity, together with attempts to germanize Low Saxon orthography, has led to a confusing situation that does anything but help this minority language survive.\n\n"
    author: "Language"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-08
    body: "A better question is if there will be any differences to translate.\n\nMost regional dialects have a plethora of terms for \"rutting\", but there are few local words for \"burning DVD's\" (though some of the rutting ones might usefully be employed)\n\nThe sorts of terse phrases used in computer software are likely to be depressingly similar \n"
    author: "Simon"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-08
    body: "Perhaps this would be the funny issue: define new words in the reguional language and make it ready for information society."
    author: "Henno Harms"
  - subject: "Re: Niederdeutsch"
    date: 2005-01-09
    body: "> I don't share the view that a translation to a minority language (not a\n> DIALECT or a FUN language) was \"useless\". However it would be nonsense to\n> start a project on my own (I can't do that alone) if there already was work\n> in progress. So...\n\nHi Gerd,\nindeed there's a project for KDE in Low Saxon (or Plattduetsch). It's located at <a href=\"http://nds.i18n.kde.org/\">http://nds.i18n.kde.org/</a> (though the page is a little outdated now), and participation would be quite appreciated. And if you happen to use the Opera browser, you might find this link interesting: <a href=\"http://home.foni.net/~sdibbern/oppage/indexop.html\">Opera op Platt</a>\n\nIf you need further information, send me a mail: s [underscore] dibbern [at] web [dot] de\n\n"
    author: "Soenke"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-08
    body: "Klingon?? - sound like a KDE Apllication... :-)"
    author: "Matze"
  - subject: "Linux lacks a common multimedia framework"
    date: 2003-11-07
    body: "The area where linux is SEVERELY lacking is in a common crossplatform/crossdesktop multimedia framework.\n\nThe KDE and GNOME people need to agree on a standard for KDE4/Gnome2.6... Alot of discussion has been heading towards the GStreamer + MAS sound server.  \n\nI personally hope that a solution to this problem is found soon!  Why make the users and developers suffer?"
    author: "Grapevine"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-07
    body: "what's wrong with arts!?\n}:D"
    author: "illogic-al"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-07
    body: "ARTS IS EQUAL TO SHIT!\nit never really works! always the same problems!\n\n1. starting \"xmms\"\n2. starting playing a song\n3. getting error message about \"busy device\"\n\ndon't tell now, it is relating to xmms!\n\nanother reason:\nplaying multiple wave-files, or mp3+wave at the same time isn't possible, at least not by default. really, since YEARS, the first thing to switch of \"aRts\", after a new Linux or KDE installation!\n\nAlso, i hope, Linux could be used for PROFESSIONAL audio editing/engineering/recording etc. compared to \"Cubase\" and Co. !!!!!\nthat's all, well, waiting for a beautiful and strong KDE3.2!\n\nBy the way, what about the Idea, to collecting money for a simple but\nprofessional TV Commercial about KDE/Linux ?!?!?! what stands against that idea !\n\nbye!"
    author: "nx7000"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-07
    body: "Use the aRts module under XMMS.  busy device means you are not using aRts but trying to access the device directly."
    author: "anon"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-08
    body: "Well, I think it was more of an example.  I mean, we can't really switch all\nof our apps to arts.  XMMS is one that we can fix, by downloading the plugin and blah blah blah.  It really is a pain, even with that wrapper.  We need things simple.  Sure it works, but its just a hack and an annoyance."
    author: "Nonya"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-12
    body: "So what? You'd have the same problems with GStreamer.\n\nIf the thing isn't built right into the kernel, you need either a OSS/ALSA wrapper for it, or a output plugin."
    author: "So?"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-08
    body: "Well personally I've never managed to get aRts to play sound without severe latency (sometimes up to 1 second!). If you tweak the slider in the KControl applet it just makes it stutter.\n\nIt could be a hardware/driver issue, I suppose (I have an SB Live), and maybe this has improved in KDE 3.2. Using the 2.6 kernel with ALSA may improve things as well."
    author: "Paul Eggleton"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-11
    body: "If you have a sblive then you have no need for arts anyways.  The kernel emu10k1 drivers support hardware mixing.  IIRC up to 64 apps can open and write to /dev/dsp at a time."
    author: "MrGrim"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-12
    body: "Yes, that's all very well except quite a lot of KDE apps insist on using aRts, so you have to use it anyway."
    author: "Paul Eggleton"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-08
    body: "I think the general opinion is that, while its suitable for desktop stuff, its can't handle the demands of serious multimedia apps, especially the latency requirements."
    author: "Rayiner Hashem"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-07
    body: "What discussion is headed to GStreamer + MAS?"
    author: "anon"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-08
    body: "The one that finally realizes ARTS sucks monkey."
    author: "J"
  - subject: "Re: Linux lacks a common multimedia framework"
    date: 2003-11-08
    body: "I think a network transparent multimedia system capable of connecting different computers, palmtops, TV etc. for everything multimedia related might be the right way to go in the long run. The German computer magazine c't had an article about the \"Network-integrated Multimedia Middleware (NMM)\" project which looks very good. http://www.networkmultimedia.org/"
    author: "Datschge"
  - subject: "On another note"
    date: 2003-11-07
    body: "America's Army for Linux 2.0 was released just yesturday and it really is quite a good FPS."
    author: "Alex"
  - subject: "Re: On another note"
    date: 2003-11-08
    body: "sweet dude. thanks for the heads up"
    author: "illogic-al"
  - subject: "Re: On another note"
    date: 2003-11-08
    body: "Yeah bros, lets go pretend to kill people!!"
    author: "startledstupid"
  - subject: "Opera 7.1 is faster than Konqueror :("
    date: 2003-11-09
    body: "I downloaded Opera 7.1 static on my ASPLinux 9. Opera started faster and loaded pages very fast. Can't we have Static Konqueror binary (or better full KDE as a static binary package) for speed?"
    author: "KDE User"
  - subject: "Re: Opera 7.1 is faster than Konqueror :("
    date: 2003-11-09
    body: "Dynamic linking is actually faster than static linking, and Opera 7.x is actually faster than Konqueror 3.1. I find 3.2 to be around the same speed as Opera 7.x however, which means fast."
    author: "anon"
  - subject: "Re: Opera 7.1 is faster than Konqueror :("
    date: 2003-11-10
    body: ">> I find 3.2 to be around the same speed as Opera 7.x however, which means fast.\n\nGood to know this, as I found Konqueror renders some pages Opera cannot (nor Mozilla).\n\nBTW, Konqueror could use some serious usability enhancements (like not having the \"create new tab\" and \"destroy tab\" together... DOH!). BTW, Firebird little closing [x] on the tab is cool... and BMW, it's a car. :-)"
    author: "Gr8teful"
  - subject: "Re: Opera 7.1 is faster than Konqueror :("
    date: 2003-11-10
    body: "> BTW, Konqueror could use some serious usability enhancements (like not having the \"create new tab\" and \"destroy tab\" together... DOH!). BTW, Firebird little closing [x] on the tab is cool... and BMW, it's a car. :-)\n\nAlready done in 3.2. One thing that propels Konqueror further than Firebird is spell checking in forms (like Safari).. only if corrections appeared in the context menu like MS Word =) "
    author: "anon"
---
<A href="mailto:b_mann@gmx.de">Andreas Beckermann</a> wrote in to point out the <a href="http://boson.sourceforge.net/announces/boson-0.9.php">huge amount of progress</a> in the latest release of <a href="http://boson.sourceforge.net/">Boson</a> (0.9), a real time strategy game for KDE.  <a href="http://boson.sourceforge.net/screenshots.php">Screenshots here</a>.   Right after the <a href="http://www.kde.org/announcements/announce-3.2beta1.php">beta announcement</a>, KDE user <a href="mailto:jokerman64@netzero.net">illogic-al</a> submitted his <a href="http://uhartford240007.hartford.edu/new_kde32.html">personal preview of KDE 3.2</a> on <a href="http://uhartford240007.hartford.edu/">I-Kubed.org</a>.  It's been noted that there may be inaccuracies in the review -- but can you <a href="http://slashdot.org/article.pl?sid=03/11/03/1855220">feel the excitement</a>?!
Finally, several of you also wrote in to point out Linus Torvalds' comments on KDE in <a href="http://www.oetrends.com/news.php?action=view_record&idnum=277">a recent interview</a> on <a href="http://www.oetrends.com/">OET</a>:  <i>"I think the biggest single thing that has happened [...] has been a lot of good library frameworks. Qt in particular I think made a huge difference. And the KDE libraries and toolbuilder infrastructure. [...] I just noticed that the KDE people consider it more important to have it working, and sane.  
I don't get involved very much. I used to send a lot of bug reports to the KDE people, until I didn't have bugs anymore and I stopped."</i>  Thanks, Linus!  
<!--break-->
