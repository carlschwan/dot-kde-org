---
title: "KDE at Chile's Entel Event"
date:    2003-12-03
authors:
  - "dmac-vicar"
slug:    kde-chiles-entel-event
comments:
  - subject: "i18n?"
    date: 2003-12-02
    body: "Why didn't you take the screenshots with spanish localization?"
    author: "Ernst Persson"
  - subject: "excellent"
    date: 2003-12-02
    body: "Thanks for your promotion work Duncan.  KDE needs more people like you!  I hope Chileans around here are motivated to join your effort."
    author: "anonymous"
  - subject: "from the kde-is-no-longer-german dept"
    date: 2003-12-02
    body: "Didn't I hear that Duncan has a German passport?  :-)"
    author: "Scott Wheeler"
  - subject: "ERP?"
    date: 2003-12-03
    body: "What _is_ zerp exactly?  What is an \"enterprise resource planner\"?\nThe website is in ... chilean?\n\nCan I use it to plan my projects?  :)\n\nOh, and does it have an english i18n translation?\n\n"
    author: "JohnFlux"
  - subject: "Re: ERP?"
    date: 2003-12-03
    body: "Mod +1, Interesting.  \n\nIf possible, I would like to know what zerp is too!"
    author: "anonymous"
  - subject: "Re: ERP?"
    date: 2003-12-03
    body: "<a href=\"http://en2.wikipedia.org/wiki/Enterprise_resource_planning\">http://en2.wikipedia.org/wiki/Enterprise_resource_planning</a>\n"
    author: "cloose"
  - subject: "Re: ERP?"
    date: 2003-12-03
    body: "> The website is in ... chilean?\n\n<cough>Spanish</cough> -- That's the language that tends to be spoken on the southern half of that continent.  ;-)\n\nOh, and KDict -> ERP should help you out.\n \n"
    author: "Scott Wheeler"
  - subject: "Re: ERP?"
    date: 2003-12-03
    body: ">> <cough>Spanish</cough> -- That's the language that tends to be spoken on the southern half of that continent. ;-)\n \nAnd in the whole continent, very soon ;) (35 million spanish speakers in the US and rising)\n"
    author: "Sergio Garcia"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "(a) The US isn't on the same continent and\n(b) Brazil and French Guiana don't speak Spanish.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "> (a) The US isn't on the same continent\n\nNot exactly. The three Americas are actually just one continent, so the US is in the same continent as Brazil."
    author: "Henrique Pinto"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "Aparently this depends on where you're taught a system of continents:\n\nhttp://en2.wikipedia.org/wiki/Continents\n\nInteresting quote: \"The seven continent model is commonly taught in Western Europe and North America, while the six continent (combined Eurasia) model is also taught in North America and is the primary continent model used in scientific contexts. The six continent (combined Americas) model is commonly taught in Eastern Europe and South America.\"\n\nApparently that's the system taught there but not in the countries where I've lived.  :-)"
    author: "Scott Wheeler"
  - subject: "Continents"
    date: 2003-12-04
    body: "I'm born in South Western Europe.\n \nWhen I was a kid, my teachers said there are 5 continents: Europe, Asia, Africa, America and Oceania. Later they added Antarctic as a 6th continent.\n \nI don't know what it's being taught currently."
    author: "Quique"
  - subject: "Re: ERP?"
    date: 2003-12-03
    body: "Yes, very soon we will apply an i18n to all this.... please be patient :-)\n\n\nRegards\nRaimundo \n(kerp's slave)"
    author: "Raimundo"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "Brazilian Portuguese is spoken by at least 170 millions of South Americans (the brazilians, of course), and most of them do not speak spanish (me included). I believe that is a considerable part of the southern part of the continent."
    author: "Henrique Pinto"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "Ah, yeah, actually I just looked at a map -- I hadn't realized how far south Brazil's east coast goes.  That's what I was trying to get at with \"the southern half of the continent\"."
    author: "Scott Wheeler"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "As stated in another post, the three Americas are just one continent. Therefore, I thought you meant South America when you said \"the southern half of the continent\".\n\nI was not offended by your post. It is common to hear misinformation about Brazil. As I believed that was happening here, I tried to give accurate information. Just yesterday I had to told an United States Citizen (all people born in the three Americas are Americans, so I have to specify here) that people don't live up in trees here, nor do they eat other humans. Last week I talked in a chat with a british that got amazed that there are computers in Brazil.\n\nAnd most US Citizens believe that the Amazon Rainforest is not part of Brazil, instead being an \"International Area\", what is caused by their educational system that seems to incentivate this misinformation.\n "
    author: "Henrique Pinto"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "> Just yesterday I had to told an United States Citizen (all people born in the \n> three Americas are Americans, so I have to specify here) [...]\n\nThat's true in Spanish and presumably Portugese, but not in English (except in certain specific contexts).  When I speak Spanish I refer to myself as a \"un estadounidense\" but the correct translation of that term into English is \"American\"."
    author: "Scott Wheeler"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "Is it like that in British English too?\n\nAnd which term do you use to refer to anybody from the New World?"
    author: "A.C."
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "South American.\n\nFor whatever reasons -- probably language and money -- people from the US are called Americans, and people from the rest of the America's are called South Americans, except the Canadians who are called ... well, Canadians.\n\nI am English but live in Spain now, and was always getting caught out by that one -- calling the US, America."
    author: "Dominic Chambers"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "Are Mexicans also \"south americans\"? And people from Cuba, Nicaragua, Panama, etc.?"
    author: "Henrique Pinto"
  - subject: "Re: ERP?"
    date: 2003-12-04
    body: "\"Latin Americans\" would encompass Mexico, Central America and the Caribbean islands and is the preferred term; as was pointed out \"South Americans\" isn't particularly precise -- though on the other hand \"North Americans\" often refers to Americans and Canadians (and as I recall it's the same in Spanish there).\n\nHowever the last two terms are a matter of some ambiguity, the former is not in English.  If you don't believe me, try referring to a Canadian as an American and see how far you get.  :-)"
    author: "Scott Wheeler"
  - subject: "Re: ERP?"
    date: 2003-12-09
    body: "Joining the nitpicking festival.\n\n\n<i>Latin Americans\" would encompass Mexico, Central America and the Caribbean islands</i>\n\n\nThen you would describe a Jamaican as Latin?\n\nIt's getting better and better.\n\n\n\nCheers,\n\ncarlos Cesar\n"
    author: "Carlos Cesar"
  - subject: "West Indian"
    date: 2003-12-15
    body: "Immigrants from Jamaica (and Barbados and Trinidad...) are called West Indians in the UK -- most British English speakers wouldn't think of them as any sort of American.  But of course in Indian English West Indian might refer to someone from Gujarat or Punjab.  Cubans are Latin but are they Latin *American*?\n\nPicking nits like this is contagious.\n"
    author: "AC"
  - subject: "Re: West Indian"
    date: 2003-12-15
    body: "<I>Cubans are Latin but are they Latin *American*?</I>\n\nThat's a toughie!   Kudos.\n\n\nCheers, \nCarlos Cesar"
    author: "Carlos Cesar"
  - subject: "Re: West Indian"
    date: 2007-02-08
    body: "asdas dasdas d"
    author: "asd"
  - subject: "Re: ERP?"
    date: 2005-09-03
    body: "NO HENRIQUE PINTO, MEXICANS ARE NOT SOUTH AMERICANS- NOT EVEN CLOSE.  SOUTH AMERICANS ARE THE CHILEANS, ARGENTINIANS, AND BRAZILIANS... NOT MEXICO"
    author: "NAT"
  - subject: "Qt Designer and Scalable Interfaces"
    date: 2003-12-03
    body: "\"With glade you layout interfaces, instead of painting them. You place widgets by creating container widgets like tables which hold the \"real\" widgets. The advantage of this method is scalable interfaces. What do interfaces consist of? Text, icons and widgets. If you use svg icons you have completely scalable interfaces with Gnome *now*. E.g. you rarely have to worry if text in less concise languages than English will fit on your widgets. Gtk will take care of it and dynamically resize the widgets at runtime. Users with bad eye-sight or running higher resolutions than normal can choose large systems fonts. Pixel-oriented interface design is primitive and a thing of the past.\"\n\nThis sounds very beneficial to me, is it really true that in QtDesigner a widget is not scalable automatically? It sounds like it is very outdated if this is true, in fact what are its benefits if this is true?"
    author: "Mario"
  - subject: "Re: Qt Designer and Scalable Interfaces"
    date: 2003-12-03
    body: "I need to run larger-than-usual fonts (high-res LCD) and so scalability is very important to me. KDE/Qt's is very good (but so its GTK+'s for that matter :) Certainly, its better than Windows XP, and (as I hear) OS X's."
    author: "Rayiner H."
  - subject: "Re: Qt Designer and Scalable Interfaces"
    date: 2003-12-03
    body: "It seems that this is nothing unique and Qt can do all of this too.\n\nHere are some more responses to the same question from another forum:\n\n\"Also, Qt comes with default widget spacing, avoiding\nhaving to specify it in \"a million places\" when coding\na UI.\n\n  When it comes to fitting non-Enlish text and i18n.\nHave a quick look at Qt Linquist. You do _not_ need to\nbe a programmer to be a Qt translator.\"\n\nand\n\n\"It seems to me what you describe looks pretty like the use of QLayout and derivatives...\nWhatever it looks like, Gtk *is* pixel-based. Have a look at classes : QVBox, QGrid, QLayout,\nQHBoxLayout, and so on... and of course you can use them in designer.\"\n\nand more responses here: http://dot.kde.org/1070400758/\n\nSorry, I double posted, I wanted to make sure someone answered."
    author: "Mario"
  - subject: "Re: Qt Designer and Scalable Interfaces"
    date: 2003-12-03
    body: "You posted your troll three times in three different threads.  Stop spamming this forum with your trolls, please.  I am getting complaints about you in my Inbox."
    author: "Navindra Umanee"
  - subject: "I'm not trolling, it's a adamn question!!!"
    date: 2003-12-05
    body: "I simply put up a quote from a comment I found at OSNEWS and asked if it was true, did not say it was or not. \n\nI really didn't know."
    author: "Mario"
  - subject: "Re: I'm not trolling, it's a adamn question!!!"
    date: 2003-12-05
    body: "Please stop posting the same post several times then."
    author: "Datschge"
  - subject: "Re: Qt Designer and Scalable Interfaces"
    date: 2003-12-04
    body: "> This sounds very beneficial to me, is it really true that in\n> QtDesigner a widget is not scalable automatically?\n\nLayout management is available in Qt Designer for ages."
    author: "Henrique Pinto"
  - subject: "Re: Qt Designer and Scalable Interfaces"
    date: 2003-12-04
    body: "This person obviously hasn't used Qt Designer. Scaleable layouts and widgets have been an integral part of Qt Designer for years. I have long slaved over scaleable UI issues in Visual Basic - it is an area where Microsoft has lagged behind. Strangely, Visual Studio now incorporates some UI development and layouts that are somewhat Qt-like.\n\nIs it me or are we seeing more trolls from people regarding Qt from seemingly GTK or Gnome oriented people? I know we see people trolling regarding GTK from a Qt perspective, but I'm starting to see some sort of reasonably targetted campaign to undermine Qt from many quarters. It could be targetted or it could be a lot of coincidental stuff because 'the other side' feels desperate, I don't know.\n\nAlthough I've used Glade for some GTK development and it is certainly good at what it does, it is no where near being as good as a combination of Qt Designer and KDevelop for developing on a more full-time basis. KDevelop may even end up be better at creating Gnome/GTK apps than anything Gnome based! Perhaps there may be some issues with cross-language support that can be addressed, but in terms of overall development structure KDE is streets ahead of anything.\n\nI've seen some stupid trolls over the past few weeks and held my tongue because I didn't want any stupid flame-wars to occur. This is just my penny's worth and people can agree and disagree privately as they please."
    author: "David"
---
You might know me as the original author of <a href="http://kopete.kde.org/">Kopete</a>, the instant messanger for KDE.  This week I have some news from Chile where after having reclaimed the <a href="http://www.kde.cl/">kde.cl domain</a> and having organized the site with the help of Matias Fernandez, hard work to promote KDE has begun. I was invited to talk about open source at a very important business conference organized by <a href="http://www.entel.cl/">Entel</a> -- perhaps the most important telecommunications company of Chile.  Read on for details about the event, including <a href="http://www.kde.cl/eventos/20031125-entel/">pictures</a>, and also news of a new GPL'ed Enterprise Resource Planner (ERP) dubbed <a href="http://zerp.inzignia.cl/">ZERP</a> with a KDE UI appropriately dubbed <a href="http://zerp.inzignia.cl/modules.php?name=coppermine&cat=2">KERP</a>!
<!--break-->
<p>
The Entel event took place on the 25th of November and was focussed on technology in today's business.  It consisted of an audience of top executives from big Chilean companies. The breakfast-conference included 3 presentations, the second of which was done by your very own Chilean representative!  
<p>
I talked about the open source community, open source culture, the international scene, and how enterprises could fit in the picture. Of course, I also talked about the KDE project (even showing an old picture of the core developers :-) and our role in this revolution. 
<p>
Pictures of the event are available:
<ol>
<li><a href="http://www.kde.cl/eventos/20031125-entel/talk.jpg">The Talk</a></li>
<li><a href="http://www.kde.cl/eventos/20031125-entel/panel.jpg">The Discussion Panel</a></li>
<li><a href="http://www.kde.cl/eventos/20031125-entel/breakfast.jpg">Oh, and the nice breakfast</a> ;-)</li>
</ol>
<p>
In other news, I was pleasantly surprised to be contacted by Raimundo Bilba of <a href="http://www.inzignia.cl/">Inzignia</a> who pointed me to a very advanced ERP project that is being developed through a business contract and is soon to be put in production.
<p>
The core, called <a href="http://zerp.inzignia.cl/">ZERP</a> already has 3 working modules, and the currently available UI, is called KERP and is KDE-based. You can see <a href="http://zerp.inzignia.cl/modules.php?name=coppermine">screenshots of it here</a>. 
<p>
Raimundo and I have agreed to join forces to promote both Kopete and KERP from <a href="http://www.kde.cl/">kde.cl</a> -- a great idea given the fact that <a href="http://kopete.kde.org/">Kopete</a> (which has matured a lot thanks to <A href="http://kopete.kde.org/index.php?page=developers">a very cool team</a> of developers) was originally started in Chile and is helping motivate a lot of other Chileans to start learning KDE/!t coding. The addition of KERP will undoubtably help to that effect.