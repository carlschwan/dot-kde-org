---
title: "Native Windows version of KOrganizer released as part of Aethera"
date:    2003-08-29
authors:
  - "kstaerk"
slug:    native-windows-version-korganizer-released-part-aethera
comments:
  - subject: "legal ?"
    date: 2003-08-29
    body: "is theKompany.com allowed to link a GPL program against the commercial Qt Windows release ?"
    author: "Dominique Devriese"
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "Yes, it is legal. KOrganizer is licensed under GPL+Qt Exception.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "\nNote that this makes the license incompatible with the license on other GPL software. This means the authors must be careful not to incorporate other GPL'd libraries without getting permission from the copyright holders.\n"
    author: "cbcbcb"
  - subject: "Re: legal ?"
    date: 2003-08-30
    body: "Not if the exception is an separate clause add-on/license to their GPL license. And anyway, nobody prevents anyone to actually negotiate other solutions not covered by the current license(s) with the actual copyright holders of any project's source code."
    author: "Datschge"
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "Yes, it's allowed to. Linking a GPL program against the commercial Qt is allowed and the resulting program will be GPL'd. The other way around is not allowed though, linking a commercial program against the Qt/GPL is not allowed if you want to create a commercial application.\n\nIn short:\nQt/GPL + GPL program = GPL binary\nQt/commercial + GPL program = GPL binary\nQt/GPL + commercial program = GPL binary (this situation is very rare)\nQt/commercial + commercial program = commercial binary (may be GPL as well)"
    author: "Arend jr."
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "Is there a more complete set of equations, where these came from?\nIt seems to be the most usable form of explaining the GPL intrinsics\nand might come in handy when trying to follow the SCO issue."
    author: "Pulo"
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "Not that I know of. It's just a simple list I came up with. The basic idea is that to use GPL code, the end result needs to be GPL as well."
    author: "Arend jr."
  - subject: "Re: legal ?"
    date: 2003-08-31
    body: "Note this is true of the kernel too. Those proprietary modules that everyone would like to see replaced with GPL alternatives (*cough*, *cough* NVIDIA!) aren't <i>using</i> GPLed code, they are <i>being used by</i> GPLed code (ie the kernel)."
    author: "Adrian Baugh"
  - subject: "Re: legal ?"
    date: 2003-09-06
    body: "Seems to me that all the equations follow the simple pattern:\n\n\tAnything + GPL = GPL\n\nCorrect me if I'm wrong."
    author: "Kavau"
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "Actually, it's my understanding that to link GPL'd code with non-GPL'd code, the latter must be released under a license that is at least GPL-compatible (and then as you say, the linked binary is GPL'd).  Isn't that right?\n\nQuoting the FSF:\n\n   Q: What does it mean to say a license is \"compatible with the GPL\"?\n    \n   A: It means that the other license and the GNU GPL are compatible; \n      you can combine code released under the other license with code \n      released under the GNU GPL in one larger program.\n\n      The GPL permits such a combination provided it is released under \n      the GNU GPL. The other license is compatible with the GPL if it \n      permits this too. \n\n"
    author: "LMCBoy"
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "That's right. In my equations \"commercial program\" has the requirement that its license is GPL-compatible. Anyway, for the Qt license, this still holds true."
    author: "Arend jr."
  - subject: "Re: legal ?"
    date: 2003-08-29
    body: "> Linking a GPL program against the commercial Qt is allowed and the resulting program will be GPL'd.\n\nThis is not true.  What gives you the right to unilaterally relicense the \"commercial\" Qt?  Remember that the resulting binary is considered to be a derivative work of the library and the application code."
    author: "Jim"
  - subject: "Re: legal ?"
    date: 2003-08-30
    body: "You're not relicensing commercial Qt. You're changing the license of the binary, which is different. "
    author: "Rayiner Hashem"
  - subject: "Re: legal ?"
    date: 2003-08-31
    body: "I anticipated this response, which is why I put \"Remember that the resulting binary is considered to be a derivative work of the library and the application code.\"\n"
    author: "Jim"
  - subject: "Free vs. Commercial"
    date: 2003-08-30
    body: "The wording really jumped out at me - calling proprietary software \"commercial\". As it turns out GPL software, notably Linux and GCC, is used for commercial purposes all around the globe. So please refrain from using the word commercial when what you mean is proprietary or non-free."
    author: "Ben Atkin"
  - subject: "Re: Free vs. Commercial"
    date: 2003-08-31
    body: "It is commercial, and thats what the author meant - those are the terms of the Qt license. If its a commercial product, Qt becomes proprietary. If its a non-commercial product it becomes GPL. The code is the same, but the licence depends on its use.\n\n\n"
    author: "Fred Fish"
  - subject: "Re: Free vs. Commercial"
    date: 2003-09-12
    body: "He mentioned \"commercial program\" too, which has nothing to do with Qt/Commercial."
    author: "ac"
  - subject: "Kolab compatibility?"
    date: 2003-08-29
    body: "Is this program compatible with the Kolab server? Or is compatibility with Kolab planned for the future?\nIt would provide a nice alternative to Outlook with a plugin on the Windows side."
    author: "Arend jr."
  - subject: "Re: Kolab compatibility? ."
    date: 2003-08-29
    body: "I seriously doubt that its compatible, but the should make it compatible. I don't see the added value otherwise. I think there really is a market for it on the windows platform, on linux there are to many competitors."
    author: "good idea"
  - subject: "Re: Kolab compatibility? ."
    date: 2003-08-29
    body: "Actually we are about 2 weeks away from having Aethera work with Kolab - at least that is our sense of it at the moment."
    author: "Shawn Gordon"
  - subject: "Re: Kolab compatibility? ."
    date: 2003-09-17
    body: "Well, It would be a great piece of software, if it works on windows with kolab.\nIt could really ban outlook and the f*****ing exchange!!!\nPlease reply where to get the final release thats compatible with kolab!\n\ncheers,\n\n\nflaz"
    author: "flaz"
  - subject: "Great!"
    date: 2003-08-29
    body: "That they are using Korganizer code in this. This is what open source is all about. I wish they had merged in the latest korganizer style fixes though. They de-uglyfy the korganizer events. "
    author: "anon"
  - subject: "Re: Great!"
    date: 2003-08-29
    body: "I think cvs korganizer has pretty events too - probably we'll see it ~3.2.  "
    author: "anon"
  - subject: "Konqueror port to Win32"
    date: 2003-08-29
    body: "Will they be porting Konqueror to Win32, Alan Gutierrez' project seems to have stalled: http://khtml-win32.sourceforge.net/"
    author: "bobbob"
  - subject: "It's not worth it"
    date: 2003-08-29
    body: "It's not worth porting to win32, Konqueror's main advantage is its great integration with KDE and the file system. \n\nAs a web browser alone Firebird 0.7, Internet Explorer or Mozilla 1.4 are all better. \n\nIMO, that's a waste of time."
    author: "Some Dude"
  - subject: "Re: It's not worth it"
    date: 2003-08-29
    body: "Actually, it is worth it...  Safari is an example of that.  There are also a few cross-platform applications, like the Eclipse IDE that need a cross-platform HTML engine in order to make the application behave consistently on all platforms.  Right now there aren't many choices besides Gecko... adding khtml on Windows would be an improvement."
    author: "Another Dude"
  - subject: "Re: It's not worth it"
    date: 2003-08-31
    body: ">>Actually [porting konq] is worth it... Safari is an example of that. \n\nKonq != KHTML, KHTML is a part of konq.\nSafari uses a port of KHTML.\n\nPorting KHTML to win32 seems more useful/ easy to me than porting Konq.\n\nAnyway in this context -- with HTML emails in mind -- having a win32 port KHTML would be nice.\n\nCheers,\nCies.\n\n"
    author: "cies"
  - subject: "Re: It's not worth it"
    date: 2003-09-04
    body: "the Eclipse project has chosen to go with mozilla for their embedded web browser. "
    author: "Mark Lybarger"
  - subject: "Re: It's not worth it"
    date: 2003-09-04
    body: "And they've decided to use khtml (with webkit) under OSX\n\nhttps://bugs.eclipse.org/bugs/show_bug.cgi?id=41887"
    author: "anonymous abuser"
  - subject: "Re: It's not worth it"
    date: 2003-11-02
    body: "hello? havnt you seen the windows file manager lately?\nor ever? and realized how bad it sucks compared to using\nkonqueror?\nI'd kill to have konqueror for XP for that alone...\nIve only seen one program even come close to tabbing\nexplorer at all which is tabexplorer but it's a trial\nand is pretty featureless...."
    author: "Nick Borrego"
  - subject: "Re: It's not worth it"
    date: 2004-03-06
    body: "atm it is terrible for a win32-webdesigner to test his sites in KHTML (safari & konq) ..\nand the easier it gets to test it on that renderer... the more pages will get compatible with KHTML . Most webpages get made on win32 anyway"
    author: "Tom"
  - subject: "Re: Konqueror port to Win32"
    date: 2004-11-30
    body: "i my self am stuck to using xp untill i can afford a new motherboard or a new wireless card (my ethernet was blown up by lightning and it no longer works) enough about my problems \n\ni have used knoppix and other linux varients (knoppix being my favorite)\n\nand i prefer konqueror 10 times to firefox (win or linux) as a file manager and a web browser. \n\n\ni like the fact that you can move to any area of the computer and open up the console using knoq and be in the same file path as knoq. \n\nthis is one of its main advantages that i like, plus the fact that its much cleaner. \n\nso i look forward to a win32 port of it. \n\n\n"
    author: "pureone_36"
  - subject: "Re: Konqueror port to Win32"
    date: 2005-03-17
    body: "You can easily add a \"Command Prompt Here\" feature to Windows Explorer, see this page, or just merge the attached registry file:\n\nhttp://windows.about.com/od/tipsarchive/l/bltip177.htm"
    author: "John K. Hohm"
  - subject: "Re: Konqueror port to Win32"
    date: 2006-01-05
    body: "After over a year, please do a quick search for 'KDE Win32'.  You'll notice a lot of work has been done, specifically with Q.-3/Win32, to make porting KDE and KDE apps much easier.\n\nPresently, I'm attempting to build kdelibs and the kio-slaves for Win32.  After that, I'll work on the kfm/kio-client/exec, then Konqueror and the Panel.  Once all that's in place and working, you can literally chuck explorer out the window!\n\nKeep in mind that it's a lot more work than you think; Everything will have to be modified to work from the registry rather than various *nix configuration directories.\n\nStill, being able to chuck out Explorer entirely would be an awesome thing."
    author: "Fordi"
  - subject: "What about gpg"
    date: 2003-08-29
    body: "Anyone knows if it supports gpg\nAnd what about syncing with e.g. a Zaurus?\n\n"
    author: "Moppel"
  - subject: "Re: What about gpg"
    date: 2003-08-29
    body: "gpg is being worked on, as is sync for the zaurus.  We are just finishing the changes to our PIM data structures and then we can write the sync conduits."
    author: "Shawn Gordon"
  - subject: "Nothing to do with embedded KOrganizer"
    date: 2003-08-29
    body: "Actually this has nothing to do with the embedded version of KOrganizer, we've had this technology in place since before that port ever took place, we've been embedding KOrganizer for years now, and we are using our Korelib and TINO technologies to do it.  The mini-kde that was developed for KOrganizer that allowed it port to embedded was originally developed specifically so it would work in Aethera in a multi-platform environment."
    author: "Shawn Gordon"
  - subject: "I don't really know"
    date: 2003-08-29
    body: "But, didn't KDE have a PIM suite that integrated Korganizer, Kmail etc. into one? If so, wasn't it called Kontact and not Aetherea?\n\nWhat's really the difference?"
    author: "Some Guy"
  - subject: "NM"
    date: 2003-08-29
    body: "This seems quite more ambitious than Kontact, it has messenging and notes and weather etc. It has even more tools tahn Evolution, but I don't really know if it's better, anyway it seems cool."
    author: "Some Guy"
  - subject: "Re: NM"
    date: 2003-08-29
    body: "Kontact does have notes and weather. The only thing missing at the moment is messaging. That is because kopete wasn't ready for inclusion until now. I'm pretty sure after kopete is move out of kdenonbeta, there will be an integration of it into kontact."
    author: "Christian Loose"
  - subject: "Re: NM"
    date: 2003-08-30
    body: "Yeap, seems cool.\nStill one friend of mine tried it under windows and he promptly complained about two things. The lack of an import filter for his Outlook mail folders and the fact that, when he entered the wrong password to get mail, it simply told him connection refused instead of asking him for another password.\n\nSo, yeap it is nice, but still not good enough (which is of course fine since it is a 1.0). I hope it gets better real fast!! :)"
    author: "Sleepless"
  - subject: "opengroupware compatibility?"
    date: 2003-08-31
    body: "is this compatible with opengroupware.org's groupware server?"
    author: "mark pinto"
  - subject: "Re: opengroupware compatibility?"
    date: 2003-09-01
    body: "It doesn't have support for OGo yet.\nI am trying to test it and to check what has to be changed in Aethera.\n"
    author: "Eug"
  - subject: "Re: opengroupware compatibility?"
    date: 2004-02-11
    body: "aethra arrival to OGo would be a great change in the groupware world if you did it successfully.\n\nPlease proceed your work and bring success to Opensource community."
    author: "Siva Karthikeyan"
  - subject: "Well what does 1.0 Really Mean??"
    date: 2003-09-01
    body: "I bought Kapital - their personial finical manager for a discount price three years ago as part of the beta test program.  Knowing that it was beta it was OK to have bugs, actually it was more then OK, it was expected!!\n\n~Two years ago Kapital went 1.0 - let me just tell you that this is the biggest piece of crap I have ever seen!!  We are currently waiting for 1.1 to be released, but 1.1beta2 was released ~6 months ago, and this '1.0', and '1.1' both contain many many bugs that plagued the betas, and both are still completely unusable.  Unuseable unless you can with stand crashed and data loss that is.  \n\nI think that the idea behind theKompany is great for Linux, but I think that they have bit off more then they can chew.  They seem to not have the resources to be developing not only Athera and Kapital, but a couple dozen apps for Win32, Linux, and Qtopia handhelds.\n\nLuckly, Athera is a Free application, so you can actually run it and see how bad it is without having to envest in theKompany 1st.  If we not free I for one would not give theKompany any more of my hard earned $$$\n\n\nJust my $0.02"
    author: "Annoyed"
  - subject: "Re: Well what does 1.0 Really Mean??"
    date: 2003-09-01
    body: "I'm sorry to hear you've had trouble with Kapital, yes it's taken longer to get it where we wanted, than we wanted, but we actually have a very large customer base using the software very successfully, some people have trouble, this is true, more often than not it has to do with compatiblity problems between distros and versions of distros.  I would seriously contest your \"crap\" comment, I use the software myself without any problems.\n\nA problem for us internally with regard to Kapital has been turnover with the developers working on it, unlike a great majority of our products, it really shares no code with anything else, and also the concept of a check book is alien to most of our developers, this has caused various gaps in the development of the application, but it is not abandoned.  Right now there is a bug relating to transfering a split transaction between categories that we are working to resolve, and that's the last major issue we are aware of."
    author: "Shawn Gordon"
  - subject: "LDAP???"
    date: 2003-09-01
    body: "Hi,\n\nLDAP support would be nice too"
    author: "Emmanuel"
  - subject: "crashes"
    date: 2003-09-01
    body: "It crashes under windows when exiting, imap config options poor. tkJabber is listed as available plugin, but does not work. This software is not release quality. "
    author: "dfszb"
  - subject: "Re: crashes"
    date: 2003-09-01
    body: "the free version of tkJabber is not a plug in.  Did you buy the commercial version?  that is the plug in.\n\nI'm running Aethera on WinXP every day with IMAP with no trouble, some details reported to our online tracking system would be helpful."
    author: "Shawn Gordon"
  - subject: "Re: crashes"
    date: 2003-09-01
    body: "I am using it myself on WinMe without problems.\nI have tested Aethera ONLY with 'courier' and 'imap' IMAP servers and this is pointed out in the web pages, what server are you using ?\nThe missing features like \"imap config options poor\" has nothing to do with the release version (especialy with 1.0).\nEx: see the history for the most used email clients and you will see when they were able to manage IMAP right.\n\nThe available plugin list means 'those plugins are available on the net or on your local system but they aren't used by Aethera'.\nYou have to install the jabber plugin (get the rpm or the Win installer from us) then you have to add it into the 'Plugins' list using the yellow arrow button.\nAnyway, some info you can get just reading the help files.\n\nI cannot comment your final words, it's your opinion.\nI can only see what other email app were offering for 1.0\n"
    author: "Eug"
  - subject: "1.0? This is very broken."
    date: 2003-09-01
    body: "This is nowhere near production ready, and calling it so just gives open source a bad name.\n\nWindows versions\n\n1. Default setup path under Windows is broken. If you use the default then nothing works.\n2. IMAP messages all appear as 1k, null subject, null date.\n3. Clicking on a link from an rss feed loads explorer, rather than the default html viewer (mozilla in my case)\n4. Viewing a text email after an html email renders as if it were a blank message.\n\nWindows + Linux versions:\n\n5. IMAP/POP3 server settings need to be stored in the settings dialog box and saved else mail won't be downloaded (or accesible in the case of IMAP)\n6. Icons/graphics bob around all over the place when switching between screens.\n\nSome features which are essential in a product like this which are missing:\nLDAP address book support\nSync with Palm/WinCE/Zaurus devices"
    author: "Andrew"
  - subject: "Re: 1.0? This is very broken."
    date: 2003-09-01
    body: "1. It will be fixed in 1.1\n2. What IMAP server are you using ?\n3. It is asking your system (via your file manager, explorer) to open the link.\n4. It is using QT widget for viewing HTML data and it's not as good as the KDE one. Also it is not loading anything you don't want and this is good from a security point of view. I am trying to make it better for the next releases.\n5. How do you store things ? I am saving it. May be I didn't get your problem.\n6. This should happen only first time when you are using the plugin, again if I get right your problem.\n\nYes, more things has to be done for your needs (and my too).\nBut why should be all together in 1.0 release ??"
    author: "Eug"
  - subject: "Re: 1.0? This is very broken."
    date: 2003-09-01
    body: "1. OK!\n2. bincimap on Linux\n3. Then something is wrong. If I enter www.kde.org into the Start->Run... dialog it starts Mozilla with the kde website. This should be default behaviour, not to run IE.\n4. Maybe I wrote the original message incorrectly, but this is the pattern:\ni. View html message -> Message renders ok. Next,\nii. View text message -> Message renders blank. Next,\niii. View html message -> Message renders ok.\n5. If I don't store the password in the setup dialog then no mails are ever retrieved. I'm never asked for the password for the mail server. This is the same using POP3 or IMAP.\n6. Yes, you are right.\n\nHope this helps to develop a promising app.\n\nCheers,\n\nA\n"
    author: "Andrew"
  - subject: "Re: 1.0? This is very broken."
    date: 2003-09-02
    body: "Ok, I have got all your points.\n2) I am installing bincimap on my system, so it should work in the next release.\n4) and 5) will be fixed in the next release.\n(I cannot understand why 5 isn't working, it should show a password dialog first time you are downloading emails.)\n"
    author: "Eug"
  - subject: "Re: 1.0? This is very broken."
    date: 2003-09-03
    body: "2) FYI I have tried with bincimap 1.5 and 2.0 on a fairly standard Mandrake 9 install. Aethera Linux works fine, Aethera Windows is broken.\n\nI shall head over to the Aethera sf site to help in debugging. Can't offer much help in coding as I've not written any QT/KDE stuff.\n\nA"
    author: "Andrew"
  - subject: "Re: IMAP / WIn32 Setting"
    date: 2004-05-22
    body: "I can confirm Andrews problem regarding IMAP not working with version 1.0.2 on win2000 / Courier IMAP unless you enter the password and tick the save password box when creating the account.\n\nAs far as I can tell there is no network activity otherwise and definitely no pop up dialogue box.  No errors , no nothing!\n\nOnce I have saved the settings it works fine and I will be giving it a thorough test over coming weeks.\n\nLooks very promising though!!"
    author: "Alec"
---
<a href="http://korganizer.kde.org/">korganizer.kde.org</a> reports that <a href="http://www.thekompany.com/home/">theKompany.com</a> recently has released the 1.0 version of their <a href="http://www.thekompany.com/projects/aethera/">cross-platform PIM suite Aethera</a>. This includes <a href="http://korganizer.kde.org/">KOrganizer</a> as calendar and todo list component. The KOrganizer component (<a href="http://www.thekompany.com/projects/aethera/images/aethera_6.png">screenshot</a>) is based on the original unmodified KOrganizer source code and has been ported to Windows using a similar technology as was used for porting <a href="http://korganizer.kde.org/korganizere/korge.html">KOrganizer/Embedded</a> to the <a href="http://www.trolltech.com/products/qtopia/index.html">Qtopia</a> 
environment. This once again is a great proof of the power and flexibility of open source development and KDE technology.
<!--break-->
