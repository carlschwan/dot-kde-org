---
title: "KDE-CVS-Digest for January 3, 2003"
date:    2003-01-03
authors:
  - "dkite"
slug:    kde-cvs-digest-january-3-2003
comments:
  - subject: "nice"
    date: 2003-01-03
    body: "Wow, nice job.  I like the new stylesheet :-)  Thanks for the updates!"
    author: "not me"
  - subject: "sorry"
    date: 2003-01-03
    body: "Sorry, but... it's only getting better and better! :-)"
    author: "anon"
  - subject: "Great work!"
    date: 2003-01-03
    body: "Keep up the good work Derek! ;)"
    author: "cartman"
  - subject: "Re: Great work!"
    date: 2003-01-03
    body: "Absolutly. Great work!\n"
    author: "Marlo"
  - subject: ";)"
    date: 2003-01-03
    body: "Nicely done. It is getting better with every release. Thanks for news."
    author: "stromek"
  - subject: "great!"
    date: 2003-01-03
    body: "I'm really happy to see the KDE-CVS-Digest serie come back each week! I hope you can find people to help you so that it doesn't depend on too few people (It's sad the KDE Weekly News is not as regularly published as the cvs digest eg)\n\nCongrats for your great work!"
    author: "raphinou"
  - subject: "Re: great!"
    date: 2003-01-03
    body: "If you notice, there is very little original material. All I do is quote others. So the time required is small. I've had time this last while to work on some scripts that put most of it together.\n\nMatching the backports and the emails from developers require hand editing.`\n\nWhen I started this, I felt that the important \"feature\" would be doing it weekly. I've learnt to have the utmost respect for Zack Brown.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: great!"
    date: 2003-01-04
    body: "So afaics you just take a selection of well chosen quotes to summarize the activities without any fuss like additional comments and interpretations. I say this is the way to go, and I'm very happy you're doing as well as that it's being received that well. ^_^\n\nIn short: truely objective journalism at its best. ;)"
    author: "Datschge"
  - subject: "Re: great!"
    date: 2003-01-04
    body: "> without any fuss like additional comments and interpretations\n\nHah! I spare you all much pain.\n\nA couple of times I got pontificating, then hit the most important key, Delete. Managed to come out with my self-respect intact.\n\nNow if I could only spell everybody's name properly. Grrrr.\n\nDerek"
    author: "Derek Kite"
  - subject: "\"libkhtml went down from 7248 to 3271 PLT\""
    date: 2003-01-03
    body: "\n\nThis sounds very nice and interesting, optimisations are always a good thing :-)\n\nmy question is:\n\nis it in HEAD or in KDE_3_1_BRANCH ???\n\n\n"
    author: "ac"
  - subject: "Re: \"libkhtml went down from 7248 to 3271 PLT\""
    date: 2003-01-03
    body: ">is it in HEAD or in KDE_3_1_BRANCH ???\n\nAccording to kde-cvs, it's HEAD stuff."
    author: "kissakala"
  - subject: "Re: \"libkhtml went down from 7248 to 3271 PLT\""
    date: 2003-01-03
    body: "\n>>is it in HEAD or in KDE_3_1_BRANCH ???\n\n>According to kde-cvs, it's HEAD stuff.\n\n\n:-((\n\nwell a backport for the upcoming KDE3.1 would be very nice, but I guess\nthis is too intrusive to force into a release..."
    author: "zambazumba"
  - subject: "Re: \"libkhtml went down from 7248 to 3271 PLT\""
    date: 2003-01-04
    body: "Hi!\n\nI'm using both windows xp and linux on an Athlon XP 2000. I've always found KDE to be *MUCH* slower than Windows and didn't really have any hopes of that ever getting better. Gnome programs were only a little slower than windows.\n\nGuess what: I've installed gentoo, defined my compiler-settings (processor type) in /etc/make.conf, said \"emerge kde\" and went to bed. From the next day on, I had the fastest KDE system I have *ever* seen.\n\nGentoo rocks, KDE rocks. Thanks both!"
    author: "me"
  - subject: "Re: \"libkhtml went down from 7248 to 3271 PLT\""
    date: 2003-01-04
    body: "This sounds like you missed http://www.gentoo.org/doc/en/prelink-howto.xml."
    author: "Anonymous"
  - subject: "Re: \"libkhtml went down from 7248 to 3271 PLT\""
    date: 2003-01-04
    body: "Prelink doesn't really help in this case, unfortunately, as Konqueror doesn't link to khtml - it dlopen()'s it. \n"
    author: "Sad Eagle"
  - subject: "Re: \"libkhtml went down from 7248 to 3271 PLT\""
    date: 2003-01-06
    body: ">Prelink doesn't really help in this case, unfortunately, as Konqueror doesn't \n>link to khtml - it dlopen()'s it.\n\n\nmmmm I wonder if this could not be changed easily...\nthe performance gain of prelink is very nice and important.\n\nMaybe one could also get rid of the kdeinit-hack at this moment?\n"
    author: "ac"
  - subject: "BAD RUMOR"
    date: 2003-01-04
    body: "HI I HEARD KDE IS GOING OUT OF BUSINESS I HOPE THIS IS NOT TRUE HELP ME"
    author: "Bob"
  - subject: "Re: BAD RUMOR"
    date: 2003-01-04
    body: "HI BOB NO THIS IS NOT TRUE KDE WILL NOT GO OUT OF BUSINESS ANYTIME SOON AND NEXT TIME COULD U TRY NOT 2 TYPE WITH CAPITAL LETTERS PLZ THX"
    author: "frerich"
  - subject: "What's missing / krdc"
    date: 2003-01-04
    body: "In the last week Remote Desktop Connection (kdenetwork/krdc) got initial support for RDP, the protocol used by the Windows Terminal Services. Right now it can act as a simple frontend for the rdesktop executable (done by Arend van Beelen jr.). A native RDP backend is in the works. krdc has also been re-structured to support multiple backends."
    author: "Tim Jansen"
  - subject: "Re: What's missing / krdc"
    date: 2003-01-05
    body: "You ROCK!  Will you have my babies?\n\nThat is all.\n\nThank you."
    author: "My Left Foot"
  - subject: "The Kapers"
    date: 2003-01-04
    body: "Hey, Ron Kaper, is that Rob's bro'? ;)\n\nReminds me of a Judas Priest single I had that credited \"Bob Halford\" as the singer.\n\nAnyway, great stuff."
    author: "ac"
  - subject: "Re: The Kapers"
    date: 2003-01-04
    body: "Grrrr! \n\nFixed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: The Kapers"
    date: 2008-09-01
    body: " Hi solo parasaber mas infrmacion sobre jesus"
    author: "ISMAEL"
---
The latest <a href="http://members.shaw.ca/dkite/jan32003.html">KDE-CVS-Digest is out</a>. This week read about some new KDE optimizations, <a href="http://konsole.kde.org/konstruct/">Konstruct</a>, <A HREF="http://www.unixcode.org/atlantik/">Atlantik</A> (<a href="http://unixcode.org/img/atlantik/atlantik-20021219.jpg">screenshot</a>) and <A HREF="http://edu.kde.org/kalzium">Kalzium</A> updates, as well as many bugfixes and various new features.
<!--break-->
