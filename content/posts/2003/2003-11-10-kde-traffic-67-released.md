---
title: "KDE Traffic #67 Released"
date:    2003-11-10
authors:
  - "hpinto"
slug:    kde-traffic-67-released
comments:
  - subject: "dIMAP"
    date: 2003-11-09
    body: "66469 Disconnected IMAP tries to use the prefix as another mailbox\n\nI believe Evolution has a similar bug ... perhaps an IMAP server issue??\n\nAny libs in common with Evo??"
    author: "qtnice_but"
  - subject: "Re: dIMAP"
    date: 2003-11-09
    body: "... and dIMAP seems to be really slow compared to standard IMAP.\n\nSyncing an Exchange2k-IMAP account with ~3000 mails needs 60 seconds. Checking a cyrus account for new mails with 50000 mails via standard IMAP without syncing needs a second.\n   \n*me* wonders, that I can use kmail filter rules with dIMAP. Hmmm, is it possible to build cyclic rules with 2 running clients using the same IMAP account?\n\nWhats the status of server side filtering (Sieve)?\n\nBye\n\n  Thorsten\n\n "
    author: "Thorsten Schnebeck"
  - subject: "Re: dIMAP"
    date: 2003-11-10
    body: "dIMAP is pretty much guaranteed to be slower as i understand it.. .as for Sieve filtering there's an option for it in KMail in CVS... i haven't tried it yet, thought..."
    author: "Aaron J. Seigo"
  - subject: "Re: dIMAP"
    date: 2003-11-10
    body: "Support for Sieve didn't make it into KDE 3.2. See \nhttp://developer.kde.org/development-versions/kde-3.3-features.html"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: dIMAP"
    date: 2003-11-10
    body: "How is then\n\n\"Imap folders as targets for filter move actions. Till Adam <till@adam-lilienthal.de>, Don Sanders <sanders@kde.org>\nClient side imap filtering with body downloading avoidance. Don Sanders <sanders@kde.org>, Till Adam <till@adam-lilienthal.de>\"\n\nfrom 3.2 feature plan implemented?\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: dIMAP"
    date: 2003-11-10
    body: "Filter rules are only applied on new messages in your INBOX on the IMAP server. Therefore cyclic rules should be impossible unless another (not KMail) client puts messages back into INBOX.\n\nAnyway, the possibility of cyclic filter rules is one of the reasons why clientside filtering with IMAP should be avoided if possible.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Layout of thumbnail-enabled iconviews in Konqueror"
    date: 2003-11-09
    body: "I think it is worth waiting a little bit before committing the patch as there are still some small problems. I will improve the patch when I have some time and if everything works perfectly, it will be committed (but after 3.2)..."
    author: "Benoit W."
  - subject: "Re: Layout of thumbnail-enabled iconviews in Konqueror"
    date: 2003-11-09
    body: "I think, this patch is more a bugfix than a feature.\nIt sould be committed now, otherwise people will use Nautilus."
    author: "Asdex"
  - subject: "Re: Layout of thumbnail-enabled iconviews in Konqueror"
    date: 2003-11-10
    body: "Asdex, you should read Coolo's Blog http://www.kdedevelopers.org/node/view/228\nto understand the difference between a bug and a showstopper\n\nBeno\u00eet should commit when he thinks it is technically ready in true Open Source way...\n\nThere is a life after 3.2... 3.2.1, 3.2.2"
    author: "Charles de Miramon"
  - subject: "Re: Layout of thumbnail-enabled iconviews in Konqueror"
    date: 2003-11-10
    body: "The only thing I ever get from kdedevelopers.org is timeouts on port 80."
    author: "Leonscape"
  - subject: "Re: Layout of thumbnail-enabled iconviews in Konqueror"
    date: 2003-11-10
    body: "Its working now, had too disable ecn checking to get it to work though. Can something be done about this? or is their some other problem?"
    author: "leonscape"
  - subject: "Re: Layout of thumbnail-enabled iconviews in Konqueror"
    date: 2003-11-10
    body: "Not commiting it right now has nothing to do with the feature freeze; in fact, it is a bug fix. It's not being committed right now because there are some small problems with the patch. Even if it doesn't make it into 3.2.0, it could be backported to 3.2.1, as it's a bug fix."
    author: "anon"
  - subject: "Icons"
    date: 2003-11-09
    body: "I don't think those new icons have too many details.\n\nBut there is another problem: Some icons differ from those of any other desktop:\nundo - redu: Both crystal icons are clockwise.\nAt every other desktop \"undo\" is counter clockwise, \"redo\" is a clockwise arrow. http://bugs.kde.org/show_bug.cgi?id=64199"
    author: "Asdex"
  - subject: "linking"
    date: 2003-11-10
    body: "and again, the same article is linked twice within the news item (which is only two sentences). bad style :)"
    author: "me"
  - subject: "Re: linking"
    date: 2003-11-10
    body: "Sorry for that.\n\nAnyway, the two links point to different places. The first one points to issue number 67. The second points to the latest issue, which is #67 now, but won't be when the next issue is released. That's why it is \"the usual place\"."
    author: "Henrique Pinto"
  - subject: "KDE Traffice Lives!"
    date: 2003-11-10
    body: "great issue... thanks for picking this up and putting effort into it... it's a very nice addition/contribution to the KDE community =)\n\none question: where are the archive links? will you be adding them in future editions? i know they are something of a pain, but they take only a few minutes to do and are really quite handy..."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE Traffice Lives!"
    date: 2003-11-10
    body: "Thank you very much!\n\nThe archive links are likely to return in the next issue. I completely forgot about them and no one else remembered, also."
    author: "Henrique Pinto"
  - subject: "Re: KDE Traffice Lives!"
    date: 2003-11-11
    body: "Cool about the archive links returning - another small request - could\nyou include the name of the mailing list before each summary?\nThanks for your efforts!"
    author: "steve"
  - subject: "Re: KDE Traffice Lives!"
    date: 2003-11-11
    body: "> could you include the name of the mailing list before each summary?\n\nCertainly. This will happen from now on.\n\nThank you!\n"
    author: "Henrique Pinto"
  - subject: "Glad to sea link to KDE-SUPPORT"
    date: 2003-11-10
    body: "This is an important step in remedying this problem.\n\nhttp://bugs.kde.org/show_bug.cgi?id=63868 "
    author: "Alex"
  - subject: "Re: Glad to sea link to KDE-SUPPORT"
    date: 2003-11-10
    body: "The motivation for including the link came from that bugreport.\n\nBTW, you may consider (if you haven't done that yet) joining the kde-promo mailing list and discuss these issues there."
    author: "Henrique Pinto"
  - subject: "KDE SVG Icons"
    date: 2003-11-10
    body: "So these are still PNG rendered from SVG sources?  \nI was to understand that these SVG icons were going \nto be rendered on-the-fly.  I wanted to have 240X240 \ndesktop icons too :(\n\nIn any case, welcome back KDE traffic :)"
    author: "standsolid"
  - subject: "Yearh!"
    date: 2003-11-10
    body: "Thanks for bringing it back and thanks to Russell Miller for doing the work until now!"
    author: "Joergen Ramskov"
---
After a not-so-long break, KDE Traffic is back. <A href="http://kt.zork.net/kde/kde20031109_67.html">Issue #67</A> has just been released, with news regarding <a href="http://kmail.kde.org/">KMail</a>, <a href="http://www.kontact.org/">Kontact</a>, general look and feel and more. Check it out at <A href="http://kt.zork.net/kde/latest.html">the usual place</A>.
<!--break-->
