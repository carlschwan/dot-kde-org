---
title: "KDE Traffic #62 is Out"
date:    2003-08-29
authors:
  - "numanee"
slug:    kde-traffic-62-out
comments:
  - subject: "thanks!"
    date: 2003-08-29
    body: "I pretty much agree that GCONF most likely sin't needed and neither is a patch to remove waldo's helpful titles. In fact, such an option would be what the GNOME camp is reffering when they say KDE has too many options. \n\nSuch an option is simply not needed. It's ok to have a lot of options, but if they are well thought out and meaningful. \n"
    author: "Some Guy"
  - subject: "Re: thanks!"
    date: 2003-08-29
    body: "Hello,\n\nI agree that gconf is not needed. After all, config should be like config was always in Unix, in files. That way it remains truly accessible for users. The rollback stuff sounds great. I would love that.\n\nPlease explain to me why the choice in case of menus it is not needed. I think the problem is not a camp complaining. And it's not too many options, but the user interface to them. KDE differs from Gnome. One way it does is to provide all config that the users want to change. I want that config option.\n\nI would prefer a fully customizable RMB menu as well. I don't need many things currently there and on the other hand would add others. It should potentially be more context aware as well. If my (visible) toolbar has back button, does the RMB need to have one? If the toolbar button is not visible, shouldn't it have one?\n\nKDE is about user choice. Leave it it that way.\n\nI welcome all usablity work on KDE 3.2, but please don't confuse usability with restrictedness. Usability is about good defaults, good dialogs to change them and good hierachy in information presentation.\n\nMost of all, I hope you people finish KDE 3.2 soon. 3.1.3 feels old when I read all the new stuff :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "Why do you assume that gconf doesn't use files? All gconf settings are stored in text files, in XML format with subdirectories. The files are very readable by both humans (good for manual editing) and software (good for writing config apps)."
    author: "Yama"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "Files are one option. Database, registry, etc. are others.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "> The files are very readable by both humans (good for manual editing)\n\nIf you consider XML a very good format to be readable by humans, that is. I personally think .ini files are much more readable. XML does have other advantages however. \n\nBut yes, the biggest myth surrounding gconf is that it uses a flat file for all of it's settings. I thought this for the longest time. In fact works just like kconfig, but uses XML files instead of .ini files. Kconfig and gconf are largely similiar and one should not replace the other in terms in the other desktop.\n\nWhat I still don't agree is all of gnome2's removal of useful config functions in the config dialog gui's, and expecting users to use gconf tools to edit them. I refer this to as \"gconf abuse\". Hopefully, with the development of kconfedit, the same thing doesn't happen with KDE."
    author: "fault"
  - subject: "Re: thanks!"
    date: 2003-09-04
    body: "The problem with ini is that it can't handle multi-line strings."
    author: "Ac"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "It's actually quite a mess if you look at it and try to understand it.  It's full of meaningless either empty or unstructured %gconf.xml files.  What's the % for?  Why are the XML files all of the same name?  Ugh."
    author: "ac"
  - subject: "Re: thanks!"
    date: 2003-08-29
    body: "> I pretty much agree that GCONF most likely sin't needed and neither is a patch to remove waldo's helpful titles. In fact, such an option would be what the GNOME camp is reffering when they say KDE has too many options. \n\nYeah, and those titles are the reason that my K-Menu now has two columns on my 800x600 display (laptop).\n\nThanks a bunch :("
    author: "Carsten Pfeiffer"
  - subject: "ugh uh"
    date: 2003-08-30
    body: "What does it matter? They don't take up so much space, don't blame them just because you were at the limit anyway! Maybe you should nest a few things.\n\nAnyway, it's bad usabiltiy to have an option for every POS that someone could potentially want changed. it needs to be an option taht more than 20 people in KDE community will use, or else it's just bloat and makes it harder for the user and developer. \n\nBut, that's just me, I for example also don't like it when I get 15 choices of bread I've enver heard of for a sandwich and I ama ske dto pick one. than i need to see ach one and try each one to know, because I've never heard of any of tehm before. After I've picke dthe rbead than i pick from 15 styles of cheese etc. \n\n"
    author: "Right..."
  - subject: "Re: ugh uh"
    date: 2003-08-30
    body: "It is at least 20 I am sure.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: ugh uh"
    date: 2003-08-30
    body: ">What does it matter? They don't take up so much space, don't blame them just because you were at the limit anyway! Maybe you should nest a few things.\n \nEh? We're talking about the default installation of KDE on 800x600 displays, where the K-Menu is just awful to use, now."
    author: "Carsten Pfeiffer"
  - subject: "Re: ugh uh"
    date: 2003-08-30
    body: "Did you talk with Waldo about it yet? I don't use 800x600 but if K-Menu now really\nshows two columns then I'm sure Waldo will re-think his decision to not include a configuration option.\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: ugh uh"
    date: 2003-08-30
    body: "I think a better approach is to remove some of the application catagories in the kmenu. See the thread on kde-usability here: http://lists.kde.org/?t=106158645200001&r=1&w=2 ."
    author: "fault"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "I agree please no Windows Registry on KDE."
    author: "Snowball"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "You obviously have no idea of what gconf is. Gconf is an XML-based (text) format. It is easy to hand-edit or to create a config app for it (GNOME has both GUI and console tools to do that). It isn't radically different from the KDE system, but it is far more structured and consistent."
    author: "Yama"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "> far more structured and consistent.\n\nbecause unlike, kconfig, most users will see gconf tools sometime in their time using their desktop. Kconfig, on the other hand, will probably only ever be used by system admins since most options are in configuration dialogs. Of course, some options in some of KDE's app's configuration dialogs are somewhat less useful, but I strongly urge KDE developers to never castrate their configuration dialogs like gnome2 =]"
    author: "fault"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "<?xml version=\"1.0\"?>\n<gconf><entry name=\"default\" mtime=\"1059631405\" muser=\"ac\" type=\"string\"><stringvalue>/usr//bin/metacity</stringvalue></entry></gconf>\n\nHow is this structured or consistent compared to KDE's ini files?  It's a right mess is what it is."
    author: "ac"
  - subject: "Re: thanks!"
    date: 2003-08-30
    body: "I think having an element named <stringvalue> says quite a bit about the design, unfortunately (\"entry\" is not much better).\n\nBut configuration info is hard to structure - there are various forms of information (simple values, rules, logic...) and ways of sharing it (All Users, this user, this user on this machine...). Representation as XML vs. .ini file is really a minor detail - namespaces and hierarchies are probably the key things to get right.\n\n"
    author: "Ulrich Kaufmann"
  - subject: "Quick tab access on konqueror"
    date: 2003-08-29
    body: "The important point I think is to standardize tab access in all KDE apps.  The same shortcut should be used for this in all the MDI apps of KDE (konqueror, konsole, kate, etc.).\n\nA suggestion : The behavior when changing tabs should be the same as the one when changing windows with ALT-TAB.  For example, if the shortcut for changing tabs is ALT-CAPS then pressing it one time should put the focus on the previous tab and pressing it a second time should bring the focus back to the first tab."
    author: "Ned Flanders"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-29
    body: "Agreed. Consistency is the key.\n\nAs several other browsers seem to use CTRL + <left/right>, why not just make it a KDE-wide \"directive\" that tab switching should *always* use these keys."
    author: "Tom"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-29
    body: "That already works in Konqueror for me."
    author: "ac"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-29
    body: "> The important point I think is to standardize tab access in all KDE apps. The same shortcut should be used for this in all the MDI apps of KDE (konqueror, konsole, kate, etc.).\n\nI disagree.  Ctrl+left arrow and Ctrl+right arrow are *well* established in text editors as moving backwards and forwards word-by-word.  Alt-left arrow and Alt-right arrow are *well* established in web browsers as moving backwards and forwards in history.  What's left?  Shift-left and Shift-right?"
    author: "Jim"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-30
    body: ">> What's left? Shift-left and Shift-right?\nMaybe, as it's the solution adopted by konsole.\n\nIn Windows Ctrl-Tab does the job of switching between tabs in MDI apps and I must admit that it does it well."
    author: "Ned Flanders"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-30
    body: "That means you only move in one direction :-/\nI'm all for shift+arrow keys too."
    author: "Sleepless"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-30
    body: "Ctrl-Tab is for moving one tab to the right, Ctrl-Shift-Tab goes one tab to the left. This is very consistent in the Windows environment. I'm using it for my KDE as well (after removing its use for switching between virtual desktops).\n\nShift-Arrow is used for marking text in many editors, word processors, spreadsheets... I don't like Konsole using these keys, but then, they are configurable."
    author: "der Mosher"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-12-01
    body: "> I don't like Konsole using these keys, but then, they are configurable.\n\nThey are? Where? I have Konsole 1.1.3 and KDE 3.0.5. No sign of anything like that in the Configure dialog box. Nor ind konsolerc, nor in the Control Center.\n\nElek"
    author: "Elek"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-12-01
    body: "\"Settings/Configure Shortcurts...\" - but you must use a current Konsole version."
    author: "Anonymous"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-31
    body: "Even if you can go both directions, you can still only go to an adjacent tab.\n\nI think they should use what Gaim uses in tabbed conversation windows, Alt-TabNumber, which was even one of the suggestions in the kde list thread.\n\nHis assertion that the tabs would need numbers on them if you did that is wrong though.  Gaim doesn't do that, and a lot of the time I have more tabs than fit horizontally across the window so I can't see the tab I'm going to anyway (so the number is not helpful).  I just have to know what number it is (I can count!).  If I just close, then I can easily there easily."
    author: "David Walser"
  - subject: "Re: Quick tab access on konqueror"
    date: 2003-08-30
    body: "This is not related to MDI behavior. Tabs are mainly used by dialog boxes."
    author: "der Mosher"
  - subject: "Re: Quick tab access on konqueror"
    date: 2004-01-05
    body: "so, C-M-Tab won, at least on my version of konq. (SUSE 9.0)"
    author: "portos"
  - subject: "RMB menus"
    date: 2003-08-29
    body: "It's good to see the Right Mouse Button (RMB) menu issue coming up again. I really hope this problem is cracked by the time 3.2 is out!\n\nReading through the thread, it seems like the developers are trying very hard to balance features and simplicity. It seems that everybody has their own peculiar needs when using KDE, and so every developer wants to add in an entry into the RMB to suit their needs.\n\nIt seems sensible, therefore, to make the default RMB menus *very* simple indeed, covering only the basic needs, and to make it easy and obvious for users to add RMB menu entries. For example:\n\nBASIC right click on a file:\nOpen With > list of apps, plus \"other\"\nCopy\nCut\nRename\nDelete > Trash / Delete\nActions > List of actions, plus \"other\" that lets you easily add new actions\n\nThe advanced users can then add all their favourite actions in, without cluttering our menus. There could also be a repository of actions from which the user could choose from, which aren't in the menu by default.\n\nAlso, I don't understand the distinction devs make between \"preview\" and \"edit\", as though they should be seperate menus. Surely it makes sense to be able to say, in the \"open with > other\" dialogue: \"[x] embed in konqueror\", and to be able to give each entry a name other than the name in kmenu. So I can have, for example:\n\nOpen With > \n\t* The gimp\n\t* KView\n\t* Preview (this is set to kview embedded in konqueror)"
    author: "Tom"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "..and any chance kde3.2 will use new a new default style? There is a new set out called Plastic http://www.kde-look.org/content/show.php?content=7559 and it just looks really slick - many seems to agree."
    author: "AC"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "No chance. WE just changed to Keramik and that was already quite a heated discussion. It's definitely not the intention to change the default look with ever single release.\n"
    author: "Rob Kaper"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "What's so slick about it?  It doesn't look as good as Keramik...  but Keramik is improving anyway.  It might look even slicker in 3.2."
    author: "ac"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "Why not replace Keramik with thinkeramik? The changes to Keramik in HEAD are alright, but I'd like to have seen more, perhaps to the degree that ThinKeramik was modified. Keramik-HEAD is still way too weighty for a default style, especially buttons and tabs still. ThinKeramik preserves the unique keramik look and has enough familarilty but makes it look less weighty. =]"
    author: "fault"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "But don't provide a link for those who aren't cool enough to know what it looks like."
    author: "Joe"
  - subject: "Re: RMB menus"
    date: 2003-09-06
    body: "< http://www.kde-look.org/content/show.php?content=6986 >"
    author: "A C"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "Plastik will be in the KDE CVS soon. And it will be released with KDE 3.2 in the optional kdeartwork package.\nBut it will no become the default theme. Keramik it pretty cool an unique and we can not change the default theme with every release.\nThe user has the choice."
    author: "Frank Karlitschek"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: ">>>But it will no become the default theme. ...... The user has the choice.\n\nAnd what if \"the user\" wants a new default look? This is not meant as a smart ass comment but a good reasonable possibility."
    author: "AC"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "Then the user better forks KDE from the developers. ;-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: RMB menus"
    date: 2003-08-29
    body: "OK, who's with me - im about to fork. All we need now is a knife."
    author: "AC"
  - subject: "Re: RMB menus"
    date: 2003-08-30
    body: "Well, do we really want a different default theme for:\n\nKDE 3.0 - hicolor\nKDE 3.1 - keramik\nKDE 3.2 - plastik?\n\nNo.. I don't think so.. the earliest time we can do this is KDE 4.x IMHO."
    author: "anon"
  - subject: "Re: RMB menus"
    date: 2003-09-14
    body: "I think there can be a new default theme for each new KDE serie (for example 3.x, 4.x). So I think Keramic schould be the default in 3.2. In the mean time Plastic can be further inproved so that in can be the default in 4.x."
    author: "Roderik"
  - subject: "New Design!!"
    date: 2003-08-29
    body: "Okay this design isn't bad but WTF? Why aren't they using the same design taht most kDE websites ar esupposed toa dopt, one like KDE.ORG's!!!\n\nOr maybe we want a million websites for each KDE application!!?\n\nWhy isn't it using the KDE.org design? Wouldn't we rather have a consistent loon n feel for all kde website designs. This is two steps forward from teh old design but one step backward for everything else."
    author: "Some Dude"
  - subject: "Re: New Design!!"
    date: 2003-08-29
    body: "I think this design is better than KDE.org's.\nOK, there a nice ones available at KDE.org, but only \"KDE window colors\" works correctly in all browsers.\nOn my opinions the font sizes on KDE.org are too big.\nAnd so on ..."
    author: "CE"
  - subject: "Re: New Design!!"
    date: 2003-08-30
    body: "\"taht most kDE websites ar esupposed toa dopt\"\n\nLiberal use of the keyboard keys, or how do you call that?"
    author: "Datschge"
  - subject: "Re: New Design!!"
    date: 2003-08-30
    body: "it's called \"consistent loon n feel\" ;)"
    author: "anonon"
  - subject: "TESTS INDICATE..."
    date: 2003-08-31
    body: "That is called \"scramble typing\" or ST for short, and I have conclude dthrough a multiple tests on a group of 35 people that it increases understanding by over 25% and it increases how much you remember of what I say by as much as 10%.\n\nConstantly scrambling the letters in words, forces the mind to rearrance the word as it knows it to be, fast readers may never notice that the word was scrambled to begin with due to the automatic rearranging. forcing the mind to take the time to rearrange the letters this improves attentiveness. Also, the brain is far more focused on the negatives of life and remmebers and notices them easier, so it also improves memory.\n\n\nNow, the above was just because I was typing fast, and connecting letters of one word to the other does decreae efficency for example: teh improtant will improve everythingm but tehim protant will not and the scrambling should never afect more than 20% of the word.\n\nIf you want your message understood and remembered more, you should be \"Scrambling\" too!!! ;)"
    author: "Some Dude"
  - subject: "My opinion on g/kconf"
    date: 2003-08-29
    body: "On Thursday a contraversial article about the differences between linux desktops were posted on a number of news sties. As the comments shown (over a 1000 on slashdot!) there is a REAL demand for some standardization in the GUI. A unified configuartion shared between gnome, kde and others should make a real leap forward in this direction.\n\nImagine being able to do this.... Select your fonts, color scheme, button order, language, keyboard settings, cut and paste setting, etc in kde and your settings will also apply to GTK, motif, TK, Xaw and fox applications and vice versa, no more plain grey apps in your kolorful environment.\n\n As a lot of users use hybrid desktops it is really essential, and I hope the gnome and kde developers work on unifying this stuff."
    author: "Norman"
  - subject: "Re: My opinion on g/kconf"
    date: 2003-08-29
    body: "I hope they don't. I hope no graphic user interface \"unifications\" ever bear fruits. What I do hope for is that all desktop environment projects adhere to and support the freedesktop.org standards. It is important that these developers can communicate and agree on certain basic issues (e.g. copy & paste for all unix applications irrespective of environment), through a neutral medium, but it is not necessary that they agree on everything, or more yet, try to do everything the same way.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: My opinion on g/kconf"
    date: 2003-08-30
    body: ">> try to do everything the same way.\n\nThat's already happening for a long time.\n\nThe only difference is that KDE has a lot of (ie. bloat) options for the 3 geeks out there.\n\nBetter combine those efforts and create a real ass KicKing desKtop + programs.."
    author: "wup"
  - subject: "Re: My opinion on g/kconf"
    date: 2003-08-30
    body: "KDE offered the Gnomes to join and even use the superior KDE technology, but they refused and continue doing their 80s style desktop."
    author: "AC"
  - subject: "Re: My opinion on g/kconf"
    date: 2003-08-30
    body: "The problem is that gnome2 removed options for the average non-geek too. \n\nMy experience with new users to gnome is that they think it's some old crusty ass desktop."
    author: "anon"
  - subject: "Re: My opinion on g/kconf"
    date: 2003-09-04
    body: "And which options are you referring to? Show/hide desktop icons? My dad will never change that. Tearoff menus? He never uses tearoff menus."
    author: "Ac"
  - subject: "Re: My opinion on g/kconf"
    date: 2003-08-30
    body: "You talk of freedesktop.org as though it is a set of established standards, like the LSB. Just for clarification, for those who don't know, it is in fact all work in progress, a workshop in which DE developers can work on shared technologies. So really, you don't \"adhere to freedesktop.org\", you \"work in freedesktop.org on certain technologies\".\n\nI think it makes sense for an awful lot of the base code to be the same, or at least work the same. Having compatable system trays, menu entries (.desktop files), drag and drop, clipboards, application messaging (D-BUS / DCOP) and other things that the user is unlikely to want to customise is a very good thing. Sure, it limits the scope for innovation, but it also makes the experience far more enjoyable for users.\n\nThen there are base technologies which shoudn't merge. For example, Konqueror uses KHTML, whilst Epiphany uses Gecko. Both are capable renderers, and it makes no difference to the end user which one is being used, so it makes sense to develop both and reap the benefits from diversification.\n\nIt also of course makes sense to do user-end things differently where difference doesn't matter. So it's good that KDE continues to offer more configurability, whilst GNOME offers less.\n\nIdeally, you'd be able to use GNOME, KDE and other X-apps together in any desktop environment and not lose any features that they offer, whilst still enjoying the differences each suite has to offer."
    author: "Tom"
  - subject: "OKAY OKAY FINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    date: 2003-08-29
    body: "Maybe it wouldn't be a total waste of code and space to have an option to remove those tiles. \n\nI still think it is, they are not so big and they make the Kmenu easier to navigate. It just seems that having an option for this is just too much damn configurability, if we have an option for this we might as well have an option for every single thing that could be done differently.\n\nSure maybe 1 or 2 people might care if they have a ___________________ separator or a spearator which also has text on it but I am certain taht 90% will not after using it a day or two and it's defintely good for new users. \n\nIf you really want you can always use the proposed patch."
    author: "Some Guy"
  - subject: "Re: OKAY OKAY FINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    date: 2003-08-30
    body: "..or put an option in G/KConf.\n\nBut no, all those 'hardcore' geeks out there want their options in a GUI because they can't edit text files or browse through a tree view with options.\n\nI ask to you geeks; then WHY for god/allah/boedah/<insert yours> sake do you want to have the ability to edit text files if you don't see those darn things anyway???? ??\n\nI would really like to know the answer to that, but I guess the only thing I'll get to see after this post is a silly flame in return..\n"
    author: "wup"
  - subject: "Re: OKAY OKAY FINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    date: 2003-08-30
    body: "I forgot some question marks (l33t style): ???????? ???? ?!!11 ?????????"
    author: "wup"
  - subject: "(Please stop with these kind of titles)"
    date: 2003-08-30
    body: "Why not edit text files or browse through a tree view with options you ask? KDE is a GUI, and its options should be accessible through a GUI as well. End of the story."
    author: "Datschge"
  - subject: "Re: (Please stop with these kind of titles)"
    date: 2003-08-30
    body: "Using some KConf-like utility those options would be accessable through a GUI."
    author: "wup"
  - subject: "Re: (Please stop with these kind of titles)"
    date: 2003-08-30
    body: "But not for Joe User. Do you really want books on the market like 'The best 500 hidden configuration options in KDE' like you have for Windows now? No thank you! \n\nChristian"
    author: "Christian Loose"
  - subject: "Re: (Please stop with these kind of titles)"
    date: 2003-08-30
    body: "your comparision with the windows world made me think of this: to access some hidden config options in windows, several add-on packs are available. That looks like a compromis between config option for everything, and enough (UI-accessible) config options: just distribute 'extra' config UI as a separate package (kcontrol-extras) ?"
    author: "ik"
  - subject: "Re: (Please stop with these kind of titles)"
    date: 2003-08-30
    body: ">> But not for Joe User.\n\nWe were talking about those geeky options, right?\n\nThose options aren't meant to be used by Joe User anyway (since Joe doesn't want to invest the time to find out what those options do).\n"
    author: "wup"
  - subject: "Re: (Please stop with these kind of titles)"
    date: 2003-08-30
    body: "And who gets do decide what is a geeky option and what not?  Why is the disabling of those K-Menu titles a geeky option? Just because it's a minority that doesn't want those titles?\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: (Please stop with these kind of titles)"
    date: 2003-08-30
    body: "Indeed.. this is why there has never been any user modes in any of KDE's applications. Unlike GNOME where they've experiemented with it and failed several times. For example, take Nautilus's user mode system. 99% of users just switched to advanced and kept it that way. Case2 is GNOME 2.x's overuse of gconf. 50% of new users don't know how to do something because it's not in the configuration dialogs, and the other 50% just use gconf all the time. Hopefully GNOME 3.x's does some usability testing of what the most used gconf properties in gconf tools are and adds them back to the GUI. It's their poor usability changes that have made this a failure as well (KDE has equal problems, but in the reverse direction)\n\nI think the best approach might be yet again, Apple's. The configuration dialogs actually have more options than GNOME 2.x's, and I have only had to edit .plist files once or twice."
    author: "fault"
  - subject: "Re: (Please stop with these kind of titles)"
    date: 2003-08-31
    body: ">> Unlike GNOME where they've experiemented with it and failed several times.\n\nAfaik that only happened with Nautilus.\n\n\n>> KDE has equal problems, but in the reverse direction\n\nRight, that's the whole issue.. both desktops are taking this to the extremes.. GNOME2 has too little and KDE3 has way too many options.\n"
    author: "wup"
  - subject: "konqueror tabs"
    date: 2003-08-30
    body: "the close buttons are still horrid to the extreme.\nim not too pleased with the icon turning into a button thing, but if this is the way it is going to be get rid of that ugly square box and have the intermediate button graphic to be a faded out version of the final graphic.\n\nthanks."
    author: "not_registered"
  - subject: "Re: konqueror tabs"
    date: 2003-08-30
    body: "Fully agreed... the button hover looks fugly with 99% of all styles, because 99% of styles are buggy with such small buttons. They should jsut remove the button border and have the favicon fade to the closeicon. \n\nAnother thing maybe to try using a toolbutton instead of a pushbutton. Perhaps it's less buggy. "
    author: "anon"
  - subject: "Re: konqueror tabs"
    date: 2003-08-31
    body: ">> Another thing maybe to try using a toolbutton instead of a pushbutton. Perhaps it's less buggy.\n\nVery good idea, Qt Assistant is doing that as well and it looks much better.\n\n(and more KDE apps should do that)\n"
    author: "wup"
  - subject: "KDE 3.2?"
    date: 2003-08-30
    body: "So when is KDE 3.2 coming?\nAnd KOffice 1.3 with fixed tables?\n\nDoes anybody know?\nThose two look like good candidates for primary schools,\nto save money."
    author: "it for schools"
  - subject: "Re: KDE 3.2?"
    date: 2003-08-30
    body: "Targetted dates are 8th December and 18th September."
    author: "Anonymous"
  - subject: "Re: KDE 3.2?"
    date: 2003-08-30
    body: "http://developer.kde.org/development-versions/kde-3.2-release-plan.html"
    author: "Christian Loose"
---
<a href="http://kt.zork.net/kde/kde20030823_62.html">KDE Traffic #62 was quietly released</a> last week.  A whole lot of news in this one, including some discussion on the new KPrefs, GConf2, quick tab access in Konqueror, a lot of KOffice news (beta 3 feature freeze, better support for Word 6 and Word 95), and mention of the <a href="http://pim.kde.org/">new pim.kde.org design</a>.  Thanks Russell!
<!--break-->
