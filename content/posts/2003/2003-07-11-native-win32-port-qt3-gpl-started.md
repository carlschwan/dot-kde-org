---
title: "Native Win32 Port of Qt3 GPL Started "
date:    2003-07-11
authors:
  - "binner"
slug:    native-win32-port-qt3-gpl-started
comments:
  - subject: "Cool"
    date: 2003-07-11
    body: "Very cool indeed ! So sooner or later, native Konqueror for Windows isn't a dream anymore."
    author: "Ariya"
  - subject: "Re: Cool"
    date: 2003-07-12
    body: "Not cool indeed.\nFirst reason would be that I consider this as a complete waste of time, which could be invested in projects useful to free software users. Needless to say that there are good reasons why developping fs on windows is not a good thing : http://www.fefe.de/nowindows/ .\nSecond and much more important. Qt once made a real gift to the community by licensing their extraordinary graphical toolkit (the best ever, every platform, etc. that may be) with the GPL licence. There decision was surely not easily made at this time. Do you really think we should made them doubt to continue licensing their following work with the GPL. They live thanks to the proprietary version of their Windows toolkit. Let's make them live and leave them on that... Free software is also a matter of respect, and some should never forget that. </angry>"
    author: "Henri"
  - subject: "Re: Cool"
    date: 2003-07-12
    body: "I forgot: it's long since windows has ever been a \"dream\"... Last time was win3.11fwg for me.\nWhy don't just leave windows if you want kde on it ? Or kill your boss if he doesn't want to, or just change job ? If everyone does so, only incompetent people will still use windows and this bunch of idiots will spend a _lot_ of time working together with their poor skills: that sounds like a better dream to me."
    author: "Henri"
  - subject: "?"
    date: 2003-07-11
    body: "There must be a reason why Trolltech doesn't give Qt away for free on Windows, don't you think?\n\nWhy don't you guys spend your time on something more useful: make Qt work on DirectFB or so.\nThat would help all Linux people instead of just those 2 Windows freaks out there trying to run KDE apps on top of a bad OS.\n\nAnd if people can run KDE apps on Windows there aren't much reasons to run Linux anymore.\n(except for staying away from the blue screens and Bills money eater)\nAre you sure that is really what you guys want?\n\n(sorry for being so negative.. I just don't see any future for XFree86 with core team members laying backwards doing things I don't want to know and only caring about bugfixes, if Windows/KDE would provide a faster and better looking display, why would I want to stay with Linux and the slow XFree86?)\n"
    author: "plib"
  - subject: "Re: ?"
    date: 2003-07-11
    body: ">There must be a reason why Trolltech doesn't give Qt away for free on Windows, don't you think?\n\nBut what is that reason? It cant be money. I think it would be better to Trolltech \nif they just release Free QT for Windows and Mac OS.\nThat is best way to QT become de-facto standard in cross-platform toolkits.\n\n\n>And if people can run KDE apps on Windows there aren't much reasons to run Linux \n>anymore.\n\nI disagree with you. It is not harmfull to Linux or KDE either if you could run KDE apps natively in Windows."
    author: "JMI"
  - subject: "Re: ?"
    date: 2003-07-11
    body: ">> But what is that reason? It cant be money. I think it would be better to Trolltech\n>> if they just release Free QT for Windows and Mac OS.\n>> That is best way to QT become de-facto standard in cross-platform toolkits.\n\nIf there's a GPL-ed version of Qt for Windows companies can use it to create inhouse apps without paying Trolltech.\nYes, I think money is one of the reasons.\n\n\n>> I disagree with you. It is not harmfull to Linux or KDE either if you could run KDE apps natively in Windows.\n\nI truely hope you're right about that..\n"
    author: "plib"
  - subject: "Re: ?"
    date: 2003-07-12
    body: "It's not harmful to Linux - once windows users are familiar with using linux applications, if they discover that the switch is seamless - no change of their choice of apps, then all the better for us."
    author: "Tyreth"
  - subject: "Re: ?"
    date: 2003-07-12
    body: "The problem with most (Windows) users I know is that they're saying: \"hey, if Linux doesn't have any additional value, why would I switch?\"\n\n(they don't care if it's free or not because Windows is 'free' as well)\n\nAnd switching from Windows to Linux isn't just about the apps.\n"
    author: "plib"
  - subject: "u gotta be kidding!"
    date: 2003-07-13
    body: " If there's a GPL-ed version of Qt for Windows companies can use it to create inhouse apps without paying Trolltech.\n\n\nIf there's GPL-ed version of Qt for windows, companies willing to use the free version will be forced to release their software as GPL-ed, so companies that use Qt on Windows won't use the free version like photoshop and that supposedly fast browser I keep forgetting the name. That means those companies will keep using the proprietary version and trolltech won't make any less money. Also that would be great cause people will be allowed to use great apps such as Kopete, Konqueror and the like on Windows. I really do think it's a win-win situation for floss software :)"
    author: "pat"
  - subject: "Re: u gotta be kidding!"
    date: 2003-07-13
    body: ">> companies willing to use the free version will be forced to release their software as GPL-ed\n\nYou have got to be kidding.. indeed.\n\nRe-read the GPL please.\n"
    author: "plib"
  - subject: "Re: u gotta be kidding!"
    date: 2003-07-13
    body: "Nothing would stop Trolltech from releasing a free Qt for windows under a modified GPL that doesn't allow using the free version for in-house development."
    author: "Carlos Rodrigues"
  - subject: "Re: u gotta be kidding!"
    date: 2003-07-13
    body: "Good point - but then the problem would be again misuse."
    author: "Anonymous"
  - subject: "Re: u gotta be kidding!"
    date: 2003-07-14
    body: "> Nothing would stop Trolltech from releasing a free Qt for windows under a modified GPL that doesn't allow using the free version for in-house development.\n\nTechnically true, except such a license would not be compatible with the GPL and therefore unable to run KDE.\n"
    author: "Micah"
  - subject: "Re: ?"
    date: 2003-07-11
    body: "> There must be a reason why Trolltech doesn't give Qt away for free on Windows, don't you think?\n\nProbably because they consider it to not be in their best interests.\n\n\n> Why don't you guys spend your time on something more useful\n\nProbably because those guys working on it consider it to be useful.  Scratching an itch and all that.\n\n> And if people can run KDE apps on Windows there aren't much reasons to run Linux anymore.\n\nWindows has plenty of applications already.  This isn't about KDE applications, it's about Qt applications.  Increased userbase for Qt applications means higher quality across all platforms.\n\n\n> (sorry for being so negative.. I just don't see any future for XFree86 with core team members laying backwards doing things I don't want to know and only caring about bugfixes, if Windows/KDE would provide a faster and better looking display, why would I want to stay with Linux and the slow XFree86?)\n\nStop whining and do something about it then!"
    author: "Jim"
  - subject: "Re: ?"
    date: 2003-07-11
    body: ">> This isn't about KDE applications, it's about Qt applications. \n\nLet me quote this for you:\n\"The plan is to provide a base for a future native KDE port.\"\n\n\n>> Stop whining and do something about it then!\n\nHow can I do something about it if even one of the greatest guys that has been kicked out of the team isn't able to change things drastically?\n"
    author: "plib"
  - subject: "Re: ?"
    date: 2003-07-15
    body: "Getting kicked off the team hasn't stopped Keith Packard.  Just check out http://www.xwin.org/\n\nIf he can't change the group from within, it seems he'll change the group from without."
    author: "AC"
  - subject: "Re: ?"
    date: 2003-07-11
    body: "I would strongly disagree that this is a bad thing. KDE, and the Qt/Free community stand to gain a lot by this. If one could write apps with Qt's great framework for Free on Windows, many of those apps will eventually end up on KGX (KDE/GNU/linuX). Moreover, increased adoption of technologies such as Konqueror/KHTML, KMail, and KOffice will encourage more standards compliant web sites, e-mail messages, and document formats. It's all good stuff.\n\nKonqueror is the best part, IMO. If Konqueror/KHTML are available in a native Windows format, that would mean that KHTML would be available for almost all users (Konqueror on Linux/*nix/Windows/Mac and Safari on Mac). Since Qt/Free is now available for Mac OS X, a Windows version is the last step towards a very cross platform Free Software solution. \n\nI might also add, the pressure of a community port might make TrollTech inclined to release and official Qt/Free Win32 version, since TT would gain volunteer support for their Win32 version that way... something they won't do if the Qt/Free Win32 version is a seperate codebase from their own Win32 version.\n\nI doubt it will hurt TT much, since many companies won't want their software licensed under a GPL compatible license. Furthermore, it probably won't have the full Visual Studio integration like TT offers.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: ?"
    date: 2003-07-11
    body: ">I doubt it will hurt TT much, since many companies won't want their software licensed under a\n> GPL compatible license.\n\nWhat about companies that write software internally?\n\n\n"
    author: "Arno Nym"
  - subject: "Re: ?"
    date: 2003-07-12
    body: "How many of these companies actually write these in-house, vs hiring consultants? If consultants write software, or the company hires another one to write the software, the GPL kicks in."
    author: "Rayiner Hashem"
  - subject: "Re: ?"
    date: 2003-07-12
    body: "\"Why don't you guys spend your time on something more useful: make Qt work on DirectFB or so.\"\n\nTranslation: someone on Slashdot said that XFree86 sux, and DirectFB is l33t, so why don't you work on that instead?\n\n\"And if people can run KDE apps on Windows there aren't much reasons to run Linux anymore.\"\n\nTranslation: I have no idea what Linux is all about, so I'm assuming it must just be the KDE thing.\n\n\"why would I want to stay with Linux and the slow XFree86?\"\n\nTranslation: back to this XFree86 thing, I also heard it was slow, but I never actually checked it out for myself, and am only assuming that the exceedingly sparse and barren feature set of the win32 API should be enough for anybody."
    author: "David Johnson"
  - subject: "Re: ?"
    date: 2003-07-12
    body: "I'm wondering why I'm replying to this, but what the heck..\n\n\n>> Translation: someone on Slashdot said that XFree86 sux, and DirectFB is l33t, so why don't you work on that instead?\n\nXFree86 does not completely suck, but it's development is more or less stopped (except for the bugfixes).\nThat's my problem with it, almost no new features and no new code to actually _improve_ things is being added anymore, and the core team members seem to be happy with that.\n\nDirectFB is certainly cool, but not usable (especially when using a NVIDIA graphics card).\n\n\n>> Translation: I have no idea what Linux is all about, so I'm assuming it must just be the KDE thing.\n\nSeeing how you speak with words like 'l33t' and 'sux' and visiting sites like Slashdot tells me enough about what you actually know.\n\n\n>> Translation: back to this XFree86 thing, I also heard it was slow, but I never actually checked it out for myself, and am only assuming that the exceedingly sparse and barren feature set of the win32 API should be enough for anybody.\n\nAt least the win32 GDI is multi threaded, doesn't block on many functions, doesn't have any slow way to get properties, ..... yadda, yadda, yadda, ......, and most importantly; it does not _feel_ slow, like XFree86.\n"
    author: "plib"
  - subject: "Re: ?"
    date: 2003-07-12
    body: "I swear that TuxRacer on Linux/XFree86 doesn't feel slow at all."
    author: "Ac"
  - subject: "Re: ?"
    date: 2003-07-12
    body: "That's because TuxRacer uses OpenGL, which does not use _any_ drawing functions from XFree86, and doesn't really use the other XFree86 stuff either (except for setting up some initial settings and a window, but that doesn't make a program slow).\n"
    author: "plib"
  - subject: "Qt on DirectFB"
    date: 2003-07-15
    body: "Wouldn't Qt GPL on DirectFB be pretty close to Qt/Embedded? \nI think you can even use Qt/E under the GPL.\n\n"
    author: "Jens Tinz"
  - subject: "pretty sad"
    date: 2003-07-11
    body: "Yeah, go ahead, port more free software on non-free platforms.\n\nWhy couln't these people work on something really useful ? Koffice, Kdevelop, Wine , Samba .. all these projects need help (koffice especially)."
    author: "Nobody"
  - subject: "Re: pretty sad"
    date: 2003-07-11
    body: "If Koffice want's to be really usefull and used on as many computers as possible then needs to run on as many plattforms as posibble. It means Mac and it means Windows (think about OpenOffice). Creating freely usable Qt on Windows is first step."
    author: "Petr Balas"
  - subject: "Re: pretty sad"
    date: 2003-07-12
    body: "If your primary goal is to make everybody use KOffice, yes. \nIf your primary goal is to make everybody use free software, then certainly not. \n\nSimply for the same reason that Microsoft stops to port Office to Linux (beside the PR effect), even if they could make money with it. KOffice would help Windows, because it lowers the overall costs (just as OO already does, but Sun does it only to cut off MS's primary source of revenue). "
    author: "Arno Nym"
  - subject: "Re: pretty sad"
    date: 2003-07-12
    body: "I want everybody to use free software. But you can't switch right now - there are missing some applications (MicroStation, ...). So I can go slow route - switch some apps for now and wait for other apps to catch up. OpenOffice is great for this. Having KDE apps on Windows can be very usefull in this scenario.\n\nYes this can hurt Trolltech. But maybe not. GPL version of QT can be used only for inhouse development. GPL version can be created to only work with gcc (yes somebody can port this to msvc) and without msvc development environment integration. Trolltech don't want to release GPL version of X11 (linux/unix) Qt because they think it will hurt them. Harmony created pressure forces them to release GPL version. From today point of view I beliewe GPL version helped Trolltech. Maybe Win32 GPL version can help them too."
    author: "Petr Balas"
  - subject: "Re: pretty sad"
    date: 2003-07-12
    body: "In my opinion it is better to have a common open office file standard than having koffice on Windoze. I think that porting more and more KDE applications over to Windoze does not free us from the Microsoft monopoly, on the contrary, people are lazy enough and will stay on Windows forever as they are too ignorant to learn and understand that the 21st century is the information century and whoever controls the underlying information storage, processing and distribution framework rools the world.\nOn Windoze people will probably more likely stick with MS office, as it will always be the best integrated office suite on this platform (as MS controls the underlying API). Microsoft will lower the price for their office suite so much that the free software alternatives will have great difficulties competing - remember, most people are only interested in free beer as they are ignorants and don't understand the importance of free speech."
    author: "tuxo"
  - subject: "don't think that's a problem"
    date: 2003-07-12
    body: "because most users use the OS that was installed when they bought the computer. And what does [insert name of discounter here] do when they have to make the decision:\nok, we got 50.000 comps to sell here, + windows each does ~50.000*20\u0080=1 million. So if we put Linux on it, we could do that for free (without license cost), if we wanted to. Mhh. Windows is buggy and confusing, Linux is easy and performant. Mhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh...\n=> Sooner or later Open Source will prevail even though capitalism...\nAnd I think maybe end of this year it'll start... with kernel 2.6; Koffice 1.3; KDE 3.2; Kdevelop 3 it should kick M$ ass - for free (muahmuah)\ngood thing ain't it..\nciao Marcel"
    author: "Marcel Partap"
  - subject: "Re: pretty sad"
    date: 2003-07-12
    body: "> ...Samba .. all these projects need help\n\nI would put the Samba server on the list of software that helps to promote (or rather keep) proprietary software on the desktop. If people want to use proprietary desktops, they should pay the full price for their servers. If people want to access free systems from proprietary systems, they should implement a file system that allows the proprietary systems to access the data on the free systems. \n\n(of course I can not stop anybody to do what he want with his time or money, but if you want to help to create a free infrastructure I would suggest you to work one something that helps free software by making free software better, and not by lowering the costs for the competition...)"
    author: "Arno Nym"
  - subject: "Re: pretty sad"
    date: 2003-07-13
    body: "\"Why couln't these people work on something really useful \"\n\nMaybe because its a hobby? And they want to work on a GPL port fo QT to win32?\n\nWhy do people play video games? Couldn't they be working on something really useful?\n\nThese people aren't factory slaves that can be ordered around. They are real people that have interests and ambitions. They do what they want within reason (usually).\n"
    author: "anonymous"
  - subject: "what would be _nicer_"
    date: 2003-07-11
    body: "would be a LGPL \"port\" of Qt.  That would make it attractive to more developers who must now use GTK+/wxWindows/other for licensing reasons."
    author: "Anonymous"
  - subject: "Re: what would be _nicer_"
    date: 2003-07-11
    body: "Do you want to destroy Trolltech?"
    author: "Anonymous"
  - subject: "Re: what would be _nicer_"
    date: 2003-07-12
    body: "Actually, since an identical API exists under the QPL/GPL licensing, the upshot of a GPLd Qt-Windows is that it still acts as QPL/GPL. Why? Because the application is no longer tied to a specific implementation and its license. This same issue came up during discussions of Harmony's licensing, and RMS himself remarked that a GPL Harmony would act as a QPL/GPL Qt.\n\nIt still won't allow proprietary applications to link to the library, but it's much more free than a GPL-only requirement."
    author: "David Johnson"
  - subject: "fools"
    date: 2003-07-11
    body: "void Windoze (Fools)\n{\n\nSupport QT by buying QT for Windoze. If you want QT around in the next decade buy QT for Windoze. Why push out of the market a great company like TrollTech by porting QT to Windoze. \n\nreturn (Fools)\n}"
    author: "Coder"
  - subject: "Re: fools"
    date: 2003-07-12
    body: "You may not know that through Qt 2.x TrollTech offered Qt/Windows under the GPL.  Yet somehow they weren't destroyed.\n\nBy the way, don't pretend you are a coder.  You clearly aren't."
    author: "em"
  - subject: "Re: fools"
    date: 2003-07-12
    body: "Yes, at one point (and for 1 release only!), Trolltech released Qt 2.x for Windows. As was once said on the Trolltech mailing list: the results were disastrous, with a very sudden decrease in the number of sold licenses.\n\nI know quite some companies that use Qt only for in-house tools that will never be used outside the company. GPL does NOT force anybody to release code to the rest of the world. They only require to release the source code when binaries are released.\n\nIn other words, all those in-house tools that were developped for Windows suddenly didn't need a commercial license. Hence the drop in revenue for Trolltech...\n\nTom"
    author: "Tom Verbeure"
  - subject: "Re: fools"
    date: 2003-07-14
    body: "Hmm, then TrollTech has no reason to actually want to see KDE/Linux succeed on the desktop?\n\nIf what you say is true, then TrollTech exists by virtue of the Windows desktop monopoly (because that's where their licensing revenue comes from, or at least that's what you claim). And, since both MacOS X and Unix/X versions of Qt are \"free\", if one of these platforms would take a significant/majority share of the desktop market (which is what we're all rooting for, no?), this would dramatically reduce Qt/Windows revenue. At which point it might suddenly be in TrollTech's best interest _not_ to support KDE/Unix (by not offering newer versions of GPL'ed Qt, for example) any longer.\n\nThus, if what you say is true, then KDE's success on the desktop risks destroying the company that created and supports one of KDE's base technologies..."
    author: "Pingu10000"
  - subject: "Re: fools"
    date: 2003-07-14
    body: "Obviously they have a business interest in seeing KDE succeed. Any non-free KDE application requires a commercial Qt license per developer, because you can not avoid using Qt for KDE applications.\n\n\n"
    author: "AC"
  - subject: "Re: fools"
    date: 2003-07-14
    body: "Trolltech has in interest seeing the KDE desktop *framework* succeed, since many extensions of Qt features in KDE libs are later integrated back into Qt itself, thus encouraging KDE developers preferring Qt also for commercial projects. On Unix and alike KDE as a desktop framework is already the most widespread so supporting it without sacrifying much is easy. On OSX Apple successfully convinced people that their system represents a Unix alike system encouraging the use of both commercial and non-commercial Unix solutions. While OSX itself already offers a desktop framework solution itself Trolltech obviously is confident that the KDE desktop framework will be accepted as well by many OSX users.\n\nThis can't be said of Windows user, the acceptance for an alternative desktop framework is not there thus limiting the use of Qt to it sole toolkit self. And as a toolkit without desktop framework around it encourages more in house developments (since \"it's easier\") than commercial apps integrated with Qt. A GPL'ed Qt hurts the former and boosts the latter. The latter, as mentioned, is none-existant on Windows tho so there won't exist an official GPL'ed Qt for Windows by Trolltech.\n\nI really hope those working on a GPL Qt for Windows are aware of this situation and better disallow the usage of their Qt port alone without KDE libs for this very reason."
    author: "Datschge"
  - subject: "Re: fools"
    date: 2003-07-16
    body: "You seem to understand TrollTech's interest in KDE (the framework) as rooted in TrollTech's view of KDE as an interesting (and free) feature research and test platform. Hadn't looked at it from that angle, and I must say it could make sense.\n\nI don't agree with your point of view re. MacOS X. I'd say there's just as few Mac users interested in running KDE (desktop) apps as there are Windows users interested in that (percentagewise that is, there's a lot more Windows users than Mac users, I believe ;-). Yet there is a free Qt for MacOS X, and not for Windows. So that leaves marketshare of both OSes as an important factor. Which results in an interesting situation should these marketshare figures significantly change in favor of free OSes like Linux, the BSDs, etc.\n\nNot that I don't agree that a GPL Qt/Windows port separate from TrollTech appears to be a waste of time, at least to me. Then again, since I'm not writing code for it, who am I to say what this/these guy(s) should do.  If they're itching, let them scratch it!"
    author: "Pingu10000"
  - subject: "Re: fools"
    date: 2003-07-16
    body: "You are kind of missing the point: Of course there's just as few Mac users interested in running this or that framework as there are Windows users interested in that, users naturally shouldn't even need to bother about this kind of stuff.\n\nWhat makes the OSX platform different from the Windows platform is that Microsoft on the one hand is known for trying to offer more and more all-included packages for every possible solution which sells (and buying up small specialized companies all the time for that purpose). Combined with their quasi-monopolistic position in the market this is strongly discouraging alternative solutions by competitors on the same platfrom. \n\nApple on the other hand already support KDE indirectly by using and contributing to kjs and khtml, they actively supported Trolltech for having a well integrated Qt/Mac, they advertise OSX as Unix compatible system and offer a native X Window implementation encouraging the use of non-OSX specific software as well. There are worlds between Microsoft and Apple, and Trolltech offering a GPL'ed Qt on OSX is a consequential result of that imo."
    author: "Datschge"
  - subject: "Re: fools"
    date: 2003-07-12
    body: "Don't you think they stopped providing it under GPL for windows for a reason?"
    author: "Andr\u00e9 Somers"
  - subject: "Re: fools"
    date: 2003-07-12
    body: "They didn't offer it under the GPL. They offered it under a license that forbids all commercial use.\n"
    author: "Sad Eagle"
  - subject: "OK, I'll correct a couple things"
    date: 2003-07-12
    body: "First, you can still download Qt/Windows 2.3.1 from TrollTech's site:\nhttp://www.trolltech.com/download/qt/noncomm.html\nI suppose TrollTech didn't find it so destructive after all.\n\nSecond, as pointed out by SadEagle, the license isn't GPL, but a different license that forbids commercial use.  I don't know if that includes in-house development at companies, though.\n\nMaybe a solution would be that TrollTech donated a license to KDE that couldn't legally be used for any other purpose than porting KDE to Windows."
    author: "em"
  - subject: "Re: fools"
    date: 2003-07-12
    body: "Yes I know what you mean, Trolltech is a great company, and KDE owes, well, a great deal to them. I don't know how KDE would have been without the terrific toolkit that is Qt.\n\nI find myself not wanting this project. Qt is something that is great because a company is behind it. Yes there are benefits to the larger community if there is a win32 open source port, but at what costs? I'd not have started this project as I feel I owe too much to Trolltech.\n\nI don't spose we'll know the effects for a few years. I hope it turns out ok."
    author: "MxCl"
  - subject: "Re: fools"
    date: 2003-07-12
    body: "You have a point.\n\nBut, I can't just buy an individual license for like $50.00.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "cooperation with http://kde-cygwin.sourceforge.net"
    date: 2003-07-12
    body: "Hopefully they are cooperating with the already (at least partially existing) port of Qt to win32 of the KDEonCygwin-Project.\n\n- me."
    author: "stupid anon /.-poster"
  - subject: "kde on win .."
    date: 2003-07-12
    body: "KDE + win XP ? You need a cluster :P"
    author: "anonymous ..."
  - subject: "KDE-Cygwin News From LinuxTag"
    date: 2003-07-12
    body: "http://lists.kde.org/?l=kde-cygwin&m=105796393123320&w=2"
    author: "Anonymous"
  - subject: "Well..."
    date: 2003-07-12
    body: "I hope it won't be a full featured Qt port but just enough to run some promotion-wise important KDE apps. There is no reason why people should get the impression a highly restrictive and expensive EULA-based system is allowing them to get everything for free. (I hope Microsoft will declare any execution of GPL'ed software under Windows as illegal in their EULA. =P)\n\nhttp://www.cyber.com.au/cyber/about/comparing_the_gpl_to_eula.pdf"
    author: "Datschge"
  - subject: "Re: Well..."
    date: 2003-07-12
    body: "Drop QFileDialog ;-) - honestly I don't think this is an option."
    author: "Anonymous"
  - subject: ":(((((((((((((("
    date: 2003-07-12
    body: "Make apps for LINUX! \n\nI love Linux and KDE! KDE = Linux\n\njust port to LINUX! w$ have ppl work for them... do not be stupid"
    author: "Francisco"
  - subject: "Re: :(((((((((((((("
    date: 2003-07-12
    body: "> KDE = Linux\n\nWrong."
    author: "Anonymous"
  - subject: "Re: :(((((((((((((("
    date: 2003-07-12
    body: "For him it is, let him be."
    author: "Datschge"
  - subject: "A shame!"
    date: 2003-07-12
    body: "I agree with most people here, this is really a shame.\nNow when there are really some apps which are free and\njust better than Windows we try to port them. People\nwill never switch from Windows if they can all the advantages\nof the free software world without switching.\nWhoever tries to make KDE run on Windows is doing\ngreat harm to the free software world. SHAME ON YOU!!!"
    author: "Jan"
  - subject: "What you don't get..."
    date: 2003-07-14
    body: "What you don't get is that this lays the groundwork for damaging the Microsoft stranglehold in the long run.\n\nYes, porting free software to Windows gives Windows users the benefits of free software without switching OSs. But what ties them to the Microsoft OS usually isn't any great respect or enjoyment of the OS itself, but rather the need for the tools available only on that OS.\n\nA Windows user that relies on Word, Excel, Outlook Express, and Internet Explorer will not dream of switching.\n\nA Windows user that relies on, say, KWord, OpenOffice.org calc, KMail, and Mozilla could consider switching, knowing that their tools are available on Linux.\n\nOpenOffice.org and Mozilla are two very good first steps at bridging the gap and breaking the MS dependency. More would be good. It will still take time for OO and Mozilla to gain more acceptance, but the idea isn't to \"win\" overnight. It's to provide competition vs. Microsoft products 5 years down the road...\n\n"
    author: "*Legion*"
  - subject: "Re: What you don't get..."
    date: 2003-07-15
    body: "> A Windows user that relies on, say, KWord, OpenOffice.org calc, KMail, and Mozilla could consider switching, knowing that their tools are available on Linux.\nBut why should a user switch if the tools she/he needs are all avaiblable on Windows? People honestly think that Windows is free for them as it came with their computer for no additional charge. Also, many people don't understand what an operating system is, for them it is part of the computer and they will therefore not change it unless there is a really crucial need to do so. People will also stay on Windows because like this they can have all the free software plus keep on copying proprietary apps like Photoshop etc.\n\nThus I think that it would be wiser to provide free applications only on Linux and other free operating systems. Furthermore, there should be replacements for the most important general purpose apps that behave similarily to what people are used to use on Windows. Like cars, there are many brands but it is very easy to switch from one brand to the other, as all cars have a similar human-machine interface and similar features.\n "
    author: "tuxo"
  - subject: "Re: What you don't get..."
    date: 2003-07-16
    body: "What about when Dell starts offering a PC with Linux etc.. for $100 cheaper than it's Windows counterpart? If the consumer is comfortable with the tools and apps available on Linux, because they used them on Windows.. then their NEXT PC purchase is more likely to be a Linux machine.\n\nIt's impractical to expect ANYBODY to ever install a new O.S. on an existing machine. People will only switch when buying a new machine, and this type of work makes that MUCH more likely."
    author: "Enjolras"
  - subject: "Re: What you don't get..."
    date: 2003-07-16
    body: "> What about when Dell starts offering a PC with Linux etc.. for $100 cheaper than it's Windows counterpart?\n1) With most computers sold you don't see how much costs the operating system and neither how much cost the individual parts in the computer. Note also that the price for Windows is much lower than $100 for computer assemblers.\n2) M$ will probably lower even more the price for Windows. When it is in the range of say $30, it is still profitable for them and the user will not care anymore for alternatives ($30 more or less is not important, it's a mere 10% of a future machine costing let's say $300). \nM$ will fight with all might to keep the preinstalled OS hegemony as they know that users don't care about the operating system, but they care as the one who provides the OS controls the API and also controls how information is encoded and transmitted and has thus the possibility to lock in users. This becomes even clearer when you have a look at future M$ technologies such as Palladium/TCPA: http://www.againsttcpa.com"
    author: "tuxo"
  - subject: "Re: What you don't get..."
    date: 2006-09-09
    body: "I DO know how much the OS costs. Two hundred fifty dollars. That's for people who try to get it \"open-source\" and can't access the security features. Linux doesn't need them."
    author: "Penguin"
  - subject: "Some points"
    date: 2003-07-12
    body: "Some IMHO invalid points people are making:\n\n1. \"This project is a waste of time that could be spent in other projects\".  Please remember that most people working in open source projects are volunteers.  I already have to program what I'm ordered to in my job; in my free time I will program what *I* decide according to *my* criterions.  This is explained in most FAQs about open source.\n\n2. \"This will harm migration to Linux\".  People making this point don't appear to have a very high opinion of Linux.  KDE is indeed great software but Linux can stand on its own merits.  If anything, being able to use the same desktop in Linux and Windows is one *less* reason to use Windows.\n\n3. \"This will destroy TrollTech as companies doing in-house development won't need to buy Qt\".  As I already pointed out in another comment, TrollTech itself offers Qt/Windows 2.3.1 under a non-commercial license at http://www.trolltech.com/download/qt/noncomm.html .  Surely companies doing in-house tool development don't need the latest and greatest Qt yet TrollTech hasn't been destroyed."
    author: "em"
  - subject: "Re: Some points"
    date: 2003-07-13
    body: "3. You still do not seem to understand the difference between the GPL and the non-commercial license. GPL would allow in-house development at companies, but a non-commercial license does not. Please understand that in-house development at a company is commercial activity."
    author: "Erik"
  - subject: "Re: Some points"
    date: 2003-07-13
    body: "> You still do not seem to understand the difference between the GPL and the\n> non-commercial license. GPL would allow in-house development at companies,\n> but a non-commercial license does not. Please understand that in-house\n> development at a company is commercial activity.\n\nWell, IANAL and all that.  But in any case, if TrollTech has been able to keep on offering Qt/Windows 2.3.1, whether it's because in-house development is prevented by its non-commercial license or for any other reason, why can't they do the same with the latest Qt/Windows versions?  And no, \"they probably have a reason\" is not good enough.  Unless TrollTech provides a very clear and concrete explanation of why this should not be done, I think this project ought to be supported."
    author: "em"
  - subject: "A real dissapointment"
    date: 2003-07-13
    body: "Hey whats wrong with *nix`s ????\n\nI can see QT/KDE going the same way many other ports have gone.\n\"Windows version gets more features and the Linux version gets left behind\"\n\nWhy dont we just send money to Bill Gates, It would be much easier!\n\nI say Forget Windows, lets build our own OS up first!!\n\nHey i would quite comfortably pay a small registration fee for QT/KDE if money is the problem!!\n"
    author: "Ian"
  - subject: "Re: A real dissapointment"
    date: 2003-07-13
    body: "Money is always a problem: http://www.kde.org/support/support.php"
    author: "Anonymous"
  - subject: "???"
    date: 2003-07-13
    body: "> I can see QT/KDE going the same way many other ports have gone.\n> \"Windows version gets more features and the Linux version gets left behind\"\n\nCan you provide an example of any such case?"
    author: "em"
  - subject: "Let TT be the judge"
    date: 2003-07-13
    body: "Lots of people are claiming that this will not harm TrollTech. \n\nTrollTech obviously disagree: they briefly released a GPLed version of QT for windows, and then stopped because it killed their revenue. Since then, they have not released a GPLed QT for Windows.\n\nTrollTech probably knows what is best for itself much better than the denizens of the dot. So this probably _will_ harm TrollTech, and it's a bad, ungrateful thing to do.\n"
    author: "dave"
  - subject: "Re: Let TT be the judge"
    date: 2003-07-13
    body: "> they briefly released a GPLed version of QT for windows\n\nWrong. Read the other comments before replying. *sigh*"
    author: "Anonymous"
  - subject: "Re: Let TT be the judge"
    date: 2003-07-13
    body: "I did read the other comments, esp. this one, which seemed to be informed:\n\n<quote>\n Yes, at one point (and for 1 release only!), Trolltech released Qt 2.x [implied: under the GPL] for Windows. As was once said on the Trolltech mailing list: the results were disastrous, with a very sudden decrease in the number of sold licenses.\n</quote>\n\nIf that is wrong, my apologies. However, this doesn't change the underlying point that Trolltech does not now offer a GPLed QT for Windows, and TT probably knows its own interests best."
    author: "Dave"
  - subject: "Re: Let TT be the judge"
    date: 2003-07-13
    body: "The parent of this comment, which let you imply GPL, was wrong. Read all of its followups.\n\n> this doesn't change the underlying point that Trolltech does not now offer a GPLed QT for Windows\n\nWhich proves? The problem is not a GPL version or a non-commercial version but misuse. Same can happen if a company buys one copy of the commercial version and 100 developers use it (and then request support). And I'm sure that this happens."
    author: "Anonymous"
  - subject: "Re: Let TT be the judge"
    date: 2003-07-14
    body: "As I said in the original post, I trust TrollTech's judgment better than yours or those of other dot posters. They haven't released a GPLed version of Windows QT, presumably because they think it would be bad for their business. What makes you think you know any better?"
    author: "Dave"
  - subject: "QT and the Non-commercial license"
    date: 2003-07-13
    body: "Most software is not published. Most of it is\na) Developed in-house for internal use\nb) Developed to spec by consultants for other companies for internal use.\n\nIn each case just one instance of the software is created. It therefore really doesn't matter to the companies developing it whether it's GPL or not, in the first case for obvious reasons, in the second case because the software is designed to spec and can't be used (at least not easily) by other companies. In both cases releasing under the GPL makes no commercial difference.\n\nMost of Qt's business comes from selling licenses for these two uses. If a Windows port becomes available under the GPL, they will lose most of this business. \n\nThe \"free\" version of Qt2 was for NON-COMMERCIAL use. It could not be used inside a company at all, not even in the building! (read the license if you don't believe me) It could only be used by people on their own PCs at home doing their own thing. However people are dishonest, and judging by the comments above, Qt lost money through people using the non-commercial version in commercial settings.\n\nIt is important to realise that the GPL was never about freeware, it's about selling software, but supplying the source with it, and sharing technical knowledge. It isn't a perfect situation though. Qt, in fairness to them, tried to work their way around it with the non-commercial license, but it didn't work.\n\nKDE needs Qt. It's an excellent toolkit, well worth it's cost. Trolltech needs to make money to keep Qt going. To do this they need to sell Qt licenses. I really don't see why anyone who wants KDE to succeed would encourage something that could hurt Qt.\n\nAnd in case anyone asks, I have nothing to do with Qt, I'm mainly a Java guy with hopes of better things :) I have used Qt from time to time though, and it's one of the nicest ways of programming on C++."
    author: "Bryan Feeney"
  - subject: "Re: QT and the Non-commercial license"
    date: 2003-07-13
    body: "I fully agree with you.\n\nIt isn't very wise to dig the ground where one's stading on. \nWhile the intentions are certainly of a good nature, it's a very bad idea imho. (reasons see the great explanation from Bryan above)"
    author: "stephanh"
  - subject: "Oh come on... This is great"
    date: 2003-07-14
    body: "It is. \n\nHeck, it works for GTK too. There is a GTK for windows, and people use that. Does that hurt GNOME? No. Not that I like GNOME, but that's offtopic. I say this is a great effort. Having Qt apps for all (including the sucky) platforms is a great thing. It will not harm the open source movement or Trolltech _at all_. \n\nSure people might use it in-house, but that's okay. When they release, they either have to buy a commercial Qt license, or GPL their app. What's the problem here?\n\nI for one applaud this effort. If I had some more C++ foo, I'd have helped this guy. \n\nOh, and quit spouting about Linux already. KDE works just fine on my Free/NetBSD and Solaris boxen. Oh dear, Solaris is a commercial platform! We can't have Qt on that! *gasp*"
    author: "Coolvibe"
  - subject: "Re: Oh come on... This is great"
    date: 2003-07-14
    body: "Your statement implys that GTK is as good or better a development enviroment than the native win API.  GTK is NOT!  Its a pain to develope in!  QT (and Trolltech) make their money selling a better API than Windows API to windows users.   You GPL this toolkit it will hurt their revenue!  Don't believe it?  Too bad!  There is already proof that it has and does!\n\nThe other problem with the logic of your statement is that Gnome is not a company.  It cannot be hurt as long as its developers are willing to continue working on it.  In the KDE world we have PAID developers working FULL TIME on our toolkit (the basis of it anyway)... KDE will go on if Troll Tech is hurt... the problem is that we will loose alll of those full time, paid developers.  \n\nThis project is a wast of time, effort, and is counter productive to OSS.  Period!\n\nStrid..."
    author: "Strider"
  - subject: "Re: Oh come on... This is great"
    date: 2003-07-15
    body: "If you really cared about freeware hurting someone's revenue, I do not think you will work with it in the first place. Linux hurts MS revenue, will you claim that is also too bad?\n"
    author: "Paul Selormey"
  - subject: "Re: Oh come on... This is great"
    date: 2003-07-15
    body: "We shouldn't need to care about hurting anyone's revenue, except the one we base the whole framework on, and that's incidentally Trolltech's Qt. As someone else already put it above \"It isn't very wise to dig the ground where one's standing on.\""
    author: "Datschge"
  - subject: "Ya Trolltech won't be harm by GPL software"
    date: 2004-06-25
    body: "GPL makes all soft must opensource . How can u sell an opensource soft on close source Windows ? Trolltech Qt is free for non-commercial . When we don't use it for commercial , it's ok .\nBTW , the \"signal and slot\" type of Qt is good , but Windows has MFC, WTL , wxWidgets and several GUI libs . If a company see Qt is good and they want to make their products in Qt , they must buy it . How a GPL progs can make they more profit than a commercial softwares ? Let's compare between Fedora and Red Hat Enterprise Server."
    author: "Tu Nam"
  - subject: "Not so free software"
    date: 2005-01-01
    body: "After reading several of these replies I've realized that free software isn't free at all.  Programmers need to eat just like everyone else and the money to pay them has to come from somewhere.  Based on the comments I've read it seems TrollTech has chosen Windows developers to be the ones to pick up the tab.  This seems to be the case since everyone is afraid that TrollTech will go bankrupt if they don't get their Windows license revenue.  Thus, it seems *quality* software can never be truley free unless we developers all take jobs at fastfood restaurants to pay the bills and write code by night for the greater good.  Sadly (or gladly) this is not the world in which we live.\n\nI've also read something along the lines of \"free software shouldn't be available on a closed source OS.\"  This has been countered by stating that QT works on many commercial closed source OSes.  It seems the issue is not free v. commercial software but more MS v. non-MS users.  This seems counter-productive in my mind if the goal is to win converts.  We gain nothing my charging Windows users, they have to pay anyway.  If we really want to loosen the hold MS has on the market we must embrace their users not isolate them.  Churchs don't charge sinners to hear sermons.  However, those people who believe the teachings of a particular church have no problem donating money in the name of those beliefs.  It seems that the free software model *requires* the exact *opposite* approach.  This makes it hard to convert people don't you think?\n\nI find it unfortunate that a company making such an excellent product that is in such wide use would have to go out of business because the *minority* of their users stopped buying.  If this is actually the case, and I'm not convinced that it is, then perhaps that company should re-evaluate its business model.  And indeed they will if things start to get bad.  Perhaps this is what everyone is so upset about.  Not that Windows users are getting free software but that the software won't be free at all anymore."
    author: "anonymous"
  - subject: "Re: Not so free software"
    date: 2005-01-01
    body: "No, the case is this:\n\nMost people supporting Free software are doing so for the ideology behind Free software, they take Free software code according to its rules and decided to not abuse it and give back. The few people who code and think code should be ultimately \"free\" (as in free also for abuses) often do put their code under the BSD license or into the public domain.\n\nWhat you are talking about is the very unfortunate situation on the Windows side where there is a huge 'free (as in gratis) riders' culture, people who have no respect for anything regardless if it is commercial software, shareware (which barely exists anymore), freeware or Free software. This led to a wave of adware driven software which has a higher possibility of ensuring an income for developers of seemingly gratis software, this also led to a rampage of piracy there which is holding back the adoption of Free software, and last but not least this led to abuse of Free software against its rules set by the license which is then getting dangerous when the receiving and potentially abusing audience is taking and demanding more than it is ever willing to give back. In Trolltech's case the issue was glaring: A Qt version for Windows allowing the development of Free software existed, but it was heavily abused for non-Free software use cases not allowed by the license, the very area Trolltech's livings depend on. Through this Trolltech learned it lesson and will not again make a Qt version for Windows freely available in foreseeable future.\n\nSuch a glaring disrespect does not exist among Linux/Unix as well as Macintosh users, that is why Trolltech has no problem releasing a GPL'ed version of Qt on those platforms. Furthermore Trolltech has good contact to many Free software developers, some which requested an allowance to port their Free software to Windows and then got a license for this purpose."
    author: "Datschge"
  - subject: "a Windoze port would be good"
    date: 2006-02-10
    body: "a windows port would be good.  I will try to explain through this scenario:\n\n1/  Joe finds brilliant free software {konqueror, openoffice.org, kpdf, ...} on the internet and thus 90% of the software he uses is free and open source.\n2/  Don, a friend of Joe is slightly more experienced in computer matters and uses Linux.  He explains to Joe that more or less all the software he uses can be used on linux as well.\n3/  Joe tries out a \"noob\" distro such as ubuntu or better kanotix,  quickly downloads all the open source apps he used under windows and then he adopts linux as his desktop OS.\n\n"
    author: "Tahir"
---
The <a href="http://kde-cygwin.sourceforge.net/">KDE on Cygwin project</a>, which produces ports of Qt and KDE to Windows using <a href="http://cygwin.com/">Cygwin</a> and <a href="http://xfree.cygwin.com/">Cygwin/XFree86</a>, <a href="http://sourceforge.net/forum/forum.php?forum_id=291974">announced</a> that the <a href="http://kde-cygwin.sourceforge.net/qt3-win32/">native Win32 port of the Qt3 GPL library</a> has been started, mainly driven by Richard L&auml;rk&auml;ng at the moment. The plan is to provide a base for a future native KDE port. Several <a href="http://medlem.spray.se/larkang/qt3-win32/">screenshots of the ongoing port</a> are available. Richard is <a href="http://kde-cygwin.sourceforge.net/qt3-win32/join.php">seeking developers</a>, who don't have access to the original Qt Windows sources, to help him.
<!--break-->
Holger Schroeder of <a href="http://www.automatix.de/indexlsh.html">Automatix GmbH</a>, who started the native Win32 port of the Qt2 library and finished about half of it before license conflicts due to use of the commercial Qt version disallowed him to continue, will be present in Nov&eacute Hrady at the <a href="http://events.kde.org/info/kastle/">KDE Contributor Conference 2003</a> and give a talk about the &quot;KDE on Cygwin&quot; project and its current state.