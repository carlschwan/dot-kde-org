---
title: "KDE Accessibility Project Receives Trophy"
date:    2003-05-27
authors:
  - "oschmidt"
slug:    kde-accessibility-project-receives-trophy
comments:
  - subject: "WOW"
    date: 2003-05-27
    body: "Just checked out a fe wof KDE's acessibiltiy tools and they are indeed good applications, good job KDE team!\n\nNow, there really isn't such a wide gap between KDE CVS and GNOME 2.3.x for acessibility."
    author: "Alex"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "I like KDE,\n\nBut I don't enjoy this egalite stuff. KDE developers are individuals that deserve tribute for the effort they have put in, and I think what we benefit is mostly a side effect of their doing things the way their creative minds have thought out.\n\nCheers to those who are disabled and are benefitted from the work of those who can."
    author: "drunk"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "> KDE developers are individuals that deserve tribute for the effort they have put in\n\nSure. From a KDEAP point of view, the \"egalite stuff\" is not about discouraging developers, but about emphazising the importance of accessibility. All KDE developers care about internationalization, so why not accessibility as well?\n\nAnyway, this become really important when we are implementing AT-SPI support for KDE, which will still require a huge amount of work."
    author: "Olaf Jan Schmidt"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "But where are the virtual keyboard (like GOK/GTKeybord/OSkey-X), some people can't use their fingers to the full, and there are equipment that could use KDE but it's kinda impossible without an keyboard, and perhapt some kind of braille terminal interface (like brailletty)?\n\nAnd if there would be some free/GPL software agent for Linux around...\n\n\nPs. Hopefully kdeaccessibility gets merged to kdeutils soon... ;-)\n\n"
    author: "Joe Average"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "> Ps. Hopefully kdeaccessibility gets merged to kdeutils soon... ;-)\n\nWe currently rather plan to develop kdeaccessibility as a package in its own right. This gives us the freedom to have our own release cycle, and will later allow for extra dependencies that might be needed.\n\nOlaf."
    author: "Olaf Jan Schmidt"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "> Now, there really isn't such a wide gap between KDE CVS and GNOME 2.3.x for\n> acessibility.\n\nThank you very much for you encouraging words. Regarding GNOME, you should try GOK and Gnopernicus, they are simply amazing technologies, so if I am honest, I have to admit that we won't catch up until we have implemented AT-SPI support in KDE, which will be extremely hard."
    author: "Olaf Jan Schmidt"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "What is AT-SPI?\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "AT-SPI stands for \"Assistive technology service provoder interface\". It allows braille device programs, screen readers and On-Screen Keyboards to access all the elements of the User Interface of a program, without using dirty hacks. This means that all information and widgets of a program can be accessed using alternative means ike a braille device or a one-click pointing device.\n\nIn order to connect KDE to AT-SPI, we need to implement a bridge between the Qt Accessibility framework and AT-SPI."
    author: "Olaf Jan Schmidt"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "Interesting. There's some (possibly ifdef'd out) code in KTalkEdit (which i hacked together a couple of years ago) that started to experiment with this sort of thing. Since I wrote the code things have got easier (due to the properties support in widgets) but you may still find it worth a quick look. It was basically trying to read the text of the focuswidget/widget under the pointer.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "Great! This is exactly what we are looking for. I couldn't find the source code of KTalkEdit using lxr.kde.org and Google - could you send a mail to our list concerning this?\n\nThanks,\n\nOlaf."
    author: "Olaf Jan Schmidt"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "http://mirrors.midco.net/pub/kde/unstable/apps/KDE1.x/utils/\n\nthough as I said, it should be easier now we have properties.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "Isn't AT-SPI a GNOME thing?  Where is the homepage so that I can read about AT-SPI?"
    author: "anon"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "AT-SPI is part of SUN's Accessibility Architecture which is being implemented into Gnome, see: http://wwws.sun.com/software/star/gnome/accessibility/architecture.html"
    author: "Datschge"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "Sun?  Well, that can't be good.  Not only is the information on AT-SPI is sketchy at best, they are also using complex and messy CORBA stuff all over again.  UGH. \n\n"
    author: "anon"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "Exactly. That's why I said that it will be a huge amount of work. We haven't decided yet how to do the briding, maybe we can avoid using Corba."
    author: "Olaf Jan Schmidt"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "Yes.. this is a problem... however, the use of AT-SPI is still important, I beleive. \n\nGNOME, java, open-office, and mozilla have all pretty much standardized on it, and it's quite cross platform. Using it would not only give KDE a broadbase of existing accessiblity applications while KDE native applications, such as screen readers were made, it would allow the later made KDE applications to work with non-KDE applications. For someone like a blind person, this is like removing X11 compatability between Qt and GTK. \n\nA bridge from the Qt accessibility classes (which probably need to be extended..) direct to AT-SPI would be the best solution, in my opinion. This would let KDE developers not have to use COBRA at all, except while writing this bridge of course :)... \n\nGood that accessibility in KDE is getting some attention.. congrats on the trophy, KDE accessiblity developers."
    author: "ti"
  - subject: "Re: WOW"
    date: 2003-05-28
    body: "You can find out more at\n\nhttp://developer.gnome.org/projects/gap\n\nhttp://developer.gnome.org/projects/gap/tech-docs/\n\nNote that the core compatibility layer of the AT-SPI has no autogenerated documentation at this time - however the IDL can be viewed from a link on the above page.  The AT-SPI interfaces are network-transparent, fully platform neutral and language-independent, and can be implemented on any platform or language which can use an ORB.  CORBA was chosen for the \"standard implementation\" because it's an open standard which is network transparent, platform independent, and for which many language bindings are available.  But it's easy to get bogged down in this implementation detail - which is just that, the transport layer and ABI layers can be replaced as long as the IDL definitions are met and bridges are available.\n\nThe \"complexity\" of CORBA is a consequence of doing \"full strength\" object programming in a network/IPC environment - you can't avoid reference counting, etc.  But the actual degree of difficulty is a function of the language binding (for instance the Java bindings for CORBA are very simple, C bindings tend to be ugly) and the language's support for OOP and lifecycle management.\n\nThe good news is that applications and toolkits don't have to deal with CORBA directly in order to use or implement AT-SPI; they only have to implement all the functionality that is defined by the IDL, and bridging code can be dynamically loaded to manage the backend.  At worst, this \"bridging\" code only needs to be written once for each major toolkit.\n\nSo interoperability and KDE were very much in mind when AT-SPI was designed, and I am delighted to see the interest and dedication of the KDE team towards accessibility.  Together, by promoting interoperability and free standards for accessibility, we can open doors for many users, leveraging each others effort to produce a desktop that is not only an accessible alternative, but which eventually can offer new capabilities for users with disabilities.  For such users, choice can be especially important.\n\nBest regards,\n\nBill Haneman\n"
    author: "Bill Haneman"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "See http://accessibility.kde.org/events/meeting1/index.html....\n\n"
    author: "fault"
  - subject: "Re: WOW"
    date: 2003-05-27
    body: "Maybe not on the surface, but the accessibility-support in GNOME thanks to for instance at-spi (mentioned in another comment) is really extensive and much further along than KDE. \n\nIn addition, it was actually a GNOME-app (GOK) that won the first place in the accessibility competition, something that wasn't mentioned here.\n\nI love this competition and both GNOME and KDE has still plenty of things to learn from the other."
    author: "Gaute Lindkvis"
  - subject: "text to speech"
    date: 2003-05-27
    body: "What is \"upcoming text to speech service\" ?\nFor example, a service who \"talks\" various messages from the desktop, such as error messages ? It would be great...\n\nI already tried festival, but it doesn't seem to speak french, but only english, spanish and others.\nIs it planned?"
    author: "LC"
  - subject: "Re: text to speech"
    date: 2003-05-27
    body: "The kttsd is a system that easily allows KDE programs to use a text-to-speeach system, such as Festival. It can currently be used only by KMouth, but reading out error messages can be implemented as well once kttsd is ready.\n\nTo use kttsd, you need to have a working text-to-speech system."
    author: "Olaf Jan Schmidt"
  - subject: "Re: text to speech"
    date: 2003-05-27
    body: "Here's a resource with links to several commercial and non-commercial text-to-speech programs that can speak French:\nhttp://tcts.fpms.ac.be/synthesis/mbrola.html\nI think it's possible to install French voices for festival using these mbrola voices, but I'm not sure though.\nI'm using mbrdico for Dutch, it can also do French, and it depends on mbrola."
    author: "dwt"
  - subject: "Re: text to speech"
    date: 2003-05-28
    body: "Remember Text-to-speech shipped with soundblaster? Hello, I'm Harry. \n\nProf. Brian Kahin's voice is very similar. I don't know how to imitate it, this guttural style.\n\nCool!"
    author: "Andr\u00e9"
  - subject: "Re: text to speech"
    date: 2007-04-08
    body: "POPAVA"
    author: "MUHAMMAD atif butt"
  - subject: "Thanks"
    date: 2003-05-27
    body: "A big thank you to Philippe Fr\u00e9my who took the initiative to present 2 KDE teams for competing in the Troph\u00e9es Du Libre (KDE-Edu was also in competition but GCompris won).\nFrance indeed tries its best to promote Free Software! \nBTW, anyone knows if RMS'speech about \"Eglit\u00e9,...\" is online?\n\n\"Bravo\" to KDE-Accessibility, your work deserves it!"
    author: "annma"
---
The <a href="http://accessibility.kde.org/">KDE Accessibility Project</a> is proud to have accepted a trophy at the international Free Software competition <a href="http://www.tropheesdulibre.org">Troph&eacute;es Du Libre</a> in France. The KDEAP received this trophy for a number of <a href="http://accessibility.kde.org/aids/">accessibility aids</a>, including <a href="http://www.schmi-dt.de/kmouth/index.en.html">KMouth</a>, <a href="http://kmag.sourceforge.net/">KMag</a>, <a href="http://www.mousetool.com/">KMouseTool</a> and the upcoming KDE Text-To-Speech Service.  Many thanks and congratulations to everyone who has contributed to these applications!  Captioned photos <a href="http://accessibility.kde.org/events/soissons2003/">are available</a> on the KDEAP site.
<!--break-->
<p>The event took place in Soissons near Paris. It was organized by a local Free Software group and supported by FSF Europe, a number of companies and several regional goverment agencies. Free Software is a key element in a plan to fight Soissons's unemployment problem, as the mayor of Soissons explained when she announced that Soissons wishes to become "the Capital of Free Software".</p>

<p>Richard M. Stallman gave a long talk in French about Free Software, showing how <i>libert&eacute;</i>, <i>egalit&eacute;</i> and <i>fraternit&eacute</i> relate to Free Software. Peter Korn from the Sun Accessibility Team later pointed out that all three of these are also closely linked to accessibility issues. The KDE Accessibility Project fully agrees with this. By writing programs that do not work with different means of using a computer (braille displays, screen readers, head point devices, one button navigation...) certain users are deprived of their freedom to choose the software they like -- they are made unequal. The Free Software community cannot tolerate this if it claims to act in fraternity.</p>

<p>As the GNOME Accessibility Project (<a href="http://developer.gnome.org/projects/gap/">GAP</a>) also received two trophees, it was a great opportunity to get to know some of the GAP developers in person. The atmosphere between the two projects was excellent. We had a lot of good and interesting talks about possible interoperation between the two projects. Some of the topics discussed will be presented at the <a href="http://dot.kde.org/1053713020/">KDE Developer meeting</a> in Nove Hrady.</p>

<p>The KDE Accessibility Team currently consists of a very small group of volunteers. Any help is welcome. If you have suggestions, observations, or if you are willing to contribute to the challenging effort of improving KDE Accessibility, please do not hesitate to <a href="mailto:kde-accessibility@mail.kde.org">send a mail</a> to the <a href="http://mail.kde.org/mailman/listinfo/kde-accessibility">KDE Accessibility mailing list</a>. </p>