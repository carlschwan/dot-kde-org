---
title: "Interview: Author of Ambitous Flash4Linux Project"
date:    2003-12-16
authors:
  - "fmous"
slug:    interview-author-ambitous-flash4linux-project
comments:
  - subject: "SVG?"
    date: 2003-12-16
    body: "Doesn't this sort-of defeat the purpose of the SVG standard? I assumed SVG had timeline abilities in the same way as Flash?"
    author: "Kresimir"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "The problem is no-one uses SVG... ive never seen it in the wild.\n\nThe only place ive ever seen SVG, we converted it to png, before display.  While cute, i think SVG will take years to catch on.\n\nJust my 2 agrivated cents as someone who wants an open vector format on the web.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser "
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "I once heard a legend about svg->swf convertor... Maybe working on karbon base (adding animation and things) and using the convertor you could get something done. Not that i have time or skill to work in this area (now, i don't even know what that legend said exactly -- maybe it was some pricey, commercial, proprietary and whatsnot piece of code... google would surely help -- but i have no time - aieeee, i should have been somewhere else already)."
    author: "Peter Rockai"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "http://ming.sourceforge.net/\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "They will use it ..."
    date: 2003-12-16
    body: "GIF was supposed to make PNG irrelevant and that didn't work.\n"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: They will use it ..."
    date: 2003-12-17
    body: "Huh? I thought it's the other way around.\n\nSee http://www.w3.org/Graphics/PNG/.\n\nQuote: 'PNG provides a patent-free replacement for GIF'...\n"
    author: "Leendert Meyer"
  - subject: "Re: They will use it ..."
    date: 2003-12-17
    body: "and how long was it until IE supported PNG?  even today its PNG support isn't so hot."
    author: "Ian Reinhart Geiser"
  - subject: "Re: They will use it ..."
    date: 2003-12-17
    body: "But it is good enough for most things.  As long as you are not using greater than 1 bit transparencies pretty much every browser out there will render them just fine.  Browser compatibility is not the problem here, people's habbits are.\n\nTake the logo at the top of this site for example.  It is a 9.2k gif.  A simple lossless conversion to png yeilds a size of 7.9k.  That is a 14% savings in size.  Things like that can add up quickly on your bandwidth bill.\n\nFor a site like amazon the savings would be huge.  But most people just think of png as a patent problem free version of gif.  I think someone could make a pretty penny selling their services to these websites.  Convert some key images to png, charge them 10% of their savings for your fee."
    author: "theorz"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "I'm currently working on a consulting project where SVG files are used as templates for printing stuff (all Java, using Apache's Batik (http://jakarta.apache.org/batik/) with the real data being inserted on the fly. Designers can use Adobe Illustrator (which is convenient for them) and our code can parse the templates quite easily (as SVG is XML based) and just manipulate the DOM tree.\n"
    author: "Maik"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "yes? and? so? what?\n\nthats neat, its a great backend, just like EPS, or some of the other formats.  the fact of the matter that you cannot just open it in IE, makes it pretty much a moot point.  There are a fair number of neat tools out there, and SVG might be one of them, but until MS thinks its cool we are stuck... at least until Nutscrape, Opera and Konqi all take back more than 50% of the browser market... and even then... we still dont have good plugins for those."
    author: "Ian Reinhart Geiser"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "IE can't open PDF either, in both cases Adobe offers respective plugins, and IE can't do Flash, nevertheless it's called a widespread technology.\n\nAnyway I think we shouldn't care about IE, we should rather look at what new technology can offer KDE. Regarding SVG I think it should be used by default for visualizing stuff within KDE, eg. in help tutorials and the likes."
    author: "Datschge"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Project Aon, a project which aims at bringing the roleplaying gamebooks from the Lone Wolf series online, uses SVG to display the graph of the numbered sections of the gamebooks. (Note: This will of course spoil the fun when you haven't played the books yet.) The sections are linked to the corresponding sections in the gamebook.\n\nhttp://www.projectaon.org/quick/\n\nNot all books have been SVG'ed yet. To view one of the graphs simply select a book, e.g. The Darke Crusade, and then select Graph in the second selection box.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "This is a great project. I hope as soon as it will mature it will support svg as well."
    author: "huh"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "I read SVG documents and impressed from that but couldnt find a good c++ lib for file output. I hope QT will support that too then again with one dependincy f4l will make everything."
    author: "\u00f6zkan pakdil"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "AFAIK, you can save QPictures as SVG files already. Wouldn't make this SVG output easy?\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "Yes, and from the little I have played with it so far, its seems impressive.\nI have gone as far as starting refactoring the KPresenter painting stuff to export to SVG.  The little I have shows that you can paint to is just as you would to a pixmap, but it saves it to XML.... now if there was just a viewer so i could test how accurate it was.  (yes i know of ksvg, but it needs help).\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser "
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Hey Ian,\n\nNot sure what you mean here? KSVG should be fine for static svgs.\nCan you indicate the problems you encountered? And how did you do the svg exporting?\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Mostly gradients, that and it gets confused easily.  KSVG may be fine, so we will have gotten 1% of the browser market there... its just i really would enjoy it if i could export a SVG of my KPresenter presentation and have a client read it who is still running IE...\n\nas for how i made it.  I just setup a QPicture as my paint device and painted the KPresenter page.  Now this might have bugs, I just cannot tell since there seem to be no good SVG viewers that I can definitively say work... Batik comes close, but seems to also get \"funny\" with gradients.  Maby its  a bug in QPicture? Dunno... Ill need to play some more."
    author: "Ian Reinhart Geiser"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Give them a link to the free adobe SVG plugin and they'll be fine.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Hi Ian,\n\nLike Richard said, ASV is fine to use on win using IE.\n\nI think the answer to your problems is that QPicture doesnt know\nnothing about gradients. In fact I think QPainter doesnt know about\ngradients either. Perhaps we could create a KSVGPicture that does support\nthis though.\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Try sodipodi or batik"
    author: "TomL"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "*zing* the sound of the point being missed again ;)\n\ndid i mention i was exporting from within KPresenter?\ndid i mention i was doing this as an option for the web export?\n\nthe point is we can write these buggers over and over but we cannot read them in a useful manner...  we need to wait until this catches on as a standard first.\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Ah.  Good points."
    author: "TomL"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "How many people do you really think will move from Flash to SVG when they switch from Windows? "
    author: "Jilks"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "Doesn't matter ... \n\nKDE will play crucial role in establishing SVG."
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "Well, that's not an entirely fair comparison --- Flash is an open standard (http://www.openswf.org), while Windows is not."
    author: "Rayiner Hashem"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "Actually using SVG is a great \"open\" way of doing animations on the web - our company would switch to it immediately for demos etc except for one small problem - none of the \"stock\" browsers that users have support it.  Sure the Adobe plug-in could be installed but most web developers want something that users already have.  The answer is that SVG is the future - people should start writing for SVG, browsers should start to support it (like KDE is doing) and users demand that OS's ship and load the SVG plug-ins just like they do for swf, quicktime, real-player, etc.  Of course a svg -> swf converter would be a great temporary fix.\n\nI applaud Ozkan and any student applying themselves on an ambitious learning project, but in this case future efforts would be better spent moving towards the SVG future and also avoiding any direct copyright infringement - besides I thought the Flash designer interface was a nightmare - why copy it?  Would like to see something better.  \n"
    author: "john"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "Once you have a Flash creator, SVG is just a matter of making another export filter. And personally I rather enjoyed Flash's interface when I was using it several years ago. Really they only distinctly Flash thing I see from the screenshots is the way the timeline is setup.\n\nAnd software interfaces can not be copyrighted. Expression can be copyrighted not ideas. There are other forms of 'intellitectual property' that have been argued to cover software interfaces with mixed results."
    author: "Ian Monroe"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "**I thought the Flash designer interface was a nightmare - why copy it? Would like to see something better. \n\nI dont know it was a nightmare or anything bad. anyway if u have any idea about improvement for making UI better.feel free to send me. I am a developer not a creator it was looking good to me :)"
    author: "\u00f6zkan pakdil"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "The object-oriented behaviour of Flash is very well thought out and one of Flash's greatest strengths. But the user interface is really a weak point, it is very difficult to move around and resize objects on the timeline. If you compare this with Director (another authoring-system from Macromedia), the Director solution is much better. I give Flash and Director workshops since several years and have experienced, that most people learn the Director interface much faster. It is also better for the professionals, because it is much easier to handle big projects. There are a lot of things in which Flash is better than Director, but the handling of sprites (which is what the objects in the timeline are called in Director) is much better in Director."
    author: "Michael"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "It will depend upon for quite a large audience how swiftly Mozilla-Firebird integrates full SVG 1.1 support and be ready to augment this when SVG 1.2 becomes final.\n\nPersonally,\n\nI want Konqueror, Mozilla and Safari to get their CSS 1,2 & 3 standards up to the offerings W3C has defined.\n\nPartial implementations are quite annoying, even though it is better than no implementation.\n\nFor me the browser differentiation should not be about choosing which browser to develop content for but which browser's implementation I prefer using while browsing the Internet's content.\n\nA page should load seemlessly for all the competing browsers--isn't this the point of recommended standards?\n\nFlash has become more than a feature it is now more often a nuisance to behold.\n\nIf I want to see demo presentations then yes give me a Flash presentation (ala PPT or Keynote via the browser).  If I want to navigate a site I sure as hell don't want to deal with Flash. And now that advertisers are flooding sites with Flash animations it slows down the browser experience even more.\n\nHell it also allows for poorly written Flash which then tends to lock up Firebird.\n\nSVG requiring validation makes that a bit more trouble free."
    author: "Marc J. Driftmeyer"
  - subject: "Re: SVG?"
    date: 2003-12-17
    body: "> Partial implementations are quite annoying, even though it is better than no implementation.\n\nFull implementations of recent w3c specs are quite hard to do, and take years to perfect. Perhaps css2 will be completely supported by IE && Gecko && Opera && KHTML, but I wouldn't bet on it until 2005 or so."
    author: "anon"
  - subject: "Re: SVG?"
    date: 2003-12-16
    body: "It would make sense to have F4L save its files in SVG or even compressed SVG (SVGZ) as its default format, for several reasons:\n\n* Creating a new format is a waste of effort when a sufficient one exists and KSVG now exists\n* It would put F4L right at the center of KDE thanks to KSVG\n* It would help drive the transition from SWF to SVG\n* It would mean you could edit the files in other applications without having to export, something that will be *really* appreciated in KOffice and OpenOffice in the future\n\nI only hope this happens..."
    author: "Tom"
  - subject: "The screenshots are slow to look at with Konqueror"
    date: 2003-12-16
    body: "Has someone tried to look at these screenshots:  http://s3dmesh.sourceforge.net/SS/screenshots.htm The page is *really* slow in konqueror(Beta2 from CVS-07.12). I also exprerience this with large images in konqueror in general. I have a Dell-Inspiron 8200 with 1.8GHz and 512MB-RAM which should be quite sufficient."
    author: "Andreas Joseph Krogh"
  - subject: "Re: The screenshots are slow to look at with Konqueror"
    date: 2003-12-16
    body: "On a 2x800p3 (one mencoding something @ 100%) w/2GB, it just loads right away (might have something to do with connection speed?) CVS from 5days ago? \n\nHaven't had a problem with large images in konqueror much at all for a long time (1.x & 2.x were *sometimes* slow)"
    author: "James L"
  - subject: "Re: The screenshots are slow to look at with Konqu"
    date: 2003-12-16
    body: "bug 39693 \"loading images blocks konqueror\"\nhttp://bugs.kde.org/show_bug.cgi?id=39693\nthe 2nd bug in the list of bugs with most votes (without wishlist)"
    author: "Helge Hielscher"
  - subject: "filters"
    date: 2003-12-16
    body: "and what about doing a flash export/import filter for karbon ?\nI never used karbon but that's seems like an obvious solution for the future (f4l\nwas a demo project that had to be self-contained if I am right ...)."
    author: "nicolas"
  - subject: "Re: filters"
    date: 2003-12-16
    body: "Is importation of Flash files ever possible? The last time I played around with Flash was several years ago (Flash 4) and I don't think it was then."
    author: "Ian Monroe"
  - subject: "n1"
    date: 2003-12-16
    body: "offtopic but 2 cool... http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/screenshots/OOo-KDE-NWF.jpg"
    author: "anon"
  - subject: "Re: n1"
    date: 2003-12-17
    body: "huh? what did I miss here?"
    author: "me"
  - subject: "Re: n1"
    date: 2003-12-18
    body: "<a href=\"http://dot.kde.org/1071245692/\">http://dot.kde.org/1071245692/</a>\n"
    author: "Anonymous"
  - subject: "Eeek.  Flash?"
    date: 2003-12-18
    body: "Flash is evil.  It's used for entire websites instead of small elements within a site.  That needs to change.  It's discriminatory, making blind users second class netizens when normal websites actually give them much better opportunities than normal.\n\nBeyond that, Flash is just a hideous app -- it's totally unintuitive, and basically was never designed.\n\nIf you want my advice, create a nice, new object-oriented SVG app.  If you're really stuck on Flash, then clone Adobe's LiveMotion instead.. it, in itself, was a clone of Flash's technology, only it was much more logical and easy to use."
    author: "Lee"
  - subject: "sodipodi"
    date: 2005-04-18
    body: "sodipodi is a vetor tool that works with SVG standard format.\nit is also opensource, but does not contains animation I think."
    author: "Ali Imran"
---
When I first came across the <A href="http://f4l.sourceforge.net/">Flash for Linux website</A> my curiosity <a href="http://f4l.sourceforge.net/SS/">was piqued</a>. I contacted the author <a href="mailto:ozkanpakdil@users.sourceforge.net">&#214;zkan Pakdil</a> and it resulted in the following small interview.  As you will note, the project is seeking developer assistance.  So if you are looking for a project to work on, this might be the one.
<!--break-->
<p>
<hr>
<h2>&#214;zkan Pakdil on Flash for Linux</h2>
<p><strong>Please introduce yourself.</strong></p>

<p>My name is &#214;zkan Pakdil a.k.a. mascix, on IRC. I am 24 years old and from Turkey.</p>

<p><strong>How did you start the project?</strong></p>
<p>A friend of mine suggested me to submit <A href="http://f4l.sourceforge.net/">F4L</A>
to <A href="http://freshmeat.net/">Freshmeat</A> so that it could get more exposure.</p>

<p><strong>What made you start working on it before you submitted it to Sourceforge?</strong></p>

<p>About 5 months ago me and my friends were talking about a good project we could use as a reference
for our graduation. They suggested to code something like <A href="http://web.icq.com/">ICQ</A>
but because there already are a lot of projects for instant messaging on Linux we decided not to do that.
Then I had the idea of F4L.</p>

<p><A href="http://www-5.ibm.com/tr/">IBM T&#252;rkiye</A> had a
<A href="http://www-5.ibm.com/tr/products/linux/odul/projeler.html">competition</A> between
universities in Turkey about application development on Linux. One of the rules
was that projects have to be open source. F4L was chosen as a finalist from 21 projects. </p>


<p><strong>What license is F4L distributed under?</strong></p>

<p>F4L is fully GPL.</p>

<p><strong>What is the current status of F4L?</strong></p>

<p>F4L is not finished, yet. Currently it works just like KPaint and other drawing programs, so there isn't any motion
tweening  at the moment. I just wrote code in C++ and Osman Y&#252;ksel, a Flash designer, <A href="http://f4l.sourceforge.net/SS/f4l01.jpg">has</A>
<A href="http://f4l.sourceforge.net/SS/f4l02.jpg">made</A>
<A href="http://f4l.sourceforge.net/SS/f4l03.jpg">some</A>
<A href="http://f4l.sourceforge.net/SS/f4l04.jpg">samples</A>
<A href="http://f4l.sourceforge.net/SS/f4l06.jpg">and</A>
<A href="http://f4l.sourceforge.net/SS/f4l06.jpg">screenies</A>.</p>

 <p><strong>What code can we find in CVS? What functions are working ?</strong></p>

<p>Right now, it can draw 2D objects and can import other images. If someone would want to make an animation,
he/she can make it by hand as  it can add frames. :) After adding some frames, you can draw objects and
move the timeline cursor by hand one by one and watch animation. There isn't an SWF output option, yet.</p>

<p>Basically, I created the menus based on Flash's layout, but not all the options are available, yet. In
the Window menu, some options are working and about half are working in the Text menu.</p>


<p><strong>Do you plan to have those features in F4L?</strong></p>

<p>Right now I am working for <A href="http://s3dmesh.sourceforge.net/">s3dmesh.sf.net</A>,
which is my graduating project for university and, therefore, I'm lacking some time, but I plan to have things
like motion tweening  and other abilities like Flash has.</p>


  <p><strong>So right now the project is paused?</strong></p>

<p>Yes, temporarily. It is more in a sleep mode now and it will be awake in about 1.5 months. Now,
I am looking for developers who want to help me, which is why I decided to announce it on Freshmeat.</p>


  <p><strong>What does the project need most right now?</strong></p>

<p>Developers. Actually, I wanted to make this team bigger, but good developers already seem to have
their projects and don't want to work for F4L. I hope that announcing my project on Freshmeat will
attract a few developers that are looking in the same direction as F4L.</p>


<p><strong>Is there a mailinglist for F4L?</strong></p>

  <p>Yes, there is a <A href="http://lists.sourceforge.net/lists/listinfo/f4l-general">mailinglist</A> at sourceforge
    and there is a also contact link on our <a href="http://f4l.sourceforge.net/">website</a></p>

 <p><strong>What motivates you to work on F4L?</strong></p>

   <p>I think that, within any given Linux environment, there is an extreme lack of WYSIWYG type applications -- particularly
    of the Flash variety. In the Windows world, Flash is a well-known tool for creating animated vector graphics. I want to bring
    this type of functionality to Linux, but I do realize that this will not be an easy task. Flash functionality is very hard to
    duplicate. Typically, users do not know about the internals of good SWF designers such as its algorithms and data structures.
    They just create great looking Flash videos for the Internet. With all the algorithms and data structures involved, I
    believe doing this application will make me a more experienced developer. :)</p>