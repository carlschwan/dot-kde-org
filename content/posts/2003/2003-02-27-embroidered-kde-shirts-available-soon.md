---
title: "Embroidered KDE Shirts Available Soon"
date:    2003-02-27
authors:
  - "ebrucherseifer"
slug:    embroidered-kde-shirts-available-soon
comments:
  - subject: "What about business?"
    date: 2003-02-27
    body: "I'd like a business shirt, white, button-down-the-front with the KDE logo.  Or alternately, a tie.  Some of us have to or even like to dress nicely.  Given the comments I get whenever I wear one of my Beatles ties, I'd imagine that a KDE tie would promote KDE's visibility nicely."
    author: "Evan \"JabberWokky\" E."
  - subject: "Charles's Nemesis"
    date: 2003-02-27
    body: "You see, Evan, ties are the most rediculous articles of attire in the history of humanity.\n\nIn terms of stupidity and uselessness, they surpass even the propeller beanie.\n\nI will not rest until ties these abhorrances are eliminated from the face of the planet."
    author: "Charles Samuels"
  - subject: "MOD PARENT UP!!"
    date: 2003-02-27
    body: "MOD PARENT UP!!"
    author: "Navindra Umanee"
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "Actually, I have been trying to get my ex's friend to design a tux tie. Whil you may not like ties, they are in the world. I wore them so high school, and even now, I will wear them when I have to (hehehehe). If you have to wear a tie, better a cool KDE or Tux tie vs a green, red, or blue tie."
    author: "a.c."
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "www.thinkgeek.com has some nice Tux ties, I own one :o)\n\nThey also have a cool binary tie."
    author: "Brunes"
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "I couldn't agree more. Ties became fashionable because some 17th century noble frenchman who refused to bathe used a scarfe around his neck to keep the stink in. This 'evolved' into the tie you see today. Think of this when you see someone in a tie. Then you see why I fail to take ties and tie-wearing seriously. And why I refuse to wear them. "
    author: "coolvibe"
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "Well, there's also bowties, which are useless AND harder to tie.\n\nAnd let's not get started in legginses! (I honestly don't know if that is the word, but babelfish says it is ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "> And let's not get started in legginses! (I honestly don't know if that is the word, but babelfish says it is ;-)\n\nIf you meen those stretch trousers used mainly for sports/gymnastics... no this word doesn't exist ;-)\n\nleggins IS plural. As there are no trouserses there are no legginses.\n\nIt's one or more \"pair(s) of leggins/trousers\". This is because there are two \"shafts\", one for each leg.\n\nOf course most people just say leggins/trousers (the same for glassES->two eyes).\n\nJust a boring thursday afternoon ;-)\n"
    author: "Harald Henkel"
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "Ok, I knew Babelfish output doesn't always make sense. But returning non-words is a new one :-)\n\nTry this: In babelfish, ask for the translation of \"polaina\", spanish->english\n\nYou will get leggins\n\nNow try to translate \"polainas\" ... oops :-)\n\nAnd in spanish we have the same rule for these things (pantalones==pants, anteojos==glasses)"
    author: "Roberto Alsina"
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "How can one trust a guy who intentionally puts a noose around his neck?\n\nUwe\n"
    author: "Uwe Thiem"
  - subject: "Re: Charles's Nemesis"
    date: 2003-02-27
    body: "Reminds me of a declaration HRH Claus made during \nthe award ceremony for the Prince Claus Awards.\n\nDeclaration Of The Tie, december 8 1998:\n\nNow the workers of the world have cast aside their shackles,\nthe modern workman has bound himself again.\nTo the ever present tie,\nto the snake around his adam's apple,\nto the cord of sin we use today to measure decency.\nTiewearers of all nations unite!\nCast off the rope that binds you!\nRisk your neck! \nLiberate yourself and venture forth into open-collar paradise!\n\n(takes off his tie, waves it around a little and throws it to the floor)\n\nIf a person in his position can make such a statement, so can you.\n\nJohan Veenstra"
    author: "Johan Veenstra"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "This is our first try with embroidered shirts. So lets see how successful it is, then we can consider other products. \n\nGreetings,\neva\n\n"
    author: "Eva Brucherseifer"
  - subject: "Re: What about business?"
    date: 2003-03-07
    body: "I don't know if you know this but there were more profits from StarWars accessories rather than the films alone!\n\nSo if you don't already have a BUNCH of T-shirts, hats, car-stickers, PC-stickers, toys, and so on, and so on, then your marketing/advertizing mashine REALLY sux. And the logos for the sewing machines -- it should be free downloadable! At least that cost you nothing.\n\nAbout the ties: I HATE ties. But you should have A LOT of ties with KDE logo, KDE artwork, ... Whoever don't like them, don't buy!\n\nP.S. You mentioned the T-shirts will be dark-blue. I love T-shirts but I wear only light-colored ones. Why don't you make light-gray T-shirt or light-green or something like that. And don't forget this is Internet and I'm in Bulgaria and I still want a T-shirt. So please make sure there is way everyone can get the stuff if he/she want.\n\nP.P.S. If you want we can manufacture these clothings here in Bulgaria. Info: cheap workforce, many local companies are already manufacturing high-quality clothings for West-Europe."
    author: "\u00cf\u00e5\u00f2\u00fa\u00f0 \u00cf\u00e5\u00f2\u00f0\u00ee\u00e2"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "I business shirt or a tie with KDE logo? Somehow this defeats the purpose of them (looking reliable). Have you ever seen an IBM guy with a IBM shirt? :)\n"
    author: "AC"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "Uhh, yes."
    author: "Morty"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "And HP reps with HP shirts, etc.  In fact most of the big business types have either very elegantly (read: tiny, single tone) embrodered dress shirts available and/or wear tie clips and lapel pins.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What about business?"
    date: 2003-02-28
    body: "Guess I can't wear my IBM shirt to work anymore if that is true. Heck at the site I work at most of the people are in  Blue Jeans..."
    author: "Kevin"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "I totally agree. This always my problem as well. I only wear shirts and suits (I know people call me weird, oh well) and I never can wear some nice OS shirt. The only item representing my OS support is my pinguin pin on my jacket....mh, maybe we should have little Konqui pins? :-)\n\nRegards,\nStephan"
    author: "Stephan Richter"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "Yes, more konqi stuff! T-shirts, ties, pins, laether jackets with with flames and and \"Konqi Krew\" lettering. \nThe girls will go wild."
    author: "reihal"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "Aren't we already? ;-)"
    author: "annma"
  - subject: "Re: What about business?"
    date: 2003-02-27
    body: "Lapel pins might be an even better idea.  Something simple, gold... something that would fit right in alongside a Rotary and Dobbshead.  Hehehehe.\n\n(And I share your problem - although I would like to endorse certain things, I don't wear t-shirts.)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What about business?"
    date: 2006-08-11
    body: "i had 50 beatles ties from manhattan menswear. i even had the dear prudence \"red\" version and just sold it on ebay for 400.00 last week.\n\nhttp://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&category=2995&item=140010066370\n\nkeep an eye out for this tie at a reasonable cost or at all. and if you already have it congratulations."
    author: "levi"
  - subject: "car sticker"
    date: 2003-02-27
    body: "Hey ! \n\nwhat about a car sticker to put in the back window ? just like the apple we see on so much car ?\n\ni would like to have a tux and a KDE logo on my car. showing to poeple around that i use and love so much your environnement\n\n"
    author: "somekool"
  - subject: "Re: car sticker"
    date: 2004-05-05
    body: "You can go at \nhttp://tuxsticker.ptaff.ca\nto get one!"
    author: "dirac"
  - subject: "Re: car sticker"
    date: 2004-05-05
    body: "create for tux.....\n\nwhat about KDE ??? :)\n"
    author: "somekool"
  - subject: "Can the files be released?"
    date: 2003-03-02
    body: "I have access to an embroidery sewing machine (My mom has 2, she is a sewing freak).   I've love to download the logo and put it on a few my my shirts.  She has the ability to download (compatable) files from the internet and use them."
    author: "Hank"
  - subject: "DRROOOLL......"
    date: 2003-03-04
    body: "doing my best Homer Simpson impression... \n\n\"kde shirt - mmmmmmmmm.......\""
    author: "bopeye"
---
The <a href="http://events.kde.org/">KDE Promo Team</a> and <a href="http://www.kernelconcepts.de">kernel concepts</a> are bringing you more shirts featuring your favourite desktop. The shirts are dark blue and feature an embroidered 4-color blue <a href="http://dot.kde.org/1046288936/kdeshirt_embroidered_logo.png">KDE gear</a> about 5.5cm in size. They are available in polo shirt, t-shirt and girlie shirt versions. Polo shirts are especially targeted at KDE members who do public promotional work at expos and so on. We have prepared a <a href="http://events.kde.org/registration/index.phtml">form for preorders</a> with full details on price, size and shipping costs.
<!--break-->
