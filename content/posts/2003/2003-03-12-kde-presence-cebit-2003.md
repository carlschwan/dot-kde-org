---
title: "KDE Presence at CeBIT 2003"
date:    2003-03-12
authors:
  - "numanee"
slug:    kde-presence-cebit-2003
comments:
  - subject: "'view this bizcase' links broken?"
    date: 2003-03-11
    body: "Or is just my CVS konqi :) ?"
    author: "jmk"
  - subject: "Re: 'view this bizcase' links broken?"
    date: 2003-03-12
    body: "No, it wasn't just yourself there was a lot of broken stuff on enterprise.kde.org but I think I have fixed the majority of it, all except for the forums. I will have to follow that up with the sysadmin's. :-)\n\nJason\n"
    author: "Jason Bainbridge"
  - subject: "What I'm waiting for..."
    date: 2003-03-11
    body: "Is the release of Knoppix 3.2 at CeBIT with KDE 3.1! See <A href=\"http://knopper.net/knoppix/index-en.html\">this link</a> for details."
    author: "Rodion"
  - subject: "Re: What I'm waiting for..."
    date: 2003-03-12
    body: "If you look here:\n\nhttp://www.kde.org/announcements/cebit2003.php\n\nyou'll notice that we have manufactured a set of knoppix-based live-CD's as well for the KDE booth at CeBIT. Essentially they are working the same way with the exception that the KDE e.V. CD is based on Debian 3.0 (woody) while the original Knoppix is a mixture of Debian testing and unstable; the KDE packages on both CD versions are basically the same, they're all available at \n\ndeb ktown.kde.org/~nolden/kde woody main\n\nfor free download.\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: What I'm waiting for..."
    date: 2003-03-12
    body: "Do the live-KDE CDs have the same hardware detection as Knoppix 3.1 or 3.2?\n\nI'm handing out Knoppix CD's to friends and colleages to great succes. A lot of obscure hardware can be detected, but not everything. And most of the software just works! Everybody is really impressed.\n\nThere are quite a few things to improve though and it's best not to fork such things too much. live-KDE is very suitable for showcasing KDE on a system known to work, but for handing out CDs to people with random hardware, I choose Knoppix 3.2.\n"
    author: "Jos"
  - subject: "Re: What I'm waiting for..."
    date: 2003-03-12
    body: "The live-KDE CDs are built on the Knoppix base and have the same hardware detection engine. The main difference today is a slight one : the live-KDE CD is based on Woody when Knoppix uses a mix of testing and unstable. In the future, hopefully we could put on the CD more promotion material for KDE (manuals, demos, information) but first we have to write it, translate it and package it.... Volunteers are welcome..."
    author: "Charles de Miramon"
  - subject: "Re: What I'm waiting for..."
    date: 2003-03-13
    body: "> (manuals, demos, information)\n\nAnd the KDE websites for surfing offline!\n"
    author: "reihal"
  - subject: "Re: What I'm waiting for...ISOs"
    date: 2003-03-13
    body: "Any Iso's of the Knoppix or KDE eval version available?"
    author: "Ross Baker"
  - subject: "KnoppixKDE"
    date: 2003-03-15
    body: "Is the alias of the KDE live cd. It was assembled by EADZ of Knoppix.net\n\nDocs: http://www.knoppix.net/docs/index.php/KnoppixKDE\nDownload: http://ktown.kde.org/~dirk/knoppixkde/"
    author: "thadk"
  - subject: "Screenshots from 3.2 current?"
    date: 2003-03-14
    body: "... can't wait to go to cebit @monday ;-)\n"
    author: "anonymous"
  - subject: "Re: Screenshots from 3.2 current?"
    date: 2003-03-15
    body: "Monday might be too late.  I think KDE is only there for the weekend, not sure though."
    author: "Navindra Umanee"
  - subject: "Re: Screenshots from 3.2 current?"
    date: 2003-03-16
    body: "Yes, you are right. But several developers are still here until Wednesday. \nSharp-booth in Hall 1 is where the OPIE-Team is located. Hall 6 is the linuxpark\nand there are companies like Danka and Erfrakon here, too."
    author: "Carsten Niehaus"
  - subject: "Re: Screenshots from 3.2 current?"
    date: 2003-03-16
    body: "Kurt Pfeiffle, Daniel Molkentin and Rainer Endres are extending their presence until Tuesday. Best chance to meet them is in the Linuxpark, Hall 6. \n\neva"
    author: "eva"
  - subject: "KDE 3.2 roadmap?"
    date: 2003-03-14
    body: "I couldn't find a roadmap/... online for 3.2...  \n\n\"The KDE team will be showcasing current developments scheduled for KDE 3.2\"\n\nany chance people who aren't going to CeBIT could see this?\n\nja"
    author: "Jesse"
  - subject: "Re: KDE 3.2 roadmap?"
    date: 2003-03-14
    body: "http://developer.kde.org/development-versions/kde-3.2-features.html\n"
    author: "Sad Eagle"
---
<a href="http://www.cebit.de/">CeBIT 2003</a> kicks off tomorrow and as usual <a href="http://kde.org/announcements/cebit2003.php">KDE will be present</a>.  The KDE team will be showcasing current developments scheduled for KDE 3.2, Rainer Endres will be giving a talk on KDE on the <a href="http://enterprise.kde.org/">corporate</a> desktop and Martin Konold will be presenting the latest and greatest on the KDE <a href="http://www.kroupware.org/">Kolab</a> Groupware Solution.  Plus, be sure to check out the <a href="http://www.opie.info/">Opie Team</a> for synching KDE with PDAs, and feel free to join the key-signing party. 
<!--break-->
<p>
<i>Many thanks to all of our sponsors including <a href="http://www.lsexperts.de/">LSE</a>, <a href="http://www.fujitsu-siemens.de/">Fujitsu-Siemens</a>, <a href="http://www.suse.de/">SuSE</a>, <a href="http://www.credativ.de/">credativ</a> and <A href="http://www.danka.de/">Danka</a>.</i>