---
title: "KDE-CVS-Digest for July 25, 2003"
date:    2003-07-26
authors:
  - "dkite"
slug:    kde-cvs-digest-july-25-2003
comments:
  - subject: "I'm on it, I'm on it =)"
    date: 2003-07-26
    body: "My first week with CVS access and i'm already at the famous CVS Digest of Derek Kite =)\n\nThanks for it Derek !"
    author: "Albert Astals Cid"
  - subject: "Re: I'm on it, I'm on it =)"
    date: 2003-07-26
    body: "Congrats :-)\n\nI admire everybody doing the great work.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: I'm on it, I'm on it =)"
    date: 2003-07-28
    body: "Congratulations!\n\nI must admit I envy you :-)\n\nXtian"
    author: "Sangohn Christian"
  - subject: "Thanks Derek!"
    date: 2003-07-26
    body: "Thanks a lot again, I don't know what to do if there wouldn't be such a nice thing like your kde-cvs digest :)\n\nThanks."
    author: "Teemu Rytilahti"
  - subject: "CVS Commit Summary"
    date: 2003-07-26
    body: "The 'CVS Commit Summary' showing number of commits is very nice, now you can see - to some extend - how active one branch is compared to others."
    author: "IR"
  - subject: "Re: CVS Commit Summary"
    date: 2003-07-26
    body: "Branches and modules are different things."
    author: "Anonymous"
  - subject: "Re: CVS Commit Summary"
    date: 2003-07-26
    body: "And which modules need a lot of bugfixes and which are just stable ;-)"
    author: "Arnold Krille"
  - subject: "Thanks Derek for a valuable point!"
    date: 2003-07-27
    body: "> Many developers could use hardware upgrades, even a bit of cash to make it to the developer conferences. Or we can support those who support the developers. Or sponsor someone so they can devote more time to the project.\n\nDerek, I can't thank you enough for saying that. I know a lot of people really enjoy the software we produce and a fair amount have made contributions. On a recent article posted here we did very well with contributions, but we haven't seen any in over a month with the exception of two people who have stepped up to help my sponsorship of Andras with $100 and $200 a month. That's made it a lot easier. Now I'm looking at the KDE conference in August in Austria. I don't think a lot of people realize that I'm not wealthy. I just have a small business here with no employees and a handful of contractors. Having been very sick and on doctor ordered bedrest in May cost me thousands of dollars and has affected my ability to look at things like the upcoming developer conference. \n\nCurrently I'm looking at the possibility that with assistance from others I might be able to go. Thanks to the help of some others it will not cost me that much more to send Andras. The only way I can go though is if a few dozen of our users are moved to express some grattitude because I still have to cover some additional expense of doing this. I don't exactly get paid vacations. I have a lot of respect for so many KDE developers I've never been able to meet in person and this would be my first chance to get away from work (I can't remember the last time I took a real vacation) and devote full time to Quanta. It would also give me the chance to add another C++ developer to Quanta... Me. I am not real productive in C++ because I haven't been able to take time to immerse myself in it. That would make a huge difference of maybe 30%+ more developer hours on Quanta right there.\n\nThe total irony is that last year I had thought my business would be where I didn't need assistance to make this trip but things happen. Part of what happened was that I had to choose between seed capital for my business, or honoring my commitment to Andras. I believe strongly in the cause of open source and it has real costs and has involved personal sacrifice, but I don't want to cry on anybody's shoulder. I do however want to spend more time on Quanta, go to this conference and pay Andras more money. I'm hoping the CVS snapshot we are preparing so totally blows people away they will feel appreciative enough to help me realize these goals. I believe I'm proof that one person can make a difference. I certainly appreciate greatly everyone who does help and I personally thank them.\n\nMy special thanks to everyone who's contributed code, encouragement and funding to help us make Quanta the best web development tool on the planet."
    author: "Eric Laffoon"
  - subject: "Err... but I could be dense..."
    date: 2003-07-27
    body: "Andras commented on my post to say the conference is in the Czech Republic. I was thinking of my dad's wife being from Austria, their trips there and that it was close, and typed the wrong thing. Sorry."
    author: "Eric Laffoon"
  - subject: "kprinter"
    date: 2003-07-28
    body: "It wouldn't be Michael if he could not figure out some more features to impove in Kprinter.\nThanks to him and the cups team printing on Linux has become a pleasure.\n\n\n"
    author: "Ferdinand"
  - subject: "Re: kprinter"
    date: 2003-07-29
    body: "Indeed. It seems there are improvements all down the printer chain. This is one of the last barriers to linux and KDE being a mature desktop. \n\nDerek"
    author: "Derek Kite"
  - subject: "KWallet"
    date: 2003-07-29
    body: "Even though it's hardly off the ground, it's really good to see KWallet underway. The inability to just save username&password combinations in Konqueror is my number one annoyance in KDE, so just having that implemented would be great. A fuller solution with good security and integration with all applications that save passwords will be a wonderful tool as well."
    author: "Tom"
---
<a href="http://members.shaw.ca/dkite/july252003.html">This week's  CVS-Digest:</a> Find out who is paid to hack KDE. A new application, <a href="http://edu.kde.org/klogoturtle/">KLogo-Turtle</a>, for teaching children.
<a href="http://printing.kde.org/">KPrinter</a> adds access to online printer database. <a href="http://kontact.kde.org/">Kontact</a> adds a to-do list plugin. A redesigned font installer. The beginning of KWallet. And many more features and bugfixes.
<!--break-->
