---
title: "KDE-CVS-Digest for January 31, 2003"
date:    2003-02-01
authors:
  - "dkite"
slug:    kde-cvs-digest-january-31-2003
comments:
  - subject: "Definately not the end!"
    date: 2003-01-31
    body: "There is still at least one annoying bug in konqueror that makes it so I won't use it.  Konqueror doesn't honor the nowrap tag in <td> elements.  This makes it really annoying to use in squirrelmail.  Squirrelmail is my primary email client, so this forces me to use mozilla instead.  As an avid KDE user this hurts.  The really sad part is that nowrap works fine in KDE 3.0.x.  This should have been in 3.1 final but it isn't!  I have voted for the bug on bugs.kde.org and so have a few other people.  Please fix this!  Thank you.\n\n\nBret.\n\nP.S.  Yes I posted this in the story below.   Yes I am pretty annoyed with this bug.\n\nReferences:\nhttp://bugs.kde.org/show_bug.cgi?id=52993\n\n"
    author: "Bret Baptist"
  - subject: "Re: Definately not the end!"
    date: 2003-02-01
    body: "Yes, I can verify this. My backend on OSNews is terrible looking (a huge line of icons instead and it doesn't even show scrollbars!) because of this limitation on Konqueror. Other browsers don't have this limitation.\n\nHOWEVER, Safari does not have this problem! The page was able to render as it should have with Safari. I hope that this change will make it to the latest Konqueror from Apple."
    author: "Eugenia"
  - subject: "Re: Definately not the end!"
    date: 2003-02-02
    body: "Safari is based on KHTML from KDE 3.0.2.  The original poster said that this isn't a problem in KDE 3.0.x, so maybe after the merging Safari won't work either.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: Definately not the end!"
    date: 2003-02-01
    body: "This is possibly because nowrap tag is deprecated in HTML 4, it should be done using CSS\n\n.nowrap\n{\n\twhite-space : nowrap;\n}\n\n<td class=\"nowrap\">THIS TEXT WON'T WRAP</td>"
    author: "Albert 'TSDgeos' Astals Cod"
  - subject: "Re: Definately not the end!"
    date: 2003-02-01
    body: "This CSS already works on Konqueror so i think i shouldn't be too difficult making Konqueror support the non correct syntax."
    author: "Albert 'TSDgeos' Astals Cid"
  - subject: "Re: Definately not the end!"
    date: 2003-02-09
    body: "Are you sure this work in css?\nI tried it, and it doesn't work with Konqueror 3.1 (release).\nWhen you put multiple spaces, it should not collapse the spaces. Konqueror does.\n\nHere is the example I used:\nIn the .css:\n.nowrap\n {\n white-space : nowrap;\n }\n \nIn the html:\n               <p class=nowrap>\n\n                +-----+       +-----+\n                |     |       |     |\n                |     +-------+     |\n                |     |       |     |\n                +-----+       +-----+\n\n                </p>"
    author: "Fred"
  - subject: "Re: Definately not the end!"
    date: 2003-02-02
    body: "Well even if it is depreciated, it is still the part of the HTML 4 standard.  There is no excuse for dropping support of this tag.\n\n\nBret.\n\n"
    author: "Bret Baptist"
  - subject: "Re: Definately not the end!"
    date: 2003-02-10
    body: "So, write a patch. I'm fairly sure it was just an accidental bug rather than a 'limitation'."
    author: "alex"
  - subject: "Re: Definately not the end!"
    date: 2003-07-23
    body: "is there a way to create a nowrap tag that does wrap after a certain number of characters?\n\ni have a title that should not wrap as long as it can fit in the set size of the frame, BUT it should start wrapping after 4 chanracters b/c then it stretches the design out of shape.\n\nany ideas??\n\nthanks"
    author: "ivelina"
  - subject: "Good job"
    date: 2003-01-31
    body: "As always: Good job, Derek.\nI really appreaciate your work."
    author: "Marlo"
  - subject: "Thanks!"
    date: 2003-01-31
    body: "Thanks a lot to Derek Kite, by the way. The effort is much appreciated. I'm not really a programmer myself, but I love to know more about what the developers are working on, and the CVS-Digest makes it a lot easier. Thanks!"
    author: "Eike Hein"
  - subject: "Re: Thanks!"
    date: 2003-02-01
    body: "Me too! Keep doing good work, please"
    author: "Guest"
  - subject: "Re: Thanks!"
    date: 2003-02-03
    body: "It's only getting better each time. Hope you never loose interest in KDE Derek. :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "changelogs"
    date: 2003-01-31
    body: "It's me, or developers are doing better CVS changelogs to appear nicely in the digest?\nHeheheh, keep the good work.\n\nAnyway, I would like more generic information about the week, you know, the top part of the digest is great."
    author: "Iuri Fiedoruk"
  - subject: "Re: changelogs"
    date: 2003-01-31
    body: ">developers are doing better CVS changelogs to appear nicely in the digest\n\nInteresting. I doubt if there is any conscious decision, but seeing the information used somewhere else is a small incentive. It makes the boring stuff seem worthwhile.\n\n>I would like more generic information about the week, you know, the top part\n\nMost comes from developers pointing out what I missed.\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Digest"
    date: 2003-01-31
    body: "Thank you, Derek !!"
    author: "star-flight"
  - subject: "Ties to Infinity"
    date: 2003-01-31
    body: "Is the 3.1 the ultimate in KDE?\n\nAs long as machines compute, man will make evolve. The ultimate in KDE are it's ties to infinity. Future generations to inherrit 'the konstruct'. :)"
    author: "IR"
  - subject: "Re: Ties to Infinity"
    date: 2003-02-01
    body: "huh?"
    author: "confused boy"
  - subject: "thank you, KDE team!"
    date: 2003-02-01
    body: "It's late at night and I just downloaded KDE 3.1 for my old Suse 7.1 system (because there are no Mandrake RPMs available). It is a 450Mhz P2-machine and I am really impressed of the performance, especially of the Konqueror-Filemanager. I now also use Konqueror for the first time as a web-browser. The tabs-solution is really great, and all of my favorite webpages work fine. A big thank you to the KDE-team, enjoy the fame! (And to be not too OT: I think this fantastic release is a very good starting point for the next milestones. I also want to thank Derek for his work, the CVS-digest really gives a good overview of the work of the developers!)"
    author: "Michael"
  - subject: "Hey I'm quoted!"
    date: 2003-02-01
    body: "Definitely not my all time best quote. ;-)\n\nYou're doing a great job Derek. Keep it up!"
    author: "Eric Laffoon"
  - subject: "Wish they'd deal with KMail"
    date: 2003-02-01
    body: "I've made Konqueror my default browser but still cling to Mozilla Mail & News as my IMAP client. There are some major drawbacks to KMail as an IMAP client as a tour through bugs.kde.org will show you..\n\nI always watch the CVS digests for bugfixes regarding IMAP though; looking forward to being able to use KMail as my default mail client!"
    author: "Brad"
  - subject: "Re: Wish they'd deal with KMail"
    date: 2003-02-01
    body: ">bugfixes regarding IMAP\n\nI think most of the work has been done. The Kroupware project and make_it_cool kmail had (from my understanding) fixed most issues with imap. I'm not sure all has been merged yet, but the fixes didn't make it into 3.1.\n\n3.2.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Wish they'd deal with KMail"
    date: 2003-02-02
    body: "No, it hasn't. I'm still integrating folderjobs from make_it_cool and it's going rather slow. Till all folderjobs will be merged imap will behave flaky."
    author: "Zack Rusin"
  - subject: "Lycos/comdirect"
    date: 2003-02-01
    body: "I still haveto use IE (via Crossover) to use pageslike lycos chat or the comdirect online broker.\nMy fault, or still not (yet) possible?"
    author: "Micha"
  - subject: "Re: Lycos/comdirect"
    date: 2003-02-01
    body: "Your fault ;-) At least comdirect work perfectly, when you change the user agent for this page to \"Netscape Navigator 4.76\". "
    author: "Johann Lermer"
  - subject: "Re: Lycos/comdirect"
    date: 2003-02-01
    body: "Just tried it.\nToo bad, doesn't work.\nWhen I use IE 6.0/Win2K, I get a \"timeout\" after klicking \"login\".\nWhen I set it to Nav 4.76, just nothing happens :(\n"
    author: "Micha"
  - subject: "Re: Lycos/comdirect"
    date: 2003-02-02
    body: "Comdirect is very  strict with the browsers, they don't accept everything. Navigator 4.8 e.g. will never be supported, as their hotline told me.\n\nIE 6.0 doesn't work here either, but Netscape 4.76 definitely does (btw: still 3.05 here, I'm still compiling - maybe I know more tomorrow).\n"
    author: "Johann Lermer"
  - subject: "Wrong author"
    date: 2003-02-01
    body: "It's funny to see how other people are committing my commits ;) I'm doing the RDP backend in KRDC and two weeks ago I had committed some stuff and Tim Jansen was listed in the CVS-Digest as the committer, last week I did some more commits and this time Waldo is listed as the committer..."
    author: "ArendJr"
  - subject: "Re: Wrong author"
    date: 2003-02-02
    body: "Fixed.\n\nReminder to self. Update kde-common/accounts from cvs before starting anything.\n\nDerek (sorry about that)"
    author: "Derek Kite"
  - subject: "My cent"
    date: 2003-02-02
    body: "Hope the control center mess will be cleaned up.\n\nMy 3.x: \n- KDEsambaplugin\n- Kdevelop 3\n- hbasic\n- Usability improvements \n\nHope also Kde.org will estabilish a security.kde.org project to provide KDE/qt hackers with security related information and help develop a security architecture. Programming style and security related training is important."
    author: "Hein"
  - subject: "Re: My cent"
    date: 2003-02-02
    body: "http://www.kde.org/info/security/index.html\nhttp://developer.kde.org/documentation/other/index.html\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
---
Is the <a href="http://www.kde.org/info/3.1.html">3.1</a> the ultimate in KDE? The end of development? Judging by the 2000 or so
commits this week, it wouldn't appear so. Some of the less trivial fixes from <a href="http://developer.apple.com/darwin/projects/webcore/index.html">Apple</a> are getting applied to 
<a href="http://www.konqueror.org/">Konqueror</a>. The user interface continues to be refined. <a href="http://www.kdepim.org/">The KDE PIM</a> project and all of its individual parts are a beehive of
activity. Utilities such as <a href="http://www.k3b.org/">K3b</a> and <a href="http://cdbakeoven.sourceforge.net/">CD Bake Oven</a> are being actively worked on. 
Read all about it in <a href="http://members.shaw.ca/dkite/jan312003.html">this week's KDE-CVS-Digest</a>. I'm already impatient for <a href="http://developer.kde.org/development-versions/kde-3.2-features.html">3.2</a>!
<!--break-->
