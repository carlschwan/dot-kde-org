---
title: "Urgent Help Needed for UK Linux Expo"
date:    2003-06-19
authors:
  - "Jono"
slug:    urgent-help-needed-uk-linux-expo
comments:
  - subject: "I wish! :-P"
    date: 2003-06-20
    body: "I wish I could go down there and help, but I'm stuck in Edinburgh :-( \n\nWell, guys, enjoy as much as you can and take photos! :-)"
    author: "uga"
  - subject: "I wish I could..."
    date: 2003-06-20
    body: "... but the expo clashes with the best party the UK has to offer - Glastonbury festival!\nI'll probably be wide eyed, grinning and dancing to some kicking tunes for most of the Expo, bah. :-)"
    author: "lanroth"
  - subject: "Re: I wish I could..."
    date: 2003-06-20
    body: "Bastard! You got tickets!"
    author: "dave"
  - subject: "Re: I wish I could..."
    date: 2003-06-20
    body: "You might try asking 'round some LUG's - Every LUG in the uk was offered two free tickets, at least someone must have one free 8*)\n\n"
    author: "Philip Scott"
  - subject: "Re: I wish I could..."
    date: 2003-06-22
    body: "I think he was talking about tickets for Glastonbury, which are very difficult to get.  \nHowever the two don't clash completely, I'll be going to both!"
    author: "Jonathan Riddell"
  - subject: "Would if I could..."
    date: 2003-06-21
    body: "I'd love to come and help, but I can't get the day off work :-(("
    author: "bagpuss_thecat"
  - subject: "noo!"
    date: 2003-06-22
    body: "now i feel bad for being such a lousy yank :/\n\n\nbe sure to get those pics!"
    author: "standsolid"
  - subject: "Update"
    date: 2003-06-23
    body: "Hi folks,\n\nHere is an update.\n\nI just went down to set the stand up with Lee. I have got some help on the way to come and help man (and women) the booth. I will be taking lots of pics with my digital camera and will have plenty of stories to tell I am sure.\n\nThe expo looks pretty damn impressive with lots of standa and attractions. I will no doubt write a report on that also.\n\nHope to see some of you there!!\n\nCheers,\n\n  Jono\n"
    author: "Jono Bacon"
---
Recently it came to my attention that Chris Howells could not make the expo date next week to run the KDE booth, and I have taken over trying to sort something out in time so KDE is properly represented.

This is a plea for help from those people who can come to the <a href="http://www.linux-expo.co.uk">LinuxUser &amp; Developer Expo 2003</a> next week (Tue 24th - Thur 26th June 2003) in Birmingham, UK to help run the KDE booth. We are also looking for any help with printing, leaflets and other items for the booth.
If you can help, please <a href="mailto:jono@kde.org">contact me</a> as soon as possible and we can make arrangements.

<!--break-->
