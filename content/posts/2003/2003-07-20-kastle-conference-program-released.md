---
title: "Kastle: Conference Program Released"
date:    2003-07-20
authors:
  - "dmolkentin"
slug:    kastle-conference-program-released
comments:
  - subject: "Very old bug still not confirmed"
    date: 2003-07-21
    body: "The very old bug http://bugs.kde.org/show_bug.cgi?id=14553 is still marked as UNCONFIRMED. That bug has annoyed me a lot for a very long time. I find it strange that only I and 2 other people have been bothered by it and voted for it. Is it OK for everyone else? If not, please confirm it."
    author: "Erik"
  - subject: "Bug?"
    date: 2003-07-21
    body: "This is reported and filed as wish.\nOne should really consider to delete all off-topic bug report postings to this siste as long no moderation is possible."
    author: "Anonymous"
  - subject: "Re: Bug?"
    date: 2003-07-21
    body: "Full ACK! "
    author: "Andr\u00e9 Somers"
  - subject: "Re: Very old bug still not confirmed"
    date: 2003-07-21
    body: "The page will not be reloaded. Possibly it makes a GET with If-Modified-Since."
    author: "Arno Nym"
  - subject: "Re: Very old bug still not confirmed"
    date: 2003-07-21
    body: "> The page will not be reloaded.\n\nI was thinking more about the main issue, that Konqueror displays the page from the top when going back, and then maybe after a while displays the page at the expected point.\n\n> Possibly it makes a GET with If-Modified-Since.\n\nKonqueror should NEVER reload the page when going back/forward because:\n1. It involves network traffic, which is MUCH too slow in this case. Going back/forward must be fast, and must thus be handled completely local.\n2. It is not what the user expects. The user expects to see the page as it was when he left it. The result should be equivalent to if the user viewed the page and opened a link in a new window and made the equivalent of browser-'back' by switching back to the previous window (Alt+Tab). Konqueror does not make a GET with If-Modified-Since in that case either, right?"
    author: "Erik"
  - subject: "Re: Very old bug still not confirmed"
    date: 2003-07-21
    body: "\"possibly\" \nHere lies the problem. The right behaviour is: _never_ but otherwise, back/fw with \"dynamic\" sites is both slow and not the right behaviour."
    author: "Henri"
  - subject: "Questions.. :)"
    date: 2003-07-21
    body: "Wish I could come.. but can't afford travelling to Prague in what is probably the single most expensive week of the year. :-/\n\nAny Kopete/KAddressBook developers going? Atlantik needs integration with such services for its invitation system..\n\nThiago, are you going? I'd love a int KExtendedSocket::pingTime(const QString &hostName) static method. ;-)\n\nHm, if there's room on the Frankfurt bus that might work for me. Martijn, how about another highrise fest? (but do bring your hiking shoes this time)\n\nNeed.. to.. find.. euros.. to.. travel.."
    author: "Rob Kaper"
  - subject: "Re: Questions.. :)"
    date: 2003-07-21
    body: "Contact the KDE e.V. board: They offered to fund a part of travel and lodging expenses (up to 80%) as sponsoring permits if you can explain why you need it and provide copies of bills."
    author: "Anonymous"
  - subject: "Re: Questions.. :)"
    date: 2003-07-21
    body: "Yeah Rob, please write the e.V.!\nI hope you can come. Been wanting to meet with you."
    author: "ac"
  - subject: "Re: Questions.. :)"
    date: 2003-07-21
    body: "Martijn and I are going to be hacking Kopete at N7Y, and KAddressBook integration is Martijn's aim for the hackfest.  If you can't come, we should hash out what you need beforehand.\n\nWill"
    author: "Will Stephenson"
  - subject: "Re: Questions.. :)"
    date: 2003-07-21
    body: "N8Y.\n\nLasttimeIcheckedUNIXwasspacesensitive."
    author: "Rob Kaper"
  - subject: "Dreaming of one track"
    date: 2003-07-22
    body: "Difficult to decide which talk to attend at some times. I really wished that the conference would go over 4 days with only one track (and allowing you to sit in the sun during uninteresting talks). Let's hope that records of all talks will be available after like planned."
    author: "Anonymous"
  - subject: "Re: Dreaming of one track"
    date: 2003-07-22
    body: "Who said that it is planned?"
    author: "AC"
  - subject: "Re: Dreaming of one track"
    date: 2003-07-22
    body: "http://mail.kde.org/pipermail/novehrady/2003-July/000212.html"
    author: "Anonymous"
---
The Nov&eacute; Hrady program committee is proud to announce the <a href="http://events.kde.org/info/kastle/conference_program.phtml">schedule for the conference program</a> of the <a href="http://events.kde.org/info/kastle/">KDE Contributor Conference 2003</a>. From now on only minor changes are expected to happen. Conference registration for all attendees is still possible until 29th July. Companies are invited to support KDE and the conference with their attendance. If you're a KDE contributor and want to join the subsequent hackfest please drop an email to <a href="mailto:eva@kde.org">eva@kde.org</a>. Also note that there will be a <a href="http://events.kde.org/info/kastle/keysigning.phtml">keysigning party</a> on Monday. Read about the <a href="http://events.kde.org/info/kastle/keysigning.phtml">required preparations</a> if you plan to attend the keysigning.
<!--break-->
