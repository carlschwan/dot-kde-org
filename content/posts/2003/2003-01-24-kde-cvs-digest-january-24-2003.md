---
title: "KDE-CVS-Digest for January 24, 2003"
date:    2003-01-24
authors:
  - "dkite"
slug:    kde-cvs-digest-january-24-2003
comments:
  - subject: "Excellent!"
    date: 2003-01-24
    body: "The format is very nice, the news also. Nice development this week.\n\nHappy to see such a leap in the development of kde pim and kmail, and even happier to see the high level of discussion in the kmail/kdepim lists. Seems that the problems are over, and we are going to see the best pim suite ever to surface!\n\nCongrats, Derek, your digest is better every week!\n\nPS. I would like to understand more about the icon theme changes. I can see that it was a move towards the draft at freedesktop.org, but what was that move?\n"
    author: "CWoelz"
  - subject: "The real news"
    date: 2003-01-24
    body: "The masses demand 3.1 final."
    author: "Sam"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "Perhaps they're still waiting for RedHat packages to be made ....\n... somewhen next year. "
    author: "sushi"
  - subject: "Gimme gimme gimme"
    date: 2003-01-24
    body: "3.1!"
    author: "CWoelz"
  - subject: "no no no"
    date: 2003-01-24
    body: "That would just be an anti climax... Let's wait another few weeks... :)\n\nI think the KDE team should release KDE as soon as possible, in other words when it's good and ready... in the waiting time I'll just compile a debug build of RC6."
    author: "Lovechild"
  - subject: "AOL!"
    date: 2003-01-24
    body: "I would even settle for KDE-3.1rc6 as nice SuSE RPM packages....\n\nI can't compile KDE in the office..."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "Far as I'm concerned, take another month or more at it if it means we'll be seeing this many performance and memory enhancements.  Right today 3.0.5 is working just fine.  Nobody will remember the delay down the road, but folks will remember a really solid upgrade.\n\nThanks to Derek's efforts we can at least see what all is going on while we wait.  All good stuff!"
    author: "Metrol"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "I couldn't agree more, the KHTML merges that result from Apple's work on Safari are worth waiting for alone."
    author: "John"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "They can release 3.1.1 (or 3.1.0.1) if they want to two weeks after 3.1. Neither lack of some new features nor non-critical bugs should hold a release this long."
    author: "goosnarh"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "nah.. let's go for another release canidate\n"
    author: "IR"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "note: that was a joke"
    author: "IR"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "A text message as to why 3.1 has not been released on the 3.1 info page would be nice.  I tried to compile 3.1rc6 last night, and khtml errors out when it looks for htmlpageinfo.cc, which never gets created.  I will assume this an issue having to do with the Safari merges, but one never knows...\n\nI've just installed a brand new lfs system on my new desktop, and now I'm just waiting for 3.1..."
    author: "As patient as I am..."
  - subject: "Re: The real news"
    date: 2003-01-25
    body: "make fails with error 139: this means uic, which is supposed to create htmlpageinfo.cc segfaults.\n\nBut I have no idea how to fix this. I'm just as stuck as you are."
    author: "m"
  - subject: "Re: The real news"
    date: 2003-01-25
    body: "1) What distro are you using?\n2) What are your CFLAGS/CXXFLAGS?\n\nI'm running lfs cvs (which means the latest binutils/glibc/gcc), and my CFLAGS are '-Os -march=i686 -mcpu=pentium3'\n\nI'm going to try omitting optimizations as well as attempt to compile earlier release candidates to get to the bottom of this.  I've compiled 3.1rc3 without a hitch running lfs cvs from about a month or two ago on this same machine.  I've tried with and without -j (parallel) builds (I run a dual cpu).\n\nHere are my (mostly) relevant package version numbers:\nbinutils 2.13.2.1\nbison 1.875\ngcc 3.2.1\nglibc 2.3.1\nxfree86 4.2.1\nqt 3.1.0\n\nI hope when I try compiling without options as well as earlier versions I can figure out whether or not this is an issue with rc6 or with a new package that breaks something (all of the above mentioned packages are really new, with the exception of x)."
    author: "As patient as I am..."
  - subject: "Re: The real news"
    date: 2003-01-28
    body: "I've got bad news for you. You build system is broken :(\n\nThe gcc configure script doesn't recognize the w.x.y.z (2.13.2.1) version number of binutils. So you gcc is broken. I guess that the price you pay for living on the edge.\n\nregards,\n\nJohan Veenstra"
    author: "Johan Veenstra"
  - subject: "Re: The real news"
    date: 2003-01-29
    body: "I happen to have just build an lfs system from cvs as well. What I find confusing is that if my system is \"broken\" why has every thing else thus far compiled with no problem? I've got gnome completely installed (1.4, and 2) not to mention a mail server and QT-3.1, and the arts package (which according to the BLFS book have to be installed prior to kdelibs).  Wouldn't the system being broken imply that none of this could happen? It sucks so much to watch my old box compile beasts like java and mozilla from scratch (which took a total of a little over 12 hours total) only to die now. "
    author: "Mike Hernandez"
  - subject: "Re: The real news"
    date: 2003-02-06
    body: "\n*chuckles*\n    Even more mysterious is to *get* kde3.1 completely built, up functional including the wonderful addon kplayer, and after the box gets whacked by a power hit, have *everything* in /opt/kde/bin/ segfault ... period.\n    THEN go back and have *exactly* the error listed in this thread occur when trying to rebuild kde3.1 ....\n    Fortunately I've still got 3.0.5a installed and working.   In the meantime I'd like to know *what* happened at reboot to b0rk the install ... and have this result.... \n-No it was NOT QTDIR being mis-set ... by default I set QTDIR by running /opt/kde/bin/kde-config|grep Qt|awk -F : '{ print $2 }'|cut -d \\  -f 2 -- So QTDIR is set by KDE  -- /opt/bin/kde is a softlink in this case...\nwhich I can flip from install to install.\n\n\n  If anyone has even the slightest hint ......"
    author: "Alistair"
  - subject: "Re: The real news"
    date: 2003-02-06
    body: "*thwack* ..... \n\n    That was a head on a desk ... \n       nasty nasty install routines that put soft links in /usr/bin can\n       make you seem so *very* silly. "
    author: "Alistair"
  - subject: "Re:UIC segfault in kdelibs/html"
    date: 2003-02-08
    body: "    Well -- now after thoroughly cleaning up some old links into older directories, and STILL having the problem I got serious.\n   gdb uic and run in the kdelibs/khtml dir with the parameters that make passes to it generates the segfault ... and backtrace (lo and behold) points to /opt/kde/lib/libkio.so.4\n--- libkio from This Very Tree..... Hmmmm ... ldd on libkio shows nothing unusual .. SOooo Make clean in the kdelibs source dir, mv /opt/kde/lib/libkio.so.4 (and all references) to /opt/kde/hold and ... remake kdelibs.. make SUCCEEDS!! (cheers) \n\n   What happened?? the backtrace indicated a function for construction/destruction of objects ( I have it tucked away) ... and I suspect that the old softlinks in /usr/bin that pointed to an older QT are to blame for the initial libkio being incorrectly built. --- thus I have nowagain a wonderful working kde3.1 build ... now ... if I could only get noatun to start without the 'loop on one song' affect... *grin*"
    author: "Alistair"
  - subject: "Re:UIC segfault in kdelibs/html"
    date: 2003-02-11
    body: "Hello guys,\n\ni'm trying to compile KDE 3.1 from sources and i'm also experiencing uic segfaulting in a few places ( while compiling kdenetwork for example )\nRunning gdb on the offending instruction ( /opt/qt-x11-free-3.1.1/bin/uic -o newconnectiondialog.h ./newconnectiondialog.ui ) yields the following :\n\nProgram received signal SIGSEGV, Segmentation fault.\n[Switching to Thread 8192 (LWP 26276)]\n0x0805da5d in QMapPrivate<QString, QString>::QMapPrivate() ()\na backtrace gives the following:\n\n#0  0x0805da5d in QMapPrivate<QString, QString>::QMapPrivate() ()\n#1  0x40489441 in QSettingsGroup::QSettingsGroup() () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#2  0x40493345 in QMapPrivate<QString, QSettingsGroup>::QMapPrivate() () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#3  0x40493170 in QMapPrivate<QString, QSettingsHeading>::QMapPrivate() () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#4  0x4048a1a8 in QSettingsPrivate::QSettingsPrivate(QSettings::Format) () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#5  0x4048c88b in QSettings::QSettings() () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#6  0x401a8a91 in qt_init_internal(int*, char**, _XDisplay*, unsigned long, unsigned long) () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#7  0x401a8fdc in qt_init(int*, char**, QApplication::Type) () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#8  0x401f9031 in QApplication::construct(int&, char**, QApplication::Type) () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#9  0x401f8e84 in QApplication::QApplication(int&, char**, bool) () from /opt/qt-x11-free-3.1.1/lib/libqt-mt.so.3\n#10 0x0804e064 in main ()\n#11 0x420158d4 in __libc_start_main () from /lib/i686/libc.so.6\n\n\nhas anyone seen this ? i don't feel like debugging the QT code :-((\nThanks for your help !"
    author: "Charles-Edouard Ruault"
  - subject: "Re: The real news"
    date: 2003-02-14
    body: "I have the same problem with uic segfoulting on htmlpageinfo, but I looked at the output of uic and I think it's actually complete before it segfaults.  I modified the Makefile to ignore the return value and assume uic finished correctly.  The rest of khtml compiled successfully.  We'll see how well it functions, but I think it will work.  If someone knows how to fix this properly though, I'd like to know."
    author: "Ramon Medina"
  - subject: "Re: The real news"
    date: 2003-02-20
    body: "I found out the problem has to do with a broken binutils.  I got a new binutils from cvs and compiled it, along with gcc, glibc, and Qt.  uic no longer segfaults and now I just have to recompile kde properly so everything doesn't crash on exit like uic was."
    author: "Ramon Medina"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "It will be out today(Friday)\n\n"
    author: "Anon"
  - subject: "Re: The real news"
    date: 2003-01-25
    body: "too bad, it didn't :("
    author: "Iuri Fiedoruk"
  - subject: "Re: The real news"
    date: 2003-01-24
    body: "A guy from suse said that 3.1 is being packaged right now, he said it on the suse-linux-e@suse.com mailing list. Im sure there's an archive somewhere if you dont believe me."
    author: "fletch"
  - subject: "Re: The real news"
    date: 2003-01-25
    body: "So there is hope. Maybe this time, as an upside, the Suse packages save settings at logout ;-)"
    author: "Debian User"
  - subject: "Re: The real news"
    date: 2003-01-25
    body: "Ahhhhhh .. damn it . i dont need any stickin bin packages! All i need is the source code to be realeased! The joy of gentoo!"
    author: "Mike"
  - subject: "Re: The real news"
    date: 2003-01-26
    body: "The source is already available. \n\nSo you're waiting just like everybody else until your distribution provides you with the means of installing it."
    author: "Johan Veenstra"
  - subject: "Re: The real news"
    date: 2003-01-26
    body: "But where do I get them?\nOnly via CVS?"
    author: "Micha"
  - subject: "Re: The real news"
    date: 2003-01-28
    body: "http://www.kde.org/info/3.1.html"
    author: "Johan Veenstra"
  - subject: "nice"
    date: 2003-01-24
    body: "Very nice work.\n"
    author: "Mooby"
  - subject: "Frustration Frustration !!"
    date: 2003-01-24
    body: "Firstly I agree with others before me, Derek you are doing a fantastic job on this CVS updates. New format a big improvement, imho. \n\nOn the down side, still waiting for KDE 3.1, I have been staunch in my support for the fanatstic work that the paid and volunteer KDE developers do. They have my immense respect, even now as some people (understandably) are begining to sound off at the delays.\n\nI firmly believe that 3.1 is going to be an awesome upgrade, and definately worth waiting for, that is provided that it is not too buggy. New features = new bugs I know, just keep it low and cease adding (albeit tantalising) features. This release needs, nay must, go final this weekend, or the KDE community may just start to kick up.\n\nThere are now far too many apps being released that specify 3.1 as the KDE version required, we don,t all have the resources or time to compile an entire project from CVS. \n\nThis release 'now stands upon the edge of a knife, [delay] but a little, and it may fail to the ruin of all [KDE fans]' \n\n"
    author: "Paul"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-24
    body: "Let them finish and release it when they think it's ready.\n\nSince you cannot wait, why not just compile it from a CVS snapshot?\n\nStop speaking for others, KDE will never fail, just make a better and better release!"
    author: "Jens"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-24
    body: "Apparently, many of those people releasing their apps for 3.1 think its ready. \nIt will be even more fun running all those apps on a 3.1ish-something-compiled-and-messed-it-up-myself system and reporting loads of funny bugs with the software. \nYes - I enjoy KDE. \nNope - I do not enjoy compiling it myself. "
    author: "sushi"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-24
    body: "Sorry Jens\n\nYou neither read my message in full, where I stated that like many others I have neither the time or resources to compile KDE from CVS. Also I did not in any way mean that KDE as whole will fail, that would be impossible, it is too significant and advanced for even a fool to think that would happen, I merely meant that the 3.1 release would fail to shake off the memory of all these delays and RC's. Which would be a shame for all the effort that the KDE team has put into it.\n\nFurthermore I take offense at your tone Jens, as (again if you bothered to actually READ my message !!) I stated quite clearly as I have done on a number of previous postings that I staunchly support and respect the developers, so don't come on at me as though I am disrespectful little runt.\n\nSpeaking for others - now you really have lost it, I merely stated an obvious trend from users of KDE from around the web, that they are INDEED frustrated at not knowing exactly what is going on, lack of upto date information, rumoured iminent releases that do not materialise etc .... , and in this regard I started my posting by praising Derek Kite for his exemplary work on the CVS updates as without this no one would have a sodding clue what was going on !!!!! \n\nIncidentally this whole site is about people giving their INDIVIDUAL views and slants on issues raised, I stated mine, and you could have merely countered it by saying you are happy to wait (completely devoid of any information), that would have been and is your right and I respect that. \n\nI will concede however that I could have perhaps phrased it better in respect of the community sentiment, but I know for a fact a lot of KDE fans are very frustrated. This does not infer any loss of or lack of respect for the developers themselves or the huge effort they are putting into this, and I CERTAINLY DO NOT think that I am speaking FOR the COMMUNITY, I and no one individual has that right.\n\nIn future READ tfm before firing off anoying messages of your own.\n\n*********** PS :: Maybe someone should direct visitors to the KDE 3.1 info pages to Derek's CVS upates, as I appreciate the developers are too busy to be constantly updating the web pages as well. Just a thought ****************"
    author: "Paul"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "Boring waste of bandwidth...SELF-centered and TOUCHY"
    author: "Josh"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-24
    body: "The delayed release is because of Suse not having RPMs ready I guess. What should the release manager say?\n\nHe DID say, rc7 looked good to him. Then it disappeared (!) from FTP. Probably Suse will have RPMs ready, then we can see a release.\n\nWhat should he say?\n\nSuse is not ready yet?\n\nSounds like a bad reason. So there is no official reason. Nothing. Just waiting for Suse.\n\nAnd yes, it's free Software. I have it installed, nothing is holding THAT back. But somehow, official release from a Free Software project, should not be driven THAT much from company interests, when the company is by far not the only active contributor, although large enough.\n\nI personally hope, this doesn't get out of hand.\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-24
    body: "It is ready.\nWill be released soon!\nCheck http://lists.kde.org/?l=kde-cvs&w=2&r=1&s=www&q=b and you will see many, many new comits for 3.1 release pages. (info, changelog, etc...)\n\nThis weekend I will have a new KDE.\n"
    author: "CWoelz"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-24
    body: "Kde.org is already to far...\n\nHave a look at http://www.kde.org/info/3.1.html\n->You can find links to the 3.1 src packages there... but the files are not already uploaded..."
    author: "nappsoft"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "seems they removed it again... anyways, I bet 20 bucks that 3.1 is not going to be released till Tuesday. Whos is in?"
    author: "Adrian"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "How can we know when Suse will allow it?\n\nHm, probably check the release schedule on their site....\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "Please stay away from this site. We don't want trolls here."
    author: "Anonymous"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "Thanks for the link, great to know what's being announced upfront :)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "Not sure what you're up to, but the reason that KDE 3.1 is not yet released has nothing to do with waiting for SuSE. In fact, the SuSE packages are ready, for some time already. \n\nTry better conspiracy theories next time, please. ;-)\n\n"
    author: "Dirk Mueller"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "So having that name you have, could you comment on the decision about the release?\n\nProbably not, because then you would have done just that.... or not?\n\nLets see....\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "My conspiracy theory is that the slow Debian transition to gcc-3.2 delays the KDE release and KDE 3.1 will not be released before it is able to enter Debian Sid the same moment."
    author: "Anonymous"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "LOL, wish you were right :)\n\nBut honestly, KDE 3.1 cannot enter Debian because of some licence stuff. I am sure it will be found. Maybe KGamma uses non-free colors...\n\nDebian has a tradition to have current excuses why some thing cannot happen. Just look at why Woody cannot be released, why XFree 4.2 cannot enter unstable, why KDE 3.1 cannot enter unstable.\n\nI have too often heared, that will happen soon.\n\nBUT, big difference. With Debian, I know it's because nobody knows better more than one obstacle.\n\nSo don't sweat, Debian won't have KDE 3.1 debs, maybe they will wait for 3.1.2....\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "Debian tries not to break things. They go to great lenghts to keep their massive collection of software in a usable state. And they succeed quiet well. It takes some time, but the endusers always have a decent system. If something can't enter for an extended period of time, it's usually available from some other (eg the maintainers) site. This way the software is available to those that can deal with the problems that migh occur. Because of the great apt system, mixing in packages from other sites is almost painless.\nKDE 3.1 is a good example. The Debian KDE maintainer knows there will be big problems at the end of the transition to GCC3.2, if KDE3.1 would enter now. However, KDE3.1 packages have been available from various sites, and they work great."
    author: "CAPSLOCK2000"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "KDE not in Debian b/o license? \nBullshit. \n(Yes, that argument was made in the days that QT was neither QPL nor GPL, but that seems to be ages ago.)\nJust have a look at Debian - they are just so back in time that they don't carry any new software. \n\nStefan. "
    author: "sushi"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "You know openssl e.g. ?\n\nIt's BSD with advertising which is incompatible with GPL. Meaning you can do that, but not distribute binaries.\n\nProbably Ok by many standards, not by Debian standards.\n\nI believe they have KDE on GNU TLS, some alternative that is GPL compliant.\n\nAnd that is one example why KDE 3.0 debs were said to be ready (I even installed them), but not in Debian.\n\nYou probably should read debian-legal. It's not that I don't take pride of it, but Debian does care about a lot more than others.\n\nThe upside is you are safe. The downside is that it makes up for pseudo-execuses. Like why KDE 3.0 debs couldn't enter unstable....\n\nProbably truth is, nobody wants to maintain a released KDE in Debian.... so far only one person has actually _done_ that, and he gave up around 2.2, which is where KDE unstable still stands at...\n\nMaybe one year more?\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-26
    body: "Ok, granted, openssl and KDE combination might be arguable, \nAnd having KDE w/o ssl support is taking away some features, \nbut it should not be impossible to have KDE w/o openssl. \nSo your argument would be void. \n(Or do you want me to honestly believe that KOffice, Kdevelop, Kicker, KWin, ... will absolutely not work w/o ?!)\nYour 'propable truth' reasoning is the only reasoning: \nNobody in Debian wants to do that. \nLegal arguments are just another excuse. (Read: I don't propose to break the law, or ignore licenses, but they're used only as a smoke screen in this case.)\n\nYeah, you're definitely on the right side license-wise with Debian - \nNothing goes into stable before it's so outdated that everything has been hashed out beforehand by others first.\n\nYours, Stefan. "
    author: "sushi"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "Having managed the release of software in the past,\nall I can say is that it is frought with various\nunexpected and sometimes weird problems.  Thinking\nof past experiences, I can say is that waiting\nuntil everything is right is well worth it.  KDE\nis not a simple project, and it feels to me as if\nthis is one of the largest jumps the project has\nmade to date.  I appreciate the efforts everyone\nis making, and if it takes more time than people\nwould like, well, thats life.\n\nBetter to see a good KDE 3.1 release than a crippled\none.\n\nWaiting patiently...\n\n--STeve Andre'\n\n"
    author: "STeve Andre'"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "The best guess is that the \"Tuesday is a good day\" public relation, \"more binaries are needed\" and \"release when the source is done\" sections are still busy struggling with each other."
    author: "Anonymous"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-25
    body: "\"This release 'now stands upon the edge of a knife, [delay] but a little, and it may fail to the ruin of all [KDE fans]'\"\n\nOk,that settles it.\nIf Gandalf himself is a KDE fan, then we now for sure which\nDE is the ONE. :>"
    author: "kilaeden"
  - subject: "Re: Frustration Frustration !!"
    date: 2003-01-27
    body: "I don't know about Gandalf, but this quote is from Galadriel, Lady of the Wood..."
    author: "Andr\u00e9 Somers"
  - subject: "When will KDE3.1 be released, please?"
    date: 2003-01-24
    body: "If there is to be further delay, would someone post a message somewhere, say, www.kde.org, or dot.kde.org, or lists.kde.org, or...?\n\nI'm not angry, I would just like to have further information.  It seemed like the release was going to be last weekend and the lack of information is distressing."
    author: "Anonymous"
  - subject: "kgamms is great!"
    date: 2003-01-24
    body: "I want to thanks the programmers of kgamma. They saved my eyes from the dark side of the screen.\nReally nice app."
    author: "Iuri Fiedoruk"
  - subject: "KGamma programmer"
    date: 2003-01-25
    body: "The KGamma programmer is Michael v.Ostheim. He is an extremely nice and helpful person. I just wrote to him because i had noticed a small problem with KGamma in my XFree86 setup, and he immediately responded and solved it.\nI am using KDE 3.1rc6 (love it!) in the Mandrake 9.1 Beta 2 and I think KGamma is a great addition to the KDE family.\nBTW I personally can wait for as long as the KDE programmers want. There shouldn\u00b4t be any pressure on the developer community, either to speed up things or to delay them.\nAndrew"
    author: "Andrew Balsa"
  - subject: "Delay of 3.1 release."
    date: 2003-01-25
    body: "1.- The KDE guys know best when to release the software\n\n2.- If you do not like 1. you have 2 options\n\n    2.1 - See 1. (recomended)\n    2.2 - Get it from the CVS and compile it \n\n3.- If you do not like 2.2 you have 2 options\n        \n     3.1 - See 2.1 (recomended)\n   \n         and so on and on\n\n  Sumarising:\n\n   Let's the guys who know best do the job. Stop moaning.\n\n  Rgds,\n\n  JC\n\n"
    author: "Jose C. Alvarez"
  - subject: "Re: Delay of 3.1 release."
    date: 2003-01-25
    body: "1.- The KDE guys know best when to release the software\n\nAha, so that's why there have been *six*release canidates?"
    author: "IR"
  - subject: "Re: Delay of 3.1 release."
    date: 2003-01-25
    body: "Because they were only release _candidates_, no actual releases. *shrug"
    author: "Datschge"
  - subject: "Re: Delay of 3.1 release."
    date: 2003-01-25
    body: "..yes obviously *shrug\n\nSo six times in a row eh?"
    author: "IR"
  - subject: "Re: Delay of 3.1 release."
    date: 2003-01-25
    body: "yes, 6 times in a row. a decision was made with each RC whether or not it was worthy to be called a final release. to prevent having had six RCs, two things could've happened: a less-than-great 3.1 could've been shipped that included major bugs and security flaws (would you have prefered that?), or the project simply could've kept the source away from beta testers and those who wanted a preview and not released RCs at all (would you have prefered that?). the latter strategy would've also improved the odds that all those bugs that were found and fixed would've instead made it into the final release. \n\nyes, the delay on this release has been extraordinary (~6 weeks) compared to past release schedules. however, that wait isn't going to kill anyone and it will result in a better final product. isn't that what really matters?"
    author: "Aaron J. Seigo"
  - subject: "Re: Delay of 3.1 release."
    date: 2003-01-25
    body: ">>>however, that wait isn't going to kill anyone and it will result in a better final product. isn't that what really matters?\n\nYes true. Can't wait till KDE 3.2 ;)"
    author: "IR"
  - subject: "Re: Delay of 3.1 release."
    date: 2003-01-25
    body: "release when ready seems a simple enough concept to me.\n\nYogi brera would probably tell us we are there til we are there.\"\n\nI doubt there is any single person who wants to finish the\nrelease than the release manager..... so he can start working on the \nnext one. :>\nSurely,his motivation surpasses our own.\n\nIf anyone doesn't want to compile the lastest cvs, then install Debian\nand apt-get the 3.1 debs.\nContrary to popular opinion it is not hard to install and the latest\nkde is always available.\nIt is surely just as quick as compiling cvs.\n\n\n"
    author: "krakatoa"
  - subject: "Delay is Ok, Silence is a pain"
    date: 2003-01-25
    body: "I think it is super that they dont release at the predicted release date.\nWhat I dont like is just sitting waiting without knowing why. It would be really great if Dirk or someone else made a place at the kde.org site where we could see what are causing the delay. Maybe even seing exactly what is being worked on at the moment.\n\n"
    author: "Jarl E. Gjessing"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-25
    body: "In my opinion it's not that important why the release has been delayed... I think an official statement like \"The release of KDE3.1 has been delayed for one week\" would be enough in my opinion.\n\nIt's just frustrating to go through several mailing lists every day and read 1000 different opinions... The reason of the delay would be interesting, but I don't care about this that much.\n "
    author: "Peter"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-25
    body: "\"I think an official statement like \"The release of KDE3.1 has been delayed for one week\" would be enough in my opinion.\"\n\nYeah, but this hasn't happened either.  Read what the poster said again:  \"Delay is okay, silence is a pain.\"\n"
    author: "Anonymous"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-25
    body: "The silence is only to increase the tension. :-)"
    author: "Anonymous"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-25
    body: "I think they reached the climax already."
    author: "Adrian"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-25
    body: "No, it passed already...."
    author: "Debian User"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-26
    body: "> Yeah, but this hasn't happened either.\n> Read what the poster said again: \"Delay is okay, silence is a pain.\"\nUps, sorry - I think my second paragraph has been written quite confusing (english is not my native language ;-).  I totally agree with the previous poster and to \"Delay is okay, silence is pain.\" - I just wanted to say that an official statement would avoid users digging through a lot of mailing-lists with a 1000 different opinions about the release date. I didn't have the intension to offend somebody :-)"
    author: "Peter"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-26
    body: "\"I didn't have the intension to offend somebody :-)\"\n\nYou didn't.  Sorry for being snappish.  That's the trouble with this text-only communication - there are no visual cues to see if somebody is being snotty or not.  :-)   My apologies.\n\n"
    author: "Anonymous"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-26
    body: "\"I didn't have the intension to offend somebody :-)\"\n\nYou didn't offend.  I apologize for sounding snappish.  That's the trouble with this text-only communication - there are no visual cues to see if somebody is being snotty or not.  :-)   My apologies.\n\n"
    author: "Anonymous"
  - subject: "Daily release digest"
    date: 2003-01-25
    body: "Something similar to this weekly CVS digest, a daily release digest\n(consisting of just a few sentences and an up-to-date buglist)\nduring the last weeks before a release would certainly calm many\nminds. When people see what bugs are still there, they know what\nthey're waiting for and would probably even demand the release to\nbe kept off until they are fixed.\nAnd many, like me, probably just want a site they can surf to every\nday that makes them feel the release is getting closer :)\n\nOne can but dream...\n "
    author: "Mike"
  - subject: "Re: Daily release digest"
    date: 2003-01-26
    body: "> Something similar to this weekly CVS digest, a daily release digest\n\nSubscribe to apps.kde.com's apps-announce mailing list with activated digest mode. Please don't follow the bad example of http://gnomedesktop.org and post every peanut release on the dot."
    author: "Anonymous"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-26
    body: "Current plans aim for a release early in the coming week. Binary packages for a good number of distros have been coming in for the last few days and it is mostly a matter of finishing up the announcement and fixing up some server issues so that everyone will be able to smoothly download binary packages.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-26
    body: "thank you, Waldo Bastian.\n& all kde developers."
    author: "krakatoa"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-27
    body: "I've downloaded the packages a few hours ago... KDE 3.1 is already installed... just browse ftp..."
    author: "nappsoft"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-27
    body: "Any idea when RedHat 8.0 RPMs would be posted here?\n\nandy"
    author: "Andy"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-27
    body: "Now that Bero is gone, maybe nowhere.\nUse the next phoebe beta..."
    author: "CWoelz"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-28
    body: "RedHat, aka the Rhat ?\n\nAsk Mosfet, Bero or Tim Butler."
    author: "kdwang"
  - subject: "Re: Delay is Ok, Silence is a pain"
    date: 2003-01-28
    body: "I agree that the KDE team should take whatever measures are necessary to ensure a quality release, but there is a price to pay for these delays, even in an open source project.  I don't have much time to assist with development or detailed bug-reporting.  My small contribution is through advocacy.  I often recommend open source software to my clients, and get them to purchase a support from the project maintainers, if available.  Even if my team has to handle the support, I'll make a donation to software that I deploy.  I've been very impressed with the KDE 3.1 betas and release candidates, and have been trying to hype it up since November: \"Amazing  desktop environment coming out any day now.\"  With all the non-showstoppers being backported to the 3.1 branch, lately all I can say is, \"Sorry, I have no idea when.\"  The response I get most often is, \"I know you like this Linux thing, but at least we know what we're getting ourselves into with Windows.\"  One of the MCSEs that I work with setup an email autoresponder while he was on vacation that said, \"The SuSE packages have been ready for a week.\"  That was funny in December."
    author: "Anand"
---
The latest KDE CVS Digest is <A href="http://members.shaw.ca/dkite/jan242003.html">now available</a>.  In this week's news, Kaplan is reborn as <a href="http://www.kontact.org/">Kontact</a> (a <a href="http://pim.kde.org/">personal information management</a> application for KDE that integrates <a href="http://kmail.kde.org/">KMail</a>, KAddressBook, <a href="http://korganizer.kde.org/">KOrganizer</a> and other applications), KMail is moved, and a new <a href="http://www.imc.org/pdi/vcardwhite.html">VCard</a> parser makes an appearance.  Also read about
<a href="http://www.konqueror.org/">KHTML</a>'s continued improvements, thanks to the <a href="http://www.apple.com/safari/">Apple Safari</a> contributions, and improvements in the <a href="http://www.koffice.org/">KOffice</a> filters.  A number of new applications were also added to the CVS repository.
<!--break-->
<ul>
<li>
<a href="http://kwifimanager.sourceforge.net/">WiFi</a>, from kwirelesstools by 
Stefan Winter, was added to kdenetwork. With the application KWiFiManager you can configure 
and monitor your wireless LAN PC-Cards under Linux/KDE. 
<li><a href="http://kgamma.berlios.de/index2.php">KGamma</a> 
is a KDE Control Center module for gamma calibration/correction of XFree86. With proper gamma 
settings, your display (websites, images, etc.) will look the same on your monitor as on other 
monitors. 
<li><a href="http://www.schmi-dt.de/kmouth/index.en.html">KMouth</a> is a KDE program 
which enables persons that cannot speak to let their computer speak. 
<li><a href="http://accessibility.kde.org/aids/kmousetool.html">KMouseTool</a> is a KDE program 
that clicks the mouse for you.
</ul>

