---
title: "KDE-CVS-Digest for May 9, 2003"
date:    2003-05-09
authors:
  - "dkite"
slug:    kde-cvs-digest-may-9-2003
comments:
  - subject: "Thanks"
    date: 2003-05-09
    body: "I just want to thank Derek for the work that he puts into the cvs digest. It is the highlight of my week reading it (usually on a saturday morning cos I'm in South Africa so I'm GMT +2). Anyway thanks a lot. And yeah I can't believe there weren't any posts before me today. =)\n\nLuke"
    author: "luke randall"
  - subject: "Re: Thanks"
    date: 2003-05-10
    body: "Yes, today you're the first one. We see. I think it is better that you didn't give a chance to the being-first-mads.\nOh, of course they were also the first ones between millions of sperma and this should make them happy and happy and happy. "
    author: "New Era"
  - subject: "Re: Thanks"
    date: 2003-05-10
    body: "eh eh eh good one :)\n> Oh, of course they were also the first ones between millions of sperma and this should make them happy and happy and happy. \nand yes thank's for the digest it really saves the day - again.\ncya zero08"
    author: "zero08"
  - subject: "Re: Thanks"
    date: 2003-05-11
    body: "'It is the highlight of my week reading it...'\n\nReading CVS digests is highlight of your week reading? How boring your life must be ;-)"
    author: "anon"
  - subject: "Re: Thanks"
    date: 2003-05-13
    body: "Logical deduction ..\n\nReading the CVS digest is the highlight of his week.\nLife is lived through weeks.\n\nConclusion: Reading the CVS digest is the highlight of his life!!"
    author: "feedTheTroll"
  - subject: "Thanks"
    date: 2003-05-09
    body: "I haven't thanked Derek before, but I will now. I read it compulsively every week and contributes towards that Friday feeling. Thanks for all the work you do it's much appreciated!"
    author: "MxCl"
  - subject: "Re: Thanks"
    date: 2003-05-10
    body: "hey, i get the same Friday feeling too with the CVS digest!!! Nice to know there are people as geeky as me, probably more so. :-)\n\nAnyways, thanks Derek.  I have never said thanks before, and have been really only a heavy kde user since Dec 2002. It's so awesome :-)"
    author: "*nixnewbie"
  - subject: "Kile"
    date: 2003-05-09
    body: "> Lubo\u009a Lu&#328;\u00e1k committed a change to kdeextragear-2/kile\n\n\nKile in kde-cvs? great news! i've been waiting for an update for one of my favorite apps. thx!"
    author: "lite"
  - subject: "Re: Kile"
    date: 2003-05-10
    body: "Hope next step will be including lyx with the Qt frontend.... and .... a merge? Uhmmmm.... kile's great interface and flexibility+ lyx's wysiwyg style....That could be quite interesting ;-)\n\nWell, first, lyx, Qt, XFree, the distros or whoever is responsible should have all those font issues fixed. I had to install the ttf fonts and the poor konqui shows some webpages in greek now. Luckily I don't like those pages :-D"
    author: "uga"
  - subject: "Re: Kile"
    date: 2003-05-11
    body: "http://kile.sourceforge.net"
    author: "KDE User"
  - subject: "Re: Kile"
    date: 2003-05-12
    body: "If you mean that I should help with it.... I'm in trouble. I'd like to, since I like the tool a lot, but I just started a new project, and I don't think I have time to support the two of them when I have already enough with one:\n\nhttp://www.sourceforge.net/projects/krecipes\n\n"
    author: "uga"
  - subject: "THANKS A LOT!"
    date: 2003-05-09
    body: "This is the high light of all my fridays =)"
    author: "Alex"
  - subject: "Re: THANKS A LOT!"
    date: 2003-05-11
    body: "dang that sucks"
    author: "standsolid"
  - subject: "Beep, beep, emergency..."
    date: 2003-05-10
    body: "Beep, there was a oGALAXYo in my view sometimes. Beep, I can't get any signals from that oGALAXYo for a long time. Beep, is there a problem with my space mirrors and lenses or that strange oGALAXYo disappeared from the KDE universe?\nWho knows, that oGALAXYo may be gone to another universe to code with his lovely :) GTK+.\nYes he is a fan of KDE, even a programmer, but in KDE CVS nobody can see his name. (I am glancing, I couldn't ) Because he has no free time, he continues programming with GTK+.\nA few comments to Ali, exGNOME user (?), oGALAXYo:\nAyinesi istir kisinin lafa bakilmaz, sahsin gorunur rutbe-i akli eserinde. (Ziya Pasa)\nBizim kumesten yem yiyip komsunun kumesine yumurtlamak...\n"
    author: "oHUBBLEo"
  - subject: "Re: Beep, beep, emergency..."
    date: 2003-05-10
    body: "Why don't you simply do a google search if you miss him so much? Trolling was never his purpose so why should he continue writing here and at other places if everything he writes is perceived as only that, trolling?"
    author: "Datschge"
  - subject: "Re: Beep, beep, emergency..."
    date: 2003-05-10
    body: "I was only kidding. I really want to see his comments. He was writing the longest replies and after that big arguments were starting.\nBut I really wonder and with my special comments I tried to learn that if he is a KDE hacker? Because he seems to be a good (perhaps advanced) programmer.\n"
    author: "oHUBBLEo"
  - subject: "Re: Beep, beep, emergency..."
    date: 2003-05-10
    body: "No, he's (still) a Gnome hacker while using KDE on his desktop as well sometimes. As I said do a google search for him. =P"
    author: "Datschge"
  - subject: "Re: Beep, beep, emergency..."
    date: 2003-05-17
    body: "He was never a Gnome hacker. If you want to find him just look at anon and KDE User below."
    author: "A"
  - subject: "Re: Beep, beep, emergency..."
    date: 2003-05-17
    body: "personally he was annoying and i'm quite glad to see that \nhe's no longer posting. several bug reports from him were \nso arrogant and self-righteous that i felt on occasions like \nsimply removing them, even though there was a partial truth \nto them :(\n\nand no, he wasn't a kde hacker either.\n\nmvg,\nAlex"
    author: "Alex"
  - subject: "Re: Beep, beep, emergency..."
    date: 2003-05-10
    body: "What was that? Didn't anybody install spamassassin in dot.kde? ;-)"
    author: "uga"
  - subject: "OT, but"
    date: 2003-05-10
    body: "http://www.gnomedesktop.com/article.php?sid=1112&mode=thread&order=0 (changes JUST from the latest snapshot!!!)\n\nI think GNOME is actually progressing faster than KDE after looking through the change logs. I really wish KDE would take GNOME's approach and put usabiltiy first and features second. KDE also needs to attract more new developers, by improving the base technologies, having Qt integration and GREATELY improving that POS documentation which is now in the KDE developers corner. Almost everything there is obsolete or poorly/insufficently explained, not many people will want to develop for KDE with THAT documentation. I think its as important as the tools themeselves, what good are tools if you don't know how to use them.\n\nCheck out KDE-LOOK and check out ART.GNOME.org its a perfect example of the 2 DEs. KDE LOok relies on more advanced technology, yet ART.GNOME.org is more usable because the entries are hand picked for one thing.\n\nUsability and lack of good documentationn for developers and users is what drags it down the most. For example check out the innovative search utility in the latest GNOME snapshot and compare it to KDE's CVS search tool. GNOME's is much nicer and more elegant IMO. \n\nBTW: Having daily spanshots is a great idea, KDE should do this too like every 2 weeks, that would keep the community more involved. Also speedwise this latest Snapsot is actually faster than KDE CVS on my computer. (also the dot.kde newsfeed on KDE-LOok and kde.org seems to be broken.\n"
    author: "mario"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "> I think GNOME is actually progressing faster than KDE after looking through the change logs.\n\nStrip the description, documentation and translation sections over there and compare it with the *weekly* CVS-Digests here.\n\n> I really wish KDE would take GNOME's approach and put usabiltiy first and features second.\n\nI don't see \"usability\" mentioned once in the log you refer to but a lot of new features.\n\n"
    author: "Anonymous"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "Forget the second, grepping for \"HIG\" shows one (and not more) person working on it."
    author: "Anonymous"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "<i>KDE also needs to attract more new developers, by improving the base technologies, having Qt integration and GREATELY improving that POS documentation which is now in the KDE developers corner</i>\n\nWhich KDE project are you describing?  The one I know does not fit this description at all.\n\n+ KDE needs to attract new developers:  I agree that lots of developers are a good thing ,but we already do have a lot.  Sure more would be nice, but why do we NEED to attract more?  Don't forget the fundamental difference between KDE and GNOME: KDE is primarily developed by a volunteer community; GNOME is primarily developed by employees of linux companies, as part of their jobs.  So to say KDE NEEDS to do anything is kind of weird.\n\n+ KDE needs to imporve the base technologies. uh, that's a joke, right?  KParts, DCOP, C++, etc...\n\n+ Qt integration.  It must be another joke.  Since you are from GNOME, I'll explain something about C++ to you called class inheritance.  It's a great way to re-use code and extend the functionality of existing classes/widgets.  KDE makes prolific use of Qt's excellent libraries by inheriting from Qt classes to make KDE classes with extra functionality.  Qt is everywhere in KDE, they could not possibly be more tightly integrated.\n\n+ Poor API documentation.  Well, maybe for very new classes, the docs haven't caught up yet.  That's what happens when development is so fast, I guess :)\n\n\n<i>Check out KDE-LOOK and check out ART.GNOME.org its a perfect example of the 2 DEs. KDE LOok relies on more advanced technology, yet ART.GNOME.org is more usable because the entries are hand picked for one thing.</i>\n\nTo me, this difference nicely illustrated fundamental contrasts between KDE and GNOME.  Your version of kde-look has <i>hand-picked</i> entries, whereas anyone can submit something to kde-look and then the community votes on them.  It's a really great example of our two communities.  Have fun in the Cathedral, mario; I prefer the bazaar!\n\n \n"
    author: "LMCBoy"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: ">Since you are from GNOME, I'll explain something...\nOh boy! In that dot news, if anybody writes about GNOME then he is perceived as a GNOME fan.\nI never use GNOME, I really like KDE; but I beleive that GNOME is changing faster. Also some GNOME applications are really good. Do we have project manager like MrProject,  or a GNOME meeting etc?"
    author: "New Era"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "> Do we have project manager like MrProject, or a GNOME meeting etc?\n\nThere are http://www.koffice.org/kplato/ and http://www.wirlab.net/kphone/ projects."
    author: "Anonymous"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "1. KPlato? Do you know the status of it? \nhttp://www.koffice.org/kplato/current.phtml\nLook at it. It's Last modified August 12 2002 23:53:49\nWill it be ready in KOffice 1.3? Simply \"NO\".\n2. KPhone? Visit the http://www.wirlab.net/kphone/changes-3.1.html (The latest version) There is:\n{\nImportant note!\n Video call has been successfully tested only with certain Debian\n distributions. Thus the video call support should be considered\n only partial at the moment. You need to download the VIC software\n separately (look at the INSTALL file in KPhone package) and use \n the scripts provided to compile and install it.\n Any resources with the video call development are welcome ...\n}\nBut GNOME Meeting is really stable. Good luck for KPhone.\n\n\n"
    author: "New Era"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "KPlato is slowly but steadily developed (http://lists.kde.org/?l=kde-cvs&w=2&r=1&s=kplato&q=b). In my impression MrProject development has slowed down recently. And has GnomeMeeting good codes? I read \"no\". So stable, but not good. May I in return ask you for a mature Gnome presentation program or TeX editor?"
    author: "Anonymous"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "Ooops, I'm not Gnomist.:) I only like some Gnome programs. \n>good codes? \nI'm a user. For me usability and stability is important. Not code. I can easily use it and I like it.\nIf KPlato comes with KOffice and works good, of course I will use it. Because it will be integrated with other KOffice programs. But not now!\n>mature Gnome presentation program?\nOk, everyone knows that Gnome doesn't have good office applications. Gnumeric is a bit good but not as KSpread. \n\n"
    author: "New Era"
  - subject: "Re: OT, but"
    date: 2003-05-20
    body: "What do you mean by \"good codes\"? Do you mean good codecs? or good C code?\nI'm interested to know where you read the code was not good, if you are talking about it of course."
    author: "Me"
  - subject: "Re: OT, but"
    date: 2003-05-20
    body: "I wanted to write \"codecs\"."
    author: "Anonymous"
  - subject: "Re: OT, but"
    date: 2003-05-20
    body: "GnomeMeeting has the best codecs available in the Open Source world... right, it doesn't provide patented codecs, but there is nothing they can do about that except going into illegality or selling plugins."
    author: "Me"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "If you are not a gnomist, why are you making excuses for mario's post that has been refuted point by point by LMCBoy?  And then you make accusations that everyone thinks you are gnomist?  Sad."
    author: "anon"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: ">If you are not a gnomist, why are you making excuses for mario's post...\nYou proved my idea :\"Oh boy! In that dot news, if anybody writes about GNOME then he is perceived as a GNOME fan.\" Thanks ;)\nOf course I don't make accusations. \n> LMCBoy? I don't know the meaning of it.\n"
    author: "New Era"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "Then read it and think about, then maybe you will know the meaning of it.  Instead of posting trolls here because you don't know the meaning..."
    author: "anon"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "Wow, you're very aggressive.\nI lately saw that LMCBoy is his name.\nDon't get angry we're only talking. "
    author: "New Era"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "I only hope you now realise that your words that GNOME is progressing faster than KDE was a troll."
    author: "anon"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "OK. Bye."
    author: "New Era"
  - subject: "Also"
    date: 2003-05-11
    body: "I was not trolling, by mentioning that I THINK GNOME is now being developed faster than KDE. Its just what I'm seeing from the snapshots, all the changes I showed you on that link are only from the previous snapshot which is released daily.  I was bringing it to your attention, it seems only a mention of GNOME will summon calls of \"troll\" here."
    author: "mario"
  - subject: "Re: Also"
    date: 2003-05-11
    body: "You are trolling because the snapshot is not released daily, and not even weekly as mentioned.  When you are objective and compare to KDE, GNOME progress seems quite slow."
    author: "anon"
  - subject: "Re: OT, but"
    date: 2003-05-11
    body: "Ok, what I was describing was the integration between Qt applications like Opera or Quanta Gold (I know our Quanta is better) and KDE. Qt applications are just as integrated in KDE as they are in GNOME. For example, they don't even use KDE's awesome file dialog. IMO, Trolltech really needs to fix this. \n\nLook, its always good to attract more developers and tehre will always be a need for more skilled developers, by \"needs\" I'm just saying waht IMO would be good for KDE.\n\nNo that was not a joke, KDE should improve the base technologies, while tehy are excellent and probably some of the best on Linux, there are always things to improve. They can be optimized, made more intuitive to use, include BETTER DOCUMENTATION and so on. Now, just because KDe already has good technology don't become so arrogant and pretend its perfect, or competing technologies will catch up while you have your head high in the clouds.\n\nYou also don't seem to understand my point. KDE-look has an almost useless voting systema llowing you to vote only good or bad and also vote many times, it can be easily abused. What I was trying to say is that GNOME development seems to be more disciplined, and while not always on the bleeding edge it generally implements what it has better for the average user. \n\nYou really have to understand that I want the best for KDE, and being arrogant like that will not help anyone. You may pretned nothign I mentioned exists, but it will only hurt KDE in the long run.\n\nBTW: This is the poor obsolte documentation i was reffering to http://developer.kde.org/documentation/index.html\n"
    author: "mario"
  - subject: "Re: OT, but"
    date: 2003-05-11
    body: "> BTW: This is the poor obsolte documentation i was reffering to http://developer.kde.org/documentation/index.html\n\nYou are very welcome to update it.\n\n "
    author: "Andr\u00e9 Somers"
  - subject: "What are you talking about!"
    date: 2003-05-11
    body: "\"You are very welcome to update it!\"\n\nYou think anyone has the skills to do that. I don't know KDE's apis or Kparts, or DCOP or even Qt. I'm just starting to learn C++ and I was curiously looking through it and noticed it was obsolete. \n\nI'm glad not every KDE user is so arrogant and completely discards criticism.\n"
    author: "mario"
  - subject: "Re: What are you talking about!"
    date: 2003-05-11
    body: "KDE is a true community effort, there is no part where you are being excluded if you want to contribute. What you do now is pointing to some generic pages and shouting \"it's outdated\", it should be obvious even to you that nobody is knowing what you are exactly talking about. So you are welcome to update it yourself since only you yourself can know what you yourself think is outdated.\n\nAgain: You are very welcome to update it, correct it yourself or point others to the parts you are sure are outdated now so they can correct it. Usually this all is only a matter of minutes, no reason at all to go over the top and talking about \"arrogant\" and \"completely discards criticism\"."
    author: "Datschge"
  - subject: "Re: What are you talking about!"
    date: 2003-05-12
    body: "No, I don't think anybody has the skills to do that. However, noone has the skills to know what documentation exaclty you are talking about that is supposed to be outdated. What's more, since you are seemingly just starting to learn your way into C++, KDE and Qt, how would you know that it is outdated at all? The fact that a technology was introduced back in the 2.x days doesn't mean that it isn't used anymore. This is certainly true of stuff like KParts and DCOP. If the principles are still the same, why should the docs change?\nAlso, there are still people working on the 2.x series, or at least making sure their software will compile and run on it. Is it so strange to have the docs still available for them?\n\nIf you really find an error in the docs, please report it as a bug if you can't fix it yourself. complaining about it here probably won't do any good."
    author: "Andr\u00e9 Somers"
  - subject: "Re: What are you talking about!"
    date: 2003-05-12
    body: "No, I don't think anybody has the skills to do that. However, noone has the skills to know what documentation exaclty you are talking about that is supposed to be outdated. What's more, since you are seemingly just starting to learn your way into C++, KDE and Qt, how would you know that it is outdated at all? The fact that a technology was introduced back in the 2.x days doesn't mean that it isn't used anymore. This is certainly true of stuff like KParts and DCOP. If the principles are still the same, why should the docs change?\nAlso, there are still people working on the 2.x series, or at least making sure their software will compile and run on it. Is it so strange to have the docs still available for them?\n\nIf you really find an error in the docs, please report it as a bug if you can't fix it yourself. complaining about it here probably won't do any good.\n\nOh, and by the way, please refrain from calling names. I don't like to be called arrogant on a public forum like this. You seem to pretend you know a whole lot about me. You don't."
    author: "Andr\u00e9 Somers"
  - subject: "Re: OT, but"
    date: 2003-05-11
    body: "<i>No that was not a joke, KDE should improve the base technologies, while tehy are excellent and probably some of the best on Linux, there are always things to improve. </i>\n\nWhat do you think developers are doing all the time?\nEven if KDE development would be slower than Gnomes (which I dont believe), complaining wont help getting  more work done. Helping does."
    author: "AC"
  - subject: "Re: OT, but"
    date: 2003-05-11
    body: ">Ok, what I was describing was the integration between Qt applications like Opera or Quanta\n>Gold (I know our Quanta is better) and KDE. Qt applications are just as integrated in KDE as\n>they are in GNOME. For example, they don't even use KDE's awesome file dialog. IMO,\n>Trolltech really needs to fix this. \n \nAnd how do you think that should work? It's a cyclic dependancy (KDE requires Qt requires KDE...), kinda the chicken-egg problem. We had that with KDE support in designer back in KDE 2.x times and it was a pain in the butt. Again: it's non-trivial to get that right. Yet we will hopefully be able to address that in Nove Hrady, see http://mail.kde.org/pipermail/novehrady/2003-April/000048.html.\n\nCheers,\n  Daniel\n"
    author: "Daniel Molkentin"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "> http://www.gnomedesktop.com/article.php?sid=1112&mode=thread&order=0 (changes JUST from the latest snapshot!!!)\n\nUh.. the time period for that snapshot far eclipses the time period for this week's CVS digest. \n\n> Almost everything there is obsolete or poorly/insufficently explained, not many people will want to develop for KDE with THAT documentation\n\nWell, I've been writing an application recently that had both gtk and qt frontends (with optional KDE and GNOME intregration support.) I'll have to agree with you that there is a lot of obsolete information in developer.kde.org. Some of it was written during the KDE 2.x era, and is simply not applicable now. Hmm.. perhaps I should open up a few bugs.kde.org reports :-)\n\nOn the other hand, most developers will primarily use the API reference. In terms of this, the KDE API reference still covers more of its API than GNOM does with its. I also still like the Qt docs better than the Gtk+ reference guide.\n\n\n> Having daily spanshots is a great idea, KDE should do this too like every 2 weeks, that would keep the community more involved\n\nWell, KDE used to have these kinds of \"development-branch\" releases back before KDE 2.0. It was well... extraneous. Most people used CVS anyways, because once snapshots were released, things were quickly changed in CVS (keep in mind that the CVS digest only covers some of what goes on in KDE cvs.. you'll have to follow the kde-cvs mailing list for it all..) This made many bug reports from development snapshots useless. "
    author: "fault"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "Often easier than writing bug reports about documentation is to fix it directly. AFAIK everybody with CVS access can edit files on developer.kde.org, its content is in the CVS module of the same name. If you dont have a CVS account, drop by on #kde-devel and ask somebody to apply a patch.\nMost 2.x documentation is not obsolete though. But somebody must change the version number...\n"
    author: "Tim Jansen"
  - subject: "Re: OT, but"
    date: 2003-05-10
    body: "Aha, I forgot that developer.kde.org is in a seperate module from kde-www, which is controlled by ACL's. Perhaps this'll give me a reason to use my kde-cvs account for the first time in about a year :)"
    author: "fault"
  - subject: "A few points from a real GNOME fan ;-)"
    date: 2003-05-11
    body: "Those snapshots aren't daily and not weekly either, they are more like unstable development releases.\nIt might be that GNOME is progressing faster at the moment but you have to set this in relation with KDE beeing much more \"mature\" (as in finished) already. When the KDE 2/3 platform was new and fresh, KDE progress was insanely fast. :)\nAnd while I like GNOME much better (usability, looks and everything :)) and think that a few GNOME applications are really more advanced, there is no denying that the KDE desktop is more complete at this point."
    author: "Spark"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-11
    body: "Given that these snapshots aren't even weekly, it doesn't look like GNOME is progressing fast at all compared to KDE."
    author: "anon"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-11
    body: "Well comparing changelogs and CVS commits isn't objective either."
    author: "Spark"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-11
    body: "They are daily, there has been more than one and it is released after a specific amount of time. The changes in the latest snapshot seem to be almost as big as KDE 3.1.2 to CVS now.\n\nAnyway, like I said that was my opinion, don't take it like a fact."
    author: "mario"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-11
    body: "The changes in the latest snapshot are bigger than the changes from KDE 3.1 to KDE CVS?? What are you talking about?  From the link you have provided this is clearly not true.  Have you even read it?  It's mostly a bunch bugfixes and translations.  \n\nDo you want to see the new and improved apps KDE has had since KDE 3.1? Look at http://apps.kde.com/ and you will be amazed."
    author: "anon"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-11
    body: "I was reffering only to the changes from KDE 3.1.x to the current CVS now which does not include applications outside of the KDE package you get by default.\n\nIn addition, I meant to say that the combined changes from the last two GNOME snapshos seem to be greater than the ones KDE has.\n\nCheck it out: \n\nhttp://gnomedesktop.org/article.php?sid=1046\n\nhttp://gnomedesktop.org/article.php?sid=1112\n\nIn addition, by 2.6 GNOME will also have a lot fo new and improved applications. Most, applications will also be GTk 2 and a substantial amount will be HIG compliant. \n\nHere are a few examples:\n\nGaleon 2\nGIMP 1.4\nAbiword 2\nEvolution 2\nRed Carpet 2\nGNUCash 2\nPan GTK 2\nSodipodi GTK 2\nAnjuta GTK 2\n\nand that doesen't include commercial applications like Moho and only scratches the surface. As you cans ee, by KDE 3.2 and GNOME 2.6 KDE might not be ahead anymore, especially if it doesen't catch up on usability.\n\n\n\n"
    author: "mario"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-12
    body: "It took till GNOME 2.****6**** for these GNOME1 applications to be ported to GNOME2 and you are using this as proof that GNOME is progressing faster than KDE?  That's just trolling or pure ignorance IMHO.\n"
    author: "anon"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-12
    body: "The problem with Gnome, and the main reason for me to start using KDE instead was the fact that all Gnome-programs are terribly overestimated when it comes to completeness/stability. They write things like: \"Oh this is _THE_ blah blah app for Linux\" when in fact it is far from usable. Oh, GIMP is one app that _IS_ working good, but then again, it's not a Gnome app, maybe that's why it delivers what they promise on webpages.\n\nMaybe this has changed but I hardly believe so. When using RedHat 8 it just looks as if they removed everything that didn't work. And what is left? Well, not much to be honest. They can say whatever they want about usability, because they don't really seem to know what they're talking about, that's what happens when hackers do usability."
    author: "KDE User"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-12
    body: "Be honest, you never looked at KDE's current CVS, did you?"
    author: "Datschge"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-14
    body: "Actually a mature project would have a smaller changelog, no?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-06-03
    body: "OMG OMG OMG, this whole thread is the most pathetic thing I've seen in a long time."
    author: "Anonymous Enthusiast"
  - subject: "Re: A few points from a real GNOME fan ;-)"
    date: 2003-05-12
    body: "Granted the GNOME change logs are more detailled than KDE's because of GNOME's structure with many packages and many responsible maintainers and the more cathedral-like development. KDE even fails to produce complete bugfix change logs from e.g. 3.1.1 to 3.1.2."
    author: "Anonymous"
  - subject: "DOCUMENTATION IS THE WORST PART OF OSS (IMO)"
    date: 2003-05-12
    body: "Well than it amy very well be that KDE's development is faster, but if there ar eno detailed change logs, who would know or who could back up their claims. IMO things such as changelogs are almost as important as the changes themeselves. If I don't know what changed I will assumue that the annoying bug I hate is still ther and  might use something else. THis goes for documentation too, IMo even if you have the best tools in the world, if the documentation sucks and no one knows how to use them what good is it.\n\nAlso, some of you mention that GNOME has companies working for it as a bad thing. Since that happened usability is actually good in GNOME and so is acessibility.\n\nCheck this out ona cessibilty: http://gnomedesktop.org/article.php?sid=1111&mode=thread&order=0&thold=1\n\nBTW: I do not know how to update the documentation ro have the skills to do so, its just that I looked throuhgh it a bit and noticed it was mostly from 2000-2 or older."
    author: "mario"
  - subject: "Re: DOCUMENTATION IS THE WORST PART OF OSS (IMO)"
    date: 2003-05-12
    body: "For KDE the places to look for\n*code changes and additions: http://lists.kde.org/?l=kde-cvs\n*bugs and done bugfixes: http://bugs.kde.org/\n*missing and done translations: http://i18n.kde.org/\n\nEverything else are mostly summarizings and overviews which are necessarily omitting a lot of info so people can and will actually read it and possibly even enjoy doing so.\n\nAnd the date when a documentation has been updated the last time doesn't say anything about how accurate the information still is. (In times immemorial API stability was a great thing, heh.)"
    author: "Datschge"
  - subject: "Re: DOCUMENTATION IS THE WORST PART OF OSS (IMO)"
    date: 2003-05-12
    body: ">>Well than it amy very well be that KDE's development is faster, but if there ar eno detailed change logs, who would know or who could back up their claims. IMO things such as changelogs are almost as important as the changes themeselves.<<\n\nHave you seen any changelog for commercial DEs like Windows or MacOS? \n\nBeside that, IMO the length of the Gnome changelog shows how slow their progress it. KDE's changelog would be *much* longer. Check out kde-cvs, there are up to 10000 changes per month (on everything, including documentation and the web sites, and also backports). Realistically you would get maybe 500 changelog entries every month if you document each minor change, like Gnome does, and 5000 for a release. Would you really want to read through such a monster?\n\n"
    author: "AC"
  - subject: "Re: Let the file dialogs tell the tale"
    date: 2003-05-14
    body: "When talking about being 'User Friendly', take a look at the GTK2/Gnome filedialog v.s. KDEs. \n\nhttp://reviewed.homelinux.org/screenshots/kde-vs-gtk2/no/gtk2-filedialog.jpg.index-no.html\nv.s. \nhttp://reviewed.homelinux.org/screenshots/kde-vs-gtk2/no/kde-quanta-filedialog.jpg.index-no.html\n\nIt is obvious what dialog box works better. It really bugs me\u00a0when using a GTK2 application (which, in fact, quite often are more powerfull and developed. GTK2 ownez, look at Gnumeric, Gimp, etc) is that many simple, shared parts actually are quite useless. In short, then suck bigtime.\n\nKDE lacks the power-apps GTK2 has. But the framework is much more amazeing. (And I see no advantage at all of running these from Gnome v.s. running them through my favourite desktop: KDE)"
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Gnome is the best ..."
    date: 2003-05-16
    body: "Now uninstall mozilla from your system .."
    author: "anonymous ..."
  - subject: "Progress maybe shortchanging userability"
    date: 2003-05-11
    body: "I realize KDE volunteers are more attracted to \"cool\" features (since the pay is so high:) and I think the KDE team is doing some really great stuff, even setting the standards for new and future programming techniques (that's why I read this page),  but I'm afraid some really great work might go wasted if some basic userability isn't incorporated more.\n\nFor example, how about a survey page for what features are really most needed or desired by users?  How about a feedback page like this for each major component - if I'm trying to use KNode and it doesn't meet my needs where do I post something? (and please, not mailing lists - which most regular business people don't want to deal with)\n\nFor many of us business types, KDE-PIM would be a critical app but it needs lots of fixes to be ready for prime-time or even for many of us leading edge types to use.  For instance, the address book main interface: only 3 phone nos are listed but for normal business use 4 are really needed: office, fax, cell, home.  And in the main view side panel (which is a very nice layout) phone nos. are constantly cut-off so that the dialog view is totally useless.  And since many of us are Palm users, the Palm synching is still badly, badly  broken (my latest attempt to use the synch in 3.1.1 under Suse 8.2 did okay I think with addresses, but only copied some of the notes, would not synch the to-dos or date appointments.  This is mission critical stuff for business people - less is okay but almost on a lot doesn't cut it.  In short, in our business we cannot rely on this yet which is too bad because we have to turn to other alternatives (I still prefer us to use KMail instead of Evolution but....) - So, how can we get more userability into the process ?\n\n"
    author: "john"
  - subject: "Re: Progress maybe shortchanging userability"
    date: 2003-05-11
    body: "Hi John. Welcome to open source software.\n\n> For example, how about a survey page for what features are really most needed or desired by users? How about a feedback page like this for each major component - if I'm trying to use KNode and it doesn't meet my needs where do I post something? (and please, not mailing lists - which most regular business people don't want to deal with)\n \nHow about stepping back and getting in touch with reality? Open source software is about scratching itches. If you have an itch not being scratched you can scratch it or pay someone to scratch it for you. I do both. I lead a project and I *know* what I want for features. I don't really need to be told, but that doesn't matter because I get piles of requests anyway. I guess I asked for it because we have a feature request page. Of all the requests I get the vast majority of them I've long since thought of. My limitation is not my imagination... it's the amount of people coding for the amount of hours they have to do it with. \n\nSo let's look at reality. Even though I have little time to code and do very little of the coding on our project I still answer my emails and work on various aspects of the project... organizing, program specifications, resolving developer questions and focus... I typically spend my first few hours of the day in email. So I have to say something. I also own a business and I've done professional web development on and off since the mid 90s. I would wager my time is worth as much as most people in your business circles. So I really get tweaked by your comments about most business people not wanting to use mailing lists. \n\nWhy should I waste my time answering the same damn question two dozen times when I can do it once or reference the archives? I defy you to follow our list and then show me *one* commerical program with as quick a response time, as a solid of service and as accomidating of people as we have. We respond often wthin minutes or hours, or one of our knowlegable users responds. It's rare anything on our list is unanswered after 12 hours. When it comes to features, we had one user contribute less than the cost of a competative web tool and then request a feature. (code shortcuts) We implemented it for him in less than 3 hours instructing him where to download a version with his requested feature.\n\nMaybe a lot of business people need to change the way they look at things? It seems to me that if you are interested in productive results that would be what you would focus on. While we have limitations we still outshine most commercial programs in many areas.\n\nBTW we measure our progress in how productive users can be with Quanta so you could say that our conerstone is usability. "
    author: "Eric Laffoon"
  - subject: "I agree with you completely"
    date: 2003-05-11
    body: "I also wanted to take this opportunity to say that Kitty Hooch really is an excellent cat toy. My cat, has played for an entire hour only taking a break to eat before going back and trying to rip apart the well stiched mouste full of cat nip. It really has been the best toy so far, and there is nothing funner when playing with her than attaching a string to the mouse and watching her run after it when I pull the mouse.\n\nI also wanted to explain why one of the monographed mice said Windows. (I'm mentioning this because there was a not asking why not Linux.) I would much rather see my cat bitting, scratching and tearing apart Windows that I would Linux. I actually like Linux, while I think XP is solid, I still have nightmares from Win 9x series which IMo was as stable as flubber. \n\nI guess I probably should of e-mailed you, this is really OT, but I didn't want to bother you wiht such insignificant comments.\n\nThanks again for the toy and Quanta!"
    author: "Rodica"
  - subject: "Re: I agree with you completely"
    date: 2003-05-12
    body: "Omigod... This is my life. For those who don't know I used to be a full time professional web designer and besides burn out (and being too upset to think after losing my mom) I decided to take a few years and focus on my Kitty Hooch business. I still manage the Quanta development project and still do web work of a less stressful nature.\n\nThis really is OT (and should have been an email) and my wife mentioned the \"Windows\" monogram and asked me if I had any idea. I could only come up with the twisted speculation confirmed here. ;-) My personal favorite monogram is \"Cat Drugs\" however I thought the people who named their cats \"chips\" and \"Dips\" was funny.\n\nI just got back from  the coast (Astoria), one of the four open markets my wife and I split up and covered this weekend. I heard great testimonials from excited customers all weekend. I thought I'd catch up on my KDE and Quanta stuff... Yes, it's safe to say Kitty Hooch is the most potent and durable catnip toy anywhere. Our goal is to be able to say the same about Quanta.\n\nThanks for saying good things about both Quanta and our sponsor, Kitty Hooch. I hope to update the site this week and announce a new sponsor who will be helping us to continue to grow Quanta. Also anyone doing complex PHP work might want to try CVS this week and check out the PHP mode of the structure tree... We are working on showing off page PHP data in the tree. Currently it works but needs some refinement."
    author: "Eric Laffoon"
  - subject: "Re: Progress maybe shortchanging userability"
    date: 2003-05-11
    body: ">  how about a survey page for what features are really most needed or desired by users?\n\nGo to http://bugs.kde.org and click on link \"The most wanted features\"."
    author: "Anonymous"
  - subject: "Also OT"
    date: 2003-05-12
    body: "For those who don't know what catnip is:\n\n\"\"Catnip\" is the common name for a perennial herb of the mint family. It is native to Europe and is an import to the United States and other countries. The catnip plant is now a widespread weed in North America.\n\nGiven to the right cat, catnip can cause an amazing reaction! The cat will rub it, roll over it, kick at it, and generally go nuts for several minutes. Then the cat will lose interest and walk away. Two hours later, the cat may come back and have exactly the same response.\n\nBecause there really isn't any scent that causes this sort of reaction in humans, catnip is hard for us to understand. However, it is not an uncommon behavior in animals that rely heavily on their noses. For example, there are many scents that will trigger intense hunting behavior in dogs, and other scents will cause dogs to stop in their tracks and roll all over the scent.\n\nAlthough no one knows exactly what happens in the cat's brain, it is known that the chemical nepetalactone in catnip is the thing that triggers the response. Apparently, it somehow kicks off a stereotypical pattern in cats that are sensitive to the chemical. The catnip reaction is inherited, and some cats are totally unaffected by it. Large cats like tigers can be sensitive to it as well.\n\nThe reaction to catnip only lasts a few minutes. Then the cat acclimates to it, and it can take an hour or two away from catnip for the cat to \"reset.\" Then, the same reaction can occur again. Very young kittens and older cats seem less likely to have a reaction to catnip. \"\n                        -How Stuff Works\n\nThe cats affected by catnip IMO are far higher than some people think, it just depends on the quality of the catnip a lot, anyway that's what I heard."
    author: "mario"
  - subject: "Re: Also OT"
    date: 2003-05-13
    body: "Wow! Suddenly KDE is catnip land. As Robert Nickel said to me when he thought I was confused \"I think you've been sampling your inventory\".\n\nYou are correct in your disertation except that the effect of catnip is an airborne aphrodisiac. This is why kittens have to reach a level of maturity, usually 3-6 months, before they are affected. Also catnip and the less potent catmint were the most popular tea in England until the mid 1600s when they began importing tea from the Indies.\n\nThe reaction is often hilarious. Older cats upwards of 21 typically use it for a pillow. For most cats it can amplify playful or sleepy moods. Veterinarians prescribe it for sedintary cats that have lost their appetite as they will play and then eat. I've heard of 14 year old cats doing back flips, cats laying on their back with a toy held in their mouth by their front paws while their back legs twitch and even cats batting at non existant things in the air. Often they roll and kick and bite. I've heard accounts of cats playing for a while and then sitting in the window for an hour looking like it was bombed. On one occasion the smell was so intense in our house while making toys that our four cats were laying around like they melted with their eyes completely black staring off into space for an hour. Don't expect this from a single toy. ;-) One of my cats sat on a chair and stuck his head in a shopping bag full of catnip for five minutes pulling it out to look quite happy. A number of my friends have said if there is reincarnation they want to return as one of my cats.\n\nAs far as the percentage of cats affected the supposed number is 80%. I cannot give a scientific result but our experience with our money back guarantee and conversations with our customers about previously unresponsive cats leads me to believe as many as half do not respond to the stale mulched stuff sold commercially and that as few as 1% or less are unaffected by our stuff. I'd love to see clinical data too on the genetic link.\n\nBTW when you look it up note that as a mint it is supposed to grow 3 feet (1 meter) tall. Note that my organic cocktails get mine to 8 feet! (2 2/3 meter) See the attached of me picking."
    author: "Eric Laffoon"
  - subject: "Re: Progress maybe shortchanging userability"
    date: 2003-05-12
    body: "<i>I realize KDE volunteers are more attracted to \"cool\" features</i>\n\nDo you think so? My impression is that KDE rather lacks many cool features that I would like to have, starting with more eye candy and ending with buzzwords like web services...\n\n"
    author: "AC"
  - subject: "Re: Progress maybe shortchanging userability"
    date: 2003-05-13
    body: "Please suggest the features you'd like to see as wishlist items, or vote for them if they are allready in the database. Better yet, if you can, please implement some yourself.\nBTW: I don't think many KDE devellopers spend much time on websites. In my (limited) experience, websites are often maintained by interested people who want to become involved in the KDE project, but can't write C++ code."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Progress maybe shortchanging userability?"
    date: 2003-05-14
    body: "I run a 'small business' and it has been based on free-software only since the very beginning. All PCs run a KDE desktop and a mix of KDE/Gnome software, since many of the best applications are based on GTK. Most important:\n\nKWord for writing needs\nQuanta for all web development\nGnumeric for finance needs. (KSpread is totally too slow to be usable)\nKdepim (Korganizer/Kmail) for PIM\n\nWhat I found strange in the critizisem is: \"only 3 phone nos are listed\". Well, as you can view in the screenshot at  \nhttp://reviewed.homelinux.org/screenshots/apps/no/kadressbook.jpg.index-no.html  , you can actually store *5* different phone numbers. And I don't know anybody who's got _that_ many.\n\nI am used to clicking on the person whom I need information about, and when I do I allways view all the phone numbers complete without being cut off. \n\nI also love the fact that birthdays from the adressbook are listed in KOrganizser and Kickpim  ( http://kickpim.sourceforge.net/ ).\n\n.. At last, I just wanted to say that I have found some very annoying bugs over the years. And my experience is that http://bugs.kde.org/ are read by the right people, I have allways got an answer (and often a solution) impressivly fast."
    author: "\u00d8yvind S\u00e6ther"
  - subject: "WTF is going on at KDE websites"
    date: 2003-05-11
    body: "Maybe its just me, highly unlikely, but I visit the dot sometimes and the new news items disappear than reappear after a few hours and than comments don't show up for a long time and sometimes not at all. Websites that use the enws feed such as KDE-LOOk don't show updates for the Dot until a few days etc,\n\nIs KDE having server troubles. Btw is there a way to make the dot have a color scheme or design that matches kde.org?"
    author: "mario"
  - subject: "nevermind"
    date: 2003-05-11
    body: "Seems to work now, even though looky is still lagging. "
    author: "mario"
---
<a href="http://www.koffice.org">KOffice</a> developers have added a
number of templates to <a href="http://www.koffice.org/kspread">KSpread</a>, while work
towards Excel compatibility continues. <a href="http://edu.kde.org/kstars">KStars</a> now has a telescope hardware interface.
The KWin and KDesktop fine-tuning continues, as well as the work on implementing <a href="http://www.freedesktop.org">freedesktop.org</a> standards. Read it all in <a href="http://members.shaw.ca/dkite/may92003.html">the latest KDE-CVS-Digest</a>.
<!--break-->
