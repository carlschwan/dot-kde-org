---
title: "KDE Community Wiki Site"
date:    2003-10-01
authors:
  - "luci"
slug:    kde-community-wiki-site
comments:
  - subject: "Great work...."
    date: 2003-10-01
    body: "I was proposing on the French Translation Team two days ago to create a Wiki to put our always half-finished translation Howto and various information that is now buried in the mailing list archive. I didn't know of your project and I'm very happy that you did this. My guess is that we could reach a wiki of 10-20 pages for the French translators.\n\nI was thinking for the French translation team of something simpler that TikiWiki like MoinMoin but your site looks much nicer that anything I could do. Would it be possible to create some sub-wikis for the different translation teams : something like kde.ground.cz/i18n/french with our own hierarchy of pages, sandbox and index. Everything will be in French in these pages so I don't think it should be indexed in the global English index.\n\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: Great work...."
    date: 2003-10-02
    body: "thanks charles,\n\ni think since TikiWiki is \"multilingual\", uses utf-8 encoding and has the ability to switch the language of interface to french too, you can feel free to put these pages to KDE CWS and we can make an own structure to them...\n\notherwise i think it would be better to set up your own Tiki under some *.fr domain instead of making sub-wiki"
    author: "luci"
  - subject: "Re: Great work...."
    date: 2003-10-03
    body: "I think there is no problem with pages in french, if they start with their own main page. It is good to keep the site together, for the critical mass of contributors needed to make it work. I would welcome such topics! And then, you make the work you do more visible. "
    author: "Ante Wessels"
  - subject: "Yikes!"
    date: 2003-10-01
    body: "Yikes, that site is completly farked up in konqueror (cvs build). X_X\nScreenshot: http://vazagi.homepage.dk/kde-cvs.gif\n\nIt's probably this bug again:\nhttp://bugs.kde.org/show_bug.cgi?id=64286"
    author: "Mikkel Schubert"
  - subject: "Re: Yikes!"
    date: 2003-10-01
    body: "Or this one:\nhttp://bugs.kde.org/show_bug.cgi?id=64286"
    author: "jos"
  - subject: "Re: Yikes!"
    date: 2003-10-01
    body: "Hmm, i meant this one:\n\nhttp://bugs.kde.org/show_bug.cgi?id=64496"
    author: "jos"
  - subject: "Re: Yikes!"
    date: 2003-10-01
    body: "looks like a nasty regression indeed. works fine in 3.1."
    author: "anon"
  - subject: "Re: Yikes!"
    date: 2003-10-24
    body: "The site displays fine in khtml/HEAD again for some days now."
    author: "Anonymous"
  - subject: "Non-official"
    date: 2003-10-01
    body: "While I certainly applaude the initiative, sites like these tend to get announced and then die slowly.  For instance, who remembers the KDE Wiki announced at http://dot.kde.org/1045070317/1045254304/ anymore?\n\nI wish something official could be arranged, or at least make an alias URL like http://wiki.kde.org for this Wiki -- to keep it alive and popular!"
    author: "Haakon Nilsen"
  - subject: "Re: Non-official"
    date: 2003-10-01
    body: "> For instance, who remembers the KDE Wiki announced at [..] anymore\n\nA wiki announced in a comment (by a non-known KDE contributor?) which not even works? I don't wonder that this didn't get attention."
    author: "Anonymous"
  - subject: "Re: Non-official"
    date: 2003-10-01
    body: "I do agree that if this Wiki is going to be done properly it should at least be at some place like wiki.kde.org, have serious maintainers and permanent links to it from KDE.org.\n\nThen again, I never got what Wiki's were all about.  Seems to be a lot of hype."
    author: "Navindra Umanee"
  - subject: "Re: Non-official"
    date: 2003-10-01
    body: "Wikis are positively a lot more than hype.  Just look at http://www.wikipedia.org -- a collaborative encyclopedia with some 120.000 well-written articles of all topics.  Anyone can edit articles without even registering, and it still works, not in spite of this, but because of it.  When a Wiki reaches a \"critical mass\" of popularity, it Just Works and becomes an invaluable source of live documentation.  To reach this mass is why I really think a KDE Wiki should be official, and as you say, linked to from other KDE sites etc."
    author: "Haakon Nilsen"
  - subject: "Re: Non-official"
    date: 2003-10-02
    body: "ACK, \"critical\" is the keyword here.  \nIMHO it just won't work if it's not official \nor if it's only one among others.\n"
    author: "cm"
  - subject: "Re: Non-official"
    date: 2003-10-02
    body: "Being \"official\" is never a guaranty for success. Neither popularity among visitors. Only the \"critical mass\" of contributors is."
    author: "Anonymous"
  - subject: "Re: Non-official"
    date: 2003-10-02
    body: "There never is any guarantee.  \nBut I think that without being linked to by prominent \nplaces and having a \"good\" name like wiki.kde.org\nit will have neither enough contributors nor \nvisitors.\n\nI've seen too many KDE news boards and forums \ndie of a lack of both. \n\nHaving a name like wiki.kde.org and being linked \nto from www.kde.org is my definition of \"official\" \nhere.\n\n"
    author: "cm"
  - subject: "Re: Non-official"
    date: 2003-10-01
    body: "I don't know why it should be a problem to get a second level domain which includes \"kde\" like kde-look.org, kde-forum.org and kdedevelopers.org do. There will be no problem with KDE e.V. if \"kde\" is properly used. I see potential problems if Wiki content is presented as third level domain under kde.org, more than when it is only linked from kde.org. And a link is likely if there is useful content (which I yet miss in this Wiki) in future."
    author: "Anonymous"
  - subject: "Re: Non-official"
    date: 2003-10-01
    body: "It's just a waste of money and other resources.  Getting wiki.kde.org shouldn't have to be so hard IMHO, when the people behind the site just want to help the project and organise a community beneficial to KDE."
    author: "Navindra Umanee"
  - subject: "Re: Non-official"
    date: 2003-10-03
    body: "I'm sure the aforementioned sites could get kde.org subdomains if they really wanted to."
    author: "anon"
  - subject: "Re: hype"
    date: 2003-10-03
    body: "At the KDE website I saw pages that are not maintained. No maintainer, no one carring to implement changes proposed. A wiki may work better. That's the idea behind it. "
    author: "Ante Wessels"
  - subject: "Re: Non-official"
    date: 2003-10-24
    body: "> While I certainly applaude the initiative, sites like these tend to get announced and then die slowly. For instance, who remembers the KDE Wiki\n\nVisit the site now, seems to be a nice start for under one month existence."
    author: "Anonymous"
  - subject: "Czech Involvement"
    date: 2003-10-01
    body: "Did you notice the increasing Czech KDE involvement? First two core developers (Lubos and Lubos), then the KDE conference in Nove Hrady, followed by Cuckooo and now a Wiki site."
    author: "Anonymous"
  - subject: "Re: Czech Involvement"
    date: 2003-10-01
    body: "oops... the czechs are comin...! A takeover? Well, \"I for one welcome our new...\"  ... argh .... nevermind... \nok:    so what ?"
    author: "Thomas"
  - subject: "Re: Czech Involvement"
    date: 2003-10-01
    body: "\"This is a faraway country of which we know little.\""
    author: "Neville Chamberlain"
  - subject: "Sorry Lukas :-)"
    date: 2003-10-01
    body: "This should have been of course \"Lubos and Lukas\"."
    author: "Anonymous"
  - subject: "Re: Czech Involvement"
    date: 2003-10-02
    body: "That's of course the next step of the great world takeover plan. Why do you think KDE name is an everyday's czech word?"
    author: "L.Lunak"
  - subject: "Re: Czech Involvement"
    date: 2003-10-02
    body: "So, errh, what does it mean? :)"
    author: "Haakon Nilsen"
  - subject: "Re: Czech Involvement"
    date: 2003-10-02
    body: "'KDE' just means 'WHERE' in czech :)"
    author: "luci"
  - subject: "Re: Czech Involvement"
    date: 2003-10-02
    body: "I, for one, welcome our new Czech overlords."
    author: "Anon"
  - subject: "Spaces"
    date: 2003-10-01
    body: "Why can't article names have spaces? ThemeDoc, StyleGuide and KDEDevelopment look a bit strange and wrong, actually."
    author: "Daan"
  - subject: "Re: Spaces"
    date: 2003-10-01
    body: "+1 Insightful"
    author: "anon"
  - subject: "Re: Spaces"
    date: 2003-10-02
    body: "It is possible to make articles with spaces! You link to them by using brackets. IIRC \"((link to a page with spaces))\" should work. Actually my small additions to the KDE Wiki contained links with spaces but someone changed them :-(\n\n"
    author: "MK"
  - subject: "Re: Spaces"
    date: 2003-10-02
    body: "Actually, they're not articles, they're wiki pages. \"SomethingPutTogether\" is a common convention to title pages in Wiki world (it's then used e.g. to simple link to another page). I've changed in site settings to the \"strict naming\" to keep some consistency, but if there will be more requests to have Wiki pages named with spaces (can cause problems with URLs in some browsers) i can switch it back to \"full\"."
    author: "luci"
  - subject: "Re: Spaces"
    date: 2003-10-02
    body: "> named with spaces (can cause problems with URLs in some browsers)\n\n\nwhich browsers? They always been encoded to %20 since the early days of the WWW"
    author: "anon"
  - subject: "Re: Spaces"
    date: 2003-10-02
    body: "i may be wrong, but i remember i've had some problems in Netscape 4.xx\nbut maybe it was just URLs to images with spaces, these images were not displayed on the page then (broken)..."
    author: "luci"
  - subject: "Re: Spaces"
    date: 2003-10-02
    body: "Well not throughout the whole Wiki world...\n\nThe great and honorable Wikipedia uses a different system: \nWiki pages are marked with double angle brackets, \nand the names can contain spaces.  I don't know \nwhat problems with certain browsers you are referring to\n(as someone already pointed out spaces should work \nif properly encoded). \n\nWikipedia seems to use underscores instead of spaces in links:  \nhttp://www.wikipedia.org/wiki/Parliamentary_system\n\nBut I guess that's mainly for greater readability of the links...\nhttp://www.wikipedia.org/wiki/Parliamentary%20system\nwould indeed be somewhat ugly.\n\nWould the latter be possible in your wiki version?  \nIf so I'd vote for names with spaces because the Wiki content \nwould be more natural without WordsRunTogether, IMHO.   \n"
    author: "cm"
  - subject: "Re: Spaces"
    date: 2003-10-02
    body: "> WordsRunTogether, IMHO.\n\nAgreed.. the wikipedia concept of \" \"->\"_\" internally/in links (but not to user) seems good"
    author: "anon"
  - subject: "Re: Spaces"
    date: 2003-10-02
    body: "Yes, this is indeed a good idea, I think. And I must say that the Wiki by itself looks great."
    author: "Daan"
  - subject: "Registration"
    date: 2003-10-01
    body: "I have a question, why a registration to be able to modify content? I thought that the idea of a Wiki is that everyone can change everything and rampage is to be reverted by the serious majority of contributors."
    author: "Anonymous"
  - subject: "Re: Registration"
    date: 2003-10-02
    body: "I think it increases quality and reduces those incidents of rampage.  \n(Of course, it still cannot avoid it altogether.)\n\nEveryone can register, and you are not asked any personal questions\nbeside your email address.  IMHO it's not too much to ask.  \nSo anyone can edit.\n"
    author: "cm"
  - subject: "Re: Registration"
    date: 2003-10-02
    body: "Before I knew Wikipedia I would have agreed with your arguments. But now I think Wikipedia shows that anonymous edits work pretty good. The amount of vandalism is small and many people start with Wikipedia by fixing some typos--which they wouldn't do if they had to create an account first.\n\nIMHO one should at least try allowing anonymous edits and if one finds that it does not work switch back.\n\n"
    author: "MK"
  - subject: "Re: Registration"
    date: 2003-10-02
    body: "> IMHO one should at least try allowing anonymous edits and \n> if one finds that it does not work switch back.\n\nOk, why not.   *If* there is a history in our wiki, that is. \nYou're right as far as wikipedia is concerned.  \n"
    author: "cm"
  - subject: "Re: Registration"
    date: 2003-10-02
    body: "> The amount of vandalism is small and many people start with Wikipedia by\n> fixing some typos--which they wouldn't do if they had to create an account \n> first.\n \nthat sounds right but anyway, i would like to know who to thank for these fixes ;)\n\n> IMHO one should at least try allowing anonymous edits and if one finds that  > it does not work switch back.\n\nok, we can try it...\n\nluci\n \n "
    author: "luci"
  - subject: "Re: Registration"
    date: 2003-10-02
    body: "Does it not have a history so you can resurrect old versions if someone went berserk? I have only used one wiki so far which depended on MySQL as a backend and allowed the maintainer to revert to older versions of a page."
    author: "Maik Schulz"
  - subject: "Re: Registration"
    date: 2003-10-02
    body: "yes, it does. there should be no problem to revert back..."
    author: "luci"
  - subject: "Great news"
    date: 2003-10-02
    body: "That's great news! I was thinking about starting one too.\n\nThings I would like to put in the wiki:\n- user tips\n- developers tips\n\nAnd everybody can contribute. In fact, lot of documents from developer.kde.org should be put in the wiki.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Great news"
    date: 2003-10-02
    body: "> In fact, lot of documents from developer.kde.org should be put in the wiki.\n\nDuplicate storage makes no sense and would be a nightmare to synchronize."
    author: "Anonymous"
  - subject: "Re: Great news"
    date: 2003-10-03
    body: "> Duplicate storage makes no sense and would be a nightmare to synchronize.\n\nAgreed. the documents on d.k.o should be removed completely and linked to the wiki pages (if this becomes wiki.kde.org of course)"
    author: "anon"
  - subject: "Re: Great news"
    date: 2003-10-03
    body: "That would be a mistake.  Wiki's are disorganized and/or have lame URLs with StuddlyCapsAndNoSpacesOrLame_Underscores_InThem.  Do you also suggest we break all the existing links to developer.kde.org by removing developer.kde.org?"
    author: "ac"
  - subject: "Re: Great news"
    date: 2003-10-03
    body: "> have lame URLs with\n\nuh, since a URL isn't particularily important, who cares.\n\n> Do you also suggest we break all the existing links to developer.kde.org by removing developer.kde.org?\n\nPlease re-read the grandparent post, I said \"linked to the wiki pages\". This would break nothing, and have a heap of advantages, like easy editing. "
    author: "anon"
  - subject: "UserFaq"
    date: 2003-10-02
    body: "This seems to be the right platform to create and maintain a up-to-date KDE Frequently Asked Questions (but please only add common questions *with* answers). http://www.kde.org/documentation/faq/ seems to be very outdated."
    author: "Anonymous"
  - subject: "JiJi La pointe is watching you"
    date: 2003-11-18
    body: "I hope your process secure."
    author: "JJlapointe"
---
I would like to let you know about a new project which I have started, which is based on an idea by Ante Wessels: a Wiki for KDE. The 'working draft' name for this project is "KDE Community Wiki Site" (KDE CWS) and you can find the site which is more than just a Wiki at <a href="http://kde.ground.cz/">kde.ground.cz</a>.
<!--break-->
<p>It is based on the very powerful and feature rich <a href="http://tikiwiki.org/">TikiWiki CMS</a>. The interface is multilingual and content can be in almost any language too (uses utf-8). Registered user can set up many settings in their preferences including site layout and style (users can even make their customized versions of available styles).
</p>
<p>Everyone is welcome to join and participate on the content. The main idea is 
to share KDE users' and developers' experience. Every registered user is able to create, edit and modify the content of Wiki pages. It's fast and easy.
</p>
<p>You can start reading <a href="http://kde.ground.cz/tiki-index.php">about the site</a> and when you are curious what a Wiki is, read <a href="http://kde.ground.cz/tiki-index.php?page=Wiki">this page</a> first.
</p>
<p>What are the rules? A simple KDE CWS <a href="http://kde.ground.cz/tiki-index.php?page=WikiGuide">Wiki Guide</a> is available.
</p>
<p>And one last but not least important thing, you can register <a href="http://kde.ground.cz/tiki-register.php">here</a> :)
</p>
<p>What's planned? Making it much better than it's now (btw: I'm one of the 100 
TikiWiki developers, so it's not just a 'plain sentence'), better accessibility and xhtml 1.0 strict compliance, better and more compatible CSS styles (KDE.org style would be nice too) and of course more and more KDE related content (devel, documentation, ideas, everything is needed)...
</p>
<p>Any comments, questions or suggestions are welcome. This is a test which 
depends on you.
</p>