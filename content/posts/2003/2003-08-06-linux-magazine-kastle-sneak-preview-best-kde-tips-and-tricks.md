---
title: "Linux Magazine: Kastle Sneak Preview & Best KDE Tips and Tricks"
date:    2003-08-06
authors:
  - "binner"
slug:    linux-magazine-kastle-sneak-preview-best-kde-tips-and-tricks
comments:
  - subject: "Nice stories...."
    date: 2003-08-06
    body: "...however the pictures of the on-line PDF documents where really unreadable. This destroyed the otherwise very proffesional look. What happend? Where the pictures of low quallity to begin with or did the pdf converter mess it up, as usual?\n\nOverall, i must say that it's a joy that it is so easy to convert stuff to pdf-files from almost everything in Kde. But pictures tend to get really bad. I guess it has to do with the gs tool in use ps2pdf, why obiously have some problems  with the compression of pictures. \n\nWhat I can't understan in this context, is why Scribus makes such a perfect jobb when converting to pdf's. Why can't Kde use whatever they are using? \n \nI think converting to PDF is a very important part of computing, especially now when there is no standard Ofiice file format around. So please say tha there is something happening. I'm willing to help out as much as I can, just point me in the right direction. \n\nThanks for a great job all of you KDE developers!"
    author: "Swoosh"
  - subject: "Re: Nice stories...."
    date: 2003-08-06
    body: "When you look with Acrobat Reader at the properties of these PDFs you will see that they're created with Acrobat Distiller 5.0.5 for Macintosh and not with KDE. :-)"
    author: "Anonymous"
  - subject: "Re: Nice stories...."
    date: 2003-08-06
    body: "Ok, that's even worse. Acrobat on a Mac??? For heaven's sake people??? ;)\n\nAnyway, I think  the question is still rerlevant. Sribus can export PDF-files with nice looking pictures. Useing the Pdf-creation option in any KDE application can't. Why?  "
    author: "Swoosh"
  - subject: "Re: Nice stories...."
    date: 2003-08-06
    body: "Because of PDF filter obviously has some room for improvements. Feel free to fill it :)\n"
    author: "AC"
  - subject: "Re: Nice stories...."
    date: 2003-08-06
    body: "because scribus generates the pdf-code directly from the docuement, whereas the \"print as pdf\"-file in the kprinter-dialog actually prints to a postsript file and converts this to a pdf-file.. some quality may get lost during this operation..."
    author: "Thomas"
  - subject: "Re: Nice stories...."
    date: 2003-08-06
    body: "Well I guess I allready knew that in a way. But thanks for spelling it out for me. Then it is nothing stopping me from coding a Adobe Acrobate clone for KDE. \n\nJust ofcourse the issue of comming up with a name for the project. Well, let's see it should start with a K....\n\n "
    author: "Swoosh"
  - subject: "Re: Nice stories...."
    date: 2003-08-07
    body: "to get it right from the beginning, it should be implemented in QPrinter. That would be a huge step forward, if QPrinter would be able to directly output slick pdf-code without generating postscipt-code..."
    author: "Thomas"
  - subject: "Re: Nice stories...."
    date: 2003-08-07
    body: "How about \"Kontortionist\""
    author: "Paladin128"
  - subject: "Re: Nice stories...."
    date: 2003-08-06
    body: "In my experience, the PDF printer in KDE is capable of producing absolutely excellent PDF files. If you get bad images, then I guess the underlying converter at your computer is badly set up.\n\nAt this computer, I get nice pictures and bad fonts. I guess there's something I could do to fix it, but I haven't looked into it."
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Nice stories...."
    date: 2003-08-06
    body: "\nAt a guess, you need to set the 'embed fonts in postscript data when printing' option under 'System options' in the print dialogue."
    author: "cbcbcb"
  - subject: "xtermset for konsole?"
    date: 2003-08-06
    body: "I use xtermset to change the background when I log into a remote with ssh computer. Is it possible to do the same with konsole? (the solution explained is not satisfactory for me as I sometimes log in computers located behind a gateway, thus not accessible directly)\n\nRaph"
    author: "raphinou"
  - subject: "Re: xtermset for konsole?"
    date: 2003-08-06
    body: "did you file a wish list at bugs.kde.org?\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: xtermset for konsole?"
    date: 2003-08-06
    body: "Before doing that I want to be sure it's not possible now :-)"
    author: "raphinou"
  - subject: "Re: xtermset for konsole?"
    date: 2003-08-06
    body: "http://bugs.kde.org/show_bug.cgi?id=37942 ?"
    author: "Anonymous"
  - subject: "Re: xtermset for konsole?"
    date: 2003-08-07
    body: "Already tried to do this with dcop? Use kdcop to find the appropriate calls."
    author: "Yaba"
  - subject: "Re: xtermset for konsole?"
    date: 2003-08-07
    body: "dcop between different machines?"
    author: "Anonymous"
  - subject: "Cool stuff"
    date: 2003-08-06
    body: "I've also just managed to find a bug in netscape, Mozilla, Firebird, and maybe others. They freeze if you middle click on a PDF lin to open in a new tab or you select open in new tab or you ctrl click to open in new tab.\n\nIf anyone, has a bugzilla mozilla account please submit this bug for me, if not I guess I'll have to get one."
    author: "Mike"
  - subject: "Konsole ssh tip"
    date: 2003-08-06
    body: "I tried the ssh tip, but the font keeps defaulting to Linux. I've tried setting it back to medium and clicked \"save as default,\" but when I open a new session it opens it incorrectly again.  Is there another way to save the font selection for a session?\n\nthx\nMike"
    author: "anonymous"
  - subject: "Re: Konsole ssh tip"
    date: 2003-08-06
    body: "Make sure you have selected the corrent session-type. Happened to me too.\n"
    author: "Roland"
  - subject: "tricks"
    date: 2003-08-06
    body: "nice articles =)\n\nsome other tricks i particularly enjoy in KDE: \n\n o hold down the shift button when moving a button on the panel and it will \"shove\" other buttons around instead of switching places with them\n\n o middle click a KMail folder that has an associated mailing list to create a new posting to the mailing list address\n\n o select a bunch of files in Konqi, copy them, then paste into a text area (KWrite, KMail, even Konsole) and you get a nice list of URLs\n\n o DnD from kdcop to a text area is handy, esp since you can switch between shell, C++ and Python modes\n\n o fast browser identity and feature switching, as well as per-host settings (great for getting to the online bank =)\n\n o kstart, which allows you to start programs on different desktops, control their positioning (fullscreen, maximized, etc), and window type (tool, menu, dialog or no decos at all (aka Override)), send them to the system tray, keep them out of the taskbar, etc...\n\nprobably more, but that's all that's leaping to mind at the moment..."
    author: "Aaron J. Seigo"
  - subject: "Re: tricks"
    date: 2003-08-06
    body: "Maybe there should be a KDE Tips site/wiki like http://jodrell.net/gnome-hacks?"
    author: "Anonymous"
  - subject: "Re: tricks"
    date: 2003-08-07
    body: "Good idea, anyone willing to donate a server for it?"
    author: "Datschge"
  - subject: "Just put on kde.org in the Documentation section"
    date: 2003-08-07
    body: "http://kde.org/documentation/ \n\nher ethere could be a link to tips and tricks."
    author: "Alex"
  - subject: "Re: Just put on kde.org in the Documentation section"
    date: 2003-08-07
    body: "I was more interested in a suggestion for an existing easy to use infrastructure, not a suggestion where one could possibly put it. For example I think a Wiki site is still too complex for a tricks site, the gnome hacks site by Jodrell is nice regarding simplicity but is imo lacking the ability to include descriptive pictures (if we want to target average users) as well as categorizing all submissions (if we want a huge databases to stay easy to look through)."
    author: "Datschge"
  - subject: "Re: tricks"
    date: 2003-08-07
    body: "Yes, but I don't think we need one as much as the Gnomers do. ;-)\n\nhttp://jodrell.net/gnome-hacks/hacks.html?id=7\n"
    author: "Gaffeltruck"
  - subject: "Re: tricks"
    date: 2003-08-07
    body: "Do you know about KDEWM? Fine for you but who else too?"
    author: "Anonymous"
  - subject: "Re: tricks"
    date: 2007-04-17
    body: "I found another mouse trick; for those with a 3-button mouse, hold middle mouse button over a panel icon to move it to another location on  the panel (and / or desktop?)."
    author: "QBall2U"
---
For its <a href="http://www.linux-magazine.com/issue/34">September 2003 issue</a>, <a href="http://www.linux-magazine.com/">Linux Magazine</a> has interviewed several KDE developers, resulting in two feature stories. Both are available online in PDF format. The first, titled &quot;<a href="http://www.linux-magazine.com/issue/34/KDEContributorConferencePreview.pdf">Sneak Preview</a>&quot; deals with the <a href="http://events.kde.org/info/kastle/">KDE Contributor Conference 2003</a> (&quot;Kastle&quot;) and summarizes the developers' personal plans for the conference and what they expect as an outcome of the hacking festival. In &quot;<a href="http://www.linux-magazine.com/issue/34/KDETricks.pdf">Hidden but sooo useful</a>&quot; several KDE tips and tricks are revealed by the developers.
<!--break-->
