---
title: "Second KDE 3.2 Alpha \"Tereza\" Released"
date:    2003-09-26
authors:
  - "skulow"
slug:    second-kde-32-alpha-tereza-released
comments:
  - subject: "This is so great...."
    date: 2003-09-25
    body: "Hi,\n\nI am all excited and just building it. Hopefully this time -pedantic won't be a problem in configure (it was last time, Debian Sid gcc) and I can finally have a peek at Kontact.\n\nTime for the killer app: Kontact.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: This is so great...."
    date: 2003-09-25
    body: "Don't expect too much yet. While Kontact is certainly a nice thing, it's probably one of the least complete and least stable things in the 3.2 feature plan in this alpha. But we are getting there, thanks to all the contributors and especially to Matthias Kretz, who invested a really great amount of time into Kontact and KControl!\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: This is so great...."
    date: 2003-09-26
    body: "The -pedantic errors where not in konstruct or even KDE at all. They are mistakes in glibc 2.3.2, and discovered by a new strickter gcc 3.3."
    author: "Another Debian User"
  - subject: "Re: This is so great...."
    date: 2003-09-26
    body: "Hi there :-)\n\nThanks for pointing this out. I guess I shall watch for updates on libc then (actually I am not on Sid, but mostly testing if I can).\n\nYours, Kay"
    author: "Debian User"
  - subject: "Can't wait.."
    date: 2003-09-25
    body: "Ok, I'm a emerge -u world freak. I just can't wait for KDE 3.2. \n\nBTW, is there any chance to get kwin shadow[1] and kdesktop icon shadows[2] in KDE 3.2? I've patched my ebuilds for 3.1.x, but an official release with this patches would be much better :-)\n\n[1] http://kde-look.org/content/show.php?content=5638\n[2] http://kde-look.org/content/show.php?content=4750\n\nBest regards,\nNorberto\n"
    author: "nbensa"
  - subject: "Re: Can't wait.."
    date: 2003-09-25
    body: "[1] Won't ever make it. It's an evil hack and it's slow as hell, especially on shaped windows. And it's not stable enough.\n\n[2] Something similar is implemented since quite some time. Alpha1 has it already. Just enable it.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Can't wait.."
    date: 2003-09-26
    body: "[1] and looks -- if working properly -- crappy; glichy -- if not working"
    author: "AC"
  - subject: "Re: Can't wait.."
    date: 2003-09-26
    body: "The second patch has already been in KDE for a long time. What i'm disappointed with is not having the options in kcontrol for configuring it. :(\n\n(and it also comes with a throughily crappy default)\n\nPut this in your kdesktoprc:\n\nShadowConfigName=OS X\nShadowCurrentScheme=0,1,16.0,192.0,2,4,0\nShadowEnabled=true\nShadowTextBackground=invalid\nShadowTextColor=1,1,1\n"
    author: "anon"
  - subject: "Re: Can't wait.."
    date: 2003-09-26
    body: "The default's is actually pretty good if you adjust the surround colour to fit in with your colour scheme better. The solid white on black default is pretty nasty looking and not easy to read.\n\nThe best thing about the KDE shadow style is it's not a copy of another operating system's style."
    author: "Max Howell"
  - subject: "Re: Can't wait.."
    date: 2003-09-26
    body: "I have not yet tried it, but it looks good to me and easier to read than shadow behind."
    author: "jukabazooka"
  - subject: "I know, WTF"
    date: 2003-09-26
    body: "Why aren't they using  an OS X, GNOME or XP like shadow by default. I think the default looks worse than none at all.\n\nCome on people, let's have some GOOD defaults.\n\nAnyway, Alpha 2 seems to be pretty good over all, thoguh I hope that this isn't true: http://osnews.com/comment.php?news_id=4658#147610"
    author: "Mike"
  - subject: "Re: I know, WTF"
    date: 2003-09-26
    body: "> Come on people, let's have some GOOD defaults.\n\nsubmit a bug report"
    author: "anon"
  - subject: "There already is a bug report"
    date: 2003-09-27
    body: "http://bugs.kde.org/show_bug.cgi?id=58944"
    author: "mike"
  - subject: "Gentoo?: Can't wait..? You don't need to..."
    date: 2003-09-26
    body: "\"emerge -u world freak\" -> You are using Gentoo Linux.\n\nKDE 3.2alpha1 is already in portage...\n\n1. Try grep kde  /usr/portage/profiles/package.mask\n2. sed s/=kde/#=kde/g  or hand-edit the file\n3. ACCEPT_KEYWORDS=\"~x86\" emerge kdebase\n\nKDE 3.2 alpha2 is not yet in portage. The KDE 3.2alpha1 ebuilds only need little modification to be used with a2. The version I used (my usr/local/portage) is here:\n\n1. wget http://xiando.homelinux.net/gentoo/xiandos-portage.tar.bz2\n2. tar xfvj xiandos-portage.tar.bz2 -C /\n3. Verify that PORTDIR_OVERLAY=/usr/local/portage in /etc/make.conf\n4. ACCEPT_KEYWORDS=\"~x86\" emerge kdebase kdenetwork kdepim\n\n\nKDE 3.2 is installed alongside KDE 3.1, so it is very safe to install both and experiment. \n\nKDE 3.2 loads Konqueror much faster, has very nice transparent kicker and eyecandy like:\n\nkcontrol -> Appearance &  Themes -> Launch Feedback -> Busy Cursor\n\nand is generally fun. \n\nBut kcontrol -> change icon theme, and any open Konqueror window crashes... so you'll be visiting bugs.kde.org a few times.."
    author: "xiando"
  - subject: "Re: Gentoo?: Can't wait..? You don't need to..."
    date: 2003-09-26
    body: "I tried to install the officiel kde 3.2 alpha 2 official package but it conflicts with kde 3.1.3. So i guess that you can not install both of them together."
    author: "Philippe Fremy"
  - subject: "Re: Gentoo?: Can't wait..? You don't need to..."
    date: 2003-09-26
    body: "It doesn't conflict.  It's just masked.  You'll need to comment the lines out in package.mask if you want to install it."
    author: "Caleb Tennis"
  - subject: "Re: Gentoo?: Can't wait..? You don't need to..."
    date: 2003-09-27
    body: "Problem is QT > 3.2 is required for this a2 release but QT brakes (somehow) kde 3.1.3. kde 3.1.4 is now in the stable tree and works with QT 3.2.1 (not my own experience). So, edit the QT 3.2.1-r1 ebuild, delete the line about kdelibs, emerge this QT, emerge kde 3.1.4 and emerge kde 3.2a2. I am compiling QT right now.\n\nwill keep you busy for the next 36 hours ;)"
    author: "balk"
  - subject: "Re: Gentoo?: Can't wait..? You don't need to..."
    date: 2003-09-26
    body: "... since there are CVS ebuilds, too. \nthey also leave your stable version intact, so you can use it to play around\njust head to http://dev.gentoo.org/~caleb/kde-cvs.html\nI would advice to install ccache, too, since this makes updating much faster from the second build on."
    author: "Fabio"
  - subject: "Perhaps on the next version?"
    date: 2003-09-25
    body: "The biggest thing missing from the kde-3.2alpha-series is the inclusion of the k3b from the kdeextragear-1 and possibly kmplayer and kmyfirewall from the kdeextragear-2, is there any chance that any of those could be imported to the kde-3.2alpha3?\n\nOr are some/all of those going to pushed down the stream to kde-3.3/4.0, or even pieces of software never gonna be included in the main distribution?\n"
    author: "Nobody"
  - subject: "Re: Perhaps on the next version?"
    date: 2003-09-25
    body: "I don't see how KMyFirewall belongs into the core distribution. It's not portable beyond Linux. It's probably nice to have and important such a tool and distributors should ship it if they don't have a solution already.\n\nIt's not that you couldn't install the packages on your own, is it? Plus most distributors ship packages of apps in kdeextragear.\n\nAnyway, neither of those apps is in the feature plan, so the move won't happen. It's a bit too late to ask for it now, we better try to get the feature plan implemented and the bugs down.\n\nCheers,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Perhaps on the next version?"
    date: 2003-09-26
    body: "the point of the extragear modules is to allow applications to be developed in KDE's CVS repository without having to be part of the KDE devlopment cycle, e.g they don't have to adhere to the feature freeze and so on.\n\nAs for being imported for alpha3, there's no chance."
    author: "Chris Howells"
  - subject: "KWin-III feature-request"
    date: 2003-09-26
    body: "I know it's a bad time to ask for major feature-requests, but I think this is a rather small request, as something similar is already implemented. KWin-III now has a \"Window noborder\" menu-entry in its title-bar context-menu, finally(!). How about having a separate submenu dedicated to window-borders, like Enlightenment has? It should have, IMO, \"Noborder, Border only, etc.\" This is one of the features I miss the most from KDE-1.x days. KWin-III doesn't have an option to bring up the context-menu for windows which cannot get focus. This is a problem for f.ex. KPager when its in \"noborder\"-mode. Then you can't bring up the context-menu to toggle \"window above others\" which is really annoying.\n\nDoes this seem too much to fix before a 3.2 release? Every other window-manager that I have used have these features, and it's a shame if KWin wont have them.\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: KWin-III feature-request"
    date: 2003-09-26
    body: "Agreed.. it should have a Window Type Submenu.. right now there are five entries that could into that submenu (Above others, below others, full screen, no border, store window settings)\n\nJust add the other window types that kstart allows, and it'll be great!\n\nSubmit a wishlist for kwin."
    author: "anon"
  - subject: "Re: KWin-III feature-request"
    date: 2003-09-27
    body: "I don't know if it's a bad time or not, but it's definitely a wrong place. The right place is bugs.kde.org ."
    author: "AC"
  - subject: "Re: KWin-III feature-request"
    date: 2003-10-09
    body: "Hi\n\nFor the second part of your request :\n\nYou can set a keyboard shortcut to bring the window context menu of the currently selected one (which of course works no matter if there is a border or not)\nI am not sure but I think the default shortcut is ctrl-esc ? Or otherwise it's alt and a function key, something like alt-f3. Anyway, look at the keyboard shortcuts in the control center and you'll see .. (I am unfortunately suffering on a windows system right now ;-)\n\nI am also sure you can set a keyboard shortcut for this window-above-others switch.\n\nbye ...\n\ntendays"
    author: "tendays"
  - subject: "View Source"
    date: 2003-09-26
    body: "Can anyone tell me if View Source is back in the Konqueror context menu?  That alone would be worth the upgrade!"
    author: "ac"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "No, and it likely won't be in 3.2."
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Then I'll just switch to Mozilla - perminately."
    author: "Chris Spencer"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "That's a bit of a shallow reason to switch. Just assign a keyboard shortcut to view source."
    author: "Max Howell"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Or, you could just download clee's .rc files, which puts View Source back, but its your choice. \nEveryone is going to have to make *some* compromises. You lost this one. Don't whine about it."
    author: "Rayiner Hashem"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "*Why* was it removed?"
    author: "Nero"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Because a lot of people find it useless, and the developers (apparently) agree with them. I've never once used View Source, and I presume that most people who don't do web-development don't use it either. The menu is already getting too large, so they decided to kick out a little-used option.\n\nPS> I'm not going to have this large-menu vs small-menu debate again. When somebody convinces Apple, Microsoft, and the GNOME project to change their HIGs, then I'll gladly concede the point..."
    author: "Rayiner Hashem"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "That's some strange reasoning, if you don't mind me saying so. You don't use it, so you assume nobody else does?\n\nI'm not a web developer, but I find it invaluable, especially for working out why some pages don't seme to work properly with Konq  - not Konqs fault, it's usually some Javascript explicitly checking for IE - but without view source, I wouldn't know that."
    author: "Stephen Douglas"
  - subject: "PROPERTIES"
    date: 2003-09-26
    body: "Hee,\n\nlet me be hugely off-topic and propose to have 'Properties' allways as the last itmes of the context menu... This makes an UI so much more consistent.\n\ncheers"
    author: "cies"
  - subject: "Re: PROPERTIES"
    date: 2003-09-26
    body: "Do you use HEAD? Properties is almsot always last. The only case I can't think of is sometimes in the sidebar. That looks like a slight bug though."
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Here's what I do: \n\n- I assigned Ctrl-U to \"View document source\" (same as in Moz)\n- I assigned Shift-Ctrl-U to \"View frame source\" \n\nand I won't cry a single tear when the two options \nare removed from the menu although I really need the two actions \nregularly.\n\nI wholeheartedly agree that the right mouse \nbutton menu has become way too large. \n\n\n\n"
    author: "cm"
  - subject: "Re: View Source"
    date: 2007-01-20
    body: "how do you assign those keys?!?!"
    author: "jon"
  - subject: "Re: View Source"
    date: 2007-01-21
    body: "In a Konqueror window: \nSettings menu -> Configure Shortcuts\n\n"
    author: "cm"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "You are forgetting one important usability axiom:\nOptimize for intermediates\nIf you are looking for browser identification scripts, you are an expert user. A program/system should be open for expert use but not designed for it from ground up. Care shoud be taken, as it is here."
    author: "Daniel Karlsson"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "> That's some strange reasoning, if you don't mind me saying so. You don't use it, so you assume nobody else does?\n\nThat's not the point. The feature is still there; just not in the context menu. It's an advanced feature that even most intermediate/expert users just won't use. Stop Animations is gone from the context menu in HEAD now (change was made after alpha2 was released), but something like Set Encoding _will_ be used by a lot of intermediate users, as many languages need this feature. "
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "I am not a web developer and yet I find the feature very useful in the context menu.  So that disproves your entire premise.\n\nIt's just like Mail.  You always want easy access to the source.\n\nBy removing this feature from Konqueror you have disrupted my workflow for nothing.  Meanwhile there is useless stuff like \"Security...\" in the menu."
    author: "anonymous"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "> I am not a web developer and yet I find the feature very useful in the context menu. So that disproves your entire premise.\n\nI said nothing about web developers. I just said that most intermediate/expert users won't need to use this. Most don't. This is all about compromises. Not everybody is going to get what they want. \n\n> I am not a web developer and yet I find the feature very useful in the context menu. So that disproves your entire premise.\n\nJust assign a shortcut key, or perhaps a mouse gesture through khotkeys2. \n\nIf you're enough of an expert to read the source of something, you should be expert enough to do this."
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "This is about KDE usability.  \n\nThis is not about silliness like editing rc files or shortcuts or mouse gestures.  Nobody should have to be an expert to have a conveniently usable KDE."
    author: "anonymous"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "> This is not about silliness like editing rc files or shortcuts or mouse gestures. Nobody should have to be an expert to have a conveniently usable KDE.\n\nYes, but the point remains that most people won't need view source, and thus, it will not be in the context menu. \n\nUsability is all about compromises, we could stuff a number of things in the context menus that are only applicable to a select group of expert users, but we don't since it hurts the efficiency (this has been proved over and over again in usability studies) of the rest of us.\n\nI would in fact advocate 100 item context menus if it didn't hurt efficiency.  "
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-27
    body: "I think there is enough proof that removing View Source was a mistake and broke UI usability for many people.  I don't hear people complaining about anything else having been removed.\n\nFor god's sake just remove \"Security...\" and replace it with View Source.  :-)"
    author: "anonymous"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "The developers don't agree at all.  Look at kdedevelopers.net and you'll find a lot that disagree."
    author: "anonymous"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "KDE Developers != Konq Developers/People who contribute to Konqueror"
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "I cannot understand that either, in times where spam, worms spread like nothing else, you cannot even look at the mail source??????\n\nI often get mails that look empty, looking behind the scene shows, it's a self generating worm, that generates an exe if you using  IE, looking at the source reveals it.\n\nI'm not using KMail and won't if they think about small menus, look at all the other mail programms, and I have never heard about the fact that someone said they have to many entries in the menu."
    author: "Lord"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Read again! This isn't about KMail!"
    author: "anonymous"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "I don't use view source that much and I'm all for making the context menus smaller. I would prefer to have this option available via right click but I don't think it is a huge problem to have to access it via konqueror menus or with some shortcut - since many people are complaining about it, assigning a default shortcut at least would be positive.\n\nI do think however that kde apps should be as consistent as possible, and even though they are very different, IMO it would be positive if the source of emails (kmail), ng posts (knode), and web pages (konqueror), could be accessed in similar ways. Since I really want to keep this option accessible in kmail and knode, for sake of consistency, I think it would be nice to keep it in konqueror too.\n\n"
    author: "Sleepless"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "> since many people are complaining about it, assigning a default shortcut at least would be positive\n\nAlready done in CVS."
    author: "Anonymous"
  - subject: "Clee"
    date: 2003-09-26
    body: "Did clee's excellent patches to the context menu make ti in? His patches removed a lot of redundant and useless things from the cotnext menu such as 'Stop Animations\u0092 and \u0091Set Encoding\u0092 entries taken out, as well as rearranging the order of the link-related menu items.\n\nI REALLY HOPE THIS MADE IT In, this is perhaps one of the best things that could be done to that crazy context menu. If you remvoe the 'View Source' than there is definitely no arguement for 'Stop Animations' and 'Set Encoding', these aren't even context sensitive, view source at elast was context sensitive to the frame it was in. Come on, you got to be kidding, I've never sued these features and nobody I know has, they don't belong in the context menu!!!\n\nMore here: http://www.kdedevelopers.org/node/view/194\n\nIf you agree, please say so here too: http://bugs.kde.org/show_bug.cgi?id=53772"
    author: "Mike"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "The patches indeed are great. I don't see \"View Document Source\" the same way, but the resulting menu is good enough even with them. Are the \"Image\" and \"Link\" context menus editable in the same way? They don't seem affected by the patches, they have some items that are removed in the \"Frame\" menu."
    author: "Rayiner Hashem"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "So ar ethey in CVS or not?? Is there still a 'Stop Animations\" and 'Set Encoding\" option in the context menu for example?"
    author: "Mike"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "Set Encoding will likely stay, just not for links or images. It'll only appear if you click on the background.\n\nStop Animations will likely be removed before b1. "
    author: "anon"
  - subject: "Set Encoding"
    date: 2003-09-26
    body: "I really hope that will stay. I'm currently building Alpha2, but I don't remember it being missing from Alpha1.\nSet encoding is probably the item I use most fron the context menu - more then \"save as\" or \"view source\". people in Europe and North America just can't appreciate how important it is - one of the only two reasons I don't use Mozilla (or derivatives) as my main browser is because it lacks this in the context menu (the second being its mind boggingly slowness)."
    author: "Guss"
  - subject: "At least 'Stop Aimations\""
    date: 2003-09-27
    body: "won't be in the context menu, good riddance, it doesen't belong there.\n\nHowever, neither does set encoding, that's also rarely used and should notbe there."
    author: "mike"
  - subject: "Re: At least 'Stop Aimations\""
    date: 2003-09-27
    body: "> However, neither does set encoding, that's also rarely used and should notbe there.\n\nNo that's used a lot in non-East/Central European Languages. "
    author: "AC"
  - subject: "Re: At least 'Stop Aimations\""
    date: 2003-09-27
    body: "er, I meant non-West/Centeral European Languages"
    author: "AC"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "What!!\nYou want to remove \"stop animation\"?\nThis is the feature I prefer in konqueror as a web browser!\nI generally like to have the animated gif in web pages, but sometimes you have one that is horrible or load lots of CPU, and I am happy to have a quick entry in the menu to stop it."
    author: "renaud"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "It looks like in current HEAD \"Stop Animations\" was removed from context menu without adding it to the menus or toolsbar by default."
    author: "Anonymous"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "> It looks like in current HEAD \"Stop Animations\" was removed from context menu without adding it to the menus or toolsbar by default.\n\nI guess somebody could add it to a menu, but remember it's still in Konqueror's preferences under Web Behavior. "
    author: "anon"
  - subject: "YES"
    date: 2003-09-27
    body: "That's good, it didn't really belong tehre, I think it should be in the 'View' menu or be part of waht the 'stop loading' option does. Anyway, are you saying that now it's \\not in Konqueror at all?\n\nI think it shouldn't be in Konqueror at all until it gets flash support, since users might think its buggy when tehy click stop animations and the flash animations keep on flashing..."
    author: "Alex"
  - subject: "Re: YES"
    date: 2003-09-27
    body: "it's in the prefs"
    author: "AC"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "I think what would make more sense there is if it were truly context-sensitive i.e. it only appears in the context menu when the mouse is over an aminated pic.\n\nThat way you can disable individual animations, which you cannot do at the moment."
    author: "Stephen Douglas"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "Yes, that would be a good feature for KDE 3.3. It'd of course need someone willing to implement it :)!"
    author: "AC"
  - subject: "Re: Clee"
    date: 2003-09-26
    body: "\"Set Encoding\" is very usefull for cyrillic pages, \nwhere konq often uses wrong encoding,\nand I hope \"Stop Animation\" will not be revimed too from the context menu.\n"
    author: "Dmitry"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "View source isn't something very used except by developpers and by people who can understand it.  Since it's not the case for the vast majority of computer users, then it should not be present in the context menu.  Anyway, it's rarely necessary to view a web page source when it's properly rendered."
    author: "Dominic"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "View source is a huge factor in katering to the web development types. since Konqueror currently (and probably for a long time to come) requires some tweaking to work properly with very dynamic web sites (which are becoming more and more common these days), and you want Konqueror to support all these web sites - the you must cater to the web developer.\n\nMozilla currently has the best web development experience, with JavaScript console, debugger, dom-inspector and cookie manager (that can manage cookies' content). while Konqueror has some of these (kudos for the beginning of a java script debugger !), it falls behind even IE with its support for web developers and frankly not having \"view source\" in the context menu could be the last nail to drive web developers away.\n\nWhile in most cases \"View source\" can be achieved by keyboard shortcuts and using the \"View\" menu, having it on the context meu is essential when debugging dynamic multi-frame sites.\nI'm doing most of my web development on Mozilla even if its almost unusable (speed-wise) on my computer, simply because Konqueror lacks the developer support I need. as a result adapting sites to Konqueror is slower and messier (includes a lot of shotgun debugging and 'fix-type, reload' cycles) which under most circumstences just isn't worth it.\n\nThe way to go is give more developer support in the browser - not less - because that's the way you win browser wars: developers cause users, bad developer supports drive users away. Microsoft has implemented it nicely in IE and now Netscape/Mozilla had learned the lesson. when will Konqueror developers graduate ?"
    author: "Guss"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "> While in most cases \"View source\" can be achieved by keyboard shortcuts and using the \"View\" menu, having it on the context meu is essential when debugging dynamic multi-frame sites.\n\nDo you use any of the alphas? \"View Frame Source\" is in the frame submenu. Too many people commenting about alphas without using them :(\n\n\n>  While in most cases \"View source\" can be achieved by keyboard shortcuts and using the \"View\" menu, having it on the context meu is essential when debugging dynamic multi-frame sites.\n\nHmm, stop being so fatalist. Web developers are not going to stop testing in konq because of this. Just like they won't stop testing in things like Safari or Epiphany, both of which also don't have it. It's still in the menus. It still can be assigned a shortcut key. It isn't in the context menus anymore because most ordinary users just won't use it-- and they shouldn't have to use it. \n\nIt's not like if we are removing the feature all together.  Most current browsers only put it there for historical reasons. Viewing source is a debugging tool. The context menu is most definatly not a place for debugging tools. What do you propose next? putting the javascript debugger in the context menu?"
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "\"and frankly not having \"view source\" in the context menu could be the last nail to drive web developers away.\"\n\nYeah because it's SOOOOO difficult to assing a shortcut for it or access it through the view-menu!\n\n\"While in most cases \"View source\" can be achieved by keyboard shortcuts and using the \"View\" menu, having it on the context meu is essential when debugging dynamic multi-frame sites.\"\n\nToo bad that most people use Konqueror for regural web-surfing and not \"debugging dynamic multi-frame sites\". If everyone had the entires they wanted in the context-menu, it would be so huge that it wouldn't fit the screen! For most users, \"view source\" is useless, so it was removed. Get over it."
    author: "Janne"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "\"Yeah because it's SOOOOO difficult to assing a shortcut for it or access it through the view-menu!\"\n\nThe context menu is better because you can view the actual source, no matter if it's a frameset or a normal page. With a \"shortcut\" (this is no longer a shortcut) you would see a useless frameset and then have to use the contextmenu anyway to view the frame source.\n\n\n \n"
    author: "Roland"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "So, on one hand every demands that context-menues must be less cluttered. The developers listen and start removing less used entries, and everybody whines that \"Yes, remove entries but not THAT entry!\".\n\nIf you remove entries from the context-menus, you are going to step on some toes. You just have to step on as few toes as possible"
    author: "Janne"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "\"So, on one hand every demands that context-menues must be less cluttered.\"\n\nNot everybody demands that. The vast majority of users doesn't care wether a context menu has 10 or 20 entries as long as the ofen used ones are on top.\n\nI always see \"the average user finds that confusing\", never \"I find that confusing\", it's *always* the self-proclaimed usability experts that demand removal of stuff they don't like, never the users themselves.\n"
    author: "Roland"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "7 +- 2 choices/items is what the human brain usually can get grasp of just by glancing at them. More and you have to stop and read them through. The latter is not acceptable because context menus should enhance your flow and not hindering it.\n\nAnd please remember that usability is a professional work also. A very important one often overlooked by programmers. A lot of projects have failed just because of bad design and no firm grasp on who the end usersa are.\n\n"
    author: "Daniel Karlsson"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Take a look at the UIs of some professional apps. They generally have context menus with a small number of items. Mathematica, for example, has 10 items in its main context menu. Maya has two types of context menus, reguler and radial. Reguler menus have about half a dozen entries. Radial ones have more, but they are arranged in sub-sections in a circle around the cursor, so there are no more than about half a dozen in each spatial location. Its not a matter of new users being confused! Its a matter of efficiency. The workflow of these professional apps is incredible, partially because of good UI design. Making menus that can be used by glancing, rather than reading, is a part of that good design."
    author: "Rayiner Hashem"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Like another reader wrote before:\n\n\"Do you use any of the alphas? \"View Frame Source\" is in the frame submenu. Too many people commenting about alphas without using them :(\"\t\n \n \n"
    author: "Chony"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "You don't get it, right?\n\nOn many webpages a frameset isn't obvious.\n\nSo far, I had to check one menu (the context menu), both framed and non-framed sources could be viewed here.\n\nNow it got inconsistent and you have to check two different places. (The menubar and the context-menu)\n\nThis takes longer, it's inconsistent and unintuitive.\n"
    author: "Roland"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "If you're smart enough to be a web developer or somebody who knows how to use View Document Source, you can probably tell if a site is using frames. \n\nThe problem with the frame menu is that it can't be added to the main menus in any reasonable fashion. View Document Source can."
    author: "anon"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "I don't see your problem.\n\nJust assign a key to \"view frame source\", \n(I use Ctrl-Shift-U, for instance)\n\nThen, on a page with frames: \n\n- left click the area of your interest\n- press the key combo\n\nworks like a charm for me\n\n(Besides, as others have pointed out, \nthe option *hasn't* been removed)"
    author: "cm"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "And, in case there aren't any frames on the page\nnothing will happen.  Just press the key combo \nfor \"View page source\" then.\n"
    author: "cm"
  - subject: "Re: View Source"
    date: 2004-03-05
    body: "how do u assign a key combo to view source?"
    author: "alex"
  - subject: "Re: View Source"
    date: 2004-03-05
    body: "As usual, \"Settings/Configure Shortcuts...\""
    author: "Anonymous"
  - subject: "Re: View Source"
    date: 2004-03-05
    body: "please could u explain further? is that an option in windows xp, ive got 2000"
    author: "alex"
  - subject: "You missed my point entirely"
    date: 2003-09-26
    body: "and I really don't think I hid it that well. so here it is spelled:\n\n- Removing features that make web developers life easier will cause web developers spend less time working with Konqeuror (unwanted effect no.1).\n- Spending less time with Konqueror will cause web sites to be less compatible with Konqueror [1].\n- Having less web sites that support Konqeuror means that less users will choose it as the browser of choice (They'd always have Mozilla based browsers).\n\nso - making the browser less \"web-developer friendly\" will make sure that it will also be less \"luser friendly\", so removing the view source shortcut from the context menu will ultimatly cause more \"simple\" (non developer) users to stop using Konqueror as well[2].\n\nQ.E.D\n\n[1] ooohh! I can do this neat trick with Mozilla! lets see if IE can supprot it ? why, yes it does! ok - let's put it in. Who wants to check if konqi does it as well ? nobody? shacks, I'll guess we'll just let the users find out then.\n\n[2] not like I invented something new here - this is how Microsoft beat Netscape, which was my whole point. Its a common enough mistake to think that they did that because they gave the browser away for free integrated in the OS. the real reason that they won, as any person who'se been in the real web development market in the last 5 years can tell you, is because they made life easier to the developers, and as a result harder for Netscape users. I don't mind downloading a 7 MB browser, or getting it on a free CD, but I won't install it if using it means having less accessability to web sites."
    author: "Guss"
  - subject: "Re: You missed my point entirely"
    date: 2003-09-26
    body: "\"- Removing features that make web developers life easier will cause web developers spend less time working with Konqeuror (unwanted effect no.1).\"\n\nAdding features that regural users want (for example: less cluttered context-menus) would increase the userbase alot more.\n\nAnd how has this feature been \"removed\"? You can't assing a shortcut-key for it? You can't use it through the view-menu?\n\nAnd why couldn't those web-developers write standard HTML for crying out loud?!?!\n\n\"so - making the browser less \"web-developer friendly\" will make sure that it will also be less \"luser friendly\",\"\n\nHate to break this to you, but the world does not revolve around \"web-developers\". For vast majority of users, \"view source\" is unneeded for most of the time.\n\nAnd calling users \"lusers\" is just a tad elitist.\n\nHell, regural users kept on insisting that KDE needs less cluttered context-menus. And now that they are being cleaned up, people whine even more. You can't please everyone. But it seems that there are more people who want cleaned-up menus than there are web-developers who insist on having \"view source\" in the menu. Just because it's not in the context-menu does NOT mean that it's not there. If the developer is so lazy that he refuses to access it through alternative methods (view-menu, keyboard-shortcut) that really is his problem, not KDE's."
    author: "Janne"
  - subject: "You didn't read a single thing I said, didn't you?"
    date: 2003-09-26
    body: "1. \"View source\" not in the context menu is useless in frames.\n2. You can't make the type of dynamic web sites people expect today with \"standard HTML\" - you have to use modern DOM2/CSS2 and non-standard extensions, which are browser dependant. even the standard stuff does not have full or even partial support the same way in all browsers.\n\nIn an ideal world you'd be right. unfortunatly, we are not living in an ideal world."
    author: "Guss"
  - subject: "Re: You didn't read a single thing I said, didn't you?"
    date: 2003-09-26
    body: "Actually if you go above CSS1 in the current line of browsers, Mozilla is going to be about the only player that will support you.  There is not alot of CSS2 yet."
    author: "Paul Seamons"
  - subject: "Re: You didn't read a single thing I said, didn't you?"
    date: 2003-09-26
    body: "If you are using frames, you are not using modern web design standards.  Frames are obsolete and are the work of the devil anyway.  Get over it.\n\n\nsmeat!"
    author: "SMEAT!"
  - subject: "Re: You didn't read a single thing I said, didn't you?"
    date: 2003-09-28
    body: "1. Maybe that's why there is a \"View Frame Source\" option in the Frame submenu?\n\nAre you using HEAD, or one of the Alphas?"
    author: "Henrique Pinto"
  - subject: "Re: You missed my point entirely"
    date: 2003-09-26
    body: "> - Removing features that make web developers life easier will cause web developers spend less time working with Konqeuror (unwanted effect no.1).\n\nPlease stop acting like if this feature has been removed. It hasn't, and never will be. \n\nI have a few friends who are web developers, and their favorite browser is Safari (yeah, they are Mac users).. guess what? Safari doesn't have View Source in it's context menu either. I don't see flocks of web developers abandoning Safari. I don't think somebody who has never used Konqueror will even notice either."
    author: "anon"
  - subject: "Re: You missed my point entirely"
    date: 2003-09-27
    body: "You've obviously never used Safari.\n\nIf you right-click on the document background in Safari one of the only two context menu items is 'View source'.\n\nI think that this is a fairly strong argument for including it, personally. But others obviously don't agree with me.\n\nOh well."
    author: "clee"
  - subject: "Re: You missed my point entirely"
    date: 2003-09-28
    body: "DUDE, WE AGREE WITH YOU.\n\nWe are clamoring for it.  PLEASE ADD IT BACK."
    author: "anon"
  - subject: "Re: You missed my point entirely"
    date: 2003-09-26
    body: "Oh please...\n\nFirst of all, the frame source can still be viewed through the context menu.\n\nSecond of all, you can assign shortcut keys to both \"view document source\" and \"view frame source\".\n\nI don't see your problem, and i happen to be a (web) developer. I guess i work differently from you, considering i rarely need to view the document source, and i never use frames so that is one problem less as well.\n\n> [1] ooohh! I can do this neat trick with Mozilla! lets see if IE can supprot it ? why, yes it does! ok - let's put it in\n\nHeh, sounds like a real good way to develop quality software... :)"
    author: "Troels Tolstrup"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "> The way to go is give more developer support in the browser - not less - because that's the way you win browser wars: developers cause users, bad developer supports drive users away. Microsoft has implemented it nicely in IE\n\nThe right way is not to make web developers optimize their pages to a special browser and lock them into it like Microsoft did. The right way is to encourage web developers to use and adhere non-proprietary HTML standards.\n "
    author: "Anonymous"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "That would be nice if it would work that way - \"the nice thing about standards is that there are so many of them to pick from\".\n\nDifferent browser implement different subsets of the standards, and Konqeror is a weird half-breed: it implements a subset of the standard W3C standard APIs, a lot less then Mozilla, and also some of the non-W3C standard IE extensions. what you get is a browser that while providing most of the capabilities you need to design a really neat web site, does not fully support \"standard\" (i.e. Mozilla based) techniques, nor \"non-standard\" (i.e, IE based).\nAs a result, I have to write \"cross-platform\" code that detects Mozilla, IE and konqueror and uses the right call for each.\t\n\nWhile the lowest common denominator is constantly slowly rising, it is always far below the curve, and to get the most effect from your dynamic website you need to make the code specialized. currently its much harder to do for Konqueror then for the competitors, and I'm not surprised to see that most interesting uses of DHTML in the wild won't work on Konqueror."
    author: "Guss"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "\"while Konqueror has some of these (kudos for the beginning of a java script debugger !), it falls behind even IE with its support for web developers and frankly not having \"view source\" in the context menu could be the last nail to drive web developers away.\"\n\nExactly. And without anybody testing Webpages for Konqueror all Konqueror users will suffer.\n\nDevelopers are the most important users. Any platform alienating developers will die sooner or later (mostly sooner).\n\n\n \n"
    author: "Roland"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "\"Developers are the most important users\"\n\nCough...Cough... Sorry.  Swallowed some water there.\n\nWithout a userbase - any userbase - a developer is just developing for him(her)self.\n\nAND ... the \"web developers\" are not the same as the konqueror developers.  I doubt these changes will drive the konqueror developers away.\n\nAND ... I'm a developer and I feel alienated from Microsoft (and have for 5 to 10 years) - does that mean they are going to die mostly sooner?  Well it will have to be later now since sooner has already happened."
    author: "Paul Seamons"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "You have to understand, \"View Source\" was not removed, and never will be removed.  It's entry in the default shipped context menu was removed.  The feature remains in KHTML.  It is implemented as an \"Action\" and you can add it to the context menu with a text editor if you want!  Just edit the .rc file.  You will find that you can do lots of cool things like that with KDE apps.  You can also assign a keyboard shortcut to the action, saving yourself having to use the mouse even."
    author: "George Staikos"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "All these problems about what should be in the context menu would probably be reduced to nothing if the menu was configurable. A bit like toolbars usually are. One could then choose a nice subset of all available actions and all this \"flaming\" would be done with.\n"
    author: "Chakie"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "I guess that's a good compromise that would\nsatisfy the people who think they cannot live without it...\n"
    author: "cm"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "I agree. I suggested that to the kfm-devel list earlier\n\nhttp://lists.kde.org/?l=kfm-devel&m=105683726420202&w=2urce\n\nwhen the question was how delete should be represented in the menu. How does that look in the alpha by the way?"
    author: "Claes"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Shred was completely removed from konqueror altogether (both in context menus, the main menus, toolbars, etc...)\n\nBoth \"delete\" and \"move to trash\" are there. Unifying them will probably require the trash:/ kioslave that's planned for kde 3.3 (we can have configurable smart trash emptying policies)"
    author: "AC"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "They are configurable.  Edit the .rc file."
    author: "George Staikos"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "Mozilla does this.  If that's worth it to you, consider upgrading to Mozilla."
    author: "anonymous"
  - subject: "Re: View Source"
    date: 2003-09-26
    body: "\"Upgrade?\""
    author: "George Staikos"
  - subject: "Re: View Source"
    date: 2003-09-28
    body: "LOL sounds liek the MS IE webpages.\n\nPlease upgrade your brwoser to IE 5 or 6 to view this page.\n\nLOL considering that if you are using Opera 7.2 or anything Mozilla based this is pretty much a  downgrade, that's pretty funny.\n\nHowever, KHTMl while it has many advnatages to Gecko, when it comes to pure rendering Gecko is better at least in my experience."
    author: "Alex"
  - subject: "Re: View Source"
    date: 2003-09-29
    body: "Why not using Mozilla's context menu organization? With all Frame-oriented functions in the context menu but group and isolated in one voice. I find it an extremely intuitive and clean way to work with frames, and would solve the current view document/frame source issue."
    author: "Vajsravana"
  - subject: "Dumb question (was View Source)"
    date: 2003-10-01
    body: "With all this clamouring about the context menu, why not do the obvious -- make the stupid thing user configurable.  All of the other KDE menus are user configurable except the context menu.  Personally, this drives me crazy.  I would really really like to have a more configurable context menu.\n\nAlso, Copy is and has been notoriously absent from Konq's context menu.  Will this get introduced in 3.2?\n\nOf course if we had configurable context menus, this wouldn't be such a problem. ;)"
    author: "Surak"
  - subject: "Who's Tereza?"
    date: 2003-09-26
    body: "Konqi's girlfriend?"
    author: "Max Howell"
  - subject: "Re: Who's Tereza?"
    date: 2003-09-26
    body: "Perhaps Coolo's? Katie is Konqui's girlfriend: http://women.kde.org/pics/katie.jpg"
    author: "Anonymous"
  - subject: "Re: Who's Tereza?"
    date: 2003-09-28
    body: "Hey..\nI was there :-)\nTereza is one of the girls that work at the our nightly stop pub on N7Y :-)\n( If i'm making a mistake, please correct me )"
    author: "Helio Chissini de Castro"
  - subject: "KWin rewrite - where is Plastik ?"
    date: 2003-09-26
    body: "I'm all for rewrite, but I was surprised and disappointed to see the neat Plastik kwin style vanish as Alpha1 gave way to Alpha2. I like it so much better then Keramik and I really really hope it will be in the release and hopefuly before the beta (I'm using kontruct to build from HEAD when its stable enough to compile, so I'd like to be able to get Plastik from there)."
    author: "Guss"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-26
    body: "I beleive it's in the process of being written for kwin-HEAD. It's on the feature list for 3.2 and probably will be. Perhaps by beta1 :)"
    author: "anon"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-26
    body: "In current CVS it is already ported to the new KWin API."
    author: "Anonymous"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-26
    body: "Plastik is just great. I really hope so much that the kde team will make this the default theme for 3.2...!! "
    author: "Anonymous"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-26
    body: "Won't happend...Maybe KDE 4.0."
    author: "anonymous"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-26
    body: "It can't be, since we just changed the default theme for kde 3.1. We generally don't want to change it more than once in minor releases. \n\nWho knows in kde 4, though. BTW, the concept of a default theme in very much lessened in KDE anyways, since kpersonalizer is run at startup. Some users will stay with the default, but other options are there."
    author: "anon"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-26
    body: "Plastik needs to be fixed. It looks good on reguler mid-res CRTs, but has poor contrast on LCDs, and the toolbars feel too narrow at higher resolutions."
    author: "Rayiner Hashem"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-27
    body: "I have an lcd and I haven't changed the colorset.\nBut when I instaled plastik on my father's session, I thought it would be too bright for him, so I used the KDE default colorset in Control Center instead of Plastik's (under color, in apearance)."
    author: "jukabazooka"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-27
    body: "and also increased contrast of buttons (under colors too).\nI have no opinion if the default color and contrast default should be changed. I like it for me, though."
    author: "jukabazooka"
  - subject: "Re: KWin rewrite - where is Plastik ?"
    date: 2003-09-27
    body: "Why should Plastik be included ? Plastik and Thin Keramik are, for the most part, nothing but modified versions of Alloy. They don't include any variations of the crystal icon set - they include the original. I think the same repsect should be shown for Alloy."
    author: "Chris Spencer"
  - subject: "How do you enable desktop switching buttons?"
    date: 2003-09-26
    body: "I know this is off topic, but this has been really annoying me for a while, and the only place where someone would know the answer to this question would be a KDE forum.\n\nI'm using KDE 3.1. I was messing with the Panel config in KControl, and with the applet lists. Somehow I managed to remove the desktop switching buttons that always show in kicker, and now I can't figure out how to make them show up again. I have multiple desktops enabled, and I can switch between them with Ctrl-Tab, but the buttons are not there. Any ideas?\n\n-George Vulov"
    author: "George Vulov"
  - subject: "Re: How do you enable desktop switching buttons?"
    date: 2003-09-26
    body: "It's under miniprograms->switcher"
    author: "DP"
  - subject: "Re: How do you enable desktop switching buttons?"
    date: 2003-09-26
    body: "Isn't it Add->Applet->Pager ?"
    author: "Thomas"
  - subject: "Re: How do you enable desktop switching buttons?"
    date: 2005-05-08
    body: "I want to recover my active desktop   :("
    author: "swordf1sh"
  - subject: "Re: How do you enable desktop switching buttons?"
    date: 2003-09-26
    body: "right click on the kmenu->panel menu->add->applet->Pager"
    author: "anon"
  - subject: "Re: How do you enable desktop switching buttons?"
    date: 2003-09-26
    body: "> the only place where someone would know the answer to this question would be a KDE forum\n\nHow correct you are, so go to http://kde-forum.org/ and stop posting off-topic here."
    author: "Anonymous"
  - subject: "Why is kontact sooo bugy?"
    date: 2003-09-26
    body: "IMHO is kontact the important thing on KDE3.2. Full Kolab support is needed!"
    author: "Stephan"
  - subject: "Re: Why is kontact sooo bugy?"
    date: 2003-09-26
    body: "\"Why is Kontakt so buggy?\"\n\nIn case you didn't notice, these are ALPHA releases! And that means that they are not yet finished. They aren't even beta yet, there are bound to be lots and lots of bugs!"
    author: "Janne"
  - subject: "Re: Why is kontact sooo bugy?"
    date: 2003-09-26
    body: "Because you are probably unable to read. I clarified Kolabs status in like the second post. And yes, Kolab support will be in.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "can't compile kdemultimedia"
    date: 2003-09-26
    body: "make[5]: Entering directory `/opt/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1.92/kscd'\nif g++ -DHAVE_CONFIG_H -I. -I. -I.. -I.. -I/opt/kde3.2-alpha2/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT -I/opt/kde3.2-alpha2/include -I/usr/X11R6/include -I/opt/kde3.2-alpha2/include -I/usr/X11R6/include -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -O2 -I/opt/kde3.2-alpha2/include -I/usr/X11R6/include -L/opt/kde3.2-alpha2/lib -L/usr/X11R6/lib -O2 -pipe -I/opt/kde3.2-alpha2/include -I/usr/X11R6/include -L/opt/kde3.2-alpha2/lib -L/usr/X11R6/lib -O2 -pipe -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT kvolumecontrol.o -MD -MP -MF \".deps/kvolumecontrol.Tpo\" \\\n  -c -o kvolumecontrol.o `test -f 'kvolumecontrol.cpp' || echo './'`kvolumecontrol.cpp; \\\nthen mv -f \".deps/kvolumecontrol.Tpo\" \".deps/kvolumecontrol.Po\"; \\\nelse rm -f \".deps/kvolumecontrol.Tpo\"; exit 1; \\\nfi\nkvolumecontrol.cpp: In member function `virtual bool\n   KVolumeControl::eventFilter(QObject*, QEvent*)':\nkvolumecontrol.cpp:69: error: `KeyPress' undeclared (first use this function)\nkvolumecontrol.cpp:69: error: (Each undeclared identifier is reported only once\n   for each function it appears in.)\nmake[5]: *** [kvolumecontrol.o] Fehler 1\nmake[5]: Leaving directory `/opt/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1.92/kscd'\nmake[4]: *** [all-recursive] Fehler 1\nmake[4]: Leaving directory `/opt/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1.92/kscd'\nmake[3]: *** [all-recursive] Fehler 1\nmake[3]: Leaving directory `/opt/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1.92'\nmake[2]: *** [all] Fehler 2\nmake[2]: Leaving directory `/opt/konstruct/kde/kdemultimedia/work/kdemultimedia-3.1.92'\nmake[1]: *** [build-work/kdemultimedia-3.1.92/Makefile] Fehler 2\nmake[1]: Leaving directory `/opt/konstruct/kde/kdemultimedia'\nmake: *** [dep-../../kde/kdemultimedia] Fehler 2\n"
    author: "razor"
  - subject: "Re: can't compile kdemultimedia"
    date: 2003-09-26
    body: "I get the very same error on SuSE 8.2\n\nAny ideas how to work around this?"
    author: "Roland"
  - subject: "Re: can't compile kdemultimedia"
    date: 2003-09-26
    body: "Open kvolumecontrol.cpp, go to line 69, change \"KeyPress\" into \"QEvent::KeyPress\", save."
    author: "Frerich Raabe"
  - subject: "Re: can't compile kdemultimedia"
    date: 2003-09-26
    body: "Configure with --enable-final and compile kscd/ separately."
    author: "Anonymous"
  - subject: "Re: can't compile kdemultimedia"
    date: 2003-09-26
    body: "I don't mean to bitch but this really isn't the place for reporting bugs.  That is what the mailing lists are for. "
    author: "anonymous"
  - subject: "Re: can't compile kdemultimedia"
    date: 2003-09-26
    body: "Why not? Many people reading this thread are going to Konstruct the named release.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: can't compile kdemultimedia"
    date: 2003-09-26
    body: "And Konstruct got a fix in today's version."
    author: "Anonymous"
  - subject: "desktop switching"
    date: 2003-09-26
    body: "last time i tried, switch which mouse wheel doesn't work when view icons on the desktop is disabled (because i like to waste space and stuff)."
    author: "not_registered"
  - subject: "Re: desktop switching"
    date: 2003-09-26
    body: "which = with\nyou better believe it."
    author: "not_registered"
  - subject: "Re: desktop switching"
    date: 2003-09-26
    body: "Yeah, I've noticed the same thing? Have you done a wish on bugs.kde.org about this?"
    author: "Teemu Rytilahti"
  - subject: "Re: desktop switching"
    date: 2003-09-26
    body: "How about searching first? http://bugs.kde.org/show_bug.cgi?id=59488"
    author: "Anonymous"
  - subject: "Re: desktop switching"
    date: 2003-09-26
    body: "Sure :)"
    author: "Teemu Rytilahti"
  - subject: "Re: desktop switching"
    date: 2003-09-26
    body: "When this came up a long time ago I implemented desktop switching by using the mouse over the pager as sort of a compromise."
    author: "Scott Wheeler"
  - subject: "\"Active Desktop\"... does it exist in KDE 3.x?"
    date: 2003-09-26
    body: "i remember it did work in one of the 2.x KDE versions, but not so in 3.x\nsame applies to world-watch as background, and other stuff... why was this functionality removed (if it was, or else how do i activate it), and will it see a \"comeback\" some time (soon)?\n"
    author: "MaxPayne"
  - subject: "Re: \"Active Desktop\"... does it exist in KDE 3.x?"
    date: 2003-09-26
    body: "that's something i'll also like to have"
    author: "Heinrich Wendel"
  - subject: "Re: \"Active Desktop\"... does it exist in KDE 3.x?"
    date: 2003-09-26
    body: "If we talk about the same, I'm using it (3.1.x):\n\nActivate it via: kcontrol -> Desktop -> behaviour -> check: Programs in the desktop windows\n\nThan goto:\nLook and Feel -> Background -> tab background -> in the drop down menu\nselect background program\nAfter that a little lower is the select or adjust and select your background program there.\n\nHope this is what you're looking for....\n\n(the menu and tab names are translated into english you need to find the correct ones\nyourself)\n"
    author: "Richard"
  - subject: "Re: \"Active Desktop\"... does it exist in KDE 3.x?"
    date: 2003-09-26
    body: "thanks for the info... finally found it:\n\n---\nLookNFeel->Behavior, \"Programs in desktop window\"\n\nthen go to\n\nLookNFeel->Background, tab \"Background\" (first)->\"Setup...\", \"kwebdesktop\"\n---\n\nimporvement could be made in making it more configure-friendly (meny entry, right-click on desktop), and more easily \"parameterizeable\".\n\nthanx again for enlightening me, great work!\n"
    author: "MaxPayne"
  - subject: "Re: \"Active Desktop\"... does it exist in KDE 3.x?"
    date: 2005-08-23
    body: "Is there a way to get the same or close to the same functionality from kwebdesktop as you get from Active Desktop. Specifically Kwebdesktop does not seem to show graphics from the web and it would be nice if I could click on links on the desktop. Are things these possible?"
    author: "Cyoung"
  - subject: "umbrello won't compile"
    date: 2003-09-26
    body: "if /bin/sh ../../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../../.. -I/home/wesm/kde3.2-alpha2/include -I/usr/share/qt3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT -I/home/wesm/kde3.2-alpha2/include -I/usr/share/qt3/include -I/usr/X11R6/include -I/home/wesm/kde3.2-alpha2/include -I/usr/share/qt3/include -I/usr/X11R6/include  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -O2 -I/home/wesm/kde3.2-alpha2/include -I/usr/share/qt3/include -I/usr/X11R6/include -L/home/wesm/kde3.2-alpha2/lib -L/usr/share/qt3/lib -L/usr/X11R6/lib -I/home/wesm/kde3.2-alpha2/include -I/usr/share/qt3/include -I/usr/X11R6/include -L/home/wesm/kde3.2-alpha2/lib -L/usr/share/qt3/lib -L/usr/X11R6/lib -O2 -pipe -mcpu=7400 -fsigned-char -maltivec -mabi=altivec -mpowerpc-gfxopt -O2 -pipe -mcpu=7400 -fsigned-char -maltivec -mabi=altivec -mpowerpc-gfxopt -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT libclassparser_la.all_cc.lo -MD -MP -MF \".deps/libclassparser_la.all_cc.Tpo\" \\\n  -c -o libclassparser_la.all_cc.lo `test -f 'libclassparser_la.all_cc.cc' || echo './'`libclassparser_la.all_cc.cc; \\\nthen mv -f \".deps/libclassparser_la.all_cc.Tpo\" \".deps/libclassparser_la.all_cc.Plo\"; \\\nelse rm -f \".deps/libclassparser_la.all_cc.Tpo\"; exit 1; \\\nfi\nIn file included from ClassParser.h:29,\n                 from ClassParser.cc:20,\n                 from libclassparser_la.all_cc.cc:17:\n/usr/include/FlexLexer.h:112: error: redefinition of `class yyFlexLexer'\n/usr/include/FlexLexer.h:112: error: previous definition of `class yyFlexLexer'\nIn file included from ClassParser.cc:20,\n                 from libclassparser_la.all_cc.cc:17:\nClassParser.h: In member function `const char* CClassParser::getText()':\nClassParser.h:133: error: `YYText' undeclared (first use this function)\nClassParser.h:133: error: (Each undeclared identifier is reported only once for\n   each function it appears in.)\nClassParser.h: In member function `int CClassParser::getLineno()':\nClassParser.h:138: error: `lineno' undeclared (first use this function)\nmake[6]: *** [libclassparser_la.all_cc.lo] Error 1\nmake[6]: Leaving directory `/home/wesm/konstruct/kde/kdesdk/work/kdesdk-3.1.92/umbrello/umbrello/classparser'\nmake[5]: *** [all-recursive] Error 1\nmake[5]: Leaving directory `/home/wesm/konstruct/kde/kdesdk/work/kdesdk-3.1.92/umbrello/umbrello'\nmake[4]: *** [all-recursive] Error 1\nmake[4]: Leaving directory `/home/wesm/konstruct/kde/kdesdk/work/kdesdk-3.1.92/umbrello'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/home/wesm/konstruct/kde/kdesdk/work/kdesdk-3.1.92'\nmake[2]: *** [all] Error 2\nmake[2]: Leaving directory `/home/wesm/konstruct/kde/kdesdk/work/kdesdk-3.1.92'\nmake[1]: *** [build-work/kdesdk-3.1.92/Makefile] Error 2\nmake[1]: Leaving directory `/home/wesm/konstruct/kde/kdesdk'\nmake: *** [dep-../../kde/kdesdk] Error 2\n\nAnyone else getting this? I'm running Debian sid / powerpc."
    author: "BassHombre"
  - subject: "Re: umbrello won't compile"
    date: 2003-09-27
    body: "Works fine for me on SuSE 8.2. Change to kde/kdesdk/, remove \"--enable-final\" from Makefile and try again after a \"make garchive clean\"."
    author: "Anonymous"
  - subject: "kwebdesktop vs activedesktop"
    date: 2003-09-26
    body: "\nok, got it working now (see prev. post from me). however there are a few points I have to criticize (which i'd like to be discussed before filing a feature/improvement request):\n\n1. focused on kwebdesktop\n1.1 no interaction w/ webpage\n\n1.2. only one \"window\" (active border), full screen, can run (ok, this can be \"hacked\" by using creating a \"master frame\" which then loads in each frame a website...)\n\n\n2. not just kwebdosk focused:\n2.1. only one \"program\" can run at the same time, even if you deactivate \"Common background\"\n\n2.2. only one program / desktop, full screen, can run\n\n\nthere's prob. lots more that could be done here, i'm looking forward to input!"
    author: "MaxPayne"
  - subject: "I totally agree with what you're saying!!"
    date: 2003-09-27
    body: "That's why I submitted a bug report about that a while back.\n\nCheck it out and vote for it: http://bugs.kde.org/show_bug.cgi?id=62797"
    author: "Alex"
  - subject: "kwebdesktop rendering"
    date: 2004-11-25
    body: "I call a script that fetches a few parts of different pages and load that with kwebdestop, like > sh ./getback && kwebdestop %x %y %f /tmp/tmp.html\n\nneeds a delay or wait til 'done rendering' signal before creating an image from the page.\n\nthe last image on the html page only renders 10% - 20%\n\nwhen I view the tmp.html in konqueror it looks fine.\n\nscreenshot attached."
    author: "Rick Paq"
  - subject: "Re: kwebdesktop rendering"
    date: 2004-11-27
    body: "\nlooks like a clever approach - any idea how to make it fully work?\n"
    author: "max"
  - subject: "Re: kwebdesktop rendering"
    date: 2004-11-28
    body: "loads pics about 80% of the time now\n\nturned up the ht caches (keep synced) via konqueror settings\nseems to help but...\n\nsuperkaramba gives me a nice 5day 4cast\n\nI still use kwebdesktop to show radar images updated hourly as my\ndesktop background. here's a snap."
    author: "Rick Paq"
  - subject: "Konqueror's KHTML?"
    date: 2003-09-26
    body: "Is KHTML engine in Konqueror from KDE 3.2a the one with those Apple's patches? Or the old one?"
    author: "marcoos"
  - subject: "Re: Konqueror's KHTML?"
    date: 2003-09-27
    body: "Don't think it's bundled with all the patches Apple made up till Safari 1.0, but it does have a good number of patches.\n\nAnyway, don't think that Apple's patches ar eso amazing jsut because they have good name recognition, KDE developers ar ethe ones who have done most of the work on KHTML. Apple is not even using the same branch. Remember apple selected KhTML not the other way around. "
    author: "Alex"
  - subject: "Re: Konqueror's KHTML?"
    date: 2003-10-02
    body: "However, why did they create their own branch?"
    author: "branch"
  - subject: "Re: Konqueror's KHTML?"
    date: 2003-09-27
    body: ">  Is KHTML engine in Konqueror from KDE 3.2a the one with those Apple's patches?'\n\nSome of Apple's patches. Plus it's own updated stuff."
    author: "AC"
  - subject: "wow"
    date: 2003-09-27
    body: "I've compiled it using konstruct (great tool, thanks guys!) and it simply rocks so far (only kdebase).\nI'm star to think about replacing the 3.1.3! Way to go kde!"
    author: "Iuri Fiedoruk"
  - subject: "Re: wow"
    date: 2003-09-27
    body: "Good idea! 3.1.4 has been out a while.\n"
    author: "Anonymous Yellowbeard"
  - subject: "Re: wow"
    date: 2003-09-27
    body: "heheheh, I'm going to miss this one and go 3.2 ahead ;)\nFound few bugs in khtml, but so far, it's very stable, so I can use it daily."
    author: "protoman"
  - subject: "Problems running KDE 3.2 Alpha 2"
    date: 2003-09-27
    body: "After hours of compiling, I'm now trying to run KDE 3.2 Alpha 2. When I run startkde from the kde3.2-alpha2 directory, it just loads my old KDE though.\nWhat am I doing wrong? Thanks in advance."
    author: "Z_God"
  - subject: "Re: Problems running KDE 3.2 Alpha 2"
    date: 2003-09-27
    body: "IMHO the best way to test a new KDE release is to add a new unix user (e.g. kdetester) since you have a lot of environment variables to be set.\nEnsure that:\n * PATH contains kde3.2alpha2/bin directory before the main kde3/bin entry\n * KDEDIR is set to kde3.2alpha2\ndo this by adding the following lines to your .bashrc or .profile:\nexport PATH=\"kde3.2-alpha2/bin:\"$PATH;\nexport KDEDIR=\"kde3.2-alpha2\";\n"
    author: "Ruediger Knoerig"
  - subject: "Please give more information sir, "
    date: 2003-09-29
    body: "\n\nWhere can our bashrc or .profile be found?\n\n\nCan you provide a screenshot of you editing this so we can see what it looks like.  I dont mean the commands but I want to know what the proper way to edit a bashrc or profile is, by the way I use Redhat 9."
    author: "linuxNEWBEE"
  - subject: "Re: Please give more information sir, "
    date: 2003-09-30
    body: "In your $HOME directory (~/)\n\nexemple : more ~/.bashrc\n... if you're using bash, of course.\n"
    author: "SANNIER Jean-Baptiste"
  - subject: "The new KNode is just boring"
    date: 2003-09-28
    body: "Each time I open it, KNode will ask me for a \"wallet\" password, or complain \"wallet manager not running\". It is REALLY BORING!. And I cannot find a way to disable KNode looking for its \"wallet\". \n\nI have to say, just because of this boring KNode, KDE 3.2 gives me more nagative impressions."
    author: "Jeff"
  - subject: "List of problems with KNODE."
    date: 2003-09-29
    body: "\n\nSome other bad features of KNODE\n\nFirst, why cant I select the application I want to open a file? With pictures if I dont want them to open inline and I want kuickshow to open them why cant I do this?\n\nWhy cant I just \"launch\" files and not save them? Why cant I preview pictures in thumbnail form inline and click to have it launch fully in my application of choice? I'd like to have all of Free Agents best features, I want better handling of binaries, I'd like the ability to launch movies in one click meaning I highlight the file it downloads, unrars, decodes and begins playing in my application of choice.\n\nId like these features, if any Knode developers are watching thats what we want."
    author: "linuxNEWBEE"
  - subject: "Re: List of problems with KNODE."
    date: 2003-11-15
    body: "Why can't they make Knode works like Xnews.  Xnews is simply, powerful and easy to use."
    author: "Alan"
  - subject: "Re: List of problems with KNODE."
    date: 2003-11-15
    body: "The problem with knode is that for the last three years, there has been a lack of developers, especially compared to kmail. Pretty much a random group of people is developing knode ATM, none of which knode is their main app that they develop. Help is always needed =)"
    author: "anon"
  - subject: "Problems compiling kdeutils (SNMP dependency)"
    date: 2003-09-29
    body: "Here is the error message I get compiling ksim on my RedHat 8.x box with net-snmp-devel-5.0.6-8.80.2 installed:\n\nif /bin/sh ../../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../../.. -I../../../ksim/library -I/usr/local/kde3.2-alpha2/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT -I/usr/local/kde3.2-alpha2/include -I/usr/X11R6/include -I/usr/local/kde3.2-alpha2/include -I/usr/X11R6/include -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -O2 -I/usr/local/kde3.2-alpha2/include -I/usr/X11R6/include -L/usr/local/kde3.2-alpha2/lib -L/usr/X11R6/lib -O2 -pipe -I/usr/local/kde3.2-alpha2/include -I/usr/X11R6/include -L/usr/local/kde3.2-alpha2/lib -L/usr/X11R6/lib -O2 -pipe -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT ksim_snmp_la.all_cpp.lo -MD -MP -MF \".deps/ksim_snmp_la.all_cpp.Tpo\" \\\n  -c -o ksim_snmp_la.all_cpp.lo `test -f 'ksim_snmp_la.all_cpp.cpp' || echo './'`ksim_snmp_la.all_cpp.cpp; \\\nthen mv -f \".deps/ksim_snmp_la.all_cpp.Tpo\" \".deps/ksim_snmp_la.all_cpp.Plo\"; \\\nelse rm -f \".deps/ksim_snmp_la.all_cpp.Tpo\"; exit 1; \\\nfi\nIn file included from ksim_snmp_la.all_cpp.cpp:5:\nsnmp.cpp:33: warning: non-static const member `const int\n   EnumStringMapInfo::enumValue' in class without a constructor\nsnmp.cpp:35: warning: non-static const member `const char\n   EnumStringMapInfo::snmpLibConstant' in class without a constructor\nsnmp.cpp:37: `SNMP_VERSION_1' was not declared in this scope\nsnmp.cpp:38: `SNMP_VERSION_2c' was not declared in this scope\nsnmp.cpp:39: `SNMP_VERSION_3' was not declared in this scope\nsnmp.cpp:42: `SNMP_SEC_LEVEL_NOAUTH' was not declared in this scope\nsnmp.cpp:43: `SNMP_SEC_LEVEL_AUTHNOPRIV' was not declared in this scope\nsnmp.cpp:44: `SNMP_SEC_LEVEL_AUTHPRIV' was not declared in this scope\nIn file included from ksim_snmp_la.all_cpp.cpp:5:\nsnmp.cpp:244: `SNMP_ERR_TOOBIG' was not declared in this scope\nsnmp.cpp:245: `SNMP_ERR_NOSUCHNAME' was not declared in this scope\nsnmp.cpp:246: `SNMP_ERR_BADVALUE' was not declared in this scope\nsnmp.cpp:247: `SNMP_ERR_READONLY' was not declared in this scope\nsnmp.cpp:248: `SNMP_ERR_GENERR' was not declared in this scope\nsnmp.cpp:249: `SNMP_ERR_NOACCESS' was not declared in this scope\nsnmp.cpp:250: `SNMP_ERR_WRONGTYPE' was not declared in this scope\nsnmp.cpp:251: `SNMP_ERR_WRONGLENGTH' was not declared in this scope\nsnmp.cpp:252: `SNMP_ERR_WRONGENCODING' was not declared in this scope\nsnmp.cpp:253: `SNMP_ERR_WRONGVALUE' was not declared in this scope\nsnmp.cpp:254: `SNMP_ERR_NOCREATION' was not declared in this scope\nsnmp.cpp:255: `SNMP_ERR_INCONSISTENTVALUE' was not declared in this scope\nsnmp.cpp:256: `SNMP_ERR_RESOURCEUNAVAILABLE' was not declared in this scope\nsnmp.cpp:257: `SNMP_ERR_COMMITFAILED' was not declared in this scope\nsnmp.cpp:258: `SNMP_ERR_UNDOFAILED' was not declared in this scope\nsnmp.cpp:259: `SNMP_ERR_AUTHORIZATIONERROR' was not declared in this scope\nsnmp.cpp:260: `SNMP_ERR_NOTWRITABLE' was not declared in this scope\nsnmp.cpp:261: `SNMP_ERR_INCONSISTENTNAME' was not declared in this scope\nIn file included from ksim_snmp_la.all_cpp.cpp:5:\nsnmp.cpp: In function `QString messageForErrorCode(int)':\nsnmp.cpp:294: `SNMP_ERR_NOERROR' undeclared (first use this function)\nsnmp.cpp:294: (Each undeclared identifier is reported only once for each\n   function it appears in.)\nsnmp.cpp:294: `MAX_SNMP_ERR' undeclared (first use this function)\nIn file included from ksim_snmp_la.all_cpp.cpp:7:\nvalue.cpp: In constructor `KSim::Snmp::ValueImpl::ValueImpl(variable_list*)':\nvalue.cpp:89: `SNMP_NOSUCHOBJECT' undeclared (first use this function)\nvalue.cpp:93: `SNMP_NOSUCHINSTANCE' undeclared (first use this function)\nvalue.cpp:97: `SNMP_ENDOFMIBVIEW' undeclared (first use this function)\nIn file included from ksim_snmp_la.all_cpp.cpp:15:\nsession.cpp: In member function `bool KSim::Snmp::Session::snmpGetInternal(int,\n   const KSim::Snmp::IdentifierList&, KSim::Snmp::ValueMap&,\n   KSim::Snmp::ErrorInfo*)':\nsession.cpp:73: `SNMP_MSG_GETNEXT' undeclared (first use this function)\nIn file included from ksim_snmp_la.all_cpp.cpp:15:\nsession.cpp: In member function `bool KSim::Snmp::Session::snmpGet(const\n   KSim::Snmp::IdentifierList&, KSim::Snmp::ValueMap&,\n   KSim::Snmp::ErrorInfo*)':\nsession.cpp:249: `SNMP_MSG_GET' undeclared (first use this function)\nmake[6]: *** [ksim_snmp_la.all_cpp.lo] Error 1\n"
    author: "Johannes Grosen"
  - subject: "Re: Problems compiling kdeutils (SNMP dependency)"
    date: 2003-09-29
    body: "Use http://bugs.kde.org"
    author: "Anonymous"
  - subject: "Re: Problems compiling kdeutils (SNMP dependency)"
    date: 2005-06-11
    body: "I had the same problem.  Install Net-SNMP from source (http://net-snmp.sourceforge.net/), then run ldconfig and try the kdeutils install again.  In my case, I also had to copy /usr/local/lib/libnetsnmp.so to /usr/lib/libnetsnmp.so."
    author: "Travis Williams"
  - subject: "Can't use a .jpg image as a background"
    date: 2003-09-30
    body: "Hi boyz! I've been compiling this beta of KDE all night, having no troubles at all during the compiling session (wow! :-) ) But, as soon as I finished, I could see that no jpeg image can be loaded as background (I say, using the Control Panel I cannot see jpeg images as a preview, nor use it to put onto background, even if .png images are correctly showed and can freely be used...) \nThe strange thing is that libjpeg are correctly installed, and the same images I can't even see from the control panel are showed with no trouble by Konqueror, and others built in viewers..... \n\nCan someone help me before I go mad? ;-) Thx to all of you!"
    author: "Marcello"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-10-02
    body: "Hi!\n\nCheck the options when compiling KDE and QT. \"./configure --help\"\n\n\n"
    author: "henrik"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "Hi,\n\nI have the same problem in the stable release of KDE 3.1.4. How can this problem be solved?\n\nThx,\n\nTom"
    author: "Tom"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "If you've got this problem running a \"stable\" version of KDE, you can solve it simple recompiling your qt adding to the ./configure options the flag --system-libjpeg (but be sure libjpeg have  already been compiled 'n installed on your system). Then recompile also kde, and all shall function....."
    author: "U1iu5ca35ar"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "Is it also possible to solve this problem without compling packages myself. Is it possible to make sure that qt is cleanly reinstalled as it is provided on the stable servers? I suppose that those versions are compiled with the right options to allow jpg images as background?"
    author: "Tom"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "Mmmmmh...... I don't know how to see what options qt has been compiled with.... It sounds like you're using a pre-compiled version..... Aren't you? I say, you are using the same qt your distribution installed at the setup time, aren't you? So, what is the distribution you r using? "
    author: "U1iu5ca35ar"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "Yes, is is a pre-compiled version and installed at set-up time. I use the Debian stable distribution and the latest stable version of KDE (3.1.4)."
    author: "Tom"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "It's sounds quite strange.... Debian's pre-compiled qt are configured for jpeg support.... Have you already tried to re-install libjpeg?"
    author: "U1iu5ca35ar"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "apt-get --reinstall install libjpeg\n\nsays\n\nCan't find package libjpeg\n\nIs it the wrong name for the package?"
    author: "Tom"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2003-12-10
    body: "I think you'd better try to download original libjpeg sources, and compile 'em.... the link is \n\n[url]http://www.ijg.org/files/jpegsrc.v6b.tar.gz[\\url]\n\nGotta only follow the instructions in the INSTALL file....."
    author: "U1iu5ca35ar"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2004-01-28
    body: "I have the same problem as Marcello and have compiled libjpeg6b, I do have a pre-built QT, but it's the same QT being used by my KDE3.1 installation and that works, plus I'm pretty sure the 3.2 beta DID work in this respect. I'm going round the bend with this problem - did you get anywhere Marcello?\n\nCheers,\nMatthew"
    author: "Matthew"
  - subject: "Re: Can't use a .jpg image as a background"
    date: 2004-01-28
    body: "No, Sorry matthew but I wasn't able to have that problem fixed...I even have no time to study the source and try to find the reason why... I'm now using the 3.1.5 release, and I've got no problem at all.....It's possible that there's noone out there that have had this trouble fixed? ;-)"
    author: "U1iu5ca35ar"
  - subject: "Java Dependency under Debian (Knoppix)"
    date: 2003-10-02
    body: "Any Suggestions?\n\nchecking for Java... configure: error: Incorrect version of /usr/include//jni.h.\n                    You need to have Java Development Kit (JDK) version 1.2.\n\n                    Use --with-java to specify another location.\n                    Use --without-java to configure without java support.\n                    Or download a newer JDK and try again.\n                    See e.g. http://java.sun.com/products/jdk/1.2\nmake[1]: *** [configure-work/kdebase-3.1.92/configure] Error 1\nmake[1]: Leaving directory `/home/jjohnston/src/konstruct/kde/kdebase'\nmake: *** [dep-../../kde/kdebase] Error 2\n\n\n$ sudo dpkg -l j2sdk1.4\nDesired=Unknown/Install/Remove/Purge/Hold\n| Status=Not/Installed/Config-files/Unpacked/Failed-config/Half-installed\n|/ Err?=(none)/Hold/Reinst-required/X=both-problems (Status,Err: uppercase=bad)\n||/ Name                Version             Description\n+++-===================-===================-======================================================\nii  j2sdk1.4            1.4.0.99beta-1      Blackdown Java(TM) 2 SDK, Standard Edition\n"
    author: "Jayme Johnston"
  - subject: "Re: Java Dependency under Debian (Knoppix)"
    date: 2003-10-02
    body: "Knoppix is a live CD Linux you can install rpm's or anything there ... use a normal distro... like Conectiva, Debian, Mandrake, Slackware... or Red Hat..."
    author: "Joaquim Andrade (Conectiva Test Team)"
  - subject: "Re: Java Dependency under Debian (Knoppix)"
    date: 2003-10-02
    body: "Knoppix is installed on the hard drive.  It *IS* debian.  It uses the same deb packages.  Besides the error is really with the Debian java package.\nIs this a known issue?\n\n--Jayme"
    author: "Jayme Johnston"
  - subject: "Re: Java Dependency under Debian (Knoppix)"
    date: 2003-10-19
    body: "Just had exactly the same problem with Knoppix 3.3 on my harddisk. The following seems to work:\n\n- I downloaded jdk 1.2 from http://java.sun.com/products/jdk/1.2 (as suggested by the error message)\n- I included the jdk directory in my PATH variable (as suggested in Sun's installation info)\n- I set a symlink from /usr/local/jdk1.2.2/include/jni.h to /usr/include/jni.h\n\nI wonder wether a newer Java sdk version (1.3 or 1.4) could be used. \n\nHauke"
    author: "Hauke Hell"
  - subject: "kssh not in kdenetwork -- why??"
    date: 2003-10-04
    body: "One very inportant tool for Linux/UNIX users are ssh. Telnet is at least for me, dead since it is not too secure.\nSSH is partly secure and easy to access the servers with, and for KDE there is this great tool kssh. Why is a tool so central to Linux/UNIX administration not part of KDE?\nI'm asking just to understand this, I'm sure there is a perfectly good explanation.\n\nOther than that, KDE-3.2-alpha is really super, but so is KDE-3.1 :-)"
    author: "Jarl E. Gjessing"
  - subject: "Re: kssh not in kdenetwork -- why??"
    date: 2003-10-04
    body: "It was never proposed by the developer so there was never a reason to consider inclusion?"
    author: "Anonymous"
  - subject: "Re: kssh not in kdenetwork -- why??"
    date: 2003-10-08
    body: "What's so important about kssh? I used it once, and there was nothing there that seemed to be particularly newsworthy. Fortunately, the ssh frontend I would recommend is still included: Konsole. Would be a bit odd if THAT was removed ;-)"
    author: "Andreas Steffen"
  - subject: "Re: kssh not in kdenetwork -- why??"
    date: 2003-10-09
    body: "Don't get me wrong. I'm able to type ssh ... :-)\nIt's just that kssh is a great tool for quickly accessing hosts via SSH \nAnd many people like gadgets like this, that makes life easier.\nActually it took me some time getting used to using kssh, but now I really feel it as being a great tool.\n\nFor me, it is easier to make a std. option in kssh and then select a host rather than doing ssh -X -C... hostname -lname\nFor others these tools are almost an annoyance."
    author: "Jarl E. Gjessing"
  - subject: "kde-i18n support?"
    date: 2003-10-08
    body: "Hi!\nI run Gentoo, and right now I'm using 3.2alpha1, compiled from portage. However, the kde-i18n package is still at version 3.1.4, and appearantly, unable to generate a german language setting, leaving me with US-English. Now I can work with that, but I'd rather have german. Would 3.2alpha2 do the trick?"
    author: "Andreas Steffen"
  - subject: "Re: kde-i18n support?"
    date: 2003-10-09
    body: "Just update your kde-i18n ebuild. Or ask at http://forums.gentoo.org/ for help."
    author: "Anonymous"
  - subject: "kde-i18n-de not working, who to contact?"
    date: 2003-10-13
    body: "Building kde-i18n-de (3.2.0_alph2) crashes with this message:\n\nmake[5]: Entering directory `/var/tmp/portage/kde-i18n-3.2.0_alpha2/work/kde-i18n-de-3.1.92/docs/kdemultimedia/artsbuilder'\nmake[5]: F\u00fcr das Ziel \u00bball-am\u00ab ist nichts zu tun.\nmake[5]: Leaving directory `/var/tmp/portage/kde-i18n-3.2.0_alpha2/work/kde-i18n-de-3.1.92/docs/kdemultimedia/artsbuilder'\nindex.docbook:6: validity error: Element appendix content does not follow the DTD\nExpecting (appendixinfo? , (title , subtitle? , titleabbrev?) , (toc | lot | index | glossary | bibliography)* , tocchap? , (((calloutlist | glosslist | itemizedlist | orderedlist | segmentedlist | simplelist | variablelist | caution | important | note | tip | warning | literallayout | programlisting | programlistingco | screen | screenco | screenshot | synopsis | cmdsynopsis | funcsynopsis | classsynopsis | fieldsynopsis | constructorsynopsis | destructorsynopsis | methodsynopsis | formalpara | para | simpara | address | blockquote | graphic | graphicco | mediaobject | mediaobjectco | informalequation | informalexample | informalfigure | informaltable | equation | example | figure | table | msgset | procedure | sidebar | qandaset | anchor | bridgehead | remark | highlights | abstract | authorblurb | epigraph | indexterm | beginpage)+ , (sect1* | refentry* | simplesect* | section*)) | sect1+ | refentry+ | simplesect+ | section+) , (toc | lot | index | glossary | bibliography)*), got (title )\n</appendix>\n           ^\n\nWho do I have to contact?"
    author: "Master of Disaster"
  - subject: "Re: kde-i18n-de not working, who to contact?"
    date: 2003-10-13
    body: "> Who do I have to contact?\n\nGentoo KDE maintainer(s), http://bugs.gentoo.org/"
    author: "Anonymous"
  - subject: "Re: kde-i18n-de not working, who to contact?"
    date: 2003-10-13
    body: "But this problem has apparantly nothing to do with Gentoo, it seems that one of he translators screwed up one .xml file."
    author: "Master of Disaster"
  - subject: "Re: kde-i18n-de not working, who to contact?"
    date: 2003-10-13
    body: "Nothing? So why had I no problems with kde-i18n-de-3.1.92.tar.bz2 on SuSE 8.2?"
    author: "Anonymous"
  - subject: "Re: kde-i18n-de not working, who to contact?"
    date: 2003-10-13
    body: "You got point"
    author: "Master of Disaster"
  - subject: "Re: kde-i18n-de not working, who to contact?"
    date: 2003-10-17
    body: "Could you tell me the version of libxml & libxslt that you are using please?"
    author: "Master of Disaster"
  - subject: "konq gripes"
    date: 2003-10-31
    body: "I am using orth debs for debian unstable and there are a few things I don't like about konqueror's behaviour now.\n\nThe biggest is when using konqueror to browse files and Detailed List View.  In 3.1.x it is possible to right click in the file-list pane to paste file(s) by right clicking on any column other than \"Name\".  Doing so brings up a context menu that includes \"paste\", whereas right clicking in the name column brings up a context menu for the file you have clicked and which does not include paste.\n\nIn 3.2.0 however, right clicking in ANY column brings up a context menu for the file whose row you right clicked on.  Thus, in a directory with enough files that there is no blank space at the bottom, in Detailed List View it is impossible to right-click paste (no matter where you click you get a context menu for a file or a directory with no option to paste)  If you don't know what I mean, switch to detailed list view and try to use the context menu to paste a file into a directory with a lot of files :)  That for me is a killer.  Who cares about being able to open a file by clicking on its modified date?  The other nasty effect of this is that it removes the easy way to select a single file.  In 3.1.x, left click on any column but the name and that one file is selected.  Not so in 3.2.0.  Bad as that is, not being able to right click, paste at all is worse I think.\n\nI also don't like the removal of the \"Open Terminal Here\" item in the context menu.  Of course it is in the Tools menu, but it is nice to right-click on a directory other than the one I am currently in to open a terminal there, and it is something I do frequently.  Saves a click over having to open it first :)  Does anyone else do this as well?  \n\nLast gripe is how there is no more choice between \"Open in Background Tab\" and \"Open in New Tab\" in the right-click menu.  You can choose between them in the config menu of course, but I use both.  On a page with thumbnails I'll open several in background tabs to look at later.  Other times I want to read a link without moving away from the page it is on (If pressing the back button instantaneously brought up the previous page like in Opera, I might not need that ;) )  Sometimes a nice combo of both.. Open several thumbnails in background tabs, then the last one in a new tab to get started with viewing right away.  I really do use this.  But does anyone else?\n\nI know some options need to go from that menu, it is too large.  A more dynamic menu that had a clue about where you clicked would be nice.  How about getting rid of \"Open With\" and \"Preview In\" options when right clicking a webpage?  It is already open ffs!  Or maybe konq could see fit to leave out \"create data CD with k3b\" while browsing the web.. there are several things that don't need to be there for browsing.  \"Open in Background Tab\" and \"Open in New Tab\" however  do (imho of course, would love to hear others' opinion)"
    author: "MamiyaOtaru"
  - subject: "Re: konq gripes"
    date: 2004-02-05
    body: "well i have just start exploreing kde 3.x. i also want \"open terminal here\" back because now i have to open terminal from menue and then change my director ....which is bit boring can any body tell me how to edit desktop menu \n"
    author: "farooq hasny"
  - subject: "Re: konq gripes"
    date: 2004-02-05
    body: "You can still press ctrl-t to get your terminal."
    author: "Boudewijn Rempt"
  - subject: "KDE3.2 and Java"
    date: 2004-01-28
    body: "3.2 Alpha rocks, but I also have the problem the chap above had with reading JPEG files in certain apps - and the QT is the same as for my 3.1 installation... anyhow...\n\nAnyone else found the following problem? I have a Java app from which several child windows are spawned. If I go to another virtual desktop, then come back again, all the children get minimised! Can't seem to find a reason for this behaviour, and it certainly doesn't happen in 3.1. Because I use this app a lot for my work and the minimizing is a real pain, it's holding me back from converting over... shame because I love Konqueror 3.2!\n\nCheers!"
    author: "Matthew"
---
As the <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">first beta has been delayed</a> to finish more <a href="http://pim.kde.org/index.php">PIM features</a>, we're proud to present the second alpha release of KDE 3.2. The first alpha was already <a href="http://dot.kde.org/1064353967/">seen as a very strong release</a> and the second one is even better with <a href="http://bugs.kde.org/weekly-bug-summary.cgi?tops=15&days=31">1374 bugs closed in the last 31 days</a>. The major changes are the import of <a href="http://svg.kde.org/">KSVG</a> and KPDF into the KDE distribution, along with a major rewrite of the window manager. You can <a href="http://download.kde.org/download.php?url=unstable/3.1.92/src/">download the new release here</a>. The are currently no binary packages, but you can of course use <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> to build it. Please give this one a good testing as we'll be moving to the beta phase next.
<!--break-->
