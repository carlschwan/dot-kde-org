---
title: "KOrganizer: Website Update and New Features"
date:    2003-07-07
authors:
  - "kstaerk"
slug:    korganizer-website-update-and-new-features
comments:
  - subject: "Hot new stuff"
    date: 2003-07-07
    body: "The hot new stuff idea is really nice :-)"
    author: "Maarten"
  - subject: "Re: Hot new stuff"
    date: 2003-07-08
    body: "If only the name was different! The word stuff should only be used in very informal writing, which a computer programme is not. Would it not make sense for it to be changed to something more dignified?"
    author: "Sinuhe"
  - subject: "Re: Hot new stuff"
    date: 2003-07-08
    body: "IMHO the name is not appropriate. What is the hot new stuff? Drugs? Warez? MP3?\n\nI certainly do not expect a calendar, even in KOrganizer..."
    author: "Anonymous George"
  - subject: "Re: Hot new stuff"
    date: 2003-07-08
    body: "I also would like a new name for this functionality. We had a lot of problem in the French translation team to find an adequate translation for 'hot new stuff'. Colloquial English should be, by all means, avoided in KDE messages because it is unclear for the end user and difficult to translate.\n\nSomething like 'find new calendars / screensavers / etc...' would be better\n\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: Hot new stuff"
    date: 2003-07-08
    body: "You are right ... sometimes developers tend to forget that some \"stuff\" needs to be translated as well ... \n\ngrtz'\n\nFab"
    author: "Fab"
  - subject: "A few nits "
    date: 2003-07-07
    body: "Don't get me wrong, I love the hot stuff idea.  Just a few mostly\nminor nits:\n\n+ How come if I click on the hot stuff .ics calendars at http://korganizer.kde.org/calendars/hotstuff.html\nwith konquerer I'm asked if I want to open the calendar\nwith kwrite instead of korganizer?\nHowever, clicking on a .ics on icalshare asks to\nopen with korganizer as expected?\n\n+ I'm not sure the Help menu is the best place\nfor the Get and Load Hot New stuff functions.\nMaybe move to Tools menu and rename to\nGet/Load Public Calendars?\n\n+ The Get Hot Stuff dialog has an OK and Apply button. Why?\n\n+ Once I select a Hot Stuff calendar I am presented with a list\n  of all the events.  That's a nice feature.  But, why am I permitted\n  to click/highlight each event?  I expect to be able to do something\n  once I highlight the event but I can't see what that would be.\n\n+ I don't always want to merge the new calendar into my current calendar.\n  Can a new calendar be created for me?\n\nI'm using KDE 3.1.2, btw.  Thanks for the great work!"
    author: "allen"
  - subject: "Re: A few nits "
    date: 2003-07-07
    body: "Probably MIME type settings on the web server"
    author: "MIME-Typer"
  - subject: "Re: A few nits "
    date: 2003-07-08
    body: "> I'm not sure the Help menu is the best place\n> for the Get and Load Hot New stuff functions.\n\nI agree completely with this. So long time using korganizer, and I didn't see the function until right today when I read this news. \n\nIt's plain stupid to put something that is not \"Help\" related under \"Help\"\n"
    author: "uga"
  - subject: "Re: A few nits "
    date: 2003-07-08
    body: "I also partly agree with the comment. I'm a long time KDE user and haven't found this nice funtionality until now, because I never looked in the help menu (its very rare that I need the help system).\n\nBut I don't think that you can win the hearts of the developers by calling something plain stupid. This puts the developer in a defensiv position and will always make it less likely that he listens to your opinion.\n\nChristian\n\n"
    author: "cloose"
  - subject: "Re: A few nits "
    date: 2003-07-10
    body: "> But I don't think you can win the hearts of the developers by calling something\n> plain stupid.\n\nYou're right, and I'm very sorry about that. I didn't mean to offend anyone. I just didn't find the correct words to express myself. It's just that whenever I do similar things (which I _have_ done), I call them plain stupid the same way ;)\nSorry again... I think I need to improve my social and english language skills :-P\n\nActually I've followed some parts of the work of the kdepim developers, and I can really say that the way they usually work is quite smart.\n\nIf I really did offend any of you developers, please accept my apologizes.\n\n\tUnai"
    author: "uga"
  - subject: "pinning of any mimetype to an appointment / todo m"
    date: 2003-07-07
    body: "taken from the korganizer paragraph of the kde3.2 feature plan\n(http://developer.kde.org/development-versions/kde-3.2-features.html):\n\no pinning of any mime type to an appointment or todo mark\n\nThis by far one of the most desired features by me. I'm glad it's in the feature plan. Does anybody know, if work on this is going on already?"
    author: "Thomas"
  - subject: "Re: pinning of any mimetype to an appointment / todo m"
    date: 2003-07-07
    body: "This is on the featurelist at least since KDE 3.0. I don't think it will be implemented any soon - sorry.\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: pinning of any mimetype to an appointment / todo m"
    date: 2003-07-08
    body: "I don't quite get this, unless it's unrelated to the server mimetypes being discussed earlier.  What would it do?"
    author: "hmmm... ?"
  - subject: "Re: pinning of any mimetype to an appointment / todo m"
    date: 2003-07-08
    body: "Basically it means, you'd be able to attach a file (or a link/url to a file) to an appointment. Imagine a situation like this: You're working on several projects and you're using korganizer to keep on schedule with different tasks.\n\nIt would be nice, if korganizer reminds you of a certain deadline in advance _and_ presents you e.g. the not yet finished kpresenter-Document as an icon.\n\nThis way it is possible to start work on the file again with just one click and without the pain searching it on your local HD..."
    author: "Thomas"
  - subject: "Virtual Folders"
    date: 2003-07-08
    body: "This would basically extend the concept of virtual folders to all of KDE. Outlook also provides this function; in fact, you can use Outlook as a file manager (and many people do) and present different views of the user's files than a flat filesystem representation. If you have an event where you are doing a presentation, when adding an event to KOrganizer, you could \"attach\" a directory containing your KOffice files, and possibly annotate them to know if each is complete or still needs work. There are many possibilities here. Are vFolders in the works?\n\nRegards,\n\nEron"
    author: "Eron Lloyd"
  - subject: "stacking"
    date: 2003-07-07
    body: "can we stack calendars, eg a personal calendar, a workgroup, a uk public holidays, an evening class timetable, a travel itinerary from a travel agent, and an exercise schedule provided by another program ( maybe via DCOP)? \n\nImporting is pretty bad compared to dynamic composition of calendars.  "
    author: "rjw"
  - subject: "Re: stacking"
    date: 2003-07-07
    body: "This already is possible with current CVS :-) Not perfect yet, but there is some time left until KDE 3.2 final...\n\nciao"
    author: "Guenter Schwann"
  - subject: "Updating Calendars?"
    date: 2003-07-07
    body: "Is it possible to update calendar entries obtained via \"Get Hot New Stuff\", e.g. if an updated KDE 3.2 release plan is imported will old obsolete entries be deleted? Possible implementation: cache all imported calendars and diff against the newer version."
    author: "Anonymous"
  - subject: "Nice idea!"
    date: 2003-07-07
    body: "But why are the buttons not created with the same width for each button in the following image?\nhttp://korganizer.kde.org/images/screenshots/hotstuff_get02.png\n\n\n(sigh, and KDE people keep saying that GNOME is inconsistent.. (not meant as a flame))\n"
    author: "pleb"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "Why should they? The texts for each button are of different lenghts so as a result the buttons having different widths is just consequential, isn't it?\n\nEven if you would like them to be of the same width, what width should that be? And which buttons should comply to that additional rule instead to the current one, and why?"
    author: "Datschge"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "Actually, stuff like this should be covered in the KDE HIG. Sizes and spacing should be specified down to the pixel.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "It is: be exactly as wide as necessary for the content to fit in. Nothing more and nothing less.\n\nPlease give an explanation for why you think this doesn't do the job and what rules you want to set up instead to ensure that the result is \"great\" to \"alright\" in all possible cases."
    author: "Datschge"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "I should have been more clear. Sizes and spacing layout wise is what I meant. And yes, there are some UI properties that should have a recommended value in the HIG. For example, a QButton has properties for height and margin. What should the recommended values be? Qt Designer can make objects a bit \"boxy\" by default. In this case pixel-specific recommendations (note recommendation, not rule) could be provided. As far as layout, GNOME and Apple both specify pixel-level spacing recommendations:\n\ndeveloper.apple.com/documentation/UserExperience/Conceptual/AquaHIGuidelines/AHIGLayout/\nhttp://developer.gnome.org/projects/gup/hig/1.0/windows.html#alert-spacing\n\nOf course there has to be accomodizations for language localization and accessibility adjustments...\n\nCheers,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "While I agree that layouts should be standardized I don't think hardcoding some pixel values like it seems to be suggested by both Apple's and Gnome's HIG is really future-proof at all. Layouts should be ideally completely independent from the used screen resolution, instead they should only define some classes for different UI elements/purposes and then let an (exchangeable) style visualize them in KDE wide consistent pixel values. While the current model used in KDE is not that flexible (yet?) many UI elements are already standardized KDE wide on the library level."
    author: "Datschge"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "We kind of do that already with layouts, see KDialog::spacingHint and KDialog::marginHint; \nIan Geiser recently converted many of .ui files to use those properly; so now it's\npossible to change layout spacing in one spot.\n"
    author: "SadEagle"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "Thanks for reminding me of that. I guess we need an own HIG just for the sake of documenting these kinds of features. ;)"
    author: "Datschge"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "Is anything truly future-proof? ;-) While perhaps in an ideal world layout would be independant from resolution, but what would you use to specify sizes and spacing--percentages? Of course kdelibs takes care of some of this, but there are many things the developer has to be aware of, too."
    author: "Eron Lloyd"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "It strikes me as very odd that in times where pixel artwork for icons is becoming deprecated and to be replaced with vector based artwork which can display fine in all resolutions, that the same systems are even supporting manually hardcoding anything pixel based as part of a HIG. To me this only seem to introduce twice useless work, first manually adding all those pixels everywhere and the removing them all again as soon as it will be obvious that hardcoded pixel values can only break a layout's flexibility to adapt to different resolutions.\n\nAs for the \"many things the developer has to be aware of, too\" be sure to make a list of all you can find, so not every single person need to get that idea themselves after some time (yes, I suggest to request additional features in kdelibs in case there's indeed something seriously missing so far)."
    author: "Datschge"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: ">Sizes and spacing should be specified down to the pixel.\n\nYou can't specify something like button sizes. The required text sizes in the buttons differ a lot from one language to another one and you also need to take into account the font sizes.\n\nTake for example: \"Help\" in english takes 24px on my screen, so we could set Help buttons to be 30-35 pixels wide. Right? Now go for the Basque translation: \"Laguntza\". It's 50px wide. It doesn't fit in. That's why it's better to adjust the sizes of the buttons according to the text they contain.\n \n"
    author: "uga"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "The bigger it is, the more important it is or seems in the UI.  It doesn't make sense for something to be more important just because it takes more letters to spell it."
    author: "anon"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "Erm, excuse me but when the use of something makes only sense through the letters used in/on it then you just can't rip away them for the sake of keeping all somethings' sizes relatively reflecting their importance. And besides, who decides what's important and what's not?\n\nI guess we better force everyone to learn Chinese where pretty much all text is neutral size wise so we can tweak all the \"importance sizing\" manually and present everyone the \"one and only perfect UI\". Other than that I don't see any sane way how this kind of \"wishes\" can be fulfiled without causing a lot of additional work for everyone with nearly zero benefit at the end."
    author: "Datschge"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "They should be because it just looks better, more consistent and more polished.\n(KDE isn't just a toy desktop anymore, right?)\n\nie. GNOME uses button boxes to make sure other buttons are the same width as the button with the largest width.\n\nNow, please compare the following 2 images:\n\nhttp://korganizer.kde.org/images/screenshots/hotstuff_get02.png\nhttp://gthumb.sourceforge.net/gthumb_1.106_shot3.jpg\n\nLook at the Prev, Next & Close buttons in the latter screenshot (the property dialog).. doesn't that look more consistent to you?\n\n(I'm not talking about themes and toolkits here so please leave that discussion out of here, just in case someone starts blabbing about that)\n\nOh, and Windows doesn't do it the KDE-way either.. (buttons are as wide as the largest one)\n\nNot sure about how OS X solves the issue.\n"
    author: "pleb"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "Windows does that for technical reasons: they use fixed positioning for everything. You can not do i18n without having extra space..."
    author: "Anonymous George"
  - subject: "Re: Nice idea!"
    date: 2003-07-08
    body: "How would you do this with different languages? What if \"Cancel\" is 3 or 4 times the size of ok and apply in a different language?\nWouldn't this waste space? Wouldn't it look even more ugly then?\nI fully agree here, that the buttons are only as wide as the content.\n\nPhilipp"
    author: "Philipp"
  - subject: "Finishing touch & polishing"
    date: 2003-07-08
    body: "KOrganizer has many promising features, but I think it lacks polishment.\n\nFor examples colors, the first thing that you see when you start the app. Compare the following:\nhttp://korganizer.kde.org/images/screenshots/mainshot_big.png\nhttp://www.apple.com/ical/images/index_top12312002.jpg\n\nOr look at the \"Get hot new stuff\" feature (i try to avoid complaining about the name a second time, but it is hard not to :). The dialog looks pretty lost, like a prototype of a dialog that has not been finished. Or the strange Edit Calendar Filters dialog with that raised frame that I don't understand. Or the 'Publish Free Busy Information' function, which shows a address selector that seems to be completely unrelated to the menu name. No instructions or anything that could tell the user what to do. I could probably go on like that for at least 50% of KOrganizers UI. The most important elements, like creating a new event, is good though. And I also like the date selection.\n"
    author: "Anonymous George"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "If you have a nice set of coulors, than let the developers know (kdepim-mailinglist).\nFor the rest of your suggestions it's the same. Maybe you just create a list of stuff that is wired (in your oppinion) in korganizer's UI, send it to the mailing list, and the developers can try to fix it. Maybe you also add sugestions for solutions of the \"broken\" stuff (or even some code?).\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "Unfortunately with UI design it is like with literature. I can recognice that I book is bad, but that does not mean that I can write a better book. "
    author: "Anonymous George"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "So you can at least sum up some UI problems and report then. This would help the developers a lot already.\nciao"
    author: "Guenter Schwann"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "There is someone on KDE-LOOK who has proposed better looks for Korganizer. I'll paste the link below. I've searched on kdepim archives but haven't been able to find any message refferring to this.\n\nAnd let's face it. It really does look horrible atm.\n\nhttp://www.kdelook.org/content/show.php?content=6361"
    author: "Mathieu Kooiman"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "I agree with you! The iCal (Apple) shown in your second link is much less cluttered, more accessible and more beautiful than KOrganizer. \n\nWhy not learn from Apple instead of reinventing the wheel? It's not about copying exactly the look and feel of Apple applications, but learn from them, what they have done right and improve KOrganizer by adjusting its current interface."
    author: "tuxo"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "> Why not learn from Apple instead of reinventing the wheel?\n\nBecause KOrganizer was already there for many years when iCal was introduced perhaps? Because people didn't care until they found something else they can brown-nose with generic stuff like \"much less cluttered, more accessible and more beautiful\"?\n\nPlease contribute your improved color scheme, better wording and suggestions for layout tweaks instead playing to someone else's writing without adding something yourself. Thank you."
    author: "Datschge"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "One thing that I would like to see is the modification of the calendar views to more closely follow Outlook/Evolution, especially the full-week view. I'm working on a usability report for KOrganizer now, so these issues will be addressed. I think there are many things that KOrganizer can learn from it's predecessors (Outlook, Notes, etc.). Lots of people will just be needed to contribute to make it happen.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "> Because KOrganizer was already there for many years when iCal was introduced perhaps?\nOk, but this doesn't mean that there are things that can be learned from a newcomer.\n\n> Because people didn't care until they found something else they can brown-nose with generic stuff like \"much less cluttered, more accessible and more beautiful\"?\nI never really liked the interface of KOrganizer, didn't really know why. My post was not an attempt at brown-nosing as you prefer to refer to it. When I saw iCals screenshot I was simply amazed, thus my words were chosen a bit too generic, I agree. Will list some points that disturb me with the current implementation of KOrganizer further below.\n\n> Please contribute your improved color scheme, better wording and suggestions for layout tweaks instead playing to someone else's writing without adding something yourself. Thank you.\nOk, here are a few suggestions:\n\nLayout:\n- Interface clutter: Organization of the icons on the toolbar. Apples toolbar icons are clusterd together according to their function and placed where they are most relevant. This however is probably more a problem with the generic KDE interface guidelines.\n\n- iCal calender on the side shows not only the current month but also the next one. This is very handy as one often has events comming up in a time-frame of a month. It's nice to be able to look ahead. This is the reason also why analog clock interfaces are still popular, you can actually see how much time is in front of you.\n\n- It is a great idea to be able to switch quickly between different calenders: iCal allows this directly via the side panel.\n\n- Search function to search for meetings according to keywords. The search field is provided conveniently on the bottom of the application, see the screeshot of iCal.\n\nDesign:\n- Current event entries appear with frames that are too blocky, compare e.g. the rounded edges iCal provides.\n\n- I very much like the colors attibuted to the different event entries in the iCal screenshot. The default color(s) of KOrganizer are pretty boring in comparison.\n\n- Event entries have a title bar section that indicates the time. This is useful with many entries in order to immediately grasp when exactly a given event starts.\n\n- The color of the default toolbar icons (Go to today, New event etc), don't fit with Crystall icons. Especially the shade of blue is wrong when compared to the blue color used in other Crystall icons (e.g. the home icon).\n\n- The system tray icon of KOrganizer is outdated, it doesn't fit in well with the default crystall icons (e.g. too sharp edges)."
    author: "tuxo"
  - subject: "Re: Finishing touch & polishing"
    date: 2003-07-08
    body: "Please send those ideas about 50% of KOrganizer's UI to me (elloyd@lancaster.lib.pa.us), and I will try to include them in my report. You don't have to be a coder to contribute.\n\nEron"
    author: "Eron Lloyd"
  - subject: "More Specific summaries for hot new stuff events"
    date: 2003-07-08
    body: "I merged the KDE release 3.2 release schedule in my calendar, and I have a little suggestion. Instead of \"Alpha 1 release\", I would rather write \"KDE 3.2 Alpha 1 release\", and so on. These public calendars should have descriptive summaries for the events IMHO.\n\nAnd I agree with the suggestions about the menu location (I discovered this feature in this article, not in the menues) and the feature title. OF course these are humble suggestions, not complains.\n\nThanks for the great work, please keep going!"
    author: "MandrakeUser"
  - subject: "OT: How does one make a feature change/request?"
    date: 2003-07-09
    body: "Please excuse me for my OT question.\n\nI have recently compiled KDE 3.1.2, and was excited to see the capabilities of KDE.  However, there was a wish for Konqueror (the file manager) and the way XIM input was handled.\n\nFrom what I have been reading on the Dot, it seems as if most people enjoy the single-click to navigate.  However, I tend to prefer to double-click on files/directories based on my habit from Windows and Macs.\n\nIn the double-click mode, a single-click will highlight the name of the file/directory in Konqueror (FM).  When the text is hi-lighted, the hi-light color starts almost exactly on the pixel of the first character (left side for English/Japanese), but the right side has a space worth almost an entire character.\n\nI know this sounds confusing, and if I could be told how I could ask for this to be changed (although it may not be acceptable) like the way Nautilus hi-lghts file/directory names (i.e., equal spacing on both the left and the right margin(?), I would include a screenshot of what I'm trying to explain.\n\n\nThank you in advance.\n"
    author: "sjk"
  - subject: "Re: OT: How does one make a feature change/request?"
    date: 2003-07-09
    body: "KControl->Peripherals->Mouse->\"Icons->Double-click to open files and folders\"\nI guess that's exactly what you are searching for :-)\nFeaturerequests and bugs can be reported via http://bugs.kde.org\nciao"
    author: "Guenter Schwann"
  - subject: "Re: OT: How does one make a feature change/request?"
    date: 2003-07-09
    body: "Thanks for your reply.\n\nI guess my explanation was just not clear enough.\nI'll try again.\n\nI have my setup as exactly how you have instructed me; however,\nupon doing so and single-clicking on an icon in the Konqueror the\nfile manager, the filename/directory name will be hi-lighted.\n\nThis hi-lighted color surrounds the name.\nOn the left side of the hi-light, the hi-lighting color start at the \nfirst character of the name.\n\nOn the right side of the hi-light, the hi-lighting color end about a \ncharacter space away from the last character of the name.\n\ni.e. something like this...\nfilename: blah.sh\n\n    ----------\n    |blah.sh |\n    ----------\n\nand I was wondering if it's possible to change this to:\n\n    -----------\n    | blah.sh |\n    -----------\n     ^\n     | This space is what I wished for.\n\nI guess, with the dotted-line border of the hi-lighted color area, \nit makes it difficult to read the first character of the name of \nfile/directory.  Or is it because of the font I'm using?\n\nBest Regards"
    author: "sjk"
  - subject: "Re: OT: How does one make a feature change/request?"
    date: 2003-07-09
    body: "The right way to make a feature request is to go to bugs.kde.org and create one. But I am not sure whether your request is possible without modifying Qt."
    author: "Anonymous"
  - subject: "Re: Thank you"
    date: 2003-07-09
    body: "I'll go try to figure out how to do that on bugs.kde.org now.\n\nThanks for your replies!\n\nRegards\n"
    author: "sjk"
  - subject: "OpenGroupware.org"
    date: 2003-07-10
    body: "Check out this site http://www.OpenGroupware.org\nSeems like it could be made to work (or already works?) with korganizer too!"
    author: "tuxo"
---
The <a href="http://korganizer.kde.org/">KOrganizer website</a> has a new section covering information about sharing and exchanging iCal calendars. First, we have an overview of <a href="http://korganizer.kde.org/calendars/sites.html">calendar sites</a>, websites offering calendars in iCal format. These sites have a huge offering of downloadable iCal calendars covering arts, culture, economics, finance, government, science, sports and many more. The second page offers the so called <a href="http://korganizer.kde.org/calendars/hotstuff.html">hot new stuff calendars</a> that are available via the new <a href="http://korganizer.kde.org/features/newstuff.html">'Get Hot New Stuff' feature</a> in KOrganizer. Users of KOrganizer can import these calendars from the <a href="http://korganizer.kde.org/features/newstuff.html">'Help' menu</a> in KOrganizer but we also offer these <a href="http://korganizer.kde.org/calendars/hotstuff.html">calendars for download</a> in iCal format. If you feel that something is missing in this new section, feel free to contact <a href="mailto:schumacher@kde.org">Cornelius Schumacher</a> or <a href="mailto:staerk@kde.org">Klaus St&auml;rk</a>. You're also very welcome to provide us with more iCal calendars to put up on the KOrganizer webserver. 
<!--break-->
