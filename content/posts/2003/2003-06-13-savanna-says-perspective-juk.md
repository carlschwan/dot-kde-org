---
title: "Savanna Says: Perspective on JuK"
date:    2003-06-13
authors:
  - "numanee"
slug:    savanna-says-perspective-juk
comments:
  - subject: "Nice review"
    date: 2003-06-13
    body: "I've seen a lot more people switch to music players that aren't winamp clones (xmms), recently. The move from winamp/xmms/audion, to itunes/juk/rythybox is great-- the former is way harder to use than the later. I think most people still use xmms because they are simply used to it and winamp."
    author: "lit"
  - subject: "Re: Nice review"
    date: 2003-11-04
    body: "Well, they are used to it, that's right, but I think, it was mostly performance reasons. I used a half year ago an AMD Athlon k7 500 (slot-a :) ) with epox board and had win xp installed. This OS was not very fast on my system. what should I do? Yes, install winamp, it executes fast and plays almost instantly. Then the board wished no more to work. Now I have an asus with athlon xp 2200 (is about 1900MHz) and now I use itunes and love it, because I have got enough performance to use hugher programs. so I do not wonder of the new trend. :)"
    author: "litelightlit"
  - subject: "Hayes Playlist"
    date: 2003-06-13
    body: "I just want to say that I am using noatun with Hayes playlist for about one year now. I want to thank the authors for their work :)\n\nAnd now I am trying Juk but it doesn't have the same advantage / disadvantage of Hayes. I will stay with noatun and Hayes for now but thank you to the Juk author too/"
    author: "Shift"
  - subject: "Re: Hayes Playlist"
    date: 2003-06-13
    body: "There is also something to be said for a playlist player like JuK to be packaged, easily available and ready to go in KDE instead of requiring advanced plugin knowledge of Noatun from the user...  JuK really has a chance to shine here."
    author: "ac"
  - subject: "Re: Hayes Playlist"
    date: 2003-06-13
    body: "BTW is it planned to have a meta-data view (artists, albums etc). That would be more interesting than playlists for many purposes, IMHO. Having a playlist for each album should not be neccessary.\n"
    author: "AC"
  - subject: "Re: Hayes Playlist"
    date: 2003-06-13
    body: "IMHO, such a meta-data view should exist at the konqueror level... a search-folder like in the next kmail, but for the filesystem, sothat the result can be used in all kde applications (open file dialog for exemple)."
    author: "capit. igloo"
  - subject: "dynamic playlists"
    date: 2003-06-13
    body: "Yes, it was there as of about a week ago.  I decided to refactor that code and so at the moment it doesn't work again, but I'm hoping to get that done by this weekend."
    author: "Scott Wheeler"
  - subject: "juk versus hayes"
    date: 2003-06-13
    body: "i tried both noatun+hayes and juk, and i still prefer hayes (altough juk is nice) (http://www.freekde.org/neil/hayes/hayes.png), but that is probably a matter of taste. I really like the filesystem-based concept of hayes..\n\njuk also has some problems that need to be sorted out: 'add directory' takes a loong time here (it takes more than a minute, xmms loads the same directory in a couple of seconds). xmms only loads id3 tags when they are shown, maybe that has something to do with it (but i don't think its the only reason). but kde3.2 is not released yet offcourse\n"
    author: "ik"
  - subject: "Re: juk versus hayes"
    date: 2003-06-13
    body: "Actually really the slowness for adding directories is because JuK:\n\n- does a recursive scan of that directory\n- adds playlists and audio files\n- reads all of the tags at once and collects more information than i.e. XMMS\n- optimizes for fast loading by doing all of the dirty work up front\n\nBasically the idea behind JuK is to make the thing  that you do infrequently (adding files) slow and make the things that you do more often (i.e. start the program) fast.  As such, it caches all of the meta information and only updates it if the file is modified.  I can load up my playlist of about 2400 files in about 3 seconds -- and aparently there are some people (hi Ian) using JuK with up to 50,000 items.  Also I've tried to ensure that the GUI stays responsive while loading files so that JuK can still be used while loading files.\n\nThere will be some optimizations before 3.2 (I  won't go into the details unless requested), but I would expect things to get 2-3x faster, not say 10-20x.\n\nAs compared to Hayes the main difference is that JuK is playlist / collection oriented and assumes that playlists and meta-data to be the primary way of organizing music; Hayes on the other hand assumes  that the file system is being used for this.  They're both valid takes on the problem and cater to different folks.\n\n(D'oh, I guess I blew the cover; yes, JuK is for geeks too, though a significant motivation is keeping the interface simple and hiding the details that I'm talking about.)"
    author: "Scott Wheeler"
  - subject: "Re: juk versus hayes"
    date: 2003-06-13
    body: "I wonder, do you have anyone using JuK over NFS?\n\nHayes delays as much as possible, and does as little as possible, and I still get NFS complaints."
    author: "Neil Stevens"
  - subject: "Re: juk versus hayes"
    date: 2003-06-13
    body: "It's not significantly worse over NFS on a 10 MB/s LAN than on the local file system (Daniel and I did a good bit of testing with this.). The initial scanning is slow -- but after that on load JuK just stats files on start up (and even that is delayed until after the GUI is up and usable in CVS).  With stating files over NFS, your bottleneck is still a hard drive's seek time somewhere.\n\nSo while if you're loading 10,000 items over NFS it won't be exciting, you only have to do it once.  After that you can read all of the items back in a few seconds.\n\nIan, who to my knowledge has the record for biggest collection in JuK (50k files) is on NFS."
    author: "Scott Wheeler"
  - subject: "Re: juk versus hayes"
    date: 2003-06-13
    body: "I do. I have my home directories and MP3s on NFS. Nothing special, a 700MHz P3 with a big disk and a half-gig of RAM. I'm using 2.4.20 with the kernel nfs server on a 100m switched network.\n\nIt's perfectly usable. I've been using this setup (with various hardware/disk changes) for many years now without complaint."
    author: "Blue"
  - subject: "iTunes interface is patented"
    date: 2003-06-13
    body: "Worth noting that the iTunes interface is patented.\n\nhttp://jriddell.org/patents.html  iTunes patent details linked from that page.\n\nLike all software patents the problem is probably best ignored until/unless they come after you."
    author: "Jonathan Riddell"
  - subject: "Re: iTunes interface is patented"
    date: 2003-06-13
    body: "See now thats the funny thing, as someone who has actually _used_ iTunes and Juk, they are not the same thing.  In a static screen shot they may look the same, but they act VERY VERY different. Try to run them side by side and you will see :)  I mean just because KDE has windows dosent mean its a copy of GEOWorks :)  \n\nLets stop compairing Juk to iTunes...  Its probibly the same as compairing Juk to Windows Media Player.\n\nJust my 2c\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: iTunes interface is patented"
    date: 2003-06-13
    body: "True, but I think the main components of the iTunes interface that can be reasonable supported as new and unique are the artist/album drop downs, and it's unique device integration capabilities.  Also it's (now) media purchasing interface.  However, playlists in a column with a list of tracks in the other window is hardly unique.  Just my $0.02. =)"
    author: "anonymous"
  - subject: "no luck for me."
    date: 2003-06-13
    body: "I bumped into Juk yesterday when I was trying to get a decent playlist manager. Compiled and installed it. It searched and added the files to the playlist, but when I clicked on the songs, KDE gave error. I don't know what is the problem with it. It did not even form album lists. :("
    author: "nachiketa sahoo"
  - subject: "Re: no luck for me."
    date: 2003-06-13
    body: "Have you submitted a bug report? Scott's usually pretty quick to reply, and now that I've got a bit more free time I'm going to get back into JuK development."
    author: "Stephen Douglas"
  - subject: "me too"
    date: 2003-06-13
    body: "Just a \"meeeee toooooo\", but Juk is so much better than anything else on Linux for messy people like me who have songs spread about everywhere, not just in nice neat file hierachies. Id rather just throw the files at Juk and let it sort things out for me. \"Search All Visible\" is simply brilliant, I dont even have to think to get the songs I want, I just type whatever comes into my head first. Forget skins, they would only get in the way."
    author: "spooq"
  - subject: "UI doubleclick??"
    date: 2003-06-13
    body: "I notices that juk uses doubleclick to play a file - I thought KDE is per default single click?\n\nSo, what about\n\npoint: single select\npoint&drag: multi-select\nshift/ctrl+click: multi-select\nclick: play\n\nSo far juk looks nice!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "What I like about KDE..."
    date: 2003-06-13
    body: "... is how easy it is to connect to various parts. Start Juk, open your favorite terminal, and type:\n\n\"dcop juk Player\"\nYou get a nice list of available controls.\n\n\"dcop juk Player play\" starts playback, and so on... It makes scripting a breeze. DCOP rules ! :)\n\n"
    author: "rom1"
  - subject: "Re: What I like about KDE..."
    date: 2003-06-15
    body: "2nd that!\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: What I like about KDE..."
    date: 2003-06-17
    body: "let's see apple do something THAT open!\n\n"
    author: "Aaron Klaupt"
  - subject: "Re: What I like about KDE..."
    date: 2003-11-04
    body: "One word: AppleScript. It doesn't have to be open-source to provide a clean, scriptable interface, and AppleEvents (the interface) has been around since Mac OS 7.0 came out in the early 90s."
    author: "Anonymous"
  - subject: "GStreamer?"
    date: 2003-06-13
    body: "I don't really  understand how it can \"output\" to both aRts and GStreamer. They are multimedia frameworks both, they do the whole thing apart from the user interface.\n\nSo, how does it use both?"
    author: "Mike Hearn"
  - subject: "Re: GStreamer?"
    date: 2003-06-13
    body: "Well, it doesn't use both at the same time.  ;-)\n\nBetter said, if you have GStreamer installed when you're compiling JuK, you can choose between using aRts and GStreamer at runtime."
    author: "Scott Wheeler"
  - subject: "Re: GStreamer?"
    date: 2003-06-13
    body: "Couldn't aRts be made a gst sink (I think it already has been..), and can't gst be made an artsplayobject (I think it already has been)"
    author: "lit"
  - subject: "Re: GStreamer?"
    date: 2003-06-13
    body: "Maybe, but I thought the main target group for GStreamer were those who do not have arts installed (or deactivated it, or have arts-incompatible hardware, or...)."
    author: "AC"
  - subject: "Re: GStreamer?"
    date: 2004-04-01
    body: "or... prefer alsa for the speed."
    author: "name773"
  - subject: "Re: GStreamer?"
    date: 2003-06-13
    body: "There is a sink for arts. I don't think anyone is using it though... JuK output with GST is currently hardcoded to OSS.\n\nGST could be made an artsplayobject, but why bother? For pure playback it does not have any features that Xine does not have... GST shines when you do more complex things. One possibility that I am thinking about is to multicast the sound that JuK plays so other systems in the network can attach to it.\n\n"
    author: "Tim Jansen"
  - subject: "A User's Perspective on JuK"
    date: 2003-06-13
    body: "If my \"User's Perspective on JuK\" is negative and I reflect why in this topic, will my post be removed?\n\nIf the answer is no, why was my negative perspective removed? Last time I checked, I was not a trolling."
    author: "AC"
  - subject: "Re: A User's Perspective on JuK"
    date: 2003-06-13
    body: "What's even more disturbing is that no-one has picked up on the fact that the dot was hacked today. \n\nWhy did this happen?\n\nI even have the webpage source to prove it :)"
    author: "I'll wait for KDE 3.2"
  - subject: "Re: A User's Perspective on JuK"
    date: 2003-06-13
    body: "s/hacked/ISP was screwed up/"
    author: "ac"
  - subject: "Go Juk"
    date: 2003-06-13
    body: "I discovered Juk recently as well, it's a great app and simple to use. A big thanks to the authors. Can't wait for the lastest version in KDE 3.2."
    author: "Sean "
  - subject: "Noatun remains the undefeated champion..."
    date: 2003-06-13
    body: "I tried Juk today, but my experience with it wasn't potent enough to warrant a change from Noatun. I've never really been a mouse and click person, so perhaps Juk isn't for me to begin with. I've grown accustomed Noatun's 'global' keyboard combinations, skins and inumerable plugins(hayes playlist inclusive). Juk is great, but Noatun is better. And Xmms is just old. \n\nBut for Noatun and aRts control, kde multimedia is an embarrasment, an abomination and an abberations. I hope exciting changes are been made to Noatun for KDE-3.2 because I don't yet find Juk as worthy replacement. I also hope (K)mplayer becomes KDE's default media player. It will be interesting to integrate Noatun, Kscd and K(m)player into a single application. \n\nOtherwise, kde multimedia is just a joke. Are there any exciting features we can look forward to in KDE-3.2, with regards to kde multimedia? A thumbs up to both the Noatun and Juk team. To the kde multimedia team; we support you but we are yet to see the best from you guys. \n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: Noatun remains the undefeated champion..."
    date: 2003-06-13
    body: "Note kmplayer is no mm framework like arts, just a frontend for mplayer/xine/ffmpeg for people how know what they are doing (and have broadband internet). Its main focus is playing inside konqueror/khtml. So I don't expect kmplayer becoming the default media player ever."
    author: "Koos"
  - subject: "Re: Noatun remains the undefeated champion..."
    date: 2003-06-13
    body: "I very much like Kplayer. Like Kmplayer it is a frontend to mplayer, but its interface rocks! Even though it is only at version 0.2, it is very stable already and has a great GUI, check it out: http://kplayer.sourceforge.net. There is no playlist support yet, but for films it's just great!"
    author: "tuxo"
  - subject: "Re: Noatun remains the undefeated champion..."
    date: 2004-04-29
    body: "You know something?  I just tried this (umm, over nine months later.  LOL.)\n\nKplayer is at version 0.5 now (from CVS) and I like it.  It compiled fine and worked quite well, once I upgraded GNU automake as it required.\n\nI guess I'll have to try Kmplayer too, at some point.\n\nMaybe it doesn't make much sense to reply after this long a time, but on the other paw, let's hear it for archives :O)"
    author: "fLameDogg"
  - subject: "A note about the CVS version"
    date: 2003-06-13
    body: "Just to be clear -- a lot of people are trying the CVS version and coming away saying, \"Hey, this isn't stable at all.  This isn't release quality.\"\n\nSimply put, you're right.  It's not.  That's why it hasn't been released.  JuK has been under heavy development for the last several months -- there have been thousands of lines of changes and additions to JuK's code since 1.1.  There are crashes and bugs that I am well aware of and will get to before the next release with KDE 3.2, which is many months away.\n\nThat said, if you're using the stable version - 1.1 and still experiencing problems, *please* report them.  JuK 1.1 has very few known bugs; If more are reported I'll backport my fixes and release a 1.2.\n\nHEAD is where a lot of the cool things happen, but at this point in the release cycle (several months before we even hit the feature freeze) things will be broken on a regular basis; this is just the nature of software development."
    author: "Scott Wheeler"
  - subject: "FLAC support?"
    date: 2003-06-13
    body: "I haven't noticed it in any of the feature lists, but a requirement for any media player for me is the ability to play FLAC files.  I'm sure other people have other formats they want to play - does juk have input plugins?"
    author: "Jim Dabell"
  - subject: "Re: FLAC support?"
    date: 2003-06-13
    body: "I do not think that the Arts backend supports it, but GStreamer should."
    author: "AC"
  - subject: "Re: FLAC support?"
    date: 2003-06-13
    body: "KDE's lack of FLAC support is the only thing keeping me using XMMS. This is something that needs to happen at the aRts level. If that happen, any media player in KDE should \"just work.\"\n\nVote for bug #54394 in bugs.k.o - http://bugs.kde.org/show_bug.cgi?id=54394"
    author: "Blue"
  - subject: "Arts"
    date: 2003-06-13
    body: "I would love to use Juk, or any arts based player. If it wasn't for the sound click when I either:\n- start a compile.\n- switch virtual screen.\n- in fact, do anything other than staring at the player window (yes, this is a troll)\n\nMy box is quite decent (1.2Ghz 256Mhz) and XMMS plays audio without any problem. I have tried to increase the arts buffer, but it didn't work. Any interesting settings someone want to share?\n\nOne day, I'll try Juk with gstreamer :-)"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Arts"
    date: 2003-06-13
    body: "you should enable \"realtime priority\" in KControl, this ends this problems. But maybe you also have to set the '+s' bit of the arts executeable; turn the error messages on (also in KControl) to find this out."
    author: "Heiner"
  - subject: "Re: Arts"
    date: 2003-06-14
    body: "artswrapper is the binary to set the bit on.\nAfter that + enabling realtime priority it's quite hard to get arts playback to skip except if you have IRC problems or you try playing files from a hdd that is currently busy with other things (you know, buffers aren't infinite *g*)."
    author: "mETz"
  - subject: "Re: Arts"
    date: 2003-06-15
    body: "You're brilliant =D\n\nFinally, I can use juk, an average of 4% cpu-time on a XP1800 for a sound server is still ridiculously high (mplayer can play mpeg4 videos using less =/ ) but at least it delivers\n\n<scriptkiddymode> juk ownz u </scriptkiddymode>"
    author: "l/p: anonymous/e-mail"
  - subject: "Re: Arts"
    date: 2003-06-13
    body: "Try this:\n\nGo to kcontrol --> Sound --> Sound System --> Sound I/O --> Sound I/O method.  \nChange this to Threaded Open Sound System.  \n\nSee if that helps at all.\n\n\nsmeat!"
    author: "SMEAT!"
  - subject: "Re: Arts"
    date: 2003-06-13
    body: "Thanks for your answers. I'll try these settings when I'm back home."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Arts"
    date: 2003-06-14
    body: "Hmm, juk runs fine over here, even if i compile something in the mean time.\n\nrinse"
    author: "rinse"
  - subject: "Aaarrrhhh! My eyes!"
    date: 2003-06-13
    body: "What the hell is wrong with that site designer?! I'll be seeing green stripes for weeks. Oh the pain..."
    author: "Psiren"
  - subject: "Re: Aaarrrhhh! My eyes!"
    date: 2003-06-14
    body: "which site designer???"
    author: "mETz"
  - subject: "Re: Aaarrrhhh! My eyes!"
    date: 2003-06-14
    body: "Second link - http://www.slackorama.net/cgi-bin/content.pl?juk\n\nIt's not good."
    author: "Psiren"
  - subject: "Re: Aaarrrhhh! My eyes!"
    date: 2003-06-14
    body: "Hehe, i like that site :p\nIt's.. different... ;o)"
    author: "Bart Verwilst"
  - subject: "Re: Aaarrrhhh! My eyes!"
    date: 2005-04-14
    body: "wot is that site supposed to be?"
    author: "K'an't Thien'k"
  - subject: "Anyone tried both Juk and Yammi?"
    date: 2003-06-13
    body: "As the developer of Yammi (http://yammi.sourceforge.net) I really would like to try Juk, but that won't be before I update my whole system to a current distro (Juk doesn't compile on my current configuration).\n\nAnyone tried both applications? I think their concept is very similar, but I myself think that Juk is much better integrated into KDE (as Yammi, so far, also runs on a QT-only system). But maybe there are some good pieces in Yammi that could find their way into Juk (eg. the fuzzy search)? That's why I would like to hear the opinion of people who tried both...\n"
    author: "onoelle"
  - subject: "Re: Anyone tried both Juk and Yammi?"
    date: 2003-06-13
    body: "The fuzzy search sounds interesting indeed."
    author: "AC"
  - subject: "Re: Anyone tried both Juk and Yammi?"
    date: 2003-12-07
    body: "I have found yammi to be my favourite jukebox as i find interaction and music selection is more refined in it, the only problem is i use it with xmms for crossfading (crossfading is a freaking MUST HAVE on any jukebox) and xmms and yammi never like eachother for more then 10 songs or so....."
    author: "surfed"
  - subject: "Almost perfect."
    date: 2003-06-14
    body: "I am very happy with JuK, having just emerged it on Gentoo. It's much more useful than Noatun and SplitPlayList\n\nThere's one set of UI changes I'd like to see: when viewing the collection list, arrange the songs in a tree format, maybe by Artist, Album, Genre, and maybe Year. The exact heirarchy and tree depth should be user-selectable; I would prefer to sort by Artist alone, but other people might prefer something else. In most cases I can't imagine the tree getting more than 3 levels deep...you'd need a boatload of songs to make that really useful.\n\nMy reasoning is thus: a large song collection is rather cumbersome to navigate in a flat list. A collapsible tree would give users the option to collapse Artists/Genres/etc that they are not interested in.  Also, it would enable users to add a large number of songs to a playlist by dragging that level of the tree to a playlist. Besides, when you're looking for a song to play, don't you do it by classes? I usually remember either the artist or genre before I can remeber the name of the song I'm looking for.\n\nPlease note that I would _only_ apply this change to the collection view, not when viewing a playlist. Playlists are by nature sequential, and they should be displayed that way. The existing UI is excellent for that."
    author: "Steve B."
  - subject: "Re: Almost perfect."
    date: 2003-11-04
    body: "The Opera M2 mail client technique of having various 'views' of an underlying set of items looks like a good idea here. All your songs are in some file tree, but you'd like to have a display by date, by artist, by artist after a date... or filter to show only the songs with 'KDE' in the title (only kidding!). This is a powerful mechanism to allow the user to dynamically change the way their stuff is represented, independently of the file system underneath (which will usually remain album-per-directory for music).\n\nI'd agree with KDE multimedia being KMuddled what with so many alternative mixers, etc being available, and artsd being unruly. Time to slim it down (at least on Mandrake, which is many people's KDE showcase) and get it working for new users who can then explore happily.\n\nMiles\n\nM2 works pretty well now, by the way, after a slightly troubled start. Worth looking at (Opera 7.21 is current)\n"
    author: "miles"
  - subject: "Re: Almost perfect."
    date: 2004-01-30
    body: "I completely agree. I have gentoo as well, and of course we love choice (isn't that why anyone has gentoo?). I'd like to see the \"song library\" list as customizable as possible, with hierarchies or whatever I want. It might be useful to have tabs at the top (browse by artist)(browse by album)(browse by song) where browse by artist is a list of artists, where you can click on and get a list of that artist's albums, and then songs."
    author: "Ryan"
  - subject: "Queue"
    date: 2004-01-30
    body: "I'd really like to see a enqueue/dequeue for while songs are being played. I frequently play a shuffle of a playlist or all my songs, and while it's being played, I think of a song that I want to hear. In the new xmms, I can just press \"q\" on my keyboard and it will play that song next, and then go back to the shuffle. I'd like to see this function in juk."
    author: "Ryan"
  - subject: "Re: Queue"
    date: 2004-01-30
    body: "I just found the \"play next\" menu button. This is good, but I would like to see this expanded more (at least some notification that a certain song is going to be played next maybe? A way to dequeue?) It would also be nice if this was a queue instead of just a \"play next\""
    author: "Ryan"
  - subject: "mp4/aac/m4a support"
    date: 2004-04-15
    body: "I've been using Juk for a while now, its taken over pretty much from XMMS as my defacto player. Most of my files are in mp3 format (this is in forsight of buying a hard disk based player, maybe even an iPod if i can afford one!) I have recently come into ownership of a substantial quantity of mp4/m4a files which dissappointingly juk doesnt like to play.\n\nI was going to have a look at the source code for juk and see if there was any way I could implement this but i dont really know where to start.\n\nI have discovered a couple of tools which enable me to play mp4 / m4a / aac files under linux (XMMS being one of them) i discovered a really useful tool which enables me to convert to wav file its called\n<A href=\"http://www.audiocoding.com/download.php\">\"faad\"</A> http://www.audiocoding.com I think mplayer uses it! (unfortunately juk wont play wav's either and recompressing into another lossy format is not a route i wish to travel)\n\nfaad also allows the file to be output to stdio, this means it should be a really trivial job to get mp4 working in juk! Please , Please Please implement this feature , or at least give some clues to a clueless person how i might go about adding it myself using these tools; (who hasnt even considered the implications of tag editing!)\n\nnick ..."
    author: "Nicholas Fellows"
  - subject: "Re: mp4/aac/m4a support"
    date: 2005-06-05
    body: "well... I had a similar problem, and finaly, I tried that...\nIf you change your mind about recompressing the files...\n * If you find a way to play m4a with XMMS, I'm interested!\nthe following scripts need LAME and FAAD to work...\n\n###########\n# m4a2mp3 #\n###########\n#!/usr/bin/perl -w\nuse strict;\n\nforeach my $file (@ARGV) {\nnext if ($file !~ /\\.m4a$/i);\nmy $base = $file; $base =~ s/\\.m4a$//i;\nsystem \"faad -o \\\"$base.wav\\\" \\\"$file\\\"\";\nsystem \"lame -h \\\"$base.wav\\\" \\\"$base.mp3\\\"\";\nunlink(\"$base.wav\");\nprint \"$base.m4a converted to mp3.\\n\";\n}\n\n###########\n# mp42mp3 #\n###########\n#!/usr/bin/perl -w\nuse strict;\n\nforeach my $file (@ARGV) {\nnext if ($file !~ /\\.mp4$/i);\nmy $base = $file; $base =~ s/\\.mp4$//i;\nsystem \"faad -o \\\"$base.wav\\\" \\\"$file\\\"\";\nsystem \"lame -h \\\"$base.wav\\\" \\\"$base.mp3\\\"\";\nunlink(\"$base.wav\");\nprint \"$base.mp4 converted to mp3.\\n\";\n}\n\n"
    author: "niki"
  - subject: "Re: mp4/aac/m4a support"
    date: 2007-04-28
    body: "apt-get install xmms-mp4"
    author: "David"
  - subject: "Re: mp4/aac/m4a support"
    date: 2005-08-13
    body: "Check out http://www.qwirx.com/xmms-plugin-mp4/xmms-plugin-mp4-0.4-1.i386.rpm for the rpm and src's.\n\nI installed the rpm and it worked fine for all of my m4a's.  Hope it works for you."
    author: "MattoHippo"
  - subject: "Re: mp4/aac/m4a support"
    date: 2006-03-02
    body: "this works like a champ!  Thank you!\n\nChad"
    author: "Chad"
  - subject: "Re: mp4/aac/m4a support"
    date: 2006-03-06
    body: "In case you're still wondering, check \nhttp://www.captain.at/howto-debian-sarge-xmms-mp4-plugin.php"
    author: "Till"
---
In this entertaining review, <a href="mailto:savanna@kdenews.org">Savanna</a> takes us through her <a href="http://www.slackorama.net/oss/juk/">discovery of JuK</a>, a new <a href="http://www.perl.org/">pearl</a> in the <a href="http://developer.kde.org/~wheeler/images/juk/juk-default.png">treasure</a> trove of <a href="http://apps.kde.com/">KDE applications</a>.  Expect to see JuK ship with KDE 3.2 since it has already made an appearance in the <a href="http://www.kde.org/areas/multimedia/">KDE Multimedia</a> <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdemultimedia/">module</a>.  Kudos to Savanna for taking the time to contribute the review and, of course, hats off to the developers of JuK!
<!--break-->
<p><hr>
<h2>A User's Perspective on JuK</h2>
<i>by <a href="mailto:savanna@kdenews.org">Savanna</a></i>
<p>
<blockquote>
<i>
"Juk? What the heck is that?"<br>
 "You really should try it."<br>
 "But I use XMMS. I love XMMS."<br>
 "Well, give it a try. It's sort of like iTunes - a very nice playlist editor." <br>
 "Okay, okay...I'll try it out."<br>
</i>
</blockquote>
<p>
And so I did.
<p>
That is approximately how the conversation went a few weeks back on the #debian-KDE IRC channel. A person there named "grepper" told me to try it. Grepper knew one thing: I like pretty things. In fact, that is why I like KDE and have since around a year. 
 <p>
I first experienced it when I got a copy of <a href="http://www.debian.org/">Debian</a> installed on my backup machine. From there, I booted up KDE and started to play around. In about ten seconds flat, I had one of the nicest looking desktops I had ever seen, and I was hooked. 
 <p>
I'm a user, not a programmer. I don't know what makes most things tick in Linux and KDE, nor do I really want to. Only recently, I learned how to upgrade to the latest CVS packages and install an Nvidia driver Debian package without seeing anything but a console line - and without freaking out because I couldn't see a mouse cursor. 
 <p>
Okay, I admit it: I'm a blonde who isn't a techie. I'm learning because it is kind of fun, but I'll only go so far. I know most people who will read this will probably chuckle because this is for a techie site, but it is worth noting that I am a user who has switched her desktop from Microsoft to Linux with KDE. That is a pretty big jump. 
 <p>
So when Grepper talked about my switching from <a href="http://www.xmms.org/">XMMS</a> (comfortingly like a windows application) to Juk (something like a Mac application with lovely KDE tidbits - from my point of view), he knew that I would do so reluctantly. 
 <p>
What a surprise! 
 <p>
Juk is easy. Juk is elegant. Juk is simple. 
 <p>
Juk is awesome. 
 <p>
It opens up as a simple collection list with a space for icons in the left to make more custom playlists with. Nice big icons at the top make it very hard to miss the start/stop/skip functionality of the program. It looks friendly, and it is. Big columns on the right tell you everything you need to know. A search function at the top lets you instantly select things live from the collection list to make your playlist the way you want. A nice little icon in the tray on the Kicker lets you control the application from there as well. You right-click on the left area, create new playlist, name it, and then drag-and-drop from your collection list to your playlist. 
 <p>
You don't need to do anything else: it is that simple. 
 <p>
Every time it opens, it scans your MP3/OGG/Music directories (which you add very easily whenever you like) for any new music files. <a href="http://developer.kde.org/~wheeler/images/juk-cvs/juk-search.png" target="juk views">Alternate light gray and white rows make spotting songs a breeze.</a> A "jump to currently playing song" button on the bottom right makes it really easy to go to where you are, even while you are building more playlists and listening to another. A pop-up track announcement from the Kicker tray with a forward and backward skip button on either side comes up (if you want it) at the change of every song. I find this particularly useful. Right-click on the Kicker tray icon and you get a selection of the standard music player functions. Click on it with the left mouse button, and the entire program pops up. Another click minimizes it once more. There are no flashy player skins from outer space, or separate player displays. This is a simple program which doesn't need many bells or whistles. 
 <p>
Everything is big and friendly. 
 <p>
Big friendly icons make for happy users. 
 <p>
I was hooked. In fact, I was so hooked that after I got the stable version from orth's CVS debs, I switched everything to Juk and no longer use XMMS. 
Now, I do miss the XMMS skins, and I had quite a collection, I'll tell you. And I miss the plugins feature for falling asleep and waking up with music - but wheels assures me that this is going to be coded in relatively soon so I'm no longer worried about that. 
 <p>
Other than that, it is a dream come true. There is something to be said for a Mac-design where things are supposed to be friendly and simple for regular users. Juk hits that on the head. I love XMMS but it was sort of tiny on my screen and making a good set of playlists accessible was, at times, kind of annoying. I also like <a href="http://noatun.kde.org/">Noatun</a>, but I have some issues with it at the moment - though with the <a href="http://www.freekde.org/neil/hayes/">Hayes playlist</a> feature, it was as close to Juk and about as friendly and intuitive as I've ever seen it before (and does have its own very nice merits). 
 <p>
But Juk is...perfect. Well, so far. It screams: "non-coders will use me happily", and that is a good thing. 
 <p>
I love KDE because it is easy to use. Juk follows that example and reminds me, once again, why I run KDE in the first place. 