---
title: "Preliminary KDE 3.2 Release Schedule"
date:    2003-06-17
authors:
  - "binner"
slug:    preliminary-kde-32-release-schedule
comments:
  - subject: "Xmas!!!"
    date: 2003-06-17
    body: "Wow! KDE 3.2 would make a great Christmas-present.\n\nHope you guys make it in time!\n"
    author: "Robert Schouwenburg"
  - subject: "Great to see it at last !"
    date: 2003-06-17
    body: "The plan looks great. With more than a month for the final bug fixes. I have the feeling that one beta will not be enough, just because the integration in the PIM area will be very involved, but we will see. Even if it is released early next year, it is great news. For most users this is irrelevant, because most of the distros will release around september/october anyways, and KDE 3.2 will be out for the following release (march/april 2004). \n\nHaving the Nove Hrady meeting as a discussion forum right before the release cycle is gonna help so much. Cheers, thankyous and congrats !"
    author: "MandrakeUser"
  - subject: "Kwallet"
    date: 2003-06-17
    body: "Is it still likely that KWallet will make it? Only 2.5 months left, and no commit since many many months...\n"
    author: "AC"
  - subject: "Re: Kwallet"
    date: 2003-06-17
    body: "its being talked about... \nmeasure twice cut once...\n\nwith something as important as a safe place to store creditcard # and passwords, i would hope some thought was putinto it.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Kwallet"
    date: 2003-06-18
    body: "Personally, I'd appreciate even a very simple kind of KWallet for Konqueror just to save username&password combinations. It's particularly annoying having to retype them in every dialogue/form! Then a really secure system could come later."
    author: "Tom Chance"
  - subject: "Re: Kwallet"
    date: 2003-06-17
    body: "I'm gutting everything that is there and redoing it.  I will commit in a large chunk, most likely by the end of the month for the bulk of it.  I'm developing it locally for now.\n"
    author: "George Staikos"
  - subject: "Re: Kwallet"
    date: 2003-06-18
    body: "Wow! Great, I'm willing to start using it. I was just about to code the part of my app related to password storage, but I may wait till then."
    author: "uga"
  - subject: "After KDE 3.2???"
    date: 2003-06-17
    body: "Will it be KDE 4.0 or KDE 3.3? /me hopes Qt 4.0-final comes out sometime in the middle of 2004. That would mean a right time for KDE 4.0. If it comes out later than that, perhaps a quick release of KDE 3.3 would be better."
    author: "John"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "KDE will be called KDE 4.0 as soon as it requires Qt 4. That's it. :)\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "Why?\n\nWe should have at least another 2 years of Qt 3.  Most companies upgrade about once every 2-3 years, some will put off almost 5....  To rush a major upgrade so often only allows us to damage our userbase.\n\nI have just moved my last client from KDE 2.2.2 here.  They are now on KDE 3.1.2.\n\nRemember we are not trying to cater to kids in their basement any more, those of us trying to make money off of this need a stable longterm project.\n\nJust my 2c\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "> Why?\n\nI know reasons. There are many desirable things that may require a new major release (because they will most likely break binary compatibility), like\n\n- a solution for reflection of API classes, so languages like Python and C# can access all KDE APIs all the time without the need for wrappers. Basically the functionality of COM or CLR, but hopefully nicer\n- a standard for binary compatibility with the goal that you can just copy a binary (for your CPU) on your computer, execute it and it runs. Or that more complex apps can use something like autopackage.org. The goal would be easy installation and less work for vendors/projects that create external KDE apps. Basically a Windows-like situation....\n- a new multimedia system\n\nThe problem with all 3 issues is that I don't know a good solution for any of them. But if at least two of them could be solved, that would easily justify a new major release.\n\n \n"
    author: "Tim Jansen"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "AFAIK the only thing that justifies a new major release is binary and source compatibility changes within the KDE libraries.\n\nas for the three points you enumerated, the second one is not in the purview of KDE as much as it is the OSes it runs on. a new multimedia system may not necessarily destroy the old API; it may fit in transparently or even just keep the old API for compatibility (something I'd hope would happen even in a major release) and provide a new preferred API. this preserves compatibility and avoids a major release.\n\nas for the reflection/clr point, this is the one most likely to create problems if it requires any sort of vast internal changes. but if it is provided as an add-on layer then we may once again be in the clear.\n\nian is very, very right about maintaining long period of compatibility. not doing so hurts our users and tech companies and contractors who depend on KDE for their liveliehood (yes, those exist, and yes, there are more and more of them popping up).\n\nof course, this isn't an excuse to avoid progress in KDE where necessary. rather, this is a great ballancing act that the entire KDE team must chart through with prudence. i do not envy the release dude(tte)."
    author: "Aaron J. Seigo"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "I understand the concerns, but IMHO at least the first two points hurt even more.\n\nNumber 1 prevents people from using more languages that may make them more productive. Right now when using a scripting language you feel like a second-class citizen. It also hurts future compatibility - C++ will not be the right language forever. I would feel much better if I knew that the stuff I am working on today will not be obsolete in 5 years because there is no way to use the C++ code by whatever language has displaced it then. And I am pretty sure that I won't use C++ in 5 years. I don't know whether it will be a future version of Java, or C#, or Pike, or maybe something else, but certainly not C++. I am ready to switch as soon as they are feature complete, fast and stable enough. \nAnd the longer KDE's APIs are not future-proof, the more work will it be to make them (as they are growing all the time).\n\nNumber 2 hurts because it makes it impossible for most users to install software that has not been distributed with/for their specific distribution. And it hurts vendors since it increases the cost to support KDE (BTW this is not the OS/distributions fault: only KDE can give instructions on how to build KDE using which tools in order to make KDE apps compatible among distributions - it's unlikely that distributors will create suc a standard).\n\nAll this talk is irrelevant right now, of course, since there's no solution yet for any of these problems.\n"
    author: "Tim Jansen"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "> Number 1 prevents people from using more \n> languages that may make them more productive.\n\nagreed; language support is nice. but let's be a wee bit realistic here: even when there are language options people usually pick just a few of them for the vast majority of work. and no, we don't need to build into our libs some magic future proofing. what we \"need\" is a layer that talks to our native libraries that exposes and easy-to-bind set of interfaces.  i keep hearing rumours of such things from various binding people, so we'll see.\n\n> And I am pretty sure that I won't use C++ in 5 years\n\njust like nobody uses C anymore ;-)\n\n> know whether it will be a future version of Java, or C#, \n> or Pike, or maybe something else,\n\nRuby? =)\n\n> Number 2 hurts because it makes it impossible for \n> most users to install software that has\n> not been distributed with/for their specific distribution.\n\nof course. but KDE can't say, \"SuSe, you will now start using gcc x.y, XFree86 a.b with extensions foo and bar, provide these versions of these specific libraries ....\" let's not even get onto the topic of systems like Solaris. we can recommend tools and libraries (and we already do) but we can't force the vendors to comply. the LSB is an important tool in this direction. and before anyone decides to whip out their LSB hate on me, i suggest they look long, hard and with rational minds at the evolution of the LSB because then you'll perhaps realize that while it isn't there yet, it is quite far along the trail and it has massive buy-in which is what really counts.\n\nof course, a new version of KDE, minor or major, is not going to fix this. even if KDE started shipping a complete OS it would only further the problem by adding one more derivative to  the mix."
    author: "Aaron J. Seigo"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: ">what we \"need\" is a layer that talks to our native libraries that exposes and\n>easy-to-bind set of interfaces.\n\nThat would have similar problems as the current language bindings: they are always at least one step behind. And it's also a lot of work. What we need, IMHO, is to use that kind of layer as primary API for all languages, including C++. COM and QSA's use of Qt slots show that this can be possible without less comfort for the C++ developer. \n\n> Ruby? =)\n\nOnly type-safe languages for me...\n\n> we can recommend tools and libraries (and we already do) but we can't force the\n> vendors to comply. \n\nWe can't force them, but we could promote those distributions that follow the standard.   And I think as soon as 2-3 distributions support it and developers are starting to release their software as convenient binaries, the users would want their distribution to adopt it. \n(BTW part of the problem is that such a thing needs a lot of other requirements, like LSB, or KDE needs a lot more wrappers for system-near functions that would otherwise be specified by the LSB - and it's a good thing to make KDE more platform-like, I would like to write a server or a web site using the KDE libs)\n"
    author: "Tim Jansen"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-18
    body: "I\u00b4m glad to see references to Pike.\nAnybody tried to get Qt/KDE wrappers for Pike?\nI\u00b4m not quite happy with Gtk (even when wrapped in a OO language like Pike becomes pretty usable). But I wasn\u00b4t able to hack the ugly Perl mess to get it working."
    author: "Julio G\u00e1zquez"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-19
    body: "> Only type-safe languages for me...\n\nHey now... Ruby is more type-safe than C, C++, or Java.  Maybe you are thinking of staticly-typed languages, which Ruby is not.  However, it *is* strongly-typed - more so than C++, where a reinterpret_cast or C-style cast can bypass the type system.\n"
    author: "Avdi"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-18
    body: "I won't whip out my LSB hate because I don't hate the LSB.  But, I do have an engineer's skepticism.\n\nI do NOT see how the LSB or the FSH will ever solve this problem.  Didn't say that it wasn't possible, only that I don't see it. :-)\n\nYou are welcome to read my comments on: dlc-discuss@desktoplinuxconsortium.org:\n\nhttp://www.desktoplinuxconsortium.org/pipermail/dlc-discuss/2003-April/thread.html\n\nPlease join the list and comment on what was said:\n\nhttp://www.desktoplinuxconsortium.org/mailman/listinfo/dlc-discuss\n\nNote that there has been only one posting (which was OT) since the thread I started.\n\nI think that they just don't get it.  They will not solve the problem because they don't see that there is a problem to be solved.  Actually, it is a problem CREATED by the distros.  Just look at how RedHat slices and dices packages -- installing the parts in what appear to be very obscure locations.\n\nWhat we need is (to be able to have) a set of generic KDE RPMs that will install on any LSB system subject only to the normal requirement that all needed libraries (with versions >=  the requirement) are present.  If that were possible, then applications could easily be installed from a standard RPM.\n\nUntil that will work, the LSB is useless, and I don't really understand what they are doing.\n\nI can see enhancements that could be made to RPM (or KPackage) that might make this possible (or easier), but what we have now is distro fragmentation just the same as the fragmentation that killed UNIX on the desktop. :-(\n\nBut the only real solution that I can see is a GNU distro for Linux that will set the standard for where things should be installed.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: ">  ian is very, very right about maintaining long period of compatibility. not doing so hurts our users and tech companies and contractors who depend on KDE for their liveliehood (yes, those exist, and yes, there are more and more of them popping up).\n \nWhy not just install multiple versions of the libraries concurrently and implement a thin layer that auto-magically manages them --- something like Microsoft's new Strong Binding in Longhorn. \n\nMicrosoft has broken binary compatability between different releases of their system dll's for years, but Windows users get the all the benefits of having binary compatablity. The only downside of this was \"DLL Hell\"-- conflicts between different versions of the same dll which caused apps to be broken. Strong binding fixes this. "
    author: "ANON"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "It's should be obvious that even with \"auto-magical fixes\" like \"Strong Binding\" (heh) this is simply bad design and shouldn't be necessary at all."
    author: "Datschge"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "> - a solution for reflection of API classes\n\nObviously not all languages are reflective, however I've been toying with ideas for some kind of decent component model for a while now. No time to experiment though :)\n\nOne idea I had was to do something like Swig, but in multiple directions, ie not just C++ -> scripting languages. It would work a bit like GStreamer by connecting the elements, so for instance if you don't have a plug in for direct binding C++ to Ruby, it would first bind it into GObject, then from GObject into Ruby. Inefficient yes, but OTOH I believe that doing automagic bindings with the ability to do manual overrides produces bindings of a far superior quality to that of COM. \n\nThe main problem is that this would not be contract based, ie no component activation. But I'm not really convinced that it's hugely useful anyway. Outside of embedding spreadsheets in word processors etc, which could be done using IPC, I see little real world usage of it. COM mostly defines interfaces specific to an implementation, and where plugin style code is used (shell, urlmon, ole embedding) it's rarely implemented.....\n\nOf course, COM is widely used, and this is just my ranting :)\n\n> - a standard for binary compatibility\n\nWell the LSB is attempting to specify the ABI of many libraries, but I'm not sure how useful that is. Beyond glibc symbol version sets, the ABIs are versioned via libtool anyway. And projects like KDE and GNOME take binary compat very seriously now, which is good.\n\nBasically the problem is more one of dependancy management, ie being able to request a set of ABIs and know they are present.\n\n> with the goal that you can just copy a binary (for your CPU) on your computer, > execute it and it runs. \n\nSure. If all its dependancies are present. But we can already do this today, with a little bit of work. Just need to make it super easy for developers to know about, and get the word out.....\n\n> Or that more complex apps can use something like autopackage\n\nLet's hope so eh? :) This is not a KDE issue though, it's separate to the desktops in my view....\n\n> - a new multimedia system\n\nThat can be phased in next to aRts, don't have to scrap the old one to use a new one. GStreamer! :) But I know you already like that software.... bloat isn't really an issue here. Neither arts nor GStreamer are exactly huge by todays standards. Why not have two?\n\n"
    author: "Mike Hearn"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-18
    body: "> OTOH I believe that doing automagic bindings with the ability to do manual\n> overrides produces bindings of a far superior quality to that of COM. \n\nI would rather have a single interface than learning a new one for each language.\nAnd if each language has a different interface it must have its own API docs etc..\n\n>> with the goal that you can just copy a binary (for your CPU) on your computer, > >execute it and it runs. \n> Sure. If all its dependancies are present. But we can already do this today, with a >little bit of work. Just need to make it super easy for developers to know about, and\n \nThe difficulty is to a) get all distributors compile KDE with the same compiler version, flags etc and b) to find abstractions for everything that is currenlty outside of KDE's scope (either by requiring something like LSB, or by writing KDE wrappers).\n\n"
    author: "Tim Jansen"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-18
    body: "> I would rather have a single interface than learning a new one for each  \n> language.\n> And if each language has a different interface it must have its own \n> API docs etc..\n\nYes, that is the downside. The problem with things like COM though is you need to suddenly use non-native strings, can't use language features like iterators etc\n\n> The difficulty is to a) get all distributors compile KDE with the \n> same compiler version, flags etc\n\nOnce we are past the ABI break, that shouldn't be a big problem anymore.... and this was a one off.\n\n> b) to find abstractions for everything that is currenlty outside of KDE's \n> scope (either by requiring something like LSB, or by writing KDE wrappers).\n\nWell I don't really understand this part, why do you need to add another layer to everything? That doesn't imply greater stability."
    author: "Mike Hearn"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-18
    body: "> Yes, that is the downside. The problem with things like COM though is you need to\n> suddenly use non-native strings, can't use language features like iterators etc\n\nThat would be part of the language binding to the component model. A developer using Python should not, of course, have to convert their strings into a QString first. This will not always be possible, but if there is a nicer language-specific way to design the APIs you can always add better wrappers later. But no one should be required to use them, because they cant be always available.\n\n\n> Well I don't really understand this part, why do you need to add another layer to\n> everything? That doesn't imply greater stability.\n\nKDE's APIs are not complete. Many apps need POSIX or possibly even more OS-specific syscalls. When your app, for example, needs a list of processes it has to parse the /proc file system. And when the /proc interface changes this is as bad as a change in one of KDE's lib for binary compapatibility.\n\nSo there are three possibilities:\n- KDE needs to require certain properties of the OS, like the /proc file system (listing them would be a huge amount of work...)\n- the KDE compatibility standard must be build on top of other layers, like LSB\n- KDE must forbid apps to use non-KDE APIs if they want to stay compatible. Maintaining binary compatibility would be only KDE's responsibility then. This implies, of course, that KDE needs to wrap a lot of interfaces that are not accessible by the existing APIs.\n\n"
    author: "Tim Jansen"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-18
    body: "> That would be part of the language binding to the component model.\n\nYes, I suppose you are right. Designing a component model that fully takes advantage of every languages features though is not really possible, you would always be accused of \"dumbing down\" like the accusations against .NET (which is a nice way to do things imho, but perhaps not politically acceptable).\n\n> And when the /proc interface changes this is as bad as a change in one of KDE's > lib for binary compapatibility.\n\nSo, the kernel developers should \"get\" binary compatability too.  I don't think it's KDEs place to abstract every little thing - binary compatability is everybodies task, I don't think KDE/GNOME should be attempting to cover up others lack of correctness here."
    author: "Mike Hearn"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-19
    body: "> So, the kernel developers should \"get\" binary compatability too. I don't think \n> it's KDEs place to abstract every little thing - binary compatability is \n> everybodies task, I don't think KDE/GNOME should be attempting to cover \n> up others lack of correctness here.\n\nYes, BC in/by the kernel would be nice, but there are other reasons for offering all POSIX functionality with KDE APIs (a single API for everything, less problems when porting to non-Linux systems...).\n\n"
    author: "Tim Jansen"
  - subject: "Re: After KDE 3.2???"
    date: 2003-06-17
    body: "I wholeheartedly agree. Unless there is a compelling reason for switching to a new Qt codebase, the KDE project would be better off having a long release cycle in the 3.* series, providing API compatibility, so that people can develop on KDE without having to port to a new API in six months :-) "
    author: "MandrakeUser"
  - subject: "KDE needs polish."
    date: 2003-06-22
    body: "All KDE needs to do after 3.2 is work on polish.\n\nThis means make it easier to use, add more things like the control panel, make it easier to install programs, perhaps make it possible to upgrade KDE at the click of a button instead of having people go through hell to upgrade it.\n\nOh and SVG Icons, Fonts, Widgets etc, we need to make the KDE interface run as SVG and phase out pixels. The Xwin guys are working on the backend, KDE needs to have the frontend ready to actually take advantage of hardware rendered vector graphics.\n\nOverall KDE just needs polish, all the major issues with KDE are over, we dont really need more software, we just need to improve the current software.\n\nFinish and improve Kopete, Polish the interface, go SVG to make it modern, work on figuring out how to make KDE the easiest to use interface on the planet, add useful features, take a few ideas from Apple and Microsoft, and just make it ready for the tidal wave of casual users who will switch to Linux in the next few years.\n\nKDE 4.0 should be released to compete with Longhorn in 2005, hopefully by this time the interface will be fully SVG, everything will be polished, transgaming will have games working, programs will be easy to install, and hopefully the fonts and alpha channel will be working and not just cheap software hacks.\n\nWhen this happens, Linux will be set to compete with Longhorn on the desktop, KDE team should position itself to beat Longhorn and whatever Apple comes up with next, this means KDE developers should focus on innovation, so we have the Konquerer browser? Add some features to it that no other browser has, INNOVATE.\n\nInnovation is the only way Windows users will use Linux, its the only way Linux will compete with Longhorn, hopefully some developers are reading this, INNOVATION is the key, you want Microsoft to copy us, not us copying Microsoft.\n"
    author: "HanzoSan"
  - subject: "Its time to Innovate."
    date: 2003-06-22
    body: "\n\nI think KDE is functional, the only thing thats missing right now is innovative new features, and polish.\n\nDont copy Apple or Microsoft, currently KDE is looking like a Windows clone, its fine to take good ideas from Windows, but after KDE 3.2 I'd think its time to innovate, come up with our own ideas, and make Linux the easiest to use OS by 2005.\n\nThe Slicker project seems to be a good start, its innovative, and could have potential to make Linux easier to use than Windows.\n\nhttp://slicker.sourceforge.net/\n\n\nBelieve me if the KDE team and Linux does not innovate, and decides to copy Windows Longhorn after we see how it looks, No ones going to switch to Linux, why use something which is trying its best to copy something else?\n\nLinux and KDE needs an identity, I think its currently as functional as Windows, it has all the software anyone ever needs, now whats needed are features that no other OS has. \n\nMy suggestion would be to focus on polish for the 3.3 release, also I'd focus on making KDE more modern by switching to SVG.\n\nHopefully in 2004-5, I will see KDE 3.3 come out and be the easiest to use interface out there, with features Microsoft nor Apple have, if this happens I think Linux will have a good chance as a Windows replacement.\n\nI see alot of developers talking about working on the backend, thats the problem with Gnome development, Gnome 1.3/2.0/2.2, it all looks alike, I dont see it being any easier to use, in fact it seems like it gets harder to use every time I try it. \n\nI think the future of Linux will be on the desktop, Gnome currently is so awkward that I dont see anyone picking Gnome over WindowsXP or OSX, KDE has a chance.\n\nThe only thing a normal person cares about when they use Linux on the desktop is how intuitive it is, and what features it has.\n\n\n"
    author: "HanzoSan"
  - subject: "Re: Its time to Innovate."
    date: 2003-06-22
    body: "> I think its currently as functional as Windows, it has all the software anyone ever needs\n\nYou're so wrong. Oh, and you forgot to mention when you will start to send patches."
    author: "Anonymous"
  - subject: "Re: Its time to Innovate."
    date: 2003-06-24
    body: "\n\nI have done some Linux development.  Alot of the ideas I have I'm not capable of doing, considering that I am not a master of C++, I dont know how Xrender works, or how libSVG works, I just know how to innovate.\n\nWe have enough coders, a coder with no vision is just a machine cranking out code, we need vision."
    author: "HanzoSan"
  - subject: "kexi"
    date: 2003-06-17
    body: "It's bad that KDE 3.2 will not have kexi! When shall we have it? Whenever it comes out, converting my more than 27 MS-Access programs into native kexi programs will start in full swing. Thanks for the good work kde developers"
    author: "Charles"
  - subject: "Re: kexi"
    date: 2003-06-17
    body: "1. Kexi is part of KOffice, not KDE. It will not be in KOffice 1.3\n2. If it's not in KO 1.3, it *may* still be released separately when it is finished\n\n"
    author: "Tim Jansen"
  - subject: "KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Hello friends,\n\nCan we expect a major UI overhaul in KDE-3.2? Also, are fonts and font rendering much improved? Right now they are horrible. My fonts don't look anti-aliased and they are sometimes disturbingly misaligned in some window decorations. Furthemore, how about kde-multimedia, can we expect significant improvement or changes in KDE-3.2? \n\nIn addition, apart from new integration of tools, new features and bug fixes, are they any major enhancements being made to KDE's fundamental architecture? I'm talking about kdelibs, kparts, dcop and basically any other fundamental rewrites that can make KDE much responsive, much faster and utilize better memory/system resources. Perhaps, even lighter though I admittedly doubt this.\n\nI apologize for all the trivial questions but I'm just curious. Of late, the news has been about new features and new apps, which are well appreciated and welcomed. But not much has been said about the improvement being made, if any, to KDE's underlying architecture. Maybe not much needs to be said about it. Maybe it ain't broke, so there's no need to fix it. *shrugs*\n\nI'd also like to commend the developers, the administrators and the community, in general. You're all doing a great job.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: ">  Also, are fonts and font rendering much improved? Right now they are horrible. My fonts don't look anti-aliased and they are sometimes disturbingly misaligned in some window decorations.\n\nFont rendering done by KDE? You don't want a new KDE version but install other fonts/fix your system."
    author: "Anonymous"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Are you using TrueType fonts? I have no problems with my fonts. They look as good or better than my windows machines. If you are using bitmap fonts, yeah I will agree with you!"
    author: "Echo6"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Hello again,\n\nI thought the KDE fonts where all bitmap fonts. The fonts on kde look a tad different from, say, that of XFCE or Gnome. In other words, the fonts look much smoother on XFCE or Gnome than on KDE, at least on my system. So I'm confident it's not a problem with my system fonts. Because of this anomaly, I was under the impression that KDE had a perculiar rendering technique that makes the fonts on KDE look a little rough on the edges on my system. Fortunately, I'm wrong. I'd check to see if I haven't configure something right in KDE or if I've improperly setup my font server. :?\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Fonts on Linux can be a bit of a pain. You can run bitmap fonts, Adobe Type 1 and now True Type. Antialiasing came available early in XFree 4x. X manage this and QT was about the first program to support the xrender extention. So something like a year and a half ago KDE had antialiased fonts. Gnome just recently got them. I seem to recall something a while back about an early solution they had where they sort of hacked some of the code to get it done. I don't know if the current install is at all non standard but I am wondering how it shows up right there and not on KDE unless you simply have selected bitmap fonts on KDE and GNOME has good fonts.\n\nNow would be a good time to ask what your distro is if this is set up so badly. Anybody for conspiracy theories? ;-) I'm guessing if you were to grab a Knoppix CD you'd be amazed at all the improvments suddenly in KDE in 3.1.2."
    author: "Eric Laffoon"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Hello again,\n\nHmmm...very strange. I use Gentoo Linux, arguably the best distro for a responsive and well installed KDE. I also currently use KDE-3.1.1a. I'll be updating to version 3.1.2 most probably this weekend. I use the Verdana font, a true type font provided in XFREE 4x. The rough edges are almost invisible to the eye, except on close inspection or if I use a font larger than 12. I'm pretty sure I enable the anti-aliasing option in KDE's Kcontrol. \n\nNow that I know it's problem specific to my system, I'll try haunt down the reason for this strange behaviour. I also need to mention that there is sometimes no difference between certain fonts. For example, on my system there is no difference between symbol, verdana, and helectiva,[b]especially on KDE[/b] if I'm not mistaken. This is just one example, there are several fonts that look exactly the same. I'm begining to suspect this has something to do with my font configurations. I'll check that out when I get back home.\n\nReagrds,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "> Hmmm...very strange. I use Gentoo Linux, arguably the best distro for a responsive and well installed KDE.\n\nWell then your problems will largely depend on the last time you rsynced and emerged. Gentoo has been moving toward a single font configuration in /etc/X11/ from XF86Config and XftConfig to fs/config. Even worse is the fact that for some reason ghostscript builds with hard coded font paths and refuses to use any newly listed path so that things display one way and print another if you've added new font paths. As good as Gentoo is this is a bit of a mess. I've been back and forth trying to resolve the font issues with little satistaction. At least they look great on screen and the Corel fonts I directly imported into my default Type 1 font directory work great for KDE apps. Of course I can't read a printed email. Some day I'll spend some more time and get it sorted out but for now it's not really that much to do with KDE as with growing pains with X and Gentoo attempting to resolve them. Ironically previous builds of XFree and Ghostscript worked virtually perfectly.\n\nNext time I have time to work on it when I can see straight I may just blitz bugzilla because I think people have found these problems on the forums and not reported them."
    author: "Eric Laffoon"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "for Gentoo, type in a shell:\n\nfc-cache\n\nAll my fonts work great"
    author: "AC"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "I had this problem, I'd forgotten to do rc-update add xfs default\n"
    author: "MaRTiaN"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "> Can we expect a major UI overhaul in KDE-3.2? \n\nIn some parts, like kcontrol.\n\n> Also, are fonts and font rendering much improved? \n\n KDE itself doesn't do any font rendering. So we can't improve it. It's all handled by the freetype library. KDE/qt/GNOME/gtk access freetype through another library called xft2. \n\n> My fonts don't look anti-aliased\n\nMight be a configuration problem, but nothing KDE developers can do about it. Up to the distro maker. \n\n> disturbingly misaligned in some window decorations\n\nPerhaps a problem with the font (improper baseline/ascent/decent) or xft/freetype.\n\n> Furthemore, how about kde-multimedia, can we expect significant improvement or changes in KDE-3.2? \n \njuk is being added in kde 3.2. again, bigger changes will happen in kde 4.0-- perhaps a removal of aRts for something else. It's a long time until then, we'll see :)\n\n>  In addition, apart from new integration of tools, new features and bug fixes, are they any major enhancements being made to KDE's fundamental architecture? I'm talking about kdelibs, kparts, dcop and basically any other fundamental rewrites that can make KDE much responsive, much faster and utilize better memory/system resources. \n\nWe can't really do much until kde 4.0 in this department except for optimizations. This is because we need to keep binary and wire compatability with KDE 3.0-KDE 3.2. \n\nOf course, there is ongoing work on things like dbus and other freedesktop.org standards."
    author: "anon"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Hello,\n\nThanks for your insight. I, like everyone else, look forward to improved versions of KDE. :)\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "I'm not sure why you say that.  I'm thrilled to be using KDE these days, I think it's quite wonderful.   I'm sure there are improvements that can be made, but I see it as being quite nice in appearance and functionality.   I always look forward to what the KDE folks are doing, but I trust them to do a good job."
    author: "TomL"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Hello,\n\nI'm as thrilled as you are to be a fanatic KDE user. It is simply the best and most powerful Desktop Environment I've used. And I spend every opportunity I get encouraging and commending the KDE devs and community alike. There efforts are much appreciated. However, it is almost an unwritten consensus that KDE needs a user interface update,overhaul, redirection or redesign. Especially with the introduction of Keramic as KDE's default theme. Keramic while initially attractive becomes clunky, bulky, unnatural, somewhat unresponsive and just plain annoying with time.\n\nBut the joy of using KDE lies in the power it bestows upon its users. The multifarious options, customizability and configurability empowered me to immediately customize my KDE sytle and window decoration to suit a simple, slim and sleek elegant design(latest version of 'dotNET' for sytle and latest version of 'MKUltra' for windows decoration). After a few tweaks and within a few minutes my windows were much more responsive and a sight to look at. \n\nContents within widgets are sometimes improperly scaled. An arrow, a picture or a font should fit nicely and naturally within its frame and approximately equidistant from the borders that encapsulate it, regardless of fonts size or type. Only today the directional arrow on Korganisers calender looked oversized for the widget that contained it. Not a big deal, but it is annoyingly distracting and sometimes ruins the pleasant environment KDE kindly spoils you with.\n\nI could go on, but I won't. Once again, KDE is the best desktop environment I've experienced and I genuinely wish it improves as it matures. It is almost 30% faster than XP on my system. And provides me with more tools, choices and tweaks that other desktop environments can only dream off. I simply love it.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "Rest assured, it _will_ improve. It improves all the time. I remember running the first beta of 2.0, and I was almost chocked by how slow and buggy it was. Prettier than 1.x, yes, but insanely slow. It improved rapidly, to say the least.\n"
    author: "Apollo Creed"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "> Can we expect a major UI overhaul in KDE-3.2?\n\nthis is really a trickier goal than most people give it credit for. we have people (aka users) who are used to the way KDE is today, and we have many areas that should be improved. how do we maintain enough familiarity that we don't disenfranchise users but still improve? how many person hours will it take to do it right? how much time do we actually have, both in terms of GUI designers/developers/testers and in terms of the patience of our user base? how much real world data do we have to base changes on? how fast can we collect and interpret said data? IMHO the interface will likely evolve at a manageable pace, not go through massive revolutions... this is an elephant we have on our hands, and there's only one sane way to eat such a beast that i know of: one bite at a time. some bites will be bigger than others, but i don't think the project is ready to swallow the whole thing in one go."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Sounds reasonable to me.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-17
    body: "Heck, we don't need a major UI haul. I'd be happy with some tweeks and polish:\n\n1) Reducing toolbar and menu clutter. The OS X KOffice screenshots show this clearly. Far too much crap in the default setup, looks especially garish on the otherwise elegant OS X setup. Let's just say this: more apps should look like Quanta and fewer should like like Kivio. \n\n2) More font sensitivity. I have a 133dpi flat panel, and as a result run larger than usual fonts. KDE handles my setup better that pretty much every other GUI (blows away Windows in this department) but still has rough edges. For example, there are several places (menus, kate sidebar) that have non-adjustable 16-pixel icon bars. 16 pixel icons look positively comical on this screen. Many windows will open up with important widgets (okay/cancel buttons) hidden until the window is resized. \n\n3) Adoption of an HIG. Heck, large parts of Apple's HIG are equally applicable to KDE. In fact, KDE has always reminded me a lot of a messy version of classic MacOS. That's a compliment, because getting the little details right is a lot less work then getting the overall paradigm right. I think KDE has the right foundation, but still needs work on that polish.\n\nPS> I'm saying this as someone who uses KDE and only KDE. In fact, I use maybe 3 non-KDE apps regularly, so the progress of development of KDE is rather important to me :)"
    author: "Rayiner Hashem"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "> Heck, we don't need a major UI haul. I'd be happy with some tweeks and polish:\n \n> 1) Reducing toolbar and menu clutter. The OS X KOffice screenshots show this clearly. Far too much crap in the default setup, looks especially garish on the otherwise elegant OS X setup. Let's just say this: more apps should look like Quanta and fewer should like like Kivio. \n \nLook at that! I've done something that is being pointed to as a good example. I'm tickled. I hope some of my former school teachers who thought I didn't take my studies seriously are reading this. ;-)\n\nThe amazing thing is that an admitted gadget freak could do clean UI work. Seriously though, not to knock any other programs, I've always thought Quanta's UI benefits have been in use more than appearance."
    author: "Eric Laffoon"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "> In fact, I use maybe 3 non-KDE apps regularly\n\nwhich 3?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "I bet it's Gimp, Evolution, Gnucash. ;-)\n"
    author: "Apollo Creed"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "i'm betting that he'll say gimp, xchat, and xmms.\n\ni can't shake off xmms personally because i've ALWAYS used winamp. when there's a good winamp-type media player for kde that doesn't use arts, i'll drop xmms like a piece of hot butter :^)"
    author: "n1n"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "xmms, xemacs and mozilla?\nThat's about what I'm using beside KDE. And it allways hurts:-(\nBut xemacs just has the most advanced editing methods, I'm used to xmms and Mozilla is just a fallback for pages not working with konqui"
    author: "Simon H\u00fcrlimann"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "VMWare, Mathematica, and LyX (which is a Qt app, but not a KDE one :)\n\n"
    author: "Rayiner Hashem"
  - subject: "KDE should not copy OSX or Windows Longhorn."
    date: 2003-06-22
    body: "\n\nThats the problem, KDE is too busy trying to copy, why not improve on, make KDE the BEST and most unique?\n\n\nKDE does need more polish but it also needs to be made modern, SVG Icons, SVG widgets, SVG should replace bitmaps, why do we need to use bitmaps and pixels when we can use SVG?\n\n\nI really hope KDE doesnt fall into the trap Gnome fell into, the one thing which seperates KDE from Gnome is polish, I dont use Gnome because while Gnome may have a more advanced core, it lacks polish.\n\nWhen I see all the KDE developers talking about how to improve the core of KDE and working on Apps that we dont really need, I see KDE falling into the same trap as Gnome.\n\nKDE needs more polish, while it beats Gnome it doesnt beat OSX or Longhorn.\nThe goal should be to innovate and not just copy or compare to OSX and Longhorn, but BEAT them both, KDE must be the BEST and EASIEST to use interface, with the most unique features.\n\nIts really that simple, anything less than this and Microsoft wont even be challenged by Linux."
    author: "HanzoSan"
  - subject: "Re: KDE should not copy OSX or Windows Longhorn."
    date: 2003-06-22
    body: "So, have you been using Longhorn for long then?"
    author: "Stephen Douglas"
  - subject: "Re: KDE should not copy OSX or Windows Longhorn."
    date: 2004-12-04
    body: "I kind of disagreee but still agree. KDE should atempt to copy longhorn, and then even improve on it. I'd prove that linux can to anything windows can and more (not that WE need to prove that) and it would go right in M$s Face!"
    author: "bob7"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "If you have ugly fonts it is probably because you are scaling bit mapped fonts.\n\nIf you will provide me with an e-mail address, I will send you a copy of the current draft of my HOWTO (some of which is now obsolete but still has some useful information).\n\nYou probably need to install more scalable fonts since XFree86 doesn't come with many.  I recommend the URW clones of the 35 standard PS fonts:\n\nhttp://www.gimp.org/urw-fonts.tar.gz\n\nand the MicroSoft TrueType fonts:\n\nhttp://sourceforge.net/project/showfiles.php?group_id=34153&release_id=105355\n\nAnd the fonts that come with Adobe Acrobat Reader 5 (get the Windows version -- and see my HOWTO).\n\nAnd learn how to setup XF86Config so that it won't scale bitmapped fonts.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-18
    body: "I bet its pan, OpenOffice.org and galeon/mozilla\n\nBTW: I remember that there is a Konqueror mozilla binding. What does this really mean? Can i use the Mozilla 1.4 RC2 enging instead of Konqueror's to display web pages in konqueror with?"
    author: "Alex"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "nah, its dia,, abiword 2, and sodipodi"
    author: "mario"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "it has to be sketch, guppi and Mrproject/Anjuta"
    author: "angel"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "I think all of the former KDE dia users (myself) went back to Kivio once it started coming with dia stencils..\n\nAbiword is very comparable to kword-- not sure why any KDE users would use it.\n\nIt has to be OO.org.. it's interface sucks ass, even Ximian's copy, but it's MS-doc compatability is nice. \n\nAs for sodipod vs. karbon14 vs. killustrator (yes, I'll use the real name), I'd have to go with none of them. Adobe Illustrator on wine is a much better solution. "
    author: "ANON"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "What are you talking about. OO.org 1.1's interface absolutely rocks, probably the ebst interface for any word processor with so many features. It is also quite customizable in almost everything, includign the abiltiy to have menus/context menus etc. only show options that can be used or just show all of them but make indication for the ones not able to be used.\n\nSeriously, 1.1 ROCKS! The best office suite for linux next to the SO 1.1 betas which have a few extra goodies. \n\nThey both have fantastic documentation, in their help files and if taht wasn't enough there are a lot of great and through books on using this office suite.\n\nttp://tinyurl.com/eqqf\nStarOffice 6 Companion 1056 pages\n\nhttp://tinyurl.com/eqqi\nSpecial Edition Using StarOffice 6.0 1176 pages\n\nand there are even clue sheets which can be handy:\n\nBasics: http://tinyurl.com/eqql\nWriter: http://tinyurl.com/eqqp\nDraw: http://tinyurl.com/eqqr\nImpress: http://tinyurl.com/eqqt\nCalc: http://tinyurl.com/eqqu\n\nand in addition to the great documentation available and familiar interface fo MS uers there is also great support for it in the community. All distros shi with it and some companies like Ximian have worked a little on improving it too by replacing all the icons with GNOME ones, and in general improving integration with GNOME.\n\nOO.org's problems are mostly in its stability, integration with OS, and launch speed (fixed with quickstarter) its also a little buggy as any program and may crash though not often. Its only missinga  few features which i want also, and that's saying a lot.\n\n"
    author: "Alex"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "> Seriously, 1.1 ROCKS! \n\nAre you __really__ being serious here? Wow! I found OOo 1.1b1's interface certainly better than 1.0 (which is in turn, was much better than StarOffice 5), but it simply doesn't compare to Microsoft Office, koffice, or gnome-office (Abiword, Gnumeric, ...)\n\nOOo's strength in comparison to koffice or gnome-office is it's very strong feature-set. This is where OOo really shines at. However, even if it a lot more features, OOo 1.1 is still a massive nightmare to use compared to MS Office, which has even more features. \n\nThis is why for right now, I use kword for general word processing, gnumeric for spreedsheet stuff, and OOo only for opening Word and Excel files and converting them to rtf. \n\nXimian's work on OOo is decent, but it still feels like an alien environment has landed on the earth and taken over your computer. I think MS office users will be more attracted to koffice or gnome-office. I tried to get a friend of mine to use OpenOffice 1.1 in windows a few weeks ago, because she reinstalled Windows and didn't have a copy of MS-Office anymore. She didn't like it much at all. I next installed a copy of Abiword, and she liked and picked it up pretty quick. Eventually, she needed more than a wordprocesser so she ended up downloading Office from kazaa. There goes the whole alternative software thing; many times the product with 97% market share often really does work better. "
    author: "h00dz"
  - subject: "Re: KDE-3.2 and A User Interface/System Overhaul"
    date: 2003-06-19
    body: "> BTW: I remember that there is a Konqueror mozilla binding. What does this really mean? Can i use the Mozilla 1.4 RC2 enging instead of Konqueror's to display web pages in konqueror with?\n\nIt means that Konqueror would use Gecko instead of khtml to render webpages.\n\nHowever, when it was current, very few people actually used it, so it's become unmaintained and doesn't work with anything past Moz 1.2 afaik. It was cool and all, but it always felt weird using a KDE app with gecko :)\n\nPerhaps if one day the Qt port of Mozilla (which gradually became unmaintained years ago once khtml started becoming better, and for a time, had eclipsed gecko until about Mozilla 0.8-0.9 or so) was revived and added back into the main development trunk. "
    author: "jo3"
  - subject: "Geckon in Konqueror"
    date: 2003-06-19
    body: "I think taht's cool, a great way to enjoy full comaptibility with the web while KHTML becomes just as good! I've not found a single webpage that would not render correctly with Mozilla 1.4 RC 2 and now it renders very fast too. \n\nI hope that this project will be maintained again. Though, I do have one big concern, will this mean Konqueorr will have the same start up time as Mozilla?"
    author: "angel"
  - subject: "Exactly, KDE needs POLISH"
    date: 2003-06-22
    body: "\n\n\n\nWe need better font rendering, we need KDE to switch to SVG rendered interface, SVG icons, etc.\n\nThe most important thing KDE needs right now is polish, I dont know if we need more apps.\n\n"
    author: "HanzoSan"
  - subject: "Re: Exactly, KDE needs POLISH"
    date: 2003-06-22
    body: ">  I dont know if we need more apps.\n \nFunny, two comments before you were sure that all apps are there."
    author: "Anonymous"
  - subject: "Higs, YEP ;)"
    date: 2003-06-17
    body: "I agree, KDE needs to adopt a HIG which every KDE application that will be included in the default KDE distribution needs to follow.\n\nPerhaps taking the best parts of these freely available and well known UI\nguidelines and creating one for KDE.\n\n\nhttp://developer.apple.com/techpubs/macosx/Essentials/AquaHIGuidelines/index.html\nhttp://developer.apple.com/ue/\nhttp://msdn.microsoft.com/library/default.asp?url=/nhp/default.asp?contentid=28000443\nhttp://developer.gnome.org/projects/gup/hig/1.0/\nhttp://developer.kde.org/documentation/design/ui/\nhttp://developer.kde.org/documentation/standards/kde/style/basics/ \n\nAt the very least combine the 2 KDE higs. I also think KDE needs to have a banenr or something on its website encouraging documentation writters =)"
    author: "Alex"
  - subject: "Why create a new HIG?"
    date: 2003-06-18
    body: "Why not use the GNOME HIG?  A good deal of the systems that have KDE apps installed also have GNOME apps, and it would be nice if they both had a consistent UI."
    author: "cparker"
  - subject: "Re: Why create a new HIG?"
    date: 2003-06-18
    body: ">  Why not use the GNOME HIG? \n\nBecause there is a good degree of disagreement about about some parts of the Gnome HIG. There are certain sections of it that will probably never be followed by KDE, and wouldn't generally be good for KDE apps either. \n\n>  and it would be nice if they both had a consistent UI.\n\nYes, of course. Whoever is interested enough in this can draft up a set of guidelines that combines common elements of the GNOME HIG and what KDE follows. There would then still be some differences, but consistancy would be improved.  \n"
    author: "lit"
  - subject: "Re: Why create a new HIG?"
    date: 2003-06-18
    body: "I reference lit's excellent post from the previous CVS Digest:\n\nhttp://dot.kde.org/1055539007/1055544932/1055545303/1055546879/1055593780/1055609915/1055637869/\n\nSorry to reply to your post and link to this lit, but I think your work deserves to be given  good deal of publicity. I want to see something come of it, or at the least, people to learn from it.\n\nI'm writting a small KDE app currently, but after I've finished the majority of the feature set that I want to do some major work with KDE's usability as I feel I have some talent and experience in the area. Hopefully some of my patches will be submitted * crosses fingers * I don't feel that KDE's usability is bad, but things can certainly be improved. It is important that new users can look at applications like Konqi and instantly understand how to use it and what most of the buttons/menus/widgets do. Afterall, experienced users already know how to use the application, it's only new users who have to learn the app, and if the interface is complex they might not ever turn into experienced users and then we have lost another newcomer back to Windows/MacOSX."
    author: "MxCl"
  - subject: "Re: Higs, YEP ;)"
    date: 2003-06-18
    body: "> I agree, KDE needs to adopt a HIG which every KDE application that will be included in the default KDE distribution needs to follow.\n \nOf course, before KDE were to adopt such a thing, it would have to be created first :)\n\nI'm sure a lot of people would be willing to help write such a beast (myself included),  but very few people would be willing to spearhead it. \n\n> I also think KDE needs to have a banenr or something on its website encouraging documentation writters =)\n\nI think what could be helpful is either the start of a user contributed-Wiki or user-annotated (like http://www.andamooka.org/ or php.org Docs)\n) documentation (even for developers) website. Then, anybody who would otherwise not contributed to writing docs, could add their comments and what not.\n\nAnother useful approach would be adding something like \"Rate the quality of this document\" with live results (like Microsoft's msdn). This would be helpful in letting documentation writers know what docs must be updated or be focused on. It's easier than reporting bugs. \n\nWith this two approroaches, comments and contributions from users could be merged in by doc writers to form the static docs that are shipped with KDE releases. A plug for the website from khelpcenter would of course be nice too :-)\n\nAnyways, it's just an idea. It takes someone to actually create it. The second approach could be extended to anything in KDE btw-- you could have \"Rate this dialog\" or whatever. /me imagines a \"AmIHotOrNot\"-type of thing :-)"
    author: "lit"
  - subject: "Re: Higs, YEP ;)"
    date: 2003-06-18
    body: "HIGS sounds like \"Hide inherent gnome shortcomings - behind a potemkin village of virtual actions (like inventing and praising rules don't need)\". In kde, the OO-based approach and the strong framework leads to good code reusuability and rapid prototyping. So KDE hasn't the problem that gnome has - the difference in the UI of programs resulting from the reinvention of existing code which can't be adopted. \nThat's the advantage of a good framework - it makes additional actions for getting  similar UI's redundant. So please, add your advices to the Gnome lists - THEY may need it. "
    author: "Ruediger Knoerig"
  - subject: "Re: Higs, YEP ;)"
    date: 2003-06-21
    body: "I agree that KDE is uniform. Uniformly messy that is. KDE is extremely cluttered. With my setup, the problems become acute: I'm using 1600x1200 on a 15\" LCD. I can immediatly spot clutter --- its when 16x16 icons are jammed so close together that I can't tell what they're for anymore. The sidebars in Kate and Konqueror are examples of these. Don't get me wrong, I'm a big fan of KDE, and as I've said before, its the only desktop environment on my 100% Linux machine. However, even I have to admit that GNOME is a whole lot more streamlined. Its not about making KDE less powerful or taking out options --- its about presenting all that power in a way in which the interface is clean and accessible at any given time. Until we get totally automated GUIs (which which the programmer only specifies content, and all layout is done by the toolkit) it will be impossible to get a clean UI without some sort of HIG. Heck, even if we had such an automated GUI interface, we'd still need an HIG, its just that it would automatically be enforced by the layout engine, rather than by the programmer."
    author: "Rayiner Hashem"
  - subject: "Speed improvements?"
    date: 2003-06-18
    body: "Will it be any faster, especially going backwards in Konqueror?\n\nDarren"
    author: "darren"
  - subject: "Re: Speed improvements?"
    date: 2003-06-18
    body: "...Or when moving or copying 5000 files..."
    author: "Murphy"
  - subject: "Re: Speed improvements?"
    date: 2003-06-18
    body: "... or will KDE improve the framerate in counter strike?"
    author: "AC"
  - subject: "Re: Speed improvements?"
    date: 2003-06-18
    body: "i installed cvs one, maybe 2 months ago. you wouldnt believe speed improvement..\nand the latest cvs-checkout is even faster!\nthx, kde-devel. you rock! "
    author: "Juergen"
  - subject: "Re: Speed improvements?"
    date: 2003-06-18
    body: "Maybe we can run kde very fast on a 486 in the future :D"
    author: "Sander"
  - subject: "Re: Speed improvements?"
    date: 2003-06-18
    body: "I was wondering about the following ...\n\nIf you had the cash to pay for -Accelerated-X Summit Series v2.2- from xig.com, would it make a difference to the perceived speed when using KDE?\n\nWould apps run/start faster, OpenGL or non OpenGL?\n\nWhat is the dot's opinion on this please.\n\nThank you."
    author: "AySee"
  - subject: "Re: Speed improvements?"
    date: 2003-06-18
    body: "If the driver of your GXF card is the problem, then it would be faster. But this is extremely unlikely...\n\nApps wouldnt run faster, but maybe a screen update needs only 1 ms instead of 2 ms. If you are lucky and your Xfree driver is extremely bad. I doubt that you can see the difference, unless you are using CAD apps or other things that are very demanding."
    author: "AC"
  - subject: "Flickerrr"
    date: 2003-06-18
    body: "hey\nI hope that one thing leaves with kde 3.2, the flickerr..\n\n- the selection line, u know. the line with wich you select desktop icons.. it flickers when moved.\n- text in kate/kwrite. when i type the whole text flickers..\n\nits annoying\nbut this are just details of course. i think kde is good for the rest\n\ni use kde 3.1.1\n"
    author: "Alien`"
  - subject: "Re: Flickerrr"
    date: 2003-06-18
    body: "> - text in kate/kwrite. when i type the whole text flickers..\n\nalready fixed in cvs\n\n>  the selection line, u know. the line with wich you select desktop icons.. it flickers when moved.\n\nwhat do you exactly mean? are you talking about how when you select a icon on the desktop and move it around or what?"
    author: "anon"
  - subject: "nasty flicker when resizing!! HATE IT!"
    date: 2003-06-18
    body: "I set KDE not to show the contents ofa  window when resizing or moving.\n\nThe thing is that the line which is shown instead flickers like mad, instead of being a nice thick black line it jsut changes colors all voer the place and flashes, is this fixed iN CVS?"
    author: "Alex"
  - subject: "Re: nasty flicker when resizing!! HATE IT!"
    date: 2003-06-19
    body: "> is this fixed iN CVS?\n\nNo. It sometimes happens that problems that are not reported at the proper place simply don't get fixed."
    author: "L.Lunak"
  - subject: "Re: Flickerrr"
    date: 2003-06-19
    body: "<i>what do you exactly mean? are you talking about how when you select a icon on the desktop and move it around or what?</i>\n\nGEM called it rubber band.. the rectangle that you use to select icons on the desktop.\n"
    author: "Tim Jansen"
  - subject: "Re: Flickerrr"
    date: 2003-06-19
    body: "The thing what tim sais is what i mean.  The line itself flickers. \nI know it isnt a disaster as long as you can select the icons. \nBut still... details count\n\nAnd the thing that alex is saying is also true here.nice to hear the text flickering is fixed:)\n\ni would rather see a small solid BlACk line to move windows non-opaque then a fake flickering translutant one.. same for the select line\n\n\n\n\n"
    author: "Alien`"
  - subject: "http://people.kde.org/"
    date: 2003-06-19
    body: "I saw the link to this in the article summary. Is this website ever going to be updated again? AFAIK, it said, \"gone for the summer\", back about two years in 2001, ago, and hasn't come back since. \n\nOh where oh where is tink with her interviews? I miss reading em every week on the dot =("
    author: "jo3"
  - subject: "UPDATE  NEWs FEED PROBLEM"
    date: 2003-06-19
    body: "It seems the .dot has a bit of a problem with its news feeds. The \"recnt software\" and the \"latest looks\" section are not so recent after all, they're a few days old. I hope the script hasn't been coded wrong."
    author: "angel"
  - subject: "GNOME VS KDE DEVELOPMENT SPEED"
    date: 2003-06-19
    body: "Well, I like the planned features for KDE 3.2, and I am definetely looking forward to it.\n\nBut, since a while ago I said I thought that GNOME development is progressing faster (http://dot.kde.org/1052493117/) and you all said that GNOME wasn't even clsoe to being as fast as KDE development, yet I just looked at the weekly summary for KDE and GNOME right here:\n\nKDE: http://bugs.kde.org/weekly-bug-summary.cgi\nGNOME: http://bugzilla.gnome.org/weekly-bug-summary.html\n\nAnd it seems that the GNOME developers have resolved 250 more bugs than KDE developers, so stop thinking your so far ahead and like I said before, look at the competition.\n\nAnd provide THE BEST tools making KDE a no brainer platform, umbrello, Qt Designer, Qt Assistant, Qt Linguist, Qt Script, Kommander, Kdevelop 3, DCOP etc. are just a few of the technologies that make KDE stand out and that's great, keep it up we need to convince people to develop for KDE instead of GNOME IMO. More powerful easy to use tools is what KDE needs IMO and by easy to use I mean tools that are actually documented. its been shown everywhere taht if something is simple and yet powerful with wide appeal it becomes very sucessful quickly, just like web design did.\n\nJust wanted to bring that to your attention."
    author: "Mario"
  - subject: "Re: GNOME VS KDE DEVELOPMENT SPEED"
    date: 2003-06-19
    body: "Mario, I don't understand your obsession with bug counts. That's NOT a good indicator of how much work is being done - for example, if Gnome2 is more recent and has more trivial bugs to fix, of course they'll be quicker. That doesn't mean KDE is stagnating or anything, just that hard bugs take longer.\n\nOf course it might mean something completely different, the point I'm making is that throwing meaningless statistics around and saying \"work harder!\" (to people who code as a hobby) is pointless "
    author: "Ed Moyse"
  - subject: "Re: GNOME VS KDE DEVELOPMENT SPEED"
    date: 2003-06-19
    body: "> And it seems that the GNOME developers have resolved 250 more bugs than KDE developers, so stop thinking your so far ahead \n\nStill trolling? You should learn to read statistics, they are not comparable: GNOME's statistic explicitly states that it includes enhancement requests while KDE's doesn't. Then look at the total number of bugs+wishes: KDE's count is lower. Look at the count of opened reports in the last week, GNOME's number is more than double so high. GNOME's report wizard is very bad at preventing duplicate report submitting, so no wonder they have to close so many reports. Look at the fluctuation: KDE's total count shrank during the last week, GNOME's grew. Let's not even consider the different products on both side (GNOME has no html rendering widget or Abisource but Gtk in Bugzilla, Evolution bugs are kept somewhere else). In short, not comparable.\n\n> And provide THE BEST tools making KDE a no brainer platform, Qt Designer, Qt Assistant, Qt Linguist, Qt Script\n\nAs their names say, those are not provided by KDE but Trolltech."
    author: "Anonymous"
  - subject: "Re: GNOME VS KDE DEVELOPMENT SPEED"
    date: 2003-06-19
    body: "Ed, don't pretend to quote me, I never said it meant either group worked harder in the original post. I just said that KDE's development isn't so much faster based entirely on the statistics which show that GNOME devs have resolved 250 more bugs than KDE devs in a week. I don't know if maybe half of those resolved bugs were duplicates, I doubt it but who knows. All I wanted to say was that there is no clear winner and we should stop saying oh GNOME is so far behind because they are ahead in some areas just as we are in some, there is no need for this arrogance. \n\n"
    author: "Mario"
  - subject: "Re: GNOME VS KDE DEVELOPMENT SPEED"
    date: 2003-06-19
    body: "> we should stop saying oh GNOME is so far behind\n\no.k. stopped. I'll never do it again\n\nWho's _we_? Jes' I've never said that Gnome is 'far behind' Well I doubt the developers walk around and have nothin to do but to tell everybody that Gnome is oh so far behind... In fact I get the impression that the KDE folks are aware of competition (in a positive manner...) And if somebody says that gnome may be behind in tis or that area, that's okay for a K-developer as you _have_ to be at least a bit proud of your work...\n"
    author: "Thomas"
  - subject: "KDE develop does seem to have slowed."
    date: 2003-06-22
    body: "Or at least innovation has slowed.\n\nSince KDE 3.0 I havent really seen any new features. Wheres the SVG Icons? What about adding some unique features to Konquerer to make it better than MOzilla? How about improving the existing Apps? I see tons of new Apps popping up and then theres a million unfinished Apps that seem to have been abandoned."
    author: "HanzoSan"
  - subject: "Re: KDE develop does seem to have slowed."
    date: 2003-06-22
    body: "> Since KDE 3.0 I havent really seen any new features. \n\nMaybe you should install KDE 3.1? Really, what was exciting new in KDE 3.0?"
    author: "Anonymous"
  - subject: "Re: KDE develop does seem to have slowed."
    date: 2003-06-23
    body: "Crystal SVG is already in KDE 3.1.\n\nWho gives a damn if it's SVG though?  It's still an icon."
    author: "anon"
  - subject: "Re: KDE develop does seem to have slowed."
    date: 2003-06-28
    body: "\n\nCrystal SVG is pixel based. Its not true SVG iconset.\n\nWho gives a damn? Its the only thing KDE is currently missing, polish.\nHows it going to compete with OSX and Longhorn if its got flicker and messed up icons."
    author: "HAnzoSan"
  - subject: "Re: KDE develop does seem to have slowed."
    date: 2003-06-28
    body: "No, crystal svg is a true svg iconset. It's prerendered into bitmaps before usage, just like in OSX. \n\nSVG has nothing to do with flicker. "
    author: "lit"
  - subject: "Re: GNOME VS KDE DEVELOPMENT SPEED"
    date: 2003-06-19
    body: "> And it seems that the GNOME developers have resolved 250 more bugs than KDE developers, so stop thinking your so far ahead and like I said before, look at the competition.\n\nWhy do you have to assume such a competeitive attitude between KDE and GNOME? KDE and GNOME developers, barring the first several years of history (after all, GNOME was created to destroy KDE, which it horribly failed at ;) ), have had a good relationship and have (mostly) coded their own desktop quietly and only taken brief to short looks at the other desktop. \n\nI mean, why would they? KDE was created to created a nice UNIX based desktop environment, not to compete with CDE exactly. GNOME, for the last 3 years, has been similar (pretty much since Qt went QPL and later QPL/GPL)\n\nPeople don't code for KDE because they hate GNOME. People don't code GNOME (at least for the last 3-4 years), because they hate KDE. They code because it's a hobby for them. Very few on other side are getting paid to do anything. This isn't back in 2000 when Corel employed twenty people with KDE cvs access, Eazel burned through 50 million dollars worth of startup money to create Nautilus, or non-\"(Redhat/SuSE) distros like Caldera/SCO, TurboLinux, Mandrakem, etc.. sponsering a whole host of KDE/GNOME developers. Usually the earlier in the case of these distros.\n\nAnd no, KDE/GNOME aren't even in competition with Windows. Perhaps the KDE/GNU/Linux platform is, though. But that's pushed by other people, and not the KDE developers."
    author: "anon"
  - subject: "Re: Gnome and KDE development speed"
    date: 2003-06-19
    body: "Mario, nobody is working harder than before just because you tell them to so so. The only way you can help out is by contributing your own time for supporting those who already work on KDE. You can do so by voting for specific reports which you would like to see being solved, confirming unconfirmed reports when you can reproduce them, looking for duplicate reports so only one report about a certain problem exist etc. At bugs.kde.org there's a tool near the bottom which allows you to search through very old reports to which you could help moving others attention to again (either for closing them as duplicate or invalid or for confirming and reviving them).\n\nPS: Please refrain from using caps, it looks really highly impolite."
    author: "Datschge"
  - subject: "Re: Gnome and KDE development speed"
    date: 2003-06-19
    body: "I'm not assuming such a competitive attitude, its jsut that I've heard reviews of KDE vs GNOME all the time and some very biased reviewers said there was nothing GNOME did better and that they are far behind and also some people here repeated the same junk while it is obvious GNOME has some virtues too or else nobody would have switched 80,000 school computers to linux with gnome: http://osnews.com/comment.php?news_id=3832\n\nI actually hope that KDE/GNOME developers work together as said in this interview: http://osnews.com/story.php?news_id=2997 I hope that the FreeDesktop.org, and all the standards like LSB will be very sucessful in acieving their goal and making linux consistent and giving companies the power to attack all of linux wiht a release, not just a few distros.\n\nThat's all, i want both projects to stop ripping on each other and to realize each others virtues and work together. I guess I thought this wasn't really happening just because of a few rude reviewers and users.\n\n\n\nhttp://osnews.com/comment.php?news_id=3832"
    author: "Mario"
  - subject: "Re: GNOME VS KDE DEVELOPMENT SPEED"
    date: 2003-06-29
    body: "STOP talking about!\nDO it yourself, if you can!\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "kicker"
    date: 2003-06-19
    body: "I hope the 3.2 release will finally get support for transparent kicker and more flexibility for choosing the look of the minipager applet (horizontal display for \"large\" and \"normal\" size of the kicker, etc.)."
    author: "ovidiu pascui"
  - subject: "SCREW THAT!"
    date: 2003-06-19
    body: "Translucency/Transparency, window shadows, menu shadows etc. are all a COMPLETE WASTE OF TIME FOR THE KDE DEVELOPERS it means that the result will automatically be slow, inconsistent and not quite feature complete. For example the current menu shadows only work in KDE applications, if I open GIMP the menus have no shadows which ruins the appearance of the desktop. Window shadows are not displayed while you move them and even so they are slow. Translucency again is slow and is not dynamically updated, just a hack. \n\nCreating these eyecandy features is counter productive because it it a hacked together mess and I hope KDE developers waste no more time on such things unless tehy want to implement such things the right way, into X so all desktops could use this stuff and so it would actually be quite fast and more feature complete.\n\nAnyway, there was a far better done hack that achieves what you want using OpenGL accelration for the desktop which is really cool: http://www.stud.uni-karlsruhe.de/~unk6/transluxent/"
    author: "Mario"
  - subject: "Re: SCREW THAT!"
    date: 2003-06-19
    body: "If you don't like pseudo-transleucency or window shadows, it's easy enough to turn them off in KControl."
    author: "LMCBoy"
  - subject: "Re: SCREW THAT!"
    date: 2003-06-20
    body: "Hey, I think somebody already said to you to stop using caps, it looks impolite..."
    author: "Ned Flanders"
  - subject: "Why do these hacks at all?"
    date: 2003-06-22
    body: "\n\nI say they should do it right or not do it at all.\n\nWork on making the interface SVG, at least the underlying support for that is ready and it can be done, alpha channel effects and shadows cant even be done in hardware."
    author: "HanzoSan"
  - subject: "Re: kicker"
    date: 2003-06-19
    body: "> I hope the 3.2 release will finally get support for transparent kicker\n\nAlready in cvs\n\n>   more flexibility for choosing the look of the minipager applet (horizontal display for \"large\" and \"normal\" size of the kicker, etc.).\n\nPerhaps this should be implemented into KDE-cvs? Someone pluged in kpager to kicker in 5 hours or so. http://www.kde-look.org/content/show.php?content=6702"
    author: "lit"
  - subject: "Re: kicker"
    date: 2003-06-19
    body: "The thing with the minipager is possible also for KDE 3.1.1 according to the screenshots of the new SuSE Linux Desktop which is based on KDE 3.1.1.\nhttp://www.suse.de/de/business/products/desktop/sld/images/konqueror_view.jpg"
    author: "ovidiu pascui"
  - subject: "Re: kicker"
    date: 2003-06-19
    body: "I don't really care I jus tthink that developers could spend time doing something which looks good and is not a hacked together pos like helping Xfree get real transparency. But, tis their time so they can do whatever they please, that's just my opinion."
    author: "Mario"
  - subject: "Shadows"
    date: 2003-06-19
    body: "Will KDE 3.2 include desktop shadows? (under the icon texts)\n(perhaps using the patch from kde-look?)\n\nI'd like to have this because those white texts are unreadable with some wallpapers and a shadow makes it more visible (and better looking).\n(and yes, it's probably possible to change that color somewhere, but I don't like to use other colors than white for texts under the icons on my desktop)"
    author: "nac"
  - subject: "Re: Shadows"
    date: 2003-06-19
    body: "I totally agree and there's a bug report for this: http://bugs.kde.org/show_bug.cgi?id=58944\n\n"
    author: "Mario"
  - subject: "and a patch"
    date: 2003-06-19
    body: "http://www.kde-look.org/content/show.php?content=4750\n\nUsability wise it is flawed. it should not list OS X or Windows as a type of shadow, taht make us sound like copy cats and besides it wouldn't help someone not familiar wth the way shadows in those other operating systems look, tehre needs to be a proper description. I won't get into more details, because this si obviously ot finished."
    author: "Mario"
  - subject: "Re: and a patch"
    date: 2003-06-22
    body: "it lists OSX or Windows as a type?\n\noh geez.\n\nwheres the innovation?!"
    author: "HanzoSan"
  - subject: "Re: Shadows"
    date: 2003-06-19
    body: "That's the wrong one for this, you search 33107."
    author: "Anonymous"
  - subject: "Fake shadows dont count."
    date: 2003-06-22
    body: "\n\nKDE should focus on doing REAL shadows that are done via Hardware or not do them at all.\n\nI thinn its more important that we get SVG Widgets and Icons."
    author: "HanzoSan"
  - subject: "Re: Fake shadows dont count."
    date: 2003-06-22
    body: "Kongratulations, you showed that you didn't understand about what \"shadow\" the parent comment talked.\nAnd KDE doesn't develop XFree86 or other X servers."
    author: "Anonymous"
  - subject: "Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "Vote for this wish if you want to see this in next KMail version. Citing Don Sanders: \"If you can get a thousand votes on 4202 from kde.org addresses before KDE 3.2 feature freeze I'll put my time where my mouth is implement it and send a patch to the KMail list. Otherwise I think people are just whining and they don't really care that much.\" (http://bugs.kde.org/show_bug.cgi?id=4202#c23)"
    author: "Anonymous"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "I can't imagine a feature I'd want less."
    author: "MxCl"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "As others have said: No!\n\nI'm sorry but I've yet to read a convincing reason why html emails are neccesary."
    author: "Joergen Ramskov"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "For one thing, they are commonly used outside of your GNU/Linux world. My (13 year old) daughter commonly uses html mail to write formatted text to her friends on Windows. She likes writing with colors and bold/italics and attaching inline pictures. And guess what, all of her friends do that too. \n\nI swear it's not my fault for introducing her to html mail =) : her mom (my x-wife), uses AOL at home.\n\nOutside of the technical geeky world or buisness worlds, html composing is a must have for any email client. Just because you find it annoying receiving html emails (I do too), doesn't mean other people don't have viable reasons for using it. "
    author: "lit"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "I can't understand why html mail composing is a must have, as you say neither geeks nor buisniss needs it, but it's a nice thing for the kids. This does not make a must have in my book :-) Besides as I understand KMail show recived html mails quite well since 2.x, not sure cuz the only html mail I get are spam, and thats what filters are for :-)\n"
    author: "Morty"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-20
    body: "> I can't understand why html mail composing is a must have\n\nJust looking to answer the question here.  Been using regular old KMail for over a year now sending out all plain text messages.  A couple of things worth noting.\n\nWhen putting together some business letters, being able to utilize tables can be extremely helpful.  There's just no telling if the recipient is using monospaced fonts or not.  In fact, it's probably more common to have HTML capability by default than monospaced fonts.  Sad but true.\n\nWhen writing tutorials an author may wish to emphasize certain points with bold or italics.  Inline screen shots of configuration dialogs can also be helpful.  Especially true when sending to folks who normally don't ever look at configurations.\n\nI'll absolutely grant the point that 99% of the HTML mail out there is fluff.  Comic Sans default anyone?  Furthermore, I'm not sitting here arguing for HTML composition in KMail as a \"must have\".  The point here is that there are valid reasons why someone would want this ability.\n\nPersonally I love saying to folks, \"You just gotta wonder how Twain and Hemmingway managed to get along with only one font?\"  The point of course being that the important thing is what is said, not how it's presented.  The counter point being that sometimes presentation is important.  If it weren't, we'd all still be using CLI apps and magazines wouldn't include pictures.  We're a bunch of visual critters, and presentation does add to what is being communicated.\n\nWhat I do hope to see in the not so distant future is some decent way of composing an HTML document via a word processor style interface.  Along similar lines to what Mozilla's composer does.  In my dream world of apps (peacefully resting in my wee brain) I would picture two such applications.  One being KWord working in solid HTML support for regular user types just looking to get stuff posted to an Intranet.  The other being a Quanta style approach for those folks actually doing web development work.  For some reason these two distinctly different audiences get treated like one group, which they are most certainly not."
    author: "Metrol"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-20
    body: "[No copy option.... Well, I must rewrite by hand.) \n\"What I do hope to see in the not so distant future is some decent way of composing an HTML document via a word processor style interface. In my dream world of apps I would picture two such applications. One being KWord working in solid HTML support for regular user... The other being a Quanta style approach for those actually doing web development work.\" \n[I shortened.]\n\nIt should already be realised.\n\nBy word modelling the possibility of personal expressions has enhanced a lot.\nBut it requires a taste and inteligence."
    author: "Alojz Stanich, Slovenia"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2007-08-09
    body: "I use HTML in my personal and business email(s). I was using Thunderbird and decided to try kmail, to me, that was a mistake because of the lack of HTML sending. I hear the arguments that it is not required for email, and people don't need it. I disagree completely, plain text was the 80(s) even for the console(s), now  the desktop has to be a GUI or it will NOT be used. For me, email must be HTML capable or I will NOT USE IT!"
    author: "Robert Coulter"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "Oh yes, I really hate html emails. Btw.: Why don't exist Anti-Votes?"
    author: "Carlo"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "Because bugzilla doesn't support anti-votes.\n(See http://bugzilla.mozilla.org/show_bug.cgi?id=48570)\n\nIf you really want this feature, you can get an account and\nvote for it. :-)\n\nChristian"
    author: "cloose"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "Thx, did it. :-)"
    author: "Carlo"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-19
    body: "Hello friends,\n\nAren't there security vulnerabilities to implementing HTML in mail apps. I'd certainly turn this feature off. It's absolutely useless.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-06-20
    body: "No thanks.  HTML has nothing to do in email.  \n"
    author: "arcade"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-08-28
    body: "I don0't know if i have time or just out of it, but in a fight for the freedom in writing.. please, support HTML in Kmail. (natively or explain a way to do it via external editor). thanks"
    author: "Jordi A. Cam\u00fas"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2003-11-21
    body: "Tables, tables, and more tables please....."
    author: "Mike"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2004-02-10
    body: "Narrow minds. Why not just use plain text on your web pages also?"
    author: "Living in the present"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2004-04-28
    body: "There is no reason lacking one option deliberately in  a great email client , \nthen this option is in almost everyother client , \nI would vote yes to have html composer \n\n"
    author: "aT"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2004-08-10
    body: "Well I used to lean towards 'no need to write HTML mails'...\n\nRecently I had to exchange some info/thoughts with guys using M$ tools. They simply could not understand the text with picture attachment at the bottom (actually if a mail contained more then one picture, they had problems matching them ;-). I simply did not belive that a mature product like KMail does not have a text/HTML configuration somewhere...\n\nBottomline: my vote for adding this feature."
    author: "mariusg"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2004-08-14
    body: "What about writing HTML for Kmail anyway?  At least building the functions will fix this list from growing and you can be done being harassed about it.  I think it's a great idea.. not for HTML in Kmail, but freedom for you, in the long run."
    author: "Jordan Peterson"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2007-05-20
    body: "Ok, so I'm three years late, but amen, AMEN, and AMEN!!!!"
    author: "phenomshel"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2004-09-24
    body: "I want to paste a table from a spreadsheet straight into an email.\nI want to compose in HTML\n\nI want to include an HTML link in my signature.\nI want to compose in HTML\n\n"
    author: "Vince Webb"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2004-09-30
    body: "What is really necessary is not a full-featured HTML editor, but it would be nice to be able to include some basic formatting such as hyperlinks or italic text. Of course some people overuse HTML but most people only want to do simple formatting.\n"
    author: "Antoine Aubry"
  - subject: "Re: Bug 4202: Support for writing HTML in KMail"
    date: 2007-05-20
    body: "Yes, a full HTML editor *is* necessary in Kmail. It's the ONE reason I have Windows XP still in VMWare.  I use Incredimail and/or Outlook. It may seem like useless eyecandy to YOU, but to thousands of everyday users, it *is* a necessity and we flat refuse to use something that does not have this capability. Please, I cannot stress this enough, add full HTML capabilities (both reading and composing) to Kontact/Kmail in the near future!! \nMost Linux users are not idiots, let us choose our own security risks!!"
    author: "phenomshel"
  - subject: "mmmm, nope."
    date: 2003-06-19
    body: "Sorry, i think plain text messages are far more useful.\nYeah HTML looks well, but.. when i write a message, i care in content, not in look of the message. It's my way of understanding e-mail .... "
    author: "ALG"
  - subject: "Re: mmmm, nope."
    date: 2003-06-20
    body: "I agree with you"
    author: "Giovanni"
  - subject: "KDE development team, please innovate."
    date: 2003-06-22
    body: "\n\nWhile KDE is nice, http://slicker.sourceforge.net/ is the only innovation I have seen in terms of making KDE easier to use.\n\nKDE is great, its nice, but it lacks polish. How about one of these developers steps up and tells me and the world what the plan is?\n\nWhen will we get SVG Icons, Widgets, Alpha Channel, etc?\n\nI know, the backend is still being worked on, but this should be the goal, along with innovating. Maybe setup a site where the community can submit ideas or something, this could help with innovation.\n\n"
    author: "HanzoSan"
  - subject: "Re: KDE development team, please innovate."
    date: 2003-06-22
    body: "> Maybe setup a site where the community can submit ideas or something\n\nAlready exists, http://bugs.kde.org - how about a Troll newbie FAQ?"
    author: "Anonymous"
  - subject: "Enhancements in konqueror"
    date: 2003-06-28
    body: "I think right now konqueror is great over its previous version, very good java script support  and all that stuff, and even then it is light in terms of memory(than mozilla). But still there are some features required as:\n1) Support for flash with konqueror /KDE distribution itself, it will benefit it as a file system explorer/viewer also. I whole lot sites keep flash but konqueror can't show them and downloading and finding appropriate plugin, if there is any, for koqueror flash is a pain in neck. It can be supported right from start , just as in case of java script and all that.\n2) Many a times while i am surfing i want to DOWNLOAD a tgz file, but it just opens them in konqueror. That's good, but we should have an option to download/saveas it also in the right mouse click. Shift click does not work always.\n3)*if i am surfing say, yahoo.com with only one tab in konqueror and i want to open a new tab in which i want to surf google.com , then i cant do it unless i right click on a link in yahoo.com for opening it in a new tab. And when that url of yahoo.com opens , then i type google.com.\n  Only when i have two tabs in konqueror now , i can right click on a tab and get the option of opeing a \"new blank tab\"\n   What i want to say, that there is no option in menu bar for opening a new location in a new tab ( not in the current tab) and neither we have any shortcut buttons for it\n4)**** MOST MOST IMPORTANT *****: somehow integration of samba protocol understanding in konqueror itself. so that we can see Windows Network Neighbourhood in konqueror (just like in MS-Windows explorer) itself without using say LinNeighbourhood and all that stuff.\nCheers :-)\nAshish"
    author: "Ashish"
  - subject: "KDE 3.2 Timing is Excellent!"
    date: 2003-08-10
    body: "MS Windows NT is no longer supported as of Jan 1, 2004. If KDE 3.2 meets its schedule we should see a large number of migrators from NT4 to Linux. Kroupware will also help this migration since many organizations will be forced to either upgrade and learn Windows 2003/Exchange Server 2003. The cost will be high as will the learning curve. With KDE, the learning curve is no higher but the price can't be beat!\n\nI would like to thank the KDE team, I love the look of KDE and do not have the problems others in this list have. I am looking forward to the next version though.\nFor all wondering about the Windows desktop users after migration of exchange, there are bynari connectors and/or cygwin.\n\nThanks again KDE team!\n"
    author: "Alex Chejlyk"
---
<a href="http://people.kde.org/stephan.html">Stephan Kulow</a> (KDE 3.2 release coordinator) and <a href="http://people.kde.org/ralf.html">Ralf Nolden</a> <a href="http://lists.kde.org/?l=kde-core-devel&m=105559467212771&w=2">announced</a> a preliminary <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">KDE 3.2 release schedule</a> and asked everyone to check with the personal schedule. Every planned feature should be entered into the <a href="http://developer.kde.org/development-versions/kde-3.2-features.html">KDE 3.2 feature plan</a> before September 1st, 2003 and has to be implemented before September 29th, 2003. An Alpha 1 release is expected for release after the <a href="http://events.kde.org/info/kastle/">KDE developer conference 2003 </a>in Nove Hrady on September 1st, 2003. Provided that only one Beta release on October 6th, 2003 is necessary, the final KDE 3.2 release is planned on December 8th, 2003.
<!--break-->
