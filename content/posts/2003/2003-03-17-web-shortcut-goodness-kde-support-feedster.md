---
title: "Web Shortcut Goodness: KDE Support for Feedster"
date:    2003-03-17
authors:
  - "rkaper"
slug:    web-shortcut-goodness-kde-support-feedster
comments:
  - subject: "On the subject of RSS!"
    date: 2003-03-17
    body: "Greetings<Br>\n\tIf you are also looking for a ton of RSS feeds that work great with KNT check out <a href=\"http://www.syndic8.com\">http://www.syndic8.com</a> (Ed: fixed link).  They have quite a few very nice ones there.\n<p>\n\tFor those of you who cannot get enough KNT  we are working on a sidebar plugin (<a href=\"http://kde.geiseri.com/sidebarnews.png\">sidebarnews.png</a>) to keep your fix in check ;)\n<p>\nCheers<br>\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "Bah!\n\tThat should be http://www.syndic8.com\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "And if you wait a week or so, you can all get KRsN ;-)\n<p>\n<a href=\"http://www.pycs.net/lateral/pictures/640x480/4.html\">KRsN shots</a>"
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "Hmm, looks interesting. Is there a source tarball, or a more details feature list available somewhere? Is this an applet, or a Konqueror plugin, or a standalone application?"
    author: "Frerich Raabe"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "Standalone application right now.\nMaybe later I will do some addons (I got a suggestion for a screensaver, for example)\n\nIt's written in python, though, so it is dubious it will get much of a market :-P\n\nIf you want it, mail me at ralsina at kde org and I can send it to you tomorrow."
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "I was thinking about a screensaver for dcoprss myself.  Now that we have a central rss service in KDE it should be quite trivial...  \n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "dcoprss?"
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "Frerich, Marcus and I are makeing a new DCOP service that will provide rss feeds to KDE applications from a central location.  This will include sidebar plugins, and knt to start with.\n\nCurrently we support RSS but we are expanding the service to handle management to maintain feeds.  It is in kdenonbeta and will hopefully be in KDE 3.2 as the engine for the sidebar and KNT.  Even your app is possible with DCOP support ;)\n\nCheers\t\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "Heh, seems Python and PyQt is really nice for RSS stuff. I did this  little thing\n<a href=\"http://www.infa.abo.fi/~chakie/media/pyrss.png\">pyrss.png</a> as a test in a few hours using a RSS library I found somewhere. Works, but has no features other than it can view the articles and open links in Konqueror.  Roberto's browser looks a lot more sexy and makes me want to use it. :)"
    author: "Chakie"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "Python for some reason has attracted a lot of RSS/blog related development.\n<p>\nFor example:\n<p>\n<a href=\"http://diveintomark.org/projects/rss_parser/\">rss_parser</a><br>\n<a href=\"http://diveintomark.org/projects/misc/rssfinder.py.txt\">rssfinder.py.txt</a><br>\n<a href=\"http://pyds.muensterland.org/\">pyds</a><br>\n<a href=\"http://pycs.net\">pycs</a>\n<p>\nIt is mostly just a matter of tying things together and plastering a GUI on it ;-)"
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "Yeah im basicly trying to do the same thing in C++ currently and its a nightmare.\n\nWe have the RSS Service working now quite well, but manageing feeds is going to be a trial.\n\nThe python code to grab rss headings via xmlprc is so trivial it makes me cry.\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "Then why not use the nice Python KDE/Qt bindings? I even uploaded a working KDE 3.1 python binding yesterday!\n\nIt's not like the app is any slower or uses much more resources for being written in python :-)"
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "Because we cannot have kdenetwork depend on something not in kdecvs...\nIt would be cute, but again, it seems the bindings are randomly maiintained and not in our CVS. \n\nIt might be cool though... btw are you the maintainer of the week for PyKDE?  Its cool to see it maintained again :)\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "Erm... I didn't know it was unmaintained, I simply made it work because I wanted it :-)\nThe authors still work on PyQt, so it would only be a matter of tracking the KDE API, which is supposed to be stable for a long while .... right? ;-)\n\nIf yes, maybe I could do it."
    author: "Roberto Alsina"
  - subject: "PyKDE"
    date: 2003-03-18
    body: "It really *is* maintained, see for example here: http://mats.gmd.de/pipermail/pykde/2003-March/004780.html"
    author: "Daniel Naber"
  - subject: "Re: PyKDE"
    date: 2003-03-18
    body: "Yeah these are the pyqt mailing lists, im on them... \nIts just if you read them you know that the KDE bindings are randomly maintained as people have time.  Ive been waiting for us to roll pyqt/kde into kdebindings so we can have it up to date.\n\nIve been playing with PyKDE and it seems fast...  I was impressed with some of the examples, but im not sure if its worth baseing something in core kde off of.\n\nThere is also the issue of SIP, but i will leave that to be debated by those of us who have suffered its wrath ;)\ndunno.\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: PyKDE"
    date: 2003-03-19
    body: "Just what _is_ holding PyKDE back from being included in kdebindings? I would love to use it, but since its releases always seem to lag by as much as six months or so, it isn't as practical. Correct me if I am wrong, but wouldn't its inclusion in kdebindings mean that it would be syncronized with the latest KDE (and API changes)?\n"
    author: "Rodion"
  - subject: "Re: PyKDE"
    date: 2003-03-19
    body: "Inclusion in KDE CVS is not magic.\nWhat would be needed is someone to keep it working.\n\nNow, had I known this was so desired, I could have done the minimum maintenance (ie: not tracking the KDE API, but making sure it doesn't devolve) anytime.\n\nNOw, do I feel confident enough to put it in CVS? No, because I am not the developer, I only did a little (very simple) maintenance. Let's wait for the real 3.1 version and check again :-)\n\nBut really, I got mail from about 20 people thanking me about the hack. If there are 40 people using it, and each kept track of a piece of KDE, it could be maintained asily. Of course that is not how free software works ;-)"
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "Yes, KDE needs to be able to easily distribute apps based on PyQt and PyKDE. Combine Python with Qt and the Qt Designer and you have a very powerful too for creating applications very fast. I've done a lot of PyQt apps for my own use, but I don't even consider distributing them as I don't want to end up answering a lot of questions like \"how do I install PyQt\". PyQt needs better support by distros, and KDE should take non C++ apps more seriously and allow them to be integrated into KDE proper. Sure, if that means integrating PyKDE and PyQt I think it's worth it. Gnome makes it easy to make non C apps...\n\nHmm, sorry, I sound like I'm just whining. Didn't mean it to sound that way. I'm happy with PyQt for my own use, but I'd like it to be easier to let others use the apps too."
    author: "Chakie"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "Since PyKDE for KDE 3.1 is a couple of weeks away (hopefully :-) and the KDE's API is going to be backwards compatible to KDE 3.1 for a long time, I expect this to be more and more feasible with time.\n\nAlso, the wrapping process is becoming more automated becaus of a new tool, which should makethe delays between releases shorter."
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "Color me jadded but this has been the song and dance for 2 years now ;)\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-18
    body: "Ok, here is the URL for a working KDE 3.1 binding, and I promise to fix any bugs or omissions reported about it, as far as I understand them ;-)\n\nftp://ftp.kde.org/pub/kde/unstable/apps/KDE3.x/utils\n\nNow, hope you unjade a bit ;-)"
    author: "Roberto Alsina"
  - subject: "PyKDE"
    date: 2003-03-18
    body: "cool!\nso is there any chance that pyKDE will be part of KDE anytime soon? It's really rather unusable as long as pyKDe for any given KDE-version only appear 1-2 month before the next version of KDE ships (ok, timing seems to be much better now) "
    author: "Johannes Wilm"
  - subject: "Re: PyKDE"
    date: 2003-03-19
    body: "Well, I can make those delays shorter by doing what I did this time, as long as KDE's API doesn't go through some radical change. But the question is better asked to the real developers of PyKDE, insrtead of on the dot :-)"
    author: "Roberto Alsina"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "The standalone for Linux RSS reader that has attracted the most attention so far (Straw) uses Python. It's quite a functional program. I'm glad there is finally a KDE-alternative."
    author: "Wrenkin"
  - subject: "Re: On the subject of RSS!"
    date: 2003-03-17
    body: "In fact, straw seems to use the same RSS parser (the liberal rss parser) and rss discovery tool (rssfinder).\n\nAs for a KDE alternative... well, wait a week, and, if everything goes right, there will be one ;-)"
    author: "Roberto Alsina"
  - subject: "Hehe"
    date: 2003-03-17
    body: "Another well hidden goody in KDE, enjoy. ;)"
    author: "Datschge"
  - subject: "Tired of colons in shortcuts?"
    date: 2003-03-17
    body: "I'm told that if you don't like colons in your shortcuts and prefer a space like that in Mozilla and Opera, it's now possible in KDE 3.1.1\n\n[[[\nThis is now configurable in the upcoming 3.1.1 release. \u00a0You can change the keyword delimiter to a space by adding KeywordDelimiter=32 to the [General] section of $KDEHOME/share/config/kuriikwsfilterrc file.  The default is still a ':' for compatiblity reasons. \n]]]\n\nI haven't confirmed this yet though."
    author: "Joseph Reagle"
  - subject: "This usefull too?"
    date: 2003-03-17
    body: "Maybe this is usefull too? I made an IMDB search. Download the desktop at\nhttp://home.student.utwente.nl/a.t.somers/imdb.desktop"
    author: "Andr\u00e9 Somers"
  - subject: "Forbidden"
    date: 2003-03-17
    body: "Sounds really cool, but I get:\n\n\"You don't have permission to access /a.t.somers/imdb.desktop on this server.\"\n"
    author: "LMCBoy"
  - subject: "Re: Forbidden"
    date: 2003-03-17
    body: "Sorry! Fixed now.\n\nAndr\u00e9"
    author: "Andr\u00e9 Somers"
  - subject: "Re: This usefull too?"
    date: 2003-03-17
    body: "its in CVS allready\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: This usefull too?"
    date: 2003-03-17
    body: ":-) It's a pretty obvious addition, so I'm not supprised at all..."
    author: "Andr\u00e9 Somers"
---
You already have <a href="http://www.student.uni-oldenburg.de/frerich.raabe/knewsticker/">KNewsTicker</a> and you often hit the popular sites for the latest news.  But have you tried <a href="http://www.feedster.com/">Feedster</a>, the RSS search engine? Now,  the latest KDE CVS includes <a href="http://unixcode.org/kde-eye-candy/">support for Feedster</a> in the elegant form of a web shortcut.  If you're not familiar with this KDE feature, be sure to read up on <a href=" http://docs.kde.org/en/3.1/kdebase/konqueror/enhanced-browsing.html">Enhanced Browsing</a> (<a href="http://docsrv.caldera.com:8457/en/KDEdoc/kcontrol/ebrowse.html">details</a>). It is quite handy, especially in combination with KDE's Run Command (Alt-F2).  And if you cannot wait until KDE 3.2, simply <a href="http://unixcode.org/downloads/kde/feedster.desktop">download</a> and <a href="http://unixcode.org/kde-eye-candy/">install</a> this file and start using <tt>rss:</tt>, <tt>blog:</tt> and <tt>feedster:</tt> right away!
<!--break-->
<p>
Fortunately, adding support for Feedster (and <a href="http://news.google.com/">Google News</a>) to KDE proved to be quite trivial. The man behind Feedster was so amazed, he put up <a href="http://www.feedster.com/blog/2003/03/13.html">a blog entry</a> full of praise for KDE!  If you're new to Enhanced Browsing in <a href="http://www.konqueror.org/">Konqueror</a>, give it a try. Rest assured, you'll be posting happy comments about <tt>gg:</tt>, <tt>php:</tt> and the other shortcuts in no time.
