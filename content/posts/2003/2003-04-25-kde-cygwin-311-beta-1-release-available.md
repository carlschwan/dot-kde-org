---
title: "KDE on Cygwin: 3.1.1 Beta 1 Release Available"
date:    2003-04-25
authors:
  - "binner"
slug:    kde-cygwin-311-beta-1-release-available
comments:
  - subject: "wow"
    date: 2003-04-24
    body: "Wow, thanks for the great screenshots, Steve!  It's quite scary how Konqueror looks like any Windows app..."
    author: "Navindra Umanee"
  - subject: "Re: wow"
    date: 2003-04-25
    body: "With another icon set and a more darker color scheme they would look more alike."
    author: "Anonymous"
  - subject: "I thought"
    date: 2003-04-24
    body: "Qt was already multiplatform and the same toolkit would work on OS X, Windows, and Linux, sow hy is there a need to port it?"
    author: "Alex"
  - subject: "Re: I thought"
    date: 2003-04-24
    body: "Because this uses Qt-x11 on cygwin.  So you don't need a Trolltech license to use it.\nThere is a group porting KDE libs to native win32 though, and from what I hear they are pretty far.  The advantage here is that the apps look and behave like real windows apps.\n\nThe goal not being to do silly things like run konqi on win32 but to allow companies to write native win32 apps that can easily be run on Unix and MacOS X.   Really you need quite a beefy machine to make the cygwin stuff load, but it still looks very much out of place.\n\nShort synopsys is, neat for about 20 minutes until you need to get back to work.  Its not fun to wait 45seconds to wait for konqi to appear and show my home directory :P  Maby the native port will be faster?  Maby I should not test on an AMD k6 333 ;)\n\nJust my own 2c  \n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: I thought"
    date: 2003-04-24
    body: "Do you have a link to this project?  I didn't see it in the first few google results I tried."
    author: "Tom Matelich"
  - subject: "Re: I thought"
    date: 2003-04-24
    body: "Same site: http://kde-cygwin.sourceforge.net/qt2-win32/"
    author: "Anonymous"
  - subject: "Re: I thought"
    date: 2003-04-24
    body: "oh, I am a Qt-Windows licensee.  I was hoping for native windows porting of KDE libraries.  Much of KDE is portable, but a lot of it uses unix-y stuff."
    author: "Tom Matelich"
  - subject: "Re: I thought"
    date: 2003-04-24
    body: "http://www.iidea.pl/~js/qkw/"
    author: "Peter Simonsson"
  - subject: "Re: I thought"
    date: 2003-04-24
    body: "Thanks!"
    author: "Tom Matelich"
  - subject: "Re: I thought"
    date: 2003-04-25
    body: "Without a GPL'ed (or compatible) Qt on Windows this isn't possible to distribute.  i.e. You can't distribute binaries of kdebase linked to Qt for Windows for licensing reasons."
    author: "Scott Wheeler"
  - subject: "Re: I thought"
    date: 2003-04-25
    body: "Why not?  Is it also illegal to ship a binary of GNU Electric on win32? (links to ms libs) Is it also illegal to ship cygwin (yes virginia this too links to ms libraries)?  Is this just a religious thing so we don't get RMS after us again? On that rationale it would be illegal to ship any GPL app on windows.  There are actually quite a few open source projects that ship linked to Qt commercial on win32.  Should we be \"Cracking Down\"?\nhttp://psi.affinix.com/?page=Download (ships with a qt-mt305.dll)\nhttp://sourceforge.net/projects/qtella (uses qtnc.dll so prolly okay)\n\nThen again this is all a moot point, since this is more so geared for closed source companies to write software on win32 and then have it available on Unix.  We are basicly in the same boat on MacOS X. Has anyone seriously tried Konqi as a web browser on cygwin?  I did a side by side and I would recommend not drinking coffee while using it, if you know that I mean.  KDevelop was nice, but slow as dirt.  I did not try KMail, yet, but im going to go out on a limb and assume it wont be a speed demon either.  From what I was told by the cygwin guys over the years, \"If you make a POSIX call, expect it to be slow.  If you call fork(), expect it to be REALLY slow.\"    Granted this may change with mingwin, but Im not sure if that is ready enough for prime time.\n\nJust my 2c from the few who have tried this ;)\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: I thought"
    date: 2003-05-12
    body: "While preparing the KDE3 release I've done some test on recent pc hardware with enough ram (512MB) and recognized, that many kde applications are quit usable. There are of course things to optimize, like the loading time and some improvements in the basic libraries like cygwin. \n\nAfter releasing KDE 3 I have got feedback, that KDE3 runs much faster than KDE2 with same hardware, os and cygwin release, which shows me, that there were some efforts to speedup KDE internal. \n\nKdevelop was explicity released as alpha state, because we expects performance issues. \n\nKMail is really usefull. I and other have used is on a daily basis. \n\nRegards \n\nRalf \n"
    author: "Ralf Habacker"
  - subject: "New Koffice"
    date: 2003-04-24
    body: "Hey All\n\nHow about posting a story on the newest koffice beta ?\nhttp://www.koffice.org/announcements/announce-1.3-beta1.phtml\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: New Koffice"
    date: 2003-04-24
    body: "Yes. I'm missing that, too! Where should we post our opinions on KOffice???\nPlease have mercy with us and create an article. ;-)\nBTW, I've installed it and I really like it. It still crashes and has missing\nfunctions but it is the first beta so that's perfectly OK.\nEspecially the word import filter is pretty cool. I often get doc files via email\nand it's much faster to run kword than OpenOffice.\nKarbon14 has improved A LOT! The changelog really doesn't do it justice.\nThe text function doesn't work at all for me right now, but it's nice to\nget a glimpse of what will be contained in the final version.\nKexi does not work properly either. I cannot create fields in a table. \nSo there's lots of work to do, anybody who hasn't installed it yet: Do not expect\ntoo much. It's definitvely a very early beta but nevertheless an amazing product.\n\n"
    author: "Michael"
  - subject: "Re: New Koffice"
    date: 2003-04-25
    body: "> Kexi does not work properly either. I cannot create fields in a table. \nWorks for me with an mySQL database, and it also works with an embedded one, but you need to use pure SQL (and maybe a more recent version:-)\n"
    author: "Norbert"
  - subject: "Re: New Koffice"
    date: 2003-04-25
    body: ">Works for me with an mySQL database, and it also works with an embedded one, >but you need to use pure SQL (and maybe a more recent version:-)\n\nErr....I have the same problem here. It's a CVS from 10 days ago (it's quite tough to rebuild koffice every week). I don't think it's an sql problem. It's just the interface that doesn't work. I find no way to add a new field in a table. Unless one needs to press the 6th button in the mouse to open a hidden menu ;-)\n\nWhat do you mean that one needs to use pure SQL? I have mysql here, and works with every program I made using qsql...\n "
    author: "uga"
  - subject: "Re: New Koffice"
    date: 2003-04-24
    body: "Nobody has submitted an article.  I didn't even know it was released!    Why do you post here instead of contributing an article?  I don't have time to write one right now unfortunately... so someone please submit a good write-up."
    author: "Navindra Umanee"
  - subject: "IT was on the main kde.org site for a while"
    date: 2003-04-25
    body: "As well as on the Koffice site and its even on the dot now."
    author: "Alex"
  - subject: "Re: New Koffice"
    date: 2003-04-25
    body: "Oh. I'm sorry. I didn't know we could contribute articles and whom I should\ninform. I'm just following these news articles directly from kde.org.\nAnd to make things clear: I did not expect a complete article, I just thought\na short note: \"Koffice 1.3 released\" would have been nice, because on kde.org\nthere was a discrepancy between the release section \"Koffice 1.3 released\" and\nthe most recent news article (this one) so I posted on this one. I did not want\nto offend someone. ;-)\n"
    author: "Michael"
  - subject: "Re: New Koffice"
    date: 2003-04-26
    body: "> I didn't know we could contribute articles and whom I should inform.\n\nOn dot.kde.org click on \"contribute\". I would like e.g. to read an introduction for and about differences between Karamba and SuperKaramba. Both were not mentionened on dot.kde.org yet."
    author: "Anonymous"
  - subject: "Re: New Koffice"
    date: 2003-04-27
    body: "I plan to do it sometime if nobody else is quicker."
    author: "Datschge"
  - subject: "http://khtml-win32.sourceforge.net/"
    date: 2003-04-25
    body: "This port doesn't seem to be going anywhere :("
    author: "bobbob"
  - subject: "Re: http://khtml-win32.sourceforge.net/"
    date: 2003-04-26
    body: "Don't complain, help it!"
    author: "Anonymous"
  - subject: "shell"
    date: 2003-04-26
    body: "Is it possible to use this as a shell for windows; similair to that of litestep, etc.?"
    author: "uber"
  - subject: "Re: shell"
    date: 2003-04-27
    body: "Yes, you can use the fullscreen mode of the KDE-cygwin desktop for doing like this. See the kde-cygwin mailing list archive for further informations how to replace explorer with the KDE-cygwin desktop.\n\n\n"
    author: "Ralf Habacker"
  - subject: "KDE 3.1 for Cygwin Apps"
    date: 2003-04-29
    body: "I downloaded KDE 3.1 for Cygwin and it looks great!  But I can't find any applications.  I can't even find a terminal.  Am I missing something, or are the apps coming out later?\n\nThanks,\nJimbo"
    author: "Jimbo"
  - subject: "Re: KDE 3.1 for Cygwin Apps"
    date: 2003-05-06
    body: "There are all application located in the original kdelibs and kdebase package like konqueror (with ssl support), kwrite, kate, kcontrol, kdesktop, khelpcenter, kicker and more. \n\nRalf \n"
    author: "Ralf Habacker"
---
The <a href="http://kde-cygwin.sourceforge.net/">KDE on Cygwin</a> project, the project to port Qt and KDE to MS Windows, has <a href="http://sourceforge.net/forum/forum.php?forum_id=269806">announced</a> the first <a href="http://kde-cygwin.sourceforge.net/kde3/">beta release of KDE 3.1.1</a> for <a href="http://www.cygwin.com/">Cygwin</a> and <a href="http://xfree.cygwin.com/">Cygwin/XFree86</a>. Currently, both kdelibs and kdebase are ported (<a href="http://static.kdenews.org/mirrors/kde-cygwin-shots/kde-cygwin-normal.png">screenshot</a>), but of further interest is this in combination with the <a href="http://xfree86.cygwin.com/devel/shadow/">Cygwin/XFree86 Test Series</a>.  The latter combination allows better integration into a Windows desktop with either the <a href="http://static.kdenews.org/mirrors/kde-cygwin-shots/kde-cygwin-rootless.png">rootless</a> (no root window, KWin as window manager, no taskbar entries) or <a href="http://static.kdenews.org/mirrors/kde-cygwin-shots/kde-cygwin-multiwindow.png">multi-window</a>  (no root window, MS Windows as window manager, taskbar entries) modes.
<!--break-->
