---
title: "Eye on Karamba: Talking to Hans Karlsson"
date:    2003-05-05
authors:
  - "Datschge"
slug:    eye-karamba-talking-hans-karlsson
comments:
  - subject: "Great work!"
    date: 2003-05-05
    body: "It's good to see karamba/super-karamba/karamba-rss merge into one project.\n\nBy the way, are we going to see a graphical editor for new sensors, such as the one in Samurize?"
    author: "fault"
  - subject: "Konfabulator compatibility"
    date: 2003-05-05
    body: "The *Karambas are all a Konfabulator clone (which itself is a clone of Stardock's Object Desktop which exists for years now).\n\nAre there any plans to change the API of *Karamba in a way that will make the \"porting\" of the already 280+ existant Konfabulator widgets easily portable under KDE?\n\nhttp://konfabulator.deskmod.com/?site=konfabulator"
    author: "Jean-Pierre"
  - subject: "Re: Konfabulator compatibility"
    date: 2003-05-05
    body: "not to be nitpicky, but actually, Karamba is a clone of samurize, which is the successor to CureInfo.. \n\nand CureInfo existed before StarDock or konfabulator ever existed :)"
    author: "fault"
  - subject: "Re: Konfabulator compatibility"
    date: 2003-05-05
    body: "Stardock has been around since the early 90s when it began introducing games on OS/2. They introduced Object Desktop in the mid 90s around the time win95 came out and was actually seen in TV ads for OS/2 3 Warp. Is CureInfo that old? I do recall though that it provided a custom toolbar, sensor widgets and custom window decorations... nothing actually writing on the screen."
    author: "Eric Laffoon"
  - subject: "Re: Konfabulator compatibility"
    date: 2003-05-05
    body: "Ooops, change StarDock with ObjectBar in my last post, and there you go :)\n\nStarDock's ObjectDesktop for OS/2 and for win32 are not the same, by the way. The only part of the win32 OD that was actually released before 2000 was WindowBlinds.  Most of the rest came out in 2000, but didn't really mature until later. \n\nYeah.. cureinfo's main purpose was to put things in the start menu or taskbar. However, later on, there was a version called CureInfo 2k which let you put transparent information on the desktop with the new GDI extentions present in Windows 2000."
    author: "fault"
  - subject: "Karamba!"
    date: 2003-05-05
    body: "Karamba really is the best thing to hit KDE-LOOK.org, in my opinion it is not just eyecandy, but it can be put to real uses. Of course, Karamba is at the very beginning of its life before it can really stand out from its competition on other platforms, but its already looking very nice. I hope this project really gains momentum and developers. \n\nWhat I would like to see is a combination between these :\nhttp://www.stardock.com/products.asp (truly the best of the best when it comes to desktop customization for windows, this is soooooo cooool)\nSamurize\nKonfabulator\n"
    author: "Mario"
  - subject: "Karamba is cool but..."
    date: 2003-05-05
    body: "I think its sort of a \"transition\" application.  I mean, sure you've got your OS-X dock rip-off called TuxBar and its cool but it's too slow.  I have a 750Mhz Athlon with 256MB of RAM and a GeForce MX420 -- granted no where near bleeding edge but it should be decent.  However TuxBar runs slow enough that it's annoying.  Karamba as a \"sensor\" output program is probably where it should stay.\n\nIn order to really implement TuxBar, it should be a C++ application, not the current Python+Karamba.  Maybe karamba ought to have its functionality exposed as a library as well as a Python object.  Then someone could implement scripts, along with KDE's new RSS capabilities, as a compiled application which will run a lot faster.  Should I start practicing what I preach, yes I probably should.  Will I do it, maybe :)  That all depends on how much I like deriving equations from ASM charts and FSM graphs.\n"
    author: "Super PET Troll"
  - subject: "Re: Karamba is cool but..."
    date: 2003-05-05
    body: "TuxBar for those who don't know:\nhttp://www.kde-look.org/content/preview.php?file=5812-1.jpg\n\nLooks terrific."
    author: "anon"
  - subject: "Re: Karamba is cool but..."
    date: 2003-05-05
    body: "Actually the python/c++ nature of SKaramba works out very nicely.  Most of all the hard work is done in c++ libs, not in python, so in reality the python code stays out of the way  (for the most part) and isn't a limiting factor to the speed of the program.  The people writing python enabled karamba themes just need to realize that they need to be cognizant of CPU usage in their programs (like the C++ coders already do.)\n\nI released a SKaramba theme called kroller, and it uses much less CPU than the original Tuxbar (for an unknown reason).  The version I will be releasing today or tomorrow will use about 1/2 the CPU of the current version, and I have plans to release a version that will hopefully use at least 1/2 of that sometime in the next few weeks.\n\nI think that right now the people writing karamba themes aren't worried about CPU usage, it's all about \"ohh! look what I can do!\", and not really about making CPU effecient programs.  With time these themes (and karamba itself) will mature and I think we'll have an awesome addition to the KDE.\n\nWith that said, it would still be very cool to have SKaramba's functionality exposed as a library :)"
    author: "Caspian"
  - subject: "Re: Karamba is cool but..."
    date: 2003-05-06
    body: "Try the SuperKaramba bar; don't remember what it's called.  I have run it on a k62-350 and have it run reasonably fast; your Athlon should be able to handle it.\n\nHaving said that, I agree that this isn't the best way to implement a dock; anyone who has used the \"real\" Apple dock would scoff at this."
    author: "regeya"
  - subject: "Re: Karamba is cool but..."
    date: 2003-06-10
    body: "TuxBar runs slow? Great! It will be a terrific emulation of how fast the dock panel runs on my daughter's iMac!"
    author: "TinWeasil"
  - subject: "Re: Karamba is cool but..."
    date: 2004-11-12
    body: "Yeah...ok, why are you posting on this forum? If you don't know, this is for a Linux program...\n\nI think SKaramba does wonders. Yes it has its weaknesses, but those will be fixed. Just be patient. Code maturity will come. Plus, if you have a problem with a program, maybe you should stop complaining and contribute more efficient code. It is open source after all..."
    author: "Andrew"
  - subject: "Re: Karamba is cool but..."
    date: 2004-11-12
    body: "Wow. It's been well over 17 months since I posted that, and it's taken this long for someone to reply!\n\nI am fully aware that the discussion is about a Linux program. My comment was a joke. Since Tuxbar was said to \"run slow\" I was simply pointing out the fact that it would run about as fast as the Mac dock panel runs on my daughter's old iMac.\n\nI wasn't complaining. Just making a joke about older hardware. Sorry if you were offended.\n\n"
    author: "TinWeasil"
  - subject: "Datschge"
    date: 2003-05-05
    body: "This guy gets more and more involved into KDE, does anyone know who he is? :-)"
    author: "Anonymous"
  - subject: "Re: Datschge"
    date: 2003-05-05
    body: "I think he is related to qwertz.  ;-)"
    author: "Navindra Umanee"
  - subject: "Re: Datschge"
    date: 2003-05-05
    body: "Nah, I'm independent. =P"
    author: "Datschge"
  - subject: "If it feels good do it..."
    date: 2003-05-05
    body: "> The work with Karamba felt more like a regular job than the fun hobby I wanted it to be.\n\nHmmm? It was really fun at first and then... I remember that fun feeling. Imagine how I felt when my partners left the project and I had never previously coded C++. Fun? There was the politics and the nasty emails... Then a few months later I lost my mom. In the process of learning the QT/KDE APIs and our code base I was fortunate to find Andras. Quanta has become very popular. I don't think it gained popularity quite as fast as karamba. At least there were not as many users back then. Currently Andras works on it full time, there are many contributors and it eats huge blocks of my time. Our motivations are different.\n\nAlong the way I found something else that felt really good. It was the positive feedback from the community.I started to believe that what we were doing had to be done because it needed to be done, and if not us than who? I'm not knocking the author's position of coding for a fun hobby. I'm just saying that in spite of the challenges there are other rewards. In the end there has to be someone at the helm of an open source project or it dies, despite popular mythology about never losing the source. Source gets outdated fast around here.\n\nBTW I'm using Karamba and I really like it and recommend it. I wrote a Kommander dialog to switch the cities I get the weather for. ;-)"
    author: "Eric Laffoon"
  - subject: "Karamba vs. SuperKaramba"
    date: 2003-05-05
    body: "I'm one of the new maintainers for Karamba and creator of the RSS patch. We've been in brief discussions with the maintainer of SuperKaramba and it looks like, at this time, we will NOT be merging the code bases. Mainly, I think, as projets we have different aims. Hopefully in the future a better separation between the Python and C++ portions can be established and the projects can co-exist or intermingle.\n\nWith that being said, we're in the early stages of getting the project setup on SourceForge. You can download the last version Hans created, 0.17, from SF right now: http://sourceforge.net/projects/karamba Hopefully, we'll have some more stuff up soon (including a new version with my RSS patch integrated). After that I plan n just working through some of the items on the TODO list, etc.\n\nAlso, I think there is interest in maybe enhancing the extensibility of Karamba -- making it easier to add non-core functionality. Maybe through \"plugins\", etc. Anyway, lots to do! Please feel free to post to the SF forums or drop us a line. Thanks!\n\nRMC"
    author: "Ralph M. Churchill"
  - subject: "Re: Karamba vs. SuperKaramba"
    date: 2003-05-05
    body: "This really doesn't make much sense at all. In what ways are the aims of Karamba and SuperKaramba different? Sooner or later, the two projects are going to have to merge, or else one will die off--doesn't it make sense to merge them now and avoid wasting time before the inevitable?"
    author: "Rodion"
  - subject: "Re: Karamba vs. SuperKaramba"
    date: 2003-05-10
    body: "Sure"
    author: "Dave"
  - subject: "Re: Karamba vs. SuperKaramba"
    date: 2003-06-08
    body: "Hhmmm - a thread entitled Karamba vs. SuperKaramba that totally ignores the aim.  Not handy.  So, is anyone actually going to dare approach the topic of this thread and actually DETAIL the differences?\n\n- HiltonT"
    author: "HiltonT"
  - subject: "Re: Karamba vs. SuperKaramba"
    date: 2003-10-24
    body: "I guess not, huh? and here I am, after googling \"karamba vs. superkaramba\" months later, and void of information because of it!\n\nFor shame!"
    author: "Miles"
  - subject: "Re: Karamba vs. SuperKaramba"
    date: 2003-10-24
    body: "Karamba uses perl for its theme scripts while SuperKaramba (without space gets you more hits when searching) mainly uses python instead."
    author: "Datschge"
  - subject: "Slashdotted"
    date: 2003-05-05
    body: "For those who might be wondering as to why Look is down and/or slow, Karamba is the lead story on Slashdot as of right now."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Slashdotted"
    date: 2003-05-05
    body: "...and I thought the slashdotters where all gnomesters...\n"
    author: "reihal"
  - subject: "Lund LUG"
    date: 2003-05-05
    body: "Hasse!\n\u00c4r det inte dags att f\u00e5 ig\u00e5ng en LUG i Lund med omnejd? (S\u00f6dra Sverige, ej Danmark)"
    author: "reihal"
  - subject: "Re: Lund LUG - i18n"
    date: 2003-05-05
    body: "Hasse!\nIsnt it about time to get a LUG in Lund and surounding areas? (Suthern Sweeden, not Denmark)"
    author: "kidcat"
  - subject: "Re: Lund LUG - i18n"
    date: 2003-05-06
    body: "Darn! You cracked the secret code!"
    author: "Patrik Grip-Jansson"
  - subject: "Re: Lund LUG"
    date: 2003-05-06
    body: "jo."
    author: "teatime"
  - subject: "Re: Lund LUG"
    date: 2003-05-07
    body: "Bra, d\u00e5 ses vi p\u00e5 Internetkaf\u00e8et p\u00e5 Lilla Gr\u00e5br\u00f6dersgatan, l\u00f6rdag 10:e maj kl 16:00.\nOk?\n"
    author: "reihal"
  - subject: "Karamba superkaramba bars and window behaviour"
    date: 2003-05-05
    body: "I like karamba and superkaramba very much from  an eyekandy perspective. My main problem with tuxbar style bars is not cpu useage but the fact that they get covered by windows. The rollover effect in kicker is pretty lame compared to that nice svg zoom . So what I was wondering is if there are any plans to improve kicker to give it smoother icon zooming effects? Or any way to prevent windows covering tuxbar when they are maximized? Any, thanks for all the nice cpu chomping eyecandy folks!"
    author: "Tim Linux"
  - subject: "Re: Karamba superkaramba bars and window behaviour"
    date: 2003-05-06
    body: "SVG zoom? I think that you're dreaming."
    author: "Anonymous"
  - subject: "GTK?"
    date: 2003-05-05
    body: "Okay, so I really like this app, my only problem is it looks like crap under GNOME are they any plans to run the app, under gtk+ thanks. \n\n"
    author: "Vito"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "You should be able to run it under KDE.  Your GNOME apps will still work."
    author: "anon"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "that wasn't the question, now was it?"
    author: "Uranus"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "It's a good answer though, isn't it?  Why not go troll on a GNOME site?"
    author: "anon"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "It should run just fine, you'll need to have QT and KDElibs installed, though, for some things to look right...you don't need the entire KDE package, just the libraries (which depend on QT)..."
    author: "D. A. Stone"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "Karamba relies on the krootpixmap class in kdelibs/kdecore, which relies on kdesktop. So.. you need to either make it get the bg from elsewhere, or run kdesktop."
    author: "fault"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "This just makes me fall in love with KDE all over again.  So simple, so easy, so powerful.\n\n/**\n * Creates pseudo-transparent widgets.\n *\n * A pseudo-transparent widget is a widget with its background pixmap set to\n * that part of the desktop background that it is currently obscuring. This\n * gives a transparency effect.\n *\n * To create a transparent widget, construct a KRootPixmap and pass it a\n * pointer to your widget. That's it! Moving, resizing and background changes\n * are handled automatically.\n *\n * Instead of using the default behaviour, you can ask KRootPixmap\n * to emit a @ref backgroundUpdated(const QPixmap &) signal whenever\n * the background needs updating by using @ref setCustomPainting(bool).\n * Alternatively by reimplementing @ref updateBackground(KSharedPixmap*)\n * you can take complete control of the behaviour.\n *\n * @author Geert Jansen <jansen@kde.org>\n * @version $Id: krootpixmap.h,v 1.14 2002/03/08 18:01:07 rich Exp $\n */"
    author: "anon"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "I was not trolling sorry, I was / am not aware of the working of KDE I had no Idea that it was kwin that was causeing the app to show black instead of my background. \n\nI love the application, but am set in using GTK for now. \n\nThanks for the info guys! \n"
    author: "Vito"
  - subject: "Re: GTK?"
    date: 2003-05-05
    body: "You don't understand, KWin isn't causing anything.   You can keep running your GTK apps in KDE. "
    author: "anon"
  - subject: "Re: GTK?"
    date: 2003-06-04
    body: "anon: you don't seem to grasp his question.  \nvito: it *should* work under gnome, I've seen it work under Waimea, so presumably it should work under gnome, assuming the proper libraries are installed.  I'm obviously not  knowledgebase on kde, but this karamba is one cool piece of software.  Still wouldn't make me switch to kde tho (I use Waimea just so you know anon)"
    author: "gtk guy"
  - subject: "Re: GTK?"
    date: 2003-06-05
    body: "Has same problem as Vito's under Waimea.  Using ESetRoot for the background.  Otherwise works fine"
    author: "gtk guy"
  - subject: "Re: GTK?"
    date: 2003-05-07
    body: "I also like Karamba, despite some problems I'm having with it (seg faults when using rss output from slashdot), and it requiring KDE. I don't really have anything against KDE, and I use things like KMail fairly often, I just prefer enlightenment as my window manager (without gnome btw), which is where the problem comes in. Since enlightenment uses its own background daemon, and karamba needs kdesktop to render its background, I can't run karamba under E. I wonder how difficult it would be to get Karamba to fetch the desktop background from something other than kdesktop?"
    author: "Orclev"
  - subject: "Re: GTK?"
    date: 2003-05-07
    body: "It would be easier (and a much better idea) to make enlightenment publish its background in way compatible with KRootPixmap, that way anything in KDE that uses it for transparency would work nicely with enlightenment.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: GTK?"
    date: 2004-04-12
    body: "Is there a workaround for using karamba with enlightenment and still get transparency? I like enlightenment and my only interest from kde is superkaramba\n\nthanks.\nValentino Crosato Jr.\n"
    author: "Valentino Crosato Jr."
  - subject: "Re: GTK?"
    date: 2004-04-12
    body: "Not unless you use kdesktop.. you can try gdesklets, which doesn't depend on KDE's background mechanism"
    author: "anon"
  - subject: "Re: GTK?"
    date: 2004-04-12
    body: "Ive tried:\n\n1-  enlightenment\n    kdesktop --x-root\n    superkaramba \n\n    # result: still no transparency, black background on superkaramba\n    #         and no enlightenment (left,middle,right) click menus\n\nWhat is the right way to do it?\n\nthanks\nValentino C. Jr.\n  \n     "
    author: "Valentino Crosato Jr."
  - subject: "Customizer"
    date: 2003-05-06
    body: "There is nothing wrong with a look on a windows theme page like customizer.com\n\nwallpapers can be used on every machine."
    author: "Andr\u00e9"
  - subject: "Karamba could be great."
    date: 2003-06-05
    body: "Ok, I like the idea.  It's a really cool idea in many cases.  It's like \"Hey, I've now got active desktop on crack for linux\" yeah sure... whatever.  But it has serious flaws.  For example, last time I tried it, I had this tight looking little control bar thing with the kmenu and such all on it, it was nice looking, and functional, but, EVERY SINGLE SECOND, it would cut into the cpu usage something like, 40 percent.  And that's an underestimate.  *YOU* go play quake3 with 40% cpu and some hard drive usage going on every single second... dead yet?  Yeah, that's what I thought.  Ok, that's fine, it can be fixed with proper idle calls and implementing the common features internally and in python script instead of external apps.  Ok, so we know *HOW* to fix it.  Nod if you understand... ok, now, for the second, truly very annoying problem.  I love the feature in kwin that allows windows to auto-attract to each other, I can't remember what it's called, but, I don't care, they basically snap together, and that's nice, helps me keep things well organized without much effort.  But, heh, why in god's name do my windows need to snap to the real border of a karamba item?  I cannot possibly be the only person who has noticed this extreme bug... ok fix those two things and I'll be sold.\n\n-picky bastard"
    author: "cybereal"
---
<a href="http://www.efd.lth.se/~d98hk/karamba/">Karamba</a> (<A href="http://www.kdelook.org/content/preview.php?file=5561-1.jpg">shot1</a>, <a href="http://www.kdelook.org/content/preview.php?file=5561-2.jpg">shot2</a>) is a desktop enhancement, similar to <a href="http://www.konfabulator.com/">Konfabulator</a> and <a href="http://www.samurize.com/">Samurize</a>, that appeared on <a href="http://www.kdelook.org/">KDE-Look.org</a> (looky) about <a href="http://www.kdelook.org/content/show.php?content=5561">one month ago</a>. Since then, Karamba has become the <a href="http://www.kdelook.org/index.php?xsortmode=high">highest rated project</a> on looky with a <a href="http://www.kdelook.org/content/search.php?page=0&search=1&type=0&name=&user=&text=karamba&sort=2&perpage=10&scorefilter=0">surprisingly large number</a> of themes and extensions having been contributed, subsequently leading to a <a href="http://www.kdelook.org/index.php?xcontentmode=karamba">dedicated category</a> for Karamba contributions. We had a little chat with author Hans Karlsson about himself, KDE,  Karamba, and the hype surrounding it.
<!--break-->
<p>
<b>1. Please tell us about yourself and how you got involved with KDE.</b>

<blockquote>
I'm a 25 year old from <a href="http://www.visit-sweden.com/gb/article.asp?show=a&articleID=5385">southern Sweden</a>. I have just finished my education, Master of Science in Computer Science and Engineering, at <a href="http://www.lth.se/">Lund Institute of Technology</a>. I got my first computer (<a href="http://www.c64.org/">Commodore 64</a>) around 1986 or 1987 and I've been hooked since then. I became a Linux user in 1999 when I installed Red Hat.  Around the end of 2001 I found the kde-look site and there were some impressive looking desktops. Before that I liked, be prepared for a shock :), <a href="http://www.gnome.org/">GNOME</a> more than KDE. But none of them could replace my <a href="http://www.windowmaker.org/">Window Maker</a> desktop. Window Maker was replaced by KDE when I got a new computer a couple of months later.
</blockquote>

<p><b>2. You are known as <a href="http://www.kdelook.org/usermanager/search.php?username=themesKnugen">themesKnugen</a> on looky and you were contributing wallpapers for nearly one and a half years prior to Karamba. What made you want to create software this time?</b>

<blockquote>
I've been coding on and off since I got my first computer. But Karamba is the first program that I've released to the public. I began developing Karamba when I couldn't stand being without the Windows program <a href="http://www.samurize.com">Samurize</a> anymore. When I had a desktop I thought looked good I uploaded a screenshot to kde-look. It was partly a kind of inverted April fools joke. A suspicious looking screenshot that wasn't a fake was uploaded on April 1st. =) People seemed to like what they saw, so I released the source and some documentation.
</blockquote>

<p><b>3. What were your objectives with Karamba, and what features do you think are still missing?</b>

<blockquote>
The objective was to bring Samurize-like functionality to my Linux machine, i.e., displaying system information integrated with the desktop in a good looking themeable way. I think that Karamba has the features that I need right now. But that can of course change. By this I don't mean that Karamba is finished, just that it has what I personally need. One possible direction for Karamba is to make it more interactive. <a href="http://netdragon.sourceforge.net">SuperKaramba</a> is a initiative to do this by adding Python scripting capabilities. I think the support for non-Linux platforms can be improved.
</blockquote>

<p><b>4. What is the story behind the name "Karamba"?</b>

<blockquote>
As Karamba was supposed to be a Samurize clone I thought of finding a name that had some connection to that. But the word Samurize is a clever word play with the English word summarize, that made it even harder to find a good name. So I decided to continue the loved and hated KDE tradition of using a K name. I wanted a word with a leading "C" letter that can be changed to the mighty "K" letter. <a href="http://www.wordreference.com/es/en/translation.asp?spen=caramba">Caramba</a> came to my mind after a while, and I thought the word had a positive and happy feeling. Karamba! One gets happy just by saying it. =)
</blockquote><p>

<b>5. Your Karamba project lasted for several weeks between March and April. Right after the announcement on looky its rating skyrocketed and remains the highest rated content on looky even today. There have been over one hundred themes and extensions contributed to looky since then which are making use of Karamba, leading to an own category for Karamba themes. Looking at this all it's safe to call Karamba a true shooting star. Are you surprised about the public response to your project?</b>

<blockquote>
Yes and no. Samurize is popular and I really like Karamba myself. My desktop just feels so empty if I remove my Karamba themes. But I can't say that I expected it to become the highest rated item on kde-look, but it certainly feels quite good. I wish to thank all Karamba users for your comments and compliments!
</blockquote><p>

<b>6. On looky, <a href="http://www.kdelook.org/comments/discussion.php?id=1&month=16#c26544">you wrote</a> that the project is practically dead for you now. What led to this situation?</b>

<blockquote>
I don't have a really good answer to this. But I'll try to explain it. Before the public release of Karamba I coded when I felt for it. So it could take days between the coding "sessions". It was fun to add the new features I wanted. After the release the work on Karamba became very intense. I tried to help people with, e.g., bugs, compilation errors, patches, feature requests, themes, scripts. Then I was away from home for about two weeks during an easter break from school. But when I came back I couldn't manage to get any work done. The inspiration wasn't there. The work with Karamba felt more like a regular job than the fun hobby I wanted it to be. And the situation hasn't changed yet. I don't blame those who contacted me, not at all. They all wanted to help to make a better program. I just wasn't ready for the seriousness of being a maintainer of a project. The conclusion is that I think Karamba deserves a better maintainer than what I am now. And a little clarification: Karamba is not dead for me, more like deep frozen. A subtle but important difference. :)
</blockquote><p>

<b>7. What are your favourite tools under KDE?</b>

<blockquote>
There are so many good tools. <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://konsole.kde.org/">Konsole</a> and <a href="http://kmail.kde.org/">KMail</a> all work great. <a href="http://www.kdevelop.org/">KDevelop (Gideon)</a> and <a href="http://extragear.kde.org/apps/kmplayer.php">KMPlayer</a> are nice too. And one must not forget <a href="http://apps.kde.com/uk/0/info/id/488">KPatience</a> and <a href="http://www.katzbrown.com/kolf/">Kolf</a>.
</blockquote><p>

<b>8. What are your dreams for the desktop of the future? How far are we from the ideal desktop?</b>

<blockquote>
Very difficult  questions. I can't say that I have a vision of a perfect desktop. But there are some things that would make me happier: Improved tabs in Konqueror, better support for transparency in X, and a sound system with mixing capabilities that don't skip and that has no delay. I don't know if there is an ideal desktop, and I certainly don't know how far away it is. But I'm sure that Karamba will be part of it. :)
</blockquote><p>

<b>9. What kind of hardware do you have and what OS do you use?</b>

<blockquote>
I have an Athlon XP 1800+ with 512 MB RAM. I use Red Hat 8.0 but, e.g., XFree86, glibc, and gcc is from rawhide and my KDE is from CVS HEAD. So I guess it's more like Red Hat 8.5 or something. :)
</blockquote><p>

<b>10. Any final thoughts and comments?</b>

<blockquote>
I just want to wish <a href="http://www.kdelook.org/usermanager/search.php?username=MrChucho">Ralph M. Churchill</a>, <a href="http://www.kdelook.org/usermanager/search.php?username=ageitgey">Adam Geitgey</a>, and <a href="http://www.kdelook.org/usermanager/search.php?username=dolmant">Matt Jibson</a> good luck. They are currently the people behind <a href="http://www.kdelook.org/comments/discussion.php?id=1&month=17#c26573">Karamba's recent move</a> <a href="http://karamba.sourceforge.net/">to sourceforge</a>. This will hopefully make Karamba to gain some momentum.
</blockquote><p>

<i>Best wishes for the future and many thanks to Hans Karlsson for
        starting the Karamba effort as well as taking
        the time to answer our questions! =)</i>