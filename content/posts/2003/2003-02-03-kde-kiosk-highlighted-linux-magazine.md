---
title: "KDE Kiosk Highlighted in Linux Magazine"
date:    2003-02-03
authors:
  - "aseigo"
slug:    kde-kiosk-highlighted-linux-magazine
comments:
  - subject: "Internet Explorer"
    date: 2003-02-03
    body: "I remember some years ago when I start a similar project under IE with Internet Helper Objects :))) but he died because of lack of documentation. I'm happy now to see the Kde team making so good stuff instead!\n\nSacx\n"
    author: "sacx"
  - subject: "Re: Internet Explorer"
    date: 2003-02-03
    body: "Wow, I hope I don't die because of a lack of documentation in what I write. :)\n"
    author: "Super PET Troll"
  - subject: "Title is wrong"
    date: 2003-02-03
    body: "It isn't in Linux Journal but in Linux Magazine :)"
    author: "Peter Simonsson"
  - subject: "Re: Title is wrong"
    date: 2003-02-03
    body: "Oh dammit!  That is an unfortunate mistake...\n"
    author: "Navindra Umanee"
  - subject: "Are we allowed .."
    date: 2003-02-03
    body: "to translate this article for use during events? Anyone knows? Does the copyrigth belong to Aaron J.Seigo or to Linux Magazine?\n\nbtw this article is really good and comprehensive. \n\nFab"
    author: "Fab"
  - subject: "Re: Are we allowed .."
    date: 2003-02-03
    body: "you'll have to talk to the Linux Mag people about it.. but they are VERY nice and very community oriented. i am sure that they would welcome translations ... email me privately if you're interested and i can give you the address/name of the fellow to contact."
    author: "Aaron J. Seigo"
  - subject: "Re: Are we allowed .."
    date: 2003-02-03
    body: "Aaron, nice job!\n\nThis was an excellent article and I am very impressed with the description of the kiosk framework.  I have a few questions:\n\n* Will the kiosk framework lock non-kde applications such as Evolution or Open Office? And if so, are there any limitations?\n\n* Any plans for a graphical interface perhaps in the KControl?\n\n* How about using the kiosk for thin client setups like Largo? does this work?\n\nThanks!\n\nAdam"
    author: "Adam Treat"
  - subject: "Re: Are we allowed .."
    date: 2003-02-04
    body: "Some answers\n\n* The kiosk framework only locks KDE applications.\n\n* Chris Howells has the early stages of a GUI, see kpolicy in kdenonbeta\n\n* I don't have a thin client setup around but I am sure it will work just fine :)\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Are we allowed .."
    date: 2003-02-04
    body: "waldo already answered the first question, but i'll add answers for #2 and #3..\n\nyes, there are plans for a GUI interface to all of this. as Waldo pointed out there is kpolicy, but i also have my eye on kconfedit (also in kdenonbeta right now) as another tool that might be very useful in this regard\n\nas for thin client set ups: yes, Kiosk works very well in those situations. i've done some work with thin client set ups and kiosk makes locking things down and providing standardized resources a breeze!"
    author: "Aaron J. Seigo"
  - subject: "small addendum, before i forget"
    date: 2003-02-04
    body: "if you decide to do KIOSK stuff via the GUI as root (but please don't =) and you start by reseting your KDE home dir, remember that for root it is $KDEROOTHOME and not  $KDEHOME as it is for all other users...\n\noh, and a major thanks to everyone (hey waldo! =) who has worked on the Kiosk stuff in KDE. it is one of those things that makes KDE a viable option in corporate settings. and that isn't just theory speaking, i've actually been witness to Kiosk being the deciding factor between KDE and other options."
    author: "Aaron J. Seigo"
---
A feature article on KDE that appeared in the November edition of Linux Magazine is now <a href="http://www.linux-mag.com/2002-11/kde_01.html">available online</a>. The article concentrates on deploying KDE in business environments and showcases a few of KDE3's newer features. It outlines how to lock down and configure a KDE3 installation using the KDE kiosk framework and how to set up KDE to use centralized resources for configuration and data.
<!--break-->
