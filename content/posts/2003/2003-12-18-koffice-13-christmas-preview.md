---
title: "KOffice 1.3 Christmas Preview"
date:    2003-12-18
authors:
  - "ltinkl"
slug:    koffice-13-christmas-preview
comments:
  - subject: "Krita?"
    date: 2003-12-19
    body: "Changelog made no statement about krita?\n\nIs it still part of KOffice?\n\nI know there are some activities on the mailing list now..."
    author: "NamShub"
  - subject: "Re: Krita?"
    date: 2003-12-19
    body: "Yes, krita is part of koffice. But not in version 1.3 :-(\nLet's hope for koffice 1.4/2.0"
    author: "Birdy"
  - subject: "Re: Krita?"
    date: 2003-12-19
    body: "But it will be released as a first stadnalone version?\n\nBtw. i don't understand why Koffice applications are not released more often."
    author: "Gerd"
  - subject: "Re: Krita?"
    date: 2003-12-20
    body: "Krita propably not. (Kexi might be have a preview.)\n\nAs for why not more often: well, find more developers, then may be.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Krita?"
    date: 2003-12-19
    body: "It's really and truly and definitely not ripe for inclusion now. I wish it were different, especially since I expect Krita to attract more support and attention when it gets a wider distribution, but at the moment all it has is a strong architecture, a rather unpolished interface and one buggy, completely unfinished paint tool. \n<p>\nHowever, the Christmas holidays are coming up, and I have taken three days off, so I should be able to spend at least some time on that paint tool, making it fit for human consumption. Having at least one working paint tool is important because it validates the architecture and provides an example for other paint tools.\n<p>\nIn case people would like to take a stab at hacking, it might be interesting to read the short paper I did on Krita's internals at:<br>\n<a href=\"http://koffice.kde.org/developer/krita/\">\nhttp://koffice.kde.org/developer/krita/</a><br>\nKrita could actually be a pretty interesting project for someone interested in computer graphics -- the Krita architecture should support interesting things like 32-bit colour, CMYK and other colour models and interesting types of paint tools, packaged in an easy to use package.\n<p>\nMy personal personal interest is in presenting natural media type paint tools in a package that's easy enough for an artist to sit down and start painting.\n\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita?"
    date: 2003-12-19
    body: "32Bit color means 24RGB+Alpha or 32Bit per Channel?\nToday you need at least 16Bit per Channel (RAW-Pictures of scanner and digicams)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Krita?"
    date: 2003-12-19
    body: "No, no -- it should (but doesn't quite at the moment), support 32 bits per channel.\n\nAt the moment there's buggy CMYK(A) and decent RGB(A), both with 8 bits per channel, but all the colour model specific code is (or should be) encapsulated in colour strategies, and it's really easy to add new colour models. One day, when we have progresses a bit further, it will seem natural to add new colour models simply by adding a colour model plugin.\n\nOn the other hand, I am really not knowledgeable in this area. When starting on Krita I read four or five books on computer graphics and a few papers, but that's all, and my particular interest isn't in colour models -- even though I feel that I can abuse the colour strategy design to implement a 'wet & sticky' paint model or a 'wet' paint model. People who really know what they are doing might want to do a little gentle work on the current kis_strategy_colorspace_cmyk, for instance."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita?"
    date: 2003-12-19
    body: "Have tables improved in kword ? That was the single reason \nI was not using kword, while I very much want to switch to Koffice \nfrom Open Office which I am currently using.  \n\n"
    author: "Asokan"
  - subject: "Tables in KWord"
    date: 2003-12-19
    body: "They have improved a little bit (e.g. it's easier to resize rows and columns, there are less painting bugs, and a few other changes I forgot), but table support in KWord still isn't optimum, to be honest. Still worth a try though?/\n"
    author: "David Faure"
  - subject: "KWord"
    date: 2003-12-19
    body: "I just finished compiling and installing it and I have to say, koffice is *much* improved since the last time I looked at it. KWord is actually usable for me now, table support is much better than it was (though not 100% yet). The only thing really missing is import and export filter being functional and comprehensive. (i.e. I exported a document to open office format and it didn't include any of the frames or the text in the frames.)\n\nIn any event, great job guys, keep it up DF.\n\nCheers,\nTormak"
    author: "Tormak"
  - subject: "Re: KWord"
    date: 2003-12-19
    body: "Finally, someone gave it a try!  I expected more people to be posting to this thread but I guess having to compile sources is a problem."
    author: "KDE User"
  - subject: "Re: giving it a try"
    date: 2003-12-20
    body: "Well, working on it, but it's being a bit awkward to compile...  \n\n(Compiling on a Fedora Core 1 system, current as of today's, 2003 12 19's, updates.)\n\ncd ../../.. && \\\n  /bin/sh /usr/src/redhat/BUILD/koffice-1.2.95/admin/missing --run automake-1.7 --foreign  lib/kotext/kohyphen/Makefile\nconfigure.in:43: version mismatch.  This is Automake 1.7.8,\nconfigure.in:43: but the definition used by this AM_INIT_AUTOMAKE\nconfigure.in:43: comes from Automake 1.7.6.  You should recreate\nconfigure.in:43: aclocal.m4 with aclocal and run automake again.\nmake[4]: *** [Makefile.in] Error 1"
    author: "Graydon"
  - subject: "Re: giving it a try"
    date: 2003-12-20
    body: "never mind, I'm dumber than bricks.  Take the libtool patch out of the RPM sources for the 1.2.94 version and it (so far, still compiling) seems fine."
    author: "Graydon"
  - subject: "Re: KWord"
    date: 2003-12-20
    body: "The last time I tried to compile the 1.3beta, I got a lot of dependency errors and I couldn't fing the needed files.  I'm running MDK9.1.  Is there anything special I need to compile it?\n\nCire"
    author: "cirehawk"
  - subject: "Re: KWord"
    date: 2003-12-20
    body: "Yes, for the word import/export filter you will need wv2. Unfortunatly there isn't a recent wv2 wv2-dev rpm for mdk9.1 (which is what I have). I had to get the wv2 sources and libwmf source and configure them on another machine since I ran into autoconf problems on 9.1, then compiled them on my machine. wv2 has a long list of dependencies, and if you don't need the ms word filters it is much easier. There was one other dependency which I don't recall, but I got it using urpmi (with the plf source iirc search on google for easy urpmi). The only other problem I had was compiling kdchart in kchart. It seems that a #define didn't get defined properly and I had to manually add #include <config.h> to KDchartGlobals.h (something like that), and then add to the Makefile.am -I.. -I. in the C and CXX includes. After a few hours it finished and installed w/o further problems. It really is much better, esp kword, but unfortunatly kword also segfaulted on importing a really large (and fairly complex) msword document that I use a a benchmark of when I think a wp is up to snuff. That said, once there are solid word and openoffice import and export filters (and it's stable) I would strongly consider moving to kword permanently.\n\nIf you want I could make the compiled source tree available to you, for libwmf, wv2 and koffice-1.2.9 if you have a place for me to upload it, and have all of the required libs installed. (This would be for mdk 9.1).\n\nCheers,\nSheldon."
    author: "Tormak"
  - subject: "Re: KWord"
    date: 2003-12-20
    body: "If someone can reproduce the KDChart problem, posting the real compiler error message could be useful.\n\nHave a nice day1"
    author: "Nicolas Goutte"
  - subject: "Re: KWord"
    date: 2003-12-21
    body: "The  problem (once I checked again) was actually 2 things. 1) config.h wasn't found b/c the directory wasn't included at compile time, and 2) in kdchart/KDChartGlobal.h there is:\n\n#if defined(unix) || defined(Q_WS_MAC)\n#include  <limits.h>\n#else\n#define  MINDOUBLE DBL_MIN\n#endif\n\non my machine (Mandrake 9.1) for some reason unix wasn't getting defined and limits.h wasn't getting included. If you want more info contact me directly."
    author: "Tormak"
  - subject: "Re: KWord"
    date: 2003-12-28
    body: "The compile problems should be fixed now.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KWord"
    date: 2003-12-22
    body: "Thanks for the input Tormak.  You described exactly the problems I had compiling.  That would be great if you could provide the compiled sourse tree.  Once I have that, what would I need to do (I'm not a programmer, so sometimes I'm not quite sure of some aspects of compiling)?  Also, how large is it and I will find somewhere you can upload it.\n\nThanks again!\nPaul....."
    author: "cirehawk"
  - subject: "Featured on theAGE, an austrialian newsi"
    date: 2003-12-20
    body: "<a href=\"http://www.theage.com.au/articles/2003/12/19/1071337146955.html\">\nhttp://www.theage.com.au/articles/2003/12/19/1071337146955.html\n</a>\n\n"
    author: "anon"
  - subject: "initial experience"
    date: 2003-12-20
    body: "Well, it builds fine; dropped it into the same spec file as the Fedora 1.2.94 rpm, and with the patch files removed it builds nicely.\n\nThere do seem to be a few minor problems, though:\n\nBuild Messages  (from running 'configure' without arguments)\n--------------\n\nYour libaspell is too old or not installed, I couldn't find aspell.h.\nYou must download aspell >= 0.50.2 See http://aspell.net/.\nSpell-checking disabled.\n\nBut \nrpm -q aspell\nreturns\naspell-0.50.3-16  (and there isn't an aspell-devel)\n\n\nYou're missing ImageMagick (>=5.5.2). krita will not be compiled.\nYou can download ImageMagick from http://www.imagemagick.org/\n\nIf you have problems compiling ImageMagick, please try configuring it\nusing\nthe --without-magick-plus-plus flag, the C++ API isn't needed for krita.\n\nBut\nrpm -q ImageMagick\nreturns\nImageMagick-5.5.6-5  (and there isn't an ImageMagic-devel in Fedora, either.)\n\n\nIn Use\n------\n\nKSpread\n\nSo, how do I globally change the text font and font size of cells?  Nothing seems to work but one cell at a time.\n\nNo print-to-fit?  If it doesn't have that, it isn't a useable\nspreadsheet for any business application, and I keep hoping this is going to show up.\n\n\nKword\n\nKWord hit 86% CPU and held it until I closed the file when asked to load\na 1.7 MB rtf file.  Which it displayed and handled fine, including\nrandom jumps to halfway through.  The individual pages wouldn't appear\nin the frame display, though; the long list would appear with the click\nand then vanish.\n"
    author: "Graydon"
  - subject: "Re: initial experience"
    date: 2003-12-20
    body: "KWord first: if you could make the file public, could you please add a bug report in http://bugs.kde.org (Personally, I know two big RTF files with tables that do not work.)\n\nAs for the libraries, I am going to look at what is searched.\nCan you give the output of:\nMagick-config --prefix\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: initial experience"
    date: 2003-12-20
    body: "It would helpful to have the outputs of:\nwhich Magick-config\nMagick-config --version\nwhich aspell\naspell --version\n\nIf you prefer to open a bug report for these configure problems, then please attach your config.log file. (I really mean to attach the file and not to copy it, so you can only do it after doing the bug report.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: initial experience"
    date: 2003-12-21
    body: "The *-devel rpms don't exist anymore on Fedora ?\nThat's a huge improvement :)"
    author: "JC"
---
The official release of KOffice 1.3 was originally planned for this week
but since many people are already preparing themselves for the upcoming
end-of-year festivities we are afraid that binary packages may not become
available for all platforms in time. For that reason we have decided to
release a special <a href="http://www.koffice.org/releases/1.3rc2-release.php">KOffice 1.3 Christmas Preview</a> for all of you who can't wait
to give this new KOffice a try over the upcoming holidays. In the middle of January, the official version of KOffice 1.3 will be released together
with the usual variety of binary packages.



<!--break-->
<p>The KOffice 1.3 Christmas Preview is available for download from:
<a href="http://download.kde.org/unstable/koffice-1.2.95">http://download.kde.org/unstable/koffice-1.2.95</a>
</p>

<p>A changelog listing the differences to the previous release candidate is also <a href="http://koffice.kde.org/announcements/changelog-1.3.php">available</a>.
</p>



