---
title: "The Creative Penguin: Everaldo and Tackat Talk on KDE Art"
date:    2003-01-12
authors:
  - "binner++"
slug:    creative-penguin-everaldo-and-tackat-talk-kde-art
comments:
  - subject: "Noia icons"
    date: 2003-01-12
    body: "Those <a href=\"http://www.kde-look.org/content/show.php?content=3883\">Noia icons</a> are really amazing.\n"
    author: "anonymous"
  - subject: "Re: Noia icons"
    date: 2003-01-12
    body: "Hi,I commit they look interesting, but they are to childish for a corporate desktop! I think it's good to have crystal-icons as the default icons, because they look modern, professional and ready for the corporate desktop:)"
    author: "Philipp Frenzel"
  - subject: "Re: Noia icons"
    date: 2003-01-12
    body: "Although I agree that Crystal may be a better choice as default, I really can't see why Noia shouldn't be available on a corporate desktop.\n\nAs long as it is easy to change back to default, I think the user should be able to influence her workspace quite a bit.\nThere is no such thing as a \"professional\" look; it's more or less a matter of taste."
    author: "G\u00f6ran Jartin"
  - subject: "Re: Noia icons"
    date: 2003-01-12
    body: "Yep, and obviously *everyone* who uses KDE is on a corporate desktop. "
    author: "yep"
  - subject: "Re: Noia icons"
    date: 2003-01-13
    body: "No, but there is nothing wrong in having visions."
    author: "tremble"
  - subject: "Re: Noia icons"
    date: 2003-01-13
    body: "We'll I never thought someone would come up with a viable alternative to the hicolour icons, which were dull but very functional!\n\nI looked at this icon theme and am positively surprised by the functionality (each icon can easily be tied to a function, something crystal SVG has totally missed in it's attempt to be different) and the clean modern look of it. \n\nContinue the great work, this one should really be default (and please drop the uggly mess crystal is currently)\n\nregards"
    author: "Charlyw"
  - subject: "Re: Noia icons"
    date: 2003-01-13
    body: "Yes, they are fantastic! I keep switching between iKons and Noia.\nPlease add Noia to kdeartworks..."
    author: "Peter"
  - subject: "Link?"
    date: 2003-01-12
    body: "Does anyone have a link to that new icon set created by Everaldo?"
    author: "rajan r"
  - subject: "Linux-Mandrake 10"
    date: 2003-01-12
    body: "Everaldo Coelho: ...\"I will be working with Helene Durosini on the design of Linux-Mandrake 10\"\n\nGood Mandrake news there!"
    author: "IR"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-12
    body: "Hard to find, eh?"
    author: "Debian User"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-12
    body: "..well let's just say I wasn't looking for any and I found ;)"
    author: "IR"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-12
    body: "Those who believe that if \"Linux\" or GNU/Linux wins, it is because it is more open that proprietary, should consider the following.\n\nWhat is Debian... an open structure allowing everybody including companies to contribute.\n\nWhat is Mandrake... a closed structure allowing nobody but selected people to participate.\n\nOne day, not only Turbo Linux and Caldera Linux will be gone, but also Suse, Mandrake and possibly even Redhat .... and there will be only Debian alikes... even if one of them may be called Gentoo....\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "Ah man, your attitude is as disgusting, ignorant and shortsighted as can be.\nActually, it has come to my attention that a lot of debian users behave in the same way as you do... Could be wrong ofcourse, they're not all as shortsighted as you, and i think a lot of them are embarassed when they read your braindead remarks... FFS man, get a life, go outside for a while, and get back to the real world, stop being a \"debian-rules-and-all-the-rest-sucks\"-nerd. Just my 2 cents"
    author: "Anonymous"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "Don't read it wrong, I was talking about the future, not what is right now.\n\nAs to myself, whover cares, few it may be: I recommended Mandrake more often Debian to people who ask me. For products we ship, I was in a 2 person team that ended up with Redhat, for the support.\n\nWhat bugs me is projects, companies begging for money. Mandrake is such one. Perl6 is another example.\n\nKDE is different. In KDE work people from many companies and more important, many independant individuals, and KDE doesn't depend on money. I like that.\n\nThe same is true for Debian. It's a distribution of greater complexity than Mandrake, needing more resources to develop, but it's all just there and getting only more. Talk of 1000 people, who in parts paid by HP, IBM, or just doing it for fun.\n\nMy rationale is, if people care enough for something, they should do it. If companies did it, they should do it.\n\nWhat I find wrong and what likely won't work, is companies beggin on people that care about something to give them money, so they do it. Over and over, and apparently it just gets burned, eh. not completely into the hands of people like Everaldo.\n\nIcons, artwork, etc. does Windows have so much themes because people get paid for it? \n\nIt may all be completely different though.\n\nKay\n"
    author: "Debian User"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: ">  KDE is different. In KDE work people from many companies and more important, many independant individuals, and KDE doesn't depend on money. I like that.\n\nDo you know who is working for KDE ?\nSuSE, Mandrake and other persons paid by Linux companies. Who will pay this persons ? This companies. Who will pay this companies ? Us by giving money buying products or services.\nFree software is great but like all other things it need money"
    author: "Shift"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "Do you know who in the kernel development is paid by companies?\n\nDid it take companies to get Linux to the point where companies wanted it to be better?\n\nFor KDE it's the same. Even without Suse, Mandrake and all of them paying people to contribute, the work will be done and is done.\n\nIt's just a matter of how quickly a critical mass is assumed, then such a thing takes off.\n\nKDE like Linux is becoming too important for society. Maybe even more.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "Look at the stats of the CVS of KDE and you will see if Mandrake and SuSE hadn't paid guys if KDE will be so good now.\n\nThe work will probably done but it will be done very more slowly because KDE guys need to had time and if they work for companies that don't want to pay them for working on KDE they won't have enough time to work on it\n\n"
    author: "Shift"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "\nI am not saying an open structure doesn't benefit from companies. I am just saying, that Debian-alike structures will be prevailing for this. Mandrake in itself is not like Debian and has too high costs to do something others in an open structure.\n\nCould you imagine a closed KDE development by donations to a company to really work?\n\nMandrake is participating in open development, but the Drake tools are also there, and why specifically, what will they contribute for GNU/Linux? Nothing in the end.\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-16
    body: "Dear Debian Troller,\n\nMandrake is developped in a open fashion (see cooker), and all the tools they developped have been released under the GPL. So their contribution to GNU/Linux is way bigger than your ridiculous trolls for ex.\n\nNow regarding their need for money, the point is that they wanted to take their time to do an IPO and once they were ready the .com bubble had already burst, so they couldn't benefit from the same amount of cash that a distro like RedHat had. The reason why they're not profitable as of today, is because they have to repay for for the dumb mistakes of the idiot.com management team that ventures capitalist had forced on them.\n\nBtw you forgot to say that another characteristic of debian is to be the distro with the greatest number of arrogant ignorant snobs using it."
    author: "dob"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-14
    body: "Yes I know (not personally) a few people who are paid to work on the Linux kernel.  Without the companies paying these people, Linux would progress a LOT more slowly.  While its nice to think of all OSS as strictly community and voluntarily developed with no corporate involvement, you'd only be delusional.  LVM2's development is sponsered at least partially by Sistina.  EVMS is developed by IBM.  XFS is developed by SGI.  ReiserFS's development has been sponsered by mp3.com and the US Govt.  Not to mention all the universities that receive govt and business money to operate who's had students and professors contribute to Linux.  Maybe before 1993 Linux's development fit your puratin OSS beliefs, but not now and probably never will again.  If you don't like it, that's fine just fork eveything and get people to develop it for free.  While its true that a lot of people get squat for developing KDE, but can you say that the paid KDE developers would be able to work on KDE as much as they can if they had to work at Cub Foods, Walmart, or even some programming job that forbid them from working on KDE except on weekends and nights?\n\nSome of the kernel developers who are paid: Linus Torvalds, Alan Cox, Ted Tso, Robert Love\n\nI use Debian and like it a lot and have since last summer (slack before that).  Generally, Debian has some fairly decent bandwith.  Who pays for that bandwidth?  I HIGHLY doubt that all the bandwidth Debian has is due to all the Debian users collectively paying for it.  If you do a traceroute to debian.org, you'll notice that it passes through a couple HP hops:\n11  ftcgwb01-p7-1.americas.hp.net (15.227.138.6)  79.943 ms  79.739 ms  80.372 ms\n12  ftceagw2.cns.hp.com (15.68.120.9)  80.362 ms  79.529 ms  79.863 ms\n13  gluck.debian.org (192.25.206.10)  83.541 ms  81.538 ms  82.475 ms\n\nSo you see, a large corporation is paying for debian.org's bandwidth.  Are you going to stop using Debian now?\n\nLinux and KDE are becoming too important for society?  Well that's a laugh, especially with KDE.  Do you think that if Linux and KDE disappeared, that society would be on the virge of collapse or what?  Have you heard of  BSD?  Did you know that, technically, FreeBSD is probably the most widely used *nix not Linux (Mac OSX).\n\nReading your comments reminds me of a sign I saw in a professor's office a few years ago:\n\"Please engage brain before using mouth.\"  Stop being such a OSS/FS/Bazaar puratin."
    author: "Super PET Troll"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-14
    body: "I meant \"too important\" as in, it just won't let it die. No matter what happens.\n\nThis means, Mandrake or not, Suse or not, KDE 4 will be out and better. If not Suse pays the people, others will.\n\nI know of FreeBSD, of course, but why do you think the technical inferior kernel is the one that prevails? Why did NetBSD have a fork to OpenBSD?\n\nIt's because of the closed structure and elitist development. Linux worked without a math coprocessor and low and behold even IDE. It's not that people didn't want to add it, the core development team thing, just never shared prepared releases.\n\nNowadays FreeBSD and the like are developed largely open.\n\nBut I didn't mean that society would collaps if Linux went away. I meant that society won't let it go away.\n\nAlso, as to companies supporting open structures, why not? HP has no control over Debian. HP wants to support Debian for its own benefit, so let them do it. The difference of HP and Mandrake is that HP is not asking Debian users for money.\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-14
    body: "Yes I know (not personally) a few people who are paid to work on the Linux kernel.  Without the companies paying these people, Linux would progress a LOT more slowly.  While its nice to think of all OSS as strictly community and voluntarily developed with no corporate involvement, you'd only be delusional.  LVM2's development is sponsered at least partially by Sistina.  EVMS is developed by IBM.  XFS is developed by SGI.  ReiserFS's development has been sponsered by mp3.com and the US Govt.  Not to mention all the universities that receive govt and business money to operate who's had students and professors contribute to Linux.  Maybe before 1993 Linux's development fit your puratin OSS beliefs, but not now and probably never will again.  If you don't like it, that's fine just fork eveything and get people to develop it for free.  While its true that a lot of people get squat for developing KDE, but can you say that the paid KDE developers would be able to work on KDE as much as they can if they had to work at Cub Foods, Walmart, or even some programming job that forbid them from working on KDE except on weekends and nights?\n\nSome of the kernel developers who are paid: Linus Torvalds, Alan Cox, Ted Tso, Robert Love\n\nI use Debian and like it a lot and have since last summer (slack before that).  Generally, Debian has some fairly decent bandwith.  Who pays for that bandwidth?  I HIGHLY doubt that all the bandwidth Debian has is due to all the Debian users collectively paying for it.  If you do a traceroute to debian.org, you'll notice that it passes through a couple HP hops:\n11  ftcgwb01-p7-1.americas.hp.net (15.227.138.6)  79.943 ms  79.739 ms  80.372 ms\n12  ftceagw2.cns.hp.com (15.68.120.9)  80.362 ms  79.529 ms  79.863 ms\n13  gluck.debian.org (192.25.206.10)  83.541 ms  81.538 ms  82.475 ms\n\nSo you see, a large corporation is paying for debian.org's bandwidth.  Are you going to stop using Debian now?\n\nLinux and KDE are becoming too important for society?  Well that's a laugh, especially with KDE.  Do you think that if Linux and KDE disappeared, that society would be on the virge of collapse or what?  Have you heard of  BSD?  Did you know that, technically, FreeBSD is probably the most widely used *nix not Linux (Mac OSX).\n\nReading your comments reminds me of a sign I saw in a professor's office a few years ago:\n\"Please engage brain before using mouth.\"  Stop being such a OSS/FS/Bazaar puratin."
    author: "re: Super PET Troll"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-15
    body: ">  What bugs me is projects, companies begging for money. \n\nHave a look at the KDE site and you will find where they are happy to accept support. Though we don't \"beg\" for money on the Quanta site we do ask. You can look on the KDE site at http://kde.org/thanks.html for Kitty Hooch to see that I sponsor Andras Mantia to work full time on Quanta, which is now part of KDE proper. I ended up putting my web business on hold but did not want the GPL'd Quanta to die so I took this action. Given the difficulty of starting up a business I'm undergoing personal sacrifice. I dropped digital cable and returned the DVD player I bought last year as well as sold off much of my dear mother's estate to sustain our start up... all while paying money out every month that was not easy to sponsor a project I didn't have to. I hope to be in a position to re launch a business around Quanta again this year.\n\nIn spite of the stress, difficulty and sacrifice I didn't complain or beg. However it seems to me that I should not be the only one footing the expense of this project. I figure that if everyone who uses Quanta sent me $1 every time they got a new major version I could have WYSIWYG, a Flash editor, a Gimp/Photoshop tool, full data management and a lot of resources and features added before the end of the year. In the final analysis your time has to come from somwhere, which means it's either money or, more important, family. We receive a barely noticable amount of code contributions and financial contributions, for which I am grateful for each.\n\nIt is frustrating when I create a system for storing templates and not a single one arrives from hundreds of thousands of users. Even more frustrating is seeing people with attitudes like yours suggesting that we ignore the financial aspects... Quanta Plus would be a memory instead of a celebrated application. \n\nDo you think I should stop employing Andras or should we just try not to think about it and let me quietly assume all the costs? In my opinion the problem isn't companies or projects asking for resource help (provided it will be used responsibly), it is that so few people ever step up to become a real part of the community by giving back where they are led instead of operating with a consumer mentality."
    author: "Eric Laffoon"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "> What is Mandrake... a closed structure allowing nobody but selected people to participate.\n\n?????\nSo explain why you can create rpm for Mandrake and propose them for the contrib CD. Explain why they had bugzilla and mailing-list. Explain why why you can vote for them rpm you want to see in the next distribution.\n\nFor some things Mandrake is more open than Debian. Just look like how much time it take to accept a stupid application like wmcoincoin to be accepted in Debian regarding to the time to be integrated in Mandrake :)\n\nMoreover, if you look like the Changelog of Gentoo you will see that it is more a Mandrake than a Debian ;)\n\nPS : Mandrake is the best </troll>"
    author: "Shift"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "s/look like/look at/ :)"
    author: "Shift"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "> Just look like how much time it take to accept a stupid application like *wmcoincoin* > to be accepted in Debian regarding to the time to be integrated in Mandrake :)\n \n\\o/ \\o/ \\o/ \\o/ \\o/ \\o/ \\o/ \\o/ \\o/ \\o/ "
    author: "houpla"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "COIN COIN WILL SAVE YOUR SOUL"
    author: "POULPY"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "To make something software part of Debian-alike, you need it to be acceptable on a consensus-base.\n\nElections are a majority thing. If the majority wants KDE, but not Gnome, will Mandrake stop distributing Gnome (do they anyway?).\n\nWhy not have an open structure that just does everything people really want?\n\nAnd reasons why Debian may be slower to add just another to their 10000(?) packages, is that they have a \"policy\". And not knowing the concrete case, I have trust, that a window manager is expected decent behaviour, even to enter unstable.\n\nYou see, Debian is the open structure of people who chose to restrict software availability under the Debian brand by quality, not so much by popularity. If you disagree, go to apt-get.org and find apt-sources for the other 10000 softwares...\n\nI personally do care if it's /usr/share/doc or /usr/doc/share .... many may not. But I expect more Debian-alike distributions to happen. Gentoo is one with technically different focus, somebody know others?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-16
    body: "Gentoo is as different from Mandrake as it is from debian.\nGentoo really isn't \"debian like\"\n"
    author: "dob"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-13
    body: "> So explain why you can create rpm for Mandrake and propose them for the contrib CD.\n\nMandrake sux, RedHat rox."
    author: "matiasf"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-14
    body: "I don't know why Debian and KDE don't charge for their product.  I would gladly pay a \"reasonable\" price for such quality product (at least KDE, never used Debian).  At least that way there would be some money coming in and could perhaps benefit the developers (the owners of the technology).  Working for free is not to last (at least when looking at the history of cccp).  The amount of talent in the Linux community is almost as staggering as the lack of business knowledge.  Buck up, make some money, this isn't a charity.  Stop supporting a bunch of crybabies demanding free software.  The developers of KDE deserve to be rewarded for their effort, so for what it's worth, I would gladly pay for your products, but I refuse to give money to charity (or buy it when you give it away for free).  "
    author: "Snackbar"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-14
    body: "> I would gladly pay a \"reasonable\" price for such quality product (at least KDE, never used Debian)\n\nThanks, you define how much is \"reasonable\": http://www.kde.org/kde-ev/about/donations.php"
    author: "Anonymous"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-15
    body: "I'm sorry, but it appears you understand very little here, beginning with the GPL.\n\n> I don't know why Debian and KDE don't charge for their product. \n\nHow many reasons do you want? Some people would rather code than manage a logistical nightmare and associated police procedures. Did you pay for every piece of software that the license required?\n\n> The amount of talent in the Linux community is almost as staggering as the lack of business knowledge.\n\nStack it up buddy. My business, Kitty Hooch, was part time until Q3 of last year and is targeting substantial income growth this year. Want to compare balance sheets in 12 months? Note also what is happening in Germany with Kmail.  I also sponsor a full time developer on Quanta... which could be argued is not good business. I guess you just have to decide if all business decisions have an immediate scrooge-like effect.\n\n> Buck up, make some money, this isn't a charity.\n\nWhat is charity? Maybe for some it is. Maybe it's my charity to bombastic users like you?\n\n> Stop supporting a bunch of crybabies demanding free software.\n\nI suspect you know little of business, but I'm positive you know nothing of free software. I have received many emails from people thanking me for empowering them to raise up and create income where there was little hope before. I'll take that over money any day!\n\n> The developers of KDE deserve to be rewarded for their effort\n\nI consider the thankyous more rewarding than any donation attached, though the latter shows commitment.\n\n> I would gladly pay for your products, but I refuse to give money to charity (or buy it when you give it away for free). \n\nWe gladly accept donations, but because a nice dinner out in the US is a month's earnings in other places I can't see punishing those with lesser income. In fact I can't see charging money for what I think should be available to all. I believe in equal opportunity. I was told in a Borland seminar they estimate as much as 80% of software is pirated. You can get any software for free so by your reasoning why pay for any of it... that leaves donations. \n\nIt's a nice catch 22 you offer. If you are so uncharitable as to refuse to support any cause or effort than frankly I feel sorry for you. What hooked me was how good it felt to to get those thankful emails every day. I gladly give to these people, and in doing so it is too much trouble to restrict you so as to force you to do what you would not do otherwise.\n\nThere is a selfish reason to offer support to what is offered for free... because it increases your benefit. Just as people would help their neighbors harvest fields, raise barns, rebuild homes... it's called community and it's where you do the right thing becuase it's important to you. What is important to me is insuring the freedom of unlimited access and opportunity to future generations where ever they may be. Viable alternatives to despotism are needed today. You want someone with jack boots to come get your money? Buy M$. You want a better future cough up to those who are building it. If you didn't care... why would you be here?"
    author: "Eric Laffoon"
  - subject: "Re: Linux-Mandrake 10"
    date: 2003-01-14
    body: "MWWWWWWWOAUAHAHAHAHAHAHAHA ( pov tache , vieux trolleur de mes couilles) \n\nplease read this and shut up :\nhttp://www.mosfet.org/noredhat.html\n\n"
    author: "CabaleXP"
  - subject: "Re: Linux-Mandrake 10"
    date: 2004-03-14
    body: " by matiasf on Monday 13/Jan/2003, @22:21\n> So explain why you can create rpm for Mandrake and propose them for the contrib CD.\n\nMandrake sux, RedHat rox.\n*************************************\nIF you knew anything, Mandrake was/is derived from Redhat. dumbass."
    author: "Jimmy"
  - subject: "Re: Linux-Mandrake 10"
    date: 2004-03-04
    body: "Remember, the benefit of Linux is that there are so many distributions to choose from.  Everyone has their own preference as to which one is better, FOR THEIR USE. Some find Debian better for what they use it for, others may choose Red Hat, Mandrake, Gentoo, etc. I choose my distro because it does exactly what I want it to do, with little effort.  One thing we all must realize is that it's not the distro that matters, it's the fact that we chose Linux over M$."
    author: "Nate Dogg"
  - subject: "size matters"
    date: 2003-01-13
    body: "to cut down on kde-look bandwidth problem, start by sizing down the preview pictures. When I was on rtc, the site was virtually unusable for previewing.\nAlso, have everyone host it's own stuff, host only small (in size) previews pics and text. We dont need the extra fancy stuff. Just provide us with a nice central location to FIND all the stuff.\nNow that I'm finished with the whining, a BIG sincere Thank YOU!"
    author: "jaysaysay"
  - subject: "Well..."
    date: 2003-01-13
    body: "I must admitt I was very sceptical when Everaldo's (Connectiva's) Crystal icon theme appeared on the kde-look.org.\n\nFirst, because a great part of the icons in the first releases of Crystal icon theme were already present in some other icon themes created for MacOSX, second, because some of the icons were variations of WindowsXP icon theme. I didn't like that kind of mixup. It looked weird to me.\n\nIIRC the first releases of Crystal icon theme were composed of 'crystalized' folders from iKons (Kristof Borrey's icon theme) + some MacOSX icons + some WindowsXP icons + some 'crystalized' kde icons from KDE 2xx hicolor icon theme.\n\nIIRC we had a meeting on IRC kde-artists were tackat & I agreed that Crystal icon theme was not usable as a new default KDE icon theme because it was too shiny, not original and a little bit disaster from the usability point of view. Qwertz insisted that Crystal should become the new default KDE icon theme (probably having in mind combination of Crystal and his Keramik windecoration which was at that time almost ready for the prime time). He also argued that Crystal had enormous popularity on kde-look.org. The rest is 'history'.\n\nShortly after Everaldo invited other icon creators to contribute to his Crystal icon theme which resulted in some good and some bad contributions, some nice icons and some ugly ones. The problem here was simply that Everaldo (as a maintainer) didn't say no to some ugly icons incorporated to Crystal icon set (some of them are still a part of the theme).\n\nCrystal icon theme really matured when tackat joined the 'team' ( I could easily recognize tackat's decisions and work) and probably at that time Crystal was enriched by some excellent icons made by floood (mostly application icons already published as WinXP icons in MacOSX style). A little (or maybe a big) problem here was that floood's icons had (and still have) a different color palette then the rest of Everaldo's icons (floood's blue & green colors are much more pleasant than Everaldo's ones).\n\nCrystal has obviously matured and it is almost completed in two segments: beautiful devices icons (I presume that was tackat's work) and filesystems icons except a little problem with trashcan icon which is not centered well and IMHO should be a little bit wider. \n\nApplication icons are still mixup of everything: different styles, different sizes, different color palettes, some are 'crystalized' and some are cartoonish. Konsole icon is wierd: it is the only icon in Crystal icon set that has funny perspective (and instead of black it is now blue).\n\nMime icons have 5 different papers as template, some of them are not centered well,  elements are not placed consistently (up and down, in the middle), and elements have very different sizes.\n\nI hope Everaldo, Tackat & comp. will fix above mentioned issues and finish the Crystal icon theme.\n\n-----------------------------------------------------------\n\nP.S. I am a little bit confused. Is Crystal really KDE icon theme? There are different sites on the web where the same icon theme has been promoted as Crystal icon theme for MacOSX, WindowsXP & KDE, or Connectiva icon theme or Everaldo's crystal icon theme.\n\nP.S.S. Background icon in Crystal is a minimized WindowsXP desktop showing WinXP taskbar & icondock for WindowsXP. Wht not KDE's kicker?\n\nCheers,\n\nantialias\n\n\n\n\n\n\n"
    author: "antialias"
  - subject: "Re: Well..."
    date: 2003-01-13
    body: "I think you are correct on the perspective point. However, I must say that I really do like the new Konsole icon. It's very well drawn and communicates the idea of a shell better than the old one which conveys the idea of a monitor. And if one has never seen an LCD screen, it will be even more confusing.\n\nThere are still missing icons for Mail, and a couple of other filesystem types.\n\nIIRC Crystal was originally a KDE thing but other people using other systems liked the icons so much they ported them to Mac, Windows, GNOME, etc.\n\nYour point about the mimetypes is also on the money. Crystal-SVG definitely needs more mimetypes and more consistent. Foood's icons are indeed very nice, but I think they do not fit with the Crystal-SVG theme very well. I'd much prefer they have their own beautiful theme rather than be in Crystal."
    author: "Antiphon"
  - subject: "Making Crystal bearable :-) "
    date: 2003-01-13
    body: "Here's a tip for those who like crystal for their completeness - but dislike them for their colorfullness.\n\nControlCenter | Appearance | Icons | Advanced:\nSet \"To Gray\" effect as Default, choose amount of \"greyishness\" according your taste. Viola!\n\nPS: I used to reduce the color of that icon set from day to day, until someone asked why all my icons are grey ;-)"
    author: "Hafer"
  - subject: "Re: Making Crystal bearable :-) "
    date: 2003-01-13
    body: "Wow, thanks a lot. This really boosts my desktop experience....\n\nGreat stuff this KDE :-)\n\nKay"
    author: "Debian User"
  - subject: "Re: Making Crystal Grey:-) "
    date: 2003-01-24
    body: "If you tend to prefer grey icons...check out \"Steel Icons\", an icon theme of mine that's metal-themed. (hey, its no comparison to everaldo's crystal, but it *is* grey...:-)"
    author: "Doches"
  - subject: "Color schemes"
    date: 2003-01-13
    body: "I think we all have it wrong wrt the color scheme. As some of you may remember, I am maintaining a green version of Crystal. I should not have to be doing this, however, because now that we are going to move to SVG, why not make it so that the user has the ability to choose what the basic icon colors will be. That is, an option in KControl to choose the primary color (in my case, emerald green), dark color, and light color.\n\nI'm not a programmer but if we could do this, it would be something that AFAIK has never been done.\n\nSure some people would make their desktops look like crap but imagine the possibilities: organizations could make all of their systems use the same style and icon colors (great for the corporate desktop), users would get more control (great for the blind or color-blind), and KDE would further distinguish itself as the Number 1 operating environment.\n\nThe idea would be somewhat along the lines of what we already have, just make it so that certain hues will always be drawn the options the user has specified."
    author: "Antiphon"
  - subject: "Re: Color schemes"
    date: 2003-01-21
    body: "Now that is an interesting idea.  I hope they consider it."
    author: "CY"
  - subject: "Iconsets"
    date: 2003-01-15
    body: "For everybody who wants to have a quick look:\n\nthe new KDE3.1 iconset:\n http://www.everaldo.com/images/ico3.jpg\n\nthe noia iconset:\n http://skins.stardock.com/Carlitus/ip/noiaip200prev.jpg\n\n\nThey both look beautiful to me...\n   "
    author: "Jens Henrik"
  - subject: "Re: Iconsets"
    date: 2003-01-16
    body: "I'm in love with the noia (which means girl) iconset :p\nThey look soooooo cooooool!!!!!! \nThey should be included ASAP in kdeartwork"
    author: "Anonymous Coward"
  - subject: "Re: Iconsets"
    date: 2003-01-18
    body: "Sorry, thats not the 3.1 icon set but the \nsvg icon set you linked to. Svg is not\ngoing in to 3.1 maybe 3.2?\n\nhttp://svg.kde.org\nhttp://www.everaldo.com/splashs/icons.html"
    author: "Oops.."
---
<a href="http://www.ofb.biz">Open for Business</a> features an <a href="http://www.ofb.biz/modules.php?name=News&file=article&sid=190">interview</a> with Torsten Rahn and <a href="http://www.everaldo.com/">Everaldo Coelho</a>, the famous icon duo of the <a href="http://artist.kde.org/">KDE artist</a> world, and creators of virtually all of the default icons you see in KDE. The <a href="http://www.ofb.biz/modules.php?name=News&file=article&sid=190">interview</a> discusses how the new <a href="http://www.kde-look.org/content/show.php?content=2539">Crystal icon theme</a> came about, how it ended up replacing Tackat's HiColor theme, and the overall importance of icons to the enterprise desktop.  <i>"In 2001, Frank Karlitschek put up his web page "KDE Look". This boosted KDE artwork contributions beyond belief. A lot of fresh new <a href="http://www.kde-look.org/index.php?xcontentmode=iconsall&xsortmode=high">icon themes</a> appeared and I saw the chance to get the most active people involved with the work on the default icon set."</i>  Speaking of <a href="http://www.kde-look.org/">KDE-Look.org</a>, you may have noticed recent slowdowns.  The site is <a href="http://www.kde-look.org/news/news.php?id=40">in need</a> of new hosting or donations.
<!--break-->
