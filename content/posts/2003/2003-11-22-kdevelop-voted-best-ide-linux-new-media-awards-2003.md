---
title: "KDevelop voted \"Best IDE\" in Linux New Media Awards 2003"
date:    2003-11-22
authors:
  - "hfernengel"
slug:    kdevelop-voted-best-ide-linux-new-media-awards-2003
comments:
  - subject: "Not a Surprise Really"
    date: 2003-11-22
    body: "KDevelop outclasses (and eclipses!) other IDEs by a country mile."
    author: "David"
  - subject: "Re: Not a Surprise Really"
    date: 2003-11-23
    body: "Really? I can send a bunch of MS zealots at you who will claim MS Visual Studio blows KDevelop out of the skill and flame you down. ;)"
    author: "Anonymous"
  - subject: "Re: Not a Surprise Really"
    date: 2003-11-24
    body: "Funny! Bring them on. I'd like to see any MS idiot who has even heard of KDevelop let along used it. If they are going to flame then the least that I ask is that they have uses it and seen the features.\n\nSeriously, the Visual Studio IDE is good, and always has been, (can't really compare to Borland and others though) but in terms of outright flexibility, extensability, polish, not doing all those annoying things VS sometimes does and of course price it really does win."
    author: "David"
  - subject: "Re: Not a Surprise Really"
    date: 2003-11-24
    body: "Slip!!!! KDevelop wins that is. Watch your grammar."
    author: "David"
  - subject: "Re: Not a Surprise Really"
    date: 2003-12-02
    body: "the title was 'KDevelop voted \"Best IDE\" in Linux New Media Awards 2003'.  was MS there?  not to mention the fact that MS users make false claims all of the time."
    author: "Anonymous"
  - subject: "That old thing? ;)"
    date: 2003-11-22
    body: "It could be worth pointing out that the awarded version of KDevelop is quite limited in comparison to the (about to be released) KDevelop3 version.\n\nThe 2.x version was mostly developed years ago and while quite stable and refined (was it the first KDE app to have a seperate setup program?), it hasn't seen significant changes for probably two years. Since about a year all development on it has stopped in favour of the new version.\n\n\n\n"
    author: "teatime"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-22
    body: "There is no hint about the awarded version. And afaik the KDevelop team is everyone telling to use KDevelop 3 versions instead of ancient 2.5 since the first alpha versions."
    author: "Anonymous"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-22
    body: "True, but I find it very unlikely that they would rate alpha versions in these kinds of evaluations."
    author: "teatime"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "You cannot base an assertion on high probability. Or, to be more precise, you can do it, as long as you say \"I think\" or \"I guess\". But then again it won't be an assertion... ;-)"
    author: "Gr8teful"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "That's why he said \"I find it unlikely.\" Its semantically equivalent to \"I think it is unlikely.\" "
    author: "Rayiner H."
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "I think ( ;-))the anonymous was refering to the first post of the thread, where he does not say \"I think\"."
    author: "oliv"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-22
    body: "Yeah its a good sign when our old tech is still better than their \"modern\" stuff :)\n\nI'd like to personally thank Jens, Harry, Roberto and the hundreds I forgot for making me an awesome development platform to use on Linux.  I in fact use KDevelop not only for my KDE stuff, but also for my python and Java development.  While its not polished completely yet, I can expect from the current progress that we will have a very powerful cross language IDE.\n\nIt should ALSO be noted that because of KDevelop3's modular design you can have as MUCH or as LITTLE KDevelop as you want.  Dont want peices turn them off.  Want more turn them on.  Truly a developer's IDE IMNSO ;)\n\nAlso why isnt anyone cheering the RegExp Debugger?  That alone is worth the KDevelop download!  \n\nOne last thing, got a bug?  Run dont walk to http://bugs.kde.org get those reports in so they get fixed.\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "I started playing with it this weekend.  It's remarkable how many features it has, and it does help Standard C++ newbies such as myself.  But I wonder how stable it really is, when I select \"Delete class\", type in the wrong classname and the IDE transports itself to a back planet in the Twilight Zone.\n\nI'd personally like to see AutoComplete.  My conscience keeps telling me \"Do the work in MsDev and compile it on the Linux machine!\"\n\nJohn Reynolds"
    author: "John Reynolds"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-25
    body: ">> My conscience keeps telling me \"Do the work in MsDev and compile it on the Linux machine!\". <<\n\nIf that really is what your conscience tells you, man, I doubt you have any... ;) LOL -- sorry, just couldn't resist...\n\nCheers,\nFlexx\n\n"
    author: "Flexx"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-23
    body: "> The 2.x version was mostly developed years ago and while quite stable\n\nquite.. I was using it for a long time, but now it started to crash every now and then. Guess I shouldn't bother to do any bug reports, since they are working on the 3.0 (just finished compiling it myself), I'll just switch to that and be happy.\n\nFor those who are interested, it crashed when I switched back to the KDevelop window from Kicker, after being on some other window. Maybe someday I'll have the time to find the bug."
    author: "Tommi Uimonen"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "phew. few minutes with gideon (cvs) and one backspace in code window crashed the beast. Back to emacs & make for now... :("
    author: "Tommi Uimonen"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "Huh? You crashed it with a backspace? We would be interested in that backtrace!\n\nWas this a fresh install from cvs, or had you built it before? If the latter, please make sure you run 'kdevelop', not 'gideon' (the binary changed name a month or so ago, but the old one, if still installed, still sorta works..)."
    author: "teatime"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "It was fresh cvs install, run with kdevelop. I don't have gideon in my computer.\n\nI still have the shell window contents of the kdevelop output what it gave when still running, just before the crash:\n\nKate (View): slotRegionVisibilityChangedAt()\nkdevelop (cpp support): =======> compute current function definition\nKate (View): slotRegionVisibilityChangedAt()\n\n\n(I configured with just ./configure, so I don't know if this is debug output or what.. Maybe I should reconfigure with --enable-debug=full to get decent debugging output)\n\nKde gave short backtrace, I didn't save it, but it was not very informative.\n\nI was browsing code, pressed the little minus box to hide one {...}, and then backspaced the one { that was still left and it crashed.\nI tried the same manouver again, but now it behaved correctly; just reopened the {...} and moved the brace to the previous line\n\nbtw. Now when fiddling around with the {} hiding feature, once it left the both braces there after hiding. I have a picture as an evidence. This has happened from time to time, not always reproducible.\n \nhttp://www.hut.fi/u/tuimonen/evidence.png\n\nMaybe I should start using the bug tracker.. :)"
    author: "Tommi Uimonen"
  - subject: "Re: That old thing? ;)"
    date: 2003-11-24
    body: "Hmm.. sounds like you're hitting an editor bug, most likely in katepart. It's a little hard to tell from the screenshot.\n\nIn KDevelop3 even the editor is a plugin(KPart). The default is to use the editorpart used by Kate. I haven't seen katepart crash in a long time though, and then it was a fairly shortlived problem in CVS HEAD. \n\nTo be clear: Syntax highlighting and code folding is done by the embedded editorpart. Problems here are best reported to the editor developers.\n\nAnd yes, please use the bug system. :)\n"
    author: "teatime"
  - subject: "Congratulations"
    date: 2003-11-22
    body: "I would just to congratulate the KDevelop team. Both the old one (including Sandy & Bernd) and the current one including so many great developers. It is one of my favorite applications. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Very pleased"
    date: 2003-11-22
    body: "I am very happy with KDevelop, while I have not tried the development version of KDEvelop, just KDEvelop 2.1.5, but it is the best I've seen. I'm also sure that I didn't use many of its features, because I'm just learning, but what I did use I liked =)\n\nIMO, development tools are one of the most important things for KDE. We can't beat GNOME on acquisition costs for closed source companies due to the license, but we can make up through much better tools, faster and flexible development. Through an unmatched backend and sets of tools, this is where we can win and we are ahead already. It is also the way to get new developers, even developers like things easier, and they will go for the best tools."
    author: "Alex"
  - subject: "Re: Very pleased"
    date: 2003-11-22
    body: "> even developers like things easier, and they will go for the best tools.\n\nAnd the best tools makes you more productive.\nThat is, gives more money back for your investment in the developer."
    author: "OI"
  - subject: "vim?"
    date: 2003-11-23
    body: "i played a little bit with kdevelop3 recently. the new IDEA interface kicks ass (much place for the code even in small resolutions AND fast access to all important functions), code completion seems to work fine, debugging is nice and powerfull.\n\nbut is a real support for vim planned? i know vimpart can embedd kvim, but are the important features like codecompletion (this dropdown-menu from kdevelop, not vims strg+p), small things like setting (and displaying) breakpoints etc. planned for the future? now it seems not very good integrated and therefore not usable.\n\n\n "
    author: "L1"
  - subject: "Re: vim?"
    date: 2003-11-23
    body: "Other than bug-fixing, the original authors of kvim and vimpart have pretty much abandoned these projects.  Instead they are focusing on developing a new vim-like editor: yzis (http://www.yzis.org/).\n\nI'm not sure how far along yzis development is, but hopefully we'll soon have a fully integrated vim clone working with KDE."
    author: "Anonynnous Coward"
  - subject: "Re: vim?"
    date: 2003-11-24
    body: "yep I am working on that :)\nIt is slowly developed because I am mostly alone coding so any help would be welcome (developers, patches, comments, ideas, beers, pizzas ... :)\nFor now, we have the basis, there are some design issues to improve still but it's about to become quite usable soon.\nUnderstand it clearly, we don't mean to replace vim in a near future, we just want to make it as we like it ;)\n\nMik"
    author: "Mickael Marchand"
  - subject: "Congratulations"
    date: 2003-11-23
    body: "Congratulations to the developers. On a personal note, Kdevelop 3 has made my working life that bit more tolerable :)"
    author: "Stephen Douglas"
  - subject: "htsearch"
    date: 2003-11-24
    body: "I've never gotten lookup-in-documentation to work. Where do I get the htsearch executable?"
    author: "ac"
  - subject: "Re: htsearch"
    date: 2003-11-24
    body: "Install htdig from www.htdig.org.\n\nIf you don't have the root password for your machine you'll need to pass a heck of a lot of options to configure when compiling it to make it install its files and, importantly, the htsearch executable to somewhere you can write to."
    author: "Andrew Coles"
  - subject: "KDevelop doesn't work for me !"
    date: 2003-11-24
    body: "I have used KDevelop 2.x stable series for a year, then they have started working on KDevelop3. When they have started, I have noticed that they were moving on without a good definition of final IDE. I think Kdevelop3 is a software piece which is designed visually as a GUI rather than a software with an underlying concept.\n\nOther IDEs or editors like Eclipse, MS Visual C++ and Jedit, have strong concepts and extension languages like beanshell and visual basic scripts.\n\nFor me kdevelop looks an inflexible solution which can not be extended easily.As a C++ developer, I see everyday more and more reasons to customize my IDE ( currently I am using Jedit with lots of plugins and beanshell extensions )\n\nAdditionally, long time ago I have considered working on the code for KDevelop to add features, but I definitely didn't like to the code, it was under-documented and didn't look a piece of code which is robust and worth time investment.\n\nAnother aspect was that the development is very slow. What they are trying to achieve is already achieved by some other tools and the only good thing is that it is native KDE/Qt/Linux and it is fast and comfortable.\n\n"
    author: "Mderdem"
  - subject: "Re: KDevelop doesn't work for me !"
    date: 2003-11-24
    body: ">For me kdevelop looks an inflexible solution which can not be extended easily\n\n> ... I have considered working on the code for KDevelop to add features, but I definitely didn't like to the code, it was under-documented ... \n\n\nYou have got to be kidding...\n\nhttp://developer.kde.org/documentation/library/cvs-api/kdevelop/html/index.html"
    author: "teatime"
  - subject: "Re: KDevelop doesn't work for me !"
    date: 2003-11-24
    body: "While you where busy telling us how much cannot be done we did it. Please look at KDevelop 3.0 for a taste of things to come."
    author: "Ian Reinhart Geiser"
  - subject: "how to customize make options"
    date: 2003-11-29
    body: "I'm doing a project that uses libmxl, and currently I have to modify the Makefile by hand to get the 'xml2-config --cflags` and `xml2-config --libs` added to INCPATH and LIBS, respectively.\n\nI tried to look around the project options, but couldn't find any solution.\nThe documentation lacks also this kind of information.\n\nAny ideas?\n\n(This might not be the best forum to ask around, but just don't feel like subscribing to mailinglist yet)"
    author: "Tommi Uimonen"
  - subject: "Re: how to customize make options"
    date: 2003-11-29
    body: "Ok. Since this is imported as Qmake project, it has no \"Compiler options\" under \"Project options\"\n\nNow I should figure out how to import it differently."
    author: "Tommi Uimonen"
  - subject: "Re: how to customize make options"
    date: 2003-11-30
    body: "Here is tutorial about autoconf and example to get the xml-libs included\n\nhttp://www.kdevelop.org/index.html?filename=tutorial_autoconf.html"
    author: "Tommi Uimonen"
  - subject: "Undeserved"
    date: 2003-11-30
    body: "TBH, this is undeserved. Kdevelop *is* a good IDE, but it really doesn't hold a candle to Eclipse, or, among proprietary IDEs, IntelliJ.\n\nI think the developers would do well to develop a large or medium sized project in one of these two IDEs, and take a few notes along the way. Be honest - there is no way KDevelop can be said to be leading the pack. Eclipse should certainly have won this award by a mile. \n\nGideon is great on the multi language front, and has a nice configuarable interface and debugger. Its great that it fits in very well with the KDE desktop and supports languages like shell, perl and haskell. \n\nBut a modern IDE needs real, working, type and context aware code completion or intellisense or whatever you want to call it. I've never been impressed by the code completion in CVS gideon (on the occasions it has worked at all).\nIt is fast becoming a requirement of the IDE to perform refactorings with minimal intervention. Once you have used these features, you will get pretty annoyed at their absence. \n\nUnfortunately, these things aren't quick hacks. They will take a lot of time and effort. But hopefully things can come together, maybe some of the stuff going on in Umbrello will be integrated.  \n\nSorry. But it had to be said. "
    author: "rjw"
---
We're pleased to announce that <a href="http://www.kdevelop.org/">KDevelop</a> took <A href="http://www.kdevelop.org/index.html?filename=awards.html">first place</a> in the fourth annual <A href="http://www.linuxnewmedia.de/en">Linux New Media</a> Awards with 29.4% of the votes in the category of best IDE development system; second and third places went to Eclipse and Anjuta respectively.
The jury consisted of the editors of the German <a href="http://www.linux-magazin.de/">Linux-Magazin</a> as well as authors, industry leaders and members of the Open Source Community, including kernel hacker Alan Cox and the president of Linux International, Jon "Maddog" Hall. The full article (in German) <a href="http://www.linux-magazin.de/Artikel/ausgabe/2003/12/award/award.html">can be found here</a>.
<!--break-->
