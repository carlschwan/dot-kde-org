---
title: "The People Behind Quanta Plus"
date:    2003-04-22
authors:
  - "numanee"
slug:    people-behind-quanta-plus
comments:
  - subject: "VPL"
    date: 2003-04-22
    body: "Hi all,\n\nI used to maintain the WYSIWYG editor project Kafka, and I just thought I would give a pat on the back to the hackers who are adding this support ot Quanta. I was contacted in the early days about this support to Quanta and decided to approach it in a seperate application as I didnt want to clog Quanta with it.\n\nAfter some hindsight, WYSIWYG support in Quanta is a good thing. Although many professional coders dont use this, it will open up the application to those people who are less hardcore and just want to put some static pages on the web.\n\nWYSIWYG support in Quanta is going to be a hard task, and I had a hell of a time with it in Kafka. It is certainly not impossible, and some advice from the KWord team may be helpfull in coding the conversion of events to code. I wish the team all the best and maybe one day I will join and get involved again. My time for KDE has been limited somewhat as I have moved onto other projects and commitments.\n\nGood luck chaps and I look forward to running your code. :D\n\n  Jono Bacon\n\nPS - One hint. Dont use the acronym VPL. Here in England a lot of people refer to this as a Visible Panty Line; i.e, when you can see a young ladies knickers through her skirt. I would suggest a different acronym or the kind of hilary that was associated with kant might happen again.\n\n"
    author: "Jono Bacon"
  - subject: "Re: VPL"
    date: 2003-04-22
    body: "LOL @ VPL..  I vote we keep it.  :)"
    author: "KDE User"
  - subject: "Re: VPL"
    date: 2003-04-22
    body: "I have to admit I wanted something besides WYSIWYG for two reasons. One is I was really getting sick of typing it over and over... the other is all the negative connotations in how is has been poorly implemented in the past that I wanted to distance myself from. Having this other meaning brought to my attention... well this sounds like a great opportunity for a risque advertising campaign if we were a commercial project. Now I'm going to laugh every time I save time typing. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: VPL"
    date: 2003-04-22
    body: "I felt rather fortunate when Andras and I had been talking about visual page layout and Nicolas dropped the Kafka part in our lap. Of course Jono was leading this project some time back and we had discussed if we might have some collaboration. While there is still a fair amount of work to do I certainly want to acknowlege Jono for his efforts. Also he did an amazing job laying out the project. Any project would be fortunate to have his efforts. I think one key here is that the huge amount of work we have put into Quanta in the last two years makes it a vastly enhanced platform that solves a lot of problems on the back end for the kafka part. So it is, after all this time, a perfect marraige. I'd like to say I planned it that way, but I can honestly say at one time I did and it has been in a general sense my plan to round Quanta out with this feature.\n\nI would not say most serious developers will not use it either. We're really working to provide an implementation unlike anything in the past that is compelling and functional in ways that have not been previously available. We've got some very good people on this.\n\nIt's really nice to hear from Jono on this. Thanks Jono."
    author: "Eric Laffoon"
  - subject: "Re: VPL"
    date: 2003-04-22
    body: "Thanks very much for your kind words Eric.\n\nMy work on Kafka was an effort that was essentially limited by time and experience. I had done some development on other applications for KDE, but my lack of knowledge of XML and DOM in Qt and KHTML made the learning curve steep. Although my knowledge has furthered since then, I am glad I did not litter the Quanta tree with my learning curve. ;) Luckily, the developers behind Quanta have got some solid experience in this area, and the likelyhood of adding a WYSIWYG component is far better given the qualified structure and backend of Quanta.\n\nI am really impressed with Quanta and the direction it is taking. I think few people are fully aware of the struggle the Quanta project has faced over tha past years and Eric is one guy that I would truly love to meet and buy him a beer. There are some Open Source developers who *really* make a difference and Eric is one of them.\n\nQuanta has gone from being a basic web editor to one that is making real waves on functionality. I for one use it every day for PHP / HTML / XML / XSL development, and I think the project really is going to be a killer app for Linux when the WYSIWYG component is added.\n\n  Jono\n"
    author: "Jono Bacon"
  - subject: "Re: VPL"
    date: 2003-04-22
    body: "Wow! I'm blown away. You're welcome for the kind words Jono. I'm afraid I'm going to have trouble coming up with something as glowing as this. ;) You're on for that beer. For a guy I had a bit of a freindly rivalry with I'm pretty much speechless. I've always felt a bit like an outsider trying to fit in with all these really cool developers like Jono. What can I say... I'll take that beer. It's a ways from Oregon to GB but I hope to be able to manage the trip to Nove Hrady in August. I figure I can be the lost american who looks up to a bunch of guys half his age and tries not to look stupid. ;-)\n\nWhenever I see you Jono you're on for the beer, and I will be looking forward to it at least as much as you. If you ever get an itch to do some coding on a good KDE app drop me a line. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: VPL"
    date: 2003-04-22
    body: "Well the acronym is international I believe, in France we say LSV : Ligne du Slip Visible, most of the guys love it, as it's rather suggestive, amd make the imagination very active :)"
    author: "LSV"
  - subject: "Re: VPL"
    date: 2003-04-23
    body: "actually calling it vpl might help encourage people to make donations to the project. *grabs credit card* ;-)\n\n"
    author: "macewan"
  - subject: "Re: VPL"
    date: 2003-04-23
    body: "ROFL\nI had no idea... I'll have to order some lacy panties from Victoria's Secret and then print \"Quanta with VPL\" on them to hang from your car mirror for our next merchandise item. ;-)\n\n(Actually I told my wife I would not order from them any more because all I ever got was lingerie and that's not what the picture showed!)"
    author: "Eric Laffoon"
  - subject: "Catnip and Quanta"
    date: 2003-04-22
    body: "\nI love Quanta and Cats.\n\nI don't understand too much about HTML, CSS and programing but I did a great site about enviromental education with Quanta. Eric and Adr\u00e1s, thanks for this great free software.\n\nI had, sometime at the past, 12 black cats, Bombaim. That time I was living at rural area.\n\nBut about the catnip, this is a plant that makes the cats get high, go on nuts, hallucinated. Whoooouuu mannnn meowww !! =8-D\n\n"
    author: "V. B."
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-22
    body: "Cats are great! They have a very interesting social structure. They are very smart and contrary to myth can be trained well and are quite loyal. However they abound in personality and individuality. They can tell time without a clock and are great alarm clocks as well as excellent companions. It's funny that I have a business based on those little furry beasts. I love 'em.\n\nWhen you talk catnip though you have to keep Kitty Hooch seperate. Kitty Hooch is grown in a climate controled environment with as much organic nutrient as is possible to to affect their growth. In fact it's downright freaky! They're only supposed to grow 3 feet tall (1 meter) but I have pictures of me harvesting flower buds from 8 foot (3.7 meter) plants! Kitty Hooch is Catnip++.\n\nLast weekend we sold catnip toys to several visitors from Germany and we have had our toys go as far away as the UK, New Zealand and Isreal. Kitty Hooch, like Quanta, is a lot of fun. It's the other \"accidental endeavor\" I'm becoming world famous for. I think I must be one lucky guy. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-22
    body: "I don't thing that you are just \"lucky\" Eric :), I think that you do your job or your hobby and talk about them with a lot of _passion_, and its shows !!, a french philosopher used to say \"Nothing great can be achieved without passion\""
    author: "Khalid"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-22
    body: "That's what I like about KDE people... they're smart. When I was a top performing sales manager in my region one of my mentors told me that he believed my success was due to my passion. I have seen people who were total failures at everything find one thing they were passionate about and achieve fame and fortune... just for doing what others did with more passion. \n\nI decided long ago to approach whatever I did with passion. My life is not always easy and I think a lot of people could not handle the uncertainty and sacrifice often required to be self employed. I don't really believe in luck. It's just an expression. Still when I reflect that I spend most of my time doing things I love and my work makes a difference to thousands of people... It is pretty amazingly good considering I fell into both Kitty Hooch and Quanta. That's no lack of good fortune."
    author: "Eric Laffoon"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-22
    body: "Well, Eric, I was thinking about helping you out with Quanta for some time. I am not a web developer, nor a quanta user, but I respect what you are doing for free software. Software prices should decline with time, since there are no marginal production cost, but we are seeing unfair prices for software that have not changed a lot in the last ten years.\nDon't get me wrong, I am a liberal (in the european laisser faire, laisser passer sense, not in the american sense), small state, etc... I just think the prices are unfair, ando more, that people are unfairly transfering income to market monopolies. So I think this (helping free software) is the right think to do, it is cool, it help the small companies around the world, avoid income transfer from the poor to the rich, it make sense in economical terms (Inovative commercial software will allways have it value. But non inovative commercial software will have trouble with free software. Free software will allways have limited resources. Most of us are here for the free ride.\nI could give money for other projets or developers, some of them more useful to me. But i think you and Quanta deserve it most.\n\nKeep the great work.\n\nCarlos\n"
    author: "cwoelz"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "Hi Carlos,\nthat's very well put. I think some people might initially get the idea I'm anti-business and then be confused that I'm pro business. I just believe like you in equitable business and against monopolistic wealth transfers. I can tell you what store markups are on products and what reasonable costs are. Unfortunately commercial software seems to never want to get real. If Quanta were sold at $5 a copy (not practical because of the GPL) and we had near our current user base I'd be a millionaire. Go figure!\n\nI would not change things if I could though. I believe in free software. I also believe in people like yourself that will take action to do what they believe is right in their heart without the software being held hostage. There are hundreds of KDE programs alone. Free software is good. The alternative would cost me more than Quanta has. ;-)\n\nThanks for your kind words."
    author: "Eric Laffoon"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "\"Just moment ago I finished putting together a 50 page annual report - I decided at the very beginning of the project to give Quanta a shot; I knew I was in for a lot of copy and paste, I've been working with vi for ages and had a feeling that I may be able to save time by taking this on using Quanta.\"\n\nIts on /.  already.  I hope you have a steady heart to deal with it.  LOL.\n"
    author: "KDE User"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "SlashDot has to have become the cesspool of the internet. I've never seen a bunch of high minded whiners that need to do laps around the video arcade and so bad in my life. Intelligent and civil conversation seems to be frowned on there replaced by schoolyard banter presented as if there were some superior intelligence involved. I guess it's the initial rush of testosterone that sparks all the acne... I just have little use for it since it is now the cool place for teen aged windoze users to preen and pontificate. Perhaps I should change that to deficate because that seems to be the general content.\n\nI couldn't find your post but I now know I'm an idiot for not writing Quanta in GTK2, for writing it to begin with, for not settling for a console app, for not chaning it's name, for introducing Kommander... I did not check below the threshold for eating, talking, breathing or whether the vote was in on whether I should be allowed to procreate.\n\nThanks for reminding me why I don't go there. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "It's not my post but you can see it here\nhttp://developers.slashdot.org/comments.pl?sid=61610&cid=5784901"
    author: "KDE User"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "http://developers.slashdot.org/comments.pl?sid=61610&cid=5784924\nhttp://developers.slashdot.org/comments.pl?sid=61610&cid=5785546\nhttp://developers.slashdot.org/comments.pl?sid=61610&cid=5785554\nhttp://developers.slashdot.org/comments.pl?sid=61610&cid=5785939\nhttp://developers.slashdot.org/comments.pl?sid=61610&cid=5784860\n\nSome of it is good too."
    author: "KDE User"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: ">  Some of it is good too.\n\nHmmm... it's like they somehow set those to below the threshhold and focus the annoying ones at me. Well I have to say I may have been overly harsh in my review. I still find the noise annoying but apparently not all the adults have left the building. My apologies. There are some very nice comments here, including one from one of our contributors today. I would certainly not want to insult them most of all.\n\nBTW I do vastly enjoy the sanitized digest you provided. I think you have a real marketable service going there. ;-) dotsanislash.org filtered content... I like it.\n\nThanks"
    author: "Eric Laffoon"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "<quote>\n If Quanta were sold at $5 a copy (not practical because of the GPL) and we had near our current user base I'd be a millionaire. Go figure!\n</quote>\nIf Quanta were sold at $5 a copy you wouldn't have got the current user base by far. There's a difference between knowing the path and walking the path. Go figure :)"
    author: "xxl"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "> If Quanta were sold at $5 a copy you wouldn't have got the current user base by far. There's a difference between knowing the path and walking the path. Go figure :)\n\nTrue, but forgetting the logistical licensing issues and my personal convictions it does server to illustrate how far afield the commercial model is when competative packages go for $50-$500. Giving away version 1 of a piece of software to get the user base and charging $20 for a lifetime upgrade could yield more revenue than $50 and $35 for each upgrade. The current model is based on small market penetration and hoping to strike it rich with a big hit. It's not just M$ that is promoting free software by broken assumptions.\n\nBTW since Quanta can be updated by XML, any scripting language and Kommander dialogs it essentially would not be able to sell upgrades as it's user extensible. These are other design considerations that commercial software vendors can't afford because it weakens their posture to get more money from their best market, existing customers."
    author: "Eric Laffoon"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "Make it shareware. IT is legal, you know ;-)\n\nOk, it is not legal to say \"use it for 30 days or register\", but a copyright notice (and donation request) on startup would not be out of line, and would be covered by the GPL as not removeable, IMVHO.\n\nIn fact, I have been meaning to do that, just to see if RMS finally blows a fuse ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Catnip and Quanta"
    date: 2003-04-23
    body: "> Make it shareware. IT is legal, you know ;-)\n\nOh common... give me a break. I was just trying to illustrate a point about commercial software.\n \n> Ok, it is not legal to say \"use it for 30 days or register\", but a copyright notice (and donation request) on startup would not be out of line, and would be covered by the GPL as not removeable, IMVHO.\n\nHmmm... we have a donation menu item in the help menu. I'm betting it's been sanitized in at least Red hat. Including it in the copyright notice and flashing it at startup had not occured to me. ;-)\n \n> In fact, I have been meaning to do that, just to see if RMS finally blows a fuse ;-)\n\nOh yes... I can see you doing this. ;-) Please make sure I have a seat for the festivities. ;-)"
    author: "Eric Laffoon"
  - subject: "fish://"
    date: 2003-04-22
    body: "A large reason I use quanta is because the KDE open dialog has support for SCP with the fish:// protocol. It means you can work with the files of any computer with SCP as if they were on your machine. I've always thought that KDE's Open/Save dialog box was one of KDE strongest points, it is definitedly one of those little things that can make a big difference. I really wish GTK would come up with a better one, as there some GTK programs that I still use.\n\nThe other nifty thing about quanta is the code completion for PHP. I wish I had that when I was doing Ada development (its what we learn in our introductory Computer Science courses.) Features like that, where you don't have to figure out some dialog box or use a wizard but our integrated into the editor itself are often the most valuable.\n"
    author: "Ian"
  - subject: "Re: fish://"
    date: 2003-04-22
    body: "Gtk 2.4 will finally have a new Open/Save dialog (with everything that should be in a modern O/S dialog).\n"
    author: "nac"
  - subject: "Re: fish://"
    date: 2003-04-22
    body: "Fish does not use scp.\n"
    author: "AC"
  - subject: "Re: fish://"
    date: 2003-04-22
    body: "Humm... I think it does...\nThere is no word about scp in their homepage (http://ich.bin.kein.hoschi.de/fish/), but\nat apps.kde.com they say it does support scp..."
    author: "Iuri Fiedoruk"
  - subject: "Re: fish://"
    date: 2003-04-22
    body: "I seem to remember that it is SSH/SCP too. I'm not sure how you would transfer the files with SSH alone."
    author: "Eric Laffoon"
  - subject: "Re: fish://"
    date: 2003-04-22
    body: "I think programs that just use SSH basically connect then do a cat on a file, ie simple shell manipulation.\n\nKeep up the good work. Quanta looks like the best bet for a GUI web designer :)"
    author: "Mike Hearn"
  - subject: "Re: fish://"
    date: 2003-04-22
    body: "Fish installs a perl script on the host which implements a simple protocol for file operations. No scp involved."
    author: "AC"
  - subject: "Re: fish://"
    date: 2003-04-23
    body: "Yes, if you want scp, you can use sftp:// (AFAIK) instead, works very well"
    author: "Stephan"
  - subject: "PHP Code completion"
    date: 2003-04-22
    body: "I must be missing something, but in my version of Quanta I can't use PHP code completion. I'm using Quanta Plus 3.1. Maybe someone can tell me where to find it?\n\nThanx in advance,\nMcTrex"
    author: "McTrex"
  - subject: "Re: PHP Code completion"
    date: 2003-04-22
    body: "Well, it definetely should be there. Try the following(s):\n<?\na<--press CTRL-SPACE\n\n$(if you have other variables, they should pop up)\n\nabs(<--press CTRL-SPACE\n\n?>\n\nand similar combinations.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: PHP Code completion"
    date: 2003-04-23
    body: "Thanx a lot, I didn't search enough, sorry!"
    author: "McTrex"
  - subject: "Thanks Quanta Team"
    date: 2003-04-22
    body: "Thanks for all your fantastic work, it really is by far my favorite web development tool for Linux. Dreamweaver MX is still ahead IMO, but the quata team has nowhere near the same resources as Macromedia. In addition, the imrovements made with each Quanta version sem much bigger to me than the ones I see in each new Dreamweaver version.\n\nBTW Andr\u00e1s I too am from Romania =)! I moved to the US in 3rd grad, but I back to Romania every 2 years. I'm not too good at writting in romanian, but let me try.\n\nCe may faci? Si, in ce oras traesti? Eu traesc in Iasi, aproape de Bucharest.\n\nAnyway... thanks, both of you =)"
    author: "Alex"
  - subject: "Re: Thanks Quanta Team"
    date: 2003-04-22
    body: "I don't know how many people work on Dreamweaver, but I belive at least 10. Well, taking in account Kommander, DTD definitions, documentation and project management, we are 6. There are also some other contributors, with more or less frequent code submitting. By looking at the CVS commit logs, this number drops down.... Anyway, as we stated in lot of places, we don't try to copy Dreamweaver, but go on our own way. Of course good ideas are welcome, but saying that \"look at Dreammeaver/Homesite/etc.\" is wrong, as we usually don't have those applications on our systems. \n\nAndras\n\nPS: And some word in Romanian: Salut, momentan locuiesc in Cluj, dar sunt originar din Sf. Gheorghe, jud. Covasna, si dorim sa-ne mutam inapoi aici. Nici eu nu scriu/vorbesc prea bine romaneste, dar asta se explica destul de usor, romana nefiind limba mea materna. Altfel ma simt bine, numai daca n-ar fi birocratia asta din Romania...."
    author: "Andras Mantia"
  - subject: "Multi layered tabs!!"
    date: 2003-04-22
    body: "Taking a look at the screenshots I see multi layered tabs, oh no!  Someone should tell them about kjanuswidget!\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "Okay I'll bite. It's not on freshmeat.net or apps.kde.com. Where must one look? fictitiouswidgets.org? ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "Take a look at:\nhttp://developer.kde.org/documentation/library/cvs-api/classref/kdeui/KJanusWidget.html\n\nRich.\n\n"
    author: "Richard Moore"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "As I understood KJanusWidget is designed for configuration dialogs (I heard about it, but never used in code). On which screenshot you saw that the KJanusWidget would be useful? \n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "I think he's probably referring to the action configurations dialog which is a bit of a usability nightmare I'm afraid. I'm not 100% certain how KJanusWidget would help here though.\n\nRich.\nps. You've probably use KJanusDialog alrady via KDialogBase."
    author: "Richard Moore"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "Hi Rich!\nThe actions dialog *was* a bit of a usability nightmare. Have you seen the one in CVS? It's been totally reworked for just that reason. It feels like coming out of a straightjacket on meds to a nice spring day. ;-)\n\nUntil you pointed it out I did not realize KJanusWidget was in the offical KDE. To me this looks like a perfect building block for some new Kommander widgets. BTW I thought he was referring to the file tabs but we do have the pop up file listbox."
    author: "Eric Laffoon"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "I almost forgot... here, have a screenshot."
    author: "Eric Laffoon"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "That's much better!\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Multi layered tabs!!"
    date: 2003-04-22
    body: "Here's an example of an entire application based around KJanusWidget:\n  <a href=\"http://www.eleceng.ohio-state.edu/~ravi/cvs.png\">~ravi/cvs.png</a>.\n<p>\nYou can find it in kdenonbeta/frontman. I was surprised when I actually learned that KControl does not use KJanusWidget. It could use some developer attention, as its internals have been significantly modified in the last 3 months.\n"
    author: "Ravi"
  - subject: "Quanta is cool."
    date: 2003-04-22
    body: "I use it all the time now for PHP editing. It's the small things that are realy helpful. Like automatic variable completion. You use a long variable name (like $number_of_records) once and the next time you type $nu... you just have to press enter to get the whole variable name. Pretty cool. Syntax colouring has also been fixed what I'm still missing is the ability to select a different colour for <? and ?> which is important for mixed HTML/PHP documents. Furthermore the file selector is still a mess and \"automatically\" undocks all of a sudden due to a bug.\nSomething like in HomeSite (press F9 to open, click one or more files to open, press F9 to close again) would be nice. Currently I mapped Ctrl+Q to \"Open\" and made the Open dialogue window full screen. The maximized state is fortunately saved. Now I press Ctrl+Q which \nis easier to do with the left hand only than Ctrl+O and can select a file. The bad points about\nthis workaround are: You cannot open various files at once (except clumsily using\nCtrl to select multiple files and this works only in the same folder) and Quanta forgets about the last folder next time and chooses the folder the current file is in. Sometimes not even that.\nThis is the only real bad point I can say about Quanta. Otherwise everyone who hasn't tried it yet for PHP should definitively give it a spin.\n"
    author: "Michael"
  - subject: "Re: Quanta is cool."
    date: 2003-04-22
    body: "Have you tried project views? You can set your current group of files and loaded toolbars as a view. You can open and close all the files at once from the menu while simultaniously loading and unloading the toolbars. Get the files you want open and define your view. You can add or remove files from a view too. Why muck with all those selections when you can make a group to single click."
    author: "Eric Laffoon"
  - subject: "Re: Quanta is cool."
    date: 2003-04-22
    body: "Thanks for your answer but this is not what I need.\nI do not want to open a group of files at once that I have\npredefined before. I need a better way of opening\nvarious files across folders that I need on that day \nall of a sudden."
    author: "Martin"
  - subject: "Re: Quanta is cool."
    date: 2003-04-22
    body: "Would the file upload tree widget be better suited? We could create a multiple file open from it's selection output listed from project files..."
    author: "Eric Laffoon"
  - subject: "Re: Quanta is cool."
    date: 2003-04-22
    body: "Why so complicated? The only IMO simple thing I'm requesting here\nis the ability to open and hide a file selector with separate folder\nand file view on key press. In HomeSite you can press F9, the source code \ngets narrower and docked on the left hand side there is a file selector which \ndisappears when you press F9 again. It's so simple it's difficult to describe\nit better. This is something I really miss and the functionality is basically\nalready there in Quanta. But there are some bugs in Quanta which prevent me from\ndoing this. I would be really glad if you fixed this one day. But even without\nthat it's a fine product, so I'm patient.\n\n"
    author: "Michael"
  - subject: "Re: Quanta is cool."
    date: 2003-04-22
    body: "You're talking making the file tree on the left collapsable? This is something we've wanted to get to for some time. We may be adapting some of the UI functionality of Gideon too. At least one developer has expressed interest in doing that."
    author: "Eric Laffoon"
  - subject: "(k)vim part"
    date: 2003-04-22
    body: "Are you planning support (k)vim part in quanta?"
    author: "m."
  - subject: "Re: (k)vim part"
    date: 2003-04-22
    body: "Not specifically. ;-) But it will most likely happen. We are getting closer to where we able to do so without it being too difficult. However it would probably happen a lot faster if a motivated coder were to want to join the project with that objective."
    author: "Eric Laffoon"
  - subject: "Re: (k)vim part"
    date: 2003-04-22
    body: "Its already there, and has been there for some time.  You see Quanta uses KTextEditor to do all of its editing.  So KVim part can be used.  Although I must warn you VIM is a very primitive editor, and may not do everything you want it to do.  Kate is the best, but the Qt Editor is getting better to, especially for C-like languages.   Kate has some neat features like code folding, code completion, syntax hl, and dynamic word wrap.\n\nWord is there is a NEdit part on the way too, again, limited but allows Geeks to be as geeky as possible.\n\nReally the Vim stuff will gain you nothing more than some Geek factor and a good case of turrettes syndrome.\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: (k)vim part"
    date: 2003-04-22
    body: "> Really the Vim stuff will gain you nothing more than some Geek factor and a good case of turrettes syndrome.\n\nAnd a great deal of added producivity!"
    author: "fault"
  - subject: "Re: (k)vim part"
    date: 2003-04-22
    body: "> > Really the Vim stuff will gain you nothing more than some Geek factor and a good case of turrettes syndrome.\n\nROFL!\n \n> And a great deal of added producivity!\n \nIt's not that I'm against making it available but it will lose features compared to the current editor. Look at what Ian listed. I would really conjecture that it could add that much productivity, especially sacrificing things like code completion. The real key as I'm told is the key bindings and scripts you can set up... however note the new Actions dialog I posted here (in another thread). Quanta currently has the ability to do quite a lot with keystoke binding and actions. Actions can run scripts in any language and interact with the editor. In CVS we're improving the DCOP bindings and you can now fully release project actions to make semi polymorphic project keystroke actions. (There's some geek appeal ;) CVS has new code shortcuts that can enable you to use \"js\" and Alt-J to create Javascript tags for instance. Kommander dialogs can be helpful even to vim types on large complex PHP classes...\n\nEvery time I try to see if there is something I'm missing and I engage in discussion on the vim mystique it comes down finally to the difference being binding some of the cursor movement keys. Let's be fair. Quanta is already one hell of a productive environment and lots of vim users are getting hooked. I seriously wonder how many will switch back to the default editor for better features instead of staying for old keystrokes once it's available."
    author: "Eric Laffoon"
  - subject: "Re: (k)vim part"
    date: 2003-04-22
    body: "Just a correction: the basics are there. Last time when I tried to use KVim in Quanta didn't worked, possibly due to a bug in the KTextEditor implementation of KVim. I haven't played with it recently, but as I would to make more independent of Kate part itself, and rely only on the KTextEditor interfaces, it may be so that KVim will be usable in Quanta starting from version 3.2. But I would expect to feature losses when you switch to the vim part. But there aren't promises. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: (k)vim part - HEY MAN !"
    date: 2003-04-23
    body: "I happen to suffer from Tourette' Syndrome and I also like VIM... -- No I had Tourrette' LONG before I discovered VIM, so don't use that as proof for your theory.\n\n- David Nielsen"
    author: "Lovechild"
  - subject: "Thanks to the Dot!"
    date: 2003-04-22
    body: "I just wanted to thank Navindra and the editors for the good press. Of course thanks goes to Andreas and Klaus for the original interview. It's both humbling and gratifying to get the attention we do here at the Dot. I realize a lot of that comes from having a large user base too. I can't say how much I appreciate that as well. We have great users. On the weekends I do the Portland Saturday Market with Kitty Hooch. I strike up conversations with other vendors there in the morning and come to find out they do their web sites with Quanta. It's really a very cool experience. Everyone I talk with is very positive. If they're not running KDE their friend is. \n\nQuanta has been a tremendously positive experience and I just want to express my appreciation for all who continue to make it so gratifying and larger than the sum of it's parts. Enjoy!"
    author: "Eric Laffoon"
  - subject: "WYSIWYG"
    date: 2003-04-22
    body: "Nice option. I think Dreamweaver is a very nice tool with some bad IDE. Quanta may fill the gap. I hope you will better support CSS than Mozilla-Composer. And there will be some code cleanup utilitys plugged into, for Word html ecc."
    author: "Bert"
  - subject: "quanta is a killer application!"
    date: 2003-04-22
    body: "Whenever I want to convince somebody to try out KDE, I just have to let them look over my shoulder while I do some work with quanta and konqueror. Sooner or later they will ask me where they can get those cool applications for windows. I tell them that they can't, because these are unix applications. Then I burn them a knoppix cd to try it out at home and they are hooked. Works every time.\n\nThanks a lot to eric and andr\u00e1s for your good work. \n\nA.H."
    author: "Androgynous Howard"
  - subject: "Don't forget Kurumim"
    date: 2003-04-22
    body: "\nFor brasilian guys. If you are thinking about Knoppix like a penguin marketing tool, give a chance for Kurumim. It's small, could be burned at a mini-cd, pretty beautiful and 100% brasilian portuguese. A gret KDE marketing tool.\n\nhttp://www.guiadohardware.net/linux/kurumin/\n\n :-D,\n\n"
    author: "V. B."
  - subject: "Re: quanta is a killer application!"
    date: 2003-04-22
    body: "Ah-Ha! Quanta is a killer app... and that means you have to be one of our best \"hit men\" as it appears to be a big hit with your friends. Now for a little extra razzle dazzle throw in a dash of Kommander for something they surely don't have and show them how easy it is to make a dialog for your custom PHP functions and launch it from a toolbar or keystroke. Add that to a live preview test bench (load your project on your local server and set the project preview) and they will need a tall iced coffee to get the glazed look out of their eyes. ;-)"
    author: "Eric Laffoon"
  - subject: "Take care of your own life!"
    date: 2003-04-22
    body: "While I appreciate, maybe even adore your spirit to work full-time on free software for little pay: if you can not earn enough money by working on free software, you shouldnt do it.  Free software is nice, I believe in it as well and spend the largest part of my spare time working on it. But I will not give up my financial security for it, and I don't think that anyone should. \nIf you don't take care of yourself because you deserve it, then do it for the other developers. By working for too little money you practically devalue your work, and by releasing it as free software also the work of all other developers. In the end free software can only work when somebody pays for it, just like for proprietary software. There may be no margins that are comparable to Microsoft's with free software, but programmers should not worry about buying a CD either..."
    author: "AC"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-22
    body: "It's been a while since I fed the trolls... so here goes.\n\nThere are these things that many peopl ehave called beliefs or ideals.  You might have some, hell financial security might be one of them.  Many of the people that dedicate so much time to free software do it because they believe it is right.  Money does not have any effect on a person who truely believes in what he does.  It's just like any other kind of volunteer work.  You might not be able to handle not having financial security, but for others financial insecurity might not bother them.  I know it does not bother me.  I've been without money before, and it's not that big a deal really.  While it is a sad thing we live in a world where people like these have to do this kind of thing for free, things are not going to change.\n\nSpeaking as someone who was just laid off this very day,  I find the idea of just packing up my shit in my car and hitting the road to see where it takes me pretty exciting, and it wouldn't be the first time either.  These people love what they do, don't try to take that away from them.  Some things are just more important than money."
    author: "MrGrim"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-22
    body: "KUDOS!\n\nWell said, Mr. Grim.\n\nI was laid off myself in Jan 2001, and it was frightening at first, sometimes inducing \"rapid weight loss sessions\" during the days....\n\nI also develop (not a programmer, but a wysiwyg app user) interfaces and database tools for a zero-income-generating hobby of mine from which I hope to earn income by selling support rather than directly charging for my created apps. I develop them in Lotus Approach and Lotus WordPro, but hope someday there will be a tool that lets me create non-dba applications in Linux. I need and crave a non-server, non-ms-acess-imitating interface that is flexible enough to let me make it LOOK like what I want so I can work THEN go on to focus on my data mining or generation. (BOy, I wish I had 100,000,000 to buy SmartSuite off of IBM. I'd GIVE it to the OSDN & KDE so they could gut SmartSuite and give it new life instead of what it's currently doing: Languishing in windoze land, slowly dying and not even seeing a port to Linux (maybe WINE & other developments are not making it worh IBM's while???)\n\nToo often too many of us (indivs as well as corps) \"live\" around money and image and status. Too much is hoarded and squirreled away, and the REAL denigration of \"free\" work is not that it is being DONE for free by those who love it, but rather the damage is being done by those who fail to help promote the free-maker. What I mean is that rather than poo-poo or not even pitch a dime, at the very LEAST the new observer can help promote or advertise or spread word to any possibly interested person.\n\nGo Mr. Knoppix-burner. Keep getting them Hooked. & Mr. Grim, despite your handle, I like your reply.\n\nDavid Syes\n\nnaughtycal-artkitekture.com"
    author: "David Syes"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-23
    body: "I was really touched by this. I just wanted to offer my encouragement and best wishes. I have personally grown so disillusioned with employers I only view myself as a viable employer. I have been wealthy and I have been poor... but most of all I prized being free above all else. Here's hoping you find real happiness and freedom where the road takes you."
    author: "Eric Laffoon"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-22
    body: "I don't know whether this is directed at Andras or me... and I don't know why. I can't fully speak for Andras but it is my understanding he is making comparable money to what he would be making in his local job market. However his local job market does not have a 40 hour work week, would have him on MFC and he would be spending a lot of time away from home and not have the freedom to come and go as he pleases and go visit family and work from his notebook. I believe he also loves what he does.\n\nFor myself I cannot devote the time I'd like which is why I sponsor him. My current business, Kitty Hooch, is in it's third year and has been very successful. Our present financial stress comes from an issue of low inventory temporarily affecting cash flow combined with required expansion expenses. I will not divulge my finances publicly but while I'm forgoing cable TV and other nicities right now while sponsoring Andras I expect to be able to afford to fly from Oregon to Europe later this year to meet with other KDE developers. I hope within a years time to be buying acreage and building large commercial greenhouses.\n\nAndras and I both believe in what we are doing and I consider the last several years of financial stress since losing my mom to be a small price to pay for the rewards I have and will have. Anyone who reads what I have to say and knows me knows that I'm not looking to make money directly from open source, only indirectly. While I have grand plans for my finances my driving motivation is not primarily financial at all. My driving motivation is based upon what I believe is right. I'm doing my part to strike out for freedom and to make a better world. I am working to elevate those less fortunate around the world by giving them the tools to prosper to give them hope. I am working to promote a new age enlightenment for your children as opposed to a new age of big brother.\n\nThe argument about devaluing software is right out of the M$ handbook though. over 80% of all software is contracted and vertical market software. Quanta supports people in this area. Operating systems have already proven to be far more successful as community developed software. Applications are next. Shrink wrap software is a tool. Tool costs should be reasonable to what delivery costs are. That means Quanta should cost from around $1 a copy. Of course commercial software is far messier and greedier. Since a $1 charge is eaten up by the financial transfer costs down to about 65 cents it is fine for a few people to cover this cost as they see fit. I consider it a privlege to have spent thousands of dollars on Quanta development. \n\nI really can't fathom the implication in this post that we are somehow selling out or something. Our objective is to have the type of impact on web development software that Linux has on operating systems and Apache on servers. It works for us. As to the rest... we really don't care. We care that people have the best tools and the tools promote freedom. We get to have our tools exactly as we want them in the deal. What's wrong with that?"
    author: "Eric Laffoon"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-22
    body: "You have a good point is saying that you shouldn't destroy your life. But the reality and the reasoning behind is much more complicated. As I said in the interview I used to work at Ericsson, Finland. My salary was quite nice, my work was quite easy, the stress was minimal, so you can say it was a \"perfect\" job. But I've left that work, came back to Romania, which is surely not the best place to live from economical point of view. But I had personal reasons to came back, and I accepted also by that time that it's very possible that I will never get so much money in the rest of my life. Many would think that I am stupid to do such a thing. Well, I may be stupid. ;-) Probably many others are that I'm stupid now to work on a free project full time. Well, I may be stupid. But I like it, and until I can cover my daily costs and don't have to use my savings, it's fine. I like to have money, I like fast cars and big houses, but that's not all in the life. Life wasn't easy here, and it still not easy at all. I grew up in an apartament, and altough the conditions here were bad and we were far from being reach, my childhood and the my life until today was more than acceptable. If I have much money it's nice, if I don't have, well, I try to live without it. This doesn't mean that I wouldn't want to earn more in a month/year. Right now it's at the acceptable level, when I can pay for my rent, the internet connection, for food and such mandatory things. We don't travel abroad often, we don't fly with planes, don't buy CD-s too often and we try to not spend much for \"luxury\" items. Ok, I don't know why I wrote the above here in a public place, but I won't delete it now.\n The salary offered by that time for me was only a little higher that I get by working on Quanta full-time (and around 1/10th of my old salary), so even if I go to a company where I write closed source applications under Windows (using MFC) I could say that my work is undervalued. For my ego is much nicer to read the download counters that appear on Sourceforge or apps.kde.com, and to read the users feedbacks, and see that the users are trying to support us, than get $50-$100 more from a company. And I REALLY LIKE to program under KDE, I really like this community and I really enjoy to work with Eric. :-) He's a nice guy, he also stand on his own feet, and he admirable supports free software. Well, if I would be in a better financial situation, I would do the same. And I believe in free software and I also believe that you can build your life based on free software development. And I try to prove this (not for you, but also for my family ;-) ), altough we only took the first steps. We may fail, we may be successfull. It depends on great amount on us, but it also depends on supporting us.\n\nThanks for all of you, who have donated to us.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-23
    body: "Iread both yours and Eric's posts and I totally agree with both of you. Now I want to say hi to Andras from Bulgaria :). Now on the subject of money... the person that posted this is porably white American, 10 to 25 years of age with no life and a really screwed up job. Now i am not saying this to insult anyone but ever since I moved to tha States 3 years ago this is what i have seen. Every person that i know that talks about how they love open source but would not code in it or work on it are people who were screwed in life, period.  I go to college and at the same time work for my college's unix labs, and servers admin. He is the same way... he loves unix he runs solaris 9 on every machine that he owns(all sparcs :) ) but every time I ask him why he doesn't release some of the really cool tools that he has written for maintaining his servers, workstations he starts with:Because back in the day when i worked for *this company*.... it is really annoying. Well thats my 2 cents.\nBoris"
    author: "Boris Kurktchiev"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-23
    body: "Sorry, wrong guess, neither American nor 10-25 years, and I produced a lot of open source code - in my spare time. I just wanted to say that there are things that are more important than open source, like financial security. How many 55 year old programmers are there? "
    author: "AC"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-23
    body: "> I just wanted to say that there are things that are more important than open source, like financial security.\n\nWhat exactly is financial security? Howard Hughs had it... along with so much paranoia he locked himself in a sterile room and went insane. To me it is knowing that you can produce what you need to live happily. Eric Raymond did a little coding and wrote a book... then he got in on the VA Linux introduction as a token consultant and landed over $30 milion in stock. Who knows what he did after six months when he could cash it out. One should not discount the opportunities that could arise from being at the head of a high profile project when the M$ gravy train unravels. Who knows how much someone might want to pay us to speak or consult. Philandering ex presidents get a million dollars a speech. We would be delighted with much less. Besides, I own a business that has a huge income potential in the coming years. Right now it is challenging because I am doing it without borrowing anything from anyone. That makes today difficult and tomorrow mine. Andras is like a son to me and as I draw breath I will make sure his options are his choice. Nothing would make me happier than to see him be able to realize whatever he wants to.\n\n> How many 55 year old programmers are there? \n\nThat's funny. My dad is much older than that, but he's a retired programmer. I'm reminded of the line in the movie \"Almost Famous\" spoken by the promoter suggesting the Rolling Stones won't be doing tours when they are grandfathers so grab it while you can. If we were talking framing houses I would see your point. Beyond that there is not much point. I grow catnip and now I program a bit less than when I banged out 6-10 hours a day building sites. \n\nI personally believe, and I stand on this, that when you are an old man on your death bed you will be much less concerned with your former distractions on illusive security than on what you left for a legacy. I believe you will be a lot more concerned on what the world you are leaving behind will be like becuase of you that on what no longer matters. Knowing security is an illusion is freedom. People lose a job and their world turns upside down. Look at a wealthy person and you will see they crashed and burned financially not once, but several times in acrueing that wealth.\n\nAndras and I have security in our passion and performance always being in demand and our partnership always being solid. Financial issues are transitory and opportunity is unlimited. Knowing this we have focused on what we believe to me most important and anre thankful for the freedom to pursue it."
    author: "Eric Laffoon"
  - subject: "Re: Take care of your own life!"
    date: 2003-04-23
    body: "Hi. Welcome to America, often referred to as the only country that does not teach it's young it's system of economics. In my experience people who come here from other countries are amazed and enthused by the opportunities and people who grew up here have little concept because they have nothing to compare it to. They never lived without a security net. They never struggled for food or shelter. They never fled their homes for thier lives. America is a great place, but people should go tour third world countries for a year to get perspective.\n\nHowever now that you're here and you see all this you can take advantage, as many immigrants do, and succeed and prosper. When you do, rememeber one more American axiom. We like a gracious winner. ;-) Try not to be hard on people or too easily stereotype them. It will leave you having to apologize from time to time. ;-)\n\nBTW note that the Quanta team I am in Oregon, Robert is in California, Adam is in Boston, George is in Florida and several other members of our team are here in the US. So you should get out more... that way you can meet the right people. ;-)"
    author: "Eric Laffoon"
  - subject: "Only reason I have KDElibs installed is Quanta."
    date: 2003-04-23
    body: "Just a few words of encouragement - Quanta is the only KDE application I use presently, and it is probably the most productive application I have installed on my hard drive.  I have churned out more valid HTML pages with Quanta than any other editors in years of online projects.\n\nI'm not a developer though, so I'm going to pony up a cash donation just to encourage continued development of this very-usable-in-its-present-state application.  The potential for this to be *the* Linux-based HTML editor is high.  \n\nI want to personally think Eric and Andr\u00e1s for their work and sacrifice on this.  I cannot state enough how terribly crucial it is that we have a free impressive web development environment.  I'd definitely put money on Quanta right now.  I've encouraged so many people to try it out.\n\nThanks guys, seriously."
    author: "Quag7"
  - subject: "Re: Only reason I have KDElibs installed is Quanta."
    date: 2003-04-23
    body: "I think Andr\u00e1s would agree with me when I say how flattering this is. I'm beside myself. Knowing we've made a difference like this is the best thing of all. However there is still that matter of hard costs and you have put your money where your mouth is. So I already thanked you personally, now I can thank you publiclly. We will continue to to strive for excellence and innovation... a little more encouraged.\n\nThanks to you for your support..."
    author: "Eric Laffoon"
  - subject: "Go Quanta!"
    date: 2003-04-23
    body: "I have been using Quanta for a few years now.  I used it to design and maintain my own web site (http://www.chinilu.com) which is intentionally simple (I personally detest web sites which make lavish use of flash and java and such just to project an image, but to each his own).   I find Quanta to be a very useful and practical tool, a piece of software that I can always rely on to do the job.\n\nAlthough I don't do web design professionally, a few months back, I was approached by a customer who wondered if I could fix a rather complex professionally designed website.  Several other vendors had attempted to modify it using various WYSIWYG Visual web editors and the result was a mess.  There was one particularly annoying defect that my customer had been told was unfixable.  I had never tackled a project like that, but I decided to accept the challenge.  With Quanta and some HTML documentation I was able to clean up the HTML and solve every problem and my customer was delighted.  In addition I have created web sites for several other customers.\n\nSo I am really happy to see that Quanta continues to move forward.  I especially look forward to a visual editor component.  I think that a great visual web editor combined with a full featured text level editor like the current Quanta would be a real killer app!  I also think that it is great that Quanta will be moving into the XML realm as well as XML is the future of the web and electronic commerce.\n\nHats off to Eric and Andras for a job well done!"
    author: "George Mitchell"
  - subject: "Re: Go Quanta!"
    date: 2003-04-23
    body: "It's George! Hi George. It's nice to see one of my old LinuxToday pals hanging around and saying good things about us. Thanks for your kind endorsement. Keep up the good fight, getting your customers solid systems they can run Quanta on. ;-)"
    author: "Eric Laffoon"
  - subject: "Wrong answer..."
    date: 2003-04-23
    body: "I want to publicly say sorry that I said above that Eric is sponsoring me since the middle of last month. This is completely untrue, as he does since the middle of last year.\n\nSorry, Eric. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Wrong answer..."
    date: 2003-04-23
    body: "I want to publicly say sorry that I said above that Eric is sponsoring me since the middle of last month. This is completely untrue, as he does since the middle of last year.\n\nSorry, Eric. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Wrong answer..."
    date: 2003-04-23
    body: "Wow! Two apologies. Now I finally have proof you're not perfect. ;-)\n\nNext time we let our wives proof read our interview... they never miss this kind of detail. ;-)\n\nA better explanation is that this was an oversight as this interview was revised from what was done last year. Okay... you're back on our Christmas list. ;-)"
    author: "Eric Laffoon"
  - subject: "Cervisia Support"
    date: 2003-04-23
    body: "Thank you! Thank you! Thank you!\n\nI'm so dependent on CVS, for the usual reasons.  Having Cervicia support in Quanta is one of the main reasons I've abandoned all other editors for Quanta -- and this only in the last month.\n\nExcellent idea. Terrific job!"
    author: "Soloport"
  - subject: "Re: Cervisia Support"
    date: 2003-04-23
    body: "In the CVS version it actually can be loaded into a tab like a text file. This allows it to be left open and selected when you want it which has proven to be very cool. We have the kpart UI load and unload menu and toolbar items per tab on selection so you can have multiple plugins loaded and hop from one to another with correct UI. ;-)\n\nOne of the things I like best about this is that I use \"$Id: Exp $\" inside my files to keep a visual record of the latest change there. On update it edits the files and causes a pop up. I always felt uncomfortable chosing whether to keep/discard/diff it when all I saw was Cervisia. \n\nFor anyone who has not tried it, it is incredbly easy to set up a local CVS and have file versioning for your projects. Even working alone it provides substantial benefits and peace of mind."
    author: "Eric Laffoon"
  - subject: "Quanta Gold"
    date: 2003-04-23
    body: "Does anyone use and pay for Quanta Gold? It seems to me that theKompany, while a nice effort, is actually proving to the world why open source is better that closed source. ;) (I could definitely be wrong, though.)\n"
    author: "Apollo Creed"
  - subject: "Re: Quanta Gold"
    date: 2003-04-23
    body: "Quanta Gold is by no means better than Quanta Plus, Quanta Plus, from my experience is actually significantly better and is more integrated with the KDE enviroment.\n\nHowever, Quanta Gold has one big advantage (which could be seen as a disadvantage because it sacrifices integration in some areas) is that it can run on Windows and OS X in addition to Linux. \n\nBut, if you just run Linux there is little you will find in Quanta Gold which you will not find in Quanta Plus. And there will be quite a lot which you will find in Quanta Plus and not in Quanta Gold.\n\n(Maybe things have changed, but this is how I feel since the last time I tried Quanta Gold)"
    author: "Mario"
  - subject: "Re: Quanta Gold"
    date: 2003-04-23
    body: "> Quanta Gold is by no means better than Quanta Plus, Quanta Plus, from my experience is actually significantly better and is more integrated with the KDE enviroment.\n \nThank you.\n\n> However, Quanta Gold has one big advantage (which could be seen as a disadvantage because it sacrifices integration in some areas) is that it can run on Windows and OS X in addition to Linux. \n\nThis is not entirely true. Quanta Plus runs on Mac OS X using the Fink project (http://fink.sourceforge.net/). As far as Windows goes you can also run KDE and Quanta+ there. See http://kde-cygwin.sourceforge.net/. So the reality is that you can have it all and have it as F/OSS. Quanta Plus not only gives up nothing there but brings more fun to the party.\n \n> But, if you just run Linux there is little you will find in Quanta Gold which you will not find in Quanta Plus. And there will be quite a lot which you will find in Quanta Plus and not in Quanta Gold.\n \nThere are I think a few features we don't have and more we have that they don't. A review a few months back of both found in our favor for productive use. Beyond that our development team is growing and the KDE re-use cannot be underestimated. By release 3.2 of quanta Plus I don't think there will be much doubt in a side by side comparison.\n\nAs always, in spite of our costs, the cost of Quanta Plus is whatever a user may feel compelled to freely contribute. ;-)"
    author: "Eric Laffoon"
  - subject: "a reason to have wysiwyg"
    date: 2003-04-23
    body: "hello all of you,\nI just wanted to give you a good reason to have at least a minimal wysiwyg in Quanta. In languages other than english, there are special symbols, accentuated characters and the like, that make a html source just as hard to read than to write. Plain text in french, for example, as it's my native language, is full of &eacute; &egrave; &agrave; and since it's still possible to write, that's much more convenient to be able to enter and modify text in your native language and let the software replace special characters with their correct code. And I think it would be specially useful for people for whom programming is not the first competence and have to maintain a web site.\nThanks a lot for your great piece of software.\n"
    author: "sylvain mottet"
  - subject: "Re: a reason to have wysiwyg"
    date: 2003-04-23
    body: "There is a lot more than just a basic application in the works. See http://members.shaw.ca/dkite/mar212003.html and scroll down a little to the discussion on what is to come."
    author: "Eric Laffoon"
  - subject: "Re: a reason to have wysiwyg"
    date: 2003-04-24
    body: "You should be able to just specify character set in the document header and use these characters as normal. I do it all the time with characters such as \u00e5, \u00e4 and \u00f6. As far as I remember this is completely legal, if and only if the character set is specified. (It often works anyway though, at least with ISO 8859-1)."
    author: "EUtopian"
  - subject: "Rexx?"
    date: 2003-04-24
    body: "Very interesting and fascinating interview.\n\nI personally do not have a use for a web editor and don't have any Quanta experience, but I briefly launched it once and the interface seemed professional and well-structured. It is comforting that the people in charge are quite the visionaries and I believe that this application may go far.\n\nAs for the ideas about applications interacting, it sounds a lot to me like ARexx on the Amiga and Rexx on OS/2. They were very nice but somewhat under-appreciated. They did frequently make good glue to tie applications together and automate tasks, though."
    author: "EUtopian"
  - subject: "Re: Rexx?"
    date: 2003-04-24
    body: "Rexx is nice because it is a very natural language non typed programming language that is quick and easy to grasp. Using a program like VX-Rexx you could create mini applications or quick dialogs or prototype programs. It was great for private or in house work. Unfortunately the company that produced it was sold and it fell by the way side.\n\nFundementally Kommander, scripting and DCOP are from similar conceptual roots but very much different in application, aside from being open and free unlike VX-Rexx. Rexx proved to be a great scripting language for instance in the Mesa 2 spreadsheet by sundial, the one application I really miss. The problem is that you needed to have a Rexx API in an application and then you needed to access it. Additionally you needed to know Rexx and you had to live with it's limitations like it's clunky tail strings instead of arrays. Object Rexx looked like a true winner but then there was no good visual tool one could depend on always being there to work with it. In fact non on OS/2 that I know of. Finally all of this lacked the abilities that would have come into play from DDE (Dynamic Data Exchange) which OS/2 shared with win 3x. This was in my Opinion a far more useful tool that OLE (Which I believe menat One Large Expletive as your system collapsed when you used it).\n\nConversely using Kommander, shell or other scripting and DCOP actually does promote user accessiblity. You actually need actual programming to create an interactive dialog. You can choose between many competent languages to augment it. DCOP actually provides tools to make implementation very easy for developers. In fact most of the top applications have extensive DCOP integration for a variety of purposes. So the process of extention is already in place. So the next step here would be to promote specific DCOP uses, profile applications and create the ability to something more user friendly than kdcop to enable a filtered presentation of available calls for an app.\n\nThe central key here is to build on a visual tool that enables this without programming and can be further enhanced with shell scripting and/or the language of your choice. The idea is that your desktop becomes user extensible with custom dialogs, scripted interactions, data transparency and in effect merges into a seemless user application. So various aspects of tasks and projects become interactions of best of breed focused tools. That is really a very *nix way of looking at things anyway. ;-)"
    author: "Eric Laffoon"
---
In this fascinating interview, Eric Laffoon and Andr&aacute;s Mantia give us a glimpse into the  world of the <a href="http://quanta.sourceforge.net/">Quanta Plus</a> project.  Read on for everything from tantalising references to <a href="http://quanta.sourceforge.net/main2.php?snapfile=snap02">Kommander</a>, billed by Eric to be part of the foundations for the next generation desktop and user experience, to details of future plans for Quanta VPL (Visual Page Layout).  We also launch a call for help from the users, either in the form of <a href="http://quanta.sourceforge.net/main1.php?contfile=needs">Quanta contributions</a> or <a href="http://quanta.sourceforge.net/main1.php?actfile=donate">much-needed donations</a> to help sponsor the work.  
<!--break-->
<p>
<i>This interview was <a href="http://www.kde.de/appmonth/2003/quanta/index-script.php">first conducted</a> by the <a href="http://www.kde.de/index-script.php">KDE.de</a> team and recently updated for the dot.</i>
<p><b>1. Please tell us a bit about yourselves.</b>

   <p><i>Eric L. Laffoon:</i>
<blockquote>
   I'm a 46 year old confirmed tech addict. I began building electronics
   projects at 9 years old surprising my dad by implementing a feedback
   resister on the output stage of my Heathkit radio. (I got the idea from
   his Harmon/Kardon schematic) I first programmed Fortran in Navy schools
   in 1975 where I nearly aced the computer section. I have done a number
   of things and fell into web work in the late 90s while using OS/2. In
   1999 I decided to make the switch to Linux as it seemed to have all the
   &quot;mind share&quot;. The first thing on my agenda was to find a good web
   development package.
</blockquote>

  <p><i>Andr&aacute;s Mantia:</i>
<blockquote>
   I'm a little boy compared to Eric with my 26 years. :) I'm of Hungarian
   nationality, but I live (and was born) in Romania. I work with computers
   since the beginning of the '90s, and I really love it. My first PC was
   a ZX Spectrum clone, and I already wrote some programs on it (even a full
   game). I finished university in 2000 and that same year I went to
   Finland, where I worked for Ericsson (mobile systems). For the rest of the
   story see below.
</blockquote>

<p><b>2. How was Quanta born?  How did you get involved in the project?</b>

   <p><i>Eric:</i><blockquote> 

   It was early 2000 when I saw an announcement for <a
   href="http://quanta.sourceforge.net/">Quanta</a> on <a
   href="http://freshmeat.net/">Freshmeat</a>. I had been looking for
   something more advanced that the EPM editor I was using on OS/2. I
   looked at Coffee Cup shareware, Visual Slick edit and a bunch of
   free software. I had quickly come to prefer KDE to GNOME and was
   looking for a good KDE app. The only half decent one was <a href="http://apps.kde.com/na/2/info/id/317">Webmaker</a>
   and it lacked a lot. Quanta was version 0.97 or so when I first saw
   it and it was basically a stripped <a href="http://www.kdevelop.org/">KDevelop</a> with a KHTML
   preview. It had no tag dialogs and few useful features but it was
   cute. I wrote Dima and Alex out of frustration that this program
   looked so good but didn't satisfy my needs as a web developer. To
   my surprise they asked me for input and I specified a number of
   things to make it better. In fact my focus was strictly on what
   would get the job done the fastest. They had some good ideas too
   like programmable actions but much of the interface and efficiency
   was my specification.<p> Early in the development they mentioned
   they would not have access to school computers soon and they lived
   in a youth hotel in Kiev. I helped them to get into a house with a
   phone line and later to get them a second computer. I don't think a
   lot of people realize that despite what we imagine with OSS had I
   not stepped up at the time Quanta might well have died an early
   death.
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   All I know is that Quanta was
   started by Dmitry Poplavsky, Alexander Yakovlev and Eric. Alex and Dmitry
   left the GPL'd Quanta project before 2.0 final came out, and they are
   working now on the <a href="http://www.thekompany.com/products/quanta/">commercial version</a> at <a href="http://www.thekompany.com/">theKompany.com</a>. I was invited to join
   the project around November 2001 by Eric. He found me as he had downloaded  the
   <a href="http://www.virtualartisans.com/kde/andras/en/kallery.htm">Kallery</a>
   application -- my very first application written for KDE. ;-)
   At that time GPL'd Quanta was basically abandoned, and I became the only coder,
   and the first step was to fix the most annoying bugs and make a stable 2.0 release.
</blockquote>


<p><b>3. Who else is (or was) involved with Quanta?</b>

   <p><i>Eric:</i><blockquote>
   It's only fair to mention Dmitry and Alex, even though it was not the
   most fun when we parted ways, because they began the earliest versions.
   Clearly I think Andr&aacute;s is the number one person to mention and I agree
   with his other names listed. Marc Britton and Robert Nickel have been
   great as have a
   number of other people. Too many to name. While converting Quanta from
   KDE 1 to KDE 2, <a href="http://people.kde.org/rich.html">Rich Moore</a> helped out a bit. I miss Rich. He is a wealth
   of knowledge.
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote> First there is
   Eric, who is devoted to this project and supports it from his own,
   sometimes limited, resources. Alex and Dmitry should be mentioned
   as they started the project, and currently some more or less active
   developers help us, such as Marc Britton (author of <a
   href="http://quanta.sourceforge.net/main2.php?snapfile=snap02">Kommander</a>), Robert Nickel (who wrote a good amount of
   documentation for Quanta). Just take a look at the &quot;Thanks
   To&quot; list in the About box.
</blockquote>


<p><b>4. How is the project organised?  Do you know all developers or
contributors personally?</b>

   <p><i>Eric:</i><blockquote> Largely I manage the project and leave
   Andr&aacute;s to focus on the internals.  I have a loose management
   style because I believe that people need to work from their
   passions. Andr&aacute;s and I are very close in our vision and
   thinking and while I have input on technical issues I trust him
   implicitly. I first coded over 25 years ago and have coded in many
   languages but I've only done a little C++ and only since I started
   on Quanta. When I was left alone with Quanta I was determined to
   learn and code if I had to do it all myself.  I can read C++ fairly
   well and I keep saying I will spend a few weeks playing some
   time. I know that I will, however I'm forced to accept that my
   project management and business activity must come first and they
   take a lot of time. Many times I wish I could switch places with
   Andr&aacute;s as it would probably be more fun. ;-)<p> I take input
   from users and from my own use of Quanta as a starting point for
   what I want to do. I read every day about Linux developments, new
   programs and what is happening in high tech. I converse with
   developers and then I work to put together a plan. I try to get
   other volunteer developers to commit to small pieces of code and
   have Andr&aacute;s be the answer man. Because I've owned businesses
   and run crews of independent contractors I think I have a bit of an
   edge in the mindset to manage this type of project. It is much
   different than having employees dependent on you. You have to be
   able to inspire and help people develop confidence in what they are
   doing. I like that a lot.<p> I have met everyone involved with
   Quanta through Quanta. I happen to think that is very cool. It's
   also quite humbling. You really have to have a degree of success
   before you can attract more people to help you succeed. Recently
   I've seen more interest and people coming in like Adam Treat and
   Nicolas Deschildre who are working on WYSIWYG, or as I like to call
   it, VPL (Visual Page Layout). ;-)
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   I know none of them personally. Some coordination is done by Eric, who usually
   defines what should we implement, when should we make a release, he writes
   the articles, updates the Quanta site and so. He is the project manager. But
   if we are talking about coding, then I'm the one who makes the decisions. :-)
   The discussions usually are taking place on our development list. Since the
   beginning we have had only one IRC discussion, before the 3.0 release. This is also
   because I don't have a permanent internet connection. Before I had a very slow
   dial-up one, now it's far better, but still dial-up and not so cheap. I have
   to thank to Eric who supports me having this internet connection.
</blockquote>


<p><b>5. What are your future plans for Quanta?</b>

   <p><i>Eric:</i><blockquote>
   Quanta has several key areas I have plans in. For one, having Andr&aacute;s at
   the development helm means that I know he will be constantly looking to
   improve the internals and since he has full time exposure he will be
   very much on top of it. I have immense trust in him. I know that he is
   working on his passion and he always has the right answer so I have a
   very collaborative relationship with him.<p>
   Andr&aacute;s and I are focused on having the best markup and script tool anywhere.

   My vision for Quanta is to make it the next "killer app" on Linux. Even
   though the use of web development tools is currently limited among computer
   users I feel there are two key aspects people overlook when they say that
   Quanta is not well suited for this mantle. First of all there are a lot of
   web developers. These people are generally more technically inclined than
   the average user and in marketing terms are called "early adopters". This
   is because they each tend to have a number of friends who look up to them
   as being knowlegable on computers. Early adopters lead the masses in
   coming to any new technology or product. The other reason is that I believe
   that web page and document creation in various markups is going to become
   more common among users and this means leading here would make Quanta and
   KDE a more desirable work environment. I'm very excited and also humbled
   be a part of KDE. Whether you look at the desktop, the development
   environment or the people you can't help to be impressed.<p>

   The key to bringing Quanta to the level of my vision is very simply man
   power. Granted KDE is an extremely efficient development platform, but to
   reach the heights of application competence in something so diverse as
   general markup and scripted web interfaces you need to cover a lot of ground.
   The idea I've been working to advance is that we can do this, and this is
   very important for people to understand, much better by creating a two
   tiered development model. What we are working to do is to make it possible
   for users to extend Quanta. Examples of where we are working on this are
   templates, scripts (new in 3.2), DTDs (largely just XML), programmable
   actions, customizable toolbars and custom Kommander dialogs. We want to
   focus our developers on the internals that cannot be done by users like
   code optimizations, debugging, universal features, visual page layout and
   the like. I believe to date we have not adequately gotten the message out
   so I will state it clearly.<p>
   <em>We are looking for a few dozen users to step out of "consumption mode"
   and into "community mode" and help us flesh out the features we're adding.</em>
   <p>
   My goal is that by the release of 3.2 Quanta will be considered to be a
   viable candidate for best in class web development tool on any platform
   and not just among free software! I believe it's going to take some of
   our userbase stepping up to help us take on new DTDs, add templates
   (though we will add some soon) and more to make this a certainty.<p>

   I would be remiss not to mention Nicolas Deschildre and Adam Treat helping
   us to bring WYSIWYG, or as I prefer to call it Visual Page Layout (VPL), to
   Quanta. We have a positive expectation of delivering a fairly complete
   implementation for KDE 3.2.
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   To make Quanta the best SGML/XML/Script tool. ;-) Seriously, my plans are to make Quanta
   even more usable, more customizable. The basics are there since the 3.0
   version, but there is a lot of things that must be done before I say, that
   yes, this is what I wanted. One of the weakest points of Quanta is its
   slowness while editing certain kind of documents. 3.1 is a step forward, but now
   I'm working on an even faster and better parsing algorithm. Otherwise I don't
   want to talk about new features. We have a <a href="http://quanta.sourceforge.net/todo/display.main.php">todo list</a> on our web page, and you
   can check out the wishes on the KDE and Sourceforge bug site.
   I know, most users would like to see a WYSIWYG editor. This is not a
   priority for me, but we have recently had contributions in the area, as Eric mentioned.<p>
   Short and middle term plans are bugfixing (as usual), off-page parsing,
   improved autocompletion, integrating some new, cool plugins, adding new
   DTDs and toolbars to the Quanta tree, so it can be used by another group
   of people, not just by those who write HTML/XHTML/WML pages.
</blockquote>


<p><b>6. Why did you choose the GPL for the Quanta project and what
are your thoughts on Open Source in general?</b>

   <p><i>Eric:</i><blockquote>
   Originally I confess I was not too sure about the GPL. We were debating
   on license early on and Dmitry and Alex wanted GPL. It looked to me like
   with the code we were using from kwrite and other programs we pretty
   much were locked in. Most of the original Quanta was just reuse. When we
   were presented with the opportunity to produce a commercial application
   I was surprised that the idea was that the GPL'd version was to be left
   to wither and die. That caused some soul searching for me. Aside from
   the fact that I did not believe there was much money to be made in small
   retail software I began to feel very strongly about programs being
   available via the GPL. I have received a number emails from people
   around the world thanking me for making it possible for them to find
   work developing web sites and bettering their lives. I could not put a
   value on that but I would not part with it for any price.<p>
   To me software has been a lottery for many people. Bill Gates was in the
   right place at the right time to leverage a shoddy operating system into
   a fortune. I don't like lotteries. I don't really want to receive money
   unless it is for something I did to add value to someone's life. I don't
   believe the lesson of "getting lucky" and raking in money for years to
   come for an idea at the expense of all others is a good one, or
   realistic. Everyone has the power to bring about positive changes in
   their lives, but sometimes they need a little help. Open source software
   levels the playing field and allows someone who can barely afford a
   computer to learn the skills to compete with those who have all the breaks.
   To me tools should always be secondary to products in costs. I
   think software that is charged for should be contracted or have some
   explicit merit for its cost. Commodity software should be free or cheap.<p>

   I am a strong advocate of GPL'd software and I don't really understand
   why more businesses haven't recognized the tremendous advantage in
   leveraging this development model for superior and inexpensive tools.
   Businesses will always compete on factors other than tools as most tools
   will be available to all competitors. The GPL offers coop-etition advantages
   for all parties except software retailers and fits better with vertical
   market uses. The arguments against it offer stagnation, and the inevitable
   eventual inaccessibility of too much good work.
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   I always liked the idea of free software, and never thought that I will
   publish my code which was written in my free time under a proprietary
   license. If you work for a company, then it's OK to do non-free software, but
   if you do it for your own fun, let's share freely with the world. And GPL is
   a nice free software license. Right now I don't work on Quanta in my free
   time, but I do it full time, and it is even more fun.
</blockquote>


<p><b>7. If publishing free software doesn't make one rich, how do
you earn your living?  Is Quanta sponsored in any way?</b>

   <p><i>Eric:</i><blockquote>
   At one time I intended to make my living directly from Quanta, though
   now I do indirectly. I own and operate <a href="http://www.kittyhooch.com/">
   Kitty Hooch Catnip</a> and before you say anything I have been really busy so
   I hope to finish making my site impressive soon. ;-) We sell a super
   premium product, the only one guaranteed cats will love it, to pet
   stores, online and at open markets, holiday bazaars and pet shows. In
   2001 it was a side business, In 2002 we made a difficult transition and
   in 2003 we expect to begin earning what it will take for a major
   expansion.<p>
   Since the beginning I have sponsored Quanta development out of my
   pocket. We have had some donations which we appreciate very much.

   I'd like to note that coming into 2003 we had some slow months and now
   we are looking at considerable expense to expand. Expansion is not
   optional. Demand is through the roof! We need to further expand in 2004
   to a new location where we can finally produce on the level we need
   to realize our long term goals. This will enable me to look at the
   possibility of helping other related projects too. I want KDE to have
   everything I need application wise at a level that is second to none.
   At this time I would like to be giving Andr&aacute;s more as he deserves it.
   However until I cover our expansion expenses everything spent elsewhere
   is costing me many times that down the road.<p>

   It should be made clear. Quanta is sponsored, mostly by me, and by
   miscellaneous small donations.  <a
   href="http://quanta.sourceforge.net/main1.php?actfile=donate">Making
   a small donation</a> to the project is the best way to keep me in a
   good mood instead of thinking about what I could buy with all the
   money I'm spending. ;-)
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   Right now I depend on Eric. He sponsored Quanta in the beginning, and starting
   from the middle of year 2002 he was able to sponsor me. There are no big
   figures here, all I can afford from this sponsorship is the (dial up)
   internet connection, paying my bills, buying some food for us (I have a
   wife...)  and such. If there are unexpected expenses, then it is not enough,
   but I've decided to work full time for Quanta, even for less money than I
   could get from a company where I would develop proprietary software under
   Windows using MFC...<p>

   I would therefore like to mention again that we have a <A
   href="http://quanta.sourceforge.net/main1.php?actfile=donate">donation
   page</a> which is even accessible from within Quanta itself
   (Help->Make a donation).  It's easy to donate for those who use
   PayPal but we are certainly open to other means and types of
   donations.  Every small amount helps ease my and Eric's job. 

<p>
Of course, if there is a company that would like to sponsor Quanta
   development, do contact us.  I would really like to focus on Quanta
   development in the future, but this would mean some secure income
   and what is considered a small amount for a company can make the
   difference for one man.  The other reason why I accepted to work
   full time on Quanta is that I've made some savings in the past
   (from my former job in Finland at Ericsson) and can therefore
   survive one or two months without a job, but I think it's
   completely understandable that I would like to use those savings as
   little as possible.
</blockquote>


<p><b>8. What are your favourite tools under KDE?</b>

   <p><i>Eric:</i><blockquote>
   Quanta comes to mind. ;-) Beyond that <a href="http://www.kdevelop.org/">Gideon</a>
   is very cool. I really like <a href="http://cervisia.sourceforge.net/">Cervisia</a>. It's
   great for CVS and I like to do web work from a CVS repository.
   <A href="http://kfilereplace.sourceforge.net/">KFileReplace</a> is very nice for multi directory find and replace with
   wildcards. <a href="http://bruggie.dnsalias.org/kompare/">Kompare</a> is very nice too. I suppose I better mention <a href="http://www.virtualartisans.com/kde/andras/en/kallery.htm">Kallery</a>
   so Andr&aacute;s doesn't get mad. ;-) I'm also a big fan of <a href="http://www.koffice.org/kspread/">KSpread</a> and I'd
   like to see some improvements there. Actually <a href="http://quanta.sourceforge.net/main2.php?snapfile=snap02">Kommander</a> is becoming my
   favorite tool along with <a href="http://quanta.sourceforge.net/">Quanta</a>. I'd really like a great database tool
   but <a href="http://knoda.sourceforge.net/">knoda</a> looks promising.
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   I love KDevelop, and I'm really wish to see Gideon becoming more
   stable and usable. I'm already using it now, but it still has some annoying
   bugs, but they are less and less. I'm proud that I was the one who fixed some
   of them. :-) All what I can say is that KDevelop made me forget about Delphi,
   which I think was the best IDE under Windows. And of course the Qt/KDE
   libraries also helped me in this case. These are the best libraries in the
   area so far!<p>
   I like also some other tools, some of them appear in Eric's list.
   What do I use on daily basis?  Gideon, Cervisia, <a href="http://kmail.kde.org/">KMail</a>,
   <A href="http://www.konqueror.org/">Konqueror</a>, <A href="http://konsole.kde.org/">Konsole</a>, <A href="http://apps.kde.com/na/2/info/id/292">KwikDisk</a> and <a href="http://kate.kde.org/">Kate</a>. <a href="http://k3b.sourceforge.net/">K3b</a> is also nice for burning CDs.
</blockquote>


<p><b>9. What are your dreams for the desktop of the future?  How far are we from the ideal desktop?</b>

   <p><i>Eric:</i><blockquote>
   I don't know how much looks matter for me. To me KDE 3.1 Keramik is
   already beautiful. In the looks and use department
   <a href="http://slicker.sourceforge.net/">Slicker</a>
   is somewhat <a href="http://slicker.sourceforge.net/screen.php">impressive</a>.<p>
   However my desktop of the future would revolve around Kommander. I'll
   explain.<p>
   Most people think of software as shrink wrap or commodity software.
   however most software is actually in house or vertical market packages.
   So if you have an out of the box install you have lots of great tools
   but what you don't have is &quot;your&quot; application. You have word processors
   and spreadsheets and calendars and email... but until you work with them
   they are not configured to do what you want. If you have a system
   engineer sit down with you they could take the various programs and
   customize them as well as work on basic templates for files, etc... When
   the programs are integrated by common dialogs and scripts then it
   becomes a far more productive desktop. It becames your application, not
   just a bunch of general applications. This integration could be far more
   easily managed by the average person if they had a tool that allowed
   them to exchange all relevant information, settings and data between
   applications using dialogs and scripts to complete the integration. That
   is what will be possible with Kommander and fully <a href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/dcop.html">DCOP</a> enabled
   applications.<p>
   The desktop of the future allows for all applications to interact
   seamlessly, automates regular tasks and enables you to structure your
   work flow efficiently to avoid duplication of effort and utilizing the
   best tools at hand. I would say that this is becoming available now but
   if we take the right direction in the next 1-2 years we could see
   dramatic improvements. To my mind, once you &quot;get it&quot; there is nothing
   else close. The current desktop star is a monolithic &quot;one size fits all
   &quot; approach which is not the best but works because it handles some small
   degree of personalization and customization for users. Imagine if we
   took these concepts and tied them all together. Kommander should be a
   big part of the new seamless desktop.
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   I usually don't dream about desktops (maybe sometimes about coding...) ;-) The
   current desktops fulfill my requirements, what I would like to see are more
   good applications. Sometimes the core of an application may be good, but the application itself is not very usable.
</blockquote>


<p><b>10. What kind of hardware do you have and what OS do you use?</b>

   <p><i>Eric:</i><blockquote>
   I just run Linux. I do have several old copies of Windows but I can't
   remember the last time I booted one. I have a local network with
   currently three systems and a firewall from an older system. I've been
   running <A href="http://www.mandrake.com/">Mandrake</a> exclusively after trying all the mainstream distros but
   I'm switching my system over to <A href="http://www.gentoo.org/">Gentoo</a> before January. My system is
   loaded with an Athlon XP 1700 overclocked to 1900+, 512 MB RAM, 90 GB disk 
   space and a 19
   inch monitor. I also have an Athlon 700 in my wife's office with a 17
   inch monitor and mom's old K6-500 with a 15 inch monitor that will be
   moving into our production area or as an entertainment unit.  I'm going
   to upgrade to an Athlon 2000 soon because I do a fair amount of
   compiling... and I can't get over Andr&aacute;s having a system that much
   faster than mine. ;-)<p>
   I will probably get a notebook in 2003 and my whole business is being
   run off my local net along with a remote server. I intend to work on
   enhancing that to be a model of efficiency.
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   I mostly work on an Athlon XP2000+ with 256MB RAM, 40GB HDD and a
   Hansol 17&quot; monitor. I built it from parts, as I don't trust pre-made
   systems. And of course, I use Linux (<a href="http://www.suse.com/">SuSE 8.0</a>, but quite modified, as I
   compiled lot of things from source). I have an older Compaq laptop, which has
   SuSE 8.0 and Win98 on it, but I almost never boot  Windows. KDE is self
   compiled on both of them. For developing I use KDE HEAD, as my
   current internet connection makes it possible to update from time to time.
</blockquote>


<p><b>11. What do you do in your spare time, apart from working on
Quanta? ;-)</b>

   <p><i>Eric:</i><blockquote>
   Is there such a thing as spare time? I've been checking and I only seem
   to get 24 hours a day which is hardly enough. It seems like forever
   since I took a day off. Work
   for me is diagonally across the hall from my bedroom. I have put in as
   much as 20-30 hours a week on Quanta while working seven days a week the
   rest of the time from when I get up until I go to bed. So if you want
   to push my buttons write and say you'd like to help but you don't have
   time... just be sure you can show me 100 hour weeks. ;-)<p>
   I do have some things l like to do. I find movies relaxing and I have
   been a musician since I was 14. I have set aside my instruments for the
   last several years but I plan to set up a new recording studio in 2003
   and begin playing again. I have been working on the lyrics for an album
   to honor my dear mother who I lost unexpectedly while driving her back
   to Oregon on August 31st, 2001. I also enjoy going to the gym and
   bodybuilding. Actually for a guy my age (46) it is my primary defense
   against getting old and how I keep my energy up. My
   goal is to drop to 10% or less body fat this summer and finally
   show off great abs at 46. After 20 years you
   realize the odds are decreasing if you don't get with it. ;-)<p>
   Aside from that I have developed a passion for hand rolled cigars over
   the years. They force me to relax for an hour and ponder. I've come to
   appreciate that a lot, as well as the fact that they have been made the
   same for over 150 years, touched by 100 craftsmen each and aged for
   years. You can't even say that about wine. I know Andr&aacute;s says he
   wouldn't smoke and he likes chocolate. I like chocolate too but it
   gathers around my waist... and I can't smoke it. ;-)<p>
   I hope in 2003 to get my pilot's license and by 2004 to begin assembling
   kit aircraft. I am a history buff and would love to have replica
   warbirds with laser tag systems because dogfighting with your friends
   has to be a blast!
   

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   I like to travel in the nature, and I like to climb mountains. The mountains
   are one of the reasons why I moved back to Romania.
   And another thing is listening to music.  It's great that I can do that even
   while working. ;-) The first thing I bought in Finland from my first salary
   was a Technics receiver and two 100W speakers. Unfortunately the CD prices
   are high, and I'm lucky if I can buy one in two months. The selection in Romania
   is also very limited.
</blockquote>

<a name="kommander"></a>
<p><b>12.  Any final thoughts and comments?</b>
   
   <p><i>Eric:</i><blockquote>I'd like to make a special mention of
   <em><a
   href="http://quanta.sourceforge.net/main2.php?snapfile=snap02">Kommander</a>,
   a DCOP-enabled dialog builder and executor.</em><p> Marc Britton
   and I were working with <a
   href="http://kaptain.sourceforge.net/">Kaptain</a> 0.7 and decided
   to build Kommander instead. In simple terms it uses "text
   associations" with widgets.  So you can create strings by
   manipulating widgets and output them.  You can create custom
   strings to input into an editor like Quanta for PHP functions or
   classes or you can launch programs. You can even use internal shell
   scripting for logic as well as DCOP communications and running
   scripts or programs with the dialog. Kommander dialogs are
   essentially <a
   href="http://www.trolltech.com/products/qt/designer.html">Qt
   Designer</a> UI files using standard widgets that have been
   modified to work with Kommander. The dialog designer is actually a
   stripped and modified Qt Designer run as a KDE application. It is
   even possible to create the XML for the dialog on the fly with
   scripting languages. <p>Kommander is what I call "application glue"
   because it can stitch KDE applications together into one seamless
   desktop application.  In my mind that is the next killer
   app... being able to make any app interact via user created
   dialogs. This little program gives any KDE application its own user
   dialog engine, especially useful if it has a solid DCOP
   implementation. Look for docs and demos on the <a
   href="http://quanta.sourceforge.net/">Quanta site</a>.
   <p>

   Thanks to everyone who has been so kind in their praise of Quanta. I
   would never imagine I would have the priviledge to manage a project like
   this. It is a total accident that I arrived here but now I would not
   give it up for anything. Most of all I wish I could share with everyone
   just how wonderful it feels to be part of something like this. What
   we're doing matters to people. Knowing that I am making a positive
   impact in so many lives exceeds any financial rewards. I wish everyone
   reading this could experience this. In fact you can. I encourage
   everyone to realize that being a community requires much more than just
   consuming. Someone has to produce, and that is where the real reward is.
   <p>
   It is so obvious and cliche to say "<a href="http://quanta.sourceforge.net/main1.php?actfile=donate">send in your donations</a>" but in fact
   for many of us it is so easy it doesn't really <em>cost</em> us anything.
   Right now each and every donation I've received has been meaningful and
   helpful. Thanks to all who have contributed. Help us
   make Quanta a good tool for Zope, Java and XML. Help us with templates. Find some
   small but worthwhile thing you can give back... because what I've
   learned in this experience is that the old saying really is true... it
   is better to give than to receive. What you do that we don't can become your
   own little subproject in Quanta... Your own little world.
   <p>

   Thank you everyone for taking the time to read this interview and my
   ramblings. ;-) Most of all thank you for helping to make Quanta perhaps
   the most popular Open source web development tool and certainly on
   Linux and KDE.<p>
   See you on the net...

   </blockquote><p><i>Andr&aacute;s:</i><blockquote>
   Yes, help is wanted. First we need financial help, as I wrote above. But of
   course developers are also welcome -- be sure that we will find a job for you!
<p>
   And the thank you's: first of all I would like to thank Eric, as he
   &quot;discovered&quot; me, and showed me that it's possible to fulfill my dream of making my living doing something I enjoy. I trust him and I think he is a great man for sponsoring GPL software development. More should follow his example -- I would certainly do the same if I could.<p>
   There are others to thank, most importantly my wife
   and my parents. They have accepted that I've choosen this not so secure way to earn my  living, as it's more important to feel good and enjoy life than to grow rich and depressed. Of course, this doesn't mean that I don't want to earn more money.
   :-)<p>
   To the users: Please try out the latest version of Quanta from KDE CVS, contribute and
   report bugs. If you haven't noticed yet, we now have a <a href="http://mail.kde.org/mailman/listinfo/quanta">user mailing list</a>
   
   <p>
   Thanks for inviting me to this interview.
</blockquote>
<p>
<i>Special thanks to <a href="mailto:diekmann@kde.org.nospam">Andreas C. Diekmann</a> and <A href="mailto:staerk@kde.org">Klaus Staerk</a> for sending us the English version of this interview.</i>