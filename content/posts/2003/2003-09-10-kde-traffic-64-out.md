---
title: "KDE Traffic #64 is Out"
date:    2003-09-10
authors:
  - "rmiller"
slug:    kde-traffic-64-out
comments:
  - subject: "Quite interesting"
    date: 2003-09-09
    body: "Thanks Russel!! That was a very interesting read!\n\nFrom the Traffic\n\n\"Neil responded that he is perfectly agreeable to reverting both his commit and Christoph's.\"\n\nHmmmm, What were Christoph's commits?\n\nAlso, I tried checking http://www.indigoarchive.net/gav/autorun1.jpg\nbut it's reporting an unknown host.\n\nI'm hope OO integration goes well"
    author: "Jasem Mutlaq"
  - subject: "Re: Quite interesting"
    date: 2003-09-09
    body: "Christoph was the person who committed the change to KDEs front page.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Tiepoos"
    date: 2003-09-09
    body: "A provate extension for DCOP and a polotical discussion! You might want to run the spell checker over it again before trolls complain we're not just using the letter K disproportionately, but also the O. :)\n\n"
    author: "Rob Kaper"
  - subject: "Re: Tiepoos"
    date: 2003-09-09
    body: "I noticed those today.  I'm going to submit a corrected version soon, but even then, it takes quite a while to put up :(\n\n--Russell"
    author: "Russell Miller"
  - subject: "Great read"
    date: 2003-09-09
    body: "Thank you, Russell !"
    author: "marko"
  - subject: "Re: Great read"
    date: 2003-09-09
    body: "Yeah, excellent job.  Your coverage of \"The Thread\" was objective and fair, IMHO.  "
    author: "LMCBoy"
  - subject: "Re: Great read"
    date: 2003-09-09
    body: "Thanks.  That was a particularly tough one. \n\nSome of the things that I left *out* of the article were much more telling and definitely much more amusing.  But I make it a policy not to cover flamewars, no matter how tempting.\n\nMy personal, non-editorial opinion is that Neil was way out of line, those commits should have never been made, and once they were, they should have been immediately removed once it was clear that the community at large didn't want them.  I am an american citizen myself, and I felt that Neil's commits were in poor taste and had no place in the KDE project.  But he stuck to his guns, and things played out exactly as they were going to.  But now that it has all played out, I think the KDE project is much better off.  A lot of time and energy was spent fighting battles, such as this one, that really should not have been fought, at least to the extent that this one was.\n\nA lot of hubbub was raised over the fact that the page was modified, and Neil made those commits, an then attempted to play them as equal.  It seems that the subtleties of the situation, which almost everyone else seemed able to see and distinguish, were totally lost on Neil.\n\nI don't care what the politics were.   Credits to \"old europe\", or to \"nazi germany\", etc., would have been just as out of line as Neil's were.\n\nHowever, all of the above is simply my personal opinion - as I've made clear before, my posts to the dot are not made under the auspices of my \"position\" as KDE traffic editor.  If I have something to say under those auspices, I'll put them in the newsletter itself. \n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: Great read"
    date: 2003-09-10
    body: "Russell, while I'm not going to say that I strictly agree with Neal, I can completely see his viewpoint and sympathize.  That said, IMO you did a fine job of writing it up and summarizing the situation.  It very much impressed me that you tried to explain Neal's reasoning and that you also revealed your own bias at in the note at the end.\n\nI still think that both issues (inspirational credit and the KDE Project making a position statement on legislative issues) need to be formally addressed before they come up again.  IMO, the first should not be done (so, while I agree with Neal, I also agree it should be removed!  :)  ), and the second should be discussed without a specific situation but rather creating broad guidelines.  Code changes can work with the general consensus and most active developers having the biggest say... that simply makes sense.  But the concept of \"I speak for everybody\", especially for legislative (which *are* political) issues worries the hell out of me.  Not this time, but as a general precident and position.  \n\nI am *not* saying it can't be done and that the KDE Project[1] shouldn't have official positions on issues... I just think that right now the process is very vague.  I worry about fragmentation.  Perhaps at the next big conference this would be a good topic of discussion?\n\n[1] Note that I am talking about the code/CVS/release side of things.  There are KDE associated orgs that *are* political and PR groups."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Great read"
    date: 2003-09-10
    body: "Thanks, I take my \"job\" here quite seriously.  There is a place for goofing off (although Scott Wheeler probably wishes I'd choose a very different place) and a place for being professional.  CVS commit messages and messages to the mailing list are a place to be at least moderately professional.  Editing a newsletter with such a large viewership is a place to be completely professional.  Of course, professional doesn't mean no goofiness.   It just means that, in this case, one must strive to create a product that is as in line with the rigors of journalism as possible, given my lack of formal journalism training.  I screwed up once, and got roundly slapped down for it, I'm hoping the last few issues have at least partly redeemed me.\n\nI think your points are valid, but I would counter with the point that there is only one KDE front page, and if we were to take the attitude that everyone has to agree what is posted there, nothing would get done.  There comes a time when someone has to step up and say \"your objections are noted, but this is what most of us want, and you'll just have to live with it\" - as happened here.  In this case, the decision should probably rest with the e.V.  Not as a unilateral decision making body - I hasten to say - but *someone* has to break the stalemate.\n\nHowever, the commits that Neil made were made unilaterally, and to the vociverous objection of what was obviously a large majority of the KDE developer community.\n\nThis, to me, is what makes the action of redirecting the webpage acceptable, and Neil's commits unacceptable - even setting entirely aside my visceral distaste for Neil's commits (blind patriotism - unquestioning acceptance of the decisions of leaders, etc., is supremely annoying to me and has no place in a free society).  The spirit of KDE policies, if not the letter, were followed in all cases, and I'm perfectly satisfied with how it was handled.\n\nThat said, there should have been nothing to handle in the first place.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: Great read"
    date: 2003-09-10
    body: ":: I think your points are valid, but I would counter with the point that there is only one KDE front page, and if we were to take the attitude that everyone has to agree what is posted there, nothing would get done. There comes a time when someone has to step up and say \"your objections are noted, but this is what most of us want, and you'll just have to live with it\" - as happened here. In this case, the decision should probably rest with the e.V. Not as a unilateral decision making body - I hasten to say - but *someone* has to break the stalemate.\n \n    I do not disagree with using the KDE front page to counter software patents.  I'd vote for it.\n\n    My only point is that the *process* for political decisions (i.e., non-technical) is vague for the KDE Project (as opposed to e.V.).  And the same processes that work well with technical code decisions do not apply to non-technical decisions.  Especially when you have cases that appear to some people to be \"we'll allow this one because we like it\" vs. \"we dislike this\", no matter how objective everybody really is.  \n\n    Basically, KDE should take stances on certain issues and stay away from everything else (lest it lose its focus on being a desktop environment).  The line should be clearly stated and generally agreed to.  Some people will always grumble, yes, but there is no clear statement on the issue.  The biggest reason I think a statement should be agreed to is so that the KDE Project can focus on code and releases.  As I say, there are already KDE related organizations whose focus is discussing PR and politics.\n\n---\n\nNow, having said all that, I might as well chuck in my opinion.  Personally, I would say that KDE should be agressively non-political except where it affects the actual KDE.  I think that Neal's comments should have been removed.  The patent issue, affecting the actual desktop environment, should have been done.\n\nSo I'm happy with how everything turned out.  I just think the process of getting there was horrible and was a needless aggravation to everybody involved.  Not to mention the loss of a talented author from the official CVS which occured solely due to unclear direction.\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Great read"
    date: 2003-09-10
    body: "I would have agreed with everything until sometime ago.\nSome people explained (and it seemed to make sense) that Neil only did that with\nthe credits, not becouse he thought it was right, but to (somehow) try to prove\na point - in other words, he had chosen that example to show that we should not\ngo that way. It was the american army but he could have chosen some other stuff\nthat just didn't seem right. Hey I think the analogy just isn't valid but\notherwise everything seems ok.\nBut then he kept insisting in that \"american army\" stuff, and posting stupid\ncomments like \"KDE is anti american\" on his blog. Now that is not trying to\nprove a point, it's just acting maniac."
    author: "Sleepless"
  - subject: "Re: Great read"
    date: 2003-09-10
    body: "> CVS commit messages and messages to the mailing list are a place to be at least moderately professional. \n\nThe fact is that many emails sent to the lists have political slogans attacted as signatures. One can guage the political feelings of the writers quite easily. But they have no bearing on the discussions, are rarely commented on. The discussions are about the software (most of the time). Russell and myself may quote an email, but wouldn't include the signature in our reports. It adds nothing, is off topic.\n\nSome in this discussion have said that the kde community should codify these things more, similar to the Debian project. To be truthful, this very idea is what irritated me the most of this situation. Rules replace thought. I like the freedom that the kde community represents. It is amazingly open to contributions from all sorts. And from time to time someone will step over a line and be corrected.\n\nI'm sure those who set up the home page protest have since thought long and hard about this as a result of the fuss. And they probably would do exactly the same thing again.\n\nDerek"
    author: "dkite"
  - subject: "Re: Great read"
    date: 2003-09-10
    body: "\"Credits to \"old europe\", or to \"nazi germany\", etc., would have been just as out of line as Neil's were.\"\n\nDo you really see a comparison between credits to Nazi Germany and Neils? Gee, I'm glad you kept your personal opinion out of the Traffic..."
    author: "David Johnson"
  - subject: "Re: Great read"
    date: 2003-09-10
    body: "Of course I do.  They're both personal, political opinions.  The relative \"evilness\" of the different regimes has nothing to do with it.\n\n--Russell"
    author: "Russell Miller"
  - subject: "The newsforge article"
    date: 2003-09-10
    body: "The newsforge article Russel referred to was by me, and following an email from a KDE developer who I shan't name, I want to make one thing clear: I did not mean to suggest that KDE is apolitical, or deals with political and social matters poorly. In fact, I wanted to stress how well KDE dealt with the software patents and Neil Stevens episodes.\n<p>\nJust so everyone knows :)\n<p>\nOh, and the article can be read here: <a href=\"http://newsforge.com/newsforge/03/09/02/1930234.shtml?tid=51\">http://newsforge.com/newsforge/03/09/02/1930234.shtml?tid=51</a>"
    author: "Tom"
  - subject: "Re: The newsforge article"
    date: 2003-09-10
    body: "I thought your article was a nice read (someone posted it to KDE Cafe).  So you're the one who made the KFirewall and KHumanRIghts jokes, right?  :-)"
    author: "Navindra Umanee"
  - subject: "Re: The newsforge article"
    date: 2003-09-10
    body: "Guilty :-)"
    author: "Tom"
  - subject: "Re: The newsforge article"
    date: 2003-09-11
    body: "FWIW, I was on the look out for real trolls/troublemakers/abusers on that article not to get someone honestly trying to make a point. :-)"
    author: "Navindra Umanee"
  - subject: "CRZ?"
    date: 2003-09-10
    body: "Can somebody explain the CRZ joke?"
    author: "AC"
  - subject: "Re: CRZ?"
    date: 2003-09-10
    body: "I will be the only one who is able to.\n\nOn #kde-devel while I was writing traffic, crz (who is usually crz|unhappy) asked me to put a little blurb in about him drinking coffee.  He was just joking, but I thought it was funny, so I decided to make it a running gag, with his blessing.  I thought it was just the right mixture of goofiness and, well, goofiness.\n\nHe was fully aware I'd be pulling something like this, so he's not an unsuspecting victim, and it was all in good fun.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: CRZ?"
    date: 2003-09-10
    body: "Well, i enjoyed it (-:\n\nOne thing i still dont know tho: who is crz, asin whats his/her full name?\n\n- Jeff\n"
    author: "JS"
  - subject: "Re: CRZ?"
    date: 2003-09-10
    body: "> Well, i enjoyed it (-:\n\nI thought it was good too but I really thought it got tired after about the 4th reference..."
    author: "Anon"
  - subject: "Re: CRZ?"
    date: 2003-09-10
    body: "Then it wouldn't have been a running joke.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: CRZ?"
    date: 2003-09-10
    body: "Humor will always have people who did not like it.  If you're secure, ignore it.  As long as it doesn't interfere with communicating your point, light humor is appreciated by most people.\n\n(And since I was drinking coffee as I read it, it was doubly amusing)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: CRZ?"
    date: 2003-09-10
    body: "Thanks.  I'm not worried about it, really.  The same people who complain about all of the funny stuff I post (and I'm not necessarily referring to any parent poster of this thread, it getting tired towards the end is probably a valid criticism, though not one that I agree with) would turn around and complain that it was dry and boring if I decided to remove all of the goofiness.  Some people are not happy unless they can find something to complain about, and are not happy unless they can make themselves look so much better than others.\n\nPerhaps it's just a result of inadequacies down below (or up top, for women).\n\nHaving a \"thick skin\" has been a difficult lesson to learn.  But when one is visible in the open source community, you either grow one or you just give up.  So far giving up has not seriously crossed my mind.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: CRZ?"
    date: 2003-09-11
    body: "Well, my own opinion is that if it's a private joke shared between 2 or 3 people, it shouldn't go on for *that* long in a public digest. If it's shared amongst a whole bunch of people however, then fair play.\n\nMind you, I'm probably a bit odd in that I don't really think jokes and stuff belong in places like kde-traffic. Each to their own though, I guess.\n\nI'm not sure if you'll take this as criticism or not, it's not really meant as such - more a reminder that different peoples senses of humour are *vastly* different. Or something."
    author: "Stephen Douglas"
  - subject: "Re: CRZ?"
    date: 2003-09-11
    body: "Actually, I agree with your first statement, except it wasn't a private joke.  A private joke would have been one that no one would have gotten at all and wouldn't have found funny without context.  This was a joke *initiated* in private, but very public.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: CRZ?"
    date: 2003-09-10
    body: "LOL!\n\nI loved it!"
    author: "Joergen Ramskov"
  - subject: "3.2 Work-in-Progress Knoppix ISOs?"
    date: 2003-09-10
    body: "What about that project to create monthly Knoppix ISOs with KDE HEAD? Still alive?\n\n(Note: I did post that to the thread about this weeks CVS Digest as well, but I appeared late in the game and got no audience, so I'm taking my chances over here again. Last time, I promise.)"
    author: "Sho"
  - subject: "Re: 3.2 Work-in-Progress Knoppix ISOs?"
    date: 2003-09-10
    body: "I'm currently working at that though I don't yet know if I've got time enough at the office to do that. At least I'm trying to catch up with the debian dirs so packages can be made out of HEAD."
    author: "Ralf Nolden"
  - subject: "Re: 3.2 Work-in-Progress Knoppix ISOs?"
    date: 2003-09-10
    body: "Sounds cool! Thanks for the effort."
    author: "Sho"
  - subject: "Re: 3.2 Work-in-Progress Knoppix ISOs?"
    date: 2003-09-10
    body: "I recently ran accross this DragOS distribution that aims to deliver a recent KDE CVS LiveCD:\nhttp://dragos.berlios.de/"
    author: "ac"
  - subject: "Re: 3.2 Work-in-Progress Knoppix ISOs?"
    date: 2003-09-10
    body: "Gonna look at that, thanks :)."
    author: "Sho"
  - subject: "Re: 3.2 Work-in-Progress Knoppix ISOs?"
    date: 2003-09-10
    body: "That would be GREAT!! "
    author: "Sleepless"
  - subject: "KDE Traffic"
    date: 2003-09-10
    body: "Thanks Russel. What I like most about Traffic is that it is written as a story with humor and personality rather than just a list of facts and quotes like Digest. \n\nDon't get me wrong, I like the DIgest as much as Traffic, they are very different in the way the information is presented and digest has a wider scope. I just want you both to continue writting the same way you do.\n\nNow about the Theme Manager, I\"m all for replcing ti too, check out what it did to my KDE: http://bugs.kde.org/show_bug.cgi?id=62801 yuck \n\nHere's what I think needs to be improved: http://bugs.kde.org/show_bug.cgi?id=55710"
    author: "Alex"
  - subject: "Re: KDE Traffic"
    date: 2003-09-10
    body: "Hi,\n\nagain a Troll. We need not compare different things. Traffic and Digest are both great.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: KDE Traffic"
    date: 2003-09-10
    body: "The design of the Digest should get changed, I think it's not good to read."
    author: "CE"
  - subject: "Re: KDE Traffic"
    date: 2003-09-10
    body: "OK, so you don't like the format as it is.  Kindly tell me how you would like it changed.  Otherwise, your complaint is entirely useless to me.\n\nThanks.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: KDE Traffic"
    date: 2003-09-11
    body: "RUssel, ALex's comment was a complement to both you and Derek, nobody said they didn't like the format of traffic, jsut one person said they didn't like the format of the (CVS)Digest."
    author: "Adi"
  - subject: "Re: KDE Traffic"
    date: 2003-09-11
    body: "I misread.  Sorry.\n\nI still think that complaints that have no suggestions for improvement are useless.  But I'll let Derek deal with that, since it was the Digest the complaint was directed at.\n\nEven though they look a lot the same...\n\n--Russell"
    author: "Russell Miller"
  - subject: "Re: KDE Traffic"
    date: 2003-09-11
    body: "I think the format of the Traffic is alright.\n\nI don't know what changes would increase legibility.\nPerhaps a usability expert should be asked. ;-)"
    author: "CE"
  - subject: "Re: KDE Traffic"
    date: 2003-09-12
    body: "How so? Please explain.\n\nDerek"
    author: "Derek Kite"
  - subject: "Spaguetti"
    date: 2003-09-10
    body: "Hey, thanks! I'll add it on the next release :)"
    author: "uga"
  - subject: "Respect"
    date: 2003-09-10
    body: "After having harshly critized Russel for #59 I must say that I enjoy and am pleased with the size and quality of the latest KDE Traffic issues. And a wonder too that he additionally found the time to actively hack on KDE."
    author: "Anonymous"
  - subject: "Re: Respect"
    date: 2003-09-10
    body: "I'm glad to hear that I've converted you :-)\n\nThere is a price, however, to hacking, and I'm starting to understand what it is, my priorities are a little wacked.\n\n--Russell"
    author: "Russell Miller"
  - subject: "kde theme manager"
    date: 2003-09-10
    body: "so its going to be replaced with another theme manager is it?\nhopefully that wont mean they will be all those seperate theme related modules in kcontrol...\nor is anybody going to bother with make entire themes?... anybody? meh..."
    author: "not_registered"
  - subject: "Devices kicker applet"
    date: 2003-09-10
    body: "Thanks Russell for KDE Traffic!\n\nJust for information, the devices kicker applet\nis in KDE CVS now and is integrated in kdebase.\n\nI have a CVS account now, and I'll contribute more. ;-)\n\nRegards."
    author: "K\u00e9vin \"ervin\" Ottens"
  - subject: "Re: Devices kicker applet"
    date: 2003-09-12
    body: "Are there any plans for a similiar thing for the network cards?  Very much like how windows does it, showing a ballon when the network lead comes unplugged, or speed changes etc.\n\nThe information is there (run mii-tool) but it would be nice to be used in the kde gui :)\n"
    author: "John Flux"
  - subject: "Re: Devices kicker applet"
    date: 2005-01-07
    body: "KNemo does this: http://kde-apps.org/content/show.php?content=12956"
    author: "Nike"
  - subject: "agreeable"
    date: 2003-09-10
    body: "wow. that issue sure makes me sound like an agreeable, up-beat sort of person. to which i must agree that i often am like that. ;-) my \"Aaron is a Crotchety Grump\" persona is going to become completely unbelievable if this sort of reporting continues, which is, obviously, why i started the \"remove kthememanager\" crusade. wouldn't want people thinking i'm going soft, or something ;-)\n\nas for what made it into CVS, bot the datepicker patch and the devices made it in."
    author: "Aaron J. Seigo"
  - subject: "KDatePicker not working"
    date: 2003-09-10
    body: "KDatePicker is not working, I still have no date."
    author: "ac"
  - subject: "kdepim HEAD"
    date: 2003-09-11
    body: "seems really broken ;-( \nkmail won't display messages, kontact won't start... \n\nargh!"
    author: "rjw"
  - subject: "Looking forward to Cuckoo!"
    date: 2003-09-15
    body: "Ximian OpenOffice is nice; integration with my desktop of choice would be preferable. :-D"
    author: "Shane Simmons"
---
<a href="http://kt.zork.net/kde/kde20030907_64.html">KDE Traffic #64 has been released</a>, with lots of news articles about KDatePicker, iCalendar, OpenOffice integration in KDE, the software patent fracas, a neat little devices applet for Kicker, a private extension for DCOP, and of course, a nice treat at the end.  Get it at <a href="http://kt.zork.net/kde/latest.html">the usual place</a>.
<!--break-->
