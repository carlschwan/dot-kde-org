---
title: "Tonight Live: The KDE Desktop on The Linux Show!!"
date:    2003-12-10
authors:
  - "wbastian"
slug:    tonight-live-kde-desktop-linux-show
comments:
  - subject: "02:00 GMT"
    date: 2003-12-10
    body: "For other clueless Europeans like myself, 2am GMT may help. Or not. Hopefully it does!\n\nCurrently I'm listening to a very quiet American guy talk about something, I think it's Linux but I can't concentrate on what he's saying..\n\nI'll tune in tomorrow, I can't stay up tonight. Well maybe I could.. gaa! How come KDE always keeps me up all night?!"
    author: "Max Howell"
  - subject: "Re: 02:00 GMT"
    date: 2003-12-10
    body: "if you can't stay up, don't worry... they replay each week's show continuously until the next week's show, so you can grab it whenever (that quiet American guy you hear right now is quite likely Bruce Perens or ESR from last week's show). of course, by grabbing the repeat feed you can't join in on the IRC chat or experience the excitement of being part of a live experience. but sleep is usually far more interesting than either of those things, especially during the work week. ;-)\n\noh, and KDE keeps you up all night because it loves you.  ;-) \n"
    author: "Aaron J. Seigo"
  - subject: "Licenses"
    date: 2003-12-10
    body: "I wasn't aware that KDE was all released under the GPL/LGPL; I thought there were lots of components under BSD and others. A bit of a slip there in the announcement!"
    author: "Tom"
  - subject: "Re: Licenses"
    date: 2003-12-10
    body: "yeah... we didn't write the announcement, the Linux Show guys did... there's a few phrases i kind of winced at when reading it... but it's generally cool... and it's a pretty good opportunity to address some of The People directly."
    author: "Aaron J. Seigo"
  - subject: "peercast"
    date: 2003-12-10
    body: "Hey all the servers are sux0rd. Can someone mirror this on peercast p2p radio plz?"
    author: "vod"
  - subject: "Re: peercast"
    date: 2003-12-10
    body: "try the realplayer stream at http://stream.linuxquestions.org:8000/linuxlive"
    author: "Aaron J. Seigo"
  - subject: "thanks!"
    date: 2003-12-10
    body: "I caught you just in time!"
    author: "vod"
  - subject: "Good Show"
    date: 2003-12-10
    body: "I listed in, it was a good show.\n\nConcise, accurate and clear. Well done guys!\n\n"
    author: "Leonscape"
  - subject: "Bad timing"
    date: 2003-12-10
    body: "Man... 9 EST is when the 2nd of SciFi's remake of Battlestar Galactica aired.  Don't make geeks choose! :)"
    author: "Brendan Orr"
  - subject: "Re: Bad timing"
    date: 2003-12-10
    body: "hehe.. no doubt.. priorities, gawd dammit! you can listen to it later, though... they play it on repeat loop all week... \n\nscrew andy warhol. gimme 10080 minutes of fame! ;-)"
    author: "Aaron J. Seigo"
  - subject: "Archive Link"
    date: 2003-12-10
    body: "http://www.thelinuxshow.com/archives/2003/tls-12-09-2003.mp3"
    author: "Anonymous"
  - subject: "Insightful"
    date: 2003-12-10
    body: "This interview was great, I learned a lot about KDE and it's always nice to see some insider information. I love reading what developers have to say, such as on their blogs and listening to them is even better!\n\nI think Aaron and George have done a great job, very detailed and helpful answers. Not superficial and brief like some other interviews I've seen. However, I was dissapointed that the hosts didn't seem to transmit any real energy, for any part of the show, not just KDE>\n\nAlso you can find the full show here: http://www.thelinuxshow.com/archives/2003/tls-12-09-2003.mp3\n\nThe entire thing is in the MP3 format, sorry redhat users =p, and the KDE segment starts about 55% through and takes up the rest of the show."
    author: "Alex"
  - subject: "Re: Insightful"
    date: 2003-12-10
    body: "> the KDE segment starts about 55% through\n\nWith minute 54 to be more informative. :-)"
    author: "Anonymous"
  - subject: "Re: Insightful"
    date: 2003-12-10
    body: "Dang.  Transcripts, anyone?  :)"
    author: "Navindra Umanee"
  - subject: "From Aaron's blog"
    date: 2003-12-10
    body: "http://www.kdedevelopers.org/node/view/265 :\n\nThe Linux Show went rather well tonight. George was a great co-guest, and I think we handled the questions well in general. We maintained a positive and factual yet enthusiastic air throughout. Even the SUSE/Novell/Ximian and User Linux queries went well. They had a record number of people attending tonight; even the IRC channel had ~80 people in it (they usually only have ~50). So, they were happy with it too. Good fun and a great way to get KDE some positive exposure. I?m feeling rather spent now, though. Time to find some wine and a good book.\n\nYay!  Nice jobbie!"
    author: "ac"
  - subject: "awesome interview"
    date: 2003-12-10
    body: "Both Aaron and George are really well-spoken!  I am really enjoying this interview!\n\nAnyone know what happened when Aaron was cut off?  heheh."
    author: "anon"
  - subject: "Re: awesome interview"
    date: 2003-12-10
    body: "i was abducted by aliens, obviously. ;-) honestly, i have no idea. ++weird.\n\nbtw, we just got an email from one of the show hosts and apparently the show has been a great success... 1000s have listened to the show since it aired some 21 hours ago, and their website visitation has spiked noticeably since the show as well... =)"
    author: "Aaron J. Seigo"
  - subject: "World Summit on The Information Society"
    date: 2003-12-10
    body: "At the UN world summit the free internet cafe is based on KDE/knoppix with Mozilla and Open Office.org"
    author: "WSIS-koord"
  - subject: "Re: World Summit on The Information Society"
    date: 2003-12-11
    body: "documentary photos would be cool =)"
    author: "Aaron J. Seigo"
  - subject: "kiosk"
    date: 2003-12-10
    body: "Now I know how kiosk is pronounced in English. Very different from the Swedish pronounciation (it's spelled the same way). ;)\n"
    author: "Apollo Creed"
  - subject: "Re: kiosk"
    date: 2003-12-10
    body: "Give me a clue on Swedish \"kiosk\" remembering the English word \"ghoti\"\n\ngh as in \"enough\"\no as in \"women\"\nti as in \"station\"\n\n(George Bernard Shaw got there first)"
    author: "Gerry"
  - subject: "Re: kiosk"
    date: 2003-12-11
    body: "Well, \"kio\" in Swedish sounds like \"sho\" in \"shot\", while \"sk\" is pronounced the same."
    author: "Apollo Creed"
  - subject: "Kiosk Mode"
    date: 2003-12-10
    body: "I listened to the recording earlier today and was really very impressed with how both George Staikos, and particularly Aaron Seigo represented themselves and KDE. Really very professional, and exciting; what a shame the presenters aren't made of the same metal.\n\nOne thing I was really surprised about was Kiosk mode. I had heard the name but assumed it was something to do with running KDE inside little cabinets.\n\nI really feel you have the wrong name here. Aaron mentioned that most people do not seem to be aware what a winner this technology is, and I think it's the name that's the problem. Sadly, kiosk != centralised enterprise desktop configuration. It's a silly thing really, but if one of your messages is simplified enterprise desktop management, you need to say that loud and clear.\n\nPerhaps the name was chosen because it is almost relevant and has a K in it? How about something like KEnterprise Desktop Manager instead? There is no confusing that!"
    author: "Dominic Chambers"
  - subject: "Re: Kiosk Mode"
    date: 2003-12-11
    body: "not in name but in spirit it is... read what its about and stay tuned ;)"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Kiosk Mode"
    date: 2003-12-11
    body: "you are, IMHO, correct. i believe that with time the term \"Kiosk\" will likely recede from public view to remain primarily as a developer's detail. as Ian said: stay tuned.\n\nas to where the name \"Kiosk\" comes from, the Kiosk framework started with work done in response to people's needs to run KDE in kiosk environments in public places. in such environments, you need to lock down the system and control it completely. thus was born Kiosk. it has come a long ways since.\n\ni'm really happy that you, and many others, enjoyed the interview... hopefully we'll be able to make such events a more regular part of KDE's public persona."
    author: "Aaron J. Seigo"
  - subject: "Why APPS.KDE.COM IS DOWN!"
    date: 2003-12-11
    body: "Andreas, the maintainer of the website sounds off:\n\nhttp://lists.kde.org/?l=kde-www&m=107099152930002&w=2\n\nBy the way, I don't think using the FM website is a good idea unless it won't use the same design. I want something that is only KDE/Qt and that is more feature rich than FM.net Possibly with some better organization and sorting (such as by downloads, popularity, activity, release date etc.) I also don't like the design, it is too impersonal and not enough eyecandy =p"
    author: "Alex"
  - subject: "zzzzz"
    date: 2003-12-11
    body: "They all sounded like they were about to fall asleep! They were speaking so slooowwwlyyyy... Maybe they're all just wrecked for some reason or other."
    author: "Anonymous"
  - subject: "Really Impressed.."
    date: 2003-12-11
    body: "with Aaron's articulation. The KDE league needs to sponsor this man to go and talk about KDE everywhere. I bet we could even sell KDE to Microsoft if Aaron was the pitchman :-)\n\nThe interviewers need to study how Aaron talks.  :-) Please take this as constructive criticism but if they are reading this the interviewers need to :\n\n- stop coughing in the mic and trying to muffle it up.. just say \"excuse me\" instead. aaron did this once and he said \"pardon me.\" \n- be a bit more quick witted. notice how aaron had always something to say? dead silences aren't good.\n- respond a bit better to what the interviewiees are saying. follow up on stuff that people say rather than relying on a list. \n- stutttttering is bad in talk shows :)"
    author: "anon"
  - subject: "Re: Really Impressed.."
    date: 2003-12-11
    body: "> The KDE league needs to sponsor this man to go and talk about KDE\n> everywhere.\n\nthanks for the vote of confidence =) however, shipping me all over hell's half acre to yak about KDE is probably not a realistic option right now, baring  someome who steps forward with funding and some speaking opportunities. but i'm more than happy to do interviews/roundtables via phone or other teleconferencing means. if you line up a speaking engagement and you give me ample time to schedule around it, i'll do it. \n\nof course, i still won't turn down in-person speaking engagements if the travel & loding is paid for ;-)\n\n> I bet we could even sell KDE to Microsoft\n\nhrm... is that harder or easier than selling ice to Eskimos? ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: Really Impressed.."
    date: 2003-12-12
    body: "> hrm... is that harder or easier than selling ice to Eskimos? ;-)\n\nEasier..\n\nEskimos have ice, MS doesn't have a decent desktop ;)"
    author: "Leonscape"
  - subject: "Re: Really Impressed.."
    date: 2003-12-12
    body: "lol... POINT!"
    author: "Aaron J. Seigo"
  - subject: "Re: Really Impressed.."
    date: 2003-12-12
    body: "> with Aaron's articulation. The KDE league needs to sponsor this man to go and talk about KDE everywhere. I bet we could even sell KDE to Microsoft if Aaron was the pitchman :-)\n \nI've always been impressed with Aaron. He has an incredibly solid grasp of the technical aspects and very good insticts. He's also usually excellent with his diplomacy, though like me, he's occasionally too hot blooded for casual conversation. I would personally love to be partnered with Aaron (or George for that matter) in some sort of KDE presentation. While George presents himself well Aaron is overtaken by his enthusiasm, something I enjoy as others have noted the same about me, often in entertaining ways.\n\nI do have to note that in KDE circles I'm probably the guy with the most sales experience, though George and Aaron have more specific to KDE. Still, I thrive on the ultimate pitch... but due to my attitude I'd probably just make the MS guys feel really inadequate with the \"take it away\" tactic and then say \"what the heck\" and make them beg. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Really Impressed.."
    date: 2003-12-13
    body: "You do like talking about yourself a lot, however ;-). Admit it! :-D"
    author: "Eike Hein"
  - subject: "Re: Really Impressed.."
    date: 2003-12-13
    body: "And if we can get you to quit talking about Quanta so much, maybe we would put you in that position.\n\nYours Truly,\n\nAnonymous Coward"
    author: "AC"
  - subject: "Re: Really Impressed.."
    date: 2003-12-13
    body: "hehe... actually, we need to find an Eric Laffoon for every important KDE application. it's notable how the involvement of someone like Eric, when coupled with excellent and dillagent developers, can really elevate a project to the next level."
    author: "Aaron J. Seigo"
  - subject: "Re: Really Impressed.."
    date: 2003-12-13
    body: "I mean, don't get me wrong, Eric rocks - I love Quanta, I enjoy how he drives both the developers as well as the user community. I'm getting wonderful software for free, and I'm grateful. Rock on, Eric!\n\nHowever, I some criticism applies IMHO. You've typed \"one of the most important apps in open source [...]\" and \"Thinking about how I work on Quanta, the best thing since sliced bread [...]\" a little bit *too* often - it's beginning to sound like a broken record. You really don't have to do that in the first place! Quanta speaks for itself. \n\nWell. To make things easier, we should probably all donate more so the Quanta team doesn't have to worry about funding as much."
    author: "Eike Hein"
  - subject: "Re: Really Impressed.."
    date: 2003-12-13
    body: "Agreed - Aaron was great. You got the feeling he knows what he's talking about, he spoke very clearly, seemed honest and fully prepared. Good job!"
    author: "Eike Hein"
---
NEWS ALERT: In tonight's edition of the The Linux Show our beloved KDE desktop
will be featured and KDE developers George Staikos and Aaron J. Seigo will join in. It all starts LIVE at 6pm PST / 7pm MST / 8pm CST / 9pm EST at
<a href="http://www.thelinuxshow.com">www.thelinuxshow.com</a>.
<b>Update:</b> The archived version of the show is now available <a href="http://www.thelinuxshow.com/archives/2003/tls-12-09-2003.mp3">for download</a>.

<!--break-->
<p>
From the announcement:
</p>
<h4>A Look at KDE from the top</h4>
<p>
Tonight we are going to joined by George Staikos and Aaron J. Seigo, two 
of the most currently active participants in the KDE project. KDE is a 
network transparent contemporary desktop environment for UNIX/Linux 
workstations. The KDE project is a large open group of developers 
consisting of several hundred software engineers from all over the world 
committed to free software development. The KDE project is a free 
software project. Each and every line of KDE code is made available 
under the LGPL/GPL licenses. This means that everyone is free to modify 
and distribute KDE source code. This implies in particular that KDE is 
available free of charge to anyone and will always be free of charge to 
anyone.
</p>
<p>
Aaron has been a UNIX developer since the early 1990s and Linux user 
since 1994. Today Aaron works almost exclusively with Free Software. 
After the release of KDE2, Aaron began contributing to the KDE project 
via the weekly Kernel Cousin KDE (now KDE Traffic) and source code 
patches. Today he maintains several applications and classes in KDE, is 
active in the KDE usability project and helps out with project PR from 
his home in Calgary, Alberta.
</p>
<p>
George Staikos is a professional software developer and Linux hobbyist 
from Toronto, Canada. (Two Canadians in one show, WOW!) He has been 
using and developing for Linux since 1993, and has been a KDE developer 
since 1999. He is presently a member of the core KDE development team 
and the North American press contact for KDE. George is self employed, 
developing Linux, KDE, and generally portable GUI and web software.
</p>
<p>
KOffice is a free, integrated office suite for the KDE desktop. The 
application suite consists of a word-processor, spread-sheet, 
presentation application, organizer and more. For more information about 
KOffice visit <a href="http://www.koffice.org">http://www.koffice.org</a>.




