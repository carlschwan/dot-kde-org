---
title: "KDE Traffic #69 is Out"
date:    2003-12-02
authors:
  - "prockai"
slug:    kde-traffic-69-out
comments:
  - subject: "Well"
    date: 2003-12-01
    body: "There must be people who really do not want KDE to succeed, for various reasons.\nEverybody knows to be nice when they get something for free."
    author: "OI"
  - subject: "Wow"
    date: 2003-12-01
    body: "I was surprised we are now in KDE traffic. Now I will have to be nice on the Quanta developer list. Darn. ;-)"
    author: "Eric Laffoon"
  - subject: "I did not find the first message offensive"
    date: 2003-12-01
    body: "I did not find the first kmail message offensive and I was surprised by Ingo's reply. Sure that Simon guy made a mistake but I did not think his message was really critical, misguided yes, but not critical. The poster did not say Outlook was better than Kmail. \n\nOk Simon should not have posted this message on the mailing list and I am pretty sure that Ingo is pretty tired of having tons of poeple bugging him with such and such features. Ingo had every right to reply in a \"freezingly polite way\" has the author puts it....\nbut calling that person a \"fuckhead\" ? \n\nThat was a bit harsh in my opinion. \n\nDon't get me wrong I fully support the dedication of the KDE team and I am very thankful for KDE, but maybe Ingo was a bit cranky when he wrote that email."
    author: "Bleakcabal"
  - subject: "Re: I did not find the first message offensive"
    date: 2003-12-01
    body: "It's the user that called Ingo a \"fuckhead\".  No need for your rant!"
    author: "anonymous"
  - subject: "Re: I did not find the first message offensive"
    date: 2003-12-01
    body: "Ah sorry if I have mistaken the posters I tought that Ingo posted his reply and then the \"fuckhead\" remark off the list on top of that.\n\nSorry to Ingo and everyone then....\n\nThat Simon guy is really dumb then."
    author: "Bleakcabal"
  - subject: "Re: I did not find the first message offensive"
    date: 2003-12-01
    body: "> Don't get me wrong I fully support the dedication of the KDE team and I am very thankful for KDE, but maybe Ingo was a bit cranky when he wrote that email.\n\nheh.. David Hyatt had a very good piece on this a few days ago on his blog. \n\nhttp://weblogs.mozillazine.org/hyatt/archives/2003_11.html#004358\n"
    author: "anon"
  - subject: "Re: I did not find the first message offensive"
    date: 2003-12-02
    body: "I take back everything I said, I had not properly understood the text in question. I feel sorry for my post. "
    author: "Bleakcabal"
  - subject: "Re: I did not find the first message offensive"
    date: 2003-12-02
    body: "you don't need to feel sorry. The KDE traffic article is at fault. I first understood the same, i.e. that Ingo had sent 3 messages as answers to the original poster, each message being worse."
    author: "oliv"
  - subject: "Ax"
    date: 2003-12-01
    body: "I think you mixed the persons involved..."
    author: "OI"
  - subject: "Re: Ax"
    date: 2003-12-02
    body: "Yes I did, sorry for my mis-interpretation, that changes everything I sympathise with Ingo and feel disgusted by this Simon guy."
    author: "Bleakcabal"
  - subject: "Re: Ax"
    date: 2003-12-02
    body: "Yeap he did :)\n\nStill I don't think the answers on the mailing list were the best. I do find the answers fair, but not a good choice. Why? Just see what happened... I bet he even liked appearing on KDE Traffic, that's not good.\nIMO a better choice would have been either:\n- just one short, almost automatic sounding, message saying it's off topic go to bugs.kde.org\n\nor\n\n- completely ignore him.\n"
    author: "Src"
  - subject: "apps.kde.com????"
    date: 2003-12-01
    body: "What has happened to appsy? Or should I not ask?"
    author: "a.c."
  - subject: "Re: apps.kde.com????"
    date: 2003-12-02
    body: "It's been down and up recently. Too bad dre has been quite busy lately to work on KDE stuff recently =("
    author: "anon"
  - subject: "Re: apps.kde.com????"
    date: 2003-12-02
    body: "All his domains seem to be down so presumably there are server problems or ISP issues.  That last downtime was due to the ISP moving their datacenter.  Dunno about this one."
    author: "Navindra Umanee"
  - subject: "Re: apps.kde.com????"
    date: 2003-12-07
    body: "I asked Dre about updates to apps.kde.com recently and he said that he might have to shut down the site.\n\nSince there isn't even an entry anymore for apps in the kde.com DNS, I suppose that's exactly what happened.\n\n"
    author: "Rob Kaper"
  - subject: "Re: apps.kde.com????"
    date: 2003-12-07
    body: "ouch.. hopefully if Dre has to stop running it, it can become a community-run server or something. A lot of other sites and users relied on apps.kde.com. "
    author: "anon"
  - subject: "Wiki Link"
    date: 2003-12-01
    body: "http://kde.ground.cz/tiki-index.php?page=Adding+KDevelop+Projects+to+the+KDE+CVS+Repository is now called http://kde.ground.cz/tiki-index.php?page=How+To+Use+KDevelop+with+KDE+CVS *shrugs*"
    author: "Datschge"
  - subject: "Thanks, Peter and Henrique"
    date: 2003-12-02
    body: "Thanks for an excellent and very interesting KDE Traffic.  It's the perfect complement to the CVS Digest!"
    author: "Haakon Nilsen"
  - subject: "You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "\"When will KMail come with native HTML writing support? I'd really love to be able to write the same kind of emails in KMail that I do with Outlook for Windows.\" \n\nchanged to->\n\n\"Hey Fuckhead,\n\nHow long has this been the most requested feature? Get off your ass and get it done. And please refrain from sending ME your trifling criticisms.\"\n\nThis Simon person never intended his first e-mail as a question, rather as a criticism of KDE and tried to make KDE's PIM facilities seem behind the times. \n\nHe not only posted it in the wrong place, instead of bugs.kde.org of which this is obviously the most requested feature for Kmail, but he also knew the answer himself. He knew that this was the most requested feature, so he must of looked at the bug report, and from the responses to the bug report it is clear that the earliest it will make it in KDE is 3.3. If the bug report wasn't enough he could look at the KDE 3.2 and 3.3 feature plan to get his question answered.\n\nThe only point of his message was to demoralize developers, discourage work on KDE's PIM in general and just outright insult the KDE community of developers. \n\nHe not only used intense profanity but threw out all kinds of insults as if Ingo worked for him or had any obligation whatsoever to him.\n\nFrom this behaviour I can only come to two conclusions:\n\nA. He was pretending to want KDE to become better but his true intentions were to destroy it. After all you should always keep your enemies closest. Perhaps he is hired by Microsoft.\n\nB. He is socially inept and was on heavy drugs.\n\nI greately appreciate the work done by KDE developers and their committment to the community. This is why every time there is a new KDE release I will donate at least $25 but I think if I were to buy something comparable to it I would have to pay at least $50. \n\nEVERYONE who uses KDE should donate as much as they can afford or think it will cost in the commercial world. KDE developers give you KDE for free, but THAT DOES NOT MEAN DEVELOPING IT IS FREE!!! Think of open source as the first place where you are trusted enough to pick the right price for something."
    author: "Mario"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: ">  it is clear that the earliest it will make it in KDE is 3.3.\n\nI think the current plan is to have another release of kontact between 3.2 and 3.3, with things like HTML editing, disconnected IMAP fixes, exchange and kolab support, etc.. Probably a few months after 3.2 is released. \n\nFor anybody actually working on said apps, correct me if I'm wrong."
    author: "anonY"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "I think disconnected IMAP is already fixed on CVS. We'll probably see code for HTML editing on KMail very soon, as KHTML caret mode and Quanta VPL editor is in the BE branch. I just love KDE, I update it every week with CVS. With this KDE 3.2 release and the next KMail release with Kolab, exchange and HTML edit support, the only part of KDE won't be (almost) perfect is KOffice. I hope more developers join the KOffice team so we can see more progress on it."
    author: "Penna"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "While maybe he shouldn't have said \"fuckhead\", I completely support Mr Jim Simon and no I am not him.  I have been frustrated just like him many times with other OSS/FS developers who think they (and other OSS/FS bizzaro-world citizens) feel the developers are somehow being forced to do the work they do.  Hey you can always leave if someone's comments hurt you feelings too much.\n\nI think this Ingo fellow, you, and others like you have some serious issues.  Let me ask you this.  How many times has \"red tape\" bitten you in the ass?  How many times have you thought some government policies or large organization's policies are rediculous?  Well here is one case right here.  So this Mr Simon fellow didn't know its \"against the rules\" to post feature requests to the list.  If he did, maybe he's never heard a response and decided that the only way he'd have a chance at hearing why his requested feature hasn't been implemented was to contact a list the developers read directly.\n\nThis Ingo fellow over-reacted plain and simple.  So the hell what if you have one legit message on the list.  My god, what's going to happen next -- will KDE fall?  My god, what are you people computers?  This is absolutely pathetic that this is the type of response he gets.  I mean, not only was Ingo semi-rude to him once, he has the gaul to write back to him and escentially yell at him.  I say piss off to you Ingo.  Grow the hell up, if thats the type of developers that are KDE who the hell needs you.\n\nGoing back to Comdex, what if some CTO for some large coroporation is very interested in KDE and wants to find out more -- after seeing it at Comdex.  So they stumble upon dot.kde.org and reads this article.  What do you think their response would be after seeing this article.  If he has questions regarding KDE and accidently emails the wrong place, someone will yell at him?  Is that what you want to portray to the business community that I will agree, is maybe overly sensitive to some extent?\n\nYour last line is a complete joke and goes back to what I said originally about people thinking they're forced into developing OSS/FS.\n"
    author: "SuperPET Troll"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "You're right, developers are not forced to work on KDE, and while working at it they aren't forced to work on a specific feature or bug. They'll work on what they feel like working. And that's a good thing, a happy coder codes better. But this would lead to developing just the features (and fixing just the bugs) the developers think are important. That's the reason for bugs.kde.org, users can post their bug reports and feature requests.\n\nNow, every KDE developer knows peolple want HTML editing. The feature request for it on bugs.kde.org is full of comments like \"Me too, I want it!\". And developers receive an e-mail everytime one of this comments is made on b.k.o. So it's *really* annoying when, after receiving one or two e-mails about it every day, to see an message on a developers mailing list, where people are supposed to send patches and discuss techininal issues, request this feature. KMail developers already know people want this and when they'll implement it. This is true for every feature request on b.k.o. So, if you want to tell them you really want it, just vote for it! Stop wasting people's time.\n\nIf we want better software, let's be better users: fill bugs reports with as much information as you can. Make feature requests on bugs.kde.org. Vote for the bugs that annoys you and the features you want the most.\n\nBTW, developing free software is not politics. If Mr Simon was offended, let him go use Gnome or Windows or whatever. The users needs the developers much more than the other way around, as most of them are just producing code just for their own use and simply contributing it to the comunity. It's just a good deed, that's what free software is all about. While it's good to see people using your work, they don't care about world domination. Just leave it to the average Slashdot reader."
    author: "Penna"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "I think as more and more people use KDE, more and more people will want to post bugs or be involved in the 'bug process'. So the current bug report system works, but perhaps this system itself is not ready for the masses yet. Clearly not everyone can know the rules of 'bug reporting' beforehand, so perhaps bug reporting for very large projects needs to be better fine tuned? \n \nYou and I might know current developing status of a feature, but a more or less static bug report page with a looong list of comments is NOT user friendly in the sense that for some people it provides confusing or too much information. Yes there are other places to find a project status on, but not everyone has thats knowledge. \n \nIf you say you want not to lower the barrier for bug reporting too low, you basically shut peoples mouth - therefor this whole bug reporting is working for a certain level of user at this moment. This does not have to mean MORE bug reports, but better access and more ability of user base to express wanting/needing features/fixes. \n \nNot all bug reporters are developers and when the great masses start bug reporting, will the current bugs.kde.org suffice? \n \nBut this is all theory, ain't theory easy?"
    author: "ac"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "Creating bug reports is by far the easiest way to contact developers nowadays, there is a \"Report Bug...\" in every KDE app's \"Help\" menu, guiding the user through the whole procedure. Any other way of contacting developers makes it a necessity for the user to find the KDE website, find the respective contact information etc. all on his own. At this point (with the implied proficiency  about the different actions) it should be easy to assume that the user would have had much less work simply using the guide reporting the bug.\n\nThis is actually all off topic since it's pretty clear that both Simon as well as SuperPET Troll are trolling, even though the latter gets bonus points for asking seemingly \"good\" questions (questions which are completely irrelevant due to the above reasons and the fact that Simon knew about the bug report and thus knew about the rules)."
    author: "Datschge"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "This isn't a sales organisation.\n\nThe KMail developers have gradually made a very good application. If someone thinks that, well, I can snap my fingers and HTML editing will appear, everybody will jump for me, well they can go stuff it.\n\nIngo has been a very good maintainer for KMail. He has harnessed the disparate and cranky developers, the progress over the last year has been remarkable. There are things that need to be done, and progress is being made. If you don't think he is doing a good job, then start a fork.\n\nOtherwise, just wait and be patient. You have an opportunity of learning how things are done, and how complicated some 'simple' features can be. Watch and learn.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "Fine I agree, he's done a good job with KMail but this isn't the issue at all.  You and the other people who have responded to me are missing the point, it isn't that he's doing a bad job as a maintainer and developer of KMail -- far from it.\n\nSee here's the thing.  In my opinion, and probably the opinion of most others here, KDE has grown out of the small community where the developers are users.  I think most people here would really like KDE to be used by \"regular\" people at home and at work.  Hence my point about some CTO from comdex stumbling upon this tift here.  The developer community shouldn't go around publically attacking users who don't follow their trivial rules, its bad PR.  The thing is, no matter if they are working for free or not, the developers are the representatives for KDE.  Yelling at users will only hurt you in the end, unless of cource you dont care if anyone actually uses your \"product\".  Some of the KDE developers have to grow up if the project is going to grow up.  Case in point, Neil Stevens who removed his applications from cvs because he couldn't put in his pro-US slogan into an About box.  See that just wont fly with the public.  Even if people agree with it a lot wouldn't want to see it in a piece of software, they'd find it inappropriate which is why it was removed.\n\n> Otherwise, just wait and be patient. You have an opportunity of learning how\n> things are done, and how complicated some 'simple' features can be. Watch and > learn\nSee thats what I mean, there you go being condenscending which is completely uncalled for.  Before you say you're not being condenscending, you are -- go look it up in the dictionary.  You have NO idea who I am or what I have done.  Firstly I have never sent feature requests to the email lists.  Secondly, I am a developer myself so I understand how complicated 'simple' features are.  Not ONCE did I EVER state that HTML editing should be easy and should already be in KMail.  Its nice for those who need it, but I dont and can live with out it.\nSo why dont you just keep your mouth shut you ass.\n"
    author: "SuperPET Troll"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "Well, I will condescend to respond.\n\nOne of the largest problems in software development, all software development is how to handle expectations. Marketing people promise the world. IT higher ups promise the world. ERP vendors promise that the moon.\n\nThe reality is that the majority of projects fail. What if the developers had the freedom and gumption to say 'F*** YOU'? Is it possible that they know what is going on best, what is possible to deliver, when and how many resources? Ever heard the response 'where is the patch' when someone demands a feature? What that means is someone actually having the guts to say 'we need more resources'. If Ingo says rather curtly 'No', or even more strongly, 'go away', maybe he knows best. He is giving a realistic and true expectation of the situation. If someone doesn't like it, so what. If someone wants a feature without any contribution, why is anyone surprised that they get a curt response? The developers have a limited amount of time to work on the projects. I would rather Ingo spend time sorting through patches or fixing bugs. If you feel the responses should be different, maybe you could watch the lists for newbies and respond the way you want.\n\nI repeat. The kmail development lists are not a PR department. What part of that don't you get?\n\nIf some CEO is so, umm, disconnected from reality, that he doesn't recognize the sometimes rough to and fro in real non-filtered conversations, I for one wouldn't want him as a customer anyways. Let them believe the lies from someone else.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-03
    body: "> Well, I will condescend to respond\nOooh I can feel just the wit.\n\nYou still fail to realize my point.  I have been saying all along that Ingo as a developer is NOT the problem.  I never once criticized him for not doing anything or doing a poor job.  I criticized him for being unnecessarily rude.  Let me rephrase that incase you still dont understand.  INGO IS DOING A FINE JOB AS A DEVELOPER BUT I THINK HE WAS OVERLY RUDE TO A USER SIMPLY BECAUSE A USER DID NOT KNOW THE EXACT RULES FOR SUBMITTING FEATURE REQUESTES.\n\nI understand the KMail development lists are not a PR department and I never said they were.  What I was saying was the devlopers turn into representatives for their projects, even if they aren't being paid or paid to be the reps.  So for the sake of the developers and the KDE project itself, maybe tone down the harsh attitude with outsiders because you dont know who they are.  If you think its perfectly fine to be belligerent to random strangers then fine, but dont be suprised how outsiders view you and KDE.\n\nLook -- if I hated KDE or wanted it to fail, would I even be arguing this point.  No I'd probably laugh and say cool they kde developers are at it again.  But the fact is that I'm not, so realize who your allies are because you have no idea who I am (and not saying I'm some big imporant person).\n"
    author: "SuperPET Troll"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-03
    body: "Ingo was called a \"fuckhead\", but ingo is the rude one?\n\nNo wonder you call yourself Troll."
    author: "sqlite"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-03
    body: "Maybe you suffer from the Slashdot Syndrome, where you don't read what people say,  I don't know.  So let me re-iterate, for you, what I had said in my previous post.  Not once did I say that 'fuckhead' was an appropriate response to what Ingo had said.  However I did say I shared in the frustration of the person who said it.\n\nYes, Ingo was rude to the fellow -- he was.  Do you know what makes me execessively rude?  The fact that he wrote another email back to him the next day even though Jim Simon only wrote him the original email.  Jim Simon's email wasn't even a demand, it was a request/question.\n\nHere is what he said:\n> \"When will KMail come with native HTML writing support? I'd really love to be > able to write the same kind of emails in KMail that I do with Outlook for\n> Windows.\"\nFirst sentence is a simple question, no demands.  Second sentence, he states that he'd like to have the feature in KMail that he has in Outlook.  He doesn't say \"you better have it or else\".  No demands nor threats their either.\n\nHere is Ingo's first response:\n> \"When it's ready. Not in KDE 3.2. And please refrain from sending such\n> annoying messages to our mailing list in the future. Thanks for\n> understanding.\"\nI'm not sure how this is annoying, but then everyone has their own pet peeves.\n\nThen Ingo sends a second message the next day without a response from Mr Simon:\n> \"Dear Jim, you are not in the position to demand anything from us. If KMail\n> doesn't serve your needs then please consider using another email client.\"\nI don't know what crawled up Ingo here.  Maybe he had a hangover from the previous night and had roosters wake him up at the crack of dawn -- who knows.  All I do know is this is a completely rude email and deserves one in response.  I might not have said 'fuckhead' but I wouldn't not have been too nice myself.  Firstly, Jim Simon never once demanded anything, he asked a question.  Secondly, he didn't do the \"threat\" tactic.  Like \"I wont use KMail anymore unless you add HTML-editing support.\"  No he simply asked when it would be done because he uses Outlook, its in there, and he likes it.\n\nI don't know why you can't accept the fact that Ingo was rude to the Jim Simon guy?  I simply don't get it.  I'm not saying he's less than human, he should be repremanded, or anything like that.  I'm just saying that Ingo was overly rude to this person, and shouldn't be suprised at the email he received.  Go show the emails between Ingo and Jim to some outsiders not in the OSS/FS community.  I bet you will find that other people would find Ingo's response overly rude and Jim's response somewhat acceptable.\n"
    author: "SuperPET Troll"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-03
    body: "Dude.  Now I *know* you're a troll or a fool.\n\nIngo wrote \"Dear Jim, you are not in the position to demand anything from us.\" \n\nIN RESPONSE TO the \"Fuckhead\" comment.\n\nStop displaying your own \"Slashdot syndrome\" here..."
    author: "anonymous"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-03
    body: "Someone beat me to it - replying that you seem to have things mixed up.\nGiven your nick, I do wonder if I have just been 'had' by a troll, or whether you were honestly mixed up.\n\nOh well"
    author: "JohnFlux"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-03
    body: "Well after re-reading it again, I see that the kde-traffic worded that a bit funny.  Usually, one would order the events in chronological order not mixing the two.  So yes I was wrong, I thought that Ingo was being a jerk and emailing Jim Simon twice.  So then, I'd think Ingo would have deserved the email he got.  Seeing as Mr Simon sent a completely off-the-wall email first, then yea it was completely uncalled for.\n\nHowever had the order of events I had thought took place actually took place, would everyone still feel the same way they do now?\n"
    author: "SuperPET Troll"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-04
    body: "Of course not."
    author: "Datschge"
  - subject: "Re: You will ALWAYS have enemies!"
    date: 2003-12-02
    body: "> developers who think they (and other OSS/FS bizzaro-world citizens) feel the developers are somehow being forced to do the work they do\n\nNo, you don't understand.  They are doing the work they do because they want to.  What they *aren't* doing is the work *you* want them to do.   \n\nIngo's answer was, if you don't like KMail, use another email client. GNOME has *paid* developers to work on their desktop.  You can actually go insult them, if you want to and they will likely do what you say because you are ultimately their master.\n\n> Hey you can always leave if someone's comments hurt you feelings too much.\n\nIt would be much better if the *user* left instead of the *developer*.\n\n> I think this Ingo fellow, you, and others like you have some serious issues\n\nHere and elsewhere in your message, you are just being condescending and showing your complete lack of understanding of how an OSS project works.  Not only that, you are being insulting.\n\n> Going back to Comdex, what if some CTO for some large coroporation is very\n> interested in KDE and wants to find out more -- after seeing it at Comdex. So they\n> stumble upon dot.kde.org and reads this article. What do you think their response\n> would be after seeing this article. If he has questions regarding KDE and accidently\n> emails the wrong place, someone will yell at him? Is that what you want to portray to\n> the business community that I will agree, is maybe overly sensitive to some extent?\n\nI agree.  This is a good point for KDE as a project to consider."
    author: "anonymous"
  - subject: "3.2 beta 2 ?"
    date: 2003-12-02
    body: "Hey folks\n\nany news on the beta-2 release ? Coming anytime soon ?\nThanks !"
    author: "MandrakeUser"
  - subject: "What I REALLY WANT"
    date: 2003-12-02
    body: "I would love to test beta2 as I really want 3.2 to rock and want to see how its going, however I do not have the space to install another distribution and don't want to upgrade my stable one to a beta.\n\nFurthermore I don't want the hassle or wasted time, what would be GREAT and I think should be available for every major KDE release is a KNOPPIX release bundled with the latest KDE development milestone so all of us can lok for bugs in the latest KDE and make sure it is well tested and rock solid. Of course, this shouldn't be done for alphas, just betas and release candidates.\n\nThis would not only be useful for finding bugs, but also for marketing and community involvement. We would be able to show off the latest and greatest KDE goodies and play with the new technology without any hassle. Interested KDE users will surely be pleased to see the next beauty =)\n\nI wouldn't ask for this if KDE has a shorter release cycle, but with KDE 3.2 having a full year cycle you tend to get a little impatient and a KNOPPIX release would definitely ease people up and keep them informed on the state of KDE and enable them to help the project by closing bugs that are no longer valid and reporting new bugs.\n\nI really hope to see something like this!\n\nBTW: \n\n#1 I have no clue about programming yet.\n#2 I have little time. I do donate to KDE when a new version is released.\n#3 I really hope KDE 3.3 will have a release cycle that is no longer than 6 months, Qt 4 looks awesome and KDE 4 even better =)"
    author: "Mario"
  - subject: "Re: What I REALLY WANT"
    date: 2003-12-02
    body: "Release candidates have a supposed lifetime of a few days, creating/test a CD would not be practicable. Nobody knows if the next major KDE version will be 3.3. And you surely had no look at Qt 4 yet."
    author: "Anonymous"
  - subject: "Re: What I REALLY WANT"
    date: 2003-12-02
    body: "Beta2 on the other hand, will have a life span of a couple of months.\n\nAFAIK unless there is a sudden change of plans 3.3 will be the next major version: http://developer.kde.org/development-versions/kde-3.3-features.html\n\n"
    author: "Src"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-02
    body: "Read the release plan on developer.kde.org, if there are no delays Beta 2 will be released on Thursday."
    author: "Anonymous"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-02
    body: "Thanks, but that _IS_ my questions, if there are any delays on the horizon. Maybe someone was on the know. No big deal of course ..."
    author: "MandrakeUser"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-02
    body: "The directory for the packagers appeared one day later than in the plan on the ftp server. If you find that it's something to worry about..."
    author: "Anonymous"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-06
    body: "Maybe nothing to worry.\n\nBut poeople (like me) are curious.\n\nSo, no ranting intended, but\n\na) Why must we wait untill the packages are finished?\n\nThere are people whi will compile from scratch, or just emerge...\n\nb) Why ist there no information, that there is a delay, and when to expect the Beta2.\n\nIt should be out thursday, now it's saturday.\n\n\n\nA little Info on the dot would not be too much trouble, but really good PR.\n\n\n\nMicha\n"
    author: "Micha"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-06
    body: "You do PR when it's released, not that it's behind a plan. Don't expect releases on weekends because that would be PR-wise bad."
    author: "Anonymous"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-06
    body: "> You do PR when it's released, not that it's behind a plan.\n\nBut why not tell the people \"Hey, forget about that schedule\".\n\nI'm checking ftp.kde.org regulary since thursday, ftp.de.kde.org already has a 3.1.94 directory, but it's not executeable. That's bad. So close, but still so far away.\n\n\n\n\n\n> Don't expect releases on weekends because that would be PR-wise bad.\n\nWhy? On the weekend one has time to compile.\n\n\n\nOK, if I can forget about a code-release on the weekend (I don't care about packages), why won't somebody tell us that there's a delay?\n\n\n\ngreets,\n\n  Micha"
    author: "Micha"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-06
    body: "The schedule has not changed. And the KDE schedules were IIRC never accurate to a day.\n\n> OK, if I can forget about a code-release on the weekend (I don't care about packages)\n\nYou don't care about packages? So why the excitement above?\nCompile the snapshots or use CVS (there exist even CVS ebuilds) and enjoy a more current version than Beta 2."
    author: "Anonymous"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-06
    body: ">You don't care about packages? So why the excitement above?\nBecause I want to have to Beta 2, not just _any_ CVS snapshot.\nI've got some issues with beta 1, if they still exist in Beta 2, I could file a bug report.\nBut I won't file a bug report against a cvs version, as it will be outdated when it's compiled on my machine.\n\n"
    author: "Micha"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-06
    body: "> But I won't file a bug report against a cvs version, as it will be outdated when it's compiled on my machine.\n\nThe same is true, and even more, for Beta 2 which is already one week old code when the source packages are released. The nice thing about incremental CVS update and incremental compilation is that you can stay more current."
    author: "Anonymous"
  - subject: "Re: 3.2 beta 2 ?"
    date: 2003-12-06
    body: "Yes, but if there is a _defined_ base, I can crosscheck with others, if the issues I've got relate to my system, or to kde.\n"
    author: "Micha"
  - subject: "What a jerk!"
    date: 2003-12-02
    body: "That's all...\n\nThanks for all the hard work KDE devs!"
    author: "Joergen Ramskov"
  - subject: "Ingo was rude and deserved that reply."
    date: 2003-12-02
    body: "I really think that Ingo's comment about \"such annoying messages\" is what set that user of to send such a rude reply.  However, that does not mean that Ingo deserved to be called a fuckhead.  I kinda feel that Ingo was the first one to be rude in that exchange.  He could have simply said that feature requests are to be directed toward bugs.kde.org and left it at that.  Perhaps if he tries to watch comments like this in the future such posts could be avoided in the future.  Still, though, Ingo does not deserve to be called a fuckhead.\n\nKeep up the good work.  I may be silent on lists but I show my appriciation every day by the fact that when my computer is on, I have kmail running."
    author: "Anonymous"
  - subject: "Re: Ingo was rude and deserved that reply."
    date: 2003-12-02
    body: "Yes, and no. On the one hand, these kind of messages in devel lists *are* annoying. Still, a more polite re-direction to b.k.o. would be good for users that don't know about that. By Simon's second message however, it becomes perfectly obvious he _does_ know about b.k.o., and still posts his requests in the devel list. Now that is truely annoying. So, you are IMO right in that it would be good to try to be really polite and redirect people to b.k.o. (remember that not everybody has a good command of English, so they may have inadvertently written the message in a way that others see as impolite). However, we must not forget that the real impolite a**hole here is Simon, who knew perfectly well what he was doing and had no right whatsoever calling Ingo names or making demands.\n\nIn general: if you want a feature that bad, you should make it yourself, pay someone to make it for you or use a system that meets your needs better."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Ingo was rude and deserved that reply."
    date: 2003-12-02
    body: "While I agree that Ingo was a bit rude, the poster was far worse. When you are putting in hard hours, it gets tiring to hear ppl complain about what you do, esp. if you are doing it for free. Ingo snapped back and the psoter got downright nasty.The poster could have, and should have, refrained a bit and understand that others can be under stress. \nOn an earlier posting, I got it into with another developer here and simply refrained. It is much easier in the long run on everybody.\n\n"
    author: "a.c."
  - subject: "Re: Ingo was rude and deserved that reply."
    date: 2003-12-03
    body: "If you post in the wrong place, and get told about \"such annoying messages\", then you can take that as being told off, but hardly insulted.\nHence hopefully you try not to post in the wrong forum from then on.\n"
    author: "sqlite"
  - subject: "They were both at fault"
    date: 2003-12-02
    body: "To be honest, Ingos first response was hardly perfect. If you want people to go to b.k.o. to file bugs, then a polite reply telling the user that would have been sufficient (yes, it later transpired that the user knew about b.k.o.)\n\nGranted, the users reply was out of order and I am not trying to defend that. But his original post wasn't that bad."
    author: "Stephen Douglas"
  - subject: "Re: They were both at fault"
    date: 2003-12-02
    body: "I find this all fairly interesting because I've had many good and bad experiences with the open source community as well as the same types of issues with closed source.\n\nFor me the answer is simple, if you don't like it leave. :) sounds harsh but it works for me. I had a bad experence with bug reporting with openoffice and was basically told by the devs to f*** off and my report was worthless.\n\nInterestingly I had almost the exact same experience with thekompany.com when reporting an issue with Aethera. Being told that by a company I've bought product from is harder to take I can tell you.\n\nMy point is closed or open you can get the same response. I choose to deal with it silently (well until now) by just simply not helping out those projects by doing any bug reports on those products.\n\nBTW, my reports to those products were very polite and respectful yet I got a harsh response in both cases.\n\nRobert"
    author: "Robert Cole"
  - subject: "remember"
    date: 2003-12-02
    body: "internet isnt real life... Expressing yourself in mails/fora etc isnt like in real life. I've seen alot of flamewars (who hasn't?) and they all come down on one thing: misunderstanding emotions. \n\nEmotions can't be expressed very well digitally. Most of the time, ppl overreact. Emotional reactions seem stronger if read - so if an reaction is written a little irritated, ppl percieve it to be very rude. I think thats the most important problem online communication does face. I'm thinkin' bout doing an investigation on this topic (I study psychology), to measure to what extend emotions seem to be 'enlarged' by 'digitalizing' them..."
    author: "Superstoned"
  - subject: "Re: remember"
    date: 2003-12-02
    body: "I tend to say that emotions can't really be expressed digitally at all, especially not if you are communicating in groups across many different national and cultural borders. If something is interpreted as intended emotion it nearly always turns out to be received as an extreme form of it. There are way too many differences in explicite and implicite communication between different regions so that the emotional value one receives through a communication is only by a tiny lucky chance the same as the sender actually intended. Thus I personally suggest to keep open group communication free of any suggestion or expectation of emotions. (Intercultural communication is the study field for this kind of researches, interesting stuff, am just back from such a course. =)"
    author: "Datschge"
  - subject: "Bad Grammar"
    date: 2003-12-02
    body: "Dear Peter.\n\nI don't want to offend you, but your use of \"i\" instead of capital I \nwhen refering to yourself is very disturbing (at least to me) and it\nis damaging the reading flow. I would be very grateful if from now on\nyou will try to avoid this over-common language pitfall.\n\nThank you."
    author: "NOKA"
  - subject: "Re: Bad Grammar"
    date: 2003-12-02
    body: "Hmmh... As far as i know, this is a spelling issue, not a grammar one. And yes, i know about English custom of writing capital 'I' to refer to oneself. However, i do not like it and will not start doing it again (i used to conform in this respect). I find my underuse of articles more disturbing for example (or other bad grammar, like verb patterns and the like).\nSure, i will have to conform in the (more official) written documents, but please, leave me with my little traits while i write the Traffic. I hope you can cope with it as it is... You might as well get used to it =).\n\nCheers..."
    author: "Peter Rockai"
  - subject: "Re: Bad Grammar"
    date: 2003-12-02
    body: "i also like the use of \"i\", non capitalized, as a humble way of referring yo self, I really do :-)\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: Bad Grammar"
    date: 2003-12-02
    body: "Right, it humbly states that you don't think that the rules of grammar and syntax apply to you.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: Bad Grammar"
    date: 2003-12-08
    body: "i do it, not becauase i think that the rules don't apply to me.. i do it because 1, this is an informal mode of communication (do you always use perfect english when speaking with a friend?) and 2, because i'm lazy and don't want to be bothered with pressing the shift key. the fact that you can still read it proves that it is still effectivley communicating."
    author: "jcd"
  - subject: "Developers vs. Users"
    date: 2003-12-04
    body: "Users do not demand anything, they make feature requests.  Feature requests should be taken as suggestions.\n\n> \"Dear Jim, you are not in the position to demand anything from us. ... \"\n\nDevelopers should not say things like this.  It is by definition arrogance.  The reason is that it is presumptuous.  Assuming a \"demand\" is a presumption.\n\nDevelopers should be aware of the fact that the various statements to the effect that this is free and you can (therefore) take it or leave it are not appreciated by users.  I realize that developers do not understand why this irritates users, but it is the simple truth that it does.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Developers vs. Users"
    date: 2003-12-04
    body: "JESUS CHRIST!  Do we have to go over this again? The user called Ingo a \"Fuckhead\" and everyone is complaining about Ingo's response to *that*?\n\nSorry, but this is just getting ridiculous..."
    author: "anonymous"
  - subject: "Re: Developers vs. Users"
    date: 2003-12-04
    body: "No, as I said that was unacceptable. However, Inogs initial response (before the user called him that) was also out of order."
    author: "Stephen Douglas"
  - subject: "Re: Developers vs. Users"
    date: 2003-12-04
    body: "The person who was in the 'wrong' here, if you can call it that, was the guy who prepared the Traffic. I do not know about many people, but from what I read, or rather, how I read, it seemed that Ingo (is it?) wrote 2 emails after receiveing the first. However, this has been shown not to be the case. I am sure some people literally 'skim' through the traffic, and read the 'salient' points, i.e., highlighted bits and so on. I certainly do this. It was only later, after wisely refraining from posting, having noticed that there seemed to be a major misunderstanding between people, that I revisited the traffic. Having reread it more thoroughly, I realised that those people who were saying Ingo was overly rude had the wrong chronolgy in their heads. Partly their fault because they didn't read too thoroughly, but partly Peter's, because he did nto anticipate that his resentation could cause problems. Next time, when reproducing an exchange, I would advise the writer to preserve the chronology, as this is valuable to people who, with good reason, cannot be bothered to read each and every letter of the traffic."
    author: "Maynard"
  - subject: "Re: Developers vs. Users"
    date: 2003-12-05
    body: "It should be noted that my posting, as well as other's, was based on a misunderstanding of what KDE Traffic said.\n\nI think that others have now clarified this point in some detail.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Replacing unthemed icons with CrystalSVG"
    date: 2003-12-04
    body: "> I'd like to propose replacing some of the existing icons with Rohit's \n> versions, ... .\n\n> David Faure was quick to agree ...\n\nAnd (didn't seem to make the Traffic) James Richard Tyrer strongly dissented.\n\nNote that the point here is: *replacing*.  Why do we have to _replace_ them?  Can't we allow the user to choose?  Is this starting to sound like GNOME? :-)\n\nThis is a usability issue.  These new icons are harder to visualize than the traditional unthemed ones.\n\nIf we need to handicap our office suite (and other applications) perhaps we can find a way to shoot ourselves in the other foot as well. :-)\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Replacing unthemed icons with CrystalSVG"
    date: 2003-12-05
    body: "I think that this needs some clarification.\n\nIt appears that after looking into this in more detail that the icons which are being replaced are KDEClassic icons which for some reason were installed as crystalsvg.  These should be replaced since they shouldn't have been installed there to begin with.\n\nWhat I was objecting to was, and still is, the removal of the KDEClassic icons correctly installed as hicolor.\n\nAnd that is why I ask if this is going to be like GNOME.\n\nI have no objections to installing CrystalSVG icons, but why is it necessary to REMOVE the KDEClassic icons?\n\n--\nJRT"
    author: "James Richard Tyrer"
---
<a href="http://www.kerneltraffic.org/kde/kde20031130_69.html">KDE Traffic #69</a> is here, chock full of news and waiting for you. Topics include <a href="http://usability.kde.org/">usability</a> issues, <a href="http://quanta.sourceforge.net/">Kafka</a> progress,  <a href="http://www.kdedevelopers.org/node/view/240">KDE apps</a> <a href="http://xmelegance.org/kjsembed/">in ECMAScript</a>, <a href="http://kde.ground.cz/tiki-index.php?page=How+To+Use+KDevelop+with+KDE+CVS">importing KDevelop projects</a> into KDE CVS and more.
<!--break-->
