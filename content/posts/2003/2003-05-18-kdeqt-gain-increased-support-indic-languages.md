---
title: "KDE/Qt Gain Increased Support for Indic Languages"
date:    2003-05-18
authors:
  - "numanee"
slug:    kdeqt-gain-increased-support-indic-languages
comments:
  - subject: "great"
    date: 2003-05-17
    body: "Indic languages were pretty much the only set of languages that were not supported very well by Qt, but were supported quite well by pango. Places like India have a great potential for many GNU/Linux/KDE installations.. already a increasing amount of people use Linux.. perhaps it'll free them from the chains of using pirated copies of WinXP :)"
    author: "lit"
  - subject: "Re: great"
    date: 2003-05-19
    body: "perhaps, if internet connectivity is cheaper than winXP."
    author: "seenic"
  - subject: "semi-OT: Old languages"
    date: 2003-05-17
    body: "Are there internationalisations for olds languages for KDE like Latin, Greek or Sanskrit?"
    author: "Somebody"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "Old languages? Greek???"
    author: "KDe User"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "Not many people speak in New Testament Greek, now, but see the KDE-based BibleTime for a place where handling ancient Greek is useful. There's loads other classical works written in ancient Greek as well."
    author: "Blod"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "Like Works and Days (\"Ergai kai heterai\") of Hesiod or the famous Ilias."
    author: "Somebody"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "Ancient Greek, the new one is a mixture of the old one, Latin and languages of the barbarians!"
    author: "Somebody"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "What about Klingon? Or Elvish(sp?)?"
    author: "KDE Fan"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "Yeah, Klingon would be nice."
    author: "Somebody"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "Klingon would be nice, but somebody (Charles Samels?) suggested it, even created a commit and got ready to support it in CVS, and it got nixed by the developers as \"silly\".\n\nIt's a shame, IMO.  It would have hurt nothing, and would have been an attractive harmless sidebar note.  Users would have liked it."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "Rob Kaper"
    author: "Charles Samuels"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-05-18
    body: "The whole story is at http://www.kerneltraffic.org/kde/kde20020605_38.html#3"
    author: "Datschge"
  - subject: "Re: semi-OT: Old languages"
    date: 2003-06-04
    body: "Sanskrit uses the 'Devnagri' script (and in fact was the origin of a lot of indian languages). therefore, I think only correct translations will be needed, the support is already present :-)"
    author: "Hrishikesh Mehendale"
  - subject: "NDS"
    date: 2003-05-18
    body: "What about plattdeutsch? Recent postings on prolinux got high attention. Perhaps KDE may become the only desktop environment that fulfills the European Language charter. Institut f\u00fcr Niederdeutsche Sprache Bremen wants to translate Gnome."
    author: "Hein"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "Let them waste their time and bandwidth with unneccessary languages... I doubt that there is demand for plattdeutsch, and there are certainly things that are more important...\n\n"
    author: "Tim Jansen"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "  Plattdeutsch \\Platt\"deutsch`\\, n.\n     The modern dialects spoken in the north of Germany, taken\n     collectively; modern Low German. See {Low German}, under\n     {German}.\n\nWhat is useless about that?"
    author: "ac"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "Because it is a dialect that no one writes, it is only spoken."
    author: "Tim Jansen"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "It is no dialect, stupid! \n\nRead our European Language Charter...\n\nIf KDe fulfills the European Language charter as the only Desktop environment it will be very useful to \"sell\" KDe to governmental use. \n\nAfaik governmental institutions are obliged to reply in indigene languages in Europe.\n\nHa! Unnecessary? Sure, KDe only incorporates few basic features... Who need islandic KDE as everybody speaks english over there \n"
    author: "Hakenuk"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "I doubt that any government wants a KDE version in 'Plattdeutsch'. Show me, for example, a single newspaper written in it. Or a radio station that uses it. Everything over there is in regular german."
    author: "Tim Jansen"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "The main reason is discrimination of indigene languages, in the times of nationalism language variety wasn't wished by the authorities. Later it became a traditionalism issue. \n\nWikipedia tells that it was official language of the Hanse.\n\nThe main reason why there is no radio station is that the authorities didn't want to have one. Minority languages (Plattdeutsch was majority language) became the languages of rural people. Very much the same in the third world. the upper class speaks English not Hindi, Ewe, Suaheli ecc..\n\nIt almost the same with noble men, they spoke french. National languages (like German) were regarded (and were) lower class."
    author: "Gerd"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "The tiny, but very good public station 'Radio Bremen' has daily news in plattdeutsch. It's a real fun to hear and I'm sure quite a few people like it.  Headlines from last friday: 'Schr\u00f6der snackt mit Powell +++ Eichel will V\u00f6rdeelen bi de St\u00fcer afbuun +++ De School warrt reformeert - Orientierungsstufe geiht tweedusenunveer to Enn ... '. You can listen to it at: <a href=\"http://193.97.251.179/cgi/intra/RD/stream.radiobremen.de/ramgen/media/rbton/melodie/platt/news.rm\">news.rm</a> .\nBut I agree that there are more important things for KDE than a localized version for platt. For example to fix konqueror (version 3.1.1) crashes on http://www.radiobremen.de/ :-)\n<p>\nThomas"
    author: "Thomas"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "> But I agree that there are more important things for KDE than a localized version for\n> platt. For example to fix konqueror (version 3.1.1) crashes on\n> http://www.radiobremen.de/ :-)\n \nWorks perfectly fine here using KDE 3.1.1.\n\nAnd besides it's by far not like programmers who could fix these kind of possible problems are the same people who are doing documentation and translations. By denying people to just do translations they'd like to see and contribute to you are more likely to also discourage them to work anywhere else within KDE so you lose more than you win. Right now Tim Jansen is doing a great job keeping the community small."
    author: "Datschge"
  - subject: "Translation"
    date: 2003-05-19
    body: "\nI am from Brasil (not *zil). I don't understand nothing about C ou C++ ou Kt ou Gtk, neither Python or Camel. Maybe HTML and CSS.\n\nI help Free Software with translation! That's all I can do.\n\n\nSeid umschlungen, Millionen !\nDiesen Kub der gazen Welt !\n\n;-D\n"
    author: "Ventura"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "Wiki information:\n\nhttp://www.wikipedia.org/wiki/Low_Saxon_language\n\n"
    author: "Gerd"
  - subject: "Re: NDS"
    date: 2003-05-28
    body: "Wilst du gern disse link lesen:\n\nhttp://www.lowlands-l.net/talk/eng/lowsaxon.htm\n\nIk kom ut Denemark un ik snaak ok Neddersassisch. Het is 'n spraak un het is tou vergelyken mit Nedderlands.\n\nIk ken vele dey Neddersassisch schryfen.\n\nGr\u00c3\u00b6ytens,\nKenneth"
    author: "Kenneth Christiansen"
  - subject: "Re: NDS"
    date: 2003-05-28
    body: "If you didn't understand what I was saying (due to me speaking another language more related to Dutch than High German), please let me rephase.\n\nLow Saxon is a language and has been it for centories. Please read the supplied link for more information. It is spoken in North Germany, East Netherland, Russia and also other places. \n\nDanish is one of those language who like High German has been highly influenced by Low Saxon.\n\nLow Saxon is a litteral language. Especially in The Netherlands where there are official spelling systems.\n\nHarry Potter is also translated to North-Lowlands-Saxon:\n\nhttp://www.buecher-versandkostenfrei.de/hp/hp-platt.html\n\nLow Saxon is protected by the European Language Charter for Regional and Minority Languages:\n\nhttp://conventions.coe.int/Treaty/en/Treaties/Html/148.htm\n\nCheers,\nKenneth"
    author: "Kenneth Christiansen"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "Yes, and there are around 30.000 mennonites in Paraguay that speak a dialect of Platt (plus many of the native people too). And no, they are not the horse and buggy mennonites from the USA, that everyone thinks of when they hear \"mennonite\". And yes many, but not all of them speak high German.\n\nKAH - Uruguay, South America\n\nWant Infos?\n\nhttp://gosouthamerica.about.com/library/weekly/aa042002a.htm?iam=dpile_1\n&terms=Safari%2BHotel%2BFiladefia%2BParaguay\nhttp://www.der-ueberblick.de/aktuell/200203.breih/content.html\nhttp://www.mundartverlag.de/regionen/plautdietsch/plautdietsch.htm\nhttp://www.monde-diplomatique.fr/2001/08/CASSEN/15437\nhttp://mitglied.lycos.de/claudiahauf/Mennofoto/Coloniesbilder.htm\nhttp://members.fortunecity.com/dikigoros/chacoaustausch.htm\n"
    author: "Phantom"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "You think 'hindi' is unnecessary?? You make me laugh"
    author: "sujan"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "I think he was referring to Plattdeutsch, not the indic languages."
    author: "cm"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "sorry...my bad :)"
    author: "sujan"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "Well, there are many languages that are less spoken. In many third world states trough urbanisation languages die, english and french as former colonial languages take over..."
    author: "Hakenuk"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "Uh? Why not actually let the demand decide before declaring everything as \"waste of time and bandwidth\" and being \"not important enough\" to be considered at all? There's really no need for someone to appear like knowing the only true truth and suggesting everyone else got it wrong."
    author: "Datschge"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "YEAAAH!\n\nThat is true FOSS style!"
    author: "Gerd"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "http://conventions.coe.int/Treaty/EN/Treaties/Html/148.htm\n\nArticle 10 \u0096 Administrative authorities and public services\nWithin the administrative districts of the State in which the number of residents who are users of regional or minority languages justifies the measures specified below and according to the situation of each language, the Parties undertake, as far as this is reasonably possible: \n  \nto ensure that the administrative authorities use the regional or minority languages; or \nto ensure that such of their officers as are in contact with the public use the regional or minority languages in their relations with persons applying to them in these languages; or \nto ensure that users of regional or minority languages may submit oral or written applications and receive a reply in these languages; or \nto ensure that users of regional or minority languages may submit oral or written applications in these languages; or \nto ensure that users of regional or minority languages may validly submit a document in these languages; \nto make available widely used administrative texts and forms for the population in the regional or minority languages or in bilingual versions; \nto allow the administrative authorities to draft documents in a regional or minority language. \nIn respect of the local and regional authorities on whose territory the number of residents who are users of regional or minority languages is such as to justify the measures specified below, the Parties undertake to allow and/or encourage: \nthe use of regional or minority languages within the framework of the regional or local authority; \nthe possibility for users of regional or minority languages to submit oral or written applications in these languages; \nthe publication by regional authorities of their official documents also in the relevant regional or minority languages; \nthe publication by local authorities of their official documents also in the relevant regional or minority languages; \nthe use by regional authorities of regional or minority languages in debates in their assemblies, without excluding, however, the use of the official language(s) of the State; \nthe use by local authorities of regional or minority languages in debates in their assemblies, without excluding, however, the use of the official language(s) of the State; \nthe use or adoption, if necessary in conjunction with the name in the official language(s), of traditional and correct forms of place-names in regional or minority languages. \nWith regard to public services provided by the administrative authorities or other persons acting on their behalf, the Parties undertake, within the territory in which regional or minority languages are used, in accordance with the situation of each language and as far as this is reasonably possible: \nto ensure that the regional or minority languages are used in the provision of the service; or \nto allow users of regional or minority languages to submit a request and receive a reply in these languages; or \nto allow users of regional or minority languages to submit a request in these languages. \nWith a view to putting into effect those provisions of paragraphs 1, 2 and 3 accepted by them, the Parties undertake to take one or more of the following measures: \ntranslation or interpretation as may be required; \nrecruitment and, where necessary, training of the officials and other public service employees required; \ncompliance as far as possible with requests from public service employees having a knowledge of a regional or minority language to be appointed in the territory in which that language is used. \nThe Parties undertake to allow the use or adoption of family names in the regional or minority languages, at the request of those concerned. "
    author: "Hakenuk"
  - subject: "So many unnecessary languages in KDE..."
    date: 2003-05-18
    body: "(note: this post is pure sarcasm to show what some particular people would prefer to see obviously completely ignorant of the current situation in KDE)\n\n...such a waste of time and bandwidth... ...there are certainly things that are more important...\n\nBasque (eu), everyone is speaking Spain or French there anyway.\nBrazilian Portuguese (pt_BR), why not stick to Portuguese instead doing it twice.\nBreton (br), everyone is speaking French there anyway.\nBritish English (en_GB), again, why not stick to (Amercian) English instead doing it twice.\nCatalan (ca), everyone is speaking Spain there anyway.\nChinese Traditional (zh_TW), the majority of the Chinese population uses Simplified Chinese nowadays.\nEsperanto (eo), not even a natural language.\nFrisian (fy), everyone is speaking German or Dutch there anyway.\nGalician (gl), everyone is speaking Spain there anyway.\nIrish Gaelic (ga), everyone is speaking English there anyway.\nMaori (mi), everyone is speaking English there anyway.\nOccitan (oc), everyone is speaking French there anyway.\nSorbian (wen), everyone is speaking German there anyway.\nTibetan (bo), everyone is speaking Chinese there anyway.\nVenda (ven), everyone is speaking Afrikaans there anyway.\nWalloon (wa), everyone is speaking French there anyway.\nWelsh (cy), everyone is speaking English there anyway.\nXhosa (xh), everyone is speaking Afrikaans there anyway.\nZulu (zu), everyone is speaking Afrikaans there anyway.\n\n...and we finally need to combine all those minor Skandinavian languages, their are also taking way to much time and space...\n\n...there certainly is a connection between the lack of developers and the overkill of available languages in KDE... ...removing them all certainly would boost the development of KDE..."
    author: "Datschge"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-19
    body: "I fully agree.\n\nIf people that speak some language want to translate into it, why not?\n\nJust on  a side note, I'm from Portugal. Brazilian Portuguese is spoken by much more people than Portuguese. Brazil is several times larger than Portugal. One of the things that really annoys me is that many international companies only translate into brazilian portuguese and not in portuguese. An understandable economic reasoning, unfortunately.\n\nAlthough similiar, the two languages are different, not only in pronounciation, but in grammar and vocabulary as well. The same thing happens with galician, which is a blend of portuguese with spanish.\n\nOpen Source projects, being voluntary work, need not worry with the economic issues of the cost of translations, so why not translate in anything if there is a voluntary to do the work?"
    author: "Luis Carvalho"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-19
    body: "TRUE!\n\nThe translation overdose will sort the good/used/maintained translations out.\n\nIt is self regulating, isn't it?\n\n"
    author: "cies"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-20
    body: "Plattdeutch internationalization may be based on German. It wouldn't matter at all if not 100% was translated.\n\nOnly for Mennonite people from South America probably bad..."
    author: "Gerd"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2006-08-14
    body: "There are also many Mennonite Brethren (no horse and buggy's) in central USA and Western Canada who speak Plauttdietsch, but they are dying out with many of the younger generation just speaking English.  I am attempting to pick it up.  There is a Plauttdietsch New Testament on www.biblegateway.com with audio.  Alphabet doesn't use the German style vowels."
    author: "Michael Warkentin"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-19
    body: "Now, British English is the original, KDE should be in standard (British Received Standard) English per default. :) "
    author: "Sinuhe"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-21
    body: "I agree with this. It's incredibly stupid to say that translations should be removed just because, in your irrelevant opinion, they are using up space.\n\nWe should accept the reality that in every other form of media, people are used to reading and listening to their variant of a language. If a piece of software is written by an American, then I would agree that in the case of English, nobody can be blamed if that application is natively written in American English. But if somebody contributes a British (rather, international) English variant, what's the problem with maintaining this also?\n\nThe arrogance of people that presume American English is the more communicated form of English frustrates me enormously. It is the unfortunate standard precisely because software is always in American, it is never translated back to international english, because the similarities are such that it is still understood. In every respect, providing an en_GB translation with KDE is just a more refined solution, not a lazy byproduct of mass-production. It is certainly appreciated by me, and I'm British.\n\nI would therefore agree with the opinion held by some of the more sensible posters -- open source sensibly deals with translations in the same way it does code: if your language (or feature) is not present in an application, and you would like it to be, and you are able to do that work, you probably should do it yourself. Long before you complain about it not being there.\n\nBut, please, don't remove translations just because they seem 90% similar. I'm sure that is the case, but it is irrelevant. When I watch TV, it is in British. When I read the newspaper, it is in British. And generally, when I read books printed in this country, they too are in British. I do not see why I should have to make do with American English if the work has been done on an en_GB \"translation\"."
    author: "Alistair Strachan"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-21
    body: "Just to qualify what the original (and cleverly sarcastic) thread was saying -- I don't think I used a single British/International English variant of any \"American\" word in that post.\n\nPerhaps maintaining en_GB is not so difficult afterall?\nSounds like search and replace to me.\n\nIs it like this in other language variants?\n"
    author: "Alistair Strachan"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-19
    body: "wow, I didn't realise there was a Maori translation..."
    author: "Stuart Herring"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-05-20
    body: "Let alone Canadian English..."
    author: "Wrenkin"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-06-10
    body: "Arggg!!!! It's so painful to see catalan here!!!!\n\nCatalan is a language spoken by 10 milion people, not only in Spain BTW. It's my mother tongue, I also speak spanish, french and english.\n\nYou must understand that I speak catalan to everybody, I can speak spanish as well as catalan, but I don't use it much ... It's not a thing about: \"they could use KDE in spanish\" (I could do that in english too)... We are speaking of culture here...\n\nYou must understant too, that catalans speak spanish because of political imposition which began after a great defeat and has many political connotations.\n\nWhen a catalan sees a computer in catalan, he/she feels \"at home\". Just another way to make it easy for the user.\n\nLanguages are not only for communication, they also represent a culture... \n\nBTW: One of the strong points that free software has in front of M$ products in Catalonia is that M$ lacks catalan support in almost all their products. Soo don't drop this one... It would be so stupid!"
    author: "josep"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2003-11-30
    body: "You are damn right.They ought to streamline the garbish of that language confusion.The only one that i did not get was \"sorbian\".Maybe you meant \"serbian\" which is the same as croation.Maybe.regards. delma. "
    author: "Delma Del Fr\u00e9"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2004-03-11
    body: "The sorbs are a small minority in the east of germany."
    author: "jwk"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2004-01-01
    body: "\"...and we finally need to combine all those minor Skandinavian languages, their are also taking way to much time and space...\"\n\nThere are at least 20,000,000 speakers of the Scandinavian languages.  What do you consider to be minor languages?  Anything other than your variety of Yankee English?  \n\nYou cannot combine the Scandinavian languages.  Not only do they vary a lot (in some cases it would be like trying to combine German and Dutch) but you would not believe the politics involved in the language situation in Scandinavia in general.  However, I will not discuss about that because it would take up too much space and is very complex.  \n\nNorwegian is my first language.  Danish and Swedish are also languages.  It seems many people on the outside have an idea that these languages are all very similar.  They are similar in many ways, but for example I cannot communicate with a Swede or Dane, unless of course we use English or Norwegian.  The differences are too great for clear oral or written communication and understanding.  \n\nYou will notice that there are two traslations of KDE.  One is in Norwegian Bokm\u00e5l and one in Norwegian Nynorsk.  These are seperate written (and spoken, more or less) dialects within Norway itself.  Most people use Bokm\u00e5l, and Nynorsk is used mainly on the westcoast and interior of the country.  Both are official written forms of the languages, so by law, everything must be in both.  It is spoken a bit differently also, and so a certain percentage of the news must also be broadcast in each speech variety (despite the fact there are hundreds of sopken varieties within these \"norms\" as well).  Maybe it can be likened to Canada's situation of English and French being both official and everything must be in both, despite the fact most people are bilingual and the majority of French speakers are in Qu\u00e9bec, numbering around 12-13 million speakers there.  The same thing occurs in the States with Spanish being the secondary media language ( and quickly becoming a major media language in places like California, Florida, NY).  If there is a market for it, they're going to do it.  They're not stupid.  \n\nBy the way, what about the Nordic languages Finnish (Finland), and Icelandic?\nYou do not mention them.  Should we forget about Finnish because there are \"only\" 5 million speakers?  Finland gave us Nokia and is one of the most tech-savvy nations in Europe, and in the world.  Scandinavia and the Nordic countries are very tech-savvy, dare I say more than Canada and the states.  Nearly everyone has a computer and internet access, and no not everyone speaks English, especially not in Sweden and Finland.  Most people do not know much English.  It is learnt in school, but many people forget because they never use it.  Many people cannot read it well, so there is a need for translations into the Scandinavian languages.  About Icelandic, it's spoken officially in Iceland and a large Icelandic community in Manitoba, Canada in total numbering only around 310,000 speakers in the world.  However, it would be wise if KDE were to translate into Icelandic because Icelanders are not only very proud but also very tech savvy, and many refuse to use programmes not in Icelandic.  Icelandic is very different from any and all languages because it is isolated.  They would be well to do if they had an Icelandic version as well, if you ask my opinion.  \n\nIf you didn't already know, British English is pretty much the world standard.  If anything is to be eliminated from the list it should be \"American\" English.  Afterall, it's only used in the States."
    author: "Ole"
  - subject: "Re: So many unnecessary languages in KDE..."
    date: 2005-05-21
    body: "Not to forget, Finland gave us LINUX. (but Linus Torvalds speaks swedish, the second language in Finland)"
    author: "Mikael Johansson"
  - subject: "Re: NDS"
    date: 2003-05-18
    body: "Okay... You all know the Open Source rules by now.\n\nIf you want it, if you need it, do it. \n\nIf you want plattdeutsch in KDE, then gather together a translation team and start making it happen. If you can't get that together, or it isn't sustainable, then it isn't that important to people.\n\nIf people want it enough to translate everything, the KDE project will be more than glad to welcome plattdeutsch into the gathering of languages.\n\nSo no more people having a go, okay? If it's wanted, it will be done."
    author: "Dawnrider"
  - subject: "Re: NDS"
    date: 2007-06-29
    body: "It is called Plautdietsch you tards!"
    author: ":)"
  - subject: "perhaps this will attract more developers :)"
    date: 2003-05-18
    body: "Hey this is good stuff! If we are luckey this will give us a long term edge.\nMore users == more developers.. evenutally :P\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-18
    body: "With people arguing against the addition of new languages like Tim Jansen did above I sadly tend to doubt this will be the case."
    author: "Datschge"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "It apears that its just plattdeutch that he has a problem with.. and if i understand it right plattdeutch is a very local dialect.\n\nIm from Denmark, and we have a few of these local dialects too (I speak northern-juthlandish myself). These dialects sounds like total rubbish to the rest of the population. However *all* of us understand regular Danish, so i would too consider it a total waste of time to i18n my own dialect (however it would be enormously amusing to show off with) .\n\nHowever the people who live in the alot bigger India might not speak anything but their local dialect. So this addition will give KDE a head-start when it comes to education - which is probably the most important use of computers in India both now and in the future.\n\n/kidcat\n\nPS.: *Anything* that can make KDE a strong competitor in the market of free educational software is Plain Good(TM)... as we all recall what happened to the kids who grew up with M$ in school ;-P"
    author: "kidcat"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "Well, the very point is that Plattdeutsch is not a \"very local dialect\", it's not even really \"only\" a dialect anymore since it has way more similarities with the German language spoken and written ~500 years ago (the resulting historic value is one of the main reasons why the Institut f\u00fcr Niederdeutsche Sprache Bremen is working on such a translation for Gnome) and is mostly spoken outside Germany nowadays (see Phantom's links above). For comparisation most local dialects in Germany are in the end just variations of pronunciation and grammar \"tweaks\" usual to any everyday speech.\n\nBut this all shouldn't really matter anyway, I think every language, regardless if spoken by one billion or just one thousand people, should be supported by \"the KDE project\" as soon as someone is willing to contribute and maintain it. That Klingon got rejected as being \"only a toy\" (while it actually is a working language) is kind of understandable. I however really can't see what caused Tim Jansen's repeated asocial knee-jerk reaction above. From what I can find on the net he's not involved with any documentation or translation stuff within KDE what would force him to have to care about Plattdeutsch as soon as it's started.\n\nHe might be concerned that many of the i18n packages will be unmaintained when a lot of them get added. But that's more a reason to improve organization and publicity for getting more contributions, not a reason to discourage the addition of any further language and thus making further contributions less likely.\n\nClearly, since he's known for being involved with KDE development and thus people expect him to be able to speak for KDE, I'm expecting to hear an explanation from him for this kind of motivation-killing rampage."
    author: "Datschge"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "FYI, Plattd\u00fc\u00fctsch is not a dialect at all (like swabian or bavarian, but an independant) language, and has been included in the european charta of regional and minority languages (http://conventions.coe.int/treaty/en/Treaties/Html/148.htm) for years now."
    author: "Frerich Raabe"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "Come on guys, don't bash at each other. If someone volunteers to do Plattt\u00fctsch then go ahead and *DO* it instead of complaining !\n\nGo take a plattd\u00fctsch dictionary and try to be accurate on your orthography :-)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "1. I only speak for myself, and definitely not for KDE.\n\n2. Actually the original posting referred to Gnome and their actions, not to KDE.\n\n3. I am not going to tell anyone what to do with his time, but I do have an opinion how helpful something is for the success of KDE and how big the chances are to complete this work.\n\n4. As I said, I would not consider support for \"Plattdeutsch\" very valuable for the success of Gnome or the KDE project. Other people are entitled to different opinions. The historic value, that you mention, does not make it more important for KDE unless you consider embracing historic languages a goal for KDE. The fact that an institute for language research did the translation, and not a speaker who wants to have Gnome in her native language, also tells about the practical importance IMHO.\n\n5. I definately think that other things, like more documentation, are more helpful for the success of KDE than having support for 20 different languages and dialects that are spoken in and around Germany. At least as long as it is reasonable that all speakers of those languages are comfortable reading one of the already supported languages as well, and I think that this is the case.\n\n6. Yes, language maintainance is a lot of work and will (hopefully!) be much larger part of the work in the future than it is now. Not only because of added features and applications, but also because many KDE applications have a poor standard for online documentation like tooltips and especially WhatsThis texts. It would be desirable that KDE's word count will increase _a_lot_ in the next years (a factor of 2 or even higher). This makes it even more important to have at least a few languages that are fully supported, and not a huge number of incomplete languages. I do know that you can not add up these efforts though (e.g. someone who wants to translate to Plattdeutsch is probably not interested in translating to any other language). But it would be, well, courageous to start a new i18n effort when so many other langauges do not even have 95% completeness.\n"
    author: "Tim Jansen"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "My only point is that by the way you are writing you are actively discouraging people from contributing at all. You are talking about unnecessary stuff like \"value\" and \"success\" while missing the point that the fact alone that someone starts and uses a particular project is already value and success enough for it to exist. When you start judging every project by whether outside people think it has \"value\" or \"success\" you can scratch 99%+ of all voluntary projects. It's usually hard to get any steady ecouragement when people intend to start a project, we really don't need to add deathly discouragement to the mix.\n\nEven now you take the incompleteness of existing language efforts for an implication that new language efforts shouldn't even be started and supported at all, ignoring that a (pseudo) language with a fanatic following like Klingon could possibly lead the way becoming one of the more active language teams, something which then again would encourage all other teams to be more active. =P\n\nThat being said I absolutely agree that the support for WhatThis text is really lackluster atm, something I attribute to its poor support even in the basic package with the standard language. WhatThis support should be made obligatory in every KDE program (only then we can expect l10n/i18n teams to pick them up as well)."
    author: "Datschge"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "I guess that depends on why you are contributing. You can either contribute for yourself (for fun, or to learn something etc), or you can contribute because you want to ensure that the project of your choice succeeds, and possibly the rest of the free software world as well. I contribute for the latter reason. \n\nEverybody is free to create new as many i18n projects as she wants, but unless it increases the number of potential users noticeably (like the support for Indic languagesdoes ), my reaction is as enthusiastic as when you, say, announce to write another text editor. Every new text editor helps the project, no doubt, because there will be a few people who prefer it over all the other text editors. Still there are few people who would enthusiastically welcome them. Especially when there are 50 other half-finished text editors....\n"
    author: "Tim Jansen"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "I don't like your idea of languages competing against each others like text editors. =P"
    author: "Datschge"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-20
    body: "Me neither.. eventhoug i would for provocative reasons like KDE to switch to en_GB as the default.. (only joking... well sort of)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "actually he only said that the gnome developers are wasting their time but did not argue against the languages (e.g. kde people are free to waste their time as well)."
    author: "AC"
  - subject: "Re: perhaps this will attract more developers :)"
    date: 2003-05-19
    body: "Saying \"everyone is wasting their time\" neither does encourage more people to contribute whatever they could contribute nor does it motivate people already contributing to keep contributing. So what's the point saying this when at the same time he knows that KDE is clearly not suffering on a too large amount of contributions, rather the opposite of that?"
    author: "Datschge"
  - subject: "text input ?"
    date: 2003-05-18
    body: "What about that increased support for text input ?"
    author: "capit. igloo"
  - subject: "Re: text input ?"
    date: 2003-05-19
    body: "According to Lars, the support should now be on par with whatever Windows 2000/XP offer."
    author: "Navindra Umanee"
  - subject: "Re: text input ?"
    date: 2003-05-19
    body: "Support for standardized text input for QT and GTK apps is there in form of XIM for years already, see http://www.suse.de/~mfabian/suse-cjk/xim.html\n\nDo a search for >> XIM \"X Input Method\" << in google and you'll be able to find controls for the respective languages. If you are using a commercial distri bug them to include those controls in the future."
    author: "Datschge"
  - subject: "That's very nice, but..."
    date: 2003-05-18
    body: "That's very nice, but what's there that GTK+ hasn't been able to do for over a year now?"
    author: "iamagod"
  - subject: "Re: That's very nice, but..."
    date: 2003-05-18
    body: "I don't know, I'm not using GTK+ and also don't see any point making combative comparisations. A more of flexibility regarding internationalizations is always good anywhere."
    author: "Datschge"
  - subject: "Re: That's very nice, but..."
    date: 2003-05-18
    body: "For one thing, there's no decent web browser that can handle indic languages with GTK (or any other way), but now with Qt 3.2 Konqueror can. That's something, wouldn't you say ?"
    author: "deepayan"
  - subject: "Re: That's very nice, but..."
    date: 2003-05-19
    body: "GTK+ isn't very usable for the developer like Qt is, so it really doesn't count.  KDE/Qt is much more developer-friendly."
    author: "ac"
  - subject: "Re: That's very nice, but..."
    date: 2003-05-19
    body: "so what...?\nFor KDE/QT this is good news. Is it bad news for the GTK camp? I doubt it, so sh..up."
    author: "Thomas"
  - subject: "Re: That's very nice, but..."
    date: 2003-05-19
    body: "Sigh... what has this new story got to do with Gtk or GNOME anyways?\n\nOf course, on the other hand, there were tons of \"Oh, Qt has supported this and that for over two years\" posts when Pango came out. "
    author: "tilt"
  - subject: "Erosion in the Culture"
    date: 2003-05-19
    body: "A people become poor and enslaved when they are robbed of the tongue left then by their ancestors; they are lost forever (Ignazio Buttira, sicilian poet)\n\nTwo per cent of the world's languages are becoming extinct every year and four european languages comprise more than 80 per cent of all book translations. (english among then)\nhttp://www.etcgroup.org/documents/other_etccentury.pdf\n\nKDE team is great !!\n"
    author: "Ventura"
  - subject: "Re: Erosion in the Culture"
    date: 2003-05-20
    body: "Good Post(TM) - that link was interesting! :-)\n\n/kidcat"
    author: "kidcat"
  - subject: "Thank you KDE/QT team"
    date: 2003-05-19
    body: "Lars and other KDE team,\n\nThank you very much for supporting Indic languages. \n\n"
    author: "prabu"
  - subject: "J\u00fcrgen L\u00fcters"
    date: 2003-05-20
    body: "LINUX-DESKTOP OP PLATT\n\n- 10/02 - Die Idee, das freie Computer-Betriebssystem Linux ins Niederdeutsche zu \u00fcbersetzen, wird konkret: Inzwischen gibt es eine erste Fassung des Linux-Desktops op Platt. Und eine eigene Web-Adresse haben die Entwickler mittlerweile auch: http://platt.gnome-de.org. Der Bremer Informatiker J\u00fcrgen L\u00fcters, der Vater der Idee, sucht nach wie vor Mitstreiter f\u00fcr sein kleines Team, um weitere Programmteile in die norddeutsche Regionalsprache zu \u00fcbertragen.\n\nWEITERE INFORMATIONEN gibt J\u00fcrgen L\u00fcters, Bremen, Tel. (0421) 49 39 90.\n\nINFORMATIKER WILL LINUX PLATT BEIBRINGEN \n\n- 08/02 - Was Computernutzern in zwei Dutzend L\u00e4ndern Recht ist, soll demn\u00e4chst auch den Plattdeutschen billig sein \u0096 und das im wahrsten Sinn des Wortes: Der Bremer Informatiker J\u00fcrgen L\u00fcters m\u00f6chte das freie Computer-Betriebssystem Linux ins Niederdeutsche \u00fcbersetzen. Er sucht f\u00fcr seine Idee eine Handvoll Mitstreiter, die Plattdeutsch k\u00f6nnen und was von Computern verstehen. Weil Linux frei, also gratis zur Verf\u00fcgung gestellt wird, winkt den freiwilligen Plattdeutsch-Helfern kein Geld, sondern nur die Ehre. \n\nDas Betriebssystem entstand vor zehn Jahren als Hobbyprojekt an der Universit\u00e4t von Helsinki \u0096 eine zun\u00e4chst milde bel\u00e4chelte kostenlose Software auf Basis des Industriestandards Unix. Inzwischen hat sich die Freeware-Idee so erfolgreich entwickelt, dass sie von \"Goliath\" Microsoft als unangenehme Konkurrenz empfunden und bek\u00e4mpft wird. Denn nicht nur \"David\" Linux gibt's umsonst, sondern auch dutzende von Software-Entwicklungen, die daf\u00fcr geschrieben wurden. Zum Beispiel ein Office-Paket mit Textverarbeitung und Tabellenkalkulation. Jetzt entdecken auch deutsche Beh\u00f6rden, von der Bundestagsverwaltung bis zur nieders\u00e4chsischen Polizei, diese Vorz\u00fcge und die M\u00f6glichkeit, mit Linux Geld zu sparen.\n\nDie plattdeutsche Variante des Betriebssystems w\u00fcrde zwei Fliegen mit einer Klappe schlagen: Linux, das es hochdeutsch l\u00e4ngst gibt, k\u00f6nnte auf Schulcomputern installiert werden und das Interesse an Niederdeutsch wecken. Und das, ohne daf\u00fcr auch nur einen Euro Lizenzgeb\u00fchr zu berappen. \n\nDas in Aussehen und Bedienung dem MS Windows-Vorbild weitgehend entsprechende Linux soll auch in der Platt-Variante nicht f\u00fcr Computer-Freaks oder Programmierer gedacht sein, sondern f\u00fcr \"Jan und alle Mann\", die mit Computern Briefe schreiben, Hausaufgaben erledigen oder im Internet surfen wollen. Es sollen nur die Teile des Programms ins Plattdeutsche \u00fcbersetzt werden, die f\u00fcr den Endanwender von Bedeutung sind.\n\nIm ersten Schritt will L\u00fcters mit anderen Platt-Linux-Helfern die etwa 11.000 \"Strings\" wie \"Datei \u00f6ffnen\", \"Drucken\", \"Speichern\", \"L\u00f6schen\" usw. \u00fcbersetzen, die die Linux-\"Oberfl\u00e4che\" Gnome bilden. Nach Vorstellungen des plattbegeisterten Informatikers sollte m\u00f6glichst von der Originalsprache Englisch ins Niederdeutsche \u00fcbertragen werden. Wenn das Betriebssystem mit seiner Bedienoberfl\u00e4che (Desktop) \u00fcbersetzt ist, k\u00f6nnte das Linux StarOffice-Paket, vor allem dessen ausgefeilte Textverarbeitung, in Angriff genommen werden. Die Idee: M\u00f6glichst gleich mit plattdeutschem Rechtschreib- und Silbentrennprogramm, damit die Plattschnacker auch zu Plattschrievers werden. Ob und wie dabei plattdeutsche Dialekte erhalten werden k\u00f6nnen \u0096 schlie\u00dflich wird im M\u00fcnsterland ein anderes Platt geschnackt als an der Nordseek\u00fcste \u0096, ist noch zu kl\u00e4ren.\n\nWEITERE INFORMATIONEN gibt J\u00fcrgen L\u00fcters, Bremen, Tel. (0421) 49 39 90, eMail: j.lueters@intranet-engineering.de.\n\n\n\n"
    author: "Gerd"
  - subject: "KDE and Malayalam Language(ml)"
    date: 2003-06-29
    body: "\nHi,\n\n  I have tested QT3.2 beta for malayalam language(Spoken in kerala, one of states in India, Language code ml). It had some errors, I have fixed some. To whom should I send my patches? If anybody knows it please inform me so that we can add malayalam too to kde.\n\n     ---------SajithVK----------"
    author: "Sajith VK"
  - subject: "Re: KDE and Malayalam Language(ml)"
    date: 2003-06-29
    body: "qt-bugs@trolltech.com. \n"
    author: "Sad Eagle"
  - subject: "Re: KDE and Malayalam Language(ml)"
    date: 2004-09-24
    body: "Hello,\n\nIam a linux developer.\nCan u please send malayalam support thinks to me?"
    author: "SarathLakshman"
  - subject: "Greet news for khmer "
    date: 2004-03-11
    body: "that greet that my language (khmer ) can work with kDE , and I want to test it can you tell me what should I do? I all so want to join khmer language project in kde but I can not find it,Is there anybody working with khmer kde? "
    author: "pen sokha"
---
With the release of <a href="http://www.trolltech.com/newsroom/announcements/00000127.html">Trolltech's Qt 3.2.0 beta1</a>, the <a href="http://developer.kde.org/development-versions/kde-3.2-features.html">upcoming KDE 3.2</a> has gained <a href="http://lists.kde.org/?l=kde-i18n-doc&m=105309821104136&w=2">increased support</a> for Indic languages both in terms of rendering and text input.  Currently, Devanagari (<a href="http://trolls.troll.no/lars/konqueror_hindi.png">screenshot</a>), Bengali (<a href="http://www.stat.wisc.edu/~deepayan/Bengali/WebPage/screenshots/konq-bn-2.png">bn-2</a>, <a href="http://www.stat.wisc.edu/~deepayan/Bengali/WebPage/screenshots/konq-bn-3.png">bn-3</a>, <a href="http://www.stat.wisc.edu/~deepayan/Bengali/WebPage/screenshots/konq-bn-1.png">bn-1</a>) and Tamil have been tested but Syriac, Tibetan, Khmer and others are expected to work as well.  <a href="mailto:mueller@kde.org">Dirk Mueller</a> writes: <i>"The KDE Project encourages interested people who understand these languages
to submit feedback and help the i18n teams (<a href="http://i18n.kde.org/teams/index.php?action=info&team=bn">Bengali</a>, <a href="http://i18n.kde.org/teams/index.php?action=info&team=hi">Hindi</a>, <a href="http://i18n.kde.org/teams/index.php?action=info&team=mr">Marathi</a>, <a href="http://i18n.kde.org/teams/index.php?action=info&team=ta">Tamil</a>, <a href="http://i18n.kde.org/teams/index.php?action=info&team=bo">Tibetan</a>, <a href="http://i18n.kde.org/teams/">etc</a>) with contributing a fully localized
KDE 3.2."</i>  These languages require Open Type fonts and a working Xft installation.
<!--break-->
