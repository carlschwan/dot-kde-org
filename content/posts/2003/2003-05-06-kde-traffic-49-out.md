---
title: "KDE Traffic #49 is Out"
date:    2003-05-06
authors:
  - "Juergen"
slug:    kde-traffic-49-out
comments:
  - subject: "Thanks a bunch =)"
    date: 2003-05-06
    body: "That was a very nice read, but the first link is broken, I am also sorry about the Koffice Icon Contest, maybe it should have been extended."
    author: "Alex"
  - subject: "Re: Thanks a bunch =)"
    date: 2003-05-06
    body: "Which link is broken?"
    author: "Navindra Umanee"
  - subject: "Re: Thanks a bunch =)"
    date: 2003-05-06
    body: "Eyecandy link is broken, sorry I said it was the first one"
    author: "Alex"
  - subject: "Re: Thanks a bunch =)"
    date: 2003-05-06
    body: "There are several broken links in the KC:\n\n| By Russell Miller,\n| <a href=\"\">Fabrice Mous</a>,\n| <a href=\"\">Juergen Appel</a> and\n| <a href=\"\">KDE Traffic Team</a>\n\nAlso,\n| Now, go ahead for <a href=\"www.kde-look.org\">eye-candy</a> or for shorts..\n\nshould be \n| Now, go ahead for <a href=\"http://www.kde-look.org\">eye-candy</a> or for shorts.\n\nAnd\n| www/announcements/changelogs/changelog3_1_1to3_1_2.php\n|\n| URL was not linked because it is not a valid URL as quoted.\n\ncan be replaced with\n| <a href=\"http://www.kde.org/announcements/changelogs/changelog3_1_1to3_1_2.php\"> changelog3_1_1to3_1_2.php</a>\n\n(www/announcements/... is not a broken URL, but the CVS position of that page)\n\nAnyway, these are small issues, thanks for this very informative KC.\n"
    author: "Olaf Jan Schmidt"
  - subject: "Many missing icons"
    date: 2003-05-07
    body: "Glad there's a contest ... most've my koffice apps hust use tiny white squares for their icons on the various toolbars."
    author: "KDE Fanatic"
  - subject: "koffice icon contest"
    date: 2003-05-06
    body: "I liked how the newsflash was written.. good job :)\n\nI think the koffice contest would have been a lot more successful if it were posted as a contest on the various art community sites, such as deviantart.com. kde-look is nice, but is rather.. umm.. KDE specific.. most artists use Windows or MacOS, so if you want to reach them, you have to post on more general sites :)"
    author: "fault"
  - subject: "Re: koffice icon contest"
    date: 2003-05-06
    body: "Tried but couldnt get it on deviantart.com :/"
    author: "cartman"
  - subject: "kedit"
    date: 2003-05-06
    body: "Why not just move kedit (or kwrite, or kate) into kdeextragear?\n\nI think the best way to do it is:\n\nkwrite - kdebase - keep kdebase slim.. most people only need a basic text editor.. not the power of kate..\nkate - kdeutils - someone in the \"remove kedit\" thread mentioned that moving kate to kdesdk might be good. The presumption there is that kate is a programmer's text editor. It's not--- it's an advanced text editor than can be used as a programmer's text editor. \nkedit - kdeextragear\n\n"
    author: "fault"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "KDE Extra Gear isn't really a dumping ground, that's kdeblackhole.  :-)\n\nSeriously, why keep KEdit if it is being superceded by KWrite?"
    author: "anon"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "Because at the moment it is the only editor that supports bi-di, this might change with Qt 3.2 which might be able to enable kate to also support it (for details, please read the lists).\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "And AFAIK it is only the only editor for editing UTF8..."
    author: "AC"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "You can load utf-8 files into Kate if you select utf-8 encoding in the load/save file dialog. Or is this not what you meant?"
    author: "Apollo Creed"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "Oh, thanks... well hidden but I found it :)\n"
    author: "AC"
  - subject: "Re: kedit"
    date: 2003-05-07
    body: "I actually had to have it pointed out to me myself. If I had known it was there earlier, it would've saved me an evenings worth of work. ;) It really is well hidden..."
    author: "Apollo Creed"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "Merge it and kill KEdit?"
    author: "Somebody"
  - subject: "Re: kedit"
    date: 2003-05-09
    body: "I noticed that kedit behaves different to kwrite, when using copy & paste via both the mouse and the keyboard. kedit copies whole words (double-click) and pastes them always in the first column (whereas kwrite analyses the syntax, so you can e.g. copy numbers out of an url but unfortunately not whole \"words\". Also kwrite pastes everything at the current curser position, e.g. in the middle of the screen.) Moreover kedit additionally supports also copy and paste with the combinations CTRL-INSERT and SHIFT-INSERT and so behaves similar to notepad in Windows (or the old DOS editors/programming UIs :)\n\nIn general there is a little more overhead when using kwrite. It's greatest feature is however the highlighting.\nkedit is then more productive when used by people who are(where) familiar with Windows/DOS. Though kedit remains only a spartan text editor.\n\nMy conclusion is that I would like very much that the (advantageous) copy&paste features of kedit would be moved also to other editors like kwrite!\n\n\n(I would like to mention that kwrite does not support other charset/encodings like iso8859-2 (well, I could configure it under 'highlighting' and checked it with most fonts but the special characters are not shown, whereas kedit on the same installation can show them well!)\n"
    author: "david"
  - subject: "Re: kedit"
    date: 2005-08-15
    body: "\"why keep KEdit if it is being superceded by KWrite?\"\n\nBecause it's smaller, less bloated, loads faster, and takes less RAM. It's good to have both a lightweight editor (like Windows' Notepad.exe) for viewing text files, taking temporary notes to and from the clipboard, etc., and a serious editor for coding and writing like Kate. I personally find KEdit more useful than KWrite because of this; if I want a quick job I'll pick KEdit, if I want a serious job I'll pick Kate; when should I use KWrite?"
    author: "Wiseman"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "> The presumption there is that kate is a programmer's text editor. It's not--- it's an advanced text editor than can be used as a programmer's text editor. \n\nSay what you will, but when you load Quanta+ you're loading a number of Kate interfaces and if they are not there then we have a problem. The packaging for Quanta would have added dependancies that make little sense. One of the reasons an app goes in kdelibs/kdebase is because other apps depend on it."
    author: "Eric Laffoon"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "Kate is an integral part of kdelibs, and that should never change within KDE 3.x. \n\nBut is the kate application really a integral part of kdebase? Quanta does not rely on the kate application (kdebase), but only the part/KTextEditor interface (kdelibs), no? "
    author: "fault"
  - subject: "Re: kedit"
    date: 2003-05-06
    body: "kate -> kdesdk. it's a programmer's editor, kdesdk is where the programming tools go. it would fit in nicely with other bits of kdesdk like cervisia, IMHO."
    author: "Aaron J. Seigo"
  - subject: "Re: kedit"
    date: 2003-05-08
    body: "Noo, please: Don't throw it into kdesdk. It is GREAT for quick editing of HTML files and all sorts of text files a number of non-programmers do need and use Kate.\n\n"
    author: "Anonymous"
  - subject: "Re: kedit"
    date: 2003-05-08
    body: "Throw it into kdesdk. And tell all of this distribution people like Mandrake, Redhat etc to abandon the \"kdenetwork.rpm\" approach. I want maybe KMail, but not Kppp (I have DSL). I definitely don't want to install 12 apps at once.\n\nIf the distributions would be saner, it would not make any difference if kate would be in kdesdk or in kdebase. Because then \"kdebase\" would just mean a CVS repository for Developers.\n\nHolger, happily compiling from CVSUP :-)"
    author: "HolgerSchurig"
  - subject: "Re: kedit"
    date: 2003-05-08
    body: "Actually, I'd say dump kwrite and kedit.  Kate is by far the best, and really the only text editor that is needed for KDE (in my opinion).  Since KDE is so modularized, I really see no need for multiple text editor applications.  Sure MS Windows has Notepad and Wordpad, but that's just because Notepad is a simple dumpy little editor and Wordpad is for those who are too poor to buy office but need some type of a basic word processor.  KDE doens't have to follow that route because it has Kate and far exceeds any other GUI text editor.  Although a KJoe part would be nice but I bet it could be emulated with Kate somehow.\n"
    author: "Super PET Troll"
  - subject: "KD Chart"
    date: 2003-05-06
    body: "Re KD Chart (used as backend of KChart...)\n\nHow does the licensing of this work? Is it dual GPL / Commercial, ala Trolltech?  Couldn't find info on the page. "
    author: "rjw"
  - subject: "Re: KD Chart"
    date: 2003-05-19
    body: "Yes, exactly. The idea is that people shouldn't have to read yet another license text, so we are using exactly the same licenses (for the Commercial license with kind permission of Trolltech). The GPL version is in the KDE CVS repository and is synced on a regular basis with the Commercial version (which also comes with support and a nice manual).\n\nCheers,\n\nKalle Dalheimer\n"
    author: "Matthias Kalle Dalheimer"
  - subject: "KRename"
    date: 2003-05-08
    body: "KRename seems like a really cool application.  I don't know if I could even count the number of times something like this would have come in hand.  I have written numerous shell scripts for mass renaming of the years.  Only last thing I could think of that would make this even better, well maybe cause it's just an idea off the top of my head, would be to have KRename also be a Konqueror sidebar.\n"
    author: "Super PET Troll"
---
<a href="http://kt.zork.net/kde/kde20030504_49.html">Kernel Cousin KDE #49</a> covers the <a href="http://www.kde.org/announcements/changelogs/changelog3_1_1to3_1_2.php">upcoming KDE 3.1.2</a> release, the new <a href="http://www.kde.cl/">Chilean KDE site</a>, the <A href="http://dot.kde.org/1051497356/">KDE Developers' Conference</a>, the results of the <A href="http://dot.kde.org/1045754124/">KOffice icon contest</a>, <a href="http://www.krename.net/">KRename</a> and much more.  Get it from the <a href="http://kt.zork.net/kde/archives.html">usual source</a>.
<!--break-->
