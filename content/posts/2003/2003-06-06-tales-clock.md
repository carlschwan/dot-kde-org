---
title: "Tales of a Clock"
date:    2003-06-06
authors:
  - "clee"
slug:    tales-clock
comments:
  - subject: "Another thing that needs attention"
    date: 2003-06-05
    body: "PITE (eye) for former Win32 lovers http://bugs.kde.org/show_bug.cgi?id=56654 - about no borders in maximized window mode."
    author: "Tar"
  - subject: "Re: Another thing that needs attention"
    date: 2003-06-06
    body: "This actually should be fixed for usability reasons, not just for Windows migrants. As the bug says, it would greatly improve usability to have things such as scrollbars (my pet peeve) right at the edge of the screen.\n\nIn fact, I've been thinking recently that it should be possible simply to snap windows to the edge of the screen (as can be done now), but with the innovation that the actual *border* of the window would go offscreen in the process. Thus, any window positioned right against a screen edge would have no annoying border to hamper interaction."
    author: "Rakko"
  - subject: "Re: Another thing that needs attention"
    date: 2003-06-06
    body: "Let me see whether I understood the problem correctly - you don't want to see the window border of the maximized window. As far as I know you can have this behaviour. In the KDE Control Center->Window Behaviour->Moving make sure to disable the \"Allow moving and resizing of maximized windows\" option. I believe this will give you the desired maximized window appearance. But i must admit, the name on this config option is not descriptive enough.\n\nHope this helps,\n\tPaul."
    author: "Paul Koshevoy"
  - subject: "Re: Another thing that needs attention"
    date: 2003-06-06
    body: "Sorry, doesn't seem to fix it for me (KDE 3.1, Keramik theme).  This bug has long annoyed me."
    author: "Peter Kasting"
  - subject: "Re: Another thing that needs attention"
    date: 2003-06-08
    body: "I just tried this on SuSE 8.2 and it worked. Maybe the reason it did not work for you lays in the fact that you are using Keramik? Try the same test with KDE2 window decoration (combined with dotNET widget Style, it looks great).\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: Another thing that needs attention"
    date: 2003-06-09
    body: "Works on Keramic for me"
    author: "James Duncan"
  - subject: "Re: Another thing that needs attention"
    date: 2003-06-09
    body: "Works on Keramic for me on 3.1, are you sure you have the correct option?\n\nThough I also didn't know about this option, thanks for that!"
    author: "James Duncan"
  - subject: "Thank you Gnome"
    date: 2003-06-05
    body: "I want to thank Gnome and Ximian team to make react KDE team to fix a bug.\nContinue guys !!! If you talk about each KDE bug, KDE will be more and more better .\n\nMoreover, is it possible for the Mozilla team to talk about Konqueror and to say that \"Konqueror support less CSS specifications than Mozilla\" ? :)\n \nLong life to KDE and GNOME \\o/"
    author: "Shift"
  - subject: "Re: Thank you Gnome"
    date: 2003-06-06
    body: "By the way, do anyone at kde know when the gnome filemanager will get improved?"
    author: "imr"
  - subject: "Re: Thank you Gnome"
    date: 2003-06-06
    body: "probably between now and GNOME 2.8."
    author: "reflux"
  - subject: "Nautilus has been improved"
    date: 2003-06-06
    body: "in the development branch it flies, its as fast as konqueror and in a few cases faster, such as direcories with lots of files such as images. it also has a few great features which konqueror does not and i would like to see such as emblems and zoom as well as a slick look with shadows behind previews for screenshots, text files etc., this make sit look infinitely better.\n\nIn fact there is a topic on this: http://bugs.kde.org/show_bug.cgi?id=58944"
    author: "Mario"
  - subject: "Re: Nautilus has been improved"
    date: 2003-06-06
    body: "Considering that SadEagle's been at work on QIconView in our CVS, I seriously doubt that Nautilus will be faster (if it even is) for long, wrt huge directories.\n\nCompetition is great, isn't it? :)\n\n-clee"
    author: "clee"
  - subject: "Re: Nautilus has been improved"
    date: 2003-06-06
    body: "Not in our CVS ATM -- e-mailed the Trolls, no response back yet though. Once they merge in the QIconView fix, there are less significant but still noticable changes that can be made to KFileIVI/KIconViewItem, Still. (Although they are tricky due to excessive inlining in KIconViewItem constructor -- see kde-optimize archives). \n"
    author: "Sad Eagle"
  - subject: "The massive obvious problem with Nautilus"
    date: 2003-06-06
    body: "... globbing ... i.e. the total lack of it.  In konqueror's location dialog:\n\n~/*jpg  works (konqueror is borken too for things like *.[jpg|png] etc. but this will be fixed)\n\nNautilus has no drop down history in the location bar either ... pure dumbness.\n\nBut really ... the lack of GLOBBING is silly and is proof of the clumsiness of the Gnome framework.  Not even the experienced Gnome hackers can implement it without massive code rewrites.\n\nKonqueror also has things like Ctrl-T to \"start terminal here\" etc. (plus drag and drop of directories into terminal windows! ...Nice!!\n\nNautilus may look better and be faster but it lackds very basic features and Gnome cannot implement them ..."
    author: "KDE Fanatic"
  - subject: "Re: The massive obvious problem with Nautilus"
    date: 2003-06-06
    body: "> But really ... the lack of GLOBBING is silly and is proof of the clumsiness of the Gnome framework. Not even the experienced Gnome hackers can implement it without massive code rewrites.\n\nActually, Nautilus in gnome 1.4 had globbing. It was removed (sigh) in gnome 2.0. Not sure if it's back in gnome 2.2. Probably not though. It has nothing to do with the clumsiness of the gnome framework (which parts of are not very well intregrated compared to KDE, but this might be an advantage to some too.), or the coding experience of gnome hackers. It has everything to do with gnome taken over by Havoc (sigh) and people who share a Havocian school (sigh) of thought.\n\nIt was all of these kind of small deletion of features that made me switch from gnome 1.4 to kde 3.1-- I was massively underwhelmed by gnome 2.0's features. Too bad havoc ruined what was a decent desktop."
    author: "non-anon"
  - subject: "Re: The massive obvious problem with Nautilus"
    date: 2003-06-07
    body: "I, unfortunately, had the same problem. The Havocian takeover of GNOME finally gave a large, user-apparent difference between GNOME and KDE. I liked GNOME 1.4 much better than KDE. In fact, I learned GTK and GNOME 2, and compiled gnome 2 from cvs when it first became remotely usable. At that point I disliked KDE. But then, when so many features were dropped, GNOME 2 became much more difficult to use. I was a supporter of viewports and edge-flipping, which quickly alienated me from the Havocian school of thought. Nautilus 2 is much better than Nautilus 1 in speed and looks, but in usability, it lacks essential features for advanced users, like so many other elements of GNOME 2.\n\nAnd thus, I am now a KDE user. Havoc has done a good job, it seems, of angering the advanced users in GNOME's user base."
    author: "C. Evans"
  - subject: "Re: The massive obvious problem with Nautilus"
    date: 2003-06-06
    body: "You could just type ~/*.jpg *.png in konq for your example above IIRC.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: The massive obvious problem with Nautilus"
    date: 2003-06-12
    body: "Indeed! Wow. I would have never thought this worked.\n(And yes, I'm the one who coded that feature, long ago :))"
    author: "David Faure"
  - subject: "Re: The massive obvious problem with Nautilus"
    date: 2006-10-17
    body: "A little late, but the syntax is not *.[jpg|png] but *.{jpg,png} which should work if Konq uses the standard glob."
    author: "jake"
  - subject: "my bad"
    date: 2003-06-07
    body: "I was just trying to joke with the person above and of course made a fool of myself.\nI was talking of the infamous file dialog. The one I've been bothered by for many years in gimp and mozilla.\nI hope they finally found someone interrested in updating that old piece of crap. I have nothing against the gnome project, but to have to scan through directory with that old thing is a pita."
    author: "imr"
  - subject: "hacking away"
    date: 2003-06-05
    body: "With plenty of free time I spent yesterday's afternoon and this morning coding up the changes.  Now, using less code, memory, starting faster and having a better configure dialog I have commited my changes.  http://www.csh.rit.edu/~benjamin/clock.png here is a screenshot of the new configure dialog."
    author: "Benjamin Meyer"
  - subject: "Re: hacking away"
    date: 2003-06-05
    body: "wow that dramatically reduces a lot of the unecessary, good work.  I love how kde responds criticism, not by getting into an ego match with gnome by comparing its others features, but by simple fixing what was criticized.  excellent work, however, why is the horizontal bar so long especially without being necessary, at least without the list expanded.  "
    author: "good changes..."
  - subject: "Re: hacking away"
    date: 2003-06-05
    body: "what sucks about this is that KDE seems to only be responding to criticism. This should\nof been fixed years ago. "
    author: "anonymous"
  - subject: "Re: hacking away"
    date: 2003-06-05
    body: "sometimes it just works this way-- most developers are working in their free time, and want to develop on things that they are interested in- for most, this means features. \n\nif you're interested in something like usability in KDE, I suggest you join the team! anybody is free and open to."
    author: "trill"
  - subject: "Re: hacking away"
    date: 2003-06-05
    body: "Have you ever filed a bug ?\nHave you ever voted for a bug ?\nIs any high rated bug being ignored for years ?\n"
    author: "cylab"
  - subject: "Re: hacking away"
    date: 2003-06-07
    body: "http://bugs.kde.org/show_bug.cgi?id=24806"
    author: "anon"
  - subject: "Works for me in KDE 3.1.1 (n/t)"
    date: 2003-06-07
    body: "Works for me in KDE 3.1.1"
    author: "em"
  - subject: "Re: hacking away"
    date: 2003-06-05
    body: "Nobody can look at 8500+ reports at bugs.kde.org and listen to all the additional complaints and flames all at once, and while this particular bug is embarassing and ideal food for others to show how \"crowded and cluttered\" KDE is in their opinion, it never really had been a serious itch needed to be scartched for anyone. Just look at the numbers of votes #58775 got (currently only 40, from four people), there are a whopping 133 reports which have more votes than this and are such more likely to get attention.\n\nDid *you* vote for #58775 before complaining here?"
    author: "Datschge"
  - subject: "Re: hacking away"
    date: 2003-06-06
    body: "I agree, i use this option at least 6 times a day, and this really is slowing down my work time \n\nOK, to be serious, it's nice that this gui has been fixed, but to say that it had to be done years ago?\n\nRinse\n"
    author: "rinse"
  - subject: "Re: hacking away"
    date: 2003-06-06
    body: "perhaps you only notice when there is a response to criticism because someone has made a public bruhaha about it bringing it to your attention. and then we post it on theDot, only to raise the awareness even more. truth is that lots of such issues get fixed quietly and effectively without any public criticism beyond users asking for things in bugs.kde.org, mailing lists or IRC. often it's common sense. it just takes time. when something gets openly and blatantly criticized in a cheap shot remark, that thing tends to get reprioritized on various developer's TODO lists so that the same criticism can't be repeated over and over ad nauseam. that gets tiring.\n\nif you REALLY want to see things get fixed faster, sponser a developer or strap on your coding shoes and go spelunking in the code."
    author: "Aaron J. Seigo"
  - subject: "Re: hacking away"
    date: 2003-06-05
    body: "Looks great.  I suggest the heading City might be better as Location"
    author: "Ross Baker"
  - subject: "Re: hacking away"
    date: 2003-06-06
    body: "Please make the clock faces plugins while you're at it. :-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: hacking away"
    date: 2003-06-06
    body: "What are you talking about ?"
    author: "Shift"
  - subject: "Re: hacking away"
    date: 2003-06-06
    body: "I think http://c133.org/files/newclockprefs.png is much better.  There's no reason you need to cram everything in one dialog like that.  The timezones rightly have their own panel."
    author: "anon"
  - subject: "Re: hacking away"
    date: 2003-06-06
    body: "FULLY agreed!"
    author: "Frank"
  - subject: "Re: hacking away"
    date: 2003-06-06
    body: "Full ACK!!\n\nAlthough not perfect, this one looks much better. Having everything on one\npage plus a separate dialog for the clock type (I guess that's what the 'configure type' button is for) is too much of \"simplification\".\n\nBesides I change the clock type more often than the timezone. So this should be really separate tabs.\n\nChristian"
    author: "cloose"
  - subject: "GNOME and KDE hig"
    date: 2003-06-05
    body: "I know sometime back, there was talk about merging the GNOME and KDE higs, (as in finding common elements, and put that stuff in a seperate document on freedesktop.org..) Anyone know what happened to that effort?\n\nAn article about that is at http://www.osnews.com/story.php?news_id=2730"
    author: "trill"
  - subject: "Re: GNOME and KDE hig"
    date: 2003-06-05
    body: "I think a better idea would be to unify the \"KDE User Interface Guidelines\", and the \"KDE User Interface Guidelines\". (Yes, they both have the same title..)\n\nThey are located at : http://developer.kde.org/documentation/standards/kde/style/basics/index.html\nand at :\nhttp://developer.kde.org/documentation/design/ui/\n\n\nAfter these have been merged, update the first to KDE 3 levels.. the standards guide has lots of screenshots and code from KDE1. Much of which is currently irrelevent. Just browing some of the code in the guide shows classes like KCommand and KColorDrag.. I don't think those have been used since KDE 1. This guide also needs to cover more controls that KDE apps currently use. Things like listviews aren't even currently covered in the guide.\n\nThe second guide is pretty good-- covers lots of Human User Interface Interaction theory-- sticking it at the beginning of the guide would be good. There has been a lot more info since then, so that could be put there as well.\n\nI'm currently very busy-- any takers in doing this? I've been wanting to for a while. You don't even need to be a programmer-- just have KDE cvs access (anybody with cvs access can modify http://developer.kde.org/).. I'm sure coolo or dfaure would be happy to create a cvs account for anyone willing to update the interface guide."
    author: "lit"
  - subject: "Re: GNOME and KDE hig"
    date: 2003-06-05
    body: "Your good suggestion has been noted in my to-do list. ;)"
    author: "Datschge"
  - subject: "browsing the kde-usability mailing list lately.."
    date: 2003-06-06
    body: "activity seems to be kicking up, once again.. at certain period of it's history, this list has even been more busy than kde-devel.. for most of this year, it wasn't though.\n\nFor KDE 3.2, I hope the this get finished :\n\nreplacing the window deco, colors, styles with a themes control panel (which has features same from all of them...)\nhttp://www.avenheim.online.fr/kthemes2-2.png"
    author: "reflux"
  - subject: "Re: browsing the kde-usability mailing list lately.."
    date: 2003-06-06
    body: "Wow!! I really hope that that makes it to KDE 3.2! That looks amazing!"
    author: "Braden MacDonald"
  - subject: "Re: browsing the kde-usability mailing list lately.."
    date: 2003-06-06
    body: "Wow!! I really hope that that makes it to KDE 3.2! That looks amazing!"
    author: "Braden MacDonald"
  - subject: "Re: browsing the kde-usability mailing list lately.."
    date: 2003-06-06
    body: "Sorry, but your design isn't perfectly.\n\nThe list(redmond, keramik, ...) available on the left, ans the listbox \"style\" are the same ? if not, why one in a listbox, and the other in a plain list?\n\nWhy do you have a listbox \"colours\", a grey button ton configure, and a onglet \"custom colours\" \n\nAnd the part \"Description\" shouldn't be an onglet."
    author: "Mooby"
  - subject: "Re: browsing the kde-usability mailing list lately"
    date: 2003-06-06
    body: "<i>Sorry, but your design isn't perfectly.</i>\nPersonally, I don't it's at least way better than the way it is done right now. It provides easy all-in-one-place theming.\n\n<i>The list(redmond, keramik, ...) available on the left, ans the listbox \"style\" are the same ? if not, why one in a listbox, and the other in a plain list?</i>\nThe first one is the entire theme, while the listbox is just the style (which only covers the shape of widgets and so).\n\n<i>Why do you have a listbox \"colours\", a grey button ton configure, and a onglet \"custom colours\"</i>\nHere I have to agree with you. It would be more logical to remove that tab and put its contents into a dialog that's shown when the configure button is clicked.\n\n<i>And the part \"Description\" shouldn't be an onglet.</i>\nWhy not?\n\nThe only complaint I have is that this module covers icon and window dec. selection, but does not show a preview of the icons or the windec.\n\nFor the rest, yes I would really like to see this in KDE 3.2 ;)"
    author: "Arend jr."
  - subject: "THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "Thanksa  lot for fixing that, i couldn't stand the old panel, I'm glad KDE developers are focusing more on usability than just features.\n\nI am sure they want the most usable desktop, even more so than GNOME and don't want to bloat it with rarely used features. I really hope losts of the dialogs and uis are getting cleaned up like the clock one."
    author: "micahel"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "> I am sure they want the most usable desktop, even more so than GNOME and don't want to bloat it with rarely used features. I really hope losts of the dialogs and uis are getting cleaned up like the clock one.\n\nYup.. and the good thing is that as more and more dialogs are being created in Qt Designer, anybody can modify or create new ones. I've noticed that you seem to really care about KDE usability. If you want to help, load a copy of Qt-Designer, redesign a control panel or dialog, and post it either here (if you want immediate feedback, or are replying to a thread), the kde-usability mailing list, or bugs.kde.org. \n\nKDE is all about community involvement after all-- even more so than GNOME, where companies  like Ximian and Sun set policies. "
    author: "till"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "I don't know how Qt works, I can make a mockup design, but I can't connect the signal and slots correctly yet. I am jsut starting to learn C++ now =)\n\nAnyway, do kde applications share a common dialog framework or is it unique for all apps?\n\n"
    author: "Michael"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "> I don't know how Qt works, I can make a mockup design, but I can't connect the signal and slots correctly yet. I am jsut starting to learn C++ now =)\n\nThat's quite fine-- even submitting a non-slot connected ui file helps. It gives people ideas that otherwise they wouldn't have. It's much easier, and concrete to visualize something based on a Designer ui file than saying visualize a bug report that simply says \"oh the layout of this and this suxx, and is too cluttered and could be done a lot better\", which unfortunatly, is how a lot of people \"constructively\" complain, but doesn't end up being constructive at all. =)"
    author: "till"
  - subject: "Mockups"
    date: 2003-06-07
    body: "I'm wondering... Is there a website/mailing list where people can request and submit UI mockups? I'd like to do one or two (if I can find some free time...) but I don't know what needs work and I don't know how to find out. If there were some global list of what UI needs work, that might help people in my position. (I tried searching bugzilla, but I don't know what to look for.)"
    author: "roie_m"
  - subject: "Re: Mockups"
    date: 2003-06-07
    body: "try www.kdelook.org. not really an official thing for developers, but a good place to get input from other people. "
    author: "anon"
  - subject: "Re: Mockups"
    date: 2003-06-07
    body: "Better kde-usability mailing list. On kdelook.org everything with \"Improvement\" in title gets 80+% \"good\" automatically."
    author: "Anonymous"
  - subject: "Re: Mockups"
    date: 2003-06-08
    body: "I'd prefer both kdelook.org and bugs.kde.org, mailing lists (especially kde-usability) easily get off topic and thus are quite inefficient. looky on the other hand allows one to check the public response to new UI's and buggy has a very reliable voting system."
    author: "Datschge"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "> Anyway, do kde applications share a common dialog framework or is it unique for all\n> apps?\n\nKDialogBase\n \n "
    author: "Aaron J. Seigo"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "I haven't really looked at Qt Designer yet. \n\nDoes it work with KDialogBase? I'm really interested because then I might use it for Cervisia.\n\nChristian"
    author: "cloose"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "the easiest thing to do IME is to create your UI as a Widget, then create a KDialogBase subclass which instatiates and controls the Designer created UI as a child widget. it creates a greater separation between the UI and the code that drives it, which helps speed up development. it also makes tweaking the UI painless =)"
    author: "Aaron J. Seigo"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "Eyecandy != usability. The keyboard input & shortcut system is still imperfect. An single example: Why do I have to rename a session in Konsole using STRG+Alt+S while renaming in Konqueror is simply pressing F2? The worst idea of navigation you'll find in KMail imho. I'm using v1.52 (KDE 3.1.2), so I hope this will change."
    author: "Carlo"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "Many of the shortcuts in konsole are different from what you'll find from other KDE apps because some of the same shortcuts may be (or may not be) used by terminal apps, and therefore, must be passed on to the terminal widget."
    author: "till"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-07
    body: "O.k., but F2?\n\nOn the other hand one could ask, why it's not STRG+R in Konqueror. I mean, all the data handling is STRG+C, STRG+V, but then F2. That's another point in KDE in general; I would expect, that the shortcuts for data-handling and interaction with more application-specific functions would be separated in a way that makes sense. STRG+_SOMEKEY_ only for data for example. It's obvios that there are applications , where this is not possible, but I can't see a _strong_ policy in KDE. \n\nI'm aware, that this point of view breaks the idea of using F2 in Konsole. Take it as a test balloon. ;-)"
    author: "Carlo"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-06
    body: "I hope you told us via bugs.kde.org what you don't like about the navigation in KMail (which might be non-standard, but which is very fast) by either submitting appropriate wishes or by voting for already existing wishes.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-07
    body: "No, I didn't. I'll have a look. But I really don't _want to_ understand your point. One main advantage of using a DE is standardization to give the user an intuitive interface at hand, and not \"huh that's fast, like vim, if you know how to...\". ;-)\n\n"
    author: "Carlo"
  - subject: "Re: THANKS I HATED THE OLD PANEL!!!"
    date: 2003-06-08
    body: "For me KMail's navigation is a breeze, and I hope it will become the standart for that kind of UI's everywhere (eg. KNode would greatly profit of it)."
    author: "Datschge"
  - subject: "picayune"
    date: 2003-06-06
    body: "This is a petty gripe of Nat's. Yes, the clock dialog should have been improved, and I'm glad that it has been, but if this is the foremost complaint about KDE on his mind, then I think we're doing pretty damn well."
    author: "David Johnson"
  - subject: "Re: picayune"
    date: 2003-06-06
    body: "He didn't say that it in itself was the worst problem with KDE, he used at as an exmaple of the feature creep and clutter found in much of the KDE UI."
    author: "damiam"
  - subject: "What theme is that in the example?"
    date: 2003-06-06
    body: "I want it! In speaking of UI issues, I really would like to see more simple, out-of-the-way themes become available. Light Style 3rd rev. is nice, and KDE Classic is very well designed, and dotNet has good mouse-over & shading, but I still haven't found the perfect one. Perhaps this is it?\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: What theme is that in the example?"
    date: 2003-06-06
    body: "that's dotNET from cvs-head.. very nice theme.. kermaikII from cvs-head is improved a lot too."
    author: "till"
  - subject: "Re: What theme is that in the example?"
    date: 2003-06-06
    body: "No, really, it is dotNET!\n\n(I already sent you an email explaining this, but I'll reply here too for everyone else.)\n\nIt really is. Grab http://c133.org/files/dotNET-1.4.1.tar.bz2 and install it if you want the absolute latest code (1.4 is the latest public release; 1.4.1 isn't final yet, but it fixes an annoying bug with the bolded text on 'default' QPushButtons).\n\nAlso read http://c133.org/ and check back every week or so for new updates/information. :)\n\n-clee"
    author: "clee"
  - subject: "Re: What theme is that in the example?"
    date: 2003-06-06
    body: "Ahh! So, after downloading & compiling...I see no difference. I must be doing something wrong. A good idea perhaps in the Style applet is to show what version of each theme is installed? I will try 1.4.1 now.\n\nThanks!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: What theme is that in the example?"
    date: 2003-06-06
    body: "Make sure that your KDEDIR was set when you compiled. (If it wasn't, you'll have a /usr/local/lib/kde3/plugins/styles/ directory - delete it.)\n\nTry 'kde-config --prefix' to figure out where KDE is installed, then 'export KDEDIR=`kde-config --prefix`' and try 'configure && make && make install' again.\n\nOr else wait for 3.2, when it'll all be in kdeartwork. :)\n\n-clee"
    author: "clee"
  - subject: "I thought I help out a bit :)"
    date: 2003-06-06
    body: "So-so: <a href=\"http://c133.org/files/newclockprefs.png\">http://c133.org/files/newclockprefs.png</a><br>\nBAD:  <a href=\"http://www.csh.rit.edu/~benjamin/clock.png\">http://www.csh.rit.edu/~benjamin/clock.png</a><br>\n<p>\nBen's version is extremely crammed with things, while the first one is better\nbecause it does have a logical flow, it is just that ordering is bad, while\nthe frames are not nessesary in this specific design, it makes everything\nlook boxy and more complicated that it really is. Just let the natural flow\nout....\n<p>\nPlease consider my mockup and adopt the Modified one if you agree with it. Clee's effort is not bad, just needed a bit of clean up. :)\n<p>\nModified effort:<br>\n<a href=\"http://img.osnews.com/img/3721/clock.png\">http://img.osnews.com/img/3721/clock.png</a>"
    author: "Eugenia"
  - subject: "Got to give it to you Eugenia"
    date: 2003-06-06
    body: "Yours does look better, and I like the way the \"Foreground\" and \"Background\" color options are aligned in yours more too.\n\nThe font selections is also better, why do you need \"font:\" when there is a choose font on the button? =p\n\nIn addition, it also seems less cluttered and I like it better over all.\n\nYou have a great eye for detail and that's really improtant when it comes to usability, thanks a lot =), I'm sure the KDE developers will adapt the design to your mockup. If you know any other poorly designed KDE dialogs or whatever, please do post about them. it might be easier for you to actually jsut use qt Designer without connecting the slots, I just used it for fun and its easier than using an image program when you want to show this kind of stuff, especially considering that many KDE programs have a ui file available for download. \n\nAgain thanks, and I hope you notice more things, I honestly didn't even notice those problems until you pointed them out. I really want KDE to excel in usability so we need your help too =)\n\nBTW: There is a problem with the timezones when you select a whole lot: http://bugs.kde.org/show_bug.cgi?id=58948\n\nSince we were on the subject of the clock configuration dialog anyway..."
    author: "Mario"
  - subject: "Re: I thought I help out a bit :)"
    date: 2003-06-06
    body: "Please reload the mockup, I did some changes:\nhttp://img.osnews.com/img/3721/clock.png"
    author: "Eugenia"
  - subject: "Re: I thought I help out a bit :)"
    date: 2003-06-06
    body: "Actually I liked the frames. IMHO your mockup now has too many elements scattered over one page. If I remember right, the human mind can only grasp 5+2 elements at once. That's why a sheet of music uses 5 lines. You now have 10 active elements.\n\nBut your positioning of the foreground and background buttons is much better. Although I think it would be even better when they could be somehow moved to the left border.\n\nJust some suggestions. :-)\n\nChristian\n\n"
    author: "cloose"
  - subject: "Re: I thought I help out a bit :)"
    date: 2003-06-06
    body: "It too like clee's interface a bit better. I like Eugenia's even better though. The group boxes are quite unnecessary here- all the controls in the \"Options\" group box have an effect on appearance, and all of the controls in the \"Appearance\" group box are options. \n\nThey were all display options, so changing the tab from \"General\" to \"Display\" is also good. Since timezones have nothing to do with display options, keeping it in a different tab promotes organization and usability. \n\nThe Foreground and background colors selection boxes are in the right place in Eugenia's mockup too. They are suboptions of \"Use Custom Colors\", and the state of the parent has an effect on the children, so therefore, they are indented. Taking a quick look at the old kde user interface guide, it has nothing on this. I know that both GNOME's HIG and Apple's pre-Aqua HIG do, however. If someone were ever to update the KUIG (or rewrite it from scratch), it should include things like this."
    author: "fault"
  - subject: "Re: I thought I help out a bit :)"
    date: 2003-06-06
    body: "I think I liked your older one a bit more. I really hate the wide space after \"font preview: \" so maybe you should leave the older mockup up also.\n\n"
    author: "Mario"
  - subject: "Re: I thought I help out a bit :)"
    date: 2003-06-06
    body: "There are some more places where completely superfluous group boxes are used. Take, for instance, kcontrol/Appearance/Window Decorations. In the \"General\" tab as well as in most \"Configure\" tabs group boxes aren't used to group anything, but as an additional tab description around the whole tab. They are redunant and don't give any relevant information. What does a \"Decoration Settings\" group box want to tell me? I entered the \"Window Decorations/Configure\" tab for a reason, didn't I?"
    author: "Melchior FRANZ"
  - subject: "Erm, usability team?"
    date: 2003-06-06
    body: "So it only takes a public interview to fix a suck clock config dialog, makes you wonder about the rest \"of things\". \n \nNow are \"people\" proud that less then 24 hours after Nat Friedman's interview on OSNews where he pointed out his clock configuration dialog problem, it was fixed? \n \nOr are \"people\" proud that such suck design obviously gets in KDE in the first place and that on top of that it takes a freaking public interview to take care of it? \n \nIts nice that it gets fixed so \"fast\", but lets not pretend that this story is the way it should really work. Just imagine how long it might have taken for this clock config dialog to be fixed if this interview never took place. This makes it seem like people care only if its addressed, where it should be the initial clock config dialog designer getting a clue. \n \nIf somewhere sometime a developer can just cram suck design in somewhere without some usability team overseeing things, what is the freaking effectiveness of this usability team???"
    author: "AC"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-06
    body: "I would point out that 1) The interview brought it to my attention because the interview was relatively well publicized, 2) The dialog was obviously not originally designed by a UI guru, 3) Our usability team was formed well after the creation of many of the dialogs and programs that exist in KDE (including this one) and 4) The fact that it was being fixed so quickly is in fact something to be proud of.\n\nAlso, keep in mind, the bug report in our bugzilla is only two weeks old. I didn't notice it there because it's not my module, and I don't spend all of my free time obsessively searching the bug DB looking for bugs to fix (although I probably should). Even if I had, I probably wouldn't have looked very hard, since the bugreport itself is labelled rather poorly.\n\nI agree, this should never have happened in the first place, but that's why we're still developing everything. It's not \"done\", it's not set in stone, it's not dead. It's alive, and it's constantly sucking less, which is what I usually aim for.\n\n-clee"
    author: "clee"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-06
    body: "5) It needs some experience to create good UI and some KDE developers have just started to program (IMO it's a myth that good developers can't great good UIs).\n6) Nobody really complained before. So it seems as if it wasn't so important for our users.\n\nChristian\n\n"
    author: "cloose"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-06
    body: "wow. someone needs to relax. \n\nin any case, i was already working on the clock dialog before this interview. see: http://urbanlizard.com/~aseigo/clockshot.png\n\nsecond, the usability \"team\" isn't some crack group of coordinated individuals whose life mission is to drop onto KDE like a tiger team and sort out all the problems between now and next wednesday. it's really just a way for people who are improving KDE's usability to confer, discuss and help each other out. and most of those people are doing so as they have spare hours in the day.\n\nas you probably have noticed, KDE has a LOT of interface components. as you may or may not have noticed, it takes a lot of time to get those components right and fully implemented. multiply those two figures and you see how much work there is.\n\nthat said, things are improving. and i'm happy about that. i'd like things to move faster too, but until we get a full-time UI guru or two involved in the KDE project or a bunch of new part-timers who actualy code rather than gripe, things will continue to move at the current pace."
    author: "Aaron J. Seigo"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-06
    body: "AC here ;) (relaxing)\n\nIf KDE remains too dependand on volunteer work, I think its logical not to see usability team members with \"a life mission\". Same goes for developers.\n\nPerhaps I am simply wrong, but it seems almost a taboo to mention that \"if somehow people could get paid for helping KDE\", KDE development would become much more streamlimed, while still open source.\n\nIf at one time millions and millions of users jump on KDE, pure volunteering seems like stupidity to me."
    author: "AC"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-06
    body: "I think you underestimated the \"free-time\" developers.\n\nReasons:\n1. We work on KDE because it's fun --> higher motivation than paid workers.\n2. We do it in our free-time  --> it's our baby so we are willing take more\n    responsibility for our code.\n3. There are a lot more developers working on KDE than most software companies\n    could afford --> many fresh ideas and point of views coming to KDE.\netc...\n\nA paid developer neither ensures good code quality, design nor usable UI. It just increases the amount of time he can work on the code.\n\nJust an opinion of a full-time software developer. :-)\n\nChristian"
    author: "cloose"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-07
    body: "Where is your superior developer or whatever paid by you?\n\nAlso your comments could be more streamlined as well: better spend your time commenting on and voting for reports which bother you at bugs.kde.org."
    author: "Datschge"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-07
    body: "Wow, I really like your clock dialog, the best of all the mockups I have seen so far!"
    author: "tuxo"
  - subject: "Re: Erm, usability team?"
    date: 2003-06-07
    body: "Great.....I like Aaron's much better."
    author: "Nadeem Hasan"
  - subject: "Updates Version 2"
    date: 2003-06-06
    body: "Ok after getting some review I have moved the dialog to something much simpler. http://www.csh.rit.edu/~benjamin/clock1.png here is a screenshot of the new configure dialog.\n "
    author: "Benjamin Meyer"
  - subject: "Re: Updates Version 2"
    date: 2003-06-06
    body: "Nice - that's better than the others people have knocked up."
    author: "GldnBlls"
  - subject: "Re: Updates Version 2"
    date: 2003-06-06
    body: "\nok, to simplify even more, why not remove the font selector?\nWithout being able to select the fonts size IMO the font selector itself should go.\n\nor\n\nreplace it with a \"standart\" fontselector where u can choose size etc aswell"
    author: "yg"
  - subject: "Re: Updates Version 2"
    date: 2003-06-06
    body: "Help, Defaults, Ok and Apply buttons, just for one self explaining dialog??\n"
    author: "nac"
  - subject: "Re: Updates Version 2"
    date: 2003-06-07
    body: "By the way, there's one thing I like with gnome, it's that there's no need for \"Apply\" / \"Ok\". \nAll changes take effect immediately and just \"Close\" exists.\nDoing the same with KDE would be great.\n\nWhen talking about KDE, the clock dialog was also a common example for me. I think the code quality of KDE is higher than of gnome's so is its features.\nBut Gnome wins for usability and look - I never found Keramik \"clean\" for instance. I would love that KDE adjusts its level here, since it's simpler for KDE  to do that for Gnome to make betters APIs and more features ;)"
    author: "Henri"
  - subject: "Re: Updates Version 2"
    date: 2003-06-08
    body: "> All changes take effect immediately and just \"Close\" exists.\n\nThis may not be the best thing in terms of usability.\n\n>  I never found Keramik \"clean\" for instance\n\nTry KeramikII in cvs. It's much less \"bulky\" feel."
    author: "lit"
  - subject: "Re: Updates Version 2"
    date: 2003-06-06
    body: "Font selector should NOT be a drop down box. It is way too limited to be of use. How do I change the size of the font. I am sure KDE will have a standard font dialog in there, so just use a button to bring that dialog up and choose font properly. Also, it makes you have to do less on the dialog itself. No need to implement a separate font size, bold, italicise etc. Check out Eugenia's dialog."
    author: "Maynard"
  - subject: "Re: Updates Version 2"
    date: 2003-06-06
    body: "agreed"
    author: "fault"
  - subject: "Re: Updates Version 2"
    date: 2003-06-07
    body: "This a great but I think there needs to be a preview of what the analogue, digital and other clocks will look like. Once we have that then I think it looks great! "
    author: "David"
  - subject: "/me = impressed!"
    date: 2003-06-06
    body: "Wow, I'm impressed!\n\nNice, not way too large, not lots of useless buttons, not too many options, not too less, it should be an example for all other dialogs in KDE + associated programs. :)\n\nAnd please don't add any more buttons (like the other example), it's good like this (altough the mockup from Eugenia might be better, but this is also pretty good already :)\n\nbtw. The only complaint I've got is the color buttons, they look a bit old (as in 'old from the win 3.11 period') with that bevel.\n"
    author: "nac"
  - subject: "Another draft"
    date: 2003-06-06
    body: "Hi!\nI've created another draft.\nYou can find screenshots and the UI-files on\nhttp://segfaultskde.berlios.de/other/clock-design\n\nTell me what you think about them.\nI've posted this to the kde-devel and kde-usability-lists, too."
    author: "Christian Nitschkowski"
  - subject: "The finalists"
    date: 2003-06-07
    body: "Check out the bug report http://bugs.kde.org/show_bug.cgi?id=58775 its been updated and now lists the top 5 proposals for the new clock design, please tell which your favorite is."
    author: "Alex"
  - subject: "MESS"
    date: 2003-06-07
    body: "The real usability mess is in the ControlCenter\n"
    author: "Gerd"
  - subject: "Re: MESS"
    date: 2003-06-07
    body: "Agreed!\nJust tested knoppix with KDE 3.1 and was mighty impressed with everything but the Control Center. I know the Control Center has been worked on but they seem to just moved things around a bit without rethinking the whole concept. I don't know how to fix it but I can say I had a hard time locating things. Can the layout be edited by just edit ui xml files, If so it would be a good thing to have a competition where coders and non coders could give there input, maybe someone will strike gold."
    author: "Fredrik"
  - subject: "Re: MESS"
    date: 2003-06-07
    body: "Why do you start up the control center when you can find context sensitive control modules in the rmb menu?\n\nComplaining about control center having too many modules is like going into a library and then complaining that you need to find the books you want first, the control center's purpose *is* containing all settings KDE gives. If that isn't what you want don't use it and make use of my hint above."
    author: "Datschge"
  - subject: "Re: MESS"
    date: 2003-06-07
    body: "Well, the comparison of the control center to a library is good. I personally like that everything can be configured in a centralized manner in the control center. However, a library without an index is useless. If books were messed up, you would never find the book you are looking for.\nCurrently,the entry \"Appearance & Themes\" category is a bit messy, people are working on it though. Other things are maybe the naming of some configuration entries, e.g. \"Fonts\" appear both in the \"Appearance & Themes\" and \"Web browser\" category, maybe it would be better to call the \"Web Browser\" entry \"Web page fonts\" instead of just \"Fonts\". Another example is \"Preferences\" in the \"Internet & Network\" category. What \"Preferences\" represent is not immediately clear as in the control center all entries are preferences. "
    author: "tuxo"
  - subject: "Re: MESS"
    date: 2003-06-08
    body: "I personally think that the key in the future will not be an index increasing in size more and more but an improved search facility (currently it already suggest the books but showing the exact pages the hit occured would be better). Of course this shouldn't remove the index, instead someone needs to create a definitive guideline for it which book should be put into what category. Definitive means there should be no more remaining and possible future questions about that issue.\n\nThe naming issue is valid, I'd hope a guideline (as soon as it exists) will be able to resolve it."
    author: "Datschge"
  - subject: "Possibility of formatting the date"
    date: 2003-06-07
    body: "As long as we are discussing improvements here on the dot , here goes :)\n\nI am using KDE 3.1.0, so if any of this has changed since, I apologise.\n\nPlease make it possible to configure the date format from the same dialog.\n\nThe mockups that have been presented here are great improvements except that the date format is still unchangeable. I prefer using Kicker in the 'small' size, which puts the time and date next to each other. The only format for the date is\ndd/mm/yyyy, which takes up too much space on kicker.\n\nCan there please be a few more options for the date format, such as\n7th - day only\n7th June - day and month\n7 Jun - Shorter day and month format\n7/6 - d/m format\n07/06 - dd/mm format\n7/6/03 - d/m/y format\n07/06/03 - dd/mm/yy format and finally\n07/06/2003 - dd/mm/yyyy format for all the people that use Kicker in 'normal' size.\n\nIf I chose the other option, 'Date and Time Format...' I am faced with the Date and Time KControl module which has nothing to do with the clock on Kicker. If I change the date formats in that Kcontrol module, nothing happens to the date format in the clock. \n\nThat leads me to ask, why have that option at all? Surely, if you want to change your locale you would do so from KControl and not from the Kicker clock?\n\nThank you.\n\ndev.null"
    author: "dev.null"
  - subject: "At the risk of being pronounced anathema here..."
    date: 2003-06-07
    body: "Maybe a look at Redhat's clock configuration dialog is in order too. It may no be best and does not offer as many options as KDE will probably want, but it is good UI and damn easy to use. They have split things like time changing to the UI. users can customize the UI, although quite limited, but more than adequate for me. To configure the time and time zones, you will need a password. That seems to make sense with me too."
    author: "Maynard"
  - subject: "Re: Possibility of formatting the date"
    date: 2003-06-11
    body: "Date and time format is centralized in KDE. As it should be. Did you read the warning when you changed the date format? It states that the change will only affect new applications. So if you restart kicker the clock-applet should have the new format. However this is also true for all application you start  afterwards. So you might want to turn of the date from the clock-applet, or use normal size kicker.\n\nAnd it is configurable (from help):\nDate format codes:\nYYYY - The year, using 4 digits.\nYY - The year, using 2 digits.\nMM - The month, using 2 digits (01 to 12).\nmM - The month, using 1 or 2 digits (1 to 12).\nMONTH - The name of the month.\nSHORTMONTH - The abbreviated name of the month.\nDD - The day, using 2 digits (01 to 31).\ndD - The day, using 1 or 2 digits (1 to 31).\nWEEKDAY - The name of the weekday.\nSHORTWEEKDAY - The abbreviated name of the weekday."
    author: "Ingar"
---
Every now and then, someone points out something embarrassing in KDE that just needs to be fixed, and fixed quickly.  So, less than 24 hours after <a href="http://www.osnews.com/story.php?news_id=3705">Nat Friedman's interview</a> on OSNews, the main valid issue he pointed out  -- that KDE's clock configuration dialog was messy and bloated -- was already being fixed. There's even a <a href="http://bugs.kde.org/show_bug.cgi?id=58775">bug in our database</a> filed for it, and if it made enough of an impression on a fairly well-known GNOME hacker that he had to comment on it publically, I figured it deserved fixing.  So moments after the comment, <a href="http://c133.org/files/newclockprefs.png">we began hammering out</a>  the <a href="http://c133.org/kicker-clock-config.ui">UI</a> for a newly revamped Clock Configuration page.  Expect to enjoy the fix in CVS soon!
<!--break-->
