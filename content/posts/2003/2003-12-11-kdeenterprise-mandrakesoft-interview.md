---
title: "KDE::Enterprise: MandrakeSoft Interview"
date:    2003-12-11
authors:
  - "akellett"
slug:    kdeenterprise-mandrakesoft-interview
comments:
  - subject: "In the MandrakeMove page..."
    date: 2003-12-11
    body: "What is the window decoration being used, does anyone know?\n\nI love seeing these KDE::Enterprise types of articles, thanks for rounding them up."
    author: "TomL"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-11
    body: "It's the Galaxy theme, which is also mentioned in the interview. Mandrake has made a similar theme for Gnome"
    author: "Lars"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-11
    body: "Mandrake's Galaxy theme (used in both qt and gtk apps to provide  a uniform L&F in Mandrake)"
    author: "anon"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-11
    body: "You can expect to see quite a bit more in the future.  KDE for the enterprise is really heating up."
    author: "anon"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-11
    body: ">> You can expect to see quite a bit more in the future. KDE for the enterprise is really heating up.\n\nIt sure is! That's EXACTLY why Sun have just deployed 500,000 KDE desktops in China, and are looking at another 800,000 in the UK!  Oh wait a minute, that wasn't KDE....."
    author: "Gil"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-11
    body: "Oh c'mon, no reason for all that negativity. "
    author: "Eike Hein"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "I can't believe that we're all happy to sweep this under the carpet!\n\nGnome have made major enterprise wins this week, and with Suse under the wing of Novell and Ximian, that looks to be the next to disappear. What will be left with?  The support of a bankrupt second-rate Linux distro, and a label as a hobbyist DE?\n\nI have a feeling that in five years time, Gnome will be the only real alternative to KDE."
    author: "Gil"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "I'm not sweeping anything under the carpet.\n\nTrue, Gnome's going well in the enterprise right now, better than KDE. Why? Because Nat and Miguel are doing a good job promoting it. I see KDE.org acknowledging the need for better promotion - look at what Staikos has been doing over the past few weeks (rock on, George!), or the revamped KDE::Enterprise website. So, nothing is being swept under the carpet - on the contrary, KDE is facing the situation and reacting accordingly.\n\nWaving \"We're all doomed!\" and \"The end is nigh!\" signs doesn't help *anybody*. Remember what we got here: Years of successful history, the most solid development framework on the Linux platform, an active development community, hundreds of thousands of users. Hobbyist DE? Not by a long shot. KDE is superior to anything else on the Linux market in so many ways - architecture, administrativa (Kiosk mode, anyone?) et cetera. KDE is still on forefront of things, and lots of efforts are underway to make sure it stays that way.\n\n"
    author: "Eike Hein"
  - subject: "You are right!"
    date: 2003-12-12
    body: "KDE is sooooo very nice and wonderful that I can't see it going away.   It's just way too damn great.   I use it each and every day at work and at home (ok, so they're the same place), and it's just a great pleasure to use.\n\nI'm just so amazed at how much better it gets.   KDE 3.2 is just absolutely amazing.   Konqueror (and everything else) is much faster and more responsive.\nKonqueror also handles more sites.   Kopete is great, pretty much everything is.   It's so easy to use.\n\nAnd I've just started to use quanta - now that's the way to edit html!  And the cervisia integration just blew me away.\n\nMy only down side to kde is that if I had to recommend a DE to use for my company, I'd probably have to go with gnome just because we're a 4 person company and I'm not sure we can pay for Qt.   On the other hand, the productivity of qt is very, very attractive.\n\nGNOME still doesn't have a good file dialog, and they've completely screwed up their dialog buttons and configurability - for me, anyway.  I'm sure that they are right - for the audiences they are going for they've done the right thing.  But for me KDE is head and shoulders above everything else, including the mac.   Whenever I go up and work on my boss' machine, Mac OS X just drives me nuts.  And I can't believe how slow it is.\n\nAnyway, I just don't see the doom and gloom.   Even though I do admit that I wonder how the Novell/SUSE stuff will work out."
    author: "TomL"
  - subject: "Re: You are right!"
    date: 2003-12-12
    body: "Pay for Qt? Do you do development work?"
    author: "David"
  - subject: "Re: You are right!"
    date: 2003-12-12
    body: "Yes, but right now neither Qt nor gtk is a choice for us.  We do java development on linux with deployment on windows and solaris clients and a linux server."
    author: "TomL"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "Yeah, it doesn't help at all to be negative. There was a time a few years ago when Gnome was a worthless piece of bug infested crap (the 1.0 release, anyone?), and had the Gnome folks had the same attitude the whole project would have died. But they worked on and believed in themselves and now Gnome 2.x is less buggy. KDE folks should just keep on doing what they have been doing these past years: use the desktop they love and develop stuff that is fun. It's as simple as that. Being negative gives you nothing at all...\n\nI just hope there was anything I could do wrt KDE development. Everything I'd like to have is mostly already done. :) Fixing bugs deep down somewhere is far too hard for a newbie like me...\n"
    author: "Jan Ekholm"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "You should try installing KDE 3.2 and make good bug reports and clarifying those that are already reported. For instance if Konqueror is not rendering a page correctly, open up its HTML in an editor, one instance in Mozilla and one in Konqueror and try to make it as little HTML as possible while still having the misrendering. For several months theres been a bug report about Megatokyo.com misrendering. But no had posted what HTML code was actually doing it until I did a few days ago. It looks like Konqueror doesn't handle <center> quite correctly. Stuff like that."
    author: "Ian Monroe"
  - subject: "Re: VHS"
    date: 2003-12-12
    body: "It totally agree with you, but I also know that being the best doesn't always mean you'll win! Remember why we where stuck with the inferior VHS video for years? The masses are begging for a standard. I'm afraid that if Gnome does build up momentum then KDE will indeed move to a Hobbyist De. They do have the backing of some real big companies. KDE somehow just hasn't enough backing. I would love to see an IBM or alike to promote KDE. But at the moment it's not looking good at all."
    author: "VHS"
  - subject: "Re: VHS"
    date: 2003-12-12
    body: "VHS won because Betamax didn't provide what people wanted. When VHS came, there was no Betamax tape long enough to hold an entire film (for home use, I mean) - but that's what people wanted to do, record an entire movie without switching tapes. So while Betamax had the superior technology, VHS was the better *product*. VHS tapes were also cheaper and offered a bigger choice of hardware at lower cost. Betamax never caught up again.\n\nOf course, that teaches a lesson as well. "
    author: "Eike Hein"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-13
    body: "they are not bankrupt (anymore) OR second rate, but, they are probably the ONLY commercial distro left that releases ALL thier tools OSS and provides ISO's for ALL thier releases. unlike some so called \"major\" commercial distros.\n\njust because you don't like them doesn't mean that they are second rate.\n\n\nblah........why do I even bother. "
    author: "LD"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "> are looking at another 800,000 in the UK!\n \nlink? i haven't heard of that before..\n"
    author: "dood"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-13
    body: "Here you go\n\nhttp://observer.guardian.co.uk/business/story/0,6903,1101344,00.html"
    author: "Leonscape"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "\nWe use Gnome at work.  It seemed okay(a really old version), but when we upgraded we feel we lost functionality.  I mean the fonts are nicer, but it's just so annoying.  I tried using Gnome at home, but so many little things are impossible.  I like my TV window to be \"Always on Top\" but they took it out of gnome.  I seen bug threads of people wanting this feature, but the gnome developers don't listen to their own users and say \"We don't want to clutter the menu\" yet put all the \"Move to desktop\" menu items all in the root menu, instead of a sub-menu.\n\nI'm not saying Gnome sucks, but if the users want a feature, by all means give it to them....not a bunch of excuses.  You know what?  My boss agrees, and our company may be going to KDE soon."
    author: "Twig and 2 berries"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "> 500,000 KDE desktops in China\n\nno, they just agreed on a deal where China will start using their desktop in the future. the license agreement doesn't even start until the end of the year. interestingly, they say they will deploy .5 to 1 million desktops a year, and aim for 200 million in total. cool, only 200-400 years. ;-) \n\nah, but there's a clue here that may sort out the math yet: almost all of the articles made this exact quote: \"China plans to install at least 200 million copies of an open-standards-based desktop\". note that it doesn't say \"of Sun Java desktop\" but of \"an open-standards based desktop\". so really, the 200 million number probably includes Sun's JDE and a lot of other systems as well. e.g. Red Flag Linux which happens to default to KDE.\n\n> looking at another 800,000 in the UK\n\nyep, this is even more vaporous than the China deal.\n\nnow, i'm not saying that Sun won't end up fulfilling on these contracts. what i am saying is that it hasn't happened yet, and that they are playing with fire. preannouncing massive desktop deals is a sure fire way to get Microsoft flying in with fancy discounts. it's also a great way to get egg on your face when a year from now there aren't 500k in China and 800k in Britain and a reporter pulls up these (now one year old) press releases that Sun has been peppering the media with.\n\nso Sun's really playing a somewhat dangerous hand and gambling on the outcome. but Sun really doesn't have much of a choice, given their financial and strategic positions. but maybe it will work and it'll turn out that their desparate situation is exactly what has been needed to get them out of complacent mode and hungry enough again to actually do some new market penetration. or maybe they're just desparate, period. time will tell.\n\noh, and if they were using KDE, i'd be rather annoyed that they were calling it the Java Desktop Environment and showcasing Java replacements for crucial bits of the environment like the window manager and panel.\n\nbut let's assume that Sun DOES get all these contracts fulfilled and gets back on the money train. in that scenario, by the end of next year they've got a million or two desktop systems running JDE. that means they'll have added .5% to 1% of the market to Linux' desktop share, leaving another 90%+ for the rest of the Linux plays, while showing that Linux can indeed make your company profitable. in other words, if successful, Sun will lend desktop Linux further legitimacy while still operating in a wide-open market. 90%+ leaves a lot of room for KDE to do its thing, too. so Sun posting some real wins would be good news for everyone, with the exception of Microsoft.\n\nso no matter which way it goes for Sun and their \"Java\" DE, it's all good for KDE. you've misconstruing Sun's potential future success as an indicator of KDE not moving into the corporate and enterprise desktop markets, when really it's no such thing."
    author: "Aaron J. Seigo"
  - subject: "Re: In the MandrakeMove page..."
    date: 2003-12-12
    body: "Er, no. These are Linux based desktops using Gnome as the base desktop, possibly  with some Ximian enhancements as I understand it. It is basically a rip off of Suse and the tools packaged in a Gnome/Java format. In a word, awful. It is basically a vehicle to get Java on the desktop."
    author: "David"
  - subject: "What is so special about this?"
    date: 2003-12-11
    body: "Can anyone tell me what is so special on MandrakeMove? I can't see any big difference to Knoppix. I can also carry my personal data and the configuration on an usb-stick using Knoppix.\n\nJust wondering.\n  Sven"
    author: "Sven Bergner"
  - subject: "Re: What is so special about this?"
    date: 2003-12-11
    body: "Can anyone tell me what is so special on Knoppix? I can't see any big difference to MandrakeMove. I can also carry my personal data and the configuration on an usb-stick using MandrakeMove.\n \nJust wondering\n;)"
    author: "ac"
  - subject: "Re: What is so special about this?"
    date: 2003-12-11
    body: ":-D"
    author: "OI"
  - subject: "Re: What is so special about this?"
    date: 2003-12-11
    body: "I think the difference is in the focus on the product - konppix aims to be a \"demo disc\" which has no persistent state, Wheras MandrakeMove aims to be a \"portable desktop\" which keeps your files & settings wherever you go in a transparent manner. (wether it achieves this or not i dont know..)\n\n-J"
    author: "ac"
  - subject: "Re: What is so special about this?"
    date: 2003-12-11
    body: "Yes, that last part is what differentiates it from Knoppix."
    author: "anon"
  - subject: "Re: What is so special about this?"
    date: 2003-12-11
    body: "> Can anyone tell me what is so special on MandrakeMove? \n\nIn addition to new features it brings (look at the spec page), it's... Mandrake! And for many people, it means much :)"
    author: "John Steve"
  - subject: "A question arises"
    date: 2003-12-11
    body: "Now that kde is going tho have a native scripting languaje, is there any posibility that mandrake ports its configuration utilities to kde?\n\nThat just would kick ass\n\n"
    author: "Mario"
  - subject: "Re: A question arises"
    date: 2003-12-11
    body: "Having all Mandrake tools ported to KDE would help a lot. I always experience problem when I lack mose. It seems gtk has strange behaviour regarding moving from widget to widget vith kbd."
    author: "Olivier LAHAYE"
  - subject: "Re: A question arises"
    date: 2003-12-12
    body: "You would have to ask Mandrake, I guess."
    author: "Stephen Douglas"
  - subject: "French Translation"
    date: 2003-12-12
    body: "A french translation of this interview can be found here:\n<a href=\"http://annmadocs.sourceforge.net/french/traductions/duval.html\">\nhttp://annmadocs.sourceforge.net/french/traductions/duval.html</a>\n<p>\nI saw MandrakeMove used by a new Linux user who is switching from Windows and he was very impressed by it and by KDE. It's easy to install and KDE applications are easy to use. The difference with Knoppix is that MandrakeMove is aimed to be used, it's not just a demo and the USB key stores all your settings.<p>\nI also note with satisfaction that MandrakeMove mentions Educational applications.\n"
    author: "annma"
  - subject: "Re: French Translation"
    date: 2004-10-19
    body: "C'est une bonne traduction... Merci!!!\nAline http://www.all-translations.com"
    author: "Aline"
  - subject: "PR"
    date: 2003-12-12
    body: "I think one of the best pages on the internet to promote kde is the docs.kde.org page containing the manuals of all the programs. It is not too well maintained however, and I think that is terrible. The accessibility programs have not been added for example. \n\nErik"
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Re: PR"
    date: 2003-12-13
    body: "The accessibility programs are available under the KDE From CVS link, since they haven't been part of a KDE release yet.  When KDE 3.2 is the current stable release, they will show up on the front page too.  As for maintenance, there's not a whole lot to do with a fully automated site (other than adding the other languages, which I know is a concern of yours, and which I think will be happening very soon).\n\nWhat we really could use, is more people writing documentation.  There's quite a lot of work to be done, and hardly anyone doing it, and it would be extremely welcome.\n\nIf you can write more or less correct English, can use a text editor and have access to a recent (since Beta 1) CVS build or beta of KDE 3.2, KDE needs you.   If you only have KDE 3.1.4, then we can still find you things to help out with in the documents. \n\nHow to join up? Well, first check out the unfinished docs list (currently at http://people.fruitsalad.org/lauri/docs/ ) - writing is hard work, we don't want to have to turn down someones hard work because someone else did it first.  Take a look at the existing manuals, to get a feel for what you're embarking on - I recommend Konqueror, Kmail, or Knode, as probably the pick of the bunch, what we'd like all the rest to be like.  Also, take a look at the resources on http://i18n.kde.org/doc/ especially the Writers Questionnaire which is a good list of the kind of things you will be writing about.\n\nPick something that isn't maintained to work on, or come up with something that may not be on the list (there's the extragear apps for instance, or KOffice) then either write to me, or introduce yourself on the kde-doc-english mailing list, and the docs team will do our level best to make you both welcome and help get you started writing.\n\nThings you don't need to do: \nYou don't need to be an expert on the application you're going to write about.\nYou don't need to know (or learn) DocBook XML, in fact it's probably quicker initially if you don't, you can pick it up later if you want.\nYou don't need to be a professional or experienced writer, and,\nYou don't have to take on an entire manual.  A chapter here, an update to a chapter there, that's a big help too."
    author: "Lauri Watts"
---
It is only a few days after the 
<a href="http://enterprise.kde.org/">KDE::Enterprise</a> website has been
<a href="http://dot.kde.org/1070917908/">freshened up</a> and already the content is flooding in! In this
<a href="http://enterprise.kde.org/interviews/mandrake/">interview</a> with <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>'s Co-founder Gaël Duval we hear of one of their latest products, <a href="http://www.mandrakesoft.com/products/mandrakemove">MandrakeMove</a>, and the reasons behind MandrakeSoft's choice of KDE as its principle graphical interface.

<!--break-->
