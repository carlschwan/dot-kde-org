---
title: "KDE Conquers the Vectors with KSVG"
date:    2003-09-16
authors:
  - "astreichardt"
slug:    kde-conquers-vectors-ksvg
comments:
  - subject: "Eolas"
    date: 2003-09-15
    body: "Hope it doesn't infringe the EOLAS patent, not eolas also owns the trademark \"invented here\".\n\n521 Mio US $ huh!\n\n:-)"
    author: "Wurzelgeist"
  - subject: "Re: Eolas"
    date: 2003-09-15
    body: "Note more than other embedded content and maybe less, since the Eolas patent is about processes started by embedded content and KSVG runs in-process.\n"
    author: "Tim Jansen"
  - subject: "Eolas patents security breaches"
    date: 2003-09-17
    body: "... viruses, MS scripting, etc. etc.\n\nGreat patent ... glad someone admits to having invented this stuff"
    author: "OSX_KDE_Interpollination"
  - subject: "kde 3.2 really does rock!"
    date: 2003-09-15
    body: "hi,\n\ndid an emerge of kde-cvs today, its really such a great thing! \nthere are plenty of usability-enhancements which will get the masses on the desktop onto kde!\n\nthanks and regards to the kde-team,\ntom"
    author: "tom"
  - subject: "Re: kde 3.2 really does rock!"
    date: 2003-09-16
    body: "How did you do this?  I'd like to emerge it myself, though it will probably take a couple of days for me on my 500mhz celeron.\n\nPaul....."
    author: "cirehawk"
  - subject: "Re: kde 3.2 really does rock!"
    date: 2003-09-16
    body: "http://dev.gentoo.org/~caleb/kde-cvs.html\n\nPlease read the whole manual.\n\nWorks well. If it doesn't compile, wait a day and try again. Do each module separately rather than the kde one, otherwise it quits at the first error.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: kde 3.2 really does rock!"
    date: 2003-09-16
    body: "Also see http://forums.gentoo.org/viewtopic.php?t=69335&postdays=0&postorder=asc&start=0"
    author: "anon"
  - subject: "Re: kde 3.2 really does rock!"
    date: 2003-09-16
    body: "Thanks Derek, I'll read the manual and give it a shot.\n\nPaul....."
    author: "Cirehawk"
  - subject: "Re: kde 3.2 really does rock!"
    date: 2003-09-18
    body: "These two scripts help with the whole \"wait a day\" syndrome. They are released under the GPL, if you edit it, release it.. enjoy!\n\nhttp://davidsmind.com/davidsmind/ebuild-kdebase\nhttp://davidsmind.com/davidsmind/ebuild-kdeapps\n"
    author: "davids mattatall"
  - subject: "Re: kde 3.2 really does rock!"
    date: 2003-09-16
    body: "I just emerged a fresh copy of KDE CVS. It rocks! The default Konq menu is down to 14 entries, and contains \"Copy Text\" :) Konq is also really fast, though the speed seems to be fluctuating a bit with CVS releases. Kontact looks a bit nicer too. KDE 3.2 is going to rock!"
    author: "Rayiner Hashem"
  - subject: "fonts?"
    date: 2003-09-15
    body: "would it make sense to create fonts in svg format? they'd be smoothly scalable and easy to read on any platform.\n\nI'm really no font expert (as you probably already know because of the question), but I think the current font-situation in linux sucks a little. So, is this a very stupid idea?"
    author: "me"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "I've always thought that a font format allowing advanced vector capabilities like color and shading would be nice.  Especially something where you can define generic and relative colors and alter according to a base setting."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "Not really. Fonts need extensive hinting information that isn't present in SVG. It won't be practical to put pure vector fonts in anything until we get at least 300 dpi screens, if ever. Although, the font situation in Linux is quite good. Between Qt 3.x and Fontconfig, installing fonts is reduced to just copy some fonts to /usr/share/fonts or $HOME/.fonts. Font rendering is also really high-quality, as long as you've got some good Postscript or well-hinted TrueType fonts. "
    author: "Rayiner Hashem"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "Hinting informations in a font is patented by Microsoft.\n\nWe have to find another way to render fonts.\n\nActually, while there is no alternative technology, M$ and Adobe can decide at any moment to forbit any free software to display or print any text..."
    author: "aegir"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: ">>  Hinting informations in a font is patented by Apple.\n\nRight.\n"
    author: "blegh!"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "> Hinting informations in a font is patented by Microsoft.\n\nYou mean, hinting information in a TrueType font is patented by Apple, don't you?"
    author: "anon"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "Yes by Apple, sorry.\n\nBut I do not mean \"hints in a TrueType font\" but \"hints in any font format\".\n\nAFAIK..."
    author: "aegir"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "Hinting in type1/type3 fonts is not patented. Hinting TT fonts is also not patented, but hinting with bytecoded information in TT fonts is patented by Apple. "
    author: "anon"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "AFAIK, the patent is not limited to TT fonts, but any fonts :\n\nUS Patent 5159668 :\n\nWhat is claimed is: \n\n1. A method for manipulating the outline of a symbol image at a plurality of output sizes for improving the display of a digital typeface on raster output device having an output resolution and having an array of pixels, comprising the steps of: \n\nstoring in a first memory means a plurality of control points corresponding to an outline of said symbol image, at least one of said control points having predetermined information specifying different positions of said at least one of said control points for at least two of said plurality of output sizes; \n\nselecting at least one of said control points of said outline which requires manipulation; \n\nselecting a first size from said plurality of output sizes to display said outline at said first size on said raster output device; \n\ncalculating a distance and direction for repositioning said selected control point at said first size and said output resolution; \n\nmanipulating said outline by using the distance and direction calculated for said selected control point to reposition said selected control point; \n\nand storing the results of said outline manipulation in a second memory means. \n\n(...)\n\n"
    author: "aegir"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "Nope. There is one way to hint fonts that is patented by Apple. Freetype2 uses another way (auto-hinting). Go check their website:\n\nhttp://www.freetype.org/patents.html"
    author: "LaNcom"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "I don't see how. Hinting was in Postscript Type1 fonts long before Apple and Microsoft came up with TrueType. Between the PSHinter and the Autohinter in Freetype 2.1.5, I have no need for the patented TT hinter anymore :)"
    author: "Rayiner Hashem"
  - subject: "Re: fonts?"
    date: 2003-09-16
    body: "It would only seem to make sense when either the text is pretty big and/or the text needs much decoration, like a gradient or a filter effect like dropshadow. OTOH the svg fonts specification is still a work in progress AFAIK, so who knows :)\nBTW the infrastructure is there in ksvg to implement svg fonts, its just waiting for someone to implement it :-)\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Nice work, Andreas ;-)"
    date: 2003-09-15
    body: "Thanks for the time you spent KSVG ;-)"
    author: "Michael Rother"
  - subject: "Any use as a general-purpose canvas?"
    date: 2003-09-15
    body: "Stupid questions!\n\nI've heard of various rendering subsystems on different platforms using particular 'languages', like Display Postscript on some old UNIXes and Display PDF on MacOS X. Would such a thing be possible with SVG?\n\nSome standardised, ultra-high-quality renderer that can easily be redirected to printing would be brilliant. It could be used by word-processors, DTP packages, vector graphics packages etc for proper WYSIWY-really-G, not the rough approximation that often seems to be the case on Unix software.\n\nIs KSVG up to the task? The output screenshots remind me of Artworks on the Acorn - in other words, lovely. :-)"
    author: "Adam Foster"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-15
    body: "xsvg.org is working on something like this. I can't judge whether it is a good idea to move SVG rendering on the server. The advantage would be that the X11 server could render directly without any client communication, the disadvantage would be that the X11 server may need to expose the SVG tree to the client and manipulations of that tree would be slower and possibly more complicated."
    author: "Tim Jansen"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-16
    body: "> I've heard of various rendering subsystems on different platforms using\n> particular 'languages', like Display Postscript on some old UNIXes and \n> Display PDF on MacOS X. Would such a thing be possible with SVG?\n\nAs I understand this, Cairo combines all of this:\n\nhttp://cairographics.org/xr_ols2003/\n\n> Some standardised, ultra-high-quality renderer that can easily be redirected \n> to printing would be brilliant. It could be used by word-processors, DTP \n> packages, vector graphics packages etc for proper WYSIWY-really-G, not the \n> rough approximation that often seems to be the case on Unix software.\n\nIf you are talking about Qt, then you are correct, it doesn't work.  The reason is that it is designed wrong -- backwards.  What you are always going to have (if correctly designed) is that WYS is a lower resolution approximation of WYG on the printer.  \n\nThere is NO solution to this problem except a 1200 DPI display.\n\nIf you want a better approximation of WYG, you need to turn font hinting OFF which makes the display look worse.\n\n--\nJRT  \n\n "
    author: "James Richard Tyrer"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-16
    body: "> As I understand this, Cairo combines all of this:\n> http://cairographics.org/xr_ols2003/\n\nNo, Cairo is a renderer that offers drawing commands. It works on a lower layer and does not store a vector model (that's why you can use it for both PDF and SVG - their rendering models are quite similar)."
    author: "Tim Jansen"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-16
    body: "I hate to tell you this, but PostScript uses vector model.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-16
    body: "I hate to tell you this, but I never claimed something else :)\n"
    author: "Tim Jansen"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-19
    body: "Yes, Postscript uses a vector model. BUT, once you have drawn something, you cannot change it.  For example\n\n100 100 moveto 200 200 lineto stroke\n\nwill draw a line.  You cannot give this line a name and change its coordinates later, as with SVG objects.  Once the stoke command is given, the line if final.  If, on the other hand, you just give the PS interpreter\n\n100 100 moveto 200 200 lineto\n\nthen nothing at all will be drawn, and a newpath command will cause the PS interpreter to forget it entirely.\n\nThat is what is meant when the previous poster said that the vector model is not stored. (It isn't: only the display bitmap is stored, unless you have a DSC PS file and a device that handles the DSC stuff, but that isn't strictly part of the PS interpreter, and is certainly nothing to do with the PS display model.)\n"
    author: "John Allsup"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-19
    body: "Thank you for explaining what I and/or the previous poster didn't understand.\n\nHowever, what you say doesn't quite make sense to me.\n\nI think that what you are doing is confusing the lack of a WYSIWYG PostScript editor program with the language used in a PostScript file.  If such a program existed, it would clearly have some of the same capabilities as Sketch or Sodipodi, and it would output a PostScript file, not a bit map.\n\nOTOH (IIUC), SVG stores more information than just the strokes and fills -- it stores information that several of these primative objects go together to make a more complex object, this is an advantage of SVG over PostScript for this use.  IIUC, this is what is meant by the vector model.  PostScript would need to use comments to do this and this would not be very elegant.\n\nHowever, Cairo *does* appears to be an attempt to integrate PS, PDF, and screen rendering.  I see nothing specific about a native file format for it.  But I would assume that it will be XML with embedded SVG.  In that case, it would be able to export to PS and PDF but not use them as input, or if useable as input then an export and reinport would result in the loss of information.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Any use as a general-purpose canvas?"
    date: 2003-09-19
    body: ">>Cairo *does* appears to be an attempt to integrate PS, PDF, and screen rendering. I see nothing specific about a native file format for it. But I would assume that it will be XML with embedded SVG. In that case, it would be able to export to PS and PDF but not use them as input, or if useable as input then an export and reinport would result in the loss of information.<<\n \nCairo is just a renderer that happens to have a X11 extension and is integrated in the X11 server. You call a few C methods, and it will paint something. Usually it will paint on the screen, but with the right driver it may print to PS or PDF stream.\n\nIt can not load or save data, and it does not know anything about XML. It does not know anything about PS or PDF either.\n\nPDF rendering model refers to the type of commands that it processes. There are different ways to describe vector graphics, and Cairo happens to go the Adobe way.\n"
    author: "Tim Jansen"
  - subject: "This is unrelated (sorry0"
    date: 2003-09-16
    body: "But, what do you think about spatial uis like the one in GNOME 2.6?\n\nMore on that here: \n\nhttp://arstechnica.com/paedia/f/finder/finder-1.html\nhttp://lists.gnome.org/archives/desktop-devel-list/2003-September/msg00446.html\n\nPersonally, I don't really liek it, but it seems interesting.\n\nKSVG alsos eems great, especially since now we can have SVG icons and an easy to sue Kpart for it!"
    author: "mario"
  - subject: "Ok now I read everything"
    date: 2003-09-16
    body: "I read the preview page and SVG is awesome, I can't believe it has so many uses , even animations! WoW! That is great news!"
    author: "mario"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "> But, what do you think about spatial uis like the one in GNOME 2.6?\n\nIt's been in there in Konqueror since KDE 2.0, just not by default (and will never be.. usability of spatial interfaces is better than browser interfaces, but usability of spatial interfaces for people who are used to browser interfaces (99% of computer users), is not good)\n\nSo, basically, do you want to make your interface easy for new computer users or classic MacOS or BeOS users (GNOME), or for existing Windows/MacOSX/KDE/GNOME users (KDE)\n\nNot sure which!"
    author: "anon"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "So how do you enable it in konqueror?\n"
    author: "Sleepless"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "Settings -> Configure Konqueror -> Open Directories in separate windows\n\nAnd then right click on a toolbar, Toolbars submenu and uncheck them all.\n\nVoila.\n\nIt's not a single option thing, but it is very quick ( < 1 minute)."
    author: "Dawnrider"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "Damn, if that's it, thanks but no thanks :)"
    author: "Sleepless"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "No, thats not all. Please read the artikel. To disable the toobar and open directories in new windows works in Nautilus too."
    author: "MaX"
  - subject: "that is NOT all it is"
    date: 2003-09-16
    body: "its MUCH more involved and much better than that hack, which nautilus has had since 1.0 too.  read the article before you spout off please."
    author: "dude"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "Eh. I'm not impressed. Its the GNOME bowing down to the Apple guys again... There is a good post on the gnomedesktop thread about this article. He points out that these days computers have become so pervasive that people have adopted to navigation methods that are most efficient on a computer, rather than what maps well to the real world. Thus, the spatial metaphor is less important today."
    author: "Rayiner Hashem"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "Yes, especially since the Apple itself somewhat abandoned spatial interfaces in OSX."
    author: "anon"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "Question... Neither the article nor the list discussion makes it obvious what we are talking about... Is this, essentially an argument about \"Open in a new window\"? \n\nIf so, I'm a bit underwhelmed. As far as I can see, most users can use either method quite happily, and really don't care very much.\n\nBest usability improvements of the last five years for file management are: image thumbnailing and previews, tabbed file management in Konqy."
    author: "Dawnrider"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "Yes, the short story is that every directory/folder is opened in a new window, and that these windows represent the folder. In other words, they do not have navigation buttons(forward/back) or a location bar, because the directory of the window is fixed.\n"
    author: "Jeff Johnson"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "To be honest, from having used this method of file management under countless older operating systems, then Windows 95, every time I installed win 98 I would just take five minutes to tweak it to do the same thing. Tweaking KDE to do the same is indeed trivial. Just get it to open new windows and remove the toolbars.\n\nThen one day I got tired of doing the changes when I reinstalled each machine at work and left it. Since then, I've been fine with things in a browsing method.\n\nReally, it makes no difference in the way I work, and my range of users, from people who know almost nothing about computers to the web/e-mail/wordprocessing users to the presentation/photoshop/dreamweaver lot have all been fine. As far as I can see, the users really don't care; in fact, they don't even seem to have noticed.\n\nThe only issues I have with browser navigation, is that Windows makes it very hard to open a folder in a new window when you need to, so that you can copy using DnD. Konqy doesn't have that problem, and the tabbed FM is great :)"
    author: "Dawnrider"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "Windows has not implementet it, please test it on MacOS 9 or older!"
    author: "MaX"
  - subject: "Re: This is unrelated (sorry0"
    date: 2003-09-16
    body: "It's funny that all these articles/posts all start with something like \"Personally I think that OO UIs suck, but as the usability guys say that they are logical and easy to use...\".\n\nP.S.: Knowing the 'a new window for each directory' principle from Windows, I agree that it sucks. \n\n"
    author: "Jeff Johnson"
  - subject: "aparently you've never used it"
    date: 2003-09-16
    body: "because windows, gnome or kde have NEVER had a spatial GUI. dont judge it before you see it, lest you look like a troll."
    author: "dude"
  - subject: "Re: aparently you've never used it"
    date: 2003-09-16
    body: "Not true; The classic MacOS finder was quite spatial. It went well with Apple's \"pretend your computer is a desk\" mantra. \n\nThe OSX finder is not so-spatial for very good reasons.\n\n1. People are used to non-spatial interfaces. Because of hrrm... Microsoft, NeXT, etc...\n\n2. Spatial interfaces tend to clog the whole screen full of windows. This was why, for example, why Netscape didn't originally open up a new window for every page opened and why it had back/forward navigation buttons. Microsoft and others found that this approach worked equally well for local files.\n\nAs others have pointed, out, it's already possible to have a psuedo-spatial Konqueror. Just turn all of your toolbars off and set things to open in new windows. "
    author: "fault"
  - subject: "Re: aparently you've never used it"
    date: 2003-09-17
    body: "I defintely wouldn't wat mozilla to have a web browser with a spacial interface, damn taht would be a pain, I think it sucks too, but I guess I need to try a real implementation first."
    author: "mario"
  - subject: "Sue Kparts?"
    date: 2003-09-17
    body: "\nWho would want to sue KParts?  I mean, really."
    author: "Sandy C. Otmus"
  - subject: "Mario, a GNOME fan"
    date: 2003-09-17
    body: "Did you post some comments in GNOME sites too? In various of the Brazillian and WORLD sites about KDE I can see you posting notes about GNOME. I like GNOME too but, what\u00b4s your intension?\n\n\"I invite the KDE users to try GNOME...\"\n\"This GNOME feature is cool...\"\n\nHey man, please continue to promote GNOME but try to have a bit of ethic..."
    author: "Paulo Junqueira"
  - subject: "i never posted anything ona  brazilian site"
    date: 2003-09-18
    body: "I'm a fan fo both desktops and I want KDE to improve and I list notable GNOME features which I would like in KDE on the dot sometimes, so what.\n\nI refer you to this thread which explains everything to a person just like you. \nhttp://dot.kde.org/1063222993/1063247522/"
    author: "mario"
  - subject: "Can it display rotated (non-animated) text?"
    date: 2003-09-16
    body: "One of the biggest shortcomings of HTML is the lack of rotated text.\n\nIs the KSVG-implementation in KDE 3.2 able to display a simple text which is rotated for example by 90\u00b0?\n\nThanks"
    author: "Roland"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2003-09-16
    body: "Hi,\n\nYes KSVG can both display simple text at any rotation/scale as well as display\ntop-to-bottom scripts. Try it if you dont believe me :)\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2003-09-16
    body: "The lack of rotated text is NOT a shortcoming of HTML.  It's at best a lack of CSS.  Sorry, it had to be said :)\n"
    author: "Haakon Nilsen"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2003-09-17
    body: "I think it will be in CSS 3."
    author: "Niels"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2003-09-30
    body: "You should be able to adress this with CSS3 when support for CSS3 becomes widespread(read- when hell freezes over, when ipv6 support is in all OS's). SVG, or PNG images are probably a better idea for now. SVG support is not all that great either, so probably images are a better solution for now. The writing-mode property in the text module will let you you get 90 degree rotated text, but its a nasty hack seeing as that property is meant to help display mongolian..."
    author: "jeremyn"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2004-01-28
    body: "yes it can be done\n\n<p style=\"DIRECTION: ltr; WRITING-MODE: tb-rl\">rotated text</p>\n<div style=\"DIRECTION: ltr; WRITING-MODE: tb-rl\">rotated text</div>"
    author: "Basti Bla"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2005-10-30
    body: "Very old thread, but since I found this looking for an answer to the same question, I suppose I can revive it.\n\nwriting-mode is not part of any CSS standard. It is part of the XSL recommendation (http://www.w3.org/TR/xsl/slice7.html#writing-mode-related), though. So I guess it is a bad idea to rely on it for now.\n\nwriting-mode is not implemented in Mozilla browsers (Mozilla, Firefox, Netscape) as of Firefox 1.7."
    author: "Gecko"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2005-12-09
    body: "Did you check the CSS3 text module?\n\nhttp://www.w3.org/TR/2003/CR-css3-text-20030514/#writing-mode\n"
    author: "Raphael"
  - subject: "Re: Can it display rotated (non-animated) text?"
    date: 2004-09-21
    body: "But what about the rotation in the other direction I Want read it from the bottom to the top direction ... is that possible ??"
    author: "AHmed Kamal "
  - subject: "Could this see the vectorization of kde?"
    date: 2003-09-16
    body: "With this engine, we don't need to settle for arbitrary icons anymore.  Features like large/stretchable icons, icons coloring themselves depeinding on the color scheme. It would be a lot easier to do these things when the icons are vectors rather than prerendred png. KDE is a long way behind gnome when it comes to SVG graphics, and I'm glad KDE is starting to catchup with KDE."
    author: "norman"
  - subject: "Re: Could this see the vectorization of kde?"
    date: 2003-09-16
    body: "\"I'm glad KDE is starting to catchup with KDE\"\nI'm glad about that as well. ;)"
    author: "Datschge (has troubles with his i-net connection at home =( "
  - subject: "Re: Could this see the vectorization of kde?"
    date: 2003-09-16
    body: "yeah, it's about freaking time we caught up to those guys.  I mean KDE has been SO far ahead for SO long... it's good to see some competition like KDE rock the boat a little.  when desktops compete, it makes both of them better. ;p"
    author: "standsolid"
  - subject: "ROFL!!!"
    date: 2003-09-17
    body: "That's a funny ;)\n\nAnyway, I don't think GNOME was ever ahead of KDE in SVG, it's jsut that they have had a shroter release cycle and so their work is already out while KDE 3.2 will be worked on for at least a year I hope. GNOME only has SVG icon rendering as does KDE 3.1. This implementation is much more than jsut icons.\n"
    author: "mario"
  - subject: "Re: ROFL!!!"
    date: 2003-09-17
    body: "No, the themes (GTK) can also be SVG. And Metacity I think."
    author: "Maynard"
  - subject: "Re: ROFL!!!"
    date: 2003-09-17
    body: "No, I think, that there's an experimental vector-theme implementation available for gtk+, though it does not seem to be based on svg. It was made to test out the functionality of cairo (related to the XFree Project), which aims to incorporate vector based functionality into XFree."
    author: "Thomas"
  - subject: "Re: ROFL!!!"
    date: 2003-09-17
    body: "No. There is a whole suite of themes. Gorilla, Nuvola, Lush, Wasp and Crystal. go to librsvg.sourceforge.net to check them out. "
    author: "Maynard"
  - subject: "Re: ROFL!!!"
    date: 2003-09-17
    body: "o.k... svg icons are not the most interesting part, we'll have crystal svg cions sooner or later plus any free svg icon themes from other desktops...(me thinks)... \n\nI was speaking of a widget style made of vector gfx... well _that's_ cool..."
    author: "Thomas"
  - subject: "SVG in games?"
    date: 2003-09-16
    body: "I wonder if simple games such as turnbased strategy games and board games could benefit from using SVG for the graphics? The whole idea is in my opinion not that far fetched. \n"
    author: "Chakie"
  - subject: "Re: SVG in games?"
    date: 2003-09-16
    body: "Not really. They could use it, of course, but SVG itself wouldnt such a large benefit over any other cnavas with a similar renderer. The largest advantage would probably be that that your cnavas is based on a common file format."
    author: "Jeff Johnson"
  - subject: "Re: SVG in games?"
    date: 2003-10-08
    body: "Which is exactly why Atlantik is adopting KSVG. \n"
    author: "Rob Kaper"
  - subject: "Will KDE 3.2 have SVG icons?"
    date: 2003-09-16
    body: "Will svg icons be the default in KDE 3.2 or will we have to wait for another release?\n\nSD"
    author: "Simon"
  - subject: "Re: Will KDE 3.2 have SVG icons?"
    date: 2003-09-16
    body: "i want Auto-recreate icons at anysize, because of speed\n\nAuto recreate SVG ICON as PNG with the new selected size.\n\nchris"
    author: "chris"
  - subject: "Widget styles in SVG"
    date: 2003-09-16
    body: "Could we see Qt widget styles in SVG? It's already possible in gtk. If someone were to make a widget style engine for Qt, we could have cross desktop themeing ! :)"
    author: "anon"
  - subject: "Re: Widget styles in SVG"
    date: 2003-09-16
    body: "I've always tought about this too.\nThis would be great. You could more easily create a theme (no need for hardcoded, you can use SVG+EMCAScript) and themes could be cross-plataform! \n\nWow makes me dizzy! Would be really good :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Widget styles in SVG"
    date: 2003-09-17
    body: "That would be nice-nice-nice.\nSomebody from Troll Tech lurking around?"
    author: "Peter Plys"
  - subject: "Re: Widget styles in SVG"
    date: 2003-09-17
    body: "You don't need trolltech to implement this because of the nature of qstyles (they are qt plugins)"
    author: "anon"
  - subject: "one important question!"
    date: 2003-09-16
    body: "As a brazilian I want to know why there are some countries not highlited on football (soccer) zones?\nI mean, why only Brazil, Argentina, Uruguay, Paraguay and Ecuator are highlited?\n\nYou know... football and brazilians are important to each other, so I'm curious :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: one important question!"
    date: 2003-09-16
    body: "Ouch, nevermind, NOW I've read the text.\nIt's the nations who got into 2002 world cup.\n\nA candy to who answer who was the champion! :)"
    author: "Iuri Fiedoruk"
  - subject: "Where's Daniel M. Dudley!!??"
    date: 2003-09-17
    body: "Where did mosfet go, it seems write after someone posted the X fork \"unconfirmed rumor\" all over at PCLINUXONLINE.COM, Inquirer, Madpenguin, KDE-LOOk etc. and everyone started totally insulting him and being a bitch his website was offline and it's still offline.\n\nI hope he didn't get sick and if he's just pissed at the community and other people he has every right to be and the people who insulted him for posting that rumor on his personal website ought to be ashamed, he had every right to and clerarly stated he did not know if it was true yet.\n\nI really hopes he will come back to KDE and is jsut too busy now, PixiePLus (development version) and Liquid 0.9.6 (development versioN) are fantastic and he did a lot more than just Liquid and PixiePlus.\n\nIf anyone knows where I canc ontact him please tell me.\n\nSorry taht this si O, it's jsut that all this talk about svg themes, svg icons and all this graphics talk made me think about him."
    author: "mario"
  - subject: "Re: Where's Daniel M. Dudley!!??"
    date: 2003-09-17
    body: "Don't forget MosfetPaint!"
    author: "Wurzelgeist"
  - subject: "Re: Where's Daniel M. Dudley!!??"
    date: 2003-09-17
    body: "Yeah, but I never actually got to try his paint program. I did like a lot of his ideas, like his professional thumbnail specifcation which the entire Linux community would benefit from, it is much faster and just plain better in every way. His kicker contributions and HiColor theme were also cool, thecommand line image browser was interesting too, but I like to have a GUI, he did a lot for KDE and Linux.\n\nIt's a shame the community was so mean, I guess all the SCO news postings were stressing out everyone and lot sof people flew of their handle thinking that mosfet posted to large nws sites when in reality he only poste don his website and his website readers did that for him,"
    author: "mario"
  - subject: "Capturing User Input"
    date: 2003-09-17
    body: "I know that the vector graphics look great in SVG, way to go KSVG team! However, can anyone tell me about the limitations of scripting in SVG? I've seen examples where you can push buttons, etc. How interactive can SVG be? Could this be a replacement for applets where some client side interaction is useful?\n\nThanks for any help,\n-Chuck"
    author: "Chuck"
  - subject: "Re: Capturing User Input"
    date: 2003-09-17
    body: "Hi,\n\nIt can become very interactive. Simulating input widgets like buttons, combo boxes etc. are very well possible and there exist svg implementations for this for a long time now. The limitations depend on how much of the DOM you can access, but for svg you can change its whole DOM and so do things like update text after pressing a button. I am not sure how much of the normal html/xml DOM can be accessed and changed though. The new work on SVG 1.2 tries to standardize possible UI functionality using svg AFAIK.\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: Capturing User Input"
    date: 2003-09-17
    body: "SVG 1.2 adds geturl/posturl and parsexml to SVG. With get- and posturl you can get content from a webserver (triggered by a button for example). parsexml will then return a documentfragement which you could add to your SVG. So SVG can be VERY interactive.\nThis is currently broken in cvs but niko is trying to fix that ;)."
    author: "Andreas Streichardt"
  - subject: "Re: Capturing User Input"
    date: 2003-09-18
    body: "Could you give an URL or something where I can find this kind of examples?\nI've searched for this, but all I can find is code that 'will eventually be able to....'\nNothing working, so far! :-("
    author: "Paul"
  - subject: "Great !"
    date: 2003-09-18
    body: "Especially the KPart layer. Outstanding work !\n\nRegards, Johan"
    author: "Johan De Messemaeker"
  - subject: "so why isn't it working? *whimper*"
    date: 2004-03-25
    body: "What are the technical prerequisites for konqi to show SVG? I just built 3.2.1 (libs, base, and graphics, plus some others) from the NetBSD pkgsrc and konqi's as ignorant of SVG as ever ... what could be missing? ksvgtopng works, and svgdisplay sorta works (gets the colours wrong if it doesn't run out of threads and crash) ... but my konqi thinks SVG are some sort of text object ..."
    author: "hume smith"
---
<a href="http://svg.kde.org/">KSVG</a> has recently been moved to the kdegraphics module, meaning that KSVG will now be part of the <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">KDE 3.2 release</a>.  KSVG aims to be a full flavored implementation of the W3C SVG standard. Some of you will think of icons when we speak of SVG but SVG is much more: It is a web technology with full ECMAScript/DOM support. With the number of SVG powered sites growing steadily, Konqueror will soon be able to display these sites with a high-quality and open-source viewer.  KSVG is fully integrated into the KDE framework and can be used in your applications as a <a href="http://phil.freehackers.org/kde/kpart-techno/kpart-techno.html">KPart</a>, enabling you to add support for vector graphics quite easily.  Have a look at <i><a href="http://svg.kde.org/preview_kde32.html">this special preview of KSVG</a></i> and prepare yourself for the power of vectors in KDE 3.2!
<!--break-->
