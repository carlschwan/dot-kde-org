---
title: "German Government Agency Rolls Out KGX"
date:    2003-03-10
authors:
  - "Dre"
slug:    german-government-agency-rolls-out-kgx
comments:
  - subject: "Why KGX?"
    date: 2003-03-10
    body: "I though KGX stood for KDE Gnome & LinuX - the article makes no mention of gnome, so why is KGX used in the title?"
    author: "GldnBlls"
  - subject: "Re: Why KGX?"
    date: 2003-03-10
    body: "KGX stands for KDE/GNU/LinuX, but hopefully the X can still be tweaked to stand for *NIX or even POSIX, KDE is being implemented successfully on more than Linux alone. There's a paper or draft on KGX somewhere, but Google won't find it. :("
    author: "Rob Kaper"
  - subject: "Re: Why KGX?"
    date: 2003-03-10
    body: "Fine, I have no problem with that, I think Solaris is a very nice OS for example. I was actually wondering about the 'G' part of it, not the X - ie why was gnome mentioned?"
    author: "GldnBlls"
  - subject: "Re: Why KGX?"
    date: 2003-03-10
    body: "Sorry, misread the previous answer - every previous mention of KGX I've seen, the G stood for gnome, I guess I read the reply too rapidly - Sorry :)"
    author: "GldnBlls"
  - subject: "Re: Why KGX?"
    date: 2003-03-10
    body: "Damn, guy, not everywhere you read an G is really dedicated to GNOME - Gtk, Gimp, etc etc. are all *not* owned of GNOME, but to many peeple do think so.\n*This* is the misunderstanding here. So, keep cool you read a G in any acronym next time ;)\n\nCheers,\ntrapni"
    author: "Christian Parpart"
  - subject: "Re: Why KGX?"
    date: 2003-03-10
    body: "> There's a paper or draft on KGX somewhere, but Google won't find it. :(\n\nSure it can find it: http://www.kdeleague.org/kgx.php?p=1"
    author: "Anonymous"
  - subject: "KOffice?"
    date: 2003-03-10
    body: "I think for now they would be better off with OpenOffice. I like KOffice, especially KWord and KSpread, but for professional use by non-geeks it lacks stability and features. \n\nI think it is better to start small so that the new OS is fun rather than frustrating for people.\n\nregards,\n\n   A.H."
    author: "Androgynous Howard"
  - subject: "Re: KOffice?"
    date: 2003-03-10
    body: "You may be right.  But this shows a dedication to KDE and it will most likely result in funding for KOffice components to fix what (if anything) is lacking.\n\n--adam"
    author: "Adam"
  - subject: "Re: KOffice?"
    date: 2003-03-10
    body: "> You may be right. But this shows a dedication to KDE and it will most likely result in funding for KOffice components to fix what (if anything) is lacking.\n \nGod I hope so. I like Koffice. My wife uses kspread for everything since I showed her how to take advantage of spreadsheets. Yet every time I use it I find there is another simple thing it will not do. I printed some pages only to find out that where text ran across two cells and displayed on the screen it chopped in print. Kword, for all it's attention was even worse. I needed to print out some point of sale display signs Sunday morning and it did Poster Bodoni from a saved file, but I previewed it first. In Kspread the font only printed as displayed if I set it bold. Otherwise it displayed an unrelated font... however on a new Kword file it proved to be virtually impossible to get it to print the displayed Poster Bodoni font. I was left with whether to copy the other 6 page file and delete the text and hope I could re-enter and it would work. \n\nThe trouble is you just don't know what these apps will do. In my experience they work really good until you absolutely need to have something right away and then they hang you out to dry. Scribus puts them to shame for printing, however it has serious usability issues like making large fonts overlap the line above until you remember how to configure it. I noted the fonts I installed with the KDE font installer were not there. Are they KDE only? It's hard to decipher having to look to see if it's Type 1 or TT...\n\nOne thing I really miss in Kspread is scriptablity. I'd like to be able to call a script in any language as I can with Quanta and use Kommander dialogs using DCOP to work with it. What potential. I keep dreading that I will have to make it happen myself... it would be really great for some benefactor to show up for Koffice.\n\nWhy do I feel so strongly about that? Because I sponsor Andras Mantia full time on Quanta. BTW I'm just a guy with a start up business so it's not easy for me. Aside from what he's done to help Quanta grow he's managed to do some serious bug squishing. I just looked at bugzilla and Quanta was #94 of 100 in open bugs with 5. Andras gave the status of all 5 and they will be going away... Andras also was #13 of the top 100 bug squishers. Guess how proud Andras and I are. ;-)\nhttp://bugs.kde.org/weekly-bug-summary.cgi?tops=100&days=7\n\nI know there are some people putting in some serious work on Koffice, but there needs to be at least one person sponsored or otherwise seriously committed (as in not having much of a life because of their passion to program) on each app. There's just too much to do. Too many people need the benefit and too few are carrying the load."
    author: "Eric Laffoon"
  - subject: "Re: KOffice?"
    date: 2003-03-10
    body: "Hello Eric,\n\nby talking about printing in KSpread I'm presonally hit, as this is the area I'm working on.\nHopefully you are describing the bug with the cvs version, as I don't have any bugreport about this with the release. If there would have been one, I would have fixed it.\nFor the cvs version I know about this, but this is not high priority for me to fix it now (release is in months, so enough time for fixing).\nI'm also in the top 20 of bug squashers in the last 180 days, mainly for KSpread bugs, bugs which have been mentioned by someone.\n\nI don't know what you mean with missing DCOP functionality. KOffice has DCOP interfaces, maybe some functions are missing, but as long as nobody mentions them, we cannot include them.\n\nFinally, I agree with you, that a full head would help very much. There is so much to do, which part timers simply cannot do correctly. For myself, I can mostly only scratch the surface as changing fundamental things is just too time consuming to do within 1 release cycle. Additional, for MS filters you normally don't find volunteers in OS, simply because it's just too boring."
    author: "Philipp"
  - subject: "Re: KOffice?"
    date: 2003-03-11
    body: "\"Additional, for MS filters you normally don't find volunteers in OS, simply because it's just too boring.\"\n\nThat's another reason why Kspread and kword need to use the Oo file format. When an occasional Word or Excel file comes in, we could use a tool based on Oo to convert it to the Oo format and then use it with Kword or Kspread."
    author: "Murphy"
  - subject: "\u00a1Funding!"
    date: 2003-03-20
    body: "That's the Keyword: Funding and this means developement, being able to put full-time programmers to work, acceleration...\n\nImprovement in quality, ergonomics, design, jobs in third-part firms...\n\nGermans firms has a nearby psichotic tendence to Standards (which is BTW the way things should be anywhere else), so if the German Gov installs KOffice this will be the standard in the short term (!).\n\nGreat Deal ;)\n\n"
    author: "Enric 'runlevel0' Montero"
  - subject: "Re: KOffice?"
    date: 2003-03-10
    body: "I beg to differ, Kword has made leaps and bounds towards being a tool for professional users in recent months. It works well now. It's completely intuitive to use, it's not overburdened by useless un-needed features which only 1% of the users might use once in 5 years.\n\nI like Kword. It doesn't set up the fonts quite right without help, that's all."
    author: "Christopher Sawtell"
  - subject: "Re: KOffice?"
    date: 2003-03-10
    body: "Are you talking about the development version?  What are the leaps and bounds you mention?"
    author: "not me"
  - subject: "Re: KOffice?"
    date: 2003-03-12
    body: "Only 1% of the users might use once in 5 years? Seriously Koffice needs Microsoft filters for Word (doc.) & excel & then you have access to 90 pecent of the desktop offices. And that's not bagging out Koffice, I love it but no good RTF export & no great doc inport or excel import or export = me asking my friends or work asking to switch to Koffice & that's not going to happen."
    author: "Jim"
  - subject: "kool!"
    date: 2003-03-10
    body: "Finally! That's where the \"K\" stands for ;-).\n"
    author: "rolf deenen"
  - subject: "Re: kool!"
    date: 2003-03-10
    body: "hehe... finally...\nAs I can recall the official version was: there is _no_ special meaning of the 'K'... (to be honest, when I first stumbled over KDE some years ago I thought the K came from K..alle D....). But if (maybe) KDE makes it to the desktops of more and more users... more and more media (which are not specialized in IT) will start to write stories about KDE and they will not be satisfied with a: 'Hey, the 'K' stands for nothin...'. In the end they will start to invent their own interpretation of this acronym..."
    author: "Thomas"
  - subject: "Re: kool!"
    date: 2003-03-10
    body: "The \"official way I remember the K story\" is at kdemyths."
    author: "Roberto Alsina"
  - subject: "Re: kool!"
    date: 2003-03-10
    body: "Maybe KDE stands for Kool Desktop Environment!"
    author: "Edemar"
  - subject: "Re: kool!"
    date: 2003-03-11
    body: "cute ;)"
    author: "anon"
  - subject: "Re: kool!"
    date: 2003-03-11
    body: "I'd rather if it was changed to be KDE Desktop Environment :)"
    author: "Luke-Jr"
  - subject: "KDE"
    date: 2003-03-11
    body: "K Desktop Environment\nKool Desktop Environment\nKorporate Desktop Environment\nKonfigurable Desktop Environment\nKoherent Desktop Environment\nKlown Desktop Environment\nKompatible Desktop Environment\nKute Desktop Environment\nKwazy Desktop Environment\n'k Desktop Environment\nKrauts Desktop Environment\netc. etc. etc.\n\nPick your favorite one or add your own suggestions."
    author: "Datschge"
  - subject: "Re: KDE"
    date: 2003-03-11
    body: "K'hoverment Desktop Environment"
    author: "Zatcher"
  - subject: "Re: KDE"
    date: 2004-01-30
    body: "Are you...???\n"
    author: "M Thorz"
  - subject: "Re: KDE"
    date: 2003-03-11
    body: "Kickass Desktop Environment"
    author: "Adam Treat"
  - subject: "Re: KDE"
    date: 2003-03-12
    body: "I like this one!"
    author: "Zeraien"
  - subject: "Re: KDE"
    date: 2003-03-14
    body: "'Honestly we're not CDE, look, it's a K' Desktop Environment... ;)"
    author: "Antony"
  - subject: "Re: kool!"
    date: 2003-03-13
    body: "Well, it is true that in Kalle's initial 'call to arms' paper, the 'K' did in fact stand for 'Kool', but that was promptly dropped when the first lines of code were written.  Also, on a historical note: the first class written for KDE was KConfig.  That class is still around too, which says a lot about design decisions early on.\n\nTroy Unrau\nKDE almost-hacker and enthousiaste"
    author: "Troy Unrau"
  - subject: "Re: kool!"
    date: 2003-03-13
    body: "Kalle?  I think you mean Matthias."
    author: "Navindra Umanee"
---
According to a recent
<a href="http://www.idg.net/ic_1188049_9676_1-5123.html">story</a> on
<a href="http://www.idg.net/">IDG.net</a>, <em>"A small German institute
has become one of the [German] Interior Ministry's first agencies to
implement Linux on the desktop, as the government pushes ahead with its
ambitious plans to introduce open-source software in the public sector."</em>
The test 50-seat rollout was spear-headed by the Federal Office for
Information Security (BSI) in conjunction with several small German IT
companies.  The thin-client setup reportedly includes
<a href="http://www.koffice.org/">KOffice</a> as the office suite.
<!--break-->
