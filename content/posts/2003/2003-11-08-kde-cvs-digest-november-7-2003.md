---
title: "KDE-CVS-Digest for November 7, 2003"
date:    2003-11-08
authors:
  - "dkite"
slug:    kde-cvs-digest-november-7-2003
comments:
  - subject: "Thanks!"
    date: 2003-11-08
    body: "Thanks, Derek!"
    author: "Loket"
  - subject: "filelight"
    date: 2003-11-08
    body: "will filelight be in kde 3.2 final release? it looks promesing"
    author: "Mohasr"
  - subject: "Re: filelight"
    date: 2003-11-08
    body: "No, but fsview (showing rectangles)."
    author: "Anonymous"
  - subject: "Re: filelight"
    date: 2003-11-08
    body: "Nope, it wasn't complete enough to submit in time, although now it is fairly complete and stable :)\n\nAnyway I am concerned about the clash of roles between Filelight and FSView. Especially as I am planning to make Filelight a KPart so it can integrate into Konqi just like FSView. But I also plan to separate the input gatherer from the ouput renderer and allow any kind of heirarchical data to be represented by the RadialMapPart and then try to get it working with KSpread as a chart type, maybe working with KCacheGrind, and anything else I can think of as this will make it more worthy of bundling with the next KDE release.\n\nAlso I was thinking of having a start up screen that summarised your mounts just like KDiskFree. If I use KDiskFree's code maybe Filelight would count as KDiskFree version +1 and it could replace it? Worth some consideration perhaps."
    author: "Max Howell"
  - subject: "Re: filelight"
    date: 2003-11-08
    body: "Well, a KRadialMapWidget will be cool, and a filelight kpart will rock for sure. I think it should replace the fsview 'cause filelight is so clean and it has it's smart radial display that helps you find wasted space.\n\n Maybe it should replace fsviewpart after the freeze, many friends told me about it.\n\n About the clash.. I think that summarizing disk usage and mount points with a nice graphical interface and letting the user 'go deep' with filelight is all we need in place of KDiskFree and other silly disk usage tools ^_^\n\nGreat Work, MaxH. ! ;-P"
    author: "Enrico R"
  - subject: "Re: filelight"
    date: 2003-11-08
    body: "Perhaps you can join the two views, offering a switch via a dropdown-button (see IconView)..."
    author: "Friedrich"
  - subject: "How do I enable FSView"
    date: 2003-11-08
    body: "I am running KDE 3.1.4 on Fedora and I don't see it.\n\nAnyway, from what I read and saw of FileLight, it seems very cool, but KDirStat seems to offer more information, even if it doesen't look as cool.\n\nhttp://kdirstat.sourceforge.net/kdirstat/"
    author: "Tron"
  - subject: "Re: How do I enable FSView"
    date: 2003-11-08
    body: "fsview will be new in kdeaddons 3.2."
    author: "Anonymous"
  - subject: "How do I DISABLE FSView"
    date: 2004-10-06
    body: "I want disable fsview from konqueror and kdirstat... any idea?"
    author: "Gtavo"
  - subject: "Re: How do I DISABLE FSView"
    date: 2004-10-07
    body: "Obvious answer: Just don't install it or remove it from the KDE lib folder again."
    author: "Datschge"
  - subject: "Re: filelight"
    date: 2003-11-09
    body: "Hi,\n\nfor many things Filelight will be better suited. I have testes FSView and found it not so productive for file management. I would guess that the circle you draw is better to overview.\n\nSo go ahead. You ought to try and share code with FSView where possible, so the circle is just one presentation. Ideally I would hope for read-write access. I should be able to delete into the view if possible. Dreams?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: filelight"
    date: 2003-11-09
    body: "First of all, I like the radial drawing in Filelight.\nIt won't clash with FSView; I think they can complement each other.\n\nThe most fundamental difference between the radial pie drawing in\nfilelight and a TreeMap (as used in FSView) is that the first\nstill splits children in the 1-dimensional space (circle border),\nbut TreeMaps use 2-dimensional splitting (rectangles).\nThus, the resolution capability of a TreeMap is 1 order of\nmagnitude higher.\n\nIt would be nice to have the radial drawing as a widget with a\nclass interface similar to QListView (I've done this with the\nTreeMapWidget class). This simplifies its adoption as replacement\nfor hierarchical representations (even perhaps in KCachegrind).\n\nSome questions for the drawing part, as I didn't haved tried it yet:\n- does it use tooltips for additional info?\n- does it allow multiple labels per item (as columns in QListView, or\nthe labels in the TreeMapWidget)?\n- Drag & Drop? That's missing in my widget.\n\nJosef\n"
    author: "Josef Weidendorfer"
  - subject: "Re: filelight"
    date: 2003-11-09
    body: "Hi Josef,\n\n> First of all, I like the radial drawing in Filelight.\n> It won't clash with FSView; I think they can complement each other.\n\nGood stuff, I spose once I've KParted it, it can be listed below FSView in Konqi then. Choice is good :)\n\n> It would be nice to have the radial drawing as a widget with a\n> class interface similar to QListView (I've done this with the\n> TreeMapWidget class). This simplifies its adoption as replacement\n> for hierarchical representations (even perhaps in KCachegrind).\n\nYeah this makes sense, I'll look into FSView's code and try to mirror its interface. For some reason it had slipped my mind to make the radialMap available to KDE as a widget, I kept thinking it would only be useful as a KPart. Silly me :)\n \n> Some questions for the drawing part, as I didn't haved tried it yet:\n> - does it use tooltips for additional info?\n\nYep, indeed it does, just like FSView.\n\n> - does it allow multiple labels per item (as columns in QListView, or\n> the labels in the TreeMapWidget)?\n\nCurrently no, but in order to make the Widget more generally useful I'll be implementing this shortly I should think. I'll refer to your class as a reference.\n\n> - Drag & Drop? That's missing in my widget.\n\nI hadn't really thought about D'n'D as yet. What sort of objects should the widget accept?"
    author: "Max Howell"
  - subject: "'Kexi's Environment' link"
    date: 2003-11-08
    body: "The current 'Kexi's Environment' link can't be viewed since it asks for username and password. The visitor link is http://www.kexi-project.org/wiki/wikiview/index.php?Kexi%27s%20Environment"
    author: "Datschge"
  - subject: "Re: 'Kexi's Environment' link"
    date: 2003-11-08
    body: "It's fixed now.\n\nDerek"
    author: "Derek Kite"
  - subject: "Thanks a lot ^_^"
    date: 2003-11-08
    body: "Thank you for bringing us fresh news on kde development!\nI asked for a cvs account because i read kde-cvs-digest and now that i have it, i'm waiting for the feature un-freeze..\nHurray for Derek Kite !! ^_^"
    author: "Addicted to Derek!"
  - subject: "Re: Thanks a lot ^_^"
    date: 2003-11-08
    body: "> I asked for a cvs account because i read kde-cvs-digest and now that i have it\n\nI doubt that you got it because of this rationale.\n\n> i'm waiting for the feature un-freeze\n\nSo you're one more who doesn't care about fixing bugs?"
    author: "Anonymous"
  - subject: "Re: Thanks a lot ^_^"
    date: 2003-11-08
    body: "My goodness. You've managed to cover at least 4 of the rules in \"How to ruin a community project\". Question qualifications. Question motives. Suggest that you are really no help. Question what they are doing.\n\nThe appropriate response when someone offers to give some of their time and expertise is to say Thank-You.\n\nOr is it just a bad hangover?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Thanks a lot ^_^"
    date: 2003-11-08
    body: "i ++agree. \n\ninstead of negatively replying \"so you're one more who doesn't care\" perhaps he could've said something like, \"cool, can't wait to see what you get up to; hopefully you'll get after some outstanding bugs while you're at it. i know i'd appreciate that.\"\n\nmost negatives can be phrased as positives; when i manage to do that (which isn't always =) it seems to feel much better when it leaves me, and i know that it feels much better when it arrives. and not just for me =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Thanks a lot ^_^"
    date: 2003-11-08
    body: "i think what he meant was this: Derek's CVS digests got him interested in working on KDE, and that in turn led to him work towards getting a CVS account. in other words, KDE has garnered one more active developer due to the CVS Digests. sounds like a nice win to me, and a good nice bit of encouragement for Derek.\n\nas for being \"one more who doesn't care about fixing bugs\": chill out... there are lots of people already fixing bugs, and it seems that after getting into KDE development most people start fixing bugs whether that was their original intention or not.\n\nof course, there are a number of use cases that KDE does not cover at this point; we need more features just as we need fewer bugs."
    author: "Aaron J. Seigo"
  - subject: "Re: Thanks a lot ^_^"
    date: 2003-11-09
    body: "Cool, can't wait to see what you get up to; hopefully you'll get after some outstanding issues while you're at it. i know i'd appreciate that. :-)\n\nWhat Derek does to KDE cannot be underestimated. A lot people see things and start to believe the truth: They can actually help. :-)\n\nBTW: Feature Freeze is not true for kde-nonbeta and there you can already go ahead and make things more ready.\n\nYours, Kay"
    author: "Debian User"
---
In <a href="http://members.shaw.ca/dkite/nov72003.html">this week's CVS-Digest</a>:
Bug fixes and more bug fixes. <a href="http://uml.sourceforge.net/index.php">Umbrello</a>, <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://quanta.sourceforge.net/">Quanta</a>, <a href="http://konsole.kde.org/">Konsole</a>, <a href="http://korganizer.org/">KOrganizer</a> encoding, <a href="http://www.koffice.org/kspread/">KSpread</a>,
KHTML, <a href="http://www.slackorama.net/cgi-bin/content.pl?juk">JuK</a>, <a href="http://kopete.kde.org/">Kopete</a>, <a href="http://devel-home.kde.org/~kgpg/">KGPG</a>, KWin and kdeui all have a large number of bugs fixed.
<!--break-->
