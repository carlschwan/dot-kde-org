---
title: "linmagau.org: Kaboodle/Noatun vs XMMS"
date:    2003-05-02
authors:
  - "jblomo"
slug:    linmagauorg-kaboodlenoatun-vs-xmms
comments:
  - subject: "if noatun were only usable"
    date: 2003-05-01
    body: "Fine that it sounds better. But what about hte usability? Listening to one file at a time. who does that. hte playlist on noatun sucks. the app crashes all over the place. So how good is that really? I have my hope high for juk as the noatun replacement. Better functionallity and being part of 3.2. Still has some way to go, but it's getting there..."
    author: "Juergen"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "\ndude you are fuckin' right... I wonder if the noatun developer ever used it"
    author: "ac"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "I use it all day and have no crashes, no clue what you guys are doing"
    author: "mETz"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "<I>Listening to one file at a time. who does that.</I>\n\nI almost never listen to more than one file at a time.  It's too confusing!  ;)"
    author: "LMCBoy"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "I still wish I knew why some people have crashing problems with noatun. I have been using kde cvs version since the kde 3 alphas and I have not had noatun crash at all. I really like noatun for usability since I can turn off its gui pretty much and use the keyz bindings so I can control it from any application just from the keyboard without having to click on something.  \n\nI also have to admit that I extremely rarely listen to more then one audio file at a time. However you can have multiple noatuns open and they will play just fine. I am currently use kde cvs head under debian sid."
    author: "kosh"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "I dont have trouble with Noatun's stability. only problem I have is it doesnt obey session management half the time. If you are looking for a better playlist, check out hayes. <a href=\"http://www.freekde.org/neil/hayes/\">http://www.freekde.org/neil/hayes/</a>. It is a file system base playlist that handles large collections well. Thanks Neil its the bomb! It ought to be distributed with KDE IMHO."
    author: "Echo6"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "Yes I also I also think it\u00b4s time for the noatun::hayes playlist to be ditributed with KDE.\nI hope we\u00b4ll see it 3.2 or maybee in 3.1.2\n\nGreetings.\n"
    author: "Sangohn Christian"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "Noatun with Hayes has been my choice for a long time already. No stability problems since Hayes 1.2 release. Everyone who has not yet tried hayes, go try it now! Especially useful if you have large collection of music."
    author: "samppa"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-04
    body: "i'm also a big fan of hayes, and i'd really like to see it in the standard kde distribution ...\n\n\n"
    author: "ik"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-04
    body: "Just want to add:\n\nNoatun (KDE3.1) adds all files in a directory to the playlist, but how can I play txt or jpg-files? :-D\n\nAfter I added a webradio url all other tracks were deleted.\n\nI really like XMMS with Sunshine-Live skin :-)\n\n"
    author: "Christian Schuglitsch"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "Playlist problems?  Use Hayes.\n\nYes, Hayes.\n\nhttp://www.freekde.org/neil/hayes/\n\nThe last Noatun playlist you'll ever need (until the Playlist API breaks for KDE 4 and I have to write a new one again, just like I had to scrap Tron in favor of Hayes for KDE 3 :-)"
    author: "Neil Stevens"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "[++++++++++++++}\nThe Hayes playlist made me remove xmms and use noatun instead :)"
    author: "Shift"
  - subject: "OT: libart_lgpl"
    date: 2003-05-02
    body: "Everytime I try to compile a KDE program it doesn't find\n/usr/lib/libart_lgpl_2.la\n\nAnybody has a clue in which package that file is?\n\nThanks a bunch"
    author: "Roland"
  - subject: "Re: OT: libart_lgpl"
    date: 2003-05-02
    body: "\"Libart is a 2D drawing library: its goal is to be a high-quality vector-based 2D library with antialiasing and alpha composition.\"\n\nhttp://gnuwin32.sourceforge.net/packages/libart.htm\nor search a package according to your distribution.\n\nregards"
    author: "thierry"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-03
    body: "Wow.. this playlist is pretty cool! Why isn't it included by default with KDE?"
    author: "lit"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-03
    body: "Bigotry of the Noatun maintainer."
    author: "Anonymous"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-02
    body: "I use to have problems with Noatun crashing, but not too much anymore -- well except for if I use a visualization like Synnescope(sp?) or Tippercanoe.  Speaking of those two, why can't you just ditch the one that doesn't have full-screen capability.  Why have two \"seperate\" visualization plugins when both do the same thing except that one can go into fullscreen while one cant? \n\n<rant>\nAnyway, what I do have problems with now, is Noatun either locking up (rarely but does happen) or Noatun having \"lag\" problems.  I bind the \"Show Playlist\" option to one of my million extra keys on my keyboard.  Sometimes if I open it, nothing happens.  So I press it again because I don't know if my crappy keyboard didn't \"see\" when I pressed it (happens sometimes) or what.  Nothing still happens, so I press it again wait a few sec and press it again.  Then all of a suddent my music skips a lot and then the playlist window open's and closes a few times.  Then about maybe 5% of the time, noatun locks KDE up to the point that the keyboard doesn't work at all but music keeps playing.  So what I have to do, is navigate with the mouse to the process viewer and kill noatun.  Then my keyboard starts to work again heh.  I guess I really should report this as a bug, but I've been way to lazy to do that.\n\nAnyway, I think the main problem with KDE's multimedia \"framework\" is that it feels all to hacked together.  I know if I dont like it,then I should fix it, but I don't have that kind of time to start something or help because I'd have to end up abandoning it.  I think now that MPlayer has basically \"stabilzed\", KDE needs to ditch Noatun and Kaboodle and have one multimedia application like Windows Media Player.  KPlayer is just fine for it.  I know it reeks of MS, but I think WMP is a nice idea.  Really, \"regular\" people don't give a rats ass for having Kaboodle, Noatun, KPlayer, KMplayer, ... and they just want to play their music and videos.  Making this into a better \"Part\" similar to how WMP is would be nice too.  My resolution is 1600x1200, but even in 1024x768, when the Noatun part loads, having only the \"progress bar\" and the controls looks really crappy.  Adding a small view for the part would be better.  Also, maybe having a visualization embedded into the part would be a good option -- defaulting with a either \"null\" visualiztion or a really low-cpu-hungry vizualization.\n</rant>\n\nSo basically, I think KDE needs one multimedia application based on MPlayer 0.90 (either a ripped mplayer) or how KPlayer is.  For audio, just use the standard lame/vorbis/etc... libraries or whatever.  Hopefully someone gets my drift.\n"
    author: "Super PET Troll"
  - subject: "Re: if noatun were only usable"
    date: 2003-05-03
    body: "in my experience, i ahve albums of songs (like over 2000) in direcrtories and the split playlist absolutely sucks donkey balls.  what you need is the hayes plugin for ALL of your playlists. even not folderized in my experience it is much more stable."
    author: "standsolid"
  - subject: "JuK for president! :p"
    date: 2003-05-02
    body: "JuK has replaced XMMS on my desktop a few months ago.. I just LOVE the look 'n feel of it, i really can't live without it anymore :p XMMS is also stuck with gtk 1, and it seems it's gonna stay that way for quite some time... But i would recommend JuK (<a href=\"http://www.slackorama.net/cgi-bin/content.pl?juk\">JuK homepage</a>) to every KDE user out there.. It's based on iTunes, the MaxOS player. It needs some more features (like a search function), but it's nice and stable. IMHO it should replace noatun as default KDE player, since noatun is waaay too slow and ugly imho... That's just my 2 cents though.."
    author: "Bart Verwilst"
  - subject: "Re: JuK for president! :p"
    date: 2003-05-02
    body: "Juk CVS has already a search function."
    author: "Anonymous"
  - subject: "Re: JuK for president! :p"
    date: 2003-05-02
    body: "Woohoow! :D"
    author: "Bart Verwilst"
  - subject: "Re: JuK for president! :p"
    date: 2003-05-02
    body: "I'm also a JuK user. It's really cool. It's only a matter of time before it becomes a (gasp!) match for iTunes :)"
    author: "Rayiner Hashem"
  - subject: "Re: JuK for president! :p"
    date: 2003-05-02
    body: "Does it supports PLS play streams?\n\nThanks,\nHetz\n"
    author: "Hetz Ben-Hamo"
  - subject: "Re: JuK for president! :p"
    date: 2003-05-02
    body: "It looks like an interesting player, but as for that website I've got a sneaking suspicion that the author is colour blind"
    author: "Someone"
  - subject: "Yammi is coming along nicely + other rants"
    date: 2003-05-02
    body: "After I saw my friends using iTunes search feature I was very jealous.  Luckily Yammi has a very good search feature (Usability is another issue).  Yammi also has issues with xmms and noatun (the integration seems like a kludge).  I guess I should stop complaining and submit a patch.....\nThe other day I tried ripping some albums and I found that abcde worked the best.  The KDE 3.1 ripper tool has a pretty good interface, but I couldn't get it to point to another device (other than /dev/cdrom) and the cddb lookup didn't work (but oddly enough it did for kscd....).  I tried grip and it worked but the interface is pretty complicated and it also crashed.  Once I configured abcde, it worked the best."
    author: "ac"
  - subject: "Re: Yammi is coming along nicely + other rants"
    date: 2003-05-02
    body: "Hmm..\nI think JuK looks more professional than Yammi... Those next, previous, .. buttons look so amateuristic and ugly :$ Ofcourse that's just my view, and I can't wait as well for JuK to get search possibilities!\n\nCheers!"
    author: "anon"
  - subject: "Re: Yammi is coming along nicely + other rants"
    date: 2003-05-02
    body: "As was said earlier, JuK has this in CVS."
    author: "ac"
  - subject: "Re: Yammi is coming along nicely + other rants"
    date: 2003-05-06
    body: "As the developer of yammi I also think that the user interface needs some polishing. Anyone wants to contribute some decent icons? As you can see, I am not too much of an artist...\n\nNext version will be ready in a week or so and focus on functionality, but afterwards I really want to polish the dialogues and interface in general. Any help is more than welcome!\n"
    author: "onoelle"
  - subject: "Why XMMS is __still__ better than noatun..."
    date: 2003-05-02
    body: "Why XMMS is __still__ better than noatun as a mp3 player for me:\n\nbecause of the crapability of aRts.\n\nNoatun and artsd use about 13% of my CPU while listening to mp3s.. this is because arts is doing software mixing. my sound card, a sblive, can do hardware mixing. xmms can use OSS, alsa, as well as artsd. using OSS or alsa, xmms takes about 1-3%  CPU time. \n\nI reallly hope that artsd will support hardware mixing (no more sound server!!), or that noatun will support direct OSS or alsa playback. \n\nIt's cool that juk supports multiple ways of playback! Hope it supports direct playback soon."
    author: "lit"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-02
    body: "aRts is not a soundserver."
    author: "Neil Stevens"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-02
    body: "yes, but doesn't it's capabilities include that of a sound server? (that is, mixing two or more steams together into one)\n\n\nQuite a few users don't actually need that these days since their cards can do it much faster by themselves. The emu10k1, which is probably the most used card in Linux, can do this with both OSS and alsa drivers. "
    author: "lit"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-04
    body: "I'm using a Typhoon Acoustic 6 with the C-Media 8738-Chip.\n\nIt's quite good but perhaps not the best accelerated card.\n\nIt supports 44,1khz natively, has optical/coax digital in&out.\n\nEven many mainboards use it as on board sound but then many conncetors are missing.\n\n\nI use alsa 0.9.3 now and it seems like this card needs artsd or esd to mix sounds,\n\nSchugy"
    author: "Christian Schuglitsch"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-02
    body: ">Noatun and artsd use about 13% of my CPU while listening to mp3s.. this is because arts is >doing software mixing. my sound card, a sblive, can do hardware mixing. xmms can use >OSS, alsa, as well as artsd. using OSS or alsa, xmms takes about 1-3% CPU time. \n \nThis is also my main concern about the KDE multimedia players (noatun, kaboodle, juk). When I compile or do some CPU intensive task, they tend to stop and you hear clicks in the playback. The situation with XMMS is much better. On my machine XMMS uses around 1-2%, while the artsd uses >5%. But I agree, Noatun sounds warmer than xmms.\n\nAndras\n"
    author: "Andras Mantia"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-02
    body: "With JuK, you are free to use gstreamer, which doesn't have a daemon. This might help."
    author: "Daniel Molkentin"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-02
    body: "I agree completely. I say, either make noatun have the ability to use alsa or oss, or speed arts up. Quite frankly, I would use noatun instead of XMMS if it wasnt for that damned arts. Noatun's playlist, IMHO, is much better than XMMS'. You can select/deselect songs without removing them from the list, or use the handy \"type to search\" feature that every kds list has to quickly jump to a specific artist (if your list is sorted and artist name is first). Extremely useful, I think, vs XMMS' rather simplistic playlist."
    author: "optikSmoke"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-03
    body: "Do you understand the whole point of this? Noatun sounds better because it uses Arts. Arts isn't about being a sound server, that secondary to it's main purpose of produce high quality sound. It would be good if it does get faster, but I'm more interested in having good sound."
    author: "David"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-03
    body: ">  that secondary to it's main purpose of produce high quality sound. \n\nThen why is arts a part of KDE then?\n\n\n> It would be good if it does get faster, but I'm more interested in having good sound.\n\nAnd I think most people are interested in the former, considering the popularity of xmms and the sheer number of people who hate arts."
    author: "lit"
  - subject: "Re: Why XMMS is __still__ better than noatun..."
    date: 2003-05-03
    body: "No, I think you have misunderstood my cristicism. I am simply saying, outside of sound quality, it would be nice to be able to do other things while the music is playing, and not have it skip and jump. Quite frankly, music quality is a meaningless measure if arts can't even play continuously while the system has any appreciable load on it."
    author: "optikSmoke"
  - subject: "Tell me about it..."
    date: 2003-05-02
    body: "On RH 9 my system with galeon (6 tabs), 3 Nautilus, OO 1.1 my system uses about 5% CPU. After using XMMS my CPU usage jumps to 29%! WTF!\n\nBTW: KDE should really looka t Nautilus, it is extremely easy to use and very professional and good looking. I love the way it does selecting, the zoom feature, the sidebar (with emblems which can be placed on desktop icons too), the nice shadow for pictures, view as audio and list modes, notes sidebar module, burn to cd feature, fonts viewer etc. It is jsut so much easier to use too, there are only a few buttons only for waht you want. Context menus are clean, not like 20 options in KDE and its fast like Konqueror. Technically it is not superior, but its usability IMO is far better. Please improve Konqueror.\n\nSomeone else complained about selections too: http://www.kde-look.org/content/show.php?content=3910\n"
    author: "Michael"
  - subject: "Re: Tell me about it..."
    date: 2003-05-03
    body: "If you want something that's easy to use, then why are you using KDE with its many different options and configurations?"
    author: "nac"
  - subject: "Re: Tell me about it..."
    date: 2003-05-03
    body: "Uh, when was usability not on the priority list for KDE development?\n\nOh wait, usability has always been very important for KDE. Unfortunatly, sometimes the excitement of adding new features temporarily hinders usability work. In the end, I think both will be balanced. :)\n\nThere are of course, sadly, many parts of KDE 3.1 where usability is lacking. On the bright side, KDE 3.1 is not the end of KDE 3.x development by any means, and KDE 3.2 should have tons of usability improvements. \n\nOne problem is, of course, differing opinions about usability. I for one, would advocate much simplier toolbars for 90% of KDE applications out there. This to the point of removing buttons like \"Forward\" and \"Go\" from Konqueror's default toolbars. Other people might say that this might actually hinder usability; as some people are used to these kinds of features being in toolbar, and they would have to look around to put it back. Unfortunatly, both sides have valid points."
    author: "fault"
  - subject: "Re: Tell me about it..."
    date: 2003-09-06
    body: "xmms only uses 0.3% of my cpu while playing high quality mp3's, and cd's. P3 533,  sblive!. Theres no reason it should be using 29% unless your running lots of stuff in the backround."
    author: "terminater"
  - subject: "Thank from a satisfied Noatun/Kaboodle user"
    date: 2003-05-02
    body: "Personally, I like both. I have found that they are more than useable and sound great.\nSo once again, thanx."
    author: "a.c."
  - subject: "erm - wtf?"
    date: 2003-05-02
    body: "Am i the only person who, throughout reading this, thought \"wtf sort of a review is this?\"\n\ntheres no mention of :\n* who the author was (background w.r.t audio etc)\n* what sound card he was using\n* what drivers he was using (alsa? oss?)\n* software versions\n* hi-fi equipment\n* speakers\n* system spec\n\nany one of these could change could change the outcome entirely.\n\nand his song choice was hardly systematic or structured to say the least (\"this is one i w4rz3d @ a lan party\"). Further, theres no mention of what the mp3s were encoded with, where the original track came from, sample rate, etc, etc.\n\nEverything in that article either points to the guy not knowing what hes talking about and/or not knowing what hes doing.\n\nSo, to the dot.kde editors:\nYes, it comes out in favour of a kde app, but *please* try to show *some* integrity when linking to articles. posting something like that is just whoring."
    author: "s.f"
  - subject: "Re: erm - wtf?"
    date: 2003-05-02
    body: "From the first page...\n\n The testing machine has a Sound Blaster Live! card, with a set of Sennheiser HD 212 Pro headphones. The Audio API in use is RedHat's default OSS drivers, both mp3 players will be using their default options, therefore using the default audio decoding engine provided. There will be no EQ enabled to keep the playing ground fair. I've come up with the following list of songs to try out with each player; \n \nInertia Creeps - Massive Attack (224 kbps) \nBlack Coffee - All Saints (128 kbps) \nWake Up - Rage Against the Machine (128 kbps) \nWhat Your Soul Sings - Massive Attack (224 kbps) \nMoshi Moshi - Corouroy (128 kbps) \nHero of the Day - Metallica (192 kbps) \nPart of Me, Live Version - Tool (192 kbps) \nOutlaw Torn, S&M version - Metallica (192 kbps) \n"
    author: "spooq"
  - subject: "Re: erm - wtf?"
    date: 2003-05-02
    body: "my apologies on not seeing those facts - i think i'd forgotten them by the time i'd got to the end of the article.\n\nHowever, I still maintain my points about the track selection (which i did not overlook the first time), and the format (random mp3s from unknown sources).\nIf he was, as stated, using the default mp3 players provided, then the versions must be out of date since (afaik) redhat 8+ doesnt ship with mp3 support. (note software versions sill not specified).\nWhat ive said above fits in with my opinion of the way the article was written, and my overall opinion of the article remains mostly unchanged. its still whoring."
    author: "s.f"
  - subject: "All obsoleted by MAS"
    date: 2003-05-02
    body: "http://www.MediaApplicationServer.net/\n\n:-)"
    author: "C++ Lover ..."
  - subject: "Re: All obsoleted by MAS"
    date: 2003-05-03
    body: "How well does MAS perform? Does it use less CPU than ESD and aRts? Does it not mess up the sound like ESD does?"
    author: "Ac"
  - subject: "Re: All obsoleted by MAS"
    date: 2003-05-05
    body: "Not tried it yet . However they calim to have 'low memory print' and the screenshoots look really impressive-a sound engineers paradise. However still got to be put through the rack i guess.\n"
    author: "Bharat"
  - subject: "Re: Yet another useless media API."
    date: 2003-05-06
    body: "I really hope people will learn from SGI. The SGI media libraries are good from a media application programmer POV. They are simple to understand, they provide access to what media devices export and little else. What you want when you do media is some coherent method to,\n\n1) read data from a device.\n\n2) to write data to a device.\n\n3) control the hardware properties of the devices you use.\n\nEverything else you can put OUTSIDE of the API because given any advanced application it will be highly specific and all the simple applications will only use simple things like read to a device, writing to a device or manipulating the controls of a device..."
    author: "coward"
  - subject: "Cool!"
    date: 2003-05-02
    body: "So Kaboodle/Noatun actually work now? I've got the versions that come with KDE 3.1.1a and niether player will play any files. wheather they are mp3, ogg, mod, it, midi, mpg, avi, even wavs won't play. I've tried the Kabbodle with 3.0, 3.1 and 3.1.1a, and the only time Noatun played any files was back when I had KDE 2.*, and even then it was fairly unstable."
    author: "Thomas Fjellstrom"
  - subject: "Re: Cool!"
    date: 2003-05-02
    body: ">KDE 3.1.1a and niether player will play any files. wheather they are mp3, ogg, >mod, it, midi, mpg, avi, even wavs won't play.\n\nDo you actually have a soundcard? ;-)\n\nI don't know why kaboodle shouldn't play for you. Does anything else work? (i.e. xmms for example) otherwise you have serious hardware/driver problems "
    author: "uga"
  - subject: "Re: Cool!"
    date: 2003-05-02
    body: "Har Har ;) Good one though :)\n\nYup. Have an SBLive! Value, (and a builtin AC97 thing, usually disabled), arts is usually running, XMMS, Xine, Mplayer all work with the OSS and/or ALSA drivers, KDE's startup sound even plays. But noatun hasn't worked since 2.* (and then It'd crash most of the time) As for kaboodle, I try and play a file... Ok, scratch that, I tried just starting kaboodle and using the file -> open command and Whoop! That worked, Ok.. Now I'm confused. :( I retried Kaboodle launched from Konqeror (I'm pretty sure thats all I've ever tried before) and it worked too.. Maybe it was just the Debian 3.1 version[s] I used to use. Possible, that whole install was a mess. Same goes for Noatun :( I tried testing it again, just to see how foolish I can look ;) It works all of a sudden. heh.\n\nI'm somewhat impressed with the Gentoo packages now. Things are faster, and more reliable :o\n\nSory to post such a negative message!"
    author: "Thomas Fjellstrom"
  - subject: "Review"
    date: 2003-05-02
    body: "Yay, I won.  Well, not really.  Two problems here.\n\n1.  Jim Blomo is right.  John Knight was wrong to say that Kaboodle and Noatun sound the same.  Kaboodle is the generic one-shot player. Noatun is the optimized music player.  Given some effort with the Noatun equalizer and plugins, Noatun should *always* sound better for music playback than Kaboodle.\n\n2.  Why would audiophiles use lossy compressed music?\n\n3.  This was really a review of mpeglib, the mpeg decoder library that ships with kdemultimedia.  That player with the horrid, horrid UI could sound just as good as Kaboodle if an mpeglib plugin were written for it, too.\n\n4.  To all you people complaining about the software not working, get a better packager.  If you're trusting SuSE, Mandrake, Red Hat, Debian, or FreeBSD, you're not getting a good kdemultimedia."
    author: "Neil Stevens"
  - subject: "Re: Review"
    date: 2003-05-02
    body: ">To all you people complaining about the software not working, get a better\n>packager. If you're trusting SuSE, Mandrake, Red Hat, Debian, or FreeBSD, you're\n>not getting a good kdemultimedia.\n\nWhat about us who use cvs? Are we getting the good kdemultimedia by default, or should we compile/add/change/tune something else?\n\nI saw that there are some plugins in kdemultimedia that are not compiled by default right now. Should we, or should we not use them?"
    author: "uga"
  - subject: "Re: Review"
    date: 2003-05-02
    body: "You do not want to compile the two kdemultimedia arts plugins that are disabled.  Those were efforts started when mpeglib didn't work on FreeBSD, and those efforts were aborted when mpeglib was made more portable.  I don't think the plugins ever did get stabilized."
    author: "Neil Stevens"
  - subject: "Re: Review"
    date: 2003-05-02
    body: ">2. Why would audiophiles use lossy compressed music?\n\nBecause Noatun doesn't play FLAC. http://bugs.kde.org/show_bug.cgi?id=54394\n\nThis means that I only use XMMS, as no KDE apps will play my FLAC files."
    author: "Blue"
  - subject: "Bit more to add"
    date: 2003-05-02
    body: "Heh, I started with two points and ended with four, swell.\n\nJust a followup to the packaging issue:  If you're trusting Red Hat, you're not even getting mpeg 1 layer 3 audio playback at all!  Now that Red Hat has applied for its own software patents (on portions of Linux in fact), Red Hat has decided to play ball with the rest of the software patent world, starting with honoring the mp3 patents."
    author: "Neil Stevens"
  - subject: "I still like XMMS"
    date: 2003-05-02
    body: "It seems to react much \"snappier\" to me. And I still don't like that arts-stuff.\nSometimes the audio is blocked without any reason and I can't play\nany new files. Otherwise, no audio player is really what I need. Most ones\nare too small and try to simulate some real-world device. I find\nthis somewhat ridiculous. KWord doesn't look like a typewriter, either. I want\nan application which I can run fullscreen which shows all my songs properly\nand has a standard KDE GUI. Next Problem: I don't like those ID-Tags.\nWho has the time to enter all that information. Do you always know\nfrom which year a song is? And what if I want to categorize songs in \nsome way which does not fit into those categories? I'm about to write a\nKDE-MP3-Player myself because I really need some good management software\nfor my MP3s. But there learning curve for writing KDE apps is quite steep\nif you are coming from Windows / Delphi and there is hardly any help in\nthe kdevelop forums. Anyway I hope I can release a first version soon."
    author: "RJ"
  - subject: "Re: I still like XMMS"
    date: 2003-05-04
    body: "http://easytag.sourceforge.net/\n\nThis one makes the work easier. Well, I really do things like\n\nmd mycd\ncdparanoia -z -B -g /dev/scdx initials.wav\noggenc -q5 *.wav\n\nUntil now the names are like track01.initials.wav\n\nI enter the tag and afterwards I use the renamer: %a -%n- %b %t or something similar :-)\n\nIf you just knew how many cddb entries don't match my needs....\n\nHave fun :-)"
    author: "Christian Schuglitsch"
  - subject: "management"
    date: 2003-05-05
    body: "If you're looking for management software, look at JuK (or for that matter one of the dozen organizers on apps.kde.com).  It's in KDE CVS and will be released as part of 3.2 or 1.1 is available at http://www.slackorama.net/oss/juk/.\n\nJuK does tagging, playlist organization, search, tag data guessing, will soon have MusicBrainz support (for looking up meta-data information by using a sample of the audio and comparing it to a database), oh, and it plays stuff too.  ;-)\n\n(Ok, and to come clean, yes, I am the author plugging my own software.  ;-) )"
    author: "Scott Wheeler"
  - subject: "Sorry"
    date: 2003-05-02
    body: "But I think that noatun is the most crappiest player I have used. It crashes, hard to use for newbies, and doesn't even know how to do streaming (PLS) right!\n\nI asked few of out local Linux group who uses KDE all the time - NONE of them liked noatun, and all of them are using XMMS now..\n\nSorry, but if there's a crown for the most crappiest player that I spent lots of time trying to use it - is noatun. \n\nNo offence charles...\n\nThanks,\nHetz\n"
    author: "Hetz h.dyndns.orgBen-Hamo"
  - subject: "Re: Sorry"
    date: 2003-05-02
    body: "Let me just add a 'me too' to that.  aRts sucks - it's inefficient, crashy, laggy, and skippy.  Noatun sucks - it's hard to use, crashy, has a stupid default playlist, and has a rediculous obsession with plugins.  Winamp uses plugins well - they are a good way to extend winamp's already good functionality without needing access to the closed source code.  Noatun uses plugins poorly - they are redundant, badly designed UI wise, don't have a good set of base features to build on, and aren't really necessary in the first place since everyone has access to Noatun's source to add whatever feature their little heart desires.  Plugins should not be used to implement an application's basic functionality.  Plugins should only be used to separate out piddly little features that are only useful for a few people (such as an alarm or a thing to put your currently playing song into your IRC chats), so the rest of us don't have to load the extra bloat.\n\nOh, and let me also add that comparing two mp3 players on the basis of how they sound is rediculous.  Especially when it is done by one guy who played all the songs himself, so he knew exactly where each song was being played.  It's amazing what hogwash audiophiles will believe about their precious audio.  Unless the decoders are buggy, the output of two different mp3 decoders is indistinguishable even in labratory conditions.  If this was a comparison of the sound-enhancing plugins available for each program, that would make a little more sense.  But comparing the sound of two players is just absurd."
    author: "not me"
  - subject: "Re: Sorry"
    date: 2003-05-02
    body: "I cannot agree more with you. Your are right concernig arts amd noatun. Noatun or kaboodle better than xmms? C'mon, give a break, XMMS _does_ support vorbis and mp3 streaming, for example. xmms is far older, and it shows, but noatun or kaboodle have a long way to go yet. "
    author: "Ricardo Galli"
  - subject: "Re: Sorry"
    date: 2008-08-27
    body: "That and it only plays about three formats."
    author: "anonymous"
  - subject: "Re: Sorry"
    date: 2003-05-02
    body: "me too.\n\nthat article was a pretty lame way to make noatun look good. \n\nthere is something fundamentally wrong with noatun/arts even before you take into account how unstable they are."
    author: "anon"
  - subject: "Re: Sorry"
    date: 2003-05-03
    body: "Not only Noatun is a crappy player, it's a crappy name.  Both are soo crappy, I barked each time I clicked on a song.  Thank goodness for xmms."
    author: "Booga blue"
  - subject: "Noatun skins"
    date: 2003-05-02
    body: "Why aren't there more skins fore Noatun on kde-look.org ?"
    author: "KDE User"
  - subject: "Re: Noatun skins"
    date: 2003-05-02
    body: "Because nobody actually uses Noatun, because it sucks.  Everyone tries it for a while, and then figures out that they were much happier with xmms, and goes back.  Want proof?  Just look at the screenshots on kde-look.org, count how many feature xmms (nearly all of them), and how many feature Noatun (I've just gone and looked at 10 or so, and I didn't see any)."
    author: "not me"
  - subject: "Re: Noatun skins"
    date: 2003-05-02
    body: "Who let the trolls come out again"
    author: "mETz"
  - subject: "Re: Noatun skins"
    date: 2003-05-02
    body: "Don't know why you cosider this trolling.\nKDE can only get better if you say frank and open what isn't good.\nNoatun / Arts is really awful. Noone in my company uses it though\nwe all really like KDE. \nIt's not about attacking without any reason. \n"
    author: "Michael"
  - subject: "Re: Noatun skins"
    date: 2004-01-16
    body: "Didn't find a reason in the text above"
    author: "mETz"
  - subject: "Re: Noatun skins"
    date: 2003-05-02
    body: "I don't think it's trolling at all.. simply, not many people use Noatun compared to xmms. "
    author: "lit"
  - subject: "Re: Noatun skins"
    date: 2003-05-02
    body: "If you are talking about the KJ\u00f6fol skins this is probably because the original player who invented this format is dead.\nI have links to some kj\u00f6fol skin-archives on my homepage:\nhttp://metz.gehn.net/index.php?sect=projects#KJ%F6fol\nI still have some ideas to create new skins but as I'm a rather bad artist it takes a lot of time ;)"
    author: "mETz"
  - subject: "XMMS has the best search function I have ever seen"
    date: 2003-05-02
    body: "One of the main reasons why i use XMMS is that it has the best search function i have ever seen in a player:\n\nthe j key gives you a list where you can type the name of the song or of the artist and XMMS will list everything which contains letters you typed in!!\n\ni find another player with this function i maybe will change the player...\n\nand i can minimize XMMS and place it on the top of my screen in always on tope mode so i have it always available in front even if i have other programs maximized.\n\nbut i read something about the hayes playlist in the text above so i will check it out today and have a look at it."
    author: "k8uenstler"
  - subject: "Re: XMMS has the best search function I have ever seen"
    date: 2003-05-02
    body: "Two words: JuK CVS"
    author: "Daniel Molkentin"
  - subject: "Re: XMMS has the best search function I have ever "
    date: 2003-05-03
    body: "Two words: Bloated Application.\n\n"
    author: "tomato"
  - subject: "Re: XMMS has the best search function I have ever seen"
    date: 2003-05-02
    body: ">i find another player with this function i maybe will change the player...\n\nWell I think winamp first had this feature, but I guess you aren't going to switch to that ;)  Seriously, though, this is a nice feature and I would like to see it in one of the playlists (preferably Hayes since that's the one I mainly use).\n\n>and i can minimize XMMS and place it on the top of my screen in always on >tope mode so i have it always available in front even if i have other >programs maximized.\n\nI would suggest the Young Hickory and Keyz plugins to implement this functionality.  Young Hickor will dock noautn in your panel so you can have it available even when other programs are maximized.  Keyz lets you bind keys for many things, including Forward, Play/Pause, and Back.  This way you can access noatun without even switch apps.\n\nBTW, these plugins are only so usefull because noatun has the ability to use plugins for some of its core functions.  (That was more in reply to one of the above posts about plugins)."
    author: "Jim Blomo"
  - subject: "Re: XMMS has the best search function I have ever "
    date: 2003-05-04
    body: "XMMS is very usuable, just hide the Windowm make it sticky (or not) and use the gamepad (/dev/js3 for me) or gkrellm-Plugin to navigate.\n\nAnd I can use all WinAmp2.x-skins but I really miss skins without corners like in ZINF or that crappy (hardly readable) one in Noatun."
    author: "Christian Schuglitsch"
  - subject: "Differences in decoding?"
    date: 2003-05-02
    body: "Why are there differences in decoding mp3s at all?  Unless I am missing something, they should all sound the same, and if they don't, one or more of them are buggy.  I can easily believe the reviewer conned himself into hearing differences where there were none, especially if it wasn't a double-blind test.\n\nOn the subject of artsd, it has never worked well for me, ranging from stupid amounts of CPU usage, to hanging the audio programs I use, to crashing completely, to failing to compile altogether.  The first thing I do after installing KDE is disable artsd.\n\nXMMS wins on audio quality for me every time.  That's because it's the only one that supports FLAC, the non-lossy codec that has recently been adopted by the Ogg project.\n\nEvery single KDE player I have used has seemed buggy, hideous, or lacking in features to me.  For instance, noatun crashed on startup every time I tried it until recently.  I've just loaded it up, and it doesn't look impressive at all compared to XMMS, loads of wasted space, badly thought-out toolbars (why on earth is the first toolbar icon a quit button, when I can just close the window?  Why are there separate play/pause buttons?).\n\nI would much rather use a KDE player than XMMS, because I dislike the gtk dialogs, and don't like the way loading files into the playlist completely blocks the UI.  But KDE audio has always seemed to me to be a botched job.  I hope this changes, but I'm not very hopeful.\n"
    author: "Jim Dabell"
  - subject: "Re: Differences in decoding?"
    date: 2003-05-02
    body: "Arts uses splay as decoder, xmms uses mpg123. xmms also does a lot of post-processing, like the volume slider, equalizer and so on. There is a lot of potential for rouding errors and similar things, so it's not surprising that there can be differences. \n\nAudio and especially video is still immature on free systems... in one or two years things will look better..\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: Differences in decoding?"
    date: 2003-05-02
    body: "1.  xmms doesn't do any post-processing itself related to the volume slider, it simply sets the sound card's hardware mixer volume.  Noatun (or aRts) does do post-processing to adjust the volume in addition to the sound card's hardware mixer, thus introducing the possibility of more rounding error.  If anything, Noatun loses here.  (but in reality, not audiophile-land, any rounding error would be far too small to hear)\n\n2.  The guy specifically said in the article that he disabled all mixers and extra plugins to compare the sound of the players directly.\n\n3.  mpg123 is an excellent mp3 decoder, agreeing with other good mp3 decoders up to the last bit of precision in decoded samples (http://mp3decoders.mp3-tech.org/objective.html).  If splay is different enough from mpg123 to hear the difference in a listening test, that means that splay is horribly buggy.\n\nAnyone who believes that Kaboodle/Noatun sounds \"better\" than xmms when playing mp3s with all equalizers and sound processing plugins disabled is only decieving themselves, as audiophiles often do."
    author: "not me"
  - subject: "Re: Differences in decoding?"
    date: 2003-05-02
    body: "In what context is video immature on free systems? For playing DivX files, I have a lot easier time then my friends who use Windows since they have to go download various codecs from all over the place. On Linux, install mplayer (and kplayer or kmplayer of course), and it supports everything (except some new .mov files like the Animatrix, since the audio codec isn't supported yet.) Weird problems like the video stopping and audio continuing are not uncommon on Windows. And the Linux players are better able to handle corrupt files in general. I think this is because whether you use mplayer or xine the developers feel responsible to have their players play as many type of files as possible and well. None of the players in Windows really seem to have this ethic, they just wrap around the Windows Media system for playing codecs."
    author: "Ian"
  - subject: "Re: Differences in decoding?"
    date: 2003-05-02
    body: "1. MPlayer (the current generation) is very focused on playing and simple recording, you cant do anything else with it. Like writing a video conferencing tool or a video editor... or, like iTunes does, broadcast streams to other computers. There is also no API, so an application can't really control the video and you can't, for example, get meta-information about files or just analyse them (file length etc). This is why KMPlayer is quite limited and not very responsive. MPlayer is a fine hack that solves the most urgent immediate problems, but it is not a long-term solution for video\n2. ... and don't forget that the Windows codecs that you use with MPlayer are not licensed for use on Linux with MPlayer and thus illegal. Most native video codecs violate patents. The MPlayer version that ships with Suse 8.2, which contains only legal stuff, is almost completely useless. There are very few video codecs that you can legally use, at least as long as Theora isnt finished, and they can not compete with the proprietary ones.\n\n"
    author: "Tim Jansen"
  - subject: "What do you know?!?"
    date: 2003-05-02
    body: "I've tried using Noatun and Kaboodle since the KDE 2.x days and have never been able to get them to play. Everything else seemed to work but not these two. I also always had difficulty navigating the interface so, I figured it could be me or it could be the app. Either way, I had no worries with other players so I just abandoned Noatun and Kaboodle.\n\nI'm using KDE 3.1.1 now and after reading this article I thought I'd give them another bash. As before, I had trouble with the interface. I opened, or tried to open, a local MP3 three times and it kept saying it was trying to play a .ram stream form the internet. It really didn't seem to be doing anything. On the fourth attempt I found the playlist which was FULL of stuff that I don't know where it came from. Mostly internet URLs. I found the MP3s I had just tried to open three times at the bottom of the play list and when I selected it, low and behold I finally got sound out of Noatun. Gleefully I jiggered around a bit with plugins and the equalizer. The equalizer, though basic, seemed to work but, plugins immediately disabled my sound.\n\nAfter listening to a track, I decided to compare the sound with XMMS. I had expected an MP3 played on the same hardware to sound identical, regardless of the player. To my surprise, it was immediately apparent that they sounded very different indeed. In this case XMMS was noticably superior in sound quality. I can't really explain why except for the possibility that XMMS is using OSS and Noatun is using aRTS, I dunno. \n\nAnyway, I am willing to concede that different people will like different things. For some Noatun will be the choice player while for others it will be something else. But, I do think that there is something in this thread that seems to be share by the majority of people. That is, a general dislike for the Notatun interface. It seems that there are a lot of people who do not like it and/or find it difficult to navigate, myself included. It does not seem intuitive and or easy to use.\n\nOf course my opinion is as subjective as anyone else's but, as there seems to be a general consensus that the interface is bad, and since KDE is all about interface and ease of use, perhaps the developers may wish to re-examin the interface design. "
    author: "Nobody Cares"
  - subject: "Noatun without arts?"
    date: 2003-05-02
    body: "Ok, the subject may sound trollish but I'm serious.\n\nThe article had me trying Noatun once again and it's unusable due to arts; arts uses more than 2% cpu on my athlonxp 1800 compared to 0.1% for xmms and even if that numbers are not correct (xmms using cpu-power some other way too, etc) it *feels* like they're correct.\n\nI'm currently compiling in the background and noatun skips every no and then or when I'm switching windows, desktops or even doing nothing. It's annoying like hell, we're not talking about mpeg4 in 1024x768 with 2000kbit but about mp3s!\n\nIf it's somehow possible to use Noatun without arts I'd probably use it (k-jofol is cool =) but with arts it's simply junk"
    author: "?"
  - subject: "Re: Noatun without arts?"
    date: 2003-05-03
    body: "> arts uses more than 2% cpu on my athlonxp 1800 compared to 0.1% for xmms and even if that numbers are not correct (xmms using cpu-power some other way too, etc) it *feels* like they're correct.\n\n<SARCASM>Yeah, that makes it unusable for me too. I mean, really... if I use artsd my CPU only idles 98% of the time instead of the usual 99.9%...</SARCASM>\n\nNo, what makes artsd unusable for me is the fact that it **sucks ass**. It has absolutely friggin' horrendous responsiveness rendering it completely useless for anything but the most trivial of applications (ie. playing a \"ding\" whenever an error occurs). Even switching songs in XMMS/aRts is annoying because of the .250s (or whatever) delay. All sound servers are evil, with the possible exception of JACK which has very good responsiveness, but unfortunately lacks application support."
    author: "i think not"
  - subject: "Re: Noatun without arts?"
    date: 2004-10-28
    body: "Same question. my arts crashes sometimes (amd64@gentoo). And it seems pointless have arts only for naotun. Can't use xmms, because it skips sometimes (mplayer, xine etc have no problem, so it's not a alsa problem). So I discovered noatun to be good substitute. Except, can i use it without arts?"
    author: "Fox"
  - subject: "Noatun Interface (look and feel) stinks"
    date: 2003-05-02
    body: "I like noatun and it sounds great esp. with \"extra stereo and other effects\". But Noatun Interface is very ancient, somewhere in 1980s... When usability did not matter...\n\nLook at windows media player 9. It sounds bad but wmp's Interface and playlist option is very nice. and even other things like effects, settings are more sanely placed. Noatun must be a integrated Multimedia Player...Noatun is very cumbersome to use... It should have a new interface. neither I like kjofol, xmms, etc plugins for noatun...\n\nBut Ugliness rules in KDE... "
    author: "Rizwaan"
  - subject: "Re: Noatun Interface (look and feel) stinks"
    date: 2003-05-02
    body: "WMP is by far the most ugly and unusable thing I've ever used so your points can't be taken serious at all. A stupid app (WMP) that cannot even easily save a playlist file somewhere."
    author: "mETz"
  - subject: "Re: Noatun Interface (look and feel) stinks"
    date: 2003-05-02
    body: "In addition to agreeing with Metz's statements, I'm not sure how you consider XMMS's interface to be that great.  Try asking a new user to open the playlist.  How quickly will they see the tiny button marked with \"PL\"?  How about getting to options?  Right clicking anywhere on the interface is not very clear way of accessing them.  I think you are confusing fancy looking with good interface.\n\nThat being said, one may like the interface provided my XMMS because it looks cool/they are used to it/it looks like winamp.  That's why noatun is able to use other interfaces to suit your desires.  You can use winamp skins. You can use kjofol skins.  You can use no window and just keep it docked."
    author: "Jim Blomo"
  - subject: "Re: Noatun Interface (look and feel) stinks"
    date: 2003-05-02
    body: "I like in XMMS that you can right click and choose \"Select Directory.\" My files are all organized in a Genre/Album (sometimes Genre/Artist/Album) directory structure, and I like to listen to whole albums at a time. Granted, in Noatun I can choose \"Open...\" and just select all the files in a directory. All the noatun playlist's are a little bulky that I've seen, but I can see the UI advantages it has over XMMS I suppose. I use the Winamp skin in noatun so it looks just like xmms. I wouldn't mind the simple Milk-Chocolate interface except you can't shade it up.\n\nHow do you have it just docked with no other UI? \n\nWhen I used XMMS I actually used the arts output plugin. For some reason, straight OSS sound doesn't really work on my emu10k1 system. It sounds like its overpowering the speakers even when the speakers themselves are relatively quiet (there is a similar effect when I turn up the Arts volume to around and above -15, I usually keep it about -24). "
    author: "Ian"
  - subject: "Re: Noatun Interface (look and feel) stinks"
    date: 2003-05-04
    body: ">My files are all organized in a Genre/Album (sometimes Genre/Artist/Album) >directory structure, and I like to listen to whole albums at a time.\n\nYou would definitly benefit from using the Hayes playlist by Neil Stevens.  Ithas been discussed in previous threads, or you can see it directly at URL http://www.freekde.org/neil/hayes/.\n\n>How do you have it just docked with no other UI?\n\nWhen using Young Hickory, click on the icon in your dock.  This toggles showing and hiding the main UI.  (This is actually standard KDE behavior for items in the dock.).  I'm not aware of any settings to make the UI hidden by default, although I suspect it is possible using DCOP.  Cheers,\n\nJim"
    author: "Jim Blomo"
  - subject: "I use XMMS with aRts"
    date: 2003-05-02
    body: "It rocks.  For all those of you saying aRts is unstable, well it hasn't crashed a single time for me and it multiplexes *all* my sound."
    author: "KDE User"
  - subject: "Noatun systray icon"
    date: 2003-05-02
    body: "I was listening to xmms when I read the article. So, I stopped xmms and started to play the same stream in noatun. I have to agree with the writer: it sounds better than xmms.\n\nSo, I switched back to my browser, and noticed a flashing icon in my eye corner. Obviously something important was happening. But no: it was simply noatun telling me that it was doing what it was designed to do: play a media stream. Great.\n\nI am NOT going to work 8 hour days watching a flashing icon in my systray.\n\nI'm back with xmms again."
    author: "Erik Hensema"
  - subject: "Re: Noatun systray icon"
    date: 2003-05-02
    body: "You can turn off the systray. And mine doesn't flash while playing, perhaps your using an older version? I seem to remember seeing some player do that at some point. I use Gentoo and KDE 3.1.1."
    author: "Ian Monroe"
  - subject: "Re: Noatun systray icon"
    date: 2003-05-02
    body: "You can turn off the flashing/blinking and still have it in the tray."
    author: "TomL"
  - subject: "KDE's multimedia is just 80% from perfect"
    date: 2003-05-02
    body: "Personally I think both noatun/kaboodle and xmms/xine bindings are not even close to be a viable solution, I would prefer if those all would be replaced with kmplayer/kplayer combo that use mplayer as it has much more supported fileformats and works just great with kde-3.1+..."
    author: "nobody"
  - subject: "Re: KDE's multimedia is just 80% from perfect"
    date: 2003-05-02
    body: "Mplayer is a bit overall kill for playing mp3 files.  I use kplayer for pretty much all video files, I wish it had a better playlist system though, for the 20 audio .wma's I have. \n\nA mplayer-audio-only plugin for one of those players wouldn't be a bad idea, to pick up all those other formats."
    author: "Ian Monroe"
  - subject: "Re: KDE's multimedia is just 80% from perfect"
    date: 2003-05-02
    body: "Yes it's an overkill but the point is that if mplayer would do all mpg/divx/mp3/aac/ogg etc. decoding/encoding (kmencoder sucks by the way as it bloats the memory) and have kmencoder used as konqueror plugin and kplayer as external (separate window) player would enable to dump whole bunch of binding code.\n\nHopefully kmplayer (why not kplayer too?!?) will be included to the kde-3.2 from kdeextragear-2... ;-)\n\nAnd then just to get k3b (embedded for konqueror too?!?) from kdeextragear-1... Heh!"
    author: "nobody"
  - subject: "plugins"
    date: 2003-05-02
    body: "For me, mostly all is resumed to plugins.\nCan I play nsf (nes sound format), spc (snes sound), psf (playstation sound format) in noatun? No.\nCan I get functionality of noatun with some xmms plugins? Yes.\n\nSo the choise is clear.\n\nAnd besides, Noatun is slooooow."
    author: "Iuri Fiedoruk"
  - subject: "XMMS"
    date: 2003-05-02
    body: "Yes, there are slight differences in XMMS vs. Noatun mp3 decoding quality, Noatun does sound slightly better, but both of them have trouble recognising some broken? mp3 files, whilst the other works.\nA good reference for this would be comparing to Winamp (it's had a lot of time to improve its' playback quality.. and does require a lot more cpu than XMMS for example), or the even better decoding quality of tools like Sound Forge and Cool edit pro.\n\nBut as an audiophile, what are you doing listening to mp3s?\nogg/vorbis being a 4th generation codec sounds a _lot_ better at a smaller filesize\nand the decoder for it is built into libvorbis, so it oughta sound the same whichever player you choose. (there is no reason not to choose ogg/vorbis anymore since 99% of players support it)\n\nand FLAC... well, it always sounds like it should, so hopefully all players will have flac playback possibilities by default soon. (it's how I store samplecds, my own material and anything else sensitive to the ear)\n\nI still mainly use XMMS for the simple reason that many of the plugins I need for it aren't available for Noatun and hopefully this will be fixed in the near future.\n(although, I think the Noatun playlist using a native gui is better than the XMMS counterpart)\n\nExamples are the following:\n\nShoutcast broadcasting\n\nCrystality plugin (to enhance the sound of mp3) and no, it's not trivial BS, most mp3 compression programs use low/high pass filters leaving a laughable frequency range for the end material.\n\ndumb-xmms plugin (IMO the best sounding mod/s3m/xm/it playback library)\n\nvarious plugins for snes/nes sound chip instruction playback (ie. authentic sounding console music)"
    author: "Funk"
  - subject: "I forgot about the 1# reason, JACK output"
    date: 2003-05-02
    body: "JACK (http://jackit.sourceforge.net/) is the reason linux may one day become a viable solution for people in proffessional audio business, and hopefully arts will be dropped in favour of JACK or adopt the same kind of architecture (needing a total rewrite from scratch I presume)\n\nLow latency is not a luxury, it's a requirement if anyone is going to ever be able to do any serious audio work on linux & variants.\n\nJACK implements just this, and it is ready and more than good to go."
    author: "Funk"
  - subject: "Sound quality"
    date: 2003-05-03
    body: "How can Noatun and Kaboodle produce \"better\" sound? If I play an MP3 in Noatun and XMMS, they both use the same MP3 decoding algorithm! They both send the decoded stream to the same audio device! So how can one player produce better sound than the other if both the source and output are the same?"
    author: "Ac"
  - subject: "arts just sucks"
    date: 2003-05-04
    body: "arts just sucks. it can't use hardware mixing, if i'm compiling something while playing an mp3, arts skips. playing a game, arts skips, dragging a window, arts skips.\n\nits bloody useless."
    author: "arts_sucks"
  - subject: "aRts could use some attention indeed...."
    date: 2003-05-04
    body: "- It doesn't support hardware mixing, even though a lot of soundcards do (mine does, when using ALSA)\n- Even when I'm not playing *anything* at all, artsd still uses 1.5-2.5% CPU time /all the time/ on my Athlon XP 2100+ . That is *NOT* ok.\n- Even when set to play on 'realtime priority' (in the control center), it skips all the time, e.g. when you switch to another window, or start a compile job. Yes, I do have a kernel with the realtime patch applied, and xmms does not have this problem.\n-When it does mixing, CPU usage is even a lot worse. Note that this should not have to happen, because most soundcards have supported hardware mixing for a long time now...\n- My pentium 2 333 MHz could play MP3's at 4-5% CPU usage, so you're not gonna tell me my XP 2100+ still needs 5% CPU power to play the same MP3, right? It should be more like 0.4% by now...\n\nSo these things basically render any KDE-based sound player unusable at the moment. I guess that's why everone is using XMMS (I know I am!)\n\nI hope some KDE developers will read this thread, and acknowledge the need to take some action on the kde-multimedia front, esp. the aRts back-end :-)"
    author: "Wilke"
  - subject: "Re: aRts could use some attention indeed...."
    date: 2003-05-04
    body: "Just did a little experiment to prove this problem:\n\n- Running aRts, using ALSA for output, realtime priority, playing a 192 KBit MP3, takes about 4.5% CPU time constantly.\n\n- Running XMMS with ALSA output, I often can not even find it in top, but when it's there it uses like 0.1-0.2% CPU time, as expected.\n\nXMMS users should take note that turning of the option (in Preferences->Options) \"Smooth title scroll\" will save a lot of CPU cycles! I disabled the scrolling songtitle entirely (it's useless anyway), so now I just see the first ~ 30 chars of the songtitle. Disabling the equalizer visualisation helps, as well.\n\nWhat I'd like to know is: what is aRtsd doing in these 4.5% CPU cycles anyway? I mean by simply looking at the numbers above it looks like it could decode the MP3 20 times over in that amount of time!"
    author: "Wilke"
  - subject: "Re: aRts could use some attention indeed...."
    date: 2003-05-04
    body: "I agree completely. I have personally raised this problem on various forums already several years ago. Unless working with network transparency mode of ARTS ( what I never do)  - I don't need it. What for ? Sound stream mixing? Aren't 32 streams in hardware enough for non-audio-professional home user? \nUnified API - maybe. Network transparency - maybe. But give me hardware mixing."
    author: "Alex"
  - subject: "I don't care about this obsolete codec generation"
    date: 2003-05-04
    body: "Vorbis & MPC are the ways to go :-)\n\nI don't like that artsd uses about 10 - 15% of my Athlon 900 and didn't even work 2 years ago when I started with Linux and had a p133.\n\nGive me Noatun without artsd for OSS and espicially Alsa-modules.\n\nI don't know what Noatun does with the soundfiles while it uses the processor that much.\n\nAnd well, there's a arts-plugin for xmms. It's worth a test :-)\n\n\nSchugy"
    author: "Christian Schuglitsch"
  - subject: "After a few thoughts about the ultimate integrated"
    date: 2003-05-04
    body: "multimedia system for KDE:\n\nAt first I want to mention there might be some security issues but it's about usability:\n\nHave a look where you're in contact with multimedia:\n\nwebbrowsing\ninstant messaging\nsound and video encoding, decoding, broadcasting\ncd/dvd burning\n\nWebbrowsing is quite ok but sometimes Konqueror crashes after clicking too fast, what isn't page related. And there seem to be some bugs when using the tree view (was moving files into subdirectories and suddenly Konqueror moved them into my home-dir what wasn't visible in that /LAN-tree).\n\nInstant messaging: I use SIM and the original AOL-AIM for linux. Well, I need that different design to recognize the network and I don't mind three contact-lists for aol, icq and gabber.\n\nIf I watch a DVD or listen to vorbis files and want to discuss with my IMcontact about it, I would like to send a 100kbit-video-stream. I choose \"send stream\" and get a list of noatun(s) stopped with playlists full with mp3, vorbis, xvid, swf, cdda://, vcd:// , dvd:// ...  and alternative buttons like \"open file\", dvd-browser, vcd-browser, cdda-browser [Perhaps the file isn't in that list or noatun isn't open].\nPerhaps I get a new video-window or I have it docked into the wm-window (100kb streams aren't big), 3 lines for the dialog and my line to enter new text.\nSome people would even like to encrypt everything :-)\n\nSound&Video-player:\nWell, perhaps you don't want to send streams via IM but want to make a server.\nBy now I use icecast2 and darkice for vorbis and I have to disable ICQ-sounds that are send to /dev/dsp and will be broadcasted too. I have no clue about video. \n\nI want to be able to mark some files in the playlist and toggle them on and off via shortcuts.\n\nartsd is annoying, I can disable the xine border in icewm, noatun always has a border and a title bar in other windowmanagers. Looks very strange when using the k-j\u00f6fol-skin.\n\nEncoding:\nI still like to encode all files @ the commandline, it's fast and safe.\nI don't like programms that use cdda to grab files and I don't want to lose the cdparanoia output. Sometimes I choose another drive to grab CDs with errors\n\n\nBurning:\nCD-Text is essential, it should support cdrdao-toc- and cue-files.\nI want to choose whether I decode oggfiles first and write them to disk or burn them on the fly.\n\nHave fun"
    author: "Christian Schuglitsch"
  - subject: "JACK instead of ARTS"
    date: 2003-05-04
    body: "Don't get me wrong, ARTS is great work, and I have not experienced any trouble with it.\nAbout the hardware mixing, I disagree, ARTS can guarantee a quality mix doing it in software, and hardware should be optional but not default since 90% of pc soundcards suck.\nThe cpu cycles used for this are trivial IMO.\n\nBut, arts is not good enough, it has the severe problem of extremely high latency, which makes it unsuitable for any kind of professional audio work.\n\nSign me up on the list of JACK instead ARTS\nhttp://jackit.sourceforge.net/"
    author: "Funk"
  - subject: "Re: JACK instead of ARTS"
    date: 2003-05-05
    body: "About the hardware mixing, I disagree, ARTS can guarantee a quality mix doing it in software, and hardware should be optional but not default since 90% of pc soundcards suck.\n-------------------------------------------------------------------\nSo define some configurable list of cards that do suck and cards that not suck, and make hardware mixing DEFAULT for cards that don't suck. I personally don't think that 32-channel mixing of SB Live sucks for non-professional / semi-professional home use. There is no reason to overload the CPU when there already is a dedicated chip for a particular task at hand."
    author: "Alex"
  - subject: "Re: JACK instead of ARTS"
    date: 2003-05-06
    body: "So you'd want GFX cards to use 8-bit blenders for 32bit graphics as well then?\nIt's the same thing here, they offer hardware digital mixers with unacceptable algos (creative etc)\nor in all too many cases, the mix enters the analogue domain (creative again)\n\nIf you think the cpu wins and sound quality loss gains are acceptable then fine.\n32-64 channel mixing oughta be no problem, I used to do it on a 386 in dos (trackers)\nso unless you've got some weird need of 2000 channel mixing, I don't see what the problem is.\n\nGood cards:\nRME\nM-Audio (Midiman)\nMOTU\nand the list goes on..."
    author: "Funk"
  - subject: "Re: JACK instead of ARTS"
    date: 2003-05-06
    body: "Uh, wtf? On modern SoundBlasters (basically, anything past the SB AWE), mixing is 100% digital and handled within the DSP directly. Naturally, sound that is from analog inputs, such as lien-in, microphone input, and cd audio would have to pass through a ADC. This is how it works on non-consumer oriented sound cards as well.\n\nMost of the quality loss exhibited by modern SoundBlasters is the fact that it handles all wave audio at 48kHz. Most audio is NOT sampled at this rate, and thus must be resampled. However, for most consumer related, this isn't exactly a problem. It only becomes one if you are doing sound editing. I don't see many people who are using Noatun or xmms doing that :x. Anyway, this behavior is exhibited irregardless of if you are doing hardware or software mixing. "
    author: "fault"
  - subject: "Re: JACK instead of ARTS"
    date: 2005-10-30
    body: "ir\u00b7re\u00b7gard\u00b7less   Audio pronunciation of \"irregardless\" ( P )  Pronunciation Key  (r-g\u00e4rdls)\nadv. Nonstandard\n\n    Regardless."
    author: "Another Alex"
  - subject: "xmms rocks"
    date: 2003-05-09
    body: "I agree with everybody else here, I like xmms better than noatun.\n\nIt starts up faster, runs better (uses less CPU), crashes less, looks better and it's easyier to use.\n\nAnd I don't like artsd or esd, a decent soundcard does the same on hardware and works better.\n\nIt would be ok to use artsd or esd for network transparency (like mad), I don't know if artsd has this capability, but anyway, I don't need it."
    author: "Alan"
  - subject: "How about Audacious?"
    date: 2006-01-27
    body: "I prefer Audacious Media Player. It's a second generation XMMS descendant via Beep Media Player and it's like XMMS and Beep combined without the bugs.\n\nhttp://audacious-media-player.org/Main_Page for those who are interested.\n\nI can't even consider things like JuK because they lack support for Amiga Modules (MOD, XM, IT, S3M, etc.), Chiptunes (SPC, GYM, HSC, NSF, etc.), MIDI, and other such esoteric formats.\n\nIt helps that Audacious has what is apparently the best-sounding MP3 decoder in existence. I also like the in-progress conversion to format detection by headers rather than extensions and the planned switch to LADSPA effects and libvisual visuals."
    author: "Stephan Sokolow"
---
There's a <a href="http://www.linmagau.org/modules.php?op=modload&name=Sections&file=index&req=viewarticle&artid=114&page=1">story</a> (<a href="http://www.linmagau.org/modules.php?op=modload&name=Sections&file=index&req=printpage&artid=114">print version</a>) on <a href="http://www.linmagau.org/index.php">linmagau.org</a> reviewing sound quality between  <a href="http://noatun.kde.org/">Noatun</a>, <a href="http://freekde.org/neil/kaboodle/">Kaboodle</a> and <a href="http://www.xmms.org/">XMMS</a>.  Not surprisingly, Noatun and Kaboodle come out on top, with the author stating that <i>"I'd recommend the Noatun/Kaboodle solution to most audiophiles and people with decent sound gear."</i>  Unfortunately, the author didn't take the time to discover some of the other features and plugins that make Noatun a better all around player.  An interesting, if audio-centric, read.  
<!--break-->
<p>
<i>[<b>Ed</b>: Of course, this is really a review of <a href="http://www.arts-project.org/">aRts</a>, <a href="http://www.kde.org/areas/multimedia/">KDE Multimedia</a> and all those Open Source audio libraries involved.  Noatun, Kaboodle and even XMMS win automatically.]