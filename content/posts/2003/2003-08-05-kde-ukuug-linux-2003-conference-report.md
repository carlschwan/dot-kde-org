---
title: "KDE at UKUUG Linux 2003 Conference Report"
date:    2003-08-05
authors:
  - "jriddell"
slug:    kde-ukuug-linux-2003-conference-report
comments:
  - subject: "Community event"
    date: 2003-08-04
    body: "Nice to read news about a community event while another DE reports about a contributing company being acquired by an old fashioned commercial company."
    author: "Anonymous"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "Is this sour grapes I detect here. I think you don't like that GNOME seem to be getting more and more coporate backing than KDE. Now Novell.\n\nA few days ago, SUN announced it will become the standard desktop for Solaris. Must be painful I know.\n\nBut, what they wanted was probably really Evolution and Connector. Now they can manage to migrate people off Exchange much better. (Caryy on using your Exchange with Evolution, then take out exchange, drop Groupwise in, and the end users should hardly notice). Plus Redcarpet, nice way to manage huge deployments on Linux which they would probably do if they manage to get defections. PLus they get a company with much expertise in UI coding. Novell gets a good (extremely good) front end. Plus Mono. Seriously, who doesn't want to tap into the future pools of coders brandishing their C#/.NET certs. Novell certainly doesn't want to discover it can't hire 90% of coders because they now all use .NET. So actually buy a company actively developing an implementation of .NET, and you will not be left out of that.\n\nSeems to make sense on many levels. PLus Novell doesn't have to pay anyone else. No licensing a toolkit form anyone (read Trolltech). They can actually get to influence development of GTK/GNOME by contributing to GTK/GNOME. Me thinks Trolltech *may* (note the may) just have to revise its licensing. But then again, they cannot, because that is their bread and butter. But they need mindshare, and GTK/GNOME has it all right now it seems."
    author: "Anonymous Coward"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "Company support is good, company involvement taking over development direction in most cases not: Look at GNOME, it's targetting a specific audience while having lost all of its configurability to make it fit for other audiences too.\n\n> A few days ago, SUN announced it will become the standard desktop for Solaris. Must be painful I know.\n\nThey announced this years ago. Must have been painful for Sun until they recently got GNOME 2.0(!) mature enough for inclusion."
    author: "Anonymous"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "I meant GNOME@. PLease, if KDE was good enough in its raw form they would have included.\n\nTo include it in Solaris, they had to work on it themselves to give it features they would have found useful. This is just like Ximian doing XD2"
    author: "Anonymous Coward"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "They wasted 3 years on GNOME and many of their internal developers even hate it."
    author: "ac"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "That's not really fair.   In every group of people, there will always be different opinions.\n\nOften it seems as though in any group of 3 people, there are 5 positions on any point of contention.   \n\nI would have been very surprised if all Sun developers liked GNOME.\n\nWhile *I* greatly prefer KDE, I don't expect everyone else to.  And I admit that I really like Bluecurve."
    author: "TomL"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "Btw, they do include KDE in the Solaris CDs."
    author: "ac"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "That's the best analysis of the Novell/Ximian event I've read yet."
    author: "TomL"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "How great it is for Novell.  How sad it is for GNOME."
    author: "ac"
  - subject: "Re: Community event"
    date: 2003-08-05
    body: "First the commercial (trolltech, Qt) influence was considered _evil_ to KDE.\n--> Issue solved. [http://kde.org/whatiskde/qt.php]\n\nNow GNOME (more/less started out as an alternative to then 'half commercial' KDE) suffers a lot under commercial influences.\n\nHow to cure GNOME? is the question I ask my self...\n\nThis \"more and more coporate backing\" you talk about, Mr. A.C., doesn't seem so nice to me. GNOMErs don't seem so happy with their end product anymore either. I suggest GNOME developers take hold of it and release on their own, without corporate intervention.\n\nJust my .02 (EUR) ;-)"
    author: "cies"
  - subject: "Re: Community event"
    date: 2003-08-06
    body: "What do you mean cure GNOME. How does GNOME suffer. From having a well written HIG. From having paid developers working fulltime on it. From having good accessibility suite and good usability tests. No, I don't think so. I think the groundwork is being laid down for greater things later."
    author: "Aonymous Coward"
  - subject: "WOW THANKGS!"
    date: 2003-08-05
    body: "Thanks for all the information and scrnshots, this is some very cool stuff! And, Umbrello seems cool too, never actually tried it, I'm just learning C++. but can it actually genreate diagrams from my code or do I draw them and if so is there any support for python 2.3?"
    author: "Alex"
  - subject: "Re: WOW THANKGS!"
    date: 2003-08-05
    body: "It mostly goes the other way around. You draw your diagrams in Umbrello and it generates the code for you. At the moment we have support for several languages like C++, Java, Python, Perl, PHP, Ada... I think till next major release along with KDE 3.2, there will be improved code generation handling. For more information see: http://nvo.gsfc.nasa.gov/Umbrello/\n\nSteinchen"
    author: "Steinchen"
  - subject: "Re: WOW THANKGS!"
    date: 2003-08-05
    body: "I agree, this is a wonderful report."
    author: "ac"
  - subject: "Together"
    date: 2003-08-05
    body: "The UML Modeller presentation is really interesting http://jriddell.org/programs/umbrello/html/\nJust a few informations about Together ( http://jriddell.org/programs/umbrello/html/x192.html ) because it's the best development software I have used so far. The synchronization of the source code with the UML diagram is simply amazing!\n\nYou code something and automagically the UML diagram appears, if you modify the UML diagram, the code is also modified transparently.\nFor example you want to rename a class, just do it in the UML diagram and the corresponding source code will be renamed. You want to copy-past a class? it takes 2 seconds no more.\nIn the upper part of the screen you have the UML diagram, below you have the source code editor.\n\nEverybody knows that UML is great for programming, it's like creating a house with 2D and 3D plans: it's easier than just reading a text describing the futur house. You get the big picture more easily. Also you can catch errors very easily. UML is simply an higher level tool that helps you a lot especially in a team: it's easier to understand what your friend has done with a diagram rather than just the source code.\nThe problem with UML is that you have to maintain the UML diagram and the source code up-to-date. With Together there is not such a problem since everything is synchronised without doing anything special.\n\nI'm dreaming about a free software with such functionalities for the free software community because it will help to make much better programs in much less time.\nMaybe one day UML Umbrello will do that with the help of KDevelop ;)"
    author: "Tanguy Krotoff"
  - subject: "Re: Together"
    date: 2003-08-05
    body: "It is a nice dream and I welcome you very much to join the Umbrello developers, so that we can get faster to a free real UML based CASE tool! Please see: www.umbrello.org\n\nSteinchen"
    author: "Steinchen"
---
The <a href="http://www.ukuug.org/events/linux2003/">UKUUG Linux2003 Conference</a> took place in Edinburgh last weekend.  I ran the KDE stall, gave a talk on <a href="http://uml.sourceforge.net/index.php">Umbrello UML Modeller</a> and attended the other talks. Read on for the whole report.


<!--break-->
<p>The KDE stall was helped along by Eilidh the booth babe and Kenny the booth boy (<a href="http://www.duffus.org/photos/200307UKUUG/cimg0009">photo with me in middle</a>).  This was a technical conference so everyone knew what KDE was but people were interested in some of the new applications such as <a href="http://www.slackorama.net/cgi-bin/content.pl?juk">JuK</a> and <a href="http://www.koffice.org/kexi/">Kexi</a>. <a href="http://kplayer.sourceforge.net/">KPlayer</a>, while not part of KDE itself, impressed everyone by being a media player with a useable interface.  We also demonstrated the <a href="http://kolab.kde.org/">Kolab server</a> to a couple of people interested in using it for their clients. People were asking if t-shirts were on sale but unfortunatly we were unable to get hold of them, many thanks to the <a href="http://www.kdeleague.org/">KDE League</a> though for supplying t-shirts to wear. Our neighbours on the <a href="http://www.debian.org/">Debian</a> stall had their usual impressively large turnout, we had to keep an eye on them to stop them spreading onto our stall!</p>

<p>David Faure briefly popped into the conference to <a href="http://www.duffus.org/photos/200307UKUUG/cimg0016">talk about KDE Development</a> (<a href="http://www.ukuug.org/events/linux2003/prog/abstract-DFaure-1.shtml">slides</a> and <a href="http://www.zzoss.com/weblog/index.php?p=135">a writeup</a>). He talked about <a href="http://developer.kde.org/~sewardj/">Valgrind</a>, kdialog (for creating dialogues for bash scripts), the <a href="http://www-105.ibm.com/developerworks/education.nsf/linux-onlinecourse-bytitle/A96500917FA1DB0786256BC000808C0F?OpenDocument">simplicity of KParts</a> in KDE 3, <a href="http://developer.kde.org/documentation/tutorials/automation/index.html">using DCOP</a> and the <a href="http://developer.kde.org/development-versions/kde-3.2-features.html">new features coming in KDE 3.2</a>. He ran out of time to discuss unsermake (can we finally get rid of automake?!).</p>

<p>Bo Thorsen gave a talk on <a href="http://kroupware.kde.org/">Kroupware</a> (<a href="http://www.ukuug.org/events/linux2003/prog/abstract-BThorsen-1.shtml">slides</a>) which included a successful live demonstration. Several people were heard to whisper that this could be a killer-app for GNU/Linux on the desktop.</p>

<p>Commented on as "the best talk I've been to all weekend" was my own UML, Free Software and <a href="http://uml.sourceforge.net/index.php">Umbrello UML Modeller</a> talk (<a href="http://www.duffus.org/photos/200307UKUUG/cimg0052">photo</a>, <a href="http://jriddell.org/programs/umbrello/">slides</a>). The plan with Umbrello is to get Free Software developers using UML where it makes sense - to sketch and share program structure ideas and to document current program structure to help new and old developers.</p>

<p>On Saturday evening Maddog, who I had inspired to wear a kilt, gave a talk using a Sgian Dubh as a pointer (<a href="http://www.duffus.org/photos/200307UKUUG/cimg0041">photo</a>) on automated piano players. Apparently the music industry of the time complained of being put out of business by these playerless pianos.</p>

<p>It was a fun and useful event, thanks to <a href="http://www.ukuug.org/">UKUUG</a> and my brother for making it happen. It would have been good to be more organised and have some posters and handouts for the stall but I'll get that right for next year.</p>

