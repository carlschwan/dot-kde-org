---
title: "Scribus 1.0 Released"
date:    2003-07-19
authors:
  - "Dre"
slug:    scribus-10-released
comments:
  - subject: "props to Franz"
    date: 2003-07-18
    body: "This is a fantastic killer app that is doing much to promote KDE and Qt.  I would not be surprised if the likes of RedHat decided they had to install this application by default.  That would mean installing Qt at the same time.  =)\n\nThanks Franz!!"
    author: "anon"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "well, it's not a KDE app, though.\n"
    author: "That guy!  But who is that guy?"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "Which makes me wonder why I have to read about it here, instead of Freshmeat. Or even apps.kde.com. Seems rather unfair to apps like Quanta and KWord. Hopefully we're not going to fall in the GNOME habit of calling everything that happens to share one or two dependencies with KDE a KDE application.\n"
    author: "Rob Kaper"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "Referring to Qt as \"one or two dependencies\" is a little bit extreme.  I would love to see the source try after you remove the Qt methods.  An app in Qt is considerably similar to an app in KDE - though I definately hate giving up the File Dialog."
    author: "Paul Seamons"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "um - source tree"
    author: "Paul Seamons"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "Quanta and KWord do DTP? That's news to me."
    author: "Anonymous"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "No, they are not. But you should know that many people are using Quanta for very strange things, so I can imagine that one is using for DTP...\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "> > Quanta and KWord do DTP? That's news to me.\n> No, they are not. But you should know that many people are using Quanta for very strange things, so I can imagine that one is using for DTP...\n \nQuanta is Getting better with DocBook. I know of one person doing a document definition package for Povray. (and you didn't think we did graphics.) BTW PDF is a sort of mark up language, though not a real friendly one."
    author: "Eric Laffoon"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "Scribus intergrates with KDE via Drag and Drop, as well as inheriting KDE desktop\nthemes."
    author: "Anonymous"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "> Which makes me wonder why I have to read about it here\n\nAren't you a dot.kde.org editor? Then you should know result of internal discussion if any happened - or why didn't you start one instead of moaning now? Let me guess, your own \"KDEnews\" site doesn't run well so you're just bored."
    author: "Anonymous"
  - subject: "Re: props to Franz"
    date: 2003-07-19
    body: "No, I am not an editor.\n\nNo, I was not bored. I watch DVDs, program, or take photography when I'm bored.\n\nYes, that site doesn't run well, but only because it ended up low on my list of priorities. The sites that I do actively work on run just fine actually.\n"
    author: "Rob Kaper"
  - subject: "Re: props to Franz"
    date: 2003-07-19
    body: "Rob : Don't forget to mention that what slowed down kdenews.unixcode.org was that it didn't have corporate patronage and was running off of your DSL, and that you had to pull the plug on it so that you could eat and stuff."
    author: "Neil Stevens"
  - subject: "Re: props to Franz"
    date: 2003-07-20
    body: "And don't forget to mention your private kdenews@unixcode.org list."
    author: "Navindra Umanee"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "So are you coming around on secrecy?  Or is just a disengenous remark designed to shout down critics?"
    author: "Neil Stevens"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "I'll stack unixcode's openness against this site's any day.\n\nunixcode's queue of submitted articles has always been open.  Where's yours, huh?\n\nunixcode's standard of posted articles was documented on the site.  Where's yours, huh?\n\nunixcode never modified people's posts and called posters names, while keeping the modifying editor anonymous.  What can you say?"
    author: "Neil Stevens"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "Maybe your site is so great because it does nothing?"
    author: "Navindra Umanee"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "> unixcode's standard of posted articles was documented on the site. Where's yours, huh?\n\nDo you see that link called \"faq\" at the left?"
    author: "Anonymous"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "Alias, used for technical administration. Not editing or moderation. And actually, that's just a forward to kdenews@lists.capsi.com which has always been unmoderated and open for subscription through kdenews-subscribe. Or use anoy of the other ezmlm-idx options:\n\nTo get messages 123 through 145 (a maximum of 100 per request), mail:\n   <kdenews-get.123_145@lists.capsi.com>\n\nTo get an index with subject and author for messages 123-456, mail:\n   <kdenews-index.123_456@lists.capsi.com>\n\nTo receive all messages with the same subject as message 12345,\nsend an empty message to:\n   <kdenews-thread.12345@lists.capsi.com>\n"
    author: "Rob Kaper"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "Pretty cool."
    author: "Navindra Umanee"
  - subject: "Re: props to Franz"
    date: 2003-07-20
    body: "Well you were at some point...  I guess we didn't announce that you left."
    author: "Navindra Umanee"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "Glad to see at least some unworthy news bits don't make it. ;)\n"
    author: "Rob Kaper"
  - subject: "Re: props to Franz"
    date: 2003-07-22
    body: "Implying that this article you're making such a fuss about wouldn't have been posted on KDEnews?  Seems to me your site has published Qt-only stories."
    author: "Navindra Umanee"
  - subject: "Re: props to Franz"
    date: 2003-07-19
    body: "Gee, an anonymous detractor calling for secrecy.  What a poor, but by now expected, way for kdenews.org to be run."
    author: "Neil Stevens"
  - subject: "lame"
    date: 2003-07-20
    body: "anon. coward...heh..\ncya marcel (synth-worldz.de)"
    author: "Marcel Partap"
  - subject: "Re: props to Franz"
    date: 2003-07-18
    body: "> Which makes me wonder why I have to read about it here, instead of Freshmeat. Or even apps.kde.com. Seems rather unfair to apps like Quanta and KWord.\n\nI don't mind so much. I really hope it continues to get better. Unfortunatly from the little I've done with it the early interface seemed very unintuitive and I was unable to get large text to do line spacing correctly. It would just overwrite the text on the next line. Maybe I think on the wrong level because you may need to set line height independenty with a publsing app, but I couldn't find the setting. I've also really wondered why he didn't release it as a KDE app. File dialogs and other things are better. Still it has a lot of potential and I was able to do things with it I was not able to with other apps. It produced beautiful printed documents too, but it really would have been nice to have the KDE print system used automatically instead of futzing around.\n\nI'm looking forward to trying out this release. As far as it being posted here, it may not be a KDE app, but I think it's of some interest to a lot of KDE users. Frankly I wish it was in KOffice, but there's not doubt they've done a good job at a lot of features you can't get elsewhere.\n\nI'd also like to announce that I'm not developing a PDF package for Quanta. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: props to Franz"
    date: 2003-07-20
    body: "First: very good work Franz!\n\nHave you ever used it?  Or any real DTP application?\n\nYou can do DTP with a word processor.  WordPerfect does an excelent job with DTP type documents.  HOWEVER, doing a DTP document on a true DTP application is a completly different process.  You write things with a text editor with NO formating and then import them into the DTP app.\n\nPerhaps some KDE developers would like to help Franz with a native KDE version.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: props to Franz"
    date: 2003-07-19
    body: "this reminds me of the steadily reoccuring Gnome discussions which apps are actually Gnome apps. we obviously have to keep up with Gnome in that regard so let's start dozens of discussions as well since we have so much time to spare =P"
    author: "Datschge"
  - subject: "Re: props to Franz"
    date: 2003-07-19
    body: "There's a difference between GNOME and KDE.  GNOME was founded to kill KDE.  KDE was founded for UI consistency.\n\nFor KDE contributors to hyperventilate over apps that don't even take the basic necessary steps to integrate with KDE betrays the reason KDE exists."
    author: "Neil Stevens"
  - subject: "Re: props to Franz"
    date: 2003-07-19
    body: "Contributors \"to hyperventilate over apps that don't even take the basic necessary steps to integrate with KDE\" aren't contributors to KDE, easy as that. I really don't see a need to start a discussion with strong words about something this obvious.\n\nThat being said I wish Scribus much success and hope its selling point (fully ISO compilant PDF/X-3 export including transparenty, CMYK separations, ICC color management) can be integrated into KDE libs sometime."
    author: "Datschge"
  - subject: "Re: props to Franz"
    date: 2003-07-19
    body: "Red Hat dose install QT by default, if you don't want it you have to remove it after instalation. In anacondas GUI install you can't chose not to install it. "
    author: "Me"
  - subject: "It has a wonderful pdf exporter!"
    date: 2003-07-18
    body: "When its pdf exporter will be used by every app? It is great!"
    author: "Michelinux"
  - subject: "sKribus"
    date: 2003-07-18
    body: "And what about sKribus ?"
    author: "Murphy"
  - subject: "Re: sKribus"
    date: 2003-07-19
    body: "Probably when you convice someone to change the name also to Kuanta ;-)"
    author: "uga"
  - subject: "Re: sKribus"
    date: 2003-07-19
    body: "> Probably when you convice someone to change the name also to Kuanta ;-)\n\nOh! We tried that,  but I couldn't kwit spelling it with a kue. ;-)\n\nPS Strangely I can't Quit captializing Q's either. It's Quite a Quandry.\n\nPPS Actually we were going to use \"Kuato\" from the movie Total Recall... \"Start the reactor... Free Mars...\""
    author: "Eric Laffoon"
  - subject: "Re: sKribus"
    date: 2003-07-19
    body: "No, I mean a real Kde version..."
    author: "Murphy"
  - subject: "MDI"
    date: 2003-07-18
    body: "I see that Scribus is using an MDI interface. Does anyone know if you can run it in SDI mode as well? MDI is being depreciated and is generally considdered a bad UI choice. It doesn't let you interleave your Scribus documents with documents from other programs, nor does it let you spread documents over multiple desktops."
    author: "Andr\u00e9 Somers"
  - subject: "Re: MDI"
    date: 2003-07-18
    body: "I like MDI interfaces. No matter what everyone else says.\nBTW, AFAIK there is a MDI class in development which\nallows undocking of windows so they can be moved to\nother virtual desktops if necessary.\nScribus is really cool. Kudos to the author for his work!!!\nWhat would make it an absolute killer app though would be\nthe ability to at least import from Quark and/or InDesign.\n\n"
    author: "Martin"
  - subject: "Re: MDI"
    date: 2003-07-18
    body: "It looks pretty good to me!  No need to change what's not broken.  Give it a try if you haven't..."
    author: "Anonymous"
  - subject: "Re: MDI"
    date: 2003-07-19
    body: "I like mdi, too."
    author: "Carlo"
  - subject: "Re: MDI"
    date: 2003-07-19
    body: "> MDI is being depreciated and is generally considdered a bad UI choice\n\nKeep in mind that in many cases, apps that have MDI interfaces are generally accepted to be more intuative to use than pure SDI interfaces. Take Photoshop versus GIMP (yes, even GIMP 1.3), or Quanta in MDI mode versus Bluefish (which is an alright editor, but has a horrid interface compared to Quanta.)\n\nOnly by a very small amount of entities/corporations/people shun MDI (e.g, Microsoft, Apple, KDE/GNOME)  .. the rest of the world uses MDI when it makes sense (many, many, many, many, many Windows applications)\n\nI'm generally ambivalent about MDI myself. After riding the wave of MDI-hate that filled the UNIX desktop world for the last few years, I saw a lot of the nice parts of MDI with tabbed browsing in konq/moz/opera and in many MDI apps (such as Photoshop and Quanta)\n"
    author: "anon"
  - subject: "Re: MDI"
    date: 2003-07-19
    body: "Yes, The GIMP is a real problem. When they ctreated an alternative Menu, such as Jasc Paint shop, it may get a real dent in the market. simply because users want to feel at home. alternative modes for advances user... let's give more choice to the desktop... \n\n\nUnfortunately there is not KDE paint program that competes with The Gimp. "
    author: "Gerd"
  - subject: "Re: MDI"
    date: 2003-07-19
    body: "I used to love the Gimp. The more complex stuff I started doing the more I began to loathe the interface. Try doing a bump map with several images that have 20 layers each open. The stupid pick list drops your choice off the screen. And forget editing the 20 photos you just took without first finding the toolbox on the taksbar and the layers dialog. Then forget about recursing to the color menus. The gimp is clearly the greatest proof that avoing MDI at all costs is utterly insane. I detest the interface on some tasks.\n\n> Unfortunately there is not KDE paint program that competes with The Gimp. \n\nThat's largely because the Gimp is not a paint program. It's an image manipulation program. ;-)\n\nBelieve me I'd love to fix it, but I only have so many hours and so many dollars. I would dearly love to take on an additional project like Krita, but it would require some serious sponsorship from somewhere to free up that time and especially to get backbone development support like we have with Quanta. Maybe next year I will strike it rich and be able to plug in another 10-20 hours a week and another developer. Anyway I hope next year the core Quanta application will reach a less demanding state as the features are realized to where I can ponder if this is as insane as it probably is.\n\nOf couse someone else could give up their free time and resources for this project. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: MDI"
    date: 2003-07-19
    body: "You'd better teach it The GIMP...\nMDI, SDI, ...\n\nGIMP 2.0 will be released soon together with the HURD Kernel. I will supposed to be be GUI indenpendend..."
    author: "Gerd"
  - subject: "I hope it gets packaged properly"
    date: 2003-07-19
    body: "I don't think I have a single KDE or Qt application that has it's full complement of icons ...\n\nSo many toolbars full of little white boxes!"
    author: "OSX_KDE_Interpollination"
  - subject: "Re: I hope it gets packaged properly"
    date: 2003-07-19
    body: "\"Use the source Luke\"\n;-)"
    author: "Eric Laffoon"
  - subject: "What's with this?"
    date: 2003-07-21
    body: "\"You need a version 4+ browser to see these images which are png's.\"\n\nI've used three browsers recently.  Firebird 0.6, Mozilla 1.4, and Konqueror 3.1.2.  They are all capable of displaying PNGs.  Why must people make these stupid statements about what I must use to view a page, when it's quite obvious they don't know what they are talking about?"
    author: "Jim"
---
Franz Schmid, who has been diligently working on the excellent Desktop Publishing application
<a href="http://web2.altmuehlnet.de/fschmid/">Scribus</a>
(<a href="http://home.comcast.net/~scribusdocs/scriscreen.html">screenshots</a>) for over
two years, this week
<a href="http://www.graphics.com/modules.php?name=News&file=article&sid=1780">announced</a>
the first stable release.  Available in 17 languages, Scribus is billed as "the first
open source DTP application capable of generating professional 'press-ready' results."
To mark this milestone release, <a href="http://www.osnews.com/">OSNews</a> has
<a href="http://www.osnews.com/story.php?news_id=4064">published a review</a> of Scribus 1.0.
In addition, <a href="http://slashdot.org/">Slashdot</a> has a
<a href="http://slashdot.org/article.pl?sid=03/07/15/2143210&mode=thread&tid=106&tid=152&tid=185">thread
on the release</a>.
<!--break-->
