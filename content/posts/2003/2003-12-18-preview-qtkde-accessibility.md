---
title: "A Preview of Qt/KDE Accessibility"
date:    2003-12-18
authors:
  - "hfernengel"
slug:    preview-qtkde-accessibility
comments:
  - subject: "*cough*"
    date: 2003-12-18
    body: "Naming a link \"here\" should be known to be very bad usability/accessibility-wise. =P"
    author: "Datschge"
  - subject: "Re: *cough*"
    date: 2003-12-18
    body: "Thanks for correcting it, and regarding the article: great stuff. =)"
    author: "Datschge"
  - subject: "Re: *cough*"
    date: 2003-12-18
    body: "Is it correct?  Shouldn't a hyperlink look like \"subject of link\" and nothing more, like \"Apples\" or \"Picture of an apple\" rather than \"Apples can be found here, along with other pages about various fruit and vegetables and comments and other cool stuff\"?\n\nHypertext has been around a long time guys... let's get it right :)"
    author: "Lee"
  - subject: "Re: *cough*"
    date: 2003-12-18
    body: "I've wondered about this myself. The problem is that sentences require verbs. So I guess something like \"<a href=''>Screenshots</a> are available.\" would make sense.\n\nHaving \"here\" be a link is just bad form in general. I know I often scan pages and read the links; \"here\" doesn't tell me much.\n\nAdjectives and adverbs have been around a while as well... doesn't mean folks will stop mixing up well and good. Similarly \"here\" links will be around for a while."
    author: "Ian Monroe"
  - subject: "Re: *cough*"
    date: 2003-12-18
    body: "Well, when I posted my first comment only \"here\" was part of the link. Having half of the sentence as link is not ideal, but still way better than just \"here\"."
    author: "Datschge"
  - subject: "Re: *cough*"
    date: 2003-12-19
    body: "Still wrong and out of spec, though ;)"
    author: "Lee"
  - subject: "Re: *cough* - Actually"
    date: 2003-12-19
    body: "Actually cause we are on the subject the link \"more information and screenshots can be found here\" does not say \"what\" i will find if i follow the link. So it would be more clear and good for scanning the page if it would state something like: \"More <link>information and screenshots of Qt Accessibility to ATK Bridge </link> can be found ....\"\n\nBut nonetheless great work and thanks for your work.\n\n"
    author: "coward"
  - subject: "Re: *cough*"
    date: 2003-12-19
    body: "I think KDE needs a HIG, just like GNOME, and a clear aspiration to reduce clutter! I therefore vote that links be simplified to a much more generic form. The full stop is an ideal character to symbolise a hyperlink, and it provides us with a wonderful metaphor. Think of the web as a collection of documents, and hyperlinks as pieces of string, connecting them that users can follow. As such, the full stop actually is an accurate visualisation of the piece of string from the end on.\n\nI think that users will really be able to get behind this new metaphor and that developers should work hard to make this idea (\"string theory\") a reality.\n\n.\n"
    author: "Dawnrider"
  - subject: "Re: *cough*"
    date: 2003-12-19
    body: "j/k, btw, in the OSNews form :)"
    author: "Dawnrider"
  - subject: "Re: *cough*"
    date: 2003-12-19
    body: "And there I was about to raid your house and force you to use only GNOME for the rest of your life.\n\nIn all seriousness though, for real hyperlink accessibility you should use the title attribute..."
    author: "Eldhrin"
  - subject: "sweeet!"
    date: 2003-12-18
    body: "Herald, that's like some of the awesomest stuff!  Go KDE!"
    author: "KDE User"
  - subject: "Glib and ATK..."
    date: 2003-12-18
    body: "Is it possible to do this without Glib and ATK - such as Qt having its own native solution? Is this part of the future plans and this only a temporary solution?"
    author: "David"
  - subject: "Re: Glib and ATK..."
    date: 2003-12-18
    body: "Qt already has its native solution, look at the KDE accessibility project for infos how to use it. Sun has put a considerable amount of work into accessibility and this bridge allows Qt/KDE apps to be accessible by those tools as well (next to the native KDE tools). Qt apps will be as accessible as JAVA, Mozilla, OpenOffice...."
    author: "Harald Fernengel"
  - subject: "Re: Glib and ATK..."
    date: 2003-12-18
    body: "Good stuff then."
    author: "David"
  - subject: "Snapshots early 2004"
    date: 2003-12-18
    body: "Does this means it's part of Qt 4, or an extension for Qt 3.3, or usable even with Qt 3.2?"
    author: "Anonymous"
  - subject: "Re: Snapshots early 2004"
    date: 2003-12-18
    body: "It will certainly be part of Qt 4. There is a chance that it will feature in Qt 3.3 already."
    author: "Waldo Bastian"
  - subject: "Sun's ATK?"
    date: 2003-12-18
    body: "I thought ATK was a GNOME project.  Guess not.  But this IS the same ATK library that GNOME takes advantage of, right?  So they'll be fully interoperable, as far as accessibility goes?"
    author: "Lee"
  - subject: "Re: Sun's ATK?"
    date: 2003-12-18
    body: "Yes, full interoperability is the goal here."
    author: "Waldo Bastian"
  - subject: "Re: Sun's ATK?"
    date: 2003-12-19
    body: "Well... every accessibility stuff was done by Sun, the mozilla MAI stuff, the OpenOffice accessibility integration, the Java Bridge, atk and at-spi. Okay, they host it in the gnome cvs, but all copyright headers reference Sun."
    author: "Harald Fernengel"
  - subject: "Re: Sun's ATK?"
    date: 2003-12-19
    body: "OH.  That's a shame, actually.  I was all proud of GNOME and the wider OpenSource movement for actually getting that whole thing done well.  Still, at least it's done.  And it's great to see KDE going a similar route.  This stuff is important, and doesn't get enough attention! :D"
    author: "Lee"
  - subject: "Re: Sun's ATK?"
    date: 2003-12-19
    body: "I think public money shall be spent on it. this is work that has to be paid by the governments."
    author: "Heini"
  - subject: "Re: Sun's ATK?"
    date: 2003-12-20
    body: "They do more or less. Some credit should go to the pressure that governments have exerted via their purchasing power. At least its an educated guess that ADA requirements and their counterparts outside the US was at least part of the incentive for development. "
    author: "Ian Monroe"
  - subject: "Re: Sun's ATK?"
    date: 2003-12-20
    body: "Done by Sun does not imply that it is not part of GNOME. Sun supports GNOME and is member of the GNOME Foundation. And they chose it as their new official desktop environment for Solaris. So, if they take part in developing GNOME, and developed ATK as their part in that development, ATK is part of GNOME. \n\nThe fact that it requires Glib should be another hint..."
    author: "Sun/GNOME/ATK"
  - subject: "Re: Sun's ATK?"
    date: 2003-12-20
    body: "> Sun supports GNOME and is member of the GNOME Foundation.\n\nThe GNOME foundation's bylaws doesn't allow a company to become a member.\n\n> The fact that it requires Glib should be another hint.\n\nSo aRts is part of GNOME?"
    author: "Anonymous"
  - subject: "Thanks!"
    date: 2003-12-19
    body: "Nice Christmas present Harry!! Thanks a lot :)\n\n"
    author: "Roberto Raggi"
  - subject: "Re: Thanks!"
    date: 2003-12-20
    body: "Any on-screen keyboard? Thats what I need."
    author: "AC"
  - subject: "Re: on screen keyboard."
    date: 2003-12-20
    body: "There is actually more than one, and you may be able to use one right now, depending on what exactly you need. If you just need text input, try out viki (it's in kdenonbeta); you may have to combine it w/kmousetool from kdeaccessibility, to make input easier, depending on your needs. Through the atk bridge, the gnome onscreen keyboard may be usable; that one has slightly more features, including capturing UIs to button grids, but it's of course not clear whether it works right, since the bridge is not yet avaiable\n\n(There is also a sort of third option, klavi, which I wrote, and which uses a patched Qt to talk to Qt accessibility interfaces directly; that has basic input and menu + toolbar capture, but it's too much of a prototype to likely be of much use)\n"
    author: "Sad Eagle"
  - subject: "Re: Thanks!"
    date: 2003-12-21
    body: "There are several options, depending on your needs.\n<p>\nViki from kdenonbeta is a small utility that shows a keyboard using the currently configured keyboard layout / language. To make it useful, you should it combine it with the \"sticky keys\"-feature in the KDE Control center (see devices / keyboard), and maybe with kmousetool from the KDE accessibility package. If you wish to try this out, please ask for further information on kde-accessibility@mail.kde.org\n<p>\nAnother option is gok from the GAP (GNOME Accessibility Project), which much bigger and is very feature rich. It allows you to define your own keyboard layouts and works with any X application. For applications that support ATK another great features is mpossible: The integration of the applications' user interface into the on-screen keyboard. Once the bridge is completed and KDE has been extended to fully use Qt's Accessibility Framework, perfect interoperability will be possible here, but that still needs to be done.\n<p>\nIn any case, if you have further questions, please join out mailing list at <a href=\"https://mail.kde.org/mailman/listinfo/kde-accessibility/\">\nhttps://mail.kde.org/mailman/listinfo/kde-accessibility/</a>\n<p>\nOlaf Jan Schmidt<br>\n(KDE Accessibility Project)"
    author: "Olaf Jan Schmidt"
---
With the new Qt-ATK bridge, Qt/KDE applications will integrate seamlessly with existing assistive technologies on GNU/Linux desktops as well as other Unixes that support Sun's accessibility framework. First snapshots can be expected in early 2004, <a href="http://trolls.troll.no/~harald/accessibility/">more information and screenshots can be found here</a>.



<!--break-->
