---
title: "Gwenview 1.0.0 Released"
date:    2003-12-08
authors:
  - "Photographer"
slug:    gwenview-100-released
comments:
  - subject: "Hmm.."
    date: 2003-12-08
    body: "I don't know if its a coincidence, but this looks a lot like Mosfet's PixiePlus. PixiePlus 0.5 is awesome, if this is anywhere as good as it than its very good."
    author: "Mario"
  - subject: "Re: Hmm.."
    date: 2003-12-08
    body: "Well, PixiePlus is a dead project, Mosfet has dissapeared and has even deleted the project from most well known repositories! All it's left is a screenshot of it: http://www.sweb.cz/samba/screen/pixie.png\nAnyone's got a URL from his latest source code? 0.5.4 I think it was?\n\nI would say that Gwenview is s lot alike to gThumb, both really nice apps."
    author: "Photographer"
  - subject: "latest pixie"
    date: 2003-12-08
    body: "Look at the SuSE download pages. They still ship it.\nRegards"
    author: "Sebastian"
  - subject: "Re: Hmm.."
    date: 2003-12-08
    body: "what is mosfet upset about now? and where has he gone to?"
    author: "a.c."
  - subject: "Re: Hmm.."
    date: 2003-12-08
    body: "Debian also still seems to ship it."
    author: "daniel"
  - subject: "Re: Hmm.."
    date: 2003-12-09
    body: "Try this as source for 0.5.4 \n\nftp://ftp.tu-chemnitz.de/pub/FreeBSD/ports/distfiles/pixieplus-0.5.4.tar.gz\n\nKonstruct has pixie in apps/pixie also.\n\nOsho"
    author: "Anonymous"
  - subject: "Re: Hmm.."
    date: 2003-12-08
    body: "In fact I think the docked interface in Gwenview predates PixiePlus.\n\n...Checking project release file archives...\n\nYes, it was introduced in v0.2 which was released on 2001-01-08. Well maybe I should make more noise on public places, but I can't stand the marketing cap too long :-)"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Gwenview is great.."
    date: 2003-12-08
    body: "been using it for a few months now. It's a wonderful and quite feature complete program. Now that it seems stable, I really hope it gets moved from kdeextragear to kdegraphics. A nice kuickshow mode would be nice of course ;-)\n\nKDE, imho, has needed a good general purpose image browser since Pixie was removed (erm.. kde 2.1?). Kuikshow is a nice app, and I don't think many people realize it's hidden functionality, but:\n\n- it's harder to use- I think most people think kuickshow is basically kview.\n- it uses imlib\n- it seems less active in development than it used to be. I don't think carsten pfeiffer is active as he used to be, and coincidently, kuickshow hasn't been either :(\n\n\n"
    author: "anon"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-08
    body: "I agree. I've been looking at a few of the various directory-oriented image viewers (pixie, kuickshow, showimg), Gwenview looks to have pretty much everything. I used Kuickshow for a few weeks, but it doesn't browse zip files nor does it have a 'fullscreen' mode, the closest I found is starting a slideshow with a large time interval. \n\nWith gwenview, the only feature lacking when it comes to normal browsing is that zoom-in always zooms into the middle, it should do one of those things where the cursor is changed to a magnifing glass and zooms-in on where the image is clicked.\n\nThe sky is the limit when it comes to possible features related to image editing, but image editing features should remain simple. The only feature missing currently is that the rotates aren't done to all currently selected images. A program that can do batch rotating solves about 95% of all my image editing needs. PixiePlus could, but it destroyed EXIF tags in the process, that was to be fixed in the next version, which I think is the one that never came out."
    author: "Ian Monroe"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-08
    body: "I see I'm not the only person looking for a good rotation program. I'm excited to see all this activity on KDE digital camera tools...\n\nFYI - Kuickshow does fullscreen quite nicely, just hit \"Enter\" to toggle in and out of it. For veiwing purposes I've actually been very happy with Kuickshow.\n"
    author: "Anony Guy"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-08
    body: ">>>I used Kuickshow for a few weeks, but it doesn't browse zip files nor does it have a 'fullscreen' mode<<<\n\nActually, it does both.\n\nAs was already stated, hit <enter> to toggle in and out of fullscreen mode.\n\nKuickshow _does_ browse zip files (and any kio supported transport) it just doesn't offer .zip files in its browser view. This clearly makes the feature somewhat hidden, but if you enter an URL in kuickshow's location bar in the same format that konqueror shows when you have browsed into a .zip archive, you will notice that kuickshow also shows the contents of the archive and lets you view any images therein.\n\nI have used kuickshow with kio_fish, for example, and it works very well. :)\n\n"
    author: "teatime"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-08
    body: "> I really hope it gets moved from kdeextragear to kdegraphics\n\nA third image viewer within kdegraphics!? Keep on dreaming..."
    author: "Anonymous"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-08
    body: "> A third image viewer within kdegraphics!? Keep on dreaming...\n\nOf course, you realize, it'd take moving the current two to other places (two we need currently because kuickshow doesn't do everything kview does, and kview isn't as capable as kuickshow/gwenview/pixie is)\n\nkview-> kdeblackhole  - Gwenview does everything kview did.   \nkuickshow->kdenonbeta - Unless Carsten/somebody else is willing to maintain it in kdeextragear.. I don't think Carsten has much time for KDE development anyways though. Kuickshow is plenty stable, but I don't see much room for future development there unless somebody is willing to pick it up again. Only a few minor features were commited between 3.1 and 3.2, for example. "
    author: "anon"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-08
    body: "Problem with dropping Kuickshow is that Gwenview still does not do everything Kuickshow can do, especially in the image editing area (contrast, brightness, gamma editing for example). It would be annoying for some users to loose some features."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-09
    body: "Yes, I'm rather short on time since summer. I'll be fixing some bugs during christmas holidays and maybe even add some features for the next release (I finally have a digital camera, so my hdd is going to be flooded with images :)\n\nI'm quite concerned about the kwin/qt situation -- current KuickShow only works properly with the patched qt-copy. I really hope, those patches will make it into Qt 3.3. Any news on this, Lubos?\n\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "Re: Gwenview is great.."
    date: 2003-12-08
    body: "> - it uses imlib\n\nWell, it is bad to have such an external dependency, but it is good to have an image viewer that is so fast and not memory hungry (it was able to load images still leaving some spare MB when ee fill my memory and crash not having enough, just don't speak of kview).\n\nKonqueror should be using kuickshow to display images."
    author: "renaud"
  - subject: "KDE 3.2 Beta 2"
    date: 2003-12-08
    body: "Does anybody know anything about it?\nAccording the release schedule it should've been released last Thursday."
    author: "george"
  - subject: "Re: KDE 3.2 Beta 2"
    date: 2003-12-08
    body: "You can find it at address ftp://ftp.kde.org/pub/kde/unstable/\n"
    author: "KDE user"
  - subject: "Image viewers - clashes."
    date: 2003-12-08
    body: "First I have to thank you for this great piece of software. I build it from CVS last night and now it's my picture viewer of choice.\n\nBut I think there are, at the moment, too many image viewers under the kde repository. Many functions are duplicated, so, when i want to view pictures I don't know if it's better to do that with KuickShow, or with KView, or with ShowImg, or with Gwenview. (or with Pixie, or ..., ... ).\nIt resembles the pain I have when i have to open a text file and I don't know if use Kate or Kedit or KWrite ... ^_^\n\nSo, why doesn't the authors of those great image viewers join themselves and try do develop the 'best' image viewer ? Clean design, clean interface, customizable in look, fast and simple.. those are the goals. And then KDE will have it's own complete image viewer, not too many small programs.\n\nWhat do you think about it?"
    author: "E.Ros"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-08
    body: "IMHO the framework should provide a central location for filter plugins shareable over all image viewers."
    author: "Ruediger Knoerig"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-08
    body: "\"\"\"So, why doesn't the authors of those great image viewers join themselves and try do develop the 'best' image viewer ? Clean design, clean interface, customizable in look, fast and simple.. those are the goals. And then KDE will have it's own complete image viewer, not too many small programs.\"\"\"\n \nThis is a praiseworthy goal, but it's not that simple. For example \"Clean interface, customizable in look\" and \"simple\" are not easy to put together. Another problem is that some viewers try to do lots of things (Pixie, Showimg), others want to stay simple (Gwenview, Kview). This make it not really easy to merge.\n\nStill, I agree there should be more cooperation. For example I know that Pixie, Showimg and Gwenview all use QXCFI to read XCF files, but they all include it in their code, rather than relying on it to be installed as a Qt image plugin, the way it was meant to be. Using QXCFI as an image plugin means *all* Qt/KDE applications would be able to load XCF files, and patches would not need to be copied from viewer to viewer. I think the next major release of Gwenview will do it this way, maybe providing QXCFI as an easy to build (automake/autoconf instead of QMake) package alongside."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-08
    body: "``So, why doesn't the authors of those great image viewers join themselves and try do develop the 'best' image viewer ? Clean design, clean interface, customizable in look, fast and simple.. those are the goals. And then KDE will have it's own complete image viewer, not too many small programs.''\n\n  We've already chat about that Aur\u00e9lien and I but, as he said, the goals are differents. \n\n  The other question would be: who will accept to see its prog 'disappeared' into another one? Or maybe we have to make a poll..."
    author: "Richard Groult (ShowImg author)"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-08
    body: "> The other question would be: who will accept to see its prog 'disappeared' into another one?\n\nMy guess would be many people, as long as it 'disappears' into something better (or you can call the result GwenImg ;)  ). Gwenview and ShowImg are _so_ similar (well, from the user's point of view) that the only difference I can see are differences in missing features that I'd like to use, well, and some bugs. If there was an image viewer that could do what Gwenview+ShowImg can do, that'd be pretty close to my ideal image viewer (both of them alone seem to be the best image viewers for Linux that I've seen though, only surpassed by ACDSee).\n\nBut it's your decision of course. Cooperation with others comes at a price, and it's up to you to decide if it is or isn't worth dividing your users base. But note that some of those users could be also potentionally contributors - who should get my bugreports, wishes and possibly patches, if I don't want to bother spending time finding out which of the two so similar solutions is better?\n"
    author: "Lubos Lunak"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-08
    body: "Hum... After spending some time to think about it, I think Lubos has a point. It's too bad to divide our user base and, now that Showimg is in KDE Extra Gear, to make the translators do the same work twice. Therefore I think merging is a good idea.\n\nHowever, I do not want to loose the aim of keeping the image viewer simple: I'm willing to merge, but I think lots of features should be kept as separate programs, integrated through something like the external tools in Gwenview. I'm thinking about things such as batch conversion and renaming (see http://www.krename.net), slideshow, html gallery generation (see http://www.virtualartisans.com/kde/andras/en/kallery.htm), image acquisition...\n\nThis does not mean \"throw away the code\", but rather \"turn some parts of the code as separate programs\". This way the main program stay small, users can replace some tools with other they prefer or they can use the tools alone.\n\nPS: I post this here because I would like to have Richard as well as user opinion about this idea."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-09
    body: "``...  to make the translators do the same work twice. Therefore I think merging is a good idea. ''\n  it's more than a translation problem I think :)\n\n`` However, I do not want to loose the aim of keeping the image viewer simple: I'm willing to merge, but I think lots of features should be kept as separate programs, integrated through something like the external tools in Gwenview. ''\n  Well... you'd like to merge while keeping 2 separate progs ?\n\n`` This does not mean \"throw away the code\", but rather \"turn some parts of the code as separate programs\". This way the main program stay small, users can replace some tools with other they prefer or they can use the tools alone. ''\n  The problem is for users who want to have all the features: they'll have to download a lot of separate progs... it's boring. Look at all these progs which need 10 progs (as DVDRip... :) if you want all the features: you have to found all the corrects versions then compile and install them...\n\n  I think it's better to have only one complete prog, and smaller ones with less features.\n"
    author: "Richard Groult (ShowImg author)"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-09
    body: "\"\"\"it's more than a translation problem I think :)\"\"\"\nAgreed :-)\n \n\"\"\"Well... you'd like to merge while keeping 2 separate progs ?\"\"\"\nNo, I'd like to merge the core functionality: browsing the file system for images. The extra features can be separate programs. This does not mean these programs *must be* downloaded separately. Some of them could be included in the source archive, but compiling the source would create multiple binaries: one for the image viewer and several others for the various tasks, with some default configuration to make it easy to run the external tools from the viewer.\nThis way users get a viewer that is quite complete, but the binaries stay small and they can easily replace some of our included external tools with other ones, if they wish to."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Image viewers - clashes."
    date: 2003-12-08
    body: "\"\"\"It resembles the pain I have when i have to open a text file and I don't know if use Kate or Kedit or KWrite ... ^_^\"\"\"\n\nThe difference in this case is that in KDE you will find two programs: KView and Kuickshow. Then, if their features do not fulfill your needs, you can install a third-party image viewer: PixiePlus, Showimg, Kimdaba, Gwenview or maybe GQView or GThumb. I would say that the \"only\" problem are file associations: right now I think no image viewer associates itself with images once it's installed and it's quite painfull to manually associate it with all image types.\n \n"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Thanks!"
    date: 2003-12-08
    body: "I'm the author of Gwenview, Thanks for the nice article! I was surprised by the number of bug reports in my mailbox this morning. I guess I just found the explanation :-)\n\nAs for multiple image rotation, I never thought it would be useful, but again I don't own a digital camera. I will try to add this."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: Thanks!"
    date: 2003-12-08
    body: "Multiple image rotation would be handy for me, too. \n\nI have a 35mm film camera, and I usually get my photos scanned to CD (Fuji Photo CD, it's just an iso9660 CDROM with hires and lores images) at the same time as they are developed.  There's no EXIF information there, of course, so all the portrait shots need rotating, and sometimes the lab really messes up and all the images are upside down on the contact sheet and on the CD.\n\nCheers for producing such a nice piece of software.\n\nWill"
    author: "Will Stephenson"
  - subject: "jpeg?"
    date: 2003-12-08
    body: "Hm, looks nice, but it doesn't display any jpegs here!? It reports a size of 0x0 and shows a black image.\nAny clues?\n\n-js"
    author: "-js"
  - subject: "Re: jpeg?"
    date: 2003-12-08
    body: "This often happens when Qt is not compiled with the correct jpeg options.\nCan you see your jpegs in Konqueror or KView?"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: jpeg?"
    date: 2003-12-08
    body: "Yes, I can view them in konq, but now I realize, that konq does not generate thumbnails for jpegs. I'll try to rebuild Qt.\nThanks!\n\n-js"
    author: "-js"
  - subject: "Nice... but much slower than gqview"
    date: 2003-12-08
    body: "I have been using gwenview off and on for quite\na while now. I like some of the features, and this\nis definitely the best image viewer for KDE.\nHowever, this is still not a patch on gqview.\n(http://gqview.sourceforge.net)\nIn particular, it is MUCH slower. In fact,\ngqview under KDE runs a lot faster than gwenview\nunder KDE, though the former is not a KDE app.\n\nIf this program could be improved to be comparable\nto gqview in speed and ease of file manipulation,\nit would be a wonderful app.\n\nMagnus.\n\n\n"
    author: "Magnus Pym"
  - subject: "I would like to add"
    date: 2003-12-08
    body: "Most people don't really need a picture viewer as Konqueror does it. But by adding image manipulation functions to Gwenview as plugins this may create a true \"image program\"  via the bootstrapping method of development. We already know from kpart how fruitful a plugin module system may be.\n\nI think the method of slow growth is better than the big shoot approach."
    author: "Jupp"
  - subject: "XCF Support: THANK THE UNIVERSE THERE IS A PLAN!"
    date: 2003-12-09
    body: "Using the latest CVS debian packages which Orth makes Gwenview always crashes upon touching .xcf files as opposed to ignoring them--how it doesn't just result as an unknown file type and try not to render is beyond me but now that it can that makes life so much easier.\n\nI hope Konqueror either ignores .xcf or uses the same API.\n\nMy luck its been fixed and I just have to wait for Beta 2 debs.\n\nGreat Job everyone!\n\nMarc"
    author: "Marc J. Driftmeyer"
  - subject: "Re: XCF Support: THANK THE UNIVERSE THERE IS A PLAN!"
    date: 2003-12-09
    body: "Have you tried Gwenview 1.0.0? This bug was present until 1.0.0pre4 and has been fixed in 1.0.0."
    author: "Aur\u00e9lien G\u00e2teau"
---
<A HREF="http://gwenview.sourceforge.net/home/">Gwenview</A> is a simple image viewer for KDE. It features a <A href="http://gwenview.sourceforge.net/screenshots/show_shot.php?shot_id=7">folder tree window</a> and <A href="http://gwenview.sourceforge.net/screenshots/show_shot.php?shot_id=8">a file list and thumbnail window</a> to provide easy navigation of your file hierarchy, and uses <A href="http://gwenview.sourceforge.net/screenshots/show_shot.php?shot_id=13">docked windows</a> that allows you to alter the layout in any way you want (<a href="http://gwenview.sourceforge.net/screenshots/">screenshot page</a>).  Gwenview can load and save all image formats supported by KDE, but Gwenview can also browse GIMP .xcf files thanks to the <a href="http://www.lignumcomputing.com/qxcfi/index.html">QXCFI component</a> developed by <a href="http://www.lignumcomputing.com/">Lignum Computing</a>. When browsing JPEG images with EXIF information, Gwenview even automatically rotates them according to the EXIF Orientation tag.  See <a href="http://gwenview.sourceforge.net/overview/">here for a full overview</a> of the features in this release.
<!--break-->
