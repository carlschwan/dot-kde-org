---
title: "Kastle 2003: Arrival and KDE e.V. Membership Assembly"
date:    2003-08-22
authors:
  - "binner"
slug:    kastle-2003-arrival-and-kde-ev-membership-assembly
comments:
  - subject: "Image comments"
    date: 2003-08-22
    body: "Can someone please put up the name of the person/people that are in the photos.  Being how many of them are known by there handle to us.\n\n-Benjamin Meyer "
    author: "Benjamin Meyer"
  - subject: "Re: Image comments"
    date: 2003-08-22
    body: "They're added with the time and when the names are known."
    author: "binner"
  - subject: "How many ?"
    date: 2003-08-23
    body: "\"large numbers of developers \" -> You have the numbers, I mean, X from German, Y from France, Z from Belgium, W from Brasil, ..."
    author: "Tony di Lorenzo"
  - subject: "Re: How many ?"
    date: 2003-08-23
    body: "Isn't that a bit, umm, old-fashioned??\nIt's just like 20 Italians, 6 Germans and 3 French died in \nan Airplane Crash... I mean they are DEAD in the first place\naren't they?"
    author: "Mike"
  - subject: "Re: How many ?"
    date: 2003-08-23
    body: "Is it old-fashioned to originate from a country? "
    author: "Apollo Creed"
  - subject: "Re: How many ?"
    date: 2003-08-23
    body: "It is old-fashioned to focus on it."
    author: "CE"
  - subject: "Re: How many ?"
    date: 2003-08-23
    body: "wuh.... me don't care...\nhope they're having a good time and makin new friends from all over the world. And at least they've come together to actually _work_ together (kind of a kde task force?) I am amazed how many contributors show up at \"the kastle\"..."
    author: "Thomas"
  - subject: "Re: How many ?"
    date: 2003-08-24
    body: "From a month year old registration data, newer changes are not known to me:<br>\n<br>\n41x Germany<br>\n10x Czech Republic<br>\n&nbsp;7x Netherlands<br>\n&nbsp;6x Austria<br>\n&nbsp;5x USA<br>\n&nbsp;4x England<br>\n&nbsp;4x France<br>\n&nbsp;3x Norway<br>\n&nbsp;2x Canada<br>\n&nbsp;2x Spain<br>\n&nbsp;1x Brazil<br>\n&nbsp;1x Denmark<br>\n&nbsp;1x Israel<br>\n&nbsp;1x Scotland<br>\n&nbsp;1x Sweden<br>\n&nbsp;1x Ukraine<br>"
    author: "binner"
  - subject: "Re: How many ?"
    date: 2003-08-24
    body: "Hm, I'm also here. A Hungarian guy from Romania. ;-)"
    author: "Andras Mantia"
  - subject: "Good luck, folks!"
    date: 2003-08-23
    body: "Hope you all have a good time over there and can hack with smoking fingers. One week of so many KDE hackers together is bound to deliver awesome improvements. \n\nSeeing forward to them\nUwe\n"
    author: "Uwe Thiem"
  - subject: "Software Patents"
    date: 2003-08-23
    body: "Related to the Vote on September 1, can you as developers sent a strong message from your conference to the EU parliament?\n\nWill KDE.org take part in the NET-demonstration?\nhttp://swpat.ffii.org/gruppe/demo/"
    author: "Wurzelgeist"
  - subject: "Re: Software Patents"
    date: 2003-08-23
    body: "Good point. I'd like to see KDE support this fully, especially given their strong roots in Europe. Even a prominent notice on the web page would suffice, if they don't want to do the demonstration properly."
    author: "Tom"
  - subject: "KStreamer and D-BUS"
    date: 2003-08-23
    body: "Is KDE infiltrated by GNOME or Red Hat?\nWhat's behind it?"
    author: "CE"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "Stop smoking crack, it's no good for you. Both talks perfectly fine, even though it seems the KStreamer talk unfortuntely won't happen because the speaker couldn't make it. \n\nJust to be very clear on that: Havoc and Co. have been invited so we can exchange ideas and improve interoperability. We are happy to have them here and the ongoing discussions are really interesting and fruitful. Sorry guy.\n\nDaniel"
    author: "Daniel Molkentin"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "mmmm crack"
    author: "crackman"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "But will KDE switch to some GNOME technologies in future versions?\n\nPS: I don't even smoke cigarettes."
    author: "CE"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "hmm, cigarettes"
    author: "he-sk"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "> But will KDE switch to some GNOME technologies in future versions?\n\nIf the technologies are good and make sense for KDE, then they will. Wether they are still hss to be seen.\n\nBTW: D-bus isn't a GNOME technology. It's a freedesktop.org technology."
    author: "anon"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "So what's the argument against using DCOP?"
    author: "AC"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "I think D-BUS is more comparable to libice than to DCOP.  Don't quote me on that though."
    author: "ac"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "no, it's more comparable to DCOP+libice than DCOP. There really isn't an argument to move away from DCOP at this stage, except for greater compatability between KDE and GNOME. Which is why, DCOP won't be replaced by DBUS in KDE4 most likely, but there might be a bridge between DCOP and DBUS distributed with KDE. Sorta like kxmlrcpd used to be run by default in KDE. "
    author: "anon"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "DCOP is, in the current implementation, only useful for apps in a KDE session talking to each other.\nNor for desktop apps to talk to system daemons or vice versa, especially not when running as a different user.\n"
    author: "Tim Jansen"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-27
    body: "As far as I know, dbus is not a gnome technology. It is a technology developed by Havoc for Red Hat and he is trying to push it in gnome and in KDE because it would provide a common communication protocol for the desktops. The pro were:\n- common techno between two desktop\n- does not depend on libice\n- does not depend on Qt and C++\n\nDCOP is still doing a good job, so there is no reason to replace it. Havec said that adopting dbus is even more difficult on the gnome side as on the kde side, because it looks too much like DCOP. So actually, it is Gnome using clones of our technologies, not the reverse. :-)"
    author: "Philippe Fremy"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-27
    body: "The more important advantage is that DBUS allows desktop apps to communicate with system applications/daemons. Letting Gnome and KDE apps communicate is quite useless, this this usually requires that both are pretty tightly coupled (having a generic way to communicate does not help you if the app is not designed to use a specific interface).\n\n>>does not depend on Qt and C++<<\n\nI call this a huge disadvantage. Actually enough that I am already thinking about reimplementing it in C++/Qt. It just sucks that when you need to add a feature or fix a bug you have to do this using pre-historic technology. What's next? Will we have a freedesktop replacement for Sycoca or KTrader written in COBOL?\n\n\n \n"
    author: "Tim Jansen"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-10-24
    body: "Your philosophy absolutely sucks. For low layers, C beats CPP."
    author: "somebody"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-10-24
    body: "how so? More and more low level API's are being written in C++ or other such mid-level languages. Of course this is true more in the non-UNIX world, but is starting to be the case among the UNIX-like world as well."
    author: "fault"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-08-23
    body: "Well, today I listened to Havoc's talk about freedesktop.org in which I actually were interested in, but to be honest it was the most boring talk so far. I assume it's not really Havoc fault though since it was to be expected that he can only talk about GNOME related interests in freedesktop.org, and thus the talk turned out the way it has. First talk with no single question or discussion as follow up. But talking to Havoc personally (like right now at the 'social event') is nice. =)"
    author: "Datschge"
  - subject: "Re: KStreamer and D-BUS"
    date: 2003-11-26
    body: "Guys, gimme a break... \nDBUS is a great idea because its desktop agnostic!  Do you really expect the Gnome guys to incorporate DCOP when it rely's on a whole slew of Qt and KDE bindings??\n\nDBUS is very similar to DCOP.  I think KDE should embrace the DBUS standard and incorporate it into KDE 4 since it should be very simple to incorporate.  The gnome guys have a tougher time switching to DBUS than we do, they use CORBA which is very different...\n\nI am all for better desktop interoperability!  Why not let KDE apps run well on Gnome and vice versa?  What do we have to lose?"
    author: "Guest"
---
Tomorrow is the first day of the <a href="http://events.kde.org/info/kastle/">KDE Developers' Conference 2003</a> (nicknamed "Kastle"), preceded today by the <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> membership meeting and followed by a week long hackfest until Sunday 31st.  The Kastle event is hosted by KDE e.V., the <a href="http://www.greentech.cz/">Academic and University Center Nov&eacute; Hrady</a> and the <a href="http://www.fhs-hagenberg.ac.at/">Polytechnic University of Upper Austria</a> in Hagenberg. Amongst the sponsors are <A href="http://www.hp.com/">Hewlett Packard</a>, <a href="http://www.trolltech.com/">Trolltech</a>, <a href="http://www.klaralvdalens-datakonsult.se/">Klarälvdalens Datakonsult AB</a>, <a href="http://www.suse.com/">SuSE</a> and <a href="http://www.stadrin.com/">Stadrin</a>. Press coverage of the conference is by <a href="http://www.linux-magazine.com/">Linux Magazine</a> and also in a small series on the Dot starting today.
<!--break-->
<p>
Yesterday, the first participants arrived at Nov&eacute; Hrady. There was a <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day0/dscf0039.jpg">shuttle service from Prague</a> sponsored by SuSE and another bus running from Frankfurt sponsored by a Fortune 500 company. A small number of participants arrived on their own at various hours of the day while the shuttles only arrived late evening. The <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day0/dscf0050.jpg">process of  checking in</a> large numbers of developers and assigning accomodations <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day1/dscf0029.jpg">in the castle</a>, <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day1/dscf0039.jpg">in the monastery</a> (and even some pensions due to space reasons) took a while but late snacks provided a distraction.
<p>
While the official language at Kastle is English, many German and Dutch sounds are filling the air. Among the countries outside Europe from where developers have joined us are Australia, Brazil, Canada, Israel and the USA. 
<p>
After moving their luggage to the rooms, many developers moved out to explore the nearer environment sunken in darkness and visited <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day0/dscf0061.jpg">a local pub</a> with cheap and good beer, getting to know each other until long after midnight.  Internet access was not available until Friday afternoon, so don't worry if you hear nothing from a significant share of KDE developers for a while. 
<p>
It was a warm and sunny day today, the day of the KDE e.V membership meeting.  The most important result of that meeting is that the long discussed <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day1/dscf0011.jpg">changes of the bylaws were approved</a> by the required majority. A full report of the meeting and a new translation of the new bylaws will be made available later on the KDE e.V. website. 
<p>
Lunch consisted of 3 courses and supper of 2 courses. In late evening, a second shuttle from Prague is expected to arrive bringing the total number of participants for the conference tomorrow up to about 100. Preparations to set up a <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day0/dscf0067.jpg">third computer lab</a> for the hackfest are still ongoing.
<p>
Links to more photo albums are starting to appear on the <a href="http://www10.informatik.uni-erlangen.de/~simon/n7ywiki/wiki.cgi?action=Browse&id=PhotoAlbum">photo page of the conference wiki</a>. Another good place to look for comments of attending developers are their personal
diaries on <a href="http://kdedevelopers.org/blog/">KDE Developer Journals</a>.