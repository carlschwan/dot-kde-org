---
title: "Lord Of The Rings Wields Our Precious"
date:    2003-11-29
authors:
  - "numanee"
slug:    lord-rings-wields-our-precious
comments:
  - subject: "well now..."
    date: 2003-11-29
    body: "This has just about puched me over hte edge of buying this DVD set... must.....have......movie.........features.........extra.....kde......."
    author: "standsolid"
  - subject: "Great!"
    date: 2003-11-29
    body: "It's always nice to see examples of KDE in the real world, doing real things for real people.  Not just us hackers writing a desktop to use to write a desktop to use to write a desktop to use to ...."
    author: "George Staikos"
  - subject: "What's the app"
    date: 2003-11-29
    body: "What is the app being used to model Golum?"
    author: "rizzo"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "blender ?? refactored for the lotr ?"
    author: "Philippe"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "It's Maya (http://www.alias.com)."
    author: "eyeballkid"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "Hmmm, yes they use Maya. But what another graphic app do they use on Linux plattform?\nGimp, Photoshop (w/cxoffice), VMware?\nI don't think they have only one app (Maya) for one PC with Linux. I think Maya is useless if I can't change some textures with ??gimp??photoshop?? on the same machine!?\n"
    author: "anonymous"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "Perhaps they use CinePaint as well?"
    author: "rinse"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "My guess is that they useed Photoshop with Crossover Office. I heard that the US Film Idustry paid Codeweavers $100000 to make it work with Crossover."
    author: "Calle"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "It would be nice to see that money used to enhance GIMP to compete against Photoshop iff Adobe is not really going to port it.\n\nWhile I support Commercial software (I will but it iff I like it and will not use it otherwise), I do think that we need alternatives if these companies are not coming over."
    author: "a.c."
  - subject: "Re: What's the app"
    date: 2003-12-01
    body: "From their perspective, having the exact same application running on either platform may be a greater advantage than having a native, free application that is somewhat different.  Pretty much everyone in the industry knows Photoshop."
    author: "kjd"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "There are a number of custom tools that the development houses have ported over. A lot of these tools ran on IRIX or Solaris already, so the ports were pretty simple. Beyond that, there are a number of other standard pro tools the studios use, namely XSI (used by ILM), Shake, etc."
    author: "Rayiner Hashem"
  - subject: "Re: What's the app"
    date: 2003-12-02
    body: "They could have used Amazon for Linux instead of Photoshop (likely, at least for textures and backgrounds). It's way beyond Photoshop (or the Gimp), but hold your breath, it's _very_ expensive.\nhttp://www.ifx.com"
    author: "LaNcom"
  - subject: "Re: What's the app"
    date: 2003-12-05
    body: "I disagree, they probably have only one app for one PC with Linux. These are very specialized guys, there's no way, the animator or the modeller is going to do the textures himself. Probably they use Photoshop on MacOS for it..."
    author: "grovel"
  - subject: "Re: What's the app"
    date: 2003-11-29
    body: "I think it is Mirai.\nCheck out one of the main modellers discussion site for more info:\nhttp://cube.phlatt.net/forums/spiraloid/index.php\nThere is a special thread about Gollum:\nhttp://cube.phlatt.net/forums/spiraloid/viewtopic.php?TopicID=493"
    author: "anders"
  - subject: "One thing is for sure..."
    date: 2003-11-29
    body: "The Two Towers Extended Edition rocks! :)\n\nHighly recommended!"
    author: "Joergen Ramskov"
  - subject: "Fairly obvious from shots that FreeBSD is the OS"
    date: 2003-11-29
    body: "and not Linux ..."
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Fairly obvious from shots that FreeBSD is the OS"
    date: 2003-11-29
    body: "Cool. What do you base that on?\nWhat have I missed?"
    author: "a.c."
  - subject: "Re: Fairly obvious from shots that FreeBSD is the "
    date: 2003-12-01
    body: "Hmmm... you have \"obviously\" not kept up with Weta's Linux commitment and usage.  There is no way to tell from the pics which *nix is being used but any slightly informed individual would safely bet that they are using Linux.  Weta's commitment to Linux and Linux development is almost famous in the motion picture industry.   \n \nbrockers"
    author: "brockers"
  - subject: "Re: Fairly obvious from shots that FreeBSD is the "
    date: 2003-12-02
    body: "Oh yes, there _is_ a way. Weta uses Linux - check Weta's press releases.\nBTW, Maya (the 3D app on one of the screenshots) won't run under *BSD..."
    author: "LaNcom"
  - subject: "Kind of ironic..."
    date: 2003-11-29
    body: "That the movies are being produced on Linux, but it's not legal in the US to watch them on Linux.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: Kind of ironic..."
    date: 2003-11-29
    body: "I guess that explains why there are so many crappy movies out there. If the producers can't legally watch their product..."
    author: "Kavau"
  - subject: "I missed it!"
    date: 2003-11-30
    body: "I can't believe I watched all the appendices and missed KDE in it. Got to get my eyes checked!"
    author: "Leonscape"
  - subject: "Re: I missed it!"
    date: 2003-11-30
    body: "If you are so used to your own favourite desktop environment this is quite likely to happen ;-) I already found myself seeing KDE on TV without really realizing it at once (\"Hm, looks like my own desktop\") - it was just after a while that I got aware of it (\"Hey, they used KDE!\")"
    author: "Tackat"
  - subject: "Maya"
    date: 2003-11-30
    body: "What toolkit is Maya built with?\nIt looks pretty nice.\n\n"
    author: "frappa"
  - subject: "Re: Maya"
    date: 2003-12-01
    body: "Ha ha ha.  I am not certain if it is Motif or a custom toolkit - probably custom, actually.  Maya's interface is a *mess*.  It has an extremely high learning curve, but much like blender (which has a FAR worse interface), once you get familiar with it you can work quite quickly."
    author: "Ben Staffin"
  - subject: "Re: Maya"
    date: 2003-12-03
    body: "I can't think of a Pro Graphics App that does not have a big learning Curve. I found Maya at least a lot easier to learn than 3D Studio Max.\nI think that Mayas interface is one of the most Innovative I have seen in a long time, It is just that the program is so huge it can be hard to know where to begin.\n Anyhoo I am uncertain of toolkit but those toolbars look awefully Motify don't they. "
    author: "Danni Coy"
  - subject: "Re: Maya"
    date: 2003-12-20
    body: "It's Motif. The Linux version comes with openmotif which must be installed prior to Maya."
    author: "Mike"
  - subject: "Re: Maya"
    date: 2003-12-04
    body: "\"Maya uses OpenMotif 2.1 as its GUI framework. The version used is OpenMotif 2.1.30 from ICS.\"\n\nhttp://www.alias.com/eng/support/maya/qualified_hardware/QUAL/maya_50_linux.html"
    author: "Linz"
  - subject: "Fuzzy Time"
    date: 2003-12-01
    body: "In the last screenie, they are using fuzzy time :)\n\nI think it's cute.  Actually, I think it's awsome that there is at least one person in the world that has used/is using fuzzy time.  Makes me feel like feature creap isn't all for nothing :P\n\nTroy Unrau (http://tblog.ath.cx)"
    author: "Troy Unrau"
  - subject: "Re: Fuzzy Time"
    date: 2003-12-01
    body: "Hey, I use fuzzy time all the *t*i*m*e*!!\n\nI love it."
    author: "TomL"
  - subject: "Re: Fuzzy Time"
    date: 2003-12-02
    body: "I'd forgotten all about fuzzy time - I'm going to start using it again now. Maybe it's a bit strange, given I set my computer's time by NTP, but it's a nice human way of telling the time :)"
    author: "Ben Roe"
  - subject: "Re: Fuzzy Time"
    date: 2003-12-06
    body: "Hey, fuzzy time is awesome!\n\nThere's no other clock for me."
    author: "Divide"
  - subject: "Television"
    date: 2003-12-02
    body: "KDE has been displayed and used on the television show Alias before.  Look for it on the CIA desktop displays. :)"
    author: "newt"
  - subject: "I like the story's title so I ..."
    date: 2003-12-05
    body: "I nominate this story's title as \"Best Story Title\" for 2003."
    author: "Allen"
  - subject: "Re: I like the story's title so I ..."
    date: 2003-12-06
    body: "hee, hee.  thanks!  :)"
    author: "Navindra Umanee"
  - subject: "Unhidden Kicker panel"
    date: 2003-12-06
    body: "This is certainly an excellent example of Linux and KDE being used \"in real life\", but the one thing which struck me about the screen captures (and the one thing which made it possible to easily tell that KDE was being used at all) was that the users of those machines run KDE with the Kicker panel unhidden!\n\nIt's funny but the first thing I do on any machine I have access to is to set the panel to autohide when not in use, in order to maximise my vertical [1] screen real estate! I'd have thought that other power users, such as these, who spend most of the day in the one app, would do the same.\n\n[1] I know you can align the panel to the side of the screen to maximise vertical space that way, but unfortunately, given the necessary width of many of the panel contents, it doesn't really work so well that way (although you do tend to have more horizontal space to play with/sacrifice, than you do vertically). Another muse here: I wonder if anybody has done any research as to whether it's easier to fling the mouse left/right to reach a panel, than up/down? It would seem to me that it's a slightly easier wrist movement to 'rotate' left/right than stretch/retract up/down..?"
    author: "David"
  - subject: "Re: Unhidden Kicker panel"
    date: 2003-12-31
    body: "They obviously unhid the panel so everyone would see what environment they use, sort of to give credits and to enhance popularity. That should be done more often by all of us."
    author: "Volker"
  - subject: "sdf"
    date: 2008-04-12
    body: "sdfsdf"
    author: "dfsdf"
---
To the surprise of many, KDE turns up unexpectedly on the extended DVD edition of <A href="http://www.imdb.com/title/tt0167261/">The Lord of the Rings:  The Two Towers</a>.  Dan, Robert and Antonio were the first of several to point out that KDE was <a href="http://dot.kde.org/sites/dot.kde.org/files/gollum_success.png">clearly</a> <a href="http://dot.kde.org/sites/dot.kde.org/files/kde_gollum1.png">being</a> <A href="http://dot.kde.org/sites/dot.kde.org/files/kde_gollum2.png">used</a> by Weta Digital (likely on <A href="http://www-3.ibm.com/software/success/cssdb.nsf/CS/MMAA-5ARPZY?OpenDocument&Site=default">IBM Linux Intellistations</a>) in an animation shot with Gollum and Sam.  This isn't the first time KDE has been spotted either on the silver screen, television, or being used behind the scenes of Hollywood.
<!--break-->
