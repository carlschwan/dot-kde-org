---
title: "OSNews.com: The Big freedesktop.org Interview"
date:    2003-11-25
authors:
  - "numanee"
slug:    osnewscom-big-freedesktoporg-interview
comments:
  - subject: "D-BUS and DCOP?"
    date: 2003-11-25
    body: "Waldo's comments suggest that by KDE4 we will see the KDE Project making a committment to migrate from DCOP to D-BUS. Is this policy, or his opinion?\n\nFrom what I have read of the two technologies, and largely based on the integration opportunities, I think this sounds like a good idea, but I know it has been a topic of some controversy since it was first raised."
    author: "Tom"
  - subject: "Yeah, but"
    date: 2003-11-25
    body: "It's true he does say that it would be a good idea to switch to D-BUS by KDE 4, however he is one of the few KDE developers who endorses such a move. Don't be suprised if this doesen't happen.\n\nD-BUS is nice, but how is it really better than DCOP and why should KDE switch to D-BUS instead of the rest switching to DCOP? In addition, such a major change would definitely set KDE development back a few months while GNOME would be speeding ahead since they already use D-BUS.\n\nI am all for integration, but such a move, while a win for the Linux desktop it also seems like a big blow to the hard work that went into DCOP and in making DCOP work through out KDE. It also uses up precious time.\n\nBTW: I hope KDE 3.3 will have a release cycle no longer than 6 months since KDE 4 will need a lot of time, probably at least a year and a half."
    author: "Alex"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "Is D-BUS based on DCOP?  if so I'd hope a switch \nwouldn't be as radical as you are suggesting.\n\nGnome has it's own DCOP-ish \"cobra\" so they will \nbe just hard at work kitting together their already\nloosely knit applications anyway with this new\nframework.  on the other hand, though we shouldn't \nworry about gnome innovationg faster than us, rather \nwe work together with them.  \n\nIt'd be REALLY cool if DCOP didn't have mad-style KDE \ndependencies so gnomes could use it without hainvg to\ninstall KDE as well."
    author: "standsolid"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "1) Yes, D-BUS is based on DCOP. Its actually very DCOP-ish, and was designed so it could replace DCOP. Its got the advantage that its desktop-agnostic, unlike DCOP.\n\n2) GNOME uses CORBA, a major cross platform standard for IPC. Switching to DBUS would be even a bigger pain for them, because CORBA is *very* different from D-BUS, while DCOP isn't that different from DCOP.\n"
    author: "Rayiner H."
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "So we could speak of D-BUS as a DE-intependent DCOP successor? Dose it deliver other improvements than this? I heared it's faster then CORBAS IPC, is it faster than DCOP? Or are there no other differences? (I konw, my questions are annoying. :) )\n\n-panzi"
    author: "panzi"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "Check out the D-BUS tutorial first: http://www.freedesktop.org/software/dbus/doc/dbus-tutorial.html\n\nChoice quote: \"Semantics are similar to the existing DCOP system, allowing KDE to adopt it more easily.\"\n\nOverall, D-BUS is more elaborate and has more features than DCOP. \n\nSupposedly, Havoc has said that its actually harder for the GNOME people to adopt it, because its so similar to DCOP.\n\nOverall, I think D-BUS is a good thing, but whether it actually gets into KDE 4 is up to the KDE dvelopers. Its good that they are wary about this, though. KDE is clearly the better framework, at least in places like DCOP, KParts, KIO, and Qt, and I would really hate to see better technology get pushed aside in favor of easier to integrate technology. "
    author: "Rayiner H."
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "Okay, I'm sporry, i guess I was misinformed.\n\nI thought D-BUS was set to be adopted by GNOME in the near future and had no idea that it was actually absed on DCOP and that it is better.\n\nIn that case, i think adopting it for KDE 4 would be a GREAT idea and probably drive GNOME to adopt it and so our desktop unification woes would be mitigated."
    author: "Alex"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "D-BUS is better than DCOP because it has a chance to become a desktop-independent (and even GUI-independent) standard.  As I understand it DCOP has dependencies on QT and X windows, while D-BUS does not, making it much more likely to be adopted as a standard.  D-BUS would be good for the Linux desktop, and whatever is good for the Linux desktop is good for KDE.  I doubt that it would take that much effort to switch over anyway, because D-BUS is supposed to be similar to DCOP."
    author: "Spy Hunter"
  - subject: "FUD"
    date: 2003-11-25
    body: "It won't change anything for KDE.  Instead of DCOP using libice as the transport protocol, it can use D-BUS.  DCOP will still be around it will just use D-BUS underneath.\n\nAnd yes, this is *exactly* what D-BUS was *designed* for.  It's not GNOME-technology.  Almost none of the GNOME apps use it in fact.  GNOME uses CORBA."
    author: "anonymous"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "in addition to what others have mentioned, DBUS will allow marshalling w/out relying on Qt's idea of data ordering, which means it can be used by all sorts of apps. think about controlling apache via a DBUS control: fire up kdcop (kdbus? ;) and click on the \"apache -> restart\" item and voila, apache restarts.\n\nDBUS is also being designed with the idea that it can be used (and authenticated) over the network, something that's clumsy at best with DCOP...\n\nDBUS is really something of a more globally useful and flexible version of DCOP. since KDE is already centered around using DCOP and nothing else is, this transition should be easier for KDE than most others. the big thing for KDE will be providing a compat layer so that apps speaking DCOP (e.g. KDE2/KDE3 apps) will be able to talk to apps using DBUS. this should be doable with some work and if done completely should make the transition practically transparent. but that part of the puzzle is yet to even really be looked at AFAIK...\n\nIMO it basically boils down to the idea that we've learned a lot about how well DCOP works via it's use in KDE, as well as where it's weak. DBUS is attempting to address the weaknesses and create something that allows the rest of the world to take advantage of what we've learned as well... everyone will win in the end..."
    author: "Aaron J. Seigo"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "Ouch, if I hear it one more time, I'll scream. Qt's marshalling is trivial to reproduce. See dcopc, see the Qt source code, etc. (Yes, there is an issue of fixing a format revision. But it's addressable, it's in fact /trivial/ to address, compared to the 'translation layer' idea which may not be even doable, given very different effective type equivalence semantics, and DCOP's natural uses on untagged marshalling)\n\n\n"
    author: "Sad Eagle"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "To be more specific, here is an example where DCOP and D-Bus semantics diverge on type equivalence, making fully transparent conversion impossible[1]. Is this construction useful? I am not sure, but it can provide fully transparent overloads in an arbitrary number of combinations (imagine a function that should take 3 of the Triangles below -- doing it like this lets you take any of them as 3 point). Is it a big hack? Perhaps. But \"no one will use stuff like this\" is often famous last words..\n\nstruct Triangle\n{\n\tQPoint p1, p2, p3;\n};\n\ninline const char* dcopTypeName(const struct Triangle& triangle)\n{\n\treturn \"QPoint,QPoint,QPoint\";\n}\n\ninline QDataStream& operator<<(QDataStream& str, const Triangle& tri)\n{\n\tstr<<tri.p1;\n\tstr<<tri.p2;\n\tstr<<tri.p3;\n}\n\n...\nin some interface:\n\nclass SomeIface: virtual public DCOPObject\n{\n        K_DCOP\nk_dcop:\n\tvirtual void fn1(QPoint p1, QPoint p2, QPoint p3) = 0;\n\tvirtual void fn2(QPoint p1, QPoint p2, QPoint p3) = 0;\n\tvirtual void fn3(QPoint p1, QPoint p2, QPoint p3) = 0;\n\tvirtual void fn4(QPoint p1, QPoint p2, QPoint p3) = 0;\n\tvirtual void fn5(QPoint p1, QPoint p2, QPoint p3) = 0;\n\t//...\n};\n\n[1] The best I can think off is to provide a separate overload for << for some DBusStream class, or templating it. Note that \"use foostream in place of QDataStream is not a solution since an application may use QDataStream for storage on disk, which absolutely should remain binary compatible\n\n"
    author: "Sad Eagle"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "Please be sure to bring up your concerns on the right freedesktop.org forums!  They are very willing to fix issues with D-BUS, but if you don't tell them it will never happen."
    author: "anonymous"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "If its so trival why is dcopc so broken beyond repair to the point its disabled?  Ill give you a hint, its not ;)\n\nDBUS may not fix the problem completely, but it will give us a common ground to speak on.  That and its more code we dont need to maintain.  Unless you are offering to maintain libice for us.  The only pisser will be that the GNOMES are too attached to their CORBA to even consider something better.  In the end I fear even if we use DBUS we will never see the interop that is promised.\n\n"
    author: "Ian Reinhart Geiser "
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "dcopc is probably broken because no one has actually wanted to use it, and that probably happened because it was not promoted. People did not go around shouting from every tree how it's a solution to every problem in the world, how it's something revolutionary, and how KDE and Gnome would both be using it before developers have even heard of it. \n\nAnd look, I've written marshalling code dozens of times (quite a bit this week, in fact). At its most complicate it is just a bunch of bit shifts and masks, and some of the most boring and trivial code that's out there[1]. The only piece of Qt marshalling that's complicated is for QImage, which basically just sends a PNG over the wire --- but it's hardly critical, and any decent toolkit framework can load and save PNGs. And there is nothing particularly magic about writing out an integer as 4 bytes in a fixed-endian order, and a point as 2 such integers. \n\nAnd as to maintaining libICE: I wouldn't maintain it, but I would gladly maintain a proper implementation of the wire protocol; including writing it, which is probably at most about a week long excercise for the core protocol.  (I can't comment on the authenication protocol plugins, since they are not specified in the core spec; however magic cookies can't be too hard).\nA flip question, however: would you maintain the current implementation of dbus (I certainly wouldn't)? Remember, maintainers don't stick with projects forever. \n\n[1] Although DBus and ICE both make it more 'fun' by introducing some gratious byte-swapping\n"
    author: "Sad Eagle"
  - subject: "Re: Yeah, but"
    date: 2003-11-25
    body: "Yeah, but in the end DBUS is also just repeating what SOAP did in the last years. They re-invented their own serialization format (instead of XML or one of the binary XML formats for the performance-junkies), re-invented an interface description language (IDL vs WSDL/Xml Schema) and half-re-invented authentication mechanisms (as they are using SASL). In the future, if DBUS should become more popular and is used over the network, they will probably re-invent encryption mechanisms, maybe transaction and routing mechanisms and so on. Great plan. \n\nAnd they are also doomed to re-invent new IDLs for all applications that already have a SOAP-equivalent.\n"
    author: "AC"
  - subject: "Re: Yeah, but"
    date: 2003-11-26
    body: "no, it isn't just repeating what SOAP or XMLRPC has done. it doesn't use XML, which means its faster, simpler and lower overhead. it's primary use will be applications on the same machine running as the same user. you are right in that DCOP, DBUS, SOAP and XMPLRPC are remote procedure call schemes, but DCOP and DBUS are designed specifically with desktop use in mind. the tasks and needs there are rather different from (and in some ways a lot simpler than) web services. DCOP works very well with just a few outstanding issues, which hopefully DBUS will allow us to address.\n\nas for reinventing encryption and authentication: it would not only be more work to reinvent than use what already exist; it would also limit interop and be a pain in the patoot to verify it as being Correct compared to leveraging something that's already been through the usual cryptographic scrutiny.\n\nthe direction you seem to be leaning towards (a high-overhead, complex, and overly-complete solution) is very similar to what drove people to try CORBA. both KDE and GNOME have attempted to use CORBA. but that real world usage revealed that a simple, lightweight, purpose-built mechanism beats the shinola out of hacking heavy, general purpose mechanisms for desktop IPC."
    author: "Aaron J. Seigo"
  - subject: "Re: Yeah, but"
    date: 2003-11-26
    body: "1. I don't think that there is any significant performance difference if you are using a binary XML serialization.\n2. SOAP is *not* limited to web services. That's just its marketing niche. SOAP is also used in embedded devices (like all UPnP devices) and on Windows for IPC. Another myth is that SOAP is limited to HTTP. Unlike XMLRPC it isnt. .Net allows other mechanisms like raw TCP for faster communication.\n3. SOAP is not as complicated as CORBA, it is a really simple system. All you need are four XML tags. There are extensions on top of SOAP that are needed in some cases, but DBUS will have the same problem if it should ever do more than trivial messaging\n"
    author: "AC"
  - subject: "Re: Yeah, but"
    date: 2003-11-26
    body: "1. Binary XML is just for faster over-the-wire transfer. At the other end\nXML still needs to be parsed which is very costly compared to DBUS/DCOP marshalling.\n\n2. Yes, but it was primarily design with web services in mind.\n\nSOAP had specific design goals, features and limitations. Don't try\nto use it for something it was not meant to do (inter-desktop \ncommunication) even thought it is the standard in IT'S domain (web \nservices).\n\nDon't try to use a horse for travelling the sea!\n\n"
    author: "buggy"
  - subject: "Re: Yeah, but"
    date: 2003-11-28
    body: "SOAP can be used on Windows for IPC, but isn't generally.  The normal Windows RPC mechanisms are COM, DCOM, and MSRPC.  MS's RPC tends to use the DCE RPC NDR format for serializing the data, and is generally pretty complex.  There are historic reasons for all of this, but it's not important right now.\n\nI would hope that D-Bus is sufficient that a SOAP connector could be developed, just as an XML-RPC connector was developed for DCOP.  The problem with taking a standard and adding proprietary extensions to it that become required by your implementation is that it nullifies the advantage of using the standard in the first place.  For KDE and Gnome, there's no marketing department that will trumpet the use of something like SOAP despite the fact it's dependant on those proprietary extensions, so there's no point in using it."
    author: "AC"
  - subject: "Re: Yeah, but"
    date: 2003-11-28
    body: ">>The normal Windows RPC mechanisms are COM, DCOM, and MSRPC.<<\n\nFor old applications. They are all deprecated in .Net and Longhorn.\n\n>>For KDE and Gnome, there's no marketing department that will trumpet the use of something like SOAP despite the fact it's dependant on those proprietary extensions, so there's no point in using it.<<\n\nI'd call DBUS proprietary as well. The protocol is published, but instead of using the standard that the W3C established a completely new protocol has been created. It's like re-creating HTML, no less.\n\n\n"
    author: "AC"
  - subject: "Re: Yeah, but"
    date: 2003-11-28
    body: "In .NET, the serialization format can be specified by implementing the \n \n  System.Runtime.Remoting.Messaging.IRemotingFormatter\n\ninterface. There are two already provided, BinaryFormatter and SoapFormatter.\nThe former is preferred for high-performance needs, because the serialization\nand deserialization is much faster and the transferred data is more compact\nthan for SOAP. Of course, the same argument applies to DBUS.\n\nFor web services, different trade-offs are possible, because performance\nand interoperability considerations are radically different from IPC.\n"
    author: "Bernd Gehrmann"
  - subject: "Re: Yeah, but"
    date: 2003-11-28
    body: "But a) they still have the same interface, the serialization is transparent and b) today's Remoting will be deprecated in favour of Indigo / System.MessageBus."
    author: "AC"
  - subject: "Re: Yeah, but"
    date: 2003-11-28
    body: "> a) they still have the same interface, the serialization is transparent\n\nAnd since it's transparent, the natural conclusion is that one should use\nthe most efficient serialization, as doing otherwise does not give any\nadvantage.\n\n> b) today's Remoting will be deprecated in favour of Indigo / System.MessageBus.\n\nYeah, I'm sure they can continue throwing out new APIs in 2-year intervals\nfor the next 20 years, no doubt."
    author: "Bernd Gehrmann"
  - subject: "Re: Yeah, but"
    date: 2004-04-18
    body: "SOAP is hardly simple; it is a classic example of \"design-by-committee\" at its worst, and consists entirely of bloat that is unneccessary in the problem space DBUS is addressing.  I'd argue that the SOAP-Bloat is unneccessary in *any* problem space -- SOAP adds a lot of overhead without providing very much (if any) added value.\n\nYour \"four tags\" quadruple the size of most SOAP messages.  I also disagree that DBUS will encounter the \"same problem\" of bloat that SOAP suffers from; I've been involved in a number of projects that used REST and simple XML messages /without/ SOAP wrappers and SOAP bloat.  What the designers of SOAP forgot is that all of the magic happens in the WSDL, not in the RPC wrapper.  If you absolutely feel the need to wrap your messages up in a bunch text, use XML-RPC.  The specification is all of two pages (compared to the 20 pages of SOAP crap you have to wade through).\n\nDBUS seems to be fairly light, like DCOP, which is nice.  Distrust overly complicated system!\n\n\"When I'm asking simple questions, and getting simple answers, I'm talking to God.\"  -- Einstein"
    author: "Sean"
  - subject: "simple questions"
    date: 2006-04-30
    body: "I want to use the Einstein quote \"when I'm asking simple questions and getting simple answers, I'm talking to God\" in a paper I'm writing for school, and I'd like to know more about where exactly this quote comes from.  Any information on when and where Einstein said this?  Response much appreciated.\n"
    author: "Rachel Kayhan"
  - subject: "Copy/Cut & Paste"
    date: 2003-11-25
    body: "It's nice with what the freedesktop.org is trying to do. I just hope that somebody is trying to adress the very bad behavior of copy/cut & paste through all linux applications, even though it works most of the time in KDE it's a pain from other applications in my experience. I think that you should be able to use ctrl+c/x and then do ctrl+v through all applications...\n\n/David\n"
    author: "David E"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-25
    body: "If fact, just selecting should be enough (it is Unix after all, not Windows...). I have noticed that Konqueror/KHTML is about the worst application of all when it comes to copy/pasting. It hardly ever works, and most of the time it's faster to just re-type an URL than to try and copy/paste it. :-("
    author: "Andr\u00e9 Somers"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-25
    body: "So you have never used konqueror, of course you can copy/paste URL's.\n\nAnd you have that icon on the left of the URL sou you can delete the old URL and then click the central button and you have it or you can use the Knotes or you can ...\n\nAbout DBUS , there is a lot of really good apps from the other part of the universe : mozilla, openoffice, gabber, ... so it is a must a better integration just because kde is really the best but those wondefull apps from the dark side of the moon jai,jai, :-)"
    author: "The man they couldn't hang"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-25
    body: "No, I'm afraid he's right. At least on my system, selecting some text in Konqueror (3.1.4) and then pressing Ctrl+C doesn't always copy properly. It's very irritating, but because the 3.2 release is so close I've put up with it."
    author: "Paul Eggleton"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-25
    body: "Have you filed a bug report?  I believe most developers use middle-mouse-button copy/paste, so this bug could be overlooked for quite a while if you don't report it."
    author: "Spy Hunter"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-27
    body: "No, I haven't - I should have done earlier. However now is not really the time to be filing reports against 3.1.4."
    author: "Paul Eggleton"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-25
    body: "Yes, I know you _can_. Konqueror is my main browser, I only switch to Mozilla if a site really doesn't work on Konqueror. However, I still frequently run into this problem that copy/pasting text from a webpage doesn't allways work. I see that on both my laptop and my desktop systems."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-25
    body: "ack. I'm sorrt, but he's right. I've been using nothing but konqueror for two years and copy/paste support is terrible (though I do love the \"clear location bar\" button dearly).\n\nI rely almost entirely on my mouse's middle button, because you never know what that last thing you actually managed to copy to the clipboard was."
    author: "c"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-26
    body: "What I've never understood is why there seems to be two different clipboards.  If I copy by selecting (classic X method) I can paste with the middle mouse button but if I copy or cut using ctrl-c or ctrl-x I have to paste using ctrl-v.\n\nDoes KDE's Klipper copy to a differnt place then XFree86 does?"
    author: "kinema"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-26
    body: "It's all configurabel in Klipper. The default are different clippboards for X \nand ctrl- . Why I can't tell you, and I have never had any problems cutting and pasting :-)\n"
    author: "Morty"
  - subject: "Re: Copy/Cut & Paste"
    date: 2003-11-28
    body: "The clipboards are different because they are used in different ways.  With a Windows-style clipboard, you can copy a URL with Ctrl-C, select all the text in the location bar, delete it with backspace, and Ctrl-V to paste the URL.  If the clipboards are unified, selecting anything nukes the clipboard, so when you paste you just paste back what you selected.  This was a very *very* common source of complaints in KDE 2. (Yes I know about the button to clear out the location bar, but some people are used to doing it the other way, and there are other situations where separate clipboards are useful)."
    author: "Spy Hunter"
  - subject: "Systray icons"
    date: 2003-11-25
    body: "Related question: when will kde programs start using the systray specifiation from freedesktop.org? It's rather frustrating that the systray icons of most GTK+ apps (like Gaim, Rhythmbox) also work fine in KDE, but most KDE's systray icons (kopete, k3b, printing icon) all appear in a seperate window in Gnome."
    author: "Fred"
  - subject: "Re: Systray icons"
    date: 2003-11-25
    body: " Given that there are KDE apps which don't use KSystemTray class directly, but use the low level API for systray (they were simply written before KSystemTray was created AFAIK), hardly before KDE4, as the low level API can't handle the way fd.o systray spec works. And given that there are certain plans to (quite radically) change the way KDE systray works (the underlying mechanism) to something better, maybe never.\n\n Just because some specification is posted at fd.o doesn't necessarily mean that everybody is suddenly going to use it. See for example the \"extension to the ICCCM selections mechanism\" (nobody uses that at the present time), or XDS (neither GNOME nor KDE use it, although there are some apps which use this one). Maybe these proposals will get into use eventually, maybe they'll silently die, maybe they'll be superseded by something better, who knows?\n\n That said, I think it shouldn't be that difficult to write a small utility that you'd run with GNOME, which would act like a proxy and make KDE apps appear like using fd.o systray spec to outside. So the answer to your 'when' could be 'when somebody writes this'. Maybe I'll give it a try.\n"
    author: "L.Lunak"
  - subject: "Unified desktop"
    date: 2003-11-25
    body: "So there is actually a move to unify the desktop on Linux.\nWhen all apps can integrate regardless of GUI toolkit used,\nthen it won't matter which a developer chooses. Great."
    author: "OI"
  - subject: "Re: Unified desktop"
    date: 2003-11-25
    body: "Great mess."
    author: "AC"
  - subject: "Expocity"
    date: 2003-11-25
    body: "Something for KDE to include? Or is there something similar available already?\n\nSeems that GNOME is getting ahead of KDE in several areas: integration, information managment etc.\n\nWell, it's maby time for me to also lend KDE a hand (or two). Where do I start? \n\n\nComments anyone? \n"
    author: "Swoosh"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "For something like Expocity I would for starters take a look at the advanced pager. My guess is there are some/lots of code to steal/reuse to get you started. Good luck!\n\n"
    author: "Morty"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "in which ways is GNOME ahead of KDE in integration and information management?"
    author: "Aaron J. Seigo"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "Their web browser context menus have \"View Document Source\" in them? :-)"
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "Epiphany does? that's news to me =)\n\nYou could easily make a KonqMenuPlugin to add said action, if you wanted to, and distribute it. =)"
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "> You could easily make a KonqMenuPlugin to add said action, if you wanted to, and distribute it. =)\n\nNo offense, but that's the dumbest thing I've ever heard. =)"
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "> No offense, but that's the dumbest thing I've ever heard. =)\n\nI'm sure it would appear dumb to people who beleive viewing source is a feature appreciated by very many intermediate users =)"
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "Mozilla, Opera and IE think it's important enough.  Is the KDE approach to make things annoying and to push users to other browsers?\n\nA context menu should let me do interesting things in the context.  Easily.  It's ridiculous that when I right click on an Image, I have to go to the \"Image->\" submenu to do anything with it! "
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-26
    body: "> A context menu should let me do interesting things in the context. Easily. It's ridiculous that when I right click on an Image, I have to go to the \"Image->\" submenu to do anything with it!\n\nThis was fixed. \n\n> Is the KDE approach to make things annoying and to push users to other browsers?\n\nNo, the KDE approach is to make it so that the largest amount of end users feel comfortable in KDE. This means that we should target intermediate users (not beginning OR advanced users). This allows beginning users to get a feel for the interface quickly and advance to the intermediate stage (because nobody wants to be a beginner for long). Eventually advanced users will appreciate things too. \n\nThis is why view source doesn't belong in the context menus, and why it was a mistake re-adding it in. If you really want such advanced features, as I said, why not make a web developers plugin for konqueror? "
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-26
    body: "GNOME has already taken the approach of dumbing down the interface.  KDE has to find a better way.  KDE can be powerful, convenient and easy to use without being dumb.\n\nAaron made the right choice.  Anyone who says \"implement a plugin\" is very very wrong and there is not much to discuss if you honestly think that is a valid answer."
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: ">No, the KDE approach is to make it so that the largest amount of end users feel comfortable in KDE.\n\nIE has view source in the context menu.\n\nMozilla has it, and even \"view selection source\".\n\nI like to surf with Konquerer, but no \"view source\" in the context menu ist extremly annoing behavior.\n\nBut it's up to the developers to lose market share to mozilla for such a stupid decision."
    author: "Micha"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "> IE has view source in the context menu.\n> Mozilla has it, and even \"view selection source\".\n\nYes, they do for historical reasons.\n\n> I like to surf with Konquerer, but no \"view source\" in the context menu ist extremly annoing behavior.\n\nIf you use this feature, you're likely an advanced user in the scope of web browsing. What's next? Putting \"Show DOM Tree\" in the context menu? Oh yeah, lets put \"Show JavaScript Debugger\" in there also. These would be incredibly useful for web developers and advanced users as well. But should we optimize for them (the potential minority) or the majority who don't consider them an action they do everyday while using context menus (intermediate users, I'm NOT talking about Grandma, but Joe User who has been using Lindows for a few months)\n\nView source is a remenant of the early days of the web when all web users==web developers. It's time to grow up and be focused on end users. Hell, if we wanted to optimize for advanced users, we might as well put in show dom tree, since it is as useful in the modern scope of the web. Do any browsers put it in their context menus though? Nope, it historically has never been there."
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "If you want a dumb interface, use GNOME. \n\nDon't destroy KDE for some mythical \"end-users\" niche group. \n\nYou end up with a dumb KDE and your \"end-user\" using Mozilla anyway (with it's nice little View Source Document).\n\nYou don't remove a function that has been in KDE for years and has come to be expected.  That's crazy."
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "> You don't remove a function that has been in KDE for years and has come to be expected\n\nErm, who talked about removing a function? It'd still be there of course in menubars and via shortcut keys. \n\n> Don't destroy KDE for some mythical \"end-users\" niche group.\n\nheh. We aren't talking about a niche group, we are talking about intermediate users. We shouldn't optimize for newbies who won't use a context menu anyways, and we shouldn't optimize for advanced users, who often use keyboard shortcuts anyways. If you don't understand this, you have a lot to learn about 1). usability 2). why KDE was founded. \n\nKDE was founded to be easy to use for everybody, and ultimatey, that means making an interface that newbies can get accustomed to, intermediate users will be productive in, and advanced users will be comfortable in. ;>\n\n"
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "> Erm, who talked about removing a function? It'd still be there of course in menubars and via shortcut keys.\n\nI mean removing a function accessible via a sane interface like the context-menu.\n\n> heh. We aren't talking about a niche group, we are talking about intermediate users. \n\nI'm an intermediate user.  I certainly don't write plugins to change my interface or use advanced keyboard shortcuts.  I find it more \"advanced\" to use a smart context menu than to spend time implementing plugins or learning shortcuts that do different things in different applications.\n\nWhat you are doing is creating a niche group and looking down on them as if they are some dumb group who can use Mozilla with its View Document Source but are somehow incapacitated from using Konqueror.  Do you see?"
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "> I'm an intermediate user. \n\nI really don't want to get an argument over pure semantics here, but are you sure than you are an intermediate user when it comes web browsing? After all, if you use View Source enough to warrant it to be in a context menu (where the MOST used items for an particular object lie), you surely can read html, javascript, css, and associated technologies. That surely sounds like a set of advanced features, does it not?\n\n> but are somehow incapacitated from using Konqueror. Do you see?\n\nEr, I never said anything about incapacitation.  Please do not interpret \"optimization\" as such."
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "HTML was designed so that the dumbest of us could read and write it.  You'd be surprised at how many complete morons do that!  \n\nIt is true I admit that the Web is starting to be committee designed and standardized and now HTML is getting to be complicated but still the dumbest of us want to be able to use it.\n\nThink of me as that dumb kid maintaining his dumb homepage and stealing  my code from other websites.  I might be intermediate, but I'm not advanced.  Now with a single strike you are satisfying dumb, intermediate and clever users."
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-27
    body: "Well, it's not in the context menus, but you can just go to View --> Page Source, or hit \"Ctrl-U\" in Ephy to get the source.\n\n(visiting GNOME-er here - just thought I'd share. :)"
    author: "jck"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "Yup, view source was assigned a default ctrl-U when it was removed as part of KDE 3.2's context menu clean up. However, it was readded again recently*.\n\n\n* which I don't like- only advanced users and web developers will ever use it anyways in the mumble jumble code of the web that is the mix of html, javascript, arcane css, and buzzwords like \"XHTML\" and \"stylesheets\". Sure, it might have been fine in historical browsers, but it's time to move on from the historical unfriendliness of Mozilla and Netscape 2.0. "
    author: "je"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "Ctrl-U?  It's really time to stop this attitude of butchering Emacs/Unix shortcuts.\n\nPractically every other Linux application (GNOME, Mozilla, OO, whatever...) support Emacs shortcuts properly by default, except KDE.\n\nIt's time to stop the dumbing down of the Linux desktop.  Power to the user! "
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "> Practically every other Linux application (GNOME, Mozilla, OO, whatever...) support Emacs shortcuts properly by default\n\nReally how? \n\n> It's time to stop the dumbing down of the Linux desktop. Power to the user!\n\npower to the user=good\ndumbing down=bad\ndefaults optimized for intermediate users, rather than advanced users=good"
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-28
    body: "> Really how?\n\nI believe it is built into the toolkits by default.  Even Qt supports proper Unix shortcuts by default but KDE overrides it.\n\n> defaults optimized for intermediate users, rather than advanced users=good\n\ndefaults optimized for one niche group whatever you call them=BAD, BAD, BAD\ndefaults optimized for everyone = Good\n"
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2004-02-01
    body: "\"optimise for everyone\" is almost an oxymoron.  The only thing stopping it being one is the \"for\" in the middle."
    author: "TX"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "Umm, so do we... Right Click, Open With->Text Editor\n\nI like our solution better, as it gives us options and choice -- such that we can just as easily open a webpage in kword (import html) as we can with kwrite.\n\nNow if only my kwrite build from HEAD would work for me one day on freebsd :P\n\n--\nObligatory self-promotion - visit my website at http://tblog.ath.cx/troy"
    author: "Troy Unrau"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "I only have \"Open With...\" which pops up a Window and requires you to type or click a hundred times more...\n\nThat's another thing that's annoying and where KDE lacks in integration.  Konqueror should inline text, images, flash, etc automatically instead of making you open separate programs!!"
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "> That's another thing that's annoying and where KDE lacks in integration. Konqueror should inline text, images, flash, etc automatically instead of making you open separate programs!!\n\nUmm.. it does embed pretty much everything that you tell it to embed.It handles text, images, and flash. "
    author: "anon"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "In the default?  When I try to open .swf here it prompts me for an application.  Have you tried opening .swf?"
    author: "anonymous"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "Have you installed the flash-plugin? \nAnd let konqueror detect it?\n\nIf you had you wouldnt have that problem. If you dont like that solution, you can install the native flash-plugin for konqueror from kdenonbeta, but it only works half of the time."
    author: "Carewolf"
  - subject: "Re: Expocity"
    date: 2003-11-25
    body: "Yup, just make sure the mime type is correct."
    author: "Rinse"
  - subject: "Re: Expocity"
    date: 2003-11-26
    body: "we do have \"View Document Soruce\" in the context menu. i committed this to CVS last night, so you can all stop bitching about it now ;-P"
    author: "Aaron J. Seigo"
  - subject: "Re: Expocity"
    date: 2003-11-26
    body: "Yahooo!   Konqueror rewls the world.  :-)"
    author: "Navindra Umanee"
  - subject: "Re: Expocity"
    date: 2003-11-26
    body: "It seems to me that GNOME is about to get ahead in a number of areas forinstant information managmanent. That was my point.\n\nIf you forinstance look at Nat's work/ideas at www.nat.org  \n\ndashboard, human readable language searches etc, etc\n\n\nExpocity is a rip-off from OS X Panther. But the other stuff seams like home brewed. "
    author: "Swoosh"
  - subject: "Re: Expocity"
    date: 2003-11-26
    body: "dashboard is a lot like stuff i've seen in plan9 (at least i think it was plan9; it was a while back now). they called it \"plumbing\" though. \n\nnat's storage stuff is very WinFS-inspired.\n\nnot that that makes them automatically wrong. there are other good reasons they aren't, even as concepts, ready for prime time (IMHO). but this probably isn't the thread for me to get all ranty about that...\n\ni will say that KDE isn't exactly standing still."
    author: "Aaron J. Seigo"
  - subject: "Re: Expocity"
    date: 2003-11-26
    body: "FYI, Nat called dashboard a \"short-lived\" project implying that it was dead now."
    author: "Navindra Umanee"
  - subject: "DBUS is a load of arse"
    date: 2003-11-26
    body: "I think KDE needs fewer components, not more. I don't want to have a load of daemons loaded at boot time (which dbus is in the default debian installation) nor do I want a load of daemons loaded just because I am logged in to my desktop environment. If I have an empty desktop, I want only the window manager the panel, and the background drawing app to be loaded. And they should be idle, not polling away consuming CPU time.\n\nI don't want a load of complex interacting systems which I don't understand, because it leads to bloat, security issues, and it makes the environment look different depending on whether you are using apps which support all this extra stuff, or ordinary command line tools. For example, having distinct kio_slave processes has made this bug possible: http://bugs.kde.org/show_bug.cgi?id=63088\n"
    author: "cbcbcb"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: ">For example, having distinct kio_slave processes has made this bug possible: http://bugs.kde.org/show_bug.cgi?id=63088\n\nOK, so it is a bad thing to open 3 connections to one ftp-server?"
    author: "rinse"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "\nObviously yes. Public ftp servers can restrict the number of open connections and blacklist users who use too many, it doesn't work with round robin DNS, and it contradicts the user expectation which is that one FTP session in the app corresponds to one FTP connection to the server."
    author: "cbcbcb"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "Ok, but then kio is not to blame, Konqueror should contain an option to restrict the number of outgoing connections.\n\n"
    author: "Rinse"
  - subject: "file it"
    date: 2003-12-12
    body: "give them a bug report..."
    author: "Marcel Partap"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "So the right way is to let the developer of each application reimplement the functionality that it has in common with other applications, because you don't like it when two applications share the code?\n\nDo you think it is safer when 20 apps use 20 different HTML implementations, so the attacker can chose which one of the 20 he wants to attack?"
    author: "AC"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "\nI'm not complaining about code re-use, I'm complaining about having weird background daemons and excessive abstraction from the real system behaviour.\n\nApps sharing the same HTML implementation is something KDE does the right way (app links with khtml). Sharing the http implementation is done the wrong way, and it goes wrong in bizarre ways.\n\nEg, konqueror stops working; user kills and reload konqueror; still doesn't work; user uses ps and kills kio_slave; konqueror starts working.\n\nAlso, a more tightly integrated http implementation would have made it easier to fix the stupid problems with view source causing a file to be re-downloaded, which were in KDE for about 2 years.\n\n"
    author: "cbcbcb"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "Actually a less integrated and more centralized implementation would have prevented the re-downloading problem: the problem is that Konqueror and the text editor are two different applications. If they would use different HTTP stacks there is no chance that do not download it twice. Mozilla can only avoid it because it contains a text viewer. \n\nBut if there would be a single process responsible for all downloads, instead of separate KIO slaves, it could easily coordinate the caches and make sure nothing is downloaded twice (and even avoid other problems, like two applications downloading the same file at the same time).\n\n"
    author: "AC"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "\nThat's bullshit. Web browsers on RISC OS have no trouble whatsoever doing this and the text editors don't even have http functionality. View source works either by the browser saving the file to disk and instructing the editor to load it, or by a (slightly strange) direct transfer protocol.\n\nThe external slave mechanism needs direct communication between the browser and the cache to ensure that the slave knows exactly which objects are visible in the browser and need to be retained."
    author: "cbcbcb"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "I just tried, and my 3.1 Konqui saves the file to disk and then starts KWrite with that file. It has, however, other disadvantages: you get an ugly and useless file path in the editor. So you can not edit the HTML and then upload it, even if you have sufficient permissions.\n\n"
    author: "AC"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: ">Eg, konqueror stops working; user kills and reload konqueror; still doesn't work; user uses ps and kills kio_slave; konqueror starts working\n\nOk, but this indicates that you do know how it works (you know which subprocesses should be killed as wel to get the program back online).\n\nAbout konqueror, I remember Netscape in 1998, which had similar problems (sometimes if netscape crashed (and it crashed a lot those days) I had to restart my computer to get Netscape back online. \n"
    author: "Rinse"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: ">> I don't want a load of complex interacting systems which I don't understand\n\nExactly, you don't understand the reason it's done.\n\nAnd you don't, but lots of people do want it."
    author: "kaaskop"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "\nNo. I don't understand how it works, so I can't assess the security risk. And I definately do not want a system daemon running all the time even if no applications are using it.\n\nThe philosophy that users want is that something should happen if they instruct that it should happen, and nothing happens that they did not instruct. The fact that windows doesn't act like this is what makes people frustrated with it."
    author: "cbcbcb"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "But often messages are needed for functionality. For example when using hotplug-devices. Another example is kontact. Without dcop, kontact would end in a big bloatware."
    author: "Birdy"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "What's bad about a system daemon that's running when not used? When it's not used and doesnt do anything it takes a ridiculously small amount of physical RAM for the kernel entry in the process list and nothing more. In most cases the performance impact is positive, because the cost of keeping the process running (and swapping it out, in the worst case) is lower than restarting it."
    author: "AC"
  - subject: "Besides"
    date: 2003-11-26
    body: "Besides the fact that you would not even feel the difference and it allows applications to seamlessly integrate and talk to each other, D-BUS will most likely replace DCOP in some time. "
    author: "Alex"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-26
    body: "quote: The fact that windows doesn't act like this is what makes people frustrated with it.\n \n\nwell, I agree with this.. was the reason for me. KDE should restrict the amount of deamons as much as possible, imho."
    author: "Superstoned"
  - subject: "Re: DBUS is a load of arse"
    date: 2003-11-27
    body: "The problem in windows is not the architecture of the applications, but the lack of a good way of shutting them down without exposing users to the internal structure.\nMany programs use several processes, it's often a neccessary design if you want to avoid threads. The way to fix the problem is not to avoid using several processes (which are usually used for a reason, and not to annoy the user), but to create a higher level mechanism that knowns which processes belong together and shows them in a user-friendly way. Showing the name or path of the binary is a bad idea anyway, often there is not obvious connection between the icon that you clicked on your desktop and the binary path."
    author: "AC"
  - subject: "Standard Linux Software Installer"
    date: 2003-11-26
    body: "Okay, hear me out..\n\nWindows has Installshield (and possibly a few other installers).  But the point is, you can download one single installer, and install practically any application in Window's with ease!\n\nIn linux, its an entirely different story.  You've got Redhat 6.2 RPMS, Redhat 7.0 RPMS, Redhat 8 Rpms, Mandrake RPMS, Gentoo ebuilds... you get the point.  Its a nightmare on linux to install software that does not have distro specific packages....  Unless you want to compile the sources (but thats not an answer for everyone).\n\nCan't the KDE and GNOME (w/the help of FD.org) agree on a standard packaging system for KDE/GNOME?  This would give that system momentum and make it possibly a standard for linux.  A possible solution would be AutoPackage (see autopackage.org).  \n\nWhat do you all think?"
    author: "gururise"
  - subject: "Re: Standard Linux Software Installer"
    date: 2003-11-26
    body: "I'd say, read the interview about autopackage :)"
    author: "rinse"
  - subject: "Re: Standard Linux Software Installer"
    date: 2003-11-27
    body: "Havoc doesn't know anything about Autopackage."
    author: "Anonymous"
  - subject: "Re: Standard Linux Software Installer"
    date: 2005-03-11
    body: "Still, installing software on linux is a pain. Is there any reason why a standard software installer for linux doesnot exists?\n"
    author: "Anil Jagtap"
  - subject: "Re: Standard Linux Software Installer"
    date: 2005-03-12
    body: "No, there is no reason. It's just the inherent violence of the system."
    author: "Roberto Alsina"
  - subject: "Re: Standard Linux Software Installer"
    date: 2007-08-07
    body: "Installing software in windows requires me the following steps:\n\n- find the website of the producer of the software\n- find the download link, optionally register\n- proceed to download\n- relog into windows as administrator\n- install the software, optionally reboot the OS\n\nInstalling software on linux requires the following (current ubuntu, suse, fedora):\n\n- open the software installing tool by clicking on the menu\n- enter my own, or root's password, depending of the distro\n- find the software by typing it's name\n- click on install, and watch the tool resolving dependencies and install while I take a coffee\n\nLooks like the people complaining here are using linux distros that are 5 years old (even back then suse had the yast software installer)\n"
    author: "yglodt"
  - subject: "Re: Standard Linux Software Installer"
    date: 2003-11-27
    body: "InstallShield actually has java vers\u00edon which should work on Linux as well as on Windows. Never tried though, no idea how it works and naturally it doesn't integrate with native package management systems. Just thought to point it out :)"
    author: "huru"
  - subject: "Re: Standard Linux Software Installer"
    date: 2003-11-27
    body: "1. The Java version is for installing Java software\n2. It sucks, it just a way to install software into a directory and find the location of some resources. It doesnt solve any of the real problemsor doesnt do anything that Loki's Setup doesnt do..."
    author: "AC"
  - subject: "WE NEED THIS AMAZING PROJECT"
    date: 2003-11-27
    body: "Don't listen to havoc, he is clearly clueless as one of the Authors fo autopackage.org explains in the comments section. This project, once completed will be far better than any current solution today.\n\nRead the interview with AutoPackage.org's project leader:\nhttp://osnews.com/story.php?news_id=2307"
    author: "Alex"
  - subject: "Re: WE NEED THIS AMAZING PROJECT"
    date: 2003-11-28
    body: "Havoc clearly said he is not an authority on autopackage.\n\nOne concern I do have is how well it will play with rpm and deb. I think it is a very good solution for developer packaged apps which include all the libraries they need to run, and will like to be registered on the system. i.e., good for stuff like commercial apps. For things like distribution provided stuff, I do not see how it is better than the current systems. I would need to be educated more I guess."
    author: "Maynard"
  - subject: "Very well"
    date: 2003-11-28
    body: "Autopackage will attempt to use a package build specifically for the distro it is being installed on if available. SO for debian it will try to find a .deb for what is being installed and for SuSE a rpm so that the packages can be as integrated as possible."
    author: "Alex"
  - subject: "Re: Standard Linux Software Installer"
    date: 2004-08-26
    body: "Well if they want to compete with microsoft -- the big dogs they better come up with something as MOST of the windows users they want to capture as customers will ABSOULETLY NOT put up with comand lines and compiling ETC.  They either make linux user friendly for the casual computer user as most of their market will be or the can give it up now as they will never be more than a novelty.     "
    author: "mike"
  - subject: "Re: Standard Linux Software Installer"
    date: 2005-04-16
    body: "Completely agree Mike.  I think that if linux ever stands a chance, the software for it need to become distro neutral.  Autopackage has the potential to do this.  Autopackage will evolve into very powerful software as they are planning to integrate it with alh all current ways of installing software (RPM,DEB,ETC.).  Of course we have the gcc issues but I think that will work itself out eventually. Unfortunately developers have to change their software to support autopackage.  This means that if autopackage stands a chance, developers have to start supporting it.  But I think with the progress that autopackage is making, this will start to happen more and more.   Of course, the way to really get autopackage in quick development, and get the supported app list to grow, is if KDE integrates it into the next release.  This is an issue because autopackage isn't really ready yet.  But personally I think this is OK because they promise backward compatibility.  Of course this system also ensures that you will have the lastest software available.  You won't have to wait for an RPM that is most likely a few versions old.  Imagine having a front-end on your desktop that you can search a DB on kde-apps.org for software and install it just by clicking.  This would be awesome!  And it could monitor for newer versions for you!"
    author: "sleepkreep"
  - subject: "Re: Standard Linux Software Installer"
    date: 2006-08-26
    body: "A couple of additional possibilities for Linux installation:\n\nBitRock http://bitrock.com   Also for Windows, OSX, etc.\nLoki Installer http://www.lokigames.com/development/setup.php3  For Linux and other Unix platforms."
    author: "Daniel"
  - subject: "Re: Standard Linux Software Installer"
    date: 2007-03-14
    body: "I am a Linux Newbie and you guys are right . I am working to make the jump but I can not install the basic software to get started.As an ex windows moron I find this line item stuff a challange to quickly learn.. \n"
    author: "Tim P"
  - subject: "Re: Standard Linux Software Installer"
    date: 2007-08-07
    body: "I am also relatively new to linux.  I was raised on Windows, switched to Mac and now am making the move to Linux.  The only drawback to a complete switch is the fact that I have had to compile any type of software I wanted.  Why hasn't the group that determines what is included in the different kernel releases created a standardized system for software installation and just give it a name like linux installer.  This is mostly just venting so ignore the entire reply."
    author: "zkascak"
---
<a href="http://people.kde.org/waldo.html">Waldo Bastian</a> <a href="http://www.osnews.com/story.php?news_id=5215&page=5">talks about KDE's involvement</a> in <a href="http://www.freedesktop.org/">freedesktop.org</a> as part of <a href="http://www.osnews.com/story.php?news_id=5215">a larger interview</a> with several major players of the interoperability and common infrastructure movement.  Waldo knocks down certain myths and covers everything from <a href="http://accessibility.kde.org/">accessibility</a> to the <a href="http://developer.kde.org/documentation/library/kdeqt/dcop.html">DC</a><a href="http://developer.kde.org/documentation/tutorials/dot/dcopiface/dcop-interface.html">OP</a>-inspired <A href="http://www.freedesktop.org/Software/dbus">D-BUS</a>.
<!--break-->
