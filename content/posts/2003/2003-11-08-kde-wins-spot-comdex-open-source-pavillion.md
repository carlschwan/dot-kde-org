---
title: "KDE Wins Spot @Comdex Open Source Pavillion"
date:    2003-11-08
authors:
  - "chowells"
slug:    kde-wins-spot-comdex-open-source-pavillion
comments:
  - subject: "Sounds like a great opportunity."
    date: 2003-11-07
    body: "Neat. Go, George, go! :-)"
    author: "Eike Hein"
  - subject: "Plone?"
    date: 2003-11-08
    body: "What the heck is Plone, and how did it get more votes than KDE?"
    author: "Spy Hunter"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "<p>\nExactly my first question ;-).\n</p>\n<p>\nApparently, it's a CMS: <a href=\"http://www.plone.org/\">http://www.plone.org/</a>\n</p>"
    author: "Eike Hein"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "CMSes are a dime a dozen.  I've never heard of Plone, even though I've heard of every other project on that list other than the three bottom ones.  Is it really that much better than the other CMSes that it deserves its own booth at Comdex?  Are there even 1690 people using Plone to manage their site?  I don't see how it could get that many votes.\n\nNot that I hate Plone or anything, I'm just kind of curious who is voting for it."
    author: "Spy Hunter"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "> I'm just kind of curious who is voting for it.\nme too:\n\nPlone is build on CMF, CMF in it's turn is based on Zope, Zope is written in Python and needs a tweaked Apache (if I remember well). [dot.kde.org made with squishdot, which is also built on Zope, BTW]\n\nSo Plone is only a layer on top of a lot of (IMHO very good) code...\n\nI interpreted the 'victory' for Plone as a victory for all stuff underneath it, just as I think KDE is thanking part of it's glory to trolltech and it's cute-'Qt'-lib.\n\ncheers,\ncies.\n\nP.S.: Not that i wasn't thinking of a voting script in the first place ;-)"
    author: "cies"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "We use Zope and Plone for our complete intern and extern webmanagement and workflow. Its a quite complex but powerful tool. We use the zope server on localhost and use Apache virtualhost feature to bind urls to our zope webservices via zope's so called Vitual Host Monster.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "I agree. I voted in this poll a while ago when gnomedesktop.org was running something on it. I was surprised that Gnome had less than 1/3 the votes of KDE! I'm also shocked that OpenOffice had so many votes also. It's not a very exciting project, in my opinion. Too bad the OS booth couldn't have been cooler. KDE is definitely going to be the center of attention!"
    author: "Anonymous"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "[Mod's note: Wiggle again...]\n<!--\nuser-1835.bbd16tcl.dsl.pol.co.uk - - [08/Nov/2003:01:16:51 +0100] \"GET / HTTP/1.\n1\" 200 30695 \"-\" \"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6a) Gecko\n/20031029 Firebird/0.7+\"\nuser-1835.bbd16tcl.dsl.pol.co.uk - - [08/Nov/2003:01:17:15 +0100] \"GET /10681901\n82/ HTTP/1.1\" 200 35571 \"http://dot.kde.org/\" \"Mozilla/5.0 (Windows; U; Windows\nNT 5.1; en-US; rv:1.6a) Gecko/20031029 Firebird/0.7+\"\ndefiant.msoe.edu - - [08/Nov/2003:01:20:11 +0100] \"GET /1068239760/1068248959/1068249424/addPostingForm HTTP/1.0\" 200 7898 \"-\" \"Lynx/2.8.4rel.1 libwww-FM/2.14\"\ndefiant.msoe.edu - - [08/Nov/2003:01:21:18 +0100] \"POST /1068239760/1068248959/1068249424/ HTTP/1.0\" 200 3282 \"http://dot.kde.org/1068239760/1068248959/1068249424/addPostingForm\" \"Lynx/2.8.4rel.1 libwww-FM/2.14\"\n-->\n<!--\nWon't matter anyway, in a year KDE will just be a memory.  Novell will move Gnome into the enterprise desktop and KDE can go back to a basement project.  Nice knowing you guys.\n\nPlease don't bother with the quality argument, MS has proven that has no meaning anyway.\n-->"
    author: "Anonymous"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "This does qualify as mindless, destructive trolling, no? Mods?"
    author: "Eike Hein"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "I think this is very premature.  Novell purchased SuSE for its desktop distribution and tight integration.  I would venture to guess that they would destroy SuSE linux in the process of converting it wholesale to gnome.\n\nOn that note I'd be more mindful of Novell's past history (Word Perfect, UnixWare, etc..)  I'm thinking worst case senario the world will miss SuSE and Ximian, while KDE marches on, unencumbered by corperate agendas and missteps.\n\nJust my 2c though\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Thanks..."
    date: 2003-11-08
    body: "...well said.\n\nMuch better to reason than to mod trolls off."
    author: "cies"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "Well, yeah, you're probably right. In my defense, I'm quickly getting frustrated and I've heard above comment a lot over the last few days, sometimes even in worse, even more content-lacking versions. I just don't comprehend what people get out of behaving like that."
    author: "Eike Hein"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "here's my theory: a lot of people are personally invested in the various desktop projects and some of those people would be more than happy to see their favourite \"win\" through the destruction of the others. after all, if there is no competition, your favourite will always be the best, right? and if your favourite will always be the best, then your personal investment is guaranteed to be worthwhile and shrewd. you win, they loose. hooray! yes, this is shortsighted and sad, but not completely uncommon.\n\nthe reverse can also happen out of panic: something appears to be negative for your favourite, and because of the personal loss such a death-knell would represent were it actually true, one may get overly concerned or read the situation to be overly-negative.\n\ni think Sun Tzu would have a choice word or three for both sorts..."
    author: "Aaron J. Seigo"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "Well i think both sides are just plane nervous now.  I mean no-one knows whats going to happen.  Will it just fizzle out? Will it be the next big thing?  I think its enough to make people crazy.  I mean personally Id love to see KDE succeed.  I really dont care less if Gnome goes anywhere, but I know they push us to keep getting better.  I think the true tragity would be to have one or the other ignored, or worse, them molded into some mess that is not what the respective authors want.\n\nWell see... its going to be a fun year :)\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "Proven long-time abusers are not welcome here. That's all."
    author: "Navindra Umanee"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "You have more patence than me, i had to disable anon posting on kdedevelopers because we had some guy comming in and just posting garbage posts in past blogs...  it sucks but the internet is the worlds largest bathroom stall door :)"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "Yes, perhaps we can switch to plone from squishdot/zope ;)\n\nthere probably is even a migration path from various zope prods to plone.. =)"
    author: "anon"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "<p>\nHehe.  I thought I'd take a look at Plone.  So out of curiosity I did a search for \"squishdot\" on the site to see if there is a decent/backwards-compatible migration path.  \n</p>\n<p>\nGuess what?  The <a href=\"http://www.plone.org/Members/runyaga/news/News Item,2002-03-09,1015704695057735977/talkback/1015710958/view\">first 2 results</a> I found mention dot.kde.org in a rather flattering way.  :-)\n</p>"
    author: "Navindra Umanee"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "I don't think one person justifies changing the way the dot works though... and I dare say the dot works dang well.  :-)\n"
    author: "Navindra Umanee"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "[Mod's note: Baleeted.]\n<!--\nI'd be very interested to hear how you justify deleting any posts you don't like\nby loudly accusing them of being \"Wiggle.\" You *DO NOT* know that the\nposts you've seen recently are from that guy.\n\nAnd how do I know this?\n\nIt's because I made those original Wiggle posts (I find it amusing that \nthe name still strikes terror into the heart of KDE fanatics at thought of reading my opinions), and I did *NOT* however make any of the posts you have recently deleted.\n\nSo you explain why you think it is acceptable, as a administrator of this site,\nto run around using an old troll poster's reputation as an excuse to remove *any* content you merely disagree with (NOTE: Not offensive, or full of bad language)?\n\nBack when I was having fun here, you naively banned the Freeserve uk transparent web proxy from accessing the site (all FS UK users seem to have that IP), thereby blocking about 2.5 million web users in UK from  accessing dot.kde.org -- a hilariously incompetent move. After someone pointed out your ignorance, presumably, you unblocked the web proxy and instead prevented posting from the FS proxy... a move easily sidestepped by public proxies... and which, again, blocked 2.5 million people! Now you are still so petrified that people may read my opinions, you run around deleting huge numbers of other people's posts -- making yourself look like a fool and witless tinpot tyrant -- all in a mad attempt to silence a troll bogey-man from your past.\n\nIf there is a winner in the trolling game, then I am he. Today, I am a GOD... and you are my puppet. Dance, monkey-boy, dance.\n\n\nP.S. My reign of \"terror\" lasted all of about two weeks before I got bored -- long time abuser indeed. One wonders how long you've been using the \"Wiggle\" excuse for your rampages.\n-->"
    author: "Wiggle"
  - subject: "Re: Thanks..."
    date: 2003-11-08
    body: "[Mod's note: Don't feed the troll. :)]\n<!--\nInteresting, and now afterward what is your justification for your \"action\" before you stopped it due being bored?\n-->\n"
    author: "Datschge"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "Well, that's ximian spin on the story. SuSe has a market, Ximian is just a programming think tank. In Europe Linux means Linux/KDE. Customers request it. Linux isn't strong on the desktop because of Ximian but KDE.\n\nSo probably it will be more the other way round. \n\nMarketing: Ximian FUD: push - strategy give them Gnome\n           SuSE: Pull strategy - the customers request KDE\n\nGuess who will be the stronger part? \n\nI imagine that ximian will be forced to drop its KDE bullying and will be forced to provide proer interoperability.  \n\n\"Novell purchased SuSE for its desktop distribution and tight integration\"\nNo, for the market access."
    author: "Language"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "well i refuse to speculate until the cards are on the table, i mean there are too many unknowns that go both ways.  until someone says \"This is what we are going to do...\" its all wild armchair speculation :)\n\nin the mean time im going to try to make KDE kick ass...  \n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "nice try wiggie troll! "
    author: "anon"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "Yes, it's wiggle -- somehow with Lynx/shell access to defiant.msoe.edu although his DSL is apparently still in the UK."
    author: "Navindra Umanee"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "> Gnome had less than 1/3 the votes of KDE\n\nbecuase nobody reads gnomedesktop.org (in comparison to the old gnotices, of course =) )\n\n> It's not a very exciting project\n\nI disagree.. OpenOffice is the premier free software office suite, just like KDE is the premier free software desktop."
    author: "anon"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "OpenOffice is no Gnome project. Unfortunately they oriented the apllication suite more towards Gnome while KDE or QT integration is still poor. However, perhaps SOT Office may focus on this. "
    author: "Henno Harms"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "As others have said, Plone is a CMS based on Zope. While there are lots of CMS packages out there, Plone is modular and comes packed with a LOT under the hood such as workflows, user & group management, customized content types, cache management, clustering (run multiple servers behind a load balancer and have centralized management), etc. Needless to say, for larger sites that maintain a larger staff of content developers, Plone provides a fantastic foundation to allow the site to be managed. \n\nAs far as why it topped out the list, I am guessing that this has a lot to do with timing. The project just had a conference in mid-March which stirred up a LOT of interest .. I think around 150 people attended and over 800 people watched the live stream of the conference. In addition to this, on the plone.org website as well as mailing lists, etc.. the core development team persisted with reminders to get people to vote. \n\nIn anycase, with Plone v2 set to be released at the end of the month (or early next month), I think having such an impressive project at Comdex is a great thing. It sounds like there have been many major newspapers, government agencies and businesses that have used the Plone/Zope combo. It should provide some impressive case studies for people unfamiliar with these larger OSS projects to be made aware of them.\n\nNeedless to say, I am very excited that KDE will be there as well. KDE is the best FOSS desktop available and with the exciting new features in 3.2, it should make for an EXCELLENT Open Source Pavillion.. Hopefully there is ample time to make the booths inviting and fun to continue to open the eyes of others that have been less fortunate to experience FOSS first hand. :-)"
    author: "John Alamo"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "does it got something to do with slashdot? they called it \"they're own\" on they're site."
    author: "steve"
  - subject: "Re: Plone?"
    date: 2003-11-08
    body: "No, the story submitter called it his own, not the Slashdot editors.  It has nothing to do with Slashdot, which runs on its own Perl code (Slashcode)."
    author: "Spy Hunter"
  - subject: "COMDEX"
    date: 2003-11-08
    body: "Thanks for reminding me to register.  KDE will be in the exhibit area, right?\n\nI wonder how many people on the KDE boards and scene are going?  It would be nice if we could all get together for lunch or dinner one day.  Vegas is certainly the place for groups to meet."
    author: "Evan \"JabberWokky\" E."
  - subject: "Congradulations!"
    date: 2003-11-08
    body: "I'm suprised nobody mentioned that KDE came in ahead of GNOME by about 8,00 votes, which means that KDE got voted about twice as many times as GNOME did =)"
    author: "Alex"
  - subject: "Re: Congradulations!"
    date: 2003-11-08
    body: "Correction, KDE is ahead of gnome by 800 votes and got about three times as many votes as GNOME. I was ina hurry ;p"
    author: "Alex"
  - subject: "Re: Congradulations!"
    date: 2003-11-08
    body: "Yeah, but then dot.kde.org was the only site that openly encouraged its viewers to vote early and vote often.\n"
    author: "anon2"
  - subject: "Re: Congradulations!"
    date: 2003-11-08
    body: "Wrong, http://plone.org/newsitems/comdex"
    author: "Anonymous"
  - subject: "Re: Congradulations!"
    date: 2003-11-08
    body: "Actually, no.  The site that gnomedesktop.org linked to (O'Reilly) encouraged a daily vote."
    author: "anon"
  - subject: "Worked out Well"
    date: 2003-11-08
    body: "It's nice to see my favourite and voted-for projects up there! KDE is a given, but Zope together with Plone are two very good projects. CMSs are not a dime a dozen, particularly with the advantages that an environment like Zope provides."
    author: "David"
  - subject: "Is Plone really that good?"
    date: 2003-11-08
    body: "Is Plone really that good? Just tried their demo server at http://demo.plone.org and I got:\n\n502 Proxy Error\nProxy Error\n\n The proxy server received an invalid response from an upstream server.\n The proxy server could not handle the request GET /.\n\n Reason: Could not connect to remote machine: Operation timed out"
    author: "Anonymous"
  - subject: "Re: Is Plone really that good?"
    date: 2003-11-09
    body: "Timed out... I guess it has been very popular lately then :)"
    author: "Paul Eggleton"
  - subject: "Re: Is Plone really that good?"
    date: 2003-11-09
    body: "That wouldn't be a Plone thing so much as the Zope server on which it is built.  The server's been slashdotted though..."
    author: "Navindra Umanee"
  - subject: "Isn't PHP-NUKE better?"
    date: 2003-11-09
    body: "From what I heard, ti is very good."
    author: "Alex"
  - subject: "Re: Isn't PHP-NUKE better?"
    date: 2003-11-09
    body: "nope - php-nuke isn't quite as advanced as plone is. plone is really targeted more at professional/government/enterprise websites than php-nuke is, and as such, has goodies like full section 508 compliance and a quite modern optional design mode (which doesn't work with konq)"
    author: "anon"
---
KDE will be represented at the large US computer show <a href="http://www.comdex.com/lasvegas2003/">COMDEX</a>, in Las Vegas, from the 17th to 20th November, as a result of <a href="http://www.oreillynet.com/pub/wlg/3957">placing second in a poll</a> run by <a href="http://www.oreillynet.com">O'Reilly Network</a>. <a href="http://people.kde.org/george.html">Developer George Staikos</a> will be demonstrating the soon-to-be-released KDE 3.2, featuring a vast number of improvements in all areas, as well <a href="http://kolab.kde.org">Kolab</a>, the Free Software groupware solution. Thank you for voting for KDE and giving us this opportunity of introducing our desktop to a whole new audience.  See you there!
<!--break-->
