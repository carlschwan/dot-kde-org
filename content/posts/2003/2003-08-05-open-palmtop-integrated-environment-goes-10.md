---
title: "Open Palmtop Integrated Environment goes 1.0"
date:    2003-08-05
authors:
  - "oopie)"
slug:    open-palmtop-integrated-environment-goes-10
comments:
  - subject: "Sounds really great! "
    date: 2003-08-05
    body: "I'm definetely getting a Zaurus when tehy release the next version of it ;)"
    author: "Alex"
  - subject: "Good, but what is the difference between Q 1.7?"
    date: 2003-08-05
    body: "I know its based on it, but how is it better and how is it worse?"
    author: "Mike"
  - subject: "Isn't this REALLY old news?"
    date: 2003-08-05
    body: "Didn't this release occur in April?"
    author: "Ross Baker"
  - subject: "Re: Isn't this REALLY old news?"
    date: 2003-08-05
    body: "If you are referring to OPIE's web page, the date in the fromat DAY/MONTH/YEAR."
    author: "Anonymous"
  - subject: "ok, great"
    date: 2003-08-05
    body: "So how do I get it to my Zaurus?"
    author: "anon"
  - subject: "Re: ok, great"
    date: 2003-08-05
    body: "The easiest way I've found is to use the ROMS posted on http://openzaurus.org.  There isn't a version with OPIE 1 yet though."
    author: "Mark"
  - subject: "Desktop?"
    date: 2003-08-05
    body: "Sounds cool, but my question is... can I develop for opie on my linux desktop?  In KDevelop, even?  Or a similar tool?"
    author: "Lee"
  - subject: "Re: Desktop?"
    date: 2003-08-05
    body: "Yes, with a crosscompiler for ARM and using libraries such as QT-embedded, libopie and so forth. KDevelop will certainly not stop you from doing that."
    author: "Daniel Karlsson"
  - subject: "Re: Desktop?"
    date: 2003-08-05
    body: "No.. I mean, is there an emulator or \"qte-on-x\" or something, so I can develop for opie without actually owning a zaurus or whatever?  And will KDevelop support me, as opposed to simply not hindering me?\n\nFor example, if I'm developing a linux daemon, and kde/gnome clients, how do I go about adding an opie client to the fold, without actually buying a zaurus just to support it?\n\nI don't mean to find problems here, but this is really something I want to do.  And, I think it's important, with so many PDAs available, to have a generic way to develop for them all without owning them all."
    author: "Lee"
  - subject: "Re: Desktop?"
    date: 2003-08-06
    body: "You use qvfb, its pretty easy. It will run a virtual Qtopia/ Opie.  \n\nIf your app is CPU intensive, you won't get a realistic idea of how its going to work. \nEspecially if you use floating point a lot, you will be screwed. Switch to fixed point, which is, \nfor a lot of stuff, a bit hard. \n\nThere are some QT embedded templates for Gideon. I've not used them, dunno how it deals with QTDIR stuff for running it, etc. \n\n"
    author: "rjw"
  - subject: "5600 supported?"
    date: 2003-08-06
    body: "Is the z 5600 supported?"
    author: "Neal Becker"
  - subject: "Re: 5600 supported?"
    date: 2003-08-06
    body: "Simply said: yes"
    author: "Oliver Fels (Project Opie)"
  - subject: "Other ROMs and compatibility"
    date: 2003-08-11
    body: "To say that Opie is compatible with existing zaurus applications is a bit misleading as they've changed a lot of things and package management tools and have given up on their original mission of maintaining compatibility with the Zaurus for a bigger mission of supporting other devices.\n\nWe've gotten a tremendous response for our 100% Sharp Zaurus compatible ROM called theKompany.rom which is fully available for the 5000/5500/B500/5600.  The rom is totally free and available at www.thekompany.com/embedded/rom.\n"
    author: "Shawn Gordon"
---
The <a href=http://opie.handhelds.org/>Open Palmtop Integrated Environment</a> (Opie) project is pleased to announce its first 1.0 release. Having been forked from <a href="http://www.trolltech.com/">Trolltech</a>&trade;'s <a href="http://www.trolltech.com/products/qtopia/index.html">Qtopia</a>&trade; environment (<a href="http://www.trolltech.com/newsroom/announcements/00000137.html">version 1.7 of which</a> was also released today), Opie has emerged as the most sophisticated free and open graphical user interface for Linux-based embedded devices and PDAs. KDE users will recognize <a href="http://opie.handhelds.org/gallery/">familiar artwork</a> and applications such as <a href="http://www.konqueror.org/embedded/">Konqueror/Embedded</a> running on it. Synchronisation with KDE 3.1 is possible with <a href="http://www.handhelds.org/~zecke/kitchensync.html">KitchenSync</a>.
<!--break-->
<p>Opie features a sophisticated personal information (PIM) framework as well as several other productivity apps, extended multimedia capabilities and document model, networking and communication tools as well as multi-language support for more than a dozen languages.</p>
<p>Based on common industry standards such as XML, OBEX, and IrDA, Opie is capable of interacting with devices ranging from cell phones to server backends. Opie is highly optimzed for mobile devices and tries to support the user with shortcuts and ease of use.</p>

<p>No matter whether one would like to organize life issues, keep up to date reading daily news or requires a mobile internet accessing terminal, Opie provides all capabilities necessary for daily usage.</p>

<p>Key highlights of Opie:</p>

<ul>
<li> Binary compatibility with Sharp Zaurus applications</li>
<li> Sophisticated PIM framework (including an easy to use access API) with addressbook, todolist, today, mail, drawpad, datebook, texteditor and search-all-facility</li>
<li> Today application featuring plugins for dates, todos, mail, birthdays, weather and stock values</li>
<li> Multimedia capabilities provided by the Xine-based based opieplayer2 (featuring streaming audio and video) and image viewer</li>
<li> Palmdoc compatible opie-reader for ebook reading and, in cooperation with eg. jpluckx (<a href="http://jpluck.sourceforge.net">http://jpluck.sourceforge.net</a>) daily news coverage</li>
<li> Linux shell/terminal providing access to the operating system (for those who care)</li>
<li> Network based installation and setup management</li>
<li> PDF reader based on xpdf</li>
<li> IRC client, Konqueror web browser and mail reader</li>
<li> Multiple input plugins to ease data input</li>
<li> Network time support and time correction capabilities</li>
<li> Full network setup support through plugins (wlan, ethernet, ppp, irda)</li>
<li> Adaptive backlight settings and calibration (for devices light sensors)</li>
<li> Advanced security supporting Linux security services</li>
<li> Syncable with KDE PIM/Kolab, MS Outlook and Qtopia Desktop (3rd party tools necessary)</li>
<li> Voice memos</li>
<li> Backup and Restore capabilities to CF/SD cards</li>
<li> Data exchange with Palms, PocketPC, mobile phones and other bluetooth/IrDa capable devices</li>
<li> Fully localized</li>
<li> Themes and styles to adapt look and feel to your personal flavor</li>
</ul>
<p>Opie is designed to run on all Linux based devices. Images and packages are currently available for HP iPAQs, Sharp Zauri and SimPad. The platform has also proven to run on Psion5, Ramses, Tuxphone, Simputer and a lot of other embedded Linux devices.</p>

<p>We as the Opie team believe in the strengths of the software as an open productivity and business platform and will continue to develop it further -- new and exciting features are already planned. We would also like to thank our supporters and developers for making this 1.0 version possible.</p>
