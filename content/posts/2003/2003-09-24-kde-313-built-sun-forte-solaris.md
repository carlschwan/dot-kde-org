---
title: "KDE 3.1.3 built with Sun Forte on Solaris"
date:    2003-09-24
authors:
  - "ebrucherseifer"
slug:    kde-313-built-sun-forte-solaris
comments:
  - subject: "kdenonbeta/nonlinux"
    date: 2003-09-23
    body: "Ah, that's great. Congrats!\n\nBTW:\nWe have just created\n  kdenonbeta/nonlinux/patches/hpux\nSo feel free to put any additional patches, howtos, compiler options or whatever in kdenonbeta/nonlinux/patches/solaris."
    author: "Marcus Camen"
  - subject: "Re: kdenonbeta/nonlinux"
    date: 2003-09-24
    body: "Why do you need a module for that?  Can't you put it in KDE CVS HEAD?"
    author: "ac"
  - subject: "Re: kdenonbeta/nonlinux"
    date: 2003-09-24
    body: "They are mostly for problems with the compiler and shouldn't go into the mainline.  They will be reviewed and if suitable for the mainline, they will go in over time."
    author: "George Staikos"
  - subject: "great news!"
    date: 2003-09-23
    body: "Solaris 9 KDE would be nice.\nSolaris is a quite stable OS but i really hated the UI.\nKDE surely is a good thing for Solaris.\n\nAnd it is just more proof of open source portability.\n\nnice work guyes !"
    author: "Mark Hannessen"
  - subject: "gcc3.x vs forte"
    date: 2003-09-23
    body: "How does it compare to KDE compiled with gcc3.x on solaris?  Any speed boost?"
    author: "Anonymous"
  - subject: "Good thing...."
    date: 2003-09-23
    body: "Hi,\n\nit's always a good idea to get things working on as many compilers as possible. You get better code quality and even catch undiscovered bugs doing that.\n\nDid anybody compile KDE with icc yet? \n\nYours, Kay"
    author: "Debian User"
  - subject: "very interesting"
    date: 2003-09-23
    body: "Hello,\n\nThis is very interesting experience.\n\nI am interested to know making KDE compile without GCC but with the OS native compler does it lead just to C++ code related issues or also replacing some glibc dependency with the libs that Sun provides?\n\nCongrats,\nAnton"
    author: "Anton Velev"
  - subject: "Why was this so hard?"
    date: 2003-09-24
    body: "I appreciate the work but I'm surprised it was such a major undertaking. Shouldn't the code be at least as portable between compilers as it is between platforms? Does automake/autoconf or some other part of the build system have a problem with Forte?"
    author: "Otter"
  - subject: "Re: Why was this so hard?"
    date: 2003-09-24
    body: "Most of the problems tend to be due to broken C++ compilers.  I don't think I've met a non-broken C++ compiler. :-)  GCC seems to be one of the best now though."
    author: "George Staikos"
  - subject: "Re: Why was this so hard?"
    date: 2003-09-25
    body: "The problems are not with SunProCC, all the problems were caused by non-standard GNU/GCC-isms which are abundant in KDE. They may make code writing somewhat easier, but they make porting to strict compilers very difficult.\n\nSunProCC 7 and 8 track the C++ standard very closely.\nFor those who doubt this statement, please visit:\n\nhttp://www.jamesd.demon.co.uk/csc/faq.html\n\nand note the email addy of the Chairman of the ANSI/ISO C++ Standard Committee.\n\nThe performance improvement on SPARC using SunProCC is 2.5-3x over gcc.\n\n--Stefan\n"
    author: "Stefan Teleman"
  - subject: "Re: Why was this so hard?"
    date: 2003-09-24
    body: "You've obviously never had to port some C++ code from one Unix to another. It's a black art which can leave you very frustrated and unsure of how to proceed. Apart from Compiler differences you also have to deal with different underlying libc implementations and posix calls changing. Plus some compilers define things using built-ins rather than include files. It's something you get better at with practice but it's by no means trivial."
    author: "Nick"
  - subject: "Re: Why was this so hard?"
    date: 2003-09-24
    body: "I've been working with the native KDE on MacOS X project, so I definitely realize how hard moving between platforms can be. But this is a case where the code has already been ported to the new Unix. I'm surprised that then switching to a new compiler was so difficult. \n\nI'm not arguing with or insulting the guy who did it -- just expressing surprise that there were so many obstacles."
    author: "Otter"
  - subject: "Re: Why was this so hard?"
    date: 2003-09-29
    body: "The problem is when you switch compiler you switch a lot of other things without realising it. The libc will almost certainly change as does the C++ STL library implementation you are using. Your linker will change and your assembler. Lots of compilers implement certain functions as built in as well. Porting to a different compiler can be harder than porting to different hardware.\n\n"
    author: "Nick"
  - subject: "Re: Why was this so hard?"
    date: 2003-09-24
    body: "The problem is that the KDE developers seem to like to use the latest C++ \"features\" instead of keeping it simple. So the compiler has to be fully up to date with the latest C++ featurecreep spec, or things dont go too well.\nThere is lots and lots of C++ code out there that has no problems with sun forte C++. even the old 6.2 release. But QT +KDE is a nightmare with it.\n\n"
    author: "Philip Brown"
  - subject: "actually usable?"
    date: 2003-09-24
    body: "As sparcs aren't that state of the art CPUs, how fast is it? Usable with 400MHz/192MB U5 WS?"
    author: "jmk"
  - subject: "Re: actually usable?"
    date: 2003-09-24
    body: "I had KDE 2.x on a Sparc 4 (175Mhz, ~128MB memory), compiled with g++ some years ago and it was slow, but usable. But I admit, that I compiled it on an UltraSparc server... Later I got an Ultar5 which was around 400-500Mhz and 256MB RAM and that was quite OK. See my experience about it on http://developer.kde.org/build/solariscompile.html, just don't ask me for the X11 headers as I don't have them anymore (notice the date at the top!)\nWith the native compiler I would expect to see faster code and less compilation time, but I may be wrong.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: actually usable?"
    date: 2003-09-25
    body: "KDE was built for UltraSPARC specific v8plusa ISA with aggressive cache prefetch and O5 optimized code.\n\n--Stefan\n"
    author: "Stefan Teleman"
  - subject: "SUNWkderequired"
    date: 2003-09-25
    body: "This sure is a great job done. But I have one question. While KDE and qt is placed in the /opt tree, the software from SUNWkderequired is placed in the /usr/local tree. Why not put this in the /opt tree too? I maintain a /usr/local structure I wouldn't like to 'taint'. If the SUNWkderequired package had a base like /opt/KDErequired most people would be able to useit without 'tainting' an existing system."
    author: "Lasse Aagren"
  - subject: "Re: SUNWkderequired"
    date: 2003-09-25
    body: "I know that the required libraries can cause this problem for\nexisting installations in /usr/local. The problem is that these\npackages have to be installed somewhere, and putting everything\nin /opt might cause the partition to fill up. The other thing is,\nno matter where the pkg would default its root install, there\nwill always be the chance that one cannot use that particular tree.\n\nUnfortunately, there's no easy win in this. :-)\n\nYou can relocate a Sun pkg with pkgadd -R. If you do that, you need\nto do a little bit of hacking of the *.la files and of your ld.so:\n\n1. replace all instances of the string '/usr/local' in the KDE *.la\nfiles with the relocated path (i.e. '/opt/kderequired').\n2. create an alternate ld.so cache and an alternate LD_CONFIG file\nfor the shared libraries which used to reside in /usr/local --\nuse crle(1) to do this\n3. define the relocated directory as a trusted directory for ld.so -- also\nuse crle\n4. explicitly set LD_CONFIG at the top of startkde to this new ld.so.config\nfile.\n\nI realize this is painful, but, no matter where i place these libraries,\nthere will always be someone who cannot use that directory and needs to\nrelocate.\n\nI promise that installation for 3.2 will be a lot less painful. :-)\n\n--Stefan\n"
    author: "Stefan Teleman"
  - subject: "Re: SUNWkderequired"
    date: 2003-09-25
    body: "Thanks for the answer. I'll try your solution.\n\nBut for the sake of the debate :)\n\nI think that /usr/local is a tree which most people uses in some kind ore another,  so dropping files here are likely to conflict with something.\n\nYou say that /opt might fill up. This can be solved by simple symbolic links to another location which hold enough space. And by giving the package a unique /opt/SUNWsomething root it will most likely not conflict with anything. Just my two bits :)\n\naagren"
    author: "Lasse Aagren"
  - subject: "Should be called CSWkde"
    date: 2003-09-26
    body: "The packages really should be named CSWkde since they're not packaged by Sun.\n\nCheerio."
    author: "Anonymous"
  - subject: "Re: Should be called CSWkde"
    date: 2003-09-26
    body: "These packages are not CSW.\n\nThe naming convention is the one recommended by\nSun Microsystems in their Answerbook collection,\n\"Building Packages\".\n\nCheers.\n\n--Stefan\n"
    author: "Stefan Teleman"
  - subject: "Re: Should be called CSWkde"
    date: 2003-10-01
    body: "Can you point to what you're referring to with a URL?\n\nIf my understanding is correct, CSW = Contributed Software for which\nthis is contributed on top of Sun's SUNW packages (prefix SUNW).\n\nThis guy has the right idea:\nhttp://www.blastwave.org/packages.php/kdebase_gcc\n\nPlease clarify."
    author: "Anonymous"
  - subject: "Re: Should be called CSWkde"
    date: 2003-10-01
    body: "> Can you point to what you're referring to with a URL?\n\nI can point you to the AnswerBook Collection which is available\non Solaris, if installed. Search for \"Building Packages\". If you\ndo not have AnswerBooks installed, you may search on Google for\nsomething available.\n\n> If my understanding is correct, CSW = Contributed Software for which\n> this is contributed on top of Sun's SUNW packages (prefix SUNW).\n\nNo. This is blastwave's packaging standard. The KDE packages were not\nreleased through blastwave, nor were they packaged for blastwave. There\nis no relation or connection whatsoever between the packages available\nat KDE and blastwave. These packages were built by me, specifically for\nKDE, and they were kindly made publicly available by KDE.\n\nThe /opt/csw tree is reserved by blastwave, and so is the 'CSW' package\nprefix. KDE software installs under /opt/kde*.\n\nWhether or not i choose to release KDE packages through blastwave,\nin addition to the KDE packages, it is something to be decided\nin the future, and that will only happen with KDE's consent. At that\npoint in time, packages released through blastwave will most likely\nobserve blastwave's package naming standards.\n\n> This guy has the right idea:\n> http://www.blastwave.org/packages.php/kdebase_gcc\n\nread my comments above.\n\n\n"
    author: "Stefan Teleman"
  - subject: "Are all the things in SUNWkderequired really req'd"
    date: 2003-10-03
    body: "I'm in the process of installing the packages. However, I'm shocked at the size of the SUNWkderequired package - and some of the things in it. Do we really need Opera, AfterStep, BlackBox, pico, pine, xv, and more to run KDE? - I don't have those things installed on my Linux system at home, and KDE runs just fine, thank you.\n\nTrimming out some of the stuff like this should help with the size, no?"
    author: "Grant McDorman"
---
For the first time ever, KDE is available for SPARC Solaris compiled with Sun's own Forte compilers. Thanks to the hard work of <a href="mailto:steleman@nyc.rr.com">Stefan Teleman</a> you can download <a href="http://download.kde.org/stable/3.1.3/contrib/Solaris">Solaris packages here</a>.   Read on for details as to how the Sun Forte port began and how KDE finally managed to get a maintainer for its Solaris packages.  Those of you with an interest in Solaris and KDE may also wish to subscribe to the <a href="http://mail.kde.org/mailman/listinfo/kde-solaris">kde-solaris mailing list</a>. 
<!--break-->
<p>
<hr>
<h2>KDE 3.1.3 on Solaris built with the Sun Forte compilers is out</h2>
<i><a href="mailto:steleman@nyc.rr.com">Stefan Teleman</a></i>
<p>
First off, many thanks to Eva Brucherserifer, Dirk Mueller,
and to everyone at KDE and the KDE Solaris community for their
kind support.
<p>
This project of mine has been a long time in the making. I started
thinking about it about one year ago, during a brunch with one of
my very good friends. I had taken the subway to Park Place, Brooklyn
to meet him for brunch, on a hot late July Saturday. He, and
his family were going back to France, so this was going to be
one of the last times we could hang out.
<p>
At the time, I was looking for the "next thing" to do. I had been
using KDE for a while, on Linux, and the idea popped in my head,
what if it were available for Solaris with the native Sun compiler.
I remembered seeing a 2.2 gcc version on someone's Sun box at work
about a year earlier.
<p>
So, while chatting at brunch, i mentioned to Seb "what if i ported
KDE to Solaris with the Sun compiler". His initial reaction was:
"You're crazy. Do you realize the time commitment. What if it can't
be done." I was actually asking myself the exact same questions.
<p>
Three weeks went by, we met again for brunch. This time around i
didn't mention anything about KDE. Out of the blue, he asked me:
"what about KDE." i said "i'm going to do it. Sun is selling
refurbished workstations on eBay. i ordered one yesterday". He
didn't say anything, but i could tell what he was thinking. 
<p>
That was at the end of August 2002.
<p>
The box didn't show up until late October, and i started working
on KDE in November 2002. It was a clean slate start. My idea was
to build everything with Forte, including the additional libraries,
in order to get the best performance on Sparc. I also knew from
previous experiences that mixing compilers (gcc and SunProCC) does
not really work well.
<p>
It took me 6 months to get the first workable release out (3.1.1,
April 2003). A lot of work went into it, all of it done in the
evening and on weekends. Wasn't anywhere near fully functional,
but it mostly sort of worked. Except html rendering and multimedia.
<p>
I kept working on 3.1.2 trying to find out where the html rendering
problem was. Dirk Muelller ssh'ed to my Sun box in New York City from
Germany and we debugged kthml together.
<p>
3.1.3 came out pretty nicely.  It's fast, it's very stable and Keramik
looks beautiful. It's truly the best desktop.
<p>
I am trying to get another Sun box with Solaris 9, which comes with
XRender and XFreeType. As soon as that happens, i will make KDE  
available for Solaris 9 as well.
<p>
My plans for 3.2: kdemultimedia to work, and write a friendly
installation program. And one more thing: ask my doubting friend how
KDE Solaris is working for him. :-)
