---
title: "Obliquid: Search Through KDE Translations"
date:    2003-05-05
authors:
  - "Stefano"
slug:    obliquid-search-through-kde-translations
comments:
  - subject: "KBabel Dictionary"
    date: 2003-05-04
    body: "How does this compare to KBabel Dictionary? Any advantages?"
    author: "Anonymous"
  - subject: "Re: KBabel Dictionary"
    date: 2003-05-05
    body: "It really doesn't want to compete with Kbabel Dictionary: it's just a simple search. Some months ago I wrote a small parser to read PO files and store them in a database. I wanted to be able to search for my open source application  messages, and I wanted to be able to do so on Linux, Windows or on whatever computer I was using at the moment.\n\nThen, since I had the parser working, I used it to import compendium files. I  thought that since it was useful for me, it could be useful for others to have such thing online. It's just a little script, on the site you can click on the PHP source and on the Smarty Templates buttons and view it. Anybody that may be interested to improve it can ask me for an account and edit it online. \n\nStefano"
    author: "Stefano"
  - subject: "Re: KBabel Dictionary"
    date: 2003-05-06
    body: "I believe translation assistance technology is very important. There are many that would like to translate software but less that understand the tranlation process or CVS ecc. \n\nI believe grid translation is a good approach, also for larger text translation e.g. documentation. Everybody downloads a piece of text from a server, translates it with his client tool and resummits the changes. Then there is a grid review process that commits the translation efforts.\nIt is simply too hard to translate a whole text but everybody can spent 5 minutes to translate a paragraph."
    author: "Andr\u00e9"
  - subject: "Re: KBabel Dictionary"
    date: 2003-05-07
    body: "I agree with you. I made online a first test of a Wiki Wiki style translation!!\n\nIn the KDE message search page I just added a link called \"Help to translate a sentence in Obliquid\". After choosing a language (now only Italian, Traditional Chinese and Spanish), you will be moved to a random sentence to translate. Submitting the form will store the translation and you could translate a new message or end your work.\n\nThere are of course big rooms of improvements, but you get the idea."
    author: "Stefano"
  - subject: "Translation"
    date: 2003-05-04
    body: "Is there a translation of KDE to Latin somewhere?"
    author: "Somebody"
  - subject: "Re: Translation"
    date: 2003-05-06
    body: "Hope there will be a lower German (ISO: nds) version soon. J\u00fcrgen Peters translates Gnome with Institut f\u00fcr niederdeutsche Sprache, Bremen."
    author: "Bert"
  - subject: "Search Through KDE Translations - Improvements"
    date: 2003-05-05
    body: "Is it possible to support different languages to be searched as well? It really would be helpfull, if you could search for the italien translation of a german message for example. Also it would be great if there would be more supported languages. So some other important (european) languages such as frensh or polish could be searched.\n\nBut anyway thanks for your effort!"
    author: "cylab"
  - subject: "Supporting other languages"
    date: 2003-05-06
    body: "I will probably add French when i18n.kde.org will be up again. It seems this may take a week. Going directly from a language to another seems interesting, but it can be also dangerous. \n\n"
    author: "Stefano"
  - subject: "Re: Supporting other languages"
    date: 2003-05-06
    body: "Hmm, machine aided translations are dangerous by definition, as seen by using babel.altavista.com :) Displaying three columns may help in such situations.\n\neg:\n \u00f6ffnen (german) => open (english) => abrir (spanish)"
    author: "cylab"
  - subject: "Re: Supporting other languages"
    date: 2003-05-06
    body: "Yes, I agree, infact. \n\nKDE i18n site is back online, so I've added French language too."
    author: "Stefano"
  - subject: "Re: Supporting other languages"
    date: 2003-05-08
    body: "nice! looking forward to further work of you !"
    author: "cylab"
---
In the <a href="http://dev.obliquid.com/index.php?page=misc_support">support section</a> of the <a href="http://dev.obliquid.com/">dev.obliquid.com</a> site, I've added the possibility of searching through KDE .po compendium files.  So far I've loaded the Italian, Spanish, Traditional Chinese and German compendiums. This can be of help to <a href="http://i18n.kde.org/">translators</a>, since it provides a web interface to search through the many thousands of already translated messages (nearly 200 000).
<!--break-->
