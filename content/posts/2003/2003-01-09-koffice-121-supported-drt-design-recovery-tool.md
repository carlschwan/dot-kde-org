---
title: "KOffice 1.2.1 Supported by DRT Design Recovery Tool"
date:    2003-01-09
authors:
  - "uteam"
slug:    koffice-121-supported-drt-design-recovery-tool
comments:
  - subject: "Nifty!"
    date: 2003-01-09
    body: "This is outright *awesome*!\n\n/kidcat"
    author: "Anders Juel Jensen"
  - subject: "Source instrumentation needed"
    date: 2003-01-09
    body: "Yep, this looks great.\n\nIf only it could be turned into a valgrind plugin ... so that no source instrumentation is needed.\nYes I know, we are being spoiled and lazy since valgrind exists :)\n\n(The point is that the least time it gets to set it up, the most useful it is. If it takes too much time to set it up everytime one needs it, other debugging techniques will still be preferred).\n\nDavid, who has bad memories of the instrumentation recompiles for Insure...\n(and the docu for DRT indicates  that it's the same kind of nightmare there....)"
    author: "David Faure"
  - subject: "Re: Source instrumentation needed"
    date: 2003-01-11
    body: "Hi,\n\nWe will look into valgrind.  However, even if it does\nallow us to obtain (caller,callee) information upon\nevery function call and exit --- including those in shared \nlibraries and without too much of a slow down in the application\nexecution --- we would still need to recompile the application \ncode.\n\nCurrently, DRT patches the source of the application to make\nit run in a way suited to DRT.  In particular, this involves\ntwo things:\n\n* having a non-blinking cursor in a color different from the\n  rest of the application display; this allows us to \n  easily track the cursor from screenshots (so that we can\n  zoom in on the user's focus in an action)\n\n* preventing frequent polling, which currently confuses the\n  DRT action detection mechanism\n\nWhile we hope to remove the second requirement soon, the cursor\nissue will still remain.  One could imagine heuristics that automatically\nfind the cursor, but what happens if the blinking cursor was off during the screenshot?  \nAlso, what happens if the application uses a non-standard cursor, such as kspread?\n\nTo alleviate the burden of instrumentation, we have already provided\nscripts to automate the procedure for LyX 1.2.1 and KOffice 1.2.1. \nWe hope to have more scripts soon.\n\nIn the near future, we plan to provide p2p networking so that users\ncan easily search and browse action collections created by others.\nThis means that it is possible to use the tool to understand the design\nof an application without having to do any demonstrations/instrumentation yourself.\n\nWe also plan to provide collaborative commenting, so not only would users\nshare their action collections, but they could also comment on code that\nthey have finally understood and others would benefit from those comments/analyses as well.\n\nUNSW DRT Team"
    author: "UNSW DRT Team"
  - subject: "Re: Source instrumentation needed"
    date: 2004-11-18
    body: "Where does the \"kde\" come in on your title to this?\n\nWhere are you located?\n\nI run a small business in Glastonbury, CT and sell instrumentation.\n\nMy wife is Kathleen, so our initials spell KDE.\n\nPerhaps you will be so kind as to reply to this Email, to satisfy my curiosity.\n\nWill you please?\n\nThanks, Don"
    author: "Don Eberhardt"
  - subject: "Re: Source instrumentation needed"
    date: 2004-11-18
    body: "How about visiting http://www.kde.org ?"
    author: "Anonymous"
  - subject: "Wow"
    date: 2003-01-09
    body: "Pretty neat."
    author: "shm"
  - subject: "Frightning - i do not understand anything"
    date: 2003-01-09
    body: "Although i took a look at the webpage i do not understand\nhow and when to use. I hope i am not the only one. \n\nMay be someone is so kind and could explain it in a dummy proof way ;o)\n\nFor example i do not know what is meant by execution bursts.\n\nIf i understood correctly \"execution burst consists of the methods called\" \nmeans that the programm checks code-execution on the code/programming base ?!\n\nDoes this mean DRT will help you to pay attention to the more important code lines and to improve them first ?\n\nThanks for any reply.\n\nKudos to the kdeteam for 3.1 ASA gentoo 1.4 is out i will install it at my home \nmachine.\n\nbest regards.\n\nsincerly your anonymous coward"
    author: "anonymous coward"
  - subject: "Re: Frightning - i do not understand anything"
    date: 2003-01-11
    body: "Hi,\n\nIt's hard to explain in only a few words.  We recommend\nreading our paper:\n\nhttp://citeseer.nj.nec.com/536115.html\n\n(You can download it in several formats or view\n it as a graphic; see the links in the top right corner.)\n\nThis should give you a better idea of what we are trying to achieve,\nincluding future work. The paper contains some technical material,\nbut this can be skipped if you are just interested in understanding\nthe point of the tool.\n\nUNSW DRT Team"
    author: "UNSW DRT Team"
  - subject: "Please don't do this"
    date: 2003-01-11
    body: "Please don't write \"X Windows\": man X"
    author: "Manfred Tremmel"
---
<a href="http://www.cse.unsw.edu.au/~drt/">DRT</a> is a design recovery tool for interactive graphical applications running under X Windows. The tool automatically captures actions performed while using such an application. Functions particularly relevant to each action are highlighted. Moreover, the action itself is described 
<a href="http://www.cse.unsw.edu.au/~amichail/kword.png">visually</a> from fragments of the application display. One can search and browse these actions to learn about the design of an application. The latest version, <a href="http://www.cse.unsw.edu.au/~amichail/DRT-0.2.2.tar.gz">DRT 0.2.2</a>, now supports shared libraries, which allows applications such as those in KOffice to work. This version also includes template drt files for all KOffice 1.2.1 applications for easy instrumentation
<!--break-->
