---
title: "KDE Traffic #61 is Out"
date:    2003-08-13
authors:
  - "rmiller"
slug:    kde-traffic-61-out
comments:
  - subject: "Nothing like your weekly news fix."
    date: 2003-08-13
    body: "Quality release of KT; much appreciated."
    author: "Sho"
  - subject: "Two to three hours a week?"
    date: 2003-08-13
    body: "This is what many developers spend per day on KDE."
    author: "Anonymous"
  - subject: "Re: Two to three hours a week?"
    date: 2003-08-13
    body: "Actually i spend every second of my day toiling at kde in hopes some user will beg me for some pet feature so they wont have to spend the $199 on windows XP...\n\nNo really guys its a joke, laugh :P\n\nPlease keep in mind KDE is a volenteer effort.  People develop when and if they can.  If something is not happening fast enough for ou whip out your C++ compiler and pet text editor and go to town.  We always love fresh meat!\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Two to three hours a week?"
    date: 2003-08-13
    body: "And don't think it's not appreciated.  However, that doesn't change the kde-traffic situation."
    author: "Russell Miller"
  - subject: "comment"
    date: 2003-08-13
    body: "\"I'm cool with having that break. If a user is dumb enough to keep the config dialogs of two KMail's open at the same time, then he shouldn't complain if the second-to-be-closed one overwrites the settings of the first-to-be-closed one. I don't see a need to make that work.\"\n\n\n'If a user is dumb enough' - argh, awful.\n\nAnd also, the dialog boxes should be modal. I HATE dialog boxes that aren't modal - they end up floating around, pissing you off after you thought you closed the main app window.\n\nIt seems some developers are dumb enough to make their settings dialog boxes as normal windows.\n"
    author: "foo"
  - subject: "Re: comment"
    date: 2003-08-13
    body: "\"It seems some developers are dumb enough to make their settings dialog boxes as normal windows.\"\n\nThere are certain situations where it is quite handy to not have the settings windows be modal. Take the Gimp for example. The settings dialogs for tool configuration are not modal. This allows you to leave the settings for your paint brush open while still painting. When you want to change a brush parameter you don't need to go to a menu again. Instead you can just click on the already open dialog box and make your changes."
    author: "Bob"
  - subject: "Re: comment"
    date: 2003-08-13
    body: "You cite out of context. We talked about the problem that the user might open several config dialogs in several different running instances of KMail (of course at most one config dialog per instance). It's irrelevant whether this dialog is modal or not because it's always at most modal with respect to that instance of KMail it belongs to.\n\nBTW, Konqueror and other programs which allow several running instances already have this problem today. The user can open two config dialogs in two different instances (for Konqueror the instances have to run on different machines), then make changes in both and then apply the changes in both. I'd be impressed if any KDE program (or any other program for that matter) could handle this situation."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: comment"
    date: 2003-08-13
    body: "KWallet can! :-)"
    author: "George Staikos"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "Even if two instances are running on different machines? Nice. Not that it would be difficult to watch the config files for changes, but this has to be implemented in KConfig."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "GConf - Gnomes config system does this. \n\nFreedesktop ahve plans to make a DBUS based config daemon to replace it, and use that in Gnome. Hopefully that can be used in KDE too. And DBUS can be used to replace DCOP.... "
    author: "rjw"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "There are no plans to replace DCOP with DBUS either in KDE3 or KDE4."
    author: "anon"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "It's an inevitability.  Mark my words that KDE4 will use D-BUS."
    author: "ac"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "Why should it? KDE could also found a desktop satndardization group and declare DCOP to be *the* desktop protocol. And then?\n"
    author: "AC"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "When KDE does it I'll believe it.  I also don't see anyone in KDE trying to standardize DCOP while the GNOMEs seem to do a lot of work getting their stuff standardized."
    author: "ac"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "DCOP uses a lot of QDataStream methods to marshal data in a portable way... this is simply not doable (easily) in non Qt applications.\n\nHowever, even if DBUS is standardized within freedesktop, that doesn't mean KDE will use it. I seriously doubt that dcop will be replaced with dbus in kde4. There might be a bridge though."
    author: "fault"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "1. It's easily done outside KDE applications -- there are even C-only bindings to \ndcop in kdebindings. A true issue is versionning/format revisions, but that's solvable.\n\n2. FreeDesktop.org is *not* a standards organization. It can't make standards. If\nonly Gnome uses something, it's not a standard, period. \n"
    author: "SadEagle"
  - subject: "Re: comment"
    date: 2003-08-14
    body: "Accessing wallets remotely is actually impossible, on purpose.  The only way to really do that would be insecure, and would require XMLRPC or similar.  This uses @internal design of kwallet though.  The external, supported API is very rigid about this."
    author: "George Staikos"
  - subject: "Re: comment"
    date: 2003-08-13
    body: "I really love modal dialog boxes on Mozilla when the MailNews component prompts me, for the millionth time, for my nntp login and password. Can't go back to my inbox (at work) and grab the account information. If I close the modal box, the message I clicked on never downloads. Obviously this is the fault of a buggy app but give me non-modal windows any day!"
    author: "Vigster"
  - subject: "Re: comment"
    date: 2003-08-15
    body: "I think they shouldn't be really modal.\nThey should mostly behave like a normal window, but if they are called a second time, bring back the first window instead of opening a second.\nIsn't that mixture possible?"
    author: "CE"
  - subject: "Make a Donation"
    date: 2003-08-13
    body: "KDE also accepts donations: http://www.kde.org/support/#Money"
    author: "Anonymous"
  - subject: "Re: Make a Donation"
    date: 2003-08-13
    body: "And I'd encourage you to make a donation there as well.  The KDE project is something which I obviously get behind and would like to see succeed as much as possible.\n\n--Russell"
    author: "Russell Miller"
  - subject: "Text index"
    date: 2003-08-13
    body: "About the KMail thread: what is a text index? How does it work?"
    author: "Apollo Creed"
  - subject: "Re: Text index"
    date: 2003-08-13
    body: "It's a full text index which should considerably speed up searching messages which contain some given words."
    author: "Ingo Kl\u00f6cker"
  - subject: "Kcalc vs Google toolbar"
    date: 2003-08-13
    body: "Hi, \n\nI'm just reading about the new google toolbar for IE:\nhttp://www.google.com/intl/de/help/features.html#calculator\n\nHmmm, would be a cool option for Konqi\n\nURL: \"calc:2*sqrt(2)\"\n\n:-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Kcalc vs Google toolbar"
    date: 2003-08-13
    body: "It's already implemented as \"gg:2*sqrt(2)\"."
    author: "Anonymous"
  - subject: "Re: Kcalc vs Google toolbar"
    date: 2003-08-13
    body: "Already there:\n  gg:2*sqrt(2)\nreturns\n  2 * sqrt(2) = 2.828 427 12\nIf you don't like using the 'gg:' prefix, add your own via the Konqueror configuration (Web shortcuts page).\n"
    author: "Grant McDorman"
  - subject: "Re: Kcalc vs Google toolbar"
    date: 2003-08-13
    body: "Cool :-)\n\nAlready a \"tip of the day\"? ;-)"
    author: "Thorsten Schnebeck"
  - subject: "useful knoqi shortcut"
    date: 2003-08-13
    body: "A useful shortcut that should be in konqi should be Netcraft's hosts.\n\nSearch provider name: Netcraft\nSearch URI: http://www.netcraft.com/?host=\\{@}\nURI shortcuts\nnc,netcraft\ncharset: default\n\nI use this to look up a host prior to buying from them. If they are running Windows, I will not submit my Credit Card.\nCheck netcraft ~ a year ago, and you will find that Linux was number 2 server by IP (number 1 by domain) and catching up (should have passed Windows by now).\n\nYet, MS accounts for almost all stolen credit cards.\n\nI have been watching news.com/Linux today/Slashdot for cracked and closed sites. Whenever they are cracked, I check them out. All have been MS where they involve CCs except for a south african site (of which they claim no CC's were stolen and Playboy about 2 years ago, which was Sun).\n\nSeveral years ago, I was going to build a site devoted to calculating the likehood of a theft of CC. The early numbers showed that &gt; 99 % chance that MS site would be cracked.  Now that MS forbides the mentioning of cracked sites (unless required to by law), it would be worthless. I wish I would have done it back then.\n"
    author: "a.c."
  - subject: "Re: useful knoqi shortcut"
    date: 2003-08-14
    body: "Search uri for Netcraft is http://uptime.netcraft.com/up/graph?site=\\\\{@}. In any case I just added it to the Konqueror search providers and it's available in CVS. So nc:www.kde.org will assure you that it's ok to buy from us ;)"
    author: "Zack Rusin"
  - subject: "Re: useful knoqi shortcut"
    date: 2003-08-14
    body: "Isn't this the same as RMB|View Document Information? Or at least that seems to include everything you might want to know about the server...\n\nFWIW this is one of my favourite new features in Konqueror for the reasons stated above... "
    author: "Martin Hignett"
  - subject: "Re: useful knoqi shortcut"
    date: 2003-08-14
    body: "No. On Netcraft you get a graph and statistics of the uptime along the server informations."
    author: "Zack Rusin"
  - subject: "Re: useful knoqi shortcut"
    date: 2003-08-14
    body: "Thank you\n\n"
    author: "a.c."
  - subject: "Re: useful knoqi shortcut"
    date: 2003-08-14
    body: "Boy, these two shortcuts are great - the google-math and netcraft ones.  Thanks for mentioning them!  I've added the netcraft one to my setup."
    author: "TomL"
  - subject: "Re: Kcalc vs Google toolbar"
    date: 2003-08-13
    body: "Interesting. I didn't know that. \nI wonder if there should be something like this should be implemented \nin KDE without requiring web access and without opening a browser\nwindow. Alt+F2, 3*5, Enter => dialog box shows 15 would be cool."
    author: "Jan"
  - subject: "Re: Kcalc vs Google toolbar"
    date: 2003-08-14
    body: "Hmm why don't you use Kcalc??? Add a shortcut like Strg-c type 3*5 Enter and voil\u00e0.\n\nDim"
    author: "Dimitri"
  - subject: "Re: Kcalc vs Google toolbar"
    date: 2003-08-14
    body: "<Alt>-<F2>\nkcalc 3*5 <Enter>\n\nWould be a cool shortcut"
    author: "Markus Gans"
  - subject: "Re: Kcalc vs Google toolbar"
    date: 2003-11-14
    body: "Because Google calculater does more... gg:345g=?oz and gg:21C=?F for example, to calculate weight and temperatures from grams to ounces and degrees Celcius to degrees Fahrenheit.\n"
    author: "Rob Kaper"
  - subject: "KMail INBOX is still mbox?"
    date: 2003-08-13
    body: "Is the KMail INBOX still an mbox file? If so, why is it not a maildir like all the other folders can be?\n"
    author: "dOxxx"
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2003-08-13
    body: "Its up to you: configure mbox or maildir.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2003-08-14
    body: "Does any sane person understand the difference? KMail should take whatever is the best."
    author: "Jeff Johnson"
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2003-08-14
    body: "KMail does take whatever is the best, namely maildir. The normal user doesn't need to understand the difference. What's your point?"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2003-08-14
    body: "Maildir is the best and the default. Mbox is also supported and that is a good thing.\n\nJ.A."
    author: "J.A."
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2003-08-14
    body: "Since KDE 3.0 by default all folders are maildir. But of course mbox folders are not magically transformed into maildir folders. Only new users get maildir for all folders including INBOX.\n\nQuick \"Howto convert the INBOX from mbox to maildir\". Use at your own risk:\n- Move all messages from Inbox to another folder.\n- Set the default folder type to maildir.\n- Exit KMail.\n- rm ~/Mail/inbox ~/Mail/.inbox.*\n- Start KMail. KMail will then recreate Inbox as maildir.\n- Move all messages back to the Inbox.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2003-12-28
    body: "Howto convert the kmail maildir to mbox format?\n\nHow could I convert the kmail maildir's to mbox format?\nI want to use a other email client than kmail. How could I do this?\n\nNeed help.\n\nF.Jaeckel"
    author: "Frederic Jaeckel"
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2004-09-13
    body: "Try running Courier Imap pointed at your maildir then point your new client to localhost for imap and copy across...\n\nhttp://www.courier-mta.org/imap/"
    author: "JT"
  - subject: "Re: KMail INBOX is still mbox?"
    date: 2008-06-29
    body: "I have 1.8GB email and a courier IMAP server. When I tried to copy my mail from kmail across to the IMAP server, kmail used lots of memory, a small amount of the mail was transferred, and eventually (hours later) I got tired of not being able to use the machine. I tried again from a kmail installation on the same machine as the IMAP server, same problem. I got the same behaviour when I tried to use kmail to copy my mail to a local mbox folder. It looked like if the time was linear and the job was going to complete, it would have taken 30 - 50 hours. I'm looking for another way to get my kmail maildir store into my courier IMAP server. Just copying the dir's didn't work. Any suggestions gratefully received :)\n"
    author: "Wayne McL"
  - subject: "KOrganizer default colors"
    date: 2003-08-14
    body: "Thank god they changed the default BG color in KOrganizer. KOrganizer has always been one of the uglier KDE apps. The color change, though, does wonders!"
    author: "Rayiner Hashem"
  - subject: "Do you all experience this"
    date: 2003-08-14
    body: "I have submitted a large number of bugs and wishes http://tinylink.com/?ewUbUepvKK   wanted by many people and recently I hav submitted a bug in the View Profile function of Konqueror http://tinylink.com/?TUfocoAgHY  \n\nCan someone please validate this in 3.2. Thanks!"
    author: "Alex"
  - subject: "KCalc ideas are great"
    date: 2003-08-14
    body: "I think the pi-button and button-group proposals should be implemented in KCalc.  I also think the rest of the features should be implemented in a different, more sophisticated calculator.  I have even been toiling with the idea of doing it myself.\n\nI have an old Casio FX-795P.  It has a built-in function memory, and a databank.  These are the two greatest features ever implemented in a calculator.  They work together like this:\n\nYou have formulas in the data bank\n\tSPHEREA=4*radius^2*pi\n\tSPHEREV=(4/3)*radius^3*pi\n\tMiToKM=miles*1.609344\n\tPYTHERM=SQR(legA^2+legB^2)\n\nYou enter the beginning of the formula name and press \"MEMO\". The databank shows the first formula that matches.  If you don't want that one, press memo again and it shows the second formula...\n\nOnce you find a formula, you press \"IN\" on the function memory and it copies the formula into the function memory.  Then you press \"CALC\" and it prompts you for all the parameters in the formula.  Once you've entered the parameters, it shows you the result.\n\nExample:\n\n\tType PYTH <MEMO>\n\tDatabank displays the Pythagorean Theorem formula\n\tPress <IN>\n\tThe formula is copied to the function memory\n\tPress <CALC>\n\tThe calculator prompts for the value of legA\n\tEnter 3<EXE>\n\tThe calculator prompts for the value of legB\n\tEnter 4<EXE>\n\tThe calculator displays \"PYTHERM= 5\"\n\tPress <EXE>\n\tThe calculator loops around and prompts for the value of legA again\n\nYou can also write up a formula directly, press <IN> and instantly copy it to the function memory.  This is such a handy feature, I'm surprised I've never seen it on any other calculator."
    author: "Burrhus"
  - subject: "Re: KCalc ideas are great"
    date: 2003-08-14
    body: "That sort of thing is best left to calculator emulators, of which there are some around. With the benefit of a GUI surely there are far better ways of selecting a formula.\n\nI personally would like to see different modes for KCalc, e.g. Stats/Trig-Geom/Calculus etc., perhaps with an optional graphing facility.\n\nabdulhaq"
    author: "abdulhaq"
  - subject: "Re: KCalc ideas are great"
    date: 2003-10-08
    body: "Yes!  The memobank function in that calculator was awesome.  I would love to see that same functionality in a modern unix app.  Maybe not <MEMO> <EXE> <IN> keypresses, but a little more gui-fied.  Select the function from a listbox, then have labelboxes for the required values.  Boy that would be great."
    author: "tandy pc6 user"
  - subject: "Solution for Kalculator"
    date: 2003-08-14
    body: "OKAY! Why make two calculators for KDE?!! SImply make an advanced option somewhere in the Configure section, this option could have all the advanced features we want. The current Kcalc is alrady too complicated IMO and many of its features would be best left as an advanced option. Sin, Cos, Tan, these are all advanced features.\n\nPLEASE MAKE AN ADVANCED OPTION IN the CONFIGURE DIALOG OF KCALC AND MAKE THE DEFAULT OPTION  \"SIMPLE\" really simple :)\n\nAnyway, please don't fork it and cause further fragmentation. Who do you think is going to want 2 calculators on their system,  why?! Just have the normal Kcalc beefed up with more features via advanced option."
    author: "Mario"
  - subject: "Re: Solution for Kalculator"
    date: 2003-08-14
    body: "So much debate about an advanced Calculator... Yes a good calculator would be cool, but really useful would be a numerical/symbolic computational system for KDE.\n\nOctave and Yacas could really benifit from a KDE GUI frontend. Like Klaus Niederkrueger says:\n\"Who uses Linux? I think mainly pupils and students, and I think this would be a really useful application for them.\"\nIt would be great for those projects, great for KDE, really useful for a few hundreds or even thousands of students that, at the moment, must either use an comercial system or a non-intuitive one.\n\nDust"
    author: "Dust"
  - subject: "Re: Solution for Kalculator"
    date: 2003-08-14
    body: "http://bubben.homelinux.net/~matti/koctave/"
    author: "Thorsten Schnebeck"
  - subject: "Re: Solution for Kalculator"
    date: 2003-08-14
    body: "I've seen it. Matias has done a good job there, I've tried it and it is insteresting.\nI was talking about something much better though, a serious kde project, up there with others like Quanta and Koffice, possibly with coordination between KDE and Octave/Yacas teams to make something really good. \n\nIMO it this should have a really high priority, and the first Desktop Environment to acomplish it is going please many users.\n\nDust"
    author: "Dust"
  - subject: "Re: Solution for Kalculator"
    date: 2003-08-14
    body: "Problem is not lack of interest but lack of (lets say) continuity...\nIf you're working on your diploma thesis (like me at the moment) you'd really like to have a powerful yet easy to use (based on kde libs) scientific application... But unfortunately your time is limited so you take what is already available (e.g. gnuplot in my case).\n\nBut when you finish your scientific exercises the interest in better applications vanishes.\n\nConclusion: when you're in need of better tools you don't have the time to get into programming (scientific work can be time consuming). When you may have time to start a project, your interest in it is limited."
    author: "Thomas"
  - subject: "Re: Solution for Kalculator"
    date: 2003-08-14
    body: "Yes, but that is my point exactly. \nI wouldn't like such application developed *by* but *for* those who need it.\nAsk yourself, by whom and why is Koffice beeing developed? I don't think the situation is much different. It could be a very strong point in favour of KDE."
    author: "Dust"
  - subject: "Re: Solution for Kalculator"
    date: 2003-08-16
    body: "Full ack!"
    author: "Gerd"
  - subject: "Waldo is on holiday"
    date: 2003-08-14
    body: "\nHe is not in the first posters so I guess he is having some time off. :-)"
    author: "Philippe Fremy"
  - subject: "utterly OT"
    date: 2003-08-15
    body: "... but who cares, it's the dot!\n\nCan anyone explain to me how I could move my Evolution mail folders over to KMail? I've tried using the import tool but nothing seems to work. Using KDE 3.1.3."
    author: "dave hugh-jones"
---
KDE Traffic #61 <a href="http://kt.zork.net/kde/kde20030811_61.html">has been released</a>, with news about KGhostview, KCalc, KRandr inclusion, Qt 3.2 requirement, <a href="http://kmail.kde.org/">KMail</a>, KProcess, <a href="http://korganizer.kde.org/">KOrganizer</a> L&amp;F changes (<a href="http://www.tjansen.de/blog/pics/before.png">before</a>, <a href="http://www.tjansen.de/blog/pics/after2.png">after2</a>, <a href="http://www.tjansen.de/blog/pics/after1.png">after1</a>) and more.  Now let's just wait for everything but the kitchensync.  Anyway, <a href="http://kt.zork.net/kde/kde20030811_61.html">get it here</a>, if you dare.  Muahaha.

<!--break-->
