---
title: "KAddressBook 3.2 Sneak Preview"
date:    2003-09-04
authors:
  - "kstaerk"
slug:    kaddressbook-32-sneak-preview
comments:
  - subject: "people kioslave.."
    date: 2003-09-04
    body: "it'd be neat to have something like http://pim.kde.org/img/screenshots/kaddressbook_cvs/3_2/kaddressbook_10.png\n\nin a konqueror window with a people:/ kioslave.. hell you could even have a sidebar panel with the different catagories. \n\nalso, in the tooltip for each person, show the email and homepage for each person (like in the picture)\n\njust an idea."
    author: "anonymous abuser"
  - subject: "Re: people kioslave.."
    date: 2003-09-04
    body: "===\nwith a people:/ kioslave\n===\n\nI dunno -- kioslaves always make me feel a twinge of guilt, like some tiny creature is trapped in my computer and forced to perform services for me. A people:/kioslave would just be too creepy.\n\nAnd, yes, I feel vaguely bad for slaved ATA devices, too."
    author: "Otter"
  - subject: "Re: people kioslave.."
    date: 2003-09-05
    body: "Not to mention all those commands you've executed...\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Great! But..."
    date: 2003-09-04
    body: "I'm very happy with all those improvements (I'm running KDE CVS), but what I'm still missing is _proper_ printout for ringbinders/planners/calendars. There are different ones in various (paper-)sizes, so it should be supported to set the page size and margins. The printwizard should also offer to print a number of sheets per A4-page (I know it's possible to select this in the printer settings but it resizes the pages in a wrong way). Then it should also draw the page borders, so you can cut them out and put them into your ringbinder.\n\nI hope this will make it into 3.2 :-)\n\nTK"
    author: "TK"
  - subject: "Re: Great! But..."
    date: 2003-09-05
    body: "Great idea. Did you propose this at http://bugs.kde.org/ ?\n\nRegards,\n\nDirk"
    author: "Dirk"
  - subject: "Re: Great! But..."
    date: 2003-09-06
    body: "A part of this is already in CVS, but it hasn't got the options I mentioned above. "
    author: "TK"
  - subject: "Re: Great! But..."
    date: 2003-09-05
    body: "I was going to add an envelope printer to the current addressbook, but I am wondering if it can be gotten in this round.\nMy father is missing the ability to easily print off a bunch of envelopes with addresses and OpenOffice is kind of a pain to use for that."
    author: "a.c."
  - subject: "Re: Great! But..."
    date: 2003-09-06
    body: "KWord can access the address book data in newest version, so maybe you can create a KWord envelope template with mailmerge variables inside\n\nCiao,\nTobias"
    author: "Tobias Koenig"
  - subject: "Spreadsheet-like input?"
    date: 2003-09-04
    body: "It would be nice if one could edit directly in a view similar to the table view, spreadsheet style. Since it is already possible to create custom table views, one could then edit comfortably in a table of contacts showing only relevant fields. This would be useful both for entering several contacts at once and for adding to existing contacts.\n\n(Disclaimer: I just looked at the screenshots; this may already be implemented somewhere or already suggested on the wish list. And if not I know I should add it to the wish list, but I'm too lazy, OK? Not that writing this disclaimer was any quicker than checking the bug tracking system would have been, but now it's done anyway...)"
    author: "Martin"
  - subject: "Software patents"
    date: 2003-09-04
    body: "There were ideas to create a EU-Parliament address book in order to help the anti-swpat lobby effort and and other groups. Is KAdressbook suited for this purpose?"
    author: "Wurzelgeist"
  - subject: "Re: Software patents"
    date: 2003-09-04
    body: "Yes, it should work equally well in the Europe and the US."
    author: "ac"
  - subject: "Misses most of the point..."
    date: 2003-09-04
    body: "I really wish I could get the KAddressBook guys a few friends they need to try larger addressbooks.  I was crushed to learn that still its performance is lacking for large addressbooks even on local resources.\n\nWhile these features are cute, they are going to be a crushing disapointment for any company of more than 50 users who try to use it. Dont even try to share the resource via nfs, and forget ldap if you wish to see an addressbook yet this year.  \n\nLets hope incremental loading and resource merging come sooner than later.  A pretty UI is quite useless if I cannot manage my 5,000 contact addressbook, worse, try using kmail or any koffice (for grins enable an ldap resource and see 2-5 minute kword startup times) applications at that point :P\n\nOh well we have a few months until release, maby well get lucky.  Then again it took ACT a few years to get down large addressbooks so im not holding my breath."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Misses most of the point..."
    date: 2003-09-05
    body: "Hi Ian,\n\nYes definitely an important issue, and one that was discussed at n7y. Do you know for LDAP if it's possible to get the number of addresses that match a search, and get say the i'th address from the list of addresses that match a search sorting by some arbitrary field?\n\nDon."
    author: "Don"
  - subject: "Re: Misses most of the point..."
    date: 2003-09-05
    body: "Yes, but the key problem is also present in my NIS addressbook plugin.  On large networks its hard to deal with all the addresses.  Now this is my fault because i load them all at once, but that is because its unclear if or how incremental loading works or will work.\n\nDont get me wrong, KAddressbook is lightyears better than it was, but now, it seems there is one fault that really makes it hard to use.\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Misses most of the point..."
    date: 2003-09-05
    body: "The koffice startup time issue has been fixed - the addressbook is now only used in the \"document info dialog\". Update :)"
    author: "David Faure"
  - subject: "Re: Misses most of the point..."
    date: 2003-09-05
    body: "Cool :)\n\nI just got my fast desktop cvs merged with my n7y code, so ideally I should see this in a few hours."
    author: "Ian Reinhart Geiser"
  - subject: "mail-merge"
    date: 2003-09-04
    body: "will there be an easy way to use the addresses for mailmerge (in kword or openoffice)? "
    author: "AC"
  - subject: "Re: mail-merge"
    date: 2003-09-04
    body: "Hm, one of the pictures showed the ldap lookup function, which already exists.\nI hoped that KAdressbook would allow storing/editing addresses directly in a local ldap directory, which would mean to have synchronised addresses in KDE and OpenOffice."
    author: "Carlo"
  - subject: "I demand to know..."
    date: 2003-09-04
    body: "...the author's political views on the us army and what's in the about box.\nCould someone post the screenshot of that?"
    author: "frizzo"
  - subject: "Re: I demand to know..."
    date: 2003-09-04
    body: "I thought that guy was the one who wrote Kaboodle?  Anyway, what I heard he wrote was something about the US army providing the \"freedom\" that makes free software possible-- pretty typical right-wing blather if you ask me."
    author: "stunji"
  - subject: "Re: I demand to know..."
    date: 2003-09-04
    body: "And that's a pretty typical liberalist (i.e. ignorant of how the real world works) response. Can I point you towards Ann Colters new book? <evil grin>"
    author: "Anonymous"
  - subject: "Re: I demand to know..."
    date: 2003-09-04
    body: "The real world doesn't work obviously."
    author: "CE"
  - subject: "Re: I demand to know..."
    date: 2003-10-27
    body: "The real world works just fine.  It's just us looking at it differently."
    author: "Victor de Antoni"
  - subject: "Re: I demand to know..."
    date: 2003-09-04
    body: ">>>Anyway, what I heard he wrote was something about the US army providing the \"freedom\" that makes free software possible-- pretty typical right-wing blather if you ask me.<<<\n\nThat pretty typical ignoramus reply.  Use the tabs in Konq to open more news sites than one."
    author: "frizzo"
  - subject: "Re: I demand to know..."
    date: 2003-09-04
    body: "Hey, this subject has already been discussed and its useless to continue."
    author: "Ned Flanders"
  - subject: "Re: I demand to know..."
    date: 2003-09-04
    body: "Okily, dokily there.\nI'll check in some sarcasm into CVS next week, maybe you'll be able to use it."
    author: "frizzo"
  - subject: "Dnt let us wait :-)"
    date: 2003-09-05
    body: "I just saw the sreen shots. That looks good. I can't wait to get it setup. Don't let us wait to long :-O\n\nBest regards\n\nNils Valentin\nTokyo/Japan"
    author: "Nils Valentin"
  - subject: "Europe / American ZIP/Postalcode"
    date: 2003-09-05
    body: "As in Europe first the Postalcode is printet before the city name, it&#347; not easely for me to fetch the address from contact card to another program. There sould be a  function to define the format of ZIP-city or city-ZIP."
    author: "Sven Fischer"
  - subject: "Re: Europe / American ZIP/Postalcode"
    date: 2003-09-05
    body: "(Not in the UK it isn't, which is in principle at least in europe ;-)  )\n\nBut yes, an option would be good.\n\nEd"
    author: "Ed Moyse"
  - subject: "Re: Europe / American ZIP/Postalcode"
    date: 2005-06-15
    body: "please cold u gave me some zip/postalcode for any country u know?"
    author: "hossam"
  - subject: "Re: Europe / American ZIP/Postalcode"
    date: 2008-09-11
    body: "please kindly give me the zip/postalcode of the Europe/American\nthanks for helping me. infact this site is the \nbest among all. once again i say thanks. \n\nfrom richlove. "
    author: "richlove"
  - subject: "american state zip/postalcode"
    date: 2004-02-26
    body: " hello       looooooooooooooooooooooooooooooooooooo"
    author: "taiwo"
  - subject: "Re: Europe / American ZIP/Postalcode"
    date: 2007-11-21
    body: "i would like to know more postal codes of  th UNITESD STATRES OF AMERICA"
    author: "hi"
  - subject: "good stuff."
    date: 2003-09-05
    body: "kaddressbook is quite basic at the moment, all improvements are welcome.\n\nWhat I'd still particularly like though is to link kaddressbook into kmail's filtering rules.\n<a href=\"http://bugs.kde.org/show_bug.cgi?id=26021\">bug 26021.</a>\n\nkeep up the good work whatever though."
    author: "caoilte"
  - subject: "how about list management?"
    date: 2003-09-05
    body: "hi,\nhow is the list managment handelt in KAddressBook 3.2?\nthe most part missing in kmail is the non-intuitive list managment:\n\n1. clicking at \"new mail\" and then at the button with the three point \"....\" to get the addressbook: no lists in there\n\n2. no visual mark for a list, neither icon nor hightligtning\n\ni am using kmail for my daily work and it would be great if this could change.\n-gunnar"
    author: "gunnar"
  - subject: "Re: how about list management?"
    date: 2003-09-05
    body: "On cvs HEAD, this brings up the address selection dialog. The available addresses are from Kaddressbook or recent addresses from kmail. You can make up distribution lists that can be saved. The addresses are categorized as they are in kaddressbook.\n\nWorks well.\n\nDerek"
    author: "Derek Kite"
  - subject: "palm sync?"
    date: 2003-09-05
    body: "So, like will this sync with my palm pilot?\n\nI understand kpilot will be part of the KDE-PIM, but what I really want is a unified address book so i dont have to copy stuff back and forth between the palm addressbook and the kaddressbook.\n\n-john\n\n(if kpilot already has this functionaity i wouldn't know since I've never been able to get it to sync. :( jpilot works tho)"
    author: "john"
  - subject: "Re: palm sync?"
    date: 2003-09-05
    body: "Works here (in cvs HEAD). Shuffles things both ways.\n\nDerek"
    author: "Derek Kite"
  - subject: "And whatabout irmc/syncml"
    date: 2003-09-05
    body: "Currently I use evolution and http://multisync.sf.net to keep my S55 upto date, which sucks because my entire other enviroiment is kde :/\n"
    author: "nchip"
  - subject: "Does it have LDAP write support?"
    date: 2003-09-05
    body: "It's sad that evolution (in my experience the slowest IMAP client out of 3 the most functional linux GUI mail clients) is the only one with LDAP write support. Does KAddressBook have it yet, and if not, is it in the plan?\n\nA complete LDAP browser (kio_ldap isn't there yet), competing with AD browsing in Windows, would also be nice ... and could be the way to implement write support (including uploading photos, digital certs, gpg keys etc) in KAddressbook"
    author: "ranger"
  - subject: "Re: Does it have LDAP write support?"
    date: 2003-09-06
    body: "Hi,\n\nyes, you can write to a local ldap directory. The problem is the limited number of supported attributes, since everybody has it's own ldap schema...\n\nCiao,\nTobias"
    author: "Tobias Koenig"
  - subject: "Re: Does it have LDAP write support?"
    date: 2003-09-06
    body: "Why not parsing all existing schemes and let the user write plugins via scripting langauges/Kommandereditor?"
    author: "Carlo"
  - subject: "Small business userability"
    date: 2003-09-05
    body: "First, the screenshots show off some pretty great advances - thanks for all the hard work.  Look forward to checking out the beta.\n\nNot sure what all is included in this release but our biggest needs for using KAddressbook on a daily basis are:\n1) Reliable, flawless synching with Palms (or could be Zaurus or whatever)  - I read somewhere that KitchenSynch was to be added - for us, it is whatever works well.  Lots of QA needed here.  In the past we have lost data with Kpilot and that has caused us many problems and we've stayed away from it and rely on good old-fashioned spreadsheets and jpilot.  Our Palms are vital for conducting business and it hurts not being in synch with our email.\n\n2) Good integration with KMail for email lists - while OpenOffice mail merge is also desirable, we now do all our mass mailings by email - thus list management is very important.\n\n3) An easy way to import data - can simply be comma, delimited CSV - ie we currently have lots of data in spreadsheets\n\n4) Easy way to dupe cards (I think that's already in there)\n\n5) Maybe a way to integrate with a scan device such as card scan or whatever, this typing in of cards has got to go and the world isn't quite ready to \"beam\" cards to each other.  \n\n6) A good way to share and synch files between users (ie my secretary types in the cards but then I need to use it - might require a private field or whatever.\n\nHope this feedback helps make an even greater program and would not be surprised if you are already ahead of this.\nJohn"
    author: "sage"
  - subject: "Re: Small business userability"
    date: 2003-09-05
    body: "Have you filed wishlist-items on bugs.kde.org?"
    author: "Marc Tespe"
  - subject: "Re: Small business userability"
    date: 2003-09-06
    body: "csv import is in CVS."
    author: "m."
  - subject: "Accel"
    date: 2003-09-05
    body: "I hope it has Alt+S or somesuch to focus the search field..."
    author: "Tar"
  - subject: "Re: Accel"
    date: 2003-09-06
    body: "I just want to second this -- the incremental search itself is very efficient, and I find that most of the time it takes me to do a search is spent clicking on the text entry field.  A shortcut for this would be a significant enhancement for me."
    author: "Andy Neitzke"
  - subject: "WoW, I'm impressed"
    date: 2003-09-06
    body: "it's looking very good, and i defintely like the search functiona dn being able to put pictures adn sounds ;)\n\nGreat job Cornelius and Co!"
    author: "Alex"
  - subject: "great"
    date: 2003-09-06
    body: "\n\nWell, well, well.\n\nIn Spain we love KDE."
    author: "Manuel Rom\u00e1n"
  - subject: "Intergration with KMail?"
    date: 2003-09-08
    body: "Is there going to be better intergration between K-Mail and KAddressbook? Though I hate MS; Outlook Express automatically added new contacts to the addressbook when i e-mailed or replyed to them.  Also in a new mail <TO> <CC> and <BCC> were clickable and listed my contacts for selection, and in the address book, i could easily select contacts and then add them to each of those lists for a new mail. \n\nIf these features exist, then they are poorly presented cuz i can't find them in v.3.1"
    author: "Steven"
  - subject: "Nice, but..."
    date: 2003-09-12
    body: "...I am missing essential features. I *do* admit, that all I saw from CVS version are these screenshots, but I feel, that this is all UI functionality but not featuring extensive database stuff. With that I mean: fields!\n\nHere are my desires for an address-book:\n\n- Integration with GPG/PGP (PGP-ID, etc.)\n- It should be possible to get all personal emails by this user into a listview\n- unlimited email addresses per person\n- multiple IM and pager addresses \n- same for homepage\n- print business card plugin with different card layouts\n- direct link to IM programs\n- unlimited phone numbers which are to be labelled by the user (i.e. I have two mobile numbers, a stationary home number and a stationary fax number)\n- bank-account details\n- books/CDs/Videos lent (with catalog number, title and date)\n- \"contacted first\"\n- \"letter-opening\"\n\nplus there is so many more, one might like for a powerful address-manager. Check out the commercial programs. With KDE now coming to cooperate desktops this might be well important!\n\n"
    author: ".jon"
  - subject: "KAddressbook custom fields? import to category?"
    date: 2003-12-19
    body: "I wonder if it would be possible to add to KAddressbook some custom fields? In particular, I like ot keep soem other dates beside just Birthday and Anniversary.\n\nAlso, would it be possible to import to a specified category? perhaps thus having the record directed to a sub .vcf addressbook if such existed for that category?"
    author: "R S Prigan"
---
As <a href="http://pim.kde.org/news/news-2003.php#25">announced</a> on the <a href="http://pim.kde.org/">KDE PIM site</a>, we are featuring a preview of the upcoming <a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a> 3.2 in the form of <a href="http://pim.kde.org/development/kaddressbook_cvs_32.php">some annotated screenshots</a> taken from the CVS version.  KAddressBook 3.2 will be included in the KDE 3.2 release.
<!--break-->
