---
title: "KDE Traffic #60 is Out"
date:    2003-08-07
authors:
  - "rmiller"
slug:    kde-traffic-60-out
comments:
  - subject: "Impressed..."
    date: 2003-08-06
    body: "of the quality of this KDE traffic. Thanks.\n"
    author: "Ricardo Galli"
  - subject: "Re: Impressed..."
    date: 2003-08-07
    body: "As was i - very well written/composed indeed\n\nOne thing about it that supprised me though: \"This thread doesn't fit with my usual standards of what is interesting, not to mention it's quite out of context...\" \n\nThe discussion that followed about restucturing & the future of kmail i found highly interesting and well summarised - possibly the best bit imho, certainly not OT\n\nanyway, thanks Russell for a great issue of KDE Traffic (:"
    author: "James"
  - subject: "Re: Impressed..."
    date: 2003-08-07
    body: "I agree; great job. Keep it up, Russel.\n\nI only wish all projects I hold dear had as good news reporting. The Dot-Digest-KT combination is truely exemplary. Thanks for all the hard work, guys. I appreciate it.\n"
    author: "Sho"
  - subject: "Re: Impressed..."
    date: 2003-08-07
    body: "Agreed. The best KDE traffic so far."
    author: "nusuth"
  - subject: "Indeed"
    date: 2003-08-06
    body: "This si full of great stuff, as good as the Digest."
    author: "Mike"
  - subject: "No more shred"
    date: 2003-08-07
    body: "I agree for the most part with: http://kt.zork.net/kde/kde20030726_60.html#4 except the removal of shred. Why remove shred, I like dthat feature."
    author: "Alex"
  - subject: "Re: No more shred"
    date: 2003-08-07
    body: "Because \"shred\" gives users a rather false sense of security."
    author: "fault"
  - subject: "Re: No more shred"
    date: 2003-08-08
    body: "But some security is better than none."
    author: "Tormak"
  - subject: "glad you sat it out"
    date: 2003-08-07
    body: "Happy you're back and strong, Russell!"
    author: "ac"
  - subject: "Re: glad you sat it out"
    date: 2003-08-07
    body: "Back, for sure.  Strong, we'll see."
    author: "Russell Miller"
  - subject: "The Shooting of Python"
    date: 2003-08-07
    body: "\"Of course, then someone just HAD to suggest using python instead, which was (ed: thankfully) shot down.\"\n\nAnd just what's wrong with using Python, hmm? ;)\n\nInstallation issues aside, I think PyQt/PyKDE is an excellent way to get into GUI programming."
    author: "dOxxx"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-07
    body: "I'm something of a language bigot.  I perfer C++ for raw coding, and perl for scripting.  Python has never appealed to me."
    author: "Russell Miller"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-07
    body: "I agree, and I think that KDE should really help promote Python/PyQt/PyKDE, and maybe even officially work with Riverbank who maintain the bindings. They're a potentially explosive feature - the first competitor to Visual Basic on Windows, only ten times better due to the superiority of both Python and Qt/KDE.\n\nNow I've used them for a while, I can write a reasonable KDE application in one day's coding, and then if I wanted to use it as a prototype for a C++ implementation.\n\nPromoting Python/PyQt/PyKDE would not only give KDE a really nifty feature to attract new programmers to the platform, but it could also herald a surge in contributed applications / frontends. Hey, if they seperate the UI from the core of KMail, I could write a new really simple frontend in a day... wouldn't that be fun? ;-)"
    author: "Tom"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-07
    body: ">I think that KDE should really help promote Python/PyQt/PyKDE\n\nPlease no - KDE should remain as language neutral as possible, whilst continuing to provide  technologies that make it easy to create bindings such as PyQt, PerlQt etc.\n\nSome folks love Python, some hate it - Ruby, Tcl, Perl (your favourite here) all have strong user bases, and KDE should embrace them all."
    author: "Rich"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-07
    body: "I didn't mean that KDE should support Python as its first language and neglect others; C++ will probably remain KDE's first language for a long time, with other languages playing catch up game (support and work here could improve).\n\nKDE already promotes and encourages experienced programmers to code KDE apps in C++... what is wrong with KDE also encouraging programmers (newbie and guru alike) to try out PyKDE? It's a really useful technology, and when used with an IDE like eric3 (and hopefully soon kdevelop3), it's a real VB killer.\n\nIf Ruby, Tcl and Perl have any killer features to give to KDE, they should be promoted too.\n\nAs it is, the impressive PyKDE technology is barely recognised by KDE."
    author: "Tom"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-07
    body: "IMHO this should not happen until we have a system that allows scripting languages to access C++ APIs without wrappers, or creates the wrappers automatically without any human intervention.\n\nCommitting to scripting languages comes with responsibilities. That means that when you update a C++ API, all scripting APIs should be updated. Unless this is completely automized that would be a lot of effort for the developer of the C++ API feature (the dev would need to know all scripting languages and update their bindings, or you need active binding maintainers who sync everything as soon as possible manually).\n\n"
    author: "Tim Jansen"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "Boost.Python might help here. There is a tool in Boost.Python that automatically generates Python glue code for C++ APIs. I don't know how it would interact with Qt signals/slots, but it might be a very good way to bridge C++ and Python. I'm using it right now on a rather simple API, and it would definately need some work to scale to something like Qt/KDE. But it would be doable..."
    author: "Rayiner Hashem"
  - subject: "Go SMOKE! :)"
    date: 2003-08-08
    body: "\"That means that when you update a C++ API, all scripting APIs should be updated. Unless this is completely automized that would be a lot of effort for the developer of the C++ API feature (the dev would need to know all scripting languages and update their bindings\"\n\nYes, you're dead right! I've recently checked in some ruby bindings into kdebindings, called 'QtRuby', which use the same runtime library as the PerlQt bindings - libsmokeqt.so. I didn't need to parse headers or change SMOKE in any way, just write a relatively small 'smoke adaptor' for the new language. The adaptor is dynamic and so it doesn't need to be updated for each tiny release, so we don't need those 'active bindings maintainers' so much - I prefer to be an 'inactive maintainer and to have fun writing apps (especially ruby ones!) using the bindings instead..\n\nAnother interesting property of sharing a runtime is that it might make it possible for a KPart to be written in Perl, and then subclassed in Ruby - they share a common marshalling scheme and C++ stack. That is relatively easy to do add with SMOKE 'bridge extensions'.\n\nI've just started working on a Java SMOKE adaptor, that is possible too.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Go SMOKE! :)"
    date: 2003-08-08
    body: "I think it is important to raise the profile of non-C++ bindings for KDE/Qt both amongst developers and amongst distributors.  Do you have any ideas on how to this?"
    author: "ac"
  - subject: "Re: Go SMOKE! :)"
    date: 2003-08-09
    body: "Well, I've only just checked the QtRuby project into the CVS so there isn't any need to tell any distributors about it quite yet :). But I think it works well enough that people can try it out, and perhaps I should do a news item here on KDE.news about how it works with the Qt SMOKE library. A few hours after we got the install working for the CVS version, Alex Kellett wrote a pretty functional text editor - see kdebindings/qtruby/rubylib/examples/textedit/textedit.rb. You need ruby 1.8 to run it. Don't under estimate ruby - I think its suitable for serious applications, not just 'scripting'.\n\nSo to raise the profile of this approach to adding a language independent dyamic interface to a Qt/KDE's static (or 'semi-dynamic' wrt moc additions) api, via SMOKE 'adaptors' and 'bridges' ... I suppose we need to explain what an earth we're doing sometime.\n\nAnother interesting idea for a future version of the SMOKE library is to add a unique name to each method/class etc in the api as a URI. See this:\n\n http://deego.gnufans.org/intrspctr.pl?NodeUris\n\nThat would allow networking infrastructure such as documentation or wiki resources to be tied to the name. Please see the most interesting 'kdebindings' list for some recent discussions - is that all available on the web somewhere?\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "You don't really need to expose everything to manage scriptability. You fundementally want to expose the menu functionality and widgets containing information for read write. DCOP works very well for this and is language neutral. Since this is basically kactions and there is also the DCOP IDL there is a lot already done for developers.\n\nThe scripting language development issue is deceptive because it leaves a scripter having to do many of the same things you have to do as an application developer, just in a different language. Marc Britton and I had this conversation about the design of Kommander. With Kommander we treat widget creation different and we don't focus on the language but on the process. Our objective is to bring greater power to less skilled users and to avoid the complexities inevitable to certain language driven operations. Scripting is optional, but when it's required we don't ever want to force the user to use a given language.\n\nThis model is working really good and we are extending the DCOP functionality of Quanta as is needed. All that is needed to script is to give a user access to commands in an application. All the religious language wars can be avoided and more over the true benefit of scripting gained by the model we use with Quanta and Kommander... Use the language you like as needed (if needed). I wish this thinking were dominant. Developers... try Kommander! ;-)"
    author: "Eric Laffoon"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "Ok, that probably came over as slightly extreme.\n\nNaturally KDE should promote technologies that work with KDE and Qt as far is reasonably possible with the resources available.\n\nI'd be less happy if KDE was supporting a single language binding in preference to improving potential support for all languages. Tim Jansen sums it up far more eloquently than I did:\n\n\"IMHO this should not happen until we have a system that allows scripting languages to access C++ APIs without wrappers, or creates the wrappers automatically without any human intervention.\"\n \nOf course, it's ultimately the developers say, and rightly so. If KDE users can influence development with wish lists, that's great, but I'm not complaining (not too much anyway!)."
    author: "Rich"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-09
    body: "You're describing what the language independent bindings library 'SMOKE' does. It has worked for PerlQt and QtRuby so far. We already have a system up and running, just like you describe above, \n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-09
    body: "True, and it's bl**dy marvelous. Language neutrality (as far as reasonably possible) is a great bonus.\n\nThanks very much for the work you and other developers have done on this."
    author: "Rich"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-07
    body: ">> Now I've used them for a while, I can write a reasonable KDE application in one day's coding, and then if I wanted to use it as a prototype for a C++ implementation.\n\nThat's the exact same reasons I've been using it for the last few weeks.\n\nAnd I must say I'm quite impressed..\nA few weeks ago I'd never touched any Python code (I didn't like it either) and a few days later I wrote a pretty complete KDE test program, it's that easy :)\n(using a KHTML view, for example, is also just a few lines of code - you can create your own tiny browser in < 100 loc with Python =)\n"
    author: "blub"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "I think 90% of the current application base could have been coded, with the same result, using a scripting language instead of C++. IMHO you only need C++ if you want to do fast graphics (SVG, Canvases etc). \n\nDevelopment speed is also a lot higher because of:\n*More reasonable error messages \n*No segfaults (at least not in your scripting part) \n*No make environment to setup \n*No waits for compiling"
    author: "Vincent"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "Scripting languages and their runtime-error checks also have problems, for example with rarely used code. If you have a simple bug (e.g. calling a method of an object, and you mistyped the method name, or an invalid cast) in your error handler, there is a good chance that your C++ compiler will find it or at least warn you. But if you have that kind of bug in a scripting language, you won't notice the bug until you somehow manage to execute the error handler. \nThis is also one of the reasons why re-factoring with scripting languages is almost impossible. Unless you have ways to retest and execute *every* code path in the refactored app, it is very likely that you have introduced errors in the code.\n\nThat's why most larger apps written in dynamic languages are buggy and feel fragile.\n"
    author: "AC"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "On the flip side, this means if you have an error, you would get a \npretty accurate error message, and not at best a backtrace, and mostly likely not even that --- \nmost people are using binary packages, which make it impossible to extract backtrace information....\nWhich means if a serious bug slips in, it'd be much easier to fix. \n"
    author: "Sad Eagle"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "This is an implementation problem. You could ship binaries with full debug information (and they would still be faster than Python :), or use pseudo-code like .NET, or even ship C++ programs as source and run them in an interpreter.\nI just wanted to say that runtime-typing of Python, Perl and friends does not automatically lead to shorter development times compared to C++, Java or C#. In fact I doubt that it shortens the 'time-to-market' for stable applications. But this probably also depends on your development style. If you are a testcase-fanatic and use measurement tools to ensure that your tests execute every single line of code, they can probably help. But if you work like most programmers and ship when the code works for you...\n"
    author: "AC"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "You forgot those:\n\n- Dog slow apps\n- A lot of memory usage.\n- False sense of being free of errors because \"the engine does this for you\"\n\nI like scripting for prototyping an app. But using scripting for building entire applications is insane IMO."
    author: "fredi"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "These points are implementation problems, not problems of dynamic languages. "
    author: "AC"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: ">Dog slow apps\n\nThat's questionable IMHO - I've used PyQt and PerlQt, and neither of them are \"dog slow\" - most java GUI apps I've used feel slower both at startup and runtime. On a reasonably modern machine (say a Duron 900 upwards) it's not an issue for the majority of applications, unless they're absolutely speed critical.\n\n>A lot of memory usage\n\nTrue, but again that has to be put into context - with low-end machines having 256-512M memory, this is becoming less of an issue.\n\n>False sense of being free of errors\n\nOnly if you don't know what you're doing - modular code, mandatory test suites, assertions, consistent coding style... are just as necessary in scripting languages as in compiled languages. I doubt whether any experienced coder would have a false sense of being error free, any more than developers would assume that a program is error free if it compiled ok.\n\n>But using scripting for building entire applications is insane IMO\n\nI honestly believe that within 10 years most application development will be script based. Machines will easily cope with the extra resources demanded by scripting languages, and areas that compiled language developers have issues with, such as type safety and code coverage, will be covered. Python and the ongoing Perl6 development looks interesting, and Ruby, Tcl etc are undoubtedly working on these issues - I'm sure others here with more experience of them can comment further.\n\nThat's not to deny that the disadvantages of scripting languages listed here are true, but they're a little overstated, and overlook the considerable advantages as well. \n\nUltimately though, if the app does what it's supposed to and does it well, I couldn't care less what language it's written in. I'm sure for most end users that's true as well."
    author: "Rich"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "I'm all for scripting languages and use several of them. I think they're great and great for prototypes and small mini apps. However try doing Quanta's parser with a scripting language. Even doing it in C++ created problems without over a month of optimizations. Quanta is also an example of a far more sane approach to using scripting too. It allows and even exploits extention of the base application with XML configuration files and user programmable actions, which are in effect the same as menu and toolbar items in KDE. Our approach is different from the traditional one which is \"developer builds it and if your lucky there is some scriptability, often in a specified language\". Our model is \"developers build the core appliation in C++ setting up access for scripting in any language as well as scripted or scrptless Kommander dialogs.\" \n\nThe advantage to our design is clear, though it seems to be hard to get it through to people. It's a core application but it's more of a pallete to draw your own custom appliation on. We look to involve more script level developers to extend the base program. However the base program could never realize adequate functionality as a scripted application. Also it is just wrong to drive people to a script language not of their choice. Perl and Python are a good example of different ways of doing things.\n\nThe one thing you will always have with any script language is the interpretation phase that actually compiles the script. I guess you can cache it, but that makes dynamism hard. Operations you consider not to be time critical on today's hardware become so when there are large numbers of them because of application level complextity. Due to certain laws of physics these will be issues for the forseeable future. Not only that but you will eventually realize a level of complexity similar to to what you'd have with C++ once you are to an application level. You may as will learn the right tool of the job. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-09
    body: "> The one thing you will always have with any script language is the interpretation phase \n> that actually compiles the script.\n\nWell you can pre-compile to opcodes in some languages, though I'm not sure how reliable the results are. You can also dynamically compile when necessary - I use that to reduce start up times for scripted Qt apps. But thinking ahead, if parrot happens, we'll have a language neutral(ish) open source VM for running compiled scripts - nice :)\n\n> Due to certain laws of physics these will be issues for the forseeable future.\n> Not only that but you will eventually realize a level of complexity similar to to what you'd \n> have with C++ once you are to an application level. You may as will learn the right tool of \n> the job. ;-)\n\nThat's too general a comment - there's no \"law of physics\" stating that large applications can only be written in compiled languages.\n\nTraditionally it's more due to the fact that machines didn't have adequate resources for running larger scripted applications - now, thanks largely to XP, Java etc, the spec of the most basic modern machine is more than adequate. Equally, there's still a lot of snobbery in code development - C++/Java (examples only, don't flame!) will always be held as superior because that's what the developers know and they're not willing to investigate alternatives - scripting languages are often considered too easy for hardcore developers, and must therefore be inferior - I'm not suggesting that's the case here BTW. \n\nAs I stated previously, I can already see scripting languages evolving to reduce the percieved gap between themselves and traditional compiled languages for larger scale application development. Even if this involves adding complexity to the core language, it does not automatically mean that developers will then be faced with C++ levels of complexity.\n\n>You may as will learn the right tool of the job. ;-)\n\nI 100% don't agree with what you imply here, but I like Quanta :)"
    author: "Rich"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-08
    body: "As a disclaimer, I like scripting, I'm not against it at all. I wanted just to point out that lately scripting languages are a lot overlooked (while you think the opposite is true :)).\n\nAbout the speed, I dont know python. But in general a compiled program can be a lot faster that every interpreted one.\n\nAbout the memory usage, C/C++ and any language without garbage collection lets you choose how to use memory. This sometimes is good, sometimes bad. But with scripting you have no choice of doing this. I've seen a lot of java programmers tweak their code in ugly ways for workarounding the VM's memory management.\n\nAnd, if you were like me (till a month ago ~) using a pentium2 233MHz 64Mb ram I bet you will for sure agree on my concerns about speed and memory ;) Upgranding CPU's or RAM in an old laptop is not that cheap.\n\nA lot of people were (and are) saying that C is evil and noone will use it. And it is insecure or similar complaints. Well, I see C quite widespread and used. And about security, if the programmer does errors in C, think about what will do in an environement that promises him security.\nAbout the type safety, why having the typechecking slow things down in runtime when you can have it done during compilation?\nAnother advantage of scripting languages is that are portable in various HW/OS'es. Well, with open source progs you have that with compiled languages too.\nScripting languages are really good at prototyping an application, so you can know in advance what design problems you'll have and you can correct the issues quickly. End this is not a little point.\n\nAll this IMO of course.\n"
    author: "fredi"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-09
    body: "- Dog slow apps\n  use psyco\n - A lot of memory usage.\n for any non trivial application I have certainly not found that to be true and in many C/C++ apps I find that they use more memory since it seems they spent more time trying to optimize the little things rather then fixing the data structures.\n - False sense of being free of errors because \"the engine does this for you\"\n The engine does do a lot of things for you but it does not make your program free of errors however it is easier to debug these apps. Overall I find that you work on making your code simpler and clearer and as a result your apps are far less buggy and are easier to fix when a bug is found.\n\n I like scripting for prototyping an app. But using scripting for building entire applications is insane IMO.\n\nThere are a fair number of large apps written in python and they work very well. I use zope every day and while it does have some issues I have not had it crash in about a year and a half under any usage situation.\n\nThe biggest problem I see is that people write python code like they would write C/C++ etc and as a result their apps are slower, take more memory to run are more buggy etc. Apps that are written in a more pythonic manner don't suffer from that problem to anywhere near that degree. One small example is adding strings together to make a large string. In python if you do that a lot you will kill performance since strings are immutable. I helped one person with a program and changed their string addtions into list appends and had it do a join at the end to create the string and it ran 1000x faster then their older version. \n\nSpeed is a design issue not a language issue especially with stuff like psyco."
    author: "kosh"
  - subject: "Re: The Shooting of Python"
    date: 2003-08-09
    body: "The only speed related problem I found with PyQt was creating a lot (> 1000) of overloaded qlistviewitems. Using psyco did not solve this problem, but this problem can be solved through a better design (virtual listviews that load the visible items on demand)."
    author: "Vincent"
  - subject: "KDE Traffic and KDE CVS-digest"
    date: 2003-08-07
    body: "These are IMHO very important parts of KDE. They may not directly contribute code, graphics, translations nor docs, but I think their influence on a large crowd can't be denied. Not that many follow and really read the mailing lists to see what happens in the KDE world, but thousands read KT and CVS-digest. These projects help bring many users closer to the development process and makes them somehow feel like they were a part of the process. I also think that they make new interested developers find something to work on.\n\n"
    author: "Chakie"
  - subject: "KDE Development Book "
    date: 2003-08-07
    body: "The idea is great and its form would kick it ahead of the competition. Hoping that it will have a great kick-off soon."
    author: "Anonymous"
  - subject: "KDE Development Book"
    date: 2003-08-07
    body: "I am *really* looking forward to buying a copy of the KDE development book.  Good luck!  (But take your time, don't get rattled by the inevitable flames about how long it is taking to finish and publish!)"
    author: "Anon."
  - subject: "Me Too :)"
    date: 2003-08-07
    body: "Your not the only one. I would really like to buy this book as well (too add to my allready full book self :) ). I really hope that it does become a book covering even some of the more obscure aspects of QT/KDE programming."
    author: "Mark"
  - subject: "Re: KDE Development Book"
    date: 2003-08-08
    body: "Looking forward to buy this book, as it is really not so simple starting to code in C++ AND Qt for KDE Gui programing.\n\nThat's why i wish to stress my opinion to REALLY start with some starters in this KDE development book.\nThat's the best way IMHO to have C++ beginners contributing to the community, and perhaps avoid a lot of *stupid* starting questions.\nPerhaps there is place for two volumes ? beginners and advanced ??\n\nI think it must be possible to start and learn programming, directly via KDE but a good book will certainly be needed. (RTFM will be the credo?) "
    author: "Dalo"
  - subject: "Re: KDE Development Book "
    date: 2003-08-09
    body: "I too am very interested in this, and I hope it starts with the basics, even explaining the C++ code used.  A real straightforward, easy to udnerstand book like C++ From the Ground Up Third edition.\n\nI also hope that by the time ti will eb released it will cover KDE 3.3 too."
    author: "Alex"
  - subject: "WOW! They mentioned me..."
    date: 2003-08-07
    body: "... how exciting! The downside is, the patch didn't get in. There was a long discussion about KDE's need for a real trash system, and it seems like messing with the UI has been deferred until that has done. I am not sure if anyone is doing anything about it at the moment.\n\nYeah, KDE usability has the problem of \"lots of ideas, not much code\" and I am just as guilty as anyone in that respect. It could be that a more structured usability improvement process would work better.\n\n\n"
    author: "dave hugh-jones"
  - subject: "Re: WOW! They mentioned me..."
    date: 2003-08-08
    body: "Seems a little weird at first huh? ;-) I'm surprised I didn't bookmark them all... I would email Andras every time Quanta got mentioned or me. Probably some people thought I was a little odd, but when you look at the work that has been done on KDE it's rather exciting to get mentioned in context. KDE has some of the finest people I've ever met as both developers and users.\n\nI think as a rule of thumb you can figure that as open source grows, if you do enough, then everywhere you go in your travels you can expect to get invited out for a beer. ;-)"
    author: "Eric Laffoon"
  - subject: "just one word for you Russel:"
    date: 2003-08-08
    body: "thanks"
    author: "protoman"
  - subject: "Very nice"
    date: 2003-08-08
    body: "Enjoyed the coverage of kmail. I imagine a little editing was required to make it suitable for all ages.\n\nDerek"
    author: "Derek Kite"
  - subject: "Appreciation"
    date: 2003-08-11
    body: "Keep on keepin' on, Russell.  I enjoy the traffic report, and appreciate the time and effort you spend on it.\n"
    author: "Chris Goldman"
  - subject: "On the new feature for KMail"
    date: 2003-08-17
    body: "- Maybe instead of a button, you could double-click a row to edit it?\n- Maybe use a real listbox?\n- Select all items by default. If some entries already exist, first display a dialog saying this, and deselect them by default."
    author: "Daan"
---
Well, you asked for it and now you've got it, a very content-filled KDE Traffic has just been released.  Covered items include <a href="http://www.slac.com/pilone/kpilot_home/">KPilot</a>, the KDE man page generator, a new KDE development book in the works, a <a href="http://www.neundorf.net/src/view1394-snapshot.png">new KInfocenter module</a> for FireWire and proposed <a href="http://www.realness.ch/~sts/kmail/">new features</a> for <a href="http://kmail.kde.org/">KMail</a>. Get <a href="http://kt.zork.net/kde/kde20030726_60.html">KDE Traffic #60 here</a>.
<!--break-->
