---
title: "KDE-CVS-Digest for July 11, 2003"
date:    2003-07-12
authors:
  - "dkite"
slug:    kde-cvs-digest-july-11-2003
comments:
  - subject: "First post, yeahhh...:)"
    date: 2003-07-12
    body: "Thanks for another digest... Can't wait for kde 3.2 :)\n\nbest regards..."
    author: "Paulo Dias"
  - subject: "Thanks, merci, Vielen Dank, grazie..."
    date: 2003-07-12
    body: "it's the piece of chocolate every Saturday.\nYou are my hero ;)"
    author: "Nicolas Christener"
  - subject: "Kolab client"
    date: 2003-07-12
    body: "Note that KDE Kolab client 1.0.0 is available as of today :)"
    author: "Arend jr."
  - subject: "Re: Kolab client"
    date: 2003-07-13
    body: "Which tar file should we get?   Theres kdenetwork-kolab and kdepim-kolab.   Do we need both or just one?\n\nThanks."
    author: "TomL"
  - subject: "Re: Kolab client"
    date: 2003-07-13
    body: "You'll need both."
    author: "Arend jr."
  - subject: "Gnome feature - Dashboard"
    date: 2003-07-12
    body: "Those old Gnomers have been working on a very nifty feature: \n\nhttp://www.nat.org/dashboard/\n\nThis is a \"desktop integration\" strategy. All your apps send \"clues\" on a message bus ( eg DCOP) about what you are doing. Eg web pages you go to, conversations on IRC / IM, email you write/read, newsgroup messages you read, sms messages on your bluetooth phone. \n\nThe next stage lets clues get chained - in an rdf triple kind of way .. listeners can inject new clues. Eg addressbook associates an IM contact with a person. Then other listeners associate the person with other conversations you've had, emails you've sent, photos you've conscientiously tagged, files that've been indexed, bugs in bugzilla, weblogs, news,  etc etc. \n\nAll of these clues are then filtered for relevance, and displayed in a neat vertical bar ( an html widget infact) along side the desktop. This is the \"dashboard\". Could be autohidey, or a completely different gui.. \n\nAnyway, this leads to a great usability feature, the dashboard seems to sense what you are trying to do, and shows a lot of contextual information. Eg talk about a bug on IRC, get a link come up in the dashboard.. \n\nThe main driver behind this feature is Nat Freidman , Ximian CEO - he reckons it'll be in OSX and Longhorn before too long. KDE needs to be there too. \n\nKDE probably needs to get working on a similar feature so it can be in KDE 3.3. Somekind of interoperability with the Gnome one would be brill. \n\nThis is going to be a killer feature in Gnome, and its a big enough feature and \"Wow\" factor to sway a lot of people towards Gnome. Really. Gnome is approaching KDE in polish, it'd be stupid to make people choose between a great feature like this and the goodness that is KDE.  \n"
    author: "rjw"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-12
    body: "Reading over the whole thing, interesting, but a couple of issues come up.\n\nFirst is how much of it is doing on the desktop what should be done on a server? Google is powerful because they have almost limitless hardware and bandwidth to search. This is typical microsoft, making the client the center, simply because that is what they sell and control.\n\nIt seems that it is a way to display lots of information. On the navigation sidebar in konqueror, one can display things like rss feeds, weather, etc. And it has a history of\nall the websites you have visited. There is so much information there it becomes useless. It could have some context added to each link, and a search, etc. But why, when you can go (and I do regularly) gg:whateverIforgot and it is there. Since google is so efficient, how many people even use bookmarks? Sorry lypanov.\n\nThe IM notifications are simply having in one place what is done individually. The link to the addressbook data is neat, but again lots of information. Essentially having all your incoming messages send notifications to the same place. Like having Kopete integrated into Kontact. After the first 5 or 6, you get lost in a flood of information.\n\nNow if something like this could be configurable, maybe. But Nat says explicitly no configuration. So now I depend on my stupid machine to figure out what I want to see?\n\nOne good idea is the cvs tie in. One could watch cvs commits as they happen, and have easy access to the changes, as they happen. Wouldn't that have a better server-side solution? Something like the kde-commits irc channel, or kde-cvs mailing list?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-12
    body: "You really need to try it. \n\nFrom Nats blog:\n\n<i>\nPretty nifty. Obviously we're gonna have to start developing some better clue relevance/match quality functions to stop the dashboard from being such a cluttered mess. And we'll need a configuration system of some kind so people can enable the backends they like and disable the ones they don't like. I plan to start focusing heavily on UI in a couple weeks.\n</i>\n\nSo I think everyone knows it needs filtering / polishing. \n\nRe: the navigation sidebar - I agree, I never use it. I don't think thats because Google is so good though, I think its because its a bit rubbish. - It looks clunky at the side of the browser, feels like its taking up a lot of space ( for some reason a separate window ala kicker etc doesn't feel like this...dunno why).  I do use bookmarks, however... \n\nReasons its not google: \nIts \"proactive\". Its looking for your stuff before you know you need it. Its not explicitly activated.  \nIt \"knows\" a lot more about what *you* think than google does. Its not limited to the web. \nIts not so susceptible to privacy worriers like google toolbar. \nIts just a completely different experience than a search engine...\n\nAlso, I've always preferred at least nominally client side solutions. I mean, I prefer kmail to hotmail, and knode to gmanes web interface or google groups... \nRob"
    author: "rjw"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-12-28
    body: "I'm not sure that you really ubderstood the aim of this tool. \n\nIt's not to trigger your attention on events: like incoming messages. Because IM or mail apps already do this.\nIt's not to provide a central search place, nor another bookmark system.\n\nthe aim is to provide information regarding your actual work context:\n- Documents relevant to what you are writting,\n- web-pages relative to the document you're reading,\n- personnal information on who you are speaking to,\n- etc...\n\nDashboard should not display you all the information accessible, but all the one needed. Currently, the filtering method is not really developped: Dashboard is not a mature software but it's still useful.\n\nIf you want some more information on this kind of tools, see:\nhttp://www.research.ibm.com/journal/sj/393/part2/rhodes.html"
    author: "Mortimer"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-12
    body: "i've seen something extremely simliar to this on another OS... was it plan9? can't remember exactly at the moment. it was basically a command line version of dashboard. yes, i think there are some good possibilities with this concept. there are also challenges with it, though most are of the sort that get less relevant with time and hardware.\n\nhowever, i would ask, since you feel this is such a great feature and a must-have: what are you doing about it?\n\nnote that i don't mean that question in an \"in your face!\" or a \"shut the hell up, you whiney little be-otch!\" manner. it's an honest, open, and IMHO valid question."
    author: "Aaron J. Seigo"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-13
    body: "I would like to spend some of my copious spare time on this. \n\nBut it really needs some cooperation or it turns into a management nightmare - \n\nie maintaining patches for a whole band of other apps.... \n\nAnd my spare time is anything but copious :-(\n\n"
    author: "rjw"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-13
    body: "I think regarding KDE a Dashboard alike system could be started by requiring KDE apps having a specific set of DCOP commands which are needed by any Dashboard alike system but incidentally might be useful in other situations as well (accessibility as mentioned etc.).\n\nAny idea how that could look like?"
    author: "Datschge"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-14
    body: "i think that would be the wrong way around for this concept. not only would most apps find such a mandatory interface less than useful, it wouldn't really save much, if any, work for the app developers. \n\npersonally, i'd implement it by writing an app that had some sort of a UI for display (HTML sounds a bit expensive from the perspective of desktop real estate, personally) and that had a DCOP interface for accepting hints. to distribute hints, it would emit DCOP signals that interested processes could listen for. they would then register their answers by DCOP'ing the sender of the signal. you may even want to add a plugin system to the main app so as to enable an easy way to add hint translation techniques.\n\nthis would allow loose coupling and a quick path to development."
    author: "Aaron J. Seigo"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-14
    body: "you don't have to maintain patches for a lots of apps. just implement the core application and have perhaps a test app, or just improve one KDE app as a proof-of-concept. if it is shown that it's working and usable, it will get adopted via the natural processes. just create the dashboard bit, and you'll likely get help with the rest."
    author: "Aaron J. Seigo"
  - subject: "Re: Gnome feature - Dashboard"
    date: 2003-07-12
    body: "If nothing else something like this within KDE could become a nice tool for increasing accessibility in conjunction with tools like dasher and klaviatura.\n\nBut I seriously doubt that something like this will be widely used in OSX or Windows soon. OSX generally has a more application centric approach, and Longhorn won't appear in the next year(s). When do Ximian and/or Gnome intend to include Dashboard as part of their stable desktop version (so we can see the public response)?"
    author: "Datschge"
  - subject: "A nice way to get monitored"
    date: 2003-07-13
    body: ">This is a \"desktop integration\" strategy. All your apps send \"clues\" on a \n>message bus ( eg DCOP) about what you are doing. Eg web pages you go to, \n>conversations on IRC / IM, email you write/read, newsgroup messages you read, \n>sms messages on your bluetooth phone.\n\nI would say that all this is a very \"smart\" way to get your self monitored and hacked by an alien \"listener\" app. I would not likely use this \"feature\" for a security reasons.\n"
    author: "Anton Velev"
  - subject: "Re: A nice way to get monitored"
    date: 2003-07-13
    body: "since it will use DCOP (in KDE, anyways) you can \"hack\" and \"monitor\" everybody even today -- all apps are DCOP-enabled anyways."
    author: "Anonymous"
  - subject: "Re: A nice way to get monitored"
    date: 2003-07-13
    body: "wtf? \n\nThis is all local. If someone can intercept stuff on your local machine, you are very likely screwed anyway.. If an \"alien\" app can get on your box, you have a lot more to worry about than people knowing what you are looking for. \n\nThat is why this stuff should be done client side - so that no big brother accusations can be made... like Google is getting right now. \n\n"
    author: "rjw"
  - subject: "Do it all with superkaramba"
    date: 2003-07-14
    body: "Do it all with superkaramba!!!\n\nI'm sure it would work ..."
    author: "karumba!"
  - subject: "Re: Do it all with superkaramba"
    date: 2003-09-01
    body: "What is karamba? Superkaramba? I have been looking for a clear cut answer and have yet to find one..."
    author: "J Mac"
  - subject: "Re: Do it all with superkaramba"
    date: 2003-09-01
    body: "It's a dynamic multi-purpose extension to the desktop layer. Ie. you can add static and dynamic content as well as script programs resembling the behavior of eg. OS X's zoom bar etc. The problem in the end is that there can't be any real clear cut answer since the possibilities are unlimited. =P"
    author: "Datschge"
  - subject: "Yeah"
    date: 2003-07-12
    body: "This si very intersting and the GNOME camp definitely isn't behind with 2.3 branch its mostly a matter of prefference."
    author: "Ale"
  - subject: "Ugh. Stupid me"
    date: 2003-07-12
    body: "I didn't upload to the server. What you saw was a draft. Final version should be there now.\n\nBuilding html tables manually screws with your brain.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Ugh. Stupid me"
    date: 2003-07-12
    body: "Nice. ;)\n\nI think you can put the statistics as an own section right after the table of content. Having some basic bugs.kde.org statistics like numbers of open bugs/wishes, opened/closed bugs/wishes within that week and the one product with the most positive opened/closed ratio would be nice. =)"
    author: "Datschge"
  - subject: "Re: Ugh. Stupid me"
    date: 2003-07-12
    body: "Yes, I was thinking of that. Another is http://i18n.kde.org/stats/gui/HEAD/top10.php to give some coverage to the i18n efforts.\n\nHow about top 10 lines changed. It would be different from the commits.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Ugh. Stupid me"
    date: 2003-07-13
    body: "> Building html tables manually screws with your brain.\n \nThat's why I use the structure tree in Quanta if I'm doing complex tables. It enables a degree of structural validation as well as the ability to select segments by node. At least one enhanced table editor should make it into 3.2. Our table wizard only helps with the initial creation now."
    author: "Eric Laffoon"
  - subject: "Hi Eric"
    date: 2003-07-14
    body: "I was wondering if you could make it so that Quanta can save bookmarks after you've closed the program and then when you reopen Quanta, not only will your MRU files be opened, but also at the last line you were at or your bookmarks will be intact."
    author: "Bob"
  - subject: "Re: Hi Eric"
    date: 2003-07-14
    body: "> I was wondering if you could make it so that Quanta can save bookmarks after you've closed the program and then when you reopen Quanta, not only will your MRU files be opened, but also at the last line you were at or your bookmarks will be intact.\n\nWe've discussed this and we're unlikely to do it. It's got a lot of messy aspects (aside from being more work in a huge pile). For one we have to save the information somewhere and that means project files or quantarc as creating hidden files for this is a mess. Project files are also a problem if they're shared. Then there's the basic mechanism. If they're auto saved you'll end up with garbage bookmarks, as could still be the case with manual save over time. Do we expire them? \n\nPersonally I don't want old bookmarks when I open a file because I close the issues I bookmark when it's open. I also write very modular code so this is easy. If we were going to save the bookmarks I'd want it to be a manual load... and that comes back to creating a bookmark file, probably in $KDEHOME/share/apps/quanta/bookmarks. Of course that raises the question of whether we have only one per file or more, which becomes moot if it's not updated by the editor on an ongoing basis for where the bookmark actually is.\n\nSo the short answer is, if we get sort of bored or this suddenly looks important maybe, but don't expect it in the forseeable future."
    author: "Eric Laffoon"
  - subject: "Re: Ugh. Stupid me"
    date: 2003-07-14
    body: "I .:HEART:. Quanta!!!!!!!!!!!!!\n\nsorry for the OT but I had to say that ^_^"
    author: "ZennouRyuu"
---
In <a href="http://members.shaw.ca/dkite/july112003.html">this week's digest: </a>The <a href="http://kolab.kde.org/">KDE Kolab client</a> nears release. <a href="http://mail.rochester.edu/~mo002j/klavi5.png">Klaviatura</a>, a simple proof-of-concept on-screen keyboard is added to kdenonbeta demonstrating the possibilities of <a href="http://accessibility.kde.org/developer/qt.php">QAccessible</a> and <a href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/dcop.html">DCOP</a>.  The <a href="http://www.kdevelop.org/">KDevelop</a> CVS service is improved.
<a href="http://www.koffice.org/kivio/">Kivio</a> gets loads of new stencils. <a href="http://www.konqueror.org">Konqueror</a> can now
create a bookmark folder from open tabs, and can now open pages in a new tab (as opposed to a new window) when invoked from other applications.
The <a href="http://messenger.yahoo.com/">Yahoo</a> and <a href="http://www.jabber.org/">Jabber</a> IM protocols are improved in
<a href="http://kopete.kde.org">Kopete</a>.  <a href="http://devel-home.kde.org/~kgpg/index.html">KGPG</a> is moved
into the official KDE Utilities package.  And more. Enjoy.
<!--break-->
