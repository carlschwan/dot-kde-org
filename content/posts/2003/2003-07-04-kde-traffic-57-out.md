---
title: "KDE Traffic #57 is Out"
date:    2003-07-04
authors:
  - "rmiller"
slug:    kde-traffic-57-out
comments:
  - subject: "KDE traffic bug fix"
    date: 2003-07-04
    body: "sorry I can't report this with bugs.kde.org ;)\n\nBut the title says\n\"By Russell Miller and\"??"
    author: "anon"
  - subject: "Canada Day"
    date: 2003-07-04
    body: "Since you've extended a happy Independence Day to your American readers, I'd like to extend a happy Canada Day to my Canadian readers ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: Canada Day"
    date: 2003-07-04
    body: "Why does everyone celebrate being free of british rule. I mean we are not that bad right. Right..?\n\n:)"
    author: "Mark"
  - subject: "Re: Canada Day"
    date: 2003-07-04
    body: "Well you did loot India pretty badly.  :-)"
    author: "anonymous"
  - subject: "Re: Canada Day"
    date: 2003-07-04
    body: "Ive never forgiven you for sending the puritains over... I mean why couldnt the austrailians get them?\n\n-ian reinhart geiser\n(hint its a joke)"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Canada Day"
    date: 2003-07-04
    body: "Because they got the convicts of course!"
    author: "tfar"
  - subject: "Re: Canada Day"
    date: 2003-07-04
    body: "I'm deeply impressed this thread didn't set off a flame from Scotland/Wales/N Ireland - does this mean no one from these places is involved with KDE?\n\n  "
    author: "Gerry - London, UK"
  - subject: "Re: Canada Day"
    date: 2003-07-04
    body: "No I think people recognised its ment in fun.  I think we all agree the world over that our parents and governments did (and still do) some pretty silly things.  I like to pretend we are above that. \n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Canada Day"
    date: 2003-07-04
    body: "Nah people do live in Scotland. I study at Aberdeen University, so can vouch for this :)\n\nAnd yep it was meant as a joke."
    author: "Mark"
  - subject: "Re: Canada Day"
    date: 2003-07-05
    body: "Poor old covicts the got the beautiful girls and the sunshine, I sure they missed the English rain."
    author: "Jim"
  - subject: "System tools"
    date: 2003-07-04
    body: "Will they use the Gnome Setup Tools backend? \n\nThey've done a lot of the heavy lifting already, its pointless to duplicate it. \n\nAlso copying the GUI of the Gnome tools is Good Thing. \n\nIf people just learn one method of configuring things on the Free *nix desktop, \nit can only be a good thing. \n\n"
    author: "rjw"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "This is a very, very good proposal indeed.  To give some links for further information:\n\nThe source code for the backends can be found here:\nhttp://cvs.gnome.org/bonsai/rview.cgi?cvsroot=/cvs/gnome&dir=setup-tools-backends\n\nThe frontend information is available at the follow location:\nhttp://www.gnome.org/projects/gst/\n\nThe latest release information:\nhttp://linuxtoday.com/developer/2003050700726NWGNRL\n\nAbout 2 years, if I'm not mistaken, people have already undertaken an effort\nto use these tools with kde.   It stopped due to lack of people/time/etc.\nThere used to be a mailinglist devoted to this I think it was named ksysconfig,\nbut it is/was not hosted at kde.org.\n"
    author: "Richard"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "There is a pretty new effort in kdenonbeta"
    author: "Anonymous"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "Which module (ksysctrl looks e.g. not touched for a long time).\n"
    author: "Richard"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "kggst"
    author: "Anonymous"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "I have started kggst some time ago, till now only the platform chooser works. I had not enough time to do any further work, but I hope that I will have the gui for the gnome system tools network module ready till Nove Hrady.\n\nKind regards\nJoseph Wenninger"
    author: "Joseph Wenninger"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "It looks like that 3 people are working on a similar kcm module;\n\nthere is kggst, knetworkconf and a 3rd one.  For more info see the\nfollowing url:\nhttp://lists.kde.org/?l=kde-devel&m=105633689500459&w=2\n"
    author: "Richard"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "There will be a workshop at N7Y..."
    author: "Anonymous George"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "well the knetworkconf is suppose to be portable to unix, not just linux.  I will resist and protest any tool no matter how pretty looking that will not at least run on Solaris, and maby netBSD...  I think the gnome ones have a few tools that run on Solaris, but afaik most do not. \n\nRemember kids, this is a desktop for Unix not Linux.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "All sane configuration systems should be split into a GUI frontend and a OS-specific (or distribution-specific) backend. If there are not enough Solaris (BSD etc...) developers who contribute backends for their system, then this should not stop the progress on more popular systems."
    author: "Tim Jansen"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "This is what the network configs goal is.  I think the hardest part about doing it though will be to enforce the neutrality of the backend vs frontend.\n\nI mean its infinitely easier to write a gui that edits the vfstab on solaris than to abstract a fstab API that can edit the vfstab, fstab on linux and maby the other strange formats they have on AIX :)\n\nThis difficulty kinda makes it easy to stall out, but I admire anyone who can pull it off.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: System tools"
    date: 2003-07-04
    body: "AFAIK, the Gnome Tools have totally configurable backends that should work on any UNIX, as long as someone takes care of such backend. Support for FreeBSD was started some time ago, dunno what its state is as of now.\n\nhttp://lists.ximian.com/archives/public/setup-tool-hackers/2003-May/000094.html"
    author: "Traveller"
  - subject: "Nice1 Russell!"
    date: 2003-07-04
    body: "^^^^^^^^^^^^^"
    author: "Juergen"
  - subject: "Some ideas"
    date: 2003-07-04
    body: "1) IMO privacy clearing should offer an option to do that automatically on\nlogout and/or kde start\n2) \"System\" modules should not be separate but within the normal control\ncenter because it is diffiult to decide from an end-user point of view\nwhere the difference is between \"system\" modules and \"kde\" modules.\nThis distinction would probably seem arbitrary.\n3) Because it is difficult to write system modules because of all those \nvarious distributions there should at least be some common \"interface\"\nwhich distributors can \"hook\" into to provide those functions with\na common UI across all distributions. If they choose not to implement that\nfunction with this interface there is of course nothing we can do about it,\nbut they might save some time not developing UIs themselves for every\nsingle function but just a KDE-compatible CLI.\n"
    author: "Jan"
  - subject: "Re: Some ideas"
    date: 2003-07-04
    body: "\"1) IMO privacy clearing should offer an option to do that automatically on\n logout and/or kde start\"\n\nGood idea. I'll put it on my TODO list."
    author: "Ralf"
  - subject: "Re: Some ideas"
    date: 2003-07-05
    body: "Good ideas! Especially #3. I personally would like to see KDE evolve enough to provide something like Active Directory, that can be used to configure an entire network of KDE workstations. AD's concept of \"snap-ins\" is right in line with this idea.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Mr. Spammer'"
    date: 2003-07-04
    body: "Thanks for this great digest, Russel, your efforts is really great.\n.\nThe mail adrees above, futuretells63@hotmail.com, belongs to the \"Africa Million Dolar Spammer, privete mail adrees. I hope his fellows grab this adrees and spam a lot. customerservice@africainternationalbank.com - \"Dr.James Desouza\" <jamesds1@hotvoice.com> - "
    author: "Mr. Spammer"
  - subject: "System Modules for Control Center\n"
    date: 2003-07-04
    body: "\nHi! \nDo you know about\nhttp://users.skynet.be/top/webconfig/\nit integrates browser based configuration into kcontrol\n"
    author: "Ferdinand"
  - subject: "Re: System Modules for Control Center"
    date: 2003-07-05
    body: "Looks good, would be nice to revive it and move it into CVS."
    author: "Datschge"
  - subject: "KDE Release Schedules & System Admin"
    date: 2003-07-05
    body: "If we waited until all the bugs have stopped arriving then there would never be any releases. There will always be issues relating to some specific distro or some library version.\n\nSystem Admin modules for Control Center are an excellent idea, however it might be worth creating a seperate Admin Control Center."
    author: "Ian Ventura-Whiting"
---
<A href="http://kt.zork.net/kde/kde20030702_57.html">KDE Traffic #57</a> is out, with more on the KDE 3.2 release thread, news on KDE 3.1.3, a new widget, a <a href="http://www.well.com/~ralf/kcm_privacy.png">new privacy module</a> and more.  Get it at <a href="http://kt.zork.net/kde/latest.html">the usual place</a>.
<!--break-->
