---
title: "Evans Data: Linux Developers Prefer KDE"
date:    2003-08-05
authors:
  - "dvega"
slug:    evans-data-linux-developers-prefer-kde
comments:
  - subject: "in the numbers"
    date: 2003-08-05
    body: "65% of the readers will believe these findings while 56% will not."
    author: "right"
  - subject: "Re: in the numbers"
    date: 2003-08-05
    body: "I think it's more interesting that most people have more than the average number of legs (it's true, think about it).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: in the numbers"
    date: 2003-08-05
    body: "Well, using the median (or, better yet, mode) would probably be more informative in this case...\n\nMore to the point, while the latest KDE/GNOME jockeying is interesting, I'd be much more curious to know the details of the 125 people in \"other\". What is up and coming these days -- Fluxbox, ion, ratpoison? Are there still a lot of Enlightenment and WindowMaker users? CDE on Linux -- are these guys still using Red Hat 4.0?"
    author: "Otter"
  - subject: "Re: in the numbers"
    date: 2003-08-05
    body: "\nI use Windowmaker on my laptop because it uses less memory and is significantly faster than KDE. I run KDE on my desktop and at work.\n\nI actually hate Windowmaker and periodically try looking for a different lightweight window manager which works nicely. Kwin would be ideal if it wasn't so SLOW. :("
    author: "cbcbcb"
  - subject: "Re: in the numbers"
    date: 2003-08-05
    body: "try icewm."
    author: "Lee"
  - subject: "Re: in the numbers"
    date: 2003-08-05
    body: "i doubt that it is kwin that you are finding to be slow, but the added load of all the other apps and processes that are run when in a standard KDE session. btw, how fast is your laptop, and how much RAM does it have?"
    author: "Aaron J. Seigo"
  - subject: "Re: in the numbers"
    date: 2003-08-06
    body: "\nIt's a celery 333, with 128MB of RAM. It doesn't normally swap in regular use though.\n\nI don't actually run many KDE apps - I usually only have Konqueror loaded and a shed load of xterms. When switching desktop from a desktop with Konqueror to a desktop with 4 xterms it takes MUCH longer to redraw the screen, which with Windowmaker it's almost instant. I always try again when I install a new version of KDE. When debian gets QT3.2 I'll build KDE 3.2 from CVS and see if it's any better.\n\n"
    author: "cbcbcb"
  - subject: "Re: in the numbers"
    date: 2003-08-11
    body: "the refresh/redraw may well be due to kdesktop or other kde apps, but i HIGHLY doubt it is kwin... i wonder if you'd get the same problems with desktop icons turned off, or a light (or no) wallpaper, or.....\n\nbtw, how do you manage with only 4 xterms on a desktop? right now my konsole window has 11 sessions in it and i'm trying to keep it down today ;-)"
    author: "Aaron J. Seigo"
  - subject: "It's normal"
    date: 2003-08-05
    body: "It's a power law distribution, and the average it's not important, but the _typical_ value.\n\nBTW, the web, internet, and even TCP congestions have the same distribution."
    author: "Ricardo Galli"
  - subject: "We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Seriously, I really wish we could find why many companies choose GNOME to KDE like SUN and Novell so we can get ebtter funding too. I remember in one of Derek's Digests that very few people are actually paid to work on KDE, while at the GNOME camp there is a larger number o people who are actually getting paid for their work on KDE."
    author: "Alex"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "well\nsun american\nnovell american\nred hat american\nximian american\nsuze european -> kde\nmandrake european -> kde"
    author: "djay"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Except that Mandrake has pledged support to both KDE and GNOME and seems to stick to that. Mandrake's tools are all GTK-based as well.\n\nFWIW, I think besides \"americaness,\" I think some of the driving factor for companies such as Sun and Novell is the fact that GTK is LGPL'ed, thus giving them free reign with making proprietary solutions in addition to Free Software ones. Red Hat, I suspect likes GTK because they can have more influence on it than they ever could Qt (they also seem to be willing to stick to an early bet).\n\nAt any rate, this isn't to say all of this is good, I'm a long time KDE user, but its some observations...\n\n "
    author: "Timothy R. Butler"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "FWIW other reasons I've seen listed are:\n\n- gnome/gtk is in C which means more familiarity with the sun guys\n- Ximian was around meaning a pool of developers that could be easily hired\n\nI know Sun hired in Ximian for their expertise on at least a few matters, from what I recall, and that was a deciding factor. And of course the license.\n\nAs to why Red Hat prefer it, I guess it's because they have lots of developers who are experienced in it. It's also more aligned with their (perceived) needs for what a corporate desktop should be like.\n\nLike most things, who really knows?"
    author: "Mike Hearn"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Your first reason is bullshit (sorry), given what a mess GTK+/C is compared to Qt/C++.  Many have acknowledged this except for the trolls looking for fake \"reasons\".  I don't know about your second reason but seems to me Sun isn't very interested in Ximian except for Evolution.\n\nAs for Redhat, well of course they have great amounts of control over GNOME since they control GTK."
    author: "ac"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-10
    body: "If you going to accuse others of BS, then you should atleast refrain from it yourself. RH doesnt control GTK -- TT has FAR more control than RH ever will. Just because their employees are highly placed, doesnt mean their employees are sheep, or dont have to listen to the community.\n\nLanguage IS a factor for what people use. It might not be the most rational factor, but you are full of it if you dont think so. In fact I initially wasnt going to use GTK because it was C based, until I found the nice GTKmm C++ bindings.\n\nAs far as \"GTK the mess\" you should atleast have a single clue about what it is your talking about. GTK is one of the most elegant C libraries Ive ever seen -- its so good it even does OO in a reasonable manner, which C was never designed to do. I dont like C, its not designed for modern large scale development, so I use C++, but GTK is NOT a \"mess\". \n\nGo Troll somewhere else.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "<I>Except that Mandrake has pledged support to both KDE and GNOME and seems to stick to that. Mandrake's tools are all GTK-based as well.</I>\n \nI think that it isn't so much a case of them wanting to use GTK as it is them wanting to use Perl.  All of the extra applications are in Perl.  Until recently there hasn't been as a stable of an offering of Qt for Perl.\n\nHopefully now that <a href=http://perlqt.sourceforge.net/index.php?page=faq>smoke and Qt Perl</a> are going strong there will be more of a chance of them using them for the utilities.  Of course - they may need to work for KDE Perl bindings to be done."
    author: "Paul Seamons"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: ">I think that it isn't so much a case of them wanting to use GTK as it is them wanting to use\n>Perl. All of the extra applications are in Perl. Until recently there hasn't been as a stable of an\n>offering of Qt for Perl.\n\nI'd agree with that and, as you say, PerlQt is coming along nicely now - a great testament to the ongoing work by Ashley and Germain.\n\nThis could be an ideal time for Mandrake to review MDK Tools, since some of them very buggy. The odd thing is that RedHat, Mandrake, and SuSEs tools are all pretty flakey - try their respective log file viewers on large log files to see just how bad they are - bizarre really, since it would only take a few hours coding at the most to fix them."
    author: "Rich"
  - subject: "I don't believe that."
    date: 2003-08-05
    body: "In fact here is what miguel said about that: http://osnews.com/comment.php?news_id=4198"
    author: "Alex"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "> sun american\n> novell american\n> red hat american\n> ximian american\n> suze european -> kde\n> mandrake european -> kde\n\nKitty Hooch (me) KDE sponsor - American (Though Andras is a Hungarian living in Romania)\n\nSo remember, if you really don't like Americans don't use Quanta. I'm sure there's a lot of other parts of KDE you'll want to uninstall too but I'll let others post this in a long and tedious fashion so you can watch it melt slowly. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "LOL. I hope it won't get that far. I for my part don't have any problems with \"American\"\nsoftware. Usually it's a rather American type of behaviour to rename French Fries into\nFreedom fries and pour away German beer for some foolish reason. KDE is quite an\ninternational project even more than some other Open Source projects. No reason now\nto spin it into sth. European. It's been invented here and has lots of sponsors in Europe,\nbut anyone in the world can help working on this project. So all you Americans: No more\nexcuses why you don't help making KDE a better desktop ;-)\n"
    author: "Jan"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: ">> American type of behaviour to rename French Fries into\n>> Freedom fries and pour away German beer for some foolish reason. \n\nI really hope that most Europeans know that that was pretty much a joke; nobody on this side of the world really referred to them as \"Freedom Fries\" except in jest (other than the original cafeteria which was the source of the name and widely and rightly lampooned for being quite silly).\n\nAs for KDE, I keep hoping that it will break down walls through cooperation, but threads like this worry me intensely.  Less about KDE and more for humanity."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "> I really hope that most Europeans know that that was pretty much a joke\n\nSorry to disappoint you, but here (in France), there was very little indicating that this was a joke (seeing the general tone of Fox News toward France and Germany at the time, it would have required some serious stretch of thought to imagine so). It was just downright appalling."
    author: "Guillaume Laurent"
  - subject: "Ack!  Not FOX!"
    date: 2003-08-06
    body: "FOX News does NOT represent the mainstream American point of view.  They have a definite agenda that they try to push, using the \"repeat a lie often enough . . .\" method.  :P\n\nI would argue that seeing it on FOX (or Faux, as it is often called) \"News\" should be taken to mean the opposite is true.\n\nPleeease, do not judge all Americans by FOX.  \n\nOr CNN, for that matter.\n\nThanks!"
    author: "Ed Cates"
  - subject: "Re: Ack!  Not FOX!"
    date: 2003-08-06
    body: "> FOX News does NOT represent the mainstream American point of view.\n\nThat's somewhat reassuring, but isn't it the most watched information channel ?\n\n> They have a definite agenda that they try to push\n\nThat we know. From here it seems they succeeded in pushing it.\n\n> Pleeease, do not judge all Americans by FOX.\n\nI don't, as I routinely discuss with americans, but it's somewhat hard not to. To have such blatant propaganda as the leading information TV channel does not improve how a country is seen from abroad (then again, that's just one among the many issues in how the US are seen from \"Old Europe\" these days :-)."
    author: "Guillaume Laurent"
  - subject: "Re: Ack!  Not FOX!"
    date: 2003-08-07
    body: "I believe that Faux is the highest rated CABLE news channel.  Which makes it a far cry from being \"the most watched\" anything.\n\nI understand that it's hard to not judge all Americans by the news outlets, especially Faux.  But Mr. Murdoch knows which politics and policies will best line his pockets, and he knows how to sell them to the American public.  Such as, tax cuts.  You would be surprised how many Americans foolishly believe that THEY are in the upper 1% income bracket, and are thus lined up to get more money back from the federal gov't this year.\n\nProbably about the same percentage that believe Iraq had something to do with 9/11, but I'm getting waaaay off topic now.  ;-)\n\nSad.\n\n"
    author: "Ed Cates"
  - subject: "Re: Ack!  Not FOX!"
    date: 2003-10-17
    body: "Funny, FOX news is news, and pretty straight up. You and your ilk, have become so accustomed to the Communist News Network, that anything not along the party-line of neo-socialism is seen as an agenda of the right.\n\nWake up and get real. News, whether it be print or TV, is nothing more than a chronicaler of events, however Tom, Dan, and Peter, as well as the krew at KNN, only tell you the story they think you are capable of understanding. If you took a minute to actually listen to those three talking heads and the KNN knuckleheads, you would realize that anything positive in the USA is not to be reported, and anything bad, should be reported and made overly important.\n\nWay off topic is tax cuts, but if you insist, according to the latest figures from the IRS, 96% of taxes are taken from the top 50% of wage earners. So, before you spout that crap about tax cuts for the rich. look up the numbers. They deserve a break because they pay the largest chunk of taxes. And those who make small incomes pay almost no taxes percentage wise."
    author: "Bob Collins"
  - subject: "Re: Ack!  Not FOX!"
    date: 2004-05-17
    body: "I think that Fox News represents a refreshing change of pace from CNN. Case in point: Hannity and Colmes. Sean Hannity is a articulate and opinionated person with integrity who is clearly a conservative. Alan Colmes is also an articulate and opinionated person, also with integrity, who is clearly a liberal. Each of them respects the other person's opinion, if not their ideologies, and each of them tries to be fair and balanced to each other and their guests. This is a very nice change. I respect how each of them is willing to say when their personal perspective differs from that of their parties; they aren't parrots of party philosophy.\n\nWhat I don't like about CNN is how one sided the reporting is, but that's ok. Everyone, including news networks, are entitled to their own opinions -- I'd just like to hear both sides. I'm an American and I respect and support both \"sides\" on different issues. For example, I support privacy, civil and personal liberty, the European Union, and the due process of law in its entirety -- i.e., not too much power to police agencies. That would brand me a Democrat. On the other hand, I believe Bush is doing a very good job in going after terrorists where they live (although I disagree with some of the more aggressive information-gathering policies on US citizens) and I think he's done a good job in a tough situation; I also credit him -- and Alan Greenspan -- with bringing back an economy that crashed two months after he won office. He got handed a bad economy and a very tough war to fight, and he's done admirably. I'll vote for him in the upcoming election, although if John Edwards or even Wesley Clark was still in the running, it would be a closer call. (I just have zero respect for John Kerry.)\n\nWhat I like and respect about Fox News is that they mean what they say: We report, you decide. You do hear both sides on Fox, with the possible exception of O'Reilly Factor. (I do agree with a good portion of O'Reilly's points, but he's a bit strange for my tastes.) I think Shepard Smith is a lot more interesting than Aaron Brown and he seems to have a sense of humor.\n\nNot sure what brought this up, but I just wanted to toss a vote in for Fox News. They're doing a good job by me. CNN just isn't balanced enough.\n\nThis is a big world. We need to listen to all sides. Everyone shouldn't get their news from just one source anyway -- even if the one side consciously avoids bias, an institutional bias is inevitable and must be balanced by news from multiple sources. We're intelligent enough to discern the truth most of the time. (You can't fool all the people all the time.)\n\nSo, the other responses are right: don't judge a country on its news reporting, anymore than the non-Arabs should judge the Arab world based on Al Jazeera. There are always dissidents, and most of the time the natural biases built into the media does not even reflect popular view; instead, it's the other way around: the natural biases try to shape popular view.\n\nJust two cents worth...."
    author: "Jamieson Becker"
  - subject: "Re: Ack!  Not FOX!"
    date: 2004-12-06
    body: "do you actually watch the news????? ummm if you read the PIPA survey it proves that people who do watch Fox news and only Fox news are less informed than people who get their news from other sources. before you open your mouth read the facts....."
    author: "Mel "
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "I don't think the author meant his/her comment as something anti-American, more like an observation. I find it pretty obvious that companies invest in \"local stuff\", so it's pretty natural for large US companies to support Gnome which is largely based and controlled by smaller US companies and individuals. \n\nI just wish that European companies did the same and invested more in largely European projects, such as KDE. But it's a sad and curious fact that there really is very little IT industry in Europe, the few large ones focus on mobile phones and technologies related to that. Few European companies do shrink wrapped software or anything else related to desktops.\n"
    author: "Chakie"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "This is not only about \"local stuff\", also about personal relationships. If you look, for example, at the speakers of US conferences like Linux World you can rarely find a speaker who does not live in the US. And those who do are usually from Canada, Australia or the UK. On the other hand you can frequently find US speakers at european conferences.\nOne of the main reasons for this is the language barrier. While most europeans, especially in the open source field, understand english well enough to listen to a US speaker, most of them do not have sufficient skills for a good presentation in front of an english-speaking audience. \n\nThe personal network among Gnomes, large (US) companies and well-known hackers is much tighter, while it often appears like KDE is quite disconnected from the rest of the free software world."
    author: "AC"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "It's funny all this comes up. Americans are really okay as are Europeans... but those Canadians... There's talk of building a large wall at the border. (I know my Canadian friends know I'm teasing them.) ;-)\n\nMaybe I should seek to get on the speaking circuit. I lead a popular project, have experience doing public speaking and love to work a crowd. I also have a good perspective and a visionary approach in my area which I can couple with my irreverent humor and moments of insane rambling. I would think I would have a very good chance of having them in awe of the excellence of KDE or laughing hysterically. At least they would have some opinion as to whether they would want to program in close proximity to an opinionated nut case. But hey, wouldn't we all? ;-)\n\nCheers"
    author: "Eric Laffoon"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Hey, I didn't say that it's a bad thing. But there is some language, or cultural barrier, or whatever barrier. It's hard to dispute that all >5 people companies that employ KDE contributors are european and from non-english-speaking countries(suse, trolltech, mandrake, connectix(sp?), sap, kroupware consortum etc, see the last cvs digest).\n\nBTW I wonder why the more visible US distributions that are using KDE (Lindows, Xandros, Lycoris) are hardly visible in the KDE community.\n\nGnome, on the other hand, is supported by Ximian/Novell, Red Hat, Sun, HP and maybe others. Is there a european company that is doing any Gnome work?\n\nThere must be a reason why it is like this. Assuming that there are no technical problems, the logical consequence would be some failure of communication.\nSo what is it? Location (geographical distance between the KDE founders and the english-speaking world)? Language (english-speaking people are more comfortable to speak wit other native speakers)? Promotion (KDE people seem to attend more trade fairs in Europe)? Culture? Patrotism/'Continentism'? My guess is \"relationship networks combined with the language barrier\". But that's just my guess.\n"
    author: "AC"
  - subject: "Lindows, Lycoris, Xandros"
    date: 2003-08-05
    body: "They are largely ignored in the KDE and Linux community because they are not for us, if we're interested enough in DE to visit the Dot it usually means that we are curious enough to want to really learn Linux and have the latest version of KDE.\n\nxandros and Lycoris are both running KDE 2 and don't have the klatest and greatest so we don't really care too much. Lindows is more ignored ebcause of their CNR which gives you free packages in an easier way to install but for qutie a price. Also, because of their marketing lies.\n\nIt has nothing to do with location IMO. "
    author: "Mike"
  - subject: "Re: Lindows, Lycoris, Xandros"
    date: 2003-08-10
    body: "http://www.lycoris.org/gallery/gallery/desktop_screenshots/manage-files.png\n\nThat doesnt look like KDE 2 to me."
    author: "Ryan"
  - subject: "Re: Lindows, Lycoris, Xandros"
    date: 2003-08-10
    body: "This is clearly KDE 2.2 with Lycoris adaptations."
    author: "Anonymous"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "> BTW I wonder why the more visible US distributions that are using KDE (Lindows, Xandros, Lycoris)\n\nXandros, when they were Corel, used to have 20+ KDE developers. Alas, they are still working on KDE 2.x :(\n\n>  Gnome, on the other hand, is supported by Ximian/Novell, Red Hat, Sun, HP and maybe others. Is there a european company that is doing any Gnome work?\n\nMandrake does a little bit with GNOME. HP doesn't work on GNOME anymore. "
    author: "fault"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Xandros just announced a beta program for Xandros Desktop 2.0 which \"will incorporate the latest version of KDE plus several unique innovations from Xandros\"."
    author: "Anonymous"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "I don't really think it's a US vs Europe thing, but a licensing issue. Companies know that they can create closed-source software based on GTK+ without paying anyone.\n\nI'm not trying to complain about KDE's license here. One license favours free software, and the other is business-friendly. Nothing wrong with either approach."
    author: "AC"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Same here. I use KDE as desktop, but I do not target for the QT widget purely because of license issues. Its almost a paradox and while I plaud KDE development, the fact that I just cannot afford to use QT at this moment makes it much more attractive to develop for other wxWindows in this case - a shame really.\n\nCross plaform is the wave of the future, as its about platform independance AND Open Source AND making a bloody living."
    author: "IR"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "...but I do not target for the QT widget purely because of license issues.\n\nI meant I do not develop software to use QT"
    author: "IR"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "You mean you are more likely to earn money by working on closed software which you then sell instead by offering your work on improving open software as a paid service? Just asking."
    author: "Datschge"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "My purpose is commercial use plus having a GPL license to my software.\n\nThe only thing available under the GPL seems QT v2.3 for windows. How does that encourige using QT as a cross platform tool? I dont want to develop using an old version of QT."
    author: "IR"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "Who said that the use of Qt as a free cross platform (=win) tool should be encouraged? TT obviously does not have an interest in it, and most KDE people do not care.\n\nIf your platform is proprietary, use proprietary tools."
    author: "AC"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "There was never a Qt 2.3 for Windows available under GPL."
    author: "Anonymous"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: ">>There was never a Qt 2.3 for Windows available under GPL.\n\nWhat I meant was that if my software is GPL and I want to use QT on windows, then I seem to have to use a QT version 2.3 - this doesnt make QT attractive\n"
    author: "IR"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "That's like saying the $500 don't make MS Office attractive :)\n"
    author: "Jeff Johnson"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-07
    body: ">>>That's like saying the $500 don't make MS Office attractive :)\n \nThats true I get your point, but its also a matter of entrance barriere for new developers/managers. One entrance barriere early on and you are not very likely to port the whole thing back to QT once you do have the money."
    author: "IR"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "There was a free edition for non-commerical use."
    author: "CE"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "> I don't really think it's a US vs Europe thing, but a licensing issue. Companies know that they can create closed-source software based on GTK+ without paying anyone.\n\n\n.. except that companies license other people's software all of the time. It's more of a C++ versus C thing mostly.. and the fact that a truly KDE company doesn't really exist like Ximian."
    author: "fault"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-09
    body: "Lycoris."
    author: "Anonymous"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-11
    body: "Lycoris is a distro maker, Ximian is an application house. how many core KDE devels does Lycoris employ? how many LOC in KDE's CVS are due to Lycoris employees or their sponsorship? how many CVS modules are maintained by Lycoris people? not saying that Lycoris is bad, just that they aren't a Ximian-alike. heck, SuSE does a TON more than Lycoris for KDE, and even SuSE isn't a Ximian-alike."
    author: "Aaron J. Seigo"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Novell purchasing Ximian is different than what we've seen before. IBM, Sun, HP, and others are hardware companies that sell software/hardware solutions. FOSS is a natural for them, commoditize software, value-add their hardware. Novell is a software company. They sell shrinkwrap.\n\nShould be interesting to see how it works out. I suspect we'll see more like the Exchange connector that Ximian brought out; closed proprietary based on lgpl libraries.\n\nI'm not sure if that is good. Brings in market share and all that, but is it free as in speech?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Why guess?  There was an interview with Sun programmers involved with GNOME a while back where they said it was because GTK/GNOME are written in C.  Old-time C/Motif hands are going to be much more comfortable with that style of API than with OOP and Qt.\n\nI doubt if nationality or even licenses had much to do with it."
    author: "Jaldhar"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "People at Sun now believe they should have bought TrollTech and then made Qt LGPL. It is much more elegant and cross-platform compatible than Gtk. Unfortunately, Sun only had Unix on its mind when it initially got involved.\n\nSigh..."
    author: "Bob James"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "What is your source for this?"
    author: "claes"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-06
    body: "His ass."
    author: "Anonymous"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-10
    body: "I wish I could moderate this \"+1, funny as hell\".\n\nLOL Im still smiling.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "There are a few other more important reasons :-\n\n1] Binary compatibility:\n    Sun has a commitment to BC (look at Solaris/SPARC).\n    C++ makes this harder, especially due to the everchanging ABI. Binary\n    incompatibility is an utter nightmare for ISVs.\n\n    Ref: http://developers.sun.com/tools/cc/articles/CC_abi/CC_abi_content.html\n\n2] Licencing:\n    No need to explain here... they obviously want to develop commercial \n    applications without licencing fees.\n\nI doubt whether the quality of kde versus gnome at the time of their\ndecision had any impact, or that the ammericanness of gnome is related\nto this either.\n"
    author: "JohnK"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "There are wide reports that internal developers at Sun regret the decision.  If you think OOP is very pretty in C/GTK compared to C++/Qt then you must not be very well informed. :-)"
    author: "ac"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "The wide-spread reports come primarily frm a single developer, who would like to re-write the kernel in Java; and doesn't live in the real world IMHO."
    author: "Anonymous coward"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "No it comes from actually talking to Sun developers at conferences."
    author: "ac"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "That's not what Sun developers said at Linuxtag this year :)\n\n(although American and European Sun developers may beleive differently.. rh-Europe has always been more pro-KDE than their American counterparts)"
    author: "anon"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-10
    body: "So? Your never going to get a multitude of developers to agree on a choice like that. \n\nIf your source is real, hes not very well informed. Certainly at the time there was no GPL QT, let alone an LGPL QT. Also, while GTK+ manages to support OO in C, something that C was never designed to do, in a reasonably sane manner, there is also the well maintained GTKmm C++ bindings which that disgruntled developer could use. \n\nIf you could supply the name of the developer that you personally spoke to, Id be interested to get his further opinions.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-10
    body: "Basically my point is, aside from license issues there is no real difference between the two libraries."
    author: "Ryan"
  - subject: "Re: We all knew that didin't we ;)"
    date: 2003-08-05
    body: "Dont worry! Trolltech works for us! ;)"
    author: "Marc"
  - subject: "GPL"
    date: 2003-08-05
    body: "BTW, it's really strange that now the GPL is becoming the major concern.\nHey GPL's supposed to be A Good Thing (TM). Of course it can't be hijacked\nby commercial interests as easily as other licenses. But wasn't that what it's been\ninvented to do?"
    author: "Jan"
  - subject: "Re: GPL"
    date: 2003-08-05
    body: ">Hey GPL's supposed to be A Good Thing (TM)\n\nThere is nothing that is \"A Good Thing (TM)\", that's why there are other licenses: LGPL, BSD,etc...\n\nI personnaly prefer the LGPL for libraries, toolkits, and the like and the GPL for applications, YMMV of course."
    author: "renoX"
  - subject: "Re: GPL"
    date: 2003-08-05
    body: "Hijacked by commercial interests?\n\nPeople need to realize that on a given OS there needs to be a FREE GUI toolkit to develop your applications with (e.g. Win32 + DirectX on windows). While I _love_ KDE, I can't be surprised if Gnome gets more supported because it's LGPL. The only stumbling block in KDE's mass adoption in the licensing issue really.\n\nJust my 2 penneis\n"
    author: "Slovin"
  - subject: "Re: GPL"
    date: 2003-08-05
    body: ">People need to realize that on a given OS there needs to be a FREE GUI toolkit to develop\n>your applications with\n\nWhile I partially agree with that, be careful with the word 'free' :). According to the FSF Qt is free, more that GTK+.\nIt's indeed somewhat unfortunate that KDE's success is connected with with the willingness of vendors to produce software that is also free, or to pay a non-trivial amount of money to TT. \n"
    author: "AC"
  - subject: "Re: GPL"
    date: 2003-08-06
    body: "Don't let FSF define free for you. It seems free to them means \"The freedom we like, and if you don't like it, go kill yourself\" Seriously, how can something that gourges you more be freer. LGPL says, if you modify my program, then make you modifications freely available, but if you want to link to this LGPL'd one, it ok. To me that is freer than GPL. But GPL is also very useful in other contexts, when you want to limit these freedoms. Like in the kernel. Now, we don't want people running away with out kernel a la Apple/OS X/ Darwin do we. Nothing against Apple, the day I get enough money, I buy an Apple."
    author: "Maynard"
  - subject: "Re: GPL"
    date: 2003-08-06
    body: "LGPL is less restrictive. The GPL is about keeping freedom, not having temporary freedom. If you don't want it you could also buy the source code and a site license of a proprietary app."
    author: "AC"
  - subject: "Re: GPL"
    date: 2003-08-06
    body: "<i> It's indeed somewhat unfortunate that KDE's success is connected with with the willingness of vendors to produce software that is also free, or to pay a non-trivial amount of money to TT. </i>\n\nAh, please, name a couple of companies that use GTK+ in closed source applications, preferable crossplatform.\n\n"
    author: "Kevin"
  - subject: "Re: GPL"
    date: 2003-08-07
    body: "http://wingide.com/\nhttp://www.vistasource.com/\nhttp://www.roebling.de/"
    author: "notme"
  - subject: "KDE vs Gnome"
    date: 2003-08-05
    body: "The real reason why Gnome has the number of users that it does is down to the popularity of Red Hat, especially with their contracts with big companies. Since Red Hat installs Gnome by default and Red Hat is the most popular distribution its popularity doesn't seem that strange (Yes I know that KDE is an option on Red Hat).\n\nTo move a greater number of people to KDE Red Hat need to give greater support to it, this maybe forced onto them if KDE continues to grow into a dominant Linux desktop. Maybe another company like Mandrake (with their financial issues) or SuSE (with their excellent corporate contracts) could replace Red Hat as the no 1. Linux provider? They both use KDE as their default desktops."
    author: "Ian Ventura-Whiting"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "Umm... Redhat is the most popular in the _US_, isn't it?  Here in NI, everyone I know uses Debian.  It's similar everywhere I hang out online, too."
    author: "Lee"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "*shrug* It seems that SuSE and Red Hat are both popular in the UK. I know at my work place (former DERA, so lots of scientists/engineers) there was a mix of Red Hat and SuSE. \n\nI think a lot of people choose their distro based on their preferred desktop. I know I switched from SuSE to Red Hat because I decided I preferred Gnome and suses gnome simply wasn't as good.\n\nI'm not sure how this story got posted, considering the numbers add up to more than 100%. But then it's bashing gnome, so I guess that's why it got posted :("
    author: "Mike Hearn"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "> I'm not sure how this story got posted, considering the numbers add up to more than 100%\n\nThe survey did not ask for the preferred DE, but for the DEs that the developers use. Obviously many devs use both.\n"
    author: "AC"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "I guess it depends on the definition of a \"case\". The tallies for each desktop all added up to 600, so I can't see how they could have voted for more than one. Then the next column is percentage of \"cases\" which adds up to more than 100, but case isn't defined anywhere."
    author: "Mike Hearn"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-11
    body: "case == respondant\n1 respondant provide >= 1 response\nso responses >= cases\nergo >100%"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "I'm not sure why you post here, considering that you always bash KDE."
    author: "ac"
  - subject: "Redhat & KDE"
    date: 2003-08-05
    body: "Btw, you are wrong on another thing.  There are a LOT of Redhat desktops deployed that default to KDE, customized or otherwise.  Like the City of Largo for instances."
    author: "ac"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "I don't know any RedHat user who hasn't even at least tried KDE."
    author: "Ac"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "Don't forget that Redhat basically crippled KDE to be worse than GNOME.  Most users don't know that."
    author: "ac"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "Troll!!!"
    author: "Anonymous Coward"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-05
    body: "> Don't forget that Redhat basically crippled KDE to be worse than GNOME\n\nSuch as? (I know about the default blue curve theme and the mozilla icon on kicker, but you must be referring to something else)\nActually I was quite impressed by the kde 3.1 desktop on rh9."
    author: "KV"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-10
    body: "This is bunk, that has been discussed and flamed beyond recognition. RH changed KDe in ways that the community didnt like, but there was nothing done that was crippling.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-11
    body: "by shipping with a crash-prone KWin deco, a buggy style and changes to includes and desktop flie locations that were incompatible with other more stock KDEs, they deffinitely hurt KDE's showing. most if not all of these things were addressed in future releases.\n\nsomething that wasn't was defaulting to pretty much all non-KDE apps when choosing the KDE desktop as your platform of choice. i understand their reasons for doing so, but many still consider that \"ripping the guts out of\" KDE.\n\nhope that helps you understand how others have arrived at an opinion so different from yours."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-11
    body: "As I said above, I have heard all those arguements to *death*. Bottom line: those who think RH is out to \"destroy\" KDE need to retire their shiny tin-foil hat and find something more productive to do. You may not have liked they changes, but they were done to satisfy their customers, not KDE (or GNOME, if you think Im being unfairly critical) hardcore users.\n\nIn fact, my guess is that RH wont ship the \"Ximainized\" OpenOffice 1.1 (with font AA, Gnome icons, etc) because it is inconsistent between desktops.\n\nHope that helps you understand how others see KDE as not so hard done by with regards to RH.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: KDE vs Gnome"
    date: 2003-08-06
    body: "Red Hat's Linux version is now using a community development model (http://rhl.redhat.com/), similar to and thus competing against Debian. If it's as open as they imply KDE fans who prefer Red Hat should be able to help improving KDE within Red Hat."
    author: "Datschge"
  - subject: "SUN is going to use SuSE"
    date: 2003-08-05
    body: "Did you all see the the message on the SuSE site that they have an agreement with Sun? (http://www.suse.de/en/company/press/press_releases/archive03/sun.html) So maybe Sun will start using KDE..."
    author: "harry"
  - subject: "Re: SUN is going to use SuSE"
    date: 2003-08-05
    body: "It doesn't look like that."
    author: "CE"
  - subject: "Re: SUN is going to use SuSE"
    date: 2003-08-05
    body: "No, sun have basically fully committed to GNOME.  They're active developers of it -- writing docs, adding patches regularly, etc.  And they just announced full &amp; ongoing support for GNOME on their systems."
    author: "Lee"
  - subject: "Re: SUN is going to use SuSE"
    date: 2003-08-05
    body: "Sun has provided KDE with Solaris for quite a while too."
    author: "ac"
  - subject: "Re: SUN is going to use SuSE"
    date: 2003-08-05
    body: "You dream. That is for Linux, not Solaris, which is where they default to GNOME"
    author: "Anonymous Coward"
  - subject: "Re: SUN is going to use SuSE"
    date: 2003-08-06
    body: "I read today that the version of Sun will use GNOME.\nIt's sad to see that it'll be very uncomfortable. ;-)"
    author: "CE"
  - subject: "Weird research?"
    date: 2003-08-05
    body: "1) How can 65% prefer KDE while *more* than 35% prefer GNOME? That doesn't make sense.\n\n2) \"As of this survey, however, KDE has jumped ahead of GNOME by a significant margin for the first time.\" - is it just me or does this sound like \"OMG we won! victory! you suck wahahahaha!!!\"?\nAnd if I remember correctly, there have been a lot of polls in the past about GNOME vs KDE usage. And for most of them, KDE has the most votes. What the heck is \"As of this survey\" supposed to mean? To declare a victory to a \"war\" that never existed in the first place?"
    author: "Ac"
  - subject: "Re: Weird research?"
    date: 2003-08-05
    body: "> 1) How can 65% prefer KDE while *more* than 35% prefer GNOME? That doesn't make sense.\n\nThey asked what they use. Some developers use both KDE and GNOME quite obviously."
    author: "fault"
  - subject: "KDE & Gnome user"
    date: 2003-08-05
    body: "On my Redhat 7.3 desktop I use KDE, but I use Gnome almost exclusively for any configuration issues. I find Gnome easier to use when configuring the system. On my Redhat 6.2 (Yes we still use it) & 8.0 Servers it's Gnome all the time. "
    author: "D.L.Evans"
  - subject: "Re: KDE & Gnome user"
    date: 2003-08-05
    body: "Hmmm, but what does configuring the system have to do with GNOME or KDE?"
    author: "ac"
  - subject: "disney likes qt"
    date: 2003-08-05
    body: "stolen from kde-cafe:\n\n---------------------------------------------\nfrom the source article at http://www.eweek.com/article2/0,3959,1210083,00.asp\n\n\"[Jack Brooks, director of technology at Walt Disney Feature Animation,] and\nhis team also moved all their GUIs to Qt, a multiplatform kit from Trolltech\nInc., and ported more than 4 million lines of code to Linux.\"\n\napparently they have 600+ desktops running Red Hat Linux... nice...\n\n- --\nAaron J. Seigo\n-----------------------------------------------\n\nand so much for american vs european!"
    author: "anon"
  - subject: "Re: disney likes qt"
    date: 2003-08-05
    body: "> apparently they have 600+ desktops running Red Hat Linux... nice...\n\nOf course, but being redhat, they are most likely using GNOME."
    author: "anon"
  - subject: "Re: disney likes qt"
    date: 2003-08-05
    body: "No way.  KDE is stronger than you think.  That's the only reason Redhat didn't get rid of it from their distribution...  too many big customers are using KDE."
    author: "anon"
  - subject: "Re: disney likes qt"
    date: 2003-08-06
    body: "It wouldn't make much sense to them to move all their GUIs to Qt when they actually use Gnome, would it?"
    author: "Datschge"
  - subject: "Gnome is quite like windows"
    date: 2003-08-08
    body: "I had a problem on my red hat 9 last week. It complained about a serious error in Gconf, and since then all my gnome applications crash when I try to access them. I can't understand why GNOME guys used a windows-registry-like approach, since windows registry is a real (and undocumented ;-) mess. That's why I prefer KDE, and each new version of KDE makes me believe that it going to be the ***MAIN*** linux desktop. GNOME is nice, but needs a lot of work and efforts to be compared to KDE. "
    author: "tiago"
  - subject: "Re: Gnome is quite like windows"
    date: 2003-08-09
    body: "Just because M$'s Registry implementation has flaws does not mean one could not create good implmentation.  Indeed, it is the only sensible way to go to enable network wide configuration..."
    author: "Adrian Bool"
---
A <a href="http://www.evansdata.com/n2/pr/releases/edc_linux_aug_4_2003.shtml">recent study</a> prepared by <a href="http://www.evansdata.com/">Evans Data</a> concluded that <a href="http://www.evansdata.com/n2/surveys/linux/linux_03_2_xmp1.shtml">KDE is the preferred desktop environment</a> amongst Linux developers. 65% of the 600 surveyed said that they currently use KDE, against 56% for GNOME. Nicholas Petreley, Evans Data's Linux analyst, said <i>"It is also interesting to note that KDE has taken a statistically significant lead in use and deployment after running neck and neck with GNOME for the past three years."</i>

<!--break-->
