---
title: "KDE and Konqueror win Open Choice Awards 2003"
date:    2003-09-21
authors:
  - "binner"
slug:    kde-and-konqueror-win-open-choice-awards-2003
comments:
  - subject: "Congratulations"
    date: 2003-09-21
    body: "To all the developers and everyone else involved in the project. These are awards that are well deserved."
    author: "Vic"
  - subject: "Re: Congratulations"
    date: 2003-09-21
    body: "I second that, thanks to every one involved."
    author: "Sean Og"
  - subject: "KDEPrint, kdevelop and konqueror explicely named"
    date: 2003-09-21
    body: "Although I know how \"mission critically\" important printing may be for professional environments -- that they should explicitely mention KDEPrint (along with kdevelop and konqueror) was little surprise to me, albeit a pleasant one.\n\nKudos to Michael Goffioul for most of this KDEPrint development work -- and I hope yuu'll find time to come back to some level of active development soon....  ;-)"
    author: "Kurt Pfeifle"
  - subject: "Re: KDEPrint, kdevelop and konqueror explicely named"
    date: 2003-09-22
    body: "Cause we are on kdeprint:\n\nIn a medium to large printing enviroment something like choosing configured presets seems to be more usable than the current printing dialog.\nI normaly use a color printer with a draft, transparent and a photo setup, an A0-poster printer and 2 b/w laser printers with a normal and a pamphlet setup.\n\nI think its better to choose your preset than choosing the printer and then change its setup(?)\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "..."
    date: 2003-09-21
    body: "But look at these awards:\nBest Instant Messaging Software: Gaim 0.68\nBest E-mail Client: Evolution 1.4 (was kmail last year)\nBest Office Suite: OpenOffice.org 1.1 (Release Candidate)\n\nNow doesn't that show that KDE will not be replacing _all_ linux-applications in the nearby future? Doesn't it show that svg-widget-themes that can be used in Gnome as well as KDE and corresponding icon-themes are essential? (and unified file dialogs)\nAnd doesn't it show that the OOo-QT-port http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/ is needed urgently (before they switch away from the VCL-layer in the distant future)?\n\nAlso a way to actually use kio-urls in gnome-apps to point to the fuse-connection http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/fuse_kio/README?rev=1.2&content-type=text/x-cvsweb-markup&f=h\nwould be somewhat helpfull."
    author: "Johannes Wilm"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "Not really, no.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "Well, we all see how popular the Qt port of Mozilla was . . .<p>\n\nAll of that would be up to the GNOME team.  I'm not a developer for either, but GNOME was the late-comer; they were the ones who instead of working with Troll Tech to make Qt GPL-friendly, and instead of helping with the Harmony project in those days when it looked like Qt would never be GPL-friendly, they ran off and made their own NIH-syndrome-infected desktop, with many of the features of GNOME but written in a totally incompatible way.\n\nI like Evolution and all, but I use KMail instead.  OpenOffice I use on occasion, but only on those stubborn MS Office docs that nothing else can handle.\n\nAnd now that GNOME is around, is it really such a bad thing to have GNOME apps be the more popular ones?  Is it really so bad that KDE isn't striving to make their system more GNOME-compliant?"
    author: "regeya"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "Sorry about that; that should read \"many of the features of KDE but written in a totally incompatible way.\"\n\nSorry for any possible confusion."
    author: "regeya"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "No matte how much you hate GNOME, they provided the leverage for Trolltech to GPL their toolkit. If they had not done that, We would have Linux running under desktop with a non free toolkit. And KDE did have a headstart."
    author: "Maynard"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "And working on Harmony instead of GNOME would have allowed Linux running under one desktop with a free toolkit. "
    author: "anon"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "I think that is very naive. It is certainly not easy to try do a clean room reimplementation of an entire toolkit. Especially when it gets pretty large. It is easier if you can fork it. Anyway, it worked out for the better. Now there are two high quality toolkits avialable for Linux. I think they need to come together and use the same themes and all, like many toolkits do on Windows. Then the compatibility issues will be gone. Freedesktop theme spec anyone."
    author: "Maynard"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "Note that I don't hate GNOME.  In fact, there are GNOME apps running on my desktop as I write this! :-D  However, I find it a little puzzling to see people calling for KDE to become more GNOME-friendly; guess I just don't understand the reason why anyone would think it's KDE's responsibility to make GNOME more KDE-compliant, or to be more GNOME-compliant.\n\nI suppose it's only fair, since GNOMErs seem to have no interest in being more KDE-friendly. ;-D\n"
    author: "regeya"
  - subject: "Re: ..."
    date: 2003-09-23
    body: "<em>I suppose it's only fair, since GNOMErs seem to have no interest in being more KDE-friendly. ;-D</em>\n \nYou are wrong."
    author: "A Debian User"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "Hello,\n\nfor a large part, kmail has stagnated until 3.2 is released. Once it is I can switch back to it. For now Mozilla Mail does it better for me mostly in terms of filtering IMAP. I guess the same would be true of Evolution.\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: ..."
    date: 2003-09-23
    body: "What do you mean, stagnated? There was a huge improvement in KMail from KDE 3.0 to KDE 3.1, particularly with it's IMAP support. The 3.1.x point releases have mostly been bug fixes, with very little feature-creep.\n\nKMail development has most certainly not stalled, as anyone who reads KDE-CVS-Digest or KDE-Traffic should be well aware of. Read up, compile it from CVS and see what's new.\n\nKDE 3.1 was released on January 28th of this year. KDE 3.2 is scheduled for early December, complete with KMail improvements. One year is not a long time to wait for a major point release.\n"
    author: "Anonymouse"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "> Best Instant Messaging Software: Gaim 0.68\n\nWhich version of Kopete did they try? Kopete rocks!"
    author: "OI"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "kopete is far from being rock hard stable like gaim is.. I would give the award to gaim too.. \n\nnext year there will be more competition though, in that field :)"
    author: "anon"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "Yup, and gaim has been around for 5 years. In that time, they've produced a wonderful app. \n\nkopete has been around for slighly more than one year, and they've come VERY far already. "
    author: "Anonymous"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "Sadly, even though I've finally made the switch to kopete from gaim a few weeks ago, Kopete is still lacking and most of the issues I see are already bugzilla'd.\n\nWe have to consider that this is 2003 and messaging clients have been around for ages and users _expect_ a certain level of maturity about them. As such even though I use kopete now I would have never given it the award even if I used cvs.\n\nThe following bugs are really a turnoff for both windows users and ex-gaim users alike: 54156, 63936, 63570.  Also, reliable sending of html messages is really non existant in kopete. Other things that I know about are that gaim handles file transfers a bit better than kopete (I've never successfully done one with kopete but with gaim it's somewhat reliable).  The ability to direct connect is also sometimes useful and more or less disabled in kopete right now.  You'll notice that these gripes are really AIM specific.  They are, but AIM/ICQ/MSN probably represent the largest population of IM users and if those protocols don't work then of course gaim will get this award.\n\nFurthermore, gaim cvs now has the concept of meta-contacts so we can no longer use that to our advantage.  But on the other hand Kopete will have nice integration with pim and the chatwindow styles will no doubt grow on people. Just be realistic and let the best program win.  Currently gaim is the best but maybe a few months _after_ the feature freeze is over, kopete will be able to topple it."
    author: "Jesse"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "I'm sorry, but... No custom AIM buddy icons a turnoff? A somewhat oddly formatted tooltip a severe problem? IMO, that's really streching what turnoff's are... I can see how not being able to chat with more than one AIM user might be a problem. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "Like I said though.  Users _expect_ this.  I hate it when my girlfriend asks \"Do you see my new buddy icon\"  and I go  \"duuuuh ... nope I don't see it.\"  I expect this functionality. Besides, there's lots of funny ones out there too and I can't see em let alone set my own -- it would be 'nice' is all I'm saying.  The hords of AOL users (of which I am not) thrive on emoticon themes and buddy icons and with so many other clients able to do it I see no shame in expecting a client with hopes of being a de-facto standard to support this either esp. when no harm is done.\n\nThe tooltip is completely useless and I expect this tooltip (like the other tooltips in KDE) to give me additional, accurate, and quick information.  It's broke, unprofessional, and embarrising when I try to explain the merits of KDE when the idle time cannot even be reported correctly or even in some sort of sane fashion.  I expect this and I think everyone else does in principle as well.\n\nSo yes, no buddy icons are turnoffs to the hundred of thousands of people who need/want/expect that.  And when information is wrong and useless (like the tooltip) I might stretch that to severe. Gaim does not have these problems and so it should get the award. I was merley explaining the rational behind not giving the award to kopete because such perceived _basic_ functionality is missing."
    author: "Jesse"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "I'm pretty sure both ichat and trillian don't have AIM buddy icon support and they are used by hordes of non-technical users.\n\nOf course, this would be nice to have, but unfortunatly, with all things open source, it requires somebody with the time/motivation to do it :)"
    author: "anon"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "What are you talking about?  iChat and Trillian both have AIM buddy icon support.   iChat uses the buddy icon as a character in the chat window, and the messages appear to be in speech bubbles coming out of the icon.  Have you ever even used iChat?"
    author: "Spy Hunter"
  - subject: "Re: ..."
    date: 2003-10-24
    body: "Trillian have buddy icons"
    author: "Captain"
  - subject: "Re: ..."
    date: 2004-03-19
    body: "I hate buddy icons. Not to mention that no other protocol i know of uses them and since gaim is mostly for aim it decided to basically mimick the aim client for windows which is fine. Kopete is meant to be an integrated IM solution which it is. Open source projects due to their nature can only compliment each other and offer healthy ideas and variety, gaim isn't winning it is simply offering an alternative. Since kopete came about gaim has exploded with new features coincidence? maybe. Just a few things to be thinking about"
    author: "Celery"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "Kopete's OK, but it's certainly not as good as gaim.  I defintiely would prefer to run a KDE app over a gtk one, but not if the gtk app is better.\n\nUnfortunately, one of my favorite things about gaim, Perl scripting, probably won't be in kopete any time soon."
    author: "KDE User"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "GAIM is not only GTK+. The GAIM people are in a process of splitting the GUI from the library. \nThis already works well, there is a Qt/E GUI calle qpe-gaim with a Qt Interface.\nSO the only thing is the glib2 dependency but I think nowadays you can call glib a system library."
    author: "Was auch immer"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "You'll find very little agreement that glib is a system library amongst anyone except the Gnome team (and not even from all of them).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "> SO the only thing is the glib2 dependency but I think nowadays you can call glib a system library.\n\nuhh?"
    author: "anon"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "If you hadn't noticed, glib2 is a dependency for KDE CVS these days too. aRts uses it."
    author: "Rayiner Hashem"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "That's why lots of people are using the ARTS_1_1 branch with HEAD.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: ..."
    date: 2003-09-23
    body: "I find this ridiculous. Such thing was never applied to a kde module. Or should e.g. kdepim also work with KDE_3_1_BRANCH? Perhaps I don't want to upgrade my kdelibs -- no sane developer would let this go through. The last time I looked arts was in the same way part of the KDE release as every other module. So why treat it special? Please note:\n\n1) arts' scheduler always had this glib dependency and had a copy of the source in it. It was just ripped out in HEAD to avoid code duplication (which is a good idea)\n2) glib is most often already available on the system. Take a look at the reverse-dependency-graph of glib on a casual distribution!\n3) I'm sure that every developer/user who can compile KDE (and only those matter here) is also able to compile glib (in fact it's really tiny in comparison to kdelibs and kdebase). It's not that kdelibs didn't have any external dependencies!\n\nHISTORICAL NOTE: Many people might not remember, but in pre-KDE2-days kdelibs had a dependency on MICO (this fat CORBA stuff, for OpenParts before KDE got KParts). That was really a hell of a dependency. It took about 8 hours or so to compile for me then. Such pain should better be avoided. But seeing people complain about glib is laughable. \n\n(If you don't like it go ahead and rewrite the code to not use glib, but so far is it using glib, so KDE has a dependency on it. Live with it!)"
    author: "Oliver Bausinger"
  - subject: "Re: ..."
    date: 2003-09-23
    body: "You've got things backwards. HEAD works with ARTS_1_1 branch, I said nothing about what is happenning in the HEAD branch for arts. The reason it is treated specially is that the arts guys have decided it should be independent of KDE - that's their choice.\n\n1) I'm not sure of the details here, but I'm pretty sure arts existed before glib.\n2) It is not available on my system, and unless they sort out it's ridiculous dependency issues it never will be.\n3) That's not actually true. It has an awful dependency problem and I have better things to do than to try to sort out keeping it happy.\n\nRegarding your historical note - Since I've been heavily involved with KDE since before we'd made any releases at all (or had any apps) I remember this quite well. We ditched that code - there is a lesson here.\n\nRich.\n\n"
    author: "Richard Moore"
  - subject: "Re: ..."
    date: 2003-09-25
    body: "Well, then the problem arises when something in KDE (presumably kdemultimedia) is going to start features of arts HEAD. We'll see what happens.\n\nad 1) The glib code was introduced with the gsl stuff. I can track down arts/flow/gsl/gslglib.h to \"Fri Sep 21 19:59:32 2001 UTC\" according to webcvs. So KDE had glib code since then in it :-)\nad 2)/3) MICO was an awful dependency. glib is harmless like libxml, libxslt, pcre whatever. See, it took me way longer to write this comment than to install glib:\n~/glib-2.2.3$ time sh -c \"./configure --prefix=/tmp/test; make ; make install\"\n[..SNIP..]\nreal    2m21.504s\nuser    1m34.280s\nsys     0m27.690s\n\nHave fun :)"
    author: "Oliver Bausinger"
  - subject: "Re: ..."
    date: 2003-09-25
    body: "> Well, then the problem arises when something in KDE (presumably\n> kdemultimedia) is going to start features of arts HEAD\n\nSince arts is now an optional dependency that will not be a problem.\n\n2,3) I'm glad you've decided this dependency is harmless. I'll be sure to consult with you whenever I decide to add a new dependency to my own code as you are obviously the authority on this.\n\nRegarding glib, what kind of point are you trying to make? Given that you've already got in installed, of course the dependencies are satisfied and configure works. It doesn't here.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: ..."
    date: 2003-09-25
    body: "No, I'm not the authority in any way. Calling it harmless means for me that it's easy and fast to install and doesn't drag a bunch of other problems with it. Just my opinion.\n\nAnd about glib's dependencies: after consulting the INSTALL file, the dependencies of glib are as follows: a iconv() implementation (glibc for me), pkgconfig and gettext. Nothing tragical. I assume you're missing pkgconfig. So what? It's a quite small piece of software.\n(Yes, I know it's annoying to collect stuff together like this, but we're talking here about people who want to compile kde from sources with its not so small amount of required/optional dependencies, these people will have no problem with this small stuff. If I had stopped when some \"configure\" didn't work, I would never have compiled KDE from sources.)\n\nSo what is the point I'm trying to make? Obviously I didn't realize that the meaning of arts for KDE changed for 3.2. \nAs arts is only optional, perhaps it should be clearly marked as such.\n\n(And sorry for sounding like Neil, I really don't want to replace him :-))"
    author: "Oliver Bausinger"
  - subject: "Re: ..."
    date: 2003-09-25
    body: "The problem isn't that glib doesn't have many dependencies; the problem is that it 1). it's a completely foreign API for KDE developers. This is why there aren't many developers hacking on arts. Even plain C would be better in this regard. 2). It introduces bloat, since almost all of glib is duplicated inside of Qt anyways."
    author: "anon"
  - subject: "Re: ..."
    date: 2003-09-25
    body: "Also, since arts is pretty much used only by Qt, there is no real reason to use something like glib instead of Qt. It would make it MUCH easier to maintain. "
    author: "anon"
  - subject: "Re: ..."
    date: 2003-09-25
    body: "You don't want plain C. Nobody wants plain C. :-)\nAfter all, it was the decision of the maintainer to let in the scheduler code based on glib. Whether this was a technically sound decision can be doubted. Blame Stefan Westerfeld for this :-) \nI just find it slightly strange that when the glib-inside-code is ripped out to an external dependency, people suddenly start to cry: No, I don't want glib on my system, this is bloat, this is terrible. Those people had glib code on the harddisks for two years. \n\nAnyway, speaking about technology I welcome that arts is optional."
    author: "Oliver Bausinger"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "The moment glib was added as a dependency to arts, it was made optional. There is (probably) going to be a large call for an effort to replace it with something else for KDE 4.0 (with a compat layer, of course)"
    author: "anon"
  - subject: "Re: ..."
    date: 2003-09-22
    body: "Thank god. aRts has been the weakest link in KDE for awhile now. I vote MAS to be the replacement. Further, it'd be nice of KDE and GNOME could agree on a neutral sound server backend. "
    author: "Rayiner Hashem"
  - subject: "Re: ..."
    date: 2003-09-23
    body: "Don't GNU system utilities start to depend on it today too?"
    author: "Anonymous"
  - subject: "Re: ..."
    date: 2003-09-21
    body: "First the framework, next the rest - we're beating them later. With this most excellent framework an the beginning support of the german,japanese,indian,taiwanese government the song is: \"Time,time,time is on our side - YES IT IS!!! :-)))))))))))))))))))))))))\n"
    author: "Ruediger Knoerig"
  - subject: "Re: ..."
    date: 2003-09-23
    body: "I agree with you.  Interoperability is the key if we want Linux and KDE to be successful.  We sometimes see things about the Apple vendor lock-in, there should not be a KDE lock-in too."
    author: "Dominic"
  - subject: "Sign this petition!!"
    date: 2003-09-22
    body: "http://swpat.ffii.org"
    author: "gdg"
  - subject: "Thanks, KDE developers"
    date: 2003-09-23
    body: "A well-deserved award. From a user standpoint, there's still work to be done. But I find KDE a productive and attractive daily work environment. I've even moved my Mac over to KDE on Yellow Dog Linux.\n\nNice to see GAIM get a nod, too. An Linux app that has exceeded its Windows progenitor.\n\nLooking forward to 3.2. "
    author: "Christopher Baskind"
---
<a href="http://www.ofb.biz/">Open for Business</a> announced the winners of the &quot;<a href="http://www.ofb.biz/modules.php?name=News&file=article&sid=265">Open Choice Awards 2003</a>&quot;. KDE 3.1 won in the &quot;Best Desktop Environment&quot; category and Konqueror 3.1 succeeded in the "Best Web Browser" category. The editors liked KDE for being the most polished desktop environment and best replacement for Windows and MacOS users and honored the time spent for making KDE applications' look and feel unified and integrated into the desktop. Konqueror scored with integration too besides responsiveness and better rendering of pages intended for Internet Explorer.
<!--break-->
