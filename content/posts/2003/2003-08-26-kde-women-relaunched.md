---
title: "KDE-Women Relaunched"
date:    2003-08-26
authors:
  - "foster"
slug:    kde-women-relaunched
comments:
  - subject: "A lot better!"
    date: 2003-08-25
    body: "Whew, this is a lot better than before, and a nice site to visit even fi you'r not a woman. Now where's KDE MEN ;?\n\nI'm happy that KDE's new design is slowly but steadily being adopted by more and more KDE websites, thiugh there still are a lot of old designs, like for the Events page and Kdevelop."
    author: "Alex"
  - subject: "Re: A lot better!"
    date: 2003-08-25
    body: "I liked the drafts on kde-look.org.\nI think the old one is better than the final version."
    author: "CE"
  - subject: "Re: A lot better!"
    date: 2003-08-26
    body: "Keep in mind that kde.org is themeable (see settings in the top)\n\nI really like the \"Blue, Mellow\" color settings. "
    author: "fault"
  - subject: "Re: A lot better!"
    date: 2003-08-26
    body: "This design is nice, but it has some problems.\nThe empty line between \"[...] KOffice 1.3 Beta 3 changelog.\" and \"KDE 3.1.3 Released\" is missing, for example.\n\nFurthermore \u0096 independent to design \u0096 it is to overloaded, too much text and links."
    author: "CE"
  - subject: "Re: A lot better!"
    date: 2003-08-28
    body: "KDevelop.org doesn't have the old but already an own design."
    author: "Anonymous"
  - subject: "Mmm, Don't know..."
    date: 2003-08-26
    body: "...why this is necessary. We don't have KDE men, KDE blondes,\nKDE Europeans, KDE aliens, etc either.\nAll are working on the same project. What has this to do\nwith if you have breasts or not?\nDon't think this is really necessary, but well if they need it...\nI'm off to the gym - I want to join too.\n\n\n"
    author: "Jan"
  - subject: "why not? "
    date: 2003-08-26
    body: "> ...why this is necessary\n\nfor example, it is known that majority of KDE developers are men, \nand i am sure most of them have girlfriends or wifes, and while the men do their job, what should their women do? Who is going to compassionate them ? -- only those of their own kind! (i.e. other women of kde men) :-) \n\n    A programmer, \n          often observing his girlfriend \n          feeling for another programmer's girlfriend.  "
    author: "SHiFT"
  - subject: "Re: Mmm, Don't know..."
    date: 2003-08-26
    body: "Women are a vastly under-represented group in the geek community.  If we truely want to be a community (and achieve World Domination, they do make up 52% of the population you know) then we need to provide support to encourage then into the community, not chase them away with macho bullshit.\n\nBesides, the 3 best programmers I have ever had the privalege to work with were women, they kicked my arse all over the joint.  They brought a different perspective to the table, and just got on with it while we guys were still comparing our measuring tapes :-)\n\nJohn."
    author: "John"
  - subject: "Re: Mmm, Don't know..."
    date: 2003-08-26
    body: "Yes, I think us aliens of the uncharted left arm of the milky way demand attention! :-)\n"
    author: "Slovin"
  - subject: "Re: Mmm, Don't know..."
    date: 2003-08-26
    body: "> with if you have breasts or not?\n\nOf course it doesn't matter, but we need to encourge more women to get involved with contributing to open src software :)\n\nPresently, I would guess more than 95% of all KDE contributors are men. \nSee http://tinyurl.com/l6ar"
    author: "anon"
  - subject: "Re: Mmm, Don't know..."
    date: 2003-08-26
    body: "<I>.why this is necessary. We don't have KDE men, KDE blondes,\n KDE Europeans, KDE aliens, etc either.</I>\n\nIf you like, you can probably start men.kde.org."
    author: "a.c."
  - subject: "Screenshots please"
    date: 2003-08-26
    body: "Screenshots please"
    author: "frizzo"
  - subject: "Re: Screenshots please"
    date: 2003-08-26
    body: "Of what?  Women? :P"
    author: "Not so anonymous..."
  - subject: "Re: Screenshots please"
    date: 2003-08-26
    body: "*LOL* *LOL* *LOL*"
    author: "Marc Tespe"
  - subject: "Re: Screenshots please"
    date: 2003-08-26
    body: "http://women.kde.org/aboutus/who.php"
    author: "Anonymous"
  - subject: "Re: Screenshots please"
    date: 2003-08-28
    body: "Whoa... hot CS women.  *packs bags for Germany*"
    author: "anonymoose"
  - subject: "Not necessary"
    date: 2003-08-26
    body: "I agree that KDE-Women is an unnecessary component. It gives the impression that \"women\" are a special group of people with special needs when it comes to KDE. Well, they're not. We don't need segregation in KDE. Thank you.\n"
    author: "Slovin"
  - subject: "Re: Not necessary"
    date: 2003-08-26
    body: "I agree that *in theory* it is an unecessary component. We shouldn't need to discriminate at all, not even positively. But the fact remains that there are very few women involved in KDE. There are many reasons for this, and none reflect poorly on the way that KDE welcomes contributions from women, but any initiative that can encourage women to actively contribute is worthwhile.\n\nWhy do we want more women participating? Well, KDE reflects the needs of its users, or rather the needs of the users that express their needs on bugzilla. If you don't have any women participating, from using bugzilla to coding DCOP, KDE will become increasingly male orientated and male dominated. That's not to say that women somehow how very different needs or tastes, but that you are excluding a large number of users, because you aren't making an effort to include them.\n\nSo promoting participation to women is a good thing, IMO. If there comes a time when we notice that there are very few elderly people using KDE, then it'd be a good idea to promote KDE to the elderly, concentrating on particular benefits they might find in KDE the product and KDE the community. And so on.\n\nTo me, Free Software is about *providing Freedom* to users, not just satisfying my own wants and building cool technology."
    author: "Tom"
  - subject: "Re: Not necessary"
    date: 2003-08-26
    body: "> It gives the impression that \"women\" are a special group of people with special needs when it comes to KDE.\n\nIt has nothing to do with \"segregration\". What's wrong with encouraging one of the most underepresented minorities in KDE development from contributing? \n\nWomen as percentage of World's Population - 51.72% - (Dec 2001)\nWomen as percentage of KDE's contributor Population (less than 5%, conservative estimate)\n\nIt's important to encourage women to help out in KDE development. If women had not been encouraged in other fields, we would have seen even more male-domination in the world than we have now."
    author: "anon"
  - subject: "Re: Not necessary"
    date: 2003-08-26
    body: ">>It's important to encourage women to help out in KDE development.\n\nLook, if _people_ are interested in KDE, they will help and contribute, there is nothing more or less to it."
    author: "AC"
  - subject: "Re: Not necessary"
    date: 2003-08-27
    body: "That's bullshit. Its simple mathematics to wor out why initiatives like this are needed. Consider the X-axis to be time, and the Y-axis to be a particular gender's involvement with technology. If women start out lower on the Y-axis (and they do, because of various historical and social reasons), and the slope of the two lines is the same (implying equal rate of progress for both genders as a result of neutrality) they will *always* be behind. The only way to get women to catch up is by increasing their rate of progress, and outreach efforts like this can do that.\n\nPS> I used to be in a robotics club in high school. We sometimes did technology events for the kids in the area. We'd line up some programmable Lego robots and watch the kids come in and play with them. I noticed then that the 8-12 year olds that came in were pretty much evenly divided among girls and boys. Meanwhile, our robotics club was all guys, and at my college (an engineering school) a full 72% of the students are guys. There is definately a terrible social phenomenon at work here --- there is no point in going all \"ivory-tower\" and pretending its not there. \n\nPS2> Now the above mathematical metaphor does not hold if gender inequality is a self-correcting phenomenon. That is, in the abscence of external force, it reachs some equilibrium. There is no reason to believe that it is, and lots of reasons to believe that it isn't. Gender inequality is a lot like financial inequality in that the rate of change is proportional to the current level. This can be easily seen in economic statistics, which show that the gap between the rich and poor keeps getting larger, even in a theoretically neutral system like capitalism. The same probably applies to gender equality. "
    author: "Rayiner Hashem"
  - subject: "Re: Not necessary"
    date: 2003-08-27
    body: "Double posts aside, it's worth pointing out that countless studies have confirmed exactly what you observed in your robotics clubs -- there are real social factors at play which are disencouraging girls from science/engineering.\n\nIMHO, seeing the KDE community (well, some of it) actually *doing something* about this is a big part of what makes KDE, as a whole, so very cool."
    author: "azza-bazoo"
  - subject: "Re: Not necessary"
    date: 2003-08-27
    body: "That's bullshit. Its simple mathematics to wor out why initiatives like this are needed. Consider the X-axis to be time, and the Y-axis to be a particular gender's involvement with technology. If women start out lower on the Y-axis (and they do, because of various historical and social reasons), and the slope of the two lines is the same (implying equal rate of progress for both genders as a result of neutrality) they will *always* be behind. The only way to get women to catch up is by increasing their rate of progress, and outreach efforts like this can do that.\n\nPS> I used to be in a robotics club in high school. We sometimes did technology events for the kids in the area. We'd line up some programmable Lego robots and watch the kids come in and play with them. I noticed then that the 8-12 year olds that came in were pretty much evenly divided among girls and boys. Meanwhile, our robotics club was all guys, and at my college (an engineering school) a full 72% of the students are guys. There is definately a terrible social phenomenon at work here --- there is no point in going all \"ivory-tower\" and pretending its not there. \n\nPS2> Now the above mathematical metaphor does not hold if gender inequality is a self-correcting phenomenon. That is, in the abscence of external force, it reachs some equilibrium. There is no reason to believe that it is, and lots of reasons to believe that it isn't. Gender inequality is a lot like financial inequality in that the rate of change is proportional to the current level. This can be easily seen in economic statistics, which show that the gap between the rich and poor keeps getting larger, even in a theoretically neutral system like capitalism. The same probably applies to gender equality. "
    author: "Rayiner Hashem"
  - subject: "Is that KDE-Jewish Women?"
    date: 2003-08-26
    body: "???"
    author: "frizzo"
  - subject: "Re: Is that KDE-Jewish Women?"
    date: 2003-08-26
    body: "Yep, noticed the magen David too... probably an slip, since the outfit of the lizardess is more pagan than haredi, but yet..."
    author: "Anonymous"
  - subject: "Re: Is that KDE-Jewish Women?"
    date: 2003-08-26
    body: "You talking about the thing around her neck?\n\nI think it's supposed to be a KDE gear. :-)"
    author: "AC"
  - subject: "Re: Is that KDE-Jewish Women?"
    date: 2003-08-26
    body: "Wow, that's awesome. Is there anywhere to buy KDE gear necklaces?"
    author: "ANON"
  - subject: "Re: Is that KDE-Jewish Women?"
    date: 2003-08-26
    body: "I don't know. What I'm wondering is... if dragons wear clothing, why is Konqui naked?"
    author: "AC"
  - subject: "Re: Is that KDE-Jewish Women?"
    date: 2003-08-26
    body: "It reminds me of the smurfs. 'de smurfen'\n\nAll these male smurfs and one female smurf, its just as silly as this whole KDE woman thing. If there is such a thing as a nerd factor, this would top the scale."
    author: "AC"
  - subject: "Re: Is that KDE-Jewish Women?"
    date: 2003-08-26
    body: "Buy a star of David and flat the edges a little :)"
    author: "Anonymous"
  - subject: "Hope"
    date: 2003-08-26
    body: "it will be more active than in the past. Yes, KDE Women is a nice project."
    author: "Wurzelgeist"
  - subject: "silly"
    date: 2003-08-26
    body: "All I want to say is: some KDE people are really really silly.\n\nIf there is anything showing immaturity and utter geekmanifest in KDE, its nerds settings up a KDE woman corner. Go ahead, visit KDE woman and share your woman things there.. and when you have something, contact some KDE 'men'.\n\nConsider this a bug report for some of my 'fellow' KDE 'men'."
    author: "AC"
  - subject: "Re: silly"
    date: 2003-08-26
    body: ">>its nerds settings up a KDE woman corner<<\n\nObviously this has been set up by women (who also happen to be nerds, of course :).\n"
    author: "AC"
  - subject: "Necessary?"
    date: 2003-08-26
    body: "Why does every news about KDE women trigger a debate if this is necessary or not? It doesn't matter if it is necessary or not, it is already there. Obviously someone thought it's necessary. "
    author: "Bojan"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "erm, im going to start a Satanic, Racist KDE hate group, we need it."
    author: "AC"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "If you sincerely feel it is necessary, then do it."
    author: "Bojan"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "Primary target: Bojan ;)"
    author: "AC"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "Funny that (almost ?) only men discuss if women should have their area."
    author: "Alain"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "Well... there are not enough women around to have an argument!"
    author: "Roberto Alsina"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "You don't have to discuss whether women should have their area - we already have it! "
    author: "Anke"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "Discussion closed - lol"
    author: "AC"
  - subject: "Re: Necessary?"
    date: 2003-08-26
    body: "Yes, so let's just drop it?\nI'm a man, and if this gets more people involved, great.\nIf it makes some women who help out feel better represented, great.\n\nThere is no down side, so just drop it."
    author: "Joe"
  - subject: "Re: Necessary?"
    date: 2003-08-27
    body: "> Primary target: Bojan ;)\n\nLOL"
    author: "Bojan"
  - subject: "About the Kontact article"
    date: 2003-08-28
    body: "I misses to mention that there are also KNewsticker and KNode modules."
    author: "Anonymous"
  - subject: "Any evidence to support this?"
    date: 2006-09-04
    body: "Does anyone have a link, or other evidence that this seeming divide of the community encourages more women to get involved? Any statistics?\n\nIMO, it seemed that the aims of the FSF and projects like this are not only to develop great software, but also create a community, a united community. Not one where certain members belong to women.kde.org and certain members belong to asians.kde.org, russians.kde.org, pinksocks.kde.org and the rest go to community.kde.org. \n\nI also don't see how this is much different from developer.kde.org. Apart from the sparse uses of the word \"women\" in a few places, theres no male-oriented content in developer.kde.org to make such a difference. Unless of course, women.kde.org exists just to make women developers feel better and \"separated\" from the rest of the \"male macho\" community, in which case its nothing serious and is fine.\n\nAnyway, any evidence would be appreciated, I really am curious. :)\n"
    author: "Amara"
---
<p>The <a href="https://community.kde.org/KDE_Women">KDE-Women project</a> was <a href="http://dot.kde.org/984985009/">founded in 2001</a> as an international forum for women involved with or interested in KDE. It was originally intended to be a place where women could present their current contributions to KDE and where women who wished to contribute could find a starting point. That was the goal of KDE-Women then, and still is now. After a period of dormancy, the project has been relaunched in terms of <a href="http://women.kde.org">a revamped website</a> based on the new KDE design as well as fresh and updated content such as the <a href="http://women.kde.org/articles/">tutorials and howto's</a> and an <a href="http://women.kde.org/articles/articles/kontact.php">article on Kontact</a>.</p><!--break--><p>In the <a href="http://women.kde.org/events/">events section</a> you'll also find a listing of the newest events, such as the KDE Contributor's Conference 2003 currently taking place in Nove Hrady and the upcoming Informatica Feminale.</p><p>The KDE-Women are planning on adding a lot of new features and hope to find more women who want to contribute to KDE, and support the KDE-Women community. So be sure to visit the new website, and if you are a woman, feel free to join the KDE-Women community!</p>