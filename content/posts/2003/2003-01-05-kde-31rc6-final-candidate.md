---
title: "KDE 3.1rc6:  The Final Candidate?"
date:    2003-01-05
authors:
  - "Dre"
slug:    kde-31rc6-final-candidate
comments:
  - subject: "TeX URL"
    date: 2003-01-05
    body: "Wouldn't www.tug.org be a better URL for TeX instead of www.dante.de?"
    author: "em"
  - subject: "Re: TeX URL"
    date: 2003-01-05
    body: "www.ctan.org would be the best I think..."
    author: "Pierre"
  - subject: "possible mission"
    date: 2003-01-05
    body: "so your mission, if you choose to accept it, is to run this release\n\nwill the packages auto destroy after ?"
    author: "jaysaysay"
  - subject: "Re: possible mission"
    date: 2003-01-06
    body: "Yes - and they will take your computer with them (or at least damage it)- if they are too close to your computer.\n"
    author: "SegFault II."
  - subject: "Re: possible mission"
    date: 2003-01-06
    body: "no no no! my keyboard is still wor"
    author: "jaysaysay"
  - subject: "Re: possible mission"
    date: 2003-01-07
    body: "*lol* (hearing a computer die)"
    author: "SegFault II."
  - subject: "(RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "If RedHat8 is installed sans KDE, and Konstruct is used to install the latest and greatest KDE stuff, wouldn't that be the best of both worlds?\nHas anyone tried this?"
    author: "Anonymous"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "But youd still be using redhat with its broken kernel/compiler/glibc/etc/etc/blah/blah so you will still basically have a sub standard linux system. Be a man try debian, it really does kick ass, and with apt-build as an upcoming rival to gentoo's portage you get some great benefits. If that doesnt take your fance, then Mandrake is pretty damn good too."
    author: "Tom"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "I tried getting a windows friend into linux, and people like you are what turned him off. According to him now, linux users are \"all wankers\" ."
    author: "Chris"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "Linux is a user product, not a religion or way of live.\nYour friend should choose an OS that fits his needs, not an OS that has a nice user audience.\n\nRinse\n"
    author: "rinse"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "That's not exactly true, since in most cases, Linux is user-supported.  If the user-community of a distro is hostile to new users, that distro has a very big mark against it in weighing the advantages or disadvantages with going with a specific distro or choosing another.\n\nDebian has a ton of technical merits, but in my experience has a not-insignificant number of users who think that gives them the right to insult everyone else's choices."
    author: "Ranger Rick"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "Introduce your friend to a debian-based distro such as Xandros (if shelling out some $$$s is not entirely an issue). It is as user friendly as linux can get (as of now) and then one can always explore the rock solid debian root.... (no pun intended)"
    author: "Kanwar"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "> Be a man try debian,\n\nI'd rather have a *usable* system. While I was contemplating for a while to \n\"be a man\", I chose to stay a coward when I saw that the only offer for Samba\nwas\n  --a--: 1-year-old version 2.2.3a for so-called \"stable\" (a version which has \n         been urged by the Samba Team themselves long ago to upgrade)\n  --b--: the future version 3.o, currently tagged \"21alpha\" by the Samba Team\n         (not *really* fit for production use) in \"unstable\"\n  --c--: the same in \"testing\"...\n\nI must say, the hi respect Debian *did* have in my mind (before I put a toe\ninto their water), has been heavily decreased now (and I can't say I'm in any\nway faminliarized with it now. \n\nCheers,\nKurt   [still a SuSE-coward]"
    author: "Kurt Pfeifle"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "Just because one debian is an asshole doesn't mean they all are."
    author: "Dr. Freak"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "Hi Kurt!\n\nMaybe Gentoo is something for you as an experienced SuSEaner ;-)\n\nHere are the current versions of cups, samba and kde.  Gentoo is quite nice cause it makes no problems when installing a mix of distro-packages and normal source-tarballs. \n\nbash-2.05b# ls /usr/portage/net-print/cups\nChangeLog              cups-1.1.15-r2.ebuild  cups-1.1.17_pre20021025.ebuild  files\ncups-1.1.14-r4.ebuild  cups-1.1.16.ebuild     cups-1.1.18.ebuild\nbash-2.05b# ls /usr/portage/net-fs/samba/\nChangeLog  files  samba-2.2.5-r1.ebuild  samba-2.2.6-r2.ebuild  samba-2.2.7.ebuild  samba-2.2.7a.ebuild\nls /usr/portage/kde-base/kde/\nChangeLog  kde-2.2.2-r1.ebuild  kde-3.0.3.ebuild   kde-3.0.4.ebuild   kde-3.1.ebuild      kde-3.1_rc6.ebuild\nfiles      kde-3.0.2.ebuild     kde-3.0.3a.ebuild  kde-3.0.5a.ebuild  kde-3.1_rc5.ebuild\n\nHave a lot of fun!\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "Security patches and fixes for grave bugs are usually backported to older versions in debian stable.\n\nSo you get a stable and secure system, and don't have to reconfigure your system every other day as would be the case if you always upgrade to the latest and greatest version of every package.\n\nIf you absolutely need new features of a certain program you usually can build a package from debian unstable or testing inside of debian stable. Just adjust the deb-src lines inside of /etc/apt/sources.list to testing instead of stable. So you get stable binary releases and testing sources. Then all you need to do ist \"apt-get source --compile <packagename>\" to build your *.deb files.\n\nStefan"
    author: "Stefan Heimers"
  - subject: "not true with samba though ...."
    date: 2003-01-06
    body: "Nice try, but the samba team has been recommending migrating to 2.2.6 or later for quite a while, and the security backport patches only addressed the security issue, not the bug fixes (mostly locking and printer support) that have ocurred in samba.\n\nMandrake provides updated samba RPMs (at ftp.samba.org, http://people.mandrakesoft.com/~staburet/samba or http://ranger.dnsalias.com/mandrake/samba) for most current releases (currently 8.0, 8.1, 8.2 and 9.0) with and without ldap support (compile-time option in 2.2.x) plus there are samba3 RPMS that will parallel install with 2.2.x (avialable for 9.0, and in cooker).\n\nIMHO, only SuSE and Mandrake are really useful for production samba servers (maybe Gentoo, which I haven't tried)."
    author: "ranger"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "Don't confuse Red Hat 8 it with the 7.x series, which used the experimental GCC branch 2.96.x. RH8 uses GCC 3.2.x - there's nothing broken about it."
    author: "dealing_death"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "Just a badly compiled glibc"
    author: "Bob"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "Have never had any problems with glibc on redhat.\nWhat is supposed to be wrong with it?"
    author: "Uno Engborg"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "RedHat 8's glibc is a pre-release CVS snapshot. It's quite close to 2.3.0 though, so I don't know that there are any specific problems that aren't in glibc 2.3.0 (there is one that makes some database problems not run properly) -- and RedHat certainly knows a great deal about glibc, as they employ the glibc maintainer, so I guess they probably have some idea of what they're doing with it (unlike with KDE). Also, IIRC, RedHat 7's biggest problem was actually glibc and not gcc - they shipped a pre-release as well, and if my memory serves me right, that release had some binary compatibility problems and had to be corrected with an update. \n"
    author: "Sad Eagle"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "I have been using the RedHat distribution since 4.0. I have informally tried other distributions out of interest but all production servers and desktops since 1997 have all been RedHat, simply because it is what I know best and not becuase of any deficiencies in those other distributions. I have never had even one single problem related to the integrity of Redhat versions of kernels, libraries or compilers including the supposedly infamous 2.96. This version of gcc was released for very valid reasons at the time and I personally believe it to be more standards compliant and certainly no more buggy than any other release as far as I can tell from net traffic over the past few years.\n\nComments like this from Tom merely serve to highlight the unfortunate tendency that some people have to spout off about something that they very obviously know nothing about.\n\nHaving said that I think RedHat have made a mistake with the BlueCurve desktop design and I will be continuing to faultlessly compile the excellent KDE desktop from source and implementing it on RH 8.0. "
    author: "tfar"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "I'll second this, and I've coincidentally been using RH since 4.0, and I've never had *any* problems that I could attribute to the stuff that it usually trotted out.\n\nBut I do agree about bluecurve, and even if not then I'd still be building my own KDE so that it can live in /opt/kdeX, which is clearly were The Great Budgerigar [*] intended it to go.\n\nMike\n\n[*] s/The Great Budgerigar/Deity of Choice/"
    author: "mike ta"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "Totaly agree with you"
    author: "Xavier"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "I absolutely agree with tfar : \"Comments like this from Tom merely serve to highlight the unfortunate tendency that some people have to spout off about something that they very obviously know nothing about.\"\n\nI personally switched from RedHat to Mandrake because I *prefer* Mandrake (I love their user-friendly approach). And I spend less time in configuration and sofware management. That's all. But I consider RH a rock-solid distro. Moreover, they are the strongest company behind a free software distro, this is a plus for them. I only have gratitude to RedHat. And in gneral to the whole free software community."
    author: "NewMandrakeUser"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "hey i'm curious if there is a kde 3.0.5 package compiled for redhat but use the default kde menus? i tried installing the 3.0.5 rpm's but still very ugly menus, i would just like stock kde in rpm. if this exist please reply, but don't waste my or your time saying for me compile it myself (i want it in rpm)."
    author: "anoymous"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "I have never had a hassle with RH, sure version 7.0 was quite buggy, but I found it easy enough to get around most problems. I must say though I do agree with the Bluecurve idea, at least they are changing their approach towards KDE a lot by at least supporting both, thats more than I can say for SuSE. I mean I do like SuSE and I do like Debian, even though it is very messy: /etc files are all over the place, Redhat make their OS a pleasure (as with SuSE and other Standard Linux's), who store their main config files in the /etc/sysconfig/\n\nSo \"Mr I'm better than everybody else Debian User\" before you slag off an operating system take into consideration that without Red Hat a lot of the cool things that come with Linux now (Oracle and other high end stuff), most probably would not have really made it here as quickly. Oh and another point before you say sub standard, you should reconsider because Red Hat, SuSE, SCO, Mandrake are all LSB compliant. Which makes them THE standard, and that is mearly one reason why they all have massive followings compared to the little debian system, and I can bet you right now that RH Advanced Server will kick the shit out of any Debian box."
    author: "Thundabird"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "One can bicker about the relative merits of any Linux distro for hours without really getting anywhere (no need to try; it's been proven many times on IRC).\n\nI have never had a hassle with Debian unstable; sure it was quite buggy around last year when a bunch of core packages went through some incompatible changes (like Perl), and occasionally something breaks now (there's a libc and qmail that just got fixed), but I've found it easy enough to get around most problems.\n\n... surely you see what I mean.\n\nIn response to the comment about /etc files, practically every admin and server package, and a good many of others, install something in /etc by default, and it's the distro's business to clean it up if it is a problem. I've found Debian's setup to be a good balance -- close if not exactly what the upstream distributer does with its configuration where it makes sense, but changes to the distributed init scripts so that you generally don't have to alter /etc/init.d files, grouping multiple files related to one package in a subdirectory, and putting all real config files in /etc to match LSB even when the upstream puts them elsewhere (e.g. qmail).\n\nIn the end, though, it's a matter of personal preference. There are a large number of often-conflicting considerations when laying out a distribution, and different people will weigh them differently.\n\nYou bring up a good point about LSB. While it is not perfect for everyone, it does give a base for 3rd-party software distributors to build upon. While Debian chose not to make its base distribution LSB-compliant, installing three packages (lsb, lsb-release, and lsb-rpm) is sufficient to add LSB compliance, making it a Standard Linux as you call it.\n\nI would advise not placing any real money on the bet you bring up in the last sentence, especially where any hardcore Debian user is involved. For example, I find (after rejecting ~5 cookies from the redhat.com site in the process) the top listed Highlight of Red Hat Advanced Server to be an \"extended release cycle for long-term stability.\" If I am not mistaken in my interpretation, it means that the same typical arguments against Debian's stable distribution -- that software is too old -- can apply to RH Advanced Server. As for the other highlights, clearly the corporate backing allows them to get certification from other companies, but it comes at a price, literally: subscription services. I'm not claiming this is a bad thing; it is just necessary to see the whole picture before making a judgement. The rest of the Highlights are available in one form or another in the broader Linux community, and able to be packaged and distributed by any distribution.\n\nIn summary, I give Red Hat the credit it is due for helping to push Linux into the mainstream. (Other companies, such as IBM, Dell, Oracle, VMWare, and many others, also deserve credit in this regard.) However, I caution against taking any one radical user's opinion from any side before making a choice. After all, Linux is all about choice; isn't that what we say to the \"GNOME Rules!\" crowd?\n\nBy the way, KDE 3.x on Debian is easy enough, though not as easy as it would be if they included it in unstable (*hint, hint*); a line like this in /etc/apt/sources.list will work for KDE 3.0.5a on a 'sid' system:\n\ndeb http://download.us.kde.org/pub/kde/stable/latest/Debian/sid ./\n\nKen\n\nP.S. - I apologize for the length. I have been writing a lot of essays lately, and the habit has carried over to my everyday writing."
    author: "KenArnold"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-30
    body: "This is probably the most intelligent argument I have probably ever read in a forum. You are right about one thing, I would never argue with a true and hardcore debian user. I would also never trash an Operating System before I try it. I like debian infact, even though my workstation at the office is RH we are going to be rolling out Debian for this new software we are writing. (Quite franckly because an installation is tiny and Debian is the most secure linux distro I have tried).\n\nLook I will say this: Debian and RedHat put out a great name for Linux and I would like to say I do give credit where it is deserved and I think if it wasn't for any of the linux distributers we would end up like FreeBSD??????? With hardly any application support etc, etc, etc. \n\nBut the point I was trying to make even though I let my love of flaming take over was that Linux is about pure preference, thats what makes it linux and a great OS! It completely destroys the object of being forced and thats what I love about it. I also love the fact that we can sit here and slag off every other distro out there except for our own. But the difference with that is, and if I do so its like supporting the home team. As they say how many Geeks do you need to choose a linux distro: 1 because they will fight for years and never come to a neutral ground or a compromise.\n\nand to justify everything: A year ago I started a project on asynchrony called Messiah. It was supposed to eventually become a linux distro that destroyed the boundries between Distros like Debian and RedHat because it was a direct merger of both. In other words if you were to take a debian package and dump it on it will work perfectly on the box, likewise with RPMS from any distro, and it I was even going to build special developer tools to make packages for each of the distros independantly. It only ever got to be a small linux build that could boot in under 30 seconds. I plan to rebuild this project soon.\n\nThunderbird\n\nP.S. I too appologize for the length but when you code as much as me typing comes natuarally!!!!!!!!!! "
    author: "Thunderbird"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-09
    body: "RH8 uses gcc 3.2. Why does it seem to be broken?\nRH8 uses stabilized glibc 2.3 development snapshot. Why should it be considered substandard?\nRH8 uses 2.4.18 kernel with added scheduler optimizations and other patches that add functionality. Why is this broken?\nRH8 uses your everyday etc directory ;-)\n\nRedhat8 is a well performing system, I use it every day for cross-platform development work. You don't seem to have a clue what you're talking about.\n\nCheers, Kuba"
    author: "Kuba"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-05
    body: "Indeed, so. I've compiled my own KDE in RH8. Went without problems, and I now have a proper KDE in an otherwise very nice and stable distro."
    author: "Haakon Nilsen"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "Um...you know that you can change the QT style and KDE icons to KDE default in the Control Center, so there's no need to rebuild the whole of KDE on Red Hat 8 just to avoid the Blue Curve look..."
    author: "moz"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-06
    body: "There is a need to do that to avoid some serious packaging bugs, though."
    author: "Sad Eagle"
  - subject: "Re: (RedHat8-KDE)+Konstruct=???"
    date: 2003-01-07
    body: "Maybe so, but I wanted to try out 3.1 in the same go."
    author: "Haakon Nilsen"
  - subject: "Slightly OT..."
    date: 2003-01-05
    body: "Is anyone with an i810 chipset getting artefacts when resizing windows with the Keramik decorations/widgets?"
    author: "Somebody"
  - subject: "Re: Slightly OT..."
    date: 2003-01-05
    body: "Hi!\n\nNot directly, but using i815 onboard graphics and its XFree86 driver (currently the latest Debian XFree86-release, \"4.2.1.1\") I get vertical colourful stripes in Konquerors toolbars and in titlebars of inactive windows, both with the Highcolor-Style/ModSystem window decoration of KDE 3.0.x and with Keramik/Keramik of KDE 3.1rc5. (The problem appeared at least with all versions of XFree 4.2.x IIRC, possibliy with XFree 4.1 as well, although there once was a time where I had no graphic problems.)\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: Slightly OT..."
    date: 2003-01-05
    body: "I had the same problem with my i810 card, and was able to fix it by putting:\n\n        Option          \"XaaNoOffscreenPixmaps\"\n\n...in the \"Device\" section of my /etc/X11/XF86Config-4\n"
    author: "Ranger Rick"
  - subject: "Re: Slightly OT..."
    date: 2003-01-06
    body: "Thanks a lot! That fixed the stripes in the tool bars. The stripes in inacxtive title bars, however, still persist. Maybe I'll have to play a bit with other acceleration settings or wait for XFree 4.3 which should be released in less than a month as I've heard.\n\nGreetings,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "How do I make my local cvs copy sync with release?"
    date: 2003-01-05
    body: "See the title :)"
    author: "Adam Treat"
  - subject: "Re: How do I make my local cvs copy sync with release?"
    date: 2003-01-05
    body: "cvs upd -rKDE_3_1_BRANCH kdelibs kdebase\n\nor you can use the KDE_3_1_0_RELEASE tag instead.  read the infopage on CVS for details.\n"
    author: "anon"
  - subject: "Re: How do I make my local cvs copy sync with release?"
    date: 2003-01-05
    body: "And don't forget that it's for arts \"cvs up -r ARTS_1_1_BRANCH\". I read that the KDE_3_1_0_RELEASE tag has not been moved for all modules (e.g. kde-i18n) to reflect this release candidate."
    author: "Anonymous"
  - subject: "Konstruct is Cool"
    date: 2003-01-05
    body: "I wish this tool had been put together a long time ago.  It will help the beta process alot as it makes it much easier to download and compile KDE.  Job well done."
    author: "me"
  - subject: "where is apps/* ?"
    date: 2003-01-05
    body: "I just pulled down the CVS sources for Konstruct and there is no apps/koffice as mentioned in the README.  There is a meta/everything however which started to pull down a koffice tarball before I killed it.  (I had expected it to have a dependency on meta/kde, but apparently not, so I'm building \"install\" in meta/kde first.)"
    author: "B. K. Oxley (binkley)"
  - subject: "In the konstruct top-level directory"
    date: 2003-01-05
    body: "How should it otherwise know where to pull the koffice tarball if apps/koffice would be missing?"
    author: "Anonymous"
  - subject: "GUI for Konstruct"
    date: 2003-01-05
    body: "Is there anybody who thought about creating a GUI for the same job Konstruct does?"
    author: "Andreas Pietzowski"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-05
    body: "There's no need for that. See: \nAnybody who is compiling KDE from source needs to be able to handle the console. There is nothing difficult in reading the documentation, go to meta/whatever and type \"make install\". Why on earth should anybody need a GUI for that? Remember: The right tool for the right job."
    author: "Anonymous"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-05
    body: "I tried konstruct but it stoped with this error :\n\ng++ -c -pipe -fno-exceptions -Wall -W -O2 -pipe -D_REENTRANT -fPIC -DQT_SHARED -                                                              DQT_NO_DEBUG -DQT_THREAD_SUPPORT -DQT_THREAD_SUPPORT -DQT_NO_CUPS -D_LARGEFILE_S                                                              OURCE -D_FILE_OFFSET_BITS=64 -DQT_NO_XFTFREETYPE -DQT_NO_IMAGEIO_MNG -DQT_NO_IMA                                                              GEIO_JPEG -DQT_BUILTIN_GIF_READER=1 -DQT_NO_STYLE_MAC -DQT_NO_STYLE_AQUA -DQT_NO                                                              _STYLE_INTERLACE -DQT_NO_STYLE_WINDOWSXP -DQT_NO_STYLE_COMPACT -I/home/alex/Desk                                                              top/Downloads/Linux/konstruct/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/                                                              mkspecs/linux-g++ -I. -I../include -I/usr/X11R6/include -I.moc/release-shared-mt                                                              / -o .obj/release-shared-mt/qpngio.o kernel/qpngio.cpp\nIn file included from kernel/qpngio.cpp:45:\n/usr/include/png.h:324:18: zlib.h: Datei oder Verzeichnis nicht gefunden\nIn file included from /usr/include/png.h:327,\n                 from kernel/qpngio.cpp:45:\n/usr/include/pngconf.h:1103: syntax error before `*' token\n/usr/include/pngconf.h:1104: syntax error before `*' token\n/usr/include/pngconf.h:1105: syntax error before `*' token\nIn file included from kernel/qpngio.cpp:45:\n/usr/include/png.h:1040: 'z_stream' is used as a type, but is not defined as a\n   type.\nkernel/qpngio.cpp: In function `void qt_zlib_compression_hack()':\nkernel/qpngio.cpp:1216: `compress' undeclared (first use this function)\nkernel/qpngio.cpp:1216: (Each undeclared identifier is reported only once for\n   each function it appears in.)\nkernel/qpngio.cpp:1217: `uncompress' undeclared (first use this function)\nmake[6]: *** [.obj/release-shared-mt/qpngio.o] Fehler 1\nmake[6]: Verlassen des Verzeichnisses \u00bb/home/alex/Desktop/Downloads/Linux/konstruct/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1/src\u00ab\nmake[5]: *** [sub-src] Fehler 2\nmake[5]: Verlassen des Verzeichnisses \u00bb/home/alex/Desktop/Downloads/Linux/konstruct/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.1\u00ab\nmake[4]: *** [build-work/qt-x11-free-3.1.1/Makefile] Fehler 2\nmake[4]: Verlassen des Verzeichnisses \u00bb/home/alex/Desktop/Downloads/Linux/konstruct/konstruct/libs/qt-x11-free\u00ab\nmake[3]: *** [dep-../../libs/qt-x11-free] Fehler 2\nmake[3]: Verlassen des Verzeichnisses \u00bb/home/alex/Desktop/Downloads/Linux/konstruct/konstruct/libs/arts\u00ab\nmake[2]: *** [dep-../../libs/arts] Fehler 2\nmake[2]: Verlassen des Verzeichnisses \u00bb/home/alex/Desktop/Downloads/Linux/konstruct/konstruct/kde/kdelibs\u00ab\nmake[1]: *** [dep-../../kde/kdelibs] Fehler 2\nmake[1]: Verlassen des Verzeichnisses \u00bb/home/alex/Desktop/Downloads/Linux/konstruct/konstruct/kde/kdebase\u00ab\nmake: *** [dep-../../kde/kdebase] Fehler 2\nalex@linux:~/Desktop/Downloads/Linux/konstruct/konstruct/meta/kde>\n\n\nWhats wrong ? Something with png maybe ?\n\nThanks\n"
    author: "Alexander"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-05
    body: "zlib.h: Datei oder Verzeichnis nicht gefunden\n\nInstall zlib Development package\n"
    author: "Anonymous"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-05
    body: "A parada he maluka mano. Vacilaun legal, se liga ai !\n\nFalta o lance do zlib Development\n\nInstala essa parada na fita que o lance rola legal."
    author: "Mano Brodi"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-06
    body: "Ou n\u00e3o."
    author: "Fil\u00f3sofo."
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-05
    body: "\n...\n> /usr/include/png.h:324:18: zlib.h: Datei oder Verzeichnis nicht gefunden\n...\nLook like your missing the zlib headers. Install the zlib-devel package and you should be going. ( Or check your include paths if already installed )\n"
    author: "Andreas"
  - subject: "Missing zlib (was Re: GUI for Konstruct)"
    date: 2003-01-05
    body: "Install zlib."
    author: "lemonite"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-12
    body: "But the README needs to be patched:\n\nI doesn't tell you how to start.  I looked all over for a configure file... there were several Makefile-s, but without reading each one, one doesn't know where to start.\n\nSo, Proprosed addition to the README (To be added right before AFTER INSTALLATION):\n\nGETTING STARTED\n===============\nBe sure that you have a live internet connection.\ncd to the ...konstruct/meta directory.\nThen issue the \"make install\" command.\n\n"
    author: "Charles Hixson"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-13
    body: "I second this propocal. I worked it out eventually, but it could be a lot clearer!\n\nEd"
    author: "Ed Moyse"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-09
    body: "Hi,\n\nI have always the same problem with konstruct after:\ncd kde/kdebase\nmake install\n\n--06:12:41--  http://download.kde.org/unstable/kde-3.1-rc6/src/do-not-fail-fast-malloc.diff\n           => `download/do-not-fail-fast-malloc.diff'\nR\u00e9solution de download.kde.org... compl\u00e9t\u00e9.\nConnexion vers download.kde.org[131.246.103.200]:80...connect\u00e9.\nrequ\u00eate HTTP transmise, en attente de la r\u00e9ponse...302 Found\nLocation: http://ftp.du.se/pub/mirrors/kde/unstable/kde-3.1-rc6/src/do-not-fail-fast-malloc.diff [suivant]\n--06:12:41--  http://ftp.du.se/pub/mirrors/kde/unstable/kde-3.1-rc6/src/do-not-fail-fast-malloc.diff\n           => `download/do-not-fail-fast-malloc.diff'\nR\u00e9solution de ftp.du.se... compl\u00e9t\u00e9.\nConnexion vers ftp.du.se[130.243.32.22]:80...connect\u00e9.\nrequ\u00eate HTTP transmise, en attente de la r\u00e9ponse...404 Not Found\n06:12:42 ERREUR 404: Not Found.\n\nmake[2]: *** [http//download.kde.org/unstable/kde-3.1-rc6/src/do-not-fail-fast-malloc.diff] Erreur 1\nmake[2]: Quitte le r\u00e9pertoire `/mnt/win_e/konstruct/kde/kdelibs'\n*** GAR GAR GAR!  Failed to download download/do-not-fail-fast-malloc.diff!  GAR GAR GAR! ***\nmake[1]: *** [download/do-not-fail-fast-malloc.diff] Erreur 1\nmake[1]: Quitte le r\u00e9pertoire `/mnt/win_e/konstruct/kde/kdelibs'\nmake: *** [dep-../../kde/kdelibs] Erreur 2"
    author: "Vincent"
  - subject: "Re: GUI for Konstruct"
    date: 2003-01-09
    body: "You have an incomplete copy of Konstruct or broke it yourself like this guy: http://dot.kde.org/1041714974/1041937909/"
    author: "Anonymous"
  - subject: "Yes!  Let's implement gKconstruct! (nt)"
    date: 2003-01-09
    body: "-"
    author: "Vu"
  - subject: "what nugs in rc5?"
    date: 2003-01-05
    body: "i didn't follow the bugs in rc5 ... too disappointed in the multiple final RCs... what were the showstoppers to release another candidate?"
    author: "standsolid"
  - subject: "Re: what nugs in rc5?"
    date: 2003-01-05
    body: "It was the securities fixes after the source code audit of RC5\n\nI guess, if everything is ok, next week RC6 will become final."
    author: "JC"
  - subject: "Re: what nugs in rc5?"
    date: 2003-01-05
    body: "Great, yeahhh, if you could't found any bugs that means kde 3.1 will be great, wonder, super. \n\nYou must feel the beaty side of our lifes, get out the darkness of our soul, you aren't installing a MS stuff.\n\nThink about !!\n\n8+D\n"
    author: "Jonnathan"
  - subject: "HATS OFF to the KDE Dever's"
    date: 2003-01-05
    body: "I have to say personally I am very surprised at how quickly they have managed to undertake and complete such a massive task. It seems some people have forgotten that the team identified a security flaw throughout the kde source tree, they have also aggresively tackled all showstopper and grave bugs, and have bought many new features to the environment we love, and finally that they are largely a volunteer team.\n\nAll things considered I am downright 'bloody' impressed. This is the sort of professionalism and committment that you simply do not get anywhere else.\n\nKDE Rules and is Second to NONE !!\n\nOpen Source will change and indeed is changing the World !!\n\nA huge unabashed thank you and mega kudos to all members of the KDE team.\n\nPaul\n\nP.S To those KDE developers who are sponsored or salaried, I do not distinguish in my respect for your efforts, skills etc.."
    author: "Paul"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-06
    body: "KDE 3.0 was released on april 3rd last year, KDE 3.1 should have been released middle november, so they had 7.5 months planned for this version. Now the final 3.1 will only arrive middle january: 2 months behind schedule, or more than 25% late on the original schedule.... who's fast here?\nIf m$ releases a new windows version so far behind schedule, we all laugh at them...\nSo.... shouldn't we make fun of the KDE dever's now??? Or should we allow such mistakes in open source development? Can it be late because it is free???"
    author: "Lightning"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-06
    body: "> 2 months behind schedule, or more than 25% late on the original schedule.\n\nIsn't that the usual delay for most big projects? Better be in this segment than in the xx% who fails.\n\n> who's fast here?\n\nMicrosoft releases one or more versions of their Windows GUI every year?\n\n>  If m$ releases a new windows version so far behind schedule, we all laugh at them.\n\nm$ keeps it schedule and if it's for the prize of bugginess. Is that better and what you want?"
    author: "Anonymous"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-06
    body: "> Isn't that the usual delay for most big projects? Better be in this segment than in >the xx% who fails.\nSo hurray, if everyone else makes mistakes, we should do it too?\n\n> m$ keeps it schedule and if it's for the prize of bugginess. Is that better and what >you want?\nKDE has no bugs??? Please take a look at bugs.kde.org...\nIf we should wait till all these are solved, we can only hope we live long enough to see KDE 3.1 in the next century.\n\nI'm not saying that KDE is bad, it is a common problem of all software, but 'hats off' is a little to euphoric in my opinion. There are still bugs, things that other programs or environments do better. So KDE is good, but not superior to others, and 2 months overdue isn't that fantastic."
    author: "Lightning"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-06
    body: "Man,\n\nLet yourself be happy just a little.  When you were a kid the icecream cone was always too small wasn't it?\n\nCore"
    author: "CoreWarrior"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-06
    body: "The reason for such a big delay is because of the holiday season.  They decided to wait instead of rushing it out the door.  They could have easily released this sooner but they decided to wait because most people would be enjoying the holidays.  They also wanted to make sure binaries were made and tested before this release."
    author: "Bridged13B"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-06
    body: "You obviously forgot how longer than expected it took to get windows 95, windows 2000 and windows xp out of the door. XP was not too late as a windows release, but it is also \"the release that will merge nt and 9x lines with an nt kernel\", which was supposed to be out back in 99!"
    author: "nusuth"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-07
    body: "Windows is of course well a lot more then a GUI and some apps...\nI don't say it's good that windows is so late, open source just isn't better.\nAnd concerning kernel changes... the merging of 9x and nt to the xp kernel was a giant operation, which resulted in a superior but expensive OS. At least, MS tries to change there kernel, while linux still isn't able to use plug and play devices as they should be used... hardware support still is very meager and some very ordinary configurations still don't work.\nRecently I tried a cdrom drive of LG, but it did not work with RH8.0 and MDK9.0, although... who would guess it... windows 98 could use it without any troubles. My zip drive mysteriously stopped working under linux  when I put it in another computer (it takes 3 minutes to get past it during booting!!!), and win98 uses it without any extra drivers or so and withou any troubles...\n\nAs for KDE, konqueror isn't as good as IE and why can't kmail use background pics in mails, while outlook express can???\nWhere is this superiority???\n\nAnd why, if all this is so good and superior to windows, keeps everyone paying for windows while there is this fantastic free alternative?"
    author: "Lightning"
  - subject: "Incorrect statements"
    date: 2003-01-07
    body: "Linux does support many devices, has almost perfect plug and play, does now finally offer excellent hotplugging support (My digicam works out of the SuSE-8.1 box).\n\nLinux does in fact support much more architectures and strange stuff than windowsXP. In fact I deleted my MS windows, when after an upgrade from win95 to win98 my old sound card wouldn't work anymore.\nOf course some hardware works better in WindowsXP for a while, but once you have open source linux drivers, they are usually very good.\n\nConcerning Konqueror and IE: I prefer Konqueror. It is a lot safer, reasonably fast and a lot cheaper and more free as well.\n\nYour argument about \"background pics\" shows that you are not very well informed. khtml does not yet support mhtml (it is in CVS though) and loading background pictures from a server is very dangerous for a mail agent. You can turn it on though, kmail will still be safer than OE.\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Incorrect statements"
    date: 2003-01-07
    body: ">Linux does support many devices, has almost perfect plug and play, does now \n>finally offer excellent hotplugging support (My digicam works out of the SuSE-\n>8.1 box).\n\nI can only say something for RH8.0 and MDK9.0, both very recent, and plug and play support just isn't what it should be: winXP autodetects new hardware and autoinstalls them without any further questions. And hardware support: I know quite a few people using linux, and we encountered troubles with LG cdrom drive, iomega zip drive, ethernet card with RLT8139 chipset in laptop, HP scanner, ... Need to say more? All working perfectly under win9x or winXP.\n\n>Concerning Konqueror and IE: I prefer Konqueror. It is a lot safer, reasonably \n>fast and a lot cheaper and more free as well.\nCheaper??? Last time I checked, IE was free as well...\nAnd safer? I should answer yes, but that's not thanks to konquerors safety, but thanks to its impopularity. It just isn't fun for hackers to write hacks for products only 1 or 2% of the users use. When - or better if - konqueror becomes more popular, security exploits will be found there as well. I don't state this out of the blue, but after reading several articles about security issues for my studies.\n\n>Your argument about \"background pics\" shows that you are not very well >informed. \n\nNo, it is because it does not work: kmail places the attached picture below, while it should be shown as background. All options are configured so that it should work."
    author: "Lightning"
  - subject: "Re: Incorrect statements"
    date: 2003-01-09
    body: "Why, then, do you come here to a kde forum regarding a release of kde, and bitch about Linux and it's inferiority to Windows?  KDE doesn't assert that it is the best out there; though some users *do* tend to cheerlead excessively (hence, I recognize your initial perturbation).  However, this is the appropriate forum to do so, and it is not the appropriate forum for OS wars.  If you are content with Windows, and indeed so confident in its long term success, then please, go to a Windows forum, and blow your load there, and don't come back here criticizing us for doing the same.\n\nP.S. I have several ethernet cards running off of the RTL8139 chipset; all work perfectly and get autodetected fine.  You are gravely mistaken if you think that Linux hardware support is worse than Windows--I could give you horror stories but I'm fortunately informed enough to discard them as weak foundation for such arguments.\n\nP.P.S. At any rate, if you need your hand held so much when you use a P.C. check out OS X.  They've put together the finest desktop experience ever created, hands down.  Hats off to the KDE folks; we should be striving for OS X quality, not XP."
    author: "GreyWolf3000"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-07
    body: "Since you are so bold with your kind of statements I suggest you to take a look at http://www.pivx.com/larholm/unpatched/ where you can see a list of 19 known but still not patched vulnerabilities in Microsoft's core product Internet Explorer, the oldest one of them dating one year back. Now please also consider that Microsoft, often being called the biggest software company in the world, should have the man power and money to solve those problems very quickly, but not only are they not doing this, they also won't officially delay their products due to them.\n\nInternet Explorer is not the \"best\" browser: http://www.xs4all.nl/~ppk/css2tests/index.html\n\nHTML shouldn't be used in emails:\nhttp://www.american.edu/cas/econ/htmlmail.htm\n\nThe actual reason why everyone keeps paying for Windows while there are better alternatives:\nhttp://www.kuro5hin.org/story/2001/10/23/13219/110\n\nHave a nice day."
    author: "Datschge"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-08
    body: "One can never know all bugs before a release, and who says there are no security bugs in unix systems? A security report of CERN on last year stated that all major security bugs in server applications were found on unix servers. NO major security bugs were found on windows servers. Unfortunately, one never mentions this...\n\n> Internet Explorer is not the \"best\" browser: >http://www.xs4all.nl/~ppk/css2tests/index.html\n\nTalking about bold statements.... this is only one test, and only about css2, which is only a tiny part of the whole browser...\n\n> HTML shouldn't be used in emails:\n> http://www.american.edu/cas/econ/htmlmail.htm\n\nThey state cases where it can be used, so no reason here not to support it well\n\n>The actual reason why everyone keeps paying for Windows while there are better >alternatives:\n> http://www.kuro5hin.org/story/2001/10/23/13219/110\n \nRidiculous, I know lots of computer stores here in Belgium, where you can choose to buy your computer without windows, and where you can choose for a linux OS.\n The real reason remains that linux stays an OS for people with computer knowledge, because it still is to difficult for the ordinary user..."
    author: "Lightning"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-07
    body: "Windows Rulz !!! Windows is great windows meets the deadline !!!! Even after all these good things about windows and microsoft that you know ... you still come to http://dot.kde.org to take so much pain in pondering about the bad stuff about KDE and it seems you are using KDE in your home or office too (KDE + LINUX + GNU Tools) because you know that Konqueror is nothing as compared to IE but you still use it ... Zip drive dosent work in Linux but you still try to make it work... Your CD ROM Did'nt worked in RH 8.0 then you tried Mandrake 9.0 but it works in Windows without any extra efforts.... can you tell me why do you use linux... and invest huge amount of time in this useless crap OS that you think it is...\n\nOne more thing you wont find many linux users searching mailing lists of M$ world ... and quoting .. it's just that you want to learn something that you think and you know it far superior and better but you are not able to accept... \n\nIf you are a IT pro... then maybe this goes for you too...\n\nThou shall never believe that by clapping hands and chanting \"La! La! La! Free Software is the best!\" long and loudly enough, it'll come true. Choose free over non-free only when it is better or when thou art willing to fix what is broken.\n\n--- soft panorama.org"
    author: "Anand Singh Bisen"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-08
    body: "I have to use it for my studies.... I'm studying informatics, and we have to program on unix and linux systems.\nAnd I'm playing devil's advacate here.... Of course linux and kde have some great features, and are sometimes better than windows, but I just think that looking down on windows isn't going to promote open source. Programmers should realize that user-friendliness is a major issue, and not a tiny thing for when there is time left...\nWhen you look at windows XP, without any prejudice, you just see a great and user-friendly system. Very expensive, but usable for the ordinary user.\n\n> Thou shall never believe that by clapping hands and chanting \"La! La! La! Free\n> Software is the best!\" long and loudly enough, it'll come true. Choose free over\n> non-free only when it is better or when thou art willing to fix what is broken.\n\nThis just confirms my statement... The ordinary user just can't fix a thing, so they have to buy a system they can work with....\n \n"
    author: "Lightning"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-09
    body: "As I mentioned in another post (I apologize for the flame; when you read these messageboards more often, you get sick of the Windows fanboys, and jump the gun), OS X is a far better model for gui intuitiveness.\n<p>Case in point: XP home doesn't let you tell the computer what your IP/gateway/dns addresses are, and instead forces a cumbersome, annoying, patronizing \"guide\" that asks you to format all you networked computers and install XP on them.  The technology to \"do it yourself\" exists in Home edition, but nowhere does it say it has been locked away, only to be touched by a really annoying \"wizard.\"\n<p>It's getting harder and harder to manage Windows if you know what you're doing and want to avoid senseless amounts of \"help\" aimed at grandma; OS X is a UI that grandma can use much more easily than Windows.  Experts also feel right at home."
    author: "GreyWolf3000"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-09
    body: "Your flame provided me just with a good laugh, as my sources were creditable and I really don't need my hand held, as I study informatics for 3 years now.  If you like to get sick, I really don't care.\nIt's just plain silly to deny the good things about windows: it is a system which is quite user-friendly and mostly works fine. There are bugs and bad things, but where aren't? There are quite enough in every linux system too.\nI have little experience with winXP, but what I saw of it was all really good. Perhaps the look isn't every thing, but if you see the themes for kde and gnome, all those open source people who are looking down on windows are trying to copy there themes, quite funny if you ask me.\nYour point about XP and network is - again - stupid: a guide is just what most people need for such things, not something like mdk9.0 offers now: a bad standard configuration or an export option with very little help. \nGuides and help can often be ignored or also used by experienced users, but when it isn't there and one needs it, what then?\n\nI can't comment on OS X as I have never seen it, only that you are stuck with macintosh then...\n"
    author: "Lightning"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-10
    body: "> Your flame provided me just with a good laugh, as my sources were creditable and I really don't need my hand held, as I study informatics for 3 years now. If you like to get sick, I really don't care.\n\nI never said that you were a 'n00b,' merely the equivalent of, say, a Communist entering a Republican or Libertarian rally and preaching his values.  Not the time or place, don't you agree.  I am not a Linux fanboy, and do apologize for \"jumping the gun\" with you, so to speak.  You strike me as being fairly knowledgeable on the subject matter, but I believe that is irrelevant.  Believe me, I am quite immune to the \"harshness\" of online conversation (in reality, I don't really care either beyond a minor annoyance).\n\n> It's just plain silly to deny the good things about windows: it is a system which is quite user-friendly and mostly works fine. There are bugs and bad things, but where aren't? There are quite enough in every linux system too.\n\nYou'd really have to have experienced OS X to know what I'm talking about here, but Apple has outdone Windows in every aspect of GUI friendliness and ease.  It doesn't just \"mostly\" work fine, it always works fine.  I believe that the problem of your comment is *not* failing to address Linux's superiority on the desktop, but for finding an inferior role model for success.  Trying to acheive Windows would be like emulating the steps that an Olympic runner who made second or third place took, as opposed to the first.\n\nHence, you're ignorance of OS X has made your argument invalid; because you are trying to show the KDE team that their efforts should be put to improving KDE to the standards of Windows in term of usability, you are clearly blinded to the fact that Windows isn't King of the Hill in terms of UI design.\n\n> I have little experience with winXP, but what I saw of it was all really good. Perhaps the look isn't every thing, but if you see the themes for kde and gnome, all those open source people who are looking down on windows are trying to copy there themes, quite funny if you ask me.\n\nYes, there are XP themes, and there are Aqua themes as well.  The open source movement tends to emulate more than it creates; that is an admitted shortcoming.  However, I believe that the simple flexibility of KDE to not only look the part, but to act the part of various user interfaces is testament to it's power.  Suffice it to say that *if* KDE had the money to outsource talented professionals, the results *would* be quite impressive.\n\nBut a GUI goes beyond that.  However, being that Windows has emulated Mac's UI design guidelines, I don't feel like this is a point I need to attack, since not only did you not make it, but you couldn't have if you tried.\n\n> Your point about XP and network is - again - stupid: a guide is just what most people need for such things, not something like mdk9.0 offers now: a bad standard configuration or an export option with very little help.\nGuides and help can often be ignored or also used by experienced users, but when it isn't there and one needs it, what then?\n\nYou cannot blame KDE for this problem.  KDE doesn't exist to set up networking.  My point was to show that Windows has made a huge design error by pigeon holing and narrowly defining basic tasks and thereby render a user who needs to do something outside that definition helpless.  And what I was trying to do was not something that only experienced pros would do!  Again, the difficulty of using Linux is not the result of KDE project--the KDE project is not designed to set up your networking; hence, it should not even be compared to Windows as you have done so.  This clearly shows how you have turned a project release candidate into an out-of-place, hackneyed OS debate which has not been helpful to either of us.\n\nBy the way, Mandrake and the other distributions are making huge progress in terms of ease of use;  and they're more than happy to give you support providing you pay for your product.  Your point, friend, is the one that is - stupid -.\n\n> I can't comment on OS X as I have never seen it, only that you are stuck with macintosh then...\n\n\"Stuck\" is a negative word that doesn't apply to having a superior product.  If something is better, you'll want it and you'll stick with it, but you won't be \"stuck\" with it.\n\nMost users are in fact \"stuck\" with Windows, but I don't want you to get sick :)"
    author: "GreyWolf3000"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-09
    body: ">I don't say it's good that windows is so late, open source just isn't better.\n\nNo, you didn't say that. Instead, you said we all make fun of MS when windows slips. My point was, we don't. They usually slip at least as much.\n\n\nWhen they slip and hype and deliver windows 95 in the end, we do. If they are late and bring XP or 2k to the table, noone cares. I'm sure kde3.1 will be a fine release. I don't care if they are late, I want kde 3.1 to work as advertised and I'm sure it will. I'd like them not to abuse \"rc\" tag but that is my only gripe. \n\nI find your story about non-working cdrom hard to swallow. Can you supply more details? Only once I have seen such a thing and it was a buggy bios that wouldn't detect a masterless slave IDE device. \n\nYour rant about missing SUPERIOR free alternative is misleading. Everybody has a different taste for what superior is. I don't care about background pics. I do think konqueror is better than IE. OTOH sub-par font rendering of X+Xft2+KDE seriously bugs me, just like braindead command prompt of NT does.\n\nBTW noone merged NT and 9x kernels; XP is running on a dot-modified win2k kernel (w2k=5.0, wxp=5.1), it was the product lines that had to be merged. They could have done that with windows 2000, had it been a little less memory hungry for its time.\n"
    author: "nusuth"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-09
    body: "> I find your story about non-working cdrom hard to swallow. Can you supply more\n> details? Only once I have seen such a thing and it was a buggy bios that\n> wouldn't detect a masterless slave IDE device. \n\nIt was an LG 52x CDROM in a new computer. It stood master on the 2nd IDEwith no slave. (2 HD on 1st IDE, correctly master-slave configured).\nWindows worked fine. Boot from CDROM with MDK9.0 CD worked, but shortly after, the setup gave the error 'CDROM not foud', with lots of mount errors. A RH7.2 I had too, installed fine.\nFor my zip drive the same: I double checked the master-slave options (it's now slave on 2nd IDE), BIOS finds it, works perfectly under win98, but fails to mount under MDK9.0, and locks booting for 3 minutes! (on 'mounting local file systems')\n\n'Superior' is indeed a personal opinion, but IE is still faster than konqueror, which is in my opinion an important measure. But tabs in konqueror 3.1 is also a big plus, and most pages are shown quite well.\n"
    author: "Lightning"
  - subject: "Re: HATS OFF to the KDE Dever's"
    date: 2003-01-09
    body: "I've never even had a zip drive and I have no idea why it might fail. \n\nAs for cd and mdk, try booting with the alternate kernels on second cd. If that fails too, try hdparm -d 0 /dev/hdc at busybox prompt. Mandrake's QC is a bit lax, to say the least. \n"
    author: "nusuth"
  - subject: "Thank you all"
    date: 2003-01-05
    body: "Thanks for KDE. It's the best Desktop Environment on U*IX (and derivates)."
    author: "oGALAXYo"
  - subject: "Konstruct and upgrade-diff packages"
    date: 2003-01-05
    body: "Konstruct is an excellent tool, thank you!  As KDE is getting increasingly large, it seems that it would be possible to provide upgrade-diff tar balls and have Konstruct (via patch) pull it all together.  The process could be automated on both ends (tar packagers and users of Konstruct) to provide a seemless upgrade between releases.  This would save a considerable amount of bandwidth, and certainly would help modem users like myself.\n\nDarin"
    author: "Darin Manica"
  - subject: "OT qt-copy issues"
    date: 2003-01-06
    body: "Could someone confirm some recent borkage in qt-copy HEAD ? fails to create moc.y or something and fails to build in general. I didn't made any changes to my system e.g. same bison, same compiler same glibc same lexer (flex) so I assume it's more coderelated."
    author: "AC"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-06
    body: ".. used to compile like a charm 26th dec 2002 with named setup."
    author: "AC"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-06
    body: "Same problem here. Can't get it to build... :(\n\nAny ideas?"
    author: "Shyru"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-06
    body: "Nope, no ideas. Let's wait for a qt-copy update which may arrive every day. But good to know that I'm not the only one."
    author: "AC"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-06
    body: "You may want to read README.qt-copy\n\nGNU Bison did not work for me, so i did as they said and used byacc instead. I think there's your problem. "
    author: "eli0tt"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-06
    body: "No, I also read README.qt-copy, and thought, ahh, its a yacc problem. So I installed byacc, and set YACC='byacc -d' before configuring QT, but this did not help! :("
    author: "Shyru"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-06
    body: "Yeah, setting the YACC variable did not do the trick for me, too. When trying to build it still used the \"yacc\" command instead of \"byacc\". Try looking for this.\n\nIf this is the case try editing the Makefile in 'src/moc' directly and change \"YACC = yacc\" to \"YACC = byacc -d\". Finally - in that dir - do a \"make clean\" and \"make\".\n\nIt worked for me."
    author: "eli0tt"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-07
    body: "It seems to me to screw up y.moc during the qmake/configure portion.  If you do a cvs up after these steps you will see cvs seemingly trying to merge two files.  I deleted these two files, cvs up again and then make and it worked."
    author: "Alan Chandler"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-06
    body: "No, Bison used to work flawlessly for me before. That's definately no Bison issue. As I previously wrote QT-COPY used to compile flawlessy for me with the same setup on 26th dec 2002."
    author: "AC"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-07
    body: "I can confirm that QT 3.1.1 seems broken (3.1.0 builds nicely on this box).\n\nOn my box it breaks like this :\n\n\ngmake[2]: Entering directory `/usr/local/qt-x11-free-3.1.1/src/moc'\nyacc -d moc.y\nmoc.y:849: warning: previous rule lacks an ending `;'\nmoc.y:908: type clash (`' `arg') on default action\nmoc.y:1066: type clash (`' `string') on default action\nmoc.y:1067: type clash (`' `string') on default action\nmoc.y:1068: type clash (`' `string') on default action\nmoc.y:1094: warning: previous rule lacks an ending `;'\nmoc.y:1273: type clash (`' `string') on default action\nmoc.y:1274: type clash (`' `string') on default action\nmoc.y:1402: type clash (`' `string') on default action\nmoc.y:1436: type clash (`' `string') on default action\ngmake[2]: *** [moc_yacc.cpp] Error 1\ngmake[2]: Leaving directory `/usr/local/qt-x11-free-3.1.1/src/moc'\ngmake[1]: *** [src-moc] Error 2\ngmake[1]: Leaving directory `/usr/local/qt-x11-free-3.1.1'\ngmake: *** [init] Error 2"
    author: "Jesper Juhl"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-07
    body: "Yep exactly this. Thank you."
    author: "AC"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-07
    body: "I emailed the QT people of TrollTech about this, and they told me the reason for this breakage.\n\nIt turns out that building QT 3.1.1 (and, they say 3.1.0 as well) requires Berkeley yacc. Using bison in yacc compatibility mode is not good enough, you need the real yacc installed.\n\n\n/Jesper Juhl\n"
    author: "Jesper Juhl"
  - subject: "Re: OT qt-copy issues"
    date: 2003-01-07
    body: "that's what i said from the beginning, thx!\nit builds fine for me (with Berkeley yacc)."
    author: "eli0tt"
  - subject: "Keep up the good work!"
    date: 2003-01-06
    body: "Even though I haven't had the time to try rc6 yet, rc5 is truly a good product. The developers have done a great job, and deserve to be thanked excessively :-)"
    author: "Christian Iversen"
  - subject: "Re: Keep up the good work!"
    date: 2003-01-08
    body: "hail that!\n\n/kidcat"
    author: "Anders Juel Jensen"
  - subject: "Konstruct on FreeBSD"
    date: 2003-01-06
    body: "anyone had any luck getting it to work? I get:\n$ make install\n\"../../gar.conf.mk\", line 66: Need an operator\n\"../../gar.conf.mk\", line 74: Need an operator\nVariable LD_LIBRARY_PATH is recursive.\n"
    author: "elmaquer"
  - subject: "Re: Konstruct on FreeBSD"
    date: 2003-01-06
    body: "doh! \ngmake should do the trick methinks"
    author: "elmaquer"
  - subject: "Re: Konstruct on FreeBSD"
    date: 2003-01-31
    body: "I am getting this same message on my FreeBSD 4.7 install.\nI have read that LD_LIBRARY_PATH is a bad thing and im not\nsure if i want it set on my system (yet).\n\nIs there another way to run konstruct without setting \nthe LD_LIBRARY_PATH variable?\n\nAgain, here's my issue with konstruct:\n\n$make install\n\"../../gar.conf.mk\", line 66: Need an operator\n\"../../gar.conf.mk\", line 74: Need an operator\nVariable LD_LIBRARY_PATH is recursive.\n\nThanks\nNate"
    author: "nate"
  - subject: "Re: Konstruct on FreeBSD"
    date: 2003-01-31
    body: "Is that with or without GNU make? And if you're root, change /etc/ld.so.conf and run ldconfig (don't know what matches on FreeBSD) instead of setting LD_LIBRARY_PATH."
    author: "Anonymous"
  - subject: "Re: Konstruct on FreeBSD"
    date: 2003-02-10
    body: "Well, I'm following the Konstruct instructions as follows:\n\n\nLogin:user3\n\npassword:\n\n$ cd ~/dwnlds/konstruct\n$ cd meta/everything; make install\n\nI also tried to use GNU make.\n\n$ cd ~/dwnlds/konstruct\n$ cd meta/everything; gmake install\n\nafter I run gmake for over 4 hours, it gives me compiling errors, but I don't know where to look for the errors.  Any ideas?\n\nIs there a gmake log file somewhere that might enlighten me?\n\nThanks \nNate"
    author: "nate"
  - subject: " KDE 3.1 Requirements"
    date: 2003-01-06
    body: "I note that if you get the current version of Java from Sun that it includes JSSE.\n\nI would suggest that you should get at least GhostScript 7.0.5 (8.0 is available but it insn't GNU).  Does KDE require: \"libgs.so.x\" and/or does it require an application named: \"gs\"?\n\nI didn't see anything about FontConfig or Xft[2] -- the FCPackage.\n\nDoes anybody know about: \"dnotify\"?  Can you use this?  Does it do any good.?\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re:  KDE 3.1 Requirements"
    date: 2003-01-07
    body: "dnotify is a linux 2.4.x kernel API for informing processes of reads/writes\nto files. I compiled my kdelibs with this option and it seems it\nmakes konqueror hold a lock on directories you open with it.\n\nAs a result you can't umount /mnt/cdrom while having it open in a \nwindow.\n\nDon't use it, install libfam and no such things will trouble you :)"
    author: "Stijn Buys"
  - subject: "Re:  KDE 3.1 Requirements"
    date: 2003-01-07
    body: "I experienced exactly the opposite.  In my case, it was fam that held locks on directories... and in the case of removable media (floppies, cdroms), I sometimes had to manually kill the fam process before I could unmount them.\n\nThe easier workaround (when I stuck to using fam) was to change konqueror to a different directory before closing it's window.\n\nOn a side note, I'm not sure if dnotify and fam are mutually exclusive, or even if these options even serve the same purpose (I *think* they are at least similar).\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "kde requirements"
    date: 2003-01-06
    body: "Hey, has nobody noticed the second-to-last link in the article? (http://promo.kde.org/kde31_reqs.php)  It is the often-requested list of every single library or external program that KDE needs or can use at compile time, complete with an explanation of the features enabled when KDE finds it!  Thanks a lot Dre!"
    author: "not me"
  - subject: "Re: kde requirements"
    date: 2003-01-06
    body: "I noticed it, it was mentioned a while ago in an other article. The only thing that is not clear to me about those 'requirements' is wether they are compile-time requirements or a mix of compile-time and run-time requirements.\n\nIf it's a mix, the page should show which ones are compile-time and which ones are run-time."
    author: "Johan Veenstra"
  - subject: "Re: kde requirements"
    date: 2003-02-07
    body: "That page seems to be broken... is there an updated link? I'm trying to build via konstruct, getting some errors, want to be sure of my libs etc...\n\nthanks!\nmax inglis"
    author: "Max Inglis"
  - subject: "Speed"
    date: 2003-01-06
    body: "I've just compiled arts, libs, base and the one thing I notice is that it feels much faster than rc5 (I do *not* think rc5 was slow, btw). \n\nHas anyone else noticed this or is it just the palcebo effect?"
    author: "Gralbe Able"
  - subject: "Re: Speed"
    date: 2003-01-07
    body: "I found it very quick too.\nI have compiled KDE with specific gcc flags :\nCFLAGS and CXXFLAGS =\n-fomit-frame-pointer -falign-functions=4 -fancy-math=387 -mcpu-i686 as recommended in konstruct and there is a big difference in speed with all previsous \nreleases. \nSo far everything is very stable using these flags...\n\nDoes someone know if I can safely compile QT with those options ?\n"
    author: "socialEcologist"
  - subject: "Kdvi"
    date: 2003-01-06
    body: "Hi, \n\nthanks for this RC. I only have the following problem.\nKdvi tends to freeze when reading a .dvi file. At the end, it does the job, but it takes too much time and in the meantime the machine is useless. Even the clock freezes.\nThe only message i get when running Kdvi it from the konsole is:\n\nQObject::connect: No such slot infoDialog::setFontInfo(fontPool*)\nQObject::connect:  (sender name:   'Font Pool')\nQObject::connect:  (receiver name: 'Document Info')\nQMetaObject::findSignal:KDVIMultiPage: Conflict with KMultiPage::numberOfPages(int)\nkdecore (KAction): WARNING: KAction::insertKAccel( kaccel = 0x814de28 ): KAccel object already contains an action name \"help_whats_this\"\nQMetaObject::findSignal:KViewPart: Conflict with KParts::Part::setStatusBarText(const QString&)\n\nI'm using texstar's rpms of RC6, on a box with Mandrake 9.0\n\nThanks again for the excelent work!.\nI hope this helps.\n\nmuyfeo"
    author: "Muyfeo"
  - subject: "Re: Kdvi"
    date: 2003-01-07
    body: "\nthe rpms are not texstars, although he links to them. They are build by lmontel.\n"
    author: "obi"
  - subject: "Screen savers"
    date: 2003-01-06
    body: "After compiling RC6 on my Slackware-current installation none of my screen-savers work anymore.  Has anyone else had this problem?  \n\nThe x-screen-saver collection is still installed in /usr/X11R6/lib/xscreensaver/... which isn't in my path, but why did it break just from upgrading to RC6 (from RC5)?\n\nAside from that thanks to all the developers!  I love KDE and it seems like it keeps getting more useful every few weeks.\n"
    author: "Caspian"
  - subject: "Re: Screen savers"
    date: 2005-03-17
    body: "I would like to get the screen savers from the gnome desktop to run on the KDE environment.  Is that possible??\n\nThank you!"
    author: "Nick Sella"
  - subject: "Kiosk goodness!?"
    date: 2003-01-07
    body: "I've been putting together a Debian box with KDE 3.0.5, trying to set it up as a kiosk system.  (A public terminal to be available at a newspaper/magazine store here in Davis, California; once it's up, they've asked for another for a local Co-Op grocery store.  In both cases, they're replacing older Windows/Netscape systems)\n\nI've been eagerly awaiting 3.1, since everyone's been telling me the kiosk framework is much more polished.  Yay!  I hope this does it for me! :^)\n\nBill Kendrick\nLinux Users' Group of Davis\nhttp://www.lugod.org/"
    author: "Bill Kendrick"
  - subject: "Slackware-8.1 packages"
    date: 2003-01-07
    body: "There are packages for slackware-8.1 on the ftp if some people didn't notice it yet."
    author: "JC"
  - subject: "Re: Slackware-8.1 packages"
    date: 2003-01-08
    body: "many thanks to whoever packages these kde packages for slackware...\n\non another note, the cut & paste functionality of kde3.1RC6 seems to be less reliable than the 3.0 series. i never know what's going to be pasted when i Ctrl-V. it usually isn't what is listed as the newest copy on klipper. also, with 3.0.x the cut & paste in regards to konsole was good. with 3.1RC6 it's somewhat out of control.\n\ni've been using linux as my desktop exclusivly for 2+ years and i've never gotten a handle on the cut and paste usability.\n\nstill, kde rocks & many thanks to all who contribute....  "
    author: "jhollett"
  - subject: "Konstruct problem"
    date: 2003-01-07
    body: "Hi,\nI've tried using Konstruct (the tar konstruct-20030106.tar.bz2), and in the process of downloading things, two patches where not found (kdelibs's do-not-fail-fast-malloc.diff, and qt's auto-license.diff).\nAre those diffs needed? If not - they should be removed from the Makefiles of Konstruct.\n\nOther then this - this is a greate tool :)\n\n\nThanks,\nIlan"
    author: "Ilan Finci"
  - subject: "Re: Konstruct problem"
    date: 2003-01-07
    body: "They are in the files/ subdirectories of kdelibs and qt. You didn't delete the files/ directories yourself?"
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2003-01-07
    body: "sorry,\nMy mistake - I commented out the last line in gar.conf.mk, that added the local files (where the diffs are). I had some problems yesterday I thought where caused by it.\n\nWorking now.\n\nThanks.\nIlan"
    author: "Ilan Finci"
  - subject: "Re: Konstruct problem"
    date: 2003-02-07
    body: "Thanks, i made the same mistake. Your post saved me _a_lot_ of time.\n\nCheers\ntrenton"
    author: "trenton"
  - subject: "Re: Konstruct problem"
    date: 2003-06-10
    body: "I've hit the same problem, but I don't understand your explanation.  \n\nI downloaded all the .tar.bz2 source files to /usr/local/src/kde then added the line FILEDIR=/usr/local/src/kde to gar.conf.mk\n\nWhen I build konstruct/kde I just get loads of errors along the lines of:\n\nLocation: ftp://kde.uk.themoes.org/pub/kde/stable/3.1.2/src/do-not-fail-fast-malloc.diff [following]\n--20:55:12--  ftp://kde.uk.themoes.org/pub/kde/stable/3.1.2/src/do-not-fail-fast-malloc.diff\n           => `download/do-not-fail-fast-malloc.diff'\nResolving kde.uk.themoes.org... done.\nConnecting to kde.uk.themoes.org[212.100.230.18]:21... connected.\nLogging in as anonymous ... Logged in!\n==> SYST ... done.    ==> PWD ... done.\n==> TYPE I ... done.  ==> CWD /pub/kde/stable/3.1.2/src ... done.\n==> PORT ... done.    ==> RETR do-not-fail-fast-malloc.diff ...\nNo such file `do-not-fail-fast-malloc.diff'.\n\nmake[3]: *** [http//download.kde.org/stable/3.1.2/src/do-not-fail-fast-malloc.diff] Error 1\nmake[3]: Leaving directory `/usr/local/src/kde/konstruct/kde/kdelibs'\n*** GAR GAR GAR!  Failed to download download/do-not-fail-fast-malloc.diff!  GAR GAR GAR! ***\nmake[2]: *** [download/do-not-fail-fast-malloc.diff] Error 1\nmake[2]: Leaving directory `/usr/local/src/kde/konstruct/kde/kdelibs'\nmake[1]: *** [dep-../../kde/kdelibs] Error 2\nmake[1]: Leaving directory `/usr/local/src/kde/konstruct/kde/kdeutils'\nmake: *** [build] Error 2\n\nI've not deleted anything, whats going on.  "
    author: "Simon"
  - subject: "Re: Konstruct problem"
    date: 2003-06-10
    body: "Don't mess with FILEDIR! Read the README and *add* your source path to FILE_SITES."
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2003-06-11
    body: "Ah ok, that makes sense.  However the README file doesn't say to add entries to the FILE_SITES.  It implies you should replace the FILE_SITES variable all together (so I thought I was being clever only using the bit that appeared unused).  \n\nThanks, perhaps the README should be made a bit clearer on this, I can't be the only person to have made this mistake.  "
    author: "Simon Bazley"
  - subject: "Re: Konstruct problem"
    date: 2003-06-11
    body: "> However the README file doesn't say to add entries to the FILE_SITES.\n\n\"FILE_SITES  If you have already downloaded source tarballs add the path here.\"\n\n> It implies you should replace the FILE_SITES variable all together.\n\nDefinitive not."
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2004-03-11
    body: "I had problem also with do-not-fail-fast-malloc.diff, it was because the program was not able to create a symlink on the file, probably because I unzipped and run konstruct on a fat32 partition."
    author: "Christian"
  - subject: "Re: Konstruct problem"
    date: 2004-10-22
    body: "Starting from fat32 partition has generated the same error for me!\nI moved the folder to ext3 and it skips the error!\nThank you for your help gays!"
    author: "Jelio"
  - subject: "Re: Konstruct problem"
    date: 2004-10-22
    body: "I ment GUYS!\nReally SORRY!"
    author: "Jelio"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "I also seem to have a problem with the do-not-fail-fast-malloc.diff file.\n\nThe FILE_SITES variable points to the correct directory and the file does exist on my drive. This is the o/p when I try to use konstruct to build kdelibs-3.1.4:\n\n\n\n[===== NOW BUILDING:    kdelibs-3.1.4   =====]\ninstall -d download\n ==> Grabbing download/kdelibs-3.1.4.tar.bz2\n        ==> Trying file//files/kdelibs-3.1.4.tar.bz2\nmake[3]: Entering directory `/konstruct/kde/kdelibs'\nmake[3]: *** [file//files/kdelibs-3.1.4.tar.bz2] Error 1\nmake[3]: Leaving directory `/konstruct/kde/kdelibs'\n        ==> Trying file///home/crawford/kde3.1.4-sources/kdelibs-3.1.4.tar.bz2\nmake[3]: Entering directory `/konstruct/kde/kdelibs'\nmake[3]: Leaving directory `/konstruct/kde/kdelibs'\n ==> Grabbing download/do-not-fail-fast-malloc.diff\n        ==> Trying file//files/do-not-fail-fast-malloc.diff\nmake[3]: Entering directory `/konstruct/kde/kdelibs'\nmake[3]: Leaving directory `/konstruct/kde/kdelibs'\n        [fetch] complete for kdelibs.\ninstall -d cookies\n ==> Running checksum on kdelibs-3.1.4.tar.bz2\n82c265de78d53c7060a09c5cb1a78942  download/kdelibs-3.1.4.tar.bz2\nfile kdelibs-3.1.4.tar.bz2 passes checksum test!\n ==> Running checksum on do-not-fail-fast-malloc.diff\n9ec7379a27efa6bc738a1ddad0b92f4e  download/do-not-fail-fast-malloc.diff\nfile do-not-fail-fast-malloc.diff passes checksum test!\n        [checksum] complete for kdelibs.\ninstall -d work\n ==> Extracting download/kdelibs-3.1.4.tar.bz2\n        [extract] complete for kdelibs.\n ==> Applying patch download/do-not-fail-fast-malloc.diff\npatch: not found\nmake[2]: *** [normal-patch-do-not-fail-fast-malloc.diff] Error 127\nmake[2]: Leaving directory `/konstruct/kde/kdelibs'\nmake[1]: *** [dep-../../kde/kdelibs] Error 2\nmake[1]: Leaving directory `/konstruct/kde/kdebase'\nmake: *** [dep-../../kde/kdebase] Error 2 \n\n\n\"patch: not found\" is reported above but its in the download/ directory!\n\nAny ideas?"
    author: "Ross"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "You're not missing not the patch file but the \"patch\" binary (usually /usr/bin/patch). Install the package of your distribution which contains it (likely called \"patch\") and you should be fine."
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "Hey thanks for your help :) but I'm still a bit confused.\n\nI downloaded the patches from ftp://ftp.mirror.ac.uk/sites/ftp.kde.org/pub/kde/stable/3.1.4/src/ and the one which is having a problem is kdelibs-3.1.4.tar.bz2.  Are you saying there should be a separate binary file for the .diff file and if so I can't find that?\n\nThanks.\n\n:-?"
    author: "Ross"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "Try \"which patch\", what does it tell? If it doesn't tell you a path you're missing the required program, called \"patch\", to apply patches."
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "ah ok.  Thanks :)\n\nI'm a bit further now...\n\nWhile trying to configure Qt/X11 it falls over because it cannot find the qt_windows.h header file. Tried a google search for this file but couldn't get my hands on it.\n\nGot a copy?"
    author: "Ross"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "Qt/X11 doesn't need qt_windows.h. You didn't post the error and drew a wrong conclusion."
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "Sorry, here is the o/p:\n\n\n\n[Crawford Morgan@home]/konstruct/meta/kde:{55}:$ make install\n[===== NOW BUILDING:    kde-1   =====]\n        [fetch] complete for kde.\n        [checksum] complete for kde.\n        [extract] complete for kde.\n        [patch] complete for kde.\n ==> Building kde/kdebase as a dependency\nmake[1]: Entering directory `/konstruct/kde/kdebase'\n[===== NOW BUILDING:    kdebase-3.1.4   =====]\n        [fetch] complete for kdebase.\n        [checksum] complete for kdebase.\n        [extract] complete for kdebase.\n        [patch] complete for kdebase.\n ==> Building kde/kdelibs as a dependency\nmake[2]: Entering directory `/konstruct/kde/kdelibs'\n[===== NOW BUILDING:    kdelibs-3.1.4   =====]\n        [fetch] complete for kdelibs.\n        [checksum] complete for kdelibs.\n        [extract] complete for kdelibs.\n        [patch] complete for kdelibs.\n ==> Building libs/arts as a dependency\nmake[3]: Entering directory `/konstruct/libs/arts'\n[===== NOW BUILDING:    arts-1.1.4      =====]\n        [fetch] complete for arts.\n        [checksum] complete for arts.\n        [extract] complete for arts.\n        [patch] complete for arts.\n ==> Building libs/qt-x11-free as a dependency\nmake[4]: Entering directory `/konstruct/libs/qt-x11-free'\n[===== NOW BUILDING:    qt-x11-free-3.1.2       =====]\n        [fetch] complete for qt-x11-free.\n        [checksum] complete for qt-x11-free.\n        [extract] complete for qt-x11-free.\n        [patch] complete for qt-x11-free.\n ==> Running configure in work/qt-x11-free-3.1.2\n\nThis is the Qt/X11 Free Edition.\n\nYou are licensed to use this software under the terms of either\nthe Q Public License (QPL) or the GNU General Public License (GPL).\n\nType 'Q' to view the Q Public License.\nType 'G' to view the GNU General Public License.\nType 'yes' to accept this license offer.\nType 'no' to decline this license offer.\n\nDo you accept the terms of either license?\nCreating qmake. Please wait...\nmake[5]: Entering directory `/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/\nqmake'\ng++ -c -o qglobal.o -I. -Igenerators -Igenerators/unix -Igenerators/win32 -Igene\nrators/mac -I/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/include/qmake -I\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/include -I/konstruct/libs/qt-\nx11-free/work/qt-x11-free-3.1.2/include -DQT_NO_TEXTCODEC -DQT_NO_UNICODETABLES\n-DQT_NO_COMPONENT -DQT_NO_STL -DQT_NO_COMPRESS -I/konstruct/libs/qt-x11-free/wor\nk/qt-x11-free-3.1.2/mkspecs/cygwin-g++ /konstruct/libs/qt-x11-free/work/qt-x11-f\nree-3.1.2/src/tools/qglobal.cpp\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:176:24:\n qt_windows.h: No such file or directory\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp: In\n   function `int qWinVersion()':\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:190: er\nror: `\n   Qt' undeclared (first use this function)\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:190: er\nror: (Each\n   undeclared identifier is reported only once for each function it appears\n   in.)\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:190: er\nror: parse\n   error before `::' token\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:205: er\nror: parse\n   error before `::' token\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:210: er\nror: parse\n   error before `::' token\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:216: er\nror: parse\n   error before `::' token\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:218: er\nror: parse\n   error before `::' token\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:220: er\nror: parse\n   error before `::' token\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp: At\n   global scope:\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:235: er\nror: syntax\n   error before `::' token\nmake[5]: *** [qglobal.o] Error 1\nmake[5]: Leaving directory `/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/q\nmake'\nqmake failed to build. Aborting.\nmake[4]: *** [configure-work/qt-x11-free-3.1.2/configure] Error 2\nmake[4]: Leaving directory `/konstruct/libs/qt-x11-free'\nmake[3]: *** [dep-../../libs/qt-x11-free] Error 2\nmake[3]: Leaving directory `/konstruct/libs/arts'\nmake[2]: *** [dep-../../libs/arts] Error 2\nmake[2]: Leaving directory `/konstruct/kde/kdelibs'\nmake[1]: *** [dep-../../kde/kdelibs] Error 2\nmake[1]: Leaving directory `/konstruct/kde/kdebase'\nmake: *** [dep-../../kde/kdebase] Error 2\n\n\n\nThe reason I thought this was because of:\n\n/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/src/tools/qglobal.cpp:176:24:\n qt_windows.h: No such file or directory\n\nwhich appears to be the first error above. The qglobal file at line 176 has a #include for qt_windows.h which isn't on my system.\n\nApologies for any confusion."
    author: "Ross"
  - subject: "Re: Konstruct problem"
    date: 2003-10-26
    body: "> -I/konstruct/libs/qt-x11-free/work/qt-x11-free-3.1.2/mkspecs/cygwin-g++\n\nQt's configure detects that it's running on CygWin. You don't try to do that, or? You didn't say anything about your system yet."
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2004-05-26
    body: "I have the exact same problem. I'm trying to install the free qt on windows XP, and it cant find qt_windows.h"
    author: "Anonymous"
  - subject: "Re: Konstruct problem"
    date: 2005-05-26
    body: "I have a same problem:\n/cygdrive/c/cygwin/qt/qt-embedded/src/tools/qglobal.cpp:263:24: qt_windows.h: No\n such file or directory.And I don't no to fix that.Can you help me?"
    author: "andout"
  - subject: "Re: Konstruct problem"
    date: 2005-06-23
    body: "qt_windows.h can be found in any of the snapshots here:\nhttp://webdev.cegit.de/snapshots/kde-cygwin/qt/\nAdd it to your qt-x11-free-3.3.4/include directory"
    author: "Moe Bigsley"
  - subject: "Re: Konstruct problem"
    date: 2004-02-29
    body: "I get a slightly different variant of the above problem :\n(I'm installing KDE-3.2 on solaris8)\n\ninstall -d work/kdelibs-3.2.0\n ==> Applying patch download/do-not-fail-fast-malloc.diff\ncan't find file to patch at input line 3\nPerhaps you used the wrong -p or --strip option?\nThe text leading up to this was:\n--------------------------\n|--- work/kdelibs-3.2.0/configure       2003-11-30 14:18:30.000000000 +0100\n|+++ tmp/kdelibs-3.2.0/configure        2003-12-01 20:18:09.000000000 +0100\n--------------------------\nFile to patch: \n\n% /usr/local/bin/patch -v\npatch 2.5.4\n\nAny clue on what's wrong ?\n\nThanks,\n"
    author: "Prabal"
  - subject: "Konstruct problem while making kmail..........."
    date: 2005-04-24
    body: "hi!\nthere is a problem with konstruct & that is it get's stuck while making kmail. i'm attaching the o/p for the review.it doesn't moves ahead of the last line. any suggestions for that ...."
    author: "arun "
  - subject: "Re: Konstruct problem while making kmail..........."
    date: 2005-10-10
    body: "No reply!!!!!!!!!!!\n\nHello guyz i am having the same problem.\n\nDoesn't any one have the solution to it?\n\nComeon man i want to compile kde 3.4 on my fedora..."
    author: "[onlybasit]"
  - subject: "KDE 3.1rc6 RedHat 7.3 RPMS available."
    date: 2003-01-11
    body: "KDE 3.1rc6 RedHat 7.3 RPMS are available.\n\nSee\nhttp://kde-redhat.sourceforge.net/ \nand\nhttp://sourceforge.net/forum/forum.php?forum_id=242422\nfor details.\n\nEnjoy.\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "how do I save my session?"
    date: 2003-01-12
    body: "Hi,\nI've compiled and run KDE 3.1RC6 (using kostruct, which is just greate), and I have one small problem:\n\nWhen logging out, there is no option to save the session.\n\nI've seen in the control panel the options for the session manager, but it seems to to change:\n\n1. When I have the default, to start with previous session, some of the application I've started do not restart.\n2. When having the option to start with a manully saved session, I don't see how I save the session I want to start with.\n\nWhat do I miss?\n\nTHanks,\nIlan"
    author: "Ilan Finci"
  - subject: "Re: how do I save my session?"
    date: 2003-01-12
    body: "The second option enables a \"Save Session\" in the K menu right beside \"Lock Screen\"."
    author: "Anonymous"
  - subject: "Re: how do I save my session?"
    date: 2003-06-17
    body: "I want the session manager to save a session by default while logging off... but still want to be able to manually save a session. How do I do this? Is there a command-line thing I can do to save a session?"
    author: "Amit Shah"
  - subject: "Re: how do I save my session?"
    date: 2003-06-17
    body: "dcop ksmserver default saveCurrentSession"
    author: "Anonymous"
  - subject: "Re: how do I save my session?"
    date: 2003-08-02
    body: "On Solaris 9 :\n\n# dcop ksmserver default saveCurrentSession\n\ncall failed\n\nIdeas?"
    author: "Anonymous"
  - subject: "Re: how do I save my session?"
    date: 2003-12-07
    body: "Hi, Did you ever find the answer to your question? I am facing with the same problem.  Thanks!"
    author: "dan tan"
  - subject: "Re: how do I save my session?"
    date: 2003-12-08
    body: "As someone (anonymous) posted above:\n\ndcop ksmserver default saveCurrentSession\n\nIt didn't work for someone on Solaris, though."
    author: "Amit Shah"
  - subject: "Re: how do I save my session?"
    date: 2004-03-03
    body: "I noticed in previous versions of KDE (2 something?), when logging out of KDE,\nthe \"confirm logout\" dialog used to have a checkbox to cause the current session to be saved and restored when next starting KDE. Since then (kde 3), that feature is gone. Why?\n\nI would like to be able to save my session sometimes, but not always, and not have to manually choose another option to save my session, besides the \"logout\" option. Why was the \"save session\" checkbox in the logout dialog feature removed? Was it simply overlooked when developing KDE 3? It's a useful feature and IMO should be added again. Also useful would be if the checkbox would remember its previous state. i.e. if I check the box one time when logging out, the next time I log out, the checkbox is by default checked. If I leave the box unchecked, the next time I log out, it is by default unchecked.\n\nI would highly appreciate seeing this feature again in a soon-to-come version of KDE."
    author: "anonymouss"
  - subject: "Re: how do I save my session?"
    date: 2004-03-12
    body: "Omitting the \"save session\" checkbox in the logout dialog box has been an aggravation for some time.  I'd also like to see it fixed.  I'd like to see it just as described in the previous post:\n\n\"save my session sometimes, but not always, and not have to manually choose another option...  the checkbox would remember its previous state. i.e. if I check the box one time when logging out, the next time I log out, the checkbox is by default checked. If I leave the box unchecked, the next time I log out, it is by default unchecked.\""
    author: "Dean Brunson"
  - subject: "3.1 or RC7"
    date: 2003-01-14
    body: "well we'll see next week :)"
    author: "Adrian"
  - subject: "Re: 3.1 or RC7"
    date: 2003-01-14
    body: "hehehehehehehehehe"
    author: "Zvjer"
  - subject: "Re: 3.1 or RC7"
    date: 2003-01-15
    body: "=) 15 jan..mmm "
    author: "nick"
  - subject: "Resize"
    date: 2003-01-15
    body: "why i cannot resize the \"Find file\" window? i have a 1024x768 desktop this window i much larger! (i have Kde3.1 Rc5 compiled for Mdk9.. but this also appen whit the Kde3.0)\n\nCompliments for the good work an sorry for my horrible english!"
    author: "Anonymous"
  - subject: "Another (strange) Konstruct problem - dtd/kdex.dtd"
    date: 2003-01-22
    body: "I'm having problems building KDE with Konstruct:\n\nmake[5]: Entering directory `/mnt/spool/usr/tla/build/konstruct/apps/kcpuload/work/kcpuload-1.99/doc/en'\n/home/tla/kde3.1-rc6/bin/meinproc --check --cache index.cache.bz2 ./index.docbook\nindex.docbook:8: error: failed to load external entity \"dtd/kdex.dtd\"\n\nI've read around and found that libxml2 ver 2.4.26 introduced this problem, but also that it was fixed in later versions. My libxml2 version is 2.5.0...\n\nThe really strange thing is, all of the documentation upto this point has built successfully. I left the build running overnight, and when I came back it had stopped with the above error. But now when I go back into docs it's already built and attempt to rebuild them, that fails too... I've reinstalled libxml2{,-dev} to no avail. Any suggestions?"
    author: "lilac"
  - subject: "Re: Another (strange) Konstruct problem - dtd/kdex.dtd"
    date: 2003-01-29
    body: "I was having this problem too with the KDE 3.1 source, except it happened in the kdeedu docs.  I think Konstruct was using the wrong meinproc (or rather, it didn't build its own), so I had to do a few things by hand:\n\n - Change the DTD in each index.docbook from V1.1//EN to V1.0//EN\n\n - If the app's name isn't there already, add it to /usr/share/apps/ksgmltools2/customization/entities/general.entities\n\n - Run \"meinproc --check --cache index.cache.bz2 index.docbook\" by hand in each directory that Konstruct's make fails in.\n\nThere's probably a better way to do it, but this worked for me.\n\nHope that helps,\nCharlie"
    author: "Charlie"
  - subject: "Re: Another (strange) Konstruct problem - dtd/kdex"
    date: 2004-02-06
    body: "I had this problem with KDE3.2 also.\n\nFirst off, thatnks to Charlie for the initial pointer.  Running the meinproc command manually as you suggested fixed things up.\n\nHad a little trouble figuring this out initially.  This may be obvious to some but hopefully it'll help out a few.  For anyone else having this issue just before all the xml errors you'll see the following lines;\n\nmake[6]: Entering directory `/home/unifex/src/konstruct/apps/koffice/work/koffice-1.3/doc/koshell'\n/usr/bin/meinproc --check --cache index.cache.bz2 ./index.docbook\nindex.docbook:8: I/O error : failed to load external entity \"dtd/kdex.dtd\"\n\nWhat you need to do is cd to the dir in the final make[6] line and then run the meinproc command by hand.  You might as well open a new shell and do it from there also.  You'll be doing this a few times.  After each continue the make process until you get another.\n\nThe thing I don't get is that it appears the command is actually being executed by the script.  Why doesn't it work there?\n\nRegards,\nGold.\nhttp://www.babel.com.au/gold/\n\nThe directories below are those that I've had to do this in.\nkonstruct/apps/koffice/work/koffice-1.3/doc/kugar\nkonstruct/apps/koffice/work/koffice-1.3/doc/kword\nkonstruct/apps/koffice/work/koffice-1.3/doc/koffice\nkonstruct/apps/koffice/work/koffice-1.3/doc/koshell\nkonstruct/apps/koffice/work/koffice-1.3/doc/kspread\nkonstruct/apps/koffice/work/koffice-1.3/doc/kchart\nkonstruct/apps/koffice/work/koffice-1.3/doc/kpresenter\nkonstruct/apps/koffice/work/koffice-1.3/doc/thesaurus\nkonstruct/apps/koffice/work/koffice-1.3/doc/kformula\n"
    author: "Gold"
  - subject: "Re: Another (strange) Konstruct problem - dtd/kdex.dtd"
    date: 2004-02-09
    body: "> - If the app's name isn't there already, add it\n> to /usr/share/apps/ksgmltools2/customization/entities/general.entities\n\nHow should it be added? Which syntax? On my system that file\nalready lists kivio, for example:\n\n<!ENTITY kivio  \"<application>Kivio</application>\">\n\nTIA,\nMarco"
    author: "Marco Fioretti"
  - subject: "Re: Another (strange) Konstruct problem - dtd/kdex"
    date: 2003-12-16
    body: "I Have exactly the same problem with Konstruct for the last beta of KDE where building the doc of KOffice 1.2.94. \n\nDoes anyone have a solution for this ? I'm completly stucked... "
    author: "Ajrarn"
  - subject: "Re: Another (strange) Konstruct problem - dtd/kdex"
    date: 2003-12-16
    body: "Install bzip2 headers, http://bugs.kde.org/show_bug.cgi?id=70190"
    author: "Anonymous"
---
KDE 3.1rc6, which most likely will be the final KDE 3.1 release candidate (where have I heard that before<a href="http://dot.kde.org/1037074615/">?</a>), is now
<a href="http://download.kde.org/unstable/kde-3.1-rc6/src">available for
download</a>.  It incorporates all of the 
<a href="http://www.kde.org/info/security/advisory-20021220-1.txt">security fixes</a> from the security audit that <a href="http://dot.kde.org/1039281841/">delayed the release
of KDE 3.1</a>.  KDE 3.1 is scheduled for packaging on January 13th, so
your mission, if you choose to accept it, is to run this release through
its paces to find any show-stoppers.  To help compile it you might check
out <a href="http://konsole.kde.org/konstruct/">Konstruct</a>, a build system
which helps you to install KDE releases - it has been <a href="http://apps.kde.com/nfinfo.php?vid=8533">updated today</a> for
the rc6 release.  While its compiling you might hop
over to the draft
<a href="http://promo.kde.org/kde31_reqs.php">KDE 3.1 Requirements</a>
page and notify <a href="mailto:pour at kde dot org">me</a> of any omissions or mistakes.



<!--break-->
