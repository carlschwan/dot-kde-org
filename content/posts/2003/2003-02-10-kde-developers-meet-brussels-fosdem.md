---
title: "KDE Developers Meet In Brussels For FOSDEM"
date:    2003-02-10
authors:
  - "rkaper"
slug:    kde-developers-meet-brussels-fosdem
comments:
  - subject: "Mistake In Article?"
    date: 2003-02-10
    body: "<quote>\nWhile suggestions from the Swedish KDE clan on #kde-freebsd had us (well, some of us) consider visiting an American nightclub, we opted not to come off as complete geeks, so instead some stayed at the hotel\n</quote>\n\nI think you got this completely opposite.  Not going to the club, instead deciding to stay at the hotel, is what makes you guys complete geeks.  :-P\n"
    author: "Navindra Umanee"
  - subject: "Re: Mistake In Article?"
    date: 2003-02-10
    body: "Also, how can you visit an American nightclub in Brussels? One of these KDE developers own a Lear Jet? Or am I missing something? "
    author: "Brunes"
  - subject: "Re: Mistake In Article?"
    date: 2003-02-10
    body: "An 'American Nightclub' is a place for visitors from the US. They seem to feel at home in those places, maybe because they have to pay twice as much for a drink (compared to other places). Those Swedish developers probably won't even notice the inflated prices as well, a beer in Sweden is even more expensive.\n\n\n"
    author: "Johan Veenstra"
  - subject: "Re: Mistake In Article?"
    date: 2008-04-06
    body: "I completely agree with all that here is told\nSo you can find the information on it on my search resource \nhttp://fileshunt.com\n"
    author: "Rapidshare"
  - subject: "Re: Mistake In Article?"
    date: 2003-02-10
    body: "Not me, I revisited Lop Lop with Dave. Don't cut off that part of the quote. ;-)\n\n"
    author: "Rob Kaper"
  - subject: "A bit OT, but what the heck"
    date: 2003-02-10
    body: "\nSeems you captured a fine car make, namely Lotus :\n\nhttp://static.capsi.com/img/photography/2003-02-09-fosdem/800x600/dscf0084.jpg\n\nCheers,\n\nRob. (Lotus and Ferrari fan(yes it is possible))"
    author: "Rob Buis"
  - subject: "Re: A bit OT, but what the heck"
    date: 2003-02-12
    body: "Again OT, but...\n\nhttp://static.capsi.com/img/photography/2003-02-09-fosdem/800x600/dscf0075.jpg\n\nOK, I don't know this guy - bug or feature ?! :/  "
    author: "AC"
  - subject: "Presentations"
    date: 2003-02-10
    body: "One of the presentations I gave in the KDE developers room was about \"how to write a Makefile.am\", something that's still not very well documented anywhere.\nI hope to find some time to complete that presentation (I put it together in 15 minutes right before the talk:), and to add that stuff to .... hmm, the developer FAQ on developer.kde.org, I guess.\n\nThe other presentation was about KTrader, but without slides.\n\nI'll put the main presentation (the one made in front of a full room), into the kdepromo module. If anyone wants to see it and doesn't have CVS access, please tell me and I'll put it online as html on my homepage."
    author: "David Faure"
  - subject: "Re: Presentations"
    date: 2003-02-10
    body: "Oh, yes. Please do this! I spent hours the other day before it did what I wanted, and it was obvious to me at least that really didn't know what I was doing. A clear, up-to-date, description of how to manually set up a new KDE project is sorely needed."
    author: "jd"
  - subject: "A few more pictures:"
    date: 2003-02-10
    body: "Here are my (few) pictures from the event:\n\nhttp://devel-home.kde.org/~wheeler/fosdem-2003/"
    author: "Scott Wheeler"
  - subject: "Re: A few more pictures:"
    date: 2003-02-10
    body: "More FOSDEM pics are available at http://fosdem.3ti.be\n\nYeah, I was there too... I went to Faure's talk (it was excellent), visited the KDE developers room, and did many more things. There were so many things to do, and yet so little time (a whole weekend :-o)\n\nI will be back next year :-)"
    author: "Andy Goossens"
  - subject: "You should be ashamed of yourself !"
    date: 2003-02-10
    body: "<em>Comment snipped</em>\n<!--\nGo play with your toys somewhere else please. It seems like you're too young to understand grown-ups stories. You may be smart enough to code but it's now obvious that it doesn't prevent you from being dumb enough to do such idiot things like going to the Iraq ambassy to take some ridiculous shots. Actually, you're just too stupid... May God forgive you and your stupidity, you don't know what you're doing... You're a shame for your country, hopefully most Americans are a lot smarter than you...\n\nShame on you\n\nthibs\n\n-->\n"
    author: "thibs"
  - subject: "Re: You should be ashamed of yourself !"
    date: 2003-02-10
    body: "<em>Comment snipped</em>\n<!--\nDon't worry.   Soon he'll go and hook himself back up to CNN, where everything is right with the world.   Or at least it will be as long as the god given saviour nation of the universe does its job.\n-->"
    author: "anon"
  - subject: "Re: You should be ashamed of yourself !"
    date: 2003-02-10
    body: "Did I miss something??"
    author: "AC"
  - subject: "Re: You should be ashamed of yourself !"
    date: 2003-02-10
    body: "<em>Comment snipped</em>\n<!--\nI'm not American. And I'm afraid you don't get sarcasm. I deliberately did not link that session from this article or mention it, because I don't want a political discussion here. But they are on my site, and I welcome anyone to discuss it on kde-cafe instead.\n\n-->"
    author: "Rob Kaper"
  - subject: "chinatown"
    date: 2003-02-10
    body: "Chinatown in brussels? There are about 20 chinese restaurants in Elsene, but a real chinatown?"
    author: "anon"
  - subject: "Re: chinatown"
    date: 2003-02-10
    body: "Well, for the lack of a better word.. it is definitely an area with a high contentration, just like there are a lot of d\u00f6ner places on the other side of Grande Place (also recommendable).\n\n"
    author: "Rob Kaper"
  - subject: "Re: chinatown"
    date: 2007-09-30
    body: "There's indeed no Chinatown in Brussels. It is merely a few Asian Shops together. The only official recognised Chinatown in Belgium is the one located in Antwerp. This Chinatown is 10 times larger then the one in Brussels. Antwerp is the second largest city in Belgium, although it is not the capital of Belgium, it seems that all Asian people of Belgium go there for shopping and dim sum. Antwerp seems actually richer then Brussels if u look at the streets and people, maybe becoz of the income of the Port of Antwerp. \n\nHowever take a look at this website:\n\nhttp://www.chinatown-antwerpen.be/\n"
    author: "Nordin"
  - subject: "If you feel lack of time"
    date: 2009-03-07
    body: "If you feel lack of time needing to find important files you should look here <a href=\"http://sharesdigger.com\">http://sharesdigger.com</a> I keep using it continually."
    author: "Harvey"
---
Several KDE developers travelled to Brussels, Belgium to attend the <a href="http://www.fosdem.org/">FOSDEM</a> event. Activities included bugfixing, development discussions and presentations, keysigning and of course socializing. 
<!-- remove links as long as inappropriate politically charged pictures are there
<a href="http://capzilla.net/photography/sessions/20030207.html">Pictures</a> (<A href="http://devel-home.kde.org/~wheeler/fosdem-2003/">more</a>, <a href="http://fosdem.3ti.be/">more</a>) are available.
-->
Read on for an extensive report of all the happenings!

<!--break-->
<p>
<blockquote>
<p>
Friday afternoon the first developers arrived in Brussels by train. After the necessary hotel check-ins and refreshing, we had dinner in Brussel's chinatown. First lesson learned: coffee with sake works amazingly well, excessive chili sauce does not.</p>

<p>
Spices were washed away at the famous <a href="http://beeradvocate.com/beerfly/user_reviews/3315/">Lop Lop bar</a>, also worth visiting <a href="http://capzilla.net/photography/sessions/20020217.html">last year</a>.</p>

<p>
On <a href="http://capzilla.net/photography/sessions/20030207.html">Saturday</a>, many bugs were fixed for <a href="http://unixcode.org/atlantik/">Atlantik</a> and <a href="http://kopete.kde.org/">Kopete</a>, while David Faure worked on <a href="http://koffice.kde.org/">KOffice</a> and the <a href="http://www.kroupware.org/">Kroupware</a> initiative in between talks and presentations.</p>

<p>
Friends of KDE and open source such as <a href="http://www.gnome.org/">GNOME</a>'s Michael Meeks also paid a visit to the KDE developer room, while <A href="http://www.mozilla.org/">Mozilla</a> developers too met with KDE developers, emphasizing the respect and friendship there is between the projects and their developers.</p>

<p>
While suggestions from the <a href="http://i18n.kde.org/teams/sv/">Swedish KDE</a> clan on <a href="http://house.linuxinstruct.com/stats/kde-freebsd/">#kde-freebsd</a> had us (well, some of us) consider visiting an American nightclub, we opted not to come off as complete geeks, so instead some stayed at the hotel, partially sponsored by the <a href="http://www.kde.org/ev/">KDE eV</a>, and others revisited Lop Lop.</p>

<p>
Sunday brought more development and discussion, as well as some keysigning. The KDE web of trust keeps growing and growing, stretching vast parts of Europe while gaining a trust paths to and from developers in the Netherlands, resulting in a joining with the Californian web of trust, which entered Europe after <a href="http://capzilla.net/photography/sessions/20020812.html">LWCE 2002 in San Francisco</a> and has now been connected.</p>

<p>
Altogether a successful event. Things were a little more organized than last year, both FOSDEM itself as well as the KDE presence: David Faure, Olivier Goffart, Alexander Kellett, Rob Kaper, Martijn Klingens, Daniel Molkentin, Fabrice Mous, Josef Spillner and Scott Wheeler. Thanks for coming (again), I have enjoyed and am looking forward to meet many more KDE developers next year or at one of the <a href="http://events.kde.org/calendar/coming.phtml">upcoming events</a> in 2003!</p>
</blockquote>
