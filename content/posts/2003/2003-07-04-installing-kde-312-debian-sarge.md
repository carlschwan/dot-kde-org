---
title: "Installing KDE 3.1.2 on Debian Sarge"
date:    2003-07-04
authors:
  - "pmaqueda"
slug:    installing-kde-312-debian-sarge
comments:
  - subject: "Great!!! Just what I needed when I needed!"
    date: 2003-07-04
    body: "Nice to see telepathy exists :-)"
    author: "Albert"
  - subject: "latest software revisions on debian"
    date: 2003-07-04
    body: "see my weblog on <a href=\"http://hotzenplotz.homelinux.org\">my dyndns server</a> on how to get the latest software revisions on your debian installation.  I had trouble putting xfree86 4.3.x. on my dell inspiron 2600."
    author: "Peter"
  - subject: "This link is dead!!"
    date: 2004-10-08
    body: "Sorry, but i can`t find this article, only in Spanish, but i can`t read in Spanish. Where can i found it? please answer."
    author: "devep"
---
The <a href="http://es.kde.org">KDE Hispano</a> user group <a href="http://es.kde.org/modules/sections/index.php?op=viewarticle&artid=19">has published a HOWTO</a> on installing KDE 3.1.2 on <a href="http://www.debian.org/">Debian</a> Sarge (testing).  Although it is very easy to install KDE 3.1.2 on Debian Woody (simply add a line to souces.list), and even easier on Sid (unstable) where it is available by default,  there aren't specific packages for Sarge -- only the old KDE 2.2.2 ones.  Of course, the HOWTO is also available in <a href="http://es.kde.org/modules/news/article.php?storyid=81">Spanish</a>.  Enjoy!
<!--break-->
