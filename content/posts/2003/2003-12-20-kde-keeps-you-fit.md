---
title: "KDE Keeps You Fit"
date:    2003-12-20
authors:
  - "kpfeifle"
slug:    kde-keeps-you-fit
comments:
  - subject: "And UNIX programming whitens your teeth!"
    date: 2003-12-20
    body: "We should do an info-mercial...\n\nKDE will help you:\n\nWhiten your teeth\nLose weight\nLook 10 years younger\nAnd much much more!\n\nAnd if you order now..."
    author: "Rayiner Hashem"
  - subject: "World Summit on the Information society"
    date: 2003-12-20
    body: "At the UN World Summit on the Information society in Geneve the internet cafe in the restricted area was based on KDE/Debian. However the company Lynuxcomputing(?) didn't configure it properly, so it was as buggy as a usual Windows internet cafe solution. We could easily mention various horrible usability issues. For instance Mozilla was installed as a default browser but users didn't get that the browser could be reached via the red star bird icon from the panel, instead they tried the openoffice icon, it took very long for oo.org to start. PDF couldn't be read at some computers, a Mozilla plugin wasn't installed. The password manager asked whether to remember logins. Of course you cannot use this feature in a multi-user environment. Kmail was also installed, but of course you would have to configure it first for a multi-user environment: many personals mails from other persons were stored in the outbox. Nobody configured his imap or pop3 account. A workaround solution would be to sent emails via a preinstalled email account and ask for the sender's adress to be entered to the reply-to field. Gaim was the third application that was \"shared\" among users.\n\nWhat does this example show: \n\n1) Linux lacks a good and easy Internet Caf\u00e9 solution. there are only few good solutions for Windows, Internet caf\u00e9s with Linux are still a pain as they lack the software infrastructure.\n\n2) Configuration matters\n\n3) KDE is a mature environment, it lookes amazing. You cannot blame KDe for misconfiguration.\n\n4) multi-user environments cause trouble"
    author: "Andr\u00e9, FFII"
  - subject: "Re: World Summit on the Information society"
    date: 2003-12-21
    body: "I have to say I'm honestly surprised how often there seem to be efforts to set up web kiosk computers mixing KDE and Mozilla, while setting up KDE alone would be sufficient (and easier, with no issues regarding basic PDF and multi-user environments support). We should also emphasize more that Konqueror is perfectly capable for above job and referring to Apple's use of KHTML in its Safari for reference."
    author: "Datschge"
  - subject: "Cool"
    date: 2003-12-20
    body: "I love what KDE::Enterprise is doing, KDE definitely needs more promotion."
    author: "Mike"
  - subject: "KDE really ROCKS"
    date: 2003-12-21
    body: "I've been totally weaned off of that other OS for about 14 months now.\n\nSince the beginning I've been using KDE, I've tried to use Gnome several times each time I was quite dissappointed. Retrunning to KDE because of the user friendlyness and stability.\n\nAlso KDE has a large pool of programs built to run on KDE desktop and many of them have become a part of my PC life.\n\nJust to name a few, Kjots, Knewsticker, Kmail, Kpackage Manager"
    author: "Steve McMullen"
  - subject: "Re: KDE really ROCKS"
    date: 2003-12-21
    body: "KDe does not compete with gnome but I believe that today KDE has a desktop market that opened up while gnome hasn't yet (despite SUn/Solaris ecc.). I don't think that one desktop has to rule but we have to bridge the gap. Huge progress has been made."
    author: "Jack"
---
<a href="http://www.itbusiness.ca/">ITBusiness.ca</a> has an interesting
<a href="http://www.itbusiness.ca/index.asp?theaction=61&sid=54420">
success story</a> about how LinuxMagic switched FitnessWorld to Linux and KDE.
Although the article does not mention KDE, we know they are using KDE thanks
to this <a href="http://enterprise.kde.org/bizcase/showbizcase.php?id=64">KDE Success Story</a> that was submitted to
<a href="http://enterprise.kde.org/">KDE::Enterprise</a>.
<a href="http://www.linuxmagic.com/">LinuxMagic</a>
is one the sponsors of the 
<a href="http://desktop.kdenews.org/strategy.html">KDE for the Enterprise</a>
initiative that was recently launched by KDE and Debian developers.
If you too have a
<a href="http://enterprise.kde.org/bizcase/">KDE Success Story</a>
to share with the world you can
<a href="http://enterprise.kde.org/bizcase/addcase.php">
submit it via the KDE::Enterprise website</a>.



<!--break-->
