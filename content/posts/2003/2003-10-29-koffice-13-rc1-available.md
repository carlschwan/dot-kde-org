---
title: "KOffice 1.3 RC1 Available"
date:    2003-10-29
authors:
  - "ltinkl"
slug:    koffice-13-rc1-available
comments:
  - subject: "Test with kde 3.1.x or kde cvs ?"
    date: 2003-10-29
    body: "Is it better to test koffice with kde 3.1.x or kde cvs ?\n"
    author: "maxime"
  - subject: "Re: Test with kde 3.1.x or kde cvs ?"
    date: 2003-10-29
    body: "It doesn't matter"
    author: "Lukas Tinkl"
  - subject: "keck sie"
    date: 2003-10-29
    body: "kexi aleady included??"
    author: "gerd"
  - subject: "Re: keck sie"
    date: 2003-10-29
    body: "Not yet, see http://www.kexi-project.org/wiki/wikiview/"
    author: "Lukas Tinkl"
  - subject: "way to go"
    date: 2003-10-29
    body: "Personally the last beta for 1.3 was more stable than 1.2.1.  The new features also push it past the toy/usable line for me.  It is now displacing oo.o as my day to day.  This is shaping up to be a really good release.\n\nAlso knowing tidbits like oo.o file formats will be default for 1.4 helps my confidence with it.  Prominent future goals give it a sense of direction that other projects seem to be lacking.\n"
    author: "theorz"
  - subject: "Re: way to go"
    date: 2003-10-29
    body: "Absolutely...\nIf only Krita could be there.\n\nGo Koffice."
    author: "Murphy"
  - subject: "Re: way to go"
    date: 2003-10-29
    body: "Oh yeah. I would love to have something simpler than Gimp any day. Right now, there are no alternatives. Don't mention XPaint, and don't ask what's wrong with Gimp. :) I use Gimp 1.3.x and I love it, but it has problems, and everyone knows it. Go Krita!"
    author: "m0nx00n"
  - subject: "Re: way to go"
    date: 2003-10-29
    body: "If nothing else, gimp is way too complex for average users. My father wa switching to Linux, but stopped partially due to the complexity of gimp. He needed a simpler image manipulation."
    author: "a.c."
  - subject: "Re: way to go"
    date: 2003-10-30
    body: "There is also Kolourpaint, which is new in kdenonbeta.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: way to go"
    date: 2003-10-30
    body: "Don't forget TUXpaint! You can do more with this program than with the average paint program for linux."
    author: "gerd"
  - subject: "Re: way to go"
    date: 2003-10-29
    body: "Well, there have been checkins to Krita recently... I'm finding Krita a fun project to hack on, and the underpinnings are quite solid nowadays, but there's still a lot to do. Apart from loading, saving, importing, exporting and showing images, Krita is now also capable of painting 10 pixel wide squares in any colour you like, as well as drawing one pixel-wide lines in any colour you like.  There are the rudiments of support for CMYK as well as RGB, too. And there support for tablets, too. The coloured squares will more or less transparent depending upon the pen pressure. And Patrick is working on premultiplied alpha, and i'm taking a stab at updating the webpages, too.\n\nI hope to add real brushes this week... And then there's the UI to be worked on, and other paint tools, and so on. But work is being done -- take a peek at the <a href=\"http://lists.kde.org/?l=kde-kimageshop&r=1&b=200310&w=2\">mailing list</a>.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: way to go"
    date: 2003-10-29
    body: "Krita is being resurrected from the dead? \nThat'd be outright wonderful!!"
    author: "Mike"
  - subject: "Re: way to go"
    date: 2003-10-30
    body: "It is a wonderful news...\nI hope some other developper could jump in. Where are you, Mosfet???"
    author: "Murphy"
  - subject: "Kword Tables still bad"
    date: 2003-10-29
    body: "as the title suggests: kword tables suck  \nbut otherwise a usable release. especially kspread is very nice."
    author: "jim"
  - subject: "Re: Kword Tables still bad"
    date: 2003-10-30
    body: "Can you open new bug reports for problems that you see with KWord's table?\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Kspread & Scatter Plots"
    date: 2003-10-29
    body: "Allright the title says it all. Can kspread do this? Can we add regression lines on the scatter plots?  This is one feature which i use heavily, if kspread doesnt have it, then there are plenty of ppl in the engineering industry for whom this is useless.\n\nI wish i am wrong!\n\nwhat do u say?\n\n\n\nPS: if it does, then an explanation on how to do it would be very helpful. "
    author: "TaNgO"
  - subject: "RPMS for redhat"
    date: 2003-10-31
    body: "KOffice 1.3RC1 (1.2.94) RPMS for redhat are now available in the 'unstable' section of the repositories hosted by kde-redhat:\nhttp://kde-redhat.sourceforge.net/\n\nEnjoy.\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: RPMS for redhat"
    date: 2003-11-01
    body: "The kde-Redhat people are doing a very good job.  I now enjoy \nan uncrippled kde on the distro I am most familiar.  The bad \nstate of KDE on official RedHat was always a headache, until \nkde-RedHat arrived."
    author: "Asokan"
---
The <a href="http://www.koffice.org/people.php">KOffice team</a> is pleased to announce the availability of the first release candidate of <a href="http://www.koffice.org/">KOffice</a> 1.3. As we're <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">approaching the final release</a>, we definitely need more users to test and <a href="http://bugs.kde.org/">report bugs</a> -- there are still some left. A <a href="http://www.koffice.org/releases/1.3rc1-release.php">short announcement</a> is available and also a <a href="http://www.koffice.org/announcements/changelog-1.3rc1.phtml">full changelog</a>. Happy downloading and bug hunting!
<!--break-->
