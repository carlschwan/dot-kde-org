---
title: "Adopt-a-Geek: Put Old Hardware to Good Use!"
date:    2003-01-27
authors:
  - "swheeler"
slug:    adopt-geek-put-old-hardware-good-use
comments:
  - subject: "What about distcc?"
    date: 2003-01-26
    body: "Distcc[http://distcc.samba.org/] should help a lot in those low-spec PC networks."
    author: "Saiyine"
  - subject: "Re: What about distcc?"
    date: 2003-01-27
    body: "Unfortunately this doesn't help if you are the owner of only one box."
    author: "eva"
  - subject: "Re: What about distcc?"
    date: 2003-01-27
    body: "If someone can't afford a new/upgraded PC at $500, what makes you think that he can afford to run 5-10 slower ones after considering the electricity costs?"
    author: "Anonymous"
  - subject: "Re: What about distcc?"
    date: 2003-01-27
    body: "The cost for electricity varies from country to country.  The cost for hardware are pretty much the same around the world.  So while a person might be able to afford the electricity in her or his native country, the same person might have to use be in a position to go and buy a computer just like that.  Let's say I can buy a beer in my country for $1.  In other countries the same single dollar can buy you 10 beers.  The same may hold true for electricity."
    author: "Anonymous"
  - subject: "Re: What about distcc?"
    date: 2003-01-27
    body: "At least in Norway, people have frozen to death recently (seriously) because they can't afford the electricity. Powering 5 computers for one year would cost around \u00801300, and besides, not everyone has the physical room for that much equipment."
    author: "Haakon Nilsen"
  - subject: "Re: What about distcc?"
    date: 2003-01-27
    body: "Where I live the shipping is more than the electric. I'd be happy to donate resources to a distributed compile farm but I'm much less likely to ship boxes."
    author: "Jacob Vanus"
  - subject: "Re: What about distcc?"
    date: 2003-01-28
    body: "power still isn't much.  Sure it might cost $10/month to run another comptuer, but that means you are looking at 50 months before the cost of power to run it == the cost to buy it.  Even today's best comptuers will be obsolete in that time.  To get $10/month means you never turn the comptuer off too.   Dorm rooms generally include power, and even if it isn't included, you can count on it not being much.   \n\nI have found that lack of a machine to test on has made KDE development impossibal.  While I'm sure it is possible, I gave up installing KDE-CVS for a different user because I couldn't figgure out how to use kde-release for most sessions (and considering some of the unstable stuff I wanted to test that is a requirement) and KDE-CVS for the one user.   I tried, but obvious modifications to the code didn't show up, proving I wasn't running KDE-CVS even though it was compiled.   User error I'm sure, but still a pain.   It would be much nicer to have a second machine to work with.\n\nI'm currently at a point where I can't justify $500 for a new computer, but I can pay my electric bills.   Sure I can save for it, but that will take a while.  If someone has an old computer they don't need, I can use it.   (Note, I'm not positive I will actually help on on KDE development, I'd like to but I haven't really proven to myself that I would contribute if given equipment.   since I'm not sure I will make use of the donation I'm hesitant to take it)"
    author: "bluGill"
  - subject: "Re: What about distcc?"
    date: 2003-01-27
    body: "What about distcc?? What about KDE 3.1??? someone got a nice joke about it at hand?"
    author: "joker"
  - subject: "KDE bloated?"
    date: 2003-01-27
    body: "I think that developers should develop/run KDE on old hardware, that way they'll find out how slow it is in the 'real world'\n\nCheers,\nbobbob\n/ dons flameproof underwear "
    author: "bobbob"
  - subject: "Re: KDE bloated?"
    date: 2003-01-27
    body: "Right, but in the 'real world' users don't rebuild KDE three times a week.  ;-)  When was the last time you tried to run a KDE in Valgrind (a memory error detection tool)?  No, the real world doesn't try to run KDE with a \"make -j 3\" running in the background, with full debugging symbols.  :-)"
    author: "Scott Wheeler"
  - subject: "Re: KDE bloated?"
    date: 2003-01-27
    body: "<<\n  the real world doesn't try to run KDE with a \"make -j 3\"\n>>\n\nUh... would that mean that gentoo users are not in the real world ?"
    author: "Henri"
  - subject: "Re: KDE bloated?"
    date: 2003-01-27
    body: "Basically if you're using the \"build everything from source\" option, no, you're not in the type of \"real world\" that the original poster was talking about (Since it was in reference to \"real world\" users having slower machines.).  And again, there's a big difference between building KDE once per release and building it several times a week.  \n\nThere's also quite a bit of a difference between binaries optimized for performance (a la Gentoo) and binaries optimized for debugging productivity.  And again, this is ignoring running apps with gdb, valgrind, etc.  :-) "
    author: "Scott Wheeler"
  - subject: "Re: KDE bloated?"
    date: 2003-01-30
    body: "Not all gentoo users have fast macines. I have a 300 Mhz comp, and i build KDE from source, along with everything else. Admitedly, I'd only do KDE-cvs on a fast comp, as that involves freuent rebuilds. "
    author: "Abhishek Amit"
  - subject: "Re: KDE bloated?"
    date: 2003-01-27
    body: " >> / dons flameproof underwear\n\nI am not going to flame you, just point out that this isn't an appeal so that KDE developers can computer fast enough to run KDE (I'd imagine even the poorest KDE developer can stretch to that, otherwise how'd they get into KDE in the first place), it is so that they can have machines which will compile KDE as quickly as possible (as is stated in the article (read it))."
    author: "nonamenobody"
  - subject: "Re: KDE bloated?"
    date: 2003-01-27
    body: "I think they shouldn't. One needs much more powerful machine for developing KDE than just for running it. I have an old K6/188. KDE still runs acceptably on it(well, assuming \"acceptably\" is defined as \"it won't make you so angry that you'll want to kick your computer\"). But developing KDE on it is next to impossible, changing one single file and rebuilding after that can take even 5 minutes (and of course also much more if you change an important header file). It wouldn't be much fun to spend 75% time just watching output from the build tools.\n\nYou should try to develop something KDE related for a week. THAT would show what is really slow (and perhaps you'd find KDE somewhat faster after such experience :-/  )."
    author: "L.Lunak"
  - subject: "Re: KDE bloated?"
    date: 2003-01-28
    body: "KDE plain sucks.  I'd rather go buy Windows because the GUI is not an after thought, like it is in Linux.  Gnome is a far superior GUI, but still got a long way to go.  If the KDE developers used GUI tools to develop, instead of the command line, they might understand the pain they put the KDE users through.  Take a look at Windows or even the Macintosh GUI tools and see why a GUI is not just \"press a button with the mouse\".  Theyere a many standard keyboard short cuts that lets users navigate the GUI without having to touch the mouse, and please dump that TCL/TK for building GUIs, the whole idea about that sucks, and the UIs created using those shows badly.  Then fix up the Tree on the File manager, it looks and behaves pretty badly.\n"
    author: "KDE Sucks"
  - subject: "Liberal vs. Conservative C/C++ Coding"
    date: 2003-02-01
    body: "I think that KDE 3.x could be made to run fast on a reasonable machine (reasonable being Pentium 200 Mhz with 32 Megabytes of RAM with a Cirrus Logic 4556 Video card, with more conservative C and C++ Coding. Its not that the software is too big, its just poorly optimized for older systems.\n\nThe developers of KDE will have every incentive to do this because of course if they employ conservative C/C++ Coding, it will not only mak KDE 3 run on slow boxen like that, but also be faster on faster machines"
    author: "Zombie Ryushu"
  - subject: "Re: KDE bloated?"
    date: 2003-02-13
    body: "\nI love KDE and Qt in general, but find that I (and the people I know) only use about half of the bundled apps KDE ships.  \n\nThere needs to be more focus.  Trim out some of the apps that just don't need to be there.  Or split out the bundles in more ways so that people can opt out of the apps they never plan to use.  A lot of it is just unnecessary and unwanted....\n\n"
    author: "Damage"
  - subject: "Only KDE?"
    date: 2003-01-27
    body: "I don't use KDE but I think this is a very good idea. Any KDE developers that develop applications that work well in Gnome also? Most do and I use some of them - I'd be willing to donate to anything I found interesting. Media editing, games, and edutainment are especially interesting to me.\n\nWhat I'd really like is someone that could get 3D/DRI working for a Trident Blade 3D chipset. I'd definately donate to that."
    author: "MikeFM"
  - subject: "Re: Only KDE?"
    date: 2003-01-27
    body: "Every modern CPU should be able to render faster than the Blade 3D...."
    author: "AC"
  - subject: "Re: Only KDE?"
    date: 2003-01-27
    body: "If you're using a Blade 3D you probably don't have a modern CPU either."
    author: "MrResistor"
  - subject: "Not Good Enough"
    date: 2003-01-27
    body: "We should be able to sponser geeks with cash!!!!\n\nLet us pay geeks via paypal. Let geeks set up the amount of money they require for the amount of development or tasks they complete.\n\nThen let us sponser their work."
    author: "HAnzo"
  - subject: "Re: Not Good Enough"
    date: 2003-01-28
    body: "I already do this. I pay Andras Mantia to work full time on Quanta, though I have to use more expensive methods than PayPal since they don't authorize Romanian residents. We do however accept donations via PayPal on quanta.sourceforge.net. If we could somehow manage to generate more interest in the community I could manage even more projects and developers but so far only a small portion of my expense is being covered by the community. We realize roughly 1 donation in 1,000 downloads which of course does not count the great many more users that got us in an RPM with their distro. Also we are seeing half the downloads we did for 2.x so it appears many more people are just running the distro provided version. Unfortunately several major distros shipped our worst pre-release ever.\n\nIf we ran donations in actual 1 in 1,000 users or 1 in 100 downloads not only would Quanta be further along but we would be working on other KDE programs too, like graphics solutions, and preparing for future developments in web AI and animation. Regardless I would say to anyone, if you don't think a guy in a start up business, who is by no means rich, should almost entirely subsidise the development of one of your tools... you know what to do. ;-)\n\nI don't care if you support Quanta per se, but if you can do something to help a project you like you should. KDE is the most incredibly efficient design model I've ever seen. If every user put pocket change in we could grow an application base faster than most people could imagine. If I were rich I'd do it because it's the right thing to do. At least I'm doing what I can right now."
    author: "Eric Laffoon"
  - subject: "Re: Not Good Enough"
    date: 2003-06-24
    body: "Yes, I would be interested in receiving money by paypal. I work as\na non-profit and would be interested in sponsors who are willing to\ndonate some money so that I can offer my services for free. If you \nare interested, please send me an email and I'll send you more details\nabout my work.\n\nAnu"
    author: "Anu"
  - subject: "good idea"
    date: 2003-01-27
    body: "I for one think this is an excellent idea! If I had any spare hardware that met the requirements (you don't need a 286??) I'd gladly donate! Keep up the good work!!\n\n-Jon"
    author: "jb"
  - subject: "I agree : good idea"
    date: 2003-01-27
    body: "\n\nI agree with the idea.... GOOD :)\nMaybee i will participate\n\ngettin' slashdotted!"
    author: "Mike Harris"
  - subject: "a-geek"
    date: 2003-01-27
    body: "There was a former KDE developer here who had to leave KDE development because he didn't have the sufficient hardware resources.  He was always writing long comments here.  I hope he gets a machine now."
    author: "ac"
  - subject: "All KDE developers in Finland"
    date: 2003-01-27
    body: "All KDE developers living/visiting Helsinki (Espoo etc.)\narea can contact me for help in this matter. I would be\nglad to collaborate and help with the project providing\na quiet place to code with tea/coffee and so forth.\n\nLater,\n\n-- Mikko Gr\u00f6nroos"
    author: "Mikko Gr\u00f6nroos"
  - subject: "distcc idea?"
    date: 2003-01-27
    body: "Reading some of the above comments I got a good idea...\nPerhaps someone could setup some form of a distcc network people could join and it  would match up compatible systems so people who want to compile KDE can share parts... For example, if I have a very similar system to someone else and we both want to compile KDE 3.1, the automated system could match us and my system would compile some of it as would his. When all parts are compiled, both systems would get the output files.\nI guess the real question is whether there's enough people who compile from source to make this worth doing..."
    author: "Luke-Jr"
  - subject: "Re: distcc idea?"
    date: 2003-01-28
    body: "It's a nice thought. But to do it you need bandwidth. Generally most people on a broadband connection have good hardware. Even so I belive you will only see about an 80% gain per identical system on a 100 Mbps network. This is way faster than T1 let alone dialup or ISDN. Add to that the costs for some for metered (and possibly bad) internet connections. To make practical use of it you would need it on a local net.\n\nEven setting up a compile farm raises the bandwidth issue. With CVS you are only getting the changed files in a package so it is actually a benefit to developers WRT to bandwidth."
    author: "Eric Laffoon"
  - subject: "donating processor time"
    date: 2003-01-27
    body: "hi\n\ni have an athlon 650 mostly idling in my house. It's my server, so I can't give it away, but its life is pretty boring right now.\n\nSo, would it make sense to write some kind of compile@home-client? I'm not interested in a 1-to-9485794759375395793857395 chance of finding extraterrestrial life, but helping KDE would be a good thing."
    author: "me"
  - subject: "3.1 is out"
    date: 2003-01-27
    body: "seems that 3.1 is avalible for SuSE at ftp.kde.org\n\nAnyone who knows if you can use Yast2 to upgrade to KDE3.1?.. \n\nbye"
    author: "KDE"
  - subject: "Good idea"
    date: 2003-01-27
    body: "\nI think this is a good idea, it tries to address a problem which actually exists. \nIn my case, I'm locked out from KDE development simply because my 486DX doesn't have a VGA compatible graphic card(it wouldn't have the power to run it anyway) and that's a shame. \nI don't think there will be many single contributors that 'send in' one or two computers; but, if this projects gets more 'official' and the computer/parts gets sent to a central place instead of individuals I think we perhaps will see corporations donating old hardware(but still useable for KDE devs). We must perhaps wait until corporations rests financially on the KDE project and when they *know* their donated hardware will come to serious use, and to those that really needs it to contribute to KDE.\n I think we can help with that - making sure those donating are assured their effort is meaningfull. They must know it will be used by serious persons. Perhaps somekind of system where people can comment on those that want hardware to make sure they are serious.\n\nAnyway, at these days, no matter where you live it's not that hard to get a acceptable computer. If you really want to develop for KDE you simply get yourself a ordinary job and work for a couple of weeks. For 500$ you get a formidable computer at these days(exclusive monitor though).\n\nMy two cents..\n\ncheers,\nFrans"
    author: "Frans Englich"
  - subject: "Re: Good idea"
    date: 2003-01-27
    body: "One more thing; alot of people feel like freeloaders when using open source products. If I got a computer donated I would feel bad if I didn't contribute. I think it will be hard finding people willing to recieve free hardware - simply because they would feel 'guilty' and freeloaders.\n\ncheers,\nFrans"
    author: "Frans Englich"
  - subject: "Re: Good idea"
    date: 2003-01-27
    body: "Well, you just don't know what you're talking about... Or you're talking US/Western Europe only. Two weeks/$500... \nAbout 1 billion of people on this planet live on less than $1 a day...\nWell. I would actually donate to this project, if postal/customs costs wouldn't be so high. $150 MoBo/CPU/RAM combo (quite decent combination, BTW) costs about $200 to send from here (.ru).\n\n\n"
    author: "Yarick"
  - subject: "IT'S OUT!!!!"
    date: 2003-01-27
    body: "[Ed: Please don't flood the main server, give the mirrors a chance to sync.]\n"
    author: "Anon"
  - subject: "Re: IT'S OUT!!!!"
    date: 2003-01-27
    body: "The info page has been updated as well\nhttp://www.kde.org/info/3.1.html\nThe MD5 sums and links to the binaries are there."
    author: "Excited like a little school girl"
  - subject: "Compile Farm"
    date: 2003-01-27
    body: "Well now all of the people harping about performance are going to donate Pentium 1 machines for the developers to run KDE on.  Of course compiling on them would be a nightmare, but maybe if we could provide a compile farm somewhere it would be a valid option.  Of course this depends on the network connectivity the developers have."
    author: "Anonymous Monkey"
  - subject: "Re: Compile Farm"
    date: 2003-01-27
    body: "i think this MIGHT be a better idea then donateing hardware to the developers - just run a compile farm - maybe like sourceforge.net's - but the user would have to have high speed internet access - i dont know if anyone even uses dial up anymore.\n\nImagin a whole rack of 1u's clustered together, you could compile KDE in 10 seconds :-D"
    author: "BlakeRG"
  - subject: "Re: Compile Farm"
    date: 2003-01-28
    body: "Actually no matter how fast you can compile things; you still have to link the compiled objects together and that doesn't parallelize well.  Let's say you could compile all of KDE in 10 seconds, but then it takes you an hour to do preprocessing and two hours to link, then another one to send across the internet -- even to those with home broadband.  As unfortuntate as it is for all of those interested in helping out this way -- and the developers that would potentially benefit from it, with current tools it really isn't practical.\n\nSF doesn't do that to produce working binaries for use on the developers' computers -- this is for them to debug their software with different hardware / software combinations.  While a worthwhile effort, it addresses a completely different need than Adopt-a-Geek."
    author: "Scott Wheeler"
  - subject: "Let us send cash via paypal"
    date: 2003-01-27
    body: "I want to sponser geeks development via paypal."
    author: "HAnzo"
  - subject: "Re: Let us send cash via paypal"
    date: 2003-01-28
    body: "You can find more info on kde.org, or if you are so inclined you can find it on my favorite... quanta.sourceforge.net"
    author: "Eric Laffoon"
  - subject: "Re: Let us send cash via paypal"
    date: 2007-04-06
    body: "I need to think about sharing a big deal to all peoples who are searching for a really solid way to start a program which can make internet a very rich place and also it gives a very different way to change the world microfinancial position up to much extent .\n      The main theme of my idea ,which starts from today is :-\nI have started a business in which i have 50% share .This business starts with only $500 as initial money which i have earned so far from various sources and my job .Now I have divided this amount in 500000 shares of 1cent each .\n   Now as i need more money to run this business (the details will be available to all participants of this program )So initially each share with face value 1cent is available for $1 to all the shareholders .\n   Benefits to shareholders :-\n---------------------------------\n1.They have a shared profit from the company income .And whatever the income is equally divided and paid using paypal .\n2.They have the opportunity to earn too much if the company outperforms in the market .\n3.They help the online community to develop and search some way to grow more by which they have the opportunity to make the online world a much better place .\nThings to do for investors :-\n----------------------------------\n1 .They have to pay the money for the specific number of shares they need .The costing is 1cent per share .That is for 100 shares ,Participants need to pay $1 .\n2 .The Paypal address to which the amount paid is ashumit02@gmail.com "
    author: "mits"
  - subject: "Re: Let us send cash via paypal"
    date: 2003-01-30
    body: "There are a few problems with this -- the first is that I think Paypall is mostly a US/Canada thing.  Most KDE developers are in Europe.\n\nSecond, accepting money provokes a lot more red tape.  I don't really want to have to deal with managing that or determining who needs money.  The whole point of this thing isn't to pay people for their work, but rather just to show some appreciation, namely where it's actually needed."
    author: "Scott Wheeler"
  - subject: "Can I keep him?"
    date: 2003-01-27
    body: "Check out this poorly-drawn comic that just happens to be about adopting KDE users. Weird coincidence:\n\nhttp://pixel.torsion.org/4/"
    author: "witten"
  - subject: "distributed supercomputer"
    date: 2003-01-27
    body: "I remember reading a story less than a year ago about google sponsoring the deployment of a program that you could put on your computer and it would help the computer act as a node in an internet wide supercomputer by using unused cpu cycles on each machine it was installed on.  Then universities could use the resources of the unused cpu cycles to aid in their projects.  Could this be a possibility?  sort of like distcc but internet wide...you could even make one that would work for windows users, that would be ironic. "
    author: "bonito"
  - subject: "Great Idea"
    date: 2003-01-30
    body: "this is a great idea. \ni hope your campaign is crown with success. \nto maintain this i have released an [1]article on a german news-site.\n\ngreets \nar\n\n[1] - http://www.oszine.de/modules.php?name=News&file=article&sid=655"
    author: "ar"
---
There are a lot of KDE developers out there that are students, inbetween jobs or otherwise deprived of monetary resources. Many of these folks overwork their underpowered computing setups to help bring you an outstanding Open Source desktop. There are also appreciative users that are willing to donate obsolete hardware to help the KDE project. In order to bring both parties together and thereby increase the productivity of these KDE developers, I have started the KDE <a href="http://devel-home.kde.org/~wheeler/adopt-a-geek/">Adopt-a-Geek</a> program. If this sounds interesting to you -- either as a developer or donor -- don't hesitate to contact <a href="mailto:adopt-a-geek@kde.org">me</a>.
<!--break-->
