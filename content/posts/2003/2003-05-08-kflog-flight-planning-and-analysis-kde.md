---
title: "KFLog: Flight Planning and Analysis for KDE"
date:    2003-05-08
authors:
  - "asomers"
slug:    kflog-flight-planning-and-analysis-kde
comments:
  - subject: "cool!"
    date: 2003-05-08
    body: "although I suspect the target audience for this software is smaller than usual for KDE programs, KFLog seems to be a REALLY cool and high-quality software.\n\nI just wonder where they got all those maps from!"
    author: "me"
  - subject: "Re: cool!"
    date: 2003-05-08
    body: "That's right, though there are quite a lot of gliderpilots around the world, and especially in Europe. We get the maps from a number of sources. The ground data is taken from the Digital Chart of the World, the terrain data (elevations) is from GTOPO30 and we also have a source for the aeronautical data, but I'm not quiet sure anymore what that was. We have compiled all that data into our own mapformat, and sliced it up into 2x2 degree sections. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: cool!"
    date: 2003-05-08
    body: "The aeronautical data is derived from the 'Digital Aeronautical Flight Information File' (DAFIF). It's a database provided by the american 'National Imagery and Mapping Agency'. These maps are updated monthly."
    author: "Heiner Lamprecht"
  - subject: "Re: cool!"
    date: 2003-05-08
    body: "Are you making money with this software or is everything completely free ? I am under the impression that the software is free and you can pay to have more maps (there is nothing wrong with this, I am just curious).\n\n"
    author: "Philippe Fremy"
  - subject: "Re: cool!"
    date: 2003-05-08
    body: "Nope, everything is completely free! The application is under GPL, and you can download all the maps you need from http://maproom.kflog.org or let KFLog download them for you as soon as you need them. We have them available for the whole world, at no charge whatsoever. But please: don't download them all (almost 2 GB), we have a limited bandwidth and monthly traffic... \nThe PDA programs mentioned are also freely available under GPL.\n"
    author: "Andr\u00e9 Somers"
  - subject: "Re: cool!"
    date: 2003-05-10
    body: "Did you know that public maps were locked behind a proprietary patented format, so that nobody may extract them? I read it in the FFII Call for action."
    author: "Andr\u00e9"
  - subject: "Re: cool!"
    date: 2003-05-11
    body: "Huh? Are you saying that we are making illegal use of the maps? We were not aware of that, and it deserves to be investigated. Still, could you please point me to the source of your wisdom? AFAIK, the maps we use are all in some (known) GIS format. I would like to make sure our data is legal in all ways."
    author: "Andr\u00e9 Somers"
  - subject: "Re: cool!"
    date: 2003-05-15
    body: "The DCW data is Public Domain.  Dont know about the rest, but ANY data released by the US government can only ever be Public Domain, thats the law :-)"
    author: "John"
  - subject: "Wow!"
    date: 2003-05-08
    body: "That's quite an impressive, professional looking application!  Nice work!\n"
    author: "Steven Brown"
  - subject: "Reusing the map part"
    date: 2003-05-08
    body: "This wonderful app interest only a little part of KDE users.\nBut i think the map part would be interesting for a lot of people.\nFurthermore, it seems that data of the entire world is available.\nIs it separable / reusable ?\nIs it planned to make a kpart, a separate app or something like this ?"
    author: "LC"
  - subject: "Re: Reusing the map part"
    date: 2003-05-08
    body: "Right now, there is no lib or other program using the map. We are thinking about some changes in the code-structure and putting all maprelated stuff into a lib, which than can be used by other apps as well. A kpart is also a good idea, we will keep that mind."
    author: "Heiner Lamprecht"
  - subject: "Sounds like a nice app, but..."
    date: 2003-05-08
    body: "...I can't be the only person who read that as  'KFlog' and wondered what possible combination of whips and KDE anyone would want o_O I say this using and loving KDE as my main DE, by the way--I just found the name amusing :P"
    author: "nobody"
  - subject: "Great"
    date: 2003-05-08
    body: "Great job! But I have two remarks:\n1) The maps should be separated in a library. It could be used by other\nKDE applications (anyone writing a krouteplanner?)\n2) The maps of Belgium & Holland are less than 1 MB, so why not make it\npart of the Dutch KDE-translation package? You can do this with other trans-\nlations (Portugese, Danish, ...) too\n"
    author: "anon"
  - subject: "Re: Great"
    date: 2003-05-08
    body: "1) We are thinking about how best to do this. Currently, I don't think the dataquality it good enough for use as a routeplanner though. \n2) All languages are shipped with the normal distribution. That would mean, we would have to ship almost all of europe with the distribution if we used this suggestion. That's quite a few MB's! Since KFLog will automaticly download the maps you need, why ship them with the package? \nBTW: we supply the maps and program on CDROM if needed (for people on slow connections for example)..."
    author: "Andr\u00e9 Somers"
  - subject: "Great Work!"
    date: 2003-05-08
    body: "I really enjoy KFLog! I used it a few days ago to optimize a igc-file for OLC, and it works! I haven't tested the new Filser interface yet, but I hope it works for my SDI-devives, too.\n\nJust the handling could be better. How can I say KFLog that a task I created is finished and I don't want to add more waypoints? And it would be great if the map could be controled using  mouse (for example using one mouse button to move the map and the mouse wheel to handle its width)."
    author: "Felix"
  - subject: "Re: Great Work!"
    date: 2003-05-08
    body: "I agree handling can be improved, and we are working on that as we speak! The next version will see some major improvements on this front.\nEnding creating a task can be done using CTRL+right click. As for the controlling of the map with the mouse: you can zoom in by just selecting a rectangle using the left mousebutton, but you can't zoom out that way (yet). You can re-center the map using the middle mousebutton."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Great Work!"
    date: 2003-05-09
    body: "You are also able to zoom out with the mouse. You just have to drag your mouse out of the map window.\n\n"
    author: "Florian"
  - subject: "Re: Great Work!"
    date: 2003-05-16
    body: "<a href=\"\">.</a>"
    author: "."
  - subject: "A Suggestion"
    date: 2003-05-10
    body: "How about contacting ibiblio.org to store the map data?  There mission is to preserve and distribute Free data, so the map data seems to me to qualify.  That would solve your bandwidth problems."
    author: "John"
---
The KFLog Team is proud to announce the <A HREF="http://www.kflog.org/index.php?lang=en&content=download.php">latest major release</A> of <A HREF="http://www.kflog.org/">KFLog</A>. KFLog (<A HREF="http://www.kflog.org/index.php?lang=en&content=screenshots.php">screenshots</A>) is a flight analyser program aimed at glider pilots and is the only of its kind for Linux to be recognized by the <A HREF="http://www.fai.org/">FAI</A> <A HREF="http://www.fai.org/gliding/"> IGC</A>. Of course, KFLog runs on any KDE platform, not just Linux, and with <A HREF="http://embedded.kflog.org/">KFLog/Embedded</A> and <A HREF="http://cumulus.kflog.org/">Cumulus</A> on <A HREF="http://www.trolltech.com/products/qtopia/index.html">Qtopia</a>/<A HREF="http://handhelds.org/opie/">Opie</a>, even PDAs are supported.  With the introduction of version 2.1.0, KFLog gives glider pilots a powerful tool to plan their flight tasks, <A HREF="http://www.kflog.org/icons/screen5.png">analyse</A> their own flights or gawk at the recorded flights filed in the <A HREF="http://www.segelflugszene.de/floh_ae010114.html">Aerokurier</A> <A HREF="http://www.segelflugszene.de/olcphp/olc-i.php?olc=olc-i">Online Contest</A>. 
 
<!--break-->
<p>
KFLog projects each flight on a digital vectormap that displays interesting objects such as airflields, air structures, roads, cities, rivers, and the elevation map. It is the first flight analyser to supply maps (downloaded automatically) for the whole world, making it suitable for users the globe over.
<p>
Another unique feature of KFLog is its capability to calculate the optimal declaration for the Online Contest. Where other analysers find only an approximation, KFLog delivers the exact answer, thereby maximizing scores. KFLog can <A HREF="http://www.kflog.org/icons/screen8.png">communicate directly</A> with a variety of flight recorders -- support for more loggers and GPS devices is planned.
<p>
Currently, KFLog is available in English, German, Norwegian, French, Italian and Dutch. 