---
title: "KDE at Paris Solutions Linux 2003"
date:    2003-02-17
authors:
  - "cmiramon"
slug:    kde-paris-solutions-linux-2003
comments:
  - subject: "Thanks for the report!"
    date: 2003-02-16
    body: "Very interesting read. We need more people reporting about event related to KDE. =)"
    author: "Datschge"
  - subject: "cool pics of RMS  checking out KDE"
    date: 2003-02-16
    body: "I noticed some guy with a video camera filming\nthe historic occasion . \nThat would be interesting to watch."
    author: "kannister"
  - subject: "Re: cool pics of RMS  checking out KDE"
    date: 2003-02-18
    body: "We are waiting for it.\n\nGerard, one of the guys on the pictures, who organized the tea party."
    author: "gerard"
  - subject: "Thanks for a very lively report"
    date: 2003-02-17
    body: "Thanks for that very lively and interesting report. I would have loved to be there. Well done to G\u00e9rard for invinting RMS and presenting him the latest KDE brew. The French KDE team did very well to promote KDE to the show. I hope Mandrake will survive, it's so nice to have a French based distro. And again a big \"BRAVO\" to the French KDE team.\n\nannma\n"
    author: "annma"
  - subject: "France ain't the U.S"
    date: 2003-02-18
    body: "Check out the topless poster in the background:\nhttp://www.kde-france.org/galerie/expo2003/jour1/fullsize/expo4.htm\nYou're not going to see *that* at the Javits Center.\n\nAlso, is the lady on the left hot, or what?\nhttp://www.kde-france.org/galerie/expo2003/jour1/fullsize/expo13.htm\nAnd she porbably speaks a fluent french, grrrr!"
    author: "AC"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: " http://www.kde-france.org/galerie/expo2003/jour1/fullsize/expo4.htm\n\nThis picture was on traduc.org's booth. It is an organization about translating free software into french.\nThere was a challenge to find the best franch translation for \"root me\".\nThe winner won the picture\n\n;-)"
    author: "gerard"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "For the record, I won the poster thanks to my translation \"Prends les droits !\". Unfortunately, this day I had to leave a bit early and I wasn't able to go and check if I had won. I then met the guy from Traduc.org (Cheyenne) 2 days later at Fosdem, we were in the same youth hostel. He told me that the guy who won never came to get his price. I asked about the translation that won, he replied \"Prends les droits !\". I went : \"That's me, just check the name of the guy on the list, that's me !\". \nHe said he will look into it and send me the poster. Great ! Unfortunately, I was told lately that he forgot it at FOSDEM :( Too bad.... \nThat's one more reason to go back to FOSDEM next year ;)\n\nthibs"
    author: "thibs"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "a) That poster is not topless, don't waste my time ;-)\nb) Pretty girl, yeah, so you are forgiven for the previous item"
    author: "Roberto Alsina"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "<i>a) That poster is not topless, don't waste my time ;-)</i>\n\nYou're surfing dot.kde.org for porn? That's the funniest thing I've heard in ages :-)"
    author: "Dr_LHA"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "Hey, I surf here for the KDE news, but if by chance some promise of porn comes up, who am I to say no! ;-)"
    author: "Roberto Alsina "
  - subject: "Re: France ain't the U.S"
    date: 2003-02-19
    body: "You're a gem Mr. Alsina, an authentic KDE gem.\n\nDon.\n"
    author: "Don Sanders"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-19
    body: "Whoa, sarcasm alert?"
    author: "Roberto Alsina"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-20
    body: "No sarcasm.\n\nI'm not sure how much of what you say I agree with, but you make me think, and laugh.\n\nYour kibitzing for KDE over the years has provided me with countless hours of entertainment.\n\nI've also enjoyed reading your forays into other philosphical areas such as questions surrounding the creation of life, and zygotes.\n\nI sincerely thank you."
    author: "Don Sanders"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-20
    body: "Oh. (blush)"
    author: "Roberto Alsina"
  - subject: "yeah, THAT'S the spirit"
    date: 2003-02-19
    body: "you lol me up, huh..\nciao bella"
    author: "zero08"
  - subject: "Re: yeah, THAT'S the spirit"
    date: 2003-02-19
    body: "Dude, I am a man, I have a beard, and I weigh 100K. I don't think anyone has ever called me pretty, much less \"bella\"! ;-)"
    author: "Roberto Alsina"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "Or maybe he is looking for a pretty, topless geek girl, humm ! that would be really sexy !"
    author: "Patrick"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-19
    body: "Have one of those. Ok, she is not a geek, but I am not racist. And she is not topless most of the time, either. But she is a girl. I am pretty sure of that ;-)"
    author: "Roberto Alsina"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "> You're not going to see *that* at the Javits Center.\n\nHowever, you will see Las Vegas models, dancers, strippers, and showgirls at Comdex, which is the US's largest trade show by far. :>"
    author: "fault"
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "Well, it USED to be the largest. Nowadays it is bankrupt, probably not going to be another one, and the last couple were smaller than The Electronic Consumer Expo, or however it\u00b4s called."
    author: "Roberto Alsina "
  - subject: "Re: France ain't the U.S"
    date: 2003-02-18
    body: "Oui elle est \"bonne\" :) as you would say in french slang, nice air bag too ! "
    author: "Patrick"
---
<a href="http://www.kde-france.org/">KDE-France</a> held a booth at the Solutions Linux 2003 show in Paris recently. For an account of our activities as well as some pictures, be sure to check out <A href="http://www.kde-france.org/article.php3?id_article=98">our report</A>. Interesting items include: details of the team, promoting KDE with a <a href="http://www.knoppix.net/docs/index.php/KnoppixKDE">remastered</a> version of <a href="http://www.knoppix.net">Knoppix</a>, Richard Stallman's hour-long visit to the booth and test-drive of KDE 3.1, an analysis of our userbase and more.
<!--break-->
