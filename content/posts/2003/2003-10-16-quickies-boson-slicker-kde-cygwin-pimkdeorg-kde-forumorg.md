---
title: "Quickies: Boson, Slicker, KDE on Cygwin, pim.kde.org, KDE-Forum.org"
date:    2003-10-16
authors:
  - "binner"
slug:    quickies-boson-slicker-kde-cygwin-pimkdeorg-kde-forumorg
comments:
  - subject: "go slicker"
    date: 2003-10-16
    body: "Some of the ideas presented on that slicker site look really innovative.  The card based clipboard really sticks out in my mind.  That is such a natural representation for having a multi item clipboard I am surprised that I have not seen it done before.  It would especially excel for having items such as images on the clipboard.\n\nI am glad to see the work done to make sure slicker has a well thought out api.  I would love to see slicker merged into kde for 4.0.\n"
    author: "theorz"
  - subject: "Re: go slicker"
    date: 2003-10-17
    body: "I would also love to see cards to be used on other thigs as well, like at the point k3b might get integrated to the system it would show the status of the burn on a card, also audio players could use a card to show off and generally any program that has a updating (live) status screen that does not need user input (frequently) should have an user-selectable (via radiobutton on config) option for showing status on a card and/or using the same cards-system used in the konqueror side-bar.\n"
    author: "Nobody"
  - subject: "Re: go slicker"
    date: 2003-10-17
    body: "Hi,\nI'm a member of the slicker group, not a developer, more of a dweller/idea person. The re-design of teh core is going well AFAIK, but slicker will be shipping with only a default set of cards (To be decided), we are thinking of creating a second project for cards (so cards and slicker-core don't get confused, and so core developers can work more efficiently) and any cards/ideas will be very welcome. If you have anymore questions regarding slickere, either mail tehm to the ML or join #slicker on freenode.\n\nBut thanks for all the nice comments and keep a look out for slicker in future, we're back!\n\n\n"
    author: "cmf"
  - subject: "kde on cygwin : what a shame !"
    date: 2003-10-16
    body: ".. and still no kde on directfb yet."
    author: "ita"
  - subject: "Re: kde on cygwin : what a shame !"
    date: 2003-10-16
    body: "you are invited to join to the native QT3 port project, which is over 90% complete. Currently the main task is to migrate from qt3.1 to qt 3.2.1. \n\nSee http://cygwin.kde.org/qt3-win32/ for more informations. \n\nRalf "
    author: "Ralf Habacker"
  - subject: "Re: kde on cygwin : what a shame !"
    date: 2003-10-16
    body: "Nice work. I'm looking forward it to when I start programming with C++ (should be until the end of the year). \nIt will be great to develop GPL programs and being able to run them both on linux and windows."
    author: "protoman"
  - subject: "win32 != directfb"
    date: 2003-10-17
    body: "win32 does not equal directfb, unless I'm missing something here."
    author: "Ian Monroe"
  - subject: "Re: win32 != directfb"
    date: 2003-10-17
    body: "I think the original author implies that removing the X11 dependency, directfb\nsupport would be automatic."
    author: "luciano"
  - subject: "Re: kde on cygwin : what a shame !"
    date: 2003-10-17
    body: "shame on you - this is just undermining the fundamentals we are all relying on.\n"
    author: "hoernerfranz"
  - subject: "Re: kde on cygwin : what a shame !"
    date: 2003-10-18
    body: "Looks like it's already underway.  http://qt-directfb.sourceforge.net/"
    author: "ac"
  - subject: "Re: kde on cygwin : what a shame !"
    date: 2003-10-19
    body: "It looks like they stopped developing it. Last change in CVS is from 7 weeks ago..."
    author: "Andy Goossens"
  - subject: "Found 800 articles, 38575 comments."
    date: 2003-10-16
    body: "It's a bit off, but meanwhile KDE Dot News is at 800 articles and almost 40000 comments.   Whee!  :-)"
    author: "Navindra Umanee"
  - subject: "Just one or two questions"
    date: 2003-10-17
    body: "Is anyone really using KDE on Cygwin? Shame on me, but I had much trouble installing it (and it is kind of slow, of course). Unfortunately it is not as easy as installing any other cygwin package.\nWhy is that?"
    author: "Jay Pee"
  - subject: "Re: Just one or two questions"
    date: 2003-10-19
    body: "True, Cygwin is difficult too install. Why? Maybe because RedHat leads the development :-)\n\nOn the other hand, Holger Schroeder ported the Gentoo Portage system to Cygwin. It is very experimental but it does the job..."
    author: "Andy Goossens"
  - subject: "Re: Just one or two questions"
    date: 2003-10-25
    body: "Because the cygwin installer lacks of batch processing featuer which would make is easier to integrate it into the KDE-cygwin installer. \n\nCurrently there are some discussions on the cygwin-apps list (http://www.cygwin.com/ml/cygwin-apps) how to do this. \n\n"
    author: "Ralf Habacker"
  - subject: "Buggery!"
    date: 2003-10-19
    body: "The e-mail address slicker@leinir.dk now exists and is open for your Slicker logo competition submissions. The e-mail address did not exist, and I had just changed the rules for recieving e-mail. But that has been fixed, so send away people!"
    author: "Dan Leinir Turthra Jensen"
---
In other news, some until now unpublished submissions: The team of the strategy game <a href=http://boson.eu.org/>Boson</a> has released <a href=http://boson.eu.org/movies.php>two movie files</a> showing their development progress. The Kicker alternative 
<a href="http://slicker.sourceforge.net/">Slicker</a> <a href="http://sourceforge.net/forum/forum.php?forum_id=317274">gets redesigned</a> and its team started a <a href="http://slicker.sourceforge.net/contest.php">logo contest</a>.
The <a href="http://kde-cygwin.sourceforge.net/">KDE on Cygwin</a> project announced that both <a href="http://sourceforge.net/forum/forum.php?forum_id=318425">KDevelop 3.0 Alpha 7</a> and <a href="http://sourceforge.net/forum/forum.php?forum_id=318424">Quanta 3.2 Alpha 2</a> packages for KDE 3.1.1 on Cygwin are now <a href="http://sourceforge.net/project/showfiles.php?group_id=27249">available for download</a>. The <a href="http://pim.kde.org/">pim.kde.org</a> site gained a page describing the command line program <a href="http://pim.kde.org/components/konsolekalendar.php">KonsoleKalendar</a>.
Finally, the <a href="http://www.kde-forum.org/">KDE-Forum.org</a> recently passed the 1500 users and 5000 posts milestones.
<!--break-->
