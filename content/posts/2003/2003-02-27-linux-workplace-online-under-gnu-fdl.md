---
title: "\"Linux in the Workplace\" Online Under GNU FDL"
date:    2003-02-27
authors:
  - "binner"
slug:    linux-workplace-online-under-gnu-fdl
comments:
  - subject: "Source material and license"
    date: 2003-02-26
    body: "First, are they releasing the source format (i.e., LaTeX, DocBook, OOo, whatever)? As already pointed out, the HTML version looks awful and the PDF version is difficult to edit. Having it in its original format would be nice.\n\nSecond, I notice that the original copyright is still preserved, namely, \"No part of this work may be reproduced or transmitted in any form or by any means, electronic or mechanical, including photocopying, recording, or by any information storage or retrieval system, without the prior written permission of the copyright owner and the publisher.\" That part should be taken out if it is indeed under the GFDL."
    author: "Rodion"
  - subject: "Re: Source material and license"
    date: 2003-02-27
    body: "I agree completely. Heck, the source material alone is a wealth of learning material. Especially if they used LaTeX. TeX files are great study materials when you are learning LaTeX :)\n\nAs for the copyright notice, yeah, that needs to be changed too."
    author: "coolvibe"
  - subject: "Re: Source material and license"
    date: 2003-02-27
    body: "The pdf info says Creator : QuarkXPress 4.11/Destiller 4.05 for Mac"
    author: "OJ"
  - subject: "Re: Source material and license"
    date: 2003-02-27
    body: "Right. They usually do PDFs from Quark to send it to the author(s) for review."
    author: "Stephan Richter"
  - subject: "sad place !"
    date: 2003-02-27
    body: "Why are the other comments being deleted here ? Is KDE going the censorship way now ? All comments they don't like are getting deleted ? Shame on you !"
    author: "AC"
  - subject: "Re: sad place !"
    date: 2003-02-27
    body: "Yes.  Take it to <a href=\"http://www.kde-forum.org/\">www.kde-forum.org</a> where perhaps it belongs.  Beware that they're modded though, so it won't be as easy to troll there."
    author: "Navindra Umanee"
  - subject: "Re: sad place !"
    date: 2003-02-27
    body: "Just so you don't just get complaints, I'll toss in a big thank you to Navindra for keeping the Dot on topic and in focus by removing trolls.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: sad place !"
    date: 2003-02-27
    body: "Comments that aren't related to the article in question are being removed in some cases, yes. You are of course completly free to put as much inflamatory drivel as you like on your own website.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: sad place !"
    date: 2003-02-27
    body: "Maybe because they were offtopic? Try posting things related to the article.\n\n> Is KDE going the censorship way now ? \n\nuh, moderation has been here for a long time. "
    author: "user"
  - subject: "OT"
    date: 2003-02-27
    body: "congratulations for the new kde mainpage it really owns."
    author: ":)"
  - subject: "Re: OT"
    date: 2003-02-27
    body: "I assume you mean other than the fact the page contains js errors, the sidebar box titles don't match the height of the other blue section and that the list of developers is empty. On the other hand it is nice to see some effort being put towards updating the site.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "New homepage (Off-topic)"
    date: 2003-02-27
    body: "Ok. Hope this won't get deleted even if it's off-topic,\nbut I just wanted to say: Congratulations to the\nnew look of kde.org. This is really awesome.\nThe dragon was cute, but this looks so much more professional.\nI think its extremely important for the KDE project in general\nto make a professional impression to become more accepted\nby companies. The new look of KDE.org is definitively the\nright way to go.\n"
    author: "Damon"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-02-27
    body: "Definitely a step in the right direction. However, I think more sober colors would be better, and black on white is always more readable. For hyperlinks though, I suggest they make the colors darker, i.e. approximate black more, sort of like at Redhat.com, and maybe try to keep the side bar/menu shorter.\n\nGood work though."
    author: "Maynard"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-02-28
    body: "Yep, the colors are too flashy. Also the fonts are too large at least here. The dot font size is much more appropriate.\n\n"
    author: "Anonymous"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-02-28
    body: "The same with Netscape 6 on windows. Fonts are huge so you need a very large (wide) window to avoid stupid linebreaks."
    author: "me"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-02-28
    body: "\"The dragon was cute, but this looks so much more professional.\"\n\nYou sad sad creature, they removed konqui and you applaud? Yes it looks nice, but at what cost? Is this the right way to go? Despizing the ones who were always by your side? I think not, many people think not, and they are sad and disgusted by this kind of attitude. Was it because he smiled in a world that's always gray? Was it because is green skin color was not tolerated in a blue kde world. Yes, it looks more professional in deed, congratilations. I for one, will miss our cute loyal dragon, though. \n\nK.F.\nP.S.: No, it was not Kate who wrote this."
    author: "Konqui Fan"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-02-28
    body: "It was because he reminded everyone of Barney. I'd like to rid the logout dialogue of this ill-conceived graphic as well.\n"
    author: "MC"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-02-28
    body: "Oh, I see.\nWho is Barney, I have never heard of him?\n(And who is \"everyone\"?)\n\nKonqi rules, proffesionalism sucks!\n"
    author: "reihal"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-03-02
    body: "Everyone is me, obviously. And Barney is a Satan in a purple dinosaur costume (and subject to one of the most famous Doom hacks).\n\nI maintain that this \"Konqi\" is an aberration, regardless of the level of \"professionalism\" you aspire to. The Gnome foot is a good example of design, as is the Debian logo and Mandrake's.\n"
    author: "MC"
  - subject: "Re: New homepage (Off-topic)"
    date: 2003-03-03
    body: "\"gnome foot is a good example of design\"\n\nYeah, right.\nA foot in the face is not my idea of good design.\nLets say that this is a matter of taste, and tastes vary wildly.\n\nKonqi rules forever!"
    author: "reihal"
  - subject: "Web site"
    date: 2003-02-28
    body: "The new web site style sheet is cool !"
    author: "socialEcologist"
  - subject: "Typical linux/KDE document."
    date: 2003-03-01
    body: "Am I the only one that thinks these two \"publications\" epitomize the problem with most documentation on/about Linux/KDE/etc.\n\nThe PDF and HTML version are split into multiple files. (You thought you could hit ctrl-f and find something, think again!)\nThe individual files are named via some incomprehensible algorithm. (I'm giving the author the benefit of the doubt here that these wern't the result of his cat playing on the keyboard.)\n\nThe PDF and HTML versions neither one have links from toc/index into the body of the work. (Let's see, hmmm, page 123 is about ~what~ chapter, no wait, the next one. What's the name of that file again \"7_4\" or is it \"4_9\"?  What was I looking for again?  Oh yeah, good documentation.)\n\nI'd be better off if, after downloading the html version, I spent the time to edit the thing and create a single document with a proper toc/hyperlinks/index/etc.  It might only take me a day but, geesh, you'd think that it wouldn't be necessary.\n\nAnd some folks wonder why everyone isn't using KDE/linux.  The reason isn't so much that there isn't any documentation, it's just so damned hard to use."
    author: "jfb3"
  - subject: "Re: Typical linux/KDE document."
    date: 2003-03-03
    body: "Keep whining about stuff you get for free.  This was probably just taken from the author's source document and turned into PDF/HTML.  Go buy the book if you want to flip around easily."
    author: "AC"
  - subject: "Re: Typical linux/KDE document."
    date: 2003-03-03
    body: "Keep whining about stuff you get for free.  This was probably just taken from the author's source document and turned into PDF/HTML.  Go buy the book if you want to flip around easily."
    author: "AC"
---
The <a href="http://www.nostarch.com/litw.htm">"Linux in the Workplace"</a>  book by <a href="http://www.ssc.com/">SSC</a>, publisher of <a href="http://www.linuxjournal.com/">Linux Journal</a>, introduces Linux users to the desktop capabilities of Linux with the KDE 3 desktop. It includes information on how to use email and surf the Internet, perform general office-related tasks, work with the command line and much more. While being available for purchase in traditional <a href="http://www.nostarch.com/litw.htm">printed version</a>, all 400 pages have been released under the <a href="http://www.gnu.org/licenses/fdl.html">GNU Free Documentation License</a> and the full text of the book is available online in <a href="http://linuxpip.org/">HTML and PDF versions</a>.  <i>Editor's note:  the HTML version is rather less than ideal, but the PDF version is pretty much identical to the printed book.</i>
<!--break-->
