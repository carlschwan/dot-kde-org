---
title: "KDE Developer's Corner: Using KConfig XT"
date:    2003-11-10
authors:
  - "zrusin"
slug:    kde-developers-corner-using-kconfig-xt
comments:
  - subject: "How about this..."
    date: 2003-11-10
    body: "This is VERY interesting.  I really like it.\n\nHere's a thought (if it hasn't been brought up already.  if it has, please flame on)\n\nMake a KCM module that allows the user to interact with his/her /etc/ directory, using a bunch of these .kcfg files that define cetrain config files like fstab and such\n\nit would have to have a different set for each distro, but much could be shared.\n\nmmm...  gentoo configuration without CLI.... mmm......"
    author: "standsolid"
  - subject: "Re: How about this..."
    date: 2003-11-10
    body: "I started work on a similar project 2 years ago, but ran out of time. A couple of things that I was looking at in the design was 3 levels of xml.\n1) for an interface to the system, using either file description, cli, or language; by definition, this was language and platform specific.\n2) an object description to serve as an interface between 1 and 3. This allowed for a clean seperation of function vs display. In essence, a level of redirection.\n3) a graphic description, that was not kde specific, but allowed for extensions for kde or gnome specific.\n\nThis was purposly 3 levels so that I could enable for a generation of a remote system, i.e. generating level 2 as RPC, corba, soap, etc. \n\n\nOne of the biggest problems for *nix in general, was lack of easy standard config and sadly, it has not changed. In addition, I felt that any config solution would be useful if it was easy to generate KDE, GNOME, webmin, closed solutions (such as ca's stuff, etc). This would also encourage tool developers to write to the specs so that they had the widest possible audience.\n\n\nWithout checking through all of this (tutorials are good starts but do not show everything), it appears that you also came up with a similiar solution that I first had, but found limiting. But I am glad to see you on a similar path."
    author: "a.c."
  - subject: "Re: How about this..."
    date: 2003-11-10
    body: ":: One of the biggest problems for *nix in general, was lack of easy standard config and sadly, it has not changed.\n\nIf what I've read is accurate, Apple has done something along the lines of what you are talking about with OSX.  It might be good to see if their method is standardized and at least documented and work from there."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: How about this..."
    date: 2003-11-11
    body: "well, I become tired with the constant complaints of friends saying \"Linux is hard to use\".  It's not because of the interface (KDE has an EXTREMELY good UI), but because it's not what they are used to (ease of configuration via something like hte control panel).  \n\nIMHO (no distro wars please) gentoo is the best OS ever created, but has a huge learning curve (especially when a user is coming from windows).  I am currently developing a couple GUI tools to change this (gentoo-specific).  I am not satisfide one bit with tools other distros give to configure the system.  I have tried most of the major distros and he closest solution I saw was suse's yast2.    \n\nI have not set a plan out yet on making the tool to replace something like \"mandrake control center\", etc -- but kconfig XT might be the glue that takes me there.\n\nto check out the tools I'm making, I post to the gentoo forums about updates and what not\nhttp://forums.gentoo.org/viewtopic.php?t=103540\nis for my first tool."
    author: "standsolid"
  - subject: "Better way"
    date: 2003-11-10
    body: "That's not the solution. A real solution would be the shift to xml for the config files itself. Doing it this way there would be one syntax for all configs, the configs would remain in seperable files (no registry pain) but could be inter-linked. So you could browse though your configuration."
    author: "Ruediger Knoerig"
  - subject: "Look at e-smith.org ..."
    date: 2003-11-11
    body: "They just create a parallel system that writes to the original legacy config files ... easier than changing the whole app.\n\nhttp://www.e-smith.org/\n\nCould probably do the same thing in XML and create a GUI to go with it"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Better way"
    date: 2003-11-11
    body: "Yes, the lack of standards for config files is very frustrating, but a dual file solution requires just as much maintenance (only more modular), and will lead to the 'lost update' problem. Plus, calling non XML files legacy will be an unpopular thing to do; I much prefer the format of most config files as they are right now since they are easier to manually edit, and I'm a huge XML fan.\n\nThe best solution I can personally think of is to look at what we have out in the wild right now, and create standard(s) that allows the majority of existing config files to remain unchanged, or with mimimal 'nip and tuck' -- a sort of best-fit aproximation. Provided that a canonical XML binding is possible for each config variant, then we still get all of XMLs advantages.\n\nThis would be a major win for everyone involved. Daemon writers could drop the burden of maintaining a parser (if they wanted), tools could configure services reliably when they have prior knowledge of the schema being used, advanced text editors could provide visual feedback as to which commands were allowed and whether any errors had been made (for any file that has a schema), and the syntax of various config files would be easier to keep in your head at any one time."
    author: "Dominic Chambers"
  - subject: "Re: Better way"
    date: 2003-11-11
    body: "There is an initiative going on that aims at fisint this.  Take a look at http://www.lysator.liu.se/~astrand/projects/hiveconf/ and see that many of the issues of the configuration file mess are already being addressed.\n\nThe goal of Hiveconf is to create a configuration framework for all of UNIX with a unified file format for configuration files.  Peter \u00c5strand, the guy who started this, is chief developer of Cendio Systems AB and a very experienced person in these matters. \n\nRight now there is a reference implementation of the API and a tool to handle configuration files written in Python. There is also a presentation that compares the current stat of things, Hiveconf and the windows registry as well as outlines the goals of the project. I am currently working on a C version of the library.\n\nThings are in the later stages of design right now, so if you are interested in this and have things to say, then please come to the mailing list and state them. We would love to know where we have taken the wrong path if we have.\n\nThe nice thing for KDE developers with Hiveconf is that it uses the same format as the KDE configuration files.  Other programs with the same format are Samba, and ThinLinc, a commercial thin client solution for Linux.\n\nInge Wallin"
    author: "Inge Wallin"
  - subject: "Re: Better way"
    date: 2003-11-11
    body: "I looked very briefly and it seems you are treating config data as key-value pairs. That is to restrictive I think. Hierarchical config data is not uncommon, and that is very messy to translate if you only have key-value pairs."
    author: "Claes"
  - subject: "Re: Better way"
    date: 2003-11-11
    body: "It also offers a folder model so that hierarchical config data can be easily represented and stored."
    author: "Inge Wallin"
  - subject: "Freedeskop.org"
    date: 2003-11-10
    body: "How about making non-kde bindings for this, and submitting to freedeskop.org?\nSomething like this really should be desktop indendant.  (I for one would love say apache etc to use the same framework as everything else)"
    author: "JohnFlux"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-10
    body: "It certainly sounds like something that could easily be made none KDE specific for all to use. Whether freedesktop.org would accept it is another matter, however, but one can always try :).\n\nHow does this whole thing compare to something like XUL?\n\nAlthough I like XML and have used it in many situations, I am always cautious about it being used and advocated as a panacea for everything. This looks quite nice however, and I give this framework a warm welcome. With this extension the KDE development framework is certainly better and streaking ahead of anything I have seen in any other environment such as within Gnome, or especially concerning Mono. However, that's my opinion. I agree with previous posts that this could be the beginnings of a real interoperability boon.\n"
    author: "David"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "Well, for that matter, what's wrong with re-using GConf?  It's already stable, and very well tested.  Why not just build KDE bindings for it?"
    author: "jck"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "Um, well I'm guessing nobody wants a Windows-like registry in KDE...  From my experiences with gconf, I would agree."
    author: "anon"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "Add to that gconf has CORBA, glib, gmodule, oaf dependencies and you have a more convincing argument especially when KDE already has a config system that pretty much works."
    author: "anon"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "It's a common myth that gconf is like the windows registry- it is as much like the registry as kconfig is.  Gconfedit has a interface that's somewhat like Window's regedit (and kde 3.3's/kdenonbeta/kconfedit), but the similarities end there.\n\nlike kconfig, gconf keeps it's settings in seperate file - unlike the registry\nlike kconfig, gconf has pluggable backends (gconf uses xml, and kconfig uses ini files) - unlike the registry\nlike kconfigXT, almost all gconf keys are documented\nunlike the registry, NO binary data is allowed by convention in gconf (there is some in kconfig files, but rarely)\nunlike kconfig and the registry, gconf handles notification as well, which makes things like instant apply easier to implement.\n\nHowever, it is indeed unreasonable for KDE to use gconf (which is currently reliant on a full host of dependencies, including a CORBA ORB), in the same way that it's unreasonable for the GNOME folks to use something like DCOP (which currently is reliant on Qt..) "
    author: "fault"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "Thanks for the excellent reply.  It's a shame, but yeah, it does look like it'd be unreasonable for KDE to adopt it...  I'm not wholly convinced that a Glib dependency would be too awful, but adding ORBit2 and oaf would just be too much.  Ah, well.  Maybe someday we'll acheive configuration nirvana. :)"
    author: "jck"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "Yeah, orbit2 may become a dependency of (most installations of) kde4 (and more precisely, qt4 with at-spi support)."
    author: "anon"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "No, I believe Qt will optionally use ATK, not ATSPI."
    author: "anon"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-11
    body: "Ah, ok.. atk does seem to have much lighter dependencies than ATSPI does (only glib)"
    author: "anon"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-12
    body: "You missed two important points:\n\nlike registry, gconf has a rather arbitrary structure, especially the resulting XML files are not at all human readable as clear text due to adding quite some \"unrelated text\" (for example \"<entry name=\"blabla\" type=\"string\"><stringvalue>bla^2</stringvalue></entry>\" instead a simple ini style \"blabla=bla^2\").\n\nunlike kconfig but like registry, gconf is not advertised as a transparent middleware which should only matter to developers, but instead it is advertised as dumping ground for feature options a program wants to offer but is, for reasons unknown, not willing to give the user a GUI for (ironically most of those programs are GUI programs itself, making the whole GUI ploy a farce).\n\nThe philosophical approach of kconfig and gconf are so different that it is imo really no wonder why the latter is rather despised by those who prefer the former (like I do)."
    author: "Datschge"
  - subject: "Re: Freedeskop.org"
    date: 2003-11-13
    body: "Maybe a way that devfs is replaced with odev/sysfs could be made for the whole /etc?\n\nBy that I mean maybe there could be a \"oetc\" that would use /sys/<some config dir> as a interface for a /etc, but be strictly XML-based and of course the /sys/<?> would be linked to a XML-config file somewhere in the filesystem (/var)?\n\nThat way all programs using old /etc-formats could access the /etc just as in the past, but the uetc would map program's config file via the uetc that would just point the virtual filesystem on the /sys to give out old-style config file...\n\nPrograms that would use the new /sys/<?>-interface would benefit by eliminating the uetc-layer altogether and could use the XML-style for everything needed.\n\nSure this kind of change is HUGE and is out of KDE's work and would need to be done on a kernewl-level of atleast freedesktop-level, uetc would of course be a user-space program jusr like udev is...\n\nJust a thought to be processed via people reaaly knowing enought to see is there any idea worth keeping... ;-)\n"
    author: "Nobody"
  - subject: "How about internationalization ?"
    date: 2003-11-10
    body: "Maybe that\u00b4s a stupid question, but is it possible to create internationalizable configs ? \nWouldn\u00b4t that be important, as configuration is a big part of the interface?"
    author: "Leonardo Peixoto"
  - subject: "Re: How about internationalization ?"
    date: 2003-11-10
    body: "No, not really, this level is behind the scene.  The config files stored in /etc, not the dialog with buttons and sliders that is presented to the users.  Those are translated like normal.\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "what about database-like features?"
    date: 2003-11-10
    body: "Cun we have a config system with really powerful features?  I particularly want transactions.  I want to be able to submit several updates to the configuration engine, and have them either all go in or none, so as to maintain consistency.  I want real rollback, and versioning.  Pretty please?"
    author: "benploni"
  - subject: "Re: what about database-like features?"
    date: 2003-11-10
    body: "Looks like you need use KexiDB for that (with SQLite file-based engine, in simpler case, with network SQL engine in multiaccess case). This could offer you transaction support, although is not (yet?) intergrated with KSyCoCa & friends.\n\nI've told about this because it's just potentially possible..."
    author: "Jaroslaw Staniek"
  - subject: "Re: what about database-like features?"
    date: 2003-11-10
    body: "I think i could safely say we will never see something as lean and mean as ksyscoca tied to a very slow sql database... granted ksyscoca has absolutely nothing to do with kconfig thats another issue.  \n\nPersonally I hope to see a central daemon for system settings in KDE 4 but from my early tests it seems going out of process to get config info hits the machine pretty hard, unless you have dual CPUs...  Another approach is shared memory... but i the torture there is not worth rehashing...  truthfully the only way the current lightning fast kconfig can get any faster is do use maby db4, at wich point we can get things like transactions... but then if the user has an NFS mounted home... kiss your data integretry good bye...\n\nso is life, sometimes the best solution is the simplest one..."
    author: "Ian Reinhart Geiser "
  - subject: "Re: what about database-like features?"
    date: 2003-11-11
    body: "GNOME uses gconf though.  That's an out-of-process config server right.  Why aren't they seeing the performance penalty hit you claim?"
    author: "anon"
  - subject: "Re: what about database-like features?"
    date: 2003-11-11
    body: "id be amused to see what your benchmarks actually say... "
    author: "Ian Reinhart Geiser"
  - subject: "Re: what about database-like features?"
    date: 2003-11-11
    body: "We don't even have to look at benchmarks.  \n\nIn your benchmark you find that 2 CPUs would be needed if KDE switches to a config daemon.  Empirically, we already know that most desktops are single CPU.  We already know that these desktops use both KDE and GNOME successfully.  Another thing we know is that GNOME uses a config daemon and there are a fraction of complaints about GNOME speed compared to KDE speed (which is always getting better.  i'm not bashing KDE speed.  I use KDE on my celeron 400 and *never* have the speed problems some people with 2 GHz machines pretend they have.  it runs fast for me.)\n\nI would conclude that either there is something wrong with your benchmark or your config server..."
    author: "anon"
  - subject: "Re: what about database-like features?"
    date: 2003-11-11
    body: "id look at the benchmarks... you would be shocked ;)\n\npeople who think kde startup is slow because of KConfig are either on a 80x386 or on to much crack.  what i was getting at is by going out of process you seem to make it slower.  and what do you gain?  not much, especially when kconfig is fast enough to just reparse... yeah you learn the darndest things when you actually research a problem :)\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Check your whole /etc into CVS RCS or ... monotone"
    date: 2003-11-11
    body: ".... or better yet ... keep it all in a Zope instance as a filesystem object!"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Check your whole /etc into CVS RCS or ... monotone"
    date: 2003-11-11
    body: "Well, actually ZODB would be sufficient to do just that (and pretty neat).\nAnd adding some ZEO to the mix has great potential, too."
    author: "anonymous"
  - subject: "KDE towards GNOME"
    date: 2003-11-11
    body: "Finally you people made it up to compy more stuff from GNOME than innovating on your own. As sad as this sounds but KDE totally lost individuality. Not just that you are also moving towards using more GNOME components in your framework.\n\nWhere is the individuality of KDE these days ? ARTSD uses glib which requires pkgconfig, other components require libxml, libxslt2, libart_lgpl. All stuff usually found and belong to GNOME. Now you started using KConfig which IS GConf but sold as something differently.\n\nWhat next ? Button reordering ? Switching from qt-copy to GTK+ ? If I need to add and use so much GNOME stuff then wouldn't it be better switching entirely to GNOME ?\n\nSorry folks, as much as I respect your work but you totally lost here. There is more GNOME in KDE than KDE in GNOME."
    author: "Sven Fischer"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-11
    body: "You don't know what you're talking about. KConfig already exists since 1997 (that is, GNOME 1.0's birth is March 1999). This KConfig XT stuff is only (sort of) improvement over the API, nothing is dramatically changed. \nAnd the other libraries you mentioned aren't tighted to GNOME as well, check the fact by peeking at libxml's source code for example."
    author: "anon"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-11
    body: "> As sad as this sounds but KDE totally lost individuality.\n\nAnd now that Apple adopted khtml KDE even lost its browser rendering engine individuality, eh?\n\n> Where is the individuality of KDE these days ? All stuff usually found and belong to GNOME.\n\nAssuming you would be right with this statement, what would be wrong with picking up the good stuff from GNOME while skipping the by majority bad stuff?\n\n> If I need to add and use so much GNOME stuff then wouldn't it be better switching entirely to GNOME ?\n\nYou're trolling. Oh, and don't forget to tell GNOME developers to skip Valgrind for debugging just because it happen to be hosted at KDE.\n "
    author: "Anonymous"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-11
    body: "> Now you started using KConfig which IS GConf but sold as something differently.\n\nErm, do you have __any__ idea of what you are talking about? kconfig has been around since KDE 1.0. It just was never publicized unlike gconf. They are fundamentally differnent things too.\n\nHere is a page from kde 1.1's API docs:\n\nhttp://developer.kde.org/documentation/library/1.1-api/classref/kdecore/KConfigBase.html"
    author: "fault"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-11
    body: "So what, if it's good-stuff why shouldn't 'we' adopt it?\n\nAnd, isn't growing a bit towards each other what both camps want noadays? \n\n\n\nyou may call it my .sig ;-) :\n\nMy list of things that might need a reimplementation/unification in desktop UN*X:\n - the filesystem-tree (I quite like GoboLinux experiment)\n - the boot scripts (go multithreading, go python; see SystemServices)\n - configuration files (I think XML reads to cluttered for this, although it would suit the job)\n - the package management, one to rule ?m all! (the best of deb and portage)\n - X and the framebuffer should play very nice with eachother (for a sexy boot)\n - one sound system (JACK looks promising)\n - one print..sy.t.m.... allready CUPS!\n - one documentation format/place (with working links please)\n"
    author: "cies"
  - subject: "Just another stuck up troll!!!!!"
    date: 2003-11-11
    body: "KDE has innovated most of the features present in Linux's modern desktop environmments, it has not lost its individuality, in fact it is becoming more unique especially now since unlike GNOME which sacrifices usefullnes for usabiltiy, KDE is trying to strike a balance.\n\nThere is nothing wrong with using GNOME technologies in KDE or vice versa, this would actually be desirable, this would mean that the desktops would be more integrated. As long as the technology is good, it does what you had in mind also, than by all means use it. It is stupid to bite the hand that feeds you something good juse because you've got too much pride to accept it, we should be working to create a better and more unified desktop environment, not competing with ourselves in these ways. Currently, KDE does not use GNOME technologies for technical reasons, as explained before.\n\nHowever, it would be stupid to ignore a good GNOME technology simply because it is a GNOME technology, that slows down Linux on the desktop and in fact it would slow down KDE's adoption too since it would now need to reinvent the wheel and maintain its own technology. Argue against a GNOME technology for technical reasons but not for pride, that is not going to help."
    author: "Alex"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-11
    body: "> libxml, libxslt2, libart_lgpl\n\nThese do not 'belong' to Gnome.\n\n> Switching from qt-copy to GTK+\n\nGTK is a horrible toolkit and its API and structure really cannot compare to something like Qt.\n\n> If I need to add and use so much GNOME stuff then wouldn't it be better switching entirely to GNOME ?\n\nNice try :). Well, I suppose you can dream."
    author: "David"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-18
    body: ">> GTK is a horrible toolkit and its API and structure really cannot compare to something like Qt.\n\nYou're exaggarating here.  While I agree that Qt is nicer in many aspects, Gtk+ is not that bad.\n\n* Gtk+ is C, which is more language-friendly (easier to bind to more languages).\n* Gtk+ is more portable (As free on Windows as anywhere else).\n* Gtk+'s widget class hierarchy was (at least up to Qt 2 which I knew well) almost identical to Qt's widget class hierarchy and with almost identical API's.  The Gtk+ requirements for ugly cast macros was purely syntatic and inessential. The differences in architecture (having layout widgets instead of attaching a \"layout engine\" to each widget, having a special window frame widget rather than having every widget support rendering as a frame) are not clearly \"better\" or \"worse\"."
    author: "Peaker"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-18
    body: "GTK's so-called language-friendliness has been termed a failure by Miguel himself.  Anyway KDE/Qt have C bindings, Python bindings, Perl bindings, Ruby Bindings, Java bindings, Objective C bindings, JavaScript bindings.  And just like GTK, almost none of the bindings except maybe for Python, Perl and JavaScript are used all that much. As for GTK being more portable...  that's a somewhat silly claim already.\n\n"
    author: "anonymous"
  - subject: "Re: KDE towards GNOME"
    date: 2003-11-11
    body: "ARTSD uses glib which requires pkgconfig\n>>>>>>>>>\nWhich is a freedesktop.org project. KDE and GNOME both using it is kind of the whole point!\n\nother components require libxml libxslt2\n>>>>>>>>>>\nThese are totally independent libraries that were developed for GNOME, but are not tied into GNOME in any way. XML parsing is XML parsing. Its not rocket science. No need to reinvent the wheel when the existing solution is good enough.\n\nlibart_lgpl\n>>>>>>>>>>>\nLibArt was originally developed by Ralph Levine and had absolutely nothing to do with GNOME. GNOME started using it (and hosting it) because it was the best solution available, which is the same reason KDE starting using it.\n\nNow you started using KConfig which IS GConf but sold as something differently.\n>>>>>>>>>>>>>\nKConfig has been around since 1997. KConfig XT is just an XT-ension of KConfig. "
    author: "Rayiner H."
  - subject: "Kconfig"
    date: 2003-11-14
    body: "How does this tool compare with yast?"
    author: "Gerd"
  - subject: "Re: Kconfig"
    date: 2003-11-15
    body: "Complete different stuff."
    author: "Anonymous"
---
As some of you may know, KDE 3.2 will introduce a heavily improved configuration framework, known as KConfig XT. This new framework extends, not deprecates our current configuration API. To help developers understand KConfig XT I have created <a href="http://developer.kde.org/documentation/tutorials/kconfigxt/kconfigxt.html">a short tutorial</a> (<a href="http://www.automatix.de/~zack/kconfig_tutorial.ps">ps</a>, <a href="http://www.automatix.de/~zack/kconfig_tutorial.kwd">kwd</a>) available on <a href="http://developer.kde.org/">developer.kde.org</a>. The tutorial goes over all the basic concepts of the new framework and hopefully will make your life a little easier.
<!--break-->
