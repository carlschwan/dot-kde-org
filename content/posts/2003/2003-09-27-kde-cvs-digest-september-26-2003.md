---
title: "KDE-CVS-Digest for September 26, 2003"
date:    2003-09-27
authors:
  - "dkite"
slug:    kde-cvs-digest-september-26-2003
comments:
  - subject: "KHTML text selection"
    date: 2003-09-27
    body: "Glad these changes made it in. Text selection was always a blotch on KHTML's otherwise excellent performance. \n\nThe change to make the background fill the leading space came as a surprise to me. I always thought this was weird, but its been in KHTML so long I thought it was a feature, not a bug!"
    author: "Rayiner Hashem"
  - subject: "Re: KHTML text selection"
    date: 2003-09-27
    body: "Wow, text selection is fixed?  Awesome!  It's such a simple thing, but it really puts people off.  They think Konqueror is slow, when it is really pretty fast and the text selection just wasn't optimized well.  Now all we need is for it to tint images the \"selection\" color when they should be selected."
    author: "Spy Hunter"
  - subject: "Re: KHTML text selection"
    date: 2003-09-27
    body: "they are in HEAD (for a while)"
    author: "AC"
  - subject: "Re: KHTML text selection"
    date: 2003-09-27
    body: "Wow.  The KDE developers must be psychic ;-)"
    author: "Spy Hunter"
  - subject: "Re: KHTML text selection"
    date: 2003-09-27
    body: "Finally, one of the things I disliked most has been fixed! :-)"
    author: "Steffen"
  - subject: "Re: KHTML text selection"
    date: 2003-09-27
    body: "Yeah, the text selections are a LOT faster in very recent head. The only thing still missing is updating the selection as you scroll down a page, which can help performance.\n\nFor example, goto bugs.kde.org, click on something that will give you a large table with a lot of rows, like \"The Mosted Hated Bugs\". Put the window at the top of the screen, and start dragging from the top of the page to the bottom, making sure that the mouse pointer is at the bottom of your screen as you drag. Before you hit the bottom of the page, let go off the mouse pointer and watch it lag as it updates the selection. You hav e do this test correctly, or khtml will update it halfway through. Mozilla doesn't have this problem because it updates the selection piece by piece when scrolling. Opera 7.2 also doesn't have this problem, as it periodically updates the selection (every few seconds)"
    author: "AC"
  - subject: "Re: KHTML text selection"
    date: 2003-09-28
    body: "\ndoes that mean this bug is fixed:\nhttp://bugs.kde.org/show_bug.cgi?id=61533\n\n"
    author: "cbcbcb"
  - subject: "Re: KHTML text selection"
    date: 2003-09-29
    body: "How about copying text from a table where there is multiple cells in one row? in 3.1.4 when the copied text from each cell is pasted on it's own row.. sometimes with extra newlines while for example pasting from IE is much more nicer, the text is pasted to same line separating the cells with spaces. This is one of the most irritating features imo and will be hopefully looked upon some day :)"
    author: "huru"
  - subject: "Title bar heights"
    date: 2003-09-27
    body: "IMHO the titlebars of most window decorations are too high/big.\nThe \"Laptop\"-decoration rules because it does not look so bloated-up.\n\nI think a good measure for the ideal titlebar height would be the height of\nthe menubar (\"File\",\"Edit\",etc)\n\nAnything bigger looks unbalanced.\n\nStyles like plastik take a good approache because the make the high congigurable.\nIMO however even the smallest size in plastik is still too high (by 1 or 2 pixels, not more)\n\nVery often, the detail makes it!"
    author: "ac"
  - subject: "Re: Title bar heights"
    date: 2003-09-27
    body: "A good measure is the height of the titlebar text, anything else than that is an ugly hack."
    author: "poephoofd"
  - subject: "Re: Title bar heights"
    date: 2003-09-27
    body: "> A good measure is the height of the titlebar text, anything else than that is an ugly hack.\n\nIt should have the height of the menubar, where the standard font size\nfits well inside.\nIf the font it bigger, the titlebar should get bigger aswell"
    author: "ac"
  - subject: "Re: Title bar heights"
    date: 2003-09-28
    body: "> A good measure is the height of the titlebar text, anything else than that is an ugly hack.\n\nI notice that for mosr decocations the titlebar is much higher than the actual\ntext...\nThey are 2 big, 2 or 3 pixels less would make it look much more sexy"
    author: "ac"
  - subject: "Re: Title bar heights"
    date: 2003-09-27
    body: "For usability and accessiblity reasons, it should depend on the font size."
    author: "AC"
  - subject: "Re: Title bar heights"
    date: 2003-09-29
    body: "Funny.  I think the Laptop titlebar looks too cramped.  That's why I never used it, even on my 800x600 dpi laptop.\n\nI also don't understand what so many people see in the Plastik theme.  I think it's pretty bland.\n\nDifferent strokes, I suppose....\n"
    author: "Tukla Ratte"
  - subject: "Kudos for all participants about bug squashing!"
    date: 2003-09-27
    body: "The kde-3.2 is really getting there... :-)\n\nBut as there is intention (?) to move the rest of X11-specific code to use Qt instead (so that the whole kde system could be compiled on top of Qt-Embedded on fbdev-variations) could/should the DRI/DRM-specific stuff to be imported to the Qt to enable full blown OpenGL/SVG accelleration via GPU(s) to leave more idle cycles for processor to waste on worthless eye-candy? ;-)\n"
    author: "Nobody"
  - subject: "Re: Kudos for all participants about bug squashing!"
    date: 2003-09-27
    body: "There are places where X11 specific code is needed as there is no Qt equivalent. One example is ksnapshot where I query the X11 window hierarchy.  There are also things that will never get accepted by the Trolls into Qt because they can't be done in a cross platform way. So the short answer to your question is no.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Kudos for all participants about bug squashing!"
    date: 2003-09-27
    body: "Oh, then I just misundestood the item on the KDE 3.2 feature plan that has a line: 'Make kdelibs compile (and run) without dependencies on X11, for example useful for qt/embedded or qt/mac. Holger Schroeder <schroder@kde.org>' on the Libraries TODO list.\n\nMaybe that means that the kdelibs will be \"embeddable\" but not all separate programs?!?\n\nIt would just be so clean and much simpler to be able to dump all other UI libraries (like Xlib/gtk+/whatever) while retaining functionality provided by the base kde...\n"
    author: "Nobody"
  - subject: "Re: Kudos for all participants about bug squashing!"
    date: 2003-09-27
    body: "You sort of misunderstood, but sort of didn't. What's happening is that these parts of the code won't be available for non-X11 platforms. This will mean some loss of functionality.\n\nI don't think dumping X11 is a good idea at all, but being usable on a Mac etc. is reasonable.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Kudos for all participants about bug squashing!"
    date: 2003-09-30
    body: "Yeah, replacing the whole Xlib with something equal is a huge task, but isn't Qt-Embedded already have the most of the needed windowing system inplace?\n\nCould there be a \"info page\" to let people know what parts of the kde would be dropped incase using just kdelibs/qt-embedded API?\n"
    author: "Nobody"
  - subject: "Re: Kudos for all participants about bug squashing!"
    date: 2003-09-30
    body: ">>Yeah, replacing the whole Xlib with something equal is a huge task, but isn't Qt-Embedded already have the most of the needed windowing system inplace?<<\n\nWhat would Qt/E buy you? It has almost exactly the same (old | outdated | unflexible) rendering model as X11. If you want to replace it, at least vote for something that can compete with Quartz or GDI+....\n\n"
    author: "Jeff Johnson"
  - subject: "Licenses"
    date: 2003-09-27
    body: "> The first [discussion about licenses] was about a file that is part of KMail.\n> A license was added to the file, since there was none.\n\nAFAIK nowhere in copyright law is the file designated as the minimum unit that must have an explicit license.  Compare with functions: we don't demand an explicit license header for every function.  If a function doesn't have an explicit license, the file license is assumed to apply.  Just the same, if a file doesn't have an explicit license, we should be able to assume safely that the project license applies.\n\nIt's good to care about licenses, but let's not go overboard."
    author: "em"
  - subject: "Re: Licenses"
    date: 2003-09-27
    body: "If the license is changed, or a license added (which one, bsd? GPL? Artistic? all are in KDE) the authors need to give permission. Very simple in theory, but tough in a project where many contribute and are never seen again.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Licenses"
    date: 2003-09-27
    body: "> If the license is changed, or a license added (which one, bsd? GPL? Artistic?\n> all are in KDE)\n\nEasy.  Let me quote Ingo Kl\u00f6cker from your own digest:\n\n\"AFAIK KMail was started as GPL (which version?) application. And since the license was never changed all files which belong to KMail are of course GPL licensed.\"\n\nSo the GPL meets nicely the concept of \"project license\" I was talking about.\n\n> the authors need to give permission.\n\nOnly if you see the addition of the explicit license header as a license change.  IMO it would be more of a clarification, so no, I don't think the authors would need to give permission."
    author: "em"
  - subject: "Re: Licenses"
    date: 2003-09-27
    body: "I don't disagree. However the issue isn't whether it makes sense, but if a judge would agree. Copyright is established in law, hence lawyers and judges are the ones who decide what stands. And no, it doesn't make sense.\n\nDerek ( who believes the only ones who really benefit from all this is the lawyers )"
    author: "Derek Kite"
  - subject: "I understand that"
    date: 2003-09-27
    body: "I'm trying to argue from the point of view of a copyright lawyer.\n\nAnyway, the chances of this ending up in a court of law seem negligible to me."
    author: "em"
  - subject: "Re: I understand that"
    date: 2003-09-28
    body: "Trying to argue from the point of view of a copyright lawyer is pointless.  The law is so non-obvious and has so many nooks and crannies that unless you _are_ a copyright lawyer, you will probably get it wrong.\n\nThe KDE project cannot afford to take chances with licensing issues.  More than anything else, licensing issues have the ability to destroy KDE.  What if someone had at one point copied commercial code into this file with no license?  Then next year there could be a new case just like SCO's, only targeting KDE instead of the Linux kernel.  If the license is in every file, it gives KDE better legal ground to stand on.  That's just the way copyright law works.  I think everyone agrees that it is stupid.  But you can't ignore it, becuase the threat of stupid litigation hindering the KDE project is too high."
    author: "Spy Hunter"
  - subject: "Re: I understand that"
    date: 2003-09-28
    body: "> Trying to argue from the point of view of a copyright lawyer is pointless.\n> The law is so non-obvious and has so many nooks and crannies that unless you\n> _are_ a copyright lawyer, you will probably get it wrong.\n\nPerhaps, but when people say adding a license header would require the permission of all contributors to that file they're trying to argue from the point of view of a copyright lawyer too, aren't they?\n\n> What if someone had at one point copied commercial code into this file with\n> no license?\n \nSince I believe the file authors already licensed the file under the GPL by contributing it to a GPL project and not specifying a different license, I don't think the license header would make any difference here.  Please note that this is the same reasoning that allows people to contribute patches without adding an explicit license to each.\n\n> Then next year there could be a new case just like SCO's, only targeting KDE\n> instead of the Linux kernel.\n\na) SCO's case, at the moment, consists exclusively of a breach-of-contrat suit against IBM; everything else has been a smokescreen to spread FUD against Linux; b) SCO's case doesn't seem to be going to well for SCO at the moment, does it?; c) It has been widely acknowledged that all the Linux maintainers had to do was to remove the possibly infringing lines the moment they were notified of them (as they did) and they would be legally safe and d) the Linux kernel source files don't usually contain explicit licenses.\n\nI think I have made my opinion clear by now so I'll stop repeating myself."
    author: "em"
  - subject: "Re: I understand that"
    date: 2003-09-28
    body: "Licenses have no defaults. In order for a thing I write to have a license, I have to say what that license is.\n\nAnd no, just because it is contributed to a GPLd project, that's not enough. What if the author has no intention of making it GPL, but some incompatible-in-binary license? In that case, the source may be ok, but it can't be included in the project.\n\nIn short: when you contribute to a project, put a license header in the code,\nwhen you write code, put a license header in it."
    author: "Roberto Alsina"
  - subject: "Re: I understand that"
    date: 2003-09-28
    body: "> In short: when you contribute to a project, put a license header in the code,\n> when you write code, put a license header in it.\n\nGo tell the n00bs at linux-kernel they're doing it wrong then."
    author: "em"
  - subject: "Re: I understand that"
    date: 2003-09-29
    body: "Dude, you should look up what the appeal at authority fallacy is, and then come back."
    author: "Roberto Alsina"
  - subject: "Re: I understand that"
    date: 2003-09-29
    body: "Doh. Guys at linux-kernel are experts programmers. But are they experts lawyers?"
    author: "leo"
  - subject: "Re: I understand that"
    date: 2003-09-28
    body: "No, we're not trying to argue what a copyright lawyer might argue.  We have asked actual copyright lawyers, and what they say is that we should put the license in every file.  I don't see how you can argue with that.  It doesn't matter how \"reasonable\" it might seem, or what the merits of SCO's current case are, or how rediculous a lawsuit against KDE might also seem.  We must follow even the stupid laws or risk being targeted for stupid lawsuits."
    author: "Spy Hunter"
  - subject: "Kafka"
    date: 2003-09-27
    body: "Reading about all those improvents in the visual editing part of Quanta I wonder:\nAt work we have to use a content management system which works with\nM$-Word-HTML files. As you probably know Word saves and opens \"special\" HTML files\nwhich include additions Microsoft-Tags and Attributes which make it possible to\nkeep aditional editing information usually not saved in HTML. This way a file looks\nthe same again when reopened in Word. These tags don't influence the HTML display\nin a browser though. Now my question: Will Kafka allow me to edit those files without\ndestroying this information? For example, if I change the width of a table column will\nKafka keep additional unknown attributes within the TD-tag?\nThat would be really wonderful because this is why I still need Windows at work most\nof the time."
    author: "Mike"
  - subject: "Re: Kafka"
    date: 2003-09-27
    body: "Altough I'm not the one who is actively working on VPL, but I can say that our goal is to provide such an implementation, that the user's own code doesn't get destroyed once it's edited in visual mode. Just like in case of normal editing it isn't destroyed either.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Kafka"
    date: 2003-09-27
    body: "Thanks for your answer (and for you work on Quanta of course)\nI'm waiting unpatiently...  ;-)\n"
    author: "Mike"
  - subject: "Re: Kafka"
    date: 2003-09-28
    body: "Far be it from me to argue with my good friend here, but as neither of us work with eevil software this may not be entirely clear.\n\nFirst of all I know that FlunkPage uses additional non HTML extentions. I'm not aware of exactly what additional information Wurd pukes out, however I suspect it's part of the base document file. FP also uses additional directories to store extra info. If it exists as XML or comments it's pretty safe and if it's extra tag attributes it probably is safe... but I'm not sure that will mean things will work as you expect.\n\nHere is what is done by Kafka in Quanta. It will write information to only what is relevent that you add or edit within a node. This means that the rest of your hodge podge HTML from Wurd will be just as it was before... a best guess mess, but one that your company knows and loves. This is fundementally different from all other visual web editors which will happily reformat the document and munch your nice W3C compliant carefully laid out markup into a DTD schizophrenic mess if you edit one letter and save it.\n\nJust not touching this information doesn't mean you are free of the proprietary two step though. That wouldn't be good lock in design. Editing in Quanta will produce W3C DTD compliant markup (where you edit) but it will leave all your proprietary stuff alone. This could possibly cause problems going back to your proprietary extentions as they may be out of sync for the document. You should know that while we will make every effort to support and exchange data with proprietary tools we will *not* attempt to replicate proprietary porcesses to do this. (It is always possible to do this with scripting if someone wants to. We support DreamWeaver templates this way.)\n\nIn fact we hope that in 2004 using Quanta in a CMS entry configuration will be far more compelling. Aside from native Linux desktops this could include live CDs, emulation and various other corporate solutions. Using Wurd does not allow you to live preview content on page with a test server. This type of thing will be possible in Quanta 3.3/4.0. In 3.2 or 3.3 it will be possible to grant permissions to directly manage abstracted content on the server while preventing content people from affecting layout or back end. This frees developer resources. These types of solutions and others are not available using a word processor, which is hardly a focused or logical tool for this."
    author: "Eric Laffoon"
  - subject: "Re: Kafka"
    date: 2003-09-28
    body: "As long as the extra informations are written in proper SGML/XML, kafka WON'T modify what you don't edit. In others words, it only touch what you want to be touched ;-)\nBut i can't be sure that the file will then be properly read by Word, as i can't synchronize proprietary-specific-information, and as Eric said, we won't try."
    author: "Nicolas Deschildre"
  - subject: "amarok"
    date: 2003-09-27
    body: "amarok is great for all of you xmms/winamp lovers. it feels like xmms (gets our of your way), but doesn't have the horrible usability problems that xmms has. no, I don't like jukebox-like apps :)"
    author: "fault"
  - subject: "Re: amarok"
    date: 2003-09-30
    body: "except Markey still hasn't fixed the fft! :) Markey! u hear me?"
    author: "notMe"
  - subject: "KHTML Text Drag and Drop"
    date: 2003-09-27
    body: "Any work planned for this? You'd need it for kafka/design mode, but it'd be nice to have otherwise as well (like in moz)\n\noh yeah, the new caret browsing is uber cool. It actually navigates better than in mozilla. I find myself typing f7 a lot these days, thanks leo!"
    author: "aac"
  - subject: "Re: KHTML Text Drag and Drop"
    date: 2003-09-27
    body: "What is caret browsing?"
    author: "Anonymous"
  - subject: "Re: KHTML Text Drag and Drop"
    date: 2003-09-27
    body: "the act of using caret mode to browse pages.. if you have Mozilla > 1.2, just press f7 and a caret shows up in the page. you can control it with the keyboard"
    author: "aac"
  - subject: "Re: KHTML Text Drag and Drop"
    date: 2003-09-29
    body: "You're welcome :-)\n\nWhy does F7 work for you? For me, it doesn't, so I had to redefine it to Shift+F7. So that means caret mode works better for you than me ;-)\n"
    author: "Leo Savernik"
  - subject: "icons"
    date: 2003-09-27
    body: "i hate those new ones that look like ugly stupid pages with a bent corner. the dont seem to have much intrinsic meaning that relates to the type of file and look like a blurry mess when in detailed view mode (with normal size icons). stop going in the wrong direction and step back a minute. i dont mind icons conforming to a squary shape BUT get rid of the bent corner and make them actually look good. thats all i can do is critisize because i cant do any better."
    author: "not_registered"
  - subject: "Re: icons"
    date: 2003-09-27
    body: "I think the new icons in cvs look a bit cleaner than the old ones. Would not want to switch back... :-)"
    author: "Steffen"
  - subject: "Re: icons"
    date: 2003-09-28
    body: "all the good icons that look like paper (like new on konsole) have the bent corner at the top where it should be. that new icon should be the base design for all of them."
    author: "not_registered"
  - subject: "Pure SVG Icons"
    date: 2003-09-27
    body: "Is there any plans to ship Crystal as a pure SVG iconset? (e.g, not providing icon sizes in the icon theme, but rather scalable like GNOME does)"
    author: "Pure SVG Icons"
  - subject: "Re: Pure SVG Icons"
    date: 2003-09-28
    body: "I was playing with pure SVG icons a bit last night, and the rendered results were very variable.  I just did 4 icons in Sodipodi for up, back, forward and gohome, and they would render differently in two different Konqueror runs.  I don't know exactly where the problem lies, either Sodipodi's output is off, or the SVG icon renderer is not mature enough yet.  \n\nIf you want to have a play with this, read the excellent icon index.theme specification at freedesktop.org.\n\nWill"
    author: "Will Stephenson"
  - subject: "Re: Pure SVG Icons"
    date: 2003-09-28
    body: "It seems to render all of the nuvola svg icons fine."
    author: "anon"
  - subject: "Re: Pure SVG Icons"
    date: 2003-09-29
    body: "Hi Will,\n\n>I was playing with pure SVG icons a bit last night, and the rendered results were >very variable. I just did 4 icons in Sodipodi for up, back, forward and gohome, >and they would render differently in two different Konqueror runs. I don't know >exactly where the problem lies, either Sodipodi's output is off, or the SVG icon >renderer is not mature enough yet.\n\nThe answer is probably, both :)\nCan you send me the svgs? I may be able to fix it or at least tell you what is going wrong. Also if you can, a screenshot of how (mis)rendered they look for you would be nice...\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "When will we have an SVG GUI?"
    date: 2003-09-30
    body: "\n\nWhy need to get the whole interface rendered in SVG for KDE4"
    author: "SVGMaster"
  - subject: "Re: When will we have an SVG GUI?"
    date: 2003-09-30
    body: "What does it buy you beside a huge performance loss?\n"
    author: "Jeff Johnson"
  - subject: "Re: When will we have an SVG GUI?"
    date: 2003-12-30
    body: "Moving from a Raster(bitmap) based GUI to a vector(line) based Interface provides a couple of benefits:\n\nResolution independance\n - specifying GUI sizes in points, mm or inches rather than pixels makes it easier to map the windowing system on higher resolution devices.  Comparing this with dot-matrix printers which started out with output resolutions of 75 to 140 dpi pixels were relevant, but now they are around 2000 dpi pixels seem rather redundant.  Screen technology can follow suit as soon as windowing technologies start using vector graphics. Vector graphics = sharper,cleaner output at higher device resolutions.\nExample: My laptop runs at 130dpi, and my PDA at 140dpi - they take a little getting used to with small fonts, though I have increased all the font sizes, but not all applications play ball (it's a bit of a kludge).\n - Anti-Aliasing and ClearType technologies can be applied with very little extra effort to components of the system that were traditionally only used for vector based text.\n\nThe move is being made elsewhere\n - We all like being sheep, and copying other people, so I have included some examples of some guys and gals to copy:\n   Microsoft - the new Avalon vector-based(Direct3D accelerated) GUI of longhorn\n  http://weblogs.asp.net/pleloup/archive/2003/10/30/34676.aspx\n   Macromedia's Cold Fusion - A lightweight vector based GUI\n   Apples OSX (Aqua?) Interface - Based on Adobes PDF (based on Postscript) (OpenGL accelerated), as opposed to SVG (based on Postscript)\n\nAbility to take advantage of some SVG features that we didn't think of back when X11 was invented:\n  - different color models HSV, CMYK, RGBA (A=Alpha channel - see PNG)\n  - rendering windows in 3D (donning a virtual headset and having a video screen 360x360 degrees- ever thought that 2048x1600 res screen was a bit cramped)\n  - http://www.apple.com/macosx/features/expose/ - hey cool!\n\n\nIf you are afraid of incurring a performance impact you'll always have twm"
    author: "John"
  - subject: "clarification on KStars"
    date: 2003-09-27
    body: "Actually, the catalog that changed this week was the \"deep sky\" NGC/IC catalog, not the stellar catalog.  We already dealt with the license issues of our stellar catalog months ago :).\n\nAnyway, the issue was, I obtained a really complete version of the NGC/IC catalog from the ngcic project webpage, but it was licensed (like a *lot* of scientific data) as \"free for non-commercial use\".  I contacted the author to try to work out an arrangement, explaining the requirements of the GPL, and he said \"of course you can use it, KStars is non-commercial\".  He was a bit confused as to why I was bothering to ask.\n\nAnyway, I followed up, and asked explicitly if I could relicense it under the GPL, but he never replied.  So I added it to KStars with a notice that if someone wanted to use the catalog commercially, they should contact the author first.  This is not DFSG compatible, since it technically constitutes a usage restriction.  For this reason, we started a \"kstars_debian\" branch with only capital-F-Free data, and gave this branch a less complete version of the NGC/IC catalog that I compiled myself from several public-domain sources.\n\nHowever, I was then informed that it wasn't just Debian objecting to \"free for non-commercial use\" stuff, KDE can't accept that either.  So now HEAD uses the lame Free version of the catalog as well.\n\nIt's kind of weird.  The better catalog can't be used because it restricts against commercial use.  The catalog we are using is GPL'd, so technically, a company could harvest it and try to sell it to people, as long as they kept it GPL'd.  This is something that no company is going to do, since their first customer would be free to give it away to everyone else.  So the end result is the same.\n\nAnyway, look for a downloadable version of the better catalog on our website after 3.2 is released, if you can handle the burden of having a file on your system that you can't try to make money from..."
    author: "LMCBoy"
  - subject: "Re: clarification on KStars"
    date: 2003-09-28
    body: "------------------------------------------------------------------------------------------------------\nIt's kind of weird. The better catalog can't be used because it restricts against commercial use. The catalog we are using is GPL'd, so technically, a company could harvest it and try to sell it to people, as long as they kept it GPL'd. This is something that no company is going to do, since their first customer would be free to give it away to everyone else. So the end result is the same.\n--------------------------------------------------------------------------------------------------------\n\nCommon sense with \"free\" software fanatics is useless.\n"
    author: "mein"
  - subject: "Re: clarification on KStars"
    date: 2003-09-28
    body: "Well, I disagree.  No one was being fanatical, they just can't afford to make exceptions.  When I was told about the KDE rules, I happily complied.  My point was, it's unfortunate that we have to ship an inferior catalog to guard against what is (IMHO) a remote possibility.  But I do not disagree with the need of it in general."
    author: "LMCBoy"
  - subject: "Re: clarification on KStars"
    date: 2003-09-28
    body: "Hi,\n\ncould you possibly tell us how to help supporting the relicensing or do you think it is impossible to achieve? I think a good resource for humanity is wasted if not used. With GPL they still had the option to sell for use under other licences but GPL.\n\nThey should instead of \"non-commercial\" say \"\"non-commercial except GPL\".\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: clarification on KStars"
    date: 2003-09-28
    body: "Imagine that a bunch of people want to distribute Kstars at a price, to pay for costs. Or that a scientific magazine wants to put it in its CD. If kstars had the non-free catalogue, it would prohibit them do to this.\nKde (with Kstars) wouldn't be distributed by RedHat, Suse, Mandrake, etc, who all sell (distribute, make money of) kde. Having such an exception in one of its package would make this impossible.\n\n\n"
    author: "jukabazooka"
  - subject: "Re: clarification on KStars"
    date: 2003-09-28
    body: "Why would any sane company charge money for a GPL software , except for the costs of packaging or shipping maybe? \n\nThe only way to get money from a GPL software is through 'services' or 'subscriptions' or in any form dervied from that. It's a big joke to suggest you can make money from a GPL software from the software itself.\n\nIIRC, KStars shipped with similar non-free for commercial use catalogs before and all distributions shipped KDE without a problem. When you put KStars on a CD and charge for it, you're not charging for KStars itself (if you are, then you're insane) but you're charging for the medium and the costs associated with it, therefore, you're not 'commercilizing' KStars and the whole argument falls apart.\n\n"
    author: "mein"
  - subject: "Re: clarification on KStars"
    date: 2003-09-28
    body: "The FSF used to charge $1000 for a copy of \"the GNU system\", and it didn't even boot. They offered paper tape, too.\n\nSo yes, many charge for copies of GPL software. Hell, *I* charge for copies of BSD software."
    author: "Roberto Alsina"
  - subject: "Re: clarification on KStars"
    date: 2003-09-28
    body: "\"a big joke to suggest you can make money from a GPL software from the software itself.\"\n\nNo, that's no joke. A joke is putting words on other people's mouths. I never said anything you said i said, nor will I say more even if it needed saying."
    author: "jukabazooka"
  - subject: "Re: clarification on KStars"
    date: 2003-09-29
    body: ">Imagine that a bunch of people want to distribute Kstars at a price, to pay for costs. Or that a scientific magazine wants to put it in its CD.\n\nNeither situation makes KStars commercial software, so the use of \"free for non-commercial use\" data is still OK, IMHO.  Same goes for commercial Linux distributions; just because they sell a CD with my program on it doesn't make my program commercial software.  Linux distros sell convenience and support.  \n\nThe only situation we have to worry about is someone harvesting the catalog from KStars and incorporating it in their own commercial software.  KDE policy is that we do not want to prevent people from trying to do this, even though the Free version of the catalog is protected by the GPL, making commercialization of it nearly impossible anyway, IMHO.  So, I have to ship an inferior catalog to allow for the possibility that someone may want to sell it, even though no sane company would try (why would anyone buy a KStars catalog when it can be download for free?)"
    author: "LMCBoy"
  - subject: "Possible solution?"
    date: 2003-09-29
    body: "How about putting a menu option that would go off and download the catalog for you?\n\nCatalogs == good. Any plans for the Abell or Hickson catalogs?\n\nGreat work on kstars, by the way!\n\n-- Stephen"
    author: "Stephen Boulet"
  - subject: "Re: Possible solution?"
    date: 2003-09-29
    body: "We're plannning on this, and also a startup wizard which will ask if you want to install \"extra\" data the first time you run it.  Other apps in Edu need it as well, so we'll likely make a common widget, or use KNewStuff from PIM.  Have to wait for feature thaw, though.\n"
    author: "LMCBoy"
  - subject: "Re: Possible solution?"
    date: 2003-09-29
    body: "Great!\n\n-- Stephen"
    author: "Stephen Boulet"
  - subject: "Text selecion"
    date: 2003-09-27
    body: "I do't really understand is this really the simple act of selecting text or something more. I never thought Konqueror was very slow when selecting text.\n\nAnyway, I'm very happy about the recent changes in KDE and especially the focus on bug squashing.\n\nI also want to know up to what version fo safari have changes to KHTML been made or are they just from all over?\n\nEither way, Konqueror's rendering engine seems to be approaching Gecko quickly."
    author: "Alex"
  - subject: "Re: Text selecion"
    date: 2003-09-28
    body: "> Either way, Konqueror's rendering engine seems to be approaching Gecko quickly.\n\nIt doesn't deserve this harsh form of criticism. ;-)\n\nAs to selecting text, selecting in Konquerors HTML view looked different than in widgets. It no longer will and that's a big deal for consistency. For those like you and me used to it, we probably will notice only the absence.\n\nThe speed I cannot find a problem either, maybe in complex layouts. But until I can correctly paste tables with layout into e.g. KWord, I won't do that much anyway.\n\nYours, Kay\n\n\n\n"
    author: "Debian User"
  - subject: "Slowness of Konq (the browser)"
    date: 2003-09-29
    body: "I totally agree that Konq has become very good at rendering pages.\nHell, when I compiled the latest CVS, I couldn't believe that Konq rendered ESPN well.\n\nHowever, there is an area of Konq which I believe to be slow...  CSS.\n\nFor those doubters, please use Gecko-based browsers and hit\nhttp://www.enlightenment.org\n\nNotice how the background image is smooth when scrolling down the page?\n\nNow please try it with Konq, and notice how it's choppy? (or is it just my Thinkpad?).\n\nI know it's nitpickin', but thought I'd mention.\n\nAlso, mouse-over menus at http://www.cnnsi.com\n\nMouse-over effects are rather slow (choppy) compared to IE on Windows.\n\nPlease excuse me if I'm the only one encountering these lags.\n\n\n\n\n"
    author: "KJS"
  - subject: "Re: Slowness of Konq (the browser)"
    date: 2003-09-30
    body: "That's because you shouldn't be looking at alternate projects ;p\n\nYeah, I notice taht too, but very slightly."
    author: "Alex"
  - subject: "Re: Slowness of Konq (the browser)"
    date: 2003-09-30
    body: "No I have also noticed css mouse-over slow downs.  It is mostly when you have a bunch of text displayed on the screen though.  Check out http://sycophant.org, then click on a topic, then open an article, when you move up and down the menu on the left you can see a great deal of slow down.\n\n\nsmeat!"
    author: "SMEAT!"
  - subject: "kdedevelopers.org NOT WORKING"
    date: 2003-09-27
    body: "Okay, this is just great, I've been trying to get an account to comment on kdedevelopers.org for 3 days since it makes you register now. Unfortunately, no amtter what I do it will NOT work :((( I've tried 3 e-maila dresses and trust me, my e-mail is NOT THE PROBLEM, it's the website! It won't send me my password and when I say I forgot my password it also won't send me my password! I've tried everything, if you can't make registering actually work,a t elast don't force us to register."
    author: "Alex"
  - subject: "Re: kdedevelopers.org NOT WORKING"
    date: 2003-09-28
    body: "I registered a few days ago and it worked without problems. Perhaps you should try to contact geiseri at yahoo dot com who is AFAIK responsible for the site."
    author: "MK"
  - subject: "Re: kdedevelopers.org NOT WORKING"
    date: 2003-09-28
    body: "try getting an account through drupal."
    author: "uuu"
  - subject: "\"plus many bug fixes\""
    date: 2003-09-27
    body: "I should first note that I am only talking about bugs which resulted from a programing error.  I realize that there are other types of bugs ... -- some which require major redesign.\n\nWhat does it mean that a bug is fixed?\n\nI received the automatically generated e-mail advising me that Bug 48747 had been: \n\nRESOLVED\nFIXED\n\nBut, the bug in KDE 3.1.x has NOT been fixed -- it is still the same after an update.\n\nI think that this is a corporate culture issue.  Perhaps it shows the rather low position occupied by:\n\n1.  The users.\n\n2.  The current release.\n\nYes, it is the same problem as with commercial software.  What it means is that the bug will be fixed in the NEXT release.\n\nI would like to again strongly disagree with this.  This is not the way to do it.\n\nIf this type of bug was reported as being in the current release then it should be fixed in the current branch (which might not be the release it was reported in), not in a future release.  It should start out as a patch to the current branch and then be ported to future releases and HEAD.\n\nI suggest this as a modest first step in changing the culture to be more oriented towards fixing bugs.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-27
    body: "Maybe I don't get it.. but don't bugs get always squashed in head first? And then there may be a backport to the latest stable branch..? At least that's how it's done with the linux kernel."
    author: "Thomas"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "I am not 100% famailar with how the Kernel is developed.  But, you should first consider that it has a much longer release cycle and that bugs in the current release branch are fixed.  But, in any case, perhaps ultil we release KDE-3.1.20 we shouldn't use the Kernel development cycle as the model for KDE.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "Sorry, I don't think this is the way how things work. Developers mainly work on the HEAD branch. Bugs are fixed here and later are backported to BRANCH. As the probability of the new release of BRANCH goes low (like now when the release cycle for 3.2 has started) developers are really busy working on HEAD and only critical bugfixes are backported. You should know that it's not fun at all to maintain two or more branches and many people don't do it at all. Thankfully there are some developers who cared about backporting, but from the commit logs I see that they are also very busy with HEAD right now. And this is normal.\nAnd don't forget that some bugs are easy to be fixed in HEAD (may not even apply), but are hard or close to impossible to backport. For example imagine that Quanta parses incorrectly a HTML document in BRANCH. I most probably won't fix it as the parser is rewritten in HEAD and if it's working there sincerly I don't care about the BRANCH as it would stole from me maybe several hours to fix. Hours that I could use to fix bugs in HEAD or implement new features. And that is more fun. ;-)\n\nSorry for dissapointing you.\nAndras"
    author: "Andras Mantia"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "If you have complaints about how we handle our bug reports for free for you, then maybe you can take over maintenance of our ~9000 bugs and wishes.  Bugs cannot be left open forever simply because there exists a release that does not contain the fix.  It is a maintenance nightmare and developers will spend more time closing bugs than they will actually fixing them.  It's a big waste of everyone's time, and in the end, I think we'll all just give up on bugzilla.  We do this for free, and this is how we have chosen to deal with things.  We are open to constructive suggestions on how we can manage our time more effectively.  We are not open to complaints that we do not do enough for user.  Everything in KDE is done for ourselves (as users, yes we use KDE too) and for our users.\n\nKDE is getting close to 3.2 beta.  Development of 3.1.x is basically done, with only critical bugs being backported.  If you want 3.1.x to be developed for your own personal needs, perhaps you should consider a support contract with one of the several KDE related businesses.  Even better, you could join the KDE team and contribute to it yourself.  You could even contribute to bugzilla if you have better ideas for how to manage bugs too.\n\nSuggesting to KDE developers how we should change our behaviour to better suit people who use our software for free is one of the most pompous and presumptuous acts I can imagine.  You have a right to have that attitude with a company that you pay to develop software for you.  You don't have that right with OpenSource.\n"
    author: "George Staikos"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "> Suggesting to KDE developers how we should change our behaviour to better suit people who use our software for free is one of the most pompous and presumptuous acts I can imagine. You have a right to have that attitude with a company that you pay to develop software for you. You don't have that right with OpenSource.\n\nWell said, considering most KDE Developers are working on their own time."
    author: "AC"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "> Well said ... .\n\n> Suggesting to KDE developers how we should change our behaviour to better suit \n> people who use our software ... . [sic]\n\nTo me this statement is the height of arrogance.  It is the canonical expression of the \"arrogant developer syndrome\".\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "It is the height of arrogance to suggest that you have any expectations at all with FREE software.\n\n                            NO WARRANTY\n\n  15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO\nWARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.\nEXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR\nOTHER PARTIES PROVIDE THE LIBRARY \"AS IS\" WITHOUT WARRANTY OF ANY\nKIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE\nIMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR\nPURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE\nLIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME\nTHE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n\nWe have use of this software because someone at great cost of time and resources decided to make it available to us. I expect that the individual developers would have the maturity to manage their time and resources to benefit themselves. If I benefit, it is purely incidental. And if I want more than someone else is willing to GIVE? Isn't that the height of arrogance? The license that you agree to is that you bear the cost of the fix.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "HELLO! Ever read a Micro$oft software license?\n\nThey say the same things!  No warranty at all.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "Yes, but they say it after you've shelled out your $300 and opened the shrink-wrapped box, thus making it unreturnable."
    author: "Tukla Ratte"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-30
    body: "And your point is?\n\nHave you ever thought that maybe your suggestions are essentially the same as saying \"you give me 20 hrs each week. What I really want is 40\"?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-30
    body: "No, I never thought that.\n\nWhat I do think is that Tom Petters' and Edwards Deming's ideas are good ones.\n\nThat using them in software developement would result in more being acomplished with less effort and that the results would be higher quality.\n\nIf we set those ideals as the goal, do you disagree with it?\n\nI have to conclude that many of the arrogant an condecening remarks are from people that did not read and/or did not understand what I said.\n\nYou need to learn to think outside of the box.\n\nNow, you may have legitimate disagreement with the methods of achieving these goals but ... .\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "> To me this statement is the height of arrogance. \n> It is the canonical expression of the \"arrogant developer syndrome\".\n\nHow is that arrogant? Would you do everything other people whine about when this is someones hobby? Some people in this project work hard on fixing bugs in both HEAD and BRANCH and those are really admirable. However, we have at several occasions pointed out this is not, cannot and will never be normal just because of the way contributional software works. FOSS does NOT exclude the option to pay people to fix your bugs, but you can't expect voluntary contributors to roll out the red carpet before and sweep behind you. Would you? This has been pointed out to you so many times and you still don't seem to understand. Why is it so hard? FOSS is _not_ your personal cockaigne, it's what people contribute, and contributors... you get the idea, don't you?\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "OH! Goody, a semantics discussion.\n\nYou can call it what you will.  \n\nThe M-W dictionary defines 'arrogant':  \n\n1.  exaggerating or disposed to exaggerate one's own worth or importance in an overbearing manner\n\n2.  proceeding from or characterized by arrogance \n\nand 'arrogance':\n\na feeling or an impression of superiority manifested in an overbearing manner or presumptuous claims \n\nOr you can call it something else if you want, but that doesn't change what it is and what is is isn't good.\n\nOTOH, you posting is not really arrogant, I find it condescending.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "> OTOH, you posting is not really arrogant, I find it condescending.\n\nQuite remarkable scentence Mr. \"I am an professional you inapt idiots\" Tyrer, isn't it?\n "
    author: "AC"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "You must have totally misunderstood what I previously said -- perhaps intentionally.\n\nI did not intend to denigrate anyone.\n\nWhat I was saying was that I did not appreciate the condescending treatment I received from self taught hackers.\n\nSo to rephrase your statement, I am Mr: \n\n'I am a professional engineer, you shouldn't treat me like a know-nothing newbie.'\n\nHope that that clears things up.  Sorry for any misunderstanding.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "\"Cockaigne\"?\n\n<riffle riffle riffle mutter mutter>\n\nHey, I learned a new word today!\n"
    author: "Tukla Ratte"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "Stop feeding the troll. Ignore him and he'll go away, or get a clue, eventually."
    author: "Brad Hards"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "We don't have the resources to do this. If you want to fund a small team of people (6-12 people) to work full time in backporting bugs, then go ahead, I think we'd all appreciate it :)"
    author: "AC"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "Still wondering why KDE developers don't listen to you? Because of your behavior shown in past and present (like http://marc.theaimsgroup.com/?l=kde-cafe&m=106473157227245&w=2). Accusing others of arrogance and disqualification don't increase your chances to be considered as valuable KDE user (user because you still failed to bring in any active development effort)."
    author: "Anonymous"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "> Still wondering why KDE developers don't listen to you?\n\nNo, I don't wonder anymore.  I know -- it is due to arrogance.\n\n> Accusing others of arrogance ... .\n\nThe three messages in the kde-cafe link demonstrate this arrogance.\n\nRa's ipso liquid; the thing speaks for itself.\n\n> ( ... you still failed to bring in any active development effort).\n\nPerhaps I misunderstood, but I was lead to believe that my help in fixing bugs would not be welcome because I would be submitting patches to the current release branch.\n\nI *have* found solutions to several bugs, but it appears that nobody (hardly anybody actually) is interested.  I'm not really clear about the reasons for this, but I did what others do, I found solutions to the bugs that bothered me the most.\n\nNote: I posted to kde-cafe because I thought that it would be better to continue this discussion there.\n\n--\nJRT  "
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "Applying patches to the branch and not to HEAD makes the bug tracking problem worse, not better.  Then we mark a bug fixed and it returns, or we have to leave bugs open until HEAD becomes \"stable\" and then reiterate through all the bugs all over again.  Supply patches against the branch, but also check out the code for HEAD and do the merge of the patch there.  Send both patches - as part of a bugzilla report, not a post to a mailing list that will certainly be lost - and they will certainly be accepted if they are correct."
    author: "George Staikos"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "> Applying patches to the branch and not to HEAD makes the bug tracking problem\n> worse, not better\n\nHmmmm ... Does/would this really change things.\n\nA bug is marked as closed when a patch is posted but not really fixed till the patch is applied to the CVS tree.  Perhaps an additional status marker should be added to Bugzilla to indicate that the patch had been applied.  But, this doesn't change -- it is the same whether you make the patch for the current BRANCH or for HEAD.  The only issue is that it may take some tweaking to apply the patch for the BRANCH to HEAD.\n\nIn any case, bugs have a habit of returning on HEAD because it is not stable.  This should not happen with a patch to the *stable* branch.\n\n> Send both patches - as part of a bugzilla report, not a post to a mailing list\n> that will certainly be lost - and they will certainly be accepted if they are\n> correct.\n\nDuh!  I post my patches to Bugzilla.  I only post them on the mailing list for possible discussion; usually a short while before I post them to Bugzilla.  But, you are still saying that if I only post a patch for BRANCH that it isn't acceptable.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "No, bugs return in HEAD because a patch gets applied to BRANCH and not to HEAD, or because the patch is rebroken in HEAD.  You are welcome to post whatever patches you like as long as they pass moderation on the list.  If you want a developer to apply it, you either have to provide a patch for HEAD as well, or you have to hope that the developer will forward port for you.  They don't live to serve you though, and if they don't have time to forward port it, then it has to wait.  File a report and one day it will be solved."
    author: "George Staikos"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "Getting back to your previous posting.  I thought about this a while and it appears that you may have stumbled onto something.\n\nBugs that are marked as fixed because they are fixed in HEAD should be left open as you said -- then need to be left open till a stable BRANCH with the bug fix is released.  Otherwise, there is no assurance that the bug is actually fixed -- that it will not regress.\n\n> No, bugs return in HEAD ... . \n\nYou can, if you wish, believe that there is no regression in software development but I don't think that anyone will believe that. \n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "\tIt's a futile attempt trying to encourage most KDE developers to close bugs. Or trying to encourage a development process that isn't \"entirely feature centric\". You'd be dissappointed and flamed to oblivion. Only some weeks ago I reported a bug. The response I got was \"It doesn't happen here on KDE-CVS\". Yeah, I guess if I was using KDE-CVS I wouldn't have reported the bug. \n\n\tSome developers are responsible enough to fix the bugs. Others just ask for a backtrace and tell you \"it doesn't happen here on KDE-CVS\". Fix!. Forget it. You either need to use KDE-CVS, bear with the bug or use the next major release of KDE, 6 months later or so. \n\n\tThe question is why release your work for public consumption if you are not willing to be responsible for it, maintan it, or you don't care for the productivity of the users who are using and testing it? Oh, just watch the flood of responsive that follow this. They'll all be in the form of \"put up or shut up\", \"hire a development team\", \"they work for free\", \"you don't contribute so keep quiet\", \"you don't know how open source works\", etc. Have I missed any?\n\n\n\n\n "
    author: "Mystilleef"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "Hi,\n\nyou are still free to use more professional products. When did you e.g. last time had a backport for an other desktop you used? I mean some of those rich companies must be very eager to listen to you.\n\nNot?!\n\nNow what... just because those people try to make something fun to use means they have to be your slaves?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: ">Hi,\n \nHello\n\n>you are still free to use more professional products\n\nWhat make's you think KDE isn't professional?\n \n>When did you e.g. last time had a backport for an other desktop you used?\n\nWhen did KDE start using other desktop policies?\n\n>I mean some of those rich companies must be very eager to listen to you.\n \nI doubt they are. \n\n>No?!\n\nYes, they will not.\n\n>Now what... just because those people try to make something fun to use means they have to be your slaves?\n \n\tFirst of all, I don't believe I ever called anyone slaves. It is repugnant even concieve that thought. Secondly, if I *freely*, *willingly*, *without any compulsion* submit my package for public consumption, I think it is also my responsibility to cater for it, yes, that includes fixing bugs that may arise from public usage and public testing. \n\n\tThe concept is not as controversial as you make it sound. If my orientation was successfully, isn't that one of the benefits of open source? Thirdly, everyone stands to benefit from such as culture, you, I, the developers etc. I'm not being selfish, I wish the best for the project.\n\n\n"
    author: "Mystilleef"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "Hi,\n> The concept is not as controversial as you make it sound. If my orientation was successfully, isn't that one of the benefits of open source? Thirdly, everyone stands to benefit from such as culture, you, I, the developers etc. I'm not being selfish, I wish the best for the project.\n\nI am not controversial about what you think you would do. See the many \"I, my\" in your statement.\n\nI see need to defend people's right to do whatever is fun for _them_. And not come across people who play on their honour or something. You make it look like if they don't jump to solve your problems with their software, that would be bad.\n\nThey do indeed care and solve problems with KDE. If you are unable to help yourself, it may take some time before you get a new release with the fix by somebody else. So what?\n\nDid you ever think why you get the source?\n\nThat said, KDE is indeed professional. I wanted to point out that their approach is actually giving you the best support of all desktops in the market. I don't think that KDE has to backport bug fixes for users more than others do that have multibillion dollar sales income from their desktop version. I mean, do you recognize that elsewhere the priviledge to _report_ a bug is something you cannot afford. Not talking of fixes in the next version you of course have to buy...\n\nBut as you say, you already know that.\n\nNow listen, you get the source, because that way you can help yourself. Or you can help somebody who is capable of helping you in return.\n\nWhining on what you would do doesn't help anybody. Do it. Become a developer of KDE and deliver the backports. Or pay somebody to. Or stop killing people's fun about developing the latest and greatest KDE.\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: " The conversation is not getting anywhere. Feel free to believe whatever you think I'm making it seem. One thing remains the same. Bugs open security holes, vulnerability and exploits. The bug count in KDE is rising. I believe we need to curb via policies. If we don't, it will get out of proportion. KDE is not all about fun. It is about responsibility, working together as team, realising our mistakes and accepting criticisms and moving forward to correct them. \n\nYou can call my suggestions whining, bitching and moaning. I this point I really don't give a crap. Let's work together to fix it, or lets sit here arguing about the human rights of developers.\n\n"
    author: "Mystilleef"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "Yeah, let's create a policy which forbids to commit security holes, vulnerabilities, exploits and bugs to KDE CVS!"
    author: "Anonymous"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "\"Bugs open security holes, vulnerability and exploits. The bug count in KDE is rising.\"\n\nThe reason bug-count rises is because lots and lots of users report bugs. The number of users keep on going up, and that drives the number of bug-reports up. But there would be bugs in the software whether they are reported or not.\n\nOf course bugs are a problem. But the number of bug-reports is NOT the problem, quite the contrary! your problem seems to be the number of bug-reports, not the bugs themselves.\n\nIt seems to me that solution to your \"problem\" is that we prevent people from reporting bugs, and that would cause the number of reported bugs to drop to zero in a short while. Problem solved!"
    author: "Janne"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "Pardon my ignorance, aren't bug reports a fair indication of existing bugs? So lets go by example and assume they aren't. What exactly is your accurate criteria of calculation the actual number of open bugs in this project?"
    author: "Mystilleef"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "If KDE had no users there would not be any bug-reports. If there are lots and lots  of users, there will be lots and lots of bug-reports. That is a fact. And your obsession seems to be about the number of open bugs in bugs.kde.org.\n\nAnd, like I have repeatadly said (in vain it seems): Many of the bugs in KDE don't matter, they are from old versions, from CVS or beta-versions, unconfirmed, from broken packages etc. etc. Clean those out and you get ALOT less bugs. And I heard that Mozilla has something like 40.000 bugs. Suddenly KDE seems quite bug-free to me!"
    author: "Janne"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "Compare the bug numbers for kexi and konqueror.\n\nwhich has more bugs?\n\nI predict the bug numbers will be considerably higher the closer we are to release. In indirect proportion to the number of users that find the software useful. This is a limitation and problem with the bug reporting system, rather than any indication of stability of the software.\n\nIf you want something to do, I suggest you go through the 4000 or so open bug reports and figure out what they are, whether they are fixed already, are they duplicates of others. Try duplicating the problem on your machine. Maybe try to figure out what is the cause of the problem.\n\nAfter doing this for a few weeks you will grow in respect of the developers that have been doing this very for years. \n\nThere isn't any nice easy answer except rolling up your sleeves and getting to work. Those doing the work find it very demoralizing listening to the complaints from the sidelines. If you find an unpleasant reaction to your comments, take it as an indication of the effect you are having on the developers.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "> Some developers are responsible enough to fix the bugs. Others just ask for a backtrace and tell you \"it doesn't happen here on KDE-CVS\". Fix!. Forget it. You either need to use KDE-CVS, bear with the bug or use the next major release of KDE, 6 months later or so. \n\nThe thing is that most developers just don't have the time or energy to have 3.1.x installed. So if it works in kde-cvs, it must have been fixed at some point between 3.1.x and kde-cvs. This is how it's always worked so far. I'm sorry, but for a mostly volunteer organization, we have limited resources. "
    author: "anon"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "> The question is why release your work for public consumption if you are not willing to be responsible for it, maintan it, or you don't care for the productivity of the users who are using and testing it?\n\nMany developers are working on KDE just for fun. Don't you have a hobby of some kind? For many KDE developers/contributors/doc authors/artists/translators, it's just a hobby, nothing more :-)"
    author: "anon"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: ">>>The question is why release your work for public consumption if you are not willing to be responsible for it, maintan it, or you don't care for the productivity of the users who are using and testing it?<<<\n\nThe KDE-3.0 series had 6 minor (bugfix) releases.\nThe KDE-3.1 series has had 4 minor releases, so far.\n\nHow does this fit into your argument?"
    author: "anonon"
  - subject: "Are you so clueless?"
    date: 2003-09-28
    body: "People responded to your thread last time and had more than a dozen reasons that showed what was wrong with your quixotic approach and suggestions of a \"100% bug free\" KDE. From your comments it was clear you did not understand the way software development works, in OSS or in software development houses.\n\n\nKDE developers are not feature centric all the time, it depends on teh rlease schedule and their will. Right now there are suually 3-4 times as many bugs fixed than the number of feature enchancements. In addition, many feature enchancements are necessary to fix a lot fo bugs often. You can't constantly patch something forever,  there comes a point where that patched up thing isn't capapble of handling an more due to its architecture.\n\nKDE developers tend to support old KDE releases for about a year, for 1.x releases and about 2-3 years for x.0 releases since the following versions like 1.x are binary compatible. This is more than many companies spend supporting a product. KDE developers are very responsible for their code and maintain it.\n\nIts true that KDE developers don't backport bugs that aren't critical to an old version of KDE. It is no fun, often it is not possible and if the bug is already fixed in CVS you should just upgrade to the next version of KDE instead of waiting for the issue to get backported. Don't you see what an incredbile wasted effort it is tring to backport all the bugfixes from CVS not only is it not possible for a large number of bgus, it is also a dupicated effort unless the bgus are critical.\n\n\"Oh, just watch the flood of responsive that follow this. They'll all be in the form of \"put up or shut up\", \"hire a development team\", \"they work for free\", \"you don't contribute so keep quiet\", \"you don't know how open source works\", etc. Have I missed any?\"\n\nYes, when you make unrealistic, unpproductive suggestiosn and you clearly ar ejsut clueless adn talking idealistically without knowing how the process works or having experience in the software field you shouldn't be suprised by these responses. KDE developers work for free, and I would like to see how you react when someoen constantly bugs you about how to spend your free time and in some way suggesting that you are responsible for maintaining stuff you do in your free time. If I build a sand castle in my free time, I don't expec tor want to maintain it or add enchancements to it if I don't want to and you know what telling me what is reponsible for me to do or what I should do with it is plain ignorant.\n\nSuggestions are always welcome, but when tehse suggestions turn into demands and personal attacks that's another matter. Your suggestiosn are not generally constructive or are just far too unrealistic and clueless for anyone to take you seriously. Yes, you don't know how opensource works and these kind of demands will not be met, until you chose to do so.\n\nDon't forge tot read these all over again: http://dot.kde.org/1063743715/1063799495/ it is clear you failed to udnerstand why people reacted the way they did and why you received the responses you did. I am serious study these carefully, until you understand what people are saying.\n"
    author: "Alex"
  - subject: "Re: Are you so clueless?"
    date: 2003-09-28
    body: "\tYeah, I predicted your type in my last paragraph. Might I suggest you have a look at the Linux project. Read the mailing list, kernel trap and kernelnewbies. Also observe how they manage bugs, fix and back port bugs. When you are done return here and change the topic to \" Are you cluesome?\" \n\n\tPerhaps, then, you might learn a thing or two about how open source should work. It's simple, there are feature releases, what KDE-3.2 is supposed to be, and there are bug and security fixes, what KDE-3.1.* is supposed be. But of course, I'm clueless. And I'm brain farting at the moment. You are making the most sense.\n\n\tIf you can't stand the process being criticized just don't reply.\n\n\n\n"
    author: "Mystilleef"
  - subject: "Re: Are you so clueless?"
    date: 2003-09-28
    body: "I am familiar with te way the kernel development works and yes you are cluesome, but not in the sense that you sue it. You jsut have a lot of cluses that you are very much wrong, but I guess it's not enough for 2 dozen people and a few KDE develoeprs to tell you why.\n\nThe kernel is very different from KDE, unlike KDE a new kernel takes almost 3 years to release, a new version of KDE is released aproximately every 6 months. Upgrading the kernel is also a much more complicated process than upgrading KDE and can break the entire system and cause more incompatibilities than you can imagine. The kernel is the heart of linux (actually  it is linux, but some people reffer to the entire Linux suite of applications when they say Linux) it is much more critical that older versions of the kernel have lots of critical fixes backported than it is for KDE.\n\nIn addition, many more developers are paid to work on the kernel than KDE, so they can afford to do this. KDE is a far smaller project.\n\nIn any case the kernel is not even close to what you wanted KDE to be in your quest to be controversial. it is nowhere near \"bugless\". \n\nIf you take into account the frequency of kernel releases and the frequency of KDE rleases, you will realize that the number of bugfixes backported to an older version of the kernel in comparrison to how many bugfixes were actually backported to an older version of KDE are about proportional when you take into account the total number of bugs fixed in the development versions of KDE and the kernel. Therefore, your comparrison does not stand.\n\nIn addition, the kernel is a separate project from KDE, apples and oranges, if you want to compare something compare it with GNOME. But any comparrison will not be accurate since not all OSS projects are the same and people definitely aren't the same.\n\n\"But of course, I'm clueless. And I'm brain farting at the moment. You are making the most sense.\"\n\nWhen your right, your right. In your post you failed to adress anything I said in my previous post.\n\n\"If you can't stand the process being criticized just don't reply.\"\nwhat's taht supposed to mean. Why would you think I havve anything against you trying your best to criticize me?\n\n"
    author: "Alex"
  - subject: "Re: Are you so clueless?"
    date: 2003-09-28
    body: "\tThere you go again. You just won't stop would you. First I'm clueless about OSS development, and now I'm cluesome. Take a stand pal. And will stop mixing my issues \"Bugless by KDE-4\" with that of the original poster. They are two different issues. Reread them if you wish. With regards, to Linux project, my suggestion is exactly as they practice. If a developer doesn't maintain a package, doesn't fix bugs, or something breaks due to bugs, the developers work is thrown out of Linus' branch. Things have even gotten more strick ever since the vm issue in 2.4. \n\n\tPeople are going to be using 3.1.* series for a good while. It is pointless fixing bugs in KDE-CVS without backporting to 3.1.* series. KDE-CVS is for features, development and so on. KDE-3.1.* series is for fixes and security pacthes. So why fix bugs in CVS when the people who really need them are using KDE-3.1.*. That is what the original poster is trying to get accross. This is totally different from \"Bugless by KDE-4\" which I still vehemently standby. I'm I making sense now? Or I'm a I still clueless? It's like fixing kerne-2.4.* bugs in kernel-2.6-test6. It doesn't make sense. \n\n\tAnd for the last time, developers can do as they wish. I don't care. I'm not forcing them to code. I don't command them to code for me. They are doing it out of their freewill. And I appreciate that. What I'm suggesting is to the KDE community as a whole not a group of people. You have two choices, a high quality KDE with few bugs, or a bleeding edge experimental project with over 5000 bugs. Even though KDE is a unique OSS project different from GNOME or XFCE or Linux, clearly it is far from a perfect project. We learn a lot from other projects, OSS or commercial. The bugs are still manageable now. What happens when they explode out of control and their is so little developers to deal with them? Yeah, that's what's going to happen if think a project as successful as this is all about fun. "
    author: "Mystilleef"
  - subject: "Re: Are you so clueless?"
    date: 2003-09-29
    body: "> fixing bugs in KDE-CVS without backporting to 3.1.* series. KDE-CVS is for features, development and so on\n\nmost can't. \n\n> It's like fixing kerne-2.4.* bugs in kernel-2.6-test6. It doesn't make sense. \n \nread what you said above.\n\n> a high quality KDE with few bugs, or a bleeding edge experimental project with over 5000 bugs\n\nthere you go with bug count statistics again. there is simply *no way* that a project as big as KDE can't have as many bugs as it has. GNOME has an equivalent amount. Mozilla and OpenOffice probably have more since their code size is bigger than all of KDE's. The Linux Kernel probably does too. linux 2.6.x will probably have a much larger bug count than 2.4.x since it is about 30% in LOC. "
    author: "anon"
  - subject: "Re: Are you so clueless?"
    date: 2003-09-29
    body: "\"You have two choices, a high quality KDE with few bugs, or a bleeding edge experimental project with over 5000 bugs. \"\n\nLike I said, large part of those bugs don matter. They are either obsolete (from KDE2 or even earlier), unconfirmed, from CVS or beta-versions (where things are bound to be buggy) etc. etc. But hey, we do have a solution for \"100% bug-free KDE\"! We just have to stop people from reporting new bugs! In the last 14 days the developers closed over 800 bugs, while over 600 new bugs were reported. If we stopped people from reporting new bugs, I think it would take couple of months for bugs.kde.org to show that KDE has zero bugs left. Then you would be happy since KDE would be \"100% bug-free\".\n\nWhat was that you said? That would acutally  make KDE worse then before since people would not report new bugs? Oh.... Well, that would not work now would it?\n\nthe \"problem\" of KDE is that it has too many users that do their duty and report bugs. Those bug-reports inflate the number of bugs.\n\nWe do not have the \"two choices\" you suggested. We have two other choices: Stagnating but relatively bug-free KDE, or hi-functionality KDE with advanced features, excellent usability, good performance and a bit more bugs. I bet users would prefer the latter."
    author: "Janne"
  - subject: "Re: Are you so clueless?"
    date: 2003-09-28
    body: ">>Also observe how they manage bugs, fix and back port bugs. When you are done return here and change the topic to \" Are you cluesome?\"<<\n\nBut also notice this important property of the kernel: if there is no one who wants to maintain and fix a old kernel series, it dies. If a driver or architecture has no maintainer anymore, it will be removed. It is all about people who are willing to do the work (or fund it). Linus Torvalds calls it directed evolution. KDE is much like the kernel in that aspect, the main difference is that there are no forks but branches. Nobody prevents anybody from fixing bugs in existing branches. Nor should anybody prevent anybody from adding features to future branches. People naturally do what they need most. KDE is self-regulated, because every change has a cost and people will carefully spend their time/money. \nThe direction that KDE takes is the direction of those who pay for it - with time or money. In that aspect KDE is not different from any proprietary licensed, off-the-shelf product. It's all about the customers, and they have different needs and wishes. Some of them may share your view (and thus invest in the same development), others do not. The influence of an individual or group is proportional to the amount of contributions. \nBut actually it does not matter for KDE as long as they pay and thus improve KDE. \n"
    author: "Tim Jansen"
  - subject: "Re: Are you so clueless?"
    date: 2003-09-29
    body: "> Yeah, I predicted your type in my last paragraph. Might I suggest you have a look at the Linux project. Read the mailing list, kernel trap and kernelnewbies.\n\nThere are *way* more commercial contributors to the linux kernel, who are PAID to do some of the grunt work that most KDE developers wouldn't do just because it's not fun (and a lot of work)"
    author: "anon"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "Well, since you obviously hate KDE so much (since KDE is \"so full of bugs and developers are unwilling to fix them\"), then why don't you use some other desktop then? Surely there are some desktops there that comply with your \"100% bug-free\" ideals? No? They ALL have lots and lots of bugs? Well, that's what I thought....\n\nBugs in KDE are not that bad. I mean, take a look at them. Some of them are simply things that \"work, but they could work a bit better\". Or they are reports about obsolete versions (KDE2 anyone?). Or they are from CVS. Or they are duplicates. Or they are unconfirmed.\n\nOh in case you didn't notice, the KDE-developers have closed ALOT of bugs in the last few days: \"629 bugs opened, 818 bugs closed in the last 14 days\""
    author: "Janne"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "If I hated KDE so much do you think I'd be wasting my time here willing to take flames for making KDE better?"
    author: "Mystilleef"
  - subject: "You are wasting everyone's time."
    date: 2003-09-29
    body: "It would be very nice, if you, after a lot of bragging, went ahead and backported some \"simple\" bugs. If you tried that, you would note, that backporting is very hard and very much likely to introduce new bugs. Noone wants new (possibly more severe) bugs in a stable branch. Thus only critical and trivial bugs are fixed in the _STABLE_ branch.\n\nIf you come across a critical or particularly annoying bug, it will in all likelihood be fixed in the stable branch as well."
    author: "Moritz Moeller-Herrmann"
  - subject: "Nice Article at OSNEWS.COM"
    date: 2003-09-29
    body: "Just read your article at OSNEWs.com, it's pretty good except some spelling and gramatical errors such as using where and were.\n\nI agree completely with \"I'd like to contribute but to this project but it doesn't look professional. No Documentation\"\n\nHowever, your other points, umm... let's just say that the voice interface isn't exactly very good usabiltiy wise or productivity wise. and you have to understand that simplicity and very few features are a bad way to design softare. It's as if instead of cleaning your house you just destroy all the rooms that are messy, yeh you have a clean house, but would you want to do that? \n\nAnyway, some features do need to be removed completely or at least reorganize dina  lto of sfotware projects I know, these feautres jsut get in the way, but most are useful.\n\nNow the other point you made \"The \"Who cares, its free\" Attitude\" is also clearly wrong.\n\nThis isn't proprietary software, you release half-baked projects in OSS so that many developers can help you release a truly good version fo your software. This is what version numbers are for. responsible developers mark these as pre 1.0 and clearly say they are not ready for primetime.\n\nAlso, while nobody is chasing OSS developers deadlines in thsi market odn't come  from your boss, but rather from your competition.  I'm afraid, that you can't wait forever making your little project perfect, because by that time ther will be a clear market leader which has gathered so much momentum and now includes so many more features than you had planned that its just not worthwile. OSS develoeprs might also be discouraged seeing taht the proprietary alternative is soo much better and that catching up is virtually impossible. OSS projects have as much pressure for release as proprietary IMo.\n\nAlso, OSS does not copy MS or other companies excpet when tehy have come wih a good solution, for example the taskbar in windows and windows are very good and not copying these would be stupid. You also need to make the 98% of people who don't use Linux feel at home and not introduce them to something completely different, at least not until linux has at least 10% f the market.\n\nBTW: I'm rooting for mid 2005 as the date Linux will be ready for primetime and I mean really really ready. Too bad that's when Longhorn comes!\n"
    author: "Alex"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-09-29
    body: "\tFor some odd reasons I've been unable to get connected to osnews. I also apologize for the horrible grammatical blunders. I must have sent the wrong copy of the article to osnews. I could have sworn I ran that thing through three different spell checkers including Microsoft Word, eww did I just say that. :-) As my final pathetic excuse, English is my third language. :-) I admit that no excuse for being sloppy. In the future I'll be more vigilant.\n\n\tOverall, I'm glad you enjoyed the article, save the spelling errors and poor quality, and your criticism is noted and welcome. I still think KDE should have bug paranoia culture though, that won't change. :-P"
    author: "Mystilleef"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-09-29
    body: "I disagree, that article just writes large his complete refusal to understand a FOSS project like KDE.  We are not developing this for people who need their hand held to write an email.  Period.  I think all of us would agree that we want to develop a system that *we* would like to use, and there's really nothing wrong with that.\n\n\"but..but...what about Joe User?!  He can't be expected to figure out a hugely complex app like KMail!  You have to *know* the email address you want to send to!  That's too complicated!!\"  Well, in the first place, I think you *hugely* underestimate the abilities of an \"average\" user.  I've never heard of anyone giving up KDE because they couldn't figure out how to do their work.  You are really exaggerating this.  Secondly, I think KDE's apps are not less usable than the equivalent apps on other systems, and in many cases they are more useable.  Your email example goes way too far, beyond usability and into annoyingness.  You are basically proposing \"Clippy for KMail\"  UGH, no thanks!  \n\nAnyway, choice exists, and this is good.  If anyone finds KDE to be the dizzying array of incomprehensible junk that you portray, then other projects can probably fit his needs better.  Actually, for such a person I'd recommend no computer at all.  They are inherently complex devices.\n\n\"You'll never penetrate the market then!\"  *Who  Cares??*  I don't think most of us are too concerned about \"taking over the world\", we just want to make great software.\n\nPlus, I find some of your comments about how all FOSS software is poorly designed and very poorly documented highly insulting.  I happen to think my app (KStars) has one of the simplest, most intuitive interfaces for a program of its kind, and the Handbook is quite extensive.  With less effort than it takes to write your rants, you could get in here and help us improve things where you see problems.  You don't need to know C++ to rewrite a Handbook chapter or redesign a UI file.  Put up, or shut up.  Or both! :)\n"
    author: "LMCBoy"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-09-29
    body: "Sir, the article wasn't on KDE usability, the KDE development process or KDE in general. I don't remember mentioning  KDE or Kmail or any other Kapplication once in the article. My illustration on the \"intelligent mail software application\" was a hypothetical example demonstrated how an application could impress/attract users who see no advantages in using software applications running on Linux over what they are currently familiar with. I suggest you read the article again without the bias or vehemence you have against me. \n\nWhile Kstar might be well documented there still exist a horde of open source applications, even in KDE, that aren't and will most likely never be documented. And I have participated in the documentation of several projects. I plan to participate in some documentation in KDE when I'm less exasperated and after my graduation. It's unfortunate you find a fact insulting. The truth hurts, deal with it. :-)"
    author: "Mystilleef"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-09-30
    body: "I didn't think it was so nice. I'll admit there were good points, but they were intermixed with severe problems and I can already predict that I'll be labeled a \"bad developer\" in spite of the fact that my project has documentation and an excellent record squishing bugs. I'm okay with that though because this guy has got a growing attitude of contentiousness and is more and more ready to fling out generalities followed by confrontational statements. I think it is important to put things in perspective...\n\nFrom the article \"Mystilleef is a computer enthusiast who believes computers are hard to use and software is diminishing in quality. He will be graduating from university this year. His field of specialization is Accounting and Finance with a minor in Computer Science and Economics. Among other things, Mystilleef enjoys arguing, reading, playing video games and outdoor sports.\" So he's ready to go out into the big bad world and make his mark, and he is specializing in accounting. My God! No wonder he is taking the position KDE must be 100% bug free. Bugzilla is the unimpeachable red ink on a balance sheet where the unquantifiable profit column must be world domination. If I thought like an accountant it would make sense. Then again I would have been too risk averse to start a business. I note he enjoys argueing, but no indication if that is debate, or just postulating interspersed with jibes. It does look like pushing people's buttons must be more entertaining than diplomacy.\n\nQuoting again... \"Don't release half-baked attempts at hacking to the public. If your project isn't complete fundamentally, if it still contains bugs you don't know how to solve yet, if it is not properly documented, if you have no intentions to complete your project or support in the near future, just be responsible and don't release it to the public until you are sure of what you are doing. I know several people who just got tired of packages breaking their system, and went back to safer Mac or Windows.\"\n\nI think we can resolve this rather quickly. Step back in time and start Linux and KDE this way. Both projects celebrate the initial emails where a small amount of code had been started and releasing it attracted other developers. Mystilleef's position is as appalling as any I can think of. Had this position been taken by pioneers in OSS it would be dead today. Period! There is no way the lone individuals could have taken the projects to anywhere near where they are today, let alone a remotely usable state, by themselves. In the case of KDE, it was the catalyst for Gnome. So you could be running CDE on BSD. In open source when someone has an idea we say \"show me the code\" and projects advance on their relative merit. Now we are treated to \"don't show the code\" and I guess the only leadership we will have is the loudest voice not showing code. Oh goody!\n\nThere are few things more offensive than someone serving up theoretical nonsense as if it was manna from on high. I prefer the advice of Eric Raymond, \"Release early and release often\". Here's my advice. Go out in the real world, get a job, tell your employer he is all screwed up and you have a better way, get kicked in the teeth... repeat as needed until the concepts of practical applied knowledge and diplomacy take hold and foster some humility... then check back.\n\nAbout this author... Eric Laffoon first programmed in fortran in 1976, has been self employed for the last 14 years and has been leading the Quanta Plus project for the last several years. He also believes age and guile beat youth and exhuberance every time. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-09-30
    body: ">I didn't think it was so nice.\n\nWhy I'm I not surprised.\n\n>I'll admit there were good points, but they were intermixed with severe problems and I can already predict that I'll be labeled a \"bad developer\" in spite of the fact that my project has documentation and an excellent record squishing bugs.\n\nBased on my criteria, I really can't tell what kind of developer you are. Quanta, however, happens to be one of the exemplary OSS projects I admire a lot. Excellent documentation and bugs, well they are managed intelligently.\n\n>I'm okay with that though because this guy has got a growing attitude of contentiousness and is more and more ready to fling out generalities followed by confrontational statements. I think it is important to put things in perspective...\n \nAttack the issue, not the author. It is difficult to speak about open source software without generalising. I intentionally avoided mentioning any particularly open source software project to be as objective as possible. The article wasn't meant to be confrontational. It was meant to identify a few of the weaknesses in OSS as well as enumerate avenues to attract people to OSS, if in fact it is our objective to do so. I didn't write the article to please people. I wrote the article based on my observation and participation in several projects. Undoubtedly, the truth offends.\n\n>So he's ready to go out into the big bad world and make his mark, and he is specializing in accounting. My God! No wonder he is taking the position KDE must be 100% bug free. Bugzilla is the unimpeachable red ink on a balance sheet where the unquantifiable profit column must be world domination. If I thought like an accountant it would make sense. Then again I would have been too risk averse to start a business. I note he enjoys argueing, but no indication if that is debate, or just postulating interspersed with jibes. It does look like pushing people's buttons must be more entertaining than diplomacy.\n \nContrary to your misconception, my comment regarding the issue \"Bugless by KDE-4.0.0\" wasn't stated to make KDE bugless, it was stated to encourage a culture or sub-project the reduces the astounding number of confirmed bugs to a manageable amount. Look into \"Gentoo Bugs Day\" for I where I stole this idea from.\n\nYour stereotype about accountants is insulting, ignorant and unfounded. Why you had to attack my image dumbfounds me beyond belief. I didn't expect this from a respected member of the KDE community. Because of my utmost respect for you, I'll take it in good faith and in jest. It's okay for friends to make fun of each other.\n\n>\"I think we can resolve this rather quickly. Step back in time and start Linux and KDE this way. Both projects celebrate the initial emails where a small amount of code had been started and releasing it attracted other developers. Mystilleef's position is as appalling as any I can think of. Had this position been taken by pioneers in OSS it would be dead today. Period!\"\n\nThe key word in my statement is \"complete fundamentally\". Now that I've said that, go back and read the article, this time with an open mind and without bias against the author. XFCE is a good example of a project that comes to mind. The rewrite, XFCE4 had been in the lab for so many years and when it was released to the general public, it was a success.  Going by your argument, XFCE4 should be dead. You mistake release for public consumption with release for public development. I stand by the statement and I don't see anything appalling about it. I only think it will make open source software better and more reliable contrary to it's already existing reputation. \n\n>There are few things more offensive than someone serving up theoretical nonsense as if it was manna from on high. I prefer the advice of Eric Raymond, \"Release early and release often\". Here's my advice. Go out in the real world, get a job, tell your employer he is all screwed up and you have a better way, get kicked in the teeth... repeat as needed until the concepts of practical applied knowledge and diplomacy take hold and foster some humility... then check back.\n\nI don't blame your attitude towards my article. I imagined it will offend people like you, the older generation. For the likes of you OSS is perfect, it couldn't and wouldn't get any better than this. And change is definately not condoned, nothing beats the traditional way of doing things, the way things are done today. I have had several jobs. Last summer I had an internsip at a software firm. In school, I work for the computing services as an administrator assistant. I'm also a resident advisor. But I'll take your advice, and report back to you on it. :-)\n\nSo let's recap why you think my article sucks in no particular order,\n\n1) The author is contentious.\n2) The author is training to be an accountant. And accountants are anal\n3) The author discourages OSS projects not to release their works to the public until it is complete and properly documented. \n4) The author claims he enjoys arguing, so the article is written in vain.\n5) The author is not pragmatic because he isn't as experienced as I am in coding and leading OSS.\n\nI'll concede partially to the last point. Although, I haven't lead any OSS project, I have participated in several. In general, I'm dissappointed by your response. It is hardly contructive, riddled in personal attack and other immature nuances, and hardly touches open serious issues plaguing OSS as discussed in my article. When I saw your name in the reply link, I jumped in excitment at the link thinking I'd learn a thing or two from a respected OSS developer. Now, I wish you never even responded. Have a great day Mr Lafoon, I hope we meet under more hospitable circumstances in the future.\n\n\"It is silly to mistaken knowledge for wisdom, one make you earn a living, the other makes you live a life\""
    author: "Mystilleef"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-09-30
    body: "I apologize for misspelling your name in last response Mr Eric Laffoon. "
    author: "Mystilleef"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-10-01
    body: "\"Excellent documentation and bugs\" \n\nIndeed, Qunata has very good documentation, but I never actually rate bugs on how cool they are...\n\n\"Contrary to your misconception, my comment regarding the issue \"Bugless by KDE-4.0.0\" wasn't stated to make KDE bugless, it was stated to encourage a culture or sub-project the reduces the astounding number of confirmed bugs to a manageable amount. Look into \"Gentoo Bugs Day\" for I where I stole this idea from.\"\n\nWell, if that was your intention why did you have to say something so unrealistic adn flawed, nto that it couldn't happen in 10 years hypothetically, but because no develoeprs would work on KDE anymore due to their boredom, GNOME would also lead even tough it might be buggier, most bugs do not affect the user and so the user would choose GNOME over KDE because it would visibly evolve and become easier, faster, and richer in features. by the time KDE is bugless the rules of UI design and what software is designed to do would be totally different. These are jsut some flaws with this idea.\n\nIf your goal was to motivate KDE developers to fix bugs, you should not have stated an unattianable goal, rather something that KDE developers can actually reach. People lose hope when tehy are working towards a goal they can not reach> Even the galapagos turtle didn't walk anymore once it realized that it was acting as a transportation tool and that the apple on the stick would never be caught.\n\n\"The key word in my statement is \"complete fundamentally\". Now that I've said that, go back and read the article, this time with an open mind and without bias against the author. XFCE is a good example of a project that comes to mind. The rewrite, XFCE4 had been in the lab for so many years and when it was released to the general public, it was a success. Going by your argument, XFCE4 should be dead. You mistake release for public consumption with release for public development. I stand by the statement and I don't see anything appalling about it. I only think it will make open source software better and more reliable contrary to it's already existing reputation.\"\n\nYou do realize that from your aritcle the idea people get is that you should silently work on your project without tellinga nyone anything about it and not release till its very good. People would not think that you mean releases for the users not the developers. This si already done, through release numbering, rarely do I see an OSS project which is 1.0+ and really sucks.\n\n\"I don't blame your attitude towards my article. I imagined it will offend people like you, the older generation. For the likes of you OSS is perfect, it couldn't and wouldn't get any better than this. And change is definately not condoned, nothing beats the traditional way of doing things, the way things are done today. I have had several jobs. Last summer I had an internsip at a software firm. In school, I work for the computing services as an administrator assistant. I'm also a resident advisor. But I'll take your advice, and report back to you on it. :-)\"\n\nNow your stereotyping too as you have in toehr aprts of your post. I happen to agree with your point on documentatin but generally disagree with the rest and I am just 16, does that mean I'm part of the old generation too. \n\nAccept that it is nto always that people don't understand your opinion or are too scared of change, but rather jsut plain disagre with your ideas and for good reasons.\n\nI doubt that almsot everyone posting on the dot is old and fails to understand your points. Maybe it's time for you to rething and reread what we write too. You are just too radical in your statements as was your \"bugless KDE statement\" msot people are far more moderate, while they tend to agree on the general issue. I for example agree that KDE's number of bugs should be reduced to under 2,000 and it is being reduced, but I don't think less than 1,000 bugs is practial."
    author: "jace"
  - subject: "Re: Nice Article at OSNEWS.COM"
    date: 2003-10-01
    body: "\"I don't blame your attitude towards my article. I imagined it will offend people like you, the older generation.\"\n\nDoes that mean that you are 12 years old? I for one am 26 years old, am I \"old\"?"
    author: "Janne"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-30
    body: "Conversations are best held between people with reasonable viewpoints. Without express knowledge it may be difficult to determine what is reasonable. Many people are often surprised at the statistical level of failure inherent to the most successful performers in any endeavor. In most endeavors the only way to have a perfect performance is to succeed on a small effort and never try again. I say this because you had previously suggested KDE should be bug free. This will happen with the 100% efficient internal combustion motor. Be sure to file a bug report for heat loss and inertial loss at the top and the bottom of each piston stroke. The only way to have bug free code is never release it.\n\n> It's a futile attempt trying to encourage most KDE developers to close bugs.\n\nThis is not a reasonable statement. How many developers have indicated to you they will not close bugs, or are you just encouraging deficient? At least you are right up front with your hostility. I'll give you that.\n\n> Or trying to encourage a development process that isn't \"entirely feature centric\".\n\nAgain I say, your credentials please? If you had to walk through a minefield would you want a guide who said \"I've walked through this and know the way\" or one who said \"I haven't got any experience with minefields but I think I know a better way than the guy with experience\"? Given the possibillity for disaster I like experienced leadership.\n\n> You'd be dissappointed and flamed to oblivion.\n\nYou're still here. Maybe there's hope for you, if you lose that chip on your shoulder.\n\n> Only some weeks ago I reported a bug. The response I got was \"It doesn't happen here on KDE-CVS\".\n\nDo you know what this means?! It means it is fixed!!! You should be jumping up and down for joy. Of course it could still be a configuration glitch from your distro and it is unfortunate that the developers can't come over and install the fixed version. Hey, grab a clue... If it does not manifest it's self to a developer it CANNOT be fixed. You can't fix what's not broken... and if you leave the bug report there then you will incur the wrath of these people who complain how many bugs are in KDE. Do you know how annoying that can be? We don't want to upset them, do we?\n\n> Yeah, I guess if I was using KDE-CVS I wouldn't have reported the bug. \n \nNo. You would have reported a number of other bugs because this is where developers work and it is considered unstable. That's because they are also completing requested features. Guess what. If they stopped the features, fixed the bugs and back ported them, then built the features, then debugged them... they would arrive at the same place. The only difference would be that they would get there much later... AND... there would probably be more bugs because each step touching something and changing it introduces the possibility of a bug. So sheer statistical probabllity, something you don't have to be a programmer to understand, indicates the current flawed process is superior to your proposed much more flawed process.\n\n> Some developers are responsible enough to fix the bugs. Others just ask for a backtrace and tell you \"it doesn't happen here on KDE-CVS\". Fix!. Forget it. You either need to use KDE-CVS, bear with the bug or use the next major release of KDE, 6 months later or so.\n\nIf anything else you said indicated you were reasonable this might look like an issue. However while some bug fixes can be back ported to the prior release (assuming the developer has multiple releases set up on their system which is not always easy to do) others are just impossible. We have lots of stuff going into 3.2 that can be run on 3.1 if you run Quanta from CVS. But don't expect to see the visual page layout... because it requires kdelibs from CVS.\n\nSo there could be a little bug in an application that you want fixed that you would have to upgrade Qt and half of KDE to fix, and after that the other half of KDE won't run. So the only options are run CVS, wait for the release or have the developer abandon their current release work and fork KDE rebuilding a mountain of code themselves or possibly back porting everything from Qt on up. The problem is, that can't be done faster than the next release. \n\nThere seem to be a few guys who are so incredibly unaware of what is involved in the process, and seemingly unwilling to listen to any reasonable explanation, that they feel the only thing to do is attack the character of those people who are failing to give them what they want fast enough. Developers are not always articulate enough and rarely have the time to spell out for you why reality prevents you from having your instant gratification. Of course once you start suggesting flaws in their character prevent them from circumventing known reality you are a lot less likely to get a response you will enjoy.\n\nGet a clue. Sometimes instant gratification is possible, other times it is not and sometimes what is possible is so horribly inadvisable and impractical it is better to think of it as impossible. There are some people I wish would try developing, not because I want to develop with them, but because I'd like them to begin to understand when they are being unreasonable. If you can't be reasoned with what difference does any explanation make?"
    author: "Eric Laffoon"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-30
    body: "I just don't have time for people with \"can't be done\", \"never be done\", impossible to be done\" mantra. If I listened to people like that, I wouldn't be where I am today."
    author: "Mystilleef"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-10-01
    body: "He's right you know, even relatively simple programs have dozens of bugs which are discovered and the ones that are discovered are at least just as many.\n\nSorry, we're only human. Sometimes impossible and can't be done are appropriate and this is one fo thsoe cases. By this I don't mean that it is entirely out of the question, but that it would really never happen, nobody would spend 10+ yeas jsut to perfect a piece of software to the point of which the developer feels that tehre are no bugs whatsoever discovered or not and by which time the software design and function is archaic."
    author: "jace"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "Hi,\n\nthis would require 2 times testing from the developers. KDE 3.1.x is now in a state where no more releases except security is expected and KDE 3.2.0 is about to appear.\n\nYou have to understand that. If you don't want to accept it, feel free to add to the resources.\n\nMy understanding is that as soon as KDE is the enterprise desktop supported by e.g. IBM or HP, you will get what you want. Lots of engineers designated to backport fixes to the stable release. \n\nBut to achieve that, KDE has some more way to go. I perfectly understand the preference to achieve the level where more company support will arrive instead of private people in their spare time acting as if they were paid for it.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-28
    body: "I think that any in depth discussion would be better carried out on kde-cafe, but I will respond to your misconceptions.\n\n> this would require 2 times testing from the developers.\n\nWhy?  I wouldn't have made the suggestion if I had thought that.  \n\nIt is easier to fix bugs on the current release branch because it *is* stable.  Then you need to try to apply the patch to the current development branch.  In the case of my example, it appears that this would have worked OK.\n\nYour point was about testing.  Since the current release *is* stable, you can figure that if you make a minor change and it works that that is it.  No extensive testing should be required.  Then you apply the patch to the development branch.  That has to be tested anywho; doesn't it?  Does applying the patch increase the amount of testing required?  I think not -- you have to test the Alpha anywho. \n\nSo, my thinking is that my idea would result in less work and better results.\n\n> KDE 3.1.x is now in a state where no more releases except security is expected\n> and KDE 3.2.0 is about to appear.\n\nThat isn't always the case, is it.  I'm not saying that you should fix bugs on KDE-3.1.x forever, only that this is what should be done till it is closed and the final release in the branch is issued.  I think that this would be when KDE-3.2rc1 is released, but this is a management decision not really part of my idea.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: \"plus many bug fixes\""
    date: 2003-09-29
    body: "Hi James, \n\nthanks for the answer. I tend to agree that it could be done. \n\nBut we also need to look at the difference of KDE development vs. company development:\n\nSay you were a developer that develops KDE mostly for your personal use, want it to win the desktop challenge (not war) and care deeply about getting as soon as possible to improved new KDE releases.\n\nTo develop KDE you possibly use gcc 3.3.2 plus CVS fixes as comes with Debian unstable, the latest KDevelop and the latest working KDE from CVS HEAD. \n\nThe stable KDE would not even _compile_ without further work (I guess). So you naturally stay on CVS only. Nobody risks untested checkins on stable, even trivial ones for not-so-important issues.\n\nEarly in a stable series, a lot of bugs will get fixes on the stable release, but after such a lot of time, it is likely not worth it. The most annoying bugs are worked out and you can wait few months more till KDE 3.2 release, not?\n\nWhen 3.2 is occured, for a lot time most people will work on that I guess, but once KDE 4 is approaching, don't expect fixes when everybody is on QT4, gcc 3.5 and so on....\n\nYours, Kay\n\nYours, Kay"
    author: "Debian User"
  - subject: "Gopher ioslave"
    date: 2003-09-27
    body: "It's nice to see support for gopher:// URLs being added, but it seems to be being done in a nonstandard way?\n\nIt looks like it will try to guess from the content if the URL was for a gopher directory or something else. Standard gopher URLs encode the content type of the target in the first character of the URL's path, as shown here:\nhttp://www.freesoft.org/CIE/RFC/1738/16.htm"
    author: "AC"
  - subject: "Re: Gopher ioslave"
    date: 2003-09-27
    body: "I suppose that it would be better if you would report such a bug to KDE Bugs: http://bugs.kde.org\n\nReported only here it would be soon forgotten, if it has got the attention of the right developer at all.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Gopher ioslave"
    date: 2003-09-28
    body: "Actually, I did that over a year ago, and it was returned to me as not needed.\n\nI started working on file dialogs with plugins and multi-paged, (hit the bush economy for 5 months unemployment; sure can stop a lot). The plugins can be done to allow searching in the space, but also to allow for conversion .\n\nI also had another one returned that also works with the above. Kio's are too limited in structure. There should be 3 base type of file, page, and 2d/block base (sql, but also snmp). \n\nBy doing the above, the concept of files, dbs, prints, and even export/import disappear. Instead, we just \"Send To...\" and \"Get from...\" with shortcuts allowed (read bookmarks)"
    author: "a.c."
  - subject: "Re: Gopher ioslave"
    date: 2003-09-28
    body: "Then: sorry, that I have written this. You seems to have reached the right developer after all. :-)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Gopher ioslave"
    date: 2003-09-28
    body: ">  Kio's are too limited in structure. There should be 3 base type of file, page, and 2d/ block base (sql, but also snmp).\n\nI have seen that there will be support for \"Extra fields\" in KDE 3.2, whatever that means. Is this what you mean by that?\n"
    author: "mm"
  - subject: "Re: Gopher ioslave"
    date: 2003-09-30
    body: "\"Extra fields\" ...Is this what you mean by that?\n\n\nNo. While I have not been able to do any kde work for the last year, I would assume that extra fields is for extra attributes.\n\nThe real problem is that kio is suppose to represent a file stream via disk IO.\nthe KIO gives us the virtualization of it, but it just does not get it done for all of the real world. The real world has actually several major forms out there.\n1) a simple stream for which kio can, and does, represent pretty well.\n2) a page device/stream. PDF, PS, even pixs are pages that are ultimatly sent to a form of a page device such as a printer or a fax.\n3) a multi- dimensional item, but more like a 2D or table based approach. The sql and snmp KIO are but 2 that would have benefited from a based class.\n\n\n\n\n\n\n "
    author: "a.c."
  - subject: "Re: Gopher ioslave"
    date: 2003-09-28
    body: "If you mean the gopher_kioslave in kdenonbeta, thanks for the url because i thought gopher url's were given in a different way, I'll have a look at that page you say and to others RFC i have here to see if i was reading in a bad way."
    author: "Albert Astals Cid"
  - subject: "package inclusion"
    date: 2003-09-28
    body: "does anyone else wish the package system would be replaced? i like most of the default kde programs but that doesnt mean ill use all of them. and most of the time id rather only install the ones i want, instead of the entire kdenetwork or kdemultimedia package, which actually contain some reduntant programs. i know you can disable the packages you don't want but this is a little tedious, especially when you are recompiling every week or so... seems to me that it might be a cleaner solution to just have the kde distribution be kdelibs, kicker and konqueror, then let you install the programs you actually use. just a thought"
    author: "ac"
  - subject: "Re: package inclusion"
    date: 2003-09-28
    body: "No, that's the job of the distributors. We make it easy for the distributors to get everything, and for contributors/developers to contribute. Everyone else should probably get KDE from their distribution. If your distribution doesn't split KDE up, then I suggest you complain to your distribution. I use debian, and they split KDE up.\n\nIf you only want to __compile__ kicker and konqueror in KDEbase, I suggest you investigate the DO_NOT_COMPILE environment flag. Search for it in google."
    author: "anon"
  - subject: "Re: package inclusion"
    date: 2003-09-28
    body: "Hi,\n\nthen use a distribution with a policy that requires that for everything. I may suggest Debian which I use.\n\nInstall a 20MB basesystem then do:\n\napt-get install xserver-xfree86 kmail\n\nwhich downloads/installs everything needed for just that one program. You probably also want kdm to login graphically and kdebase to have konqueror, kwin and the other basics.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: package inclusion"
    date: 2003-09-28
    body: "I totally agree, using the current way I'm getting all kinds of programs I don't want on my system, cluttering up my menus, cluttering up context menus (hopefully real context menus in 3.2) and other things.\n\nA config system like the Linux kernel has would be ideal.. just do a 'make kdeconfig' (or so) and config what programs you want and what not.\n\nIt takes a bit more time, but it's worth it.\n(and hey, it would add even more choice, and that's what we all want, right?)\n"
    author: "poephoofd"
  - subject: "Re: package inclusion"
    date: 2003-10-02
    body: "I (would) vote for this one. :-)\n"
    author: "Nobody"
  - subject: "this is great"
    date: 2003-09-29
    body: "Nice digest, thanks!\n\nbest bug report ever:\n\nRefer to Bug 64769 - restarting kicker makes kwin something something\nDiffs: 1, 2\n\nanyone else get it?  i read down teh page for a while until i went \"OH OH OH!\"\n\nlol nice.\n "
    author: "standsolid"
---
In <a href="http://members.shaw.ca/dkite/sep262003.html">this week's CVS digest</a>:
<a href="http://quanta.sourceforge.net/">Quanta</a>'s visual editor makes progress. 
KHTML gets text selection optimizations and immediate repaint from <a href="http://www.apple.com/safari/">Safari</a>.
<a href="http://www.kontact.org/">Kontact</a>, <a href="http://kmail.kde.org/">KMail</a> and 
<a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a> get work on settings, time cards, drag and drop. Plus many bug fixes.
<!--break-->
