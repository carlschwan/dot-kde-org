---
title: "KDE.org: New Design, New Implementation"
date:    2003-03-02
authors:
  - "numanee"
slug:    kdeorg-new-design-new-implementation
comments:
  - subject: "Hmm... doesn't validate"
    date: 2003-03-02
    body: "Check out:\n<p>\n<a href=\"http://validator.w3.org/check?uri=http://www.kde.org/&charset=(detect%20automatically)&doctype=(detect%20automatically)&verbose=1\">validate www.kde.org</a>\n<p>\nthe sight doesn't validate ( but it's admirably close, clearly they\nwere trying ).  Perhaps these issues could be fixed.\n<p>\nEd\n"
    author: "Ed Warnicke"
  - subject: "Re: Hmm... doesn't validate"
    date: 2003-03-02
    body: "That's okay... Your post doesn't validate with a dictionary, either. :)\n\n'the sight' -> 'the site'.\n\n\"but it's admirably close, clearly they were trying\" \n\nYou too :)\n\n::grins and legs it::"
    author: "Dawnrider"
  - subject: "Fixed!"
    date: 2003-03-02
    body: "This Page Is Valid XHTML 1.0 Transitional!"
    author: "Navindra Umanee"
  - subject: "Re: Fixed!"
    date: 2003-03-03
    body: "Yep, thx to Jason, he submited the patch first ;)"
    author: "Christoph Cullmann"
  - subject: "Re: Fixed!"
    date: 2003-03-06
    body: "So, put the WC3 passed logo on it!"
    author: "James Richard Tyrer"
  - subject: "Re: Fixed!"
    date: 2003-03-21
    body: "It's broken again."
    author: "Phil"
  - subject: "Looks ok"
    date: 2003-03-02
    body: "Hmmm looks ok, the spacing between lines of text is a little big though, and I reckon the text should be left-justified, it's easier to read.\n\nOh well just personal opinion. Looks ok but maybe not 100% professional... actually I prefer the GNOME website, simpler and cleaner. Just IMHO. Seeya."
    author: "foo"
  - subject: "Re: Looks ok"
    date: 2003-03-02
    body: "Gnome.org? You can't really mean that random mix\nof colors and font-sizes? As someone who is working\nas a website-designer I sometimes wonder why \nLinux users often do not even _see_ how horrible\nsome designs are. But, perhaps that's why lots\nof people wear clothes that look like s#+# even\nif they could easily afford better-looking ones:\nThey actually think they are looking great...\nThe IRC application KVirc, i.e., is an extremly good\nprogram. But the user-interface: Yuk! But probably\nthey are even thinking they did a really cool job.\nMmm, fortunately with KDE there is some hope.\nThat's why I like the KDE project.\nKDE.org now looks very clean and stylish. KDE 3.1 itself\nwith Crystal or Noia icon set is light years ahead\ncompared to Gnome not only in graphics-design but\nalso in interface-design. Finally there is some\nprofessionalism in the design of open source software,\nBTW, Gnome.org uses Times as a font. When will this\never stop? Look at Microsoft.com, Sun.com, Corel.com, Adobe.com, etc.\nNobody uses Times. Why not? Well, a font with serifs is not\nsuitable for the internet. That's why M$'s web-fonts are only\nwith out serifs (the little bars at the end of each letter).\nA font like Times doesn't fit into a well-designed modern look.\nThat shows that Gnome.org is not really made by a professional designer.\n\n\n"
    author: "Martin"
  - subject: "Re: Looks ok"
    date: 2003-03-02
    body: "Yes I meant gnome.org. It has a plain white background, the text is normal, it just looks plain. And (again I say IMHO) plain is good for me. Personally I prefer fonts like Times - you say it's not suitable for the internet, but I like it, and as far as I know, I'm using the internet and looking at webpages right now. Therefore it must be suitable for some people :-/\n\nI won't comment on clothes, it's not really relevant. But for user interface design I will say this: icon themes, window manager themes, widget themes etc. don't really amount to much. They are only important when the applications themselves have decent interfaces (layout, placement, interaction). But unfortunately there are far too many apps for KDE and GNOME and whatever else that have awful, cheap, hacked-together interfaces. No amount of themes and icons can change or mask this. Of course this isn't exclusive to KDE.\n\nAnyway, I looked at the KDE website again - it looks worse than it did the first time! Looks cheap to me. Large sans-serif fonts look cheap to me. Search button on the right is too wide. Bla bla bla... just in my eyes. Seeya.\n"
    author: "foo"
  - subject: "Re: Looks ok"
    date: 2003-03-03
    body: "I agree with this poster.  The new KDE site is using way too large of fonts.  It really looks messy.  The big blue headers everywhere also are a bit much, perhaps if they were just that blue color as a border, than the gray color within it would look better.\n\nAnyways, it's not terrible, it's very nice.  I especially like the new logo up in the header area, very tasteful.\n\nAs for the guy who seems to know what everyone should be wearing and what fonts need to be used for web sites, give it a break buddy, it's called taste.  You can say everyone who's taste differs from your own is tasteless, but that'd make you an idiot.  Although I agree that sans serif looks better, if it was meant to be the font used on the internet, then why when not specifying a font does it default to serif?"
    author: "Travis Emslander"
  - subject: "Re: Looks ok"
    date: 2003-03-03
    body: "\"The new KDE site is using way too large of fonts.\"\n\nIt uses the font-size configured in your browser.  If you don't like it, configure your browser the way you want.\n\n\"Although I agree that sans serif looks better, if it was meant to be the font used on the internet, then why when not specifying a font does it default to serif?\"\n\nProbably because that's a decision made for mosaic, and every mainstream browser since then has put far too much effort into retaining the default look of existing browsers.\n\nThere are legitimate reasons for choosing serif over sans-serif and vice-versa, that don't have anything to do with style.  Where they can be rendered appropriately, serif fonts are quicker to read.  However, at low resolutions or small sizes, serifs cannot be rendered well, and sans-serif is quicker to read.\n\n(Can't seem to choose HTML as an encoding option at the moment, sorry for the formatting).\n"
    author: "Jim Dabell"
  - subject: "This Shames GNOME"
    date: 2003-03-02
    body: "GNOME has been working on their new website for years.  They pulled in top guns from Ximian and even their release manager but niet."
    author: "anon"
  - subject: "Lovely Improvement but..."
    date: 2003-03-02
    body: "The font size is very big. it should be the same size as it used to be. The big font looks ugly."
    author: "Rizwaan"
  - subject: "Why not hire a real designer ? Was: Lovely Improve"
    date: 2003-03-02
    body: "ack\n\nAlthough I appreciate the work on the new site, I think it could really look a bit more professional, or simply not so \"coulorful\" and wild ...\n\nAs already said, the space between the lines would be a good point to fix. And what about the colors - the make the site look like a toy-store. \n\nI'd suggest two long-term-solutions :\n\no) Hire a professional designer (or become one =)\no) Make a public contest, who can delivert the best site. And let the people choose.\n\nSorry, but this is my opinion ... :("
    author: "Marc"
  - subject: "Re: Lovely Improvement but..."
    date: 2003-03-03
    body: "But big fonts are much more readable.  Personally way to many sites make the mistake of looking good at the expense of usability."
    author: "theorz"
  - subject: "Re: Lovely Improvement but..."
    date: 2003-03-03
    body: "The question is simple: Why should we use the font size \"small\" instead of \"normal\" for the content text ? That size is relative to your settings, why enforce users which have set their \"normal\" textsize to something usable to use \"small\" and therefor perhaps \"too small\" text only because somebody can't configure his browser right ?\nI think it is the way to go to use the \"normal\" font for any content and perhaps \"small\" for the menus (as we do, as menus should not take that much room in my eyes, they allready take enough). If the \"normal\" text is too big, you should ask yourself if you need to change your settings. I don't like it to have pages that break my settings and just enforce me to set my small font up to 10 or 12pt just to read some content only because their webmasters think it would be nice to enforce some small font for their whole page."
    author: "Christoph Cullmann"
  - subject: "Re: Lovely Improvement but..."
    date: 2003-03-03
    body: "I bet you're using IE, aren't you?\n\nIn Konqueror and Mozilla, you can explicitly set the preferred font size, but in IE you're restricted to View->Text Size.  I couldn't find a way of changing \"normal\" in IE to be a smaller font size, and if I use \"smaller\" some sites are too small.  IE has no minimum font size either.\n\nI fixed it for me by not using IE.  You can't do that for other people though, so on my sites I did this in the style sheet for IE users only:\n\nbody, td, th { font-size: 80%; }\n\nAll of the other font sizes must then be relative and not explicit (eg: \"larger\", \"smaller\" or \"xx%\" instead of \"normal\", \"large\" etc).  Other elements will inherit the parent element's font size, and adjustments will be relative to the inherited size.\n\nI think the KDE site would benefit from doing the same.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Lovely Improvement but..."
    date: 2003-03-03
    body: "You want to watch out when using hacks like that.  The default font-size for IE 4 - 6 (in quirks mode) is small, which makes the 80% very small indeed.\n\nIf you only test in IE 6 (in standards-compliant mode), you won't catch this, as it uses the proper default, medium.\n\nWhy not just let your users view the site using the setting they want?\n"
    author: "Jim Dabell"
  - subject: "Perfect but..."
    date: 2003-03-02
    body: "Very good.\nNow the site is far from a XHTML/CSS compliant website - few errors keep -\n\nSo I have an item in my wishlist : I want Konqueror  to kept the previous stylesheet I choose in the \"Stylesheet\" menu after I close and reopen it. I don't want anymore that websites use cookies to do so. \n\nRmq :\nMozilla is worst than Konqui on that point. It doesn't kept the stylesheet after clicking a link on the page :)"
    author: "Shift"
  - subject: "Re: Perfect but..."
    date: 2003-03-03
    body: "Page validates now again ;) At least the front-page ;). Some other pages may still be invalid, but with the huge masses of pages we have will take some time to clean up them all, but we will trying it hard ;))"
    author: "Christoph Cullmann"
  - subject: "problem with no javascript"
    date: 2003-03-02
    body: "\nHi,\n\n  I use Netscape 4.7.8 un SuSE linux.\n  The site looks greate with JavScrpt but has a grey background\n  without JavaScript.\n\n  Q: it is possible to get it working without JavaScript ?\n\n ( the old site worked without JavaScript too)\n\n\nmfg\n\n  aotto"
    author: "Andreas Otto"
  - subject: "Re: problem with no javascript"
    date: 2003-03-02
    body: "Netscape 4.x only takes care of style sheets if Java Script is enabled, does'nt it?"
    author: "pipesmoker"
  - subject: "Re: problem with no javascript"
    date: 2003-03-02
    body: "Arrgh, Netscape 4.7 without JavaScript??? In 2003???\nWhen there are completely free alternatives?\nVery cool! BTW, my KDE 1.0 keeps crashing when I try to start K3b.\nCan't there be a separate version for KDE 1.0?\nSorry man, *get a life*!\n"
    author: "Martin"
  - subject: "Re: problem with no javascript"
    date: 2003-03-02
    body: "Please !!!\nDon't use Netscape4.x\n\nThis browser is horrible and it doesn't respect the standards. It is a waste of time for webmasters to try to create web pages compatible with it :("
    author: "Shift"
  - subject: "Re: problem with no javascript"
    date: 2003-03-03
    body: "It's not a waste of time at all.  If you @import all  your styles, netscape 4.x shouldn't pay any attention to them, which makes it equivelent to a browser without any css support whatsoever.  If your sites don't work in non-css browsers, then you are doing something very wrong.\n"
    author: "Jim Dabell"
  - subject: "Re: problem with no javascript"
    date: 2003-03-03
    body: "It works without CSS, but if you think you can get a nice colorfull experience without CSS than you are wrong, as that would cause the use of color and font tags which will break any alternate stylesheet ;) It is usable, it is readable, it even works in text browsers (even with layout if they support tables and even with content before navigation if not)."
    author: "Christoph Cullmann"
  - subject: "Re: problem with no javascript"
    date: 2003-03-03
    body: "I was replying to the person who said it was a waste of time to consider netscape 4.x when building a site.  I never implied that kde.org was broken in any way without css.\n\nUsing things like the <font> element should not break user stylesheets.  If they do, then the user-agent is broken.  Styling added with presentational html elements and attribute have a low specificity, so anything done in css should automatically override it.\n\nI do, however, think that it's a mistake to build a site with a fairly simple layout using table hacks today.  Perhaps it was acceptable a couple of years ago, but browser support has come along very well recently, and imho it really isn't that important to have older browsers have 100% of the experience of recent browsers at the expense of well-coded alternative user-agents.\n\nI also think it's a mistake to develop the site in accordance with IE 6 quirks mode.  The xml declaration is redundant, and the only thing it really achieves is to make sure that IE 6 doesn't follow the relevent web standards.\n"
    author: "Jim Dabell"
  - subject: "Re: problem with no javascript"
    date: 2003-03-03
    body: "I now the @import tips but it is always a hack.\n\nI consider that Netscape4.x is dead and that you can upgrade with other browsers following w3c standards very easily. \nIf we continue making site compatible with Netscape4.x and IE3.x then XHTML and CSS will stay a dream but I don't want that.\nFor me using @import is bad because I can't use alternate stylesheet with this :("
    author: "Shift"
  - subject: "Re: problem with no javascript"
    date: 2003-03-03
    body: "@import isn't a particularly bad hack, it's valid css.  If your server is configured correctly, it's usually a matter of a few bytes the first time you access a site that uses it.\n\nAs for making sites compatible with older user-agents, you get this for free by writing valid, meaningful html, and hiding the css.  As I said before, if you are relying on css, you are doing something wrong, it's optional for user-agents, there are plenty that will never support css.  If you aren't relying on css support, there won't be a problem with older browsers.  Okay, so I'm ignoring javascript, but the same argument applies to that as well.\n\nI don't see what precludes alternate stylesheets with @import, unless there is a browser bug I don't know about that affects this combination.  Most of my stylesheets get automatically shielded with @import by using a combination of mod_rewrite and a simple php script.\n\nThe biggest obstacle to xhtml support on the web is internet explorer, it still doesn't support it.  ie6 will be in use for years to come, essentially blocking proper use of xhtml for that timeframe (xhtml as text/html is a hack that only works because no browser implements html properly).\n"
    author: "Jim Dabell"
  - subject: "Re: problem with no javascript"
    date: 2003-03-03
    body: "Sorry for your problems with Netscape 4.x, but to fix the color problem for netscape 4 would mean to go away from CSS, as CSS works only with javascript support on in netscape4 (as netscape uses javascript code to do the css stuff internal). We have tried to keep the page as compatible to netscape 4 as the xhtml/css code allows, without breaking the possiblity to use alternate stylesheets. It doesn't look that nice in netscape 4, i am sorry, but more is not possible without breaking the atm clean code which is a no go. Therefor: hope you have not that much problems with the page and netscape and if you can update your browser, real worth the work."
    author: "Christoph Cullmann"
  - subject: "broken link"
    date: 2003-03-02
    body: "Hi,\n\n  sorry I have to post a broken link in the news section because\n  I can not find a \"webmaster\" URL (or something else) on the main site\n\nURL: http://developer.kde.org/build/compile_kde2.html\n\nBroken link:  ftp://download.kde.org/stable/2.2.2/\n\n\nmfg\n\n  aotto"
    author: "Andreas Otto"
  - subject: "Re: broken link"
    date: 2003-03-02
    body: "Look to your left, under communicate there is a contacts link. \nIf you go that way you will see webmaster@kde.org \n\nKDE TG"
    author: "KDE Turist Guide"
  - subject: "old webpages"
    date: 2003-03-02
    body: "Are all the old webpages archived somewhere (other than cvs)? It's always quite funny to look at old webpages, and I wodner how kde's first page looked.\n\nIsn't there also some internet-archive where you can have a look at i.e. microsoft's first attempt of a website?"
    author: "me"
  - subject: "Re: old webpages"
    date: 2003-03-02
    body: "Yes, let the Wayback Machine take you back.\n\nhttp://web.archive.org/web/*/http://www.kde.org\n\nIt also has Microsoft from 1996 :)\n"
    author: "Haakon Nilsen"
  - subject: "Wow. Look what Miquel had to say in 1997 about kde"
    date: 2003-03-03
    body: "http://web.archive.org/web/19971210214143/www.kde.org/comments.html\n\"Miguel de Icaza: miguel@tirania.nuclecu.unam.mx\nHave I mentioned that some of the KDE code is very well done? I really hope we can use as much code as possible from KDE. The GNOME project definetly would like to be interoperable with KDE, so in every aspect where we can stay compatible, we will try to be compatible with them. Both KDE and GNOME are good for pushing Linux into the desktop. Right now KDE is the best tool we have to increase the Linux market share, and thus it is very important.\"\n"
    author: "kannister"
  - subject: "Re: old webpages"
    date: 2003-03-02
    body: "www.archive.org"
    author: "Chris  Howells"
  - subject: "Re: old webpages"
    date: 2005-11-19
    body: "http://www.archive.org"
    author: "Per Jarle"
  - subject: "Hey, looks good!"
    date: 2003-03-02
    body: "Good work on the site guys! I think it look much nicer than the old one!"
    author: "esuvs"
  - subject: "Resemblance"
    date: 2003-03-02
    body: "Is it just me or does the new design somehow remind me of Microsoft.com? Look at all the blue.\n\nAnd the spacing between lines should be a bit smaller..."
    author: "Stof"
  - subject: "Re: Resemblance"
    date: 2003-03-03
    body: "That's what I thought. The layout is very similar too. I think the older site was a little more appealing because of the more varied colors."
    author: "Rayiner Hashem"
  - subject: "Re: Resemblance"
    date: 2003-03-03
    body: "Yea it kind of does.  Its more or less the color scheme though.  As far as web-page layouts go and\nespecially if you're not using Flash, there are only a small finite number of acceptable layouts.  The layout has similarities yes, but then you could probably find 1000 other pages that have different similarities than MS's.\n\nBut I like it on a whole.  I'd give the old layout about a B- and the new layout about a B+/A-\n\nIf they ad a ton of goddy flash with sfx for everything, I'll give it an auto A+ heh.\n"
    author: "Super PET Troll"
  - subject: "Re: Resemblance"
    date: 2003-03-03
    body: "Shades of Blue just work better, visually.\nLet  MS  switch to some other shade."
    author: "kannister"
  - subject: "Sorry, I dont like the new look"
    date: 2003-03-02
    body: "I don't like the new look, here's why:\n\nDetails\n- too big blue colored boxes (waste space, add no usability)\n- sans serif fonts (dont like them, they look cheap)\n- too much line spacing (space-wasting)\n- graphics too light, no distinctive coloring\n\nGeneral\n- cheap look --> not well-designed\n- layout wastes screen space --> bad on small displays\n- link boxes: too large --> irritating, need more time to find stuff\n\n\n\n"
    author: "Stefan"
  - subject: "Re: Sorry, I dont like the new look"
    date: 2003-03-02
    body: "I don't completely like the new look either. It's a good start, but I think it can easily be made a bit better.\n\nThe 'clean' design is great, but I think it has been overdone a bit. I especially like the new K Desktop Environment logo on the current page; the light blue look is good, the font is very well chosen. On the other hand, the screenshot mentioned above looks a bit too 'cluttered', but has some very cool elements.\n\nFor example, I think it would look better if the 'gray' menu backgrounds where changed into the light blueish color as shown in the screenshot above.\n\nOf course, where would the Open Source community be if everyone one just said 'I want it this way', and then sat on their ass and waited until someone fixed it...so, I deployed my 1337 Gimp-skills *cough*, and made up some quick&dirty screenshots to show what I mean....I combined some elements of both 'themes'.\n\nShot 1: nothing changed, only the menu now has a lightblue background. Looks a lot better IMHO. Maybe the blue should be even lighter then shown here.\n\nhttp://www.havinga.nu/kde-lightblue-menu-bg.png\n\nShot 2: I think the font in the 'other' theme's menu looks a lot better (maybe that's just my browser messing up though). Also, the blue bars are a bit smaller, maybe some of the 'eye candy' fits in as well...you get the idea. This is a real quick mockup, you can see that the bars are not equally big etc., but it's just to give a quick impression of what it could look like.\n\nhttp://www.havinga.nu/kde-mixed-styles.png\n\nAny comments on this?"
    author: "Wilke"
  - subject: "Re: Sorry, I dont like the new look"
    date: 2003-03-02
    body: "Oops, the 'other theme' I'm referring to is actually below this message, not above. But you get the idea :)"
    author: "Wilke"
  - subject: "Re: Sorry, I dont like the new look"
    date: 2003-03-02
    body: "I really like second one."
    author: "Guenter Schwann"
  - subject: "Re: Sorry, I dont like the new look"
    date: 2003-03-02
    body: "The second screenshot looks fantastic."
    author: "David Walser"
  - subject: "Re: Sorry, I dont like the new look"
    date: 2003-03-02
    body: "Very nice. I hope your changes are incorporated."
    author: "anonymous"
  - subject: "Re: Sorry, I dont like the new look"
    date: 2003-03-03
    body: "The second picture is light years ahead of the current design .. that is how agonising it is to design websites .. just a few small changes can make a world of difference.\n\nPlease incorporate these changes."
    author: "Sparky"
  - subject: "This one looks better imo"
    date: 2003-03-02
    body: "http://www.kde-look.org/content/preview.php?file=4582-1.jpg"
    author: "pe"
  - subject: "Re: This one looks better imo"
    date: 2003-03-02
    body: "full ack ! .... horny *g*"
    author: "Marc"
  - subject: "Re: This one looks better imo"
    date: 2003-03-02
    body: "I forgot - to be constructive : \n\nCan't we just get rid of the current page, and replace it with the one the link is pointing to ? \nOr at least _make a vote_ ? Btw, whose idea was it to replace the page ?\n\n"
    author: "Marc"
  - subject: "Re: This one looks better imo"
    date: 2003-03-02
    body: " definitely!\n"
    author: "Andorsch"
  - subject: "Re: This one looks better imo"
    date: 2003-03-02
    body: "Full ACK - BUT:\nThe current version is a big step in the right direction.\nAnd now, when there is a completely CSS based page,\nit will be a lot easier to make the changes necessary to\nachieve the version you mentioned. I only see one problem\narise: Who is making all the graphics for each topic?\nIf you want a consistent look you'll have to create an\nicon for each topic on all KDE pages. A solution for that\nproblem must be decided upon in advance - perhaps\nthere is some different version without images for minor\ntopics, follow-up pages or so. But for the main\npage of kde.org this is really perfect."
    author: "Martin"
  - subject: "Re: This one looks better imo"
    date: 2003-03-02
    body: "Yeah, but how big is that compared to the current one? I see a lot of graphics on there. Remember that well over two thirds of net users are dial-up modem users."
    author: "Joeri"
  - subject: "Re: This one looks better imo"
    date: 2003-03-03
    body: "I commented before on the previous mock-up shot. This is better as it brings back Konqui!!\n\nI am sad to see Konqui gone .. bring him back!!"
    author: "Sparky"
  - subject: "Re: This one looks better imo"
    date: 2003-03-03
    body: "Yeah, we still need some more pictures on the page, that's true.\nIf somebody wants to help with additional artwork, mail me cullmann@kde.org or kde-www@kde.org. But keep in mind: that page is for information, not only as promo. We can't have a big image for each and every topic or title, not everybody has a 1mbit connection to net, many of our users like me use some slow dial-in :/"
    author: "Christoph Cullmann"
  - subject: "Re: This one looks better imo"
    date: 2003-03-03
    body: "With \"alt\" tags and because browsers cache pictures, I don't think that would be a big problem.\n\n"
    author: "Roland"
  - subject: "Looks very different in every Browser"
    date: 2003-03-02
    body: "Just assume that almost nobody will ever change the default font settings.\nKonqueror uses the best fonts on my 1280*1024-Screen.\n\nThe standard font and the headlines are too big in Mozilla and Opera 7.0.0. pr1.\nThis causes large boxes, large distances between the words.\n\nOnly the underlined links look good in every browser.\n\n\nI don't like this font color very much:\n#3991EE\n\nAnd then I just klicked into that K-banner on top to find a value that fits into this design.\n#ACBEE6\nTry to build an environment out of the color range that is used by this very nice banner. It's just a matter of harmony :-)\n\nThese link-boxes (awful grey background color #EEEAEE) are much too wide. No link gets close to the border.\n\nTry to make smaller box-headlines and decrease the height of that font background. Then try to use the look of your default KDE-theme!\nYou know how the new kde title bars look like?\n\nAnd then just little spaces between this link boxes.\n\nThx for reading\n"
    author: "Schugy"
  - subject: "Little bit OT but a good reading."
    date: 2003-03-02
    body: "Due the article from Nicholas Petreley yesterday on Slashdot. Now even Miguel de Icaza writes about GNOME. Even he admits that GNOME has seriously fallen behind KDE. Read more\n\nhttp://mail.gnome.org/archives/desktop-devel-list/2003-March/msg00026.html\n\nhere."
    author: "AC"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "What's with the licensing FUD from miguel - what is he talking about?\n\n\"Those with long-term visions believe strongly that the foundation\nfor building applications on Linux should be royalty free so Gnome is a\ngood choice there.\"\n\n\"end-users which [sic] do not care about the royalty issue do feel that KDE is a better\ndesktop.\"\n\nRoyalty issues???? WTF?"
    author: "steve"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "\"Royalty issues???? WTF?\"\n\nIt's just Mr. de Icaza once again voicing his affection for the LGPL over the GPL.  His pursuit of encouraging commercial software development on Unix platforms has continually influenced Miguel to spout his appreciation for the LGPL.  Since he firmly believes commercial development will not happen using GPL'ed libraries (even though there is plenty of evidence of commercial companies successfully adopting the GPL in the past), he will for the rest of time refer to such things as \"royalty issues.\"\n\nIndeed, Miguel recently refered to the GPL as \"tainted\":\n\nhttp://dot.kde.org/1044312611/1044364582/1044368376/1044370818/1044374726/\n\nJudge for yourself where his biases lay."
    author: "Cowardly Anonymous"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "OK, I'm a huge GPL fan, but I actually do see Miguel's point.  If you want to attract commercial software to Linux, an LGPL toolkit is clearly preferable to a GPL toolkit.\n\nPersonally, I don't care if there is never much commercial software available for Linux; given a choice, I will always choose the Free software alternative!  So, to me, the GPL vs. LGPL thing is a total non-issue; however, I can see why someone who wanted to attract commercial vendors would prefer the LGPL.  \n"
    author: "LMCBoy"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-04
    body: "It sounds like a good assumption, but the facts indicate that Qt's dual licensing model is superior regarding commercial attraction to Linux.  Already, IBM, Adobe, and *many* *many* of the worlds largest software companies are using Qt.  This 'royalty/tainted' FUD that Miguel is throwing around is crap."
    author: "anon"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-04
    body: "Actually, I think that has more to do with the quality of the toolkit than the licensing model.  A couple of thousand isn't much when you factor in all the other costs of developing an application.\n\nWhat I don't understand is why businesses don't use the GPLed version to develop with, and buy a single developer's license for the closed-source version, and compile with that.\n"
    author: "Jim Dabell"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-04
    body: "Sure, no one can argue that Qt is excellent and that is the largest factor in it's success ... but I think the dual licensing/cross-platform nature and the knowledge that a real stable company is behind it also probably helps for commercial third-party licensees.\n\nYou can't use the GPL version to develop and then switch to a commercial license.  That is restricted in the license AFAIK."
    author: "anon"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-10
    body: "> You can't use the GPL version to develop and then switch to a commercial license. That is restricted in the license AFAIK.\n\nWhich license?  The GPL only covers redistribution - so as long as it's kept within the organisation during development, they don't have to agree to any of the terms in it.  And until the end of the development cycle, they don't need the commercial QT, so they don't have to agree to any of the terms in that until that point.\n\nSo unless the commercial license has something to the effect of \"you may not link code to this version that was previously linked with the GPL version\", I'd say somebody doing this would be in the clear.\n"
    author: "Jim Dabell"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-06-26
    body: "jim u nonce, get out more and stop killing time on this crappy sites!\nget the bacon on kid."
    author: "ricky martin"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-06-27
    body: "too much low level spinning last nite kid and i pulled no one.   wish i was like dave the playa.  respect to the DBA."
    author: "rob field"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "Quoting a response Havoc Pennington made to Miguel's message above:\n\n\"I hope your mail won't land on Slashdot or some other web site.\"\n\nOops."
    author: "Cowardly Anonymous"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "Looks like Miguel de icaza finally realized that KDE is better than GNOME. I understand that since he runs his own company and like to get monetary to pay his employees he has realized that this is only possible if you work for and on a serious Desktop Plattform which is definately KDE. Why do you think he is working so hard on Mono, just because of the fact that C is not a good language for something like a Desktop or for rapid application development. The reason why many GNOME applications suffer today, they look unesthetical and all differently. I bet that KDE has filled all dreams that Miguel had and now he feels pissed. Look still no GNOME 2.0 or 2.2 final release from Ximian, what are they waiting for ? He likes the way KDE works but wished it was GNOME that works like this. Yeah this problem many people have these days. They stare on KDE and say wow 'my dream Desktop' and then they say 'shit that it isn't GNOME'. Look how many people recently converted from GNOME to KDE and the amount is growing."
    author: "Mogwai AC"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "Yeah he admits that KDE is better. Congratulations guys, you have won the desktop war.\nYou can stop actively flaming GNOME down now. It's over. You have won."
    author: "Stof"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "What to do to get rid of the GNOME trolls?  They wont go away."
    author: "anon"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "No offense but I only see 2 trolls here. You and Stof - while the others made good valuable points in their statements. I mean those who call other people trolls are usually those who start trolling. I mean the writings show here have value and wasn't cut out of the ass of someone."
    author: "AC"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "I would like to point out that you yourself have now called 2 people trolls.  I don't know how that will affect your 'I mean those who call other people trolls are usually those who start trolling.' theory! ;-)"
    author: "ac"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-02
    body: "My excuse is \"I haven't started\" :)"
    author: "Troll"
  - subject: "Re: Little bit OT but a good reading."
    date: 2003-03-05
    body: "This is certainly a much mellower Miguel than in years past, not to mention more pragmatic. That's good to see. Of course old habits die hard so he is still waving the \"royalty issue\" around. I think it's a dead issue because clearly it has proven to be a winner. We get solid organized development of our toolkit for GPL'd software and commercial entities get that plus a company to support them. As has been pointed out many of Gnome's partners are beginning to adopt QT. Clearly this has been a win for everyone except Gnome. Also Ximian proves bytheir existance that some business entity somewhere being involved can be a benefit. Many company support the Linux kernel in various ways too so what it really comes down to is how a partictular project should be supported. In my opinion IBM and sun can afford to pay for support and if I were going to sell QT based software it would cost me a lot less than to develop M$ .NET.\n\nWhile I'm sure it was sad for Miguel to see KDE running where Gnome was started it still has some wry humor for others. Miguel seems to be basing the ongoing existance of Gnome on the red herring of his \"royalty issue\" and the emotional and time investment of it's developers. The second is understandable from a developer perspective but in no way should be made an argument for any reason for people to adopt it. Other factors ought to prevail for users. \n\nIt was interesting that he made this statement. \"I personally (because of the emotional component described before), would like to see more work be done on the Gnome desktop and less on replicating infrastructure.\" Petrely's recent article soundly thrashed them for having abandoned their focus on infrastructure. Clearly this is crucial for how it affects your app. With Quanta we sacrificed a lot of little stuff to establish a powerful \"infrastucture\". The simple fact is you are never going to be fast, clean and powerful without it. KDE has managed to make so much progress precisely because of it. In fact they seem to be changing course to a more spartan development model in the name of \"streamlined usability\". They acknowledge that they have lost their edge and then they seem to go out and do the exact opposite of what you would think the lesson to learn is...\n\nUsers of a product follow their more technically inclined friends. Those people are called in the marketing world \"early adoptors\" and they want power and configurability because they want to get the most out of their software or they like to play with it. Where are these types of people going to go now? Spending effort to produce product without first focusing on infrastrcture is what marketing departments like M$ do to produce inconsistent and buggy software. I'm not saying that Gnome will do that... I'm saying play the odds. On top of everything else they are coming to their developers telling them they need to work harder to produce what they make and here is how to tell your user he can't have what he wants. My question is, knowing open source development, where is the fun in that?\n\nI've long advocated for two strong desktops to give people a choice. I thought Gnome 2 and 2.2 were going to be addressing a lot of this stuff that Miguel seems to be saying they don't have time for. Is it mostly delivering things we had over a year ago like AA fonts? I don't think it's really a matter of which desktop is better. I think it's more a matter of them needing to somehow learn the right lessons before they are so far behind there isn't another viable desktop besides KDE. Perhaps it is a matter of not admitting somebody else is right even if it means jumping from one lifeboat to another.\n\nFrankly it's just sad. They've gone through three desktops in two major releases and seem to be changing various other aspects of their architecture before they can bear fruit and now Miguel seems to be advocating slapping something on top and worring about the structure later. There's no point in debating. This looks pretty bad to me. \n\nBe kind when developers and users come to you..."
    author: "Eric Laffoon"
  - subject: "Not bad"
    date: 2003-03-02
    body: "I like it, but I think it could do with a larger, more distinctive KDE logo at the top."
    author: "ac"
  - subject: "I do like it"
    date: 2003-03-02
    body: "\tDespite the naysayers, I find the new look very nice and it is certainly a very good thing to have a unified look between the desktop and the web sites. Thanks Cristoph and the others for your hard work.\n\n\tI was wondering if it could be possible to be able to click on the upper left KDE logo to go back to the home page of the web site. It is kind of standard in most websites."
    author: "Charles de Miramon"
  - subject: "Doesn't render right in Konqueror 2.2.2"
    date: 2003-03-02
    body: "The search box and 3 dropdown box widgets in the top right are not full height, they look less than half normal height.  In the right column the links run over the left side of the box that's supposed to be under them.\n\nAt the top there's a short yellow bar with text on the right and left...the text is white, you can't read it.  Also the text is slightly taller than that yellow bar.  The KDE logo sticks below the the bluish box a bit.\n\nAt the very bottom (this may have been intentional, but doesn't look right) there's a light blue bar that goes all the way across the page, with a darker one sitting on top of it in the middle column.\n\nIf you want a screenshot, let me know."
    author: "David Walser"
  - subject: "Re: Doesn't render right in Konqueror 2.2.2"
    date: 2003-03-02
    body: "Kongratulations, you have found the perfect reason to update to KDE 3.1 now or 3.1.1 in 2 weeks."
    author: "Anonymous"
  - subject: "Re: Doesn't render right in Konqueror 2.2.2"
    date: 2003-03-02
    body: "Duh.  I will when that's feasible :P"
    author: "David Walser"
  - subject: "Re: Doesn't render right in Konqueror 2.2.2"
    date: 2003-03-02
    body: "And whoa, what the hell.  The thing flips through like 6 different themes while it's loading.\n\nThe first has no colors, is readable.\nThe second is a blue on gray, it's decent.\nThe third is a blue on blue, looks nice.\nThe fourth is like blue and purple, some alignment problems, otherwise ok.\nFifth is a yellow and blue, looks good.\n6th is what it stays, light yellow/light purple/blue"
    author: "David Walser"
  - subject: "Usability"
    date: 2003-03-02
    body: "I don't mind the layout, but I do dislike the white background.  For people like me with light sensitive eyes, it gets rather grating after reading a lot of text via computer.  When web pages don't force their white bg down my throat, I can view it with a nicer background like a dull grey.\n"
    author: "Anonymous"
  - subject: "Re: Usability"
    date: 2003-03-02
    body: "Now the browsers can use specific stylesheet for this.\n\nIn konqui you have a \"stylesheet\" section in the prefernces where you can specifie your own stylesheet or use the GUI to create one.\n\nUse these tools. There are here for that"
    author: "Shift"
  - subject: "Re: Usability"
    date: 2003-03-02
    body: "That isn't a complete solution.  It's limited to the browser supporting it.  I also don't know of a way to achieve the same effect of just making the default background a certain color if its white, not all backgrounds.\n\nEven in the case where white could be replaced with something I picked, the color scheme might be at odds and not blend well as the site maintainer made it to look good with only white.\n\nRemoving it in the first place is much more polite.  Another issue is that the images could be more background friendly if they were made to have transparent backgrounds, instead of being on white.  Otherwise they look screwy and out of place when the bg-color is changed.\n"
    author: "Anonymous"
  - subject: "Re: Usability"
    date: 2003-03-03
    body: "More than providing additional style sheets we can't do for you, sorry. The logos will get some rework and get some transparent background then I guess, but changing the background to something more \"grey\" will perhaps help you but not the people who needs good contrast because of some other kind of eye-problems. I can understand that you have problems with the bright bg, but you must understand that there will ever be some kind of people having problems with some colors or with bright/dark/contrast/color-combinations. Therefor we have the stylesheets and I hope that doesn't make you too much problems. But thx for the feedback, never thought about such problems, thought more of problems like missing contrast."
    author: "Christoph Cullmann"
  - subject: "Re: Usability"
    date: 2003-03-03
    body: "The default stylesheet is mostly there.  The middle column only seems to specify a white bg, and the top.  The sides don't, so my default bg from KDE is filled in (#E5E5E5 I think). \n\nEven for people who don't have light sensitive eyes, it can be a problem if they've got a really old crappy monitor that has a lot of glare.\n\n(And yeah, I understand there isn't necessarily a best combo color-scheme for everyone at all times. Just pointing something out here.)\n\n\n\n\n"
    author: "Anonymous"
  - subject: "I like it"
    date: 2003-03-02
    body: "Personally I like the new design much better than the old one. All the problems with that old one - tiny text, horrible font, nasty colours has been fixed in this new version.\n\nNow everything is well laid out and clearly readable which makes it look a whole lot more professional and easier to read. Nice one."
    author: "CmdrGravy"
  - subject: "very nice"
    date: 2003-03-02
    body: "I really like the new design ! Good work and thanks you !"
    author: "quarus"
  - subject: "Alternate stylesheets"
    date: 2003-03-02
    body: "I haven't seen this mentioned, so I'll do the job. The new design has support for a set of alternate stylesheets, which effectively decides how kde.org looks in your browser. So if you have differing needs or tastes, just change the style. In mozilla, this is easy. Go to www.kde.org, View -> Use Style in the Mozilla menu, and have a look at the different styles!\n"
    author: "Haakon Nilsen"
  - subject: "Re: Alternate stylesheets"
    date: 2003-03-02
    body: "Why Mozilla? Konqueror has it too ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Alternate stylesheets"
    date: 2003-03-02
    body: "The \"yellow\" stylesheet rules the school!!  :)"
    author: "LMCBoy"
  - subject: "This is great."
    date: 2003-03-03
    body: "I always thought the old design was rather, well, ugly. I liked the colors and style of the pre-October 2000 design (no offense to anyone intended, I just suspect web design was not the speciality of the ones who made that version of KDE.org), and this new rendition is again a nice looking site. It looks clean and modern, and finally makes KDE.org comparable to GNOME.org again.\n\nAny chance the Dot will get a matching style?\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: This is great."
    date: 2003-03-03
    body: "I would need to see samples to decide but I don't think we're going to adopt all this latest technology stuff as it's obviously problematic for many people and backwards compatibility is important.\n\nBut yeah, definitely send mock-ups, etc... :)\n"
    author: "Navindra Umanee"
  - subject: "I still prefer the original site"
    date: 2003-03-03
    body: "I think the original site looked better and was more functional.\nI liked the boxes on the left edge, with links to the relevant info in small boxes.\n\nSee here: http://www.kde.org/screenshots/images/large/kde2b3_1.png\n\nThe last design (cira 2000) and this latest revision use large fonts with each link bulleted - not the best way IMHO."
    author: "Ez"
  - subject: "Is the source that builds the content available?"
    date: 2003-03-03
    body: "Hi, \n\nI like the look and feel of kde.org, and would be interested in adapting it for an internal project I am working on. I have downloaded the CVS copy, however, can't see any admin type bits that update the site without modifying pages by hand.\n\nIs the backend available?  if so, where do I get a copy?\n\nThanks\nGlen Ogilvie"
    author: "Glen Ogilvie"
  - subject: "Re: Is the source that builds the content available?"
    date: 2003-03-03
    body: "We modify the pages per hand ;) As we use the header/footer templates that is even less work than using any content-managment system. Yes, you need to know html but in the end: this page is written and modified mostly by developers, translators and artists which should be able to add some basic html tags into their stuff ;) And as we keep the whole page in cvs we have good control about who and when has changed something and we uses acls to ensure not everybody can change for example the frontpage out of fun ;)"
    author: "Christoph Cullmann"
  - subject: "NEW Design Feedback"
    date: 2003-03-03
    body: "The design of KDE's website is definitely a major improvement in every area. It is very usable, cheerful, easy on the eyes, consistent, full of features and takes up little bandwidth. The site reflects the image of what we want people to see in KDE perfectly.\n\nWho wouldn't want a usable, good looking, feature rich, cheerful consistent and not very demanding desktop. I know I would! For those criticizing the fonts, you are greatly mistaking, on my Xandros box with Freetype 2 enabled in Mozilla, those fonts look gorgeous and I do not need to squint or manually change the font size to see it well.\n\nKDE.org vs GNOME.org\n\nIn my opinion KDE.org outshines GNOME.org by quite a long shot. First of all it is much easier to read the text without manually changing the size and in the naviagtion panel I can not even change the size. This brings me to the next complaint, GNOME's site is far more resource hungry than KDE's and in effect loads slower and is harder on their server. I also find it does not have good usability or consistency in it. Professional websites need to have their navigation panel on all web pages for easy access to the user and the style should not dramatically change. KDE achieves this well, GNOME is another story entirely. Their website is also gloomy and the contrast between a gray background and black text is nowhere near as good as black text ona  white background like on KDE's page. These are only a few apparent problems, but there are many more.\n\nProblems with KDE.org:\n\n- Doesn\u0092t display too well in Explorer, navigation panles stretch up mroe than they should.\n\n- Only a part of KDE related websites have adopted the new design and color-scheme.\n\n- At the top right there is a \"Choose your location.\" drop down menu. The idea it conveys is fundamentally wrong, someone's language is not determined by my location in many cases. For example there are many people in the US who prefer to communicate with another language online. Therefore the dropdown menu should be changed like this. \"Choose your language\" and the countries should be replaced with the language it is meant for.\n\nAs a web-designer I have to say you guys have done an outstanding job with your website as long as you convert all your websites to that design and fix a few more minor issues here and there.\n\nA GREAT desktop environment DESERVES a GREAT website!\n"
    author: "Alex Radu"
  - subject: "Re: NEW Design Feedback"
    date: 2003-03-03
    body: "Forgot to add an issue to problems:\n\n- KDE logo should bring user to mains ite"
    author: "Alex Radu"
  - subject: "Re: NEW Design Feedback"
    date: 2003-03-03
    body: "Also Konqueror should also appear somewhere on the website. For example near \"Conquer your desktop\".\n\nCheckout this pic: http://www.kde-look.org/content/preview.php?file=4582-1.jpg for mroe dieas.\n\nWHATEVER YOU DO DON'T CHANGE THE FONTS! THEY WERE PERFECT, and I'M sure if you hada   poll wiht more people they would think the same."
    author: "Alex"
  - subject: "Re: NEW Design Feedback"
    date: 2003-03-03
    body: "Last suggestion is to make LAtest Looks section under applications."
    author: "Alex"
  - subject: "Re: NEW Design Feedback"
    date: 2003-03-03
    body: "here my comments:\n- yeah, still some IE problems, agreed, but not that much our failure, more ie probs with CSS, too bad\n- transition of the other pages: we work hard to move them over to the new design in the next weeks/months, but much work :P\n- Choose location should not seen as language chooser at all, location fits it. The different pages are not that translated versions, they are regional versions of the kde.org page. They have some different content and are no 1:1 translation. That means on kde.de you will found more content which interest the german users and not that much only a german version of kde.org. Therefor it says more to give the countrynames than the languages. if the pages would only be translations than you would be right in deed ;)\n\n- kde logo link to mainpage: will be done, but first I must get home ;)"
    author: "Christoph Cullmann"
  - subject: "I really really much prefer this over the old site"
    date: 2003-03-03
    body: "The old one was ugly, ugly, ugly and of course, ugly. Did I mention ugly? No... it wasn't that bad (I liked it a lot when it first came out), the big problem was that it was horribly inconsistent between subsites (e.g. koffice.org). Hope this would fix the problem. One thing I don't get is that why are the fonts so freaking big? Most websites use 12px ot 10px, but not the sizes you guys are using.... If there is anyone visually impaired, I'm sure he would be using a browser that allows text to look bigger."
    author: "rajan r"
  - subject: "It rulez"
    date: 2003-03-03
    body: "The new design simply rulez."
    author: "AC"
  - subject: "please please ..."
    date: 2003-03-03
    body: "No more blue .. no more blue :("
    author: "anonymous ..."
  - subject: "Re: please please ..."
    date: 2003-03-03
    body: "I agree, I don't like blue, it's cops/right-wing/M$ colour ! Blue everywhere is making people sick in their mind, yes it does, really, why do you think corrupted politicians use blue background ?\nTry to use some black, red, orange & yellow instead but no more blue indeed !\nI prefered the formier site, lol !"
    author: "Herv\u00e9 Parissi"
  - subject: "use of apache forrest"
    date: 2003-03-03
    body: "Has anyone look at using forrest for KDE ?\n\nhttp://xml.apache.org/forrest/index.html\n\nforrest is now being used to generate the pages for most of the apache projects."
    author: "ac"
  - subject: "Hooray!"
    date: 2003-03-03
    body: "Keramik, Crystal, kde.org...\n\nWhere would KDE be without the help of these wonderful teenagers who are making it what it is today: a big shiny ball of blue? I thank God KDE doesn't let trained designers interfere with the appearance of the project. Who wants subtle when you can get loud? Who wants elegant when you can get flashy?\n\nCarry on my fine lads. I know you won't rest until you've given everything in KDE that signature baby blue glow. And I love you for it."
    author: "Heathen"
  - subject: "Change it if you don't like it."
    date: 2003-03-03
    body: "And feel free to contribute your own CSS color/design scheme to the existing seven available ones."
    author: "Datschge"
  - subject: "Re: Change it if you don't like it."
    date: 2003-03-03
    body: "I wish they worked for me (tried konqueror CVS Head, mozilla 1.3b) I expected it to work *at_least* with konqueror!\n\nAhhhhh... I know.... It just works with IE!"
    author: "Myself"
  - subject: "Re: Change it if you don't like it."
    date: 2003-03-06
    body: "The www.kde.org server currently doesn't allow using cookies and GET posts so no browser can fix it at the moment. http://kate.kde.org/media/settings.php and http://czechia.kde.org/media/ however are on other servers with the correct setup and work fine, check them out to see how it is intended to look and work."
    author: "Datschge"
  - subject: "Re: Hooray!"
    date: 2003-03-03
    body: "Green start button, red close button, pop-art window colors,....\n\nWhere would Windows be without the help of these wonderful designers \n(Frog Desing) who are making it what it is today: a big shiny ball\nof all colors at once? I thank God Microsoft doesn't let\nthe taste of ordinary end-users  interfere with the appearance of\nthe project. Who want subtle when you can get loud? Who\nwants elegant when you can get flashy?\n\nCarry on snobbish designers. I know you won't rest until you've given\neverything in the world something _YOU_ call design. And I love you for it."
    author: "Martin"
  - subject: "Re: Hooray!"
    date: 2003-03-03
    body: "Microsoft has a monopoly with Windows. They don't have to do well."
    author: "Heathen"
  - subject: "strange - can not change color under settings"
    date: 2003-03-03
    body: "Hi!\nI tried to choose a different coloring scheme under settings, but it did not work with konqueror 3.1.0 or mozilla 1.2.1.\n\nI have javascript enabled and cookies enabled.\n\nI can see the cookie coming in, but after I accept it, the selector displays again the kde-windows coloring...\n\nregards,\nandorsch"
    author: "Andorsch"
  - subject: "Re: strange - can not change color under settings"
    date: 2003-03-03
    body: "Same problem here. As I posted in a previous thread, this thing doesn't seem to work for me. Tested on konqueror from cvs head and mozilla 1.3b. Mmmmm... maybe we should use IE ;-)\n\nBTW, I'm getting zope errors quite often in these threads... may be quite busy?"
    author: "Myself"
  - subject: "php-nuke"
    date: 2003-03-03
    body: "Looks like KDE.org can use php-nuke for its current layout."
    author: "Anonymous"
  - subject: "Classic mistake, bg but not fg/txt color set"
    date: 2003-03-04
    body: "My default settings are light text on a black background.  The site's default settings are white background, no text color set.\n\nThat means I get my white text color default, on the site's white background color default, and I can't see it unless I select all, or change style sheet options. \n\nIf the background is specified, the forground/text should be as well, to avoid just this sort of problem.  Unfortunately, this is an all-to-common mistake, both according to the tutorials at http://richinstyle.com , which specifically caution AGAINST this mistake, and from my own experience.\n\n(Disclaimer:  I am not a web designer.  I just suffer when folks that are make this sort of mistake on something that WOULD be common sense, if they stopped to think about it.)\n\nDuncan (No e-mail address because last time I posted that here, I had to dump that address for the spam it generated and create a new one.  Luckily it was one I could do that too.  There should be a warning!)"
    author: "Duncan"
  - subject: "switch.php doesn't work"
    date: 2003-03-04
    body: "it does seem http://www.kde.org/media/settings.php is there for nothing now... unimplemented?\nwill it be working some time in the future?"
    author: "luci"
  - subject: "Re: switch.php doesn't work"
    date: 2003-03-05
    body: "it must be a bug, 'cause e.g. at http://czechia.kde.org the stylesheet switching at settings.php does work well with the switch.php script..."
    author: "luci"
  - subject: "Splendid"
    date: 2003-03-04
    body: "I see that the new site has received some criticism, and I hope it doesn't removes focus from all the great work. Personally I have nothing but praise for the site, I think it looks gorgeous and is one of the most well-designed sites I have seen. Clear and simple yet beautiful. I can't really think of anything that I dislike about it."
    author: "EUtopian"
  - subject: "A suggestion"
    date: 2003-03-05
    body: "Please make the text light up or change size when the mouse is voer the links on the navigation panek"
    author: "Alex"
  - subject: "Re: A suggestion"
    date: 2003-03-05
    body: "Good idea, but please avoid changing the size or making it bold.  I (and many others) hate text jumping around on mouseover, it makes a site less usable.\n\nMy preference would be for a slightly lighter shade of grey to be used as the background for the link.\n"
    author: "Jim Dabell"
  - subject: "Indeed"
    date: 2003-03-06
    body: "Even better than my idea, change the background color of the row in the table for the text. PERFECT.\n\nPLEASE incorporate this!"
    author: "Alex"
  - subject: "multilingual site ?"
    date: 2003-03-05
    body: "can we expect, some day or other, a multilingual site and forum like mandrakeclub ?"
    author: "capit. igloo"
  - subject: "Does not work with squid proxies..."
    date: 2003-03-05
    body: "The CSS styling does not work with mozilla through\na squid proxy.\n\nWhen I look at my logs I see rather strange lines\nbeing served\n\nhttp://www.kde.org/media/styles/standard.css - NONE/- image/gif\n\nthe style sheets are being severed as images...\nWhen I turn the proxy off everying looks OK."
    author: "AM"
  - subject: "Re: Does not work with squid proxies..."
    date: 2003-03-05
    body: "Sounds like your proxy is broken in some way.  I've just checked that address, and it's certainly being served as text/css:\n\nHTTP/1.1 200 OK\nDate: Wed, 05 Mar 2003 13:21:52 GMT\nServer: Apache/1.3.26 (Unix) PHP/4.1.2\nCache-Control: max-age=28800\nExpires: Wed, 05 Mar 2003 21:21:52 GMT\nLast-Modified: Tue, 04 Mar 2003 16:03:33 GMT\nETag: \"ed00d-c26-3e64ce55\"\nAccept-Ranges: bytes\nContent-Length: 3110\nConnection: close\nContent-Type: text/css\n"
    author: "Jim Dabell"
  - subject: "IT DOESN'T WORK! MUST BE A BUG!"
    date: 2003-03-06
    body: "Ok, let's put things clear. Am I the only one that feels that this CSS stylesheets thing doesn't work? I tested:\n\n* with/without proxies\n* from two different internet providers/locations\n* 3 different browsers (konqi from cvs, mozilla, opera).\n* Cookies and java/javascript were on in all cases, \n\nand ***Neither works***\n\nThis means that it doesn't seem to be my network config, nor my browser's problem. IT MUST BE A BUG on the site.\n\nA Couple of questions. Is there a script that says:\n\n\tif (in UK) then make_it_not_work\n\n??? That's the only answer I find :-(\n\n\n"
    author: "Myself"
  - subject: "Re: IT DOESN'T WORK! MUST BE A BUG!"
    date: 2003-03-06
    body: "The www.kde.org server currently doesn't allow using cookies and GET posts so no browser can fix it at the moment. http://kate.kde.org/media/settings.php and http://czechia.kde.org/media/ however are on other servers with the correct setup and work fine, check them out to see how it is intended to look and work."
    author: "Datschge"
  - subject: "Re: IT DOESN'T WORK! MUST BE A BUG!"
    date: 2003-03-06
    body: "Thank you very much, ... at last I saw the colors changed! :-)"
    author: "Myself"
  - subject: "Re: IT DOESN'T WORK! MUST BE A BUG!"
    date: 2003-03-06
    body: "They are worse...\n\nEntirely blanck pages with a squid proxy.\n\nthe page title is stranegly \"Gif image 1x1 pixels\"\nunder mozilla.\n\nWithout a proxy it works"
    author: "AM"
  - subject: "Re: IT DOESN'T WORK! MUST BE A BUG!"
    date: 2003-03-06
    body: "Then it sounds like you have an over-zealous ad-blocker.  Those are the only things I can think of that routinely replace things with 1x1 images.\n"
    author: "Jim Dabell"
  - subject: "Re: IT DOESN'T WORK! MUST BE A BUG!"
    date: 2003-03-06
    body: "Following on from above comments squid proxy \n2.3.STABLE4\n\nSeveral different squid proxies used, anonymizing\nturned off so I am not stripping headers.\n\nMozilla 1.3b no css\nopera  6.1 no css\nkonqueror 3.1 css works\n\nWith no proxy all three work correctly.\nMozilla set to http/1.0. No pipelineing\nno keep alive just to be conservative.\n\n"
    author: "AM"
  - subject: "New KDE webpage Survey?"
    date: 2003-03-07
    body: "I think KDE should have a survey, if KDE folks like the new KDE webpage?\n\nItems should be:\n\n* Hate it\n* Looks Ok\n* Don't Care\n* bad\n* Good\n* Nice\n* Excellent\n\nOr something like this.\n\nI don't really like it. It looks like novice webpage. It should be compact. http://www.apple.com/macosx/ has good text spacing and layout except for those big tab above and very big icons and big J Box image. Mimimised scrolling, a probably small tab layout will do.\n\nBut I think should be replace by now with the negative comments. Please redo the webpage and then have a SURVEY. \n\n\n"
    author: "NS"
  - subject: "Re: New KDE webpage Survey?"
    date: 2003-03-08
    body: "Jeeez! Please give a better example. The page you suggest (apple one) has (IMHO) one of the worst ever designs for webpages. Reasons:\n\n* No \"about\" page: what the heck is MacOSX? Weird name. Not a single word mentioning what it is. This is the most irresponsible thing a web designer can ever do.\n* Fonts are tiny. They're specifying 9px and 10px fonts in the stylesheets!!!! That's not the correct way to do things, man. \n* The menu in the left is \"too mixed\" with the text, so that the text moves left and right... not nice to read.\n* The box photo and icons that are showed as main \"menu\" look... well messy and silly. The second one is just my opinion, but I guess most agree on the term \"messy\"\n* The site is not configurable at all compared to kde.org, since it's pixmap based, and it takes ages more to load\n* I doubt it supports easily accessing in text mode (blind people, wap phones, console browsers as lynx (without fb graphics ;-) ),....)\n* It's not standards compliant: see http://validator.w3.org/check?uri=http%3A%2F%2Fwww.apple.com%2Fmacosx%2F\n\nIn contrast, kde.org:\n\n* Has more intelligently chosen the fonts\n* Is standards compliant: http://validator.w3.org/check?uri=www.kde.org\n* Is much more tidy to find easier things\n* There's of course loads of info about \"what's kde?\"\n* It's configurable\n* Mainly text based: fast! Dialup users appreciate it\n\n\nOk, maybe it doesn't fit to the tastes of some people, but... come on, don't even try to compare to that crap webpage.\n\nRegards,\n\n\tU\n\nPS: I probably forgot some other complaints, but the list was already too long ;-)"
    author: "Myself"
  - subject: "Shame on you. New look does not work on Konq 2.2.1"
    date: 2003-03-08
    body: "You must be kidding guys. The new web site looks problematic in Konqueror 2.2.1. \n1. Button heights are 4 ro 5 pixels\n2. Tables do not align\n3. Resizing spoils. \n4. Colors change during loading.\n\nDo not blame or accuse me. I've been using KDE ever since and my setup is really OK. You should have checked. \n\nCheck out the file I've attached."
    author: "Mustang"
---
The KDE Web Team is proud to present a new and exciting design for  the <a href="http://www.kde.org/">official KDE site</a>!  This is the first overhaul of the flagship homepage since Kurt's vastly successful <a href="http://dot.kde.org/973021769/">update</a> more than two years ago.    The KDE Web Team has maintained a focus on standards-compliance as well as improved the overall <a href="http://usability.kde.org/">usability</a> and <a href="http://accessibility.kde.org/">accessibility</a> of the site.  Also featured prominently is an overhaul of the content as well as the implementation of a whole new design matching the shiny new Keramik/Crystal look from KDE 3.1.  
<!--break-->
<p>
Over the next few weeks and months, even more of the KDE.org family of sites will be migrated over to the new target.  Meanwhile, feedback and bug reports are welcome at <a href="mailto:webmaster@kde.org">webmaster@kde.org</a>. Maintainers of KDE mirrors:  Please take note of the  <a href="http://www.kde.org/mirrors/web_howto.php">updated requirements</a> for hosting mirrors due to internal changes.  
<p>
The new maintainers of KDE.org are Christoph Cullmann, Rainer Endres, and Jason Bainbridge. The design of the new website is based on work by Sebastian Faubel. 
<p> 
Chris Howells, Dirk Mueller, Olaf Jan Schmidt, Datschge and Neil Stevens have contributed at various levels performing either invaluable grunt work or contributing designs and ideas.  
<p>
There are also many people who have also contributed feedback and fixes -- please let us know if we have forgotten anyone who should be listed here.  
<p>
Huge kudos go out to any and all who have been involved!
<p>
<i>A note from <a href="mailto:cullmann@kde.org">Christoph Cullmann</a></i>:
<blockquote>
<p>
The whole process of redesigning and restructuring of kde.org began at the end of 2002.  After many heated discussions, a multitude of drafts and a lot of fun, we finally reached a state that we felt deserved deployment.
<p>
What has been done?
<ul>
<li>A new default design matching the current look of KDE has been created.</li>
<li>Much content has been reviewed and updated, and the menu structure has been updated to reflect usability concerns.</li>
<li>We have adopted usage of the latest web technology including XHTML and CSS1/2, while maintaining compatibility for older browsers as much as possible.</li>
<li>The underlying PHP scripts and HTML code have been overhauled or altogether rewritten.  Maintainers of web mirrors should therefore consult the updated <a href="http://www.kde.org/mirrors/web_howto.php">KDE Mirror HOWTO</a>.</li>
</ul>

We hope to port over the remaining KDE pages in time. If you wish to help in this process or otherwise contribute new artwork, patches or bug reports, please contact us at <a href="mailto:webmaster@kde.org">webmaster@kde.org</a>. 
<p>
As one of the new maintainers of KDE.org I would like to thank the people involved in redesigning process for their invaluable contributions.  I sincerely hope that users will enjoy the new website, and find it a worthy replacement to the old one.
<p>
</blockquote>