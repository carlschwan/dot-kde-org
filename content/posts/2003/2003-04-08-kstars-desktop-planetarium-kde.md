---
title: "KStars: A Desktop Planetarium for KDE"
date:    2003-04-08
authors:
  - "LMCBoy"
slug:    kstars-desktop-planetarium-kde
comments:
  - subject: "Very Good"
    date: 2003-04-08
    body: "The entire virtual universe looks amazing!\nKongrats Jason, this is really a big and hard work!"
    author: "Anton Velev"
  - subject: "A job well done"
    date: 2003-04-08
    body: "I must add a congratulations too.  I was impressed when I saw the first versions of this a while back.  It's something I always wanted to see in KDE, but never had the time to help with.  I'm lost without my star guide from my old Palm Pilot, but this is far more impressive.\n\nFor those interested in astronomy, either as a hobbyist or a professional, KDE is a great platform to start porting the old astronomy apps and writing new ones.  It would be great to see some IRAF enhancements in KDE, such as a kgterm, kimtool, etc.  Also an IRAF cl syntax highlighter for Kate.\n"
    author: "George Staikos"
  - subject: "Bug with Kstars and Gnome desktop?"
    date: 2003-04-08
    body: "There is a bug that happens on my Red Hat Linux 9 and KStars under Gnome. I don't know which program's fault is, but I know that it happens only when KStars (latest version) is loaded on my 1600x1200@24bit@75Hz Gnome desktop, with the \"nv\" 2D driver.\nSo, the problem is that when KStars is loaded (not maximized), all the other apps are really slow! Even typing on a text editor is slow! It just slows down everything, until I minimize or close down Kstars!\n\nAny ideas from the authors? Thx."
    author: "Gentu"
  - subject: "Re: Bug with Kstars and Gnome desktop?"
    date: 2003-04-08
    body: "I think KStars uses OpenGL. If you're using the 2D driver, then that rendering is being done in software by mesa. It's probably grabbing all the cpu time it can get, which is why the other apps will be slow. Try running some other OpenGL programs and see if you get the same problem. You could use NVidia's supplied drivers which perform very well for 3D, unless you're a \"free software only\" type of person, in which case you'll just have to live with it. Them's the breaks."
    author: "Psiren"
  - subject: "Re: Bug with Kstars and Gnome desktop?"
    date: 2003-04-08
    body: "Hi, \n\nWe don't use OpenGL yet, so that can't be the issue.  I used to have a problem where my X server would eat all of the CPU when KStars was opened, which sounds similar to what you are seeing.  Try running 'top' while kstars is running, see if it's X that is using the CPU.  You might try upgrading to this new version.\n\nSend me an email (kstars AT 30doradus DOT org) or open a bug if the upgrade doesn't work.\n\nregards,\nJason"
    author: "LMCBoy"
  - subject: "Screensaver?"
    date: 2003-04-08
    body: "What about making it a screensaver also? So when running as a screensaver KStars starts in fullscreen mode. Would be nice"
    author: "Peter"
  - subject: "Re: Screensaver?"
    date: 2003-04-08
    body: "Or a wallpaper?"
    author: "EK"
  - subject: "Re: Screensaver?"
    date: 2003-04-08
    body: "Yes!!!! Like QtVision does it when it goes desktop mode (TV picture as wallpaper/on root window)\n\nThat would be nice!"
    author: "Sangohn Christian"
  - subject: "Re: Screensaver?"
    date: 2003-04-08
    body: "Can't wait to see that.\nUnfortunatly, QTVision does not seem to find devices on mandrake 9.0"
    author: "a.c."
  - subject: "Re: Screensaver?"
    date: 2003-04-08
    body: "Sounds like you don't have the Xv extension loaded in your X server. This is a common error and is in the README file.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Screensaver?"
    date: 2003-04-08
    body: "http://edu.kde.org/kstars/todo.php\n\n;)\n\nJason"
    author: "LMCBoy"
  - subject: "Another astronomy software for KDE"
    date: 2003-04-08
    body: "[shameless self promotion]\n<p>\nIf you like KStars and if you have a 3D card check out Celestia:\n<a href=\"http://www.shatters.net/celestia/\">celestia</a>\n <p>\nyou can download RPMS for Mdk 9.0 and Suse on SourceForge\n<a href=\"http://sourceforge.net/project/showfiles.php?group_id=21302\">[sourceforge.net]</a>\n<p>\nAn rpm for Mdk 9.1 is available in the contrib folder of Mandrake mirrors.\n<p>\nYou can browse the handbook (in development) here:\n<a href=\"http://teyssier.nerim.net/celestia/doc/\">doc</a>"
    author: "Christophe Teyssier"
  - subject: "Re: Another astronomy software for KDE"
    date: 2003-04-08
    body: "Celestia is awesome.  It's definitely been an inspiration for KStars.\n\nthanks,\nJason"
    author: "LMCBoy"
  - subject: "Re: Another astronomy software for KDE"
    date: 2003-04-08
    body: "Actually the star info in the RMB popup menu in Celestia is directly inspired from KStars, so thank you too!\n\nI was thinking of having some integration with KStars too, like a [Point this object in KStars] entry in the popup menu. Does KStars has a DCOP interface?"
    author: "Christophe Teyssier"
  - subject: "Re: Another astronomy software for KDE"
    date: 2003-04-08
    body: "> Does KStars has a DCOP interface?\n<p>\n<a href=\"http://edu.kde.org/kstars/handbook/dcop.html\">It sure does!</a>\n<p>\nDoes celestia have one?"
    author: "LMCBoy"
  - subject: "Re: Another astronomy software for KDE"
    date: 2003-04-08
    body: "No, not yet but we already have a script language so it should be easy to add.\n\n"
    author: "Christophe Teyssier"
  - subject: "Feature request"
    date: 2003-04-09
    body: "I was actually using this program to help me figure things out while using the Keck telescope in Hawaii a couple of weeks ago. Unfortunately one feature that I would really like (perhaps its there but I couldn't find it) is the ability easily add new objects. I know that its possible to load them from a text file - but I couldn't figure out the format that was needed and it would be really nice to have a dialog box for doing a quick-and-dirty addition of a new object.\n\nReally nice program though. :-)"
    author: "dr_lha"
  - subject: "Re: Feature request"
    date: 2003-04-09
    body: "Having a GUI for adding custom objects is a good idea.  In the meanwhile, you can find details about the custom-file format in the file README.customize.  Basically, each line consists of colon-delimited fields:\nTYPE : RA : DEC : MAG : \"Name of Object\""
    author: "LMCBoy"
  - subject: "Re: Feature request"
    date: 2003-04-10
    body: "Yes - it was really just a matter of timing. I needed to get an answer quickly so thought I'd give kstars a try. When I found out that I couldn't do it quickly I used another program so never really investigated the text format. I was a little rushed at the time, Keck time is valuable :-)"
    author: "Dr_LHA"
  - subject: "Re: Feature request"
    date: 2003-04-10
    body: "TYPE : RA : DEC : MAG : \"Name of Object\"\n\nBTW - another quick question: Can I assume by the feature above that you don't use proper motions when calculating star positions? What kind of accuracy can one expect from kstars: minutes? seconds?\n\nThanks again."
    author: "Dr_LHA"
  - subject: "Re: Feature request"
    date: 2003-04-10
    body: "We don't yet use proper motions.  We correct for precession/nutation/aberration/atmospheric refraction.  Minutes?!  That's harsh, man :)  The positions of stars are as good as the SAO, modulo the lack of proper motions, which is only going to matter for a handful of stars, and only for dates far from the present day."
    author: "LMCBoy"
  - subject: "Re: Feature request"
    date: 2003-04-10
    body: "Hehe - sorry about that! Congrats on the Hubble Fellowship BTW."
    author: "dr_lha"
  - subject: "Glad this is in Knoppix"
    date: 2003-04-09
    body: "... because it's one of my favorite things to show people, really a slick piece of software. My astronomy is about as strong as my astrology (;)), so I do little besides enter a location and look at all the pretty dots, but the location db is very complete, it's great to see that even many smallish U.S. cities are listed. \n\nKids like it, too! :)\n\nTim"
    author: "Tim"
  - subject: "Kstars"
    date: 2003-05-12
    body: "What a wonderful tool for people... well done, Jason.  I know that you worked hard on this.  Hope lots of people will use it."
    author: "Mary Ray"
---
The KStars Team is pleased to announce the latest stable snapshot release of <a href="http://edu.kde.org/kstars/">KStars</a>, the powerful graphical desktop planetarium for KDE.  Recently <a href="http://www.linux-magazine.com/issue/25/KStars.pdf">featured</a> in <a href="http://www.linux-magazine.com/">Linux Magazine</a>, KStars displays an accurate representation of the night sky as seen from any location on Earth, on any date, including all of 40 000 stars, 13 000 deep-sky objects, all planets, the Sun and Moon,  and 2500 comets and asteroids.  KStars has <a href="http://edu.kde.org/kstars/screenshots.php">an intuitive interface</a> that makes it easy 
for anyone to explore the night sky.  
<!--break-->
<p>
The program works like a virtual
observatory; you can pan and zoom the display just like a telescope.  
You can also identify and track objects as they drift across the sky, 
download beautiful images from the internet, and browse topical 
webpages for more information.  The simulation clock can be paused, 
and the rate that time passes can be changed -- you can even make time 
run backwards.
<p>
New features in this release include:
<ul>
<li> <a href="http://edu.kde.org/kstars/handbook/">Updated handbook</a>, 
including new <a href="http://edu.kde.org/kstars/astroinfo.php">AstroInfo</a> articles
<li> <a href="http://edu.kde.org/kstars/screens/minorplanets2.png">2500 comets and asteroids</a>
<li> <a href="http://edu.kde.org/kstars/screens/jupiter.png">Jupiter's moons</a> (Io, Europa, Ganymede, Callisto)
<li> Solar system bodies can <a href="http://edu.kde.org/kstars/screens/mars.png">leave 
a trail</a> behind them
<li> New tools: 
<a href="http://edu.kde.org/kstars/screens/calculator.png">astrocalculator</a>, 
<a href="http://edu.kde.org/kstars/screens/altvstime.png">altitude vs time plotter</a>, 
<a href="http://edu.kde.org/kstars/screens/aavso.png">variable-star lightcurve plotter</a>,
<a href="http://edu.kde.org/kstars/screens/wut.png">"What's Up Tonight" window</a>
<li> Object details window now contains link manager, interface to 
  professional astronomy databases, and ability to add custom text notes
<li> Tag any object with a name label
<li> High-quality printouts
<li> Sky rendering time has been cut in half
<li> Startup time reduced
<li> DCOP functions added; can script complex behaviors
</ul>
<p>
 KStars is a part of the <a href="http://edu.kde.org/">KDE Edutainment Project</a>.