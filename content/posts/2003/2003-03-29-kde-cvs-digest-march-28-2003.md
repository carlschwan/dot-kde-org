---
title: "KDE-CVS-Digest for March 28, 2003"
date:    2003-03-29
authors:
  - "dkite"
slug:    kde-cvs-digest-march-28-2003
comments:
  - subject: "Thanks!"
    date: 2003-03-28
    body: "Thanks again Derek. \nI probably don't have a life since it seems that I am first to post in this week too... :)\nOk, ok, I'll go out and get me a life. :) "
    author: "138"
  - subject: "Re: Thanks!"
    date: 2003-03-29
    body: "http://new_life.sf.net\n\n\n(sorry, man, I had to... :-) )"
    author: "Xanadu"
  - subject: "Re: Thanks!"
    date: 2003-03-30
    body: "Hehe... :)\nBut I got 404 when I try to get that new life :)\n\n\"Not Found\n\n The requested URL / was not found on this server.\n\nApache/1.3.26 Server at new_life.sourceforge.net Port 80\"\n\nBut atleast I now have an explonation why I don't have a life... :)"
    author: "138"
  - subject: "Thanks"
    date: 2003-03-28
    body: "Wow, I'm the second. Many thanks Derek. Keep doing this good work!"
    author: "Torpedo"
  - subject: "Friday thing"
    date: 2003-03-28
    body: "Yep I seem to spend friday waiting for this to be released. :)"
    author: "Mark Hillary"
  - subject: "kontact/kmail integration"
    date: 2003-03-28
    body: "I wonder why the latest attempts of integrating kmail in kontact didn't make in here. Is the signal forwarding properly working already? I'm attempting to update right now, if it's not fixed I might try to mess it up myself :-)"
    author: "uga"
  - subject: "Re: kontact/kmail integration"
    date: 2003-03-29
    body: "If you are referring to the sidebar integration of the folderview: I will have a look next week.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: kontact/kmail integration"
    date: 2003-03-29
    body: "Yes, that was what I meant. Thanks! :-)"
    author: "uga"
  - subject: "KDE-CVS"
    date: 2003-03-29
    body: "The digest always makes my Saturday morning a little better. Much sleep, some coffee and the newest KDE-CVS. Hearing regularly about all the nice progress that the hard working KDE and apps developers do makes me personally feel much more involved with KDE. :)"
    author: "Chakie"
  - subject: "Remote Desktop (OT)"
    date: 2003-03-29
    body: "Does anyone know when Remote Desktop will use SOCKS?\nI tried to connect from my company to my computer at home\nbut that doesn't work and runsocks always crashes.\nI thought all KDE application use the SOCKS settings specified within\nthe control panel."
    author: "Martin"
  - subject: "Re: Remote Desktop (OT)"
    date: 2003-03-29
    body: "It uses the original VNC code which doesn't know anything about SOCKS. Actually I have never thought about that, this is the first time that I hear that somebody uses SOCKS since 5 years :)\n\n\n"
    author: "Tim Jansen"
  - subject: "Is there anyway..."
    date: 2003-03-29
    body: "To allow remote desktop on linux (KDE)  to connect to my Windows computer and vice versa using the software bundled with Windows?\n\nProblems/Suggestions:\n\n- can not see mouse when using Remote Desktop and with control of mouse and keyboard\n- no way to have an always on connection like in Windows \n- no way to define when connection will expire or if it should expire at all\n- no way to configure password or even see password after I click close on an invitation\n- little slower than Windows remote desktop\n- a way to record a desktop remotely or take a screenshot would be cool\n- autodetect type of connection tehrefore eliminating to select a lan, cable etc. connection and only allowing it for users who the program can not determine the connection of\n- way to define permissions of user using remote desktop\n\nI'm sure there are many more things that could be added, but I can not think of any now. THANKS FOR YOUR GREAT SOFTWARE!"
    author: "Alex"
  - subject: "Re: Is there anyway..."
    date: 2003-03-29
    body: "- Yes, in HEAD it supports RDP (Window's protocol). \n- Where can't you see the mouse, server or client? (and with which client?)\n- you can enable uninvited connections in KControl. You still need to be logged in though (for other cases there are also solutions possible, but they are a lot of work)\n- to avoid expirations use uninvited connections. I didn't bother to make expirations configurable yet because this makes the GUI more complicated and I didnt see a case when one hour was not ok. And IIRC you are the first person who asked for that\n- again a GUI issue. Would be possible of course, but makes the GUI more complicated.  Simple work-around if you forgot the password: just create a new invitation.\n- performance is partially a X11/XFree problem (there has been on some work done on this), partially the VNC concept (which works bitmap-oriented)\n- for a hackish way to record see http://lists.kde.org/?l=kde-promo&m=104430062329339&w=2 and http://lists.kde.org/?l=kde-promo&m=104456257121317&w=2 . Working on a better way has a very low priority for me right now\n- auto-detecting is quite hard, that's why it has not been done yet.. it also depends on the preferences of the user (would you prefer a higher quality or lower latency?)\n- permissions are a difficult issue.. i hope to finish kerberos authentication for 3.2, but i can't promise it.. for security reasons it is not a good idea to let a user log in with the regular account without using kerberos (because krfb runs with user permissions and can't be trusted).\n\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: Is there anyway..."
    date: 2003-03-29
    body: "Thanks for all the information! I use the default remote desktop which came with mandrake 9.1 using KDE 3.1. almost KDE 3.1.1.\n\n\nI just couldn't see the mouse when I was hovering over a remote desktop i was onnected to which gave me full control. I could see it just as soon as it exited the remote desktop window though. Not sure if this happens every time, I ony had full control once. I'll try again and see what happens.\n\nI do think being able to configure the amount of time before it expires is an important feature. Sometimes you might want to work for only 2 hours on a public desktop and you don't want anyone else to be allowed after that. The issue with the password is taht no matter how many invitations I make it is stilll hard to remember the cryptic passwords and I think it would be awesome to be able to define the password yourself. it is especially unforgiving because if I try to log in froma  remote desktop and miss the passowrd I can never use that invitation again.\n\nI don't know how to code... yet. But, it seems odd that it's hard to implement these 2 features. I mean when I click create personal invitation a nice window appears with the adress, password, and expiration date in what seems to bea  textbox. These 3 boxes beg to be able to be modified. The interface would not need to be changed at all, the generated defaults should still be there, but allow a person to click on that textbox and write something new. The first time I saw that i was sure I could just click in the white textbox and modify it. \n\nAnother question would the RDP protocal be faster or slower than VNC in your opinion?\n\nOkay, well it is not really high priority to auto detect, jsut more convinient. There is an app in gnome that detects wireless signal strength in the taskbar, maybe it is possible to reuse code from it.\n\nPermissions are important, but I guess when KDE gets gui for kiosk mode you could make a special locked in account for remote desktop.\n\nTHANKS FOR THE infor, I'll try your suggestions.\n\nBTW: I use GNOME sometimes and there are a few features I really loved and used. Some of the more simple ones are emblems and the drawers! God I loved those! There is nothing comparable to drawers in KDE I tried a childpanel and set the size to 1% and told it to expand to fit required items, i also made the hide button up, but it is not teh same. Especially because it does not autoresize if i remove an item until I hide and display it again. Windows also do not go in the background and i can not put it in the taskbar. Is there any drawer immitation for KDE, that is a really innovative feature if you learn it. Emblems are also helpful, though setting KDE to reflect contents of folder, logical names and special icons are a good substitute it is not quite as good. is there any KDE solution for this?\n\nThese are the features I miss from GNOME most, and that it is a little easier to use. (When i right click on empty space on the filemanager I get about 20 options not counting submenus. This is far too much an could easily be merged into 10 options. For example it gives me 4 ways to zip files, tar, bzip etc. This could be combined into zip and a  popup could ask what kind of zip.)\n\n\nSorry for my grammar and spelling. I will correct my posts next time."
    author: "Alex"
  - subject: "Re: Is there anyway..."
    date: 2003-03-29
    body: "The expiration time does not limit how long you can be connected to that desktop and the invitation can be used only for only one login. It is not intended as a way for someone to connect to his own desktop, but to invite somebody else. If you want to connect to your own desktop, configure it in KControl (control center). There you can define your own password and it will never expire.\n\nConcerning the textfields that 'beg to be modified': yes, writing these values into text fields was a huge mistake, usability-wise. Originally they were labels, but somebody wanted to cut&paste the information and it seemed like the only way to do this was to make them non-ediatble text-fields. It has been corrected in HEAD, now they are 'active labels' that are clearly non-editable but can be selected for cut&paste.\n\nRDP is definitely faster (lower latency&bandwidth) than VNC's protocol. However it is technically not possible to share a X11 desktop using RDP without integrating this functionality into the X11 server itself, as RDP makes extensive use of graphic primitives (lines, text and so on). With VNC it is easy to do since it is bitmap-oriented.\n"
    author: "Tim Jansen"
  - subject: "Re: Is there anyway..."
    date: 2003-03-31
    body: "Have you had a look at NX ? \nwww.nomachine.com\n\nIts a proxy server solution for X, it is really *very* fast. Something \nlike 10 times better than RDP or Citrix. It can even reencode \nVNC and RDP. \n\nIt consists of a GPL base layer, which is controlled (AFAIK) \nby a few proprietary apps. The plan is to keep it as a proxy - \nit allows some interesting optimisation. \n\nUnfortunately, I guess it suffers from the same problem as \nVNC right now - it can't use the real desktop. It needs some \nway to get at the X protocol....\n\nIt would be interesting to see if this could fit into the kde \nremote desktop thing\n"
    author: "rjw"
  - subject: "Re: Is there anyway..."
    date: 2003-03-31
    body: "Yes, it is possible. I am not working on it right now though, and still hope that somebody else will do it..."
    author: "Tim Jansen"
  - subject: "Re: Is there anyway..."
    date: 2003-03-31
    body: "Thanks for your replies Tim!\n\nIt's helped me a lot. Now all tehs ettings as I want them although, I still dsiagree about the way invitations work. Why not enable write acess for the text fields in the \"Create Personal Invitation\" dialog. That way the user could easily change the password or expiration date and at the same time if he liked the defaults he may just click close. Sounds like the best of both worlds, you do not even need to change the way the interface looks.\n\n\nAs for my mouse, i can see it, but it is not the same mouse that I am using in Xfree. I don't know why. Anotehr annoyance is that the desktop seems to be of lesser quality and sometimes text in the Kmenu has a red gradient through remote desktop. I'm not sure why again. Anyway most of the functionality I need is there, what's killing it is the speed.\n\nThanks a lot! Maybe somebody will come up with an innovation that will make this as fast as Windows's remtoe desktop or better. \n\nBTW: You said RDP was in head, but at the same time you said to make RDP work it needs to be incorporated into X? How can it work in HEAD if RDP is not incorporated into head? \n\nNX sounds awesome, is there anything comparable to it? Why isn't technology like it used instead?\n\n"
    author: "Alex"
  - subject: "Re: Is there anyway..."
    date: 2003-03-31
    body: "The mouse cursor is different because I cant get the shape of the mouse cursor from XFree. There is a extension that is supposed to fix this, but iirc it didnt make it into XFree 4.3. I should re-check it...\n\nWhich settings do you use in your client? If you use anything but 'high quality' in the kde client the quality will be noticably lower to increase the compression rate (enabled jpeg compression). With tightvnc the quality is lower as well.\n\nRDP is in the client, so you can access windows servers. But windows clients cannot connect to your KDE desktop.\n\nNX has the same issue as RDP (and is quite similar in the way it works): both cannot share the desktop. \n\n\n"
    author: "Tim Jansen"
  - subject: "Re: Is there anyway..."
    date: 2003-04-03
    body: "Tim, what you say is not true. NX can use \"single\napplication\" mode and run whatever X client on the\nexisting desktop. I did it and ran a xterm on testdrive\nserver. Window popped up on my desktop as if I was\nrunning on any other Linux box. I don't know why, but\nit is just a little bit slower than when running\ninside a full remote session. I tried also Mozilla\nand Konqui and performances were really not bad."
    author: "Truemlizer"
  - subject: "Re: Is there anyway..."
    date: 2003-04-03
    body: "With 'share the desktop' I meant that two users use the same application simultanously.\n\n"
    author: "Tim Jansen"
  - subject: "Re: Is there anyway..."
    date: 2003-04-04
    body: "No, Tim is right. An X server is not able to share its\n'framebuffer' by itself. NX can only do what is possible\nto do using the X protocol (or some extensions) and at the\nmoment there is no protocol primitive to get the content\nof a -screen- update. For example, using the X protocol\nmessage X_GetImage you cannot get the content of the\nrectangle from coordinate 10,10 to 100,100. You can just\nget what is inside a single drawable (a window or a\npixmap). To cross the boundaries of a window you would\nneed to combine the content of any of the hundreds of\nwindows that constitute 'the screen'. It would have been\nso complex and inefficient to do that in a X client that\npeople had to create a new type of X server, Xvnc. Xvnc\nis a X server that draws its internal framebuffer and\nis able to return areas of this framebuffer in one of\nthe encodings supported by the client. Encodings are,\nmostly, ways to compress a bitmap, so it is very simple\nfor clients to reproduce the content of the Xvnc screen.\n\nThe best way to reproduce 'shared desktop' functionality\nin X is to replicate X clients' protocol primitives to\nmultiple 'displays' and this is the way we are trying to\nimplement it. It is not a simple, anyway. X clients use\na lot of intelligence built into the X server (fonts,\npixmaps, GCs) to run faster (and indeed they CAN run\nfaster than RFB  and RDP clients). For example the same\nfont must be present on all the involved machines to be\ncorrectly displayed and can be very hard to find a\nreplacement. Recent developments in XFree86 (RENDER,\nRandR from Keith Packard and Jim Gettys) are addressing\nthese issues.\n\nRDP is in the middle between RFB and X. Altough it's a\nmuch more complex protocol than RFB, its 'objects', like\nfonts or offscreen 'caches', are simple to be simulated\nwhen not available on one of the 'viewers'.\n\n/Gian Filippo Pinzari.\n"
    author: "Gian Filippo Pinzari"
  - subject: "Re: Is there anyway..."
    date: 2003-04-04
    body: "\"Recent developments in XFree86...\"\n\nI forgot to name fontconfig/Xft.\n"
    author: "Gian Filippo Pinzari"
  - subject: "Re: Is there anyway..."
    date: 2003-04-04
    body: "\"Something like 10 times better than RDP or Citrix\"\n\nSpeaking of Citrix ICA, probably this is a little\nbit 'exagerated' :-).\n\n/Gian Filippo Pinzari."
    author: "Gian Filippo Pinzari"
  - subject: "Re: Remote Desktop (OT)"
    date: 2003-03-30
    body: "Why?? I know a lot of our clients use Socks, too. I think it's quite useful when you have\na firewall. Well I still can use runsocks with TightVNC. runsocks with KDE's remote\ndesktop client terminates immediately after showing the screen for a second, unfortunately."
    author: "Martin"
  - subject: "Thanks and request"
    date: 2003-03-30
    body: "God I love kde-cvs digest\n\nJust one request (not to the digest) please, please improve kdm to something more like gdm (theameable), that would be great (To bad I dont know C++).\n\nI just dont like gdm because It increases starup times and there is no automatic way to shut down the computer from within kde (you have to log out and then issue the shutdown command)\n\nThanks again"
    author: "Mario"
  - subject: "Re: Thanks and request"
    date: 2003-03-30
    body: "Isn't it a good time to start learning c++ ? :-)\nIf you want a feature the best way is to just implement it."
    author: "Norbert"
  - subject: "Re: Thanks and request"
    date: 2003-03-31
    body: "Maybe it is. But Im really impatient ;-)\n\nActually Im in the process of learning c++"
    author: "Mario"
  - subject: "KDE HEAD is so damn fast!"
    date: 2003-03-30
    body: "Just wanted to say that im currently using the latest CVS version of kde and I have to say that it is a great improvement in speed compared to KDE 3.1.1! Everything, especially launching apps, feels much more snappier. Great job, guys! :-)"
    author: "Steffen"
  - subject: "Re: KDE HEAD is so damn fast!"
    date: 2003-03-30
    body: "How is that even possible?  I found 3.1.1 to be a great improvement over 3.1.0 speed-wise.  Isn't there some kind of legal speed-limit that developers should be vary not to violate?"
    author: "Haakon Nilsen"
  - subject: "Re: KDE HEAD is so damn fast!"
    date: 2003-03-31
    body: "Who cares. We don't have a speed limit on highways in Germany. Let's share this feeling. ;P"
    author: "Datschge"
---
In <a href="http://members.shaw.ca/dkite/mar282003.html">this week's version</a> of KDE-CVS-Digest, read about improvements to KBugBuster which now supports most <a href="http://www.bugzilla.org/">Bugzilla</a> sites (GNOME, Mozilla, Apache, XFree86, Ximian, Red Hat and more) and not just the KDE version of Bugzilla, the introduction of
<a href="http://www.shadowcom.net/Software/ksplash-ml/">KSplash/ML</a> to KDE CVS, and how <a href="http://www.koffice.org/kspread/">KSpread</a> is getting some love in the form of some serious optimizations.  Also read about the beginning of the integration of Kafka (WYSIWYG HTML editor) into <a href="http://quanta.sourceforge.net/">Quanta</a>, and much more.
<!--break-->
