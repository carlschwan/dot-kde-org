---
title: "KDE-CVS-Digest for October 25, 2003"
date:    2003-10-24
authors:
  - "dkite"
slug:    kde-cvs-digest-october-25-2003
comments:
  - subject: "Thank you"
    date: 2003-10-25
    body: "Also, is Kexi scheduled to officially debut in koffice 1.4?"
    author: "Alex"
  - subject: "Re: Thank you"
    date: 2003-10-25
    body: "It's planned to be released \"when it's ready\", independent from a KOffice release."
    author: "Anonymous"
  - subject: "Re: Thank you"
    date: 2003-10-25
    body: "this is corretly only partly.\nwhat we do want to do is releasing a first technical preview before koffice 1.4 (which might be koffice 2.0) somewhen mid jannuary and the next not 1.3 koffice release.\n\n   lucijan\n"
    author: "Lucijan Busch"
  - subject: "KWallet"
    date: 2003-10-25
    body: "I'm looking forward to having a KDE password repository, but from what I've heard about KWallet, some people don't like it.  Is it ready for release?  Does it have a nice simple GUI that doesn't get in your way, and is it well-integrated with KDE?"
    author: "Spy Hunter"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Don't rely on [outdated] opinions of other people, try it yourself!"
    author: "Anonymous"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "I'm not going to go to all the hassle of compiling KDE just to try out KWallet. (OTOH, if you can point me to .deb packages of a recent KDE CVS that I can install concurrently with my working KDE 3.1, I'd be happy to try them out, but I think I'd have heard of such a thing already if it existed...)  I don't want to rely on outdated opinions of other people, I'd much rather rely on up-to-date opinions of other people. ;-)  Hence the question."
    author: "Spy Hunter"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "If you're not going to then you have no reason to complain."
    author: "Anonymous"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Oh come on.  Are you being dense on purpose?  I'm not complaining, I'm asking.  There's a difference.  Plus, I don't see why compiling KDE is a prerequisite to complaining about KDE.  Constructive criticism from users is and should be welcomed by the KDE team."
    author: "Spy Hunter"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Compiling is no prerequisite but being a user of something you criticise."
    author: "Anonymous"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "try the orth's cvs debs (see http://opendoorsoftware.com/cgi/http.pl?p=kdecvs) while there not installable next to working kde 3.1, they all depend on kde-cvs snapshot and are thus easily removed should you experience real problems (hasn't happened yet, as he takes care to only do snapshots when cvs is relatively stable)"
    author: "cobaco"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Hey, cool.  I might give those a try.  I better back up my ~/Mail and ~/.kde first though."
    author: "Spy Hunter"
  - subject: "Re: KWallet"
    date: 2003-10-26
    body: "good idea =)\n\ncvs has been pretty unstable over the last few weeks for me.. but it was rock stable in the beginning of october. It's starting to stabalize again though, as beta1 is coming."
    author: "anon"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Firstly, to answer your question, I quite like KWallet. It's really useful and well integrated with quite a few KDE apps. Secondly, there are debs of kde-cvs. AFAIK orth does them. I dunno when to find them though. Hope this helps you"
    author: "lrandall"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "In the version I've tried, KWallet is a usability nightmare, it asks for the password all the time and I couldn't find a way to turn that off.\n\nPerfect would be if it would ask for the password only once per KDE-session.\nBut I'm confident that that will be fixed in KDE3.2-final or 3.2.1"
    author: "Roland"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "that's what it does in the current kde-cvs ask for password only once per session ."
    author: "maor"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "KWallet asks me for a password very often, for example whenever I start Kopete (and when I restart Kopete I have to enter the password again), when I login on a website for the first time in this KDE session and when I login using a FTP or HTTP authentication for the first time in a KDE session.\n\nI really do not like this, after logging in to KDE the wallet should give the passwords to an application without of asking me, if it has been the same application that stored the password!"
    author: "Steffen"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "In a \"Perfect World (TM)\" I think it \nshouldn't ask to _enter_ a password\nbut ask me if I want to \"spend\" the\npassword from my wallet in some system dialog box\nwhich cannot be automatically answered by ANY application\n(it should be in some special protection layer if that\nmakes any sense)\nThis is much safer than completely automatically spending \nthe password. Even if it can't have a special protection\nI would prefer this solution in contrast to not being asked\nat all or asked to enter the password at first login.\nI'd be very suspicious if I'm playing Kolf and suddenly something \nis requesting my banking site password. This would make the whole \nthing much less prone to trojan attacks.\nDon't know if this is planned to be implemented or already implemented\nin CVS though.\n"
    author: "Mike"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Read my posting again, I think the password should only be given to an application if it has been stored by this application! So if Kopete wants to access my website form passwords this should be denied (or alternatively I should be asked whether I want to allow it)."
    author: "Steffen"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "I understood that but one website might access passwords of a totally\ndifferent website anyway because they all run in Konqueror.\nOr some app might pretend to be Konqueror and temporarily replace it.\nMmm, come to think about it, there are a lot of security issues...\nWell, I'm quite sure the KDE developers will finally get to a solution that\ncan be trusted - they are certainly a lot more savvy about all those\nsecurity pitfalls than I am. And the fact that it's open source ensures\nthat it won't be the dreaded \"security by obscurity\" solution.\n\n"
    author: "Mike"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Hi,\n\nhow do you identify the application in a reliable way?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "I'd say by it's location, because to exchange an application you usually need to be root. Well, this of course means that you have to trust your administrator..."
    author: "Steffen"
  - subject: "Re: KWallet"
    date: 2003-10-26
    body: "Use gpg or some form of tickets.  Similar to the way services authenticate against kerberos servers..."
    author: "AM"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "\"once per session\" is already annoying...\n"
    author: "Tim Jansen"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "Agreed, I want to use my computer without entering any password. Is this annoying mandatory \"once per session\" going to disapear?"
    author: "John Herdy"
  - subject: "Re: KWallet"
    date: 2003-10-26
    body: "Another agreement. I just want KWallet to let me save usernames and passwords in a secure manner, so people can't get them off the hard disc. People already have to login to see my information come up without a password; that's plenty of security for me."
    author: "Tom"
  - subject: "Re: KWallet"
    date: 2003-10-27
    body: "Without a password storing anything in a secure manner is not possible. All you can do is obscure data, e.g. by encrypting with a password that's stored in the source code or in another file. But for anyone who has access to the source code/the other file and your obscured data it's a trivial task to \"decrypt\" your data.\n\nSimply use an empty password if entering a password annoys you.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: KWallet"
    date: 2003-10-27
    body: "Thanks for your reply. I don't mean I don't want to have a password, but I already enter one when I log in Linux. I don't want to enter another password for kwallet. Is it possible to use my linux account/password automatically with KWallet?"
    author: "John Herdy"
  - subject: "Re: KWallet"
    date: 2003-10-27
    body: "One way to achieve something like that would be Kwallet integration into KDM, 'spying' the password from there. I don't know whether that's such a good idea though."
    author: "Datschge"
  - subject: "Re: KWallet"
    date: 2003-10-28
    body: "As John Herdy said, I don't think you quite understood what I meant. I don't want to have to use a password at all with KWallet; it seems like an unecessary fuss for me. I am quite happy with Mozilla's behaviour, whereby once you've logged in, you can launch Mozilla and have access to all of the data you've allowed it to store, so you go to a site with a login form and ther username and password are automatically filled in without any extra fuss. \n\nI don't want to have to login, start up KDE and then put in another password to make KWallet work. The login authentification is plenty for me."
    author: "Tom"
  - subject: "Re: KWallet"
    date: 2003-10-28
    body: ">  I don't want to have to login, start up KDE and then put in another password >  to make KWallet work. The login authentification is plenty for me.\n\nsame.. after it prompts for the password once, it should have a checkbox IMHO that says \"Fill In Passwords Automatically\".. kwallet as of cvs two weeks ago was just down right annoying because of this. We are all pretty much on multi-user systems, so the only times I should have to put in passwords imho is in login :)"
    author: "fault"
  - subject: "Re: KWallet"
    date: 2003-10-28
    body: "I second that."
    author: "Steffen"
  - subject: "Re: KWallet"
    date: 2003-10-29
    body: "Not inputting any password for KWallet would equal to allow KWallet to save all passwords in clear text, I can't imagine anyone really wanting that. The other solution I mentioned above, taking the password you input when logging in into the system, is risky as well since that would allow one further app to see strictly confidential passwords in clear text, risking both the user account and the KWallet password collection at once.\n\nTo save yourself from inputting two passwords a session how about allowing your user to login without password or even automatically while keeping the password collection in KWallet secure by actually using a password for that?"
    author: "Datschge"
  - subject: "Re: KWallet"
    date: 2003-10-25
    body: "I think KWallet is quite nice - both the idea as such and how it's been done so far. \n\nWhat would be quite cool is if one would be able to store passwords on a Smartcard. (Ever heard of ActivCard? They have something similar...) \n\n<disclaimer>Maybe it's just me and this is even already possible... ;-) (I suppose a conventional USB memory stick should work, but I didn't try/investigate).</disclaimer>"
    author: "Anonymous"
  - subject: "Re: KWallet"
    date: 2003-10-26
    body: "I'm more interested on what kwallet is based. I really don't need a \"kde only\" secure password container."
    author: "Carlo"
  - subject: "Re: KWallet"
    date: 2003-10-27
    body: "Is there anything for it to be based on anyway?"
    author: "Datschge"
  - subject: "Re: KWallet"
    date: 2003-10-27
    body: "Sure - databases & encryption software. I really don't know if there is anyone, who wrote a lib and cmd tool for this purpose; But I think a widely accessible lib rary would be far better, than a isolated kde solution."
    author: "Carlo"
  - subject: "POLL KWord at 3.78%"
    date: 2003-10-25
    body: "http://www.gnomedesktop.org/pollBooth.php?op=results&pollID=46\n\n\n:-)"
    author: "Heini"
  - subject: "Re: POLL KWord at 3.78%"
    date: 2003-10-25
    body: "Its a GNOME website isn't it?\n\nAnd anyway, I too think that OO.org is the best in terms of features, but Koffice has great advantages when it comes to integration, speed and ease of use."
    author: "Alex"
  - subject: "Re: POLL KWord at 3.78%"
    date: 2003-10-26
    body: "well, duh.. it's at gnomedesktop, and koffice 1.3 isn't out yet, which has a much better kword =)"
    author: "anon"
  - subject: "looking forward to a binary release."
    date: 2003-10-26
    body: "I live with hundreds of minor annoying bugs in 3.1 and like most people I'm not in a position to file a bug report about them. The KDE development process just doesn't cater for it. The chances are they've mostly been fixed in HEAD but we don't have time to compile the whole of CVS (+break my current installation) just to check for that odd one which might slip through to 3.2.\n\nI look forward to the first binary (.deb) release so the rest of us can see the great progress that the l33t h4x0r's slap us down for not appreciating, and maybe even file a few bug reports....\n\n(any chance of a milestone release structure next time guys?)\n\noh yeah, and great work on khotkeys, which sorely misses a GUI in 3.1!"
    author: "caoilte"
  - subject: "Re: looking forward to a binary release."
    date: 2003-10-26
    body: "Don't understand why you're not in a position to file a bug report I'm afraid. Why do you say that?\n\nBy not filing the reports there is a chance the bugs will still be there in 3.2, you know?"
    author: "Max Howell"
  - subject: "Re: looking forward to a binary release."
    date: 2003-10-26
    body: "I've got issues with KWin mouse focus, konqueror tabs and (every day) more and more websites rendering.\n\nBut those are all issues I know have been addressed in HEAD, and I hope very much totally fixed."
    author: "caoilte"
  - subject: "Re: looking forward to a binary release."
    date: 2003-10-26
    body: "There never were and never will be \"binary releases\" from KDE: http://www.kde.org/download/packagepolicy.php"
    author: "Anonymous"
  - subject: "Re: looking forward to a binary release."
    date: 2003-10-26
    body: "I've read it and it says:\n\u00abwe work with various third-parties to ensure that as many binary packages as possible are built.\u00bb"
    author: "Ricardo Cruz"
  - subject: "Re: looking forward to a binary release."
    date: 2003-10-26
    body: "Then you have also read \"The KDE Project itself only releases and supports the source code packages.\" That KDE helps other to create packages doesn't make them the maintainer of them."
    author: "Anonymous"
  - subject: "Re: looking forward to a binary release."
    date: 2003-10-26
    body: "Hi,\n\nthen use Konstruct. If you know which -dev packages you need to install that's an easy way to run KDE 3.2 Alpha2. Soon Beta 1 will be out and I will update with it. I doubt there will be official debs before the final release.\n\nThe Alpha2 has substantial usability bugs. Kontact, KWallet are not usable yet. Konqueror has a few problems in part related to rendering, in part to Authentification and so on. But I still love it over 3.1 for its speed and usability.\n\nYours, Kay"
    author: "Debian User"
---
In this week's <a href="http://members.shaw.ca/dkite/oct252003.html">KDE-CVS-Digest</a>:
KHotKeys now has a GUI. <a href="http://korganizer.kde.org/">KOrganizer</a> now supports todo attachments. 
<a href="http://www.koffice.org/kexi/">Kexi</a> has a <a href="http://www.postgresql.org/">PostgreSQL</a> driver. Bug fixes in KHTML layer support and
rendering engine. Many bugfixes in <a href="http://kmail.kde.org/">KMail</a>, 
<a href="http://kopete.kde.org/">Kopete</a> and 
<a href="http://pim.kde.org/components/korganizer.php">KOrganizer</a>.
<!--break-->
