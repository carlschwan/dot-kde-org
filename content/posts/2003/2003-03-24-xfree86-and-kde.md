---
title: "XFree86 and KDE"
date:    2003-03-24
authors:
  - "Datschge"
slug:    xfree86-and-kde
comments:
  - subject: "XFree86 new modular multilingual support and KDE"
    date: 2003-03-24
    body: "The new version supports a (non-backward compatible) new setup for multi-lingual keyboards, which doesn't work at all with the current kikbd and kcm module, both need to be changed to support the new API. \ncurrently I don't mind that much because I use Mandrake which changed their keyboarddrake configuration utility to support the new scheme."
    author: "Guss"
  - subject: "Re: XFree86 new modular multilingual support and KDE"
    date: 2003-03-24
    body: "Do you have any link with info about the changes? "
    author: "Sad Eagle"
  - subject: "Re: XFree86 new modular multilingual support and KDE"
    date: 2003-03-24
    body: "look here:\nhttp://www.xfree86.org/4.3.0/XKB-Config2.html#4"
    author: "Guss"
  - subject: "Demanded changes"
    date: 2003-03-24
    body: "For people who want to know what is being requested of XFree86:\n\nPolitically:\n* Actual admission that the XFree86 project is nowhere near as open as comparable projects - so far they are still maintaining their innocence\n* Work to change this - effectively this means being less mean about giving out CVS write access given the current record of core team innefficiency, and actually engaging with the main users of their work. \n* Stop believing that a few core developers are a better testing framework than their userbase. This is the bottleneck with driver updates. \n* Sadly it seems unlikely that the core team will forgive Packard anytime soon. This is not brilliant, as he has been one of the most visibly productive members of the XFree team.\n\nTechnically:\n* New build system - imake is awful. Autoconf/ automake are the obvious choice. \n* Make disconnected trees easier - ie independently released drivers and extensions, which can be folded in later. \n* Listen to what desktop projects, the main users of the software, want:\n   * Proper rendering model - ie RENDER like functionality that is implementable \n      on modern hardware, and is properly accelerated. Some suggestion that this\n      can be done via OpenGL - skeptics remain... \n   * Make expose events actually work properly - ie no crappy redraw updates \n      when windows are dragged around. This may be better solved by saving all \n      mapped window contents. \n   * Same with resize events - opaque resizing is excremental on X right now due\n      to syncronisation issues. \n   * Proper alpha channel model between toplevel windows- the shape extension\n      is sick, and transparent terminal/menu hacks are outdated.\n   * Good infrastructure for layering XFree on other window systems, ie\n      Win32,GDI,DirectFB, X itself etc. So reimplementation is not a chore. \n      Make it easy to get extensions like XVideo and glx working on top of other \n      window systems. \n      Also would ease transition to a theoretical X12, which would get rid of \n      core cruft and change the protocol to be more suited to modern networks. \n      Ie bigger packets, accept higher latency more gracefully. This all may be\n      acheivable within the X11 framework.   \n\nWhat sane people are NOT asking for:\n* Getting rid of the wire protocol: this request implies a severe misunderstanding of modern graphics hardware and 2d performance issues. \n* Moving network transparency to a screen scrape VNC model - this would be a massive step backwards. \n* Changing the driver model for GGI or FBDEV - efficient acceleration would not be  easy. "
    author: "rjw"
  - subject: "Re: Demanded changes"
    date: 2003-03-24
    body: "I just have to say that I totaly egree with that list.\n\nAt one time I wanted to help with xfree, but I could not find any way to ease into it.  Maybe I will try again if they clean up the pollitical situation.\n\nAlso using a modern rendering model that allows desktop opengl acelleration, and is not constantly forcing the windows to redraw would be a huge gain.  Heck just saving all mapped window contents would be huge.\n\nI also agree with the not list.  I find it funny how all the x (as a protocal) bashers completely miss the point when demanding x reforms."
    author: "theorz"
  - subject: "Re: Demanded changes"
    date: 2003-03-24
    body: ">* Proper rendering model - ie RENDER like functionality that is implementable \n> on modern hardware, and is properly accelerated. Some suggestion that this\n> can be done via OpenGL - skeptics remain... \n\nWhat's wrong with RENDER?  It's implementable on modern hardware, and it can be properly accelerated.  What possible benefit would OpenGL have over RENDER?"
    author: "not me"
  - subject: "Re: Demanded changes"
    date: 2003-03-24
    body: "RENDER is an X extension. OpenGL is a language for your graphics card to do hardware accelerated 3D drawing."
    author: "FooBar"
  - subject: "Re: Demanded changes"
    date: 2003-03-25
    body: "That's correct.  It also doesn't answer my question."
    author: "not me"
  - subject: "Re: Demanded changes"
    date: 2003-03-25
    body: "I don't even get your question. OpenGL is for video cards. RENDER can be implemented on top of OpenGL. RENDER is not a replacement for OpenGL, while your question implies it is."
    author: "FooBar"
  - subject: "Re: Demanded changes"
    date: 2003-03-27
    body: "RENDER is not a replacement for OpenGL in general, but it can replace the 2D portions of OpenGL for general windowing system use.\n\nThe original post implied that X needed a new rendering model besides RENDER.  I was asking why RENDER was not good enough.  When I asked what benefit OpenGL had over RENDER, I was asking \"what benefit would using OpenGL as a new 2D rendering model for X have over simply continuing to use RENDER?\"  If the original post was only talking about implementing RENDER on top of OpenGL, then I misread it.  But I think the post was talking about replacing RENDER with something else, possibly OpenGL."
    author: "not me"
  - subject: "Re: Demanded changes"
    date: 2003-03-25
    body: "Accelerated for those who have acceleration support. Some of us (especially laptop users) are so unlucky that we're using savage or other S3 video cards. Not everyone can afford to buy a laptop with Nvidia G4 :-("
    author: "Myself"
  - subject: "Re: Demanded changes"
    date: 2003-03-24
    body: "There have been issues with accelerating RENDER. \nHopefully these issues can be addressed by eveolution \nof the spec.\n\nCurrently *2 years* after RENDER was released, the mga driver has \nabsolutely minimal acceleration and thats it for free drivers. \nXIG apparently has some acceleration too, after a fair amount of \nhair pulling. \n\nThe problem was - the specifier, KeithP, didn't know as much about \nthe hardware as the driver authors and hardware vendors, and they \njust didn't seem to care enough to have any real input into the process. \nHopefull all this can be solved.... \n\nThe OpenGL issue: there has been suggestion that the RENDER extension \ncould be implemented over the top of OpenGL operations, with the X Server \ncalling the OpenGL implementation for all 2D operations. So the X Server would \nbecome a user of the OpenGL implementation and most of XAA would go out the window. AFAIK, this would require the OpenGL driver to understand nested contexts - eg creating a context on a window which is then composited by the \nroot context onto anything else... this whole method begs the question - why not just use the OpenGL API to draw everything (and just have a legacy X11 proxy)?\n\nThe other idea was just to use the same facilities on cards as are used to implement OpenGL, and not reuse the drivers. \n"
    author: "rjw"
  - subject: "Re: Demanded changes"
    date: 2003-03-25
    body: "I really like that idea.\n\nAs for just using opengl, I think that is too simple.  The x server should be handling fonts, understanding nested contexts, and so on.  I see it as a good split.  opengl for getting the hardware to do its work.  And having an x server on top of it that does the windowing specific work."
    author: "idspispopd"
  - subject: "Re: Demanded changes"
    date: 2003-03-26
    body: "Well, duh. \n\nOf course I didn't mean *everything*, there needs to be a \nwindow system. \nFonts is probably going client side from what I can see. \nNested contexts is definitely an OpenGL issue. "
    author: "rjw"
  - subject: "Re: Demanded changes"
    date: 2003-03-25
    body: "The problem with RENDER is two fold:\n\n1) Graphics cards don't support it yet. There is no point in defining another API if an existing, well support, and well established standard does the job. \n2) RENDER (actually, Xr/Xc) is a 2D API. Not only does this restrict integration with more advanced OpenGL features (beyond the standard Postscript/PDF imaging model), it keeps the imaging model from taking advantage of the ever improving OpenGL API, and it makes life more difficult for driver implementors, who have to coordinate simultanious 2D and 3D rendering. To look at the big picture, both Xr/Xc and OpenGL are vector graphics APIs. There is no point in having two of them if you can have one and use a subset of it for 2D graphics. \n\nNow, from what I've seen, OpenGL seems to be suitable enough for advanced 2D graphics. SVGL (http://www.lri.fr/~conversy/svgl/), for example, has already implemented an SVG viewer using OpenGL, and has seen incredible (up to 100x) performance improvements. EVAS already has a very good demo using OpenGL, and OpenGL rendering looks just as good as their software rendering. Xr/Xc tries to implement a pixel-exact rendering model, which is of dubious use. For things where pixel-level accuracy matters (fonts), one can always use textured 2D triangle bitmaps, which is precisely what Quartz Extreme in OS X does."
    author: "Rayiner Hashem"
  - subject: "Re: Demanded changes"
    date: 2003-03-25
    body: "More Info: The Amaya people also seem to be working with SVG and OpenGL:\nhttp://www.w3.org/Amaya/User/gldev.html\nScroll down to the bottom of the page for some notes they made about using OpenGL to implement SVG."
    author: "Rayiner Hashem"
  - subject: "Re: Demanded changes"
    date: 2003-03-24
    body: "Another political / technical challenge is how to get the X consortium to accept changes. There are some issues with X itself that need fixing, and some feel there hasn't been effective representation to bring necessary changes.\n\nThis whole thing will probably improve the situation for everyone. Every project goes through defections and controversy. Usually the issues are fixed as much as possible, and hopefully with time everyone simmers down. Even a fork may be necessary to permit an experimental approach on one hand, and a stable tree on the other.\n\nI think every project needs an Alex Viro to keep itself on track.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Demanded changes"
    date: 2003-03-24
    body: "So what would be KDE's equivalent of Al Viro?  \n\nNeil Stevens?\n\n*duck*"
    author: "Navindra Umanee"
  - subject: "Qt/KDE on framebuffer"
    date: 2003-03-24
    body: "before i start:\n1. I don't mind using X.\n2. I wouldn't mind -- corr. -- would love to see an alternative to X\n\t(and make used of it with KDE)\n3. I am running the latest X server and liking it...\n\t...i thank the X team for so much end-user-impotant-changes!\n\nSee here a link to some info on gtk without X:\nhttp://mail.gnome.org/archives/gtk-list/2002-February/msg00022.html\n\nWould it be possible to do Qt/KDE without X?\n\nThis link (from 2000) sais some on it:\nhttp://listserv.externet.hu/lists/linux/0003/msg00085.html\n\nAnybody got me some update info on this?\n(links, screenshots, etc.)\n\nHave a nice day...\nCies."
    author: "cies"
  - subject: "Re: Qt/KDE on framebuffer"
    date: 2003-03-24
    body: "> Would it be possible to do Qt/KDE without X?\n\nSure, just re-write the bits of KDE that require X (there's not a huge amount of it).\n\nSupposedly some parts of KDE are already able to build without X."
    author: "Chris Howells"
  - subject: "Re: Qt/KDE on framebuffer"
    date: 2003-03-24
    body: "How about XDirectFB (http://www.directfb.org/xdirectfb.xml)? Has anyone tried it? Does it work with KDE?"
    author: "kurre"
  - subject: "Re: Qt/KDE on framebuffer"
    date: 2003-03-24
    body: "This news item has made me wonder if I shouldn't look into alternatives like XDirectFB."
    author: "Richard Plante"
  - subject: "Re: Qt/KDE on framebuffer"
    date: 2003-03-24
    body: "You need X to run XDirectFB\n\n======================\nfrom the README:\n....\nShort installation instructions:\n\n- Check out module \"xc\" from XFree CVS (www.xfree.org)\n- Apply patch \"xc-directfb.diff\" with \"-p0\" within \"xc/\"\n- Copy directory \"programs/Xserver/hw/directfb\" into \"xc/programs/Xserver/hw/\"\n- Copy files in \"config/cf/\" into \"xc/config/cf/\"\n- Adjust \"xc/config/cf/host.def\"\n- Run \"make World\", lean back, run \"make\" for safetiness, run \"make install\".\n=======================\nI haven't tried it yet (checking out ATM) anybody use it though"
    author: "Brendan Orr"
  - subject: "Re: Qt/KDE on framebuffer"
    date: 2003-03-25
    body: "I just compiled it, it is nice and all, but slow (I.e. unaccelerated on my machine)  Setting the default window opacity to 255 (100%) results in an ugly magenta background where transparency once was.  Setting to 254 fixes the problem.  Also, make sure to turn off the fade effect on menu transitions.  Animate works, but it creeps out at a slow,steady rate instead of just popping out..  There are quite a few nice features to this (besides the ability to have true transparency with windows).  For instance, all cursors get a soft drop shadow (I.e. MS windows cursor shadow). I haven't tested it with my cursor sets though.\n\n"
    author: "Brendan Orr"
  - subject: "XDirectFB"
    date: 2003-03-26
    body: "Your cursors probably got the shadow effect from using XFree86 CVS which has configurable animated cursors with support for shadows"
    author: "Guss"
  - subject: "Re: XDirectFB"
    date: 2003-03-26
    body: "the core set has shadows?  I though it was just the new sets that were made from PNGs? (where the shadow as added using a graphics program)"
    author: "Brendan Orr"
  - subject: "Re: XDirectFB"
    date: 2003-03-26
    body: "The core set of 4.3 is a new core set, not the same as you've used before and it has shadows"
    author: "Guss"
  - subject: "Re: XDirectFB"
    date: 2003-03-28
    body: "Hmm, it must've not been in the official release because I reverted to the pre-compiled binaries and there is no shadow (with the exception for the CursorShadow option which I have used since the beginning of 4.2).  oh well :)"
    author: "Brendan Orr"
  - subject: "Re: Qt/KDE on framebuffer"
    date: 2003-03-26
    body: "Yes it works with KDE.\n\nSetting it up is a pain though.  First, It is really just a patch to XFree86, so you have to download the huge sources (100MB+ or so) and compile everything.  Second, you have to somehow get it to co-exist with your existing XFree86 installation (not easy, but not XDirectFB's fault).\n\nI used it around summer of 2002, and it worked pretty well.  Unfortunately, it didn't support many of the X extensions, so some apps refused to run (notably VMware and anything 3D).  On the positive side, setup is a breeze.  It uses the keyboard/mouse/video drivers of the kernel, so things just work.  The transparency tricks are a bit useless, but they prove a point.  XDirectFB also claims to be able to run normal DirectFB software (like that GIMP port), managed with your X11 window manager.  I never tried this, I was just happy to be using a nicer X server for once.\n\nI went back to normal XFree86 a few months later.  IMO, XDirectFB is the future of the Linux desktop, but it is not ready yet."
    author: "Justin"
  - subject: "Re: Qt/KDE on framebuffer"
    date: 2003-03-24
    body: "There is Qt/Embedeed and kde-nox already."
    author: "KDE User"
  - subject: "So: XdirectFB is an option but no alternative yet!"
    date: 2003-03-26
    body: "So: XdirectFB is an option but no alternative yet!\n\n(Had to conclude some from the intersting Re's on my post...)\n\nI think linus desktopping will stay with X for a looooong time, cauz:\n- desktop pc's are getting faster and faster\n- x works and does a lot\n- lot's of stuff dep.'s on/uses x...\n- when the FB stuff matures we want networkt transperancy in it ;)\n\nI think opensource movement must be happy to have a good standard!\nBut the freedom of choosing something less bloaded will only enrich...\n\n\nHave a nice day,\nCies.\n"
    author: "cies"
  - subject: "KDE support"
    date: 2003-03-24
    body: "So what is the status for KDE support of these features like the fully graphical mouse cursors and resolution changes? Is that stuff coming for KDE 3.2?\n\nAlso, with Keith kicked off the XFree team, might they decide to throw away his work on the RENDER and RANDR extensions?"
    author: "AC"
  - subject: "Re: KDE support"
    date: 2003-03-24
    body: "Are they stupid/fanatic enough to do that?\nIf so, there should be a \"cleanup\" in the XFree BOD."
    author: "Somebody"
  - subject: "QT re-drawing issues are slow not X ???"
    date: 2003-03-24
    body: "Not sure if this is OT or not but everytime the discussion comes up on ./ about throwing out X, someone suggests that X itself is not slow, but that the modern toolkits (gtk and qt) are not written to take advantage of X (ie they redraw incorrectly etc).\n\nI actually have no clue about this but was wondering if anyone here could explain it.  Obviously KDE does a lot more that twm, but is this eye candy that is slowing down X or toolkit problems?\n\nDoes QT have inherent X problems (I wouldn't think this to be the case)?\n\n(This is NOT a troll, I'm really would like to know)"
    author: "ac"
  - subject: "Re: QT re-drawing issues are slow not X ???"
    date: 2003-03-24
    body: "I don't know anything about redrawing \"incorrectly\", but I'll try to take a stab at it...\n\nThe way Qt draws themes involves drawing primitives with X11 (or GDI under win32). For a complex control such as a button with a bevel, gradient background, pixmap icon, and AA label, a lot of primitives get called to draw the button. This happens each time the button is redrawn. To make things go faster, the obvious solution is to cache a pixmap of the finished widget the first time and bitblt on any redraws. But who does this? X11, Qt or the theme? Either one of the three are viable options. The best choice though would be X11, as this sort of stuff belongs on the X server. There is some areas where Qt should cache or double buffer, and it does. But Qt is merely a toolkit, not the base GUI system. It shouldn't have to worry about whether the server is local or remote, whether some window is going to intercept any drawing events, or whether to use video or local memory to cache in.\n\nJust my thoughts..."
    author: "David Johnson"
  - subject: "Re: QT re-drawing issues are slow not X ???"
    date: 2003-03-24
    body: "Don't know anything about what Qt/GTK do right or wrong,\nbut here's another possible culprit: the scheduler in\nthe kernel (i.e. Linux in most cases):\nX11 is a user-space process. So, whenever a GUI application\nreacts to a user event (eg. a mouse-click), the CPU has to\nswitch from the X11 process to the application process and\nback. Many think that the time it takes to do these context\nswitches is the problem. However (particularly since Linux\nis pretty fast at context-switching) I can't imagine that\nthis takes more than a few milliseconds (probably even less\nthan one) - so that would hardly be noticeable. But if the\nkernel doesn't switch directly from X11 to the GUI\napplication (or back) but instead gives a timeslice to\nyet another process in between, the reaction to the user\nevent will lag.\nThe brute force way of dealing with X11 slowness proposed\nby many is to put the graphics driver in the kernel. But\nthat wouldn't even deal with the problem above: the gfx\ndriver would probably be implemented as a kernel thread,\nand if that isn't scheduled correctly, you still get a\nslow GUI. The only advantage would be that you could\ngive special treatment to that thread in the kernel since\nthe kernel knows that this is the graphics driver.\nBut that special treatment could also be done for a user-\nspace process, so in the end putting the gfx driver in the\nkernel doesn't buy you much - but gives a lot of headaches\ninstead: Gfx drivers are pretty complex and hence often\ninstable - at least when compared with the stability of\nLinux.\nAnyway, I've heard that the kernel developers are on it\nfor 2.6. As usual with the Linux developers, they don't\nwant to hack up a special treatment for X, but go for a\ngeneric solution, because apparently there are other\nsituations where the same scheduling problem arises.\n"
    author: "Marvin"
  - subject: "Re: QT re-drawing issues are slow not X ???"
    date: 2003-03-26
    body: "If the problem was coming from the scheduler giving too much cpu time to other processes instead of X when switching context, then it would be relatively easy to correct it by giving an higher priority to X.  However, this doesn't increase X performance on a mostly idle system.  As X is also slow on these systems, the problem is probably elsewhere in the X architecture."
    author: "Ned Flanders"
  - subject: "Re: QT re-drawing issues are slow not X ???"
    date: 2003-03-26
    body: "You are saying that X11 is slow all the time. I wonder how you come to this impression. Maybe you have a slow driver? With both the ATI r128 and the proprietary nvidia drivers I was never ever able to see a redraw or something else that would make X11 appear slow."
    author: "AC"
  - subject: "Re: QT re-drawing issues are slow not X ???"
    date: 2003-03-28
    body: "I'm using the ati driver.  I know that 3D support on the mach64 is experimental on Linux, but won't use it as a point of comparison.  However, I think 2D graphics performance can be compared with Windows, and window moving/resizing is really faster on Win2K, even if my CPU is idle.  The difference may not be visible on new hardware, but isn't Linux useful on older computers?"
    author: "Ned Flanders"
  - subject: "Re: QT re-drawing issues are slow not X ???"
    date: 2003-03-24
    body: "I can't comment on Qt, but the right way to do X11 calls is to minimize interactions between the client and the server, especially during a redraw. You should minimize queries on the client-side, and especially should not put a query between your drawing commands. \n\nThe general problem with xlib is that it is not transparent which function will cause a interaction with the server, and under which circumstances. xlib will cache information to a certain extend, but it is often hard for the application/toolkit developer to find out when it caches.\n\nA few people have worked on a more direct, non-caching API for the X11 protocol called XCB (http://xcb.wiki.cs.pdx.edu/cgi-bin/twiki/view/XCB/XcbOverview) which would make this more transparent for the toolkit developer. I don't know it's current state, last time I checked it lacked support for most extensions, and AFAIK not a single toolkit has been ported to it."
    author: "Tim Jansen"
  - subject: "So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-24
    body: "Ok, so I have xfree86.4.3 and kde 3.1.1 built using konstruct. How do I actually get the tools and cursors shown in the screenies built on my system?\n\nThanks\n\nTim"
    author: "TimLinux"
  - subject: "Re: So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-25
    body: "Its easy enough.\n1a)go to www.kdelook.org, www.themedepot.org or some other theme/resource site and get a new icon set.\n1b)Use one of the default icon themes that are shipped with 4.3.0(or 4.2.99).  They are whiteglass, redglass, handheld, and core(the old B&W default).\n1c)Make your own :)\n\nIf installing for a single user(or a particular user):\n\t2)make the directory \"~/.icons\"\n\t3)copy the \"cursors\" directory and \"index.theme\" from the archive to ~/.icons\n\t4)restart X\n\nIf installing system-wide:\n\t2)extract the archive contents to its own directory in /usr/X11R6/lib/X11/icons\n\t(E.g. you would have a .../X11/icons/reddot directory with its own cursors directory and index.theme\n\t3)edit the index.theme in the default directory to say:\n\t\t[Icon Theme]\n\t\tInherits=theme \n\tobviously replacing theme with the themes name\n\nAlternatively, if you just want to test a set out you can set the variable \"XCURSOR_THEME\" to a cursors name and \"XCURSOR_SIZE\" to set the size of the cursor to any size (yes, you can have a 128x128 cursor if you wanted to)\n\nTo make your own  or for more info check out uga's Red Dot theme.  It includes a Cursor Making HOWTO along with a probably more clearer documentation on how to install :)\nhttp://www.kdelook.org/content/show.php?content=4805"
    author: "Brendan Orr"
  - subject: "Re: So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-26
    body: "kcmrandr is in the cvs module kdenonbeta.  An example of how to check out just kcmrandr is here: http://svg.kde.org/status.html (replace ksvg with kcmrandr)\n\nThen, follow the readme.\n\nI'm sorry it can't be any easier, because you need to apply patches to Qt, kdelibs and kdebase to make it work properly..."
    author: "Hamish Rodda"
  - subject: "Re: So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-26
    body: "Hey nice job, btw!\n\nI think we need to fit it in a better section than \"Look & Feel\" in KControl.  Also, I guess we don't need to repeat the screen dimensions in mm for every resolution.  :-)"
    author: "Navindra Umanee"
  - subject: "Re: So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-26
    body: "\"Look & Feel\"? Maybe you should update to KDE 3.1? ;-)"
    author: "Anonymous"
  - subject: "Re: So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-26
    body: "He may have it, since the directory is still called LookNFeel even in CVSHead. (I'm talking about the real directory name, not the name that shows in the menu)."
    author: "Myself"
  - subject: "Re: So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-26
    body: "Actually, as Hamish noted I was looking at some old screenshots.  But you're right, Look & Feel is gone from KControl."
    author: "Navindra Umanee"
  - subject: "Re: So how do I get the snazzy cursors and kcmrandr ?"
    date: 2003-03-26
    body: "Thanks :)\n\nThose screenies are really old though, both issues are fixed.  Now it only needs a cosmetic lift (maybe a picture of an LCD screen with the changes being displayed)."
    author: "Hamish Rodda"
  - subject: "Time to open the eyes"
    date: 2003-03-26
    body: "It seems like the people at the XFree86 project had ( or have? ) their eyes closed for a long time.\nWhen you open your eyes and look around you'll see two big players on the desktopmarket. Apple has done a great job with their latest OpenGL accelerated userinterface. Really slick, and one of the major reasons OSX became intresting for the desktop. Microsoft is planning Direct3D acceleration for their graphical shell, and will surely not take long to be implemented. However XP has nice graphics, they too realize that it is time to go further. Especially now a 3D accelerated card is all but rare in today's common homecomputer or workstation.\nIn the mean while, the GNU/Linux OS becomes more and more popular. While applications shipped with current distributions get better and more accessible to common homeusers, the graphical environment is still behind the commercial implementations. Things like antialiased fonts and the new features of XFree 4.3 are surely a step forwards, but it can be so much more.\nI can not imagine there aren't people who share my desire of a fully OpenGL ( <- most obvious choice imho) accelerated desktop showing of nice effects that make OSX users cry, unmatched performance, and a better chance of graphiccards companies writing drivers for it. \nI realize that not everyone has a state-of-the-art graphicscard in their system, but that problem could be solved by just letting the OpenGL renderer run in software mode. Backwardscompatibility should -however i am not familiar with the current XFree codebase - not be hard to achieve, writing simple wrappers or emulation layers.\n\nSo the question may be: \"Is it wise to stop innovation in fear of releasing the past?\" \n\nI think it's time to look closer at people who have already achieved what the goal should be for the current graphical environment(s) on the GNU/Linux platform.\n\n\nRegards,\n\nScythe."
    author: "Scythe"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-26
    body: "The problem is not that people have no \"good ideas\" but that only very few are actually trying to realize those \"good ideas\" at all as well as that there's too little support for those few who do try."
    author: "Datschge"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-26
    body: "Unfortunately, Quartz isn't actually OpenGL accelerated. OpenGL is only used to accelerate compositing of windows. Everything *inside* the windows is drawn in software. "
    author: "Rayiner Hashem"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-27
    body: "Which is a good thing to do. OpenGL sucks at 2D operations! Drawing 2D in OpenGL may actually slow things down."
    author: "FooBar"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-27
    body: "\"When you open your eyes and look around you'll see two big players on the desktopmarket. Apple has done a great job with their latest OpenGL accelerated userinterface.\"\n\nOh no not this overrated stuff again...\nQuartz is *slow*. It *feels* fast, but if you compare framerates then Quartz is *slower* than XFree86.\nJust by using OpenGL doesn't automatically give you drop shadows or uber-l33t-performance or whatever. OpenGL is meant for 3D. 2D operations in OpenGL is *slow*. Blitting in OpenGL is *very* slow. Imagine what happens when a high-framerate 2D app tries to blit everything through OpenGL. It would kill performance rather than improving it.\nThe usage of OpenGL only accelerates drawing in certain cases, but also *slows down* things in other cases.\n\n\n\"Really slick, and one of the major reasons OSX became intresting for the desktop.\"\n\nWhile still being stuck at 2% market share...\nWindows XP users don't seem to care about \"3D-accelerated GUIs\".\n\n\n\"I realize that not everyone has a state-of-the-art graphicscard in their system, but that problem could be solved by just letting the OpenGL renderer run in software mode\"\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nTry running Tux Racer in non-accelerated Mesa (software OpenGL rendering). Be happy if you can manage to exit Tux Racer in 10 minutes using the menu.\nDo you want your windows to draw at 0.5 fps too?\n\n\n\"So the question may be: \"Is it wise to stop innovation in fear of releasing the past?\"\"\n\nIt is not wise to implement hyped designs just because they're hyped. OpenGL is not the ultimate solution to everything!"
    author: "FooBar"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-27
    body: "Quartz is *slow*. \n>>>>>>>\nYes, it is. It's also all done in software. In OS X, Quartz Extreme (the OpenGL acceleration) only comes into play when all the drawing is done and it's time to composit all those transparent, drop-shadows windows together. All Quartz2D calls are still rendered via the CPU. The big reason Quartz Extreme was such a performance boost for the OS X folks is because, before QE, the CPU was doing all the drawing *and* all the compositing. \n\nBlitting in OpenGL is *very* slow. Imagine what happens when a high-framerate 2D app tries to blit everything through OpenGL. It would kill performance rather than improving it.\n>>>>>>>>\nActually, the speed of 2D operations is dependent on the implementation. On workstation-level implementations, 2D operations are very fast. On many consumer level implementations, the 2D subset (glCopyPixels, etc) is (was) rather slow. I haven't benchmarked 2D operations on modern NVIDIA (or ATI) hardware, but since's NVIDIA's OpenGL implementation is good enough to be in SGI workstations (all the Intel VPro chips are modified Quadros) I'd guess 2D operations would be quite fast. Of course, all of this is rather irrelevent. The modern way to do 2D blits is with textured quads. Both the ATI and NVIDIA implementations allow you to use arbitrary texture sizes, so no scaling is involved and the end result is exactly equivilent to blitting, and is *very* fast. It should be noted that modern consumer OpenGL implementations are very good. In the past, consumer-level cards struggled if more than one OpenGL window was in use at a time. On my GeForce4Go, however, 6 glxgears shows only a nominal drop in total framerate over the 1 window case, and with 3 windows there is actually an *increase* in total framerate. \n\nThe usage of OpenGL only accelerates drawing in certain cases, but also *slows down* things in other cases.\n >>>>>>>>>>\nDepends on the implementation, but that shouldn't be the case for any modern implementation. From what I've read at the SVGL and AmayaGL sites, most of SVG can be accelerated by OpenGL, including gradients and filters. \n \nWindows XP users don't seem to care about \"3D-accelerated GUIs\".\n>>>>>>>>>>\nHello? That's one of the huge new parts of Windows Longhorn. It's supposed to have a fully Direct3D accelerated GUI.\n\n Blitting in OpenGL is *very* slow. Imagine what happens when a high-framerate 2D app tries to blit everything through OpenGL. It would kill performance rather than improving it.\n\n Try running Tux Racer in non-accelerated Mesa (software OpenGL rendering). Be happy if you can manage to exit Tux Racer in 10 minutes using the menu.\n Do you want your windows to draw at 0.5 fps too?\n>>>>>>>>\nNow that's the kicker. Software rendered GL is *very *slow. This is where the \"leaving the past behind and daring to innovate\" bit comes into play. You can have an optimized software-rendering fallback, as EVAS does, but it won't really run apps that take full advantage of the increased rendering power. \n"
    author: "Rayiner Hashem"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-28
    body: "I've tried glDrawPixels a while ago on a TNT2 with NVidia drivers. It was horrid, taking near a second to blit something like a 640x480 rectangle.\nEven using XPutImage and synching the GL and 2D streams seemed faster, IIRC!\nTextured quads are an option, but they're also a /hack/. Unextended GL \ngives you only the power of 2 textures - other sizes are vendor extensions;\nand an implementation only has to support something that's 64x64, so the\nuser has to do tile cache management. It's prolly a big win in many cases\nwhen the HW accel is there, but it's also quite possible that the decicated 2D \naccel may be more usable on some hardware.\n\nAnd I think yes, the real kicker is that you can't really find out what will \nbe accelerated and what wouldn't be. That's because OpenGL is the wrong layer, really, IMHO. What we need is closer to the driver inside the GL implementation, which provides only the accelerated primitives, and the information on what it supports, etc.  Most of the upper layers are useless, and provide rendering that's too complex  for the 2D case. Do we need the T&L pipeline (which in S/W for older accelerators)? No. Support for controllable Z-buffer, etc.? Nope. \n"
    author: "SadEagle"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-28
    body: "How long ago did you try doing that test? I've got a TNT2 as well and a while back I tried using the OpenGL video output from mplayer and it crawled. It was worse than the plain x11 driver, and many, many times worse than xv. A few days ago I tried the gl driver again and this time it worked fine. As far as I could tell, the performance was the same as what xv provided."
    author: "Alex"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-28
    body: "Gosh, that could have been 1.5 - 2 years ago, true.. At any rate, current versions of mplayer use textured quads, not glDrawPixels, and they have for at least 2 years if WebCVS isn't lying. The difference you saw could be an improvement in the driver of course -- video obviously uploads lots of data, so it has to load new textures a lot; perhaps NVidia optimized anonymous texture upload a bit. On the other hand, it could also be an improvement to the software YUV -> RGB conversion it needs. At any rate, I think it'd be hard for OpenGL to compete vs Xv here -- Xv is very much designed for good/fast video, and it can handle YUV natively. \n\n\n"
    author: "Sad Eagle"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-28
    body: "\"Hello? That's one of the huge new parts of Windows Longhorn. It's supposed to have a fully Direct3D accelerated GUI.\"\n\nMicrosoft != it's users.\n*Microsoft* cares. Most of it's *users* do not. Try convincing my grandmother to switch to Longhorn just because it has this \"exciting new 3D GUI\"."
    author: "FooBar"
  - subject: "Re: Time to open the eyes"
    date: 2003-03-28
    body: "Users content to use legacy hardware should also be content to use legacy software, and stop ruining it for us that want something good!\n\nI have a h/w 3D card, and if that would make X really spiffy then I say, so be it!"
    author: "Idimmu"
  - subject: "Quartz Extreme framerates"
    date: 2003-05-02
    body: "FooBar wrote:\n> Quartz is *slow*. It *feels* fast, but if you compare framerates then\n> Quartz is *slower* than XFree86.\n\nThe Quartz display acceleration system normally limits framerates to the display refresh rate when an app issues multiple sequential flushes.  Trying to go faster than that produces no visible effect, as the intermediate frames simply won't appear on screen, and is effectively a waste of bus bandwidth.\n\nTry flushing a window as fast as you can.  The system will report a flush rate remarkably close to the refresh rate.\n\nAn app will get the best results and highest overall performance in either Quartz or Quartz Extreme by doing needed computations and I/O, then drawing, then issuing a flush.  This will interleave GPU and CPU operations. "
    author: "mpaque"
---
The <a href="http://www.xfree86.org/">XFree86 project</a>, one of the major window systems hosting the KDE/Qt platform, <a href="http://www.xfree86.org/4.3.0/">released version 4.3.0</a> almost a month ago. This version incorporates many frequently requested features such as fully graphical <a href="http://www.kdelook.org/index.php?xcontentmode=mouse">mouse cursors</a> with support for shadows and animation (<a href="http://www.kdelook.org/content/pics/5271-1.jpg">3d.jpg</a>, <a href="http://www.kdelook.org/content/pics/5507-1.jpg">gold.jpg</a>, <a href="http://www.kdelook.org/content/pics/5376-2.jpg">tux.jpg</a>), and on-the-fly <a href="http://lists.kde.org/?t=103503809300001">screen resolution</a> changes (<a href="http://yoyo.its.monash.edu.au/~meddie/patches/screenshots/dialog.png">dialog.png</a>, <a href="http://yoyo.its.monash.edu.au/~meddie/patches/screenshots/kcontrolmodule.png">kcontrolmodule.png</a>, <a href="http://yoyo.its.monash.edu.au/~meddie/patches/screenshots/notification.png">notification.png</a>, <a href="http://yoyo.its.monash.edu.au/~meddie/patches/screenshots/popup.png">popup.png</a>). In other news, <a href="http://xfree86.org/pipermail/forum/2003-March/000168.html">amidst</a> <a href="http://xfree86.org/pipermail/forum/2003-March/000476.html">some</a> <a href="http://www.advogato.org/person/mharris/diary.html?start=5">criticism</a> regarding XFree86's current development model, the project has gained a much needed public <a href="http://bugs.xfree86.org/">bug tracking system</a> as well as  a <a href="http://xfree86.org/pipermail/forum/2003-March/">new forum</a> inviting <a href="http://xfree86.org/pipermail/forum/2003-March/000001.html">public comment</a> on the direction of the project.
<!--break-->
<p>
The latter forum is already very active, and arguably a <a href="http://xfree86.org/pipermail/forum/2003-March/000446.html">success</a>, spawning an hourly updated <a href="http://www.xfree86.org/cvs/changes.html">XFree86 CVS changelog</a>.  Unfortunately, in a <a href="http://developers.slashdot.org/article.pl?sid=03/03/20/1215243">political shuffle</a>, much loved X developer <a href="http://www.keithp.com/">Keith Packard</a> involved with <a href="http://fontconfig.org/">FontConfig</a>, Xft and the X RENDER extension found himself ousted from the XFree86 Core Team.
<p>
<a name="Update1">
<b>Update: 03/24 18:50</b> by <a href="mailto:navindra@kde.org">N</a>: A group of KDE and GNOME developers have released <a href="http://www.xfree86.org/pipermail/forum/2003-March/000524.html">an open statement</a> on the matter.  The statement doesn't say much that hasn't already been said, though it lays out some key issues in point form.
<p>
<a name="Update2">
<b>Update: 03/25 00:36</b> by <a href="mailto:navindra@kde.org">N</a>: <a href="http://www.x.org/">X.Org</a> chairperson Steve Swales <a href="http://www.xfree86.org/pipermail/forum/2003-March/000554.html">speaks up</a> on behalf of the X.Org members.