---
title: "Kernel Cousin KDE Reborn, #47 is Up"
date:    2003-04-24
authors:
  - "numanee"
slug:    kernel-cousin-kde-reborn-47
comments:
  - subject: "Thanks Russell!"
    date: 2003-04-23
    body: "Me, it was me!!!"
    author: "The-one-who-always-wanted-to-be-the-1st-to-say-thx"
  - subject: "YES - Thanks Russell !! I Agree ..."
    date: 2003-04-24
    body: "Ooohhoooo \nI can't belive. \nTo be or not to be the first .... post\nOohooohhohohoooo no !!!\n\n>:-P\n\n"
    author: "The_one_who_can't_believe_this"
  - subject: "URL"
    date: 2003-04-23
    body: "I seem to remember reading in Kernel Traffic that the www.kerneltraffic.org domain was preferred to kt.zork.net for linking to them."
    author: "jb"
  - subject: "Big thanks"
    date: 2003-04-24
    body: "I wanna say a really big thanks for this as well - I like to keep half and eye on KDE news and this helps enormously. Many thanks\n\nAndy Cheung\nhttp://www.andycheung.com"
    author: "Andy Cheung"
  - subject: "About kmyfirewall"
    date: 2003-04-24
    body: "Being this mentioned in the Kenel Cousin... I must admit that kmyfirewall is being maintained quite well lately. Thanks a lot Chris!"
    author: "uga"
---
<a href="mailto:rmiller@duskglow.com">Russell Miller</a> recently took over as maintainer and editor for <a href="http://kt.zork.net/kde/archives.html">KC KDE</a> (from a long line of predecessors) and has lost no time in <a href="http://kt.zork.net/kde/kde20030419_47.html">releasing issue #47</a>!  This week he covers everything from KImageEdit MMX optimizations to <a href="http://kt.zork.net/kde/quotes/Ellis_Whitehead.html">KDE hacker</a> <a href="mailto:ellis@kde.org">Ellis Whitehead</a>'s joyful step up in life.  Congratulations Ellis, and thanks Russell!  The more news the merrier...
<!--break-->
