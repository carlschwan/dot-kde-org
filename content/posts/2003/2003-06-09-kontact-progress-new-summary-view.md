---
title: "Kontact Progress: New Summary View"
date:    2003-06-09
authors:
  - "f"
slug:    kontact-progress-new-summary-view
comments:
  - subject: "Great going!!!"
    date: 2003-06-09
    body: "Excellent work so far!!!\n\nCan't wait for the groupware functionality to be added so that we can kick out this Exchange stuff. :)\n\n"
    author: "Adriaan Putter"
  - subject: "Impressed!"
    date: 2003-06-09
    body: "Hello,\n\nI was previously against the integration because I thought It might make the programs run slower when integrated and opposed to when each program runs individually. But I must assert, I'm impressed. Those who said Microsoft Outlook has no rival, should think again. Thanks the KDE PIM developers for a great work. I can only hope it's fast and it works flawlessly.\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: Impressed!"
    date: 2003-06-10
    body: "Those who said Microsoft Outlook has no rival obviously haven't heard of Evolution.\n\nWill be waiting for Kontact."
    author: "Maynard"
  - subject: "KOrganizer part of Kontact"
    date: 2003-06-09
    body: "Kontact is a great piece of work, thanks for this ueber-flexible thingy !\nLast time I checked out Kontact from cvs I was hoping the most desired feature (by me) for KOrganizer got implemented already        ...hmpf.., ok... not yet...\n\nThe feature list says \"pinning of any mime type to an appointment or TODO Mark\"\n\nWhen I add an object (e.g. a pdf-file) to an appointment, is the actual pdf-file going to be stored in the calender? Or does KOrganizer only stores the URL to the file?\nAdding the URL of a file to an appointment would totally suit my needs, it may even be the easier option to implement... But, what happens if you'd like to publish an appointment with attached pdf-file to somebody else? An URL pointing to a pdf-file on your local harddisc is not sufficient."
    author: "Thomas"
  - subject: "will it use fetching mail in the background?"
    date: 2003-06-09
    body: "Will it still block the UI when fetching mail, or will it use a separate thread?\n\nWill there be an integration with the news reader knode?"
    author: "joe"
  - subject: "Re: will it use fetching mail in the background?"
    date: 2003-06-09
    body: "Yes, i think knode is missing :-("
    author: "Manu"
  - subject: "Re: will it use fetching mail in the background?"
    date: 2003-06-09
    body: "It is currently, however, I think the plan currently is that when knode's GUI is cleaned up, it'll be ready to put into kontact. Hopefully this will happen by the time Kontact is released in KDE 3.2. Perhaps some people can work on it in the kdedevelcon @ Nove Hardy."
    author: "lit"
  - subject: "Re: will it use fetching mail in the background?"
    date: 2003-06-10
    body: "I agree, this is the biggest problem with most kde programs.\n\nIn itself, everything goes fast enough, but more things should run in their own threads.\nI have a dual cpu in my desktop, but it doesn't make knode feel any bit faster when it is fetching the new headers than my old rusty laptop (the ui still blocks). \nKmail and Konqueror suffer from the exact same problem.\nFor example, I have a personal website, wich has a giant (750+ nodes) javascript tree in it (don't ask :), wich takes about 8 seconds to fill. During that time, konqueror is fully blocked, the UI doesn't update at all, I can't switch to another tab, ...\n\nIt's a shame that many programs can't actually use the dual cpu's (fortunately I can just switch to another program when konqueror/kmail/knode is blocking, and still run that one at full speed :)\n "
    author: "cxvx"
  - subject: "Re: will it use fetching mail in the background?"
    date: 2003-06-10
    body: "I think most KDE programmers were waiting for a decent thread implementation coming out for KDE's main platform: Linux. This comes in the form of NPTL. \n\nUnlike KDE 2.x, KDE 3.x has been thread-aware (and also relies on qt-mt), so apps can use it when they chose to.\n\nIn Konqueror, in the case you mentioned, I think a few calls to QApplication::processEvents would do the trick (http://doc.trolltech.com/3.2/qapplication.html#processEvents)\n\nIt's a lightweight method to give the GUI more reponsiveness during long tasks. I'm sure many KDE programmers are already aware of it."
    author: "reflux"
  - subject: "Re: will it use fetching mail in the background?"
    date: 2003-06-10
    body: "NPTL is mostly for high end server applications where very large numbers of threads are needed. It has minimal impact on the desktop.\n\nIf a network app isn't multithreaded properly then that's just a bug unfortunately. KHTML/Gecko are kind of special exceptions, writing a multithreaded rendering engine is *really* hard. OTOH, in Konqueror I was under the impression that KHTML was mostly a separate plugin part, unlike in Moz where the same rendering engine instance renders both the GUI and the web page (which is why moz can sometimes lock up when chewing large pages). So, it should be possible for KHTML to run in its own thread easily enough.\n\nMaking software more interruptable is another way of doing it, as you said...."
    author: "Mike Hearn"
  - subject: "Re: will it use fetching mail in the background?"
    date: 2003-06-10
    body: "Though I realize that you're referring to a more general problem -- here this isn't really a good example.  First, all of the apps that you listed use IOSlaves for their communication, which is out of process and asyncrynous.  Second multithreaded programming is much more complicated; making all of your logic ayncrynous is a lot of work.  Hopefully as our tool threading will be more of an option for some circumstances, but for most applications it's overkill."
    author: "Scott Wheeler"
  - subject: "Tab Interface"
    date: 2003-06-09
    body: "It's great what these developers are putting together.  Integration is a good thing, if you can keep the program startup time just as fast as before.\n\nI was looking at the interface and wondering one thing, though.  It seems nowadays that users are used to tab interfaces in web browsers and IRC clients.  Could this been a good place to expand on that and use it to switch between the integrated components of Kontact?\n\nKeramik is a pretty bulky GUI style already, but when they had the sidebars (kinda like the mozilla sidebar I guess), it *really* makes it bulky.  The sidebars really take up a lot of real estate.\n\nAnyone else agree with me, or am I smokin some good stuff?\n\nthanks,\njp"
    author: "Jeff P."
  - subject: "Re: Tab Interface"
    date: 2003-06-09
    body: "I like the sidebar interface.\n\n> The sidebars really take up a lot of real estate.\n\nTrue, i also noticed. But there is something overdone, IMHO, namely the title (the black bar below the toolbar). If the sidebar-button that is clicked jumps to the top it can serve as title too (a bit more like moz's sidebar behaves). This way the left part of the black title bar can be removed (saves some space).\n\nSome more bullshit:\nThe now empty Contacts sidebar tab can contain the search and search options wigets.\n\nAll so nice and integrated... And with the kolab-server (kolab.org) for the inter-client connectivity. >:-) Can't wait to happily use a stable vesion of this on daily basis.\n\nThis whole freedom of PIM thing will surely help to get freesoftware on lot of corporate desktops!"
    author: "cies"
  - subject: "What makes you truly free"
    date: 2003-06-09
    body: "is that this is not tied to any single server... all you really need is imap.\n\nIf you configure your client to use a mail server to store your PIM information then you get the same thing basicly.  Since KOLab uses Imap to export the data, you are not even locked into clients.\n\nYou are now free!\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "the names"
    date: 2003-06-09
    body: "It is always great to see the progress done by the KDE-developers, but out of duty  I must just point out for the millionth time that you really should choose names that doesn't depend on the k naming convention. It consistently results in ugly and clumsy names. First impressions matters disproportionatly much in relation to function, although I have a strong suspicion that you really haven't got an inkeling about what I am talking about here. To use an analogy, you might not use a lawyer who is showing up in a Hawaii shirt to your divorce meeting, even though he actually is quite capable. \n\nKnowing what you people think about this, writing this feels like trolling. But I though I am tired of belaboring this issue, it needs to be done."
    author: "will"
  - subject: "Re: the names"
    date: 2003-06-09
    body: "I'm not sure a name like \"outlook\" does any better at stating the functionality as \"contact\" or \"kontact.\"  We won't even try to figure out how \"evolution\" fits in.\n\nOnce an application is used and becomes know for what it does, the name really doesn't matter that much."
    author: "Paul Seamons"
  - subject: "Re: the names"
    date: 2003-06-09
    body: "Evolution is a good name - it implies that in a few million years it'll be really good. ;-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: the names"
    date: 2003-06-09
    body: "I think instead of changing binary names (which dont matter for many users), we should make \"show application description instead of name\" in the k-menu. \n\nUsing k-names is nothing new. I have 62 apps on my box starting with 'x'-- nearly all of them work on X11 someway (and most are packaged with Xfree86.)\n\nI have 43 starting with 'g'. 11 of these start with 'gtk' or 'gtk-'. 26 start with 'gnome-' or 'gnome'. \n\nI have 76 starting with 'k'. I think nearly all of them are KDE apps. \n\nThis kind of things exist in the commercial world. Microsoft Windows(tm)(c)(r), Microsoft Word, Microsoft Excel, Microsoft .NET, Microsoft Visual Studio, Microsoft Bob (=)), etc.."
    author: "reflux"
  - subject: "Re: the names"
    date: 2003-06-10
    body: "Start Control Center, go to 'Desktop' > 'Panels' > 'Menus' and change 'Menu item format' to 'Description (Name)' (don't ask for removing the name completely since there are many apps having similar descriptions, especially games)."
    author: "Datschge"
  - subject: "Re: the names"
    date: 2003-06-10
    body: "It's a start, but it isn't perfect. The ideal thing would be to have a user-friendly name, that could contain spaces, as well as a description. The reason is that, as you stated, many apps have similar descriptions. It would be good to have a way for users to identify each app uniquely, without needing to use rather counter-intuitive names... but this really belongs on usability@kde.org, where I hope to be back soon.\n\ndave"
    author: "David Hugh-Jones"
  - subject: "Re: the names"
    date: 2003-06-10
    body: "What are \"user-friendly\" names? You mean eg. I should change my nick since barely anyone can speak it out correctly and thus it's user-unfriendly? =P\n\nImproving descriptions: yes. Changing names: please don't."
    author: "Datschge"
  - subject: "Re: the names"
    date: 2003-06-12
    body: "\n\nI wasn't suggesting changing the actual names of the programs. That would be difficult in any case, since (AFAIK) it is not easy to run a program from the command line if its name contains a space.\n\nI was thinking of having names which were easier to understand, stored in the same way to the program descriptions. The underlying idea of this is that name != description and sometimes we want an easy way for people to talk about a particular program. (Mozilla and Konqueror are both \"web browsers\", but quite different in lots of ways.)\n\nWe already do this to some extent - e.g. konqueror is really launched by \"kfmclient\", at least on my system.\n\nI appreciate that programmers and users get attached to the program and know it by an existing name. I agree that it would be better to get the name right in the first place. But I also think that some program names are cryptic and confusing. (E.g., why is \"knode\" anything to do with news?) Furthermore, by their nature, program names can't be internationalized. This would not be the case for  the \"user-friendly names\" I am suggesting.\n\nDavid\n"
    author: "Dave"
  - subject: "Notes"
    date: 2003-06-09
    body: "Is it possible to have the notes shown as small yellow papers (with the text on them) instead of icons?\n\nLooks like a great app."
    author: "Kde User"
  - subject: "Re: Notes"
    date: 2003-06-10
    body: "A patch went in today or yesterday that displays the icon with the text of the note.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Notes"
    date: 2003-06-10
    body: "So is the text big enough to read then?\nJust curious..."
    author: "KDE User"
  - subject: "Please correct me if I'm wrong ;-)"
    date: 2003-06-09
    body: "Please correct me if I'm wrong ;-)\n\nKontact intergrates kmail, korganiser, knotes, etc. as kparts, right? So all programs can still be run individually.\n\nWill this continue to be this way? Or will kontact eventually eat all the apps (and become a fat bastard)?\n\nCheers!"
    author: "cies"
  - subject: "Re: Please correct me if I'm wrong ;-)"
    date: 2003-06-09
    body: "You can run them together/separate any way you slice it :)\n\nThe only issue imho is htat you can only run kmail either in kontact or stand alone, you cannot run two kmails at once.\n\nRemember this is KDE, KParts rock!\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Please correct me if I'm wrong ;-)"
    date: 2003-06-09
    body: "> The only issue imho is htat you can only run kmail either in kontact or stand alone, you cannot run two kmails at once.\n\nAre you sure about this? it works here. KUniqueApplications should not interfere with kparts, should they?\n\n> Remember this is KDE, KParts rock\n\nindeed."
    author: "lit"
  - subject: "Re: Please correct me if I'm wrong ;-)"
    date: 2003-06-09
    body: "Cool they fixed that,  I must note I have not tried to run both at the same time here in a while.  Granted IMAP may get cross with me, it would still be cool.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Please correct me if I'm wrong ;-)"
    date: 2003-06-19
    body: "Well, I'm quite sure that running KMail and Kontact _with active KMail part_ won't work, because\nof a unique lock file, created by the KMail part (which is used by Kontact an the stand alone KMail).\n\nAnd this is the correct behaviour IMHO.\nThe integration could be a bit better, so that when you try starting KMail via kicker and you have\nalready a running Kontact, the mail part inside Kontact raises automatically, but I'm quite sure\nsuch things will get fixed until KDE 3.2.\n"
    author: "Tobias Koenig"
  - subject: "So why did this work?"
    date: 2003-06-09
    body: "So why does Kontact work when Aethera/Magellan/Pick fork this week fail?\n\nSomeone tell me the codebase sucked, but kmail isnt any diamond either.\n"
    author: "Androgynous Howard"
  - subject: "Re: So why did this work?"
    date: 2003-06-09
    body: "Kontact uses stable code that already exists, and has been worked on for nearly five years--- (i.e, kmail, korganizer, etc..)\n\nAethera/Magellan decided to make a outlook-like app from scratch, instead of using well maintained, already established code. To their defense, kmail, korganizer, and knotes were not kparts when Magellan started, and thus, it would have been impossible to use things like kmail without the developers of those apps helping them. The kmail and other developers were not interested in making an outlook-like app back then-- they wanted seperate clients as had always been done in UNIX.\n\nI think the success of Evolution got it rolling, and the announcment of kroupware and kollab really made it have guarenteed success. "
    author: "lit"
  - subject: "Suggestions to improve task separation"
    date: 2003-06-09
    body: "Anybody else feeling not comfortable with the layout shown in the screenshots? What disturbs me is that the buttons to switch between the applications or tasks are actually integrated and look as being part of the current task (they are stacked together in a non-prominent way on the bottom left corner of the window. Compare this to e.g. evolution (http://www.ximian.com/products/evolution/screenshots.html), where the task switcher bar is placed to the very left and going from buttom to top. I feel that this is more intuitive. \nActually, I would even go as far as having the switcher bar going from the bottom to the very top so as to separate task better from each other, see the attached modified screenshot of evolution."
    author: "tuxo"
  - subject: "Re: Suggestions to improve task separation"
    date: 2003-06-09
    body: "Another suggestion of a modified (gimped) version of Kontact attached.\nTuxo"
    author: "tuxo"
  - subject: "Re: Suggestions to THE SIDEBAR"
    date: 2003-06-10
    body: "I posted the following in this discussion yesterday:\n\n> I like the sidebar interface.\n\n> > The sidebars really take up a lot of real estate.\n\n> True, i also noticed. But there is something overdone, IMHO, namely the title\n> (the black bar below the toolbar). If the sidebar-button that is clicked jumps to\n> the top it can serve as title too (a bit more like moz's sidebar behaves). This\n> way the left part of the black title bar can be removed (saves some space).\n\nNow everyone comes-up with mock-ups... So i want to show what i mean in a mock-up too!\n\nHere you go!\n\n-cies\n\n"
    author: "cies"
  - subject: "Re: Suggestions to THE SIDEBAR"
    date: 2003-06-10
    body: "The attachment thingy didn't worked out...\n\nNext try...\n\n"
    author: "cies"
  - subject: "Re: Suggestions to THE SIDEBAR"
    date: 2003-06-11
    body: "I like your mockup! Your proposition certainely goes in the right direction.\nTuxo"
    author: "tuxo"
  - subject: "Re: Suggestions to THE SIDEBAR"
    date: 2003-06-11
    body: "How to bring this to the attention of the coders then?\n(or did i got attention allready by posting here)\nIs is possible to file a bug/wish on a CVS version (with link to this mockup)?\n\n-Cies."
    author: "cies"
  - subject: "Re: Suggestions to THE SIDEBAR"
    date: 2003-06-11
    body: "> How to bring this to the attention of the coders then?\nWell, I see two posibilites:\n\n1) Write to the KDE application developer list http://lists.kde.org/?l=kde-devel&r=1&w=2\n\n2) As you say, try to file a bug report. However, I don't know if that is possible with an application only availble in CVS.\nMaybe it is a good idea to ask Datschge (datschge () gmx ! de) about it, I think he is involved with the bug database. Maybe it would be a good idea to open a new discussion entry into the bug database that could provide input to the developers by providing ideas/mockups/patches/ui-code etc?\n"
    author: "tuxo"
  - subject: "Re: Suggestions to improve task separation"
    date: 2003-06-09
    body: "Oups, the first attachement of a modified Evolution-version (gimped) didn't make it, sorry."
    author: "tuxo"
  - subject: "Re: Suggestions to improve task separation"
    date: 2003-06-09
    body: "I agree completely.\n\nI like the mockup that is posted as well."
    author: "TomL"
  - subject: "Re: Suggestions to improve task separation"
    date: 2003-06-10
    body: "> What disturbs me is that the buttons to switch between the applications or tasks are actually integrated and look as being part of the current task (they are stacked together in a non-prominent way on the bottom left corner of the window\n\nI think that's one of the points. It does save some screen-estate for what it's worth."
    author: "fault"
  - subject: "Nice Work - Will this be in 3.2 or Sooner?"
    date: 2003-06-09
    body: "Can't wait to use it - photo contacts are really nice and it's nice to put faces to the names of those we thank.  will Kontact synch with my Palm Pilot?  \nbtw - I like the K names - it makes it easy to identify a KDE program.\nJohn"
    author: "J Taber"
  - subject: "I am also impressed!"
    date: 2003-06-10
    body: "Kontact is starting to become a worthy competitior to Evolution on the Linux platform."
    author: "MArio"
  - subject: "Does this work with kde3.1.2??"
    date: 2003-06-10
    body: "Hi All,\n    I want to get the latest cvs version of kontact and run it with my existing kde3.1.2. Is this possible, any pointers on how to do it would be highly appreciated.\nThanks"
    author: "Raj"
  - subject: "Re: Does this work with kde3.1.2??"
    date: 2003-06-10
    body: "No, sorry. It does not work with KDE 3.1.x. Current kontact needs the CVS version (HEAD) of kdelibs, kdepim and maybe even kdebase (imap-slave).\n\nciao"
    author: "Guenter Schwann"
  - subject: "I still just don't get it"
    date: 2003-06-10
    body: "I'm staring at these screenshots and still scratching my head.  I honestly don't understand why grouping these very seperate applications into one window with a huge chunk set aside for tabs makes this better.  I guess I just don't see any relationship between an application for calendaring/scheduling and E-Mail.  I really don't understand how KNotes fits into any of this.\n\nI do appreciate the fact that the end game is an Outlook replacement.  I never did understand the relationship of these apps in there either.  I understand that at times you may want to send an appointment out through E-Mail, but I also send web links via Konqueror without having it all embedded into one application window.\n\nThis isn't just me trolling the site here.  I honestly don't get it.  All these apps getting rolled in here work beautifully as stand alone programs with pipes to eachother.  I can see the need for better conversing between them, but do they have to all go in one window to accomplish this?\n\nI am pretty slow on the uptake sometimes.  I recall reading tech reviews raving about how IE was going to be used as the file manager.  At the time I couldn't being to understand why someone would want that.  I've never wanted to manage files while web browsing, or vice versa.  Years later I still have never wanted to do this, nor have I run across users who wanted this either.\n\nPerhaps this is a poor comparison, but from here it sure feels similar."
    author: "Metrol"
  - subject: "Re: I still just don't get it"
    date: 2003-06-10
    body: "I agree with you, though my boss switched from kmail to evolution just because of the integration.    Now he'd like to switch back when kontact is done, but doesn't look forward to moving his data.   I don't *think* he'll have an issue because I believe both use standard formats for contacts and mail, which is all he needs, I think.  I don't know why he wants it all integrated, but I didn't use to believe that a GUI email client would be useful.  I used mh and thought it was the be all and end all of mail handling.   Now, while I still like it I don't want to go back to it.   I loved exmh so that I could do both gui and command line munging of my email.  But these days exmh seems very clunky.   But I also can't really stand evolution.   But that's not because of it's all-in-one approach, I just think it's clunky and somewhat ugly, except for the icons.   I'm am VERY sure (judging from the rest of KDE) that kontact will be a much better application for me than evolution could ever be.   Partly because the KDE guys are so good that I think kontact (and I do like the name) will be great, but also because I believe that one of the winning factors of KDE is that it's so well integrated.  So kontact will be a better part of my desktop in general than evolution could ever be.  Ditto for Konqueror vs. mozilla or galeon.   But all that's just me.   I realize that others disagree and love evolution.   My boss doesn't and would rather use a KDE app.\n\nI am not sure that knode needs to be part of it, though I'm glad to see the summary page.  It doesn't look quite as good as evolution's, but it's a neat start."
    author: "TomL"
  - subject: "Re: I still just don't get it"
    date: 2003-06-10
    body: "It's the 'activity center' principle. You bundle functions that you use together in order to do stuff with fewer clicks. (Another one is listening to music, see RealOne or Windows Media)\n"
    author: "AC"
  - subject: "Re: I still just don't get it"
    date: 2003-06-10
    body: "The point is that a lot of people do expect things to work this way.  What's so great about this solution is that it allows you to use the same applications in a non-integrated way or to choose to use them as a group.  It's all up to you.  :-)"
    author: "Scott Wheeler"
  - subject: "Palm Pilot and distribution list support?"
    date: 2003-06-10
    body: "Does Kontact allow syncing with a Palm Pilot?\n\nAnd have they finally added distribution lists or contact lists to kmail? Currently there seems to be no easy way of assigning a name to a list of contacts and it was one of the main reasons I switched to evolution."
    author: "Jason"
  - subject: "The app I've been waiting for"
    date: 2003-06-10
    body: "Every time I get into a Linux discussion the subject of groupware comes up, and KDE is definately the best platform for this. The potential for contact is tremendous, but it will certainly be hard to compete with the reputation Evolution has established. Kontact firstly will need major UI polish, especially the calendaring. KOrganizer is very powerful, but the interface is quite clunky and seems to just be crammed together. The Evolution views are much more appealing. One thing I haven't heard anyone mention was Kopete integration. *That* would fantastic! I wish I was a C++ coder...I'd certainly pitch in."
    author: "Eron Lloyd"
  - subject: "Re: The app I've been waiting for"
    date: 2003-06-10
    body: "You don't need to be a C++ coder to help. You can draw icons, translate or write documentation. Or you just tell the korganizer developers what _axactly_ is clunky, and how you suggest to improve the UI.\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: The app I've been waiting for"
    date: 2003-06-10
    body: "True, perhaps I could make some UI files and prototype things in PyQt...\n\nThanks,\n\nEron"
    author: "Eron Lloyd"
  - subject: "How to backup your data?"
    date: 2003-06-10
    body: "How easy will backing up your data be?\n\nWhen you use cron for making backups, how easy will it be to do a backup of all your 'groupware' data in one go? (mails, contacts, organizer etc.)\n\nSame goes for restoring that data?"
    author: "IR"
  - subject: "Customisability"
    date: 2003-06-11
    body: "Please tell me that the layout will be more customisable  than KMail currently is.  For instance, can the preview pane be turned off??  Can the annoying panel on the left be disabled?  If not, can we limit what appears in the left panel (for instance, only mail directories)?\n\nIf the layout is not customisable, then few people will want to use it.  The whole \"Outlook\" layout is not productive."
    author: "Phoenix"
  - subject: "Re: Customisability"
    date: 2003-06-11
    body: ">>If the layout is not customisable, then few people will want to use it. The whole \"Outlook\" layout is not productive.<<\n\nI guess that's why so few people are using outlook <g>\n\n"
    author: "AC"
  - subject: "data in DBMS + web interface will make it  perfect"
    date: 2003-06-12
    body: "How PIM information are currently stored? Is it possible to have them stored on an  RDBMS (postgres or MySQL preferred). This feature will simplify agenda sharing but also back-up in an Enterprise (where they don't want to have exchange).\n\nAnother value added could be the possibility to have a web interface to access to those data (again this could be easily done if data are stored in a RDBMS).\n\nIanna"
    author: "ianna"
---
<a href="http://kontact.kde.org/">Kontact</a>, the <a href="<a 
href="http://pim.kde.org/">KDE personal 
information management</a> application evolves from day to day. The 
integration of <a href="http://kmail.kde.org/">KMail</a>, 
<a href="http://www.korganizer.org/">KOrganizer</a>, 
<a href="http://www.kaddressbook.org/">KAddressBook</a> and 
KNotes is nearly finished now and over the weekend the summary 
view -- which can display current weather information and upcoming events such as birthdays -- was added.
It is planned to get the groupware functionality working in time for the 
<a href="http://www.linuxtag.org/">LinuxTag</a> in Karlsruhe and 
present a replacement for Outlook/Exchange. 
If you are interested in current screenshots, <a 
href="http://wgess16.dyndns.org/~tobias/kde/kontact/index.html">take a look
here</a>.  I would like to personally thank all the KDE PIM developers, especially those who worked so hard on the new summary view!
<!--break-->
