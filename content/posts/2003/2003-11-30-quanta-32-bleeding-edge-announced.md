---
title: "Quanta 3.2 Bleeding Edge Announced"
date:    2003-11-30
authors:
  - "numanee"
slug:    quanta-32-bleeding-edge-announced
comments:
  - subject: "VPL Mode"
    date: 2003-11-29
    body: "Any screenshots of this available?"
    author: "Anonymous"
  - subject: "Re: VPL Mode"
    date: 2003-11-30
    body: "Hmmm? Guess how tired I was at 5 AM when I finally got everything done not to anticipate people wanting to see screenshots. D'Oh! I'll put some up real soon and post back here when I do. Sorry about that. I will take some new ones as there actually is a fair amount to see and this would be really good. thanks for asking the \"obvious\" question. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: VPL Mode"
    date: 2003-11-30
    body: "Okay... once again I'm getting to bed at 5 AM on one of the busiest sales days of the year for Kitty Hooch. (That's okay I guess because I didn't fall over Saturday and did manage to pay some bills.) I looked through what I had and also took some more screen shots. I posted 28 screen shots. If you go to the regular link on the top of the page is a link to the BE shots. I only have two VPL mode shots for now including the dual mode, because I think that's very cool. I did them alphabetically so they happen to be last. Several other shots tie into what is being developed to support VPL. Also, there's a whole lot of other development besides VPL and much of it has interesting visuals. I wanted to give a good picture of where Quanta is now.\n\nI hope these shots help to paint a picture of all the work we have been doing to make Quanta better. I think it's safe to say that what we are releasing now is the largest single jump in features in the history of Quanta. It's a big leap forward and we are not near done. So keep checking out what's new. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: VPL Mode"
    date: 2003-11-30
    body: "Looks great!!\n\nI'm looking forward to this.  I know a lot of windows people are awaiting something like this, too.  Looks like they're gonna get even more than they hoped for :)\n\nMy curiosity raises a couple of technical questions, though: is the VPL capable of handling everything?  I guess what I'm asking is... is it a hack?  Are there any fundamental limits on what it can do, or can it keep up with new css features, etc?  It's built on konqueror with a few new hooks, right?\n\nWhat about server-side debugging etc?  Is that in there, or is it planned?  Server-side python would blow me away (even more ;), although I realise that's unlikely :)\n\nFinally... any rough estimates on when I can play with this in debian? :)\n"
    author: "Lee"
  - subject: "Re: VPL Mode"
    date: 2003-12-01
    body: "> Looks great!!\n\nThanks!\n \n> I'm looking forward to this. I know a lot of windows people are awaiting something like this, too. Looks like they're gonna get even more than they hoped for :)\n\nWell I hope so... Of course I've been hoping they'd stop waiting and join the development team but I'm ever the optimist. ;-)\n \n> My curiosity raises a couple of technical questions, though: is the VPL capable of handling everything? I guess what I'm asking is... is it a hack? Are there any fundamental limits on what it can do, or can it keep up with new css features, etc? It's built on konqueror with a few new hooks, right?\n\nYeah... a hack. Shhh! :-O Hey you'd really have to look at what it is to answer your question. It's basically subclassing khtml using a new public interface and adding new user interfaces to integrate with Quanta's infrastructure. That probably doesn't answer the question for a lot of people either. KHTML is the engine the renders web pages in Konqueror. It's being extended with a public interface, which means in non programming terms that what we're doing will become codified and protected so functionality won't be broken or lost in an upgrade. The capabilities are an extention of what Quanta can already do as well as new functionality that's specific to the issues of visually editing markup. The simple answer to your question is that it is not theoretically limited however the first release will have some practical limitations in implementation because it just takes time. For static pages it will be nearly identical to preview but some of the things done for visual queues will make it appear very slightly different. \n\nOur objective is to step beyond accepted behaviors and performances and ask how we might accomplish tasks that need to be done instead of just assuming they are all being done the best way already. In this regard capabilities and limitations of editing and rendering become two different sets. Depending on your task you may want visual perfection or you might want visual feedback that is not part of the actual render.\n \n> What about server-side debugging etc? Is that in there, or is it planned? Server-side python would blow me away (even more ;), although I realise that's unlikely :)\n\nServer side preview is already there regardless of your programming language or module. This enables review of dynamic content in preview. Any use of this in VPL is an incredibly complex proposition as it becomes an intermediary layer that is absurdly difficult to make bi-directional changes on. However for debugging we are working on a generic debugger interface that can easily be extended for plugging in any debugger. Right now it works with Xdebug and Gubed for PHP. This will tightly integrate with Quanta so that debugging is intuitive and fully featured. We hope to be able to introduce in in a future BE very soon but it was not yet stable enough at this time. If you can recommend a Python debugger and especially if we have a developer or at least a very good tester we could make headway there.\n \n> Finally... any rough estimates on when I can play with this in debian? :)\n\nNow if you want to build from source. ;-) If someone knows who the Debian maintainer is now for KDE please have him get in touch with me. It appears Ben is no longer the guy. I've been in touch with the Gentoo maintainer and we are looking at having a BE build for it probably right after beta 2 arrives."
    author: "Eric Laffoon"
  - subject: "Re: VPL Mode"
    date: 2005-04-22
    body: "a tutorial \"Debugging PHP scripts with Quanta Plus and Gubed PHP Debugger\" can be found here:\n\nhttp://www.very-clever.com/quanta-gubed-debugging.php"
    author: "tom"
  - subject: "Re: VPL Mode"
    date: 2003-11-30
    body: "Wow.  Those are some very impressive screenshots!"
    author: "anonymous"
  - subject: "Re: VPL Mode"
    date: 2003-12-01
    body: "> Wow. Those are some very impressive screenshots!\n\nThanks. I needed to read that after all the time I spent on it. \n\n(Anonymous? I've heard of you ;)"
    author: "Eric Laffoon"
  - subject: "Compilable on 3.2 Beta?"
    date: 2003-11-29
    body: "Is this really compilable on KDE 3.2 Beta?  make gives me this error:\n\n../document.h:41:42: ktexteditor/editinterfaceext.h: No such file or directory\n\nI do have ktexteditor/editinterface.h, but no editinterfaceext.h.  I suspect maybe this was some non-public API that was removed in the beta.  Any ideas?"
    author: "Haakon Nilsen"
  - subject: "Re: Compilable on 3.2 Beta?"
    date: 2003-11-29
    body: "OOps, no. It requires KDE 3.1.x or latest CVS (or the upcoming Beta2)."
    author: "Andras Mantia"
  - subject: "beta2?"
    date: 2003-11-29
    body: "I think the \"latest beta\" is wrong. The page says \"upcoming beta2\"."
    author: "tj"
  - subject: "Re: beta2?"
    date: 2003-11-29
    body: "Thanks."
    author: "Navindra Umanee"
  - subject: "VPL"
    date: 2003-11-30
    body: "Where are the screenshots of VPL?  How is it coming along?  I'm interested in this new Quanta mode."
    author: "Spy Hunter"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "(me too :-)\n\nIs VPL == quanta/quanta/parts/kafka ?\nI can't compile that\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "> Is VPL == quanta/quanta/parts/kafka ?\n\nEssentially yes. There's a good deal of integration with Quanta internals and also a lot of new public interface in KHTML supporting this too. Of course you need them all.\n\n> I can't compile that\n\nMost likely that would be because you are on KDE 3.1. As I mentioned there are a lot of KHTML changes. Also, you might not expect this, but because of the integration we also use some new features in the Kate part. Currently we have been working to get VPL as usable as possible and since changes were required in these parts of kdelibs it doesn't build and run unless you are using a fairly new CVS build on KDE 3.2. I can't even imagine the issues to back port this to KDE 3.1. So it is a little awkward to deliver on KDE 3.2 but not many people are running it yet. Also, because this is a development branch there are small enhancements already showing up that either are not yet in HEAD or won't be in until after release due to freezes. It should run fine on beta 2 and we will most likely release again before the release candidate.\n"
    author: "Eric Laffoon"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "No, here is KDE-cvs-HEAD andgcc-3.2 (Stable is for cowards :-P )\n\nmake[1]: Entering directory `/usr/local/src/kde-cvs/quanta/quanta/parts/kafka'\nsource='htmlenhancer.cpp' object='htmlenhancer.lo' libtool=yes \\\ndepfile='.deps/htmlenhancer.Plo' tmpdepfile='.deps/htmlenhancer.TPlo' \\\ndepmode=gcc3 /bin/sh ../../../admin/depcomp \\\n/bin/sh ../../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../../.. -I../../.. -I../../../quanta/parts/kafka  -I/usr/kde/3.1/include -I/usr/qt/3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT -DKOMMANDER -DDESIGNER -DQT_NO_SQL -DHAVE_KDE  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -Wcast-align -Wconversion -Wchar-subscripts -Wformat-security -Wmissing-format-attribute -DLIBXML_2_5 -c -o htmlenhancer.lo `test -f 'htmlenhancer.cpp' || echo './'`htmlenhancer.cpp\nhtmlenhancer.cpp: In member function `virtual bool\n   HTMLEnhancer::enhanceNode(Node*, DOM::Node, DOM::Node)':\nhtmlenhancer.cpp:58: `class Node' has no member named `_rootNode'\n[...]\nhtmlenhancer.cpp:131: `class Node' has no member named `_leafNode'\n[...]\nmake[1]: *** [htmlenhancer.lo] Error 1\nmake[1]: Leaving directory `/usr/local/src/kde-cvs/quanta/quanta/parts/kafka'\nmake: *** [all-recursive] Error\n\nSorry for misusing the dots for reporting compiling problem :-}\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "Please contact us via e-mail (directly or through the user list). I don't\nsee any error in the code, so I suspect a local configuration problem at you. Send me the configure output.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "From the old HEAD setup I used\n./configure --with-kafkapart\nThis does not work and make these errors. A clean checkout and reinstall worked. Playing with VPL is nice. Ok, this stuff needs some work and quanta crashed after a while.\n\nBut Eric and Andras:\nQuanta is a realy nice and useful productive tool. Sometimes I wish that Quanta gets more visual tools like building clickable graphics\n(kimagemapeditor) or an active-link-checker integrated in quantas workflow. Maybe Kommander is here a solution. For working in a intuitive way please pay attention to the plugin and tools menu: I don't think that its good to split these helpers into 2 main menus(?)\nPlease look from time to time to Dreamweaver and Adobe Golive: They have some interesting GUIs and visual tools for the web workflow. So far Quanta is still more web programming than web designing.\n\nKeep up the good work!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "KImageMapEditor can be used as a plugin. The author of it converted to a KPart quite some time ago and he advertises that you can use inside\nQuanta. :-) We just doesn't ship it by default.\nRegarding menus, especially the Plugin menu: yes, there is room to improve."
    author: "Andras Mantia"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "> ./configure --with-kafkapart\n \nThis is not needed as it's now the default.\n\n> Sometimes I wish that Quanta gets more visual tools like building clickable graphics (kimagemapeditor)\n\nSee the screenshots. It integrates as a kpart and of course, we didn't have to build it. ;-)\n\n> or an active-link-checker integrated in quantas workflow. Maybe Kommander is here a solution.\n\nAs I've often said, our problem has never been one of ideas, but of resources. (Though occasionally users send in really good ideas) One of our ideas I plan on building shortly will obsolete link checkers, however there's piles of scripts our there that do a decent job. There's not much to them. You can use the script directory and select a launch method. If you have parameters to set on that Kommander works great for it. That is why we made Quanta user extensible. In spite of us only being a handful of guys there's an emberassing wealth of free Unix type utilities like link checking scripts that can be integrated into Quanta. We're not trying to make people dependent on our tool. We're trying to make our tool the most dependable core of their toolkit.\n\n> Please look from time to time to Dreamweaver and Adobe Golive: They have some interesting GUIs and visual tools for the web workflow.\n\nI've looked through their preview sales info. I'm not saying it's bad but the windows way is to worship the GUI and forget the power of the console and scripting. These interfaces can be cute but get in your way when you want to be fast. Theirs have had time to refine but to me they show the differences in how one team or programmer approaches things and how it's all custom built for each thing. Quanta is not as glitzy because some of of our reuse is not the sexiest choice and certainly we haven't had time to finish our vision either. Personally I'm not against taking a good idea from these tools but I don't want to get myself dirty with a lot of what I don't think are good ideas that might muddy my thinking.\n\nThere are several ideas we plan on introducing shortly, and in discussions with users of commercial tools the conclusions are that there is nothing even remotely close to these ideas. I don't personally feel there is much real innovation on that platform anyway. I'm betting that a year from now a hell of a lot more people will be telling those guys to look at us but they already will be. Frankly I'd much rather lead than copy so I don't care to start sniffing their flanks right now when I'm going for the fresh air in front. ;-)\n\n> So far Quanta is still more web programming than web designing.\n\nIt takes time. And web designing from the \"rearrange the static nightmare and make users dependent on our tool to fix the mess\" perspective promoted by these guys is not anything to be proud of. Even when our design elements are incorporated they will work to advance new paradigms and further W3C usage instead of coddling lamers who can't even understand HTML 3.2. We seek to make our users more empowered, not more dependent.\n\n> Keep up the good work!\n\nYou know we will. ;-) Thanks!"
    author: "Eric Laffoon"
  - subject: "Re: VPL"
    date: 2003-12-01
    body: "> > Please look from time to time to Dreamweaver and Adobe Golive: They have some interesting GUIs and visual tools for the web workflow.\n\n> I've looked through their preview sales info. I'm not saying it's bad but the > windows way is to worship the GUI and forget the power of the console and scripting. \n\nActually Dreamweaver is mostly javascript underneath, take a look in the data directories and you'll see that you can tweak a hell of a lot.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: VPL"
    date: 2003-11-30
    body: "> Where are the screenshots of VPL?\n\nAs I mentioned on another post, I kind of zoned out on this. I'll take new ones and post them soon. Look for a mention on the other thread here.\n\n> How is it coming along?\n\nIt's progressing fairly well I think. Really it's difficult to assess in some ways because there are a lot of pieces coming together and things can change dramatically on convergence. The later release of KDE 3.2 was less of an issue for Quanta with general bugs as our counts are pretty low. However in the areas of the parser and VPL there's a lot that needs \"fixed\". We have just done a substantial rework of the parser which provides benefits to speed and bug fixes in Quanta as well as greater capabilities. It also broke some things in VPL. Most of that has been fixed and a number of issues have been taken care of so that VPL is marginally usable now. It's usable for most editing tasks but has some focus issues for new element creation moving to the next element.\n\nIf I were to give a candid assessment I'd say that it's several months behind where I'd like it to be in a perfect world. I believe it will be stable and usable for 3.2 but we are just getting to reviewing how our theories work in use right now. This leads to fine tuning and if an interface enhancement is indicated it's pretty much going to end up in the BE branch. Some of our objectives that would set VPL way ahead such as being usable for non HTML markup will not be in this release. What VPL *will* accomplish in 3.2 is to write DTD compliant markup and *not* mangle your document. So if it falls short of virtual perfection it will at least avoid the terrible legacy of classic WYSIWYG markup butchers like FrontPage. Refinements will be available in futre BE releases prior to KDE 3.3/4.0.\n\n> I'm interested in this new Quanta mode.\n\nI think a lot of people are. One very cool thing that it does right now very well is to offer a split mode of visual and text where you can click on the page and have the cursor follow in text. Even being able to point on a page and go right to the underlying markup is quite liberating for an old hand coder like me. ;-)"
    author: "Eric Laffoon"
  - subject: "why can't we hide the tree views?"
    date: 2003-11-30
    body: "It's a small thing, but it's one that i miss alot! Why isn't possible to have a colapsable tree view? Something like konqueror has on it's sidebar, if you click on a icon it opens the view, but if you you click on a icon of something already open it colapses the sidebar... Horizontal space (space in general) is very important when you code... and now that quanta have a visual editor it should be more important! it's annoying all that horizontal scroll... \nAnd no.... windows floating around it is not the solution!\n\n\nRegards"
    author: "Castanheiro"
  - subject: "Re: why can't we hide the tree views?"
    date: 2003-11-30
    body: "> Why isn't possible to have a colapsable tree view?\n\nIt is possible! All you have to do is enable a highly volatile option on configure (--enable-kmdi) and you will have all that and more. As usual, the only thing we're really missing is the time for this few people to complete that many tasks. ;-) We hope to introduce it very soon on a BE release. This will be pretty much what the new Kdevelop does for selecting windowing modes and collapsing panels. Wait a little and your patience will be rewarded... or send patches. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: why can't we hide the tree views?"
    date: 2003-11-30
    body: "Thanks!!! It would be a big usability improvement! \n\nRegards."
    author: "Castanheiro"
  - subject: "Thank you"
    date: 2003-11-30
    body: "Quyanta is reallly a fantastic tool, and it's amazing to see the progress it's making in such a short period of time. \n\nThank you Andras, Eric and other Quanta contributors, your work is greately appreciated!"
    author: "Alex"
  - subject: "Quanta and W3C"
    date: 2003-12-01
    body: "For the Quanta team :\nCan you please remove w3c html4 logo on the quanta homepage or correct the page ?\nIt is not serious to keep the logo and that the website doesn't validate to w3c standards : http://validator.w3.org/check?uri=http%3A%2F%2Fquanta.sourceforge.net%2F\n\nMuch better, you can use XHTML and CSS and remove all the tables used only for design.\n\nThanks,\n\nFranck\n"
    author: "Shift"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "Woo! You tell 'em! Yay!\n\nI worked up something a while ago for the site, but Eric didn't accept it. :-(\nhttp://openschooling.org/~jilks/quanta_Web/index.php (Some stuff is broken 'cause I've been playing with CSS, PHP, and bunches of other stuff, but you get the idea.) That site will generate several versions of itself depending on what it -thinks- the browser can handle (it's a great thinker, too). (HTML or XHTML (Strict, and all valid), no, low, or high color graphics, and so on.) Design-wise, it's pretty pimp. Style-wise, it's not so great. All it takes to change it is a little CSS, though. :-)\n\nI still need to figure out a way to show that the XHTML is valid. Viewing in Mozilla tells you it is, but W3C's validator only gets HTML. I'll probably need to force it with a flag or something...\n\nEnjoy!"
    author: "Jilks"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "Oh, I forgot to mention that some pages don't work due to me not creating them. Also, those pages (that work) should do pretty well accessibility-wise. ;-)"
    author: "Jilks"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "Lucky me! Heckled by my own team member. I can't pick on Chris too much because he's done a lot for Quanta... so this is said with kindness...\n\nFor the record Chris, as you're posting this publicly without emailing me first. I never rejected what you sent in. I just haven't had time to look at integrating it. I've been too busy. I should also note you volunteered your work without first discussing various criteria and concerns with me, thus creating a need for major changes to use your work. It completely breaks the modular layout I use with PHP. Had you discussed it with me then I'd have given you access to my CVS and discussed objectives. Then we would actually function as a team. Also, the original site was largely done by Alex who was very HTML 3.2 oriented and it was an absolute mess. I cleaned and processed the site and validated it but as the pages are assembled in PHP it seems some of his code has slipped back in. I have a lot of experience in PHP and web development so when I go to implement changes I like to make sure that they don't create problems for me if I want to maintain it. \n\nI'm not trying to be obnoxious. They validated when I tested it, even if they don't now. I found several things in there that are just ugly too that were done before I decided I needed to exclude sloppy coders. It's on my do list but I honestly have so much to do I really need 2-3 of me for my business and another 2-3 of me for KDE/Quanta. So if it really bugs you send a patch.\n\nBefore the end of the year I plan to completely revise the site and add online resource search/download/submit directly from within Quanta. I had planned on asking Chris if he'd like to help with this. In the mean time I have had to make public appeals to get the money to pay Andras as recently as last month and now I need to get my business squared away with major revisions on my business site and a big ebay launch so I can stop bleeding money and begging for help and start enjoying discretionary income. I don't even have cable TV because I can't spare the money for it or the time to watch it. I'll get to it this month and that's going to have to be good enough."
    author: "Eric Laffoon"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-02
    body: "> Heckled by my own team member. \n\nOh, don't worry about that. I do everyone. :-) Sometimes I even email people randomly that use XHTML + CSS just to say, \"Hey, nice to see more and more people using XHTML + CSS! Nice work!\""
    author: "Jilks"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "Personally I've found using CSS for positioning to be a pain in the neck (specifically relative positioning). I gave up on it in the end as one those 'nice-in-theory' things that's just not worth hassle in practice.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "LOL\n\nThanks Rich. Now I feel better. I support the W3C and their efforts, but I can write W3C complaint HTML that won't render on any browser or is managed very differently from one to another. While people suggest non tabled layouts they never seem to send a patch. A look around the web shows almost nobody moving away from tables yet. \n\nIts very easy to repeat an idea but much harder to put ideas in action and do something."
    author: "Eric Laffoon"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "It is possible and it is more clear than tables :\nhttp://shift.free.fr/\nhttp://shift.freezope.org/\n\nI will never suggest to use CSS+XHTML if I haven't done it yet on my website ;)"
    author: "Shift"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-02
    body: "Whoa! Konq Rellinks is great! This should be in Konqueror! (Or is it and I'm blind?) This would be a great usability enhancement!\n\nBTW, nice sites. ;-)"
    author: "Jilks"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-02
    body: "It is not in Konqueror (yet?) because I haven't got much time to work on it and I have lot's of small bugs because it is my first C++ experience.\n\nIf somebody want to help me it will be great !!\nJust mail me :)"
    author: "Shift"
  - subject: "Request for Konqueror developers"
    date: 2003-12-04
    body: "If you are a Konqueror developer, I have a request.\n\nI tried submitting this to the bug list (Mozilla now that I think about it, maybe not Konqueror), but I'm too dumb to figure out the complicated bug submission site (must be Mozilla).\n\nCan someone PLEASE, PLEASE, PLEASE set up the pop up screen that asks if you want to download/install flash so there is a 3rd choice, No, AND DON'T ASK AGAIN, instead of just Download/Cancel or Yes/Cancel or whatever it is now?\n\nIf I were a programmer, this would be my first patch.  But I'm not...so I'm asking here.  Maybe you can submit it as a bug/feature request for me if you aren't interested in doing it yourself.\n\nThank you VERY much in advance for the consideration."
    author: "No Flash Please"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "That's prolly because you tried writing it by hand. Complex layouts without a WYSIWYG tool are certainly not a good idea."
    author: "AC"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "That may be true, but I don't know any tools that allow you to use relative positioning effectively (including dreamweaver), and if you use absolute positioning your page looks shit as soon as someone resizes it or changes the font size. So, if we can't do it by hand and there are no tools we're back at the conclusion that CSS relative positioning is not worth the trouble.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Quanta and W3C"
    date: 2003-12-01
    body: "Oh! absolute positioning is really easy and effective! The trick is to ensure your  divs (or whatever) are the correct size and to use percentages to position them. Any sized browser window and any resolution will all look very much the same. That allows you do put the main content at the top of the source and everything else at the bottom, which has the effect of making it -seem- like the page loads faster."
    author: "Jilks"
  - subject: "More stars but still no telescope..."
    date: 2003-12-01
    body: "Thanks for the good work once again !\nHowever in my opinion the major problem when working with quanta is not bleeding edge stuff like VPL. It's fun to see how these new features are implemented. But we quit using quanta because of one thing:\nMissing support of debugging facilities. \nOr has anyone meanwhile succeeded in getting that php debug stuff in quanta to work? Whithout useable support of a debugger quanta is a nice thing, good look and feel, but it's not suitable for real programming tasks.\nOnce again: thanks for quanta, a good peace of work.\nArkascha"
    author: "Arkascha"
  - subject: "Re: More stars but still no telescope..."
    date: 2003-12-01
    body: "> Thanks for the good work once again !\n\nThank you. That's always nice to read.\n\n> However in my opinion the major problem when working with quanta is not bleeding edge stuff like VPL. It's fun to see how these new features are implemented. But we quit using quanta because of one thing:\n> Missing support of debugging facilities. \n\nDon't blame us. The existing support was developed by a Russian developer who was later sponsored to develop for a commercial editing package. The free package they support is consistently behind the current releases, though I was cheerfully informed I could buy the rights to distribute the newest version. That was comforting. Meanwhile PHP 3 had built in debugging support and they dropped it for PHP 4. There also hasn't been much in the way of debuggers available for PHP 4 which is why Linus McCabe started Gubed. Integrating support became an issue along with continuing development. Mathieu Kooiman suggested we look at Xdebug, which I was unaware of. Xdebug provides profiling as an extra benefit and both have their own advantages. Mathieu began a debugger agnostic debug interface for Quanta and we discussed the deep integration. Meanwhile Linus got involved with it. Had they both not been really busy like the rest of us it might have shipped with the initial BE release. I don't want to give a date, but I hope we have it available before you know it. These options combined will give you extensive features and the ability to debug even on sites with limited privileges.\n\nVery soon Quanta will have the ability to easily add a debugger by configuring it's specific data to couple with our debug interface. This will allow for more than just PHP but for extending other scripting languages with debugging. \n\n> Or has anyone meanwhile succeeded in getting that php debug stuff in quanta to work? Whithout useable support of a debugger quanta is a nice thing, good look and feel, but it's not suitable for real programming tasks.\n\nSo why did the PHP team drop support for debugging in PHP 4 and why are there so few debuggers out there? We just haven't gotten to our extensive list including some custom form debugging tools too. As far as we're concerned we're ready to drop support for that debugger as it has required rebuilding PHP and it needs ongoing support from people who are looking to charge us money and could easily view Quanta as competition to their product. \n\n> Once again: thanks for quanta, a good peace of work.\n\nHang in there. It just takes time. We only have maybe half a dozen guys actively coding any given time.\n "
    author: "Eric Laffoon"
  - subject: "3.2 be vs 3.1.93"
    date: 2003-12-01
    body: "exactly how different is this from the 3.1.93 version?\n\njgraham\n"
    author: "jgraham"
  - subject: "Re: 3.2 be vs 3.1.93"
    date: 2003-12-01
    body: "The biggest difference between BE and the upcoming Beta2 is in the parser (and from there it comes a difference between the VPL), some new Kommander functionality and some extra bugfixes that can't be done in Beta2 due the above issues. The parser is about 2 times faster than in Beta2 when it comes to scripts and if it shows to be stable it will be merged to the official 3.2 in order to fix the most annoying bug in Quanta (#63000). Once KDE & Quanta 3.2 is released the BE releases will be the \"stable development\" releases of CVS HEAD. This way users without knowledge or time to regularly update from CVS will get from time to time the latest and greatest of Quanta. ;-) And unlike the CVS versions which may be broken at some point, we try to make the BE releases stable. BE releases will be English only.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: 3.2 be vs 3.1.93"
    date: 2003-12-01
    body: "> exactly how different is this from the 3.1.93 version?\n\nI don't want to be too exact and reference specific commits. ;-) So in general it is very close. This is built from the quanta_be branch. It is our development branch. Currently much of what has been put in the release has been ported to HEAD so it's synced. There are small differences from HEAD and more from beta1. The parser has been reworked and sped up. A number of fixes for Kafka arrived first and resided there until we could sync the parser. IIRC there are but a few UI differences but I don't remember specifically what. Nothing huge yet. The script tab has some updates it needs I have not done yet so the functionality I hacked as a short term release for viewing the information there has this looking very functional which it is not in HEAD or the quanta_be branch.\n\nOne big point here is that many people don't want to run CVS but will download and build an application. Also many distros will end up with builds either internally or contributed. A lot of people don't realize that we have maintained backwards compatibility with KDE 3.1x in CVS too. This is a way to bring these changes to a great many more people. If you're running CVS it will be very similar. Some time before the release of KDE 3.2 I hope to release another BE with a fair number of changes. We set the date for this release and had a lot of changes... Unfortunately a lot of what would have been different from 3.1.93 just wasn't ready. We decided to go anyway considering everything in balance."
    author: "Eric Laffoon"
  - subject: "Need more concentration on wysiwyg and..."
    date: 2003-12-01
    body: "...modperl..."
    author: "anonymous gerbil"
  - subject: "Re: Need more concentration on wysiwyg and..."
    date: 2003-12-01
    body: "> modperl\n\nQuite! Not all of us are at all enamoured with PHP. Integration with one or more of the perl templating systems would be highly appreciated. I'd be happy to help, if I can (on the perl side rather than the C++ side :-)"
    author: "Nick Mailer"
  - subject: "Re: Need more concentration on wysiwyg and..."
    date: 2003-12-02
    body: "Well, Nick, \n\nGlad to hear that you'd be happy to help.  Quanta+ can always use more developers...\n\nGet on the mailing list, by emailing the developers: \n\nEric Laffoon <sequitur@kde.org>\n\nand request to get on this list:\n\nQuanta users list <quanta@mail.kde.org>\n\nOr do it properly: \n\nhttp://quanta.sourceforge.net/main1.php?actfile=develop\n\nhttps://mail.kde.org/mailman/listinfo/quanta\n\nBut you should check the list archives (if it is available) for your ideas (with help of course).  And take a look at what is being done/planned prior to offering ideas, as things have gotten a bit hot with someone giving a wish list and not knowing that it was already done/couldn't be done/wouldn't be done.  \n\nBut modPerl is definitely something to be asking about, especially if you can help (they can use help just for testing cvs releases).\n\nwysiwyg is being worked on, just not as fast as some would like, and may not work like dreamweaver from what I'm seeing."
    author: "anon"
  - subject: "Re: Need more concentration on wysiwyg and..."
    date: 2003-12-02
    body: "Well put about helping out, except that we have a developer list too which is where someone helping would be. I have had someone express interest in doing some perl work, but it's tough keeping up with who said what when you have to search for who didn't follow up. Perl support can be added by XML. I don't care what you like, if somebody can't put in a few hours of XML I can't have everybody stop everything and spend the next several months writing support for numerous languages and dialects they don't use. That's why we made it so that people who don't know C++ can extend it. If you were paying for Quanta we could just hire someone to do it, but this is community developed software. So one person from the community using a language needs to step up. We had about 5 people volunteer for ColdFusion and we have CF support now. Some ColdFusion developers \"get it\" and now they have support. Why not perl developers? Open invite guys. You can do a little XML to get a free tool that does what you want, right? I've also had half a dozen requests for JSP. I've asked each of them to contribute a few hours. Guess how many were willing to do that?\n\nAs far as VPL being like Dreamweaver or how fast it's going... it's never fast enough, but as I said elsewhere it'sd difficult to say where the convergence of several factors will put it at release. Most likely it will not be as feature rich at 3.2, or at least not as we would want. I don't use Dreamweaver or why would I build Quanta. As I also mentioned here, it will be several months into 2004 that it begins to mature and become more enjoyable and powerful. However here are two things it will do right now that I think are worth noting...\n1) It will write DTD compliant markup\n2) It will only make changes on the DOM node you edit when you open a document and it will not shuffle the thing without your permission.\n\nThe BE release line will give us the ability to bring in improvements to VPL in a timely fashion. Minor point releases to 3.2 will have string freeze limitations on the user interface so that will limit what we can do there."
    author: "Eric Laffoon"
  - subject: "Re: Need more concentration on wysiwyg and..."
    date: 2003-12-02
    body: "I didn't mention the developer list as I didn't want to overload the post and did want him to get in contact with you easily instead of having to do the legwork himself (and risk the posibility that another project may distract him along the way ;o)\n\nI was sure that you would put him on the developer list if the interest was there on your part.\n\nAwaiting wysiwyg with bated breath.\n\nThe BE release sounds like you're taking on too much/spreading development thin.  You don't have enough people testing (or developers) prior to BE coming out (as posted on the list), yet it looks like you want to release faster than kde releases...so now there are two versions? Or are there two versions?  It is more than stable and cvs, right?\n\nThat's just my observation, which from the outside is quite possibly wrong. \nBut you are severely underestimating wysiwyg.  While the candidates who are more likely to use wysiwyg won't bring you more developers, it will bring more observers, fans, and buzz. And press.  And this will very likely bring more developers. \n\nJust another observation.  "
    author: "anon"
  - subject: "Re: Need more concentration on wysiwyg and..."
    date: 2003-12-02
    body: "> The BE release sounds like you're taking on too much/spreading development thin. You don't have enough people testing (or developers) prior to BE coming out (as posted on the list), \n\nOkay, by this rational we've always been too thin and are thicker now than ever. ;-) We have more people developing on more things, more people per feature and substantially more people testing. Many people on our user list are running CVS and helping us a great deal by finding bugs, for which I am very thankful. The point I make where you might get the idea we are thin is how much we do with so few people. We should have more developers in a perfect world, but it isn't perfect. Because we've always had to deal with this we decided on a tactical approach that would make Quanta user extensible, but so many people are used to a \"consumer\" mentality that the community aspect has not taken hold yet on over 99% of users WRT Quanta and testing, developing or donating.\n\nOur approach is that any substantial area of Quanta becomes a sub-project and gets it's own leader. Chris does XML and shares Docs with Fab, Nicolas does VPL, Marc does Kommander, Luciano does CSS and other people are doing various tasks that could move about. Andras does the parser and main project as well as fill in on whatever else needs done and so far, just directing and interfacing along with everything else, I do a very limited amount of coding. I plan to take a couple weeks \"coding vacation\" later this month to get more proficient prior to taking on some larger design projects.\n\n> yet it looks like you want to release faster than kde releases...so now there are two versions? Or are there two versions? It is more than stable and cvs, right?\n\nBE is a development version. This means a BE release will be a branch off of HEAD prior to KDE 3.2 final and from HEAD after. In all cases it will be a snapshot where we have taken several weeks of testing and withheld destabilizing commits so that it should be pretty stable and usable. It will have more functionality than prior releases but some features may be incomplete. It will be close to release stability but with some obvious bugs that should not interfere with most people's use.\n\nAs I said in our story, the decision to begin an external release schedule had to do with the rate which various tasks were reaching maturity. Our release schedule for some features is a year, but they don't start always with KDE's schedule. Other features are 1-3 months. Quanta has become a large complex application and note that Kdevelop and Koffice have not been able to make KDE release schedules. We're proud to be a part of the official release package, but a lot of what we wanted to get into 3.2 will be ready first quarter of 2004. No matter what anyone says statistical probabilities place 3.3 a year out. Going to a dual release schedule seems imperative. We are also seeing a good number of new developer interest emails and people learning Qt/KDE programming wanting to be part of our project. We hope they all become active developers and will do all we can to help them. So it's all good.\n\n> But you are severely underestimating wysiwyg. While the candidates who are more likely to use wysiwyg won't bring you more developers, it will bring more observers, fans, and buzz. And press. And this will very likely bring more developers. \n \nWell I don't know who you are, but it appears you're operating at a disadvantage of not having discussed my thoughts on the matter with me. ;-) Once we have reached a level that I believe we are a serious contender for professional development you can be sure that I will be promoting Quanta on KDE to the mainstream press, if they don't get to me first. BTW visual design is actually only one of three critical components that will cause this buzz. ;-)"
    author: "Eric Laffoon"
  - subject: "Click and modify?"
    date: 2003-12-01
    body: "Looking at the last screenshot of the \"Quanta BE shots\" images...\nIs it possible to right-click(or something) in the web-page\nand change form parameters? "
    author: "OI"
  - subject: "Re: Click and modify?"
    date: 2003-12-02
    body: "> Is it possible to right-click(or something) in the web-page and change form parameters?\n\nAs an old OS/2 guy I want to right click everything. ;-) That is not up but will be. I'm not sure if Nicolas has the required strings reserved for that or it just goes into BE. What we do have is the attribute editor which you can see in the lower left of the screen shots. It allows you to edit the parameters of a parent tag from where the cursor is. You can also select up the parent nodes to the root node with the dropdown box. There are also buttons on it to delete a tag or delete a tag and all child tags.\n\nObviously this is very powerful in abilities but for some tagging situations more intuitive paradigms would be desired. Right now we're working on first steps first. There are some architectural considerations to refining the process and design we need to review. After the basic foundation is in place the extra functionality should actually be a lot easier and faster."
    author: "Eric Laffoon"
  - subject: "Re: Click and modify?"
    date: 2003-12-04
    body: "Well, I think, :-), you answered my question.\n\nIt is actually possible to change the web-page without\nchanging the tags/code directly.\n\nGreat. :-D"
    author: "OI"
  - subject: "VPL and CSS"
    date: 2003-12-02
    body: "I like the VPL and the fact that I can see both the VPL and source at the same time. But it seems like VPL doesn't display my CSS background color and style although the mozilla editor does. how come?\n"
    author: "Pat"
  - subject: "Re: VPL and CSS"
    date: 2003-12-02
    body: "I'm not sure. It's really just reaching a level of adequate usability for heavy testing. I just checked the origin of background colors on a test page and it is displaying where a table data is using a class=\"\" CSS call that sets background color.\n\nTry switching to the error reporter tab below which will tell you to switch to the structure tree to generate the error reports. You might find bad markup confusing it. VPL really wants to see good markup. If you find that the page is good and it's just not working right you could either file a bug or join our user mailing list and post it there."
    author: "Eric Laffoon"
---
The Quanta team is <a href="http://quanta.sourceforge.net/main2.php?newsfile=qberelease01">pleased to announce the Bleeding Edge (BE) branch</a> of <a href="http://quanta.sourceforge.net/">Quanta Plus</a>.  <a href="http://sourceforge.net/project/shownotes.php?release_id=200659">Quanta 3.2 BE 1</a> features a number of new improvements including an <i>"awesome new CSS editor"</i>, <A href="http://kfilereplace.sourceforge.net/">KFileReplace</a> support, auto save and crash recovery, <a href="http://quanta.sourceforge.net/main2.php?snapfile=snap03">and much more</a>.  Note that VPL (WYSIWYG) mode requires kdelibs CVS or the upcoming beta.  If you are looking for something to give thanks for this season, or you just like <A href="http://quanta.sourceforge.net/main2.php?snapfile=snap01">pretty screenshots</a>, the Quanta Plus project is a worthy pick.  The Quanta Plus project is <A href="http://quanta.sourceforge.net/main1.php?actfile=donate">supported by your donations</a>. 
<!--break-->
