---
title: "KDE 3.1.1: It's Not Odd at All!"
date:    2003-03-20
authors:
  - "dmueller"
slug:    kde-311-its-not-odd-all
comments:
  - subject: "first thank you kde developers post! [nt]"
    date: 2003-03-20
    body: "KDE 3.11 for workgroups is finally here! (wish it was released on 3-11 though :))"
    author: "orooro"
  - subject: "Re: first thank you kde developers post! [nt]"
    date: 2003-03-20
    body: "mod +1 funny! :-)"
    author: "Andr\u00e9 Somers"
  - subject: "Re: first thank you kde developers post! [nt]"
    date: 2003-03-20
    body: "KDE 3.1.1 ( for Workgroups :P ) and in overall less time than another commercial shell environment. Additionally with a hell-of-a lot more features too :)"
    author: "Gary Greene"
  - subject: "Re: first thank you kde developers post! [nt]"
    date: 2003-03-20
    body: "Isn't this the one that breaks OS/2?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: first thank you kde developers post! [nt]"
    date: 2003-03-21
    body: "I think IBM did that already.  :)"
    author: "Brad"
  - subject: "Re: first thank you kde developers post! [nt]"
    date: 2003-03-21
    body: "I thought even XP could run OS/2 binaries.  I know w2k and NT can...\n"
    author: "Xanadu"
  - subject: "Suse RPMs only for 8.1"
    date: 2003-03-20
    body: "This is the first release of KDE that I can remember where there has only been Suse RPMs for the latest version. Normally there are packages for about the last three versions. \n\nI'd like to think that they will be along shortly, but packages for XFree 4.3 were only released for 8.1, so it's worrying trend."
    author: "Jason"
  - subject: "Re: Suse RPMs only for 8.1"
    date: 2003-03-20
    body: "I heard that SuSE RPMs for older distributions will arrive at a\nlater time. "
    author: "Dirk Mueller"
  - subject: "Re: Suse RPMs only for 8.1"
    date: 2003-03-20
    body: "Well, they are readying 8.2, due out next month.  I'm running 8.0, and it's starting to show it's age. I can compile from source, but I'm probably going to jump to 8.2 for convenience sake.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "shameless plug"
    date: 2003-03-20
    body: "<a href=\"http://quanta.sourceforge.net/\">Quanta Plus</a> has undergone quite a few fixes as well (it's included in this release, in case you hadn't noticed).  If you like Quanta, <a href=\"http://quanta.sourceforge.net/main1.php?actfile=donate\">help them out</a> cause they're looking for donations to help pay the developers."
    author: "Navindra Umanee"
  - subject: "Re: shameless plug"
    date: 2003-03-20
    body: "Thanks for mentioning it. :-) Maybe we have the longest changelog entry, as I always write down the important changes made to Quanta, and it's easy to provide it to the public.\nFor those running KDE 3.0.x: Quanta Plus 3.1.1 will be released also for you on our site.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: shameless plug"
    date: 2003-03-22
    body: "Yes! And most notably: Syntax colouring does work well now,\nno randomly coloured characters anymore. Thanks for the\nnice work. Finally, Quanta has become usable for me and \nmy co-workers and we have switched over from Kate. Still on my wishlist:\n\n- Bugfree saving of the tree views' docking state.\n- Separate syntax colouring for php opening and closing tags\n- A file / folder browser which can be opened / closed again by pressing a key\n  (like in Homesite) and _separate_ folders and files like in Konq.\n- better syntax-colouring support for mixed documents (HTML + PHP + JavaScript)\n\nWell, perhaps in the next release... ;-)\n \n"
    author: "Martin"
  - subject: "Re: shameless plug"
    date: 2003-03-24
    body: "Yes Martin, based on what's been posted on the quanta devel list, this is coming next release (the better syntax coloring)."
    author: "Jeff"
  - subject: "Thank you all"
    date: 2003-03-20
    body: "Thanks everyone for this nice piece of Desktop Environment. There's nothing similar. Good Job and Thumbs up."
    author: "oGALAXYo"
  - subject: "Re: Thank you all"
    date: 2003-03-20
    body: "Ah, so its a KDE week for oGalaxyo"
    author: "oGoWatch"
  - subject: "Re: Thank you all"
    date: 2003-03-20
    body: ".. maybe you should go out with friends (if you have some) for a while. I was only thanking the KDE team for a great Desktop Environment. There is no need for your unwanted sarcasm."
    author: "oGALAXYo"
  - subject: "Re: Thank you all"
    date: 2003-03-20
    body: "aaawww little kitten want some tickle ;D"
    author: "oGoWatchers"
  - subject: "Re: Thank you all"
    date: 2003-03-21
    body: "It wasn't sarcastic"
    author: "oGoWatch"
  - subject: "Gentoo"
    date: 2003-03-20
    body: "> Those of you who wish to compile from source can use Konstruct for near automatic compilation.\n\nOr use Gentoo - ebuilds for 3.1.1 were in and marked stable before the announcement was even made. I'm running 3.1.1 already. :-)"
    author: "Dr_LHA"
  - subject: "Re: Gentoo"
    date: 2003-03-20
    body: "Yes. I noticed that too. They haven't mirrored though and ftp.kde.org got overloaded as soon as the announce on the dot. Now I have kdelibs-3.1.1 merged and the rest of kde-3.1. (its working fine though) Oh well. Will have to try after a few days(??). Meanwhile a mix of kdelibs-3.1.1 and kde 3.1 should work (I hope!)"
    author: "Rithvik"
  - subject: "Re: Gentoo"
    date: 2003-03-21
    body: "Did you use the mirrorselect tool?  I have and I'm getting 150k downloads right now."
    author: "Joshua Moore-Oliva"
  - subject: "Re: Gentoo"
    date: 2003-03-23
    body: "I guess you misunderstood what I was saying, but anyway, thanks for mentioning the mirrorselect tool. I tried it and it is good (though it went back to the defaults anyway)\nThe latest kde is mirrored now, but it wasn't back then.\nPosted from konqueror 3.1.1."
    author: "Rithvik"
  - subject: "Re: Gentoo"
    date: 2003-03-20
    body: "so am i and i use debian so kde 3.1.1 must be old hat."
    author: "Caoilte"
  - subject: "Re: Gentoo"
    date: 2003-03-21
    body: "> Or use Gentoo - ebuilds for 3.1.1 were in and marked stable before the announcement\n\nEven so, I haven't been able to get svga-libs to compile for a month or so now.  I can't (at this point) do almost any updating to my boxes.\n\n:-\\\n"
    author: "Xanadu"
  - subject: "Re: Gentoo"
    date: 2003-03-21
    body: "And that's a good thing... how?\n\n"
    author: "Sad Eagle"
  - subject: "Re: Gentoo"
    date: 2003-03-25
    body: "Or use FreeBSD, KDE 3.1.1 packages are available at rabarber.fruitsalad.org :)\n\nAnd yes, I'm running kde 3.1.1 too"
    author: "coolvibe"
  - subject: "konqi startup speed"
    date: 2003-03-20
    body: "from the changelog:\n\n>konqueror:\n>    * Various startup performance improvements\n\ncan anyone, who actually tested the release confirm this? \n"
    author: "tschortsch"
  - subject: "Re: konqi startup speed"
    date: 2003-03-20
    body: "I'm using Debian Woody with the KDE 3.1.1 packages. Konqi loads directories a LOT faster before.\nI.e, takes just a few seconds on loading a 521 folders directory. Don't seems to be a better start-up time loading.\n\n--{@ Alexander."
    author: "Alexander P\u00e9rez"
  - subject: "Re: konqi startup speed"
    date: 2003-03-20
    body: "shhh. careful you will spoil Debian's image of \nalways having out of date software.\nIf too many people find out that this isn't true\nthe mirrors might get too busy."
    author: "kannister"
  - subject: "Re: konqi startup speed"
    date: 2003-03-21
    body: "I can.  I've noticed more than just konqueror loading faster.  For example, View -> Document Source opens up KEdit significantly quicker than 3.1 did.\n\nJust for the record, I compiled the source with --enable-final, and that was the only configure option I used."
    author: "Brad"
  - subject: "Re: konqi startup speed"
    date: 2003-03-21
    body: "You really should use at least --disable-debug (and even --enable-fast-malloc=full when compiling kdelibs.) --enable-final really doesn't do much for runtime speed, only makes it compile faster on machines with much ram.\n\nI've been following KDE from CVS, so --enable-final is the one I'm <i>not</i> using."
    author: "Vic"
  - subject: "Re: konqi startup speed"
    date: 2003-03-22
    body: "Oh right.  Thanks for that - I didn't know about that one.\n\nI'll try a rebuild tonight and see how it goes.\n\nThx again.\n\nBye"
    author: "Brad"
  - subject: "Re: konqi startup speed"
    date: 2003-03-22
    body: "I tested arklinux 1 alpha 7 which uses a very close cvs version of 3.1.1. It also uses XFree4.3 and prelinking, while I have used neither before. konq speed was the first thing I noticed, it feels like you click and it has already opened. It is soo fast. Part of that must be prelinking though. I don't know if xfree 4.3 is any faster than 4.2.1.\n\nOn a related note, don't use ark just yet."
    author: "goo.."
  - subject: "Not faster ...."
    date: 2003-03-22
    body: "if konqueror is already running opening a new window is reasonable.\n\n\nRendering some web page somewhere might be marginally faster but who cares?\n\n\nNo matter how many browsers and office apps linux/bsd/unix ends up with it, will never have a usable GUI file manager.  It was always thus and always will be ....\n\nPeople who have to use unix desktops use xterms and bash as their file managers ....\n\nPeople who don't have to use unix desktops don't use them ...."
    author: "Sad Truth Teller"
  - subject: "Re: Not faster ...."
    date: 2003-03-24
    body: "\"People who have to use unix desktops use xterms and bash as their file managers ....\"\n\nVery amusing, you really don't know what you are talking about, so I'll make it short. \nKonqueror is all in all the best browser I ever used. For work I use Linux, and my file management is sometimes from the command line and sometimes Konqui, dependeing on what's faster for what I need. At home we only use Linux, we use it for everything, and we couldn't be happier Speak for yourself next time ...\n \n"
    author: "NewMandrakeUser"
  - subject: "Re: Not faster ...."
    date: 2003-03-24
    body: "well, if you don't like Konq, then you should try Rox. I've heard plenty of people rave about it."
    author: "Matt"
  - subject: "wow: redhat 8.0 packages ? "
    date: 2003-03-20
    body: "I must have eaten something wrong. Downloading right now.\nI was messing with debian because of rh attitude, hmm  maybe nxt time\n.\nwow. 8.0 packages. still receiving\nwow\n\n\nwow\n\n(i still can't believe this)\n"
    author: "lintjens"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-21
    body: "Debian also has packages for 3.1.1."
    author: "jeke"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-21
    body: "Yeah, wow.  I think you can thank Than Ngo from Red Hat Europe for that.\n\nNot sure though.  I'm only did an rpm2header on kdeedu-devel-3.1.1-0.8x.1.i386.rpm"
    author: "Navindra Umanee"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-22
    body: "and it rocks. Looks very stable, fast. Very nice.\nkuddo's to kdepim people. I can now finaly share my addresbook with my wifes.\nBeen working a day with 3.1.1, and it is as solid as 3.0. I can say that I am very impressed\nwith svg icons and konq tabbed browsing (althoug i miss mozilla's X button next to the tabs)\n\nkde rocks (and yes, I've been progressing with debian, actually got 3.1.1 working on debian this afternoon! Hope to convert rather soon, since debian seems to be closer to the kde releases and apt-get install simply rocks)\n\nAnd hey people, check this out: Konsole is actually working this time (what, rh didn't want to disable kde with this release ?) No more ugly 20x Xterm \n\nDaniel\n(for the first time in 5 month's a happy rh 8.0 user)"
    author: "lintjens"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-23
    body: "\"althoug i miss mozilla's X button next to the tabs\"\n\nYou can add a \"close this tab\" button to your toolbar to get equivalent functionality. Sure, it's not in the same spot (which is more off-putting than you would think), but it works."
    author: "Joeri Sebrechts"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-23
    body: "yeah, not on the same spot but good enough for me. tnx for the tip!\n\nDaniel\n"
    author: "lintjens"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-23
    body: "I was stunned as well.  A few days ago I was debating whether or not to move away from RH due to their marginal support of KDE - and now here they go rolling binaries right out the gate with the release of KDE 3.1.1. \n\nI hope this is going to become a pattern for Red Hat.  I have nothing against Gnome, but Red Hat is a large and popular distribution and their support of KDE only helps their position and standing with users plus the community in general.\n\n"
    author: "David Bakody"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-23
    body: "Chances are, it's one guy doing it on his own in his personal time."
    author: "KDE User"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-23
    body: "I hope that is not the case, but I suspect you are right.  It's probably some lone gunman at Red Hat that rolled these and then got called onto the carpet by a PHB warning him not to ever try that again.\n\nOn the other hand, maybe Red Hat really is just playing nice and gave some time to an employee to roll binaries for everyone.\n\nYou never know.\n\nI'm sure we'll eventually find out."
    author: "David Bakody"
  - subject: "Re: wow: redhat 8.0 packages ? "
    date: 2003-03-24
    body: "Rex Deiter at University of Nebraska at Lincoln has a sourceforge project for kde and rh8, at:\n\nhttp://sourceforge.net/projects/kde-redhat/ \n\nand has had a public apt repository up for about 3 weeks now. "
    author: "hcmeyer"
  - subject: "Cool !"
    date: 2003-03-20
    body: "   Thank you so much for such a wonderful environment. The number one problem about it is that going back to anything else is painful. OS X ? much too limited. XP ? hurts my eyes, and the filemanager sucks at ftp (not to mention no ssh (fish://)).\n\n   As I am (nearly) an engineer, I write all of my reports with LaTeX. I was wondering if one day Kword would in fact be able to replace it. And I think that the new improved equation editor -- capable of actually _performing_ calculations -- is making it advance towards that goal. \n\n   My questions (very humble) are :\n\n-- will there be at some point \"typesetters\" on the kde-level capable of TeX-quality output ? I am thinking, even for monospace type fonts for the mails. \n\n-- Cross-reference/index-generation for Kword ?\n\n   I know those points are not directly related to the article -- but by my book, though I have no doubt that KDE can become even better (small (or big) usability inprovement here and there, eye-candy, even more IOSlaves, perfect translations, focus-follows-mind :)) but as it is it is for me perfect. Seriously.\n\n   Perhaps the defaults could be slightly altered (see screenshot), but what, this is a stricly selfish point :)\n\nAgain, thank you !\n\nPS (the TeX fonts have been transformed into TT fonts\n http://sourceforge.net/project/showfiles.php?group_id=41605 \n\nthey could be included with KDE :)\n\n"
    author: "hmmm"
  - subject: "Re: Cool !"
    date: 2003-03-20
    body: "What's wrong with LaTeX though -- if you're adept at LaTeX there's no reason to throw it away.  :)\n\nI don't think KWord is targeting the same crowd as LaTeX does, though I could be wrong.  There's always the KLyX option...\n\nNice screenshot, btw."
    author: "Navindra Umanee"
  - subject: "Re: Cool !"
    date: 2003-03-20
    body: "   There certainly is nothing wrong with LaTeX. LaTeX is the Tao. LaTeX is Beauty, and therefore it is Truth (sory Keats :)) \n\n   But something must be said about a program wich can read MS-Word docs and make them into something that doesn't hurt the eyes :) Also, although I'll probably keep writing (La)TeX without anything else than an editor (Another fantastic feature of KDE, it can use the TeX-ispell dict ! Yay kate ! ), It would be easier to convince my partners to use kword than LaTeX... (though I have made many adepts in my class :) -- it is my own little contribution to dent the hegemony of MS-Word)\n\n   In fact I believe Koffice is the single most important subproject of KDE -- not because it is the most useful to me -- it isn't, by far -- but because getting rid of the evil that is MS-word is of paramount importance for the survival of humanity :)\n\n   You think I jest ? \n\n   A civilisation which keeps its archives, its knowledge and History in a format controled by a single, close entity, not interested in anything else than profit (which is fine by me, note -- they are a company driving for profit, after all) deserves to decade, decay and die. \n\n   Yes, I love books made out of paper (proper books, sewn, not glued, made with paper capable of withstanding Time). \n\n   Sorry for the rant. So anyway, when is Koffice 1.3 due ? ;)"
    author: "hmmm"
  - subject: "Re: Cool !"
    date: 2003-03-21
    body: "You're right about the problem of hegemonic closed file formats, and in fact it's a problem bigger than MS-Word itself. Have a read of http://www.tomchance.uklinux.net/articles/darkages.shtml for a scary look at what could become of our culture and history, should open formats not be promoted, and laws be changed to allow the cracking of redundant closed formats.\n\nWhat we really need is for the KOffice team to get together with AbiWord, OpenOffice and the other big FS players and agree on a set of open, XML-based formats, so they're standard between all office suites, and then the pressure will build on MS to get least use them, even if they still tack on pointless proprietary extensions."
    author: "Tom"
  - subject: "Re: Cool !"
    date: 2003-03-21
    body: "TeXmacs,  http://www.texmacs.org/  is interesting for people who want to do latex type stuff in a WYSIWYG type way.\n"
    author: "josh_stern"
  - subject: "Re: Cool !"
    date: 2003-03-23
    body: "Still I would love to use Lyx to typeset my document and then tweak it by export to kword. I have faced some minor cosmetic problems in a report I had typed in LyX. I did not know how to correct the problems without tweaking the latex code (which I'm not familiar with). I simply love the consistency of output from lyx/latex and the speed and automation, but minor problems do crop up. \n\nLyx 1.3.1 is out now and the website says it has an improved Qt frontend. I wish they had an option to export to kword. Best of both. Maybe kword will not have cross-references/indexing etc features (and document consistency, most importantly) which lyx or latex excel in. But it certainly does better in the WYSIWYG dept (minor, per page, adjusting problems and annoyances) and for quickly writing a small letter etc.\n\nI would like to know if kword can import Lyx documents. Heard of a PDF import filter for kword recently, and maybe a latex import?"
    author: "Rithvik"
  - subject: "Re: Cool !"
    date: 2003-03-23
    body: "> and for quickly writing a small letter etc.\n\nI cannot believe anything want any WYSIWYG editor after ever trying\nletter/scrltr style for LaTeX (all of them have LyX layout files\navailable).\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: Cool !"
    date: 2003-03-24
    body: "Haven't tried it yet. Thanks for the suggestion.\n\nI still feel small documents could be more easily handled by kword though. A one page letter need not be sent through the latex typesetting system,( previewed through the dvi viewer ) and then printed. It could be better done directly on a WYSIWYG processor. Same goes for presentations, where look is everything. \nWhere consistency counts more than mere looks, like a professional document (thesis, journal or legal document), LyX and latex are the way to go. (Formal letters do come under this catagory, but they are small enough to be handled by WYSIWYG, unless they are too rigid in structure). \nThe DTP-like features of kword come in handy here too. So if I have that nice book typeset using lyx and want to give it a fancy cover with wordart and eye-candy, I could use kword. Kword should really concentrate on this.\nThe only part of the modern LyX-Qt that I don't like is integration with Koffice. Apart from the above mentioned things, I would have liked to embed kivio or karbon14 artwork directly in LyX instead of using xfig, export to eps-latex and then embed. That's why I'm looking forward for kLyX. \n<dream_on>Maybe someday they will develop KalyX (KLyX resurrected and integrated in KOffice) and a DTP-like wordprocessor Korolla (kword enhanced to work with former). Then we would have a real Whorl </dream_on> (hope u know yr botany;)"
    author: "Rithvik"
  - subject: "Re: Cool !"
    date: 2003-03-25
    body: "Could you please contact me via email? I would love to discuss with\nyou personally. Thanks.\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: Cool !"
    date: 2003-03-21
    body: "... focus-follows-mind ...\n\nThat's one technology that could get a lot of people into a lot of trouble."
    author: "AC"
  - subject: "Re: Cool !"
    date: 2003-03-21
    body: "Well there is lyx (1.0.3) and it even has a qt-frontend.\nThe only problem  have with programs such as scribus and lyx is that they don't use the kde-icons and the kde-file-dialog . And that makes it really imossible to push them unto Joe User, who doesn't understand anything but the standard kde-fd.\n\nDoes anybody know whether there are plans to get qt-apps to integrate closer with kde-apps?"
    author: "Johannes Wilm"
  - subject: "Re: Cool !"
    date: 2003-03-23
    body: "lyx 3.1.1 is ... (oops, beg pardon, carried away:) lyx 1.3.1 is out now."
    author: "Rithvik"
  - subject: "Re: Cool !"
    date: 2003-03-21
    body: "There is an excellent KDE-editor for TeX called kile (http://perso.club-internet.fr/pascal.brachet/kile/). We use it on a regular basis at our lab. \n\nAnd thanks for the font-link I am already installing ;-)\n\nGreetings,\neva"
    author: "Eva Brucherseifer"
  - subject: "Re: Cool !"
    date: 2003-03-21
    body: "   As nothing is perfect, I must add that those are unaccented fonts... Which annoys me to no end (that is, unless I am to write english). Thus, I am still looking for those very same fonts, but the European version.\n\n   I have not yet lost all hope, as there exists indeed the accented version (in PS type 1 format). But for reasons as yet undetermined kfontinst will not install them properly..."
    author: "hmmm"
  - subject: "Re: Cool !"
    date: 2003-04-28
    body: "If I'm reading this right, as of 27 April 2003, development of Kile has been stopped.  What the heck happened?  Kile looked like a great piece of software.  Burnout?"
    author: "Randall Wood"
  - subject: "Re: Cool !"
    date: 2003-04-28
    body: "If you ask the author and find out, let us know.\n\nhttp://perso.club-internet.fr/pascal.brachet/"
    author: "KDE User"
  - subject: "Re: Cool !"
    date: 2003-03-21
    body: "LyX (www.lyx.org) now has a QT-based frontend. It gives you the power of LaTeX but looks like a word processor so is easy to use. I used it for my PhD thesis and it was great!\n\nDan"
    author: "Dan"
  - subject: "NOOOOOO!!!!"
    date: 2003-03-21
    body: "I just finished installing 3.1 yesterday using Gentoo on a p2 300.  (3 day compile time, if anyone can find ways to shorten that email me secondsun72@hotmail.com)\n\nBut anyway cudos to KDE team.  It is the reason windows isn't my main OS. "
    author: "Secondsun"
  - subject: "Re: NOOOOOO!!!!"
    date: 2003-03-21
    body: "If you have a fast machine or machines at your disposal you can build binary packages for your packages.\n\n\nCFLAGS=\"...\" CXXFLAGS=\"${CFLAGS}\" emerge -b kde*\n\nemerge -k kde*\n\nor something like that."
    author: "Me"
  - subject: "Re: NOOOOOO!!!!"
    date: 2003-03-21
    body: "Use -B (capital B) to only build packages, and not merge.  -b builds a package and merges it.  It would be nice though for people with slower machines, if they could just download precompiled packages.  (I build them for my slower machines, but not everyone has that luxury of course.)"
    author: "Anonymous"
  - subject: "Re: NOOOOOO!!!!"
    date: 2003-03-21
    body: "Use Debian instead. I'm sure you can get it up & running in less than 3 days.\n"
    author: "Blue"
  - subject: "Re: NOOOOOO!!!!"
    date: 2003-03-21
    body: "I switched from Debian to Gentoo, and never looked back. People often say to Gentoo-users \"Maybe you should run Debian, then you don't have to compile so much\". But many run Gentoo BECAUSE of the compiling, not despite it. And of course, there's alot of other things in Gentoo that attarct users as well."
    author: "Janne"
  - subject: "Diffs"
    date: 2003-03-21
    body: "Thanks for providing diffs from 3.1 to 3.1.1 for the source tarballs!"
    author: "Anonymous"
  - subject: "Did you notice?"
    date: 2003-03-21
    body: "I read nowhere that 3.1.0 is overly plagued by bugs and there was no one asking daily here when 3.1.1 will be released although it was delayed compared to the release schedule. So KDE 3.1.0 must have been a good release. :-)"
    author: "Anonymous"
  - subject: "YES"
    date: 2003-03-21
    body: "the time spend in release candidate paid in the end\n\n3.1.0 is very stable and useable !\n\nthank"
    author: "yes"
  - subject: "smb-ioslave bugs"
    date: 2003-03-21
    body: "Hi,\n\nunfortunately it seems that my latest fixes to the smbro-ioslave (the smb ioslave which doesn't link to libsmbclient.so) didn't make it into 3.1.1.\nFor those of you who have problems with this ioslave: either update kdebase/kioslave/smbro/ from cvs (HEAD or KDE_3_1_BRANCH) or get it as a single package from http://www.neundorf.net/src/smbro-3.1.tar.bz2\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Konstruct rant"
    date: 2003-03-21
    body: "Konstruct is quite nice. Haven't used it before, but thought I'd use it to finally upgrade from KDE 3.1rc2 :)\n\nHowever, it seems many of the mirrors it tries to download from still haven't got KDE 3.1.1. I have had to restart it several times (fortunately, it doesn't start again from scratch each time!). Minor annoyance.\n\nWouldn't it be VERY cool to wrap Konstruct in a nice KDE gui (don't talk to me about chickens and eggs! ;) that not only runs Konstruct and visualizes it nicely (think progress bars), but also parses errors and tries to suggest which packages you lack? For instance, I got the error that some cdda_*.h files weren't found, and had to research which rpms I lacked. This GUI could even be integrated with apt (which I have for my RH8) and urpmi to download missing libraries automagically! It should be feasable to make a \"knowledge database\" which is a function from \"possible things that can go wrong\" to \"ways to solve it\" (most often installing a missing library package). In addition, the GUI would let you set all kinds of options in the gar.conf.mk file. Wouldn't this be a killer argument for installing KDE and keeping it updated? I know Nick Betcher were working on a nice GUI installer like this, but it's complex when you have to worry about the low-level details such as locating and downloading source, unpacking, starting the build system etc etc... Konstruct does all this now, the GUI only needs the high-level logic.\n\nI know I should put my money where my mouth is (that's a strange expression, I may have got it wrong), but I'm not a killer C++ God. If I get more free time, this may be the excuse I need to learn C++ more actively, perhaps :)"
    author: "Haakon Nilsen"
  - subject: "[OT] Re: Konstruct rant"
    date: 2003-03-21
    body: "> I know I should put my money where my mouth is...\nNo, you got it right :)  "
    author: "OUSpirit"
  - subject: "Re: Konstruct rant"
    date: 2003-03-21
    body: "In a perfect world Konstruct should do the following.\n\ncd konstrunct\n./configure\nmake\nmake install\n\nkonstruct\n\nKonstruct should be able to compile w/o any dependencies and get ALL the KDE dependencies right and compile KDE for you. All with a nice GUI once these libs are available of course.\n\nIn a less then perfect world a nice graphical tool that will download and install it all for you would still be very pleasing.\n\nSome more cents of mine:\n- Easy interface for other projects to configure konstruct so that developers can provide an easy way for people to get progs running without going through RPM Hell or provide a dozen or so rpms just for the major distros.\n\n- Mechanism for security updates with minimum use of band with and compile time (May be as in just get the diff patch the source and compile the relevant parts)\n\n\nCheers,\nThorsten"
    author: "Thorsten"
  - subject: "css and konq 3.1.1"
    date: 2003-03-21
    body: "just wondering if someone else got problems with css and konq 3.1.1? \n \n e.g. http://bytesex.org/xawtv/ looks reaaaaaaaaly strange... when changing css handling to user spezified everything looks ok .. \n \n rgds \n marc'O"
    author: "Marco Puszina"
  - subject: "KDE and Linux: not ready yet for the desktop"
    date: 2003-03-21
    body: "http://osnews.com/story.php?news_id=3064\nUntil all these issues are solved, KDE and Linux will remain labeled as \"unusable on the desktop\" by the mass media.\n\nDon't get too excited, there's still a lot of work to be done."
    author: "FooBar"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "Well, I know you use GNOME and that you agree on what's written on OSNews.com but those who use KDE actually belive that KDE is totally usable as desktop. The total success of KDE today is because it's the way as is NOW. I for my personal opinion don't like to see KDE suck down the way GNOME suck these days."
    author: "oGALAXYo"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "<I>Well, I know you use GNOME and that you agree on what's written on OSNews.com but those who use KDE actually belive that KDE is totally usable as desktop</I><BR>\nWhile it is easy to dismiss many authors, sometimes, it is better to examine closely what is said. Think about Linux and when MS showed that it had certain flaws. It was overcome and made to blaze. In this context, think about this author. Yes, some of what she says is crap and very subjective, but some may be valid and of great use."
    author: "WindBourne"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "Whether I use GNOME is irrelevant. GNOME itself is irrelevant. KDE is *not* ready yet. OSNews critisizes GNOME as being worse than KDE but that doesn't make KDE acceptable enough. KDE is bad, GNOME is worse than bad, but KDE is still bad."
    author: "FooBar"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "> GNOME is worse than bad, but KDE is still bad.\n\nHmm, people that i know of that have *really tried* (more than 10 minutes) KDE think it's the best desktop they've ever used. Therefore i have to say that i deeply disagree, it's very much usable as it is. As a matter of fact, i think that give it a little bit of additional commercial software, distribution agnostic easy (read: GUI based) software installation process and we're in the mainstream. Big time."
    author: "jmk"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "I like to agree with you. Saying that KDE is not ready, just to express the frustrations over GNOME is not nice. I personally think that KDE is pretty much ready as Desktop Environment. I tend to say that it's in some cases far superior over commercial alternatives."
    author: "oGALAXYo"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "Then you tell me why sites like OSNews.com or all those \"Linux reviews\" out there mark Linux and KDE down as \"not ready for the desktop\"!\nKDE may be the best desktop for you geeks, but not for Joe Average. OSNews.com and other sites have stated that over and over.\n\n\n\"Saying that KDE is not ready, just to express the frustrations over GNOME is not nice.\"\n\nThere you go again: you ignore constructive critism against KDE, and blames everything on GNOME. GNOME HAS GOT NOTHING TO DO WITH THIS! Stop your elitist attitude and start realizing that KDE IS NOT PERFECT FOR AVERAGE USERS!"
    author: "FooBar"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: ">  \"Linux reviews\" out there mark Linux and KDE down as \"not ready for the desktop\"!\n\nAs I see it, most of this comes from not-too-well productified linux distributions concerning desktop use. This is really not KDEs fault. Installation process is pretty good on most of the distributions by now, but 'every day usage' for joe average is painful. Usually this comes down to two things: installing new software is far more difficult than clicking setup.exe and administration tools are not a) easy enough b) robust enough c) coherent enough (similar metaphors between distributions are needed). YaST on the other hand is quite far, but there's always 'some little clitch' that makes Joe Average to give up on it.\n\nHmm, I wonder if it would be possible to create a generic application installer for KDE. Would be really nice."
    author: "jmk"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-09-15
    body: "Agreed. I've tried SuSE 8.2 with KDE for over a month now. The biggest issue that drove me back to Windows was the great difficulty in installing new/updated applications. Also little things--cut and paste not working across all applications, applications not rememembering their settings (e.g., size, location). Forget \"Joe Average\"--Linux isn't even ready for \"Joe who knows a fair amount about computers and is willing to give it a good try but doesn't want to make configuring his computer his only hobby.\""
    author: "kfw"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "Because these reviews are written by people who like to see another merged OS such as BeOS or QNX where the Kernel directly boots into an easy to use GUI, without issues of installing drivers and stuff like this. These people have seriously wrong visions and ideas what Linux is meant to be and should seriously use Windows or whatever."
    author: "oGALAXYo"
  - subject: "Re:\"The Definitive Desktop Environment Comparison\""
    date: 2003-03-22
    body: "As I stated in my LWN response to this story, many of the issues raised there can't be directly addressed with a multiplatform X environment, anyway, at least to any great degree, while keeping the multiplatform aspect.  Integration is a good example.  Sure, KDE can and does provide the Linux Kernel Config control panel applet, but that doesn't do much for those using KDE on *BSD, for instance.  There's a limit to how OS integrated it pays to get, when the object is to continue to be usable on all sorts of platforms.  The same thing goes, but even more so, for consistency.  KDE and all X environments continue to support all sorts of non-environment apps, which **WILL** bring consistency scores down, no ifs, ands, or buts, about it.\n\nThe one area where KDE excels, and the reason many users and MSWormOS switchers continue to use it today, is in configurability.  Yet, even tho Eugenia says KDE is the most configurable of all those evaluated, she only scores it an 8, because she both doesn't like having to deep-dive several layers deep for her config options, and doesn't like hundreds of options on the same level.  Well, when something as complex as KDE is as configurable as KDE unappologetically is, there WILL either be multiple layers of diving to do to get to the individual config option, or hundreds of options on the same level.  With that many config options, there's simply no way around it.  It WILL be one or the other, and she marks KDE down because of it, in the configurability area, even tho she admits it's miles more configurable than most of the competition.  <shrug>  What can you do when faced with an evaluator that obviously biased against that sort of configurability, when configurability is unappologetically a goal of your platform, as it is with KDE?\n\nThen again, get this:  Even where there isn't a direct GUI option to configure something, Eugenia actually speaks approvingly of sticking it in some obscure registry setting, editable only with specialized tools, rather than in a highly commented plain text file, editable by any text editor you want to use.  <shaking head>  The other at least makes some sort of sense, for those scared of config options.  This makes no sense at all.\n\nMany of us using KDE left MSWormOS in part because it was to constricting -- it didn't allow the sorts of deep config we wanted to do, or made us pay a price for it.  We are attacked to KDE for the same reason -- it's configurability.\n\n[E-mail address omitted due to the huge volume of spam I received the last time I posted it here -- I had to shut down that address and start with a fresh one.  The spammer address harvesters unfortunately LOVE this site, it would appear.]"
    author: "Duncan"
  - subject: "Try this on windows for \"consistency\""
    date: 2003-03-22
    body: "Excellent post, good points.\n\nnow everyone try this:\n\nInstall XP\nInstall Office 8.0 (you paid 400$ for it)\nInstall Photoshop\nInstall Mozilla\nInstall strange tcl/tk app mandated by company you work for\nInstall Java applications\nInstall DOS games\nInstall client apps (wtf is that written in) for corporate finance application\n\nNow if you can get all that to even *run* tell me if everything is totally consistent down too all the configuration panels and open/save dialogs etc.  Remember this is a company with 50BillionUSD ... why isn't is *perfect*?\n\nAnswer the question \"why\" ... Now apply answer to much more open environment of OSS/BSD/Linux/X etc etc\n\nConduct same tests on MacOX/S with carbon cocoa java etc.\n\nBTW as we all know it is possible to create a totally consistent GUI for Linux/BSD without X  ... in fact these exist already.  If this were the only stumbling block there'd be no more MS-Windows. BeOS (as the review notes) had a great consistent UI ... BeOS is now dead. Same goes for NeXT.  Windows3.1 had possibly the worst GUI ever conceived (esp. compared to OS2 at the time) but which won out?\n\nThe stumbling block is hardware vendors and software operating system monopolists. That's it. The rest is peanuts."
    author: "KDE Fanatic"
  - subject: "Re:\"The Definitive Desktop Environment Comparison\""
    date: 2007-03-16
    body: "\"Install XP\nInstall Office 8.0 (you paid 400$ for it)\nInstall Photoshop\nInstall Mozilla\nInstall strange tcl/tk app mandated by company you work for\nInstall Java applications\nInstall DOS games\nInstall client apps (wtf is that written in) for corporate finance application\"\n\nI have a CD thats does all this for me automatically. And its not running gimp software. Your point? Noob."
    author: "Trek"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-22
    body: "Huh?? Is that from some Microsoft employee??\nPretending to be unbiased but in fact full of prejudice\nand ignorance. I've switched from Windows to KDE about 7 months ago\nat home as well as in my office. I have never used or even seen Linux in action before.\nNow my experience:\n\nResponsiveness/Speed:\nWindows XP's responsiveness may be better than KDE's -\nwith a Pentium XXII with 200 THz - Or with Big Blue - Perhaps -\nI don't know, because I just can't afford such a machine.\nI was constantly complaing that my Home computer (1 GHz / 128 MB) was\nvery slow under Windows XP (fresh install). There was constant HD activity\nand starting applications like Word did take quite a while. Everyone was telling\nme: 128 MB is not enough! You need more. Instead I installed KDE and everything\nworks just fine. So - do I really need more? Doesn't matter anymore, KDE does\neverything what I need and I'm quite happy I don't hear that disk thrashing anymore\nwhich I thought was normal on a PC.\n\nStability:\nWindows XP is quite stable when you first install it.\nWith every application you install and uninstall it gets worse. \nCorrupted registry, OLE problems, you name it - I know it :-(\nAnd the worst thing: When you search on google you always find lots\nof persons with the same problem but nobody has a solution. And nobody\ncan find a solution because it's closed source.\nWith Linux I have installed and remove a lot of RPMs and my Linux/KDE machines are still as stable and fast as if they had been freshly installed.\n\nLooks:\nMy KDE with Noia icons and Liquid even amazes the Mac folks in my company\nwhich is quite an achievement ;-)\n\nIs it \"ready for the desktop\"?\nI think it's quite clear that KDE/Linux can \"host\" any application at least as\ngood as - or even better- than Windows. The foundation is definitvely there.\nAnd if you compare Windows XP to Windows 3.1 than Windows 3.1 \nwould have lost. But does that\nmean that Windows 3.1 has not been \"ready for the desktop\". The\nwhole \"ready\" thing is quite ridiculous IMO. The only thing that matters is:\nAre there enough applications for what you want to do?\nAn office which mainly wants to write letters can do so without any problem.\nOn a wider scale: What's really missing IMO is some kind of GIMP with a better\nUI. The lack of an web image-editor with a good integrated UI like Adobe ImageReady\nor Fireworks is what still lets many of my colleagues boot to Windows.\nBut forunately ImageReady runs quite well with the latest Wine version.\n\n"
    author: "Martin"
  - subject: "Re: KDE and Linux: not ready yet for the desktop"
    date: 2003-03-24
    body: "Don't feed the troll.\nAnd I believe your name is really spelled FUBAR"
    author: "joe average"
  - subject: "k3b build problems"
    date: 2003-03-22
    body: "Konstruct is still going here, but i had to remove k3b building from the Makefile as it *just wouldn't build*. I kept getting the error at http://www.lstud.ii.uib.no/~s1027/kde/konstruct-error.txt - it seems to me like a coding error. This is still kde3.1.1, and k3b 0.8.1."
    author: "Haakon Nilsen"
  - subject: "Re: k3b build problems"
    date: 2003-03-22
    body: "Remove \"--enable-final\" from apps/k3b/Makefile and you should be fine after a \"make buildclean\"."
    author: "binner"
  - subject: "Re: k3b build problems"
    date: 2003-03-22
    body: "Thanks, worked wonderfully. Maybe --enable-final shouldn't be there in the first place?"
    author: "Haakon Nilsen"
  - subject: "Konq's Tab Delay Bug"
    date: 2003-03-22
    body: "I see the darn Konqueror bug which causes a delay in opening tabs is still there. And all this downloading for naught. It's a valid bug in the bugs database (marked as \"unconfirmed\") but I couldn't be bothered to find the number again.\n\nBasically opening up a new tab takes 3+ seconds. At least on dialup. This necessitates me using mozilla and I'd love to just stick with Konq."
    author: "Kev"
  - subject: "Re: Konq's Tab Delay Bug"
    date: 2003-03-22
    body: ">  I see the darn Konqueror bug [..] but I couldn't be bothered to find the number again.\n \nIt's as easy as going to http://bugs.kde.org and clicking on the \"Most hated bugs\" link. If your most hated bug doesn't appear there, vote for it (you have at maximum 20 votes points per report and 100 per product) and if another user has voted for it too, it will appear magically in this list."
    author: "binner"
  - subject: "Re: Konq's Tab Delay Bug"
    date: 2003-03-22
    body: "\nHey thanks - I just went and gave it a full 20 since it's the only thing holding me back from using Konq full-time."
    author: "Kev"
  - subject: "Re: Konq's Tab Delay Bug"
    date: 2003-03-22
    body: "This is no bug but a shortcoming of QTabWidget. There is work done atm for a super-duper new tab widget for Qt 3.2/KDE 3.2 which will allow to create a tab before its widget. "
    author: "binner"
  - subject: "Re: Konq's Tab Delay Bug"
    date: 2003-03-22
    body: "Hmm, so the problem is that we have to wait for the widget until we create the tab around it? Why don't we use a wrapper widget, that will only get its child when it's available? ... KonqView does that btw, so I'm surprised. Or would this lead to geometry management problems? (in a tab widget, I doubt it...)\n"
    author: "David Faure"
  - subject: "Re: Konq's Tab Delay Bug"
    date: 2003-03-23
    body: "Thanks to everyone for taking a peek at this. The delay really makes the tabs effectively useless. 3 seconds is a conservative estimate. Sometimes it takes 15+ and the secondary problem being that one has to wait for the qeued tab to arrive before requesting another. Just sitting around... waiting...\n\nPS - thank you whoever started making xdeltas for the source tars. This really saves so much time. And while I can't see spending the time to get the Berkeley DB running for xdelta 2.x, versions 1.13 works really well. A big kudos to all.\nK"
    author: "Kev"
  - subject: "Re: Konq's Tab Delay Bug"
    date: 2003-03-23
    body: "\nNow it's been a long time since I've done Qt programming (1.1?) but I was just thinking. A Crtl-T (or whatever combo) creates the tab instantly. Why would it be so difficult to make the tab and then assign it afterwards? ISn't this just an ordering problem?"
    author: "Kev"
  - subject: "Konqi timeout bug"
    date: 2003-03-22
    body: "There is another bug in Konqueror easy to reproduce.\n\n- Say you open a page which you can't access for some time. After 10 seconds idle you decide to go to another page and you then start replying to some curious message written by someone.\n- Then after some minutes the timeout comes up and kills what you have written, you can't go back or forward anymore and all the stuff you wrote is removed.\n\nAnyone made similar experiences and could describe it more clearly than I was able to ?"
    author: "Report"
  - subject: "Good Work"
    date: 2003-03-23
    body: "Just want to say thank you to all the KDE developers. I'm running 3.1 under Slackwere and its great! Keep up the good work.\n\n\n\nLee"
    author: "Incubi"
  - subject: "Controversial name change"
    date: 2003-03-24
    body: "Check out bug 52205 ( http://bugs.kde.org/show_bug.cgi?id=52205 ) for some political controversy. I particularly like the fabulously self-contradictory Comment #6."
    author: "dave"
  - subject: "Kprint vs. GNUlpr"
    date: 2003-03-24
    body: "Are there any plans to make Kprint work with LPR?\n\nAnd, what happened to KLPQ?\n\nKJobViewer doesn't work with LPR.  Nothing happens!\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "KDE 3.1.1 problems with K3b 0.8.1"
    date: 2003-03-26
    body: "Hi there, I have a problem!\nSince i've installed KDE 3.1.1 on my SuSE 8.1 system, my K3b doesn't work any more. I've downloaded the latest release (0.8.1 release for KDE 3.1) from K3b.org, but when i want to start K3b, it just gives me:\n\nk3b: relocation error: /opt/kde3/lib/libartskde.so.1: undefined symbol: _ZN4Arts20VideoPlayObject_base4_IIDE\n\nAs far as i know, libartskde.so.1 is part of kdelibs. I've checked my version of kdelibs, and it's the newest release.\nI've tried to compile K3b by myself, but it just gives me compilation errors, so I don't know what to do...\nWho can help?"
    author: "AbHotten"
  - subject: "Re: KDE 3.1.1 problems with K3b 0.8.1"
    date: 2003-03-26
    body: "try to make a link to the libartskde.so in /opt/kde2/lib like\n\" ln -sf /opt/kde2/lib/libartskde.so /opt/kde3/lib/libartskde.so \"\nfor me it works fine\nsorry for my english!\n\n"
    author: "Dakar"
  - subject: "Re: KDE 3.1.1 problems with K3b 0.8.1"
    date: 2003-03-28
    body: "Hi,\n\tFor some reason they missed arts-1.1.1, what we have in our system is arts-1.0.3. Hence, the problem. Attach is a gzipped tar file. Please cd to /opt/kde3/lib and as root expand this file there. It replaces the older libraries with the new ones present in this tgz. Also restart the sound server once after that. After doing this I could run kaboodle, noatun etc and could listen to mp3s too.. I am not sure whether everything works."
    author: "Pramod Immaneni"
  - subject: "Re: KDE 3.1.1 problems with K3b 0.8.1"
    date: 2003-03-28
    body: "One thing I missed, after extracting make sure you change permissions to root.root, it might be easier to just change on all files in /opt/kde3/lib."
    author: "Pramod Immaneni"
  - subject: "kstars: \"Country of Lhasa is China, not Tibet\""
    date: 2003-04-05
    body: "i don't understand, why there's this \"unlucky\" formulation in the \"KDE 3.1.0 to 3.1.1 Changelog\" (http://kde.org/announcements/changelogs/changelog3_1to3_1_1.php) in the kdeedu section: \"KStars: Fixed bug #52205: Country of Lhasa is China, not Tibet.\"\nto avoid political/national feelings it should be formulated as follows: \"Country of Lhasa changed to China, province set as Tibet\"...\n(btw: yes, i have read the comments to the bug #52205)\n\non the other hand, i'm still using kstars 0.9.1 (which comes with kdeedu of KDE 3.1.0) with KDE 3.1.1 and there's already \"Lhasa, Tibet, China\" so i don't get the point of the changelog here for KDE 3.1.1\ni didn't upgraded kdeedu yet. does this mean kstars got the list of \"City, Province, Country\" from main KDE database (which i have already upgraded) or should i expect just \"Lhasa, China\" in the new kstars for KDE 3.1.1?\n\nluci"
    author: "luci"
---
The <a href="http://www.kde.org/">KDE Project</a> <a href="http://www.kde.org/announcements/announce-3.1.1.php">has released KDE 3.1.1</a>, the first maintenance release of the KDE 3.1 release series. It features more and much improved <a href="http://i18n.kde.org/">translations</a> and many problem corrections. Read the <a href="http://www.kde.org/announcements/changelogs/changelog3_1to3_1_1.php">Changelog</a>
or jump directly to the <a href="http://www.kde.org/info/3.1.1.php">download links</a>.  Those of you who wish to compile from source can use <a href="http://konsole.kde.org/konstruct/">Konstruct</a> for near automatic compilation.
<!--break-->
