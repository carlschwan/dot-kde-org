---
title: "OpenOffice.org: KDE Integration project launched today"
date:    2003-12-12
authors:
  - "jholesovsky"
slug:    openofficeorg-kde-integration-project-launched-today
comments:
  - subject: "cool"
    date: 2003-12-12
    body: "What?! I had no idea this was in the works. Way cool!! "
    author: "Apollo Creed"
  - subject: "KOffice"
    date: 2003-12-12
    body: "What means it for KOffice ?? It is better to concentrate on OOo instead KOffice??"
    author: "Marc Tespe"
  - subject: "Re: KOffice"
    date: 2003-12-12
    body: "KOffice is still a nice, lightweight, fast suit.\nReally interesting project."
    author: "OI"
  - subject: "Re: KOffice"
    date: 2003-12-12
    body: "OOo is now, koffice is the future ;-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "Exactly the way I see it! Frankly, openoffice is a great program functionality-wise. However, koffice is lighter, cleaner, MUCH faster, and offers a somewhat fresher take on office software (rather than emulation of ms office interface). As it improves (filters being the most important improvement), I think a lot of people will begin to use it instead.\n\nOn the other hand, OOo's flash export is cool :)"
    author: "optikSmoke"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "I am definitely against this unnecessary project of integration OOo in KDE!\nKDE looses its authenthicity and european originality. \nKDE should work on KOffice - it`s original programme!\nOpenOffice.org had done a very important break in a time for the success of Linux. But it happened because there was very little work on KOffice or none from the begining up to the last times. I know it very vell! KDE people neglected KOffice, they underestimated the world of fonds, writings, colors and so - they knew only codes and coding. So OpenOffice.org came in as a crucial necessity.\nKDE should be concentrated less on mass of new and new whim programmes, but explicitly on creative developement of KOffice!"
    author: "Alojz"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "You're kidding -- 'looses its authenticity and european originality'?  Didn't StarOffice (what OpenOffice was opened from) start as a German companies project?  Just because Sun 'owns' it now doesn't change it's European heritage ... \n\nNow - I don't disagree about your perspective on KOffice ..."
    author: "mark"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "Plus this hole \"European identity\" doesn't mean anything !\n\nAren't open source projects international and open for everybody, whatever their country or ethnicity are !?"
    author: "khalid"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "I agree, it is not important at all where it comes from. It should be open for everybody.\n\nGreetings\n\nTim\n"
    author: "Tim Gollnik"
  - subject: "Re: KOffice"
    date: 2003-12-15
    body: "Not only that, but much of the StarOffice hacking still takes place in Hamburg, Germany. There are even internships and diploma theses being offered for students to hack on OpenOffice."
    author: "dimby"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "What it means? Not much. All the expert/enthusiasts with knowledge about file formats, filters, text rendering, spreadsheet calculations and so on will continue to work on KOffice."
    author: "Anonymous"
  - subject: "o10n"
    date: 2003-12-14
    body: "moreover, i think that open source community (KDE to begin) should concentrate more on optimization of existing code.\n\nwhy gnome still alive? it's faster then kde. i prefer kde mainly because of it's integration and .NET style. But for some people speed is the main reason.\n\nSuch big companies as IBM are donating open source development, but they ain't interested in oprimization cause they sell hardware first of all (so if linux was fast enaugh, people wouldn't buy new hardware)"
    author: "20khz"
  - subject: "KOffice"
    date: 2003-12-12
    body: "This looks very promising, I just hope it doesn't mean taking attention away from KOffice.\n\nI think KOffice is a great project with great potential, and I wouldn't like seeing KDE taking a \"shortcut\" by replacing KOffice with OOo. One of the reasons why I like KDE so much is how it always choses to make things from the ground up instead of adopting existing implementations. I don't think there's such a thing as \"reinventing the wheel\" in software. Take Konqueror for example, KDE chose not to take Mozilla or any other existing browser and so now we have a _great_ lightweight browser. The same could happen to KOffice eventually, I think it has the potential, it just hasn't got enough momentum so far.\n\nOtherwise, very cool project :)"
    author: "Pilaf"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "FYI, Konqueror is actually older than Mozilla.."
    author: "Mark Dufour"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "KFM is older than Mozilla, but I don't think that any HTML rendering code from KFM is used by Konqueror..."
    author: "Ricardo Cruz"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "I assume gecko has also been rewritten a few times.."
    author: "Mark Dufour"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "Actually Netscape always wanted to call their browser Mozilla -- it just never caught on.  \"Mozilla\" was the browser made by Netscape.  It's been in the user agent string since the beginning..."
    author: "Scott Wheeler"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "> One of the reasons why I like KDE so much is how it always choses to make things from the ground up instead of adopting existing implementations.\n\nWhich is not true. Think of libxml2 (for whose usage KDE sometimes gets dissed because some consider it GNOME technology), fribidi, lcms, xine-lib and many other libraries."
    author: "Anonymous"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: ">Think of libxml2 (for whose usage KDE sometimes gets dissed because some consider it GNOME technology),\n\nThat's FUD. \n\n1. libxml2 _is_ in fact being used in some places.\n2. For many things, the Qt XML classes are more convenient (Unless you need a validating parser, that is).\n\nSame for fribidi, Qt has a pretty good bidi mechanism included. So this is all about pragmatic choices rather than about pseudo-political decisions. \n\nCheers,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "Agreed. libxml is good for the purposes for which it is intended. The stuff over Gnome libraries and CORBA is more complex, because adopting those in a big way means changing the structure of KDE (not going to happen) and dictates to KDE how it will be developed. I like the KDE peoples' attitude to D-BUS and other stuff on this basis."
    author: "David"
  - subject: "Re: KOffice"
    date: 2003-12-13
    body: "> [...] and dictates to KDE how it will be developed. I like the KDE peoples' attitude to D-BUS and other stuff on this basis.\n\nDoes that mean you're against or you're for using D-BUS and other things of freedesktop.org?"
    author: "CE"
  - subject: "Re: KOffice"
    date: 2003-12-15
    body: "D-BUS is different. It is desktop independent and it isn't a Gnome technology. The Gnome people have had huge rows about using it - perhaps because they would have to change evrything :)."
    author: "David"
  - subject: "this rules"
    date: 2003-12-12
    body: "not much more to say really.  koffice is fantastic and will no doubt one day be the office suite of choice but it's still some way off from that.  In the meantime, a Qt'ed up OpenOffice.org sounds like a great stopgap measure.  Of course it'd be nice if OOo just used Qt from the ground up, the standard VCL is sucky on all platforms...  oh, and it'd make a MacOS X version a breeze...\n\nHey, Apple, you want a really good project to sponsor?  Look here!"
    author: "my left foot"
  - subject: "Re: this rules"
    date: 2003-12-13
    body: "Apple is working on their own solution"
    author: "Marc J. Driftmeyer"
  - subject: "Re: this rules"
    date: 2003-12-14
    body: "Name or link plz?"
    author: "cies"
  - subject: "Re: this rules"
    date: 2003-12-13
    body: "I've asked myself the same thing many times... \"Why isn't OO using Qt as their toolkit?\".  I know there isn't a free version of Qt for Win32, but to me it'd make since for Qt to donate licenses to all developers as a way to promote Qt.  OO is a key component to the success of Linux on the desktop, which would mean a better audience for Qt to sell their product.\n\nAnyway, This is all just gut instinct looking in, without a detailed knowledge of what's gone on with OO. *shrugs*  But I'd definitely like to know!"
    author: "Jay"
  - subject: "Re: this rules"
    date: 2003-12-13
    body: "> I know there isn't a free version of Qt for Win32, but to me it'd make since for Qt to donate licenses to all developers as a way to promote Qt.\n\nAnother way would be to offer a special Qt-License for Win32 allowing OOo development."
    author: "Anonymous"
  - subject: "Re: this rules"
    date: 2003-12-14
    body: "Check this out!\n<br><br>\n<a href=\"http://kde-cygwin.sourceforge.net/qt3-win32/\">\nhttp://kde-cygwin.sourceforge.net/qt3-win32/</a>\n"
    author: "cies"
  - subject: "Re: this rules"
    date: 2003-12-14
    body: "qt3-win32 has improved a *lot* over the last few months, mostly due to the efforts of Ivan Deras. With some patching, I've gotten Qt Assistant and Designer to work, although the later crashes after some use. However, most drawing issues are quite well implemented, and even complicated widgets like QTextEdit work quite well. "
    author: "fault"
  - subject: "oh nice"
    date: 2003-12-12
    body: "Jan, you absolutely rule!\n\nAnd yes, I'm pretty sure that the people who are currently working on KOffice won't be the same people who are working on OOo.  The codebases and philosophies are just completely different."
    author: "Navindra Umanee"
  - subject: "Importance of OpenOffice integration"
    date: 2003-12-12
    body: "It is impossible to overestimate the importance of working with projects outside KDE. OpenOffice is stable and usefull _today_.\n\nSupport for OOo is important because it could be the deciding factor btween KDE and GNOME. Not technical merit, but skins, hacks, etc... are today Gnome best arguments to say it is more \"integrated\". And to say the truth, KDE in general did make less effort to coopt external projects than GNOME/GTK does: Mozilla, OOo, oGo?, etc... I hope this is changed now.\n\nIt is not true that GNOME \"integrates\" better with the above mentioned projects: it is skinning stuff mainly. But this may not be true in the future. Then, it may make a difference. So you are correct. The time to move is _now_.\n\nI think the KDE community already perceived this. See George Staikos http://dot.kde.org/1069632528/ , Aaron Seigo  and others really changing this, working on many fronts, like freedesktop.org, fedora project etc... On technical merit, my opinion is that KDE is the winner.\n\nI would also like to mention Craig Drummond work integrating KDE to Gtk apps (GtkQt, QtCurve, etc...). Thanks, Craig. Your work is _really_ appreciated. Makes kde feel more polished.\n\nThere could be more systematic use of kde-look.org stuff inside one of the kde optional modules.\n\nCongratulations for the announcement, and I really hope this project succeed. This are exciting times. it is a challenge. I hope I can do something about it."
    author: "cwoelz"
  - subject: "Re: Importance of OpenOffice integration"
    date: 2003-12-13
    body: "> KDE in general did make less effort to coopt external projects than GNOME/GTK does: Mozilla, OOo, oGo?\n\nCorrect me: khtml was there before Mozilla went OpenSource, KOffice was there before OOo, Kolab before oGo. There was a part to run the Mozilla rendering engine inside Konqueror, but it didn't receive much interest. Also there seems to be no real interest/need to have a Qt/KDE browser based on Gecko (just compare how many browsers using GTK/GNOME widgets exists: Nautilus extension, Galeon, Epiphany, Atlantis, Firebird). OOo cooperation is running as you see. Finally, oGo and Kolab can be integrated/work together AFAIK. And I think I heard about a oGo kdepim connector prototype."
    author: "Anonymous"
  - subject: "Re: Importance of OpenOffice integration"
    date: 2003-12-13
    body: "You miss the point: they can be older or younger, better or worse. But they have an active development community. And if you don't work with them, they will work with somebody else. We should try to make integration easy. Otherwise, it will be KDE vs the world.\nI am n_not_ saying that KDE is not working on it. I am just saying this is important."
    author: "cwoelz"
  - subject: "Why use Mozilla?"
    date: 2003-12-14
    body: "Why use Mozilla?\n\nApple managed to make the fastest browser in the world\nusing KHTML. So why shouldn't KDE use the best of the best too..."
    author: "OI"
  - subject: "Why use Mozilla? Because it does CSS/CSS2 better"
    date: 2003-12-15
    body: "Mozilla also displays many non-compliant pages better than Konqueror. It has that very cool 'Venkman' javascript debugger.\n\nI can also 'flatten' the horizontal menubar and toolbar at the top with just one click, increasing my area for viewing web content. (And I can restore either one or both with one click when I want to re-display the corresponding bar.) In Konqueror, crawling into the 'Settings / Toolbars' is much less convenient, and I have to restore the menubar to get at the 'Settings' even if that isn't the bar I want to restore or hide.\n\nThe auto-learning junk mail control works very well, and the explicit User Filters are easy to configure.\n\nIf you've got a speed problem with Mozilla, maybe you need to buy a $30 CPU upgrade on Ebay. Mozilla is a bit piggish on memory, but it runs fine for me with 256MB. (That's PC-100, integrated cheapo graphics, and an old Athlon 900-200FSB.)\n\nHave you tried Mozilla lately? I think that the rate at which it is improving has been very impressive."
    author: "Rick"
  - subject: "Good thing."
    date: 2003-12-12
    body: "Proper OpenOffice.org integration will make KDE that much more interesting for enterprise deployment, which is what we need right now. ROCK ON."
    author: "Eike Hein"
  - subject: "Spectacular News"
    date: 2003-12-12
    body: "Jan, I am very excited to see you working on this.  I hope Xandros or one of the KDE specific distributions helps you out with some work.  I wanted to join your mailing list, but I can't find a valid link.  The link to the development archives is not working.  How to sign up for the development mailing list?"
    author: "manyoso"
  - subject: "Re: Spectacular News"
    date: 2003-12-13
    body: "> I hope Xandros or one of the KDE specific distributions helps you out with some work.\n\nIndeed.. I really think it would be highly useful for a KDE-specific distro to sponsor Jan on his work. Someone like Mandrake (Gert even made a comment about OOo integration w/ KDE being important to Mandrake on his interview on KDE::Enterprise), Novell/SUSE, Lindows, Xandros, Lycoris, Connectiva, TurboLinux, RedFlag, etc.."
    author: "anon"
  - subject: "Re: Spectacular News"
    date: 2003-12-13
    body: "You have to be registered on the openoffice.org pages. If you are logged in, you can see the \"Subscribe\" buttons. The archives seem not to work, because there are no messages in them yet:)"
    author: "Jan Holesovsky"
  - subject: "Re: Spectacular News"
    date: 2003-12-13
    body: "This is a bug I think ( with the previous web infrastructure you did not need to be a member ).\n"
    author: "CPH"
  - subject: "This is fantastic news"
    date: 2003-12-13
    body: "Really, this is great news. The lack of integration between KDE and OOo, more specifically the fact that it did not take full advantage of the printing infrastructure provided by KDE was always a sore point for me. \n\n(I could say the same about Mozilla, BTW.)\n\nI have not read the other comments, neither have I looked at the articles/pages linked to, yet. So sorry if I ask something that has been addressed elsewhere, but do we have a timetable, here?\n\nWithin the next 7-12mo would be great, as there are a handful of comitees I'm aware of who are *really* thinking of taking alternative to the M$ Desktop in consideration for the next corporate desktop standard (this is not a joke)...  I guess the rash of M$-centric worms and other high-impact exploits during last year has shaken up a few more corporate Mucky-Mucks than we think.\n"
    author: "John D. Smitte"
  - subject: "Re: This is fantastic news"
    date: 2003-12-13
    body: "About printing integration, just configure \"kprinter\" as print command line in Mozilla, OpenOffice.org, Acrobat Reader and others."
    author: "Anonymous"
  - subject: "Re: This is fantastic news"
    date: 2003-12-15
    body: "Indeed, more specifically in the openoffice.org Printer Administration tool change the properties of the generic printer so that the command line is kprinter --stdin (will probably be lpr -P <something> by default). Has always worked for me, just need to remember to do \"Print this directly\" so I don't get two printer dialogs.\n\nI suppose something like that would work for Mozilla though I have never really understood a) people who feel the need to print web pages b) people who use mozilla when they run KDE, so I'm probably not the guy to ask in this case.\n\nAnd the linux version of Acrobat Reader is like pulling teeth, I mean there is a reason people don't write Motif apps anymore. KGhostView is fine for most things these days, and KPdf looks like being able to deal with the shortfall."
    author: "Blort"
  - subject: "Great news"
    date: 2003-12-13
    body: "However, the most important thing isn't how it looks or what icons it uses. It's the general integration into KDE that matters - fonts, antialiasing, printers, DCOP, KParts."
    author: "AC"
  - subject: "Re: Great news"
    date: 2003-12-13
    body: "thats why we have koffice (:"
    author: "another AC"
  - subject: "Congratulations!"
    date: 2003-12-13
    body: "I really need the best OOo-KDE integration possible.\nThere are many environments where you have to coexist Linux and Windows machines and OOo is the only office suite that can work on both environments, so it is the only solution available.\nBut right now OOo is kind of isolated in its own world and this is bad.\nAs others already said, being able to use KDE printing functionality, adressbook, open/save dialogs, etc. from OOo out of the box is just what we need.\n\nI like KOffice and as time goes on it could end being a real alternative, but we need OOo now!\n\nPlease go on with this effort, I wish many others join in to make this project progress as fast as possible."
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Congratulations!"
    date: 2003-12-13
    body: "> being able to use KDE printing functionality, adressbook, open/save dialogs, etc. from OOo out of the box is just what we need\n\nIf possible, it would be sooooo great ! KPrinter is so cool, I even use it when printing with OO.org, though I actually get two print dialogs, where the first one (native OO.o) is the annoying one...."
    author: "Thomas"
  - subject: "Very smart move"
    date: 2003-12-13
    body: "OO.org is the de facto for businesses and this is really needed."
    author: "Alex"
  - subject: "Re: Very smart move"
    date: 2003-12-13
    body: "...eyes wide shut.... MS Office is the de facto for businesses. Sad, but true..."
    author: "Thomas"
  - subject: "Re: Very smart move"
    date: 2003-12-13
    body: "But not for _all_ businesses.\nSome ven refuse doc:s in favour of pdf.\nSo it it is not that black or white."
    author: "OI"
  - subject: "Re: Very smart move"
    date: 2003-12-13
    body: "I've to admit... Yesterday I mailed a list of addresses over to a company for them to sent little christmas presents to selected customers of my business. The preferred format was OO.o sxc...\nI was a little bit surprised as it was a very well-doing company. If they choose OO.o they won't choose it for the reason of saving a bit of money... (It was Niederegger Marchpane, located at L\u00fcbeck, northern Germany http://www.niederegger.de/. They are aparently using OO.o/SO)"
    author: "Thomas"
  - subject: "What I meant"
    date: 2003-12-13
    body: "I meant for businesses that USE Linux."
    author: "Alex"
  - subject: "Simple things to make it better"
    date: 2003-12-13
    body: "I also think this is a very smart move.\n\nQt port seems a huge job though. Right now there are many simple things that could improve OO.org experience in KDE, like better drag and drop support from konqueror.\nIn OODraw for instance it seems to work but just stores the pic url! If you move the documents or the pics, you lose the pics from the doc!!\nhttp://www.openoffice.org/issues/show_bug.cgi?id=23526\nIn OOWriter sometimes it works, it works, sometimes it doesn't accept the pic so you have to do copy/paste...\n\nAnyway! Thanks and Good Luck!!"
    author: "Jadrian"
  - subject: "rant"
    date: 2003-12-13
    body: "Rant:\n\nI don't care how many office solutions are created.\nI don't care how many different styles, icons, widgets, api's gui's come with each.\nI don't care who creates what, where why and for whom.\nI don't care by who each is created.\n\nAs long as any office suit follows at least some STANDARDS as not to explicitly RULE OUT one office suite above others.\n\nEnd rant."
    author: "ac"
  - subject: "better integration"
    date: 2003-12-13
    body: "Koffice looks great but they still have ways to go. I can only use it for text files.\n\nThe begining of KDE/OOo cooperation is upcoming use of OOo's file format and filters for Koffice. With this good idea I might be able to use it more.\n\nFor NOW I can do with OOo's toolkit (I installed the icon patch from kde-look.org) because OOo does the job.\n\nWhat really gives me trouble every day and needs to be fixed is:\n1- Cut and paste of text doesn't work from OOo to Kmail (or is it Mandrake?)\n2- The file and print dialogs need to use the KDE equivalent."
    author: "ybouan"
  - subject: "Re: better integration"
    date: 2003-12-13
    body: "To 1: Neither nor :-)\nI use Mandrake 9.2 and OO 1.1. I can copy&paste from \nKMail to OO and also from OO to KMail.\n\nTo 2: Would be nice, but the printing works perfectly \nhere with cups and a HP LaserJet 1300.\n\nCarsten"
    author: "Carsten"
  - subject: "kdeifying OOo"
    date: 2003-12-13
    body: "Hi,\n\nI'm currently working on some code to use the kde file manager and the kde ioslaves via a simple C api. This should be easy to integrate into OOo, maybe even via LD_PRELOADING.\n\nProbably I'll put it into kdenonbeta within the net week.\n\nAnother thought: MS has two office suites, MS Office and MS Works... we have OOo and koffice... maybe not so bad after all ? Two office suites at different levels... maybe...\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "WOW"
    date: 2003-12-13
    body: "I haerd there already was a script to replace OO.org Icons by Crystal.  "
    author: "Gerd"
  - subject: "Yer ok"
    date: 2003-12-14
    body: "Standards are important but I like Kwrite and all the other things that come as standard already. Love the K news ticker thats how I found out about this project I hope to see more sources of news as well. (good not junk) Practicly anything that is about important developments.\n\nhttp://linux-enduser.tripod.com modzilla browsers will see jiberish."
    author: "ArmsDealer"
  - subject: "Clipboard!! Focus on the priorities, please..."
    date: 2003-12-15
    body: "Getting the clipboard working really WELL, I recommend come before anything else\n\nI and a few others are getting somewhere at least by implementing Linux/KDE in a big company. There are only a few really issues, but those _are_ really annoying as they are perceived as \"simple\"\n\nKeep up the good work. Hope you'll consider them"
    author: "SN"
  - subject: "NO FUTURE imho"
    date: 2003-12-19
    body: "The project will die a slow withering death. And it will drag the KOffice down as well for a while.\n\n1. Office is primarily a business tool. Without the business support things like that wither away. UserLinux had a point when it chose GNOME. Sun will not be able to sell the commercial QT-OOo without paying to TrollTech. <- QTOOo not a reality in any usefull form.\n\n2. Ppl droping interest in KOffice and jumping on a OOo wagon says much about their interest in KOffice developement.\n\nThe future of KDE-native office is very sad. It's even more sad-er cause i use KDE and would like to stick with it."
    author: "Daniel"
  - subject: "Re: NO FUTURE imho"
    date: 2003-12-19
    body: "I'm sure Sun can afford the one-off payment for a developers licence to QT."
    author: "James O"
  - subject: "Re: NO FUTURE imho"
    date: 2003-12-19
    body: "That won't work for open source projects, since sun would've to pay for every developer in this project.\n\nI wonder why trolltech doesn't implement an additional license policy similar to bitkeeper (www.bitkeeper.com). Take a one time fee (some multiple of the per-developer-fee) who want to use QT for open sourced software and require them to publish their code - eliminating the internal GPL'ed software loophole.\n\n"
    author: "anon coward"
  - subject: "Re: NO FUTURE imho"
    date: 2003-12-19
    body: "KOffice is not losing developers to OOo as far as I know.  If you want to do something about it, sponsor a KOffice developer today.\n\nAnd another note, Sun could probably save itself a lot of pain time and money by switching OOo to Qt instead of the horrid own-toolkit they use right now.  Yes, they'd have to negotiate a deal with Trolltech but chances are it would be hugely worthwhile."
    author: "Navindra Umanee"
  - subject: "Will it start faster?"
    date: 2003-12-19
    body: "Does this mean I won't need a Quad-Opteron with 2GB of memory to start OOo in less than a half hour?\n\nAnd a bug. I tried submitting the bug, but lost interest because it's so complicated in submitting it:\n\nWhen saving web pages in konqueror, it appears that konqueror saves a web page by downloading it again during the save process.  This creates a problem since I run konqueror on linux, and I don't shut down my computer at night, it runs for weeks or months without a reboot.  So I sometimes have web pages on konqueror sessions that are out of date.  When I try saving it, the page has changed, or is no longer available (very common for news sites that only give access to news stories for a few days before archiving them, or move the web page after a few hours or days).  Internet explorer does it differently.  Instead of downloading the page (again), it saves from the temporary file it created to originally display the page.  This allows you to save the page that is staring you in the face, instead of the browser telling you that the page that you were staring at no longer exists (and the page disappears, which brings up another bug:  when attempting to view document source, it downloads the document source from the site, instead of pulling it from the existing temporarty html file you already downloaded, so again, if the page has changed, or is no longer available, the source code can't be obtained, even though you were just looking at the page).\n\nA good method would be to 1. check the temporary file originally downloaded to display the page and save from there first, and if unavailable, then 2. try downloading again.  \n\nAnother important bug or feature request:  With Mozilla and Internet Explorer, I can save as: web page including images (\"web page complete\" instead of \"web page html only\").  Why can't I do this with Konqueror?  I'm not talking about \"archiving\" the page (useless because of not being able to set it up the same way as \"save as\"), I'm talking about using the \"save as\" and also being able to save the images.  In Mozilla, it works by saving the html page, and then saving images in a directory that is linked to the page, so that when you open the page from your hard drive, the image links are not broken.  Is it really necessary to save a web page and then be forced to go back and save individual images manually, one at a time, and then have to go back and edit the html page so that you don't get broken image links?\n\nbtw, kde is great. And thanks. If you could just get rid of some of the bloat..."
    author: "Anon12"
  - subject: "Re: Will it start faster?"
    date: 2003-12-19
    body: "> I tried submitting the bug, but lost interest because it's so complicated in submitting it\n\nYou're doing really bad at interesting developers to fix your complicated bug. If you don't even bother to report the bug, why should a developer bother to fix it?"
    author: "Anonymous"
  - subject: "Maybe if it were easier to submit the bug"
    date: 2003-12-19
    body: "I would submit it.\n\nI'm not a programmer.  Ask your mom to submit a bug, and see how far she gets.\n\nAnd that's not meant as sarcasm at your expense.  Maybe that's what's missing.  Some usability tests.  Put non-technical users in front of your projects, see how far they get, and see how you can make it easier for them.\n\nI'm posting the \"bug\" or feature request here because after a half hour, or hour (actually more), I gave up trying to post it using the programmer's interface for submitting bugs.\n\nNow you're telling me you don't want to here it.  Fine.  I'll keep my mouth shut, and you can go on grousing about a product with inferior features.  Some features are great.  But if you don't want to hear about problems from non-technical users, then I identify you as one of the poeple that regularly posts about how linux is ready for the desktop and the masses, but if you have a problem, go rtfm!\n\nKind of like military intelligence, no?"
    author: "Not a programmer"
  - subject: "Re: Maybe if it were easier to submit the bug"
    date: 2003-12-19
    body: "sorry. stupid logic. there are thousands of other bugs\nout there and you somehow think that your bug is so important\nthat we should search public forums for it?\n\nsorry. that'll waste more time than it could *possibly* save.\nthough your report was written well and would probably be \nfairly easy to fix, you failed to use the extremely easy\nto use bug reporting interface to get it into the correct\nforum, and yet you are a linux user.\n\ni'm not sure i understand.\nbugzilla is slow i agree. but difficult? pleaaasseee....\n\nAlex"
    author: "lypie"
  - subject: "Re: Maybe if it were easier to submit the bug"
    date: 2003-12-19
    body: "I believe your attitude is broken.\n\nIf some random user fails to use the bug reporting interface after making an honest attempt at it, the interface is *not* easy to use, no matter what you might think.\n\nConsider it a good thing that they're pointing this out, and maybe get them to explain to you, in their own terms, what the problem was, so it can be fixed."
    author: "Just some guy"
  - subject: "Maybe you just don't want to hear him"
    date: 2003-12-20
    body: "If it were so easy to file a bug report why didn't you file his report?  The explanation is easy enough to understand and is easy to check later on if it's been fixed.  File->save as, does it save from web server or from temp file?\n\nYou aren't interested?  Then why slam him?  Move on."
    author: "Big Easy"
  - subject: "Re: Maybe you just don't want to hear him"
    date: 2003-12-21
    body: "> If it were so easy to file a bug report why didn't you file his report? \n\nI for one didn't because it's already there. \n \nBesides he said (below) that he was actually trying to report \na bug against Mozilla and found *that* too hard.  He mixed it up.\n\n"
    author: "cm"
  - subject: "Re: Maybe if it were easier to submit the bug"
    date: 2003-12-19
    body: "> I'm posting the \"bug\" or feature request here because after a half hour, or hour (actually more), I gave up trying to post it\n\nOver an hour? Thats make me consider you a liar and troll."
    author: "Anonymous"
  - subject: "Re: Maybe if it were easier to submit the bug"
    date: 2003-12-20
    body: "Who's trolling who?"
    author: "Tom"
  - subject: "Re: Maybe if it were easier to submit the bug"
    date: 2003-12-19
    body: "You should report your bug to bugs.kde.org since it's the only central place for managing such kinds of reports. When you post it anywhere else like here you already wasted your time since no developer will see your report ever again later on.\n\nAnd reporting bugs to bugs.kde.org is very easy once you are registered. Every KDE app has following menu entry:\nHelp -> Report Bug...\nUse it and fill out the few remaining questions, I doubt you'll need to be a programmer for doing that. If you think there's still something needing improvements let me know."
    author: "Datschge"
  - subject: "Please excuse my error"
    date: 2003-12-20
    body: "I was incorrect about the bug submission problems.  My memory was off.  Apparently I confused Konqueror and Mozilla bug submissions.  As I remember it now, Mozilla is the one I gave up on submitting bugs or feature requests.  Because of that, I have never submitted a bug (really a feature request) to Mozilla.  And I gave up trying.  Their bug submission screens are really hard for a non-technical person.  I had trouble figuring out if I was submitting to the correct application, version, and several other fields, because of the large number of choices present, and the non-intuitive (in my opinion) interface for submitting those bugs. And my last attempt resulted in leaving a half dozen screens open on one of my desktops until the computer had to be rebooted, which ended tht attempt.\n\nI have, in fact, submitted several bugs to the konqueror project, but that was quite a while ago.  And as I remember it now (thanks to someone mentioning registration), it was, as reported, a fairly simple procedure.  And the response was in a day or two.  The bug (actually a few iirc) were not accepted because there were similar features or capabilities, just not the way I envisioned them.\n\nWhy did I post my bugs (feature requests) here?  It wasn't done as a random message board post as someone suggested.  I have been to this message board a couple of times in the past (different areas) and have seen that many posters ARE developers.  So, in my confusion with Mozilla and Konqueror bug submissions, I knew I had given up on formal bug submissions, so I figured I'd give a shot to putting my suggested feature request in front of the eyes of possibly one or more developers, one of whom might be interested enough, and kind enough to submit the feature requests to make it better for everyone, not just me.\n\nDid I imagine that it would inflame one or two people?  No.  And that was not the intent.  The intent was simply an attempt at making my favorite browser better.  That's it.\n\nI apologize for my mistake."
    author: "Anon12"
  - subject: "Re: Will it start faster?"
    date: 2003-12-21
    body: "> When saving web pages in konqueror, it appears that konqueror saves a web\n> page by downloading it again during the save process. \n\nThis bug has already been reported and (at least partially) fixed \n(not sure about the current state of affairs): \nhttp://bugs.kde.org/show_bug.cgi?id=59965\n\n\n\n> With Mozilla and Internet Explorer, I can save as: web page including images\n>(\"web page complete\" instead of \"web page html only\"). Why can't I do this \n> with Konqueror? I'm not talking about \"archiving\" the page (useless because \n> of not being able to set it up the same way as \"save as\"),\n\nI don't see why the web site archiver plugin would be useless.  \nIt works exactly the way you described.  It just puts those images \nand the HTML into a tarred gz archive, the default file extension is .war.  \nThere are no broken links when you click on it in konqueror's file manager\nmode.  Browsing into it is transparent.  You don't have to unpack it \nto be able to view it.  Very convenient, I use it quite often. \n\nWhat do you mean by 'not being able to set it up the same way as \"save as\"'?\n\n\n"
    author: "cm"
  - subject: "But the real question is..."
    date: 2003-12-19
    body: "Will I continue getting blue screens of death with OOo?\n\nError saving the document.  xx.sxw:\nWrong parameter\nThe operation was started under an invalid parameter.\n\nI've been having nightmares over that dialogue box, and the lost data thanks to it.  I thought I left all that behind with windows."
    author: "Anonymous Gerbil"
---
The <a href="http://kde.openoffice.org/index.html">
OpenOffice.org KDE Integration project</a> has been launched today.
Maybe you remember 
<a href="http://kde.openoffice.org/cuckooo/index.html">Cuckooo</a>,
a KPart allowing OOo to be embedded in the Konqueror window (as a viewer).
In the meantime this has evolved into an 
<a href="http://kde.openoffice.org/ooo-qt/index.html">OpenOffice.org Qt port</a> and the development does not stop there.
KDE Integration is now an official 
"<a href="http://incubator.openoffice.org/">Incubator</a>" project at 
<a href="http://www.openoffice.org/">OpenOffice.org</a>, which means that it has been accepted by the OpenOffice.org community and if it continues well, it will become an official "accepted" project.









<!--break-->
<p>
You can help with the development and speed it up. You can subscribe to
its <a href="http://kde.openoffice.org/servlets/ProjectMailingListList">
mailing lists</a>, download the sources and code!
</p>
<p>
As many people have been asking for a KDE compatible look for OpenOffice.org
a KDE Native Widget Framework subproject has been added to the main page
as well.
It has not started yet but if there are volunteers that want to
work on it they are very welcome! I will try my best to help you.
More information about OOo's Native Widget Framework can be found
<a href="http://gsl.openoffice.org/files/documents/16/1286/vclNativeWidgetProposal.sxw">here</a>
and
<a href="http://people.redhat.com/dcbw/">here</a>.
</p>








