---
title: "KDE-CVS-Digest for December 5, 2003"
date:    2003-12-06
authors:
  - "dkite"
slug:    kde-cvs-digest-december-5-2003
comments:
  - subject: "Thanks Derek"
    date: 2003-12-06
    body: "Good work bro."
    author: "Turd Ferguson"
  - subject: "Nice"
    date: 2003-12-06
    body: "A lot of khtml bugfixes.. great.. there were a lot of evil regressions before.\n\nThe new kids iconset looks quite good for kde at schools in k-6 environments. "
    author: "anon"
  - subject: "kdeinit ?"
    date: 2003-12-06
    body: "Is kdeinit (yes, the kdeinit-hack) still necessary?\nI remember that the old dynamic linker had problems and kdeinit has been\ncreated as a workaround for this.\n\nAnd somehow it really seems ugly that all (or most of) the kde processes\nare called kdeinit. With exceptions, as e.g. kmail runs as its own\nprocess!?!\n\nOther place where it sucks is when you start an application as root,\ne.g. kwrite. Then you can see a bunch of kdeinit processes coming up\nfor the root user. Also to note that it takes a long time to start up\nthe root-kwrite, as it needs the whole kdeinit-load to come up.\n\nThe same is of course true when you run a kde application outside of\nkde, e.g. in gnome. Also plenty of kdeinits around for one\nsimple konqueror or kate...\n"
    author: "ac"
  - subject: "Re: kdeinit ?"
    date: 2003-12-06
    body: "Assuming that going away from kdeinit is wanted, it cannot be done before KDE4, whatever the good reasons that might exist.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: kdeinit ?"
    date: 2003-12-08
    body: "\nAlready requested: http://bugs.kde.org/show_bug.cgi?id=51157\n\nStatus is RESOLVED LATER.\n\nVote for it if you like :)"
    author: "cbcbcb"
  - subject: "Re: kdeinit ?"
    date: 2003-12-06
    body: "Why is kdeinit needed?  What does it do?"
    author: "kinema"
  - subject: "Re: kdeinit ?"
    date: 2003-12-07
    body: "\nWhat kdeinit is, is probably explained a thousands times, google for it, otherwise you will probably find an extensive answer at developer.kde.org\n\nkdeinit's is KDE's way of speeding up startup time, when starting an application alot of time is spent on symbol lookup and linking, caused by the nature of highly object oriented c++ code. Instead of having every application do that, a kdeinit is  linked to common libraries and the cumbersome startup procedure is only done once. Instead kdeinit forks off processes(which then don't have to do the lookup).\n\nBut I could be wrong though.\n\ncheers,\nFrans"
    author: "Frans Englich"
  - subject: "Re: kdeinit ?"
    date: 2003-12-08
    body: "Would prelinking make kdeinit redundant then?"
    author: "JohnFlux"
  - subject: "Re: kdeinit ?"
    date: 2003-12-08
    body: "\nTheoretically it could make kdeinit obsolete in the sense that the speedboost kdeinit offers is not needed. But unfortunately it's not that easy since kdeinit is pretty heavily interwoven into kde's framework.\nBut no one wants bloat. If an unnecessary mechanism exist in KDE it will be removed. But not because of prelink, atm. (there could be other reasons besides speed which kdeinit's are used but I don't know).\n\ncheers,\nFrans\n"
    author: "Frans Englich"
  - subject: "Re: kdeinit ?"
    date: 2005-04-24
    body: "> What kdeinit is, is probably explained a thousands times, google for it\n\nI googled for it before reading this thread.  This thread was the top result :-)"
    author: "andy chambers"
  - subject: "Thanks a lot!"
    date: 2003-12-06
    body: "One thing that I don't quite understand is if Kspell 1.2 will be in 3.2 or not and if the great new Kids icon theme will be in 3.2.\n\nThey're both cool and I hope they'll make it."
    author: "Alex"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-06
    body: "Kids icon are in for 3.2. Not sure of kspell.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-06
    body: "The kids icons is not yet in beta2 (I'm currently using it) but I downloaded it from kde-look and it's pretty fun. Looks like a cartoon ;)"
    author: "JC"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-06
    body: "I might not be in the tagged code but it is in current CVS. I was added some (like three?) days ago. I use and love it :-)\n\nCarsten (24y old)"
    author: "Carsten"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-07
    body: "> Carsten (24y old)\n\nhehe. are you sure you aren't ten? :)\n\nBut yeah, the kids icon set is sweet. another fantasic creation by everaldo. There are a number of fantastic new icon sets coming out for KDE recently.. Sparkling (http://www.kde-look.org/content/show.php?content=9245) is damn good, Relax is very good (http://www.kde-look.org/content/show.php?content=9148). \n\nNow that kdeartwork is getting quite big,  someone should do a third party release of a whole bunch of good icon sets, including the large versions. Of course, such a set could be more than 50 mb these days... "
    author: "anon"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-07
    body: "Any reason for the kids SVG sources not being imported?  128x128 was not included because of the size and it is true that kdeartwork can be quite large now.\n\nThe use of SVG would have negated all this from the beginning.  Why the use of .png still?"
    author: "Jesse"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-07
    body: "Maybe kdeartwork should consist of svg files only, than the png's are rendered on the machine it gets installed on, according to his needs."
    author: "cies"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-08
    body: "Yes, but not all icon sets are svg based yet. I know the new Crystal is supposed to be 100% SVG.. why do pre-rendered png's come with HEAD? Supporting larger and larger icon sizes is becoming a must these days.. with KDE 2.x or 3.0, most icon sets got away with providing largest icon with 48x48 sizes, and I beleive the classic Crystal (3.1) provided 64x64 and some larger.\n\nWith the high resolution LCD's that most new computers come with, 128x128 is going to be more than likely a necessity before KDE 3.2. This is where SVG can help greatly."
    author: "anon"
  - subject: "Re: Thanks a lot!"
    date: 2003-12-06
    body: "KSpell 2 is in kdenonbeta (the developer's sandbox). So it will not be part of KDE 3.2, which is in feature freeze anyway.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "What would be cool:"
    date: 2003-12-07
    body: "A GUI script builder for DCOP... something like http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-scriptbuilder.html , but more generic.. anybody know of one? \n"
    author: "*69"
  - subject: "Re: What would be cool:"
    date: 2003-12-07
    body: "are you looking for kdcop?"
    author: "Ian Reinhart Geiser "
  - subject: "Re: What would be cool:"
    date: 2003-12-07
    body: "no.. a more editing oriented app than kdcop-- which is great for looking up what a dcop app. \n\nI'm looking for something more like Apple's Script Editor for AppleScript, if you've ever heard of that. Perhaps that much automation (e.g, recording) isn't possible with dcop yet though."
    author: "*69"
  - subject: "Re: What would be cool:"
    date: 2003-12-07
    body: "yes, i've played some with an editor approach, but all i have done so far was add drag and drop code export from kdcop.\n\nWhat im more interested in doing is going from a KDevelop approach that can build scripts on the fly via the actual dcop calls.  The issue is we lack something similar to Apple's script dictionaries.  I can emulate it with parsing the raw KIDL files, but its still not perfect.  The main difference of DCOP vs Applescript, is apple uses apple events to record what is going on.  We done have such a beast.  I have tried to get arround it by having dcop interfaces emit that they where called when the funciton is executed, but still its not perfect.\n\nStill thinking... Personally I think the easiest thing to do at this point would be to build a KDevelop part, but im not happy with that yet... Maby kde 3.3 :)\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: What would be cool:"
    date: 2003-12-07
    body: "Okay, I'm diving in without understanding entirely what you are saying, admittedly, but...\n\nSurely the DCOP server should be able to log these things, Ian, instead of the interfaces emitting it. That way, you also get an understanding of routing and whether operations suceeded?"
    author: "Dawnrider"
  - subject: "Re: What would be cool:"
    date: 2003-12-07
    body: "Because the issue is not tracking what dcop interface was called, but to record what action would map to a dcop interface.  What I was going to, was to have an action emit that something happened, and if you call dcop interface \"foo\" you can duplicate it...  crude but effective...\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: What would be cool:"
    date: 2003-12-07
    body: "> What I was going to, was to have an action emit that something happened, and if you call dcop interface \"foo\" you can duplicate it... crude but effective...\n\nYup, that's essentially how AppleEvents work in Carbon apps and in Classic MacOS. They were passed through the eventloop in fact (pretty much how kipc works), so I wouldn't call that approach terribly crude. In Cocoa Apps in OSX, AppleScript skips Apple Events in favor of the message handling features of Objective C (and some support somewhere in the system layer) "
    author: "fault"
  - subject: "Re: What would be cool:"
    date: 2003-12-07
    body: "yes, and we have nothing like that in KDE ;)\n\nPersonally id love to have something like NSProxy, so that we could automaticly bind the object... but i fear such a thing just wont happen much better than what we have with QObject.\n\ni should probibly note, ive been using NeXT since the mid 90s, and OpenStep until the PPC version became OSX server version 1\n\nive been tracking the GNUStep guys, to see what they are doing, but imho until we evolve to a real OO Language everything we will do will be a hack...  Granted im not holding my breath for the trolls to port Qt to ObjC ;)"
    author: "Ian Reinhart Geiser"
  - subject: "Re: What would be cool:"
    date: 2003-12-08
    body: "Hey, cool, glad someone noticed our scriptbuilder!  :)   \nIt would be cool to have a generic GUI ScriptBuilder for all KDE DCOP interfaces; probably built onto kdevelop would make the most sense.\n\nFor KStars I basically just hand-coded all of the functions in; for a full KDE scriptbuilder, you'd definitely want to be more clever than that.  Perhaps the interface header files could be parsed to figure out what kind of data each function expects.  Still, in some cases this would not be enough.  For example, we have the \"lookToward( QString )\" DCOP function in KStars, but you can't just feed it any old string and expect it to work, so in our ScriptBuilder we provide widgets that allow the user to pick a sensible string (i.e., a combobox for the compass points: \"north\", \"northeast\", \"east\", etc. plus \"zenith\"; and a button for selecting a named object from the list of known objects).\n\nAnyway, it would be really nice to have a KDevelop extension for DCOP scripts."
    author: "LMCBoy"
  - subject: "Re: What would be cool:"
    date: 2003-12-08
    body: "You mean something like the KJSEmbed support we're adding to KDevelop? This can talk dcop.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Notice for the developpers :)"
    date: 2003-12-07
    body: "I just discovered the closing tab button in kdevelop3-beta2. Why not using it in konqueror. That would be great !\n"
    author: "JC"
  - subject: "Re: Notice for the developpers :)"
    date: 2003-12-07
    body: "It is configurable, off by default.\n\nKonqueror settings, web behavior, advanced options.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Notice for the developpers :)"
    date: 2003-12-07
    body: "Settings -> Configure Konqueror -> Web Behaviour -> Advanced Options -> Show close button instead of website icon\n\n:)"
    author: "Hamish Rodda"
  - subject: "Re: Notice for the developpers :)"
    date: 2003-12-08
    body: "Yeah, thanks a lot.\n\nIt should be the default option :)\nI love it."
    author: "JC"
  - subject: "Re: Notice for the developpers :)"
    date: 2003-12-08
    body: "Just to even it out..\n\nIt should not be the default option. :)\nI hate it.\n\nI want it out of KDevelop. :)"
    author: "teatime"
  - subject: "Re: Notice for the developpers :)"
    date: 2003-12-07
    body: "It's placed on the tabbar instead in konq-head by default, but yeah, but galeon-type (close button on tabs) and mozilla-type (close button in tabbar) type. The latter is default and will probably be the more KDE-way to do it."
    author: "anon"
  - subject: "Re: Notice for the developpers :)"
    date: 2003-12-08
    body: "I think the KMdi approach is even better. (Showing the app icon, but when moving the mouse cursor over it fading the close button in).\n\nRischwa"
    author: "Rischwa"
  - subject: "Tab icons"
    date: 2003-12-08
    body: "What IMHO really needs to be fixed before 3.2 final is replacing the old, ugly tab icons by new ones found here: http://kdelook.org/content/show.php?content=6449"
    author: "Niek"
  - subject: "Re: Tab icons"
    date: 2003-12-08
    body: "These icons were moved from the toolbar in kde 3.2"
    author: "anon"
---
In <a href="http://members.shaw.ca/dkite/dec52003.html">this week's KDE-CVS-Digest</a>:
<A href="http://kate.kde.org/">Kate</A> gets highlighting optimizations and bug fixes. 
<A href="http://kolourpaint.sourceforge.net/screenshot1.png">KolourPaint</a> adds a curve tool and zooming. 
kabc, the <A href="http://pim.kde.org/components/kaddressbook.php">addressbook</a> library adds <A href="http://www.egroupware.org/">eGroupWare</A> and 
<A href="http://www.phpgroupware.org/">phpGroupWare</A> resources. A <a href="http://kdelook.org/content/show.php?content=9144">new icon theme</a> for kids is included for 3.2.
Plus many bugfixes in KHTML, <a href="http://kmail.kde.org/">KMail</a> and <A href="http://uml.sourceforge.net/">Umbrello</A>.

<!--break-->
