---
title: "Andrew Stanley-Jones on KSirc"
date:    2003-07-02
authors:
  - "numanee"
slug:    andrew-stanley-jones-ksirc
comments:
  - subject: "KSIRC is very nice"
    date: 2003-07-02
    body: "But where are they keyboard bindings?  That's really the only thing I feel the client is missing...  The ability to use ctrl-left/right to move between tabs and the ability to customize some of the other bindings (like removing ANY binding to ctrl-enter, I can't count how many times I hit ctrl-enter to send a long long message in IRC (a la most IM clients) and end up with a blank /MSG, grrrr...)\n\nDo you think there is any capability to having the perl backend able to be sitting on a different computer and be able to detach/attach the GUI, so that I can ssh over and use it in text mode in the cases where I don't have access to a GUI?  (I know a little far-fetched...)\n\nSeriously though, ksirc is very very cool; Easier to use than irssi and bitchx and all of those \"I'm too leet to be using a GUI IRC client\" while maintaining the ability to script and actually do some \"real work\" in IRC.  Thanks!"
    author: "Andrew"
  - subject: "What about kopete..."
    date: 2003-07-02
    body: "I think Ksirc will have to kompete with kopete in k3.2, cause kopete will do irc-ing too, and a lot more!\n\nhttp://kopete.kde.org/img/screenshots/current/kopetechatwindow.png\n\nAnd why perl? Why does an irc client _need_ perl to load (i can imagine perl is used for optional plugins).\n\nRight know i still use ksirc, because it's standard and works great!"
    author: "cies"
  - subject: "Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "Hi!\n\nI've used KSirc and KVirc, and KVirc has much more features and most of them are need-to-have ones. So I don't understand why KSirc and not the more powerfull KVirc are shipped with KDE."
    author: "Robert Penz"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "1. There is no need for including 20 IRC clients\n2. ksirc works well, especially the version in HEAD\n3. Correct me if I am wrong, but kvirc is a Qt and not a KDE app"
    author: "AC"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "1. sure just include one that works e.g. with pycho-bnc\n2. know only the version which is shipped with kde 3.1.1\n3. you can compile it as qt or as kde app."
    author: "Robert Penz"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "No, you can't. You can turn on shallow integration changes, but that \ndoes not make it a KDE apps. KDE apps may use Qt, but they're written \nvery differently from pure Qt apps.\n"
    author: "Sad Eagle"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "Hey, I've tried them both and I stick with XChat, one of the few gtk/gnome apps I use.\nThere are to many things wrong and missing in KSirc...\nLets start \"Connect Server Dialog\"\nHow do I minimize it?\nHey but I can maximize it, useful and beautiful isn't it... argh...\nHmmm where do I set different nicks for different servers?\nWhere do I even set my nick here? I think I don't, just in global prefs. \n\nOk, lets just connect to some server and see what we have.\nLet me search the available rooms... search the availa... search... no, no option...\nJust /join #something... let me right click some guy, and check the options.\nWhere is dcc? dialog?\n\nOh well, I'll stop here. Yes much simpler than xchat, but way too simple... :-/\nIt looks good, and it feels good, but lacks way too many features.\n\nXC"
    author: "Xchater"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "Another problem with ksirc is that it keeps the idle time below 30s, which is annoying..."
    author: "Arnaud"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-03
    body: "This has been fixed. Maybe it's possible to backport that one."
    author: "Daniel Molkentin"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "> Where is dcc?\n\nJust drag a file onto the nick (or a chat-window) to dcc-send it."
    author: "Carsten Pfeiffer"
  - subject: "Re: Why not include Kvirc in KDE"
    date: 2003-07-02
    body: "Or in HEAD, Channel->DCC Manager (hmm, why is it in the channel menu?), \nand then go to the manager tab/window and press \"New\". Probably could be simplified, I guess, but the UI is there.\n"
    author: "Sad Eagle"
  - subject: "Ksirc CVS preview screenshots"
    date: 2003-07-03
    body: "I did a couple of screenshot from my kde cvs installation on gentoo.\n\nget em there :-) :\n\nhttp://www.kde-look.org/content/show.php?content=6891"
    author: "pat"
  - subject: "KVIrc"
    date: 2003-07-02
    body: "I've been happy user of KVIrc for a long time now. I changed from X-Chat when qt3 version of KVIrc(beta1 IIRC) came out. I have been wondering why not dump KSIrc in favor of KVIrc. KVIrc has lot more features and it's really a lot better than KSIrc IMHO."
    author: "Jorma Tuomainen"
  - subject: "Re: KVIrc"
    date: 2003-07-02
    body: "As has been said before, KVirc is not a KDE app. Heck, it's not even close to being a proper Qt app, as it re-implements half the toolkit. ksirc might not be perfect, and as ASJ described, the Perl backend provides some problems, but it is still essentially a KDE app.. And as for features.. Try ksirc HEAD (it builds fine on KDE3.1.x, and is nice and stable, in my experience). It fills in most of the gaps in the older ksirc versions (providing a nice DCC \nmanager, nick coloring, etc.); but also does not provide functionality not related to IRC. \n"
    author: "Sad Eagle"
  - subject: "Konversation rulez"
    date: 2003-07-02
    body: "Konversation interface is a lot cleaner.  Why do people write program that opens so many windows?  KSirc reminds me of Gimp.  It's interface is confusing.  Minimizing all the windows and trying to get to the same windows that you left off from is a horendous time consuming process.  Where as Konversation, u simply click on the Konversation minimized icon and boom, that's where u left off at."
    author: "Konversation"
  - subject: "Re: Konversation rulez"
    date: 2003-07-02
    body: "I agree. I know no IRC client (neither on Windows nor on Linux) which\nis really suitable for a novice. Somehow IRC clients always tend to have\na confusing interface. What's necessary IMO:\nAn IRC client which makes chatting on IRC as simple as doing a web chat\nin a similar interface. Advanced functions don't have to be removed,\nthey just need to be organised in way that doesn't interface with a novice\nuser. KDE is usually very good at this. For example everyone can use kicker\nright away but it can be configured in quite a complex way. ksirc is really\ndisappointing. All those windows ARE confusing and it has quite a nonstandard\ninterface anyway. It's no excuse that an IRC client needs to provide quick\nshort-cuts for pro-users: The slow way of doing things still needs to be\nthere. You can cut and paste files with Ctrl+X and Ctrl+V in Konqueror.\nDoes that mean we can get rid of those entries in the menu bar? Of course\nnot! This is really horrible, but I think IRC client programmers are still a somewhat\nspecial \"nerdy\" group: All those IRC clients and all with confusing dialogues,\nthousands of status windows, and complicated functions. Oh well."
    author: "Jan"
  - subject: "Check out Kopete"
    date: 2003-07-02
    body: "> An IRC client which makes chatting on IRC as simple as doing a web chat\n> in a similar interface.\n\nAn interface similar to an IM client also ok? (my little sis kills you with msnmess ;-)\n\nCheck this out:\nchat window:\nhttp://kopete.kde.org/img/screenshots/current/kopetechatwindow.png\nand here some other screenshot that shows how the irc-favorites fit in the \"buddy list\":\nhttp://kopete.kde.org/img/screenshots/current/kopetemainwindow.png\n\nkopete and kontact will rock k3.2!"
    author: "cies"
  - subject: "Re: Check out Kopete"
    date: 2003-07-03
    body: "> kopete and kontact will rock k3.2!\n\nGiven kopete's current track record with regard to stability and overall quality, I'd say this falls under the \"famous last words\" category.\n"
    author: "Guillaume Laurent"
  - subject: "Re: Check out Kopete"
    date: 2003-07-03
    body: "I've been using it for a month now (from cvs) and it hasn't crashed yet compared to gaim that crashes every two second when I use it with msn. kopete looks great and is very active and full of very cool plugin. u really should check it out"
    author: "pat"
  - subject: "Re: Check out Kopete"
    date: 2003-07-03
    body: "I'm using 0.6.2 daily with the AIM plugin and it's still very easy to crash. It also sucks at reporting error conditions (when it can't connect to a server). The irc plugin is simply not useable. I suppose it has improved in CVS, but they have a lot of work to do before it's on par with other KDE standard apps."
    author: "Guillaume Laurent"
  - subject: "Re: Check out Kopete"
    date: 2003-12-22
    body: "OK, it's six months later, and I'm using Kopete 0.7.4. It crashes constantly, especially with the Yahoo plugin. Gaim (similarly versioned 0.74) rarely crashes. I wish kopete were stable, because whenever anything changes in the system tray, gaim pops up unexpectedly."
    author: "johnb"
  - subject: "My 2 euro on Ksirc"
    date: 2003-07-02
    body: "Ksirc is cool but it needs two things:\n\n1- Remove ugly colors in ksirc.pl ;)\n2- Polish the GUI"
    author: "cartman"
  - subject: "Konversation rulez"
    date: 2003-07-03
    body: "Konversation interface is a lot cleaner.  Why do people write program that opens so many windows?  KSirc reminds me of Gimp.  It's interface is confusing.  Minimizing all the windows and trying to get to the same windows that you left off from is a horendous time consuming process.  Where as Konversation, u simply click on the Konversation minimized icon and boom, that's where u left off at."
    author: "Konversation"
  - subject: "Re: Konversation rulez"
    date: 2003-07-03
    body: "I would agree, the new 0.12 beta is even a bit better... I am looking forward to its real release... Though the beta is good enough for daily use... I really like using the tab interface ie, all different irc servers and channels tabbed in one window."
    author: "dnm"
  - subject: "Kinkatta"
    date: 2003-07-03
    body: "While working at Sharp electronics they let me work on Kinkatta sense in a round about way they binifited.  You might be surprised who will let you work on it.\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Great!"
    date: 2003-07-03
    body: "Hello,\n\nI use KSirc exclusively as my IRC client. It is simple and it works. And,  really, that's all that matters. I look forward to its evolution. Good job!\n\nRegards,\n\nMystillee"
    author: "Mystilleef"
  - subject: "DCC Chat session opens .... in channel ! : ("
    date: 2004-10-01
    body: "Apologies if this comment is obsolete, since i m running 1.1 , which might be a little old by now, BUT\nWhen i type a /ctcp trigger and the dcc chat request returns, i open it, but it then talks into the channel i typed it from!\nDespite the lack of privacy, it s clear that what i type in there is not being forwarded. This happened when connecting to a mIRC user . \nKSIrc does a good job, I guess that's why its included in the KDE, but\nthis seems a major drawback. HAs it been fixed in later releases? is there a reliable linux client other than wine mIRC.exe for /ctcp ing out there?\n"
    author: "WEI XiaoBao"
  - subject: "Linux user"
    date: 2008-03-21
    body: "Ksirc is a cool client to use. If I might make a suggestion a \"bookmark\" feature would really make Ksirc rock! That way people could go from channel to channel flawlessly. \n            Rock on!!\n                     Eric"
    author: "Eric"
---
In the following interview, <a href="mailto:asj@kde.org">Andrew Stanley-Jones</a>, original author of KSirc, gives us some of the insights behind the design of KSirc -- the <a href="http://www.irchelp.org/irchelp/new2irc.html">Internet Relay Chat</a> (IRC) client for KDE.  Read on for such gems as <i>"No company I've ever worked for has offered to pay me to write a client that allows you to waste time chatting online"</i> and <i>"I argue [that chatting on IRC] keeps me awake during a chick flick"</i>.  This interview <a href="http://www.kde.de//appmonth/2003/ksirc/">was featured</a> some time ago on <a href="http://www.kde.de/">KDE.de</a> and is provided in English courtesy of <A href="mailto:diekmann@kde.org">Andreas C. Diekmann</a>.
<!--break-->
<p><hr></p>
<h2>Andrew Stanley-Jones on KSirc</h2>
<i>Interviewed by <A href="mailto:diekmann@kde.org">Andreas C. Diekmann</a></i>
<p></p>
<p><b>1. When did you first start to work on KSirc?  Who else is involved with this
project?</b></p>
<p>
KSirc was first released Nov 11 1997, but it really evolved in many
stages.  I had always wanted a powerful IRC client that ran under X
and provided very strong scripting support. For the past few years I
had worked on Perl/Tk, XForms and Perl/Qt based IRC clients.
</p>
<p>
By late 1997 I had given up on the notion of writing the whole client in
Perl.  The Perl/Tk or Perl/Qt libraries just didn't cut it, and the
installation hassles for users was too much.  Around September 1997
I decided to give programming Qt in C++ a shot and see what I could
learn.  As I wrote the client I started using KDE widgets since they
made my life easier and provided some nice features.
</p>
<p>
I ran into Stephan Kulow online and we started up #kde on irc.linpeople.org,
which is now irc.freenode.org.  After some time I was asked to put KSirc into
kdenetwork, and off it went.  I started getting a lot of help at that
time with code cleanup, making things more sane, etc.
</p>
<p>
In 1999 I graduated from school, moved to the US, and stopped
maintaining KSirc due to a lack of desire and personal reasons.
Essentially KSirc did everything I needed.  Work on on KSirc was done by
various KDE people for the next few years.  Names included in the
credits are Waldo Bastian, Carsten Pfeiffer, Malte Starostik, Daniel
Molkentin, and Simon Hausmann.
</p>
<p>
A lot of work was done, like porting it to Qt 2, Qt 3, and tons of
maintenance and fixes.  Simon Hausmann put a huge effort into writing a
new IRC text widget which I'm really impressed with.  I don't want to
leave anyone out, but listing the amount of work done over 3 years by
various people would be a huge task!
</p>
<p>
In late 2002 due to change in employment, or lack thereof, I started
working on KSirc again.  I've contributed some minor patches to the 3.1
tree, but the bulk of the new work are big changes to head.
</p>
<p></p>

<p><b>2. How is this project managed? Do you use KSirc to communicate with the
other project members? Do you know them personally?</b></p>
<p>
KSirc never really seemed to garner a strong group of developers working
on it like other software did.  Even when it first started I'd randomly
receive patches every once and a while.  I'm not sure if it's just due
to the style of the application or my personality developing it.
</p>
<p>
I used IRC to get a lot of feedback from people and discuss how to
implement features. Since I took up KSirc again in 2002 I've
talked extensively on IRC with Simon about his new widget and how it
works.
</p>
<p></p>

<p><b>3. What do you see as the &quot;strong points&quot; of KSirc in
        comparison to other IRC clients?</b></p>
<p>
The design of KSirc is based around a couple goals:
</p>
<p>
1. Powerful Perl scripting.  That's why the actual IRC code is written
in Perl.  The underlying code that does IRC is from an application
called sirc, which has a Perl core with an ncurses front end.  The nice
part about it is the ease of extending or enhancing the program with 
your own scripts.  sirc has a hook mechanism that really works well
with your scripts just becoming part of the IRC client.  Other features
such as timers, adding file descriptors to the select loop, etc are all
implemented providing the would-be script writer with a very nice interface.
</p>
<p>
2. Ease of use.  This means something different for KSirc when
      compared to other KDE
apps.  Since I use KSirc about 8 hours/day I need it to be easy and
fast to work with.  For example, most users of IRC in text programs are
used to typing /command.  For help they type /help. I've always found
this faster and less disruptive than having to click on a menu and then
another one, and finally getting around to a command.
</p>
<p>
This also means the application must be stable, I can't stand clients
that crash.  This was one of the leading reasons I got back into development.
</p>
<p>
3. Configurable.  The way I use KSirc may not be the same way you or
anyone else want to use the application.  At first a lot of this was
done was using scripting and filter rules, but as time and features
expanded this has meant more GUI configuration options.  It still
misses a per server configuration, but that's not far away.
</p>
<p>
The order I've given to these is the order of importance.  This does
conflict with KDE ideals sometimes, and we knew this when we started.
I never meant to write an application that looked pretty, gave you a
choice of what server and channel and ended there.  This means that
for a new user unfamiliar with Unix, networking or scripting may be
harder than it might have been.  As such I've always supported KDE
having a simple IRC client.
</p>
<p>
There are few weak points in KSirc inherent in the design.
Internationalisation is hard since the Perl backend is not written to
support it.  Even if it was, it would be Perl i18n, not the familiar KDE
one.  Since everything inside the IRC window is created by the Perl backend,
this is a lot of the messages.
</p>
<p>
The C++ front also has no easy way to request data from the Perl
backend.  There's two problems here, the C++ GUI cannot block on the
backend, and there's no way to know who's waiting for that particular
command.  For example, did the user just type /whois or was it the
application?
</p>
<p></p>
<p><b>4. What are your plans for KSirc? Any new features we may be looking
forward to?</b></p>
<p>
There's a couple areas I feel KSirc is really lacking in, the first
being the Perl scripting and its ability to interact with the GUI.  At
this time there is no way to tell the GUI to do something, set a flag,
or alert the user.
</p>
<p>
Back in 1998 I began work on a way to allow the Perl to take control of
the GUI, it was called Perl User Kontrol Environment, or PUKE.  I made a
few mistakes, and it turned out to be a lot of work.  DCOP has since
been created and seems to allow for a similar feature set. So I look
forward to enhancing both KSirc and Perl/DCOP to provide a nice
interface.
</p>
<p>
The Perl backend is showing it's age.  It was first written for Perl 4,
and Perl has gone through many changes since then making many things
much easier.  For example, the current hook implementation could be done
using code references, and data structures could be built using more
complicated types than multiple hashes.  Not sure how this will take
shape.
</p>
<p></p>
<p><b>5. KSirc is published under the &quot;Artistic
        License&quot;. What are the reasons for this? What do you
        think about &quot;Open Source&quot; in general?</b></p>
<p>
KSirc was changed from GPL to Artistic in 1998 or so.  I got fed up with
the Qt is not free discussion and all the endless junk that was going
on.  I figured the best way to show my dislike for the discussion was to
stop developing a GPL'ed application.  Perhaps no one would cared, but
it was the best I could do.
</p>
<p>
After a few years working in various companies I've started to rethink
the benefits of the GPL and have been thinking about trying to get it
switched back.  Based on the number of copyright notices in the headers
this doesn't seem like a major problem, most of the code contributed is
under the GPL.
</p>
<p></p>
<p><b>6. Is your work on KSirc sponsored in any way?</b></p>
<p>
Not directly.  No company I've ever worked for has offered to pay me to
write a client that allows you to waste time chatting online.  Wouldn't
that be nice to tell the boss, yes I have to be on IRC to allow me to
test the software.
</p>
<p>
In my new job I take the train to work, 1 hour each way.  I picked up a
laptop so I can work on KSirc.  This has really helped to move KSirc
forward over the past few months.  Features I've added on the train
include the new dock mode, improved preference controls, new DCC
interface, etc.
</p>
<p></p>
<p><b>7. Do you have any favourite KDE tool (apart from KSirc)?</b></p>
<p>
It's a pretty hard call.  For web browsing I use Konqueror almost
exclusively.  So it must be pretty high on the list.  Of course, Klipper
rates very high, but a look at the credits will answer why.  What I
don't understand is why it doesn't exist on other platforms.  The idea
of clipboard history isn't very complicated, but incredibly useful.
</p>
<p>
Other applications that get a lot of use are ksysguard, klaptopdeamon,
kwifimanager, etc.
</p>
<p></p>
<p><b>8. What should the &quot;desktop of the future&quot; look like in your opinion?
</b></p>
<p>
I really don't spend much time thinking about the desktop of the future.
I really care about an environment that allows me to work quickly and
efficiently.  I appreciate good looks as much as anyone but it can't be
at the expense of operation.
</p>
<p></p>
<p><b>9. Which OS and hardware do you use at work and at home?</b></p>
<p>
All my work is done under Linux.  At home I do development on a dual
Celeron 466 with 512M of RAM, my laptop is a 1.2 GHz PIII with 256M. Both work pretty well for KDE development.
</p>
<p>
At home I have a small network of 3 machines including an old p200 doing
routing and various services.  I talk with the laptop using 802.11b
which is working out very well.
</p>
<p></p>
<p><b>10. Could you tell us more about the person &quot;Andrew Stanley-Jones&quot;?</b></p>
<p>
Well let's see, I'm a Canadian who was lured to the US by jobs and the
promise of less snow.  All of which turned out to be true, and I haven't
seen snow in almost 4 years so I'm happy.  I graduated with a bachelors
of Electrical Engineering in 1999 and I've been doing embedded Linux
development ever since.  I got married last year to the best lady on
earth, and am having all around a good time.
</p>
<p></p>
<p><b>11. How do you spend your spare time, if any?</b></p>
<p>
Spare time tends to be split up in many areas.  Spending time with my
wife and family is very important to me.  I also spend a good part of one
day of the weekend at the local airport either fixing or flying my air
plane. Frequently the local flying organizations organize &quot;Young Eagles&quot;
events where you give school age children a flight in a small plane.  I
try to attend those any chance I get.
</p>
<p>
Since I spend 2 hours a day on the train doing KSirc, at home I don't do
much development, but more actual chatting on IRC.  I get frequent complaints
from the wife when I bring my laptop in during a movie so I can chat on
#kde-devel or what have you.  I argue it keeps me awake during a chick
flick.
</p>