---
title: "Linux Magazin: Software Modelling with Umbrello"
date:    2003-05-06
authors:
  - "sstein"
slug:    linux-magazin-software-modelling-umbrello
comments:
  - subject: "First post!"
    date: 2003-05-06
    body: "Whoo-hoo.\n\nWas anyone able to make any sense out of the English translation of that page?  I could barely understand anything it said."
    author: "Mike"
  - subject: "Re: \"(google_en)\" ??"
    date: 2003-05-06
    body: "Read some slower or reread before you post ;-)\n<p>\nHere you go Mike:\n<a href=\"http://translate.google.com/translate?u=http%3A%2F%2Fwww.linux-magazin.de%2FArtikel%2Fausgabe%2F2003%2F06%2Fumbrello%2Fumbrello.html&langpair=de%7Cen&hl=en&ie=ISO-8859-1&safe=off&prev=%2Flanguage_tools\">google_en</a>\n<p>\nAnd it is quite a good translation (IMHO)."
    author: "Come on Mike..."
  - subject: "Re: \"(google_en)\" ??"
    date: 2003-05-06
    body: "> Read some slower or reread before you post ;-) \n\nHow slow? ;-)\n\n> And it is quite a good translation (IMHO).\n\nI admit since I don't speak German I can't evaluate that, but I can try to make sense of the following in English as a particularly disturbing example...\n\n\"Again abandoned in order to return to the showing mode, it proves the input mode as amazingly tricky. To expect it would be that to everyone mouse-click outside of the text field the editing mode terminated.\"\n\nAfter rereading some things that made no sense they began to make small sense. Some, like the above text, make little sense at all. Frankly I'd rather be standing on a street corner in Frankfurt exchanging hand signals and pantomime than reading this as my mind bobs up and down trying to assemble it into a rational sentence structure. At least we could communicate on where to get a good pilsner. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: \"(google_en)\" ??"
    date: 2003-05-06
    body: "My guess:\n\n\"Again, leaving input mode in order to return to display mode proved amazingly tricky. I would have expected that clicking outside of the text field would terminate the edition mode.\"\n\nBut really, compared to babelfish, this is great work :-)\n\nFootnote: I speak no german, and english ain't my language either. Hell, I don't even understand UML!\n\n"
    author: "Roberto Alsina"
  - subject: "Re: \"(google_en)\" ??"
    date: 2003-05-07
    body: "> Footnote: I speak no german, and english ain't my language either. Hell, I don't even understand UML!\n \nROTFLMAO \nThe man who claims to know nothing speaks wisely. He's hauniting me... taunting me...\n\nRoberto, when you finish translating the translation please send me a copy. Be sure to include plenty of those witty footnotes. ;-)\n\nEric"
    author: "Eric Laffoon"
  - subject: "Re: \"(google_en)\" ??"
    date: 2003-05-10
    body: "\"To exit the edit mode again to return to the view mode is amazingly tricky. We would expect that any click outside of the text-field terminated the edit mode. Actually one must confirm changes with the [Return] key or cancel with the [ESC] key. Clicking into the help window or a double click on another structure component exits the input mode as well, although in these cases Umbrello loses any changes made.\"\n\nregards,\nchris"
    author: "The Hun"
  - subject: "Re: \"(google_en)\" ??"
    date: 2003-06-10
    body: "Not really, it is not a good translation. You cant expect to second guess the content of a technical document..."
    author: "mt"
  - subject: "Better Translation..."
    date: 2003-05-06
    body: "Hey! What is everyone complaining about the google translation?\nIMO it's pretty impressive what the google translation sometimes\ncan achieve today. Don't you think you are expecting a bit to much\nif such a complicated article is to be translated automatically??\nThis article is especially difficult because it heavily uses the ability of\nthe German language to put many parts in arbitary order into a sentence\nand then link them with grammar and sometimes only the order gives\nthe meaning. You can do that in English. Therefore, the longer a sentence\nthe more difficult is it for a computer to translate properly because it only\ntranslates words. In cannot \"translate\" a different word order into something.\nAnyway, I made a quick translation which isn't perfect because I did it in\n30 minutes and have no idea about UML but I hope it's easier to understand \nthan the google version:\n<p>\n<A href=\"http://rgpublic.tripod.com/umbrello.htm\">http://rgpublic.tripod.com/umbrello.htm</a>\n\n\n"
    author: "Jan"
  - subject: "Umbrello uml"
    date: 2003-05-06
    body: "Umbrello is the best tool for UML on Linux. I've tried to install Rational Rose on my D\u00e9bian during 4 hours. No way, it can't start. Other tool like dia are not made for UML, and that's why IMHO there are not user friendly as UML modeller.\n\nGo Umbrello !"
    author: "Gon\u00e9ri Le Bouder"
  - subject: "Re: Umbrello uml"
    date: 2003-05-07
    body: "Of course you haven't tried Poseidon (http://www.gentleware.com/products/index.php3), it's great for all kinds of UML.  And it's based off a free software product.  But it's proprietary, which is why I'm posting anonymously."
    author: "Anon"
  - subject: "Re: Umbrello uml"
    date: 2003-05-07
    body: "Why didn't give the link to the product it is based of?\n\nhttp://argouml.tigris.org/"
    author: "Jas"
  - subject: "Re: Umbrello uml"
    date: 2003-05-07
    body: "But it is very slow and not integrated into KDE desktop. Of course it has other strengths, but I think the open source community is big enough to support at least 2 UML tools.\n\nSteinchen"
    author: "Steinchen"
  - subject: "Re: Umbrello uml"
    date: 2003-05-07
    body: "I've used Umbrello, and like it a lot.\n\nSome of my co-workers can't use Linux, so I've downloaded ArgoUML for them.\n\nArgo looks terrible, but works well.\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: Umbrello uml"
    date: 2003-05-07
    body: "The probleme is that we can't export file from Umbrello to Rose/Argo/PutYourUMLEditorHere."
    author: "Gon\u00e9ri Le Bouder"
  - subject: "Re: Umbrello uml"
    date: 2003-05-07
    body: "Well, Umbrello files are save in XMI. It should be possible to share xmi files between all UML tools. But, this is only theory. In XMI there are only the modell data stored (which classes, use cases and so on). But the diagram informations are not stored in XMI (so things like where on the diagram is the class, which colour and so on). There is till now no standard how to exchange these information.\n\nMost tools save these information in the XMI file in a special section. So to import a file to another tool you need a filter which could interpret this special section. So it is very clear, that Rose/Argo or some other tools don't provide such filters for import Umbrello XMI files.\n\nThis is of course a mess. But in the upcoming UML 2.0 standard, this problem will be solved. There, diagram information will be saved as SVG. But 2.0 isn't published yet...\n\nSteinchen"
    author: "Steinchen"
  - subject: "Re: Umbrello uml"
    date: 2006-02-07
    body: "It's taken a few moons, but we're getting there: First exports\nto Argo and StarUML are starting to work, plus we are getting\nimport from Rose (beginning in version 1.5.2.)\n"
    author: "Oliver Kellogg"
  - subject: "Re: Umbrello uml"
    date: 2006-02-07
    body: "gogo umbrello , this tool is important !!!"
    author: "chris"
  - subject: "Re: Umbrello uml"
    date: 2003-05-08
    body: "Well,  lets say that it's one of the best free tools for Linux.\nIt still have a long way to go until it reaches the same class as commersiol tools like MagicDraw and ObjectDomain. \n\n"
    author: "Uno Engborg"
  - subject: "Re: Umbrello uml"
    date: 2004-01-06
    body: "I like Umbrello, but i think that the MagicDraw UML is the best tool for linux."
    author: "Daniel Torres Bonatto"
  - subject: "Very nice!"
    date: 2003-05-06
    body: "At a first try Umbrello allowed me to conveniently draw a diagram and save it and that's better than other open source UML diagramming tools I'd tried previously. At last an opportunity for me to learn UML hands-on. Great work!\n"
    author: "Case Roole"
  - subject: "UML Tools for Linux"
    date: 2003-06-04
    body: "ArgoUML is a nice tool but it's a little slow and the user interface is ugly. Poseidon is based on ArgoUML and has a cleaner user interface. Speed is about the same but maybe a bit faster. It's nice to be able to export diagrams as PNG. However, both these tools do not currently support PHP code generation to my knowledge. I know Umbrello does and this is why I'm considering trying it out. Oh yeah, one drawback to Poseidon is that it's not open source. Hence, the reason I don't want to use it. Anyway, I'm anticipating Umbrello 1.2 because it's going to include the missing deployment and component diagrams, will have bug fixes and will support code generation for many more languages. I hope more people contribute to Umbrello. We need a good enterprise UML tool that is open source and can be used to foster enterprise OSS development. It would also be nice to see Umbrello ported to different platforms to encourage its widespread use. But it's KDE-based so I don't know how much work that would take.\n\nDoes anyone know of any other UML tools that are open source and can generate PHP code?"
    author: "Anonymous"
  - subject: "Re: UML Tools for Linux"
    date: 2004-01-29
    body: "I am now using Visual Paradigm for UML and Visual Paradigm SDE for NetBeans Community Edition. It is extermly fast. The Community Edition only support real-time Java code generation. I know the Professional Edition can do code generation better. I don't know is it supports PHP. But Visual Paradigm for UML is not open source."
    author: "Anonymous "
  - subject: "Re: UML Tools for Linux"
    date: 2004-01-29
    body: " send me free Linux based UML tools"
    author: "sachin"
  - subject: "Re: UML Tools for Linux"
    date: 2004-05-10
    body: "Maybe could be interesting this page:\n\nhttp://plg.uwaterloo.ca/~migod/uml.html\n(overview)\n\nR."
    author: "Roman"
---
The German edition of <a href="http://www.linux-magazin.de/">Linux Magazin</a> (de) is currently <a href="http://www.linux-magazin.de/Artikel/ausgabe/2003/06/umbrello/umbrello.html">featuring an article</a> (<a href="http://rgpublic.tripod.com/umbrello.htm">Jan's translation</a>, <a href="http://translate.google.com/translate?u=http%3A%2F%2Fwww.linux-magazin.de%2FArtikel%2Fausgabe%2F2003%2F06%2Fumbrello%2Fumbrello.html&langpair=de%7Cen&hl=en&ie=ISO-8859-1&safe=off&prev=%2Flanguage_tools">Google's version</a>) on <a href="http://uml.sourceforge.net/">Umbrello</a>. Umbrello (<a href="http://uml.sourceforge.net/screen.php">screenshots</a>) is a UML drawing tool that will be shipped with <A href="http://developer.kde.org/development-versions/kde-3.2-features.html">KDE 3.2</a> in the coming months. The article gives the user a short introduction to the latest stable version (1.1.1), and also lists some known problems in the current code base.   So help us test <a href="http://uml.sourceforge.net/">Umbrello</a> and get rid of those last remaining bugs!
<!--break-->
