---
title: "KDevelop 3.0 (Gideon) Alpha 4 is Out"
date:    2003-04-18
authors:
  - "ctennis"
slug:    kdevelop-30-gideon-alpha-4-out
comments:
  - subject: "Very Powerful IDE"
    date: 2003-04-18
    body: "Perl, Python, C++, Java, etc...  This IDE is really getting powerful.  Templates to do almost anything in KDE and flexible enough to add whats missing.  \n\nLast night in 3 hours I added support for editing Bash Shell scripts.  Complete with a function browser, code folding and syntax highlighting via KTextEditor.  \n\nIn short people KDevelop 3.0 will be the IDE for Unix :)\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "Nice!  Oblig:  What theme are you using?  Those fonts are very blocky!"
    author: "KDE User"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-19
    body: "> What theme are you using? Those fonts are very blocky!\n\nlet me guess :)\n\nThe Window Decoration looks like the new glow from cvs.\nThe Widget style looks like qnix.\nThe font....looks somewhat like Chicago from the old MacOS.. but it's not.. looks like a derivative :)"
    author: "fault"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "And bash. Cool.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Very Powerful IDE"
    date: 2006-03-23
    body: "I know this is old news but I just want to add my thanks for the bash features  :)"
    author: "Richard Hendershot"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "could you provide a plugin to add bsh support? would be great! maybe it will be included in the official project!?"
    author: "panzi"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "hrm.. what is bsh?  I only know bash so that is all I will ever support, but its simple to add more plugins if you wish.\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "hups, did i miss the 'a' button?"
    author: "panzi"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "um, i must be confused, bash shell support is in CVS and will be a part of KDevelop 3.0.\nIs this what you want?\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "Well what kdevelop-bash.png shows is exactly what i want! :)\n\nbtw: \"ian reinhart geiser\"? Sprichst Du Deutsch? Muss ich mich nicht mit Englisch abqu\u00e4hlen! ;)\n\nPS (@all who know the answer):\nWhen will be 3.0 final anaunced? I couldn't find any planed releasedate on the website. I must not be a very exact date, only 2003 Q4 or so. "
    author: "panzi"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "Wow, cool. I was just about to post asking if it had bash support .... I might check this out, for autopackage presently I use xemacs, which is really nice, but a proper IDE might be good also.\n\nI'll have to find out if this alpha has any wierd dependancies on CVS KDE or anything, plus I need to get a bigger harddisk, i don't have enough space for kde presently."
    author: "Mike Hearn"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "Most of us run it from KDE 3.1, and some still run it on KDE 3.0.  Works like a champ."
    author: "Caleb Tennis"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-21
    body: "Works okay under fluxbox for me"
    author: "martian"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "Your mail address geiser@kde.org keeps bouncing...  feel free to look into it or turn off the \"Notify\" option when you post. :-)"
    author: "Navindra Umanee"
  - subject: "Re: Very Powerful IDE"
    date: 2003-04-18
    body: "Sorry Im scheduled to take a typing class in the summer.  Ideally that may help the situation ;)\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "KTouch"
    date: 2003-04-19
    body: "> Sorry Im scheduled to take a typing class in \n> the summer. Ideally that may help the situation ;)\n \nSo KTouch isn't good enough for you?!?\n"
    author: "Mike"
  - subject: "fp!"
    date: 2003-04-18
    body: "thanks Derek!"
    author: "fp!"
  - subject: "Re: fp!"
    date: 2003-04-18
    body: "You read the story, yes?"
    author: "Anonymous"
  - subject: "he was kidding"
    date: 2003-04-18
    body: "Because so many CVS-Digests were posted by Derek, he was pretending its a habbit."
    author: "Mario"
  - subject: "Development Tools"
    date: 2003-04-18
    body: "Kdevelop is perhaps the MOST critical application for Linux's long term success. It is currently the best, most fexible, customizable and comprehensive open source IDE available for Linux today. \n\nWhy is this marvellous piece of engineering so important for Linux's future? For the same reason most peope no longer program in lowlevel languages, and instead choose Java and C++. Work can be done faster, with fewer bugs, and an easiet to maintain codbase. In effect, the better tools create better applications in approximately the same time. If there was to be such a gap between linux's development tools and the tools of other OSs we would more than make up for our smaller development team, by letting them be much more productive. \n\nAs the development tools will become more refined so will the applications, the rate of progress and eventually Linux's popularity. One of Linux's highest priorities is to have the best development enviroment/framework available, getting this set up will expotentially increase the number of developers and the quality applications available and so will the demand. Kdevelop needs to be even better than MS's latest and greatest without the lock-in of course.\n\nThere already are some very nice tools, for example\n\nKDevelop \nCervisia \nKbugbuster \nKompare \nKate\nUmbrello \nQtDesigner \nQt Linguist \nKdbg \nQSA (scripting toolkit, not yet released) \nGLADE\nKylix \n\nand many more. but, most of these applications are not better than equivalents on other platforms or have a les suser friendly interface. \n\nIn my firm opinion improving Linux's tools will create a much stronger foundation and produce better applications faster. Just like it would not be wiseto do most of your development on a 300 MGZ computer and upgrade to a 2 GHZ computer after you've already finished most of your work, it is not wise for Linux to have such heavy work going into other applications before the tools are \"upgraded\". Kdevelop needs a larger team, even though its current team has already shown to be very talented and has done an amazing large of work on Kdevelop, which often teams quite as big probably would not achieve in the same time frame. Still, to effectively have the best tools for any platform quicker Kdevelop should have a team at least as big as Konqueror's. Other development projects like GCC, the ons mentioned above, etc. should also get a far bigger team.\n\nThanks for listening, I really hope I did not upset anyone. This is just my opinion from quite a lot of research. Thanks for reading it."
    author: "Mario"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "> needs a larger team\n\nThanks for providing the people and/or you helping out..."
    author: "me"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "However, one problem is that the tool may be not \"that easy\", too feature bloathed for beginners. Is there a \"Learning C++ with KDevelop\" Guide? No! \n\nYou want to write an easy application, say \"hello world\", and --hmm, why are there so many files generated by the wizard?\n\nWhat Linux really needs is a programming tool like (in back in 1991) TP 6.0 and its easy IDE. (Today there is of course rhide, a IDE clone) Or VisualBasic 6.\n\nThe second tool is targeted, progress has been made: \nhttp://hbasic.sf.net \nhttp://gambas.sf.net  \n\nFuture Kdevelop for instance :\n- integration with other tools\n- improved documentation\n- more languages\n- SF access/automatic website/automatic announcements\n- bugzilla integration"
    author: "Hakenuk"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "There will be better documentation coming this summer.  Look for a published book on KDevelop within a year."
    author: "Caleb Tennis"
  - subject: "Re: Development Tools"
    date: 2003-04-19
    body: "Cool, I'm already looking forward to reading it. =)"
    author: "Datschge"
  - subject: "Re: Development Tools"
    date: 2003-04-19
    body: "\"Look for a published book on KDevelop within a year.\"\n\nThis is great news!!!  I will buy it immediately after it becomes available!\n\nThanks also for adding Fortran support.  Some of us number-crunching dinosaurs still use it!  :-)\n\nIMHO, KDevelop is the killer-app (or one of them, anyway) for KDE and Linux.\n\n-Robert"
    author: "Robert"
  - subject: "KDevelop + Knoppix?"
    date: 2003-04-19
    body: "Just a thought - one could write a tutorial style \"Learn C++\" \nbook, and bundle it with KDevelop + Knoppix, so students could\neven run it on systems (e.g. school labs?) without Linux installed :-)"
    author: "Steve"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "There is no wizard to magicly make you know how to program.  While VB may be an easy to learn language, it really is no harder than PHP, python or C++.\n\nWhat are have provided here is a powerful tool to develop code, if you need a click and drop language Id suggest LabView, but even then you will need some sort of programming skill..\n\nIn short Id  suggest you avoid programming as much as possible because if you cannot learn something as simple as python or bash then you this is not for you.\n\nCheers\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "Or if you don't like LabView try VEEPro (formerly known as HP VEE) from Agilent.  VEE Pro has the same scope as LabView, but the instrumentation/automation part has a less dominant role in the IDE. It has a slightly more generalpurpose feel. It's sometimes nice to draw programs :-)\n\n"
    author: "Morty"
  - subject: "Re: Development Tools"
    date: 2003-04-20
    body: "You will find that LabVIEW and HP VEE are two very different programming languages. HP VEE is works like a visual \"c\" like language, almost Schneidermann in a way.\n\nLabVIEW is built for DAQ and is highly advanced for many things. It is not just drawing a program. You can do advanced stuff in little time. I did a quick IRC client (with finger service) in less than 2 hours :-)\n\nLabVIEW is available for all platforms. The user interface is just a bit \"ugly\" if you don\u00b4t know how to work around it.\n\nMy experience? I have been programming both languages and there is more than \"drawing\" to both of them. We dumped HP VEE in the company because of lack of support and got into LabVIEW. No regrets there! \n\n"
    author: "Jens"
  - subject: "Re: Development Tools"
    date: 2003-04-30
    body: "Hi Jens, \n\nyou dumped the wrong product. Lab View is by far more complicated\nto use than VEE. "
    author: "Willi Korel"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "I agree, somewhat - it may be a bit difficult for beginners.  The idea here is that we're attempting to provide an IDE that encompasses what many different types of projects need.  If you're going to write a simple \"hello world\" app, KDevelop is probably much more sophisticated than what you need.  Similiarly, if I wanted to write a 5 line text file, I probably don't need KWord.<br>\nInstead, we are attempting to give a tool that allows the management of various unix project languages of varying complexities.  Since everyone seems to have different coding styles, different directory layout preferences, and different interface needs, it's a far from trivial task.  <br>\nLuckily, all of the parts you mentioned are relatively simple to add to KDevelop, thanks to the plugin nature of the addons.  All it takes is for someone to write them.\n"
    author: "Caleb Tennis"
  - subject: "Re: Development Tools"
    date: 2003-04-20
    body: "Not necessary. Look at the Qt documentation / Qt designer doc. Libs and tool are\nvery powerful, but you can get it quickly when working through the tutorial. \nI think that the Qt doc is what KDevelop/kdelibs needs too, integrated in KDevelop itself (via the KDE help system)."
    author: "Ruediger Knoerig"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "> It is currently the best, most fexible, customizable and comprehensive open source IDE available for Linux today. \n \nYou don't know Eclipse, or?"
    author: "Anonymous"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "I've never used Eclipse, but from what I can tell it's mostly Java based, and it seems like it's really nebulus.  But it's got some good corporate sponsors, so its development is probably very mature."
    author: "Caleb Tennis"
  - subject: "Re: Development Tools"
    date: 2003-04-18
    body: "Sure Eclipse has its roots in Java. But http://www.eclipse.org/cdt/ exists and they plan e.g. refactoring support for C++ in 2003."
    author: "Anonymous"
  - subject: "Re: Development Tools"
    date: 2003-04-19
    body: "And when do they plan to finally add support for working autocompletion and intellisense-like features for the C++ part?\n\n(wasn't that going to be added 2 years ago?)\n"
    author: "nac"
  - subject: "Re: Development Tools"
    date: 2003-04-19
    body: "After that long list of tools and the long essay on what will make a difference it only seems right to thump a little. Quanta+ may only be a web tool but it's a very good one and progressing quickly. Web developers are an early adopter audience so Quanta can be very significant to Linux/KDE if a number of web developers choose to adopt it. Additionally Kommander may mot be a full blown application tool but it is a great user tool to extend development. In the end applications are useless unless you can customize them to do what you want. Kommander brings universal end user customization which is the next killer app. One should not assume a purely traditional approach can be universally successful. \n\nOne last thing... I'm not speaking from a hypothetical standpoint and saying \"this program needs these resources\". I'm actively working on the program. All the expositing in the world doesn't write a single line of code. If there's one thing cool about KDE it's that most core developers are too busy coding to pontificate. What can *you* do?"
    author: "Eric Laffoon"
  - subject: "Re: Development Tools"
    date: 2003-04-19
    body: "I just thought it interesting to point out that Andras Mantia uses Gideon for his Quanta+ development. :)"
    author: "teatime"
  - subject: "Re: Development Tools"
    date: 2003-04-21
    body: "> I just thought it interesting to point out that Andras Mantia uses Gideon for his Quanta+ development. :)\n\nWe all do. As of about a month ago it's the default IDE for Quanta development and it does some things a lot better than the 2X version like not being so dependent on it's own internal information. We used to have more issues with makefiles and what 2x would over write if you just wanted to do a quick edit just to name one factor.\n\nHowever the more serious point is that while good tools like Gideon make a big difference in delivering applications, and it is a really good tool, there are a very small fraction of any total user base that will develop applications. Also there are lots of features in professional tools that Gideon does not yet outshine. What makes it appealing is the total package, the great API for KDE and the potential of the tool.\n\nThe thing is that the traditional argument of good tools producing a better app and garnering market share can be seen to be lacking by looking at Mozilla vs IE. Applications have to dramatically transcend conventional design thinking and offer a lot more as well as be widely used to make a difference in broad platform acceptance. That is my point, that Quanta can reach more early adopters in a more compelling way... not to take anything away from how good Gideon is. That was my contention with the other post. Just better than before and more stable is not as compelling as \"If you want this feature set you need to be running KDE\" which is our year end target for Quanta+."
    author: "Eric Laffoon"
  - subject: "Re: Development Tools"
    date: 2003-04-21
    body: "Sure I do! I think it's the best IDE for Linux, and quickly becoming the best IDE I've ever seen. \nYeah, it's complex and highly configurable, and there are tons of features I don't use, but I don't think it's confusing. I find it easy to use (ok, I'm not a newbie, but with a good tutorial, book, it should be easy also for newbies).  \n The only \"problem\" with it is that it's software under development and to get the best out of it, you need to update from CVS from time to time. Or you have to fix the bugs yourself. ;-) \nAnyway, thanks for the developers and those who contributed to KDevelop. If I would have more lives (or more a day would contain more than 24 hours), I think would be also a KDevelop developer. :-) But Quanta+ has the priority and expect more from it. Altough it was not present in the last CVS digest, the autocompletion was improved, it more customizable  and works also for PHP classes (enter \"new \" and the possible class names will appear). And \nCSS autocompletion is close to be ready.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Can't use it yet unfortunately"
    date: 2003-04-18
    body: "I've been developed rather complex applications in Windows before but\nsince I've switched to KDE / Linux I had to stop it for a while. Though\nI have a lot of ideas for useful things I want to write it all comes down\nto the problem that I'm not able to insert some menu items. I asked\nthe question multiple times in the Kdevelop forum but noone was\nable to help me. Without a proper menu you can forget app development\nat least on a professional level as I want a toolbar and menubar like\nall other standard KDE apps. I've read all the documentation I could find\non the internet about XML menus and stuff but to no avail.  I'm repeating \nmy question here because this really keeps from writing apps for KDE.\nSo my question is:\n\nThe default KDE framework application in Gideon contains\n the following llines of source code:\n ---------------------------------------------------------------------------------\n // this doesn't do anything useful. it's just here to illustrate\n // how to insert a custom menu and menu item\n KAction *custom = new KAction(i18n(\"Cus&tom Menuitem\"), 0,\n this, SLOT(optionsPreferences()),\n actionCollection(), \"custom_action\")\n ---------------------------------------------------------------------------------\n \n However when I run the sample application this menu item is not\n shown even though an XML file demoui.rc seems to exist. Ok, it says\n\"Doesnt do anything useful\", but what is necessary so that it _does_\nanything useful i.e. to make this item really appear in the menubar / toolbar?\n I already tried calling createGUI with the rc file as parameter \n and I tried to call custom->plug(menuBar()); but to no avail.\nPerhaps it something simple but I have tried for quite some time and haven't\nfigured it out yet.\n\n "
    author: "Ras"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-18
    body: "Did you install the application (make install) into a the kde directory structure?  If not, you'll need to update your kde settings to point to this project directory so the XMLGUI stuff knows where to find the .rc file.\n\nTry running \"make install\" and see if this problems goes away."
    author: "Caleb Tennis"
  - subject: "Sorry"
    date: 2003-04-18
    body: "I was not aware of Eclipse, it may be better or worse, I do not know. I was calling it the best out of what I've tried. I will give Eclipse a shot sometime too.\n\nAnd me,\n\n\"> needs a larger team\n\nThanks for providing the people and/or you helping out...\n\"\n\nActually, I just started learning C++ now. I'm only on chapter 2 of the \"C++ How To Program\" book, but once I am proficent in C++. I plan on learning Qt 4.x too (by than it will be out) and using my skills to help OSS projects like Kdevelop.\n\nI also know that a very complex IDE may be too daunting for beginners, even thouugh I am comfortable with Kdevelop 2.1.5, that's why maybe there should be a \"Beginner or Learning Mode\" and \"Advanced\" mode. This would ease the learning curve signficantly, taking it one step at a time.\n\nI also agree that KDevelop will need better documentation and its website should also be updated, there is very old information on it, which is no longer valid in some areas. IMO, it should adopt the KDE.org design like Konqueror. The new color-scheme is already a nice touch.\n\n"
    author: "Mario"
  - subject: "Looked for Eclipse Info"
    date: 2003-04-18
    body: "Eclipse seems to be really good from what I've read, and it also has some nice tools like the CVS Diff tool, which I have not seen integrated well into Kdevelop. Here is some info about it here too: http://www.osnews.com/comment.php?news_id=2036&offset=0&rows=15 ( old new sitem, but info still true)\n\nAnyway, I have not actually tried it yet, but it sounds good and I'm sure their team and the Kdevelop team can learn a lot from both projects.\n\nIts nice to see more quality IDE's like Kdevelop, Eclipse and Anjuta (not quite that good yet, but is improving) on linux.\n\nThanks for pointing it out to me.\n\nBTW: This is OT, but is there any chance Kdevelop's website might mighrate to PHP Nuke 6.5 or 7 when its released. It would help its popularity a lot, much more interactive and PHPBB are soo much better than the current forums. I have a friend which I think might know how to set up PHP nuke for you too. But, I'm not sure, he could set up Ikonboard forums though/."
    author: "Mario"
  - subject: "Re: Looked for Eclipse Info"
    date: 2003-04-18
    body: "Also see http://download.eclipse.org/downloads/drops/R-2.1-200303272130/whats-new.html  for a nice overview and screenshots of new features in Eclipse 2.1."
    author: "Anonymous"
  - subject: "Re: Sorry"
    date: 2003-04-19
    body: "How comes these comments are here and not one tree further above?"
    author: "Datschge"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-20
    body: "Thanks a lot!!!!! Finally it works. At least I can make simple apps right now.\nStill I have to figure out how to put my menu item in the toolbar too\nbut I hope I can find that out alone after some more trying... ;-)\n"
    author: "Ras"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-19
    body: "you need an item in your ui.rc file for \"custom_action\" (and any other custom actions you want to show up) and you need to create all the actions before you call createGUI() ... your ui.rc file also has to be somewhere the app can find it, which usually means doing a make install or passing a hardcoded a path to the file to createGUI. if you use any standard actions (e.g. KStdAction::open(..) for File->Open), they'll appear automatically in the right place(s) once you call createGUI, assuming you haven't overriden the merging in your ui.rc file...\n\nthe best way i can recommend to figure this stuff out is to visit the XMLUI Howto on developer.kde.org (http://developer.kde.org/documentation/tutorials/xmlui/preface.html) and then take a look at some of the ui.rc files for other KDE programs.. it's pretty simple once you've looked at a couple of examples."
    author: "Aaron J. Seigo"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-19
    body: "I generally do this:\n\n    #ifdef DEBUG\n    createGUI(\"~/comp/progdir/src/progui.rc\");\n    #else\n    createGUI(\"progui.rc\");\n    #endif\n\nOut of interest, does anyone know a function that returns the directory of the running executable in order to avoid having to hardcode the path?"
    author: "nic"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-19
    body: "man 3 getcwd"
    author: "rjw"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-21
    body: "> man 3 getcwd\n\nNot so simple, unfortunately.  A running image's \"current working directory\" does not have to be the same directory that it was launched from.  I spend most of my time in a subdirectory of my home directory, but *very* few of the apps I run come from there.\n\nI'd be very interested in the answer, too!\n"
    author: "Andy Marchewka"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-23
    body: "Can often be found from argv[0] although I don't think it's particularly cross-platform."
    author: "Andy Parkins"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2003-04-20
    body: "I had this problem until I put the absolute path in for the demoui.rc file.  I don't know why this had to be done but it fixed the problem for me.\n"
    author: "Jason Ormes"
  - subject: "Re: Can't use it yet unfortunately"
    date: 2004-04-26
    body: "I'm converting my projects from KDE 2.2 where I used to get away with really simple calls to QT, e.g.\n\nKMenuBar *menubar= menuBar();\nQPopupMenu *popup=new QPopupMenu();\npopup->insertItem(\"Item\", this, SLOT(slotMySlot()));\nmenubar->insertItem(\"My Menu\", popup);\n\nIt appears this still works in KDE 3.2, but of course you don't get user-customizable menus.\n\n"
    author: "Alex Fielding"
  - subject: "Debian dislikes Gideon"
    date: 2003-04-20
    body: "Unpacking gideon-data (from .../gideon-data_1%3a2.999+cvs20030411_all.deb) ...\ndpkg: error processing /var/cache/apt/archives/gideon-data_1%3a2.999+cvs20030411_all.deb (--unpack):\n trying to overwrite `/usr/share/icons/hicolor/16x16/apps/kdevelop.png', which is also in package kdevelop-data\n\nThe problem seems obvious, but not the solution -- to me anyway. Can I somehow force it to install even though the old KDevelop and Gideon share this image file? Any suggestions appreciated!"
    author: "EUtopian"
  - subject: "Re: Debian dislikes Gideon"
    date: 2003-04-20
    body: "Uninstall KDevelop?"
    author: "jimbo"
  - subject: "Re: Debian dislikes Gideon"
    date: 2003-04-20
    body: "I was thinking about that solution but I prefer not to, since I don't know whether or not I will be able to use Gideon in place of the older KDevelop for my projects. Particularly since it's just alpha yet."
    author: "EUtopian"
  - subject: "Re: Debian dislikes Gideon"
    date: 2003-04-20
    body: "Ohww... I remember these fights with dpkg very well...\nSimply build it from source (works great) or use one of the rpm's.\nsource:\ndl, tar xzvf **.tar.gz,cd **,configure, make && make install"
    author: "Ruediger Knoerig"
  - subject: "Re: Debian dislikes Gideon"
    date: 2003-04-21
    body: "Thanks for the advice. I tried compiling it properly and it seemed to work (no errors or anything), but when I tried to launch it I got the same error as I got when I installed from a Debian package:\n\n---\nUnable to find plugins, KDevelop won't work properly!\nPlease make sure that KDevelop is installed in your KDE directory, otherwise you have to add KDevelop's installation path to the environment variable KDEDIRS and run kbuildsycoca. Restart KDevelop afterwards.\n\nExample for BASH users:\n\nexport KDEDIRS=/path/to/gideon:$KDEDIRS && kbuildsycoca\n---\n\nSo I did the following:\n\nexport KDEDIRS=/usr/bin/gideon:$KDEDIRS\n\nThen when I ran kbuildsycoca, I got:\nWarning: kbuildsycoca is unable to register with DCOP.\n\nWhen I launched Gideon after that I got the same problem as always, ie it complains about not loading plugins. Without those plugins, KDevelop seems like a very empty environment where I can't do much of anything.\n\nAny ideas?"
    author: "EUtopian"
  - subject: "Re: Debian dislikes Gideon"
    date: 2003-04-21
    body: "Your \"KDEDIRS=/usr/bin/gideon:$KDEDIRS\" is incorrect\n\nInstead, point it to the base directory where you told the whole gideon installation to go.  For me it was /usr/kde/3.1\n\nIt looks like for you it may have just been /usr\n\n(If you have the source still, you can go to that directory and type \"./configure --help\" and see what the --prefix directory is set to.  That's your installation directory).\n\nCaleb\n\n\n\n"
    author: "Caleb Tennis"
  - subject: "Re: Debian dislikes Gideon"
    date: 2003-04-21
    body: "I checked configure to find out the dir, and that did the trick for me.\n\nI had tried all sorts of directories and was going nuts. Thanks!!!"
    author: "EUtopian"
  - subject: "Re: Debian dislikes Gideon"
    date: 2004-02-03
    body: "Which directory was it? How can i check it out in my configure? Thanks"
    author: "Tasio"
  - subject: "Re: Debian dislikes Gideon"
    date: 2003-04-27
    body: "Just do the following:\ndpkg -i --force-overwrite /var/cache/apt/archive/gideon*.deb \nthis will overwrite any existing file, worked for me.\n\nMike"
    author: "Mike"
  - subject: "What is needed.."
    date: 2003-04-20
    body: "..to get the Java application templates to compile?\n\nI still have yet to ever make those work.. :("
    author: "bcore"
  - subject: "Failure because of `AM_PROG_LIBTOOL'"
    date: 2003-04-21
    body: "When I try to run most of the project types available, I get the following:\n\naclocal: configure.in: 8: macro `AM_PROG_LIBTOOL' not found in library\n\nAnd it all stops, and I don't get a makefile. Any ideas? I run Debian, and a Gideon compiled from source.\n"
    author: "EUtopian"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2003-04-22
    body: "Upgrade your autoconf and automake.  You're probably doing 2.13 and you need 2.5x (2.57, say).\n\nHope that helps.\n"
    author: "SB"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2003-04-27
    body: "Thanks for your reply.\n\nMy autoconf is already 2.57. My automake is 1.4-p6. apt-get says it's already the newest version (I'm using Debian unstable)."
    author: "EUtopian"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2004-09-09
    body: "Debian solution:\n\nFollowing the solution for Red Hat below, locate the package providing libtool.m4. It is \"libtool\".\n\nThen: apt-get install libtool\n"
    author: "Martin"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2004-02-25
    body: "I have the same problem with RedHat9 when I try to compile the default Hello World application in C++.\n\nInfo:\nI have compiled kde3.2 and kdevelop with konstruct last weekend.\nautomake (GNU automake) 1.8.2\nautoconf (GNU Autoconf) 2.59\n\nAnother problem, the CVS init fail with Kdevelop, but the same command line is working inside Konsole."
    author: "Yan Morin"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2004-02-25
    body: "Ok, this problem was easy to resolve.\n\nthe command \"aclocal --print-ac-dir\" should return the path in with\nthe file \"libtool.m4\" is supposed to be.\n\nIt wasn't on my system. I've just added a symbolic link (ls -n) to it.\nAnd tadam!\n\nThanks to this post:\nhttp://sources.redhat.com/ml/automake/2002-02/msg00037.html"
    author: "Yan Morin"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2004-10-29
    body: "It was OK for me to just remove the problematic line from configure.in. My simple application needed no libtool anyway, so there is no use of configuring it."
    author: "Stephen Kolaroff"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2005-10-09
    body: "In my case, the libtool in not even installed on my System. (I am using Kubuntu 5.10 preview) I just have to install the libtool, then the problem was fixed\neg.\napt-get install libtool\n"
    author: "Freeman2500"
  - subject: "Re: Failure because of `AM_PROG_LIBTOOL'"
    date: 2006-03-25
    body: "just a quick jot...\n\nmanually installing any of the programs involved might cause this... \n\nif aclocal --print-ac-dir\n\nreturns something like /usr/local/share/aclocal you should probably delete your manually installed aclocal and automake stuff from /usr/local (especially /usr/local/bin).\n\nObviously if you don't know what you are doing (like me) you can get in trouble.  Anyway, deleting the aclocal files from that directory and apt-get install ... fixed it for me."
    author: "Matthew"
---
The <a href="http://www.kdevelop.org/">KDevelop team</a> announces the availablility of <a href="http://www.kdevelop.org/graphics/pic_corner/gideon-3.0.png">KDevelop 3.0</a> Alpha 4a (yes, 4a).  In the more than 3 months since the last release, many new features have been added, bugs have been squashed, and existing features have been refined and polished.  Downloads are available via the KDE <a href="http://download.kde.org">mirrors</a>.  Direct links to packages can be found at <a href="http://www.kdevelop.org/index.html?filename=download.html">the KDevelop download page</a>.
<!--break-->
<p>
Since the third alpha release, the developers have been extremely busy.  Some of the noted improvements are:<p>
<ul>
<li>New File Creation Interface, standardized across all of KDevelop </li>
<li>New Search/Replace Part for multiple files, including regexps</li>
<li>New Application Templates - kdehello, javahello, qtopia, fortran</li>
<li>Make Output View verbosity configuration</li>
<li>Added Fortran, Ruby, and tmake Project Support</li>
<li>New Class Wizard added features (inc. Make Member support)</li> 
<li>Better documentation browsing, XML support </li>
<li>Rewritten Classparser code -- faster and more compact </li>
<li>DCOP interface for FilterParts and CppParts - can trigger events via DCOP</li> 
<li>Most non C++ (Perl, PHP, Java, etc.) project managers improved</li> 
<li>Vastly improved Integrated Doxygen Support </li>
<li>Debugger integration improved</li>
<li>Numerous bug fixes (debugger, IDEAl mode layouts, crashes)</li>
</ul>
<p>
And, the development doesn't stop here!  Look soon for better persistent class store support, and smarter code completion -- already in CVS!
<p>
<strong>Your feedback is welcome.</strong>
The developers would love your feedback, from positive comments to concerns and wishlist items.  Messages in this forum, to <a href="mailto:kdevelop-devel@kdevelop.org">our mailing list</a> or on irc.kde.org, channel #kdevelop, are all appreciated!