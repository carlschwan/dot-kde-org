---
title: "KDE-CVS-Digest for September 12, 2003"
date:    2003-09-13
authors:
  - "dkite"
slug:    kde-cvs-digest-september-12-2003
comments:
  - subject: "great work"
    date: 2003-09-13
    body: "congratulations, an other nice cvs-digest\n\nthanks"
    author: "tetra"
  - subject: "ksvg"
    date: 2003-09-13
    body: "great that it's finally done.. woot."
    author: "anon"
  - subject: "FanMail!"
    date: 2003-09-13
    body: "Thank you *so* much guys! It is really great to read about all the exiting new stuff.. but even more exiting to read about all the bugfixes ;-)\n\nI am especially pleased that kpovmodeler is being worked on.. not to mention ksvg!\n\nSo... *big* kudos to the 181 developers who made this weeks improvements a reality! [insert huge applause here]\n\n/kidcat\n__\nThose who can: develop\nThose who can't: give moral/finacial support"
    author: "kidcat"
  - subject: "Re: FanMail!"
    date: 2003-09-14
    body: ">Those who can: develop\n>Those who can't: give moral/finacial support\n\nThere's a lot more to do for \"those who can't\".\n\nhttp://kde.org/support/ suggests:\n\n<snip from http://kde.org/support/>\nContributing: \n[ Artwork & Icons | Enterprise Reports | Usability Studies ] \n\nParticipating: \n[ KDE News | KDE Traffic | Promotion | Bug Hunting | Documentation | Translation ] \n</ snip>\n\nBut I got some more suggestion for \"those who can't\":\n1. use\n2. interrest oneself\n3. implement\n4. learn to develop\n\nDo you have got more suggestions; please put them in this thread!\n\n\n>ksvg!\nIndeed this will rock! A lot of kapps will benefit from this -- not you mention the eyecandy that it will bring forward. Hopefully karbon14 will get a boost up after ksvg stabelizes (sorry i'm in the: learn to develop (4) catagorie, i can't do it myself)."
    author: "cies"
  - subject: "Re: FanMail!"
    date: 2003-09-14
    body: "> Those who can: develop\n> Those who can't: give moral/finacial support\n\n:-)\nThat's cute. It made me smile. Don't forget... \n\nThose who couldn't but now can (that would be me) somehow seem to end up doing all of the above and more\nThose who can't do C++ can do XML language support, templates, artwork, web work, translations, CVS digests...\n\nNot to be critical, because short phrases can never be comprehensive and I really like yours! As much as Quanta consumes me I wish I could do more, but I like to eat too much. ;-)"
    author: "Eric Laffoon"
  - subject: "Kwin rewrite?"
    date: 2003-09-13
    body: "I never knew about this. What is the main reason it is being rewritten? Speed? Features? Both?\n\nOh and it's great news KDevelop now has code completion, that's an essential feature for me. \n\nAlso, I think that KDE should have fewer planned features for 0.1 releases, it just takes too long, 3.2, shouldn't take more than 7 months IMO and it will most likely be ready in early 2004. Now a 1.0 release that's a true feature release and that should take about a year IMO. Ia lso want to know why KDE no longer has long freeze periods, I think it would be less buggy if it did, last year, the Alpha did not have 5,000 bugs like this one has. \n\nAnyway, if all the feautres in the 3.2 releae plan are accomplished and 3.2 gets at least below 2,500 bugs, 3.2 is looking like an amazing release!\n\nThanks KDE developers and Derek!"
    author: "Adi"
  - subject: "Re: Kwin rewrite?"
    date: 2003-09-13
    body: "Compliance with standards.\n\nhttp://www.freedesktop.org/standards/wm-spec/\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Kwin rewrite?"
    date: 2003-09-13
    body: "> it just takes too long, 3.2, shouldn't take more than 7 months IMO and it will most likely be ready in early 2004.\n\n3.2 will be out in early december most likely. 3.1 was horribly delayed in order to stabalize a little bit buggy RC and beta releases. 3.2 alpha1, in relation, is MUCH more stable already. "
    author: "anon"
  - subject: "Re: Kwin rewrite?"
    date: 2003-09-13
    body: ">>Ia lso want to know why KDE no longer has long freeze periods, I think it would be less buggy if it did, last year, <<\n\nUnlikely, the last time KDE had a very long feature freeze (3.0? 3.1? cant remember) this just caused people to start dozens of branches all over the repository so they could work on new stuff. \nMost people fix bugs because they annoy them, not because they are in a feature freeze. If the software is reasonably stable they will just go on and extend it, because then features become more important than extra-reliability.\n\n"
    author: "Tim Jansen"
  - subject: "Re: Kwin rewrite?"
    date: 2003-09-14
    body: "> I never knew about this. What is the main reason it is being rewritten? Speed? Features? Both?\n\n It's not really being rewritten (i.e. it's not all code dropped and started from scratch). I actually don't think I ever called it a rewrite. It's simply being normally developed, and it's branched in order not to get too many angry mails from people running CVS HEAD ;) - e.g. after the Nove Hrady conference, the only working window \"decoration\" was a thick red border with a grey square acting as the close button, and it also locked up from time to time. However, there are many places in kwin_iii that have been rewritten in order to clean it up (so cleaning up the KWin core was the reason). This in turn allows fixing many bugs (I even had kwin bug less than #20000 that simply wasn't possible to fix before), and implementing new features, both that will come in KDE3.2 and that will come later. One of the \"features\" should be also updating KWin's and in general KDE's support for the EWMH (a.k.a. NETWM) window manager spec, and \"official\" support for running KDE with other window manager than KWin.\n Important part of the rewrite (heh, ok, looks I called it sometimes a rewrite after all) was the new API for the decoration styles, as the old one has probably never meant to be public API (very closely coupled with all internals of KWin core, so the whole application had to stay binary compatible - acceptable for libraries, but annoying for application, as it was limiting the development). This has the unfortunate effect that old decoration styles won't work with KWin in KDE3.2, but the other possibility was limited KWin development for several KDE releases (note that the decision about kwin_iii came about a year ago, when absolutely nothing was known about KDE4). The new decorations API should be properly documented, and there should be hopefully also some porting HOWTO - decoration developers can join the kwin@kde.org mailing list if they have questions. Decorations in KDE CVS are currently being ported, and the whole branch should be merged back in CVS HEAD next week."
    author: "Lubos Lunak"
  - subject: "KWin deco preview  .. GREAT"
    date: 2003-09-13
    body: "\"\"Lubo\u009a Lun\u00e1k committed a change to kwin_iii: kdebase/kwin\nDecoration previews work now, more or less.\"\"\n\ndoes that mean we can preview deco in the control planel like styles? if yes then great :)\n\n\nI've read that kwin is bieng rewritten , right? so, will old kwin deco will work with this new version without change or should be updated ?\n\n "
    author: "Mohasr"
  - subject: "Re: KWin deco preview  .. GREAT"
    date: 2003-09-13
    body: "sorry when I wrote my post the message \"Kwin rewrite?\" wasn't there , I'm sory if there is any duplication "
    author: "Mohasr"
  - subject: "Re: KWin deco preview  .. GREAT"
    date: 2003-09-13
    body: ">  does that mean we can preview deco in the control planel like styles? if yes then great :)\n \nYes\n\n> I've read that kwin is bieng rewritten , right? so, will old kwin deco will work with this new version without change or should be updated ?\n \nIt has to be updated, but it's not a terrible amount of work and probably can be done by the author of the deco in a few hours or less. Many of the current ones in KDE have/are being updated in a matter of a few days. "
    author: "anon"
  - subject: "Awesome"
    date: 2003-09-13
    body: "Does this mean taht it no longer shows the KDE 1 preview for styles also?!! If it doesen't than this bug should be fixed: http://bugs.kde.org/show_bug.cgi?id=32101"
    author: "Adi"
  - subject: "Re: Awesome"
    date: 2003-09-14
    body: "> If it doesen't than this bug should be fixed: http://bugs.kde.org/show_bug.cgi?id=32101\n\nIt will probably this week :)"
    author: "anon"
  - subject: "KWin + (x)kill"
    date: 2003-09-13
    body: "When an application locks up when running under GNOME2/Metacity, Metacity will automatically offer the user (after a few seconds of no activity in the app) to kill the application.\n\nI've found this to be a really handy feature.\nNot that I'm using lots of crashy apps, but if it happens it's good to have.\n\nAnyway, will KWin get this feature as well?\n(some day?)\n"
    author: "lalala"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-13
    body: "Why make this a function of the window manager? Try KDE's \"Runaway Process Catcher\" applet. :-)"
    author: "Anonymous"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-13
    body: "This has been a part of KDE since KDE 2.0 afaik. It's just not enabled by default for a long time because people on slower computers used to complain that it got activitated a lot."
    author: "anon"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-13
    body: "Do you also know how to enable this feature?\n\n..and isn't it time-based? Iis it just a hack like in many other places?\n(even Qt was doing it wrong before 3.2)\n"
    author: "lalala"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-13
    body: "The gnome runaway catcher works better because it only gets activated when the close button is clicked. Last time I used the KDE runaway catcher, it didn't behave that way. It's problably easier too, if you implement it in the window manager. (The user clicked the close button but the application is still not closed (after n seconds) let's ask the user if he wants to kill it.)"
    author: "Vincent"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-13
    body: "It can be made such that if the app does close, then the dialog offering to kill it could go away. Whilst I am all for less bloat, environments like KDE are not meant for lightweight systems anymore. Why should users of heavy systems have to suffer because some people still want to use Pentium 100s. Next year I plan to get an AMD Athlon XP 3000+ and there are going to be more like me, and less with slower systems. I think it is time to move on and make things like this the default."
    author: "Maynard"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-14
    body: "I always use CTRL ALT X and click, but I can see how this XP style feature could be useful. I'm for it."
    author: "Adi"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-14
    body: "Ctrl-Alt-X doesn't do anything here.  \nCtrl-Alt-ESC and clicking works for me though..."
    author: "cm"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-14
    body: "I believe kwin_iii will have support for NETWM_PING which is what you are referring to: http://www.freedesktop.org/standards/wm-spec/1.3-onehtml/#id3224238"
    author: "Hamish Rodda"
  - subject: "Re: KWin + (x)kill"
    date: 2003-09-14
    body: "Yes, that's exactly what I meant :)"
    author: "lalala"
  - subject: "Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "How does it compar to kghostview?"
    author: "TomL"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "Similiar interface, a __LOT__ faster with pdf's since it uses xpdf's engine, and not slow ass GV's./  "
    author: "anon"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "And kpdf is cool, but is it just me or why it seems that having kpdf/kdvi/kghostview a bit redundant?\n\nAnd isn't there almost entire xpdf source code base on the kpdf, any idea could there be any work being done for example making uniform library used by both?!?\n\nAnd while being in the subject, why there is kamera and kooka as both do very similar things and having some sort of integrated software for common image input + ocr functionality?\n\nAnd (redundand 'yes'es, heh!) thing I would like to see is to move noatun/kaboodle/xine(plugin)/xanim(legacy or what?) out of distribution to kdenonbeta/kdeextragear(s) and move in kmplayer and possibly approaching to get kplayer to use much much more vide codec base from MPlayer. Oh well not gonna happen. :-(\n"
    author: "Nobody"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "Kaboodle and aKtion (xanim) are gone. And why obey to your personal preference of MPlayer over Xine?"
    author: "Anonymous"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "Just try playing less common video files like divx5, vcd or theora as support for those is somewhat... While even MPlayer-0.9x can do them, not to mention the 1.0pre1.\n\nBut hopefully noatun is fixed for similar support as kaboodle were as for some strange reason some files don't play with noatun but kaboodle don't have any difficulties... Strange by any standards as I thought kaboodle were just a little brother to the noatun?\n"
    author: "Nobody"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "And xine's UI is way too different from all other kde system programs while kplayer is pretty standardized and of course kmplayer is just a plugin for konqueror so pretty complete replacement for kaboodle, too bad it doesn't inplement a konqueror's side kard...\n"
    author: "Nobody"
  - subject: "xine"
    date: 2003-09-14
    body: "The benefit of the xine architecture is that the library is separate from the interface, so you don't actually need xine-ui (which is no doubt the UI you are referring to, however it isn't the only one available) to run the xine engine. Having used the library myself, it's also very well designed and easy to use from a developer's perspective. Not only that, but it has working DVD menus and well-rounded codec support.\n\nFor those reasons IMHO it makes a better choice as a base for a KDE player than MPlayer (at least until Mplayer2 comes along - however xine is here now)."
    author: "Paul Eggleton"
  - subject: "Re: xine"
    date: 2003-09-17
    body: "MPlayer can be compiled as lib-only, but the point were that mplayer support much more video codecs that xine don't even plan to support yet on their todo-list, like theora/divx-5.02 (sure they acclaim divx-5.02 and newer are supported, but those files just don't work).\n\nAnd as kmplayer (were to replace kaboodle?) is already in the kdeextragear2, why not bump it to the release as it doesn't waste too much space anyway...\n\nAnd just if there were kplayer then the whole noatun/xine stuff could be replaced.\n\nWhy wait if the mplayer and companions might replace 'em in future kde?\n"
    author: "Nobody"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "It is not redundant.  A library would be nice, but it doesn't exist yet, and there's no reason to make us keep using slow, ugly GhostScript PDF translation when KPDF is sitting right there for us.  In fact it will be much better to have both I think.  KPDF is so much better for viewing PDFs that most people will be much happier with it.  I think after the release we will probably see many happy comments exclaiming about how much better pdf viewing is in KDE 3.2.  For those people who need to view .ps files, though KGhostView will always be necessary.  KGV is still much better than any free Windows .ps viewer."
    author: "Spy Hunter"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-13
    body: "why not just merge kpdf\\kgs\\kdvi into one powerfull app to view .pdf , .ps , .dvi  ? I think they are similar\n\nsounds as if we have a viewer for .png and one for .gif and another for .jpg\n\nisn't pdf ps div are documents that are plateform independent just as pics songs mp3's ? \n\nthis is just my own opinion \n"
    author: "Mohasr"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "Yes I have been using the kpdf for a while myself (from CVS) and it really is a cool piece of software compared to having xpdf just for PDFs to bloat the dependency hell.\n\nBut the point were that why are those own separate programs, I bet those are overlapping code that could be wiped off if kpdf/kghostview/kdvi would be a single application.\n\nThe library scenario is of course way off to the future, but that singularity would be for KDE's benefit as IMO there are too many separate programs for very similar tasks.\n"
    author: "Nobody"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "> But the point were that why are those own separate programs,\n> I bet those are overlapping code that could be wiped off if\n> kpdf/kghostview/kdvi would be a single application.\n\nMy guess is, that all these apps you mentioned are based on kviewshell hence all _do_ have a sort of a common codebase... though it may not be the biggest part of code that's shared among them. I am not a coder so don't rely on me... You could still take a look at it here:\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdegraphics/kdvi/\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdegraphics/kpdf/kpdf/\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdegraphics/kghostview/\n\nWell, to be honest I did not find any includes referring to kviewshell in kpdf, but as I said I've no experience with reading c++\n "
    author: "Thomas"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: ">why there is kamera and kooka as both do very similar things\n\nkamera and kooka? You've lost me.. Kamera is a digital camera ioslave, Kooka is a SANE/OCR frontend. Apart from dealing with \"images\" they are not very similar. \n\nOr did I misunderstand something?"
    author: "anonon"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "> And isn't there almost entire xpdf source code base on the kpdf, any idea could there be any work being done for example making uniform library used by both?!?\n\nI'm sure it could be done, but it'd be hard. The GNOME folks do the same thing with gpdf in gnome 2.4."
    author: "anon"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "Yes, but Acrobat Reader ist still a bit faster and the Font-Rendering is better. (xpdf 2.02.1 compared to acroread 5.0.8)"
    author: "Carlo"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "Indeed, acroread is quite good. So, here's what I want to know: does KPDF have the table of contents sidebar thing like acroread? That's the main reason I use it, it makes navigating pdf's much better than in xpdf or kghostview."
    author: "optikSmoke"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "Hopefully printing is fixed on kpdf/kghostview as atleast old versions can't seem to know how to scale the rendered document to correct size to the printer?!?\n"
    author: "Nobody"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "KPDF may be a lot faster than GV but its not near as good at accurately rendering things. See below\n\nAdobe Reader 6:  http://homepages.ihug.co.nz/~midgedog/pdf/ad6.png\nKGhostView:  http://homepages.ihug.co.nz/~midgedog/pdf/kgv.png\nKPDF: http://homepages.ihug.co.nz/~midgedog/pdf/kpdf.png\n\n"
    author: "Mike"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "KDE should have only one, I mean come on 2 pdf viewers, this is getting ridiculous and besides none of them even stand up to Acrobat Reader 6. KGhoStView is the best on Linux it should jsut be expanded and include xpdf support, no need makinga  separate app just for that."
    author: "Adi"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "Both kghostview and kpdf are implemented as kparts, and this, the apps \"kghostview\" and \"kpdf\" are pretty much small wrappers. "
    author: "anon"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "I have had far more compatibility problems with KGhostView than with xpdf.  My pet peeve is the detection of \"landscape\" or \"portrait\" mode.  Half the time when I find a pdf on Google it is wrong, which makes it impossible to view the document.  xpdf always gets it right."
    author: "Spy Hunter"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "Exactly!   I have the same nit to pick about kghostview.\nThere are a fair number of pdf's that I have to use xpdf with, just because kghostview can't get\nit right.  That's why I'm looking forward to kpdf.\n\nxpdf also seems faster."
    author: "TomL"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-14
    body: "Which Ghostscript version? I use 7.0.5 and it works well."
    author: "Asdex"
  - subject: "Re: Anyone have a screenie of kpdf?"
    date: 2003-09-15
    body: "Whatever is in Debian unstable."
    author: "Spy Hunter"
  - subject: "Can we edit PDFs yet?"
    date: 2003-09-14
    body: "Two features I'd love to see are:\n\n* the ability to select text in a PDF, so you can quickly copy+paste text out of them (I get really fed up of typing out quotations from PDF papers when working!)\n\n* the ability to edit PDFs in place. This could either just be a matter of letting the user change some text, or anything so far as making KWord really good at working with PDFs so you don't just make your document and export it, and keep the source file along side the PDF\n\nHow possible are these two, out of interest? I know, I should file a bug, but at the moment I'm still thinking about the ideas..."
    author: "Tom"
  - subject: "Re: Can we edit PDFs yet?"
    date: 2003-09-14
    body: "Yes, I would like to see #1 fixed as well. FYI:\nhttp://bugs.kde.org/show_bug.cgi?id=11823"
    author: "Paul Eggleton"
  - subject: "voice notifications"
    date: 2003-09-13
    body: "Did voice notifications with ttsd are planned ?\nI mean : in Mac OS style, when a message box pops up..."
    author: "LC"
  - subject: "RPM Wizard"
    date: 2003-09-14
    body: "Has anyone seen this great app?\nWhy can't it be included in the standard KDe release?"
    author: "OI"
  - subject: "Re: RPM Wizard"
    date: 2003-09-14
    body: "just checked it out. It's pretty cool, I think the main reason it's not included is because distros often give the suer an app like this, it would be cool if it was in utilities."
    author: "Adi"
  - subject: "Re: RPM Wizard"
    date: 2003-09-14
    body: "Not everybody uses *shudder* RPM distro's.."
    author: "None"
  - subject: "Re: RPM Wizard"
    date: 2003-09-15
    body: "Not everyone uses packages at all. Im ok with RPM Wizard in KDE.. under the following conditions:\n\n1: The name is changed to Install Wizard (or whatever).\n2: It gets support for the Debian way of doing things.\n3: It gets support for any other major system that im just not aware of. :P\n4: It gets support for the one and only .tar.[gz/bz2].\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: RPM Wizard"
    date: 2003-09-14
    body: "Idea is great. BUT:\nI'm using SuSE 8.2. Quite an un-geeky distro.\nNow the RPM Wizard page says:\nInstall python, rpm-python, and so on.\nrpm-python is not available for SuSE, you have to\ncompile it for yourself. \nNow, is this a joke or what? An installation utility which\nis so difficult to install that you have learned so much about\nLinux installations that you don't need it anymore once it's\ninstalled? An interesting way to develop a software solution\nI supposed. What comes next? A calculator which requires\nyou to enter all numbers by entering their square roots?\nYou can do it all by head once you've figured out how to use\nthat.\n\n"
    author: "Jan"
  - subject: "Konqueror"
    date: 2003-09-14
    body: "Im running the latest CVS debian debs.  Konqueror seems to slow\nthe whole system down to a crawl when downloading largish images.\nI will post a bug report if other people are seeing the same effect.\n\nhere is the url:\nhttp://www.swrt.com/cpimages/441769.jpg\n\nMozilla had no problem with the same image.\n\n~Mike"
    author: "Mike"
  - subject: "Re: Konqueror"
    date: 2003-09-14
    body: "Same problem for me :("
    author: "Shift"
  - subject: "Re: Konqueror"
    date: 2003-09-14
    body: "me too.\n\nthe version is 3.1.90(CVS >= 20030827) on the control center"
    author: "kzk"
  - subject: "Re: Konqueror"
    date: 2003-09-14
    body: "I'm experiencing the same problem. :-("
    author: "Steffen"
  - subject: "Re: Konqueror"
    date: 2003-09-14
    body: "I'm using the latest downloads form SuSE - early September version(4th - 92 at the end of the version?), it took about 10-15 seconds on broadband to download the picture - seemed reasonable to me  "
    author: "Gerry"
  - subject: "Re: Konqueror"
    date: 2003-09-14
    body: "Same problem.. I made a 6000x6000 jpeg with GIMP that was filled completely black with a few lines drawn in the middle (~750k). \n\n\ngqview took 3 seconds to load it - using /tmp/test.jpg\nxv took 4 seconds to load it - using /tmp/test.jpg\nkuickshow took 7 seconds to load it - using /tmp/test.jpg\nkonq-HEAD took 24 seconds to load it - using /tmp/test.jpg\nkonq-HEAD took 49 seconds to load it - using http://localhost/test.jpg\nMozillaFirebird took 4 seconds to load it - using /tmp/test.jpg\nMozillaFirebird took 5 seconds to load it - using http://localhost/test.jpg\nopera 6.12 took 3 seconds to load it  - using /tmp/test.jpg\nopera 6.12 took 3 seconds to load it  - using http://localhost/test.jpg"
    author: "anon"
  - subject: "Well it looks like"
    date: 2003-09-14
    body: "KOnqueror's constipated on mine too."
    author: "Adi"
  - subject: "Re: Konqueror"
    date: 2003-09-14
    body: "> I will post a bug report if other people are seeing the same effect.\n\nPlease do so. I have seen this problem many times with kde 3.1.\n"
    author: "name"
  - subject: "Re: Konqueror"
    date: 2003-09-15
    body: "Please vote for bug 39693 (http://bugs.kde.org/show_bug.cgi?id=39693)\n\nAll of the large image hangs/crashes/takes a long time in konq have been duplicates of this bug."
    author: "anon"
  - subject: "Re: Konqueror"
    date: 2003-09-15
    body: "I voted for this bug. However, I would also like to ask people to vote for bug http://bugs.kde.org/show_bug.cgi?id=52026. This has been bugging me for ages now, but somehow no-one else seems to notice it (and I'm sure it happens on different systems)."
    author: "Jelmer"
  - subject: "Re: Konqueror"
    date: 2003-09-14
    body: "I compiled a version of kdelibs and kdebase today, and the picture loads within 2 seconds here."
    author: "Jan Jitse"
  - subject: "Re: Konqueror"
    date: 2003-09-15
    body: "I think Konqueror has always had this problem.  Please, file a bug!"
    author: "Spy Hunter"
  - subject: "Re: Konqueror"
    date: 2003-09-15
    body: "ARGH !!\nyes, same problem with KDE 3.1.3\nactually i thought, my PC was freezing !\n;)"
    author: "Yubaba"
  - subject: "Kexi?"
    date: 2003-09-15
    body: "Who invented this awfull programm name?\n\nSounds like a vacuum cleaner to me ..\n\nregards\nthefrog"
    author: "thefrog"
  - subject: "Re: Kexi?"
    date: 2003-09-16
    body: "sorry,\n\never been in a situation where you really want to start development and don't want to take to much time on thinking about a name?\n\nok here the explenation:\nit needs a k.\nk...\nit is/was planed a smaller form of ms access we have the suffix 'i' therefor\nk...i\naccess is like a toungh braker\nk.xi\nand a keks is a cookie in german (at least austrian)\n\nkexi\n\n    lucijan"
    author: "Lucijan Busch"
  - subject: "Re: Kexi?"
    date: 2003-09-16
    body: ">and a keks is a cookie in german (at least austrian)\n\nReally? Because keksi is a cookie in finnish, and if you would write it with an x (kexi) ,and you could if you were a teenager :), it would still be pronounsed same way as keksi."
    author: "138"
  - subject: "Re: Kexi?"
    date: 2003-09-17
    body: "Don't know if it's just a legend, but if I recall it correctly the \"Keks\" (cookie) was introduced to the german language after a well known maker of sweets\n\n(Bahlsen, http://www.bahlsen.com/root_bahlsen_anim_en/root.html) \n\nchanged the (once english) name of one of it's more popular products from \"cakes\" to \"keks\", because the germans (in the late 19th century) pronounced the original name awfully wrong. A german tongue would pronounce \"keks\" much like an english would pronouce \"cakes\" (well, nearly...)\n\nThis way, the term \"keks\" found it's way to the german language and maybe to others as well..."
    author: "Thomas"
  - subject: "Re: Kexi?"
    date: 2003-11-23
    body: "Well, \"keks\" is a word meaning cake (or specifically baked cake) is the same in almost all languages, most of the slavic languages have the word \"keks\" (tho spelled in cyrilic mostly).\n"
    author: "Favorit"
---
In <a href="http://members.shaw.ca/dkite/sep122003.html">this week's CVS digest</a>: 
<a href="http://xmelegance.org/kjsembed/">KJSEmbed</a>, the KDE JavaScript implementation now supports event handlers. 
<a href="http://www.kdevelop.org/">KDevelop</a> adds support for code completion databases. 
<a href="http://www.koffice.org/kexi/">Kexi</a> now has a PostgreSQL driver. 
<a href="http://kopete.kde.org/">Kopete</a> integrates with 
<a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a> for IM contacts. 
The KWin rewrite continues with an added window decoration API. Plus many bugfixes throughout.
<!--break-->
