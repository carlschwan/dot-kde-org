---
title: "KBarcode: Professional Label Printing for KDE"
date:    2003-04-14
authors:
  - "dseichter"
slug:    kbarcode-professional-label-printing-kde
comments:
  - subject: "LOoks appealing"
    date: 2003-04-14
    body: "I haven't trie dit , but I checked out their website. IT looks nice and the screenshots show that their program is very capable."
    author: "Alex"
  - subject: "Re: LOoks appealing"
    date: 2003-04-14
    body: "As our website is down, because of a harddisk failure and it will take some time to get it up again, you can still download KBarcode and the PDF Documentation from our sourceforge page <a href=\"http://www.sf.net/projects/kbarcode\">http://www.sf.net/projects/kbarcode</a>. I attached also a screenshot of KBarcode to this post so that you can still see the label editor in action.\n<p>\nCU Dom (KBarcode Programmer)"
    author: "Dominik Seichter"
  - subject: "Re: LOoks appealing"
    date: 2003-04-14
    body: "The website is up and running again thanks to a new harddisk :-). \n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "YOU GOT to be kidding me!!\nDear developers of this program, if you are reading to this, PLEASE shrink your user interface!!!\nYou have some dialogs over there that DO NOT FIT on a 1024x768 resolution which is what *most people* have today (and about 10% of the users are still on 800x600)! You have some dialogs there that are **UNESESSARY** long or wide!!!\nPlease, I beg you, pay some attention on how you design that stuff.\nAlso, it is not just the 1024x768 users that will have problem, but even if you are on 1600x1200, that application steals real screen estate for NO GOOD REASON.\n\nOther than that, it seems like a fin application, but its UI needs serious fixing. :("
    author: "Jean-Pierre"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Eh, besides that you got a point but using a imho wrong tone. It's not like you are seriously having to suffer through this all. 800x600 is overrated nowadays, and this application is a special case application so it's really not like one million helpless users are now having to go suicide due to that program..."
    author: "Datschge"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "1024x768 a standard ? I suppose this is a joke.\n\nWe talk about a software which is used in the industry, not in a student room :)\n\nAnd in a production environment, there is no need (and what's more, it's unuseable) for r\u00e9solutions you talk about.\n\nWe need to read a screen from 6 feet (in front to, not under :p).\n\nMost of the society i work for are still using 640x480 to please the op\u00e9rators.\n\nSo ... no good reason ? You must be joking (especially with 1600x1200 !)  :)\n\nMike"
    author: "Mike"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "I tried KBarcode today for the first time in 1024x768 and I found _no_ dialog that did not fit into this resolution. Sure, there are some who are to big for 800x600, but I think that I cannot care for everyone and most people use 1024x768 on the desktop now.\nBut there is no problem at 1024x768 as I exspected it. So would you please be so kind a show/tell me which dialogs make problems for you so that I can fix the problem.\n\nBTW, KBarcode is no program that you will use from a 6 feet distance so there is definitly no need for it working in 640x480.\n\nCU Dom (KBarcode Programmer)"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "> so there is definitly no need for it working in 640x480.\n \nThink of an embedded system using your app, maybe with the size of a PDA. "
    author: "anonymous coward"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Sure, it is a regular job for me to Print 10.000 labels with barcode from my PDA or even my mobile phone! ;-) \nKBarcode is written for use on Desktop PC's and nothing else!\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Maybe you use it for that, but what about others? What if I am working in a shipping and logistics company where I have a pda, walk around to ensure that the 10,000 products people wanted sent are prepped, look at my pda running your program to start the printing of the 10,000 labels.\n\n\nThis is part of the problem of open source software developers. They think they know how everyone will use their software, then when someone comes up with a comment or problem with it, they say 'this is my software, I do what I want, do not tell me what people want'.\n\nThanks for the code, but with an attitude like that, I doubt you will get many users, let alone bug reports or other tips."
    author: "tom hanks"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "<quote>What if I am working in a shipping and logistics company where I have a pda, walk around to ensure that the 10,000 products people wanted sent are prepped, look at my pda running your program to start the printing of the 10,000 labels. </quote>\n\nYou're kidding, right?  Be realistic my friend, that's all he's telling you.  \n\nDo you know what a PDA is? It's a flimsy little thing like a palm pilot.  You don't sit down and do graphics work with it.\n\n*LOL*"
    author: "ac"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Thanks for your support ac. If you want to print 10.000 labels which will take a few hours anyways, you will have the time to sit down on a desktop PC to start printing I guess.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Thank *you* for KBarcode.  :-)"
    author: "ac"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Most people have 1024x768 on their desktops though!! Please remodel your UI."
    author: "Jean-Pierre1"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "We consider it important that the UI is useable under 1024x768 and after all my tests it is useable. All dialogs fit perfectly on a 1024x768 screen. If someone is able to show me one that does not fix, I promise to fix it within a few hours.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "This one does not fit:\nhttp://www.kbarcode.net/modules/Screenshots/04-batch1.png\nIt is 770 pixels high, and if you count on this the 64pixels of the KDE Kicker/taskbar, you will need to find a way to make such dialogs no more than 700 pixels high. More over, I would even advocate to make your application workable on a 800x600 resolution too. But as it stands today, even 1024x768 (plus Kicker) doesn't fit some of your dialogs.\n\nThank you.\nJP"
    author: "Jean-Pierre"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "Stop acting like a fool. If you indeed use small resolutions like 800x600 or standard 1024x768 then you'd be crazy to set the font size that big like has been done in that screenshot. The default font size in KDE is considerable smaller, and that window (like most others) is respectively smaller then."
    author: "Datschge"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "Please try the real version instead of looking on old screenshots from older development releases. The dialog from the final version was slightly changed and is now (with a sensible font size) 634 pixels high. I think it fits very good on a 1024x768 screen. Screeshot is attached - so take a look for yourself, please :).\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Do a web search for \"portable barcode printer\". See how many hits come up?\n\n*You* may not have a need for such a thing, but other people do, including at my workplace.\n\nIncidentally, Dominick, this is *not* a criticism of you or KBarcode. It's useful as a desktop app -- we're just constructively suggesting another niche it could be made to fit. Thank you for making it!"
    author: "Otter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "My answer may have been a bit harsh, sorry for this. I just wanted to say, that it works under 1024x768. \nI see that it is _not_ useable on lower resolutions. I will try to improve it for the next release and I am happy about all your feedback and suggestions.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "How about making it useable on 768x1024 (no, that's not 1024x768)? The idea is, that you can rotate some of the new LCD monitors on it's stand. I can tell you, that works awsome. The new RAND features in Xwindows and the support from KDE for this make that you can actually rotate your monitor and have a desktop that is higher than it is wide. It enables you to view your A4 document as a whole and keep it readable at the same time. Many people I've seen working with these screens, prefer the rotated mode. \n\nThe moral of the story: don't assume too much about your users system or preferred setup. Although his desktop may be big, it may not all be available for your application. Keep up the good work!"
    author: "Andr\u00e9 Somers"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "That is really interesting, a did not know that. But I think that all dialogs will already perfectly work on 768x1024, because the problem of my dialogs is their height and not their width ;-). But I will take a deeper look on this and improve support for this monitors.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "Actually, OpenSource supports minoritie's needs much better than any closed source software.\n\nIn your case, the needs are 100% hypothetical, there is nobody walking around with a PDA to print, let alone designs barcode labels with it.\n\n"
    author: "Roland"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "It's not only hypothetical: It exists, and is being marketed:\n\nhttp://www.amerbar.com/catalog/pda_printer.asp"
    author: "anonymous coward"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "OK, nice, it exist. Point is, a PDA is a different kind of platform than the desktop, so you'd need to re-do the interface anyway. Or does your PDA come with a full-flegded keyboard and a mouse? You can't expect desktop software to be immediatly useable on a PDA. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-15
    body: "You're right actually. I just found it funny mentioning that nobody goes around printing with a pda ;-)\n\nIt's easy to notice that this soft is only desktop oriented. I think I made the wrong assumtion that this tool was more industry oriented, since barcodes are mainly used by industry."
    author: "anonymous coward"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: ">1024x768 a standard ? I suppose this is a joke.\n> We talk about a software which is used in the industry, not in a student room :)\n> And in a production environment, there is no need (and what's more, it's >unuseable) for r\u00e9solutions you talk about.\n\nYou're wrong. Think of how most industries work nowadays: embedded systems (note: embedded=usually small), robots with small LCD touch screens as control panel (maybe even smaller than 800x600), numeric controls with same LCD's... It's the student room that has biggest screens nowadays, just to play UT :-)\n \n"
    author: "anonymous coward"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Argh! Sorry, I didn't read properly what you wrote above. I think we just agree then ;-)"
    author: "anonymous coward"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "> >1024x768 a standard ? I suppose this is a joke.\n> > We talk about a software which is used in the industry, not in a student room :)\n> > And in a production environment, there is no need (and what's more, it's >unuseable) for \n> r\u00e9solutions you talk about.\n> You're wrong. Think of how most industries work nowadays: embedded systems (note:\n> embedded=usually small), robots with small LCD touch screens as control panel (maybe even\n> smaller than 800x600), numeric controls with same LCD's... It's the student room that has\n> biggest screens nowadays, just to play UT :-)\n\nAnd kbarcode will never be used on a robot control panel :-). KBarcode is for the use on desktop PC's only, so I think 1024x768 is fine as a minimum resolution requirement. As soon as someone is able to use KBarcode on a embedded system for robot control, I'll change all dialogs to fit on 640x480 ;-).\n\n\nCU Dom (KBarcode Programmer)"
    author: "Dominik Seichter"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2007-03-27
    body: "> As soon as someone is able to use KBarcode on a embedded system for robot\n> control, I'll change all dialogs to fit on 640x480 ;-).\n\nConsidering how quickly the technology is changing, I'm wondering if embedded systems for robot control will support 1024x768 before you redesign the UI to handle lower resolutions ... ?\n\nExcellent piece of software, by the way.  I've got a couple wishes I'm going to file on it shortly.\n  "
    author: "KS"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "So you seriously think that this is an application that will be used from an embedded device at some plant? Of course that's not how it works. It will be used at the office area of that plant where some employee creates the labels once and for all, and then copies labels onto some standard paper or whatever, and then sends it down to the labelling machines in the plant.\n\nEvery office employee at the plant will have a normal computer, nobody has a robot with a small LCD touch screen."
    author: "Chakie"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-14
    body: "Ah, that's fine. I said this because I had a system in front of me not long ago. It was an automated system, which made some pieces and printed the barcodes in stickers after doing it... This was just a small student project, so it was controlled through a PC, but in a real life system that automation part may not have a 1024x768 screen.\n\nAnyway, to say the truth, it's a really good tool! "
    author: "anonymous coward"
  - subject: "Re: The UI needs fixing *immediately*"
    date: 2003-04-17
    body: "we are working on a fix and considering to release 1.2.1 next week (after Easter). We will annouce it here, so please test it.\n\ncu\nStefan Onken\nProject Manager Kbarcode"
    author: "Stonki"
  - subject: "Language in the screenshots"
    date: 2003-04-14
    body: "From the screenshots: this is not correct German, right?\n\"Massen Druck\"\n\"In die Zwischen Ablage Kopieren\"\n\"Eigene Label Definition Hinzof\u00fcgen\""
    author: "Erik"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-14
    body: "Sorry, it is \"Eigene Label Definition Hinzuf\u00fcgen\", but that still looks wrong."
    author: "Erik"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-14
    body: "Indeed it is not.\n\nCorrect would be:\n\n\"Massendruck\"\n\"In die Zwischenablage kopieren\"\n\"Eigene Label-Definition hinzuf\u00fcgen\"\n\nHave a nice day/evening/night!"
    author: "Nicolas GOUTTE"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-14
    body: "ok, da wir ja alle Legastiker sind (hoffentlich ist das richtig geschrieben :) ), werden wir das deutschen Beschreibungen \u00e4ndern. \n\nDie Sache mit der Bildschirmanzeige: ok, werden wir ber\u00fccksichtigen, vielleicht k\u00f6nnen wir das ja kurzfristig \u00e4ndern.\n\ncu\nStonki"
    author: "Stonki"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-14
    body: "The screenshot's are from a slighly older version. The final stable release got spell checked, so there are hopefully not that many spelling mistakes in the final :-).\n\nAnyways thanks, for the information, we will update the screenshots as soon as possible.\nUnfortunately, we can't right no, because we had an IDE harddisk crash on our server and we are still waiting for a new one. That is also why our webpage can't be accessed right now. Sorry for all the inconviences.\n\nCU Dom (KBarcode Programmer)"
    author: "Dominik Seichter"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-14
    body: "catch (LegasthenikerAlarm e)\n{\n  e.replace (\"Legastiker\", \"Legastheniker\");\n};\n"
    author: "Bebop"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-14
    body: "LOL! What a word to make a mistake in!"
    author: "Jim"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-14
    body: "catch( LameCatchUsage& e )\n{\ne.replace( \"LegasthenikerAlarm\", \"LegasthenikerAlarm&\" );\n};\n// >;-)\n"
    author: "Zelva"
  - subject: "Re: Language in the screenshots"
    date: 2003-04-17
    body: "Hallo Erik,\n\nwhen working for more than 9 months on a program like kbarcode, you do not see this kind of mistakes. Please: give us better phrases and keywords for the German and English language. We will release a 1.2.1 version, which will fix the 800x600 problems, so this would be a good opportunity to fix the language as well.\n\ncu\nStefan Onken\nProject Manager kbarcode"
    author: "Stonki"
  - subject: "Who said KDE didn't have apps?"
    date: 2003-04-14
    body: "First KStars, now KBarcode.  Quality apps doing a very special and useful job.  Keep them coming!"
    author: "KDE User"
  - subject: "KPart?"
    date: 2003-04-14
    body: "First, let me congratulate for the excellent quality of KBarcode.\nSimilar software under windows are often based on OLE controls which can be embedded by various applications (word processors, report printers and so on). Can (or will) KBarcode make something like this using KParts?"
    author: "Andrea Cascio"
  - subject: "Re: KPart?"
    date: 2003-04-15
    body: "I already thought about this, because I think it would especially be a good addition for KOffice. Maybe this is a good chance for me to learn KParts better ;-)!\n\nFor now you can still export barcodes as png images (or other formats) and import them in almost every app and document. But I know a KPart would be much better.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: KPart?"
    date: 2003-04-15
    body: "When it comes to using barcode programs it's not the importing into other apps or documents which is the important task. The important ting is to be able to use it as a lib for printing. Naturally you will use the barcodeapp to design the layout, but what a power user wants is the ability to use this as a template and print from a 3rd party app. The 3rd party app generates someting like part/serial-number or something and the barcode lib generates the barcode from the template and prints it.\n\nIn the windows world they would use ActivX not OLE. Having seen this implemented, I can tell you settting up and dumping the ActivX controller into the program took less time than designing the label according to the spec :-)\n\nBTW: How good does it work with dedicated barcodepriners?\n"
    author: "Morty"
  - subject: "Re: KPart?"
    date: 2003-04-15
    body: "DEfinitelly agree with that one. I would like to use it from my \"underdeveloppement\" library software to print book label. The requirement is just to generate on the fly lable using a specific template for new document."
    author: "stephane PETITHOMME"
  - subject: "screen resolution"
    date: 2003-04-15
    body: "As of July 24th, 2002 and according to OneStats, common screen resolutions are:\n\n1.  1024 x 768  43%\n2.  800 x 600  37%\n3.  1280 x 1024  12.9%\n4.  1152 x 864  3.8%\n5.  640 x 480  1.3%\n6.  1600 x 1200  1%\n7.  1152 x 870  0.2%\n\nAnd personaly, I've a 800*600 screen. All programs should be usuable on such a screen."
    author: "ced"
  - subject: "Re: screen resolution"
    date: 2003-04-15
    body: "These are the stats for KDE users in kde-look (which differ quite a lot!):\n\nhttp://www.kde-look.org/poll/index.php?poll=19"
    author: "uga"
  - subject: "Re: screen resolution"
    date: 2003-04-15
    body: "Perhaps because 800x600 is still unusable pretty much in KDE? :)"
    author: "fault"
  - subject: "Re: screen resolution"
    date: 2003-04-15
    body: "that's a poll, not stats"
    author: "ced"
  - subject: "Re: screen resolution"
    date: 2003-04-15
    body: "True.. but it shouldn't matter much with XFree86, which probably 99% of KDE users are using. You can have virtual resolutions. On PowerBase 200 (old mac-clone), I use a resolution of 800x600 on a 1600x1200 virtual screen."
    author: "fault"
  - subject: "Re: screen resolution"
    date: 2003-04-15
    body: "Well, and what's the difference? Stats are just calculated from people questioned about a matter, the same way as this poll. The only difference may be that stats might have a broader audience.\n\nIf you consider that kde-look users/voters are representative enough of all KDE users, and that more than 20000 users (votes) are meaningful compared to the full user group, then you can fairly say that this poll gives us proper statistics."
    author: "uga"
  - subject: "Re: screen resolution"
    date: 2003-04-15
    body: "It should be noted that most users keep sticking to a system's default screen resolution. Windows' default screen resolution after installation has been 640x480 and 800x600 until recently. Knoppix on the other hand starts up in 1024x768 on almost all computers so most other Linux distributions should be able to do so as well."
    author: "Datschge"
  - subject: "Re: screen resolution"
    date: 2003-04-17
    body: "We will work on the 800x600 resolution issue the next days and we will release a new version (1.2.1) of kbarcode, which will \"fix\" these kind of problems.\n\nThanks a lot for pointing this out. This was something we never thought about. But this is the big strenght of Open Source Software: the feedback !\n\ncu\nStonki\nProject Manager kbarcode"
    author: "Stonki"
  - subject: "we like it"
    date: 2003-04-16
    body: "Hello KBarcode developers!\nyou did a nice job overall I think...\nthis program solves an expensive set of problems for me...\nKeep up the good work. "
    author: "Greg Rotunda"
  - subject: "Possible uses?"
    date: 2003-04-16
    body: "What home uses for kbarcode are there? Do all of them require a barcode reader as well?"
    author: "AC"
  - subject: "Re: Possible uses?"
    date: 2003-04-16
    body: "There're plenty of uses, but mainly with commertial uses:\n\nLocal magazines who are registered and therefore have an ID.\nHome based companies (like home based companies)\nLinux developers who want to distribute their own soft-CD's with barcode ID's....\n.\n.\n.\n\n\n"
    author: "uga"
  - subject: "Re: Possible uses?"
    date: 2003-04-16
    body: "Ahem! I should start reading properly my own posts before submitting:\n\n>Home based companies (like home based companies)\n\nGood example! :-)  I should have written \"(like farms for example)\".\n\n"
    author: "uga"
  - subject: "Re: Possible uses?"
    date: 2004-01-20
    body: "What do you mean I cannot understand what you writing all about im very sorry ok?\n"
    author: "ariel pascual"
  - subject: "Re: Possible uses?"
    date: 2003-04-17
    body: "You can use KBarcode for many things at home, without the need for a barcode reader. \nOne example is printing CD-Covers, but there may be more confortable applications for this task. A more usual task is printing business cards or address labels, both of them do not contain barcodes usually, so you won't need a barcode reader.\n\nAs a matter of a fact, I do not even own a barcode reader, so one can definitly use KBarcode without owning one.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: Possible uses?"
    date: 2004-04-11
    body: "Hi;\n\nI was wondering if you know any open source for barcode reader preferably Real-time software (on RTLinux or other real-time OS)?\n\nThanks\nAli\n"
    author: "Ali Tizghadam"
  - subject: "Re: Possible uses?"
    date: 2005-12-09
    body: "Yes, I can only tell you that you are the biggest geek I know..."
    author: "yi"
  - subject: "KDE Interface Guidelines"
    date: 2003-04-18
    body: "http://developer.kde.org/documentation/standards/kde/style/dialogs/complex.html\n\nA dialog should be designed such that it does not need to be bigger than 4/5th of the screen dimensions. So with a screen size of 800x600 pixels (width by height) the dialog should not exceed 640x480 pixels in any dimension. \n\nKDE assumes a minimum screen size of 800x600 pixels. "
    author: "Morten Hustveit"
  - subject: "Thank you"
    date: 2003-04-25
    body: "You should not have to listen to these complaining ingrates. You worked hard to put out a program that you charge $0.00 for. It makes me wonder how many of these negative comments are coming from Europe.  I say this because many Europeans tend to not appreciate the work or sacrifice of others. :)   "
    author: "Chris"
---
After more than 5 months of development, the KBarcode team has released <a href="http://www.kbarcode.net">version 1.2.0 of KBarcode</a>.  This latest stable release brings professional high-quality label printing to the KDE desktop. In fact, KBarcode is already used by a few companies under production conditions and has proven to be reliable and stable -- considering the high costs for similar commercial applications, KBarcode might save you some money!  KBarcode supports labels with most major barcode types, images, as well as any kind of rich text, and can be used for everything from simple CD covers to printing thousands of business cards. Labels can easily be created using the <a href="http://www.kbarcode.net/modules.php?name=Screenshots">WYSIWYG label designer</a> (<a href="/1050284582/1050286905/1050314936/kbarcode.jpg">misc shot</a>) and over 1000 predefined labels.  Configuring is easy thanks to the graphical setup wizard, which can also optionally create SQL tables.  KBarcode is available in English, Finish, German, Hungarian, Italian, Spanish and Swedish.  For further information on documentation and support,  <a href="http://www.kbarcode.net/modules.php?name=Documentation">click here</a>.  
<!--break-->
