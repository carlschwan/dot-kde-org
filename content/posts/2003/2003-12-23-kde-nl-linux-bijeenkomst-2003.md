---
title: "KDE-NL at Linux-Bijeenkomst 2003"
date:    2003-12-23
authors:
  - "fmous"
slug:    kde-nl-linux-bijeenkomst-2003
comments:
  - subject: "Sessions"
    date: 2003-12-23
    body: "Ah yes. I have three users here, usually we have three open sessions. My wife particularly will open a session, open KMail, then myself or my daughter will do something in our session. Ctrl-Alt-F* is slow, and there are 5 keys to try. If she can't find her session, she opens a new one, tries to open KMail, gets an error saying there is a KMail open already. And I hear about it.\n\nSo far, the solution is to not close any session, leaving her at F7.\n\nIt would be nice if the Start New Session menu item would either be a list of open sessions, or list of users. KDM uses a FIFO to open new sessions, and there was a patch a while ago to allow two way communication. This would allow (among other things) kdm to give info about open sessions.\n\nIn the meantime, is there any application that lists the sessions that are open? I searched a while ago without success.\n\nDerek"
    author: "Derek Kite"
  - subject: "Calendar (stupid question)"
    date: 2003-12-23
    body: "Is there a calendar or mailing list somewhere that relays info about KDE and Linux events in western Europe? How does everyone else keep informed about what is coming up?\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Calendar (stupid question)"
    date: 2003-12-23
    body: "We have http://events.kde.org/ but this is seriously in need for a webmaster. Please contact Eva <eva at kde.org> when you are interested .. \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Calendar (stupid question)"
    date: 2003-12-23
    body: "Or mail me. events.kde.org is looking for someone with PHP + MySQL skills who knows what SQL injection means."
    author: "Waldo Bastian"
  - subject: "Chief Commander"
    date: 2003-12-23
    body: "Please ask him why the usability website is \"uptodate\". I guess because they are busy.\n\nWhat about the usability reports, is it deprecated?\nhttp://usability.kde.org/activity/usabilityreports/"
    author: "Matze"
  - subject: "Re: Chief Commander"
    date: 2003-12-23
    body: "> Please ask him why the usability website is \"uptodate\".\n\nlack of an active webmaster? i think the average usability.k.o webmaster lifespan hovers somewhere around 3 weeks. they are welcomed, encouraged, appreciated, etc. while they are around, but they inevitably just disappear into the void with nary a sound. apparently website maintenance is even more boring than usability work ;-)\n\n> What about the usability reports, is it deprecated?\n\nthe web infrastructure for it never got completed (see previous paragraph). and now, yes, they are deprecated\n\n\"Deprecated in favour of what?!\" you ask? wel, i'll have the answer for you right after 3.2 is out. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Chief Commander"
    date: 2003-12-23
    body: "Ehemm, so you believe we don't need usability reports anymore as soon as KDe 3.2 is out? Is this true?"
    author: "bala"
  - subject: "Re: Chief Commander"
    date: 2003-12-23
    body: "heh.. no, not usability reports themselves, but the website and (lack of a standardized) process for creating them will be \"deprecated\". this will result in more usability reports, as measured both in quantity and quality."
    author: "Aaron J. Seigo"
  - subject: "usability menu issue"
    date: 2003-12-23
    body: "KDE 3.1: In the CC you can select \"Icon with text\". This could make sense at some applications with very few icons in the bar but when you start products of Koffice with that option enabled, the icon bar becomes unusable. (for instance the small bold icon in your writer , below the relatively long text \"bold\") So what's the use of this option? Per application may be okay, but kde wide is just causes trouble."
    author: "maga"
  - subject: "Re: usability menu issue"
    date: 2003-12-23
    body: "You already know that you can change this setting per application, don't you? RMB menu on the toolbar->Text Position.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: usability menu issue"
    date: 2003-12-23
    body: "Per toolbar even, or per button. The latter could be tedious if one wants an entire toolbar to be displayed with textlabels."
    author: "Koert van der Veer"
  - subject: "Re: usability menu issue"
    date: 2003-12-23
    body: "one of the targets for post-3.2 is toolbar use and abuse ... kmail and some other apps are a bit bitter in 3.2, but for post 3.2 the plan is to do a sweep of every toolbar using app in KDE CVS... the good news is that doing so only take a week or so.. the even better news is that the expected standards for toolbar button usage will be recorded for posterity (and finger pointing ;)"
    author: "Aaron J. Seigo"
  - subject: "Does KDE-NL knows the author of KMplayer ?"
    date: 2003-12-24
    body: "KDE-NL is very active. However, I was wondering if they have any contact with the author of KMplayer since he's dutch (I believe so). His app is really good & integrates wonderfully with KDE. A talk from him will sure be interesting."
    author: "Phil Ozen"
  - subject: "Re: Does KDE-NL knows the author of KMplayer ?"
    date: 2003-12-24
    body: "Why not contact the guy yourself :) and do an interview?"
    author: "Fabrice Mous"
---
<A href="http://www.kde.nl/">KDE-NL</A> was present at "<A href="http://linux-bijeenkomst.nl/">Linux-Bijeenkomst 2003</A>" at Tiel, Netherlands. We have published a <A href="http://www.kde.org/international/netherlands/events/linux-bijeenkomst-2003/lb2003_e.html">small bilingual impression</A> of that day and, included as a bonus, is a small IRC snippet where we discuss some usability issues with Aaron Seigo, chief commander of the <A href="http://usability.kde.org/">KDE Usability Project</A>.
<!--break-->
