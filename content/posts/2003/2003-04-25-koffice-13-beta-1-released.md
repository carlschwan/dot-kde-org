---
title: "KOffice 1.3 Beta 1 Released"
date:    2003-04-25
authors:
  - "binner"
slug:    koffice-13-beta-1-released
comments:
  - subject: "w00t !"
    date: 2003-04-24
    body: "first post ?\nw00t.\n\nThanks kde guys"
    author: "kannister"
  - subject: "Thanks"
    date: 2003-04-24
    body: "OO 1.1, Koffice 1.3, Abiword 2, GobePRoductive 3 etc. Who says there aren't enough productivity programs for Linux? Thanks everyone! "
    author: "Alex"
  - subject: "Re: Thanks"
    date: 2003-04-25
    body: "You forgot the \"good\" before the \"enough\"...\nOO 1.1 is quite nice, but it is also a monolitic beast. \nKOffice is coming along nicely, but it is still not mature enough. \nThe other ones I don't know, never used them. \n\nThings are changing though, fortunately.\nKeep up the good work, Koffice team!\n\nWM"
    author: "WM"
  - subject: "compilation fails"
    date: 2003-04-25
    body: "Compilation with gcc-2.95.3 fails:\n\nmake[4]: Entering directory `/usr/local/src/koffice-1.2.90/kivio/kiviopart'\nsource='libkiviopart_la.all_cpp.cpp' object='libkiviopart_la.all_cpp.lo' libtool\n=yes \\\ndepfile='.deps/libkiviopart_la.all_cpp.Plo' tmpdepfile='.deps/libkiviopart_la.al\nl_cpp.TPlo' \\\ndepmode=gcc /bin/sh ../../admin/depcomp \\\n/bin/sh ../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. \n-I. -I../.. -I./ui -Iui -I./kiviosdk -I./tklib -I./tools -I../../lib/kofficeui -\nI../../lib/kofficeui -I../../lib/kofficecore -I../../lib/kofficecore -I../../lib\n/store -I../../lib/store -I../../lib/kwmf -I../../lib/kwmf -I../../lib/kotext -I\n../../lib/kotext -I../../lib/kformula -I/opt/kde/include -I/usr/lib/qt/include -\nI/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno\n-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwri\nte-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wc\nhar-subscripts -DNDEBUG -DNO_DEBUG -O2 -O2 -march=i386 -mcpu=i686 -fno-exception\ns -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_\nCAST -DQT_NO_COMPAT  -c -o libkiviopart_la.all_cpp.lo `test -f 'libkiviopart_la.\nall_cpp.cpp' || echo './'`libkiviopart_la.all_cpp.cpp\nIn file included from libkiviopart_la.all_cpp.cpp:18:\nstencilbarbutton.cpp:109: warning: ANSI C does not allow `#warning'\nstencilbarbutton.cpp:109: warning: #warning \"Left out for now, lacking a style e\nxpert (Werner)\"\nIn file included from libkiviopart_la.all_cpp.cpp:6:\nkivio_view.cpp: In method `void KivioView::setupActions()':\nkivio_view.cpp:440: warning: value computed is not used\nkivio_view.cpp: In method `void KivioView::setLineWidth()':\nkivio_view.cpp:1040: warning: initialization to `int' from `double'\nIn file included from libkiviopart_la.all_cpp.cpp:31:\nkiviostencilsetaction.cpp: At top level:\nkiviostencilsetaction.cpp:39: redefinition of `const char * default_plug_xpm[]'\nadd_spawner_set_dlg.cpp:35: `const char * default_plug_xpm[22]' previously defin\ned here\nmake[4]: *** [libkiviopart_la.all_cpp.lo] Error 1\nmake[4]: Leaving directory `/usr/local/src/koffice-1.2.90/kivio/kiviopart'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/usr/local/src/koffice-1.2.90/kivio/kiviopart'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/usr/local/src/koffice-1.2.90/kivio'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/usr/local/src/koffice-1.2.90'\nmake: *** [all] Error 2\n"
    author: "Andrey V. Panov"
  - subject: "Re: compilation fails"
    date: 2003-04-25
    body: "run configure without --enable-final"
    author: "AC"
  - subject: "Kexi"
    date: 2003-04-25
    body: "Wow! that is pretty cool looking. Looks like it might rival Access for ease of use for working with databases. I cant wait to see where KOffice is in a year!"
    author: "Echo6"
  - subject: "Re: Kexi"
    date: 2003-04-25
    body: "> in a year!\n\nI think KOffice 1.3 will be released in august this year. :-)"
    author: "KDE User"
  - subject: "Re: Kexi"
    date: 2003-04-26
    body: "I think that you should better think of September instead of August."
    author: "Nicolas GOUTTE"
  - subject: "Gnome requirements..?"
    date: 2003-04-25
    body: "Hi All,\n\nI was just installing Koffice 1.3 Beta1 from the SuSE 8.1 RPMS.\n\nIt seems to want quite a few Gnome derived RPMS installed, including,\n\nORBit\nBonobo\nGConf\nGnome VFS\n\nDoes anyone know why are these libraries required for KOffice...?"
    author: "Adrian Bool"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-25
    body: "These are not required if libgsf is compiled with \"--without-gnome\". libgsf is required by wv2 which is responsible for the much improved MS Word import filter."
    author: "Anonymous"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-25
    body: "Why did SuSE compile it with GNOME?  Does this mean everyone who installs KOffice has to install GNOME?  This is really bad and bloated for KDE."
    author: "KDE User"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-26
    body: "Perhaps because the library installed by this package is also used by Gnome on SuSE?"
    author: "Anonymous"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-26
    body: "That's why they need a libksf for KDE so that they don't load GNOME everytime someone loads KOffice.  :-("
    author: "KDE User"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-28
    body: "Well, libgsf provides a --without-gnome option which reduces the dependency to glib2 and libxml2. The problem is, that the SuSE package hasn't been split up in libgsf and libgsf-gnome as done by other distributions. I talked to the maintainer of the SuSE KDE rpms and he said he's going to notify the libgsf packager, so stay tuned."
    author: "Werner Trobin"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-25
    body: "Just to add, this is good enough reason to build libksf because KDE has technology as \nDCOP, \nKParts, \nKConfig and \nKIO \nto do the same work and loading a duplicate set of libraries to do the same thing is completely nuts."
    author: "KDE User"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-28
    body: "Do you have any idea how much work went into libgsf? Re-writing it would be a major waste of effort. At heart, libgsf really only has one core dependancy (glib2.0), which only takes up a very small amount of memory. \n\nBy all means, please do seek out a solution so the other Gnome libraries aren't pulled in (I'd be quite interested in this myself), but please don't suggest fragmenting one of the (very few) points of collaboration between the various office projects. At some point, we have to find a common ground or be condemned to be perpetually re-inventing each other's wheels."
    author: "William Lachance"
  - subject: "Re: Gnome requirements..?"
    date: 2003-04-28
    body: "Simply use the basic runtime version of libgsf which doesn't contain Gnome specific extensions. Doing a search for libgsf (by mmb click) resulted in eg. http://packages.debian.org/unstable/libs/libgsf-1.html\nAnd if you compile it yourself achieve that with \"--without-gnome\" like Anonymous on Friday 25/Apr/2003, @11:02 wrote already above."
    author: "Datschge"
  - subject: "Nice..."
    date: 2003-04-25
    body: "Kexi rules! Thank you, I needed that."
    author: "Anno"
  - subject: "Re: Nice..."
    date: 2003-04-25
    body: "My question is: \"what language is used to make the kexi front-end talk to the MySQL database end? Just like in the windows world, one could use Visual Basic to talk to a MS-SQL Server database?\"\nThanks,\n\nCb.."
    author: "buyondo"
  - subject: "Python, please..."
    date: 2003-04-26
    body: "The Basic language is not welcome in the KDE developers world, it's a bad or a good thing, I don't know...\nThe choosen language seems to be \"a JavaScript/ECMA-Script like language (QSA)\" and I feel it is too complex (and it seems specific...). For me the better language for such an Access-like program is Python, both simple and powerful, now universal standard, easy to learn for those who know the VBA Basic of Access... "
    author: "Alain"
  - subject: "Re: Python, please..."
    date: 2003-04-27
    body: "http://www.kbasic.org/"
    author: "KDE User"
  - subject: "Re: Python, please..."
    date: 2003-04-27
    body: "What ? Nothing to do with Kexi or Kdevelop...\nAnd this KBasic project is old and has a very slow progress (nothing from october), it has not a good place in the KDE world.\n\nThe great force of the MS Office is the VBA language. A good Linux office needs such a language, both simple and powerful. I feel it can only be Python, wich has a very energetic community."
    author: "Alain"
  - subject: "Re: Python, please..."
    date: 2003-04-28
    body: "So what? Support whatever you want (and don't complain that others support what they want)."
    author: "Datschge"
  - subject: "Is Kexi credible ?"
    date: 2003-04-29
    body: "The question is not here, in the choice of a person or another. The question is to use wether a standard, very well known language, wether a \"JavaScript/ECMA-Script like language (QSA)\" which seems to be badly known and not powerful (I may be false, it's easy to be false about some unknown thing...) and not easy (here, it's perhaps more personnal, the java languages are less easy than Basic or Python...). \n\nIf Kexi uses a specific language, numerous users will not use it, it's not credible in terms of durability and support of a standard language (also available in others applications). The users support the most credible solutions. The question is here : is Kexi a convaincing solution ?\n\nMore generally, I think that it lacks for KOffice something like VBA for Ms Office. Is it necessary ? If it is, what language to choose ? Here also, I think that Python would be a good solution. Perhaps another may be credible, but not something that seems specific, with a name impossible to memorize that you may only use by copy/paste :  \"JavaScript/ECMA-Script like language (QSA)\"."
    author: "Alain"
  - subject: "Re: Is Kexi credible ?"
    date: 2003-04-29
    body: "Firstly ECMA compatible script languages are not \"badly known\" but *the* client side script language in the internet. Secondly no single KDE program uses a \"specific (scripting) language\", they are generally open to everything, someone just need to implement it (it could be you). Thirdly look what's already possible with DCOP in conjunction with any language. Forthly Python is getting popular among KDE developers, support them and let them know you'd like Python scripting support (and post a wish report at bugs.kde.org, not here). Fifthly stop sounding like someone who is just bitching about the lack of VBA while ignoring existing alternatives. Thank you."
    author: "Datschge"
  - subject: "Re: Is Kexi credible ?"
    date: 2003-04-30
    body: "Sorry, today for the users there are not existing alternative for VBA in KOffice, and your speech don't show such an alternative. Many things are possible for developers but it is not here. Your word \"bitching\" has no justification, it's sad that you use it."
    author: "Alain"
  - subject: "Bidi in Word?"
    date: 2003-04-25
    body: "What's the status of bidi text support in KWord? It has been coming Real Soon Now (TM) for a long time."
    author: "Joe Forbes"
  - subject: "Re: Bidi in Word?"
    date: 2003-04-26
    body: "From the KOffice 1.2 announcement: \"KWord and KPresenter now feature full support for reading and writing bi-directional text, such as Arabic and Hebrew\". So what have you been missing?"
    author: "Anonymous"
  - subject: "Re: Bidi in Word?"
    date: 2003-04-26
    body: "It should work.\n\nKOffice 1.2 had bugs corrected in KOffice 1.2.1 if I recall correctly.\n\nSo if you still find bugs, please report them.\n\nHave a nice day/evening/night!"
    author: "Nicolas GOUTTE"
  - subject: "looks nice but..."
    date: 2003-04-25
    body: "... when will Konqueror/(KDE?) follow the XDND protocol for we can finally DND between it and Gnome/GTK2 apps?\n\nI know it is off topic, I just wanted to draw the attention of someone on the bug that was opened 1.5 years ago :-)"
    author: "oliv"
  - subject: "Re: looks nice but..."
    date: 2003-04-25
    body: "KDE follows the XDND protocol a long time ago, no?  Since Qt switched which is a very long time ago."
    author: "ac"
  - subject: "Re: looks nice but..."
    date: 2003-04-27
    body: "Unfortunately not: see http://bugs.kde.org/show_bug.cgi?id=36297\n"
    author: "oliv"
  - subject: "Re: looks nice but..."
    date: 2003-04-25
    body: "This is off topic, but I have been having these problems so..\n\nThere are still some problems.  For example I can't drag files from konqueror into a totem playlist.  Does anyone more knowledgable know who I should be reporting this bug to?  Is kde sending it wrong, or is totem recaiving it wrong.\n\nBy the same merit, why do none on my kde apps go into the gnome system tray.  Gnome apps seem to use the kde tray just fine.  Should this bug go to gnome or kde?"
    author: "theorz"
  - subject: "Re: looks nice but..."
    date: 2003-04-26
    body: "I don't really run any Gnome apps I can think of on my Gentoo system. I can't remember emerging anything except compatibility libraries. I know the interoperability work has been ongoing but I don't know what stage it's in. When encountering problems like this the troubleshooting method is to see if you can find evidence of it working in one case where if fails in a like case. The failing application will point the way. That doesn't always work and you might check freedesktop.org or look for docs or newsgroup postings to clear things up.\n\nHere's a simple rule of thumb in what I have observed. First, KDE has generally been leading in this area, being first to load Gnome and X programs as parts, use GTK themes and other areas. Second, KDE uses a very organized OO framework which means that a number of things a developer has to have little or no concern about to have those sets of features. You might have noticed talks about interface consistency and other issues that are big concerns with Gnome. They have to have a lot more developer attention in a wide variety of areas. \n\nSo first I'd try to confirm what level has been stated to be achieved, look for insconsistencies, especially with Gnome, and proceed from there."
    author: "Eric Laffoon"
  - subject: "Re: looks nice but..."
    date: 2003-04-27
    body: "Your problem with totem is the bug I mention:\n36297\nThis is KDE bug, not Gnome.\n\nFor the system tray, I don't know.\n"
    author: "oliv"
  - subject: "Re: looks nice but..."
    date: 2003-04-27
    body: "KDE/Qt do use XDND since a long time. And, if you want to draw attention to some reported bugs, you'd better vote for them at bugs.kde.org (or at least you shouldn't forget to mention the bug number >;) ).\n"
    author: "L.Lunak"
  - subject: "Re: looks nice but..."
    date: 2003-04-27
    body: "Okay, here is the bug number:\n36297\n\nI just wanted to see if people here where able to search by themselves: Conclusion, they are not. They just THINK that KDE follows the XDND protocol, while it is NOT the case.\n\nAnd I have voted for this bug. And I have reported another one similar that was classified as duplicate.\n\nSorry KDE advocates, there IS a bug in KDE XDND implementation. If YOU want to see it dissapear, vote for it, don't just play the three monkeys :-)\n"
    author: "oliv"
  - subject: "Re: looks nice but..."
    date: 2003-04-28
    body: "Sorry, but there's more serious work to do than searching for bugreports when somebody forgets to give the number.\n\nThe issue is currently being examined. Yes, there were some bugs in KDE implementation. I believe I fixed them all in my local copy, so that drags produced from KDE apps are completely correct. So far the only visible result seems to be the fact that Nautilus no longer accepts the drops. EOG and similar still don't accept them. Moreover, the test application linked in the bugreport doesn't accept drops from Nautilus. So it looks like fixing the problems in KDE won't help that much, as that's not the only things that has bugs.\n\nThis needs to be discussed with GNOME developers.\n"
    author: "L.Lunak"
  - subject: "Re: looks nice but..."
    date: 2003-04-28
    body: "Funny how this came up in the thumbnail thread on xdg-list very recently:\nhttps://listman.redhat.com/pipermail/xdg-list/2003-April/001401.html\n"
    author: "Sad Eagle"
  - subject: "Re: looks nice but..."
    date: 2003-04-27
    body: "When I drag a link from Mozilla to Konsole/KDE only the first character or two seems to get pasted.  Does it work for anyone else?"
    author: "anon"
  - subject: "Re: looks nice but..."
    date: 2003-04-28
    body: "I looked into this today together with Lubos. The conclusion is that\na) The bugreport from 1.5 ago is about missing hostnames in drags, but adding hostnames makes DND break with a lot of apps that don't expect them.\nb) Problems with DND between Qt and GTK2 apps are due to a bug in Gtk2. Will be fixed in the next version of Gtk2 I assume. See also http://bugzilla.gnome.org/show_bug.cgi?id=109495"
    author: "Waldo Bastian"
  - subject: "Re: looks nice but..."
    date: 2003-04-28
    body: "When I drag a link from Mozilla to konsole, only \"h\" gets pasted but it seems to work to Konqueror and KEdit.  Is this a bug with konsole?"
    author: "KDE User"
  - subject: "Re: looks nice but..."
    date: 2003-04-29
    body: "Thanks!\n\nSo the two/three bugs I have opened or participated too (I opened the GTK one and the KDE one which was then duplicated to the old hostname one) will soon be closed. That 's great news."
    author: "oliv"
  - subject: "So what about debs?"
    date: 2003-04-25
    body: "It's too bad that Ralf Nolden can't make debs any more :( Is there a woody source out there, somewhere?"
    author: "Joe Debian"
  - subject: "Re: So what about debs?"
    date: 2003-04-26
    body: "What keeps him from doing it?"
    author: "Anonymous"
  - subject: "Re: So what about debs?"
    date: 2003-04-27
    body: "Most likely the lack of time."
    author: "Datschge"
  - subject: "What theme/style/setup?"
    date: 2003-04-27
    body: "This may be a silly question, but what taskbar and kicker/panel theme is being used in http://www.koffice.org/kexi/screenshots/manipulation.png? It looks really cool, but I don't have a clue how to set it up :-p\n\nKyle"
    author: "Kyle Gordon"
  - subject: "Re: What theme/style/setup?"
    date: 2003-04-27
    body: "That's not Kicker. That's Slicker: http://slicker.sourceforge.net/"
    author: "Anonymous"
  - subject: "Re: What theme/style/setup?"
    date: 2003-04-28
    body: "Excellent! Cheers :-)"
    author: "Kyle Gordon"
  - subject: "Re: What theme/style/setup?"
    date: 2003-05-04
    body: "i'd love to see slicker implemented in KDE :-)"
    author: "hook"
  - subject: "Long time user"
    date: 2003-04-29
    body: "Working with koffice on SuSE 8.0 for a long time now:\nForget OpenOffice - when using KDE - Koffice offers\nthe best integration and its a great Application.\n\nT.\n\n"
    author: "Thomas"
---
On April 23rd 2002, the <a href="http://www.kde.org/">KDE Project</a> released the first beta version of <a href="http://www.koffice.org/">KOffice 1.3</a>. It comes with <a href="http://www.koffice.org/announcements/changelog-1.3beta1.phtml">many new features</a> and improvements, <a href="http://www.koffice.org/filters/status.phtml">new filters</a>, <a href="http://whiteboard.openoffice.org/lingucomponent/download_dictionary.html">hyphenation</a> and the new <a href="http://www.koffice.org/kexi/">database client Kexi</a> (<a href="http://www.koffice.org/kexi/screenshots.phtml">screenshots</a>). Read more in the <a href="http://www.koffice.org/announcements/announce-1.3-beta1.phtml">KOffice 1.3 Beta 1 announcement</a> and in the <a href="http://www.koffice.org/announcements/changelog-1.3beta1.phtml">full KOffice 1.3 Beta 1</a> changelog. First <a href="http://download.kde.org/unstable/koffice-1.2.90/">binary packages</a> are available, or grab the <a href="http://download.kde.org/unstable/koffice-1.2.90/src/">sources</a> or use the  newest version of <a href="http://konsole.kde.org/konstruct/">Konstruct</a> (cd apps/koffice-unstable;make install). The second beta and feature freeze is <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">planned</a> for June 24th.
<!--break-->
