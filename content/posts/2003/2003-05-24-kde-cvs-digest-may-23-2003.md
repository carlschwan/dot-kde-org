---
title: "KDE-CVS-Digest for May 23, 2003"
date:    2003-05-24
authors:
  - "dkite"
slug:    kde-cvs-digest-may-23-2003
comments:
  - subject: "At last, thanks!"
    date: 2003-05-24
    body: "I've been desperately accessing your site every two or three minutes for the 23/May version ;-)\n\nThanks very much, at last I can sleep."
    author: "Derek's fan"
  - subject: "YESSSS!!!! THANKS A LOT MAN! =)"
    date: 2003-05-24
    body: "I'm sure the suspense was all part of your plan ;p \n\nI've been refreshing this page like ever 15 minutes since 3 PM, hehe... I don't know what I would do without my daily servincs of CVS-Digest =p I would probably ....\n\nThanks Derek!"
    author: "Alex"
  - subject: "KUIViewer"
    date: 2003-05-24
    body: "Some more details on KUIViewer:\nhttp://geiseri.myip.org/~geiseri/pictures/computers/kuiview-1.png - Selecting a theme from the konqi part.\nhttp://geiseri.myip.org/~geiseri/pictures/computers/kuiview-2.png - Preview of the scheck theme in the konqi part\nhttp://geiseri.myip.org/~geiseri/pictures/computers/kuiview-3.png - Thumbnail preview in konqi.\nhttp://geiseri.myip.org/~geiseri/pictures/computers/kuiview-4.png - Preview of opening a UI file in the file dialog.\nhttp://geiseri.myip.org/~geiseri/pictures/computers/kuiview-5.png - KUIView shell with the properties viewer.\n\nThese also work in KDevelop and possibly with Kate too.  They will be ready and more refinied for KDE 3.2.  Im still trying to make the properties browser more usable, so any opinions are welcome, although keep in mind this is a readonly part ment only to view UI files without needing to fire up Qt Designer.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: KUIViewer"
    date: 2003-05-24
    body: "Great feature! It's a shame that I don't use designer too much. At most, I just draw a draft with it and then clean-up things by hand.\n\nBTW... (this may be OT, sorry): when I saw your screenshots... that reminded me of something: \n\nis there any chance to change the style of the \"QSplitter bar\" for konqui? Keramik's splitter style is rather bulky, and it doesn't look right at all in konqueror's sidebar. It looks like separate patch on the left, instead of a single integrated app. It's the only reason I never enable the sidebar."
    author: "uga"
  - subject: "Re: KUIViewer"
    date: 2003-05-24
    body: "UI Files are better than code IMHO for the following reasons:\na) less code to cause code/syntax errors and bugs (yes im one of those guys who believes that less human made code == less bugs)\nb) SCheck - Style guide checker for UIs can happen at preview ( not only styles but those easy to screw up keyboard accels)\nc) infinitely easier to make complex layouts that scale well on screen (lets add a 5 pixel margin to everything on the screen)\nd) without compling any code i can test my widget with different styles and properties all in the designer \ne) non-coders (read designers who know a thing or two about gaphic design) can work on the UI leaveing the coders to make the code\n\nNow you may not agree with all or any of them, but I can say that these are things I have found in my experiance with Qt development for over 5 clients in the last three years.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: KUIViewer"
    date: 2003-05-24
    body: "I know the main advantages I found several things that I couldn't do with the designer for my apps:\n\na) In order not to touch what designer had done, and add my own added code, I always ended up subclassing so much that I had classes for everything except for integers and floating point numbers ;-)\nb) I didn't find the way to add simple widgets like a splitter for example. \nc) This may be only my problem, but when using layouts with spacers, sometimes designer becomes a nightmare.\nd) No, there's no d), it's not that bad :-)"
    author: "uga"
  - subject: "Re: KUIViewer"
    date: 2003-05-24
    body: "a) Okay yeah that is a point.  The latest designer offers this but im still not a fan of it.\nDont get me wrong, Im not saying \"USE DESIGNER EVERYWHERE\".  But its a powerful tool\nb) I see you found that ;)\nc) Yeah it gets complicated fast, but hey, remember trying to do this in PowerPlant, or that dumbass MSVC editor?  With Qts Bonzo Powerful layout system comes added complexity.  It still kicks ass though im my biased opinion.\nd) See above, its a powerful tool.  Its not for everything but its good for most I have done.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: KUIViewer"
    date: 2003-05-24
    body: "Oh no.... I just opened designer... and guess what? I found out the button to add the splitter. Why is it that I've never seen it before? ;-)\n\nI think I should have another look back at it..."
    author: "uga"
  - subject: "Re: KUIViewer"
    date: 2003-05-25
    body: "heh, took me a while to find it too... :)"
    author: "LMCBoy"
  - subject: "Re: KUIViewer"
    date: 2003-05-26
    body: "Maybe because it was added only in QT3..."
    author: "Romain"
  - subject: "Re: KUIViewer"
    date: 2003-05-24
    body: "<<is there any chance to change the style of the \"QSplitter bar\" for konqui? Keramik's splitter style is rather bulky, and it doesn't look right at all in konqueror's sidebar. It looks like separate patch on the left, instead of a single integrated app. It's the only reason I never enable the sidebar.>>\n\nYes its butt ugly :(\n\ndotNET draws QSplitters much nicer, but there are also some broken apps where the style has no influence."
    author: "Martin"
  - subject: "What i really want to see worked on!!!"
    date: 2003-05-24
    body: "OK, I am really tired! I can't find any really good file managers on Linux. Even Konqueror does not support basic features I need, there are tons of things that annoy me about it like the bad UI, menu clutter, stability etc. but on the top of my head I can tell you a few things I do not like.\n\n- It doesn't show free space in the selected directory! C'mon!!!\n\n- File copy/move can't resume! I mean, c'mon! I've spent 4 hours copying a file across a slow link when the network goes down momentarily when 98% done. I try to resume and it only asks to overwrite/rename/cancel/skip. (Well, at least it doesn't remove partial files when aborting transfers like M$ Explorer does.)\n\n- You can't access the file transfer queue. I'm copying a few gigs of stuff when I realize that I don't want to copy the next to last item I selected. I either have to abort the whole transfer, re-select the other files and restart the transfer, or I have to sit and wait until the unwanted file starts copying and then abort, remove the partial file, re-select the last file and re-start the transfer. I also can't add files to be transferred to that queue. Thus there is no way to copy files between more than two dirs in one go. Stupid, stupid.\n\n- Select two directories full of files, right-click and choose properties and it'll say \"2 items - 0 files - 2 directories\", and then when you delete those dirs you are happily unaware of your thousands of files being deleted. This probably isn't so very bad once you are aware of this, but I wasn't for a long time and I suspect I've deleted a lot of non-empty dirs that I thought were empty.\n\n- Split a file listing \"Left/Right\" a few times to get several panes, and then try to drag'n'drop a file from one pane to another. Since the target pane is full of files you can't drop the file anywhere because the whole row (of the \"Name\" column) of an existing file will be chosen as target. You actually have to scroll horizontally and then drop the file on another column.\n\n- It caches A LOT, and it doesn't update the cache when you re-visit the directory. The actual filesystem is thus seldom what konqueror shows unless you press \"Refresh\" all the time. In the last few hours it has also removed several files and directories from the file listing cache for no apparent reason. They reappear when pressing F5. (Why can't it start with displaying the cached file listing and do the refresh in the background to ensure it's up-to-date??)\n\n- Selecting dirs with many files really shows the bad underlying design. The file listing isn't updated until the whole directory has been scanned/cached. It isn't even cleared, which gives the impression that the newly selected dir contains exactly the same files as the previously selected dir. It doesn't even give a hint that something is happening! What's even worse is that it doesn't respond to anything while it's scanning the dir. You can't abort it e.g. by trying to select another dir instead. It seems that it doesn't even repaint the window. What's even worse is that it caches the mouse clicks you've done when you tried to change directory while it was so busy, and when it's finished scanning through the dir it responds to those long ago sent mouse clicks! C'mon! Haven't the developers ever seen a book on UI design? (Quite obviously not.)\n\n- The dir tree doesn't stay in sync with the active file listing. E.g. when dragging a file from the file listing and dropping it on another dir in the dir tree the other dir becomes selected while the file listing shows the contents of the first dir.\n\n- The trashcan is a normal directory! This must be among the most stupid design decisions ever made. The trashcan doesn't remember where the file came from. You can't set it to keep only X MB of the last deleted stuff. You can't set it to permanently delete stuff older than N days. You can't even have two files with the same name in it at the same time! (Not even if they originally came from different directories. Not that that should matter.) This is absurd! (Perhaps the trashcan isn't at all part of Konqueror...)\n\n- It keeps a lock on something in visited directories. This becomes apparent when you try to unmount something that konqueror has accessed at some point. You have to close the relevant konqueror window for it to release whatever lock it has in the mounted filesystem, so that it can be unmounted.\n\n- The TABs won't shrink after they fill up the whole row, so if you have many TABs you only see a few at a time.\n\n- No emblems like nautilus\n\n- No integrated cd burning like in windows\n\n- extremely slow on directories with thousands of items\n\n- when loading directories, such as one with pictures it seems to over lay the pictures and generally looks extremely ugly until it finishes loading, it should load smoothly like nautilus\n\n- no zoom like in nautilus\n\n- no difference in list views for different types of files, for example in music directories such as order by artist album, play time etc. like in Explorer\n No drag and drop support on Konqueror's tabs. For example if I have 2 tabs open and I take one file and drag it over the second tab, I expect that tab to be dispalyed after I hovered over it for 1-2 seconds. \n\n- UGLY, selection and text! More on this at KDE-LOOK. No shadow under screenshot or text etc.\n\n- No detailed folder view which will tell you things like how many files are in a directory. I know you can see this through preview, but I need to see it quickly from the beginning, I don't want to browse over every folder!\n\n- A few bad defaults, for example word wrap should be disabled by default IMO.\n\n- little distinction from web browser to file manager. Many file manager options not applicable tot he web are still here or used incorrectly. For example, clicking home is not going to take me to my homepage, but instead to my home directory! I suggest just spearating File Manager From Browser, it would be easier on the devs and users.\n\n- Bad usability in some areas! When saving a view profile it feels like your renaming one of the existing profiles since one is highlighted and yet if you choose a different name a new one appears. It would be better if it just had a new button, so you actually know what is about to happen.  Terrible menus context or not in many areas. For example in view there are 2 separate items one called Background Color and the other Background Imagem both fo these could easily be combined into one called background. Konqueror really needs to be simplified. \n\nWhen will Konqueror actually become a good fast file manager, not with preloading? Is any of this in the works for 3.2? PLEASE IF YOUR GOING TO DO ONE THING FOR 3.1 FIX KONQUEROR!\n\nWell, that's my rant for today. Otherwise, I like Konqueror and it is superior to most file managers in a lot of other areas. \n\nAndThanks Derek and KDE-Developers."
    author: "Michael"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "You're repeating yourself: http://dot.kde.org/1053363860/1053565469/\nGo away!"
    author: "Anonymous"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "> You're repeating yourself: http://dot.kde.org/1053363860/1053565469/\n> Go away!\n\nis this the way KDE deals with users wishes?\nthis is not XFree86.\n"
    author: "yg"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "He was already told where to go (bugs.kde.org) to post these kinds of wishlist items. It's somewhat offtopic here. "
    author: "shifte"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "First of all, I don't think this is the best place for a \"tiny\" comment like this. The mailing lists have some uses, you know?\n\nAnyway, I'll point out a couple of things:\n\n1) \"Otherwise, I like Konqueror\"\n\nReally? What is it that you like? ;-)\n\n\n2) \"No integrated cd burning like in windows\"\n\nWhat do you mean with \"integrated\"? When I right-click a directory, it says... \"Create Data CD with K3b\" Isn't that \"integrated\"?\n \n3) \"UGLY, selection and text! More on this at KDE-LOOK\"\n\nThis already turned up in the developer lists, and I think it's on the way. The patches the guy in kde-look offered required plenty of BIC changes IIRC.\n\n4) \"File copy/move can't resume\"\n\nYou never heard of kget, did you?\n\n5) \"No emblems like nautilus\"\n\nI don't know why that makes it so bad... well, everyone has their strange tastes. You know, code is coded when someone has interests...\n\n6) I didn't take the time to check all the rants...\n\n\n"
    author: "uga"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "i agree with some points but note this is a internet chat forum... trolls are ignored, if you really want to see these things haul your little ass on over to http://bugs.kde.org and start filling out wishlist items.  Now you are going to get some votes with you nice little account.  USE THEM!\n\nWe developers take these votes seriously! (at least i do)  Something piss you off vote it up, well look at it.  We dont make a habbit of ignoring users but REMEMBER we are going this for fun in our spare time.  You have to give a little to get a little. \n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "Thanks for your comments, but it is not the best place to troll!\n\nPlease submit a wish to bugs.kde.org\n\nThanks!"
    author: "goddard"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "1) - little distinction from web browser to file manager. Many file manager options not applicable tot he web are still here or used incorrectly. For example, clicking home is not going to take me to my homepage, but instead to my home directory! I suggest just spearating File Manager From Browser, it would be easier on the devs and users.\n \nI'm ambivalent about your other points.  But I agree 100% with this one.  A file manager and a web browser are completely different entities.  As a simple example, I want to use xv to view images locally, but I want them embedded when I'm browsing the web.  Well, too bad.  Either an image is embedded in both or pops up a new window for both.  I know I can use the MMB to force a new window for local images, but I don't want to do that.  I want my file manager to be a file manager.  Note: when I say embedded, I don't mean just in web pages, I mean if I click on http://somesite/someimage.jpg I want my web browser to embed the image like Netscape does.  If I click on file:/someimage.jpg (ie, I'm in file browsing mode), I want xv to pop up.\n\nNow... I do like the ability to split my window in half, with http: on one side and file: on the other (or fish, or ftp, or whatever).  But I really want a distinction between files and the web."
    author: "KDE User"
  - subject: "Why even bother calling me a troll"
    date: 2003-05-24
    body: "its obvious that even contrstructive critical comments are immidieately labeled as trolls. because, of course KDE is perfect and anyone witha  different opinion or that thinks parts of it need a lot of improvement is a troll.\n\nAlso, i did not repeat myself totally, i added about 5 more things.\n\nBTW: Onl Konqueror looknfeel vs nautilus and explorer http://www.kde-look.org/content/show.php?content=3910\n"
    author: "Michael"
  - subject: "But"
    date: 2003-05-24
    body: "Since many of you have asked me to, I will submit a few wishlists later tommarow.\n\nAlso, by integrated CD burning, I mean like in Windows Explorer. Once I open my CD RW device in a Konqueror window, if the cd I place is a blank cd or cd rw than I should be able to simply drag fils to it and than burn thm. The K3b (great app) service menu should still be available."
    author: "Michael"
  - subject: "Re: But"
    date: 2003-05-24
    body: "Oh, but that's not a KDE problem. That's a Kernel problem. I guess you refer to formats like Reinier to write CD's. Reinier is supposed to be making into into 2.6 kernels.\n\nIf the kernel doesn't support such way of accessing the CDwriter, there's no much KDE can do."
    author: "uga"
  - subject: "Re: But"
    date: 2003-05-24
    body: "Sorry, my spelling was wrong. It's called \"Mount *Rainier*\""
    author: "uga"
  - subject: "Re: But"
    date: 2003-05-24
    body: "On XP files are simply copied to a temp directory and can later be burned to CD. Works fine with ISO CDs. and very convenient."
    author: "Anonymous"
  - subject: "Re: But"
    date: 2003-05-24
    body: "CD Bakeoven supports something like this (unless it has been removed, I haven\u00b4t updated it in a while), you get an entry in the Services tab in Konqueror with a \"virtual cd\" that you can copy files to. When it's full you rightclick it and either create an iso file or just burn it.\n\n.. or something like that. I haven't actually used it, since I finally settled on a burner program. K3b is it for me. :)"
    author: "teatime"
  - subject: "This is what Nautilus does ..."
    date: 2003-05-26
    body: "... you open the window on cdburn:// or something like that and then just drag and drop the files you want to burn into that window and click burn.\n\nI'm flabbergasted that people here say it can't be done without support for the kernel etc. ???   Konqueror could easily have such a feature."
    author: "KDE Fanatic"
  - subject: "Re: This is what Nautilus does ..."
    date: 2003-05-26
    body: "I think what people would rather want is a method called \"Packet Burning\". See a windows-based product called DirectCD for an example of this. \n\nNo temp files used whatsoever."
    author: "fault"
  - subject: "Re: This is what Nautilus does ..."
    date: 2003-05-26
    body: "Packet Burning is nice... But just being able to drag files to a CD and then burn it without starting a new program (or using extra context menu items) makes the task of simply burning a permanent cd-r so much simpler for beginners - because they do not have to learn anything new - they can just copy the files to the cd and then burn it as in \"ok, now i am done - finish that cd\".\n"
    author: "Anon"
  - subject: "Re: This is what Nautilus does ..."
    date: 2003-05-27
    body: "Of course, what you mentioned could be done. It's what every burning program does actually. Copy all files to a temporal directory, create ISO image and burn.\n\nThat's not what I had in mind though. Nowadays, it's even possible to burn CD's just by drag-n-drop files to the \"CD Icon\". It's not a temporal directory, nor ISO image generation. Every single drop is *added* to the CD instantly. It's usually called packet writing I think.\n\nBut this burning type doesn't use the typical iso9660 format. IIRC it's based on the UDF format writing, which is still missing in the linux kernels."
    author: "uga"
  - subject: "Re: This is what Nautilus does ..."
    date: 2003-05-27
    body: "> Nowadays, it's even possible to burn CD's just by drag-n-drop files to the \"CD Icon\".\n\nYup.. this is what packet burning is. I think it's better to wait until it can be treated as \"just another directory\" instead of writing a kioslave or gnome-vfs target (like the Nautilus folks have done..)\n\nWindowsXP also \"collects files\" until burning, similiar to Nautilus... it doesn't do packet writing. This is why all the popular third party burning software for Windows, like Roxio Easy CD Creator Pro and Nero, include packet-writing software (InCD and DirectCD) that let you use CDR/CDRW's as basically another harddrive (except for re-writing and deleting on CDR's :))\n\n> But this burning type doesn't use the typical iso9660 format. IIRC it's based on the UDF format writing, which is still missing in the linux kernels.\n\nYep.. it's a UDF variant called CDUDF... iso9660 doesn't do packet writing at all. \n\nHowever, last time I looked, 2.5 was supposed to have some UDF writing support. Haven't gotten around to installing it yet though."
    author: "fault"
  - subject: "Re: This is what Nautilus does ..."
    date: 2003-05-27
    body: "> However, last time I looked, 2.5 was supposed to have some UDF writing\n>support.\n\nI'm using 2.5.69, but the necessary patches for packet writing are not included yet in the 2.5 kernels.  It's supposed to be getting into the kernels *before* 2.6. is released, but who knows. (For more info: http://www.kernelnewbies.org/status/Status-27-May-2003.html See at the end for missing parts)\n\nThe good thing is that IDE cd writing is now possible without SCSI emulation, which works much better. Cdrtools (cdrecord) already supports it, but K3b just refuses to write to them.\n\nI made a patch for k3b a while ago and submitted to the maintainer, but it seems that he didn't like the patch, since k3b still cannot be used in 2.5.x kernels without scsi emulation. Well, I should better call it a \"quick workaround\"\n"
    author: "uga"
  - subject: "Re: This is what Nautilus does ..."
    date: 2003-05-30
    body: "I retract what I wrote. K3b started supporting ATAPI writing in CVS. There seems to be a small problem right now though... cdrdao gives a warning, and k3b stops copying the CD... argh! nearly!"
    author: "uga"
  - subject: "Re: This is what Nautilus does ..."
    date: 2003-07-23
    body: "At K3B startup, there is a warning that cdrdao does not support direct ATAPI (even if kernels/other cdrtools does). Disk-at-once mode is not likely to work, as far as I know cd-copying is using DAO. Possible if a ISO is done first?\n\nUsing kernel 2.6-test1, no SCSI emulation (base system is Mandrake 9.1)\nK3B IS 0.9 from texstar.\n"
    author: "Gerhard"
  - subject: "Re: Why even bother calling me a troll"
    date: 2003-05-24
    body: "The fact that your comment is sincerely meant to be contructive, doen not preclude it from being a troll. KDE is not perfect (no software is probably), but we all know that. If you post a huge list of grievances, people might see it as if you were bad-mouthing KDE. Since they either put a lot of personal effort into KDE or just plainly love the system, they probably will feel inclined to respond if only to point out some of the better aspects of the system or the fact that you're not really being contructive by posting it here. Regardless of your being right and sincere it's likely to provoke a lot of reactions. Therefor you are labelled as a troll. Since the purpose (at least that's what I believe) of this site is a mix of offering a place for leisure and kde promotion, endless bickerings and arguments as such are not wanted here.\n\nKeeping that in mind, I'm actually quite surprised at the (number of) reactions so far. Most of them just are just pointing out that this is not the place for your comment instead of counter-arguing you. Maybe it's a sign that we are maturing as a community (or might that be considered a troll :?)."
    author: "M. Sypkens Smit"
  - subject: "Re: Why even bother calling me a troll"
    date: 2003-05-24
    body: "I think this behavior is mostly related to the fact that in general KDE users and developers are very openminded toward any wishes, they/we are just pitying those who think posting long messages at the wrong places will rise the possibility that those wishes acutally get realized. =P"
    author: "Datschge"
  - subject: "Re: Why even bother calling me a troll"
    date: 2003-05-24
    body: "The troll impression might come from your previous Gnome announce posting here (http://dot.kde.org/1053363860/1053646364/). And nobody here has the time to study a lengthy off-topic posting in while to see if there are five new arguments hidden inside somewhere."
    author: "Anonymous"
  - subject: "Re: Why even bother calling me a troll"
    date: 2003-05-24
    body: "Because you submit them at the wrong place... it's not like you're increasing the chances of implementing those wishes by writing lengthy off-topic mails...\n"
    author: "Tim Jansen"
  - subject: "Re: Why even bother calling me a troll"
    date: 2003-05-24
    body: "> its obvious that even contrstructive critical comments are immidieately labeled as trolls\n\nYes, because you keep on repeating yourself, and often post offtopic. The best way to reach developer's attention is through bugs.kde.org and possibly the developer mailing lists. Posting on dot.kde.org and kde-look and expecting developers to do things is not really too great. \n\nMost people who frequent dot.kde.org have a reason to check the site. Most of them are KDE fans. So it's foolhardy NOT to expect comments against yourself (i.e, being called a troll) when you post comments that are anti-KDE. You'll almost never be called a troll on bugs.kde.org or lists.kde.org, so I hope you take your wishes there. :-)"
    author: "shifte"
  - subject: "Re: Why even bother calling me a troll"
    date: 2003-05-26
    body: "to everyone else's comments, i'd just add that when you say this: \"C'mon! Haven't the developers ever seen a book on UI design? (Quite obviously not.)\" it makes it that much more difficult to take you seriously. remember that we're all working together on making this software, so insulting people and getting confrontational is retrograde to the task. was it really necessary to display a general lack of respect, or could you have delivered your message in a nicer more productive way? we're all on the same side here, you should start acting like it. i'd also ask what you've done for Free Software recently, besides use it and complain.\n"
    author: "Aaron J. Seigo"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "I wrote laste time:\n\"Now did you already look if your complaints are already posted at bugs.kde.org? If so please feel free to vote for all the stuff you complained so much about, and post every complaint which isn't there yet as new report. You don't need to complain again if you prefer to skip that all which I consider a bare minimum of contribution.\"\nhttp://dot.kde.org/1053363860/1053565469/\n\nI write this time:\nConsidering you don't even intend to fulfil the bare minimum of contribution on your own, instead just copies your own post and think it's good enough to extend your complaints barely anyone will read and take serious I can only reply: Stop trolling and waste your time somewhere else, we don't need your complaints here."
    author: "Datschge"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: "Btw, just so you know how you can be helpful: You are welcome to refer people to existing bug reports at bugs.kde.org (simply mention the bug number for every single one of your points), this way people who think the same way can add votes and comments to those bug reports and developers can easily find and look at them and possibly pick up one of the suggested solutions there. And everyone is happy (in contrary to reading off topic poorly written complaints poluting the news here on dotty)."
    author: "Datschge"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-24
    body: " - It doesn't show free space in the selected directory! C'mon!!!\nYeah, that would be nice. \n\n - File copy/move can't resume! <snip>\nAgreed, that would be nice.\n\n - Select two directories full of files, right-click and choose properties and it'll say \"2 items - 0 files - 2 directories\"\nI've never noticed that before, I usually just check the total size when I want to see if dirs are empty. I still miss the ability of DirOpus to show sizes of directiories, which was much easier then either method mentioned.\n \n - It caches A LOT, and it doesn't update the cache when you re-visit the directory. \nI've been bitten by this as well and it can be quite annoying.\n \n - Selecting dirs with many files really shows the bad underlying design. \nAgreed, the filelisting should at least be cleared when selecting a new directory.\n \n - The dir tree doesn't stay in sync with the active file listing. \nAgreed, should be fixed.\n \n - The trashcan is a normal directory! \nI rarely use the trashcan, so this isnt a problem for me. However if a windows like trashcan were to be used, it would probably give the same problems as the windows one: If you have ever tried looking in the trashcan of a windows partition from DOS or linux, you'll know what I mean.\n \n - It keeps a lock on something in visited directories.\nSo does consoles, I assume that it's necesarry to get updates from that dir, but I dont really know. =)\n \n - The TABs won't shrink after they fill up the whole row, so if you have many TABs you only see a few at a time.\nShould be an option to switch between the two modes, some people seem the prefer the current while IMHO the second is better. \n\n - extremely slow on directories with thousands of items\nIt seems quite for me, but I'm using the cvs version and it might be faster than 3.1.2.\n\n - no zoom like in nautilus\nJust curious, what is the use of this? \n\n - No drag and drop support on Konqueror's tabs. \nAgreed, would be nice to have this fixed\n \n - UGLY, selection and text!\nCant say I agree on this one. =/"
    author: "Vazagi"
  - subject: "Ok, I understand"
    date: 2003-05-24
    body: "This was the wrong place for my comments, but I never knew that bugs.kde.org listed wishes too, and I should of bothered to check. I was misleaded by the name and because in the Windows world rarely do you see such an innovative way to give the developer suggestions on how to improve their products. \n\nAn actual voting system, and a place where the suggestions are all publicly available is a great thing. Many times, I could not even find any feedback forms for Windows companies and the ones I submitted suggestions to never responded. They probably just had all the suggestions directly headed for the trash and just had that form so I would feel that they are listening to me and care about what I want. =(. \n\nAnyway, I'm new to all of this and I understand that my comments were out of context, kind of like saying how bad someone was at his funeral... \n\nPlease help me clear up the suggestions, validate that they exist in CVS too and that they are worded correctly. I run KDE 3.1.1 so there probably have been a lot of changes.\n\nI REALLY NEED YOUR HELP FOR THIS, I DON'T WANT TO BOTHER ALL THE DEVS WITH FALSE SUGGESTIONS.\n\nBTW: When i was referring t the ugly text etc. this is what I mean: http://www.kde-look.org/content/show.php?content=3910\n\nYes, that is an older KDE and GNOME, but everything is still valid. Please read his entire post."
    author: "Michael"
  - subject: "Re: Ok, I understand"
    date: 2003-05-24
    body: "bugs.kde.org is (or should be) covering everthing which is bugging someone. ;) To clarify that it doesn't only log bugs lately many links on that site has been changed to \"wish, bug or crash\" instead \"bug\" alone. Another somewhat related issue is that there's currently no real starting point to refer newbies to to learn about how to support KDE (is being worked on right now).\n\nI guess you learned it the \"hard way\", hope it won't happen to too many other people in the future. ;)"
    author: "Datschge"
  - subject: "Someone on CVS HELP!!!! Please =)"
    date: 2003-05-25
    body: "Try to validate that my complaints have not been dealt with in CVS so that I may not bug the developers with reports of issues they have already dealt with. If you find any that are invalid or fixed in CVS please tell me right here. Even fi you find nothing, at least tell me you looked.\n\n\nI will not submit any wishes or bug-reports until someone can do this. Thanks a lot in advance."
    author: "micahel"
  - subject: "Re: Someone on CVS HELP!!!! Please =)"
    date: 2003-05-25
    body: "Uh, this is one of the purposes for which bugs.kde.org exist. Nobody is looking at dot.kde.org for checking what bugs and wishes exist and which ones of them are resolved already. (So yes, your complete threads should better be in bugs.kde.org instead, this is simply the wrong place for such kind of issues.)"
    author: "Datschge"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-25
    body: "like in windows, like in windows, like in windows ...\nYou want windows ? Use windows \n"
    author: "anonymous ..."
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-25
    body: "No, i don't want windows, but it is stupid to ignore that it has some good features too. Not all of it is bad, in fact XP is quite good too."
    author: "micahel"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-26
    body: "Well, maybe you're using some other Konqueror than me, but:\n\n- It caches A LOT, and it doesn't update the cache when you re-visit the directory.\n\nDon't know what you mean, I've never experienced such behavior - my dirs always get updated when something changes...\n\n - Selecting dirs with many files really shows the bad underlying design. The file listing isn't updated until the whole directory has been scanned/cached.\n\nMay be true, but I've never experienced this, too - any dir simply gets scanned to fast. Maybe you have a very slow system?\n\n - The trashcan is a normal directory!\n\nYes, it is.\n\n - No emblems like nautilus\n - No integrated cd burning like in windows\n - no zoom like in nautilus\n - No detailed folder view which will tell you things like how many files are in a directory.\n\nI've never missed those... - btw. Konqueror has zoom.\n\n- UGLY, selection and text!\n\nWell, better than anything I've ever seen - maybe you should go buy a Mac?\n\n- little distinction from web browser to file manager.\n\nNever split a winning team! This massive integration is about the best feature of Konqueror - if you don't like it, use Mozilla/ Firebird/ Opera/ Whatever as browser.\n\n\"When will Konqueror actually become a good fast file manager, not with preloading?\"\n\nWhat? Loading Konqueror takes less than one second on my machine, so I guess it's pretty fast allready.\n"
    author: "LaNcom"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-26
    body: "Trash can as a normal directory sucks because you can't restore files, and misses a lot of features it could have.\n\nUGLY seelction and text in comparrison to Finder, nautilus, Explorer and almost every file manager.\n\nhttp://www.kde-look.org/content/show.php?content=3910 (yes its an older nautilus and konqueror, but nothing as really changed in the look.\n\nAlso I haven't seen any zoom button like in nautilus in konqueror.\n\nI'm not ASKING TO SPLIT KONQUEROR!!! I just want it to act more like a web browser  when in web browser mode, I don't want the home button to point to my home directory for example or have access to features not useful for a web browser."
    author: "mario"
  - subject: "ALL+ BUGS HERE and A FEW MORE REPORTED ON BUGS.KDE"
    date: 2003-05-26
    body: "Okay, I reported all teh bugs like you all have been telling me. I am very very impressed with the KDE developers, they have commented and asked for more details, and given me suggestions or their experiences with it in less than 5 minutes for all the bugs I submitted and a few times less tahna  full two minutes. In addition, 4 of my bugs were marked invalid or duplicates and so now there are only 9 bugs that are valid of which only 3 are real bugs, and the rest wishes.\n\nKDe devs I expected you were very fast and committed, but WOW =) The votes thingy is also vey kewl =) So is the nice statistics you can see and the \"my Bugs\" buttona s well as \"Show Comments\" button. Oh the wole bug thing is great, and Kbugbuster front end is very coool too.\n\n"
    author: "Michael"
  - subject: "Re: What i really want to see worked on!!!"
    date: 2003-05-26
    body: "> UGLY seelction and text in comparrison to Finder, nautilus, Explorer and almost every file manager.\n\nKonq's selection and text are very comparable to Windows2000's explorer. Not really ugly at all. It'd be nice to have other modes, but nobody is really screaming to get rid of what we have."
    author: "fault"
  - subject: "Umm.. yes but we are in 2003"
    date: 2003-05-26
    body: "So please compare that to the soon to come longhorn or to Windows XP which was released in 2001 and has yet to be surpassed by linux. \n\nStop comparing old versions of windows to the latest and greatest in KDE."
    author: "Alex"
  - subject: "Re: Umm.. yes but we are in 2003"
    date: 2003-05-27
    body: "> So please compare that to the soon to come longhorn or to Windows XP which was released in 2001 and has yet to be surpassed by linux.\n\n\n1. Longhorn will come out in 2005. Hardly \"soon to be released\". \n2. Linux is ahead of windows in other things, and KDE is not Linux nor Windows.  \n3. A whole shitload of people prefer win2k to XP, and even in XP, they use the builtin-\"Windows Classic\" visual style, which uses win2000's selection style. Some versions of XP even default to this, especially the very pricey ones (Advanced Server, and DataCenter, among others...)\n4. It doesn't take the two-three year gap between 2000 and XP to develop such a feature as XP's selection style... Nobody has yet implemented this in KDE because no developer, as of yet, really cares enough to do it."
    author: "fault"
  - subject: "Re: Umm.. yes but we are in 2003"
    date: 2003-05-27
    body: "Longhorn may come in late 2004. Well, even more people use the standard XP theme. Besides GNOME has it =p"
    author: "Alex"
  - subject: "Re: Umm.. yes but we are in 2003"
    date: 2003-05-27
    body: "Gnome or just Nautilus?"
    author: "Datschge"
  - subject: "Faxing with KDE"
    date: 2003-05-24
    body: "I see I can send faxes with KDE Print. What do I *receive* faxes with? Is there any fax program frontend in KDE that could help me receive faxes? I am way too lazy to use the command line and stubborn enough not to use efax-gtk.\n\nThanks for your advice.\n\nZoltan\n"
    author: "Zoltan Bartko"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-24
    body: "How about KVoice? http://apps.kde.com/na/2/info/id/590 KSendFax? http://apps.kde.com/na/2/info/id/194\n\nGeneric advice: do a search on apps.kde.com for finding KDE apps. ;)"
    author: "Datschge"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-24
    body: "Dear Datschge,\n\nI must tell you I had a look at apps.kde.com, even freshmeat.net and I have not found anything significant.\n\nKVoice is way too old (who uses kde 1 these days?) and the ksendfax documentation says nothing about receiving faxes.\n\nAnyway, thank you for your help.\n\nBest regards\n\nZoltan\n"
    author: "Zoltan Bartko"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-24
    body: "Hi Zoltan,\n\nyou are right. Seems like except for special cases like KMLOFax and HamFax there's not enough demand (or not enough motivation for the developer) to keep fax receiving applications up to date (didn't even notice that klprfax and efax got removed from kdeutils in CVS). Oh well..."
    author: "Datschge"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-25
    body: "This looks like an example of a situation where cooperation between the desktop environments instead of competition would actually be beneficial. A certain app exists for Gnome, but not KDE, and demand obviously isn't high enough for a KDE app to be created / maintained.\n\nSo I guess competition doesnt _always_ lead to better products...\n"
    author: "Apollo Creed"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-25
    body: "Cooperation between KDE and Gnome already exist. Competition only exists in the eye of the beholders, many developers and users simply don't care about \"KDE versus Gnome\" stories, they are mostly by outsiders.\n\nThat being said: how do you think a better cooperation between KDE and Gnome could lead to a fax application for KDE? In any case you need a developer interested in either picking up an outdated KDE app, porting a Gnome app written in a different language for a different toolkit or writing a new fax app from scratch."
    author: "Datschge"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-26
    body: "I see your point, I wasn't exactly clear, sorry.\n\nHere's what I wanted to say: Sometimes, there are arguments about whether it'd be better for the \"Linux Desktop\" if there was only one leading, popular desktop environment instead of two. Some people think it'd be better if one DE ruled the Linux desktop; others think competition between two (or more) DE's lead to better DE's overall.\n\n(I tend to agree with the former standpoint; if there was only one DE, commercial companies would know which modern DE to target for Linux apps instead of resorting to Motif apps, and we wouldn't be using feature-complete but insanely ugly AcobatReaders. ;)) \n"
    author: "Apollo Creed"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-26
    body: "I'm receiving faxes with KMail. Every in hylafax incoming fax is converted to PDF and sent to my inbox. I don't need a fax-receifing Software."
    author: "Manfred Tremmel"
  - subject: "Re: Faxing with KDE"
    date: 2003-05-26
    body: "Thanks for telling us about this, now after some googling I'll happily refer people to http://www.hylafax.org/howto/delivery.html#ss4.2 in the future. =)"
    author: "Datschge"
  - subject: "State of CVS, and KDE 3.2"
    date: 2003-05-24
    body: "I've been using CVS for a few months now, and so far, the experience has been great. There are a lot of improvements over KDE 3.1. However, personally, before KDE 3.2, I'd like to see the following things. /me hopes that these get done:\n\n1. konqueror ported to the new tab code, with features of tab moving, tab drag and drop, tab detaching, and at least possible features of close button on each tab (versus close button in right side), tab highlighting (on new data), tabs on left and side (rotated!)...\n2. I'd also like konsole to be ported to the new tab code... big inconsistancy to have so many different tab codes!\n3. kwallet made, stablized, and finished..\n4. faster selection in khtml-- perhaps gecko's is faster because it only uses one color instead of inverting colors in khttml. Also, being able to select Images in Gecko is very nice!\n5. hope that the WYSIWYG mode in quanta gets stablized! (doesn't have to be feature complete)\n6. a kdevelop-gideon beta1 at the same time as KDE 3.2 announce? That would be sweet! (and perhaps a gideon final with KDE 3.3.3 :))\n7. a new koffice version with 3.2. It's gotten a LOT faster recently, especially kspread.\n"
    author: "tril"
  - subject: "Re: State of CVS, and KDE 3.2"
    date: 2003-05-24
    body: "\"5. hope that the WYSIWYG mode in quanta gets stablized! (doesn't have to be feature complete)\"\nI think the basic VPL (Visual Page Layout, i prefer this instead of WYSIWYG :-) mode i.e. delayed synchonization between the Quanta editor and the kafka view should be done for 3.2, on condition that i get some more free time! I'm currently working alone on kafka, and this is a huge work(for me :-)"
    author: "Nicolas Deschildre"
  - subject: "Re: State of CVS, and KDE 3.2"
    date: 2003-05-24
    body: "Unless something goes madly wrong, a first Gideon (KDevelop-3) beta will definitely be out this year. The as yet unspoken intention is to go final with KDE-3.2. \n\nMore users (== testers) will certainly help. :)"
    author: "teatime"
  - subject: "It's my birthday"
    date: 2003-05-24
    body: "and thanks for this wonderful gift, Derek!"
    author: "Anon"
  - subject: "OT: Just wondering"
    date: 2003-05-24
    body: "I have seen lots of progress from KDE since January when 3.1 was released and I am wondering, when will an aplha of KDE 3.2 be released. yes, I know that the labels probably don't matter, its just a mental thing for me, and more distros package KDE when some kind of label is put on it. It's been a long time and I really want to know why there's no alpha.\n\ne expect nothing but the best work from DKe developers and I know they do too and want to take their time, but is tehre a problem or why is there no alpha? thanks in advance."
    author: "Alex"
  - subject: "Re: OT: Just wondering"
    date: 2003-05-25
    body: "Releasing a Alpha in terms of KDE development historically implies a feature plan freeze (so that there is plenty of time for stablization) I don't think anyone wants that right now, as this time seems to be the phase in KDE development that features get thought of, started, and developed. \n\ni beleive alpha1 will come out somewhat between now and the annual KDE developer's conference in august (in Nove Hrady, Czech Republic this year)"
    author: "lit"
  - subject: "Transparent tasklist buttons"
    date: 2003-05-24
    body: "The transparency feature works like a charm except for the fact that taskbar buttons are not translucent. Is this possible to add w/o recoding a lot?"
    author: "Tabber"
  - subject: "Re: Transparent tasklist buttons"
    date: 2003-05-25
    body: "No, it's not. I had some complains about it and if I'll have enough time I'll work on it."
    author: "Zack Rusin"
  - subject: "Re: Transparent tasklist buttons"
    date: 2004-07-12
    body: "Any developments on this, cos I think its a major gap :/"
    author: "Uresu"
  - subject: "Konqy context menus"
    date: 2003-05-24
    body: "Aaron recently cleaned up some of the menus in Konqueror but IMHO they're still too cluttered. Also the image context menu hasn't been cleaned up yet. It should be much smaller than the page menu since there are fewer things one can do with it.\n\nPlease vote for Bug 53772 at http://bugs.kde.org/show_bug.cgi?id=53772\n\nHere's my proposed image context menu:\n\nView Image \nCopy to Clipboard \nCopy Image Location \n ----- \nSave Image \nSend Image \n ----- \nStop Animations \nImage Properties \n -----\n(Linked image menu)\n  -----\nOpen Link in New Window\nOpen Link in New Tab\nOpen LInk in Background Tab\n"
    author: "Antiphon"
  - subject: "Re: Konqy context menus"
    date: 2003-05-25
    body: "(I guess we need a global message board at bugs.kde.org where people can promote their reports...)"
    author: "Datschge"
  - subject: "Re: Konqy context menus"
    date: 2003-05-25
    body: "No. Why promotion (lengthy duplicate postings?) instead of arguments/comments and voting?"
    author: "Anonymous"
  - subject: "Re: Konqy context menus"
    date: 2003-05-25
    body: "Don't ask me, ask those who post messages promoting specific wish & bug reports at places like dotty where they are off topic most of the time but reach a larger audience. Your answer is not convincing any user to stop promoting \"their\" reports here, while having a board for such posts whould at least allow us to refer them to there while deleting them here as off topic."
    author: "Datschge"
  - subject: "Re: Konqy context menus"
    date: 2003-05-26
    body: "But they only post them here because here is an audience... an audience that is probably getting smaller and smaller with each off-topic posting though...\n"
    author: "AC"
  - subject: "Re: Konqy context menus"
    date: 2003-05-26
    body: "That's why I suggest having a separate board to which we can refer them while moving the off topic but b.k.o related stuff to there. It's not like off topic posts here don't get any valuable response, so such a separate board wouldn't be born dead but could ensure that article boards stay on topic.\n\nNavindra, what do you think?"
    author: "Datschge"
  - subject: "Re: Konqy context menus"
    date: 2003-05-27
    body: "I don't see how my post was ot since one of the things mentioned in Derek's article was the fact that the Konqueror menus have been revised. This has long been one of the nagging bugs in KDE UI. However, this work hasn't been completed.\n\n"
    author: "Antiphon"
  - subject: "Re: Konqy context menus"
    date: 2003-05-27
    body: "there will likely be more work done on konqi's menus for 3.2... if you really want to be helpful, here's what you could do: start keeping track of every menu entry you select, and preferably the order you select them in, while doing normal work/browsing/file management. start a file and enter each menu item select, one per line, something like \"Context / Open In New Window\" or \"Location / New Window\"... do this for a week or two and then post the file somewhere we can get to it for analysis. i suppose we could also instrument the code base to generate the files more or less automagically and distribute patches ... i'd just hate to invest time in that only for it to go unused (especially by the people for whom the data would be the most meaningful: average users) ... yes, doing it manually would be a lot of work, but just think how much work many of the coders put into making KDE better =)"
    author: "Aaron J. Seigo"
  - subject: "Good and bad"
    date: 2003-05-24
    body: "Great work, although it's a bit disappointing to see the features column filled and the optimize column completely empty :(. I'm not saying KDE is very slow but it can always be improved.\n"
    author: "zank"
  - subject: "Re: Good and bad"
    date: 2003-05-25
    body: "Premature optimization is the root of all evil. One cannot optimize without being reasonably feature complete.\n\nThe optimization required may be in my grey matter. Or maybe that would be a feature?\n\nEssentially, if the commit comment refers to a speed increase, it gets chosen for optimization (most of the time). So I'm sure I miss alot of optimizations that are side effects of code cleanups, more correct code, or even a change with a precise description without reference to it's speed improvement.\n\nI'm sure this is the case. I noticed cvs konqueror being a bit slow a few weeks ago, then it sped up again. I recompile about twice a week. Some change made the difference, fixing a bug, that I didn't flag as an optimization, or maybe didn't notice at all.\n\nThe focus right now seems to be finishing up the features needed for 3.2, and bug fixing.\n\nDerek"
    author: "Derek Kite"
  - subject: "Konqueror and free disk space"
    date: 2003-05-26
    body: "\nThis has already been asked:\n\nhttp://bugs.kde.org/show_bug.cgi?id=19886\n\nhttp://bugs.kde.org/show_bug.cgi?id=24143\n\n\nPeople want that !!!"
    author: "yg"
  - subject: "Re: Konqueror and free disk space"
    date: 2003-05-26
    body: "Ask your friends to vote for them then."
    author: "Datschge"
  - subject: "Re: Konqueror and free disk space"
    date: 2003-05-26
    body: "Uh, first let someone reopen them or look for similar reports which are still open, those two bugs got closed even before bugzilla was used there..."
    author: "Datschge"
  - subject: "Re: Konqueror and free disk space"
    date: 2003-05-26
    body: "\nhow can I reopen a bug-report?"
    author: "yg"
  - subject: "Re: Konqueror and free disk space"
    date: 2003-05-26
    body: "You can't. Enter a well written new report pointing out the problem and suggesting all possible and ideal solutions if you think it's worth the trouble."
    author: "Datschge"
  - subject: "Koffice startup"
    date: 2003-05-26
    body: "oh, well,\nkspread from cvs now has a new startup - the handy feature\n'start with previously used document' including MRU list has gone !\nthere is not even the choice of staring with an empty document...\nI wonder who has decided do make such 'improvements'..."
    author: "Hoernerfranz"
  - subject: "Re: Koffice startup"
    date: 2003-05-26
    body: "It's not only KSpread, this affects all KOffice apps.\n\nBUT: This new dialog just appeared and it will be adjusted. Just follow the thread on the respective mailingslist (koffice-devel).\n\nWould be a nice addition, if you start to discuss there and not here.\n\nPhilipp"
    author: "Philipp"
  - subject: "konqueror close tab"
    date: 2003-05-29
    body: "using a mouse over on tab icons to reveal a close button seems more of an evil hack than something good. either you add permanent close buttons to each tab or make the user use the middle mouse button maybe (it reloads the last link gone to currently ;/) or just right click than select close tab. maybe add mouse wheel support to cycle through the tabs (any kind of tabs, even different konsole sessions).\n\nmeh, so far kde is nice so im not complaining that much at all."
    author: "not_regisitered"
---
Menu usability gets improved. <a href="http://printing.kde.org/">KDE Print</a> gets printer capability access and quite a few bug fixes.
<a href="http://kate.kde.org/">Kate</a> now has command line access to variables, similar to Vim commands or Emacs local variables. Plus numerous fixes
to keyboard handling, <a href="http://www.koffice.org/kspread">KSpread</a> and <a href="http://www.konqueror.org">Konqueror</a>. All this and more in the <a href="http://members.shaw.ca/dkite/may232003.html">latest KDE-CVS-Digest</a>.
<!--break-->
