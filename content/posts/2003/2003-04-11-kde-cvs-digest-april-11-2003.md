---
title: "KDE-CVS-Digest for April 11, 2003"
date:    2003-04-11
authors:
  - "dkite"
slug:    kde-cvs-digest-april-11-2003
comments:
  - subject: "oblig"
    date: 2003-04-11
    body: "thanks Derek!"
    author: "LMCBoy"
  - subject: "go kword"
    date: 2003-04-11
    body: "Thought kword is not my main word processor, I do use it a lot.  It has taken over for all my small doc needs.  I find it to be the easiest for layout, but I still need msword for grammar checking my poor english.  A big thanks to all the word developers.\n \nAnyways, I thought I read about all the open source word processors switching to the open office file format.  I have seen that both kword and abiword have put a lot of work into open office filters.  Is it true that they are all switching to the same format, or did I just make it up and convince myself I read it somewhere?"
    author: "theorz"
  - subject: "Re: go kword"
    date: 2003-04-12
    body: "is there is any grammar checking k project in progress?"
    author: "babo"
  - subject: "Re: go kword"
    date: 2003-04-12
    body: "Is there a free backend for grammar checking?"
    author: "Tim Jansen"
  - subject: "Re: go kword"
    date: 2003-04-12
    body: "What I have heard is that a comittee (in which Sun is  a member) is developing a standard format, which will then be used by OpenOffice and hopefully all other Office suites.\n\nCan somebody confirm that? And what does the future of the KWord format look like?"
    author: "Roland"
  - subject: "Re: go kword"
    date: 2003-04-12
    body: "The first step is that KOffice has well working filters for the OO file formats. When it is done, we will be able to decide what the next step might be. The current direction is to replace the KOffice file formats by the OO ones or by the OASIS ones.\n\nHave a nice day/evening/night!"
    author: "Nicolas GOUTTE"
  - subject: "Re: go kword"
    date: 2003-04-16
    body: "Have you used tables in Kword ?\nI find it horrible(I am using koffice 1.2, I don't\nknow whether it has improved since then).  Other \nthings were all good, particularly the ease with which \nframes can be managed.  And another problem is the \nlack of math support (kformula is not usable)\n"
    author: "asok"
  - subject: "konqueror \"quick tabs\",... again gone?"
    date: 2003-04-12
    body: "As I got no response in kde-devel... I hope voicing it here someone might hear it.\n\nThere was a fix from Zack Russin, something like 10-15 days ago, fixing the tabs of konqueror, so they opened immediately even if the connection was slow.\n\nThis doesn't work anymore in CVS Head at least for me... is it gone? "
    author: "uga"
  - subject: "Re: konqueror \"quick tabs\",... again gone?"
    date: 2003-04-12
    body: "If it no longer works, then why don't you re-open the original bug report?\n"
    author: "me"
  - subject: "Re: konqueror \"quick tabs\",... again gone?"
    date: 2003-04-12
    body: "Maybe because I cannot reopen a bug like that? The bug can only be reopened by the person that has been assigned to as far as I know. \n\nThe only thing I could do is reporting a new bug, which would be immediately tagged as (duplicate of...) and would be ignored.\n\nI just want someone to confirm this thing. Otherwise there's nothing to do."
    author: "uga"
  - subject: "Re: konqueror \"quick tabs\",... again gone?"
    date: 2003-04-13
    body: "Hi, I'd appreciate if you'd stop misspelling my name. The fix to focus opening tabs in backgrounds committed after mine was incorrect and it breaks opening in backgrounds if you didn't check the option to open new tabs after the current tab. I have to pick my brother from the airport today so I'll fix it tomorrow."
    author: "Zack Rusin"
  - subject: "Re: konqueror \"quick tabs\",... again gone?"
    date: 2003-04-13
    body: "Sorry about that.... I thought I copy-pasted the name? Maybe Ctrl+C Ctrl+V has another bug? ;-)   Sorry, and thanks a lot for the info."
    author: "uga"
  - subject: "Re: konqueror \"quick tabs\",... again gone?"
    date: 2003-04-15
    body: "It works for me. You have to de-select the option \"Automatically activate new opened tabs\" in Konqueror's configure dialog (in the \"Behavior\"-tab). "
    author: "Fredrik Edemar"
  - subject: "Why KPlayer?"
    date: 2003-04-12
    body: "I'm really wondering why kplayer is in development even though kmplayer(http://kmplayer.sourceforge.net) is already much further along and integrates very nicely already with the rest of kde?  It seems to me that the goals of both these project are the same (make a kde frontend to mplayer that integrates with the rest of the environment) and having kplayer in development just seems like a lot of duplicated effort....."
    author: "Joseph Kowalski"
  - subject: "Re: Why KPlayer?"
    date: 2003-04-12
    body: "From what basis do you say that kmplayer is ahead of kplayer? They started development pretty close to each other :)\n\nI personally like kplayer's interface better (the slider to track around a file is nice)"
    author: "fault"
  - subject: "Re: Why KPlayer?"
    date: 2003-04-12
    body: "I also prefer KMPlayer (except for the missing slider :-)\nIn the end we will maybe end up in two unfinished players instead of one really good one. Again... Seems like a very common way in the open source world."
    author: "Peter"
  - subject: "Re: Why KPlayer?"
    date: 2003-04-12
    body: "It integrates nicely into konqueror for playing videos on the web for one....I don't think kplayer does this....."
    author: "Joseph Kowalski"
  - subject: "Re: Why KPlayer?"
    date: 2003-04-13
    body: "I think the writer of this cvs report was confused by the name KPlayer vs KMPlayer. KMPlayer is in cvs (http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdeextragear-2/kmplayer/) and a new version will be released next week.\nAmong other things, it does feature a position slider now. Btw, if anyone follows the kde from cvs, please give it a try before and report any bugs found."
    author: "koos"
  - subject: "About Krita"
    date: 2003-04-12
    body: "Anybody knows that is it included in next beta Koffice?"
    author: "Anonymous"
  - subject: "Re: About Krita"
    date: 2003-04-12
    body: "Krita won't be included, it's far from being finished."
    author: "Lukas Tinkl"
  - subject: "Re: About Krita"
    date: 2003-04-12
    body: "Then there is no raster graphics project for kde now ?\nI mean usable of course...\nI heard of something named kpainter. What is it ?\n\nIt's a pain since it is THE big lack for kde. And when a project like quanta grows very fast, it means it will lack such an app to work with."
    author: "LC"
  - subject: "Re: About Krita"
    date: 2003-04-12
    body: "Well, it is the old problem of KOffice. If there is nobody to develop an application, it will not become better and as the rest of KOffice becomes better, the application even looks worse in comparison.\n\nThe death of Kontour is such an example. It can happen again.\n\nOf course, volunteers are welcomed.\n\nHave a nice day/evening/night!"
    author: "Nicolas GOUTTE"
  - subject: "Re: About Krita"
    date: 2003-04-12
    body: "And right now nobody is working on it..."
    author: "Norbert"
  - subject: "Re: About Krita"
    date: 2003-04-12
    body: "This is really a pity. I hate GIMP"
    author: "Jim"
  - subject: "Re: About Krita"
    date: 2003-04-12
    body: "Wasn't it planned to make GIMP's backend independent from its current GUI? It'd be nice to see a usable KDE frontend to GIMP sometime."
    author: "Datschge"
  - subject: "KWord is simply astounding and The Kompany Rox!!"
    date: 2003-04-12
    body: "\nNo those are not contradictory statments ;-)\n\nI see a potentially very useful synergy between projects KWord and The Kompany.  The Kompany is the sort of commercial software entity we need more of ... \n\n- global outsourced development allows engineers from around the world to get income from their work\n- intimately linked to broad open source development (contributes to and builds commercial packages out of the source code)\n- doesn't spend billions on limiting the sharing of licenses, encrypting and DRM'ing all its software\n- is content to profit reasonably rather then become a 50Billion$ company\n- sells into a potentially big (I hope huge) market of handheld devices and phones for ****reasonable prices**** (I cry with joy when I see *the real cost* of software reflected in tkc's apps for these devices: thank you thank you thank you for the sanity).\n- The devices and phones are perfectly compatible with a free desktop computer platform.  Free availabilty of the later and contributions of tkc help to sell the ultra-cheap apps for devices\n\nWhatever \"personality\" conflicts or strong words or controversy surround tkc I hop there will be 100's of companies like it all over the world. They are cutting edge. \n\nWhe I look at the OSS and tkc tools for python and Qt I get excited. Python based macros for doing amazing things with KOffice make me drool.  And via The Kompany's tools, the Hancom apps etc. I can do everything I do in KOffice on my handheld.\n\ne.g.'s\n\nI call up my computer with my telephone (tkphone answers) ... a few voice recognized commands cause koffice kexi to prepare a report in G3 or PDF format and fax/email it somewhere. ... This can be done now but soon grannies can do it.\n\nI click on a desktop icon or a cron job runs that instantly synchs everything with everything and for good measure creates a compound document with text and data pulled from various remote sources. One source is a video stream that uses Vorbis's new (soon!) video format, the other gets stuck into a frame with a bunch of links to voice mail messages in Ogg.  Via Koffice the whole thing gets exported in exquisitely w3c standards compliant XML/XHTML to a web site.  Away on holidays I log in to my personal info portal via my brother's TV settop box (based on Linux of course) ... it is running on a virtual linux machine with 500 Gigs of disk space that I rent for 25$ a month from a new upstart telco that uses IBM mainframes to run it's services.  I see a page with video, voice mail, data from my online transactions, info about the security status of my house. etc. etc. \n\nThis could happen - if it was easy to build (say my mom sent me the KOffice macros) and the essential glue (Koffice KDE) is widely available, free, modifiable and works with the small customized commercial tools builtin to phones, set top boxes, PC desktops, etc. We don't need .NET\n\nVis-a-vis MS-Office functionality welll ... can you say \"catch up, pass and dominate\" ??!!   The pieces are there."
    author: "Insane about KDE (and on drugs)"
  - subject: "Just do it!"
    date: 2003-04-13
    body: "I wish you much success. ;)"
    author: "Datschge"
---
This week in the <a href="http://members.shaw.ca/dkite/apr112003.html">latest KDE-CVS-Digest</a>, lots of work is happening on the basic services. A rewrite of the SMTP <a href="http://developer.kde.org/documentation/design/kde/ioslaves/">kioslave</a>, S/MIME support added to KSSL, and continuing improvement to the RSS <a href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/dcop.html">DCOP</a> service. KMail gets an mbox (== Unix mail spool format) import
filter. <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">KOffice</a> 1.3 is getting ready for beta, and new releases of <a href="http://edu.kde.org/kig">Kig</a> and <a href="http://kplayer.sourceforge.net/">KPlayer</a> are out. As I write this I also notice the new spell checking feature in Konqueror!
<!--break-->
