---
title: "theKompany.com: Rekall Now Available Under GPL"
date:    2003-11-18
authors:
  - "numanee"
slug:    thekompanycom-rekall-now-available-under-gpl
comments:
  - subject: "thanks tkc!"
    date: 2003-11-17
    body: "Good to see news from old faces in the KDE community. Is shawn gordon still around?"
    author: "anon"
  - subject: "woooo!"
    date: 2003-11-17
    body: "Rekall, Scribus, Kile, Quanta, ... these are potentially killer applications that deserve a front place on the Linux desktop.  KDE/Qt applications are coming a long way!"
    author: "ac"
  - subject: "Re: woooo!"
    date: 2003-11-20
    body: "Can Rekall code be used for Kexi?"
    author: "gerd"
  - subject: "Re: woooo!"
    date: 2003-11-21
    body: "Instead of simply answering yes or not, let's mention these issues:\n\n- Kexi is built upon different database abstraction library (KexiDB);\n\n- Kexi try to utilize KDE as much ass possible and required, while AFAIK Rekall has quite some wrappers around Qt to act as Qt-only app, (however KexiDB is \"mostly\" KDE-independent);\n\n- Rekall should have extracted some of its functionality as LGPL libraries to simplify reusability for Kexi\n\n- Application type: Kexi tries _not to be_ a database frontend or administration tool (almost) at all, it hides all database engine-specific issues from the user when this is expected;\n\n- Storage paradigm: Kexi not only stores all regular relational data in database servers, it also try to store a project data there\n\n- Summing up, with Kexi we get a bit new, refreshed style of a tool for creating database applications (hope, I'm not wrong :)\n\nThere are similarities in groups of features: tables, queries, reports, forms, data & schema importing/exporting - all this is requested today by people that need reliable MS Access replacement.\n\n    Jaroslaw Staniek / OpenOffice Polska\n    Kexi Project Team; http://www.kexi-project.org\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: woooo!"
    date: 2003-11-23
    body: "Rekall has always used a database abstraction layer, the concept is the same as what we built years ago with KDE-DB, which was really very cool, we had a plug in for konqueror that allowed you to browse databases and do basic queries and such, we donated it to KDE, not sure what whatever happened with it.\n\nRekall creates both native KDE and Qt versions, it's something we did intentionally so that KDE specific versions would be native to KDE and use those services while people not using KDE or needing the multi-platform version would use the Qt versions.\n\nRekall stores all of its information in a database as well.\n\nRekall is not a database administration tool, we have DataArchitect for that.  It is meant to build database centric applications in a rapid fashion and it abstracts the database at a level that even ODBC is a plug in to.  This provides both direct and speedy access through direct drivers, or more generic access through the ODBC driver.\n"
    author: "Shawn Gordon"
  - subject: "yep"
    date: 2003-11-17
    body: "Of course I am :)\n\nIn other positive news, we should be releasing a version of Aethera that works with Kolab by the end of next week.  Since Aethera runs on Linux and Windows, it will be the only Windows client that works with Kolab.\n\nShawn Gordon\n"
    author: "Shawn Gordon"
  - subject: "Re: yep"
    date: 2003-11-17
    body: "Great news. I'm looking forward trying Rekall. It looks like the Kompany has now found a business model (cross-platform QT/Python programs) that can strive alongside the GPL KDE with mutual benefits. I hope you are making enough sales to pay the rent.\n\nWhat would be a great closed-source companion for Rekall would be migration tools from Access to Rekall migrating forms, reports, database structure, macros... I don't know, Shawn, if you are planning that ?"
    author: "Charles de Miramon"
  - subject: "Re: yep"
    date: 2003-11-17
    body: "Yea a migration app would be great.  Currently you can use our ODBC driver to either write a Rekall front end to an Access database, or to copy the data as part of a migration.  Structure and data we can get pretty easily today, forms and reports might be possible, we haven't spent much time looking at it, but writing a VBA to Python convertor would be a pretty intense project.  Of course part of the reason for open sourcing is so other people can get involved.  I'd love to see something like that come available."
    author: "Shawn Gordon"
  - subject: "why giving  to opensource."
    date: 2003-11-22
    body: "So, part of the reason of giving rekall is to get more people involve.\nAny others reason you are allowed to give us ?\n\nI love to see company giving,... I just would like to understand the motivation.\n"
    author: "....."
  - subject: "Re: why giving  to opensource."
    date: 2003-11-22
    body: "I think I answered that pretty well in the press release.  Essentially in my experience in corporate america, MS Access is one of the most used tools outside of basic office suite stuff.  It's really critical to getting a job done, especially with analysts and power users.  the usefulness of your data is measured by what you can do with it.  We're a small company, sure we've got Rekall for sale at retail outlets around the world, but we can't get the critical mass and all the features in ourselves.  We did something similar with Kivio and that has actually worked fairly well but that situation wasn't properly handled and we are trying to learn from those mistakes with this release.  If a GPL version of Rekall can make it in to all the distributions by default, then we can get more people using it, and more people interested in the professional end of it, and other people can be working on it and enhancing it as well.  The license has always provided the customer with the source code, so this change isn't really huge in that regard."
    author: "Shawn Gordon"
  - subject: "Re: why giving  to opensource."
    date: 2003-11-22
    body: "Completely agree with you that there is large user base for MS Access-like applications. Huge part of these users are corporate users. And now: some of them have a need for creating and distributing their works under non-GPL. This may be because of internal company's licensing police, 3rd party components restrictions, etc. (at least some companies need a choice for distributing the works under dual licensing). Such works are usually distributed with a runtime, that would be typically downgraded version of Rekall's, MS Access', Kexi's, whatever - name it - core libraries. \n\nSo: is it planned for Rekall to enable it to this market? Looks like converting runtime part of Rekall to LGPL could solve the problem, but this is just idea."
    author: "Jaroslaw Staniek"
  - subject: "Re: yep"
    date: 2003-11-23
    body: "Uhm, I thought Kolab also runs with Outlook. Haven't tried it yet though. Just heard it."
    author: "Yaba"
  - subject: "Thanks Shawn"
    date: 2003-11-17
    body: "Shawn your work with/for KDE has not gone unnoticed.  Thanks for all the work you have done to make KDE the best Linux desktop environment in existence.  There is nothing that will propel the acceptance of Linux/KDE as a mainstream desktop solution like the proliferation of commercial KDE and QT apps.  Your work (and the work of your developers) is much appreciated... your donation of Rekall is more than expected.\n\nThank you!"
    author: "brockers"
  - subject: "Re: Thanks Shawn"
    date: 2003-11-17
    body: "Seriously, thanks very much.  It is always nice to hear kind words.  People tend to not say anything when they are satisfied, and to say something when they are not, so you tend to only hear negatives, so a nice comment is always appreciated.\n\nShawn\n"
    author: "Shawn Gordon"
  - subject: "Re: Thanks Shawn"
    date: 2003-11-17
    body: "Hi,\n\nyour move deserves deep respect. From a business standpoint, it may work out very well. Many people will want to licence your engine non-GPL. And I hope it will. :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Collaboration"
    date: 2003-11-17
    body: "Perhaps we'll see some cross-collaboration and pollination between Rekall and Kexi, or maybe a merger? They both seem to have some very neat features and together they would be great."
    author: "David"
  - subject: "Re: Collaboration"
    date: 2003-11-17
    body: "Undoubtely, at least data and/or database schema portability between both platforms can be something usefull. This can be eventually easier _if_ some of Rekall libraries could be released under LGPL."
    author: "Jaroslaw Staniek"
  - subject: "Re: Collaboration"
    date: 2003-11-18
    body: "I don't think that KDE developer community can support two database application. One of the two should be dropped immediately and the focus should be to make the remaining one up to scratch with the corresponding windows application."
    author: "Rudi the red nose elefant"
  - subject: "Re: Collaboration"
    date: 2003-11-18
    body: "Just like they cannot support two OSS browsers?\n\nLast time I checked Konqi wasnt doing so bad and neither was Mozilla.  I think you should just let the big boys handle this one for you.\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Collaboration"
    date: 2003-11-18
    body: "To be fair he did say KDE developers, and Mozilla isn't a KDE project. However I'm only nit-picking ... Rudi is obviously wrong - a merge doesn't seem likely or practical, and there's plenty of room for competing databases."
    author: "Ed Moyse"
  - subject: "Re: Collaboration"
    date: 2003-11-18
    body: "Since when is Rekall developed by the KDE developer community?"
    author: "Ingo Kl\u00f6cker"
  - subject: "Shush up, Ingo..."
    date: 2003-11-19
    body: "Don't make me get the stick!"
    author: "Joe Schmoe"
  - subject: "Re: Collaboration"
    date: 2003-11-18
    body: "One of the features of Rekall that I think is great for the average desktop user(me) is the ability to access xbase files.  I for one am not a database administrator, so I'm reluctant to even try software requiring MySQL, etc.  Even requireing SQLite is a stretch."
    author: "John Gutierrez"
  - subject: "Re: Collaboration"
    date: 2003-11-19
    body: "Uhh... requiring sqlite means \"install this 100KB library that requires no dependencies\".\n\nHow is that a tretch of anything?"
    author: "Roberto Alsina"
  - subject: "Slashdot'ed"
    date: 2003-11-17
    body: "www.rekallrevealed.org is being slashdot'd right now (2300GMT-ish) - normal service will be resumed as soon as possible:)\n\nMike Richardson"
    author: "mike ta"
  - subject: "GPLed Rekall"
    date: 2003-11-18
    body: "Hello people,\n\nI am doing some contract marketing for theKompany.com, and also want to say a big thanks to theKompany.com. I have been working closely alongside Shawn with regard to the Rekall situation, and I feel that he has made wise decision in collaboration with some other developers. theKompany.com is going to be able to offer a good portolio of services in connection to a mature GPLed application that will benefit from the free software development model.\n\nNice one. :)\n\n  Jono\n\n"
    author: "Jono Bacon"
  - subject: "Shawn, How about a server version of kolab"
    date: 2003-11-18
    body: "I think the one think I would consider buying from you is a server version of kolab.  The web page says its version1.xx, but the installation is still by compilation, and it doesnt seem like  the various components have gelled together into one app. I was trying out novell's linux services beta and, its also just ldap,samba,nds and postfix bolted together, but somehow, because of the ways its packaged, it feels whole.\n\nSomeone needs to organise kolab and put a friendly kde interface on it. it has great potential."
    author: "asto venji"
  - subject: "http://www.rekallrevealed.org/"
    date: 2003-11-18
    body: "This site is still being set up? Like the PHP-Nuke instead of a Rekall logo at the top?"
    author: "Anonymous"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-18
    body: "And there is also http://www.totalrekall.co.uk/ with logo - seems there are two parties each believing to have the copyright of Rekall and both released it under GPL? Call me confused..."
    author: "Anonymous"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-18
    body: "OK, here is the situation.\n\nI am the original developer of rekall. I am running www.rekallrevealed.org as a community site, non commercial. My main interest is in Rekall, not the twiddly bits of the site, so it will be a while before it gets all polished up. In case anyone wonders, www.rekall.a-i-s.co.uk is exacly the same site.\n\nwww.totalrekall.co.uk is being run by John Dean, who was another developer on Rekall. This site is looking to provide commercial support based around Rekall.\n\nwww.thekompany.com is of course theKompany's web site. theKompany will continue to provide binary versions of Rekall and eMail support.\n\nThe copyright to the Rekall code base is held jointly by myself, John Dean, and theKompany.\n\nRegards\nMike Richardson"
    author: "mike ta"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-18
    body: "> \n> The copyright to the Rekall code base is held jointly by myself, John Dean, and theKompany\n\nAre you sure about that?\n\nhttp://www.unixguru.com/california_law.html"
    author: "anon"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-18
    body: "English translation? US-English translation, even??\n\nMike\n"
    author: "mike ta"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-19
    body: "My quick take is that when you signed on as an employee of theKompany.com you should also have signed something that retains your rights as the creator of the program... or something like that... your typical US-law-rubish to make companies own their employees ideas."
    author: "M\u00e5rten Woxberg"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-19
    body: "Except he's not an employee, he's a contractor. Big difference."
    author: "Some guy"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-19
    body: "*Who* is a contractor?  John?  Well in that case John isn't even the main author of Rekall and *anyway* all three parties have agreed to release Rekall under the GPL."
    author: "anon"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-21
    body: "It is a pity you don't know what you are talking about and what has gone on behind the scenes"
    author: "anon"
  - subject: "Re: http://www.rekallrevealed.org/"
    date: 2003-11-21
    body: "No, not confused, just a brick short of a full load"
    author: "anon"
  - subject: "Well, we shure have some killer apps, but..."
    date: 2003-11-18
    body: "OK. we sure have OO.org, quanta, scribus, konq, kdevelop-3.0 and stuff. \n\nBut we also lack analogs of CorelDraw (Sodipodi does not match yet) and Photoshop (well -- maybe GIMP 1.3 will compete Photoshop 5.5 or something) \n\nAnd of cource -- something like DreamWeaver (Quanta is more like HomeSite)\n\nbut, unfortunately, the major issue is drivers, which generally can not be installed without compiler. \n\nand more games!\n\nWhen the above issues will be solved, my girlfriend will stop using windows at all! "
    author: "SHiFT"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-18
    body: "check out the www.inkscape.org - based on sodipodi, but improving really fast"
    author: "mark"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-19
    body: "Interesting, the primary difference between Sodipodi and Inkscape being that the latter is a C++/Gtkmm conversion of the C/Gtk orignal... I guess some toolkit independent code snippets could be shared with Karbon14 then?"
    author: "Datschge"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-18
    body: "> OK. we sure have OO.org, quanta, scribus, konq, kdevelop-3.0 and stuff. \n\nI have to admit, whenever I read something like this I always wonder \"so what are they doing besides anonymously talking about what we should be doing?\" That's not to be critical. It gives me a grin because people identify and it is rewarding to have your app liked... so in the community paradigm I guess I'm just on permanent jury duty. ;-) More people taking small actions makes it happen faster and contrary to what most people think desire compensates for ability faster than the other way around.\n\n[...]\n> And of cource -- something like DreamWeaver (Quanta is more like HomeSite)\n\n(sigh) Our objective with Quanta is to make it more like Quanta than any other app. After all why would you want something that was a faded copy of something else? Professional developers don't flinch at the cost so you don't win by saying \"but look, we're free\". Apache didn't win like that. Linux didn't either. Applications that win best of breed accolades have their own vision, as do we.\n\nFWIW HomeSite users (I should say former users) regularly tell me they have dropped HomeSite because Quanta is better. The most substantial advantage in the market for DreamWeaver is WYSIWYG. We have VPL (Visual Page Layout) in development with Quanta using the KHTML engine. We call it VPL because it is a different approach. For one thing it's geared to produce DTD compliant mark up. We're knocking the rough edges off the first incarnation now. The second incarnation will begin implementing more visible innovations. We have a new CSS tool that is XML configurable and can be user upgradable to CSS3 when it arrives. We have several plugin tools in the planning stage that exceed DreamWeaver abilities and we have Dreamweaver users telling us we are close to or already better in most areas.\n\nBeing like something, good enough or an acceptable alternative has never captured my imagination. This is what is said about OO.org and MS Office is still eating it's lunch. Please don't tell people we're \"almost as good as..\" Tell them we're \"getting ready for lunch\". Mmmm... Steamed DreamWeaver... De-lish! ;-)\n\n> When the above issues will be solved, my girlfriend will stop using windows at all! \n\nCool! Do we like get invited to a \"breaking windows\" party? ;-)\n\n--\nIn a world without walls and fences who needs windows and gates?"
    author: "Eric Laffoon"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-19
    body: "Please think if VPL is a good acronym - every time I hear it, I think of \"Visible Panty Line\"...\n\nI suppose that's just me ;)\n\nDavid"
    author: "dmp"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-20
    body: "> Please think if VPL is a good acronym - every time I hear it, I think of \"Visible Panty Line\"...\n\nI was seriously tired of typing WYSIWYG and also wanted to differentiate us from the usual interpretation of how it is done. We have no interest in doing it like FrontPage which... well it's terrible. Anyway, immediately after I came up with VPL Jono Bacon said it reminded him of \"Visible Panty Lines\". I suppose this could be a horrific vision, say on a large sea mammal, but I think the phrase was concocted to sell sheer underthings to women who wear tight pants because they want men to look. Anyway this seems more to evoke that response from Britts. Here in the US we have Victoria's Secret, which hardly seems secret at all when they are parading down the runway with nothing covering entirely visible panties. The whole thing is very humorous to me... so some people will think \"Visible Pany Lines\" when I mean \"Visual Page Layout\" (which I think is more elegant than WYSIWYG anyway). On the other hand there are those who will be watching a Victoria's Secret fashion show and when they see some particularly nice patnies they will think \"VPL... I see panties... I could be using Quanta right now to make web pages!\"\n\nI don't have any hard data yet as to how this will affect lingerie shows from guys leaving early or staring at their notebook computers but I hope it adds to the hip and sexy image of Quanta Plus. ;-)\n\nRemember, if you're looking at VPL you could be staring at what you shouldn't when you're driving, or you could be using Quanta... guilt free."
    author: "Eric Laffoon"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-20
    body: "Actually, originally VPL was something to be avoided.\n\nAnd if you shouldn't be looking at VPL when you're driving, you probably also shouldn't be using Quanta :)"
    author: "Stephen Douglas"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-19
    body: "It's funny. On the one hand, we get people saying that Open Source only copies, it never innovates. Then when somebody comes out with an innovative piece of work, it gets criticised for (in this case) not being more like a piece of commercial software.\n\nDamned if you, damned if you don't."
    author: "Stephen Douglas"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-20
    body: "I don't think it's a critisizm. I think it's more a matter that people don't see the vision that the developer has and tend to look at something in a rather static way. I also think they are willing to keep a low expectation because after all, it was free, as in free beer.\n\nAll I'm saying is that we should not allow ourselves to view things from a historically ignorant or philosophically impotent position. CP/M was king once. There was Visicalc, then 123 then Excel. Aside from the question of what was substantially better in the predicessor we know that the idea of a dynasty in software is unsupported. Microsoft is diversifying like crazy because they know their core business has limited life. Eventually it all comes down to whether our children will have access to knowlege freely or will pay Bill Gates' children every time the look something up. Silly little things like applications become the front line of that war. The alternatives are the best and worst we can imagine. In war the second place finisher is usually killed or subjigated. Making your strategy to copy the current leader is planning to be second and assuring them you will not challenge their lead.\n\nI don't expect a lot of people to really understand where I'm coming from because the one thing about history that remains constant is how few people realize the signicance of events as they unfold. We all see them clearly in retrospect. This is the opening of the most incredible time in the history of civilization. We look at our great grandparents going from horses to cars and oil lamps to electricity and as shocking as that was the level of change in the next 100 years will likely be far more incomprehensible to us today... but we do know of one big battle for control of the key to the age... information and it's management and delivery systems. Let's not walk around with a grand vision to come in second and lose. I innovate for freedom!"
    author: "Eric Laffoon"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-21
    body: "Speaking for myself, it's not so much that I want dreamweaver-free, but that I want the dreamweaver functionality. My machine at work is dual boot and I have dreamweaver on it. Currently this is the ONLY reason I use windows, as the group webpages are all made using dreamweaver. I would LOVE to be able to use Quanta instead!\n\nIf I can get KDE3.1.4 to compile on my work machine (a horrible patch RH7.3) I'll try the latest version of Quanta though ... it sounds interesting. Andby the way Eric,  I think your approach is the right one ... this is the only way we'll get true development and innovation (it's just a bit unfortunate for people like me who don't want to have to use windows to maintain webpages ;-) "
    author: "Ed Moyse"
  - subject: "Re: Well, we shure have some killer apps, but..."
    date: 2003-11-19
    body: "Have you tried OpenOffice Draw for vector-graphics authoring? It's an excellent application for doing diagrams/illustrations. If you're a graphic designer, look elsewhere, but for technical/office users like myself, OO-Draw is way better than SodiPodi etc. \n\n"
    author: "bc"
  - subject: "Kapital"
    date: 2003-11-18
    body: "Hello,\n\nI was just wondering when the next version of Kapital is due for release?  I tried the first demo, but decided I wanted to wait for a second version before using it full time.  I feel that this is a much needed application and it should be hyped more that it is.\n\nCheers,\nJesse"
    author: "biz"
  - subject: "Cool!"
    date: 2003-11-18
    body: "Thanks a lot!\n\nI hope The Kompany is doing well!"
    author: "Joergen Ramskov"
  - subject: "I wonder if the GPL release..."
    date: 2003-11-18
    body: "...has anything to do with this: http://tinyurl.com/vezg\n\nIt would be sad if it did."
    author: "Bryan Feeney"
  - subject: "I just saw this"
    date: 2003-11-18
    body: "If you've read the above you should read this. It casts a bit of a shadow on the whole thing though http://tinyurl.com/vez5"
    author: "Bryan Feeney"
  - subject: "Re: I just saw this"
    date: 2003-11-18
    body: "Wow is this true? That's terrible...."
    author: "anon"
  - subject: "Re: I just saw this"
    date: 2003-11-18
    body: "OK, there were some disagreements over the last few weeks - and don't ask me what they were - and some things were said in anger which should never have been said.\n\nTo the best of my knowledge, theKompany is not about to go under. It is certainly true that Rekall development was soaking up a lot of effort, and that the user base was not growing as fast as we had hoped. We came to the conclusion that the best way forward was to GPL the rekall code base and look to things like customisation and services as a source of income.\n\nOther products have made a go of it this way - JBoss is a good example. Would that Rekall will be as successful.\n\nMike Richardson\nlead Rekall developer"
    author: "mike ta"
  - subject: "Re: I wonder if the GPL release..."
    date: 2003-11-18
    body: "Well Mike is the primary developer of Rekall not John, and whatever problems they had with Shawn, it seems like Mike (the person who did the most work on Rekall) has made up with theKompany."
    author: "anonymous"
  - subject: "If it doesn't sell, then open-source it"
    date: 2003-11-19
    body: "I hate to say it, but it seems like this is exactly the case."
    author: "frizzo"
  - subject: "Great news, but...."
    date: 2003-11-20
    body: "This is a great news, but why this guys GPLed their software. Just for love..? Maybe Ghandi turned on a light over their sinner soul..? There was economic trobles with their company..? \n\nI loved this news, thanks Kompany, I wold like to kiss you all boys...! \n\nBut, why..?"
    author: "G. G. S."
  - subject: "Re: Great news, but...."
    date: 2003-11-21
    body: "Didn't you read the threads above?"
    author: "Andr\u00e9 Somers"
  - subject: "... and knoda"
    date: 2003-11-20
    body: "I think, besides Rekall and Kexi, Horst Knorr's knoda database frontend, based on his hk_classes has to be mentioned. IMHO this is the most mature database frontend for KDE at the moment.\nhttp://www.knoda.org"
    author: "P. Braun"
  - subject: "Re: ... and knoda"
    date: 2003-11-20
    body: "How about TOra http://www.globecom.se/tora/"
    author: "kontakt"
  - subject: "Re: ... and knoda"
    date: 2003-11-21
    body: "tora is a very nice tool, however their aims (one is datapocessing the other is database_administartion_) are different."
    author: "lucijan"
  - subject: "Re: ... and knoda"
    date: 2003-11-22
    body: "but does knoda can make ERD entity-relationship-diagram ?"
    author: "somekool"
  - subject: "Rekall"
    date: 2003-11-21
    body: "Rekall, Kexi, all of these are premature solutions (hope they will exchange code and unifie their framework), but there is a tool for oracle administration that is quite good. And what about phpmyadmin?\n\nFrom my perspective a user oriented approach may be very good.\n\nStill missing\n- OLAP tools\n- Object DBMS\n\n"
    author: "Heini"
---
<a href="http://www.thekompany.com/">theKompany.com</a> has just announced that <a href="http://www.thekompany.com/products/rekall/">Rekall</a>, the rapid application development database tool for Linux, similar in concept to MS Access, <a href="http://www.thekompany.com/press_media/full.php3?Id=242">has been released under the GPL</a>.  Rekall can be built with KDE3 support as well as Qt-only.  <i>"Rekall will be supported via a community portal <a href="http://www.rekallrevealed.org/">www.rekallrevealed.org</a>. The codebase will be available as <A href="http://www.rekallrevealed.org/packages/">source tarballs</a>, and CVS access will also be available for those who wish to stay on the bleeding edge. Feedback, bug fixes and contributions are actively sought. This is meant to be totally community driven and oriented."</i>  <a href="http://www.koffice.org/">KOffice</a> has of course evolved <a href="http://www.koffice.org/kexi/">Kexi</a> (<a href="http://www.kexi-project.org/about.html">devel</a>) in the meantime.  It will be interesting to see how things play out with Kexi being very new and Rekall relatively mature, but either way the GPL'ing of Rekall is fantastic news for the Linux community at large.  Thank you, theKompany.com for your KDE support.
<!--break-->
