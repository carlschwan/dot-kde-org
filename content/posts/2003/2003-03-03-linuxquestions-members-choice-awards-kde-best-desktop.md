---
title: "LinuxQuestions Members Choice Awards: KDE Best Desktop"
date:    2003-03-03
authors:
  - "Dre"
slug:    linuxquestions-members-choice-awards-kde-best-desktop
comments:
  - subject: "www.linuxtoday.com disagees"
    date: 2003-03-03
    body: "In a reply to a talkback at www.linuxtoday.com recently I posted this:\n\n \"KDE is the standard Linux desktop. The war is over\"\n\nIt never appeared. I have had positive postings about KDE censored by them before. I wonder why?\n"
    author: "reihal"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-03
    body: "because your lame trolls suck"
    author: "flash"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-03
    body: "It would be nice if KDE was the standard Linux desktop .. then all the apps would be made as KDE apps and the Linux desktop as a whole would be better (IMHO) -- everyone involved with linux desktops would have one clear focus --> improve KDE. Apps like kmail, koffice, kopete, notaun with the consistent interface and dialogs is so nice to use ... would be nice if OpenOffice.org and mozilla shared the same interface...."
    author: "J Alamo"
  - subject: "I disagree"
    date: 2003-03-03
    body: "\"It would be nice if KDE was the standard Linux desktop..\"\n\nAnd then everyone would develop for KDE, there'd be no competition and things would stagnate for KDE like they are for Windows right now.\n\nI like things as they are thank you very much. Although it'd be nice if Open Source operating systems had a greater desktop share overall so that more people in general contributed to KDE and its like."
    author: "MxCl"
  - subject: "Re: I disagree"
    date: 2003-03-04
    body: "You seem to think that by not having Gnome or other desktops, then KDE development would stagnate... I tend to disagree. Since there is no heirarchical dictatorship with regards to Open Source (ie commercial app -- we have no competition.. no need to invest in R&D) it would seem that competition would move from an external force to an internal force. Various distributions/developers would add KDE specific add-ons .. if the add-on is stable/popular, it could be melded into the core KDE feature set (ie Linux kernel development, apache, samba, etc) Additionally, since this would provide an open, standard development platform, there could be significantly more interest by commercial developers as the \"lowest common denominator\" would be equivilant to other operating systems (Mac OS X, Windows, etc..) that have a standard interface/feature set.\n\nThis would lead to greater desktop share overall, more contributors, more competition to get specific features added, more benefit to the overall community, etc..etc..etc.. \n\nPerhaps I am way off base, but it seems like standardizing the desktop is more of a good thing than a bad thing."
    author: "J Alamo"
  - subject: "Re: I disagree"
    date: 2003-03-04
    body: "\"Perhaps I am way off base, but it seems like standardizing the desktop is more of a good thing than a bad thing.\"\n\nIt is: see freedesktop.org\n\nForcing people to use one desktop is:\na) Impossible\nb) A really bad idea anyway\n\nBear in mind that while parts of KDE are cool, other parts aren't. Standardisation normally lets you develop something better than what the desktop projects today already have."
    author: "Mike Hearn"
  - subject: "Re: I disagree"
    date: 2003-03-04
    body: "> And then everyone would develop for KDE, there'd be no competition and things\n> would stagnate for KDE like they are for Windows right now.\n\nNo, things stagnate the way it is now. I fully agree to my previous writer that having a standard Linux desktop KDE was the right choice. Right now a lot of users can't really decide wether to go for GNOME or for KDE. Even I as developer am cut between 2 Desktops and write applications for one and then the next and both applications I worte end into shit because none of them I wrote finished. If we all would actively concentrate on one major Desktop then we would have done wonders already. Instead splitting up good apps for KDE or GNOME we would have written THE good apps for one Destkop and we wouldn't have a split up community user and developer wise. Look that's why GNOME is trying to contact KDE all the time for begging for cooperation. GNOME is IN NO WAY close to be called a serious competitior to KDE. This doesn't mean that GNOME is not having it's nice sides. They have nice technology but that's what GNOME is for the past couple of years only a plattform for new technology, nothing that is finished and could finally be used by someone. I am suffering the past couple of years on GNOME and never felt confortable with it entirely. There are always things that totally pissed me off. With KDE 3.0/3.1 things finally changed for me. I never thought that KDE went on THAT quickly. Even the rapid development under KDE is far better. The GIMP if written in C++ and QT/KDE would have been the beating hell Picture manipilating program for Linux today, konsistent, split into KParts, embedded niceley into KDE and has a consistent UI. Not as the hack it was made out of it right now with 1.3 ... My personal opinion. KDE go ahead you are doing it right."
    author: "Ex GNOME user"
  - subject: "Re: I disagree"
    date: 2003-03-05
    body: "> If we all would actively concentrate on one major Desktop then we would have \n> done wonders already. Instead splitting up good apps for KDE or GNOME we would \n> have written THE good apps for one Destkop and we wouldn't have a split up \n> community user and developer wise.\n\nCan you please define \"good\" here?"
    author: "Michele"
  - subject: "Re: I disagree"
    date: 2003-03-06
    body: "I think he means feature complete, good usability and nice looking and perhaps some other things that didn't pop up in my mind immediately."
    author: "Patrick Smits"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "I prefer to see the success of projet like www.freedesktop.org : the diversity is always a good thing (even if I'm a KDE-addict :)."
    author: "capit. igloo"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "Thanks for the info on freedesktop.org .. I looked at the mission statement and I'd have to agree that the concept of freedesktop.org is a good thing. It would be nice to see this extended further to allow GNOME apps look & feel like KDE apps or for that matter, any freedesktop.org compliant app. By this I mean, use of the system-wide themes, icons, dialog boxes (save, open are two BIG ones), general user-experience, etc.\n\nNothing bugs me more than the inconsistency among applications (granted, its getting better, but it seems to get better against desktop environments instead of for the \"common user experience\") -- hopefully freedesktop.org can go far enough to develop some type of a framework that apps can be developed against that would allow the apps to seamlessly integrate into Gnome, KDE, etc... that would be simply awesome."
    author: "J Alamo"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-03
    body: "Because your post suggest there was a war in the first place. There is none, and has never existed.\nSay whatever you want, the whole \"war\" thing is made up by GNOME and KDE zealots. The developers don't care. That's why both KDE and GNOME exist, with different designs, goals and targets.\nYou should be glad people still have the freedom to *choose* between GNOME and KDE. Saying GNOME or KDE is dead or that the \"war is over\" is denying people the freedom to choose.\n\nNow, instead of actively trying to kill GNOME or KDE (which is futile anyway), why don't people join forces and try to kill Windows instead? (Oh wait, that is no good either, people will just mod us down as \"fucking Linux zealots\"!)"
    author: "FooBar"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "I have choosen. After some years suffering on the GNOME plattform I finally made it up to KDE and will stay there. Thanks for the freedom of being able to *choose* GNOME will soon become an orphaned lonely plattform for their corporate minions. I had high hopes in the beginnings of GNOME now it matured into a pile of worthless Desktop which made me unproductive."
    author: "Ex GNOME user"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "That's your choice. Nobody blames you for choosing KDE. Congratulations with *your* decision.\n\nBut, no matter what *you* may think about GNOME, there are always people who will disagree. I know a few people who have the exact opposite experience: they chose GNOME instead of KDE.\nSo you guys choose KDE while other people choose GNOME/WindowMaker/Enlightenment/BlackBox/FluxBox/XFCE/twm/fvwm/MSWindows/whatever. No need to try to kill off each others' choice and everybody is happy."
    author: "FooBar"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "Yeah sure we are living in a free world lucekly. Anyways I made my personal decision based on a technical level from the programmers view. i had seriously hard times doing development under GNOME while there are NO programming resources, then various implementations never suited my needs thats why I left. It makes no sense staying on a plattform where I'm not sure where it goes on the long run."
    author: "Ex GNOME user"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "Lesson of the day:\n\n\"If you start off with the assumption that product (x) sucks and cannot compete with product (y), you're probably going to find evidence to support your claim... regardless of your local values for (x) and (y).\"  -- AJS"
    author: "minkwe"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "Lesson for the next day:\n\n\"I don't need to ask anyone else what to do or get blamed for my decision and opinions I have regardless of the fact if I'm right or wrong\"\n\nThis is also called individuality."
    author: "Ex GNOME user"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "No war. eh?\nMy firewall logs could tell you a thing or two about linuxtoday.com and gnomesters. I forgive them since they have lost and I pity them.\n\nThe war for the standard on the Linux desktop have been won by KDE. That doesn't mean there is no choice, like Gnome, GnuStep or whatever. Enjoy.\n\n\n\n"
    author: "reihal"
  - subject: "Re: www.linuxtoday.com disagees"
    date: 2003-03-04
    body: "\"My firewall logs could tell you a thing or two about linuxtoday.com and gnomesters.\"\n\nLike I said, the whole \"war\" thing is invented by GNOME and KDE zealots. Again: *the developers don't care!*"
    author: "FooBar"
  - subject: "KDE User"
    date: 2003-03-03
    body: "Shouldn't they push new innovative suits like KOffice instead of ols commercial projects like OpenOffice??\n\nAlso, I guess that the majority of people who use KDE also use KMail, Kate and Konqueror so these awards are not representative of what people actually use."
    author: "KDE User"
  - subject: "Re: KDE User"
    date: 2003-03-03
    body: "OpenOffice is a commercial project?\n\nAlso you guess wrong, although I use KDE I always pick the best application for the job, that is evolution for mail and groupware, phoeni for webbrowsing, vim in konsole for text editing."
    author: "flash"
  - subject: "Re: KDE User"
    date: 2003-03-04
    body: "What you meant to say was emacs for mail and groupware, emacs for webbrowsing, and emacs for text editing."
    author: "emacs heathen"
  - subject: "Re: KDE User"
    date: 2003-03-03
    body: "Actually a lot of people still use mozilla/galeon and evolution with kde.\n\nI switched over after the mess that was gnome2 and found kde3.0 a great desktop environment with inferior applications. So I continued to use evo, galeon and gaim.\n\nWhen I started using 3.1rcs I quickly found that konqueror and kmail were now at least as good as the old gnome apps (kmail still has a little way to go) and I switched to those full time.\n\nI imagine this survey was taken before many of the voters had the chance to try out the excellent progress made in kde 3.1. Konqueror is going to leap up in popularity this year if the word spreads."
    author: "Caoilte"
  - subject: "Re: KDE User"
    date: 2003-03-04
    body: "Just curious:\nwhat do you miss in KMail? (I've never used any other mail client on Linux so I really don't know what I'm missing :-)\n"
    author: "Norbert"
  - subject: "Re: KDE User"
    date: 2003-03-04
    body: "I miss the groupware and complete IMAP support that the team are promising us for 3.2. Evolution has absolutely superb support for this though, and it will be hard for the team to better it (kolab sounds cool though, really cool).\n\nEvolution also has superior searching tools (very very fast binary searches of an indexed copy of the maildir) vfolders (a sort of 'view' (in sql terms) of a mailfolder), better message marking and probably some other stuff I'm forgetting. It feels very complete when you are using it (which is ok, a year and a half ago kmail felt like a joke).\n\nBut Evolution is also slower.... hehehe. I like kmail now."
    author: "Caoilte"
  - subject: "Re: KDE User"
    date: 2003-03-04
    body: "Mostly IMAP-support is limited:\n* no IMAP-subscription\n* no IMAP-filtering (which is a good and bad thing)\n* no offline-IMAP\n* no limits for downloading large attachments (when using IMAP)\n* no html-composer (some people really want this)\n* no as you type spellchecker\n* the mailreader-window is very basic and limited\n* searching is rather simple\n* PGP/MIME is a pain to install\n* some minor stuff\n\nMany of these features are in KDE-cvs :-) (disconnected imap is very basic by now).\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: KDE User"
    date: 2003-03-05
    body: "Thank god for KMail not having a html-composer. If one really wants to attache html stuff s/he can do so by... attaching it. ;) No native support for unnecessary features like that please."
    author: "Datschge"
  - subject: "Re: KDE User"
    date: 2003-03-03
    body: "\"Shouldn't they push new innovative suits like KOffice instead of ols commercial projects like OpenOffice??\"\n\nThey're \"pushing\" what is best. Right now OpenOffice is better than KOffice in terms of functionality and stability. OpenOffice may be huge, bloated and slow, but it works well and that's what counts. We'll see if KOffice surpasses OpenOffice next year..."
    author: "David Johnson"
  - subject: "Re: KDE User"
    date: 2003-03-03
    body: "Well, at least I'd like to see KOffice rule since I'm on this list => I'm a KDE fan."
    author: "KDE User"
  - subject: "ummm...no"
    date: 2003-03-06
    body: "<BLOCKQUOTE> I guess that the majority of people who use KDE also use KMail, Kate and Konqueror</BLOCKQUOTE>\n\n\numm no.... \nI use KDE, OpenOffice, Evolution, & Pheonix\nBut I Do LOVE!, The Klipper, and Knoqueor (the file manager not the web browser), and kdevelop\n\nthe kicker isn't bad either.....\nBut The Best the best of all the aps \nKbounce!....:)"
    author: "me"
  - subject: "Re: KDE User"
    date: 2003-03-08
    body: "I wish I could use KOffice, but the fact is it isn't compatible with anything.  Even its support for other open filesystems is sketchy."
    author: "Seth Davenport"
  - subject: "Re: KDE User"
    date: 2003-03-08
    body: "I meant open file formats... sorry"
    author: "Seth Davenport"
  - subject: "KDE's application suck"
    date: 2003-03-03
    body: "KDE is the best desktop, but it's applications aren't :-(\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: KDE's application suck"
    date: 2003-03-03
    body: "Hey Guenter,\ncome on. The apps get better and better you are a good developer - you`ll bring it to the front :-)"
    author: "star-flight"
  - subject: "Re: KDE's application suck"
    date: 2003-03-03
    body: "I do my best to bring one of KDE's apps to the front... :-)\nBut I do think that KDE needs more \"killerapplications\"."
    author: "Guenter Schwann"
  - subject: "Re: KDE's application suck"
    date: 2003-03-03
    body: "There are killerapps, see kontact :), konqueror, koffice, kopete, kdevelop, ... Don?t forget the small superior tools like kprint. Give it a bit more time."
    author: "star-flight"
  - subject: "Re: KDE's application suck"
    date: 2003-03-03
    body: "* kontact - is just as good as it's komponets are. KMail's IMAP supports still lacks some important features (offline, subscribtion,...). It looks like with KDE 3.2 most of this features will be available. KAddressbook  gets more an more features (I hope there won't be too much). And korganizer is the best of all kalendars already ;-)\n\n* konqueror - as web-browser gets a lot of (needed) help from apple. But still some features woul be nice (spell checking right now as I'm typing this, format-auto-fill,...)\nAs filemanager I begin to like it more and more (together with cervisia-kpart it's very nice)\n\n* koffice - is nice for small and easy tasks. I see a lot of progress. But it's hard to exchange documents with others. OpenOffice is far ahead (feature-wise). And many many people think that abiword is better than kword, and gnumeric is better than kspread. My presentation later this week will be done with OpenImpress. Just because others need to be able to download my slides (otherwise I'd use kpresenter)\n\n* kopete - is progressing very fast. Soon it will be as good as it's kompetitors (I hope).\n\n* kdevelop - if only 3.0 final would be out! 2.x is limited to singe applications. The kurrent 3.0 cvs-version is very powerful (I'm using it). But when will it be released - in 6 month?\n\n* krita - well I think KDE will never have a painting-tool like gimp\n\n* kexi - looks like an application needed by many offices. The first usable replacement for MS Access.\n\nKDE has some strong applications. But mostly there are even stronger alternatives :-(\n(kontact - evolution, konqueror - mozilla / mc, koffice - openoffice, kopete - gaim, kdevelop - emacs?!?, ...)\n\nLet's see if KDE 3.2, koffice 1.3 and kdevelop 3.0 can turn my mind...\n\nciao\n\nP.S.: Is anyone able to \"decrypt\" the KDE 3.2 release schedule?"
    author: "Guenter Schwann"
  - subject: "Re: KDE's application suck"
    date: 2003-03-03
    body: "You are right. The advantage of KDE is the integration of ALL parts. The single apps are not so strong as it competitors, but the sum of all is the strong part. You can't catch the other apps in month, wait a year or two, then lets talking again about it ;-) Maybe koffice need much more time to catch OpenOffice. But it's ok. Do you remember KDE a year ago? Do you want switch back?\nOK, graphic-apps are not KDE's battlefield - Office-, Dev- and Internetapps are far better. I can't await kexi or quanta with WYSIWYG.\nKDE on more and more Office-Desktops here in Germany and world wide - learning by doing, growing by using. Let's wait and see."
    author: "star-flight"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "What about Quanta? Had our developer team voted we would probably have been the only KDE app winner in this poll, which as I said falls short of scientific.  Quanta doesn't fall short of anything out there and soon will be the next killer app on KDE. Also there are cool utilities like Kfilereplace. Nobody even realizes Kommander is in KDE but I predict in the coming months it will become a killer app on the desktop because it can enable an average user to extend an app or create application interaction. Currently the only app close to Kommander is Kaptain, a QT app."
    author: "Eric Laffoon"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Sorry, but I really wouldnt call Quanta a Killer app. It may be a very good application, but with a very limited target group (people who write HTML and dont want a WYSIWYG editor or use their regular editor).\n\nAnd what is Kommander?"
    author: "AC"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "> Sorry, but I really wouldnt call Quanta a Killer app. It may be a very good application, but with a very limited target group (people who write HTML and dont want a WYSIWYG editor or use their regular editor).\n \nSorry, but I wouldn't call that a very good definition of Quanta. First off it's now DTD agnostic, not just HTML. Soon it will have some XML tools for validation, web services and the like as well as a completed DocBook DTD and interface. CVS HEAD has an extremely fast and powerful parsing engine which will allowfeatures and fuctionality not possible with other text editors like off page linked document CSS, classes, functions and variables... scoped and by location. It is also under review for adding WYSIWYG right now. Add to that a number of other features and factors and then tell me what a killer app is. Does it have to be something nearly everybody uses? If it manages to attract a large number of web developers to run Linux/KDE and develop on Quanta then that is significant because any degree of migration to Linux by web developers impacts a number of web sites. To my mind this is even more significant than what your word processor is. Kword is unlikely to lure people from windows for years becuse Word is relatively adequate for most uses and Kword is a ways behind. However on the web things change much faster and productivity and being able to capitalize on change is important to web developers... Therefore a web development tool stands to make a much bigger difference sooner... also web developers are more technical, thus \"early adoptors\" who will bring their friends to Linux/KDE. Office applications are needed when the mainstream shows up, but early adoptors bring the mainstream. That's been gospel to the rest of the high tech world not stuck in a monopoly void for some time.\n\nI would say those factors give Quanta an excellent shot at being the next killer app. That is if a killer app has the potential to turn the tide for many users. ;-)\n\n>  And what is Kommander?\n\nIt's better we release our docs and tutorials because it doesn't really sink in until you see it work. It was inspired by Kaptain (kaptain.sourceforge.net) but has some significant differences. It uses a stripped and modified QT Designer (as a KDE app) to visually create dialogs. The dialog widgets can have text associated with them enabling them to construct strings. They also speak DCOP. You can create custom dialogs to output text based on selection, call scripts, run programs or issue DCOP instructions. I've used it for PHP class management and we have a few other things we're working on. It's bascially for creating a quick mini app, cli dialog, an app extention or \"application glue\" where it bridges two applictions to make them more seamless.\n\nI will try to get everyone's stuff together and post demos and docs this week. It's current;y part of the Quanta module so you can look at the editor by running kmdr-editor. I've attached a little demo/text file that can be run by running \"kmdr-executor form5.kmdr\"."
    author: "Eric Laffoon"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Eric,\n\nCall me a n00b but I have no idea how to use Kommander. When can we expect some demo's and/or documentation?\n\nGreetings,\nJohn."
    author: "John Herdy"
  - subject: "Re: KDE's application suck"
    date: 2003-03-07
    body: "Here comes a little demo. It uses 'convert' from Image Magick to create thumbnails for all images of a dir.\n\nEnjoy :-)\n\nPS: When using Kommander, be sure to set the \"Text association\" of any input widget to \"@widgetText\", otherwise you won't be able to read its content (took me a while to figure this out)."
    author: "Aur\u00e9lien"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Hi, please don't value the applications on GNOME so high. Evolution has a lot of issues. It's definately a nice application but for me as person it's to much to deal with. Most of the time I only write emails so I don't need to load up everything such as Evolution which includes more than my tasks require. Gnumeric is indeed better than KSpread with the few examples I tried but AbiWord is far from being serious.\n\nAnd now count all the 3 apps together. None of them can exchange stuff amongst them. I can't embedd addresses written in Evolution in AbiWord when writing a letter and so on. That's why I think that KDE is far supperior. You people did a grandious job with KDE 3.0 and 3.1 is even better. Even your rapid development because of C++ makes apps grow FAST and makes them become more powerful. I think within another year you bombed out all major GNOME apps. Your framework is there.\n\nI was a GNOME developer and know much about the stuff behind it and I can tell you that the libraries are far from being complete, consistent etc. Many people wrote that eveything in GNOME 2 was written from scratch which is a lie. This explains the 5 Toolbars issue and all the wrappers in the code which also explains the major inconsistence that they won't get rid of for the next 5 years. GNOME is a nice techology plattform but It's not really attractive for people who know more about it."
    author: "Ex GNOME user"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Evolution is a nice mailclient with pretty all features one needs. It has some issues, but none of them is really grave. And it loads components only when needed. GNOME has 3 mailclients - balsa is rather simple, sylpheed has everything you need and evolution is a PIM with look. KMail as the only KDE mailclient suffers many IMAP-related features. When using POP3 KMail is a pretty good choice.\n\nI do not use spreadsheets for other stuff than for listing (prices?) and do simplest calculations (sum). So I can't decide which spreadsheet is the best. I only read and heard that gnumeric is the best one for Unix.\n\nYesturday I did (again) a simple 1/2 hour KWord/Abiword test. KWord seems to have a few more features, and both are fast. Abiword's only benefit is it's platform independency...\n\nCan one really use kaddressbook's data in kword? I didn't find an option for that one. Integration doesn't seem to be that far :-(\n\nGNOME's development does indeed seem to have some troubles. Two indications:\n* GNOME 2.0 is out for 6 months now. But only very few of those \"killerapplications\" are ported (evolution, gnumeric, abiword, gimp, galeon,...).\n* Miguel de Icaza does want to use .NET (mono) because with .NET he hopes to speed up development (I'd say because .NET is ObjectOriented).\n\nciao\n\nP.S.: quanta indeed is the best web-tool :-)"
    author: "Guenter Schwann"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Well little correction. Sylpheed is NO Gnome application and Balsa itself has a lot of issues. Belive me I worked on Balsa developmentwise for a long time. I don't suggest people using it if they like to keep their important Emails for longer :)"
    author: "Ex GNOME user"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "This sounds like an autocritics to me. ;-)"
    author: "Bojan"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "\"* GNOME 2.0 is out for 6 months now. But only very few of those \"killerapplications\" are ported (evolution, gnumeric, abiword, gimp, galeon,...).\"\n\nWell, GIMP, AbiWord etc aren't actually gnome2 apps, but they have close ties with GNOME. Basically that's because GNOME2 is a big change from 1.4, so rather than simply port across the new APIs (easy) they are also bringing in the new artwork, the HIG etc etc, so it takes a long time. Also, most of these apps are doing it at the same time as adding new features in their unstable cycles etc, it's not so much that it takes a long time to port apps, it's more that those pieces of software are working towards their next version and porting to gnome/gtk2 is one of those things.\n\n\"* Miguel de Icaza does want to use .NET (mono) because with .NET he hopes to speed up development (I'd say because .NET is ObjectOriented).\"\n\nWell what Miguel uses doesn't really impact GNOME, which is written in object oriented C, GNOME is basically unaffected by Mono at present.\n\n(sigh) I wish threads like this weren't necessary. You know sometimes I think, maybe it would have been better if KDE and GNOME simply didn't exist. I don't mean, the desktop was still hard to use, but I mean for instance there were lots of mostly independant projects, one for the help system, one for the IO layer etc. It would have advantages in terms of less code duplication and such.... still, then I guess things like usability, i18n and accessability would be harder to push through... I think it's such a shame it's become a \"you're in, or you're out\" thing, leaving proprietary software and \"neutral\" software like AbiWord/GAIM (which don't use the gnome libs btw) kind of stranded in the middle."
    author: "Mike Hearn"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Didn't the development teams of gimp, aboword, etc. start porting _before_ gnome 2.0 final?!? I thought that most of those important applications would be \"2.x\"-released rather soon after the GNOME 2.0 release.\n\nAbout Icaza: I don't know how much it will affect GNOME. But for me it was strange to hear that he thinks development is too slow...\n\nBut for slow development GNOME and man of it's apps make pretty good progress ;-)\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "GNOME's apps have made good progress b/c of GNOME's slow pace of development. i.e. the api has been stable for a long time, which is necessary for an application to evolve. The problem with KDE's apps is that KDE's api is a fast moving target. What we do need for KDE is a much longer period of major stable releases."
    author: "Tormak"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "KDE's API has been stable since 3.0, and not moving a lot since 2.0\n\nThe only \"you will have to rewrite it\" move was the 1->2 (but it happened twice ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "GNOME's api sucks.  Sorry I can't understand how people use that."
    author: "ac"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Because a) there is a large number of people, especially on Unix, who don't know C++ well enough or don't like it and b) there is more literature for Gtk\n"
    author: "Tim Jansen"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Agreed. I've done a bit of gnome hacking simply because I know and understand C but not C++. Objects in C are wierd at first, and rather verbose, but just for adding the odd feature here and there it isn't so bad.....\n\nIdeally of course we'd all be using python or some other high level language, rather than C or C++, but we really need a good object model first. CORBA could be it I think, but Bonobo still has too many rough edges and I don't think KDE would accept it :( We need something like MS COM though....."
    author: "Mike Hearn"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "CORBA is simply so incredibly complicated compared to DCOP in KDE code that no one who ever used DCOP wants to go back."
    author: "Tim Jansen"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Yeah, I know it's complicated :(\n\nStill, iirc DCOP is more an IPC/scripting interface, for instance I wouldn't want to write an XML parser component using it. CORBA can do inproc in a language neutral fashion as well."
    author: "Mike Hearn"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "IIRC at least in HEAD DCOP can be used in-process as well (Kontact does it).\n"
    author: "Tim Jansen"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Are there any docs/emails on that? If not, I'll go hunting through CVSweb tomorrow.\n\nI was under the impression DCOP didn't do objects or garbage collection or anything like that though, it was just an end-to-end messaging system."
    author: "Mike Hearn"
  - subject: "Re: KDE's application suck"
    date: 2003-03-07
    body: "CORBA is complicated for the same reasons C++ is. C++ gives a programmer advanced features and because of this it's more complicated. The same goes for CORBA. CORBA is a very advanced, mature architecture, which also means is complicated. But if you would load DCOP with the same features CORBA has, you would reach the same complexity."
    author: "Takis"
  - subject: "Re: KDE's application suck"
    date: 2003-03-07
    body: "But the result of C++'s complexity is very different. C++'s features make it easier for me to implement stuff, compared to C. There's less to type, more things get done automatically (especially oo and memory management).\nBut CORBA's complexity makes it harder, compared to DCOP. It's much more difficult to write a CORBA server and to use it from command line tools.\n\nThere may be cases where CORBA would make my life easier with its capabilities, but I have never encountered such a situation on the desktop.\n"
    author: "Tim Jansen"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "And what about when GNOME switches to Mono?  Are you going to whine that you understand C and can't hack GNOME anymore?"
    author: "anon"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "The beauty of Qt is that you don't need a bible of documentation on it.  Go to trolltech's website and you have more than enough documentation.  I highly doubt that GTK documentation is any better than this.  The last time I looked it was incredibly hideous.\n\nYour reason that people don't understand C++ is something of crock given the incredibly complicated and messy OO C code that GNOME uses!"
    author: "anon"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "The problem is not learning Qt or Gtk. There are a lot of people who know or prefer C, and it seems like they rather stick to C and learn a complicated library that makes their lives miserable (:-) than learning C++ and using a simple library. \n\nI can understand that somewhat, since I used Gtk for a while because of my lack of C++ experience."
    author: "Tim Jansen"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Learning enough C++ to write a Qt app is at most a month-long project.\n\nAll you need is:\n\na) classes, with overloading\nb) casts (the new kind)\nc) templates\nd) STL containers, if you wanna be fancy (not really needed, if you stick to Qt classes)\n\nOf course C++ has a bazillion other things, but this is all you REALLY need. You will be writing in some sort of C+ in no time, and even that crippled language is incredibly easier in the long run than using GTK+'s objects in C."
    author: "Roberto Alsina"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "I'd say (c) needs to be qualified with \"using \", too. No need to know how to write one to use one, really. And certainly no need to be able to implement STL or Boost :-)\n"
    author: "Sad Eagle"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Oh, yeah, just \"using templates\" is what I meant :-)\n\nBTW: I barely know any more C++ that this!\n\nAnd, here is the winning point: if you want to develop GNOME apps, you have to learn all that anyway.\n\nOh, sure, it is not the same. The difference is that when you write GNOME apps, you have to do classes without the C++ syntactic sugar[1]. When you use glib containers, you do template containers without the STL syntactic sugar[2], and so on.\n\nSo, what is easier, learning a few language extensions (really, it is not even much syntax): you have <>, you have the keywords class, public, private, protected (FORGET ABOUT THEM, make everything public, if it is not a library, noone cares), const... not much else. Maybe smart pointers to make life easier, but you could still do it the C way if you like pain.\n\nNo, this is not a month, this is two weeks learning, TOPS.\n\nSure, you will then have to learn when and how to apply your new knowledge, but the same is true on the GNOME side.\n\nSay yes to syntax sugar, say no to square pegs in round holes!\n\n[1] And with function pointers\n[2] And with void*"
    author: "Roberto Alsina"
  - subject: "Re: KDE's application suck"
    date: 2003-03-07
    body: "The point is, that sooner or later some guy will actually use all of C++'s features -which is a good thing in itself- which will make the code uncomprehendible by your 2-weeks old C++ hacker.\n\nJust take a look at the STL sourcecode and compare the implementation of queues, lists with glib's lists and queues. \n\nAnd no, I don't prefer C myself. But claiming that C++ is simple is ridicilous. Just compare the Stroustrup C++ book with any C book. Surely, C++ is more powerful, but that power brings its problems as well."
    author: "Takis"
  - subject: "Re: KDE's application suck"
    date: 2003-03-07
    body: "Probably not.\n\na) This guy is a beginner but he is not stupid. He will learn more C++ eventually, so this is just a window of problem. And as you say, \"sooner or later\", and later is not a problem.\n\nb) If he is the main author of the app he is working on, this is really not a problem, because most code is written by him\n\nc) Even if the app is developed by a group, usually contributions have locality. \n\nd) I see KDE code every day. Most apps ARE written with very basic C++\n\ne) Set policy. Mozilla did ;-)\n\nf) Use QList if STL is too hard\n\ng) Comparing books is stupid. C++ has more syntax, and more features. But those features and syntax fall in two categories:\n\n* Those who make things easier\n* Those who don't\n\nThe first are worth learning, the others aren't. On C, you have no features of the first kind, and you have to do it in your own code, which is why many C libraries have small APIs but are hard to use.\n\nIn short: you can't have it both ways. Either C++ makes life easier and it is worth learning, or it doesn't and it isn't. If so, then C is worse, since you could trim C++ to fit C."
    author: "Roberto Alsina"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Well, the GIMP and GAIM are just really big updates. The Gimp introduces new tools, new colour abilities, new artwork, totally revamped text handling, new scripting etc..... they also aren't GNOME apps, they just use GTK+ and parts of its HIG, but not for instance GConf or gnome-vfs.\n\nI can't wait for GIMP 1.4 :) I use the unstable series and they really kick ass! Gaim is starting to look nice also, I was going to try kopete because I never liked the Gaim UI, but read about instability and i couldn't be bothered installing everything needed. Now it looks like a lot of those ui problems are cleared up, so that's another app that I hope will go stable soon :)"
    author: "Mike Hearn"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Just a little on abiword. \n\n* It's *not* just a port to GNOME2/GTK2. It's a massive feature addition as well in current cvs, such as tables, footnotes etc. The basic GTK2 was done by Dom in something like a week or so (of course with some bugs, but anyway).\n\n* abiword is not really a \"pure\" GNOME application. It would probably be easier to use qt since it's based on C++ and to my understanding they are currently using the C libs for the *nix version.\n\nAs for the \"I thought that most of those important applications would be \"2.x\"-released rather soon after the GNOME 2.0 release\" I tend to agree, but the fact was that abiword e.g. just released it's 1.0 and was (and still is) in a haevy development phase with feature editions.\n\nRegards, J\u00f6rgen"
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: KDE's application suck"
    date: 2003-03-17
    body: "A couple of comments about evolution\n\nIt may do a lot of other things beside email, BUT as a gui mail client I have not come across anything else as good in terms of features and most importantly capacity.\n\nIt is the only client I have tried (sylpheed, balsa, netscape, mozilla) that can actually handle my  300-400mb mailbox\n\nAlso I cant live without vfolders"
    author: "redtux"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "Hehe  Ex GNOME user, nice try."
    author: "minkwe"
  - subject: "Re: KDE's application suck"
    date: 2003-03-04
    body: "In first case I wonder why so many GNOME people are spending there time on here convincing KDE users how much more mature GNOME is. Must be due the fact that none of the GNOME related pages such as http://www.gnomedesktop.org/ and http://www.gnomesupport.org/ are reachable for various weeks now. Anyways I as programmer like to point out some roof to basement things that GNOME has made.\n\n- They ported GNOME from 1.4 to 2.0 and THEN they invented the HIG. In my opinion it would have made more sense to came up with the HIG first and have the apps ported using the during the development phase and not months after the first official GNOME 2.0 release came out. No offense but every GNOME application looks differently than others none of them look close to the other.\n\n- Some would argue that GNOME 2.x was written from scratch only for the sake to argue. This is not true most of the libraries and utilities are simple ports to make them operateable under GNOME 2.x. If so then why are there 5 different Toolbars inside GNOME yet ? They are soon being replaced with LibEGG Toolbar and the old code are pointing to the new one through Wrappers. If it was build up from scratch then such stuff never made it inside GNOME 2.x here a source of proof.\n\nhttp://www.gnome.org/~chrisime/random/ui/\n\n- Some would argue that GNOME 2.x was in a development cycle for 6 Months only but that isn't true either GNOME 2.x was overdue for over 1.5 years. I must admit that GNOME 2.x was a heavy change from 1.x and I would be a fool to say something else but definately not written within 6 months. It took more time.\n\n- GNOME still has no serious way doing normal sound without having the pain in the ass getting GStreamer up which is still under development and far away from being implemented in ALL parts of GNOME and far from being stable from everydays use.\n\nThere are various good ideas in GNOME no doubt but for the past 3.5-4.0 years that I am using GNOME it really matured into something strange. It was good at the beginning of development phase and good competitor to KDE during these days looking similar in function and operation. But 2.0 shot the bird with reversed buttons and gconf and make me smack my head against the wall because they still make the same mistakes over and over again, now backed by RedHat, Sun and Ximian everything isn't anymore as it used to be. Something went better, something not. But as other people wrote already. Why should I wait another 4.0 years until GNOME is getting stable and usable when I can use a good integrated and pleasing desktop such as KDE today ? And I like to say and write this without feeling sad for my opinion since it is my opinion. Instead simplifying GNOME with the removal of Preferences and have them stored in GConf-Keys they only reached the opposite. I have now all the pain going through GConf keys configuring the hell out of my system than it was before with the Control Center and all preferences correctly organized and well structured listed in it. GConf only added another source of complexity there. There is so much to complain about it simply outweights the whole issue therefore it's better to use the better plattform and I like to do this without getting blamed for my decision. As someone else pointed out here GTK+ 2.x has good documentation. This is simply not true. Stuff as GTKTree are neither really correctly explained in either the tutorial nor in the reference manual. Many functioncalls are not explained and so on. Same is valid for many GNOME library calls, you as developer don't know which functions are DEPRECATED, which functions soon are being replaced. Which methods of programming are not common anymore and what functions are meant to do. For everyone who need a proof for this now go to developer.gnome.org and make yourself confortable with it. it's not that I as capable programmer would not like to help but help how if the API and stuff is only known to a handful of people \"The family\". Without knowing the API, no documentations, no resources for fresh docs you as developer easily come into the risk developing a new application where you spent much time into it and motivation and then find out that things are not being suggested to do it that way.\n\nThis isn't meant to be an offense, just my personal opinion since I know various people on here namely comming from the GNOME plattform. Instead advocating GNOME which is your good right, you should also look under the hood of the car. Things will for sure change. No doubt GNOME will sure become better but when ?\n\nI hope you people accept this as a valid comment made by someone and as nothing else. If you find I'm wrong then please reply and argue as a man and not as a whimp."
    author: "Ex GNOME user"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Those pages are accessible just fine.  Looks like you got yourself banned."
    author: "Navindra Umanee"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "No, these pages are not reachable for many of us. Sometimes they work sometimes not. It's already addressed to Stro who runs these pages and is likely an ISP issue. This has nothing to do with banning since a bunch of people are affected to it."
    author: "Ex GNOME user"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "No, Galaxy is right, I can't connect to either http://www.gnomedesktop.org/ or http://www.gnomesupport.org/, both of them time out here. It seems at least the largest German ISP T-Online as well as the French ISP Wanadoo (which I'm using right now) are affected. Maybe both sites went US-only about a month ago?"
    author: "Datschge"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: ">  none of the GNOME related pages such as http://www.gnomedesktop.org/ and http://www.gnomesupport.org/ are reachable for various weeks now.\n\nThat's the same server. I can tell you that it's running and available but e.g. not from Germany's biggest ISP T-Online while no problem with other German ISPs exists."
    author: "Anonymous"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Heh, this is really suckage. Some people I know of are ssh tunneling from t-offline onto these pages."
    author: "Ex GNOME user"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Do you know an address where one can complain to technical stuff? noc@t-online.de?"
    author: "Anonymous"
  - subject: "Re: KDE's application suck"
    date: 2003-03-05
    body: "Dunno, try 'host t-online.de' and look the information it outputs then you see the technical contact. Write an email to them."
    author: "Ex GNOME user"
  - subject: "Re: KDE's application suck"
    date: 2003-03-06
    body: "whois t-online.de\n\nsorry my bad :)"
    author: "Ex GNOME user"
  - subject: "Documentation"
    date: 2003-03-11
    body: "I have to agree on the state of GNOME documentation.  It seems like only a handful of people know which functions are deprecated or not, and they are not letting the rest of us know."
    author: "mrcsparker"
  - subject: "representative?"
    date: 2003-03-03
    body: "Hey have you seen how many votes have been there on these polls? I have no idea who was asked but I dont think that around 500 is a number which is supposed to be significant... no wonder suse and kde weren't that strong...\n\ncheers\n"
    author: "Stefan"
  - subject: "Exactly! Also......"
    date: 2003-03-04
    body: "I'm willing to bet that >50% of the votes were posted using Internet Explorer. And no, I'm not talking about browser spoofing either.\n\nI've been looking for a site with just these types of polls but, have had little luck finding them. I even thought about building my own and including public access Webalyzer pages with it but, the problem is the same as with Linux Questions, how do you generate enough votes for it to be meaningful.\n\nIdeally, Slashdot would be the perfect place for such polls. But, they don't seem interested in these types of polls, rejecting them as fast as I can submit them. Funny thing is that it is just these types of polls that initiate the religious flame wars that generate lots of posts and make Slashdot interesting."
    author: "Anonymous Coward"
  - subject: "I'd like to say I'm honored... "
    date: 2003-03-03
    body: "I made some comments on this poll when Bluefish was included in the editor category and Quanta was not. (Nor was Kate as I recall) The pollster changed things around to make a web editor category, and I guess Bluefish already had a few votes. A few weeks later I thought it would not be right if I didn't cast my vote... but to do so I had to sign up and give my information to LinuxQuestions. Know what? I didn't feel like I wanted to. I'm sure they're helping people there with questions but I didn't want to give out my personal information to do a survey... and I didn't have any questions. I guess I was not alone because there were only about 10 votes total in the web editor category. I have several hundred people on maiing lists. I'm sure had I encouraged them to vote Quanta would have stormed to first place...\n\nI'm happy knowing the kind of response we get on Quanta like over 30,000 downloads of a version on sourceforge before it was included in distros giving us a top 10 berth in over 50,000 projects at the time. As KDE is the most popular desktop by any standard Quanta is now part of that. Moreover to my knowledge we are the only one that manages a parsing engine that allows for DTD neutral quick tag and script dialect adoption and blinding speed performance on huge files.\n\nWe've put a lot more into our base architecture so we could be ready for our next hurdle... the first professional web tool on Linux with WYSIWYG. Once we are further along with our technical review we will post our plans there.\n\nSo while we were narrowly edged by an editor with a decent feature set we have been working to give Quanta the agility and development ease and accessability to enable our users to help us extend it. We have never really wanted to compete with Bluefish. We want to dominate when compared to Homesite and Dreamweaver. This is the year we do it. If you're a Quanta user you can help too.\n\nEric Laffoon - Quanta+ Team Leader \nhttp://quanta.sourceforge.net  \nMailing list - http://mail.kde.org/mailman/listinfo/quanta"
    author: "Eric Laffoon"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-03
    body: "WYSIWYG?\n\nYou mean like Netscape Composer? Where you just write text and paste images?\nThat would be great. Try to beat composer please."
    author: "KDE User"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-03
    body: "WYSIWYG == What You See Is What You Get\n\nI really like the style of Dreamweaver where you can switch between a code/WYSIWYG view whenever/wherever you want.  If Quanta could do that nicely it would be awesome. :)"
    author: "Anonymous"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: ">  I really like the style of Dreamweaver where you can switch between a code/WYSIWYG view whenever/wherever you want. If Quanta could do that nicely it would be awesome. :)\n\nDoes Dreamweaver provide WYSIWYG services to dynamically constricted HTML? Does it debug XSLT and web services? Does it do DockBook or other SGML?\n\nI don't use Dreamweaver so I don't know what I might have missed but for the most part these applications are dealing with lots of legacy hard coding. Our objective with Quanta is to manage whatever you want to edit with minimal setup for the dialect as well as manage PHP and other back end scripting with a focus on making advanced usage easier.\n\nObviously we are not going to ever block out the editor. WYSIWYG is only an added convenience for layout and content management. The ability to switch will of course be able to track the location in the document from source, pre-processed source and rendered.\n\nIn fact given the complexities of the subject and the shortcomings of other approaches we have not ruled out other alternate and useful supporting views that might prove beneficial  in a production environment."
    author: "Eric Laffoon"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: "> Try to beat composer please.\n\nOh come on... don't make me snort coffee out my nose. If you've looked into Quanta you know it's first rate. If you've read any of my posts on this subject you'd know we will never do WYSIWYG until we know we can do a lot better than what has been done before because everything I've ever seen left a lot to be desired.\n\nQuanta is no longer an HTML tool... it's a DTD agnostic markup and script tool... therefore we can't do the composer route without going backwards to the dark ages... and obviously we can't hack things up. Have a look at Quanta... why do you suppose we have gone to DTD specific markup and a multi DTD capable document tree with extensively parsed documents? ;-) \n\nWhen Quanta does WYSIWYG it will be writing markup according to the rules you set and specifically for your DTD. It will also be offering these benefits to programatic approachs too so it is not just a way to make dead languages easier. ;-)\n\nQuanta has never been about being another web tool... it has always been about being the best."
    author: "Eric Laffoon"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: "I've tried quanta (2 and 3) and regularly have a look at it, but it seems it is still unusable for me. It looks like an editor to create html 2.0 or 3.0, but definitely not html4/xhtml documents. For instance, there is a <b> button instead or a <strong> one (<b> has been \"discouraged\" for more than 5 years - same for <i> versus <em>). Also, I could not find how to create a page of xhtml in Quanta with the \"wizard\".\nWith respect to ease of use, Bluefish deserves its first place. I haven't voted, as I didn't know there was such a vote, but clearly would have voted for Bluefish.\n\nMaybe next year with quanta 3.2 it will be first. But at present not.\n\nregards."
    author: "oliv"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: " Yes, Quanta still shows up some old/deprecated tags in it's default\nHTML toolbars, but with the current infrastructure it's just a matter\nof XML writing and point&click to correct this. And it can be done\neven by any users. The keywords are DTD definition/tag files;\nuser toolbars and actions. For the problem you bringed up the solution\nis quite easy: create a new (tag) action for <strong> and <em> in the\nConfigure Action dialog, put it on the Standard toolbar instead of\nB and I and enjoy it. You can save the new \"Standard\" toolbar, or e.g e-mail\nto me so I will include in a new release.\n It's quite easy to do it.\n And I think the power of Quanta is configurability. :-) (And we progress in\nthis area...)\n\n BTW, the Quick Start wizard is an old one, and will be replaced (soon).\n\nAndras\n"
    author: "Andras Mantia"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-05
    body: "I have to salute Andras here because he has been working his ass off day in and day out with rarely a day off to bring Quanta from the rather lame HTML 3.2 editor that it was to where it is now. In particular, making Quanta so incredibly user configurable has been a lot of work that seems largely ignored by people seemingly in a \"consumer mindset\". Spending several months making Quanta function with user configurable DTDs (has anyone else seen an editor that parses the DTD of the document, handles compound DTDs and adapts toolbars and auto-completion?) including the rather difficult task of writing the rule set to make scripting languages like PHP adhere to psuedo DTDs so that they could be defined in the same way for the same benefits has been remarkably unheralded. (Java or Perl could be added now with XML configuration and an rc file) Spending another couple months squishing bugs, tuning for cpu usage and making the parser thousands of times faster than it was may get little notice. In fact it may get ignored and over-shadowed by other \"more exciting\" features in 3.2.\n\nDuring this time I was interacting in the design phase and scrambling my ass off in the months my business income is lowest and costs are highest to make sure I honored my sponsorship commitment. Of course someone would notice one of the things we had not done, and in classic style instead of sending us an email telling us to wake up and get our face off the keyboard and fix it... or better yet sending us a fix... wait until it could be delievered as an illustration of our shortcomings in what is essentially an unfinished conversion for some time. Of course the poster is right, and I am duly shamed.\n\nI think Andras deserves some real kudos for doing all he does, responding immediately to bug reports and even having diplomatic skills too. Clearly we are now both in agreement that our recent work redesigning configurable actions now needs to be followed by a tutorial so that people will know what can be done from the settings menu. Maybe then we can finally get a few web developers who will help us advance the user configurable aspects of Quanta so we don't have to take a few months off the core coding like scoping PHP variables in auto-complete and the tree or a WYSIWYG plugin.\n\nIf I could ask one thing of users it is this. The next time you read about Linux and KDE as an open source community and perhaps swell with pride over over community accomplishments ask yourself how you feel about your standing in the community. The next time you find something that needs done in a piece of open source software remember that you are that community. You cannot imagine how much greater the victory is when you know you've played a part in it and we've gone out of our way to make that really easy for our user base. We need more web developers on our applicaton development team!"
    author: "Eric Laffoon"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-06
    body: "> a DTD agnostic markup and script tool...etc\nOk this all sounds really cool.  :-)\n\n> When Quanta does WYSIWYG it will be writing markup\nHey, I thought for a moment you guys were going to make a cut and paste thing...\n\nYou're not planning on using xcschema instead of DTD:s?"
    author: "KDE User"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-03
    body: "Ooooh, WYSIWYG, that?s what I?m dreamin` of :-)"
    author: "star-flight"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-03
    body: "WYSIWYG?  You're kidding, right?  I thought that you guys had more of a clue about web development than that.\n\nA graphical editor for web pages is one thing, but please don't advertise it as WYSIWYG, because that is _impossible_ for a website.\n\n"
    author: "Jim"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: "> WYSIWYG? You're kidding, right? I thought that you guys had more of a clue about web development than that.\n \nWell consider that it's our number one most requested feature and the one obsticle to getting noticed and compared with the big boys... I would think anyone with a clue would realize that no matter how good your web development tool is, until you can draw pretty pictures with it a lot of people are still going to be in game-land.\n\n> A graphical editor for web pages is one thing, but please don't advertise it as WYSIWYG, because that is _impossible_ for a website.\n \nSince Quanta has always had a GUI (Graphical User Interface) it's always been a graphical editor for web pages. Is WYSIWYG impossible? Let's see. I would agree that every implementation I've seen has had real problems but I can also tell you why, what shortcuts the developers took, the assumptions, the obvious marketing interventions and the fact that they were really just targeting the lowest common denominator of human laziness and stupidity because their customers really didn't care what actually ended up happening underneath the page...\n\nGiven that a markup page is both written and rendered then by definition WYSIWYG cannot be impossible unless you want to argue semantics and point out that you don't actually see the cursor in the final display page.\n\nThere has been a lot of general stupidity about WYSIWYG from software companies like M$ making FlunkPage that hacks your source to hell and doesn't validate, not to mention needs proprietary extentions to work, to developers who take no responsiblity for the integrity of what they produce. Let's not assume that exists here. Okay?\n\nBottom line: I want WYSIWYG (and I'm going to call it that for the same reason you call your OS Linux, it's easy and people understand it even if it's semantically inacurate) because I want to be able to:\n1) Speed up my layout and content entry\n2) Be able to provide a custom configured Quanta on a Knoppix CD to any web client so they can use it for content editing providing me ready XML files.\n3) Finally stop getting pestered about how totally rad and cool it would be if we had WYISYWG so people could draw a web page because tags confuse them. ;-)\n\nQuanta's application will not hide what is going on and will encourage people to still learn what's happening."
    author: "Eric Laffoon"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: "Is WYSIWYG impossible?\n\nWYSIWYG stands for \"What You See Is What You Get\".  The only thing that is possible on the www without a significant step backwards is \"What You See May Be What You Get Under Some Circumstances, And God Only Knows What Everybody Else Gets\".\n\nThe final rendering of a website depends on a hell of a lot of different things, very few of which can be influenced by the developer.  Christ, in some cases, the \"see\" bit is completely irrelevent.\n\nI wasn't arguing semantics.  I just don't want you to lead newbies down the wrong path.  \"Graphical editor\" is okay.  \"WYSIWYG\" is not.  Many newbies assume what they are seeing is what everybody else sees.  This is one of the biggest mistakes they can make, and calling a graphical editor a WYSIWYG editor only encourages it, even if you know the difference.\n\nI wasn't assuming that the graphical editor component would produce broken code, I'd consider that to be the bare minimum to avoid making Quanta the laughing stock Frontpage is.\n\nI do think that there is a strong possibility of dropping meaningful markup when you add the graphical editor component though.  When somebody hits the \"italics\" button, how will you know whether they meant \"this should be in italics because I'm emphasising something\", \"this should be in italics because it's a quote\", or \"this should be in italics because it's a ship name\"?  Each of these has a separate way of marking things up, <em>, <blockquote>, and <i>, respectively.  If you don't use the right one, you are subverting HTML's goal of meaningful markup and prolonging the wait for useful applications of the Semantic Web.\n\n\"Quanta's application will not hide what is going on and will encourage people to still learn what's happening.\"\n\nGood.  But calling it a WYSIWYG editor is in direct opposition to this statement.  Most people who talk about WYSIWYG editors without qualifying it are clueless about web development, which is why I was so disappointed to see somebody from the Quanta team do so.\n"
    author: "Jim"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: "> WYSIWYG stands for \"What You See Is What You Get\". The only thing that is possible on the www without a significant step backwards is \"What You See May Be What You Get Under Some Circumstances, And God Only Knows What Everybody Else Gets\".\n \nI grant you what you said, but to me it's still semantics. If I'm editing what I'm seeing on my system then at least it's my point of reference. Granted other browsers will use different pixel measurments in places and handle some tags differently, not to mention CSS and Javascript issues. But for the sake of argument you have to start somewhere. Operating under the assumption that what you see on one system is what you see on all is a user error, not a software error. Also as I said Quanta is already, by convention of terminology, a GUI editor as it uses a graphical interfce, even though it's text based. I suppose I could call it a visual layout mode... but then I'd get far more protest from the other side of the aisle. Have some compassion... Stallman's argument for GNU/Linux makes a point too but part of our language is to use semi broken terminology because it is widely understood, or as you so aptly point out, widely misunderstood. At least the misunderstanding is relatively consistent. You are visually creating a page.\n\n> I do think that there is a strong possibility of dropping meaningful markup when you add the graphical editor component though. When somebody hits the \"italics\" button, how will you know whether they meant \"this should be in italics because I'm emphasising something\", \"this should be in italics because it's a quote\", or \"this should be in italics because it's a ship name\"? Each of these has a separate way of marking things up, <em>, <blockquote>, and <i>, respectively. If you don't use the right one, you are subverting HTML's goal of meaningful markup and prolonging the wait for useful applications of the Semantic Web\n\nSo your point is based on assumptions? And you were just arguing against a term because people may have falacious assumptions about it. Shame on you. ;-) \n\nFirst off we intend to have it rule base to assist the user under certain structural conditions or to set a preference where, as you know people as people just decide to be lazy and bad... and we can't beat them over the head. We can only offer them a better way. Beyond that it would mean that when you were tagging as in  your example we would have two viable options...\n1) Offer the tag from a list with tooltips for proper application.\n2) Advance our parser to a level where it would be able to offer semantic breakdowns and allow user configuration of the correct tagging for the syntactic element.\n\n> > \"Quanta's application will not hide what is going on and will encourage people to still learn what's happening.\"\n \n> Good. But calling it a WYSIWYG editor is in direct opposition to this statement. Most people who talk about WYSIWYG editors without qualifying it are clueless about web development, which is why I was so disappointed to see somebody from the Quanta team do so.\n \nMost people? There we go with that assumption again. I came up from the code, not the picture. I understand and detest the problems inherent with tratiional WYSIWYG and I personally think none of them have gotten it right. Key to that is that I am unaware of any web development tool that is both DTD agnostic and has an advanced parsing engine driving it's operation. As I've said before, I would like Quanta to be the first application to do WYSIWYG right."
    author: "Eric Laffoon"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: "<feature request>\nIt would also be very nice if I could edit the html code in the treeview, where the (DOM ?) tree is displayed. E.g. select a subtree and copy it somewhere else, delete it, move it and so on.\n</feature request>\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: I'd like to say I'm honored... "
    date: 2003-03-04
    body: "> <feature request>\n>  It would also be very nice if I could edit the html code in the treeview, where the (DOM ?) tree is displayed. E.g. select a subtree and copy it somewhere else, delete it, move it and so on.\n>  </feature request>\n \nIt's on the do list. One problem is resolving heirachies with tables and such. However you can actually just about do it right now...\n1) RMB on element, select tag area\n2) Cut selected text\n3) LMB on new location in tree - cursor follows\n4) Visually confirm location WRT tags and paste\n\nNot bad for lacking the full feature huh? ;-)"
    author: "Eric Laffoon"
  - subject: "KDE framework, gnome apps..."
    date: 2003-03-03
    body: "I'm probably one of the silent majority who loves KDE for the framework and integrated apps it offers but who still use other, non-KDE 'main' applications. That means Evolution over KMail (but Kontact will be a seriuous contender), Mozilla/Galeon over Konqueror ( but Apple might have a big impact here...), Mozilla Composer over Quanta as for my needs, Composer's WYSIWYG is the killer feature (but as we heard from Laffoon, that might change in the coming months) So I'm quite convinced that if Gnome doesn't get their act together, I'll be using the KDE equivalents before long.\nSome KDE apps however don't even get near Gnome equivalents (eg. Gimp/Krita, Sodipodi/Karbon14) and will remain that way for the foreseable future. \nThen there are the non-KDE, none-Gnome apps like Blender, OpenOffice, Mplayer for which there is no KDE competition.\nOf course, some major apps have a KDE leader like KDevelop, Konq-the-file-manager, and a couple more.\nSo on the whole, KDE is already looking good and is set to get better soon, but wther complete domination will be reached (or if it is even desirable...) is pretty unlikely. Agree? Disagree?\n\nYours,\n  UglyMike\n  Satisfied KDE user"
    author: "UglyMike"
  - subject: "Re: KDE framework, gnome apps..."
    date: 2003-03-03
    body: "One thing missing in KDE is a _good_ default GUI for the mediaplayer Noatun. A GUI like Windows Mediaplayer.\n\nWell, many of the apps you mention are not gnome apps but it is true that some Koffice apps don't seem to get the developers they need..."
    author: "KDE User"
  - subject: "Re: KDE framework, gnome apps..."
    date: 2003-03-04
    body: "Yuck... while I'm not a big fan of noatun, I can't imagine it being good to imitate windows media player.  The version from a few years ago was pretty good, but it's gone WAY downhill.  "
    author: "anonymous"
  - subject: "Re: KDE framework, gnome apps..."
    date: 2003-03-04
    body: "You are right: but why isn't there anybody willing to help in KOffice?\nWhy not you? It is not complicated to write office applications.\nEverybody who is able to add small things is very welcomed. Many things do not need much knowledge about the whole application. \n\nI don't if there are more than 10 people right now working on KOffice regularly...\n\nThere must be some people out there capable of speaking a little bit c++... *dreaming*"
    author: "Norbert"
  - subject: "Re: KDE framework, gnome apps..."
    date: 2003-03-04
    body: "You make it sound so easy. Don't you agree that it's very difficult to just sit down and read & understand other peoples code? \n\nThat said, I don't want to discourage people either. It can be done, if you are willing to put in a lot of time."
    author: "ac"
  - subject: "Re: KDE framework, gnome apps..."
    date: 2003-03-06
    body: "No, (for me) it's not that hard. And for most of things you just need to understand the few lines you are working on or the lines where you integrate new features. And the new feature code is yours.\nEven now after working on my current project for a year, I haven't seen all of the code. But I also fix bugs in other projects.\n\nAnd also: you can get a lot of help from people knowing the code very well.\n\n"
    author: "Norbert"
  - subject: "Re: KDE framework, gnome apps..."
    date: 2003-03-04
    body: "Mozilla is a non-gnome app!"
    author: "mathjazz"
  - subject: "KDE Gimp GUI"
    date: 2003-03-04
    body: "I've read some posts on the net about the posibility of having a second go at at KIMP not that GIMP 2.0 is under way.  Is that even feasable? If it is, would anybody be likely to give it a go?"
    author: "Kostumer"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "Do you have anything rational or just the same old rumors? Frankly the gimp is pretty powerful and I was thrilled to find it but the more I use it the more I begin to detest it's interface. I espeically hate trying to select a layer from the list for image maps when I have several images open with many layers each. It just drops off the screen. It's the most brain dead ui element I've ever seen. Even Motif would pop up another list column to complete it.\n\nLast I heard they gimp team is not decoupling the ui from the functionality so putting another interface is a lot more than just coding the interface. Have you heard something I haven't? There is a decent start on a package in Koffice but last I heard all they did was give it a crummy name. It needs developers. Mosfet is looking at doing something but it's primarily a paint program. ImageMagik has lots of potential to get there fast but the underlying API changes enough from version to version you have to spec an exact version, not an \"at least\" version. \n\nIf you really want it you could do what I did... Learn to code and sponsor a full time developer. That is how most of Quanta gets done. Maybe in a year or two we will be able to assist or take on a KDE graphics package. If I had community support to sponsor more developers I might take on such a task... but even managing a project and trying to bring in and manage developers is more work than most people know. FWIW I want a good KDE based graphics tool bad enough to consider even less sleep and losing more work time... Better someone else gets motivated to do this. ;-)\n\nFOSS gets built by a remarkably small percentage of people making any contribution whatsover. If more people acted on their concept of community a lot more would be getting done."
    author: "Eric Laffoon"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "I love the GIMP but I would love it more to see it being ported to KDE as a real Photoshop alternative with equal look and feel. There is still nothing outside that could beat the hell out of Photoshop. There are many advantages porting the GIMP to KDE:\n\n- Better Framework and Plattform,\n- Rapid development. The Gimp would be more today,\n- Better Maintainance,\n- Better integration into other apps due KParts and plugins e.g. easy extension.\n\nThink about it. I bet there are many people outside who would welcome this. At least for my personal opinion I'm missing a serious competitor to The Gimp but more in the means of Photoshop. A real competition. So at the end I can finally made a complete cut from the GTK+ cruft dependency on my current system. KDE now has many major applications, that still need some more tweaking but the framework and the current usability and intuitivity of the apps are cool. I see a good future for KDE."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "> - Better integration into other apps due KParts and plugins e.g. easy extension\nGimp is a gtk-app. If they'd do the rather simple step to make it a GNOME-bonobo-app it woul integrate very well into \"GOffice\". But loose it's platform independency.\n\nKrita as \"KDE's gimp\" is nice, but no one is working on it anymore :-(\nFor me it already has everything I need. Too sad it's not shiped with koffice 1.3. Even if it would be under active development, it couldn't beat gimp or even photoshop. But it could gain many users from gimp. Just because it's powerful enough for most users and has a much easier interface than gimp.\nKDE (Unix at all) would really need such an easy to use \"painting\"-tool.\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "Well have you looked at The Gimp 1.3 it's really close to mature into a total GNOME application. Only a matter of time. GConf (Windows Registry for GNOME) should soon become part of GTK+ as Havoc Pennington wrote on developer.gnome.org Mailinglist and chances are good that The Gimp will adapt it one day. It's just a matter of time until it matures into a fullworthy Gnome application. Yes Krita is kinda nice but not really serious compared to Gimp that's why it really needs work."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "> Yes Krita is kinda nice but not really serious compared to Gimp that's why it really \n> needs work.\n\nFor me (and many others) it suits my needs already. Krita's strength is (would be) not it's featureset, but it's easier interface than gimp.\nEven if krita would be under active development it wouldn't catch up with gimp's features for the next year(s). But many users would be happy to be able to have a graphics suit they can easily use (many user do not want to use gimp because of it's interface).\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "You sure it will take years ? I doubt !\n\nWell anyways I would welcome if Krita would mature and become some equivalent and easy to use as Photoshop under Windows or MacOS. I mean how can you be sure that it takes years to keep up with The Gimp if you haven't tried it.  By saying so you bascially STOP the project before it really begins to mature because of the personal justification you have. I mean if the developer at least lays his hands on it and push it up to his maximal extends of capabilities then I it's possible. I bet it's also possible to take a lot of Gimp plugins and convert them to C++ maybe not the GUI but the engine itself. May reduce a lot of time."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "If the only way to estimate how long someothing will take was trying to do it, there would be even more half-done projects out there.\n\nIn fact, I think those who act like that are the ones who don't finish them (and I am one ;-)\n\nEx GNOME User, oGALAXYo, Ali, or however you want to be called today, before investing a large amount of effort for years (and yes, it is going to be years, even if catching up with GIMP could be done in months, which I doubt), someone has to look at the task ahead, and decide if he is going to jump in a full pool.\n\nNot doing it is, IMHO, not smart."
    author: "Roberto Alsina"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "Hehe, Ex GNOME user, maybe AC was better afterall.  So transparent. He doesn't cease to amuse me. "
    author: "minkwe"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "Someone on OSNews.com told you that AC stands for Anonymous Coward. Which is Default on /. for people who like to stay anonymous. Anyways you can't blame me for that since everyone is welcome to read the 'Ex GNOME user' comments and he or she will find out that I only made my point with valid comments without being offensive or insulting to other people. you can say and try what you want but these are the facts."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "Havoc Pennington is not Gimp maintainer, so what he says about the future of gimp should not be taken very seriously (by the way, I have not find such a post on the gnome-dev list, could you give me an exact URL).\n\nOn the other hand, Gimp maintainers have stated:\n- that gimp 2 will have a separation beteen the core and the interface(*)\n- that gimp is not a gnome app, but a gtk app (thanks God!)\n\nIf Mr. Pennington or his colleague Mr. Taylor have time to spend, better would it be to debug Gnome and GTK, to answer to bug-reports, that to try to upset gimp users and maintainers by imposing changes that are not welcome (like the new default for change-accels).\n\nThen maybe, they will have a chance to produce a desktop as user friendly as KDE, that is configurable, definitely one of its best qualities.\n\nRegards,\n\n(*) maybe like LyX. Remember LyX. KDE people forked to an ugly and unstable KLyX. The hard way preferred by LyX people was to make the program GUI Independant. It was a long tedious way, but nowadays, LyX-Qt is here. Still needs polish, but there are two LyX with common code, and a LyX-Gtk is in work.\nConclusion: To see a KDE/Qt Gimp, maybe the best way is to help the current Gimp/GEGL project instead of creating a Quirk and Dirty clone of Gimp.\n"
    author: "oliv"
  - subject: "GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-04
    body: "No, i haven't written that Havoc Pennington is a Gimp Maintainer. I wrote that Havoc Pennington likes to get GConf into GTK+ which is quite some difference between Night and Day.\n\nHere the writing that Havoc likes to bring GConf to the GTK+ level.\n\nhttp://mail.gnome.org/archives/desktop-devel-list/2003-February/msg00154.html\n\nHe also talks later in this Thread that KDE is going to adapt GConf as default configuration or has been adapted it already."
    author: "Ex GNOME user"
  - subject: "false!  kde does not adopt registry!"
    date: 2003-03-04
    body: "Havoc is not a KDE developer so what he says about KDE development should not be taken very seriously."
    author: "anon"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-04
    body: "No I haven't written you had written that Havoc Pennington is a Gimp maintainer. I have written that Havoc Pennington is not a gimp maintainer, which is quite some difference between Night and Day. :-)\n\nThanks for the link, by the way. I never said you were lying. I said I could not find it and would be interested to read it.\n"
    author: "oliv"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-04
    body: "Hehe, no problems! Misunderstandings could happen. I'm glad we cleared it up."
    author: "Ex GNOME user"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-04
    body: "That's not entirely true, what Havoc is talking about is creating a new config system over at freedesktop.org, that is desktop neutral. He does not say it should be a part of GTK+, he said it would be *below* the GTK level, ie as a shared lib usable by all software.\n\nYou're talking out of your ass when you claim he says KDE will adapt GConf. For starters, this is just idle talk. Secondly, if one day KDE does replace KConfig, it'll be with a desktop-neutral standard, which won't be GConf (as gconf is tied to gnome). It might well be something a bit like it however."
    author: "Mike Hearn"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-04
    body: "Dude, you are able to read aren't you ? I had various personal conversations with HavocPennington and some exchanged Mails with KDE developers. There are NO plans to implement a NEW configuration system. IamTheRealMike, why don't you investigate before commenting on various places ? You think you know everything better than others which is seriously not the case."
    author: "Ex GNOME user"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-04
    body: "\"There are NO plans to implement a NEW configuration system.\"..... \n\nwhich is what I said - \"For starters, this is just idle talk.\" and \"if one day KDE does replace KConfig\" - notice the word if.\n\nAlso, I'm not sure how I'm supposed to read private emails you've had with KDE developers and Havoc.\n\nFinally, I think it's amusing that you decide to throw in my Slashdot nick for seemingly no reason. Are you the anonymous coward who decided to randomly flame me the other day, using my real name in the post?\n\n\"You think you know everything better than others which is seriously not the case.\"\n\nAnd why would you think that? My postings to this forum have been minimal. Or are you the guy I was talking with on email the other day?"
    author: "Mike Hearn"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-04
    body: "Sorry in the previous mail you wrote\n\n\"That's not entirely true, what Havoc is talking about is creating a new config system over at freedesktop.org, that is desktop neutral.\"\n\nThis assumes me to belive that there are plans for a new configuration system which is not GConf.\n\nThe rest of your comments I leave open because you make no sense now. You can't come up with valid points anymore and turned the way off of making normal comments so other readers at least get the feeling that we do a normal conversation here. I am a bit sick about people who can't argue anymore and turn into the TROLL mode. Sorry no offense again."
    author: "Ex GNOME user"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-05
    body: "\"This assumes me to belive that there are plans for a new configuration system which is not GConf.\"\n\nWell, there are I think, in Havocs head. But it's just a \"wouldn't it be cool if kde and gnome shared a config system\" type thing, there are no hard plans or decisions made, DBUS is the first priority, maybe later stuff can be built on top of that. For now he simply wants to make GConf more generic and not tied into gnome as much i think.\n\n\"You can't come up with valid points anymore and turned the way off of making normal comments so other readers at least get the feeling that we do a normal conversation here. I am a bit sick about people who can't argue anymore and turn into the TROLL mode. Sorry no offense again.\"\n\n????? I don't understand this paragraph, except it leaves me convinced that you are in fact oGALAXYo."
    author: "Mike Hearn"
  - subject: "Re: GTK+ and KDE adapts GConf ? Here the Proof !"
    date: 2003-03-05
    body: "> Well, there are I think, in Havocs head. But it's just a \"wouldn't it be cool if\n> kde and gnome shared a config system\" type thing, there are no hard plans or\n> decisions made, DBUS is the first priority, maybe later stuff can be built on\n> top of that. For now he simply wants to make GConf more generic and not tied\n> into gnome as much i think.\n\nWell Havoc's plans are to put GConf into GTK+ at least from what I was able to read from the Mail he wrote on Desktop-Developer-List. I'm actually no mindreader so I don't know what the 5 majors in GNOME are planning. But I don't go on places and write about planned imaginary Configuration Systems that was meant to be discussed on Freedesktop.org.\n\n> > \"You can't come up with valid points anymore and turned the way off of\n> > making normal comments turn into the TROLL mode. Sorry no offense again.\"\n\n> I don't understand this paragraph, except it leaves me convinced that you are\n> in fact oGALAXYo.\n\nBy the way, nice try. Or better nice try that you turn a normal conversation and make it some sort of \"look that troll\" comment. Sorry to disappoint you you already tried on slashdot and various other places and the #gnome-de people on irc.sventech.com whom I have told about totally laughed about you. By using/trying a different Desktop Environment this doesn't mean that I also turned the back of my people there whom I went through all sorts of things over the time. Anyways I like to encourage everyone to investiagte and compare on their own of what I wrote and what not. And I told you various times even per eMail in a friendly way that I only write about GNOME. There is no need for you or anyone else to get personal. You have serious issues understanding this. You yet try again and again and again. It's a big difference if we sit down and discuss the pros and cons about a Desktop or go onto a place and take people to the shambles by calling them trolls, slander and libel them. You exactly did this here:\n\nhttp://slashdot.org/comments.pl?sid=55517&cid=5412666\n\nCalled my UI review to be a Troll while it was discussed with the GNOME germany people. It's even stored on http://www.gnome.org/. The whole UI review was good, necessary, backed by GNOMERS and good. That's why I don't see you as serious conversation person.\n\nDude realize that I still have a good relationship to many GNOME developers. And it makes me a fair strong person to write the truth how I see things in public discussion places such as this here where right now more GNOME people are replying than KDE ones what I think specially when I directly got asked for it. This is another reason why I think that GNOME has issues in, even in the case that GNOME may or may not be a good Desktop plattform but it's sad to see that people can't play in teams. Far to many individuals on it that rip their head of each other. I welcome you to the #gnome or #gnome-de channel, visit it see me idling there and have a nice conversation with the people. You don't see anyone there riping my head off just because of my opinion."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "This war between KDE and GNOME is getting ridiculous. Looks like some people are jealous of GNOME apps, and want them subverted to KDE. GIMP and GNOME share a common heritage, GNOME IS built on the GIMP TOOLKIT. You have to live with the fact that some GNOME apps are better than the KDE ones, and the GNOME guys also have to live with the fact that sometimes KDE apps are better than GNOME ones.\n\nThere is absolutely nothing wrong with that. All that is needed is focus. I would much rather see a common spec for creating GUI's in Linux, and then all your applications would take on the native Desktop's environment look and feel. But that is a tall order.\n\nI think though, that Mono might just be the last bit that gives GNOME a development environment that is as good as KDE's. I mean, if the rapid devlopment of .NET (Mono) is realised, GNOME might just become more exciting. \n\nAs for competition, Whoever says competition is not helping the Linux desktop grow better is a liar. Look at this forum. KDE and  GNOEM developers are earnestly trying to outdo ech other. That can only be good."
    author: "Maynard"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "> This war between KDE and GNOME is getting ridiculous.\n\nWhat WAR ? Sorry but why do you and some others see a WAR in everything ? What we did the past couple of writings here was just explaining the pro's and con's nothing else.\n\nWell for MONO and .NET are you seriously that sure that many GNOME developers are happy with MONO and welcome it with open arms ? It's only a proof that C is not suited for rapid applications development, the reason why Miguel de Icaza wrote it. To be able to rapid develop applications for GNOME. Thus leads to the conclusion that writing GNOME in C and various other hacks called languages was plain wrong and don't lead in the speed he likes to see. How many languages are now used to build an entire GNOME system ? C, C++, Python, Pearl and soon MONO .NET this will add another complexity and the maintainance getting dealt with all the code is really timeconsumming. One who needs to care for the C code, then another one who needs to care for the C++ code then Python then Pearl and so on. I personally belive that this is not such an good evolution. I played with MONO 0.18 some weeks ago, compiled and installed it and was horrified with the *.exe files and *.dll files it installed on a UNIX like plattform."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "Mosfet is currently writing a KDE based painter called MosfetPaint. I think that it will evolve into a serious Gimp competitor in a few months. "
    author: "Androgynous Howard"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "I'm hoping that Mosfetpaint will turn out to be a modern Deluxe Paint (a la Amiga) equivalent. DP is the only paint program I've really been able to understand. :) I still run it in UAE when I need to create graphics."
    author: "Apollo Creed"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-04-17
    body: "I totally agree :) Although TVPaint from newtek is stunning as well!"
    author: "m0ns00n"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "Can you please back up these assertions? I see people claiming these things, but never backing them up. This isn't a troll, I want to see the arguments behind the claims.\n\n\"- Better Framework and Plattform,\"\n\nPlease be more specific. I know all about KDEs architecture, so no need to repeat stuff. Please tell me why it's \"better\" and what that means.\n\n\"- Rapid development. The Gimp would be more today\"\n\nPossibly, but only by using KDE code, which would tie it to KDE. They don't want to do that, the GIMP is used by all sorts of people regardless of what desktop the y use (and also on windows). So..... what else? BTW I've yet to be convinced that C++ is a \"better\" language than C, more features yes, harder yes (therefore fewer hackers).\n\n\"- Better Maintainance\"\n\nWhy?\n\n\"- Better integration into other apps due KParts and plugins e.g. easy extension\"\n\nNot sure what you'd embed, the GIMP is an image editing app. I can't see people embedding a mini-gimp into KWord or whatever, nor vice-versa. You neglect to mention that in this case \"other apps\" really means \"other KDE apps\", which is a small subset of all available applications.\n\nWhat's really needed is a cross desktop, reasonably neutral object model.\n\n\"So at the end I can finally made a complete cut from the GTK+ cruft dependency on my current system\"\n\nWhat's the point in that? It's just a widget toolkit. Why not dump Xlib as well, after all, it's written in C and isn't KDE? You seem to have something against GTK/Gnome, but you haven't explained why even using it is so bad. I use the Gimp 1.3.x, and it's really quite nice these days.\n\n\"I see a good future for KDE.\"\n\nYeah, me too."
    author: "Mike Hearn"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "> > \"- Better Framework and Plattform,\"\n\n> Please be more specific. I know all about KDEs architecture, so no need to\n> repeat stuff. Please tell me why it's \"better\" and what that means.\n\nYeah that's why you recently switched from KDE to GNOME and advocate it now. But yes I think that KDE has a better framework than GNOME first of all there are documents on the KDE developer page where you can actually READ about the KDE architecture and the libraries used inside. It's not like on GNOME where one app looks differently like the other app while on KDE you can simply inherit an object and have you app look similar to the other apps used on KDE because of real OO design and OO language you use. OO is definately better suited for a Desktop plattform than C. Don't you agree ? You can play Lego with KDE you take something and put it inside something else and it works. Look at Kaplan for example, it hasn't taken any long and you saw Kmail, Kaddressbook, Kalender and some other stuff inside it's rendering Window and still remain usable as their standalone app. Look at Nautilus as an example. Everything stuffed inside as Nautilus View. Even own applications are written only to work for Nautilus they should simulate the powers of Konqueror that way since you can do much work inside Konqueror but on the GNOME planet they forget that on the KDE side they work with KParts. The applications on the KDE side are existing. e.g. A CD-Burner program embedds some stuff as KParts and Konqueror is able to use it as well. But you know that you still being able to run the application as standalone. Good example on the GNOME side is nautilus-cd-burner as the name says only usable in Nautilus and people only know it to be there when they enter burn:// otherwise they never realize it to be there since there is no standalone application that suggest it. All those Nautilus views are what it is only usable in Nautilus. Not compareable with KParts. It would have make more sense to get rid of Nautilusview and have the components be real Bonobo ones so I can use my porno picturees viewer and use it to jack off while using the same program to burn the pictures on CD. Besides the last sentence as sarcasm that's what I mean. Call me wrong if you want. But I as developer actually am able to read on the KDE development pages about the framework while it stays a big secret on the GNOME side. Let's assume GNOME's framework is better but how do you think I should find out ? Spending years learning from sources how things are used to work instead spending 1 Week reading a nice onlinebook (on KDE) to learn all about the api and framework ? You may guess which one is better. Look at the various helping souls who help on GNOME they definately do a good hard job no doubt and saying something else is wrong. But without any good books, explaining references and other stuff people are progressing slowly because they need to investigate into new things which takes really ages. Even I have serious problems understanding Bonobo, BonoboUI and other stuff and this is really a hell to learn and NO documentations and I call myself an experienced Programmer but should I cut manuals out of my left leg ? I spent days in the source of Bonobo, spent days in the Reference Manuals, spent hours reading the little writings made by Michael Meeks and and and but they only cover a tip of the whole tree. Do you want to blame me because of this ? That's why I think KDE is better.\n\n> > \"- Rapid development. The Gimp would be more today\"\n\n> Possibly, but only by using KDE code, which would tie it to KDE. They don't\n> want to do that, the GIMP is used by all sorts of people regardless of what\n> desktop the y use (and also on windows).\n\nWell it was just an idea. Do you blame me for having that idea ? The idea having a default KDE photo manipulating program in the means of Photoshop or the Gimp are not bad after all. Seamingless implementation into KDE.\n\n> > \"- Better integration into other apps due KParts and plugins e.g. easy\n> > extension\"\n\n> Not sure what you'd embed, the GIMP is an image editing app. I can't see\n> people embedding a mini-gimp into KWord or whatever, nor vice-versa. You\n> neglect to mention that in this case \"other apps\" really means \"other KDE\n> apps\", which is a small subset of all available applications.\n\nYou can split parts of it out so it can be used by other applications can't you ? E.g. having an Layers manipulating object which then can be used in other applications, When embedding a picture into Kword you can embedd some simple functionality of a GFX program inherited out of objects to have simple image manipulation out of KWord and so on. Can you do it with The Gimp ? No !\n\n> What's really needed is a cross desktop, reasonably neutral object model.\n\nThat's an idea."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "You know I have the wierdest feeling I'm talking to Ali Akcaagac once more. Why not use your real name also, Ex Gnome User?\n\n1) Documentation. Yes, GNOME takes a whipping here. They have focussed more on the desktop than on infrastructure.\n\n2) Parts. Well, you realise a Nautilus View is in fact a Bonobo object right? It could be used in other apps, but why? It represents a CD to be burned as a file browser location, I'm not sure what other apps would need such functionality. Nonetheless, it's there if you need it.\n\nI'm sort of split on KParts vs CORBA. I think CORBA gets a bad rap, mostly because all the example code out there is C, and CORBA on C is horrible. I stumbled across a half-finished Bonobo/Python tutorial and was really impressed... so far I haven't seen another technology other than .NET which lets you write objects in any language and use them in any other. It also used to be the case that Bonobo/CORBA was pretty slow, but these days it's much faster than DCOP for out of process, and the Shlib stuff is much better in GNOME2 so you can do inproc (like kparts) as well.\n\nOn the other hand, KParts gains simplicity (something corba is most certainly not) at the cost of features. Like, they have to be C++ (or did last time i checked).\n\nI think CORBA is a better long term solution at present, but unfortuntely it doesn't get much attention docs or evangelism wise.. it's also more complex as tim janson pointed out. It really needs a super-slick and easy to use implementation. C is not the ideal language for that.\n\nOK, so I'm mostly convinced on the documentation point. KDE is better in this regard. In GNOME you can inherit from GnomeApplication I think, but.... I only know this because I read it. No docs :(\n\nI hadn't thought of splitting the various Gimp components out. That would be good yes, but unfortunately KParts isn't really up to the job, as GNOME is Glib C and KDE is Qt/C++. That's why a decent object model is needed, to bridge these different platforms. But what?"
    author: "Mike Hearn"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "> You know I have the wierdest feeling I'm talking to Ali Akcaagac once more. Why\n> not use your real name also, Ex Gnome User?\n\nYes, that's who I am. But I prefer anonymoty over using my real name all the time. \n\nYour other reply is indeed a good one. To say in respect that this was the first reply from you that I really agree most with. I wished we could base all our comments on this which makes it a good base on further discussions. I mean what does the best implemention give one if the documentations are missing. Nothing that is the case with Bonobo and various other things. So far nothing more to add from my side.. Respect for this good reply."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "What is the point of anonymity if everyone knows who you are?\n\nI mean, I see a post, it consists of twelve line paragraphs, it is from someone who claims to have dropped KDE for GNOME or GNOME for KDE in the last couple of weeks, and mentions he is a developer, I know it's you! ;-)\n\nBTW: just how many times have you switched? I could swear I saw you posting as oGALAXYo on osnews saying you had switched to GNOME not even 2 weks ago, yet here you are, and you switched to KDE again?"
    author: "Roberto Alsina"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "> What is the point of anonymity if everyone knows who you are?\n\nSo, does everyone else know who I am ? I mean if so who actually cares what bad does it cause or what bad information will this give to you ? Even in the case I plug a sticker on my head saying hey I am GALAXY, will it actually change something ? trying to achive maximum anonymoty is my good right and nothing to feel ashamed of and nothing you or anyone else could blame me for.\n\n> I mean, I see a post, it consists of twelve line paragraphs, it is from\n> someone who claims to have dropped KDE for GNOME or GNOME for KDE in the last\n> couple of weeks, and mentions he is a developer, I know it's you! ;-)\n\nI haven't written that I dropped GNOME for KDE and vice versa, to say I am using both Desktops, permanently switching to the one and the other because I can't decide where to stay. After my pre KDE 3.0 experience I have seriously HARD times. Actually I use KDE here for doing my normal workd and some hours per day I am on GNOME because for development on Atlantis. Nothing bad for that and all this doesn't make my comments less reliable or less valid. I would appreciate if we can drop personalities here and concentrate again on productive Desktop conversation."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "Ali, I realy don't understand most of what you wrote, but as a general rule, lying is not a right.\n\nWhy do I say lying? Because you just said you use GNOME daily. But you post as \"Ex GNOME User\". So, you are saying you are an \"Ex User\" when you are not. And that is not true, and you know it. Therefore, it is a lie.\n\nNow, expression opinion is a right. Damn it, I express more opinion than average! And doing it anonymously is, of course your right.\n\nBut anonymity has a price. The price of anonymity is that you have no credibility. You have no credibility because noone knows who you are, and  why should anyone trust the opinion of the nameless unknown? And after you post anonymously things that are not true, well... let's say that I don't expect any oGALAXYo, Ali, whatever posts to be taken seriously, except by the uninformed.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "Listen, you are getting seriously too far right now. The Nickname I have chosen here was decided less than 1 second when pressing reply for the first comment I gave. Calling my a liar is highly insulting and you need to show me where I actually have lied. Didn't I gave you a fair answer after you asked who I am ? I mean what are you up to right now and what does it have to do with my good points that I have brought up regarding GNOME here with the nick 'Ex GNOME user' ? You can go from top to bottom of this entire Posting and read them then you can comment on them and show me wrong. Then and only then you can show me being a liar but not earlier. I don't know who you are and I don't even know who the other 2 are who permanently come up and trying something on me without any justification. I fully stand behind my comments made here which where in no case and in no situation either insulting to GNOME or KDE. Just made up my clear points on what I think. In what world do we life where I need to contact people to ask what I'm allowed to write and what not. Now do you have anything to add to what I have written as 'Ex GNOME user' and where you can show me being a liar ? If not then please simply shut up. All my comments here have grounding because I am probably one of the minor ones who seriously tried KDE and GNOME before opening the mouth and my comments about lack of documentations for developers are as valid as everything else I wrote."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "Ali, I never asked who you were, so you hardly could have given me a fair answer \"when I asked\". Now, you say, I must show where you lied? Simple, you claim to be an ex GNOME user, and you are not, and you obviously know it. It is very simple, really. \n\nPosting wat you posted, under thaat name gives the impression of a GNOME user who is now using KDE (apparently since less than ten days ago), while the truth is that you are a GTK+ app developer who _also_ uses KDE. So, the message sent is not truthful.\n\nIn fact, under your various aliases you have claimed to have switched from GNOME to KDE, and viceversa, unless my memory betrays me. \n\nAlas, a little googling shows it didn't:\n\" Well I recently switched back to GNOME 2.2 from months of using KDE 3.1 and I already start to regret my decision.\" from http://www.osnews.com/comment.php?news_id=2871&limit=no\n\nThis was not even 10 days ago. In that opst you describe your switching back and forth. I think you are not a KDE user, or a GNOME user, you are an undecided user.\n\nNow, is this \"ex GNOME user\" thing a big lie? No. Is it an important lie? No. Do I care at all about it? No. Is it a lie? Of course! And since I am reading and posting to this board, and others may not have enough information to properly judge your postings, I add a piece of fairly uncontroversial data. Of course it should not impinge at all in any verifiable data you may offer about facts. It will probably impinge on the credibility of opinions, of course.\n\nNow, you are experiencing what happens when you pretend to be several different characters (which is not strictly the same as being anonymous): It becomes hard to keep your stories straight.\n\nYou say \"In what world do we life where I need to contact people to ask what I'm allowed to write and what not\"\n\nI am confused by this. I never asked you to contact anyone about what you write. Then again, I am perfectly free to post whatever I want, as well. As for who I am, it is pretty easy to find out, since my name is in all my posts. Then, if you really REALLY want to verify I wrote this, just email me.\n\nWhat you wrote here, loudly complaining about things noone seems to have written (at least in this board) is quite bizarre.\n\nBesides, you seem to believe I am defending GNOME. Trust me, such is not the case. It is just the principle of the thing, really.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "Ok I see you have nothing to add to my comments that I made previously. I accept it as is then and leave you alone.\n\nIt's always the same with these people. Once placed in a bad position they loose any points of argumentation and start becoming rude and unfair by attacking the person itself than arguing about the Plattform they use.\n\nOne last sentence. You quoted right.\n\nhttp://www.osnews.com/comment.php?news_id=2871&limit=no#75782\n\nMy exact writing. Before I tried KDE 3.0 (which was only meant to be a 5 mins test and then scratch it again but unfortunately got me sold) I was using GNOME for years permanently. Then I tried KDE 3.0 some pre-releases of it used it for nearly 4 Months. Got bored because I left some code open for GNOME that I liked to finish and switched back to GNOME CVS now I really often switch forth and back. Stuck in the same trouble that other users and developers have. Knowing that KDE is what I like GNOME to be and then on the otherhand knowing that I did to much for GNOME to simply ignore it or leave it that easy. Where is your point ? If you have asked nicely then I would have given you the same answer. And how do you compromise my randomly chosen nick for this conversation (that I seriously cut out of my butt) to be a liar of what I said. Dude you just got an exclusive reply 3 times from me now.\n\nBut no this is not what you like to show the readers outside. Your real intent is to suggest the readers how much of an asshole I am and that I am not trustable. This is your real intent, that's what you and someone else tried here and that's what you are up to. You were not even willing to reply to my feedback about GNOME that I gave because you know that I'm obviously right and that you have no real arguments against it. So you plan to make me look bad in the public. Right or not ? 3-4 Writings from you and all are meant to make me look bad.\n\nI seriously shouldn't feed trolls."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "Ali, there is an obviour reason why I don't reply about your GNOME feedback. I don't care at all about GNOME! You see, I am not a ex GNOME user, or an ex KDE user. I am just a KDE user, and one time developer.\n\nIf I attacked you, well, you seem to be surprisingly reluctant to even accept the thought that saying you are an ex GNOME user when you use GNOME daily is, let's say, misleading. As you said \n\n\"It's always the same with these people. Once placed in a bad position they loose any points of argumentation\".\n\nAnd no, there are not \"3 or 4 writings from me\" and no, they are not all \"to make you look bad\". Perhaps you should go and read the thread?\n\nDude, this is probably not going to appear in the story because it seems the moderators have clamped down on us, but I hope you can read it. You really sound paranoid!"
    author: "Roberto Alsina"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "Ahem. NO, the moderators have not clamped (yet). I just saw the wrong subthread :-)\n\nBut anyway, that got me thinking. Why am I arguing this? I have nothing to add. So, Ali, be my guest, and finish the thread. Whatever you say will remain uncontested, even if I have to chop a finger to stop myself. \n\nMe? I think I will code a little."
    author: "Roberto Alsina"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-05
    body: "Trust me, we don't clamp down often at all.  That last time, well things got started to an obvious trollfest and got way out of control when someone started posting some nasty ill-justified profanity.  \n\nAll that and it was the second thread attached to a completely unrelated article.\n\nI hate the thought of deleting stuff but that was just abuse.  Wees got limits."
    author: "Navindra Umanee"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-06
    body: "Ex GNOME User says:\n\n\"I haven't written that I dropped GNOME for KDE and vice versa, to say I am using both Desktops, permanently switching to the one and the other because I can't decide where to stay.\"\n\nwhich is a direct lie, because he says in a post near the top,\n\n\"I have choosen. After some years suffering on the GNOME plattform I finally made it up to KDE and will stay there.\"\n\nSo, the message is, STOP LYING!  It does not help KDE when you lie to make a point."
    author: "anon"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-07
    body: "That is no lie it's all a matter how you lay out someone writings and how it was meant while it was written. You finally need to see things within a context and not cut it out of one.\n\nWhen saying that he likes KDE and that he suffers the past years on GNOME and writing that he finally made it up to KDE and will stay there then this is no lie. It's just explaining how much impressed that person is by using KDE because he expected all the nice features that is in KDE to be in GNOME as well. Unfortunately this is not the case and probably won't make it in GNOME either. How can we judge and say that we are not going to use something else one day. Who of us is able to look in the future.\n\nAnyways it's unfair sticking all the writings made by this person on one sentence he wrote. It's not the problem what he uses or not, that's finally his own decision. What matters is what he has to say in general. The whole GNOME project and everyone working on it are basically liars and so are we here on KDE.\n\nHow many times was it said that GNOME gets a new webpage with the GNOME 2.0 release. It never happened. The guy who promised it is a liar. Do we take that guy to the shambles and throw stones at his head ?\n\nThis whole community is build up on a big lie after all. How many times it was said that GNOME is the GNU project for desktops and now it's sold to commercial companies. What a big lie.\n\nNow let me ask you one question. You found one conflicting sentence written by that person. Please bring up the proof that everything else he wrote is also a lie."
    author: "anon2"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-07
    body: "Your lack of english skills gives you away man ;)  It is quite obvious that you are defending yourself by speaking in the third person and adopting the anon2 moniker.\n\n\"How can we judge and say that we are not going to use something else one day.\"\n\nWell apparently you had no problem doing this.  After all you said that you were going to stick with KDE from now on.  It's simple really. You lied.\n\n\"Please bring up the proof that everything else he wrote is also a lie.\"\n\nI don't have to.  I and the rest of society usually turn a deaf ear to people who lie, because they are untruthful.  You do yourself a disservice by lying.  I am as big a fan of KDE as anyone, but lying is not a good way to promote your favorite desktop.  If you wish people to take you and your comments seriously then ... don't lie.  Otherwise don't be surprised if you are dismissed out of hand.  Who wants to listen to a liar.\n\nThe 'everyone else is a liar so why can't I' is no defense.  It just makes you into a liar too.  That is a choice that you made.  Hopefully, you will choose not to lie in the future."
    author: "anon"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-07
    body: "Who should trust some anonymous jackass like you ? Sorry to call you that way but that's it after all. You don't comment on the context written by Ex GNOME user. Instead you only badmouth someone and try making him look like a liar in the public. Grow up and get some brain. If you have nothing to say about the context then simply shut up."
    author: "anon2"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-07
    body: "Your continued use of the third person is laughable galaxy ;)  I also find it interesting that you resort to calling me a 'jackass' when you asserted that personal attacks are the refuge of those without real arguments.  Another obvious hypocritical statement.\n\nContrary to your contention, I am not badmouthing you.  I am simply pointing facts regarding the truthfullness of your posts and obvious contradictions and attempts to mislead.\n\nI do have something to say about the context:\n\nIn the post where you clearly stated that you had chosen KDE and would no longer go back to GNOME you were responding to FooBar who stated that no WAR exists between the two desktops and choice was a good thing.  You responded that you had chosen KDE and went on to state that GNOME was a worthless desktop.  It is obvious from your statements in this post and in many others that you wish for a WAR to exist.\n\nNow onto your second post where you lied and claimed that you had not chosen...  You were responding to Robert who asked just how many times you have switched back and forth.  And this is where you lied by saying that you hadn't really decided one way or another.  A direct contradiction and lie from a post you had just made to FooBar in the same article right here on the dot.\n\nSo my advice in the future is try keeping your story straight and refrain from defending yourself in the third person."
    author: "anon"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-08
    body: "Ok Listen. I was only expressing my concerns about GNOME and these are my own opinions all I did was only comparing some tools between KDE and GNOME that's significantly all. Who are you some kind of judge ? Who actually gave you the right to judge about me as person and what I think is right or wrong and who are you judging about what I wrote ? You want to force me to say thinks that I don't belive in ? I bet it's not about my bad english that I used to learn in school. Until now you must have realized that it is not my native language. At the end It doesn't really matter for you what I have written or what I tried to express all you do right now is personally attacking me and I don't even know you, never hurt you, never personally met you! Nothing. Look I am just writing about software and that's almost it. Don't you think that people couldn't make their own opinion about what I have written or what not ? They don't need an minion. About the WAR shit you are writing here. I haven't taken the WAR argument in my wordings after all. What is wrong with you and what are you actually up to ? You are highly offensive, highly insulting and I can not tolerate such kind of slander and libel of my person. What did I do to you to deserve this ? What you actually do on the dot.kde.org list is showing that the people in the GNOME community are indeed stupid and really narrowminded people and this is really sad. Until now I friendly asked you several times to reply to the real context I made such as having a nice discussion about the Tools or the Desktop we use but until now no sign. Look what you do right now is you make yourself look like a fool and nothing else and I'm no dumbstore for your personal and emotional and immature issues.\n\nFriendly regards."
    author: "Ex GNOME user"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "http://bugzilla.gnome.org/show_bug.cgi?id=71514"
    author: "KDE User"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-06
    body: "Very cool!!! Very cool!!! So it might just happen. =)"
    author: "Kostumer"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "If The Kompany could break out HanComPainter from HanComOffice there would be a nice Photoshop clone for KDE. "
    author: "reihal"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-04
    body: "> If The Kompany could break out HanComPainter from HanComOffice there would be a nice Photoshop clone for KDE.\n\nPuh-lease! First off I believe they worked with someone who was starting an app open source and contracted him. Once this is done I'd bet dollars to donuts they have a clause in his contract to keep him from releasing the same thing free and undercutting their product. Beyond that the two companies are no longer affiliated.\n\nI for one would like to see Krita made usable. Pie in the sky won't do that and I have my hands full now with Quanta."
    author: "Eric Laffoon"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-06
    body: "Eric - mind to your own business.  You don't know what we do or what we are doing or what our business relationships are.  Alex and Dima send their love and wanted to make sure you haven't removed their credits from Quanta+ since they wrote 90 odd percent of it.\n\nBest,\nShawn\n"
    author: "Shawn Gordon"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-06
    body: "=)))  LOL, =)"
    author: "Kostumer"
  - subject: "Re: KDE Gimp GUI"
    date: 2003-03-06
    body: "Can I buy the HancomPainter only? What does the Koreans think about that?\n\n\n"
    author: "reihal"
  - subject: "Quanta code [was Re: KDE Gimp GUI]"
    date: 2003-03-07
    body: "Be sure that the credits for them are still there. There are some (one/two?) files completely rewritten where you cannot find their copyright notes anymore, and nor you can in the new files. And it's hard to say that 90 percent of the _current_ Quanta code is written by them. :-) The oldest copyright not is from Nov. 28, 1999. I joined the project in November 2001, so they worked 2 years on it. Now it's March 2003, so I worked almost one and a half year on it. Current CVS HEAD has 25125 lines of code (without Kommander), Quanta 2.0.1 (which already had some additions from me) has 16202 lines of code (without the kwrite fork). The meassuring was done with cxxmetric from the kdesdk. And there is a 341 lines long ChangeLog. Do I have to say more?\n\n I'm thankfull to them for writing such a good code that I could improve, and I don't want to take any merits from them. Their names show up on our web page, in the Authors tab in About Quanta, after the \"make install\" is done and in lot of other places in the code and the informational files.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Question"
    date: 2003-03-04
    body: "It was just a question, that's all."
    author: "Kostumer"
  - subject: "www.myspeciallinuxportal.com"
    date: 2003-03-04
    body: "I opened www.myspeciallinuxportal.com and I had a poll that rated KDE the best, please create a story for this.  Also, I put \"KDE R0><er2\" on my shoes, want a photo for a story? :)\n"
    author: "Super PET Troll"
  - subject: "mmmm ?"
    date: 2003-03-04
    body: "Bluefish better tan quanta LOL :P"
    author: "fty"
  - subject: "Re: mmmm ?"
    date: 2003-03-04
    body: "> Bluefish better tan quanta LOL :P\n\nThank you for your belated vote. ;-)\n\nEric Laffoon - Quanta+ Team Leader \nhttp://quanta.sourceforge.net  \nMailing list - http://mail.kde.org/mailman/listinfo/quanta\n"
    author: "Eric Laffoon"
  - subject: "File Sharing Apps?"
    date: 2003-03-04
    body: "I think most KDE applications are quite good. \n\n-KMail and KNode work fine for me. I do not need/want any more integration.\n\n-KOffice is IMHO much better for 95% of all tasks than OpenOffice. Sure, OO has\n much more features, but KOffice is lean and mean and has all the features I\n need.\n\n-The editor comparison is meaningless. emacs should be compared to kdevelop.\n\n-Quanta Plus vs. Bluefish: They can't be serious! Quanta Plus is so much better\n than Bluefish, they are not even in the same category.\n\n-What about IDEs? KDevelop would certainly win.\n\nBut one category I missed was File Sharing Tools. Is there any mature File Sharing Tool for KDE? I mostly use Lopster, which is GTK based. In fact Lopster is the only GTK based app that runs on my KDE Desktop right now."
    author: "Androgynous Howard"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-04
    body: "KNode has only one big problem - it does not have ofline mode. For us poor dial-up users:-)\n\nFile sharing, can Qtella do the job?\n"
    author: "Morty"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-04
    body: "Does it work? I never had much luck with the gnutella network. This does not have to do with the applications. I think the protocol just plain sucks!\n\nWhat I would like would be something like knapster2 but capable of connecting to multiple servers like lopster. "
    author: "Androgynous Howard"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-06
    body: "Or http://knapster.sourceforge.net/"
    author: "Morty"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-04
    body: "Use leafnode to get offline support with knode."
    author: "ac"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-05
    body: "Use KRN from KDE 1.1.2. Hey, it *IS* a KDE app! ;-)"
    author: "Roberto Alsina"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-04
    body: "mmmmm.\n\nI want IM integration with my mail suite. I've been waiting years for it. I'm sure the developers will have run out of other things to do by kde 3.3...."
    author: "Caoilte"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-06
    body: "There are already plans to move Kopete into the Kontact framework (which inludes KMail, KOrganizer. KAddress etc.) after both of them are reasonably usable. Exact timeframe is still to be decided tho ;)"
    author: "Datschge"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-06
    body: "really?\n\nthass cool.\nI thought it was just plans to go into kdenetwork at the moment. "
    author: "Caoilte"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-04
    body: "I'd defintely have to agree with most of the poll results. While I love the speed that KDE gives to my older computer, and thus try to use QT apps exclusively, there are simply things that can only be done in OpenOffice and Mozilla and Evolution.\n\nOne of KDE's strength is in it's integration, but on the other hand, all of those 'big 3' productivity applications are cross-platform to varying extents (Evolution having better IMAP support, and some enterprise Exchange capabilities). They're just going for different goals. I like Koffice for some things, but whenever things get complicated I find myself reaching for swriter. Likewise, I use the new gnome2-based galeon frequently, as I find it just renders things better. Konqueror will improve, but I respect the Mozilla project and gecko immensely. KHTML is designed to be lighter, and faster, but sometimes I have to fall back on gecko for strange sites. It's just using the best tool for the job.  Sometimes those tools are gtk/gnome based (like the amazing Straw, with no KDE counterpart in sight), and you gotta go with what works, regardless of environment. \n\nYou just have to be agnostic about these things sometimes. Some of the commentators on gnomedesktop.org and dot.kde.org just give the same arguments for diverse purposes."
    author: "Alex Cooke"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-06
    body: "If you do not need such basic functions such as the LOOKUP functions, then maybe in your little world, Kofice will be better for 95% of the stuff out there. The problem is that the other 5% is pretty important. Openoffice has the 'problem' of being cross platform, therefore it has to have its own UI interface.\n\nI think Eclipse would win the IDE wars. Its head and shoulder above the rest. It really competes with the Visual trashes of this world. I do not know how well you can make your GTK an QT apps on it though. But I think it just takes a plugin."
    author: "Maynard"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-06
    body: "\"Openoffice has the 'problem' of being cross platform, therefore it has to have its own UI interface.\"\n\nUnfortunately, projects like OpenOffice and Mozilla have propogated this myth.  It's completely untrue.  Take a look at anything done in QT, for example."
    author: "Jim Dabell"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-06
    body: "Qt based applications DO have their own interface, which is Qt :-)\n\nIt just so happens that Qt is\n\na) very good at mimicking the native UI\nb) a native ui on X (because it is popular and there is no official ui in X!)"
    author: "Roberto Alsina"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-07
    body: "Opera is written in Qt.\n\nOn Linux it looks anything but at home. Still stands I am sorry. That is what the 'problem' of being cross platform entails. Mozilla now mimics very well the GTK interface. Anyway, I think that it overrated, the look factor. On Windows, many apps look quite different. I think there are the core apps, which should look the same, and then also work together. If its a standalone app, I don't mind it looking different, if it is good and functional. I think variety is teh spice of life."
    author: "Maynard"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-07
    body: "It doesn't look at home not because it uses Qt, but because it fails to use it fully. Half the widgets it uses are nonstandard/custom. If it did use standard widgets, all you would have to do is tell it to use a KDE style, and it'd look the same, as happens with i.e. the various Qt tools like designer, assistant, linguist, and the QSA IDE. \n\nI do agree with you on your second point though."
    author: "Sad Eagle"
  - subject: "Re: File Sharing Apps?"
    date: 2003-03-08
    body: "> Opera is written in Qt.  On Linux it looks anything but at home.\n\nDefine \"Linux\".  It certainly wouldn't look at home on a text-only terminal.  It could well look different if you use GNOME.  But it looks at home in KDE.\n\n> Mozilla now mimics very well the GTK interface.\n\nMozilla reimplements standard GUI features like toolbars, ensuring it doesn't look at home/work the same anywhere.  It doesn't look like a normal Windows app, it doesn't look like a normal Mac app, it doesn't look like a normal KDE app.  I don't even see a lot of similarity between it and other GTK/GNOME apps.  If I tried dragging the toolbar about like I do with virtually every other app I use, it simply wouldn't work.  I've heard a defense that Mozilla does this to be consistent across multiple platforms, but that's not at all useful - I only use one platform at a time.\n\n> I think variety is teh spice of life.\n\nTrue, except with interfaces.  I only want to learn how to manipulate one toolbar, not a dozen.  The same applies across the board - scrollbars, menus, even non-computer interfaces like doors and cars.  Consistency aids usability.\n"
    author: "Jim Dabell"
  - subject: "choice awards"
    date: 2003-03-04
    body: "They must be kidding!!\nWhat does their audience look like!"
    author: "pieter philipse"
  - subject: "Don't forget KWin"
    date: 2003-03-05
    body: "KWin got third place in the \"Window Manager of the Year\" category with 13.87% of te votes. Fluxbox won and MetaCity edged out KWin by three votes. Congratulations to all developers."
    author: "Per"
  - subject: "Is this really a compliment?"
    date: 2003-03-06
    body: "Red Hat also won the best distro in these awards... I'm not sure it's a compliment to have been voted best by those very same people!\n\nMy preference of distros is: Gentoo, Debian and Red Hat... but RH only comes third cos it's the only other one I have much experience using..."
    author: "x3ja"
  - subject: "Re: Is this really a compliment?"
    date: 2003-03-07
    body: "My preference of distros is: Debian, Gentoo, Mandrake, SuSE, Slackware, Lycoris, Xandros, Lindows, Yoper, ..., ..., FreeBSD, OpenBSD, ..., ..., Windows* (just kidding ;) ..., ..., RedHat\n\nheh heh"
    author: "anon"
---
<a href="http://www.linuxquestions.org/">LinuxQuestions.org</a> has just
<a href="http://www.linuxquestions.org/questions/showthread.php?s=&threadid=47970">announced</a>
the results of its <em>2002 LinuxQuestions.org Members Choice Award
Winners</em>.  KDE <a href="http://www.linuxquestions.org/questions/sh
owthread.php?threadid=39873">took first place</a> in the category
<em>Desktop Environment of the Year</em> with 59.14% of the votes.
KDE applications also did very well in virtually every other GUI category.
Congratulations to the many talented and dedicated developers whose
achievements have been recognized.
<!--break-->
<table border="0" cellpadding="0" cellspacing="8">
<tr>
  <th>Category</th>
  <th>Application</th>
  <th>Showing</th>
  <th>Notes</th>
</tr>
<tr>
  <td><a href="http://www.linuxquestions.org/questions/showthread.php?threadid=40659">Web Development Editor of the Year</a></td>
  <td><a href="http://quanta.sourceforge.net/">Quanta Plus</a></td>
  <td>Second place (47.65%)</td>
  <td>Bluefish took first place</td>
</tr>
<tr>
  <td><a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39878">Mail Client of the Year</a></td>
  <td><a href="http://kmail.kde.org/">KMail</a></td>
  <td>Second place (26.76%)</td>
  <td>Evolution took first place from KMail this year with 35.55%</td>
</tr>
<tr>
  <td><a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39876">Office Suite of the Year</a></td>
  <td><a href="http://www.koffice.org/">KOffice</a></td>
  <td>Second place (10.16%)</td>
  <td>OpenOffice was the big winner</td>
</tr>
<tr>
  <td><a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39867">Browser of the Year</a></td>
  <td><a href="http://www.konqueror.org/">Konqueror</a></td>
  <td>Third place (15.61%)</td>
  <td>Mozilla was the big winner; Galeon snuck into second</td>
</tr>
<tr>
  <td><a href="http://www.linuxquestions.org/questions/showthread.php?threadid=40296">Word Processor of the Year</a></td>
  <td><a href="http://www.koffice.org/kword/">KWord</a></td>
  <td>Third place (11.26%)</td>
  <td>swriter (OpenOffice) and AbiWord took first and second</td>
</tr>
<tr>
  <td><a href="http://www.linuxquestions.org/questions/showthread.php?threadid=40295">Speadsheet of the Year</a></td>
  <td><a href="http://www.koffice.org/kspread/">KSpread</a></td>
  <td>Third place (10.55%)</td>
  <td>scalc (OpenOffice) and Gnumeric took first and second</td>
</tr>
<tr>
  <td><a href="http://www.linuxquestions.org/questions/showthread.php?threadid=39875">Editor of the Year</a></td>
  <td><a href="http://kate.kde.org/">Kate</a></td>
  <td>Fifth place (7.51%)</td>
  <td>CLI editors were the big winners:  Vim, Emacs, vi and pico</td>
</tr>
</table>
