---
title: "Report: Visitors to Linux World Frankfurt Give KDE A Thumbs Up"
date:    2003-11-13
authors:
  - "dmolkentin"
slug:    report-visitors-linux-world-frankfurt-give-kde-thumbs
comments:
  - subject: "Great!"
    date: 2003-11-13
    body: "Always great to see KDE get more positive PR. Hopefully KDE @ Comdex will be a hit too!"
    author: "anon"
  - subject: "Kontact"
    date: 2003-11-13
    body: "Kontact's website hasn't been updated for a while. Shoudn't take long - just some up to date news on its status and some more screenshots and functionality descriptions."
    author: "David"
  - subject: "Re: Kontact"
    date: 2003-11-14
    body: "Looks like kontact.kde.org is looking for a maintainer to take over the task."
    author: "anon"
  - subject: "Why didn't I see anything about ArkLinux before???"
    date: 2003-11-14
    body: "I'm a bit puzzled that I never before heard anything about arklinux...\n\nDoes anyone know which version of KDE they use? is it the latest?\n\n/Dave\n"
    author: "David E"
  - subject: "Re: Why didn't I see anything about ArkLinux before???"
    date: 2003-11-14
    body: "Yes, see http://kde.ground.cz/tiki-index.php?page=Distributions+Shipping+KDE.\n\nHTH,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Why didn't I see anything about ArkLinux befor"
    date: 2003-11-16
    body: "ArkLinux is in my opinion one of the most promising distributions. They have a quiet demeanor and seem to be working silently towards a very, very polished distribution.\n\nI think they deserve more of the support of the KDE community as they are one of the few completely GPL distributions that is 100% behind KDE.\n\nThe installation takes 20 minutes. I encourage anyone to give it a shot.\n\nRegards,\n\nGonzalo"
    author: "Gonzalo"
  - subject: "Ark looks interesting"
    date: 2003-11-14
    body: "A community driven, user friendly distro with KDE has been missing IMO.\n"
    author: "frappa"
  - subject: "Re: Ark looks interesting"
    date: 2003-11-14
    body: "If you are interested in a great community driven distro, look no further than Gentoo (www.gentoo.org) To see a sampling of the amazing super-vibrant community, check forums.gentoo.org. KDE is favored by many Gentoo users and some of the KDE dev team use and love Gentoo. I compile KDE CVS a couple times a week and use the build scripts (called ebuilds) provided by KDE developer and Gentoo ethusiast Caleb Tennis. "
    author: "Imrahil"
  - subject: "Re: Ark looks interesting"
    date: 2003-11-14
    body: "I use Gentoo and it seems that enough of our developers do that you can easily get Gentoo questions fielded on our list. I'm also surprised at times to find that seeming n00bs are asking questions about their Gentoo install. Gentoo is not focused on KDE or GNOME but instead offers these and other packages as the developers intended them. Also because you compile it for your system, set the compiler optimizations and only include what you need in the kernel it's amazingly fast. You should have broadband to use it though. As an extra benefit you can forget the CD upgrade merry go round."
    author: "Eric Laffoon"
  - subject: "Re: Ark looks interesting"
    date: 2003-11-14
    body: "Also gentoo makes getting and compiling a copy of kde-cvs (or betas) as easy as getting kde 3.1.x, because it's a source based distro. No waiting for someone to update binary packages of kde-cvs for your distro because you can't build cvs properly. No wonder that as of late, a lot of bug reporters who use CVS/3.2b1 on bugs.kde.org are gentoo users."
    author: "anon"
  - subject: "Re: Ark looks interesting"
    date: 2003-11-14
    body: "Erm, ever heard of two community based distros called Debian and Gentoo that offer great KDE support (including have KDE developers as the packagers)\n\nBoth are great, I currently favor Gentoo because of it's great forums, but I used Debian for a number of (3) years."
    author: "fault"
  - subject: "Re: Ark looks interesting"
    date: 2003-11-15
    body: "Yes, but I meant user friendly in a point, click and drool sense. Most people want this and Debian and Gentoo doesn't fit that description.\n"
    author: "frappa"
---
Continuing the successful run of trade show presences, the KDE team used 
the opportunity of this year's <a 
href="http://www.linuxworldexpo.de">Linux World Expo and Conference</a> in 
Frankfurt, Germany to show off the latest K Desktop Environment. The booth staff presented the latest KDE pre 3.2 snapshots hot off CVS
as well as the last stable release KDE 3.1, to a broad audience ranging from 
home users to system administrators, professional developers, IT managers and government representatives. The most common question from booth visitors: <i>"I can't wait for 3.2 to be released, can you tell me the exact release date?"</i>
<!--break-->
<p>
The KDE usability study got a lot of attention. To show the marvelous ease of 
use of a corporate KDE desktop, <a href="http://www.basyskom.de/">basysKom</a> and <a 
href="http://www.relevantive.de/">Relevantive AG</a> presented the results of 
the <a 
href="http://www.linux-usability.de/download/linux_usability_report_en.pdf">usability 
study</a> along with the test environment and videos from the tests. Of 
course, an introduction to KDE's <a 
href="http://www.kde.org/areas/sysadmin/">KIOSK mode</a>, which e.g. allows 
for enterprises to lock down their employees' desktops, was included. <i>"We enjoy KDE for its simple and uncomplicated administration possibilities in large scale setups"</i> said one visitor.
</p>
<p>
Many people asked about <a href="http://www.kontact.org/">Kontact</a>, and were 
happy to see it included in the next major release. They were amazed to see 
that all the KDE <a href="http://pim.kde.org/">PIM</a> components can now act in an 
even more integrated way. IT managers from companies and public authorities 
were interested in employing Kontact as soon as possible.
</p>
<p>
During the fair, KDE's Tobias K&ouml;nig and the <a 
href="http://www.egroupware.org/">eGroupware</a> folks finished a 
proof-of-concept eGroupware connector for Kontact based on XML RPC which was 
shown at the eGroupware booth. This once again demonstrates the amazing flexibility of the KDE PIM framework.
</p>
<p>
People were also amazed by the brand new <a 
href="http://www.kdevelop.org/">KDevelop 3.0</a>, dubbed Gideon, which will 
have its initial stable release with KDE 3.2. It features support for more 
than a <a 
href="http://developer.kde.org/documentation/library/cvs-api/kdevelop/html/LangSupportStatus.html">dozen 
programming languages</a>, several build environments and reversion control 
systems such as the broadly used <a href="http://www.cvshome.org/">CVS</a>, 
the new <a href="http://subversion.tigris.org/">Subversion</a> and <a 
href="http://www.perforce.com/">Perforce</a>. The new improved help browser with lots 
of documentation, which crowns the feature palette, as well as as-you-type 
syntax check and syntax-completion won a lot of visitors over to KDE 
development with KDevelop.
</p>
<p>
People were also surprised when Eva Brucherseifer showed the ease of platform 
independent programming with <a href="http://www.trolltech.com/">Trolltech's Qt</a>, the flexible C++ toolkit which serves as the foundation for KDE.
</p>
<p>
<a href="http://www.arklinux.org/">ArkLinux</a>, a distribution devoted to 
bring Linux on the desktop with great ease, was featured at the computer show 
"<a href="http://www.3sat.de/neues/">3Sat neues</a>". They portrayed KDE 
developer and ArkLinux founder Bernhard Rosenkr&auml;nzer installing his KDE 
based distribution on an Athlon64 system.
</p>
<p>
Personal discussions at the fair proved that KDE is running successfully in 
many  companies and at public authorities and its success and popularity 
still increase. The KDE team is looking forward to Linux World Expo & 
Conference 2004.
</p>
<p>
Thanks go to <a href="http://www.hp.com/">HP</a> for sponsoring hardware, <a 
href="http://www.basyskom.de/index_en.phtml">basysKom</a> for even more 
hardware and manpower, and finally to <a 
href="http://www.credativ.de/">credativ</a> and the <a 
href="http://www.debian.org/">Debian</a> team for running the infrastructure of 
the OpenSource booth.
</p>