---
title: "KDE-CVS-Digest for February 21, 2003"
date:    2003-02-22
authors:
  - "dkite"
slug:    kde-cvs-digest-february-21-2003
comments:
  - subject: "Like Clockwork :-)"
    date: 2003-02-21
    body: "This is really my primary source of news on the day to day happenings within KDE.  It can't be said too much ... great job Derek!"
    author: "Adam Treat"
  - subject: "Re: Like Clockwork :-)"
    date: 2003-02-21
    body: "Merde, you beat me to the first \"Thank you, Derek\" post. :-)\n\n"
    author: "Eike Hein"
  - subject: "Re: Like Clockwork :-)"
    date: 2003-02-26
    body: "Glad you enjoy it. Doing it weekly was the primary goal of the project. Now the challenge will be continuing during my busy work period. May to August gets rather hairy.\n\nDerek"
    author: "dkite"
  - subject: "Word import"
    date: 2003-02-21
    body: "Yay, David Faure is working on the Word import filter for KWord!  Hopefully KOffice 1.3 will have a nearly complete filter.  That would make KWord a lot more useful for me, at least.  Thanks for the updates, Derek!"
    author: "not me"
  - subject: "Re: Word import"
    date: 2003-02-22
    body: "Yes, and it seems most of the really important things are in there (except for images)"
    author: "Peter"
  - subject: "Re: Word import"
    date: 2003-02-28
    body: "Important for you...important for everyone. The decisionmakers who choose \nsoftware won't even consider an office package that isn't 100.00% \ninteroperable with M$ Office. Sad but true.\n\nWhy I used to work for one of the major movers behind OpenOffice.org where\nwe used Applix company wide. There were enough problems importing from \nand exporting to Word and Excel documents for customers that I would say that \nhad more to do with the purchse of Star Office than any other gallant pupose.\nAnd why not, not being able to work with the customer means no sales, no \nsales means no company, and I thnk we all understand the rest.\n\nThe completeness of the Word and Excel input filters for the office suites will \nhave more to do with the expansion of Linux onto the corporate desktop than \nany single application or operating framework now or in the forseeable future.\n\nHow do I know this? I have had this conversation with the decision-makers\nwho considerd using *N*X as their enterprise desktop when making the switch\nat Y2K. Interoperability was the one flaw that stopped the migrations. One of\nthese was a LARGE enterprise too, a major Utah university. While the other \nwas not nearly so large, they were very visible on the corporate radar. \n\nFilters, filters, filters!! Good Luck to the author that he might get them as near\nto perfect as is humanly and economically possible!\n\nNow lets talk about printing...\n\nLater all,\nKerry"
    author: "Kerry Whittle"
  - subject: "User Interface"
    date: 2003-02-21
    body: "> Aaron J. Seigo committed a change to kdebase/kdesktop\n>\n> allow running apps by their desktop file name (as a last resort)\n> would be nice to make it case insensitive one day, [...]\n\nNO!!! Never ever make anything file system related case insensitive! That's the root of all evil\nand still bites Micros~1 users in their asses. Unix file systems aren't case insensitive. Period.\nI'm afraid I will have to sue somebody, if I type \"RM -rf ~\" and KDE chooses \"rm -rf ~\" instead, because this cool new RM program happens not to be in the $PATH.\n\nThe correct answer to the application name vs. binary name problem IMHO is, to either make\na few hundred links (ln -s KMail kmail), or to ask the user: \"There's no application 'KMail', did you mean 'kmail'?\" Guessing what the user might have meant is not an option.\n\nm."
    author: "Melchior FRANZ"
  - subject: "Re: User Interface"
    date: 2003-02-21
    body: "i wasn't talking about making all commands in minicli case insensitive, just the check for .desktop Name= entries which are checked AFTER looking for the program in your PATH, looking for servicenames, looking for .desktop file names, etc... so it wouldn't run random commands on disk (e.g. /usr/bin/RM vs /bin/rm) but only work w/regard to the names you see in the KMenu as a last ditch attempt to figure out what you want before trying to resolve it as a domain name.\n\nsomeone typing \"quanta plus\" probably means \"Quanta Plus\", wouldn't you say? and for the record, i've NEVER 'rm -rf'd from minicli ;-) i'd really like to see this go even further and try some clever heuristics so if you do \"quanta+\" and that doesn't match anything in your PATH, services, .desktops, etc.. it runs Quanta Plus which is actually just quanta. but that's for 3.3...\n\nas for asking the user \"did you mean Foo?\", that would be easily implementable once case insensitive matching is there. but that's the big job since it means mucking with ksycoca's hash storage of things, which is performance critical and not something to do lightly obviously =)"
    author: "Aaron J. Seigo"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "The problem is, that with these changes the user is no longer is control of which application will\nbe started. He won't even notice. If I type \"kMaiL\" and there is no such binary, then I want to\nget a warning, and not have \"kmail\" started silently behind my back. I might have meant\n\"kmail\", but then again, I might not have. What comes next? An animated paperclip that starts\nkonqueror when I requested mozilla?\n\nAnd for the record: I've never used \"rm -rf\" from minicli myself. So why would I want it started,\nwhen I was actually typing \"RM -rf\". I'm not a frequent minicli user anyway. I always have a\nkonsole open. But I don't want to apologize for things like that every time I recommed KDE\nto some advanced user.\n\nPeople who are interested in typing-in commands, rather than clicking on colored pictures, are\nprobably also interested in literal execution, rather than computer aided guesses. (Hey, what\nabout making this configurable. ;-)\n"
    author: "Melchior FRANZ"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "before my patch, mincli would already try the .desktop files by name. now it also tries the Name= entry (localized to your language, of course). minicli is not hogtied to the command line, or even the filesystem since (for example) it undertands and launches URLs.\n\nif you type kMaiL and there is a kMaiL binary then it will get launched.\notherwise next it tries services and .desktop file names\nif we still don't have a find then it tries Name= entries\nif it STILL doesn't find anything then i'd like it to try the Name= entries case insensitive. note that this isn't looking at actual binaries (we've already done that) or random input (it's using the .desktop files)\n\ngiven the above set of rules, can you describe a real world situation where this will not work as expected?"
    author: "Aaron J. Seigo"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "Easy: what if I have two desktop files: FOO and Foo (wich refer to programs with completely different names), and I try to start foo. Which one will get started? What if I have these: Foo and food, I type foo, but I just incidently hit the Enter button before the d. I will end up with Foo, not what I wanted at all! \nReally, I like the idea of trying to help the user find his software, but I really think a \"Did you mean Foo?\" or even a, to refer to my first example, \"Did you mean FOO or Foo?\" (\"FOO\", \"Foo\", \"Neither\") would be order. Adding a checkbox \"Don't ask me again\" so the user won't be bugged again about _this_ name could help users who like the feature, just like the user should be able to turn it off completely."
    author: "Andr\u00e9 Somers"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "> what if I have two desktop files: FOO and Foo (wich refer to programs with\n> completely different names), and I try to start foo.\n\nthis is pretty contrived, but possible. the solution is easy, as you note: it would need to ask which one was intended.\n\n> What if I have these: Foo and food, I type foo, but I just incidently hit the Enter\n> button before the d\n\nred herring. what if i type local and hit enter accidentally when i wanted to execute locale instead. so this particular issue has nothing to do with capitalization."
    author: "Aaron J. Seigo"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "Oh, but it does! \nI did not name Foo with a capital for nothing, nor is food called like that for nothing. Please keep in mind that there are more languages than English, and that words with different meaning may look very much alike in these languages. Anyway: what's the hurt in _asking_ the user what he wants? Sure, it may take more than that six lines of code, but it may also save a lot of frustration.\nMy point is: you should be wary to make assumptions on how \"the user\" would want the system to react, especially with things as fuzzy as this. One of the things I like most about Linux/KDE is that is does not try to decide for me what's good for me or what I may want..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "I'm sorry, but Aaron is being quite reasonable. I appreciate that you are trying to be perfectly set-complete about the namespaces, and case sensitivity is good for that.\n\nThat said, however, people almost never put files of the same name with different capitalisation in the same directory as a matter of choice, because *it is confusing*! The English language (or indeed any other), was not parallel assignment of words to different concepts. It happens, but not often, and in social and oral environments, it causes a great deal of confusion. In fact, Chinese is an example of a language that has parallel assignment with substantially different meaning. Words are distinguished by changes in tone, such as a word rising in tone at the end, staying level or lowering. Tonal accidents are an entire form of humour in Chinese! My advice, make sure your files have properly unique filenames... It'll help you and others who ever want to use them.\n\nCase sensitivity is a good thing in that it helps reduce namespace clashes, but only up to a point. In usability terms, it has serious issues. Perhaps someone had intended to start Scribus, but it had been named ScRibUs. I'd like to see the length of time it would take most users to be able to launch it this way without case insensitivity. They will get the case wrong many times. I know I certainly would.\n\n>What if I have these: Foo and food, I type foo, but I just incidently hit the Enter button before the d. I will end up with Foo, not what I wanted at all! \n\nBad example... That is like saying that I typed './h' and expected /home/dawnrider/mydocs/server/ip/thisbin to be executed.\n\nIf someone types kmail, that isn't found naturally and it gets to the stage of searching the desktop files, picking 'Kmail' would satisfy the majority of people. The people with 'Kmail' and 'KMail', who received the former and wanted the latter would be tiny, compared to the rest (of an already small group).\n\nI appreciate being able to be absolutely correct every time, but sometimes 'close enough' will do. This is probably one of those times :)"
    author: "Dawnrider"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "I don't want to continue this debate ad infinitum and this is probably my last reply. However, I\nwas quite amused about the attempts to elaborate how case insensitivity is an improvement.\n\n* \"minicli is not a command line but an URL launcher\":\nLOL -- There are two parts in URLs that are case insensitive: the protocol (\"file:\" in our case)\nand the hostname (\"localhost\" in our case). Everything else =is= case sensitive. So this\nargument backfires.\n\n* \"why not drop tab completion as well\" (meant to be sarcastic):\nLOL -- What a great example for the right thing to do: 1) Tab completion isn't forced on you.\nYou have to activate it (<TAB>). 2) It doesn't do anything behind your back. It =shows= the\nsystem's suggestion. 3) It asks for confirmation. You can still edit the suggested command\nor parameter. Now, these three points are exactly what I'm asking for. So this argument\nbackfires as well.\n\n* \"case sensitive filesystems are bad, anyway\":\nThis opinion is pointless. KDE runs almost exclusively on systems with case sensitive\nfile systems. (Exceptions: (v)fat mounted devices; the KDE2 port to MICROS~1)\nThis is the world we are living in. Case insensitive file systems are obsolete. And there's\na reason for it. I'm even willing to discuss this on a newbie list.\n\nThe main problem is, that this new mechanism takes predictability from the system.\nPredictability is one of the most important properties of a reliable computer system.\nWhat's wrong with \"educating\" the user? If someone asks KDE to start kmaiL and\nthere is no such thing, he should be told. He will profit next time when he tries to start\nkmaiL in konsole, where there's no Clippy around.\n\nWhile the current behavior is already reason enough for me to warn people against using\nminicli(ppy ;-) for starting programs (\"might start something else\"), the comment \"would be\nnice to make it case insensitive one day\" is alarming. Is this where we want to go tomorrow?\n\n(I don't mean to sound arrogant and I'm not religious about this issue ... no, wait. I =am=\nreligious about it. I'm believing in the one true religion of Unix. And I know that I can change\nthe code in my local copy. It's not like this would be the first change in my KDE. :-)"
    author: "Melchior FRANZ"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "> no, wait. I =am= religious about it. \n> I'm believing in the one true religion of Unix. \n\nReligious arguments aren't the best way to \nmake an engineering or a GUI decision.\n\nThere is no one true religion of Unix. Perhaps\nyou refer to the ancient AT&T stuff and it's\nideals, not the result of 30 years accumulating \nstuff.\n\nBy the way, Mac OS X is Unix and is case \ninsensitive down to the Terminal level.\n\nSo, there!\n\n"
    author: "Space Cowboy"
  - subject: "Re: User Interface"
    date: 2003-02-23
    body: "Who takes Mac OS X, at the user level (where it's case-insensitive) seriously as a UNIX? It might be a UNIX at the lowest levels (where it is case sensitive), but not in the absolutely hacked up, \"let's hide everything from the user even in Terminal.app\" user level."
    author: "Rayiner Hashem"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "> Case insensitive file systems are obsolete. And there's\na reason for it. I'm even willing to discuss this on a newbie list.\n\nI don't know how you get this. NTFS is pretty much a case insensitive, as is HFS+.\n\nWhat is really evil is case destroying file systems such as VFAT and many network file systems (sans nfs.) \n\nFace it.. if you want grandma or Joe Little-Skilled Office Worker to use KDE, you can't expect them to remember case in binary names.  Ideally, they shouldn't be forced to remember the binary names either... :)\n\n> Is this where we want to go tomorrow?\n\nPersonally, I do, but doing it at anything under the desktop would be painful for more advanced users perhaps. Still, it makes sense usability wise in most instances."
    author: "fault"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "> * \"minicli is not a command line but an URL launcher\":\n> LOL -- There are two parts in URLs that are case insensitive: \n> the protocol (\"file:\" in our case) and the hostname (\"localhost\" \n> in our case). Everything else =is= case sensitive. So\n> this argument backfires.\n \ni supplied URLs as an example to demonstrate that minicli is not tied to the command line as was asserted. and since there are parts of a URL that are case insenstiive, it obviously shows that there are places where that makes sense. case insensitive matching on .desktop Name= entries is another.\n\nminicli is a way for human beings to interact with \"commands\", be they in .desktop files, on the network or in their file system. minicli is human centric, not resource centric. and humans are decidedly non-case sensitive.\n\n> While the current behavior is already reason enough for me to warn people against\n> using minicli(ppy ;-) for starting programs (\"might start something else\"), the\n\nyou really have absolutely zero clue about what the patch i committed does, don't you? if you don't understand it, which you obviously don't, please don't comment on it. there's enough useless noise in the universe as it is."
    author: "Aaron J. Seigo"
  - subject: "Re: User Interface"
    date: 2003-02-23
    body: "humans are decidedly non-case sensitive\n>>>\nThat's entirely untrue.\n\nGnarfnarkel <--- proper noun\ngnarfnarkel <--- adjective or verb, other noun\nGnarf Nar Kel <--- Title\ngnarf nar kel <--- three random words.\nGNARFNARKEL <--- Exclamation\n\nYou can get a great deal of information out of an arrangement of words, just from the casing of the words."
    author: "Rayiner Hashem"
  - subject: "Re: User Interface"
    date: 2003-02-26
    body: ">humans are decidedly non-case sensitive\n\nA pacifist would definitely not interpret the files Chirac_agrees_with_us.pdf and  Chirac_agrees_with_US.pdf the same way :-)"
    author: "oliv"
  - subject: "Re: User Interface"
    date: 2003-02-23
    body: "I agree 100% with everything that Melchior has said."
    author: "Joe User"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "Right. And please disable tab-completion, people want to write the whole name. Otherwise they dont remember the spelling when using tools that dont support it. And please disable this ASCII input, enter the name is hexcode. People should be reminded that the name is just a sequence of numbers. After all were elite unix users, no whiny windows users."
    author: "Tim Jansen"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "Tab completion is quite different as pointed out elsewhere -- It does not execute its guesses automatically. In fact, the equivalent of tab completion for minicli would be great, even if case insensitive, assuming it waits for enter after offering a canonical (correct?) application name."
    author: "Jan Schumacher"
  - subject: "Re: User Interface"
    date: 2003-02-24
    body: "Go back and re-read the post you're answering to, but now in \"satire-enabled\" frame of mind ;-)"
    author: "Robert"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "> If I type \"kMaiL\" and there is no such binary, then I want to\nget a warning, and not have \"kmail\" started silently behind my back. \n\nWhy not? Isn't it what you meant?\n\n> The problem is, that with these changes the user is no longer is control of which application will be started. \n\nPerhaps.. but it sounds like if the software is now empowering the users to be more productive. Software should work for the users, not the other way around.\n"
    author: "fault"
  - subject: "Re: User Interface"
    date: 2003-02-22
    body: "How many non-experts (e.g. people that don't know that linux is generally case-sensitive and/or don't know the correct spelling of application names) do actually use Alt-F2 or \"run command\" to start ANY application. These people usually just browse the application menu until they find what they need and only use the run feature if someone explicitly tells them to type in a command name (on windows that might be regedit, then search for key and edit).\n\nI really don't see how this feature will help new users, but if people really think it does please a east add a confirmation dialog.\n\n"
    author: "Anonymous"
  - subject: "Re: User Interface"
    date: 2003-02-23
    body: "About one of the messages in the KDE .cvs digest (by Aaron J. Seigo)\n\n>> it was done at the request of a user wondering why they couldn't just type the same name that they saw in the menu.\n\nTo be honest with you I did think that this patch was a good idea at first, it is a somewhat drastic change in the purpose of 'Run Command...', it should be renamed to reflect the change to something like 'Run Program...'.  A 'Run Program...' button is much more useful than a 'Run Command...' button (IMHO) because when I want to run a command I will use Konsole (or hit CTRL+ALT+F2, or whatever).  I use 'Run Command...' / ALT+F2 because I want to launch a program and it is quicker (and requires less thought) to type its name than to take my hands of the keyboard and search through menus/click an icon (for me atleast).\n\n>> allow running apps by their desktop file name (as a last resort)\nwould be nice to make it case insensitive one day, but that will require\nsome fairly drastic alterations to the ksycoca storage, methinks\n\nI am not a KDE developer but I fail to see how it would it be a drastic change.  Surely it wouldn't require much more than converting the name given to lowercase and testing it against the lowercase equivalents of the .desktop names you have already tested it against.\n\n<b>Please, consider a change of name (at least in the long term) to reflect the new functionality</b>"
    author: "Anonymous"
  - subject: "case"
    date: 2003-02-22
    body: "The only reason there are any problems with Windows being case-insensitive is that every other OS isn't.  If every OS was case-insensitive, then there would be no problems.  Actually everything would be nicer.  There is *no* reason to have a case-sensitive filesystem.  If I give a name to something, I don't want anything else to have a name that could be confused with it just because one or two of the letters are uppercase, and I don't want to have to try and remember whether I capitalized certain letters when I named my file or not (or try and figure out the case convention used by some other guy when he named all his files).  Case sensitivity is confusing for inexperienced users and adds nothing useful for power users.  The only reason computers are case-sentitive in the first place is that it is the path of least resistance when using ASCII.  Just laziness.  If I ever make a programming language or OS, it will be case-insensitive all the way.\n\nI wish KDE could do something about this, but it is really too late.  Changing Linux now is totally impractical, and it goes against POSIX and accepted practice anyway.  Kinda a shame, really."
    author: "not me"
  - subject: "Re: case"
    date: 2003-02-22
    body: "So, check out the diff between 'a' and 'A' in an ASCCI table.\nThey are differnt.\nSo why sould the computer handle them, like they are the same?\n\nSome peopla user the case of a word for sorting their files and directories.\nSo, why take this option from them?"
    author: "Micha"
  - subject: "Re: case"
    date: 2003-02-22
    body: "Of course they are different in the internal representation of the computer.  But exposing the computer's internal representation to the user is the cause of an enormous number of usability problems.  The user shouldn't care that the computer sees them differently.  The computer should conform to the user's expectation.\n\nAs for using the case of a word for sorting, I'm not sure what you mean.  I think it is a silly idea to sort 'Z' before 'a', if that's what you mean.  If what you really want is a way to make a directory be listed at the top when you type 'ls', then you should implement that feature in a way that doesn't break standard alphabetic sorting.  Maybe a special \"priority file\" flag or something.  I can see how such a thing could be useful.  But using case to do it is stupid, since it breaks users' expectation of how sorting works.  Before I got used to this UNIX quirk, I often had trouble finding files that had the misfortune to be named with an uppercase letter at the beginning, because they weren't in the correct sorted place."
    author: "not me"
  - subject: "Re: case"
    date: 2003-02-22
    body: "for me its logical, and I expect that when I do a ls, the README shows up before anything in lower case.\nIf you think, Joe User needs some differnt behaviour, ok, BUT make that option configurable.\nA hint for not losing files: give everything lower case letters, except the _very_ important ones.\n"
    author: "Micha"
  - subject: "Re: case"
    date: 2003-02-22
    body: "I never make files that have capital letters in them.  That doesn't help me much though.  Maybe you don't, but I often want to deal with files not named by me, and it never ceases to annoy me when (for example) I can't find a manpage because it's XF86Config instead of xf86config or XF86config or xf86Config or Xf86config.  For you it is logical because you're used to it, and that is precisely why KDE can't change it now.  Too many people are used to it the way it is; the case-sensitive attitude is pervasive in UNIX.  But case-sensitivity is hardly found anywhere except computers.  Certainly nowhere but computers (and UNIX in particular) are uppercase letters sorted before lowercase letters.  Casual computer users and newbies are apt to be confused by case-sensitive behavior."
    author: "not me"
  - subject: "Re: case"
    date: 2003-02-22
    body: "Man, the argument you've stumbled onto is the one that was on a few days ago involving mosfet.\nKeep useful features or take em away from all the users because some of them might be dumb.\nCase sensitivity gives very useful features to a lot of people. Why take it away?\nI'm in the mosfet camp. I've never found it hard to remember cases of my binaries.\nDon't make me suffer because some people are retarded. You might as well just use something like \nwindows if you want everything dumbed down to the lowest denominator and that's exactly the reason\nwhy I fdisk'ed my windows partition.\n(the above is about the silly idea of making case insensitive Linux, not the kde patch)\nActually, I've just had a great idea. If you think case insensitivity is so great, start your own distro.\nLet's see how many people use it.. I'm thinking of a number, it's lower than 2."
    author: "vod"
  - subject: "Re: case"
    date: 2003-02-23
    body: "Who is advocating taking away Linux's case sensitivity?  Not I!  I would be the last to propose such a thing.  I think you need to re-read my posts.\n\nThat said, I still have yet to see what \"useful features\" case sensitivity gives.  The sorting thing is bunk.  Are there any other benefits to case-sensitivity that I don't know about?"
    author: "not me"
  - subject: "Re: case"
    date: 2004-07-20
    body: "Case sensitivity is evil!  Well, really bad at least.  The anti-MS camp or language snobs don't think so, but all you here out of them is \"We don't have a problem remembering case\".  The very point is that you shouldn't have to have gotten used to it to begin with!  It's a notion whose time has long passed.  Computers are not the place for semantic meaning; this is not the English language.  There should be no difference between things that are spelled the same way, just cased differently.  It serves no purpose.  We were tasked with porting our Windows FTP traffic over to UNIX; the nightmare was so painful because of case sensitivity.  Finally, we were let out of the carnage because it was taking too long to compensate for the case sensitivity coming from an environment that didn't care.  The problem is even worse in programming.  I have more important things to worry about than trying to remembert the proper case of a file, variable, class, or function name (C-based languages).  The issues of having to codify non-Latin-alphabet-based languages and other localization issues are no longer a problem (the reason B, Unix, and subsequently C were case sensitive).  MS had a wonderful opportunity to leave this old baggage behind when developing C# - oops!  But they had to for backward compatibility and familiarity expected by would-be Java/C++ converts.  Check out this link: \"http://www.kurumi.com/opinion/case.html\"..."
    author: "Kang"
  - subject: "Re: case"
    date: 2003-02-22
    body: "[quote]I wish KDE could do something about this, but it is really too late. Changing Linux now is totally impractical, and it goes against POSIX and accepted practice anyway. Kinda a shame, really.[/quote]\nYou seem to be in luck! Checkout your Konqueror: in the view menu, there is a Sort option. Guess what, you can set it to case insensitive! "
    author: "Andr\u00e9 Somers"
  - subject: "Re: case"
    date: 2003-02-22
    body: "Surely you jest.  You can still create two files whose names differ only in capitalization and you always have to refer to a file with its correct capitalization or it won't work.  Plus every non-kde program is still totally case-sensitive.  Changing Konqueror's sorting option doesn't change the kernel filesystem api.  If only life were that simple!  KDE can try and hide the case-sensitivity if it wants, but that will only cause more problems while the underlying system is still case-sensitive."
    author: "not me"
  - subject: "Re: case"
    date: 2003-02-22
    body: "You can always have a layer around POSIX cruft anyway. MacOSX is pretty much case insensitive (as is cygwin), and they handle POSIX stuff fine."
    author: "fault"
  - subject: "Re: case"
    date: 2003-02-22
    body: "I totally subscribe what you say. I've suse distribution, and their files are often with \"SuSE\" prefix, or have capitalization in some points to make them easier to read. I've not only to remember their name, but also the capitalization! So bad. The OS have to be \"users goal centric\", not machine code friendly. But since it was programmed in C, it inherits this \"assembler like\" language. If it were programmed in Pascal, it whould have been better ;)\nOk, you can write all in lowercase, but often case mixing makes things be more readable (MyFinancialBudget can be read in a snap, while myfinanciialbudget does not).\nKDE should hide this the most it can, in my opinion.\nOne of the best reading a programmer can do are Alan Cooper's books: www.cooper.com. They will change the way you see software UI and design in general.\nregards\nMarco Menardi"
    author: "Marco Menardi"
  - subject: "Re: case"
    date: 2003-02-22
    body: "> The only reason there are any problems with Windows being case-insensitive is that\n> every other OS isn't.\n\nmac os x is case insensitive as well. not that that means or proves anything, except that this assertion is false. "
    author: "Aaron J. Seigo"
  - subject: "Re: case"
    date: 2003-02-23
    body: "Good for OSX!  How do they reconcile that with their BSD subsystems?  Is the command-line case-insensitive too?  If not, how does the finder handle it when two files are given names that differ only in capitalization?"
    author: "not me"
  - subject: "Re: case"
    date: 2003-02-23
    body: "yes, i believe the command line is case insensitive (don't have an os x box at home to make sure, though). as for the finder, it simply ignores capitalization and gives similar errors that you get on linux when you try and create a file with the same name (including capitalization)\n\ni'm not a big MacOS user, so i'm not long on details. i basically use it when visiting various friends to compare UI concepts, see how people deal with that OS's design decisions and try out those decisions for myself =)"
    author: "Aaron J. Seigo"
  - subject: "In MacOS X"
    date: 2003-02-24
    body: "It depends on the filesystem.\n\nOn an HFS+ (the default) partition, everything is case-insensitive.  You lose some POSIX compliance brownie points and some software written for POSIX doesn't work right.\n\nOn a UFS (BSD-derived) partition, everything is case-sensitive.  Your POSIX apps behave correctly, but some MacOS Classic apps don't run correctly.\n\nCocoa and Carbon apps behave appropriately either way.\n\nFrom a usability perspective case-insensitivity has some advantages but they're pretty marginal in a GUI environment where you have file dialogs.  In defence of case-sensitivity, it seems to encourage people not to use SHOUTY NAMES for files unless they want them to stand out, which is nicer on the eyes.\n\nNot something to get terribly worked up over though, and not something that's easily changed either."
    author: "My Left Foot"
  - subject: "Re: case"
    date: 2003-02-26
    body: "I'll just give my example again which proves your opinion is completely wrong:\n\nA pacifist would definitely not interpret the files Chirac_agrees_with_us.pdf and  Chirac_agrees_with_US.pdf the same way :-)\n\nCQFD"
    author: "oliv"
  - subject: "Re: case"
    date: 2006-01-12
    body: "     Case sensitivity blows! flatout. I'm surprised that they haven't resolved this problem in unix. It seems like an easy fix you try to resolve the old way for backwards compatibility and if it falls you typecast the filesystem and the users input and resolve it that way. it might suffer from collisions but for the most part it would resolve as long as the developers didn't abuse type sensitivity in the first place, which I don't think they commonly do. this could even be built from kde in the format of METADATA. its alot of tables that would need to be built since each file and folder entry would need a key but it doesn't sound that hard.\n\n     ASCII by itself really needs to get dumpt anyway for a unicode system but but no designers want to do that they all love there 8 bit ascii."
    author: "yo!"
  - subject: "Re: User Interface"
    date: 2003-02-23
    body: "Ok, I know a lot of people have commented on this already, so most of what I'm about to say will probably be redundant - but I still want to add my own personal views on this.\n\n- Case sensitive versus case insensitive :\n\t   At the file system level I personally like having stuff be case sensitive. I like naming scripts with an initial capital letter so I always know if I'm launching a binary or a script simply by looking at the first letter. This lets me create scripts that mimic the behaviour of other commands (but slightly different) without having to invent new names - I just name them the same but with a capital first letter.  This is a personal habbit/preference and many people may disagree, but I like it that way. if launching stuff was suddenly case insensitive then I'd have a hell of a time launching some of my scripts.\n\t   I also like the fact that most programs sort file lists case sensitively. I use this when I organize my documents. I usually name my 'main' or 'index' documents with a capital first letter to make them show up in the top of directory listings  - other documents related to the same topic usually are named with the same starting word, but in lower-case as to not show up in the top but still be grouped together lower down in the list.  This way case sensitivity helps me organize the contents of a directory without having to stuff things in subdirs.\n\n- Launching programs by desktop entry names :\n\t   I like the idea that if a program is not found by searching for a binary of the name the user enters that a search for the name specified in the desktop files is then made. But, in my oppinion, if we get to this last resort (and only then) it should be handled like this;  if there is a case sensitive match of the name on the desktop file with what the user entered, then launch the program. If there is only a case insensitive match, then prompt the user if this is the program he/she wants to launch (with an option to never ask again for this specific name). if there is an ambiguity between different programs due to case differences, then the user should be presented with a list of the programs to run (with as much info about the different programs as possible) and be asked to select one of them (again with the option to make this choice permanent).\n\nJust my 0.02euro \n\n\n/Jesper Juhl"
    author: "Jesper Juhl"
  - subject: "Re: User Interface"
    date: 2003-02-23
    body: "Instead of capitalizing the first letter, you could use an underscore before or after the command, or the prefix 'my_', or the letter 'z', or whatever.  There are lots of choices that don't require a case-sensitive filesystem and the other drawbacks that come with it.\n\nAs for the sorting, it seems that there is a genuine need for a \"priority flag\" or something to make files appear first in the directory listing.  That's fine.  Just implement that feature isntead of mangling another feature to get the behavior you want.\n\nOnce again, let me reiterate that I am not advocating a change in Linux.  People are used to it the way it is, and changing it would upset them tremendously.  I am simply saying that case-insensitive is better user-interface design."
    author: "not me"
  - subject: "Re: User Interface"
    date: 2003-02-26
    body: "> Instead of capitalizing the first letter, you could use an underscore before or after the command, or the prefix 'my_', or the letter 'z', or whatever. \n\nyes, I'm well aware that there are plenty of other options. I was simply describing the way that I like to do it. \n\n\n> I am simply saying that case-insensitive is better user-interface design.\n\nI'm not sure I agree. Depends on your target audience."
    author: "Jesper Juhl"
  - subject: "Re: User Interface"
    date: 2003-02-26
    body: "It is better user-interface design, period.  Now, since mistakes have been made in the past, we're stuck with the status quo.  That doesn't mean that the status quo has become good user-interface design in the meantime.  It just means we shouldn't change the behavior of a system people are used to unless the result is an order of magnitude better than the current situation.  Although case-insensitive is better, I wouldn't argue it's quite THAT much better.\n\nIf you were designing a new operating system though, you should go case-insensitive all the way."
    author: "not me"
  - subject: "Re: User Interface"
    date: 2006-07-06
    body: "\nGood day to you sir,\n\nI am AUBREY FLORES, an IT student, I would just like to ask, what's your point of enhancing the status bar of installing a particular user interface. For your comment would be a big help because we are now having our thesis, our final requirement before graduation. I would also like to ask if you can advice us about what's a good user interface on installing a particular process.\n\nThank you sir, we will be expecting for your reply sooner. It would be a big help on our part.\n"
    author: "aubrey flores"
  - subject: "khtml-kjs-webcore mailing list?"
    date: 2003-02-22
    body: "I read on the kfm-devel mailing list once that there's a new mailing list for better cooperation between khtml, kjs and Safari developers. There were also some questions were it actually is but they never got answered afaik. Isn't it moderated and that's why it's so well hidden? I'm just asking because as of late I'm looking at http://www.mozillazine.org/weblogs/hyatt/ for the latest khtml changes which is kind of funny. =P"
    author: "Datschge"
  - subject: "Re: khtml-kjs-webcore mailing list?"
    date: 2003-02-22
    body: "Don't know about the list, but a cvs account was created for Darin Adler @apple.com.\n\nHe was software vp at easel.\n\nNot sure what this all means, but speculation is always fun.\n\nSafari was based on 3.02 iirc, and the apple developers had the advantage of being able to see the development on the kde side. So their tree probably contains most or all the fixes the kde people did, plus what they did. The kde people got a large chunk of code they now are in process of integrating in their tree. All the while the apple boys are continuing to implement fixes. Maybe they will port some of their recent fixes to the kde tree. Would be nice.\n\nIt would be preferable to have one tree, or two closely matching trees each with their respective ui library stuff. And some sort of consensus building mechanism for future directions. But I suspect it will be a while before that happens.\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Re: khtml-kjs-webcore mailing list?"
    date: 2003-02-23
    body: "They are more or less working toward a common three already, yes. Coordination seem to work well as well. Dave Hyatt (who worked on Mozilla and Chimera before) is reporting a lot of fixes as well as merges from khtml to webcore on his weblog and also mentioned that he started working on an inline model for khtml based on the code he did for Mozilla before (actually it seems he's merging some stuff here). Anyway it would be really nice if someone knows and can tell us about the khtml-kjs-webcore mailing list =o)"
    author: "Datschge"
  - subject: "Re: khtml-kjs-webcore mailing list?"
    date: 2003-02-23
    body: "There is a list, but it is a private list - I guess you'd have to prove your committment to khtml development before you can get onto it (haven't asked).  The emails asking about it by others have met with no replies as far as I can see.\n\nWatch kde-cvs and grep for khtml :)"
    author: "Hamish Rodda"
  - subject: "Re: khtml-kjs-webcore mailing list?"
    date: 2003-02-24
    body: "\"There is a list, but it is a private list - I guess you'd have to prove your committment to khtml development before you can get onto it (haven't asked).\"\n\nThat's what I assume as well. Derek Kite, maybe you could ask for an account and then report to the outside world about the current ongoing changes? =o)\n\n\"The emails asking about it by others have met with no replies as far as I can see.\"\n\nThus me asking here obviously. ;)"
    author: "Datschge"
  - subject: "Being for the Benfeit of Mr. Kite"
    date: 2003-02-22
    body: "For the benefit of Mr. Kite\nThere will be a show tonight on trampoline\nThe Hendersons will all be there\nLate of Pablo Fanques Fair - what a scene\n\nOver men and horses, hoops and garters\nLastly through a hogshead of real fire!\nIn this way Mr. K. will challenge the world!\n\nThe celebrated Mr. K.\nPerforms his feat on Saturday at Bishopsgate\nThe Hendersons will dance and sing\nAs Mr. Kite flies through the ring - don't be late\n\nMessrs. K. and H. assure the public\nTheir production will be second to none\nAnd of course Henry The Horse dances the waltz!\n\nThe band begins at ten to six\nWhen Mr. K. perfoms his tricks without a sound\nAnd Mr. H. will demonstrate\nTen somersets he'll undertake on solid ground\n\nHaving been some days in preparation\nA splendid time is guarateed for all\nAnd tonight Mr. Kite is topping the bill\n\n(Lennon/McCartney: Being for the Benefit of Mr. Kite)"
    author: "Zoltan"
  - subject: "Re: Being for the Benfeit of Mr. Kite"
    date: 2004-02-25
    body: "This song rules."
    author: "Mr. K"
  - subject: "Xinerama"
    date: 2003-02-22
    body: "I like to that you for working on xinerama/dual head support.\nI don't had the chance to test it. I can't wait for 3.2, thanks !"
    author: "hal"
  - subject: "thanx"
    date: 2003-02-22
    body: "thanx derek!!!\ni love ur report!\n-gunnar"
    author: "gunnar"
---
<a href="http://members.shaw.ca/dkite/feb212003.html">This week's edition</a> of KDE-CVS-Digest highlights the larger merges of <a href="http://www.apple.com/safari/">Safari</a> code, Xinerama support, 
msword <a href="http://www.koffice.org/filters/">filters</a>, <a href="http://kmail.kde.org/">KMail</a> bugfixes, <a href="http://kopete.kde.org/">Kopete</a> developments, along with continuing work on <a href="http://www.kde.org/areas/multimedia/arts.php">aRts</a> and much more.
<!--break-->
