---
title: "Qt/Mac KDE Call For Help"
date:    2003-08-17
authors:
  - "breed"
slug:    qtmac-kde-call-help
comments:
  - subject: "portability == a good one..."
    date: 2003-08-17
    body: "Didn't some trolltecher started this allredy?\n--See:\nhttp://ranger.befunk.com/blog/archives/000072.html\n--link from dot's site:\nhttp://dot.kde.org/1055852609/\n\nAnyway it's good to have the KDE code free from X11...\n--reminds me of:\nhttp://dot.kde.org/1048476487/1048507639/\n\nI don't mind using X BTW -- just a fan of portability ;-)\n\n"
    author: "cies"
  - subject: "Re: portability == a good one..."
    date: 2003-08-17
    body: "Yeah, Sam Magnuson did the original port, but it went through a ton of bitrot.  He said we're better off basically starting over, so that's what I'm doing (using his old patches as a reference when I need to)."
    author: "Benjamin Reed"
  - subject: "license nitpicks..."
    date: 2003-08-17
    body: "Note that all of KDE can't be (legally) compiled on Qt/Mac since it's only GPL'ed.  There are a number of places in KDE where the Artistic license is used (Klipper, KSirc, some Notaun plugins).  At least it's something to keep an eye out for.  There are parts of KDE (most likely including things I'm not mentioning) that aren't GPL compatible."
    author: "Scott Wheeler"
  - subject: "Re: license nitpicks..."
    date: 2003-08-17
    body: "I thought Matthias Ettrich was proposing to drop QPL completely for all the releases."
    author: "ac"
  - subject: "Re: license nitpicks..."
    date: 2003-08-17
    body: "afaik the artitic license give you the permission to relicense as GPL. As the GPl is a further restricted subset of the Artistic license that gives you every freedom you want. AFAIK you can also sell a product under the artistic license, incorporate it into your prorpietary work. "
    author: "Gerd"
  - subject: "Re: license nitpicks..."
    date: 2003-08-18
    body: "See:\n\nhttp://www.fsf.org/licenses/license-list.html#NonFreeSoftwareLicense\n\nThe Artistic license is not GPL compatible; any license change (relicensing) requires the consent of all copyright holders."
    author: "Scott Wheeler"
  - subject: "Re: license nitpicks..."
    date: 2003-08-17
    body: "I don't get it, Qt/X11 is also GPL'd; are you saying it's illegal for me to compile KDE with that too?"
    author: "LMCBoy"
  - subject: "Re: license nitpicks..."
    date: 2003-08-17
    body: "Qt/X11 is not GPL. It's dual-licensed under GPL and QPL, the latter of which permits additional open-source licenses to be used.\n\n"
    author: "Sad Eagle"
  - subject: "Re: license nitpicks..."
    date: 2003-08-17
    body: "Well, as long as we're picking nits....\n\nWhen I configure Qt, it asks me to agree with one (1) of the licenses, GPL or QPL.  You're saying if I choose GPL, then there are parts of KDE that I can't compile.  Can you please point me to the list of KDE components that I should avoid?\n"
    author: "LMCBoy"
  - subject: "Re: license nitpicks..."
    date: 2003-08-17
    body: "The prevelant interpretation is that you're allowed to use both terms, using whichever one is appropriate per-application. \n"
    author: "Sad Eagle"
  - subject: "Re: license nitpicks..."
    date: 2003-08-18
    body: "Hi Scott,\n\nActually Trolltech GPL'd Qt/Mac at WWDC2003, \nhttp://www.trolltech.com/newsroom/announcements/00000131.html\n\nYou can get the Qt/Mac Free Edition (GPL) here,\nhttp://www.trolltech.com/download/qt/mac.html\n\nDon.\n"
    author: "Don"
  - subject: "Re: license nitpicks..."
    date: 2003-08-18
    body: "D'oh sorry should have read Scott's comment more carefully. Guess I'll keep out of this Artistic license and GPL license debate.\n\nDon.\n"
    author: "Don"
  - subject: "Why?"
    date: 2003-08-17
    body: "KDE on Mac?? Why?\nUNIX OSX already has the best desktop ever!\nThey also have the Safari KHTML/KJS (I beleive the best part of KDE project already ported, also btw freed from QT/GPL).\n\nThere is lack of apps for linux and most are buggy. But this is not the situation on UNIX Mac, there are good commercial apps that really work. I suggest that you guys focus on making linux better, Apple will take good care for the UNIX."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "because they can; because there are KDE apps that are quite good (despite what you so condescendingly propose); because people who use KDE on other UNIX(-like) OSes might like some of the apps they are used to; because you can never have enough apps on a platform... etc...\n\ncuriosity begs me to ask: before Apple picked up KHTML/KJS, did you think it was the best part of KDE? or is it because Apple \"blessed\" it with their participation (which is very welcome, i might add) that you feel this way? if/when Apple picks up another bit of KDE technology, will you then feel that this too is another best part of KDE?\n\nyou know, Apple relies heavily on Free / Open Source software these days. that means that the quality platform you appreciate so much depends on that same community of which Linux is a part of. something to think about, anyways..."
    author: "Aaron J. Seigo"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Well Apple is now the biggest UNIX vendor, and more than that OSX is the best desktop that runs on UNIX. I think their choice is right when picking the best part of kde.\nAs you are saying that they rely on Open Source, this is Right. But they are doing it in a very smart way. They build proprietary solutions on top of NON-GPL opensource technologies.\nAlso in the KHTML/KJS case they help the community to get free from QT/GPL as their way to say thanks.\n\nLinux desktop (KDE) is far from the UNIX desktop (OSX), they have commercial apps like Photoshop, MSOFFICE, Corel, Macromedia Flash, etc, etc, which kde will never have. They have their own grown apps, with a commercial software quality, and also of course they collaborate with the community to build a proprietary technology on top of opensource ones, while returning the changes back to the community and keeping their upgrades home.\nKde could improve the situation on Linux desktops before mass porting to a supperior UNIX platform. There are two dimensions of improvement:\n1) quality of the software - for example koffice has too much to improve (why not the next technilogy Apple could get to be exactly koffice)\n2) polotical situation - I don't know any commercial software built for KDE, and there is reason for it - QT/GPL (the gnome team has advantage here - they use GTK/LGPL)"
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> 2) polotical situation - I don't know any commercial software built for KDE, and there is reason for it - QT/GPL (the gnome team has advantage here - they use GTK/LGPL)\n\nThere is no reason for not having a commercial software for KDE because of KDE. There may be a QT/GPL reason and to disprove you: Hancom Office, Kylix, Opera, ..."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: ">There may be a QT/GPL reason and to disprove you: Hancom Office, Kylix, Opera, ...\n\nThis apps are not for KDE, I am not a lawyer to investigate why companies stay away from KDE, but definitely they do and they do not build commercial closed source solutions for KDE, and definitely it's because of political/licensing situation.\n\nOtherwise KDE offers the best environment for Linux, the direction kde follows is definitely the quality but also commercial market should be opened to meet the corporate and end stupid user needs."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Frankly speaking - I don't need all the commercial software you rave about. I have been using Linux for five years (yes, on the desktop) and am perfectly happy with it. Also the KDE environoment has a better usability for _me_ than what OSX has to offer. The latter gets in my way with its teletuby-interface geared towards the computer-illiterate."
    author: "Tuxo"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "A lawyer will not be able to present a reason for this, stop your FUD!"
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> This apps are not for KDE, I am not a lawyer to investigate why companies stay away from KDE, but definitely they do and they do not build commercial closed source solutions for KDE, and definitely it's because of political/licensing situation.\n \nHonestly, folks. The guy has a point.\n\nKDE as a desktop is far superior to anything else that exists for free platforms. In addition, it's also the most used desktop on free platforms. Therefore it's quite suprising how little commercially-breeded applications exist for it. IANAL either to tell what the precise reason for the companies to stay away from it are, but obviously something is wrong here.\n\nThis, as I see it, is the biggest obstacle in success of KDE. Without this well-hidden problem, be it real or imaginary (marketing problem), I dont think anyone would be talking about GNOME anymore (actually, if RH would default otherwise GNOME would disappear pretty fast)."
    author: "jmk"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "There is a great deal of Qt/GTK commercial software, but very little GNOME/KDE based.\n\nI don't see a great deal of commercial software for GNOME either. There is the software put out by Ximian, but that company is made of GNOME founders/hackers. theKompany also used to write KDE-based software before seeing the market conditions of commercial software on Linux- there were too many free (as in beer) alternatives and too small of a market. So, eventually, they started making cross platform software (i.e, pure Qt based)\n\n"
    author: "fault"
  - subject: "Re: Why?"
    date: 2003-08-18
    body: "You don't have to be a lawyer.\n\nIt's not the licensing. Most commercial software packages are built on components they have purchased from others.\n\nIt's a similar situation as proprietary modules in the linux kernel. They can work, and are only used when no reasonable alternative exists. If someone release a proprietary and closed application for Kde 3.*, would it run on 4? Would they still be around to support a new version? I've been bitten so many times by proprietary software that I wouldn't spend my money on it. Especially when there are good free and open alternatives.\n\nQT is different. Kylix is bound to the version they use. Pretty hard to do that with kde libs.\n\nDerek "
    author: "Derek Kite"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> Well Apple is now the biggest UNIX vendor\n\nMessured by what? My guess would have been either Sun or IBM.\n\n> the gnome team has advantage here - they use GTK/LGPL\n\nActually this advantage seems to be irrelevant. You can find several companies doing closed source software with Qt but you have to search quite hard to find any company using GTK for closed source development besides Ximian.\n"
    author: "Kevin Krammer"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Kevin is correct. There are almost no commercial developers using KDE/Gnome to do application developement for linux.  They all use QT/GTK.  But there is litterally dozens more QT based commercial applications then there are GTK based commercial applications.\n\nIn fact the only KDE/Gnome commercial applications that I know about are from the Kompany wich has two KDE applications.\n\nThe point is that GTK's LGPL status does not seem to be a significant advantage for its acceptance as a development enviroment.  Because you can gat a pure commercial liecense for QT it is a huge advantage for commercial software developers.  \n\nStrid..."
    author: "Strider"
  - subject: "Re: Why?"
    date: 2003-08-18
    body: "> In fact the only KDE/Gnome commercial applications that I know about are from the Kompany wich has two KDE applications.\n\nDon't forget Ximian's connector-- it's the only GNOME commercial app I can think of."
    author: "fault"
  - subject: "Re: Why?"
    date: 2003-08-18
    body: "Thats right.  Connector is a gnome commercial application."
    author: "Strider"
  - subject: "Re: Why?"
    date: 2003-08-19
    body: "And all it's doing is scraping the screen of Exchange's webmail.  Can't we do the same using the same architecture that YahooPops from sourceforge.net does?  It wouldn't be too difficult, as long as you have access to the Webmail portion of exchange.\n\nIt'd be great if a kio could be created that just screen scrapes webmail: hotmail, yahoo, exchange, blah blah.\n\n\nAnyone up for the challenge?"
    author: "J"
  - subject: "Re: Why?"
    date: 2003-08-18
    body: ">  Linux desktop (KDE) is far from the UNIX desktop (OSX), they have commercial apps like Photoshop, MSOFFICE, Corel, Macromedia Flash, etc, etc, which kde will never have.\n\nNever is a long time. Besides, I guessing you never ran OS/2 or you'd understand better the sheer numbers it takes for commercial software houses to support a platform. (The license BS is a red herring.) There is a lot more that could be said about your list here but I'll just say that I've had a lot of inquiries about running Quanta on OS/X. It seems there is nothing for it that a number of developers think is as good. Also please note we typically run less than 5-10 open bugs at any given time so your remarks about quality are also off base. \n\nIf you want to evangelize OS/X so much what are you doing on a KDE site? If you want to critisize what someone is doing remember that they're doing what they choose, you didn't pay them and... you're critisizing them. Don't want it? Don't use it! Others do want it."
    author: "Eric Laffoon"
  - subject: "Re: Why?"
    date: 2004-02-20
    body: "Can't we all just get along? I hate to see a good discussion turn into a rampage. Is it so wrong to like KDE and Mac OS X without comparing them. In fact it's not right to compare them, Macs have almost always been wedged between commercial software and the consumer, and Linux is more for the people by the people. Many times without pay. I'm sure when KDE gets used a little more often Corel, Adobe, Apple, and maybe even MS wouldn't mind sending their applications that way. As long as there is money in it."
    author: "Anthony Iverson"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "I tried to use Gnome but it isn't usable as KDE. KDE desktop is really good for usability. I was a windooze user for a too long time but 1 year linux and I don't think that windooze is more useable than linux thanks to the great KDE desktop. It's easier to use than the windooze shit.\n\nI never used OS X because I don't have the money ;-). Is it right that OS X is based on BSD? I think it is very smart of Apple to support the Open Source community. So they get more customers ;-). If you need professionell software for work, you've got the money to buy it (I guess, or are using P2P) but if you are student you are happy that open source exists.\n\nI agree that commercial products like Photoshop, 3DStudioMax, etc. are better than every OpenSourve product out there. Because you have to pay a lot of money for a Mac you will have the money to buy other software for it, there is no reason to port KDE, I think. Focus on 86 systems please. When KDE is getting really good (I think it is, and really stable, good work!) maybe some commercial products will be developed.\n\n(There are some bugs out there, thats right, but the software is free and open source, make it better yourself!)"
    author: "Hakan"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: ">>Is it right that OS X is based on BSD?<<\n\nIt is using a Mach-based kernel, but they took libraries, utilities etc from the BSDs."
    author: "Tim Jansen"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "That's one opinion.  Personally, I feel just the opposite, and prefer to run linux INSTEAD of Mac OS X."
    author: "Lee"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "-On mac you can run office, photoshop, flash, corel, etc which are build for mac.\n-Mac is UNIX (not linux), you have console, apache, perl, php, java and anything else one UNIX has\n-Mac has unique best user interface. Apple are pioneers in UI guidelines. Everybody imitates them - XP, KDE.\n-Apple builds quality apps that are unique for their OS and one cannot dream to run on another platform\n\nIn a mac platform you will not miss any UNIX tool, and you will not miss any commercail app. Also you will have the comfort of running on the best machine and UI. I can't guess for anything that linux user will have and mac user will not (well may be only klipper ;)"
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Because I prefer an openoffice, a gnu gimp, W3C SVG, sodipodi, etc.  I care about future guarantees of freedom, and the freedom of others who might not be able to access proprietary software through poverty, or some other obstacle.\n\nI don't like the Mac OS X user interface.  Sure, it's cute, but it's not terribly usable.  In fact, it's slow and awkward for my kind of multitasking workflow.\n\nI'm not convinced about the quality.  A lot of OS X is based on Free Software anyway.  It's taken major revisions like Jaguar to make Mac OS X even capable of some things, and we've had to pay for them.  Debian gets updates on a daily basis, and I'm sure they do it because they care, rather than because of the money.  Furthermore, updates are backwards compatible on Linux.  On Mac OS X, as soon as some new revision comes out, everything else is declared obsolete, in order to push new sales, and all the new stuff won't work on it.  Then there is stuff like OS X's command line tools not even working with their filesystem, which just sucks.\n\nNope, I'm sticking with Linux."
    author: "Lee"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "OpenOffice.org is OK, but GIMP is unserious compared to Photoshop, and there is no competiton to Macromedia Studio MX (and mostly Flash MX).\nAs for the OSX UNIX UI if you think a little and do some calculations you will find that their UI saves a lot of screen space, meaning it's more productive. Also Mac is not only for mouseclick users, if you know the shortcuts you can be fast.\n\nAbout not working command line tools, I am sorry to hear that, it is really terrible did you report bug?\n\nBtw, it's OK to port konqueror to Mac. Since apple already did the first half of the job porting KHTML/KJS you only have to port the filemanager. I will love to use konqueror on the mac.\nPlease post a message to this board when it's ready and I will download it to use it. But if you don't do it within a year I will write one for myself."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Well, on the comparison of various apps, that's all a matter of opinion.  Some people think Macromedia can't even write a webpage properly, for example.\n\nIt DEFINITELY does not save space.  That's ridiculous.  Perhaps compared to whatever default config you had last time, but in terms of how much you can put on the screen when you really try, and still use it, KDE, GNOME, and probably windows beat OS X easily.\n\nOn the command line tools: yes, it's REALLY terrible.  And no, I haven't reported the bugs, because a) I read about the problem long after I switched away from OS X, and b) It's a gaping hole of unimplemented features for an entire filesystem.  They KNOW it's not implemented, just as they knew that OS 9 wasn't fault tolerant.\n\nDon't get me wrong.  I think OS X is nice.  I'm sure lots of people will love it -- especially newbies who want something nice and easy.  I just don't find it viable for my own needs.\n\nBTW, I'm not a KDE developer, so I'll refuse to keep you updated on porting, if you don't mind ;D"
    author: "Lee"
  - subject: "Re: Why?"
    date: 2003-08-23
    body: "I won't get into the macromedia good/bad thing. \n\nHow does it \"DEFINITELY does not save space\"? the *ONLY* thing on a mac screen that has to be there is the menu bar, and thats quite small. name *ONE* real reason that it \"DEFINITELY does not save space\" and I will bow down to you.\n\nWhat exactly on the command line are you whining about? the only thing I can think of that doesn't work completely is ln for hardlinks. Should they not include ln? What exactly isn't implimented?\n\nI'm not trying to flame (not doing to well, am I...), I'm just trying to see exactly what you are talking about? Generalized ramblings against (or for, for that matter) don't help either side. The accusor seems stupid, and the accussee doesn't listen. pls clearify."
    author: "GaelicWizard"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> Since apple already did the first half of the job porting KHTML/KJS you only have to port the filemanager.\n\nI think you should spend your precious time for something else instead keeping posting here. The above quote makes it apparent that don't have the slighest idea about what you are talking about."
    author: "Datschge"
  - subject: "Re: Why?"
    date: 2003-08-18
    body: "Actually I do professional web development and I have NEVER used flash and will not be using it. You lose far too many customers with it, use more bandwidth, lower your rankings in search engines because the data does not index anymore and overall it is an unusable mess.  Long term I will probably be using svg once support for it matures which is getting better in kde and mozilla. At least svg will index, can be controlled with css and transfers well \n\nAlso I seriously doubt that macosx could be as productive as my linux 3 monitor setup with kde and 10 virtual desktops. My system is very customized to the way I work and it is highly productve.Also last I heard macosx does not have an equiv for io slaves and there is no way I could consider using a system without that anymore since it saves such a huge ammount of time. Being able to sftp, webdavs, http, https, smb, nfs, imap etc from any kde app is a huge gain."
    author: "kosh"
  - subject: "Re: Why?"
    date: 2003-08-23
    body: "you are correct, there are no io slaves on MacOSX. all the functions you speak of are directly integrated into the filesystem browser in the open/save dialogs."
    author: "GaelicWizard"
  - subject: "Re: Why?"
    date: 2003-08-24
    body: "So how would you use sftp, imap etc from any macosx app? When I last looked at it for webdav you had to mount it as a kind of virtual drive and io slaves are very very different from that. So how would you for example have your wordprocessor open up an html page? In kword I can just type in http://slashdot.org. Being able to do that from any app without doing some special mounting stuff saves a lot of time. How about if you got a file about what you are working on coding so you use your editor to do an imaps connection to the server and pull the file directly into your editor?"
    author: "kosh"
  - subject: "Re: Why?"
    date: 2003-08-29
    body: "Obviously there are a number of features still missing from MacOSX, for example there is no sftp, only standard ftp. and imap is only accessable from within mail.app (or other email sw). mounts are handled automagically when you put in a URL (beit http or aft or smb or whatever) and they unmount when nolonger used, afaik. In the same way that you can't access a disk w/o mounting it, i'm not sure how you would really get anything over a network w/o mounting it in some fashion. this also makes sure that an app that has no idea whats going on, maybe not even a native app, can still access the information. I'm not saying that MacOSX can do it all, just that its not totally lacking. :-)\n\nJP"
    author: "John"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> I can't guess for anything that linux user will have and mac user will not\n\nA free (in both senses) operating system and desktop."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "you don't have the freedom to close your source when using kde (qt/gpl)\nyou have that freedom with gnome (gkt/lgpl)\nyou have that freedom with windows and mac"
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "And that improves the position of MacOS X in the two mentioned points how?\nSince you have obviously no knowledge what \"free (in both senses)\" means: \"Free\" as in free speech and in free beer."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "third sense of \"free\":\n- free commercial market"
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Summary: MacOS X desktop is two third not free."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "You aren't serious I suppose. Or are you just trolling?\nYou don't have the freedom to hijack an airplane in Europe.\nIs the Union not free because of this?  Surely not!\nYou never have the freedom to restrict the freedom of others\nin any sane country. The GPL was created to prevent hijacking\nfree software by commercial projects. And this it does.\nIt's the _advantage_ of KDE. You don't like - don't use it. \n\n\n\n"
    author: "Jan"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Everybody has the freedom to not open what he creates.\nI will need a better filemanager for mac (better than finder i mean), and konqeuror filemanager is good enough.\n\nIf kde does not port it within a year I will create one myself and will eventually give you to use it too, but i have the right to keep the source for me."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> Everybody has the freedom to not open what he creates.\n\nYou have that freedom in KDE as well. All you have to do is pay for Qt developers license.\nIf you're gonna charge for your software, you might as well pay, right?\n\nThe reason why companies develop for Qt and not for KDE is that they like their software to be cross-platform. KDE is still not available for Windows, but <A HREF=\"http://kde-cygwin.sourceforge.net/\">that might change as well</A>!"
    author: "Anonymous"
  - subject: "Geeze..."
    date: 2003-08-17
    body: "Translations for those who have no clue:\n\n> you don't have the freedom to close your source when using kde (qt/gpl)\n\nAs long as everything you write is completely your own without relying on others work you are free to choose the license yourself. As soon as you are relying on or taking stuff from others you have to respect their rules. If you rely on works of others which is available under GPL license you are free to either use GPL as well or ask every single person involved whether you can use their code under another license.\n\n> you have that freedom with gnome (gkt/lgpl)\n\nNot really, this only affects linking libraries, you are still not able to take over any code. Considering that all KDE core libraries are LGPL as well it boils down to companies wanting to use an existing working framework for their commercial products and earning money with it while being either too greedy to give the source back to the community which made the framework possible, or too greedy to paying the company which made the foundation of the framework possible. I don't have merci with greedy people so I don't see any problem with this \"issue\".\n\n> you have that freedom with windows and mac\n\nAs soon as you use Windows and Mac (and any of the supposedly free included programs) the companies responsible for developping them were already paid for you being able to do so. Neither Windows nor Mac is available for free. So no, you don't have the freedom to start with. As soon as you are at a point where you think you have the freedom you already moved a lot of money. Are you too greedy to spend a little part of that for open source instead?"
    author: "Datschge"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Why do you need to have the \"freedom\" to close your work when you build it on top of others?\n\nAnd who says you have not. Just keep whatever you create for yourself and it's fine.\n\nYou make it look like GPL would disallow privacy. That is untrue.\n\nAnd what is your motivation anyway. Are you scared that your \"best desktop OS\" could get concurrency? If so, you are right.\n\nLike GNU won over when it didn't have a kernel, KDE can as well. Just a new user group.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: ">  office, photoshop, flash, corel, etc which are build for mac.\n\nhello, as a poor student... I don't have the money to buy and upgrade every few years office, photoshop, and corel..\n\nIf you are willing to buy me a copy, please reply for a email address :)"
    author: "fault"
  - subject: "Re: Why?"
    date: 2003-08-18
    body: "> -On mac you can run office, photoshop, flash, corel, etc which are build for mac.\n\nAnd while the Mac used to lead on these they have fallen behind. in fact Office is being critisized for not keeping up and even having compatibility issues with windoze. Which one is more popular? The Mac has only taken a small portiion of the market. In fact pundits are currently predicting Linux will exceed it shortly. It's true you have more commerical apps, but if that's your argument you may have to find a new one in a year or two. BTW if you're into movie production you pretty much end up running Linux because in that area it already has taken the lead in commerical apps.\n\n> -Mac is UNIX (not linux), you have console, apache, perl, php, java and anything else one UNIX has\n\nSo what!? Are you telling me the average user with a touchy feely interface is going to notice the small differences in a few Unix utilities? You're naming off apps that are natively developed on Linux by most of their development teams.\n\n> -Mac has unique best user interface. Apple are pioneers in UI guidelines. Everybody imitates them - XP, KDE.\n\nOh make me laugh!!! You must be kidding! Apple bought the Xerox Parc interface and copied it with very little innovation even using the Xerox terminology like \"mouse\". If you want to gush then tell me how wonderful the folks at Xerox were. As for the UI my personal experience was that it was really bad if you didn't know it... Tell me that dragging and dropping a floppy on the trash to eject it is intuitive. To me it was terrifying because it looks like it would erase it and stupid because it was a sycophantic adherance to a visual UI that went over the top and wasn't productive.\n\nBTW while we're at it let's not forget the reason windoze has a double click program actuation causing people carpal tunnel syndrome is that Gates and company did such an unimaginitve job copying the one button mouse. It wasn't until they studied the OS/2 interface that they discovered a use for the second button on the right. If Apple were truly visionary they would not have copied this so precisely from Xerox.\n\n> -Apple builds quality apps that are unique for their OS and one cannot dream to run on another platform\n \nSorry. I can't help but think of where they have gone. Not just apps, but an OS. Do you know why they went to OS/X? Because their OS versions going into 8 and 9 were so bad that users would go check with other users to get consensus if a new OS version would even run. Never run a x.0 version was a standard rule. In fact recent versions didn't have any multi tasking at all!!! I remember using one in Kinkos to scan and asking the staff Why I couldn't do anything else while it was scanning or sending.\n\nNow I understand that Apple has done a pretty good job with OS/X and it's something one can feel good about recommending to grandma. However your glowing ad copy here is little more than irradiated fertilizer. It would also be worth keeping things in perspective. I noted with OS/2 that all the new stuff was coming from the Linux camp. Apple couldn't pull off a decent OS... BSD to the rescue. (M$ uses their TCP/IP stack for the same reason.) Apple didn't have a decent browser and in comes KHTML. The office suite could be next. I happen to like Apple and how they are working with OSS now. I'm glad to be partnering with them... but let's not get confused about the value of OSS here."
    author: "Eric Laffoon"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> UNIX OSX already has the best desktop ever!\n\nYou don't expect sympathy for such statements on a site such this, or?\n\n> There is lack of apps for linux and most are buggy.\n\nNot talking about apps like Safari which delete your complete home folder when you download a file...\n\n> But this is not the situation on UNIX Mac, there are good commercial apps that really work.\n\nSame is true for Linux.\n\n> I suggest that you guys focus on making linux better\n\nYou suggest what already happens? Sensational idea."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Well KDE does good stuffs example is KHTML/KJS.\nIf you are not happy with something on mac report them the bug.\n\nCan you guess one commercial app for KDE?"
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> If you are not happy with something on mac report them the bug.\n \nAnd they will fix it? Ok, I just reported them that I can't get their desktop free of charge and I'm lacking the sources to be able to adapt it to my wishes.\n\n> Can you guess one commercial app for KDE\n\nSure, visit http://www.thekompany.com"
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> And they will fix it? Ok, I just reported them that I can't get their desktop free\n> of charge and I'm lacking the sources to be able to adapt it to my wishes.\n\nWell it's ridiculous to ask a closed source app vendor for the source. But on the free market you can always choose another vendor and buy from it, this is how the free market works, the best wins.\n\n> Sure, visit http://www.thekompany.com\n\nThese are QT apps not KDE. If you were reading the dot often you would read what the president of the kompany said about it, and why the kopmany will never write again software for KDE.\nSo you are pointing to wrong opposite example."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> Well it's ridiculous to ask a closed source app vendor for the source. But on the free market you can always choose another vendor and buy from it, this is how the free market works, the best wins.\n\nYour postings here are ridiculous. Your free market is also possible and happening on Linux. Let's talk again when Linux has got a higher desktop market share than Apple (prognoses differ from this year to in two years). Then more commercial companies will offer Linux applications like Corel, Softimage, Alias, Main Concept, Sun, Hancom, Applixware, Borland, IBM, Metroworks, Rational, Samsung, Novell, Lotus and others already do.\n\n> These are QT apps not KDE. \n\nKapital? KDE Studio Gold? Do you want to now decline that they exist?"
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "There should be a reason for that you don't like Macs.\nMay be you have bad experience and/or some bad nightmare with 8 bit Apple II.\n\nI had happy experience with Apple II and I am happy that Apple is still on the top in terms of quality of their hardware and software."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> There should be a reason for that you don't like Macs.\n\nOnly Mac zealots like you. I never used Macs."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "then dont post about something u dont know about!!!!!!!!!"
    author: ":-)"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "What was wrong what I said about MacOS? I didn't visit a Mac news site and post wrong stuff for cheer-leading Linux/KDE like Anton did with visiting this site and posting about Linux, KDE and OpenSource for cheer-leading Apple."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Hey, you are taking me wrong. I am not against kde. I like it and I use it when I boot to linux.\nAlso when i have to show someone what is that misterious thing called \"Linux\" i show them kde, and they like it.\n\nWhat I do really want to see is that kde is more improved for example if koffice gets more improved Apple could decide to port it to Mac and then you don't have to port it yourself. See? Apple is kicking Microsoft from their platform with the help of the community, they will do the job for porting the really good stuffs.\nAnd of course I will love to run koffice on mac but when it's mature enough. And you should agree that if it's mature enough as khtml was apple will port it."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "o... ok... i just \"assumed\" :-)\n\nignorance is bliss...\n\nbut still, i prefer freedom for businesses to close the source if they themselves improve it... i mean it should be their choice... ya know?  just give credit where credit's due... if credit's due to the open source community... cause developers got feed their children too...\n\nplus, macs arent that bad... u should try em out... u might enjoy them... i mean, they may have been slow at certain tasks for older machines, especially running 500mhz... but, not anymore (thank god for panther)... try em out after panthers released, u may be surprised... no, im not kidding... seriously take a look at it...\n\ntell me your misconceptions and i may help you to clear them :-DDDD"
    author: ":-)"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "I'd like to make you aware of the fact that you are here on a KDE site. Nobody has to tell you anything about Macs here. Rather I'd like to hear from you for what reason you decided to spam this forum with your unfounded assumptions."
    author: "Datschge"
  - subject: "Re: Why?"
    date: 2003-08-18
    body: "Reason? Because for 1/3 of the money I can get an equivalent powerful machine. And for that reason the only time I've used them is when a friend had one. I tried them, found the interface counterintuitive. And no, I don't use Windows. I disliked the precision movements of the mouse which are necessary to, for example, scroll. I don't like moving my arm as much as is required by the Mac. And, up till OSX, the stupid things crashed even more than MS stuff. They just made it look pretty.\n\nMy first experience with programming was with pascal on an apple 2e. Command line. Been hooked ever since, not by apple, but by programming.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "And you are a troll."
    author: "CE"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "There should be a reason for that you don't like Macs.\nMay be you have bad experience and/or some bad nightmare with 8 bit Apple II.\n\nI had happy experience with Apple II and I am happy that Apple is still on the top in terms of quality of their hardware and software."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Indeed I like Macs."
    author: "CE"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "i really wonder why you visit the dot, because its mainly about kde and directly about your beloved mac os?"
    author: "derelm"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "To try to prevent that KDE will be natively available for MacOS?"
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "You are using KDE and Linux interchangeably.  But yes, there are commercial KDE apps."
    author: "Lee"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> Can you guess one commercial app for KDE?\n\nThe KDE Kolab Client is a commercial app for KDE. I also suggest to check out the products sections of www.klaralvdalens-datakonsult.se and www.thekompany.com. Hancom Office comes to mind, too. Most of those applications are even _proprietary_ (which is what I guess you really meant instead of \"commercial\").\n"
    author: "Marc Mutz"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "What will Apple do *for* UNIX? Huh...\n\nIf you don't like Linux/KDE then why are you trolling here?\n"
    author: "Chakie"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "Well, I like it of course, KDE is good for my Linux.\n\nWhen I want to demonstrate some winuser the misterious \"Linux\" i show them KDE. And they like Konqueror, XMMS, OpenOffice, MPlayer, Kate, Korganizer, Klipper and the virtual desktops.\n\nI feel comfortable under KDE, Win and Mac.\nBut for linux I miss some apps that i have to run wine or vmware sometimes.\nFor windows I must install cygwin, X server and putty to be in touch with the UNIX.\nAnd for mac I don't need to do anything special, i have the terminal, i have the apps i have the desktop.\n\nAnyway in all cases if one wants can make any environment comfortable for him, it just varies how much efforts you must spent for acheiving it. Linux makes it most difficult because too often you must compile, and also most successfull commercal apps are not available.\n\nI beleive that UnitedLinux consortium can help because once standarts are accepted binary distribution will be easier, and may be the most popular commercial apps will appear too."
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> But for linux I miss some apps that i have to run wine or vmware sometimes.\n\nNo reason to discourage an native port of KDE to Mac.\n\n> Linux makes it most difficult because too often you must compile\n\nYou don't know comprehensive distributions like Debian, Mandrake or SuSE.\n\n> I beleive that UnitedLinux consortium can help because once standarts are accepted binary distribution will be easier, and may be the most popular commercial apps will appear too.\n\nThis sentence doesn't make sense with \"UnitedLinux\" (*one* standard?), try with \"LSB\".\n "
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> No reason to discourage an native port of KDE to Mac.\n\nIt will not be a native port, since QT is some kind of emulation and abstraction layer over the underlaying Aqua and Quartz. Also btw there already is Linux PPC (and KDE on it).\nReal Native port would be nice - as Apple did with KHTML/KJS. And if some other part (for example koffice) of KDE gets enough improved Apple will do this job, more than that they will clean it from QT/GPL as they did with safari.\n\n\n> You don't know comprehensive distributions like Debian, Mandrake or SuSE.\n\nFor example I have compiled more than 5 times KXMLEditor, I use SuSE when i boot to linux, and i haven't find a binary package last time i searched.\n\n\n> This sentence doesn't make sense with \"UnitedLinux\" (*one* standard?), try with \"LSB\".\n\nread here please, and find why companies would (and more and more already are) support it, united linux also uses LSB, but UL has the chance that can became de facto a standart linux distribution for commercial support and apps\nhttp://www.unitedlinux.com/"
    author: "Anton Velev"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> And if some other part (for example koffice) of KDE gets enough improved Apple will do this job, more than that they will clean it from QT/GPL as they did with safari.\n \nThis will not happen (and therefore you can't measure the quality of any KDE part if Apple picks it up or not) because khtml is only loosely coupled with Qt (QT is short for QuickTime!) while other apps/parts of KDE are not.\n\n>  For example I have compiled more than 5 times KXMLEditor, I use SuSE when i boot to linux\n\nYour fault, it's included in SuSE: ftp.suse.com/pub/suse/i386/8.2/suse/i586/kxmleditor-0.8.1-58.i586.rpm\n\n> but UL has the chance that can became de facto a standart linux distribution for commercial support and apps\n\nYou seem to be a true believer in marketing sayings (of Apple and UL), it's not to be expected atm that RedHat will vanish."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: "> It will not be a native port, since QT is some kind of emulation and abstraction layer over the underlaying Aqua and Quartz. \n\nUh, Qt/Mac uses the appearance manager, and thus, is as \"native\" as something like Carbon or Cocoa is.\n\n> And if some other part (for example koffice) of KDE gets enough improved Apple will do this job\n\nI think it's good for KDE overall. Having more users (and testers), will give new developers. Apple using khtml has done wonders for it's development. If someone were to compile something like koffice or konqueror and post it on versiontracker.com (in the future), I'm quite positive a lot of people would use it. \n \n"
    author: "fault"
  - subject: "Re: Why?"
    date: 2003-08-17
    body: ">  UNIX OSX already has the best desktop ever!\n\nUhm, that's certainly quite debatable.. I currently have a PC running Linux and KDE, a Dell laptop running WindowsXP, a ibook 300 running classic MacOS (too slow to run OSX), and a dual g4 running OSX/Panther (actually my dad's, but I've used it often)\n\nOf these, I like the Linux/KDE machine best, then the ibook running MacOS9, then the XP box, followed by the OSX box. "
    author: "fault"
  - subject: "I am curious"
    date: 2003-08-17
    body: "How do you explain the movement of Hollywood away from Windows/Macs/SGIs to Linux Boxes in all aspects?\nWell they make perfect sense to move to for rendering boxes (hey you could use Dos, if you do not mind down time), hollywood is also moving the artists desktop to Linux. Some are using Gnome, others using KDE, and I have seen icebox being used (it is actually the artists preference).\nIn addition, they are busy supporting the movement of their high end commercial software to Linux away from all the other platforms. Yes, I know, Apple is trying to stop that by buying up some critical peices, but my understanding is that several studios have internal efforts underway to now redo those items in a GPL fashion.\nand the answer is......?\nBTW, I do happen to like the apple desktop, but I prefer KDE due to it being GPL AND its look and feel."
    author: "a.c."
  - subject: "More choice is GOOD"
    date: 2003-08-18
    body: "Sure, OSX is a nice UNIX, but there's a bunch of nits to pick with their bundled apps (OSX Mail and Safari both have many issues that KMail and Konqueror got over a *long* time ago). And KDE offers a lot of really nice stuff that OSX *may* get around to providing *some* day, like kioslaves, kmail and konqueror :)\n\nThere's simply not a comparable mailer for OSX like kmail. Or konsole, kopete, ... (some that come close, but are far too buggy or unfriendly)\n\nIt'll be so nice to be able to develop apps using PyQt / PyKDE too :)"
    author: "Richard Jones"
  - subject: "Re: More choice is GOOD"
    date: 2003-08-18
    body: "Yes, I very much agree with the parent post.\n\nI use terminal.app everyday and I really miss Konsole's ability to have multiple terminals within a single window.\n\nAlso I prefer KMail to Mail.app, and think Kontact could do nicely on OS/X as well.\n\nKDE apps are maturing.\n\nDon.\n"
    author: "Don"
  - subject: "Re: More choice is GOOD"
    date: 2003-08-19
    body: "While you wait for a native Konsole, you could try <a href=\"http://iterm.sf.net/\">iTerm</a>.  :)"
    author: "Benjamin Reed"
  - subject: "Re: More choice is GOOD"
    date: 2003-08-20
    body: "I found iTerm to be buggy when I tried it out a couple of months ago, and switched back to Konsole-through-X. Unfortunately, apple's quartzwm (the X window manager for OSX) sucks a *lot* and doesn't pass some key events through, so the shift-left/shift-right events I have hard-wired in my brain for switching sessions in konsole ... don't work.\n\n*sigh*\n\nBring on KDE in OSX :)\n"
    author: "Richard Jones"
  - subject: "Re: More choice is GOOD"
    date: 2003-08-20
    body: "iTerm used to be pretty buggy, but it's gotten pretty solid recently.  I've done a build out of CVS and it's doing very nice."
    author: "Benjamin Reed"
  - subject: "Christ, people!"
    date: 2003-08-17
    body: "I never thought I'd see the day there were more stupid flames here than on slashdot.  That's quite an accomplishment.\n\nI'm not making you use KDE apps on Mac OS.  I'm not making you use Mac OS.  I'm not saying Mac OS is better than Linux, or worse.  No one's taking away your freedom by making KDE more portable to other platforms Qt runs on.\n\nI'm doing the port because it's fun, and because there are some KDE apps I'd like to not have to start X11 for.  Heck it will probably help with other ports.  It's not unlikely that making KDE work with Qt/Mac will also make it more likely to work with Qt/framebuffer.\n\nYou people need to chill out."
    author: "Benjamin Reed"
  - subject: "Re: Christ, people!"
    date: 2003-08-17
    body: "> I never thought I'd see the day there were more stupid flames here than on\n> slashdot. That's quite an accomplishment.\n\nYou don't visit the dot often, do you? ;-)\n\n \n"
    author: "Sad Eagle"
  - subject: "Re: Christ, people!"
    date: 2003-08-17
    body: "Oh, I've seen flames here plenty, but this is just silly.\n\nYou don't visit slashdot often, do you?  (grin)"
    author: "Benjamin Reed"
  - subject: "Re: Christ, people!"
    date: 2003-08-17
    body: "Ben the internet is the worlds largest truck stop bathroom stall door.  Any idiot with a penknife (or in this area a computer) can spout off their crap.  Chances are those who really care will contact you in person.\n\nBesides give me a few weeks and ill chip in,  I need to get a bigger drive and ill play too.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Christ, people!"
    date: 2003-08-18
    body: "\nYou mean, right after you get done with those Solaris KDE packages?\n\nWe're happily switching from Suns to OS X (G5's on order), and the one KDE thing that'll be missed is Konqueror as a file manager.  Konqueror.app would be a nice power-user tool IF it maintains resource forks where applicable, respects hiding those files which are customarily hidden in the Finder (as a default, at least), can  launch apps (e.g. `open /path/to/whatever.app`) and has drag-and-drop interoperability with the Finder and other applications.\n\nKonsole.app would be the second-most-desired port.  Interoperability with the OS X clipboard for copy/past would be excellent in both cases."
    author: "ex-Solaris"
  - subject: "Re: Christ, people!"
    date: 2003-08-19
    body: "you mean these?\nhttp://kde.geiseri.com/solaris/\n\nGranted they are 3.1.1, but they shouldnt be much different from the other fares out there.  They work for me (TM) ;)  When i get time ill see about new packages, since i only have an ultra 1 builds are slow so i only fix bugs in hopes someone with a faster machine will package them.  After things settle down in 3.2 i may do another sweep to clean up braindamages.  Maby we can even have real device support for the floppy and cdrom in 3.2 :)\n\nAFAIK Qt Mac already uses the Macos clipboard so that isall setup\nbut for hiding resource forks... that may be interesting.  Not looked into that one.  \n\nNever know... but money/hardware offers of food do help ;)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Design of KDE applications"
    date: 2003-08-17
    body: "I wonder if most KDE applications follow the Model View Controller paradigm for their application design? The reason I'm asking, is that the KDE gui guidelines are very different from Aquas(OSX). \n\nAn extra application menu and menu-structure, different keyboard shortcuts, what should go into the toolbar/palettes,.. It would be nice if it was very easy to have a model from a kde application like knode (qt/c++), and write the controller and gui in cocoa-objective-c++"
    author: "someone"
---
Now that the patches to make KDE build on Mac OS X are, for the most part, in KDE 3.2 CVS, it's time to move on to more drastic measures.  I'm <a href="http://ranger.befunk.com/blog/archives/000168.html">looking for help</a> working on making KDE compile using native Qt/Mac rather than X11.  We're still organizing, but if you'd like to help out, please <a href="http://www.opendarwin.org/mailman/listinfo/kde-darwin">join the discussion list</a> and say "hi".

<!--break-->
