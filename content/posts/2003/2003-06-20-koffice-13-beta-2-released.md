---
title: "KOffice 1.3 Beta 2 Released"
date:    2003-06-20
authors:
  - "binner"
slug:    koffice-13-beta-2-released
comments:
  - subject: "Great news!"
    date: 2003-06-19
    body: "THis is very good news that Linux is getting so many excellent office programs like Koffice 1.3, Abiword 2, and OO.org, now I think that ina  year Linux will certainly be ready for the msot demanding fo offices =)\n\nSpeaking of OO.org too, there is a plan for GNOME to leave Goffice in the dust ofr it and integrate it into GNOME etc. \n\nCheck out this interesting document on it: http://primates.ximian.com/~michael/guadec-ooo-2003/img0.html lots of cool changes too, and I think it makes some important points, but I am not suggesting that KDE ditch Kofice and go OO.org because Koffice is already quite mature just a little better integration with KDE. The OO.org quicstarter is a great step in bette integration =) \n\nThere's a lot of cool stuff in th office department for linux =) and windows I guess with Office 2003."
    author: "Mario"
  - subject: "Excitement mad me forget something "
    date: 2003-06-19
    body: "I was getting so excited about the linux office that I forgot to mention taht for some reaon the headline says beta 1 was released instead of beta 2."
    author: "Mario"
  - subject: "Also"
    date: 2003-06-19
    body: "It should say beta 2 in the titel of the article. I forgot to say that because of the OO.org stuff http://primates.ximian.com/~michael/guadec-ooo-2003/img0.html and this release and abiword etc. and missed it."
    author: "Mario-"
  - subject: "Re: Also"
    date: 2003-06-19
    body: "sorry again, I thought the first comment did not get submitted sicne it didn't appear for like 3 minutes =( Ill stop now."
    author: "Mario-"
  - subject: "Re: Great news!"
    date: 2003-06-19
    body: "The most exciting change is that Ximian OOo is sooooo unstable."
    author: "anon"
  - subject: "Re: Great news!"
    date: 2003-06-20
    body: "It isnt a GNOME plan, it is basicly (AFAIK) a suggestion by a ximian employee. There are a number of problems though:\n1) unlike mozilla which was built to be flexible, OO isnt, meaing making it integrated with gnome is practically impossible currently\n2) Both abiword and gnumeric are extremly mature in the gtk2 enviorment. There is even a possibility that gnumeric is gonna get proted to win32.\n\nSo whatever is going to happen, dont expect any major changes soon :P"
    author: "guest"
  - subject: "Re: Great news!"
    date: 2003-06-20
    body: "The Gnome Core Developers have targeted to make OO.org a Gtk-app since more than 3 years. Since then they spreaded the rumor that this will happen really soon. The truth is that you shouldn't expect it to happen within the next three years either."
    author: "Anon"
  - subject: "Re: Great news!"
    date: 2003-07-01
    body: "> Speaking of OO.org too, there is a plan for GNOME to leave Goffice in the dust ofr it and integrate it into GNOME etc. \n\nThat \"plan\" is no more than wishful thinking from one of the OpenOffice developers.   He would love if all the talented developers put their efforts into OpenOffice instead of other Gnome Office applications.  \n\nPrograms like Abiword and Gnumeric will continue on as long as there is even a single developer who want to work on them.  The gnome office app's will continue to get better and integration will come eventually, and at the same time interoperability and compatiblity with OpenOffice will improve.  \n\nAnyone interested in helping abiword integrate better with KDE is more than welcome to get involved, hell if someone wanted to make a QT port there is nothing stopping them (although there are probably better ways to integrate).  \n\n\n"
    author: "Alan H."
  - subject: "OT"
    date: 2003-06-19
    body: "I was reading news with Knode on \"news.uslinuxtraining.com\" but some weeks couldn't read any news over KDE projekts.\nCan sombody verify this or do somthing else on this.\n\ngreets andreas"
    author: "Andreas"
  - subject: "Re: OT"
    date: 2003-06-20
    body: "I don't know why, but the same applied to me."
    author: "Gerd"
  - subject: "Re: OT"
    date: 2003-06-20
    body: "You could switch to the excellent news.gmane.org (see www.gmane.org) if the problem persists."
    author: "Anonymous"
  - subject: "Re: OT"
    date: 2003-06-20
    body: "Everything should be fixed now. Could you please try again and see whether it works for you? Some of the CVS splits weren't working for a while, but they have been working again for the last few days. I currently use it as my nntp gateway.\n"
    author: "Ravi"
  - subject: "KOffice 1.3: My final migration to KOffice"
    date: 2003-06-19
    body: "I think I'll finally migrate from OpenOffice to KOffice with the release of the final stable version of KOffice 1.3. I find KOffice to be way more responsive than OpenOffice is, at least on KDE. And thank God for the PDF and OpenOffice.org filters. It is really thoughtful of the developers. Good Job guys!\n\nRegards,\n\nMystilleef"
    author: "Mystilleef"
  - subject: "Re: KOffice 1.3: My final migration to KOffice"
    date: 2003-06-19
    body: "Does the OpenOffice.org-filter make it possible to import MSOffice-documents as the OOO.org suit does or is it only the format bundled with OpenOffice.org that is concerned???"
    author: "coward"
  - subject: "Re: KOffice 1.3: My final migration to KOffice"
    date: 2003-06-20
    body: "Of course it's the OO format itself, otherwise it would state MS format."
    author: "Philipp"
  - subject: "Krita..."
    date: 2003-06-19
    body: "Is it dead? :-("
    author: "tuxo"
  - subject: "Re: Krita..."
    date: 2003-06-20
    body: "Practically yes... :("
    author: "Lukas Tinkl"
  - subject: "What I would like to see"
    date: 2003-06-19
    body: "Basically I have two problems with KOffice:\n\n1) The lack of standard file formats in the free software world. There was some talk about an oasis standard based on OpenOffice (which seems to be the most widespread free word processor today), but has anything happened? Office work has a lot to gain from Linux desktops but interoperability between them, say with a clean Bluecurve or Ximian professional desktop, must be perfect.\n\n2) The sad state of interoperability with proprietary Microsoft Office solutions. Again, OpenOffice seems to be state of the art here. There was some promising work with wvlib, did anything come out of that? Is OpenOffice a completely indepedent codebase?\n\nI would guess the easiest way to hack together a working solution would be to rip out the filters from OpenOffice to use in KOffice, and start using their document format right away. Has there been any progress with that?\n\n(As I could understand, the filters mentioned in the article are just export/import filters for OpenOffice documents from/to the KOffice semi-proprietary ones.)\n\nIt is somewhat amusing that not only do we have two major Linux desktops projects out there, but that the most widespread killer apps out there (OpenOffice and Mozilla) belong to neither. The latter is almost a desktop environment of its own. I hope all this experimentation will be fruitful for free software on the desktop for the masses in the long run!"
    author: "jb"
  - subject: "Re: What I would like to see"
    date: 2003-06-19
    body: ">\"It is somewhat amusing that not only do we have two major Linux desktops projects out there, but that the most widespread killer apps out there (OpenOffice and Mozilla) belong to neither\"\n\nBoth applications were already available before KDE and Gnome were founded. No wonder they don't 'belong' to either desktop.\n\nRinse"
    author: "Rinse"
  - subject: "Re: What I would like to see"
    date: 2003-06-19
    body: "Mozilla was a complete rewrite from the old Netscape-- the released Netscape 5.0 source was not even used for it. The Modern Mozilla thus was born in 1998. KDE was founded in 1996 and GNOME in 1997 in comparison.  \n\nYou're right about StarOffice though-- it's been around for a very long time. Before StarOffice, StarWriter was a standalone program for DOS. I beleive it started in 1989. A full fourteen years of development. Of course, it probably has had rewrites from then on :)"
    author: "fault"
  - subject: "Re: What I would like to see"
    date: 2003-06-19
    body: "I just love it when people do their homework instead of babbling nonsense...\n\n> The lack of standard file formats in the free software world. There was some talk about an oasis standard based on OpenOffice (which seems to be the most widespread free word processor today), but has anything happened?\n\nIn fact both office suites use an XML format now. Beyond that Koffice switched from gzip to the worthless zip compression format as an initial move toward a common file format. I haven't followed recent developments in this area but that doesn't mean they haven't continued. FWIW Kword is a frames based word procssor and OO writer is not. However I'd say that having filters is certainly a serious step in that direction. Some things can't be done in a short time span.\n\n> Office work has a lot to gain from Linux desktops but interoperability between them, say with a clean Bluecurve or Ximian professional desktop, must be perfect.\n \nWhat in the world are you talking about? Running M$ office on Linux with Ximian? What does that have to do with Koffice? Maybe you're in the wrong place?\n\n> The sad state of interoperability with proprietary Microsoft Office solutions. Again, OpenOffice seems to be state of the art here. There was some promising work with wvlib, did anything come out of that?\n\nWhy don't you at least look at koffice instead of looking to have somebody else read the release information to you or check the dependencies and get back to you? Filtering is a joint effort.\n\n>  It is somewhat amusing that not only do we have two major Linux desktops projects out there, but that the most widespread killer apps out there (OpenOffice and Mozilla) belong to neither.\n\nI really hope you're not amused that we have two desktops because we elected not to be fascist with one. Your \"killer OSS\" apps are a laughable choice when contrasted to the current Linux desktops. KDE is the oldest starting from nothing in 1996. OO.org in contrast started as Star Division in Germany IIRC in the late 80s but certainly was in development in the early 90s. I think we can agree that for a desktop based office suite you need the desktop first. Koffice is roughly 1/4 the age of OO.org and is preferable for many uses. OO.org had it's code base donated to open source when it was already a mature app. Mozilla is another story, starting out as Netscape it was open sourced and then re written from the ground up taking over four years to reach a 1.0 release and giving up about 60% of the web browser market. By most standards that's not exactly a stunning success. When Apple chose an HTML viewer component most of them were involved with Mozilla but went with KDE's KHTL instead.\n\nIt's actually pretty un-amusing anyone could make such a lame comparison. Outside of pre-existing software the new killer apps are largely desktop specific. On KDE that would certainly include Koffice along with Kdevelop, Quanta and a number of other excellent apps.\n\n> I hope all this experimentation will be fruitful for free software on the desktop for the masses in the long run!\n\nFUD somebody else! I'm busy making the best web development app anywhere with Quanta. It's NOT an experiment! If you want to comment then read something current on what you want to comment on instead of operating on assumptions."
    author: "Eric Laffoon"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "OK, I'll bite: how is Quanta going to be better than Dreamweaver, the current industry standard?"
    author: "dave"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Who or what is Dream Weaver? I never used it."
    author: "Datschge"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "A dot regular troll?\n\nCome one Datschge :)"
    author: "AySee"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Well, I simply dislike superficial comparisations which follow several imo wrong assumptions: That everyone know the features of a comercial program (which costs money) running on a proprietary system (which again costs money) and calling it a \"current industry standard\" (note the omission of \"quasi\", and even then Microsoft's Frontpage is probably more widespread...). \n\nIn general I'd wish people would stop making the assumption that everyone can easily check out commercial programs legally for looking what \"great features\" they are missing. Instead more people need to learn to describe missing \"killer features\" accuratelly and more detailed, when it's really that great developers might get excited as well and implement it after all.\n\nThat and the silly name was the reason for my \"troll\" post. =)"
    author: "Datschge"
  - subject: "Re: What I would like to see"
    date: 2003-06-21
    body: "I agree.\nI've never used DreamWeaver yet, but I'm a web developer, so if it's industry standard, why don't I know it?"
    author: "Beefy"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "It's basically the MS Office or Adobe Photoshop of web page/web application development. Basically any professional web page designers use DreamWeaver these days.   \n\nhttp://www.macromedia.com/software/dreamweaver/"
    author: "fault"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Or they use Adobe golive.  \n\nhttp://www.adobe.com/products/golive/main.html"
    author: "SMEAT!"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Or they use one of the many text editors."
    author: "Datschge"
  - subject: "Re: What I would like to see"
    date: 2003-07-10
    body: "Dreamweaver is only useful for semipros. A a pro you need FULL code-control!\nAnd 100% proper XHTML-conformance of the resulting documents! And when i say 100% i mean 100%!!\n\nWhat are these wizards for anyway?? Just overhead.\nWhat really counts is the ability to CODE quick and have foll control.\nAnd the ability to work flawless with all kinds of snippet of web-pages.\n(Also a reason against wizards. 99% of them don't work in snippets or complex code.)\n\nThe only acceptable thing here is homesite.\nInclude one or two libraries, a standard-setup-file, then do some code wich parses stuff from a db to xml or xhtml with some regex for searching stuff in the db. and if you then have to spit out some javascript depending on the result...\nDo i have to go any further to explain why drwamweaver is useless??\nSame for golive.\n\nBTW: If you even try to use frontpage in our company, they laugh at you... Building pages with it and putting them live on the server could result in you beeing fired!\n\nIn germany we say: \"Wenn man keine Ahnung hat... einfach mal: \"Fresse halten!\"\nSo shut up all you wannabe-pros!\n\nEver tried to code a web-application in dreamweaver mx??\n"
    author: "Hurricane"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "This is offtopic but Quanta can not be compared to Dreamweaver.\n\nDreamweaver is a WYSIWYG HTML editor (Although I'd call it WYSIAWYG - A for Almost :P) Quanta does not (yet) include a wysiwyg editor. But it is also more than just a plain HTML editor... (Go and read the features list, or better yet, try it! :P) The closest commercial program it could be compared to is Homesite, which used to be the industry-standard (for non-wysiwyg editors) before Macromedia bought it and \"integrated\" (more or less...) it into Dreamweaver. Needless to say Quanta is A LOT better than Homesite, or the non-wysiwig portion of Dreamweaver.\n\nAs for Dreamweaver being the standard I think this is sad but true. This program, along with GoLive!, generally produces \"dirty\" HTML (and I won't even talk about the javascript!) I think they are great tool for planning the design and for complex table layouts but coding should be done by hand... We can only hope Quanta will become the first wysiwyg editor that will produce clean, standard-compliant html :)\n\nThe major problem I think is that I learnt Homesite/Dreamweaver in school, but not Quanta. I did switch to Quanta but I don't think my old classmates did..."
    author: "Namshub"
  - subject: "Re: What I would like to see"
    date: 2003-06-21
    body: "Yeah, I appreciate the difference - although IIRC Quanta is getting WYSIWYG support. But the original poster claimed he was building \"the best HTML editor in the world\" or something similar. I'm all for ambition, but it should be tempered with realism. We will be able to say that Quanta is the best HTML editor in the world when web designers all over the world agree and switch to it :-)"
    author: "Dave"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: ">>OO.org in contrast started as Star Division in Germany IIRC in the late 80s but certainly was in development in the early 90s.<<\n\nYes, StarWriter has been released for 16-bit systems like the Atari ST in the late 80s. I have doubt that there is StarWriter code left though :)\n"
    author: "AC"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "zip compression is not worthless, but with the old .tar.gz compression you had to uncompress the whole file before you could read one of its contents. zip allow one contained file to be extracted by itself"
    author: "ik"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Yes... the compression rate suffers though, especially with many smaller files (because the compression dictionaries must be re-created for each file, the compressor does not 'learn' from the preceding files). "
    author: "AC"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: ">  2) The sad state of interoperability with proprietary Microsoft Office solutions.\n\nI think that so-called interoperability question is much overrated.\n\nPart of this is that people are so stupid that they send people FINISHED documents in a wordprocessor's native file format rather than RTF or PDF. \n\nQuite often these even contain deleted text. :-(\n\nIf you want to send someone a completed document, you should use an open standard file format such as PDF or PS.\n\nSo, what I think is needed is the ability to import PDF and PS files (perhaps RTF would be a good idea as well).  KWord now has a PDF import filter which I see as a great interoperability feature.\n\nSo, I ask: are there really a lot of instances where multiple users need to work on the same document with different wordprocessors?\n\n--\nJRT "
    author: "James Richard Tyrer"
  - subject: "I fully agree..."
    date: 2003-06-20
    body: "\"Quite often these even contain deleted text. :-(\"\n \nWe should inform everyone about that fact, people using Microsoft Office then might consider switching or at least using a sensible format (even though I personally enjoy seeing all the document changes a Microsoft document went through ;o)."
    author: "Datschge"
  - subject: "Re: I fully agree..."
    date: 2003-06-20
    body: "Most deleted text in office documents come from the \"Quick Save\" feature (either that, or from track changes..).. If this is on, Office basically writes documents as a series of diffs. It's completely transparent to the end user, and usually works well. However....\n\nwhile Quick Save might have made saving files lightening fast on my 16 mhz Apple IIci with Microsoft Word 4.0, I'm not sure why Microsoft still defaults to it.  It actually balloons files to several times their original size. \n\nQuick Saved' files also are much harder for foreign importers in other apps such as OOo and koffice to open. \n\nA good way to disable Quick Save in Office is to simply use the \"save as..\" command instead of save."
    author: "fault"
  - subject: "Re: I fully agree..."
    date: 2003-06-20
    body: "Let's look at it from an MS point of view ...\n\nIf you are charged with increasing Office market share, what will be your first decision? \n\n1. To make MS Office documents easier to convert to open source alternatives?\n2. To make it more difficult for open source alternatives to read MS documents?\n\nGee, let me think now :)\n\nRemember that OS/2 had such great MS application support that people were able to run MS apps under OS/2. So migrating to MS was made very easy. \n\nWhy do you think MS hasn't made it possible to mount Linux partitions under Windows? (It is possible, but only through third-party add-ons). The same thing could happen to MS.\n\nI will be totally suprised to see MS make their document format open to the world."
    author: "AySee"
  - subject: "Re: I fully agree..."
    date: 2003-06-20
    body: "Your point is correct, but I would also point out that the answer to the question depends on whether you have a monopoly.\n\nIf you have less than 50% of the market, it appears that the opposite is the case -- that it is to your advantage to have your WordProcessor file format a published standard.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "> I think that so-called interoperability question is much overrated.\n\nHave you ever worked or been involved in a buisness or job that isn't computer related? try it sometime; it'll give you a fresh prospective on things.\n\n> Part of this is that people are so stupid that they send people FINISHED documents in a wordprocessor's native file format rather than RTF or PDF.\n\nExactly. They are stupid. But you can't expect them to be smart either. For most people, .doc is the ONLY file format. They've likely not know what RTF is, and while they might know how to view PDF's, they might now know how to create them. \n\nLike it or not, Office is the standard for pretty much everything these days. If a office suite doesn't offer strong Office compatability, it'll likely not be able to be of use to very many people who do a lot of non-computer relaated work  on their computer (hint.. hint.. use their computer as a tool).\n\nBut.. that being said, continue the good work on koffice. Every little thing is appreciated.. it's becoming better and better everyday. :)"
    author: "fault"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "> Have you ever worked or been involved in a buisness or job that isn't computer related?\n\nYes, but in those days I used a wordprocessor called Olympia. :-)\n\nI still don't get it (or perhaps you don't).  If somebody is stupid and sends me a *.doc file which is a finished document, I don't need to open it in my wordprocessor.  I simply look at it with: \"Microsoft Word Viewer 97\".  That runs on WINE.  \n\nNOTE: If WINE would please get their PostScript printing fixed (it is a mess similar to KWord), I could import the text formated EXACTLY into KWord if I wanted to mark it up. \n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Well, Wine isn't much use if you don't use an x86 machine."
    author: "Stephen Douglas"
  - subject: "Re: What I would like to see"
    date: 2003-06-21
    body: "> Well, Wine isn't much use if you don't use an x86 machine.\n\nI don't know (for sure) if you can run either WINE or MS Office on an Alpha, so I will say that with the single exception of the Mac that MicroSoft Office isn't much use on non-x86 systems either.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-21
    body: "No, Wine only works on x86. But what I meant was that KOffice is useable on every processor supported by a decent *nix type OS, unlike Wine (or like you say, unlike MS Office)"
    author: "Stephen Douglas"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Well, i have to agree here. 95% of the current company's use office. It's a fact of life. So, it's quite logical they use these format's when they interchange documents, it's easy, you don't have to convert to anything. The only exception are pdf files if something has to be send without anyone changing it.\n\nThese days the real monopoly of Microsoft is on the office suite. It used to be Windows _and_ office, but with the current state of Linux+KDE it's only the office suite. If we want to give companies a reasenable alternative to Microsoft, we MUST have strong compatibility with the office formats. we really, really MUST!!!\n\n"
    author: "faz"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "No, we need not.\n\nThe overall benefit of using KGX is someday going to be big enough to make people switch for it.\n\nIf not that, nothing else is going to make it.\n\nIf you have time to volunteer to making any of the office product import/export better the MS formats, then spend it.\n\nOtherwise, please allow everybody else to do what is fun for THEM. Which may very well be improving the Office they already use.\n\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: ">  The overall benefit of using KGX is someday going to be big enough to make people switch for it.\n\nOf course, one of the problems is that in order for things like Koffice to be mainstream and supplant MS Office, it needs to have strong compatability with the latter. This is how WordPerfect supplanted WordStar, and how MS Word supplanted WordPerfect as the premier word processing application. \n\n>  Otherwise, please allow everybody else to do what is fun for THEM. Which may very well be improving the Office they already use.\n\nAgreed."
    author: "fault"
  - subject: "Re: What I would like to see"
    date: 2003-06-21
    body: "> This is how WordPerfect supplanted WordStar, and how MS Word supplanted WordPerfect as the premier word processing application. \n \nDidn't WordPerfect like -- ah -- buy WordStar?\n\nSo, please tell us why compatibility is needed.  \n\nI have tried to explain why it is NOT.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-22
    body: ">   Didn't WordPerfect like -- ah -- buy WordStar\n\nNope-- never. Both WordPerfect and WordStar ended up in the hands of Corel however. Corel licensed the code for WordStar from Softkey (which became the Learning Company) in 1994, deciding to make a office suite to compete with WordPerfect and Office. They bungled this up, although they did successfully launch another licensed product that became CorelDraw :)\n\n>  So, please tell us why compatibility is needed. \n \nAh, that's easy. Think of it this way: you have an office full of people who have hard drives full of Microsoft Word documents. This particular office has been using MS Office only since about ~1997 (as many have).  They have, over the years, piled up a nice collection of full of floppies, hard drives, zips, and DAT drives full of MS Office documents. Depending on what this-place-of-buisness does, it may mean important records, letters, documents, books, presentations, spreadsheets, etc...\n\nSo... one day, the IT manager at this particular office goes: \"Alright guys.. Money is getting tight. We don't want to pay Microsoft for the next round of licensing for the next Office version. We either are going to stick with our current older version of Microsoft Office or transition into another office suite. Most companies will pick option A, which explains the lack of market prenetation of perfectly capable MS-compatable office suites such as StarOffice. \n\nHowever, if they do decide to go with another office suite, do you think they going to pick YaYaOffice (which has great MS Office compatablity) or JujujuOffice which has very little MS Office document compatablity? "
    author: "lit"
  - subject: "Re: What I would like to see"
    date: 2003-06-22
    body: "> Both WordPerfect and WordStar ended up in the hands of Corel ...\n\nAnd then there was no more WordStar.  Which was my point, but thank you for correcting my hazy recollection of the exact history. \n\n>> So, please tell us why compatibility is needed.\n\n> Ah, that's easy. Think of it this way: you have an office full of people who have hard drives full of Microsoft Word documents. \n\nYes, they have hard drives full of old MicroSoft Word documents.\n\nSo, please tell us why they need a MS Word compatible wordprocessor? :-)\n\nTo make this clear.  In order to show why a compatible wordprocessor is *needed*, you need to show why there is a need to ever open these OLD documents in any wordprocessor.\n\nKeeping old finished documents that will never be edited again in a wordprocessor's native format is a sloppy habit that we should get over.\n\n--\nJRT\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-07-25
    body: "> > Both WordPerfect and WordStar ended up in the hands of Corel ...\n\n> And then there was no more WordStar. Which was my point, but thank\n> you for correcting my hazy recollection of the exact history\n\nActually that's not quite correct. Corel started on a 32-bit version of WordStar for Windows, but abandoned it when Word Perfect became available. WordStar never changed hands and is now owned by either Gores or River Deep - I don't think they're even sure of who owns it. The history, as far as it goes, is on my Web site at http://www.wordstar.org/wordstar/history/history.htm\n\n> So, please tell us why they need a MS Word compatible wordprocessor? :-)\n\nThey don't - but they DO, most definately need to be able to view and print, and maybe edit those old MS files.\n\nYou'd be surprised at the number of people you contact me and say something along the lines of:\n\n  \"My dad wrote our family history using WordStar. I don't know\n   which version, but I want to read those old files into Word.\n   Can you help me.\"\n\nOnce a document has been written and printed once there's no reason why, in a year's time, or maybe even ten, why someone shouldn't want to print it again. To do this you need a program that can view and print the document. Of course, if every document gets printed to Acrobat format, while that remains a standard, there's no problem - but most documents aren't printed this way.\n\nAny word processor these days MUST support Microsoft's RTF as an absolute minimum. Being able to acurately read and convert Word 2, 6, and 9x documents would also be an imense advantage. If you acn also read - correctly - word perfect and WordStar documents you've increased your potential sales. If in addition you canthen provide a keyboard commend set that replicates Word Perfect or WordStar you've increased your possible market again!\n\nCheers\nMike"
    author: "Mike Petrie"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: ">>So, what I think is needed is the ability to import PDF and PS files<<\n\nImporting PS is almost impossible. PS is a full programming language. While it may be possible to extract the text, there are a million ways to position it.\n\nPDF is a little bit easier, since it is a very restricted subset of the PS programming functionality (with some new layout abilities), but still very hard. Try Google's PDF->HTML rendering capabilities, and see how often it fails.\n"
    author: "AC"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "You didn't look at KWord PSF import yet, did you?"
    author: "Datschge"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "OH. $&#*, the \"AC\" trolls are back.\n\nYour statement makes no sense.  The fact that PS is a full programing language is irrelevant.\n\nIt is true that you can only import PS if it is being used to describe a page to be printed -- Penrose tile and affine transform ferns might be a problem.   But, if a page can be described in one representation, then it can be converted to a different representation of the same page.  How do you think that GhostScript displays stuff on the screen or sends it to the printer?\n\nPerhaps it might even be easier than Micro$oft Word (*.doc) files since PS is fully documented and there is already a code base available OS (GhostScript & PS2Edit).\n\n--\nJRT \n"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-22
    body: "This AC is not a troll.  The fact that PS is a full programming language is *very* relevant.  Writing a program that figures out exactly how the text flows on the page is nearly impossible.  There are an infinite number of ways of positioning the text, so you can't just figure it out from the code.  You have to run the PS and look at the output.  But figuring out how the text on a page is positioned and how it flows around objects is a very hard problem, on the order of speech recognition or reading text.  The text might be in several columns, right-justified, wrapping around circular objects.  Who knows?"
    author: "not me"
  - subject: "Re: What I would like to see"
    date: 2003-06-22
    body: "> Writing a program that figures out exactly how the text flows on the page is nearly impossible. \n\nWhat you are saying is true but irrelevant.  IIUC, you are saying that it is impossible for a program to determine information that is not contained in the PostScript file.  I believe that that is a tautology.\n\nHowever it is quite possible for a program to render a PostScript file as an image.  We already have one of these you know -- it is called GhostScript.\n\nThere already exist programs that import PostScript and render it as an image -- The GIMP for example.  WordPerfect will import PS (EPS) and render it as an image.\n\nHow do they do this if there is a font in the PostScript file?  It is quite simple, they render it.  To import it as text, all that is needed is not to render it but to use the same information to import it into the WordProcessor's representation of text.\n\nWe already have much of this functionality.  Have you ever used: \"pstoedit\", \"ps2ascii\" & \"pstotext\"?  If you know the text, its charismatics and where it goes on the page, you can make a WordProcessor file with that information.\n\n--\nJRT\n "
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-23
    body: "> To import it as text, all that is needed is not to render it but to use the > same information to import it into the WordProcessor's representation of \n> text.\n \nThis is not \"importing\". This is OCR.\nYou see... in many PS files there is no text at all! There is just a description of where some glyphs are to be displayed on the sheet. Some of the glyphs can be text (in the WP sense), some bitmap, some pure vectors.\nExtracting from this the \"WordProcessor's representation of text\" is nothing more or less then OCR, not much different then extracting text from a bitmap or, a better example, a coreldraw file."
    author: "Vajsravana"
  - subject: "Re: What I would like to see"
    date: 2003-06-23
    body: "To try to interpret your nonsense:\n\nThere are two possibilities:\n\n1.\tThe text is represented as glyphs directly or by a name or number that reference a font (either embedded or not embedded).\n\n2.\tThe text is a graphical representation.\n\nIn all cases that I know of, the default output of wordprocessors is #1 although some do offer #2 as an option.\n\nObviously, if it is #1 then the information can be imported as text, and if it is #2 then it can only be imported as a graphic.\n\nSince you are a PostScript expert, I don't need to tell you that even if the text can not be extracted with: \"ps2ascii\", the PS file may still be #1.\n\nIf you had left an e-mail address, I would have sent you a sample.  But, you can do it yourself.  Make a document with KWord, print it to a PS file, convert it to PDF with: \"ps2pdf\" and import the PDF back into KWord (you need the import filter if you don't have 1.3 Beta).  This might not work perfectly, but you will get text when you import it.\n\nNOW, try: \"ps2ascii\" on the PS file.  NO text.  Open the PS file with KEdit.  NO text.\n\nIs it magic??  Or, perhaps you don't know what you are talking about. 8-D\n\n--\nJRT\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-24
    body: "> There are two possibilities:\n>  \n>  1. The text is represented as glyphs directly or by a name or number that reference a font (either embedded or not embedded).\n>  \n>  2. The text is a graphical representation.\n \nWhat I meant to say is that, although not common, there are programs which generate postscript as pure vector graphics, keeping the glyphs as vector shapes and discarding the character and the font map that originates the glyphs (you can call it \"graphical representation\", but this is often used for bitmaps).\nYou can see a good example of these files if you use hylafax via WHFC and analyse the PS output of various win32 programs... the difference between similar documents printed in slightly different ways is sometimes amusing.\n\nOf course, even if the result is similar when  printed or viewed, there is no means of reimporting as text this kind of (otherwise perfectly legitimate) files, but only as a useless vector image. \nThis discards the idea of using PS as a standard archiving format, at least if you don't limit to programs that generates it \"correctly\".\n\n> Obviously, if it is #1 then the information can be imported as text, and if it is #2 then it can only be imported as a graphic.\n \nAs you know, \"as graphic\" or \"not at all\" has exactly the same meaning in this context.\n\n> Since you are a PostScript expert, I don't need to tell you that even if the text can not be extracted with: \"ps2ascii\", the PS file may still be #1.\n>[...] \n\nI usually don't use ps2ascii at all, I know very well how many problems it has, and I too skip directly to PDF instead, when I can.\n\n> Is it magic?? Or, perhaps you don't know what you are talking about. 8-D\n\nOr perhaps you did not even try to understand what I was talking about. :)\n\n"
    author: "Vajsravana"
  - subject: "Re: What I would like to see"
    date: 2003-06-24
    body: "> ... although not common, there are programs which generate postscript as pure vector graphics\n\nSo, if it is: \"not common\" it isn't really relevant to the question, is it?\n\n> This discards the idea of using PS as a standard archiving format.\n\nI didn't say that it should be used as a \"standard archiving format\", I have, and do, suggest that PDF be uses as a standard format.  However, this does not mean that it would not be useful to be able to import PS files directly into your WordProcessor without having to convert them to EPS or PDF first.\n\n> As you know, \"as graphic\" or \"not at all\" has exactly the same meaning in this context.\n\nWell, importing them \"as graphic\" isn't going to get you the text, but this feature -- which is already somewhat available on WordPerfect (it requires EPS) -- still has its uses.\n\n> Or perhaps you did not even try to understand what I was talking about. :)\n\nYes I did, but I could not tell if you were only talking about PS files that are totally graphic images or the PS files that appear not to contain text when they actually do.\n\nNow that I fully understand, I can state that your reasoning is flawed.  Your assertion appears to be that in some (not common) instances you will find a PS file that represents text as a graphic image and that, therefore, the ability to import PS (as text) into a WordProcessor is not a useful feature.\n\nThis is to say that because it won't always work that there is no point in having it.  This is illogical -- backwards reasoning.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2004-03-28
    body: "Well, is nearly impossible to know how the text is displayed by code analysis, but is perfectly possible by simulation of the code. \n\nIf I am not wrong, all the output in postscript is done by a virtual pen moving on the paper, but all the fonts are stored appart. If is like that, is possible to know where the fonts are being displayed and in which order, just simulating an execution of the program.\n\n"
    author: "Arboleya"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Boy, you sure do bring all the FUD that's been refuted millions of times already.\n\n1) The common standard is still being developed, but something like that doesn't happen overnight. KOffice is moving toward it, but the first step is the development of OOo filters which would smooth the transition. As a start, KOffice changed their file format to use zip, and be more similar to the OOo way of doing it. Changing to a different fileformat is a HUUGE undertaking and KOffice needs volunteers to help it.\n \n2) wvlib is still being developed, but the focus is on getting things right. The current filters work very well for basic elements, and support for complex stuff like embedded objects, tables etc, is coming later. wvlib is shared between Abiword and KOffice, and has nothing to do with OO filters.\n \nIf you think that ripping out OpenOffice filters is that easy, why don't you do it. The fact is that it was tried, but was so damn complicated that there would be no point. Shaheed has explained this on the Koffice list many times over. Also, switching to the OOo/Oasis file format is planned for later versions of KOffice. It is not something you can do in a day.\n \nBTW, KOffice document format is based on XML, is documented and transparent. Why are you calling it semi-proprietary?\n \nI really appreciate KOffice for what it is -- a lightweight office that's intuitive, fast, and fun to use. Once the filters improve and there is a common document format, it will be great for most purposes. The only thing KOffice needs is help from coders."
    author: "KOffice fan"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: ">>The lack of standard file formats in the free software world. There was some talk about an oasis standard based on OpenOffice (which seems to be the most widespread free word processor today), but has anything happened? <<\n\nI guess one of the problem is that a word processor is not a word processor. Unlike any other major word processor, KWord is frame-based. OO's Writer is able to do web forms, which are not supported in KWord. And there will be thousands of things like that... \n\nYou could save the world so much trouble by just using the same word processor as your co-workers (when you collaborate) and otherwise use PDF (when the document is done)...\n"
    author: "AC"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "Yes, you are right. I've been doing my share of import filters to realise that it is more or less impossible to find a common file format. In order to to that, every program would have to match feature by feature, and those features would have to be implemented in pretty much the same way. An example: What is just a visual line in one application may have a semantical meaning in another, while still denoting the same thing. Good luck finding a good way of importing from the first program into the second.\n\nThe important thing is not what file format your word processor is using, but what format you distribute your final document in. The word processor's own file format should only be used when writing your document, not when distributing it.\n\nThe default file format for an application should IMHO reflect the internal data representation of that program. Otherwise there will be a lot of programming kludges. There will be problems moving documents between versions of that program, but that can easily be solved by either sticking to the same version when writing a document, or by using some upgrade tool (which is much easier to develop than import filters between different applications). If it was possible to have one common file format for all word processors, we might as well only have one word processor.\n\nUse one format when writing your document, and then another (e.g. pdf) when you're finished and want to distribute it."
    author: "trynis"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "What I miss in the discussion is TIME. No office user has time to deal with file formats. What counts today is speed.\nUsing kmail and receiving M$.doc files is just a painful experience. No way to sell this. So it's not just KOffice which suffers it's the acceptance of KDE-Desktop. \njust my 2c\nBTW I try to use as much as possible of KDE, but sometimes this compatibility question  IS th e issue.\nferdinand"
    author: "Ferdinand"
  - subject: "Re: What I would like to see"
    date: 2003-06-20
    body: "So, you can start the KM$WordView project. :-)\n\nMaybe that isn't such a bad idea.  It is only 3567104 Bytes to reverse engineer -- plus the DLLs if you want it to run on other than x86.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What I would like to see"
    date: 2003-06-21
    body: "yes - I know - but I am not a coder as you propably know.\n\nIMHO it's necessary to create awareness among those people capable of coding to focus on key-success-factors of KDE. \n- KDEPIM, compatibility and connectivity issues\n- KOFFICE, compatibility issues\n- KHMTL, rendering issues\n\nI think those working on this programs are well aware of these matters, but I do not see enough \"recrutement\" and publicity to attract new coders.\n\nReading the announcements gives the impression, that everything is fine.\n\nEvery announcement is the place to point to open jobs!\ncu\nferdinand\n"
    author: "Ferdinand"
  - subject: "\"KOffice 1.3 Beta 1 Released\""
    date: 2003-06-19
    body: "Beta 1 or Beta 2?"
    author: "Victor R. Ruiz"
  - subject: "KD Chart?"
    date: 2003-06-19
    body: "I see that Klar\u00e4lvdalens Datakonsult AB (http://www.klaralvdalens-datakonsult.se/) has finally released KD Chart (http://www.klaralvdalens-datakonsult.se/kdchart/).\n\nIs this version incorporated into this release of KOffice? Based on their comment it is: \"It is no coincidence that the current version of the KOffice productivity suite uses our library.\" If so, is it available as a separate library? I thought that at one time a saw on their website that KD would make this available to the open source community."
    author: "Anonymous"
  - subject: "Re: KD Chart?"
    date: 2003-06-19
    body: "Last time I looked the cvs commits were 8months old. \nBut it looked fairly easy to rip out into a separate GPLed library. \nHope they will do it as just a module in KDE cvs, then koffice could \nimport ala admin if they are terrified of dependencies.... \n\nI always wonder, I reckon the only dependency moaners must be:\n\t1) Redhat users who don't know about apt for rpm\n\t2) Compile from source monkeys who don't just use gentoo\n\t3) Idiots.  \n\nI've never got it, its really unbelievable that after all this time people still \ndont just apt-get/emerge/urpmi koffice."
    author: "rjw"
  - subject: "KOffice not as multi OS as OpenOffice"
    date: 2003-06-20
    body: "Koffice is certainly nice but it's still just portable between platforms that has a GPL'd QT version. This only amounts to a fraction of the potential market, thus hindering OS adoption and the freedom for the community. If the Trolls are worried about loosing in-house development revenue then can't they add restrictions for commercial in-house development?"
    author: "Fredrik C"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-20
    body: "\"thus hindering (...) the freedom for the community\" How so?"
    author: "Datschge"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-20
    body: "If you feel that being locked to certain platform(*nix) to use a large portion of free software is a good thing then of course the above is false. But I for one think it's great to be able to use Mozilla/OO/Gimp/Scite/Python/wxWindows on every platform I use. As more and more cornerstones of free computing comes from KDE the lack of ports for Win32 keeps me from using it. Even if you think it's a good thing to force people to use Linux I don't like the notion of being ruled by the Trolls when it comes to GPL'd software."
    author: "Fredrik C"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-20
    body: "http://kde-cygwin.sourceforge.net/"
    author: "hbc"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-23
    body: "Not a optimal solution more a proof of concept. It strikes me that one of QT's selling points \"easy portability\" don't apply to the community, and nobody seems to care. Don't bite the hand that feeds you? \nAs for the FreeBSD remark below, try to understand the topic you reply to (I have nothing against GPL).\n"
    author: "Fredrik"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-23
    body: "Well, as someone who is able to pay for a Windows license, an Office license and several other licenses for professional programs you supposedly can't get anywhere else you surely can also spare some cheap money for a toolkit, can't you? \n\nBesides the community aspect should normally be an aspect pro Windows considering their supposed huge user base since more users must mean more developers willing to improve something. And this huge amount of people should be at least able to redo the tiny X11 part of the GPL'ed QT into a working GDI part for Windows, right? Right?\n\nWindows users still can't afford it? Poor poor Windows users...\n\nYes, you are right, nobody cares, and that's quite fine with me. =p"
    author: "Datschge"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-23
    body: "Well, as someone who is able to pay for a QT license, surely can also spare some cheap money for a OS/Office suite, can't you? \n\nSeriously, my business is trapped in a windows environment, why not make the best of a bad situation.\nOpenoffice has already helped loose some shackles, when more and more programs are multi os there is less reason not to use a free os. But as of now Koffice is out of the competition for 95% of the user market.\n"
    author: "Fredrik"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-23
    body: "The best any business could do to themselves would be forcing themselves to use FOSS and pay the money to developers for programming whatever specifics they need what isn't possible with FOSS yet instead paying the very same money for trapping license contracts. The result would be a true un-trapping instead pretending that Windows was still alright since you paid for it and nevertheless \"saved money\" by getting all the (still insufficient, since not actively supported) FOSS for free."
    author: "Datschge"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-20
    body: "If you do not like GPL then use FreeBSD. Koffice and KDE run there as well."
    author: "Jose"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-20
    body: "If you do not like GPL then use FreeBSD. Koffice and KDE run there as well."
    author: "Jose"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-20
    body: "You know it's really funny how you are talking about Trolltech putting restrictions on you while you are obviously happy to continue using Win32 including all the ridiculous restrictions Microsoft puts on you. Please do yourself a favor and read following article about Microsoft's EULA you chose to agree on by using Win32: http://www.cyber.com.au/cyber/about/comparing_the_gpl_to_eula.pdf"
    author: "Datschge"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-21
    body: "\nFor many people not using Windoze is not an option.  If you\never want to get these people off M$ products you have to\ngive them (and their bosses) a path to follow.\n"
    author: "Sphere"
  - subject: "Re: KOffice not as multi OS as OpenOffice"
    date: 2003-06-23
    body: "There are many paths already. The problem is not that they don't exist but that too few people care to look for them."
    author: "Datschge"
  - subject: "PDF import filters?"
    date: 2003-06-20
    body: "The paged linked as \"PDF import filter\" says nothing about PDF ...\nDoes this import PDF into KWord?"
    author: "David Fraser"
  - subject: "Re: PDF import filters?"
    date: 2003-06-20
    body: "Yes"
    author: "me"
  - subject: "Re: PDF import filters?"
    date: 2003-06-20
    body: "Great! That would mean interoperability with LyX. Output to pdf from lyx then import into kword. This is the first thing I'll try once I can emerge beta2 on my gentoo system. \nExport pdf document from lyx, then decorate the title page in kword."
    author: "kool"
  - subject: "Re: PDF import filters?"
    date: 2003-06-20
    body: "i think thats not a good idea.. probably the pdf import will loose a lot of the layout. and you loose the latex typesetting offcourse when you import into kword"
    author: "ik"
  - subject: "Re: PDF import filters?"
    date: 2003-06-20
    body: "You might also want to try LyX -> LaTeX -> KWord.\n\nBTW, the status of the PDF import filter can be found here:\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/koffice/filters/kword/pdf/status.html?content-type=text/html\n\nUnfortunately the status hasn't been updated since 6 months."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: PDF import filters?"
    date: 2003-06-20
    body: "TeX import doesn't work on 1.2.1. kword simply gives a message and quits. don't know about 1.3.."
    author: "kool"
  - subject: "What is the problem..."
    date: 2003-06-20
    body: "What is the problem with using a lot of stuff from OOo?\n- that OOo libs should be used then? (instead of KDE libs)\n- licening problems?\n- ??\n\nKOffice could ditch their own native format and adopt OOo's format as native. Use OOo filters, maybe even use (parts) of OOo's canvas(es). _IF_ this is possible it would help the end-user a lot. The opendesktops can use KOffice the Win-tops can use OOo...\n\nI know this has often been proposed, but i cant remember what was the main reason against it.\n\n-Cies"
    author: "cies"
  - subject: "Re: What is the problem..."
    date: 2003-06-20
    body: "> KOffice could ditch their own native format and adopt OOo's format as native.\n\nCheck out http://www.oasis-open.org/home/index.php. Both OOo, koffice, and abiword/gnumeric people are participating in that in order to create a common and open format to rival MS together. \n\n> Use OOo filters,\n\nWork on this is ongoing. \n\n\n> I know this has often been proposed, but i cant remember what was the main reason against it.\n\nThe rest of what you said (using OOo canvases...) will never likely happen anytime soon. OOo was simply not made with this kind of thing in mind. "
    author: "fault"
  - subject: "Re: What is the problem..."
    date: 2003-06-20
    body: "> Check out http://www.oasis-open.org/home/index.php.\n> Both OOo, koffice, and abiword/gnumeric people are participating in that in order to \n> create a common and open format to rival MS together. \n\nCheck but could find anything... Anyway the goal \"create a common and open format to rival MS together\" sound great!\n\n> > Use OOo filters,\n>  Work on this is ongoing. \n\ncool"
    author: "cies"
  - subject: "Re: What is the problem..."
    date: 2003-06-20
    body: ">>What is the problem with using a lot of stuff from OOo?\n - that OOo libs should be used then? (instead of KDE libs)\n...\nKOffice could ditch their own native format and adopt OOo's format as native. Use OOo filters, maybe even use (parts) of OOo's canvas(es).\n<<\n\nIf you want OOo (and not use KDE technology), why don't you use OOo?\n\n\n"
    author: "AC"
  - subject: "Re: What is the problem..."
    date: 2003-06-20
    body: "> If you want OOo\n\nYes I want OOo, al least right now. In need M$Office compatibility. Kword only shows the simpelest of M$Word files right.\nBut I dont like all aspects of OOo, it has several disadvantages -- we all know (slow, big, alien-look). \n\n> (and not use KDE technology)\n\nI like KDE tech, a lot. Maybe i even love it ;-)\n\n> why don't you use OOo?\n\nSo currently I am. Hopefully in the furure I'll have some thing slicker ;-)\n\nNote: my post was not a troll, I wanted to know what is the _problem_ for using OOo code. It's both C++. (I think it is the libs that are used)"
    author: "cies"
  - subject: "Re: What is the problem..."
    date: 2003-06-20
    body: ">>my post was not a troll, I wanted to know what is the _problem_ for using OOo code. <<\n\nIt's a completely different program that behaves very differently and has never been designed as a library or something like that. KOffice has its own framework, that is used by most KOffice apps. "
    author: "AC"
  - subject: "Re: What is the problem..."
    date: 2003-06-20
    body: "> If you want OOo\n\nYes I want OOo, al least right now. In need M$Office compatibility. Kword only shows the simpelest of M$Word files right.\nBut I dont like all aspects of OOo, it has several disadvantages -- we all know (slow, big, alien-look). \n\n> (and not use KDE technology)\n\nI like KDE tech, a lot. Maybe i even love it ;-)\n\n> why don't you use OOo?\n\nSo currently I am. Hopefully in the furure I'll have some thing slicker ;-)\n\nNote: my post was not a troll, I wanted to know what is the _problem_ for using OOo code. It's both C++. (I _think_ it is the libs that are used)"
    author: "cies"
  - subject: "look"
    date: 2003-06-20
    body: "What about the koffice icons project?\nI heard that everaldo was involved in that project."
    author: "Giovanni Masucci"
  - subject: "Kivio import filters for Visio .vsd files"
    date: 2003-06-20
    body: "Nice to see lots of activity with Kivio.  Any chance for Kivio import filters for Visio files (*.vsd) ?  How about exporting a Kivio screen to pdf?"
    author: "KDE User"
  - subject: "Re: Kivio import filters for Visio .vsd files"
    date: 2003-06-20
    body: "You should already be able to print Kivio screen to pdf, isn't that sufficient?"
    author: "Datschge"
  - subject: "Re: Kivio import filters for Visio .vsd files"
    date: 2003-06-21
    body: "I don't think I'll have time to make an import filter for Visio myself, but if someone else steps up good luck to him/her. (Currently there are to many other things that needs to be done to get stuck with filter development). Exporting to pdf is already possible via KDEPrint using the print to pdf option.\n\n//Peter Simonsson, Kivio maintainer"
    author: "Peter Simonsson"
  - subject: "OO and KOffice Will Co-Exist...."
    date: 2003-06-21
    body: "When we get a standardized Office DTP format that both KOffice and OO can use it won't matter what you use. The days of locking people into software through particular formats are over. Anyone know what the status of the Oasis format is with KOffice?"
    author: "David"
  - subject: "Re: OO and KOffice Will Co-Exist...."
    date: 2003-06-21
    body: "The OASIS format isn't ready yet... And switching formats isn't done over night :)"
    author: "Peter Simonsson"
  - subject: "Re: OO and KOffice Will Co-Exist...."
    date: 2003-06-21
    body: "Agreed."
    author: "David"
  - subject: "Beta 2??"
    date: 2003-06-21
    body: "I do hope thatt here will be at least a Beta 3 because AFAICS, printing in KOffice still needs a lot of work:\n\nhttp://lists.kde.org/?l=koffice-devel&m=105609957315405&w=2\n\nhttp://bugs.kde.org/show_bug.cgi?id=60105\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Beta 2??"
    date: 2003-06-22
    body: "The things discussed in the mailinglist thread isn't anything that can be implemented between two betas and the bug isn't really a koffice issue but a qt bug... So these print issues probably will have to wait until the next koffice or qt version..."
    author: "Peter Simonsson"
  - subject: "Re: Beta 2??"
    date: 2003-06-22
    body: "The point is that KWrite is going for 1.3 and it still won't print correctly.  It also appears that a major rewrite of part of it will be needed to get it to print correctly.\n\nI consider this to be a serious problem.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Beta 2??"
    date: 2003-06-22
    body: "The printing issue is one of the things that holds off a 2.0 release... We can't just not stop developing because somethings isn't optimal yet (and no you don't want me trying to develop a better printsystem...)"
    author: "Peter Simonsson"
  - subject: "Re: Beta 2??"
    date: 2003-06-22
    body: "> We can't just not stop developing because somethings isn't optimal yet.\n\nWhat am I missing here.  It is much more (much worse) than not being \"optimal\".  (We have) a wordprocessor program that won't print correctly, and the problem appears to be that it wasn't designed correctly -- or perhaps just wasn't designed.  Shouldn't that have been correctly implemented before 1.0?  And why isn't it a \"show stopper\" bug?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Beta 2??"
    date: 2003-06-23
    body: "Because many people don't care about it as much as you do."
    author: "Datschge"
  - subject: "worse is better, get with the program"
    date: 2003-06-23
    body: "Ever heard of worse is better?\n\nhttp://www.jwz.org/doc/worse-is-better.html\n\nOkay, okay.  I'm baiting you cause I know you like it.  It's still an interesting read though."
    author: "anon"
  - subject: "PDF editing"
    date: 2003-06-23
    body: "What\u00b4s really the deal with the PDF import filter? What is the intended use once it matures enough? I'm asking as I really miss a good replacement for the Adobe Acrobat program (You know edit PDF-files, merge documets, re-arrange etc) in the KDE/Linux world. Should I get my hopes up?"
    author: "Swoosh"
---
On June 18th 2003, the KDE Project released the <a href="http://www.koffice.org/releases/1.3beta2-release.phtml">second beta version</a> of <a href="http://www.koffice.org/">KOffice 1.3</a>. It comes with a lot of bugfixes and a couple of new features such as a <a href="http://www.koffice.org/filters/status.phtml">PDF import filter</a>, new <a href="http://www.koffice.org/filters/status.phtml">OpenOffice.org filters</a> and more stencils for <a href="http://www.koffice.org/kivio/">Kivio</a>. <a href="http://www.koffice.org/kexi/">Kexi</a> isn't part of this version nor will it be in the final KOffice 1.3 but it's slated for a stand-alone release later this year and will re-integrated into KOffice in the next major version. Read more in the <a href="http://www.koffice.org/releases/1.3beta2-release.phtml">KOffice 1.3 Beta 2 release notes</a> and in the detailed <a href="http://www.koffice.org/announcements/changelog-1.3beta2.phtml">KOffice 1.3 Beta 2 changelog</a>. <a href="http://download.kde.org/unstable/koffice-1.2.91/">Binary packages</a> are expected to be available soon, for now you can only <a href="http://download.kde.org/unstable/koffice-1.2.91/src/">grab the source</a>. Next step is a release candidate <a href="http://developer.kde.org/development-versions/koffice-1.3-release-plan.html">planned</a> for 8th August.

<!--break-->
