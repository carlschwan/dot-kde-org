---
title: "Announcing KDE 3.2 Beta 1 \"Rudi\""
date:    2003-11-03
authors:
  - "coolo"
slug:    announcing-kde-32-beta-1-rudi
comments:
  - subject: "Rock on!"
    date: 2003-11-03
    body: "I thought the announcement was written incredibly well - a nice overview :o). \n\nNow, excuse me while I hunt for the download ;-)."
    author: "Eike Hein"
  - subject: "Re: Rock on!"
    date: 2003-11-03
    body: "Hi,\n\nI can only second that. The announcement reads just \"right\". \n\nCongratulations to the KDE developers, rarely ever did I more look forward to a software than KDE 3.2 for the great many new features and consolidations.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Rock on!"
    date: 2003-11-03
    body: "Here here! A wonderful announcement very clearly written and with a nice presentation of facts.\n\nOne minor quibble, though. The word \"nor\" is almost exclusively used after the word \"neither.\" The release should use the word \"or\" instead."
    author: "Bob"
  - subject: "Re: Rock on!"
    date: 2003-11-03
    body: "Well, if we're quibbling you should write \"hear hear\" instead of \"here here\" ;-) ... a quick google gives a few links of proof:<p>\n<a href=\"http://www.m-w.com/cgi-bin/dictionary?book=Dictionary&va=hear\">hear</a>\n<p>\n<a href=\"http://www.parliament.the-stationery-office.co.uk/pa/cm199596/cmhansrd/vo950116/debtext/60116-32.htm\">60116-32.htm</a>\n<p>\nI believe it comes from \"hear him, hear him\", used in the UK House of Commons. "
    author: "ed"
  - subject: "Re: Rock on!"
    date: 2003-11-03
    body: "Very true! I realized my mistake shortly after posting it. I thought it would have made me too much of a grammar nut to correct my own posting about grammar.\n\nThere's nothing funner than posting a response telling others to get smart and then screwing that up yourself :-)"
    author: "Bob"
  - subject: "Re: Rock on!"
    date: 2003-11-03
    body: "> I believe it comes from \"hear him, hear him\", used in the UK House of Commons.\n\nAFAIK it comes from the heralds who went through the towns with a drum to announce\nwhat the king/emperor had to say. (in German it's \"Hoert, hoert!\")"
    author: "Melchior FRANZ"
  - subject: "Re: Rock on!"
    date: 2003-11-04
    body: "Actually, one uses \"nor\" in conjunction with \"neither\" and \"or\" in conjunction with \"either.\"  I believe the author was correct."
    author: "Sean O'Dell"
  - subject: "Rudi KNOPPIX would be great for testing"
    date: 2003-11-03
    body: "A KNOPPIX version with Rudi would be great for testing - perhaps someone is already working on this? It would make sense for people who do not want to change their existing KDE installs but who would like to participate in the beta testing process."
    author: "probono"
  - subject: "Re: Rudi KNOPPIX would be great for testing"
    date: 2003-11-03
    body: "I second that - and I'm pretty confident there will be a Rudi-Knoppix available in no time :)."
    author: "Eike Hein"
  - subject: "Me too"
    date: 2003-11-04
    body: "I don't want to upgrade my 3.1.4, but I definitely want to preview and test 3.2b."
    author: "Alex"
  - subject: "Re: Rudi KNOPPIX would be great for testing"
    date: 2003-11-04
    body: "Yes I would definitly try it!\nThis computer is used others to so I got to keep it stable, knoppix  would be really great! I know some of my friends would be willing to try it too."
    author: "jadrian"
  - subject: "Very nice announcement.."
    date: 2003-11-03
    body: "well written.. no mention of programs:/ and settings:/ in konqi though, which are great =)"
    author: "anon"
  - subject: "Re: Very nice announcement.."
    date: 2003-11-03
    body: "Maybe it's not too late to change it...\n\n(I kind of expected the authors name/e-adress on the end of the annoucement)\n\nThis announcement will probably be revised/extended and than used for the final release."
    author: "cies"
  - subject: "Re: Very nice announcement.."
    date: 2003-11-03
    body: "The announcement was written by Aaron Seigo (aseigo)."
    author: "Anonymous"
  - subject: "Re: Very nice announcement.."
    date: 2003-11-03
    body: "we had to leave _something_ for the final announcement piece, right? ;-) the beta1 article was over 2200 words in first draft form. this was eventually trimmed down to be in the low 1700s, and even that's a bit long IMHO for a beta release announcement. getting it down to \"just\" that big required picking what to mention this time around and what to leave out for this announcement; i think that says a lot about the significance of KDE 3.2.\n\n\n\nbut have no fear, i'm sure the article for the 3.2 final release will be larger and more comprehensive."
    author: "Aaron J. Seigo"
  - subject: "I guessed right!"
    date: 2003-11-04
    body: "I knew it was you since I saw your name in a recent CVS Digest or Traffic where you asked KDE develoeprs to tell you about some important features tehy've added."
    author: "Alex"
  - subject: "Re: Very nice announcement.."
    date: 2003-11-05
    body: "Can you send me the article a few days before the release?. It's just for translate and publish in Spanish in our web page http://es.kde.org (KDE-Hispano).\n\nIf you give me a few days more, maybe I can translate just at time for all KDE spanish speakers users.\n\nThanks in advance.\n\nMy e-mail is melenasIHATESPAM@kdehispano.org"
    author: "Pedro Jurado Maqueda"
  - subject: "programs:/ and settings:/"
    date: 2003-11-03
    body: "These protocols doesn't really do anything here, but as there are no programs in the K-menu and nothing in KControl I guess its a SuSE specific problem as mentioned in a thread below..."
    author: "tanghus"
  - subject: "Beta"
    date: 2003-11-03
    body: "Congratulations :o) i've downloaded KDE3.2 Beta, but after the installation there is no menu, nothing in the kcontrol-center and i can't open any directory on my desktop, does someone know what this is? i'm using SuSE 8.2"
    author: "Sandro"
  - subject: "Re: Beta"
    date: 2003-11-03
    body: "Same problem here. Also SuSE 8.2. Seems the rpms are horribly broken."
    author: "MacBerry"
  - subject: "Re: Beta"
    date: 2003-11-03
    body: "I'm also affected from this Problems (SuSE 82). Kcontrol doesn't show any configuration-items, KMenu also seems to be broken. I hope this will be fixed ..."
    author: "sam"
  - subject: "Re: Beta"
    date: 2003-11-03
    body: "did you tried moving ~/.kde out of the way?"
    author: "ac"
  - subject: "Re: Beta"
    date: 2003-11-03
    body: "Same here (SuSE 9.0). I've downloaded from ftp://ftp.gwdg.de/pub/linux/suse/ftp.suse.com/people/adrian/KDE-CVS-BUILD/build.3.1.93_2003110113 a new kdelibs-/kdebase-package and it works :)"
    author: "star-flight"
  - subject: "Re: Beta"
    date: 2003-11-03
    body: "I did this, but made no difference to me. I still have nothing in the control center. I don't know how the control center works so I've not a clue how to fix it.\n\nOtherwise things seem fairly bug-free, I've noticed some quirks. No doubt I'll be bugzilling tonight."
    author: "Max Howell"
  - subject: "Re: Beta"
    date: 2003-11-03
    body: "I had to revert to 3.1.4 until the menus and panel are sorted. It's too soon since I installed SuSE 9 to not have various coolness. I run a cvs copy anyway so it's not too important."
    author: "Max Howell"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "I also had to revert.\nAll KControl items are listed under Applications.\nFonts for the konsole is missing and lots, lots more.\n"
    author: "Jarl E. Gjessing"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "Yes, i've tried to move .kde into another dir, but it doesn't work :-( it must be a bug with the SuSE-Version of the Beta, 'cause the Alpha II of KDE 3.2 worked fine..."
    author: "Sandro"
  - subject: "Re: Beta"
    date: 2003-11-11
    body: "It's not just SuSE, I've built it from source using konstruct on Red Hat 9 and my kontrol panel is also empty!"
    author: "nicka"
  - subject: "Re: Beta"
    date: 2003-11-11
    body: "It's known and fixed.  Read the full thread and you will find solutions:\n\nhttp://dot.kde.org/1067849710/1067875849/\n\nEspecially the stuff in /etc/xdg/.  I saw the bug report but I can't remember where.  The solution is to make symlinks and/or copies from default_(*) to (*) in that directory."
    author: "anon"
  - subject: "Re: Beta"
    date: 2003-11-03
    body: "That makes two of us. I don't know how to fix it. It's a pity :-("
    author: "Dirk"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "same problem with mandrake 9.1, kde 3.2 alpha2 was fine ;-(, dunno what is the problem ?\n"
    author: "Tariq Firoz"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "Those bugs you call, are new features, don't you like them? ;)"
    author: "ac"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "Buahhahhah!!! :-D\n\nCool features!\n\nLuckily Gentoo-versions work just fine... ;-)\n"
    author: "Nobody"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "Your Gentoo-Version works? but then You dont have the new features *hehe*"
    author: "Sandro"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "The README tells you ...\n<br><br>\n<a href=\"ftp://ftp.de.kde.org/pub/kde/unstable/3.1.93/SuSE/README\">\nftp://ftp.de.kde.org/pub/kde/unstable/3.1.93/SuSE/README</a>"
    author: "Micha"
  - subject: "Re: Beta"
    date: 2003-11-04
    body: "Thank You! i've downloaded this file and now it works! Congratulations again and keep up the great work :o)\n\n"
    author: "Sandro"
  - subject: "Re: Beta"
    date: 2003-11-05
    body: "From 'rrh' on the gentoo forums, this should fix your problem:\n\ncd /usr/kde/3.2/etc/xdg/menus/ \n ln -s default_kde-settings.menu kde-settings.menu\n\nand then logout/login. Works for me using the gentoo ebuild, and your symptoms sound identical"
    author: "Ben Roe"
  - subject: "Re: Beta"
    date: 2003-11-05
    body: "I have similar problem with KDE compiled from sources on Slackware-9.1. This command does not help."
    author: "Andrey V. Panov"
  - subject: "Re: Beta"
    date: 2003-11-05
    body: "OK. The ~/.kde directory should be removed or new KDEHOME must be created."
    author: "Andrey V. Panov"
  - subject: "Re: Beta"
    date: 2003-11-05
    body: "No. I use a fresh new user dedicated to kde3.2-beta. The problem is still there. The symlink did not help either.\nEggert"
    author: "Eggert"
  - subject: "Re: Beta"
    date: 2003-11-05
    body: "After making the sym-link, be sure to do a 'kbuildsycoca'\n\n"
    author: "Caleb Tennis"
  - subject: "Re: Beta"
    date: 2003-11-05
    body: "Worked for me.  I'm running RH9 and built KDE3.2-beta1 using konstruct.  Thanks!"
    author: "Z"
  - subject: "Re: Beta"
    date: 2003-11-06
    body: "Sorry, not for me. This is a SuSE 9.0 system, kde 3.2 beta1 built by Konstruct. Running kbuildsycoca (as root) produced a lot of messages like\nkbuildsycoca: WARNING: 'katepart.desktop' specifies undefined mimetype/servicetype 'text/x-c'\nkbuildsycoca: WARNING: 'katepart.desktop' specifies undefined mimetype/servicetype 'text/x-c++'\nkbuildsycoca: WARNING: 'katepart.desktop' specifies undefined mimetype/servicetype 'text/x-shellscript'\nStill, my Controllcenter only contains \"Peripherals\", \"System\", \"Yast2\".\n"
    author: "Eggert"
  - subject: "Re: Beta"
    date: 2003-11-09
    body: "Running it as root won't fix the control center for your regular user account.  Try running the kbuildsycoca command as the regular user you are signed in as after you make the symlink as root.  Good luck."
    author: "Z"
  - subject: "Re: Beta"
    date: 2003-11-10
    body: "Ooops.. sometimes things are too simple. It works. Thank you."
    author: "Eggert"
  - subject: "Re: Beta"
    date: 2004-02-24
    body: "Excuse me!!\nWhere i put the symlink?"
    author: "k18"
  - subject: "Hmm..."
    date: 2003-11-05
    body: "If I remove ~/.kde, what happens to my addressbook, calendar, etc?\n\nI don't want to enter all that data twice!"
    author: "TomL"
  - subject: "Re: Hmm..."
    date: 2003-11-10
    body: "You're right.  In any case make a backup copy first. \n\nBut: Backing up your valuable files regularly to an \nexternal medium (floppy, CVS, CD-R) is a good idea anyway.\n\n"
    author: "cm"
  - subject: "Re: Hmm..."
    date: 2003-11-28
    body: "it's ot important to remove da whole kdehome-directory (~/.kde) only the responsible files should be removed, e. g. to make kcontrol running, make the links discussed above and remove the file KDEHOME/share/config/kcontrolrc\n\ngreetings"
    author: "tizianoo portenier"
  - subject: "Re: Beta"
    date: 2003-11-05
    body: "OK. The ~/.kde directory should be removed or new KDEHOME must be created."
    author: "Andrey V. Panov"
  - subject: "Re: Beta"
    date: 2003-11-06
    body: "OK SuSe users,\n\nFirst install desktop-data-SuSE-8.2.99-61.noarch.rpm at the noarch directory.\nSecond execute following commands as root:\n\n#cd /etc/xdg/menus/\n#cp default_kde-information.menu information.menu\n#cp default_kde-information.menu information.menu.kde\n#cp default_kde-screensavers.menu screensavers.menu\n#cp default_kde-screensavers.menu screensavers.menu.kde\n#cp default_kde-settings.menu settings.menu\n#cp default_kde-settings.menu settings.menu.kde\n\nThat works for me like charm.\n\nLuis\n"
    author: "luimarma"
  - subject: "problems with konstruct"
    date: 2003-11-03
    body: "I've not been able to use konstruct for months now ... I've tried several times, and I always get errors with the mirror download.kde.org assigns to me. Does anyone know who I should write to about this? It's extremely frustrating!\n\nConnecting to download.kde.org[131.246.103.200]:80... connected.\nHTTP request sent, awaiting response... 302 Found\nLocation: ftp://ftp.lip6.fr/pub/X11/kde/unstable/3.1.93/src/kdebase-3.1.93.tar.bz2 [following]\n--19:24:21--  ftp://ftp.lip6.fr/pub/X11/kde/unstable/3.1.93/src/kdebase-3.1.93.tar.bz2\n           => `download/kdebase-3.1.93.tar.bz2'\nResolving ftp.lip6.fr... done.\nConnecting to ftp.lip6.fr[195.83.118.1]:21... connected.\nLogging in as anonymous ... Logged in!\n==> SYST ... done.    ==> PWD ... done.\n==> TYPE I ... done.  ==> CWD /pub/X11/kde/unstable/3.1.93/src ...\nNo such directory `pub/X11/kde/unstable/3.1.93/src'.\n"
    author: "Ed Moyse"
  - subject: "Re: problems with konstruct"
    date: 2003-11-03
    body: "I have uploaded a new version of Konstruct which reduces the number of files you have to change to enforce the download from a specific mirror to one file (kde.conf.mk). Those mirrors omitting unstable/ should probably be excluded from download.kde.org rotation."
    author: "binner"
  - subject: "Re: problems with konstruct"
    date: 2003-11-04
    body: "I have the same problem with the new konstruct. There's the eunet server from NL, that not have the unstable version yet. I think it should be easy for the konstruct-constructors, to make sure of a alt-server or maybe event two alt-servers. That must be a easy change.\n\nCheers,\n\nJ"
    author: "Jayenell"
  - subject: "Re: problems with konstruct"
    date: 2003-11-04
    body: "When I tryed it, konstruct had the same problem. It sometimes tries different mirrors if you restart it, but it is boring to keep restarting it.\n\nHow about having it retry different mirrors automatically in the case of a failure?"
    author: "Stefan Heimers"
  - subject: "Re: problems with konstruct"
    date: 2003-11-04
    body: "Just add more mirrors (or duplicate the download.kde.org entry) in kde.conf.mk."
    author: "binner"
  - subject: "easier way"
    date: 2003-11-04
    body: "I just run it this way\n\nfor i in $(range 1 1000)\ndo\nmake install\ndone\n\nrange is a bash function I have."
    author: "TomL"
  - subject: "Re: easier way"
    date: 2003-11-04
    body: "> range is a bash function I have\n\nwhich presumably does the same thing as `seq 1 1000`, a unix command everyone (?) has...."
    author: "Bertie"
  - subject: "You are right!"
    date: 2003-11-04
    body: "I guess I've had it for so long."
    author: "TomL"
  - subject: "theorz"
    date: 2003-11-03
    body: "Wow!\n10,000 bugs closed\n2,000 user wishes processed\n30 new applications\n\nThis release is shaping up to be amazing.\n"
    author: "theorz"
  - subject: "Re: theorz"
    date: 2003-11-03
    body: "I'm not knocking the KDE people in any way, but...\n\n1) 10,000 bugs closed means that there were at least 10,000 bugs to begin with.\nOne should wonder about the methodologies at work in the software production: the thinking, the planning, the workflow, etc. You should ask if the process behind the software can be improved so that, without too much loss in development time, these bugs can be prevented _before_ they happen, not fixed after they happen.  Software development is one area where prevention certainly is better than cure. (But cure is better than nothing at all, and <sick joke>nothing at all is better than M$...<duck/></sick joke>)\n\n2) 2,000 user wishes processed doesn't indicate how well the needs of the user were thought about to begin with.  You could increase this figure in the short term by brainstorming for user wishes in advance, plus a little asking around, and then not implementing the wishes until they land in the 'user wish' inbox, at which point you wheel the feature out and count it as another 'user wish' processed.\n\n3) 30 new applications.  What is/isn't an application?  What do they do?\nSurely better KOffice (exceeding OOo everywhere), with a complete GIMP+Cinepaint equivalent (using the new KDE interface stuff... only 1 new application so far) and deeper framework inprovements would be better than the 'new application' count?\n\nWhether the release is shaping up to be amazing has nothing much to do with the numbers, rather it has everything to do with what those numbers are referring to.  Numbers can, and routinely are massaged, fiddled, adjusted, corrected, and forced to lie under various forms of torturous duress.  Don't listen to numbers alone.\n\nThat, ladies and gentlement, concludes today's lecture."
    author: "John Allsup"
  - subject: "Re: theorz"
    date: 2003-11-03
    body: "A \"bug\" in Bugzilla is not the same as a bug anywhere else. Conservatively, more than half of those bugs were enhancements, feature requests, TODOS, or duplicates."
    author: "Hydralisk"
  - subject: "Re: theorz"
    date: 2003-11-03
    body: "\"nearly 10,000 bugs closed\"\n\nI think the intention is good but gives the impression of too many bugs...\nLike John said, is good to correct them but is better that they don't exist at all.\n\nMoura"
    author: "Mojo_risin"
  - subject: "Re: theorz"
    date: 2003-11-03
    body: "10000 bugs is only one bug for every ~400 lines of code.  That's incredible in comparison to industry standards.\n"
    author: "George Staikos"
  - subject: "Re: theorz"
    date: 2003-11-03
    body: "once, i used to write so many lines of comments as well!\n\n *looool*\n   katakombi >8^)"
    author: "katakombi"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "It's actually even less per line of code since usually people compare all collected bugy reports to the current amount of lines, while they ignore the fact that a lot of rewrites/additions/removals predate the current number of lines. According to http://libresoft.dat.escet.urjc.es/cvsanal/kde-cvs/ the current amount of about 4 million lines has be reached through about 60 million lines (excluding websites and qt-copy) of added and rewritten code in the last 6 1/2 years."
    author: "Datschge"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "It was 1 bug per 400 lines *fixed* bugs, not total bugs. Who knows how many bugs are still lurking in all those lines of code. For a mature project like KDE which does not have to worry about strict deadlines, I'd say it's just about normal. "
    author: "Chefren"
  - subject: "Re: theorz"
    date: 2003-11-03
    body: "I think those numbers are simply interesting, in a strange sort of way. It just sounds impressive that 10,000 bugs have been processed; it gives an idea of the scale of the project. The announcement lists plenty of specific improvements and additions, and doubtless when 3.2 final comes out we will see several documents explaining it all in depth."
    author: "Tom"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "John, I am guessing that you are a PM? Yes?"
    author: "a.c."
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "PM=?  \n\nPrime minister? Nope.... that'd be a B*****d control freak called Tony.\n\nPure Mathematician? Yes.  (See e.g. my profile on slashdot.)\nI tend to share my pedantry with the mathematical logicians, which is\nunderstandable given my research area.  (specifically, I'm a postgraduate student working in models of Peano Arithmetic.)\n\nOther PM? please advise.  (I've given up guessing.)\n\nBut yes: pure mathematics does tend to make you think about what numbers are, what they mean, and what they can also mean, given someone's intended meaning.\n(Undergraduate statistics books quite often have explanations of various scams\nthat work effectively on the unwary...)\n\nJohn."
    author: "John Allsup"
  - subject: "Bug count"
    date: 2003-11-04
    body: "I'm a pure mathematician, too. Furthermore I'm a KDE developer (KMail, to be precise), so I know exactly what those numbers mean with regard to KMail. We have loads of bug reports which are:\n- duplicates of already reported bugs (cf. http://bugs.kde.org/duplicates.cgi)\n- invalid, i.e. distribution specific bugs, user error, bugs in the libraries we use, etc.\n- not reproducible (so it's not clear whether there's really a bug)\n- missing features which are reported as bugs\n\nSome numbers (bug counts for kmail since 2003-01-28, the day KDE 3.1 was released):\n- total number of new bug reports (excluding wishes): 855\n- total number of closed bug reports: 684\n- closed as FIXED: 213\n- closed as DUPLICATE: 170\n- closed as WORKSFORME: 163\n- closed as INVALID: 115\n- closed as WONTFIX: 20\n- closed as LATER: 3\n- still open: 171\n\nSome more numbers (bug counts for all of KDE since 2003-01-28):\n- total number of new bug reports (excluding wishes): 9788\n- total number of closed bug reports: 6866\n- closed as FIXED: 3052\n- closed as DUPLICATE: 1267\n- closed as WORKSFORME: 1304\n- closed as INVALID: 984\n- closed as WONTFIX: 218\n- closed as other: 41\n- still open: 2922\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Bug count"
    date: 2003-11-04
    body: "great numbers... maybe we should enlist you to write the pure math version of the release announcements ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "The methodology goes as follows:\n\nDeveloper works on feature, fix, cleanup, whatever.\n\nDeveloper commits changes for review.\n\nOther developers or users test changes, either email bugs or submit bug report.\n\nDeveloper responds, fixes bugs.\n\nrinse... repeat.\n\nSounds pretty good to me.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "That's a very simplistic view. \n\nWhere is the deep, large-scale thinking as to how everything goes together?\n\nWhere is the end-user-pov thinking, looking not at particular features or whatever but on how they are going to use the UI as a whole to get done whatever they want to get done?\n\nBefore you even get to the point of working on features, the large scale thinking lets you prioritise things so that the important features are well planned (rather than hacked up out of comparatively spur of the moment interest) \nand have far fewer bugs than average.  Also, thinking about the workflow of the people doing the programming (this is a huge thing to think over in its entirety, and many books have been written on the various aspects of it,)\nwhat procedures allow the amount of debugging/correction to be reduced?\n\nPart of the problem of development is that, when it actually comes round to writing things, you have to do so a feature at a time (well, pretty much anyhow.) But thinking and planning things according to the features doesn't tend to produce the best results.\n\nConsider building a car, starting with the best tyres you can afford, and that really cool supercharger your mate has in his hot-hatch.  If you go along that road, you'll be lucky to end up with something that even works.\n\nWhat you describe as the methodology is only really appropriate if you're working on an already planned out project, following the guide to what features are/aren't needed, where they go and what they do.\n\nIt is only recently IMO (i.e. post GNOME 2 and KDE 3) that the two main competing OSS desktops (GNOME and KDE) have really started to come around to this kind of thinking and planning.\n\nJohn."
    author: "John Allsup"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "Interesting perspective. I think you underestimate the work involved in the few lines that I used to describe the process.\n\nThere are two ways of going about this. One is to try to figure out the requirements, come up with an infrastructure design, hash out the implementation details for a few years, then start writing code from a spec. For a project like KDE, this is an impossibility. For one thing, how do you attract other developers except with working or almost working code?\n\nThe second way it to write it. Find what doesn't work, throw it out, rewrite it. Repeat. 7 years later you have a mature and stable desktop, with few bugs. Actually, you have about 3 versions of a stable workable desktop. There are a few core developers that are still around, but many have come, contributed, then moved on. User interface stuff is details, getting basic functionality is the bulk of the work. A few months ago there was a lengthy discussion here about configuration, how complicated etc. it is. Someone banged out a 4 or 5 liner that produced a simple narrowly focussed configuration dialog, that worked. Literally. To say there isn't a good solidly designed infrastructure behind that is plain ignorance. That is one example. Khtml was written, the rewritten. Features, as in capabilities to view web pages built with unusual technology, are added to the basic engine. There is a design, a good design according to the apple people, that is extensible. Another example is the intercommunication mechanism. At one time KDE used CORBA, but it was found to be too slow and heavy for the purpose. So based on what they learned, they designed a new one, and wrote it in rather short order.\n\nI would say the second way comes up with better real world design. I work in a construction related industry, and know the value of design and specifications. But it is a very different industry dealing with very different constraints. First, you can't build, throw out and rebuild. Actually you can, but progress takes generations. Second, very seldom does anything new come along, I mean really new and unprecedented. Most technologies I deal with have been around since the 16th and 17th century, at least in someone's mind. The new stuff is new / faster / cheaper ways of doing the same thing. With software, this is not at all the case. Some of what the KDE folks have done is different than anything anybody else has done. It is truly new ground. How does one formally design that, other than writing it and seeing how it works? And throwing it away if it doesn't? Or letting someone else look at it to improve or trash it?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "Dear John,\nbefore you continue rambling about how software development should work in your opinion I'd like to make you aware that in the last 7 years KDE's development model transparently grew without any big break, without any kind of top down rules/rulers. Of course there *is* a development model as well as a grown community culture. For an introduction to those I suggest you to read http://www.kde.org/whatiskde/devmodel.php as well as the other related pages in that section. If after reading those you think we are doing it totally wrongly anyway I suggest you to look for other projects which fit better to you and may listen to your suggestions.\n\nThank you.\nRespectfully, Datschge"
    author: "Datschge"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "well put Datschge! if something works really well and is a huge success, you shouldn't come along and say it doesn't work; if its churning out good product, efforts are better put to enhancing the mechanism rather than trying to turn the boat around for no reason at all."
    author: "Aaron J. Seigo"
  - subject: "Re: theorz"
    date: 2003-11-06
    body: "So, you shouldn't work. Nature proves best that this IS a possible way. Sometimes it's better if someone comes along with a vision or so, i agree. But there ARE contributors with such visions. See how Bernd Gehrmann started with a new kdevelop (gideon), and now it's the one kdevelop none of us can wait for..\n\nSo where is the problem?\n\nBest wishes\n\nTim\n "
    author: "Tim Gollnik"
  - subject: "Re: theorz"
    date: 2003-11-07
    body: "I don't think that it works that way.\n\nAND, that is why it isn't working.\n\nBut, perhaps I should see if my latest bug report *actually* is fixed in 3.2 before I go into this in detail.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "first off, that isn't 10,000 unique bugs but 10,000 bug reports. some of those were undoubtedly duplicates, some of those were against development versions that were never in production releases. however, many were against older versions. seeing as there's nearly 4 million LOC and people like to report every fiddly little bug they come across, i think that's not too bad at all. perhaps you like to show us your million+ LOC complex multi-program GUI project that's been over with a fine tooth comb by thousands as a comparison?\n\nwith regard to user wishes, you are exactly right in that it doesn't say anything about the final outcome of those wishes. some were duplicates, some were denied. still, many were implemented. how many? i don't know for sure since i haven't counted them (anyone know how to get bugzilla to spit out such a report?). at the same time, it's almost irrelevant. what IS relevant is that these reports from users get read, considered and dealth with. that means there is a real user->developer communication going on.\n\nas for your \"sand-bagging the user wishes\" conspiracy theory, believe me when i say that those in the KDE project have better things to do with our time. honestly. most of us get our enjoyment from writing code, not dicking with statistics.\n\nyou asked what is and isn't an application. here's a short list of new apps and components in 3.2: juk, devices applet (kicker), universal sidebar (kicker extension), khotkeys2, kdialog, ksplash(ml), kwallet, kmag, kmousetool, mouth, Konqi Mozila-like web sidebar, vimpart, plastik, Kig, KBruch, KGoldrunner, kopete, kontact, kcachegrind, umbrello, kgpg, kdelirc, kpdf, quanta's kommander, Kicker menubar applet, fsview, kcmrandr + systemtray tool, kcmuserinfo, kcmgamma, konq-plugins/auto-refresh, konq-plugins/crash, kuiviewer/ui thumbnails, kwifimanager, ksvg, a couple of new trivial ioslaves, libkcddb .... i'll bet i've even missed one or two in that list still. \n\nbut remember, this is a beta announcement. wait for the final release announcement for more detailed coverage of the whole thing. \n\nand btw, KOffice has gotten better (though it isn't counted in the KDE Desktop release) and in some places does exceed OOo. look up the word \"maturing\".\n\nas for the GIMP+Cinepaint equiv you mention, hey, great, you've found a place KDE doesn't have a better app. to which i'd say where's Scribus, the K3B, KStars, or the KSVG? and just wait as apps like Kexi get into shape. i could go on, but i think you get the point, which is that both sides have holes in their application coverage. and without taking anything away from the great efforts made by the Gtk+/GNOME folks, i can confidently and honestly say that KDE is filling its application gaps at a terrific pace.\n\nyou are also right that the KDE 3.2 release has everything to do with what the numbers are refering to rather than the numbers themselves. you seemed to question whether the numbers actually referred to anything, however; let me assure you they do.\n\nbut most people don't have great attention spans, and most people don't like to read. knowing that, one of the things to remember when writing announcments and other such PR bits is that while one wants to offer substance, you can only offer so much before it becomes wasted, and even retrograde, effort. ergo the shorthand and trotting out, hopefully impressive, summary concepts and numbers.\n\nof course, if you show me factual errors in the piece, i'll be happy to correct them. as it is, i stand by everything in that article."
    author: "Aaron J. Seigo"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "gah.. i really ought to Preview rather than just add ;-)\n\nwhen i said \"to which i'd say where's Scribus, the K3B, KStars, or the KSVG?\" i of course meant \"to which i'd say where's the Scribus, K3B, KStars, or KSVG apps for Gtk+/GNOME?\""
    author: "Aaron J. Seigo"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "> as for the GIMP+Cinepaint equiv you mention, hey, great, you've found a place KDE doesn't have a better app.\n\nHowever it does appear that there is once again work on paint apps as well as Krita so these holes could become more a matter of choice of apps. Not having the Gimp's hideous UI for editing several files is such a big plus for me I'd live with less features. I mean when your picklist display is limited by your screen resolution and you can't find anything without the taskbar you're talking UI from hell.\n\n> i'll bet i've even missed one or two in that list still \n\nYes, in the Quanta module there are kxsldbg and KFileReplace. KFileReplace is just included as a kpart plugin at this time but it shows up on konqueror toolbars and is the coolest multi file find and replace ever."
    author: "Eric Laffoon"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "> Yes, in the Quanta module there are kxsldbg and KFileReplace. KFileReplace is just included as a kpart plugin at this time but it shows up on konqueror toolbars and is the coolest multi file find and replace ever.\n\nAre there any way to remove the kfilereplace icon on the Konqueror toolbar, without removing the other two listview icons?\nDon't get me wrong I like it on Quanta."
    author: "Josep"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "I don't know how can you do this. It seems that Konqueror puts every KPArt that handled inode/directory on the toolbar. Cervisia is also there if you have kdesdk installed. Ask it on kde-devel or some other mailing list and if nobody know you should fill a wish item for kdelibs or Konqueror. I'm not sure which one is the best.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: theorz"
    date: 2003-11-05
    body: "Yeah I was actually a little surprised to see this myself when I opened konqueror after installing it. Then again I'm not sure how I feel about Cervisia being there as I usually use the application or as a plugin in Quanta. My personal preference would be to complete the upgrades to the stand alone app for KFileReplace, but for now we will see what the options for the toolbar are at this time."
    author: "Eric Laffoon"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "> i'll bet i've even missed one or two in that list still \n\nYes. I'm quite upset that you didn't mention kclock.kss!\n\nm.  ;-)"
    author: "Melchior FRANZ"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "/me quickly adds that to the list\n\nno. seriously. and the binary clock, speaking of clocks."
    author: "Aaron J. Seigo"
  - subject: "Re: theorz"
    date: 2003-11-05
    body: "\n> I'm quite upset that you didn't mention kclock.kss!\n\nYes! It immediately became my default screensaver! :)\n\nI might have missed KDevelop3 in that list, which is the first official release of an app that has been in the making for years.\n"
    author: "anonon"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "with regard to user wishes, you are exactly right in that it doesn't say anything about the final outcome of those wishes. some were duplicates, some were denied. still, many were implemented. how many? i don't know for sure since i haven't counted them (anyone know how to get bugzilla to spit out such a report?).\n\n----\n\n*cough* Coolo asked me to add statistic scripts like this in vein of like the existing \"weekly summary\" and \"old bug\" reports. I'm kinda late, but promise that I'll work on it."
    author: "Datschge"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "I'll give a long answer to this.  It will probably produce more questions+retorts than\nanswers, but here goes.\n\nI stated quite clearly at the start of my previous comment.\nI AM NOT KNOCKING KDE IN ANY WAY. I am not. (Not much anyway.)\n(You seem to be reacting as if I were.)\n\nThe original point was intended to stress that the numbers (namely the 10000, 2000 and 30) aren't that important, and those that are impressed by the numbers should try not to be. Instinctively, when faced with things like '90% fat free' and 'and amazing 1000 combinations', one should ask what these numbers are talking about.  Don't listen to the numbers all that hard.\n\nIn the particular case of the numbers in the comment I replied to, it is certainly not the case that more=better.  Neither is it the case that less=better.  All that can be judged from the numbers is a rough amount of effort, and even then you need to be familiar with what the terms 'bug', 'user wish' and 'application' mean here.\n\nMy response to the parent comment was mainly aimed at the 'Wow! look at the big numbers' aspect, it was certainly NOT aimed at the actual article.  I read the article, was impressed by what KDE had achieved with the current pre-release, but saw someone spouting numbers and thought 'here we go again... I'll just throw something back at the statistics this time.' Maybe I was falling for a troll, but I don't think the original comment was one.\n\n> as for your \"sand-bagging the user wishes\" conspiracy theory, believe me \n> when i say that those in the KDE project have better things to do with our\n> time. honestly. most of us get our enjoyment from writing code, not dicking\n> with statistics.\nDid I suggest such a conspiracy theory? I apologise for doing so if I did: that was not the intention. (It was a knee-jerk reaction to yet more empty statistics.) The point about an artificial way of increasing the 'use wishes' count was intended as a reasonably direct counterpoint to the 'more the merrier' attitude to the numbers.  Again, the numbers themselves don't say that much.\n\nPersonally, I think it's better to say 'this, that, the other, and approx. x many other user wishes accounted for' rather than 'x user wishes accounted for.' This is more informative and places less emphasis on the numbers.\n\nAs to the what is/isn't an application:\nThe easy way to answer this question is: 'One pile of code with a name=one application.' I'll offer an alternative view, taking the list in your comment.\n\nIn short, applications are things like Photoshop, GIMP, OpenOffice Writer, etc.\nKicker, Konqueror, etc. are features of the environment (i.e. what you have before you run any applications.)  A possibly controversial view, but I do not consider Konqueror to be an application, but an integral part of the underlying environment.\n\nApplets are applets: they are generally little gizmos that get something done, but are auxiliary to the main purpose at hand (whatever it is you are doing at your desktop.) Personally, I consider things like WinAmp and friends to be applets: you have them lying around making life a little easier, but you don't _work_ in them.  You do work inside something like Quanta or KSpread.\n(If you are wondering where I'm coming from, this point of view starts from the dumb-user end of things, looks at what they are trying to accomplish, and thinks things from there.)\n\nNote that when I say 'framework', this is to be thought to be everything available (from KDE) to the developer as a tool to get the UI to do something for the user. Possibly 'environment infrastructure' would be a better term.\n\n* kwallet: should this be considered an application, or as an applet frontend to something (namely the functionality it provides) that should be and should be considered to be an integral part of the KDE framework.  I have thought for a long time that authentication management should be intimately intertwined with a framework such as KDE's.  My first meeting with it was when I saw the capability model of security (where there was fine grained, abstract authentication management, such that the authority to do something could be sent around in a controlled way.)  Right now I think it's an application, but longer term it should not be.\n\n*juk:  Again, this is an application.  Much of the functionality it provides, however, should not lie in the application.  Longer term, the ability to do things like keeping track of MP3's, forming playlists and such should be a feature of the underlying filesystem abstraction in the framework. I'll not\nelaborate on this very much here: I'll think this through a little better, read through the other stuff on storage frameworks + the obligatory how BeOS did it, and stick something in my blog.\n\nThat said, much of what juk provides as an application should be a trivial thing\nto build on top of the underlying framework.  It is not at the moment, and hopefully that will change in the future.\n\n*devices applet (kicker): clearly this is an applet.  I consider applets not to be applications (and I consider many applications to be applets in disguise... my view: others may or may not share it.)\n\n*universal sidebar (kicker extension):  As I see it, kicker is part of the environment, and this is an extension to it. (As you might guess by now, I do not consider plugins to be applications per se.)\n\n*khotkeys2, kdialog, ksplash(ml): part of the framework.\n\n*kmag: desktop gizmo.  You do not do work within kmag: it's something to help you when working within some applications.\n\n*as for kmousetool, mouth, Konqi Mozilla-like web sidebar, vimpart, etc. you should get the picture.\n\nOf the things you listed, I consider Kig and Kbuch to be applications, though whether they should be amalgamated into a larger, more general 'KDE Maths Education Assistant' is another question. KGoldrunner is a game (not IMO an application.) I'll count Kopete as an application, but again it should really considered part of the underlying environment. Kontact is an application.\nKcachegrind is certainly an application, as is umbrello. So far as new applications, according to my definition I count about 10 or 11 out of your list. (Again, I am not forcing this view on anyone, just offering an IMO important alternative view to what applications are and how to count them.)\n\nI like to view the application count as a rough indicator as to how much useful stuff there is for getting work done. (So 'three applications' means things like \n'KWord, KSpread and Quanta', not things like 'Kicker, Konqueror and KWallet')\n\nThe point is that the actual number of applications according to the 'pile-of-code+name=application' definition is not in itself an indicator of much except maybe effort.  In many ways, if the same amount of stuff was done by fewer applications, and instead by enriching the framework, the result would be better (and possibly less confusing for the casual end user.)\n\n> KOffice has gotten better (though it isn't counted in the KDE Desktop release)\n> and in some places does exceed OOo. look up the word \"maturing\".\nI'm familiar with the word maturing.  KOffice has come a long way, and has a long way to go. There isn't an office suite that doesn't.\n\n> as for the GIMP+Cinepaint equiv you mention, hey, great, you've found a place\n> KDE doesn't have a better app.\nAnd also a place where, if proper thought is given, KDE can do a better job (as an application.) Between GIMP and Cinepaint, they do a good job of managing the image data.  Getting that image management/manipulation/high-performance-display aspect abstracted into an efficient 'engine' so that a nice KDE appliation can be written around it is probably the best way to do things from the KDE point of view.\n(I know little about the state of the art with GIMP's development, so I don't know how much or how little GIMP's image manipulation engine is coupled to the UI.  Hopefully not that much.)\n\n>to which i'd say where's Scribus\nTechnically a Qt application last time I looked.\n\n>K3B\nI agree that this looks good.\n\n> or the KSVG?\nAn important and well needed part of the framework.\n\n> and just wait as apps like Kexi get into shape.\nThe caveat is that, as with all 'just wait for' things, you don't know what you'll get, you don't know whether or not the developers will get bored/fall out/etc.  There are, and have been many 'just wait for' things in the long history of free software that haven't come to anything. I sincerely hope Kexi is not one of those.  Database frontends tend to be important and useful these days.\n\n> i could go on, but i think you get the point,\nSo could I.  I tend to enjoy a good, reasonably informed and heated discussion, with sufficiently different points of view that the talking doesn't just amount to 'preaching to the converted.'\n\n> which is that both sides have holes in their application coverage.\nYou tend to suggest that there are two sides: surely there are <i>many</i> more (and many more even than that...)\n\n> ...by the Gtk+/GNOME folks\nI'm not here to start up a KDE vs. GNOME holy war.  (Please don't think that that is my intention: it is not.)\n\n>I can confidently and honestly say that KDE is filling its application gaps \n> at a terrific pace.\nBut 'filling application gaps' is not the be-all and end-all of producing a good UI environment. Surely it is better to do what you do very well at the expense of a few holes?\n\n> you are also right that the KDE 3.2 release has everything to do with what the > numbers are referring to rather than the numbers themselves.\nYes.  That was the main point.  The rest was a surrounding discussion (which I thought better to include than a tired quote such as 'there are lies...statistics.')\n\n> you seemed to question whether the numbers actually referred to anything, \n> however;\nMore accurately, I questioned how much meaning the numbers can actually have, giving a few examples as to how they can be misleading or inaccurate depending on how they are calculated and how people read them.  Many replies gave further weight to the fact that they can be easily misread and misunderstood.  This misunderstanding is why I tend to question the numbers and try to look straight past them.\n\n> let me assure you they do. But most people don't have great attention spans,\n> and most people don't like to read. Knowing that, one of the things to \n> remember when writing announcements and other such PR bits is that while one\n> wants to offer substance, you can only offer so much before it becomes wasted,\n> and even retrograde, effort. ergo the shorthand and trotting out, hopefully \n>impressive, summary concepts and numbers.\nThat is a problem.  It can be a big problem.  Salesmen tend to love using numbers to confuse people for this very reason (they take the numbers and don't have the attention span, motivation or whatever-else to think things through.) I guess there's an art to writing announcements that are deliberately informative (and aim for the minimum confusion) just as there is a whole industry based around doing quite the opposite!\n\nFinally, I am not questioning any factual errors in the piece.  I never was: I just don't consider the raw statistics to be very meaningful.\n\nJohn.\n"
    author: "John Allsup"
  - subject: "Konqueror is no application?"
    date: 2003-11-04
    body: "Konqueror is no application? But I guess Mozilla and Opera are. Where's the difference? Just because Konqueror is more integrated with the rest of KDE it doesn't count as application? Is KDE (whatever that is) an application?\n\nAnyway, as pure mathematician I won't dispute your definition for \"application\", even if you are pretty much alone with it.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Konqueror is no application?"
    date: 2003-11-04
    body: "It's an iffy one.  In its web browser mode, it quite possibly is.  In its file manager mode, it should be considered as part of the underlying environment.\nIt depends what you see as being built on what.  Basically, if you chop the KDE things into those bits that are 'built in' or at least appear to be 'built in' and those that are not, which side of the fence does Konqueror fall on? (Note that I would say that M$IE appears to be a 'built in' part of the Windows user environment, ignoring the question of whether it actually is.) \n\nWould you consider Konqueror as being built in to KDE? I would, and for my mind that disqualifies it from being called an application. (Equally, I would not call M$IE an application, so much as part of the user environment, which being (at least so far as appearances are concerned) integrated into the Explorer, it very much is.)\n\nAny good desktop UI environment will have something that manages files and is considered part of the desktop environment.  e.g. the Finder on MacOS, explorer on Windows, and the default file manager applications.  People expect these things to just 'be there', and they are.  For example, if you start with an empty KDE desktop and open a file window, do you consider yourself having opened any applications? In this context I would say no.  The only bit that picks it out as an application is when it is used as a browser, but even then one would consider Konqueror to be KDE's built in browser.\n\nThat said, I think the word 'application' is heavily overused.  There needs to be a terms for many things, and whether you split things up based on technical definitions (e.g. a KDE application is a program involving a class that extends KApplication) or UI-point-of-view centric ideas (such as a KDE application is a program above and beyond the basic environment that is designed to do some kind of task.)  Ok: this is not a good attempt at a qualitative definition, but you get the point.  Think first about how someone who knows nothing about how everything is put together will think about it.  That is the intuitive idea of what's what, and it makes a lot of sense to keep coming back to it when it comes to explaining things.  (This will make things easier when it comes to getting inexperienced people into using KDE: the terminology will make far more 'intuitive' sense far earlier on, and subsequent discussion may be less confusing.)\n\nIn any case, the point of view as I put it across was a little extreme in the 'this isn't an application' direction, but I thought it necessary to counterbalance the 'a kicker extension is an application' sort of thinking in the comment I was replying to.\n\nAs to Mozilla and Opera: I would consider them applications.  They tend to be all enclosing environments dedicated to a particular collection of tasks (i.e those involving using the internet.)\n\nI guess I'll have to think through my point of view of what's what, and try and put it down in writing.  It does make a lot of sense, honest, it just requires a little unlearning.  That said, part of it involves effectively having one set of definitions for the user point of view and another for the developer point of view.  Setting things down in a way that avoids ambiguity is something I've yet to do.\n\np.s. I also noticed why the 'Quality not quantity' title didn't appear--silly old me stuck it in the email box.  Damn, I'm getting old.\n\nJohn."
    author: "John Allsup"
  - subject: "Consider the 'naive' user's point of view"
    date: 2003-11-05
    body: "A slightly easier way of looking at my perspective on 'what is/isn't an application.'\n\nConsider a mac user who doesn't care about technical definitions or implementation deatils.  (We'll assume that its an old one that still\nhas something called a floppy drive.)\n  [He sits down at his computer, the desktop is 'empty' except for the usual icons and stuff.]\n  YOU: Do you have any applications open?\n  USER: No.  I haven't opened anything yet.\n  [He sticks a floppy in with his work on and 'opens the floppy' by double clicking on the floppy icon.]\n  YOU: Do you have any applications open?\n  USER: No.  I'm just getting around to that... I've got my work here, and\nI'm going to open it up in X\n  (here X represents his favourite application, e.g. Word, Photoshop, Quark)\n  YOU: But really, that folder window is an application: it's called the Finder.\n  USER: Huh? Well, if you say so.  But isn't it just my floppy disk with my files on?\n  YOU try to explain the details to USER.  USER gets confused.\n  USER: Well, personally, I don't care.  Applications are things that I\nopen my documents in.  Folder windows are, well, just folders.  They're\nnot application.\n\n  [Another time, another user, this time you don't try to confuse them by explaining what the finder really is. He sits down at his computer, the desktop is 'empty' except for the usual icons and stuff.]\n  YOU: Do you have any applications open?\n  USER: No.  I haven't opened anything yet.\n  [He sticks a floppy in with his work on and 'opens the floppy' by double clicking on the floppy icon.]\n  YOU: Do you have any applications open?\n  USER: No.  I'm just getting around to that... I've got my work here, and\nI'm going to open it up in X\n  (here X represents his favourite application, e.g. Word, Photoshop, Quark)\n  [USER double clicks on the Word document.  Microsoft Word opens and the document is displayed on the screen.]\n  YOU: Do you have any applications open now?\n  USER: Yes, obviously (feeling patronised.) I've got Word out with my document, see.\n  YOU: I see.\n  [USER now double clicks the Excel spreadsheet.  Microsoft Excel opens and the document is displayed on the screen.]\n  YOU: So, which applications do you have open now?\n  USER: Word and Excel.\n\nNote that the typical user will NOT consider things like the Finder to be an application.  (And most Windows users didn't/don't realise that the taskbar, folder windows and now web browser are all part of the (Internet) Explorer 'application'.)\n\nWhere the boundary gets a little iffy is things like Windows' integrated IE and KDE's integrated browser.  Now consider the 'casual user' sitting down at KDE.  Like the previous mac user, he/she does not care about technical details or implementation details, only getting his/her work done.\n  [He sits down at his computer, the desktop is 'empty' except for the usual icons and stuff, including the KDE bar at the bottom.]\n  YOU: Do you have any applications open?\n  USER: No.  I haven't opened anything yet.\n  [He sticks a Zip disk in with his work on and 'opens the disk' by double clicking on the mount zip disk icon.]\n  YOU: Do you have any applications open?\n  USER: No.  I'm just getting around to that... I've got my work here, and\nI'm going to open it up in X.\n  (here X represents his favourite application, e.g. Word, Photoshop, Quark)\n  YOU: But really, that folder window is an application: it's called Konquror: you can see its name on the title bar.\n  USER: Huh? Well, if you say so.  But isn't it just my zip disk with my files on? Why does it need to be called Konqueror: isn't just a window with my files in?\n  YOU: Ok, I agree, it's just a file window.  But see that address bar, try typing in the URL for a website in.\n  USER: He types in 'www.google.com' and google appears.\n  YOU: Now, do you have any applications open?\n  USER: I'm not sure.  It's sort of like a browser now.  Is it an application now that it's a browser, or is this what you meant when you said it's an application before?  It's an application that sort of disguises itself as the built in file manager when its not doing any web browsing.  But if that's the case, where are the 'built in' folder windows?  Are there any?\n  YOU: (now also confused) Well, Konqueror sort of IS the built in file manager, but it's also the web browser application...\n  USER: (still confused) I give up. Can I get on with my work now?\n\nYou see how it goes. It makes a lot of sense to go back to the basic intuitive view of how people see and expect computers to work, document that and work with it rather than against it (whilst learning is important, making it that little bit more effortless to understand what's going on can surely help most users, whether technically minded or not).\n\nAs such, there are two points of view: the 'enlightened' technical users' point of view, where they know what sort of programs need to be running in order for things to appear like an 'empty' intuitive desktop environment.  There is also the view of the 'naive' user who just sits down and clicks on what they want, not being interested in how the computer really works, only that it does.  They will see things differently.  Personally, I think the 'naive' view is a better one in this case, since it is easier to get a good picture in your mind of what should be going on, without getting clouded by implementation details.  (My view: others may not share it.)\n\nThanks everybody for the discussion, I suspect this thread has long since passed the point of being labelled a 'troll fest' in everybody's minds, which was never the intention.\n\nJohn.  "
    author: "John Allsup"
  - subject: "Re: Consider the 'naive' user's point of view"
    date: 2003-11-07
    body: "You know your artifical case you build around Konqueror is actually exactly the same with Explorer under Windows.\n\nThat being said you indirectly got a point, even though I'm very sure you aren't aware of that: the term 'application' and all related terms (take 'quit' for example) are way too abtract and technical for being included a usable desktop referring to metaphors. KDE already has been moving to a more task oriented (versus application centric) metaphor in the past, I'm sure this will continue and be intensified after the release KDE 3.2, hopefully also getting rid of the remaining inconsistencies.\n\n(The problem that commercial desktop will prefer application centrism due to marketing reasons will probable lead to KDE promotion 'incorrectly' keeping referring to 'applications' since this sadly is the only way branding really works nowadays. See http://www.newsforge.com/software/03/11/05/146225.shtml for a nice article about exactly this topic.)"
    author: "Datschge"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "You know you are splitting hairs here, way too many hairs actually. \n\nYou talk about applications, applets and shells as if it makes a difference to the end user whether Konqueror is one of those. (Btw. Konqueror is technically a shell for the KPart framework, but I don't see any user caring about that.) Where this does matter is the development, being able to share as much code as possible allows developers to save time, stream line their applications, focus on their purposes, and all that while not having to reinvent every little wheel and even keep consistency with all other appplications in that desktop environment, that's the purpose K desktop environment exist for after all. You could come and say KDE has no single applications, it's all libraries and other shared stuff, and all applications users are using are actually only shells. And you know what, I actually think such a state would be the ideal case for KDE.\n\nAnd don't come with an excuse that your only point was showing that statistics are worthless, everyone knows that, but you still took the time to churn up a lot of arguments unrelated to that point."
    author: "Datschge"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "> I like to view the application count as a rough indicator as to\n> how much useful stuff there is for getting work done.\n\nand then you discount 2/3rds of the useful stuff as not being applications, as if somehow that makes them less interesting because they are stand alone apps. i *could* be pedantic in my writing and label exactly what each thing is (apps / applets / kparts / libraries), but how would that be useful besides to make the announcements even longer (not a good thing ;). when i mentioned you should wait for the final release announcement for more details, i meant it.\n\nin particular your distinction between \"underlying environment\" and \"application\" is pretty irrelevant when trying to communicate the delta between this release and the last one to a general audience, which was the point of the announcement. you are correct that enriching the underlying environment can be a better option over stand alone apps, and as you noted many of the new items in 3.2 do exactly that (from KWallet to KHotKeys to libkcddb to....). i think what you say about emphasis on framework, results not just efforts, etc align with the technical reality that is KDE. but most people (e.g. the target audience for this article) don't have the technical background to understand those shades-of-grey(-but-important) differences. when i've got my writer's hat on, i try to write to a specific audience, and this time it isn't you or me, but the average KDE user =)\n\nimagine if KDE actually spoke to the masses in a language they understood and enjoyed.\n"
    author: "Aaron J. Seigo"
  - subject: "Re: theorz"
    date: 2003-11-05
    body: "I've always enjoyed your writing Aaron. However after a few talkbacks your responses start to look like a really long compile string. Reading this reminds me something... Didn't we get to be friends over an argument we had on the Dot? It's good not to be the only hot blooded guy in KDE. Keep up the good fight man. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: theorz"
    date: 2003-11-05
    body: "haha.. yeah, i suck at talkbacks. my informal writing is usually way too verbose and expository of my internal modes of thought to be generally useful =) i also forget to use light-hearted words and phrases and come off sounding amazingly serious.\n\nvideo would be way better, as i'm a far better speaker than i am a conversational writer.\n\nbtw, the new Quanta smokes... yum yum!"
    author: "Aaron J. Seigo"
  - subject: "Re: theorz"
    date: 2003-11-06
    body: "> haha.. yeah, i suck at talkbacks. my informal writing is usually way too verbose and ...\n\nYeah man, whatever dude. ;-) Hey I really do love your writing though. I remember a Linux magazine article where I suddenly experienced a rush of excitement as I realized it wasn't just warmed over newbie fluff, but there were things I didn't know about KDE... Who was this guy... Aha! Nice work!\n\n> video would be way better, as i'm a far better speaker than i am a conversational writer.\n\nAnd in person would be even better as I'm trying to stay away from video beer.\n\n> btw, the new Quanta smokes... yum yum!\n\nThanks! That's high praise indeed. Best of all we're just getting up a head of steam. I'm already dreaming about what 3.3/4.0 will look like. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: theorz"
    date: 2003-11-04
    body: "Hi,\n\nlet me compliment you for your obvious intelligence. It's a refreshing read.\n\nThis is not ironic, but: You being a mathematican and probably even native English speaker is a good excuse for declaring applet and application two different things. It is in the style of people who love to be right. \n\nKDE is a framework. As such many of its apps are services as well as stand-alone applications. Your judgement by the GUI can be best demonstrated by asking you what Apache is. An applet or an application. Last time I thought of it, that was a application, but you are free to define as you like. For communication it's more practical to agree on definitions though.\n\nAs to the numbers. Why do you imply that everybody is unaware of how the process of development of KDE works. Most people here know what bug reports are, what wishes mean and so on. Most people here are even part of the process. We enjoy the numbers, because we know what they mean.\n\nYour implication of absence of planning and design is insulting to those who did. The examples were listed. It's no doubt that e.g. Linux 0.1 was really really bad design. To some it seems now it has the best design of all.\n\nI assume you fail to understand how you need bad code to attract people willing to improve it. Development of software is a social process not a mathematical. Esp. with Free Software. It may be true that a lot of individual time is wasted. But that's only so to explore all possibilties. What you demand to happen has an example: GNU Hurd. It took many years to design and probably already next year, it may support virtual memory or filesystems larger than 1G.\n\nGNU Hurd is one of the best planned products and a complete failure compared to Linux that just happened to become nearly useful.\n\nThink about it and share your opinion with me. This bit about intelligence was really not ironic. :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: theorz"
    date: 2003-11-05
    body: "> I'll give a long answer to this. It will probably produce more questions+retorts than\n\n answers, but here goes.\n\n\n\nI have to give you credit there. Then again, it diminished the point doesn't it? ;-)\n\n\n\n> I stated quite clearly at the start of my previous comment.\n\n I AM NOT KNOCKING KDE IN ANY WAY. I am not. (Not much anyway.)\n\n\n\nAnd when producing the long answer it's good to be clear... sort of. ;-)\n\n\n\n> The original point was intended to stress that the numbers (namely the 10000, 2000 and 30) aren't that important, and those that are impressed by the numbers should try not to be. Instinctively, when faced with things like '90% fat free' and 'and amazing 1000 combinations', one should ask what these numbers are talking about. Don't listen to the numbers all that hard.\n\n\n\nSo in simple terms we can reduce this to that point, agree and say \"so what\". As Aaron pointed out the alternative is to list the items, which with those numbers is inherently ridiculous. Now if this was being disseminated by corporate conglomerate \"X\" with intent to mislead and stick their hand in my pocket as opposed to stand up programmers looking for a way to summarize I think it would be a lot more valid. As it is I think one of my favorite programming phrases applies which says basically abstraction is knowing what you don't have to pay attention to. Some details are in fact counter productive.\n\n\n\n> Personally, I think it's better to say 'this, that, the other, and approx. x many other user wishes accounted for' rather than 'x user wishes accounted for.' This is more informative and places less emphasis on the numbers.\n\n\n\nSure, and to be fair you mention them all or... you say how many there are after giving some example so that it gives the reader an idea of scope. Just because you can lie with statistics doesn't mean all statistics are lies.\n\n\n\n> I like to view the application count as a rough indicator as to how much useful stuff there is for getting work done. (So 'three applications' means things like \n\n 'KWord, KSpread and Quanta', not things like 'Kicker, Konqueror and KWallet')\n\n\n\nThis is the second time you mentioned Quanta in your talkback. That gets you enough points with me that I now agree with everything you said. ;-)\n\n\n\n> Finally, I am not questioning any factual errors in the piece. I never was: I just don't consider the raw statistics to be very meaningful.\n\n\n\nMaybe it's a matter of personal taste. In my business endeavors I can think of few things that have been more useful to me in assessing and planning. Then again that's not the same as a release but maybe I'm geeky enough to to get a kick out of it."
    author: "Eric Laffoon"
  - subject: "screenies"
    date: 2003-11-03
    body: "screenshots where?"
    author: "ac"
  - subject: "Re: screenies"
    date: 2003-11-04
    body: "Try <a href=\"http://www.prism.gatech.edu/~gtg990h/kde_32_screenshots\">\nhttp://www.prism.gatech.edu/~gtg990h/kde_32_screenshots</a>"
    author: "Rayiner Hashem"
  - subject: "Re: screenies"
    date: 2003-11-05
    body: "Is it just me or has the Mac-like menu swallower at the top of some of the screenshots now an actual applet? So that I can add other buttons/applets to the same panel?"
    author: "Bart"
  - subject: "Re: screenies"
    date: 2003-11-05
    body: "Yes, it's an applet now. And yes, it's cool. =o)"
    author: "Datschge"
  - subject: "Re: screenies"
    date: 2003-11-05
    body: "yup, that's the general idea =)"
    author: "anon"
  - subject: "Re: screenies"
    date: 2003-11-09
    body: "Curious as to what font is being used there.  It looks great, and I can never get my desktop to look quite that clean with the fonts.  Any tips?"
    author: "Tim_F"
  - subject: "Kicker loses tray icons when crashing"
    date: 2003-11-03
    body: "Does anybody know if bug 66371 has been corrected ?\nIt's really annoying :-/\n\nhttp://bugs.kde.org/show_bug.cgi?id=66371\n\nAnyway, very good announcement !"
    author: "Macolu"
  - subject: "Re: Kicker loses tray icons when crashing"
    date: 2003-11-03
    body: "Looks like there was a bug in the bug database.  It's marked closed as a duplicate of another bug (which it is), which is itself marked closed as a duplicate of another bug.  But that third bug does not seem to have anything to do with the systray bug that you're describing.\n\nIt may have been fixed, may not have been.  I'm not running CVS, or I'd check and reopen the bug if necessary.  It looks like an accident, but may not be (that third bug *could* be the same)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Konstruct compilation problem?"
    date: 2003-11-04
    body: "Anyone have any luck building it with Konstruct?  It appears to be failing for me in libs/arts/work/arts-1.1.93/mcop (debug.cc, line 789).\n\n\nIn file included from libmcop_la.all_cc.cc:35:\ndebug.cc: In function `char *arts_strdup_vprintf (const char *, void *)':\ndebug.cc:789: `vasprintf' undeclared (first use this function)\ndebug.cc:789: (Each undeclared identifier is reported only once for\neach function it appears in.)\nasyncstream.h: At top level:\ndebug.cc:491: warning: `gsize printf_string_upper_bound (const gchar *,\nint, void *)' defined but not used\nmake[6]: *** [libmcop_la.all_cc.lo] Error 1\nmake[6]: Leaving directory `/scratch/kde-3.1.93/konstruct/libs/arts/work/arts-1.1.93/mcop'\nmake[5]: *** [all-recursive] Error 1\n\n\nIt appears to be related to \"HAS_VASPRINTF\"...  Any advice is greatly appreciated!\n\nThanks,\nCharlie"
    author: "Charlie"
  - subject: "Re: Konstruct compilation problem?"
    date: 2003-11-04
    body: "Try this..\n\ncd /scratch/kde-3.1.93/konstruct/libs/arts/work/arts-1.1.93\n./configure\nmake install\n{go back to the meta/kde directory and continue on with 'make install'"
    author: "David Hiltz"
  - subject: "Re: Konstruct compilation problem?"
    date: 2003-11-04
    body: "This likely installs to a wrong prefix! Removing \"--enable-final\" from libs/arts/Makefile and calling \"make garchive clean install\" while being in the libs/arts/ directory is better."
    author: "binner"
  - subject: "WOhoO!"
    date: 2003-11-04
    body: "I am definittely looking forward to KDE 3.2 come early February! Great works!\n\nAlso, my browser gives a 28 page printout when i look at the features guide =0"
    author: "Alex"
  - subject: "_Unwind_Resume_or_Rethrow@GCC_3.3"
    date: 2003-11-04
    body: "I am consistently getting compilation errors from the gcc-3.3.x series compiler.  Has anyone encountered this problem?\n\np2sam-pc:~/software/src/konstruct/libs/arts/work/arts-1.1.93/soundserver$ make\n/bin/sh ../libtool --silent --mode=link --tag=CXX g++  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -O2 -I/home/p2sam/software/kde3.2-beta1/include -I/usr/X11R6/include -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -O2 -pipe -I/home/p2sam/software/kde3.2-beta1/include -I/usr/X11R6/include -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -O2 -pipe -I/home/p2sam/software/kde3.2-beta1/include -I/usr/X11R6/include -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -O2 -pipe -I/home/p2sam/software/kde3.2-beta1/include -I/usr/X11R6/include -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -O2 -pipe -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common  -ftemplate-depth-99   -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -L/home/p2sam/software/kde3.2-beta1/lib -L/usr/X11R6/lib -o artsd  artsd.all_cc.o  libsoundserver_idl.la ../flow/libartsflow.la ../mcop_mt/libmcop_mt.la\n/opt/gcc-3.3.2/lib/./libstdc++.so: undefined reference to `_Unwind_Resume_or_Rethrow@GCC_3.3'\ncollect2: ld returned 1 exit status\nmake: *** [artsd] Error 1\np2sam-pc:~/software/src/konstruct/libs/arts/work/arts-1.1.93/soundserver$ \n"
    author: "Pedro Sam"
  - subject: "Re: _Unwind_Resume_or_Rethrow@GCC_3.3"
    date: 2003-11-04
    body: "i had the same Problem (Ark linux) it comes from a bad gcc build/rpm (old library versions laying around or someting like that) just download the latest gcc version from http://www.gnu.org/software/gcc/gcc.html and do the usual stuff\n\n./configure --prefix=/usr\nmake bootstrap\nmake install\n\nit worked for me and doesn't take long (compared to the complete kde build ;-)"
    author: "Jo"
  - subject: "Re: _Unwind_Resume_or_Rethrow@GCC_3.3"
    date: 2003-11-04
    body: "ahh, thanks for the response.  This problem had been plaguing me for months.  Would you happen to know of a way to install both versions of gcc cleanly without blowing away the default system compiler?\n\nthanks,\nPedro"
    author: "Pedro Sam"
  - subject: "Re: _Unwind_Resume_or_Rethrow@GCC_3.3"
    date: 2003-11-04
    body: "you could us a different prefix for example /usr/local/gcc and then create some aliases to that location if you want to uses the new gcc. (alias gcc='/usr/local/gcc/bin/gcc' - don't forget the others g++, cc, etc.......) and you have to add /usr/local/gcc/lib to your /etc/ld.so.conf.\n\nbut why would you want to do that ? if this error occurs your gcc suite is broken. the best would be to completely replace it with your own build - there won't be any binary incompatibilitys if that's your concern."
    author: "Jo"
  - subject: "Great Read Aarom!"
    date: 2003-11-04
    body: "indepth, and well written, but I did find some small errors,s o maybe you can correct them later.\n\n\"The only way to truly appreciate this release is to experience it first hand, but here are a few of the highlights anyways...\"\n\nshould be\n\nThe only way to truly appreciate this release is to experience it first hand, but here are a few of the highlights anyway...\n\nAlso, if KDE wants to get mroe donations and developers, which it definitely needs, it NEEDS TO BE MORE ASSERTIVE! KDE NEEDS BETTER WAYS TO ENCOURAGE PEOPLE TO DONATE! GNOME IS DOING AN OUTSTANDING JOB AT THIS AND WE SHOULD LEARN FROM THEM.\n\nI've filled a wishlist about many ways that KDE ould encourage its users to give back a little something. Please check it out and vote for it if you agree, this is a priority.\n\nhttp://bugs.kde.org/show_bug.cgi?id=63868\n"
    author: "Alex"
  - subject: "Re: Great Read Aarom!"
    date: 2003-11-04
    body: "i agree with you that it would be an very worthy goal to have a fundraising drive with clear goals for KDE. that's a big job, however, and probably not the sort of thing that most of the people involved with KDE are very good at. i think that Open Source projects like KDE could learn quite a bit from organizations like NPR and PBS, but i also think we really need some fundraising savvy people who are personally invested (as in they want it to succeed) to spearhead such an effort.\n\noh, and thanks for yours (and everyone else's) kind words re: the article."
    author: "Aaron J. Seigo"
  - subject: "Re: Great Read Aarom!"
    date: 2003-11-04
    body: "To donate to KDE please visit <a href=\"http://www.kde.org/support.html\">http://www.kde.org/support.html</a>"
    author: "Waldo Bastian"
  - subject: "Waldo"
    date: 2003-11-05
    body: "SUSE is set to be acquired by Novell by January though a $210,000,000 deal as of course you already know since you work there.\n\nDoes that mean SUSE will change its desktop preference, rewrite YAST in GTK, quit sponsoring KDE, switch everyoen to GNOME related technologies etc. If that happens than I have just lost my favourite distribution ;(\n\nI also think it would make more sense to keep SUSE the way it currently is, or they might screw it up as they have other things in the past when they tried to change it too radically. If everything becomes GNOME, not only is this uncompetitive environment goign to hurt Linux, but it will mean that ther ewon't be much difference between Redhat and SUSE and I definitely would not have any incentive to buy it anymore, but I do like 9.0 a lot, even though I think the real stellar releases will come in 2004 when linux 2.6, samba 3, KDE 3.2, KDevelop 3, OpenOffice.org 2, GIMP 1.4, GNOME 2.6, Xfree 4.4, etc. can all be incorporated.\n\nAny information on this, or has hell just begun?"
    author: "Alex"
  - subject: "Re: Waldo"
    date: 2003-11-05
    body: "> Does that mean SUSE will change its desktop preference, rewrite YAST in GTK,\n> quit sponsoring KDE, switch everyoen to GNOME related technologies etc.\n\nThere are no indications of that. Novell expressed in general terms that it very much likes what SUSE is doing and wants SUSE to continue in its current direction.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "God, I sure hope so!"
    date: 2003-11-06
    body: "if they install XD2 on it though, it will kill KDE\n\nI know because I've installed it and all my icons were not working, and KDE crashed everytime I went to the kicker and clicekd something, it was useless. It totally sabatoged my KDE."
    author: "Alex"
  - subject: "Re: Great Read Aarom!"
    date: 2003-11-05
    body: "Methinks your spelling error-hunt was a bit too pedantic. After all, \"anyways\" is perfectly valid (well, depending on who you ask) as an informal spelling of that word. It's fairly common in normal speech, and this isn't exactly a formal environment ...\n\nNot that I don't like pedantry, but there's a time and a place -- like on b.k.o, to make sure that 3.2 really rocks!"
    author: "azza-bazoo"
  - subject: "standard theme?"
    date: 2003-11-04
    body: "does anyone know, if everaldos screenshot (http://www.kde-look.org/content/preview.php?file=8341-1.jpg) is just a mockup? this looks incredible. something right between m$ xp and mac osx. just better. if this is the way, kde 3.2 will look, i will switch. really, fantastic work!!!!"
    author: "L1"
  - subject: "Re: standard theme?"
    date: 2003-11-04
    body: "Those icons look like Crystal SVG, but I can't tell what that widget style is."
    author: "Rayiner Hashem"
  - subject: "Re: standard theme?"
    date: 2003-11-04
    body: "i think it's a mockup, the splitter is collapsable, which the QT with KDE 3.1 definitely can't do. i don't think it can do it now. or can it? :O)"
    author: "emmanuel"
  - subject: "Re: standard theme?"
    date: 2003-11-04
    body: "It could be just \"painted\" onto the widget indicating that the splitter is movable without offering anything like a button (which is possible and would actually make sense to me). I thought it might be a style Conectiva (for whom Everaldo worked initially) is using. But just now I saw that rounded, non-dotted selection in the navigation panel, so I guess it's indeed a mockup. Oh well..."
    author: "Datschge"
  - subject: "Re: standard theme?"
    date: 2003-11-04
    body: "AFAIK the theme/style of KDE 3.2 will not change.  It has been decided that KDE can not drasticly change the way it look between every minor release.  4.0 will be the earliest that you will see a change.\n\n--adam"
    author: "kinema"
  - subject: "Excellent."
    date: 2003-11-04
    body: "December 8th will mark the day I can completely stop relying on apps like Mozilla Firebird, Evolution, and others that are currently infesting ;-D my desktop.  The Alphas were great, and it sounds like the beta is even more impressive.  Here's hoping that 3.2 can release <i>before</i> schedule."
    author: "regeya"
  - subject: "Re: Excellent."
    date: 2003-11-04
    body: "I assume that you will not be viewing pictures with \nyour web browser then? Konqueror is horribly slow and blocks\nKDE completely when loading a large picture... \nIn fact this is practically the only reason I still have\nto use Mozilla...\n\nSee the current discussion at the kfm-devel list.\n\nJohn"
    author: "John"
  - subject: "Re: Excellent."
    date: 2003-11-04
    body: "and see how they are working towards a solution on the kfm-devel list."
    author: "Aaron J. Seigo"
  - subject: "Re: Excellent."
    date: 2003-11-04
    body: "But of course Aaron, this has always been the \nreason I love opensource!! Things might take awhile,\nbut generally the direction is towards progress.\n\nThis is why my wife now exclusively uses kde and\nkonqueror for her studies. \nAnd why I do want to keep using it, also for photo sites.\nI guess I'm growing a bit impatient because this particular\nbug has always bugged me.\n\nBTW my 'reward' for fixing the bug (10 euro) still stands!\nAnybody want to join?\n\nAnd to let you see I mean it, my email adress is in the \nbug report. I'm the van Spaandonk John. Just don't want\nto post it in public.\n\nJohn"
    author: "john"
  - subject: "Please release in 2004!!!!"
    date: 2003-11-05
    body: "I want 3.2, to rock, and if it is released in 2003, than I think it will be rushed ther eis a lot to do before a final and there are still many many bugs which are pretty critical.\n\nReally, why does it matter if you relase in december or a  month  later? After all, 80% of the people won't even get it until march when their distro ships with it, no point in rushing. I would rather see a stellar release."
    author: "Mario"
  - subject: "Re: Please release in 2004!!!!"
    date: 2003-11-05
    body: "Are you running CVS? Do you have a basis in facts? Do you watch the commits and talk with the developers like the release manager?\n\nOnce you put a person in the position of release manager you have to trust him. Coolo had the development schedule on hold to fix bugs for some time. This release is aready much longer than previous major point releases. Unless you can come up with something real you're just spreading FUD. If there needs to be a delay there will and if there doesn't there won't. If there is a delay it would be to 2004. Right now there is no delay planned."
    author: "Eric Laffoon"
  - subject: "Re: Please release in 2004!!!!"
    date: 2003-11-05
    body: "yeah, the hard part of the release cycle is indeed coming: at what point is it good enough to be good enough? test the betas, provide quality factual feedback... that counts a LOT towards the project's decision making when it comes to judging releases ready."
    author: "Aaron J. Seigo"
  - subject: "here are the facts"
    date: 2003-11-07
    body: "Even after removing almost half of the planned features from the 3.2 feature plan, 25% of the features are still in the to do list. http://developer.kde.org/development-versions/kde-3.2-features.html\n\nTo have these features be stable and well tested, i think December 8 is too early and I have heard other say the same thing. Anyway, this si an opinion, not a mathematical equation."
    author: "Alex"
  - subject: "Re: here are the facts"
    date: 2003-11-07
    body: "My suggestion: Stop reading paper, paper doesn't blush after all. Test the real thing before masking uninformed sounding comments. Thanks."
    author: "Datschge"
  - subject: "Re: Please release in 2004!!!!"
    date: 2003-11-19
    body: "for those with real distros like gentoo, we\n$emerge rsync\n$emerge -u world\npractically every week, so I'd like to see it soon (but not buggy)"
    author: "ryan"
  - subject: "Just switched from a old cvs version to 3.2b1.."
    date: 2003-11-04
    body: "Looks pretty solid. I'd have to say that kwallet is pretty annoying though. It should have a checkbox \"don't ask me again\"... pretty much, it should act just like Mozilla's PSM does. "
    author: "axe"
  - subject: "questions questions"
    date: 2003-11-04
    body: "Are the 'new sidebars' in KDevelop Qt or KDE widgets which every developer can now implement, or are they currently 'exclusive' hacks for KDevelop?\n\nDoes anyone have screenshots of the new konqueror tabs? I never saw them and do not know how closing tabs will work in 3.2\n\nthanks"
    author: "ac"
  - subject: "answers, answers"
    date: 2003-11-04
    body: "KDevelop (and Kate btw.) are using KMDI's IDEAl mode, which is available (\"standardized\") in KDE's core libraries now, see: http://developer.kde.org/documentation/library/cvs-api/kmdi/html/index.html\n\nThe \"new\" Konqueror tabs don't look different at all, but now resize to fit into the width, show the tab title in other colors than black when loading and after finished loading but not viewed, each tab can be close by clicking on its little site icon, and optionally you can show buttons for creating new tabs and closing the current tab a la mozilla."
    author: "Datschge"
  - subject: "Re: answers, answers"
    date: 2003-11-04
    body: "IMHO the new tabs are an absolute disaster. They change size and move about several times while the page loads, and the different colours just look a mess. Half the time the page title isn't there.\n"
    author: "cbcbcb"
  - subject: "Re: answers, answers"
    date: 2003-11-04
    body: "I absolutely agree.  I am someone who uses Tabs and I'm surprised this hasn't been fixed."
    author: "anon"
  - subject: "Re: answers, answers"
    date: 2003-11-04
    body: "not at all. right now the tabs show up prior to loading a page, are quite configurable,  have visual feedback as to loading status (icon animation would be better than colours, though), support drag and drop, etc... compared to the tabs in 3.1 they are frieking brilliant. they still have room for improvement, such as the titles getting replaced sooner (right now it waits until the whole document is loaded, uck!), fewer repaints and perhaps some nicer loading feedback. but the improvement from \"huh, we have tabs. so what.\" to \"wow, these tabs are actually useful.\" is, at least for me, quite noticeable. "
    author: "Aaron J. Seigo"
  - subject: "Re: answers, answers"
    date: 2003-11-05
    body: "I believe you need to have a patched qt-copy to see tabs in their full flicker-free glory... eg. http://webcvs.kde.org/cgi-bin/cvsweb.cgi/qt-copy/patches/0013-qtabwidget-less_flicker.patch?sortby=date#dirlist"
    author: "Hamish Rodda"
  - subject: "Something is rotten in the state of SuSERPM. :-)"
    date: 2003-11-04
    body: "Okay, probably just me, but according to the README, it should work with an rpm -Fvh *rpm\n\nHere's my output:\n\nerror: Failed dependencies:\n        libartsbuilder.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsgui.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsgui_idl.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsgui_kde.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsmidi_idl.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsmodules.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsmodulescommon.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsmoduleseffects.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsmodulesmixers.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsmodulessynth.so.0 is needed by kdeaddons3-sound-3.1.93-0\n        libartsbuilder.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsgui.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsgui_idl.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsgui_kde.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsmidi_idl.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsmodules.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsmodulescommon.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsmoduleseffects.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsmodulesmixers.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsmodulessynth.so.0 is needed by kdemultimedia3-3.1.93-0\n        libartsbuilder.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsgui.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsgui_idl.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsgui_kde.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsmidi_idl.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsmodules.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsmodulescommon.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsmoduleseffects.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsmodulesmixers.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libartsmodulessynth.so.0 is needed by kdemultimedia3-video-3.1.93-0\n        libgnokii.so.1 is needed by kdepim3-3.1.93-0\n        libvcard.so.0 is needed by (installed) kbarcode-1.4.1-53\n        libvcard.so.0 is needed by (installed) myldapklient-0.6.6-143\n        libvcard.so.0 is needed by (installed) quanta-3.1.4-29\n        libvcard.so.0 is needed by (installed) synce-kde-0.6-48\n        libvcard.so.0 is needed by (installed) koffice-extra-1.2.92-60\n        libvcard.so.0 is needed by (installed) koffice-illustration-1.2.92-60\n        libvcard.so.0 is needed by (installed) koffice-presentation-1.2.92-60\n        libvcard.so.0 is needed by (installed) koffice-spreadsheet-1.2.92-60\n        libvcard.so.0 is needed by (installed) koffice-wordprocessing-1.2.92-60\n        libvcard.so.0 is needed by (installed) koffice-1.2.92-67\n        libartsbuilder.so.0 is needed by (installed) brahms-1.02-794\n        libartsgui_idl.so.0 is needed by (installed) brahms-1.02-794\n        libartsgui_kde.so.0 is needed by (installed) brahms-1.02-794\n        libartsmidi.so.0 is needed by (installed) brahms-1.02-794\n        libartsmidi_idl.so.0 is needed by (installed) brahms-1.02-794\n        libartsmodules.so.0 is needed by (installed) brahms-1.02-794\n\nOn a quite vanilly SuSE 9.0 installation, btw."
    author: "arcade"
  - subject: "Re: Something is rotten in the state of SuSERPM. :-)"
    date: 2003-11-04
    body: "it looks like you need to insatll the libarts package before all teh KDE packages."
    author: "standsolid"
  - subject: "Re: Something is rotten in the state of SuSERPM. :-)"
    date: 2003-11-11
    body: "Try '-Uhv', because new rpms might be added by an upgrade, which were not installed by the previous Release."
    author: "Peter Wiersig"
  - subject: "Re: Something is rotten in the state of SuSERPM. :-)"
    date: 2004-02-01
    body: "I have got the same problem. Installing the KDE 3.2 RC Packages on SuSE 9.0 Professional (especially kdemultimedia3-sound.rpm) brings a dependency failure because of the libarts modules....\n\nHave you got any solution?\n\nRegards\nAsterix"
    author: "asterix"
  - subject: "Re: Something is rotten in the state of SuSERPM. :"
    date: 2004-02-22
    body: "Had the same problems. I downloaded\nkdemultimedia3-common-3.2......rpm and\nnoatun-3.2.0....rpm.\n(both not from SuSE ??) This solved my dependencies problem but now i have\nanother problem:\nI can\u00b4t play any sound \n\nAny suggestions?\n\nphilipp"
    author: "Philipp"
  - subject: "I hope I can view images with Konqy in 3.2"
    date: 2003-11-04
    body: "Because right now it's too slow to be usefull....\n\nSee [Bug 39693] loading images blocks konqueror\n\nFrom the bug report:\n\n[...]\nI made a 6000x6000 jpeg with GIMP that was filled completely black with a few \n lines drawn in the middle (~750k). \n \n \n gqview took 3 seconds to load it - using /tmp/test.jpg \n xv took 4 seconds to load it - using /tmp/test.jpg \n kuickshow took 7 seconds to load it - using /tmp/test.jpg \n konq-HEAD took 24 seconds to load it - using /tmp/test.jpg \n konq-HEAD took 49 seconds to load it - using http://localhost/test.jpg \n MozillaFirebird took 4 seconds to load it - using /tmp/test.jpg \n MozillaFirebird took 5 seconds to load it - using http://localhost/test.jpg \n opera 6.12 took 3 seconds to load it - using /tmp/test.jpg \n opera 6.12 took 3 seconds to load it - using http://localhost/test.jpg \n[...] \n \n \nLet's all vote for this one people!\n\nFor my part, I offer ten euros (in an envelope, including a nice\ncard) to the guy who fixes it!\n\nJohn\n"
    author: "John"
  - subject: "Re: I hope I can view images with Konqy in 3.2"
    date: 2003-11-04
    body: "See\n\nhttp://lists.kde.org/?t=106773453700001&r=1&w=2\n\nIt apparently has to do with Konq's progressive image loading code and fast(er) connections"
    author: "anon"
  - subject: "Debian packages for woody?"
    date: 2003-11-04
    body: "As a translator for kde I need to try the work done before final release. As a Debian User I need packages to install because konstruct report problems (after a daily \"cvs -l update\"). Someone can help me?"
    author: "Debian User"
  - subject: "Re: Debian packages for woody?"
    date: 2003-11-04
    body: "use orths kdecvs .deb's, see:\n\"ggl: orth kdecvs\"\n\nthere great!"
    author: "cies"
  - subject: "Superb - a revolutionary KDE release..."
    date: 2003-11-04
    body: "with my nickname! I'm famous!!!!"
    author: "Ruediger Knoerig"
  - subject: "Apple's impovements?"
    date: 2003-11-04
    body: "What about Apple improvements? I cannot see any information about any KDE3.2 improvements \"promised\" by Apple some months ago.\n\nManux"
    author: "Manux"
  - subject: "Re: Apple's impovements?"
    date: 2003-11-04
    body: "Many changes have already Replybeen merged. Some have not been."
    author: "anon"
  - subject: "OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "no joke \n\nhttp://www.novell.com/news/press/archive/2003/11/pr03069.html\n\nwith SuSE as a big supporter of KDE now being merged into Novell. Does this affect KDE? I'm still not quite sure what to think of it...\nNovell baught Ximian which is clearly focused on gnome... SuSE will have to drop support for KDE to concentrate on the server (My guess is it's Ximian for the desktop and SuSE for the server)"
    author: "Thomas"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "This was actually the first question that was asked after the merger was announced to the SUSE personnel. The response was along the lines that Novell and its customers value choice and that there is demand from the market for both solutions.\n\nIt will certainly be interesting to see how things will work out in practice. For me personally I think this will offer a great opportunity to work closer with Ximian and GNOME via for example the FreeDesktop.org initiative to provide better integration of technologies from the two different desktops so that we can present independant software developers with a unified set of standards for the Linux desktop.\n\nCheers,\nWaldo\nSUSE/Novell"
    author: "Waldo Bastian"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "So it's good news after all...\nI'd be delighted to see something like the red carpet technology delivering the newest and freshest KDE ;) to the enterprise desktop. (I'll still be able to use konstruct at home...)"
    author: "Thomas"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "> So it's good news after all...\n\nLet's not swallow just yet. This is what always gets said, and then all of a sudden,\ntrack two is discarded in silence."
    author: "jmk"
  - subject: "What they bought Ximian for..."
    date: 2003-11-05
    body: "If I were a GNOME advocate, I'd be more worried that Novell really bought Ximian for the Red Carpet stuff (noting that many of their \"big wins\" seemed to be Red Carpet roll-outs) and bought SuSE for the KDE desktop expertise. However, this gives both camps a real opportunity to work together and integrate the desktops in a more sophisticated way than Red Hat managed to do."
    author: "The Badger"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Hmmm, that doesn't sound like much of a reply from Novell.\n\nBtw, didn't the company used to be called SuSE?  Why is it now SUSE all of a sudden?"
    author: "Navindra Umanee"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Well, SuSE is/was a German acronym (Gesellschaft f\u00fcr *S*oftware- *u*nd *S*ystem-*E*ntwicklung mbH) nearly noone even in Germany really understood while Suse is a nice femal name and SUSE all in caps makes it look really important, so Novell noticed them finally. Other than that I've no idea. =P\n\nPS: I however do like their new chameleon better than the old one."
    author: "Datschge"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Do germans really use words like \"Gesellschaft\" and \"Entwicklung mbH\"? Man, if I were german, I would have to untie my tongue every noght before going to bed :-)"
    author: "Roberto Alsina"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "You don't want to say that these words are long, right? Because they aren't ;)\nAnd BTW, \"mbH\" is not a word but an abreviation for \"mit beschraenkter Haftung\" (with limited liability)."
    author: "ac"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "It's not a matter of length, but of low vowel density :-)\n\nFor example, the spanish equivalent of mbH would be \"de responsabilidad limitada\". it's actually longer (25 letters vs 22, and each one has to be pronounced ;-)"
    author: "Roberto Alsina"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-07
    body: "\"It's not a matter of length, but of low vowel density :-)\"\n\nAh, ok. But it looks a bit worse than it actually is, because, for example \"sch\" is only one sound (like the \"sh\" in english \"shoe\"), same goes for the \"ll\" before it, and \"ck\" is also only a special kind of \"k\". ;)\n\n\"...and each one has to be pronounced\"\nI guess that's the difference ;)\n"
    author: "ac"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "> Man, if I were german, I would have to untie my tongue every noght before going to bed\n\nWell, yes, that's exactly what we're doing over here. It might be a bit uncomfortable to untie your tongue every evening but \u00fdou get used to it ;-)"
    author: "Thomas"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "> Why is it now SUSE all of a sudden?\n<br><br>\nIt's part of the new branding that was introduced a few weeks ago. We also have shiny new pictures on the <a href=\"http://www.suse.com\">website</a>."
    author: "Waldo Bastian"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Im going to be very blunt and very honest in my feelings here.  I think this whole deal is bullshit.  SUSE should never have even considered selling to Novell.  SUSE was my main desktop and Linux distro since version 6.4 anytime I ever developed anything for Linux I made sure it was SuSE compliant.  Now, you expect me to get googoo eyed over having to more and likely change my GUI tools and learn GTK.  GTK sucks KDE and QT are the best, asking Novell, a GNOME company, to sell KDE is like asking Ford to sell a Toyota car off of their lot.  Its not going to happen.  But i of course being as faithful as I am to SuSE will stay and see whats happening, if everything backfires and starts to go south i will then go back to Windows I guess.  If Novell pulls the retail box sets of SUSE Linux, I will be going with Windows because I like having a box with manuals, screw that download ISO off the web and burn it crap.  Thats for the dogs.  "
    author: "Roberto J. Dohnert"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Who said that Novell was a GNOME company? Novell was in talks to buy SuSE a while before they bought Ximian. Their initial offers were quite low ($140 million), and as such, were rejected by SuSE. With the help of IBM, and their $50 million investment in Novell, they were able to finally buy SuSE for $210 mill, who they obviously desperatly wanted. \n\nIf Novell had succeded in buying SuSE _before_ they bought Ximian, people would be sitting on news.gnome.org talking about Novell is going to pull the plug on Ximian's GNOME support and the world was going to be blown apart and such. Guess what, they're wrong too."
    author: "anon"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "<< Who said that Novell was a GNOME company? >>\n\nNovell owns Xiamian, a GNOME company\n\n<< people would be sitting on news.gnome.org talking about Novell is going to pull the plug on Ximian's GNOME support and the world was going to be blown apart and such. Guess what, they're wrong too. >>\n\nKind of brings up the old question.  Is the enemy of my enemy my friend? or my enemy?  I dont trust Novell and I say this the letters N-O-V-E-L-L spell \nD-O-O-M for KDE and SUSE"
    author: "Roberto J. Dohnert"
  - subject: "missed my point COMPLETELY"
    date: 2003-11-04
    body: "> Novell owns Xiamian, a GNOME company\n\nSo, now that they own SuSE, they are now a KDE company too?\n\nErm, you missed my point COMPLETELY. My point is that Novell was actually trying to buy SuSE _first_, before Ximian, but couldn't do so because of market conditions. They aren't tied to KDE nor GNOME, and aren't a KDE nor GNOME company. They own subsidiaries that certainly are though."
    author: "anon"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Mod's note:  I'm deleting all these trolls with extreme prejudice, given that they are from the wiggle troll.  Someone I have already warned and effectively banned.  -N.\n<!--\n\"integration\" -- fine, as long as it doesn't involve Qt and its odious licensing quagmire finding its way into GNOME... and the legendary KDE bloat and slowness stays firmly on its own side of the fence.\n-->\n"
    author: "anon2"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "hm... ;) don't know why, but your comment made me smile somehow. Well, I guess it's simply my elitist arrogance..."
    author: "Thomas"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Mod's note:  I'm deleting all these trolls with extreme prejudice, given that they are from the wiggle troll.  Someone I have already warned and effectively banned.  -N.\n<!--\nMore likely your staggering lack of knowledge and foresight.\n-->"
    author: "anon2"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Mod's note: baleeted!\n<!--\nYou have got ot be the worst system administrator I've ever seen.\n\nNot only you do you ban, indiscriminately, anyone you disagree with -- but you can't even lie well. \n\n1. You have never warned me.\n2. What's a wiggle troll.\n3. You have never banned me, effectively or not.\n4. If you are going to delete something, then delete the thread. Leaving the replies is, at best intellectually dishonest. \n5. I did not engage in flaming or abuse (that was all directed my way). I simply took some facts from the Novell Purchases and drew some logical conclusions which are not favourable to KDE. Apparently, this is a banning offense.\n-->"
    author: "anon2"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "> It will certainly be interesting to see how things will work out in practice. For me personally I\n> think this will offer a great opportunity to work closer with Ximian and GNOME via for example\n> the FreeDesktop.org\n\nAs a longtime KDE and SuSE user I truly hope that this will be possible. However, as a corporate junkie myself, i don't think any large corporation at times like this will include two units one way or the other competing with each other. In the long run, other one will be ditched. It's just a matter of choosing which one.\n\nThere's no-one surely saying either way, but if it was KDE that got selected to be number one priority, ximian purchase wouldn't still be void - just a trick to 'buy out' competition.\n"
    author: "jmk"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Hi,\n\nI am convinced that Novell doesn't care that much for the desktop at this point. SUSE was in my opinion picked for the Server applications that have come out of SUSE lately. I hope that more SUSE work will now focus on integration of LDAP-everything-server-world into a KDE worthy and integrated Free Software (not Yast licence if we dare hope so) frontend.\n\nIt will be interesting to see wether Yast will remain non-free. Yast is still unique, nothing from Redhat (or anybody I am aware of) comes even close. I hope Novell shall release Yast as Free Software to push GNU/Linux as a whole over competing systems.\n\nIf I were Novell, I would take Ximian Connector and integrate it into Kontact. That would be a great thing too.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Don't be so sure.  From /.:\n\nI have recently listened to some talks inside the GNOME Irc channel where I heard some Ximian employees mentioning that Novell told them to work on KDE stuff now. From the internals of Novell it sounds like their employees are more interested to work with KDE rather than GNOME because that employee listed a long list of complaints that the Novell people raised which they found in GNOME to be totally out of use.\n----------------\n\nDon't forget that many buy SuSE /for/ KDE.  Novell would be silly not to recognize that."
    author: "anon"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "That would make sense, because when you want to beat RedHat in U.S. you have to make something different - like using another desktop environment."
    author: "Anonymous"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Red Hat never offered a product targeted to the home desktop computer, they're getting rid of their entry level server distribution right now. And using a different *desktop* environment on a *server* product doesn't sound like something which would achieve more market shares."
    author: "Datschge"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Your forgetting about the server admins who need a desktop environment to run a server. But your right, most folks don't really care. "
    author: "Ian Monroe"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "Mod's note:  I'm deleting all these trolls with extreme prejudice, given that they are from the wiggle troll.  Someone I have already warned and effectively banned.  -N.\n<!--\nNonsense. That simple fact is that Novell is already sending Ximian employees out to train Novell developers in GTK/GNOME -- you need only read the GNOME developers blogs to see this, and its also been published (see website in other message). GNOME will be the Novell desktop, and KDE is out in the cold with no corporate support other than Trolltech's pals and SCO.\n-->"
    author: "anon2"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "novell employees==outsourced indian contracters in bangalore.. I remember when Sun did the same thing with wipro. The more this happens the better for kde actually ;)\n\nNovell's strategy looks like using a KDE-based desktop (SuSE Linux) and GNOME-based tools (Evolution, mono) to create a nice balance =)\n\nOf the two companies, though, Novell's investment in SuSE is much larger than their investment in Ximian-- remember that Ximian is still a pretty small company compared to SuSE (about 1/4th-1/5th the size)\n\nTheir \"desktop\" is neither GNOME nor KDE, but solutions based on both. That's where the money is. Capitalism talks, zealotry and old UNIX-style flame wars don't."
    author: "anon"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Mod's note:  I'm deleting all these trolls with extreme prejudice, given that they are from the wiggle troll.  Someone I have already warned and effectively banned.  -N.\n<!--\n*Novell's strategy looks like using a KDE-based desktop (SuSE Linux) \n\nSuSE's desktop has always been the weakest part of its distro. \n\n* and GNOME-based tools (Evolution, mono) to create a nice balance =)\n\nIf you use Evolution, you use GNOME. Fact. Evolution touches every single part of the GNOME desktop, with the exception of the panel. It was the biggest driving force behind the design and development of GNOME 2.x. For Novell to run the KDE desktop with Evolution was would be the height of idiocy. There's not one single reason to go with KDE (for those whose minds are not clouded with zealotry)... it costs a fortune to develop with (thanks TrollTech), is exceedingly bloated, and it's slow to load without bandaging it up with hacks. And besides, all the best apps run under GNOME, not KDE, and GNOME has far better i18n and (most important of all) accessiblity. Frankly, it's no contest as far as corporate desktops are concerned.\n\n-->"
    author: "anon2"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "> Evolution touches every single part of the GNOME desktop, with the exception of\n> the panel. It was the biggest driving force behind the design and development of > GNOME 2.x\n\n? Don't think, any gnome developer needs to post on the dot as \"anon2\" just to make statements like that... plz spare us further interpretations of \"driving forces\" behind gnome or kde... "
    author: "Thomas"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Mod's note:  I'm deleting all these trolls with extreme prejudice, given that they are from the wiggle troll.  Someone I have already warned and effectively banned.  -N.\n<!--\n* Don't think, any gnome developer needs to post on the dot as \"anon2\" just to make statements like that... plz spare us further interpretations of \"driving forces\" behind gnome or kde..\n\nDon't believe me then! You can always go and check the GNOME mail archives. Evolution uses virtually every part of GNOME and GTK (including many custom widgets) -- it was designed that way, and was a testbed for much of the technology that debuted in GNOME 1.4 and reached maturity in GNOME 2.x -- it's extensively bonoboised for a start. It is simply ridiculous to think that Novell would install  KDE to use for a few panels and then GNOME to use for apps like Evolution (and don't kid yourself, Evo was one of the prizes that Novell went after Ximian for. KMail is simply not up to the job in the corporate world), especially when you consider that GNOME is far superior in the basics of a desktop (such as 18n and a10y).\n\nIf you had even the slightest understanding of software development (beyond a 3 line hello world KDE window and widget that you think makes you so l33t), you would understand this. Face it zealot, SuSE is now GNOMEed, and KDE has lost its biggest distro.\n-->"
    author: "anon2"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Your sheer stunning technical knowledge makes me afraid already...\n\n...afraid, that you don't know what you are talking about. =P Help out 'your' community instead wasting everyone's and your own time with this kind of repetitive child's plays."
    author: "Datschge"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "Mod's note:  I'm deleting all these trolls with extreme prejudice, given that they are from the wiggle troll.  Someone I have already warned and effectively banned.  -N.\n<!--\nPoint out where I'm wrong, or crawl back into your zealot-hole. \n\nI do realise that hardcore fanatics, like the ones native to dot.kde.org, are rarely able to see the truth... but some of you *must* be able to see the ramifications of the SuSE purchase?\n\nIt's not hard... all you need is a basic understanding of the real world, a bit of clear thinking and some basic understanding of software development. Oh... what am I saying... this is dot.kde.org after all!\n-->"
    author: "anon2"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-05
    body: "You are at the wrong place, this is not a Gnome news site so everything you were writing here so far is simply irrelevant.\n\nRegarding the SUSE purchase everything we heard so far is that SUSE will keep its brand, keep its own head office in Nuremberg and continue their current line of Linux products which will be offered and supported by Novell's retail chain as well, additionally they'll obviously add several new Linux product for Novell related solutions as well as support additional IBM server hardware. Regarding the support of open source projects Novell states \"From advocacy and development resources to events and support of open source efforts like kernel projects, XFree86, ReiserFS, KDE, GNOME and Mono, Novell stands side-by-side with the open source community.\" The order of the mentioned project may be interesting as well, but nothing specific has been said regarding changes in SUSE's product development policy. In doubt they'll just be allowed to continue their current already proven policy while being able to dramatically expand with the help of Novell's existing retail chain.\n\nNothing more has been said so far, neither with regard to Gnome nor KDE (it shouldn't matter for any of both projects anyway). End of the story as for now."
    author: "Datschge"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "http://www.suse.com/us/company/press/press_releases/archive03/novell_suse.html\n\n\"From advocacy and development resources to events and support of open source efforts like kernel projects, XFree86, ReiserFS, KDE, GNOME and Mono, Novell stands side-by-side with the open source community.\"\n\nWe'll see. Perhaps they'll want to merge KDE, GNOME and Mono? =P"
    author: "Datschge"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "I don't think they'll try to merge KDE and GNOME, but an integration of Mono would be really cool. If Mono would be integrated into both desktops it would be easy to port an app from one desktop to the other.\nPerhaps Mono will be a part of KDE 4.0..."
    author: "Sven Langkamp"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-09
    body: "The issue with mono is that it has got a huge patent problem. Microsoft has a nice package of patents: When they start to dislike Mono, they will find a patent infringement and stop it. That's why no sane Linux vendor will distribute it.\n"
    author: "anon"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "> with SuSE as a big supporter of KDE now being merged into Novell. Does this affect KDE?\n\nI'm sure that it affects KDE somehow. But I wouldn't expect it to affect much, eg it will not kill either GNOME or KDE."
    author: "Anonymous"
  - subject: "Re: OT: SuSE baught by Novell"
    date: 2003-11-04
    body: "It's going to be tough since GNOME now has a strong foothold at Novell already:\n\nhttp://nat.org/2003/october/#16-October-2003\nhttp://economictimes.indiatimes.com/cms.dll/html/uncomp/articleshow?msid=256142"
    author: "anon"
  - subject: "Make 3.2 ROCK!!!"
    date: 2003-11-05
    body: "If that happens than Novell will have to pay attention and face demand. Besides, i think it would be a  bad business decision to focus only on GNOME since what would really differentiate them and after all there are special needs and SUSE would have to change everything."
    author: "Mario"
  - subject: "Re: Make 3.2 ROCK!!!"
    date: 2003-11-05
    body: "Forget ROCKing.\n\nWhat we need is a commercial quality application.\n\nPerhaps we can finally get some commercial support to help make a stable product.\n\nAlso: RedHat is discontinuing consumer products:\n\nhttp://www.computerworld.com/softwaretopics/os/linux/story/0,10801,86827,00.html?nas=AM-86827\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Make 3.2 ROCK!!!"
    date: 2003-11-07
    body: "Yes, \"commercial quality\" \"stable\" is most important.\n"
    author: "cjacker"
  - subject: "Re: Make 3.2 ROCK!!!"
    date: 2003-11-07
    body: "That's a nice collection of practically useless buzz words."
    author: "Datschge"
  - subject: "The slow death of KDE"
    date: 2003-11-04
    body: "Mod's note:  I'm deleting all these trolls with extreme prejudice, given that they are from the wiggle troll.  Someone I have already warned and effectively banned.  -N.\n<!--\nHow ironic. A new version of KDE is announced, and at virtually the same time it's death warrant is signed by Novell. \n\nThis is indeed bad news day for the KDE desktop. Not only is TrollTech in a close business relationship with SCO (SCO owns a significant chunk in fact)... but now SuSE, the main KDE supporter is suddenly snapped up by Novell -- which just recently bought Ximian for their GNOME desktop technology and expertise. Ominous for the KDE project is Novell sending Ximian engineers out to India to train them in developing with C/GTK/GNOME See here: http://economictimes.indiatimes.com/cms.dll/html/uncomp/articleshow?msid=256142\n\nTaken as a whole, the recent weeks have been catastrophic for the KDE project's future. I'm afraid I don't see much chance for it: It costs too much to develop for (no-one *wants* to pay the TrollTech tax to develop closed-source stuff) and has now even its meagre corporate support has been cut out from underneath it. GNOME has always been the commercial Linux desktop of choice, but this just seals it. KDE's only remaining support is TrollTech and the widely despised lawyer-pit SCO -- beyond that, it can only look to an ever shrinking band of noisy and unproductive zealots who give it a bad name with their unprofessional behaviour.\n\nHow long before the \"KDE is dying\" troll arrives on Slashdot? Not long, I think.\n-->"
    author: "anon2"
  - subject: "Re: The slow death of KDE"
    date: 2003-11-04
    body: "> How long before the \"KDE is dying\" troll arrives on Slashdot? Not long, I think.\n\nThe earlier you go there, the happier I'm.\n "
    author: "Anonymous"
  - subject: "Re: The slow death of KDE"
    date: 2003-11-05
    body: "\"no-one *wants* to pay the TrollTech tax to develop closed-source stuff\"\n\nAnd Microsoft is a poor little company because no one wants to pay the MS tax to develop closed-source stuff.\n\nGo away."
    author: "Dominic"
  - subject: "Re: The slow death of KDE"
    date: 2003-11-07
    body: "The cute thing about these trolls is that they like to point out (I'm flying by the seat of my pants since I didn't see the original message) that, unlike KDE, you don't have to pay licensing fees for GNOME!  Well, what the heck?  The libraries are good enough to develop on, but not good enough to pay for when you want to write a closed-source for-profit app?  I'm not sure I *want* software written by someone who wants to make a profit, but is completely unwilling to pay for their toolkit.\n\nFor me, the argument that GNOME is free as in beer for closed-source authors just isn't a good argument."
    author: "regeya"
  - subject: "What a bad release"
    date: 2003-11-04
    body: "Don't understand me wrong, I test every version of KDE but I don't have the time to report bugs or so, a big sorry first but there are other software-for-free jobs who need my effords some more than the good KDE guys, there are plenty supporters of KDE....\n\nAnd now the little story:\n\nI'm very lucky with a big-fat-workstation for my work so I tried to compile kde with konstruct, it worked somewhat but what a very bad release, It looks like alpha. After that I thougth that the problem was on my site and I tried to install the SuSE-build RPM's on my good (old?) SuSE 8.2 workstation, everything was/looks corrupted and it doesn't work very well.\n\nNext time better I hope, but I stuck with kde3.2-alpha2. It is the best 3.2 release until now.\n\nA little description of the problems:\n- self compiled arts doesn't work (it did in alpha2) (SuSE arts works, but read more...)\n- The content of kcontrol is gone. Damn what a big shame.\n- [somewhat longer list, will post it when I have some time]\n\nMaybe they should switch the alpha and beta names.......\n\nBTW: the features are cool and wanted by me, but don't forget somewhere the word stable when releasing....\n\nKind regards,\n\nMendel -  big KDE supporter :-)[1]\n\n[1]: Yes I understand the development process and I realy hope and pray that the KDE RC is somehow better. I'll still check the CVS version from week to week, but I don't work on CVS."
    author: "Mendel Mobach"
  - subject: "Re: What a bad release"
    date: 2003-11-05
    body: "It is your problem or your system \"SuSe\"'s problem.\n\nI compiled kde-3.2cvs from Aug Cvs to beta1 with our own release MagicLinux.\nIt works very well, of course, There is some bugs such as \"switch noatun interface will cause noatun crash\".\n\nI never used tools like konstruct, just write spec files and apply some patch I made(Chinese support patch). and build the whole release with a bash script.\n\n\n"
    author: "cjacker"
  - subject: "Re: What a bad release"
    date: 2003-11-05
    body: "I tested some more and even if I compile it by hand it has so much bugs everywhere.\n\nI will test it today or so on SuSE 9.0"
    author: "Mendel Mobach"
  - subject: "Re: What a bad release"
    date: 2003-11-06
    body: "Hi,\n\nnone of your problems exist here. Actually I haven't found a bug so far in the last 2 days, only occasional crashes (KDevelop and Kontact).\n\nOne thing Konstruct doesn't do for you is to make sure that the tools used actually work well enough or that all needed dev packages are there. I e.g. made the mistake to compile/install alpha2 on one machine as user and it had many more bugs than the installation as root. I slap myself for that.\n\nYou probably need to find out what you did wrong or what's wrong on your Suse system.\n\nYours, Kay"
    author: "Debian User"
  - subject: "libltdl issues"
    date: 2003-11-05
    body: "Can any body help me with this error?  It happened on mandrake 9.1 building meta/kde from\nkonstruct-20031104.tar.bz2:\n\nMaking install in libltdl\nmake[4]: Entering directory `/home/pauls/konstruct/kde/kdelibs/work/kdelibs-3.1.93/libltdl'\n/bin/sh ../libtool --silent --mode=compile cc -DHAVE_CONFIG_H -I. -I. -I.. -I../dcop -I../kio/kssl   -DQT_THREAD_SUPPORT -I/home/pauls/kde3.2-beta1/include -I/usr/X11R6/include -I/home/pauls/kde3.2-beta1/include -I/usr/X11R6/include -I/home/pauls/kde3.2-beta1/include -I/usr/X11R6/include -D_REENTRANT  -ansi -W -Wall -Wchar-subscripts -Wshadow -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -O2  -I/home/pauls/kde3.2-beta1/include -I/usr/X11R6/include -L/home/pauls/kde3.2-beta1/lib -L/usr/X11R6/lib -O2 -pipe -I/home/pauls/kde3.2-beta1/include -I/usr/X11R6/include -L/home/pauls/kde3.2-beta1/lib -L/usr/X11R6/lib -O2 -pipe -I/home/pauls/kde3.2-beta1/include -I/usr/X11R6/include -L/home/pauls/kde3.2-beta1/lib -L/usr/X11R6/lib -O2 -pipe -Wformat-security -Wmissing-format-attribute -c -o ltdl.lo `test -f 'ltdl.c' || echo './'`ltdl.c\nltdl.c:183: `LTDL_OBJDIR' undeclared here (not in a function)\nltdl.c:269: `malloc' undeclared here (not in a function)\nltdl.c:271: `free' undeclared here (not in a function)\nltdl.c: In function `rpl_strdup':\nltdl.c:368: warning: implicit declaration of function `strlen'\nltdl.c:371: warning: implicit declaration of function `strcpy'\nltdl.c: In function `rpl_realloc':\nltdl.c:516: warning: implicit declaration of function `realloc'\nltdl.c: In function `presym_init':\nltdl.c:1108: warning: unused parameter `loader_data'\nltdl.c: In function `presym_exit':\nltdl.c:1149: warning: unused parameter `loader_data'\nltdl.c: In function `presym_open':\nltdl.c:1195: warning: unused parameter `loader_data'\nltdl.c: In function `presym_close':\nltdl.c:1241: warning: unused parameter `loader_data'\nltdl.c: In function `presym_sym':\nltdl.c:1251: warning: unused parameter `loader_data'\nltdl.c: In function `lt_dlpreload':\nltdl.c:1352: warning: unused variable `errormsg'\nltdl.c: In function `lt_dlexit':\nltdl.c:1382: warning: unused variable `errormsg'\nltdl.c: In function `find_module':\nltdl.c:1569: warning: implicit declaration of function `sprintf'\nltdl.c:1599: warning: implicit declaration of function `strcat'\nltdl.c: In function `find_file':\nltdl.c:1698: warning: assignment discards qualifiers from pointer target type\nltdl.c:1732: warning: implicit declaration of function `strncpy'\nltdl.c:1748: `FILE' undeclared (first use in this function)\nltdl.c:1748: (Each undeclared identifier is reported only once\nltdl.c:1748: for each function it appears in.)\nltdl.c:1748: `file' undeclared (first use in this function)\nltdl.c:1748: warning: implicit declaration of function `fopen'\nltdl.c: In function `load_deplibs':\nltdl.c:1783: warning: unused parameter `deplibs'\nltdl.c: In function `lt_dlopen':\nltdl.c:2043: warning: implicit declaration of function `memset'\nltdl.c:2072: warning: assignment discards qualifiers from pointer target type\nltdl.c:2097: `FILE' undeclared (first use in this function)\nltdl.c:2097: `file' undeclared (first use in this function)\nltdl.c:2122: warning: implicit declaration of function `isalnum'\nltdl.c:2140: parse error before ')' token\nltdl.c:2143: parse error before ')' token\nltdl.c:2180: warning: implicit declaration of function `fclose'\nltdl.c:2187: warning: implicit declaration of function `feof'\nltdl.c:2189: warning: implicit declaration of function `fgets'\nltdl.c:2214: warning: implicit declaration of function `strncmp'\nltdl.c:2257: warning: assignment discards qualifiers from pointer target type\nltdl.c:2257: `NULL' undeclared (first use in this function)\nltdl.c:2342: warning: implicit declaration of function `getenv'\nltdl.c: At top level:\nltdl.c:473: warning: `rpl_memcpy' defined but not used\nmake[4]: *** [ltdl.lo] Error 1\nmake[4]: Leaving directory `/home/pauls/konstruct/kde/kdelibs/work/kdelibs-3.1.93/libltdl'\nmake[3]: *** [install-recursive] Error 1\nmake[3]: Leaving directory `/home/pauls/konstruct/kde/kdelibs/work/kdelibs-3.1.93'\nmake[2]: *** [install-work/kdelibs-3.1.93/Makefile] Error 2\nmake[2]: Leaving directory `/home/pauls/konstruct/kde/kdelibs'\nmake[1]: *** [dep-../../kde/kdelibs] Error 2\nmake[1]: Leaving directory `/home/pauls/konstruct/kde/kdebase'\nmake: *** [dep-../../kde/kdebase] Error 2"
    author: "PaulSeamons"
  - subject: "Re: libltdl issues"
    date: 2003-11-05
    body: "How about using http://bugs.kde.org and watching http://bugs.kde.org/show_bug.cgi?id=67228 ?"
    author: "Anonymous"
  - subject: "Re: libltdl issues"
    date: 2003-11-05
    body: "> ltdl.c:183: `LTDL_OBJDIR' undeclared here (not in a function)\n> ltdl.c:269: `malloc' undeclared here (not in a function)\n> ltdl.c:271: `free' undeclared here (not in a function)\n> ltdl.c:368: warning: implicit declaration of function `strlen'\n> ltdl.c:371: warning: implicit declaration of function `strcpy'\n> ltdl.c:516: warning: implicit declaration of function `realloc'\n> ltdl.c:1569: warning: implicit declaration of function `sprintf'\n> ltdl.c:1599: warning: implicit declaration of function `strcat'\n> ltdl.c:1732: warning: implicit declaration of function `strncpy'\n> ltdl.c:1748: `FILE' undeclared (first use in this function)\n> ltdl.c:1748: `file' undeclared (first use in this function)\n \nLooks like you need to install some development packages. When the compiler cannot find basic things like 'malloc', 'free', 'strlen' etc, it's a lot more likely your fault or konstructs fault (unlikely, but maybe konstruct should detect missing devel-packages)."
    author: "Johan Veenstra"
  - subject: "libltd.h error"
    date: 2004-11-20
    body: "hi, i need help for install Apollon (gift enhanced) when i try to configure i receive this error message...\n'note: that i have libltdl, libtool and libguile installed, but the installer dont have found the file.'\n\n\nchecking for lt_dlopen in -lltdl... no\nchecking ltdl.h usability... no\nchecking ltdl.h presence... no\nchecking for ltdl.h... no\n * configure: error:\n * libltdl support is temporarily required.  Please install the appropriate\n * library and header files (which includes the -dev package).  See config.log\n * for more details.\n\nThanx\n\nDaniel"
    author: "Daniel Garcia"
  - subject: "Re: libltdl issues"
    date: 2004-02-04
    body: "Paul, Ever sort this out it I am having hte same problem on mandrake 9.1 with the just released kde 3.2 "
    author: "Michael Mueller"
  - subject: "Re: libltdl issues"
    date: 2004-02-04
    body: "Install autoconf-2.5."
    author: "Anonymous"
  - subject: "Re: libltdl issues"
    date: 2004-02-04
    body: "I have the same problem. I'm currently looking for the package to satisfy the function call. I've found this thread a couple other places saying to install autoconf, but my version is at 2.53.  \n\nVery curious. I'm using konstruct as well. If anyone gets a hint as to what package is needed, I'd appreciate the answer as well.\n\n"
    author: "MadPenguin"
  - subject: "Re: libltdl issues"
    date: 2004-02-04
    body: "Shame on me. Went and read the bug tracker. Should have it resolved shortly. Sorry for the useless posts. Thanks to previous poster with bug track info."
    author: "MadPenguin"
  - subject: "Re: libltdl issues"
    date: 2004-02-16
    body: "bump"
    author: "tclark"
  - subject: "Kcontrol - no modules "
    date: 2003-11-05
    body: "I searched the bugtracker, for this, but kcontrol comes up empty, no module tree.\n\nI used konstruct to build from source on this RH9 box.  prefix/share/applnk/Settings only has a couple of dirs in it, all the desktop modules are missing.  crazy.. can't mess with themes and stuff!  dang - looks great otherwise!!\n\n\nedfardos"
    author: "edfardos"
  - subject: "Re: Kcontrol - no modules "
    date: 2003-11-05
    body: "I have the same problem, also on RH9.\n\n\n\nAmir"
    author: "Amir Michail"
  - subject: "Re: Kcontrol - no modules "
    date: 2003-11-05
    body: "It seems you are not alone. I have the same problem. There is a bug filed in Bugzilla:\n\nSee http://bugs.kde.org/show_bug.cgi?id=67192"
    author: "Robert Schouwenburg"
  - subject: "Re: Kcontrol - no modules "
    date: 2003-11-05
    body: "See\n<a href=\"http://bugs.kde.org/show_bug.cgi?id=67192\">\nhttp://bugs.kde.org/show_bug.cgi?id=67192</a>\nand\n<a href=\"http://bugs.kde.org/show_bug.cgi?id=67271\">\nhttp://bugs.kde.org/show_bug.cgi?id=67271</a>\nfor solutions."
    author: "Waldo Bastian"
  - subject: "Re: Kcontrol - no modules "
    date: 2003-11-05
    body: "Thanks! Your workaround works like a charm."
    author: "Robert Schouwenburg"
  - subject: "I' m having the same problem!"
    date: 2003-11-07
    body: "And the bugs.kde.org is down! can anyone post the work around here please ?"
    author: "Vittorio"
  - subject: "Re: I' m having the same problem!"
    date: 2003-11-07
    body: "Try again, patience is not one of your strengths?"
    author: "Anonymous"
  - subject: "not when it comes to a new kde release!"
    date: 2003-11-07
    body: ";-)"
    author: "Vittorio"
  - subject: "Re: Kcontrol - no modules "
    date: 2004-02-08
    body: "Hi,\n\nI have this problem too und I dont have any solution for it. \nThe workarounds posted above expect the directory /usr/kde/3.2 which is not available by me. Where can I find the directory?\n\nthanks\n\n"
    author: "Georg"
  - subject: "How to use xdelta?"
    date: 2003-11-05
    body: "How to use xdelta?"
    author: "xdelta"
  - subject: "Re: How to use xdelta?"
    date: 2003-11-06
    body: "man xdelta"
    author: "Anonymous"
  - subject: "Re: How to use xdelta?"
    date: 2003-11-11
    body: "how to use man?"
    author: "xdelta"
  - subject: "Re: How to use xdelta?"
    date: 2004-04-22
    body: "man man"
    author: "Slv"
  - subject: "Re: How to use xdelta? -- Don't Bother!"
    date: 2004-12-08
    body: "bash-2.05b$ xdelta\nbash: xdelta: command not found\n...\n\nbash-2.05b$ xdelta3 --help\nVERSION=3_PRERFC_0\nusage: xdelta3 [command/options] [input [output]]\ncommands are:\n    encode      encodes the input\n    decode      decodes the input\n    config      prints xdelta3 configuration\n    test        run the builtin tests\nspecial commands for VCDIFF inputs:\n    printhdr    print information about the first window\n    printhdrs   print information about all windows\n    printdelta  print information about the entire delta\noptions are:\n   -c           use stdout instead of default\n   -d           same as decode command\n   -e           same as encode command\n   -f           force overwrite\n   -n           disable checksum (encode/decode)\n   -D           disable external decompression (encode/decode)\n   -R           disable external recompression (decode)\n   -N           disable small string-matching compression\n   -S [djw|fgk] disable/enable secondary compression\n   -A [apphead] disable/provide application header\n   -s source    source file to copy from (if any)\n   -B blksize   source file block size\n   -W winsize   input window buffer size\n   -v           be verbose (max 2)\n   -q           be quiet\n   -h           show help\n   -V           show version\n   -P           repeat count (for profiling)\n\n....\n\nbash-2.05b$ xdelta3 -d kdelibs-3.3.1-3.3.2.tar.xdelta\nxdelta3: not a VCDIFF input: Invalid argument\n\n....\nbash-2.05b$ xdelta3 -d -s kdelibs-3.3.1-3.3.2.tar.xdelta\n[silence]\n^C\n...\nbash-2.05b$ xdelta3 -d <kdelibs-3.3.1-3.3.2.tar.xdelta\nxdelta3: not a VCDIFF input: Invalid argument\n...\nbash-2.05b$ rm kdelibs-3.3.1-3.3.2.tar.xdelta\n\n"
    author: "howto-waste your time"
  - subject: "Re: How to use xdelta? -- Don't Bother!"
    date: 2004-12-08
    body: "> bash: xdelta: command not found\n\nHow about installing it then? :-)\n\n> bash-2.05b$ xdelta3 --help\n\nxdelta3 is incompatible to xdelta2 is incompatible to xdelta.\n\n> bash-2.05b$ rm kdelibs-3.3.1-3.3.2.tar.xdelta\n\nThanks for proving your dumbness."
    author: "Anonymous"
  - subject: "Re: How to use xdelta? -- Don't Bother!"
    date: 2005-02-04
    body: "In all fairness, xdelta is a bit tricky to use. I've been wrestling with it for the last few hours trying to figure out exactly what sorts of things 'encode' and 'decode' are supposed to work well on.  The only case where it beats the size of gzip is when I cat a file A to file B eight times, and inflate the input window to 100times the normal size.\n\nIn this case clearly it was a diff meant to be applied to the tar file, but bafflingly, that's not the default mode for xdelta3.\n\nFurther the incompatabilities bewteen xdelta 1 2 and 3 are kind of confusing, since one is self contained, one is a library, and one is a front end, and the docs are thin on the ground."
    author: "Joshua Rodman"
  - subject: "Re: How to use xdelta? -- Don't Bother!"
    date: 2006-09-28
    body: "I promise xdelta3 will support xdelta1 inputs, one day soon. I tried to make the command-line syntax of xdelta3 as close to gzip/bzip2 as I could, which meant breaking compatibility with previous versions. xdelta2 was never finished, and it was a storage system, not an application or a library&mdash;as you say."
    author: "Josh MacDonald"
  - subject: "Re: How to use xdelta? -- Don't Bother!"
    date: 2008-03-24
    body: "So.. how do I use it?\n\naaron@Speck /cygdrive/c/ZZ_ISO\n$ ./xdelta3.0t.x86-32.exe -d KDE-Four-Live.i686-1.0.1-1.0.2.iso.xdelta KDE-Four\n-Live.i686-1.0.1.iso\nxdelta3: not a VCDIFF input: XD3_INVALID_INPUT"
    author: "Aaron Peterson"
  - subject: "Re: How to use xdelta? -- Don't Bother!"
    date: 2008-03-24
    body: "As said earlier, xdelta3 is incompatible to xdelta1 diffs."
    author: "Anonymous"
  - subject: "Re: How to use xdelta?"
    date: 2005-09-06
    body: "Here goes nothing.. I don't have my box right in front of me to check against, but I am going to do my best to give a working example of what I did to upgrade from kde-3.4.0 to kde-3.4.1 with xdelta files\n\nThe tricky part (what took me forever to figure out) was it was looking to apply the *.xdelta patches against the tarBalls (.tar files) not the source dir's, or the tar.bz2 files... (found this out by actually doing a vim (edit) on the xdelta file itself and it shows the name of the file it expects to patch against)\n\nSo, something like this...\nfor i in *.tar.bz2;do bunzip2 -v $i;done\nwill take the bz2 off and leave you with a bunch of tarBalls\ndoing an ls on the dir should give you filenames like:\nkdebase-3.4.0.tar\nkdelibs-3.4.0.tar\nand so on\n\nand then for i in *.xdelta;do xdelta patch $i;done\nwill apply all the xdelta files\n\none by one it would be:\nxdelta patch kdebase-3.4.0-3.4.1.xdelta\nand it should patch against your kdebase-3.4.0.tar file\nand the file it will spit out should be named kdebase-3.4.1.tar\n\nOne more thing.. I've you've played with the sources already (built them and then repacked them) the odds are its checksum will fail (mine did). So try to work with tarBalls you haven't tinkered with yet.\n\nI've never seen such crazy stuff before, so I ended up logging a couple hours a day, for close to a week tinkering before I stumbled on it. When the man page says it patches from one file to another you just never expect it to be a plain .tar file with no compression *Grin*.\n\nIf this helps you, or you still need help, feel free to spam me at sponix2ipfw@hotmail.com I get plenty of viagra mail there anyway, a little Linux question might brighten my day..."
    author: "sponix2ipfw"
  - subject: "Re: How to use xdelta?"
    date: 2005-09-06
    body: "The answer why not .tar.bz2 is simple: try to xdelta a bzi2-ed file. You will see that the xdelta file is huge. (The problem, which is not xdelta specific) is that already for a (uncompressed) changed bit, you can have very different compression streams.)\n\nSo you can use xdelta only on uncompressed tar files.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How to use xdelta?"
    date: 2006-09-28
    body: "Actually, xdelta3 implements specific \"external compression\" routines, so that it recognizes compressed inputs and decompresses them to temporary files. This appears somewhere in the code:\n\n  { \"xdelta3\",  \"-cfq\",  \"xdelta3\",    \"-dcfq\",  \"X\", \"\\xd6\\xc3\\xc4\", 3, RD_NONEXTERNAL },\n  { XDELTA1PATH,\"delta\", XDELTA1PATH,  \"patch\",  \"1\", \"%XD\",          3, 0 },\n  { \"bzip2\",    \"-cf\",   \"bzip2\",      \"-dcf\",   \"B\", \"BZh\",          3, 0 },\n  { \"gzip\",     \"-cf\",   \"gzip\",       \"-dcf\",   \"G\", \"\\037\\213\",     2, 0 },\n  { \"compress\", \"-cf\",   \"uncompress\", \"-cf\",    \"Z\", \"\\037\\235\",     2, 0 },\n"
    author: "Josh MacDonald"
  - subject: "Re: How to use xdelta?"
    date: 2006-09-28
    body: "Like gzip with the additional -s SOURCE. Like gzip, -d means to decompress, and the default is to compress. For output, -c and -f flags behave likewise. Unlike gzip, xdelta3 defaults to stdout (instead of having an automatic extension). Without -s SOURCE, xdelta3 behaves like gzip for stdin/stdout purposes.\n\nCompress examples:\n\nxdelta3 -s SOURCE TARGET > OUT\nxdelta3 -s SOURCE TARGET OUT\nxdelta3 -s SOURCE < TARGET > OUT\n\nDecompress examples:\n\nxdelta3 -d -s SOURCE OUT > TARGET\nxdelta3 -d -s SOURCE OUT TARGET\nxdelta3 -d -s SOURCE < OUT > TARGET"
    author: "Josh MacDonald"
  - subject: "Re: How to use xdelta?"
    date: 2006-10-13
    body: "Can someone point out what i could be doing wrong here... or why my checksums are not matching? I'm assuming it has to do with the fact that I'm dealing with gzip's... but i'm really not sure.\n\nExample (for demo purposes):\n\n[newinstall /tmp]# ls -l *.tgz\n-rw-r--r--    1 root     root      2116253 Oct 12 17:06 sb-11.tgz\n-rw-r--r--    1 root     root      4230637 Oct 13 12:53 sb-12.tgz\n\nxdelta3 -s sb-11.tgz sb-12.tgz sb.xd\n\nNow i will rename sb-12.tgz to sb-12.tgz.orig.... and restore it with\nxdelta3 -d sb.xd\n\nNow i have:\n\n-rw-r--r--    1 root     root      2116253 Oct 12 17:06 sb-11.tgz\n-rw-r--r--    1 root     root      4230637 Oct 13 13:16 sb-12.tgz\n-rw-r--r--    1 root     root      4230637 Oct 13 12:53 sb-12.tgz.orig\n-rw-r--r--    1 root     root      2083135 Oct 13 13:15 sb.xd\n\nsb-12.tgz and sb-12.tgz.orig are the same number of bytes.. butttttt:\n\n[newinstall /tmp]# md5sum sb-12.tgz sb-12.tgz.orig\nd6fd1878fe4a941169d0f4b9e0d67909  sb-12.tgz\n310b8c0a6bc6d8d5879ea9fea907eb12  sb-12.tgz.orig\n\nUltimately the delta will be transfered via xml-rpc for largescale remote backup purposes so matching checksums on either end are important... what am I doing wrong.\n\nIf it helps here's the output from xdelta3 config:\n\nVERSION=3_PRERFC_0\nVCDIFF_TOOLS=1\nREGRESSION_TEST=1\nSECONDARY_FGK=0\nSECONDARY_DJW=1\nGENERIC_ENCODE_TABLES=1\nGENERIC_ENCODE_TABLES_COMPUTE=0\nEXTERNAL_COMPRESSION=1\nXD3_POSIX=1\nXD3_DEBUG=2\nXD3_USE_LARGEFILE64=1\nXD3_ENCODER=1\nXD3_DEFAULT_WINSIZE=262144\nXD3_DEFAULT_SRCBLKSZ=262144\nXD3_DEFAULT_SRCWINSZ=8388608\nXD3_DEFAULT_MEMSIZE=262144\nXD3_ALLOCSIZE=8192\nXD3_HARDMAXWINSIZE=8388608\nXD3_NODECOMPRESSSIZE=16777216\nXD3_DEFAULT_IOPT_SIZE=\n\n\n\n"
    author: "Chiv"
  - subject: "Re: How to use xdelta?"
    date: 2007-07-26
    body: "xdelta 3 is very confusing compared to xdelta(1)\n\nAll I want to do is create a binary diff of 2 files???"
    author: "Martin"
  - subject: "Re: How to use xdelta?"
    date: 2008-08-01
    body: "Please.\n\nHow compress archives at xdelta3?\nCommandos and options?\nCompress in 7z and arc at xdelta3?\nPlease explanation.\n         \n\n                   Please. Thanks for answer."
    author: "alce"
  - subject: "Konstruct"
    date: 2003-11-05
    body: "For some reason, my konqueror can't seem to use http web pages.\n\nI built using konstruct on RH9.\n\nAny ideas?"
    author: "TomL"
  - subject: "More info"
    date: 2003-11-05
    body: "Apparently no mime types were installed.   Does that make sense?\n\nI don't know why it would say that now."
    author: "TomL"
  - subject: "ksvg inline on web pages?"
    date: 2003-11-05
    body: "I get image placeholders instead of SVG images when browsing pages with svg images in them. Is this supposed to be working in CVS/Beta now, or should I just hold my horses a little more?"
    author: "Vic"
  - subject: "Re: ksvg inline on web pages?"
    date: 2003-11-05
    body: "Are you sure the images are being sent with the proper svg mime type?"
    author: "Jilks"
  - subject: "Re: ksvg inline on web pages?"
    date: 2003-11-06
    body: "No, I'm not sure. But I would expect at least some websites with svg would do this correctly, and yet none work that I've tried."
    author: "Vic"
  - subject: "Re: ksvg inline on web pages?"
    date: 2003-11-06
    body: "I noticed the same thing, it worked a few weeks ago.\nI suspect a khtml change, will look into it...\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "moving icons in the panel, no more?"
    date: 2003-11-05
    body: "Okay, maybe I'm just clueless, but is there an obvious way to slide the panel icons around?  We used to simply middle-button click on an icon, and it'd\nslide back and forth.. never more, with 3.2b1.   Is there a hotkey work\naround perhaps?\n\nkde3.2b1 rocks - other than this!\n\nedfardos"
    author: "edfardos"
  - subject: "Re: moving icons in the panel, no more?"
    date: 2003-11-06
    body: "I've been right-clicking and selecting \"Move\" recently, but I'd appreciate having the middle-click drag back as well."
    author: "Vic"
  - subject: "Re: moving icons in the panel, no more?"
    date: 2003-11-06
    body: "Middle mouse button on panel to move items is still there. It seems that a running khotkeys makes the middle mouse button stop working (http://bugs.kde.org/show_bug.cgi?id=67376)."
    author: "Anonymous"
  - subject: "Re: moving icons in the panel, no more?"
    date: 2003-11-08
    body: "It's not khotkeys by itself, but rather its mouse gesture support, which can\nbe turned off (-> \"gesture settings\"). MMB grabbing also breaks Blender\nnavigation."
    author: "Melchior FRANZ"
  - subject: "-lreadline not found when compiling kpilot"
    date: 2003-11-06
    body: "I just got this error while building from konstruct, make -C meta/kde install:\n\n/usr/bin/ld: cannot find -lreadline\ncollect2: ld returned 1 exit status\nmake[6]: *** [kpilot] Error 1\nmake[6]: Leaving directory `/home/Amit/sources/konstruct/kde/kdepim/work/kdepim-3.1.93/kpilot/kpilot'\n"
    author: "Amit Shah"
  - subject: "Re: -lreadline not found when compiling kpilot"
    date: 2003-11-06
    body: "Check that you have the \"readline\" and \"readline-devel\" packages of your distribution installed."
    author: "Anonymous"
  - subject: "Re: -lreadline not found when compiling kpilot"
    date: 2003-11-06
    body: "It was due to missing the -dev package of readline. Wonder why that was needed. Thanks anyways."
    author: "Amit Shah"
  - subject: "Re: -lreadline not found when compiling kpilot"
    date: 2003-11-06
    body: "k, solved. It needed libreadline4-dev too. (btw, I'm using Debian sid.) Don't know why the -dev package too needs to be installed."
    author: "Amit Shah"
  - subject: "Catch up"
    date: 2003-11-06
    body: "on features and applications I'd say KDE is now equal to Win98/ME (and as a GUI about as stable luckily it runs on some damn stable OSes (bsd/linux/unix).  \n\nSo hopefully kde desktops will soon have the market share of those legacy applications/platforms ... XP and Longhorn can occupy a small chunk of the market (20-25%) and MacOS/X 15-20 but the rest - the bulk of everything - will be all Linux/BSD :-D\n\nOf course - and some noted it here - multi-media on Unix sux horribly.  It would be nice if MAS (network media server like X) or gstreamer (only using dbus and not corba) could provide cross platform and cross desktop framework for supporting MM ... sort of like how CUPS works for printing.\n\n"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Catch up"
    date: 2003-11-06
    body: "On applications, maybe, but in terms of features, its definately got an edge over even WinXP. XP can't *hope* to do some of the stuff KDE can do. And don't get me started on the API! The polish level needs work though (and I say this as a KDE user).\n\nAnd exactly why does multimedia on UNIX suck? "
    author: "Rayiner H."
  - subject: "Re: Catch up"
    date: 2003-11-07
    body: "I agree.  A common, cross-platform/cross-desktop framework for multimedia is where linux is SEVERELY lacking.\n\nThere is alot of discussion about this over on gnomedesktop.  I suppose the hope is that GStreamer/MAS become the common standard.  I hope both the GNOME & KDE people can agree on something soon!!!\n\n"
    author: "The Guru"
  - subject: "SuSE 8.2 rpm missing libarys"
    date: 2003-11-06
    body: "Hello!\nWould very much like to try out 3.2 beta...\nI am probaly just missing some package because when trying to install I get the following output:\nThanks for any help!\nlinux:# rpm -Fvh *rpm\nFehler: fehlgeschlagene Paket-Abh\u00e4ngigkeiten:\n        libkateinterfaces.so.0 wird von kdeaddons3-kate-3.1.93-0 gebraucht\n        libkateutils.so.0 wird von kdeaddons3-kate-3.1.93-0 gebraucht\n        libkonqsidebarplugin.so.1 wird von kdeaddons3-konqueror-3.1.93-0 gebraucht\n        libartsbuilder.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsgui.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsgui_idl.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsgui_kde.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmidi_idl.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodules.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodulescommon.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmoduleseffects.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodulesmixers.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodulessynth.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        kdebase3 = 3.1.93 wird von kdebase3-devel-3.1.93-0 gebraucht\n        libfribidi.so.0 wird von kdegraphics3-3.1.93-0 gebraucht\n        libexif.so.9 wird von kdegraphics3-kamera-3.1.93-0 gebraucht\n        libartsbuilder.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsgui.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsgui_idl.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsgui_kde.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmidi_idl.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodules.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodulescommon.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmoduleseffects.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodulesmixers.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodulessynth.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libkdeinit_noatun.so wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libgnokii.so wird von kdepim3-3.1.93-0 gebraucht\n        libvcard.so.0 wird von quanta-3.1-112 gebraucht\n        libvcard.so.0 wird von kdebase3-3.1.1-63 gebraucht\n"
    author: "spackenmann"
  - subject: "Re: SuSE 8.2 rpm missing libarys"
    date: 2003-11-06
    body: "> I am probaly just missing some package because when trying to install I get the following output:\n\nExactly, and it's not even hard to guess those with output like\n\n>  kdebase3 = 3.1.93 wird von kdebase3-devel-3.1.93-0 gebraucht"
    author: "Anonymous"
  - subject: "Re: SuSE 8.2 rpm missing libarys"
    date: 2003-11-06
    body: "Thanks very much! I in need didnt have that package. But unluckily I still cant install.\nThis time the missing package isn't so easy to spot (for me...) :-/. Where can I find information when I want to learn more about this kind of problems?\n\n \nlinux: # rpm -Fvh *rpm\nFehler: fehlgeschlagene Paket-Abh\u00e4ngigkeiten:\n        libartsbuilder.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsgui.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsgui_idl.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsgui_kde.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmidi_idl.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodules.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodulescommon.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmoduleseffects.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodulesmixers.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libartsmodulessynth.so.0 wird von kdeaddons3-sound-3.1.93-0 gebraucht\n        libfribidi.so.0 wird von kdegraphics3-3.1.93-0 gebraucht\n        libexif.so.9 wird von kdegraphics3-kamera-3.1.93-0 gebraucht\n        libartsbuilder.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsgui.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsgui_idl.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsgui_kde.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmidi_idl.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodules.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodulescommon.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmoduleseffects.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodulesmixers.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libartsmodulessynth.so.0 wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libkdeinit_noatun.so wird von kdemultimedia3-video-3.1.93-0 gebraucht\n        libgnokii.so wird von kdepim3-3.1.93-0 gebraucht\n        libvcard.so.0 wird von quanta-3.1-112 gebraucht\n"
    author: "spackenmann"
  - subject: "Re: SuSE 8.2 rpm missing libarys"
    date: 2003-11-06
    body: "I guess they are not surprisingly called libvcard, libgnooki, libexif, libfribidi and, what's causing the most entries, kdemultimedia."
    author: "Anonymous"
  - subject: "Niederdeutsch"
    date: 2003-11-06
    body: "Will KDe be translated to niederdeutsch (iso-code nds)?"
    author: "gerd"
  - subject: "Re: Niederdeutsch"
    date: 2003-11-07
    body: "No, it's not listed on http://i18n.kde.org/teams/ , but this shouldn't stop anyone from starting such a translation and letting it include in KDE when it has proven to be well maintained."
    author: "Datschge"
  - subject: "fantastic"
    date: 2003-11-07
    body: "What an incredible release! KDE 3.1 had become my desktop of choice after cycling through pretty much every other DE/WM out there. 3.2beta is so much better:\n- It's faster - more responsive generally, konqueror renders faster\n- It's easier to use - my few annoyances with 3.1 (mostly fitt's law violations) are gone\n- The new mac os style child-panel menu is great - it would be nice if it was clearer how to create one, though\n- it looks better - the UI has been tidied up all over the place. Even KMix is improved!\n- integrated apps greatly improved - juk and kontact are useful, kate is better than before. The new address book looks great"
    author: "Ben Roe"
  - subject: "Re: fantastic"
    date: 2003-11-07
    body: ">  it would be nice if it was clearer how to create one, though\n\nIndeed.. a lot of people are pretty confused about this particular feature. Perhaps it should prompt the user to create an empty panel with this applet if it is selected in kcontrol."
    author: "anon"
  - subject: "Re: fantastic"
    date: 2003-11-07
    body: "Yes, it's wohoo faster on my laptop. :)\nMany good improvments on GUI here and there.\nAnd I love the new open and save dialog too. More options and great icons."
    author: "JC"
  - subject: "Common Sound Server for KDE 3.4 & Gnome 2.6??"
    date: 2003-11-07
    body: "C'mon people... When are we going to see a common sound server that is used for both KDE and GNOME?\n\nThis ESD/Artsd mess is really painful for both users and developers.  Is there any chance of KDE adopting the GStreamer framework for KDE 3.4 or KDE4??\n\nAlso, a potentially good sound server could be MAS.  C'mon folks,  we need a common sound/multimedia subsystem for our desktops.  It matters not if you are a KDE or a Gnome person.  A common multimedia framework will only benefit both desktops!\n\nPLEASE!!!!! =)\n\n"
    author: "The Guru"
  - subject: "I would like to see that in KDE 4 too"
    date: 2003-11-07
    body: "A common sound server is needed and I heard that there were plans to replace arts anyway."
    author: "Alex"
  - subject: "Re: Common Sound Server for KDE 3.4 & Gnome 2.6??"
    date: 2003-11-07
    body: "> Is there any chance of KDE adopting the GStreamer framework for KDE 3.4 \n\nNo chance in hell:\n\n1. There probably won't be a KDE 3.4.. the next version of KDE will probably be KDE 3.3 followed by KDE 4.0.\n2. We can't possibly change away from arts until KDE 4.0, for compatability reasons.\n3. It probably won't be gstreamer that we switch to. Not only is it immature, but it basically has the similiar problems that arts does for KDE (uses glib, so kde developers can't hack on it much)\n\n"
    author: "anon"
  - subject: "Re: Common Sound Server for KDE 3.4 & Gnome 2.6??"
    date: 2003-11-08
    body: "GNOME is always GNOME;KDE is always KDE.\nyou can not use the two great desktop ENV at same time.\nSo you must choose one.\nI choose KDE, and never use anything related to GNOME.\nnever use xmms to play music(only because it like winamp?!)\n\nremove gtk+ from your system, if you need gimp-1.3.x, keep gtk2,\notherwise, remove it too, remove anything related to g*, and you will find that KDE can do anything.\nWe have no compatable problem.\n\n\n"
    author: "cjacker"
  - subject: "Re: Common Sound Server for KDE 3.4 & Gnome 2.6??"
    date: 2003-11-09
    body: "> you can not use the two great desktop ENV at same time.\n\nYou actually can run both environments at the same time. Do you know you can have more than one X server running? Right now, I have KDE 3.1 on :0.0 and KDE CVS on :142857.0. So, technically, you can run GNOME and KDE at the same time.\n\nFor the XMMS part, amaroK is just \"better\". KDE solutions are generally better (and better integrated) than GTK+ ones, so I agree with you in this point.\n\nHowever, I don't understand why this relates to a common media framework, what is indeed a very good idea."
    author: "Henrique Pinto"
  - subject: "Re: Common Sound Server for KDE 3.4 & Gnome 2.6??"
    date: 2003-11-27
    body: "If KDE won't move to Gstreamer (I really think a common MM framework is a benefit for both desktops).. How about at least adopting MAS as the sound server instead of Artsd?\n\nMaybe we can convince the Gnome folk to use MAS as well, and at least we would have a common Sound Server!\n\nPlease look into interoperability for USERS!\nPLEASE!"
    author: "Guest"
  - subject: "Re: Common Sound Server for KDE 3.4 & Gnome 2.6??"
    date: 2003-11-27
    body: "So what you're saying is... if KDE isn't prepared to move to a new mm framework (GStreamer), then we should instead move to a different new mm framework (MAS)?"
    author: "Stephen Douglas"
---
With the new year fast approaching, beta testing of the <a href="http://developer.kde.org/development-versions/kde-3.2-release-plan.html">upcoming KDE 3.2 release</a> has begun. The <a href="http://www.kde.org/">KDE Project</a> is excited to <a href="http://www.kde.org/announcements/announce-3.2beta1.php">announce the immediate availability of "Rudi"</a>, the first beta version of KDE 3.2. Sharing its name with the <a href="http://events.kde.org/info/kastle/">Kastle conference's</a> host coordinator, Rudi represents the culmination of the KDE Project's efforts to date.
<!--break-->
<p>As no further features will be added between the release of Rudi and KDE 3.2, this is an excellent opportunity to preview the <a href="http://developer.kde.org/development-versions/kde-3.2-features.html">most advanced and powerful KDE</a> yet. Packages may be downloaded from <a href="http://download.kde.org/unstable/3.1.93/">download.kde.org</a>. As of this writing there are sources and binaries for SuSE and Conectiva, but other distributions will follow. The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolkit was updated.
</p><p>With its 25 page feature plan, nearly 10,000 bugs closed and over 2,000 user wishes processed a complete report on what one can expect from Rudi would be so long that by the time you finished reading it, there would probably be a more current KDE release to try out. The only way to truly appreciate this release is to experience it first hand, but here are a few of the highlights anyways...
</p><p>Read on in the <a href="http://www.kde.org/announcements/announce-3.2beta1.php">full announcement</a>.</p>