---
title: "C|NET News.com: Germany-funded Linux software arriving"
date:    2003-01-31
authors:
  - "rkaper"
slug:    cnet-newscom-germany-funded-linux-software-arriving
comments:
  - subject: "KHTML Improvements..."
    date: 2003-01-31
    body: "Has anyone else had stability problems with Konqueror?  It worked fine for me with Release Candidate 6, but the final 3.1 crashed twice while I was giving it a test-drive.\n\nI think Apple's improvements should have all gone into 3.1, or they should have all waited until the next release.  I suspect the partial port is what's causing the problems."
    author: "Mikey"
  - subject: "Re: KHTML Improvements..."
    date: 2003-01-31
    body: "> Has anyone else had stability problems with Konqueror? It worked fine for me with Release Candidate 6, but the final 3.1 crashed twice while I was giving it a test-drive.\n \nNope. It's been rock solid for me. I had rc5 on one machine and rc6 on the other. They both have 3.1 final now and one khtml crash on rc5. These systems get a fair amount of use in our home business day and night.\n\n> I think Apple's improvements should have all gone into 3.1, or they should have all waited until the next release. I suspect the partial port is what's causing the problems.\n\nSuspect all you want... but compiling with --enable-debug and pointing to it is a lot better than wild speculation. Unfounded rumors start with wild speculation. A good question is if you compiled it yourself, what your optimizations are and so on, or was it precompiled in an RPM. In my experience both supporting my network, friends and thousands of users RPM packages can sneak in some nasty bugs. Even Mandrake and SuSE have released some real duds on new version releases.\n\nFYI mine is built with Gentoo for my processor with mild optimizations using gcc 3.2.1. I typically have tabbed browsers on several of my 12 desktops at any time. Hats off to Dirk and the rest of the guys! KDE 3.1 is awesome!"
    author: "Eric Laffoon"
  - subject: "Re: KHTML Improvements..."
    date: 2003-01-31
    body: "Very stable here since rc6.\n\nhttp://bugs.kde.org\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: KHTML Improvements..."
    date: 2003-01-31
    body: "I have with rc5,6 and 3.1 but only when using it as a filemanager."
    author: "Anon"
  - subject: "Re: KHTML Improvements..."
    date: 2003-01-31
    body: "Well, Konqueror looks less stable on my system, somehow KHTML is not loaded, calling DOCP does not work properly.\nMust be the rpm's of SuSE I installed, will check later if they have updated packages, or compile it myself :o)\n\nRinse\n "
    author: "rinse"
  - subject: "Re: KHTML Improvements..."
    date: 2003-01-31
    body: "The SuSE 8.1 RPMs for 3.1 work fine for me. I noticed a real speed increase over the RPMs shipped with 8.1. Also 3.1 is stable for me. No crashes yet on my two systems: the one mentioned and a SuSE 8.0 system on which I compiled from sources with the default konstruct settings.\n\nSome annoying bugs that had me worried in rc6 have gone.\n\nIn my opinion 3.1 is pretty stable and I haven't found any really annoying bugs."
    author: "Jos"
  - subject: "Re: KHTML Improvements..."
    date: 2003-01-31
    body: "Hi everybody, \n\nI had stability problems with konqueror since 3.0 (I thought), but in the end it all boiled down to kbear. I never had kbear running smoothly, and it attaches itself to konqueror when installed (or its sitemanager does...). So, I had regular crahses when opening a new windows by clicking a link with the middle mouse button. But after removing kbear it runs without a hickup. So you might want to check whether you have kbear installed.\n\nMy system is SuSE 8.1 running the rpms from ftp.kde.org, the system also runs WinXP via vmware and is normally up from Mon 8:00 to Fri 18:00, with quite a bit of load to handle.\n\nCheers\n\nNoName"
    author: "NoName"
  - subject: "Re: KHTML Improvements..."
    date: 2003-02-06
    body: "yeap, i also had RC5 before and it worked just fine, but the crashed several times since i installed it ...\n\nalso some programs that compiled and ran fine under RC5 don't compile any more or crashes. also kget doesn't work.\n\n.costin\n"
    author: "Costin"
  - subject: "Re: KHTML Improvements..."
    date: 2003-02-06
    body: "correction : ... but the *final version* crashed ...."
    author: "Costin"
  - subject: "*Please* change the Name"
    date: 2003-01-31
    body: "Kroupware is really a horrible name. I know that it is only a project name and not supposed to be a final name, but now that it gets reviewed by major news sites it is important to change the name.\n\nThere was some discussion on the dot about the final name of the project recently. Just take the 10 best proposals from there and let the dot.kde.org users choose the winner. Or make a poll at kde-look.org. Or pick one at random.\n\n*Everything* is better than Kroupware.\n\nPlease note that this is meant as constructive criticism. I downloaded the 3.1 release and I am extremely happy with it. But names are more important than most developers think.\n\nregards,\n\n    A.H."
    author: "Androgynous Howard"
  - subject: "Re: *Please* change the Name"
    date: 2003-01-31
    body: "The Server is called Kolab and so is the client (for the time Kroupware Projekt). The Clinet application that will go into KDE 3.2 is called Kontact.\n\nSo what we end up with is Kolab Server and Kontact client. No more Kroupware.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: *Please* change the Name"
    date: 2003-01-31
    body: "> So what we end up with is Kolab Server and Kontact client. No more Kroupware\n\nI guess the server doesn't depend on KDE. Does the name then have to start with \"K\" ?\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: *Please* change the Name"
    date: 2003-01-31
    body: "Why should it have to start with a K? Other KDE apps (like Noatun and Quanta) dont. It is just a tradition and makes it easier to categorize apps.\n"
    author: "AC"
  - subject: "Re: *Please* change the Name"
    date: 2003-01-31
    body: "If this whole K-naming is going to continue, it would make sense if the icon search dialogs sorted by the second initial letter. As it is, finding an icon for say, KOffice, is quite irritating. You type 'k' which will take you to the beginning of the K icons, which is basically all of them. Typing in 'o' moves you to the icons that begin with 'o'."
    author: "Rayiner Hashem"
  - subject: "Re: *Please* change the Name"
    date: 2003-02-01
    body: "Hmm......... are you sure? For file/icon selectors in KDE, I have always been able to type the entire name of the file to find it if it's there (not that you really need to go beyond the first few letters). Koffice is a bad example in itself, since there is no \"koffice\" icon (only kword, kspread, etc), so as soon as you type the first \"f\" it goes to \"f\" icons. For me, though, getting the kword icon is as easy as K-W-O :)"
    author: "optikSmoke"
  - subject: "Re: *Please* change the Name"
    date: 2003-01-31
    body: "I think you're thinking the wrong way round here.\nNot all kde apps start with a K, and reversely, we have no reason to forbid non-kde apps from starting with a K :)\n\nKolab comes from Kollaborieren which is German for Collaborate.\nNothing to do with K==KDE indeed.\n"
    author: "David Faure"
  - subject: "Re: *Please* change the Name"
    date: 2003-01-31
    body: "And then: I don't see people complain about the 'x' in xteddy, xpinguin, xbill, xedit, xless, xgamma, xbanner ... It's a kind of namespace identifier, and helps to avoid name clashes.\n(Gnome will never let any of their programs start with a 'k'. ;-)"
    author: "Melchior FRANZ"
  - subject: "Konqueror Stability"
    date: 2003-01-31
    body: "Hi All\n\nWell I myself have had a couple of crashes on SuSE Professional 8.1, using RPMs as my install approach. Personally though I think this is very good. \n\nMy use of Konqueror is extreme, it is open by default at all times on Desktop 1 as a file manager, and on Desktop 2 & 3 as a web browser, for remote administration tasks and also local development testing. This means  on average that Konqueror is up and running for anything between 14 & 18 hours day in day out, 2 crashes to me given that sort of use equals ROCK SOLID (Hell even rocks break sometimes !! ).\n\nA very very BIG thank you to all of the KDE developers, 3.1 has a damned near perfect balance of new features and massive bug fixes right across the application spectrum.\n\nI am very impressed and immensly grateful to you all, also I must just say Quanta team keep it up it is getting better and better. I now use it as my default Code editor, but I will admit I still use Dreamweaver MX for layout and management.\n\nSay does anyone know where I should post a feature request for MetaData features in KDE, you know the sort of stuff, Data about Data, notes on and describing files that are directly associated with individual files. I sure would love to have that in KDE sometime, though obviously I do realise something such as this would be huge undertaking.\n\nMuch respect and gratitude\n\nPaul"
    author: "Paul"
  - subject: "Meta data"
    date: 2003-01-31
    body: "> Say does anyone know where I should post a feature request for \n> MetaData features in KDE, you know the sort of stuff, Data about Data, \n> notes on and describing files that are directly associated with individual \n> files. I sure would love to have that in KDE sometime, though obviously I \n> do realise something such as this would be huge undertaking.\n \nThere is some support for meta data already. You can read and manipulate (as possible) meta data of all kinds of fileformats through a generic interface. You can't attach any kind of metadata to any kind of file yet, tho. Currently, you can utilize some fileformat's structures, like Jpeg EXIF data, mp3 ID3 tags etc.\n\nAttaching data to any kind of file requires support in the filesystem (see lengthy discussions on lists.kde.org, kde-look)."
    author: "Carsten Pfeiffer"
  - subject: "Re: Konqueror Stability"
    date: 2003-01-31
    body: "It crashes and you call it ''rock solid''. Now what does that tell us about the state of software?"
    author: "jonas"
  - subject: "Maybe the German govt will switch to KDE?"
    date: 2003-01-31
    body: "That's great!\nThe German government now funds two projects, \u00c4gypten (http://www.gnupg.org/aegypten) and Kroupware. Maybe some parts of the German government will switch their Desktops to KDE, when both projects are finished."
    author: "Anonymous"
  - subject: "Re: Maybe the German govt will switch to KDE?"
    date: 2003-01-31
    body: "Many parts of the \u00c4gypten project have actually been merged into the Kroupware project. It is from \u00c4gypten that KMail now has SMIME support."
    author: "Anonymous Coward"
  - subject: "Correction: Nothing from Kroupware is in KDE 3.1"
    date: 2003-01-31
    body: "Hi everybody!\n\nThe C|Net article claims that \"the first elements [of Kroupware] have appeared in the new KDE 3.1\"[1]. That's (unfortunately) wrong. As you can check yourself cvs was \"frozen for feature commits that are not listed in the planned-feature document\"[2] on July 1, 2002 while the Kroupware \"project began in September.\"[1]. So it wasn't possible to include anything from the Kroupware project in KDE 3.1.\n\nIn particular the article claims:\n\"Two elements of the client work are in the new KDE 3.1, released Tuesday: the KMail software can handle encrypted e-mail attachments, and the KOrganizer calendar software can communicate with Exchange 2000 servers.\"\n\nBoth elements are not part of the Kroupware project.\nThe KMail improvements, i.e. support for PGP/MIME (RFC 3156) and S/MIME, were made by the \u00c4gypten project[3] (which incidentally also was ordered by Germany's agency for information technology security).\nThe KOrganizer plugin[4] for connections to Microsoft Exchange 2000\u00ae servers was written by Jan-Pascal van Best completely independant of the Kroupware project.\n\nAnyway, you can all look forward to KDE 3.2 which will include most (if not all) of the client side elements of the Kroupware project.\n\nRegards,\nIngo\n\n[1] http://news.com.com/2100-1001-982816.html\n[2] http://developer.kde.org/development-versions/kde-3.1-release-plan.html\n[3] http://www.gnupg.org/aegypten/index.html\n[4] http://korganizer.kde.org/workshops/ExchangePlugin/en/html/index.html"
    author: "Ingo Kl\u00f6cker"
  - subject: "konqueror 3.1 *still* doesn't honor nowrap"
    date: 2003-01-31
    body: "I tested KDE 3.1 from RC5 through current and konqueror still doesn't honor nowrap in <td> elements.  I would think that this is important.  I have voted for a bug report that was submited to bugs.kde.org and 4 other people have also voted for this.  The real kicker (no pun intended) is that this is a backward step from KDE 3.0, it works just fine with nowrap.  Not having this makes some webpages just plain unusable.  Please read the bug report and test for yourself, you will see what I mean.  \n\nThank you.\n\n\nBret.\n\n\nReference:\nhttp://bugs.kde.org/show_bug.cgi?id=52993"
    author: "Bret Baptist"
  - subject: "Re: konqueror 3.1 *still* doesn't honor nowrap"
    date: 2003-02-03
    body: "How about using <nobr> instead?"
    author: "Datschge"
  - subject: "Re: konqueror 3.1 *still* doesn't honor nowrap"
    date: 2009-01-09
    body: "That does not work."
    author: "Tobias"
  - subject: "Bug??"
    date: 2003-01-31
    body: "Anyone tried this site : http://www.telefoongids.nl\nTo look up a telephonenumber it is requiered to start\nthe name and the placename with a capital , but konqueror\nseems to inetrpret the pressing of the shift-key is an \"enter-key\",\nall other browsers work fine with this site.\nNext problem site: http://www.omroep.nl/nos/noshome/index.html\nIt is not capable of rendering it correctly , all other browsers do.\nThese are only a few examples of the many usefull sites in the Netherlands\nthat simply don't render/work correctly in konqueror while mozilla, phoenix opera etc. do a fine job of rendering them. What's wrong with konqueror??\n"
    author: "wb"
  - subject: "Re: Bug??"
    date: 2003-01-31
    body: "I had none of your problems when visiting these two sites.\nCan you tell what exactly doe not work on the second one ?\nThe first one worked well, what's wrong with your KDE install ?"
    author: "Ookaze"
  - subject: "Re: Bug??"
    date: 2003-01-31
    body: "My mistake, you are right ..."
    author: "Ookaze"
  - subject: "Re: Bug??"
    date: 2003-02-01
    body: "The second site doesn't render completely in konqueror.\nOnly the background comes up."
    author: "wb"
  - subject: "Re: Bug??"
    date: 2003-02-08
    body: "1. Page checked by w3c-validator as html 4.01 Transitional: 324 HTML-Errors in 780 lines, wow!\n2. Page as html 4.01 Frameset, 28 HTML-Errors in 33 lines (Frameset only)\n\nThis pages are extremely broken."
    author: "Manfred Tremmel"
---
<a href="http://news.com.com/">C|Net</a> has
<a href="http://news.com.com/2100-1001-982816.html">published</a> a
story about the recent release of <a href="http://www.kde.org/announcements/announce-3.1.html">KDE 3.1</a>, with a focus on the <a href="http://www.kroupware.org/">Kroupware</a> project. Other new features
particularly beneficial for those considering rolling KDE out in an enterprise
setting are mentioned as well, including remote desktop administration, KDE's <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/README.kiosk?rev=HEAD&content-type=text/x-cvsweb-markup">kiosk framework</a> and the <a href="http://dot.kde.org/1041971213/">KHTML improvements</a> that made their way into KDE as result of the cooperation with Apple.

<!--break-->
