---
title: "KDE-CVS-Digest for October 3, 2003"
date:    2003-10-04
authors:
  - "dkite"
slug:    kde-cvs-digest-october-3-2003
comments:
  - subject: "Thanks a lot!"
    date: 2003-10-04
    body: "As uaslly -- great work, Derek! "
    author: "gray"
  - subject: "K Girlfriend Usability Study..?"
    date: 2003-10-04
    body: "C'mon, you knew someone would ask..  =)"
    author: "Hoek"
  - subject: "Re: K Girlfriend Usability Study..?"
    date: 2003-10-04
    body: "That was just a joke; it should read \"KStars Girlfriend Usability Study\".  One of the developers sat his girlfriend in front of KStars to see how well she could understand the interface.  We got lots of good feedback from our GUS, and I heartily recommend it (or a BUS, of course) to other projects ;).\n"
    author: "LMCBoy"
  - subject: "Re: K Girlfriend Usability Study..?"
    date: 2003-10-04
    body: "Over the last year there were a few similar comments in commit logs. Jason just gave it an acronym. And yes, it works.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: K Girlfriend Usability Study..?"
    date: 2003-10-04
    body: "Why stop with girlfriends? Take your cousins, uncles/aunts, grannies, etc. etc. An app like KStars is ideal for usability studies, since it's much more entertaining for general public than spreadsheets and the like.\n\nI hope results of these studies are applied to more desktop apps. It's a wonderful idea :)"
    author: "Random coma"
  - subject: "Re: K Girlfriend Usability Study..?"
    date: 2003-10-05
    body: "Recommend? You wanted to say \"rent\", or? :-)"
    author: "anon"
  - subject: "+1, Insightful"
    date: 2003-10-04
    body: "Derek,\n\nThank you for your insightful commentary in this week's CVS Digest!  You nicely encapsulate some of the concerns I've felt recently.  I think there is a growing sense of entitlement among some KDE users, and I think you are absolutely right that this sentiment has the power to damage our community, by alienating or even driving away contributors, and widening a perceived gap between \"developers\" and \"users\".\n\nI encourage everybody who uses KDE to contribute to the project however you can.  Don't just use KDE; become a member of our community.  Don't just complain; help!  More developers are always a good thing, but even if you don't code, there is plenty to do.  You can help bring KDE to a wider audience by joining one of the 40+ translation teams.  You can improve documentation by writing Handbook chapters.  You can improve usability by redesigning a dialog with Qt Designer.  You can checkout KDE-CVS and submit detailed reports of any bugs you find, or submit specific, well-formulated wishlist items.  You can check old bugs at bugs.kde.org to see if there are some that may be closable.  These are just some of the many ways you can join the KDE project.\n\nI agree with Derek that the Gentoo forums provide an excellent model for how we can effectively eliminate the growing gap between \"developers\" and \"users\".  We need active forums where the community can help itself, and really feel like part of the project, rather than the recipient of the project's result.  We have kde-forum.org; maybe this can become our community commons.\n"
    author: "LMCBoy"
  - subject: "Re: +1, Insightful"
    date: 2003-10-04
    body: "There is something strange going on in Gentoo. Who, three years ago, would have thought  that one of the most popular distributions would require half a week to install, require editing system files. And long overnight compiles to install large packages, with all the uncertainty that goes with that. I had the experience a while ago of walking a first time linux user through the process of compiling his kernel, to make sure his ethernet module would load. He stuck with it, and learned a whole lot in the process.\n\nThe kde i18n community is vibrant and active. The developer community (core, multimedia, development tools, games) is vibrant and active. The eye-candy community is active and busy (kde-look). The user community needs a little bit of life, or maybe a center of gravity.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: +1, Insightful"
    date: 2003-10-04
    body: "There are two places where KDE users can help build something like Gentoo's forums, but they are almost ignored by the KDE project. The kde-forum.org and KDE Community Wiki sites are the perfect launch pad for a proper community side to KDE... why doesn't www.kde.org link to them, promote them, lend resources?\n\nIf the KDE project took these resources seriously, lent them credibility, and maybe got a few developers to visit them in their spare time, they could grow and take a lot of the load off developers' backs."
    author: "Tom"
  - subject: "Re: +1, Insightful"
    date: 2003-10-04
    body: "www.kde.org links to kde-forum.org as long ago I can remember!?"
    author: "anon"
  - subject: "Re: +1, Insightful"
    date: 2003-10-04
    body: "I'm sorry, but there is no \"they\" here. \"They\" are quite busy preparing a release, fixing bugs. Any spare developer time would probably be better used sorting through the morass at bugs.kde.org.\n\n#kde on irc is usually empty. Some developers watch and respond. Where are the many users, many quite technically proficient, that could help with questions? If I have a question regarding kde use, Waldo, Dirk, George, or others shouldn't have to answer, although they probably would. It is so easy to say maybe the developers could spend more time on this or that. BZZT. Wrong answer.\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Re: +1, Insightful"
    date: 2003-10-04
    body: "Sorry, but it is precisely my point that by taking a couple of minutes to promote the forum and community wiki on the web site a little, the developers could continue to focus on coding and not have to worry about the problems you highlight, the most prominent of which is a Gentoo-like support community for KDE.\n\nIt would be nice if the whole KDE coding team could contribute 100% of their time to coding, but I think giving a tiny amount of that time over to helping a community establish itself is not only the right thing to do, but sensible in the long run. Half an hour given over to some links, news articles and maybe a redirection url (e.g. wiki.kde.org -> the wiki site) and the developers would then be able to return to coding. That's not much to ask."
    author: "Tom"
  - subject: "Re: +1, Insightful"
    date: 2003-10-09
    body: "To some extent, I is \"they\".  Why, because I realize that I can learn a lot by answering other.s questions -- learn things that will help me in writing code and fixing bugs.\n\nI was spending time \"sorting through the morass\" of bugs.  But, I had problems determining the proper criteria to close a bug.  I am starting to wonder if there is any point in submitting a bug for anything by HEAD.  But, I guess that it is of some help to test bugs to see if they have been fixed in the current BRANCH.\n\nA small suggestion for the morass.  Perhaps there should be a separate section in Bugzilla to submit bugs for HEAD and such bugs should expire after a certain length of time (e.g. 90 days), or the ones filed for HEAD before the BETA is tagged should all be deleted when this new BRANCH is released (because they are then obsolete).\n\nI don't use IRC, but I usually answer those questions that I can on: 'kde-linux' once a day.\n\nIt is good to see the some of the main developers are reading the list.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: +1, Insightful"
    date: 2003-10-05
    body: "> why doesn't www.kde.org link to them, promote them, lend resources?\n \nI added KDE Forum to the front page once it was started.\n\nTo have KDE Wiki linked as well, it would be nice if someone could send a patch to webmaster@kde.org\n\nI am not sure about forum.kde.org or wiki.kde.org, but anyway, it is also best discussed by sending a mail to webmaster@kde.org"
    author: "Olaf Jan Schmidt"
  - subject: "Re: +2, Insightful"
    date: 2003-10-04
    body: "> We need active forums where the community can help itself, and really feel like part of the project, rather than the recipient of the project's result. We have kde-forum.org; maybe this can become our community commons.\n \nAnd don't forget http://kde.ground.cz/tiki-list_faqs.php for FAQs, part of the KDE Community Wiki Site."
    author: "Anonymous"
  - subject: "Broken display?"
    date: 2003-10-04
    body: "Hello,\n\nI have no \"refer to\" links this time. Probably something in your script is broken, Derek?\n\nAnd in KDE3.2 Alpha2 the display is broken of: \"commited a change to kdevelop\", the text overlaps defeating readability. Ok in mozilla. Whose bug would that be?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Broken display?"
    date: 2003-10-04
    body: "When I prepared the digest, bugs.kde.org was down.\n\nThe display bug has been there for a month or so. The <strong> tag seems to overlap.\n\nDerek"
    author: "Derek Kite"
  - subject: "Kaboodle"
    date: 2003-10-04
    body: "Did i read it worng, or is Kaboodle back in CVS?"
    author: "cmf"
  - subject: "Re: Kaboodle"
    date: 2003-10-04
    body: "It's back."
    author: "Anonymous"
  - subject: "What happened?"
    date: 2003-10-05
    body: "I thought that there was a controversey about some political comments and the developer packed his bgs and left.\n\nAnywa,y if he's back, welcome abck, maybe we'll see mostfet soon too, at least to see why he's been gone if nothing else."
    author: "Alex"
  - subject: "Re: What happened?"
    date: 2003-10-05
    body: "It did go straight back in but with a new maintainer. Since it was under the GPL (I think, or at least a similar license that allows this...) anyone can take the source and repackage it themselves. It's no different to Mandrake, SuSE etc. taking KDE and making slightly modified RPMs of it..."
    author: "Tom"
  - subject: "Gideon was renamed back to KDevelop"
    date: 2003-10-04
    body: "Hi, great review. but you forgot to mention that gideon changed it's name back to KDevelop. This makes KDevelop not play well (read run in pararllel) with KDevelop 2.x.\nTherefore this was a big change. The other thing was that KDevelop got it's code restructured.\nTake a look at \nhttp://lists.kde.org/?l=kde-cvs&m=106511212217782&w=2\nfor more details.\n\nA third thing is that finaly KDevelop has it's API documentation on-line:\nhttp://developer.kde.org/documentation/library/cvs-api/kdevelop/html/index.html\n\nThanks\nAmilcar"
    author: "Amilcar Lucas"
  - subject: "Derek, Great Comment"
    date: 2003-10-05
    body: "I loved your comment, very true and eloquent. \n\nIn addion to that, please check out this wishlsit I submitted a few weeks ago: http://bugs.kde.org/show_bug.cgi?id=63868\n\nIt explains many ways in which KDE can increase uits monetary donations."
    author: "Alex"
  - subject: "Re: Derek, Great Comment"
    date: 2003-10-05
    body: "There are provisos to put a request for donations on the frontpage as this is a volunteer and non-profit project."
    author: "anon"
  - subject: "Not really, I don't see any rule for this"
    date: 2003-10-05
    body: "I'm sure the project is more important than any provisos, please reaed the full wish if you haven't."
    author: "Alex"
  - subject: "kmail + embedded vimpart for editor feature?"
    date: 2003-10-05
    body: "Hello - anyone know whether the kmail feature of embedding\nvim or kate as the default composer editor is still going to be\nincluded into kmail 3.2?  The KEditor kpart component along\nwith the vimpart are already included in kde 3.2, but unfortunately,\nI'm still unable to use vim with kmail - which is one of my most\nanticipated features for 3.2.\n\n( The 'use external editor' option is available, but it is not what I'm\n  after. I'm using the latest cvs as of last friday or so .)\n\nThanks for any status!"
    author: "Corey"
  - subject: "Re: kmail + embedded vimpart for editor feature?"
    date: 2003-10-05
    body: "I went to the kmail devel list archives and searched around and found this thread,\nwhich answers my question:\n\nhttp://lists.kde.org/?l=kmail&m=106339162511179&w=2\n\nAck!  No vim/kate for kmail yet!  I am crushed... \n\nApparently the module that will be responsible for this feature, will be called\nKomposer - and it's not going to be finished for the release of KDE 3.2... <sniffle>\n\nIn case anyone else is as interested in this particular feature as I am, I'm gonna \nmake a query to the kmail list and see if we could at least perhaps get access to a\npatch that we could apply ourselves to get this functionality until it's finally released\nas stable.\n\n\nBeers,\n\nCorey"
    author: "Corey"
  - subject: "Re: kmail + embedded vimpart for editor feature?"
    date: 2003-10-05
    body: "Here's the response from Zack, the kmail dude who is responsible for this particular \nfeature:\n\nOn Friday 03 October 2003 22:14, Corey Saltiel wrote:\n> Hello!\n>\n> I was really dissapointed (read: super-mega ultra bummed ) to learn\n> that kmail 3.2 would likely not include the selectable alternate\n> embedded editor option for composing mail - it was THE feature that I\n> was waiting for with baited breath...\n>\n> Anyhow, I understand how things go, and I wish I could help out - but\n> I doubt I could be of any use... SO - what I'm wondering is whether\n> there is anyway possible of getting a patch or something that I could\n> apply for myself that would provide this functionality to me until\n> this Komposer interface becomes stable and officialy integrated.\n>\n> My desire is to finally be able to edit/compose my mail with an\n> embedded vim, rather than with the 'external editor' option. I recall\n> back around janjuary or so, there was a keditor patch floating around\n> that would work with kmail 3.1. Would an updated version of that\n> original patch provide a solution, or has the latest codebase changed\n> too much? Otherwise, what is the likelyhood of getting/using a\n> semi-working Komposer patch?\n>\n> Thanks alot for any help or info - you guys are doing a great job;\n> the new threading works wonderfully, which was one my last issues\n> with using kmail as my mailer rather than mutt. The embedded vi thing\n> is now the only item on my personal wish-list.\n\nI don't yet having a patch for using Komposer in KMail and KNode. \nProbably sometime this month I'll finish the missing pieces of the \nKomposer and write the patches to use it in both applications. I'll \nsend an email announcing when it will be done to let the few willing \ntest it out. I can't give you a time estimate because I have a lot of \nother stuff that has to be finished in time for 3.2 and it's higher \npriority right now.\n\nZack"
    author: "Corey"
  - subject: "damn...slow.....startup......times"
    date: 2003-10-05
    body: "I'm sitting here with a completely new system (Athlon XP 2500+ based) after upgrading from a pretty old one from 1998 and.. konq still seems to start up slow as hell, and KDE in general does too.  \n\nRight now the only thing I have running is noatun, and using my stopwatch, starting up konq takes about 20seconds..argh..  Is that indicative of a serious problem or is it really just that slow for everyone?  Just tried to upgrade to QT-3.2.1 and have the same problem (except now text in Konsole is microscopic).\n\nIs it going to be much better in KDE 3.2?    I compiled kde 3.1.4 in about 4.5hours and was really pleased, but actually using it doesn't \"seem\" any faster than using my old shitteron 533. :)\n\n"
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "Something is definitely wrong with your setup.  I have an Athlon 1700, and Konq starts in only a few seconds for me.\n"
    author: "LMCBoy"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-06
    body: "I think so too.  I just tried booting test6-mm4, and was pretty stunned at how fast it started up and restored my session..  Buuut, I had forgotten to compile in my NIC driver, so I did that and rebooted....and am now here, and it's back to slow-as-hell as it was in 2.4.22.  No idea why..  ugh\n"
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "3.2 is noticeably faster. Especially konqueror startup. Quite impressive.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "My dusty old 400 MHz AMD K6-2 system with 128 megs of RAM starts Konqueror in 10 seconds. I remember KDE having very long start-up times in some versions of RedHat before I switched to Debian. Speed has been acceptable ever since."
    author: "Anon"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "There is definitely something wrong there; you must have done something very odd when compiling it, like passing it the wrong CFLAGS maybe? I'd either try and use your distributor's packages, if they exist, or just using the latest version of your distributor's packages, or perhaps getting hold of the Gentoo KDE ebuilds and looking at how they compile KDE to work out how you can do it better.\n\nThe only problem I have with KDE loading times now is loading KDE itself initially. From the framebuffer to loading X to loading KDE it takes about 10-15 seconds on my P4 2GHz which isn't too bad, but I'd like to cut it down. I've been experimenting with preloading parts of KDE in my local init script, but I've not had any luck so far :-/"
    author: "Tom"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "20s? Try prepackaged RPMs (or whatever your distro uses), or maybe try a different distribution or Knoppix."
    author: "AC"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "try con kolivas patches for the kernel (2.4.22). It made a huge difference to me.\n"
    author: "Kar"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "Check whether your hostname resolves. Startup times that long usually mean network misconfiguration. My P150 laptop can start Konqueror in around 5 seconds with minimal tuning, BTW.\n"
    author: "Sad Eagle"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "This looks very much like once when I deleted the loopback interface. It happens in Mandrake if you disable the service called \"network\"."
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "How would one know if they've got this kind of problem / how to fix it?"
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-06
    body: "Run ifconfig in console... (it's usually in /sbin or /usr/sbin). If it doesn't list \"lo\" then you have a problem."
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-07
    body: "Ah, thats what I thought.  Unfortunately, thats not the problem either...sigh"
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-07
    body: "run ltrace and strace on it. i had a problem similiar to yours and it ended up being fontconfig spending 15 seconds rewriting fc-cache.. i just ran fc-cache as root and it worked fine."
    author: "anon"
  - subject: "Re: damn...slow.....startup......times"
    date: 2007-04-25
    body: "That totally worked for me in kubuntu 6.06:\n\nsudo fc-cache\n\nNow xterms come right up instead of clicking the xterm button twice and getting two xterms 5 seconds later.  Remains to be seen whether this is a permanent, once-per-login or once-per-init fix.\n\n-Greg"
    author: "Greg Brennan"
  - subject: "Re: damn...slow.....startup......times"
    date: 2007-06-03
    body: "AAAAAARRRGHHH!\nThank you, fc-cache did the trick for me, too (KDE 3.5.4 self compiled).\nSuddenly KDE took very long to startup and I had no idea why. Searching the web didnt help till I found this thread, maybe because \"KDE slow startup\" is not really a specific search token. Maybe reason and solution should be included in some faq?"
    author: "null"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-05
    body: "You've definately got a problem there. My machine (2GHz P4 laptop) is much slower than yours, and Konqueror starts in about 2 seconds. Check /etc/hosts and make sure that there is an entry mapping your hostname to the loopback device. Thus, if you're hostname is 'foobar', then you should have an entry:\n\n127.0.0.1\tfoobar"
    author: "Rayiner Hashem"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-06
    body: "That was my first guess after posting, but isn't the problem."
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-06
    body: "I got an Athlon 2500+ system too the other month. To get it up and running quickly, I just went with the Gentoo binary packages for KDE-3.1.x\n\nWROOOM!!\n\nKonqueror cold-started in less than 1 sec. :)\n\n\nSomething really is off on your system. 20 sec sounds like some kind of network timeout."
    author: "anonon"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-06
    body: "More interesting especially since I'm also using Gentoo..  Although not with precompiled binaries, but built with the kde 3.1.4 ebuilds."
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-20
    body: "I had the same problem and managed to solve it.\nI noticed that when I was dialed-up the slowness didn't happen, but when disconnected it did.\n\nThe difference was that I was running Firestarter firewall when online, but not when offline.\n\nI found I also had running Shorewall firewall from installation and somewhere along the way I had enabled all filtering.\n\nWhen I disabled Shorewall, KDE apps all started within appropriate times.\nSo effectively Firestarter was overriding Shorewall settings allowing things to work.\nI need to investigate further, but perhaps the font server was being blocked by the firewall.\n\nCheers."
    author: "Anonymous"
  - subject: "Re: damn...slow.....startup......times"
    date: 2003-10-25
    body: "I've been wondering for a long time why even the smallest KDE apps used to take seconds to start. I have just noticed that during startup of each program, KDE *accesses the network* for some reason (doing DNS lookups, direct or inverse I guess).\n\nWhen I unplug my network cable, applications take around 20 seconds to start. When I plug it back in they take around 3 seconds (and I see blinking lights in my hub during that).\n\nI have just solved that by adding some entries in /etc/hosts. Now it does not access the net, and apps start in less than a second.\n\nFor some reason my system once did an inverse DNS lookup and found that my local IP address (I use NAT) 192.168.1.1 had a corresonding name out there in my ISP's DNS server (say \"somename.somedomain\"). Now the console prompt and \"uname -a\" display that name instead of what I manually set in /etc/hosts and other places.\n\nSo I added to /etc/hosts:\n192.168.1.1 somename.somedomain  somename\nAnd it goes nice now!\n\nI have no idea why :-) [I'm a Mandrake 9.1 user an I know there's a known bug regarding this]\n"
    author: "Alvaro Segura"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-02-09
    body: "Same here. Gentoo user, compiled kde 3.1.4 (& now 3.2.0) from source and have slow start up times on all apps. I've ran strace and confirmed that it is fontconfig scanning all of my fonts. However, fc-cache as root does not fix it. Permissions are okay on all files. 75dpi and 100dpi dirs are included in the scanning. Don't know what other info I can give..."
    author: "Jason Stubbs"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-03-19
    body: "I have Gnome, KDE, win mgrs when I startup. Gnome is reasonably fast.\n\nWith KDE 3.2, after \"starting system services' for maybe 2 minutes.. it just seems to hang on the desktop and KDE doesn't start. Only way I can usually start it is go ho to a shell and try stopping X and then eventually can get into KDE. Any ideas?"
    author: "rockee"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-03-19
    body: "sounds like a BORKED /etc/hosts"
    author: "lolmessedup"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-03-19
    body: "I agree - can you give me some help..plz...\n\nIn my hosts file, I added this entry at the beginning:\n\n127.0.0.1 localhost machinename.mydomainname.com nachinename\n\nnext entries were there and I left alone:\n::1 localhost ip6-localhost ip6-loopback\nrest are the fe entries"
    author: "rockee"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-08-07
    body: "WOW !\n\nI've been having this problem for weeks now, and this article pointed to the right direction !!\n\nmy /etc/hosts file was the faulty part!\nit simply had the 127.0.0.1 pointing to localhost\n\nit was missing the addition of 192.168.0.1  myrealhostname\n\nas soon as I changed this file, apps launched much much faster!\n\nNow, I really must know how to adapt this configuration to my usage needs, because I frequently change network config back and forth: home, office, client.\n\nand I really don't feel like changing /etc/hosts everytime...\n\nanyway ! many many thanks for pointing in the right direction , at least in my case."
    author: "Shafik"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-10-22
    body: "I have know for quite some time that DNS queries slow down KDE.  I first noticed the problem when I didn't have internet acces after I moved.\n\nI have seen here and on other website the suggestion to add a line with the host IP adress and hostname in /etc/hosts and making sure you have \"hosts=local,bind\" in your /etc/resolv.conf file to make sure that you query /etc/hosts before making a DNS query.\n\nThis does solve the problem, but not at source.\n\nFirst of all, does anyone know why KDE does so many DNS queries while starting up or while launching KDE applications?  Is it for logging application events with the host name?  If it is, then why know make a single quer while KDE boots and cache it?  It seems unlikely that the hostname will change while you are normaly using your computer!\n\nThis brings me to the second problem : What happens if your are using DHCP?  I happen to use DHCP in my home setup and I frequently add and remove many computers from my network.  Therefore, the IP adress allocated to a given box change from time to time.  I don't whant to have to update my /etc/hosts file each time I boot a computer to make sure that KDE will be able to resolve the hostname.  Furthermore, what happens if your lease expires while your computer is working and you get a different IP?\n\nIf anyone knows a place I could find the settings for KDE DNS queries, please tell me!\n\nWhile I do not provide solutions to the problem, I hope this message will benifit everyone.  It is true that you could use the DHCP hoocks on dhclient to run a script to update /etc/hosts but I feel that doing so s a big ugly patch."
    author: "prozzaks"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-10-22
    body: "Sounds like you have not got your localhost DNS entry set correctly.\n"
    author: "Richard Moore"
  - subject: "Re: damn...slow.....startup......times"
    date: 2007-02-05
    body: "This was posted over two years ago, and the problem seems to have popped up again.  Who would have thought that KDE and GNU/Linux would have old performance issues (that should by all accounts have long since been fixed) suddenly reappearing just in time for M$ Vista to be launched?\n\nI don't believe it's just a coincidence.  Free software projects have been vandalized before, and they will be again.  For example, OpenBSD's \"Only one remote hole in the default install, in more than 10 years!\" was the result of deliberate vandalism.  And why do you think M$ has an internal Linux lab?  Is it to promote greater interoperability between M$ Windows and GNU/Linux?  I think its purpose is more sinister.\n\nI think there is something fundamentally wrong with the way name resolution is done in GNU/Linux and all the BSDs, and it always comes back to haunt us (lest one should forget the Halloween documents):\n\nThe whole process depends critically on two system files, /etc/resolv.conf and /etc/hosts.  These files are in constant danger of getting clobbered by any number of processes, (and dhclient is a major culprit.)  There is a mishmash of network configuration GUIs in both Gnome and KDE that don't do what they say they do, and generally just bork things up.  Nobody seems to care, and these GUI apps never get fixed.  In particular, KDE likes to really mess up /etc/network/interfaces on my Debian system.\n\nSecond, too many applications (as the parent noted) depend all too critically and unnecessarily on proper and speedy name resolution for their basic functioning.  Why does a Web browser (namely Konqueror) need to do forward and reverse lookups on the current hostname and wait for DNS resolution timeouts before it even starts up?  I don't know what the purpose of these checks is, but the user is not alerted to anything amiss when these lookups fail.  The startup of even the simplest application is delayed by many seconds for no good reason.\n\nThe problem is even deeper than a mere inconvenience, however frustrating that inconvenionce may be.  I once had an OpenBSD machine with misconfigured name resolution, and \"localhost\" somehow resolved to \"localhost.net\", which, believe it or not, is a registered domain name.  I had the misfortune of entering my root password into what I thought was my own machine's CUPS configuration page at \"http://localhost:631/\", and by the time I realized what had happened a few seconds later, it was too late:  my machine was pwned.  Lesson learned: never trust the resolution of \"localhost\".  Always type out \"127.0.0.1\" or \"::1\" when referring to your own machine.\n\nAnd then there's the Qwest DSL modem that tries to run its own DNS server and forces all its dhcp clients to use it regardless of configuration.  These crash hard, requiring a factory reset whenever they see a AAAA record request.\n\nSorry to ramble off topic, but who cares in an obscure old thread like this? Just remember, these issues aren't just limited to KDE.\n"
    author: "Justin"
  - subject: "Re: damn...slow.....startup......times"
    date: 2005-02-03
    body: "I had this same problem suddenly on my arcade cabinet running Mandrake 10.\n\nI quickly determined it was network-related, but beat my head against the wall for 6 hours last night, thinking it was related to another change I had made on the machine while trying to get an ADSL modem working.\n\nTHIS THREAD WAS A GODSEND.  After I saw this thread, I looked and found that my hosts file had never had a 192.168.0.2 (the machine's IP) entry.  This had never been a problem before, but I recently replaced my WinXP ICS machine that I had been using as a firewall (don't laugh... I converted my home desktops to Linux before my servers).  The new ADSL modem I'm using handles DHCP itself, but it doesn't serve DNS for the internal machines (just forwards everything out)... causing KDE on the arcade machine to timeout left and right... slowing everything.\n\nThe reason this took so long to figure out is that my other desktop Linux machine is running Mandrake 10.1, does not have a 192.168.x.x entry in the hosts file, and does not exhibit the problem AT ALL.  I guess something changed between their respective KDE/XFree-Vs-xOrg versions and this one no longer depends on the lookups.\n\nNow I guess I need to install DNS on one of my Linux machines, with forwarding to my ISP.  :)\n\nThanks to everyone who contributed to this thread!"
    author: "3vi1"
  - subject: "Re: damn...slow.....startup......times"
    date: 2004-09-14
    body: "The same situation here....very slow start up times, and then when added to /etc/hosts adding my real ip and machine name the startup times boosted a lot...a really huge difference!\nthis is my /etc/hosts if someone is interested\n\n127.0.0.1       localhost\n10.0.0.10       univac       #this is the line i added\n10.0.0.12       skynet\n# IPV6 versions of localhost and co\n::1 ip6-localhost ip6-loopback\nfe00::0 ip6-localnet\nff00::0 ip6-mcastprefix\nff02::1 ip6-allnodes\nff02::2 ip6-allrouters\nff02::3 ip6-allhosts\n\n\nGood luck!"
    author: "Cristian"
  - subject: "Re: damn...slow.....startup......times"
    date: 2005-10-09
    body: "Though I was having the same symptoms, my solution wasn't the /etc/hosts file.  After reading this post, I thought that the /etc/hosts file must surely be the reason why KDE apps in SUSE 9.3 took minutes, not seconds, to load on a Pentium 4 3.0Ghz, 1G RAM.\n\nAfter using strace and netstat -ap, I tracked the problem to fam, \"the File Alteration Monitor, provides an API that applications can use to be notified when specific files or directories are changed.\"  For some reason (I'm still working on this) fam would go defunct because the default timeout was set to never.  Konqueror (my test program) would make a request to the fam deamon and not timeout for over a minute.\n\nI killed the \"hung\" fam process and started another one finally solving my problem.  Hope this helps!"
    author: "Kiel"
  - subject: "My contribution..."
    date: 2008-06-30
    body: "Funny thing I had exactly this same problem.\n\nI called support to no avail. Then I started to investigate things.\n\n1) ifconfig was ok, then checked route;\n2) took notice of gateway, which was like xyz.virtua.com.br;\n3) ping xyz.virtua.com.br and nothing... but www.google.com was instantaneous;\n4) called support again... :-/\n5) they were useless (want first to check line signals and later dowload speed);\n6) decided then to check resolv.conf (I use opendns, but had it disabled then);\n7) resolv.conf was ok:\nsearch virtua.com.br\nnameserver ip1\nnameserver ip2\n8) decided to edit resolv.conf with vi. It showed up like this:\nnameserver ip1\nnameserver ip2\n9) oops. where's the search line?\n10) pressed [ins] to insert such line, but then the search line misteriously reappeared!\n11) just did wq to save things the correct way;\n12) after 2 hours of pain, the gw was again pingable and KDE worked just fine.\n13) what was it. dunno. could be a null character or whatever.\n14) it works now.\n15) and you got another place to look... ;-)\n16) either way, good luck to you, my KDE friend!\n\nAnd thanks to everyone!\n"
    author: "Gr8teful"
  - subject: "Re: My contribution... failed!"
    date: 2008-07-01
    body: "Well, well, well... time to own up to some things.\n\n1) Probably my LCD monitor was unadjusted, after changing from graphics (X) mode to text mode (Linux console);\n2) Probably the first line wasn't visible the whole time; when I used cat, the entire resolv.conf showed in the middle of the screen, but with vi the file's first line would be hidden -- pressing insert _and_ enter would make it the second line, thus visible again;\n3) They probably fixed something at virtua, after I pestered them some 3 times talking about gateway and whether they would be overloaded (it was on the weekend  after all);\n4) I only found out this yesterday (monday, the 30th).\n\nErm... sorry about that, Chief!"
    author: "Gr8teful"
  - subject: "\"Germanisation\""
    date: 2003-10-05
    body: "Ever since this section is included in the Digest, I wonder:\n\nMany KDE developers are German, yet, the Internationalization Status of German is below that of Danish, Swedish, Hungarian, and even Catalan and Estonian !?\n\nOr is German (and English for that matter) considererd to be 100% translated all the time ?\n\nDo I miss something here ?\n"
    author: "Harald Henkel"
  - subject: "Re: \"Germanisation\""
    date: 2003-10-05
    body: "It's KDE policy that the root language for all strings is English, so there is no English translation team.  This page (http://i18n.kde.org/stats/gui/HEAD/index.php) shows that the German translation is 78% complete.  So, if you speak German, they likely need your help!"
    author: "LMCBoy"
  - subject: "Re: \"Germanisation\""
    date: 2003-10-05
    body: "English doesn't ever need translation as all strings are written in English.\n\nThe German Translation is far from complete. "
    author: "anon"
  - subject: "Re: \"Germanisation\""
    date: 2003-10-05
    body: "The real translation usually doesn't start until the message freeze. There are several languages that until now have consistently had arround 100% translated GUI strings *in official releases*, and German is one of them. Thus, I don't really see the point of translation report. Derek?"
    author: "Anonymous"
  - subject: "Maybe in the wrong place, but..."
    date: 2003-10-06
    body: "Why there isn't any konqueror plugin downloader only nspluginview/nspluginscan (sure those work just great) but nothing for donwloading the actual plugins from the Net and not just from local directory?!?\n\nMaybe the nspluginscan could be modified to scan some specified URLs to get the plugins?\n\nOnly flawless way to get plugins right now seems to be to install Mozilla/Netscape and then download all necessary plugins with them (as plugin maintainers don't seen to want to know about konqueror) and then scan for them within the konqueror... Silly!\n"
    author: "Nobody"
---
In <a href="http://members.shaw.ca/dkite/oct32003.html">this week's KDE-CVS-Digest</a>:
<a href="http://quanta.sourceforge.net/">Quanta</a> gets a table editor. <a href="http://svg.kde.org/">KSVG</a> improves with new gradient algorithms. <a href="http://edu.kde.org/kstars/">KStars</a> implements suggestions from the KGUS, aka
KStars Girlfriend Usability Study. Many bugfixes in <a href="http://kmail.kde.org/">KMail</a>, KHTML and elsewhere.
<!--break-->
