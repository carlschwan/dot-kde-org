---
title: "Season of KDE Kicks Off"
date:    2022-01-26
authors:
  - "Paul Brown"
slug:    season-kde-kicks
---
By Johnny Jazeix

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/plasma-5.24-SoK.jpg" />
</figure>

Season of KDE, the program that helps people start contributing to KDE easily, kicks off with nine fascinating projects:

<ul>
<li>Ayush Singh will be working on a Rust wrapper for KConfig. With this wrapper, and the existing ones for <a href="https://crates.io/crates/qmetaobject">qmetaobject</a> and <a href="https://crates.io/crates/ki18n">ki18n</a>, it will be easier to develop KDE applications in Rust. More information can be found in the <a href="https://mail.kde.org/pipermail/kde-devel/2022-January/000875.html">kde-devel mailing-list</a>. Ayush will be mentored by Jos van den Oever.</li>
<li>Pablo Marcos will add a new panel to show notifications on <a href="https://invent.kde.org/network/tokodon">Tokodon</a>, KDE's Mastodon client. Pablo will be mentored by Carl Schwan.</li>
<li>Snehit Sah will help package more KDE applications for Flathub. <a href="https://invent.kde.org/packaging/flatpak-kde-applications">Some packages are nearly ready for Flathub</a>, but are missing information or manifest files, so they are not yet available on the platform. The goal of the project is to improve the existing packages and publish more apps on Flathub. Snehit will be mentored by Timothée Ravier.</li>
<li>Suhaas Joshi will work on the permission management for Flatpak Apps in Discover. The aim is to allow users view permissions required by a flatpak before installation and also allow users to turn these permissions on or off. After the installation of a flatpak package, Discover will let users view and alter its permissions. Suhaas will be mentored by Timothée Ravier and Aleix Pol Gonzalez.</li>
<li>Samarth Raj will add a new activity help understand the difference between left- and right-clicking using a mouse in GCompris.  The activity will show two types of animals that want to go home. Left-clicking will move one kind, while right-clicking will move the other. <a href="https://phabricator.kde.org/T15124">Find out more about this activity here</a>. Samarth will be mentored by Harsh Kumar and Emmanuel Charruau.</li>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/Ellipse_perspective.gif" /><br /><figcaption>Perspective Ellipse tool</figcaption></figure>

<li>Srirupa will add support for a <i>Perspective Ellipse</i> feature in Krita. The aim is to create a tool that can adjust four corners of a mesh with an ellipse inside it. This will allow users to draw an ellipse in perspective with ease. Srirupa will be mentored by Halla Rempt.</li>
<li>Soumik Dutta will document KDE Connect's communication protocol. Soumik will create comprehensive documentation covering the API contracts and the Event Action Pathways, validate the generated documentation, decide the layout, and merge the work into the existing documentation. Soumik will be mentored by Apollo Zhu and Lucas Wang.</li>
<li>For the KDE-eco project, Karanjot Singh will prepare Standard Usage Scenarios for Energy Consumption Measurements. The first step will be to select an automatic tool to reproduce scenarios and then to define/write scenarios to test the energy consumption for multiple KDE/FOSS projects, such as Kate, KWrite, Vim, nano, emacs, etc. Finally, Karanjot will implement the scenarios. Karanjot will be mentored by Joseph P. De Veaugh-Geiss.</li>
<li>Stefan Kowalczyk will improve the user experience for KDE Connect's iOS Internal Error and Alerts. iOS does not allow for multiple alerts at the same time, so the aim of the project is to queue alerts for KDE Connect if there are more than one, and display them one after the other. Stefan will be mentored by Apollo Zhu and Lucas Wang.</li>
</ul>

We hope all participants have fun with their projects and look forward to your achievements!
<!--break-->