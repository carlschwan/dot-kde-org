---
title: "Season of KDE 2022 - Conclusion"
date:    2022-05-03
authors:
  - "unknow"
slug:    season-kde-2022-conclusion
---
By Johnny Jazeix

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/cover_0.jpg" />
</figure>

<b>Another year, another successful Season of KDE!</b>

In Season of KDE 2022, seven candidates took on and completed projects that helped them learn about Open Source and also expanded their knowledge of how software is created, managed, packaged and distributed; how to create features to applications aimed at end users; about the ever-pressing need for more efficient and eco-friendly software; and much more.

<h3>The Projects</h3>

<a href="https://community.kde.org/SoK/2022/StatusReport/Ayush_Singh">Ayush Singh</a> worked on writing a Rust wrapper for the <a href="https://develop.kde.org/docs/use/configuration/introduction/">KConfig</a> KDE Framework. KConfig simplifies the process of writing values to, and reading options from an app's configuration file. Ayush's project will allow developers to use KConfig in Rust projects without having to write C++ code. Ayush wrote several <a href="https://www.programmershideaway.xyz/posts/post2/">posts</a> explaining which <a href="https://docs.rs/kconfig/0.1.0/kconfig/">bindings and features</a> are now complete and can be used directly in Rust.

Talking of writing apps, <a href="https://community.kde.org/SoK/2022/StatusReport/Samarth_Raj">Samarth Raj</a> added a new activity to GCompris. <a href="https://gcompris.net">GCompris</a> is a suite of educational activities for children from the ages of 2 to 10, and is used widely in schools and homes all over the world. <a href="https://invent.kde.org/education/gcompris/-/merge_requests/94">Samarth's activity</a> helps kids differentiate between the left and right mouse click. It does so by encouraging the child to click on 2 different animals (a horse and a monkey). By using either the left or right button on the mouse, the child can then make each of the animals go to their respective homes (a stable and a tree).

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/sok_gcompris.png" /><br /><figcaption>A new activity in GCompris helps toddlers learn to use the mouse.</figcaption></figure>

Samarth <a href="https://samarthrajwrites.wordpress.com/2022/01/28/season-of-kde-2022/">relates his journey in his blog</a> and says that, although he had previous experience with HTML, CSS, and Javascript, he thought SoK was a great opportunity to learn about open source, and gain some confidence using <a href="https://doc.qt.io/qt-5/qtqml-index.html">Qt/Qml</a>.

Another new feature in an existing application is the Perspective Ellipse Assistant tool <a href="https://community.kde.org/SoK/2022/StatusReport/Srirupa_Datta">Srirupa Datta</a> worked on for Krita. <a href="https://krita.org/">Krita</a> is KDE's design and painting program and the ellipse assistant tool will help artists draw ellipses easier.

The tool is <a href="https://invent.kde.org/graphics/krita/-/merge_requests/1343">still a work in progress</a>, and you can follow up on how it is going on Srirupa's <a href="https://srirupa19.github.io/">blog</a>.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/sok_krita.gif" /><br /><figcaption>The ellipse assistant tool will help artists draw ellipses easier.</figcaption></figure>

KDE apps are not only available for Linux. In fact, many projects are making an effort to reach all users regardless of the platform they are on, and <a href="https://community.kde.org/SoK/2022/StatusReport/Stefan_Kowalczyk">Stefan Kowalczyk</a> worked on improving <a href="https://kdeconnect.kde.org/">KDE Connect</a> on iOS, Apple's mobile phone and tablet operating system.

iOS has the particularity that it can only display one alert at a time. This means that when KDE Connect raised multiple alerts at the same time, only one was being shown. Stefan's project aims to queue the alerts and avoid the user from losing information.

The <a href="https://invent.kde.org/network/kdeconnect-ios/-/merge_requests/42">code</a> has been merged. If you are an iOS user and would like to use KDE Connect on your phone, you may want <a href="https://fixmbr.github.io/">to read more about this SoK</a>, and follow the progress of the project.

For Stefan "[...] it has been a great opportunity to learn more about iOS development and work with a community-driven open source project. [...] SoK was the thing I needed to finally contribute to the Open Source community".

Building new applications and new features into applications is fine, but then comes the problem of delivering them to the users. Flatpak is becoming an increasingly popular way of distributing software to users and <a href="https://community.kde.org/SoK/2022/StatusReport/Snehit_Sah">Snehit Sah</a> packaged several KDE applications for Flatpak and is implementing <a href="https://invent.kde.org/sysadmin/ci-utilities/-/merge_requests/17">continuous integration</a> for Flatpak packages.

Snehit says "I almost jumped to the ceiling when I saw the word "packaging". [...] I've never worked with Flatpak before, but I have a basic understanding of packaging, and it is in fact one of the things I take a lot of interest in". Snehit also remarks on how Season of KDE was a great booster to his experience.

To find out about all the applications that Snehit updated, check out his <a href="https://snehit.dev/posts/kde/sok-2022/">blog</a>.

In similar news, <a href="https://community.kde.org/SoK/2022/StatusReport/Suhaas_Joshi">Suhaas Joshi</a> has been working on displaying the <a href="https://invent.kde.org/plasma/discover/-/merge_requests/282">permissions for Flatpak applications in the Discover</a> interface. This will tell users what they can expect the application to require, like read/write permissions to access the storage, or location data, and so on.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/sok_discover.png" /><br /><figcaption>Flatpak package permissions in Discover.</figcaption></figure>

Apart from guaranteeing users' freedom and privacy, KDE strives to reduce the carbon footprint of its apps by improving their energy efficiency. <a href="https://community.kde.org/SoK/2022/StatusReport/Karanjot_Singh">Karanjot Singh</a> worked with the KDE Eco team to <a href="https://eco.kde.org/blog/2022-03-03-sok22-kde-eco/">prepare Standard Usage Scenarios</a> for measuring the energy consumption of various text editors and <a href="https://invent.kde.org/teams/eco/be4foss/-/merge_requests/1/">developed a script for Kate</a>.

Karanjot Singh remarked on how he learned a lot about working with different automation tools, and creating standard usage scenarios for different applications and frameworks, a skill that will come in handy in the future.

<h3>About Season of KDE</h3>

Season of KDE allows everyone to participate in the KDE ecosystem, and helps newcomers from all backgrounds, students or not, to start contributing to the open source community. To encourage a wide range of skills, the tasks candidates carry out are not limited to coding, but can also cover graphic design, documentation, systems administration, workflow optimization, packaging and distribution, promotion, etc.

Starting to contribute to an open source project is not easy, and organizations like KDE, with its thousands of contributors and a long list of products, may seem intimidating. That is why Season of KDE exists: to make this step easy.

Through Season of KDE, we welcome newcomers and help them become part of the community, while providing them with a mentorship that will help them learn and improve new skills. SoK participants carry out meaningful tasks that elevate our software and services and have an impact on millions of users.
<!--break-->
