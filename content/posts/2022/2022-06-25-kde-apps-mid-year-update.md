---
title: "KDE Apps Mid-Year Update"
date:    2022-06-25
authors:
  - "jriddell"
slug:    kde-apps-mid-year-update
---
The summer sun is here and new apps come with it -- unless you live in the southern hemisphere, in which case, congratulations! You got past the winter solstice, and it's all longer days and new app releases from here onwards.

<h2>LabPlot 2.9.0</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://labplot.kde.org/wp-content/uploads/2021/12/universe.png" width="600" /><br /><figcaption>New worksheet elements to annotate curve data point and to show images<br />on the worksheet.</figcaption></figure>

LabPlot is KDE's open source and cross-platform data visualization and analysis software. It is accessible to everyone, and <a href="https://labplot.kde.org/2022/05/03/labplot-2-9-released/">Labplot 2.9</a> adds a bunch of new features, such as new worksheet elements to annotate curve data points and to show images on the worksheet.

You can also now plot the data against multiple and different axes. A new visualization type, <i>box plot</i> provides a quick summary of the basic statistical properties of the data set, and a collection of multiple well-known color maps and a feature that allows for conditional formatting of the data in the spreadsheet let's you obtain insights into the structure of your data and its statistical properties directly in the spreadsheet.

We added Hilbert transform to the set of analysis functions and LabPlot can now import and export more formats, having added MATLAB, SAS, Stata and SPSS to the list.

<a href="https://labplot.kde.org/download/">Labplot can be downloaded</a> with your Linux distro package manager, through <a href="https://snapcraft.io/labplot">Snapcraft</a>, <a href="https://flathub.org/apps/details/org.kde.labplot2">Flatpak</a>, as well as for Mac and Windows.
<br clear="all">

<h2>Haruna 0.8</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://cdn.kde.org/screenshots/haruna/haruna-playlist.png" width="600" /><br /><figcaption>KDE's Haruna video player.</figcaption></figure>

<a href="https://apps.kde.org/en-gb/haruna/">Haruna</a> is the KDE video player with YouTube integration. This new release has support for global menus, can be configured to pause playback when minimised and save the time position when shutdown. It also adds a "recent files" menu option, and useful actions to load the last file, restart playback and jump to your "watch later" list.

<a href="appstream:haruna"><img src="https://apps.kde.org/store_badges/appstream.svg" width="50">Install on Linux</a>.
<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="https://apps.kde.org/store_badges/flathub.svg" width="50">Download from Flathub</a> <a href=https://snapcraft.io/haruna>
<img src=https://snapcraft.io/static/images/badges/en/snap-store-black.svg alt="Get it from the Snap Store"></a>
<br clear="all">

<h2>GCompris 2.4</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://gcompris.net/news/images/r2.4.png" width="600" /><br /><figcaption>GCompris provides kids with more than 100 fun educational activities.</figcaption></figure>

Our educational activities app, <a href="https://gcompris.net/">GCompris</a>, has made a number of new releases over the last few months and, among other things, has managed to reduce the space it takes up on disk by 30%. GCompris has also added new voices in Norwegian Nynorsk and Malayalam, and new, updated images makes it look prettier than ever.

You can find packages of this new version for GNU/Linux, Windows, Raspberry Pi and macOS on the <a href="https://gcompris.net/downloads-en.html">download
page</a>. It's also available from the Android Play store, the F-Droid repository and the Windows store.
<br clear="all">

<h2>Digikam 7.6</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://i.imgur.com/O0rP1tc.png" width="600" /><br /><figcaption>Digikam's new Flow View plugin.</figcaption></figure>

<a href="https://www.digikam.org/news/2022-03-05-7.6.0_release_announcement/"> DigiKam 7.6</a> has made their AppImage slicker by using ICU for full Unicode support and Qt 5.15 and libraw.

There's a new Flow View Plugin which uses the masonry layout. Masonry is a grid layout based on columns, but, unlike other grid layouts, it doesn’t have fixed height rows. Basically, Masonry layout optimizes the use of space inside the canvas by reducing any unnecessary gaps. Without this type of layout, certain restrictions are required to maintain the structure of layout, as with the main icon-view in digiKam album window. This kind of layout is used by the Pinterest social network for example.

DigiKam 7.6.0 source code tarball, Linux 64 bits AppImage bundles, macOS Intel package, and Windows 64 bits installers can be downloaded from <a href="https://download.kde.org/stable/digikam/7.6.0/">this repository</a>.
<br clear="all">

<h2>KStars 3.5.9</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://edu.kde.org/kstars/appstream/kstars_planner.png" width="600" /><br /><figcaption>KStars</figcaption></figure>

KStars is probably the most feature-rich free astronomy software around and <a href="https://knro.blogspot.com/2022/05/kstars-v359-released.html">the 3.5.9
release</a> adds some exciting new features.

HiPS (Hierarchical Progressive Surveys) is a technology that provides progressive high resolution images of the sky at different zoom levels. KStars fully supports online HiPS where data is downloaded from online servers and cached to be displayed on the Sky Map.

A new simplified and powerful Mosaic Planner directly integrates in the Sky Map and greatly benefits from HiPS overlay to make your target-framing spot on. Toggle the Mosaic View from the toolbar, and select your equipment and the mosaic configuration. You can also use it as a very simple tool to frame a single 1 x 1 tile.

KStars also adds the ability to refocus after a meridian flip is complete. This is very useful for some optical train setups where the focuser might shift the duration the flip.

<a href="https://edu.kde.org/kstars/">KStars</a> can be installed on Microsoft Windows, macOS, and Linux.
<br clear="all">

<h2>RKWard 0.7.4</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://rkward.kde.org/assets/img/screenshot_main.png" width="600" /><br /><figcaption>RKWard.</figcaption></figure>

RKWard is KDE's R app for statistics and has improved the first use experience in its latest versions. RKWard devs have done that by reworking the "Welcome to RKWard" page entirely: it now serves as a "dashboard" with some of the most important tasks, including, among other things, several new options for importing data from other programs. Several unnecessary dialogs that used to greet new users have been removed.

Beyond this, almost all plugins now support a preview, helping you see results faster, and to send only intended output to the output window.

<a href="https://rkward.kde.org/">Download RKWard</a> for macOS, Windows and Linux.
<br clear="all">

<h2>Bugfixes</h2>
<style type="text/css">
ul.dragon li::marker {
 content: "\1F432 ";
}
</style>

<ul class="dragon">
<li>The creators of the Krita painting app have fixed some crashes in <a href="https://krita.org/en/item/krita-5-0-6-released/">version 5.0.6</a>.</li>
<li><a href="https://apps.kde.org/en-gb/ruqola/">Ruqola</a> 1.7.2, KDE's Rocket.chat app, adds a Windows testing build and can be downloaded as Linux packages and from Flathub. Incidently <a href="https://matrix.org/blog/2022/05/30/welcoming-rocket-chat-to-matrix">Rocket.chat announced that they would add bridging to Matrix</a> so they can talk to the rest of KDE's chat rooms.</li>
<li>The <a href="https://tellico-project.org/tellico-3-4-4-released/">Tellico</a> project, creators of KDE's collection manager app, has released version3.4.4 and mended a bug that stops a nasty potential data loss when using emojis.</li>
<li><a href="https://apps.kde.org/okteta/">Okteta 0.26.9</a> comes with more translations and fixes that help Okteta remember settings values. Grab Okteta from your distro's Linux package manager, FlatHub, Snapcraft or as a testing Windows build.</li>
<li><a href="https://apps.kde.org/kdiff3/">KDE's diff tool</a> for comparing files, resolved a major performance regression in handling Windows style line endings in version 1.9.5.</li>
<li><a href="https://apps.kde.org/rsibreak/">RSIBreak 0.12.15</a>, KDE's wellbeing app that helps prevent repetitive strain injuries from overusing your keyboard and mouse, adds an option to suppress breaks while full-screen windows are visible.</li>
</ul>

<h2>Pre-release betas</h2>

<ul class="dragon">
<li>Making its way closer to release, <a href="https://marc.info/?l=kde-announce&m=164994998331408&w=2">isoimagewriter</a> has published a build for Windows.</li>
</ul>

<!--break-->