---
title: "Akademy 2022 - The Weekend of KDE Talks, Panels and Presentations"
date:    2022-10-16
authors:
  - "Paul Brown"
slug:    akademy-2022-weekend-kde-talks-panels-and-presentations
---
From the notes by Aniqa Khokhar, Jonathan Esk-Riddell, and Paul Brown

<img class="img-fluid" src="/sites/dot.kde.org/files/000_Group_photo_straight.jpg" alt="" />

Akademy 2022 was held in Barcelona from the 1st to the 7th of October. As usual, the weekend of Saturday 1st of October and Sunday 2nd of October was dedicated to talks, panels and presentations. Community members and guests laid out for attendees what had been going on within KDE's projects (and adjacent projects), the state of the art, and where things were headed.

Read on to find out in detail what went on during the conference phase of Akademy 2022:

<h3>Day 1 - Saturday, Oct. 1st</h3>

At 10 o'clock sharp, Aleix Pol, President of KDE, opened this year's Akademy before an audience jittering with excitement. The attendees were animated with good reason: this was the first major in-person event for the KDE Community in two years. Old friends were seeing each other after a long time, and we were also meeting many new friends that we had only ever talked to <a href="https://webchat.kde.org/">online</a>.

During his introduction, Aleix remarked on how important this year's event was for the community, and especially for him, as Barcelona is Aleix's home town, and the Universitat Politécnica de Barcelona, the venue where activities were being held, his alma mater. After informing everyone about the logistics of the event, Aleix introduced the first keynote speakers: Volker Hilsheimer and Pedro Bessa from <a href="https://qt-project.org/">Qt Company</a>.

Volker and Pedro kicked off the talks proper with Volker introducing what is in store for us regarding Qt6.5 and beyond. Pedro laid out the new plans Qt Company has to upgrade the engagement with the Qt community, including KDE developers and contributors.

<img class="img-fluid" src="/sites/dot.kde.org/files/volker_straight_1500.jpg" alt="" />

Then it was time to talk about <a href="https://community.kde.org/Goals">KDE's goals</a>, the projects that become a top priority for the community for two years. The goals were started in 2017 program, and the first three goals set in that year covered improving the usability and productivity for basic software, enhancing privacy in KDE software, and streamlining the onboarding of new contributors. The second set of goals started in 2019 and finished now, with this Akademy. They covered solving visual inconsistencies in KDE's software and its components, promoting apps, and improving the implementation of Wayland in Plasma.

The goals voted on by the community this year cover the areas of accessibility, sustainability and ways of making internal KDE processes and workflows more efficient. Adam Szopa, KDE's Project Coordinator, will be going into detail about what each team intends to do and how they will carry things through in a separate article which will appear here next week. It is pretty clear that in the same way prior goals helped give the community focus and improved KDE and its software, these will do the same.

After lunch, the talks were split into two tracks and, in track one Tomaz Canabrava discussed <a href="https://konsole.kde.org/">Konsole</a>, KDE's powerful terminal emulator, and what the future held for the project. Meanwhile, in room 2, Nate Graham covered his traditional <em>Konquering the World - Are we there yet?</em> in which he updates attendees on the progress KDE is making in tech markets and user adoption.

Later, Devin Lin and Bhushan Shah of <a href="https://plasma-mobile.org/">Plasma Mobile</a> told the audience all about the progress KDE's mobile platform has made throughout the year. In room 2, a panel made up of Joseph De Veaugh-Geiss, Nicolas Fella (via videoconference), Karanjot Singh (in a pre-recorded video), and Lydia Pintscher informed the audience about the progress made by <a href="https://eco.kde.org/">KDE Eco</a> and what were the next steps for the project.

After a short break, David Edmundson took to the stage in room 1 and told us all about the success of the <a href="https://kde.org/hardware/">Steam Deck</a>, how the people at Valve were surprised at how popular KDE's Plasma desktop had become among users, and how they were using it in unexpected numbers and ways. David also revealed that the Steam Deck had already sold more than a million units and was still going strong. In room 2, David Cahalane tackled the difficult issue of accessibility and explained how improving it for KDE Plasma and apps would help more users adopt KDE, not only because it would facilitate the usage of the software itself, but also because it would make the desktop and apps compliant with accessibility rules in public institutions and companies across the world.

Back in room 1, Aleix explained how KDE's <a href="https://kde.org/plasma-desktop/">Plasma</a> had transcended the concept of desktop, as it was now moving into the territory of mobile and smart household appliances, like phones, cars and TVs. In Room 2, Harald Sitter talked about Bugs and how frustrating it is when the system keeps crashing, how to identify the causes, and what tools were available to solve the issues.

For the final talk of the day, in room 1 Lina Ceballos of <a href="https://fsfe.org/">FSFE</a> told us about the <a href="https://reuse.software/">Reuse</a> project. The Reuse project intends to relieve much of the confusion and tediousness of licensing software online. At the same time in room 2, both Nicolas Fella and Alexander Lohnau remotely had a discussion on getting applications ready for <a href="https://develop.kde.org/products/frameworks/">KDE Frameworks 6</a>. They discussed the current status of KF6 and why it is better to port now.

<h3>Day 2 - Sunday, Oct. 2nd</h3>

The next morning, Hector Martín, the hacker that opened the Kinect, Play Station and Wii to the open source world, told us about his new project in his keynote "<a href="https://asahilinux.org/">Asahi Linux</a> - One chip, no docs, and lots of fun". Hector explained that the new M1 and M2 Macintosh machines built by Apple are made to run a variety of operating systems, but how the company does not provide any kind of indication on how that is done. Good job reverse engineering is what Hector and his team do best and now Linux (and Plasma) can easily live on the new ARM-based machines.

Afterwards, first David Redondo and later Aleix Pol tackled the topic of Wayland in two different talks. Wayland is a hot topic for developers, since it will allow Plasma and KDE apps to evolve, improve their performance, and work more safely and reliably.

Following a caffeinated and baked goods respite, again the talks were split over two locations. In room 1 Nicolas Fella, live from his studio, explained what really happens when you launch an app; while, in room 2, Neal Gompa told us about how the <a href="https://getfedora.org/">Fedora</a> distro implements Plasma on Fedora, the advantages of Kinoite and the future of Fedora and KDE on mobile.

Later on, Aditya Mehra ran us through <a href="https://openvoiceos.com/">OpenVoiceOS</a>, an operating system with a voice-enabled AI at its core. In room 2, Volker Krause explained how push notifications, used profusely in proprietary software, could be implemented using FLOSS.

After lunch, <a href="https://ev.kde.org/corporate/board/">KDE's Board</a> sat down with attendees and presented their yearly report, informing the community about what work had been carried out and how resources had been used. This was followed by presentations prepared by each of the active working groups: the Advisory Board, the Community Working Group, the Financial Working Group, the Fundraising Working Group, the KDE Free Qt Working Group, and the Sysadmin Working Group.

<img class="img-fluid" src="/sites/dot.kde.org/files/board_report_1500.jpg" alt="" />

While this was going on, Shyamnath Premnadh was presenting his talk on how C++ and Python can thrive together in room 2.

Following a brief coffee break, it was time for the lightning talks, and Volker Krause kicked things off by talking about what was happening with KDE Frameworks 6. Volker was followed by Lydia Pintscher, who talked about the <a href="https://kdenlive.org/en/fund/">new fundraisers</a> for specific projects. Later, Albert Astals presented the KDE Security team, and Harald Sitter gave us advice on how to remain healthy and sane, while writing healthy and sane code.

As the event drew to close, it was time to show appreciation for our sponsors and host. <a href="https://shells.com/">Shells</a>, <a href="https://www.kdab.com/">KDAB</a>, <a href="https://www.canonical.com/">Canonical</a>, <a href="https://mbition.io/">MBition</a>, <a href="https://www.qt.io/">QT Company</a>, the <a href="https://getfedora.org/">Fedora Project</a>, <a href="https://collabora.com/">Collabora</a>, <a href="https://www.opensuse.org/">openSUSE</a>, <a href="https://www.vikingsoftware.com/">Viking</a>, <a href="https://kde.slimbook.es/">Slimbook</a>, <a href="https://www.codethink.co.uk/">Codethink</a>, <a href="https://www.syslinbit.com/">syslinbit</a>, and <a href="https://www.gitlab.com/">GitLab</a> took turns to explain their involvement with KDE and why they decided to support Akademy. <a href="https://pine64.org/">PINE64</a> also received a round of applause for their support.

Finally, there was a round of applause for the Akademy Team, the members of <a href="https://bcnfs.org/">Barcelona Free Software</a> community,  in particular Albert Astals, and all the other volunteers that organized the event and helped us enjoy our days with the KDE community in Barcelona.

The last act of the day was announcing the traditional Akademy awards. This year the award for the Best Application went to Jasem Mutlaq for his work on the phenomenal <a href="https://apps.kde.org/kstars/">KStars</a> astronomical program. The Non-Application Contribution Award went to Harald Sitter for his work on debugging and improving KDE's code across the board. Finally, the Jury Award went to Aniqa Khokhar for her work setting up the KDE Network across the world.

As the rest of the event, this part was a bit special, as the awardees of 2020 and 2021 joined the awardees of 2022 on the stage, as they had not had the chance to physically receive their award before now.

And with that, the conference part of the event was officially closed and KDE community members prepared themselves for a week of BoFs, meetings and hacking sessions.
<!--break-->
