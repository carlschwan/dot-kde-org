---
title: "KDE\u2019s New Goals - Join the Kick Off Meeting"
date:    2022-11-16
authors:
  - "unknow"
slug:    kde’s-new-goals-join-kick-meeting
---
By Adam Szopa

<img class="img-fluid" width="100%" src="/sites/dot.kde.org/files/NewGoals_1500.jpg" alt="Adam Szopa on stage presenting the New Goals at Akademy 2022" />

<strong>KDE is ready with three new Community Goals, and you’re invited to the kick-off meeting!</strong>

Join the new Goal Champions on Monday, November 28th at 17:00 CET (16:00 UTC) for a kick-off meeting. We will talk about the new Goals, the initial short-term plans, and the ways you can contribute. The meeting will be hosted on our <a href="https://meet.kde.org/b/ada-rqf-wbx">BBB instance</a> and will be open to all.

In case you missed the <a href="https://dot.kde.org/2022/10/16/akademy-2022-weekend-kde-talks-panels-and-presentations">announcement at Akademy</a>, the new Goals are:

<ul>
<li>
<strong>KDE for All - Boosting Accessibility:</strong> This is not the first time a proposal about accessibility has been submitted, but this year the community decided it’s time to act. The author of the proposal - and now brand new Goal Champion - Carl is well known in the community for his work on KDE websites, <a href="https://apps.kde.org/neochat/">NeoChat</a> and more. He notes that making KDE software accessible will require cooperation from many parts of the community, from designers to library developers, and will even entail tweaking the underlying Qt toolkit. Testing will also be more challenging, as we will need to ensure that everybody’s accessibility requirements are met.
</li>
<li>
<strong>Sustainable Software:</strong> KDE software is many things: free, beautiful, performant, customizable… The list goes on. But how about all that and environmentally sustainable too? This is the question that Cornelius, our new Champion, will answer while working with the community towards this Goal. This topic of course is not new to him, as he helped set up funding for KDE Eco, KDE’s incursion into research to produce more energy-efficient software. Cornelius plans to help continue certifying KDE apps (<a href="https://eco.kde.org/blog/2022-09-28_okular_blue-angel-award-ceremony/">like KDE’s PDF reader Okular</a>!), set up testing for measuring the environmental impact of our software, and improving its efficiency where needed.
</li>
<li>
<strong>Automate and systematize internal processes:</strong> Every year the number of KDE apps grows, and at the same time we acquire more users and more hardware partners. This is of course fantastic, but at some point relying solely on volunteer efforts for critical parts of delivering quality software to everyone ceases to be scalable. Nate, our first <a href="https://community.kde.org/Goals/Usability_%26_Productivity">two-time Champion</a>, will not let die of success and will work to automate processes, change our culture on quality assurance and involve more people where responsibility lies on a single person.
</li>
</ul>

Our previous Goals, <a href="https://community.kde.org/Goals/Consistency">Consistency</a>, <a href="https://community.kde.org/Goals/All_about_the_Apps">All about the Apps</a>, and <a href="https://community.kde.org/Goals/Wayland">Wayland</a>; are not forgotten! We will continue to focus on them moving forward. However, the selection of the new Goals indicate where the Community wants to go next, and it’s now time for the Champions to leverage the support of the community and the KDE e.V to deliver on those ideas.

Want to know more? The new Champions will meet you on November 28th at 17:00 CET (16:00 UTC) to discuss the Goals, so be sure to mark your calendars and see you at <a href="https://meet.kde.org/b/ada-rqf-wbx">the meeting</a>!
<!--break-->