---
title: "Join Season of KDE 2023"
date:    2022-12-14
authors:
  - "unknow"
slug:    join-season-kde-2023
---
By Benson Muite

<figure style="float: right; padding: 1ex 1ex 1ex 3ex;"><img src="/sites/dot.kde.org/files/Katie-app-dev.png" width="200" alt="Katie, KDE's friendly dragon, develops apps for SoK" /></figure>

<i><a href="https://season.kde.org/">Season of KDE</a></i> is an opportunity to contribute to KDE, while at the same time improving your skills with guidance from experienced mentors.

Apart from code, software projects require artwork, translations, documentation, community management, and funds acquired through fundraising campaigns. With Season of KDE, you too can get the chance to hone your skills in one or more of these areas over a 12-week period. Participation is open to people of all ages with an interest in learning about open source.

<h3>Finding a Project</h3>

The first place to look for projects is on the <a href="https://community.kde.org/SoK/Ideas/2023">KDE SoK page</a>. If a project listed there interests you, contact the mentor. If you are looking for something different, other open source projects are hosted on <a href="https://invent.kde.org">KDE's development platform</a>. To find interesting projects, you can list them by:

<ul>
    <li><a href="https://invent.kde.org/explore/projects/starred">the number of stars<a></li>
    <li><a href="https://invent.kde.org/explore/projects/trending">how they are trending</a></li>
</ul>

You can also try out applications listed on <a href="https://apps.kde.org/">the KDE applications</a> website and see if you can find something in the application that needs improving. If you are a programmer, have a look at the <a href="https://bugs.kde.org/query.cgi">bugs</a> listed for that application. Once you have found a project you find interesting, and have ideas for improvements, contact the project leads to see if they might be supportive of your ideas and, if you find at least one mentor, post your idea on the <a href="https://community.kde.org/SoK/Ideas/2023">KDE SoK wiki</a> before the 15th of January 2023.

<h3>Timeline</h3>

<table>
    <tr><th>Date</th><th><b>Event</b></th></tr>
    <tr><td>15 Dec 2022</td><td><b>Start of Season of KDE</b></td></tr>
    <tr><td>15 Jan 2023</td><td><b>Deadline for Participant and Mentor Applications</b></td></tr>
    <tr><td>22 Jan 2023</td><td><b>Projects Announced</b></td></tr>
    <tr><td>24 Jan 2023</td><td><b>Start of Work</b></td></tr>
    <tr><td>15 Apr 2023</td><td><b>End of Work</b></td></tr>
    <tr><td>20 Apr 2023</td><td><b>Results Announced</b></td></tr>
    <tr><td>20 May 2023</td><td><b>Certificates Issued</b></td></tr>
    <tr><td>1 Jun 2023</td><td><b>Merchandise and SWAG Sent Out by Courier</b></td></tr>
</table>

<figure style="float: left; padding: 1ex 1ex 1ex 3ex;"><img src="/sites/dot.kde.org/files/Mascot_konqi-app-graphics_cropped.png" width="200"/></figure>

<h3>Past SoKs</h3>

If you would like to know more about SoK projects and learn what to expect, check out <a href="https://dot.kde.org/2022/05/03/season-kde-2022-conclusion">SoK 2022</a> and read some of the outcomes from last year. While much of the work is focused on programming, projects in all areas related to the KDE ecosystem are welcome.

<hr />
You too make KDE possible! <a href="https://kde.org/fundraisers/yearend2022/">Check out our End of Year Fundraiser!</a> 
<!--break-->
