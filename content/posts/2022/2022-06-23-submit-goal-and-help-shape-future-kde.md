---
title: "Submit a Goal and Help Shape the Future of KDE"
date:    2022-06-23
authors:
  - "unknow"
slug:    submit-goal-and-help-shape-future-kde
---
By Adam Szopa

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/arrows-cropped.jpg" />
</figure>

I'm super excited to finally announce the start of the submission process for the brand new KDE Goals!

KDE sets goals that help the community focus on important things that need to get done in collaboration across many teams. Over the years, the community has set goals to tackle issues with <a href="https://pointieststick.com/category/usability-productivity/">usability</a>, made it easier for <a href="https://phabricator.kde.org/T7116">new contributors</a> to start working on KDE projects, implemented <a href="https://community.kde.org/Goals/Wayland">new tech</a> that will serve us for years to come, <a href="https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps">and much more</a>.

KDE Goals set a direction for the community and help concentrate efforts in areas deemed important by the KDE community itself. Every couple of years, new goals are selected to reflect the community's current priorities.

<div style='text-align: center'>
  <figure style="padding: 1ex; margin: 1ex;">
    <a href="/sites/dot.kde.org/files/JonandNico.jpg">
      <img src="/sites/dot.kde.org/files/JonandNico.jpg" width="800" />
    </a>
    <figcaption>Jonathan Riddell and Niccolò Venerandi explain their ideas for the KDE goals during Akademy 2019.</figcaption>
  </figure>
</div>

To submit a new goal proposal, you can use the dedicated <a href="https://phabricator.kde.org/project/view/322/">workboard</a> and shape the future direction of the KDE community.

This stage in the process lasts until July 16th, but don’t wait until the last moment! Submit early, and use the remaining time to listen to the feedback, refine and update your proposal. Only submissions with good descriptions will move to the next stage: the community vote.

To make things easier, we provide a <a href="https://phabricator.kde.org/T15607">template ticket</a> that you have to copy and fill out with your content. This way, none of the important parts of a good proposal will be skipped, and there will be consistency between the different proposals.

You will need <a href="https://my.kde.org/">an account</a> to create a new proposal, and then use the arrow in the “Not ready for voting” column to create a new task. Don’t forget to copy the description from the template!

Remember, by submitting a Goal proposal, you are also submitting yourself as the Goal’s Champion! A Goal Champion is the face of the goal and the motivator of the initiative, but not necessarily the one that implements most of the tasks. After all, this is a community goal, so a good champion will motivate others to join in and help achieve amazing things.

If you want to learn more about the whole process, see the <a href="https://community.kde.org/Goals/Goals_Process">wiki</a> for more details.

Don’t wait! <a href="https://phabricator.kde.org/project/view/322/">Submit your proposal</a> and, who knows? Perhaps your idea will be announced as one of the new goals during <a href="https://akademy.kde.org/2022">Akademy 2022</a>!
<!--break-->