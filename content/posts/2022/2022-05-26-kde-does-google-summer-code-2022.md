---
title: "KDE does Google Summer of Code 2022"
date:    2022-05-26
authors:
  - "unknow"
slug:    kde-does-google-summer-code-2022
---
By Johnny Jazeix

<figure style="width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/banner-gsoc2016_2.png" />
</figure>

<b>Google Summer of Code (GSoC) is a global, online program focused on bringing new contributors into open source software development. Like every year, KDE applies and aims to integrate more and more developers. This year, KDE's participation in GSoC kicks off with nine fascinating projects.</b>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 40%"><img src="/sites/dot.kde.org/files/gsoc_neochat_mock_proposal_view_space.png" /><br /><figcaption>NeoChat will get support for Spaces.</figcaption></figure>

Snehit Sah will be <a href="https://summerofcode.withgoogle.com/media/user/40db3e4ce37b/proposal/8ecS8kE3Q9Gddrel.pdf">adding support for Spaces</a> in <a href="https://apps.kde.org/en-gb/neochat/">NeoChat</a>. Spaces is a Matrix tool that allows you to discover new rooms and also a way of organizing your rooms into categories. Snehit already successfully contributed to the Season of KDE 2022 where he <a href="https://community.kde.org/SoK/2022/StatusReport/Snehit_Sah">improved the packaging of several applications of KDE for Flathub</a>.

Suhaas Joshi will work <a href="https://summerofcode.withgoogle.com/media/user/1112c0b599d4/proposal/LHSVw89CfnU2j9PZ.pdf">on permission management for Flatpak and Snap applications in Discover</a>. This will allow you to change the permissions granted to an application (e.g. file system, network, and so on) and also make it easier to review them. It is the continuation of his work during the Season of KDE where he implemented the <a href="https://community.kde.org/SoK/2022/StatusReport/Suhaas_Joshi">display of Flatpak applications permissions on Discover</a>.

This year we have two projects to improve <a href="https://www.digikam.org/">digiKam</a>. The first one is from Quoc Hung Tran who will work on <a href="https://summerofcode.withgoogle.com/media/user/f71ef26d9720/proposal/XFl8tQy1ThpxQpSD.pdf">a new plugin to process Optical Character Recognition (OCR)</a>. This will allow to extract text from images and store the output inside the EXIF data or within a separate file. The plugin will also be used to organize scanned text images with contents.

The second project is from Phuoc Khanh LE who will work on <a href="https://summerofcode.withgoogle.com/media/user/3bea17365af2/proposal/znmmTvwbY9aBIkA7.pdf">improving the Image Quality Sorter algorithms</a>. This will improve sorting images by quality using multiple criteria, for example, noise, exposure and compression.

This is the second stage of the <a href="https://community.kde.org/GSoC/2021/StatusReports/PhuocKhanhLE">original project from GSoC 2021</a> and aims to classify photos by quality (e.g. global exposure, blur, compression, noise, etc.). The first stage was an algorithm based on computer vision classic, as well as image metadata (such as focus point). The second stage involves studying and implementing a more generic algorithm based on Deep Learning. This project will help simplify significantly the complexity of code and improve performance especially when running on a GPU, and will also pave the way for more generic usage in other KDE software. For instance, it can be useful to inspect the image and spot out where to enhance it, or how much it can be enhanced.

Smit Patil will work on <a href="https://userbase.kde.org/System_Settings">Plasma System Settings</a>, <a href="https://summerofcode.withgoogle.com/media/user/d198897017ca/proposal/TlfKu0ubvpc9xyGw.pdf">redesigning the different modules by porting them to QtQuick</a>. This will help with the transition to Qt6 and clean some technical debts by better splitting the UI code from the core logic.

This year we have two projects working on the education software suite <a href="https://www.gcompris.net/">GCompris</a>. Both Aastha Chauhan and Samarth Raj will work on adding new activities to GCompris. Aastha Chauhan will work on adding <a href="https://summerofcode.withgoogle.com/media/user/e9cb54435751/proposal/e1u6FpV4ZWh9nM9q.pdf">a programmable Tux, a comparator activity and the Guess 24 game</a> in GCompris.

<figure style="padding: 1ex; margin: 1ex; border: 1px solid grey; width: 100%"><img src="/sites/dot.kde.org/files/prog_tux_comparator_24.png" /><br /><figcaption>Mockups of Programmable Tux, Comparator and Guess 24 for GCompris.</figcaption></figure>

Samarth Raj will work on an activity for <a href="https://summerofcode.withgoogle.com/media/user/726cc138fc95/proposal/W4heVVZH4mvrT4Rg.pdf">grammatical analysis and another one for using the 10's complements to add numbers</a>.

Two students will work on the painting application <a href="https://krita.org/">Krita</a>. Xu Che will <a href="https://summerofcode.withgoogle.com/media/user/38cf058947a1/proposal/UgGKqiaFiGuAsnsy.pdf">add pixel-perfect ellipses in Krita</a>. This will allow to make it possible for pixel artists to use Krita effectively.

Meanwhile, Reinold Rojas will work on <a href="https://summerofcode.withgoogle.com/media/user/8b9f8e7a4d9d/proposal/9Xu0HLRBsBtVJ9UB.pdf">exporting an image to SVG in Krita</a>; a project that will provide more options to share files with Inkscape, and will help create translatable images with text for Krita Manual without knowledge of Inkscape.

There is more about <a href="https://krita.org/en/item/google-summer-of-code-2022-edition/">Krita's GSoC projects in their blog</a>.

We would like to welcome the new contributors and hope they will have a wonderful summer within KDE and become part of the community.

Follow their progress through their blog posts on <a href="https://planet.kde.org/">KDE's Planet</a>.
<!--break-->
