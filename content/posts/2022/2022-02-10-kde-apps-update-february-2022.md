---
title: "KDE Apps Update February 2022"
date:    2022-02-10
authors:
  - "jriddell"
slug:    kde-apps-update-february-2022
---
<h2>Falkon</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://cdn.kde.org/screenshots/falkon/falkon.png" width="600" /><br /><figcaption>Falkon</figcaption></figure>

Our web browser Falkon has been quickly gathering new features, and this month saw the <a href="https://www.falkon.org/2022/01/31/320-released/">3.2 release</a>.

It adds in-screen capture functionality so you can easily grab a screenshot, it now comes with a inbuilt PDF viewer, and downloads can be paused and resumed.

Best of all, you can <a href="https://store.kde.org/browse?cat=572">Download themes</a> from the KDE Store. Who wants to customize their browser?

Falkon is available from <a href="https://snapcraft.io/falkon">the Snap store</a> or your Linux distro.

If you are interested in packaging or developing Falkon, please <a href="https://www.falkon.org/development/">contact the devs</a> to get involved. More contributions are always appreciated!

<br clear="all">
<h2>Kalendar 0.4.0</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;">
<video style="width: 600px;" controls="" src="https://claudiocambra.com/wp-content/uploads/2021/12/simplescreenrecorder-2021-12-03_17.32.38.mp4"></video>
<br /><figcaption>Kalendar</figcaption></figure>

Kalendar is a new calendar application that allows you to manage your tasks and events. Kalendar supports both local calendars as well as a multitude of online calendars: Nextcloud, Google® Calendar, Outlook®, Caldav, and many more.

Kalendar was built with the idea to be usable on desktop, on mobile and everything in between.

There have been monthly releases recently each adding a bunch of new features and improvements, including <a href="https://claudiocambra.com/2021/11/13/kalendar-v0-2-0-is-out-now-adding-drag-and-drop-improved-calendar-management-and-lots-of-bug-fixes-kalendar-devlog-22/">drag and drop</a> for calendar items.

In <a href="https://claudiocambra.com/2021/12/25/happy-holidays-kalendar-v0-4-0-is-our-gift-and-it-brings-new-views-improved-performance-and-many-many-bugfixes-kalendar-devlog-24/">the new 0.4 release</a>, there are two new views: the three-day and single-day views. These are based on the week view, presenting events and tasks according to their times. These new views should make it much easier to check your calendars when the window is width-constrained, or when you have lots of overlapping events.

<a href="https://claudiocambra.com/2021/11/28/kalendar-v0-3-0-out-soon-with-improved-stability-efficiency-accessibility-and-a-windows-version-kalendar-devlog-23/">In the prior release</a>, Kalendar aligned with Plasma’s motto of ‘simple by default, powerful when needed’. The developers tweaked the default configuration to make the application as clean and simple as it needs to be. They also changed the month view so the weeks would not be numbered by default, and for tasks to be arranged in ascending date order. This should save users from fiddling around with the settings!

The app comes on the PinePhone Pro or you can get it from your Linux distro.

It will likely be added to Plasma Mobile Gear releases in future.

<br clear="all">
<h2>Ruqola 1.6</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://cdn.kde.org/screenshots/ruqola/ruqola.png" width="600" /><br /><figcaption>Ruqola</figcaption></figure>

Ruqola (Italian for the salad leaf "rocket") is KDE's cross platform Rocket.chat app. <a href="https://rocket.chat/">Rocket.chat</a> is a secure chat app and platform. Their slogan is: real-time conversations with your colleagues, other companies or customers.  Rocket.Chat does everything other platforms do, but doesn't expose your data.

Ruqola’s recent 1.6 release settles the UI on tranditional QWidgets, adds team support and includes the all important emoji support.

Download it from your Linux distro or grab <a href="https://download.kde.org/stable/ruqola/">installers for Windows and Mac</a>.

<br clear="all">
<h2>GCompris 2</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://gcompris.net/screenshots_qt/small/click_on_letter.png" width="600" /><br /><figcaption>GCompris 2</figcaption></figure>

Our comprehensive educational games app GCompris released version 2, adding a bunch of new activities and improving a bunch more.

<ul>
<li class="puce">Baby mouse is for children learning to interact with a computer for the first time.</li>
<li class="puce">Oware is a traditional African strategy game, it can be played against the computer or with a friend.</li>
<li class="puce">In Path encoding (absolute or relative) children need to give a set of directions to follow a defined path in a grid.</li>
<li class="puce">Path decoding (absolute or relative) is the opposite. Children have to create the path corresponding to a defined set of directions.</li>
<li class="puce">In Learn quantities, the goal is to count how many items are needed to represent a quantity.</li>
<li class="puce">In Learn decimal numbers, children cut units in pieces to learn the concept of decimal numbers.</li>
<li class="puce">Learn decimal additions and Learn decimal subtractions use the same principles as Learn decimal numbers, but this time to practice these operations.</li>
<li class="puce">With Ordering numbers, children can practice ordering numbers in ascending or descending order.</li>
<li class="puce">With Ordering letters, children can practice ordering letters in alphabetical order or in reverse order.</li>
<li class="puce">Ordering sentences is a step up in which children can practice reading and grammar by sorting out parts of a sentence.</li>
<li class="puce">Positions is an activity to learn the terms describing the relative position of an object.</li>
</ul>

You can get it for <a href="https://www.microsoft.com/en-us/store/p/gcompris/9pgxjtcxn8qm">Windows Store</a>, <a href="https://play.google.com/store/apps/details?id=net.gcompris.full">Android on the Play Store</a>, <a href="https://gcompris.net/downloads-en.html">Download Mac or Linux</a>, <a href="https://flathub.org/apps/details/org.kde.gcompris">Flathub</a>, <a href="https://snapcraft.io/gcompris">Snap Store</a> or even <a href="https://gcompris.net/downloads-en.html">Raspberry Pi</a>.

<br clear="all">
<h2>Heaptrack 1.3</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://invent.kde.org/sdk/heaptrack/uploads/4a83c432205a97b33d36e5f5be95f1c8/Screenshot_20211218_193853.png" width="600" /><br /><figcaption>Heaptrack</figcaption></figure>
One for the developers, Heaptrack is an app to monitor memory usage.  It can attach to running apps and tracks all calls to the core memory allocation functions and log these occurrences.

The Heaptrack v1.3.0 release is packed with quite some important new features. Most importantly, it is now possible to filter by time ranges. To do so, simply select a time range in one of the plots and right click to filter by time. Once analysis is complete, you will see the delta between the two time points; a very useful addition to the heap memory analysis workflow!

You can get it from your Linux distro or just use the <a href="https://download.kde.org/stable/heaptrack/1.3.0/">AppImage</a>.

<br clear="all">
<h2>Kid3</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://kid3.kde.org/assets/img/ss_android_app.png" width="600" /><br /><figcaption>Kid3 on Android</figcaption></figure>

<a href="https://kid3.kde.org/">Version 3.9 of Kid3</a>, a music file tag editor, brings new features. It is now possible to add custom frames to the quick access frames, which are always directly editable in the frame table. Standard frame values can now be edited directly in columns of the file list. Users of the command line version kid3-cli now have the possibility to run QML scripts. Customization of the quick access frames is now also possible on Android. It also comes with translations to new languages and bug fixes.

Kid3 is <a href="https://kid3.kde.org/#download">available all over</a>.  <a href="https://community.chocolatey.org/packages/kid3/">Chocolatey for Windows</a>, <a href="https://flathub.org/apps/details/org.kde.kid3">Flathub on Linux</a>, <a href="https://formulae.brew.sh/cask/kid3">Homebrew for Mac</a>, <a href="https://f-droid.org/en/packages/net.sourceforge.kid3/">F-Droid for Android</a> and any Linux distro.

<br clear="all">
<h2>BugFixes</h2>

<ul>
<li><b><a href="https://mail.kde.org/pipermail/kde-announce-apps/2022-January/005681.html">Tellico 3.4.3</a></b> Our collection manager app updated its <a href="https://www.kinopoisk.ru/">Kinopoisk</a> data source</li>
<li><b><a href="https://kgeotag.kde.org/news/2021/kgeotag-1-2-released/">KGeoTag 1.2</a></h2> updated its build system and now talks updated to GPX files Mime Type.
<li><b><a href="https://mail.kde.org/pipermail/kde-announce-apps/2021-November/005673.html">KDiff 1.9.4</a> fixed DOS/Wndows line ending handling regression and a race condition during teardown.</li>
</ul>

<br clear="all">
<h2>New Beta Software</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://cdn.kde.org/screenshots/kodaskanna/kodaskanna.png" width="600" /><br /><figcaption>Kodaskanna</figcaption></figure>

<a href="https://mail.kde.org/pipermail/kde-announce-apps/2022-January/005686.html">Kodaskanna</a> is  a utility for reading data from 1D/2D codes (e.g. QR codes or bar codes) and making the data available for further processing.

The long-term vision for Kodaskanna is to be a simple utility to integrate in workflows where some data processing expects a data blob (or a series of those) to be taken from a machine readable source by the user. It should have reusable general purpose extensions both for reading all kind of encoded data in all kind of sources (e.g. graphical, acoustical) or straight from dedicated input devices, as well as for validating and previewing the extracted data for the expected data format. The invoking instance should be able to filter/define what is possible. The utility should be usable both in-process and out-of-process, and ideally itself be replaceable by other solutions providing the same interface.

<br clear="all">
<h2>ARM64 Coming to the Snap Store</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/kblocks.png" /><br /><figcaption>KBlocks</figcaption></figure>

<a href="https://snapcraft.io/publisher/kde">KDE publishes</a> over 100 apps on the Snap store, an app store for all Linux distros.  In the last 30 days there are 181,750 machines using them in 209 territories.

Launching today we have published the first <a href="https://snapcraft.io/kblocks">Snap for ARM64</a> so you can use KBlocks on your Raspberry PI or PinePhone Pro.  This architecture will become more important once laptops using the chips become more prominent, so it's great to see KDE becoming available on another platform.
<!--break-->
