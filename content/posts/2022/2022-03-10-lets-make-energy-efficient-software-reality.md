---
title: "Let's Make Energy-Efficient Software A Reality!"
date:    2022-03-10
authors:
  - "aniqakhokhar"
slug:    lets-make-energy-efficient-software-reality
---
by Joseph P. De Veaugh-Geiss
<p align="center">
   <video width="750" controls="1">
      <source src="https://cdn.kde.org/promo/Eco/Videos/KDE-eco-video.mp4">
            Your browser does not support the video tag.
   </video>
   <a href="https://spdx.org/licenses/CC-BY-SA-4.0.html">Creative Commons Attribution-ShareAlike 4.0 International Public License</a>
 </p>
<br/>
In this video, we invite FOSS developers to promote transparency in energy consumption and apply for the <a href="https://www.blauer-engel.de/en">Blue Angel ecolabel</a> for resource and energy-efficient software in three steps:

<ul>
  <li><b>Measure:</b> The first step requires you to learn how much energy your software consumes
  <li><b>Analyze:</b> By making the energy consumption transparent you can drive down the amount of energy your software needs</li>
  <li><b>Certify:</b> Finally, the certification process will guarantee your users and third parties that the software complies with Blue Angel's stringent requirements</li>
</ul>

Software is immaterial, but it determines the energy consumption of hardware. The ICT sector is <a href="https://www.hpcwire.com/off-the-wire/acm-technology-policy-council-releases-techbrief-on-computing-and-carbon-emissions/">reported</a> to contribute as much CO<sub>2</sub> as the aviation industry, and the numbers continue to rise. Making software efficient is crucial.

KDE developers have always paid attention to the performance of the desktop environment and its applications. KDE Plasma runs smoothly even on hardware that is 5-10 years old! In the context of reducing CO<sub>2</sub>, resource and energy-efficiency is a major design goal.

Here we introduce the two projects of <a href="https://eco.kde.org/">KDE Eco</a>! The Free and open-source Energy Efficiency Project (FEEP), which is developing tools to improve energy efficiency in FOSS development. And the Blauer Engel For FOSS (BE4FOSS), which collects and spreads information related to FEEP and the Blue Angel ecolabel.

The Blue Angel is the official environmental label awarded by the German government -- and they now certify <a href="https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf">resource and energy-efficient software</a>. It is the first ecolabel to link the FOSS values of transparency and user autonomy with sustainability.

Join us at <a href="https://eco.kde.org/get-involved/">KDE Eco</a> and let's build energy-efficient Free Software together!
<!--break-->