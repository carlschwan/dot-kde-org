---
title: "New KDE Slimbook 4 Now Available"
date:    2022-07-06
authors:
  - "Paul Brown"
slug:    new-kde-slimbook-4-now-available
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="https://kde.slimbook.es/assets/img/duo-ok.png" />
</figure>


<b>Spanish manufacturer (and <a href="https://ev.kde.org/supporting-members/">KDE patron</a>) <a href="https://slimbook.es/en/">Slimbook</a> has just released their new <a href="https://kde.slimbook.es/">KDE-themed Slimbook ultrabook</a>.</b>

The <b>KDE Slimbook 4</b> comes with a Ryzen 5700U processor and KDE's full-featured <a href="https://kde.org/plasma-desktop/">Plasma desktop</a> running on <a href="https://neon.kde.org/">KDE Neon</a>. It also comes with dozens of Open Source programs and utilities pre-installed and access to hundreds more.

The KDE Slimbook 4's  AMD Ryzen 5700U processor is one of the most efficient CPUs for portable computers in the range. With its 8 GPU cores and 16 threads, it can run your whole office from home and on the go, render 3D animations, compile your code and serve up the entertainment for you during down time.

The Slimbook starts Plasma by default on Wayland, the state-of-the-art display server. With Wayland, you can enjoy the advantages of crisp fonts and images, framerates adapted to each of your displays, and all the touchpad gestures implemented in Plasma 5.25.

Talking of displays, the USB-C port, apart from allowing charging, comes with video output, letting you link up another external monitor. Another nifty detail is the backlit black keyboard with the Noto Sans monographed keys, the same font used on the Plasma desktop.

Remember that with every purchase, KDE receives a donation from Slimbook and <a href="https://slimbook.es/en/kde-member">KDE Community members get a generous discount</a>!

<a href="https://kde.slimbook.es/">Check out here complete details, specs and benchmarks</a>.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="https://kde.slimbook.es/assets/img/IMG_0841_e.jpg" />
</figure>

<!--break-->