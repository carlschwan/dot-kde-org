---
title: "Got something to say about KDE? Say it at Akademy 2022"
date:    2022-06-15
authors:
  - "aniqakhokhar"
slug:    got-something-say-about-kde-say-it-akademy-2022
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/slices_0.jpg" />
</figure>

Akademy 2022 will be a hybrid event, in Barcelona and online that will be held from Saturday 1st to Friday 7th October. <b>The Call for Participation</b> is still open! <a href="https://akademy.kde.org/2022/cfp ">Submit your talk ideas and abstracts</a> as the deadline has been extended until the 19th June.

<h3>Why talk at #Akademy2022?</h3>

Akademy attracts people from all over the world, not only from the global KDE Community, but also from companies, Free Software developers and pundits from other projects, public institutions, and more. We all meet together to discuss and plan the future of the Community and its technology. You will meet people that are receptive to your ideas and will help you with their skills and experience.

<h3>How do you get started?</h3>

You do not have to worry about details or a presentation right now. Just think of an idea and submit some basic details about your talk. You can edit your abstract after the initial submission.

All topics relevant to the KDE Community are welcome. Here are a few ideas to get you started on your talk:
<ul>
  <li>What KDE projects have you contributed to? Tell us about your work, and get feedback and support!</li>
  <li>Where do you think should KDE go next?</li>
  <li>How has KDE software impacted you/people you know in real life?</li>
  <li>Are there particular problems with KDE you think you can give us some insight on? How would you solve it?</li>
  <li>How can we improve KDE for people with disabilities?</li>
  <li>How can we increase the participation of women in our community?</li>
  <li>How can we accelerate full support on Wayland?</li>
  <li>What is next for Plasma mobile?</li>
  <li>What is the current state of the project you are working on?</li>
  <li>Have you used any KDE applications in ingenious ways or created something great with them? Tell us about your work!</li>
</ul>


These are just some ideas to get the ball rolling. However, you can submit a proposal on any topic as long as you can make it relevant to KDE. For more ideas for talks, check out the videos from previous years: <a href="https://www.youtube.com/playlist?list=PLsHpGlwPdtMq6pJ4mqBeYNWOanjdIIPTJ">2021</a>, <a href="https://www.youtube.com/watch?v=_172B5er4P4&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI">2020</a>, <a href="https://www.youtube.com/watch?v=Nxem6yTRC4M&list=PLsHpGlwPdtMrEkjUUzwlIA6fJBsZxDWY3">2019</a>, <a href="https://www.youtube.com/watch?v=_yyOc6-x2yI&list=PLsHpGlwPdtMraXbFHhkFx7-QHpEl9dOsL">2018</a>, <a href="https://www.youtube.com/watch?v=Uzemu8xbsSo&list=PLsHpGlwPdtMojYjH8sHRKSvyskPA4xtk6">2017</a>, <a href="https://www.youtube.com/watch?v=-Ek368vPBz0&list=PLsHpGlwPdtMogitRYzwPz4dWTVsZRGwts">2016</a>, and <a href="https://www.youtube.com/watch?v=BFyl9lPV6rI&list=PLsHpGlwPdtMoYr6716veZi0OJteaU9ud3">2015</a>.

For more details and information visit our <a href="https://akademy.kde.org/2022/cfp">Call for Participation page</a>.
<!--break-->