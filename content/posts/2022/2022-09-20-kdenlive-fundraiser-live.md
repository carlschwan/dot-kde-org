---
title: "The Kdenlive Fundraiser is Live"
date:    2022-09-20
authors:
  - "Paul Brown"
slug:    kdenlive-fundraiser-live
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/F_et_C.jpg" />
</figure>

<strong>Today we break ground. Today we launch <a href="https://kdenlive.org/fund?mtm_campaign=fund_dot">the first of what will be many fundraisers</a> for specific projects.</strong> Our goal is to get funds directly into the hands of the people who make the software.
 
Up until now, when KDE has run a fundraiser, or received donations, the proceedings have gone to KDE as a whole. We use the money to fund operational costs, such as office rent, server maintenance, and salaries; and to pay for travel expenses for community members, event costs, and so on. This has worked well and helps the KDE Community and common project to flourish.

But the fundraiser starting today is very different. For the first time KDE is running a fundraiser for a specific project: today we have the ambitious goal of raising 15,000€ for the Kdenlive team. The funds will be given to contributors to help Kdenlive take the next step in the development of KDE's advanced, free and open video-editing application. For the record, on the cards for upcoming releases are nested timelines, a new effects panel, and improving the overall performance of Kdenlive, making it faster, more responsive, and even more fun to work with.

The advantages for the Kdenlive team members are many, but mainly there is no need for them to worry about setting up and managing bank accounts, or, indeed, a whole foundation. KDE's financial, legal, promotional, and admin teams are there for Jean Baptiste, Julius, Camille, Farid, Massimo, and Eugen, and are helping make the process as streamlined and painless as possible.

There are also immense advantages for the KDE Community as a whole. This event will set the basis for similar future fundraisers for all KDE projects. Our aim is that contributors be able to work on their Free Software projects with the peace of mind that comes from having their financial needs covered.

Want to help? Head over to the <a a href="https://kdenlive.org/fund?mtm_campaign=fund_dot">Kdenlive fundraiser page</a> and donate now.

Want to help more? <a href="https://community.kde.org/Get_Involved">Join KDE</a> and contribute to building the future of KDE.
<!--break-->