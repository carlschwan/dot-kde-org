---
title: "Linux App Summit 2022 will be held in Italy"
date:    2022-01-20
authors:
  - "Paul Brown"
slug:    linux-app-summit-2022-will-be-held-italy
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/WebBanner-text-1024x398.png" />
</figure>

<b>The Linux App Summit (LAS) of 2022 will be held in Rovereto, a picturesque city at the foot of the Italian Alps.</b>

Whether you are a company, journalist, developer, or user interested in the ever-growing Linux app ecosystem, LAS will have something for you. Scheduled for April, LAS 2022 will be a hybrid event, combining on-site and remote sessions, including talks, panels and Q&As.

The call for papers will open soon, and the registrations shortly after.

<a href="https://twitter.com/LinuxAppSummit">Follow us on Twitter</a> to keep up to date with Linux App Summit news.

<!--
<figure style="float: right; padding: 1ex; margin: 1ex;">
  <img src="/sites/dot.kde.org/files/las-2019-group_500.jpg" width="200" />
</figure>
-->

<h2>About the Linux App Summit</h2>

The Linux App Summit (LAS) brings the global Linux community together to learn, collaborate, and help grow the Linux application ecosystem. Through talks, panels, and Q&A sessions, we encourage attendees to share ideas, make connections, and join our goal of building a common app ecosystem. Previous iterations of the Linux App Summit have been held in the United States in Portland, Oregon and Denver, Colorado, as well as in Barcelona, Spain.

Learn more by visiting  <a href="https://linuxappsummit.org/">linuxappsummit.org</a>.

<h2>About Rovereto</h2>

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/Rovereto-Italy-768x432.jpg" width="100%"/>
</figure>

Rovereto is an old Fortress Town in Northern Italy at the foot of the Italian Alps. It is located in the autonomous province of Trento and is the main city of the Vallagarina district.

The city has several interesting sites including:

<ul>
<li>The Ancient War Museum</li>
<li>A castle built by the counts of Castelbarco</li>
<li>The Museum of Modern and Contemporary Art of Trento</li>
</ul>

Rovereto's economy revolves around wine, coffee, rubber, and chocolate. The town was acknowledged as a “Peace town” in the 20th century and is also the location of important palaeontological remains, such as dinosaur footprints in the surrounding area.

We look forward to seeing you in Rovereto, Italy.

<i>* The image “<a href="https://www.flickr.com/photos/75487768@N04/50194999521">Rovereto</a>” featured above is by <a href="https://www.flickr.com/photos/75487768@N04/">barnyz</a> and is distributed under a <a href="https://creativecommons.org/licenses/by-nc-nd/2.0/">CC BY-NC-ND 2.0</a> license.</i>

<!--break-->