---
title: "KDE's Google Summer of Code 2022 Projects: Final Report"
date:    2022-10-22
authors:
  - "unknow"
slug:    kdes-google-summer-code-2022-projects-final-report
---
By Johnny Jazeix

<img class="img-fluid" width="100%" src="/sites/dot.kde.org/files/gsoc2022_1.jpg" alt="" />

<a href="https://summerofcode.withgoogle.com/">Google Summer of Code (GSoC)</a> is a global, online event that focuses on bringing new contributors into open source software development. Like every year, KDE applied and aimed to integrate more and more developers. In 2022, KDE's participation in GSoC covered nine projects to improve KDE, of which six were successfully completed.

Snehit Sah worked on <a href="https://community.kde.org/GSoC/2022/StatusReports/SnehitSah">adding Spaces Support</a> to <a href="https://apps.kde.org/neochat/">NeoChat</a>. <i>Spaces</i> is a Matrix tool that allows you to discover new rooms by exploring areas, and is also a way to organize your rooms by categories. The code is still not merged to the main branch.

<figure style="float: right; padding: 1ex 1ex 1ex 3ex;"><img src="/sites/dot.kde.org/files/space-preview-1_0.png" width="200" /><br /><figcaption>Spaces in NeoChat</figcaption></figure>

Suhaas Joshi worked <a href="https://community.kde.org/GSoC/2022/StatusReports/SuhaasJoshi">on permission management for Flatpak and Snap applications in Discover</a>. This allows you to change the permissions granted to an application (e.g. file system, network, and so on) and also makes it easier to review them. The code is in two separate repositories, one for Flatpak applications which is ready to be used, and one for Snap applications which is still work in progress.

<figure style="float: left; padding: 1ex 3ex 1ex 0ex;"><img src="/sites/dot.kde.org/files/BasicPermissionsScreenshot.png" width="400" /><br /><figcaption>Basic permission for Flatpaks</figcaption></figure>

This year we had two projects to improve <a href="https://www.digikam.org/">digiKam</a>. The first one is from Quoc Hung Tran who worked on <a href="https://community.kde.org/GSoC/2022/StatusReports/QuocHungTran">a new plugin to process Optical Character Recognition (OCR)</a>. The code has been merged and allows you to extract text from images and store the output inside the EXIF data or within a separate file. The plugin is also used to organize scanned text images with contents.

The second project is from Phuoc Khanh LE who worked on <a href="https://community.kde.org/GSoc/2022/StatusReports/PhuocKhanhLe">improving the Image Quality Sorter algorithms</a>. The code has already been merged into digiKam and improves sorting images by quality using multiple criteria, for example, noise, exposure and compression.

For <a href="https://www.gcompris.net/">GCompris</a>, KDE's educational software suite, Samarth Raj worked on adding activities for <a href="https://community.kde.org/GSoC/2022/StatusReports/Samarthraj">for using the 10's complements to add numbers</a>. The activity will be split into three activities. One was finished during GSoC, the other two are still work in progress.

<figure style="float: left; padding: 1ex 3ex 1ex 0ex;"><img src="/sites/dot.kde.org/files/image.png" width="250" /><br /><figcaption>GCompris activity mockup</figcaption></figure>

Two students worked on the painting application <a href="https://krita.org/">Krita</a>. Xu Che worked on <a href="https://community.kde.org/GSoC/2022/StatusReports/XuChe">adding pixel-perfect ellipses in Krita</a>. The work is still in progress and, once it is done, it will be merged. This will allow pixel artists to use Krita effectively.

<figure style="float: right; padding: 1ex 0ex 1ex 3ex;"><img src="/sites/dot.kde.org/files/PixelPerfectExplain.png" width="250" /><br /><figcaption>Pixel-perfect ovals for Krita</figcaption></figure>

Meanwhile, Reinold Rojas worked on <a href="https://community.kde.org/GSoC/2022/StatusReports/ReinoldRojas">exporting Krita images to SVG</a>. The project provides more options to share files with Inkscape, and will help create translatable images with text for Krita Manual without knowledge of Inkscape. The code is still work in progress.

GSoC has again provided our nine contributors the opportunity to exercise their programming skills in real-world projects, allowing them to improve not only their code, but also their communication skills. We would like to thank all the mentors without whom these opportunities would not have been possible. We would also like to extend our appreciation to all the coders who invested their energy and passion into these two months of coding.
<!--break-->