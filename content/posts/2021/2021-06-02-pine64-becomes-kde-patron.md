---
title: "PINE64 becomes a KDE Patron"
date:    2021-06-02
authors:
  - "apol"
slug:    pine64-becomes-kde-patron
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/Pinephone_sponsor.png" alt="The PinePhone - KDE Edition" />

<a href="https://www.pine64.org/">PINE64</a>, the hardware project that aims to bring ARM and RISC-V devices to FOSS enthusiasts world-wide, widely known for their <a href="https://www.pine64.org/pinephone/">PinePhone</a> and <a href="https://www.pine64.org/pinebook/">PineBooks</a>, joins KDE's supporting members program as a KDE Patron.

<i>"We have a long-standing relationship with KDE, and our Pinebook Pro and PinePhone shipping with Plasma are a testament to this commitment. Indeed, the promise early Plasma Mobile development held was the deciding factor for us to create the PinePhone. We are thrilled to have been made a patron by KDE e.V. and are excited at the prospect of
an even closer cooperation in the future."</i>
— Lukasz Erecinski PINE64 Community Manager

<i>"KDE's last few years have been without a doubt spurred by Pine64's vision to bring affordable devices that have good support for our core technologies. In  KDE, we make software and we need good hardware to run on. Having such devices  available allows us to materialize the experience we want for our users. PINE64 is pushing our limits on this front. I'm looking forward to having them as our Patron and working on new great products for everyone around."</i>
— Aleix Pol - KDE e.V. President

<a href="https://www.pine64.org/">PINE64</a> joins KDE e.V.’s <a href="https://ev.kde.org/supporting-members/">other Patrons</a>: The Qt Company, SUSE, Google, Blue Systems, Canonical, Enioka and Slimbook to continue to support Free Software and KDE development through the KDE e.V.
<!--break-->