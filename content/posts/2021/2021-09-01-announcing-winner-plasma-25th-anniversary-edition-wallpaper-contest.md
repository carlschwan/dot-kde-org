---
title: "Announcing the Winner of the  Plasma 25th Anniversary Edition Wallpaper Contest"
date:    2021-09-01
authors:
  - "Paul Brown"
slug:    announcing-winner-plasma-25th-anniversary-edition-wallpaper-contest
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/patak_1500_0.jpg" />
</figure>

We are happy to announce the winner of the Plasma 25th Anniversary Edition Wallpaper Contest!

Thanks to all those who submitted their artwork. The variety and quality of the submissions in this edition was simply astounding. As always, there were multiple great options and it was very hard to choose. It was a very close call! But at the end, we have a winner: <a href="https://forum.kde.org/viewtopic.php?f=333&t=171728&sid=2c2b01a5ebacf9e18e06b96b40939388">Patak by Aron</a>!

Patak will make its official debut on October 14, when Plasma 25th Anniversary Edition is finally released.
<!--break-->