---
title: "2021 KDE Eco Sprint "
date:    2021-12-24
authors:
  - "Paul Brown"
slug:    2021-kde-eco-sprint
---
By Joseph P. De Veaugh-Geiss

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/KDE-eco-logo-name_vegetation.jpg" />
</figure>

On 11 December 2021, KDE Eco held the first of many planned Sprints. The Sprint was originally intended to be an in-person event to set up a community measurement lab, but Corona had other ideas. Nevertheless, the community deployed its usual resourcefulness, and we met online instead.

We discussed the next steps in KDE's Eco project, and the day's conversation was varied, covering topics such as setting up a team space for the project (<a href="https://invent.kde.org/teams/eco">achieved</a>), completing the Blauer Engel application for Okular (<a href="https://invent.kde.org/teams/eco/feep/-/tree/master/measurements/okular">submitted</a>), along with several technical issues related to energy-consumption measurements in the lab, including Standard Usage Scenarios, replicable reference systems, standardizing data output, budget vs. professional power meters, and more. See the <a href="https://invent.kde.org/teams/eco/be4foss/-/blob/master/community-meetups/2021-12-11_sprint01_protocol.md">minutes</a> for details.

A more detailed summary of the discussion will be published at the KDE Eco blog (coming soon), so keep an eye out for updates there!

The online Sprint was a wonderful opportunity to bring the community together and move the KDE Eco project forward, especially as we prepare for the community measurement lab that will be held at KDAB Berlin and the first of many measure-athons (planned for early 2022)! The success of such events depends first and foremost on the community, so allow us to send a heartfelt thank you to everyone who joined the conversation. Moreover, we could not do what we do without the support of KDE e.V. as well as <a href="www.bmu.de">BMU</a>/<a href="https://www.umweltbundesamt.de/en">UBA</a>, who financially support the BE4FOSS project.

<h3>Did you know?</h3>

Discussions similar to those at the Sprint occur monthly at <a href="https://webchat.kde.org/#/room/#energy-efficiency:kde.org">our community meetups</a> on the 2nd Wednesday of the month from 19h-20h (CET/CEST). <a href="https://eco.kde.org/get-involved/">Join us!</a> We would love to see you there.
<!--break-->