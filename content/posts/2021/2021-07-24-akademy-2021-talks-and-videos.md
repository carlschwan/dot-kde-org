---
title: "Akademy 2021 - Talks and Videos"
date:    2021-07-24
authors:
  - "Paul Brown"
slug:    akademy-2021-talks-and-videos
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
        <img class="img-fluid" src="/sites/dot.kde.org/files/01_cover_widex1500.png" />
</figure>

<b>All Akademy 2021 <a href="http://kde.avc.cx/akademy/2021/">videos</a> are ready for you to enjoy.</b> Check out the previous summaries of talks on the dot for ideas on what to watch:

<ul>
<li><a href="https://dot.kde.org/2021/06/20/akademy-2021-day-2">Saturday, 19th June 2021</a></li>
<li><a href="https://dot.kde.org/2021/06/20/akademy-2021-day-3">Sunday, 20th June 2021</a></li>
<li><a href="https://dot.kde.org/2021/06/26/akademy-2021-day-8">Friday, 25th June 2021</a></li>
</ul>

A good place to start is with the Keynotes by Patricia Aas

<p align="center">
<video width="750" controls="1">
  <source src="http://kde.avc.cx/akademy/2021/akademy2021-2-I-Cant-Work-Like-This.mp4" type="video/webm">
Your browser does not support the video tag.
<a href="http://kde.avc.cx/akademy/2021/akademy2021-2-I-Cant-Work-Like-This.mp4">Patricia Aas: I can't work like this</a>
</video> 
</p>

and Jeri Ellsworth

<p align="center">
<video width="750" controls="1">
  <source src="http://kde.avc.cx/akademy/2021/akademy2021-4-Journey-from-Farm-Girl-to-Holograms.mp4" type="video/webm">
Your browser does not support the video tag.
<a href="http://kde.avc.cx/akademy/2021/akademy2021-4-Journey-from-Farm-Girl-to-Holograms.mp4">Jeri Ellsworth: Journey from farm girl to holograms</a>
</video> 
</p>


Another thing to check out are the BoF wrapups:

<ul>
<li><a href="http://kde.avc.cx/akademy/2021/monday_bof_wrapup.webm">Monday, 21st June 2021</a></li>
<li><a href="http://kde.avc.cx/akademy/2021/tuesday_bof_wrapup.webm">Tuesday, 22nd June 2021</a></li>
<li><a href="http://kde.avc.cx/akademy/2021/wednesday_bof_wrapup.webm">Wednesday, 23rd June 2021</a></li>
<li><a href="http://kde.avc.cx/akademy/2021/thursday_bof_wrapup.webm">Thursday, 24th June 2021</a></li>
<li><a href="http://kde.avc.cx/akademy/2021/akademy2021-54-Final-BoF-Wrap-Up-Akademy-Awards-and-Closing-Remarks.mp4">Friday, 25th June 2021</a></li>
</ul>

These let you know what went on during the week following the talks.

Enjoy!