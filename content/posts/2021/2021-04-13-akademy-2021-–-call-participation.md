---
title: "Akademy 2021 \u2013 Call for Participation!"
date:    2021-04-13
authors:
  - "unknow"
slug:    akademy-2021-–-call-participation
---
By Allyson Alexandrou

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/callforpapers.jpg" alt="Akademy 2021 - Call for Participation" />

Akademy will be held online from 18th to 25th June and the Call for Participation is open! <b><a href="https://akademy.kde.org/2021/cfp">Submit your talk ideas</a> and abstracts by 2nd May.</b> 

While all talk topics are welcome, here are a few talk topics specifically relevant to the KDE Community:

<ul>
<li>Topics related to KDE's current <a href="https://community.kde.org/Goals">Community Goals</a>.</li>
<li>KDE In Action: cases of KDE technology in real life.</li>
<li>Overview of what is going on in the various areas of the KDE Community.</li>
<li>Collaboration between KDE and other Free Software projects.</li>
<li>Release, packaging, and distribution of software by KDE.</li>
<li>Increasing our reach through efforts such as accessibility, promotion, translation and localization.</li>
<li>Improving our governance and processes, community building.</li>
<li>Innovations and best practices in the libraries and technologies used by KDE software.</li>
</ul>

<h3>Why should I submit a talk?</h3>
KDE is one of the biggest and well-established Free Software communities. Talking at Akademy gives you an audience that will be receptive to your ideas and will also offer you their experience and know-how in return. 

As an independent developer, you will gain supporters for your project, the insight of experienced developers, and you may even gain active contributors. As a community leader, you will be able to discuss the hot topics associated with managing large groups of volunteers, such as management, inclusivity and conflict resolution. As a CTO, you will be able to explain your company’s mission, its products and services and benefit from the brainshare of one of the most cutting edge community-based Free Software projects.

<h3>How do I get started?</h3>
With an idea. Even if you do not know exactly how you will focus it, no worries! Submit some basic details about your talk idea. All abstracts can be edited after the initial submission.

<h3>What should my talk abstract or proposal include?</h3>
This is a great question! To ensure you get your point across both clearly and comprehensively, your abstract should include uses of your idea or product and show what different groups of people get out of it. For example, how can a company, developer, or even a user benefit from using your app? In what ways can you further their experiences?

If you’re still stuck on where to start or what to talk about, take a look at a brief list of talks given in previous years at Akademy:

<ul>
<li><a href="https://www.youtube.com/watch?v=rn7jfrJWuAM&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=16">The KDE Free Qt Foundation</a> by Cornelius Schumacher</li>
<li><a href="https://www.youtube.com/watch?v=5Laat0XoWZE&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=15">KDE Fedora</a> by Neil Gompa</li>
<li><a href="https://www.youtube.com/watch?v=RgPl4JoApxY&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=18">Clear Dental</a> by Tej Shah</li>
<li><a href="https://www.youtube.com/watch?v=5Laat0XoWZE&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=15">Linux Graphics</a> by Rohan Garg</li>
<li><a href="https://www.youtube.com/watch?v=uKx6x0qlvnY&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=22">Input Handling</a> by Shawn Rutledge</li>
<li><a href="https://www.youtube.com/watch?v=-ev7uvKh_JY&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=26">C++17 and 20</a> by Ivan Cukic</li>
<li><a href="https://www.youtube.com/watch?v=5JvLuHDKZzU&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=28">MyGNUHealth</a> by Luis Falcon</li>
<li><a href="https://www.youtube.com/watch?v=bayOuCvzOio&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=32">KDenlive</a> by Massimo Stella</li>
<li><a href="https://www.youtube.com/watch?v=_m2Zo6gDP6g&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=38">Integrating Hollywood Open Source with KDE Applications</a> by amyspark</li>
<li><a href="https://www.youtube.com/watch?v=gj4XV0PMKZw&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI&index=41">KDE Products</a> by Aleix Pol</li>
</ul>

You can find more Akademy videos on the <a href="https://www.youtube.com/c/KdeOrg">KDE's YouTube channel</a>.
<!--break-->
