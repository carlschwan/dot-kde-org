---
title: "Slimbook Becomes a KDE Patron"
date:    2021-02-25
authors:
  - "apol"
slug:    slimbook-becomes-kde-patron
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/Akademy2017_Alejandro_Adriaan.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Akademy2017_Alejandro_Adriaan.jpg" />
    </a>
   <figcaption align="center">Alejandro López (right), CEO at Slimbook, with Adriaan de Groot from the KDE e.V. board.</a></figcaption>
</figure>

<b>After several product collaborations, today we celebrate an extension of this partnership by welcoming <a href="https://slimbook.es/en/">Slimbook</a> to the KDE Patrons family.</b>

Alejandro López, Slimbook CEO explains,

“Since our early days in 2015, we at SLIMBOOK have been trying our best not only to sell GNU/Linux compatible quality hardware, but also to contribute and help those who make Free and Open Source Software. 

Our variety of contributions range from giving support to local groups of developers, the making of forums and tutorials to help the Linux community and sharing a common vision with KDE, to hit the market with a device able to provide the end user with the best out-of-the-box Linux experience available. 

But our mission doesn’t end there and there’s more than meets the eye. Our main goal is to share our knowledge and experience, help each other, and of course, to give the GNU/Linux users the best in hardware excellence the same way as the KDE Team do with their excellent software experience. 

We take our duty of supporting the KDE Community full of pride, and we are honored to be KDE Patrons." 

Aleix Pol i Gonzalez, President of KDE e.V. stated, 

“Slimbook’s attention towards FOSS users as a hardware provider is very important to KDE and the community at large. For KDE, being able to reach beyond the software experience to tangible and properly integrated solutions has been a dream come true. Working together in the different collaborations over the years has been really exciting, and we look forward to continuing doing so with Slimbook as a Patron.”

Slimbook will join KDE e.V.’s other Patrons: The Qt Company, SUSE, Google, Blue Systems, Canonical and Enioka to continue to support Free Software and KDE development through the KDE e.V.
<!--break-->