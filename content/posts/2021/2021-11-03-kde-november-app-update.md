---
title: "KDE November App Update"
date:    2021-11-03
authors:
  - "jriddell"
slug:    kde-november-app-update
---
<h2>Skanpage</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://cdn.kde.org/screenshots/skanpage/skanpage.png" width="600" /><br /><figcaption>Skanpage</figcaption></figure>

<a href="https://apps.kde.org/skanpage/">Skanpage</a> is KDE's new image scanning app.  It's a simple scanning application designed for multi-page scans and saving of documents and images.

It works for scanning from both flatbed and feed-through automatic document feeder scanners. It lets you configure options for the scanning device, such as resolution and margins, and you can re-order, rotate and delete scanned pages. The scans can be saved to multi-page PDF documents and image files.

Unlike our existing Skanlite app, this new program is written using Kirigami, our responsive interface toolkit which adapts to mobile and desktop devices.

You can get Skanpage from KDE neon now, and look out for it on other Linux distros soon.

<h2>KGeoTag 1.1</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://cdn.kde.org/screenshots/kgeotag/kgeotag.png" width="600" /><br /><figcaption>KGeoTag</figcaption></figure>

<a href="https://kgeotag.kde.org/news/2021/kgeotag-1-1-released/">KGeoTag released version 1.1</a>. KGeoTag is KDE's stand-alone photo geotagging program.

To improve the workflow, there's a new main menu entry called <i>Assign images to GPS data</i> that triggers a configurable action (like "<i>(Re)Assign all images</i>") from the <i>Automatic assignment</i> dock. Also, you can now load images and GPX tracks, assign them, and save the coordinates without having to mark all images and having to use the context menu of the images list or using the <i>Automatic assignment</i> dock.

Additionally, some bugs have been fixed.

You can get KGeoTag from <a href="https://repology.org/project/kgeotag/versions">these Linux distros</a>, and more download options are coming soon.

<h2>digiKam 7.3</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://i.imgur.com/L9uibbah.png" width="600" /><br /><figcaption>Digikam</figcaption></figure>

KDE's photo sorting and editing app, digKam, <a href="https://www.digikam.org/news/2021-07-12-7.3.0_release_announcement/">has released version 7.3</a>.

The famous <a href="https://exiftool.org/">ExifTool</a> is now officially supported alongside the former Exiv2 shared library to handle file metadata.  ExifTool is a powerful utility that you can use in special cases to fix metadata dysfunctions that can't be solved using Exiv2. An ExifTool metadata viewer has been appended to the metadata sidebar everywhere in digiKam. ExifTool also supports a larger list of file formats than Exiv2.

The DNG Converter received a major update of the internal Adobe SDK and has improved the support of modern original RAW files features and Digital Negative targets.

The FITS and MPO formats are now fully supported in digiKam 7.3.0, including their metadata components. Flexible Image Transport System (FITS) is an open standard commonly used in astronomy. It defines a digital file format useful for storage, transmission and processing of data, formatted as multi-dimensional arrays, for example, 2D images or tables.

JPEG Multi-Picture Format (MPO) is a JPEG-based format for storing multiple images in a single file. It can contain two or more JPEG files concatenated together and various devices from Fujifilm, Panasonic, and Sony use it to store 3D images. digiKam can process MPO-formatted images thanks to an ImageMagick codec and the ExifTool parser working in the background.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://static.inaturalist.org/photos/2378794/large.jpg?1442169418" width="600" /><br /><figcaption>iNaturalist</figcaption></figure>

Also included is a new plugin that lets you export photos to <a href="https://www.inaturalist.org/">iNaturalist</a>. iNaturalist is a social network of 130K+ active naturalists, citizen scientists, and biologists built on the concept of mapping and sharing observations of biodiversity across the globe. iNaturalist receives observations of plants, animals, fungi, and other organisms worldwide.

Finally, the <i>Find Duplicates</i> has been improved as has the slideshow export.

The digiKam 7.3.0 source code tarball, Linux 64 bits AppImage bundles, MacOS Intel package, and Windows 64 bits installers <a href="https://download.kde.org/stable/digikam/7.3.0/">can be downloaded here</a>.

<a href="https://snapcraft.io/digikam"><img src="https://apps.kde.org/assets/snapstore-badge-en.svg" /></a>

<h2><i>Beta:</i> Subtitle Composer</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="https://subtitlecomposer.kde.org/assets/img/screenshot.png" width="600" /><br /><figcaption>Subtitle Composer</figcaption></figure>

We love making videos, and, to make those videos accessible, it's great to have subtitles in them.  <a href="https://subtitlecomposer.kde.org/">Subtitle Composer</a> is a new app from KDE that helps you add texts to your videos. You can set the timing, font, size and add multiple languages.

For translations and transcriptions, you can work with two subtitles side-by-side. Subtitle Composer lets you adapt the waveform to your liking, fit your subtitles to your needs and change the interface itself! The user interface is adaptable, so you can use whatever colours work best with the video and you can move panels to find a workflow that is the most comfortable for you.

It works with all the subtitle file formats, including text and graphical subtitle formats, formats supported by ffmpeg, demux formats, MicroDVD, and graphical formats supported by ffmpeg.

It does speech recognition too, so it can even add the subtitles for you. And you can quickly and easily sync subtitles by dragging several anchors/graftpoints and stretching the timeline, doing time shifting and scaling, lines duration re-calculation, framerate conversion then joining and splitting the subtitle files. Spell checking is included, of course, and it can even be scripted in multiple languages.

<a href="https://subtitlecomposer.kde.org/download.html">Download Subtitle Composer</a> for your Linux distro, as an AppImage, Flatpak or for MS Windows. Version 0.7.1 is the latest beta release.

<h2>Bugfixes</h2>

<ul>
<li>
<b>KDiff3</b>  <a href="https://apps.kde.org/kdiff3/">KDiff3</a> is a file and folder diff and merge tool which compares and merges two or three text input files or folders. Version <a href="https://mail.kde.org/pipermail/kde-announce-apps/2021-August/005664.html">1.9.3</a> fixes multiple regressions in file comparison, including trailing new lines at the EOF that are not preserved, random line insertion during merge, as well as a crash and compile failure.  <a href="https://download.kde.org/stable/kdiff3/">Grab it</a> for Linux, Windows or Mac.
</li>

<li>
<b>RSIBreak</b> reminds you to take a break from working at your computer from time to time, so you don't strain your eyes or wrists. <a href="https://mail.kde.org/pipermail/kde-announce-apps/2021-June/005662.html">RSIBreak 0.12.14</a> has added more languages and you can <a href="appstream://org.kde.rsibreak.desktop">install the new version now on Linux</a>.
</li>

<li>
Not technically an app but still cool, <b>Latte Dock</b> <a href="https://psifidotos.blogspot.com/2021/10/latte-dock-v0103-bug-fix-release.html">has released version 0.10.3</a> and one of the newest features is that indicators can now specify the background corner margin. <a href="appstream://org.kde.latte-dock.desktop">Get it now for Linux</a>.
</li>
</ul>
<!--break-->