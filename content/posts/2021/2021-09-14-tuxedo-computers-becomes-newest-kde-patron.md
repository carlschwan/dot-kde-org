---
title: "TUXEDO Computers Becomes the Newest KDE Patron"
date:    2021-09-14
authors:
  - "Paul Brown"
slug:    tuxedo-computers-becomes-newest-kde-patron
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/tuxedo_feiler_1500.jpg" />
   <figcaption align="center">Herbert Feiler, CEO TUXEDO Computers.</a></figcaption>
</figure>

<a href="https://www.tuxedocomputers.com/en">TUXEDO Computers</a>, a company known for selling Linux-powered computers and notebooks, now joins us as a KDE Patron!

"Our customized Linux notebooks (for work or play) are equipped with KDE Plasma, which leads to a positive response from our customers", said Herbert Feiler, CEO TUXEDO Computers. "Furthermore, we additionally do our own development work, which could benefit KDE as upstream as well. We are happy to share our knowledge and would like to secure as well as expand KDE's development work in the long run. Feedback that we receive from customers can also flow directly into KDE's development work."

"For KDE, reaching and serving end-users is part of our reason to exist and TUXEDO can be a great ally in this endeavour." said Aleix Pol, President of KDE e.V. "Together, we will get to expand our frontiers and create systems and tools to further serve our users. It's especially encouraging to see TUXEDO's commitment to join our development communities and collaborate towards making KDE products better for everyone." 

TUXEDO Computers joins KDE e.V.’s other Patrons: The Qt Company, SUSE, Google, Blue Systems, Canonical, Enioka, Slimbook and Pine64 to continue to support Free Open Source Software and KDE development through KDE e.V.
<!--break-->