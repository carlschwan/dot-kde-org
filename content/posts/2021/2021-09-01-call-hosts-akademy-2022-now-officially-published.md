---
title: "The Call for Hosts for Akademy 2022 is now officially published"
date:    2021-09-01
authors:
  - "sealne"
slug:    call-hosts-akademy-2022-now-officially-published
---
<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/Akademy_CfH.png" />
</figure>

Akademy 2022 is on its way and, despite the continuing pandemic, we are aiming for some in-person aspect for our next conference.

For the upcoming Akademy, we are looking to host it later in 2022, as we expect travel to be a bit more attainable and safe by then. Ideal dates would be in late summer or autumn 2022, specifically late August through October.

If you are interested in hosting Akademy in your city, please send a letter of intent or interest before a full bid by the middle of October 2021. In addition, we ask that you assemble a team of at least 3 people before applying.

<strong>Details of what to expect and what we are looking for are in the <a href="https://ev.kde.org/akademy/CallforHosts_2022.pdf">Call for Hosts</a></strong>[PDF]

If you have any questions or concerns we will be very happy to offer advice and a helping hand, so feel free to reach out anytime!

Please submit all letters of intents and bids to <a href="mailto:akademy-proposals@kde.org">akademy-proposals@kde.org</a>
<!--break-->