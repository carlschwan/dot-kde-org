---
title: "KDE's Summer App Update"
date:    2021-07-01
authors:
  - "jriddell"
slug:    kdes-summer-app-update
---
The KDE community makes a vast array of apps which get shipped in Linux distros, on Linux app stores, for Windows and Mac and for Android too.  Following the re-branding of our scheduled app releases to KDE Gear we are splitting out the app update into these separate articles which will cover the self-released apps where the projects themselves manage their own release schedule.  Here's what we have released in the last few months.

<h2>MyGNUHealth</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/mygnuhealth-kde-plasma-desktop_0.png"><img src="/sites/dot.kde.org/files/mygnuhealth-kde-plasma-desktop_0.png" width="600"/></a><br /><figcaption>MyGNUHealth</figcaption></figure> 

Brand new this week is the <a href="https://meanmicio.org/2021/06/24/welcome-to-mygnuhealth-the-libre-personal-health-record/">first stable release of MyGnuHealth</a>.

MyGNUHealth is a Health Personal Record application focused in privacy, that can be used in desktops and mobile devices.

MyGH has the functionality of a health and activity tracker, and that of a health diary / record. It records and tracks the main anthropometric and physiological measures, such as weight, blood pressure, blood sugar level or oxygen saturation. It keeps track of your lifestyle, nutrition, physical activity, and sleep, with numerous charts to visualize the trends.

MyGNUHealth is also a diary, that records all relevant information from the medical and social domain and their context. In the medical domain, you can record your encounters, immunizations, hospitalizations, lab tests, genetic and family history, among others. In the genetic context, MyGH provides a dataset of over 30000 natural variants / SNP from UniProt that are relevant in humans. Entering the RefSNP will automatically provide the information about that particular variant and it's clinical significance.

<br clear="all">
<h2>Digikam 7.2</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/digikam.png"><img src="/sites/dot.kde.org/files/digikam.png" width="600"/></a><br /><figcaption>Digikam</figcaption></figure> 
<a href="https://www.digikam.org/news/2021-03-22-7.2.0_release_announcement/">Digikam 7.2</a> adds an advanced batch rename feature.  Albums management has been improved with database fixes to support better items grouping with special use cases.  The tool to batch items in queues comes with a few improvements and fixes, especially when removable items are remaned while processing.  

<a href="https://files.kde.org/digikam/">Digikam</a> has bundle builds for Linux AppImages, Mac and Windows which all automatically update.

<br clear="all">
<h2>KGeoTag 1.0</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kgeotag_1.png"><img src="/sites/dot.kde.org/files/kgeotag_1.png" width="600"/></a><br /><figcaption>KGeoTag</figcaption></figure> 
Another new app is <a href="https://www.kphotoalbum.org/2021/02/27/0103/">KGeoTag</a>.  You can use KGeoTag to assign image files to GPS locations. This can help you with remembering the exact location where a photo was taken, or with discovering images that were taken at the same place. Of course, this is most useful when used together with another program such as KPhotoAlbum, that can adequately display this information and lets you search by GPS coordinates.

<br clear="all">
<h2>Kup 0.9</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kup_0.png"><img src="/sites/dot.kde.org/files/kup_0.png" width="600"/></a><br /><figcaption>Kup</figcaption></figure> 
Kup is a backup scheduler integrating with the Plasma desktop with support for versioned incremental backups using Bup and for sync-type of backup by using Rsync.

The new release adds detection of when a source folder has been removed, when this happens Kup aborts saving backup and prompts user to review what should be included. It also adds a way to prune old backup snapshots, with new small app kup-purger.

Kup is available with your Linux distro.

<br clear="all">
<h2>Neochat 1.2</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/neochat.png"><img src="/sites/dot.kde.org/files/neochat.png" width="600"/></a><br /><figcaption>NeoChat</figcaption></figure> 
Here at KDE we have just run our Akademy conference through the Matrix chat network.  <a href="https://carlschwan.eu/2021/06/01/neochat-1.2-bubbles-better-text-editing-and-more/">Neochat</a> is our app to use Matrix chats.

The first thing you will see then opening NeoChat 1.2 is that it is now using message bubbles. The text input component was completely redesigned. It’s also now possible to get autocompletion of commands.  It’s now possible to send customized reactions when replying to a message with /react  for example <code>/react I love NeoChat</code>.

Most importantly the fireworks and snow visual effects can now cheer up your day.

Neochat is available in your Linux distro and <a href="https://flathub.org/apps/details/org.kde.neochat">on Flathub</a>.

<br clear="all">
<h2>Skrooge 2.25</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/skrooge_screenshot_16x9_0_0.png"><img src="/sites/dot.kde.org/files/skrooge_screenshot_16x9_0_0.png" width="600"/></a><br /><figcaption>Skrooge</figcaption></figure> 

Money isn't the only way to make great software but here at KDE we make great money counting software anyway.  Accounting app <a href="https://skrooge.org/node/244">Skrooge 2.25</a> adds a couple of new features. Opt-out from accounts from the <a href="https://forum.kde.org">"accounts" widget in the Dashboard</a>.
Also included in this release is a new dashboard layouts: by columns and with layouts in layouts.

<br clear="all">
<h2>Tellico 3.4</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/tellico_tvmaze.png"><img src="/sites/dot.kde.org/files/tellico_tvmaze.png" width="600"/></a><br /><figcaption>Tellico</figcaption></figure> 

Tellico is a software application for organizing your collections. It provides default templates for books, bibliographies, videos, music, video games, coins, stamps, trading cards, comic books, and wines. You can also create your own custom collection of anything you like!

<a href="https://tellico-project.org/tellico-3-4-released/">Tellico 3.4</a> is available, with several new features, updated data sources, bug fixes and build changes. 

App author Robby Stephenson says "<em>I’m excited about this release. It pulls together a few different things that I’d been working on. I’ve got a few more ideas about charts and reports to be added. The additional coin and stamp data sources really fill a niche. Everything seems to be working really nicely together, so I’m happy to put 3.4 out.</em>".

<h2> Bugfixes</h2>

We also made a bunch of bugfix releases for our apps.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kstars_0.png"><img src="/sites/dot.kde.org/files/kstars_0.png" width="600"/></a><br /><figcaption>KStars</figcaption></figure> 

<ul>
<li><a href="https://knro.blogspot.com/2021/03/kstars-v352-is-released.html">KStars 3.5.2</a> adds Polar Alignment can now be performed while pointing anywhere in the sky. Thus, for example, if a tree is blocking your view of the pole, you can still polar align.</li>
<li><a href="https://labplot.kde.org/2021/04/01/labplot-2-8-2-released/</a>Labplot 2.8.2</a> Labplot won an Akademy Award this year, try out the new release which contains multiple bug fixes and small new features.</li>
<li><a href="https://krita.org/en/item/krita-4-4-5-released/">Krita 4.4.5</a> needs no introduction.  The team is are preparing for a major new release but until then this bugfix which removes a nasty bug on MacOS.</li>
<li><a href="https://gcompris.net/index-en.html#2021-03-21">GCompris 1.1</a> updates graphics in 21 of the activities and not provides a .msi windows installer.  Get it from the Microsoft store, Snap Store, Android or even Raspberry Pi.</li>
<li><a href="https://kmymoney.org/2021/06/23/kmymoney-5-1-2-released.html">KMyMoney 5.1.2</a> fixes the major issue of budget not using Fiscal Year.</li>
<li><a href="https://mail.kde.org/pipermail/kde-announce-apps/2021-June/005661.html">Kid3</a>, our audio tag editor, fixes a crash in Windows installs</li>
<li><a href="https://mail.kde.org/pipermail/kde-announce-apps/2021-May/005655.html">KDiff3</a> added MacOS X and Windows 64 bit builds.
<li>And lastly the image viewer Kuickshow quietly released 0.10.2 to update it to current versions of Qt and KDE Frameworks</li>
</ul>

<h2>Betas</h2>

Music app <a href="https://mail.kde.org/pipermail/kde-announce-apps/2021-February/005637.html">Amarok 3.0 Alpha</a> was released for early testing.  And Comic reader <a href="https://mail.kde.org/pipermail/kde-announce-apps/2021-April/005651.html">Peruse 2.0 Beta 1</a> is out for trial.

<h2>App Stores</h2>

KDE's apps are available through an increasing number of channels.  Your Linux distribution usually carries the latest version but you can also get it direct from app stores.

<a href="https://snapcraft.io/publisher/kde">Snap store</a> has over 100 apps.  <a href="https://flathub.org/apps/search/kde">Flathub</a> has recently gained VVave.  

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kdeconnect.png"><img src="/sites/dot.kde.org/files/kdeconnect.png" width="600"/></a><br /><figcaption>KDE Connect preview on the Windows Store</figcaption></figure> 
New on the Microsoft Store is a preview of <a href="https://www.microsoft.com/en-gb/p/kde-connect/9n93mrmsxbf0">KDE Connect</a> for Windows.  Give it a try.

<!--break-->

