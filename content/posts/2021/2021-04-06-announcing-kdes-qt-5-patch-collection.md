---
title: "Announcing KDE's Qt 5 Patch Collection"
date:    2021-04-06
authors:
  - "apol"
slug:    announcing-kdes-qt-5-patch-collection
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/Qt_KDE_logo_2016_smaller.png" alt="Qt and KDE logos" />

As Qt 5 support is drawing to a close, and we shift to Qt 6, we need to ensure that KDE products are as reliable as ever. To this end, KDE will be maintaining a set of patches with security and functional fixes so that we can enjoy good KDE Software still based on Qt5 until our software is reliably based on Qt 6. 

You can find more information on the technical details of this patch collection <a href="https://community.kde.org/Qt5PatchCollection">here</a>.

<blockquote>
“<i>The Qt Company and KDE have been co-operating in development of Qt 6 actively and KDE is well set to migrate to Qt 6. Even though our aim has been to make porting to Qt 6 easy and straightforward, we do understand that with a large framework like KDE has porting to Qt 6 takes some time, and such a patch collection can help manage the transition.</I>”
</blockquote>
— Tuukka Turunen, The Qt Company, SVP R&D

<blockquote>
"<I>To transition to great future technologies like Qt 6 we need to have the peace of mind that our current users are catered for. With this patch collection we gain the flexibility we need to stabilize the status quo. This way we can continue collaborating with Qt and deliver great solutions for our users.</I>" 
</blockquote>
— Aleix Pol, KDE e.V. President

In this vein, we encourage everyone to participate in the KDE Frameworks 6 ongoing effort.
<!--break-->