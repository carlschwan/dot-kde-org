---
title: "Another KDE success story - the Incubator - Part 2"
date:    2015-08-21
authors:
  - "jpwhiting"
slug:    another-kde-success-story-incubator-part-2
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/middleGComprisLogo.png" /></div> 

Proceeding with the next story about the <a href="https://community.kde.org/Incubator">KDE Incubator</a> with the story of <a href="http://gcompris.net">GCompris</a>.

GCompris is a high quality educational software suite comprising of numerous activities for children aged 2 to 10. It started in 2000 using the GTK+ toolkit and was part of the Gnome project. In order to address users willing to run GCompris on their tablets, a full rewrite has been initiated in 2014 using <a href="http://doc.qt.io/qt-5/qtquick-index.html">Qt Quick</a>.

GCompris had the chance to be accepted by KDE and followed the <a href="https://community.kde.org/Incubator/Projects/GCompris">incubation stage</a> for about a year. It has now been accepted as an official KDE project in its extragear section.

Being a community project, there was a need to find a new one focused on Qt technology to help us continue the development the way we like it. GCompris targets schools and parents all over the world and it is mandatory to provide it in the language they speak. Only a large community like KDE can manage such a daunting task in the long run. GCompris also benefits from many KDE developers.

Since January 2015 a binary version is available on the <a href="https://play.google.com/store/apps/details?id=net.gcompris">Google Play Store</a> for Android. Thanks to the nice multi-platform support coming with Qt, it will be provided on other platforms within the year to come.

And as you might already know. GCompris is one of the projects that participates at this year's Randa Meetings soon to start. But this is not the only sprint KDE organizes and needs your helps with:

<p><a href="https://www.kde.org/fundraisers/kdesprints2015/"><img src="/sites/dot.kde.org/files/Fundraiser-Banner-2015.png" /><center><b>Donate to the KDE Sprints 2015 fundraising campaign</b></center></a></p>

In a few days we'll tell you about another incubated project that helps you in a whole new topic.