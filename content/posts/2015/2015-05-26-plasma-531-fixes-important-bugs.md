---
title: "Plasma 5.3.1 Fixes Important Bugs"
date:    2015-05-26
authors:
  - "jriddell"
slug:    plasma-531-fixes-important-bugs
---
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<a href="https://www.kde.org/announcements/plasma-5.3/plasma-5.3.png">
<img src="https://www.kde.org/announcements/plasma-5.3/plasma-5.3-ickle.png" style="border: 0px" width="600" height="337" alt="Plasma 5.3" />
</a>
<figcaption>Plasma 5.3</figcaption>
</figure>

<p>
Today KDE releases a bugfix update to Plasma 5, versioned 5.3.1.   <a href='https://www.kde.org/announcements/plasma-5.3.0.php'>Plasma 5.3</a> was released in January with many feature refinements and new modules to complete the desktop experience.
</p>

<p>
This release adds a month's worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important for example:
</p>

<ul>

<li> <a href='https://sessellift.wordpress.com/2015/05/14/in-free-software-its-okay-to-be-imperfect-as-long-as-youre-open-and-honest-about-it/'>show desktop</a> has now been fixed</li>
</ul>



<a href="https://www.kde.org/announcements/plasma-5.3.0-5.3.1-changelog.php">
Full Plasma 5.3.1 changelog</a>

<!-- // Boilerplate again -->

<h2>Live Images</h2>

<p>
The easiest way to try it out is with a live image booted off a USB disk. You can find a list of <a href='https://community.kde.org/Plasma/LiveImages'>Live Images with Plasma 5</a> on the KDE Community Wiki.
</p>

<h2>Package Downloads</h2>

<p>Distributions have created, or are in the process of creating, packages listed on our wiki page.
</p>

<ul>
<li>
<a href='https://community.kde.org/Plasma/Packages'>Package download wiki page</a></li>
</ul>

<h2>Source Downloads</h2>

<p>You can install Plasma 5 directly from source. KDE's community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>. Note that Plasma 5 does not co-install with Plasma 4, you will need to uninstall older versions or install into a separate prefix.
</p>

<ul>
<li>
<a href='https://www.kde.org/announcements/info/plasma-5.3.1.php'>Source Info Page</a>
</li>
</ul>

<h2>Feedback</h2>

You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a> or <a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a> or <a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.
<p>
Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.</a>
</p>

<p>You can provide feedback direct to the developers via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>.  If you like what the team is doing, please let them know!</p>

<p>Your feedback is greatly appreciated.</p>