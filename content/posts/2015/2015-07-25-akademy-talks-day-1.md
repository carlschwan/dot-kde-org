---
title: "Akademy Talks Day 1"
date:    2015-07-25
authors:
  - "jriddell"
slug:    akademy-talks-day-1
---
<a href="/sites/dot.kde.org/files/19370281324_b48639e5f9_b.jpg" title="IMG_6119"><img src="/sites/dot.kde.org/files/19370281324_b48639e5f9_n.jpg" width="256" height="320" alt="IMG_6119" style="float: right; padding: 2ex"></a>
The KDE community has spent the day in western Spain giving and watching talks showing new developments in the community and where we are likely to be going in the next year.

The city of A Coruña is warm and welcoming, and almost reminiscent of Akademy 2013 in Bilbao, with the hilly backdrops and the oceanic linings as you see dark blue and black coloured t-shirted KDE community people trampling their way and wheezing throughout the trek up and downhill the steep tracks from the residences to the University Area where the Akademy Attendees have found their cosy haven.
<!--break-->
<p>
The day opened with a keynote by Matthias Kirschner  from the Free Software Foundation Europe.  He gave a passionate talk covering the ways in which universally useful machines are deliberately restricted to prevent the owners from doing something.  He covered DVD players forcing users to not skip the copyright notice, CDs with rootkits installed and e-books which can not be lent to your friends.  But he gave a number of ways we can keep resisting and resolving these manufactured problems including talking to people who accept restrictions.</p>

<p>For those that couldn't attend Akademy you can <a href="http://files.kde.org/akademy/2015/videos/Matthias%20Kirschner%20-%20An%20Endangered%20Species:%20The%20Computer%20as%20a%20Universal%20Machine.webm">watch the video</a> of Matthias' keynote. There will be a further announcement when the rest of the Akademy videos are available.</p>

<br clear="all" />
<div style="text-align:center">
 <video width="600" height="400" controls="true">
  <source src="http://files.kde.org/akademy/2015/videos/Matthias%20Kirschner%20-%20An%20Endangered%20Species:%20The%20Computer%20as%20a%20Universal%20Machine.webm" type="video/webm">
Your browser does not support the video tag.
</video> 
</div>
<br clear="all" />

<p><a href="/sites/dot.kde.org/files/19371924863_9f2bb3cfce_b.jpg" title="IMG_6192"><img src="/sites/dot.kde.org/files/19371924863_9f2bb3cfce_n.jpg" width="256" height="320" alt="IMG_6192" style="float: right; padding: 2ex"></a></p>
David gave a lively talk about the status of our flagship desktop product, Plasma 5.  He gave some graphs by industry research companies about how desktops are still just as relevant to users as ever and how the Linux desktop has maintained position as the minor alternative without losing any relevance.  He looked at the state of Plasma 5 which is now a year old and being picked up by distributions with almost universally favourable reviews and he said Plasma 5 was set as the leading desktop out of several available to Linux distributions.  He finished by looking at some future development in Plasma including the forthcoming revival of the Kiosk framework, a unique feature that KDE used to have and which is much in demand.

<br clear="all" /> 
<a href="/sites/dot.kde.org/files/19805187970_452e832515_b.jpg" title="IMG_6255"><img src="/sites/dot.kde.org/files/19805187970_452e832515_n.jpg" width="256" height="320" alt="IMG_6255" style="float: right; padding: 2ex"></a>
One of the most significant talks of the day was from Boud and Sebas who <a href="https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform">announced the re-launched Plasma Mobile project</a>.  This updates the Plasma Mobile shell to work with Plasma 5 and the first images are created by <a href="http://kubuntu.plasma-mobile.org/">Kubuntu</a> to install on a Nexus 5.  This will be developed along with the rest of Plasma and you're welcome to join us in #plasma IRC channel on Freenode to get involved.

<br clear="all" />
<a href="/sites/dot.kde.org/files/19966926396_0c1c95203e_b.jpg" title="IMG_6257"><img src="/sites/dot.kde.org/files/19966926396_0c1c95203e_n.jpg" width="256" height="320" alt="IMG_6257" style="float: right; padding: 2ex"></a>
Ricardo Iaconelli gave a talk on WikiFM and the visions of the new KDE project with brings out free course material to people be it Calculus or a quick QML course. Integrating the elements of students, FOSS Collaboration and Technology with virtual machines helping immediate development are some of the major highlights of WikiFM. A lot of student universities with 200 odd user/day building up is the major direction in which WikiFM is growing with time. People from CERN, Fermilab, Princeton University, Stanford Linear Accelerator, have also decided to use WikiFM for their official training. You can find more about it <a href="http://blogs.fsfe.org/ruphy/2015/07/announcing-wikifm/">on Ricardo's blog</a>.

<br clear="all" />
<a href="/sites/dot.kde.org/files/19806224788_670a4c1f07_b.jpg" title="IMG_6298"><img src="/sites/dot.kde.org/files/19806224788_670a4c1f07_n.jpg" width="256" height="320" alt="IMG_6298" style="float: right; padding: 2ex"></a>
Devaja spoke on how to build KDE Love.  She showed some videos made the night before of contributors talking about what made them stay with KDE.  She spoke about how KDE India has been built and how it often needs a personal touch to help people into the community giving the example of Pradeepto explaining to a class of students how to code with Qt realising that a classroom talk wasn't working so sat with each individually to work out what problems each had and how to solve them.

<br clear="all" />
A lunch treat by Blue Systems filled us with salads and ham and chicken and French fries topped with yoghurt, pudding and bread with water to go, serving as the perfect rejuvenating experience for the almost slovenly stances of the attendees by the time the wearing summery afternoons dawned on them. Pink and Orange and Blue balloons served as beacons highlighting the pathway from the University to the Restaurant and the trek on the way served as a pleasant surprise.

<br clear="all" />
<a href="/sites/dot.kde.org/files/20002140961_f178bae138_b.jpg" title="IMG_6412"><img src="/sites/dot.kde.org/files/20002140961_f178bae138_n.jpg" width="256" height="204" alt="IMG_6412" style="float: right; padding: 2ex"></a>
The annual talks from Season of KDE and Google Summer of Code projects covered a range of projects.  Francesco Wofford and Claudio Desideri, pictured, spoke about their Open Collaboration Services server <a href="https://community.kde.org/Ocs-server">ocs-server</a> and clients which are being developed as an alternative to openDesktop.org.  Scarlett Clark showed off the new <a href="http://build.kde.org">KDE continuous integration system</a> which she had spent some months learning and a lot of Groovy code settings up.

The talk by Bruno Coudoin was all about GCompris and how they evolved from an incubation to now becoming one of the topmost Children's Educational Apps - rated at 27th most Grossing Paid Educational App in France on the Google Play Store. He talked about the entire journey and how they worked with a dedicated team of contributors which has now grown to 60 developers included 2 GSoC students. The Fundraiser project revamping the entire Graphics of Gcompris by Timothee and the business models of paid apps for Android and Windows and Distribution to Schools was also discussed which serve as sources to keep the GCompris Project up and running. There is a live desk outside the main Auditorium at the University wherein the Gcompris app is usable for trying out on the Android, IOS, Windows and Linux devices and surprisingly quite fun and sometimes challenging for Adults as well! Do give it a try, to bring out the child in you!

<a data-flickr-embed="true" href="https://www.flickr.com/photos/jriddell/19813211669/in/album-72157655875336760/" title="IMG_6539"><img src="https://farm1.staticflickr.com/392/19813211669_406dfa1e88_n.jpg" width="256" height="320" alt="IMG_6539" style="float: right; padding: 2ex"></a>
Boud gave a second talk on moving Krita from winning an Akademy Award to winning the even more prestigious ImagineFX award. Over the years, Krita has grown from a fun hobby project that tried to nothing less than cloning Photoshop to an application that actually is being used by professional artists all over the world. The first book on Krita was published in Japan (the second one will be in English). The community is growing, the user-base is growing and the development speed is growing enormously. The upcoming plans for Krita are the Port to Qt 5 and improving the version of it on Mac OS X and also better animation support which is being implemented as a Summer of Code Project right now. Also, the Secrets of Krita which is the third DVD is coming out soon and much more is planned in the months to come.

<br clear="all" />
<a href="https://www.flickr.com/photos/jriddell/sets/72157655875336760">Photos by Nim Kibbler</a> and <a href="https://www.flickr.com/photos/40864317@N08/sets/72157655913592459">Alex Merry</a> are bring uploaded throughout the day.

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

For more information, please contact The <a href="https://akademy.kde.org/2015/contact">Akademy Team</a>.