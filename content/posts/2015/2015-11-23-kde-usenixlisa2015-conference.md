---
title: "KDE at USENIX/LISA2015 Conference"
date:    2015-11-23
authors:
  - "mpyne"
slug:    kde-usenixlisa2015-conference
---
<div style="float: right; margin: 0ex 1ex 3ex 1ex; "><img src="/sites/dot.kde.org/files/KDE_USENIX14111875pct.png" /></div>
USENIX, in cooperation with LOPSA (League of Professional System Administrators), presented the <a href="https://www.usenix.org/conference/lisa15">2015 LISA</a> (Large Installation System Administration) Conference in Washington, D.C. USA from 8 November to 13 November. Two members of the KDE Community represented KDE at the <a href="https://www.usenix.org/conference/lisa15/lisa-expo">Conference Expo</a>, connecting with many of the 1,060 attendees to discuss successful large scale deployment and other KDE goodness.

Yash Shah and Michael Pyne demonstrated the features of the Plasma 5 desktop and underlying KDE Frameworks, with a focus on how those features can help meet enterprise IT needs.
<!--break-->

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/Yash_LISA15_2015-11-11550.jpg">
<strong>Yash Shah</strong></div><br />

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/Michael_LISA15_2015-11-11550.jpg">
<strong>Michael Pyne</strong></div><br />

Many users visited the KDE exhibit space with fond memories and reported about how they use KDE technology and how they've deployed it within their own organizations. Several people said that they have used KDE since version 1.0. For the most part, the LISA attendees were Linux savvy and well acquainted with KDE.

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/KDEBooth_LISA15_2015-11-11550.jpg">
<strong>KDE's exhibit space - Simple by Default. Powerful when needed.</strong></div><br />

The demos were well received, especially by users who had not recently used KDE software. They were surprised by how much the desktop had advanced since they last used it.

Attendees showed interest in KDE's cross-platform support (especially on FreeBSD), and its configurability (especially to run on hardware with less computing power). Several people made feature requests as well. The few complaints were invariably about changing or removing configurability. On the other hand, some happy users said that configurability is something that they enjoy and that it keeps them using KDE technology.

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/Reminiscing_LISA15_2015-11-11550.jpg">
<strong>KDE fans</strong></div><br />

The similarity in user experience between recent versions of the KDE 4 desktop and Plasma 5 made for a good story about the ongoing stability of KDE desktop technology. This stability, along with KDE's classic customizability, appealed to technical staff people who just want to get work done. 

<h2>Demonstrations</h2>
Yash and Michael demonstrated and discussed many aspects of KDE software, including:
<ul>
<li>KWin's  windowing capabilities and effects, along with the ability to fallback to XRender or disable compositing if necessary to improve performance,</li>
<li>Kate's SQL plugin,</li>
<li>Plasma 5's seamless support for KDE 4 applications, featuring the Krita digital painting application and other leading edge capabilities (the “KDE ASCIIquarium" screensaver was surprisingly popular),</li>
<li>KDevelop5 for Plasma 5 with semantic highlighting,</li>
<li>KDE Kiosk (a library feature useful for sysadmins deploying many desktops),</li>
<li>KDE Connect,</li>
<li>Ease of porting Qt4/KDE4 code to run on Qt5 and KDE Frameworks 5, and</li>
<li>The extensive Qt & KDE symbiosis.</li>
</ul>

<h2>The LISA Conference</h2>
The LISA Conference has long served as the annual vendor-neutral meeting place for the wider system administration community. Recognizing the overlap and differences between traditional and modern IT operations and engineering, the highly-curated 6-day program offers training, workshops, invited talks, panels, paper presentations, and networking opportunities around 5 key topics: Systems Engineering, Security, Culture, DevOps, and Monitoring/Metrics.

<h2>Thanks</h2>
Many thanks to <a href="http://usenix.org/">USENIX</a> for the generous support of KDE at the LISA 2015 Conference. Special thanks to the USENIX staff; they are superb.
