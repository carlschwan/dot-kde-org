---
title: "GCompris Fundraiser"
date:    2015-05-17
authors:
  - "animtim"
slug:    gcompris-fundraiser
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/middleGComprisLogo.png" /></div> 
The work on unified graphics for GCompris was completed in the time allocated by the fundraiser. Here is a <a href="https://www.youtube.com/embed/XGmdt-mbokg?">video</a> to show the result.

15% of the goal was funded, so not all of the work could be completed in the time proposed. But there are updates to all the core components, the main menu with all the activities icons and a good bunch of activities.

<h2>Finishing the remaining activities</h2>
<strong>Timothée Giet</strong> will keep working on GCompris in his spare time. There's still a large amount of work to do. If you want to help, please read the <a href="http://timotheegiet.com/gcompris/artwork-guidelines.html">guidelines</a> and take a look at the work already done. Then contact the development team on IRC (#gcompris channel on freenode) for some guidance to get started.

Thanks to everyone who helped make this work possible!

