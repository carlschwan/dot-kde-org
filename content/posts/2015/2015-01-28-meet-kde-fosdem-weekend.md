---
title: "Meet KDE at FOSDEM this Weekend"
date:    2015-01-28
authors:
  - "jriddell"
slug:    meet-kde-fosdem-weekend
comments:
  - subject: "ReactOS"
    date: 2015-01-30
    body: "Please meet with ReactOS team\r\nAW building,  stand 12"
    author: "Alex Rechitskiy"
---
<img src="https://fosdem.org/2015/assets/style/bg-visual-panel-73f1975ffc7435da092f7de492966249bd09606d7e9d6241cc31591203c5a2d6.png" width="126" height="124" align="right" />
KDE will be at Europe's largest gathering of free software developering this weekend, taking over the city of Brussels for FOSDEM.  We start with the traditional beer event on the Friday, sampling 100 flavours of beer while we mingle with old friends and new.  On Saturday we will have a stall showing off Plasma 5.2, our beautiful desktop launched only yesterday.  We will also show off upcoming ideas like KDE software on Android.  There will be t-shirts to buy for those who wants to show their support for the original and best free software desktop community.  Saturday evening sees the Kubuntu 10th anniversary party in Grand Place to which all KDE friends are welcome (but remember to book on first).  On Sunday we'll be running the <a href="https://fosdem.org/2015/schedule/track/desktops/">Desktop devroom</a> with our friends from other projects.  Bruno Coudoin will be talking about his port to Qt of GCompris.  Hope to see you there!
