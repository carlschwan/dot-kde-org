---
title: "LaKademy 2015 to be in Salvador, Bahia, Brazil"
date:    2015-04-21
authors:
  - "araceletorres"
slug:    lakademy-2015-be-salvador-bahia-brazil
---
<div style="float: center; padding: 1ex; margin: 1ex; text-align:center;"><img src="https://br.kde.org/sites/br.kde.org/files/field/image/lakademy2014-groupphoto.jpg" /><br />LaKademy 2014</div>
The KDE Community in Brazil will host <a href="https://www.youtube.com/watch?v=TxpcoxR1Jgg">LaKademy 2015</a> June 3rd through 6th. The conference is an opportunity for KDE users and contributors to meet in person to make plans, work on software and other aspects of KDE technology. There will also be outreach to potential new contributors. The group is <a href="https://br.kde.org/node/283">raising money</a> for conference expenses and to offset travel costs for attendees.
<!--break-->

Brazil has had great success with Free and Open Source Software (FOSS) initiatives, often due to the leadership of KDE people. LaKademy is a way to continue this advocacy, while making technical advances and strengthening the KDE presence. Certainly LaKademy provides local benefits. But the value created also has wider consequences. Brazil joins <a href="https://www.kde-espana.org/akademy-es2015/index.php">Spain</a> and <a href="https://kde.in/conf">India</a> in promoting the global reach and stature of KDE technology.

The fundraising goal is modest—R$ 10,000 (slightly more than € 3000). The <a href="https://www.youtube.com/watch?v=TxpcoxR1Jgg">LaKademy 2015 video</a> features previous editions of LaKademy as well as more information about the upcoming conference. <strong>Please <a href="https://br.kde.org/node/283">support</a> this important event</strong>.

Information about LaKademy and donating <a href="https://br.kde.org/node/284">in Spanish</a>.
