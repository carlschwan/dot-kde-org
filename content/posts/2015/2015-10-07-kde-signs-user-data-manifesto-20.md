---
title: "KDE Signs the User Data Manifesto 2.0"
date:    2015-10-07
authors:
  - "jriddell"
slug:    kde-signs-user-data-manifesto-20
comments:
  - subject: "Nice to see as GNOME apps seem to go the other way"
    date: 2015-10-08
    body: "<p>This is good.&nbsp; Recent GNOME apps such as \"Music\", \"Photos\" and \"Boxes\" all are locked down and won't let you configure where your data is stored.&nbsp; Hopefully they will reverse this trend.&nbsp; It's hard to share when it's like that.</p>"
    author: "George"
---
<img src="/sites/dot.kde.org/files/user-data-manifesto.png#overlay-context=2015/10/07/kde-signs-user-data-manifesto-20" width="620" height="116" />

KDE, through its legal body KDE e.V., is one of the launch partners and initial signatories of the <a href="https://userdatamanifesto.org/">User Data Manifesto 2.0</a>. The User Data Manifesto defines basic rights for people to control their own data in the internet age:

<ul>
<li>Control over user data access</li>
<li>Knowledge of how the data is stored</li>
<li>Freedom to choose a platform</li>
</ul>

KDE e.V. President <a href="http://blog.lydiapintscher.de/2015/09/03/kde-signs-the-user-data-manifesto-2-0-and-continues-to-defend-your-freedom/">Lydia Pintscher explains</a> "I believe that in today’s world where more and more of our daily life depends on technology it is crucial that people have control over that technology. You should be empowered to know what your technology does and you should be empowered to influence it. This is at the core of Free Software. Unfortunately it is not at the core of most of the technology people interact with every day – quite the opposite – walled gardens and locks wherever you look with few exceptions.

"KDE is working hard to provide you with technology that you control every single day so you are empowered and the one ultimately in charge of your technology, data and life – the basis for freedom for many today. This is written down in the first sentence of our manifesto: “We are a community of technologists, designers, writers and advocates who work to ensure freedom for all people through our software.”

"Do you want to <a href="https://community.kde.org/Get_Involved">join us</a> in providing more people with more access to Free technology? Today is a good day!
<!--break-->