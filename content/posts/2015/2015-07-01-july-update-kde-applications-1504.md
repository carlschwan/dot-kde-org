---
title: "July Update for KDE Applications 15.04"
date:    2015-07-01
authors:
  - "unormal"
slug:    july-update-kde-applications-1504
---
<figure style="width: 150px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /><figcaption align="center">KDE App Dragons</figcaption></figure>

Today, the KDE Community is happy to announce the release of KDE Applications 15.04.3. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to Kdenlive, the Kontact Suite, Kopete, the KDE Telepathy contact list, Marble, Okteta and Umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.21, the KDE Development Platform 4.14.10 and the Kontact Suite 4.14.10.
 
To learn more about this release, refer to the <a href="https://www.kde.org/announcements/announce-applications-15.04.3.php">full announcement</a>.