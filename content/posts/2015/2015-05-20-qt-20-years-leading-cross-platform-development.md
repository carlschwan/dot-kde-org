---
title: "Qt - 20 years leading cross-platform development"
date:    2015-05-20
authors:
  - "sandroandrade"
slug:    qt-20-years-leading-cross-platform-development
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/Qt20Birthday350.png" /></div>
Today we celebrate 20 years since the first release of Qt was uploaded to sunsite.unc.edu and <a href="http://diswww.mit.edu/bloom-picayune.mit.edu/linuxch-announce/598">announced, six days later, at comp.os.linux.announce</a>. Over these years, Qt evolved from a two person Norwegian project to a full-fledged, social-technical world-wide organism that underpins free software projects, profitable companies, universities, government-related organizations, and more. It's been an exciting journey. 
<!--break--> 
From the early days of Trolltech in 1999, through an evolution of licensing (from the original FreeQt, to <a href="http://www.ipst-info.net/download/LinuxJournal/LJ/061/3306.html">QPL</a>, to <a href="http://www.linuxplanet.com/linuxplanet/reports/2269/1">GPL</a>, to <a href="https://blog.qt.io/blog/2009/01/14/nokia-to-license-qt-under-lgpl/">LGPL</a> today), corporate cooperation from <a href="http://www.engadget.com/2008/01/28/nokia-acquires-trolltech-the-biggest-little-company-youve-ne/">Nokia</a> and <a href="http://www.digia.com/en/Home/Company/News/Digia-to-acquire-Qt-from-Nokia/">Digia</a>, <a href="https://blog.qt.io/blog/2011/10/21/the-qt-project-is-live/">Open Governance</a>, and leading edge technology refinements, Qt has supported the spirit of free software, thriving communities, and high quality products.

The KDE Community thanks everyone who helps keep Qt rocking; we share our pride in being part of this history. Since 1997, Qt has provided the foundation upon which KDE has developed its workspaces, applications, and development environments. Moreover, Qt has contributed to a fruitful symbiosis where goals, contributions, and discussions blur the boundaries between the Qt and KDE projects. As a result, today KDE is the biggest Qt showcase in the world, and there's evidence that this successful and long-running partnership will continue.

<h2>The Qt/KDE partnership</h2>
There are several fundamental aspects of this strong relationship.
<ul> 
<li>KDE has always worked hard to keep Qt free and open through the <a href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>. Since its creation in 1998, the Foundation makes continuous updates in its statutes, aiming not only at more precise and complete terms but also to accommodate new situations involving Qt's development.</li>
<li>KDE people <a href="http://pusling.com/blog/?p=362">make 40-60% of the weekly commits</a> in the QtBase repository. As in every major transition between Qt versions, KDE had close and active participation during the development of Qt 5, contributing <a href="https://community.kde.org/Frameworks/Epics/Contributions_to_Qt5_for_KF5.1">many new features and enhancements</a> which expanded benefits to the whole class of Qt applications. Furthermore, since Qt adopted the <a href="https://blog.qt.io/blog/2011/10/21/the-qt-project-is-live/">Open Governance model in October 2011</a>, contributing to Qt has become even easier, not only for KDE people but for anyone interested in the project's trends, roadmap, and technologies.</li>
<li>Conceiving KDE Frameworks 5 as a set of fine-grained and independent Qt 5 add-on modules demonstrates our confidence in Qt's commitment to KDE efforts. In turn, KDE Frameworks 5 contributes to the entire ecosystem of Qt developers by making available <a href="https://www.kde.org/announcements/kde-frameworks-5.0.php">many high-quality libraries</a>—based on over 18 years of KDE experience in building Qt applications.</li>
<li>Many people interested in Qt development start their efforts in <a href="https://dot.kde.org/2015/01/18/google-summer-code-2014-student-projects">KDE projects with seasoned mentors</a>. KDE offers several opportunities for young people to do <a href="https://dot.kde.org/2014/10/27/season-kde-2014">real-world work</a>.  As a consequence, over time, KDE is the first Qt experience for a growing number of highly skilled Qt developers, who have learned the Qt way of doing things from the beginning.</li>
</ul>

<h2>Congratulations!</h2>
Because of all this, we say again with much appreciation: congratulations to all of Qt. Thank you, and keep counting on KDE!