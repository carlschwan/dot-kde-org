---
title: "KDE Applications 15.08 - Kontact Suite Technical Preview and Dolphin 5"
date:    2015-08-19
authors:
  - "unormal"
slug:    kde-applications-1508-kontact-suite-technical-preview-and-dolphin-5
comments:
  - subject: "Stupid unecessary modifications -> block updates"
    date: 2015-10-06
    body: "<p>Modify application UI just for the sake of changing it. Looks like copying from Microsoft. For me the consequence is clear: Go back, never update again. Security problems? I don't give a shit anymore. I'm fed up with all this hazle.</p>"
    author: "Jochen"
---
<figure style="float: right; margin: 0px">
<a href="/sites/dot.kde.org/files/dolphin15.08_0.png">
<img src="/sites/dot.kde.org/files/dolphin15.08_0.png" width="400" height="242" />
</a>
<figcaption><center>Dolphin's new look - based on KDE Frameworks 5</center></figcaption>
</figure>

Today KDE released <a href="https://www.kde.org/announcements/announce-applications-15.08.0.php">KDE Applications 15.08</a>, the collection of more than 150 applications.  This release features the Kontact Suite and Dolphin ported to KDE Frameworks 5.

<h3>Kontact Suite technical preview</h3>

Over the past several months, the KDE PIM team put a lot of effort into porting Kontact to Qt 5 and KDE Frameworks 5. In addition, data access performance has been improved considerably by an optimized communication layer. The KDE PIM team is focused on further polishing the Kontact Suite, and would appreciate feedback. For detailed information about KDE PIM changes, see <a href="http://www.aegiap.eu/kdeblog/">Laurent Montel's blog</a>.

<h3>Kdenlive and Okular</h3>

This release of Kdenlive includes fixes in the DVD wizard, along with many bug-fixes and other features, including the integration of some bigger refactorings. More information about Kdenlive changes can be seen in its <a href="https://kdenlive.org/discover/15.08.0">changelog</a>. Okular now supports Fade transition in the presentation mode.

<figure style="float: right; margin: 0px; padding: 2px;">
<a href="/sites/dot.kde.org/files/ksudoku_small_0.png">
<img src="/sites/dot.kde.org/files/ksudoku_small_0.png" width="400" height="271" />
</a>
<figcaption style="word-wrap: normal"><center>KSudoku with a character puzzle</center></figcaption>
</figure>
 
<h3>Dolphin, Edu and Games</h3>

Dolphin was ported to KDE Frameworks 5. Marble now has improved <a href="https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system">UTM</a> support as well as better support for annotations, editing and <a href="https://en.wikipedia.org/wiki/Keyhole_Markup_Language">KML</a> overlays.

Ark has had numerous commits including many small fixes. Kstars commits include improving the flat Analog to Digital Unit (ADU) algorithm and checking for out of bound values, saving Meridian Flip, Guide Deviation, and <a href="http://www.mainsequencesoftware.com/Content/SGPHelp/SequenceGeneratorPro.html?AutoFocus.html">Autofocus HFR</a> limit in the sequence file, and adding telescope rate and unpark support. KSudoku just got better, with commits that include: add GUI and engine for entering in Mathdoku and Killer Sudoku puzzles, and add a new solver based on Donald Knuth's Dancing Links (DLX) algorithm.

<h3>Other Releases</h3>

This release continues the new style of releases, introduced with the latest <a href="https://dot.kde.org/2014/12/17/kde-applications-1412-new-features-frameworks-ports">KDE Applications 14.12 release</a>. Along with this release, KDE Workspaces (aka Plasma 4) will be published for the last time in its Long Term Support version (version 4.11.22).

More than half of the KDE Applications have now been ported to <a href="https://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a> (KF5). The complete list of the Applications is available on the <a href=" https://community.kde.org/Applications/15.08_Release_Notes">KDE Applications 15.08 release notes</a>.

See the <a href="https://www.kde.org/announcements/fulllog_applications-15.08.0.php">full list of changes</a> in KDE Applications 15.08. In the next three months, there will be smaller bugfix releases for the KDE Applications, and then a next bigger release in <a href="https://techbase.kde.org/Schedules/Applications/15.12_Release_Schedule">December</a>.

<figure style="width: 250px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /><figcaption align="center">KDE App Dragons</figcaption></figure>

<h2>Spread the Word</h2>

Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets, KDE depends on people like you talking with other people! Even for those who are not software developers, there are many ways to support our community and our products. Report bugs. Encourage others to join the KDE Community. Or <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5">support the non-profit organization behind the KDE Community</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like Delicious, Digg, Reddit and Twitter. Upload screenshots of your new set-up to services like Facebook, Flickr and Picasa and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 15.08 KDE Applications release.
Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from Twitter, YouTube, flickr, PicasaWeb, blogs, and other social networking sites.