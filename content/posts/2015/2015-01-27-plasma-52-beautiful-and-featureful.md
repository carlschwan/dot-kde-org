---
title: "Plasma 5.2 Is Beautiful and Featureful"
date:    2015-01-27
authors:
  - "jriddell"
slug:    plasma-52-beautiful-and-featureful
comments:
  - subject: "Looks good"
    date: 2015-01-28
    body: "I always thought KDE looked too plastic-y to me, but really this looks great. With KDE 5 or Plasma 5 (or whatever is the right term) decoupling the KDE libs from the Qt stuff and all of that, assuming the core architecture is solid (let's face it, we all like pretty looks, but they only go so far) then I think KDE has a bright future."
    author: "Eric"
  - subject: "Panel Bug"
    date: 2015-01-29
    body: "I've used KDE 5 a few times and it had a panel bug that KDE 3 and 4 did not have. When I set the panel to \"allow windows to cover panel\" in KDE 3 and 4 it would do so, and the panel would reveal when I moused to it. It doesn't do that in KDE 5. Windows just cover the panel and it can't be revealed when covered like it used to be. I submitted a bug report for this and the bug report got closed because I wasn't currently using KDE when soemone asked me what Plasma version I have. It's a bug that affected (probably still does) KDE 5 in general. What difference does it make if I'm currently using it or not? The bug report should have remained open. Oh, I know. I'll try it again soon and see for myself. Or if anyone here can tell me, has this bug been fixed yet?"
    author: "Bubba"
  - subject: "slick and modern inside and outside"
    date: 2015-01-30
    body: "This looks beautiful and modern and definitely feels like a step forward in the right direction. I do not think competing Linux desktop are anywhere close to KDE.\r\n\r\n"
    author: "Maxim Egorushkin"
  - subject: "KDE5 looks ugly to me"
    date: 2016-01-07
    body: "<p>Why KDE5 looks like the ugly Windows 10 and Android 5 ? All too simple for me. Everything too straight, too few colors, too simple, too flat.</p><p>KDE4 looks much better to me. But maybe I'm too old...</p>"
    author: "efgee"
---
<p>
Today KDE releases Plasma 5.2.  This release adds a number of new components, many new features and many more bugfixes.
</p>

<div style="text-align: center">
<a href="https://www.kde.org/announcements/plasma-5.2/full-screen.png">
<img src="https://www.kde.org/announcements/plasma-5.2/full-screen-wee.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="800" height="451" alt="Plasma 5.2" />
</a>
</div>
<!--break-->
<h2>New Components</h2>

<div style="float: right; margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center ">
<a href="https://www.kde.org/announcements/plasma-5.2/kscreen.png">
<img style="border: 0px;" src="https://www.kde.org/announcements/plasma-5.2/kscreen-wee.png" width="400" height="394" alt="Dual monitor setup" />
</a><br />
KScreen dual monitor setup</div>

<p>This release of Plasma comes with some new components to make your desktop even more complete:</p>

<p><strong>BlueDevil</strong>: a range of desktop components to manage Bluetooth devices.  It'll set up your mouse, keyboard, send &amp; receive files and you can browse for devices.</p>

<p><strong>KSSHAskPass</strong>: if you access computers with ssh keys but those keys have passwords this module will give you a graphical UI to enter those passwords.</p>

<p><strong>Muon</strong>: install and manage software and other addons for your computer.</p>

<p><strong>Login theme configuration (SDDM)</strong>: SDDM is now the login manager of choice for Plasma and this new System Settings module allows you to configure the theme.</p>

<p><strong>KScreen</strong>: getting its first release for Plasma 5 is the System Settings module to set up multiple monitor support.</p>

<p><strong>GTK Application Style</strong>: this new module lets you configure themeing of applications from Gnome.</p>

<p><strong>KDecoration</strong>: this new library makes it easier and
more reliable to make themes for KWin, Plasma's window manager. It has
impressive memory, performance and stability improvements. If you are
missing a feature don't worry it'll be back in Plasma 5.3.</p>

<h2>Other highlights</h2>

<p><strong>Undo</strong> changes to Plasma desktop layout</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="https://www.kde.org/announcements/plasma-5.2/output.gif" width="608" height="320" alt="Undo desktop changes" /><br />
Undo changes to desktop layout</div>

<p>Smarter sorting of results in <strong>KRunner</strong>, press Alt-space to easily search through your computer</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="https://www.kde.org/announcements/plasma-5.2/krunner.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="744" height="182" alt="Smart sorting in KRunner" /><br />
Smart sorting in KRunner</div>

<p><strong>Breeze window decoration</strong> theme adds a new look to your desktop and is now used by default</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="https://www.kde.org/announcements/plasma-5.2/window_decoration.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="720" height="56" alt="New Breeze Window Decoration" /><br />
New Breeze Window Decoration</div>

<p>The artists in the visual design group have been hard at work on many new <strong>Breeze icons</strong></p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="https://www.kde.org/announcements/plasma-5.2/icons.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="552" height="337" alt="New Breeze Window Decoration" /><br />
More Breeze Icons</div>

<p>They have added a new white mouse <strong>cursor theme</strong> for Breeze.</p>

<p><strong>New plasma widgets</strong>: 15 puzzle, web browser, show desktop</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px;" src="https://www.kde.org/announcements/plasma-5.2/new_plasmoid.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="563" height="316" alt="Web browser plasmoid" /><br />
Web browser plasmoid</div>

<p><strong>Audio Player controls</strong> in KRunner, press Alt-Space and type next to change music track</p>

<p>The Kicker alternative application menu can install applications from the menu and adds menu editing features.</p>

<p>Our desktop search feature Baloo sees optimisations on startup. It now consumes 2-3x less CPU on startup.  The query parser supports "type" / "kind" properties, so you can type "kind:Audio" in krunner to filter out Audio results.</p>

<p>In the screen locker we improved the integration with logind to ensure the screen is properly locked before suspend.  The background of the lock screen can be configured.  Internally this uses part of the Wayland protocol which is the future of the Linux desktop.</p>

<p>There are improvements in the handling of multiple monitors. The detection code for multiple monitors got ported to use the XRandR extension directly and multiple bugs related to it were fixed.</p>

<p><strong>Default applications in Kickoff</strong> panel menu have been updated to list Instant Messaging, Kontact and Kate.</p>

<p>There is a welcome return to the touchpad enable/disable feature for laptop keypads with these keys.</p>

<p>Breeze will <strong>set up GTK themes</strong> on first login to match.</p>

<p>Over 300 bugs fixed throughout Plasma modules.</p>

<p><a href="https://www.kde.org/announcements/plasma-5.1.2-5.2.0-changelog.php">Plasma modules 5.2 full changelog</a></p>

<!-- // Boilerplate again -->

<h2>Live Images</h2>

<p>
The easiest way to try it out is the with a live image booted off a USB disk.  Images which use Plasma 5.2 beta are available for development versions of <a href='http://cdimage.ubuntu.com/kubuntu/releases/vivid/alpha-2/'>Kubuntu Vivid Beta</a> and <a href='http://www.dvratil.cz/2015/01/plasma-5-2-beta-available-for-fedora-testers/'>Fedora 21 remix</a>.  We expect Plasma 5 to be picked up as the default desktop in leading distributions in the coming months.
</p>

<h2>Package Downloads</h2>

<p>Distributions have created, or are in the process of creating, packages listed on our wiki page.
</p>

<ul>
<li> <a href='https://community.kde.org/Plasma/Packages'>Package download wiki page</a></li>
</ul>

<h2>Source Downloads</h2>

<p>You can install Plasma 5 directly from source. KDE's community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>. Note that Plasma 5 does not co-install with Plasma 4, you will need to uninstall older versions or install into a separate prefix.
</p>

<ul>
<li>

<a href='https://www.kde.org/info/plasma-5.2.0.php'>Source Info Page</a>
</li>
</ul>

<h2>Feedback</h2>

You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a> or <a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a> or <a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.
<p>
Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.</a>
</p>

<p>You can provide feedback direct to the developers via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>.  If you like what the team is doing, please let them know!</p>

<p>Your feedback is greatly appreciated.</p>

<h2>
  Supporting KDE</h2>

<p align="justify">
 KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>
