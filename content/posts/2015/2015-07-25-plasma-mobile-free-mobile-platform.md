---
title: "Plasma Mobile, a Free Mobile Platform"
date:    2015-07-25
authors:
  - "sebas"
slug:    plasma-mobile-free-mobile-platform
comments:
  - subject: "Why not the KDE Community Forums?"
    date: 2015-07-25
    body: "(einar here from the KDE Forums staff)\r\n\r\nIs there a specific reason why a non-KDE forum was made? As far as I can tell, plasma-mobile is software by KDE and developed inside KDE. If the forums had some deficiences, at least the forum staff could have been contacted (we didn't get any mail). Notice that there's no prejudice about having forums hosted separately - as long as this is done within reason or at least communicated properly."
    author: "einar"
  - subject: "OEM support?"
    date: 2015-07-25
    body: "This is absolutely fantastic news! Congratulations to all of those involved. Is the support for Android apps going to be provided through Shashlik? Also, is the goal to ultimately have support from OEMs and ship a device that runs Plasma Mobile out of the box?"
    author: "Yahn"
  - subject: "KDE fanboy with Jolla Sailfish smartphone is crying"
    date: 2015-07-25
    body: "It's a pity for me that will going to get two opposite mobile OSes based on Qt Framework."
    author: "a_andreyev"
  - subject: "Two opposing? Ubuntu phone"
    date: 2015-07-25
    body: "Two opposing? Ubuntu phone and Blackberry are also using Qt."
    author: "Allan"
  - subject: "How will Android app support"
    date: 2015-07-25
    body: "How will Android app support work? Will it be open-source? Could it be used to run Android apps on desktop Linux as well?"
    author: "Anonymous"
  - subject: "not a pity per se"
    date: 2015-07-25
    body: "I'd say this isn't a reason to cry but the opposite: Qt apps written for one of them will probably work on the other with minimal changes and I guess the Plasma Mobile shell can be made to work on sailfish OS, too, so this might just helps to create a greater Qt ecosystem on mobile devices. Also some people might be concerned with the proprietary code in Jolla's shell so a free alternative is a good thing. "
    author: "kiwii"
  - subject: "It's just not done yet, but"
    date: 2015-07-25
    body: "It's just not done yet, but we do want to move to the KDE forums. Let's talk offline how this is best done, perhaps we can even move the data over (there's quite some design mockups and useful things)."
    author: "sebas"
  - subject: "It will be open source, yes."
    date: 2015-07-25
    body: "It will be open source, yes. We also want to make this available for the desktop."
    author: "sebas"
  - subject: "Yes, OEM support would be"
    date: 2015-07-25
    body: "Yes, OEM support would be great, and it's definitely something we're aiming for. That said, the current status is too early to talk to OEMs with a straight face, we first need to achieve a level of quality that makes it suitable for end users."
    author: "sebas"
  - subject: "\"General\" build instructions for miscellaneous devices?"
    date: 2015-07-25
    body: "If you can put together instructions describing how to build and install this on formerly-Android devices in general (e.g. an unlocked Nexus 7 tablet, for example) rather than only being able to build for a handful of specific devices, you'll be way ahead of FirefoxOS immediately.\r\n\r\n(I look forward to being able to try out Plasma Mobile, but I can't afford to buy a special new device to do it on. Even if I have to fight with a custom build environment, I'd still happily try to build and install it on one or more of my older devices that I have floating around here. I would have tried FirefoxOS the same way by now, but I can't.)"
    author: "Epicanis"
  - subject: "forum-admin at kde org is the"
    date: 2015-07-26
    body: "forum-admin at kde org is the address you want to contact. "
    author: "einar"
  - subject: "More device support for more contributors"
    date: 2015-07-26
    body: "Nice to see that you are not abandonning your device-spectrum strategy. With Plasma Active being pretty much dead, I was wondering if you were not focussing only on Desktops and Media Centers. Contrats, Plasma Mobile looks already pretty awesome ! Having currently a Nokia N9, I won't be able to try it, but you can be sure that I will choose my next phone to be compatible with it !\r\n\r\nIf I have one advice to give, I think you should make sure that this is as easy as possible to test on an average Android phone. You can attract new contributors to KDE because any computer science student can run Plasma Desktop on his current laptop and start coding. On the contrary, Plasma Active got little traction because it was very difficult to find a tablet to run it on. I think most free-time contributors get their ideas on new stuff to develop or things to improve, by using software on a day-to-day basis, so this is crucial. I am sure there are a lot of potential contributors to a free software community lead mobile OS, but it needs to be easy to test it. I know that all the cards needed for this are not in your hands, but be sure to do all you can to ease this process. The Kubuntu team has a big role to play there.\r\n\r\nCongrats again, thanks for this nice surprise and good luck this it."
    author: "AGui"
  - subject: "phone numbers"
    date: 2015-07-26
    body: "Are you sure you want the full (private) mobile phone numbers of some people displayed for the world to see?"
    author: "Diederik"
  - subject: "Those are not the real"
    date: 2015-07-27
    body: "Those are not the real numbers."
    author: "sebas"
  - subject: "Other devices"
    date: 2015-07-28
    body: "I hope i can try too. I have old nexus 7 tablet. \r\nStill cannot afford to but Nexus 5 :)"
    author: "Reza"
  - subject: "QML based apps in Ubuntu"
    date: 2015-07-29
    body: "QML based apps in Ubuntu's world have a slight gotcha: Custom UI elements different from Qt's QtQuick.Controls. (Still searching for the reason)"
    author: "nadrimajstor"
  - subject: "Multirom"
    date: 2015-07-31
    body: "<p>Can I install Plasma-Mobile in dualboot with Multirom on my nexus 5, with a pre-compiled image...???</p>"
    author: "Skippythgekangoo"
  - subject: "Installation"
    date: 2016-03-19
    body: "<p>Just a thought, and bare in mind i'm not a developer and therefore have no idea how feasable this would be.</p><p>Mobiles all use pretty much the same hardware, with a few variances but no more than with PC's, why not make the install process simular to installing Kubuntu (or something) on a PC. &nbsp;i.e plug the phone into the pc, install software scans the phone, detects the hardware and download the appropiate drivers and installs (assuning drivers are available for that device of course). &nbsp;rather then waiting for a custom ROM to be built for my device.</p><p>I think if this could be pulled off you would have a far more rapid uptake of kde plasma mobile rather then trying to crack an already oversaturated OEM market, it would also make plasma mobile the only mobile OS you could actually do that too.</p><p>It would put you in a greater barging position with OEMS's if you can go to someone like Samsung for instance and say hey we alread have a million uses running our os on your devices.</p><p>&nbsp;</p><p>I know i'm hanging out for the day when I can buy what ever phone I want and install whatever OS I want (Just like a PC)</p>"
    author: "me60732"
  - subject: "I want the same too"
    date: 2016-06-18
    body: "<p>Android unlocking is too complicated. You have to do something about it. There is a lot of updates for the phones I have now I cant root it easily I havent founded the way yet. It would be great to go into download mode, it download KDE, delete the Android interface I hate, and I can have a true computer-like mobile phone. I want to show to the entire earth that live without android or Makro$oft is possible. I love open-source and Linux. -see what I^ve got. I can do everything. Launch anything. Please make it easy to instal. Samsung Galaxy note is not easily rootable from Linux and I will never use again Makro$oft crap.</p>"
    author: "Boki Ball"
  - subject: "i agree"
    date: 2016-07-16
    body: "<p>i have a couple devices as well, i would love to replace android with linux totally, unfortunately i cant understand how to root, flash, etc</p><p>on this stuff. i have an HTC desire phone, and an LG tablet , i want to run ubuntu on . wish there was an easy way to replace android</p><p>completely&nbsp;</p>"
    author: "redne"
  - subject: "You do know "
    date: 2018-01-11
    body: "<p>Android is built %100 for all intents and purpourses . Linux, you do know that right???</p>"
    author: "Just for the Notch"
  - subject: "Any update on this project ?"
    date: 2018-11-20
    body: "<p>Hello,</p><p>I do not wish to put any pressur on this project but would like instead to have the latest development status on it we coul becom anctious about it future release.</p><p>&nbsp;</p><p>Thank you.</p>"
    author: "Martin Gagnon"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; text-align: right;">
    <img src="/sites/dot.kde.org/files/phone-still-small.jpg">
</div>
<p>Plasma Mobile offers a Free (as in freedom and beer), user-friendly, privacy-enabling, customizable platform for mobile devices. Plasma Mobile is Free software, and is now developed via an open process. Plasma Mobile is currently under development with a prototype available providing basic functions to run on a smartphone.
</p>
<p>
Plasma Mobile offers...</p>
<ul>
    <li><strong>Freedom.</strong> Plasma Mobile is Free and Open Source software. It can be acquired free of charge, with the power and licensed rights to change it in any way, to redistribute it and to understand how it works.</li>

    <li><strong>User-friendliness.</strong> Plasma Mobile is designed via an open process, making sure that the requirements and wishes of users are heard and implemented in the best possible way. Ergonomy and integration across devices on top of a high quality software stack provides a stable, rich and reliable system that helps users get things done efficiently and effectively.</li>

    <li><strong>Privacy.</strong> Plasma Mobile integrates with services trusted by the user. Instead of depending on claims from hardware or operating system vendors, trust is based on software that has been audited in an open development process, Free and Open Source software that can be combined with services from trusted sources, including those of one's own.</li>

    <li><strong>Customization and personalization.</strong> Plasma Mobile has been built with modularity from the ground up. From the wallpaper and the Look and Feel to lower-level system components, almost every aspect of the system can be customized.</li>
</ul>

<div align="center">
<iframe width="640" height="360" src="https://www.youtube.com/embed/auuQA0Q8qpM" frameborder="0" allowfullscreen></iframe>
</div>
<!--break-->

<h2>Enabling the community</h2>

<p>
The goal for Plasma Mobile is to give the user full use of the device. It is designed as an inclusive system, intended to support all kinds of apps. Native apps are developed using Qt; it will also support apps written in GTK, Android apps, Ubuntu apps, and many others, if the license allows and the app can be made to work at a technical level.
</p>
<p>
Plasma Mobile's development process welcomes contributions at all levels. If you want to get your hands dirty with a cool app, if you want to provide a system functionality such as a mobile hotspot, if you want to improve power management at the kernel level, if you want to help with the design, Plasma Mobile welcomes your contributions.
</p>
<p>
If you want to take part in the creation of Plasma Mobile, get in touch with us!
</p>
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px; text-align: right;">
    <img src="/sites/dot.kde.org/files/plasma-mobile-logo.png">
</div>

<h2>A system you can trust</h2>

<p>
Most offerings on mobile devices lack openness and trust. In a world of <a href="https://en.wikipedia.org/wiki/Closed_platform">walled gardens</a>, Plasma Mobile is intended to be a platform that respects and protects user privacy. It provides a fully open base that others can help develop and use for themselves, or in their products.
</p>
<p>
As a Free software community, it is our mission to give users the option of retaining full control over their data. The choice for a mobile operating system should not be a choice between missing functions or forsaken privacy of user data and personal information. Plasma Mobile offers the ability to choose the services that are allowed to integrate deeply into the system. It will not share any data unless that is explicitly requested.
</p>

<h2>Prototype available now</h2>

<p>
Plasma Mobile is available as a developer prototype running on an LG Nexus 5 smartphone. It can make and receive phone calls. It provides a workspace to manage the system, and a task switcher to control and navigate apps on the device. There are also x86 builds, suitable for an ExoPC, for example, which can be useful for testing. Several apps have been included—both native and 3rd party—in the device images to allow the system to be tested and improved.
</p><p>
<a href="https://community.kde.org/Plasma/Mobile">Find out</a> how you can have a look of your own!</p>

<h2>Where can I find...</h2>

<p>
More info, such as installation instructions, are available in the <a href="http://community.kde.org/Plasma/Mobile">Plasma Mobile</a> wiki, on the <a href="http://www.plasma-mobile.org">Plasma Mobile website</a> and on <a href="http://vizZzion.org/blog/2015/07/embracing-mobile">sebas' weblog</a>. The code for various Plasma Mobile components can be found on <a href="http://community.kde.org/Plasma/Mobile/Code">git.kde.org</a>.
</p>
<p>
Ask questions in the <a href="http://forums.plasma-mobile.org">Plasma Mobile</a> forum, or send an email to the <a href="https://mail.kde.org/mailman/listinfo/plasma-devel">plasma-devel</a> mailing list, or (for private inquiries) to Sebastian Kügler (sebas@kde.org).
</p>
