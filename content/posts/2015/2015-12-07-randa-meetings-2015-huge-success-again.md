---
title: "Randa Meetings 2015 - Huge Success Again"
date:    2015-12-07
authors:
  - "unormal"
slug:    randa-meetings-2015-huge-success-again
comments:
  - subject: "Glad to read up on the"
    date: 2015-12-07
    body: "<p>Glad to read up on the progress. I hope Digikam 5.0 starts off as a stable port. A lot was invested in 4.x to make it stable. Also good to read about clean-ups in the KIPI Plugins.</p><p>Thanks to all of you for making Digikam.</p>"
    author: "Ritesh Raj Sarraf"
  - subject: "You guys rock!"
    date: 2015-12-07
    body: "<p>Hey people of KDE,</p><p>This is just a big shout out to you guys and your wonderful work.</p><p>The new Breeze theme and icons just look awesomazing, the overall very consistent theming creates a very professional look and feel - it\u00b4s such a tremendous feeling to work with a beautiful desktop and an overall consistent experience. I\u00b4m an art teacher and I work with a Cintiq Companion 2 - so for me it\u00b4s great news, that you guys are focusing on touch integration (besides an already super desktop experience) and I really hope, that you will integrate graphic tablets as input devices in the system settings and maybe even throw in an onscreen keyboard (especially&nbsp;for the login screen). I would love it so much to go Linux/ KDE full time, but these little missing features force me to use Windows way more than I like to.</p><p>KRunner, DigiKam, Dolphin, Krita, Calligra, Kdenlive and many more - &nbsp;it is simply amazing what you guys brought to the table of the open-source-community.</p><p>Thank you very much for your hard work and rock on!</p><p>Greetings&nbsp;</p><p>David</p>"
    author: "david_b"
---
The Randa Meetings 2015 came to a successful end a while ago, so it is time to look back, see what we achieved and give you a little summary of the events that took place in the Swiss Alps this year. As usual we had quite a collection of meetings in one big house, which is why the event is called Randa Meetings, in plural. We might have already explained that last year, but it seems to be something that doesn't stick yet!

The meetings altogether were a big success, thus we would like to thank all the supporters, sponsors and other people and groups who made the event possible. As we pointed out during the <a href="http://www.kde.org/fundraisers/kdesprints2015">fundraiser</a>: It's because of these people, hopefully including you, that people from all across the globe were able to get a lot of work done and make the software you love even better. The participants did not only fly to Switzerland from all across the globe, they also were quite a mixed bunch regarding age, background and projects they are working on. So if you like the work we are doing and describe below don't hesitate to <a href="http://www.kde.org/community/donations/index.php">support us and donate</a>.

This led to this year's lovely group picture: 

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/GroupictureRandaMeetings2015.jpg">
<strong>All participants (including staff) at the Randa Meetings 2015</strong></div><br />


<h2>First day - Day of arrival</h2>

Every meeting tends to start with the arrival of the participants, as did the Randa Meetings. Most people arrived by airplane, so either at the Geneva or Zurich airport, both of which are basically at the other ends of Switzerland. But fortunately Switzerland is a small country, so taking the train to Randa only took a couple of hours. Friendly helpers were at the airports to greet people and hand them out a day ticket, which allowed them to use most of the public transports in Switzerland without additional fees. The journey up to Randa is quite an interesting one, as from Visp on it is a rather steep trail up the mountains. As the region is also frequently visited by tourists, the trains have been built with that in mind, so people were able to enjoy a scenic panorama view.

After arrival people were shown their rooms so they could place their luggage and, if needed, recover a bit from the journey. As the arrivals were spread out over the whole day, not a lot of work and collaboration happened yet. It was mostly about meeting good old friends, new friends and putting faces to nicknames. And of course most people needed some new energy and thus food and drinks after there long journeys.

<h2>Second day - Monday </h2>

On the second day most of the participants have arrived and had enough sleep so they were ready for some work. However, most of the second day was spent on getting to know each other, talking with and getting to know the various different groups:

<ul>
  <li>digiKam - professional photo management</li>
  <li>KDE Connect - Connect with your Android devices</li>
  <li>Multimedia - including Amarok and Kdenlive</li>
  <li>PIM - The Kontact Suite</li>
  <li>QMLweb - QML in the web browser</li>
  <li>And the biggest group: Touch&Mobile - to bring Touch to KDE</li>
</ul>

And, in the afternoon, listening to presentations from various people.  The videos of these presentations are <a href="https://files.kde.org/randa">now available</a>.

During that time we also had our first local guests: a teacher and his students from a local vocational school visited us to get an overview on Free and Open Source Software, the KDE community and on what they will be doing in the Swiss mountains during the week. 

<h2>Tuesday to Friday - Work of various groups</h2>

All the above mentioned groups worked on different ideas and projects but in the end the big topic and motto of 2015 was "Bring Touch to KDE". And although most of the participants of the Randa Meetings started their work even before the meetings, thought about the topics during their journeys and started with their work as soon as they met the first other participants on their paths to Randa the main working time of the KDE Tech Summit was the time from Tuesday to Friday.

<h2>digiKam</h2>

Gilles Caulier worked mostly on the KIPI-plugins port to Qt5 and reduce dependencies where possible to improve portability and reduce complicated code. He has backported MetadataEdit and GPYSync tools to digiKam core. Marcel Wiesweg concentrated on isolating and fixing a few performance-critical and/or critically annoying bugs that had shown up in the KF5-port. A large group of these turned out to be due to Qt's filter model dynamicSortFilter property being turned on per default in Qt5 (off in Qt4). With some more optimizations in the tag filter code, all models and views seemed to be performing very well again. Another big cleanup was needed for the (tag) completion code, with Qt5 providing the QCompleter API and the old kdelibs API gone.

Shourya Singh Gupta worked on KIPI-plugins as well. He mostly worked towards refactoring remaining plugins using common classes created during GSoC 2015 and completing the KF5/Qt5 porting of KIPI-plugins. He also fixed dead plugins like Image Shack, removed dead UI components from plugins, made a new common KPLoginDialog class to support factorization of login dialog of plugins and factorized Yandex Fotki's and Image Shack's login dialog using KPLoginDialog.

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/digiKamTeamAtWorkLateAtNight.jpg">
<strong>The digiKam Team At Work Late At Night</strong></div><br />

Veaceslav Munteanu worked on further improvements for metadatahub. Now the metadatahub user interface can support more than tags, comments and rating. Adding a new category can be done very easily by setting default values and zero changes in user interface. Saving and retrieving namespace data from config was also simplified. Except metadatahub improvements, private linking to KIPI-plugins was added, and hardcoded icon paths were removed from the digiKam source code.

Alexander Potashev made the last steps in a long way of porting KIPI-plugins to pure Qt5/KF5. As the result, all the plugins to be released with kipi-plugins-5.0.0 are now free of the kdelibs4support dependency. During the rest of the time he was thinking over and discussing possibilities to switch KIPI-plugins to a more generic interface to make them more usable to a wider range of applications, such as for example Kamoso, KMail and Blogilo. But this work is still yet to be done. And together the united digiKam team worked on the next big 5.0.0 version anticipated for <a href="https://www.digikam.org/about/releaseplan">April 2016</a>.

<h2>KDE Connect</h2>

As the software that <a href="https://community.kde.org/KDEConnect">connects your KDE Plasma Desktop</a> with your Android devices (and in the future possibly other mobile operating systems) the KDE Connect team worked on their first KF5-based release as well. During the Meetings in Randa they achieved a new Android Material design and reviewed their new security infrastructure based on SSL and encryption.

Another great integration work was the mating of KDE Connect with KDE Telepathy and thus enable to answer your SMS or texting on your desktop. Please take a look and test the <a href="http://download.kde.org/unstable/kdeconnect/0.9/src/kdeconnect-kde-0.9.tar.xz.mirrorlist">first new release</a>.

<h2>Multimedia</h2>

On the multimedia side we had the yearly sprint on Amarok bug triaging and Myriam pushing out a beta release of the current state and last Qt4-based version. There was some work as well on porting Amarok to Qt5 and KF5 or at least some coordination work. Please get in contact if you can lend a helping hand to finish this port.

The Kdenlive team - our great non-linear video editor - began with a bug fix day, just before the Applications 15.08.1 tagging, so as to get a release as solid as possible. Then they oriented on more social topics, as people in great position to help them were present. The Visual Design Group (VDG) people helped them to define their vision statement, and then lead a UI usability review, with a video editor new to Kdenlive.

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/KdenliveUiDiscussionGComprisTesting.jpg">
<strong>Kdenlive In A UI Discussion (and some visiting kids testing GCompris)</strong></div><br />

Experienced Qt hackers helped them to investigate on an old complex bug; packagers introduced them to the way of properly preparing binaries. The Kdenlive team mentioned as well that they were interested in the sprint organization BoF (we hope to blog about this soon), in order to have small gatherings more regularly.

During the last days Vincent and Jean-Baptiste had the pleasure to break things and add new features:

<ul>
  <li>Saving only parts of the timeline, as a preparation to cross project copy and paste or sub-sequences creation</li>
  <li>Automatic handling of transparent clips</li>
  <li>Reflections on crossfades on same track</li>
</ul>

All in all the Kdenlive team dedicated their time to intensive work and could get a lot of motivation, and several of the ideas that came out of it are currently being implemented or are being prepared for the next 15.12 release "that will be awesome thanks to Randa!"    

<h2>PIM - Personal Information Management</h2>

The PIM group used the Randa Meetings for what they are best at: real-time face-to-face discussions. The first few days was really mostly spending on talking and finding a common ground to build on. Towards the middle of the week this base was found and they could start discuss, plan and design new aspects of Akonadi Next and Kontact Quick.

<h2>QMLweb</h2>

The QMLweb team found their way to Randa for the second or even third time but was never as big (although with 4 people still relatively small) as this year. And they worked on a big restructuring too. You can listen to more about it in this <a href="http://files.kde.org/randa/QMLweb.mp4">introduction video</a> from Monday. Towards the end of the Meetings they start to implement their ideas and plans so we can curiously wait for blog posts about their progress for this time after Randa.

<h2>Touch&amp;Mobile - Bring Touch to KDE</h2>

By far the biggest and still most heterogeneous group work on our main topic: to bring touch capabilities to more KDE software.

Of general help for all the groups was the Visual Design Group (VDG) with their expertise in design, usability and user experiences. The icon guys Andreas and Uri talk with different projects about their needs of new and better icons and provided much help and insight in the process of icon creation. Heiko and Jens were part of several discussions regarding user interfaces design and helped different group to improve their first impression to new users.

Scarlett and her team work hard on a revamp of our powerful and badly needed <a href="https://build.kde.org">continuous integration (CI) system</a>. This revamp is necessary to bring the CI system and thus our software to major platforms as MS Windows and Apples Mac OS X. And another goal with at least as much importance is the coverage of Android and maybe in the future even Windows Phone or iOS. Emmanuel helped a lot to start the work on Docker integration.

Several of the KDE Edu projects were part of these meetings too and you can witness a lot of porting work and thus improvements for future mobile or touch friendly versions of this software. And particular group was the Marble hackers that worked on a vector-based display of the OpenStreetMap data. With a lot of success and even 3D views as can been seen in <a href="http://nienhueser.de/blog/?p=628">Dennis blog post "Vector Tiling in Marble Maps @Randa"</a>. Marble Maps is a real KDE Android application and just some days or weeks later Torsten announced that they new ship <a href="https://blogs.kde.org/2015/10/23/announcement-marble-ships-oldest-existent-historic-globe">the oldest existent historic Globe</a> (on Android too). 

Other educational application that were ported are <a href="https://edu.kde.org/applications/science/kalzium/">Kalzium - the Periodic Table of Elements</a> and <a href="https://edu.kde.org/applications/all/kturtle">KTurtle - an Educational Programming Environment</a>.

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/GComprisLiveUserTesting.jpg">
<strong>GCompris - Live User Testing</strong></div><br />

Bruno of <a href="http://gcompris.net/">GCompris</a> fame mentioned: <i>GCompris had a nice and productive Randa Meetings this year. We completed two new activities, one named 'melody' where the children must memorize and reproduce a suite of notes played on a xylophone. The second one created by Holger is based on the physics engine Box2D and uses the mobile sensors to let the children move a ball by moving the tablet. This is a small step towards the completion of the port of GCompris. So far we ported 116 activities on the 140 of the GTK+ version. If this year is as productive as we have been for now, we will be in time to make a 'port complete' party for Randa next year."</i> Additionally the GCompris team told us a bit about their <a href="http://files.kde.org/randa/GCompris.mp4">experience of working with and porting to iOS</a>.

The application that probably got most work on the UI during the time in Randa was <a href="https://edu.kde.org/applications/all/artikulate">Artikulate - the Pronunciation Trainer</a>. Andreas worked hard and in cooperation with different people and teams to reach his highly set goals.

Dolphin gained <a href="http://www.owncloud.org">ownCloud</a> integration. Using KDE Applications 15.12 and ownCloud client 2.1 you will be able to see overlay icons within Dolphin and have an action to share files from the desktop.

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/TouchAndMobileGroupWorkingRightUnderTheRoof.jpg">
<strongTouch&Mobile Group Working Right Under The Roof</strong></div><br />

Another and by far not the least important project that was worked on in Randa is an easily available <a href="https://community.kde.org/Android">Android Build Environment</a> to work on new KDE Android applications and porting of old KDE applications. The group wrote good documentation and prepared a Docker image to allow people to setup their build-environment with one command. They even started with testing and work on KDE Frameworks and their availability on Android.

And last but not least the Calligra team with Friedrich and Leinir work on further porting and integration of the Gemini infrastructure.

<h2>Thursday - Trip to Zermatt and Raclette </h2>

On Thursday two special events took place. In the afternoon a trip to Zermatt, a rather famous Swiss mountain town only a couple of kilometers from Randa, was organized. A couple of local taxi buses drove people up to Zermatt, where Mario showed them around and people were able to buy various souvenirs and swiss chocolate. Afterwards a hike back to Randa was planned, roughly 10 kilometers down through forests and over meadows, following the river Vispa back to the house.

This trip was optional, so a couple of people decided to rather stay in Randa and get more work done. Also for people not used to hiking or people who didn't bring decent shoes along it might have been a bit harsh, but in the end everybody enjoyed the trip and quite a lot of pictures were taken. And it was another good opportunity to discuss KDE topics cross-groups and with and in some fresh air. Every other day a smaller walk around Randa was organized or the different groups searched a path themselves.

<div style="text-align:center; margin: 4ex;">
<img src="/sites/dot.kde.org/files/InLineForRaclette.jpg">
<strong>Standing In Line For The Raclette</strong></div><br />

In the evening the second event took place: the traditional Raclette dinner with our sponsors. Raclette, a traditional swiss meal made out of molten cheese, served with potatoes, gherkins and usually wine, was rather well received by both the participants and also our sponsors. As last year, people, groups and companies that helped with monetary or hardware donations received an invitation to spend an evening with us, eating said Raclette, having good local wine and getting to know the event they made possible. It was a lovely ending to a great and rather eventful day.

<h2>Saturday and Sunday - Pack your luggage</h2>

For most of the participants Saturday was the day of packing the luggage, preparing for departure and thus saying good-bye which was not easy after a very productive and creative week with good old and new friends - the KDE family.

Sunday was then definitely the day for everybody to leave as we needed to hand over the house at 11 am. So with half of the participants already gone and the other half helping to clean and clear the house the 6th edition of the Randa Meetings ended.

<h2>The future - See you again next year?</h2>

It was another very good and exhausting week in Randa. A lot was done, discussed and achieved but people were tired and headed to a more or less long way home. Organizational and administrative wise there is still work left to close all up and then we can start to think about another edition of the KDE Tech Summit called "Randa Meetings". Will you be part of it, will you help again, will you read about it?...

If you would like to read even more about the Randa Meetings 2015 and also get some more personal views, a <a href="https://community.kde.org/Sprints/Randa/2015">list of personal blog posts</a> is available on our community wiki. Thanks to Gilles Caulier for the <a href="https://www.flickr.com/photos/digikam/sets/72157658453239225">nice pictures</a>. And for everybody that read the whole text here is a little surprise: you might add yourself to the <a href="http://doodle.com/poll/diri6d9iek52tr2f">date selection for the Randa Meetings 2016</a> as one of the first.
