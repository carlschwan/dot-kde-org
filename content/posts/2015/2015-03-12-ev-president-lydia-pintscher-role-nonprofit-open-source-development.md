---
title: "e.V. President Lydia Pintscher on the Role of a Nonprofit in Open Source Development"
date:    2015-03-12
authors:
  - "jriddell"
slug:    ev-president-lydia-pintscher-role-nonprofit-open-source-development
---
<img src="/sites/dot.kde.org/files/Lydia-Pintscher-KDE-president.jpg" width="275" height="187" align="right" />

Linux.com interviews <a href="http://www.linux.com/news/software/applications/815637-kde-president-lydia-pintscher-on-the-role-of-a-nonprofit-in-open-source-development">KDE e.V. president Lydia Pintscher</a>.  She talks about what KDE's legal body does and why it is important for open source communities to have a charity to represent them.  She also discusses the difference between company and community supported projects and the status of women in open source.
<!--break-->