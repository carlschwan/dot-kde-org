---
title: "Kontact Kolab Now"
date:    2015-02-27
authors:
  - "kallecarl"
slug:    kontact-kolab-now
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/envelopeLock.png" /></div>
<a href="https://kolabsystems.com/">Kolab Systems</a> has recently announced substantial improvements to their support for <a href="http://kolab.org/overview">Kolab Groupware</a>, today's <a href="http://kolab.org/blog/vanmeeuwen/2015/02/27/wifes-birthday-kolab-3.4-release-day">release of Kolab 3.4</a>, recently released <a href="https://kolabenterprise.com">Kolab Enterprise 14</a> and the upgrade of the hosted Kolab Now solution for Enterprise 14. Since the start of Kolab in 2002 as a Free Software project sponsored by the German Federal Office for Information Security, KDE people have played a significant role in its development. Kolab is enterprise-level software that includes group email, calendaring, contacts, file sharing, and task management. Its features also appeal to many individuals.
<!--break-->

<h2>A little history</h2>
The initial release of Kolab was called Kroupware (notice the "K", it means something ;^) The Kroupware Client was the result of enhancements to KMail and other KDE PIM (personal information management) software, and later became known as Kontact. About 10 years ago, KDE developers were interviewed about <a href="https://dot.kde.org/2005/01/28/big-kolab-kontact-interview-part-i">the evolution of the projects</a> and provided details about <a href="https://dot.kde.org/2005/02/09/big-kolab-kontact-interview-part-ii">Kontact</a>, the Personal Information Management Suite from KDE. Currently Kontact is the favored client for Kolab; other popular clients are supported as well. Despite its longevity, <a href="https://dot.kde.org/2012/03/29/kde-pim-needs-you">KDE PIM continues to evolve</a>. 

<h2>Email still going strong</h2>
<blockquote>Despite the rise of social networks and messaging apps, email continues to be the dominant mode of written electronic communication. Over the next few years, email use will continue to grow in the business world and decrease by less than 4% each year for consumers. The average business worker will have to deal with 140 emails a day by 2018, up from 120 emails a day now. (<a href="http://theconversation.com/email-is-still-a-problem-that-google-and-others-are-happy-to-keep-that-way-37885">Thanks to theconversation.com</a>)</blockquote>
Approximately 108 billion business emails are sent each day. Email is critical to communication in organizations.

<h2>Security and privacy more important than ever</h2>
Kolab was designed with a security centric architecture from the beginning. With the revelations of government spying over the past few years and other dangerous electronic invasions, this aspect of Kolab has grown in importance to many people. It was one of the primary considerations for the Munich city government when <a href="https://joinup.ec.europa.eu/community/osor/news/munich-chooses-open-source-groupware-solution">Kolab was selected for implementation</a> in February 2014. In addition, the City of Munich has implemented the KDE Desktop as part of <a href="http://en.wikipedia.org/wiki/LiMux">LiMux - The IT evolution</a>, migrating the City to free and open source software. Privacy and security are essential requirements to organizations, and highly important to many individuals as well.

<h2>Kolab Now</h2>
Recent reports of government intrusions—involving organizations such as the U.S. NSA and the U.K. GCHQ—are cause for alarm by some individuals. U.S.-based email providers such as Google and Microsoft have been required to release information to government agencies without notifying the people involved. Encryption and other privacy measures can't be trusted to such arrangements.

Kolab Now provides enterprise-class Kolab capabilities and support for individuals and smaller groups. The secure email and collaboration services are based in Switzerland. Individual data is "protected by a unique combination of terms of service, laws, operational principles and technology. Kolab Now will never put you under surveillance to sell your data or profile and there will be no advertisements. Enjoy the convenience of the Cloud without compromising freedom and openness." (from the <a href="https://kolabnow.com">Kolab Now website</a>)

<h2>Kolab Systems and KDE</h2>
Kolab Systems provides enterprise level support for Kolab Groupware. The company actively supports development of various aspects of Kontact; some Kolab Systems employees are paid to work on Kontact. Through this symbiosis, Kolab Systems has access to one of the best full-featured desktop clients (Kontact), while scalability and other benefits flow to one of KDE's premier applications. For example, Christian Mollekopf recently wrote about <a href="https://cmollekopf.wordpress.com/2015/02/08/progress-on-the-prototype-for-a-possible-next-version-of-akonadi">progress with Akonadi</a>. Kolab Systems is an important player in the KDE ecosystem and is a strong advocate for Free Software. Their work with organizations such as the City of Munich reflects well on KDE's value in large scale deployments.