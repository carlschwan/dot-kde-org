---
title: "Akademy 2015 Talks announced"
date:    2015-06-17
authors:
  - "sealne"
slug:    akademy-2015-talks-announced
---
<p>The <a href="https://conf.kde.org/en/akademy2015/public/schedule/2015-07-25">talks program</a> for <a href="https://akademy.kde.org/2015">Akademy 2015</a> is now available. Details for organizing BoFs will be announced later. </p>


<h2>Registration is still open for Akademy</h2>
Anyone can attend Akademy for free. But registration is required in order to attend the event. Please <a href="https://akademy.kde.org/2015/register">register</a> soon so that we can plan accordingly.

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.