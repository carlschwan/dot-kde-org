---
title: "May Update for KDE Applications 15.04"
date:    2015-05-12
authors:
  - "unormal"
slug:    may-update-kde-applications-1504
---
<figure style="width: 150px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /><figcaption align="center">KDE App Dragons</figcaption></figure>

Today, the KDE Community is happy to announce the release of KDE Applications 15.04.1.
 
This minor update includes a number of bugfixes, focusing especially on Kdenlive, Okular, Umbrello, and Marble.  In addition to software bugs, issues with translations have also been addressed in this release.
 
Beyond the core of KDE Applications, this update includes a long-term support update for the Plasma Workspaces (4.11.19), the KDE Development Platform (4.14.8), and the Kontact Suite (4.14.8).
 
To learn more about this release, refer to the <a href="https://www.kde.org/announcements/announce-applications-15.04.1.php">full announcement</a>.