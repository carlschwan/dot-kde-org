---
title: "You can help making KDE technologies even better!"
date:    2015-08-16
authors:
  - "sandroandrade"
slug:    you-can-help-making-kde-technologies-even-better
comments:
  - subject: "kdetoohardtouse"
    date: 2015-08-17
    body: "<p>I was very happy with KDE 3.5.9.&nbsp; Everything I needed to do could be found easily with one or two cliks. But the new KDE 4 series buried EVERYTHING under three or more menus.</p><p>I'm all for sturctural improvements but you guys have gone overboard on features that complicate the desktop. The documentation is poor to nonexistent for all the changes you've done. I'm very appreciative of all the hard work that has gone into KDE. But someone needs to look at how hard it is to use FOR A NON PROGRAMMER. Maybe you should think of making a series of Youtube video tutorials for the public?&nbsp; J.P.</p>"
    author: "j.d.p."
  - subject: "want to help design the UI"
    date: 2015-08-20
    body: "<p>Hi, i would like to help contribute the UI design. Where can i start?</p>"
    author: "adicahya"
  - subject: "You can start right here"
    date: 2015-09-03
    body: "<p>You can start right here https://community.kde.org/Get_Involved/art</p>"
    author: "Rostislav"
---
<p>Modern life has become increasingly dependent on software systems. Many daily used devices rely on <a href="http://fsfe.org/about/basics/freesoftware.html">Free Software</a> for their basic functionality or additional services. TV sets,  ATMs, smartphones, media centers and in-flight entertainment systems are examples of how Free Software has been pushing the boundaries of current technology. This is achieved by using well-proven solutions, developed in a collaborative,  open, and trusted way. The Workspaces, Applications, and Frameworks delivered by KDE are representatives of the empowerment Free Software provides to our lifes. Examples are educational applications of the <a href="https://edu.kde.org">KDE-Edu suite</a>, lots of KDE technology deployments in public centers for digital inclusion and a full open software stack for mobile devices  with <a href="http://www.plasma-mobile.org">Plasma Mobile</a>.</p>

<p><a href="https://www.kde.org/fundraisers/kdesprints2015/"><img src="/sites/dot.kde.org/files/Fundraiser-Banner-2015.png" /><center><b>Donate to the KDE Sprints 2015 fundraising campaign</b></center></a></p>

<p>Furthermore, in the past few years KDE has been a quite fruitful community for incubating projects like <a href="https://community.kde.org/Incubator/Projects/GCompris">GCompris</a>, <a href="https://dot.kde.org/2010/01/24/kde-gears-free-cloud">ownCloud</a>, and <a href="https://community.kde.org/Incubator/Projects/WikiFM">WikiFM</a>. This is a good indication of how open, diverse, and bold the community turned out to be. KDE also coaches young  Computer Science professionals: our continuous participation in programs such as <a href="https://developers.google.com/open-source/gsoc/">Google Summer of Code (GSoC)</a> and <a href="http://season.kde.org">Season of KDE (SoK)</a> has been providing a welcoming and fertile atmosphere for newcomers, helping leverage their technical and social skills and enjoy the full  experience of Free Software contribution.</p>

<p>Although most of  the work in Free Software projects is done remotely, mediated by online discussions in mailing lists and IRC channels, it is well known that in-person meetings (sprints) are important in driving general goals. They help having in depth discussions and to get things done. Sprints are a chance for contributors to do focused hard work, everybody being enthusiastic and committed to get the most out of these days. Sprints made the most innovative and bold ideas become true. Newcomers joined  with their hearts taken by the vibrant and thriving atmosphere they  experienced.</p>

<p>
<img src="http://dot.kde.org/sites/dot.kde.org/files/RandaWork.JPG" />
<center><b>People focused at work during a KDE sprint</b></center>
</p>
<p><br /></p>
<p>KDE has a <a href="https://sprints.kde.org/sprint/all">strong tradition in organizing sprints</a>, focused on different KDE technologies. Over the  last few years, we conducted sprints which brought together developers from teams like <a href="https://sprints.kde.org/sprint/266">Kontact Suite</a>, <a href="https://sprints.kde.org/sprint/260">Plasma</a>, <a href="https://sprints.kde.org/sprint/248">digiKam</a>, <a href="https://sprints.kde.org/sprint/170">Calligra</a>, <a href="https://sprints.kde.org/sprint/200">Krita</a>, <a href="https://sprints.kde.org/sprint/218">Okular</a> and many more. 
During  the sprint days, long-time contributors, prospective newcomers and GSoC/SoK students gathered together to build and improve relationships, strengthen the collaboration and create amazing KDE products.</p>

<p>The <a href="http://www.randa-meetings.ch">Randa Meetings</a> in particular are of the most successful KDE sprints. They allow contributors from many KDE projects doing awesome work in a quiet place  in the mountains. Focused working in separate team rooms leads to great progress among software, artwork and documentation and the shared dinning room built up good relations and friendships.</p>

<p><img src="https://community.kde.org/images.community/4/4f/Randa_group_pic_preview.jpeg" />
<center><b>Group Picture - Randa Meetings 2014</b></center>
</p>
<p><br /></p>
<p>This year's Randa Meetings motto is <a href="https://www.kde.org/fundraisers/kdesprints2015/"><b>Bring Touch to KDE</b></a>. It's a unified effort to make KDE applications ready for touch-based devices and looks like a logical follow up to the <a href="https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform">Plasma Mobile project announced</a> at Akademy 2015 in late September. But this is just a nice coincidence.</p>

<p>To make all of this happen, KDE relies on the support of our patrons and from initiatives like <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5">Join the Game</a> and the <a href="http://ev.kde.org/getinvolved/supporting-members.php">Supporting Membership Program</a>. Your contribution can make a difference in helping us keeping all those  sprints running. <a href="https://www.kde.org/fundraisers/randameetings2014/">Last year's Randa fundraising campaign</a> was of utmost importance for covering the expenses of contributors' participation. We believe that's a great reward for those who dedicate their spare time as volunteers and keep pushing the boudaries of KDE technologies. We would love to win you as an active KDE supporter, contributing in the development, translation, artwork, and promotion activities. If you are unable to do that due to whatever reasons, please  consider giving a donation in the <a href="https://www.kde.org/fundraisers/kdesprints2015/">KDE Sprints 2015 fundraising campaign</a>.  This is also an important way of helping us to move KDE technologies and values forward. We'll be glad to send you a nice "Thank You" gift in return! 

<!--
<div align="center">
<iframe width="640" height="360" src="https://www.youtube.com/watch?v=yua6M9jqoEk" frameborder="0" allowfullscreen></iframe>
</div>-->

<p><a href="https://www.youtube.com/watch?v=yua6M9jqoEk"><img src="/sites/dot.kde.org/files/Video-Randa-Meetings-2014.png" /><center>Check out what we have done in KDE Randa Meetings 2014</center></a>.</p>