---
title: "Akademy 2015 Keynote: Matthias Kirschner"
date:    2015-07-15
authors:
  - "devaja"
slug:    akademy-2015-keynote-matthias-kirschner
comments:
  - subject: "S.U.S.E 6.2 CD-ROM images"
    date: 2015-07-15
    body: "Oh how I would love to get my hands on again those S.U.S.E 6.2 CD-ROM as I have accidentally thrown away those. Left are only the box, manual and floppies :(\r\n\r\nWhy? Because I have my computer from that era and I really love to sometimes boot those to see what has gone forward and what backwards. \r\n\r\nSo wish I could get images of those installation disks....\r\nContacted once to someone and told that they can't distribute those images because proprietary software in them. Denied even when I had proof that I owned the product :("
    author: "Fri13"
  - subject: "Send me an e-mail"
    date: 2015-07-17
    body: "Please write me an e-mail with your postal address (my contact details are on <a href=\"http://fsfe.org/about/kirschner\">my FSFE page</a> ). "
    author: "Matthias Kirschner"
  - subject: "Read it in Spanish"
    date: 2015-07-18
    body: "Hi.\r\nI have translated this interesting interview into spanish for spanish speakers!!\r\n- https://victorhckinthefreeworld.wordpress.com/2015/07/18/entrevista-a-matthias-kirschner-vicepresidente-de-la-fsfe-que-dara-una-charla-en-akademy/\r\n\r\nHey, I'll be there in Akademy and I'll try to attend to the keynote!!\r\n\r\n've phun!!"
    author: "Victorhck"
---
At Akademy 2015, one of the most awaited <a href="https://conf.kde.org/en/akademy2015/public/events/281">keynotes</a> this year will be that by Matthias Kirschner and here we have a conversation with his charming self in person.

<b>Tell us a bit about yourself?</b>

Hey there, I am Matthias from Berlin, I work for the Free Software Foundation Europe, and <a href="http://blogs.fsfe.org/mk/">I love it</a>. Describing oneself is one of those really difficult tasks, you will hopefully know a little more about me after this interview. Or better still, why don't you just find out for yourself at this year's Akademy!

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; text-align: right;"><img src="/sites/dot.kde.org/files/kirschner.jpg" /><br />Matthias Kirschner</div> 

<b> Which was the first ever Linux Distribution you'd used and how did you chance upon it? And what made you never turn back to your previous non FOSS system?</b>

In 1999, my family had two computers at home: an Intel Pentium II MMX and an Intel 486. For the Pentium we had a modem, and there was an Ethernet connection to the 486. One day I wanted to send e-mails from the Pentium to the 486 without the internet connection. It did not make a lot of sense, as both computers were in the same room, but I was eager to see, if I could make it work. Although both computers had “email programs” installed, I was not able to figure out how to directly end mails from one computer to the other.

When I complained about this in school, a friend suggested a solution and brought me some SuSE 6.0 floppies, CDs and books the next day. That's how it all started. Several hours later I had my first GNU/Linux installation. To be more precise, at the beginning I just had a command line, no working X server, no audio setup, nor a working printer.

Together with some friends we visited the LinuxTag in Stuttgart and afterwards founded a local Free Software user group. We helped each other to install Free Software on our computers, configuring them to be routers, mail/print or file servers. I enjoyed learning with others, exchanging ideas, trying to fix problems. I subscribed to many mailing lists, and was eager to participate in Free Software events.

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -231px; width: 464px; border: 1px solid grey"><img src="/sites/dot.kde.org/files/distributions-small.jpg" /></a></div> 

<br> </br>

So I ended up installing every GNU/Linux distribution I could get my hands on. I did not have a dual boot on these computers, but a quintuple or sextuple boot. The only limitation at that time was disk space; and I did not want to waste it with a non-free operating system.

<b>I read some where that you studied Political and Administrative Science and yet are so deeply involved with technology. How did the two seemingly diverging domains fit together in your head and how exactly are they interlinked as far as you are concerned?</b>

I got active in politics because of Free Software.

After some time using Free Software, learning more about computers on a technical level I stumbled over the GNU GPL. Reading the preamble made me curious. I wanted to know more, therefore I read the <a href="https://gnu.org/philosophy/">GNU philosophy pages</a> with all the articles by Richard Stallman. This made me realise: Free Software is a political as well as a technical issue.

The more I thought about it, the more I realised that <a href="http://fsfe.org/freesoftware/society/democracy.en.html">software freedom is crucial for a democratic society</a>. Software is deeply involved in all aspects of our lives. I am convinced that this technology has to empower society not restrict it. It would represent a great danger to democracy if software were controlled by only a small group. We need to balance power, as we do in a democracy. For me, the best safeguard for this is software freedom.


<b>When did you get started with FSFE and how did that happen?</b>

During my studies at the University of Konstanz we had to do a 7 month internship to gather work experience. A friend of mine, who is a Debian Developer, suggested the German Federal Office for Information Security (BSI) and the <a href="http://fsfe.org/">Free Software Foundation Europe</a>.

I could not find any information about internships with the FSFE, but the more I read, the more I was convinced that I wanted to get to know this place. The first response I got was that FSFE had never had an intern before, and that they did not have an office. I replied that I would still be interested. The next reply was that they could not pay me, to which I replied that I still wanted to do it.

Then Georg Greve, the president at that time, replied that my “perseverance deserved a qualified and fair evaluation and reply”. Sometime afterwards when I’d already subscribed myself to several public FSFE mailing lists, he sent me his travel schedule, and we agreed to meet at the Geneva airport for an internship interview. As a chance coincidence, and a spotting of his GNU pin, we bumped into each other on the train to Geneva. At the start of it, we had a long talk about the internship, but very soon bifurcated to talks about all kinds of Free Software details.

When Georg boarded his plane, we both knew we wanted to work together. It was an amazing experience. Georg and I worked together in his small one-room apartment: he at the desk, I with my laptop on the sofa. I also traveled with Georg to Istanbul, Dublin and other places.

After the end of my internship, Georg convinced me to switch from the university of Konstanz to Potsdam, so that I could do volunteer work for FSFE in Berlin. It was so much fun that most of the time I ended up studying even more owing to my work for FSFE rather than the other way around. Finally, I was hired by the FSFE in 2009 and set up its Berlin office.

<b> Would you call yourself a tech geek? Any instances or examples from your lifestyle (besides the obvious ones) to support your claim?</b>

That's a difficult one, because I know so many people who are way more into tech than I am.

When I am at FOSDEM; I wouldn't dare to call myself a tech geek. But when I am in the parliament; I am seen as one.

Also, in the past, a lot of people did call me a nerd or a geek. For example in school I was carrying around a huge laptop, which was not exactly considered cool at that time. At university I often was the only one using a laptop in the seminar. I was “the guy with the laptop”, and later fellow students thought I was geeky because I was using tiling window managers, several xterms, running mutt, and writing my university notes in Latex with VI, VI, VI the editor of the beast.

<b>What is a normal day in your life like?</b>

Our baby son was born at the end of last year, so at the moment there is no such thing as a normal day. Every day something new happens, and I love it that way.

<b>As the Vice President of FSFE, which is a huge position, what does your work basically involve?</b>

There are many different tasks involved: lots of e-mail discussions, giving talks and convincing new audiences about the importance of software freedom, analysing policy drafts, speaking with politicians and civil servants, writing articles, newsletters, blog entries, press releases and answering press inquiries, meeting and discussing issues with other Free Software contributors, moderating internal discussions, trying to get rid of problems for volunteers and staff so they can do their work, brainstorming about new activities or managing ongoing activities, or convincing people to donate to FSFE.

<b>Could you give a brief overview of what your keynote at Akademy is going to be about?</b>

My keynote at Akademy is about the threat to the computer as a universal machine and how companies arbitrarily limit what we as a society can do with those machines, and how they use technical measures to take away rights from us, which we usually receive when we buy a product. I will give an overview about those developments, and raise some questions which I hope will lead to good discussions during Akademy.

<b>Any thing special that first pops up into your head when you hear the word KDE? Or Akademy?</b>

First thing that pops up is sitting in front of my computer seeing my first “K Desktop Environment” (I think it was version 1.0) when I first successfully got it running after a long phone support session with a friend.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/KDE%201.1.jpg" /><br />KDE 1.1</div>

The next thing which pops up are all those people I know in KDE who form this community. Lydia, who often comes to the KDE office in the evening, Claudia with whom I searched for a shared office for KDE and FSFE in Berlin and with whom I worked in the same space for over 4 years. And also the many interesting discussions with lots of KDE contributors at Free Software events in the last decade come to mind.

<b>Could you tell us about your alter ego's 'Into the Wild' foray? Any memorable incidences related to your hobby of conducting wilderness seminars, which you’d like to share?</b>

In my spare time I assist in wilderness first aid seminars.

Since I was a child I have loved outdoor activities. I liked caving for several years, later hiked with little equipment and far away from a power plug. In my caving team I heard about an accident where someone in the team died in the cave. And I knew then that first aid can be crucial in such situations when it takes the doctor a long time to arrive.

To prepare myself for the worst case, I participated in a wilderness first aid seminar organised by a German non-profit organisation. In realistic scenarios you learn how to evaluate the situation, how to check for consciousness, breathing, pulse, how to detect shock, dyspnoea, and hypothermia, as well as a detailed examination, immobilisation, wound treatment, and how to organise an evacuation, or an emergency camp.

From there on I participated at least every two years and have become part of the team. Beside enabling others to save lives, the seminars have led me to amazing places and experiences, like sleeping in the snow at the polar circle or plunging myself into the WhiteWater torrents at a swift water rescue technician training.

On an everyday note, I somehow often end up helping with emergencies on the streets or subway lines of Berlin.

<b>How, in a brief summary, would you say is Free Software so intricately linked to the progress and development of society?</b>

Free Software allows everybody to take part. There are no artificial barriers, so people from all over the world can learn, and can come up with new ideas. We will be able to solve many problems of our world with software, because we have intelligent people with new ideas and the will to change something for the better. It is crucial that no central entities can dictate to others what they can and cannot do with software.

<b>Your favourite books and music?</b>

To keep it short: First there is Lawrence Lessig's “Code and other laws of cyberspace”, which helped me a lot to understand the connection between software and politics. Second, the comic book <a href="https://en.wikipedia.org/wiki/Transmetropolitan"> Transmetropolitan </a> is just brilliant, and if you have not yet read it, I strongly recommend that you do and am a bit jealous that you still have that pleasure before you.

For music, I am a big fan of the German band “Die Ärzte”. Some FOSDEM visitors might have had the bad luck of eating mussels in the same restaurant where several FSFE folks were singing “Die Ärzte” songs, while Reinhard Müller was simultaneously translating into English. This has been followed by doing Monty Python's silly walk back to our Brussels hotel. My apology to everybody who suffered under this; but it might happen again at Akademy…

If these reasons seem like a lure enough to you, come to Akademy in A Coruña, and do not forget to attend Matthias' keynote! 


<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.