---
title: "Performance and Animation: Krita Kickstarter Kicks Off"
date:    2015-05-04
authors:
  - "Boudewijn Rempt"
slug:    performance-and-animation-krita-kickstarter-kicks
comments:
  - subject: "Great Work"
    date: 2015-05-05
    body: "Top class great work KDE ...  loving it.\r\n\r\n\r\n\r\nShilpa :)\r\n-------------\r\n"
    author: "Shilpa Sharma"
---
<div style="float: center; padding: 1ex; margin: 1ex; "><a href="/sites/dot.kde.org/files/Kritauser-interface.jpg"><img src="/sites/dot.kde.org/files/Kritauser-interface700.jpg" /></a></div>
Last year, we held the first Kickstarter campaign for <a href="https://krita.org/">Krita</a>. We raised more than €20,000 for Krita development, blowing past the fundraising goal. Thanks to this funding, a year later a <a href="https://krita.org/krita-2-9-the-kickstarter-release/">dozen new features have been implemented</a>, ranging from transform masks to High Dynamic Range (HDR) painting, from layer styles to improved vector objects. The Krita team did all that was promised...and much more.
<!--break-->

It was a great year for Krita all-round, with the <a href="https://en.wikipedia.org/wiki/Steam_%28software%29">Steam</a> release, a booth at <a href="http://www.siggraph.org/">SIGGRAPH</a>, and the Artists' Choice award in <a href="http://www.creativebloq.com/digital-art/paint-pro-free-digital-art-software-11513863">ImagineFX magazine</a>.

Now, it’s time for another Kickstarter! This year, we're even more ambitious. If there's one thing that's always held back free graphics software, it's raw interactive performance. That's true for Krita as well. So that is what we'll focus on first!

Next is extended animation support. Together with <a href="https://dot.kde.org/2015/03/06/kde-accepted-google-summer-code-2015">Google Summer of Code</a> student Jouni Pentikäinen, we'll be putting hand-drawn 2D animation right into the core of Krita. That will require many of the optimizations Dmitry Kazakov will be working on.

And if we go over the initial goal—well, there are 24 stretch goals. For every additional €1500 over the initial goal another stretch goal will be added. After the dust settles, the backers will be asked to vote for their favorite goals!

Help us spread the word and make <a href="https://www.krita.org/kickstarter">this campaign</a> a big success!

Check out the <a href="https://www.youtube.com/watch?v=WPwY7fTSq-0&feature=youtu.be">Krita Kickstarter video</a>. Here's a <a href="https://www.youtube.com/watch?v=BHP7HklGUuo">video of Krita in action</a> (Layerstyles Work in Progress).

https://www.krita.org/kickstarter