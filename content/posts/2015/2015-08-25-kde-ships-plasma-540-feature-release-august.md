---
title: "KDE Ships Plasma 5.4.0, Feature Release for August"
date:    2015-08-25
authors:
  - "jriddell"
slug:    kde-ships-plasma-540-feature-release-august
comments:
  - subject: "great work!"
    date: 2015-08-25
    body: "<p>I can't wait to use it!</p>"
    author: "Shawn"
  - subject: "[feature request] in Plasma"
    date: 2015-08-25
    body: "<p><span style=\"color: #3f4549; font-family: 'Helvetica Neue', arial, sans-serif; font-size: 15px; line-height: 21px;\">lets say I have Firefox pinned to Taskbar. I right click it but it doesn't give option to open a private tab. Windows, OSX, Ubuntu Unity have this.&nbsp;</span></p>"
    author: "Haider Rehman"
  - subject: "sound applet"
    date: 2015-08-25
    body: "<p>One complaint. Why cant i controll volume of specific application directly from sound applet?</p>"
    author: "realist"
  - subject: "Volume Mixer"
    date: 2015-08-25
    body: "<p>I hope the Volume Mixer allows me to adjust App Volume - I often run Rhythmbox and Steam Games at the same time and the Shell Volume management is a essential must for me.<br /><br />Here's the one I use on Gnome - <a href=\"https://extensions.gnome.org/extension/858/volume-mixer/\">Volume Mixer</a>.<br /><br />I'm also very excited to try the Full Screen App Menu on Arch.<br /><br />Plasma 5.3+ is winning me back since KDE 4.x - I appreciate that I can customize Panels on each Monitor with independent Window Lists for that monitor.<br /><br />I also appreciate the simplification of the icons, the Control Center and the well designed Icon Grid on the Desktop.<br /><br />I believe software should be \"designed\" and not \"evolved\" and KDE is a slam dunk win these days save some small areas. Great work! :)</p>"
    author: "ElectricPrism"
  - subject: "The previous (and current as"
    date: 2015-08-30
    body: "<p>The previous (and current as well?) mixer for KDE that does what you want is called KMix, it's better to use that if you want separate controls for different applications straight from the task panel.</p>"
    author: "Anonymous"
  - subject: "Audio widget"
    date: 2015-10-09
    body: "<p>Hello, Why the new audio widget can't control the sound \"by application\", in plasma 5.3 I could change volume of one application directly from the widget, but wuth the new widget in plasma 5.4 it's not possible anymore.. we can control only the different device. and need to go in the settngs, to change the volume for the desired application, it was much better like as it was before.. :(</p>"
    author: "st\u00e9phane"
  - subject: "kwallet-pam is added to open your wallet on login"
    date: 2015-10-25
    body: "\"kwallet-pam is added to open your wallet on login\", does this mean this is available to all users and enabled by default now?\r\n\r\nIf so, this would be the biggest UX-Achievement for KDE in the last 5 Years!!\r\n\r\nWhy is this REALLY HUGE step forward, hidden as a tiny remark in a big list? *sigh*"
    author: "Thomas1234"
---
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png" style="border: 0px" width="600" height="380" alt="Plasma 5.4" />
</a>
<figcaption>Plasma 5.4</figcaption>
</figure>

Tuesday, 25 August 2015. Today KDE releases a feature release of the new version of Plasma 5.


This release of Plasma brings many nice touches for our users such as much improved high DPI support, KRunner auto-completion and many new beautiful Breeze icons.  It also lays the ground for the future with a tech preview of Wayland session available.  We're shipping a few new components such as an Audio Volume Plasma Widget, monitor calibration tool and the User Manager tool comes out beta.

<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-audiovolume-shadows.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-audiovolume-shadows-wee.png" style="border: 0px" width="300" height="235" alt="Audio Volume" />
</a>
<figcaption>The new Audio Volume Applet</figcaption>
</figure>

<h3>New Audio Volume Applet</h3>

Our new Audio Volume applet works directly with PulseAudio, the popular sound server for Linux, to give you full control over volume and output settings in a beautifully designed simple interface.

<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-dashboard-2-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-dashboard-2-shadow-wee.png" style="border: 0px" width="300" height="190" alt="Dashboard alternative launcher" />
</a>
<figcaption>The new Dashboard alternative launcher</figcaption>
</figure>

<h3>Application Dashboard alternative launcher</h3>
<ul>

Plasma 5.4 brings an entirely new fullscreen launcher Application Dashboard in kdeplasma-addons: Featuring all features of Application Menu it includes sophisticated scaling to screen size and full spatial keyboard navigation.

The new launcher allows you to easily and quickly find applications, as well as recently used or favorited documents and contacts based on your previous activity.
</ul>

<br clear="all" />
<figure>
<a href="https://kver.files.wordpress.com/2015/07/image10430.png">
<img src="https://kver.files.wordpress.com/2015/07/image10430.png" style="border: 0px" width="300" height="210" alt="New Icons" />
</a>
<figcaption>Just some of the new icons in this release</figcaption>
</figure>
<h3>Artwork Galore</h3>
Plasma 5.4 brings over 1400 new icons covering not only all the KDE applications, but also providing Breeze themed artwork to apps such as Inkscape, Firefox and Libreoffice providing a more integrated, native feel.
<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-krunner-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-krunner-shadow-wee.png" style="border: 0px" width="300" height="210" alt="KRunner" />
</a>
<figcaption>KRunner</figcaption>
</figure>

<h3>KRunner history</h3>
<ul>

KRunner now remembers your previous searches and automatically completes from the history as you type.
</ul>

<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-nm-graph-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-nm-graph-shadow-wee.png" style="border: 0px" width="300" height="279" alt="Networks Graphs" />
</a>
<figcaption>Network Graphs</figcaption>
</figure>

<h3>Useful graphs in Networks applet</h3>
<ul>

The Networks applet is now able to display network traffic graphs. It also supports two new VPN plugins for connecting over SSH or SSTP.</ul>

<br clear="all" />
<h3>Wayland Technology Preview</h3>
<ul>


With Plasma 5.4 the first technology preview of a Wayland session is released. On systems with free graphics drivers it is possible to run Plasma using KWin, Plasma's Wayland compositor and X11 window manager, through <a href="https://en.wikipedia.org/wiki/Direct_Rendering_Manager">kernel mode settings</a>. The currently supported feature set is driven by the needs for the  <a href="https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform">Plasma Mobile project</a>and more desktop oriented features are not yet fully implemented. The current state does not yet allow to use it as a replacement for Xorg based desktop, but allows to easily test it, contribute and watch tear free videos. Instructions on how to start Plasma on Wayland can be found in the <a href='https://community.kde.org/KWin/Wayland#Start_a_Plasma_session_on_Wayland'>KWin wiki pages</a>. Wayland support will improve in future releases with the aim to get to a stable release soon.
</ul>

<h3>Other changes and additions</h3>
<ul>
<li>Much improved high DPI support</li>
<li>Smaller memory footprint</li>
<li>Our desktop search got new and much faster backend</li>
<li>Sticky notes adds drag &amp; drop support and keyboard navigation</li>
<li>Trash applet now works again with drag &amp; drop</li>
<li>System tray gains quicker configurability</li>
<li>The documentation has been reviewed and updated</li>
<li>Improved layout for Digital clock in slim panels</li>
<li>ISO date support in Digital clock</li>
<li>New easy way to switch 12h/24h clock format in Digital clock</li>
<li>Week numbers in the calendar</li>
<li>Any type of item can now be favorited in Application Menu (Kicker) from any view, adding support for document and Telepathy contact favorites</li>
<li>Telepathy contact favorites show the contact photo and a realtime presence status badge</li>
<li>Improved focus and activation handling between applets and containment on the desktop</li>
<li>Various small fixes in Folder View: Better default sizes, fixes for mouse interaction issues, text label wrapping</li>
<li>The Task Manager now tries harder to preserve the icon it derived for a launcher by default</li>
<li>It's possible to add launchers by dropping apps on the Task Manager again</li>
<li>It's now possible to configure what happens when middle-clicking a task button in the Task Manager: Nothing, window close, or launching a new instance of the same app</li>
<li>The Task Manager will now sort column-major if the user forces more than one row; many users expected and prefer this sorting as it causes less task button moves as windows come and go</li>
<li>Improved icon and margin scaling for task buttons in the Task Manager</li>
<li>Various small fixes in the Task Manager: Forcing columns in vertical instance now works, touch event handling now works on all systems, fixed a visual issue with the group expander arrow</li>
<li>Provided the Purpose framework tech preview is available, the QuickShare Plasmoid can be used, making it easy to share files on many web services.</li>
<li>Monitor configuration tool added</li>
<li>kwallet-pam is added to open your wallet on login</li>
<li>User Manager now syncs contacts to KConfig settings and the User Account module has gone away</li>
<li>Performance improvements to Application Menu (Kicker)</li>
<li>Various small fixes to Application Menu (Kicker): Hiding/unhiding apps is more reliable, alignment fixes for top panels, 'Add to Desktop' against a Folder View containment is more reliable, better behavior in the KActivities-based Recent models</li>
<li>Support for custom menu layouts (through kmenuedit) and menu separator items in Application Menu (Kicker)</li>
<li>Folder View has improved mode when in panel (<a href="https://blogs.kde.org/2015/06/04/folder-view-panel-popups-are-list-views-again">blog</a>)</li>
<li>Dropping a folder on the Desktop containment will now offer creating a Folder View again</li>
</ul>

<br clear="all" />
<a href="https://www.kde.org/announcements/plasma-5.3.2-5.4.0-changelog.php">
Full Plasma 5.4 changelog</a>
