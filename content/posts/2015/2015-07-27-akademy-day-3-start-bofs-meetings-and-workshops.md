---
title: "Akademy Day 3 the start of BoFs, meetings and workshops"
date:    2015-07-27
authors:
  - "sealne"
slug:    akademy-day-3-start-bofs-meetings-and-workshops
---
<h2>Akademy Awards</h2>

<a href="/sites/dot.kde.org/files/19848515069_26e9f3e6c2_k.jpg"><img src="/sites/dot.kde.org/files/19848515069_766c2506a1_n.jpg" style="float: right; padding: 2ex"></a>
Each year the KDE Community presents Akademy Awards to people who have made special contributions. The jury this year was made up of <a href="https://community.kde.org/Akademy/Awards#2014">recipients from last year</a>.

Last night the 2015 recipients were announced:
<ul><li> Best Application: <b>Milian Wolff</b> and the KDevelop team
</li><li> Best Non-Application: <b>Jens Reuterberg</b> and the Visual Design Group for the Breeze interface design
</li><li> Jury's Award: <b>Albert Vaca</b> for the KDE Connect application.
</li><li> Jury's Award: <b>Scarlett Clark</b> for her work advancing the continuous integration infrastructure to more platforms and modules
</li></ul>
<br clear="all" />

<h2>BoFs, meetings and workshops</h2>

Following the weekend of talks inspiring and informing the community, the focus shifted today to working on details.

For an overview of what happened <a href="https://community.kde.org/Akademy/2015/Monday">today</a> you can watch the <a href="http://files.kde.org/akademy/2015/videos/BoF_wrap_up_-_Monday.webm" type="video/webm">wrap-up session video</a>

<div style="text-align:center">
  <video width="600" height="400" controls="true">
  <source src="http://files.kde.org/akademy/2015/videos/BoF_wrap_up_-_Monday.webm" type="video/webm">
Your browser does not support the video tag.
</video> 
<em>We had some issues with sound at today's wrap-up session location, <br /> so the wrap-up will be moving to a better place tomorrow.</em>
</div>



<br clear="all" />

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

For more information, please contact The <a href="https://akademy.kde.org/2015/contact">Akademy Team</a>.