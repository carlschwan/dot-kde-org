---
title: "Akademy Day 4"
date:    2015-07-28
authors:
  - "sealne"
slug:    akademy-day-4
---
Today continued the BoFs, meetings and hacking sessions. For an overview of what happened <a href="https://community.kde.org/Akademy/2015/Tuesday">today</a> you can watch the <a href="http://files.kde.org/akademy/2015/videos/BoF_wrap_up_-_Tuesday.webm" type="video/webm">wrap-up session video</a>

<div style="text-align:center">
  <video width="600" height="400" controls="true">
  <source src="http://files.kde.org/akademy/2015/videos/BoF_wrap_up_-_Tuesday.webm" type="video/webm">
Your browser does not support the video tag.
</video> 
</div>

<br clear="all" />

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

For more information, please contact The <a href="https://akademy.kde.org/2015/contact">Akademy Team</a>.