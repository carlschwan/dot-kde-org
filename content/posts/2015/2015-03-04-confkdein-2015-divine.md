---
title: "conf.kde.in 2015 - divine"
date:    2015-03-04
authors:
  - "devaja"
slug:    confkdein-2015-divine
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/confKDEinLogo.png" /></div>
Building on the success of conf.kde.in 2014 at <a href="http://www.daiict.ac.in/daiict/index.html">Dhirubhai Ambani Institute of Information and Community Technology (DA-IICT)</a> in the land of Gujarat, the horizon of the KDE Community is  broadening and shifting south. <a href="http://conf.kde.in/">conf.kde.in 2015</a> takes place on the 17th and 18th of April at Amritapuri in Kerala, India. As in previous years of the conference, conf.kde.in 2015 will promote the spirit of free and open source software (FOSS) and offer ideas to build awareness about FOSS culture at the collegiate level, the time when most technology students have their first interactions with Open Source. There will be particular emphasis on <a href="https://www.kde.org/">KDE</a> technology, and on <a href="https://qt-project.org/">Qt</a>, the popular cross-platform application framework.

<h2>The Venue</h2>
Amritapuri is renowned as a spiritual center, named after the humanitarian and spiritual leader <a href="http://en.wikipedia.org/wiki/Mata_Amritanandamayi">Mata Amritanandamayi</a>. The major university of the town, <a href="https://www.amrita.edu/">Amrita University</a>, is known for its efforts in <a href="http://foss.amrita.ac.in/">promoting FOSS culture</a> among the students; an ideal place for hosting conf.kde.in 2015. The University has had extensive involvement with FOSS in the past, the latest being <a href="http://debutsav.in/">DebUtsav</a> (a Debian conference) in October 2014.
<div style="float: center; padding: 1ex; margin: 1ex;text-align:center;"><img src="/sites/dot.kde.org/files/amritaEDU750.jpg"><br />Amrita University</div>

<h2>About conf.kde.in</h2>
conf.kde.in started in 2011 at <a href="http://www.rvce.edu.in/">RVCE</a> in Bangalore as a 5 day event with 300 participants, initiating a series of such KDE events in India. There was a KDE Meetup in 2013 and conf.kde.in 2014 at DA-IICT, which brought in members of the KDE Community from all over the world to attend the event, give talks, and share the spirit of KDE.  The 2015 conference will cater to new members of KDE as well as seasoned developers, providing updates about what is going on in the KDE Community and teaching newcomers how to start making meaningful contributions. These events have been successful in attracting a lot of Indian students to mentoring programs such as <a href="https://www.google-melange.com/gsoc/homepage/google/gsoc2015">Google Summer of Code</a> (GSoC), <a href="https://season.kde.org">Season of KDE</a> and <a href="https://www.google-melange.com/gci/homepage/google/gci2014">Google Code-In</a>. 

This year, the conf.kde.in 2015 organizers intend to generate even more interest and participation by creating a fertile environment for people to get started with KDE, Qt and FOSS through numerous talks, hands-on sessions and demonstrations. 

<h2>Call For Papers</h2>
If the event seems exciting and valuable, this is an opportunity to join in.

How to be part of it?
Submit a paper explaining the content for a presentation of not more than 30 minutes on any aspect of KDE, Qt or other FOSS topic that you want to cover. Please include pertinent information about your background, other talks you've made (if any), and anything else that gives a sense of what attendees can expect from your presentation. The organizers await your innovative proposals, and are looking forward to an abundant gathering of the KDE Community. 

<strong>Submit your proposals <a href="https://kde.in/conf/cfp">here</a>.</strong>

The proposal submission period <strong>closes on March 20, 2015</strong>.

Registration for attending the event as a participant will open soon.
