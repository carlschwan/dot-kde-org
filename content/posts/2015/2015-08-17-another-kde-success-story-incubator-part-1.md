---
title: "Another KDE success story - the Incubator - Part 1"
date:    2015-08-17
authors:
  - "jpwhiting"
slug:    another-kde-success-story-incubator-part-1
---
Over the past year or so KDE has taken a new approach to projects joining our "Umbrella" Namely the <a href="https://community.kde.org/Incubator">KDE Incubator</a>. This new program aims to help projects with similar ideals to our existing projects join us with all that that implies.

<div style="float: right; padding: 1ex; margin: 1ex; "><img href="http://www.wikitolearn.org/" src="/sites/dot.kde.org/files/wikifm-logo_0.png"></div>

The incubator couples a sponsor from the KDE community with a plan to move/migrate a project into the systems that KDE provides as a community including mailing lists, websites, code repositories, etc. One of the main responsibilities of the sponsor is to help the project's members become part of the KDE community itself by guiding in any way required and helping with source code migration, mailing list migration and figuring out the other aspects of how the KDE community works.

One of the first projects to be incubated was <a href="https://community.kde.org/Incubator/Projects/WikiFM">Wiki2learn</a>. It has also been one of the slower projects to migrate fully, but at this years Akademy has had some exposure that should help it grow further.

WikiToLearn (until a few days ago also known as WikiFM) wants to change the way we create and deliver educational content to students, developers, and learners in general.

We have three core objectives.
<ol>
<li>Disseminating free, open and easily accessible scientific knowledge.</li>
<li>Building a learning and publishing platform for high-quality content, created collaboratively.</li>
<li>Integrating and collecting many existing resources on the different subjects, found both online and offline.</li>
</ol>

WikiToLearn is a platform where students, learners and key people in the academia can collaboratively create refine and re-assemble notes, lecture notes or even whole text books, tailored precisely to their needs. Our philosophy is in two simple sentences: “Knowledge only grows if shared” and we want you to “Stand on the shoulders of giants!”.

The effort was started as a purely personal project by a few students of the University of Milano-Bicocca, to collaboratively write lecture notes and free textbooks for our studies. In December 2013 we decided that we could tackle a global challenge: not only change our study career, but provide free and open textbooks to the whole world, created with the help of the best scientists in the world. For this challenge we needed big shoulders: a worldwide community with a track of positive stories of taking a project and leveraging it to success: KDE. We therefore decided to be the first group to join the KDE family through the incubation process. KDE helped us reaching a visibility we could only dream of: with its help we are now starting experimentations in a few universities around the globe, and key scientists from institutions such as CERN, Fermilab or Stanford are now daily contributors.

Check out <a href="https://www.youtube.com/watch?v=yVclxeOLBd0">the official announcement at Akademy 2015</a> (recommended) to know more and visit our <a href="http://www.wikitolearn.org">homepage</a>.

And in a few days we are going to tell you about another project that was successfully incubated. This project will be at the soon to be started Randa Meetings too so don't forget to support us if you like what we do for you:

<p><a href="https://www.kde.org/fundraisers/kdesprints2015/"><img src="/sites/dot.kde.org/files/Fundraiser-Banner-2015.png" /><center><b>Donate to the KDE Sprints 2015 fundraising campaign</b></center></a></p>

