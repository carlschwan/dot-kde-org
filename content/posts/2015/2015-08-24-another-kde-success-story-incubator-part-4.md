---
title: "Another KDE success story - the Incubator - Part 4"
date:    2015-08-24
authors:
  - "jpwhiting"
slug:    another-kde-success-story-incubator-part-4
comments:
  - subject: "Forward Strides"
    date: 2015-08-25
    body: "<p>It seems to me that lately Kdenlive has been making the greatest forward strides of the Free video editors. Recently I've ended up using it the most, and it is clearly more stable than it used to be. Perhaps the development of a stronger relationship with the KDE development community that is described here is the reason why.</p>"
    author: "CFWhitman"
---
<figure style="float: right; margin: 0px">
<a href="/sites/dot.kde.org/files/kdenlive.png">
<img src="/sites/dot.kde.org/files/kdenlive_wee.png" width="400" height="142" />
</a>
<figcaption>Kdenlive is the leading video editor on Linux</figcaption>
</figure>

To wrap up the KDE Incubator success stories, here's a bit from the Kdenlive folks.

<a href="https://kdenlive.org/">Kdenlive</a>, one of the rare free-as-in-speech video editors, started its life more than 12 years ago using KDE3 libraries. At that time, it was mostly the effort of a single person—coding, fixing bugs, publishing releases, managing the website. There was no real connection with the KDE Community. Good contributions came in from other people, but no team was built, a risky situation. In 2013, the main developer, Jean-Baptiste Mardelle, was not able to work on the project, so it was on hold for several months and had some technical problems. We tracked him down like a "Giant Spy" to get the project running until his return! That taught us a lesson. When Mario Fux presented the <a href="https://manifesto.kde.org">KDE Manifesto</a>, it was the exact answer to our problem.

Kdenlive had already started to use KDE git, forums and translation power after its first contact with the KDE Community at the <a href="https://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">Randa Meetings in 2011</a> (where we heard about the KDE Manifesto). Completing the incubation process in 2014 allowed us to benefit from all offered help. Transferring the website (with mailing lists) to the KDE sysadmin team was a great relief for us, the overbooked non-specialists. <a href="https://dot.kde.org/2015/04/15/kde-applications-1504-adds-kde-telepathy-chat-and-kdenlive-video-editing">Joining KDE Applications</a> a few months ago gave us relief from the release tasks, which lets us put the code in good shape 4 times a year instead of once.

We had heard plenty about KDE being much more than a set of libraries or a technical infrastructure, that it is a community. The Kdenlive team needed to experience it. Now that we've been to the <a href="http://www.randa-meetings.ch">Randa Meetings</a> and <a href="https://akademy.kde.org">Akademy</a>, we understand why it is worthwhile to interact with people in real life, in focused coding jam sessions. It greatly boosts motivation (smileys can't beat real smiles), helps us build a clear vision for the future (I'm not developing for myself only, but can't satisfy everyone...what should the focus be?), and offers opportunity to build bridges with other applications (want to work with drawn animations? Hey, <a href="https://krita.org/">Krita</a> is doing that!). These contacts with many different people—designers, artists, developers, project managers, and users—who are contributing to KDE are also valuable feedback and a source of ideas to make our project evolve in exciting directions. 

Kdenlive raised some money in 2013 to fund a huge refactoring task that is only coming out now. However we had refused other donations since then as we were not sure we could use that money fairly. Now we have tasted in-person meetings, but we can't spend all our pocket money. So no problem; every single cent is welcome ;-)

Please donate to the <a href="https://www.kde.org/fundraisers/kdesprints2015/">KDE Sprint fundraising campaign</a>. You'll be helping Kdenlive and other important KDE projects as well.

Thanks to Vincent Pinon and Jean-Baptiste Mardelle for this report.

<p><a href="https://www.kde.org/fundraisers/kdesprints2015/"><img src="/sites/dot.kde.org/files/Fundraiser-Banner-2015.png" /><center><b>Donate to the KDE Sprints 2015 fundraising campaign</b></center></a></p>