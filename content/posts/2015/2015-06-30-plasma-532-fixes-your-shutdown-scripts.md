---
title: "Plasma 5.3.2 Fixes Your Shutdown Scripts"
date:    2015-06-30
authors:
  - "jriddell"
slug:    plasma-532-fixes-your-shutdown-scripts
---
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<a href="https://www.kde.org/announcements/plasma-5.3/plasma-5.3.png">
<img src="https://www.kde.org/announcements/plasma-5.3/plasma-5.3-ickle.png" style="border: 0px" width="600" height="337" alt="Plasma 5.3" />
</a>
<figcaption>Plasma 5.3</figcaption>
</figure>

<p>
Tuesday, 30 June 2015. Today <a href="https://www.kde.org/announcements/plasma-5.3.2.php">KDE releases a bugfix update to Plasma 5, versioned 5.3.2</a>.  <a href='https://www.kde.org/announcements/plasma-5.3.0.php'>Plasma 5.3</a> was released in April with many feature refinements and new modules to complete the desktop experience.
</p>

<p>

This release adds a month's worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:
</p>

<ul>

<li>KWin: 'Defaults' should set the title bar double-click action to 'Maximize.'. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=26ee92cd678b1e070a3bdebce100e38f74c921da'>Commit.</a> </li>
<li>Improve Applet Alternatives dialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0e90ea5fea7947acaf56689df18bbdce14e8a35f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345786'>#345786</a></li>
<li>Make shutdown scripts work. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=96fdec6734087e54c5eee7c073b6328b6d602b8e'>Commit.</a> </li>
</ul>

<a href="https://www.kde.org/announcements/plasma-5.3.1-5.3.2-changelog.php">Full Plasma 5.3.2 changelog</a>
<!--break-->
