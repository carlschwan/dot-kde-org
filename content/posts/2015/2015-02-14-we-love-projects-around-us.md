---
title: "We love the projects around us!"
date:    2015-02-14
authors:
  - "nightrose"
slug:    we-love-projects-around-us
---
<a href="http://ilovefs.org"><img src="/sites/dot.kde.org/files/ilovefs-heart-px.png" style="border: 0 !important;" alt="I love Free Software!" align="right"></a>
Today the Free Software Foundation Europe reminds us to thank and celebrate all those in Free Software we love and whose work we enjoy and built upon. In KDE, we stand on the shoulders of giants. Everything we do in some way depends on Free Software written by many other people - the huge ecosystem around us. Here are just a few of the thousands of them:
<ul>
<li>We Love Qt for being the best toolkit we could hope for to build our software on.</li>
<li>We love the GNU Toolchain (gcc, GNU binutils, ..) - the most used toolchain to bring our amazing software to the masses.</li>
<li>We love Valgrind and the GNU Debugger for helping us improve our software craftmanship.</li>
<li>We love CMake for keeping our project structures sane and for helping us shine on every platform.</li>
<li>We love Xorg and Wayland for giving us the ability to paint on the screen of many devices large and small.</li>
<li>We love git for helping us manage our future and past.</li>
<li>We love the KDE Free Qt Foundation for ensuring that KDE and everyone else can continue to rely on a free and open Qt.</li>
</ul>
Most importantly: we love all the *people* that help run these projects and organizations. Thank you for letting us stand on the shoulders of giants and ensuring together with us that more people have access to free software and control over the technology that shapes their life.
<!--break-->