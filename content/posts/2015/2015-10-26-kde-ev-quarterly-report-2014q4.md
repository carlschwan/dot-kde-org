---
title: "KDE e.V. Quarterly Report - 2014Q4"
date:    2015-10-26
authors:
  - "sandroandrade"
slug:    kde-ev-quarterly-report-2014q4
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/ev_large200.png" /></div>
The KDE e.V. report for the fourth quarter of 2014 is <a href="https://ev.kde.org/reports/ev-quarterly-2014_Q4.pdf">available (PDF)</a>. It features a compendium of all the activities and events carried out, supported and funded by KDE e.V. in that period, as well as the reporting of major events, conferences and mentoring programs that KDE has been involved in.

<h4>Featured Article - Google Summer of Code 2014</h4>
The featured article covers the results that students of <a href="https://en.wikipedia.org/wiki/Google_Summer_of_Code">Google Summer of Code</a> achieved for KDE in 2014. It presents an overview of each student's contributions and a retrospect of KDE history in Google Summer of Code. KDE has been involved in Google Summer of Code since 2005. Since then, 389 students have made the journey into the awesome world of Free Software and KDE technology. In 2014, 39 young FLOSS lovers worked hard to improve KDE projects such as <a href="https://marble.kde.org/">Marble</a>, <a href="https://edu.kde.org/applications/s/parley">Parley</a>, <a href="https://edu.kde.org/kig/">Kig</a>, <a href="https://community.kde.org/Plasma/Plasma_Media_Center">Plasma Media Center</a>, <a href="https://www.calligra.org/">Calligra</a>, <a href="https://www.kdevelop.org/">KDevelop</a>, and <a href="https://www.digikam.org/">digiKam</a>.

<h4>Other Activities</h4>
The report also describes a synopsis of member activities during the fourth quarter of 2014. The digiKam Coding Sprint, in Berlin, focused mostly on porting digiKam to Qt 5 and KDE Frameworks 5. During the PIM Winter Sprint, in Munich, several user stories and scenarios for enabling user-centric design of Kontact were investigated. The KDE e.V. Board winter meeting, in Barcelona, included several topics regarding KDE as a community, and opportunities to improve. The report also presents a summary of KDE participation in Qt Developer Days 2014 in Berlin.

<h4>Results</h4>
The report concludes with the finances for KDE e.V. in 2014, a synopsis on activities undertaken by the Sysadmin Working Group, and the contributors who joined KDE e.V. during the quarter.

We invite you to read <a href="https://ev.kde.org/reports/ev-quarterly-2014_Q4.pdf">the entire report (PDF)</a>.