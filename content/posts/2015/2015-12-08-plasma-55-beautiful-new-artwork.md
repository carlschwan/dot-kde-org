---
title: "Plasma 5.5 With Beautiful New Artwork"
date:    2015-12-08
authors:
  - "jriddell"
slug:    plasma-55-beautiful-new-artwork
comments:
  - subject: "Overstating the number of bugs fixed"
    date: 2015-12-08
    body: "<p>The new features are nice, and it\u2019s good that bugs are being fixed, but the announcement severely overstates the number of bugs actually fixed. It says that \u2018an incredible <a href=\"https://goo.gl/mckdTF\">over 1,000 bugs were fixed</a>\u2019. So I took at look at list of \u2018bugs fixed\u2019. One of them was about text in drop-down menus being almost invisible when using Breeze Dark or other dark themes (<a href=\"https://bugs.kde.org/show_bug.cgi?id=352206\">bug #352206</a>). I had noticed this bug myself, and thought it was nice that it was finally fixed. But taking a look at the bug report, we see that it has only been closed as a <em>duplicate</em> of another bug report, not actually fixed. (I haven\u2019t actually tested it, but the bug report it was a duplicate of is still open.) Bug <a href=\"https://bugs.kde.org/show_bug.cgi?id=351875\">#351875</a> is also listed as one of the \u2018over 1,000 bugs fixed\u2019, and is <em>also</em> closed as a duplicate of the same bug report.</p><p>Some of the bug reports listed are closed as \u2018WONTFIX\u2019, which hardly meets any sensible definition of \u2018fixed\u2019. The number of bugs actually closed as fixed, which one can list by appending <code>&amp;resolution=fixed</code> to the URL in the announcement, is 367 (and of course there may be bugs fixed that never had a bug report). Still impressive, but far from \u2018an incredible over 1,000 bugs were fixed\u2019.</p>"
    author: "Karl Ove Hufthammer"
  - subject: "Full surname"
    date: 2015-12-09
    body: "<p>The full name isn't \"Risto S\" but \"Risto Saukonp\u00e4\u00e4\"</p>"
    author: "Risto S"
  - subject: "Refinements rock"
    date: 2015-12-09
    body: "Great to see a focus on finishing touches and refining the interface. 100 fixed bugs is very welcome too. Thanks and great work everybody"
    author: "jospoortvliet"
  - subject: "Noto fonts \u2013 a good choise"
    date: 2015-12-09
    body: "<p>The choise of Noto fonts is a good one! Proper font design is a lot of work if it is well-done. Differently to the Oxygen font, Noto supports a lot of languages, combining diacritics and some more advanced typografic features. And Noto fonts are typographically well-done. Maybe a little boring, but well-done and solid. This makes KDE typography much better!</p>"
    author: "Lukas Sommer"
  - subject: "GUI very spacious"
    date: 2015-12-10
    body: "<p>Hi,</p><p>I have noticed a few thing in the GUI I find strange and which makes it more spacious than necessary. It feels strange on a laptop with less than HD resolution.</p><p>1. Here and there I find \"3D\"buttons even though most of the Plasma 5 desktop is flat. Every \"OK\", \"Save\" and \"Cancel\" button is 3D.</p><p>ex.&nbsp;http://i0.wp.com/www.aelog.org/wp-content/uploads/2015/12/preview-settings.png - Flat style on the left and all buttons 3D</p><p>ex.&nbsp;https://krita.org/wp-content/uploads/2015/11/krita_animation_1.png</p><p>2. Tile bar is to wide. Too much space above and under text. Just look at the picture of \"Discover\" above.</p><p>3. While looking at the same picture, the row below the title bar is too wide. Search field to wide stealing pixels.</p><p>&nbsp;</p><p>KDE GUI team should not forget that pixels are precious and wasted space is wasted.</p>"
    author: "dajomufdkasl\u00f8fjdal"
  - subject: "This looks awesome and so"
    date: 2015-12-10
    body: "<p>This looks awesome and so many new stuff!</p><p>Thank you guys a lot!</p><p>&nbsp;</p><p>PS:</p><p>Apparently my above message looks, like spam, :-/ since this PS.</p>"
    author: "Richard T."
  - subject: "It looks great!"
    date: 2015-12-11
    body: "<p>has it been packaged fro kubuntu?</p><p>If so, where is it available from?</p><p>Thank you</p>"
    author: "emanuele"
  - subject: "Fixed full surname"
    date: 2015-12-17
    body: "Morning Risto\r\n\r\nJust fixed it. Thanks"
    author: "unormal"
  - subject: "WISH: Launch applications directly in a new/other activity..."
    date: 2016-01-29
    body: "<p>... by providing a button on hovering an item in Kickoff/Kicker.</p><p>That would let activities become more integrated into the users' workflow.<br />While different from the GNOME 'pager' approach, where you have to drag the window into the (new) activity.</p>"
    author: "Anonymous"
---
<style>
figure {text-align: center; float: right; margin: 0px;}
iframe {text-align: center; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<a href="http://www.kde.org/announcements/plasma-5.5/plasma-5.5.png">
<img src="http://www.kde.org/announcements/plasma-5.5/plasma-5.5-wee.png" style="border: 0px" width="600" height="375" alt="Plasma 5.5" />
</a>
<figcaption>Plasma 5.5</figcaption>
</figure>

<p>
Tuesday, 8 December 2015. Today KDE releases a feature update to its desktop software, Plasma 5.5.
</p>
<!--break -->

<div style="text-align: center;font-style: italic;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/3wFTo34mCj0?rel=0" frameborder="0" allowfullscreen></iframe>
<br />
Video of Plasma 5.5 highlights</div>

<p> We have been working hard over the last four months
to smooth off the rough edges, add useful new workflows, make
Plasma even more beautiful and build the foundations for the future. </p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/breeze.png">
<img src="http://www.kde.org/announcements/plasma-5.5/breeze-wee.png" style="border: 0px" width="400" height="220" alt="Breeze Icons" />
</a>
<figcaption>Breeze Icons</figcaption>
</figure>
<h3>Updated Breeze Plasma Theme</h3>

The Breeze Plasma widget theme has been updated to make it more consistent.</p>

While the Breeze icons theme adds new icons and updates the existing icon set to improve the visual design.</p>

<h3>Plasma Widget Explorer</h3>

The Plasma Widget explorer now supports a two column view with new widget icons for Breeze, Breeze Dark and Oxygen</p>

<h3>Expanded Feature Set in Application Launcher</h3>

Context menus in Application Launcher ('Kickoff') can now list documents recently opened in an application, allow editing the application's menu entry and adding the application to the panel, Task Manager or desktop. Favorites now supports documents, directories and  system actions or they can be created from search results. These features (and some others) were previously available only in the alternative Application Menu ('Kicker') and have now become available in the default Application Launcher by sharing the backend between both launchers.</p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/plasma-5.5-colorpicker.png">
<img src="http://www.kde.org/announcements/plasma-5.5/plasma-5.5-colorpicker-wee.png" style="border: 0px" width="300" height="301" alt="Color Picker Plasma Applet" />
</a>
<figcaption>Color Picker Plasma Applet</figcaption>
</figure>
<h3>New Applets in Plasma Addons</h3>
<h4>Color Picker</h4>

Not only have we restored support for the Color Picker applet, we've given it an entire new UI refresh to fit in with Plasma 5.</p>

The color picker applet lets you pick a color from anywhere on the screen and automatically copies its color code to the clipboard in a variety of formats (RGB, Hex, Qt QML rgba, LaTeX).</p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/plasma-5.5-user-switcher.png">
<img src="http://www.kde.org/announcements/plasma-5.5/plasma-5.5-user-switcher-wee.png" style="border: 0px" width="300" height="301" alt="User Switcher Plasma Applet" />
</a>
<figcaption>User Switcher Plasma Applet</figcaption>
</figure>
<h4>User Switcher</h4>

<p>
User switching has been updated and improved and is now accessible from the Application Launcher, the new User Switcher applet and in the lock screen.  It shows the user's full name and user  set avatar.  This is very useful for offices with shared desks.  More info in <a href='http://blog.broulik.de/2015/10/polish-polish-polish-5-5-edition/'>the developer blog</a>.</p>

<h4>Disk Quota</h4>
Plasma 5.5 sees a new applet designed for business environments or universities. This applet will show you usage assessed not around the real disk usage, but your allowed quota by your system administrator.</p>

<h4>Activity Pager</h4>
Done for users whose use case of activities partly overlaps with virtual desktops: it looks like a pager, it behaves like a pager but uses activities instead of virtual desktops. This gives a quick glimpse of what activities are running and how many windows are associated to each activity.</p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/systray.png">
<img src="http://www.kde.org/announcements/plasma-5.5/systray.png" style="border: 0px" width="421" height="105" alt="Legacy Systray Icons" />
</a>
<figcaption>Legacy System Tray Icons</figcaption>
</figure>
<h3>Restored Legacy Icons in System Tray Support</h3>

In response to feedback, we've rewritten support for legacy applications not using the <a href='http://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/'>StatusNotifier</a> standard for system tray icons.</p>

<h3>Bug Stats</h3>

In the run up to the Plasma 5.5 beta an incredible <a href='https://goo.gl/mckdTF'>over 1,000 bugs were fixed</a>.</h3>

<h3>OpenGL ES Support in KWin</h3>
Support for switching to OpenGL ES in KWin returns. So far only switching through an environment variable and restarting KWin is supported. Set environment variable KWIN_COMPOSE to 'O2ES' to force the OpenGL ES backend. Please note that OpenGL ES is not supported by all drivers. Because of that it's not exposed through a configuration mechanism. Please consider it as an expert mode.</p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/kscreenlocker.png">
<img src="http://www.kde.org/announcements/plasma-5.5/kscreenlocker-wee.png" style="border: 0px" width="300" height="234" alt="Screen Locker" />
</a>
<figcaption>Screen Locker</figcaption>
</figure>
<h3>Wayland Progress:</h3>

With Plasma 5.5 a basic Wayland session is provided. Wayland is the successor of the dated X11 windowing system providing a modern approach. The system is more secure (e.g. key loggers are no longer trivial to implement) and follows the paradigm of 'every frame perfect' which makes screen tearing very difficult. With Plasma 5.4 the KDE community already provided a technology preview based on the feature set of the Phone project. With Plasma 5.5 this is now extended with more 'desktop style' usages. Important features like move/resize of windows is now supported as well as many integration features for the desktop shell. This allows for usage by early adopters, though we need to point out that it is not yet up to the task of fully replacing an X session. We encourage our more technical users to give it a try and report as many bugs as you can find.</p>

<p>A new <a href='http://vizzzion.org/blog/2015/11/screen-management-in-wayland/'>screen management protocol</a> has been created for configuring the connected screens of a Wayland session.</p>

<p>Also added are some protocols for controlling KWin effects in Wayland such as window background blur and windows minimize animation</p>

<p>Plasma on Wayland session now features secure screen locking, something never fully achievable with X. Read more about fixing this 11 year old bug on the <a href='https://bhush9.github.io/2015/11/17/screenlocker-in-wayland/'>screenlocker integration developer blog</a>.</p>

<p>Please also see the list of <a href='https://community.kde.org/Plasma/5.5_Errata#KWin_Wayland'>known issues with Wayland on the Errata page</a>.</p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/discover.png">
<img src="http://www.kde.org/announcements/plasma-5.5/discover-wee.png" style="border: 0px" width="300" height="165" alt="Discover" />
</a>
<figcaption>Discover</figcaption>
</figure><h3>New Discover design</h3>
With the help of the KDE Visual Design Group we came up with a new design that will improve the usability of our software installer.</p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/noto.png">
<img src="http://www.kde.org/announcements/plasma-5.5/noto-wee.png" style="border: 0px" width="300" height="103" alt="Noto Font" />
</a>
<figcaption>Noto Font</figcaption>
</figure><h3>New Default Font</h3>
Our default font has switched to <a href='https://www.google.com/get/noto/'>Noto</a> a beautiful and free font which aims to support all languages with a harmonious look and feel.</p>

<br clear="all" />
<figure>
<a href="http://www.kde.org/announcements/plasma-5.5/baloo-kinfocenter.png">
<img src="http://www.kde.org/announcements/plasma-5.5/baloo-kinfocenter-wee.png" style="border: 0px" width="300" height="177" alt="File Indexer Status" />
</a>
<figcaption>File Indexer Status</figcaption>
</figure>
<h3>Info Center</h3>
A status module for the file indexer was added.</p>

<h3>Plasma Networkmanager</h3>
<p>There have <a href='https://grulja.wordpress.com/2015/11/08/upcoming-news-in-plasma-5-5/'>been several improvements</a> to our network manager applet.  WPA/WPA2 Enterprise validation was added, it uses a new password field widget and OpenVPN has more options.</p>

<h3>Wallpapers</h3>

<p>We have a new selection of <a href='https://kdeonlinux.wordpress.com/2015/11/13/wallpaper-contribution-for-plasma-5-5/'>wonderful wallpapers</a>
from RJ Quiralta, Martin Klapetek, Timothée Giet, Dmitri Popov, Maciej Wiklo and Risto Saukonpää for the Plasma 5.5 release.</p>

<h3>Full Changelog</h3>
<p>
<a href="http://www.kde.org/announcements/plasma-5.4.3-5.5.0-changelog.php">
Full Plasma 5.5.0 changelog</a>
</p>

