---
title: "Plasma 5.2 Beta out for Testing"
date:    2015-01-13
authors:
  - "jriddell"
slug:    plasma-52-beta-out-testing
comments:
  - subject: "Outstanding Breeze theme"
    date: 2015-01-16
    body: "Surprised and pleased with my recent Plasma 5 update. I had been using Oxygen window decorations...nice but not a good visual  fit with the Breeze icons and launcher. Before now, the Breeze decorations made my desktop operation kinda shaky.\r\n\r\nI've followed the progress of KWin without fulling understanding the technical details. (Mostly from <a href=\"http://blog.martin-graesslin.com/blog/2014/12/looking-at-the-memory-improvements-of-kdecoration2/\">Martin's blog</a>.) Anticipating the performance boost and modern look.\r\n\r\nAfter a couple of days using the full Breeze theme...everything is noticeably faster, smoother, more solid. Not just the windowing, but also apps and web content. At least that's how it seems.  Good looking too.\r\n\r\nThanks to the KWin team and the Breeze artists. I appreciate what you've accomplished."
    author: "kallecarl"
  - subject: "ISO ?"
    date: 2015-01-17
    body: "Good job to all the team. Nice features.\r\n\r\nIs there an ISO image to try it ?\r\nOr I had to wait for the snapshot here : http://files.kde.org/snapshots/unstable-i386-latest.iso.mirrorlist ?\r\n\r\n"
    author: "Syvolc"
  - subject: "Nice"
    date: 2015-01-20
    body: "Looks very neat!! Great job and contribution!!"
    author: "Axel"
---
Today KDE releases a beta for Plasma 5.2.  This release adds a number of new components and improves the existing desktop.  We welcome all testers to find and help fix the bugs before our stable release in two weeks' time.

<div style="text-align: center">
<a href="https://www.kde.org/announcements/plasma-5.2/full-screen.png">
<img src="https://www.kde.org/announcements/plasma-5.2/full-screen-wee.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="800" height="451" alt="Plasma 5.2" />
</a>
</div>
<!--break-->
<h2>New Components</h2>

<div style="float: right; margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center ">
<a href="https://www.kde.org/announcements/plasma-5.2/kscreen.png">
<img style="border: 0px;" src="https://www.kde.org/announcements/plasma-5.2/kscreen-wee.png" width="400" height="394" alt="Dual monitor setup" />
</a><br />
KScreen dual monitor setup
</div>

<p>This release of Plasma comes with some new components to make your desktop even more complete:</p>

<p><strong>BlueDevil</strong>: a range of desktop components to manage Bluetooth devices.  It'll set up your mouse, keyboard, send &amp; receive files and you can browse for devices.</p>

<p><strong>KSSHAskPass</strong>: if you access computers with ssh keys but those keys have passwords this module will give you a graphical UI to enter those passwords.</p>

<p><strong>Muon</strong>: install and manage software and other addons for your computer.</p>

<p><strong>Login theme configuration (SDDM)</strong>: SDDM is now the login manager of choice for Plasma and this new System Settings module allows you to configure the theme.</p>

<p><strong>KScreen</strong>: getting its first release for Plasma 5 is the System Settings module to set up multiple monitor support.</p>

<p><strong>GTK Application Style</strong>: this new module lets you configure themeing of applications from Gnome.</p>

<p><strong>KDecoration</strong>: this new library makes it easier and more reliable to make themes for KWin, Plasma's window manager.</p>

<h2>Work in Progress</h2>

<p>These modules have problem which makes it unlikely they will be in the stable release but we welcome testing.</p>

<p><strong>Touchpad settings</strong>: feeling left handed? You can use this new System Settings module to set up your touchpad however you like.</p>

<p><strong>User Manager</strong>: a module for System Settings to create and administer user accounts.</p>

<h2>Other highlights</h2>

<p>Undo changes to Plasma desktop layout</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="https://www.kde.org/announcements/plasma-5.2/output.gif" width="608" height="384" alt="Undo desktop changes" /><br />
Undo changes to desktop layout
</div>

<p>Smarter sorting of results in KRunner, press Alt-space to easily search through your computer</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="https://www.kde.org/announcements/plasma-5.2/krunner.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="744" height="182" alt="Smart sorting in KRunner" /><br />
Smart sorting in KRunner
</div>

<p>Breeze window decoration theme adds a new look to your desktop and is now used by default</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="https://www.kde.org/announcements/plasma-5.2/window_decoration.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="720" height="56" alt="New Breeze Window Decoration" /><br />
New Breeze Window Decoration
</div>

<p>The artists in the visual design group have been hard at work on many new Breeze icons</p>

<p>They are have added a new white mouse cursor theme for Breeze.

<p>New plasma widgets: 15 puzzle, web browser, show desktop</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px;" src="https://www.kde.org/announcements/plasma-5.2/new_plasmoid.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="563" height="316" alt="Web browser plasmoid" /><br />
Web browser plasmoid
</div>

<p>Audio Player controls in KRunner, press Alt-Space and type next to change music track</p>

<p>The Kicker alternative application menu can install applications from the menu and adds menu editing features.</p>

<p>Over 300 bugs fixed throughout Plasma modules.</p>

<p><a href="https://www.kde.org/announcements/plasma-5.1.2-5.1.95-changelog.php">5.2 beta full changelog</a></p>

<h2>Package Downloads</h2>

<p>Distributions have created, or are in the process
of creating, packages listed on our wiki page.
</p>

<ul>
<li>
<a href='https://community.kde.org/Plasma/Packages'>Package download wiki page</a></li>
</ul>

<h2>Source Downloads</h2>

<p>You can install Plasma 5 directly from source. KDE's community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need to uninstall older versions or install into a separate prefix.
</p>

<ul>
<li>

<a href='../info/plasma-5.1.95.php'>Source Info Page</a>
</li>
</ul>

<h2>Feedback</h2>

<p>You can provide feedback either via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>. Plasma
5 is also <a href='http://forum.kde.org/viewforum.php?f=289'>discussed on the KDE Forums</a>. Your feedback is greatly appreciated. If you like what the team is doing, please let them know!</p>
