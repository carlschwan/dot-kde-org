---
title: "digiKam Sprint 2014 "
date:    2015-03-05
authors:
  - "cauliergilles"
slug:    digikam-sprint-2014
comments:
  - subject: "Thanks"
    date: 2015-03-07
    body: "Sorry to hear about your accident, Gilles and great to hear that you recovered. So thanks to all of the digiKam team for this great piece of software and let's hope that the work on bringing KDE Frameworks 5 to more platforms helps you as well."
    author: "unormal"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/digiKam8a40.jpg" /></div>
digiKam is a mature open-source project (more than 14 years old now) that provides a digital asset management application oriented to photography post-production.

<h2>The Event</h2>
It had been almost three years since the last time the <a href="http://www.digikam.org">digiKam</a> team had an opportunity to meet, talk, code and spend some time together. Gilles Caulier, the lead coordinator, was a victim of a serious car crash two years ago and was thus unable to organize or attend such an event. Now, we finally had an opportunity to meet again. After a lot of effort finding a suitable place and a date suitable for all developers to work together under optimal conditions, the digiKam coding sprint 2014 finally took place in <a href="https://sprints.kde.org/sprint/248">Berlin</a>, between November 14th and 16th 2014.
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/officeentrancedb9_n.jpg" /></div>
Before going through what happened during these days, we give sincere thanks to <a href="http://www.digia.com/en/Contact">Digia</a>, the company in charge of Qt development, for hosting the event, and also in particular Tobias Hunger, who welcomed us at Digia's offices located in the South of Berlin. Many thanks also to <a href="https://ev.kde.org/">KDE e.V.</a> for financial support that made the sprint possible.

People participating in the sprint (below, from left to right) :
<ul>
<li><a href="https://plus.google.com/+GillesCaulier">Gilles Caulier</a> from France</li>
<li><a href="https://plus.google.com/u/0/105136119348505864693">Teemu Rytilahti</a> from Finland</li>
<li><a href="https://plus.google.com/u/0/100157321534338890446">Shourya Singh Gupta</a> from India</li>
<li><a href="https://www.facebook.com/marcel.wiesweg">Marcel Wiesweg</a> from Germany</li>
<li><a href="https://plus.google.com/114906808699351374523">Veaceslav Munteanu</a> from Republic of Moldova</li>
<li><a href="https://plus.google.com/u/0/+DmitriPopov">Dmitri Popov</a> from Denmark (photographer)</li>
</ul>
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/digiKamSprintGroupAll.jpg" /></div>

<h2>What happened during the sprint</h2>
The next major task is to port digiKam to Qt5. Approximately 10% was already ported by Gilles before the sprint, and the objectives for this coding sprint were as follows:
<ul>
<li>Specify timeline for porting digiKam.</li>
<li>Identify priorities (what should be ported first).</li>
<li>Delegate porting tasks to developers (who does what?).</li>
</ul>

Long discussions evolved around these topics. Gilles explained the experience he already gained with this sort of work, which tools are available to facilitate the porting, and where manual work is required. The libraries which are part of the digikam project were prioritized for the port, and tasks were assigned.

There were also discussions about the KIPI framework and its plugins. After many years of development, some plugins are essentially unmaintained and no longer needed by digiKam as their functionality was superseded or moved, leaving them out of the porting task. We also talked about APIs to provide better integration between KIPI and digiKam for a task-based framework such as digiKam's batch queue manager. The KDE Frameworks 5 (KF5) port seems like the right time to integrate binary incompatible as well as architectural changes where needed.

Shourya Singh Gupta worked on implementing the KIPI tools functionality in the Batch Queue Manager (Tools Settings). To do this, there were discussions regarding what API changes must be done to the stack to facilitate a generic way to plug kipi-plugins into BQM. By the end of Coding Sprint, there were changes made to APIs to allow a generic way to plug kipi-plugins' settings widgets into the user-interface, tested by converting two plugins (DNG converter and KIO export tool) to take advantage of this feature. Later, the background processing part of DNG converter—responsible for doing the real work—was also ported. This work is currently still in its separate feature branch, waiting to be merged after the frameworks porting branch becomes more stable.

Marcel worked on memory consumption problems with the database functionalities as well as several reported memory leaks. As soon as he could reproduce the problems under valgrind, many cleanups and fixes were committed. Among other fixes a long-standing bug (https://bugs.kde.org/show_bug.cgi?id=205776) was fixed.

He also worked to complete the Removable Collection support. The goal is to show thumbnails from disconnected media without actually having access to the full file, as this information is stored by digiKam in its internal metadata database. In practice this means that users can continue to search and preview collections with thumbnails and other metadata. Feedback to the user is provided to indicate items and collection that are not available for editing (See bugs https://bugs.kde.org/show_bug.cgi?id=191494 and https://bugs.kde.org/show_bug.cgi?id=114539). This feature was completed during the train ride back from Berlin, and committed Sunday evening following the Sprint.

Gilles polished the whole libkgeomap public API to be ready for the KF5 port. A lot of changes have been applied to reduce binary compatibility issues. This is especially needed if a library is to become a KF5 library to be more easily reused by other projects. A similar move has recently been made to libkface to make it available for KPhotoAlbum.

Veaceslav worked on porting libkdcraw from the old KDE4 Threadweaver API to the new KF5 Threadweaver implementation. Unfortunately, the new API was not quite stable nor documented, and Gilles decided to port it one more time to use a pure Qt thread pool implementation.

Teemu fixed some crashes as well as some small annoyances and introduced his plans to work on cleaning up the codebase, starting with cleaning up the CMakeLists and moving misplaced source files to their proper places. This will be an on-going process. 

Dmitry, who is a long time digiKam user and who has written the famous <a href="https://www.digikam.org/node/543">Recipes Book</a>, reported the need to have digiKam be less dependent of the KDE desktop so that it can be more suitable elsewhere. This does not mean losing KDE support, but rather wrapping properly all specific KDE features used by digiKam as optional when it's possible. Dmitry took lots of photo of the event and shared user experience with developers, which introduced some long and instructive discussions about photographer methodologies and workflow.

After long days of coding, the tired developers went out in search for food in the quarter around the hotel at Rosenthaler Straße. Sushi on Friday and Vietnamese food on Saturday managed to sustain the developers for the following day of coding.
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/dinner846_n.jpg" /></div>

<h2>Aftermath<?h2>

The <a href="https://www.digikam.org/about/releaseplan">digiKam release plan</a> has been discussed and published. As many kipi-plugins are still not yet ported to Qt5, digiKam 5.0 must be delayed until the end of the year. Christmas sounds like the right moment to offer code to the user community.

MySQL support is disabled by default for now because it's not fully functional and still experimental. MySQL support is still fully available, but as an optional feature. The plan is to find a student to work on it with the goal to stabilize code, and to add PostgreSQL support through Qt SQL plugins.

digiKam core (and the libraries it depends on) is now mostly ported to Qt5/KF5. It's compilable and running, although there is still ongoing effort to port away from KDElibs4 support libraries that are currently used. The port is not yet ready for prime-time and one can encounter bugs caused by porting, but in the near future there will be beta releases to get reports from end-users about regressions.

However, there is still a lot of work required especially with kipi-plugins, of which only a small part (about 20%) is currently ported. For people who want to try out and help with development, the code is available in the Frameworks branches of corresponding projects. <a href="https://www.digikam.org/contrib">The contribution page</a> has more information.

The build-system (CMake) structure is currently being cleaned up to make the codebase more maintainable for the future as well as making writing unit tests a breeze. At the same time, the dependencies of different parts are being investigated and cleaned up, to allow easier compilation on Windows and OSX.

<h2>Final Words</h2>

digiKam is planning to participate once again in Google Summer of Code (GSoC) this year. There are some new ideas <a href="https://community.kde.org/GSoC/2015/Ideas#digiKam">available in the wiki</a> to attract new contributors. We suggest that anyone interested in working on digiKam this summer should start getting familiar with the project already. 

Once more, thank you to the folks at Digia (and Tobias) for your hospitality and to KDE e.V. for sponsoring the event!

digiKam in action. More photos from the event are available on <a href="https://www.flickr.com/photos/digikam/sets/72157648876788027">Flickr</a>. 

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/puppy163a_c.jpg" /></div>
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/wetDog9a26_c.jpg" /></div>

