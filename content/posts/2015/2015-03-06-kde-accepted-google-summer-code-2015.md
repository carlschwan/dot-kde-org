---
title: "KDE accepted to Google Summer of Code 2015"
date:    2015-03-06
authors:
  - "teo"
slug:    kde-accepted-google-summer-code-2015
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/gsoc_logo_2015_0.png" /></div>
The KDE student programs team is happy to announce that KDE has been accepted as a mentoring organization for <a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2015">Google Summer of Code 2015</a>. This will allow students from around the world to work with mentors on KDE software projects. Successful students will receive stipends from Google.

The Google Summer of Code program offers development funding and mentorship to students who want to work on open source software projects. The program provides students the opportunity to learn more about coding, work within structured software development environments, and push bug fixes and new features into real-world, production software. For Google and the rest of the world, the program provides improvements to software that millions of people use on a daily basis.

Ideas on what a student entering Google Summer of Code 2015 with KDE might work on are listed on the <a href="https://community.kde.org/GSoC/2015/Ideas">Community Wiki</a>.