---
title: "KDE Applications 15.04 Adds KDE Telepathy Chat and Kdenlive Video Editing"
date:    2015-04-16
authors:
  - "unormal"
slug:    kde-applications-1504-adds-kde-telepathy-chat-and-kdenlive-video-editing
comments:
  - subject: "The Kdenlive screenshot link"
    date: 2015-04-16
    body: "The Kdenlive screenshot link is not correct."
    author: "Anonymous"
  - subject: "KDE Non LInear Video Editor."
    date: 2015-04-17
    body: "Now we can properly talk about that acronym for this fantastic video editor."
    author: "Ernesto Manr\u00edquez"
  - subject: "fixed."
    date: 2015-04-17
    body: "Fixed."
    author: "jospoortvliet"
  - subject: "Kino would be fun but that"
    date: 2015-04-17
    body: "Kino would be fun but that name is taken already ;-)\r\n\r\nBesides, it's up to the kdenlive team anyhow - you could've discussed it with them at any point anyway ;-)\r\n\r\nWhatever the name, I'm super happy with kdenlive joining the KDE family as I believe it's good for the pace of development and as I use kdenlive a lot - the better it gets the happier I am."
    author: "jospoortvliet"
---
<figure style="float: right; margin: 0px">
<a href="/sites/dot.kde.org/files/kdenlive.png">
<img src="/sites/dot.kde.org/files/kdenlive_wee.png" width="400" height="142" />
</a>
<figcaption>Kdenlive is the leading video editor on Linux</figcaption>
</figure>

Today KDE released <a href="https://www.kde.org/announcements/announce-applications-15.04.0.php">KDE Applications 15.04</a> our suite of 150 applications.  Notable additions in this release include Kdenlive the leading video editor on Linux and KDE Telepathy the chat application to unify your instant messaging.

<a href="http://www.kdenlive.org">Kdenlive</a> is the most versatile non-linear video editing software available on Linux. It recently finished <a href="https://community.kde.org/Incubator/Projects/Kdenlive">its incubation process</a> to become an official KDE project and was ported to KDE Frameworks 5. The team behind this masterpiece decided that Kdenlive should be released together with KDE Applications 15.04. Some new features they included are the autosaving function of new projects and a much improved clip stabilization. For the future the team plans to refactor big parts of the application and to bring back the MacOSX and Windows versions.

KDE Telepathy is our app for instant messaging. It was ported to KF5 and Qt5 and is a new member of the KDE Applications releases. It comes along with KAccounts, bringing a centralized location for handling all your online accounts to Plasma Desktop.

<figure style="float: right; margin: 0px">
<a href="/sites/dot.kde.org/files/kaccounts.png">
<img src="/sites/dot.kde.org/files/kaccounts-wee.png" width="400" height="271" />
</a>
<figcaption style="word-wrap: normal"> KAccounts lets applications connect to internet accounts</figcaption>
</figure>
    
The KDE Education team have been busy. Andreas Cord-Landwehr turned Rocs upside down: he heavily improved the graph theory core and reworked many other things. KHangman was ported to QML and given a fresh coat of paint in the process. Cantor got some new features around its Python support. And KAnagram received a new 2-player mode and the letters are now clickable buttons and can be typed like before. In addition several of the KDE Games were ported to KF5.

This release continues the new style of releases, introduced with the latest <a href="https://dot.kde.org/2014/12/17/kde-applications-1412-new-features-frameworks-ports">KDE Applications 14.12 release</a>. Along with this release is KDE Workspaces 4.11.18 as part of the LTS version of our Plasma 4 desktop which will end in August and the KDE PIM applications in their 4.14.7 versions.

Half the applications have now been ported to <a href="https://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a> (KF5), bringing the total to 72. The complete list of the applications is available on the <a href=" https://community.kde.org/Applications/15.04_Release_Notes">KDE Applications 15.04 release notes</a>.

Looking forward to <a href="https://techbase.kde.org/Schedules/Applications/15.08_Release_Schedule">August</a> it is planned to see the first KF5-based versions of Dolphin and the Kontact Suite.

See the <a href="https://www.kde.org/announcements/fulllog_applications-15.04.0.php">full list of changes</a> in KDE Applications 15.04.

<figure style="width: 250px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /><figcaption align="center">KDE App Dragons</figcaption></figure>

<h2>Spread the Word</h2>

Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets, KDE depends on people like you talking with other people! Even for those who are not software developers, there are many ways to support our community and our product. Report bugs. Encourage others to join the KDE Community. Or <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5">support the non-profit organization behind the KDE community</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like Delicious, Digg, Reddit and Twitter. Upload screenshots of your new set-up to services like Facebook, Flickr and Picasa and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 15.04 KDE Applications release.
Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from Twitter, YouTube, flickr, PicasaWeb, blogs, and other social networking sites.