---
title: "Akademy 2015 Call for Papers and Registration open"
date:    2015-03-04
authors:
  - "sealne"
slug:    akademy-2015-call-papers-and-registration-open
---
<a href="https://akademy.kde.org/2015">Akademy</a> is <b>the</b> KDE Community conference. If you are working on topics relevant to KDE or Qt, this is your chance to present your work and ideas at the Conference from 25th-31st July in A Coruña, Spain. The days for talks are Saturday and Sunday, 25th and 26th July. The rest of the week will be BoFs, unconference sessions and workshops.

<div style="float: center; padding: 1ex; margin: 1ex;text-align:center;"><a href="http://byte.kde.org/~duffus/akademy/2014/groupphoto/"><img src="/sites/dot.kde.org/files/akademy2014_group_photo-wee.png" width="750" height="251" /></a><br />Akademy 2014 attendees</div>

<h2>What we are looking for</h2>

The goal of the conference section of Akademy is to learn and teach new skills and share our passion around what we're doing in KDE with each other.

For the sharing of ideas, experiences and state of things, we will have short Fast Track sessions in a single-track section of Akademy. Teaching and sharing technical details is done through longer sessions in the multi-track section of Akademy.

If you think you have something important to present, please tell us about it. If you know of someone else who should present, please nominate them. For more details see the proposal guidelines and the <a href="http://akademy.kde.org/2015/cfp">Call for Papers</a>.

<b>The submission deadline is 31st March, 23:59:59 CEST.</b>

<h2>Registration Open</h2>

Anyone can attend Akademy for free. But registration is required in order to attend the event. Please <a href="https://akademy.kde.org/2015/register">register</a> soon so that we can plan accordingly.

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.