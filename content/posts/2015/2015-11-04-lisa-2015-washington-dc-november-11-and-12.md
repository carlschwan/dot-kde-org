---
title: "LISA 2015 - Washington DC, November 11 and 12"
date:    2015-11-04
authors:
  - "kallecarl"
slug:    lisa-2015-washington-dc-november-11-and-12
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/USENIX_KDE.png" /></div>

KDE will have an exhibit in the <a href="https://www.usenix.org/conference/lisa15/lisa-expo">Expo</a> at the upcoming <a href="https://www.usenix.org/conference/lisa15">LISA</a> (Large Installation System Administration) Conference. The full conference takes place November 8 ‒ 13 in Washington D.C. The Expo is open on the 11th and 12th. There is <strong>no charge to attend the Expo</strong>. 

Several members of the KDE Community will be in the booth—presenting various aspects of KDE; answering questions; demonstrating applications; recruiting contributors, users, companies and sponsors. All members of the KDE Community are welcome to visit, to jump in & represent KDE, or to just hang out with other KDE people. These small gatherings are especially good for local KDE aficionados who are not able to attend larger KDE Community events such as <a href="https://akademy.kde.org/">Akademy</a>. Stop by and meet The World's Happiest Person™.

This is also an opportunity for people who are interested in what the KDE Community is all about. Our governance, separation between development and administration, and strong mentoring programs make up the foundation for an effective international community that is resilient and innovative. Just in the past few years, KDE developers have built a new development platform (<a href="https://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5)</a>, a fully redesigned desktop environment (<a href="https://dot.kde.org/2014/07/15/plasma-5.0">Plasma 5</a>) and a modern look-and-feel (<a href="http://arstechnica.com/information-technology/2014/08/kde-plasma-5-for-those-linux-users-undecided-on-the-kernels-future/2/">Breeze</a>)—demonstrating KDE's value to the broad technology industry.

The LISA conference has long served as the annual vendor-neutral meeting place for the wider system administration community. Recognizing the overlap and differences between traditional and modern IT operations and engineering, the highly-curated 6-day program offers training, workshops, invited talks, panels, paper presentations, and networking opportunities around 5 key topics: Systems Engineering, Security, Culture, DevOps, and Monitoring/Metrics. Don't miss the opportunity to get involved with this essential technology sector and conference.

Many thanks to <a href="http://usenix.org/">USENIX</a> for the generous support of KDE at the <a href="https://www.usenix.org/conference/lisa15">LISA 2015 Conference</a>.