---
title: "Now accepting Google Summer of Code student applications"
date:    2015-03-17
authors:
  - "jriddell"
slug:    now-accepting-google-summer-code-student-applications
---
<p style="text-align: center;"><strong><a href="https://1-ps.googleusercontent.com/sxk/lUi00NOiZZtaYcm5-HDw_Ypz0k/s.google-melange.appspot.com/www.google-melange.com/soc/content/2-1-20150316/images/gsoc/logo/banner-gsoc2015.png.pagespeed.ce.1-XG35qq3Rwm6uEKBR_R.png"><img class="aligncenter" src="/sites/dot.kde.org/files/banner-gsoc2015.png.pagespeed.ce_.1-XG35qq3Rwm6uEKBR_R.png" alt="GSoC logo" width="920" height="156" /></a></strong></p>
<p><strong>Attention prospective Google Summer of Code students</strong>: the student applications window has begun.</p>
<p>If you haven&#8217;t contacted the relevant <a href="https://community.kde.org/GSoC/2015/Ideas" target="_blank">KDE subproject</a> yet (including umbrella projects <a href="http://kubuntu.org" target="_blank">Kubuntu</a> and <a href="http://calamares.io" target="_blank">Calamares</a>) to submit your proposal for review, it is high time to do so. Take a look at our <a href="https://community.kde.org/GSoC/2015/Ideas" target="_blank">Google Summer of Code project ideas page</a>, pick one or more of our exciting <strong>project ideas</strong>, <a href="http://teom.org/blog/kde/how-to-write-a-kick-ass-proposal-for-google-summer-of-code/">dazzle us</a> with your <strong>proposal</strong> and hack your way to <strong>ultimate glory</strong> this summer! A nice paycheck is also part of the deal.</p>
<p>If you have already received feedback and you feel your proposal is in good shape, we encourage you to officially <strong>submit it</strong> now to <a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2015" target="_blank">Google Melange</a>.</p>
<p>Submitting early means your proposal might get more attention, and you will be able to edit it until the end of the student applications period. The <strong>deadline</strong> for student applications is <strong>March 27, 2015</strong>.</p>
<p><strong>Mentors</strong>: interest from prospective students has been significant, and we&#8217;ll need to match those students with mentors. Offering more mentors might increase the number of student slots we get from Google, so if you are an established KDE developer and you are interested in giving a helping hand with Google Summer of Code, please <strong>sign up</strong> to be a mentor on <a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2015" target="_blank">Google Melange</a> as soon as possible.</p>
