---
title: "Plasma 5.2 Bugfix Update"
date:    2015-02-24
authors:
  - "jriddell"
slug:    plasma-52-bugfix-update
---
<div style="text-align: center; float: right">
<a href="https://www.kde.org/announcements/plasma-5.2/full-screen.png">
<img src="https://www.kde.org/announcements/plasma-5.2/full-screen-wee2.png" style="padding: 1ex; border: 0; background-image: none; " width="400" height="226" alt="Plasma 5.2" />
</a>
</div>

Tuesday, 24 February 2015.
Today KDE releases a bugfix update to Plasma 5, versioned 5.2.1.  <a href='https://www.kde.org/announcements/plasma-5.2.0.php'>Plasma 5.2</a> was released in January with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:
<!--break-->
<ul>
<li>Don't turn off the screen or suspend the computer when watching videos in a web browser</li>
<li>Fix Powerdevil from using full CPU</li>
<li>Show the correct prompt for a fingerprint reader swipe</li>
<li>Show correct connection name in Plasma Network Manager</li>
<li>Remove kdelibs4support code in many modules</li>
<li>Fix crash when switching to/from Breeze widget style</li>
<li>In KScreen fix crash when multiple EDID requests for the same output are enqueued</li>
<li>In KScreen fix visual representation of output rotation</li>
<li>In Oxygen style improved rendering of checkbox menu item's contrast pixel, especially when selected using Strong highlight.</li>
<li>In Plasma Desktop improve rubber band feel and consistency with Dolphin.</li>
<li>In Plasma Desktop use smooth transformation for scaling down the user picture</li>
<li>When setting color scheme information for KDElibs 4, don't read from KF5 kdeglobals</li>
<li>Baloo KCM: Show proper icons (porting bug)</li>
</ul>

<a href="https://www.kde.org/announcements/plasma-5.2.0-5.2.1-changelog.php">Full 5.2.1 changelog</a>

<!-- // Boilerplate again -->

<h2>Live Images</h2>

<p>
The easiest way to try it out is the with a live image booted off a USB disk.  Images with Plasma 5.2 are available from <a href='http://cdimage.ubuntu.com/kubuntu/daily-live/current/'>Kubuntu development daily builds</a>.
</p>

<h2>Package Downloads</h2>

<p>Distributions have created, or are in the process of creating, packages listed on our wiki page.
</p>

<ul>
<li>
<a href='https://community.kde.org/Plasma/Packages'>Package download wiki page</a></li>
</ul>

<h2>Source Downloads</h2>

<p>You can install Plasma 5 directly from source. KDE's
community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>. Note that Plasma 5 does not co-install with Plasma 4, you will need to uninstall older versions or install into a separate prefix.
</p>

<ul>
<li>

<a href='https://www.kde.org//info/plasma-5.2.1.php'>Source Info Page</a>
</li>
</ul>

<h2>Feedback</h2>

You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a> or <a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a> or <a  ref='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.
<p>
Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.</a>
</p>

<p>You can provide feedback direct to the developers via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>.  If you like what the team is doing, please let them know!</p>

<p>Your feedback is greatly appreciated.</p>
