---
title: "Akademy 2015 coming to an end"
date:    2015-07-31
authors:
  - "sealne"
slug:    akademy-2015-coming-end
---
<p>For an overview of what happened in the BoFs, meetings and hacking sessions during the second half of the week, you can watch the <a type="video/webm" href="http://files.kde.org/akademy/2015/videos/BoF_wrap_up_-_Thursday.webm">wrap-up session video</a></p>

<div style="text-align:center">
  <video width="600" height="400" controls="true"><br /><source src="http://files.kde.org/akademy/2015/videos/BoF_wrap_up_-_Thursday.webm" type="video/webm"><br />
Your browser does not support the video tag.<br /></source></video></div>


Friday marks the end of Akademy as friends old and new return home, enthused and inspired.

<h2>What is KDE?</h2>

During the BoF days from Monday to Thursday, a great many tiny videos were shot of many of the attendees by Dan Leinir Turthra Jensen. These have been edited and cut up and turned into a video explaining, very shortly, what KDE really is. Being a community of people contributing to the development of software, the conclusion is straight forward. See the unsurprising conclusion in the video entitled <em>What is KDE?</em> (<a href="http://files.kde.org/akademy/2015/videos/What_is_KDE.webm">webm</a>, <a href="http://files.kde.org/akademy/2015/videos/What_is_KDE.mp4">mp4</a>, <a href="https://vimeo.com/leinir/what-is-kde">vimeo</a>), created as a tribute to the KDE community and all the amazing people in it.

<div style="text-align:center">
  <video width="300" height="200" controls="true"><br /><source src="http://files.kde.org/akademy/2015/videos/What_is_KDE.webm" type="video/webm"><br />
Your browser does not support the video tag.<br /></source></video></div>

<h2>About Akademy 2015, A Coruña, Spain</h2><p>For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact The <a href="https://akademy.kde.org/2015/contact">Akademy Team</a>.</p>