---
title: "KDE Commit-Digest for 16th November 2014"
date:    2015-01-19
authors:
  - "mrybczyn"
slug:    kde-commit-digest-16th-november-2014
---
In <a href="http://commit-digest.org/issues/2014-11-16/">this week's KDE Commit-Digest</a>:

<ul><li><a href="https://userbase.kde.org/Gwenview">Gwenview</a> gains "Recent Files" option</li>
<li>Cantor sees multiple backends (R, Lua, KAlgebra/analitza, Maxima, Qalculate) and KLetters ported to Qt5/KF5</li>
<li>Refactoring in of the custom defines and includes plugins in <a href="http://www.kdevelop.org/">KDevelop</a>: the compiler provider is a library now</li>
<li>Skrooge adds new advice detecting scheduled operations having date not aligned with the last inserted operation</li>
<li>Heavy development including introducting a base class Resource; also bugfixes.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-11-16/">Read the rest of the Digest here</a>.