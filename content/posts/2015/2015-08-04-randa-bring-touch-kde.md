---
title: "Randa - Bring Touch to KDE"
date:    2015-08-04
authors:
  - "devaja"
slug:    randa-bring-touch-kde
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/mascot_konqi-randa350.png"></div>
About a year ago, we talked with several people who were going to work together in Randa, Switzerland. These people were united by a love of KDE and had common motives—to make KDE technology better and have tons of fun while doing it!

The 5th edition of the Randa Meetings high in the Swiss Alps in August 2014 was a <a href="https://dot.kde.org/2014/12/08/randa-meetings-2014-another-great-success">huge success</a>, with many new features and major new additions to KDE technology, through the dedicated efforts of about 50 KDE developers taking a week out of their busy lives to bring great software to users.

Among the attendees last year was Călin Cruceru, an enthusiastic Google Summer of Code (GSoC) 2014 student working on <a href="https://marble.kde.org">Marble</a>, the virtual globe and world atlas. He was one of the youngest members of KDE, and worked ardently in Randa along with his mentors and fellow GSoC students during the week. The 2014 Randa Meetings were productive for the Marble project, and quite an experience for Călin who was in his first KDE sprint.

All of this was possible because of your donations to the fundraiser for the Randa Meetings. We are asking you to <strong><a href="https://www.kde.org/fundraisers/kdesprints2015/">continue with your financial support</a></strong> this year; we are excited about the Randa Meetings in 2015 with the theme <strong>Bring Touch to KDE</strong>.

This year the campaign has been expanded to raise funds for <strong>all</strong> KDE sprints. The Randa Meetings are big, but they are only one of the sprints that KDE sponsors throughout the year. The KDE Community is centered around development, and sprints allow for in-person coding that is much more effective than working online and communicating by email. Sprints involve hard work and long days, but it's exhilarating, not tiring. During sprints, developers accomplish more in a few days than they thought possible. It's inspiring to be surrounded by committed, top KDE developers. So while the focus of this fundraising campaign is on the Randa Meetings, all money raised will go towards the high quality, innovative KDE technology that sprints produce.

In this interview, we go back in time for a glimpse into Călin's excitement and eagerness before he attended Randa Meetings 2014 and his anticipation for it. With your support and donations, you can help other newcomers have their first Randa experience this year!

We look forward to bringing you the stories and results from the Randa Meetings 2015.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/CalinCruceru-Cut.jpg" /><br />Călin Cruceru - Marble Developer</div> 

<strong>Hi Călin, please tell us a little bit about yourself</strong>.
My name is Călin Cruceru and I’m in my second year studying Computer Science and Engineering at <a href="http://www.upb.ro/en/">Polytechnic University of Bucharest, Romania</a>. I am passionate about technology and I love contributing to open-source projects and surrounding myself with optimistic people.

<strong>You’ve just started being involved with open source. Why did you pick KDE? What were your expectations and have they been fulfilled?</strong>
Indeed, I just started contributing seriously to open-source projects about 6 months ago. In fact, 'just' is not the best word here, because it certainly does not feel like a short period of time. It all began with a thought that it might be a good experience for me to work on an application which is useful to many people. I had been using KDE as my desktop environment for a year by then and so I thought that the best choice would be something I use often, something I’d been a 'user' of.

To me it was fondly named ‘Marble’. And here I am; helping Marble be a better product since February 2014. I’m considering contributing to other KDE projects, but due to limited time this summer I haven’t been able to do so.

Regarding my expectations, I think that everything went even better than I could have possibly imagined. At the beginning I was a bit skeptical about my chances of getting involved because I thought I wasn’t technically prepared. But my doubt turned out to be an illusion; especially when I received so much help from such a friendly community.

Getting selected for Google Summer of Code to work on a Marble project as well as the feeling of really doing something useful for the application is how I know that all my expectations have been fulfilled.

<strong>How difficult is it for you to manage your school work and involvement with KDE?</strong>
It is not easy for sure and I usually do not have a lot of spare time; but I’m trying to get the best of both. And I think I am managing it pretty well.

<strong>How has your experience been with real world programming, especially contributing to software used by millions of people around the world?</strong>
There is an enormous difference between writing code for school/personal projects and real world applications. Like many other students, I’ve done a lot of coding for academic projects but getting involved with the development of a real application used by millions of people is a completely different experience. You get to know what it feels like to be a part of a big community, which you can’t normally do while working on school projects. You learn how to write code responsibly, knowing that every single line will have an impact on users. Also, you discover 'best practices' when writing real world applications, which improves your skills as a programmer.

­<strong>Many students are introduced to Free and Open Source Software (FOSS) through mentoring programs, and remain associated with it only during the mentoring project. What message do you have for students who want to contribute to FOSS, but who are waiting for acceptance into a mentoring program?</strong>
I think this happens mostly because a lot of students want to improve their resumes and they think that this can be done only by participating in well-known programs. I use the words 'they think' because this is certainly not true, since companies tend to appreciate any open-source contributions (even if they are not part of mentoring programs). I think it is more impressive to see a long-term pattern of contributions without getting any accreditation other than the credits for patches.

What I mention here is just a consequence of an essential ingredient: the sheer passion for FOSS, for the feeling that you too can write code which eases people's lives in some way. So my advice for those students is to take some time and think of what they would prefer to do when their favorite application crashes: wait for the developers to first consider their bug report and then repair it (the only option in closed-source projects), or to get the application's sources and try to fix the problem on their own. Which one describes the qualities of a 'true programmer'?

<strong>When did you hear about the Randa Meetings and why do you want to be there?</strong>
I heard about the Randa Meetings after the registrations had closed, but Mario (the main Randa Meetings organizer) noticed me on IRC congratulating a Marble colleague for participating in Randa and mentioning my regret over not knowing of the event earlier. Mario contacted me and said he might still be able to find a place for me at Randa. After just a day, everything was a certainty.

I’m very enthusiastic about this meeting because I will  finally get to meet in person a lot of great people with whom I’ve been in contact daily for a couple of months now, but only virtually. I am also eager to get involved in the discussions about the future plans for improving KDE. I obviously expect to write a lot of code too!

<strong>Have you got anything in particular planned for Randa?</strong>
Yes, I do. I want to work on the User Interface of the plugin I have been working on since the beginning of GSoC. I plan to do this in Randa because I can work alongside my GSOC mentors, Torsten Rahn and Dennis Nienhüser. This is one of the most challenging parts of my project; face to face discussions about the UI will lead to a better looking and functioning solution.

<strong>What are you looking forward to in the Randa Meetings?</strong>
I am really looking forward to meeting the people behind these great applications, used by millions of people, including myself. To me they are superstars. I want to make new connections within this community that I want to be a part of for quite a long time. I am particularly interested in collaborating with the main people involved with KDE Edu (which Marble is a part of) and contributing to it.

As far as the targets of completion are concerned, I want to make sure that by the 9th of August, the first day at Randa, all the features I added during the summer are fully functional and polished.

<strong>What important things have you learned from the KDE Community?</strong>
I’ve learned that being able to work in a team is a great virtue. I’ve also discovered many tricks and 'best practices' applied in the design of applications. One more important thing is that I've developed an ability to 'feel' when a piece of code is, say, error prone or is a victim of bad design.

<strong>Why do you think meetings such as Randa are important for KDE?</strong>
Such meetings are very important for open-source communities such as KDE mostly because they are the only times when developers gather under the same roof to plan and design new features and to hack on them. A face to face conversation is usually much more productive than any form of virtual communication.

<strong>Why is it important for people to support these meetings? How has the support helped you?</strong>
I think that people should support free and open-source software development so that they can enjoy the software they love without paying for the software. Donations are a small price to pay for the value people receive; even small donations help. FOSS gives developers the opportunity to customize existing software without having to start from scratch. In my case, people's support has really helped; if it were not for their donations, Mario could not have organized my participation in this years' Randa Meeting.

<strong>How do you imagine your typical day in Randa?</strong>
Wake up; have breakfast; socialize until everybody is up; discuss what is of utmost importance to be improved/added; have lunch; continue these discussions; write some code; continue discussions, focusing on specific projects; breathe fresh air from the Alps to get refreshed; work more; have dinner; socialize more; sleep. That's just the first day.

<strong>Any other thoughts?</strong>
I want to send a big thank you to Mario Fux. Without his help I would not have been able to be a part of the trip to Randa. I also want to send the heartiest thanks to my GSoC mentors, Torsten Rahn and Dennis Nienhüser, as well as to my Marble GSoC fellows, Sanjiban and Abhinav, who encouraged me to participate in this meeting.

<i>If you would like to make more KDE sprints happen, <a href="https://www.kde.org/fundraisers/kdesprints2015/">donate</a> now!</i>
