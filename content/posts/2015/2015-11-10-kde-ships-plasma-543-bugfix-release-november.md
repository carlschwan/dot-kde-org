---
title: "KDE Ships Plasma 5.4.3, Bugfix Release for November"
date:    2015-11-10
authors:
  - "jriddell"
slug:    kde-ships-plasma-543-bugfix-release-november
---
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<a href="http://www.kde.org/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png">
<img src="http://www.kde.org/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png" style="border: 0px" width="600" height="380" alt="Plasma 5.4" />
</a>
<figcaption>Plasma 5.4</figcaption>
</figure>


<p>
Today KDE releases a bugfix update to Plasma 5, versioned 5.4.3.  <a href='https://www.kde.org/announcements/plasma-5.4.0.php'>Plasma 5.4</a> was released in August with many feature refinements and new modules to complete the desktop experience.
</p>

<p>
This release adds a month's worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:
</p>

<ul>

<li>Update the KSplash background to the 5.4 wallpaper. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=467d997d8ad534d42b779719cec03a8cbfb66162'>Commit.</a> </li>
<li>Muon fixes PackageKit details display. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=f110bb31d0599fda5478d035bdaf5ce325419ca6'>Commit.</a> </li>
<li>Fix crash when exiting kscreen kcm in systemsettings. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=4653c287f844f2cb19379ff001ca76d7d9e3a2a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344651'>#344651</a>. Code review <a href='https://git.reviewboard.kde.org/r/125734'>#125734</a></li>
<li><a href='http://blog.martin-graesslin.com/blog/2015/10/looking-at-some-crashers-fixed-this-week/'>Several crashes fixed in KWin</a></li>
</ul>

<a href="http://www.kde.org/announcements/plasma-5.4.2-5.4.3-changelog.php">
Full Plasma 5.4.3 changelog</a>

