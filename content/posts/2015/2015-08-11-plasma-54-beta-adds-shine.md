---
title: "Plasma 5.4 Beta Adds Shine"
date:    2015-08-11
authors:
  - "jriddell"
slug:    plasma-54-beta-adds-shine
comments:
  - subject: "Dolphin status"
    date: 2015-08-12
    body: "<p>Hello,<br />And what's about dolphin? ported to KF5?<br />Thank you</p>"
    author: "loways"
  - subject: "Demo"
    date: 2015-08-13
    body: "<p>Hi, I created full demo of KDE Plasma 5.4 Beta - https://www.youtube.com/watch?v=QfqRwAtV3W0</p><p>Thanks for fantastic work!!!</p>"
    author: "Paul"
  - subject: "Dolphin status"
    date: 2015-08-13
    body: "<p>As far as i know dolphin kf5 port will be released alongside kde applications 15.08 (source:&nbsp;https://www.dennogumi.org/2015/08/kde-applications-1508-rc-for-opensuse/) which will be available a few days before plasma 5.4</p>"
    author: "Josef Filzmaier"
  - subject: "Kickerdash"
    date: 2015-08-17
    body: "<p>I'm really impressed by the new kickerdash (application dashboard). It's really fast and well organized, it's much better than fullscreen homerun.</p>"
    author: "Josep Febrer"
  - subject: "Icons on the desktop"
    date: 2015-08-19
    body: "<p>I am confused about having icons on the desktop I thought KDE got rid of that practice by having folder widgets instead.</p>"
    author: "Jzarecta"
  - subject: "Tearing problems"
    date: 2015-08-21
    body: "<p>A lot of people are having tearing problem with kwin , why did you not solve this bug ?</p>"
    author: "David Carmas"
  - subject: "No new plasmoids from KDE4?"
    date: 2015-08-22
    body: "<p>Still no Picture Frame, RSS Now, or Weather widgets? I am disappoint. Love the looks of the new full-screen launcher however, and at last there seems to be a software update widget as well.</p>"
    author: "MirceaKitsune"
  - subject: "Regressions need to be fixed first"
    date: 2015-08-27
    body: "<p>I agree with this. Regressions from previous versions should be fixed before adding new features. Missing plasmoids prevent me from upgrading to Plasma 5. Please focus on bringing back these features.</p>"
    author: "Ken"
---
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png" style="border: 0px" width="600" height="380" alt="Plasma 5.4" />
</a>
<figcaption>Plasma 5.4</figcaption>
</figure>

<p>
Tuesday, 11 August 2015. Today KDE releases a <a href="https://www.kde.org/announcements/plasma-5.3.95.php">beta release of the new version of Plasma 5.4</a>.
</p>

<p>

This release of Plasma brings many nice touches for our users such as much improved high DPI support, KRunner auto-completion and many new beautiful Breeze icons.  It also lays the ground for the future with a tech preview of Wayland session available.  We're shipping a few new components such as an Audio Volume Plasma Widget, monitor calibration tool and the User Manager tool comes out beta.
</p>

<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-audiovolume-shadows.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-audiovolume-shadows-wee.png" style="border: 0px" width="300" height="235" alt="Audio Volume" />
</a>
<figcaption>The new Audio Volume Applet</figcaption>
</figure>

<h3>New Audio Volume Applet</h3>
<p>

Our new Audio Volume applet works directly with Pulseaudio, the popular sound server for Linux, to give you full control over volume and output settings in a beautifully designed simple interface.
</p>

<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-dashboard-2-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-dashboard-2-shadow-wee.png" style="border: 0px" width="300" height="190" alt="Dashboard alternative launcher" />
</a>
<figcaption>The new Dashboard alternative launcher</figcaption>
</figure>

<h3>Application Dashboard alternative launcher</h3>
<ul>

Plasma 5.4 brings an entirely new fullscreen launcher Application Dashboard in kdeplasma-addons: Featuring all features of Application Menu it includes sophisticated scaling to screen size and full spatial keyboard navigation.

The new launcher allows you to easily and quickly find applications, as well as recently used or favorited documents and contacts based on your previous activity.
</ul>

<br clear="all" />
<figure>
<a href="https://kver.files.wordpress.com/2015/07/image10430.png">
<img src="https://kver.files.wordpress.com/2015/07/image10430.png" style="border: 0px" width="300" height="210" alt="New Icons" />
</a>
<figcaption>Just some of the new icons in this release</figcaption>
</figure>


<h3>Artwork Galore</h3>
Plasma 5.4 brings over 1400 new icons covering not only all the KDE applications, but also providing Breeze themed artwork to apps such as Inkscape, Firefox and Libreoffice providing a more integrated, native feel.



<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-krunner-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-krunner-shadow-wee.png" style="border: 0px" width="300" height="210" alt="KRunner" />
</a>
<figcaption>KRunner</figcaption>
</figure>

<h3>KRunner history</h3>
<ul>

KRunner now remembers your previous searches and automatically completes from the history as you type.
</ul>

<br clear="all" />
<figure>
<a href="https://www.kde.org/announcements/plasma-5.4/plasma-screen-nm-graph-shadow.png">
<img src="https://www.kde.org/announcements/plasma-5.4/plasma-screen-nm-graph-shadow-wee.png" style="border: 0px" width="300" height="279" alt="Networks Graphs" />
</a>
<figcaption>Network Graphs</figcaption>
</figure>

<h3>Useful graphs in Networks applet</h3>
<ul>

The Networks applet is now able to display network traffic graphs. It also supports two new VPN plugins for connecting over SSH or SSTP.</ul>

<h3>Other changes and additions</h3>
<ul>

<li>Much improved high DPI support</li>
<li>Smaller memory footprint</li>
<li>Our desktop search got new and much faster backend</li>
<li>Sticky notes adds drag &amp; drop support and keyboard navigation</li>
<li>Trash applet now works again with drag &amp; drop</li>
<li>System tray gains quicker configurability</li>
<li>Wayland tech preview (complete Plasma wayland session), driven by Plasma Mobile</li>
<li>The documentation has been reviewed and updated</li>
<li>Improved layout for Digital clock in slim panels</li>
<li>ISO date support in Digital clock</li>
<li>New easy way to switch 12h/24h clock format in Digital clock</li>
<li>Week numbers in the calendar</li>
<li>Any type of item can now be favorited in Application Menu (Kicker) from any view, adding support for document and Telepathy contact favorites</li>
<li>Telepathy contact favorites show the contact photo and a realtime presence status badge</li>
<li>Improved focus and activation handling between applets and containment on the desktop</li>
<li>Various small fixes in Folder View: Better default sizes, fixes for mouse interaction issues, text label wrapping</li>
<li>The Task Manager now tries harder to preserve the icon it derived for a launcher by default</li>
<li>It's possible to add launchers by dropping apps on the Task Manager again</li>
<li>It's now possible to configure what happens when middle-clicking a task button in the Task Manager: Nothing, window close, or launching a new instance of the same app</li>
<li>The Task Manager will now sort column-major if the user forces more than one row; many users expected and prefer this sorting as it causes less task button moves as windows come and go</li>
<li>Improved icon and margin scaling for task buttons in the Task Manager</li>
<li>Various small fixes in the Task Manager: Forcing columns in vertical instance now works, touch event handling now works on all systems, fixed a visual issue with the group expander arrow</li>
<li>Provided the Purpose framework tech preview is available, the QuickShare Plasmoid can be used, making it easy to share files on many web services.</li>
<li>Monitor configuration tool added</li>
<li>kwallet-pam is added to open your wallet on login</li>
<li>User Manager now syncs contacts to KConfig settings and the User Account module has gone away</li>
<li>Performance improvements to Application Menu (Kicker)</li>

<li>Various small fixes to Application Menu (Kicker): Hiding/unhiding apps is more reliable, alignment fixes for top panels, 'Add to Desktop' against a Folder View containment is more reliable, better behavior in the KActivities-based Recent models</li>

<li>Support for custom menu layouts (through kmenuedit) and menu separator items in Application Menu (Kicker)</li>

<li>Folder View has improved mode when in panel (<a href="https://blogs.kde.org/2015/06/04/folder-view-panel-popups-are-list-views-again">blog</a>)</li>
<li>Dropping a folder on the Desktop containment will now offer creating a Folder View again</li>

</ul>

<br clear="all" />
<a href="https://www.kde.org/announcements/plasma-5.3.2-5.3.95-changelog.php">
Full Plasma 5.4 beta changelog</a>
