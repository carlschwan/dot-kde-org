---
title: "Interview with Lydia Pintscher: Akademy 2015 Community Keynote Speaker"
date:    2015-06-10
authors:
  - "devaja"
slug:    interview-lydia-pintscher-akademy-2015-community-keynote-speaker
comments:
  - subject: "Stallman or Linus?"
    date: 2015-06-10
    body: "It is missing the question \"Stallman or Linus?\" =)"
    author: "Kannon"
  - subject: "I can still answer: Neither ;"
    date: 2015-06-11
    body: "I can still answer: Neither ;-)"
    author: "nightrose"
---
Lydia Pintscher, our very own KDE e.V. Board President and a gem of a person; will be giving the Community Keynote Talk at <a href="https://akademy.kde.org/2015">Akademy 2015</a>, in A Coruña, Spain. This is just a tiny peak into her brain for all that is in store for you in her talk.

<b>So, for the minute subsection of people out there who don’t know you yet, could you tell us a bit about yourself and what drives the cool, composed and awesome you?</b>

I'm with KDE for 9 years now. Since then I have done many things for KDE from marketing to community management to running mentoring programs. I am currently the President of <a href="https://ev.kde.org/">KDE e.V.</a>, the non-profit supporting KDE. It's an exciting position to be in and I'm grateful for the opportunity to help KDE grow and reach its full potential. In my day-job I work as the product manager for Wikidata, Wikimedia's open data project. I'm passionate about enabling people to make great things happen around Free Software and Open Knowledge.

<figure style="width: 320px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/640px-Lydia-Pintscher3_cropped2.jpg" /><figcaption align="center">Lydia Pintscher</figcaption></figure> 

<b>Now; since you talked about your newest position as the KDE e.V. president, could explain a bit about what it's been like for you as the President; what changes have you seen, creeping into both your life and the KDE community?</b>

It's an interesting time for KDE and KDE e.V. With Evolving KDE we are taking a good look at where we are and where we want to go, to then decide how we can get there. This is an incredibly important step for KDE and I'm lucky to be in a position to guide us through that. We are also looking for our first Executive Director in order to help us grow. All this means a tremendous amount of change to KDE e.V. as an organisation and KDE as a community. Making sure that we go through these changes together smoothly is my main focus. And of course there is a lot of day-to-day work that needs to be done on the financial, legal and organisational side. We have a board now that is working together extremely well which makes all this easier.

<b>So, coming to the concept of <a href="https://evolve.kde.org/">Evolving KDE</a>; evolve – when jumbled up backwards can be rephrased to be e.V. Love, I noticed. Could you talk about how the entire campaign Evolving KDE is so intricately linked with the KDE e.V. organisation and the symbiotic relation between the two in terms of the visions for the KDE community and for what we aspire to be? And since this is directly linked to your talk, the Community Keynote this year at Akademy, could you tell our readers a bit more about it?</b>

KDE e.V. has the mandate of supporting the KDE community. We want to do that as best as we can. One of the things that the KDE community needs right now is a better understanding of itself, its state, its goals and the way to achieving these goals. We have been an extremely tight-knit community for many many years. This has made understanding, formulating and agreeing on those comparatively easy. As we grow and diversify this becomes much much harder and we are currently feeling the pain this causes in many areas. This is where Evolving KDE comes in. It is an effort to enable the community to answer all these questions and take an honest look at itself. As a first step we have published a survey that will give us a good overview of where we are and where we want to go as a community. I'll be presenting the data, conclusions and some recommendations based on this at Akademy. We'll then have a sprint to figure out how to put these into practice by finding a path from where we are to where we want to go. KDE e.V. is here to enable and support this process.

<b>Seeing the entirety of work and efforts put into Evolving KDE, a curiosity arises as to how this entire idea came into being. Could you talk about, the ‘Eureka’ moment, associated with this entire campaign?</b>

There was no eureka moment I fear (laughs)! Evolving KDE grew out of talking to a lot of people about how they think KDE e.V. and KDE can be improved. And the realization that we need to have a shared conversation in our community about where we want to go and how we can fulfil our potential.

<b>When using the term evolve, we talk mostly about how KDE has evolved over the years, but could you tell me how KDE has helped you evolve, as an individual over the years? </b>

KDE has helped me personally to evolve a lot. I've been able to do so many things inside KDE that I don't think I could have done so easily anywhere else. I have done marketing, community management, ran mentoring programs and am now the president of a well-established non-profit. Through all this I've always had mentors who showed me the ropes but who also let me try things even if they might not work out. Everything I do in my day-job and elsewhere today, I do well to a huge part because of all the things I learned in KDE. That culture is one of the most important things in KDE to me and we must make sure we keep it as we evolve. And I hope I'm giving some of that back as well.

<b>So; out of all the talks you’ve given till now, if you could pick a favourite for any whacky reason which would be your most memorable talk? And why?</b>

That'd be my keynote at Wikimania last year about Wikidata. When it comes to KDE talks, my talk at conf.kde.in in 2011 was the most memorable one owing to the fact that the people there were so nice and the whole conference was fascinating.

<b>Could you tell us what a normal day in the life of Lydia would be like?</b>
There is no normal, as such. Though usually I start my day with some sports. Then I'll do some emailing and catching up with the world before going to work. The next hours belong to my team and Wikidata. When I get home I'll crash on my couch to get done whatever needs to be done for KDE. This can be Board calls, accounting, reimbursements, answering inquiries by other organisations or pushing forward our mentoring programs for instance.

<b>Could you describe the world as can be seen in a peek through your window, to all our readers of the dot out there?</b>
I live in the middle of Berlin. Right now looking out of my window there's a huge tree, another house and some grass with two shy bunnies on it who are probably going to run away quickly as soon as they see the dog approaching from around the corner.

<b>Bunnies and dogs remind me, do you have a pet of your own?</b>
Not right now, unfortunately. I had a lot of pets in the past though - anything from a cat, guinea pigs, hamster, rabbits, right up to budgies.

<b>What is the most memorable thing about Akademy?</b>
Every year, just being around people who mean so much to me and having fun with them, that is what stands out in terms of memories when it comes to Akademy.

<b>And the books on your shelves? </b>
My favourite ones? Crime novels by Val McDermid. She's a great writer and I love her characters. Currently reading: Some books to polish my Spanish in time for the summer in Mexico and Spain.

<h2>Registration is still open for Akademy</h2>
Anyone can attend Akademy for free. But registration is required in order to attend the event. Please <a href="https://akademy.kde.org/2015/register">register</a> soon so that we can plan accordingly.

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.