---
title: "Akademy Talks Day 2"
date:    2015-07-26
authors:
  - "jriddell"
slug:    akademy-talks-day-2
comments:
  - subject: "full conference video?"
    date: 2015-07-27
    body: "Is there a full video of the entire conference?"
    author: "MN"
  - subject: "There will be"
    date: 2015-07-27
    body: "In past years, videos for every talk were provided a few days after Akademy finished."
    author: "Nicolas"
  - subject: "Smart tech (correction)"
    date: 2015-07-30
    body: "I understood the point of the Smart vs Sensible Tech lightning talk as proving that sane defaults (with options available) produce much better user experience than a ton of options or wizards up front.\r\n\r\nAlso, there is a small typo in my name. Not a biggie, but I\u2019d be happy if it was spelled right \u263a"
    author: "Matija \u201ehook\u201c \u0160uklje"
---
<figure style="text-align:center">
<a href="http://byte.kde.org/~duffus/akademy/2015/groupphoto/"><img src="/sites/dot.kde.org/files/20027702985_0120744d4c_c.jpg" width="800" height="475" /></a>
<figcaption>Akademy 2015 Group Photo</figcaption>
</figure>

The day today started out with showers of water drops as the late comers to Akademy waded their way amidst raincoated cyclists and residents of A Coruna sheltering themselves underneath coloured umbrellas. 

<a href="/sites/dot.kde.org/files/20012791832_e5b28bfb47_b.jpg" title="IMG_6733"><img src="/sites/dot.kde.org/files/20012791832_e5b28bfb47_n.jpg" width="256" height="320" alt="IMG_6733" style="float: right; padding: 2ex"></a>
At sharp 10:00 (okay maybe a bit late here and there), the keynote kicked off the second day to a start as Lydia spoke about evolving KDE, talking about the changing world of mobile phones, smart watches and gizmos like computer glasses. And how we as a community should integrate that into our vision, which was one of the foremost tasks at hand when it comes to helping KDE grow and move forward. While quoting a few references from the Evolve KDE survey, the talk was essentially split into three segments of answering the questions about where are we right now, where do we want to go and how exactly do we get there. In response to the first question there was a consensus on the amazing and diverse community and the wonderful level that KDE apps have reached until now. What we lack is a common vision and a goal to plan out our road ahead.  We need to envisage a more constructive and concentrated growth rather than a scattered one. Greater promotional efforts and campaigns are needed as well. Defining KDE's vision ahead is going to be discussed on the Evolve KDE BoF to be organised by Lydia. 
<br clear="all" />
<p>For those that couldn't attend Akademy you can <a href="http://files.kde.org/akademy/2015/videos/Lydia%20Pintscher%20-%20Keynote:%20Evolving%20KDE.webm">watch the video</a> of Lydia's keynote. There will be a further announcement when the rest of the Akademy videos are available.</p>
<div style="text-align:center">
 <video width="600" height="400" controls="true">
  <source src="http://files.kde.org/akademy/2015/videos/Lydia%20Pintscher%20-%20Keynote:%20Evolving%20KDE.webm" type="video/webm">
Your browser does not support the video tag.
</video> 
</div>

<br clear="all" />

<a href="/sites/dot.kde.org/files/19832479298_2c75557396_b.jpg" title="IMG_6744"><img src="/sites/dot.kde.org/files/19832479298_2c75557396_n.jpg" width="256" height="320" alt="IMG_6744" style="float: right; padding: 2ex"></a>
Visual Design Group members Andrew Lake and Thomas Pfeiffer spoke about vision statements and their importance.  They gave some bad examples such as Dell's, gave some good examples like Mozilla and suggested new ones for Plasma, watch this space.
<br clear="all" />

<a href="/sites/dot.kde.org/files/20025827841_0b0585c361_b.jpg" title="IMG_6786"><img src="/sites/dot.kde.org/files/20025827841_0b0585c361_n.jpg" width="320" height="256" alt="IMG_6786"  style="float: right; padding: 2ex"></a>
The talk on Ring 2.0.0 by Emmanuel Lepage Vallee focused the telephone application previous called SFLPhone.  Version 2 is equipped with video calling, Voice calls over the internet, text messages, multi calls and P2P sharing as some of its primary features. The timeline of the software was given as starting out in 2004 with the first Qt client in 2006, Gnome client in 2014, OS X client in 2015 and the Windows client in 2015. With state machine processes representing the basic higher level data flow and the Qt Logic combined with the 3rd party UX the basic working of the app was explained followed by as simple demo. An informative yet engaging and a prompt and precise talk by Emmanuel was a welcome listen. 
<br clear="all" />

<a href="/sites/dot.kde.org/files/20029185411_41187e402b_b.jpg" title="IMG_6865"><img src="/sites/dot.kde.org/files/20029185411_41187e402b_n.jpg" width="256" height="320" alt="IMG_6865" style="float: right; padding: 2ex"></a>
Harald and Rohan spoke about their two related continuous integration systems, Kubuntu CI was the first system and is the biggest achievement.  It builds packages of KDE Git code quickly using Kubuntu packaging in Launchpad. Debian CI does a similar task for Debian's packaging.  The latest addition is Mobile Kubuntu CI which is used on the Plasma Mobile images from Kubuntu and uses cloud ARM servers to build the packages in minutes.  The CI systems help packager and KDE coders quality assurance with automated tests and it smooths the packaging workflow with fixes being done daily instead of in one large batch.  It also catches ABI changes and has caught several problems made in Frameworks in this area.
<br clear="all" />

<a href="/sites/dot.kde.org/files/20026426575_efe3bb0dc4_b.jpg" title="IMG_6789"><img src="/sites/dot.kde.org/files/20026426575_efe3bb0dc4_n.jpg" width="320" height="256" alt="IMG_6789" style="float: right; padding: 2ex"></a>
Dan revealed his super secret Shashlik project, an attempt to get Android applications running on a normal Linux distribution.  Android is a complex system which except for Linux itself is unrelated to what we think of as Linux distributions.  He went over all the parts that had to be adapted and in the end showed off for the first time "the most exciting black rectangle" he has ever seen.  His challenge has been getting anything showing on screen but just this week he has managed to start to succeed with this.  There will be a lot of work still to do but this will open up a new world of applications to users of Plasma and other Linux desktops, it will mean we have the world's biggest application ecosystem with Plasma Mobile, as well as being an invaluable developer tool.
<br clear="all" />

<a href="/sites/dot.kde.org/files/19941144086_8d50df7c38_b.jpg" title="IMG_5952"><img src="/sites/dot.kde.org/files/19941144086_8d50df7c38_n.jpg" width="256" height="320" alt="IMG_5952" style="float: right; padding: 2ex"></a>
Vishesh Handa gave a talk on File Searching and basically compared the various techniques associated with the file systems across Windows, OS X and Linux platform (reminiscent of a Tannenbaum Operating Systems textbook) but with a very informative and present centric, engaging talk discussing the practice rather than the theory which was what was exactly needed by the audience. NTFS, OS X and Linux were pitted against each other in their modes of handling search indexing, initial indexing, modification tracking, updates and more and the talk ended with an overview of what was being implemented in Baloo as of now with the removal of the use of Xapian Library and the implementation of a customised LMDB library with future developments and usage with Akonadi in sight. 
<br clear="all" />

<a href="/sites/dot.kde.org/files/19837266149_0455b2a9e2_b.jpg" title="IMG_6903"><img src="/sites/dot.kde.org/files/19837266149_0455b2a9e2_n.jpg" width="320" height="256" alt="IMG_6903" style="float: right; padding: 2ex"></a>
Zanshen the testing tool which involves simplified testing doubles or, as in the speaker's language serves as a productive doppelgänger enhancing the testing process, was introduced and talked about by Kevin Ottens and his protégé and it was heartening seeing the mentor and his student giving a talk together and seeing their passion and brainchild and hearing about it in depth. Using stubs for inputting data to the tested code and mocks for verifying it's indirect output, they explained segments of the code and gave a short demo of it as well. 
<br clear="all" />

Before lunch was served we had the group Akademy photo taken, shown above, and then scattering again to fill our tummies and then back again to the conference venue.

The lightening talks covered a wide range of topics from the gardening team by Albert Astals Cid to Cooking Paellas with Docker by Alex Fiestas where he talked of a new and one step recipe to install eyeOS using Docker which has precooked paellas for ease of installation. Matija Sukle gave his lightening talk on the FLA which helps KDE e.V. to update its licences in case of absentee coders, protect the project code and help your code to sustain itself even after you're done contributing. At any sign of a breach of trust, the FLA protects you and gives you the right to take revert all control.

Smart Tech vs Sensible Tech by Jens Reuterberg was possibly the highlight of the lightning talks where he did a unique hands on presentation which used David and Matija as props demonstrating the difference between smart tech and sensible tech. David was filling up a survey while Matija was eating skittles as advised by Jens during the entire span of the presentation as Jens talked about design in the olden days and the need for customised user orientated systems which cater to even the dumbest of users and makes the choices for the user based on knowledge about them rather than averaging them or asking him to make the decisions. Matija hence, was supposed to be the sensible tech which assumes that the user loves Skittles and so keeps on eating them while he can be free to eat the Doritos as well but does not do so since he hasn't been told to. Whereas David played the sensible system which notes down and understands the user and hence as a reward gets the best of both resources - the Doritos and the Skittles. Irina Rempt talked about the people who make Krita possible and showed off much of the art from the artist community including Tyson Tan, Sylvia Ritter, Ahmed Teleb, Lucas Falcao, and many more who use and love Krita in all its intensity and some of who later on move ahead to development as well. 

<a href="/sites/dot.kde.org/files/19405965163_dcde52c866_b.jpg" title="IMG_6968"><img src="/sites/dot.kde.org/files/19405965163_dcde52c866_n.jpg" width="320" height="256" alt="IMG_6968"  style="float: right; padding: 2ex"></a>
Welcome to Massachusetts, with the modified talk name by Martin Gräßlin covered KWin and KWindowSystem as well as the issues with Window Decorations and the required fixes and their future plans. He impressed everyone by doing his talk in Okular running on KWin Wayland.  The architecture items he had presented in 2011 were discussed and he received a round of applause by declaring that from an architecture view all the items were complete.  He received a larger round of applause when he concluded by saying that Plasma 5.4, which comes out next month, will be a tech preview for Plasma on Wayland.
<br clear="all" />

Going Cross Platform by Aleix Pol emphasised on the reasons for writing applications across Windows, Android and other platforms. They include reaching a larger user base, reaching an official software distribution place and observing the software working in different use case scenarios.   

<a href="/sites/dot.kde.org/files/19406213513_9845f40b31_b.jpg" title="IMG_7016"><img src="/sites/dot.kde.org/files/19406213513_9845f40b31_n.jpg" width="320" height="256" alt="IMG_7016"  style="float: right; padding: 2ex"></a>
The Ask Us Anything session with the board involved inquiries about the statistics of the KDE e.V. members and their growth patterns, question regarding the responsibilities of a member of KDE e.V. and how to get elected, the openness of the information and discussions on KDE with the community.
<br clear="all" />

<p style="text-align: center">
<a href="/sites/dot.kde.org/files/20000925566_d77b7687bc_b.jpg" title="IMG_6998"><img src="/sites/dot.kde.org/files/20000925566_d77b7687bc_z.jpg" width="640" height="512" alt="IMG_6998"></a></p>

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

For more information, please contact The <a href="https://akademy.kde.org/2015/contact">Akademy Team</a>.