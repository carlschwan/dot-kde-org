---
title: "Akademy 2015 videos available"
date:    2015-07-28
authors:
  - "sealne"
slug:    akademy-2015-videos-available
comments:
  - subject: "RSS feed perchance?"
    date: 2015-07-29
    body: "Great work all round, really looking forward to these.\r\n\r\nWill there be an RSS feed so I can add it to my gpodder subscriptions (for example)?"
    author: "musakarolia"
  - subject: "That is not currently planned"
    date: 2015-07-29
    body: "That is not currently planned"
    author: "sealne"
---
Video recordings of the Akademy talks are now available in a low quality version to enable them to be released quickly. Higher quality version will be available later.

You can find these linked from the <a href="https://conf.kde.org/en/akademy2015/public/schedule/2015-07-25">talks schedule</a> or look through the video files <a href="http://files.kde.org/akademy/2015/videos/">directly</a>.

Links to slides will also be added in the schedule as presenters upload them.