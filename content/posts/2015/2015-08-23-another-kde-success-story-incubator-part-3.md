---
title: "Another KDE success story - the Incubator - Part 3"
date:    2015-08-23
authors:
  - "jpwhiting"
slug:    another-kde-success-story-incubator-part-3
---
<figure style="float: right; margin: 0px">
<a href="/sites/dot.kde.org/files/kxstitchbatman.png">
<img src="/sites/dot.kde.org/files/kxstitchbatman.png" width="400" height="242" />
</a>
<figcaption><center>KXStitch at work - Be Batman</center></figcaption>
</figure>

Continuing the series about KDE Incubator let's hear how <a href="https://userbase.kde.org/KXStitch">KXStitch</a> went through the process. KXStitch was incubated early and quickly.

As Steve Allewell tells us: "In May 2014 I was contacted by Jeremy Whiting, a contributing developer to KDE, to see if I would be interested in submitting the KXStitch application to the KDE Incubator. KXStitch is an editor for counted cross stitch patterns and had already been in development for more than ten years as an independent KDE application. It was hosted on Sourceforge and as the main developer I was supported by a number of people who had provided ideas, testing, bug fixes and some translations.

The KDE Incubator is an effort to help such applications to be migrated into the KDE infrastructure.  This is something that I had already been considering, so it was an ideal opportunity to make that transition.

It began with Jeremy announcing KXStitch as a candidate on the <a href="https://community.kde.org/Incubator">Incubator wiki page</a> and the creation of <a href="https://community.kde.org/Incubator/Projects/KXStitch">another wiki page for the project itself</a>.  This page includes some information about what the application is and includes a couple of checklists detailing the activities that need to be completed to manage the migration into the KDE infrastructure.

The first checklist is to ensure the application is ready to begin the <a href="https://community.kde.org/Incubator/Incubation_Process">incubation process</a>, i.e. it complies with the <a href="https://manifesto.kde.org/">KDE manifesto</a> and that it is in active development.  From this a plan can be devised for the migration, something which Jeremy, as the sponsor, provided invaluable help and advice pointing me to the relevant people and documentation that I needed to get set up.

The second checklist covers the activities of this plan which, for me, involved setting up a developer account, git repository, mailing lists, web site, wiki and bug tracking. A number of these activities required raising tickets with the system administrators, something that was easily done through the ticketing system and were completed promptly.  The administrators also provided help in getting my code imported into its new home in playground/graphics. Having a developer account created also allowed me access to create wiki pages on the <a href="https://community.kde.org">KDE community site</a>.

After the initial import several people have done a lot of work fixing the documentation and a couple of bugs. The translations have been incorporated into the rest of <a href="http://l10n.kde.org/">KDE's l10n repositories</a> and translations have been done in a lot more languages.

As KXStitch was already a mature application, playground was intended as a short term place whilst the early integration work was done.  At the end of May, KXStitch was moved into kdereview with the intention of moving to extragear/graphics. A number of people had a look over it and provided valuable feedback which prompted some updates fixing a few issues. At the end of June, KXStitch moved to its permanent <a href="https://quickgit.kde.org/?p=kxstitch.git">home in extragear/graphics</a> where it continues development and eventual conversion to <a href="http://api.kde.org/frameworks-api/frameworks5-apidocs/">KDE Frameworks 5</a>.

I would like to thank Jeremy for his help and support during the incubation process which turned out to be simple and straightforward, and the rest of the community for their contributions for getting KXStitch integrated into KDE."

In a few days we'll tell you about another incubated project that participates as well at this year's edition of the Randa Meetings.

<p><a href="https://www.kde.org/fundraisers/kdesprints2015/"><img src="/sites/dot.kde.org/files/Fundraiser-Banner-2015.png" /><center><b>Donate to the KDE Sprints 2015 fundraising campaign</b></center></a></p>