---
title: "June Update for KDE Applications 15.04"
date:    2015-06-03
authors:
  - "unormal"
slug:    june-update-kde-applications-1504
comments:
  - subject: "Missing KDE 5"
    date: 2015-06-03
    body: "The Dolphin is still not ported to KDE 5, that is like the most important software ever for any GUI environment and yet missing!"
    author: "Mk.82"
  - subject: "Some provide it"
    date: 2015-06-07
    body: "Some repositories provide unstable applications. I've been using dolphin for a while and it seems all good. "
    author: "JA"
---
<figure style="width: 150px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /><figcaption align="center">KDE App Dragons</figcaption></figure>

Today, the KDE Community is happy to announce the release of KDE Applications 15.04.2.

More than 30 recorded bugfixes include improvements to Gwenview, Kate, Kdenlive, the Kontact Suite, Konsole, Marble, KGpg, Kig, the KDE Telepathy call UI and Umbrello. In addition to software bugs, issues with translations have also been addressed in this release.
 
Beyond the core of KDE Applications, this update includes a long-term support update for the Plasma Workspaces (4.11.20), the KDE Development Platform (4.14.9), and the Kontact Suite (4.14.9).
 
To learn more about this release, refer to the <a href="https://www.kde.org/announcements/announce-applications-15.04.2.php">full announcement</a>.