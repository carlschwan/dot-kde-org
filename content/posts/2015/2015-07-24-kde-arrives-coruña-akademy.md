---
title: "KDE Arrives in A Coru\u00f1a for Akademy"
date:    2015-07-24
authors:
  - "jriddell"
slug:    kde-arrives-coruña-akademy
---
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/View_of_A_Coru%C3%B1a_from_lighthouse.jpg/800px-View_of_A_Coru%C3%B1a_from_lighthouse.jpg" width="800" height="250" />
<div style="text-align: right"><em>Photo by <a href="https://commons.wikimedia.org/wiki/File:View_of_A_Coru%C3%B1a_from_lighthouse.jpg">slideshow bob</a></em></div>

KDE is de-camping to the far west of Europe today to A Coruña in Galicia.  In this north west corner of the Iberian Peninsula the sun is warm and the air is fresh.  KDE contributors of all varieties will be spending a week in talks, discussions, hacking,  renewing old friendships and getting to know people new to our KDE Community.
<!--break-->

Topics will include our flagship Plasma desktop but also an exciting announcement from the Plasma developers which will take Plasma beyond the desktop again.  We'll be hearing about the next version of e-mail and calendar middle layer Akonadi. KDE is moving out of its transitional desktop ecosystem as seen in a talk about WifiFM.  One of our flagship but new to the community applications is Kdenlive and we'll be reviewing the previous 10 years of this application and looking at the next 10.  A project called Shashlik, which has been exciting the <a href="https://plus.google.com/115040331792171308990/posts/DU2esBYW3Qe">social media world</a>, will be revealed.  

A week of Birds of a Feather sessions follows the talks including some bling in VDG UI Design Open Session, a little je ne sais quoi in KDE France BoF, our desktop and beyond in Plasma General Topics, a day for planning life beyond X with Wayland and two half days planning for life in the leaderless Kubuntu.

<figure style="border: solid thin grey; text-align: center">
<img src="/sites/dot.kde.org/files/19756328400_8362faeb67.jpg" width="500" height="281" alt="DSC_0009">
<figcaption>Deep in the code at Akademy-ES</figcaption>
</figure>

<figure style="float: right; border: solid thin grey; text-align: center">
<img src="/sites/dot.kde.org/files/19967397285_cb2489f9e3_n.jpg" width="256" height="320" alt="IMG_5931">
<figcaption>Alex Talks at Akademy-ES</figcaption>
</figure>

The fun has already started with the annual conference in Spain, Akademy-ES which is happening yesterday and today.  Spain has one of the most dynamic and active free software communities and Akademy-ES always fills up with talks for those who habla Castellano.  Talks have included discussing Microsoft's attitude to standards and documenting, the history of search frameworks Baloo, behind the code by Victor the Sysadmin and lighting talks including one on the successful Barcelona Free Software Hackers meetup.

Also today is the Annual General Meeting of <a href="https://ev.kde.org/">KDE e.V.</a> our legal body.  Here we have voted on a new board member Sandro Andrade from Brazil.  Sandro has been talking about KDE and Qt at conferences across the continent such as FISL and organising Lakademy, he recently finished his PhD and was looking for new challenges to fill in spare time, KDE e.V. has just filled that slot.  We also voted on new board members of the KDE League, reviewed the outcome from Lydia's Evolving KDE questionnaire and heard from the sysadmin and community working groups about their work for the last year. A video from our treasurer Marta reviewed the accounts over the last year which while full of challenges are in a pleasingly stable state.

<figure style="border: solid thin grey; text-align: center">
<a data-flickr-embed="true" href="https://www.flickr.com/photos/jriddell/19348427463/in/dateposted-public/" title="DSC_0016"><img src="https://farm1.staticflickr.com/350/19348427463_0fd0dafd93.jpg" width="500" height="281" alt="DSC_0016"></a>
<figcaption>The Annual KDE e.V. Beauty Contest Ended with Sandro (centre) as Your New Board Member</figcaption>
</figure>

<figure style="border: solid thin grey; text-align: center">
<a data-flickr-embed="true" href="https://www.flickr.com/photos/jriddell/19357998483/" title="811829974_75499"><img src="https://farm1.staticflickr.com/280/19971056972_9e6ca25ab1_z.jpg" width="480" height="640" alt="811829974_75499"></a>
<figcaption>The Galacian Opening Ceremony to Welcome Absent Friends <a href="https://www.flickr.com/photos/jriddell/19357998483/">Video</a></figcaption>
</figure>

<h2>About Akademy 2015, A Coruña, Spain</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2015">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.