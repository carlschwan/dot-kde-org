---
title: "First Plasma Wayland Live Image"
date:    2015-12-18
authors:
  - "jriddell"
slug:    first-plasma-wayland-live-image
comments:
  - subject: "Operating system of live image"
    date: 2015-12-18
    body: "<p>This post interests me, although before I download and try this live image I would l like to know the operating system of this live image. Guessing it is some sort of Linux distro like Fedora.&nbsp;</p>"
    author: "Brenton"
  - subject: "It's Kubuntu"
    date: 2015-12-18
    body: "<p>It's a Kubuntu Live Image</p>"
    author: "Contingente"
  - subject: "Excellent"
    date: 2015-12-18
    body: "<p>I am very glad for this news. Great job!</p>"
    author: "BenjaminReilly"
  - subject: "Me too"
    date: 2015-12-19
    body: "<p>\u207a\u00b9, man, I appeared here also out of curiosity for what distro that Live Disk is using. I guess they didn't mention because it is \u00abisn't ready for everyday use yet\u00bb\u00a9, hence things like package manager from their point of view doesn't really matter.</p>"
    author: "Hi-Angel"
  - subject: "No torrent?"
    date: 2015-12-19
    body: "<p>Is there an official torrent link for this image?</p>"
    author: "Andrei"
  - subject: "Does USB disk supplied with"
    date: 2015-12-19
    body: "<p>Does USB disk supplied with Plasma Wayland Live Image?</p>"
    author: "Anonymous"
  - subject: "Plasma "
    date: 2015-12-19
    body: "<p>Yes your right it is KDE Linux OS Not one of my favorit destops,&nbsp; though I'm goimg to download it and give it a spin,</p>"
    author: "Carling "
  - subject: "Christmas debugging incoming ayyyyy"
    date: 2015-12-19
    body: "<p>Congratulations and thanks<br /><br /></p>"
    author: "geexmmo"
  - subject: "I take it the image doesn't"
    date: 2015-12-19
    body: "<p>I take it the image doesn't work in a VM yet because of graphics driver issues? That begs the question which hardware is supported?</p><p>Until now, each time I tried a Plasma5 session on the real-silicon linux rigs I have access to, I got an image from the login manager, but only a black screen after logging in. There's no point in dl'ing over a Gb and recyling a stick drive if the end result will be more of the same...</p>"
    author: "RJVB"
  - subject: "There's a high chance it's a"
    date: 2015-12-19
    body: "<p>There's a high chance it's a Debian downsteam. Probably Ubuntu based. Kubuntu maybe. What are the odds you reckon ?</p>"
    author: "Anonymous"
  - subject: "Dev channel availability"
    date: 2015-12-19
    body: "<p>When can we expect this in the dev stream of Kubuntu?</p>"
    author: "Parithy Sivakumar"
  - subject: "Pity there isn't a x86 iso :("
    date: 2015-12-19
    body: "<p>Pity there isn't a x86 iso :(</p>"
    author: "Miler"
  - subject: "this distro is kubuntu willy"
    date: 2015-12-19
    body: "<p>this distro is kubuntu willy 15.10</p><p>&nbsp;</p>"
    author: "Fernando"
  - subject: "Its Kubuntu"
    date: 2015-12-20
    body: "<p>Haven't managed to successfully boot it yet though.</p>"
    author: "Lindsay Mathieson"
  - subject: "Operating System"
    date: 2015-12-20
    body: "<p>The underlying operating system is Kubuntu.</p>"
    author: "Ferdinand Thommes"
  - subject: "The OS"
    date: 2015-12-20
    body: "<p>It's Kubuntu</p>"
    author: "Bogdan"
  - subject: "It's not working properly"
    date: 2015-12-20
    body: "<p>It was just showing a lot of stripes for me (something like pixels weren't aligned properly).</p>"
    author: "Bogdan"
  - subject: "\u041f\u043b\u0430\u0437\u043c\u0430 \u043f\u0430\u0434\u0430\u0435\u0442 (("
    date: 2015-12-20
    body: "<p>\u041f\u043b\u0430\u0437\u043c\u0430 \u043f\u0430\u0434\u0430\u0435\u0442 ((</p>"
    author: "\u041a\u0430\u043f\u0438\u0442\u0430\u043d"
  - subject: "Video"
    date: 2015-12-20
    body: "<p>Video (bad quality cause I don't know how to record screencast in wayland) -&nbsp;https://www.youtube.com/watch?v=sX85Xqgk7D0</p>"
    author: "PaulRufous"
  - subject: "It is based on Ubuntu 15.10 "
    date: 2015-12-21
    body: "<p>It is based on Ubuntu 15.10 (x86_64):&nbsp;http://i.imgur.com/KGXwFiA.png</p>"
    author: "Xiao-Long Chen"
  - subject: "Its Kubuntu"
    date: 2015-12-21
    body: "<p>Which I posted days ago but which the comment system here seems to have lost.</p>"
    author: "Lindsay Mathieson"
  - subject: "UEFI incompatible?"
    date: 2015-12-23
    body: "<p>Thank you for your work and the early present! :)</p><p>However...</p><p>Is this image UEFI incompatible or is it just my laptop that doesn't like it? I tried the same USB disk on a friend's non-UEFI laptop and it boots without issues, so it should be fine.</p>"
    author: "Stfn"
  - subject: "When it boots it shows the"
    date: 2015-12-23
    body: "<p>When it boots it shows the Ubuntu logo, I guess it's a modified Kubuntu or something like that.</p>"
    author: "Stfn"
  - subject: "Kubuntu 15.10"
    date: 2015-12-26
    body: "<p>Kubuntu 15.10</p>"
    author: "Torbj\u00f8rn Nilsen"
  - subject: "Kubuntu"
    date: 2015-12-28
    body: "<p>Kubuntu</p>"
    author: "Ulath"
  - subject: "This does not work for me"
    date: 2015-12-28
    body: "<p>This does not work for me with an nVidia GTX970 card.&nbsp; Just a blank screen with a functional mouse cursor.</p><p>Nouveau not fully compatible with this card.&nbsp; Will wait and see what nVidia does with their new modesetting module.</p>"
    author: "q_entangled"
  - subject: "What comes to the final user?"
    date: 2015-12-28
    body: "<p>I can understand why a DEVELOPER could find it more \"confortable\" than the old, bloated, incoherent Xwindow. &nbsp;</p><p>But I totally fail to understand why on earth a USER should use it.</p><p>Wayland is not faster, is not stabler, has no network transparency, is incompatible with lot applications. It has no killer application, no \"wow\" feature, no compelling reason. And, last, but not least, we will probably have to keep both Wayland AND X for years, and years. Doubling the number of bugs and halving the number of developers.</p><p>All this, just for giving developers a new shiny environment to have fun with? Let's say it's ok, but why should I, the final user, care?</p>"
    author: "Vajsravana"
  - subject: "\u00abWork on this has been"
    date: 2016-01-02
    body: "<p><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20.796875px;\">\u00abWork on this has been ongoing since 2011 and is expected to take years rather than months before a completely transparent switch away from X will be possible\u00bb</span></p><p>&nbsp;</p><p>Almost 5 years of work and still devs will need several years more. Don't you think that when Wayland will be totally \"finished\", in 2020 or so, may be it will be born already obsolete?</p><p>I know that development pace in FLOSS world is as fast as is possible, but ten years in compurter science sounds like a century. What's the problem, man power? Could user contribute with donations to hire 35 or 40 week hours programmers?</p>"
    author: "Anonymous"
  - subject: "Great news!"
    date: 2016-01-05
    body: "<p>That is great news!&nbsp; Thanks Plasma team!&nbsp; I'm looking forward to leaving X11 behind :^)</p>"
    author: "Immotus"
  - subject: "Works on Virtual Box"
    date: 2016-01-08
    body: "<p>Hi this actually works quite well on VBOX even installs with vbox guest additions to full screen</p>"
    author: "CHCo"
  - subject: "I've been waiting for weeks"
    date: 2016-01-09
    body: "<p>I've been waiting for weeks for anyone to reply to your comment and it seems like no one is using KDE nowadays.</p><p>This LiveCD is based on Ubuntu.</p>"
    author: "birdie"
  - subject: "2016 trying to start a native plasma 5 desktop like gnome ..."
    date: 2016-01-25
    body: "<p>Using Arch OenGL v 3.1 &amp;&amp; EGL. wayland and xorg-server-xwayland installed. &lt;big&gt;How can i use wayland compositor on plasma 5 ? &lt;/big&gt;</p>"
    author: "krishnasut"
  - subject: "How can it be the.."
    date: 2016-03-22
    body: "<p><span style=\"text-decoration: underline;\"><a style=\"font-family: 'Open Sans Light', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 1.7em; line-height: 1.2em; color: #00407a; outline: 0px; outline-offset: -2px; text-decoration: underline;\" href=\"https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image\">First Plasma Wayland Live Image</a></span>,<span style=\"color: #204a87; font-family: 'Open Sans Light', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 1.7em; line-height: 1.2em;\">&nbsp;when KaOS release one 6 Months before ?<br /><br />I am really annoyed that the KDE team discriminate other Distros in favor for Kubuntu.<br /><br />So hard as Canonical beats with KDE team members, so similar seem to be their mind.</span></p>"
    author: "ShalokShalom"
---
<p><a href="/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png"><img style="text-align: center;" src="/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png" alt="" /></a></p><p>The Plasma team has been working on an early Christmas present: a live image running Plasma on Wayland.</p><p>Being able to run a full session of Plasma with applications is a major milestone in our aim of moving from the 30 year old X Window System to its replacement.</p><p>The central component in this is our window manager, KWin, which has moved from drawing borders on the edges of windows to running the full compositor and talking the Wayland protocols which allow applications to draw on screen and be interacted with.&nbsp;</p><p>Users of the image will notice some obvious glitches, it is certainly not ready for everyday use yet, but the advantages of more secure workspaces, easier feature extendibility and graphics free of tearing and gitches will be appreciated by everybody. Work on this has been ongoing since 2011 and is expected to take years rather than months before a completely transparent switch away from X will be possible.&nbsp; Find more about the project on the <a href="https://community.kde.org/KWin/Wayland">KWin Wayland wiki pages</a>.</p><p>Congratulations to Martin Gräßlin and the rest of the Plasma team on making this possible.</p><p>To try it out: download the live image, carefully use <code>dd</code> to copy it to a USB disk and reboot from that disk.&nbsp; Unfortunately it won't work from a virtual machine yet.</p><ul><li><a href="http://files.kde.org/snapshots/plasma-wayland-201512181217-amd64.iso">Plasma Wayland 20151218 Live Image</a> [1.2GB download]</li></ul><p>&nbsp;</p>