---
title: "Down to Business with Major Deployments"
date:    2015-10-06
authors:
  - "David Edmundson"
slug:    down-business-major-deployments
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/klogo-official-lineart_detailed-100.png" /></div>
KDE software has been used in many large scale deployments, including <a href="https://dot.kde.org/2010/10/07/jes%C3%BAs-torres-talks-about-bardinux-spains-biggest-deployment-kde-software">universities</a>, <a href="https://joinup.ec.europa.eu/community/osor/case/limux-it-evolution-open-source-success-story-never">governments</a> and countless companies.

One of these organizations suggested that KDE create a deployment forum so that others can benefit from their deployment experience. The forum would provide an opportunity for sysadmins and developers to ask questions and discuss problems/solutions related to deploying KDE software in large, complex environments.

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/DSC_0460.JPG" /></div>
We have created a new mailing list for these discussions at https://mail.kde.org/mailman/listinfo/enterprise.

All administrators of any deployments planned or in progress are invited to join this list to share and field questions. KDE developers and people maintaining Linux distributions with KDE software will also participate.
<!--break-->

