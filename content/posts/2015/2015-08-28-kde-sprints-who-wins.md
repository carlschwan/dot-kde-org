---
title: "KDE Sprints - who wins?"
date:    2015-08-28
authors:
  - "kallecarl"
slug:    kde-sprints-who-wins
---
We are <a href="https://www.kde.org/fundraisers/kdesprints2015/">raising money to support KDE sprints</a>. People have asked legitimate questions about those funds—who gets the money? Who benefits?

To start with, KDE sprints are intensive sessions centered around coding. They take place in person over several days, during which time skillful developers eat, drink and sleep code. There are breaks to refresh and gain perspective, but mostly sprints involve hard, focused work. All of this developer time and effort is unpaid. However travel expenses for <em>some</em> developers are covered by KDE. KDE is a frugal organization with comparatively low administrative costs, and only one paid person who works part time. So the money donated for sprints goes to cover actual expenses. <strong>Who gets the money? Almost all of it goes to transportation companies</strong>.

<a href="https://www.kde.org/fundraisers/kdesprints2015/"><img src="/sites/dot.kde.org/files/Fundraiser-Banner-2015.png" /><center><b>Donate to the KDE Sprints 2015 fundraising campaign</b></center></a>
<h2>Who benefits from contributions to KDE sprints?</h2>

That's more interesting.

Certainly KDE software is improved during KDE sprints, and innovations are planned and implemented. Developers are able to realize more of what they want their applications to be and do. They get to experiment. They get to have fun (if working on code many hours a day qualifies as “fun”). People who use KDE technology get the benefit of this effort and innovation. Some users claim that <a href="http://www.itworld.com/article/2976473/linux/kdes-plasma-54-the-most-advanced-and-beautiful-linux-desktop.html">KDE does things well</a>. Clearly sprint <strong>benefits go mostly to users</strong>, the millions of people all over the world who are using KDE technology.

<h2>However, that is not nearly the whole story.</h2>

Earlier this week on August 24th, Webkit had its 14th birthday. Although the Webkit name is trademarked by Apple, most of the original code came from KDE (in the form of the <a href="https://en.wikipedia.org/wiki/KHTML">KHTML</a> and <a href="https://en.wikipedia.org/wiki/KJS_(KDE)">KJS</a> libraries). Apple developers had <a href="http://marc.info/?m=104197092318639">high praise</a> for the work done by KDE developers. KDE's Free and Open Source software is available to anyone. The results of KDE sprints can benefit anyone. This shouldn't be taken lightly; KDE developers are some of the best in the world, and they work on important, current technology.

<a href="https://en.wikipedia.org/wiki/Qt_%28software%29">Qt</a> is another example of the largesse of the KDE Community. Ironically, KDE's choice of the Qt application framework helped to launch <a href="https://en.wikipedia.org/wiki/GNOME">GNOME</a>. The licensing controversies of those early days were resolved when the <a href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a> was established, providing strong foundations for both Qt and KDE Free and Open Source software, and demonstrating the ability of the KDE Community to adapt.

KDE and Qt continue to have a mutually beneficial relationship. KDE developers represent the most number of Qt contributors outside of Digia, which licenses Qt commercially. So the code and innovation that KDE developers add to the Qt codebase benefit Digia, Digia's commercial customers and the thousands of developers using Qt to develop opensource and proprietary applications. Improvements to Qt have a direct benefit for the development of KDE applications.

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/RandaWork.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/RandaWork525.JPG" /></a><br /><center><strong>Randa Meetings intensity</strong> &nbsp; <small>photo by Anne-Marie Mahfouf</small></center></div>

At a sprint in 2011, KDE developers took the first steps to modularize the extensive KDE development platform into frameworks. In July 2014, it was <a href="https://www.kde.org/announcements/kde-frameworks-5.0.php">announced that many KDE libraries were available</a> for use by any Qt developer. Quoting from the announcement:
<blockquote>
Frameworks 5 is the next generation of KDE libraries, modularized and optimized for easy integration in Qt applications. The Frameworks offer a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. There are over 50 different Frameworks as part of this release providing solutions including hardware integration, file format support, additional widgets, plotting functions, spell checking and more. Many of the Frameworks are cross platform and have minimal or no extra dependencies making them easy to build and add to any Qt application.
</blockquote>

Since then, more such Qt capabilities have been added, and there's an <a href="http://inqlude.org/">online archive of Qt resources</a> provided by the KDE Community.

KDE sprints provide value widely and freely to anyone who wants the results. If you are a computer user, you have probably already enjoyed benefits provided by KDE.

<h2>You Can Make a Difference</h2>
KDE is one of the leading Free Software projects in the world, thanks in large part to skilled, committed developers such as those at the <a href="http://randa-meetings.ch/">Randa Meetings in September</a>. You can make a big difference by contributing financially. Please <a href="https://www.kde.org/fundraisers/kdesprints2015/">donate</a> if you can. Share the responsibility, and the satisfaction of giving.