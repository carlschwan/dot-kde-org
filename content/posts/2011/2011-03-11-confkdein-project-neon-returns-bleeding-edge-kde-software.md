---
title: "conf.kde.in: Project Neon Returns With Bleeding Edge KDE Software"
date:    2011-03-11
authors:
  - "valoriez"
slug:    confkdein-project-neon-returns-bleeding-edge-kde-software
comments:
  - subject: "Kubuntu Natty = Awesome"
    date: 2011-03-12
    body: "I used to be a Kubuntu basher. The Spanish translations were badly broken, the package manager (Adept) was less than useless and there were very noticeable Kubuntu-specific bugs. But Muon and your new focus is gaining me.\r\n\r\nToday I run Kubuntu Natty. It's amazingly stable. It's more stable than Maverick. It's 100x more stable than Ubuntu Natty (KDE is an advantage, don't you think?). It has what I need. It hibernates flawlessly. It displays a beautiful console and splash screen, even when I'm running with the NVIDIA proprietary driver. It drives my touchscreen, and, although I have little issues with my wireless, they are solved by manually reloading the wl module. I have issues only with Bluetooth (while you seem to use a dev release of Bluedevil). Nothing major. And this is an ALPHA 3 RELEASE.\r\n\r\nIf you manage to do the unthinkable and go with a Plasma Unity Desktop Container... then you will be loved. Seriously. Go ahead. "
    author: "Alejandro Nova"
  - subject: "IDEM"
    date: 2011-03-12
    body: "IDEM!\r\n\r\nIt\u00b4s Project is AWESOMEEE!!!\r\n\r\nI love ubuntu and it's true what they say, the version of Kubuntu 4.11 Alpha 3 is much more stable than the Maverick and 100 times more than the 10.04\r\n\r\nApart from all the Ubuntu Documenting and quantity of packages, makes it unique! Distro easy to use and fully integrated end-user.\r\n\r\nGreetings!"
    author: "1antares1"
  - subject: "Thanks to Sheytan (Tomasz Dudzik) for his work on the Neon logo"
    date: 2011-03-13
    body: "Unfortunately, Sheytan was left out of thanks in the above article. Thanks to you, Sheytan for your fine work! You can thank him too, here, or on http://identi.ca/sheytan , and see more of his work on his blog: http://www.madsheytan.blogspot.com"
    author: "valoriez"
  - subject: "Suse"
    date: 2011-03-13
    body: "Hope they will be able to catch up with the amazing work of the OpenSuse project when it comes to provision of <a href=\"https://build.opensuse.org/project/show?project=KDE%3AUnstable%3ASC\">\"KDE unstable\"</a>"
    author: "vinum"
  - subject: "In my opinion it already has."
    date: 2011-03-16
    body: "In my opinion it already has. (Full disclosure, I'm a Kubuntu Dev) ;-)\r\n\r\nThe KDE unstable packages replace your existing KDE packages, while Project Neon packages install alongside stable packages, allowing you to experiment without risking a working KDE environment. :)"
    author: "JontheEchidna"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 0;"><img src="http://dot.kde.org/sites/dot.kde.org/files/L3N3i_0_0.png" /></div>
Announced today at conf.kde.in in Bengaluru, Project Neon is back, new and ambitious. Those of you who have been around KDE for a while might remember the old incarnation, which provided nightly builds of Amarok. Now the new generation of talented young Kubuntu developers announce that Project Neon is open for business!
<!--break-->
Project Neon provides nightly builds of the KDE Software Compilation trunk, with similar Amarok support also coming very soon. Project Neon is an easy way for new KDE contributors to get started without having to build the entire KDE-SVN tree and maintain the checkout. Additionally, dependencies are automatically handled and updated. This makes Neon suitable for a range of contributors such as new developers, translators, usability designers, documenters, promoters, and bug triagers. See <a href="http://techbase.kde.org/Getting_Started/Using_Project_Neon_to_contribute_to_KDE">the details on Techbase</a>. The developers also have <a href="https://wiki.kubuntu.org/Kubuntu/ProjectNeon">an active set of wiki pages</a> for those who want to help with the project. The project <a href="https://launchpad.net/~neon">uses the Launchpad infrastructure</a>; the IRC channel is #project-neon on Freenode. Some screenshots are on the wiki.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex" />
<img src="http://dot.kde.org/sites/dot.kde.org/files/neon-announce.jpg" width="240" height="180" /><br />
Announcing Project Neon at conf.kde.in Today
</div>
According to Project Neon developers, running it will require latest stable or development release of Kubuntu. They say that it is possible to port Project Neon to other distributions, though there are currently no maintainers for other distributions working on the project. openSUSE also provide their own weekly build of the KDE source trunk.

Kudos to <a href="https://launchpad.net/~yofel">Philip Muškovac</a> (yofel), <a href="https://launchpad.net/~quintasan">Michał Zając</a> (quintasan), <a href="https://launchpad.net/~rohangarg">Rohan Garg</a> (shadeslayer), and <a href="https://launchpad.net/~gaurav-2004">Gaurav Chaturvedi</a> (tazz) for their accomplishment! And thanks to Sheytan (Tomasz Dudzik) for the great artwork. Branding is important, and the developers of this project appreciate it.