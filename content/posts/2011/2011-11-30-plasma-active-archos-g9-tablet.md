---
title: "Plasma Active on Archos G9 tablet"
date:    2011-11-30
authors:
  - "kallecarl"
slug:    plasma-active-archos-g9-tablet
comments:
  - subject: "3G Ability+"
    date: 2011-12-01
    body: "Nice to see progress but linux really needs to get 3G/4G ability along with KDE/PLasma-Active.\r\n\r\nI want to run desktop type apps on a tablet, that has the ability to receive phone calls, GPS, wifi, camera (to scan barcode), fast enough to play 3D games, and... long battery life,say like 14 hours.\r\n\r\nThe ASUS Prime has the hardware, but Android is the part let down.  Mayeb if KDE/Gnome was on Android and allowed 100% KDE apps to run then it maybe acceptable."
    author: ""
  - subject: "How install ."
    date: 2011-12-02
    body: "How did you install Plasma and Mer on the Archos Gen 9\r\n\r\nI write a news in France : http://www.jbmm.fr/?p=23945\r\n\r\nBest regards"
    author: ""
  - subject: "Yes, please, share the"
    date: 2011-12-02
    body: "Yes, please, share the information on how to install it on G9.\r\n\r\nThis news is also published in Russian."
    author: ""
  - subject: "For those asking how to install"
    date: 2011-12-04
    body: "This should be available soon\u2122 as the usual SDE 'firmware' for gen9.\r\nWatch the Archos SDE page for updates."
    author: ""
  - subject: "Any idea, when \"soon\" will"
    date: 2011-12-18
    body: "Any idea, when \"soon\" will be?"
    author: ""
  - subject: "Archos "
    date: 2012-05-17
    body: "Hi\r\n\r\ni have the SDE of Archos, how install plasma active ?"
    author: ""
  - subject: "Forums are best"
    date: 2012-05-17
    body: "forum.kde.org > KDE Software > Plasma Active\r\nopentablets.org\r\n\r\nThe Plasma Active developers follow these forums. These are the best resources for getting Plasma Active running while making your experience available to other people."
    author: "kallecarl"
---
<a href="http://plasma-active.org/">Plasma Active</a> and <a href="http://merproject.org/">the Mer OS</a> have been installed successfully on the <a href="http://archos.com/">ARCHOS G9 tablet</a>.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ARCHOSMerScreenShot.jpeg" /><br />Plasma Active & Mer on ARCHOS tablet</div>

Plasma Active is innovative technology for a smarter mobile user experience—the latest member of the Plasma Workspaces family from KDE. The touchscreen interface is more than just an application launcher. Rather than the commonplace grid of applications, an Activity view shows a single project, task or idea, gathering all related documents, people, web sites, media and widgets. By creating multiple Activities, a mobile device can be customized to respond effectively as the user moves between contexts.  Plasma Active is based on Qt and KDE technology, enabling quick and efficient development of touchscreen user experiences.

Mer is Free and Open Source Software being developed in the open based on work from the Core section of the MeeGo Project. It is optimized for HTML5/QML/JS and provides a mobile-optimized base Linux distribution for use by device manufacturers.

ARCHOS G9 tablets feature Texas Instruments ARM Cortex™ A9 based OMAP™ 4 smart multi-core processors along with GPS, WiFi and the ability to add a standard PC 3G stick. The ARCHOS tablets are interesting for everyday use and make an affordable development target. ARCHOS provided development devices as well as technical support to get Mer and Plasma Active up and running on the G9 devices.

The Plasma Active project team is in the process of stabilizing, rendering and optimizing images and is planning to make them available for wider testing soon. The ARCHOS™ GPL sources are already available online at https://gitorious.org/~archos

A demo of an ARCHOS G9 tablet running Plasma Active is being shown by <a href="http://www.basyskom.com/">basysKom</a> at Qt DevDays in San Francisco November 29th - December 1st. Stop by the booth to experience Plasma Active on the new ARCHOS G9.
