---
title: "Schedule announcement for Camp KDE 2011"
date:    2011-03-08
authors:
  - "Justin Kirby"
slug:    schedule-announcement-camp-kde-2011
comments:
  - subject: "Camp KDE"
    date: 2011-03-12
    body: "Wish I could attend but I look forward to reading about the highlights, especially KDE on mobile.\r\n"
    author: "jfinguy"
---
This year's event will feature keynotes from Jim Zemlin, the Executive Director of Linux Foundation, as well as Carol Smith, program administrator for Google Summer of Code and Google Code-in. We also have a full array of exciting talks organized into four tracks, plus a panel discussion and hacking sessions. 

<a href="http://dot.kde.org/sites/dot.kde.org/files/CampWorkandPlay.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/CampWorkandPlay_small.jpeg" width="250" height="166" align="right" /></a>

Don't forget to visit the <a href="http://camp.kde.org">Camp KDE site</a> where you can register to attend and keep up with the latest event information.  

<h2>Monday, April 4</h2>

<h3>Keynote</h3> 
<strong>Jim Zemlin, Linux Foundation:</strong> The State of the Linux Union

<h3>Track: KDE on Mobile</h3>
<strong>Frank Karlitschek:</strong> Why ownCloud is important for KDE
<strong>Romain Pokrzywka:</strong> KDE on Mobile, with Kontact Touch
<strong>John Layt:</strong> Geolocation Services in KDE

<h3>Track: Personal Information Management</h3>
<strong>Alvaro Soliverez:</strong> Alkimia, a Framework for Personal Finance Applications
<strong>Marijn Kruisselbrink:</strong> Calligra Introduction
<strong>Rob Oakes:</strong> Writing and Publishing with Open Source Tools


<h2>Tuesday, April 5</h2>

<h3>Keynote</h3>
<strong>Carol Smith</strong>, Google Open Source Programs Office

<h3>Panel: KDE in Education:</h3>
Carol Smith, Knut Yrvin, and Aleix Pol

<h3>Track: Qt in KDE</h3>
<strong>Thiago Macieira:</strong> Qt Open Governance Progress
<strong>Ariya Hidayat:</strong> Introduction to QtWebKit
<strong>Ariya Hidayat:</strong> Efficient Graphics with Qt: Beautiful and Blazing Fast

<h3>Track: Developing KDE software</h3>
<strong>Aleix Pol:</strong> KDevelop
<strong>Vincent Batts:</strong> Slackware: Quickly and Easily Manage Your KDE Hacking
<strong>Knut Yrvin:</strong> KDE Contributions to Qt

<h3>Hacking Sessions:</h3>
Once the talks are over, the rest of Tuesday will be available for hacking and discussing - in short, a hallway-track-on-stereoids. This is great fun: we will split up in teams according to interests and discuss those topics in personal. You get to talk to the core KDE developers in person, contribute your thoughts and be a part of the future of KDE!

<h2>Camp KDE - Exciting!</h2>
In short, Camp KDE 2011 is gearing up to be as exciting, if not more, than the previous Camp KDE events. Learn and play go hand in hand and each of the Camp KDE meetings has left its visitors with many tales to tell. Great ideas and plans have originated at Camp's, awesome work was done and new friends were made. A grassroots event like this certainly counts among the best Free Software has to offer - so if you live in the area, be there!