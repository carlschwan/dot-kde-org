---
title: "KDE Commit-Digest for 27th November 2011"
date:    2011-12-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-27th-november-2011
---
In <a href="http://commit-digest.org/issues/2011-11-27/">this week's KDE Commit-Digest</a> Andrew Lake writes about the Bangarang project and recent improvements. As usual there is also a list of updates: 

<ul><li>Kdelibs change that improves storage of passwords in a webdav share</li> 
<li>Folders panel fixes in <a href="http://dolphin.kde.org/">Dolphin</a></li> 
<li><a href="http://www.calligra-suite.org/">Calligra</a> sees updated support for DOCX and PPT and many bugfixes</li> 
<li>Share URL by mail action and remote ftp sync in <a href="http://userbase.kde.org/Rekonq">rekonq</a></li> 
<li>Optimization of map themes load in <a href="http://edu.kde.org/marble/">Marble</a></li> 
<li>Bugfixes in <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://userbase.kde.org/Dolphin">Dolphin</a>, <a href="http://community.kde.org/Plasma/Active">Plasma Active</a>.</li> 
</ul> 

<a href="http://commit-digest.org/issues/2011-11-27/">Read the rest of the Digest here</a>.