---
title: "KDE Commit-Digest for 14th August 2011"
date:    2011-08-19
authors:
  - "mrybczyn"
slug:    kde-commit-digest-14th-august-2011
comments:
  - subject: "Thank you"
    date: 2011-08-19
    body: "Saying the same thing again and again sometimes feels a little stupid - but I think it has been a while since someone said this.\r\nSo:\r\n\r\nThank you, guys, for compiling the digests week after week!"
    author: "onety-three"
  - subject: "+1, always a great read!"
    date: 2011-08-21
    body: "+1, always a great read!"
    author: ""
  - subject: "Konsole 1.6.6"
    date: 2011-08-21
    body: "Konsole 1.6.6 can use images as background... Was konsole recoded for kde 4?"
    author: ""
  - subject: "Indeed"
    date: 2011-08-22
    body: "Thank you, I'm sure there are thousands and thousands of happy eyes reading you each times :)"
    author: "DanaKil"
  - subject: "It's back in 4.8, see"
    date: 2011-08-26
    body: "It's back in 4.8, see http://www.youtube.com/watch?v=8oPP4nyWOs4&feature=related"
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-08-14/">this week's KDE Commit-Digest</a>:

              <ul><li>The refactoring of <a href="http://api.kde.org/4.0-api/kdelibs-apidocs/">kdelibs</a> for Frameworks 5 has started</li>
<li>There have been much work in the <a href="http://userbase.kde.org/Kate">Kate</a> printing module, the changes include an attempt to work around Qt printing bugs, fixes in page numbering and others</li>
<li><a href="http://userbase.kde.org/Konsole">Konsole</a> can use images as background</li>
<li><a href="http://userbase.kde.org/Kate">Kate</a> support for different syntax format has been extended and now includes also Verilog 2001, Maya MEL and DOS batch files and some other formats have been updated</li>
<li><a href="http://userbase.kde.org/KMail">KMail</a> can search by date</li>
<li>Performance optimization in <a href="http://www.digikam.org/">Digikam</a>, <a href="http://community.kde.org/KDE_PIM">KDE PIM</a> and <a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a></li>
<li>Basic keyboard navigation in Icons and Compact View in <a href="http://dolphin.kde.org/">Dolphin</a> has been implemented</li>
<li>Bug fixes in multiple applications including <a href="http://www.calligra-suite.org/">Calligra</a>, <a href="http://userbase.kde.org/Kate">Kate</a>, <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-08-14/">Read the rest of the Digest here</a>.