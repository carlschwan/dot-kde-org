---
title: "Your Chance to Win KDE!"
date:    2011-04-01
authors:
  - "Stuart Jarvis"
slug:    your-chance-win-kde
comments:
  - subject: "Holy crap! This is awesome!"
    date: 2011-04-01
    body: "Holy crap! This is awesome! \"access to future updates and the expertise of a widely respected worldwide network of developers, translators, documentation writers, promoters, artists and more.\"\r\n\r\nThis is the most awesome news!\r\n/me takes part.\r\n\r\n[P.S. :- Will /me get a chance to yell BINGO at the end? ]"
    author: "tazz"
  - subject: "Nice try!"
    date: 2011-04-01
    body: "April Fools' day, right?"
    author: "johanner"
  - subject: "Some better joke announcements would have been:"
    date: 2011-04-01
    body: "-reliable video-chat support in Kopete!\r\n-KDEPIM4 now works!\r\n-All bugs affecting Dolphin were fixed!\r\n-(the list can go on and on...)\r\n\r\n\r\n"
    author: "trixl."
  - subject: "Free Software Offer"
    date: 2011-04-01
    body: "Reminds me of my first Akademy in Ludwigsberg where a waitress at the nearest cafe was getting curious at what we were all doing here, and after quickly explaining KDE to her, someone offered her a free CD with KDE on it. She was obviously pleased with a free CD, and Seigo expanded the offer (paraphrased from memory):\r\nAaron: And you can use that software for anything you like at no cost, or obligation to us. \r\nWaitress: Oh?! thanks!\r\nAaron: And that is not all! You are also free to give the software or copies of the software to your friends and family.\r\nThe waitress seemed even more pleased, but slightly sceptical: Really?? Thank you...\r\nAaron: As long as you pass on the same rights to them as we have passed on to you.\r\nAt that point the waitress eyes seemed to glass over, and she quickly excused herself and moved on the next table.\r\n\r\nSome offers just seems too good to be true, and KDE is one of them :)"
    author: "carewolf"
  - subject: "Yep, kinda"
    date: 2011-04-01
    body: "But the offer is real: <a href=\"http://jointhegame.kde.org/\">Join the Game</a> and you get access to all our software forever and help to create it too."
    author: "Stuart Jarvis"
---

The international KDE community today announced that it will offer itself up in an exciting lottery. Even though the prize is far more substantial than that offered in <a href="http://en.wikipedia.org/wiki/Spanish_Christmas_Lottery">El Gordo</a>, the entry fee is a mere &euro;100, which can be spread over a year (equating to less that &euro;2 per week).

Those taking part will not only get the chance to win access to the current full catalog of KDE software, <a href="http://www.ohloh.net/p/kde">valued in the millions of dollars by independent experts ohloh</a>, but also access to future updates and the expertise of a widely respected worldwide network of developers, translators, documentation writers, promoters, artists and more.

<h2>Community reaction</h2>

KDE e.V. treasurer, <b>Frank Karlitscheck</b> expressed his incredulity at the competition:

<blockquote>It's a truly staggering prize. I cannot believe that we are giving people the chance to get all of this for so little. It just makes no sense, but the rest of KDE e.V. board overruled me.</blockquote>

Others were far more relaxed about the plan. Notably, the KDE e.V. business manager, <b>Claudia Rauch</b>, sees only upsides:

<blockquote>I think this will work, due to the unique way that KDE software development is funded, our costs are actually pretty low. Really, the main ones are getting people together for sprints and conferences. We always see great results from that and even a single entry to this lottery could pay for a contributor to travel to such an event.</blockquote>

KDE promoter <b>Justin Kirby</b> joined the chorus of approval:

<blockquote>Frankly, I think this is genius. It will raise the profile of KDE and our software substantially. Also, the cost of entry is low and I've heard that we've had less than 500 entries so far so the chance to win is quite high!</blockquote>

<h2>Enter now for your chance to win access to great software!</h2>

Do you want to have the chance to gain access to KDE software for free? To get access to future updates for free? To know that by entering the lottery you will also be helping to make our software even better, even if you do not have the time to contribute in other ways?

If you answered yes to any of the above, don't delay, <a href="http://jointhegame.kde.org/">get your entry in today and Join the Game</a>. The best news is that everyone is a winner.