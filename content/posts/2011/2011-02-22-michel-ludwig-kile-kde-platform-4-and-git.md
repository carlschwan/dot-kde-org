---
title: "Michel Ludwig on Kile, KDE Platform 4 and Git"
date:    2011-02-22
authors:
  - "vanloon"
slug:    michel-ludwig-kile-kde-platform-4-and-git
comments:
  - subject: "Thanks Michel for all your"
    date: 2011-02-22
    body: "Thanks Michel for all your work!\r\n\r\nI am using Kile for quite some time now and it helped me a lot.\r\n\r\nAlso 2.1 beta 5 is quite stable so far.\r\nEither I have been pretty lucky or I did not find any bugs which hindered me in using it. :)"
    author: "mat69"
  - subject: "thumbs up!"
    date: 2011-02-22
    body: "thank you for giving us (uncounted [phd] students) kile!\r\n\r\nwrote my thesis on kile 2.1b4 without a crash. spellchecking wasn't the best, but that's what friends are for ;)"
    author: "freinhard"
  - subject: "Emacs Auctex and Kile"
    date: 2011-02-23
    body: "Kile needs to learn from auctex. The editing process is way too slow. In Kile the access to LaTeX features if done mostly via menus and only a few have keyboard shortcuts. Furthermore when it's possible to input options, this is done via a big wizard that blocks the view of your text. While this is useful for beginners, once you know how the commands work it's just quicker to type everything down yourself. \r\n\r\nIn auctex you can easily access features using the keyboard (emacs commands), and any options are introduced in a non-intrusive way in the command buffer at the bottom of the editor. This does save plenty of time. There's quite a few other things that I'd miss if I were to move, but undoubtedly this would be the most important one. "
    author: "jadrian"
  - subject: "KDevPlatform/KDevelop"
    date: 2011-02-23
    body: "@Michel:\r\nHave you considered using (or do you already use) KDevPlatform in your application?\r\nYour program is very similar to KDevelop (but it handles LaTeX instead of code), so I think the two applications could share some code. That code should reside in the KDevPlatform library.\r\nQuanta uses KDevPlatform, too, so it's not a KDevelop-only library.\r\n"
    author: "poletti.marco"
  - subject: "Kile = lifesaver"
    date: 2011-02-23
    body: "One of the programs that make my life (as a phd student) much easier every day. Your work is very much appreciated!\r\n\r\nDidn't know there was only one developer; will try to find time \r\nsomewhere to look at the tasks and code to see if I can help.\r\n\r\nKeep up the wonderful work!"
    author: "talavis"
  - subject: "ConTeXt"
    date: 2011-02-23
    body: "will you add support for ConTeXt and XeTeX\u2019s unicode-math, too?\r\n\r\nthis will consist of two parts:\r\n<ol><li>change the symbol pane to input unicode characters instead of macros if the currently used TeX flavor supports it (i.e. is XeTeX or ConTeXt)</li>\r\n<li>ad a ConTeXt syntax highlighting scheme (i have a rudimentary one, but i will share it if someone improves it, since it lacks features)</li></ol>"
    author: "flying sheep"
  - subject: "Good work"
    date: 2011-02-23
    body: "I use kile now and then. Even though it's far from perfect, it has been a great help. Thank you for working on it.\r\n\r\nHow can you improve it? \r\nWell, you could begin by simplifying its interface. There are lots of options that nobody uses. For example the entire menu \"latex\" and \"wizard\" are of no use, since people using Latex already have an idea of what they're doing and the latex and symbols panel on the left already offers all that an more. \r\n\r\nSurely the most important thing missing is a good and intuitive spellchecker (like the one in openoffice and firefox). I don't know why this desperately needed feature has been procrastinated so much. Is it so difficult to implement?\r\n\r\nAnother thing missing is support for glossaries and acronyms, but this isn't really important.\r\n\r\nFinally, when kile autocompletes tables and expressions, it puts an unicode X inside the empty fields. I think this choice was very poor because it causes the document to stop compiling until you remove the X. "
    author: "trixl."
  - subject: "Kile is cool"
    date: 2011-02-23
    body: "Thank you very much for the program! I have been using it for five years, and Kile helped me a lot. Glad to know that it's evolving!"
    author: "nikita-melnichenko"
  - subject: "Tasks for new developers"
    date: 2011-02-23
    body: "Is there a list of taks that could be taken over by other developers?\r\n\r\nAlso, have you considered events such as Google Summer of Code to produce enhancements to kile?\r\n\r\nThanks for such a great program!"
    author: "nplatis"
  - subject: "Re: Emacs Auctex and Kile"
    date: 2011-02-23
    body: "Besides auto-completion, Kile also offers automatic expansion of abbreviations. Furthermore, if you want the ultimate customisation of the editing process (with possible input options), you can also use scripts which you can either bind to some specific text input of to keyboard shortcuts."
    author: "mludwig"
  - subject: "Re: KDevPlatform/KDevelop"
    date: 2011-02-23
    body: "No, Kile doesn't use the KDevPlatform yet. It might be interesting to switch to it at some point, but this probably won't happen in the near future.\r\n\r\n(Also, I wouldn't call Kile \"my\" application as many other people have contributed a lot of code to it ;-)"
    author: "mludwig"
  - subject: "Re: ConTeXt"
    date: 2011-02-23
    body: "At the moment, Kile has some rudimentary support for ConTeXt and XeTeX in the form of predefined build tools. But, this will probably be extended in the future.\r\n\r\nIf you have some particular feature requests, you can file them as feature wishes in the KDE bug tracker at http://bugs.kde.org (some wishes for extended XeTeX support are already there)."
    author: "mludwig"
  - subject: "Re: Good work"
    date: 2011-02-23
    body: "Well, I wouldn't say that the \"LaTeX\" and \"Wizard\" menus are useless. I happen to use them myself from time to time :-)\r\n\r\nHave you tried Kile recently on KDE SC 4.4, 4.5, or 4.6? KatePart (and thus Kile) now also has on-the-fly spell checking.\r\n\r\nRegarding the \"unicode X\", this is called a bullet in Kile terminology. It can be used to quickly navigate to different parts of the document. But if you don't want to have bullets inserted, you can disable them in the code completion configuration page of the configuration dialog."
    author: "mludwig"
  - subject: "Re: Tasks for new developers"
    date: 2011-02-23
    body: "For starters, one could take a look at the bug and feature request lists in the KDE bug tracker. However, before starting with the coding, I'd send an e-mail to the development mailing list of Kile, just to make sure that no one else is working on that particular bug already.\r\n\r\n"
    author: "mludwig"
  - subject: "Hi Michel thanks for your"
    date: 2011-02-25
    body: "Hi Michel thanks for your answer. I appreciate all that, but let me try to explain my thoughts on it. \r\n\r\nConsider the task, using serif font in math mode, i.e. command \\mathsf{...}. In emacs you do C-c C-f C-f, (3 strokes). In Kile, by the time you typed \\mat (4 strokes) you have a list with 32 entries, with the one you want being 25th. If you get to \\maths then you have 2 entries so you can go down and press enter (8 strokes total). But 8 strokes is enough to type \\mathsf{ so in the end you might as well just write everything. Not to mention that in emacs you can, for instance, apply a font to a text selection using the same keystrokes, in kile this would have to be user implemented using scripts.\r\n\r\nThis is evident with  lots of other tasks. In emacs, to insert an environment you'd do C-c C-e. Then you get a minibuffer on the bottom to type the environment you want, with the last selected by default. If you want to use the default one press enter (3 strokes). Otherwise you start writing the one you want and then you can auto-complete with tab. In kile you get a huge auto-complete list after \\beg (4 strokes), if you want to get more specific you have to continue writing \\begin{ (7 strokes) and only now you can trim the list down. \r\n\r\nRegarding abbreviations and user scripts, these can be nice. But someone still has to implement them. That possibility should be an extra, not required for core functionalities. \r\n\r\n\r\nSo I do understand your arguments. I also believe that I'm biased because I'm so used to emacs. But in my experience it does seem to me that you'd have to type way more in kile to get the same job done. "
    author: "jadrian"
  - subject: "Kile 2.1 beta x"
    date: 2011-02-24
    body: "Not quite sure why all the fuss from some people.\r\n\r\nI could write my latex with vim and convert to pdf in terminal, and I also could use kate with my own makefile. There are many other ways in which I could do my latex work, but every time I come back to kile. Kile is comfortable, easy & stable, which is a tall order indeed. I've had no problems with 2.1, which is quite something for betas.\r\n\r\nMichel should be very pleased."
    author: "grigore.moldovan"
  - subject: "wonderful thanks!"
    date: 2011-02-24
    body: "wonderful thanks!"
    author: "trixl."
  - subject: "Re: Emacs Auctex and Kile"
    date: 2011-02-26
    body: "Just for the sake of completeness, let me point out that there is a (configurable) keyboard shortcut in Kile for inserting environments: it inserts \"\\begin{\" and then the environment completion list is automatically shown."
    author: "mludwig"
  - subject: "Ah, that is good indeed."
    date: 2011-03-01
    body: "Ah, that is good indeed. Thanks. "
    author: "jadrian"
  - subject: "Thanks for your work"
    date: 2011-03-21
    body: "Kile is a great tool :-)"
    author: "lisandropm"
---
<p>After investigating the work being done on <a href="http://dot.kde.org/2010/10/19/thomas-fischer-kbibtex-kde-reference-manager">KBibTeX</a> a few months ago, we turn our attention to <a href="http://kile.sourceforge.net/">Kile</a>, KDE's LaTeX and TeX editor. LaTeX is a document markup language and document preparation system built on top of the typesetting system TeX. It is frequently used for scientific publications as an alternative to word processors.</p>

<p>There is no stable Platform 4 version of Kile yet, but beta releases are already available and a stable release is not far away. <b>Alexander van Loon</b> took the opportunity to ask <b>Michel Ludwig</b> how the next version of Kile is shaping up. At this moment, Michel is the sole developer working on Kile.</p>
<!--break-->
<h2>Michel Ludwig talks about Kile's development</h2>

<p><strong>Alexander</strong>: To start, can you tell us what makes Kile special compared to other LaTeX editors?</p>

<p><strong>Michel</strong>: Over the years Kile has accumulated an impressive number of features that can help with most aspects of LaTeX (and TeX) editing. Kile also has a highly configurable LaTeX tool launching system, a quick preview feature and scripting capabilities. As a KDE application, it can benefit from all the features that the excellent Kate editor has to offer, e.g. on-the-fly spell checking in particular. Kile can also make use of other helpful KDE features such as network-transparent file editing or embedding the Okular viewer for DVI, PS and PDF files. Finally, thanks to the KDE on Windows team, Kile can now also <a href="http://sourceforge.net/apps/mediawiki/kile/index.php?title=KileOnWindows">run natively</a> on the MS Windows platform.</p>

<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kile1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kile1_small.jpg"/></a><br />Autocompletion (click image for larger version) </div>

<p><strong>Alexander</strong>: How is the effort to port Kile to KDE Platform 4 going, is it difficult to port?</p>

<p><strong>Michel</strong>: The porting to the new KDE Platform is essentially complete now. However, large parts of Kile had to be rewritten. The autocompletion functionality is an example; it is now based on a much more powerful API so the autocompletion feature will be even more helpful in the future. The biggest obstacle during the porting was that some parts of the Qt 4 and KDE Platform API are fundamentally different from previous versions, and Kile had to be adapted to the new API.</p>

<p><strong>Alexander</strong>: You have been occupied with porting for some years now and except for some others who contribute patches you are the only developer. Do you need help?</p>

<p><strong>Michel</strong>: Actually, until a couple of months ago, I was assisted by another developer, who then had to stop his involvement with Kile due to personal reasons. Since then I am indeed the only developer of Kile. I am not satisfied with the current development speed, but there is not much I can do to improve the situation as I also have other commitments. So, I think that having additional active developers around would be a tremendous help. There are a couple of tasks available that would be ideal for new developers to familiarize themselves with the code base. In addition, for those that are not familiar with coding, some help with improving the documentation of Kile is also
needed.</p>

<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kile2.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kile2_small.jpg"/></a><br />Symbol insertion </div>

<p><strong>Alexander</strong>: What needs to be done before a stable version of Kile for Platform 4 can be released?</p>

<p><strong>Michel</strong>: There is not much that still has to be done before Kile 2.1 can be released, except for fixing a few minor bugs and polishing the documentation a bit. The latest version, 2.1 beta 5, is already stable and bug-free enough for daily use. I recommend that anyone who is still using Kile 2.0 should upgrade to version 2.1 beta 5, and check whether you can still find any bugs that have not been reported yet.</p>

<p><strong>Alexander</strong>:  What are the plans for the future after the next release?</p>

<p><strong>Michel</strong>: After Kile 2.1 has been released, I would like to work on a few features that have been requested for a long time. Foremost, I think that the LaTeX parser that Kile uses will have to be optimized and made faster. This would also help correct a few bugs in the autocompletion functionality that are not so easy to fix at the moment. In addition, the ability to split the editor views and to have an integrated viewer for DVI, PDF, etc. alongside the LaTeX markup would also be interesting features for Kile. Then, the quick preview feature could also be improved by turning it into a live preview, and the scripting functionality could be extended by providing downloadable scripts. People can also check the feature wish list for Kile, which has grown considerably over the last years. It has numerous interesting suggestions for new features.

<p><strong>Alexander</strong>: Kile recently migrated to KDE's Git infrastructure. Do you like the change? (Previously, Kile resided in KDE's Subversion repository)

<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kile3.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kile3_small.jpg"/></a><br />Kile table editor </div>

<p><strong>Michel</strong>: Git is a very powerful tool with numerous features that will undoubtedly help a lot in the development process of KDE software projects. But probably as a consequence of its wealth in features, Git can sometimes be complicated to use in my opinion. Also, at times it does not seem to protect the users enough from their mistakes (e.g., it is possible to perform actions that leave the repository in a broken state, from which recovery may be difficult for novice users). However, as Git evolves, I am sure that its user interface will improve as well.

<p>Additionally, I particularly like KDE's new development infrastructure around Git. The interconnection of the different websites like KDE Projects, KDE Identity and Review Board should bring a productivity boost to KDE development.

<p><strong>Alexander</strong>: Kile uses KDE's bug tracker and its Git repository, why not use KDE's infrastructure for the website, wiki and mailing lists instead of using the services of SourceForge?

<p><strong>Michel</strong>: Kile uses the hosting services for web pages and mailing lists of SourceForge purely for historical reasons. Kile started out using those services offered by SourceForge, but there has been no incentive to change this so far; SourceForge's hosting services currently fulfill the needs of the project. However, for bug tracking, for instance, it is more useful indeed to use KDE's facilities as this allows for a closer interaction with other KDE projects.