---
title: "KDE Releases Second Alpha of Necessitas"
date:    2011-06-01
authors:
  - "jonan"
slug:    kde-releases-second-alpha-necessitas
comments:
  - subject: "Good news"
    date: 2011-06-01
    body: "This is great news. I'm glad Necessitas has found a good home here with KDE. I'll be following development closely."
    author: ""
  - subject: "I like it"
    date: 2011-06-01
    body: "I've been waiting for somebody to get Qt on Android, and the fact that this is now a KDE project make it even more awesome!\r\n\r\nWho am I kidding :) with a name like \"Necessitas\" it has to be a KDE project (cuz it complies with all the Horrible Naming Conventions required to become a KDE project) - hehehe kidding (or am I ;-))"
    author: "gamaral"
  - subject: "Congratulations"
    date: 2011-06-02
    body: "Congratulations!\r\n\r\nI am sorry that I bring this up, but I really hope that you can remove the communism sign from Necessitas' current logo, because in certain countries, this sign has the same meaning as Nazi, in particular, for example, China (I am a Chinese).\r\n\r\nIf you look at the world, so called socialism countries are the major sources of internet censorship, which goes against with the spirit of being free and accessible.\r\n\r\nIt may not be appropriate to bring up this political argument here. My apologize. I just want say that I felt really uncomfortable at the Necessitas' logo. \r\n\r\nThank you very much."
    author: ""
  - subject: "New logo maybe"
    date: 2011-06-02
    body: "Necessitas could probably use an entirely new logo in fact - I'm not sure how cool Google will be with the use of the Droid part too, leaving aside problems with the hammer and sickle part.\r\n\r\nMaybe Bogdan would be interested in a contest, I'll ask him."
    author: "Stuart Jarvis"
  - subject: "Qt logo"
    date: 2011-06-02
    body: "I might be a version of the Qt logo you that you interpret to be the hammer and the sickle. To me it looks like the letters Q and t :-)"
    author: ""
  - subject: "Yep"
    date: 2011-06-02
    body: "It's a rework of the old-style Qt logo"
    author: "Stuart Jarvis"
  - subject: "China is the most capitalist"
    date: 2011-06-02
    body: "China is the most capitalist country on earth, my dear. And, no, I haven't heard of Venezuela, Brasil or Ecuador censoring the net,\r\n\r\nI love the logo, BTW."
    author: ""
  - subject: "Google explicitly said you"
    date: 2011-06-02
    body: "Google explicitly said you can use the android mascot freely. You can't use the android logo text or font though."
    author: ""
  - subject: ";)"
    date: 2011-06-02
    body: "Or in Kerala, West Bengal or Tripura. ;)"
    author: ""
  - subject: "Move to KDE repositories?"
    date: 2011-06-02
    body: "Will it be moved to git.kde.org, listed at projects.kde.org, wiki moved to techbase/userbase etc. soon? "
    author: ""
  - subject: "Ok, cool"
    date: 2011-06-03
    body: "I hear a new logo will be coming anyway, but maybe that means the droid - but not the hammer and sickle ;-) - can be incorporated somehow..."
    author: "Stuart Jarvis"
  - subject: "You are confusing \"communism\""
    date: 2011-06-03
    body: "You are confusing \"communism\" with \"socialism\"... [objectionable comment removed - Ed]"
    author: "trixl."
  - subject: "That is an unacceptable"
    date: 2011-06-04
    body: "That is an unacceptable comment, please refrain from this or you will be banned. Please read the Code of Conduct at http://www.kde.org/code-of-conduct/\r\n\r\n[Edited to correct link - Ed]"
    author: "odysseus"
  - subject: "It is in git.kde.org and on"
    date: 2011-06-04
    body: "It is in git.kde.org and on projects, you need to look in playground/mobile"
    author: "odysseus"
  - subject: "orly?"
    date: 2011-06-06
    body: "Necessitas's sourceforge page still links to the gitorious repos :("
    author: ""
  - subject: "Is it available for"
    date: 2011-06-07
    body: "Is it available for kwordquiz? "
    author: "trixl."
  - subject: "Necessitas homepage"
    date: 2011-08-24
    body: "Is there an official homepage for the project, now that it is under KDE? Sourceforge page seems to be quiet...it would be nice if there were a page so that one could get information on updates, what's going on etc."
    author: ""
---
<p><a href="http://sourceforge.net/p/necessitas/home/">Necessitas</a> is the project bringing Qt to the Android operating system. It has recently become part of KDE and now celebrates its second alpha release - its first as part of KDE.</p>

<p>Lead developer Bogdan Vatra explains the reasons for making Necessitas a KDE project:</p>
<blockquote style="font-style: italic;">"YES, we've joined KDE, because we share the same goals; to make Qt more powerful, more accessible, and to keep it free for everyone."</blockquote>
<!--break-->
<p>This new release brings lots of changes:</p>
<ul>
<li>Support for SSL</li>
<li>QtWebKit 2.1 with JIT support, which is 2.5x times faster than in the previous release</li>
<li>Qt Creator 2.2</li>
<li>A preview of QtMobility</li>
<li>GDB 7.2 with Python support</li>
<li>Fixes for the most annoying bugs</li>
</ul>

<p>Necessitas is now easier to install and update thanks to the switch to Nokia's SDK Installer. It also has the option to install Google's official SDKs and NDK automatically, and is available for Windows.</p>

<p>This is an important project for KDE, as having full Qt support in Android is the first step toward running KDE software on many more mobile devices. And both Necessitas and KDE have the same goals related to Qt. If you are interested, you can subscribe to the <a href="https://mail.kde.org/mailman/listinfo/necessitas-devel">mailing list</a>, and if you give it a try, don't forget to <a href="https://bugs.kde.org/enter_bug.cgi?product=Necessitas&format=guided">report bugs</a>.</p>

<p>Please remember this is an alpha release; it's not yet ready for production. More news after the Qt Contributors' Summit in June.</p>