---
title: "KDE e.V. Quarterly Report for Q3 2011 published"
date:    2011-11-23
authors:
  - "nightrose"
slug:    kde-ev-quarterly-report-q3-2011-published
---
The third Quarterly Report of KDE e.V. for 2011 has been published. It gives an overview of all the activities KDE e.V. supported in the last 3 months. This includes the Desktop Summit in Berlin and various sprints, and also the e.V.'s annual general assembly and an interview with Lydia Pintscher about why KDE rocks at mentoring.

<a href="http://ev.kde.org/reports/ev-quarterly-2011_Q3.pdf">Read the full report today.</a>

The work of KDE e.V. would not be possible without our supporters. If you support the work of KDE e.V. and lack the time to contribute directly to KDE, why not <a href="http://jointhegame.kde.org/">Join the Game</a>?