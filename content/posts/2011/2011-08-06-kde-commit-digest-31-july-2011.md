---
title: "KDE Commit Digest for 31 July 2011"
date:    2011-08-06
authors:
  - "mrybczyn"
slug:    kde-commit-digest-31-july-2011
comments:
  - subject: "After reading the Digest"
    date: 2011-08-08
    body: "i want to listen to Mozart - In the Hall of the Mountain King. Nepomuks future looks promising, the present is crashing though : /"
    author: ""
  - subject: "\"hall of the mountain king\""
    date: 2011-08-08
    body: "\"hall of the mountain king\" is by edvard grieg."
    author: ""
  - subject: "yea"
    date: 2011-08-09
    body: "yea, thats old. I want to hear it from mozart!"
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-07-31/">this week's KDE Commit-Digest</a> Artem Serebriyskiy introduces the new Nepomuk Web Extractor in a featured article. The list of changes include:

             <ul><li>Desktop layout control moves from pager to <a href="http://userbase.kde.org/KWin">KWin</a></li>
<li><a href="http://owncloud.org/index.php/Main_Page">OwnCloud</a> gets instant search and sees many smaller commits which bring minor new features and work on the user interface</li>
<li>An asynchronous <a href="http://userbase.kde.org/Nepomuk">Nepomuk</a> resource retriever is implemented in <a href="http://community.kde.org/KDE_PIM">KDE PIM</a> to improve performance</li>
<li><a href="http://rekonq.kde.org/">Rekonq</a> receives a synchronisation feature</li>
<li>Early version of <a href="http://dolphin.kde.org/">Dolphin 2.0</a> comitted</li>
<li>OpenVPN configuration import/export is now possible in <a href="http://userbase.kde.org/NetworkManagement">Network Management</a></li>
<li><a href="http://userbase.kde.org/Kate">Kate</a> now supports local folding</li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a> there is work on caching and multiple bugfixes, Krita gets an update of undo support.</li>
</ul>

               <a href="http://commit-digest.org/issues/2011-07-31/">Read the rest of the Digest here</a>.