---
title: "KDE Ships May Updates"
date:    2011-05-06
authors:
  - "sebas"
slug:    kde-ships-may-updates
---
<p>
May 6th, 2011. Today, KDE has <a href="http://kde.org/announcements/announce-4.6.3.php">released</a> a series of updates to the Plasma Desktop and Netbook workspaces, the KDE Applications and the KDE Frameworks. This update is the second in a series of monthly stabilization updates to the 4.6 series. 4.6.3 <a href="http://www.kde.org/announcements/changelogs/changelog4_6_2to4_6_3.php">brings</a> many bugfixes and translation updates on top of the latest edition in the 4.6 series and is a recommended update for everyone running 4.6.2 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE's software is already translated into more than 55 languages, with more to come.
</p>
<p>
The 4.6.3 release is dedicated in memory of the young daughter of KDE developer Daniel Nicoletti who tragically passed away after a car crash last month. The KDE community wishes to express their deepest sympathy and support to Daniel and his family in this difficult time.</p>
<!--break-->
