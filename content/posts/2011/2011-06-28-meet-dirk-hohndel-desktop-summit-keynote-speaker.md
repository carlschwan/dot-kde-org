---
title: "Meet Dirk Hohndel, Desktop Summit Keynote Speaker"
date:    2011-06-28
authors:
  - "kallecarl"
slug:    meet-dirk-hohndel-desktop-summit-keynote-speaker
comments:
  - subject: "Meet Dirk Hohndel"
    date: 2011-07-02
    body: "I am looking forward to meeting him"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 200px;"><img src="/sites/dot.kde.org/files/DirkHohndel203x260.jpg" width="200px"><br /> Dirk Hohndel</div>

Dirk Hohndel will be the opening keynote speaker at this summer's <a href="https://www.desktopsummit.org/">Desktop Summit</a> in Berlin, 6th - 12th August.

As a hacker-turned-businessman, Hohndel brings a business perspective of free software to the Desktop Summit. Currently Chief Linux and Open Source Technologist at Intel, Hohndel will talk about the role that large companies play in open source, and how the open source community can work effectively with them.

Hohndel has been an active developer and contributor in the Linux space since its earliest days, and has worked professionally in the SuSE and XFree86 projects. Still an active contributor in many open source projects and organizations, Hohndel joined Intel in 2001.

William Carlson talked with Dirk about his background and what he's currently working on. Read the <a href="https://www.desktopsummit.org/interviews/dirk-hohndel">full interview on the Desktop Summit website</a>.
<!--break-->
