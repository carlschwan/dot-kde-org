---
title: "Calligra Announces First Snapshot Release"
date:    2011-05-23
authors:
  - "oriol"
slug:    calligra-announces-first-snapshot-release
comments:
  - subject: "UI still unusable"
    date: 2011-05-23
    body: "Having a new layout engine and better docx support is nice, but before Calligra becomes a serious alternative to MS Office, LibreOffice, etc.,  you'll need to *seriously* rework the UI.\r\n\r\nHaving to spend ~20% of my productivity time with micro-managing the sizes and positions of all the various tool-panels in order to access the buttons I need is just not acceptable."
    author: "uetsah"
  - subject: "UI still unusable"
    date: 2011-05-23
    body: "Yeah, that's why I wrote this in the announcement:\r\n\r\n<cite>User Interface Work\r\n\r\nWe have worked to identify the weaknesses in the user interfaces that we have.  Help in this have been user interface design people, both from within the community and from outside it. We have also started to work on implementing the ideas that have come up, but this is far from finished yet.\r\n\r\nWe urge our testers to be patient in this area, since in this snapshot the user interface is still far from good. We are aware of that. The next snapshot will show the first improvements in the user interface, but we are interested in feedback already for this one.</cite>"
    author: "ingwa"
  - subject: "Great looks"
    date: 2011-05-23
    body: "As everything in KDE the looks are fantastic. If enough functionality and compatibility comes with that I'd say all is great.\r\n\r\nI really dislike Libre/Openoffice with their lack of interest in KDE."
    author: "roentgen"
  - subject: "Nice"
    date: 2011-05-24
    body: "That sounds cool so far. Hopefully, the next snapshot will be a huge step forward. \r\n\r\nKeep doing such a passionate job. Many people appreciate that."
    author: ""
  - subject: "Well...\nFor me when the small"
    date: 2011-05-25
    body: "Well...\r\nFor me when the small bugs in Words (font rendering, visual response when select all text, lag when typing) wasn't killed, it's not production ready."
    author: ""
  - subject: "What is exactly the point of it?"
    date: 2011-05-25
    body: "KOffice/Calligra has been around forever, yet it never managed to be actually usable. It is important to know when to give up and move on. They are so many parts of KDE that could use the developing time..."
    author: "trixl."
  - subject: "The point"
    date: 2011-05-25
    body: "...and it's important to know that just because sharpened rocks used to be the best tool available (for oh, say, a few million years), it doesn't always have to be that way.\r\n\r\nWith some exceptions (like Krita), your comment might have seemed more appropriate a year or so ago. But to put it mildly, the pace of development in Calligra has increased of late. It's now in a really exciting phase, and well worth pursuing. Go Calligra team!\r\n"
    author: ""
  - subject: "Calligra runs on more than the desktop"
    date: 2011-05-25
    body: "I think it's worth noting here that Calligra is the *only* free office suite that runs on mobile devices and on touch screens. I have seen OpenOffice.org on a Wetab and, frankly, it's not a sight to behold. Calligra Mobile, previously FreOffice, works very fine on the Nokia N900 smartphones.  \r\n\r\nI agree that the desktop UI is still in general bad -- even very bad at some points -- but even on the desktop there are exceptions. Ask the professional artists what they think about Krita, for instance.  They rave about it and if you go to krita.org you will find a whole section of rather impressive art that is created with Krita."
    author: "ingwa"
  - subject: "\"Help in this have been user"
    date: 2011-05-25
    body: "\"Help in this have been user interface design people, both from within the community and from outside it.\"\r\nThis sentence is obviously incorrect. I think it should say something like \"We have received help in this matter from...\" or \"Helpful in this regard have been...\" or something like that."
    author: "yman"
  - subject: "Please update the website"
    date: 2011-05-26
    body: "Might be worth updating this:\r\n\r\nhttp://www.calligra-suite.org/get-calligra/\r\n\r\nCan't wait to try it out. Good stuff guys."
    author: ""
  - subject: "Calligra runs on more than the desktop"
    date: 2011-07-20
    body: "I did not know that \"Calligra is the *only* free office suite that runs on mobile devices and on touch screens\". I use OpenOffice.org on my desktop and laptop, buy need something that will work on my Nokia phone. Thanks for the heads up.\r\n\r\n\r\n"
    author: ""
---
The Calligra project has announced the first snapshot release of the <a href="http://www.calligra-suite.org/">Calligra suite</a>, five months after Calligra and KOffice split ways. During that time, the Calligra team has improved the core libraries and all the applications.

This is a technical preview, not recommended for production work. Inge Wallin, the marketing coordinator for Calligra, says, "We have worked very hard to improve the underlying engine, making it more versatile and improving stability. The time has come to improve the user interfaces. This is why we are releasing the snapshot now. We want feedback on the user experience as it improves in future snapshots."

The snapshot contains the standard desktop user interface and two user interfaces for mobile environments: Calligra Mobile (previously known as FreOffice) and Calligra Active, a QML-based user interface for Plasma Active-like environments. It also contains two new applications compared to previous KOffice releases: Flow for diagram editing, and Brainstorm for taking notes.

More details can be found in the <a href="http://www.calligra-suite.org/news/calligra-announces-first-snapshot-release/">announcement on the Calligra homepage</a> and the <a href="http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/">screenshot tour</a>.