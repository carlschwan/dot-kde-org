---
title: "ownCloud 2 released"
date:    2011-10-11
authors:
  - "kallecarl"
slug:    owncloud-2-released
comments:
  - subject: "looks promising"
    date: 2011-10-11
    body: "Ownclowd looks really promising. I like the very clean gui (although I somehow miss references to KDE in it, since it is a KDE project). What still prevents me from giving it a real try is the lack of a syncing client similar to Dropbox. What ever clowd I use, I would not trust it so much, that it should have my only copy of my files. A copy on my computer and a copy in a clowd should be a backup for each other, so I need them to be synced, like Dropbox does it. Just accessing via WebDAV is not enough integration. Is there any syncing client planed?\r\nIn the release announcement there is an affiliation to Diaspora mentioned. Are there any plans to integrate them with each other for sharing files, links, fotos etc?"
    author: ""
  - subject: "Sure"
    date: 2011-10-11
    body: "ownCloud is designed to work cross platform but has tight integration with the rest of KDE of course because ownCloud is part of KDE.\r\nYou can access your files via the WebDAV kioslave. You can directly stream you music from ownCloud to Amarok or Tomahaw. We have KDE Desktop notifications integration via the Social Desktop integration. All KDE applications can sync key-value data with ownCloud via libattica. ownCloud has Akonadi integration with CalDAV and CardDAV. And someone is doing an ownCloud KDE syncing client called Mirall which is already working. So there is plenty of KDE integration.\u00a0:-)\r\n\r\nIntegration with Diaspora is planed but not done yet. But an unhosted backend is already working.\r\n\r\nCheers\r\nFrank\r\n\r\n"
    author: "FrankKarlitschek"
  - subject: "corrector"
    date: 2011-10-11
    body: "\"has tight integration with the rest of KDE of course because ownCloud is part of KDE\"\r\n\r\nis rather\r\n\r\n\"has tight integration with most software from KDE of course because ownCloud creators are part of KDE\" :)\r\n\r\nAnyway, congrats to the new release, keep on rocking on freeing our digital muscles!"
    author: ""
  - subject: "off-line functionality crucial to me..."
    date: 2011-10-11
    body: "So, does the availability of the syncing client Mirall mean that I can use my files when there is no (or a crappy) network connection? Because at least half the time I'm working with my files I have no network... And you can't have your KDE configuration on a owncloud share either, as my laptop needs to log in and use the credentials from the configuration to create a network connection!"
    author: "jospoortvliet"
  - subject: "Sounds awesome"
    date: 2012-08-14
    body: "Wow, this actually sounds really awesome! I've slowly been adapting to the idea of storing files online, but security issues like some services without SSL or just the mere fact that I couldn't access my information from another computer or IP at any given moment has left me hesitant. The market is already saturated with online storage services that all do the exact same thing, but it's nice to see a fresh take on this somewhat old technology. Definitely interested in learning more... hopefully this doesn't get squashed by the mighty as-yet-to-be-seen iCloud, which I couldn't imagine ever paying for! "
    author: "SimoneDavenport"
  - subject: "How about Android integration?"
    date: 2011-10-12
    body: "What I really would like to see is Android integration. On my Android phone I deactivate all functions syncing the phone settings and apps to Google, because I do not trust this company. Still I would like to sync my mobile to some server I trust. Ownclowd sounds like an interesting possiblilty. If had this and got this integrated into Cyanogenmod you would surely increase the userbase (and possibly developers too) of Ownclowd."
    author: ""
  - subject: "Spanish announcenment"
    date: 2011-10-12
    body: "Spanish translation of the release announcement in http://kde.org.ar/oc2\r\n\r\nTraducci\u00f3n al espa\u00f1ol del anuncio de lanzamiento en http://kde.org.ar/oc2"
    author: ""
  - subject: "sure"
    date: 2011-10-12
    body: "There is an official android client in the works. In the meantime you can use any application which is able to talk webDAV to access ownCloud. Calendar and Address-book syncing should also work wir CalDAV and CardDAV."
    author: "FrankKarlitschek"
  - subject: "About it"
    date: 2011-10-12
    body: "I loved how it looks and it's feel but it has a lot of small irritations... No beautifully animated upload progress bar, Music stopping and starting again just after the page renders again(when u navigate into it from page to page), .PDF files as demos but u can't open them..."
    author: ""
  - subject: "Part of KDE?"
    date: 2011-10-13
    body: "It's not hosted in KDE repositories nor mentions KDE in its webpage at all. So is it really part of KDE?"
    author: ""
  - subject: "Local file storage"
    date: 2011-10-14
    body: "Another comment went into that direction, but the answer was not clear.\r\nSo in short. Owncloud sounds very cool. But i will only use it, if i can use my files offline. The way dropbox works, is absolutely perfect for me, because i can use it as a backup system, with copies of the files of all computers connected to that account + a online copy (which i don't trust as much).\r\n\r\nSo, is anything planned in that direction?"
    author: ""
  - subject: "Beta"
    date: 2012-01-28
    body: "The <a href=\"http://owncloud.org/install/\">ownCloud 3 RC</a> is available. Install it and do some bug testing."
    author: "gaara"
---
ownCloud 2 has just been released. <a href="http://owncloud.org/">ownCloud</a> is a web-based storage application similar to Google Docs, Dropbox or Ubuntu One with a big difference—<strong>your</strong> data is under <strong>your</strong> control. With version 2, the ownCloud team has improved the basic service and added valuable features:
<ul>
<li>Access your files on the web or integrate ownCloud with desktop file managers.</li>
<li>Share files securely.</li>
<li>Access music and personal information directly or connect through applications.</li>
<li>Synchronize with other web applications that use the remoteStorage protocol.</li>
<li>More user support, demos and community interaction.</li> 
</ul>

With proprietary services, you don't have complete control over your data. You are at the mercy of their security policies and procedures, which don't always work out well. Lose control, lose some freedom.  ownCloud joins other projects and the open source <a href="http://unhosted.org/">Unhosted project</a> in keeping the web usable while decentralizing it and adding resilience.

Frank Karlitschek, ownCloud project founder—"I'm proud that the ownCloud community is growing so fast and is doing such awesome work. ownCloud is filling a critical hole in the free software world because it protects our personal data. I'm super happy about the growing interest in ownCloud from developers and users."

Please read the <a href="http://owncloud.org/announcement/">ownCloud 2 release announcement</a> for more information, including screenshots.

