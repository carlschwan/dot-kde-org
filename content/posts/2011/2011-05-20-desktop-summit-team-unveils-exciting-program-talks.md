---
title: "Desktop Summit Team Unveils Exciting Program of Talks"
date:    2011-05-20
authors:
  - "Stuart Jarvis"
slug:    desktop-summit-team-unveils-exciting-program-talks
comments:
  - subject: "Other projects?"
    date: 2011-05-22
    body: "Does KDE or the Gnome people actively reach out to other projects like XFCE or LXDE so that they attend (like sending invitation emails)??\r\n\r\nI see one E17 speaker on the list but I think having all FOSS desktop projects in one place would be best."
    author: ""
  - subject: "Can't help but agree"
    date: 2011-05-23
    body: "I'm a KDE3.5 hardcore fan and bitterly resent the arbitrary changes I see in 4, but there need to be alternatives and let's face GNOME aint the only one.  Yes, where is XFCE in this conference?  A lot of people are turning to it."
    author: ""
  - subject: "I like the variety..."
    date: 2011-05-23
    body: "<b>I like the variety myself</b>\r\n\r\n<ul>\r\n<li>KDE (preferred)</li>\r\n<li>Gnome Awesome too</li>\r\n<li>XFCE - Lean-n-Mean!</li>\r\n<li>... and there are others too!</li>\r\n</ul>\r\n<p>I like them all for what they individually may focus on or because each brews their own \"special\" apps... like there are various Gnome this or that tools I like, likewise KDE ones ...</p>\r\n\r\n\r\n<p>It's all cool! Especially the icon files, directory structure, and other gui stuff becoming more - well.. more easily customisable for the general public due to standards - very neat... </p>\r\n\r\n<p>...and cross platform in some instances (whole or in part - even just the standards) are awesome! This makes it easier to pick a \"GUI\" core technology and go to town and your time isn't wasted because you stuff will run likely all over: How cool is that!</p>\r\n\r\n<p>I like projects also like Freepascal and Lazarus because of the cross platform (but not script!) and the Lazarus in particular uses a core \"gui\" interface whose implementation is carried out based on if a \"compatible\" gui environment/toolkit is available on the system.</p>\r\n\r\n<p>I love it when I see all the parts working together seamlessly. </p>\r\n<p>Keep up the good work!</p>\r\n<strong>--Jason P Sage - </strong><a target=\"_blank\" href=\"http://www.jegas.com\">Jegas, LLC</a>"
    author: ""
  - subject: "what a waste"
    date: 2011-05-25
    body: "Where are the talks where the Gnome and KDE devs all admit that both groups stopped listening to the users years ago?\r\n\r\nSadly I've started seeing nothing but older or more fringe managers around the office lately.  From old fvwm and enlightenment, to mostly XFCE now.  Sad state for gnome/kde.\r\n"
    author: ""
  - subject: "Lucky XFCE..."
    date: 2011-05-25
    body: "Did anyone warn them the trolls are coming their way? ;-)"
    author: ""
---
<div style="float:right; border: 0; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/acquia_prosper_logo.png" /></div>The Desktop Summit is a joint conference organized by the GNOME and KDE communities, the two dominant forces behind modern graphical software on free platforms. Over a thousand international participants are expected to attend. The main conference takes place from 6-8 August. The annual membership meetings of GNOME and KDE are scheduled for 9 August, followed by workshops and coding sessions on 10-12 August.

The Desktop Summit Team is now pleased to announce <a href="https://www.desktopsummit.org/program">this year's program of talks</a>.
<!--break-->
<h2>Keynotes, Tracks and Panels</h2>

The Desktop Summit team welcomes keynote speakers from outside the GNOME and KDE world: Claire Rowland (Fjord, UX Design) and Thomas Thwaites (Technologist & Designer) are confirmed. More to come! There will also be keynoters from the GNOME and KDE communities.

The focus of the conference is collaboration between Free Desktop projects; the conference tracks reflect shared interests. The Desktop Summit hosts the annual conferences of GNOME and KDE. Similarly, each track includes speakers from different desktop projects. "We received a lot of really high quality and interesting proposals and spent a lot of time finding the best selection. It is fantastic to see how much the community has to talk about and show. It will surely be an interesting event with lots of opportunity to collaborate." says Lydia Pintscher, program committee member.


Topics addressed:
<ul>
<li>Accessibility & Help</li>
<li>Administration & Government</li>
<li>Applications</li>
<li>Community</li>
<li>Development</li>
<li>Platform</li>
<li>Tablets</li>
<li>Toolkits</li>
</ul>

There is too much going on at the Desktop Summit to give full details here. However, some of the talks highlight the quality and range of speakers and topics:
<ul>
<li><b>Karsten Gerloff</b> (President of the FSFE) will speak about <i>Free Desktops for Europe's public sector</i>.</li>
<li><b>Knut Yrvin</b> of Qt Development Frameworks will discuss <i>Ways of winning organizations over to free software</i>.</li>
<li><b>Asheesh Laroia</b> will explain <i>How to get new contributors (and diversity) through outreach</i>.</li>
<li><b>Dave Neary</b> cautions against <i>The Cost of Going it Alone</i>.</li>
<li><b>Aaron Seigo</b> will present the <i>KDE Platform 4 Roadmap</i>.</li>
<li><b>Benjamin Otte</b> and Matthias Clasen speculate on <i>GTK 4 - The future of your favorite toolkit</i>.</li>
<li><b>Owen Taylor</b> looks forward to <i>GNOME Shell Version π (pi)</i>.</li>
<li><b>Michael Meeks</b> will talk about <i>Liberating Open Office Development</i>.</li>
<li><b>Gustavo Sverzut Barbieri</b> will provide a <i>Quick Overview of Enlightenment Foundation Libraries and E17</i>.</li>
<li><b>Clemens Buss</b> tells us all about <i>Design Thinking and how the Free Desktop may benefit from it</i>.</li>
<li><b>Vincent Untz</b> answers a common question: <i>Swimming upstream or downstream? Both!</i></li>
<li><b>Federico Mena Quintero</b> introduces <i>Software with the Quality that Has No Name</i>.</li>
<li><b>Pockey Lam</b> talks about <i>GNOME a continent, starting from Asia</i>.</li>
<li><b>Pradeepto Bhattacharya</b> presents <i>KDE India</i>.</li>
</ul>

The annual conferences always generate a lot of discussion. This year's summit is no exception with a panel discussion on one of the hot topics in free software. Representatives from Canonical, Novell and the Software Freedom Conservancy will discuss the pros and cons of copyright assignment.

<h2>About the Desktop Summit</h2>

<div style="float:right; border: thin solid gray; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/panorama_mit_museumsinsel.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/panorama_mit_museumsinsel_wee.jpeg" width="350" height="232"/></a><br />The host area of Berlin</div>GUADEC (GNOME Users And Developers European Conference) and Akademy (KDE annual world summit) are the world's largest gatherings of people involved in Free Desktop and mobile user interfaces. Over a thousand participants are expected at the Desktop Summit this year, covering both the GNOME and KDE projects as well as related technologies. Organizers welcome developers, artists, translators, community organizers, users and representatives from government, education, businesses. Anyone who shares an interest in a Free Desktop is encouraged to participate.

GNOME and KDE are Free Software communities that drive the user interfaces of many Linux-powered devices--smartphones, laptops, desktops, personal media centers. 2011 is the second summit organized collaboratively by the two communities.

<h2>Stay Informed</h2>

Follow and discuss news about the Desktop Summit
<ul>
<li>Identi.ca/Twitter hashtag: #DS2011</li>
<li>IRC: #desktopsummit on Freenode</li>
<li>Mailing list: https://mail.kde.org/mailman/listinfo/ds-discuss</li>
</ul>

Book <a hRef="https://www.desktopsummit.org/accommodation"> a special discount accommodation</a> before 30 June.

If you have any questions, <a href="mailto:ds-team@desktopsummit.org">email the team</a>.

<h2>Register Now!</h2>

The Desktop Summit 2011 is free to attend, but you must <a href="https://www.desktopsummit.org/register"> register</a>.