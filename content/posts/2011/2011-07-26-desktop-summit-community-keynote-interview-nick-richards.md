---
title: "Desktop Summit Community Keynote Interview with Nick Richards"
date:    2011-07-26
authors:
  - "kallecarl"
slug:    desktop-summit-community-keynote-interview-nick-richards
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 200px;"><img src="/sites/dot.kde.org/files/nick-richards.jpg"><br />Nick Richards</div>

<p>In this last in the series of interviews with Keynote speakers at the <a href="https://www.desktopsummit.org/">Desktop Summit</a>, starting in Berlin in just a few weeks, meet Nick Richards.</p>

Nick Richards is an interaction designer. Not to be confused with graphic design, Nick's work is about the communication frameworks that connect people to data and tools. He helped the GNOME project with creating a useful and meaningful environment in <a href="http://www.gnome.org/">GNOME 3</a>, and he has also worked on the <a href="http://moblin.org/">Moblin</a> and <a href="https://meego.com/">MeeGo</a> mobile OS projects. A Senior Interaction Designer at <a href="http://software.intel.com/sites/oss/">Intel</a>, Nick contributes an important usability voice in design-driven development. 

<p>Read the <a href="https://www.desktopsummit.org/interviews/nick-richards">full interview</a>.</p>

<p>The Desktop Summit is free to attend but you must <a href="https://www.desktopsummit.org/register">Register</a>.</p>
<!--break-->
