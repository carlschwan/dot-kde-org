---
title: "Google CodeIn 2011: A Chance for the Next Generation to Join KDE"
date:    2011-11-19
authors:
  - "annnma"
slug:    google-codein-2011-chance-next-generation-join-kde
---
<div style="float:right; padding:0; padding-left: 10px; padding-bottom: 10px;"><a href="http://www.google-melange.com/gci/homepage/google/gci2011"><img src="http://dot.kde.org/sites/dot.kde.org/files/GCI_2011_logo_URL_blueborder-nowww.jpeg" /></a></div>
KDE is honored to be chosen again this year to be part of <a hre="http://google-melange.com">Google Code-In</a>. Pre-university students aged between 13 and 17 are offered a great chance to contribute to KDE by choosing from a large pool of tasks, depending on their skills—code, translation, videography, user interfaces, research and more. Spread the word about the contest to any students and parents you know.
<!--break-->
<h2>Pick your task</h2>
KDE is offering a large selection of tasks, shown <a href="http://community.kde.org/GoogleCodeIn/2011/Ideas">at the KDE Community Wiki</a>. 
Everyone can find something interesting and in their ability range—easy to hard, in C++ or QML code, imaginative screencasts, documentation, translation... Every contribution will make KDE even better! 
 
<h2>Be ready</h2>
Google Code-In will begin on Monday, November 21, 2011 at midnight Pacific (US) Time and will end Monday, January 16, 2012. If you meet <a href="http://www.google-melange.com/gci/document/show/gci_program/google/gci2011/rules">the eligibility requirements</a>, you can register for an account now. When the contest starts on November 21st, you can start right away claiming and completing tasks. 

Don't hesitate to contact the mentor prior to taking a task if you need more information. The KDE community is happy to help you and is looking forward to working with you!