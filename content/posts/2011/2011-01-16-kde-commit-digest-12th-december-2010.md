---
title: "KDE Commit-Digest for 12th December 2010"
date:    2011-01-16
authors:
  - "dannya"
slug:    kde-commit-digest-12th-december-2010
comments:
  - subject: "Erm... not quite..."
    date: 2011-01-16
    body: "<p\"Two large KOffice development branches (one for change tracking and another one previously maintained by Nokia) are being prepared for integration into Calligra Office.\"\r\n\r\n<p>The change tracking branch is still separate -- it is an NLnet funded project executed by Ganesh Paramasivam. He's still busy with it, it's really a complicated project because the underlying spec is really complicated.\r\n\r\n<p>The \"nokia maintained branch\" is nothing of the kind. It's a feature branch that was active during the feature freeze for 2.3. It wasn't just for Nokia nor even maintained by Nokia, it was open to all KOffice developers. There were kexi and krita commits, as well as commits by developers connected to the Nokia mobile office project. This branch is actually the new master branch of Calligra's git repository. "
    author: "Boudewijn Rempt"
  - subject: "Hi Boud,\nRewrite that line"
    date: 2011-01-16
    body: "Hi Boud,\r\nRewrite that line for us and i'll change it ;)\r\n\r\nCheers,\r\nDanny"
    author: "dannya"
  - subject: "sent"
    date: 2011-01-16
    body: "Sent a mail :-)\r\n"
    author: "Boudewijn Rempt"
  - subject: "Krita goes into calligra"
    date: 2011-01-16
    body: "When I first read that krita was going away from koffice i thought that it was going away from calligra as well, but seeing krita.org it seems they are just re-basing on calligra."
    author: "damipereira"
  - subject: "yes..."
    date: 2011-01-16
    body: "When calligra moved to git, I removed krita from the koffice svn directory. People were already getting confused, so it seemed the best course to me. See also http://krita.org/component/content/article/10-news/65-moving-to-git.\r\n\r\nKrita is being developed along (with calligra) at what amounts to a break-neck pace -- 1600 commits to calligra since we moved to git :-)."
    author: "Boudewijn Rempt"
  - subject: "thanks!"
    date: 2011-01-16
    body: "just a quick \"thanks, you rock!\" for getting the commit digest train rolling again. i know i'm not the only one who really appreciates these updates :)\r\n\r\ncheers and hugs ..."
    author: "aseigo"
  - subject: "Statistics"
    date: 2011-01-17
    body: "I might be mistaken, but I suspect that the statistics numbers related to bugs are... well... buggy:\r\n\r\nIt is a few digests that we have 22488 open bugs, 17182 open wishes, 596 bugs opened and 770 bugs closed"
    author: "alvise"
  - subject: "Yep, i've noticed this too,"
    date: 2011-01-18
    body: "Yep, i've noticed this too, and i've tracked down the reason...\r\n\r\nUnfortunately, my only way of getting bug stats is by downloading a statistics page from bugs.kde.org once a week - if this is missed, I can't get back data.\r\n\r\nI'll try and sort this out tonight.\r\n\r\nCheers,\r\nDanny"
    author: "dannya"
---

In <a href="http://commit-digest.org/issues/2010-12-12/">this week's KDE Commit-Digest</a>:

              <ul><li>Bug fixing efforts throughout the KDE development platform and applications after the release of KDE SC 4.6 Beta 2</li>
<li>New features and many bug fixes in the development branch of the Akonadi-fied <a href="http://pim.kde.org/">KDE-PIM</a> applications, with continued work on porting to Windows CE</li>
<li>Various features and bug squashing in <a href="http://koffice.org/"><a href="http://koffice.org/">KOffice</a></a> since the release of the first release candidate of version 2.3</li>
<li>Ganesh Paramasivam has been working on implementing the new ODF change-tracking specification. Lots of work also in the koffice-essen branch, which is to become the master (or trunk) branch of Calligra once the project moves to Git.</li>
<li>After the KOffice / Calligra split, <a href="http://www.koffice.org/krita/">Krita</a> is removed from KOffice, and <a href="http://koffice.kde.org/kivio/">Kivio</a> is renamed to (Calligra) Flow</li>
<li>Further work on Muon and its underlying library for handling the APT package manager</li>
<li>New features and fixes for <a href="http://www.digikam.org/">digiKam</a> and KIPI-Plugins</li>
<li>The big move from HAL to UDEV continues</li>
<li>Bug fixes and work on <a href="http://nepomuk.kde.org/">NEPOMUK</a></li>
<li>Features and fixes for <a href="http://plasma.kde.org/"><a href="http://plasma.kde.org/">Plasma</a></a> Mobile and polishing of the Plasma Desktop, specifically notifications</li>
<li>The RSSNow applet is being ported to QML</li>
<li>The reworking of <a href="http://oxygen-icons.org/">Oxygen</a> mimetype icons continues</li>
<li>Libalkimia moves to kdereview</li>
<li><a href="http://phonon.kde.org/">Phonon</a>, <a href="http://k3b.org/">K3b</a>, KPackageKit, print-manager, and the <a href="http://www.python.org/">Python</a> plugin for <a href="http://www.kdevelop.org/">KDevelop</a> move to Git</li>
<li><a href="http://edu.kde.org/">KDE-Edu</a> is now free of KDE/Qt3 support.</li>
</ul>

                <a href="http://commit-digest.org/issues/2010-12-12/">Read the rest of the Digest here</a>.