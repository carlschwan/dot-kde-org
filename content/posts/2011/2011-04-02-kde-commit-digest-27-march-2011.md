---
title: "KDE Commit Digest for 27 March 2011"
date:    2011-04-02
authors:
  - "vladislavb"
slug:    kde-commit-digest-27-march-2011
comments:
  - subject: "LongBug champion!"
    date: 2011-04-03
    body: "This week's champion is Camila Boemann for fixing bug 127571 which is more than 6 years old!"
    author: "emilsedgh"
  - subject: "Tsk..."
    date: 2011-04-03
    body: "Camila is a newbie wrt Dawit: he fixed bug #34578 dating back to KDE 2.2, gcc 2.95, Linux 2.4 times, which is nearly 10 years old!\r\nhttps://bugs.kde.org/show_bug.cgi?id=34578"
    author: "panda84"
  - subject: "Commit Digest feature?"
    date: 2011-04-04
    body: "Interested in adding this as a part of the regular Commit-Digest? Apply here: http://enzyme.commit-digest.org/\r\n\r\nYou can find the source code at the Enzyme homepage: http://enzyme-project.org/\r\n\r\n"
    author: "mkrohn5"
  - subject: "Well, actually..."
    date: 2011-04-04
    body: "If you really mean https://bugs.kde.org/show_bug.cgi?id=127571 (new feature: split the image), that wasn't Camila, but Srikanth Tiyyagura, a new Krita hacker from Andra Pradesh!"
    author: "Boudewijn Rempt"
  - subject: "Really?"
    date: 2011-04-06
    body: "I think it's pretty sad that bugs are allowed to live that long.  6 years to fix a bug?\r\n\r\nIt's pretty annoying actually, even more so when bugs are reported that aren't even acknowledged - like this one of mine:\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=268797"
    author: "philrigby"
  - subject: "And how"
    date: 2011-04-06
    body: "do you propose to disallow bugs living on for this long? \r\n\r\nMake sure to provide a detailed, feasible plan within the next ten days that includes an implementation timeline with detailed fallbacks in case your implementation does not reach its milestones."
    author: "Boudewijn Rempt"
  - subject: "Delete the old ones!"
    date: 2011-04-07
    body: "Just delete all bugs that are older than 365 days. This way there won't be any bugs that are older than one year ;-)"
    author: "mkrohn5"
  - subject: "Or, instead of sarcasm, how"
    date: 2011-04-07
    body: "Or, instead of sarcasm, how about having 4.6.4 or 4.6.5 \"bug killed\" versions where bugs older than 365 days are either fixed, or marked \"Can't Fix / By Design\" or similar?\r\n\r\nMy other point was that I filed an accurate, reproducible bug with a screenshot and it hasn't even been assigned to anyone."
    author: "philrigby"
  - subject: "Or"
    date: 2011-04-07
    body: "You could start learning how a volunteer-driven open source project like KDE actually works. You complain that your bug hasn't been assigned to anyone. Big surprise for you, perhaps, but there <i>isn't</i> anyone who can assign your bug to someone else. There are no managers. Generally, developers pick the bugs they want to work on themselves. And there might be nobody who can fix your bug, and even nobody who can try to reproduce and change the status from unconfirmed to new.\r\n\r\nSure, we could handle bug reports better. But the sad fact is that the libraries have a very low number of developers working on it and it's impossible for them to triage all bug reports <i>and</i> do some useful work, like fixing issues or implementing new features. So now it's up to you again: try and help out, for instance by triaging bugs as they come in, or even helping to fix them."
    author: "Boudewijn Rempt"
---
In <a href="http://commit-digest.org/issues/2011-03-27/">this week's KDE Commit-Digest</a>:

              <ul><li><a href="http://okular.kde.org/">Okular</a> now supports searching for words with ligatures</li>
<li>Work on new NetworkManager 0.9 branch in <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> and PEAP-GTC wireless authentication suppport</li>
<li>A new plugin which provides a rating and an annotation menu action for files and folders (Dolphin/Konqueror) from <a href="http://nepomuk.kde.org/">Nepomuk</a></li>
<li>A new dataengine in <a href="http://plasma.kde.org/">Plasma</a> that returns Activities thumbnails</li>
<li>Support for newer Linux kernels and extended support for terabytes and petabytes in <a href="http://userbase.kde.org/KSysGuard">KSysGuard</a> along with many bugfixes</li>
<li>New multithreaded puzzle class put to work in <a href="http://www.kde.org/applications/games/palapeli/">Palapeli</a>, removing the first-run assistant and adding puzzle list sorting</li>
<li>Proof-of-concept implementation of games as plugins for <a href="http://community.kde.org/KDE_Games/Tagaro">Tagaro</a></li>
<li>Automatic updating of comics added to the <a href="http://techbase.kde.org/Projects/Plasma/Plasmoids#Comic_Strip">Comic Strip</a> plasmoid</li>
<li>Fixes for S/FTP connections in <a href="http://dolphin.kde.org/">Dolphin</a> and other applications using the FTP <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kioslave/html/index.html">KIO Slave</a></li>
<li>Work on improved table layout in <a href="http://www.calligra-suite.org/">Calligra</a>, improved DOC support, and many bugfixes</li>
<li>Bugfixing in <a href="http://amarok.kde.org">Amarok</a> including fixes for IPod Classic detection</li>
<li>Bugfixing throughout <a href="http://rekonq.kde.org/">Rekonq</a> in preparation for the next release</li>
<li>Bugfixing in <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://www.digikam.org/">Digikam</a>, <a href="http://kst-plot.kde.org/">Kst</a>, and <a href="http://ktorrent.org/">KTorrent</a>.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-03-27/">Read the rest of the Digest here</a>.