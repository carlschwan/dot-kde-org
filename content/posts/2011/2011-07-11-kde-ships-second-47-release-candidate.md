---
title: "KDE Ships Second 4.7 Release Candidate"
date:    2011-07-11
authors:
  - "sebas"
slug:    kde-ships-second-47-release-candidate
comments:
  - subject: "link on announce page doesn't work"
    date: 2011-07-11
    body: "\"To download source code or packages to install go to the 4.7 RC2 Info Page.\" (from http://kde.org/announcements/announce-4.7-rc2.php)\r\n\r\nThe link \"4.7 RC2 Info Page\", pointing to http://kde.org/info/4.6.95.php, doesn't work."
    author: "kallecarl"
  - subject: "critical kwin multiscreen bug fixed"
    date: 2011-07-13
    body: "for us that, for different reasons, run Multi-Screen-Setup (seperate X-Servers) this release brings us one step closer to be usable: Kwin will now start nicely on all screens. I hope that this will be the last bug before I can return to KDE"
    author: ""
  - subject: "I've been plugging in a 2nd"
    date: 2011-07-13
    body: "I've been plugging in a 2nd display (VGA) to my 4.6 laptop for many months now without issue.\r\nIntegrated Intel video."
    author: ""
  - subject: "There is at least 3 ways to"
    date: 2011-07-14
    body: "There is at least 3 ways to plug in display on linux. There is Xrander, multi-headed X with or without Xinerama.  Everey one of them has some problems.\r\n\r\nXrander: \r\n  Only support 1 graphic card\r\n\r\nmulti-headed X with Xinerama \r\n  Full screesn program do not behave nicly due to the desktop expands more screens.\r\n\r\nmulti-headed X without Xinerama.\r\n  KDE 4 programs dont start on the appropriated screen. Some problem with keyboard input (eks tabbing)\r\n\r\nSince I have 2 cards to power my 4 screen (3 monitors + tv) I cannot use Xrander. Since I also like games, like warzone and widelands, I cannot use multi-headed X with Xinerama. Witch leave my with multi-headed X without Xinerama. witch used to work nicly with kde 3.5 but dont work nicly with kde 4. A lot of problem as been solve (in plasma in kde 4.5.3, in kwin in 4.7 (patch bagported to kde 4.6-serie in debian).\r\n\r\nSo that leaves me stranded unto either the kde community get those last problem solved or switch to wayland. Or I get smarter and learn another way of setting up X with kde\r\n\r\n\r\n\r\n"
    author: ""
  - subject: "KWin may start nicely"
    date: 2011-07-15
    body: "But KDE itself is not.\r\nKSplash is totally ugly when using Multihead instead of Multiscreen (which the majority will use).\r\nSo I think I will have to disable KSplash on 4.7 :("
    author: ""
  - subject: "Info page link doesn't work?"
    date: 2011-07-16
    body: "http://kde.org/info/4.6.95.php this page doesn't work. It's already pointed out by another user. Kindly check"
    author: ""
  - subject: "That's the new direction of KDE"
    date: 2011-07-18
    body: "Announcing and not working seems the new dogma for KDE. I am running openIndiana with gnome on a laptop. After all these years with KDE, I think I will switch to gnome now.\r\nDevelopment of KDE is running in a complete wrong direction for me, too much (mostly not working) bling bling, too less stable apps (PIM still a nightmare) for daily working and too complicate for newbies and essential tasks, like adding Google's calendar and addressbook f.e. Changing just the look is departed in so many kcm modules, that I get confused.\r\n\r\n"
    author: ""
---
Today, KDE has <a href="http://kde.org/announcements/announce-4.7-rc2.php">released</a> a second release candidate of the upcoming 4.7 release of the Plasma Desktop and Netbook workspaces, the KDE Applications and the KDE Frameworks, which is planned for July 27, 2011. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing last-minute showstopper bugs and finishing translation and documentation that comes along with the releases.