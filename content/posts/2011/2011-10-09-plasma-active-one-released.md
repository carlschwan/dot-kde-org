---
title: "Plasma Active One released!"
date:    2011-10-09
authors:
  - "kallecarl"
slug:    plasma-active-one-released
comments:
  - subject: "Hats off"
    date: 2011-10-11
    body: "Wow! Simply, WOW!!!\r\n\r\nI have an ExoPC from one of those Intel events, and now Plasma Active has transformed a slab of electronics in a real table that I'm not ashamed to show to other people.\r\n\r\nI don't understand how your build has better graphics, smoother transitions, and more featureful applications that Intel's own build, but it's true. Even the virtual keyboard (a must have for me, since I don't have an USB one) is ten times more useful than the one with the original MeeGo build.\r\n\r\nNow I have a web browser and a PDF viewer (and KPat!) so I can truly use this tablet as a consumer device.\r\n\r\nSeriously, hats off to the Plasma community for this."
    author: ""
---
<div style="float: right; margin: 1ex;"><a href="http://plasma-active.org/"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma-active-ONE300.png" /></div>

Today marks a major milestone for KDE Plasma Workspaces. <a href="http://plasma-active.org/">Plasma Active</a> One has been released, primarily for tablet computers. It is the latest expression of the Plasma concept, following Plasma Desktop and Plasma Netbook. In the KDE tradition, Plasma Active One is designed for the best User Experience—for people on the move and engaged in many activities.
<!--break-->
Plasma Active is a truly open project. It is modular, customizable, and offers an attractive app development environment. The KDE Community and the Plasma Active team invite participation from individuals and companies with interests in ultraportable computing.

The <a href="http://kde.org/announcements/plasma-active-one/">release announcement</a> has details, along with links to important resources. Core developer blogs have <a href="http://aseigo.blogspot.com/">additional background</a>, <a href="http://www.notmart.org/">thoughts on the process</a> and <a href="http://vizzzion.org/blog/">technical insights</a>.

Thank you to the core development team, the founders of the project, and the many contributors. KDE Plasma Active One is an impressive, inspiring accomplishment! Not only that, it opens a future full of possibilities with all types of touch computing devices.