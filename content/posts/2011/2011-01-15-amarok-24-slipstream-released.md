---
title: "Amarok 2.4 \"Slipstream\" Released"
date:    2011-01-15
authors:
  - "teo"
slug:    amarok-24-slipstream-released
comments:
  - subject: "nvidia crash"
    date: 2011-01-15
    body: "Unfortunately 2.4 continues to be affected by the nvidia 260.xx.xx drivers  issue on openSUSE 11.3 32bit. Crashes on startup :S Seems like I'll be staying with Juk for a while. "
    author: "jadrian"
  - subject: "Unfortunately, not a few of"
    date: 2011-01-16
    body: "Unfortunately, not a few of the \"Ph\u00e4nom\u00e4n 'concern that some hardware, OS (openSUSE) & the latest Nvidia drivers are not\" harmonize \"want. But you install the 256.53 .. Driver. This should support your hardware, despite the \"age\" is still sufficient in terms of performance of your graphics card. So at least you can all use KDE tools, including amaroK 2.4. :)\r\n\r\nHave a nive Weekend!"
    author: "Lisufa"
  - subject: "Very good work :)"
    date: 2011-01-16
    body: "This release is definitely significantly faster, thanks to the team ! "
    author: "mahen"
  - subject: "Nice improvements"
    date: 2011-01-16
    body: "Nice new release. Sadly i can't read audio cds (amarok does not see them), can't use my ipod touch 3g (amarok sees it, sees the songs, but can't play them nor copy new ones on the ipod touch), can't use my iphone 3gs (amarok could see it but crashed imediately, after 5 tries it does not see it anymore at all).\r\n\r\nbut rest looks nice :) i also just tried amarok under windows, works great. Will open a few bug reports when i have time to reproduce it better."
    author: "Asraniel"
  - subject: "Nvidia's 256.xx.xx drivers"
    date: 2011-01-16
    body: "Nvidia's 256.xx.xx drivers are not on the openSUSE repository anymore. Manual install is much more complicated. Personally I know how to do it, but cannot afford to play around with my system at this point. But most users cannot even do it. \r\n\r\nThere's already more duplicates of this bug being reported by people who are still on KDE 4.4.x (opensuse default) but who updated amarok to the new version. Users this conservative are definitely not going to install nvidia's drivers manually. \r\n\r\n"
    author: "jadrian"
  - subject: "Nice features"
    date: 2011-01-17
    body: "Finally the problem with utf-8 chars in file names seems to be fixed with the last Qt-Kde updates, yay!\r\n\r\nThe new Amarok widgets are very nice, great job!"
    author: "caribe"
---
After a longer than usual and especially intense development period, the Amarok team is happy to announce the immediate availability of <a href="http://amarok.kde.org/en/releases/2.4.0">Amarok 2.4, codename "Slipstream"</a>.
<!--break-->
This release brings significant performance, usability and stability improvements. Many bugs have been fixed, especially in the context pane, and the collection scanner has been rewritten from scratch. Notable features that have been added to this release include:
<ul>
<li>Support for newer iPod devices such as the iPod Touch 3G</li>
<li>Transcoding</li>
<li>A guitar tabs applet</li>
<li>A greatly improved upcoming events applet</li>
<li><a href="http://www.playdar.org/">Playdar</a> collection support</li>
<li>Access to UPnP collections on the local network</li>
</ul>

For a more complete overview check out the <a href="http://amarok.kde.org/en/releases/2.4.0">release announcement</a>, and take a look at the changelog for a detailed list of all the new features and fixed bugs.