---
title: "KDE Ships March Updates, Codenamed \"Helga\""
date:    2011-03-04
authors:
  - "sebas"
slug:    kde-ships-march-updates-codenamed-helga
comments:
  - subject: "Subversion log?"
    date: 2011-03-04
    body: "According to the announcement, \"For a complete list of changes that went into 4.6.1, you can browse the Subversion log.\"\r\n\r\nShouldn't that be the git log now?"
    author: "Karellen"
  - subject: "Tarballs?"
    date: 2011-03-04
    body: "Cool, but where are the tarballs? Probably still hidden on FTPs?"
    author: "Lustmored"
  - subject: "where?"
    date: 2011-03-05
    body: "Where can i find complete changelog ? Can't find svn logs..."
    author: "gnumdk"
  - subject: "You\u2019re now talking about"
    date: 2011-03-05
    body: "You\u2019re now talking about workspaces, apps and frameworks - yay! Far clearer communication: We are KDE.\r\n\r\nWelcome to my computer, Helga!"
    author: "ArneBab"
  - subject: "You've omitted the most important feature of Helga."
    date: 2011-03-06
    body: "I can't find in the announcement, in the release notes, or even in the changelog, news about the most important feature of KDE 4.6.1 Helga. I've tried to spread the news in Phoronix, but I need some help from you.\r\n\r\nThe fixing of <a href=https://bugs.kde.org/show_bug.cgi?id=246678>bug 246678</a>, and, consequently, a lockup-free Virtuoso, plus all the optimizations that Sebastian Tr\u00fcg made as part of fixing that bug once and for good, mean, basically, that...\r\n\r\n<strong>THIS IS THE FIRST KDE RELEASE IN WHICH YOU CAN USE THE NEPOMUK SEMANTIC DESKTOP WITHOUT HASSLES</strong>.\r\n\r\nNo 100% CPU usage anymore.\r\nNo problems.\r\n\r\nI should say this comes with a weird side-effect... the performance of Akonadi really is put to shame. I pushed Akonadi to the limit (KDE-PIM/Akonadi, plus the Google and Facebook extensions) and I saw that Virtuoso actually was waiting for Akonadi. So please, if there's a moment to migrate Akonadi to Virtuoso, it's now.\r\n\r\nKudos for the NEPOMUK team. You rock."
    author: "Alejandro Nova"
  - subject: "KDE 4.x maturity level reached !"
    date: 2011-03-06
    body: "Alejandro Nova : oh yes, this is an extremely important milestone indeed, as the semantic desktop is something that definitely sets KDE apart, and which has always been very promising but not usable (as most of you, I guess, I always ended up disabling it).\r\n\r\nSo : thanks a lot to the developers. After a few stabilization releases, then a few polishing releases, KDE 4 reached a great maturity level. (Kwin is another great example)\r\n\r\nAs far as I'm concerned, startup time is still an issue, but I heard a GSoC student worked to improve it by about 30%."
    author: "mahen"
  - subject: "Why is the changelog incomplete?"
    date: 2011-03-07
    body: "Every time it astonishes me that the changelog is incomplete. Sometimes it just lists a few fixes. Other then \"being more stable\" I can't really determine whether the update is relevant for me, or not. For all I know, some important fix for me might not be listed at all.\r\n\r\nWhy is this happening, and how can that be resolved?\r\n\r\n"
    author: "vdboor"
  - subject: "Bad looking bug always present :("
    date: 2011-03-07
    body: "https://bugs.kde.org/show_bug.cgi?id=265757"
    author: "gnumdk"
  - subject: "Indeed, currently your better"
    date: 2011-03-07
    body: "Indeed, currently your better off reading the commit digests than the release changelog"
    author: "Asraniel"
  - subject: "Good luck getting people"
    date: 2011-03-26
    body: "Good luck getting people behind this one. Though you make some VERY fascinating points, youre going to have to do more than bring up a few things that may be different than what weve already heard.  <a href=\"http://phaser8860ink.com/Phaser-8860-Forum/index.php?action=profile;u=1315;sa=summary\">phaser</a>\r\n"
    author: "ashleenmo"
---
In many parts of the world, spring is around the corner. Time for some spring cleaning, and that's true for your desktop and netbook as well. With 4.6.0 being about a month in the wild, it's time for the <a href="http://kde.org/announcements/announce-4.6.1.php">first round of updates</a>. The KDE team has made available "Helga" (bearing version number 4.6.1), a series of updates to Desktop and Netbook workspaces, applications and the KDE frameworks that make developers' lives so much easier. The changelog lists some of those fixes, but there are more that are not contained in the <a href="http://www.kde.org/announcements/changelogs/changelog4_6_0to4_6_1.php">changelog</a>. 4.6.1 is also the first release served mainly out of KDE's new Git-based development infrastructure.
As usual, 4.6.1 only contains bugfixes and translation updates, so we expect smooth sailing for everyone who upgrades from 4.6.0 or earlier versions. Enjoy Helga!