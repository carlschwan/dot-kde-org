---
title: "KDE Commit-Digest for 28th November 2010"
date:    2011-01-04
authors:
  - "dannya"
slug:    kde-commit-digest-28th-november-2010
comments:
  - subject: "Filters"
    date: 2011-01-04
    body: "Working on MSOffice import filters can't be all that much fun, so to see so much work going in to them is great news. "
    author: "tangent"
  - subject: "KMail"
    date: 2011-01-04
    body: "As always: Thank you Danny and the rest of the Commit Digest team.\r\n\r\nWow, the behaviour of KMail's system tray has apparently been fixed after all this time. I can't even begin to express how happy that makes me - that alone should make that PIM/KDE release a highlight for me.\r\nThanks go out to everyone involved!"
    author: "onety-three"
  - subject: "Nice"
    date: 2011-01-10
    body: "I have been in trouble with filters, KOffice my fav. \r\nImproving import filters has decreased process time up to %70 in my <a href=\"http://www.electroblog.org\">teknoloji</a> website, that uses KOffice."
    author: "khthelegend"
---
In <a href="http://commit-digest.org/issues/2010-11-28/">this week's KDE Commit-Digest</a>:

              <ul><li>Non-destructive editing (with an interface fully adapted to versioning) arrives in <a href="http://www.digikam.org/">Digikam</a>, along with work on demosaicing and interpolation options, and filters</li>
<li>Drag-and-drop support for launchers, and version 4 of the SlimGlow theme in <a href="http://plasma.kde.org/">Plasma</a></li>
<li>User interface improvements in KFileAudioPreview</li>
<li>Better use of native file dialogs on the Maemo 5 and Windows CE platforms</li>
<li>First (incomplete) version of a settings dialog for the message list, and a variety of message sorting options in <a href="http://kontact.kde.org/kmail/">KMail</a> Mobile</li>
<li>Loading of cover art from MusicBrainz in <a href="http://www.periapsis.org/tellico/">Tellico</a></li>
<li>Various new features in Skrooge</li>
<li>Lots of work to improve import filters in <a href="http://koffice.org/">KOffice</a></li>
<li>Continued work in the Muon Package Manager (particularly on the History view), and on new mimetype icons in <a href="http://oxygen-icons.org/">Oxygen</a></li>
<li><a href="https://projects.kde.org/projects/extragear/utils/nepomukshell ">Nepomukshell</a> moves from kdereview to extragear</li>
<li>Better support for running <a href="http://kst.kde.org/">Kst</a> on Mac OSX</li>
<li>The Git <a href="http://dolphin.kde.org/">Dolphin</a> plugin can now be used on any supported KDE platform</li>
<li>Nepomuk "Web Extractor" moves to Git.</li>
</ul>

                <a href="http://commit-digest.org/issues/2010-11-28/">Read the rest of the Digest here</a>.