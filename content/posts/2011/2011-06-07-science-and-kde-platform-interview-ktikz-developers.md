---
title: "Science and the KDE Platform - An Interview with the KtikZ Developers"
date:    2011-06-07
authors:
  - "einar"
slug:    science-and-kde-platform-interview-ktikz-developers
comments:
  - subject: "Many thanks"
    date: 2011-06-07
    body: "I didn't know about Ktikz until now. I've been using tikZ. Still not very experienced at this point, but it seems very powerful. Yes the whole process of developing a figure can be quite a pain, so I'm quite happy to see a nice frontend being developed. So thanks, and wishes of good luck. "
    author: "jadrian"
  - subject: "Huge thanks"
    date: 2011-06-09
    body: "I've been struggling with creating basic diagrams (Euclid style) for years und thanks to your interview, I discovered TikZ. With your beautiful front-end, getting started is just a real joy!"
    author: ""
  - subject: "Extremely useful"
    date: 2011-06-09
    body: "I always use this tool when I have some non-trivial TikZ drawing to do. It is one of the best pieces of software that is not included in the official debian repos. Obviously not a licensing issue, but I can imagine some extremely important bureaucratic reasons why this should not be included, such as bad layout in the man pages ... Seriously, debian people, accept this package already."
    author: ""
  - subject: "I haven't found much use for"
    date: 2011-06-13
    body: "I haven't found much use for the use of LaTEX yet, largely because I am no tyet a computer scientist. I mostly want to comment on the use of Java.\r\n\r\nI personally do not like Java, though my experience with it is limited on the programmer side. I just know I really, really hate putting up with it as an end user.\r\n\r\nI also find it odd a Qt developer (I do have some good experience with Qt as a programmer and as an end user.) would actually see the need to use Java at all, even with Qt Jambi being around since Qt always struck me as accomplishing a lot of the same goals as Java (Portability, a more sane library and framework than the default C++ stuff, GUI development, event-based programming.) without a lot of Java's poor design decisions (Using the framework relies on you effectively being forced to use one single language or a family of languages, being limited on the sort of libraries you can use, the bloated, slow runtime environment and, by extension, the bloated, slow software).\r\n\r\nJust my two cents. "
    author: ""
  - subject: "Thanks for Ktikz. Used it a"
    date: 2011-06-21
    body: "Thanks for Ktikz. Used it a lot for my thesis in civil engineering. I was using it to preview the pgfplot-graphs representing the results of experiments.\r\n\r\nBesides great windsurfing day today at Neusiedler See. ;)\r\n\r\nHang loose."
    author: "mukl"
---
Many scientists use the LaTeX typesetting system as the preferred way to write publications. Among the various widely used add-ons, one special mention is the TikZ language, a powerful extension which is used to create publication-quality figures. Of course, like LaTeX, it takes its time to learn. The good news is that, like with <a href="http://dot.kde.org/2011/02/22/michel-ludwig-kile-kde-platform-4-and-git">LaTeX</a> there is KDE software to fill in this gap: <a href="http://www.hackenberger.at/blog/ktikz-editor-for-the-tikz-language">KTikZ, a graphical front-end to TikZ</a>.

As part of the KDE Science series, Luca Beltrame interviewed KtikZ's developers: Florian Hackenberger and Glad Deschrijver.
<!--break-->
<h2>The Developers</h2>

<b>Luca:</b> Hello, and thanks for taking part in this interview. Can you tell us something about yourselves?

<b>Glad:</b> I am Glad Deschrijver, I am a mathematician with some programming skills. In my free time, I read books (usually, but not limited to, fantasy, science fiction and classical authors), I write software, I watch the news, movies, American sci-fi series, documentaries on National Geographic, BBC, Arte, ... I do not have a Facebook account since I don't like the idea of my personal data becoming the property of a private company and being available to everyone, and I hate not being able to control my own data. I do not use Twitter since I do not have time for that.

<b>Florian:</b> My name is Florian Hackenberger. I am a Master of Computer Science and I'm currently running my own company. As a professional, I'm fluent in Java, C++ and various dynamic languages. I spend my (precious) spare time with my girlfriend, my friends, my family, a few books (mostly for entertainment), a bit of theatre and a lot of windsurfing in the Spring. As opposed to Glad, I do have a Facebook account, but only share a minimal amount of information, for the same reasons as Glad does not have an account at all. In the end you have to realize that Facebook is a company, and you just happen to pay for their services with your data, instead of your money. So keep an eye on what you share with them.

<b>Luca:</b> Are you working with any other free software or KDE projects?

<b>Glad:</b> Occasionally (when I find time) I submit a bug report for KDE software or a small patch. I also develop Qonjugator, which is supposed to become a free conjugator for all European languages (it works with plugins for each language), but the development is very slow because of lack of time and because no one helps me. Currently, there is a Qt-only library with Qt-only plugins, a Qt-only app and a Plasmoid (which is of course kdelibs-based). I plan to make also a QML-version. Maybe, when the QML-version is finished, I will transform the Qt-only app into a KDE-only app.

<b>Florian:</b> Yes, related to a commercial project of my company, I created the <a href="http://code.google.com/p/opensoftphone/">opensoftphone</a>, a Java SIP client based on <a href="https://sourceforge.net/projects/pjsip-jni/">pjsip-jni</a>, another project of mine. Just recently, I released the first version of <a href="http://code.google.com/p/reincrud/">reinCRUD</a>, an annotation-based framework for creating database applications in Java with <a href="http://vaadin.com/home">Vaadin</a> (which is based on the Google Web Toolkit), <a href="http://en.wikipedia.org/wiki/Hibernate_%28Java%29">Hibernate</a> and <a href="http://www.springsource.org/">Spring</a>. I created it as I found Vaadin to be very productive to work with, but it was lacking a CRUD/RAD part.

Unfortunately, since switching to Java as the language of choice, I have not yet found the time to work on another KDE-related project, but I have tried QtJambi and found it quite pleasant to work with. Maybe I'll use that for my next KDE app.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/ktikz.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/ktikz-wee.png" /></a><br />KtikZ main window</div><h2>The Application</h2>

<b>Luca:</b>  What was the motivation for developing KTiKZ? How does it compare to other similar applications?

<b>Florian:</b> KtikZ originated as a means to an end for making me productive while I was working on my diploma thesis. The thesis involved studying and explaining the properties of dynamic equation systems based on coupled, non-linear oscillators. Creating good-looking figures for the thesis was crucial and after evaluating a few systems (XFig, Inkscape, gnuplot and Asymptote), I settled on TikZ.

In order to speed up the edit/compile/test cycle, I put my figures in their own files (separate from the document) and wrote a little script which copied the TikZ figure into a LaTeX template, compiled it and refreshed the PDF viewer. As I am a bit of a perfectionist, I found that ad hoc approach still a little too inefficient and thought that I might as well invest a few days to create an easy-to-use program which I could share with other TikZ users, especially as it took me very little effort compared to the time other TikZ users without programming skills could save. I kept improving the first prototype until I was ready to release it on kde-apps.org. After finishing my studies and starting working for my own company, I found that I had very little time to work on my little gem. But then Glad came along, implemented the missing parts and made KtikZ what it is today, a tool aligned with the Unix philosophy of doing one thing, but doing it well.

<b>Glad:</b> When I started with LaTeX in 1998 (I was strongly fed up with the bugginess and user-unfriendliness of Word 98), I made my pictures in XFig. Unfortunately, I hit the limitations of XFig quite early, so since then I had been looking for a user-friendly and powerful way of making pictures for LaTeX. Throughout the years, I tried PSTricks and MetaPost, and ultimately I found TikZ, which is part of the pgf package for LaTeX. I was very happy to have finally found a relatively easy and very powerful means of making pictures for LaTeX (which also works with pdflatex). Unfortunately, using TikZ required recompiling the whole LaTeX document, and I got bored with waiting for these recompilations to finish (especially because I recompile each time that I change something and my documents are not always small; I have thoroughly rewritten two courses of around 200 pages and added all the pictures in both of them).

So I decided that I needed a program which has an editor for the TikZ code and a preview of the output and which recompiles automatically whenever I change something in the editor. A few weeks after deciding that I needed such a program and a few weeks before creating it myself (deciding that spending much time to create a program from scratch is really worth the effort takes some time), Florian posted KtikZ version 0.1 on kde-look.org, so I happily downloaded it and I started using it. Of course, I wanted to make my TikZ-editing life as easy as possible, so I started to add features and I submitted the patches to Florian.

<h2>Choosing the KDE Platform</h2>

<b>Luca:</b> Why did you choose Qt and the KDE Platform for KTiKZ?

<b>Glad:</b> KtikZ version 0.1 was a Qt-program. Of course I was happy that it was, otherwise I would probably have made a Qt-version myself. In the past I had made programs using the Java AWT and Java Swing, both of which were horrible experiences (NetBeans Swing GUI Builder didn't exist at that time, since it was released after I stopped using Java Swing, so I never tried it). I vaguely remember also having looked at the Gtk. When I discovered Qt, I was immediately hooked. It is much better than anything I had seen before. The motto "Code less, create more" is not mere propaganda, it is actually true. I also like the "deploy everywhere" part in the slogan.

Furthermore, I wanted KtikZ to be well integrated with the rest of my desktop (which of course is a KDE desktop). KtikZ was originally a Qt-only application, but at a certain moment I decided that I really needed some KDE-features like configurable toolbars, accessing my remote TikZ files and template files with KIO, and having a KPart which nicely displays the preview in Konqueror (yes, I still prefer Konqueror over other browsers such as rekonq, Firefox, ...), so since version 0.10 there is also a KDE-version.

<b>Florian:</b> Well, there are just two mainstream Graphical User Interface toolkits on Linux, GTK+ and Qt. I was working with C++ for my diploma thesis and always found the C-based object oriented approach of GTK+ a bit awkward, so Qt was the obvious choice. Glad did the KDE integration later on, which brought the famous network transparency (KIO) and nice file dialogs to KtikZ users.

Although I'm currently working on a Gnome-based Ubuntu Laptop, I still like the KDE desktop and its applications very much. I moved from KDE on Gentoo, which is a great distribution if you want to study the inner workings of an operating system by the way, to KDE on Ubuntu for productivity reasons during my studies. Later on I switched to Gnome on Ubuntu, because I felt that KDE on Ubuntu was a bit of a stepchild for the Ubuntu developers. With every upgrade there was something which worked nicely on Gnome, but was half-baked on Kubuntu. OpenSUSE would probably be a better distribution for using KDE, but I prefer a Debian-based system. The most important applications on my desktop, my terminal emulator (Konsole) and my email client (Kontact), are still KDE software though.

<h2>Future Plans</h2>

<b>Luca:</b>  Do you feel that KTiKZ is feature-complete? If not, what are your long-term plans? How can people help development?

<b>Glad:</b> In multiple image mode, it should be possible to export the currently visible image to PDF (now all the images are saved in one file with multiple pages). The tikzcommands list should be completed (that list is used both for syntax highlighting and command completion). The TODO list contains some other items, but these are the most important things. I would really be happy if people could help with the tikzcommands list (and make sure that the list is completed in such a way that the "Insert" menu is still usable). I would also be happy if someone could add the Mac-specific stuff to the cmake and qmake configuration files and make sure that KtikZ is well-integrated in a Mac-environment. I have no Mac so I cannot do it myself.

<b>Florian:</b> As Glad mentioned already, one area needing a bit of help is the list of command completions. The command completions are something which can bring down the initial learning curve for TikZ, which I reckon is most important because being familiar with TikZ is a prerequisite for being a happy user of KtikZ. And we want many more happy users! Another area we are currently working on is distribution integration. We are heading for Debian inclusion, which would bring KtikZ to Ubuntu users in one release cycle as well.

<h2>Strengths and Weaknesses of the KDE Platform</h2>

<b>Luca:</b> Did you encounter any difficulty while developing on the KDE Platform?

<b>Florian:</b> Luckily, Glad did the heavy lifting of making the buildsystem support both a Qt and a KDE build, which is an area where KDE could definitely make things a bit easier for developers, although I recognize that having a Qt only version of a KDE application is not the common case. If you stick with KDE only, the development platform is very pleasant to use. Especially since the switch to cmake and the creation of the <a href="http://techbase.kde.org/">techbase</a>.

One change I would like to see within the Linux ecosystem in general is an increased focus on Java. C++ is a very versatile language and has speed and memory advantages (native code, shared libraries), but developing applications is much more productive in Java, as there is a vast set of great libraries and tools available. The combination of Eclipse, Maven and Hudson is almost unrivalled, even when compared to commercial offerings. But Java as a primary language for Linux desktop development suffers from its portability. I would like to have a set of Java libraries for platform specific development on Linux and a release of the JVM which is tightly integrated with Linux. Something like what Dalvik and the Android SDK are for Android. Still Java, but heavily optimized for a specific platform. This is not to say that the cross-platform nature of the Java platform is a bad thing, it's just that in my opinion there is a usecase for both, and I think that Linux as a platform could benefit a lot from leveraging Java.

<b>Glad:</b> Developing with the Qt-toolkit is much easier than with anything else that I tried. Developing for the KDE Development Platform is equally easy and some parts are even easier. Unfortunately, when you want to develop both a Qt-only and a KDE-version of the same app you are in trouble because several things are done quite differently in KDE than in Qt. I had to write several wrapper classes (for things like a color dialog, url, line edit, actions, ...) which wrap the Qt classes when compiled in Qt-only mode and which wrap the KDE classes when the KDE-version is compiled.

But the worst was the translations and the documentation. Qt uses the .ts format (which is an XML-format) and which is installed relatively easily (without using shell-scripts). The only disadvantage is that you have to load the translations yourself in the code of your application. KDE uses the .po format and requires the use of some scripts in order to create the necessary files (luckily those scripts are available on Techbase, but I had to modify them to work with multiple source directories). Since not all strings in the KDE-version are available in the Qt-only version and since the Qt-only version should be installable without using any KDE-stuff, I decided that the strings of the Qt-only version should be in the .ts format. Since the KDE-version and the KPart use .rc files for the menu- and toolbars, I was forced to use the .po format for the strings in these files, so I used the .po format for all strings that are only in the KDE-version. Quite some work is needed to support both translation systems and have them work together.

Concerning the documentation, in my opinion, the KDE way of dealing with it is much better than the Qt way. In KDE you edit a file in the docbook format (which is an XML-format) and the KDE cmake scripts do everything else. In Qt you have to create HTML files and two configuration files which have to be compiled into a binary format, and those binary files must then be installed. Automating this procedure with qmake is quite hard to achieve. Furthermore, you must write a wrapper class around Qt Assistant to load the documentation. Since I don't want to write the documentation twice, I had to write a conversion script from the docbook files to HTML (luckily there exists an XSLT stylesheet for that, but other transformations were needed as well). I didn't expect that it would be that much work to have a KDE-version together with a Qt-only version (and to make sure that the KDE-version really feels like a KDE-app).

<h2>KDE Software and Science</h2>

<b>Luca:</b> What are your opinions on KDE and science, and Free Software and science in general? What are you favorite KDE science applications?

<b>Glad:</b> I like KDE software very much, it is more user friendly than the corresponding Windows and Gnome software. I use KWrite, Kile, Konqueror (for web browsing and local file browsing, I often have Konqueror windows with several tabs containing local PDF files, local LaTeX source files, remote PDF files, PDF files on the web,..., all dealing with the same research subject; no other app is usable for this use case), Okular and KtikZ.

<b>Florian:</b> Free software and science share a lot of common values. They both rely on the willingness to share results and both communities get stronger the more their members share with each other. But I think that most scientists are still not open enough. I can only speak for the computer science community, but it seems that it is still not common for scientists to share their code. The reason for that might be the way scientists get their funding nowadays. It is increasingly common to produce as many papers as possible, even if they are basically mostly the same, because that's what the institutions providing the funding base their decisions on. But that's not the best for the advancement of science in general. If you were required to share your code and your data in order to publish a paper, someone else could immediately build on your ideas and results. This is something you would probably tend to avoid, in order to get another round of funding.

Sure, that might require a different model of funding research, but bringing the culture of the free software community to the science community could be a huge benefit for both. Research results are often not directly usable in a commercial product, but free software developers might find a new approach interesting enough to produce a piece of software to leverage it. Reproducing research results without having the code from the researcher is something which takes too much effort for someone working in their spare time. For the scientists it would probably be beneficial as a wider audience would become aware of their research efforts, something which is quite difficult without a product to use.

The KDE science application I use most is definitely Kile. It's a great LaTeX frontend and I can recommend it to everyone who writes their documents in LaTeX. Although it is not a KDE application, I can also recommend LyX for LaTeX users who prefer to be shielded from the LaTeX syntax while writing.

<h2>Final Thoughts</h2>

<b>Luca:</b> Anything else you would like to add?

<b>Florian:</b> Thanks go out to all users and contributors of Free Software and especially to Luca for his interest in KtikZ!

<b>Glad:</b> Thank you for your interest in KtikZ!

<b>Luca:</b> Thanks for talking to us and best of luck with your future development of KtikZ and your other projects.