---
title: "Schedule for Workshops and BoFs at the Desktop Summit"
date:    2011-07-23
authors:
  - "kallecarl"
slug:    schedule-workshops-and-bofs-desktop-summit
---
The <a href="http://desktopsummit.org">Desktop Summit</a> is only 2 weeks away. Excitement builds!

The <a href="https://desktopsummit.org/program">program</a> consists of 3 days of talks, followed by 4 days of workshops and BoFs. In these sessions, Free Desktop contributors and enthusiasts gather face-to-face to discuss and work on current topics.

The organizing team is glad to announce the schedule of the <a href="https://desktopsummit.org/program/workshops-bofs">pre-registered workshops and BoFs</a>. Now participants can also start registering and scheduling more sessions. Attendees themselves are responsible for scheduling BoFs and workshops that are registered from now on.

Please visit <a href="http://wiki.desktopsummit.org/Workshops_%26_BoFs/Schedule">Workshop & BoFs</a> to see what is already scheduled. If there's a topic that you want to add, register and get more information on <a href="https://desktopsummit.org/program/workshops-bofs">the Workshop and BoF page</a>. Some slots will be reserved for filling in Berlin during the conference. 

The Workshop & BoF sessions at the Desktop Summit already cover a wide range of subjects—from hardware enablement to application development and community work. Topics also include multimedia, groupware and personal information management (PIM), build systems, community management, translations and the integration of web technology in the desktop. The attendee-managed sessions will add to the flood of open source goodness. You should be there!
