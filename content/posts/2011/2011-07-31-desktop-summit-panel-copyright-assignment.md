---
title: "Desktop Summit Panel on Copyright Assignment"
date:    2011-07-31
authors:
  - "kallecarl"
slug:    desktop-summit-panel-copyright-assignment
comments:
  - subject: "That doesn't sound particularly balanced to me..."
    date: 2011-07-31
    body: "More like caged death-match: Mark Vs The other people, even though I'm not particularly for (or against) copyright assignment.\r\n\r\nEven the tone of the article is rather slanted \"an effort *they* compare...\" \"Of *course* developers want...\" etc.\r\n\r\nI just hope that we get to hear both sides manage to make their legitimate points without it just becoming a knife fight."
    author: ""
  - subject: "Interesting, I don't see it that way"
    date: 2011-07-31
    body: "I think Carl's article is pretty balanced. \"They compare\" is because afaik only Canonical have made that comparison (doesn't mean it's invalid). It's just like numerous articles following one of our release days that say \"according to KDE, highlights of the release are...\". The second quote you lift is from a paragraph that explicitly sets both arguments.\r\n\r\nAs for the panel:\r\nMark is pro\r\nMichael is anti\r\nBradley is in between (against the Canonical approach afaik, but not totally opposed in principle)\r\nKaren, as moderator, will presumably take a neutral stance/play devil's advocate\r\n\r\nBut yes, like most people (I hope), I'd like to see a reasoned and civil debate. There are plenty of copyright assignment agreements around and developers and companies need to understand them and work out what is acceptible to all."
    author: "Stuart Jarvis"
  - subject: "Middle ground"
    date: 2011-08-01
    body: "I could agree with Bradley Kuhn: if I have to assign my copyright the same way is usually done at business, the asignee should be a trusted organization, not an enterprise. IIRC, not even Nokia requires you this days copyright assignment to contribute to Qt, just he rights to relicense the code, the same way the fiduciary agreement of KDE."
    author: ""
  - subject: "I thought it was the idea of the GPL in the first place"
    date: 2011-08-02
    body: "I read Shuttleworth's blog too and I still don't get what the problem with the GPL license is with respect to contribution. If a guy takes GPL software and does something nifty with it, upstream is free to assume to adopt the code and maintain it whether or not the contributor can maintain the code. For contributors its about the possibilities and opportunities, and copyright takes away from that satisfaction -- that's why the GPL was created to create a healthy give and take environment. From a business perspective companies thrive from assuming irrevocable ownership because they can assign monetary value to things they own. Its understandable that Shuttleworth has to figure out a way to convince investors of Canonical's and Ubuntu's value. For signing a contributor agreement a contributor that quits for whatever reason walks out with the clothes on his/her back, no pride of ownership or money. I don't know of contributors that don't like to at the least \"have their name on the door,\" in that if you use a library in your work just mention their name. To me it appears that the issues that the GPL was designed to solve are creeping back to rear their ugly head in Free and Open Source software. The GPL creates a two way street that is more neighborly. I think Canonical still has the option of going the BSD route, though it would be sad to lose them as Linux contributors."
    author: ""
  - subject: "Comparison to CC"
    date: 2011-08-04
    body: "Well, this shouldn't be much of a debate if they really are comparing it to CC, all they have to do is figure out which the actual options are and do as CC did, make a CA-CT (Community Agreement, Copyright Transfered) and a CA-CR (Community Agreement, Copyright Remained) or something."
    author: ""
  - subject: "Americans"
    date: 2011-08-05
    body: "They are all from an anglosaxon background where you have a copyright regime, not droit d'auteur. We tried to explain it to RMS, to SUN, to so many others, it is not legal to fully cease your copyright under a droit d'auteur regime. It is totally different and most agreements to not work in our legal environment. Please offer us a model that works under droit' d'auteur and does not contravene the law."
    author: "vinum"
  - subject: "Why!"
    date: 2011-08-06
    body: "I agree with vinum raising the American perspective issue. I also agree with Anonymous raising the point \"I still don't get what the problem with the GPL license\"!.\r\n\r\nNow my question I like to raise is \"Has any one given any good reasons for copyright assignment? What is the problem? the purpose? And what are the benefits? etc...\r\n\r\nI believe that , to start a good discussion or dialogue, the proponents and opponents need to clearly and in their own words, start answering these question before anything else.\r\n\r\nOne middle solution would be to have two copyright owners. A primary, which would be the developer(s) and a secondary, which would be a trusted FOSS organization. The copyright of the secondary doesn't kick in unless the primary gives up the copyrights or impossible to contact.\r\n\r\n It is not that difficult, or is it?\r\n\r\n"
    author: ""
---
Should free and open source projects and companies require developers to sign community agreements in order to contribute, possibly signing over their copyrights? Some do, some don't, others have mixed policy. Canonical is leading an effort, called "Harmony", to create standardized community agreement documents, an effort they compare with the license standardization work of Creative Commons.

Of course, developers want to keep the rights to their creations. On the other hand, some project and business leaders say that they must have copyright assignments to be successful. Is there a middle ground?

Copyright assignment is a controversial topic in the FOSS community. The Desktop Summit is fortunate to have four leading thinkers on the issue who are willing to engage openly in a <a href="https://desktopsummit.org/program/sessions/panel-copyright-assignment">panel discussion about copyright assignment</a>. It's hard to imagine four people better suited.

<h2>Panel Members</h2>

Canonical's <b>Mark Shuttleworth</b> is an entrepreneur and a proponent of copyright assignment. Unity and other Canonical projects require copyright assignment.

<b>Michael Meeks</b> is a developer and an opponent of copyright assignment. He worked on the OpenOffice.org (OOo) project, and is one of the core developers instrumental in the creation of LibreOffice, based on the OOo codebase. A major factor in the LibreOffice fork is the copyright assignment required to contribute to OOo.

<b>Bradley Kuhn</b> is the Executive Director of the Software Freedom Conservancy and a Director of the Free Software Foundation. He has an extensive background in free software licensing and advocacy, and has given much thought to conflicts between software freedom and other considerations, such as business, fundraising and cloud-based computing. In Bradley's view, copyright assignment is acceptable if the assignee is a trusted nonprofit organization.

The panel will be moderated by <b>Karen Sandler</b>, the GNOME Foundation's new Executive Director, who was previously the General Counsel of the Software Freedom Law Center.

The panel should appeal to everybody who is involved in free and open source software—hackers, community managers, other kinds of contributors, business managers or lawyers.