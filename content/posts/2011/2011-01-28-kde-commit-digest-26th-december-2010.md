---
title: "KDE Commit-Digest for 26th December 2010"
date:    2011-01-28
authors:
  - "dannya"
slug:    kde-commit-digest-26th-december-2010
comments:
  - subject: "Huh?"
    date: 2011-02-01
    body: "What's an earthquake online service plugin?"
    author: "tangent"
  - subject: "I would guess"
    date: 2011-02-01
    body: "I would guess that it lets you know about recent Earthquakes around the world, possibly to be plotted on the map with magnitude...\r\n\r\nDanny"
    author: "dannya"
  - subject: "Earthquakes"
    date: 2011-02-01
    body: "Will wonders never cease...\r\n\r\nPerhaps there should also be a \"donate to red cross\" button when they cross a certain magnitude!\r\n"
    author: "tangent"
---

In <a href="http://commit-digest.org/issues/2010-12-26/">this week's KDE Commit-Digest</a>:

              <ul><li>New features in <a href="http://edu.kde.org/marble/">Marble</a>, including support for imperial units when generating driving instructions, new earthquake and OpenDesktop community online service plugins, a map creation wizard</li>
<li>Massive work in <a href="http://skrooge.org/">Skrooge</a> on budgets and better integration with Mac OS X</li>
<li>Work on dynamic playlists and podcasts in <a href="http://amarok.kde.org/">Amarok</a></li>
<li>Bugfixing in <a href="http://edu.kde.org/marble/">Marble</a>, <a href="http://www.digikam.org/">Digikam</a>, <a href="http://krita.org/">Krita</a>, KWin, throughout <a href="http://plasma.kde.org/">Plasma</a> and its Addons, and <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">KDE-Telepathy</a></li>
<li>Nuno Pinheiro completes the new 32x32 mimetype icons</li>
<li>3 new themes are added to the KDE Plasma Workspace: "Androbit", "Tibanna", and "Produkt"</li>
<li>Air theme is updated and <a href="http://lancelot.fomentgroup.org/main">Lancelot</a> themes are updated to match</li>
<li><a href="http://utils.kde.org/projects/ark/">Ark</a> gains RPM support</li>
<li><a href="http://edu.kde.org/kalzium/">Kalzium</a> becomes Git-ready</li>
<li><a href="http://www.kde.org/applications/office/kontact/">KDE-PIM</a> and <a href="http://strigi.sourceforge.net/">Strigi</a> move fully to Git</li>
<li><a href="http://www.kde.org/announcements/announce-4.6-rc1.php">KDE SC 4.6 RC1</a> is released.</li>
</ul>

                <a href="http://commit-digest.org/issues/2010-12-26/">Read the rest of the Digest here</a>.