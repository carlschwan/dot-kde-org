---
title: "KDE Makes First 4.8 Release Candidate Available, Adds Secret Service"
date:    2011-12-22
authors:
  - "sebas"
slug:    kde-makes-first-48-release-candidate-available-adds-secret-service
comments:
  - subject: "Bug 275469 - 4.7 Regression: closed windows stay in the taskbar "
    date: 2011-12-22
    body: "I think this bug should have the highest priority for fixing.\r\nFor us, users, it is highest priority (look at \"most hated bugs\" on bugs.kde.org).\r\n"
    author: ""
  - subject: "Graphics"
    date: 2011-12-22
    body: "Does RC include new wallpapers and icons or are they coming in final?"
    author: ""
  - subject: "RE: Graphics"
    date: 2011-12-22
    body: "Yes, it comes with new wallpapers. What new icons are you talking about?"
    author: "hrvojes"
  - subject: "patches"
    date: 2011-12-22
    body: "patches are welcome.\r\n\r\ni am not yet sure this is at all a 4.7 regression. it may well be (another) X11 Qt bug or even deeper in the stack -> for those affected (and it's a small number it seems), the application is simply NOT getting the event that the window has been removed into its x11 event filter. we've seen this right down in the event filter in libkdeui in KWindowSystemPrivate::x11Event. i wonder if it might even be Qt version related: there is a bug that slipped in sometime during Qt 4.7 that gets event loop ref counting wrong in x11EventFilter .. hm.\r\n\r\nand/or perhaps its related to a specific widget that some people have which is eating events or otherwise triggering the problem? most oddly, the event does get delivered to other apps, just not the plasma-desktop process. of course, those other apps don't do things like track the open windows or other things that rely on the x11EventFilter. can't be the xembed systray icons since i have a few of those and never see this issue.\r\n\r\nin short, it would be nice if we had a solid method of reproduction, but we don't even have that.\r\n\r\nmeanwhile, i also have a list of other serious issues that are more common. posted them to the mailing list the other week.\r\n\r\nbtw, the \"most hated\" thing is utterly bogus on that report. it was a set of 2-3 different issues, all of which got addressed. it is currently re-opened due to a completely different problem that shares symptoms. the votes and dupes on that report do not represent the current issue, but the previous fixed ones. if it was a new report with just the remaining (new?) issue it would be clearer.\r\n\r\nand yes .. another first post that is a bug report #. and every time it's pointed out that this is not the most useful venue for that. and every time there's a set of comments with bug report #s. it's a little like working with a group of people who, collectively, suffer from a learning disorder. ;)"
    author: ""
  - subject: "Plasma bugs"
    date: 2011-12-22
    body: "Then how about communicating with the users who suffer from this bug, asking them to provide further information (e.g. widgets used) that can help? \r\n\r\nThis bug is now open for half a year, and especially in the beginning it looked like nobody of the developers care. It started to lighten up a bit when a, at this time, external developer jumped in. This is a feeling I get quite often when submitting bugs regarding plasma parts. \r\n\r\nOther parts and developers at least give some feedback. And yes, I do know that there are tons of open bugs, and probably a lot of them are duplicates or missing information. But still, adding bugs which might have quite an impact  (like this one, or the broken plasma parts in amarok) often seem to get little attention based from a bug submitter / users view. And this can be frustrating, almost as frustrating as having to live with such an annoying bug for half a year now. \r\n\r\nAnyway, thanks for the otherwhise good work. \r\n"
    author: ""
  - subject: "Please help by triaging the bugs"
    date: 2011-12-22
    body: "Each day about 9 bugs are reported against Plasma. But only about a third of the reported bugs are actual bugs. Everything else is either invalid or duplicate. This is a huge amount of bugs the developers have to go through.\r\n\r\nIt just can happen that one of those bugs skips through the radar of the developers. That doesn't mean that developers do not know about the bug. It might be that there are tens of other bugs describing the same issue where the developers report to. It might be that the issue is already discussed on IRC and the mailinglists without even consulting the bug tracker. This specific bug was reported in a time when people are on vacation. Two weeks gone and more than 100 new bugs are reported.\r\n\r\nBut there is good news: you can help that this does not happen in future any more. You can help by triaging the bugs. Just think about the advantage of users filtering out the duplicates instead of the developers. You can try reproducing the bugs and tell the developers in the reports how to reproduce them. This gives the developers the possibility to just grab the fully analyzed bug and fix it.\r\n\r\nSo everybody can help. You don't need to be able to develop, all you need is a recent version of our software. Give it a try: <a href=\"http://techbase.kde.org/Contribute/Bugsquad\">Bugsquad</a>\r\n\r\nIf the developers know your capabilities they trust your analysis and you can easily point them to: look at this bug, that looks pretty severe."
    author: "mgraesslin"
  - subject: "How do I become a triager?"
    date: 2011-12-22
    body: "I found myself triaging some really old bugs (filed against KDE 4.1), but I don't have the privileges to mark bugs as \"NEEDINFO\". How can I get a bugs.kde.org account upgrade?"
    author: "Alejandro Nova"
  - subject: "Hang out in #plasma or"
    date: 2011-12-22
    body: "Hang out in #plasma or #kde-bugs. As soon as you show yourself worthy, you will be upgraded."
    author: ""
  - subject: "I am pretty sure that it is 4.7 regression"
    date: 2011-12-23
    body: "Because in Kubuntu 11.04 with default 4.6 I didn't have this problem. But when I installed kde 4.7, this bug appeared. So bug appeared with new kde, all the rest was the same (drivers, X, qt)."
    author: ""
  - subject: "What lists?"
    date: 2011-12-23
    body: "I don't do IRC.  Are there mailing lists to hang out in or is this an IRC-only club?  I'm already a regular on kde-general and kde-linux.\r\n\r\nMeanwhile, what about superkaramba?  I have a couple bugs up that even have patches.  I use them here regularly as I have a superkaramba monitoring theme, and I've tried to upstream them, but for all I can tell from the bugs sitting without any response at all for six months, everyone else died in a nuclear holocaust and I'm the last person (well, at least the last superkaramba user) alive...  #274906, 275070  Maybe bringing them up where will get a response?\r\n\r\nDuncan\r\n\r\n(My email's on the lists above, if anyone's interested. I tried posting with an email address here some years ago and had to shut down that address due to the instant spam load after that post!)"
    author: ""
  - subject: "patches in bugreports usually"
    date: 2011-12-23
    body: "patches in bugreports usually get lost. try submitting them to git.reviewboard.kde.org there they get looked quite fast."
    author: ""
  - subject: "After some hard testing..."
    date: 2011-12-23
    body: "...there are results. Great results.\r\n\r\nWith Akonadi 1.6.90 and KDE 4.8 RC 1, we say: farewell, Nepomuk + Akonadi performance pains. It was a pity to have you here, tarnishing KDE reputation. But we won't see you anymore. Farewell.\r\n\r\nAfter I had a solid 5% idling CPU for 1 hour, I can, finally, claim victory and say that Akonadi and Nepomuk are not the resource hogs they were in the past, anymore.\r\n\r\nCongratulations and thanks to everyone involved in this."
    author: "Alejandro Nova"
  - subject: "Actually I could swear that"
    date: 2011-12-23
    body: "Actually I could swear that that was a feature. Never thought it was a bug ;)"
    author: "Bobby"
  - subject: "KMail 2 and Digikam"
    date: 2011-12-23
    body: "What about KMail2 and Digikam are they now working the way they should? Especially importing the mails from Kmail1 to KMail2. I still haven found a strait forward howto up to now. \r\nKmail seems to be quite stable and fast but how do I import my old mails?"
    author: "Bobby"
  - subject: "KSecretService"
    date: 2011-12-23
    body: "KSecretService looks like interesting stuff!\r\nDoes \"shared\" just mean \"with different applications\", or also \"with different users/clients\" (client/server architecture)?\r\n\r\nWhere can I find some more details? Google takes me to the devel mailing-list but not much more."
    author: "Vajsvarana"
  - subject: "1. Right; 2. Depends on Digikam"
    date: 2011-12-23
    body: "1. KMail2 should be working, since it's not attempting to import KMail1 mails anymore. Now it should use directly the KMail1 data, through a special Akonadi agent.\r\n\r\n2. Digikam must adapt to this new reality."
    author: "Alejandro Nova"
  - subject: "Any idea on how to use this"
    date: 2011-12-23
    body: "Any idea on how to use this agent? I have followed some instructions online and KMail2 is in fact working. I can use my KMail1 contacts and my e-mail settings but my old e-mails are nowhere to be found (in KMail2) Any ideas on how to \"import\" my old mails?"
    author: "Bobby"
  - subject: "Importing KMail messages into KMail2"
    date: 2011-12-24
    body: "Have you checked out forum.kde.org, forums.opensuse.org, \r\nhttps://wiki.kubuntu.org/OneiricOcelot/Final/Kubuntu/Kmail2\r\n\r\nIt looks like problems are being solved at forum.kde.org. Searching on kmail2 brings up quite a few threads. Maybe one of those will relate to what you are getting."
    author: "kallecarl"
  - subject: "get mails into kmail2"
    date: 2011-12-24
    body: "have you thought of using a (imap) mailserver as middleman.. upload to the server.. install kmail2 get them again..  i did this once with a LOT of emails..  took some time :)"
    author: ""
  - subject: "What problem digiKam is"
    date: 2011-12-24
    body: "What problem digiKam is having?"
    author: ""
  - subject: "Just all new wallpapers and"
    date: 2011-12-24
    body: "Just all new wallpapers and new oxygen icons. If they were included now on RC and not just final, as beta versions did not include those. "
    author: ""
  - subject: "ksecrets..."
    date: 2011-12-25
    body: "I had exactly the same questions.  Here on gentoo, the new ksecrets package has a couple readmes (and 3 todos) installed in /usr/share/doc/ksecrets-4.7.95/.  They don't say a whole lot in themselves, but one of them points to\r\n\r\nhttp://techbase.kde.org/Projects/Utils/ksecretsservice\r\n\r\nBasically, it's to be kde's parallel to gnome-keyring, with both implementing the XDG (X Desktop Group, freedesktop.org) secrets API over DBUS.  You'd pick one or the other.  But the techbase page still has a lot of to-be-done points, and because it wasn't in the betas so 4.7.95 aka 4.8-rc1 is the first public prerelease, it looks to me like basically a minimal stub is going to ship for 4.8, and it won't really be used much until 4.9.\r\n\r\nBut eventually it's to replace the kde-only kwallet with the XDG standard-speced dbus-based secrets API, and both kde/qt and gnome/gtk apps will be able to use it.  As the note at the bottom of the techbase article mentions, however, you'd pick either gnome-keyring or ksecretservice to store your secrets, because while the implement the same standard XDG secrets API, there's no automated way to convert one to the other, at least not yet.\r\n\r\nThat's all I know of it, but once I realized the stage it's at and that it's probably be like akonadi or nepomuk in early kde4, there, but not really doing a whole lot for a few feature releases, I wasn't too worried about it.  Like those technologies, I'm sure by the time it's actually of much use, we'll have quite a number of blog posts about it, etc, and know more what's going on.  Right now tho, it's pretty much just there, but not really doing a whole lot yet as it's not complete, and even if it was, it's too late to convert anything to it for the 4.8 cycle at least, so 4.9 at the earliest.\r\n\r\nDuncan"
    author: ""
  - subject: "Adobe Flash, Shutdown button"
    date: 2011-12-25
    body: "Still do not work correctly Adobe flash with effect in fullscreen mode. Do not work button in flash. And do not work corectly shutdown button on PC and on keyboard. Must long wait then show dialog to shutdown. In desktop PC. I using KDE 4.8 beta 2 maybe work fine in rc1 wait update kubuntu beta. "
    author: ""
  - subject: "bugs.kde.org, forum.kde.org"
    date: 2011-12-25
    body: "There are better places to get problems solved than at dot.kde.org. The Dot is for KDE news and announcements.\r\n\r\nAs the original story says...bugs can be reported, verified and found at bugs.kde.org.\r\n\r\nYou can also post in the 4.8 Beta forum...\r\nhttp://forum.kde.org/viewtopic.php?f=4&t=97973"
    author: "kallecarl"
  - subject: "Interesting anyway"
    date: 2011-12-26
    body: "mmm... I was really hoping for a different answer. :) \r\n\r\nThanks anyway for your informative post!"
    author: "Vajsvarana"
  - subject: "Bug flash"
    date: 2011-12-26
    body: "1. Flash: install Flash 11.2 beta 3 - http://labs.adobe.com/downloads/flashplayer11-2.html . That kills 99% of the crashes you may see with Flash 11.1 and improves greatly how things work in Qt-based browsers.\r\n\r\n2. Shutdown button: the easiest and quickest workaround is to press Ctrl + Alt + F1 and try the power button or Ctrl + Alt + Del."
    author: "Alejandro Nova"
  - subject: "No new graphics"
    date: 2011-12-26
    body: "RC1 does not have new wallpapers or new icons. All those are missing. The default wallpaper is still what 4.7 had (and it should have included five new wallpaper already). "
    author: ""
  - subject: "Dolphin"
    date: 2011-12-28
    body: "dolphin would appear the size of files when placed in detail, something that does not appear .."
    author: ""
  - subject: "more on this akonadi agent.."
    date: 2012-01-04
    body: "Please can you give some more info about this agent? is there a possibility to not store mail-addon-data in mysql anymore? where can I read some more about that topic?"
    author: ""
---
<p align="justify">
Today KDE <a href="http://kde.org/announcements/announce-4.8-rc1.php">released</a> the first release candidate for its Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing. Compared to Beta2, RC1 contains hundreds of fixes. Please give this release another good round of testing to help us release a rock-solid 4.8 in January.<br />
<!--break-->
Highlights of 4.8 include:

<ul>
    <li>
Qt Quick in Plasma Workspaces -- Qt Quick is making its way into the Plasma Workspaces. The new Plasma Components provide a standardized API implementation of widgets with native Plasma Look and Feel. The device notifier widget has been ported using these components and is now written in pure QML. KWin's window switcher is now also QML-based, paving the way for newly designed window switchers.
    </li>
    <li>
Dolphin's file view has been rewritten for better performance, scalability and more attractive visual appearance.
    </li>
    <li>
KSecretService optionally enables shared password storage, making your saved passwords available to many other applications, leading to a more secure system and better integration of non-KDE apps into the Plasma Workspaces and KDE apps into non-Plasma workspaces.
    </li>
    <li>
Many performance improvements and bugfixes improve the overall user experience, making the KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>

As usual, bugs can be reported, verified and found at <a href="http://bugs.kde.org">bugs.kde.org</a>.