---
title: "Could You Be the Voice of Marble?"
date:    2011-06-21
authors:
  - "Earthwings"
slug:    could-you-be-voice-marble
comments:
  - subject: "I would love to have the"
    date: 2011-06-21
    body: "I would love to have the voice of Lydia Pintscher in my navigation system. \r\n"
    author: "trixl."
  - subject: "What about Chani"
    date: 2011-06-21
    body: "Listen to Chani's voice, she's got a really soft and smooth voice! :)\r\nhttp://blip.tv/chanis-kde-screencasts"
    author: "panda84"
  - subject: "I think Whoopi :P"
    date: 2011-06-21
    body: "Who wouldn't want Whoopi Goldberg telling them where to go!?\r\n\r\nMorgan Freeman would be a close second."
    author: ""
  - subject: "Right"
    date: 2011-06-21
    body: "I love her voice too :D"
    author: ""
  - subject: "or Ann Widdecombe"
    date: 2011-06-22
    body: "or Ann Widdecombe"
    author: ""
  - subject: "love Chani's voice"
    date: 2011-06-22
    body: "i love Chani's voice too, looking to hear more from her."
    author: ""
  - subject: "why not a male voice and a female voice?"
    date: 2011-06-24
    body: "I've heard that a study was conducted among pilots and it was found that pilots (most of them male) responded better to a female voice. I think the inverse would happen to women. Therefore I believe it would make sense to have a male and a female voice."
    author: ""
  - subject: "Re: why not a male voice and a female voice?"
    date: 2011-06-24
    body: "Yes, ideally I'd like to have at least one male and female speaker for each language. We collect all contributions from the contest and make them available for download and use as alternative speakers in Marble.\r\n\r\nFor space reasons we will not ship all of them in the Marble packages though. We'll ship one english speaker and maybe also speakers for a selected number of other languages, that's not definite yet. If their desired speaker is not among those, people can easily install an alternative one.\r\n\r\n"
    author: "Earthwings"
  - subject: "Her \"Da da daaa, there it is!\" is a must!"
    date: 2011-06-29
    body: "Instead of a boring \"You have reached your destination\" I would really love to hear her \"Da da daaa, there it is!\" from \r\n\r\nhttp://blip.tv/chanis-kde-screencasts/activities-in-kde-sc-4-5beta1-3685388\r\n\r\nat 2m29s.\r\n\r\nWho needs synthies, sampler, effects, ... for sound effects, when we can have this! ;)"
    author: ""
  - subject: "Marble to provide a Nokia World Gaze style experience?"
    date: 2011-07-04
    body: "I have often wondered at the potential for marble, with its offline maps, to link in to an offline wiki client such as evopedia, and perhaps geotagged photos from your camera client.\r\n\r\nAnd then i came across this:\r\n\r\nhttp://mynokiablog.com/2011/07/04/nokia-3d-world-gaze-experimental-app-at-nokia-beta-labs-demoed-on-nokia-n8/\r\n\r\nIs it intended that marble should one-day include this kind of functionality?"
    author: ""
  - subject: "voice"
    date: 2011-07-06
    body: "why not espeak for a fall back, then you can choose"
    author: ""
  - subject: "voice"
    date: 2011-07-06
    body: "the mysterions? "
    author: ""
  - subject: "Why not use computer"
    date: 2011-07-12
    body: "Why not use computer generated voices?"
    author: ""
  - subject: "Freebie?"
    date: 2011-07-14
    body: "So this is the day before final submittals are due. before I submit recordings, just wondering... am I doing this for free since the recordings will be licensed under the creative commons law??  I guess it will look good on the resume..."
    author: ""
  - subject: "Re: Freebie?"
    date: 2011-07-16
    body: "Yes, you're doing this for free. We extended the deadline until 2011-07-20, so you have some more days to work on it.\r\n"
    author: "Earthwings"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/marble_maemo0.jpeg" /><br />Be the voice of Marble</div>Thousands of people use Marble on the Nokia N900 to find their way and explore the world. Become their voice! 

Record your voice speaking a handful of turn instructions like "bear left!" and participate in the <b>Voice of Marble</b> contest. With a bit of luck, your voice will be chosen as the default speaker for voice guidance in Marble's next version (1.2, to be released in July 2011).
<!--break-->
We're looking for an English speaker (male or female) whose voice will be shipped with the Marble packages. And we're also looking for alternative speakers for each language supported by KDE - at least one each, and that's a lot!

Please participate in the contest and spread the word among your friends. The five best contributions will get a cool Marble T-shirt as a little present.

Interested? Please head over to the <a href="http://community.kde.org/Marble/VoiceOfMarble">Voice of Marble wiki page</a> which contains all the details you need to participate. <b>The deadline for submissions is July 15th 2011</b>.