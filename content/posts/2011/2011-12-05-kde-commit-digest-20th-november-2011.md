---
title: "KDE Commit-Digest for 20th November 2011"
date:    2011-12-05
authors:
  - "mrybczyn"
slug:    kde-commit-digest-20th-november-2011
---
In <a href="http://commit-digest.org/issues/2011-11-20/">this week's KDE Commit-Digest</a>, Boudewijn Rempt writes about the 2.4 version of Krita. In addition, the usual list of updates:

<ul>
<li>Progress in the work to separate QGraphicsView from libplasma</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> sees work on docx support and autocolor</li>
<li><a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy</a> sees UI refactoring; there is now extended away support and dialog-less setup for link-local XMPP</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> gets 'space view' activity</li>
<li><a href="http://www.digikam.org/">Digikam</a> gains Aspect Ratio Crop tool and Kodak HIE infrared filter in Image editor converter</li>
<li><a href="http://plasma-active.org/">Plasma Active</a> gets UI for setting start page, adblock and history; lockscreen</li>
<li><a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a> can show the balance of a budget.</li>
</li>plus many statistics on bugs, wishes, commits, i18n, bug fixes
</ul>

<a href="http://commit-digest.org/issues/2011-11-20/">Read the rest of the Digest here</a>.

Be more active in KDE. You don't need to be a coder to be <a href="http://commit-digest.org/contribute/">part of the Commit Digest team</a>. 