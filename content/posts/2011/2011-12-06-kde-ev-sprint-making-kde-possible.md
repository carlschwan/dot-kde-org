---
title: "KDE e.V. Sprint - Making KDE Possible"
date:    2011-12-06
authors:
  - "Stuart Jarvis"
slug:    kde-ev-sprint-making-kde-possible
---
Over the  weekend of 19 and 20 November, KDE contributors met in Berlin for the KDE e.V. Sprint—the first ever. <a href="http://ev.kde.org/">KDE e.V.</a> is the non-profit organization that represents KDE in legal and financial matters and provides funding to assist KDE development and promotion. We aimed to work on some key areas to form a strong foundation for KDE e.V.'s future development, including:
<ul>
<li>The Individual and Corporate Supporting Membership Programs</li>
<li>KDE's policy on providing public thanks to donors of money, hardware and services</li>
<li>Budget and goals for 2012</li>
<li>Setting up a better method for approving sprint financing, planning and reporting</li>
</ul>
<!--break-->
Most of us arrived on Friday evening and had dinner with the attendees of the <a href="http://dot.kde.org/2011/11/30/kde-harmattan-sprint-makes-advances-mobile-space">KDE Mobile Sprint</a>. 

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -254px; width: 500px; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdeevsprint.jpeg" /><br />Sprint attendees (from left): Cornelius Schumacher, Stuart Jarvis, Paul Adams, <br />Lydia Pintscher, Agustín Benito Bethencourt, Claudia Rauch, Frank Karlitschek, <br />Pau Garcia i Quiles</div>

<h2>Saturday</h2>
Our real work began on Saturday morning. We started with a discussion of the topics, then split up into smaller groups to work on them. One subteam reviewed KDE's thank you page to past donors, and set up new criteria for who should get a mention on the page and for how long.

The other team reviewed the Individual Supporting Membership Program—the "Join the  Game" campaign—and identified improvements for the main webpage and the supporting infrastructure. We decided to make the whole program more interactive for members. Steps towards this goal include providing Supporting Members with more frequent updates on the progress in KDE and inviting them to local KDE events. We also brainstormed ideas for how to promote the program at events like FOSDEM and created an FAQ for booth volunteers.

To conclude the first day, we got back together to review the results from each team and discuss the topics further. That evening was rounded off with dinner at a local Syrian restaurant. And of course we had more discussion about the tasks finished, those still to be done and KDE in general.

<h2>Sunday</h2>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdeevoranges.jpeg" /><br />Pau's oranges kept us going.</div>
On the second morning, the Board members present had a short meeting, while the rest of us worked on the topic of sprints. Sprints are important to KDE because they foster collaboration and friendship in a community that mostly works online. In addition, they are effective at getting a lot done in a short time.

In the past, sprints have been approved in an ad-hoc fashion when requested. This has not been a problem as the relatively low (though growing steadily) number of sprints has made the system workable.  However, increasing sprint activity raises the concern that a limit may someday be reached. There's a need for a fairer system than what is now effectively first come, first served. We also determined that more advanced planning would provide an overview of sprint funding needs before money is allocated.

During a quick break for pizza, we were visited by a local researcher interested in building community dashboards, to discuss the potential relevance to KDE. Some of us sat down with her to discuss the KDE community, how we feel it has evolved over time, and the trends that might be interesting to track. Building a dashboard of these trends could help the community be more aware of how it is evolving and how this affects our work. This discussion helped to create some initial interest and to get everyone moving towards a common goal. We are looking forward to seeing this happen.

In the afternoon, everyone got back together to decide on the remaining topics to cover. We had a group discussion of ways to get companies already in the KDE ecosystem more involved, centered around enhancing the Corporate Supporting Membership and Patronage Programs. Agustin presented a proposal for creating a network of companies around KDE, building upon our expertise and existing contacts in companies working with KDE and Qt.

We also discussed the budget for 2012. The Board's draft budget was received positively. There was discussion of the areas in which KDE e.V. spends its money, the costs associated with some of our tasks, and ways to tighten up financial management.

Finally, a draft trademark policy for KDE was discussed. We studied existing trademark policies from other free software organizations and considered options for KDE. We want to protect the term "KDE" from misuse, but not make unnecessary difficulties.

In the evening, most of the team went for a final dinner at a Persian restaurant on the way back to the hotel.

<h2>Wrap-Up</h2>
Overall, the e.V. sprint was productive. Progress was made on a number of issues that are essential to keeping KDE running smoothly and maintain support for our contributors.

The sprint attendees give thanks to <a href="http://www.kdab.com/">KDAB</a> for providing meeting facilities in their Berlin office. We also thank Pau for bringing a supply of fresh oranges from Spain to keep our vitamin levels up.

If you want to contribute to KDE, please consider <a href="http://jointhegame.kde.org/">Joining the Game</a>.