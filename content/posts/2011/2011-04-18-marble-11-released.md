---
title: "Marble 1.1 released"
date:    2011-04-18
authors:
  - "tackat"
slug:    marble-11-released
comments:
  - subject: "Fedora packages"
    date: 2011-04-22
    body: "Fedora packages are now queued for updates-testing for Fedora 14 and the upcoming Fedora 15.\r\nhttps://admin.fedoraproject.org/updates/kdeedu-4.6.2-2.fc15\r\nhttps://admin.fedoraproject.org/updates/kdeedu-4.6.2-2.fc14"
    author: "Kevin Kofler"
---
The Marble Team has just released Marble 1.1. This release is special! With many new features being developed during Google Code-in, the Marble Team decided to get it out between the usual KDE application releases. The new version provides several new features and improvements:
<ul>
<li>Map Creation Wizard and Map Sharing</li>
<li>OpenDesktop and Earthquakes Online Service</li>
<li>Extended Plugin Configuration</li>
<li>Map Editing</li>
<li>Voice Navigation</li>
</ul>
As with every Marble release, there is a <a href="http://edu.kde.org/marble/current_1.1.php">feature guide</a> with screenshots.
<!--break-->
The Marble library released alongside is binary compatible with Marble 1.0 (shipped with version 4.6 of the KDE applications). The source code was <a href="http://edu.kde.org/marble/obtain.php">tagged on April 14</a>. Binaries for the Nokia N900 are already available in the <a href="http://maemo.org/packages/view/marble">extras-testing repository</a>. More information at <a href="http://nienhueser.de/blog/?p=309">Dennis' blog</a>. Desktop packages of this release will appear on <a href="http://edu.kde.org/marble/download.php">Marble's download page</a> during the upcoming week. There will be packages for Linux, Windows and Mac.

Enjoy!