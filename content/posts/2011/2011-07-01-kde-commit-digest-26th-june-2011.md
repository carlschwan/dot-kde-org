---
title: "KDE Commit-Digest for 26th June 2011"
date:    2011-07-01
authors:
  - "mrybczyn"
slug:    kde-commit-digest-26th-june-2011
comments:
  - subject: "The commit-digests are doing"
    date: 2011-07-01
    body: "The commit-digests are doing wonderfully well in the post-Danny age. Props."
    author: "trixl."
  - subject: "a lot happening behind the scenes too"
    date: 2011-07-01
    body: "Danny was instrumental in setting up systems that allow for timely digests. He's still involved...in a different way. The reporting process, including the regular Friday schedule, is working well."
    author: "kallecarl"
  - subject: "Post-Danny Age"
    date: 2011-07-01
    body: "We have indeed switched from a \"Danny-does-it-all\" to a \"Danny-does-a-lot\" age. Danny is still busy behind the scenes and does some review / classification work from time to time to keep fit ;-)\r\n\r\nMost of the work for this Digest was done by Marta (with some help from Shafqat and me). Thanks a lot Marta!\r\n\r\nIf you like the Digest, please sign up for an <a href=\"http://enzyme.commit-digest.org/\">Enzyme</a> account and help us! Also please join our <a href=\"https://mail.kde.org/mailman/listinfo/digest\">mailing list</a>."
    author: "mkrohn5"
  - subject: "Vlad's cookin' too"
    date: 2011-07-02
    body: "Vlad has been all over the Digest too.\r\n\r\nThanks to all of you and to the KDEvelopers who do amazing amount of great work. "
    author: "kallecarl"
  - subject: "thanks for the positivity :)"
    date: 2011-07-03
    body: "We love the digest as much as ya'll :)\r\n\r\nVlad"
    author: "vladislavb"
  - subject: "Yeah!"
    date: 2011-07-06
    body: "I've been very happy with the relaunch and the contributions of people like Marco, Vlad, Marta and Alexander and of course many others in the content production of these new Digests.\r\n\r\nAlso, with the Enzyme system fairly mature now (and open source), the future of the Digest should be secure and also not dependent on a single person.\r\n\r\nWe do however need a few more people to reduce the burden on the core people listed above, so please volunteer if you can @ http://commit-digest.org/contribute/ - there are many ways you can help, it is not difficult at all, and there is no minimum time obligation!"
    author: "dannya"
---
In <a href="http://commit-digest.org/issues/2011-06-26/">this week's KDE Commit-Digest</a>:

<ul><li>Work on <a href="http://wayland.freedesktop.org/">Wayland</a> support in <a href="http://userbase.kde.org/KWin">KWin</a> continues</li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, among other changes, we can see performance improvement in plugins, fixed soft page break, added footnotes, and more work on DOC support</li>
<li>Updates in brush, stroke, and font support in <a href="http://kst-plot.kde.org/">Kst</a></li>
<li>Improved Python support in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<!--break-->
<li>Improved map quality in <a href="http://userbase.kde.org/Marble">Marble</a></li>
<li>GSOC work in SDK <a href="http://userbase.kde.org/Dolphin">Dolphin</a> plugin and <a href="http://userbase.kde.org/Gluon">Gluon</a></li>
<li>Fix in QIF import in <a href="http://userbase.kde.org/KMyMoney">KMyMoney</a></li>
<li><a href="http://userbase.kde.org/Skrooge">Skrooge</a> sees properties that can be copies or links to a file</li>
<li>Updated line wrap handling and multiple session management in <a href="http://userbase.kde.org/Kate">Kate</a></li>
<li>Bugfixing in <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://www.kphotoalbum.org/">KPhotoalbum</a>, and <a href="http://userbase.kde.org/Digikam">Digikam</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-06-26/">Read the rest of the Digest here</a>.