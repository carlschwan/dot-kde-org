---
title: "WebWorld 2011: Building the Next KDE Web"
date:    2011-06-08
authors:
  - "Stuart Jarvis"
slug:    webworld-2011-building-next-kde-web
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/Webteamlogo.png" style="float:right; padding: 5px; padding-right:0; padding-top:0;" /><a href="https://sprints.kde.org/sprint/10">Randa</a> was not the only place to welcome KDE contributors at the start of June. In an altogether warmer part of the world, nine contributors with an interest in KDE's websites gathered outside Essen in Germany at the world famous <a href="http://www.linuxhotel.de/">Linux Hotel</a> (page in German).

Attendees included members of KDE's design, web, promotion, UserBase and sysadmin teams, bringing a healthy mix of creativity and pragmatism. We looked at technical, design and promotion issues facing the <a href="http://www.kde.org">kde.org</a> website and the <a href="http://userbase.kde.org">UserBase</a> (and, to a lesser extent, the other KDE wikis).
<!--break-->
The UserBase team comprised <b>Anne Wilson</b> (who needs no introduction for her work in user support), <b>Hans Chen</b> (who should be familiar to many people in KDE through his work in many areas, particularly the forums and wikis), <b>Claus Christensen</b> (prolific Danish translator on UserBase), <b>Matthias Meßmer</b> (part of the <a href="http://edu.kde.org">KDE Edu</a> web team and frequent hacker on UserBase, resolving technical issues with MediaWiki) and <b>Niklas Laxström</b>, who wrote the language translation to MediaWiki that is used by UserBase.

<b>Ingo Malchow</b> represented the core KDE web team, <b>Tom Albers</b> the system administrators, <b>Eugene Trounev</b> brought his design expertise to our discussions, and <b>Stuart Jarvis</b> provided a promotion team point of view.

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -250px; width: 500px; border: 1px solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/P1020027.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020027.jpeg" /></a><br />Sprint attendees: (from left to right) Anne Wilson, Claus Christensen, Tom Albers, Matthias Meßmer, Ingo Malchow, Eugene Trounev, Niklas Laxström, Hans Chen, Stuart Jarvis.</div>

<h2>Day Zero</h2>

The sprinters all arrived on Wednesday 1 June at various times. We began with discussions about priorities and scheduling, and we all came together for a barbecue in the evening during which Eugene demonstrated his culinary skills. For several of the attendees, it was their first time at a KDE sprint and few of the contributors had met more than a couple of the others before.

<h2>Day One</h2>

The real work began on Thursday, with the contributors working on KDE issues for about 12 hours with only breaks for food, coffee and occasional escapes out into the sunshine. We split into two groups, dealing respectively with the main kde.org website and the UserBase wiki.

<h3>UserBase</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1010997.jpeg" /><br />Working space at Linux Hotel</div>The UserBase team looked at technical issues with discussion pages, archived pages and "translation memory" that assists translators by improving translation guidance based on previous edits.

They also updated instructions for editing pages ,and tested anonymous editing to see if this could be enabled to make it easier for people to make their first contributions. They also modified the sidebar to have an easier-to-use layout and, with Eugene, adjusted the wiki background.

<h3>Websites</h3>

The rest of the attendees discussed requirements for a new version of Capacity, the custom PHP framework that powers kde.org and other KDE websites. After some debate, it became apparent that the requirements effectively amounted to a full content management system (CMS) and Tom suggested that if this was the case it could make sense to use and adapt an established CMS rather than implementing a new one. It was agreed to evaluate existing solutions before coming to a decision on Capacity.

Work also began on designing the main kde.org site. This included a review of the information that is needed on the front page and the adoption, in principle, of a new menu structure developed at the <a href="http://dot.kde.org/2011/05/16/promo-sprint-2011">KDE Promotion Team Sprint in April</a>.

There was also discussion of the 80+ subdomains of kde.org and it was agreed that most add little value. We spent time creating guidelines that would determine the future provision of subdomains and set up better stats collection for the subdomains to determine which ones are needed. There was also some cleanup of the SVN repositories for KDE websites.

<h3>Logo</h3>

Eugene also spent some time putting together a logo for the WebWorld sprint. Every good sprint needs a logo!

<h2>Day Two</h2>

<h3>UserBase</h3>

The UserBase team cracked on with polishing up the wiki experience, tidying up codetags and clarifying and defining their use in addition to increasing the visibility of help files with links in the sidebar.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020066.jpeg" /><br />Late night discussion</div>Niklas made it possible to move pages and automatically have all the translations follow. This was an important issue as it was previously impossible to move a page once it had been translated (if for example there was a typo in the title) and new pages often have translations within 24 hours of their creation.

Claus made it easier to insert oxygen icons into articles by making a new icon template and Eugene worked on a UserBase logo, emphasising the user-editable nature of UserBase as a wiki by switching away from the main KDE logo that is used on our static sites.

<h3>Websites</h3>

The team discussed their experiences with a test installation of Joomla and uncovered annoyances such as limitations in user roles that would not fit well with the way KDE works. It was agreed to also evaluate WordPress. The other of the "big three" content management systems, Drupal, is already used on some KDE sites, including the Dot, so its capabilities and limitations are well known to the team.

Stu compared the old site navigation with the proposed new menu structure and identified which pages can be retained, which merged, and which pages require new content.

Ingo and Tom worked further on defining rules for provision of KDE subdomains and Ingo blogged about this to raise initial awareness. They also began to look at the visitor statistics for some of the sites.

<h2>Day Three</h2>

<h3>UserBase</h3>

On the final full day, the UserBase team discussed how to make the wiki more appealing to new users to edit. It emerged that some users do not realise UserBase is a wiki or think it is too official to be edited by them. The new logo will hopefully go some way to address this and the team will also consider, in future changes to the design,  making the "Edit" buttons more obvious (at present the main edit button is an icon).

There was also discussion of how the wiki might better fit the needs of users, by switching to a more task-oriented navigation rather than the current application-centric approach. For example, a new user may need to know how to play music but not know to look for this information under the Amarok pages.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020083.jpeg" /><br />Interview time</div><h3>Websites</h3>

Ingo, Tom and Stu spent some time evaluating WordPress for the kde.org website. This included importing some sample content and exploring useful plugins for syndicating content from other KDE sites. The results were positive and it was agreed to proceed with WordPress as the base for the initial work on the kde.org redesign. This will allow the platform to be tested thoroughly to see if it fits in with our needs.

Eugene and Ingo also returned to design aspects, in particular different ways of presenting a menu.

<h3>Interviews</h3>

Stuart spent some time interviewing most of the attendees who had not previously featured on <a href="http://www.behindkde.org">BehindKDE</a>. These interviews will be published over the coming months, giving you an insight into the lives, interests and contributions of the UserBase team in particular.

<h3>Barbecue and Table Football</h3>

We had another barbecue on Saturday evening and then followed it up with a Table Football tournament that, confusingly, operated on a league and knockout system simultaneously. Ingo was victorious in the knock-out, while Tom appeared to win the league but was accused of cheating after it turned out that he had managed to play more games than anyone else. Anne tried to maintain order and documented results to minimise disputes.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020091.jpeg" /><br />Model planes = geek heaven</div><h3>Discussions</h3>

Day three had a late end. For some people it continued until 3am on what was technically day four. Discussions ranged mainly around UserBase and what it is that stops people contributing there, in addition to considering its role within KDE documentation as a whole.

<h2>Day Four</h2>

Eugene left in the early hours of day four. The rest of the team spent the morning reviewing progress, discussing UserBase and the websites and playing with various radio controlled planes and drones provided by the hotel. Linux Hotel is truly a geek heaven. In the afternoon more people had to leave and the first dedicated KDE web sprint came to a close.

<h2>Thanks</h2>

The sprint achieved a lot and was a great opportunity for some long-time collaborators to finally meet. We would like to thank <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.linuxhotel.de/">Linux Hotel</a> (page in German) for their kind support in making this event happen.

If you would like to make more sprints like this possible in future, please <a href="http://jointhegame.kde.org/">Join the Game and become a supporting member of KDE e.V.</a> The attendees would like to thank the <a href="http://jointhegame.kde.org/member">existing supporting members</a> for helping us to build the future of the KDE web.