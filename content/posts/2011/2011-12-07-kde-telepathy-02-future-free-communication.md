---
title: "KDE Telepathy 0.2 - The Future of Free Communication"
date:    2011-12-07
authors:
  - "David Edmundson"
slug:    kde-telepathy-02-future-free-communication
comments:
  - subject: "Cool, but..."
    date: 2011-12-07
    body: "It's masked in Gentoo with a \"corruption\" keyword, I can't install it yet. Seems so cool, but the only thing I dislike is the name. Would you eventually change its name? Something like 'SpeaK', I don't know..."
    author: ""
  - subject: "Kopete"
    date: 2011-12-07
    body: "Is there any cooperation going on with the Kopete team?"
    author: ""
  - subject: "Kopete"
    date: 2011-12-07
    body: "Is there any cooperation going on with the Kopete team?"
    author: ""
  - subject: "IM=KDE-telepathy\u00b2"
    date: 2011-12-07
    body: "Looks great....!\r\n\r\nPersonally I am still waiting the basic features to be added but so far everything is just working fine.  "
    author: ""
  - subject: "nice, but..."
    date: 2011-12-07
    body: "I like this idea although i would do it differently.\r\n\r\nWhat i would do is:\r\n- Make a Qt wrapper for libpurple to be usable in Qt\r\n- Make a \"libpurpleengine\" or something so you can access it through the data engines. That way you can keep all the logic in a relatively small C++ project and all the GUI stuff in QML :)\r\n\r\nJust how i would do it although that task is probably massive...\r\n\r\nAs for the current telepathy, it wasn't working for me when i tried it (few weeks ago) with version 0.1 so i hope it's working now ;-)"
    author: ""
  - subject: "My understanding is that the"
    date: 2011-12-07
    body: "My understanding is that the Kopete team have moved on to telepathy-kde, and that the telepathy-kde-contacts and telepathy-kde-chat programs form (ideally) a  far more flexible drop-in replacement for Kopete."
    author: "madman"
  - subject: "There is a lot"
    date: 2011-12-07
    body: "The kopete team is the main actor behind this software.\r\nActually, the current kopete maintainers think that rewriting (almost) everything from scratch is the way to go, as this approach guarantees the maximum interoperability with other apps in the future, and the Kopete code has become unmaintainable."
    author: "Giulio Guzzinati"
  - subject: "Which package to install for Telepathy"
    date: 2011-12-08
    body: "Any idea which (main) package to install to get Telepathy working in Debian?\r\n\r\naptitude install kopete will get a working setup but telepathy seems to have many smaller packages."
    author: ""
  - subject: "Which package to install for Telepathy"
    date: 2011-12-08
    body: "Any idea which (main) package to install to get Telepathy working in Debian?\r\n\r\naptitude install kopete will get a working setup but telepathy seems to have many smaller packages."
    author: ""
  - subject: "I disagree to some extent,"
    date: 2011-12-08
    body: "I disagree to some extent, and given I wrote the most code in this project I think I'm most in the know ;-)\r\n\r\nThere is no Kopete team. No-one is maintaining it, a team simply doesn't exist. Some of the people are still around in KDE and the ones I've spoken to fully support and approve of the project, but I don't think any of them are actively contributing. It happens in open source, projects die, teams disappear, new ones flourish. Circle of Life.\r\n\r\nA Kopete backend port was considered, and even tried - but frankly it doesn't work. The effort porting is going to be as large as what we're doing, and far more restrictive.\r\n\r\nThere is some Kopete code still being used here, but it's selective chunks.\r\n\r\n"
    author: "David Edmundson"
  - subject: "Yes, we ship as many smaller"
    date: 2011-12-08
    body: "Yes, we ship as many smaller chunks - in the long term this is more flexible, but it does make this a pain - I think George Kiagiadakis (our Debian packager and all round hero) made a meta package called meta-telepathy-kde which installs everything."
    author: "David Edmundson"
  - subject: "Off-the-Record Messaging"
    date: 2011-12-08
    body: "Are there any plans to add OTR support?"
    author: ""
  - subject: "Plans: yes.Spare coders with"
    date: 2011-12-08
    body: "Plans: yes.\r\nSpare coders with enough time to do it right now: no.\r\n\r\nFor me personally (and from anyone in the team each time it's come up) it's not a high priority, however I'd happily help someone wanting to implement it.\r\n"
    author: "David Edmundson"
  - subject: "Sweet"
    date: 2011-12-08
    body: "Go Team Go"
    author: ""
  - subject: "Independency "
    date: 2011-12-08
    body: "It should probably be mentioned that core of telepathy-kde, telepathy and it's protocol plugins (called tubes, icq, jabber etc.) are desktop independent and are used under Gnome also via Empathy IM client.\r\nThis bodes well for future of this project."
    author: ""
  - subject: "Sigh..."
    date: 2011-12-08
    body: "OK, let me state the following. It looks neat. No, really it does! And I like the premise of integration into plasma! Also, I love the unification of several services in one app.\r\n\r\nNow with that out of the way *rant ahoy*\r\n\r\nSigh... Now, why oh why does everyone keep using sensationalism in their blog titles? Just to increase traffic? It seems so pompous and unnecessary. I am terribly sorry to be the pessimistic, sardonic and bitter jerk of the block, but I am pretty sure that this will not be the future of free communication - or in fact any significant part of future at all. It is so blatantly generalizing and obnoxiously untrue. What future? You mean the entire future? No of course it won't! What free communication? Specify please? Free as in beer?\r\n\r\nBesides why on earth would this be the future of anything?? Does it bring anything revolutionary new to the table? New way of sharing info? New interface that is very user friendly? Or perhaps it is cross-platform like no app out there? Will this perhaps come as an app for my Android device? No, it will not. KDE penetration of the general market is abysmally small (Yup, I am an avid user of KDE apps, but - hell - do please realize that the majority of general public doesn't give a flying f*** about it).\r\n\r\nSo why this sensationalism? We all admire the effort, but it's like me making a version of tetris for KDE, announcing that it will integrate with Plasma and proclaiming that it will be the future of free gaming. Come on!\r\n\r\n*rant out*\r\n"
    author: ""
  - subject: "The Most Amazing Comment Reply In The World!!!1!!"
    date: 2011-12-08
    body: "Duly noted."
    author: "David Edmundson"
  - subject: "What did the packager miss?"
    date: 2011-12-09
    body: "So the future is instability? Still not feeling telepathy is any better at all than the things we currently got. WLM not working, file-transfer slower, kwallet crashes, dropped packet issues. So, what did the packager for Arch Linux miss since WLM is not working at all, not supported? Google talk works but some messages are not sent/received, same with most others. Oh well, it's beta so I can forgive, but next release, make sure to tell me what is not working ATM! :) Wish WLM didn't exist, the worst IM protocol to maintain on free IM software it seems."
    author: ""
  - subject: "Install a package called"
    date: 2011-12-09
    body: "Install a package called telepathy-haze, then try adding an MSN account again.\r\n\r\nTelepathy supports a lot of protocols, to the extent that it has duplication, there are two backends providing MSN support (amongst others). One of these MSN backends is rubbish. But it's not the packagers fault, because all these protocol supports should be add-able at run time, we don't /depend/ on any particular backend to be available as it's all really flexible, but it is a bit of an issue when it comes to shipping setups.\r\n\r\nAlso if you have a bug (like a crash), report it on bugs.kde.org so we can frickin' see it! Martin and I regularly check bugzilla, we rarely keep opening old news articles to look at the comments when we're seeing what stuff needs fixing."
    author: "David Edmundson"
  - subject: "Shall we be finally able to"
    date: 2011-12-09
    body: "Shall we be finally able to send files via Gtalk and have new incoming mails notifications? Kopete realy sucks at that."
    author: ""
  - subject: "xDD"
    date: 2011-12-09
    body: "Amen!\r\n\r\nBut is a very cool program, anyways, taking in account all those messengers in the Linux ecosistem still at the level ot the messengers in the decade of the '90; so, not revolutionary, sure, but rather hopefull to have the posibility of finally have a decent messaging sistem that we can show to our Windows friends without feeling ashamed, haha, so big congrats and thanks to the developers."
    author: ""
  - subject: "meta-contacts?"
    date: 2011-12-09
    body: "Is it now possible to group contacts in meta-contacts just like kopete?\r\nFor me it's a really basic and necessary feature."
    author: ""
  - subject: "I agree"
    date: 2011-12-09
    body: "As I noted some time ago, it seems that many KDE devels and PRs don't understand the importance of surviving to their users' expectations. \r\n\r\nWhen people read pompous announces about the \"The Future of Free Communication\" or \"more productive and fun to use than ever before\", they are lead to expect perfectly polished implementation of revolutionary useful new features. \r\nIf what users get instead is software with some raw corners, some nasty behaviours and many \"papercut bugs\", no surprise if they get very deluded. But, this way, even promising software, based on good ideas, fails before it can show its potential!  Deluded users' rants discourage the developers and everything becomes difficult. \r\n \r\nThere is so much delusion and anger among the KDE userbase... this requires an extra effort to developers to make stabler software (which it seems it's happening) and rethink some wrong choice (which it seems it's not :( ). But, believe me, also lowering the expectations a bit could do miracles! \r\n\r\nPlease, stop the mantra \"wonderful user experience, better the ever, the future of blah blah blah\": it does no good to the whole community. \r\nInstead try to refrain and/or give more emphasis to this kind of message: \"you could encounter problems in this and this area, if it happens, report it this way\". It's much more useful and realistic, it warns users about difficulties and calls for cooperation instead of delusion."
    author: "Vajsvarana"
  - subject: "Nope. Sorry."
    date: 2011-12-09
    body: "Try it and see!\r\n\r\nAlso no. Not yet. \r\nDiffering opinions on the word \"basic\" I'm afraid. We are genuinely working on it. In fact there's even prototype code."
    author: "David Edmundson"
  - subject: "File sharing works for me,"
    date: 2011-12-09
    body: "File sharing works for me, though there have been some reported issues with the varying protocols/connection manager versions/clients. \r\n\r\nWe also don't have incoming mail notifications. I'm in two minds about doing this."
    author: "David Edmundson"
  - subject: "Oh well.."
    date: 2011-12-09
    body: "You should read more and more carefully. \r\n\r\n<p>To answer some raised points - Yes, Telepathy is a cross-desktop product. Yes, it is available even on Android (not publicly released yet). Yes, it is the future in that sense that many projects have adopted and/or are adopting Telepathy as a backend. Yes, it brings something revolutionary. Google it why.</p>\r\n\r\n\r\n<p><cite>\"do please realize that the majority of general public doesn't give a flying f*** about it\"</cite>\r\n\r\nOk, so? (and apparently they do if it is enough for them to stop by and spawn several paragraphs full of hate)</p>\r\n\r\n\r\n<p><cite>\"... they are lead to expect perfectly polished implementation of revolutionary useful new features ... you could encounter problems in this and this area, if it happens, report it this way\"</cite>\r\n\r\nThe article clearly says \"This is an early release, far from what we consider the \"finished product\". ... So if you are interested in helping, please check out our wiki page or join us on IRC at #kde-telepathy on freenode\". You deluded yourself.</p>\r\n\r\n\r\n<p><cite>\"Deluded users' rants discourage the developers and everything becomes difficult.\"</cite>\r\n\r\nUhm...why are you ranting then? I like it how you know it's bad and you do it anyway. Double deluded.</p>\r\n\r\n<p><cite>\"Please, stop the mantra \"wonderful user experience, better the ever, the future of blah blah blah\": it does no good to the whole community.\"</cite>\r\n\r\nNeither of these phrases were used (except the futuristic title..). So please stop the mantra \"you're liars, I'm full of hate and I know it the best\". It does no good to the whole community.</p>\r\n\r\nGive these guys some credit, what have you done in your spare time?"
    author: ""
  - subject: "Read carefully?"
    date: 2011-12-09
    body: "Ok, take a breath, and try to read better too. :)\r\n\r\nFirst, I'm not the <cite>do please realize that the majority of general public doesn't give a flying f*** about it</cite> one. Why not answering to the right post? :)\r\n\r\nSecond. My post was a _general_ advice about a _general_ trend in the KDE community, as stated in the first and last part of the post. I've never spoken against Telepathy in the whole post, in fact the statements come mostly from the KDE stable and beta announcements, and that's what I was speaking about. This unfortunate misunderstanding make the rest of your post rather pointless, so I will not answer.\r\n\r\nI apologize if this was taken as direct offence toward the Telepathy people.\r\n\r\n"
    author: "Vajsvarana"
  - subject: "Package missing"
    date: 2011-12-10
    body: "Sorry, as of today (10th December 11) there is no such package in official Debian list (testing). Could you, please, specify at least name of basic KDE GUI (replacement of Kopete), as all others I was able to find but no such KDE GUI... Thanks in advance."
    author: ""
  - subject: "Required components for basic"
    date: 2011-12-10
    body: "Required components for basic functionality: telepathy-kde-accounts-kcm, telepathy-kde-contact-list, telepathy-kde-text-ui, telepathy-auth-handler, telepathy-kde-integration-module\r\n\r\nHighly recommended components (should be installed by default): telepathy-kde-approver, telepathy-kde-presence-applet, telepathy-kde-presence-dataengine, telepathy-kde-filetransfer-handler\r\n\r\nOptional components: telepathy-kde-send-file, telepathy-kde-contact-applet"
    author: "David Edmundson"
  - subject: "nepomuk"
    date: 2011-12-10
    body: "Has progress been made on Nepomuk integration? It sounds like something that could be a use for meta-contacts."
    author: ""
  - subject: "Yes. That's the plan.\nThere's"
    date: 2011-12-11
    body: "Yes. That's the plan.\r\n\r\nThere's been some good progress especially this last week, but it's not usable yet. Wont be in 0.3 either, but it's moving forward."
    author: ""
  - subject: "excellent"
    date: 2011-12-11
    body: "I am excited about Telepathy and thankful for the work done here. Communication is integral to computing environments and is fundamental to extending to reaching the limits of what is possible on computation devices and the internet. The latter being a resource still expanding and providing for new possibilities. A free and open framework for internet-based communication is essential. \r\n\r\nWay to go telepathy team and everyone contributing to the project indirectly by way of other open frameworks as well!\r\n\r\nVlad"
    author: "vladislavb"
  - subject: "Please include IRC"
    date: 2011-12-13
    body: "I always thought it was ironic at best that Kopete pulled IRC functionality, but still gave out an IRC channel for support. I see Telepathy gives an IRC support channel -- I hope I can use Telepathy to access it."
    author: ""
  - subject: "msn"
    date: 2011-12-15
    body: "Does this help or affect you in any way?\r\nhttp://windowsteamblog.com/windows_live/b/windowslive/archive/2011/12/14/anyone-can-build-a-windows-live-messenger-client-with-open-standards-access-via-xmpp.aspx?utm_source=twitterfeed&utm_medium=twitter"
    author: ""
  - subject: "Great work."
    date: 2011-12-16
    body: "Hi,\r\ni just used the latest version on arch linux(0.2)!\r\nIt seems to be realy great.Keep going guys,and thank you for this great program."
    author: ""
  - subject: "Why show the protocol if the"
    date: 2012-06-12
    body: "Why show the protocol if the runtime dependency is not met for that protocol?  There's no sense in giving a button that does nothing if libs aren't present.  Hide the interface until the implementation becomes available (i.e. installed deps)."
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde_telepathy_0.2_0.png" /><br />KDE Telepathy 0.2</div>
The KDE Telepathy team is pleased to announce its second release. KDE Telepathy is a suite of applications that form an instant-messaging client for Jabber, Gmail, Facebook, MSN and more. KDE Telepathy stands out from previous instant messaging solutions by being able to integrate into the Plasma Workspaces, and can also be used like a traditional application.
<!--break-->
This release features:
<ul>
<li>KWallet integration for storing passwords</li>
<li>A Plasmoid for instant access to a contact</li>
<li>Ability to set status to the currently playing track from Amarok, Clementine or any other mpris2-compatible player</li>
<li>Auto Away</li>
<li>Progress bar and cancel button for file transfers</li>
<li>Over <a href="https://bugs.kde.org/report.cgi?x_axis_field=&y_axis_field=component&z_axis_field=&query_format=report-table&short_desc_type=allwordssubstr&short_desc=&product=telepathy&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-07-25&chfieldto=Now&chfieldvalue=&format=table&action=wrap&field0-0-0=noop&type0-0-0=noop&value0-0-0=]">130 bug fixes</a> and tweaks since 0.1!</li>
</ul>

<h2>The Value of Sprints</h2>
The whole team met at the Woshibon 2 sprint in Cambridge, UK (14th-18th September). This sprint was sponsored by both KDE e.V. and Collabora. It allowed us to resolve many of the details in this release, and plan more long term as well. All things considered, intensive KDE sprints get more done in less time than working only online. If you would like to support development sprints like this, please consider <a href="http://jointhegame.kde.org/">Joining the Game</a>. 

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -254px; width: 500px; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdetelsprint.jpg" /><br />KDE Telepathy Sprint attendees.</div> 

<h2>The Future</h2>

This is an early release, far from what we consider the "finished product". However all the functionality works and many of us now use it as a daily instant messaging client.

In the future we will have contact aggregation, audio and video calls, more Plasma integration, and much more!

We always appreciate the help from more developers, designers and testers. So if you are interested in helping, please check out our <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">wiki page</a> or join us on IRC at #kde-telepathy on freenode.

<h2>Getting the Latest Release</h2>
The latest tarballs are available from <a href="http://download.kde.org/download.php?url=unstable/telepathy-kde/0.2.0/src/">KDE's FTP server</a>. Check our wiki page for installation guidelines and troubleshooting help. Packages may be available for your distribution.