---
title: "KDE Commit Digest for 3 April 2011"
date:    2011-04-10
authors:
  - "vladislavb"
slug:    kde-commit-digest-3-april-2011
comments:
  - subject: "git diff URLs"
    date: 2011-04-10
    body: "Could you please implement the feature to the Digest that one could click on git diff URLs? I would really appreciate it.\r\n\r\nThank you :)"
    author: "szotsaki"
  - subject: "Quite a week"
    date: 2011-04-10
    body: "An impressive set of changes across the board. In particular, the rate of progress on Calligra these days is exciting to see.\r\n"
    author: "tangent"
  - subject: "You can help!"
    date: 2011-04-10
    body: "To create a new Digest we need to review quite a number of commits each week--KDE developers are very busy these days! In order to improve the Digest, produce it in time and make our readers happy (our ultimate goal!) we would appreciate some helping hands...\r\n\r\nIt is easy (you do not have to be a programmer) and it is fun!\r\n\r\n<a href=\"http://enzyme.commit-digest.org/\">Volunteer here</a> and join our <a href=\"https://mail.kde.org/mailman/listinfo/digest\">mailing list</a>.\r\n\r\nIt is not time consuming--an hour per week already makes a big difference. \r\n"
    author: "mkrohn5"
  - subject: "I agree, I haven't tried"
    date: 2011-04-10
    body: "I agree, I haven't tried Calligra yet, but every Digest mentions some progress, so that's great. It's good to see competition in the office space, as LibreOffice seems to be quite active as well and cleaning up the OpenOffice.org code base.\r\n\r\nThanks for the Digests, by the way, it's good to see them regularly now.\r\n\r\n(This should be a reply to tangent)"
    author: "nethad"
  - subject: "please fix these"
    date: 2011-04-13
    body: "https://bugs.kde.org/show_bug.cgi?id=259338\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=258278"
    author: "metosa"
  - subject: "Agreed. We have been looking"
    date: 2011-04-15
    body: "Agreed. We have been looking in to this -- do you or anyone else know of a way to access git diffs online?\r\n\r\nVlad"
    author: "blantonv"
  - subject: "KDE git facilities"
    date: 2011-04-29
    body: "You can find some KDE websites on topic git here:\r\nhttp://community.kde.org/Sysadmin/GitKdeOrgManual\r\n\r\nFrom this list maybe you need commits.kde.org or gitweb.kde.org."
    author: "szotsaki"
---
In <a href="http://commit-digest.org/issues/2011-04-03/">this week's KDE Commit-Digest</a>:

<ul><li>Initial implementation of new shadows in <a href="http://techbase.kde.org/Projects/KWin">KWin</a></li>
<li>Bugfixing and feature work throughout <a href="http://www.calligra-suite.org/">Calligra</a>, including further work supporting missing MS Office 2003 autoshapes, improved support for DOC, DOCX, and ODF file formats, a new EPUB-ODF import filter, and work on an initial Modern Menu implementation and a new map widget in <a href="http://www.calligra-suite.org/kexi/">Kexi</a></li>
<li>Improved support for 3G connections in <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> along with other bugfixes</li>
<!--break-->
<li>Work on <a href="http://www.kdevelop.org/">KDevelop-Python</a></li>
<li>Support for Windows Live Photo Gallery tags in <a href="http://www.digikam.org/">Digikam</a></li>
<li>Application uninstall feature added to <a href="http://userbase.kde.org/Plasma/Kickoff">Kickoff</a></li>
<li>Work throughout <a href="http://skrooge.org/">Skrooge</a> including improvements to category editing and initial work on a Scheduled Operations applet</li>
<li>Work on handling multiple selections in DVD ripping view in <a href="http://k3b.plainblack.com/">K3B</a></li>
<li><a href="https://projects.kde.org/projects/kdesupport/phonon/phonon-gstreamer">Phonon-Gstreamer</a> now supports subtitles</li>
<li>Memory usage optimizations for big puzzles and replacement of the tabwindow interface by a standard XMLGUI window in <a href="http://www.kde.org/applications/games/palapeli/">Palapeli</a></li>
<li>Improved handling of URL drag and drop handling in <a href="http://rekonq.kde.org/">Rekonq</a> along with other bugfixing</li>
<li>Mechanism for adding new contacts and spell checking added to <a href="http://techbase.kde.org/Projects/Telepathy">Telepathy</a></li>
<li>The Wacom Tablet KConfig Module now has an extra tab page for touch features and support for different tablet areas for the stylus and touch</li>
<li>Rework of panel management in <a href="http://www.krusader.org/">Krusader</a></li>
<li>Improved handling of special characters in <a href="http://owncloud.org/">Owncloud's</a> media player</li>
<li>Feature and usability work on guidance mode and the routing info box in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>Repair of printing regressions in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a></li>
<li>Bugfixing in <a href="http://kde-look.org/content/show.php/?content=136216">Oxygen-GTK</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-04-03/">Read the rest of the Digest here</a>.