---
title: "Meet Claire Rowland, Desktop Summit Keynote Speaker"
date:    2011-06-22
authors:
  - "kallecarl"
slug:    meet-claire-rowland-desktop-summit-keynote-speaker
comments:
  - subject: "She has my vote for the voice"
    date: 2011-06-22
    body: "She has my vote for the voice of marble!"
    author: "trixl."
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 200px;"><img src="/sites/dot.kde.org/files/ClaireRowlands.jpg" width="200px"><br /> Claire Rowland</div>

Claire Rowland, user experience guru, will be a featured keynote speaker at this summer's <a href="https://www.desktopsummit.org/">Desktop Summit</a> 2011 in Berlin.

Claire is Head of Research for Fjord London, an international digital service design agency and has worked extensively in user experience research and design. Recently her focus has been on a shift in user experience from the desktop toward services delivered through multiple platforms of widely differing form factors and the cloud. Her research and recommendations relate to what this shift means for what users expect from their devices, and what effective design, across platforms and the cloud, looks like. She also addresses what users increasingly care about the most, and what this might mean for Operating System design.

William Carlson talked with Claire to find out more about her and her work. Read the <a href="https://www.desktopsummit.org/interviews/claire-rowland">full interview on the Desktop Summit website</a>.
<!--break-->
