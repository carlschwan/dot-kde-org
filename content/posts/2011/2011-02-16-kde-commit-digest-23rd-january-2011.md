---
title: "KDE Commit-Digest for 23rd January 2011"
date:    2011-02-16
authors:
  - "dannya"
slug:    kde-commit-digest-23rd-january-2011
comments:
  - subject: "Commit misattribution"
    date: 2011-02-16
    body: "Danny, there's something amiss with commit attribution in this issue - there are various Owncloud commits that have supposedly been authored by me, but I've never committed to Owncloud."
    author: "Eike Hein"
  - subject: "Interesting, seems the commit"
    date: 2011-02-17
    body: "Interesting, seems the commit was done by Robin Appelman:\r\n\r\nhttp://gitorious.org/owncloud/owncloud/commit/b116b2fd4c75cb8ddf722ae13be85bbe4eae33f3\r\n\r\nDon't know how this could have happened, probably related to the Git mail format change of a couple weeks back...\r\n\r\nI'll look into it later!\r\n\r\nCheers,\r\nDanny"
    author: "dannya"
  - subject: "If I remember correctly it"
    date: 2011-02-20
    body: "If I remember correctly it was me who moved the Owncloud repo from Gitorious to git.kde.org for the Owncloud team, so the Commit Digest system probably grabbed the \"pushed by\" info from the emails (which is distinct from who authored a commit)."
    author: "Eike Hein"
---
In <a href="http://commit-digest.org/issues/2011-01-23/">this week's KDE Commit-Digest</a>:

              <ul><li>Minor features are added to <a href="http://www.digikam.org/">Digikam</a> and the "Public Transport" Plasma applet</li>
<li>Importing MySQL database tables is now supported by <a href="http://www.calligra-suite.org/kexi/">Kexi</a></li>
<li><a href="http://www.calligra-suite.org/plan/">Calligra Plan</a>’s scheduling speed is significantly improved</li>
<li>The KOffice "change tracking" feature sees more improvements</li>
<li>Much work is done on Calligra’s Colour Management System (CMS), <a href="http://community.kde.org/Calligra/Libs/Pigment">Pigment</a></li>
<li>Bug fixes to KDE’s basic software (such as <a href="http://dolphin.kde.org/">Dolphin</a> and <a href="http://solid.kde.org/">Solid</a>), the KDE libraries, <a href="http://nepomuk.kde.org//">Nepomuk</a>, <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://skrooge.org/">Skrooge</a>, <a href="http://www.calligra-suite.org/">Calligra</a> and <a href="http://amarok.kde.org/en">Amarok</a></li>
<li><a href="http://community.kde.org/Solid/Projects/BlueDevil/">BlueDevil</a>’s user interface for sending files is simplified so that it fits in one page</li>
<li>Improvements to tagging functionality in Amarok</li>
<li>A lot of work is done on <a href="http://owncloud.org/index.php/Main_Page">ownCloud</a></li>
<li><a href="http://opendesktop.org/content/show.php?content=122046/">Knights</a> can now support a match between two <a href="http://en.wikipedia.org/wiki/Chess_engine">chess engines</a></li>
<li><a href="http://ktorrent.org/">KTorrent</a>’s performance is improved in situations with many torrents open</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-01-23/">Read the rest of the Digest here</a>.
<!--break-->
