---
title: "Windows Team Releases Updated KDE Applications"
date:    2011-01-09
authors:
  - "panda84"
slug:    windows-team-releases-updated-kde-applications
comments:
  - subject: "Bring on the Awesome\u2122!"
    date: 2011-01-10
    body: "The stuff about lowering the barrier to entry didn't sound that impressive, until I clicked on the link and read what it was. Unless I'm mistaken, this sounds like an almost instant, setup-for-you environment to build KDE platform / workspaces / apps on Windows... with almost no voodoo knowledge required?! If I'm not misunderstanding... then this is AWESOME! Major, major kudo's to all involved!\r\n\r\nI notice Calligra isn't included in the list. Any pointers for people wanting to build / test Calligra on Windows?\r\n\r\nThanks for kicking some serious booty. Now hopefully I can help with testing like I've always wanted to! \\o/"
    author: "Bugsbane"
  - subject: "Thank you!"
    date: 2011-01-10
    body: "Very impressive. A big thank you to the KDE on Windows team."
    author: "mutlu"
  - subject: "I have been chattering with"
    date: 2011-01-10
    body: "I have been chattering with some windows and koffice developers during last week at it sounded like they did have some build problems on windows but one of the koffice developer seems to be working at it. I dont know if it is solved by now"
    author: "beer"
  - subject: "no success"
    date: 2011-01-10
    body: "That was me... But I haven't had success yet. I tried to make it work with kdewin-installer packages, instead of emere."
    author: "Boudewijn Rempt"
  - subject: "Very useful"
    date: 2011-01-10
    body: "That means KDE code can also be checked by Windows debugging tools and multiple build environments?"
    author: "vinum"
  - subject: "Kontact and Kmail"
    date: 2011-01-10
    body: "Hi,\r\nThanks for the effort. However, how to install Kontact or only Kmail?\r\nShould I download the Enterprise5 version from Kolab and will it work?\r\n\r\nThanks,\r\nJohannes"
    author: "Johannes"
  - subject: "Sorry to say, ..."
    date: 2011-01-11
    body: "but this version doesn't work for me at all.\r\n\r\nI had 4.4.x installed and it ran, i.e. programs started and (some) were usable and did what they were expected to do.\r\n\r\nNow, after installing the 4.5.4 packages the installer wanted to start some configuration processes and all those programs failed to execute.\r\n\r\nWhen after finishing the installtion wizzard I started a Program like KMahjong I received the message the msvcp100.dll was missing.\r\n\r\nSo I thought, I sould download and install it (vcredist_x86.exe, version 10.0.30319.1). Later I realized the the KdeWinInstaller had downloaded the same file called vcredist_x86-vc100.exe (same version). Did it install it also?\r\n\r\nAnyway after reinstalling the version I downloaded, the KDE programs I start show up in the Task Manager for 1 second and vanish.\r\n\r\nThe KDE/bin/assistant.exe starts ok, but noe KDE programs.\r\n\r\nI'm running Vista Business.\r\n\r\nWhat the heck went wrong? How can I fix it?\r\n"
    author: "gadeiros"
  - subject: "64-bit"
    date: 2011-01-11
    body: "Are you running Vista Business 64-bit? There are still problems in 64-bit Windows\r\n\r\nPlease join the #kde-windows IRC channel in FreeNode or the kde-windows mailing list and help us debug the issue.\r\n"
    author: "pgquiles"
  - subject: "Awesomeness"
    date: 2011-01-11
    body: "The awesomeness you've talked about, exists and works, at least for the apps :) I've been a happy user of KDE-Windows, only missing digikam."
    author: "brainfunked"
  - subject: "No, it's a 32-bit Windows"
    date: 2011-01-11
    body: "No, it's a 32-bit Windows Vista.\r\n\r\nI've never used IRC and don't have any client installed.\r\n\r\nBut I would install one if I could help you with that problem.\r\n\r\nCan you point me to some good IRC client for windows and how I would have to configure it?\r\n\r\n\r\n"
    author: "gadeiros"
  - subject: "web irc "
    date: 2011-01-11
    body: "hi, \r\n\r\nthere are many options, but to make it simple, I'd go over web interface.\r\nFor example go to: http://jdownloader.org/knowledge/chat\r\nand set your nickname, for channels replace #jdownloader with #kde-windows fill captcha and click connect, it should work, at least it did for me.\r\nIt's not the cleanest way, as they expect to connect to their channel, but in general it works."
    author: "typek_pb"
  - subject: "Same here"
    date: 2011-01-11
    body: "tried the 64 and 32 bit version, same dll missing.\r\n\r\nrunning windows 7 64 bit"
    author: "Asraniel"
  - subject: "Same here 2"
    date: 2011-01-12
    body: "Win 7 Ultimate x64 -> msvcp100.dll and msvcr100.dll are missing, I've downloaded them on http://www.dll-files.com but they aren't compatible (\"entry point non found\" error). Where can we find the right version for those 2 dll?"
    author: "Franck Dernoncourt"
  - subject: "You should look for"
    date: 2011-01-12
    body: "You should look for vcredist_x64.exe (best download directly from MS).\r\n"
    author: "gadeiros"
  - subject: "After a late night session on"
    date: 2011-01-12
    body: "Thanks for the link. It worked well this way :-)\r\n\r\nAfter a late night session on #kde-windows where SaroEngels patiently answered my questions, it seems clear, that the problem was trying to install the new/updated packages, which were compiled with a different compiler than 4.4 was over my 4.4 installation.\r\n\r\nMaybe another problem was that dbus-service.exe was running, but I'm not sure, if this was the original problem. It was only a small problem when deleting all the KDE directory for new installation. I had to kill the process manually, but I don't know if it was the 4.4 version running or already the 4.5.4 version (at least I had executed kbuildsycoca4.exe manually after installing the missing DLLs).\r\n\r\nSo, if you keep to the defaults (which seem to have changed in regards to the used compiler), better make a clean install of 4.5.4 (i.e. killing all running KDE processes and deleting all the KDE dir before installing).\r\n"
    author: "gadeiros"
  - subject: "Missing msvcp100.dll --> Win7 Admin Rights"
    date: 2011-01-14
    body: "Right click the setup icon, choose \"run as administrator\"..."
    author: "nysos"
---
The <a href="http://windows.kde.org">KDE Windows team</a> is delighted to announce the release of Windows packages from KDE Software Compilation 4.5.4. This release is doubly important because it returns the KDE Windows Initiative to the headlines after a lot of hard background work, and because it brings the some of the latest KDE software to Windows users.

Although still not considered stable and suitable for production work, this release brings to the table a lot of new improvements worth trying: 
<ul>
<li>All the <a href="http://www.kde.org/announcements/4.5/">goodness brought by KDE SC 4.5</a>;</li>
<li>All the <a href="http://www.kde.org/announcements/announce-4.5.4.php">bugfixes brought by the 4.5.4 minor release</a> and six months of work from the KDE Windows team;</li>
<li>The <a href="http://apachelog.wordpress.com/2010/11/30/phonon-vlc-0-3-rock-solid/">lastest version of Phonon-VLC backend</a> is available.</li>
</ul>
 
Support for two new compilers has also been added: mingw-w32, a more updated distribution of GCC for Windows provided by the mingw-w64 project, and vc100, the Microsoft compiler provided in Visual Studio 2010 Express Edition. These, in the future, will respectively replace mingw4 and vc90 (both of which are however supported in this release).

Aside from the software improvements mentioned above, the Windows team has worked under the hood to <a href="http://mail.kde.org/pipermail/kde-windows/2010-November/005254.html">lower the barrier of contribution</a> to development and community participation. This release itself is the result of a <a href="http://mail.kde.org/pipermail/kde-windows/2010-December/005385.html">common release effort</a> to fix the remaining compiling issues. The KDE on Windows team is quite proud of this common release effort and hopes to repeat it in the near future when Software Compilation 4.6 is released. We hope you'll "Join The (KDE Windows) Game" too!

Users can test this release by using the <a href="http://windows.kde.org/download.php">KDEWin Installer</a> to try out all the software available. Included in this release are kdebase (both the applications and the workspace), kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdesdk, kdetoys, kdeutils, and several applications from extragear such as Amarok, digiKam and kipi-plugins, KMyMoney, Konversation, KMid, Kile and Quassel.

Be aware that some of the software may be unstable on Windows, but chances are good that you might find yourself quite comfortable in using some of these applications.