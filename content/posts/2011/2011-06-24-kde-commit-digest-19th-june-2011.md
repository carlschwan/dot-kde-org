---
title: "KDE Commit-Digest for 19th June 2011"
date:    2011-06-24
authors:
  - "mrybczyn"
slug:    kde-commit-digest-19th-june-2011
comments:
  - subject: "Developer names encoding wrong?"
    date: 2011-06-24
    body: "I noticed that Dennis Nienh\u00fcser's name is truncated to Dennis Nienh - are the parts after a non-ascii character getting lost?"
    author: "Bille"
  - subject: "Thanks, fixed now.\nDanny"
    date: 2011-06-24
    body: "Thanks, fixed now.\r\n\r\nDanny"
    author: "dannya"
---
This week, the Commit Digest includes an article on the Kdenlive video editor.

Also in <a href="http://commit-digest.org/issues/2011-06-19/">this week's KDE Commit-Digest</a>:

<ul>
<li>Among other work, <a href="http://www.calligra-suite.org/">Calligra</a> sees improved OOXML, DOCX and MSWord support, Recent Projects assistant implemented in <a href="http://www.calligra-suite.org/kexi/">Kexi</a> and numerous bugfixes</li>
<li>Window decoration support for <a href="http://wayland.freedesktop.org/">Wayland</a> clients and other Wayland-specific modifications are added to <a href="http://userbase.kde.org/KWin">Kwin</a></li>
<li>Event loop in the IndexScheduler is added in <a href="http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/">Nepomuk</a></li>
<!--break-->
<li>Reduced memory usage for timezones in <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/">KDE-Libs</a> by using a private class in KTimeZoneBackend</li>
<li>Reworked item retrieval for Maildirs in <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a></li>
<li>Fixed magnitude parameters calculation for comets in <a href="http://edu.kde.org/kstars/index.php">KStars</a></li>
<li>Fixes to pgf and OpenMP support in <a href="http://userbase.kde.org/Digikam">Digikam</a></li>
<li>Bugfixes in <a href="http://userbase.kde.org/Krusader">Krusader</a>, including crash during a file move</li>
<li>Added Qt Mobility Location plugin as a position provider in <a href="http://userbase.kde.org/Marble">Marble</a></li>
<li>Added working archiver and extractor in <a href="http://userbase.kde.org/Gluon">Gluon</a></li>
<li>Bugfixes in <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/">KDE-Libs</a>, <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://userbase.kde.org/NetworkManagement"> NetworkManagement</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-06-19/">Read the rest of the Digest here</a>.