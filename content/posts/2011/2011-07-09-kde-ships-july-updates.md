---
title: "KDE Ships July Updates"
date:    2011-07-09
authors:
  - "sebas"
slug:    kde-ships-july-updates
comments:
  - subject: "Question"
    date: 2011-07-09
    body: "Do the changelogs lists \"all\" the bugs fixed in the release or just some of them?\r\n"
    author: "trixl."
  - subject: "just some...a small fraction of total"
    date: 2011-07-09
    body: "Since last monthly release update in June, from <a href=\"http://commit-digest.org/\">commit-digest website</a>:\r\n<big><code>\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; bugs\r\n&nbsp;date&nbsp; &nbsp;closed&nbsp; &nbsp;opened&nbsp; &nbsp;open(EOM)&nbsp; &nbsp; commits&nbsp; &nbsp;by #developers\r\nJune 12 &nbsp; 621 &nbsp; &nbsp; &nbsp; 435 &nbsp; &nbsp; 23601 &nbsp; &nbsp; &nbsp; 2230 &nbsp; &nbsp; &nbsp; 220   \r\nJune 19 &nbsp; 451 &nbsp; &nbsp; &nbsp; 526 &nbsp; &nbsp; 23758 &nbsp; &nbsp; &nbsp; 2756 &nbsp; &nbsp; &nbsp; 223         \r\nJune 26 &nbsp; 823 &nbsp; &nbsp; &nbsp; 434 &nbsp; &nbsp; 23390      &nbsp; &nbsp; &nbsp; 2183 &nbsp; &nbsp; &nbsp; 215\r\nJuly 03 &nbsp; 381 &nbsp; &nbsp; &nbsp; 418 &nbsp; &nbsp; 23493      &nbsp; &nbsp; &nbsp; 1917 &nbsp; &nbsp; &nbsp; 220\r\n</code></big>"
    author: "kallecarl"
  - subject: "I updated to the version but"
    date: 2011-07-10
    body: "I updated to the version but not all the bugs are covered. Some of the tools are user-friendly now thane the previous versions. Waiting for some more features.\r\n\r\nVineeta\r\n<a href=\"http://www.mykeralatour.in\"]Kerala Tour</a>"
    author: ""
---
<p>KDE has <a href="http://www.kde.org/announcements/announce-4.6.5.php">released</a> updates for its Workspaces, Applications, and Development Platform, the fourth in a series of monthly stabilization updates to the 4.6 series. 4.6.5 updates bring many bugfixes and translation updates on top of the latest edition in the 4.6 series. These updates are recommended for everyone running 4.6.4 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.</p>
<p>To  download source code or packages to install, go to the <a href="http://kde.org/info/4.6.5.php">4.6.5 Info Page</a>. The <a href="http://www.kde.org/announcements/changelogs/changelog4_6_4to4_6_5.php">changelog</a> lists more, but not all improvements since 4.6.4.</p>