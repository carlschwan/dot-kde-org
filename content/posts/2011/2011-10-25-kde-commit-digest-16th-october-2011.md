---
title: "KDE Commit-Digest for 16th October 2011"
date:    2011-10-25
authors:
  - "mrybczyn"
slug:    kde-commit-digest-16th-october-2011
comments:
  - subject: "New generator for KSudoku"
    date: 2011-10-27
    body: "Thank you very much for that! Now I can finally enjoy a good game :)"
    author: ""
  - subject: "Totally offtopic question"
    date: 2011-10-29
    body: "Im one of those that left KDE (in fact switched to osX) as a result of the KDE4 crap release. \r\nI've started to use Linux again - this time only using Gnome but not to satisfied.\r\nIs there any place where future reoadmap for KDE can be followed?\r\nIm using latest Ubuntu and tryed to install the latest KDE but its at least as frutrating to use as when i left and i havent found any forks."
    author: ""
  - subject: "Re: Totally offtopic question"
    date: 2011-10-29
    body: "Try Kubuntu.\r\n\r\nIn my opinion, KDE 4.7.x is simply wonderful.\r\n "
    author: ""
  - subject: "stick with some other operating sysem"
    date: 2011-10-30
    body: "Other operating systems have more appeal for people who insult their hosts, who don't know how to spell, and who are generally toward the clueless end of the spectrum. KDE 4.7 as frustrating to use as KDE 4.0? This is delusional.\r\n\r\nYou tried to install (apparently unsuccessfully) and then found it frustrating to use? Could you point to any of your posts on ubuntuforums so that your complaints have a little meat on them? Trying to use KDE without it being installed would be frustrating...like trying to change a flat tire and then trying to drive while the kiddy car is still up on the jack.\r\n\r\nA fork of KDE? mwahahahaha. Maybe you and the other smartest people in the world should take the readily available source code and do a KDE fork.\r\n\r\nKDE is not really looking for any more trolls, but thanks for the offer."
    author: ""
  - subject: "Table selection"
    date: 2011-11-01
    body: "Woot! This is fancy stuff, and very useful."
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-10-16/">this week's KDE Commit-Digest</a>:

<ul><li>Progress in C++2011 support in <a href="http://userbase.kde.org/KDevelop">KDevelop</a></li>
<li>Table selection tool in <a href="http://userbase.kde.org/Okular">Okular</a></li>
<!--break-->
<li>Work on PPTX, Webbrowser Widget, PDF conversion, and optimizations in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li><a href="http://userbase.kde.org/Skrooge">Scrooge</a> can download values of units with regular expressions</li>
<li>GPodder.net podcast status synchronzation in <a href="http://amarok.kde.org/">Amarok</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-10-16/">Read the rest of the Digest here</a>.