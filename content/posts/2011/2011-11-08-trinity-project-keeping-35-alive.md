---
title: "Trinity Project keeping 3.5 alive"
date:    2011-11-08
authors:
  - "kallecarl"
slug:    trinity-project-keeping-35-alive
comments:
  - subject: "Congratulations, and..."
    date: 2011-11-08
    body: "... I read \"Integration with applications like Firefox and NetworkManager with a new DBUS notification client\". \r\n\r\nIs trinity still DCOP based, or DBUS based or does it have some sort of DCOP<->DBUS gateway? Or all of them? ;)\r\n"
    author: "Vajsvarana"
  - subject: "The true power of FOSS"
    date: 2011-11-08
    body: "This is what make's 'Free and Open Source Software' so powerful, software evolves, sometimes requiring drastic revisions such as the jump from KDE 3.x to 4.x, but the old series is never confined to the bit bucket and fond memories, others can keep it alive, providing yet more choice's for users. "
    author: ""
  - subject: "Competition, competition, competition"
    date: 2011-11-08
    body: "The problem is that we can't foster growth in KDE and due to most people in Linux favouring fast development over say, stability, KDE4 could not be competitive enough after 4.0 and had large segments of market share taken away from it. This was all thanks to the stab-in-the-back of the KDE 3.5 developers who are trying so hard to keep this damned and outdated project alive.\r\n\r\nThis is the negative power of FOSS; forking splits the community if anything.\r\n\r\nI hope they don't receive funding from KDE e.V., otherwise I might concede to organising a DDoS as that would really anger me (just kidding!)."
    author: ""
  - subject: "DCOP"
    date: 2011-11-08
    body: "hey,\r\n\r\nI am part of the Trinity Development Team so i'll give this a shot. Basically we have decided not to convert to DBUS. the overhead to do so would be pretty in depth code wise for little purpose. DCOP has things DBUS doesn't, and vica versa.\r\n\r\nDCOP is small enough not to impact performance, so running both isn't an issue."
    author: ""
  - subject: "Some people don't like kde4"
    date: 2011-11-08
    body: "Some people don't like kde4 and how it works. Accept that.\r\nSome people want simple, fast and productive desktop enviroment, without unnecessary things like infinite applets, animations and so on.\r\nI'm glad that someone keeps KDE3.5 alive."
    author: ""
  - subject: "Reply to:  Competition, competition, competition"
    date: 2011-11-08
    body: "\"most people in Linux favouring fast development over say, stability\"\r\n\r\nQuite.  So are you saying that those of us who favour stability over fast development, are hereby kicked out of the FLOSS community so that you, and those who think like you, don't have to put up with the competition?  Why should we put up with a DE that we dislike (in my case strongly), so that you can have what you want?  Are you also saying that LXDE and KFCE4 must be assigned to the rubbish bucket?\r\n\r\nFLOSS is about choice, and anyone who wishes can make a fork.  If the fork is popular it will thrive.  If it is not, it will fall by the wayside.\r\n\r\nI am delighted to be able to continue with the more stable and more configurable KDE 3.x, and am very grateful to Timothy and the other developers for giving me the chance to make this choice.\r\n\r\nLisi\r\n\r\nIt occurs to me that you may just be trolling and I have now fed the troll.  But just in case....."
    author: ""
  - subject: "i guess you can foster KDE"
    date: 2011-11-08
    body: "i guess you can foster KDE growth by building something people want to use and like to use, right? sure, it would take to hear users and to learn from users. or you can always blame them forkers :)"
    author: ""
  - subject: "It's good to have a project like you around!"
    date: 2011-11-09
    body: "Yes, I'm still on KDE3 and I intend to stay on it if I can. It's great to have people like you around that continue to maintain and develop this wonderful piece of code. You may say a lot of things about KDE3, but it is a very reliable workhorse - and that is all I need."
    author: ""
  - subject: "You know..."
    date: 2011-11-09
    body: "You are not required to use \"unnecessary things like infinite applets\". KDE runs fine without compositing or any applets on the desktop."
    author: ""
  - subject: "\"Some people want simple,"
    date: 2011-11-09
    body: "\"Some people want simple, fast and productive desktop enviroment, without unnecessary things like infinite applets, animations and so on.\"\r\n\r\nThanks for showing off again the real attitude of most KDE4 \"critics\".\r\n\r\nYou mention \"simplicity\". How is KDE3 simpler than KDE4? Any proofs or are you confusing your \"I'm familiar with x and thus it is easier for me than y\" with claiming simplicity? Hello Windows user.\r\n\r\n\"fast\": Any proofs or just your assumptions? Your statements are far too general to be true or taken seriously. I can show you things in KDE4 that are faster than in KDE3, a lot, but I don't care about KDE3 and thus I do not criticise it \u2013 maybe you should learn from that.\r\n\r\n\"productive\": again, any proof that those using KDE4 are less productive? Or is it again just you who cannot or does not want to learn new things that improve productiveness or keep the level and simply look better. But hey, something that looks good cannot be productive \u2013 that's for sure!\r\n\r\n\"infinite applets\": I see. You are one of those that cannot say \"no\" but have to restrain themselves to have no choice. Otherwise you would not argue that way because \u2013 guess what \u2013 plasmoids and applets are optional in KDE4, so if you do not like them, you do not have to use them. You could even port your precious kicker or what not to KDE4 and use it.\r\n\r\n\"unnecessary\": Who are you to judge what is necessary and what not? a11y has improved since KDE3 \u2013 but who cares about those. I only would have to find a single applet or effect that helps somebody and would have proved you wrong. I guess by now you realise how obvious your lack of constructiveness in your criticism is and how one can only fail if one makes these kind of general statements.\r\n\r\n\"animations\": again. KDE4 gives you a choice, you can disable them.\r\n\r\nSo once again you showed off that most KDE3 users are unfortunately just KDE4 haters which stand in their own way and imply that KDE4 is not productive, slow, complicated to use etc. Pathetic.\r\n\r\nNobody wants to convince you to use KDE4, nobody claims that KDE4 is better than KDE3, you can use KDE3 as long as you want, nobody cares. Just don't show off your inferiority complex towards KDE4 by repeating the same general assumptions and allegations over and over again.\r\n\r\nYou should spend your time reading Qt3, KDE3 and KDE3-apps' code because those are unmaintained (packaging is not maintaining) and while there where dozens if not hundreds of professional coders which checked for security issues and fixed them, now there are not even a handful if any professionals which package that code and you can do the maths how much time they can spend on checking code, leaving alone knowing the code to a degree which is necessary to provide secure software. But, yeah, Qt3 is bug free and free of security issues, as is KDE3, KDE4 or any other piece of software\u2026\r\n\r\nHonestly, keep using KDE3 or any piece of software you like, but keep your allegations and general statements to yourself."
    author: ""
  - subject: "Great!"
    date: 2011-11-09
    body: "I'm on KDE 3.5, and I plan to stay here. At least until a number of really annoying things in KDE 4 are resolved, and its speed is as good as I'm used to from KDE 3.\r\n\r\nMy main machine is running Gentoo, which supports KDE 3 through its elaborate overlay system. Other machines I'm using are generally Fedora, and so the Trinity packages for Fedora are great news. There is a security problem with using packages from not-so-trusted sources, and I'll have to look into that of course.\r\n\r\nApparently some KDE developers are complaining over the fact that KDE 3 is being kept alive. Unbelievable! Surely it's a lone troll, right?\r\n\r\nThis is not the place to discuss KDE 4, so can I maybe just suggest that current KDE developers, for their own sake, try to look into why anybody would want to run an old version? I've already given two reasons, and I'll continue:\r\n\r\n- Missing features (like drag/drop in Konqueror's left pane)\r\n- Very awkward systems (like Activities)\r\n- Bloat (Nepomuk, Plasma and a 1000 other things that get in my way)\r\n- Speed (KDE4 absolutely doesn't feel as responsive as KDE3)\r\n- Kmail2 barely working (Yes, I can mix things, but why bother?)\r\n- I have a desktop computer with a large screen. Not a phone or a pad.\r\n- I am an experienced user, not a moron.\r\n- Get of my lawn.\r\n\r\nKDE 4 is certainly progressing. But it seems that to get a small feature back, I have to also get some whole new subsystem and workflow. Things that get in my way and that I'm not going to use, in exchange for something I already have in KDE 3 makes this an easy choice for me.\r\n\r\nOh look, I ended up discussing KDE 4 after all..."
    author: ""
  - subject: "I have not stabbed anybody in the back"
    date: 2011-11-09
    body: "I take great exception to the accusation that I (or anybody else) has stabbed KDE in the back by contributing to Trinity (in my case as a package maintainer). KDE left me and many others behind.\r\n\r\nFurthermore, I just don't see the point of either I or any of the other people involved in Trinity 'coming back' to KDE since we now have different views of where the desktop should be headed, views which I suspect are no longer capable of coexisting within the same project. The end result of such a return would be strife and/or a messy compromise which pleases nobody.\r\n\r\nKDE 4 has many great developers involved and I'm sure it works well for many people, but it doesn't work for me and the direction the project is currently headed in suggests that this situation is unlikely to change. Therefore, I will continue to contribute to Trinity."
    author: ""
  - subject: "Trinity's speed on old computers."
    date: 2011-11-09
    body: "I love KDE4 & generally prefer to use KDE4 instead of Trinity.  However, I maintain several old desktops & laptops that only have 512 MB of RAM or less.  I have tried KDE4 and Trinity DE on these old computers.  Trinity DE is fast on these old computers.  KDE4 is so slow on these computers, that they are unusable.  \r\n\r\nNow, if I spent a lot of time learning how to tweak KDE4 for use on old computers, maybe it would be just as fast as Trinity DE on these old machines, but I don't have time for that.  Trinity does the trick just fine on these old computers.  On PCs with 1.5 GB or more of RAM, I use KDE4 & it works beautifully.\r\n  \r\nAlso, I'm looking forward to hopefully having one of the Camp KDE gatherings (or whatever we end up calling it) in Bellingham, WA USA in 2012.  And Plasma Active looks very promising!\r\n\r\nCheers,\r\n\r\nElcaset"
    author: ""
  - subject: "Can't we just stay civilized?"
    date: 2011-11-09
    body: "There's no need at all to start yet another endless, mostly anonymous, discussion about whether KDE 4 is good or not, or whether KDE 3 is good, or not, or which one is better, with an endless re-threading over ground covered so often it looks like a WWI trench.\r\n\r\nThere are two things important:\r\n\r\n<ul>\r\n\r\n<li>People who felt the urge have stepped up and started working on Trinity, when it would have been so much easier to just complain. I have great respect for them, and I was really happy when Timothy Pearson promptly renamed the trinity fork of Krita when I asked him to.\r\n\r\n<li>The KDE project and community is open enough that Trinity can continue to be developed right inside the KDE subversion repository and news about can be published on dot.kde.org, when it would have been so much easier to just say \"go away\".\r\n\r\n</ul>\r\n\r\nAnd that should be enough for everyone: free software works like it should, there is an option for everyone, and nobody needs to go out and argue why they feel that KDE 4 developers are idiots who don't know what their users want or that Trinity developers are idiots who cannot possibly maintain what they started out to maintain. Whoever does so has not understood that there is no tribal war going on. We're a free software project, not acting in Lord of the Flies.\r\n\r\nThanks.\r\n\r\nBoudewijn (happy KDE user since 1.something, Krita maintainer since 2003)\r\n\r\n"
    author: ""
  - subject: "There's no need at all to"
    date: 2011-11-09
    body: "There's no need at all to start yet another endless, mostly anonymous, discussion about whether KDE 4 is good or not, or whether KDE 3 is good, or not, or which one is better, with an endless re-threading over ground covered so often it looks like a WWI trench.\r\n\r\nThere are two things important:\r\n\r\n<ul>\r\n\r\n<li>People who felt the urge have stepped up and started working on Trinity, when it would have been so much easier to just complain. I have great respect for them, and I was really happy when Timothy Pearson promptly renamed the trinity fork of Krita when I asked him to.\r\n\r\n<li>The KDE project and community is open enough that Trinity can continue to be developed right inside the KDE subversion repository and news about can be published on dot.kde.org, when it would have been so much easier to just say \"go away\".\r\n\r\n</ul>\r\n\r\nAnd that should be enough for everyone: free software works like it should, there is an option for everyone, and nobody needs to go out and argue why they feel that KDE 4 developers are idiots who don't know what their users want or that Trinity developers are idiots who cannot possibly maintain what they started out to maintain. Whoever does so has not understood that there is no tribal war going on. We're a free software project, not acting in Lord of the Flies.\r\n\r\nThanks.\r\n\r\nBoudewijn (KDE user since 1.something, Krita maintainer)\r\n\r\n"
    author: "Boudewijn Rempt"
  - subject: "indeed"
    date: 2011-11-09
    body: ".. that's pretty much what i thought when i read through the comments. in fact, it's pretty much what i wrote to the trinity mailing list a few days ago.\r\n\r\ni'm disappointed in a lot of the communication that happens around these issues, particularly as it pits people against each other who have far more in common than not. it also drives many of them to making claims that are or can not be backed up in an attempt to make their case seem stronger .. which just leads to all kinds of back-and-forth noise, reinforcing the \"pitting people against each other\" issue.\r\n\r\nwho cares! use what you like and be happy that it's all there.\r\n\r\nas for the \"backstabbing\" or \"turning their back on me\" sorts of statements: no, KDE did not turn their back on anyone. if KDE had, trinity would not be able to exist. some people confuse KDE for being a \"do what I want\" sort of slave community as opposed to a participation based community where those who do the work set the results. \r\n\r\nin this case, people have worked on Trinity and we have those results. great! i also don't see anyone, really, who would otherwise be contributing to the 4.x code bases working instead on Trinity. the vast majority of developers have continued working on 4.x and moving that now towards 5.x releases in the coming years (Frameworks 5 being the first step), and that has not been materially altered by Trinity.\r\n\r\nif anything, Trinity has given a good place for those to hang out who would otherwise simply throw negativity at the rest of the community who is working on moving things forward, albeit in a direction they (while not doing anything themselves) don't want.\r\n\r\nin other words, we're all getting what we want .. because people are putting effort into those things. applaud those efforts. save your condemnation for someone else."
    author: "aseigo"
  - subject: "Hmm"
    date: 2011-11-09
    body: ">\"fast\"\r\n\r\nYou should use KDE 3 on a recent computer, it is faster than KDE 4, it's easy to see:\r\n- KDE login is less than 2 seconds\r\n- Amarok launch is less than 3 seconds\r\n- All ui elements are faster\r\n\r\nI think KDE 4 need an optimisation release like with KDE 3.5.0 release (if i remember well)"
    author: ""
  - subject: "Some really good point Aaron!"
    date: 2011-11-09
    body: "I completely agree,\r\nit's those who do the work that set the results\r\n\r\nAs of me, the \"results\" I need more is a \"traditional\" rock-stable desktop to count on, so I'm really glad for Trinity taking some spin. \r\n\r\nBut of course there's also place for the ones who are more excited in trying new ideas and new ways, even if it means rewriting and retesting the whole thing every 2-3 years. \r\n\r\nAs long as there is place for both, why not?\r\n"
    author: "Vajsvarana"
  - subject: "The upcoming openSUSE 12.1 release will even feature the Trinity"
    date: 2011-11-09
    body: "That would surprise me.\r\n\r\nI am currently running an updated OpenSuSE 12.1-rc2 and I have been unable to find any repository featuring trinity. All I could find was an older 3.5.10 KDE. Also, I could not find any discussions in that regard and no one mentioned packaging Trinity for 12.1.\r\n\r\nI would appreciate a pointer to the RPMs."
    author: ""
  - subject: "Why do I feel so bad with KDE4"
    date: 2011-11-09
    body: "Hello,\r\n\r\nCongratilations to Trinity project developpers.\r\n\r\nI skip the description of my disappointments with the various distributions versions...\r\n\r\nI blame KDE for having abandoned users of version 3. Luckily I'm not at the head of a large company, if that were the case, I would have forced the installation of Suse (ie OSS 10.3) on the maximum number of workstations!\r\n\r\nHow long has the KDE4 project been started? where is it? If I had to hire a responsible \"research and development\", to still be a member of the team of KDE developers would be prohibitive. In fact, to persist for years to rewrite something that was working and <em>most importantly</em>, to give up the distribution (not to mention maintaining) of it is unforgivable.\r\n\r\nMore controversial: the approach of KDE4 seems to follow the spirit of the leader of the \"market\": the machines / grafical systems still need more power to run the applications they host...\r\n\r\nThanks.\r\n\r\n(Translated with help of Google)"
    author: ""
  - subject: "I really love KDE 4, but..."
    date: 2011-11-10
    body: "I would use Trinity in an old computer. I think Trinity would run awesomely in a Pentium III with 384 MB of RAM. KDE 4.7 can't run in that thing.\r\n\r\nFor that reason alone, Trinity must go on."
    author: "Alejandro Nova"
  - subject: "Thinking the same thing."
    date: 2011-11-10
    body: "I'm waiting for a Trinity spin of Fedora. Also, Trinity would kick the ass out of IceWM, LXDE, and the so called \"light\" desktops.\r\n\r\nTrinity has a place along KDE 4."
    author: "Alejandro Nova"
  - subject: "You, certainly, are not a backstabber. Thank you."
    date: 2011-11-10
    body: "I would like to thank you for keeping Trinity alive. I have four Pentium IV 1.6s with 512 MB of RAM to switch from Windows XP \"Guanaco Edition\" to Linux and I need something that runs fast on them. KDE 4 is pretty, beautiful, powerful, amazing, but it won't run as fast as Trinity on those computers.\r\n\r\nLong live Trinity AND KDE 4."
    author: "Alejandro Nova"
  - subject: "Agreed and congratulations to"
    date: 2011-11-10
    body: "Agreed and congratulations to the KDE Trinity project-team!\r\n"
    author: ""
  - subject: "And not just on \"old PCs\""
    date: 2011-11-10
    body: "Trinity has a place not just on old Personal, but wherever a lightweight, yet full-featured desktop is needed.\r\n\r\nI'm thinking public Kiosks or Terminal-servers..."
    author: "Vajsvarana"
  - subject: "But.."
    date: 2011-11-10
    body: "KDE4 has a lot of features that KDE3 don't. And look better and more polished than KDE3. Also Amarok has a lot of new feautres. \r\nActivities, semantic search, desktop effects, different architecture, a powerful and extensible krunner, a new file explorer (dolphin), a improved configuration system. You have 3 o 4 task manager to choose, 3 launchers, ... \r\nYou can't compare only time response without take in consideration all the new stuff that brings you KDE4. And with every new release they are optimizing more and more.\r\nFor me, KDE3 is good if you had a old pc. In my two years laptop, kde4 flies."
    author: ""
  - subject: "But"
    date: 2011-11-10
    body: "But all the things you're saying... you're too biased and negative. The basic answer is...You can disabled if you don't  want to use it the new features.\r\n\r\n- Missing features (like drag/drop in Konqueror's left pane)-> Use Dolphin instead, is the default file browser. \r\n\r\n- Very awkward systems (like Activities)--> Don't use it. I like it, by the way. \r\n\r\n- Bloat (Nepomuk, Plasma and a 1000 other things that get in my way)--> Disable nepoumk, don't put plasmoids in your desktop. Easy.\r\n\r\n- Speed (KDE4 absolutely doesn't feel as responsive as KDE3)--> True in old computers. But you have effects, eyecandy, semantic search... and tweaking a little, you'll have a fast desktop. \r\n \r\n- Kmail2 barely working (Yes, I can mix things, but why bother?) --> I agree.. but you're only saying negative things.. and how about the great improvements and new feautres in other applications? (digikam, okular, krita, kate,choqok,...) \r\n\r\n- I have a desktop computer with a large screen. Not a phone or a pad. --> Don't understand you... You can configure KDE4 as you needed (icon size, taskbar size and position, behaviour...)\r\n"
    author: ""
  - subject: "There's not just one KDE desktop environment"
    date: 2011-11-10
    body: "<cite>Some people don't like kde4 and how it works. Accept that.\r\n Some people want simple, fast and productive desktop enviroment, without unnecessary things like infinite applets, animations and so on.\r\n</cite>\r\n\r\nYou are clearly confusing KDE(4) with a desktop environment. Nobody prevents anybody from creating a simple, fast and productive desktop environment based on KDE technologies[*], if only those \"some people\" stopped moaning all the time and instead actually did something.\r\n\r\n[*] e.g. http://kde-apps.org/content/show.php?content=126302\r\n"
    author: ""
  - subject: "But it all comes from the use pattern."
    date: 2011-11-11
    body: "My use pattern:\r\n> Activities\r\nI don't need\r\n> semantic search\r\nI don't need (and it gets in my way)\r\n> desktop effects\r\nI don't need \r\n> different architecture\r\nI don't care\r\n> a powerful and extensible krunner\r\nI don't need\r\n> a new file explorer (dolphin)\r\nIt gets in my way\r\n> a improved configuration system\r\nI don't care\r\n> 3 o 4 task manager to choose, \r\nI don't need (but this one is the silliest! Is the one I choose slower because there are others? I hope not!)\r\n> 3 launchers\r\nditto...\r\n\r\nSo the comparation is good to me. What I use of Trinity is always faster than what I use of KDE4. \r\n\r\nI think the availability of features I don't use should not slow down the ones I use. This is usually a sign of bad underlying architecture, if I may say.\r\n\r\nOk, just my personal use-pattern and my personal experience, no offence intended, I know those features are used and cared about by many people.\r\nThat's why I'm glad there's space for both desktops."
    author: "Vajsvarana"
  - subject: "We should work as community"
    date: 2011-11-11
    body: "We should work as community not split apart.\r\n\r\nI started to use KDE 4 only in 4.5 version because the speed and feel 4.7 sluggish yet.\r\n\r\nMy computer is 3-4 years old. A very old computer these days. This is why I feel KDE 4.7 sluggish but I don't want to buy another only to run KDE 4.\r\n\r\nWhat can I do?? Help the community to develop a faster KDE 4.\r\n\r\nThere are few developers adding great features with little time to optimize the code, but anyone can help them.\r\n\r\nAnd to make the things worst, KDE 4 in a recent computer is not sluggish anyway. I have one of them in work.\r\n\r\nIf you think KDE 4 is sluggish, try Windows Vista/Seven. MacOS is always installed in a powerful hardware.\r\n\r\nMost people behave like has paid for the software instead to behave like a part of the community. The code is ours. The benefits too.\r\n\r\nTry to find the bottleneck, optimize yourself and send back the improvement. I know several people that can do it but chooses to complain. Those that don't have the skills could learn it. It is not that hard. Many optimizations is simple ones but someone has to investigate where is the bottleneck.\r\n\r\nAnd, of course, there always will be some old hardware that only run in old software. I think the Trinity is a great project and should continue for a long time."
    author: ""
  - subject: "RE: I really love KDE 4, but..."
    date: 2011-11-12
    body: "I hear ya, the same applies to low-powered netbooks as well. I mean, KDE 4 runs, but it feels sluggish. I hope this improves, but until then Trinity looks like a great choice."
    author: ""
  - subject: "Plasma Netbook runs great here"
    date: 2011-11-12
    body: "I don't know what \"low-powered netbooks\" means exactly. I have Plasma Netbook installed on a 3 year old  Asus Aspire. I maxed the memory ($20). It's plenty fast enough.\r\n\r\nInstalled the same for a friend, and suggested that he also max memory, even though his whatever-it-is MSI clunker worked fine without it. This replaced WinXP, which barely worked. He's thrilled with KDE 4 & Plasma."
    author: "kallecarl"
  - subject: "good"
    date: 2011-11-12
    body: "When KDE4 was released years ago, I didn't really like it. But the last years it was improved a lot. I know this, cause it's my main desktop for years already. (don't get me wrong: kde4* still has a lot of bugs, but there are minor for me)\r\nOn the other hand, I find it a very good initiative to keep the KDE3 series 'alive', and this because I want to keep KDE 3.5 for an old computer (700Mb) RAM. Ok, KDE4 also works over there, but I find it too slow, while KDE3.5 works fine over there.\r\nso yes, thank you Trinity.\r\nBut also thanks to the KDE community for the KDE4 releases."
    author: ""
  - subject: "Lame..."
    date: 2011-11-13
    body: "You are putting your use case against something you don't know how to use. Simple as that.\r\n\r\nContinue using KDE3 if you wish. Nobody will stop you. \r\n\r\nJust realize it's now Windows 98 to 7 for bug-fixes and security fixes. If you're fine with that, great, but don't speak about things you obviously don't know much about."
    author: ""
  - subject: "Agree..."
    date: 2011-11-13
    body: "Totally agree about Nepomuk and kmail2 not working..."
    author: ""
  - subject: "Thank you for your"
    date: 2011-11-13
    body: "Thank you for your contributions to Trinity. I'm really looking forward to upgrade from KDE4 to Trinity as soon as it gets better supported by the distributions...[removed troll comment]"
    author: "trixl."
  - subject: "Not for me.KDE developers"
    date: 2011-11-15
    body: "[edited trollish remarks]\r\n\r\n[KDE4] not for me.\r\n\r\nI want to continue using my computer how I want to.\r\n\r\nAnd I liked KDE 3.\r\n\r\nProps to all guys who keep KDE 3 alive!\r\n\r\nYou guys are my heroes and trust me, you are not alone!"
    author: ""
  - subject: "openSUSE 12.1 does not include Trinity and never will."
    date: 2011-11-15
    body: "openSUSE 12.1 does not include Trinity and never will. openSUSE comes with normal KDE 3."
    author: ""
  - subject: "It's allright"
    date: 2011-11-15
    body: "I was stuck with an outdated Debian release for a long time, to keep KDE3.5. Yes, I really liked KDE3.5. Then came Trinity and I could switch. Soon I found that KDE4 had matured to be good enough too and I switched. Having both Trinity and KDE4 at the same time was nice, so, I can say, Trinity helped me switch gradually to KDE4. Now I use KDE4 - mostly. Yes there are still some small issues, I miss some of the features of KDE3, but it ok. Thanks for the good work. "
    author: ""
  - subject: "Hi\nI am one of those that are"
    date: 2011-11-16
    body: "Hi\r\n\r\nI am one of those that are happely using trinity. And it is not due to that kde 4 is sluggish nor due to the direction of kde 4. The reason for me not using kde 4 is becouse of kde 4 is still missing support for Multi-Screen-Setup:\r\nhttps://bugs.kde.org/show_bug.cgi?id=256242\r\nfor me it is the only way to run my 3 screens becouse I use 2 cards to power them. Unto this bug is fixed or there is another solution coming op(I am fellowing wayland with some interrest but Nvidia does not have any plan to supporting it)"
    author: ""
  - subject: "I have something to tell..."
    date: 2011-11-16
    body: "I was always a KDE fan and I like KDE4 so much and I'm using it in my work PC. But at home I had used KDE 3.x series and was perfect at that time. When KDE 4.x released I tried it at home but my PC is old and weak to maintain KDE 4.x and became slow and laggy sometimes. So I used and forced to downgrade  to KDE 3.x While I'm still using KDE 4.x at WORK PC and i'm happy with both. I just wanted to tell you that some people don't like KDE 4.x and the other can not use it due to their old and weak PCs and here (KDE3.x is the solution).\r\n\r\nI'm happy that some one survived KDE 3.x and happy that KDE 4.x changed the way we use our systems and doing jobs with new technologies & efforts.\r\nThanks KDE for this :)\r\n\r\nRegards,\r\n\r\nTorn Hoopoe"
    author: ""
  - subject: ">i wrote to the trinity"
    date: 2011-11-18
    body: ">i wrote to the trinity mailing list a few days ago.\r\n\r\nI didn't know you were involved with Trinity. I know it's a fast, stable and sexy DE, but _please_ don't port Plasma to Trinity."
    author: ""
  - subject: "did you read his comment?"
    date: 2011-11-18
    body: "What is there in aseigo's comment that gives any hint that Plasma will be ported to Trinity?\r\n\r\nTroll? Failed attempt at humor?"
    author: "kallecarl"
  - subject: "Contributing"
    date: 2011-11-19
    body: "I really liked KDE 3, and I thought would it ever be back, this is awesome, Id like to help/contriute to this project."
    author: ""
  - subject: "How can i disable Plasma? Or"
    date: 2011-11-19
    body: "How can i disable Plasma? Or is there some sort of NoScript for Plasma?"
    author: ""
  - subject: "Thanks to Trinity for revitalizing Classic KDE."
    date: 2011-11-20
    body: "There are many people who prefer to use instruments/tools they used to [sic]. \r\n\r\n[troll-bage removed]\r\n\r\nThanks to Trinity for revitalizing Classic KDE.\r\nObviously, it's all about desktop environment, though.\r\n\r\n"
    author: ""
  - subject: "Yess!"
    date: 2011-11-21
    body: "KDE3 makes it possible to work real fast, and i have found no other ways to work efficient.\r\nIf you just learn few shortcuts, and some hidden features, you can do almost everything with some quick keystrokes on the keyboard.\r\n The first things to do for new users, is to google for Konqueror Web Shortcuts: http://www.tuxmagazine.com/node/1000020. I have never found a web browser with so easy going user interface. Google and Apple and may be others, are using the web-engine from KDE and Konqueror in there browsers, but they are missing the best part of Konqueror: The user interface with all the shortcuts.\r\n The next thing about KDE, is the file-dialog for opening and saving files: You can use almost any protocol to open files from remote hosts in almost any application. To open a file via ssh, you can write fish://username@hostname/folder/file in the location bar.\r\n And konsole for kde3 is just amazing: If you are using a lot of tabs, with different settings, and connections to different ssh-servers, etc. you can just save the profile, and you can edit the profiles under ~/.kde/share/apps/konsole/profileskonsole. Later, you can start konsole with the -session option."
    author: ""
  - subject: "I agree (!) to censorship, but..."
    date: 2011-11-21
    body: "Ok,\r\nthis is not the first time I see comments on news.kde.org, massively edited, or deleted altogether, because of \"trolling\", or \"ranting\", or such.\r\n\r\nIf censorship is a rule of this \"community\", who am I to debate.\r\n\r\nBut it would be nice to have a PUBLIC statement on this, and most of all I would like to know WHO is the dear big brother having fun with our posts.\r\n\r\nUnless, of course, he/she prefer to remain anonymous. As the trolls he/she edits. \r\n\r\nOr edit this post so I'll appear to be asking to wash your car, as you wish ;)"
    author: "Vajsvarana"
  - subject: "here's your PUBLIC statement"
    date: 2011-11-21
    body: "http://kde.org/code-of-conduct/ is publicly posted. People visiting an official KDE website have a reasonable expectation that it will be followed.\r\n\r\nThe Code of Conduct specifically states:\r\n\"Leaders of any group, such as moderators of mailing lists, IRC channels, forums, etc., will exercise the right to suspend access to any person who persistently breaks our shared Code of Conduct.\"\r\n\r\nI have deleted and edited comments, as have others with leadership responsibility. I will continue to do so in cases that don't fit with the Code of Conduct. Although it's not addressed directly in the Code of Conduct, <a href=\"http://en.wikipedia.org/wiki/Troll_(Internet)\">troll content</a> might also be deleted or elided.\r\n\r\nWhile I had nothing to do with creating the Code of Conduct, I fully support it and have no wish to change it. Anyone is welcome to offer suggestions for changing it. Please join the <a href=\"https://mail.kde.org/mailman/listinfo/kde-promo\">KDE Promo mailing list</a> and make your case.\r\n\r\nPlease provide support for your assertion that comments were \"massively edited, or deleted altogether\" because of \"ranting\". \r\n\r\nHow do you suggest handling content that offends the KDE Code of Conduct?\r\n\r\n\r\n\r\n\r\n\r\n"
    author: "kallecarl"
  - subject: "[biting my own tongue]"
    date: 2011-11-21
    body: "[edited by myself... trying a last attempt of resolving the thing politely]"
    author: "Vajsvarana"
  - subject: "removing access "
    date: 2011-11-21
    body: "Vajsvarana sent a private message containing the following...\r\n\"Thus, I hereby officially ask that your access as a moderator of this forum is removed.\"\r\nThere are various ways to make such an official request at http://ev.kde.org/contact.php. I don't intend to make such a request myself.\r\n\r\nUser Vajsvarana had no constructive suggestions in that private message for how to handle content that offends the KDE Code of Conduct.\r\n\r\n\r\n\r\n\r\n\r\n"
    author: "kallecarl"
  - subject: "You want constructive suggestion?"
    date: 2011-11-22
    body: "User Vajsvarana has just one right, constructive suggestion for you: when you have to weight \"Freedom of speech\" against whichever \"code of conduit\" in your life, well, screw the f***k up the code of conduit! \r\n\r\nOr just continue as you have done so far. Great results you have, mh? How much positivity and respect around ...\r\n\r\n\r\n"
    author: "Vajsvarana"
  - subject: "The KDE Code of Conduct does"
    date: 2011-11-22
    body: "The KDE Code of Conduct does not preclude criticism of any project or application.  What it does require is respect for others.  I cannot comment on messages I haven't seen, but if this one is a sample, you are certainly not showing respect, and I would support the moderator's removal of disrespectful comments, for the well-being of the community.  \r\n\r\n(To all contributors to these comments) Please try to limit comments to likes and preferences, which are legitimate, and please do not attack others for having opinions different to yours.\r\n\r\nFWIW, my belief is that both KDE 4.x and KDE 3.x have their good points, and the Trinity project should be just another project within the KDE umbrella.  I see no difference between that and multiple multimedia projects, other than it needs to be bigger, since it wouldn't interact with the 'newer' parts, but then I'm not a developer, and there could be other valid reasons.\r\n\r\n"
    author: ""
  - subject: "But"
    date: 2011-11-22
    body: "unfortunately I loathe Dolphin so that is no fix at all"
    author: "MamiyaOtaru"
  - subject: "neat"
    date: 2011-11-22
    body: "I stopped using Linux on the desktop when KDE4 came out.  This might draw me back in.  \r\n\r\nI'm just super conservative with my UI like that.  I'm currently using WinServer2008r2 with the classic widget style, and a program to make the start menu behave like the one in win9x (and not looking forward to win8)"
    author: "MamiyaOtaru"
  - subject: "KDE 4 not slow."
    date: 2011-11-26
    body: "KDE 4 runs fast on a computer running pentium 4 with 511 mbs of memory."
    author: ""
  - subject: "why not kde4?"
    date: 2011-12-10
    body: "At my first kde4 install I realised I could not use my years old configuration and tailoring. Not even the same desktop appearance could be reached. Actually, the best desktop view I was able to get could be called ugly.\r\nBy the time this happened, learning and building everything again was not feasible. So I became a kde3 user by default and never gave kde4 another try.\r\nMy judgement of the situation is that kde4, no matter how brilliant and  innovative, created unnecessary difficulties that could never exist in upgrades, especially for common users like me. It seemed to have been created to and for the creators themselves, not for users.\r\nThe radical change was not dictated by technological reasons like the windows move from 16bit win311 to 32bit w95 and followers. Not even by market pressure as kde3 was a true conqueror of hearts and minds. The move looked a bit whimsy.\r\nNever gave kde4 another try and will not do this while Trinity (kde3) works fine. As there is not major technological breakthrough to affect end users in sight, I'd say it will take several years before another desktop needs to be learned.\r\nThank you for all of you there that maintain and support Trinity.\r\n\r\nAlexandre Aguiar"
    author: ""
  - subject: "can't argue with your experience, but..."
    date: 2011-12-11
    body: "\"ugly\" is a personal opinion.\r\n\r\n\"kde4...created unnecessary difficulties that could never exist in upgrades...\" simply is not true.\r\n\r\n\"...seemed to have been created to and for the creators themselves...\". How things seem is a personal interpretation, so how it seemed to you is your business. The fact is that it's not how KDE4 was developed.\r\n\r\n\"...radical change was not dictated by technological reasons...\" Not so. The radical change to KDE 4 was a response to technological changes outside of KDE. Planning and extensive conversations happened for years before KDE 4 was released.\r\n\r\n\"whimsy\"? Well 4+ years of planning and development are not hardly whimsical. What software project have you been associated with for 4 intensive years? Very little whimsy happening there.\r\n\r\nIt's good you like Trinity and KDE3. Really no more reason than that is needed. There's certainly no need to disparage KDE 4."
    author: "kallecarl"
  - subject: "Freedom of Speech has nothing to do with this"
    date: 2011-12-12
    body: "Here, you are on KDE infrastructure, maintained by KDE people, for fun. You play by our rules - or you go away. It is as simple as that. Freedom of Speech* has NOTHING to do with it - you can have that wherever else you want, just not on OUR space. We respectfully decline to listen to your rants as they bore the heck out of us. \r\n\r\nPlease find yourself a nice, empty room where you can excercise your Freedom of Speech by yelling at the walls.\r\n\r\n\r\n*Freedom of Speech is about being able to say whatever you want. But others still have the right not to listen to you. Or tell you to find your own place to rant as they'd like to keep their place free of you."
    author: "jospoortvliet"
  - subject: "unfortunately it does depend"
    date: 2011-12-12
    body: "unfortunately it does depend a bit on what you do. With the number of mails I have, KMail2 alone eats 512 MB of ram easily. KMail1 would go up to about 250 so that one was better in this regard. Then again, KMail2 takes 5 seconds to check my imap accounts for new mail while KMail1 could spend 5 minutes on the same task. With bad network it would break and restart up to the point where I couldn't use it to read mail on most conferences.\r\n\r\nYup, it's two steps forward, one step backward. Just stick around until we've got that step backwards solved too :D"
    author: "jospoortvliet"
  - subject: "Well..."
    date: 2011-12-13
    body: "More like Windows XP to Windows 7, really. A LOT of people still like XP, much like how people like KDE3...See the similarity?"
    author: ""
  - subject: "I Agree"
    date: 2012-05-19
    body: "I've been using KDE3 since years and have been very satisfied. Upgrading my Debian with manually configured kernel to squeeze broke about everything, partly because I needed to adjust my kernel config which helped. But the key issue then was that Squeeze by default comes with KDE4.\r\n\r\nI have four machines running Debian, most of which with pretty outdated hardware. Two out of four run as servers, running VNC servers. It turns out that KDE4 doesn't work with VNC which was a real showstopper. Fortunately, a friend told me about Trinity. This solved most of my Squeeze problems.\r\n\r\nSo, thanks to the crew maintaining Trinity. Cool stuff, helping to keep me on KDE instead of migrating to Gnome."
    author: "donaldduck"
---
For people who prefer the KDE 3.5-style desktop, a new version of the <a href="http://www.trinitydesktop.org/">Trinity Desktop Environment</a> (TDE) has been released. Trinity is a continuation of the KDE 3.5 Desktop Environment with ongoing updates and new features. Trinity Desktop Environment 3.5.13 source code is available and the project also provides packages for Debian, Ubuntu and Fedora. Read on for an overview of what is new in Trinity 3.5.13!
<!--break-->
<img src="http://dot.kde.org/sites/dot.kde.org/files/sm_kde3_5_maverick_livecd.png" alt="Trinity screenshot" align="right" /> 


<h2>Features and fixes</h2>
There has been a number of changes and improvements to Trinity in the 3.5.13 release. The most visible of those are:
<ul>
<li>Added new "Monitor and Display" control center module for system-wide single/multi monitor and display configuration</li>
<li>Integration with applications like Firefox and NetworkManager with a new DBUS notification client</li>
<li>A new compositor is included and several applications including Amarok have been modified to take advantage of that</li>
<li>A new widget theme, Asteroid, has been added and the GTK-Qt theme engine has been improved</li>
<li>An optional Secure Attention Key has been implemented to further secure the login and desktop lock dialogs</li>
<li>The quicklaunch kicker applet has been improved and Kicker can now have "Deep Buttons"</li>
<li>KRandr now has DPMS support and Gamma configuration</li>
</ul>

There is also a number of new applications added to this Trinity release:
<ul>
<li>kbookreader</li>
<li>kdbusnotification</li>
<li>kmymoney</li>
<li>kstreamripper</li> 
</ul>

<h3>Under the hood</h3>
The Trinity project is working hard to make TDE 3.5 easier to maintain. Many of the modules have been ported to CMAKE in this release. There have been cleanups of a number of libraries, and many stability fixes. For example, Flash no longer segfaults Konqueror and the lockups caused by OpenGL screensavers have been fixed.

The Trinity project also announced that they are taking over Qt3 maintenance from now on.

<h2>Congratulations to Trinity!</h2>
While the KDE community is focused on developing the 4.x series of KDE Platform, Plasma Workspaces and Applications, we welcome and support the efforts of the Trinity Project to provide a desktop environment for those who want to continue using the 3.x series!