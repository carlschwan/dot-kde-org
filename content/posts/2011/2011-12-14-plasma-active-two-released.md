---
title: "Plasma Active Two Released"
date:    2011-12-14
authors:
  - "kallecarl"
slug:    plasma-active-two-released
comments:
  - subject: "Very nice"
    date: 2011-12-14
    body: "I don't like the default font, but apart from that this is really, really awesome.\r\n\r\nVery nice, but maybe you'll have to come up with a way to explain activities to people. I'm still stumped as to what to do with them. Maybe a built-in tutorial, video-game style would suffice for the purpose?"
    author: ""
  - subject: "Explaining Activities"
    date: 2011-12-14
    body: "http://kde.org/announcements/plasma-active-one/ provides a sense of Activities. However, if a person doesn't have a tablet, then explanations are likely to fall short.\r\n\r\nI've demoed Plasma Active on a tablet to many people. They say that they understand. One university student experimented less than a minute, and then said, \"This means that I could have a different computer for each of my classes. Like I could have a Math Activity, that would have only those things I need for Math...assignments, applications, notes. Separate Activities for all of my other classes. Plus a Facebook Activity.\"\r\n\r\nActivities are personalized. Explaining \"personalized\" is different from understanding how Activities \"personalize for me\"."
    author: "kallecarl"
  - subject: "Multi-touch / two finger zoom ?"
    date: 2011-12-14
    body: "Just saw the video and was wondering, if it is also possible to use multi-touch gestures to zoom in and out when diplaying pictures?\r\n\r\nIn the video the demonstrator tips \"+\" and \"-\" symbols to zoom in and out. I think nowadays most people are used to multi-touch gestures from their IPhone or Android devices.\r\n\r\nIs it already implemented or did I miss something?\r\n"
    author: "thomas"
  - subject: "multitouch is also available"
    date: 2011-12-14
    body: "Multitouch / pinch-to-zoom works in the MeeGo and Mer images, it's just not shown in the video."
    author: "sebas"
  - subject: "WARNING out-of-topic, is"
    date: 2011-12-15
    body: "WARNING out-of-topic, is there a way to use pinch to zoom in kde's plasma desktop?"
    author: ""
  - subject: "My Eyes! OO"
    date: 2011-12-15
    body: "Holy crap.\r\n\r\nThis is what the <a href=\"http://imgur.com/eYy28\">homepage</a> looks like. \r\n\r\nI don't think I've ever seen a crappier piece of web design (well, Kotaku might come close). The text is so small it turns fuzzy like it's 1995.\r\n\r\nI know that all web \"designers\" subscribe to the creed that font size settings are for losers and the smaller the better but some (like the .) are at least borderline readable without special zoom extensions. Is anyone surprised that plasma cractive central doesn't zoom well?\r\n\r\nOn a tablet it must be completely unusable.\r\n\r\nIf you want I can help you with a redesign; my dog's got nothing to do and sometimes likes to hack with his paws on the keyboard. The results are vastly better than that unreadable POS (not that I've yet to even mention the fact that the layout fails disgracefully if there's no js)."
    author: ""
  - subject: "Code of Conduct"
    date: 2011-12-15
    body: "<cite>I don't think I've ever seen a crappier piece of web design (well, Kotaku might come close). The text is so small it turns fuzzy like it's 1995.</cite>\r\n\r\nPlease read http://www.kde.org/code-of-conduct before posting a rant. Thank you."
    author: "einar"
  - subject: "xD"
    date: 2011-12-15
    body: "Rather funny! But I think the other response is right: you should keep the acid humor for yourself and try to be more diplomatic when writing.\r\n\r\nI bet most of the moderate comments here and at forum.kde.org are in reality a \"translation\" of the real feelings after some KDE's crap, but being too sincere doesn't motivate developers at all; even if you think the problem is so obvious that anyone should have noticed, the fact is the most of the time it isn't, for instance, that font size you blame so much, is perfectly readable and agreeable on my screen. Different hardware may show different results, and \"designers\" can't test their sites on every machine on earth.\r\n\r\nI agree with the javascript crap, though: almost every security-concerned user browses the Internet using some script blocker or disables js, and there are other web desing methods for achieving the same results in the page."
    author: ""
  - subject: "Home page"
    date: 2011-12-15
    body: "I wonder if it's browser-dependent? Here, the fonts are perfectly readable."
    author: ""
  - subject: "announcements about the availability of tablets"
    date: 2011-12-15
    body: "When can I buy a tablet with plasma active two pre-installed? Chritsmas is soon."
    author: ""
  - subject: "re: WARNING out-of-topic, is"
    date: 2011-12-15
    body: "Not at this moment. As far as I know multitouch ability in plasma depends on the multitouch ability of Qt. With Qt 4.8 moultitouch should be integrated. The actual KDE SC depends on 4.7.\r\nOn megoo multitouch is working because the use a patched Qt 4.7."
    author: ""
  - subject: "ergonomy is also about chosing the right words"
    date: 2011-12-15
    body: "This looks nice but you should consider working on the way you name things. For instance : from the release announcement screenshots, one can read \"use NTP\". For the average Joe, maybe this should be called \"Keep clock synchronized\" or something like that. Just my 2 cents."
    author: ""
  - subject: "soon!"
    date: 2011-12-16
    body: "Hopefully some time next year but it might take until 2013... Certainly not before this Christmas, sorry."
    author: "jospoortvliet"
  - subject: "This isn't soon."
    date: 2011-12-17
    body: "What can I do?"
    author: ""
  - subject: "Code of Conduct"
    date: 2011-12-17
    body: "And?\r\n\r\nAre you now saying criticism is not allowed?"
    author: ""
  - subject: "Criticism"
    date: 2011-12-17
    body: "I would have expressed it differently than the original author.\r\n\r\nBut point is - either his criticism in itself is correct or it is not.\r\n\r\nAnd if it is correct, then it should be addressed, irrelevant of whether you like the tone or not."
    author: ""
  - subject: "top-right icons"
    date: 2011-12-18
    body: "That looks really cool.  The only thing that looks problematic is the icons at the top-right on the top bar.  They appear to be too small and/or too close together, and the user in the video is being really careful to try and make sure they get the right one (and they are unsure until the result of the click happens).  Maybe they could be bigger or not as close together?  Or maybe when you touch there it should first make the icon it thinks you're touching bigger (not unlike some existing things that do that on mouseover) and you can slide left and right if it's on the wrong one and then tap again once you've got the right one.\r\n\r\nI also like the music, it reminds me of Drown by Melanie C :o)"
    author: "Luigiwalser"
  - subject: "Code of Conduct...And what?"
    date: 2011-12-18
    body: "This forum is provided by KDE. It is based on the KDE Code of Conduct agreed to by the Community.\r\n\r\nPoster einar asked that the commenter to read the Code of Conduct before posting a rant.\r\n\r\nThere is no suggestion in einar's comment that criticism is not allowed. So--to answer your question--NO. einar said nothing of the sort, nor was anything like that implied. You made that up.\r\n\r\nMore to the point...are you suggesting that einar did something wrong by asking someone to read the Code of Conduct before posting a rant? If so, then you are in the wrong forum...it is operated and moderated in accord with the KDE Code of Conduct.\r\n\r\n\r\n\r\n"
    author: "kallecarl"
  - subject: "On Archos G9?"
    date: 2011-12-19
    body: "A hint: <a href=\"http://news.kde.org/2011/11/30/plasma-active-archos-g9-tablet\">http://news.kde.org/2011/11/30/plasma-active-archos-g9-tablet</a>\r\n\r\nBut nothing sure until they'd release the SDE."
    author: ""
---
<div style="float: right; margin: 1ex;"><a href="http://plasma-active.org/"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma-active-logo300px.png" /></div>

Plasma Active Two has been released. In just two months, the Plasma Active development team has made significant improvements over <a href="http://kde.org/announcements/plasma-active-one/">Plasma Active One</a>. The <a href="http://www.kde.org/announcements/plasma-active-two/">release announcement</a> has more information, including a video and sites for downloading and installation instructions</a>.
<!--break-->
<p align="justify">A video introduction to Plasma Active Two, which also appears on both the live and installable device images, can be viewed below or <a href="http://share.basyskom.com/contour/UIDesign/RC2_plasma_active_two-with_Audio.ogv">downloaded</a>.</p>

<div align="center"><iframe style="margin-top: 4em" width="400" height="240" src="http://www.youtube.com/embed/UPkYyDiuGyc" frameborder="0" allowfullscreen></iframe></div>

<h3>Improved User Experience</h3>
The Plasma Active team used valuable user feedback and real-world usage to improve the end-user experience significantly over Plasma Active One. As a result, operation is more natural, faster, easier and smoother. Default settings were adjusted to provide a better "out-of-the-box" experience.

<h3>Better Performance</h3> 
Functions are more fluid. In some cases, they are more than 10 times faster than Plasma Active One. The QtQuick-based interface has been optimized for a much-needed performance boost.

The team's ultimate goal is to provide a good Plasma Active experience on devices with as little as 256MB of RAM and sub-1GHz processors. Plasma Active Two is a move in that direction. Developers will continue to focus on this goal in upcoming releases. Future development will support Plasma Active running on a wide range of devices that support touch interaction.</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="http://www.kde.org/announcements/plasma-active-two/images/PlasmaActive_ActivitySwitcher.png"><img src="http://www.kde.org/announcements/plasma-active-two/images/thumbs/PlasmaActive_ActivitySwitcher_Dot.png" align="center" width="400" alt="Plasma Active's Activities organize your device" title="Activities organize your device" /></a>
<br />
<em>Plasma Active's Activities organize your device</em>
</div>

<h3>Recommendations</h3> 
Plasma Active Two has one significant new feature―<strong>Recommendations</strong>. Plasma Active is now able to learn usage patterns. It analyzes that information to make recommendations based on what the user is doing at the moment―a valuable feature for computing-on-the-go. This capability is based on the "semantic desktop" of <a href="http://dot.kde.org/2011/09/21/nepomuk-stability-and-performance">KDE Nepomuk</a> and happens right on the device. No Internet connection is required. This brand new technology is <em>not available on any other mobile device</em>.

<h3>Expanded Device Support</h3> 
In the last two months, Plasma Active has been brought up on over a dozen different kinds of devices using both Intel and ARM architectures. At least two announcements are expected within the next month about the availability of tablets with Plasma Active Two pre-installed. 

<h3>Getting Involved</h3> 
<p>Plasma Active is fully open and transparent in design, development and deployment. There are plenty of opportunities for companies and individuals to participate. Plasma Active Three development―focused on features―begins in January, and is expected to be released in Summer 2012.

The <a href="http://www.kde.org/announcements/plasma-active-two/">release announcement</a> has information about how to get involved.

<h3>Thank You</h3>
Thanks to community supporters, developers, designers, release and packaging experts, hardware hackers and project leaders for making Plasma Active Two a reality in such a short time. Plasma Active depends on technologies from KDE, Qt and Linux; thanks also to everyone who has contributed through those projects.