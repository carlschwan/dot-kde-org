---
title: "Joining the Game at the Desktop Summit"
date:    2011-08-11
authors:
  - "Stuart Jarvis"
slug:    joining-game-desktop-summit
---

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/jonathan.jpeg" /><br />Jonathan Kolberg</div>Desktop Summit 2011 continues. On Tuesday, GNOME and KDE had their annual meetings, with <a href="http://ev.kde.org/">KDE e.V.</a> managing to finish in time for lunch. However, many members came back after lunch to continue discussion in a BoF about challenges and opportunities for KDE. Over the past few days, the University has been busy with four tracks of BoFs, workshops and meetings - enough to keep most attendees checking their schedules and the building maps.

One particular highlight of the past few days was the <a href="http://www.appup.com/">Intel AppUp</a> workshop, in which conference attendees who had preregistered were able to learn about the AppUp program, including App stores and development tools to optimize and distribute applications for Windows and MeeGo. The Intel team explained that Intel AppUp is the only large online Appstore system that is really friendly to distributing free software. For example, if the application developer chooses GPL, they are prompted to upload source code which can then be automatically distributed along with the applications binary. As a bonus, Intel loaned each of the workshop attendees a developer tablet running MeeGo to enable easy testing of applications and aid development.

<h2>Supporting Member's View</h2>

One conference attendee we wanted to track down was Jonathan Kolberg. One of our <a href="http://jointhegame.kde.org">Individual Supporting Members</a>, he took advantage of the right that goes with supporting membership to observe the KDE e.V. general meeting. Stu Jarvis caught up with him to ask why he decided to <a href="http://jointhegame.kde.org/">Join the Game</a>, what he made of the KDE e.V. meeting and the Desktop Summit as a whole, and how he is enjoying playing with the Intel developer tablet.

<b>Stu:</b> Hi, it's great to meet you - who are you and what do you do?

<b>Jonathan:</b> Hi, I'm living in Löllbach, just starting studies in informatics at Kaiserslautern. I'm 19 years old.

<b>Stu:</b> When did you discover KDE software?

<b>Jonathan:</b> It started about three years ago. I bought a new PC without an operating system and my father bought me Windows XP to use. However, my mum accidentally threw away the license key. I had read about Linux in Chip Magazine and tried Kubuntu 8.04 with KDE 3.5.10 to start with. Two or three days later I tried Plasma Desktop 4.0. Plasma crashed a lot, but there was an update a few days later and it got better, but overall it was already looking really cool. So that was the way I came to KDE and Kubuntu. I was in the forum and asked lots of questions and after a while I was able to answer some questions too and got invited to become a moderator at kubuntu-de.org.

<b>Stu:</b> Why did you join the game?

<b>Jonathan:</b> I want to help the community to have the money to afford some things that are essential, such as servers for source code and the websites. I thought, "Hey it isn't that much money but if enough people do that it will have a lot of impact." I already saw the value in 4.0, it was crashy but looked great and there was clearly a lot of work involved in it. I joined at Linux Tag. I was at the Kubuntu booth next to the KDE booth and we were working together a lot. I will definitely renew my membership next year.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasmatablet.jpeg" /><br />Plasma Desktop on the Intel tablet</div><b>Stu:</b> Do you do anything else for KDE, besides the financial contribution?

<b>Jonathan:</b> I do some user support in the forums, as I mentioned. Also some documentation for Rekonq. I want to code but... [chuckles]. The barrier to coding is a question of time and skills. Skills should not be a problem for long, as I should learn at university. I can read code that is not too difficult, but it takes a bit of time to understand it. I haven't found the right place to start I guess.

<b>Stu:</b> How was your experience of the Desktop Summit and KDE e.V. meeting?

<b>Jonathan:</b> It's the first Akademy or Desktop Summit that I attended. I found it quite cool, you learn a lot, you get to know people and connect faces to IRC nicknames and that is awesome. As for the meeting, I just wanted to see how things happen and see what is happening to my money. Really, the money is well used, I think. It is really important to support contributors to go to the conferences and the sprints. The sprints are where the great stuff is done, or at least started. KDE e.V. is the representative of KDE in legal and financial matters. We can trust in the e.V. to do what is necessary to help the community.

<b>Stu:</b> From going to the AppUp workshop you got access to one of the developer tablets. How is it?

<b>Jonathan:</b> It's really nice. In addition to trying out MeeGo and thinking about possible apps, I also wanted to try out how it would work with Kubuntu. So I ran a live image from USB and it was great to see Plasma Desktop running well. I'd also like to experiment with Plasma Active and of course next versions of MeeGo too.

<b>Stu:</b> Well it's been great to meet you and I'd just like to say thank you again for Joining the Game. People like you have made it possible for the Dot team to be here and cover the event for those who could not attend, in addition to helping the software development. Thanks for talking to us.

<b>Jonathan:</b> You're very welcome. It is really great to be here and meet everyone. I know that my money is being used to do great things and that makes me really feel a part of the community.