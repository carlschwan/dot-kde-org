---
title: "KDE Commit-Digest for 7th August 2011"
date:    2011-08-13
authors:
  - "mrybczyn"
slug:    kde-commit-digest-7th-august-2011
---
In <a href="http://commit-digest.org/issues/2011-08-07/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.calligra-suite.org/">Calligra</a> sees work on DOC list styles, improved drag-and-drop in Document Structure Docker and multiple bugfixes; kformula becomes a shape</li>
<li>The split/migration of image and thumbnails databases has started in <a href="http://www.digikam.org/">Digikam</a></li>
<li>There is a new and improved describeResources method in KDE-Runtime</li>
<!--break-->
<li><a href="http://userbase.kde.org/Kate">Kate</a> now highlights GNU LD scripts</li>
<li>There is a new journey search string parser in <a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a></li>
<li>Optimizations have been made in route rendering in <a href="http://edu.kde.org/marble/">Marble</a>, and preview rendering in KDE-Base</li>
<li>Umbrello receives work on the new code importing wizard</li>
<li>The way <a href="http://userbase.kde.org/Konsole">Konsole</a> queries for the PTY size is changed, this should fix multiple problems with terminal dimensions</li>
<li>Bugfixes in <a href="http://userbase.kde.org/Kate">Kate</a>, <a href="http://userbase.kde.org/Amarok">Amarok</a>, <a href="http://userbase.kde.org/Phonon">Phonon</a>, <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-08-07/">Read the rest of the Digest here</a>.