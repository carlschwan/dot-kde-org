---
title: "KDE Commit-Digest for 12th June 2011"
date:    2011-06-17
authors:
  - "vladislavb"
slug:    kde-commit-digest-12th-june-2011
---
This week, the Commit Digest includes an article on the status of Phonon-Xine and an update on the new Wayland support in KWin.

Also in <a href="http://commit-digest.org/issues/2011-06-12/">this week's KDE Commit-Digest</a>:

<ul>
<li>Among much bugfixing and optimization work, <a href="http://www.calligra-suite.org/">Calligra</a> sees further improvements to .doc, .xls, .xlsx, and .odt support, work on <a href="http://www.calligra-suite.org/stage/">Stage</a>, new <a href="http://www.calligra-suite.org/kexi/">Kexi</a> web widgets, improvements to Save Incremental support, finishing touches to the new strip presets picker, and flow and opacity functionality in <a href="http://krita.org/">Krita</a> equal to Photoshop&#174;</li>
<li>In <a href="http://www.kdevelop.org/">KDevelop</a>, major bugfixing and work on the new implementation of the "Ideal" UI, jumping to error lines in the executescript plugin, cleanup of the patch review toolview, new sessions runnner, autosaving and more</li>
<!--break-->
<li>Much work in <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a>, including further NM09 support and enabling of Nm08Connection's importer</li>
<li>Initial implementation of a <a href="http://wayland.freedesktop.org/">Wayland Server</a> in <a href="http://userbase.kde.org/KWin">KWin</a></li>
<li><a href="http://amarok.kde.org/">Amarok</a> sees much bugfixing, a scripting interface added to KNotify, and drag and drop suppport between any collections</li>
<li>Bugfixing and feature work in <a href="http://skrooge.org/">Skrooge</a>, including graphs in the "Bank and Account" page</li>
<li><a href="https://projects.kde.org/projects/kdesupport/phonon/phonon-vlc">Phonon-VLC</a> adds .s3m and .xm as supported mimetypes</li>
<li>Support for RAW images in HTML export in <a href="http://www.kipi-plugins.org/">Kipi-Plugins</a></li>
<li>Preview support for ICO files in <a href="http://dolphin.kde.org/">Dolphin</a>, along with bugfixing</li>
<li>Font DPI now fully configurable by the user</li>
<li>Work on Plasma's <a href="http://ivan.fomentgroup.org/blog/2011/06/06/contouring-the-share-like-connect/">Share-Like-Connect</a> project</li>
<li><a href="http://owncloud.org/">OwnCloud</a> sees work on a media player and on the initial setup of a sharing app</li>
<li>Bugfixing throughout <a href="http://gwenview.sourceforge.net/">Gwenview</a>, <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/">KDE-Libs</a>, <a href="http://www.kde.org/workspaces/">KDE-Workspaces</a>, and <a href="http://userbase.kde.org/Juk">Juk</a></li>
</ul>

<a href="http://commit-digest.org/issues/2011-06-12/">Read the rest of the Digest here</a>.