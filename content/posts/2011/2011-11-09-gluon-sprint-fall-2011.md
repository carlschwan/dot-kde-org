---
title: "Gluon Sprint - Fall 2011"
date:    2011-11-09
authors:
  - "leinir"
slug:    gluon-sprint-fall-2011
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/group-photo.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/group-photo350W.jpg" /></a><br />From the left, top: Hanna, Arjen, Ralf, <br />
bottom: Laszlo, Stuart and Leinir (click to biggen)</div>

Over the last couple of years it has become almost a tradition to have a Gluon sprint prior to Qt Developer Days in Munich. This year was not different. Through much effort, a sprint was pulled together for the Gluon team, in part commemorating the first of these sprints two years ago, where <a href="http://gluon.gamingfreedom.org/about-gluon/gluon-vision">the Gluon Vision</a> was first laid down, and in part to assist in the work towards the next release.
<!--break-->
<h2>The Tasks</h2>

Prior to the sprint, we brainstormed discussion topics for the event, and decided that there was plenty to hack on and talk about.

One of the tasks discussed was the potential for taking part in Google Code-In with the rest of KDE. Last year we had great success with this, so we decided to throw our lot in and came up with a number of possible tasks, spanning a range of topics.

We talked about the Gluon Player organization, which is too much like an experiment. Now that we are reaching a serious state, we considered seriously how to organize this collection of applications. A consensus was reached to retire some of the experiments and to restructure others. A transcript of this discussion can be found on the <a href="http://community.kde.org/Gluon/Player">Gluon Player wiki page</a>. The core result is that there will be one player installed on any platform called Gluon Player in the .desktop file.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/gluon-is-srsbsns.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-is-srsbsns350W.jpg" /></a><br />Gluon is serious business</div>
We also discussed the potential for constructing a generic settings system for games, which would allow for cross platform friendly settings dialogues and the like, and the possibility of moving Gluon from the playground section to extragear (the sysadmin team willing, of course).

<h2>Achievements</h2>
Each of the sprint participants did a good deal of hacking during the event. Some work was begun, some completed and some is ongoing:

The GluonSmarts game AI system (a Google Summer of Code project by Pranav Ravichandran) was merged in. So Gluon now has a powerful game AI system available directly from the Gluon Creator game construction tool.

Laszlo Papp researched the potential of integrating Gluon Player with Telepathy, which is important on the Harmattan platform with integration directly into the accounts system. He also began work on the restructuring of the players that was discussed during the sprint.

Ahiemstra helped push through the many topics for discussion during this sprint—picking up the markers and scribbling away. He spent time with Stuart on Windows work, and doing some code of his own—in particular preparing a long-lived feature branch for import.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/gluon-is-derp.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-is-derp350W.jpg" /></a><br />Gluon is derp</div>

Stuart reported that during the sprint he "had an opportunity to run through the project set up process from scratch, using the Qt Creator IDE and Minigw compiler on Windows. While emerge is popular, it is not essential, and I did not have the time to build qt and kde libs." During this work, he fixed a number of CMake related issues in Gluon's build system, and encountered an unusual signalling issue on the Windows platform.

Stuart again, "Taking advantage of the Qt Experts at Dev Days, we discovered that we had stumped them too—the right objects existed, the signal was being triggered, and the destination object was not being destroyed. I believe this is responsible for some of the rendering issues we witnessed on Windows where the rendered display was not being positioned correctly in the window." Work on this is continuing, but it would have been much more difficult to diagnose had we not been able to pull in the experts.

Hanna worked on the new QtOpenAL library, which is a splitting out of GluonAudio into two parts, so the code can be shared between more projects more easily. She created QALBuffer, QALBufferFormat and started on QALSources shown in <a href="http://dot.kde.org/sites/dot.kde.org/files/gluon_audio_design_from_munich.png">the diagram made during the discussion</a>.

<h2>Socializing</h2>

Traditionally sprinters hang out socially. We spent Sunday night in a pleasant and reasonably priced Italian restaurant and sports bar. Due to the immense tastiness, we went back twice (Friday , and Sunday when those attending Qt Developer Days began arriving).

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/gluon-is-socialising.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-is-socialising350W.jpg" /></a><br />Gluon is socializing</div>

On Saturday night, we had a visit from a good friend, Sulamita Garcia, an Intel engineer supporting app developers. She is one of the people who ensured that Arjen and Leinir were able to attend Game Developers Conference (GDC) Europe earlier this year. This was an incredible event where we found out that there is room for Gluon in free software, and in the wider game creation world too. Sulamita brought along a bunch of friends, pizzas and beer. We had a great evening at Nokia's office with much chatting. Sulamita talked about the potential of employing Gluon concepts more broadly.

Specifically this means that it may be possible to use Gluon Player as a game center style system on various platforms. Gluon Player is not conceptually bound to GluonEngine. So anybody who creates games using the open standards in the Gluon distribution system could use Gluon Player to distribute their games. This will require work on the GluonPlayer library, and is quite possible. It will allow for the same functionality found in GluonEngine-based games to be used in any other game, and make it centrally available——a free, wide spanning, social network based entirely around games. Welcome to our very own Gaming-ri-la ;)

There is also a tradition to cook at the Nokia office. Sunday is problematic in Germany because everything is closed. So we stocked up ahead of time with everything needed to construct some tasty pasta. The dish was very popular ;)

<h2>What's Next?</h2>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/a-future-gluon-micro-console-perhaps.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/a-future-gluon-micro-console-perhaps350W.jpg" /></a><br />Future Gluon microconsole</div>

The next version of Gluon is approaching rapidly, and this sprint helped us jump a great deal of the way towards it. With the team expanded by the addition of Hanna and Stuart, we have introduced fresh blood into the project—two active members with great energy to help further the ideals of Gaming Freedom in the world.

