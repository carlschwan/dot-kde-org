---
title: "Wrap Up - Desktop Summit 2011 Berlin"
date:    2011-08-26
authors:
  - "kallecarl"
slug:    wrap-desktop-summit-2011-berlin
---
A big "Thank You!" to all the <a href="https://desktopsummit.org/sponsors">sponsors</a>, Technologiestiftung Berlin (TSB; in English, Technology Foundation of Berlin), Humboldt University of Berlin, and nearly 800 attendees who gathered from around the world to make a successful <a href="https://desktopsummit.org/">Desktop Summit 2011</a>!
<!--break-->
Attendees gathered in Berlin to review progress, share ideas and work together on various free software projects relating to desktop and mobile user interfaces. While many participants were from Europe as expected, other contingents came from Brazil, India, the US and beyond. The GNOME Foundation and KDE e.V. sponsored travel and accommodation costs for 80+ attendees.

More than 50 volunteers pitched in to help the Desktop Summit run smoothly--preparing the venues and cleaning up afterwards, helping with registration, selling t-shirts, recording video streams, chairing sessions, running errands, managing networks, and more. Desktop Summit organizers--mostly volunteers--worked for nearly a year to coordinate all the details that made the event successful. The collaborative spirit of Free and Open Source technology was an essential factor in how the group worked together.

c-base, the world-renowned hackerspace, hosted the Summit's pre-registration event sponsored by Igalia, where attendees had a chance to meet face to face in a relaxed environment and become acquainted. While working on projects via email and the Internet is efficient and productive, personal contact is also important, and to that end, two evening parties organized by the Summit provided opportunities for people to spend time together without the stress of project deadlines. The Summit thanks Collabora and Intel for sponsoring these events.

Other social activities included the traditional soccer and volleyball matches, sponsored this year by SUSE. As participants mostly concerned themselves with having fun (as well as a bit of beer drinking), it was not clear which of the KDE or GNOME teams won. On Tuesday, there was a SUSE-sponsored ice cream dessert gathering, and unofficial curry cook-outs on Wednesday, Thursday and Friday, attracting about 35 people each evening with food, drinks and conversation--both work and fun.

As one of the foremost free and open source gatherings, the Desktop Summit was in the right setting in the City of Berlin. At the Summit, the City announced the winners of its open source competition “Berlin – Made to Create”, a program promoting Open Source and open standards ideas and solutions. At the same session, the GNOME and KDE communities also announced their outstanding contributors.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; "><a href="http://people.canonical.com/~jriddell/desktop-summit-akademy-guadec-group-photo-2011.html"><img src="/sites/dot.kde.org/files/DTSGroup500px_0.jpg" ></a><br />Desktop Summit 2011 Berlin (click for larger image)</div>

The Intel AppUp workshop on the first day of the BoFs (Birds of a Feather sessions) was one of the most sought after events at the Desktop Summit. The "sold-out" session explained several aspects of Intel's long term strategy for the MeeGo operating system, including a considerable push with developers to create mobile apps. After the session, each participant received a tablet PC to support their development efforts. Within a few hours, people were already creating hot new stuff.

From Tuesday to Friday, 85 BoFs and countless informal hacking sessions took place. Two hacking rooms and the hallways were full of people working on projects. BoFs ranged from small working groups to popular and multi-faceted projects to the introduction of new projects. The GObject Introspection Room shows the kinds of work undertaken at the Summit: a dedicated space with 12 to 20 people at any time, it ran the duration of the Summit, and was primarily focused on bugfixing GNOME API bindings. The KDE community also participated by working on bindings between GObject libraries and Qt/C++ and smoothing out other cross-desktop issues.

As another example, the KDE Release Team got together to talk about their strategy for Git versioning migration and the move to Frameworks 5. The BoF session was well attended, and included release team members and downstream packagers. In a short time, the team gathered feedback and came up with a plan for adding predictability to the release team's work and output, and for making the work within the team more effective and sustainable. Working remotely, this would have taken considerably longer and would not have achieved such good results.

The fifth Text Layout Summit was held concurrently with Desktop Summit 2011. At present, there are several font and text shaping technologies and no unified system library. As a result, complex text layout scripts such as Arabic or Myanmar are not well supported, and Western/European fonts often lack advanced text formatting capabilities. As FOSS applications are intended for use by all nationalities and languages, this is a serious shortcoming. Text Layout Summit 2011 made substantial progress toward a common approach, especially with Graphite, which is focused on the minority languages of the world.

The Desktop Summit is an important enabling event, making it possible for teams to learn, share and make substantial progress in their Free and Open Source projects. 

During the GNOME and KDE Annual General Meetings (AGMs), the respective projects recognized the achievements of members, made important announcements and reflected on the lessons learned over the past year.

New Executive Director Karen Sandler led the GNOME AGM, with the recent release of GNOME 3 being a central topic. Many perspectives were contributed, including design, marketing, bug fixes and quality. Numbers were presented on GNOME release parties, member registration and finances. The location of the 2012 GNOME Users And Developers European Conference (GUADEC) was revealed. With 3 impressive bids to host GUADEC, La Coruña, Spain, was chosen! The GNOME community looks forward to seeing its members next summer.

At the KDE e.V. AGM, President Cornelius Schumacher presented the work of the Board and KDE e.V. activities of the past 12 months. KDE e.V. organized or helped to organize several successful international conferences such as Akademy 2010 in Tampere, conf.kde.in in India, Camp KDE in San Francisco, and financially supported 21 contributor sprints. Cornelius Schumacher also explained the e.V.'s role in supporting and representing the KDE community in legal issues like domain handling, trademarks and similar areas. Frank Karlitschek, Treasurer, gave an overview of the financial situation of KDE e.V. and the budget for 2011. There were reports from the sysadmin, community and marketing working groups, and from the representatives of KDE e.V. to the Free Qt Foundation.

This year, two positions for the board of directors were up for election. Both candidates, Cornelius Schumacher (running for his third term) and Lydia Pintscher, now the newest member of the Board, were elected.

Speaking for both organizations, Pintscher said, "We consider Desktop Summit 2011 in Berlin to have been a huge success for the collaboration among free software desktop communities. We learned a lot during the first Desktop Summit in Gran Canaria and were able to improve on many big and small things that made a real difference for the conference. We are looking forward to seeing the results of this work and to increased future collaboration."

The location of KDE's Akademy 2012 conference is still to be decided; a <a href="http://dot.kde.org/2011/08/22/call-hosts-akademy-2012"> call for hosts</a> has been made. 

The Desktop Summit received favorable publicity from Radio Tux, which covered the Summit with their mobile studio. A successful press conference was also held, pulling together key GNOME and KDE contributors with about 15 local and international tech journalists. There has been <a href="https://desktopsummit.org/news/desktop-summit-news">other press coverage</a> as well.