---
title: "Open Collaboration Services Next Sprint"
date:    2011-05-18
authors:
  - "djszapi"
slug:    open-collaboration-services-next-sprint
---
Over the weekend of May 14th and 15th, the Open Collaboration Services Next sprint took place at the Physics Institute of the Humboldt University in Berlin. Seven experienced developers participated in this event--some of them coming directly from <a href:"http://www.linuxtag.org/2011/en.html">Linux Tag</a>--in order to meet face to face, create new bonds, and discuss current and future issues around Open Collaboration Services.
<!--break-->
<h2>Open Collaboration Services</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ocs-sprint-small.jpg" /><br />OCS Sprint participants at work</div>

Open Collaboration Services (OCS) is a common protocol for providing social networking and collaboration across different services. Just like IMAP works on all desktops and mobile devices to handle email, OCS allows different applications to talk with different services in the same way. OCS aims to bring features of the social web to mobile and desktop software.

<h2>The Sprint</h2>

The sprint started with a discussion of the topics that were to be dealt with over the following two days and a debate about the future direction of the project. Specifically, we discussed a more <a href="http://en.wikipedia.org/wiki/Representational_State_Transfer">RESTful API</a> and drafted the main elements of Open Collaboration Services 2.0, our next major version.

URL usage modifications and new arguments were presented. All usage was refactored and cleaned up. HTTP methods--such as GET/POST/PUT/DELETE--were used to make the specification and the modules more RESTful. The group also focused on developing consistent usage to replace mixed usages (plural/singular forms, different terms for the same functionality, etc.)

While the most important task was to make the specification more RESTful, there was also attention to better modularization. In the person module, a social network-based concept was established (i.e., usage for sites such as Facebook and LinkedIn); there is a clear core featureset separation for those that are widely used. An App Store-like feature set was discussed for the content module. The App Store parts of OCS are already running on maemo.org and MeeGo Community Apps services.

The annoying limitations were dropped, an important improvement. There is a limit in the config call, but otherwise there are no longer hard-coded numbers. Further recommendations can be provided for providers who need to implement special situations in this part of the specification.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ocs-sprint2-small.jpg" /><br />More OCS Sprint participants at work</div>

Regarding identification, all resources are identified by a string-type resource identifier. Contents of the string depend on the implementation. Common usage includes usernames, filenames, URIs, UUIDs. Resource identifiers must be unique for a particular resource type on a particular provider.

The sprint addressed other topics. There are obstacles to translating web applications, which are different from compiled desktop programs. Content internationalization was chosen as a web standard recommendation. Payment use cases were discussed briefly; the OCS specification does not contain anything related at this time. Third party APIs such as pay swarm can be used for these payment use cases. HTTP status codes will be used, replacing the previous custom status codes; metadata already contains the information.

It is hard to have a powerful specification without proper testing, so we discussed test frameworks. Providing the reference implementation will be the first step. It is likely that some validation will be established for this format.

A few glitches during the sprint added to the adventure. One of us got stuck in Riga, Latvia for 5 hours while traveling to the sprint; there were some difficulties getting situated at the apartment. Nonetheless, we had a pleasant time on Saturday night at the Ubuntu party hosted by C-Base and a nice breakfast at the Central Railway Station on Sunday morning.

<h2>Feedback and Further Information</h2>

We made huge steps forward during the sprint. Now we need people to review the specification and provide feedback. If you want to provide feedback, please check out the current specification or implement OCS. The <a href="http://www.freedesktop.org/wiki/Specifications/open-collaboration-services-20draft">draft version</a> is a work in progress. Your input is valuable.

We also want to hear about Plasma, Gluon, and other similar integrations; projects that may be interested in open collaboration; and implementations on various devices such Smartphones, Tablets, and Netbooks. We are also looking for new modules, use cases and further extensions of the specification. Ideas are welcome. If you are interested, please join <a href="http://lists.freedesktop.org/mailman/listinfo/ocs">our mailing list</a>.

The sprint participants thank <a href="http://ev.kde.org/">KDE e.V.</a> for making the sprint possible. Special thanks go to Claudia Rauch for her help with logistics, and to Thomas Murach and the Physics Institute of the Humboldt University for the room and hosting. We had a great time, agreed on plenty of topics and hacked on countless things, making the Open Collaboration Services Next sprint a full success.

<div align="center"><img src="http://dot.kde.org/sites/dot.kde.org/files/ocs-group750x799.jpg" /><br />OCS Sprint participants (from left to right): Dan Leinir Turthra Jensen, Thomas Murach,<br />Josef Spillner, Julian Helfferich, Frank Karlitschek, Laszlo Papp, Henri Bergius</div>