---
title: "Google Summer of Code Projects Accepted & the Return of Season of KDE"
date:    2011-04-25
authors:
  - "areichman"
slug:    google-summer-code-projects-accepted-return-season-kde
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/GSoC2011_300x200_px.jpg" /></div>
The KDE community is excited to accept 51 students into the Google Summer of Code program this year. Their projects will touch KDE on almost every level, and integrate the students into our community. Some are likely to become longtime KDE contributors. The next month will be spent on community bonding, getting to know the people and the code behind the project they'll be working on. From May 23rd until the end of August, they'll be working with their mentors to complete their own projects.
<!--break-->
Accepted proposals for 2011 are varied, including improvements in the KIO file transfer protocol, file sharing on a personal cloud server, and more. New form factors will be worked on in the Plasma Media Center project along with mobile profiles for Amarok and Marble. Students will also be adding QML Video support to Phonon, integrating Telepathy into the Plasma workspace, and further modularizing the Kwin Window Manager. Many other applications will get a helping hand this summer as well. Krita, part of the Calligra Office Suite, accepted 4 students this year; Digikam and Marble each accepted 3 students. Kate, Okular and Calligra Stage will each have 2 projects. Other accepted projects include the creation of an easily accessible, high quality KDE technology demo similar to Qt's, the ability to have a single login for any KDE site through identity.kde.org integration and the beginnings of a framework to conduct usability surveys.

For the full list of accepted projects, please see the <a href="http://www.google-melange.com/gsoc/org/google/gsoc2011/kde">GSoC website</a>.

<h2>Season of KDE</h2>

The KDE Community is grateful to Google for sponsoring the Summer of Code, now in its 7th year, and for giving both students and mentors the opportunity to improve the software we use every day. There were a limited number of spaces available again this year, so the KDE GSoC admins and mentors were not able to accept more proposals. Those students who were not accepted, and others who may not have applied, will still have the opportunity to see their projects through, as KDE will once again be running the Season of KDE, a separate program that runs in parallel with the Summer of Code. Season of KDE provides mentors to give contributors the support they need to get involved and start coding. Participants in Season of KDE will receive a T-Shirt, certificate and the appreciation of the KDE community, as well as valuable experience working with a large software project. Lydia Pintscher will be managing Season of KDE this year, with <a href="http://blog.lydiapintscher.de/2011/04/25/announcing-season-of-kde-2011/">more information available on her blog</a>.

KDE thanks Google for creating the Summer of Code and our mentors for their dedication to improving every aspect of KDE. We look forward to welcoming our Summer of Code and Season of KDE participants and thank them for their contributions to KDE.