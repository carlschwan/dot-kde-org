---
title: "Join KDE Italia at CoNAsSL 2011"
date:    2011-08-19
authors:
  - "panda84"
slug:    join-kde-italia-conassl-2011
---
<a href="http://www.kdeitalia.it/node/2134">KDE Italia</a>, the Italian community backing the <a href="http://www.kdeitalia.it/">KDE Italia website</a> and the <a href="http://forum.kde.org/viewforum.php?f=140">KDE Italia forum</a>, is pleased to invite you to <a  href="http://www.grolug.org/conassl-2011-3a-edizione-9-10-11-settembre">CoNAsSL  2011</a>, an end-of-summer event aimed at gathering all the Italian associations involved in Free Software. The event will take place on 9, 10 and 11 of September, in a beautiful location: the Tuscan seaside! People attending will be hosted at the same place where the talks and workshops will be held: the  <a href="http://www.baiadeigabbiani.com/">"Baia dei Gabbiani" campsite</a> in <a href="http://www.follonica.com/scarlino.asp">Scarlino</a>, a town near Follonica (Grosseto).

The KDE Italia community is meeting to improve cooperation between  translators, developers, users and enthusiasts; to get in touch with other associations; to learn what could be done to improve the website, the forum and increase user participation; to discuss further initiatives and not least, to get to see our faces.

If you're interested in participating in this Italian KDE event, you just need to get in touch by leaving a message in the <a href="http://forum.kde.org/viewtopic.php?f=140&t=96354">forum thread</a>, or  by sending an email to <a  href="http://mail.kde.org/mailman/listinfo/kde-italia">KDE Italia mailing list</a>. Participation is open to everybody interested in KDE.