---
title: "Call for Participation - Workshops & BoFs for the Desktop Summit 2011"
date:    2011-06-20
authors:
  - "jospoortvliet"
slug:    call-participation-workshops-bofs-desktop-summit-2011
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 266px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DS-logo.png"></a></div>

The <a href="http://desktopsummit.org">Desktop Summit 2011</a> is a joint conference organized by the GNOME and KDE communities in Berlin, Germany from the 6th August 2011 to the 12th August 2011.

The organizing team is now inviting applications to hold workshop and Birds of a Feather (BoF) sessions at the Desktop Summit. Read on for more details and how to make a proposal.
<!--break-->
<h2>Not Just Presentations</h2>
The Desktop Summit will have an exciting <a href="https://www.desktopsummit.org/program">program of talks</a>. But the most important part of the conference are the Workshops & BoFs. This is the part of the conference where the participants get together, discuss and work on the future of the Free Desktop. It is where the latest technology is demonstrated in a one-to-few setting and where decisions are made. The organization committee would like to schedule as many of these sessions beforehand as possible. We expect over 1000 visitors; scheduling helps to ensure minimal overlap with other sessions and allows us to provide a clear timetable for the visitors. The remainder of the rooms will be scheduled via the wiki but we urge you to get a proposal in before the deadline! We realize that many sessions are meant to be about current and urgent topics so we don't expect proposals to have an exact agenda, nor do we mind if the subject changes later on. 

<h2>Session requirements</h2>
All forms of <em>hands-on</em> activities that aim to further the Free Desktop are welcomed. Examples of such sessions include <a href="http://en.wikipedia.org/wiki/Birds_of_a_Feather_%28computing%29">BoF</a>, project and cross-project meetings, workshops, hacking sessions and training/teaching sessions. Each session is self-organized. It is up to the hosts and participants to decide if the session is to be loosely oriented around a set of topics, or have a well-defined agenda.

Each session is meant to be open to anyone who is interested. If you want to organize a closed session on a subject, contact the organization (details below). We encourage participants to make use of the fact that the Desktop Summit will bring together people from several different communities, and the unique opportunities this creates.


<h2>Reserving a spot on the Workshop & BoF Days</h2>
To register a session, use the <a href="http://desktopsummit.org/program/workshops-bofs">form found on the the Workshops & BoFs webpage</a> on the Desktop Summit website. This requires logging in to the Desktop Summit wiki (using the same login details as on the website). Please check the <a href="http://wiki.desktopsummit.org/index.php?title=Category:DS2011WorkshopProposal">sessions that have already been proposed</a> before creating a new one.

You will need to provide the following:
<ul><li>Title of session</li>
<li>Session hosts (names, contact info)[1]</li>
<li>Short description of the session. Between 150 and 400 words please.</li>
<li>Format of the session[2]</li>
<li>Expected number of attendees</li>
<li>Numbers of hours you think you need</li>
<li>Special requirements/requests (equipment, room and scheduling)</li></ul>

[1] All sessions require at least two hosts to sign up to take care of them.

[2] Format can be any of the following: BoF, team meeting, hacking session, workshop, training/teaching session.

The deadline for pre-registered sessions is July 3rd. Sessions registered before this time will be scheduled by the organization team between July 3rd and July 10th.

For sessions registered after this date, attendees themselves are responsible for finding a time and location for the session. Rooms will be available for this for the duration of the Workshop & BoF days, and the wiki can be used to coordinate.

Please note that the schedule for talks has already been published and these sessions are meant to be hands-on, not "read-only"! In case of questions regarding the conference, please consult the Desktop Summit web site or contact the Desktop Summit organising team <a href="mailto:ds-team@desktopsummit.org">by email</a>.

The registration of sessions has now opened, and pre-registration is possible until July 3rd. <strong>Register your session now by going to the webpage for <a href="https://desktopsummit.org/program/workshops-bofs">Workshops & BoFs.</strong>