---
title: "KDE Commit-Digest for 29th May 2011"
date:    2011-06-03
authors:
  - "vladislavb"
slug:    kde-commit-digest-29th-may-2011
comments:
  - subject: "LongBug champion!"
    date: 2011-06-03
    body: "This week's champion is Dawit Alemayehu for fixing <a href=\"http://bugs.kde.org/show_bug.cgi?id=94867\">bug 94867</a> which is more than 6 years old!\r\n\r\n\"No warning for fake links using username and password field of URL\""
    author: "emilsedgh"
---
In <a href="http://commit-digest.org/issues/2011-05-29/">this week's KDE Commit-Digest</a>:

              <ul><li><a href="http://www.calligra-suite.org/">Calligra</a> sees improved support for many file formats, an auto-updating Table Of Contents, configurable full-screen mode, column breaks, working even/odd headers/footers, and much bugfixing throughout</li>
<li><a href="http://krita.org/">Krita, in particular, now has a GHNS tool bar to gradients, patterns and brushes, eci profiles, and many bugfixes</li>
<li>Secret object for VPN connections in <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>, MAC spoofing for wireless, and further work on NM09 support</li>
<!--break-->
<li>Work on Presence applet for <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy</a></li>
<li>Added a security warning for URLs with bogus username</li>
<li>Memory optimizations in <a href="http://nepomuk.kde.org/">Nepomuk</a> and <a href="http://api.kde.org/4.0-api/kdelibs-apidocs/">KDE-Libs</a> amongst bugfixing</li>
<li>Reduced memory usage in <a href="http://edu.kde.org/marble/">Marble</a> along with bugfixing</li>
<li>Improve randomized slideshow in <a href="http://gwenview.sourceforge.net/">Gwenview</a></li>
<li><a href="http://userbase.kde.org/Konsole">Konsole</a> Part no longer blocks the unmounting of removable media</li>
<li><a href="http://uml.sourceforge.net/">Umbrello</a> sees GSOC work on the port to QGraphicsView</li>
<li><a href="http://userbase.kde.org/Dolphin">Dolphin</a> sees GSOC work with an implementation of a basic commit dialog interface</li>
<li><a href="http://kst-plot.kde.org">Kst</a> now supports Scientific Notation for log mode</li>
<li>Editing HTML messages that are encrypted is now possible in <a href="http://pim.kde.org">KDE-PIM</a>, Kontact now includes a Zanshin plugin, and much bugfixing</li>
<li><a href="http://amarok.kde.org/">Amarok</a> sees an expansion of the MusicBrainz dialog, Last.fm scrobbling using composer, and bugfixing</li>
<li>Work on <a href="http://www.oxygen-icons.org/">Oxygen-Icons</a></li>
<li>Bug fixing in <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://plasma.kde.org/">Plasma</a>, <a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a>, <a href="http://kde-look.org/content/show.php/?content=136216">Oxygen-Gtk</a> and <a href="http://kde-apps.org/content/show.php/?content=137507">Muon</a>.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-05-29/">Read the rest of the Digest here</a>.