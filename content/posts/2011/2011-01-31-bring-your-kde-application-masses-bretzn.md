---
title: "Bring Your KDE Application to the Masses with Bretzn"
date:    2011-01-31
authors:
  - "leinir"
slug:    bring-your-kde-application-masses-bretzn
comments:
  - subject: "Sexy!"
    date: 2011-02-02
    body: "Man, this is looking beautiful. I really hope that it *is* as easy to use as the video makes it seem. If so, we're really in for a wild ride as more and more apps become available faster, across more distro's getting more users testing and filing bugs on the latest versions and picking up more fans.\r\n\r\nHeck, even if it does nothing but make the magnificent OBS easier / more intuitive to use it could be a revolution. My only questions:\r\n\r\n1. There's a spot for RPM spec files, what about deb spec files?\r\n\r\n2. I'm guessing the \"Launh build\" button is missing a letter \"c\" ;)\r\n\r\n3. That demo image, omg... is that... a *video* on the app page! wooooooot! \\(^O^)/\r\n\r\nGreat work, all involved. This looks like it's going to be one rocking project that will benefit us all. Can't wait to see how Gluon blends into this new app-distributing world when it gets up to speed!"
    author: "Bugsbane"
  - subject: "wooaaah! definitely going to"
    date: 2011-02-02
    body: "wooaaah! definitely going to try this out :)\r\n\r\nthanks for making the world a better place for small developers ;)"
    author: "mxttie"
  - subject: "Cross-platform"
    date: 2011-02-02
    body: "What I really hope to see is the KDE App Store being cross-platform, i.e. the same gui and infrastructure used to distribute our Windows and Mac apps.  Behind the scenes it can be using whatever technical system is required to install the required libs and dependencies, but we need to make the user installation experience on both those platforms this easy.\r\n\r\nThe Windows installer does a miraculous job, but it's just too techie for the average consumer, but with the KDE App Store as the front end, well, World Domination springs to mind :-)"
    author: "odysseus"
  - subject: "It seems great"
    date: 2011-02-03
    body: "Yeah, it seems so great. But as my English is not so well. I may not understand it clearly. Also I could not see the video.\r\nIt seems that users can just download and install KDE apps via this client , and they can make comments , feedback or something else. But there is already a package manager, will they conflict.\r\nAnd this client seems a part of the social desktop. Anyway , it is a great job."
    author: "SamiZhan"
  - subject: "Love it!"
    date: 2011-02-04
    body: "I think the work you are doing is amazing! I will have to try it out!\r\n\r\nOne question though, would it be possible to use this kind of infrastructure on a local Intranet? Install all the necessary services on local servers and preform internal deployment etc. \r\n\r\nI'm thinking from the commercial point of view, if we could market this as being an easy way for people to deploy their applications we might see an uptake in businesses writing cross-platform applications using Qt/KDE. One could even licence build service servers for non open source use which would fund the open source development. \r\n\r\nJust a though. "
    author: "real_ate"
  - subject: "Package manager"
    date: 2011-02-04
    body: "So, this is a publishing mechanism?\r\n\r\nThe end-user installation process would still be through their standard installation manager (e.g. a package manager on Linux), right?"
    author: "cdonline"
  - subject: "deb"
    date: 2011-02-05
    body: "Deb is probably coming soon, it's supported on OBS... Windows might come some day too :D"
    author: "jospoortvliet"
  - subject: "..."
    date: 2011-02-05
    body: "It's not KDE specific - it's a cross-desktop, cross-platform, cross-IDE initiative... And it'll integrate with the native package management as much as possible. Depending on the exact implementation it'll probably just add a repository for your distro in the background."
    author: "jospoortvliet"
  - subject: "This whole thing is part of a"
    date: 2011-02-05
    body: "This whole thing is part of a larger cross-distro application installation discussion: http://news.opensuse.org/2011/01/26/app-installer-meeting-or-more-collaboration-accross-borders/\r\n\r\nIt all builds on FOSS and open spec systems so having it in-house should work. Also commercial apps would work too..."
    author: "jospoortvliet"
  - subject: "Basically distro's want to"
    date: 2011-02-05
    body: "Basically distro's want to use this as their new app installation tech. For KDE, Bretzn writes an app - for GNOME something is in the works too, see http://news.opensuse.org/2011/01/26/app-installer-meeting-or-more-collaboration-accross-borders/\r\n\r\n;-)"
    author: "jospoortvliet"
  - subject: "KDevelop"
    date: 2011-02-06
    body: "Hi! The plugin-code (http://qt.gitorious.org/+obs-creator/qt-creator/buildserviceplugin/trees/buildservice/src/plugins/buildservice) seems not to contain a lot of QtCreator-specific stuff, the plugin-class seems to be the onliest one using QtCreator-includes and it just modifies the GUI. Is that correct? So it should be very simple to port it to KDevelop? Btw: Why is there a QtCreator-fork for this plugin? Couldn't there simply be a single repository at git.kde.org containing only this plugin?"
    author: "The User"
  - subject: "correct ..."
    date: 2011-02-09
    body: "Yes, the plugin part is rather small, and well-encapsulated. The UI logic itself uses libattica and the buildservices project management. I haven't looked into integrating it into KDevelop, but it should indeed be very easy to adapt to other IDEs. That's not entirely coincidential. :)\r\n\r\nThe reason to keep this code in a Gitorious git repo is that the headers for building the actual plugin are not installed to the system (I understand that the Qt Creator devs want to retain some flexibility with respect to source and binary compatibility. There are also dependency between plugins, which makes things slightly more complicated. (I've kept those to a minimum in the Buildservice plugin.) So for now, you have to build it inside the Qt Creator repo."
    author: "sebas"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/bretzn-shots3.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/bretzn-shots3-sml.png" /></a><br />Bringing things together...</div>Apart from being a tasty Bavarian bread-snack, <i>Bretzn</i> is the code-name for a collection of technology aimed at solving a problem which has existed in software development for a very long time: How do you get your applications to your users?

This is particularly a question for the many developers of KDE applications that are not part of KDE's six monthly Software Compilation releases. These developers must either provide binaries for a range of distributions themselves or hope that distribution volunteers will do the packaging for them.
<!--break-->
However, there are already a number of services that can help to solve these problems. We have build services (such as the <a href="http://build.opensuse.org">openSUSE Build Service</a>) that allow the easy creation of binaries for many different Linux distributions (with other platforms in the work!), we have Open Collaboration Services (OCS) which allows easy, socially networked publishing of packages both to and from central software download sites such as openDesktop.org and KDE-Apps.org and we have powerful integrated development environments (IDEs) such as Qt Creator, KDevelop and Eclipse. 

Until now these technologies and tools made up mostly isolated islands and it seemed an obvious choice to try bridging them. A project aiming to do just this began in August 2010 and was first <a href="http://dot.kde.org/2010/11/02/frank-karlitschek-introduces-bretzn">publicly announced</a> by Frank Karlitschek at the openSuse Conference in Nuremberg in late October: Project Bretzn would make it possible, with a few clicks, to publish software projects directly from the IDE - and it would all happen before the end of the year!

<h2>Connecting the Dots</h2>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/appstore_details.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/appstore_details_wee.png" /></a><br />The KDE Application Installer</div>
Project Bretzn, then, is not a single piece of software, but rather an attempt to fill in the holes which exist in what is already there. As it stands, the project has produced two core pieces of software:

<ul><li>A thin client in the shape of a Qt Creator plugin, accessed through the Tools menu in the IDE. The plugin lets you perform all the actions required to get data sent to the various build services and publishing sites, by contacting the server part, which then distributes the information to the appropriate places. The implementation of this also prompted amending the <a href="http://api.kde.org/kdesupport-api/kdesupport-apidocs/attica-git/html/">Attica library</a> with new features. As some will already know, Attica is the full featured implementation of a OCS client library built by KDE which is now officially included in the MeeGo platform as well. The Qt Creator plugin is build around a simple library to make it easily portable to other IDE's like KDevelop - consider this an invite!</li>
<li>A server library, designed to plug into the OCS reference server implementation as published by the <a href="http://socialdesktop.org/bretzn">Social Desktop project</a>. This is the part of the system which draws the lines between the dots: It contacts any number of build services that you request your software to be built on and when you request it, it publishes the packages resulting of those build jobs on the distribution sites. The publishing system requires only of the remote sites that they implement the content module part of OCS, something that many already do.</li></ul>

Moreover, a KDE client to download and install applications is being developed (find info <a href="http://news.opensuse.org/2011/01/27/first-bretzn-sprint-opensuse-app-store-on-the-horizon/">here</a>). The screenshot shows a first prototype!

Most importantly, all this has the distinction of being open: not only is the source code for the software above freely available as you would expect, but the web API created as the communications layer between those two components is free and open, and indeed a part of the Open Collaboration Services specification as of version 1.6.

<h2>Publishing Renewed</h2>

The best software is that which gets out of your way to let you do your work, and Bretzn was designed with this principle in mind. What this means is that when you are ready to publish your software, you call up the tool and enter the required information only once: if the same information is required for multiple publishing sites, it is only entered once. The source archive is created for you when you select which folder contains your source code, and you only need to select the targets you wish to build for to get binaries for your application.

Even with the build services, building the binary packages does take a while. Therefore, Bretzn was designed to not require you to follow this process in its entirety. It is rather conceived as a system in which you create the build jobs and then simply close the plugin and let the build service work while you continue working yourself.

When publishing the software, you will normally have to give notice to many people and organizations if you wish the software to be publicized. Through Bretzn, this information can be pushed to people as the publishing happens. Information can be shared through the social networking features of the Open Collaboration Services on the sites the application is published to. For example, users may be subscribed to updates about a single application, or to activities performed by a friend, which are for example the publishing of applications.

<object width="640" height="385"><param name="movie" value="http://www.youtube-nocookie.com/v/sqe1pEv95yk?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube-nocookie.com/v/sqe1pEv95yk?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="640" height="385"></embed></object>

<h2>Who Is Working on Bretzn?</h2>

The project is a collaboration between <a href="http://hive01.com/">h i v e 01</a>, <a href="http://www.open-slx.de/">Open-SLX</a> and <a href="http://nokia.com">Nokia</a>, and specifically it has been the task of Sebastian Kügler, Dan Leinir Turthra Jensen and Frank Karlitschek to get the project working. Over the last four months, they have been working tirelessly to construct the bridges which make up the Bretzn project, both in software by writing the code, and socially by speaking with a lot of people about the goals of the project to find out just what is needed, as well as making those whose systems Bretzn bridges aware of what they have been doing.

<h2>And, It Is Available Right Now</h2>

Though the majority of the code has been developed in the open, the various bits of code have now been officially released:

<ul>
<li>The new version of <a href="http://api.kde.org/kdesupport-api/kdesupport-apidocs/attica-git/html/">LibAttica</a> required for the tools was released</li>
<li>The <a href="http://qt.gitorious.org/+obs-creator/qt-creator/buildserviceplugin">Qt Creator plugin</a> has been released</li>
<li>The OCS library extension has been released and <a href="http://SocialDesktop.org/bretzn">is available</a></li> 
</ul>

We invite you, as developers of KDE software, to use the results of this project and bring your software to the world using Bretzn. Presently, the Bretzn plugin is only available for Qt Creator, but since all of this software is released under free licenses the team hopes to see Bretzn integrated into other IDEs, particularly KDevelop. Furthermore, help is welcomed in improving the core software as well as providing plugins to support other buildservices or work with more social networks like identi.ca and facebook.

<h2>Further Information</h2>

You can find more information about Bretzn and download it <a href="http://socialDesktop.org/bretzn">at the Social Desktop website</a>.