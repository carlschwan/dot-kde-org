---
title: "KDE Ships September Updates"
date:    2011-09-07
authors:
  - "sebas"
slug:    kde-ships-september-updates
comments:
  - subject: "A lot of kmail crashes fixed"
    date: 2011-09-07
    body: "A lot of kmail crashes fixed in this release. Thanks ! Hopefully we got back to kmail1's stability level."
    author: ""
  - subject: "KDE 4.7.1 with Enhanced Nepomuk\u2122 released."
    date: 2011-09-07
    body: "All of you, unbelievers who yelled at the sight of Nepomuk. All of you, who whined against tremendous I/O load, massive CPU usage, or indexing slowness, rejoice.\r\n\r\nKDE 4.7.1 is not a normal point release; there were massive backports stabilizing and speeding up Nepomuk. So, it won't reindex as much as before, it will index faster, and the index itself will be smaller. This is great, and deserves more coverage."
    author: "Alejandro Nova"
  - subject: "Yes but"
    date: 2011-09-08
    body: "can't get rid of this bug:\r\nhttps://bugs.kde.org/show_bug.cgi?id=276893\r\n\r\nA really annoying bug, i think this is a bug due to imap server, as some see it and others not :-/"
    author: ""
  - subject: "Is that real? If so, I we"
    date: 2011-09-08
    body: "Is that real? If so, I we need more coverage."
    author: "zayed"
  - subject: "I had Nepomuk and Strigi"
    date: 2011-09-08
    body: "I had Nepomuk and Strigi disabled. Based on that original comment I decided to re-enable it after 4.7.1 upgrade.\r\n\r\n10 minutes later nepomuk indexer crashed. For me, business as usual."
    author: "Florian"
  - subject: "Reenable it. It won't crash"
    date: 2011-09-08
    body: "Reenable it. It won't crash again and it will complete indexing. Nepomuk Indexer also crashed here with 4.7.1, but it crashed only once. The crash may be because of a lack of thread safety (one of the new backported features is that Nepomuk now runs lots of nepomukindexer threads concurrently)"
    author: "Alejandro Nova"
  - subject: "Not true here. I've rebooted"
    date: 2011-09-09
    body: "Not true here. I've rebooted a couple of times since upgrading and the nepomuk indexer crashed every time right after login. It seems to recover, continues indexing and does not crash again.\r\n\r\nFurthermore search results are incorrect. It seems to find every file that has a \u00e4\u00fc\u00f6\u00df included on every query: http://forum.kde.org/viewtopic.php?f=66&t=96151"
    author: "Florian"
  - subject: "KDE should make Nepomuk and"
    date: 2011-09-09
    body: "KDE should make Nepomuk and Akonadi completely optional for those who don't want them. Why do I have to have them installed on disk in the first place, if I don't use them? Why do I get a warning every time I log in telling me that indexing is disabled and that I'm missing wonderful features, when I disabled them fully aware of the things I'll miss? Why is the nepomukserver process started even though I disabled it in the system settings?\r\n\r\nI have no problem with extending KDE, but please, make all the bloat optional (and with optional I mean uninstallable) so that people can have a streamlined version of our beloved DE. \r\n\r\n\r\n\r\n\r\n\r\n"
    author: "trixl."
  - subject: "Disable akonadi"
    date: 2011-09-09
    body: "http://techbase.kde.org/Projects/PIM/Akonadi#How_do_I_completely_disable_Akonadi_startup.3F\r\n\r\nAnd then disable nepomuk, that's what i do on my laptop..."
    author: ""
  - subject: "Well not all. Still have"
    date: 2011-09-09
    body: "Well not all. Still have plasma crashes. To bad really with all the cool plasmoids I would use them but what's the point if plasma keeps crashing."
    author: "stumbles"
  - subject: "Nice link. But umm that last"
    date: 2011-09-09
    body: "Nice link. But umm that last section about it hanging/stops working? Do your really expect the user to fiddle around at that level to have something that doesn't crash? Come on, we're at 4.7.1 for Pete's sake.\r\n\r\nKDE has been my only desktop at home since 4.0.x. But really guys, there are so many bug reports about nepomuk/akonadi/strigi/plasma crashing that I wonder if anyone really cares about that and would rather focus on the more \"gee whiz\" stuff.\r\n"
    author: "stumbles"
  - subject: "\"you, who whined against tremendous I/O load, massive CPU\""
    date: 2011-09-09
    body: "I'm giving it a try. Looked good at the beginning, but after hour or two... http://img197.imageshack.us/img197/3553/nepomuk.png"
    author: "gedgon"
  - subject: "wonder no more"
    date: 2011-09-10
    body: "At least one person cares...apparently...you.\r\n\r\nIf this is really important to you, you could help by doing bug triage, finding duplicates and otherwise taking some of the load from the people who actually fix stuff. More information here...http://community.kde.org/Getinvolved\r\n\r\nBut no need to bicker, let's look at facts...\r\nOut of the top 200 bugs for each of the applications you cited, the following are the number resolved:\r\nnepomuk 125\r\nakonadi 118\r\nstrigi 143\r\nplasma 144\r\n\r\nClearly someone is resolving bugs. \r\n\r\nI wonder if you realize how your sort of comment affects the people who are working on your behalf."
    author: "kallecarl"
  - subject: "Well you know I have in the"
    date: 2011-09-10
    body: "Well you know I have in the past helped under several monikers to some degree; albeit in my limited ways. \r\n\r\nThat's a far amount of bug fixes but seems none of those have fixed that to which I referred and others are still experiencing. Its good to see progress but really; it ain't there yet.\r\n\r\n"
    author: "stumbles"
  - subject: "... and beside performance"
    date: 2011-09-11
    body: "Progress here, regression there... http://www.youtube.com/watch?v=UxG8tLPXR7w Overall still far from production quality. So, Alejandro, too early to rejoice."
    author: "gedgon"
  - subject: "an explanation would be helpful"
    date: 2011-09-11
    body: "Would you please explain what is happening in your video? What are you trying to do? What's happening? What is and isn't working?\r\n\r\nThank you"
    author: "kallecarl"
  - subject: "Long story short..."
    date: 2011-09-11
    body: "When file name contains some(?) non standard latin signs, always will be shown in search results."
    author: "gedgon"
  - subject: "Well, it should affect their"
    date: 2011-09-15
    body: "Well, it should affect their confidence on the decision to ship nepomuk enabled by default. That's how it should affect them. "
    author: "jadrian"
  - subject: "Virtuoso bug located and destroyed."
    date: 2011-09-22
    body: "1. Please, check your strigi-libs release. Basically anything based on Debian until yesterday has strigi-libs 0.7.2, an unsupported release. Current is 0.7.6.\r\n2. The problem with accents is located in Virtuoso and it was patched. The patch landed in Fedora and it is expected to land in your distro sooner rather than later."
    author: "Alejandro Nova"
  - subject: "Encoding patch."
    date: 2011-09-22
    body: "https://bugs.kde.org/show_bug.cgi?id=271664\r\n\r\nAsk your distro to include this patch. It should really help with accents and umlauts."
    author: "Alejandro Nova"
  - subject: "thanks"
    date: 2011-09-22
    body: "already reported https://bugs.archlinux.org/task/23864"
    author: "gedgon"
---
Today KDE <a href="http://kde.org/announcements/announce-4.7.1.php">released</a> updates for its Workspaces, Applications, and Development Platform.
These updates are the first in a series of monthly stabilization updates to the 4.7 series. 4.7.1 updates bring many bugfixes and translation updates on top of the latest edition in the 4.7 series and are recommended updates for everyone running 4.7.0 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.7.1.php">4.7.1 Info Page</a>. The <a href="http://www.kde.org/announcements/changelogs/changelog4_7_0to4_7_1.php">changelog</a> and <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.7.1&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a> list more, but not all improvements since 4.7.0.
Note that these changelogs are incomplete. For a complete list of changes that went into 4.7.1, you can browse the Subversion and Git logs. 4.7.1 also ships a more complete set of translations for many of the 55+ supported languages.
To find out more about the KDE Workspace and Applications 4.7, please refer to the 4.7.0 release notes and its earlier versions.
<!--break-->
