---
title: "Release 4.7 - New Features, Improved Stability and Performance"
date:    2011-07-27
authors:
  - "sebas"
slug:    release-47-new-features-improved-stability-and-performance
comments:
  - subject: "Announcement glitch"
    date: 2011-07-27
    body: "The heading of the announcement is incorrect. Otherwise, kudos to another amazing release! :)"
    author: ""
  - subject: "Mind sharing what's"
    date: 2011-07-27
    body: "Mind sharing what's incorrect?"
    author: "sebas"
  - subject: "Not sure what the TS meant,"
    date: 2011-07-27
    body: "Not sure what the TS meant, but I did notice a glitch in the Dolphin-Gwenview screenshot (the titlebar). That's worth fixing :)"
    author: "vdboor"
  - subject: "Looks like a great release!"
    date: 2011-07-27
    body: "This looks like a great release! Will install it on one of my computers soon, love to try the new stuff and, and see the improvements."
    author: "vdboor"
  - subject: "The applications page, the"
    date: 2011-07-27
    body: "The applications page, the first image, Gwenview's title bar looks corrupted."
    author: ""
  - subject: "fixed."
    date: 2011-07-27
    body: "That one has been fixed already. :)"
    author: "sebas"
  - subject: "Yay!"
    date: 2011-07-27
    body: "Well done KDE team. I have been running svn (sorry, git, old habits...) snapshots through this dev period, and this is a great release.\r\n\r\nI guess the next one will have a semi-sentient nepomuk at the rate you are going :)"
    author: ""
  - subject: "Congratulations and thanks"
    date: 2011-07-28
    body: "Congratulations and thanks for another great release! :)"
    author: "jankusanagi"
  - subject: "First impressions, and an alert."
    date: 2011-07-28
    body: "1. KWin performance is GREAT. It's brutally kicking ass. Believe it or not, Martin Gr\u00e4\u00dflin managed to squeeze in more performance between RC2 and this. My poor 6150 has blur, and has it while running great. Blurring the Dashboard still doesn't work, but that is a minor regression compared to a (finally) working blur.\r\n\r\nAlso, my desktop PC (8500 GT) has now Lanczos filtering. Same story: effects that bogged down my system with KDE 4.5 or 4.6 now run GREAT. Infinite kudos for KWin team and their OpenGL 2 backend, with graceful fallbacks.\r\n\r\n2. DO NOT ENABLE STRIGI INDEXING, LIMIT YOUR NEPOMUK INDEXING, I'M SERIOUS. Between 4.7 RC2 and 4.7.0, Strigi and Nepomuk indexing code was heavily refactored. The new code lacks any form of testing, and my preliminary tests show a massive memory leak in nepomukstorage service. This, and some regressions in Strigi, can OOM any system with a significant number of files (specially PDFs and some MP3, varies) This was present before, but is somewhat magnified by the fact Strigi doesn't remember what was indexed and what wasn't, so, it simply reindexes all. BTW, I've already voted in the relevant bug report and reported the leak as another bug (and, if Sebastian or Vishesh are here, please give me instructions of how to test Nepomuk for memory leaks, I'm willing to test)\r\n\r\n3. The rest of the changes are minor. However, those of you with constantly changing monitor setups (and me with my swiveling TX1000 ;)) will notice a sharp decrease in Plasma bugs. Now Plasma behaves more correctly than never before.\r\n\r\n4. The new featurettes of Plasma Addons are nice. KDE Microblog can do full retweets and mark tweets as favourite, and Now Playing now supports Bangarang.\r\n\r\n\r\nI really hope the Nepomuk crew is listening, because this is serious. The new framework is technically much better, but buggier, and I'm afraid that we'll have to endure a buggy Nepomuk during the entire 4.7 cycle. I expect some bugfixing sessions from your part, and I'm willing to help, testing (I don't know how to code...)\r\n\r\nIf you disable Strigi indexing and keep Nepomuk usage to minimums, then this is a great release, and stability is better than never before."
    author: ""
  - subject: "Gnome 3 like behavior"
    date: 2011-07-28
    body: "Is it possible to have gnome 3 like behavior with kde 4.7?\r\n\r\n1. One task Taskbar (without text)\r\n2. Present window allows accessing \"Side Panel\" shortcuts\r\n3. Shortcuts on desktop (like search and launch interface) accessible in Present Window.\r\n\r\nI like the way Gnome3's menu is transformed into a nice full desktop based menu not just a box (kmenu) on the desktop. without any \"Sub directories\""
    author: ""
  - subject: "Uh?"
    date: 2011-07-28
    body: "> Between 4.7 RC2 and 4.7.0, Strigi and Nepomuk indexing code was heavily refactored. The new code lacks any form of testing, and my preliminary tests show a massive memory leak in nepomukstorage service.\r\n\r\nCould someone else confirm?\r\nI think that Strigi and Nepomuk indexing is enabled by default, no?\r\nIf yes, then this is really shocking: I think that we can expect a lot of negative reviews of KDE; again..\r\n"
    author: "renox"
  - subject: "strange design decision for Oxygen, other comments"
    date: 2011-07-28
    body: "Thank you for all the hard work developers. I have a few comments:\r\n\r\nIs it just me, or am I the only one who thinks the new folder icon isn't better than the previous one? In itself the new one isn't bad, it's the strange dark blue line which is so odd. Also the new icon seems to be mixed with the old icon, in the screenshot here the 'Documents' directory still uses the old icon which makes the iconset inconsistent. Maybe this is a trivial issue and you can't argue about taste, but why was design decision made I wonder?\r\n\r\nWhat I really like about this release (based on what I see from the screenshots) is the new user interface for Dolphin. Just like with web browsers, I barely ever use the menu in a file manager, so it's nice to have it hidden under a button so it doesn't eat up vertical space.\r\n\r\nThe announcement mentions that 'KwebkitPart, integrating WebKit into KDE applications, has improved ad-blocking support, making browsing with a KDE WebKit browser a more pleasant experience'. But I wonder if anything else has changed to improve web browsing? Asking the question differently, I still need to fall back to Chromium because rekonq doesn't work well with some websites. Are problems when web browsing with rekonq caused by rekonq itself, KWebKitPart or QtWebKit?\r\n\r\nI'm grateful for the new Akonadi-based KDE PIM, but unfortunately I still experience many problems \u2013 http://bit.ly/olg5mQ \u2013 when I use it. What worrying me even more is that I've been testing the new KMail for months before it was released and that I reported many bugs, of which the majority are still waiting to be investigated. I don't blame the developers because their time and the amount of developers working on KDE PIM are in short supply, But still, I think Evolution was more pleasant to use when I was still using GNOME. KMail 2 will probably improve in the future though, now that the developers can focus on polish since the port to Akonadi has been completed."
    author: "vanloon"
  - subject: "We're listening"
    date: 2011-07-28
    body: "We've honestly tried to fix most of the issues. The massive rewrite done after RC2 was done to address the memory leaks.\r\n\r\nThere are going to be some files which will not get indexed, as with 4.7 we do not accept incorrect data from Strigi. The solution is that the Strigi indexers need to be fixed. Trueg has fixed a number of bugs in the indexers. We can only hope that the distros upgrade Strigi regularly.\r\n\r\nI forgot to backport the bug where Nepomuk does not remember what it indexed. Fortunately someone pointed it out and I management to contact the release team in time. The fix is present in 4.7.\r\n\r\nIf you're still encountering problems please drop by at #nepomuk-kde."
    author: ""
  - subject: "I'm still on KDE 3.5.12"
    date: 2011-07-28
    body: "I'm still on KDE 3.5.12 because of this. \r\n\r\nI know you have lots of hours invested on that strigi/nepomuk thing and I can surely respect that. I only ask two things: \r\n\r\n<ul><li>A very easy way to disable all those new features that bog down KDE 4 - every time I try a new version, since January 2008. A simple option will do, or even a flag in a configuration file somewhere;</li>\r\n<li>And, please make KDE modular, do not create system wide dependencies on new untested and very difficult to have working technologies...</li></ul>"
    author: ""
  - subject: "While I have not experienced"
    date: 2011-07-28
    body: "While I have not experienced impaired performance for as long as I can remember using KDE SC 4.x, I agree to some extent in that the development of Nepomuk and Strigi worries me a bit.\r\n\r\nMy files are well organized and I never search for anything (except for e-mail in KMail ocassionally) or use that rating and tagging functionality. I wonder what the performance impact of Strigi and Nepomuk is, and I'd be interested to see benchmarks. i know that it's possible to disable Strigi and Nepomuk through System Settings \u2192 Desktop Search (possibly that it was you need Anonymous?), but does that really negate all the potential performance impact of these two?"
    author: "vanloon"
  - subject: "What makes KDE good for me is"
    date: 2011-07-28
    body: "What makes KDE good for me is strigi/nepomuk!! I have tones of files and I need a way to search inside them! I'm happy with Kubuntu 11.04 because of this. "
    author: "zayed"
  - subject: "Oxygen"
    date: 2011-07-28
    body: "Hey thanks for the comments.\r\nThe folder that is not finished is the HTML folder not the documents one. Need to come up with a better metaphor than a proto web browser that does not scale all that well...\r\n\r\nI tested this folders alot and, I'm fully aware many people wont like them specially at the beginning, but we decided it was a step that we needed to take for several reasons, and from the vast feedback i get in this things people seam to grow an increasing love for this new folder.\r\n\r\nThere are more than a few reasons we changed the folder and in the end we believe it was the right thing to do.... BTW that screenshoot was not the happiest one as its shows some icons that should not be there and way to less empty space between icons making it look crowded  a bit better would be http://wstaw.org/m/2011/07/28/plasma-desktopjI4451.jpg\r\n\r\ncheers nuno"
    author: ""
  - subject: "Not strigi/nepomuk specific"
    date: 2011-07-28
    body: ">What makes KDE good for me is strigi/nepomuk!!\r\n\r\nWell in this case, you should be upset that KDE devs chose to release this feature without tests: untested code is broken code (most of time).\r\n\r\nLook this is not a strigi/nepomuk specific issue, this is a software development process issue.\r\n\r\nIf you read about the Linux 3.0 development history[1], you'll see that they had a late bugs, that they finally understood and fixed with a 15 line patch, yet Linus *still* made another RC release to check that the patch was correct (and it wasn't 100% correct).\r\n\r\nSo a rewriting of not so small portion of code *activated by default in KDE*, *integrated after the last RC*??\r\nWhy do you think that this time they would have caught all the bugs even though they weren't able to do it the time before?\r\n\r\n1: http://lwn.net/Articles/452117\r\n"
    author: "renox"
  - subject: "I'm the OP.I'm watching"
    date: 2011-07-28
    body: "I'm the OP.\r\n\r\nI'm watching closely this, because I'm sick of watching my favourite desktop being botched down by a buggy implementation of an interesting technology, like Nepomuk. I see the heavy Nepomuk refactoring that is taking place in KDE 4.8 trunk, the rationale behind it and I suggest:\r\n\r\nWhy don't you just backport the whole thing? After all, despite all of those leaks, they a) were present before (I can attest that, KDE 4.6 exhibited similar behaviour with nepomukstorage, but I only suffered it once, since Strigi remembered what was indexed, so it was bearable) and b) they need anyway a refactoring for a proper fix (the infamous dbus CPU and memory leak bug was indeed killed by the refactoring in KDE 4.7.0)\r\n\r\nThere are users, like myself, willing to test the final fix of Nepomuk."
    author: "Alejandro Nova"
  - subject: "Asking the question"
    date: 2011-07-28
    body: "<cite>\r\nAsking the question differently, I still need to fall back to Chromium because rekonq doesn't work well with some websites. Are problems when web browsing with rekonq caused by rekonq itself, KWebKitPart or QtWebKit?\r\n</cite>\r\n\r\nNope. That is caued by the way QtWebKit releases are currently done which is beyond the control of reKonq or any webkit based KDE browser for that matter.  Since a more up to date version of QtWebKit is bundled with major feature released of Qt (4.6.0, 4.7.0, 4.8.0), users have to wait until the next cycle of a major Qt release to see any tangible improvement, other than critical bug fixes, in QtWebKit based browsers. For example, the version of QtWebKit (v2.0) that comes bundled with all of the Qt 4.7.x releases is based on a snapshot webkit from over a year ago.\r\n\r\nUnfortunately this problem cannot be addressed until QtWebKit releases are independent of Qt release which is something that is like not going to happen until Qt 5. The good news is the work on Qt 5 and Qt modularization is underway. Until then  I am afraid the only way to get an up to date webkit based browser for Qt based platforms is for either you or your distribution to compile one of the new stable branches of QtWebKit from source, http://trac.webkit.org/wiki/QtWebKitReleases."
    author: ""
  - subject: "congratulations for the new"
    date: 2011-07-28
    body: "congratulations for the new release. using it right now.\r\ndoes anybody know where i can find the wallpapers or why the overlays in the screenshots are black. i'm using kde 4.7 upstream on openSUSE."
    author: ""
  - subject: "khtml > QtWebkit"
    date: 2011-07-28
    body: "Strange... many users and devs had complains about khtml in the past, argueing that webkit was more advanced. But now that webkit is available, it looks like that the situation is not better: QtWebkit is not up to date. \r\nFor now, I'm still using khtml because it works better for me."
    author: "zeroheure"
  - subject: "Strigi!!!"
    date: 2011-07-28
    body: "Please, lobby hard for it.\r\n\r\nFedora is stuck since more than a year with Strigi 0.7.2, other distros simply follow suit.\r\n\r\nI'm used to living with Git trees, and I know that Strigi is about 0.7.6 these days, but nobody seems to know..."
    author: "Alejandro Nova"
  - subject: "you can try Plasma Netbook"
    date: 2011-07-28
    body: "you can try Plasma Netbook or... Gnome3 :P"
    author: ""
  - subject: "Use the DE which suits you best"
    date: 2011-07-28
    body: "It's totally fine to use the GNOME Shell as your primary desktop shell if it suits your usage pattern better. All your favorite KDE applications are still very well supported though you lose the features provided by our KDE Plasma Workspaces."
    author: "mgraesslin"
  - subject: "What  about KNetworkManager?"
    date: 2011-07-28
    body: "I don't want to mess my Kubuntu right now so i'm simply asking if KnetworkManager continues not to work with DSL connections!\r\nThat bug persist for almost 2 years and until now no resolve:\r\nhttps://bugzilla.redhat.com/show_bug.cgi?id=605527\r\nhttps://bugs.launchpad.net/ubuntu/+source/knetworkmanager/+bug/367161\r\nhttp://forum.kde.org/viewtopic.php?f=18&t=86951"
    author: ""
  - subject: "To be honest, I think that"
    date: 2011-07-28
    body: "To be honest, I think that the new folder icon looks a bit out of place. Not just on that screenshot, but everywhere. I don't know if its the color composition or the drawing style, but it somehow visually it doesn't mix perfectly. Maybe it's just me being used to the old icon, let's see."
    author: ""
  - subject: "Plasma Desktop Shell Crash"
    date: 2011-07-28
    body: "I just tried the opensuse 11.3 rpms and the plasma-desktop-shell crashes during login. Everything is working except the plasma desktop. Still don't know whether this is a bug or a feature.\r\n\r\n"
    author: "trixl."
  - subject: "Nobody is working in"
    date: 2011-07-28
    body: "Nobody is working in KNetworkManager !! Nowadays Plasma NM is doing all the networking GUI. See what's new in this release:\r\nhttp://lamarque-lvs.blogspot.com/2011/07/plasma-nm-bugs-fixed-after-465.html"
    author: "zayed"
  - subject: "Re: khtml  > QtWebkit"
    date: 2011-07-29
    body: "<cite>\r\nStrange... many users and devs had complains about khtml in the past, argueing that webkit was more advanced. But now that webkit is available, it looks like that the situation is not better: QtWebkit is not up to date.\r\n</cite>\r\n\r\nQtWebkit WAS more advanced than khtml at the time that discussion took place. And it still is if you compare apples to apples, i.e. the current versions of QtWebkit and khtml from their respective sources.\r\n\r\nAs I already explained what hinders QtWebkit is how it is being distributed right now. More recent versions are only included on major Qt releases. Anyhow, nothing stops distros or even you from compiling a more recent stable version version of QtWebkit and using that instead of what comes bundled with Qt [1][2]. You cannot say the same about \r\n\r\nHaving said that, there are more than a few things in khtml that do not exist in QtWebkit because no one has yet implemented them. Spell checker for input boxes and access keys support are two things at come to mind right away. However, on most prominent things like speed of the JavaScript engine or rendering more sites like the major browsers, there simply is no comparison.\r\n\r\n[1] http://gitorious.org/webkit/qtwebkit/trees/qtwebkit-2.1\r\n[2] http://gitorious.org/webkit/qtwebkit/trees/qtwebkit-2.2-tp1"
    author: ""
  - subject: "KDE-Telepathy"
    date: 2011-07-29
    body: "Is there way to test KDE-Telepathy without building for oneself, on Debian?"
    author: ""
  - subject: "Classic Oxygen"
    date: 2011-07-29
    body: "Dear Nuno,\r\nIs there a chance we'll see the \"Classic\" Oxygen Icons as an icon theme by itself???\r\nI like the new Oxygen Icons, but I have to say I'm deeply in love with the old ones, so, I'd like to see them in a different theme, something like \"Classic\" and \"New\" oxygen.\r\nCheers, Migue Chan\r\nhttp://blueleaflinux.blogspot.com/"
    author: ""
  - subject: "compiling qtwebkit"
    date: 2011-07-29
    body: ">Anyhow, nothing stops distros or even you from compiling a more recent stable version version of QtWebkit\r\n\r\nThe sheer size of qtwebkit 663 mb (downoad as source at git), and the time and configurations required to install; where are the clear and lucid documentation to compile and install qtwebkit for a distro?\r\n\r\nActually, I did compile and installed qt-4.8tp1, but it did not fix any promised issues, like java applets etc., becuase no clear instructions to compile and install the qt-4.8 or qtwebkit 2.2 are given. \r\n\r\nJust like kde extracted phonon from qt and included in KDE SC, why can't KDE extract qtwebkit into kdewebkit??? Because khtml abhors its own child webkit?\r\n"
    author: "fast_rizwaan"
  - subject: "KDE's super applications"
    date: 2011-07-29
    body: "Honestly, I use KDE because of:\r\n\r\n1. KWin - just behaves as a window manager should (compositing, wobbly, invert effect)\r\n2. System Tray - Really nice widget which does not disturb the work flow\r\n3. KWrite - very nice editor\r\n4. Okular - best pdf reader\r\n5. Dolphin - ok and better than rest but not the best\r\n6. Yakuake - best terminal reduces clutter\r\n7. smplayer - 2nd best video player but 1st in features and ease of use (filters, subtitle downloading)\r\n8. Vlc - 1st best video player but 2nd in features\r\n9. Qbittorrent - best feature wise\r\n10. speedcrunch - best calculator\r\n11. Goldendict - best dictionary application\r\n12. qt-recordmydesktop\r\n13. recoll - awesome desktop search which strigi strives to be\r\n\r\nAll the above look coherent in KDE because of\r\n14. Oxygen style - the best mac os like polished interface\r\n15. Oxygen icons - again the best mac os like polished icons\r\n\r\nAnd thanks to oxygen-gtk, most feature rich and useful programs are consistent in KDE 4; like gnome-mixer, gnumeric, libreoffice, firefox (with oxygen-kde)            \r\n\r\nAs can be observed, I'm using mostly QT only or GTK applications more for my entertainment and productivity;\r\n\r\nmost default applications do not go to the point of \"wow\" factor like smplayer, goldendict, speedcrunch. here's a list\r\n\r\na. dragon player < smplayer/vlc (even with phonon-mplayer phonon-vlc, fullscreen, subtitles, mouse, keyboard behavior)\r\nb. konqueror+kthml < firefox/chrome/opera/konqueror+kwebkitpart\r\nc. kmix < gnome-mixer, etc.\r\nd. kcalc < speedcrunch\r\ne. dolphin < thunar (the sidebar of thunar is neat for one, and filenames do not get truncated for 2)\r\nf. juk < xmms, etc. (can't play mp3/music files using juk from filemanager unless added to playlist)\r\ng. strigi+nepomuk < recoll+updatedb/mlocate (nice -n10 updatedb)\r\n\r\n"
    author: "fast_rizwaan"
  - subject: "At least get your facts straight"
    date: 2011-07-29
    body: "> Well in this case, you should be upset that KDE devs chose to release this feature without tests: untested code is broken code (most of time).\r\n\r\nThe new Nepomuk bits come with a lot of unit teests to check functionality of the internals and prevent regressions with development. Please, at least get your facts straight before going with unsubstantiated claims."
    author: "einar"
  - subject: "The sheer size of qtwebkit"
    date: 2011-07-29
    body: "<cite>\r\nThe sheer size of qtwebkit 663 mb (downoad as source at git), and the time and configurations required to install; where are the clear and lucid documentation to compile and install qtwebkit for a distro?\r\n</cite>\r\n\r\nFirst, why exactly would distros need anymore special documentation than the one provided at  https://trac.webkit.org/wiki/QtWebKit#BuildInstructions to compile QtWebKit ?  If you want to compile your own up to date version of QtWebKit all you have to do is build Qt without webkit support by passing <em>--no-webkit</em>.  Then you can compile and install your own QtWebKit version [1].\r\n\r\nSecond, the actual size of the tar ball you can download from the links I posted in the earlier reply (there is a link for downloading a tar ball) is about half the size of what you mentioned above.\r\n\r\n<cite>\r\nActually, I did compile and installed qt-4.8tp1, but it did not fix any promised issues, like java applets etc., becuase no clear instructions to compile and install the qt-4.8 or qtwebkit 2.2 are given.\r\n</cite>\r\n\r\nI have no idea what you did, but java applet support has been working since QtWebKit 2.1. See https://bugs.webkit.org/show_bug.cgi?id=33044. On Linux you need to install the offical Sun/Oracle jre or java sdk for applets to work properly. The icedtea plugin will not work properly and no one has had the time nor the inclination to look into why. I know I haven't.\r\n\r\n<cite>\r\nJust like kde extracted phonon from qt and included in KDE SC, why can't KDE extract qtwebkit into kdewebkit??? Because khtml abhors its own child webkit?\r\n</cite>\r\n\r\nDo you have any idea how much work that is ?? The amount of work required to maintain the QtWebKit port is more than enough to keep several folks in the Qt division of Nokia busy on a daily basis let along the one or two people working on kdewebkit. Not to mention the fact that KDE is in the process of getting some of the core things merged upstream for Qt 5 which would be completely counter intutive to what you are suggesting here. But then again you seem to think that the reason for this has something to do with the very old khtml vs webkit conflict...\r\n\r\nOh and KDE did not exactly extract phonon from Qt. It was the Qt folks that sucked Phonon in and then changed their mind later because they had their own specific needs that they claimed cannot be satisified through the collaborative effort of working on Phonon.\r\n\r\n[1] http://qtwebkit.blogspot.com/2011/07/qtwebkit-22-beta1-vs-qt-48-beta1.html"
    author: ""
  - subject: "> The new Nepomuk bits come"
    date: 2011-07-29
    body: "> The new Nepomuk bits come with a lot of unit tests to check functionality of the internals and prevent regressions with development. Please, at least get your facts straight before going with unsubstantiated claims.\r\n\r\nI don't claim that Nepomuk is untested, unit test are nice, but the \"Release Candidate\" test is a much more thorough test to pass (assuming there're are enough users willing to use the RC): that's why Release Candidate exist!\r\n\r\nThat new code was integrated after the last RC which means that de facto, the RC wasn't a real RC, IMHO the KDE project should have the courage of naming things properly: either they do RC and call them RC, either they don't and they call it beta-1, beta-2, etc and then release.\r\n\r\n"
    author: "renox"
  - subject: "Good comment. I really like"
    date: 2011-07-29
    body: "Good comment. I really like KWin as well, but for perhaps other reasons than you. I don't care about compositing, wobbling and other effects. In fact, disabling all of them is the first thing I do. Heck, I don't even display contents of moving windows. I like KWin because it's so intuitively productive. It is smart on its own, but also lets me manipulate windows fast and easily, by keyboard or by mouse. It is also very configurable when I need that. I'm excited to hear of improvements in 4.7, and I'm very glad this fundamental component of the desktop is not only not neglected, but actively focused on. Thanks, KDE developers!"
    author: "Haakon"
  - subject: "a feature?"
    date: 2011-07-29
    body: "Of course, a crash isn't a feature. I haven't tried the opensuse rpm's yet, so I don't know whether it's a bug in packaging. But it's most likely something weird locally to your installation. Try moving the .kde or .kde4 directory to a backup place and then login; it might be some setting in there that's causing trouble."
    author: ""
  - subject: "Unfortunately, the KDE"
    date: 2011-07-29
    body: "Unfortunately, the KDE project abuse the term \"release candidate\" in strange ways. Then they release an RC, it's explicitly *not* a candidate for release, i.e., something that they consider releasing unchanged as the final version. KDE used to have proper release candidates in the old days, but these days we see major rewrites even after the *second* \"release candidate\", creating problems in the actual release. I wish they would reconsider this practice. On the other hand, though the path is rocky, what comes out at long last is a great product, so I can't really complain."
    author: "Haakon"
  - subject: "Its in kde-look"
    date: 2011-07-29
    body: "The classical folders are just a few clicks away via Get hot new stuff (yes its not hot and new but.. ;)...), in the icons area just use the GHNS to fetch the classical folder icons in kde-look.\r\n\r\nCheers "
    author: ""
  - subject: "Before 4.7.0 icons"
    date: 2011-07-29
    body: "kde-look.org has a before 4.7.0 icon theme. Simply search \"oxygen old\" or whatever ;)"
    author: ""
  - subject: "The old icons just better."
    date: 2011-07-29
    body: "The old icons were just better. +1"
    author: ""
  - subject: "kmail1 -> 2 migration problems"
    date: 2011-07-29
    body: "I'd be a lot happier with 4.7 if kmail2 worked.  So far its been unable to migrate my kmail1 emails.  I've been using kde since 1999/2000 and have a large collection of emails to migrate.  kmigrate gave up after creating my folders.  attempting to import the folders manually causes linux 3.0 to oom...  I have yet to find a good migration guide and IMHO its badly needed.\r\n\r\nEd Tomlinson"
    author: ""
  - subject: "wow great... will try it on"
    date: 2011-07-29
    body: "wow great... will try it on my machine soon.........\r\n"
    author: ""
  - subject: "I migrated via evolution."
    date: 2011-07-29
    body: "I migrated via evolution. Evolution was happy to migrate my emails from kmail1 and kmail2 was happy to migrate my emails from evoltion. I never tried the kmail1 -> kmail2 migration so don't know how bad it is."
    author: "hconnellan"
  - subject: "Unit tests alone: not enough."
    date: 2011-07-29
    body: "Unit tests are not a valid replacement to widespread testing, when it comes to memory leaks. The unit test will pass, because the functionality is there, and the memory leak will be small and so will get unnoticed, because the function generating the leak is only used once. However, if you want to test seriously a program, you don't need only unit tests; you need torture tests. For instance:\r\n\r\n1. Select 12,000 audio clips from FreeSound and index them with Strigi.\r\n2. Repeat with 1,000 complex PDFs.\r\n3. Repeat with 1,000 MP3.\r\n4. Monitor memory consumption. Are there any leaks? How is every service in the Nepomuk chain working?\r\n\r\nI've been working with some law documents (statute histories, they are LOOOOOONG) and I proposed them as torture tests to exercise one specific Nepomuk feature: PDF text indexing. I'm amazed that you don't know that: I'm finishing a law thesis, I don't know a bit about coding, but, as you can see, I don't need to be a computer scientist to know a bit about testing.\r\n\r\nPS: Better yet: why don't you turn your unit test into a test that exercises 50,000 times the same function, and see the result (and you can also time it, measuring performance increases)?. This *COULD* be better, but still cannot replace torture tests (e.g. cleaning memory properly in the test, but not in the real world). Unit tests built this way + torture tests = the way to go ;)"
    author: "Alejandro Nova"
  - subject: "Test results: second leg."
    date: 2011-07-29
    body: "After filing a bug in Fedora, I got experimental Strigi 0.7.5 packages, I installed them, and I discovered that my Strigi install was broken. There were duplicate files in Nepomuk database, and after I purged the old install and installed Rex's packages, some of my issues (Strigi indexer not remembering what was indexed, incorrect PDF indexing, for instance) were corrected.\r\n\r\nHowever, the memory leak remains while indexing, and I suspect this leak haunted us since forever. Reindexing with the new Strigi binaries, Virtuoso is at 1 GB and counting. Fortunately, Vishesh Handa told me he can reproduce the leak, and he's working on it. Let's hope for the best :)"
    author: "Alejandro Nova"
  - subject: "KDE 4.7 is, as usual, a great"
    date: 2011-07-30
    body: "KDE 4.7 is, as usual, a great KDE release but KMail 2 shoul'd have been shipped with so many bugs :-(\r\n\r\nI only confide in packagers which will keep to distribute the great KMail 1.\r\n\r\nAnyway... great job KDE Team!!!! :-)"
    author: ""
  - subject: "Wrong!\nknetworkmanager is"
    date: 2011-07-30
    body: "Wrong!\r\nknetworkmanager is still used a lot.\r\nActually, due to all the problems with the plasma nm stuff, a lot of users switched to wicd/wicd-kde which IS stable"
    author: ""
  - subject: "Unfortunately KMail is severely broken - at least for me"
    date: 2011-07-31
    body: "I cheered when 4.7 was released and posted on Facebook as well as Google+ ... a bit too early apparently.\r\n\r\nDigikam has new features, that's great. KMail has new features, that's great too.\r\nHowever, there's something severely wrong. I have never had such serious problems with SuSE/KDE before - they're serious enough that I will be considering leaving the platform, but I hope I can figure it out so it won't have to come to that.\r\n\r\nI can't create a new person tag in Digikam from an address book entry. Don't ask me why, I haven't the faintest idea. Could be because no people are showing up in my address book. I have no idea where all my contacts are now, but the address books that worked fine before the upgrade are still appearing in KAddressBook - there's just no people in them now.\r\n\r\nKMail is simply crashing every time after filtering messages and complaining about duplicates (conflicting updates). So I can't access my old e-mail and I can only read and send e-mail using browser-based interfaces or my Android-based telephone (which doesn't have all my accounts defined).\r\n\r\nAfter reporting the KMail crash (bug 278745) I get a \"fixed in master/4.7.1\" reply slammed in my face and the error report is closed as fixed with no information about which packages the fix influences or any other information which could be useful for anyone reading the bug report and who are trying to solve their problems. I've tried upgrading quite a few packages from the factory repository, but that hasn't helped. So much for assuming that a released version is stable... \r\nApparently there is no plan to fix this in an urgent update to 4.7.0 - all the suckers such as myself will just have to learn our lessons and not upgrade any important system in the future - unless we have *a lot of* time to spare.\r\n\r\nSeems the KDE team has learnt one trick too many from the Microsoft people; ship it with known errors, no matter how serious.\r\n\r\nThe recommendation may be to perform a clean install and import all the data and settings afterwards, and perhaps everything would have worked then - but without a proper tool to migrate settings from a previous version installed (or from a backup of such an install) - how many people are willing to perform clean installs every time? I'm trying to use the computer for work - not just as a toy to waste time on.\r\n\r\nWe have an iMac in the household - I've cursed it for being unflexible and not very co-operative in a mixed environment. Perhaps I'm going to end up there, just like my neighbour who used to have Linux but said he switched to MacOS because he got tired of having to tinker."
    author: "kjetil-kilhavn"
  - subject: "Next month"
    date: 2011-07-31
    body: "If they told you that bug was fixed in 4.7.1, then that's it, it's fixed. You'll have the bug fixed in the very next upgrade, next month, and it will be better tested. I'd be happy that it was found and fixed already.\r\n\r\nIf you have very sensitive information in a production machine, you should be very careful with upgrades, on _any_ system. That's why 4.7.1, 4.7.2, etc. are there, IMO.\r\n\r\nHaven't you heard? OSX users have plenty of problems on upgrades, too.\r\n"
    author: "jankusanagi"
  - subject: "still used \u2260 has active"
    date: 2011-07-31
    body: "still used \u2260 has active development"
    author: "zayed"
  - subject: "Everything pretty but.."
    date: 2011-07-31
    body: "Everything pretty but...\r\n\r\n\r\nI still wait for full funcionality of Kontact. Lack in it, capabilities of simple integrations with Google.Simple system of backup. \r\n\r\nHere however, is better Gnome Evolution\r\n \r\n"
    author: ""
  - subject: "Brilliant comment"
    date: 2011-07-31
    body: "All I wanted to say."
    author: ""
  - subject: "Plasma NM"
    date: 2011-07-31
    body: "Have you tried it recently (current versions)? Are you still experiencing problems?"
    author: "bluelightning"
  - subject: "Upgrade problems in MacOS"
    date: 2011-07-31
    body: "Can't say I have heard about an upgrade of MacOS (or Windows) that makes the e-mail completely unusable. I'm sure it is correct that this has been fixed in the 4.7.1 branch - but leaving it at that just isn't good enough in my opinion. Then again, I'm not a KDE hacker myself so I can unfortunately not contribute to help the situation. Guess it will be webmail for me the next month and hoping that the 4.7.1 update fixes it."
    author: "kjetil-kilhavn"
  - subject: "If it's tagged as fixed in"
    date: 2011-07-31
    body: "If it's tagged as fixed in 4.7.1 then that's the KDE release it will be fixed in.  Distros may pick up the patch before then, it may help opening a bug on your distro to request this.\r\n\r\nNot to put too fine a point on it, if you're not happy with things breaking on a .0 release then don't install a .0 release, wait for the .2. or .3 bugfix release.  Unfortunately we don't have the resources to exhaustively test every possible combination, we don't have the budget or time of a Microsoft or Apple or even a Canonical, we're just a bunch of volunteers and have to accept that we will always ship with some bugs.  We try hard to make sure nothing too serious gets out, but we often don't experience the bugs that our users find and have to rely on them to help, it's just part of the process of making open source software.  In fact, this seems a perfect example, .0 release went out, user finds problem and files bug, bug gets fixed for next release.  Sure, it's not ideal, but for me it beats the alternative.\r\n\r\n"
    author: "odysseus"
  - subject: "don't boast then"
    date: 2011-08-01
    body: "Well I guess he was confused by the title of the release announcement, promising better stability? Why do you promise this then?\r\nBTW, better stability as compared to what? KDE 4.6.5? Or kde 4.6? Or gnome perhaps?\r\nIf you make a bold statement like that, people expect you to make good on that promise. That's normal.\r\n\r\nJohn"
    author: ""
  - subject: "Impresive"
    date: 2011-08-01
    body: "I just installed 4.7 from the kubuntu PPA. It is amazingly fast!\r\nOk i admit, at the same time i also activated the software rasterizer for Qt, but man is that system fast now.\r\nI was considering upgrading my laptop, but those plans are put on hold now :)\r\nkeep up the great work!\r\n\r\nPS: didn't install the new PIM yet. Hope its as great as the rest as kde 4.7."
    author: "Asraniel"
  - subject: "as usual"
    date: 2011-08-01
    body: "Installed 4.7, and yet again the desktops effects are broken again.\r\nOpening a bug report doesn't help, as nobody looks at them.\r\nThank you KDE."
    author: ""
  - subject: "Yes they do, even if we don't"
    date: 2011-08-01
    body: "Yes they do, even if we don't always write back on them.\r\n\r\nIf you don't open a bug report you should have absolutely no expectation for it to get fixed."
    author: ""
  - subject: "There is basic support for Google's services"
    date: 2011-08-01
    body: "Try akonadi-googledata-resource. A bit basic, but works for syncing calender and contacts.\r\n\r\nI compiled it from git, see if there's distro support for your system."
    author: ""
  - subject: "I also liked the old one, but"
    date: 2011-08-01
    body: "I also liked the old one, but I like the new one much better."
    author: ""
  - subject: "Because it is true"
    date: 2011-08-02
    body: "12000 bug reports closed, 2000 specific bugs in the released software resolved since 4.6 - the reference was also in relation to the semantic desktop where a lot of long-term issues have been resolved."
    author: "Stuart Jarvis"
  - subject: "how many of those were"
    date: 2011-08-02
    body: "how many of those were originally filed against KDE < 4, and simply closed as \"wontfix - obsoleted version\"?"
    author: ""
  - subject: "took some work..."
    date: 2011-08-02
    body: "my Kubuntu upgrade went with one small hitch...Smooth Tasks broke, and the maintainer isn't around. Oh well.\r\n\r\nAgainst my better judgment, I installed the new PIM. It works really well. I am surprised at what an improvement KMail 2 is over 1.\r\n\r\nThat this upgrade was against my better judgment...\r\nIt wouldn't even start when I first upgraded (by the way, there are warnings all over the place that this is _experimental_). I'm not the owner of my own inbox? Ridiculous. \r\n\r\nAfter much forum-ing and other exploring, I discovered in \"Akonadi Configuration\" (typing in start menu find box) that the place I stuck Mail was not where it was expected. Modifying that to its actual location made KM2 run. \r\n\r\nThen I had to check out the rest of the resources. Calendar was funky, cuz there are now several choices for resources to display. Pretty easy to sort out once I had discovered Akonadi Configuration. Just cuz I don't know better, I went to the Akonadi Server Configuration tab and restarted the server after the modifications.\r\n\r\nOh it also took quite a while for the 3000+ messages to become usable in KM2. In the migration, I was re-introduced to my bad email behavior of saving everything. (A message that my password was changed with no indication of the account OR the password. Yeah, gotta keep that one.)\r\n\r\nI've been using KMail for a long time. KMail 2 has dramatic improvements.\r\n\r\nThank you so much\r\n\r\nAnd Plasma 4.7...wonderful. No issues (well I miss the Smooth Tasks eye candy). Just fast and solid feel. Activities...after the first load, switching is fast, everything solid.\r\n\r\nBy the way, none of these issues were resolved by complaining at dot.kde.org. kde-look.org gave the clues about the status of Smooth Tasks and the forums (special thanks to Ben Cooksley, again) pointed the way to Akonadi and proper file locations.\r\n\r\nThank you KDE devs.\r\n\r\nCarl"
    author: "kallecarl"
  - subject: "not that many actually"
    date: 2011-08-03
    body: "of all those bugs, only 7 were from versions prior to 4. There was one from KDE 2 that got fixed with a change to the basic I/O routines and 6 exclusively from KDE 3. These mostly had to do with incompatibility with the old version of Q.T. so it wouldn't do much good to fix them now.\r\n\r\nThat is quite impressive.\r\n\r\nDid the bugs you filed get fixed? "
    author: ""
  - subject: "Test results: third leg."
    date: 2011-08-03
    body: "I will write this here with bold and caps letters, because I can't believe what I'm seeing, and it's amazing.\r\n\r\n<strong>I INSTALLED RAPTOR 2.0.4, SOPRANO LATEST, AND THERE ARE NO MEMORY LEAKS WITH STRIGI AND KDE 4.7.0. REPEAT. NO MEMORY LEAKS.\r\n\r\n<strong>VIRTUOSO MEMORY USAGE DOES NOT GROW BEYOND ITS CONFIGURED LIMIT.\r\n\r\n<strong>REPEAT.\r\n\r\n<strong>NO MEMORY LEAKS, RAPTOR 2.0.4, SOPRANO LATEST BUILT AGAINST RAPTOR 2.0.4, KDE 4.7.0.\r\n\r\n<strong>IF YOU ARE EXPERIENCING MEMORY LEAKS WITH KDE 4.7.0 AND NEPOMUK WITH STRIGI INDEXING, CONTACT YOUR DISTRIBUTOR IMMEDIATELY.</strong>\r\n\r\n\r\nThis is it. KDE 4.7.0 is going to be great... when it's packaged properly. Let's wait :)"
    author: "Alejandro Nova"
  - subject: "And how much still open"
    date: 2011-08-05
    body: "And how much bugs still open...\r\nand never looked at..."
    author: ""
  - subject: "good work"
    date: 2011-08-08
    body: "Stuart: good work with the bug fixing.\r\nI really mean that!\r\nStill: be careful with the broad, sweeping claims. :-)\r\n\r\nJohn"
    author: ""
---

KDE is delighted to announce its latest releases—Version 4.7—providing major updates to the KDE Plasma Workspaces, KDE Applications, and the KDE Platform. Check out the highlights below, or read the <a href="http://kde.org/announcements/4.7/">full announcement</a>.

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -304px; width: 600px; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/konqueror-dolphin.png" /></div>

<h3>Plasma Workspaces Are More Portable</h3>
With extensive work on KDE’s compositing window manager, KWin, and new Qt technologies such as Qt Quick, the most advanced user interface is available in more places and on more kinds of devices. <a href="http://kde.org/announcements/4.7/plasma.php">Read more...</a>

<h3>Akonadi Positions Kontact for the Future</h3>
KDE's groupware solution, Kontact, rejoins the main KDE release cycle with all major components ported to Akonadi, a leading personal information management storage service. The Digikam Software Collections comes with a new major version, 2.0. <a href="http://kde.org/announcements/4.7/applications.php">Read more...</a>

<h3>Improved Multimedia and Semantic Capabilities</h3>
There are major improvements to Phonon and social semantic desktop components in the KDE Platform. Enriched APIs and better stability offer significant benefits to developers. <a href="http://kde.org/announcements/4.7/platform.php">Read more...</a>

<h3>New, integrated Instant Messaging</h3>
The KDE-Telepathy team is proud to announce the technical preview and historic first release of the new Instant Messaging solution for KDE...still in its early stages. All sorts of accounts are supported, such as GTalk and Facebook Chat. The Presence Plasma widget drops right into the panel for managing online status. As this project is not yet mature enough to be part of the big KDE family, it has been packaged and <a href="http://download.kde.org/download.php?url=unstable/telepathy-kde/0.1.0/src/">released separately</a>, and has <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/Installing_stable_release">installation instructions</a>. Give it a try!

<h3>Stability + Features</h3>
In addition to the many new features described in the detailed release announcements, KDE contributors have closed over 12,000 bug reports since the last major releases of KDE software in January 2011. As a result, KDE software is more stable than ever before.

<h3>Help Spread the Word</h3>
While it's easy to think of the KDE team as mostly developers, non-technical users are also critical to success. Please help spread the word. Report bugs. Encourage others to join the <a href="http://community.kde.org/Getinvolved">KDE Community</a>. 

Follow what's happening with the new releases on the social web at <a href="http://buzz.kde.org">buzz.kde.org</a>.