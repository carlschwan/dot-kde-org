---
title: "KDE Commit-Digest for 23rd October 2011"
date:    2011-10-31
authors:
  - "mrybczyn"
slug:    kde-commit-digest-23rd-october-2011
comments:
  - subject: ":)"
    date: 2011-10-31
    body: "Thank you Digest-team :)"
    author: ""
  - subject: ":D"
    date: 2011-11-01
    body: "as anonymous said before..  thank you digest-team !! \r\n\r\ni really like to read about the new achievements on a weekly basis..\r\n\r\nthx !! "
    author: ""
  - subject: "Kmail broken on import..."
    date: 2011-11-02
    body: "[edited]\r\nAnonymous User not happy. Screams into the air.\r\n\r\nVague complaints:\r\nKMail 2 migration doesn't work right.\r\nA lot of people filing bug reports.\r\n"
    author: ""
  - subject: "Dot therapy"
    date: 2011-11-02
    body: "Comments like this are useless to anyone who can do anything about them. \r\n\r\nHowever, overall beneficial effect if the commenter felt better after this outburst."
    author: ""
  - subject: "Donations"
    date: 2011-11-08
    body: "Add a flattr widget to this posts?"
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-10-23/">this week's KDE Commit-Digest</a>:

<ul><li>Work on grouping for names in KDE Base</li>
<li>Added minimal global search implementation with autocompletion in <a href="http://www.kexi-project.org/">Kexi</a></li>
<li>New Gaussian autobrush type in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Amazon-based music store in <a href="http://amarok.kde.org/">Amarok</a></li>
<li>Fixed handling of redirections with HTTP DELETE and PUT requests in KDE Libs</li>
<li>Geolocation filter option added to <a href="http://www.digikam.org/">Digikam</a> filtering sidebar.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-10-23/">Read the rest of the Digest here</a>.
<!--break-->
