---
title: "4.6 RC2 Available, Last Chance to Test"
date:    2011-01-05
authors:
  - "sebas"
slug:    46-rc2-available-last-chance-test
comments:
  - subject: "Unassigned regression in Dragon player, seemingly easy to fix"
    date: 2011-01-05
    body: "... for someone who knows to code:\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=259313\r\n\r\n(Dragon player does not prevent screen saver from kicking in)"
    author: "mark"
  - subject: "I agree that this is a \"nice"
    date: 2011-01-05
    body: "I agree that this is a \"nice to have\" feature and that the infrastructure is there to implement it. However, I don't think that this is the place for individuals to promote their idiosyncratic wishes. Apart from that, it is way to late for a new feature in 4.6 due to the freezes."
    author: "mutlu"
  - subject: "Small script to improve KDE"
    date: 2011-01-06
    body: "It kills all the processes started by KDE that nobody except their developers want:\r\n\r\n<code>\r\n#!/bin/bash\r\n\r\nSHIIT=(akonadi_control akonadiserver mysqld akonadi_agent_launcher akonadi_maildispatcher_agent akonadi_nepomuk_calendar_feeder akonadi_nepomuk_contact_feeder nepomukserver)\r\n\r\nfor s in \"${SHIIT[@]}\"\r\ndo\r\n     killall $s\r\ndone\r\n\r\nexit 0\r\n</code>"
    author: "trixl.."
  - subject: "Your life can be easier..."
    date: 2011-01-06
    body: "Simply execute \"akonadictl stop\". It will not kill nepomukserver, but Nepomuk can be easily disabled in System Settings.\r\n\r\nYour other option is to compile kdebase without Akonadi support. Then Plasma will not start it in the first place (unless you want to use KDEPIM, of course)."
    author: "christoph"
  - subject: "Well, it is a bug, so it is"
    date: 2011-01-06
    body: "Well, it is a bug, so it is still possible to fix it in KDE 4.6.x. And I am awaiting Mark's patch, since it is so \"easy to fix\"."
    author: "christoph"
  - subject: "That's a very good advice!"
    date: 2011-01-06
    body: "That's a very good advice! Btw. Nepomuk is disabled in the system settings (per default in suse), but somehow it's stubborn and starts anyway. \r\n\r\nDo you think KDE would nicely compile without support for Akonadi and Nepomuk? That would be a reason to switch to Gentoo.\r\n"
    author: "trixl.."
  - subject: "tweaked KDE builds"
    date: 2011-01-06
    body: "You could build your own KDE without Nepomuk or Akonadi in the openSUSE Build Service too.  Use Gentoo if your bedroom is chilly at night.\r\n\r\nWe're enabling Nepomuk by default in 11.4 again."
    author: "Bille"
  - subject: "It is a bug and more specific"
    date: 2011-01-06
    body: "It is a bug and more specific a (annoying) regression due to a change in another part of KDE. I put it here, because it is on the list of unassigned bugs in the hope someone who is able to do this would change this 3 lines of code which is all what is to do according to a blog entry."
    author: "mark"
  - subject: "Same issue with RC1"
    date: 2011-01-06
    body: "Currently have 4.4.3 installed. Does this mean I need phonon-4.4.4?\r\n\r\n[ 62%] Building CXX object phonon/kded-module/CMakeFiles/kded_phononserver.dir/phononserver.o\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp: In constructor 'PhononServer::PhononServer(QObject*, const QList<QVariant>&)':\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:66:5: error: 'registerMetaTypes' is not a member of 'Phonon'\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp: In member function 'void PhononServer::updateAudioDevicesCache()':\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:695:9: error: 'DeviceAccessList' is not a member of 'Phonon'\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:695:34: error: expected ';' before 'deviceAccessList'\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:711:17: error: 'deviceAccessList' was not declared in this scope\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:711:37: error: 'DeviceAccess' is not a member of 'Phonon'\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:714:67: error: 'deviceAccessList' was not declared in this scope\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:733:9: error: 'DeviceAccessList' is not a member of 'Phonon'\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:733:34: error: expected ';' before 'deviceAccessList'\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:737:17: error: 'deviceAccessList' was not declared in this scope\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:737:37: error: 'DeviceAccess' is not a member of 'Phonon'\r\n/usr/src/kdebase-runtime-4.5.95/phonon/kded-module/phononserver.cpp:740:67: error: 'deviceAccessList' was not declared in this scope\r\nmake[2]: *** [phonon/kded-module/CMakeFiles/kded_phononserver.dir/phononserver.o] Error 1\r\nmake[1]: *** [phonon/kded-module/CMakeFiles/kded_phononserver.dir/all] Error 2\r\nmake: *** [all] Error 2\r\n"
    author: "stumbles"
  - subject: "As a symptom of the wider"
    date: 2011-01-06
    body: "As a symptom of the wider issues showing in KDE, it is absolutely right to raise it here, as I said in the article :\r\n\"Wow. You\u2019ve removed the \u2018presentation\u2019 profile, hidden the option to recreate it, there\u2019s no easy way to do the same thing another way, and you\u2019ve done it before even every KDE application (i.e. DragonPlayer) has been updated to call your new DBUS magic system, never mind everything else that might need to like OpenOffice (presentation mode).\r\nWhat happens if a given application doesn\u2019t add your magic code \u2013 I\u2019m thinking of Eclipse, for instance, often used in presentations ? Are users expected to jump through the horrible hoops req. to create a new profile ?\r\nBit of a fail guys, put it back how it was.\" "
    author: "Tom Chiverton"
  - subject: "I did not want to write it"
    date: 2011-01-06
    body: "I did not want to write it that way because I am happy that KDE is evolving. The problems are with those parts of KDE which have lost their maintainer. There does not seem to be a mechanism to really raise awareness when regressions happen through changes in other parts of KDE. I am not sure who reads unassigned bugs. If I would be able to code, I would do it for sure. If understand that blog entry right, the fix should  be done in few minutes. I did not want to complain, but I hoped that someone, may be even new one could jump in."
    author: "mark"
  - subject: "Just to avoid spreading FUD,"
    date: 2011-01-07
    body: "Just to avoid spreading FUD, as I already replied on my blog.\r\n\r\n\"hidden the option to recreate it\" is something you made up and totally untrue: the location of the option for creating new profiles is the same since KDE 4.1.\r\n\r\nThe \"horrible hoop\" of creating a presentation profile is:\r\n\r\n - Go to the profile editor\r\n - Click on \"Create new profile\" in the toolbar\r\n - Name it \"Presentation\"\r\n - Choose a pretty icon (optional)\r\n - Save\r\n\r\nBy default, every control is disabled: here's your new presentation profile.\r\n\r\n\r\nThe rationale of removing the presentation profile from the defaults is that the user should not care about these things in the first place - the developers should instead. Just allow some time for anybody else to pick it up, providing a default profile for that is just a way for preventing this change to happen. And I think people who need it badly can sustain doing 2 clicks for creating one."
    author: "drf"
  - subject: "GNOMEism"
    date: 2011-01-07
    body: "I am sorry that is just such a GNOMEism. Trying to do the better design in a way that is less useful to users and makes it cumbersome to users to get actual real-life work done. At least we still have the option to add it, but seriously, this sucks."
    author: "carewolf"
  - subject: "I don't think so actually,"
    date: 2011-01-07
    body: "I don't think so actually, and I'll tell you why.\r\n\r\nFirst of all, KDE was the only desktop environment providing such a setting, given that we still did not have a way for handling this situation properly.\r\n\r\nMore than that, we will try to push this specification out into freedesktop.org, so that other non-KDE applications can benefit from it.\r\n\r\nIf the above shall fail, application can still integrate this feature, as it is exposed through DBus as well - in this case, we will provide patches for major apps (such as OO.org) to fix that, in the hope they will be accepted.\r\n\r\nThis is just the first step in giving a better experience to users - we are just asking for some patience, because the only way for pushing a new feature is to show off that it works and it gives some benefits to developers.\r\n\r\nAnd after all, from a user's point of view remember that:\r\n\r\n1. You can create a presentation profile easily, as I told you before.\r\n2. If you are upgrading from a previous KDE version and not starting from a clean home folder, all of your previous profiles will be automatically converted and upgraded on first startup - including Presentation.\r\n\r\nI hope this makes things clearer."
    author: "drf"
  - subject: "Moving window is jerky during for 1 second and w/ launch notif"
    date: 2011-01-08
    body: "Hi ! First, congrats for the great work !!! \r\n\r\nUsing Kubuntu 10.10 + nvidia 9800M GTX proprietary drivers.\r\n\r\nIn 4.6 RC1 the animations got very jerky.\r\nIn RC2, it is back to normal most of the time except :\r\n\r\n- when moving a window, when the transparency plugin is enabled, the animation is very jerky during the first second (of half second) then it gets perfectly smooth again\r\n\r\n- moving a window when launching an app at the same time makes the animation jerky as long as the icon is bouncing (launch notifier)\r\n\r\nIn 4.5.x there was no such issue.\r\n"
    author: "mahen"
  - subject: "MediaTomb (upnp) in places not working."
    date: 2011-01-08
    body: "Hi,\r\n\r\nJust installed rc2 and noticed my MediaTomb (in same machine) as a folder in places. But clicking it does nothing. \r\n\r\nTried to submit a bug, but somehow my newly created kde account was not accepted..."
    author: "zaris"
  - subject: "Nepomuk error"
    date: 2011-01-08
    body: "The nepomuk error continues to appear after login, and another error interrupts the logout process. I think those two errors should be resolved before the 4.6.0 release since they are pretty obvious and weren't present in 4.5.x. \r\n\r\n"
    author: "trixl.."
  - subject: "Why not just keep the menu"
    date: 2011-01-08
    body: "Why not just keep the menu option till, at the very least, all KDE applications have adopted the new way of doing things ?\r\nThis sort of pointless removal of perfectly good and useful options is the sort of thing Windows does. Isn't the whole point of KDE *SC* is that it's an integrated whole ?"
    author: "Tom Chiverton"
  - subject: "The point is that the"
    date: 2011-01-09
    body: "The point is that the *default* option was removed for new users. We did not wipe the possibility out, we are simply advising the user against doing that, but you are still able to create your profile and work the way you want - which is pretty much the KDE way."
    author: "drf"
  - subject: "This is true. Especially as"
    date: 2011-01-09
    body: "This is true. Especially as it seems there is a patch ready -- as far as I understand from lurking on the nepomuk list.\r\n\r\nAnd also because in 4.5, nepomuk worked for me very well, and I'd be sad to not have it for a couple (point) releases. And although I know it is not the main point of nepomuk, the search/indexing is extremely useful in combination with alt-F2.\r\n\r\nNot, not just useful. Really cool actually.\r\n\r\nSpeaking of nepomuk: I expect that having this search part stable and working will yield more usage, and therefore more interest, and this is how the promise of nepomuk will be bootstrapped."
    author: "hmmm"
  - subject: "Try this"
    date: 2011-01-09
    body: "Hey,\r\nOpen dolphin (home directory), unhide files and folders with alt + . and then go to .kde (or .kde4), share, config, and open file called kwinrc with your text editor.  Below [Compositing] section enter this: MaxFPS=30 . Save the file and restart kde. See if this helps."
    author: "Nece228"
  - subject: "Thanks for the tip. It makes"
    date: 2011-01-12
    body: "Thanks for the tip. It makes no difference though. (I tried this under the latest Chakra liveCD with KDE 4.6rc2. -- by the way, contrary to my former Kubuntu install, animation is always very jerky while moving a windows ; under Kubuntu it used to be jerky during the first second of a move operation only). \r\n\r\nOther animations like \"scale / expos\u00e9\" are very smooth on the other hand.\r\n\r\nIn 4.5.5 it's almost always perfectly smooth (although it's not always optimal)."
    author: "mahen"
---
KDE's release team has rolled another set of 4.6 tarballs for us all to test and report problems: <a href="http://www.kde.org/announcements/announce-4.6-rc2.php">4.6 RC2</a> This is the last test release leading up to 4.6.0, which is planned for 26th January.