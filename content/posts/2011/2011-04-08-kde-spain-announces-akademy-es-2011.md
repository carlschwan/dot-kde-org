---
title: "KDE Spain announces Akademy-es 2011"
date:    2011-04-08
authors:
  - "tsdgeos"
slug:    kde-spain-announces-akademy-es-2011
comments:
  - subject: "incredible"
    date: 2011-04-08
    body: "All these aKademy's. Absolutely incredible, rock on Spain!"
    author: "blantonv"
---
KDE Spain is organizing Akademy-es 2011, the annual meeting of KDE users and contributors in Barcelona, Spain from May 20th through May 22nd.  Read on to learn more about this year's event!
<!--break-->
<p align="center"><img src="http://kde-espana.es/akademy-es2011/banner.png"></p>

KDE Spain is organizing Akademy-es 2011, the annual meeting of KDE users and contributors in Barcelona, Spain from May 20th through May 22nd. Akademy-es is an important event in the KDE calendar. Attendance and the technical quality of papers have increased significantly during each of the previous events.

New this year, Akademy-es will take place in two different locations. The events of each day are designed to fit ideally with the surroundings. On Friday, Akademy-es will be held at the North Campus of the Polytechnic University of Catalunya, and weekend activities will be at The School of Sant Marc de Sarrià.

News has already been reported about last year's Akademy-es <a href="http://dot.kde.org/2010/06/02/akademy-es-2010-big-success">in Bilbao</a>. We are planning for this year to be an even bigger success. You can find more information at the <a href="http://kde-espana.es/akademy-es2011">Akademy-es website</a>.

<p align="center"><img src="http://dot.kde.org/sites/dot.kde.org/files/4587882654_749e5487cf.jpg" /></p>