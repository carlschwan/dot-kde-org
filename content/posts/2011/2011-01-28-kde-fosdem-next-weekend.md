---
title: "KDE at FOSDEM Next Weekend"
date:    2011-01-28
authors:
  - "jriddell"
slug:    kde-fosdem-next-weekend
comments:
  - subject: "KDevelop"
    date: 2011-01-29
    body: "I'll furthermore do a lightening talk on KDevelop. Don't forget me :)"
    author: "Milian Wolff"
---
Next weekend (5-6 February 2011) is <a href="http://www.fosdem.org">FOSDEM</a>, one of the largest gatherings of Free Software developers in the world. KDE will be in Brussels with a stall and as part of the <a href="http://www.fosdem.org/2011/schedule/track/crossdesktop_devroom">crossdesktop devroom</a>.  KDE talks will cover: education, an introduction to Qt and Qt Quick, Phonon, KDE on Windows and mentoring.  In the crossdesktop devroom, there will be other talks on topics such as application distribution and games development, which will be interesting to KDE developers too.  In the main track, there is a <a href="http://fosdem.org/2011/interview/kenneth-rohde-christiansen">talk on Qt WebKit</a>.  That's not to mention the <a href="http://fosdem.org/2011/schedule/tracks">many other tracks of talks</a> or the legendary <a href="http://www.fosdem.org/2011/beerevent">Beer Event</a>.  See you there.
