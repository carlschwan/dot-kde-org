---
title: "KDE Commit-Digest for 1st May 2011"
date:    2011-05-06
authors:
  - "vladislavb"
slug:    kde-commit-digest-1st-may-2011
comments:
  - subject: "Commit access"
    date: 2011-05-08
    body: "Thanks for the digest, awesome read!\r\n\r\nI also liked to click through to some commits, to see how certain features (the KWin optimalisations) were actually implemented. Could the links to projects.kde.org be restored? I'd love to see these back!"
    author: "vdboor"
  - subject: "OSM rendreing?"
    date: 2011-05-08
    body: "How is it different than the currently available rendering? What's the plan? any chance that we are getting vector rendering? 3D?"
    author: "hmmm"
  - subject: "Re: OSM rendering?"
    date: 2011-05-15
    body: "While the current OSM support downloads pre-rendered graphics from OSM's Mapnik servers, the new OSM rendering just fetches the path data (i.e. where streets are located, how they are called and classified etc.) and creates the visual representation of this data by itself.\r\n\r\nWhile, given the current X11-based implementation, this requires more computational power on the computer running Marble, this approach is much better for OpenGL-based rendering, and the path data is much smaller on your disk than the pre-rendered images."
    author: "majewsky"
---
In <a href="http://commit-digest.org/issues/2011-05-01/">this week's KDE Commit-Digest</a>:

<ul>
<li><a href="http://www.calligra-suite.org/">Calligra</a> sees improvements to PPT and DOC support and work on <a href="http://ingwa2.blogspot.com/2011/05/starview-metafiles-in-calligra.html">SVM support and specifications</a> amongst further bugfixing</li>
<li>Initial GSOC work on OpenStreetMap (OSM) rendering in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li><a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> sees much bugfixing, support for Bluetooth tethering and a KAuth implementation</li>
<li><a href="http://solid.kde.org/">Solid's</a> NetworkManager device interface updated to NM-0.8.2</li>
<li>Work throughout <a href="http://plasma.kde.org/">Plasma</a> and <a href="http://community.kde.org/Plasma/Plasma-Mobile">Plasma-Mobile</a> including QML Bindings for Plasma::ToolTips, a CLI tool allowing the addition of remote plasmoids and a small Remote Widgets browser along with much bugfixing</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> now supports shadows with XRender and sees a slew of further bugfixing and optimization</li>
<li>Continued work on <a href="http://skrooge.org/">Skrooge</a></li>
<li><a href="http://kaffeine.kde.org/">Kaffeine</a> switches from Xine to VLC</li>
<li><a href="http://rekonq.kde.org/">Rekonq</a> implements Opera-like feature to load favorites with Ctrl+Number</li>
<li>Work on <a href="http://rubyforge.org/projects/korundum/">QtRuby</a> 3.0</li>
<li><a href="http://userbase.kde.org/Klipper">Klipper</a> now uses <a href="https://projects.kde.org/projects/kdesupport/prison">Prison</a> for barcodes</li>
<li>Work on <a href="http://www.oxygen-icons.org/">Oxygen</a> icons</li>
<li>Middle mouse button on tab now changes the highlight color in <a href="http://kate-editor.org/">Kate</a></li>
<li>Further work on the <a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a> plasmoid</li>
<li>New Brazilian keyboard layout and bugfixing in <a href="http://ktouch.sourceforge.net/">KTouch</a></li>
<li><a href="http://sourceforge.net/p/necessitas/home/">Android-Qt</a> now has JIT support</li>
<li>Bugfixing and optimization of DrKonqi</li>
<li>Bugfixing in <a href="http://www.digikam.org/">Digikam</a>, <a href="http://www.kphotoalbum.org/">KPhotoAlbum</a>, <a href="http://en.wikipedia.org/wiki/KIO">KIO</a>, <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a> and <a href="http://kile.sourceforge.net/">Kile</a></li>
</ul>

<a href="http://commit-digest.org/issues/2011-05-01/">Read the rest of the Digest here</a>.