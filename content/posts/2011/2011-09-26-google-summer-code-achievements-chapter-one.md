---
title: "Google Summer of Code Achievements - Chapter One "
date:    2011-09-26
authors:
  - "vivek"
slug:    google-summer-code-achievements-chapter-one
---
Over the past few months, members of the KDE community mentored students as a part of the <a href="http://code.google.com/soc/">Google Summer of Code</a> (GSoC). In this annual program, students receive a stipend to write code for a Free Software project, mentored by someone from the project. With KDE participating for the seventh time, students worked on many KDE projects, some of which are already being included in KDE releases. 47 of 51 projects were finished successfully this year. This is the first article featuring students’ achievements in the Google Summer of Code program.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/GSoC2011_198x128.png" /></div>

Lydia Pintscher, one of KDE's GSoC coordinators, is pleased with the results, saying, "At KDE, we're thrilled to have been given the opportunity again to work with so many bright and enthusiastic students. We've given them a good introduction to the world of Free Software and helped them see just how much they are capable of when they put their minds to it. I obviously want all of them to stay with KDE and help us create the future. But even if some of them don't, I am sure this Summer of Code made a difference in their lives."

<h2>Accessibility</h2>
<a href="http://gpul.org/?q=en/blog/219"><strong>José Millán Soto</strong></a> worked on the qt-at-spi bridge to allow Qt programs to work with accessibility tools. In this way, KMail, Kopete and Dolphin are usable by all users. According to José, “Good communication between developers and organizing yourself well are at least as important as writing code.”

Accessibility also received a boost from <a href="http://progshock.blogspot.com/2011/08/gsoc-final-weeks-status-report.html"><strong>Adam Nash</strong></a>, who created a system to make simon (the speech recognition application) context-aware (which programs are open, which program has the active window, the active window's title, the audio input device being used). This information helps simon improve its speech recognition results by disabling certain active vocabulary words and choosing the most appropriate acoustic model. 

<h2>Semantic Capabilities</h2>
Working on a different kind of context-awareness, <a href="http://martys.typepad.com/blog/2011/06/gsoc-pim-nepomuk-and-telepathy.html"><strong>Martin Klapetek</strong></a> improved KDE’s semantic abilities by creating PIMO:Person integration into Akonadi and Nepomuk. Martin explains, “Say that PIMO:Person represents a real-world friend of yours. He has several emails, two IM accounts and three phone numbers. You also have his home address. Each of these pieces of data are represented by one NCO:PersonContact (Nepomuk Resource), or about 10 entities that belong to the same person. My project brings them together as one PIMO:Person. The automated part will connect as much as it can, and the user will be able to do the rest. A GUI displays your NCO:PersonContacts. It provides a simple way to group resources for one Person or remove resources that are associated incorrectly.”

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Hegde2.png" /><br />Fancy Bookmarking - search through browser for any bookmarked resource</div>

<strong>Phaneendra Hegde</strong> also worked with Nepomuk, creating fancy bookmarking for Konqueror and Rekonq, which allows users to link web pages to projects, tasks, people and files for an effective bookmark management system. <strong>Smit Shah</strong>’s project created support for writing back metadata changes that are supported by a file (such as ratings, comments, tags) to the file itself in addition to a plugin system to add support easily for new file formats. <strong>Srikanth Tiyyagura</strong> added tagging and resource management capabilities to Krita. Srikanth’s mentor could have been speaking to many of the GsoC students, “You worked hard, asked for help, coded hard and… did it! Awesome!”.

<strong>Trever Fischer</strong> enjoyed working with colleagues from GNOME on his project creating tight semantic integration between KDE and Zeitgeist. He got deep into the code to add desktop resources (music, conversations, places, files, phone calls) to browser history.

Please visit <a href="http://dot.kde.org/2011/09/27/google-summer-code-achievements-chapter-two">Chapter Two</a> and <a href="http://dot.kde.org/2011/09/28/google-summer-code-achievements-chapter-three">Chapter Three</a> of the KDE Google Summer of Code Achievements.

For a description of all GSoC (and Season of KDE) student projects, please visit the <a href="http://community.kde.org/GSoC/2011/StatusReports">2011 status reports page</a>.