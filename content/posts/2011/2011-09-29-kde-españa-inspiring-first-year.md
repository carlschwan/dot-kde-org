---
title: "KDE Espa\u00f1a - An Inspiring First Year"
date:    2011-09-29
authors:
  - "kallecarl"
slug:    kde-españa-inspiring-first-year
---
At Akademy 2010, it was agreed that KDE España would be the official representative of KDE in Spain. One of the terms of the agreement is that KDE España reports on their performance annually. Recently, they sent their report to the Board of KDE e.V. They've been busy.
<!--break-->
<h2>The work that was done</h2>
The goal of KDE España is to stimulate the development and use of KDE software in Spain. To this end, the 30 active members of KDE España have organized release parties and taken part in LAN parties. They've recruited sponsors, presented on KDE and KDE software regularly throughout Spain, and talked on Qt, KDEEdu, translations, mobile programming and other topics related to KDE and other FOSS projects. KDE España participated in Software Freedom Day in Barcelona and presented at the <a href="http://2010.libresoftwareworldconference.com/index.php/en">Libre Software World Conference</a> in Málaga. Their big event for 2011 was <a href="http://kde-espana.es/akademy-es2011/anuncio.php">Akademy-es</a> in Barcelona. Over 3 days in May, there were <a href="http://kde-espana.es/akademy-es2011/material.php">21 talks</a> (in Spanish) and about <strong>80 registered attendees</strong>.

<h2>The board</h2>
As of the end of July 2011, the elected Board of KDE España is:
<ul>
<li>Albert Astals Cid, President</li>
<li>Alejandro Fiestas Olivares, Vice President</li>
<li>José Millán Soto, Treasurer</li>
<li>Aleix Pol i Gonzàlez, Secretary</li>
</ul>

The KDE España Board members expressed their appreciation for the contributions of outgoing Vice President, Rafael Fernandez Lopez over the previous year.

<h2>Awesomeness in Spain</h2>
KDE-España has represented KDE well in Spain, and, even more, has provided inspiration for other communities. They haven't just talked; they have taken action. KDE España is an example of Margaret Mead's observation — <em>"Never doubt that a small group of thoughtful, committed citizens can change the world. Indeed, it is the only thing that ever has."</em>

Follow the activities of KDE España on <a href="http://planet.kde-espana.es/">their member planet</a>. There is also a <a href="http://planetkde.org/es/">planet of Spanish-speaking KDE contributors</a>.