---
title: "KDE Commit-Digest for 21st August 2011"
date:    2011-08-26
authors:
  - "mrybczyn"
slug:    kde-commit-digest-21st-august-2011
comments:
  - subject: "It's a great thing someone"
    date: 2011-08-30
    body: "It's a great thing someone finds time to write these blog posts!\r\n\r\nFrom this post I found out about ownCloud, which I find very useful! Already translated it to my language, so this post brought at least one contributor to the KDE community :)"
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-08-21/">this week's KDE Commit-Digest</a>, Shaun Reich comments on his work to port KDM to Plasma. In addition, the change list for this week includes:

             <ul><li>Drag and drop for images and URLs is implemented in <a href="http://www.calligra-suite.org/">Calligra</a> Stage and Flow.</li>
<li>The user interface of Apper is improved.</li>
<li>Supernovae are introduced to <a href="http://edu.kde.org/kstars/">KStars</a> as small orange '+' signs on the skymap.</li>
<li>A new Panorama plugin in <a href="http://www.kipi-plugins.org/">Kipi-plugins</a> is ready for use.</li>
<li>As in previous weeks, <a href="http://owncloud.org">ownCloud</a> sees multiple updates, including calendar improvements, shared file search implementation and others.</li>
<li>The popup menu for drag and drop operations has been restored in <a href="http://userbase.kde.org/Konsole">Konsole</a>.</li>
<li><a href="http://userbase.kde.org/Kate">Kate</a> sees updates in syntax highlighting, this week for System Verilog and GNU M4.</li>
<li>The user interface has been improved in <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>.</li>
<li>Speed optimizations have been made in multiple modules, including <a href="http://edu.kde.org/marble/">Marble</a> and <a href="http://community.kde.org/KDE_PIM">KDEPIM</a> libs.</li>
<li>Bugfixes in <a href="http://kdevelop.org/">KDevelop</a>, <a href="http://edu.kde.org/kstars/">KStars</a>, <a href="http://konsole.kde.org/">Konsole</a>, <a href="http://userbase.kde.org/Kate">Kate</a>, <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, and <a href="http://www.calligra-suite.org/">Calligra</a>.</li>
</ul>

               <a href="http://commit-digest.org/issues/2011-08-21/">Read the rest of the Digest here</a>.
<!--break-->
