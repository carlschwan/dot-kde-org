---
title: "Desktop Summit Community Keynote Interview with Stuart Jarvis"
date:    2011-07-16
authors:
  - "sealne"
slug:    desktop-summit-community-keynote-interview-stuart-jarvis
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 150px;"><img src="/sites/dot.kde.org/files/stuart.jpg"><br /> Stuart Jarvis</div>

<p>Continuing the series of interviews with Keynote speakers at the <a href="https://www.desktopsummit.org/">Desktop Summit</a>, happening in just 3 weeks time, we now have Stuart Jarvis.</p>

<p>Stuart has been supplying editorial support and an active face for KDE, through the Dot and other outlets, as a member of the KDE Promotions Team. However keeping up with the flow of information can be tough, especially when at the same time you're working on a PhD in geochemistry. Free software collaboration doesn't just need coders for technical nuts and bolts; it also needs people marketing and communicating effectively so that potential users can learn about it.</p>

<p>Read the <a href="https://www.desktopsummit.org/ interviews/stuart-jarvis">full interview</a>.</p>

<p>The Desktop Summit is free to attend but you must <a href="https://www.desktopsummit.org/register">Register</a>.</p>
<!--break-->
