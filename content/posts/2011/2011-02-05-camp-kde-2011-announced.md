---
title: "Camp KDE 2011 Announced"
date:    2011-02-05
authors:
  - "seele"
slug:    camp-kde-2011-announced
comments:
  - subject: "Small correction"
    date: 2011-02-06
    body: "Camp KDE is held annually in <em>North America</em>, not just the United States. The announcement at camp.kde.org makes this same mistake."
    author: "Parker Coates"
  - subject: "Thanks"
    date: 2011-02-07
    body: "Fixed here."
    author: "Stuart Jarvis"
---
We are excited to announce <a href="http://camp.kde.org">Camp KDE 2011</a> which will be held April 4 and 5, 2011 in San Francisco, California at <a href="http://www.jdvhotels.com/hotels/kabuki/">the Hotel Kabuki</a>. Camp KDE is co-located this year with the <a href="http://events.linuxfoundation.org/events/collaboration-summit">Linux Foundation's Collaboration Summit</a> which takes place April 6 and 7.

Held annually in North America, Camp KDE provides a regional opportunity for KDE contributors and enthusiasts to gather and share their KDE experiences. Co-location with the Collaboration Summit will allow Camp KDE attendees a unique opportunity to learn from and share their experiences with members of many other successful open source software projects.

"Having Camp KDE co-located with The Linux Foundation Collaboration Summit helps to bring even more of the Linux community's key stakeholders together in one place," said Amanda McPherson, vice president of marketing and developer programs. "The KDE community represents contributors from around the globe and we're excited for the cross-event collaboration opportunities that will help advance Linux in the year ahead."

Registration and more information on the event is available on <a href="http://camp.kde.org/">the conference website</a>. Proposals for talks are now open and will be accepted until March 2, 2011.

<h2>About KDE and the KDE e.V.</h2>
<a href="http://kde.org/">KDE</a> is an international technology team dedicated to creating a free and user-friendly computing experience, offering an advanced graphical desktop, a wide variety of applications for communication, work, education and entertainment and a platform for building new applications easily.

<a href="http://ev.kde.org/">KDE e.V.</a> is the organization that supports the growth of the KDE community. Its mission statement -- to promote and distribute Free Desktop software -- is provided through legal, financial and organizational support for the KDE community.

<h2>About the Linux Foundation and the Collaboration Summit</h2>
<a href="http://www.linuxfoundation.org/">The Linux Foundation</a> promotes, protects and advances Linux by marshaling the resources of its members and the open source development community to ensure Linux remains free and technically advanced.

The Linux Foundation Collaboration Summit is an exclusive, invitation-only summit gathering core kernel developers, distribution maintainers, ISVs, end users, system vendors and other community organizations for plenary sessions and workgroup meetings to meet face-to-face to tackle and solve the most pressing issues facing Linux today.