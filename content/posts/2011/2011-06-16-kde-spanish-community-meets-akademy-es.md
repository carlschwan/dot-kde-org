---
title: "KDE Spanish Community Meets at Akademy-es"
date:    2011-06-16
authors:
  - "baltolkien"
slug:    kde-spanish-community-meets-akademy-es
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy-es1sml.jpg" /><br />A talk being delivered</div>Following the success of last years' event, the <a href="http://www.kde-espana.es/">Spanish KDE community</a> again held its national Akademy event, <a href="http://www.kde-espana.es/akademy-es2011/">Akademy-es 2011</a>, between the 20th and the 22nd of May 2011 in Barcelona. The event was sponsored by Google and Qt/Nokia and was supported by the Linux and Todo-Linux magazines. KDE enthusiasts from all over the country gathered to discuss free software and KDE.
<!--break-->
This year the event had two different hosts: the Polytechnical University of Catalonia (UPC) made its Master Classroom available for Friday's presentations, whereas the School of Sant Marc-Sarrià hosted us on Saturday and Sunday.

As usual in these events, there was a classic mixture of presentations, coffee, hallway conversations, social meals and, most importantly, a wide exchange of ideas to recharge batteries for future work in the KDE community.

<h2>Friday</h2>

Friday began in the UPC with the event's opening address by the president of KDE España, Albert Astals Cid.

After this brief introduction, several talks followed, including:

<ul><li><i>Translate KDE, a great way to collaborate with the project</i>, by cat >> KDE</li>
<li><i>Contributing to KDE</i>, by Aleix Pol, in which he explained how you can contribute to KDE if you're not a programmer, as well as how to submit patches if you are a developer</li>
<li><i>KDE Telepathy</i>, the way to a social desktop, by Alex Fiestas, who showcased a spectacular project that can revolutionize our desktop</li>
<li><i>Learn to write code with KDE and a potato</i>, by Albert Astals Cid, which was a very informative talk showing the guts of a KDE program to the attendees</li></ul>

The event then gravitated toward its more social side with a supper at Restaurant Tascaivins, where we enjoyed typical Catalan food, followed by good beer at a nearby pub. Before going to bed, most of us got to see <a herf="http://en.wikipedia.org/wiki/2011_Spanish_protests">the large camp of protesters</a> in Catalonia Square.

<h2>Saturday</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy-es2sml.jpg" /><br />It's dinner time!</div>Saturday started early in the morning with several interesting presentations such as:

<ul><li><i>The History of KDE</i>, by Antonio Larrosa, who enlightened us with an interesting review of the evolution of the KDE Community since its first days</li>
<li><i>The Plasma 4.6 Desktop</i>, by well known blogger Baltasar Ortega, who gave us his vision of KDE from the point of view of a user</li>
<li><i>Why KDE?</i>, by Aleix Pol, a brief summary of KDE projects with a passionate defense of Free Software which focused on the value of participating rather than just using it</li>
<li><i>Accesibility in KDE</i>, by José Millán, who showcased the work on a project for disabled people in which he is taking part</li></ul>

When the talks finished, almost all the attendees shared a nice lunch at Sant Marc's.
The afternoon session then started with the following presentations:

<ul><li><i>Debugging with Valgrind</i>, by Albert Astals, where Albert showed us how this application can help developers make more reliable programs</li>
<li><i>The half/rolling release cycle & the bundles system</i>, by Manuel Tortosa, an excellent overview on how developers are using Chakra</li>
<li><i>KDE Forum</i>, by Alex Fiestas, an interesting presentation where all the attendees discovered and discussed the newest KDE technologies and learned the meaning of some buzzwords</li>
<li><i>Introduction to QML</i>, by Albert Astals, a brief demonstration of how to create an application quickly with QML</li></ul>

The social event that evening took place in a restaurant near Catalonia Square, just in the center of Barcelona, called El Mussol. During dinner we shared stories about KDE, Free Software and life in general, which enriched dinner more than any seasoning.

<h2>Sunday</h2>

The last day of Akademy-es 2011 started with some enlightening talks:

<ul><li>The first presentation was <i>Making my PFC (Projecte Fi de Carrera, or Final Degree Project in English) with KDE</i>, by Aleix Pol, who explained how he used free software to go forward in his education</li>
<li>Then Alex Fiestas talked a bit about Kiosk by showing what it does and explaining how we can take advantage of it</li>
<li>Miquel Sabaté explained why he started to contribute to KDevelop Ruby support and what its state is</li>
<li>Albert Astals demonstrated the different wonders hidden within Okular</li></ul>

After a brief break, Baltasar Ortega gave a talk focused on KDE Edu in which he presented the twenty KDE Edu applications, and finally, Albert and Aleix explained what KDE España is doing and where its associates think it should go.

After dinner, we had the last talk titled <i>Introduction to the PyGame and PyOpenGL libraries</i>, by Aarón Negrín, an interesting demo about how to draw and interact with your computer by using both of these technologies.

All in all it was an awesome experience, and after it we are looking forward to more KDE events!

See you at Akademy-es 2012!

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -353px; width: 702px; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/Akademy-es3cropped.jpg" /></a><br />The obligatory group photo</div>