---
title: "Desktop Summit CFP and Registration open"
date:    2011-02-28
authors:
  - "sealne"
slug:    desktop-summit-cfp-and-registration-open
comments:
  - subject: "Looking forward"
    date: 2011-03-11
    body: "I'm looking forward to it. Making a special trip and everything. The list of things to be discussed are interesting. Can't wait to hear the feedback on it. --<A HREF=\"http://articlicious.com/232023/direct-tv-texas-promotion-tourism/\">Texas</A>"
    author: "newthing"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/desktopsummit_logo.png" width="266" height="182" align="right" />

The <a href="https://www.desktopsummit.org/">Desktop Summit 2011</a> is a joint conference organised by the <a href="http://foundation.gnome.org/">GNOME</a> and <a href="http://ev.kde.org/">KDE</a> communities, in Berlin, Germany from the 6th August 2011 to the 12th August 2011. Held annually in cities around Europe, GUADEC and Akademy are the world's largest gatherings of those involved with the free desktop or mobile user interfaces. Developers, artists, translators, community organisers, users, and representatives from government, education, and businesses and anyone else who shares an interest are welcome. GNOME and KDE are Free Software communities that drive the user interfaces of many Linux-powered devices, ranging from smartphones to laptops, or personal media centers. This year, for the second time, both communities have decided to organise a single, joint conference. We anticipate over a thousand participants, covering both projects as well as related technologies.

<a href="https://www.desktopsummit.org/register">Registration</a> for the event is now open and the <a href="https://www.desktopsummit.org/cfp">Call for Participation</a> is available.

<!--break-->

The Desktop Summit Team are excited about the opportunity to have a conference with an unprecedented breadth of presentations about the Free Software Desktop, and are looking forward to your registrations and submissions.

Of particular interest for the Desktop Summit are presentations and lightning talks on the following topics:

<ul>
<li>Closer collaboration between GNOME, KDE and related projects</li>
<li>GNOME, KDE and the mobile platform</li>
<li>The free desktops and social networks</li>
<li>Search, meta-data and the semantic desktop</li>
<li>The Desktop and the OS</li>
<li>Relationship with distributions and platforms</li>
<li>Optimizing power, memory and disk I/O usage</li>
<li>Designing and writing applications with strong user interfaces</li>
<li>Supporting non-technical contributions (e.g. documentation, visual design, marketing, project management, etc.) and attracting new community members</li>
<li>GNOME beyond the 3.0 release: the GNOME OS</li>
<li>Powerful foundations, elegant interfaces: Presentations on improvements in KDE applications</li>
<li>Government use of the free desktops; Free Software and non-governmental organizations</li>
<li>Attention for Free Software software in education and participation of students</li>
</ul>

Submissions that do not fit into these categories are welcome too, provided that they are relevant for the GNOME or KDE stacks, free desktops or mobile user interfaces in general. We'd like to invite submissions not only from the organizing communities themselves but also from users of free desktops, from projects and organizations related to GNOME and KDE as well as contributors involved in technologies they are based on. Submissions with cross-desktop relevance are preferred. We are looking both for presentations and lightning talks.

The Desktop Summit Call for Participation has the following timeline:

<ul>
<li>March 25th: Deadline for submission of abstracts</li>
<li>April 20th: Notification of speakers</li>
<li>August 6th-12th: The conference takes place in Berlin, Germany</li>
</ul>

Please submit your proposal before March 25th through the <a href="https://www.desktopsummit.org/submit">online paper submission system</a>. Papers will be reviewed by the program committee between March 26th and April 20th.

If you are interested in organizing a workshop session, a BoF or a project room please wait for a later separate call for participation.

The program committee is looking forward to your suggestions and submissions for participating in this year's exciting edition of the Desktop Summit. In case of questions regarding the CFP feel free to talk to the <a href="mailto:ds-talks@desktopsummit.org">programme committee</a>. For general questions on the conference please consult the <a href="https://www.desktopsummit.org/">Desktop Summit web site</a> or contact the <a href="mailto:ds-team@desktopsummit.org">Desktop Summit organising team</a>.

