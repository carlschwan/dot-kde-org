---
title: "conf.kde.in Announces Talks, Keynotes and Registration"
date:    2011-02-08
authors:
  - "jriddell"
slug:    confkdein-announces-talks-keynotes-and-registration
comments:
  - subject: "* Awesome! I am attending..."
    date: 2011-02-08
    body: "* Awesome! I am attending..."
    author: "tazz"
  - subject: "Off-Topic: Meego"
    date: 2011-02-10
    body: "Kind off-topic: Is there any kind of petition to Nokia for please not dumping Meego? I have postponed buying a new phone, waiting for Nokias Meego phones. I will definitely buy neither a Windows phone nor another Symbian phone. It would be really a shame if they made such a stupid decision."
    author: "mark"
  - subject: "Meego down the drain?"
    date: 2011-02-10
    body: "Me thinks the same. I have an N900 and really like the device despite it being actually a real \"brick\" ;-)\r\nI would definetely have been buying the new Meego phone.\r\n\r\nAnyway. Windows mobile still has zero marketshare for a good reason: It's barely usable.\r\n\r\nIphone and Android are the only ones left now? Nokia should really keep Meego as it can now be the only real alternative to Apple and Google.\r\n"
    author: "thomas"
---
<img src="http://kde.in/wp-content/themes/KDE4WP/images/kde-in-logo.png" width="414" height="101" align="right" />
There is only a month to go before the first KDE and Qt conference in India opens.  The event will be headlined by three <a href="http://kde.in/conf/keynotes/">keynotes speakers</a> talking on the effects of technology on culture, the law and what makes our community tick.  <a href="http://kde.in/conf/talks/">Talks and workshops</a> have been announced and <a href="http://kde.in/conf/register/">registration is open</a> for anyone planning to attend.  Read on for details.
<!--break-->
<h2>The Keynotes</h2>

<img src="http://kde.in/wp-content/themes/KDE4WP/images/speakers/runa.jpg" align="left" /> Runa Bhattacharjee is a translator for KDE and other projects who works for Red Hat.  Her talk on Traditional Hues and Technology will look at the way culture is being changed by computers and the significant impact open source is having on this process.
<br clear="all" />

<img src="http://kde.in/wp-content/themes/KDE4WP/images/speakers/adriaan.jpg" align="left" />Adriaan de Groot is a long time KDE contributor and e.V. board member.  He will be talking on Bits of Legal which looks at contributor agreements, licensing, trademarks and other legal bits we have to care about.
<br clear="all" />

<img src="http://kde.in/wp-content/themes/KDE4WP/images/speakers/lydia.jpg" align="left" />Lydia Pintscher will be asking what makes the KDE community tick.  Having herded many KDE cats in her time, she will give some insights into how KDE is run and how to become a contributor.
<br clear="all" />

<h2>Talks and Workshops</h2>

The conference has <a href="http://kde.in/conf/talks/">an impressive list of talks</a> from some of KDE’s top contributors.  Topics covered include programming Qt in Python, daily package building in Launchpad, Calligra, Qt scripting, Qt Quick and distributions Kubuntu and openSUSE.  There will be several workshops to learn skills with the experts.

<h2>Registration</h2>

<a href="http://kde.in/conf/register/">Registration is now open</a> There are three registration rates for students (&#8377;500), teachers (&#8377;700) and others (&#8377;900).  Register quick, prices go up after 25th February.

<a href="http://kde.in/conf/">conf.kde.in 2011</a> is 9 - 13 March and held at R.V College of Engineering in Bengaluru (Bangalore).