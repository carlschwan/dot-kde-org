---
title: "KDE Commit-Digest for 2nd January 2011"
date:    2011-02-04
authors:
  - "dannya"
slug:    kde-commit-digest-2nd-january-2011
comments:
  - subject: "Thanks for another commit-digest!"
    date: 2011-02-04
    body: "I noticed that diffs do not seem to be working for git repos, just wanted to point it out in case no-one noticed yet.\r\n\r\nKeep up the good work, and already flattered it too :)"
    author: "ianjo"
  - subject: "Definitely noticed, just need"
    date: 2011-02-07
    body: "Definitely noticed, just need to put out all the other Git-related fires first! :)\r\n\r\nDanny"
    author: "dannya"
---
In <a href="http://commit-digest.org/issues/2011-01-02/">this week's KDE Commit-Digest</a>:

<ul>
<li>Bug fixes in <a href="http://www.digikam.org/">Digikam</a>, <a href="http://plasma.kde.org">Plasma</a> and its applets, <a href="http://kate-editor.org/">Kate</a>, <a href="http://solid.kde.org">Solid</a>, and throughout <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://www.calligra-suite.org/">Calligra</a>, and the Oxygen style</li>
<li>Fixes in <a href="http://ktorrent.org/">libktorrent</a> including fixes to work properly with D-Link DIR 635 routers</li>
<li>Runtime switching between the new OpenGL and QPainter backends in preparation for a "Beta" OpenGL rendering mode in <a href="http://edu.kde.org/kstars/">KStars</a> for KDE 4.6</li>
<li>Themable cross-hair and compass in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>Work on new <a href="http://kde-apps.org/content/show.php/Cirkuit?content=107098">Cirkuit</a> backends</li>
<li>Work on the online game <a href="http://kde-apps.org/content/show.php?content=20534">Knights</a></li>
<li>Work on the Python parser in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Implementation of undo/redo across files in <a href="http://www.digikam.org/">Digikam</a></li>
<li>"Net Usershare" support added to KSambaShare</li>
<li>Work on a new <a href="http://liveblue.wordpress.com/2010/12/28/experimental-kwelcomedialog/">KWelcomeWidget</a> in Plasma</li>
<li>New usb:// subsystem implemented in <a href="http://solid.kde.org/">Solid</a></li>
<li>History visited count added to <a href="http://rekonq.sourceforge.net/">Rekonq</a></li>
<li>Work on filters in <a href="http://krita.org/">Krita</a></li>
<li>Work on DVB support in <a href="http://kaffeine.kde.org/">Kaffeine</a></li>
<li>Features and optimizations throughout <a href="http://smb4k.berlios.de/">Smb4k</a></li>
<li>Nuno Pinheiro does major work on all 22x22 mimetype icons in the <a href="http://www.oxygen-icons.org/">Oxygen</a> icon set.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-01-02/">Read the rest of the Digest here</a>.
<!--break-->
