---
title: "KDE Commit-Digest for 2nd October 2011"
date:    2011-10-07
authors:
  - "mrybczyn"
slug:    kde-commit-digest-2nd-october-2011
comments:
  - subject: "thanks!"
    date: 2011-10-07
    body: "As I have no idea where else to ask: what's up with the mailing list archives? There has been no updates since the 2nd!"
    author: ""
  - subject: "which one?"
    date: 2011-10-08
    body: "What mailing list are you referring to?"
    author: "kallecarl"
  - subject: "All of them: lists.kde.org"
    date: 2011-10-08
    body: "All of them: lists.kde.org has not been updated for quite some days, now."
    author: ""
---
<p>In <a href="http://commit-digest.org/issues/2011-10-02/">this week's KDE  Commit-Digest</a>:</p>

<ul><li>Group By Time feature available in <a  href="http://www.digikam.org/">Digikam</a></li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> gets Web Report  Element and incremental backup in Krita</li>
<li>Introduction of the KSecret tool</li>
<li>Optimizations in KDE-Base</li>
<li>Bugfixes in in password saving and HTTP authentication caching in <a  href="http://api.kde.org/4.7-api/kdelibs-apidocs/">KDE-Libs</a>, <a  href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>, <a  href="http://kile.sourceforge.net/">Kile</a> and others.</li>
</ul>

<p><a href="http://commit-digest.org/issues/2011-10-02/">Read  the rest of the Digest here</a>.</p>
<!--break-->
