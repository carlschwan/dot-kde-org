---
title: "KDE Commit-Digest for 28th August 2011"
date:    2011-09-02
authors:
  - "mrybczyn"
slug:    kde-commit-digest-28th-august-2011
comments:
  - subject: "thanks for the digest"
    date: 2011-09-14
    body: "and the infos in the summary. There are really some exciting things ahead. \r\n\r\nI like the addition of how long a bug existed in the database, why do only a few bugs have that info?\r\n\r\nhugs!"
    author: ""
  - subject: "not all fixes are directly bug-related"
    date: 2011-09-14
    body: "clicking a few of the revision # links shows fixes that are not related to reported bugs."
    author: "kallecarl"
---
In <a href="http://commit-digest.org/issues/2011-08-28/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.digikam.org/">Digikam</a> now offers deferred scanning of files</li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> has seen work in the Print Wizard, optimization of sky drawing and bugfixes</li>
<li>KNewstuff3 supports GnuPG2</li>

<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, among other improvements, SVG support has been added to the flake library</li>
<li><a href="http://rekonq.kde.org/">Rekonq</a> can restore tab history when restoring tabs/sessions</li>
<li>There are various improvements around the popup icon in <a href="http://userbase.kde.org/Plasma/Public_Transport">Public Transport</a></li>
<li>Performance optimizations have taken place in <a href="http://www.kde.org/workspaces/plasmadesktop/">Plasma</a>, <a href="http://userbase.kde.org/KWin">KWin</a> and <a href="http://community.kde.org/KDE_PIM">KDEPIM</a></li>
<li>There are multiple updates in different areas of <a href="http://owncloud.org">ownCloud</a></li>
<li>Some of the GSoC contributions have been merged, including the improvement of user experience in <a href="http://userbase.kde.org/Kiten">Kiten</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-08-28/">Read the rest of the Digest here</a>.
<!--break-->
