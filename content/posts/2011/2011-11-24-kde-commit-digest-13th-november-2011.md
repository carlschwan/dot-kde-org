---
title: "KDE Commit-Digest for 13th November 2011"
date:    2011-11-24
authors:
  - "mrybczyn"
slug:    kde-commit-digest-13th-november-2011
---
In <a href="http://commit-digest.org/issues/2011-11-13/">this week's KDE Commit-Digest</a> we have included a feature on updates in Plasma Active from Marco Martin. 
 
In addition, the usual list of updates: 
 
 <ul><li>Window Thumbnail support for QML in <a href="http://kde.org/workspaces/">Plasma Workspaces</a></li> 
<li>Mouse cursor size is now configurable, Oxygen mouse cursors (pseudo) scalable</li> 
<li>Basic routing activity added in <a href="http://edu.kde.org/marble/">Marble</a></li> 
<li>Import of internal libraw 0.14.3 to <a href="http://www.kipi-plugins.org/">libkdcraw</a>, with support of new cameras</li> 
<li>Porting of gamma table editing finished in <a href="http://techbase.kde.org/Projects/Kooka">Kooka</a></li> 
<li>Final border refactoring, initial support for filtered frame layers and bugfixes in <a href="http://www.calligra-suite.org/">Calligra</a></li> 
<li>Multiple crashes fixed.</li> 
</ul> 
 
 <a href="http://commit-digest.org/issues/2011-11-13/">Read the rest of the Digest here</a>.