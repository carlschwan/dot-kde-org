---
title: "KDE Commit-Digest for 9th October 2011"
date:    2011-10-14
authors:
  - "mrybczyn"
slug:    kde-commit-digest-9th-october-2011
---
In <a href="http://commit-digest.org/issues/2011-10-09/">this week's KDE Commit-Digest</a>:

<ul><li>New features in <a href="http://userbase.kde.org/KMail">KMail</a>: renaming attachments, disabling filters</li>
<li><a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy</a> gets <a href="http://userbase.kde.org/KDE_Wallet_Manager">KWallet</a> support for passwords, integration of global presence into kded-modue</li>
<li>Rewritten array classes in <a href="http://userbase.kde.org/Okteta">Okteta</a></li>

<li>Improved reliability of session closing in <a href="http://userbase.kde.org/Konsole">Konsole</a></li>
<li>Fix of broken root certificates in <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kio/html/classKSSL.html">KDELibs KSSL</a></li>
<li>First version of a QML screen locker using KGreeter</li>
<li>Bug fixes in <a href="http://ktorrent.org/">KTorrent</a>, <a href="http://www.digikam.org/">digiKam</a>, <a href="http://community.kde.org/Solid/Projects/BlueDevil">BlueDevil</a>, and <a href="http://www.calligra-suite.org/">Calligra</a></li>
</ul>

<a href="http://commit-digest.org/issues/2011-10-09/">Read the rest of the Digest here</a>.
<!--break-->
