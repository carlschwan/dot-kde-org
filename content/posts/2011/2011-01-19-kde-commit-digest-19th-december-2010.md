---
title: "KDE Commit-Digest for 19th December 2010"
date:    2011-01-19
authors:
  - "dannya"
slug:    kde-commit-digest-19th-december-2010
---

In <a href="http://commit-digest.org/issues/2010-12-19/">this week's KDE Commit-Digest</a>:

              <ul><li>The intense work throughout <a href="http://pim.kde.org/">KDE-PIM</a> and <a href="http://pim.kde.org/akonadi/">Akonadi</a> continues</li>
<li>Nuno Pinheiro continues his massive updating of the <a href="http://www.oxygen-icons.org/">Oxygen</a> icon set, this time with 32x32 icons</li>
<li>Many bugs fixed in <a href="http://phonon.kde.org/">Phonon</a> due to the extension of a hardware database</li>
<li>Simon Edwards introduces the "first big <a href="http://techbase.kde.org/Development/Languages/Python">PyKDE4</a> update for the coming KDE SC 4.6."</li>
<li><a href="http://www.kde.org/applications/graphics/ksnapshot/">KSnapshot</a> is ported to Windows</li>
<li><a href="http://www.digikam.org/">digiKam</a> gains an image history feature</li>
<li>The KWebkitPart gains automatic ad filtering</li>
<li>Fixes in the NFS:// KIOSlave</li>
<li>Bug fixes and optimizations in <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://edu.kde.org/kalgebra/">KAlgebra</a>, <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://edu.kde.org/marble/">Marble</a>, <a href="http://plasma.kde.org/">Plasma</a>, <a href="http://koffice.org/">KOffice</a>/<a href="http://www.calligra-suite.org/">Calligra</a>, and the new UDev back-end for <a href="http://solid.kde.org/">Solid</a></li>
<li>Work on <a href="http://www.koffice.org/krita/">Krita's</a> Assistants amongst other things</li>
<li>A new <a href="http://kde-apps.org/content/show.php/PublicTransport-Runner?content=136304&PHPSESSID=dae7f80808c498c1ef3c1be3511aa529">Public Transport Runner</a> in Plasma</li>
<li>HAL is officially retired for KDE software on Linux</li>
<li><a href="https://projects.kde.org/projects/playground/artwork/oxygen-gtk">Oxygen-gtk</a> is re-licensed under the LGPL v2 or later</li>
<li><a href="http://tellico-project.org/tellico-232-released">Tellico 2.3.2</a> is tagged</li>
<li><a href="http://techbase.kde.org/Projects/Aki">Aki and AkiIRC</a> move to Git.</li>
</ul>

                <a href="http://commit-digest.org/issues/2010-12-19/">Read the rest of the Digest here</a>.
