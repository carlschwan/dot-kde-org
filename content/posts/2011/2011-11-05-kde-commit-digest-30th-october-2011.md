---
title: "KDE Commit-Digest for 30th October 2011"
date:    2011-11-05
authors:
  - "mrybczyn"
slug:    kde-commit-digest-30th-october-2011
comments:
  - subject: "Kontact"
    date: 2011-11-06
    body: "When is the bug going to be fixed concerning contacts in the address book that will not display correctly ?  The contacts listed in the address book are not listed alphabetically as they should be. It takes several clicks on the Name panel in order for Kontact to sort them correctly. It is very annoying. "
    author: ""
  - subject: "If I am not mistaken I"
    date: 2011-11-07
    body: "If I am not mistaken I believe this has been addressed already and should be fixed in the next release.\r\n\r\nPlease note that the Digest does not list all updates to the KDE codebase, but only those which we think fall into our criteria. Sometimes we miss things, and there are over a thousand minor fixes and small updates that we don't include in each digest.\r\n\r\nThe criteria can be found here: \r\nhttp://community.kde.org/Commit_Digest/Guidelines"
    author: ""
  - subject: "bug #?"
    date: 2011-11-07
    body: "It's annoying when people don't provide enough information, and cause unnecessary effort. You want someone to fix _your_ bug and give you a status update, and this is the most you're willing to put forth? \r\n\r\nAs of right now, there are no bugs for Kontact contacts + name or display or alphabetical.\r\n\r\nAre you on the bug notify list? Do you have the bug ID number?\r\n\r\nActually, it's not annoying...it's amusing. Who makes your breakfast?"
    author: "kallecarl"
  - subject: "bug #"
    date: 2011-11-11
    body: "I make my own breakfast thank you very much.\r\nI did file a KDE Bug report on this. What that Bug # is, I don't remember. But it was filed. I got  a response back stating to me that this will addressed at some future time. Aand tht was the end of it. Uhh What ??? Can you be more specific as to when ?  Like 2013 or 2014 ?  If I can find it, I'll post t here. \r\n\r\nIf I actually knew how to program in Linux, I would take a stab at fixing it. But since I don't and am only a USER who loves Linux and won't go back MicroCrap, then I feel I have a right to ask when a certain bug will be addressed. It bothers me to no end that distros are making themselves look very pretty when they should be fixing the annoying bugs that won't necessarily crash your system, but make it kind of frustrating to use an application. If it doesn't do what it used to do, how did it get past quality control ?  I guess I am old school. I wouldn't put out a new and improved product if it broke things that used to work before but hey, it's okay, we'll fix that later, but wait, it sure looks pretty don't it ?  \r\n\r\nBTW, I am an advocate of Linux. I actually have gotten other people to use Linux. And believe it or not, an eighty year old mother in law that loves using it. "
    author: ""
  - subject: "Kontact"
    date: 2011-11-11
    body: "I make my own breakfast thank you very much.\r\nI did file a KDE Bug report on this. What that Bug # is, I don't remember. But it was filed. I got  a response back stating to me that this will addressed at some future time. Aand tht was the end of it. Uhh What ??? Can you be more specific as to when ?  Like 2013 or 2014 ?  If I can find it, I'll post t here. \r\n\r\nIf I actually knew how to program in Linux, I would take a stab at fixing it. But since I don't and am only a USER who loves Linux and won't go back MicroCrap, then I feel I have a right to ask when a certain bug will be addressed. It bothers me to no end that distros are making themselves look very pretty when they should be fixing the annoying bugs that won't necessarily crash your system, but make it kind of frustrating to use an application. If it doesn't do what it used to do, how did it get past quality control ?  I guess I am old school. I wouldn't put out a new and improved product if it broke things that used to work before but hey, it's okay, we'll fix that later, but wait, it sure looks pretty don't it ?  \r\n\r\nBTW, I am an advocate of Linux. I actually have gotten other people to use Linux. And believe it or not, an eighty year old mother in law that loves using it. "
    author: ""
  - subject: "getting more information"
    date: 2011-11-12
    body: "Okay, point taken. I apologize for the snarky comment.\r\n\r\nYou have the right to ask anything you want. You don't have the right to an answer. Particularly when your request puts a substantial (impossible) burden on anyone who might be able to answer.\r\n\r\nYou filed a bug and got a canned answer that it will be addressed at some future time. That would indicate that you are on record as the reporter.\r\n\r\nNow you want someone to tell you when a certain bug will be fixed...without any information about what that certain bug is. A bug number is _required_. When you comment here anonymously, do you really expect that someone will be aware of your \"certain bug\"? Do you still have the email reply to your bug report? Can you go to bugs.kde.org and search for the bug with your name as the reporter?\r\n\r\nPlease consider for a moment what it might take to fix a bug. Is it a misplaced semi-colon? Is it part of a future release? Predicting when a particular bug will get fixed is like predicting when a certain leaf will fall from a tree in autumn. It's likely, but not certain, to fall sometime. And it may fall anytime in a period of several months. Fixing a bug is different from cleaning up a coffee spill.\r\n\r\nPerhaps your problem is not even a bug. Rather it is an issue that would best be resolved in a forum. Have you posted anything in any appropriate forums?\r\n\r\nYou go on to complain about being bothered by things that you made up (\"...bothers me to no end that distros are making themselves look very pretty when they should be fixing the annoying bugs...\"). This kind of comment makes it less likely that any developer is going to go to the trouble that you expect. It displays a serious lack of awareness of what developers are doing. With this comment, you have insulted the people who make KDE look pretty (who have nothing to do with fixing bugs) AND the people who fix bugs (who have little to do with making things pretty).\r\n\r\nWas there some reason for posting the exact comment three times?  \r\n\r\nGenerally, people who show some inclination toward self-sufficiency--contributing as much as they can toward solving their own problems, and who are helpful and respectful to others will get the best reception here.\r\n\r\n\r\n\r\n"
    author: "kallecarl"
---
In <a href="http://commit-digest.org/issues/2011-10-30/">this week's KDE  Commit-Digest</a>:

<ul>
<li>Mercurial Plugin for <a  href="http://userbase.kde.org/Dolphin">Dolphin</a></li>
<li>New FlashExport Tool in <a  href="http://www.kipi-plugins.org/">kipi-plugins</a></li>
<li>Improvements in <a href="http://userbase.kde.org/Rekonq">rekonq</a>  WebKit settings</li>
<li>More intuitive global search box in <a  href="http://www.kexi-project.org/">Kexi</a></li>
<li><a href="http://userbase.kde.org/Plasma/Public_Transport">Public  Transport</a> adds a dialog to configure journey searches and a quick journey  search button</li>
<li>Speed improvement when accessing network shares in KDE-Libs: skip slow  operations while listing them, also improved speeed of HTTP Digest and NTLM  authentications</li>
<li>Bugfixes in <a href="http://amarok.kde.org/">Amarok</a>, <a  href="http://www.calligra-suite.org/">Calligra</a>, <a  href="http://nepomuk.semanticdesktop.org">Nepomuk</a>, and <a  href="http://utils.kde.org/projects/kgpg/">KGpg</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-10-30/">Read  the rest of the Digest here</a>.
<!--break-->
