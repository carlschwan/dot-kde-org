---
title: "Party Time!"
date:    2011-01-06
authors:
  - "skfin"
slug:    party-time
comments:
  - subject: "Good news :-)"
    date: 2011-01-08
    body: "It's great to be reminded about organising release parties - there's still time. I hope to see lots of new ones entered on the wiki!"
    author: "Stuart Jarvis"
---
It has been several months now since the previous set of major releases by KDE and it is time for some more! KDE's Plasma workspaces and the KDE Platform will all reach version 4.6 on 26 January and many of KDE's application teams will also be unveiling new versions of their software on that day.

That means it is time to gather with local KDE mates to celebrate the new releases. It is a good reason to just chill out with other people and discuss anything you have on your mind, from KDE to pink fluffy bunnies.

Every release party needs a bit of planning - even if it is just having a beer together somewhere. The trickiest thing is to get everyone in same place at same time. To let people know that you have an event going on and to help gather local KDE friends, please add your party to the <a href="http://community.kde.org/Promo/ReleaseParties/4.6">the wiki</a>.

If there is a party in your local area already just add yourself to the attendees list so organizers can estimate how many people are coming to the party. If your party is not listed, get in contact with other local people and start organizing one.

Remember: with great software comes great parties!
