---
title: "KDE Commit-Digest for 6th November 2011"
date:    2011-11-18
authors:
  - "mrybczyn"
slug:    kde-commit-digest-6th-november-2011
comments:
  - subject: "Noteworthy"
    date: 2011-11-22
    body: "Awesome work by Joris on KTorrent, appreciated! Some old bugs fixed, btw..."
    author: "jospoortvliet"
---
In <a href="http://commit-digest.org/issues/2011-11-06/">this week's KDE Commit-Digest</a>, we have included a feature on the recent release of Necessitas Alpha 3—the project to get Qt working on the Android platform. Several tasks are listed that must be completed for the project to move out of alpha; no coding expertise required. Pitch in if you want to help get Qt working on Android.

In addition, the usual list of updates:

               <ul><li>In <a href="http://kde.org/workspaces/">KDE Workspaces</a>, TabBox has been rewritten in QML</li>
<li>PDF analyzer in libstreamanalyzer</li>
<li>New "exact" field:=value match in collection search in <a href="http://amarok.kde.org/">Amarok</a></li>
<li>A torrent search bar and user interface reorganization in <a href="http://ktorrent.org/">KTorrent</a></li>
<li>Improved removing of city names in <a href="http://userbase.kde.org/Plasma/Public_Transport">Public Transport</a></li>
<li>Many bugfixes in <a href="http://www.calligra-suite.org/">Calligra</a> and <a href="http://amarok.kde.org/">Amarok</a>.</li>
</ul>

                 <a href="http://commit-digest.org/issues/2011-11-06/">Read the rest of the Digest here</a>.
