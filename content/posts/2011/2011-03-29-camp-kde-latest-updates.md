---
title: "Camp KDE: Latest Updates"
date:    2011-03-29
authors:
  - "Justin Kirby"
slug:    camp-kde-latest-updates
---
Camp KDE 2011 is nearly upon us, but that hasn't stopped the organizers from continuing to add to the fun.  Be sure to check out <a href="http://camp.kde.org">the Camp KDE web site</a> for the final agenda as well as speaker bios.
<!--break-->
<h3>Hacking session moved to Monday</h3>

In order to allow more participants to take part in the hacking sessions they have been moved to Monday evening, immediately following the conclusion of the talks for the day.  Don't miss out on this great opportunity to sit down face to face with fellow KDE contributors and discuss the things most important to you!

<h3>Party at Noisebridge</h3>

A Camp KDE party has been confirmed and will be hosted at the <a href="http://www.noisebridge.net/">Noisebridge</a> hacker space on Monday night at 8pm.  This is a great opportunity for anyone who couldn't make the daytime talks to catch up on the events of the day.  There will be several rounds of lightning talks, beer, and lots of lively discussion. 

<h3>There is still time to join us</h3>

Anyone who is attending the Collaboration Summit can register for free entry into Camp KDE.  There is still time to <a href="http://camp.kde.org/#register">join in on the fun</a> April 4 and 5, 2011 in San Francisco, California at the Hotel Kabuki.  See you there!