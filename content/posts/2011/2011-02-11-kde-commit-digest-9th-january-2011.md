---
title: "KDE Commit-Digest for 9th January 2011"
date:    2011-02-11
authors:
  - "dannya"
slug:    kde-commit-digest-9th-january-2011
comments:
  - subject: "Phonon"
    date: 2011-02-12
    body: "I am confused, I remember seeing a blog saying that VLC is now the preferred backend for Phonon. Or is it now largely a matter of personal preference, and we have two great backends to enjoy?\r\n"
    author: "tangent"
  - subject: "Phonon and GStreamer"
    date: 2011-02-12
    body: "I can tell you that after some recent updates I couldn't skip ahead in songs any more. Checked phonon and it was set to GStreamer (which I don't remember being the case before). Changing it back to either xine or VLC fixed it. And at freenode (irc) I was told VLC was the better choice. Just anecdotal experience, take it for what it's worth.  "
    author: "jadrian"
  - subject: "Xine"
    date: 2011-02-14
    body: "The 4.6 update wanted to use VLC by default, which pulls in xulrunner.  Even on a CoreDuo at 2.8GHz and three threads *still* takes almost a half hour to compile.  I changed my use flags to use Xine, as has been \"default\" for KDE for years.  I've never had any issues.\r\n\r\n\r\nM.\r\n\r\n#-----\r\n# EDIT:\r\n\r\nI should have been a bit more clear.  On *Gentoo*, the USE flags default to VLC.  I don't know about other distros.\r\n#-----\r\n"
    author: "Xanadu"
  - subject: "ohh Python support!"
    date: 2011-02-14
    body: "Ohh.. Python support! I've love to see that work in KDevelop. :-D\r\n\r\nDo you guys see a chance to get code completion implemented there as well?"
    author: "vdboor"
---
In <a href="http://commit-digest.org/issues/2011-01-09/">this week's KDE Commit-Digest</a>:

<ul>
<li>Beginnings of type support for lists in <a href="http://www.kdevelop.org/">KDevelop</a>’s Python plugin</li>
<li><a href="http://edu.kde.org/cantor/">Cantor</a>’s Qalculate backend is improved</li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> gains a feature to predict a <a href="http://en.wikipedia.org/wiki/Star_hopping">star hopping</a> route and dynamic switching between OpenGL and native backends</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> sees improvements to the "Postition Marker" configuration dialog, gets "Earthquake" and "Overview Map" configuration dialogs and a planet option for the map creation wizard</li>
<li>Bug fixes to <a href="http://plasma.kde.org">Plasma</a> and its applets, <a href="http://community.kde.org/KDE_PIM">KDE PIM</a>, <a href="http://phonon.kde.org/">Phonon</a>, and <a href="http://amarok.kde.org/en">Amarok</a></li>
<li>Bug fixes to <a href="http://en.wikipedia.org/wiki/KIO">KIO Slaves</a> and a large fix for the implementation of KIO Slaves in <a href="http://rekonq.sourceforge.net/">Rekonq</a></li>
<li>The table of contents generator in <a href="http://www.calligra-suite.org/words/">Calligra Words</a> is improved</li>
<li>Improvements are made to Office Open XML format importing in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li><a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a> gets an improved .csv file importer</li>
<li><a href="http://skrooge.org/">Skrooge</a> can import Money Manager EX files</li>
<li>Work is done on action editor for <a href="http://www.calligra-suite.org/krita/">Krita</a></li>
<li>KPlato is renamed to <a href="http://www.calligra-suite.org/plan/">Calligra Plan</a> and gets more view options and other improvements to Gantt charts</li>
<li>A prototype mobile version of <a href="http://www.calligra-suite.org/kexi/">Kexi</a> is introduced</li>
<li><a href="http://kde-apps.org/content/show.php/Cirkuit?content=107098">Cirkuit</a> supports <a href="http://ghns.freedesktop.org/">GHNS</a></li>
<li><a href="http://kde-apps.org/content/show.php/Kamoso?content=111750">Kamoso</a> supports Nepomuk and more webcams</li>
<li><a href="https://projects.kde.org/projects/playground/sysadmin/apper">Apper</a>, formerly known as KPackagekit, gets a single KDE Control Center Module for its settings and its updater and gains an option to stop checking for updates when on battery power</li>
<li><a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a>’s performance is improved</li>
<li>The majority of <a href="http://userbase.kde.org/Kontact">KMail</a> is ported to the <a href="http://techbase.kde.org/Development/Tutorials/Using_KConfig_XT">KConfigXT</a> architecture</li>
<li>Rekonq gains a user agent switcher</li>
<li><a href="http://techbase.kde.org/KDE_System_Administration/Kiosk/Introduction">Kiosk</a> gets bug fixes for application compatibility and a feature for file browsing restrictions</li>
<li><a href="http://developer.kde.org/~wheeler/taglib.html">TagLib</a> now has full read/write support for FLAC files</li>
<li><a href="http://gstreamer.freedesktop.org/">GStreamer</a> becomes the preferred Phonon backend and is able to install plugins automatically</li>
<li>The <a href="http://www.oxygen-icons.org/">Oxygen</a> 16x16 mimetype icons are finished</li>
<li>The <a href="http://pim.kde.org/akonadi/">Akonadi</a> KDE system tray, <a href="https://projects.kde.org/projects/playground/sysadmin/kalternatives">KAlternatives</a> and <a href="http://www.krusader.org/">Krusader</a> move to Git.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-01-09/">Read the rest of the Digest here</a>.
<!--break-->
