---
title: "New Behind KDE 'Platform' Series Launches with Patrick Spendrin"
date:    2011-01-11
authors:
  - "jriddell"
slug:    new-behind-kde-platform-series-launches-patrick-spendrin
comments:
  - subject: "Awesome Interview!"
    date: 2011-01-12
    body: "Really good read, informative and cool :)\r\n\r\n\r\nIn other news, it has just hit Slashdot:\r\n\r\nhttp://developers.slashdot.org/article.pl?sid=11/01/12/0528245"
    author: "Mark Kretschmann"
---
<a href="http://www.behindkde.org/">Behind KDE</a> is our site for interviews with KDE contributors and a new series is being started by Pau Garcia i Quiles.  The theme of the series is the different platforms that KDE is available for.  The <a href="http://www.behindkde.org/node/895">first interview is with Patrick Spendrin</a>.  Patrick works on the KDE Windows project and tells us how the project works, what can be done with it, how he got involved and how you can get involved.  Future articles in this series will talk about Mac, BSD, Solaris and other platforms.
