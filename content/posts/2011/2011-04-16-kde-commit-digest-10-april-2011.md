---
title: "KDE Commit Digest for 10 April 2011"
date:    2011-04-16
authors:
  - "vladislavb"
slug:    kde-commit-digest-10-april-2011
comments:
  - subject: "Great work keeping the commit"
    date: 2011-04-17
    body: "Great work keeping the commit digest up to date Danny!!"
    author: "trixl."
  - subject: "Not Danny"
    date: 2011-04-20
    body: "Ouch, I accidentally voted you up before reading the last word.\r\n\r\nDanny apparently wasn't even directly involved with this digest. He built the platform (for which we are grateful, of course), but this digest was compiled by:\r\n\r\nMarco Krohn\r\nVladislav Blanton\r\nMarta Rybczynska\r\nDominik Tritscher\r\n\r\nThank you, guys!"
    author: "onety-three"
  - subject: "Hehe, yes - full credit for"
    date: 2011-04-24
    body: "Hehe, yes - full credit for these Digests must go to the people listed above (and to the top right of each issue) :)\r\n\r\nI'm still lurking around, but I mostly only fix Enzyme issues as they appear so that others can continue doing their good work!\r\n\r\nCheers,\r\nDanny"
    author: "dannya"
---
In <a href="http://commit-digest.org/issues/2011-04-10/">this week's KDE Commit-Digest</a>:

<ul>
<li><a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kio/html/index.html">KIO</a> Proxy continues to receive <a href="https://git.reviewboard.kde.org/r/101037/">an overhaul</a> for KDE 4.7 with fixes and features including SOCKS proxy support, functions for obtaining multiple proxy URL addresses and support for obtaining system proxy information on Windows and Mac platforms</li>
<li>Amidst much bugfixing, <a href="http://www.calligra-suite.org/">Calligra</a> gains a map widget in <a href="http://www.calligra-suite.org/kexi/">Kexi</a>, improved xlsx support, initial porting to the new textlayout-library, "search from cursor" support, improved OpenOffice.org interoperability, and a more robust table of contents generator</li>
<li>Work to turn <a href="http://www.kde.org/applications/multimedia/kmix/">KMix</a> into a plasmoid alongside work on improved PulseAudio support, UTF-8 support, and memory leak fixes</li>
<li>The start of a new simple editor and re-written spellchecking support in <a href="http://edu.kde.org/applications/all/parley">Parley</a></li>
<li>Further work on the routing manager in <a href="http://edu.kde.org/marble/">Marble</a> amidst much bugfixing</li>
<li><a href="http://www.digikam.org/">Digikam's</a> theme engine is ported to KDE's engine, closing 7 related bugs</li>
<li>Large merger of Exif, Iptc, and Xmp editors into a common dialog in <a href="http://extragear.kde.org/apps/kipi/">Kipi-Plugins</a></li>
<li>Amongst much bugfixing, <a href="http://www.kdevelop.org/">KDevelop</a> gains support for predefined indentation styles and a Python interpreter using <a href="http://kross.dipe.org/">Kross</a></li>
<li>A new code import wizard in <a href="http://uml.sourceforge.net/">Umbrello</a></li>
<li>SVG file support and EPS and PDF export added to <a href="http://kst-plot.kde.org/">Kst</a></li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> gains support for diagonal navigation in mouse emulation</li>
<li>Plasma widgets gain access to the <a href="http://doc.trolltech.com/4.3/qscriptengine.html">QScriptEngine</a></li>
<li>Improvements to Storage and File Watch services in <a href="http://nepomuk.kde.org/">Nepomuk</a> deprecate the removable storage service</li>
<li>Much bugfixing and work on <a href="http://rekonq.kde.org/">Rekonq</a> leading up to 0.7 release</li>
<li>Initial avatar support and tootips added to <a href="http://grundleborg.wordpress.com/2010/03/24/telepathy-contact-list-update/">Telepathy-Contact-List</a> amongst other bugfixes</li>
<li>Further work on <a href="http://kde-look.org/content/show.php/?content=136216">Oxygen-GTK</a> and new <a href="http://www.oxygen-icons.org/">Oxygen</a> icons</li>
<li><a href="http://opendesktop.org/content/show.php?content=140679">Ksaolaji</a> gains profiles, a marbletilecache cleaner and basic NewStuff support</li>
<li>Implementation of Achievement and Forum services in <a href="http://techbase.kde.org/Development/Tutorials/Collaboration/Attica/Introduction">Attica</a></li>
<li>More touch options added to the Wacom Tablet KCModule</li>
<li>Object-Oriented Rewrite System (OORS) highlighting rules added to <a href="http://kate-editor.org/">Kate</a></li>
<li>Improvements to the animated sprite renderer in <a href="http://gluon.gamingfreedom.org/">Gluon</a></li>
<li>Adblock fixes in <a href="http://opendesktop.org/content/show.php?content=127960">KWebKitPart</a></li>
<li>Much bugfixing throughout <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a> and <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-04-10/">Read the rest of the Digest here</a>.