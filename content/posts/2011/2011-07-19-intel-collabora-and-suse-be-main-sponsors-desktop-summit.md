---
title: "Intel, Collabora and SUSE to be main sponsors of the Desktop Summit"
date:    2011-07-19
authors:
  - "jospoortvliet"
slug:    intel-collabora-and-suse-be-main-sponsors-desktop-summit
comments:
  - subject: "Date?"
    date: 2011-07-21
    body: "Maybe I missed it, but when will it be held (other than some time in 2011...)?"
    author: ""
  - subject: "Re: Date"
    date: 2011-07-21
    body: "6th-12th August.\r\n\r\nMore info: http://www.desktopsummit.org/"
    author: "bluelightning"
---
<p>We are pleased to announce that the <a href="https://www.desktopsummit.org/">Desktop Summit</a> 2011 in Berlin will be supported by Intel as the Platinum sponsor. The event organizers also welcome Collabora and SUSE as Gold partners, and are delighted with the community spirit of these generous corporate partners. Mirko Boehm, lead organizer of the Desktop Summit, said: <em>"Their support is essential for the Desktop Summit's efforts to bring together Free Software developers from all around the world to work in a collaborative spirit on the next generation desktop technology."</em></p>
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; width: 300px;"><img src="/sites/dot.kde.org/files/sponsors_group.png"></div>

<p>Dawn Foster, MeeGo Community Manager stated: <em>"Intel is happy to sponsor the Desktop Summit as a way to support the many projects that we use and contribute to on a regular basis. It's important to us to work closely with open source projects - and this is one way to do that."</em> </p>

<p>Christian Schaller, Marketing Manager at Collabora told us: <em>"At Collabora we are excited to support the Desktop Summit as we feel it is one of the core events in terms of moving the open source ecosystem forward. A lot of open source innovation happens on the desktop first before being widely deployed on all kinds of systems and devices. As a leader in the fields of multimedia and real time communications, Collabora are very much a part of that effort. We look forward to meeting up with and engaging with the everyone at the Desktop Summit."</em></p>

<p><em>"SUSE is proud to be a Gold Sponsor of the Desktop Summit 2011,"</em> said Michael Miller, vice president of Global Alliances and Marketing at SUSE.<em> “Cross project collaboration is a core value at SUSE, and we are committed to wide interoperability and open communication. Our support of openSUSE at the Desktop Summit demonstrates our continued commitment to a strong Free and Open Source ecosystem."</em </p>

<p>In addition to the Platinum and Gold sponsors, we are happy to welcome the following Silver sponsors for this event:
<ul>
<li>Canonical</li>
<li>Google</li>
<li>Igalia</li>
<li>The Linux Foundation</li>
<li>Red Hat</li>
<li>Qt</li>
</ul>
</p>

<p>And our Bronze level sponsors:
<ul>
<li>Lanedo</li>
<li>Mozilla</li>
<li>openshine</li>
</ul>
</p>

<p>The event is also supported by:
<ul>
<li>Froglogic</li>
<li>Open Source Berlin</li>
<li>TSB Innovationsagentur Berlin</li>
</ul>
</p>

<p>Their support is greatly appreciated.</p>

<p>Our media partners for the Desktop Summit are golem.de and Linux Magazine.</p>

<p>The organizing team thanks our sponsors on behalf of the GNOME and KDE communities. We are still looking for more sponsors. Please visit the Desktop Summit <a href="https://www.desktopsummit.org/sponsors">Sponsors page</a> if you are interested.</p>

<h2>About the Desktop Summit</h2>

<p>GUADEC (GNOME Users And Developers European Conference) and Akademy (KDE annual world summit) are the world's largest gatherings of people involved in Free Desktop and mobile user interfaces. Over a thousand participants are expected at the Desktop Summit this year, covering both the GNOME and KDE projects as well as related technologies. Organizers welcome developers, artists, translators, community organizers, users and representatives from government, education, businesses. Anyone who shares an interest in a Free Desktop is encouraged to participate.</p>

GNOME and KDE are Free Software communities that drive the user interfaces of many Linux-powered devices—smartphones, laptops, desktops, personal media centers. 2011 is the second summit organized collaboratively by the two communities.