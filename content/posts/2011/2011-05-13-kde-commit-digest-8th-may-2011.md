---
title: "KDE Commit-Digest for 8th May 2011"
date:    2011-05-13
authors:
  - "vladislavb"
slug:    kde-commit-digest-8th-may-2011
comments:
  - subject: "Is it true?"
    date: 2011-05-16
    body: "No login required to make a comment?"
    author: ""
  - subject: "thank you!"
    date: 2011-05-16
    body: "thank you all for yet another commit-digest, you can't imagine how many are grateful even if not commenting..\r\n\r\nthank you again"
    author: ""
  - subject: "It's true"
    date: 2011-05-17
    body: "No registration required :-)"
    author: "areichman"
  - subject: "Long standing bug"
    date: 2011-05-17
    body: "I see there's a bug almost 10 years old that got fixed :-)  How can that even be relevant under KDE 4?"
    author: ""
  - subject: "Crazy but true"
    date: 2011-05-17
    body: "I just tried it in rekonq on kubuntu 11.04 and it's still here. I guess it's code that was transitioned from KDE2->3 and again from KDE3->4. Must be one of the oldest, still valid bugs in existence, no? Anyways, thanks to Dawit Alemayehu for fixing it :-)"
    author: "areichman"
  - subject: "Indeed, thank you!"
    date: 2011-05-17
    body: "Danny Allen\r\nVladislav Blanton\r\nShafqat Bhuiyan\r\nMarta Rybczynska\r\nMarco Krohn\r\n\r\nThe Commit-Digest is always an interesting read! Thanks!"
    author: ""
  - subject: "Long standing bug"
    date: 2011-05-18
    body: "I see there's a bug almost 10 years old that got fixed :-)  How can that even be relevant under KDE 4?"
    author: ""
  - subject: "Maybe now someone could look"
    date: 2011-05-18
    body: "Maybe now someone could look at https://bugs.kde.org/show_bug.cgi?id=268797 before that gets to be 10 years old also?"
    author: ""
  - subject: "10 year old bug"
    date: 2011-05-18
    body: "This bug was reported 2 months ago. Thanks to this comment, someone looked at it today.\r\n\r\nIt's not on most hated, most severe or most reported. Other than squeaky wheel, is there some reason this bug should precede others?"
    author: "kallecarl"
  - subject: "Re: Long standing bug"
    date: 2011-05-20
    body: "Because it was actually a server side implementation bug. The fix was actually a workaround and not a bug fix. It took as long as it did to resolve it because the ticket was unfortunately assigned to the wrong product/component bin."
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-05-08/">this week's KDE Commit-Digest</a>:

              <ul><li>Continued work throughout <a href="http://www.calligra-suite.org/">Calligra</a>, including improvements to MS 2003 and MS 2007 file support, working anchors and server-based project creation</li>
<li>Work and optimization throughout <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, including support for Kolab and LDAP servers requiring authentication and a big wizard revamp</li>
<li>Further improvements to terminal integration (with <a href="http://api.kde.org/4.5-api/kdebase-apps-apidocs/konsole/html/classKonsole_1_1Part.html">KonsolePart</a>) in <a href="http://www.kdevelop.org/">KDevPlatform</a> and <a href="http://techbase.kde.org/Development/Languages/Python">PyKDE4</a></li>
<!--break-->
<li>Bugfixing and work throughout <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a>, including possibility to set custom routes and further Bluetooth NAP tethering support</li>
<li><a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy-Contact-List</a> sees file transfer and audio/video chat implementations</li>
<li>Major improvements to OpenGL backend in <a href="http://edu.kde.org/kstars/">KStars</a> and GSOC implementation of SVG export</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> gains support for <a href="http://code.google.com/p/monav/">Monav 0.3</a> and further work from the <a href="http://userbase.kde.org/Marble/OfflineSearch">GSOC project for offline search</a></li>
<li>Implementation of event processing for Nepomuk in <a href="http://zeitgeist-project.com/">Zeitgeist</a></li>
<li>Basic ResourceWatcher infrastructure in <a href="http://nepomuk.kde.org/">Nepomuk</a></li>
<li>Bugfixing in <a href="http://userbase.kde.org/KWin">KWin</a>, including enabling of direct rendering for all Mesa drivers</li>
<li><a href="http://okular.kde.org/">Okular</a> gains support for reading a directory as a comicbook</li>
<li>KCM Locale gains support for Digit Grouping</li>
<li><a href="http://community.kde.org/Plasma/Plasma-Mobile">Plasma-Mobile</a> gains a Custom Search dataengine</li>
<li>Basic support for strings in structures added to <a href="http://utils.kde.org/projects/okteta/">Okteta</a></li>
<li>Multihead support <a href="https://projects.kde.org/projects/kde/kdebase/kde-workspace/repository/revisions/c6b621ff55d6218bd726c53389581b10f99963b0">added to KSplashX</a></li>
<li>Bugfixing in <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kio/html/index.html">KIO</a> including improvements to KDE's proxy authentication</li>
<li>Work on <a href="http://amarok.kde.org/blog/archives/1185-bouncing-around-the-echo-chamber.html">libechonest</a></li>
<li>Work in <a href="http://www.oxygen-icons.org/">Oxygen</a></li>
<li><a href="http://ktouch.sourceforge.net/">KTouch</a> gains a Spanish Dvorak keyboard layout</li>
<li>Initial work for 3D support in <a href="http://edu.kde.org/step/">Step</a></li>
<li><a href="http://www.trinitydesktop.org/">Trinity</a> ports <a href="http://www.rosegardenmusic.com/">Rosegarden</a> to support multithreading and compilation under both Qt3 and Qt4</li>
<li>Bugfixing throughout <a href="http://plasma.kde.org/">Plasma</a>, <a href="http://techbase.kde.org/Projects/Plasma/Plasmoids">Plasma's Applets</a> and <a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a>.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-05-08/">Read the rest of the Digest here</a>.