---
title: "KDE Commit-Digest for 5th June 2011"
date:    2011-06-10
authors:
  - "vladislavb"
slug:    kde-commit-digest-5th-june-2011
comments:
  - subject: "LongBug champion!"
    date: 2011-06-10
    body: "This week's champion is (again!) Dawit Alemayehu for fixing bugs <a href=\"http://bugs.kde.org/show_bug.cgi?id=77169\">77169</a>, <a href=\"http://bugs.kde.org/show_bug.cgi?id=116639\">116639</a> and <a href=\"http://bugs.kde.org/show_bug.cgi?id=158124\">158124</a> each of them being more than 7, 5 and 3 years old!\r\n\r\n\"KURLCompletion::ExeCompletion lists directories instead of executables\"\r\n\r\n\"kurlcompletion doesn't complete executables in $PATH in kopenwith dialog\"\r\n\r\n\"URL completion doesn't list all files/dirs in the directory\""
    author: "emilsedgh"
  - subject: "Awesome ;)"
    date: 2011-06-10
    body: "Awesome ;)"
    author: ""
  - subject: "And what needs to go .....!!"
    date: 2011-06-14
    body: "Simply, KDE is great.\r\n\r\n3 months I've been using openSUSE and KDE, with the best implementation and integration with YaST. And I must say I'm excited, proud and completely satisfied with KDE.\r\n\r\nI can not believe that being an environment as configurable, customizable, useful, without distraction, powerful and stable, not as major distros default. At least openSUSE and Mandriva already implemented by default.\r\n\r\nWhile Gnome 3 removes the personality of its environment, KDE adds more.\r\n\r\nHopefully KDE never change. You are at least 10 years ahead to be so, that I can tinker all you want and that will not change.\r\n\r\nI know things still had not seen before and each time I find myself more.\r\n\r\nNow is the time to start working with openSUSE and KDE, along with C + +, QML, QT Java Creator. Because soon I will join the development!.\r\n\r\nThanks for providing all this, the KDE team. A greeting."
    author: "1antares1"
  - subject: "KDE Awesomeness"
    date: 2011-06-15
    body: "As for me, since the first days I'm completely in love with KDE. I'm amazed by the style, the usability, all the great innovations that make my life and work a pleasure.\r\n\r\nSo, developers, you all deserve a long, long hug and kisses to those who appreciate them.\r\n\r\nI know some people sometimes give you hard times. Don't listen. Look at the amazing piece of software you made and think of all those people like me who love seeing their computer start KDE each day and enjoy each single piece of awesomeness being in it.\r\n\r\ncheers!\r\n\r\n"
    author: ""
---
This week, the Commit Digest includes a featured article about <a href="http://kst-plot.kde.org/">Kst</a>, the fastest real-time large-dataset viewing and plotting tool.

Also in <a href="http://commit-digest.org/issues/2011-06-05/">this week's KDE Commit-Digest</a>:

<ul>
<li>Work on C++0x support in <a href="http://www.kdevelop.org/">KDevelop</a>, including support for variadic template parameters in the parser, new browser-like tabs, and many bugfixes throughout KDevPlatform</li>
<li>In <a href="http://plasma.kde.org/">Plasma</a>, suspend/hibernate services added to the powermanagement dataengine, battery applet replaced with QML port from <a href="http://community.kde.org/Plasma/Plasma-Mobile">Plasma-Mobile</a>, and many bugfixes</li>
<!--break-->
<li>Work on .xlsx, .doc, and VML support in <a href="http://www.calligra-suite.org/">Calligra</a>, along with support for baseline shift, a new preset chooser widget in <a href="http://krita.org/">Krita</a>, initial commit of the Krita Animator plugin, and much bugfixing</li>
<li>Further bugfixing and work on NM09 support in <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> including manual setting of speed and duplex mode for ethernet and advanced permission settings</li>
<li>Work on Kartesio (plots the best fit of a mathematical curve) to support scientific notation</li>
<li>Finishing work on <a href="http://edu.kde.org/cantor/">Cantor's</a> Scilab backend</li>
<li>GSOC work on making the canvas use the new QGraphicsView in <a href="http://uml.sourceforge.net/">Umbrello</a></li>
<li>Oxidization data added to elements in <a href="http://edu.kde.org/kalzium/">Kalzium</a></li>
<li>GSOC work on Commit Dialog in <a href="http://dolphin.kde.org/">Dolphin</a>, among additional bugfixing</li>
<li>Start of javascript API support in <a href="http://ivan.fomentgroup.org/blog/2011/06/06/contouring-the-share-like-connect/">Share-Like-Connect</a>, among other work</li>
<li>Further work on <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy-Presence-Applet</a></li>
<li>More work on IM icons in <a href="http://www.oxygen-icons.org/">Oxygen</a></li>
<li>Improved file uploader and work on the media player in <a href="http://owncloud.org/">OwnCloud</a></li>
<li>Use of Qt gesture framework in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>Bugfixing in <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://userbase.kde.org/KWin">KWin</a>, <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://kmymoney2.sourceforge.net/">KMyMoney</a>, <a href="http://amarok.kde.org/">Amarok</a>, and <a href="http://userbase.kde.org/Juk">Juk</a></li>
</ul>

<a href="http://commit-digest.org/issues/2011-06-05/">Read the rest of the Digest here</a>.