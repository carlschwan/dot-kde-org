---
title: "KDE Commit Digest for 27 February 2011"
date:    2011-03-07
authors:
  - "vladislavb"
slug:    kde-commit-digest-27-february-2011
---
In <a href="http://commit-digest.org/issues/2011-02-27/">this week's KDE Commit-Digest</a>:

<ul><li>Calligra continues its activity including PPT and PPTX file support and an experimental Google Spreadsheets plugin</li>
<li>Work throughout Phonon and its backends including an improved finding of virtual devices from ALSA</li>
<li>KDevelop sees the implementation of a new shell command interface and improvements to the Valgrind parser and Ruby language support</li>
<li>A new interactive legend in <a href="http://www.kde.org/applications/education/kalzium/">Kalzium</a></li>
<li>Further work on Pick and Color Labels in Digikam</li>
<li>Major work on the <a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a> Plasma applet</li>
<li>Keyboard navigation support added to the calendar applet</li>
<li>Work throughout <a href="http://community.kde.org/Plasma/Plasma-Mobile">Plasma-Mobile</a></li>
<li>Support for douban.com added to <a href="http://tellico-project.org/">Tellico</a> collection management software</li>
<li>New "partial seeding" extension in <a href="http://ktorrent.org/">KTorrent</a></li>
<li>New configurable Samba browsing in system-config-printer-kde, the printing GUI</li>
<li>Get Hot New Stuff support for the new template system in <a href="http://sciencekde.wordpress.com/2010/12/11/introducing-cirkuit/">Cirkuit</a></li>
<li>KDE Platform 4/Qt4 porting of <a href="http://userbase.kde.org/KMLDonkey">KMLDonkey</a> has begun</li>
<li>FreeBSD fixes in Gwenview</li>
<li>Bugfixing in KDE-Libs including Kioslaves</li>
<li>Bugfixing in KGPG, Amarok and KDE-PIM</li>
<li>Kolourpaint, KRuler, <a href="http://www.kde.org/applications/graphics/skanlite/">Skanlite</a>, KSanePlugin, and KSnapshot move to Git</li>
</ul>

<a href="http://commit-digest.org/issues/2011-02-27/">Read the rest of the Digest here</a>.