---
title: "KDE Commit-Digest for 6th February 2011"
date:    2011-02-23
authors:
  - "dannya"
slug:    kde-commit-digest-6th-february-2011
comments:
  - subject: "Great progress"
    date: 2011-02-23
    body: "It's nice to see all the commit digests coming out. Keep it up!"
    author: "nethad"
---
In <a href="http://commit-digest.org/issues/2011-02-06/">the latest KDE Commit-Digest</a>:

              <ul><li>KWin developer Martin Gräßlin works hard on porting KWin’s graphical effects to OpenGL ES, with the effects receiving many visual improvements in the process</li>
<li>KWin’s problematic "Shadow" and "Snow" effects are removed and await a better implementation in the future</li>
<li>Muon supports offline installation of packages and gains a .deb format thumbnailer plugin</li>
<li>Improvements are made to the artistic style formatter in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>A "Color Label Selector" is implemented in <a href="http://www.digikam.org/">Digikam</a>, which also sees an update of its internal libraw library which provides improved support for more cameras</li>
<li>Support for asynchronous SFTP downloading is implemented in kio_sftp</li>
<li>BlueDevil can now share files with an OBEX FTP server</li>
<li>Initial work on a user interface to control change tracking options in Calligra</li>
<li>Cirkuit gains options for manual and automatic backend selection</li>
<li>The functionality for creating a Table of Contents in Calligra Words is improved</li>
<li>The game dialog of Knights is redesigned</li>
<li>A large refactoring of the code for the KPatience game is done</li>
<li>Bug fixes in <a href="http://kst.kde.org/">Kst</a>, the Kipi plugins, Digikam, power management, kdelibs, the <a href="http://plasma.kde.org/">Plasma</a> Desktop, KDE-PIM, Calligra/KOffice, <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://kopete.kde.org/">Kopete</a>, and <a href="http://ktorrent.org/">KTorrent</a></li>
<li>Plasma-addons, kdebase, kdelibs, the KDE Build Tool and KDE’s Qt copy (qt-kde) move to Git.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-02-06/">Read the rest of the Digest here</a>.
<!--break-->
