---
title: "Stable Update 4.7.4 and Testing Release 4.8 Beta2 Available"
date:    2011-12-07
authors:
  - "sebas"
slug:    stable-update-474-and-testing-release-48-beta2-available
comments:
  - subject: "Beta2 link"
    date: 2011-12-08
    body: "Beta2 link links to Beta 1 announcement."
    author: ""
  - subject: "thank you"
    date: 2011-12-08
    body: "fixed"
    author: "kallecarl"
  - subject: "smb support"
    date: 2011-12-08
    body: "what about fixing smb streaming so \r\nyou can watch a movie off a network\r\nshare without having to copy it \r\nlocally first? only thing keeping\r\nme on gnome."
    author: ""
  - subject: "smb support"
    date: 2011-12-08
    body: "While I agree that kio is getting long in the tooth and that gvfs is very interesting (and IMHO better), you can already do this if you mount the smb share instead of using kio.\r\n\r\nThere's even an excellent KDE application, called Smb4k for doing that."
    author: "ianjo"
  - subject: "Multi-Screen-Setup"
    date: 2011-12-08
    body: "After all those release KDE 4 still does not have a Multi-Screen-Setup (seperate X-Servers) \r\nhttps://bugs.kde.org/show_bug.cgi?id=256242\r\nAnd it looks like KDE 4 will never have it.\r\nAs fare as I know it is the only option when I have 2 nvidia cards to power 4 screens and I am not a kde hacker "
    author: ""
  - subject: "KDE 4.7.4 Release Announcement"
    date: 2011-12-08
    body: "The date for the release announcement is wrong (November 2, 2011). Also, it says \"The October updates...\" and \"everyone running 4.7.0 or earlier versions\"; I assume this should be \"The December updates...\" and \"everyone running 4.7.3 or earlier\"."
    author: ""
  - subject: "Can't find fixes of memory  usage errors !"
    date: 2011-12-08
    body: "\"new and fancy view\" is very good stuff but what about idiotic random crashes of everything on signal 11? Kded4 is just the leader in 4.7.x! Is it too hard to learn:\r\n\r\nif (the_stupid_pointer!=0){\r\n      my_new_and_fancy_func(the_stupid_pointer);\r\n}else{\r\n     log(\"my fancy stuff can't start. You'll have no fun but can work.\");\r\n}\r\n\r\n???\r\nI even don't speak about smart pointers that are part of C++ standard now... "
    author: "alukin"
  - subject: "Fanciness "
    date: 2011-12-08
    body: ">fancy view engine for Dolphin, Qt Quick Components for Plasma\r\n\r\nhow about fixing long standing problems before doing the fancy stuff?\r\n\r\n\r\n"
    author: ""
  - subject: "Adding support for Multi-Head is not justifiable"
    date: 2011-12-08
    body: "Please understand that Multi-Head is a very special requirement only needed by a minority of our user base. None of the core Plasma developers is using such a setup. We offer a very good multi-screen support (XRandR, TwinView, etc.) and consider it more important to improve this user experience which affects all users who want to attach a second screen to their notebook.\r\n\r\nOur time to work on KDE is limited and we have to judge very well what we want to support and where we want to spend time on. Spending the time on Multi-Head support is unfortunately not justifiable towards all the other users who do not use it and who would miss important features and bug fixes which improves the user experience for all users.\r\n\r\nNevertheless I acknowledge the need for Multi-Screen support. But it does not change the fact that we as spare time developers cannot offer such features.\r\n\r\nIf this is an important feature to many users it should be possible to get in contact with a company to implement the missing pieces. Even I do offer such professional bug fixing service (see my blog). But you have to be aware that implementing Multi-Head support won't be something you get for 100 \u20ac. This is a complex requirement and especially ensuring that it works in future releases is very difficult due to the fact that none of the core developers is using such a setup."
    author: "mgraesslin"
  - subject: "How can you know that this"
    date: 2011-12-08
    body: "How can you know that this did not fix bugs? How can you judge whether it is worth to spend time on implementing this? Do you really think that the KDE developers are not able to decide what is best for their applications they develop and maintain? Do you think we rewrite parts of our applications just for the fun of it?"
    author: "mgraesslin"
  - subject: "To say more,"
    date: 2011-12-08
    body: "To say more, KDE 4.7.x does not even start on my desktop after regular update (Fedora 16). Cool and fancy stuff now is Krash message after splash screen: Signal 11 again! Cool work, guys! \r\n\r\nYou don't have tons of automatic bug reports? Strange but I know why. Because kded is used to send reports form Krash but kded4 crashes with signal 11."
    author: "alukin"
  - subject: "helps to read the post"
    date: 2011-12-08
    body: "This sort of comment is pretty much useless kvetching.\r\n\r\nThe story says, \"...new and fancy view engine for Dolphin...\". Another poster also takes this phrase out of context to make some point in a more clever manner.\r\n\r\nDevelopment and bugfixing do not happen as you apparently imagine. There is no Jobs or Gates assigning people from one project to another. Based on whatever input, their own sense of what's needed and their own interests, the Dolphin developers added new, cool functionality to the view engine for Dolphin. They didn't, and won't ever, look at all possible bugs to decide what to work on.\r\n\r\nUncommitted, vague complaining is more likely to result in fewer and less enthusiastic developers than more bugfixes. "
    author: "kallecarl"
  - subject: "That's very good! No new"
    date: 2011-12-08
    body: "That's very good! No new young and green developers, no idiotic ideas and no foolish bugs. I do not complain. I'm just trying to say that KDE is digging it's grave by \"new and fancy stuff\" and that's a pity.  "
    author: "alukin"
  - subject: "complain or take action?"
    date: 2011-12-08
    body: "After reading \"And it looks like KDE 4 will never have it.\", I took a look at the comment thread for the bug. It looks like that's not the true story. \r\n\r\nIt would be more accurate to say that existing KWin developers have higher priorities that serve more KDE users. And that there are some people (not KWin developers) who have solved at least some of the problem. Progress is being made.\r\n\r\nPossible ways of getting this functionality given the circumstances:\r\n- complain\r\n- take action to get it fixed:\r\n    1. ask the KWin development team if there is someone who would fix it for pay\r\n    2. get an estimate of how much it would cost\r\n    3. Add a whoops factor if it is not a known fix, but also includes discovery (time vs set price)\r\n    4. Go to pledgie.com and raise the money\r\n    5. Contract with a developer to get it fixed\r\n\r\nYou don't have to be a KDE hacker. You have to want to solve your problems rather than complaining about them."
    author: "kallecarl"
  - subject: "are you offering?"
    date: 2011-12-08
    body: "Are you offering to mentor? To help vet ideas and triage bugs?\r\n\r\nIf your assessment is true, the gravediggers are those who comment on announcements with vague, disparaging comments and gratuitous insults such as \"idiotic\". People who apparently claim higher ground than the <a href=\"http://kde.org/code-of-conduct/\">KDE Code of Conduct</a>. \r\n\r\nWhat result do these commenters hope to achieve with this type of comment? What result do you think they actually achieve?"
    author: "kallecarl"
  - subject: "Unreadable taskbar entries"
    date: 2011-12-08
    body: "Hi!\r\n\r\nThere is one thing I can't understand:\r\n\r\nhttp://www.kde.org/announcements/4.7/screenshots/dolphin-gwenview.png\r\n\r\nLook at the taskbar - \"Pictures Dolphin\", \"Gwenview\" is almost NOT READABLE. NO CONTRAST. COME ON GUYS...\r\n\r\nBye"
    author: ""
  - subject: "I have tried to complain. So"
    date: 2011-12-08
    body: "I have tried to complain. So far it did not help.\r\nAnd I have tried to offer money via a similar side as pledgie.com the pledgie was running for a year (with total og 150 euro) and no response from the developer. \r\nMy 100 euro still stand. If anyone would like to finish the rest of the bugs for this feature I will glady pay 100 euro for it.\r\n\r\nMy current workaround is using win 7 but it will not last forever and at that time I need something else (win 9???). I will prefer to pay someone from KDE that amount of money to fix the problems I have instead of buying yet another copy of windows."
    author: ""
  - subject: "how much to you think it"
    date: 2011-12-08
    body: "how much to you think it would cost you to fix it? I dont know about the other users but I would glady pay around 100 euro for the fix. But that does not see to be enough for you. Maybe there will be other that want to pay you"
    author: ""
  - subject: "Maybe I am asking a stupid"
    date: 2011-12-08
    body: "Maybe I am asking a stupid question. Multi-Head was working nicly back in the kde 3 days and the code has been maintain by the trinity project. How come it is so difficult to reuse it from that project? Or from the gnome 2 project.\r\nThe code is out there and working. Just wondering. \r\n\r\n"
    author: ""
  - subject: "Window title?"
    date: 2011-12-08
    body: "Are you referring to the window titles at the top of each application window?\r\n\r\nGwenview is easily visible. \"Pictures - Dolphin\" is greyed out because it doesn't have focus. You can easily change the appearance and behavior in System Settings (Kubuntu) or Personal Settings (openSUSE). Workspace Appearance and Application Appearance.  "
    author: "kallecarl"
  - subject: "Sounds like you know a lot"
    date: 2011-12-08
    body: "Sounds like you know a lot about these things. I suggest that you post a few patches to Review Board[1] to fix some of the crashes, it would be much appreciated by both KDE developers and users!\r\n\r\n[1] http://techbase.kde.org/Development/Review_Board"
    author: "Hans"
  - subject: "unreadable titles ?"
    date: 2011-12-09
    body: "i don't see any window titles in the taskbar, because i am using smooth-taskbar (win7 like) ;)"
    author: ""
  - subject: "Maybe I'm just too stupid to"
    date: 2011-12-09
    body: "Maybe I'm just too stupid to see it, but what is the reason to combine several X servers instead of using one X server?"
    author: ""
  - subject: "Think first, say stuff later"
    date: 2011-12-09
    body: "Do you think <strong>all</strong> the KDE developers, even the ones that write cool and fancy new stuff, would not have noticed such a small issue as the desktop not starting? Anf if they did, do you think they just didn't bother to do anything about it?\r\n\r\nOr is it not more likely that none of the KDE developers have encountered the bug in their use and testing. And judging from how the bug database, mailing lists, forums and blogs have not erupted with complaints abut this bug, it stands to reason that very few of the hundreds of thousands people using the latest 4.7.x release is affected.\r\n\r\nAnd this very rare occurring bug may not even be a bug in a KDE release. It could just as well be a distribution specific update bug,  but still a very rare one.\r\n"
    author: "Morty"
  - subject: "Excuse my ignorance, but..."
    date: 2011-12-09
    body: "... who scores the commentaris? I can't see any option to vote for or agenst them. Is some autority \"superior\" to all the comon readers to whom isn't permited to score?"
    author: ""
  - subject: "sign in to rate"
    date: 2011-12-09
    body: "Anonymous can comment, but can't rate comments. Signed in users can rate comments.\r\n\r\nIs some autority \"superior\" to all the comon readers to whom isn't permited to score?\r\nYes."
    author: "kallecarl"
  - subject: "The need for several X"
    date: 2011-12-09
    body: "The need for several X servers is very rare and very special. Valid usecases are multiple GPUs and maybe using one screen as a TV only. For everything else the normal multi-screen modes such as XRandR or TwinView are the better choice."
    author: "mgraesslin"
  - subject: "Multi-Head was working nicly"
    date: 2011-12-09
    body: "<cite>Multi-Head was working nicly back in the kde 3 days and the code has been maintain by the trinity project.</cite>\r\nI am sorry to inform you that the trinity project does not maintain KWin. I have seen through their commits and at max they broke kwin. KWin as of KDE 3.5 is unmaintained, all developers knowing that code are working on the recent version. We have never been asked to review changes. I have seen commits to areas which we would not change without a deep and long review and testing period.\r\n\r\n<cite>How come it is so difficult to reuse it from that project? Or from the gnome 2 project.\r\nThe code is out there and working. Just wondering.</cite>\r\nIf software development were that simple... Last time I actually tried Multi-Head the issues were mostly related to compositing which was not even available in KDE 3 or GNOME 2."
    author: "mgraesslin"
  - subject: "And that is the usecase I"
    date: 2011-12-09
    body: "And that is the usecase I have"
    author: "beer"
  - subject: "how much to you think it"
    date: 2011-12-09
    body: "<cite>how much to you think it would cost you to fix it?</cite>\r\nThat simply depends on what specific issues should be fixed and how long those take to fix. Without knowing which issues still exist I cannot say how long it will take. This is not a \"fix Multi-Head\" but more a fix this and that. Each of the individual issues are most likely unrelated and need to be investigated individually."
    author: "mgraesslin"
  - subject: "Bug triaging"
    date: 2011-12-09
    body: "Sounds like you are very motivated. Please join us on #plasma to support the current massive bug triaging and closing going on."
    author: ""
  - subject: "Mailing list?"
    date: 2011-12-09
    body: "Just wondering why this didn't go out on the kde-announce mailing list? Thanks."
    author: ""
  - subject: "A big thank you!"
    date: 2011-12-09
    body: "I just wanted to say a huge thank you to all the KDE developers, the amount of work you do is tremendous and I, for one, keep getting really excited at each new KDE release! (less than during the 4.0 days, of course, but that's just normal ;-) )\r\n\r\nI just realized that out of the ~30 comments this thread has now, all of them are just bitching and complaining, and I can't believe how selfish and self-centered people are.\r\n\r\nTo all the nay-sayers, people who think KDE is full of \"young and green developers\", with \"idiotic ideas\", people who think the semantic desktop is just a resource blackhole: you know KDE is all about progress and innovation, it is a pretty clear statement. If you don't like it, there's always fwvm, xv, xterm, etc... Nobody forces you to use KDE.\r\n\r\nTo the KDE developers: you know better than to pay attention to those people, but still, it is worth repeating: you are doing a fantastic job! For me KDE really epitomizes Free software at its best: not only there is this vibrant community full of talented people, but you also have a clear goal of being the best in what you do, and you keep coming up with your own new ideas (yeah, copying MS or Apple doesn't cut it for me, you're doomed to always be behind with such an attitude). KDE is without a doubt the most exciting desktop to follow.\r\n\r\nAnyway, to sum up: Rock on, KDE people!"
    author: "wackou"
  - subject: ">> fancy view engine for"
    date: 2011-12-09
    body: ">> fancy view engine for Dolphin\r\n> how about fixing long standing problems\r\n> before doing the fancy stuff?\r\n\r\nI'm not really happy that the new view-engine in Dolphin is announced as \"fancy\": The main reason to create a new engine was to improve the performance and to fix issues that could not been fixed with the old view-engine. Some background information about this is given at http://ppenz.blogspot.com/2011/08/introducing-dolphin-20.html\r\n\r\nThe fact that the new engine is fast enough to provide visual improvements is more or less a \"side effect\". So to answer your question directly: The new view-engine has been written to be able to fix long standing problems like e.g. clipped filenames or a bad performance."
    author: "ppenz"
  - subject: "Finally a positive comment"
    date: 2011-12-09
    body: "Finally a positive comment after all the negative stuff!\r\n\r\nThe developers are fixing many defects, many rewrites address known old issues and so on and what do we get? People complaining about everything and how KDE developers do not fix bugs (hint: they do, even more than in the past) and most of those who complain are (what a surprise!) ill informed not to say something else.\r\n\r\nTo the developers - keep the good work and I'm already looking forward to 4.8. Should be a nice improvement...\r\n\r\nAnd to the rest - start being productive, fill bug reports, even more important, help sorting them out. Important defect are often not seen in a flood of duplicates, variations of the same bug, upstream/downstream errors... "
    author: ""
  - subject: "Thanks for the info"
    date: 2011-12-09
    body: "Ah, thanks.\r\nLet me sugest to add some info to the page like \"login/register [and become an \u00fcbermensch, xD] to vote\" or something similar, it would be very clarifing.\r\n\r\nRegards"
    author: ""
  - subject: "/sign ;)"
    date: 2011-12-10
    body: "KDE releases are somewhat like Christmas or a birthday ;) Someone out there I don't even know has an awesome present for everyone. Love your work!"
    author: ""
  - subject: "KWin great performance improvement"
    date: 2011-12-10
    body: "There is already some background post on KWin developer blog, but after real test, I find it really improve a lot.\r\n\r\nI'm using catalyst with ATI hd 5470 (Yeah, might some other hate it, and it's blacklisted on kwin's direct rendering/OpenGL2 list), and find even with blur enabled, my wine game will run with 60 fps , comparing to previous 40-50 fps. (The optimization is not card specific, even it will boost xrender.)\r\n\r\nThe animation of dolphin is simply a side effect as the developer said. Try list /usr/bin you will find the difference.\r\n\r\nNot to mention that Nepomuk becomes more stable. (The last thing I care is akonadi and telepathy-kde though..)\r\n\r\n4.8 will be a great KDE release."
    author: "csslayer"
  - subject: "What crashes?"
    date: 2011-12-10
    body: "Here's the thing, me and thousands of others are running 4.7 without a single sign of such issues.  It's very hard to fix something that is not broken for 99.99% of people.  Have you been able to track down what is causing these crashes, if so please post them at bugs.kde.org and I'm sure the maintainers will sort it out."
    author: "odysseus"
  - subject: "OMG!"
    date: 2011-12-10
    body: "Oh my god, what's with all that bitching?\r\nAnyway, kwin and dolphin is noticeably faster, thank you.\r\n\r\np.s. to all complainers: please go get a life."
    author: "nickkefi"
  - subject: "Appreciated, but..."
    date: 2011-12-10
    body: "While I like the sentiment of this comment much more than some others, it's still a personal attack and against the code of conduct. Criticize the content but please don't make the jump to criticizing the person. It just feeds a cycle I think we'd all like to break out of."
    author: "areichman"
  - subject: "Why some people don't understand the free software?"
    date: 2011-12-10
    body: "It's not about demand or complaint for some small or big bugs, it's about community and collaboration to make good and fun stuff.\r\n\r\nIf you don't like it don't use it... don't complaint if you're not able to collaborate (there are a lot of ways to collaborate, not just hacking)\r\n\r\nThanks a lot to the KDE community for your efforts and *excellent* pieces of code. I'm using KDE and linux since 1998 and I love the control I have of my computer and the ability to make things exactly as I want (well, I'm a developer).\r\n"
    author: ""
  - subject: "\"Fix multihead\""
    date: 2011-12-12
    body: "I think the \"fix multihead\" means that people (at least me and my friends) want to have two or three displays, single KDE running, but different virtual desktops and activities on every display. \r\n\r\nOr then that user can even change virtual desktops to all screens to be same or how ever wanted. \r\n\r\nNo having just a cloned desktop or extended desktop to all displays isn't what people would expect."
    author: ""
  - subject: "oxygen scrollbar options"
    date: 2011-12-12
    body: "can you put oxygen scrollbar \"bevel\" and \"colorful hovered scrollbar\" back in KDE 4.8?"
    author: ""
  - subject: "Don't mix features and bug fixes"
    date: 2011-12-12
    body: "<cite>want to have two or three displays, single KDE running, but different virtual desktops and activities on every display.</cite>\r\nThis should already work. At least last time I tried I had different virtual desktops on each head.\r\n\r\n<cite>Or then that user can even change virtual desktops to all screens to be same or how ever wanted.</cite>\r\nThis is different. That's no longer a bugfix, but a feature request. Having two different ways (either change on all heads or just on one) means new code, new config options and is non-trivial to achieve."
    author: "mgraesslin"
  - subject: "KDE 3.5"
    date: 2011-12-13
    body: "I went back to take a look at KDE 3.5, it seemed a lot of small programs just worked great like amarok 1.4, Kmix and a few others. I actually liked adept even though it was not fancy, I missed the default sound file in amarok 1.4, in so many ways they just needed to make it faster along the years with ext4 and grub2 and other good things from today's distros not totally start over with a new and better set of bugs."
    author: ""
  - subject: "Do you run OpenSuSE + NVIDIA?"
    date: 2011-12-14
    body: "I've found weird bugs on that combo. Maybe that's biting you. I'd suggest you switch your distro."
    author: ""
  - subject: "They are solving bugs, indeed."
    date: 2011-12-14
    body: "The fancy view engine solved a LOT of keyboard interaction bugs that were lying around since KDE 4.1.\r\n\r\nAlso, the Qt Quick components (specially the Device Notifier) looks better and works faster than the originals.\r\n\r\nSo yes, devs are doing what you suggest and AT THE SAME TIME bringing in some eye candy."
    author: ""
  - subject: "Impressions."
    date: 2011-12-14
    body: "1. Nepomuk: rock solid at last. No reindexings. No breakage. No nothing. Now, Nepomuk team, it's time to focus on performance! And don't forget to iron those usability kinks.\r\n\r\n2. KWin. It gained a little more performance between Beta 1 and Beta 2. It shows. If with Beta 1 I was able to run with blur in my puny GeForce 6150SE, with Beta 2 I can go all the way up and enable the Dashboard blurring. Yes, I get something like 5 fps, but I can't complain since I have a slow video chip indeed (and if I tried to do the same with KDE 4.7, it simply was not possible, so it's a lot faster)\r\n\r\n3. Plasma. Apart from a problem with Microblog that seems to be closer to some Qt patch than Plasma itself, it's going like silk.\r\n\r\nKDE 4.8 beta 2 is having an awesome behaviour. I think it has already a lot less bugs than KDE 4.7.4, while working better."
    author: "Alejandro Nova"
  - subject: "If you are a real Fedoran..."
    date: 2011-12-14
    body: "...you run with the KDE Red Hat repository!\r\n\r\nhttp://kde-redhat.sourceforge.net/\r\n\r\nIf that fails, move your .kde or .kde4 folder to reset your setup."
    author: "Alejandro Nova"
  - subject: "not a solution"
    date: 2011-12-14
    body: "That's more of a work around than a solution. For me, who is running samba4, it's not a solution b/c samba4 doesn't support file share browsing and smb4k requires it to find shares to mount.\r\n\r\nHere's to hoping the kde devs will here this and fix it."
    author: ""
  - subject: "use cases"
    date: 2011-12-14
    body: "I understand all of your arguments about marginal use cases and focusing resources on the mainstream, even if I only partially agree with them.\r\n\r\nTo develop is an activity geared into the future, and while today the use cases for several X are arguably marginal, tomorrow they will not.  All new Intel's CPU have an integrated GPU and running that in parallel with a discrete GPU will become more common on desktops.\r\n\r\nAnother place where this attitude bites KDE IMHO is the trackpad.  The most horrible thing that prevents me from using KDE properly is inadvertent trackpad interference while typing.  Why isn't there a setting to disable the trackpad for X milliseconds after the last keystroke?  I am not talking about complex things such as actually using my touchpad (that according to Acer's marketing is \"multi-gesture\") more than like a 1990 trackpad...\r\n\r\nYou guys keep adding new big \"features\" (nepomuk and akonadi comes to my mind) that are completely uninteresting to me, while forgetting about the small things that actually make or break a desktop.  The result is that KDE feels like 1990 Mac or Windows, even if probably under the hood it is better than a star trek spacecraft.\r\n\r\nThe result?  Linux desktop has insignificant market share and is completely ignored by the rest of the world.  So much for focusing on the mainstream.  The day you will have more than single-digit market share of all desktops in the world, I will be more inclined to agree with your \"focusing the resources on the mainstream\" argument.  Currently, you are not the mainstream and by focusing on the majority of your user base you are not focusing on the mainstream.\r\n\r\nIf the developers need testing hardware, a Z68 mobo with a Core i3, 8GB RAM, and an extra discrete video card is less than $500.  I'd consider participating in a donation drive toward such a setup."
    author: ""
  - subject: "Keep up the great work!"
    date: 2011-12-14
    body: "Been a KDE user since KDE 1.0 1998.\r\nOn rare occasions I hold my breath, Ohhh, what have you done! I am depending on this for my professional work!\r\n\r\nMost of the time things are just purely beautiful.\r\n\r\nInnovation! Bright ideas! Daring design! Redesign from scratch!\r\n\r\nNever a boring moment, always a pleasure, productivity at my fingertips.\r\n\r\nThanks guys!\r\n"
    author: "mikaelb"
  - subject: "A new KDE user"
    date: 2011-12-15
    body: "Hello guys. I've been using gnome till couple of months ago. I never really gave a chance to KDE, since gnome was working well and distributions that shipped with KDE were using software that I was not used to.\r\nSince Gnome 3 and Unity were out,I really had no choice but to try new stuff, and so I tried KDE as well, it was hard at the beginning, I couldn't find stuff and had some frustrating experiences, but after a weak I was starting to get really impressed, I extremely liked the sound application where I could set what I want where, since I have tvcard, 2 web cams, usb headsets with microphone, another microphone, 2 soundcards.\r\nThe stability (from time my taskbar freezes for 30 seconds or so, or a random window becomes unresponsive) could get better, but the overall experience is great, my GF is also very satisfied on hers computer.\r\nIt's good to hear that Dolphin will become more responsive, a great file browser. The only thing I miss from nautilus is \"connect to server\" option, where I could connect to any server through ssh and put it to bookmarks.\r\nAnother thing what I'd love being able to set is the default terminal window size when I start it, it's too big by default and I love having lot of them open.\r\nIn any case, you've got two new users who are quite satisfied, it seems that other projects take users freedom, and you try to expand it, that's what I like.\r\nOh yes, one more thing, I'm not sure if it is a general case, but scripts that have +x permission should not be run by double click before giving a warning first."
    author: ""
  - subject: "even with xandr , etc, kde4"
    date: 2011-12-16
    body: "even with xandr , etc, kde4 is failing.\r\nDual screen is used a lot with laptops & docking stations.  But KDE4 doesn't \"accept\" such configuration.\r\nyou cannot undock and continue on the laptop screen.\r\nEven not after reboot, as he expects a second screen and then you are stuck as a normal user.\r\nSomething basic, and this setup is used by hell a lot of people with a laptop, and even that KDE4 cannot do.\r\n"
    author: ""
  - subject: "smb support"
    date: 2011-12-17
    body: "What application are you using to stream the movie through smb ? \r\n\r\nAny KDE application that uses KIO directly should not have such issues. And all non-KDE applications that have built-in support for remote access protocols and inform KDE about it through their .desktop files by including a \"X-KDE-Protocols\" [1] should not have any issues streaming content either. \r\n\r\nSo unless you are using an application that does not support either one of the things mentioned above, this should work. Obviously, if you are using a non KDE application that relies on Gnome's GVFS approach of mounting remote resources locally, there is nothing KDE can do about that. We most definitely are not going to change KIO to map (read: mount) remote resources like GVFS does.\r\n\r\n\r\n[1] https://bugs.kde.org/show_bug.cgi?id=253547. "
    author: ""
  - subject: "Debian link on 4.7.4 info page"
    date: 2011-12-21
    body: "Hi,\r\n\r\nThere is no link for Debian packages under <a href=\"http://www.kde.org/info/4.7.4.php#binary\">KDE 4.7.4 info page</a>. KDE 4.7.4 Debian packages are available in <a href=\"http://wiki.debian.org/DebianExperimental\">experimental repository</a> and work fine. The same issue occurs with 4.7.2.\r\n\r\nSincerely,\r\n\r\nDaniel\r\n"
    author: ""
  - subject: "Connect to server"
    date: 2011-12-22
    body: "There is a similar function in KDE: open \"Network\" in \"Places\" sidebar and use \"Add network folder\".\r\nThat'll do the trick. Unfortunately it uses KIO only, so there is a performance price to pay (even if some people carry on posting that it \"shouldn't\").\r\n"
    author: ""
---
Today, KDE makes available two new releases of its Workspaces, Applications and Development Platform. <a href="http://www.kde.org/announcements/announce-4.7.4.php">4.7.4</a> provides bugfix updates, new translations and performance improvments on top of the stable 4.7 series, while <a href="http://www.kde.org/announcements/announce-4.8-beta2.php">4.8 Beta2</a> gives a glimpse at what is coming in 4.8, to be released next month.
As 4.7.4 is limited to low-risk fixes, it is a recommended update for everyone using 4.7.3 or earlier versions. 4.7.4 will be the last release of the 4.7 series. The next stable release of KDE's Workspaces, Applications and Development Frameworks will be 4.8.0, which will be ready in January 2012. Among the highlights which will be in 4.8 are a new and fancy view engine for Dolphin, Qt Quick Components for Plasma, and of course all the fixes that went into 4.7.4.
<!--break-->
