---
title: "conf.kde.in Opens in Bengaluru"
date:    2011-03-10
authors:
  - "jriddell"
slug:    confkdein-opens-bengaluru
comments:
  - subject: "Qt Open Governance"
    date: 2011-03-11
    body: "I don't doubt the Trolls' intentions to transform Qt development to an open governance model, but it is great to hear about it from time to time, and even more so to know that there is an interest on behalf of Nokia beyond its Qt software engineers. Good days to come. :)"
    author: "mutlu"
---
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex" />
<a href="http://www.flickr.com/photos/jriddell/5512762850/"><img src="http://farm6.static.flickr.com/5096/5512762850_d3eec36d18_m.jpg" width="240" height="180" /></a><br />
Lighting the Auspicious Lamp
</div>
The first KDE and Qt conference in India has opened today in Bengaluru.  <a href="http://conf.kde.in">conf.kde.in</a> Conference has attendees from around the world as well as every state in India.  Over 300 people were at the opening talks and many had to be turned away because the lecture theatre was full.  The conference was opened by its main organiser Pradeepto Bhattacharya who introduced the dignatories K.N Raja Rao, Advisor for R.C.College of Engineering, B.S Satyanarayana, Vice-principal of the college and Sumithra Devi.K.S, Director of Master of Computer Applications Dept.   The Lighting of the Auspicious Lamp ceremony was performed to open the conference.
<!--break-->
The attendees include KDE contributors, commercial software developers both who work with Qt and who want to learn about it, but the majority are students from Bengaluru and around India who use KDE & Qt and want to learn how to be part of the community and contribute.

The first session was by Lydia Pintscher who set the keynote for the conference with her talk "So Much to do, So Little Time".  She introduced KDE and spoke about the elements that make it special, the high quality software, breadth of the project and most importantly the community.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex" />
<a href="http://www.flickr.com/photos/jriddell/5512766324/"><img src="http://farm6.static.flickr.com/5020/5512766324_ff16a9ba41_m.jpg" width="240" height="180" /></a><br />
Eager Tutorial Audience
</div>
The majority of the day was an introduction to Qt programming.  Prashanth Udupa and Abhishek Patil from V Create Logic took the audience of students from a simple "Hello World" to a useful image browser programme.  Introducing the vital basics of Qt such as signals/slots, Designer and the most common classes.  With 150 students now keen to take on their Qt learning we can expect some new talent from India coming into KDE.

e.V. Board member Ade gave a talk on e.V. explaining why there is a need for a formalised body which can represent KDE.  There are tigers out there he said and the community needs to be defined in order to prevent anarchy, even if much of that definition is unwritten.

The conference is sponsored by Forum Nokia, who are the worldwide developer network from Nokia.  Qt Developer Frameworks.  Alokin, a software consultancy company.  V Create Logic, a product and technology service company who specialise in open source consultancy.   And Spoken Tutorial who promote open source throughout the country funded by the government of India.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex" />
<a href="http://www.flickr.com/photos/jriddell/5512766874/"><img src="http://farm6.static.flickr.com/5052/5512766874_eb761c39f9_m.jpg" width="240" height="180" /></a><br />
Nepomuk semantically links people who look alike
</div>

Vishesh Handa spoke about Nepomuk.  Although he told us that Nepomuk is not about searching he started with an impressive demo he had coded the night before which searches video subtitles to offer you the video you are looking for in your collection.  

Siddharth Sharma gave a talk on Plasma Dashboard in Skrooge. Skrooge makes use of Plasma to include Plasmoids in its UI which show different views on your financial data.  Siddharth explained the code needed to embed Plasma Dashboard inside any KDE Application.

The closing keynote of the day was "Open Governance" from Knut Yrvin, the community manager from Qt.  He showed the current contribution model for Qt and announced that the Qt management in Nokia have given them approval for open governance.  The new model will be equal and fair to all contributors and based around the concept of do-ocracy.  He could not give a timetable for the changes but did say there are large pressures to make it happen soon and we should harass him in six months if it has not been implemented.  

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex" />
<a href="http://www.flickr.com/photos/jriddell/5512169311/"><img src="http://farm6.static.flickr.com/5172/5512169311_0d8479a1bd_m.jpg" width="240" height="180" /></a><br />
Ooh, monkeys!
</div>
Those of us who are visiting India for the first time have been enthralled by the busy city, the temples of Mysore, the excitement of having your lecture attended by monkeys and the finest curry for three meals a day.  The next two days will be busy with a stream of talks, two streams of workshops and two lab sessions running both days. With the labs continuing at the weekend.