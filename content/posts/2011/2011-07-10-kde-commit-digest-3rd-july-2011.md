---
title: "KDE Commit-Digest for 3rd July 2011"
date:    2011-07-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-3rd-july-2011
comments:
  - subject: "The CMYK support seems to be"
    date: 2011-07-10
    body: "The CMYK support seems to be for Krita's .psd filter. Krita itself has had a CMYK colorspace for quite a while."
    author: "illissius"
  - subject: "Public Transport Widget"
    date: 2011-07-11
    body: "The Public Transport Widget is one of the most useful plasmoids I have seen (if not THE most useful).\r\nThanks for the work on this!\r\n\r\nHopefully it'll be included in (K)Ubuntu someday. It's not that easy to get up to date versions right now..."
    author: ""
  - subject: "yes..."
    date: 2011-07-11
    body: "I just added some convenience methods for Siddharth's summer of code project -- which indeed is reading/writing psd files. Krita has had cmyk support since at least version 1.5 :-)."
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-07-03/">this week's KDE Commit-Digest</a>:

<ul>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, CMYK color support in Krita, improvements in Web widget of Kexi, updated RTF support and bugfixes</li>
<li>Multiple improvements in <a href="http://userbase.kde.org/Plasma/Public_Transport">Public Transport Widget</a>, including a new filter structure and the possibility to highlight a stop</li>
<li>Addition of development mode for system tray, DMS query optimization and other changes in <a href="http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/">Nepomuk</a></li>
<li>Addition of manual creation of plain text emails in <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a></li>
<li><a href="http://amarok.kde.org/">Amarok</a> now compiles with Clang</li>
<li><a href="http://userbase.kde.org/Marble">Marble</a> sees memory optimization in offline maps and GSoC work</li>
<li>Fixes in font display, non-local URLs and other changes, removal of the last OpenGL code from kwineffects and fixes in global shortcuts in <a href="http://www.kde.org/workspaces/">KDE's Plasma Workspaces</a></li>
<li>Fixes in <a href="http://userbase.kde.org/KMail">KMail</a>, <a href="http://userbase.kde.org/KAlarm">KAlarm</a>, <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://www.kde.org/applications/multimedia/juk/">Juk</a>, <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a>, and <a href="http://www.kdevelop.org/">KDevelop</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-07-03/">Read the rest of the Digest here</a>.