---
title: "KDE Commit Digest for 20 February 2011"
date:    2011-03-05
authors:
  - "vladislavb"
slug:    kde-commit-digest-20-february-2011
comments:
  - subject: "Amazing!"
    date: 2011-03-05
    body: "Another great week! I am looking forward to many of the things in this list. And voice navigation in Marble? Impressive."
    author: "tangent"
---
In <a href="http://commit-digest.org/issues/2011-02-20/">the latest KDE Commit-Digest</a>:

              <ul><li>New "processor information" page and FreeBSD fixes in KInfoCenter</li>
<li>Calendar fixes for the Indian National Calendar</li>
<li>Work in system-config-printer-kde</li>
<li>Fixes throughout kdelibs including crashes due to lack of disk space</li>
<li>New charsets and a charset conversion tool in <a href="http://utils.kde.org/projects/okteta/">Okteta</a></li>
<li>Support for Mercurial and BitBucket in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Voice navigation support in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>New Bulgarian keyboard layout in <a href="http://ktouch.sourceforge.net/">Ktouch</a></li>
<li>New plugin "Generate Graph" in <a href="http://www.kde.org/applications/education/rocs/">Rocs</a></li>
<li>Work in <a href="http://gluon.gamingfreedom.org/">Gluon</a> including the start of an animated sprite renderer</li>
<li>More work on Color Labels and Toolbar layout in Digikam</li>
<li>Dependency on libplasma removed from kwineeffects library in KWin</li>
<li>Workthroughout Plasma and Plasma-mobile</li>
<li>Start to annotation cleanup in Nepomuk</li>
<li>Updated holidays for Korea, Croatia, UK, and Hungary in KHolidays (KDE-PIM)</li>
<li>Improved PPT, DOC, and DOCX support and work on SmartArt in Calligra amongst bugfixing and optimizations</li>
<li>Krdc and Krfb ported to new Telepathy</li>
<li>New "networkmanagement" dataengine and bugfixes in Knetworkmanager</li>
<li>Performance optimizations to previews in Dolphin</li>
<li>Bugfixing in <a href="http://kde-apps.org/content/show.php/Kamoso?content=111750">Kamoso</a>.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-02-20/">Read the rest of the Digest here</a>.