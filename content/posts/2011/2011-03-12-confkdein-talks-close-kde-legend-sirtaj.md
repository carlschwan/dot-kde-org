---
title: "conf.kde.in Talks Close With KDE Legend Sirtaj"
date:    2011-03-12
authors:
  - "jriddell"
slug:    confkdein-talks-close-kde-legend-sirtaj
comments:
  - subject: "Wow..."
    date: 2011-03-13
    body: "Wow, I've many times wondered what happened to Sirtaj, he really is a legend around these parts.  What is he up to these days?  \r\n\r\nHmmm, wonder if that's a BehindKDE series in the making, profiling some of the old legends to see where they ended up,and assessing how their KDE experiences helped them to achieve what they are today."
    author: "odysseus"
  - subject: "Great Conference"
    date: 2011-03-13
    body: "This seems like a great place to be. I am very happy about this first Indian (and even Asian) KDE conference and hope there are more to come."
    author: "mutlu"
---
<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 240px" />
<a href="http://www.flickr.com/photos/jriddell/5519171984"><img src="http://farm6.static.flickr.com/5091/5519171984_eb4c811b4d_m.jpg" width="240" height="180" /></a><br />
<a href="http://www.flickr.com/photos/jriddell/5519171984">conf.kde.in Group Photo</a>
</div>
conf.kde.in talks have finished after a busy three days packed with inspiration and knowledge.  Closing the conference was keynote speaker and old-time KDE developer Sirtaj.  He spoke from his personal experience being a KDE developer from the start of the project.  Working on an open source project gives programming students the confidence and knowledge necessary for real world programming.  It will improve your employment chances massively because you can show you can produce results.  On top of that it is a great way to make friends and travel the world.  He said that KDE does all this best out of all the open source projects.  Read on for more talks.
<!--break-->
In the first of two KDE Edu talks Anne-Marie introduced the projects and the range of applications it includes.  Stephanie G made the case for more Indian content in KDE Edu.  Many KDE Edu applications need locale specific content written for them such as KGeography.  She showed a video of Bijra High School in a remote village near where she lives.  The school had recently received computers which run Kubuntu with KDE Edu.  The pupils explained the range of uses they made of the computers which run in Bengali.  They were also offered some Windows computers but the pupils and staff rejected them because they were had no software in a suitable language and, having tasted KDE Edu, they didn't want to use anything else.

There have been tutorials on a number of topics.  Frederick Gladhorn gave one on Qt Quick, covering the QML language and how to use it to make dynamic designer led user interfaces.  A Calligra talk by Mani Chamdrasekar showed how Calligra can be embedded in any Qt application to use OpenDocument similar to how WebKit can be embedded.  This was followed by a tutorial taking participants through creating an application which includes Calligra.  Jonathan Riddell took students through the lengthy journey of creating an application in PyKDE, the various fiddly bits to get that into a state where it can be released, then packaging it and putting it into Kubuntu.  Other tutorials included Qt Model/View and VCreate Logic's Generic Component Framework.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 180px" />
<a href="http://www.flickr.com/photos/jriddell/5518580717/"><img src="http://farm6.static.flickr.com/5180/5518580717_c29e14193c_m.jpg" width="180" height="240" /></a><br />
Sirtaj, one of the first non-German KDE developers
</div>

At the end of the second day we had a change, a talk about Chamba, the new open movie from the Blender Foundation being made in India.  On a straw poll most of the audience share films, but of course this is generally not legal.  To fix this the Foundation funds projects to create free films.  The point was made that technology has changed with the internet and widespread use of computers, so copyright no longer works.  The Blender Foundation is the only project to create films that no longer have to care about sharing restrictions.

Our closing keynote for the second day had Eugine Trounev showing us some art he had made on the aeroplane to India.  Open source drawing tools are often the best available now and the impressive pictures of temples and dragons he had done in a short time impressed on the audience how true this was.  

The third day had a strong showing from the distrubtions with talks on OpenSUSE, Fedora and Kubuntu.  Will Stephenson gave a talk to a crowded auditorium about the openSUSE build service and how it can be used to spread KDE for many different distrubutions.  He was kept busy afterwards with questions from developers wanting to know how to run their own instances and build software.  At the end of the talk a power cut struck, a melee ensued as the audience rushed to grab the t-shirts being given away, Will was lucky to escape with the clothes on his back.  Jonathan Riddell spoke about his ideas for achieving world domination with KDE and Kubuntu, he challenged the audience to develop innovative devices and make use the Plasma to provide the necessary bling to the user interfaces.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 300px" />
<a href="http://dot.kde.org/sites/dot.kde.org/files/lunchtime-discussion.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/lunchtime-discussion-wee.jpeg" width="300" height="150" /></a><br />
Lunchtime discussions
</div>

There was a continuous stream of lab sessions covering writing a first KDE application and bug squashing. Over 250 students are now trained up in these important tasks and we can expect to see them around KDE shortly.

The conference continues with two days of hacking plus more lab sessions.  Many thanks to Pradeepto for organising this successful first KDE conference in Asia.