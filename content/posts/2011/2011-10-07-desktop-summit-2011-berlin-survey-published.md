---
title: "Desktop Summit 2011 Berlin survey published"
date:    2011-10-07
authors:
  - "kallecarl"
slug:    desktop-summit-2011-berlin-survey-published
comments:
  - subject: "DS survey"
    date: 2011-10-22
    body: "Excellent work! Compliments and thanks to those who put the survey together, distributed it, and collated the results. I had a blast, learned a lot, and look forward to helping out with the next event.\r\n\r\nThanks again, all you wonderful, hard-working people who made the DS happen."
    author: ""
---
The results of the Desktop Summit 2011 survey have been published at the <a href="https://desktopsummit.org/news/desktop-summit-survey-report">Summit website</a>. The overall feedback was positive, and there were suggestions for improving future Desktop Summits.

Desktop Summit 2011 Berlin promoted collaboration between open desktop projects with attendance dominated by the GNOME and KDE communities. There were several requests to make a stronger appeal for people from other desktop projects to attend. 