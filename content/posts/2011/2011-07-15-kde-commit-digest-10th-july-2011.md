---
title: "KDE Commit-Digest for 10th July 2011"
date:    2011-07-15
authors:
  - "vladislavb"
slug:    kde-commit-digest-10th-july-2011
---
This week, the Commit Digest includes an article on the new Share Like Connect project and recent work done on Nepomuk for KDE Release 4.7.

Also in <a href="http://commit-digest.org/issues/2011-07-10/">this week's KDE Commit-Digest</a>:

              <ul><li>Work on <a href="http://en.wikipedia.org/wiki/C%2B%2B0x">C++0x</a> support in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Flag-based annotation and an image export dialog are added to <a href="http://edu.kde.org/kstars/">KStars</a></li>
<li><a href="http://edu.kde.org/marble/">Marble</a> gains read-only routing support in <a href="http://en.wikipedia.org/wiki/QML">QML</a>, Qt components and OpenCaching plugins and a QML-Meego interface</li>
<!--break-->
<li>A panorama wizard is added to the <a href="http://www.kipi-plugins.org/">Kipi plugins</a></li>
<li>A new activity manager arrives that uses <a href="http://community.kde.org/Plasma/Active">Plasma Active</a> and a basic task manager using QML</li>
<li>KDE's font installer can now download fonts via <a href="http://ghns.freedesktop.org/">GHNS</a> (Get Hot New Stuff)</li>
<li>The webbrowser in <a href="http://community.kde.org/Plasma/Plasma-Mobile">Plasma Mobile</a> gets working web shortcuts</li>
<li>The <a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a> Web Extractor has been <a href="http://techbase.kde.org/Projects/Nepomuk/PortCoreToDMS">ported</a> to <a href="http://trueg.wordpress.com/2011/06/08/nepomuk-2-0-and-the-data-management-service/">DMS</a> and sees work on various features</li>
<li>In <a href="http://krita.org/">Krita</a>, the freehand tool is ported to the <a href="http://community.kde.org/Krita/Strokes_Framework">Strokes Framework</a> which improves responsiveness and possibly speed</li>
<li>Also in Krita, a preview of changes made to the layer is implemented, support for OpenRaster is improved and many smaller improvements to the user interface are made</li>
<li>The Krita animator plugin receives loop support for its player and improved performance</li>
<li>Calligra <a href="http://www.calligra-suite.org/words/">Words</a> receives improvements to the insertion of citations and the bibliography management</li>
<li>In <a href="http://www.kexi-project.org/">Kexi</a>, mobile plugin editing and saving of records is implemented</li>
<li>The SSL dialog in <a href="http://rekonq.kde.org/">Rekonq</a> is improved</li>
<li>Much work is done on <a href="http://gluon.gamingfreedom.org/">Gluon</a></li>
<li><a href="http://owncloud.org/">OwnCloud</a> gets various new features, including file synchronization</li>
<li>Performance optimization in Marble, <a href="http://www.digikam.org/">digiKam</a>, Strigi and KHTML</li>
<li>The borders around game canvases are removed in <a href="http://games.kde.org/">KDE Games</a></li>
<li>Many bugfixes in Kate, Calligra, Amarok and to a lesser degree in Nepomuk,  <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://kmymoney2.sourceforge.net/">KMyMoney</a>, <a href="http://userbase.kde.org/NetworkManagement">Network Management</a> and <a href="http://kopete.kde.org/">Kopete</a>.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-07-10/">Read the rest of the Digest here</a>.