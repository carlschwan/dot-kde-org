---
title: "KDE Commit Digest for 22 May 2011"
date:    2011-05-27
authors:
  - "mrybczyn"
slug:    kde-commit-digest-22-may-2011
comments:
  - subject: "Cool"
    date: 2011-05-27
    body: "It's great that the Commit Digests are timely again. Keep it up!"
    author: ""
  - subject: "Awesome!"
    date: 2011-05-31
    body: "IDEM++"
    author: "1antares1"
  - subject: "Kst"
    date: 2011-06-02
    body: "Today, I learned about Kst. Thanks! :)"
    author: ""
---
This week, the Commit Digest includes a featured article about <a href="http://gluon.gamingfreedom.org/about-gluon">Gluon</a>, the free platform for creating and distributing games.

Also in <a href="http://commit-digest.org/issues/2011-05-22/">this week's KDE Commit-Digest</a>:

<ul><li>First implementation of a user secret agent in <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> along with further work on NM-0.9 support</li>
<li>Better guessing of zoom levels and an offline address search database generator in <a href="http://edu.kde.org/marble/">Marble</a></li>
<!--break-->
<li>Work on the <a href="http://www.kdevelop.org/">KDevPlatform</a> shell environment and annotation bar</li>
<li>Improved hotpluggable devices support in <a href="http://amarok.kde.org/">Amarok</a> along with much bugfixing</li>
<li><a href="http://api.kde.org/4.x-api/kdelibs-apidocs/">KDELibs</a> receives much bugfixing and optimization throughout, including optimized timezone functions</li>
<li>Groups support added to <a href="http://martys.typepad.com/blog/2011/04/quick-update-on-kde-telepathy-contact-list-now-with-groups-too.html">Telepathy-Contact-List</a></li>
<li>Further bugfixing throughout <a href="http://userbase.kde.org/KWin">KWin</a>, <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://www.calligra-suite.org/">Calligra</a>, <a href="http://kst-plot.kde.org/">Kst</a>, and <a href="http://rekonq.kde.org/">Rekonq</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-05-22/">Read the rest of the Digest here</a>.