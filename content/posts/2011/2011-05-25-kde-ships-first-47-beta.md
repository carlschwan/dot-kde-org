---
title: "KDE Ships First 4.7 Beta"
date:    2011-05-25
authors:
  - "sebas"
slug:    kde-ships-first-47-beta
comments:
  - subject: "The link to the \"4.7 Beta1"
    date: 2011-05-25
    body: "The link to the \"4.7 Beta1 Info Page\" is broken."
    author: ""
  - subject: "Fixed, thanks :)"
    date: 2011-05-25
    body: "Fixed, thanks :)"
    author: "sebas"
  - subject: "Good news!!!"
    date: 2011-05-25
    body: "Cheers!!! :-)"
    author: ""
  - subject: "Dolphin historial and split"
    date: 2011-05-25
    body: "Will Dolphin finally have a damn historial like any common sense made browser on earth does, or are we going to be forced once more to click 9 times the \"back\" arrow if we want to go back 9 steps?\r\nAnd what about horizontal split? Will we to once and for all be able read our files' names without the need to maximize horizontally Dolphin's window?\r\n\r\nRegards and thanks a lot for your hard work, even if paying attention to users (these features have been requested several times in the forum and bugs.kde) seems to cause hives to some devs."
    author: ""
  - subject: "PIM"
    date: 2011-05-25
    body: "What about PIM? It was already promised a year ago. I am eagerly waiting for the new KMail, especially for IMAP IDLE. And I hope that my address book, recently used addresses, and LDAP will work flawlessly together.\r\n\r\nAlso pim.kde.org isn't really helpful, I guess my address book problems might be easy to solve.\r\n\r\nWill there be a new KMail in 4.7?\r\n\r\nOtherwise congratulations for the new release. I love KDE!"
    author: ""
  - subject: "kmail2"
    date: 2011-05-25
    body: "what about kmail2?"
    author: ""
  - subject: "KDEPIM 4.6 RC1 has just been"
    date: 2011-05-25
    body: "KDEPIM 4.6 RC1 has just been released, and hopefully the final 4.6 release will happen next month for early adopters.  Then KDEPIM will return to the standard KDE release cycle with 4.7 which should be good for most users."
    author: "odysseus"
  - subject: "as far as I know kde 4.7 is"
    date: 2011-05-26
    body: "as far as I know kde 4.7 is the first kde 4 release that support multiple x (one X per screen). I feature I needed sin xrander etc dont support 2 graphics cards. \r\nSo (hopefully) kde 4.7 will be the first kde for release that is ready for my desktop (just in time for the mess before the transaction kde4->kde5 and X->wayland. I dont hope that those transaction would break anything)"
    author: ""
  - subject: "Please add some more info, and links are broken!"
    date: 2011-05-26
    body: "Hi,\r\n\r\nI want you to look at the two following issues:\r\n\r\n- the links on the downloadpage are broken. They point to stable/4.6.3.\r\n\r\n- please add some more info about new list of packages, this list is much bigger than it was. I still see kdelibs, kdeworkspace etc. but also a lot more. What has happened??\r\nAnd as someone who builds everything from source, what is/are the dependencies and optional and recommended software (like qt, is 4.8 required)?\r\n\r\nStef\r\n\r\n\r\n"
    author: ""
  - subject: "Hear, let's do something really useful..."
    date: 2011-05-27
    body: "Please, instead of many glittering announces in this horrible marketing-style language (better this, faster that, and flexible, vitual, proactive user experience and the like)... just tell us that in 4.7 you will finally throw Akonadi, Nepomuk, and the whole semantic shit outside the window (or AT LEAST disabled by default), and make your userbase really happy.\r\n\r\nPlease!"
    author: ""
  - subject: "I'm part of the userbase, and"
    date: 2011-05-27
    body: "I'm part of the userbase, and \"Akonadi, Nepomuk, and the whole semantic shit\" is what I'm looking forward the most in KDE development. I can't wait for it to mature and its potential to be fully explored by desktop and applications. There's quite a few lightweight DE's these days, if that's what you want feel free to use them. "
    author: "jadrian"
  - subject: "I have been a KDE user since"
    date: 2011-05-28
    body: "I have been a KDE user since 1.0. It was raw as hell but far more solid than any KDE4 has ever been.\r\nI use my desktop for my job, not spare time. I need something working, I just cannot afford any more to live in the eternal alpha-state that KDE4 has been since the beginning, battling with developers constantly spitting out new big features without consolidating any.\r\nEspecially if they are \"big\" mostly in RAM/CPU/HD (see Akonadi/Nepomuk/Strigi/Soprano/Virtuoso) and offer piles of useless \"integrations\" and \"frameworks\" but are barely able to replicate the core features of much simpler solutions\r\n\r\nIf you can wait for new technologies to mature and consolidate WHILE you are using it, crash after crash, slowdown after slowdown, crap after crap, good for you.\r\n\r\nI now need a KDE4 at least resembling a beta if not a GM. If it's not possible, or, as you say, not even wanted, I'll take the advice and go for the lightweight.\r\n\r\nSorry, and so long."
    author: ""
  - subject: "I have also been a KDE user"
    date: 2011-05-28
    body: "I have also been a KDE user since 1.0, in fact pre 1.0. Again, if you want a simple and solid desktop, there are quite a few available. Why not use those instead of complaining about KDE not being the kind of desktop you want? \r\n\r\nPersonally my desktop usage is rather basic at this point. I've had no crashes nor CPU problems with 4.6.3. To me it feels rock solid if all you require is the functionality of a simple desktop. And I've got both enabled, Nepomuk and Strigi. But if you have problems with them why not just disable them?\r\n\r\nQuite frankly, some time ago I stopped using KDE due to issues with nvidia's  260.x driver series. I moved  to LXDE and it didn't bother me much. I missed some desktop effects which are rather functional, and I missed being able to find pdf's by pressing Alt+F2 and typing related info (author, or title or whatever). That was pretty much it. So these are the kind of things that keep me a KDE  user at this point. So, for the last time, if you all you want is something basic and rock solid, just pick one from the many available. "
    author: "jadrian"
  - subject: "I agree."
    date: 2011-05-28
    body: "I must notice here that a negative opinion about Nepomuk and Akonadi is allowed, and does not have to mean you have to go somewhere else.\r\n\r\nI agree that Akonadi is not very useful for me. All the contacts, and email is done via the web, so a local daemon storing data in a local database is not very usefull for me.\r\n\r\nI think that there are a lot of users having this data on the \"net\" instead of local. \r\n\r\nStef"
    author: ""
  - subject: "That's one of the biggest"
    date: 2011-05-28
    body: "That's one of the biggest challenges the KDE community faces. Any motivated kid can join the project and start submitting patches and making architectural decision completely ignoring the voice of long time users. I have been using KDE since pre 1.0 too, doing my part as beta tester and submitting many many bugs reports. However, when I criticize some of the recent decisions I get told to shut up or start coding by people who have been involved with KDE for just 2 or three years. \r\n\r\nKDE is introducing way more bugs than it can fix, and the whole metadata thing is terribly ill-conceived. At least make all those dubious features optional. I have no way to really disable akonadi without recompiling KDE, and every time I log in, I'm being greeted by a message telling me that strigi or nepomuk is disabled and that I'm missing many wonder features, WTF!!\r\n\r\n"
    author: "trixl."
  - subject: "I am just sorry because 3.x"
    date: 2011-05-28
    body: "I am just sorry because 3.x has been an excellent experience: fast, solid, and still stuffed with good features and clever hacks. \r\n\r\nThat is: nothing like KDE4. \r\n\r\nMy every day experience with 4.6.3: Plasma crashes every once and than (have to restart KDE), kwin crashes every once and than (have to restart kwin and loose session), aurorae corrupt the screen (have to restart kwin and loose session), phonon continues the silly \"new hardware detected\", \"old hardware disappeared\" over and over, Kopete duplicates my contacts every time the jabber server has some problem, knotify goes berserk giving me tens of old messages altogether every once and then. Powerdevil simply and plain doesn't work, Device notifier thinks I'm missing something in dbus but doesn't say what, and I could go on and on and on for hours.\r\n\r\nAnd then Kontact... my dear, what once was the better e-mail client around: takes all of my CPU for ~20min half the times I start it, and sometimes closes without even a warning and sometimes kio_pop3 just hangs (and you think that you have no mail, because there is no warning). At every restart Kontact thinks there is another Kontact running, or find Akonadi stopped, and complain, or mysql gets corrupted, or starts full reindexing of all my emails (= workstation unuseable for minutes) or creates undeletable duplicates of new messages in the INBOX... AND. SO. ON. \r\n\r\nAnd this, on each one of the different notebooks and wokstations (mostly opensuse, but kubuntu does not seem any better) we use at work.\r\n\r\nNot to speak about components which has never reached the feature completeness of their KDE3 counterparts (kprinter an ksysguard comes to mind). After how many years? 5? 6?\r\n\r\nJust a last note for the oldies. I complain because I truly miss the \"we can do better than CORBA in 1 day\" attitude. \r\nKDE  developers can surely do better than Akonadi in 1 day... (in fact, in the current status, a monkey could!!) but now they prefer the way of abstracting, frameworking and over-over-over-engineering: silly because that's exactly the CORBA way. \r\n\r\nBut, I know, creating new frameworks and buses is much much funnier than the dirty job of writing code that actually do something, or (gasp!) fixing bugs.\r\n\r\nBefore you start, I know the songs: \"it's opensource, fix it yourself\" and \"there's plenty of alternatives, choose one\".\r\nI'll take the latter: LXDE looks promising, thanks for the advice."
    author: ""
  - subject: "absurd"
    date: 2011-05-28
    body: "I used KDE since 1999... I had it running on sun ultrasparc stations -- And I had to compile and install it as a user, and run it from /tmp: good times :). So there.\r\n\r\nI have the KDEPIM beta running on a machine and the older version running on another. And I am really looking forward to the day where both run akonadi: it is so much faster. IMAP PUSH is a game changer. People complaining about akonadi really complain about the essentially non-working (as in doing nothing) version that shipped a while ago. But the version in trunk is just _good_.\r\n\r\nAlso, I really like the nepomuk integration: I like typing alt-F2 and some filename, and having the system retrieve the email with the right attachment. Of course, I like to think my files are well organised. But I have literally hundreds of thousands of them! \r\n\r\nAlso, you are really complaining about a pop-up, which clearly should not be there, as it clearly is not user friendly. But this has nothing to do with the architecture. I am sorry, but how is it that an old user should be listened to about software user versus a young developer? This makes no sense. Users can legitimately complain about crashes, interface blunders, missing functionality, but they should shut up about architecture decisions. If they knew about architecture, they would be developers!"
    author: ""
  - subject: "pfff"
    date: 2011-05-28
    body: "The day I can type alt-f2, and a string, and have a list of attachments pop-up, I'll accept that lighter desktops have caught up. \r\n\r\nBut that day will never come, as that is only possible through integration and frameworks... In the mean time, if you need an application launcher, enlightenment is for you."
    author: ""
  - subject: "You rock :)"
    date: 2011-05-29
    body: "Just checking out the new stuff after installing the beta from Archlinux [kde-unstable] repo.\r\n\r\nThanks for your great work :)\r\n"
    author: ""
  - subject: "Thanks !"
    date: 2011-05-29
    body: "Thanks for the hard work, guys ! "
    author: ""
  - subject: "You lost me"
    date: 2011-05-29
    body: "I'm using KDE since 1.x (on Suse, what was it, 7?). I've donated money, bought T-Shirts, supported it in my peer group whenever I felt it was necessary.\r\n\r\nThree weeks ago, my harddisk crashed and I was forced to do a clean re-install on a new drive. So I had the opportunity to try out different distributions.\r\n\r\nI used to use Debian and then Kubuntu a lot. However, KDE in Kubuntu really has a bad performance overall (sluggish, un-elegant). Remembering the \"good old times\", I tried Opensuse. Installation was great, default configuration was way better then Kubuntu... but... it was still sluggish.\r\n<ul>\r\n<li>Strigi and Nepomuk kept hammering my system.</li>\r\n<li>Amarok crashes on mobile devices and is everything but a decent media player.</li>\r\n<li>I don't understand the principle of akonadi and I don't want to mess around with it (it never, ever worked on my system, never!). I want to have a decent PIM. It fails miserably on delivering that.</li>\r\n<li>Plasma is slow and sluggish (redraws) even with a Nvidia GTX8800 and binary drivers enabled.</li>\r\n<li>I understand quantum physics but I don't understand activities. I really don't. I have tried, but I failed.</li>\r\n<li>\"Zooming out\" of activities creates 100% CPU-usage SINCE 4.0. There has never been an improvement.</li>\r\n<li>There's so much work needed in detail (default sizes of windows and dialoges, clear user interface guidelines that don't only exist on paper but are followed).</li>\r\n</ul>\r\n\r\nYes, I know the answers:\r\n\r\n<ul>\r\n<li>It can be configured.</li>\r\n<li>Have you filed a bug report?</li>\r\n<li>KDE is not for you...</li>\r\n</ul>\r\n\r\nGuess what - I know that I can configure it. I have filed bug reports. I like the ideas behind the introduced technologies. But I'm really fed up of fiddling around, of messing with new technologies that simply do not work at all, of failing in the area of basic features while adding more and more stuff on top of them.\r\n\r\nEnd of story - I installed Ubuntu 11.04 with unity. While it is not perfect it does not demand heavy tweaking. It has lesser features than KDE but they are of high quality. It has the amount of polish no KDE-installation has ever provided (and I'm using it since 1.x...).\r\n\r\nI've never thought that it will come this far, I don't want to discourage anyone of you, I'm even a bit sad because I still somehow love KDE and the folks behind it, but KDE (and Amarok, by the way) has lost me. I just can't do actual work with it anymore. I just can't. For now, I give up."
    author: ""
  - subject: "You lost me"
    date: 2011-05-29
    body: "I'm using KDE since 1.x (on Suse, what was it, 7?). I've donated money, bought T-Shirts, supported it in my peer group whenever I felt it was necessary.\r\n\r\nThree weeks ago, my harddisk crashed and I was forced to do a clean re-install on a new drive. So I had the opportunity to try out different distributions.\r\n\r\nI used to use Debian and then Kubuntu a lot. However, KDE in Kubuntu really has a bad performance overall (sluggish, un-elegant). Remembering the \"good old times\", I tried Opensuse. Installation was great, default configuration was way better then Kubuntu... but... it was still sluggish.\r\n<ul>\r\n<li>Strigi and Nepomuk kept hammering my system.</li>\r\n<li>Amarok crashes on mobile devices and is everything but a decent media player.</li>\r\n<li>I don't understand the principle of akonadi and I don't want to mess around with it (it never, ever worked on my system, never!). I want to have a decent PIM. It fails miserably on delivering that.</li>\r\n<li>Plasma is slow and sluggish (redraws) even with a Nvidia GTX8800 and binary drivers enabled.</li>\r\n<li>I understand quantum physics but I don't understand activities. I really don't. I have tried, but I failed.</li>\r\n<li>\"Zooming out\" of activities creates 100% CPU-usage SINCE 4.0. There has never been an improvement.</li>\r\n<li>There's so much work needed in detail (default sizes of windows and dialoges, clear user interface guidelines that don't only exist on paper but are followed).</li>\r\n</ul>\r\n\r\nYes, I know the answers:\r\n\r\n<ul>\r\n<li>It can be configured.</li>\r\n<li>Have you filed a bug report?</li>\r\n<li>KDE is not for you...</li>\r\n</ul>\r\n\r\nGuess what - I know that I can configure it. I have filed bug reports. I like the ideas behind the introduced technologies. But I'm really fed up of fiddling around, of messing with new technologies that simply do not work at all, of failing in the area of basic features while adding more and more stuff on top of them.\r\n\r\nEnd of story - I installed Ubuntu 11.04 with unity. While it is not perfect it does not demand heavy tweaking. It has lesser features than KDE but they are of high quality. It has the amount of polish no KDE-installation has ever provided (and I'm using it since 1.x...).\r\n\r\nI've never thought that it will come this far, I don't want to discourage anyone of you, I'm even a bit sad because I still somehow love KDE and the folks behind it, but KDE (and Amarok, by the way) has lost me. I just can't do actual work with it anymore. I just can't. For now, I give up."
    author: ""
  - subject: "In KDE-3 there were lots of"
    date: 2011-05-29
    body: "In KDE-3 there were lots of killer applications that moved people to use KDE or at least to have it installed on disk. There is no such thing in KDE-4... every single non-trivial application is unpolished, slow and unreliable. Don't tell me that the problems will be solved in future releases etc. etc. We are already at KDE 4.6.3 and even simple things like the panels and notifications still suck. Seriously, if we cannot get the notifications to be consistently displayed at the correct place after almost 4 years of development, how in hell is KDE going to fix more complicated issues? We are doing through a dark age... "
    author: ""
  - subject: ">If they knew about"
    date: 2011-05-29
    body: ">If they knew about architecture, they would be developers!\r\n\r\nI think there are more full time professional developers among the KDE users than among the KDE developers..."
    author: ""
  - subject: ">If they knew about"
    date: 2011-05-29
    body: ">If they knew about architecture, they would be developers!\r\n\r\nI think there are more full time professional developers among the KDE users than among the KDE developers..."
    author: ""
  - subject: "Nothing like it"
    date: 2011-05-29
    body: "Funny, my experience, in several (very different) machines, is NOTHING like yours."
    author: ""
  - subject: "Nothing like it"
    date: 2011-05-29
    body: "Funny, my experience, in several (very different) machines, is NOTHING like yours."
    author: ""
  - subject: "Seccond that"
    date: 2011-05-29
    body: "It's odd that myself and thousands of other users do not see those issues. People are happily using the great functional and performing KDE Plasma desktop every day, and many have done so for quite a while. The biggest issues I currently see are Konquerors seemingly inability to handle java applets and a crashing nspluginviewer brought on by the latest OpenSuse Flash update.\r\n\r\nBased on the huge number of KDE users out there and the graveness of the problems the parent poster describe, it rather clearly illustrates that such problems are far from widespread. Otherwise the KDE community should be drowning in reports abut it. And one should guess some of the developers using it daily should have noticed too. \r\n\r\nOr one could venture to guess the poster is a troll. Could be wrong,  but he touches all the usual things that are trolled about when it comes to KDE 4. Strigi, Nepomuk, Amarok, Akonadi, Plasma, activities and polish/user interface guidelines. Could be a regular user with talent for messing up his install, but some of those complaints sounds very odd. Like the zooming out interface to activities, did that not that way of doing activities go away in 4.5(at least 4.6)? Or the PIM problem caused by Akonadi, since the akonadified PIM are not actually released yet. Installed beta software of something he \"don't want to mess around with\"?  \r\n\r\n"
    author: "Morty"
  - subject: "Not trolling"
    date: 2011-05-29
    body: "I'm no trolling here. As I stated above, I use KDE a long time now. And I am able to fix things on my own if I have to. And yes, these are fairly generic issues that are adressed very often. But: That could be a sign that these problems *do* exist...\r\n\r\nOpensuse 11.4 does introduce akonadi. No \"beta software\" installed by me. Having Kontact display my google calendar was a nightmare to configure on Opensuse 11.4 compared to one click in evolution.\r\nStrigi does hammer my system (thus forcing me to turn it off).\r\n\r\nI could go on, but one issue is (at least for me) the most important: Side by side comparison of Kubuntu 11.04 and Ubuntu 11.04. As a long-time KDE user, I was shocked - shocked because of the amount of polish the Unity/Gnome Desktop offers compared to KDE. I have *never* imagined that I would someday switch my desktop from KDE to something else after frigging 10 years (isn't that proof enough that there is something wrong?). It may have less features, it may have less sophisticated technology, but it works out of the box. It delivers. KDE promises. This makes me sad, believe me.\r\n\r\n"
    author: ""
  - subject: ">cannot get the notifications"
    date: 2011-05-30
    body: ">cannot get the notifications to be consistently displayed at the correct place \r\n\r\nyes :) the funny thing is that I started receiving this issue with 4.6.3 \"bugfix\" release - it seemed to be ok in 4.6.2"
    author: ""
  - subject: "keyboard layouts"
    date: 2011-05-30
    body: "Hey\r\n\r\nwhat about keyboard layouts for instance dvorak or neo2. Its horrobile at the moment because no global shortkeys are working if dvorak or neo2 is chosen to be the first layout and \"normal\" german or \"us\" the second. If vice versa, the program specific shortcuts of ALL kde-programs are not working.\r\n\r\nBecause of this I had to switch to gnome3. No problems over there. I hope kde is going to fix this annoying bug.\r\n\r\nRexi76\r\n\r\nps. I reported the bug twice in the kde.bugzilla"
    author: ""
  - subject: "> I understand quantum"
    date: 2011-05-30
    body: "> I understand quantum physics but I don't understand activities\r\n\r\nI'm 100% with this"
    author: ""
  - subject: "Plasma developer screencasts"
    date: 2011-05-30
    body: "Apparently quantum physics is different from Plasma Activities. I found these <a href=\"http://blip.tv/chanis-kde-screencasts\">Activity videos</a> with minimal online searching.\r\n\r\n"
    author: "kallecarl"
  - subject: "I like KDE 4.6.3!"
    date: 2011-05-30
    body: "I like KDE 4.6.3!\r\n\r\nIt is good looking, it does everything I want to, but I stop Akonadi as it eats away my CPU for no major benefit for me. But, I could stop it, as I knew how to.\r\n\r\nI think Akonadi is a good idea, which should be available for those who need it, and be easy to turn off (which it is) if you don't need it.\r\n\r\nI have run KDE since Mandrake, ca 1999, and to most friends surprise positively happy with 4.x!\r\n\r\n\r\n\r\n"
    author: ""
  - subject: "Already seen... the"
    date: 2011-05-31
    body: "Already seen... the unanswered question is not \"what can I do with activities?\" but \"why on earth should I?\""
    author: ""
  - subject: "Because horizontally and"
    date: 2011-05-31
    body: "Because horizontally and vertically dividing a working area and putting it inside tabs, that are grouped inside windows, that are grouped inside windows-groups, that are grouped inside multiple desktops, that are grouped in multiple screens is simply not enough. we need yet another abstraction level.\r\n\r\nInsult deleted-editor\r\n\r\n"
    author: "trixl."
  - subject: "Just to be transparent?"
    date: 2011-05-31
    body: "I do not use KDE because it is the environment more beautiful, nice, technology, full customizable nor its platform QT is used everywhere, the better, easier, lighter, and more, which is used in C + + .\r\n\r\nI use it a philosophy, for his idea. Be cross-platform -> Do not rely on \"Linux\" (Kernel - How will Gnome), or its founder is satisfied that a company stops supporting Gnome (eg, Miguel de Icaza).\r\n\r\nBesides if they innovate, have personalities with their applications and therefore the best, multiplatform.\r\n\r\nDebian / KDE, is for me the best combination. 2 Free minds. Independent and can be used in BSD, Solaris, etc. And many developers support from outside, even in Windows / MAC OS X can use.\r\n\r\nKDE Great!"
    author: "1antares1"
  - subject: "Amen, but..."
    date: 2011-05-31
    body: "I think you are right, Dolphin has some design defects that some people have remarked and devs continue ignoring, but your sarcastic tone won't help devs to change their minds.\r\nI know it may sound childish but devs feelings may feel hurt and they might refuse to take your criticism, even if right, into account."
    author: ""
  - subject: "you shouldn't"
    date: 2011-05-31
    body: "Activities are for people who want them. People who can't figure out how to use Activities are better off thinking about other things."
    author: "kallecarl"
  - subject: "yeah"
    date: 2011-06-01
    body: "Hey, I feel your pain. Polish is an issue in many area's. There is big progress - Strigi and Nepomuk used to cause me issues but no more these days. Both on my laptop, desktop and media center they work fine and are starting to become useful.\r\n\r\nAkonadi doesn't cause issues either but I don't run the experimental KMail2 versions yet. I've tried them and know they will cause me issues - they are quite a bit slower for now so I'll probably just have to stay on KMail 1.x for the time being. Then again, I do look forward to real improvements in KMail again - there hasn't been much change in the last few years due to the work on Akonadi.\r\n\r\nActivities I don't use either but I applaud the KDE devs for trying new things.\r\n\r\nAnd you must admit Plasma is darn good these days. I have no instability, reasonable resource usage and a convenient, flexible interface. And KWin really rocks!\r\n\r\nSo surely, it's not all perfect but many things are VERY GOOD at least. KTorrent works great, choqok is fine, Amarok is getting better, Konversation does what it is supposed to do. I do look forward to a better browser, Rekonq wasn't there last time I tried it and I'm still on Firefox & Chromium. Calligra is going in good directions, Krita is aweseome. Dolphin just does what it is supposed to do - works great for me. Kate is absolutely amazing...\r\n\r\nSo yes, things aren't perfect, but in many area's they are very good. And perfect I haven't found anywhere..."
    author: ""
  - subject: "Less communication"
    date: 2011-06-01
    body: "There's been less communication with the user base also since, for example, the dot closed anonymous comments a while back (for reasonable reasons, but still it was an issue I think).\r\n\r\nThe above comment \"You lost me\" also reflects what I think about KDE lately. A lack of polish in general and over engineered solutions to vague problems.Trying new things is good, like plasma, but it's under used. How about having two default presets:\r\n- Normal desktop for people who resist change\r\n- Crazy desktop that really makes use of plasma.\r\n\r\nAnother bug fixing & polishing only release (4.8?) would be more than welcomed IMHO.\r\n\r\nMaybe KDE lacks a \"benevolent dictator\" to give it guidance and direction ?"
    author: "Pom"
  - subject: "Kontact"
    date: 2011-06-01
    body: "Seriously, as far as Kontact is concerned if you think it was somehow better back in the 3.x days, well, I don't know how you could come to that conclusion. I use it daily and have so for years, and yes it still crashes and misbehaves but much less than it used to in my experience. The level of performance is largely unchanged as far as I have noticed.\r\n\r\nWe (as KDE users) also are on the cusp of a new KDEPIM which has given the developers a chance to fix many of the architectural problems that cause the performance issues that have plagued Kontact/KMail up to this point. I haven't personally tried it yet but I have confidence it will live up to the promise."
    author: "bluelightning"
  - subject: "Anonymous comments"
    date: 2011-06-01
    body: "I don't disagree with your comments about polish (although \"crazy desktop\" is somewhat objectionable), but as you can see with this very article, anonymous comments on the dot have been re-enabled."
    author: "bluelightning"
  - subject: "That does not seem true."
    date: 2011-06-01
    body: "That does not seem true. After my laptop fried a few weeks back, I had to work on my previous laptop for two weeks, a 6 year laptop with a 1.2Ghz Athlon XP processors and 512Mbyte RAM(!). After disabling nepomuk KDE4 worked perfectly smooth. \r\n\r\nChromium couldn't run because it used too much memory for each tab, but konqueror worked fine, even H264 movies through flash work in full-screen work on this old beast.\r\n\r\nKDE4 is a lot less bloated than you think. I don't even hate KDE4, are I was surprised it worked so well."
    author: ""
  - subject: "I know what OP is saying"
    date: 2011-06-01
    body: "I know what OP is saying about Kontact...\r\n\r\nPerformance-wise Kontact 4 is by far slower on my new core i7 notebook than Kontact 3 has ever been on my old Centrino (I still have both, I tried) on filtering, moving mail, syncing with OpenXchange and so on. Plenty of RAM, fast HD, etc, etc. The hardware is not the problem. \r\n\r\nAnd as far as stability is concerned, I have quite the same problems of the OP: The good old \"Kontact is already running\" is just a nag, but kio_pop3 locking without warning is really a PITA.\r\n\r\nAnd, yes, it crashes! It crashes so much more than Kontact 3! Exactly while I was writing this, my colleague's Kontact closed by itself. Once again. Not a warning, not a log, nothing. \r\nAnd to say the truth, nobody in the whole office was surprised. A buggy KDE software... hardly news these days. \r\nSo sad. :(\r\n\r\nSo now we have another major rewrite. I learnt the hard way, during my career as software developer, that major rewrites are more often part of the problem than part of the solution. Major rewrites requires major bugfixing, and I think we have already not enough of it.\r\n\r\nThat said, I really, really hope to be wrong, because I'm a long time KDE supporter\r\nThat's why our office has been using Kontact since years.\r\nThat's why hearing \"but MS Outlook is so much stabler\" sounds like total defeat to me :(\r\n"
    author: "Vajsvarana"
  - subject: "FUD against KDE and QT has "
    date: 2011-06-02
    body: "been stepped up recently.\r\n\r\nArticles in \"uncertainty\" over QT/KDE have been planted in the media, forums are stuffed by posts hinting the same, while praising gnome and so on.\r\n\r\nIt will not stop the adoption of QT, meego and KDE on a larger scale than now, IMO."
    author: "mvaar"
  - subject: "Or you get buried by negative"
    date: 2011-06-02
    body: "Or you get buried by negative scores, like here.\r\nYou are totally right, too many kids who can't accept valid critic points if they aren't \"served\" to them with strawberries and wine gums. No irony nor directness without \"decoration\" or the kids will get ungry, cry and \"lapidate\" you with negative votes, or similar tantrums."
    author: ""
  - subject: "Try Nouveau."
    date: 2011-06-03
    body: "My experience with NVIDIA 260.x drivers has also been pathetic, but, instead of switching to LXDE, I did four things.\r\n\r\n1. Installed Fedora 15 in my main PC.\r\n2. Ensured myself I have the latest Nouveau (git snapshot 5/25/2011 here)\r\n3. Installed a stable build of Rawhide 2.6.39 kernel.\r\n4. Installed the source package for Mesa, recompiled Mesa with --enable-texture-float.\r\n\r\nThat was like grabbing the bull by the horns, but I stayed with KDE and gave the NVIDIA driver the boot. My mileage and my performance has been good so far, and Nouveau is a lot better than NVIDIA driver in the stability dept.; I miss Warzone 2100, but at least I can maximize Konsole or start a GTK+ 3 app without a kernel panic."
    author: ""
  - subject: "Please, KDE SC 4.7 BETA COMMENTS WANTED!!!!"
    date: 2011-06-03
    body: "Guys, I've read all threads, and all of them amount to \"KDE 4.6.3 is great / KDE 4.6.3 sucks\", with a majority of \"sucks\" simply because happy users don't comment (like me: ironically, I always found KDE 3.5.10 LESS stable and MORE crash-prone than KDE 4.6.3, but, then, I used Gentoo). But they all refer to KDE 4.6.3, and the discussion derailed completely.\r\n\r\nPlease, guys, I want to know how are you doing with KDE SC 4.6.80. Bugs? New features? Is the new composition engine a freaking and useless crashfest, or the coolest thing since sliced bread? How are the system wide resource optimizations doing? The new Shadow Engine? The new KDE PIM 4.6.80?"
    author: ""
  - subject: "I'd like to, but..."
    date: 2011-06-03
    body: "To make it clear, I'm one the \"sucks\" posters. I tested many betas years ago.\r\n\r\nBut now, the many old bugs still around takes 2 major problems for testing new releases.\r\n\r\n- Testing new features is very difficult: how can I test wether \"the new composition engine a freaking and useless crashfest\", if kwin is ALREADY \"a freaking crashfest\" to me?\r\n\r\nand, most important:\r\n\r\n- Motivation: if we loose out trust in the will of KDE developers to really fix bugs, why should we hunt and report them?\r\n\r\nThat's what I'm complaining about."
    author: ""
  - subject: "I'm the OP, and forgot to"
    date: 2011-06-03
    body: "I'm the OP, and forgot to login ;)\r\n\r\nHonestly, if you lost all confidence in KDE devs, you shouldn't be here stating your views, because it's a waste of time to try to change minds of people who you don't trust. I think you got frustrated about the current state of affairs, and a lot of solutions can be made.\r\n\r\n1. Distro. I CAN'T use KDE in OpenSuSE. Period. It's simply too crashy and the performance, in my both computers, is nightmarish. Perhaps YaST is great, but I don't think so. I find SuSE like a punishment, and it's been like this for me since S.u.S.E. Linux 5.2 (the oldest SuSE I tried, this was with a Pentium-100, what memories...) until today. Almost every other distro is better for me, starting with Slackware, Arch, Gentoo, Sabayon, Ubuntu, and Fedora, what I'm using now. Sometimes, an impression is fixed simply by switching your distro.\r\n\r\n2. Hardware. Do you have any hardware-specific bugs? Something to report? \r\n\r\n3. Interface decisions. What do you hate about KDE interface? Compile everything you hate about KDE design, submit it into a proposal, and let's discuss it. I'm convinced KDE designers won't have the last word on this one, if you do that.\r\n\r\nAbout bugs, you can read in KDE Commit Digests that there ARE some great memory, CPU and robustness improvements in Nepomuk and Akonadi scheduled for KDE 4.7, and if you have problems with the KWin compositing engine (you mention that KWin crashes constantly), KDE 4.7 features a rewrite of that engine. So, if KWin has been a crashfest for you until now, you must try KDE 4.7."
    author: "Alejandro Nova"
  - subject: "Thanks for the constructive"
    date: 2011-06-04
    body: "Thanks for the constructive reply. \r\n\r\n\r\nUnfortunately, openSUSE is not a choice I can change (corporate decision), same for the hardware (will try going with Nouveu instead of NVIDIA, as someone suggested, but last time I tried Nouveau made VMware unusable... so again no choice).\r\n\r\nWhat it REALLY annoys me about KDE interface is having my PC sit down by some sort of background reindexing (Kontact is absolute winner), or by some instance of mysql gone nuts (a relational DB on a desktop?), or by some process I've chosen to disable (nepomukerver anyone?) with no warning, an no choice, always when I have to work, and work fast!\r\n\r\nReally, the problem is not about resources hogging. \r\nIt's about having no choice and no warning on what is going on. That sucks. And that's about interface design.\r\n\r\nOn the \"choice\" side:\r\na flag in system-settings saying something like this: \"I'm no social-semantic user. Please disable Akonadi, kill Nepomuk, don't launch Strigi, uninstall Soprano, STOP WHATEVER BACKGROUND REINDEXING and let me freely use my desktop\". \r\n\r\nOn the \"warning\" side:\r\nwhen some strange-named service is about to start something that can hurt the user's productivity, just display a kind requester saying \"Sorry, I really have to hog your system for %d hours, may I start now?\". It would be perfect with 3 buttons saying \"No!\", \"Not now!\" and \"Disable forever!\". :)\r\n\r\nOk, too harsh, but I think it makes some sense, and maybe not just to me.\r\n\r\nI used to always report bugs I found. But at some point I started to get the usual sad set of answers:\r\n- it's distro's fault (but, guess what, on openSUSE's forums it's KDE fault)\r\n- it's hardware's fault (but how comes gnome is rock-solid? Heck, and KDE 3.5 too!)\r\n- it's my fault because I don't know and don't understand, and it's just me working the wrong way (but a Desktop should be a tool in my hands, not a chain around my neck!)\r\n\r\nWhatever, but in the end they never get fixed.\r\nSo, now I don't report them anymore.\r\n\r\nSo, in the end you are right, I don't trust KDE devels anymore, so I will stop the whining. Let's just hope for the better...\r\n\r\n"
    author: ""
  - subject: "Tested 4.7 beta and ..."
    date: 2011-06-04
    body: "Things that didn't work:\r\n\r\nno pdf preview in dolphin and desktopview\r\ncompositing doesn't work (no dropshadow and other effects)\r\nkmail imported mail acconts, but all mails and mailfolders were gone and I was unable to fetch mail from any account (imap and pop3).\r\nkalender is empty, except for my google calender. Only month-view and timeline are available, day and week-view are grayed.out.\r\n\r\nPrograms I tested that did work:\r\n\r\nkadressbook\r\nkaggregator\r\ndolphin\r\ngwenview\r\nkonqueror web browser\r\namarok\r\nkget\r\ntellico\r\n\r\ntested on\r\nopenSUSE 11,4 standard installation\r\nAtom DualCore 1,6 GHz with integrated Nvidia graphics card\r\nNvidia driver 270.41.06\r\n\r\nAfter testing I reinstalled KDE 4.6.3 without any issues and everything works fine."
    author: ""
  - subject: "About your issues. 1. NVIDIA"
    date: 2011-06-04
    body: "About your issues.\r\n\r\n1. NVIDIA has a OpenSuSE specific bug, you can watch it here: http://forums.opensuse.org/english/get-technical-help-here/applications/452490-suse-11-3-floating-point-exception-after-update.html . That bug alone KILLS KDE under OpenSuSE, and it's SuSE specific, because no other distro shows this behaviour. No matter what the forums say, this IS an OpenSuSE  bug, and one that has been neglected.\r\n\r\nAnd I haven't started with the conflicts between vesafb and NVIDIA drivers, that kill switching to and from console, black desktop when you return from console, and failing to hibernate and suspend, among a lot of buglets in between. If you are referring to THAT KDE experience, it's pathetic. However, I'm not using SuSE on my machines, and I really enjoy KDE. BTW, Nouveau + OpenSuSE works, but SuSE doesn't provide the latest Nouveau Fedora gives me, so still no SuSE for me.\r\n\r\n2. The background reindexing is a problem, but KDE 4.7 begins to take care of that. Strigi is moved to its own process, so, if it fails, it doesn't drag Nepomuk with it, and doesn't force Nepomuk to reindex all of your data. There have been heavy optimizations all across the board, and they should become apparent soon. Akonadi has made tremendous progresses since KDE PIM 4.6 beta 1, so I trust KDE guys on that one.\r\n\r\n3. The \"hog your system\" song is no more with Strigi 0.7.3. However, no one is packaging that. KDE DEVS, PLEASE, ANNOUNCE THAT RELEASE, I'M USING IT WITH KDE 4.6.3 AND IT SLASHED CPU USAGE FOR ME (now when Strigi is indexing, CPU usage hovers between 4 and 7 percent on a 3 year old computer).\r\n\r\nAppropiate responses for all of your issues. If you don't want to believe in a light after the tunnel, you are free to do so. I believe in it, and I do what I can to promote better my favourite desktop."
    author: "Alejandro Nova"
  - subject: "SUSE + NVIDIA = DISASTER"
    date: 2011-06-04
    body: "Thanks. I'll wait for Fedora packages, because, as I wrote before, there are serious problems with the SuSE + NVIDIA mix, and, sadly, SuSE is the most recommended choice when someone speaks about the best KDE distro.\r\n\r\nIf you are using NVIDIA, you'll be fine with ANY DISTRO BUT SUSE (Fedora, Mandriva, Ubuntu, Debian, Slack, Gentoo, Sabayon, Arch, Chakra, etc.)"
    author: "Alejandro Nova"
  - subject: "How are you going? How is KDE"
    date: 2011-06-04
    body: "How are you going? How is KDE 4.7 behaving on Arch?"
    author: "Alejandro Nova"
  - subject: "Trinity-KDE"
    date: 2011-06-04
    body: "if you're not excited about the very cool features KDE4 will have in the future, and don't particularly want to take part of them, then you probably shouldn't be using it.\r\n\r\n\r\nIf you've used KDE for a while and you're familiar with it, why don't you just stick to KDE3?\r\nThey've decided to keep supporting it in a project called Trinity KDE."
    author: ""
  - subject: "Bug reports :-)"
    date: 2011-06-04
    body: "Thanks for testing. Please check bugs.kde.org for the issues you found and either add information to existing reports (if relevant) or file new ones."
    author: "Stuart Jarvis"
  - subject: "pdf preview"
    date: 2011-06-04
    body: "<cite> no pdf preview in dolphin and desktopview</cite>\r\n\r\nDo you have ghostscript (and the `gs` binary) installed?"
    author: "pinotree"
  - subject: "Now, what a cool bunch of"
    date: 2011-06-04
    body: "Now, what a cool bunch of good advices in this thread!\r\n\r\nI have many problems in common with the OP, and will gladly test your solutions ASAP.\r\n\r\nMany thanks, sir! \r\n\r\nBut I have to say that I somehow like even the idea of the requester saying \"is it good now?\", or whatever form of user interaction when dealing with long background processes.\r\n"
    author: "Vajsvarana"
  - subject: "Strigi"
    date: 2011-06-04
    body: "I got the latest Strigi snapshot, it bumped again, this time to 0.7.5 (maybe indicating an imminent 0.7.4 release).\r\n\r\nI need a fix for FFMPEG indexing support, because it's using deprecated symbols and in Fedora 15 it doesn't compile anymore, but Strigi bugs seem to have no visibility, no maintainer, and no integration with KDE bug reporting system. They are reported in a Sourceforge bugzilla, and have no connection with the rest of bugs.kde.org, despite the fact Strigi indexing is an important part of Nepomuk.\r\n\r\nIf you can do something to fix the perception of Akonadi and Nepomuk, that would be the magic bullet."
    author: "Alejandro Nova"
  - subject: "Akonadi"
    date: 2011-06-04
    body: "A thing that most people doesn't seem to know is that KDE PIM is split in 2 main trees: kdepim-runtime and kdepim. While kdepim contains... well... KDE PIM, kdepim-runtime contains all the Akonadi indexers KDE PIM needs, among other things.\r\n\r\nThe fact is: when you complain about Akonadi, you also complain about the KDE-PIM Runtime you have. And if you haven't installed Kontact2, you are complaining against KDE 4.4 and its KDE-PIM Runtime. You are missing a year of improvements.\r\n\r\nEven installing KDE-PIM 4.4 on KDE-PIM Runtime 4.6 yields consistent and measurable performance improvements. It's one year of bugfixings and optimizations, and remember Akonadi is only a database; it saves what the akonadi-agents crawl. And akonadi-agents are a part of KDE-PIM runtime, not Akonadi itself.\r\n\r\nFYI, I did those tests mixing releases with KDE-PIM 4.4 and KDE-PIM Runtime 4.5. You know how hellishly buggy KDE-PIM 4.5 was, but KDE-PIM 4.4 + Runtime 4.5 ran great. "
    author: "Alejandro Nova"
  - subject: "What you mean by \"one X per"
    date: 2011-06-06
    body: "What you mean by \"one X per screen\"?\r\n\r\nDoes that mean it would support: \r\n1) Having same desktop but different virtual desktop on every screen?\r\n2) Having different desktop and different virtual desktop on every screen?\r\n\r\nAs already it has been possible to have 2) but not 1)"
    author: ""
  - subject: "Lost me also"
    date: 2011-06-10
    body: "I'm a user since pre 1.0 and I'm in the same boat as you. I'm not against new technologies but the way these new technologies where introduced in kde seem to go against the philosophy of the project. I'm not against nepomuk, akonadi, and \"the whole semantic shit\", but making it very hard to remove from the system is wrong IMHO. \r\n\r\nI don't care much about defaults, but we should be able to easily disable these components and still have a functioning system.\r\n\r\nI'm now using KDE 3.5.12 (aka Trinity Desktop Environment 3.5.12)."
    author: ""
  - subject: "+1\nThis is what is needed:\nOn"
    date: 2011-06-10
    body: "+1\r\n\r\nThis is what is needed:\r\n\r\nOn the \"choice\" side:\r\n\r\na flag in system-settings saying something like this: \"I'm no social-semantic user. Please disable Akonadi, kill Nepomuk, don't launch Strigi, uninstall Soprano, STOP WHATEVER BACKGROUND REINDEXING and let me freely use my desktop\". \r\n\r\nI went with Trinity because of the indexing stuff (and a db daemon for a desktop!?)."
    author: ""
  - subject: "openSUSE + KDE + NVIDIA"
    date: 2011-06-22
    body: "Hi again... don't know if someone would ever read this, but I finally solved many of the problems with KDE on openSUSE simply by upgrading NVIDIA driver to 275.xx version.\r\n\r\nThis does not solve Kontact problems, and does not make Nepomuk any less crappy ;) , but at least kwin is now absolutely rock-solid and many (if not almost all) random crashes are gone.\r\n\r\nMany thanks to Alejandro Nova for the useful suggestions. And my best excuses to kwin developers: it was not their fault.\r\n\r\n"
    author: "Vajsvarana"
  - subject: "\"It's odd that myself and"
    date: 2011-06-26
    body: "\"It's odd that myself and thousands of other users do not see those issues. People are happily using the great functional and performing KDE Plasma desktop every day, and many have done so for quite a while.\"\r\n\r\nWords of someone in denial. The reality is that most KDE users already left. I used to hate Gnome (and secretly still do) but at some point it stopped getting in my way and KDE started. I kept a KDE login for versions 4.1, 4.2, 4.4, 4.6 and I keep checking in on it's progress to see how things are going but I stopped using it years ago. I want so much to show new users (and especially MAC users) a really nice polished but still efficient Linux Desktop and we really don't have one. Gnome looks like crap, Unity is a train wreck and KDE could be good if it actually worked (and made sense). Sigh, such is life."
    author: ""
---
<p>KDE <a href="http://www.kde.org/announcements/announce-4.7-beta1.php">has released</a> a first beta of the upcoming 4.7 release of the Plasma Desktop and Netbook workspaces, the KDE Applications and the KDE Frameworks, which is planned for July 27, 2011. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing new and old functionality.
</p>
<p>
The 4.7 release will bring a number of exciting improvements:
<ul>
    <li>KWin, Plasma's window manager <a href="http://www.kdenews.org/2011/02/18/kwin-embraces-new-platforms-opengl-es-20-support">now supports OpenGL-ES 2.0</a>, improving performance and deployability on mobile devices</li>
    <li>Dolphin, KDE's flexible file manager has seen user interface improvements and now sports a better user experience for searching in files' metadata.</li>
    <li>KDM, KDE's login manager <a href="http://ksmanis.wordpress.com/2011/04/21/hello-planet-and-grub2-support-for-kdm/">now interfaces</a> with the Grub2 bootloader</li>
    <li>Marble, the virtual globe now supports offline address search, especially making its mobile version more useful on the road</li>
</ul>

</p>


<p>To download source code or packages to install go to the <a href="http://www.kde.org/info/4.6.80.php">4.7 Beta1 Info Page</a>.
</p>
<!--break-->
