---
title: "Qt and the Future of KDE"
date:    2011-03-03
authors:
  - "cschumacher"
slug:    qt-and-future-kde
comments:
  - subject: "Is it me, or this doesn't"
    date: 2011-03-04
    body: "Is it me, or this doesn't explain <strong>ANYTHING</strong>? This is a post about KDE and its technology but it doesn't explain how things are going to evolve if Nokia drops Qt, and that's what we have fear of."
    author: "n"
  - subject: "Fork Qt..."
    date: 2011-03-04
    body: "Qt is wonderful, we all know that but at the cost of freedom, Im talking about the freedom of desition, desition of what bug is more important to fix and why, desition to release the next version when you think is better, desition to deside what is deprecated and what not, desition to deside what new feature is needed and what new feature is priority and many more.\r\n\r\nAll those crucial desitions should be made by KDE not for a foreinght member, When I say Fork, I say a Fork keeping compatibility, is it hard to accomplish? prolly, but I think is worthy."
    author: "ramsees.79@gmail.com"
  - subject: "On the other hand.."
    date: 2011-03-04
    body: "I think it does say quite a bit.\r\n\r\nIs KDE sticking with Qt?\r\n\r\n\"We chose Qt in 1996 because it offered the best software development framework. Today, it still does.\"\r\n\"Qt remains the strong, cross-platform foundation of everything we do.\"\r\n\r\nRe: Nokia \"dropping\" Qt?\r\n\r\n\".. Qt will always be available .. strengthened in recent years by Nokia's decisions to release it under the LGPL\"\r\n\r\nWhat's the plan re:Qt moving forward?\r\n\r\n\"We are working with our partners to speed up this process and make Open Governance a reality.\" etc etc\r\n\r\nSummary, KDE believes Qt isn't going away - Nokia or no Nokia, KDE doesn't plan to stop using Qt at its heart and core, KDE will work towards Open Governance to decentralize decision-making in Qt, KDE has no plans to fork Qt. Think that answers everything? :)"
    author: "moofang"
  - subject: "Open Governance"
    date: 2011-03-04
    body: "The whole point of the Open Governance model we're working towards is so that community members like those in KDE will have power to make decisions about the parts of the code they're involved in maintaining. Nokia have said that there's parts of Qt that they consider feature-complete and (once Open Governance is established) if a community member wants to take over chief maintainer of that code they will be able to fix bugs and add features.\r\n\r\nThere is absolutely no need for a fork. Us in KDE and the engineers in Qt all seem to agree on this."
    author: "milliams"
  - subject: "Good decisions"
    date: 2011-03-06
    body: "I think not forking is the best decision, at least until any difficulties arrise, for example if it would be good for kde to change something in qt but even with open governance, it is hard to do, then kde will have to fork qt, but until qt works good, and any needed changes are accepted there's no need for a fork, qt is used by a lot of people, there is a lot of money involved in it(actually nokia, in the future it might be someone else), and this won't change tomorrow. "
    author: "damipereira"
  - subject: "The one thing I would like to"
    date: 2011-03-06
    body: "The one thing I would like to see is closer synchronization between Qt4's Phonon and the work of the Phonon folks."
    author: "stumbles"
  - subject: "That would be nice"
    date: 2011-03-06
    body: "Specially thinking about qt multimedia/mobility maybe they should be merged? Or phonon deprecated and changed for qt multimedia(on it's code, not the ui or the name)"
    author: "damipereira"
---
Following <a href="http://conversations.nokia.com/2011/02/11/open-letter-from-ceo-stephen-elop-nokia-and-ceo-steve-ballmer-microsoft/">Nokia's recent announcement</a> about its future smart phone development strategy, KDE has received a lot of questions. Many of these questions have been related to the future of KDE and KDE's commitment to the Qt framework. In this statement we set out what we see as a bright future for Qt and KDE software.
<!--break-->
<h2>Qt</h2>

We chose Qt in 1996 because it offered the best software development framework. Today, it still does. Over the last fifteen years, KDE has worked to ensure that Qt will always be available, leading to the creation of agreements such as that which underpins the <a href="http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>. The future of Qt has been further strengthened in recent years by Nokia's decisions to release it under the LGPL and begin the process of giving the community greater influence through <a href="http://qt-labs.org/index.php/Main_Page">Open Governance</a>. We are working with our partners to speed up this process and make Open Governance a reality. In the meantime, we welcome Nokia's ongoing commitment to Qt, as well as their continued support of KDE, as <a href="http://ev.kde.org/supporting-members.php">a Patron</a> and as a sponsor of our annual conference.

<h2>KDE</h2>

Today, KDE faces new challenges and opportunities. Traditional desktop and laptop computers are no longer the only means for users to work on documents, entertain themselves and interact socially. Innovations such as netbooks, tablets and increasingly capable smart phones have changed computer use, introducing new form factors and new use cases. For the first time since KDE was founded, we have the chance to shape the nature of computer use, rather than competing with established computing paradigms.

Our Plasma framework puts us in a strong position to develop innovative and beautiful user interfaces for smart phones, in-vehicle systems, desktop computers, portable computers, home media centers and more. Few, if any, of our competitors have an application and user interface framework that is as portable, attractive and easy to develop with as ours. At the heart of Plasma and the portability of our applications is Qt. Code developed for one platform can run with minimal changes on legacy platforms such as Microsoft Windows and Mac OS X and, crucially, on Linux and other free platforms that can scale from the handheld device to the desktop powerhouse. Recent innovations in Qt, such as Qt Quick, make it easier than ever for designers as well as developers to turn their ideas into applications and offer them to users. KDE is only just beginning to take advantage of these new opportunities.

<h2>The Future</h2>

Qt remains the strong, cross-platform foundation of everything we do. Combined with KDE technologies, we believe Qt is <em>the</em> compelling framework for cross-platform software development. There has never been a better time to shape the future of computing. <a href="http://www.kde.org/community/getinvolved/">Join us</a> and make that future a future that is free.