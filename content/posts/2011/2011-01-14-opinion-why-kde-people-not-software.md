---
title: "Opinion: Why KDE is People, Not Software"
date:    2011-01-14
authors:
  - "oriol"
slug:    opinion-why-kde-people-not-software
comments:
  - subject: "HTML5 &lt;video&gt;"
    date: 2011-01-14
    body: "It would be nice if you could embed the video directly into the article with the <video> tag. There are a few JS libraries (e.g. <a href=\"http://videojs.com/\">VideoJS</a>) which provide the ability to fall back to the embedded Flash player if the browser doesn't support <video>."
    author: "milliams"
  - subject: "Something to think about..."
    date: 2011-01-14
    body: "I just had a quick play both here on the Dot and with the VideoJS demo...\r\n\r\nFirefox: fine, plays HTML5\r\nKonqueror KHTML: ok, small video on VideoJS site, plays HTML5\r\nKonqueror WebKit: sound only on both tests, does not fallback to Flash\r\n\r\nSo the JS solution is not necessarily foolproof (the browser has to correctly report what it can do and JS needs to be on, of course).\r\n\r\nHowever, we could consider a reversal of the current situation: switch to HTML5 by default and a link to a Flash version for people for whom HTML5 does not work - something to think about..."
    author: "Stuart Jarvis"
  - subject: "Now I understand!"
    date: 2011-01-14
    body: "You know what Aaron?\r\nNow that I saw you talking about it and at the same time was watching the three categories (Workspaces, Platform, Applications) listed in the article, it became perfectly clear what is that is still not working well in my mind regarding the new KDE branding scheme since the change was done.\r\n\r\nAnd that thing is that there are two brands, two overlapping brands: the \"KDE\" brand and the \"Plasma\" brand. They inadvertently compete with each other for mass attention in the headlines of every press release and news bit that has to announce something related to KDE and its world.\r\n\r\nAnd when you started to talk about \"obsolete\" denominations I immediately started to think that perhaps that is exactly what is happening to the KDE brand! The project became something different over the years, and perhaps even its name just doesn't suit it anymore...\r\n\r\nPlasma from the start sounded as a great name and over the years has become a brand on its own, with more impact, appeal, and certainly one that ignites imagination the moment people gets exposed to it. Moreover, with each new release more and more things around the whole user experience have more to do with the Plasma name than with any other one, including KDE.\r\n\r\n...I know there is quite a baggage of roots, history and tradition in the KDE name, but quite frankly I'm starting to re-evaluate if this is the more appropriate name for the project and its future.\r\n\r\nWhy not simply reach people and the press with\r\n - Plasma Workspaces: Desktop | Netbook | Mobile\r\n - Plasma Platform\r\n - Plasma Applications\r\n\r\nAdmitedly it doesn't change a bit in the programming side of things, but I envision it can really change minds quite a bit.\r\n\r\nThanks for listening!\r\nGabriel Gazz\u00e1n\r\n"
    author: "ggabriel3d"
  - subject: "or perhaps"
    date: 2011-01-14
    body: "...or perhaps, if KDE is to be maintained as the main brand, at least be more clean and completely remove the Plasma brand from communications and reach the masses with:\r\n\r\n- KDE Workspaces: Desktop | Netbook | Mobile\r\n- KDE Platform\r\n- KDE Applications\r\n\r\nI don't deny I really foresee a marketing opportunity in the shift from \"KDE\" over to the \"Plasma\" brand. But even if this never happens, the current marketing situation seems to be unnecessarily confusing, from my point of vew.\r\n(The fact that articles and interviews continually have to be made explaining this is perhaps the most clear signal of it.)"
    author: "ggabriel3d"
  - subject: "Re: Plasma umbrella brand"
    date: 2011-01-14
    body: "That won't solve a thing. The problem was/is that the KDE brand was used for everything related to the KDE community, and different meanings of KDE are confused with each other. Replacing s/KDE/Plasma/g everywhere won't clean up the confusions."
    author: "majewsky"
  - subject: "One umbrella"
    date: 2011-01-14
    body: "Well... It won't clean up confusion... But at least it will reduce it.\r\n\r\nAlso, there will be only one brand to focus on, to develop, and to promote. It will be much easier to promote one \"umbrella\" than two"
    author: "alvise"
  - subject: "New name"
    date: 2011-01-15
    body: "IMHO, KDE should stand for \"K Desktop Experiment\"."
    author: "trixl.."
  - subject: "Yeah we used to have that..."
    date: 2011-01-15
    body: "We called it KDE and it led to people thinking that you either took KDE (the desktop, the apps, the platform for developing your apps) as a whole or rejected it as a whole.\r\n\r\nSo now, we have a group of people called KDE that provide:\r\n- Workspaces under the Plasma brand\r\n- The KDE Platform for building apps\r\n- An awesome music player, Amarok\r\n- The best photo manager, digiKam\r\n- A creativity and productivity suit Calligra\r\n- A set of education apps in KDE Edu\r\n- The universal PIM application, Kontact\r\n- So much more\r\n\r\nAnd the beautiful thing is that you can mix and match:\r\n\r\nChoose Plasma Desktop with apps from KDE, GNOME and other projects such as Libre Office.\r\n\r\nChoose GNOME shell, but rediscover your music with Amarok\r\n\r\nChoose Windows, but manage your photos with digiKam"
    author: "Stuart Jarvis"
  - subject: "I think..."
    date: 2011-01-16
    body: "...in the past there wasn't a visible Workspaces, Platform, Applications (let's call them) groups of software related to KDE.\r\nInstead, the whole thing was just simply called \"KDE\" and THAT was (in my opinion) what led to that \"take all, leave all\" dichotomy you refer to.\r\n\r\nThat wasn't good, I agree, but now having a different brand for the main working environment than for everything else related to the community doesn't seems to be working great either.\r\n\r\nMoreover, what you imply is that we wanted to \"save\" the applications from being discarded beforehand by being associated with KDE. So people working with Gnome, Windows, OS X, etc. wouldn't be using them just because they saw those applications associated to KDE.\r\n\r\nBut, surprisingly enough, the solution seems to be that we've changed the name of the working environment to something else: Plasma. Different, but still always being mentioned as a subproduct of the KDE world, and have kept the KDE reference in every application we wanted to \"save\" from being associated to KDE?\r\n\r\nSo, frankly, I don't get the logic behind this...\r\n"
    author: "ggabriel3d"
  - subject: "Sure..."
    date: 2011-01-16
    body: "So we defined (or really just talked more about) the three groups you mentioned.\r\n\r\nHowever, the workspaces lacked a name. What do you think would have happened if we'd talked about KDE Desktop etc? Do you not think people would just have said 'KDE'? Then KDE remains associated as only/mainly a desktop and any KDE apps are associated only/mainly with use in that desktop. So we gave the workspaces proper names - Plasma Desktop etc -just as the other applications have names - Kontact etc.\r\n\r\nSo, we could have ditched KDE completely - or kept KDE for the workspaces and come up with some other name for the community. But what we chose was to keep KDE for the community, KDE by that point did not just belong to the workspaces. But either approach could have been ok and can separate the 'made by X' association from the 'only for use in desktop X' association. I think KDE is a valuable brand and being known as a 'KDE app' is a good thing, mostly. The challenge is to loosen the close association with the workspaces by building a distinct workspace brand and using 'KDE' to refer to us. It will take a lot of time, but so would the other approach."
    author: "Stuart Jarvis"
  - subject: "I get it"
    date: 2011-01-18
    body: "it's just there are two things in what you say that I don't agree with:\r\n\r\n- workspaces doesn't have to have a separate name. you separate the workspace from the framework and the apps, by just establishing, (as you said) talking about and exposing more clearly these different categories\r\n\r\n- the second one is that frankly KDE is not anything that sounds like a community name. It sounds like what it is (or was), an acronym, something that resumes or simplyfies a long descriptive name.\r\nLet's put it this way: if you're going to have a baby you certainly won't call it KDE, PSI or HSC. There is no feeling in it, no imagination or care ignited while pronouncing or thinking of it.\r\nSo if the idea was to have a community with a name and workspaces with other name, then perhaps it'd have been better to name the workspaces \"KDE\" and the community otherwise. Or whatever you wanted, but KDE doesn't feels like it's a community name.\r\n\r\nGoing back to the first point, I sincerely believe to be confusing and redundant promoting and having two names. Just one name, with a proper handling in communication, would be a much more powerful and effective message.\r\n\r\n...and Plasma is not a bad name for doing this, at all.\r\n"
    author: "ggabriel3d"
  - subject: "About names"
    date: 2011-01-18
    body: "Wow, this discussion is probably evolving much beyond what could have been expected.\r\n\r\nThat namechange might be a very nice discussion topic for K16:\r\nhttp://dot.kde.org/2010/12/20/introducing-k16-and-future-kde"
    author: "alvise"
  - subject: "Plasma as name has a problem"
    date: 2011-01-21
    body: "Plasma as name has a clear problem, though: http://lmgtfy.com/?q=plasma\r\n\r\nKDE is well known, and it is google-able. Plasma just is not. \u201cI use plasma\u201d \u21d2 \u201coh, so you\u2019re a particle physicists\u201d \u21d2 \u201cno, I\u2019m a poor but happy TV-geek\u201d\u2026 \r\n\r\nIf you ditch the name KDE, you lose the work of more than 10 years for establishing a brand. \r\n\r\nAnd one thing you should not ever do is ditching an established brand. \r\n\r\nKDE is just that: KDE. You name your son \u201cJon\u201d and not \u201cHyper-Bang\u201d. He might choose a secret and possibly more descriptive name when he joins a wicca community, but he will still be \u201cJon\u201d (too). KDE is a real name, and it has meanings beyond anything Plasma can be. For me it means: \r\n\r\n\u201cKDE is the Key to your Digital Experience\u201d"
    author: "ArneBab"
  - subject: "it might have problems, but..."
    date: 2011-01-25
    body: "It might have problems, just as every other name, but the one you point out it's not one of those.\r\nJust search in Google for \"Plasma Desktop\" (as in the following link) and you get a full (first) page of KDE only related results.\r\nhttp://www.google.com/search?hl=en&safe=off&q=plasma+desktop&aq=f&aqi=g10&aql=&oq=\r\n\r\nI guess if you search for 'apple' or 'windows' you'll get non related results too, but that doesn't mean the names themselves aren't good ones.\r\nYou have to search within a context if you're about to get meaningful results, if not, I guess the real problem is not the name, but you that don't really know how to effectively search for things on the Internet. That, respectfully said. :)\r\n"
    author: "ggabriel3d"
---
As the first of several opinion pieces exploring current issues in KDE, we offer you a video of <b>Aaron Seigo</b> explaining how KDE's success as a community producing all kinds of software led to outgrowing our old name, the "K Desktop Environment", what KDE means now and why it matters.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><embed src="http://blip.tv/play/AYKJszIC" type="application/x-shockwave-flash" width="480" height="390" allowscriptaccess="always" allowfullscreen="true"></embed><br />Aaron Seigo on KDE Branding (<a href="http://blip.tv/file/get/Swjarvis-AaronSeigoOnKDEBranding379.ogv">Ogg Theora version</a>)</div>

A little over a year ago, an article was published on the Dot titled <a href="http://dot.kde.org/2009/11/24/repositioning-kde-brand">'Repositioning the KDE Brand'</a>. The article publicized the outcome of a process within KDE to make sense of the relationship between the community and its products, and to reach a durable solution regarding the terms that should be used to refer to both.

Since then, good progress has been made with people understanding, accepting and using the new terms. Spreading awareness within and beyond KDE, and increasing the understanding of how the brands are now used, takes time and continued communication. This effort is still ongoing.

<b>Justin Kirby</b> caught up with Aaron - Plasma developer extraordinaire, former president of <a href="http://ev.kde.org/">KDE e.V.</a> and KDE promoter - to ask him to explain the KDE brands and the reasoning behind them. As Aaron lucidly describes, the central element of the new terminology is the fact that KDE refers to the <a href="http://kde.org/community/">community</a>, and not to any one of the products that the community creates. Thus, KDE is a community that makes three different types of products: <a href="http://kde.org/workspaces/">workspaces</a>, a <a href="http://kde.org/developerplatform/">platform</a>, and <a href="http://kde.org/applications/">applications</a>.

<ul>
<li><b>Workspaces:</b> The main brand for the workspaces is Plasma; KDE produces Plasma Desktop, Plasma Netbook, and soon Plasma Mobile and others.</li>
<li><b>Platform:</b> The KDE Platform consists of the base of libraries and services that are needed to run KDE applications.</li>
<li><b>Applications:</b> KDE Applications are created by the KDE community using the KDE Platform, that can run in environments other than the Plasma workspaces (such as Gnome or, in many cases, Windows).</li>
</ul>

You can watch the interview in the embedded video above if you have a compatible Flash player or download the free format <a href="http://blip.tv/file/get/Swjarvis-AaronSeigoOnKDEBranding379.ogv">Ogg Theora version</a>.