---
title: "Social Events at the Desktop Summit"
date:    2011-07-23
authors:
  - "kallecarl"
slug:    social-events-desktop-summit
---
We are pleased to announce the Desktop Summit social events, bringing even more excitement and fun to the Conference program. The social events vary from sports to parties, and will take place throughout the week in different locations in Berlin. They will provide opportunities for attendees to get together informally while enjoying foods and drinks provided by our sponsors—Intel, Collabora, SUSE, Igalia and corporate partner, c-base.

Those who arrive a day early are welcome to join the <a href="https://desktopsummit.org/program/pre-registration">pre-registration event at c-base</a>, the world famous hacker space, on Friday, 5th of August from 4 to 9 p.m. Enjoy drinks sponsored by Igalia while meeting other attendees and getting warmed up for the Desktop Summit. You will be able to check in early, get your conference badge, and skip the wait on Saturday morning. At the party, people can buy food vouchers for lunch and sign up for one of the day trip choices. More information on the day trips will be announced soon.

Sunday night features a <a href="https://desktopsummit.org/program/beach-party">BBQ on the beach of the River Spree</a>. Starting at 7.30 p.m., Platinum sponsor Intel welcomes you to the barbecue and open bar. The party will continue inside from 11.00 p.m. with dancing and karaoke. This is an opportunity to enjoy the personality of Berlin and the party all at once. 

On Monday night, a football match will be held supported by SUSE with t-shirts and refreshments. For those who would like to participate, please <a href="http://wiki.desktopsummit.org/Football_match_sign-up">sign up</a> for the football match on the Wiki. More information on the <a href="https://desktopsummit.org/program/football">location of the match</a> is available.

The last, but certainly not the least party, is on Tuesday, 9th of August from 7.30 p.m. on. The <a href="https://desktopsummit.org/program/island-party">Island Party</a> will be on the Insel Berlin in Treptower Park. After a nice walk along the Spree river from the Treptower Park S-Bahn station, you will reach the party venue with lush greenery surrounded by the Spree river. Collabora is sponsoring food and drinks, and there will be a Soul DJ.
