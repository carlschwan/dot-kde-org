---
title: "New look for KDE Edu"
date:    2011-04-30
authors:
  - "apol"
slug:    new-look-kde-edu
comments:
  - subject: "Would love to see videos"
    date: 2011-04-30
    body: "i would like to see small clips of the application. I did some of these in the past on my channel. But honestly I didnt have any idea of what I was doing. \r\n\r\nA user could have done this using qt-recordmydesktop or screenr.com and publish it on the site. \r\n\r\nThis is my video:\r\nhttp://www.youtube.com/watch?v=J1F7e78JBWM\r\n\r\nAlso some voice that explain what is going on would have also helped. "
    author: "JZA"
  - subject: "Dead link on Ximdex"
    date: 2011-05-01
    body: "The link to Ximdex seems to be wrong."
    author: "vicT"
  - subject: "Beautiful"
    date: 2011-05-03
    body: "This is simply beautiful! And so much easier to navigate. Thanks to all who put in hard work to get this done!\r\n\r\nmutlu"
    author: "mutlu"
  - subject: "Congrats"
    date: 2011-05-23
    body: "Nice. Very beautiful look.\r\n\r\nIncidentally: the KAlgebra link is not working as of 23-may, 18:35 UTC."
    author: ""
---
The KDE Edu Team is proud to present its new website at http://edu.kde.org as the central place to start to discover <strong>KDE Edu</strong>.

With this new website, we are also officially presenting the new KDE Edu logo as the stamp for KDE in Education. The logo emphasizes the opportunity for people to grow their knowledge freely in various fields with KDE Edu programs. The concept was started in May 2010 by Alexandre Freitas and finalized by Asunción Sánchez Iglesias.

The new website was redesigned from scratch to better display what we are doing by using artwork inspired by handmade drawings, along with the newly introduced logo theme--the growing and flourishing of a person.

These changes position KDE Edu in the Free and Open Education landscape. KDE provides a platform for the KDE Edu Team to create tools that make it possible for people to have successful educational experiences. KDE Edu covers the wide range of technology in Education: from University to Pre-School, as well as the hobby learner.

Thanks to the site designer, <a href="http://www.e-lena.es">Elena Ramírez</a>, her employer <a href="http://www.ximdex.com/">Ximdex</a>, and to the KDE Web team who made this new design possible!
