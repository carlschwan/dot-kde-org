---
title: "Amarok Insider, Written By Google Code-In Students"
date:    2011-01-26
authors:
  - "jriddell"
slug:    amarok-insider-written-google-code-students
---
The <a href="http://amarok.kde.org/en/Insider/Issue_16">latest Amarok Insider</a> features the writing of seven Google Code-In students, writing nine different articles.  They range from an interview with developer Bart Cerneels to "How To install a new script in Amarok".  Besides the mentors, students were able to get assistance and encouragement in their tasks and research from Amarok developers and users in the #amarok IRC channel on Freenode, which is what makes these articles of such high quality.

Amarok Insider featured articles: What's New in Amarok, Google and Amarok, Roktober Success, Interview with a Developer: Bart Cerneels, Automated Playlist Generator: How to Use it, Queue Manager in Amarok, Installing and using a script in Amarok, Amarok on Windows, Transcode your media files, Amarok Live CD.

Google Code-In was a huge success for Amarok, and it is great to celebrate the Amarok 2.4 release with this Insider almost completely written by our students!
