---
title: "KDE Commit Digest for 13 February 2011"
date:    2011-03-02
authors:
  - "vladislavb"
slug:    kde-commit-digest-13-february-2011
comments:
  - subject: "Guess we need a meme..."
    date: 2011-03-03
    body: "Well, here goes then:\r\n\r\nThanks Vladislav!"
    author: "odysseus"
  - subject: "thanks"
    date: 2011-03-04
    body: "Thanks Odysseus,\r\n\r\nHowever, while i may have posted up the article -- these are a team effort! :)\r\n\r\nVlad"
    author: "blantonv"
---
In <a href="http://commit-digest.org/issues/2011-02-13/">the latest KDE Commit-Digest</a>:

              <ul><li>Fixes and Optimization in KWin, including NVIDIA related fixes</li>
<li>Bugfixing in KDE-Libs including fixes for MacOSX</li>
<li>Work in <a href="http://www.calligra-suite.org/">Calligra</a> including new stencils from the Dia shape repository and support for DNG files in Krita</li>
<li>Personal finance manager <a href="http://skrooge.org/">Skrooge</a> gains a timeline in reports and the possibility to merge payees</li>
<li>Reorganization of the Electronic Program Guide (EPG) in <a href="http://kaffeine.kde.org/">Kaffeine</a></li>
<li>Beginnings of work to support GHNS for templates in <a href="http://kde-apps.org/content/show.php/Cirkuit?content=107098">Cirkuit</a></li>
<li>Possibility to generate keyboard presses using your remote control in KDE-Utils</li>
<li>Work on a new Color Label widget in <a href="http://www.digikam.org/">Digikam</a> 2.0</li>
<li>Further development in communications softwares telepathy, kopete, and konversation</li>
<li>Work and fixes in Okular, KDevelop, KGet, <a href="http://rekonq.kde.org/">Rekonq<a/>, KOffice, KDE-PIM and Oxygen-GTK</li>
<li>Digikam and Kipi-Plugins migrate to Git!</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-02-13/">Read the rest of the Digest here</a>.