---
title: "KDE in France - the View from RMLL"
date:    2011-08-24
authors:
  - "Stuart Jarvis"
slug:    kde-france-view-rmll
comments:
  - subject: "You can find a summary of the"
    date: 2011-08-25
    body: "You can find a summary of the KDE France BoF that happened at the Desktop Summit here: http://blog.ben2367.fr/2011/08/12/kde-france-is-back/"
    author: "ben2367"
---
<b>Geoffray Levasseur</b> attended <a href="http://2011.rmll.info/?lang=en">RMLL</a> (Rencontres Mondiales du Logiciel Libre/Libre Software Meetings) in Strasbourg, France on behalf of KDE, and <a href="http://jeff.levasseur.tuxfamily.org/en_GB/2011/08/rmll-2011-in-strasbourg/">gathered some useful feedback</a> on our efforts and the views of Linux users in general.

Geoffray found quite a few differences in attitudes towards the free desktop options on Linux compared to the previous year. The situation with Gnome 3 and Unity in Ubuntu has affected the view that people have of KDE. Some traditional free software users have given Unity a bad reception and are considering other options. The response to GNOME 3 seems to be more mixed. Some traditional Gnome users appreciate the new look and feel, while others do not really like it. Some think that the new desktop is not finished, likening it to the KDE 4.0 release. This creates some new interest in KDE, although of course some KDE people are excited to try out alternatives such as GNOME 3 and Unity.

<h2>Positive Reception</h2>

Most attendees have positive views of KDE software; there was definitely more satisfaction than last year with the quality of KDE software. Most of the negative remarks were about the usability of new functionality. For example, some people still do not understand Activities. However, Geoffray found that a quick demonstration was enough to show people how useful the feature could be. One suggestion to overcome such lack of understanding was to have every major update of KDE software include “What’s new?” information, preferably with a demonstration video.

Other concerns included the difficulty of hunting down misconfigurations in the KDE settings folder, prompting some users to move their .kde folder and start afresh.

<h2>Strengthening the French KDE Community</h2>
Regarding KDE in France, Geoffray found that French KDE contributors (and other projects' contributors that like KDE) want to see more representation of KDE in France and more KDE events to help people connect. The new Akademy-fr, built on the Akademy-es concept, is a good beginning, but much more is needed.