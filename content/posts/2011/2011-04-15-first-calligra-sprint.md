---
title: "First Calligra Sprint"
date:    2011-04-15
authors:
  - "Boudewijn Rempt"
slug:    first-calligra-sprint
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/calligrasprintworking.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/calligrasprintworkingsmall.jpg" /></a><br /> Calligra Sprint in progress (click image to enlarge)</div>

Over the April 1st - 3rd weekend, the first Calligra sprint took place at the KDAB office in Berlin. With a total of 31 people from 14 nations, the room was crowded to the bursting point! It was a very successful sprint, and the first KDE sprint for many of the attendees.

While hacking continued unabated at all times, a sprint is primarily an opportunity to meet face to face, create new bonds, and discuss current and future issues. As usual, Friday was free-form, with hacking and chatting until it was time to go out to dinner. After dinner we crashed the breakfast room of the hotel because the lobby was too small, and continued hacking.

Saturday was split in two sessions: core development directions and GUI issues.

An important decision was reached on releases. Starting with a first snapshot release in about 3 weeks, we plan to follow up with monthly snapshot releases until we agree that Calligra is ready for a "real" release. Our plan is for a first beta release in September with releases every four months from that point.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/calligra_sprint_group.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/calligra_sprint_groupSmall.jpg" /></a><br /> The Whole Gang (click image to enlarge)</div>

Now that Calligra lives in git, we plan to keep the master branch always releasable. KO GmbH has donated a 12-core build server, which will be used to test all commits for regressions before committing them to the master release branch.

The user interface sessions were kicked off by Anna, who has done a project on Calligra usability at the University of Oulu. Her team tested two things selected by the Calligra developers: the startup screen and the interaction of tools and dockers. She will share her complete, annotated research materials and conclusions with us, as soon as the comments are translated from Finnish to English! Already, based on her conclusions, Thomas Pfeiffer and Camilla Boemann have come up with ways to improve the interaction with the tools and dockers.

On Sunday, smaller bof sessions abounded. 

Camilla Boemann showed off her amazing progress with the new layout engine. The current layout engine (shared by all Calligra applications) has a lot of problems: it is barely testable, its behaviour is annoyingly non-deterministic, and it simply wasn't designed to support basic features, such as proper pagebreaks for multi-page tables. 

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DmitryKazakov33.jpg" /></a><br /> Dmitry Kazakov -<br /> Krita hacker/<br />Group photographer</div>

Starting over with a new layout library, Camilla has created a modern layout engine based on a nested-boxes metaphor. It can already show tables, nested tables, sections, footnotes, selections and more. And when Sebastian Sauer returned from his exile to Thailand, he immediately picked up the ball and started integrating the new layout engine in Calligra Words. 

There was also a Krita session, where the five attending Krita hackers discussed Krita's focus for this year, the task list for the sponsored hackers, the possible Google Summer of Code projects and the desirability of thorough GUI testing. Squish to the rescue!

And so much hacking got done... page caching in Words, an epub import filter, a new testing framework for ODF support, improvements to the outline mode for Stage, saving of table styles, huge progress on the Qt Quick based version of Calligra, a new menu system with a new landing page for Kexi, improvements to the artistic text shape and much, much, much more.

Thank you to KDE e.V. for making the sprint possible. Special thanks to Claudia Rauch for her help. Thanks to KDAB for offering their office, coffee and drinks. For the sponsored dinners, thank you to KDAB and Nokia.

 
