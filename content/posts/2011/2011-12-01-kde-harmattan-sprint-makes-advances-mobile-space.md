---
title: "KDE Harmattan Sprint Makes Advances in the Mobile Space"
date:    2011-12-01
authors:
  - "djszapi"
slug:    kde-harmattan-sprint-makes-advances-mobile-space
comments:
  - subject: "It is nice to hear that KDE"
    date: 2011-12-01
    body: "It is nice to hear that KDE is targeting mobile phones. It is really a shame that Meego was cancelled and that the N950 is not available for normal human beings who are not developers (The N9 unfortunately is touchscreen only)\r\nIs there any chance that other smartphones will be targeted soon, so that one cold flash away this not trustworthy Androidcrap and replace it with KDE/Meego?"
    author: "mark"
  - subject: "Through Plasma Active, mobile"
    date: 2011-12-01
    body: "Through Plasma Active, mobile phones will eventually be on the table of targets."
    author: ""
  - subject: "About keyboard"
    date: 2011-12-05
    body: "You can use bluetooth or usb keyboard with N9. While more complicated, it is also easier to replace than phone's own keyboard. :)"
    author: ""
  - subject: "Hmmm"
    date: 2011-12-09
    body: "N9 and Harmattan is a dead-end. Why not focus on devices that actually have a future ahead of them? N9 will be sold for a while, and then it will disappear, along with any software that has been ported to it. \r\n\r\nInstead of focusing on a declining platform, why not focus on platforms with upward trajectory?"
    author: ""
  - subject: "communities"
    date: 2012-01-09
    body: "We are aware that harmattan is not the best place for us to be, but Nokia and the Harmattan community has been very welcoming and has facilitated our work.\r\n\r\nI'd really like to see work on other platforms, but without devices and a proper free and open platform, it's a really hard thing to do."
    author: "apol"
---
Over the last couple of years the KDE Mobile project has been evolving as it targeted many embedded platforms. Currently, the focus is on the shiny Nokia gadgets (<a href="http://en.wikipedia.org/wiki/Nokia_N9">N9</a> and <a href="http://wiki.maemo.org/N950">N950</a>) running the platform called <a href="http://en.wikipedia.org/wiki/MeeGo#MeeGo.2FHarmattan">Harmattan</a>. Eleven talented developers met in person at a recent KDE Sprint, giving a boost to porting KDE Applications onto this platform, creating new working relationships, and discussing various issues around the KDE Mobile project for handsets. 

The sprint was held at the Physics Institute of the Humboldt University in Berlin between the 18th and the 20th of November (almost on the anniversary of the <a href="http://dot.kde.org/2010/12/03/kdes-mobile-team-meets-first-sprint">first KDE Mobile Sprint</a>, what a celebration!) 

<h2>The Goals</h2> 
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/MobileSprinthacking.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/MobileSprinthackingSmall.jpg" /></a><br />Mobile hacking (click for bigger)</div>

We began collecting the <a href="http://community.kde.org/KDE_Mobile/Sprints/November2011-Planning">main vision and critical points</a> in advance of the sprint. There were many things to discuss and hacking dependency tasks.

Short introductions got people familiar with what others were working on. Despite having individual applications to port, we agreed that there was a need to establish an optimal and simplified development workflow for KDE Mobile Application developers. Therefore, we started focusing on establishing the common infrastructure. 

We discussed the potential development environments available—the <a href="http://harmattan-dev.nokia.com/docs/library/html/guide/html/Developer_Library_Alternative_development_environments_Platform_SDK_user_guide.html">Harmattan Platform SDK (Scratchbox)</a> and <a href="http://harmattan-dev.nokia.com/docs/library/html/guide/html/Developer_Library_Alternative_development_environments_MADDE_terminal_user_guide.html">MADDE</a> (Maemo Application Development and Debugging Environment). It was important to discuss this thoroughly, because it is important to reduce set up time for our developers. 

Set up was a hot topic—how to organize development dependencies such as kdelibs, kde-runtime, libkdeedu and others. We decided to use the Community Repository on the <a href="https://build.pub.meego.com/">MeeGo Community Open Build Service</a>. We agreed to continue this way with an additional point...a a fallback option is needed for publishing KDE packages if the Community OBS is not functioning. 

We talked about the available technologies for the UI—Harmattan and Plasma. For now, the Harmattan components will provide a more native look and feel. The next topic was the packaging workflow of the development cycle. App stores such as <a href="https://store.ovi.com">the Nokia OVI Store</a> accept only applications, no libraries or plugins. After a thorough discussion of the available approaches, we grabbed the idea of getting our dependencies into the same package. We also discussed testing and debugging applications on target devices efficiently after development, and tighter integration of the KDE Software Collection (especially notifications and Plasma components) with the Harmattan platform.

<h2>Getting tasks done</h2> 

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/robert-riemann_kde-harmattan-sprint-group-photo1D.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/robert-riemann_kde-harmattan-sprint-group-photo1BSmall.jpg" /></a><br />Sprinters (click for bigger and names)</div>

The participants had a good amount of discussion and hacking during the event. Importantly, we achieved our goal of creating a <a href="http://community.kde.org/KDE_Mobile/Harmattan">Knowledge Base wiki page</a>. The sprint formed a good foundation for a KDE Android sprint in the first half of the next year. Stay tuned!

When it came time to turn to individual projects:
<ul>
<li>Laszlo Papp got a KDE Harmattan repository in place, set up the <a href="http://community.kde.org/KDE_Mobile/Harmattan">wikipage</a>, and wrote the scratchbox guide and packaging examples. He also got the <a href="http://dot.kde.org/sites/dot.kde.org/files/n950_gluon_invaders.png">Gluon Player</a> built on Harmattan after resolving many issues. He was also a resource to others on platform issues.</li>
<li>Manuel Nickschas worked on the <a href="http://quassel-irc.org/">Quassel</a> port, and shared ideas about MADDE and other topics.</li>
<li>As the local organizer, Thomas Murach took care of us, and showed off his first mobile application.</li>
<li>Aleix Pol Gonzalez dealt mostly with MADDE, made a patch against kdelibs for Harmattan and created documentation about the workflow. He also started a discussion on the kde-buildsystem mailing list about a cmake and MADDE issue. He will continue his work on the <a href="http://www.proli.net/2011/10/03/kalgebra-mobile-and-qtquick/">KAlgebra Mobile</a> project.</li> 
<li>Friedrich W. H. Kossebau studied QML and Harmattan Components for porting <a href="http://www.kde.org/applications/utilities/okteta/">Okteta</a> and dealing with his <a href="http://frinring.wordpress.com/category/kasten/">Kasten</a> framework.</li>
<li>Volker Krause was busy with hosting the <a href="https://sprints.kde.org/sprint/31">KDE e.V. 2011 sprint</a>, and worked on <a href="http://www.youtube.com/watch?v=6xehdxu8fVY">Kontact Touch</a> (video)</li> 
<li>Bruno de Oliveira Abinader helped with Webkit and Mobility questions and investigated the testing and debugging phase of the development cycle.</li>
<li>Robert Riemann helped with local organization when Thomas was not around. He also worked on <a href="http://salout.github.com/blog/2011/09/11/kubeplayer_kde_plasma_active.html">Kubeplayer</a>.</li>
<li>Albert Astals Cid dealt with getting dependency libraries packaged, while working on <a href="http://blinkenharmattan.blogspot.com/">Blinken</a> and <a href="http://tsdgeos.blogspot.com/2011/11/back-to-berlin.html">KTuberling</a>.</li>
<li>Andre Heinecke worked on packaging issues such as Raptor on the Community Open Build Service and kdelibs packaging. He also continued cleaning up Kontact Touch based on his <a href="http://mail.kde.org/pipermail/kde-mobile/2011-October/000478.html">previous Harmattan/Kontact Touch work</a>.</li>
<li>Christian Ratzenhofer was enthusiastic about helping Manuel with the Quassel porting.</li>
</ul>

<h2>Social event with the KDE e.V. 2011 attendees</h2> 

Sprints are not just about getting technical things done, but also about having some off-topic fun. Luckily, the <a href="https://sprints.kde.org/sprint/31">KDE e.V. 2011 sprint</a> was happening simultaneously with ours. So we had the opportunity to gather everyone for a pleasant meal at a nice cafe in Kreuzberg. Immense tastiness and good German beer helped us have an excellent evening there. The KDE folks of the two sprints made it a memorable social meeting. 

On Saturday evening after working hard during the day, a few of us went out for a bit of socializing at "B-Lage" where the beer was cheap and good. The place was awesome for listening to the music and chatting about non-technical issues.
