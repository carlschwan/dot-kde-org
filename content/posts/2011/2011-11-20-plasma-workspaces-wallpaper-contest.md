---
title: "Plasma Workspaces Wallpaper Contest"
date:    2011-11-20
authors:
  - "einar"
slug:    plasma-workspaces-wallpaper-contest
comments:
  - subject: "Kan't wait for this"
    date: 2011-11-20
    body: "Kan't wait for this,..Let's design a kool Picture.."
    author: ""
  - subject: "Examples"
    date: 2011-11-20
    body: "<a href=\"http://dot.kde.org/previous-default-wallpapers\">Previous Default Wallpapers</a>"
    author: "kallecarl"
  - subject: "Is this different than before?"
    date: 2011-11-21
    body: "Is this contest only for the default (i.e. primary) wallpaper? I thought I recalled the contests for previous releases also looking for other wallpapers to be included as extras."
    author: ""
  - subject: "Up to 3 wallpapers"
    date: 2011-11-21
    body: "Up to 3 wallpapers will be chosen (but obviously only one default). <a href=http://pinheiro-kde.blogspot.com/2011/11/stop-me-please.html>Source</a>."
    author: "Hans"
  - subject: "Versions"
    date: 2011-11-21
    body: "Does anyone know in which versions these wallpapers were the default? As far as I know:\r\n\r\nAir - 4.2, 4.3\r\nBlue_Curl - 4.1\r\nEOS - 4.0\r\nEthais - 4.4, 4.5\r\nHoros - 4.6, 4.7\r\nQuadros - forgot, but probably just before Ethais.\r\n\r\n"
    author: "Hans"
  - subject: "quadros"
    date: 2011-11-21
    body: "Was it the default wallpaper for Plasma Netbook 4.4? Before, or around the same time as Ethais?"
    author: "kallecarl"
  - subject: "No more dots, please. "
    date: 2011-11-21
    body: "Please, no more dots. I'm tired of my system having the blue measles.  It's like some old cartoon: \"Hey, doc, I'm seeing spots...\"  Did my laptop just get smacked in the back of the head?"
    author: ""
  - subject: "I'm pretty sure I saw it when"
    date: 2011-11-21
    body: "I'm pretty sure I saw it when running trunk (of Plasma Desktop) before Ethais, so maybe it was in a alpha/beta/minor release."
    author: "Hans"
  - subject: "quadros"
    date: 2011-11-21
    body: "yes quadros was defoult in beta stage people did not liked it and was replaced by ethais"
    author: ""
  - subject: "penguin wallpaper"
    date: 2011-11-25
    body: "Hey.\r\nMaybe something with penguin this time, or kde logo???"
    author: ""
  - subject: "Multi OS desktop environment"
    date: 2011-12-04
    body: "KDE SC is not released only for Linux operating system. Other possible operating systems what people can be using are HURD, FreeBSD, OpenBSD, NT, SunOS and so on.\r\n\r\nSo having a penguin (or TUX what is mascot of Linux operating system) is not option. \r\n\r\nAnd user is not using and seeing a operating system but a desktop environment. So operating system should not be promoted then.\r\n\r\n"
    author: ""
  - subject: "Contest"
    date: 2011-12-04
    body: "There has been contest about those:\r\n\r\nhttp://www.notmart.org/index.php/BlaBla/Reminder_on_the_wallpaper_contes\r\nhttp://blog.uninstall.it/2010/06/24/kde-sc-4-5-wallpapers/\r\n\r\nAnd the latest were http://blog.uninstall.it/2011/04/20/we-love-fresh-wallpapers/\r\n\r\nBut I never saw that contest to proceed and result to be added to 4.7. \r\nIf I remember correctly, every second version would include new wallpapers. Last time it was 4.5 so next were suppose to be 4.7. And next one then again 4.9 but now they jumped to 4.8 (so they can have for 5.0 in future?) but without having the earlier contest results. \r\n\r\nBut asking now just three with one being default one. "
    author: ""
  - subject: "Contest already done for 4.7, where are results?"
    date: 2011-12-04
    body: "There has been contest about those:\r\n\r\nhttp://www.notmart.org/index.php/BlaBla/Reminder_on_the_wallpaper_contes\r\nhttp://blog.uninstall.it/2010/06/24/kde-sc-4-5-wallpapers/\r\n\r\nAnd the latest were http://blog.uninstall.it/2011/04/20/we-love-fresh-wallpapers/\r\n\r\nBut I never saw that contest to proceed and result to be added to 4.7. \r\nIf I remember correctly, every second version would include new wallpapers. Last time it was 4.5 so next were suppose to be 4.7. And next one then again 4.9 but now they jumped to 4.8 (so they can have for 5.0 in future?) but without having the earlier contest results. \r\n\r\nBut asking now just three with one being default one. "
    author: ""
---
With the KDE 4.8 releases drawing near, it's time to change the look of the default desktop. Every two major releases, the main wallpaper of the Plasma Workspaces changes to maintain a fresh style. 
 
This time, artist and Oxygen Master Nuno Pinheiro (creator of most of the previous wallpapers) <a href="http://pinheiro-kde.blogspot.com/2011/11/stop-me-please.html">is asking the KDE community for a new wallpaper</a> by starting a contest. Submissions will be accepted until December 4th. A jury of Nuno and KDE Community Working Group members Ingo "neverendingo" Malchow and Lydia "Nightrose" Pintscher will evaluate the proposals. Up to three submissions will be selected. The judges' favorite will be used as the default wallpaper.
<!--break-->
If you are up to the challenge and want to have your work displayed on one of the largest Free desktops, this is your chance. Send in your work! 
 
<h2>Submissions</h2>
 
Send your entry to nuno@oxygen-icons.org using "wallpaper2011" as subject line (mandatory) <strong>by 4 December</strong>.
 
<h2>Contest rules</h2>
 
<ul> 
<li>Minimum wallpaper size is 1920x1200 (additional resolutions and aspect ratios preferred).</li> 
<li> The license for the chosen wallpaper(s) needs to be LGPL.</li> 
<li>Photos are accepted.</li> 
<li>Wallpapers must have a defining element that reflects KDE values—Elegance, Freedom, and Ease of Use.</li> 
<li>Winners will be notified when a decision is made</li> 
</ul>