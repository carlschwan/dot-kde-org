---
title: "Toasters and Pants at Day Three of Desktop Summit 2011"
date:    2011-08-09
authors:
  - "kallecarl"
slug:    toasters-and-pants-day-three-desktop-summit-2011
comments:
  - subject: "openproject?"
    date: 2011-08-11
    body: "where can this openproject project be found? Besides sharing a name, the creator's homepage shows no mention of it (or is it just upgraded or rebranded chilli?)."
    author: ""
  - subject: "Yeah, based on chilli I think"
    date: 2011-08-11
    body: "Yeah, they said it was based on chilli (if I remember correctly). I guess it's about making customisations that are useful to particular customers."
    author: "Stuart Jarvis"
  - subject: "Wasteful duplication?"
    date: 2011-08-12
    body: "\"at least one panel member sees Firefox and Chrome as wasteful duplication\"\r\n\r\nReally? Would this person care to elaborate for the benefit of people who didn't attend? There are only 2 open source, world-class browsers: Firefox and Chromium. Is that really too many? On top of that, add the fact that these two projects are driven by different goals, have different priorities, make different technical compromises. I'd rather argue that if anything, the market for open source browsers needs more players."
    author: ""
---

The third and final 'traditional' conference day began with Mirko Boehm, Claudia Rauch, Stormy Peters, Karen Sandler and Cornelius Schumacher meeting with members of the Berlin City authorities. The city officials wanted to get acquainted with the free and open source leaders involved in the Desktop Summit, because open technology is important to the local government (more below). 

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/contourdiagram.png" /><br />The Contour concept</div>Perhaps as a result of the beach party the night before, attendance was slightly reduced at the first round of talks, but picked up quickly.  However, there was plenty of interest in the track that included social capabilities in free desktops. And of course, many participants are eagerly awaiting the rest of the week which is filled with meetings, workshops and BoFs.

<h2>Getting Smart with Nepomuk and Zeitgeist</h2>

In a surprisingly well attended session (considering it started early), Vishesh Handa and Sebastian Trüg discussed Nepomuk, and how it is much more than a search service. Nepomuk is currently being used by high profile projects like Akonadi and Telepathy where it serves as a powerful backend and supports substantial linking of data.

While Nepomuk mostly tracks file metadata, Zeitgeist tracks data about events. Federico Mena Quintero explained how Zeitgeist can record all kinds of events for items, such as the webpage from which the file was obtained - so you can download an image or web page and always be able to find the source again later.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/telepathy.jpeg" /><br />Telepathy video chat - it works!</div>Such tools are interesting, but they only become really useful when integrated into other software. Fania Jöck and Marco Martin showcased an activity-based mobile user interface built on Plasma and Nepomuk, in which the system adapts to user needs. It does this by keeping track of context (location, time, active files etc) and patterns (common user activity in similar contexts). A recommendation interface provides access to likely actions such as calling Grandmother if it's her birthday.

In a last minute change to the schedule, Dario Freddi presented an innovative project to bring easy video calling to your TV. Based on Telepathy and GStreamer, with user interface components built with Clutter, the project acts as a plugin to Media Explorer. It is specialized to focus on just a few features (video and audio chat - text chat is not really relevant to a TV). Zeitgeist provides data to prioritize your contact list so the people you are likely to want to contact are easily accessible.

<h2>New Developments by KDE</h2>

Ivan Čukić continued this deep information tracking theme by demonstrating Plasma Activities and explaining how people can use them to change their working environment for different tasks--for example, changing desktop widgets or even open windows--and switch easily from one task to another. So far it is still quite application-centric, but the intention is to make it more object centric. At the heart of all of this information tracking is Nepomuk, tracking usage and making useful suggestions that are better than a simple recent document list. Zeitgeist is also usable as a backend.

Moving beyond the free desktop into the free cloud, Frank Karlitschek highlighted the latest advances in ownCloud. This KDE project enables a user to set up a server quickly to store documents, contacts, music and more, and access anything remotely right from their desktop or via a web interface. Coming in version 2.0 (an alpha release of which is expected shortly) are many new features such as a better web interface, file sharing, an advanced web-based media player and ownCloud extensions, and addons to enrich the ownCloud experience.

<h2>Keynote: The Making of a Toaster</h2>
Thomas Thwaites describes himself as a designer, and has some Computer Science background (he dropped out). Practically speaking, his training, experiences and design approach involve an eclectic mix of disciplines. Based on this background, he suggests that there is a relationship between complexity and design; and that arriving at the most effective design solutions depends on people working collaboratively. In essence, to have a satisfying life, we need the help of others.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/toaster.jpeg" /><br />Even a cheap toaster has lots of bits</div>In his entertaining keynote talk, Thomas shared his experiences and lessons learned from the <a href="http://www.thetoasterproject.org/">Toaster Project</a>. This project started with a quote from "Hitchhikers Guide to the Galaxy" and eventually exposed Thomas to old mining methods and a variety of production techniques.

After reading "left to his own devices he couldn't build a toaster. He could just about make a sandwich and that was it", Thomas decided to discover the real origins of stuff we take for granted. "What would it take to make a toaster these days?" He bought the cheapest toaster possible (about 5 Euro) and took it apart, creating a bewildering range of components. Analyzing the basic components, Thomas undertook to duplicate personally the processes required to produce them. His search brought him iron and copper ore, primitive plastics from potatoes that snails feasted on, and proto-ore plastics from garbage dumps, commemorative Canadian nickels from eBay. The overall product cost, without any labor costs, was about 240 times more than the cost of buying the basic toaster that he took apart

So timesaving devices individually are not that important, but taken together they free up time to do more interesting and important things. He also discovered in a real way how critical collaboration is in producing even the most mundane things.

<h2>Recognizing Achievement</h2>
Both GUADEC and Akademy have their own awards ceremonies to recognize outstanding contributions over the past year. At this year's Desktop Summit, we also had the honor of hosting the <a href="http://www.berlin.de/projektzukunft/wettbewerbe/berlins-zukunft-ist-offen/">Berlin's Future is Open awards</a> (link in German), an award system set up by the Berlin City Government aimed at recognizing the best projects and planned projects in free and open software.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/martin.jpeg" /><img src="http://dot.kde.org/sites/dot.kde.org/files/gnomepants.jpeg" /><br />Martin and Matthias collect their awards</div>
The "Berlin's Future is Open" awards included recognition for efforts to standardize webforms, spread OpenDocument usage online and keep track of events in wikis. The overall winners were Finnlabs with OpenProject, a project management infrastructure and Sugarlabs for concepts about using the Sugar interface for schoolchildren.

This year's Akademy Awards were, as usual, chosen by last year's winners. The award for best non-application contribution went to Dario Andres for his work with the bug triaging team. The application award went to Martin Gräßlin for his work on KWin. Finally came the jury award, a tough choice between many of KDE's dedicated system administrators, including Ben Cooksley, Eike Hein and Jeff Mitchell. However, in the end, it was awarded to Tom Albers for his work in building up a team of contributors, while continuing to do much work himself.

For GNOME, the Travelling Pants ceremony concluded with the award of a well-worn set of trousers to a man described as "possibly not quite human" due to the amount of work he does. Matthias Clasen received the fashion accessory of Desktop Summit 2011 for an outrageously high number of commits to the GNOME codebase over the past year. The origin of the Traveling Pants award is not known to most Desktop Summit participants. However it is such a venerable tradition that to stop would likely shift the Earth's on its axis. 

Not to be outdone, Intel announced the results of a raffle held at their booth over the three days, giving portable laptops to seven lucky winners.

<h2>Closing Thoughts</h2>
The head of the summit organizing team, Mirko Boehm, closed the conference track of the summit with a review of things we have learned in the last few days. There were too many to list fully, but highlights included the informative copyright assignment panel (and the discovery that at least one panel member sees Firefox and Chrome as wasteful duplication), the news that 30% of Qt developers discover the framework through free software, and even the trials and tribulations of building a toaster from scratch.

We saw keynotes that went beyond KDE and GNOME to a much wider world of devices and opportunity. Having brought down the wireless network, we were given food for thought in learning that by 2015 there will be twice as many devices with an IP address as there will be people in the world. Finally, a well deserved round of applause was given to the organizing team and volunteers who have looked after over 700 attendees. It was agreed they would get to keep the red t-shirts they have been wearing as a well deserved prize for their efforts.

So much has happened already and yet we are barely halfway through the summit - there are still meetings and workshops through to Friday, with much more news and views to come out of those. Stay tuned.