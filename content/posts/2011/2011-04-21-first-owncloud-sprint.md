---
title: "First ownCloud Sprint"
date:    2011-04-21
authors:
  - "blizzz"
slug:    first-owncloud-sprint
comments:
  - subject: "Brilliant"
    date: 2011-04-22
    body: "Beautiful both on the results itself and the write-up ;-)\r\n\r\nThe 5 minute issue is very recognizable, hehe... Will 2.0 come with an easy susestudio.com image which I could deploy to Amazon EC2 in one click? That'd be a nice and easy way to get my ownCloud up and running, esp considering that the micro instance on Amazon EC2 is free ATM... Would that micro instance be fast enough for OK ownCloud performance? If so, I really look forward to using it..."
    author: "jospoortvliet"
  - subject: "owncloud on nas?"
    date: 2011-04-28
    body: "I would like to see my own owncloud run on my QNAP NAS, I'll start follwing you from now and see what I can contribute in the future. \r\n\r\nA future feature could be F2F (friend 2 friend) networking support, to securely share files/communication with only trusted ones. And extend on the small world principal that everybody knows anyone else in the world in 7 steps:)"
    author: "pljanson"
  - subject: "owncloud2 on QNAP works!"
    date: 2011-10-21
    body: "I like to let you know that someone made QNAP qpkg packages to install owncloud 2 on a QNAP. See http://forum.qnap.com/viewtopic.php?f=195&t=50820#p230711  It installs out of the box (on my TS219P)! Now I have to get to know it;^) \r\n\r\nFor the future I see integration with QNAPs media directory for the music and user management (just to keep dreaming).\r\n\r\nGREAT work owncloud & qnap guys!"
    author: "pljanson"
---
<p>For four days, starting on Friday April 15th, about half a dozen souls gathered in the hive01 headquarters in Stuttgart. The goal of this very first <a href="http://owncloud.org/">ownCloud</a> sprint was to discuss, plan and of course hack on the web services project.</p>
<p>To kickoff we had a brainstorming session and discussion of the topics that were to be dealt with over the following days. We extensively debated fundamental things concerning the future directions of ownCloud.</p>
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/photo-discussion.jpeg" /><br />Discussing the navigation structure</div><h2>Releasing ownCloud 1.2</h2>
<p>OwnCloud 1.2 is the next maintenance version of the ownCloud 1.x series. It includes several bugfixes; if you are a user of ownCloud 1.1 you should <a href="http://owncloud.org/index.php/Get_ownCloud">upgrade to ownCloud 1.2</a>. Since the team decided on a new code-base at this sprint, ownCloud 1.2 will also be the final release before ownCloud 2.0 enters the wild.</p>
<h2>Refactoring, Facelift and Possibilities</h2>
<p>Probably the most important work was done on refactoring ownCloud's initial concept and work by Jakob Sack. These changes will help to make the codebase clearer, easier to maintain and a lot more flexible. ownCloud will be easy to extend with additional applications and plugins. In the very near future ownCloud's web-based storage system will be capable of being enriched by extensions like Music Streamers, Photo Galleries or PIM functionality – basically anything a PHP application can do.</p>
<p>In order to make the installation of extensions straightforward we have created <a href="http://apps.owncloud.com/">ownCloud Apps</a>: an online application sharing platform which will easily be accessible via the Open Collaboration Services (OCS) API, that e.g. acts also as the backend for GetHotNewStuff you know from KDE applications. There will also be an interface for knowledge-base articles which will be accessible from within ownCloud. So instead of writing the user documentation the ownCloud users can easily excange questions and answers directly from within ownCloud itself.</p>
<p>Since ease of use is one of our main goals for ownCloud we are very happy to have Jan-Christoph Borchardt with us, who brought a lot of expertise in creating and optimizing user interfaces. Based on the <a href="http://www.kubler.org/owncloud-mockups/">great mockups from François</a> we heavily worked on not only improving ownCloud's UI but also simplifying processes like the installation.</p>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/screenshot-files.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/screenshot-files-sml.png" /></a><br />Refurbished interface (click to enlarge)</div><h2>Doing the deed</h2>
<p>Typically, we quickly turned new ideas into code, resulting in us staying in the office until Sunday afternoon (with small breaks to sleep) and supported by the ultimate power of pizza deliveries. Though we intented to put a foot on the street, everyone needed five more minutes to commit a change, which practically ended in an infinite loop of development. On Sunday afternoon and evening we managed to have our "sightseeing" activities, and visited a Turkish fast food restaurant and an Irish pub (everything else was closed anyway).</p>
<p>The foundations laid on this sprint will be the base for the next release, which will be ownCloud 2.0. Seeing the good and fast results during the last days we want to get ownCloud into shape with all the basic functionality and release it as soon as possible, probably before this summer.</p>
<h2>And still more on ownCloud's heap</h2>
<p>We covered even more topics, that currently still need a bit more consideration. For example, translation. There are a few obstacles in translating web applications, which differs from desktop programs you usually compile. Nevertheless we want to hook up into KDE's translation system, of course, and use the synergy effects.</p>
<p>We are also looking into <a href="http://community.kde.org/Plasma/Active">Plasma Active</a> integration and are confident that ownCloud will work well as a web widget on a wide variety of devices, because making ownCloud usable from different kinds of devices, including Smartphones, Tablets, and Netbooks, has been considered throughout the design of the user interface. Nevertheless everybody is welcome to work on QML widgets that access or interact with ownCloud.</p>
<div style="width: 302px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/photo-group.jpeg" /><br />Participants hacking: Frank Karlitschek, Jakob Sack, Robin Appelman, Jan-Christoph Borchardt, Arthur Schiwon (from left to right)</div><h2>Join ownCloud</h2>
<p>We have made huge steps forward, but still  need new people to join in ownCloud development and help to make the cloud a better place. If you want start to code away, check out the <a href="https://projects.kde.org/projects/playground/www/owncloud/repository/show?rev=refactoring">the current source code</a> and implement a feature that you'd love to see in ownCloud, like a photo gallery, access over additional protocols (webdav is supported only at the moment) or Amarok integration to list a few examples.</p>
<h2>Last but not least</h2>
<p>We warmly thank KDE e.V. for sponsoring the sprint! We have had a lot of fun, agreed on plenty of topics, and <a href="https://projects.kde.org/projects/playground/www/owncloud/activity">hacked on countless features and techniques</a>, making the ownCloud sprint a full success.</p>
<p>Just 5 more minutes to world domination.</p>