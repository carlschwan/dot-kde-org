---
title: "KDE Puts You In Control with New Workspaces, Applications and Platform"
date:    2011-01-26
authors:
  - "sebas"
slug:    kde-puts-you-control-new-workspaces-applications-and-platform
comments:
  - subject: "Nice!"
    date: 2011-01-26
    body: "I like the announcement! Clear, to the point and easy to understand. \r\n\r\nNow, what I miss are non-plasma workspaces :)\r\n\r\n(that\u2019s only from the announcement text: It sounds like KDE could support workspaces which are not plasma \u2014 is something planned in that direction: A generic API a KDE workspace has to support?)\r\n\r\nLose weight is likewise cool! Is the mobile target suited to low-power desktops, too?"
    author: "ArneBab"
  - subject: "Help share the news on social networks"
    date: 2011-01-26
    body: "Some places where you can vote and share the news:\r\nReddit \u2014 http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_460_released/\r\nDigg \u2014 http://digg.com/news/technology/kde_software_compilation_4_6_0_released\r\nFSDaily \u2014 http://www.fsdaily.com/EndUser/KDE_Software_Compilation_4_6_0_Released\r\n\r\nAnd don't forget to dent and tweet and facebook and others."
    author: "JLP"
  - subject: "broken links"
    date: 2011-01-26
    body: "At kde.org these links:\r\nhttp://kde.org/4.6/plasma.php\r\nhttp://kde.org/4.6/applications.php\r\nhttp://kde.org/4.6/platform.php\r\nare broken.\r\n\r\nhttp://www.kde.org/ is not the same as http://kde.org"
    author: "nickkefi"
  - subject: "Good work folks"
    date: 2011-01-26
    body: "Many thanks to everyone who has contributed to this release!\r\n\r\nI haven't installed it yet (give me time!), but from the screenshots it looks like there is a lot of polish in this release."
    author: "matt"
  - subject: "RE: non-plasma workspaces"
    date: 2011-01-26
    body: "> Now, what I miss are non-plasma workspaces :)\r\n\r\n> (that\u2019s only from the announcement text: It sounds like KDE could support workspaces which are not plasma \u2014 is \r\n> something planned in that direction: A generic API a KDE workspace has to support?)\r\n\r\nWell, KDE applications can run under many different shells/DEs whether they are X11 based (like Gnome, XFCE, Maemo/Meego etc.) or something different (OS X, Windows, even Haiku).\r\n\r\nWhile it would be possible to create one, I'm not aware of a non-Plasma workspace currently developed by the KDE community.  The beauty of Plasma is that you can mix and match the various plasmoids, containments, data engines, etc. to create workspaces that look and behave quite differently from each other but still share a large proportion of their codebase - Plasma is basically a toolkit for creating different workspaces.  \r\n\r\nEven if you want to produce something totally different to what is currently available you could still save yourself a huge amount of time and effort by reusing common components and only implementing the relatively few bits that would be required to make your workspace unique. (Consider how different the desktop and netbook workspaces look and feel.)"
    author: "matt"
  - subject: "re: broken links"
    date: 2011-01-26
    body: "Fixed, thanks."
    author: "pinotree"
  - subject: "Good work Indeed"
    date: 2011-01-27
    body: "I did install it already on 4 systems running Kubuntu.\r\n\r\nTake it from me, it is really an excellent release. Faster, No issues so far and over all very nice.\r\n\r\nI specially like being able to switch between Desktop & Notebook interface on the fly when using my laptop. \r\n"
    author: "Abe"
  - subject: "In the dolphin article, the"
    date: 2011-01-27
    body: "In the dolphin article, the link to \"Various Improvents to the Service Menus\" is also broken.. Can you can fix it? :)"
    author: "vdboor"
  - subject: "Kdepim broken :("
    date: 2011-01-27
    body: "https://bugs.kde.org/show_bug.cgi?id=262339\r\n\r\nReally sad news, it's a really annoying bug... Sure Kde devs use kdepim 4.6 and so don't see the bug...\r\n\r\nAnd for beta realease, all distrib was shiping Kdepim 4.6... So no one notice this bug :-/"
    author: "gnumdk"
  - subject: "4.4.10"
    date: 2011-01-27
    body: "KDEPIM 4.4.10 is just around the corner. Maybe they fixed that in there?\r\n\r\nRegards"
    author: "Damnshock"
  - subject: ":("
    date: 2011-01-27
    body: "In fact, this google akonadi agent that is broken."
    author: "gnumdk"
  - subject: "Fixed"
    date: 2011-01-28
    body: "Fixed"
    author: "tsdgeos"
  - subject: "KWin is definitely faster for me"
    date: 2011-01-28
    body: "Screenshots, new wallpaper and overall looks are wonderful!\r\n\r\nWhat I really like about this releaase is the KWin performance improvements. On my old white MacBook the responsiveness / painting speed has greatly improved. All applications react faster! This is a big win for this release!"
    author: "vdboor"
  - subject: "Can System Tray icons FINALLY be made bigger in v4.6?"
    date: 2011-01-28
    body: "Honestly, the more we go up in the numbers, the least I am eager for the newest version to come up. \r\nMaybe its a sign of stability and the jumps arent as dramatic as the previous versions.\r\n\r\nThat said, there is one option that KDE has yet to add and that is the size of the System Tray icons.\r\nI do home support for two dozens friends and family (i still spend less time than I use to supporting 5 Win machines my parents and family used.) as well as a community center we helped recycle old computers and that's a lot of seniors.\r\n\r\nThey love KDE because I can modify everything to their liking... which is BIG. \r\nI know the UI fanatics would have heart attacks but 18pt fonts, 1 inch taskbars and huge icons is the norm. Everything can be made bigger but the System Tray icons.\r\nSo for many people, its a question of hit and miss where the only difference between the light blue of Kopete and the light green of Skype's icon is minimal (I just hope the monochrome option can be changed back).\r\nEvery single time I am asked that I offer a magnifying tool but these people arent blind... they just want the icons to be able to expand as the taskbar gets bigger.\r\nAnd while Im not old, I wear glasses and find the micro-size mania to be annoying.\r\n\r\nIm not saying to change defaults (i hate the default themes and windows decoration on KDE but who cares, I got plenty of options like Slim Glow and Plastik) just allow those icons to be bigger if the user so wishes. Ive noticed other widgets that use icons have the options to be made bigger but not the system tray.\r\n\r\nThe only reason I feel a need to upgrade is the faint hope that this oversight has been corrected. But I also have this feeling that the problems that nearsighted folks have seeing the system tray will continue."
    author: "zeke123"
  - subject: "systray size"
    date: 2011-01-28
    body: "We're working towards this,  but it's not yet possible.\r\n\r\nThe \"old\" systemtray protocol just specified 22x22px images, which we'd have to use, no control whatsoever about size, theming, etc.\r\n\r\nThe new protocol does the rendering in the workspace shell (as opposed to the app owning the system tray entry). So we can now do things such as theming. The infrastructure to make systray icons bigger is there, but nobody has done it yet.\r\n\r\nDid you file a wishlist item on bugs.kde.org about that? Otherwise, how should developers be aware of this desire? Our crystal balls are pretty good, but they don't show every single wish of our users. We need help by mundane communication once in a while. :-)\r\n\r\n(And no, pointing it out on the dot is not an alternative to bugs.kde.org, because it will inevitably just get lost here, while bko is being actively tracked by the developers.)"
    author: "sebas"
  - subject: "systray size"
    date: 2011-01-28
    body: "Its been reported already as <a href=\"https://bugs.kde.org/show_bug.cgi?id=133936\">bug 133936</a>."
    author: "matt"
  - subject: "Fail"
    date: 2011-01-29
    body: "obviously,  both your cristal balls and your bugtracker failed because they seem to just ignored the community in this case.\r\n\r\nIf I would take you by the word, somebody should have done something about this bug aready:  https://bugs.kde.org/show_bug.cgi?id=262339 \r\n\r\nIt would be nice if you would not blame the users for articulating here when they see themselves ignored on the bugtracker."
    author: "BurkeOne"
---
<p>
KDE is delighted to announce its latest set of releases, providing major updates to the KDE Plasma workspaces, KDE Applications and KDE Platform. These releases, versioned 4.6, provide many new features in each of KDE's three product lines.<!--break-->Some highlights include:
</p>
<div  align="center" class="screenshot"><a href="http://www.kde.org/announcements/4.6/screenshots/46-w09.png" title="Click to enlarge"><img src="http://www.kde.org/announcements/4.6/screenshots/thumbs/46-w09.png" style="text-align:center" alt="KDE Plasma Desktop, Gwenview and KRunner in 4.6" title="KDE Plasma Desktop, Gwenview and KRunner in 4.6"/></a><br /><em>KDE Plasma Desktop, Gwenview and KRunner in 4.6</em></div>

<h3>
<a href="http://www.kde.org/announcements/4.6/plasma.php">
Plasma Workspaces Put You in Control
</a>
</h3>

<p>
The<b> KDE Plasma Workspaces</b> gain from a new Activities system, making it easier to associate applications with particular activities such as work or home tasks. Revised power management exposes new features but has a simpler configuration interface. KWin, the Plasma workspace window manager, receives a new scripting and the workspaces receive visual enhancements. <b>Plasma Netbook</b>, optimized for mobile computing devices receives speed enhancements and becomes easier to use via a touchscreen interface. For more details read the <a href="http://www.kde.org/announcements/4.6/plasma.php">KDE Plasma Workspaces 4.6 announcement</a>.
</p>

<h3>
<a href="http://www.kde.org/announcements/4.6/applications.php">
KDE’s Dolphin Adds Faceted Browsing
</a>
</h3>

<p>
Many of the <b>KDE Application</b> teams have also released new versions. Particular highlights include improved routing capabilities in KDE’s virtual globe, Marble, and advanced filtering and searching using file metadata in the KDE file manager, Dolphin: Faceted Browsing. The KDE Games collection receives many enhancements and the image viewer Gwenview and screenshot program KSnapshot gain the ability to instantly share images to a number of popular social networking sites. For more details read the <a href="http://www.kde.org/announcements/4.6/applications.php">KDE Applications 4.6 announcement</a>.<br /><br />
</p>

<h3>
<a href="http://www.kde.org/announcements/4.6/platform.php">
Mobile Target Makes KDE Platform Lose Weight
</a>
</h3>

<p>
The <b>KDE Platform</b>, on which the Plasma Workspaces and KDE Applications are built also gains new capabilities available to all KDE applications. The introduction of a "mobile build target" allows for easier deployment of applications on mobile devices. The Plasma framework gains support for writing desktop widgets in QML, the declarative Qt language, and provides new Javascript interfaces for interacting with data. Nepomuk, the technology behind metadata and semantic search in KDE applications, now provides a graphical interface to back up and restore data. UPower, UDev and UDisks can be used instead of the deprecated HAL. Bluetooth support is improved. The Oxygen widget and style set is improved and a new Oxygen theme for GTK applications allows them to seamlessly merge into the Plasma workspaces and look just like KDE applications. For more details read the <a href="http://www.kde.org/announcements/4.6/platform.php">KDE Platform 4.6 announcement</a>.
</p>
