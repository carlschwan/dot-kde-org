---
title: "KDE Having Fun at Desktop Summit 2011"
date:    2011-08-07
authors:
  - "Stuart Jarvis"
slug:    kde-having-fun-desktop-summit-2011
comments:
  - subject: "Any recordings of the talks"
    date: 2011-08-07
    body: "Any recordings of the talks online? "
    author: ""
  - subject: "Not yet..."
    date: 2011-08-08
    body: "Not yet (as far as I know). But, apart from any occasionalt technical issues, everything has been recorded and the videos will be put online in the coming days. The slides will also be made available."
    author: "Stuart Jarvis"
  - subject: "asimplecomputer.com AWESOME!!!"
    date: 2011-08-10
    body: "www.asimplecomputer.com is just what we - Free Software community - need. This makes me want to buy one. Greatness. What about http://open-pc.com/ ? I guess \"asomplecomputer.com\" is the future. It looks great, thank you for making this real."
    author: ""
  - subject: "It probably depends on you"
    date: 2011-08-11
    body: "(This is my personal opinion)\r\n\r\nopen-PC is more for people who already know what they want (linux, kde software) and how to deal with it\r\n\r\nasimplecomputer.com is a bit more general in audience, everything is set up already (which may not be so necessary or even welcome for a geek audience, though of course you are free to change it all)\r\n\r\nSo maybe if you read the dot you might like open-pc but point your less-techy friends to asimplecomputer.com. Or maybe not ;-) There is definitely some overlap and choice is good, right?"
    author: "Stuart Jarvis"
  - subject: "Thanks"
    date: 2011-08-14
    body: "Thanks for your happiness about our new project and idea.\r\n\r\nAbout the open-pc.com and asimplecomputer.com comparison I'd like to hook up with what Stuart wrote."
    author: "unormal"
---

<h2>Friday, 5 August - Preparations for a Big Event</h2>

Friday, 10:00, August 5, 2011. A big group of people was standing a bit lost in the cloakroom of the Humboldt University at Unter den Linden, Berlin. They were the volunteers for the Desktop Summit 2011 - but without guidance and leadership, they were just nervously looking around and talking to each other.

But at 11:00, Mirko Boehm came in, gathered everyone together and told them what to do! Tables got moved, tape stuck to floors, posters hung up.

Meanwhile, another group of volunteers began gathering at <a href="http://www.c-base.org/">c-base</a>, worlds' first hacker space and site of the oldest crashed space station on Earth. Thanks to sponsor Igalia, several hundred attendees came to this pre-registration event to have drinks and conversation in the creative space of c-base. This also gave the registration team an opportunity to figure out good processes for namebadges and lunch vouchers. And allowed the first visitors get their badges, a welcome hug and lunch tickets for the rest of the week. A long day of hard work, and the team was ready for the official opening of Desktop Summit.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020243.jpeg" /><br />Dirk Hohndel</div><h2>Saturday, August 6 - The Summit Begins</h2>
Mirko Boehm opened the Desktop Summit and handed over the microphone to the first keynote speaker, Dirk Hohndel. Dirk spoke about where Linux Desktops came from and where they are going. He expressed his happiness with Free Desktop projects moving beyond copying commercial products to making their own choices and providing leadership. He stressed the importance of listening to users in making those choices. Dirk then got on the subject of collaboration and suggested that while disagreements might make the news, restraint, flexibility and openness make for better products and more satisfaction for users and contributors. Dirk's talk was a delightful, meaningful introduction to the collaborative Desktop Summit.

As the conference moved into the general schedule, attendees heard from project leaders and FOSS contributors in more detail on various subjects related to to common desktop interests. For the most part, presentations will be available on line soon.

<h3>Copyright Assignment Panel</h3>

The panel on copyright assignment, licensing and similar rights assignment policies was highly anticipated. CA is a hot issue in the Free software world, dealing with the potential conflict of interests between company/project managers and contributors. Some companies and projects require copyright assignment, while developers naturally want to understand what will happen to their code. The discussion touched on Project Harmony, an effort led by Canonical to define a set of contributor agreements and simplify the choices in the same way that Creative Commons makes it easier to choose a free license for creative works.

The panel was made up of key figures in the Free Software community from both sides of the debate. Mark Shuttleworth, founder of Canonical, promoted contributor agreements as essential to building a thriving ecosystem around Free Software. Michael Meeks, part of the LibreOffice founding team and long time GNOME contributor, explained how he was once in favor of assigning his copyright to larger projects. Now he has come to see problems in terms of what he calls the "Scalability, Copyright and Ownership" problems. Most companies that require copyright assignment (e.g., MySQL) end up doing most of the work themselves rather than getting real support from the community. Moreover, you can expect conflicts and FUD due to copyright issues. Finally, CA leads to less feeling of ownership by the community which in turn leads to maintenance problems. Bradley Kuhn of the Software Freedom Conservancy and the Free Software Foundation is also generally against copyright assignment to companies, arguing that projects choose licenses that reflect their values and contributor agreements change that.

<div style="width: 352px; float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020267.jpeg" /><br />Copyright assignment panel: Karen Sandler, Mark Shuttleworth, Michael Meeks and Bradley Kuhn</div>The panel was moderated by Karen Sandler, the new Executive Director of the GNOME Foundation and former General Counsel with the Software Freedom Law Center. She kept the debate on topic and explained jargon when necessary.

The discussion was lively. Mark asserted that "freedom is not on the table for discussion. Freedom is absolute", a point challenged by Bradley who argued that software freedom is "always on the table" and thus needs to be actively protected.

Shuttleworth's comment that "Linux, the Free software system, is not thriving on the client" set off another series of comments and rebuttals about funding vs contribution. In order for FOSS to thrive in the desktop environment, Mark says that companies - especially startups - need what copyright assignment offers. It gives them a competitive advantage which makes it easier for them to make money. Meeks agreed that a stronger, broader ecosystem would be great, but copyright assignment defeats the point of Free Software as it creates monopolies. Mark didn't like to talk about monopolies, as "in terms of access to the code there is no monopoly", but Bradley disagreed, pointing out that, under most assignment circumstances, only one company can take the code proprietary. This is indeed the 'competitive advantage' which was being discussed.

Michael said that ownership is critical, because "if people don't feel they own part of the code, they feel very differently about the project ... they have at best weak allegiance to the project."

Bradley pointed to the value of placing trust in an organization like the Free Software Foundation. Mark suggested that it makes sense to "trust the organization where your code fits, if you want your code to thrive." And added that, "you have to choose how you want your code to behave in a world of change. I believe the moral right lives with the project." In other words, code without the project it belongs to loses its value. So ownership by the project or company makes sense.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020259.jpeg" /><br />A full audience for the debate</div>We spoke with Karen Sandler on this subject afterwards, asking for her personal view in this. She agrees on this point, noting that a GPL "or later" kind of license effectively commits a contributor to a license they have not yet seen. This worries even some developers who trust the FSF, as the FSF is the organization which can then incorporate new things with a later version of the GPL. If the code is owned by a strong, independent, charitable entity which is trusted by the contributors, they can ensure the goals of the community and the license stay aligned. This trust can be achieved in a number of ways, including assignment agreements that limit how the code can be licensed later, or by ensuring election of leadership by a broad membership. However, she opposes assigning the code to a corporate party due to the earlier mentioned monopoly on the ability to take the code proprietary, which goes against the software freedom ideals she supports. It gives them too much power, something Meeks also brought up several times.

On the issue of patents, the differences of opinion were again on display. Mark asserted that, under the Harmony agreements, a contributor does not give away patent rights, but rather provides the necessary rights to ship the code. The debate centered on whether contributors receive the same rights back from the company with respect to patents that they themselves are expected to provide. Meeks said he has problems with the inequality which is created by the copyright assignment and he believes there's enough competition already, we don't need an advantage for a single company. He pointed out that contributors could end up in a situation where even they themselves don't have the right to ship the software they wrote themselves, despite a permissive license, because the patents are owned by the company they assigned their code to.

Mark Shuttleworth ended the discussion with a comment on generosity: "giving something to somebody is generous, you should try that, it is very satisfying". This resulted in quite some hallway discussions. It seemed to be inappropriate to some to lecture on generosity to a room full of Free Software developers!

<h3>Hallway track</h3>
Between and during the talks, the hallway track was well attended. Groups of people were meeting face to face everywhere; some were grumpy that there wasn't enough 'hacking space'. Access to AC was also limited, but an empty battery means having talk to people, which was quite good for the general atmosphere! The main hall was quite lively, with a number of info booths and in the registration area you can buy t-shirts, stickers, flyers and other swag. Outside, people gathered for a smoke or just to enjoy the sun, escaping from time to time into the shade of the majestic trees in the courtyard. Some even took a stroll, enjoying the area around the University. 

Lunch was in the Uni's canteen with pasta and salad. This was all washed down with the drink of the week, Club Mate, a carbonated caffeine drink made from the popular South American tea. It's not too heavy and keeps you alert. Thank you, Intel, for keeping us in supply.

In the evening, a large group headed back to c-base for a barbecue, while others went out in various directions to enjoy Berlin and its good food. Berlin is a fun city. Big, but friendly, active, accessible, with a mix of old and modern. Crumbling cement alongside elegant structures, greenery. There's no opportunity to be bored.

<h3>KDE at the Desktop Summit</h3>

While there were a number of talks by GNOME and KDE developers during the day, we'd like to touch on a few interesting talks which were about where KDE, as a community, stands.

Stu Jarvis from the KDE Promo team gave one of the community keynotes, asking what KDE is for nowadays. He explained that when KDE started, the motivations were clear: to make a Linux desktop environment that was as good as that available on Windows 95 and so to make Linux and free software usable by normal people. But we achieved that aim many years ago. Now we have all kinds of KDE software, including those that have their own release schedule, that do not use the KDE Platform (for example, Marble has a Qt only version) and software that is not about the Linux desktop at all, such as ownCloud. Stu referred to a TED talk that looks at how organisations need to understand not only what they do and how, but also why - and to communicate that if they want people to listen to them.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1020256.jpeg" /><br />Knut Yrvin on how KDE helps Qt</div>Stu went back to the core question of why we work on KDE and quoted Aaron Seigo and Cornelius Schumacher who argue that "KDE software is the result of the pursuit of freedom" and that "everyone should have access to great technology". If that logic is followed through then ownCloud and other apps are absolutely part of KDE's mission. Stu argued that we should not be afraid to branch out beyond our traditional areas to bring free software to everyone. In pursuit of this goal, he also announced a new project by Mario Fux, launching later this week, that will offer easy to use computers based on Debian and KDE software, initially in three form factors - a compact desktop, netbook and notebook. Everything will be preinstalled and configured, KDE software will be to the fore and part of the sale price of each item will be donated to Debian and KDE e.V. (asimplecomputer.com)

Asked what was KDE specific about the message, Stu acknowledged that most of the ideas apply to the whole freedesktop community, not just KDE. However, he argued that KDE is branching out more already, while the core of GNOME maintains a desktop focus. There are however groups and companies in the GNOME ecosystem looking much wider than the Linux destop and perhaps some opportunities for collaboration.

Another talk of interest to anyone in the free software community (and beyond) was given by Thomas Thym on conflict resolution. He argued that the normal obsession with finding out who is to blame for problems is counterproductive and it is much better to look at people's interests than positions to work towards a solution that everyone can live with

Knut Yrvin, Community Manager for Qt Development Framework, reported on the expansion in the use of Qt; it's everywhere, even on projectors in cinemas. Nokia is a big contributor, KDAB as well. In addition, at least 23 KDE people contributed 46,000 lines of code in 2010. And KDE is a major contributor to market awareness of Qt in subtle ways. 30% of developers surveyed recently reported discovering Qt through their use of FOSS, especially KDE. Meanwhile, Google Summer of Code introduces many students to Qt.

<h3>Conclusion</h3>
We've heard many people say how the Desktop Summit again exceeds expectations. As Karen mentioned during the press conference, it is amazing to see how well everyone is getting along. Or perhaps, as Aaron argued, it is not amazing at all - he claimed the communities have worked together so well for so long that it shouldn't be surprising anymore. We all want Free Software to succeed on the desktop and the new form factors. We work on similar things, according to similar values. We learn from each other and we should focus on getting the best from having two major Free Desktop projects - don't duplicate technology just for the sake of it, but compete and make sure the best technology wins! For that, it's important to be open to each other's technology, to be more accepting and to be more aware of what each community is doing. Where we can build on one another's technology we should. If we think we can find a new way and do something in a better way, then everyone can benefit.