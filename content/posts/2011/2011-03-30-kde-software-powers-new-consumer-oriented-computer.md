---
title: "KDE Software Powers New Consumer-Oriented Computer"
date:    2011-03-30
authors:
  - "tardypad"
slug:    kde-software-powers-new-consumer-oriented-computer
comments:
  - subject: "It's not KDE, it's their own development ..."
    date: 2011-03-30
    body: "Well, at least they sell it as such on their website.\r\n\r\n\"Self-developed Xompu operating system (Xompu-OS) with a clear and intuitive desktop shell designed with the latest knowledge\"\r\n\r\nI have mixed feelings about statements like that.\r\n\r\nThey also state (quoting BITKOM, a German federal association for telecommunication technology and new media) that 17% of the computer users have been so annoyed that they wanted to throw their computer out of the window.\r\nWell, can it be funnier to say nothing? Wanted to? Well ... If asked, I would have wanted to have killed people but I never actually did it. ;)\r\n\r\nOh well, they are probably nice people who just have to play by the rules of the market, so I'll stop ranting here and wish them luck. :)"
    author: "icwiener"
  - subject: "Personally I don't really"
    date: 2011-03-31
    body: "Personally I don't really like such a concept -- no matter what technology is used. ;)\r\nThey mention that no program is installed locally \"wasting\" your hd space, thus it looks to me that without internet connection you can do absolutely nothing. Yes I know that the Internet is taken as commodity nowadays, like power. Still just yesterday my connection was quite unreliable.\r\nFurther you are not able to install more programs yourself as is outlined in the FAQ.\r\n\r\nI also wonder if they release any sources of the software."
    author: "mat69"
  - subject: "Excellent Hardware/Software/Service stack idea!"
    date: 2011-03-31
    body: "Most consumers cannot resolve computer problems - they call their geek friends or family members (like me) to fix them, or they have to take their computer to a repair shop and drop it off for several days to have viruses removed, or registry cleaned, or to correctly change the system's configuration. Geeks can still build their own standalone PC's, but in the future, I think nearly all non-geek computer users will be getting a PC-with-service like this. All they have to do is pay some type of support contract, and their PC can be fixed and configured remotely, giving them almost 100% uptime. Excellent idea! This should work very well with KDE, since you will already have tools like SSH, VPN, Remote Desktop, etc available in the system to help with remote admin tasks."
    author: "andyprough"
  - subject: "Actually, because of"
    date: 2011-04-21
    body: "Actually, because of technology's fast progression, remote computer repairs are becoming popular nowadays and they are of great help for PC users that are not that familiar with technicalities. \t\r\n"
    author: "Eilaj"
---
<a href="http://www.xompu.de/">Xompu</a> (website in German - some <a href="http://www.xompu.de/index.php?cID=156">basic information in English</a>) is a new German company whose goal is to provide an easy-to-use complete computer, backed up by service support. Xompu released their computer a few weeks ago. And guess what? It runs Plasma Desktop and other KDE software.

<b>Damien Tardy-Panis</b> interviewed <b>Robert Konopka</b>, one of the founders of Xompu, to find out more about the company and why they chose KDE software. Read on to find out more about Xompu, what they think of KDE and our software, and news on job opportunities with the company.
<!--break-->
<h2>Xompu</h2>

<b>Damien:</b> Can you tell us more about this project? What's new in there?

<b>Robert:</b>Xompu is the first company that offers private customers in Germany a hassle-free computer experience with customer support via remote maintenance. The PC-package includes basic software and an administration service as well as customer support via telephone and email. This all-around package is new, and will help our clients to get rid of computer viruses, tech support nightmares, and onerous system maintenance. It is mostly based on open source software.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Xompu-Team.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Xompu-Team-sml.jpg" /></a><br />The Xompu team (click on image to enlarge)</div>

<b>Damien:</b> What kind of users do you expect to use this product?

<b>Robert:</b> Given that our PC offers an easy plug-and-play solution, we first think it will appeal to inexperienced users. But in general, I’m convinced that the Xompu-PC is perfect for the average user who only wants to surf the Web, check e-mail, and write documents occasionally.

<b>Damien:</b> Your computer is claimed to be easy to use. What are its advantages over what currently exists in the market?

<b>Robert:</b> The biggest advantage of the Xompu-PC is the support service we offer for our customers via remote maintenance. By eliminating the need to find, install, configure and update all kinds of software on their own, customers save time and aggravation. It is this convenience, combined with the automatic data backup and remote access, that makes the Xompu an attractive, hassle-free and safe option.

<b>Damien:</b> Your company is supported by the the German Ministry of Economy and Technology. What are their goals in supporting the project and what does it imply for Xompu?

<b>Robert:</b> The Federal Ministry of Economics and Technology (BMWi) supported us through the EXIST program. This program aims at improving the entrepreneurial environment at universities and research institutions, and at increasing the number of technology and knowledge based business start-ups. The EXIST program is part of the German government’s “Hightech Strategy for Germany” and is co-financed with funding from the European Social Fund (ESF). For Xompu, this meant that the entrepreneurs received a grant to cover their living expenses. In addition, Xompu received materials and equipment, as well as funding for coaching. The university provided infrastructure during the EXIST phase. 

<b>Damien:</b> Who is currently working on this project?
<b>Robert:</b> We are a team of experienced engineers and businessmen who complement one another and are all driven by the motivation to be part of the great development of the innovative Xompu-PC solution. Nevertheless, we are looking all the time for new motivated team members from all disciplines who share our vision.

<b>Damien:</b> What was your personal motivation to start such a project? And now, what drives you every day at work?

<b>Robert:</b> There are many reasons why I chose not to get a safe job at a big IT company, but to realize my dream instead. I can reduce these to three main reasons ;)  
<ol>
<li>The perspective to create a hassle-free desktop system based on open source software is motivation enough.</li>
<li>We want to establish an open source alternative to the current operating systems and introduce a new way of computer usage, profiting from new capabilities of modern software and cloud technologies</li>
<li>Every day I learn something new at work, because we are working with the latest stuff in the open source universe (which is not as bad as many commercial vendors say). Working on open source software in your job is not a bad thing. ;)</li>
</ol>

<h2>Xompu and KDE</h2>

<b>Damien:</b> Your product combines hardware, software and services. Why did you decide to use KDE software for your desktop among the many choices that Free Software offers? By the way, how was that decision made inside your company?

<b>Robert:</b> We have been looking for a desktop environment that already integrates all daily routine applications, has an active community, is (real) Open Source Software, looks good, and is easy to customize (this was the most important point). The decision was made by testing many desktop environments that we thought would fulfill the requirements, and determining the complexity of customizing them, developing new widgets, etc.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Xompu-PC.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Xompu-PC-sml.jpg" /></a><br />The Xompu PC (click on image to enlarge)</div><b>Damien:</b> Could you explain how you intend to use KDE technologies? Will we see new products based on KDE platform or will you mainly use existing KDE software and workspaces with some customizations?

<b>Robert:</b> You will see both. Basically we are using KDE Plasma Desktop as the basic technology. We adapted the window decorations, window themes and tweaked the KDE default config settings. In addition, we started to develop new components for KDE. A few weeks ago, for example, we released our first version of a vertical panel and the cupboard widget for easy access to installed applications. We are also working on a simple and easy to use Media Player user interface.

<b>Damien:</b> Have you already been involved in any other Free Software projects? How did you discover KDE?

<b>Robert:</b> I have not been involved in any well known Open Source projects yet. However during my time at CERN, I was involved in some Open Source development projects, which were interesting to other scientists and universities.
I discovered KDE many years ago, when I installed my first Linux. This was around 1999/2000, KDE was shipped together with 7 CDs of Suse Linux 6 (or maybe it was 7). It must have been KDE version 1 or 2. I liked it and kept using it during the years.

<b>Damien:</b> How can the community help you in the future? In your opinion where do we need to improve?

<b>Robert:</b> Overall I must say KDE is a very professional project. That's why we have chosen to work with KDE. There is always something to improve and also KDE has improvable points. One for example is documentation. While there is plenty of developer API documentation, I think KDE 4 is lacking End-User and Administrator documentation. I think this is needed for the future success of KDE distribution and maybe we can contribute a thing or two.

<b>Damien:</b> And do you plan to contribute back to KDE (patch, bugs or more)?

<b>Robert:</b> Yes, of course. We already reported some bugs we found in Plasma, and we are also open to write patches. Furthermore we are planning to contribute our code into KDE source code. But for this project, we first need some support from the community to adapt our code to be suitable for the KDE project.

<b>Damien:</b> Is there anything else you would like to say to the KDE community?

<b>Robert:</b> I’m very happy that we have found in KDE such a strong and reliable partner for Xompu and I’m looking forward to our cooperation.

<h2>Join Xompu</h2>

The Xompu project is a new and innovative way to bring KDE software to end users. You can find more information on <a href="http://www.xompu.de/">their website</a> (in German). At the moment, the computer is only available in Germany, but there are plans to introduce it in other countries.

Xompu is currently composed of 13 passionate people. If you feel like joining the team and being part of the adventure, you can <a href="http://www.xompu.de/index.php?cID=93">apply on their website</a> (in German). It is a great opportunity to get paid to do some KDE development among other things!