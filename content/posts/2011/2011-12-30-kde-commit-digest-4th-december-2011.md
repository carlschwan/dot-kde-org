---
title: "KDE Commit-Digest for 4th December 2011"
date:    2011-12-30
authors:
  - "mrybczyn"
slug:    kde-commit-digest-4th-december-2011
---
In <a href="http://commit-digest.org/issues/2011-12-04/">this week's KDE Commit-Digest</a>: 

<ul><li>Embedded mode for TabBox in <a href="http://userbase.kde.org/KWin">KWin</a></li> 
<li>Reimplemented name filtering in <a href="http://dolphin.kde.org/">Dolphin</a></li> 
<li>Initial support for resource events in read-only kparts in kdelibs</li> 
<li><a href="http://www.calligra-suite.org/">Calligra</a> updates include improved opening of .kexis files, and creating new app instances and main window UI changes in <a href="http://www.kexi-project.org/">Kexi</a>; support for filled radar charts, updated document handling and optimization of reverse iterator and ODF saving</li> 
<li>Improved download management and shortcut+slot to Open Downloads page in <a href="http://userbase.kde.org/Rekonq">rekonq</a></li> 
<li><a href="http://www.kdevelop.org/">KDevelop</a> supports configuration of some environment variables for <a href="http://www.cmake.org/">CMake</a> jobs</li> 
<li>Export of non-local files in <a href="http://userbase.kde.org/Skrooge">Skrooge</a>.</li> 
</ul> 

<a href="http://commit-digest.org/issues/2011-12-04/">Read the rest of the Digest here</a>.