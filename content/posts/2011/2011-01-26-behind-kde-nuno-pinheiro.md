---
title: "Behind KDE: Nuno Pinheiro"
date:    2011-01-26
authors:
  - "jriddell"
slug:    behind-kde-nuno-pinheiro
comments:
  - subject: "New sound theme coming.."
    date: 2011-01-27
    body: "Nuno let slip that Nuno' is working on a new sound theme for KDE - I can't wait!"
    author: "Bille"
---
Oxygen designer <a href="http://www.behindkde.org/node/907">Nuno Pinheiro</a> has done an interview for People Behind KDE.  He discusses his work on KDE Platform 4.5 and how he organises (or not) his busy week, "At the end of the week I don't know what I did on Monday".  He reveals where his inspiration for artwork comes from, how to become an artist and the secret to design success, "First and most important is imagination".