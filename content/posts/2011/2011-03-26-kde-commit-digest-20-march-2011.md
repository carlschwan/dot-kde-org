---
title: "KDE Commit-Digest for 20 March 2011"
date:    2011-03-26
authors:
  - "vladislavb"
slug:    kde-commit-digest-20-march-2011
comments:
  - subject: "LongBug champion!"
    date: 2011-03-26
    body: "This week's champion is Kurt V Hindenburg for fixing bug 88867 which is more than 6 years old!"
    author: "emilsedgh"
---
In <a href="http://commit-digest.org/issues/2011-03-20/">this week's KDE Commit-Digest</a>:

<ul><li>Commands can now be entered as bookmarks in <a href="http://konsole.kde.org/">Konsole</a>, along with general bugfixing</li>
<li>Bugfixes and work throughout <a href="http://www.calligra-suite.org/">Calligra</a> including an improved freehand tool, further improvements to the importing of MS Office files, and improved support for rotated shapes</li>
<li>Start page improvements in <a href="http://userbase.kde.org/Plasmate">Plasmate</a></li>
<li>Improved Python auto-completion and lex/yacc file extensions support added in <a href="http://www.kdevelop.org/">KDevelop</a>, along with many bugfixes</li>
<li>Higher order functions now supported in <a href="http://www.kde.org/applications/education/kalgebra/">KAlgebra</a></li>
<li>Improvements in face detection in <a href="http://www.digikam.org/">Digikam</a> along with many bugfixes</li>
<li>Work throughout <a href="http://community.kde.org/Plasma/Plasma-Mobile">Plasma-Mobile</a> including an improved activity switcher and a PhoneManager application</li>
<li>Usability improvements in <a href="http://k3b.plainblack.com/">K3b</a></li>
<li>Puzzle list sorting in <a href="http://www.kde.org/applications/games/palapeli/">Palapeli</a></li>
<li>Work in <a href="http://gluon.gamingfreedom.org/">Gluon</a></li>
<li>Refactored wizard for <a href="http://community.kde.org/Solid/Projects/BlueDevil">BlueDevil</a></li>
<li>A large number of improvements to FreeBSD support in <a href="http://userbase.kde.org/KSysGuard">KSysGuard</a> along with many other bugfixes</li>
<li>Bugfixing in <a href="http://nepomuk.kde.org/">Nepomuk</a>, <a href="http://www.trinitydesktop.org/">Trinity</a>, <a href="http://userbase.kde.org/KWin">KWin</a>, <a href="http://amarok.kde.org/">Amarok</a> and <a href="http://userbase.kde.org/NetworkManagement">NetworkManager</a>.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-03-20/">Read the rest of the Digest here</a>.