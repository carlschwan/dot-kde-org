---
title: "KDE Commit-Digest for 30th January 2011"
date:    2011-02-21
authors:
  - "dannya"
slug:    kde-commit-digest-30th-january-2011
comments:
  - subject: "Optimisation and feature work in Digikam..."
    date: 2011-02-22
    body: "... not only. there are 3 new important feature in 2.0 :\r\n\r\n- Tag keyboard shortcuts support.\r\n- Color Labels support to improve photograph workfow (as Lightroom and Aperture).\r\n- Pick Labels support to improve photograph selection (as Lightroom and Aperture)\r\n\r\nScreenshots :\r\n\r\nhttp://www.flickr.com/photos/digikam/5453984332/sizes/o/in/photostream/\r\nhttp://www.flickr.com/photos/digikam/5435456951/sizes/o/in/photostream/\r\nhttp://www.flickr.com/photos/digikam/5432694899/sizes/o/in/photostream/\r\n\r\nGilles Caulier"
    author: "cauliergilles"
  - subject: "Re: comments"
    date: 2011-02-22
    body: "Gilles, thanks for pointing out!\r\n\r\nWithin the hundreds and sometimes thousands of commit messages it is sometimes difficult for us from the Commit-Digest team to identify the interesting and relevant ones. In particular, if the commit message text is very short, sounds very technical or the reviewer lacks the knowledge whether a commit is interesting / important.\r\n\r\nIf a developer wants to increase chances that a particular commit is included in the Commit-Digest he can do the following:\r\n<ul>\r\n<li>add a longer description text why the commit is interesting</li>\r\n<li>refer to a bug report / wishlist item</li>\r\n<li>reference to a screenshot</li>\r\n<li>add an identifier, e.g., \"[Digest]\" to point out that the message should be included in the Commit Digest</li>\r\n</ul>\r\n\r\nFurthermore, the above is also helpful for the editor writing the introduction.\r\n\r\n<strong>HINT</strong>: the Commit-Digest team is always happy to receive a \"featured article\" :-)\r\n\r\nAnd the Commit-Digest team always looks for : <a href=\"http://enzyme.commit-digest.org/\">fresh blood</a>!\r\n"
    author: "mkrohn5"
  - subject: ">add a longer description"
    date: 2011-02-22
    body: ">add a longer description text why the commit is interesting\r\n\r\ni try to do it...\r\n\r\n>refer to a bug report / wishlist item\r\n\r\nI always do it, I'm always work against bugzilla entries.\r\n\r\n>reference to a screenshot\r\n\r\nSometime i do it. I will post more screen-shot in the future...\r\n\r\n>add an identifier, e.g., \"[Digest]\" to point out that the message >should be included in the Commit Digest\r\n\r\nAHHH. I'm waiting something like that since a long time. Please make an official git commit tag to redirect/filter right entries. All developers must use the same tag here. \r\n\r\nBest\r\n\r\nGilles Caulier"
    author: "cauliergilles"
  - subject: "Commit template"
    date: 2011-02-22
    body: "We're implementing a standard git commit template for kde core that includes all the current tags like BUG and FEATURE as a reminder to devs.  The FEATURE tag if used consistently would be a good one to look for, but if you want a specific tag for the digest let me know and I'll add it.\r\n\r\nCheers!\r\n\r\nJohn."
    author: "odysseus"
  - subject: "Thanks!"
    date: 2011-02-22
    body: "Yup, a second thanks for pointing those out, Gilles. Sorry for missing them!\r\n\r\nVladislav Blanton"
    author: "blantonv"
  - subject: "absolutely"
    date: 2011-02-22
    body: "As a Commit Digest reviewer/editor I would be in favor of having a \"digest\" tag. Especially if developers would find it useful as well. It would certainly help ensure that particularly important commits don't get missed!\r\n\r\nVladislav"
    author: "blantonv"
  - subject: "Yes!"
    date: 2011-02-23
    body: "Hi John,\r\nYes! This is something we have been discussing for quite a while now, and its something that wouldn't be difficult to implement on my end, as long as tags are consistent.\r\n\r\nI propose that the tags always be uppercase, so in this case:\r\n\r\nDIGEST (this could be the generic tag)\r\nFEATURE (self-explanatory)\r\nBUG:99999\r\n\r\nFor bugs, this is really important: currently, people use lots of different formats like \"BUG:99999\" (good), \"Bug: 99999\", even \"bug 99999\" - you can imagine how hard it is to pull this one out of a body of text!\r\n\r\nCan we enforce one standard and shame the repeat offenders?\r\n\r\nSend me an email at danny at commit-digest.org when you've got a final proposal!\r\n\r\nCheers,\r\nDanny\r\n"
    author: "dannya"
  - subject: "Some comments about UI"
    date: 2011-02-23
    body: "Hi Gilles.\r\n\r\nThanks for you hard work in digikam, it is an amazing app for a young photographer like me :)\r\n\r\nSome comments about the UI to improve it (from my POV of course...)\r\n\r\n- There are so many buttons all arround the interface (on the right side of the \"tags\" field, on the down side of the app, etcetera. Maybe the interface could be cleaner, so it is easier to understand at a first sight\r\n\r\n- There is lost space under the \"my albums\" field and with the \"Tags\" field... maybe they can share the same area, one under the other, but with one field for both (only one scrollbar when necessary)\r\n\r\n- Every Tag in \"my tags\" has a label in the left side of the text. Is it really necessary? it clutters the UI\r\n\r\n- The left bar looks old. Amarok had a similar one, and it improved it a lot with a new method: http://amarok.kde.org/files/amarok2.4.0.png\r\n\r\n- Usually the \"search\" fields are on the top of the screen... They are easilly recognizable\r\n\r\n- The borders of the photos are very \"hard\". Maybe they can be softer if you use the color of the border for the background color of the image, and avoid all the borders\r\n\r\n- Using a grey background for the images field, and some shadows for the photos (like gwenview) would make the app also softer (see http://gwenview.sourceforge.net/screenshots.php?action=view&album=/2.4&pos=0 )\r\n\r\n- The scrollbar of the preview panel clutter the UI. Maybe something like the \"add plasmoids\" from plasma could be better (see http://gabuntu.files.wordpress.com/2010/07/plasmoids4.png )\r\n\r\n- Using \"grey / white\" colors for every line of the \"my tags\" or \"my albums\" would make the different lines easier to read (see http://amarok.kde.org/files/amarok2.4.0.png again)\r\n\r\nAgain, thank you for the work done with this amazing app.\r\n\r\nRegards from Spain,\r\n\r\nJuan Manuel"
    author: "Poldark"
  - subject: ">- There are so many buttons"
    date: 2011-02-24
    body: ">- There are so many buttons all around the interface (on the right >side of the \"tags\" field, on the down side of the app, etcetera. Maybe >the interface could be cleaner, so it is easier to understand at a >first sight\r\n\r\nThe interface is clean already. Photo work-flow is a complex stuff. Take a look to pro Lightroom, Aperture, ACDSee, photostation, application and you will see how GUI are bloated everywhere.\r\n\r\nWe trying to keep in mind this : all pro application provide complex gui. We trying to not reproduce this way in digiKam.\r\n\r\ndigiKam is not Gwenview. If Photo work-flow is too complex for you, use Gwenview... digiKam is pro photo dedicated software. It's more than Picasa, F-Spot, or Shotwell.\r\n\r\n>- There is lost space under the \"my albums\" field and with the >\"Tags\" field... maybe they can share the same area, one under the >other, but with one field for both (only one scrollbar when \r\n>necessary)\r\n\r\nWhich one exactly ? Take a shot please...\r\n\r\n>- Every Tag in \"my tags\" has a label in the left side of the text. >Is it really necessary? it clutters the UI\r\n\r\nThis can be optimized, yes...\r\n\r\n>- The left bar looks old. Amarok had a similar one, and it improved >it a lot with a new method: http://amarok.kde.org/files\r\n>/amarok2.4.0.png\r\n\r\nDefinitively, no. I'm not Amarok user, i prefer Clementine GUI. I'm completely lost with Amarok sidebar. I hate it. It's unsuitable. Sorry...\r\n\r\nCurrent digiKam sidebar is clear. You see where each main component are located in GUI !\r\n\r\n>- Usually the \"search\" fields are on the top of the screen... They >are easily recognizable\r\n\r\nNot true for all, especially in photo management program.\r\n\r\n>- The borders of the photos are very \"hard\". Maybe they can be >softer if you use the color of the border for the background color >of the image, and avoid all the borders\r\n\r\nWhich border exactly ? take a screen-shot please....\r\n\r\n>- Using a grey background for the images field, and some shadows for >the photos (like gwenview) would make the app also softer (see >http://gwenview.sourceforge.net/screenshots.php?action=view&album=\r\n>/2.4&p... )\r\n\r\ndigiKam use same border code than gwenview for icon view...\r\nAbout color frame, it's not agree, since digiKam support Color Labels, which add colored frame around icon view item...\r\n\r\n>- The scrollbar of the preview panel clutter the UI. Maybe something >like the \"add plasmoids\" from plasma could be better (see >http://gabuntu.files.wordpress.com/2010/07/plasmoids4.png )\r\n\r\nIt's fixed with 2.0.0, using model/view.\r\n\r\n>- Using \"grey / white\" colors for every line of the \"my tags\" or \"my >albums\" would make the different lines easier to read (see >http://amarok.kde.org/files/amarok2.4.0.png again)\r\n\r\nDefinitively no. It's tree-view not a list view. Look like Dolphin in folder tree view do not use it. This bloat GUI, i tested it...\r\n\r\nBest\r\n\r\nGilles Caulier\r\n"
    author: "cauliergilles"
---

In <a href="http://commit-digest.org/issues/2011-01-30/">the latest KDE Commit-Digest</a>:

              <ul><li>Work on the CSV importer in <a href="http://www.kmymoney2.sourceforge.net">KMyMoney</a></li>
<li>Work across the board in <a href="http://www.calligra-suite.org/">Calligra</a>, including improved PPT format support</li>
<li>Work on video and sending/recieving files with the Yahoo protocol in <a href="http://kopete.kde.org/">Kopete</a> amongst other changes</li>
<li>Work on search history support in <a href="http://www.kdevelop.org/">KDevPlatform</a></li>
<li>New Booksmarks Manager (including support for importing bookmarks) in <a href="http://edu.kde.org/marble/">Marble</a>, amongst many bug fixes</li>
<li>Much work throughout <a href="http://kst-plot.kde.org/">Kst</a> including optimisations</li>
<li><a href="http://api.kde.org/4.0-api/kdelibs-apidocs/kio/html/classKFileDialog.html">KFileDialog</a> now automatically chooses file type from typed extension</li>
<li>Work on the <a href="http://kde-look.org/content/show.php/PublicTransport?content=106175">Public Transport</a> Plasma data engine</li>
<li>Improvements in ad blocking code in KWebkitPart</li>
<li>Features and bugfixing in <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://vizzzion.org/blog/2010/09/getting-email-done-the-stack-and-the-heap-of-lion-mail/">Lionmail</a>, and <a href="http://kde-apps.org/content/show.php/Knights?content=122046">Knights</a></li>
<li>Fixes for header-protected RAR and 7z files in <a href="http://utils.kde.org/projects/ark/">Ark</a></li>
<li>Audio streaming support in <a href="http://gluon.gamingfreedom.org/">Gluon</a></li>
<li>Optimisation and feature work in <a href="http://www.digikam.org/">Digikam</a></li>
<li>Optimisations in the <a href="http://vizzzion.org/blog/2011/01/slicing-up-the-web/">Webslice Plasma widget</a></li>
<li>All launcher config reading/saving has been moved into the new Group Manager</li>
<li>KDE-SDK's <a href="http://kdesrc-build.kde.org/">Kdesrc-Build</a> moves to Git</li>
<li><a href="http://community.kde.org/KDE_PIM">KDE-PIM</a> libs move to Git</li>
<li>Fsview is now Qt3Support-free</li>
<li>Bugfixes in <a href="http://gwenview.sourceforge.net/">Gwenview</a>, <a href="http://konsole.kde.org/">Konsole</a>, <a href="http://games.kde.org/game.php?game=kpat">KPat</a>, <a href="http://rekonq.kde.org">Rekonq</a>, <a href="http://jontheechidna.wordpress.com/">Muon</a>, <a href="http://kde-look.org/content/show.php/?content=136216">Oxygen-Gtk</a> and throughout <a href="http://plasma.kde.org/">Plasma</a> and its addons.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-01-30/">Read the rest of the Digest here</a>.