---
title: "KDE Commit-Digest for 11th September 2011"
date:    2011-09-17
authors:
  - "mrybczyn"
slug:    kde-commit-digest-11th-september-2011
comments:
  - subject: "TZ"
    date: 2011-09-17
    body: ">now supports PPoE\r\n\r\nMaybe PP<strong>P</strong>oE?"
    author: ""
  - subject: "Fixed, thanks!\nDanny"
    date: 2011-09-17
    body: "Fixed, thanks!\r\n\r\nDanny"
    author: "dannya"
  - subject: "Lilian"
    date: 2011-09-18
    body: "Wow, \"Network Management now supports PPPoE\", that's the only thing that was holding the students from my campus to move to Linux... \r\n\r\nPretty late but better later than never..."
    author: ""
  - subject: "Lilian"
    date: 2011-09-18
    body: "Wait a sec, is it able to make a PPPoE connection over Wireless?"
    author: ""
  - subject: "not sure"
    date: 2011-09-19
    body: "Being the one who wrote about PPPoE in the summary I have to admit it wasn\u00b4t exactly clear to me, but for specifics I'd like to point to the bug report: http://bugs.kde.org/show_bug.cgi?id=204170"
    author: ""
  - subject: "No possible yet because we"
    date: 2011-09-20
    body: "No possible yet because we need to filter which kind of connection can be associated to each kind of network interface. Until my commit only serial interfaces could be associated to DSL connections. Adding wifi is as easy as adding a line in the source code, but I need to know for sure all the possible combinations to prevent problems.\r\n\r\nI do not have access to any DSL connection, that is why the change took to long. I need people to test and report the problems."
    author: ""
  - subject: "Solutions"
    date: 2011-09-22
    body: "If it helps, there are scripts for PPPoE over Wireless.\r\nI write a command, in\r\n(K)Ubuntu: \"pppoeconf\"\r\nFedora: \"pppoe-setup\" for configuration and then \"pppoe-start\" to use it"
    author: ""
  - subject: "Currently I am with Mageia"
    date: 2011-09-22
    body: "Currently I am with Mageia (Cauldron, the future Mageia 2) My system is in French. I have Skrooge 0.9.93 and it is impossible to import a .gsb file contrary to what you announce. In the window and in the drop-down menu every other formats are proposed but NOT the Grisbi one. And when I try to write the path to the file and click on Open, I get an error:\r\n\r\n[ERR-5]: L'importation du fichier \u00ab /home/fred/Documents/Comptabilit\u00e9/Jacques.gsb \u00bb a \u00e9chou\u00e9\r\n\r\nthen at the following screen if I click on \"Historique\":\r\n\r\n[ERR-5]: L'importation du fichier \u00ab /home/fred/Documents/Comptabilit\u00e9/Jacques.gsb \u00bb a \u00e9chou\u00e9\r\n[ERR-8]: Contenu XML non valable dans le fichier \u00ab /home/fred/Documents/Comptabilit\u00e9/Jacques.gsb \u00bb\r\n[ERR-4]: Mauvaise version de document Grisbi. La version doit \u00eatre >= 0.6.0\r\n\r\nNo way to import a Grisbi file finally? Or a bug? Thanks.\r\n\r\n\r\n<a rel=\"dofollow\" href=\"http://investmentguidance.us/\">free investment newsletter</a>"
    author: "kevinkentt"
  - subject: "PPPOE has worked for a long time"
    date: 2011-10-02
    body: "???\r\nI used PPPOE to connect to internet more than ten years ago, it required a little configuration (at the time, I think it was done running adsl-setup or so) but once done it was rock solid."
    author: ""
---
<p>In <a href="http://commit-digest.org/issues/2011-09-11/">this week's KDE  Commit-Digest</a>:</p>

<ul>
<li><a href="http://www.kexi-project.org/">Kexi</a> receives many new features in the upcoming Calligra 2.4 release such as Kexi Mobile, AutoForms and a new <a  
href="http://community.kde.org/Calligra/Usability_and_UX/Common/Startup/Startup_view_integrated_with_the_File_menu">startup view</a> which is integrated with the file menu</li>
<li>The upcoming release of Kexi will also feature the <a  href="http://blogs.kde.org/node/4448">web form widget</a>, bringing the <a href="http://shreyapandit.com/?p=50">work</a> of Season of KDE student Shreya Pandit upstream</li>
<li>Many performance optimizations are made to <a href="http://kdevelop.org/">KDevelop</a> and the KDevPlatform</li>
<li><a href="http://userbase.kde.org/NetworkManagement">Network Management</a> now supports PPPoE</li>
<li><a href="http://salout.github.com/blog/2010/12/18/announcing_kubeplayer.html">Kubeplayer</a> works on Plasma Active now</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> gets a GPS speed render plugin to provide information on speed, direction, altitude and precision</li>
<li><a href="http://www.k3b.org/">K3b</a>'s settings located in System Settings are moved to K3b itself</li>
<li>The <a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a> applet gets support for importing the <a href="http://en.wikipedia.org/wiki/General_Transit_Feed_Specification">General Transit Feed Specification</a> (GTFS)</li>
<li>The <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy</a> Contact list gains support for basic drag and drop</li>
<li>Image title support is added to <a href="http://www.digikam.org/">Digikam</a></li>
<li>Replace in files function is available in <a href="http://kate-editor.org/">Kate</a></li>
<li>Bugfixes to Calligra, Network Management and KDE PIM</li>
</ul>

<p>Read the <a href="http://commit-digest.org/issues/2011-09-11/">rest of the Digest</a>.</p>
<!--break-->
