---
title: "Google Summer of Code & Season of KDE"
date:    2011-09-10
authors:
  - "kallecarl"
slug:    google-summer-code-season-kde
---
How awesome are the Google Summer of Code (GSoC) and Season of KDE (SoK) programs? According to Daniel Moctezuma, "All the work done by students in GSoC/SoK will have an impact in Free Software and the world." Daniel is one of the 2011 GSoC students. Lydia Pintscher is the main administrator of these programs for KDE. She made the following report.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/GSoC2011_198x128.png" /></div>

"KDE was once again chosen to take part in Google Summer of Code (GSoC) as a mentoring organization. Thanks to Google's generous funding and KDE's mentors, we were able to work with 51 students over the summer, making KDE the biggest participant in GSoC. Choosing students for the program was hard, but the selection turned out well. The students coded in nearly all areas of KDE from Calligra and Rekonq to Amarok and KStars. Their projects turned out fantastic. As in previous years, we're impressed with the talent and dedication of the students. All 51 students passed the mid-term evaluation and 47 were successful in the final evaluation. (One stopped GSoC after a successful mid-term evaluation because of a job.) Their work will be visible in upcoming releases.

As usual, KDE got more great applications for GSoC than we were able to fund. So we ran Season of KDE (SoK) again to welcome the remaining students into the KDE community, and to give them meaningful projects, mentoring, support and recognition. SoK is a program similar to GSoC where students get a certificate and limited-edition t-shirt for completing their project successfully. The response was overwhelming this year and we had to close applications after 100 submissions. Nearly everyone was matched up with a mentor and a project to work on. The students still have a few more days to work on their projects, and the results are already looking fantastic.

It makes me proud that KDE as a community is able and willing to teach newcomers to Free Software on such a large scale, while delivering high-quality results in terms of code produced and students mentored. What makes me even more proud is the overwhelming success of Season of KDE even without the GSoC monetary incentive, just because people want to work on something amazing."
<br />
<hr />

A wrap-up report with more details will be published soon. Thank you, Lydia and the other admins and mentors, for your dedication to the development of other people. To GSoC and SoK students...thank you for your contributions that inspire the KDE community and that will be valuable to millions of KDE users worldwide.