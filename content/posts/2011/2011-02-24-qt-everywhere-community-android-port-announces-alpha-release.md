---
title: "Qt Everywhere: Community Android Port Announces Alpha Release"
date:    2011-02-24
authors:
  - "Stuart Jarvis"
slug:    qt-everywhere-community-android-port-announces-alpha-release
comments:
  - subject: "Great!"
    date: 2011-02-24
    body: "if $kde-apps AND android TRUE\r\nTHEN ^_^\r\nELSE : (\r\n\r\nWay to go, plenty of applications from kde would rock on android, kmail, korganizer, akregator, okular, etc - and have a bigger audience then on meego. \r\nI sincerely believe that a lot of kde users would love to have syncing in place with native kde-apps on their mobile - and make plenty use of the donate button in the app store.\r\n\r\nThanks to the Necessitas to bring that dream a little closer : )"
    author: "thequickbrownfox"
  - subject: "Finally a decent framework"
    date: 2011-02-24
    body: "Finally a decent framework for making android apps.\r\n\r\nWill it allow developers to make native applications? or they will still somehow rely on the android dalvik java VM?\r\n\r\nKeep up the good work guys :)"
    author: "fduraibi"
  - subject: "All the answers in BehindKDE soon"
    date: 2011-02-24
    body: "I have interviewed the lead developer for BehindKDE (Platforms series). I still need to edit, spellcheck, etc but you can expect all the answers by Monday :-)\r\n"
    author: "pgquiles"
  - subject: "Eh... Ok i don't get the difference... "
    date: 2011-02-25
    body: "So what is the difference between this Android port and the already existent port <a href=\"http://code.google.com/p/android-lighthouse/\">Android Lighthouse</a>? \r\n\r\nI think maintaining 2 different community ports is a waste of time and I would hope to see them consolidate their work or just merge the projects. \r\n\r\n- Andrew - <a href=\"http://www.graticule.com\">Graticule Software Engineer</a>"
    author: "andrewm"
  - subject: "Might be the same project"
    date: 2011-02-25
    body: "I'm not sure, but they may well be the same thing (name change? release codename?), or at least significantly overlapped. Notably, the android lighthouse wiki page on preparing and compiling Qt <a href=\"http://code.google.com/p/android-lighthouse/wiki/Compile\">here</a> now directs people to the equivalent necessitas wiki page, and also, less significantly but more interestingly, the <a href=\"http://sourceforge.net/p/necessitas/home/\">necessitas</a> home page says \"Welcome on the home of the android lighthouse project.\" at the top.\r\n\r\nThey both use the same logo too :)"
    author: "moofang"
  - subject: "Cant wait to see them!\nI hope"
    date: 2011-02-26
    body: "Cant wait to see them!\r\nI hope this guys have the answer..\r\nEduardo - <a href=\"http://www.bolsadeofertas.com.br\">Compra coletiva</a> "
    author: "retrovisor"
  - subject: "Yes it is the same project"
    date: 2011-02-28
    body: "Yes it is the same project just with a name that won't run the risk of a trademark dispute with Nokia."
    author: "odysseus"
  - subject: "You write native c++ code and"
    date: 2011-02-28
    body: "You write native c++ code and there's a thin js shim to fool android into running the native code. Qt uses the NDK and not dalvik, as can the app developer."
    author: "odysseus"
  - subject: "Ready! Go!"
    date: 2011-02-28
    body: "Interview is now live: http://www.behindkde.org/node/925"
    author: "pgquiles"
  - subject: "Java, not JavaScript"
    date: 2011-03-01
    body: "\u2026"
    author: "The User"
---

The <a href="http://sourceforge.net/p/necessitas/wiki/Home/">Necessitas</a> team, led by Bogdan Vatra, is pleased to announce the first alpha release</a> of  Necessitas, a Qt SDK for the <a href="http://www.android.com/">Android</a> mobile platform.

The release contains the following:
<ul>
<li>Ministro, a system-wide Qt libraries installer</li>
<li>Qt framework, precompiled for Android (only in a Linux 32-bit version)</li>
<li>Qt Creator for Android - to create, manage, compile, deploy and debug your software</li>
</ul>

Although these make extensive use of free software from <a href="http://qt.nokia.com/">Nokia Qt Development Frameworks</a> and <a href="http://www.google.com/">Google</a>, this is a community project and is not supported or endorsed by either Nokia or Google at present. The alpha release of Necessitas is exciting for KDE as support for Qt is a prerequisite to get KDE software running on a wide range of mobile platforms. The project also underlines Qt's value in community-led, cross platform applications, which KDE strongly supports. 

As an alpha release, this is not yet ready for production use and there are known issues and <a href="http://sourceforge.net/p/necessitas/wiki/Todo">features not yet implemented</a>. The development team welcomes contributions to improve the software and <a href="http://groups.google.com/group/android-qt/">input from potential contributors</a>.