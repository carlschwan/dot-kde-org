---
title: "Plasma Active on NVidia Tegra 2"
date:    2011-10-24
authors:
  - "sebas"
slug:    plasma-active-nvidia-tegra-2
comments:
  - subject: "Video"
    date: 2011-10-24
    body: "http://www.youtube.com/watch?v=L5-setgsIfY&feature=channel_video_title"
    author: ""
  - subject: "Touchpad as well?"
    date: 2011-10-24
    body: "Does this make it any easier to run Plasma Active on an HP Touchpad"
    author: ""
  - subject: "I too have a TouchPad and I'd"
    date: 2011-10-24
    body: "I too have a TouchPad and I'd love to be able to run Plasma Active on it. It is already possible to run an Xserver and get apps via an Ubuntu chroot. The CPU in the TouchPad is a dual-core ARM CPU from Qualcomm, so it's not the same as a Tegra but I see no reasons Plasma Active couldn't run on the Touchpad, it would just take a lot of work. I've been trying to figure out how to do it , but I don't really have quite enough expertise. The biggest difficulty would be getting GPU acceleration so that t will be smooth and not ugly. The ideal thing to do would be to install a complete distro and not just run on top of webOS. It may be possible to leverage some of the Cyanogenmod work. Maybe getting Mer ported to the Touchpad would be a good start."
    author: ""
  - subject: "Video"
    date: 2011-10-25
    body: "http://www.youtube.com/watch?v=L5-setgsIfY"
    author: ""
  - subject: "Another Video"
    date: 2011-10-25
    body: "http://www.youtube.com/watch?v=bEjLMYXxN9Y"
    author: ""
  - subject: "another video"
    date: 2011-10-25
    body: "http://www.youtube.com/watch?v=bEjLMYXxN9Y"
    author: ""
  - subject: "wow"
    date: 2011-10-25
    body: "It works incredibly smooth, sweet!"
    author: "jospoortvliet"
  - subject: "Just in time for the winter."
    date: 2011-10-26
    body: "Goodbye cold pockets!"
    author: ""
  - subject: "Buy"
    date: 2011-10-27
    body: "Any chance one will be able to buy a Tegra 2 / Tegra 3 device with Plasma Active preinstalled? basysKom?"
    author: ""
  - subject: "Hoping for Plasma Active on Notion Ink Adam 2"
    date: 2011-10-27
    body: "Am waiting for Notion Inks \"Adam 2\" (which is said to be unveiled by the end of the year), because it\u00b4ll probably feature the improved Pixel Qi Display.\r\nBut I don\u00b4t like Android - so it\u00b4d be insanely great if I would be able to install Mer or Balsam plus Plasma Active on it!"
    author: ""
  - subject: "PPA"
    date: 2012-02-13
    body: "I haven't gotten it running yet on mine but there's a PPA with Plasma active for Ubuntu:\r\n\r\nhttps://launchpad.net/~kubuntu-active/+archive/ppa"
    author: ""
  - subject: "Folio 100"
    date: 2012-06-17
    body: "This could be great news... Hope it will be possible to run on a Toshiba Folio 100 as well. Vegacomb served as a base for previous Folio-roms as well...\r\n\r\nIf anyone here could confirm that flashing the kernel found on http://share.basyskom.com/contour/Deployment/mer_arm_install_archive/point_of_view_nvidia_tegra2/\r\n\r\nI'd be willing to try booting Mer/Plasma\r\n\r\nRegards,\r\nBas"
    author: ""
  - subject: "suggest posting to KDE forum"
    date: 2012-06-18
    body: "http://forum.kde.org/viewforum.php?f=211.\r\n\r\nThis kind of subject gets more exposure there."
    author: "kallecarl"
---
In a cooperative effort, the <a href="http://merproject.org/">Mer</a> team and the <a href="http://www.basyskom.com/">basysKom</a> integrators have succeeded in booting a Plasma Active image on <a href="http://en.wikipedia.org/wiki/Nvidia_Tegra#Tegra_2_series">NVidia Tegra 2</a> devices, opening the door bringing Plasma Active to a wider range of hardware. The image is based on Mer, a successor to the MeeGo operating system.

The <a href="http://www.plasma-active.org">Plasma Active</a>, <a href="http://www.merproject.org">Mer</a> and <a href="http://www.basyskom.com">basysKom</a> teams have been working together on this new hardware platform, with the project led by Martin Brook (vgrade on IRC). Last weekend, they successfully booted a non-optimized version of Plasma Active on two devices powered by NVidia's Tegra 2 (TAB-TEG-10-1-4GB-3G, Advent Vega). Tegra 2 is a powerful platform for mobile hardware, featuring a dual-core ARM Cortex-A9 CPU and Ultra Low Power (ULP) GeForce GPU with 8-core GeForce GPU with 4 cores dedicated to running pixel shaders and 4 cores dedicated to running vertex shaders. The powerful, yet energy-efficient graphics make it very interesting hardware for running Plasma Active.

Brook mentions, "Now that there is initial success running on Tegra 2 devices, the team wants to get their hands on a Tegra 3 device, which will soon hit the market." The Tegra 3 comes with a quad-core CPU and a low power companion CPU that can be used to "run" the device in standby and extend the life of a battery charge. The Tegra 3 chip is advertised to be about 5 times faster than the Tegra 2 on certain workloads.

The team is stabilizing, fixing up and optimizing the Tegra 2 images now, and is planning to make them available for wider testing soon. A demo of a Tegra 2 device running Plasma Active will be shown by <a href="http://www.basyskom.com">basysKom</a> at Qt DevDays in Munich from 24th-26th of Oktober. Please drop by the booth to see the showcase.