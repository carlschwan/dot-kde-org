---
title: "Akademy 2012 in Tallinn, Estonia"
date:    2011-11-24
authors:
  - "sealne"
slug:    akademy-2012-tallinn-estonia
comments:
  - subject: "Nice"
    date: 2011-11-25
    body: "I travelled to Tallinn after the Akademy in Tampere and loved the place, but then I'm a history geek too :-)  Looking forward to the return trip!"
    author: ""
  - subject: "I am also looking forward to"
    date: 2011-11-30
    body: "I am also looking forward to Akedemy. If I still work at the same place as I do now there is a high probability that I can go  in my working hours and he will pay my trip again"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/logo_akademy_wee.png" /></div>

<p>The KDE community is proud to announce that next year's <a href="http://akademy2012.kde.org/">Akademy</a> will be held in Tallinn, Estonia from the 30th of June to the 6th of July. Akademy is the annual world conference held by the KDE community to celebrate the Free Software desktop and work towards the future of KDE.</p>

<p>Akademy 2011 in Berlin ended a few months ago. It was part of the <a href="https://desktopsummit.org/">Desktop Summit</a>, which was a great success. Next year's Akademy awaits us in Tallinn, the capital of Estonia.</p>
<!--break-->
<p>As always, major support from the local team will be the center of the organization for the conference. Claudia Rauch, the business manager of KDE e.V. in Berlin, is the overall project manager, and a small production team will assist. There's a lot that needs to happen for Akademy to go smoothly.</p>

<p>Laur Mõtus, who proposed Tallinn as the site for Akademy 2012, speaks about his motivations for being the local coordinator, and the Free Software community in Tallinn.</p>

<p><strong>Inu:</strong> <em>Hi Laur, thanks for your time for this interview and for being a local coordinator for the Akademy 2012! First of all, would you introduce yourself—who you are, what do you do, how you are involved with KDE?</em></p>
 
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/laur_crop_wee.jpg" /><br />Laur Mõtus</div>

<p><strong>Laur:</strong> <em>Hi, I'm 26 year old guy from Estonia, one of the members of Estonian LUG, intrigued by Linux since 28 April 1999, non-stop KDE user since autumn 2004. My daily work is as a network administrator with a touch of project management. Currently, my biggest contribution to FLOSS is being an internship coordinator for Free Software translations, manuals, also some administrative and programming jobs.</em></p>

<p><em>I am a representative of the Board of ALVATAL (Estonian Open Source and Free (Libre) Software Union), a young and enthusiastic team taking care of the local organization of Akademy 2012 in Tallinn. In the past, I have co-founded and contributed to two FLOSS projects—Estobuntu, an Estonian Linux distribution and Manpremo, a Python/Django based computer remote management system. For years, I have also done translation work for various FLOSS projects, but for last couple of years my focus has shifted more towards getting more contributors involved with FLOSS.</em></p>
 
<p><strong>Inu:</strong> <em>What motivated you to become the host of the Akademy 2012?</em></p>
 
<p><strong>Laur:</strong> <em>My first Akademy was Akademy 2007 in Glasgow. At the time of my arrival, there was an airport delay and I missed the last transport to the city. Passport Control nearly did not let me into the UK because i had grown big hair and big beard compared to my passport picture. Everything seemed ruined. Almost ready to spend a night on the airport floor, I got lucky that a friendly (also stranded) student was being picked up by his friend and so I still got to the city. </em></p>

<p><em>Upon entering the hotel in the center there were a couple of KDE contributors talking in the lobby, no hotel employees though. I must have looked a bit like a stranded pet because out of the blue I got a hug from Aaron Seigo (whom I had never met or talked with before) and the KDE guys-gals tracked down a hotel employee so that I could get a room. From there on, it was like a dream. The whole week we spent in Glasgow I felt like in geek heaven. The people were awesome. Everywhere you went you could see something cool being prototyped, something no one had ever done.</em></p>

<p><em>Akademy 2010 in Tampere was when the personal dream to organize Akademy got more realistic.The Tampere team has a helpful attitude about sharing their experience and their good contacts, which is really great. So, you can say that Akademy 2012 ignited from personal experience with Akademy 2007 and good friends from Akademy 2010.</em></p>
 
<p><strong>Inu:</strong> <em>Can you give us a bit of information Tallinn, Estonia and the University?</em></p>
 
<p><strong>Laur:</strong> <em>Tallinn is the capital of Estonia, in the north-eastern corner of Europe. Located on the shore of the Baltic Sea, its biggest neighboring cities include Helsinki, Stockholm and St. Petersburg. Tallinn is special in that after prosperous medieval times as a Hanseatic League town, several wars and different rulers put a slow pace to city development. What was bad in those days is good nowadays since this is why Tallinn has such a well preserved old town. Tallinn is the Culture Capital of Europe for 2011.</em></p>

<p><em>Akademy 2012 takes place in cooperation with the Estonian Information Technology College (University of Applied Sciences) at the Tehnopol Science and Business Park. Tehnopol consists of two universities and more than 150 companies. The premises offer plenty of floorspace and several open-hacking rooms to welcome all attendees in the summer of 2012!</em></p>
 
<p><strong>Inu:</strong> <em>How big is the KDE community in Tallinn? What is the awareness of Free Software? And do you expect hosting Akademy will bring about changes?</em></p>
 
<p><strong>Laur:</strong> <em>Estonia is small in general—only 1.34 million inhabitants—so no big numbers to hope for here. Good thing to know for KDE people is that the Linux distribution most installed in schools—Estobuntu—defaults to KDE. So you have many small users all over the country. But not just school kids use Estobuntu. Grandparents in cottages in the middle of big forests use it too, because it is in Estonian language and doesn't cost anything.</em></p>

<p><em>Historically, Estonia is not a very FLOSS-rich country, but recent years have seen some improvements. For example our new version of national ID-card software is FLOSS, cross platform and written in Qt.</em></p>

<p><em>By hosting Akademy, we aim to increase local awareness about Free Software. The situation right now is that a lot of people use it at least in some way, but it is not a choice made knowingly. It is because their IT-support installed it or because they found something over the Internet that is free (in cost) not because they actually understand the main principles of
FLOSS.</em></p>

<p><em>We also want to integrate and enlarge our local FLOSS activist groups by challenging all of us with this priceless possibility of organizing Akademy 2012 in Estonia.</em></p>
 
<p><strong>Inu:</strong> <em>Thank you very much, Laur, for your time and wish you all the best.</em></p>
 
<p><strong>Laur:</strong> <em>Thank you, KDE, for giving us this opportunity to host Akademy 2012. We will do our best to give KDE an awesome Akademy.</em></p>

<p>Within the next few months, more details will be announced about the schedule. There will be calls for papers, volunteers and participation. Please stay with us and keep yourself updated.</p>

<p>For those who are interested getting to know more about Tallinn, please visit <a href="http://www.tallinn.info/flash/">Virtual Tour in Tallinn</a> or <a href="http://www.tourism.tallinn.ee/eng">Tourism Tallinn</a>.</p>