---
title: "KWin Embraces New Platforms with OpenGL ES 2.0 Support"
date:    2011-02-18
authors:
  - "mgraesslin"
slug:    kwin-embraces-new-platforms-opengl-es-20-support
comments:
  - subject: "Good work"
    date: 2011-02-18
    body: "This is fantastic. It's great to see KWin remaining at the front of new technologies. I was also impressed to see it running with nouveau - the only thing holding me back from switching over to nouveau was its compatibility with KWin."
    author: "milliams"
  - subject: "Not for you, Nokia!"
    date: 2011-02-18
    body: "Sorry, but made up your mind !"
    author: "yglodt"
  - subject: "OpenGL 2 based compositor"
    date: 2011-02-19
    body: "<cite>It also provides the basis for a new OpenGL 2-based compositor</cite>\r\n\r\nWhen will this be available? 4.7?"
    author: "jadrian"
  - subject: "It is already the default in"
    date: 2011-02-19
    body: "It is already the default in master, so yes with 4.7."
    author: "mgraesslin"
  - subject: "I tried Nouveau a couple of"
    date: 2011-02-19
    body: "I tried Nouveau a couple of months ago and it still has some stability problems. "
    author: "trixl."
  - subject: "KDE on my N900 "
    date: 2011-02-20
    body: "Great job guys, I hope you have the drive and resources to make KDE Mobile a real thing. What I really want is to have KDE and most of its apps running on my smartphone - I don't want any exotic and funny GUIs - just my perfect KDE... I hope one not so distant day this wish of mine would come true. \r\n\r\n"
    author: "abombov"
---
Over the last few months the KWin development team worked on bringing the Window Manager for KDE's Plasma workspaces to mobile devices. This has required porting the compositing code to <a href="http://www.khronos.org/opengles/">OpenGL ES 2.0</a>, the open graphics API for programmable embedded graphics hardware. With the migration of KWin's codebase to git, the code was imported into the master development tree to be part of the next release of the KDE Platform.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 480px;"><embed src="http://blip.tv/play/AYKjzWAC" type="application/x-shockwave-flash" width="480" height="300" allowscriptaccess="always" allowfullscreen="true"></embed><br />KWin running on mobile devices (<a href="http://blip.tv/file/get/Swjarvis-KWinOpenGLES20Support641.ogv">Ogg Theora version</a>)</div>The import of the OpenGL ES 2.0 code base marks an important milestone in developing a Plasma Workspace for mobile devices. Providing a mobile shell also requires us to deliver a fast, feature rich and mature compositing window manager. With KWin, KDE can deliver a proven and mature compositor for devices like the Nokia N900 and upcoming tablet devices (in the video to the right, KWin is running on an early Intel-powered tablet and a Nokia N900). KWin is the first major X11 window manager to offer these capabilities.

<h2>Desktop Improvements</h2>

The work on OpenGL ES not only brings improvements for the use of KDE Plasma on mobile devices, but also significant performance improvements to all users of the Plasma Workspaces. The code written for the mobile platform is reused in KWin for the Plasma Desktop and Plasma Netbook Workspaces. It also provides the basis for a new OpenGL 2-based compositor while keeping the existing codebase as a fallback for legacy graphics cards not supporting OpenGL 2.

KWin is therefore the first major window manager to make full use of the capabilities provided by OpenGL 2 without leaving users of older hardware behind. KWin is the only window manager to support a non-composited mode, as well as OpenGL ES 2.0, OpenGL 2, OpenGL 1 and XRender for compositing. Thus, the Plasma Workspaces provide the best possible user experience even with no hardware acceleration. At the same time, KWin delivers the best compositing experience to users of modern hardware, providing smooth effects and improved visual effects such as the gaussian blur filter for translucent Plasma elements.

By implementing an OpenGL 2 compositor, the complete effect framework received performance improvements; more are still to come. Various parts of the OpenGL stack were abstracted to better support both OpenGL 1 and OpenGL 2 in the effects. This eases future development and maintenance of the code base. Improvements to the underlying rendering stack will be available immediately for all effects.

<h2>Future Developments and Free Drivers</h2><div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 480px;"><embed src="http://blip.tv/play/AYKZkE4C type="application/x-shockwave-flash" width="480" height="300" allowscriptaccess="always" allowfullscreen="true"></embed><br />KWin running with nouveau (<a href="http://blip.tv/file/get/Mgraesslin-KWinGLES771.ogv">Ogg Theora version</a>)</div>The availability of an OpenGL ES 2.0 compositing backend also marks the beginning of further challenging development efforts to improve the user experience of advanced graphics in the complete free software stack. Support for OpenGL ES 2.0 is a requirement for porting the KDE Plasma Workspaces to the <a href="http://wayland.freedesktop.org/">Wayland</a> infrastructure that may be a long term successor to the X Server. By porting to OpenGL ES, the KDE development team has a good starting point on the long road to Wayland.

In this regard, it is important to note that the OpenGL ES port has been developed and tested on regular NVIDIA desktop hardware using the free <a href="http://nouveau.freedesktop.org/">nouveau</a> drivers provided by the Mesa development team. The KWin developers are thankful for the efforts of the free driver developers for delivering OpenGL ES drivers alongside regular desktop OpenGL drivers. It is pleasing to see that the free drivers have reached a stage where they can be used as the base for the development of a new compositing backend. As seen in the video to the right, vast performance improvements can be delivered with the new compositing backend. Altogether the OpenGL ES port has had more than 120 single commits changing more than 80 source files.