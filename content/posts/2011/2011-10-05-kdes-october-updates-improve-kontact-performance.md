---
title: "KDE's October Updates Improve Kontact Performance"
date:    2011-10-05
authors:
  - "sebas"
slug:    kdes-october-updates-improve-kontact-performance
comments:
  - subject: "Changelog?"
    date: 2011-10-05
    body: "Is there a changelog available?"
    author: ""
  - subject: "Found them myself"
    date: 2011-10-05
    body: "http://www.kde.org/announcements/changelogs/changelog4_7_1to4_7_2.php\r\n\r\nhttps://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.7.2&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0="
    author: ""
  - subject: "Kontact and Nepomuk..."
    date: 2011-10-06
    body: "...work so much better now. A huge thank you to the PIMsters, and especially Sebastian Tr\u00fcg, who has been squashing Nepomuk bugs like mad despite the circumstances. You're all heroes. :)"
    author: ""
  - subject: "The announcment says \"September 7th\"... ;-)"
    date: 2011-10-06
    body: "no text"
    author: ""
  - subject: "fixed,"
    date: 2011-10-06
    body: "Albert has kindly fixed it. Thanks for the note."
    author: ""
  - subject: "thanks :D"
    date: 2011-10-06
    body: "Yeah, the bug list is actually quite informative, the change log is almost empty. You can also look through commit digests (commit-digest.org) if you want to know if your pet bug is fixed :D"
    author: "jospoortvliet"
  - subject: "While I like the updates,"
    date: 2011-10-11
    body: "While I like the updates, this doesn't feel user ready to me.\r\nThe Kontact migration is flaky, KMail can't delete mail correctly (https://bugs.kde.org/show_bug.cgi?id=282681) nor count folder contents or reliably mark them as read (https://bugs.kde.org/show_bug.cgi?id=283272). Ktorrent's UPNP support is also totally busted (https://bugs.kde.org/show_bug.cgi?id=283335)."
    author: "Tom Chiverton"
---
Today KDE <a href="http://kde.org/announcements/announce-4.7.2.php">released</a> updates for its Workspaces, Applications, and Development Platform. These updates are the second in a series of monthly stabilization updates to the 4.7 series. 4.7.2 updates bring many bugfixes and translation updates on top of the latest edition in the 4.7 series and are recommended updates for everyone running 4.7.0 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come. The October updates are especially interesting for those using the new Akonadi-based Kontact Suite, as it contains many performance improvements and bugfixes for applications such as KMail, and others retrieving information using Akonadi.
<!--break-->
