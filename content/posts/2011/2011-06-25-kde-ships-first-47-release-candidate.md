---
title: "KDE Ships First 4.7 Release Candidate"
date:    2011-06-25
authors:
  - "sebas"
slug:    kde-ships-first-47-release-candidate
comments:
  - subject: "I hope they fixed the nasty"
    date: 2011-06-25
    body: "I hope they fixed the nasty resizing problem with the panels and removed the silly warning telling me that strigi is disabled and I'm missing wonderful features. "
    author: "trixl."
  - subject: "More information would be helpful"
    date: 2011-06-25
    body: "There are several bugs at \r\nhttps://bugs.kde.org/buglist.cgi?quicksearch=panel+resizing\r\nOnly 2 have votes:\r\n- Bug 226848 - Resizing maximized windows horinzontally is broken if an upper panel exists (40 votes)\r\n- Bug 276130 - information panel has wrong contents size proportion in resizing (20 votes)\r\nWhich panel resizing problem are you referring to? Did you vote for any of the bugs? Have you found or reported bugs that are not listed in the bugs.kde.org search above?\r\n\r\nRegarding Strigi...\r\nIf this warning bothers you, what have you done to address the issue? In response to comments you've made elsewhere on dot.kde.org, this question does not mean that you should shut up or fix it yourself. Hoping is not an effective strategy for getting something done.\r\n\r\nThere is 1 bug/enhancement request that seems to relate to your description: \r\nhttps://bugs.kde.org/show_bug.cgi?id=223294\r\nIs this what you are referring to? Only one non-developer is following this bug, which has zero votes.\r\n\r\nHow would you suggest that the developers discover what you want?\r\n\r\n"
    author: "kallecarl"
  - subject: "I was referring to this"
    date: 2011-06-25
    body: "I was referring to this bug:\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=265051\r\n\r\nas you see, there seem to be no point in submitting bugs since some developers arbitrary close them."
    author: "trixl."
  - subject: "Hardly arbitrary, Aaron"
    date: 2011-06-25
    body: "Hardly arbitrary, Aaron explained why: some bugs had been fixed for 4.7 that may have solved this problem too and the report itself was too cluttered with pointless comments to be useful anymore.  He also explained what to do next: if someone tests it with 4.7 and finds the bug is still there then open a fresh bug report clearly stating what problem(s) still remain and then don't clutter it up with whining and moaning.\r\n"
    author: "odysseus"
  - subject: "He should have waited until"
    date: 2011-06-25
    body: "He should have waited until the bug is actually resolved to close the bug report. "
    author: "trixl."
  - subject: "But how would he know,"
    date: 2011-06-25
    body: "But how would he know, there's so much noise on that report it's hard to know anymore what one single topic it's supposed to be about.  We have thousands of bugs to try keep track of, messy ones like that don't make it any easier.  He clearly set out what to do, is it too much to ask the reporter to help keep track of things instead of overloading a single point of failure?\r\n\r\nHere's an idea, instead of spending your time uselessly trolling the Dot, how about helping out with bug triaging and start learning how to fix them instead?"
    author: "odysseus"
  - subject: "Here we go again"
    date: 2011-06-26
    body: "<cite>\"and start learning how to fix them instead\"</cite>\r\n\r\nThis should be the object of some Godwin-like Law for OSS topics. "
    author: "jadrian"
  - subject: "Fix my bugs now..."
    date: 2011-06-26
    body: "There is <em>no</em> point in starting every (pre-) release announcement with a \"fix my bugs now\" posting.\r\n\r\nI also encounter some (small) bugs, however\r\n<ol>\r\n<li>I can live with them / workaround them for the time being (my experience so far is that my bugs will get fixed, but it may take some time)</li>\r\n<li>there are many bugs that annoy me in the Windows world (work/office) and there is nothing I can do about it</li>\r\n<li>even if I knew that my bugs would get fixed faster if I annoy the developer / community I would not do it, as (i) it will demotivate the developer (ii) hurt KDE in the long term.</li>\r\n</ol>\r\n\r\nHow would I feel if I spend my time on a free software project and get emails blaming me for not closing bug xyz?(*)  I accept that I live in a free software world where developers decide which bugs they are going to work on (not a perfect world, but better than others). And I try to send at least one email to one of the KDE developers per year and send them a big THANK YOU for all the great work they have done! Hopefully, this compensates for some of the frustration they encounter...\r\n\r\n\r\n\r\n---\r\n(*) this is not so much about trixl's posting, however I have seen many postings / comments on bug reports where developers were blamed"
    author: "mkrohn5"
  - subject: "People can either ask nicely,"
    date: 2011-06-26
    body: "People can either ask nicely, or do it themselves, but whining won't take these kind of users anywhere.\r\n\r\nKDE people work very hard to give us users a great system. Anyone using that system should be infinitely grateful. For some reason, some people aren't.\r\n"
    author: "jankusanagi"
  - subject: "thank you for being more explicit"
    date: 2011-06-26
    body: "I read the entire thread. I do not see the conclusion you reached.\r\n\r\nThere is a point in reporting bugs. That's some of the value that users provide. However, there is no point in harping on a particular bug...trying to make something a squeaky wheel that needs to be greased is not working. It is not likely to start working.\r\n\r\nThere is nothing arbitrary about how this bug was treated. Several people (ASeigo in the main) explained clearly why the bug was being marked invalid. This does not mean that the bug doesn't exist, nor does it mean that no one is working on it. Unfortunately, both of these conclusions were drawn in the thread.\r\n\r\nThis bug report being labeled \"invalid\" could legitimately mean that--to developers--the report has lost its usefulness. Rather than arbitrarily blowing off the bug, a developer writes:\r\n\"...my recommendation is to wait for 4.7 to come out and test against that; i'm fairly confident that at least some of the issues reported here have been resolved.\"\r\n\r\nApparently that is not acceptable to the commenters who smell the chum, and continue with the remarks that I find mildly disrespectful. \r\n"
    author: "kallecarl"
  - subject: "minor error & taken out of context"
    date: 2011-06-26
    body: "Odysseus wrote:\r\n\"...instead of spending your time uselessly trolling the Dot, how about helping out with bug triaging and start learning how to fix them instead?\"\r\n\r\nYeah, there is a little acid in the full remark. And perhaps \"and\" would work better as \"or\". However, some of the remarks are troll-ish...provoking and disruptive to useful discourse. There are many other things that people could contribute rather wasting time on trolling. Given the subject matter of this thread, this might be a place where people could start...http://techbase.kde.org/Contribute/Bugsquad. Other opportunities are described at...http://en.wikipedia.org/wiki/KDE#Contribute.\r\n\r\nIt's ironic and amusing that people are getting some of the best software in the world and complaining that their itch is not getting scratched...arguing and commenting disrespectfully to people who are creating and fixing that software.\r\n\r\nKDE is a community. It operates better than any other community I know of. And it operates from particular design principles (http://www.kde.org/code-of-conduct/)...\r\nBe considerate\r\nBe respectful\r\nBe collaborative\r\nBe pragmatic\r\nSupport others in the community\r\nGet support from others in the community\r\n\r\nReading through the comments on the bug thread, it's clear that some people are in the KDE neighborhood, but not the community.\r\n"
    author: "kallecarl"
  - subject: "Yes."
    date: 2011-06-26
    body: "I agree with all that. Not trying to defend trixl. "
    author: "jadrian"
  - subject: "Off Topic"
    date: 2011-06-26
    body: "Why critical comments are scored negatively and silly condescending ones are scored positively? And more worrying: who has the \"power\" to score comments and why?\r\nI can't see any \"thing\" to vote comments; is it some feature reserved to registered users or are the site admins the ones who can do it? (if so they aren't administrators but pure censors. I hope to be wrong)"
    author: ""
  - subject: "Off Topic"
    date: 2011-06-26
    body: "Why critical comments are scored negatively and silly condescending ones are scored positively? And more worrying: who has the \"power\" to score comments and why?\r\nI can't see any \"thing\" to vote comments; is it some feature reserved to registered users or are the site admins the ones who can do it? (if so they aren't administrators but pure censors. I hope to be wrong)"
    author: ""
  - subject: "No learning required"
    date: 2011-06-26
    body: "Another way to get your \"important\" pet bug fixed when the developers have different priorities than you, are to simply hire someone to fix it for you(or since the user in question obviously pay for their distribution, convince them to fix it). No need to learn anything about development, just find a local developer to do the job. Small independent 1-5 man consulting shops are everywhere, so it should not be hard to find someone willing and able to do the job.\r\n\r\nIt may cost something in the range of $100-$300/h, but since the bug is such an important show-stopper that should not matter.\r\n\r\nAnd that value is the same the KDE developers continue to give us for free with every hour they spend writing code and working with bugzilla(or wasting on noise, like in this case). Wasting developers time, insult them and demand them to fix, as in this case, minor issues are incredibly ungrateful and rude."
    author: "Morty"
  - subject: "Because you are posting"
    date: 2011-06-26
    body: "Because you are posting anonymously.  Sign up with a user name and the vote buttons will magically appear."
    author: "odysseus"
  - subject: "As with posting, you need to"
    date: 2011-06-26
    body: "As with posting, you need to log in and the you may choose \"promote\" and \"bury.\" Quite simple, really, and no reason for implied conspiracy theories."
    author: "mutlu"
  - subject: "well, I don't have a pet bug,"
    date: 2011-06-26
    body: "well, I don't have a pet bug, but a bug shelter, in case anyone has interest. "
    author: "trixl."
  - subject: "Sign in."
    date: 2011-06-26
    body: "Log in using OpenID is brocken @ KDE.NEWS :("
    author: ""
  - subject: "live cd..."
    date: 2011-06-26
    body: "No live cd?\r\n\r\n:("
    author: ""
  - subject: "congratulations to all"
    date: 2011-06-26
    body: "An impressive list of new features,\r\nNew and hopeful directions for the projects,\r\nA growing community\r\n\r\nKDE is advancing, FAST!\r\n\r\n...If only I had money for a tablet :-)"
    author: ""
  - subject: "Trolls should be modded down."
    date: 2011-06-26
    body: "Trolls should be modded down.  The system works well.  Good criticism is usually modded up too."
    author: ""
  - subject: "comment scoring conspiracy"
    date: 2011-06-26
    body: "Logged-in users are allowed to express their opinions of comments with scoring. Anyone is free to express an opinion; shouldn't readers be able to reply in like fashion? \r\n\r\nIn this system, admins could simply delete any comments that are not sweetness and light. However, if critical comments were just deleted, we would miss such literary gems as \"silly condescending ones\". Priceless. "
    author: "kallecarl"
  - subject: ">Why critical comments are"
    date: 2011-06-26
    body: ">Why critical comments are scored negatively and silly condescending ones are scored positively?\r\n\r\nBecause open-mindedness and opinions aren't accepted here."
    author: "Ronald"
  - subject: "It will rock, like every major release"
    date: 2011-06-27
    body: "I am testing and reporting bugs for 4.7 since the branch was opened and it is going to rock. Like did 4.6, and 4.5, and 4.4 \u2026\r\nKeep up the nice work! :)\r\nAnd it\u2019s nice to see patches you made yourself have finally gotten spread to a wide audience.\r\n\r\nThings I really like in 4.7:\r\n* The new shadow system is awesome. Tooltips and menus look so nice, KDE typical shiny.\r\n* Dolphin\u2019s new sleak appearance is definitly a plus\r\n* KDE Pim 2 is just awesome\r\n* Akonadi error messages aren\u2019t annoying anymore\r\n* further polish of course\r\n\r\nThings I really really hate:\r\n* I will rant about this forever! WHY the hell did you remove the back button from kickoff? God, WHY?!"
    author: ""
  - subject: "Because Backward compatibility and usability is not KDE"
    date: 2011-06-27
    body: ">Things I really really hate:\r\n>* I will rant about this forever! WHY the hell did you remove the back button from kickoff? God, WHY?!\r\n\r\nIt's simple, developers they are konsole freaks, maybe they feel that they are smarter just because they can remember a few applications' binary names; so alt+f2 and their appname will get what they want. \r\n\r\nOnly stupid users like you and me would use menu to navigate. And because they don't use what they develop, they are not bothered to make things easier; now that the top bread-crumb looks nice but developers did not bother to test it's usability and they don't know that it is difficult to use because of too much mouse movement. Who can make them understand the ease of use was with vertical-back-button of kickoff when they never use the kickoff menu!!!\r\n\r\nRemember users are 2nd class citizens in KDE. That's why KDE has not become a users' desktop but a developers' desktop. And voting at bugs.kde.org is a joke."
    author: "fast_rizwaan"
  - subject: "The thing I like the most is"
    date: 2011-06-27
    body: "The thing I like the most is that kwin starts correct if you have a multi screen setup;\r\nhttps://bugs.kde.org/show_bug.cgi?id=256242\r\nBut there are still a few issue with multi screen setup. I have detected that taping does not work and programs dont start on appropriate screens. but it lookes like someone is working at it:\r\nhttps://bugs.kde.org/show_bug.cgi?id=256242"
    author: ""
  - subject: "Code of Conduct, please"
    date: 2011-06-27
    body: "<blockquote>Remember users are 2nd class citizens in KDE. That's why KDE has not become a users' desktop but a developers' desktop. And voting at bugs.kde.org is a joke.</blockquote>\r\n\r\nNo matter if your point is valid or not, it's not like ranting like you did will help. In fact, it will only generate bad blood in the community and little else.\r\n\r\nThere's not a \"dedicated\" maintainer for Kickoff, IIRC. To this regard, the Plasma developers need help, rather than ranting. \r\n\r\nKDE has a Code of Conduct: you should take a look at it. "
    author: "einar"
  - subject: "You're 90% right but..."
    date: 2011-06-27
    body: "please understand that for many users there is no big point anyway in shooting pre-releases one after each other, all with the same bugs as the older versions.\r\n\r\nRight or not, users gets very nervous, very fast when developers seem more interested in \"new features\" than in \"less bugs\".\r\nIt's something like a simple fact of life...\r\n\r\nThink about this: people don't get nervous about what they don't care, and having a userbase that cares can be the best resource for the community.\r\n\r\nAnd then there's the \"live up to expectations\" factor. I've read here announces and comments full of: \"the best software in the world\", \"new wonderful technologies\", \"social-semantic revolution\"... if someone buy all this and find himself troubled by the same, old, trivial bugs, he can easily get angry.  No surprise.\r\n\r\nBeing just a bit more prudent and a bit less boastful would help KDE community very much... \r\n\r\nOk, so a big THANK YOU to KDE developers is due. But, please, try to remember that behind every pending bug there is a frustrated user."
    author: "Vajsvarana"
  - subject: "To say that the plasma"
    date: 2011-06-27
    body: "To say that the plasma developers need help is an understatement, they seriously need help.\r\n\r\nKDE4 is now soon at 4.7 and I'm still using 3.5.10 due to all the bugs and nonsense (plasma, nepomuk) in KDE4. It's as simple as KDE3 _working_, and working _well_ on _all_ my systems, including EeePC 901 - it's fast and responsive. KDE4 OTOH is annoyingly resource consuming and crawls even on my fastest system (quad Xeon 2.4GHz, 8GB RAM). And if that wasn't bad enough, the usability is really piss poor as well.\r\n\r\nI'm not trying to troll here, I'm just saying what I see and how I experience it. I really had high hopes for KDE4 to be utterly cool, but have instead been utterly disappointed with it, they're basically bringing it in the opposite direction of what I had hoped. And with that vanished the last hope for a really cool desktop on un*x.\r\n\r\nExample of how it sucks... to use music productivity software, I simply _have_ to change to something else than KDE4 - plasma constantly pulls between 60% and 90% CPU, in a stupid, everlasting clock_gettime event poll loop. Yes, the plasma developers need help!"
    author: "kolla"
  - subject: "I thought it was just me with"
    date: 2011-06-27
    body: "I thought it was just me with the problem of the plasma process running amok... "
    author: ""
  - subject: "Knotify"
    date: 2011-06-27
    body: "I have 4.7 Beta 1 installed, but lately I've been using mostly LXDE (openSUSE 11.3) so I don't know about plasma. But I can say that even here I've experienced knotify eating away my processor more than once.  "
    author: "jadrian"
  - subject: "doesn't make sense"
    date: 2011-06-27
    body: "My machine is far behind these specs \"quad Xeon 2.4GHz, 8GB RAM\". It is speedy running 4.6.4. A few things have helped with that--most notably changing OpenGL to XRender in System Settings, due to lack of support for Intel GM965.\r\n\r\nI'm far from being as tech savvy as others here. But working with the various forums appropriate to this distro, and learning--via readily available web resources--what might be causing difficulties, I've got a system that works great.\r\n\r\n4.6.4 running on some duo core laptop with 4G and also on an Aspire netbook, both work great.\r\n\r\nWhat steps have you taken to solve the problems you're having? Any forum posts? Local LUG?\r\n\r\nKDE4 has completely lived up to my expectations. Can't imagine using any other desktop. Not only that, when I run the netbook at the local coffeeshop, nearly every time, someone wants to know \"what is that?\" I've loaded KDE 4 on 3 newbie computers by now, and they love it. Yes there are a few glitches from time to time, but nothing like what they were having with other mainstream OSs.\r\n\r\nCouldn't be happier with an OS.\r\n\r\nUPDATE: Just did Windows and other Updates on my wife's Win7 machine. Dreadful.\r\n\r\n"
    author: "kallecarl"
  - subject: "KDE on a netbook"
    date: 2011-06-28
    body: "I'm using KDE on a netbook (1.6ghz n450 with a intel 3150) since months.\r\n\r\nIt's nice to use - except when I open too many tabs on chromium.\r\n\r\nI don't use nepomuk/strigi anymore because I always know where my few files are (but I've used it for awhile and worked good)\r\n\r\nOh, I just checked and plasma-desktop stay @ 0%"
    author: ""
  - subject: "difficulties involved"
    date: 2011-06-28
    body: "Just read this <a href=\"http://blog.martin-graesslin.com/blog/2011/02/kwin-and-the-unmanageable-combinations-of-drivers/\">post from a KWin developer</a>. Gives some sense of developer focus...it's not fluffy new eyecandy to the exclusion of making the existing stuff work.\r\n\r\n\r\n"
    author: "kallecarl"
  - subject: "Our familys feedback"
    date: 2011-06-29
    body: "It is the the Number 1 desktop in the world hands down.  Thanks KDE team.  My family and most all my friends use this desktop.  And some of us have just started learning how we can report bugs to give back.  Wish had money to donate but thats not the case with us.  But you guys are appriated to no end here.\r\n\r\nThanks.\r\nElsa  Saltora"
    author: ""
  - subject: "Re: KDE on a netbook"
    date: 2011-06-29
    body: "It's good that your experiences are positive, and I think some people still need to recognise that not everyone sees dreadful performance with KDE 4.x - I don't, on four different machines of varying age and capability, FWIW.\r\n\r\nOn the other hand I don't think it necessarily takes away from the negative experiences some people still seem to have with KDE. Still, my bet is that most of these problems are graphics driver related. In any case the best way to get these problems solved is if the people having these problems can try to work constructively with the developers. How can this work? Perhaps start with the following:\r\n\r\nUser: <em>tell me what I need to do / what information I need to provide you with in order to fix this problem.</em>\r\n\r\nDeveloper: <em>ok, you need to do this, this, and this, and send me the results.</em>\r\n"
    author: "bluelightning"
  - subject: "where is the changelog?"
    date: 2011-06-29
    body: "Can someone help me to find the changelog please? I would need it to write an announce on linuxfr.org french site."
    author: "zeroheure"
  - subject: "Doesnt\u2019 help 99% of the users"
    date: 2011-07-01
    body: "Nobody ever uses multihead, i.e. having mutiple X servers.\r\nYes I know (or I assumed) that it will work on mutlihead (that\u2019s what the patches in ksplashx are about) but they broke the multiscreen stuff and multiscreen is what 99% of people have. So, this is a major regression for some stupid stuff that will eventually die anyway (Wayland \\m/)"
    author: ""
  - subject: "Correct me if I am wrong but"
    date: 2011-07-01
    body: "Correct me if I am wrong but on linux there is 2 way to have more than one screen attached. There is Xrender witch only work on 1 card and there is separated x. \r\n\r\nI have 2 cards to power my 4 screens so that renders Xrender usless. And separated x has never worked proper with kde 4 (but there is still small progess with each release).\r\n\r\nAbout Wayland I think it is at least 6-8 years before it will be usefull (eg before KDE, drives (moth opensource and closed source) will support it nicely). Which is around the time where I should give up my free (free as ion beer,not speach) windows 7. I just hoped that I could return to KDE before that time\r\n\r\n\r\n\r\n\r\n"
    author: ""
  - subject: "Both kickoffs search function"
    date: 2011-07-01
    body: "Both kickoffs search function and the run tool search by more than just name. If you typed in paint looking for a program to paint with for example it would find Krita and mypaint. If I typed in word it would find the libreOffice word processer component. Both also search recent documents so you can just type in the name of the last file you were working on.\r\n\r\nI am not exactly sure what your beef is with the breadcrumbs (unless you didn't realise that you can switch to the old representation by double clicking on the address).\r\n\r\nThe problem with the vertical button in the kickoff menu is the discoverability of it. If you go back and look at the complaints about this when 4.0 was released. Of course people are used to it now...\r\n\r\n\r\n\r\n"
    author: ""
  - subject: "No OS builds information"
    date: 2011-07-01
    body: "Why isn't there any builds from the OS guys like kubuntu, opensuse etc. Apart from unstable repositories which bring in a whole bunch of other software, couldn't find another repo. Hopefully someone releases soon."
    author: ""
  - subject: "Really?"
    date: 2011-07-01
    body: "Care to provide a source for a single one of those release announcement quotes?\r\n\r\nSomething we've done in the past (it depends who writes the article and how much time they have to get the info) is detail the number of bugs fixed since the last prerelease. It's always a lot.\r\n\r\nWe perhaps don't give enough publicity to bugs fixed and developers tend to blog more about new stuff than fixing old stuff, but that does not mean it is not happening."
    author: "Stuart Jarvis"
  - subject: "Fedora unstable repository has been updated..."
    date: 2011-07-02
    body: "...and I have first impressions of what KDE 4.7 is.\r\n\r\n1. A long time ago, with KDE 4.4, an idle CPU usage of 2% with effects, without blur, was achieved, and I was happy. KDE 4.5 introduced blur, but it was unbearably slow with my poor GeForce 6150. KDE 4.6 improved somewhat the situation, but I was still idling at more than 20%. Now KDE 4.7 idles at 2% WITH BLUR. Thanks, KWin sorcerers, you made me really happy.\r\n\r\n2. There is a mega-bug with Strigi, again. Nepomuk reindexes everything all the time and the CPU usage rises to 99% unless I disable Strigi. But, this time, two things are different: 1) developers are working overtime to kill this bug once and for all, and 2) last time, it was an eaten core. Now it's each core running at 45%. Strigi can paralellize work, and it will perform better with multicore CPUs. This bug is going to be killed soon.\r\n\r\n3. Clock. It SIMPLY LOOKS AND BEHAVES AWESOMELY.\r\n\r\n4. Shadows. They look great, and, if you place your panel on top, you'll see the shadow obscuring your titlebar nicely, like it should be. Great. What's not so great is that the Plasma Air theme backgrounds are 4.6's, so, even if Plasma can render shadows properly, the Air theme still has SVG shadows drawn inline and I still see plasmoids snapping to their shadows, instead of snapping to their borders. Can you fix that? Now that the real fixes are in place, it's a matter of removing some SVG shadows, setting some flags so Plasma keeps in charge, and voil\u00e0. \r\n\r\n5. Oxygen. It has evolved nicely, except for the folder icon. I still like the old one better. Nuno, I love your work, but your new folder icon is a mistake in itself.\r\n\r\n6. KDE Microblog. It can do full retweets and mark some tweets as favorites, but it still can't retrieve more than 16 tweets at once. Please, can somebody lift this limit?"
    author: "Alejandro Nova"
  - subject: "Keyboard is broken, OP is unpolite."
    date: 2011-07-02
    body: "Actually not; if I type \"word processor\" I get LibreOffice Writer, so, it's a lot more intuitive than what you think. The biggest omission isn't the button (the breadcrumb can fully replace it), but the total lack of keyboard navigation where the breadcrumb is used (navigating with the keyboard in the \"Computer\", \"Recent Files\" and \"Exit\", where no breadcrumb is used, work; navigating with the keyboard in the \"Favourites\" or \"Applications\" panes doesn't work). The sad reality is: KDE is useless without a mouse.\r\n\r\nI filed bugs regarding keyboard interaction, but they have been unnoticed or ignored. A full review of how KDE interacts with the keyboard is required, there are simply too many inconsistencies."
    author: "Alejandro Nova"
  - subject: "Wi-fi not working"
    date: 2011-07-03
    body: "Wi-fi is not working on Ubuntu/Kubuntu 11.04 (Natty Narwhal). On trying to connect to your network the program informs you there is no firmware installed and no ammount of correction will get you on-line unless you have an ethernet cable long enough to reach your router (which may be in an inaccessible place - like by the front door to your apartment/house). Ubuntu/Kubuntu is becoming like Microsoft in making costly and silly mistakes in its supposed stable software. Basically, [I don't like] Ubuntu/Kubuntu! terabyte1"
    author: ""
  - subject: "Wi-Fi Not Working"
    date: 2011-07-03
    body: "Incidentally, I've been with Ubuntu and Kubuntu since Dapper Drake, and until Canonical moved to Unity on Ubunt I had no problems. On Kubuntu there was the usual high scoring bug-fixes by a backup-team which is best described as stupendous for their efforts in tackling the challenges from going from 3.5 to 4.00 Series KDE. Most times Kubuntu now is surpassing the 3.5 version of Kubuntu when it was around, except for the problem of the Wi-Fi no-go area. I've been with Computers since the early 70's working with BSD (Unix) fragments at Plessey Ltd sometimes on telecommunications equipment using a 'Strowger' testing lines in the main Telephone Exchange bays of Liverpool. Also we worked with Nasa with regard to their Gemini Project building equipment for them. So you see, I'm not just a computer monkey newly joined to Linux/Unix computer systems, and sometimes I get frustrated when things go wrong with otherwise superb software from my favourite stable (Canonical). So sorry if I seemed a mite peevish. terabyte1"
    author: ""
  - subject: "Wi-Fi Not Working"
    date: 2011-07-03
    body: "On the same issue (above),I currently have version 10 (Julia) on LinuxMint under KDE (becoming my favourite desktop ;) ) on my main machine and it works beautifully there - it locates my network name automatically and my pass-phrase without any problems. On my backup machine I have (again) KDE under PCLINUXOS and that works beautifully too with my wi-fi. It's just Fedora, OpenSuse, Sabayon, Centos, Debian, Suse Enterprise Server (SLED), and Red Hat Enterprise Linux 6., I'm having problems with! Terabyte1"
    author: ""
  - subject: "wifi working fine here"
    date: 2011-07-03
    body: "On my 2 machines and several others that I support, WiFi is working great...implemented in a thoughtful manner. Kubuntu (desktop and netbook) are working with no issues as well.\r\n\r\nThis sounds like a hardware issue. And almost certainly is not associated with KDE.\r\n\r\nHave you posted to the Kubuntu and Ubuntu forums?"
    author: "kallecarl"
  - subject: "Breadcrumbs work for me"
    date: 2011-07-04
    body: "About the removal of the back button in kickoff. Looks like this will a sore spot for some folks and seen as an improvement by many more? Or is it more half and half? Oh. wait...this a pre-release and hasn't been kicked out on the street yet. ;)\r\n\r\nOkay, all kidding aside, this is obviously one of those strictly subjective things and personally, I like the change to a bread crumb style navigation although the font could be a bit larger or at least a bit darker but that might be theme related (I'm running 4.7(pre) on Chakra Edn milestone 1). We all have our pet peeves and one of mine concerning the kickoff style menu was that awkward back \"bar\" on the left hand side of the menu. But that's just my opinion, you know?"
    author: "kmb42vt"
  - subject: "Not always KDE"
    date: 2011-07-04
    body: "One thing I have to keep in mind all the time is that not all \"bugs\" are due to a problem with KDE (or any DE for that matter) but more a problem with the underlying distribution itself. For example, \"nepomukservicesstub\" (I might not have gotten the spelling right on that) randomly crashing in the KDE 4 series (up through 4.7 RC1 by the way) pretty much runs across all distros I've tried on two different machines but certain other processes hogging the CPU or using too many resources are more distro related than anything else. (I know the devs are hot on the Nepomuk/Strigi problems so I'm not too concerned about it. They'll have it sorted out eventually I'm sure.)\r\n\r\nSomeone mentioned a \"clock_gettime event poll loop\" constantly using 60% to 90% of their CPU that simply doesn't exist on one particular distro I have installed that uses the latest KDE 4.7. Nor does it exist on another distro that uses the latest 4.6.4. Admittedly it can be extremely difficult when trying to figure out whether certain problem are either DE or distro related. Some are pretty obvious and some are not."
    author: "kmb42vt"
  - subject: "I also like better the breadcrumb..."
    date: 2011-07-04
    body: "...but what I don't like is the breakage of keyboard navigation. I filed a bug about that."
    author: "Alejandro Nova"
  - subject: "konqueror crashing plasma/kde"
    date: 2011-07-04
    body: "when moving toolbars in konqueror, kde and plasma is crashing, can someone confirm this ?\r\n\r\nwhich qt is needed by RC-rpm's ? is it possible to run 4.7 RC1 with 4.7.80+  ??\r\n\r\n3d (effects,compisiting) seems broken in kde 4.6.90\r\n"
    author: ""
  - subject: "konqueror crashing plasma/kde"
    date: 2011-07-04
    body: "when moving toolbars in konqueror, kde and plasma is crashing, can someone confirm this ?\r\n\r\nwhich qt is needed by RC-rpm's ? is it possible to run 4.7 RC1 with 4.7.80+  ??\r\n\r\n3d (effects,compisiting) seems broken in kde 4.6.90\r\n"
    author: ""
  - subject: "The problem there is that"
    date: 2011-07-05
    body: "The problem there is that clearly few developers are interested in Kickoff and only the basic maintenance is done... With things like these you're simply depending on someone sending in a patch which will surely be reviewed and integrated."
    author: "jospoortvliet"
  - subject: "Usually these packages are"
    date: 2011-07-05
    body: "Usually these packages are added along the way but you can count on openSUSE having them within a few days as it builds all of KDE Trunk at least once a week and semi-stable packages are available for all openSUSE versions (11.2-11.3-11.4-tumbleweed-factory).\r\n\r\nYou can build your own liveCD by using these repositories on http://susestudio.com :D"
    author: "jospoortvliet"
  - subject: "@point 4:\nunfortunately, the"
    date: 2011-07-05
    body: "@point 4:\r\nunfortunately, the plasma desktop itself can't use the KWin shadows, widgets are not windows but really part of one big plasma window...\r\n\r\n@point 6: I don't think anyone works on microblog unfortunately but a new microblog is being developed as part of plasma active with QML, I believe..."
    author: "jospoortvliet"
  - subject: "I want my back button back"
    date: 2011-08-06
    body: "I want my back button back :( \r\nIt was a nice, big, easy to hit target in a consistent location ... now what I have to click on is tiny, medium contrast text in an out of the way place - that is - I have to move the mouse all the way to the top just to able to go back! Completely destroys the flow of it IMO. At least throw it back in as an option ... pretty please?"
    author: ""
---
<p>KDE <a href="http://www.kde.org/announcements/announce-4.7-rc1.php">has released</a> a first release candidate of the upcoming 4.7 release of the Plasma Desktop and Netbook workspaces, the KDE Applications and the KDE Frameworks, which is planned for July 27, 2011. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing last-minute bugs and completing translations and documentation.
</p>
<p>
The 4.7 release will bring a number of exciting improvements:
<ul>
    <li>KWin, Plasma's window manager <a href="http://www.kdenews.org/2011/02/18/kwin-embraces-new-platforms-opengl-es-20-support">now supports OpenGL-ES 2.0</a>, improving performance and deployability on mobile devices</li>
    <li>Dolphin, KDE's flexible file manager has seen user interface improvements and now sports a better user experience for searching in files' metadata.</li>
    <li>KDM, KDE's login manager <a href="http://ksmanis.wordpress.com/2011/04/21/hello-planet-and-grub2-support-for-kdm/">now interfaces</a> with the Grub2 bootloader</li>
    <li>Marble, the virtual globe now supports offline address search, especially making its mobile version more useful on the road</li>
</ul>
<p>To download source code or packages to install go to the <a href="http://www.kde.org/info/4.6.90.php">4.7 RC1 Info Page</a>.
</p>
</p>
<!--break-->
