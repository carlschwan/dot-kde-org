---
title: "Keynotes and Sandals - Day Two at Desktop Summit 2011"
date:    2011-08-08
authors:
  - "Stuart Jarvis"
slug:    keynotes-and-sandals-day-two-desktop-summit-2011
comments:
  - subject: "I want youtube video."
    date: 2011-09-05
    body: "Where is keynote video?\r\n\r\n"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/boud.jpeg" /><br />Boudewijn Rempt shows off Krita</div>For the second full day at the Desktop Summit, the organizers played a little trick on us by starting talks at 9:00 a.m. Those who were awake enough after the dinners and chat of the previous night were treated to talks on Calligra (the KDE creativity and productivity suite), suggestions about blending the web and the desktop, color management and the build process for GNOME. Those who were still in bed will have to wait for the videos and slides to be posted online in the next few days.

The hallways and courtyard were again busy with small, lively discussions. As the morning went on the attendance at talks increased noticeably. Sunday was also the day of the press conference, where key figures from GNOME, KDE and the cross-community organizing team met with the press to answer their questions about the event and the future of free software.
<!--break-->
<h2>An Exciting Day for KDE</h2>

For KDE, there were many highlights among the day's talks. Sebastian Kügler presented the plans for Plasma Active and explained how it was different from other user interfaces designed for tablets and other mobile devices, especially with its context-awareness. The plan is to replace the long established metaphor of objects and icons to one that is based on an awareness of what the user is doing and where. The interface operates by activity centric interactions and integrates common actions like "share", "like" and "connect" for content - for example the ability to share your own photos, "like" someone else's or to discuss them. Plasma Active integrates with social web applications and is extendable via scripted plugins.

For the official set of key applications, the code is architecture-independent, so it's easy to distribute. Plasmate will provide an infrastructure for creating small, architecture-independent packages. Open Build Service is used for C++ apps. Live images are available from <a href="http://open-slx.de/en/">open-slx</a> for the curious; there are efforts to interact with hardware vendors to get Plasma Active onto consumer devices.

Exciting new applications based on Phonon and Qt Quick are just around the corner. The next version of Dragon Player will use these extensively to build a much slicker KDE video player.

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasmaactive.jpeg" /><br />Plasma Active</div>Krita, KDE's natural drawing and painting program, has gained momentum and praise over the past few years. Boudewijn Rempt explained the project's history, from an attempt at a Photoshop or GIMP clone to finding its own unique place. The focus now is on being a great tool for professional digital graphics artists, distinctly different from anything else available, FOSS or not.

A powerful application such as Krita can take a while to learn, so the team is putting together a professional quality training DVD for Krita. This is available for pre-order at a discounted rate, to help cover up-front costs. Krita is well on its way to its fundraising target, but needs a little financial help. If you have not preordered your copy of the DVD, do so now at <a href="http://krita.org/">the Krita website</a>. Any money raised over the amount needed to produce the DVD will be used to fund additional development.  

Torsten Rahn introduced the latest features in Marble 1.2, such as voice navigation, and talked about the long road to the great application we now enjoy. He also discussed developments leading up to a future 2.0 release, incorporating features such as OpenGL and Qt 5, expected in 2012 or 2013. Rekonq, a WebKit-based KDE browser with many new fans, and the default browser for Kubuntu, was discussed by Andrea Diamantini and Pierre Rossi. They explained how they were motivated to create a new browser and what will be coming next.

Perhaps the biggest KDE news of the day came in one of the last sessions, "KDE Platform 4 Roadmap". KDE's core developers unveiled a plan to switch from Platform 4 to Frameworks 5, a far more modular approach to the KDE libraries and closer ties to Qt  than they currently have. Smaller, more specialized library modules will make it easier for developers to pick just the KDE pieces they need for their applications without large increases in dependencies or size of binaries. For those who prefer to install all of KDE Frameworks, it will be just as easy to package them in large groups as is currently the case, thanks to a common release schedule and automated tools to combine modules. Moving KDE technologies into Qt will continue, if it's possible to do so.

<h2>Keynotes</h2>
As on the first day of the summit, we had two keynotes to enjoy. Both were focused on the process of design and what lessons can be applied to free software. Claire Rowland discussed design issues in cross-platform and cloud experiences, exploring what effective design looks like. Later in the day, Nick Richards gave the GNOME community keynote, again with a message broadly applicable in free software.

<h3>Claire Rowland - Design Across the Device Spectrum</h3>
Claire is not a technologist. She used to be a UI designer but no longer. She's now a researcher, specializing in understanding how people use computers, with a particular interest in designing for wide range of devices. Referring to designing for multiple devices, Claire spoke about the influence of commercial enterprises. At present, we are slaves to a proprietary empire, doing whatever Steve Jobs thinks is best.

Most Desktop Summit attendees will fiddle with tech to get it to work. Most other people, including Claire, are not like that. People want tech, but they do not want to spend time learning it.

We are getting more devices. Smartphones outsell pcs. TV programs are on the web. Cars even are connected. The number of devices connected to IP networks will be twice the global population by 2015 and the non-PC part of that is growing rapidly. That's a lot of connectivity and potential problems making stuff work together.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/nick.jpeg" /><br />Nick Richards</div>The view Claire takes is service design. It's hard to describe because we interact with services at multiple points and most people see the service as that interaction point. For example, we see our post man, not the mail service as a whole. Consider a mobile app linked to weighing scales. The app is just an entry point to the weighing service. It doesn't matter how good the app is if the entire service doesn't work well.

The prevailing view of usability is of one main device with a screen, and making sure it works well there. The new view is that there are lots of devices, some without screens, that may be context aware, and there may have multiple users. Thus there are many interaction points, touch points.

So what's good design for a touch point? It must be appropriate to the device (size, complexity, etc) and demand only the minimum amount of attention. We need to be better at prioritizing features where and when users need them. At the moment, smartphones are just buckets of apps on a desktop. Every service needs a clear mental model--what is it for and how is it used. For example, Dropbox (Personal Storage in the Cloud) hides internals. It looks to a user as just a folder where you can share stuff.

Claire's key message...Think about much more than the desktop and make your stuff work everywhere. Include free services to replace proprietary ones.


<h3>Nick Richards - Iteration's What You Need</h3>
The GNOME community keynote was presented by Nick Richards, a user interface designer with Intel who is heavily involved in GNOME Shell.

He explained that designers have a nice job because they get to solve people's problems. Nick believes that "GNOME 3 is a really good example of design led stuff" and says they aim to "listen to what people are saying and deliver that in as generic and scalable manner as possible". You can't say no all the time, as it is easy for people to take the code and do their own thing, either via extensions or forks. Therefore it is better to say yes, but not always possible.

Nick said that GNOME 2 was great, but the end of the line was being reached in terms of scalability, so a big switch to GNOME 3 was needed. He acknowledged that some of the GNOME apps are not as good as the rest of the desktop, and there are plans to work on this to raise quality overall.

Among free software in general, Nick believes that tools for designers still need to improve if designers are going to be persuaded to move away from working on paper. He also noted that individual designers do not scale, so there is a need for many more designers in free software

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/press.jpeg" /><br />Press conference panel</div>Turning to the main topic of his presentation, Nick argued that "iteration is not a rewrite from scratch". An iterative process would never have led to GNOME 3, but it can lead to rapid improvement once you have got the basics right. Feedback is essential and it is perhaps true that a free software community cannot do the same level of testing as companies. The best way of improving quality can be getting software out as quick as possible and getting reaction, by making it easy to install test versions.

<h2>Meeting the Press</h2>
The press conference was well attended by journalists from several magazines and a local radio station. They were pleased to hear that the Summit has already attracted around seven hundred attendees.

Questions ranged from specifics of the summit to larger issues in free software, such as levels of deployment in enterprises. The panel mostly agreed that this is hard to measure, since very few companies publicly disclose the software that they use. Governments and educational institutions are much happier to do this and so we hear more about them.

Another question that arose was when the next desktop summit would be and whether it would include Unity. While there are no firm plans, there is a concensus to combine summits once every two years so the next Desktop Summit will probably be in 2013. On the subject of Unity, the organizers prefer to wait and see how significant that project is in two years. However, there is an intention to invite more free software developers in general, making the Summit attractive to all people interested in consumer-facing software.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/party.jpeg" /><br />Party at the beach</div><h2>Party Time</h2>
A full day of presentations and sharing of ideas was followed by a party at the Box on the Beach of the River Spree--thanks to the party sponsor, Intel. Hundreds of people at the barbecue and karaoke extravaganza enjoyed complimentary drinks--mostly beer, and a variety of grilled goodies and salads. The line for the food was long enough to keep quite a few people down at the beach for conversation and informal get-togethers.

The coat check lady asked, "Who are these people? I have one coat and a room full of laptops." The karaoke was excellent--it's hard to say who was best and who needs more practice. There was no shortage of talent with a 3 hour waiting list to get onstage. The police came...the hallmark of a great party.

<h2>The End of Another Day</h2>
All in all, it was another great day at the summit. There is too much to report in detail, from the fun of getting hundreds of people together for the group photo to revealing who did the most karaoke at the party. One great thing is the real sense of how happy everyone is to be here. At least two of our Google Summer of Code students from India even used their payment from that program to fund their travel costs to the Desktop Summit and they seem to think it is money well spent.

There is only one more day of talks to report on now, but that will include such highlights as Thomas Thwaites revealing how to make a toaster - from scratch, including mining the iron and copper to make power cables. Check back for that news tomorrow and throughout the rest of the week for the latest from the workshop and interviews of some of the hot topics arising from the past few days.