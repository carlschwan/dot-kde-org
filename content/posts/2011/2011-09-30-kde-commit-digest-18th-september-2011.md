---
title: "KDE Commit-Digest for 18th September 2011"
date:    2011-09-30
authors:
  - "mrybczyn"
slug:    kde-commit-digest-18th-september-2011
---
In <a href="http://commit-digest.org/issues/2011-09-18/">this week's KDE Commit-Digest</a>:

<ul>
<li>New render plugin in <a href="http://edu.kde.org/marble/">Marble</a>: GpsInfo</li>
<li>A new service for powermanagement tasks such as suspension and shutdown</li>
<li><a href="http://www.kde.org/applications/multimedia/dragonplayer/">Dragon Player</a> now uses Phonon instead of xine-lib for DVD menus</li>
<li>Overburn tolerance is increased in <a href="http://www.kde.org/applications/multimedia/k3b/">K3b</a> from 10% to 25%</li>
<li>Basic drag-and-drop support for contact list is added in Telepathy</li>
<li>In <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>, DSL connections can be associated to Ethernet interfaces</li>
<li>First implementation of Plasma applet in wicd-kde</li>
<li><a href="http://userbase.kde.org/Plasma/Public_Transport">Public Transport</a> sees much work, especially around GTFS feeds</li>
<li>Adblock improved in <a href="http://opendesktop.org/content/show.php?content=94258">rekonq</a></li>
<li>KSecret file serialization logic rewrite</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> sees work on PPT and ODT formats, anchoring, search from cursor and other changes.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-09-18/">Read the rest of the Digest here</a>.

The Commit Digest team needs help! Please go to <a href="http://commit-digest.org/contribute/">commit-digest.org/contribute</a> to see what you can do.