---
title: "KDE Commit-Digest for 24th July 2011"
date:    2011-07-29
authors:
  - "mrybczyn"
slug:    kde-commit-digest-24th-july-2011
comments:
  - subject: "Awesome!!"
    date: 2011-08-01
    body: "Excelent.... Thanks!"
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-07-24/">this week's KDE Commit-Digest</a>, we have a featured article by Ivan Čukić on the new activity manager. The code changes this week include:

<ul>
<li><a href="http://www.calligra-suite.org/">Calligra</a> Stage gets a new user interface for creating and editing custom slide shows in its Slides Sorter View.</li>
<li>Gapless playback is implemented in Phonon-GStreamer.</li>
<li>For <a href="http://rekonq.kde.org/">rekonq</a> adding favorites is made easier and a user interface option for configuring the <a href="http://en.wikipedia.org/wiki/Do_not_track_header">do not track header</a> is added, there are also updates to the SSL support.</li>
<li>The <a href="http://userbase.kde.org/Plasma/Public_Transport">Public Transport</a> applet can now show stops in <a href="http://edu.kde.org/marble/">Marble</a> with the updated OpenStreetMap data engine.</li>
<li>In <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/">kdelibs</a>, we have a usability improvement in the <i>Edit File Type</i> button and an introduction of package cache for <a href="http://userbase.kde.org/Plasma">Plasma</a>.</li>
<li><a href="http://userbase.kde.org/Kate">Kate</a> now correctly handles comments when doing static code wrap.</li>
<li>Bugfixes were made to a diverse range of applications, including <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://edu.kde.org/marble/">Marble</a>, and <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-07-24/">Read the rest of the Digest here</a>.