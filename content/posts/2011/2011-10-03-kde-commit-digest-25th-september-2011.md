---
title: "KDE Commit-Digest for 25th September 2011"
date:    2011-10-03
authors:
  - "mrybczyn"
slug:    kde-commit-digest-25th-september-2011
---
In <a href="http://commit-digest.org/issues/2011-09-25/">this week's KDE Commit-Digest</a>:

<ul><li>New sorting options in KDEBase</li>
<li>Work on screen locking as an effect</li>
<li>Optimization of message list update in KMail</li>
<li>VPN status overlay icon reworked in <a href="http://userbase.kde.org/NetworkManagement">Network Management</a></li>
<li>Optimization of QVector usage in Undo manager in <a href="http://api.kde.org/4.7-api/kdelibs-apidocs/">KDElibs</a></li>
<li>Optimization of item addition and deletion to a project in <a href="http://k3b.org/">K3B</a></li>
<li>Bugfixes in <a href="http://www.calligra-suite.org/">Calligra</a>, including date entering in Kexi forms.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-09-25/">Read the rest of the Digest here</a>.

The Commit Digest team could use help! A few volunteers publish the Commit Digests as a service to the KDE community. The weekly Digest provides details about software improvements and recognizes technical contributions to KDE. When anyone from the team is unavailable, it is delayed or not published. Great technical or writing skills are not required for the tasks, which can be done in an hour or less each week.

You can make a noticeable contribution to KDE as part of the Commit Digest team. Please go to <a href="http://commit-digest.org/contribute/">the Commit Digest/Contribute page</a> to see how to get involved.