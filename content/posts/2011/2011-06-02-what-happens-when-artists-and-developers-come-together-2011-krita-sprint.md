---
title: "What happens When Artists and Developers Come Together: The 2011 Krita Sprint"
date:    2011-06-02
authors:
  - "Boudewijn Rempt"
slug:    what-happens-when-artists-and-developers-come-together-2011-krita-sprint
comments:
  - subject: "I would love to uninstall"
    date: 2011-06-02
    body: "I would love to uninstall Gimp but somehow all the hype about krita never materialized into something usable. I hope this changes in the near future.\r\n\r\n"
    author: "trixl."
  - subject: "Well..."
    date: 2011-06-02
    body: "<p>In the first place, Krita doesn't compete with GIMP, that should be clear when you compare the product vision for GIMP:\r\n\r\n<cite><em><strong>\r\n    <p>\"GIMP is Free Software;\r\n    <p>GIMP is a high-end photo manipulation application and supports creating original art from images;\r\n    <p>GIMP is a high-end application for producing icons, graphical elements of web pages and art for user interface elements;\r\n    <p>GIMP is a platform for programming cutting-edge image processing algorithms, by scientists and artists;\r\n    <p>GIMP is user-configurable to automate repetitive tasks;\r\n    <p>GIMP is easily user-extendable, by \u2018one-click\u2019 installation of plug-ins\"</strong></em>\r\n</cite>\r\n\r\n<p> and for Krita:\r\n\r\n<cite><em><strong>\r\n<p>\"Krita is a KDE program for sketching and painting, offering an end\u2013to\u2013end solution for creating digital painting files from scratch by masters.\r\n\r\n<p>Fields of painting that Krita explicitly supports are concept art, creation of comics and textures for rendering.\r\n\r\n<p>Modeled on existing real-world painting materials and workflows, Krita supports creative working by getting out of the way and with snappy response.\"\r\n</strong></em></cite>\r\n\r\n<p>So you shouldn't consider Krita as a GIMP replacement -- that's not what we are trying to achieve. We haven't achieved everything in our vision statement, of course, that's why it's a vision, but as for whether Krita has grown into something usable, I believe, based on what I have seen artists do with Krita 2.3, that from Krita 2.3, we have succeeded in creating a usable application for its purpose. Krita 2.4 will be much nicer, of course, but then, 2.5 will be better than 2.4 and so on.\r\n\r\n<p>Just check the art that regularly gets posted to http://krita.org and the Krita forums at http://forum.kde.org/viewforum.php?f=136."
    author: "Boudewijn Rempt"
  - subject: "Showfoto"
    date: 2011-06-02
    body: "And Showfoto deals with the photo editing side. I still have GIMP installed too, but there isn't a lot that I can do there that I cannot do with Showfoto and Krita."
    author: "Stuart Jarvis"
  - subject: ">So you shouldn't consider"
    date: 2011-06-02
    body: ">So you shouldn't consider Krita as a GIMP replacement \r\n\r\nI think many people are confused about that. "
    author: "trixl."
  - subject: "yes..."
    date: 2011-06-02
    body: "That confusion has always been present. Already in 1.x days we (that's the developers) discussed painting vs image editing. In 2007 we made the first public statement that Krita was about painting and that's what we've been saying ever since, though sometimes we've been confusing ourselves as well. And, sure, there are filters and so on -- but those are used by artists, as you'll see when the video recording of the artists' session is done (which will be soon)."
    author: "Boudewijn Rempt"
  - subject: "not exactly"
    date: 2011-06-02
    body: "ShowFoto is mainly to show/display pictures and it's editing capabilities are very limited compared to GIMP.\r\n\r\nBest way how to archieve nice photos remains to shoot nice photos :-)"
    author: ""
---
<p>In what is beginning to become quite a tradition, the <a href="http://www.krita.org">Krita</a> team came together from May 20 to May 22 for the third Krita meeting. The first sprint had four attendees. Last year's, eight. This year, we had twelve people in attendance. If the trend continues, the Blender Institute, which hosted our sprint, will be too small for us next year!
<!--break-->
<h2>Friday</h2>

<p>Friday was mostly spent arriving, getting network access, doing preliminary hacking and so on. The real core of the sprint took place on Saturday. In the morning, we had an Artist's Demo and Gripe Session. The four artists present took turns demonstrating how they work with Krita. With a projector, everyone could follow along. We managed to record a screencast of all sessions and a video/audio recording with the Blender Institute's camera. The Krita webmaster, Kubuntiac (or Bugsbane) is busy editing the screencasts, video and audio together into a smooth recording. The results will be available on <a href="http://krita.org/component/content/article/10-news/77-second-day-of-the-third-krita-sprint">krita.org</a>. The recording will be available as soon as it is done, together with the original .kra Krita files and brush presets as provided by the artists.

<p>Fortunately, none of the artists managed to crash Krita, and we took note of all the issues they mentioned, which Pentalis entered into <a href="http://bugs.kde.org">bugs.kde.org</a>. More than sixty new issues were logged. Quite a few of those issues were fixed during the weekend and over the next week. David Revoy's conclusion was that Krita is plenty okay for creating art, but not yet quite ready for the professional user.

<p>And that set the tone for the afternoon meeting, during which we discussed the direction for Krita for the next year. This can be summed up as: polish, polish, polish. Performance is ok. Stability is pretty good, though it can be improved. But all the little speedbumps we witnessed when watching the artists work with Krita (each in their completely individual and different way) need to be smoothed out! We also conceived of another plan... Something wickedly cool we want to ask the community to support... More about that in a few days...

<h2>Saturday</h2>

<p>On Saturday, we met our benefactor, Silvio Grosso. After the runaway success of the 2009 community donation drive to enable Lukas Tvrdy to hack on Krita full-time for three months, Silvio made it possible to extend that period for several months more. He also sponsored Dmitry Kazakov to spend more hours on Krita after his Google Summer of Code project ended. It was great to meet him in Amsterdam at the sprint. We had a group photo, of course:

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -253px; width: 502px; border: 1px solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/krita_2011_sprint.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/krita_2011_sprint-embed.jpg" /></a><br />Sprint attendees</div>

<p>Apart from the group photo, Timothee Giet also made quick sketches of all attendants. <a href="http://krita.org/component/content/article/10-news/78-meet-the-gang">Meet the gang</a> through the sketchbrush of Timothee!

<h2>Sunday</h2>

<p>Sunday was a relatively quiet day. People were mostly hacking and doing small bof-like discussions. David Revoy tried once more to follow a proper <a href="http://www.davidrevoy.com/index.php?article77/color-management-soft-proofing-and-cmyk-with-floss">CMYK</a>-based workflow with Krita, this time producing a tiff file that imported perfectly in Photoshop. We originally added CMYK to Krita not from a strong belief in its usefulness, but to quiet vocal critics. We since have discovered that many artists work in CMYK in order to keep track of ink coverage and mixing during the creative process, instead of having to change their work afterwards to resolve out-of-gamut colors. There is still a ways to go here: extra single-color channels or layers would be useful, and the channel docker does not always do the right thing. 

<p>Finally, we are grateful to our host Ton Roosendaal, who allowed us to meet in the <a href="http://www.blender.org">Blender Institute</a> in Amsterdam. For <a href="http://www.davidrevoy.com/">David Revoy</a>, it was like coming home -- and for the rest of us a wonderful place to get together. One of the unexpected advantages is that the Blender Institute's doorbell never stops ringing. All weekend, game designers, Blender hackers, 2D and 3D artists visited, passed the time, and gave us the opportunity to show off Krita. Many thanks as well to <a href="http://ev.kde.org">KDE e.V.</a> for making it possible to bring so many people to Amsterdam from all over Europe, Canada and Chile.