---
title: "Calligra Sprint Lays Ground For First Release"
date:    2011-11-29
authors:
  - "Boudewijn Rempt"
slug:    calligra-sprint-lays-ground-first-release
comments:
  - subject: "Exciting"
    date: 2011-11-30
    body: "The rate of progress in Calligra is very impressive. I am really looking forward to its release.\r\n\r\nWith Calligra getting to a first release, Nepomuk's and Kontact's polishing, and big improvements in KNetworkManager (among many others I could mention), 2012 is looking like it will be quite a year for us KDE users!\r\n"
    author: ""
  - subject: "Exciting progress"
    date: 2011-12-03
    body: "The progress on Calligra is very impressive. I am really looking forward to the release.\r\n\r\nBetween Calligra, Nepomuk, Kontact, KNetworkManager, etc, 2012 is shaping up to be a great year for KDE users!\r\n"
    author: ""
  - subject: "I like this :)"
    date: 2011-12-04
    body: "<cite>we decided to have one or two additional beta releases... It's not that we don't want to release, but the first Calligra release has to be good</cite>\r\n\r\nWay to go, people, that's the right attitude! \r\n\r\nDownloading openSUSE packages of 2.3.84 (2.4 Beta 4) right now... let's see how far it really is from release-state. :)\r\n\r\n"
    author: "Vajsvarana"
  - subject: "Wonderfully exciting"
    date: 2011-12-04
    body: "As office apps are most of what desktop users actually use besides browsers, this is a critical piece of the whole of KDE, and it looks like we might finally be getting a competitive office component.\r\n\r\nI vastly prefer the KDE way of UI, and that includes the direction that Calligra is going.  It would be so nice to be able to use all-KDE apps in my everyday experience.  Besides the office software (where I mostly use a bloated Java kludge) the only non-KDE thing I use is now Google Sketchup with WINE.  But Office software is not a small aspect of use.\r\n\r\nVery glad on the insistance on quality with the first release.  First impressions on people's minds are critical.  Calligra must be consistant, functional, and reliable to make a positive impression.\r\n\r\nI will be very happy to wait for something good."
    author: ""
  - subject: "kenny"
    date: 2011-12-05
    body: "we decided to have one or two additional beta releases... It's not that we don't want to release, "
    author: ""
  - subject: "do was may as you need"
    date: 2011-12-05
    body: "I reckon you do as many beta and RC releases as think you need. I am very exited to try the final release but would rather the dev's take their time and do what they think is right rather than trying to push a release out as fast as possible."
    author: "NaX"
  - subject: "mmm"
    date: 2011-12-05
    body: "I have done just a few tests so far, but I think taking the time for some more bug-squashing has been a good idea. ;)\r\nWill report some of them on bugzilla. Let's hope it's appreciated, this time.\r\nMaybe there is some packaging error on openSUSE RPMs, too."
    author: "Vajsvarana"
---
<p>In the weekend of November 11, more than twenty Calligra developers braved the fierce weather of Helsinki to meet up again! Well, the weather wasn't that fierce actually, but it could have been! We met up in the office tower that houses Nokia's research department during the weekdays for two days of hacking, presentations and meetings. For several attendants it was their first travel to a community gathering: welcome Smit Patel, Brijesh Patel and Dimitrios Tanis! 
<!--break-->
<div style="position: relative; left: 50%; padding: 1ex; margin-left: -250px; width: 500px; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/calligra_2011_2_group_foto_small.jpeg" /><br />Calligra Sprint attendees</div>
 
<p>Some highlights: the Saturday morning meeting where we decided to have one or two additional beta releases -- and the same day our Release Dude (who couldn't be present, unfortunately) came up with the same idea. It's not that we don't want to release, but the first Calligra release has to be good! 
 
<p>Saturday afternoon, <a href="http://www.skf.com">SKF</a> gave a remote presentation on how they are using Calligra Words as a report generator/editor in their tool. Their usage is very advanced, it includes RDF, for instance, and the presentation was a huge success. It's always great to see the results of your project in actual use! 
 
<p>A similar presentation was given by Nokia the next day: we were shown how well suited the Calligra office engine is to power the Harmattan Office application on Nokia's smartphone, the N9. This being a developer meeting, a slide that showed that we're doing better than Microsoft's mobile office was followed by four slides of Highly Important Bugs in what we can improve to make Calligra even better! 
 
<p>But that was great as well -- as great as the lunches and dinner Nokia provided for all of us. Thanks go to Nokia and KO GmbH for sponsoring travel and accommodation, and Nokia for sponsoring food and drink for the whole group! Thanks also to Claudia Rauch of KDE e.V. for helping with getting help on the organisational thinks like visas. 
 
<p>And now everyone is home, it's time make our code ready for the long awaited first release of Calligra, Calligra 2.4