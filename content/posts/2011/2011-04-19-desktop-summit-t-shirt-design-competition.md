---
title: "Desktop Summit T-shirt Design Competition"
date:    2011-04-19
authors:
  - "sealne"
slug:    desktop-summit-t-shirt-design-competition
comments:
  - subject: "I rather like the one with"
    date: 2011-04-19
    body: "I rather like the one with the speech bubbles! If nobody speaks up to design something you can always use that :-)"
    author: "irina"
---
<div style="float: right; padding: 1ex; margin: 1ex"><a href="https://www.desktopsummit.org/tshirt"><img src="/sites/dot.kde.org/files/desktopsummit-tshirt-contest.png" width=260 height=300 /></a></div>
<p>The <a href="https://www.desktopsummit.org/tshirt">T-shirt Design Competition</a> for the Desktop Summit has just opened. We are looking for designs that go beyond your typical conference shirt which finds its final resting place in the closet or drawer once you have returned home. The winning design should reflect the passion and energy of the Free Desktop communities that The Desktop Summit represents.</p>
<!--break-->
<p>The <a href="https://www.desktopsummit.org/">Desktop Summit</a>, a joint conference organised by the GNOME and KDE communities, is happening in Berlin from the 6th to 12th August. It is free to attend but you need to <a href="https://www.desktopsummit.org/register">register</a>.</p>

<p>If you are attending you may want to subscribe to the <a href="https://mail.kde.org/mailman/listinfo/ds-discuss">discussion list</a> to talk to others who are going, we also have an IRC channel on freenode #desktopsummit</p>