---
title: "When Will You Join the Game?"
date:    2011-01-11
authors:
  - "agata"
slug:    when-will-you-join-game
comments:
  - subject: "Interesting interview"
    date: 2011-01-14
    body: "It's nice to hear about why people choose to support KDE - many thanks to Paul and the 125+ others like him who help KDE and our software to exist."
    author: "Stuart Jarvis"
---
Back in June, KDE e.V. <a href="http://dot.kde.org/2010/06/07/join-kde-game-linuxtag-2010">launched its individual supporting membership program</a>, asking everyone to <a href="http://jointhegame.kde.org/">Join the Game</a>.

There are many good reasons to support KDE with a regular financial contribution - it enables <a href="http://ev.kde.org/">KDE e.V.</a> to have a predictable and stable income. That can be used to plan support for contributors and events that help speed up development of KDE software, enhance our promotion efforts and help grow our community. However, our contributors and users are scattered throughout the world and have many different backgrounds and their reasons for contributing are likely to be just as diverse. We caught up with our 125th supporting member, <b>Paul Eggleton</b> to ask him why he Joined the Game.
<!--break-->

<div style="width: 352px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/jtg.jpg" /><br />Every supporting member is represented by a playing piece at KDE e.V.'s Berlin office</div><b>Agata:</b> Hi Paul, can you introduce yourself to our readers?

<b>Paul:</b> I'm Paul Eggleton, originally from New Zealand but now living in the UK. I'm 31 and work as an Embedded Linux Engineer as part of Intel's Open Source Technology Centre (OTC) on the <a href="http://yoctoproject.org/">Yocto project</a>. Hobbies include working on open source / free software projects (naturally!), travel, movies, etc.


<b>Agata:</b> Why did you decide to Join the Game?

<b>Paul:</b> As a heavy user of the KDE desktop it's an important thing to me to contribute something back if I can. Although I'm a developer, at this precise moment I don't have much time to contribute to KDE directly so I hope this will suffice for now. I am very keen to contribute code to KDE in the future however.

<b>Agata:</b> How did you find out about the KDE e.V. 'Join the Game' individual supporting membership program?

<b>Paul:</b> Mainly from blog posts about the program via the <a href="http://planetkde.org/">Planet KDE</a> website.

<b>Agata:</b> Do you use KDE software? If yes, for how long?

<b>Paul:</b> Yes, I've been a KDE software user since the early 3.x days.

<b>Agata:</b> How did you learn about KDE and Free Software?

<b>Paul:</b> My first Linux distribution was Mandrake (now Mandriva) which featured KDE software prominently. Use of KDE software and KDevelop as a development platform and for a small project I was working on at the time was the main reason I started to use it seriously.

<b>Agata:</b> Which KDE applications do you like the most or which are useful for you? Which should be improved?

<b>Paul:</b> The ones I use the most are Dolphin, Konsole, Konversation and Kate. I also use Kontact every day, both at work and at home, so I'm interested in the new upcoming KDE PIM suite. For other improvements, I guess I would like to see Nepomuk, Akonadi and some of the other "pillars" of KDE Platform 4 more smoothly integrated with KDE applications, in a way that is not too intrusive or unfriendly to users. I've no doubt that this is being worked on as we speak. Long term I am also interested to see what happens with Konqueror and the Calligra suite.
Broadly, the thing that most interests me about KDE is how the various components and applications integrate together. If the level of integration can increase without too many hard dependencies being created, then the desktop experience can be enriched even further.

<b>Agata:</b> Is the idea of 'software freedom to everyone' very important for you? Are you identifying yourself with this philosophy?

<b>Paul:</b> Yes, definitely. I think it's amazing that so many people from different countries can collaborate over the internet to produce such a wide range of free / open source software; and in the reverse, without the free / open source software ecosystem we would not have the internet we have today.

<b>Agata:</b> Can you describe your role or an impact as a KDE e.V. player (supporting member) in the Game?

<b>Paul:</b> Well, I'm not sure. I hope that my participation helps to enable KDE development as a whole, no matter what it is used for.

<b>Agata:</b> Would you like to discover who are other supporters with whom you play the KDE Game? Perhaps by meeting some at our annual meeting, Akademy?

<b>Paul:</b> Sure! I hope to attend Akademy at some point in the near future, and if I was there I would certainly attend a meeting of Game participants.

<b>Agata:</b> We provide reports about KDE's activities first hand for our members. Would you like to learn more about KDE e.V. support for KDE Community?

<b>Paul:</b> Definitely. I think seeing the results of contributions strengthens people's interest in continued contribution (well, at least it does for me).

<b>Agata:</b> Do you have any ideas how KDE e.V. could promote better the 'Join the Game' program?

<b>Paul:</b> I suspect this Dot article will help a lot. Perhaps a small button could be added to the Dot and any other KDE websites linking to the 'Join the Game' website?