---
title: "KDE Commit Digest for 24 April 2011"
date:    2011-04-29
authors:
  - "vladislavb"
slug:    kde-commit-digest-24-april-2011
---
In <a href="http://commit-digest.org/issues/2011-04-24/">this week's KDE Commit-Digest</a>:

<ul>
<li>Bluetooth tethering support and much bugfixing seen throughout <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a></li>
<li>Further work on user management and public link sharing in <a href="http://owncloud.org/">OwnCloud</a> amongst refactoring of the installation form layout, the media player and general bugfixing</li>
<!--break-->
<li><a href="http://www.calligra-suite.org/">Calligra</a> sees further work on <a href="http://www.calligra-suite.org/kexi/">Kexi</a>'s Modern Menu, encoding autodetection with the ascii import filter and docx support among further bugfixing</li>
<li>KPresenter renamed <a href="http://www.calligra-suite.org/stage/">Calligra Stage</a></li>
<li>Improvements to tagging in <a href="http://www.digikam.org/">Digikam</a> among other bugfixing </li>
<li>Work on MKV subtitle support in <a href="https://projects.kde.org/projects/kdesupport/phonon/phonon-vlc">Phonon-VLC</a></li>
<li>Work on 3d Scene support in <a href="http://edu.kde.org/step/">Step</a></li>
<li>Session support added for keyboard layouts in Kxkb</li>
<li>Grub2 support added to <a href="http://en.wikipedia.org/wiki/KDE_Display_Manager">KDM</a></li>
<li>Support for RightToLeft in <a href="http://userbase.kde.org/System_Settings">SystemSettings</a></li>
<li><a href="http://zeitgeist-project.com/">Zeitgeist</a> support added to <a href="http://amarok.kde.org/">Amarok</a> through <a href="http://phonon.kde.org/">Phonon</a></li>
<li><a href="http://ktorrent.org/">KTorrent</a> sees a revamp of its shutdown plugin along with crash fixes</li>
<li>Initial work on a task editor in the <a href="http://www.rememberthemilk.com/">RememberTheMilk</a> Plasma addon</li>
<li>Optimizations and other work in <a href="http://techbase.kde.org/Projects/Plasma/PlasMate">Plasmate</a></li>
<li><a href="http://kile.sourceforge.net/">Kile</a> sees further work and bugfixing</li>
<li>Work on the intuitiveness of <a href="http://api.kde.org/4.0-api/kdelibs-apidocs/kio/html/classKFileDialog.html#_details">KFileDialog</a>'s filtering</li>
<li><a href="http://www.kde.org/applications/multimedia/kmix/">KMix</a>'s icon now turns to "muted" when at zero volume</li>
<li>Further work in <a href="http://www.oxygen-icons.org/">Oxygen</a> and <a href="http://kde-look.org/content/show.php/?content=136216">Oxygen-GTK</a></li>
<li>Bugfixing in <a href="http://en.wikipedia.org/wiki/KIO">KIO</a>, <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://plasma.kde.org/">Plasma</a> and its <a href="http://techbase.kde.org/Projects/Plasma/Plasmoids">addons</a>, <a href="http://userbase.kde.org/KWin">KWin</a> and <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-04-24/">Read the rest of the Digest here</a>.