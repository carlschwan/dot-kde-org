---
title: "KDE Commit Digest for 13 March"
date:    2011-03-23
authors:
  - "vladislavb"
slug:    kde-commit-digest-13-march
comments:
  - subject: "An interview with Sven Brauch too!"
    date: 2011-03-23
    body: "Dear Commit-Digest readers, \r\n\r\nI forgot to mention above that this digest issue includes an interview with Sven Brauch who introduces his work on Python support in KDevelop.\r\n\r\nEnjoy!"
    author: "blantonv"
  - subject: "LongBug champion!"
    date: 2011-03-23
    body: "I really enjoy going through whole commits and find the oldest-bug which is being fixed.\r\n\r\nSince this week, im going to share it with you:\r\n\r\nThis week's champion is Martin Gr\u00e4\u00dflin, for fixing bug 192476 which is near 5 years old!"
    author: "emilsedgh"
  - subject: "I presume you meant"
    date: 2011-03-23
    body: "Good job, but I presume you meant bug 129476?\r\n\r\nhttp://bugs.kde.org/show_bug.cgi?id=129476"
    author: "bluelightning"
  - subject: "Oh, right, sorry!\nThis"
    date: 2011-03-23
    body: "Oh, right, sorry!\r\nThis project is still in beta phase :p"
    author: "emilsedgh"
  - subject: "seems like.."
    date: 2011-03-24
    body: "If this is your passion, you are both welcome to come help out with the digest. :)\r\n\r\nhttp://enzyme.commit-digest.org/"
    author: "blantonv"
---
In <a href="http://commit-digest.org/issues/2011-03-13/">this week's KDE Commit-Digest</a>:

              <ul><li>Improved support for DOC, ODF, and Google Docs along with general bugfixing in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Work in <a href="http://sourceforge.net/p/necessitas/home/">Necessitas</a> on the unofficial Android port of Qt and Qt-Creator including making the Maemo support more generic and a JAVA implementation for QT Image Capture</li>
<li>Major work on <a href="http://thomasmcguire.wordpress.com/2011/02/27/facebook-support-in-kdepim/">Akonadi-Facebook</a></li>
<li>Marble integration in <a href="http://userbase.kde.org/KAddressBook/index">KAddressbook</a> and beginning of reverse geocoding support</li>
<li>Implementation of a new memory-bounded pooler thread in <a href="http://krita.org/">Krita</a></li>
<li>Work throughout Phonon including a DVD menu navigation interface and improvements to state handling in the Gstreamer backend</li>
<li>Series of crash related bug fixes in <a href="http://rekonq.kde.org/">Rekonq</a></li>
<li>Work on autocompletion in KDev-Python and general bugfixing in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Implementation of one click operations for all selected images in Digikam</li>
<li>Work in Plasma-Mobile including new data engines and mobile phone applets</li>
<li>Further work on Oxygen-GTK and new Oxygen icons</li>
<li>Feature work and bugfixing in <a href="http://skrooge.org/">Skrooge</a></li>
<li>Default keyboard shortcuts now synced between Konsole and <a href="http://yakuake.kde.org/">Yakuake</a></li>
<li>Integration of KSpeech to speak computer's moves in Knights</li>
<li>Search modes added to the Search Panel in Dolphin</li>
<li>Optimizations and refactoring in Kdesrc-Build</li>
<li>General bugfixing in <a href="http://gluon.gamingfreedom.org/">Gluon</a>, KDE-PIM, <a href="http://konversation.kde.org/">Konversation</a>, libksane, KIO, <a href="http://kst-plot.kde.org/">Kst</a>, and throughout Kdelibs and Kde-Workspace.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-03-13/">Read the rest of the Digest here</a>.