---
title: "KDE Commit Digest for 17 April 2011"
date:    2011-04-22
authors:
  - "vladislavb"
slug:    kde-commit-digest-17-april-2011
---
In <a href="http://commit-digest.org/issues/2011-04-17/">this week's KDE Commit-Digest</a>:

              <ul><li><a href="http://www.calligra-suite.org/">Calligra</a> sees further work on modern menu, pdf export, soft page breaks, autoshapes support, and vertical alignment support amongst continued bugfixing and optimization</li>
<li><a href="http://extragear.kde.org/apps/kmldonkey/">KMLDonkey</a> fully ported to KDE4 / Qt4, removing all Qt3 dependencies</li>
<li>Sound support added for 5 new languages in <a href="http://edu.kde.org/applications/all/klettres/">KLettres</a></li>
<li><a href="http://kate-editor.org/">Kate</a> implements movable tabs, import from vimrc files, and updates Wesnoth Markup Language and RPM Spec syntax highlighting</li>
<li>Initial implementation of new user management and sharing with public link support in <a href="http://owncloud.org/">OwnCloud</a></li>
<li>Adblock and autoscroll improvements in <a href="http://rekonq.kde.org/">Rekonq</a></li>
<li>Backend support introduced to <a href="http://community.kde.org/KDE_Games/Tagaro">Tagaro</a></li>
<li>System-wide connection support added to <a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> 0.8</li>
<li>Work on new activity switcher in <a href="http://community.kde.org/Plasma/Active/Contour">Contour</a></li>
<li>New game draft "JumpnBump" in <a href="http://gluon.gamingfreedom.org/">Gluon</a></li>
<li>Bugfixing and a new planetary nebula texture in <a href="http://edu.kde.org/kstars/">KStars</a></li>
<li>Work on <a href="http://community.kde.org/Plasma/Plasma-Mobile">Plasma-Mobile</a></li>
<li>Bugfixing throughout <a href="http://pim.kde.org/">KDE-PIM</a>, <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://userbase.kde.org/Lokalize">Lokalize</a>, <a href="http://edu.kde.org/marble/">Marble</a>, <a href="http://konsole.kde.org/">Konsole</a>, <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://kopete.kde.org/">Kopete</a>, <a href="http://dolphin.kde.org/">Dolphin</a>, and the <a href="http://sourceforge.net/projects/kcm-grub2/">Grub2</a> KControlModule.</li>
</ul>

                <a href="http://commit-digest.org/issues/2011-04-17/">Read the rest of the Digest here</a>.