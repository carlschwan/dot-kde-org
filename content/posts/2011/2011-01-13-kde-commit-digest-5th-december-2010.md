---
title: "KDE Commit-Digest for 5th December 2010"
date:    2011-01-13
authors:
  - "dannya"
slug:    kde-commit-digest-5th-december-2010
comments:
  - subject: "Dangit, where's the Like"
    date: 2011-01-13
    body: "Dangit, where's the Like button?"
    author: "Bille"
---
In <a href="http://commit-digest.org/issues/2010-12-05/">this week's KDE Commit-Digest</a>:

<ul>
<li>Various work on <a href="http://userbase.kde.org/Lokalize">Lokalize</a>, the computer-aided translation system</li>
<li>Fixes and updates to <a href="http://edu.kde.org/marble/">Marble</a></li>
<li><a href="http://vizzzion.org/?blogentry=904">Crystal</a>, the new desktop search applet has seen major fixups</li>
<li>Major bugfixing throughout <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>Various bugfixing and feature work in <a href="http://www.calligra-suite.org/">KOffice</a>, including work on multithreading with ThreadWeaver and <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> optimizations</li>
<li>Major work on the <a href="http://jontheechidna.wordpress.com/2010/12/04/muon-is-now-the-muon-package-management-suite/">Muon</a> package manager, including a GUI for searching and transaction cancelling</li>
<li>Various optimizations and work on a new RAW import tool in <a href="http://www.digikam.org/">Digikam</a></li>
<li>Porting to the new <a href="http://drfav.wordpress.com/2010/10/03/the-new-kde-power-management-system-fresh-from-madrid/">KDE Power Management System</a> across the board</li>
<li>A new "Recent Documents" <a href="http://plasma.kde.org/">Plasma</a> dataengine committed</li>
<li>Internal work on NetworkManagement libraries and the NetworkManager backend</li>
<li>Work on supporting Windows CE (mobile) in KDE-PIM, <a href="http://solid.kde.org/">Solid</a>, kdelibs, and other places</li>
<li>Work on <a href="http://skrooge.org/">Skrooge</a></li>
<li>Nuno Pinheiro continues his massive updating of the <a href="http://oxygen-icons.org/">Oxygen</a> icons (48x48 sizes this time).</li>
</ul>

<a href="http://commit-digest.org/issues/2010-12-05/">Read the rest of the Digest here</a>.
<!--break-->
