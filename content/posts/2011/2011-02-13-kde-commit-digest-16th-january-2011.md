---
title: "KDE Commit-Digest for 16th January 2011"
date:    2011-02-13
authors:
  - "dannya"
slug:    kde-commit-digest-16th-january-2011
comments:
  - subject: "Project management standards"
    date: 2011-02-25
    body: "Project management standards are used worldwide in all industries and in all disciplines, and project managers are in demand. The Project Management Online Training Program will help you improve your knowledge and skills in project management and prepare you for the next step in your professional development, including certification as a Project Management Professional. This online certificate program is offered in partnership with major colleges, universities, and other accredited education providers.<a href=\"http://www.pmpcertificationtips.com/\" rel=\"\">PMP certification</a>"
    author: "Bilnnyee"
---
In <a href="http://commit-digest.org/issues/2011-01-16/">this week's KDE Commit-Digest</a>:

<ul>
<li>This week, the KDE developers are again heavily focusing on readying the soon-to-be-released <a href="http://www.kde.org/announcements/4.6/">KDE Software Compilation 4.6</a></li>
<li>Bug fixes can be found throughout the stack and in a wide range of applications</li>
<li>Many bug fixes are going into extragear applications such as <a href="http://amarok.kde.org/">Amarok</a>, the <a href="https://projects.kde.org/projects/extragear/kdevelop/kdevplatform">KDE Development stack</a>, and <a href="http://www.kdevelop.org/">KDevelop</a> itself, as well as into <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a> and the KDE <a href="http://www.koffice.org/">office</a> <a href="http://www.calligra-suite.org/">suites</a></li>
<li>There are also a number of optimizations in various areas</li>
<li>Few and rather small features are added to else where in KDE software, mainly in <a href="http://edu.kde.org/marble/">Marble</a>, <a href="http://www.calligra-suite.org/">Calligra</a>, and the <a href="http://www.kde.org/applications/graphics/">graphics applications</a></li>
<li><a href="http://www.rsibreak.org/">RSIBreak</a> is preparing for release.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-01-16/">Read the rest of the Digest here</a>.
<!--break-->
<!--break-->
