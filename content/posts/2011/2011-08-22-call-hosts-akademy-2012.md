---
title: "Call for Hosts for Akademy 2012"
date:    2011-08-22
authors:
  - "Stuart Jarvis"
slug:    call-hosts-akademy-2012
---
While the heat of the Desktop Summit 2011 is still remaining, we are already looking for a host for Akademy 2012.

Akademy is the annual gathering of the KDE community, one of the largest and most significant in the world of Free and Open Source Software. At Akademy, the KDE community gathers to exchange ideas for development and to discuss other important topics. It is an extraordinary occasion for creativity, enthusiasm, commitment, close working relationships and innovation, so hosting Akademy is a rare opportunity. 

As many have realized, hosting an event such as the Desktop Summit or Akademy is an excellent opportunity to get actively involved within the KDE community and externally, and earn visibility as well.

<h2>Akademy</h2>
The conference program is usually structured as follows:
<ul>
<li>Friday is for travel, arrival and pre-registration by most attendees.</li> 
<li>On Saturday and Sunday, the actual conference takes place. 2 or 3 keynotes--which are available to all participants--alternate with two concurrent tracks that the attendees can choose between.</li>
<li>The remainder of the week is used for coding and workshops for smaller groups of 10 to 50 people.</li>
</ul> 
The venue needs to offer a lecture room that fits 400+ people, a smaller lecture room that fits up to 200 people, and several smaller rooms for workshops.

<h2>Host Requirements</h2>
Akademy requires a location in or near Europe that is easy to reach, preferably close to an international airport. The conference venue itself should be equipped to host the attendees comfortably, and provide reliable, high speed Internet access through wifi and Ethernet during the conference, workshops and hacking sessions. Hotel accommodations and places to eat should be close to the venue or easily reachable.

Organizing an Akademy is a demanding activity and requires a significant investment of time and effort. While there will be considerable assistance from people who have experience producing previous Akademies, the local team should be prepared to spend a lot of time on it. Previous hosting teams have found this to be a rewarding project which is highly recognized and appreciated by the KDE community. The benefits to KDE, the attendees and the Akademy hosts and organizers are immeasurable.

For more detailed information, please take a look at the <a href="http://ev.kde.org/akademy/CallforHosts_2012.pdf">Call for Hosts (pdf)</a>. In case of questions and concerns, please contact the Board of KDE e.V. at kde-ev-board@kde.org. <b>Applications can be submitted until October 1st, 2011</b>.
