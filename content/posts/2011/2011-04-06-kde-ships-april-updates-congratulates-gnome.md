---
title: "KDE Ships April Updates, Congratulates GNOME"
date:    2011-04-06
authors:
  - "sebas"
slug:    kde-ships-april-updates-congratulates-gnome
comments:
  - subject: "Yeah"
    date: 2011-04-06
    body: "Congrats to both GNOME and KDE communities ! :)"
    author: "gaboo"
  - subject: "\"ConGrats\", ..."
    date: 2011-04-06
    body: "... surely? :)"
    author: "SSJ_GZ"
  - subject: "erm..."
    date: 2011-04-06
    body: "The opensuse one-click installer for 4.6.2 is still called KDE 4.5 Base Installation..."
    author: "Boudewijn Rempt"
  - subject: "Why there is no KDE PIM included?"
    date: 2011-04-06
    body: "I do always look forward to new KDE release, but this time I am a bit disappointed. After all I read about KDE PIM being expected to be released in KDE 4.6.2, I do not understand why:\r\n1) it is not included\r\n2) there is no explanation why it is not included\r\n\r\nThis is so confusing I have created an account here just to leave a note about it. Will anyone answer?"
    author: "qpal"
  - subject: "Where are the 1-click installers?"
    date: 2011-04-06
    body: "Where are those? I'd expect them to be listed in the <a href=http://en.opensuse.org/KDE_repositories>KDE Repositories</a> or <a href=http://en.opensuse.org/Portal:KDE>KDE Portal</a> pages, but there is nothing there. "
    author: "jadrian"
  - subject: "they are waiting until Duke"
    date: 2011-04-06
    body: "they are waiting until Duke Nukem 3D gets released. Have patience!"
    author: "trixl."
  - subject: "lol, good one!"
    date: 2011-04-06
    body: "but now i would really appreciate someone from community around KDE PIM to share any info about current state and possible near future of PIM.."
    author: "qpal"
  - subject: "Fixed, thanks"
    date: 2011-04-06
    body: "Looks like nobody updated those patterns in a while - they are sort of isolated from the distro development workflow."
    author: "Bille"
  - subject: "1-click installers"
    date: 2011-04-06
    body: "...are the .ymp files in the root of each repo., eg here: http://download.opensuse.org/repositories/KDE:/Distro:/Factory/openSUSE_11.4/\r\n\r\nHaving seen the state of the filelists in those patterns I want to get someone to review them thoroughly before linking them from the wiki.  They were linked from the old openSUSE wiki.\r\n\r\nYou can list them with \"osc meta pattern KDE:Distro:Factory\" and view their sources with \"osc meta pattern KDE:Distro:Factory <PATTERN-NAME>\" - send updates to me if you want to help out."
    author: "Bille"
  - subject: ".."
    date: 2011-04-06
    body: ".."
    author: "eean"
  - subject: "http://www.kde.org/info/4.6.2.php#binary"
    date: 2011-04-06
    body: "I got them from http://www.kde.org/info/4.6.2.php#binary"
    author: "Boudewijn Rempt"
  - subject: "June 14th!"
    date: 2011-04-07
    body: "Are you talking about Duke Nukem Forever (June 14th)? it would be a neat release date for KDE-PIM/Akonadi.\r\n\r\nPlease, guys. I've been using GIT. I know the current status of KDE-PIM. And it deserves another beta release in its current status."
    author: "Alejandro Nova"
  - subject: "KDE and Gnome"
    date: 2011-04-07
    body: "Hola, just an idea (probably mentioned already a 100 times, but anyway):\r\n\r\nSince the Gnomes want to introduce the Zeitgeist framework for organising files and tasks in new ways, wouldn't it be good a opportunity to combine forces on the low level: search engine, tagging, standard dialog (FileOpenSave, print) ... ?\r\n\r\nIs there some discussion about this going on between both communities?\r\n"
    author: "spitzkopf"
  - subject: "Konsole"
    date: 2011-04-08
    body: "Hey!\r\n\r\nI'm just wondering if Konsole is going to get fixed anytime soon so we can close tabs again.  Typing \"exit\" 10+ times just for one session is really getting to me.\r\n\r\n\r\nThank you.\r\n\r\n\r\nM."
    author: "Xanadu"
  - subject: "Works for me"
    date: 2011-04-08
    body: "When I press ctrl-shift-w, the konsole tab closes."
    author: "Boudewijn Rempt"
  - subject: "I cannot edit text files by konqueror."
    date: 2011-04-08
    body: "I highlighted this issue in bugs.kde.org. But none did anything  :(   \r\n\r\nThe konqueror in KDE 4.0.0 was able to edit and save text files just by right clicking and selecting \"preview in Embedded Advanced Text Editor\".\r\n\r\nBut this feature was removed in KDE 4.6.1 and  4.6.2 ( https://bugs.kde.org/show_bug.cgi?id=259338 )\r\n\r\nWhy, why, why ????\r\n\r\nThe abilty of konqueror to edit text files is one of the main reasons that i use KDE instead of GNOME."
    author: "metosa"
  - subject: "> When I press ctrl-shift-w,"
    date: 2011-04-08
    body: "> When I press ctrl-shift-w, the konsole tab closes.\r\n\r\nAh!  Thank you!  All these years and I didn't know that shortcut sequence.  Geesh...\r\n\r\nThanks, man.\r\n\r\n\r\nM."
    author: "Xanadu"
  - subject: "Cry Havoc"
    date: 2011-04-09
    body: "Wow. I't been a busy night here ever since yesterday 19:30 GMT, when Mandriva rolled out this infamous update. I saw it pop up in the taskbar, all the main KDE4 libs were there, that was quite impressive, I read the changelog, it was all about security, and unification of some databases, oh and something about RSA certificates not read/written in the right places. Bah. I clicked \"update\", and entered a world of \"rlah\" as we say here.\r\n\r\nThe first visual clue that something went wrong was a weird cloud of pixels surrounding my cursor. Sharp, mean dead pixels like on a faulty monitor. Then the said cloud started to expand to the taskbar region and window titlebars, like a nasty fungus.\r\n\r\nI blamed and trial tested everybody but KDE. This looked so much like a HW bug that the fact that the KDE update could have caused this mess did'nt even cross my mind.\r\n\r\nI tested every RAM unit, every graphic card that I have at hand (I happen to have quite a lot, Intel ATI Nvidia younameit) and discovered (loong story short) that my current monitor (a 17' Dell LCD) is dead. Hum.\r\n\r\nRight now I digged out an old 14' Compaq LCD, I'm logged under LXDE w/o any glitch at all, and whenever I try to lauch KDE4, with whatever HW setup, the mean pixel cloud comes back...\r\n\r\nDon't get me wrong, I *love* KDE, yeah, that is love, that must be love, you see ;)\r\n\r\nOh, and while I'm here, I wouldlike to thank every possible contributor in the KDE team. I use you wonderful work ever since the first versions, and I'm impressed by your ambition. Keep up the good work !"
    author: "xaccrocheur"
  - subject: "Huh?"
    date: 2011-04-11
    body: "Ctrl+D doesn't exit your shell?"
    author: "kolla"
  - subject: "Fantastic!"
    date: 2011-04-14
    body: "I checked now and there are 2 good news.\r\n\r\n1. There is a KDE-PIM/Akonadi blocker bug. After all those bugs are gone, KDE-PIM/Akonadi is released.\r\n2. There is a KDE-PIM/Akonadi (4.6) beta 5 release! Grab it through your preferred unstable repository. I, as a daily user of KDE-PIM/Akonadi master, can say you: the KDE-PIM/Akonadi you'll get now is infinitely better than KDE-PIM/Akonadi beta 4.\r\n\r\nGrab it while it's hot, and report any loss of data you have converting the data from your old KDE-PIM. "
    author: "Alejandro Nova"
  - subject: "> Ctrl+D doesn't exit your"
    date: 2011-04-18
    body: "> Ctrl+D doesn't exit your shell?\r\n\r\nWow.  I feel *very* stupid now.  Yes, it indeed does.  I can't sit here and think of a reason why I've never tried that in Konsole.  I've been using KDE since just before 1.0 somewhere (I think it was 0.9.*, but could've been 0.8.*).\r\n\r\n:-\\\r\n\r\n\r\nThank you.  \r\n\r\n\r\nM."
    author: "Xanadu"
---
KDE has <a href="http://www.kde.org/announcements/announce-4.6.2.php">made available</a> an update to the 4.6 series of the Desktop and Netbook workspaces, the KDE applications, and the development frameworks. The <a href="http://www.kde.org/announcements/changelogs/changelog4_6_1to4_6_2.php">changelog</a> tells us that this update is worth installing for the fixes in Okular, Dolphin and Kopete, among others. 4.6.2 is a recommended update for everyone running 4.6.1 or earlier versions. As this update does not contain new features, it is a safe update for everyone. 4.6.2 marks the 36th stable release since 4.0.0 was released more than three years ago.

The KDE team has chosen the codename "Congrats" to pay respect to their friendly competition in the GNOME camp, who release a major new version of their offering today.
<!--break-->
