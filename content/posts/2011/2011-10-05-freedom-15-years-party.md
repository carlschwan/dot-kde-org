---
title: "Freedom. 15 years. Party!"
date:    2011-10-05
authors:
  - "kallecarl"
slug:    freedom-15-years-party
comments:
  - subject: "Kool!"
    date: 2011-10-06
    body: "Go CDE! ;P"
    author: ""
  - subject: "Great to know"
    date: 2011-10-06
    body: "We might try to spark some KDE magic in Cancun. :)\r\nWill keep ya'll posted."
    author: ""
  - subject: "Ultimately Fantastic!"
    date: 2011-10-06
    body: "I first saw it somewhere about the time of KDE 1.45 with FreeBSD. Initially I was intrigued, played with it a little on and off. By KDE 2.1.x I became hooked! Been using it so long that now when I sit in front a $MS Windows machine I look at it and feel vaguely lost. I have to say it, but now I am ultimately a KDE fanatic. Keep up the great KDE! "
    author: ""
  - subject: "Happy Anniversary everyone"
    date: 2011-10-06
    body: "Happy Anniversary everyone who codes and everyone who uses KDE, you are the Koolest!"
    author: ""
  - subject: "Great!!!"
    date: 2011-10-10
    body: "You can make the kde-15-years logo in high resolution?\r\nI would like to make some shirts for my local community.\r\n\r\nthanks!"
    author: "astdarkness"
  - subject: "Here's to another awesome 15"
    date: 2011-10-11
    body: "Here's to another awesome 15 years!"
    author: "SimoneDavenport"
  - subject: "thanks"
    date: 2011-10-12
    body: "You have done a great job all this years."
    author: ""
  - subject: "Thanks for the greatest X environment ever done"
    date: 2011-10-12
    body: "Thanks for everything :)"
    author: ""
  - subject: "Simplemente gracias"
    date: 2011-10-12
    body: "Simplemente gracias :)\r\n\r\nGreetings from spain."
    author: ""
  - subject: "KDE 15 Years:from linuxarrow"
    date: 2011-10-15
    body: "Using the KDE 3.5.10 on Pardus Kurumsal2..have old hardware and am amazed how crisp and clear the 3.5 still is...fantastic engineering...built to last. Love the newer KDE too when I had a better machine. KDE is still the best of all. Happy Birthday from Canada. WooHoo!  "
    author: ""
  - subject: "Hey, how to patch KDE 3 for"
    date: 2011-10-15
    body: "Hey, how to patch KDE 3 for FreeBSD?"
    author: ""
  - subject: "getting KDE assistance"
    date: 2011-10-16
    body: "http://forum.kde.org/ works better than comments here.\r\n\r\n"
    author: "kallecarl"
  - subject: "From Colombia, Thnks"
    date: 2011-10-16
    body: "Est\u00e1 reeebuenoooo :D  \"Very Koooool\"\r\nGracias"
    author: ""
  - subject: "Gracias"
    date: 2011-10-16
    body: "From Colombia With Thanks"
    author: ""
  - subject: "Russia"
    date: 2011-10-21
    body: "\u041f\u043e\u0437\u0434\u0440\u0430\u0432\u043b\u044f\u044e \u043e\u0442 \u0434\u0443\u0448\u0438!!!!!!!"
    author: ""
  - subject: "Celebration!"
    date: 2011-10-22
    body: "How we celebrated - we being Rohan Garg, Supreet Pal Singh, Karan Singh, and Valorie Zimmerman -- we wrote a book for KDE! We'll be blogging and writing more about it in the coming days, and we want collaborators and fellow celebrators!"
    author: ""
  - subject: "May be, \"From Kolumbia\" ? :)"
    date: 2011-10-30
    body: "May be, \"From <strong>K</strong>olumbia\" ? :)"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-15-years7200.png"" /></div>

On October 14, 1996, Matthias Ettrich posted a message to Usenet <a href="http://groups.google.com/group/comp.os.linux.misc/tree/browse_frm/thread/4836e257721ab7fb/cb4b2d67ffc3ffce?rnum=1&_done=%2Fgroup%2Fcomp.os.linux.misc%2Fbrowse_frm%2Fthread%2F4836e257721ab7fb%2F10e10a7a9e08943b%3Flnk%3Dgst%26#doc_cb4b2d67ffc3ffce">(comp.os.linux.misc)</a>, announcing "Programmers wanted!" for a "New Project: Kool Desktop Environment (KDE)". Now, 15 years later, Matthias’s <a href="http://kde.org/announcements/announcement.php">dream of a "GUI for an ENDUSER"</a> has been fully realized.  And more. Sometime in the next 2 weeks, Plasma Active One will be released, extending Matthias's plan for a "modern interface" with a "common look & feel" to a range of enduser devices from desktop to mobile. KDE has grown beyond a Linux Desktop GUI to a global community of people. But it still has the same innovative spirit and a commitment to serving users of Free and Open Source Software.
<!--break-->
<h2>This calls for celebration!</h2>
We’re having a global 15th birthday party for KDE. And you’re invited! The entire KDE community can’t get together in person. So we’re partying virtually. All day, all around the world, on October 14th. 

Help make it a <big>BIG EVENT</big>:
<ul>
<li>Have a party</li>
<li>With a KDE birthday cake!</li>
<li>Celebrate your commitment to freedom with Kool photos</li>
<li>Go big like they're <a href="http://kde-espana.es/news.php#itemKDEcumple15aoshabrquecelebrarlo">doing in Spain</a></li>
<li>Bring the KDE community to your party with status updates and other social media</li>
<li>Email your plans, photos and event happenings to 15@kde.org</li>
<li>Volunteer to translate the October 14th and follow up Dot stories. Think global. Party local.</li>
<li>Invite others in your community to share the bounty and beauty of KDE</li>
<li>Test <a href="http://bugs.kde.org">15 bugs</a> to see if they're still relevant</li>
<li>Make a gift of €15 or <a href="http://kde.org/community/donations/">contribute</a> in some other way</li>
<li>For 15 minutes—test an app and file bugs, update <a href="http://userbase.kde.org/">KDE userbase</a>, answer questions on the <a href="http://forum.kde.org/">forum</a>, hang out at #kde on freenode IRC</li>
<li>Use hashtag #kde15 to share your plans and partying</li>
</ul>
<br />

Take a look at where KDE started and the amazing accomplishments since then. Even if you’re hardly involved, this is an inspiring milestone in a massive collaborative project that has benefited so many. 

October 14th—a global celebration of freedom and achievement!