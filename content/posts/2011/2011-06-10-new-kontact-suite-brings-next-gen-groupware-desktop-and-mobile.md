---
title: "New Kontact Suite Brings Next-Gen Groupware to Desktop and Mobile"
date:    2011-06-10
authors:
  - "sebas"
slug:    new-kontact-suite-brings-next-gen-groupware-desktop-and-mobile
comments:
  - subject: "Video in konqueror?"
    date: 2011-06-10
    body: "Does the video appear for those of you that still use Konqueror with KHTML?"
    author: ""
  - subject: "Yep"
    date: 2011-06-10
    body: "For me neither. I'm doing a recode to Theora which I'll add to the page shortly."
    author: "Stuart Jarvis"
  - subject: "copy/pasto"
    date: 2011-06-10
    body: "here's the proper changelog link:\r\nhttp://www.kde.org/announcements/changelogs/changelog4_6_3to4_6_4.php"
    author: ""
  - subject: "Should be working now..."
    date: 2011-06-10
    body: "In KHTML, embedded if you have Flash and there's a direct link to the Theora version too."
    author: "Stuart Jarvis"
  - subject: "Fixed"
    date: 2011-06-10
    body: "Thanks :-)"
    author: "Stuart Jarvis"
  - subject: "Kubuntu packages"
    date: 2011-06-10
    body: "Great to hear that Kdepim 4.6 has finally gone gold! Thanks to all the Devs for their hard work!\r\n\r\nDoes anyone know when there will be packages available for Kubuntu? There's currently version 4.5.96 in their experimental PPA..."
    author: ""
  - subject: "Kmail + Akonadi"
    date: 2011-06-10
    body: "Is akonadi trying to kill my hard disk ?"
    author: ""
  - subject: "Wow"
    date: 2011-06-10
    body: "Wow, Kontact touch looks really neat! Time to get my touchscreen fixed :) A BIG thank you and congratulations to all and any devs involved."
    author: ""
  - subject: "Kontact"
    date: 2011-06-10
    body: "Is Kontact and all the KDEPIM stuff translated? I have installed it, on Gentoo wuth the correct USE flag for my language, but it is still in the \"language of the empire\"... >_< Perhaps I have made some mistake or KDEPIM simply isn't translated?\r\n\r\nSalve."
    author: ""
  - subject: "Congratulations."
    date: 2011-06-11
    body: "Everyone, congratulations for this solid release.\r\n\r\nI hope Akonadi gets universal recognition, and I want to see Evolution and Elementary supporting and using Akonadi."
    author: "Alejandro Nova"
  - subject: "Party time!"
    date: 2011-06-11
    body: "Wow. Thanks for all your long, hard work on this KDE PIM developers. I know it's been a long slog. Rewriting so many underlying parts may not be understood by many users, but I'm totally excited for the future of Kontact. I'm looking forward to using it on mobile and to savouring the delicious speed increases to IMAP. Once I wrap my head around how ssending / recieving encrypted mail actually works (tutorial, anyone?) I'll be jumping in with both feet there, too. So much goodness to go around! You all deserve to sit back, give yourselves a huge congratulation and know that it's all appreciated! Would love to know if there's any Gmail style conversation (no just threaded) view on the horizon, but that's for another day. For now I just want to say THANKYOU and congratulations! You rock!"
    author: ""
  - subject: "Made in QML?"
    date: 2011-06-11
    body: "I have impression that it's made in QML/Qt, which doesn't need a lot of time to make this kind of transitions and this kind of UI :P\r\nNice job..."
    author: ""
  - subject: "For me (arch linux testing"
    date: 2011-06-11
    body: "For me (arch linux testing packages) it is partly translated. The Application chooser in Kontact on the left and the Configuration Dialog selection parts are translated, the rest isn't :). Strangely the git-version was always translated.\r\n\r\nSo probably a packaging issue."
    author: ""
  - subject: "Impressed with Akonadi Integration!"
    date: 2011-06-11
    body: "After being disappointed with previous releases, I can thoroughly say that Akonadi integration with Kmail, Kaddressbook and calendar work seemingly well together and is actually very practical. I've been a long time user of internet email clients but the lack consistency between contacts, emails and calendars has been poor.Usability of organising data is still dreadfully time consuming and along with the constant manual polling has been a significant time waster. So a big thumbs up guys!\r\n\r\nThe GUI for Kmail is sufficient in the functionality but in terms of usability the main interface needs to be simpler (reducing the number elements) andthe consistency between elements (e.g. alignment) needs to be improved. However, I expect this will come with the help from a usability study?\r\n\r\nThe only thing so far that needs more work is Nepomuk integration, but again I expect this will be improved in the next major release. \r\n\r\nJust like to give a big thankyou for the hard work and effort put into such a major porting effort!\r\n\r\nLuke Parry"
    author: ""
  - subject: "Thanks"
    date: 2011-06-11
    body: "Thanks for your hint, I hadn't noticed that. It happens the same happens to me (the previous poster): the left panel is translated and also parts of the dialogs. I will keep on with Evolution till KDEPIM is really finished.\r\n"
    author: ""
  - subject: "Groupware"
    date: 2011-06-11
    body: "And groupware allow to easily share contacts and calendar with your family in your network ?\r\nIt's normally the job of a groupware... isn't it ?"
    author: ""
  - subject: "Kontact2 has Nepomuk"
    date: 2011-06-11
    body: "Kontact2 has Nepomuk integration in the following forms:\r\n1. E-mail indexing by Strigi. You can search through all your mail, using KRunner.\r\n2. The \"Create pending task\" button.\r\n3. The \"Add note\" option for every mail. You have the option to annotate every mail; the annotation gets saved in Nepomuk.\r\n\r\nThe Nepomuk integration should increase, however."
    author: "Alejandro Nova"
  - subject: "Again, the most important feature is missing ;)"
    date: 2011-06-11
    body: "Kontact2 brings us many niceties, but there's one of them that got ignored: the MAPI (Microsoft Exchange/Outlook) support, coming with the OpenXChange Akonadi resource. Yes, it was a novelty announced with KDE PIM 4.5, but you know what happened to that."
    author: "Alejandro Nova"
  - subject: "I would add that it looks"
    date: 2011-06-12
    body: "I would add that it looks much nicer than the desktop counterpart (which seems to be more and more the case)"
    author: ""
  - subject: "Groupware support"
    date: 2011-06-12
    body: "From the article: \"A wide variety of groupware servers are already supported natively by Akonadi, such as the Kolab groupware,...\""
    author: ""
  - subject: "Thank you!"
    date: 2011-06-13
    body: "You are wonderful! Thank you for getting Kontact up to shape for the release and punching through the hardships! \r\n\r\nNow I can finally stop compiling kdepim from live sources (and stop being afraid that something might break in the process). \r\n\r\nMany, many thanks!"
    author: "ArneBab"
  - subject: "Now working on arch"
    date: 2011-06-16
    body: "I suspected right ;). Translation packages got updated yesterday and everything is fine now."
    author: ""
---
<p align="justify">
<div style="display: inline; float: right; align: right;"><img src="http://www.kde.org/announcements/4.6/akonadi.png" align="center" width="140" alt="Akonadi - The Personal Information Storage Service " title="Akonadi - The Personal Information Storage Service " /></div>
June 10, 2011. KDE is proud to announce the release of the next generation Kontact Suite, based on the <a href="http://community.kde.org/KDE_PIM/Akonadi">Akonadi</a> framework. In addition to this, we are also proud to announce the June maintenance update of the KDE Software Compilation 4.6. Unsurprisingly, the port of Kontact to Akonadi is finally being released the same day as Duke Nukem Forever, making it relatively timely.
</p>
<!--break-->
<h3>Known Kontact Receives More Powerful Foundation</h3>
<p align="justify">
KDE’s Kontact Suite – a set of Personal Information Management applications – is receiving a major architectural boost.  The team has invested years of development in its new infrastructure layer, Akonadi, and in porting Kontact to the new foundation while keeping the familiar user experience.<br />
The new Kontact Suite 4.6 is a major milestone for KDE software to provide outstanding features for a wide variety of devices, from traditional desktops to the latest mobile platforms. Using the capabilities of the new scalable <a href="http://community.kde.org/KDE_PIM/Akonadi">Akonadi</a> groupware framework to build interconnected PIM-related applications, the new Kontact Suite is available in versions for desktop systems as well as touchscreen devices, such as tablets and smartphones.
</p>

<p align="justify">
Users who upgrade to the new Kontact Suite version will still recognise the applications. While the underlying internals have been updated, changes in the user interface have been kept to a minimum. Among the most noticable improvements are faster email notifications, vastly improved performance for IMAP email accounts, and interoperability with other applications that consume contacts, calendars and other groupware-related information.<br />
For most users, the upgrade should be seamless, as Kontact 2 automatically imports accounts and underlying data into Akonadi, leaving the old configuration and data in place in case a rollback is ever needed.
</p>
<p align="justify">
A wide variety of groupware servers are already supported natively by Akonadi, such as the <a href="http://www.kolab.org">Kolab</a> groupware, and backwards-compatibility with existing groupware servers is guaranteed through a bridge solution allowing legacy KDE PIM resources to be used by Akonadi.
</p>
<h3>Kontact Touch: Highly Secure Email and Groupware for Mobiles</h3>
Kontact Touch brings the powerful Kontact groupware client to mobile devices too. <a href="http://userbase.kde.org/Kontact_Touch">Kontact Touch</a> (<a href="http://userbase.kde.org/Kontact_Touch/Screenshots">screenshots</a>) provides a similar featureset to the desktop also on mobile devices, including email, calendaring, and address book. Kontact Touch makes on-the-move reading and writing of signed and encrypted emails as easy as possible, supporting multiple cryptographic technologies such as OpenPGP and S/MIME. Kontact Touch provides off-line support for all data, and access to the full set of PIM resources known from the desktop client. More information about Kontact Touch can be found in <a href="http://dot.kde.org/2010/06/10/kde-pim-goes-mobile">this article</a>.
</p>

<div style="position: relative; left: 50%; margin-left: -240px; padding: 1ex; border: 1px solid grey; width: 480px;"><embed src="http://blip.tv/play/AYLBlwgC" type="application/x-shockwave-flash" width="480" height="300" wmode="transparent" allowscriptaccess="always" allowfullscreen="true" ></embed><br />Kontact Touch Demo Video (<a href="http://blip.tv/file/get/Swjarvis-PreviewOfKontactTouchOnMeeGo163.ogv">Download in Ogg Theora format</a> )</div>

<h2>KDE SC Updates to 4.6.4</h2>
<p align="justify">
Today KDE released updates for its Workspaces, Applications, and Development Platform.
These updates are the fourth in a series of monthly stabilization updates to the 4.6 series. 4.6.4 updates bring many bugfixes and translation updates on top of the latest edition in the 4.6 series and are recommended updates for everyone running 4.6.3 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
To  download source code or packages to install go to the <a href="http://kde.org/info/4.6.4.php">4.6.4 Info Page</a>. The <a href="http://www.kde.org/announcements/changelogs/changelog4_6_3to4_6_4.php">changelog</a> lists more, but not all improvements since 4.6.3.
Note that the changelog is incomplete. For a complete list of changes that went into 4.6.4, you can browse the Subversion and Git logs. 4.6.4 also ships a more complete set of translations for many of the 55+ supported languages.
To find out more about the KDE Workspace and Applications 4.6, please refer to the 4.6.0 release notes and its earlier versions.
</p>