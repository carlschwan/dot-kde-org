---
title: "Join KDE for Google Summer of Code 2011"
date:    2011-03-19
authors:
  - "nightrose"
slug:    join-kde-google-summer-code-2011
---
For the 7th year in a row KDE has been accepted as a mentoring organisation for Google Summer of Code. We are delighted to be able to work with great students throughout the summer again. To find out more about the program visit the <a href="http://www.google-melange.com">GSoC website</a> and pay special attention to the <a href="http://www.google-melange.com/document/show/gsoc_program/google/gsoc2011/faqs">FAQ</a> and <a href="http://www.google-melange.com/document/show/gsoc_program/google/gsoc2011/timeline">timeline</a>.

If you want to take part in Google Summer of Code as a student now is the time to start working on your application. You can either come up with your own idea or get inspired by our <a href="http://community.kde.org/GSoC/2011/Ideas">ideas list</a>. It is important to get in contact with the team you want to work with early to talk about your application. You can ask questions in the IRC channel #kde-soc on freenode or email the mailing list kde-soc@kde.org.

We hope to see many great applications from students all around the world. It is going to be a great summer - be part of it!