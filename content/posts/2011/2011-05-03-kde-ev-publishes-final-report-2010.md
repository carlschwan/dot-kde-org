---
title: "KDE e.V. Publishes Final Report for 2010"
date:    2011-05-03
authors:
  - "Stuart Jarvis"
slug:    kde-ev-publishes-final-report-2010
comments:
  - subject: "Why don't you use part of"
    date: 2011-05-03
    body: "Why don't you use part of those 265,840\u20ac to fix some long standing bugs? I would even donate some money for a goal like that."
    author: "trixl.."
  - subject: "Not enough money"
    date: 2011-05-03
    body: "As you can see the eV makes around 60K\u20ac a year, that's the salary of a skilled developer in europe (give or take a few thousands) so it would mean the eV would stop doing anything it does just to hire someone, not a good idea if you ask me. Also the eV has a mandate of not driving KDE development, and hiring someone will probably be against that."
    author: "tsdgeos"
  - subject: "We've come a long way, baby"
    date: 2011-05-03
    body: "Wow, checkout the current issue versus the <a href=\"http://ev.kde.org/reports/ev-quarterly-2005Q3.pdf\">first issue</a> of the eV Quarterly Report that I put together in 2005. The new editors are doing a fabulous job."
    author: "awinterz"
  - subject: "bug auction"
    date: 2011-05-03
    body: "How about creating a bug auction?\r\nUsers can vote bugs by giving 1 euro, and every 6 months, the 3 bugs with the more votes give their money to the person who solve them!"
    author: "nickkefi"
  - subject: "Great idea"
    date: 2011-05-04
    body: "That actually sounds like a really good idea. It gives users another way to contribute to development and entices more people to help us out with bug-fixing since there's a financial incentive. I'd imagine some of the more long-standing bugs could get decent bounties and as long as the e.V. is just setting up the platform for the auction and holding/distributing the money they shouldn't run afoul of any 'driving development' issues. Maybe we could try something like that in a smaller way (top 10 bugs or something)? Or at least start talking about."
    author: "areichman"
  - subject: "Really a great idea!"
    date: 2011-05-04
    body: "I agree, it's really a great idea! \r\nMore people could be involved even if they can't code, while coders could have some more motivation on working on boring bugs, for example. Everybody win!\r\n"
    author: "gerlos"
  - subject: "Konqueror, Rekonq & Bugs"
    date: 2011-05-04
    body: "Looking at https://bugs.kde.org/weekly-bug-summary.cgi, I've 5 noob questions about the 5 most bugged KDE softwares:\r\n<ol>\r\n<li>Konqueror - it's the most buggy kde software. Couldn't KDE make it deprecated and improve Rekonq?</li>\r\n<li>KMail - When KMail2 will be ready?</li>\r\n<li>Plasma - Why is so problematic? There will be a QML transition? Could it make Plasma less buggy?</li>\r\n<li>Kopete - The situation seems a bit stagnant. What about Telepathy?</li>\r\n<li>Kdelibs - I've read that it could be merged into Qt. Is this only a dream or is there a roadmap?</li>\r\n</ol>\r\n\r\nThanks for your work :)"
    author: "frafra"
  - subject: "1) Konqueror's job is to"
    date: 2011-05-04
    body: "1) Konqueror's job is to embedd other components into itself. Most of the bug reports against it belong to other components (kparts).\r\n\r\n2) KMail2 would be released very soon I guess.\r\n\r\n3) Plasma seems very mature atm. Move to QML happens on Plasma Active project mostly. Im sure it will benefit plasma-desktop as well.\r\n\r\n5) Kdelibs is not going to be 'merged' into Qt, as far as i know. However, its going to be restructured, maybe more modularized, to make it more Qt-friendly.\r\n\r\nPlatform 11 is the sprint taking place in Randa, Switzerland, next month, dedicated to this issue.\r\n\r\nAlso, kdelibs now supports different profiles, so it builds only necessary parts on each profile (eg mobile).\r\n\r\n(And, im not really the best trusted source for all the above. I hope im not wrong about any of it)"
    author: "emilsedgh"
  - subject: "Actually one of my university"
    date: 2011-05-04
    body: "Actually one of my university professors (Germany/Duisburg) today told us how happy she was to have Konqueror installed because Firefox failed at some internal website for her."
    author: "d2kx"
  - subject: "bug auction"
    date: 2011-05-04
    body: "And because this is a free community, someone who knows how to create a web page (I don't know) can create it, and just ask the kde e.v. for an account to put the money in.\r\n\r\nI think some bugs can easily escalate to 1.000 euros or more..."
    author: "nickkefi"
  - subject: "Thanks"
    date: 2011-05-05
    body: "Thanks for your answers :)"
    author: "frafra"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 0;"><a href="http://ev.kde.org/reports/ev-quarterly-2010Q4.pdf"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-ev-q42010_0.png" /></a></div><a href="http://ev.kde.org/">KDE e.V.</a>, the non-profit organization that supports the KDE community in legal and financial matters, has just published its <a href="http://ev.kde.org/reports/ev-quarterly-2010Q4.pdf">report for the fourth quarter of 2010 (pdf)</a>.

The report summarizes KDE activities in the last three months on 2010, including sprints and trade shows attended by KDE contributors with the support of KDE e.V. It features the individual supporting membership campaign <a href="http://jointhegame.kde.org/">'Join the Game'</a>, and how KDE benefits from it. There is a showcase for community artwork, including the report itself. New KDE e.V. members are presented, as is an overview of KDE e.V.'s finances.

Check out <a href="http://ev.kde.org/reports/ev-quarterly-2010Q4.pdf">KDE's latest report (pdf)</a>!
<!--break-->
