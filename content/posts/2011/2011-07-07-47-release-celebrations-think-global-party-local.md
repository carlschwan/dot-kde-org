---
title: "4.7 Release Celebrations - Think Global, Party Local"
date:    2011-07-07
authors:
  - "kallecarl"
slug:    47-release-celebrations-think-global-party-local
comments:
  - subject: "nice :)"
    date: 2011-07-07
    body: "I always like new releases :)\r\nBut a new way has to be found to make the changelogs. The way they are generated now obviously does not work. So a new way should be found. Perhaps when reviewing commits for the weekly digest, the can be marked as \"changelog worthy\" ? Or simple give a link to the complete gitlog for the release, perhaps with a filter so that it only shows bugfixes."
    author: "Asraniel"
  - subject: "yuup"
    date: 2011-07-07
    body: "Changelogs are very hard. Actually, the fact that the Commit Digest is back means writing the release notes will be far easier as you can spot most cool changes from reading the commit digests from last months (not completely but the summaries are usually enough).\r\n\r\nOr we could provide all commits from the digests with the stuff which ended up in the release in one page :D\r\n\r\nIn any case, I also look forward to the release of course.\r\n\r\nBTW I hope the anonymous commenters who complain without even daring to try the latest RC (it's quite clear they didn't try from the silly complaints) will stay away from the dot for a while... I for one look forward to Akonadi at full strength in the next release, and it's advantages even more visible in the one after that :D"
    author: "jospoortvliet"
  - subject: "more about Toulouse Release Party"
    date: 2011-07-08
    body: "via <a href=\"http://planetkde.org/\">Planet KDE</a>...\r\nMore about <a href=\"http://blog.ben2367.fr/2011/07/08/kde-4-7-release-party-in-toulouse-france/\">Toulouse and KDE Release Parties</a>. Pictures please.\r\n\r\nToulouse is celebrating KDE and free software. Is something happening where you are? \r\n\r\n"
    author: "kallecarl"
  - subject: "I understand KDE PIM and in"
    date: 2011-07-09
    body: "I understand KDE PIM and in particular Akonadi are valuable work. But is it that much of a selling point these days? Maybe I'm just ignorant but it really strikes me as something targeted towards a rather small niche. In particular certain professional environments. Most people seem to simply use Google and Facebook to manage this information. I understand Akonadi could probably, at least in theory, interface with these services and integrate them on the desktop in a smooth and useful way. But at this point that doesn't seem likely to be happening any time soon. \r\n\r\n\r\nSemantic Desktop, Activities,... these are the things I've been looking forward, and that could take my desktop experience to the next level. But again, it still seems like it's going to take a while until they're actually mature enough to make a significant difference. At least in my particular desktop use case.\r\n\r\nThat said, the RC feels quite solid, apart from some glitches here and there. The most striking to me being virtuoso's CPU hogging, and none of the activities template working except for the main one (although the latter may be specific to openSUSE). "
    author: "jadrian"
  - subject: "\"But is it that much of a"
    date: 2011-07-09
    body: "<em>\"But is it that much of a selling point these days? Maybe I'm just ignorant but it really strikes me as something targeted towards a rather small niche.\"</em>\r\n\r\nIf the developers are happy and enjoy doing it, what is it to you?\r\n\r\nEven if no one uses it, it is still a good think for others to learn from and expand on it. May be, just may be one day it will come handy for others. You never know for sure, or  do you?\r\n\r\n"
    author: ""
  - subject: "\"If the developers are happy"
    date: 2011-07-09
    body: "\"If the developers are happy and enjoy doing it, what is it to you?\"\r\n\r\nI didn't question whether developers should be working on it. That would be silly and arrogant. In fact I didn't even question it's usefulness, quite the contrary, I said I understand it's quite valuable."
    author: "jadrian"
  - subject: "Portland KDE 4.7 Release Party at OSCON"
    date: 2011-07-15
    body: "I will be attending OSCON on KDE 4.7 Release Party day (27th July).  Yet, nobody has added an OSCON or Portland Party on the wiki.  Surely, I'm not the only KDE enthusiast at OSCON!  If interested in celebrating, contact me at elcaseti AT gmail DOT com\r\n\r\nUPDATE: I added an OSCON Portland KDE 4.7 Party to the wiki."
    author: "elcaset"
  - subject: "Sorry, but if you remove KDE"
    date: 2011-07-18
    body: "Sorry, but if you remove KDE PIM, all the people who are trying to push the kde environment in the business environment (like me) will have to switch to something else. KDE PIM is one of the nicest pieces of gear in KDE suite. The problem is that it is not developed enough. For example there aren't enough widgets extracting information from akonadi in a sensible way. Finally, the KDE PIM interface should be made more \"productive\": at the moment, simple tasks like creating todo's is quite convoluted compared to something like the \"GNOME PIM\" or MS Outlook. "
    author: ""
  - subject: "KDE PIM"
    date: 2011-07-18
    body: "Sorry, but if you remove KDE PIM, all the people who are trying to push the kde environment in the business environment (like me) will have to switch to something else. KDE PIM is one of the nicest pieces of gear in KDE suite. The problem is that it is not developed enough. For example there aren't enough widgets extracting information from akonadi in a sensible way. Finally, the KDE PIM interface should be made more \"productive\": at the moment, simple tasks like creating todo's is quite convoluted compared to something like the \"GNOME PIM\" or MS Outlook.\r\n"
    author: ""
  - subject: "SOPA/PIPA - control & greed"
    date: 2012-02-12
    body: "Which of course gets to the heart of the matter.  SOPA/PIPA is less about piracy and everything about control of the Internet and greed.  The majors could care less about the artists and really don't want them to have options of digital distribution.  They don't want consumers to have choices of entertainment at all.  I firmly believe that if they could find a legal way to get rid of video games they would.\r\n"
    author: "ezequief"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 150px;"><img src="/sites/dot.kde.org/files/1224041801_x6Wj6-O_wee.jpg" ><br /><img src="/sites/dot.kde.org/files/1224043141_EzgSQ-O_wee.jpg" ><br /><center>Toulouse, 4.6 Release</center></div>Once again, it will soon be time to get together and celebrate KDE's latest Major Releases. Developers, testers and bug chasers have been busy putting the final touches on the latest versions of our software. Soon it will be time to relax and appreciate the results!

There is a <a href="http://community.kde.org/Promo/Events/Release_Parties/4.7">wiki page for the 4.7 parties</a>. If you want to organize something, please add it there soon and start getting the word out. The release is planned for Wednesday, July 27th. This is a great cause for celebration.
<!--break-->
<h3>What's this all about?</h3>
<ul>
<li>The KDE Community will meet all around the world for a fun time to celebrate the 4.7 releases.</li> 
<li>Anyone can organize a party. Just add it to the <a href="http://community.kde.org/Promo/Events/Release_Parties/4.7">wiki</a> and invite people to sign up.</li>
<li>What happens at the party is up to you! Your local event could be anything from meeting for drinks to an all-out event with talks, demos or an UpgradeFest. Be creative!</li>
<li>Everyone is welcome – contributors, soon-to-be contributors, users, FOSS friends and anyone else who just wants to celebrate.</li>
<li>If you're going to be at a major event like <a href="http://www.oscon.com/oscon2011">OSCON</a>, wear blue and your KDE paraphernalia. Turn the 27th into KDE Release Day!</li>
<li>The new KDE software is an excellent way to show off F/OSS. Invite people who might be wondering what you are so excited about.</li>
</ul> 

Please put any questions in comments...down there <big><big><big>↓</big></big></big>.

So go ahead and reserve a table at a local restaurant. Shoot out some emails to locals. And show up. Oh... and take pictures, it's wonderful to see people excited about KDE software.