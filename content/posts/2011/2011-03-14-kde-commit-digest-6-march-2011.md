---
title: "KDE Commit-Digest for 6 March 2011"
date:    2011-03-14
authors:
  - "vladislavb"
slug:    kde-commit-digest-6-march-2011
comments:
  - subject: "Strigi backport."
    date: 2011-03-15
    body: "I will ask the obvious.\r\n\r\nIf there is a fantastic amount of bugs solved by one change, there are no new features and no string changes are needed, may you do a backport of the fix for KDE 4.6.2 or 4.6.3?\r\n\r\nThis will be a major bugfix (just like <a href=https://bugs.kde.org/show_bug.cgi?id=246678>bug 246678</a>, fixed for KDE 4.6.1) and will increase even more the NEPOMUK adoption rate. That's something needed, because 4.7.0 will surely have novelties in the NEPOMUK field."
    author: "Alejandro Nova"
---
In <a href="http://commit-digest.org/issues/2011-03-06/">this week's KDE Commit-Digest</a>:

<ul><li>Strigi analyzers now read metadata in their own threads, solving over 35 crash-related bugs in Dolphin and Konqueror</li>
<li>Global shortcuts can now be used to switch between keyboard layouts</li>
<li>Collections shared over NFS & SMB/CIFS networks return to dynamic collections in <a href="http://amarok.kde.org">Amarok</a> along with a refactoring of how plugins are managed</li>
<li>In <a href="http://edu.kde.org/kstars/">KStars</a>, labels can now be assigned to points on celestial lines, and comet trails are rendered in OpenGL mode</li>
<li>New grouping by drag and drop in <a href="http://www.digikam.org/">Digikam</a> and other work</li>
<li>Support for Mobile Broadband on/off switch together with general bug fixing in NetworkManager</li>
<li>Support for SWIFT mt940 files and new "bubble" and "stacked area" graphs in <a href="http://skrooge.org/">Skrooge</a></li>
<li>Feature work and bug fixing throughout Calligra</li>
<li>Work on bookmark handling in KOffice</li>
<li>Work on video capture support in <a href="http://phonon.kde.org/">Phonon</a>, equalizer support in Phonon-Gstreamer, and optimizations throughout Phonon-VLC</li>
<li>Revamped UPnP plugin in <a href="http://ktorrent.org/">KTorrent</a></li>
<li>Work on Oxygen-Gtk and Oxygen icons</li>
<li>New PSTricks backend for <a href="http://kde-apps.org/content/show.php/Cirkuit?content=107098">Cirkuit</a></li>
<li>First steps for OS X framework support in <a href="http://techbase.kde.org/Development/Languages/Smoke">Smoke</a></li>
<li>Work on the collection scanner in <a href="http://owncloud.org/index.php/Main_Page">OwnCloud</a></li>
<li>Bug fixing in Konsole, Kdesrc-Build, KDE-PIM, Kopete, Rekonq, and <a href="http://www.krusader.org/">Krusader</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-03-06/">Read the rest of the Digest here</a>.