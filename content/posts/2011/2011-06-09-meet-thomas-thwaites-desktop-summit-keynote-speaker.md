---
title: "Meet Thomas Thwaites, Desktop Summit Keynote Speaker"
date:    2011-06-09
authors:
  - "Stuart Jarvis"
slug:    meet-thomas-thwaites-desktop-summit-keynote-speaker
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 266px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DS-logo.png"></a></div>

Thomas Thwaite, designer and technologist, will be a featured keynote speaker at this summer's <a href="https://www.desktopsummit.org/">Desktop Summit</a> 2011 in Berlin.

Thomas is perhaps best known through his <a href="http://www.thomasthwaites.com/the-toaster-project/">Toaster Project</a>. The Toaster Project was an attempt to build a toaster from raw, self-mined materials. The project exposed the complexity of seemingly simple and everyday technology. It leaves us to wonder how technology will change our lives in the future, and shows how we all need others to get even simple products.

William Carlson contacted Thomas to ask him about his projects, his views on technology and what makes him tick. Read the <a href="https://www.desktopsummit.org/interviews/thomas-thwaite">full interview on the Desktop Summit website</a>.
<!--break-->
