---
title: "Re-live the Camp KDE experience!"
date:    2011-04-13
authors:
  - "Justin Kirby"
slug:    re-live-camp-kde-experience
comments:
  - subject: "Camp KDE 2011  "
    date: 2011-04-15
    body: "we have a bunch of interviews with the organizers, speakers, and attendees. Maybe you didn't catch Carol Smith's keynote discussing how successful KDE has been within the Google Summer of Code program. \r\n\r\n\r\n\r\n\r\n<a href=\"http://www.hvacprograms.org/\">Hvac Programs</a>\r\n"
    author: "roger_mum"
  - subject: "Combine powers!"
    date: 2011-04-15
    body: "with so much talent combined, perhaps the join forces and disable the annoying warning that nepomuk shows me every time I log in!"
    author: "trixl."
  - subject: "No one knows what you're"
    date: 2011-04-17
    body: "No one knows what you're talking about."
    author: "steveire"
---
Well, Camp KDE 2011 has come and gone.  Some of you attended in person.  Others may have listened to the live audio stream in Amarok.  Maybe you missed it completely, but fear not!  Because while time travel is not yet feasible, all of the talks were recorded and are posted for your viewing and listening pleasure.  In addition, we have a bunch of interviews with the organizers, speakers, and attendees. 

<div align="center"><img src="http://dot.kde.org/sites/dot.kde.org/files/campkde2011-group.png"></div>

That's right, even if you missed Jim Zemlin's keynote about how Linux is taking over the universe by powering everything from mobile phones to air traffic control systems or even the stock market exchanges, we've got you covered.  Maybe you didn't catch Carol Smith's keynote discussing how successful KDE has been within the Google Summer of Code program.  Or perhaps you didn't get a chance to listen in on this year's education panel covering the most pressing issues relating to KDE's presence in the world's classrooms.  Not to mention the other 12 talks covering a variety of issues relating to managing your personal information, the latest Qt news, KDE on mobile devices, and developing KDE software.  

Why exactly are you still reading this article instead of watching the videos?  Well I guess we haven't told you where to find all of this yet.  So without further ado:

<h3>Camp KDE speaker videos:</h3> http://www.youtube.com/kdepromo#p/c/8F2C1C44C7A746C4

<h3>Camp KDE interviews:</h3> http://www.youtube.com/kdepromo#p/c/7B49761F58C947C7

<h3>Camp KDE photos:</h3>
Courtesy of Wade Olson - https://picasaweb.google.com/wadejolson/CampKDE2011#
Courtesy of Blau Zahl - http://www.flickr.com/photos/blauzahl/sets/72157626429000556/

Enjoy!