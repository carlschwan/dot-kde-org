---
title: "KDE Commit-Digest for 17th July 2011"
date:    2011-07-22
authors:
  - "mrybczyn"
slug:    kde-commit-digest-17th-july-2011
---
<a href="http://commit-digest.org/issues/2011-07-17/">This week's KDE Commit-Digest</a> brings two featured articles: Milian Wolff writes about his work on C++2011 support in KDevelop and Dominik Haumann presents improvements in Kate Variable Editor. The changes review brings:

<ul>
<li>Work on <a href="http://en.wikipedia.org/wiki/C%2B%2B0x">C++2011</a> support in <a href="http://www.kdevelop.org/">KDevelop</a> continues.</li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> gains a program to write NOMAD data to MySQL, GSoC work on FOV export, speed optimizations and a fix in file seek.</li>
<li>From the security-related changes, there is improved protection against spoofing attacks in URLs in <a href="http://kde-apps.org/content/show.php?content=127960">KWebkitpart.</a></li>
<li>Resource storage support is improved in <a href="http://userbase.kde.org/Applications/Desktop">KDE-Runtime</a>.</li>
<li><a href="http://userbase.kde.org/Karbon14">Karbon</a> now supports moving objects between layers.</li>
<li>User interface improvements can be seen in <a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a>, including new alarm configuration and colors in filter menus.</li>
<li><a href="http://userbase.kde.org/KMail">KMail</a> and <a href="http://extragear.kde.org/apps/kgraphviewer/">KGraphViewer</a>, independently, see performance improvements in font handling.</li>
<li>SSL support in <a href="http://rekonq.kde.org/">Rekonq</a> gets cleanup and improvements.</li>
<li>Among the other changes, we can see bugfixes in <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a>, <a href="http://kmymoney2.sourceforge.net/">KMyMoney</a>, <a href="http://www.calligra-suite.org/">Calligra</a>, <a href="http://www.k3b.org/">K3B</a> and <a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-07-17/">Read the rest of the Digest here</a>.
<!--break-->
