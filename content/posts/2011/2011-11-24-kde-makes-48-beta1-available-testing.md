---
title: "KDE Makes 4.8 Beta1 Available for Testing"
date:    2011-11-24
authors:
  - "sebas"
slug:    kde-makes-48-beta1-available-testing
comments:
  - subject: "Testing so far."
    date: 2011-11-24
    body: "First of all, I'd like to thank the Chakra team. As always, we had the Beta 1 release ahead of everyone, and, so, that enabled me to report bugs and test.\r\n\r\nPlease, pay special attention to this one:\r\nhttps://bugs.kde.org/show_bug.cgi?id=287472\r\n\r\nApart from that, if used with a newly created Nepomuk database, KDE 4.8 Beta 1 is the most stable and fast KDE ever created, even if it has some Nepomuk problems. KWin 4.8 is a lot faster than KWin 4.7 and, for the first time ever, I can fully use blur in my GeForce 6150SE. Dolphin 2 is simply awesome, even though it's not finished. QML Plasmoid replacements are working a treat.\r\n\r\nAlso, even though there are bugs, the Nepomuk team, specially Sebastian Trueg, deserve a compliment. When used with the proper packages, and for the first time ever, the Akonadi-Nepomuk services have no memory leaks, joining Nepomuk itself in that milestone. I'm sure the quality of Nepomuk in Beta 2 will be perfect, and in RCs I'm sure we won't be talking about bugfixing, but optimizations.\r\n\r\nThanks to all. KDE 4.8 is going to be the best KDE ever released."
    author: "Alejandro Nova"
  - subject: "for testing"
    date: 2011-11-24
    body: "To test in virtual machine, do you recommend something ?\r\nIs there a distro compiled with this new version ? maybe openSUSE ?\r\n\r\n--\r\nSyvolc"
    author: ""
  - subject: "for testing KDE 4.8"
    date: 2011-11-24
    body: "Chakra Linux has packages in its unstable repo"
    author: ""
  - subject: "Arch Linux"
    date: 2011-11-25
    body: "Arch Linux provides them too:\r\nbbs.archlinux.org/viewtopic.php?pid=1020742#p1020742"
    author: ""
  - subject: "memory measurements"
    date: 2011-11-25
    body: "with porting core-always-running widgets to QML can we be sure that memory usage won't increase? at least significantly"
    author: ""
  - subject: "re: memory measurements."
    date: 2011-11-26
    body: "Actually, here KDE 4.8 uses slightly less memory than KDE 4.7. The absence of memory leaks and a more memory-efficient Plasma help. And yes, all the memory you spend in a JavaScript engine and interpreter is recovered with draw code de-duplication. Also remember that Qt has its own JavaScript interpreter ;)."
    author: "Alejandro Nova"
  - subject: "Re:"
    date: 2011-11-26
    body: "I'd like to see when kde 4.8 will be in rc ,a test against some widget that did see only a porting to qml benchmarking.\r\nCPU usage and Memory usage. If cpu usage and memory usage will be lesser than kde 4.7 widget written in c++ it will be an epic WIN. :D"
    author: ""
  - subject: "4.8 Beta forum is up"
    date: 2011-11-26
    body: "The KDE Community Forums now have a forum section for 4.8 Beta: http://forum.kde.org/viewforum.php?f=212\r\n\r\nPlease don't forget to read the <a href=\"http://forum.kde.org/viewtopic.php?f=212&t=97947\">guidelines</a>."
    author: "Hans"
  - subject: "Wayland"
    date: 2011-11-28
    body: "When is it coming?"
    author: ""
  - subject: "Hopefully never"
    date: 2011-11-28
    body: "Wayland is not ready for prime-time.  Just use X."
    author: ""
  - subject: "Useless comment"
    date: 2011-11-28
    body: "Paraphrasing...\r\n~\"Don't anybody waste your time on Wayland. It's not ready now, and will never improve. If KDE implements Wayland, it better be optional, or I'm coming back here to complain. Don't say you weren't warned.\"~\r\n\r\nThank you, KDE, for making available Monday comedy."
    author: ""
  - subject: "KDE + Wayland"
    date: 2011-11-29
    body: "If you really want to know what KDE's Wayland migration plan is, please see Martin Graesslin's talk from the 2011 Desktop Summit.\r\n\r\nhttps://desktopsummit.org/program/sessions/compositing-after-x-kwin-road-wayland"
    author: "bluelightning"
  - subject: "icon size and search"
    date: 2011-11-29
    body: "I hope that 4.8 is THE version when searching in Dolphin works out of the box. There should be no fiddling around.\r\n\r\n....and still you can change anything on the KDE desktop EXCEPT the system tray icons. No matter if in the taskbar or in a widget, the icons CAN NOT be made bigger. Considering that these are the most watched pieces of real estate on the screen, there would finally be an option for people with poor eyesight to make them bigger.\r\n\r\nI know some UI designers think that 6pt font is sleek and small is cool but making icons one size only just goes against everything KDE does to make the desktop a pleasant user experience for all (some might not see the need for the option but again, its not about what YOU think is the right way but what the user wants and system tray icons is ALWAYS in the top 3 things Im asked to change for newbs.)\r\n\r\nLets cross our fingers and hope that 4.8 is the one."
    author: ""
  - subject: "Are there any packages for OpenSuse org Kubuntu?"
    date: 2011-11-29
    body: "If not, will there be?"
    author: ""
  - subject: "Icon size"
    date: 2011-11-29
    body: "<cite>....and still you can change anything on the KDE desktop EXCEPT the system tray icons. No matter if in the taskbar or in a widget, the icons CAN NOT be made bigger.</cite>\r\n\r\nPerhaps I'm not understanding what you mean, but when I increase the height of the bottom panel (4.7.3, but earlier versions also behaved this way) it increases the size of all of the icons on the panel, not just the ones in the notification area."
    author: "bluelightning"
  - subject: "OpenSUSE packages -"
    date: 2011-11-30
    body: "OpenSUSE packages - http://en.opensuse.org/KDE_repositories#Unstable:SC_aka._KUSC_.28KDE_trunk.29"
    author: ""
  - subject: "is kded4 still crashing? is Kmail still can not import old mail?"
    date: 2011-11-30
    body: "Is kded4 still crashing on signal 11? Is Kmail still can not import old mail?\r\n\r\nI'm tired by new features and \"innovations\" of KDE. Better fix annoying bugs and do not invent the wheel. BTW, it's quite a good idea to fire PR team and get paid QA team instead."
    author: "alukin"
  - subject: "I wouldn't say getting rid of"
    date: 2011-11-30
    body: "I wouldn't say getting rid of the PR team is a good idea - the project is Open Source so good Public relations and Marketing are needed to keep the growth."
    author: ""
  - subject: "I am wondering the same"
    date: 2011-11-30
    body: "I still can't import my e-mails to Kmail 2. I have tried all that I could but it's still not working. Why can Evolution and Thunderbird import mails from older versions of Kmail but Kmail 2 can't. Why is such a simple thing made to be a rocket science?\r\nI am playing with the idea of dumping Kmail altogether just because I can't get it to work!"
    author: "Bobby"
  - subject: "hilarious"
    date: 2011-12-01
    body: "want abacus. No like new tihngs. Innovation bad. Fire everyone and hire all good ones. I pay good money for dat KDE. Why no good employes? Making me tired by now. "
    author: ""
  - subject: "Yeah, so funny..."
    date: 2011-12-01
    body: "when a maintenance release completes fries all of your old emails. HILARIOUS!"
    author: ""
  - subject: "Exactly. I used kmail for"
    date: 2011-12-01
    body: "Exactly. I used kmail for years. But some young \"innovator\" decided to break it. Thanks. \r\n\r\nAnd when stupid kded4 crashes, it calls Krash that needs kded4 to send bug report! Ridiculous!\r\n\r\nWhen I see such \"changes\" in maintenance release I think of release engineering team sanity."
    author: "alukin"
  - subject: "resolving difficulties"
    date: 2011-12-01
    body: "What Linux distribution are you using?\r\n\r\nWhat KDE versions are involved?\r\n\r\nWhat forums have you searched?\r\nPosted in?\r\n\r\nI'm a kinda simple KDE user. Kubuntu has worked well for me for many years. Not everyone likes it...that's okay.\r\n\r\nHowever, the Kubuntu announcement of KMail2 had this eye-catching warning:\r\n\"IMPORTANT: Do note that this is a major upgrade to the mail, calendar and addressbook systems, and as such still needs usage and migration testing. While loss of data should not be an issue, it is highly recommended for current Kontact users to back up all important data, mail, contact information and calendars if you plan to upgrade to 11.10.\r\n\r\nOur KMail2 [1] page has more information on the migration process, the issues involved and how to import your previous data instead of migrating.\"\r\n\r\nI followed those instructions, which seem complete and explicit. There were no difficulties.\r\n\r\nIf you can share your difficulties--especially in an appropriate forum, it's likely that you can get things working or help someone else who might be in a similar situation.\r\n\r\n[1]https://wiki.kubuntu.org/OneiricOcelot/Final/Kubuntu/Kmail2"
    author: "kallecarl"
  - subject: "This is Fedora 16 with it's"
    date: 2011-12-01
    body: "This is Fedora 16 with it's KDE. Thanks, but I do not want to use KMail anymore though used it for years. I just do not trust you guys anymore.   Mail is very important thing... And I do not trust developers who can break simple working things to get useless modern \"bells and whistles\" instead.\r\n\r\nI do not care if they break some 3D effects or colors or fonts or whatever. But if they beak my-email  - goodbye forever. I program 20+ years and know how important user's personal data are. But you, guys, do not care about user. You care only of yourself and your super-duper genius new ideas.\r\n\r\nOnly difficulties with KDE - it used to be very good for years and I still can not throw it away. Because Gnome3 is just stupid joke, not an DE. It's a pity that \"release often, release early!\" is in mind of OSS developers. This is stupidest thing ever that makes many OSS projects unusable.\r\n\r\nSuch things are very harmful. Is you do not understand why ...OK, just stop coding and try to think. "
    author: "alukin"
  - subject: "Stability"
    date: 2011-12-02
    body: "KDE in last 7 years was only adequate DE env for Linux. For real work. But whats is going on with KDE is sad.\r\n\r\nNew KMail is ugly. For what reason it is rewriten? What advantages users is obtain?\r\nEach new release is set of fun visual improvements and functional regressions. Why not implements more functional taskbar with sticking of app icons (like Win 7 and MacOS) instead of absolutly useless Dolphin \"visual improvements\"???\r\n\r\nIt seems that KDE becoming an funny toy for it developers, not more. Its sad...\r\n\r\nV. Konkov"
    author: ""
  - subject: "Talking out your ass"
    date: 2011-12-03
    body: "As a KMail 2 user, I can say with confidence that the developer's priorities with these changes ARE NOT \"bells and whistles\". They are performance, reliability and stability.\r\n\r\n1. If KMail crashes, stops responding or is killed while checking mail, mail is no longer corrupted - the check carries on in the background, separate from KMail.\r\n2. Filtering ~200 messages took ~3 seconds. It used to take ~5 seconds to filter each message. The speed improvements to filtering, copying, moving or deleting large batches of E-mails or folders containing large amounts of E-mail (>2000) are phenominal.\r\n3. Having checked KMail's settings, all the previous configuration options are still available. Every checkbox, bullet point and tab has transitioned, unchanged, from KMail 1 to KMail 2, with the exception of account set-up which *looks* different but *isn't*. Advanced settings like encryption, security and templates are completely identical and work fully.\r\n\r\nConsidering how long they've delayed the release to make it work flawlessly, and considering how difficult it is to test data migration with so many different possible set-ups, I can say with confidence that the KMail team absolutely cares about personal data. Considering the content of your post, I can also say with absolute confidence that you're just completely ignorant."
    author: "madman"
  - subject: "Here is another ass, then..."
    date: 2011-12-03
    body: "<cite>1. If KMail crashes, stops responding or is killed while checking mail, mail is no longer corrupted - the check carries on in the background, separate from KMail.</cite>\r\nYeah, funny. Now instead it gets corrupted when the background process crashes. Moving the problem is not solving it. \r\nAnd, by my experience, the thousands kmail1 crashes I had in my long years as KDE user, never lost (summed together) so much mail as the one crash I had while testing kmail2. An advice for you: if you care aboutb your e-mail, and insist to use kmail2, then backup, backup, backup.\r\n\r\n<cite>2. Filtering ~200 messages took ~3 seconds. It used to take ~5 seconds to filter each message. The speed improvements to filtering, copying, moving or deleting large batches of E-mails or folders containing large amounts of E-mail (>2000) are phenominal.</cite>\r\nWhere do this numbers come from??? Using the mediocre HD of my Notebook, kmail1 filters ~3500 messages in ~30 seconds through my ~95 filters. I would like to compare with kmail2 but I have  never been able to complete the migration  and filters got screwed in the process too. I have lost mail at every attempt, then finally I restored kmail1 and a backup. Hours and hours wasted: exciting user experience, indeed, I don't think I will want to repeat it.\r\n\r\n<cite>Considering how long they've delayed the release to make it work flawlessly, and considering how difficult it is to test data migration with so many different possible set-ups, I can say with confidence that the KMail team absolutely cares about personal data</cite>\r\n\r\nI think they cared. They simply, horribly failed. Sure as hell, \"flawless\" ain't!"
    author: "Vajsvarana"
  - subject: "I'll give KMail another chance in 4.8"
    date: 2011-12-03
    body: "I've tried migrating from KMail1 to KMail2 many times beginning with betas and ending with maintenance releases. I've abandoned KMail for Thunderbird now, but using Thunderbird in KDE is still a bit painful - I miss the perfect integration with DE, it's still quite a bit slower than was KMail1 etc. So I'll definitely give KMail another try in 4.8, especially because all the promised speed-ups and because it will finally have the ability to keep HTML in replies :)"
    author: ""
  - subject: "_YOU_ said it"
    date: 2011-12-03
    body: "In other words...you don't know what you're talking about.\r\n\r\nThe poster is reporting their own experience with KMail2. You're talking about something...not sure what. Your experience testing KMail2? Did you get it installed? Apparently not. Did you follow the install instructions and research the warnings? Apparently not. So what are you basing your assertions on? Deep seated anger about something? We're not interested in being your encounter group.\r\n\r\nBack up, back up, back up...a firm grasp of the obvious. On what computer system and for what ESSENTIAL application would this not be the advice?\r\n\r\n#2. Clearly you don't know what you're talking about. You tried some number of times to install KMail2. Failed. Had a backup and stuck with KMail1. What's the complaint? Your mediocre HD of a Notebook seems to be a hot i7 based on other of your posts. What is it--mediocre or high end?\r\n\r\nlast comment...you definitely have an opinion. Unfortunately, you don't care. Nothing in a forum. No sharing of your troubles to help other people. You simply come into the KDE neighborhood and spew nonsense.  Your comments are useless toward making anything better. Based on your history of comments, this seems to be a pattern.\r\n\r\nWhere oh where is a moderator who can teach you manners?"
    author: ""
  - subject: "My manners are good enough to"
    date: 2011-12-03
    body: "My manners are good enough to sign my posts at least, and I don't feed anonymous trolls, thanks."
    author: "Vajsvarana"
  - subject: "anonymous != troll"
    date: 2011-12-03
    body: "Weak reply,Vajsvarana.\r\n\r\nTroll remarks are extraneous and off-topic. The remarks in the post above are on topic and show some research on the part of the person who posted them. They seem to be consistent with the KDE Code of Conduct...http://kde.org/code-of-conduct/...in response to some of your comments that are not considerate or respectful.\r\n\r\nYou may not like the comments, but that doesn't make them trollish.\r\n\r\nI agree...it doesn't seem like you know what you're talking about. \r\n\r\nsigned\r\nAnother Anonymous\r\n\r\n"
    author: ""
  - subject: "Oh, my..."
    date: 2011-12-03
    body: "SIGH... as you wish\r\n\r\nMy history of comments with this account starts some months ago, when I had to re-create it. I'm in KDE community since 2001, and helped with patches, mailing-lists, KDE advocacy in my LUG. Oh, and money too... But I don't think you anonymous ones care about this\r\n\r\nI have installed kmail2 during the development of 4.5, from scratch: it was completely unuseable, but it was no big surprise at the time. Installed it again in 4.6 (opensuse repository), almost useable, until it corrupted my inbox during a crash. Nothing like that had ever happened with kmail1. Luckily was just a test inbox. I stopped testing and waited for a \"somewhat stable\" version. They say it was 4.7. I have seen the some crashes strongly resembling the 4.6 ones, AND I was unable to import e-mail and filters. I have reverted to kmail1. This is my experience.\r\nI now feel the need to help by warning people who cares about their e-mail, that the the official announcement <cite>With most components now ported to Akonadi, there is increased stability</cite> \"in my experience\" (sorry I have no other :) ) is unfortunately false! \r\nI'm happy it works for you, and never said otherwise.\r\n\r\nYes, backup is always a good advice, isn'it? It would be nice reading it in Kontact 4.7 announcements. I haven't lost anything, but if you \"do your reasearch\", you will see many other have, both during migration and during use. People deserve to be warned. But you don't care about this, do you?\r\n\r\nFYI, I have 5 PCs installed with KDE (1 with trinity), both at home and work, Gentoo, Ubuntu and openSUSE. I tried on the one I was posting from, and posted for reference, as madman numbers were totally made up. \r\nIf you wish I can try the sandybridge i7 + SSD. I don't think you really care about this either.\r\n\r\nI really tried to be helpful, I used to report any bug frantically. Until this:\r\nhttps://bugs.kde.org/show_bug.cgi?id=258171\r\nNo, sorry, as long as things go like this, I won't report anything again.\r\nNow I just warn people about the problems they may encounter, and try to correct false statements. I can stop this too, no problem.\r\n\r\nAnd, may I ask, apart from playing the anonymous forum cops, what are YOU doing for this community?"
    author: "Vajsvarana"
  - subject: "Help KMail2!!!"
    date: 2011-12-06
    body: "People, what I wrote is not to put down KDE or Kmail2 because I don't think that anyone here loves KDE more than I do BUT this is serious! \r\nI really need help and up to now all I read is a lot of blah, blah, blah.\r\n\r\nWell I googled my fingers sore and I found a few tips to delete the akonadi files in ~/.local/share/akonadi and ~/.config/akonadi which should give me a clean kmail2 and migrate my thousands of e-mails that I have. \r\nThe good news is that KMail2 is now working and the address book and my settings are there.\r\nThe bad news is that all the old mails are missing - not imported.\r\n\r\nCould somebody please give me a step by step instruction on how to import my mails?\r\n\r\nI would appreciate it greatly.\r\n\r\nThanks.\r\n"
    author: "Bobby"
  - subject: "KMail 2 email import"
    date: 2011-12-06
    body: "Hello Bobby,\r\n\r\nI'm sympathetic to your dilemma. KMail2 was quite a change from earlier versions. Switching over is not just a matter of upgrading. Most of the system supporting KMail is different, and there also may be differences between distributions using KDE.\r\n\r\nThe best place to get information such as you want is in forums (depending on which distribution you have installed).  This is a KDE news channel, and doesn't have the kind of technical response that your situation calls for.\r\n\r\nI run Kubuntu 11.10 with KDE Plasma 4.7.3. The Ubuntu and Kubuntu forums both have information that helped me get KMail2 running. The Kubuntu wiki [1] has instructions about KMail2. Googling for KMail2 and Kubuntu turns up quite a number of possibilities. \r\n\r\nAkonadi is only part of what changed. To get my KMail2 installation working right, I had to fiddle with Akonadi Configuration (alt+F2...akonadi, shows that choice). I clicked the \"Modify\" button for each of the resources to make sure that each associated bit of information is correct and actually exists. (Actually, getting the Akonadi configuration done right was the missing magic sauce that finally got KMail2 working for me.)\r\n\r\nAs another poster has suggested, backup is critical.\r\n\r\n[1]https://wiki.kubuntu.org/OneiricOcelot/Final/Kubuntu/Kmail2\r\n\r\n"
    author: "kallecarl"
  - subject: "Mouahaha"
    date: 2011-12-07
    body: "Sorry, but kmail2 is more stable than kmail1, it never, never crash here but i start using it with a fresh config, sorry, but why are you not using an imap account ?"
    author: ""
  - subject: "IMAP, sure!"
    date: 2011-12-07
    body: "Why not!\r\n3 IMAP accounts (+ 1 disconnected). AND 8 POP3 accounts. And 2 identities, 4 addressbooks (2 local + 2 openxchange), 5 transports and ~95 filters... this is my main PC at work.\r\n\r\nMaybe you think it's normal to have just ONE account in kmail, and maybe just the builtin addressbook and maybe no filters too. This could easily explain why you never see it crashing. :)\r\nBefore judging the stability of a software, try to REALLY use it, ok? :)\r\n\r\n(to moderators: this thread has become uncomfortably OT. No more answers from me, feel free to remove this one too)."
    author: "Vajsvarana"
  - subject: "Thanks for the reply and the"
    date: 2011-12-08
    body: "Thanks for the reply and the advise. I am actually running openSuse 12.1. I had this problem in 11.4 after updating KDE and I just couldn't solve it so I uninstalled KMail2 and reinstalled the older version which worked quite well. This possibilityy doesn't exist in openSuse 12.1, my only choice is KMail2.\r\n\r\nI am sure that KMail2 is a positive development but there should be a possibility to import one's mails without having to learn scripting. \r\nI do have the patience to dig deeper and try this and that BUT I just don't have the time! \r\n\r\nAn e-mail client should just work because it's critical for a lot of people. For some people messing with their e-mails is like messing with their lives.\r\nAnyway I will check out the openSuse guys and see if they can help me.\r\n\r\nThanks once again. "
    author: "Bobby"
  - subject: "Kmail1 for openSUSE 12.1"
    date: 2011-12-09
    body: "If (like me) you are unable to migrate to kmail2, why not just keep using kmail1 until (let's hope!) at least the migrator improves?\r\n\r\nMany openSUSE users had the same problem and some of them tried to solve it downstream, by keeping kmail1 alive on 12.1. OBS is your friends. ;)\r\n\r\nHere is my OBS home:\r\nhttp://download.opensuse.org/repositories/home:/Vajsravana/openSUSE_12.1/home:Vajsravana.repo\r\n\r\nYou will find the packages of kmail1 for opensuse 12.1. I built them for my own use and my coworkers, so they are a bit \"raw\" with dependencies and so on. But they work quite well AFAIK. I hope you'll find them useful too.\r\n\r\n"
    author: "Vajsvarana"
---
Today KDE <a href="http://www.kde.org/announcements/announce-4.8-beta1.php">released</a> the first beta for its renewed Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing new and old functionality. Highlights of 4.8 include, among other things:

<ul>
    <li>
    Qt Quick in Plasma Workspaces -- Qt Quick is making its way into the Plasma Workspaces, the new Plasma Components provide a standardized API implementation of widgets with native Plasma Look and Feel. The device notifier widget has been ported using these components and is now written in pure QML. KWin's window switcher is now also QML-based, paving the way for newly designed window switchers.
    </li>
    <li>
    Dolphin's file view has been rewritten for performance, scalability and more attractive visual appearance.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making the KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>

</ul>
<!--break-->
