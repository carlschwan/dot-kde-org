---
title: "KDE Applauds Qt's Move to Open Governance"
date:    2011-10-21
authors:
  - "nightrose"
slug:    kde-applauds-qts-move-open-governance
comments:
  - subject: "great news!"
    date: 2011-10-21
    body: "great news!"
    author: ""
  - subject: "Nice!"
    date: 2011-10-21
    body: "it would be awesome to see kioslaves in qt.\r\n\r\nit is so useful to be able to access manpages, files in .zips, ftp, http, \u2026"
    author: ""
  - subject: "frameworky"
    date: 2011-10-21
    body: "as one of the more 'frameworky' things in KDE, I wouldn't bet on it. At some point there's enough runtime dependencies that you should just freakin use kdelibs. :)\r\n\r\n(or maybe I'm wrong, I forget where in the big matrix KIO was)"
    author: "eean"
  - subject: "Qt is cute ^^ Simply"
    date: 2011-10-22
    body: "Qt is cute ^^ Simply wonderful news! You/we rock KDE!"
    author: ""
  - subject: "Awesome news :-)"
    date: 2011-10-23
    body: "This would probably mean wider contribution to QT and wider spread of it to more people and devices :-)"
    author: ""
  - subject: "Nokia not interested in QT anymore"
    date: 2011-10-24
    body: "Nokia finally lost their concern in Qt, which was inevitable. So, why not announce this as a good news? Well, under current circumstances these are definitelly good news."
    author: ""
  - subject: "Nokia still interested in Qt"
    date: 2011-10-24
    body: "<p>Fact is that Nokia has currently more developers working on Qt than ever before. They will use it for the next billion feature phones (Meltini). So there are no indications at all that Nokia is dropping Qt.</p>"
    author: ""
  - subject: "I consider myself as part of"
    date: 2011-10-25
    body: "I consider myself as part of kde and I do not applaud anything that requires a cla. Where was a poll conducted that you feel to be eligible to announce that the kde community applauds anything like that?  "
    author: ""
  - subject: "steps forward"
    date: 2011-10-25
    body: "This is a step forward, just as the last ~5 years have been one long trail of steps forward towards greater openness. Remember when Qt wasn't as clearly licensed, had no open repositories, no labs.qt blogs, would never pick up code from external sources unless it fully owned them, etc? There were dozens of small steps to get to this point and this is indeed a huge and important moment in that passageway.\r\n\r\nIt isn't perfect yet, however, the CLA being something that will hopefully one day also go away. It will be another step.\r\n\r\nWithholding support until perfection is achieved is simply the best way to discourage progress. We should celebrate what has been achieved, then get on to working on the next steps ...\r\n\r\n(That said, if there had been a poll, I'm pretty sure most would be supportive of such a congratulations story. Our FreeQt representatives, who are voted in and who do a great job of reporting back in a timely and thorough fashion, also watched over this process. As a final note: I personally had nothing to do with this announcement, but I do fully support it :)"
    author: "aseigo"
---
Today Nokia <a href="http://labs.qt.nokia.com/2011/10/21/the-qt-project-is-live/">announced</a> the start of the open governance model for Qt, known as the <a href="http://qt-project.org/">Qt Project</a>. The Qt Project allows both companies and individuals to contribute to the development of Qt. KDE supports this move and is excited about the possibilities it brings. We have been waiting for opportunities to take a more active role in Qt's future for a long time and open governance will make this easier. KDE has been working closely with Qt during its 15 year lifetime and the Qt Project promises to bring this collaboration to a new level.

Olaf Schmidt-Wischhöfer (<a href="http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>) and Martin Konold (co-founder of KDE) said: "We fully support the work being done with the Qt Project. An openly governed Qt is in the best interests of all Qt developers. The Open Governance structure of the Qt Project empowers developers to influence the direction and the pace of Qt development. Stakeholders in the future of Qt, such as KDE, can now contribute according to their own priorities and take ownership over areas of Qt that are of particular importance to them."

To ensure the fairness of the contribution terms to individual developers, two lawyers were contracted by the KDE Free Qt Foundation. We thank Nokia for taking their feedback into account, and for working with a number of other interested third parties so that the Qt Contribution License Agreement 1.1 is widely supported.

The purpose of the KDE Free Qt Foundation is to ensure that Qt will always be available as Free Software (LGPL 2.1 and GPL 3). The legal agreements between KDE and Nokia ensuring the freedom of Qt remain valid and can now act as solid legal footings for Qt as a community-developed Free Software project.