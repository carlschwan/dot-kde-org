---
title: "KDE's November Updates Improve Nepomuk Stability"
date:    2011-11-02
authors:
  - "sebas"
slug:    kdes-november-updates-improve-nepomuk-stability
comments:
  - subject: "Kudos"
    date: 2011-11-02
    body: "Browsing bugzilla I also see lots of kdepim fixes, and a couple of 8 years old bugs :) Thanks KDE :)"
    author: ""
  - subject: "Nepomuk Stability"
    date: 2011-11-02
    body: "I hope it gets really better in terms of stability cause Nepomuk was the only to crash(1-10 times a day) on my Kubuntu 11.10..."
    author: ""
  - subject: "Better said by trueg himself..."
    date: 2011-11-02
    body: "http://trueg.wordpress.com/2011/11/02/kde-4-7-3-the-first-nepomuk-stability-release/"
    author: ""
  - subject: "useless comment"
    date: 2011-11-02
    body: "On Kubuntu 11.10 Nepomuk crashes up to 10 times a day? Have you reported bugs for this? Posted to any forums?\r\n\r\nIf this is the only issue with stability, perhaps you should turn Nepomuk off...personal settings or system settings > Desktop Search > uncheck \"Enable Nepomuk Semantic Desktop\".\r\n\r\nPlease explain how your comment serves to improve KDE or Nepomuk."
    author: ""
  - subject: "Nepomuk - KDE treasure"
    date: 2011-11-02
    body: "Sebastian Tr\u00fcg promised Nepomuk performance improvements and bugfixes...and he's delivering. Check out the <a href=\"http://dot.kde.org/2011/09/21/nepomuk-stability-and-performance\">Dot story about Nepomuk</a>.\r\n\r\nNepomuk and the rest of the semantic desktop structure contribute to KDE being a leader. Yes, there are obstacles...if Nepomuk were easy, everyone would be doing it.\r\n\r\n"
    author: "kallecarl"
  - subject: "Windows Future Storage... revisited."
    date: 2011-11-03
    body: "What Nepomuk is trying to do is something that was explored by Microsoft in their Windows Future Storage project. If we remember, that was going to become the backbone behind Windows Longhorn, the Windows that was scrapped, redone as a \"lite\" version (without WinFS) and shipped as Windows Vista.\r\n\r\nWinFS was trying to do ontologies at the filesystem level, while Nepomuk does it at the user level, but things are amazingly similar, and Sebastian, like I said in another blog post, <b>is succeeding where Microsoft failed, and failed spectacularly</b>.\r\n\r\nSo, if we want to have a chance about competing with Windows on the desktop, we need a vision of the future, and features of the future that Microsoft will never be able to replicate, like Nepomuk. That's why Sebastian must be supported."
    author: "Alejandro Nova"
  - subject: "While Nepomuk tries it at the"
    date: 2011-11-03
    body: "While Nepomuk tries it at the user level, it would help immensely if there was some support from the kernel. Currently, Nepomuk's main performance issues are because of lacking support for proper file monitoring in the kernel."
    author: "christoph"
  - subject: "What else than"
    date: 2011-11-03
    body: "What else than inotify/dnotify is needed? And there are also fam and gamin on userspace for monitoring filesystems for changes."
    author: ""
  - subject: "Hello, do you have a"
    date: 2011-11-03
    body: "[edited]\r\nHello, do you have a translation for this article? \r\n\r\nHello, this is not a site for advertising links."
    author: "spain"
  - subject: "File moves"
    date: 2011-11-03
    body: "I seem to recall Vishesh mentioning at the Desktop Summit that there was no ability to easily detect file moves at the kernel level."
    author: "bluelightning"
  - subject: "Re:"
    date: 2011-11-03
    body: "Actually, I think that Nepomuk should be turned off by default. Users of my systems aren't able and willing to report bugs, I don't have enough time to do so. And turning off Nepomuk is one of the first things I have to do after installing Plasma Workspace. It crashes really often.\r\n\r\nPlease, don't get me wrong, semantic desktop might be a great thing once it's stable. I'm not sure, if I'm dealing with distributor's or KDE's QA's fail."
    author: ""
  - subject: "KMail 2"
    date: 2011-11-03
    body: "First I would like to say thanks for the update although I haven't really tried it as yet.\r\nMy concern is Kmail and the problems that I had after updating to KDE 4.7 a few weeks ago. I couldn't get Kmail2 to work so I had to roll back the update.\r\nIs there anyone here who could give me a few tips on how to do a pain-free migration to KMail 2? I am using openSuse 11.4.\r\nThanks"
    author: "Bobby"
  - subject: "agree, turn it off!"
    date: 2011-11-03
    body: "I agree with you, I don't like things that do a lot of work in the background trashing my system because, maybe, one day, I could use it to search something.\r\nIf someone believes in \"semantic desktop\" is free to turn it on, don't plague all KDE users with it by default.\r\nI'm scared that more and more pieces of KDE are going to depend upon there pieces, and will be unable to do even basic stuff if are turned off. Maybe dev have superpower PC with a hi-end graphic card, but also desktop effects should be turned off by default, even with decent hardware they make me work slower and I have to turn them off.\r\nGNU/Linux is a multitasking, multiuser, well designed OS, that risks to become a \"Windows trashware\" clone (i.e. abandon X-Window network transparency, settings that are user specific and hard to set globally - I deploy LTSP with KDE -, more and more \"single user centric\").\r\nHope they will correct the route :)"
    author: ""
  - subject: "I don't agree..."
    date: 2011-11-03
    body: "Nepomuk and the related systems should be on by default.\r\n\r\nTurning Nepomuk off is simple. It's one checkbox in system or personal settings. You refer to \"users of my systems\"...presumably you could send them a message advising the procedure to turn it off. When they complain to you, couldn't you advise them to turn off this function? \r\n\r\nHaving to turn everything on to accommodate your situation is much more difficult, and doesn't make sense. The system using Nepomuk includes many pieces that need to work together. Nepomuk has _never_ crashed my computer. And no, it's not some hot hardware, but rather an economical laptop that's now several years old.\r\n\r\nPerhaps you could post with your real name in forum.kde.org and get some assistance. I know several people who would be willing to help. Coming to a news channel with vague problem reports does nothing to help solve your issues. "
    author: "kallecarl"
  - subject: "not gonna happen"
    date: 2011-11-03
    body: "You don't like things that do a lot of work in the background? Maybe look into an abacus. There are all kinds of functions doing things in the background on a computer; Nepomuk isn't close to being the most complicated or error prone. How do you think multitasking works, aside from doing things in the background?\r\n\r\nKDE Developers have been explicit and open about what is sacrificed when functions are turned off. With 11.10, Kubuntu is offering kubuntu-low-fat-settings.\r\n\r\nSuperpower PC...how about my 4 year old laptop? Runs Nepomuk with _zero_ problems. I also turn off the desktop effects for better performance and to reduce eyecandy that is a distraction (for me). It would be kinda selfish for me to insist that those effects be turned off for everyone because I don't want them.\r\n\r\nIt seems to me that KDE is offering a User Interface that meets most users' needs. A User Interface with obvious settings and adjustments so that individual users can modify as they wish. \r\n\r\nWindows trashware clone? That's good. I mean that's funny. Was that meant to be a joke?\r\n"
    author: "kallecarl"
  - subject: "Kmail comments..."
    date: 2011-11-03
    body: "[edited to retain content, eliding <a href=\"http://www.kde.org/code-of-conduct/\">gratuitous insults and unnecessary abusive language</a>]\r\n\r\nKmail2 is broken [and it makes me angry].\r\n\r\nKmail from 4.7.1 was a dream...As soon as I tried to upgrade....it went kaput.\r\n\r\nEmail filters didn't work, spam filters didn't work, my accounts didn't transfer, email was collected from accounts even when set to \"Offline\", error messages EVERYWHERE, Nepomuk and akonadi dying...\r\n\r\nI have been a user of Kmail from the first betas...I have email from 1994 in my archives. I have coded import filters for Kmail. To say I am INTO Kmail is an understatement.\r\n\r\nAnd right now, I am desperately planning to leave Kmail."
    author: ""
  - subject: "On the whole I do not have a"
    date: 2011-11-04
    body: "On the whole I do not have a problem with kmail depending on or making use of Nepomuk; I can see its usefulness. \r\n\r\nHaving said that there are a couple of fiddly bits I have with kmail2 that range from annoying to \"oh come on\". \r\n\r\nThe first; importing mail. With kmail-1 (?) it was simple and straight forward. Click File>Import>Choose kmail dirs/folders>select the folder and press on. It would automagically create an import folder within kmail. Now I have to do an extra step and actually create the import folder. It was confusing at first what I needed to do because it differed from how it *used* to be done. I see no real sense in making the user take the extra step. That would be the \"oh come on\" part.\r\n\r\nThe second; I do not know if it is a kwallet issue, kmail or nepomuk but here is what I have seen if I do not want to use kwallet. When setting up the accounts, manually or with the script; I cannot input the password; it won't let me no matter how many times I click on the empty input box. As a result anytime kmail checks for mail it constantly asks me for a password.\r\n\r\nNow if I use kwallet, most times it will not ask for a password once it is provided but on occasion it will. The upside; if kwallet is set up before creating the kmail accounts it will let me enter the password when the accounts are created.  So I have gone through all the settings in kwallet to never close, continue running and all those but still kmail asks for the pop/smtp account passwords. That is the annoying one.\r\n"
    author: "stumbles"
  - subject: "No idea..."
    date: 2011-11-04
    body: "I helped code parts of kmail, and I STILL don't know how to avoid Nepomuk disasters and kwallet goofiness.\r\n\r\nCan't we just have options to have a crypter pw in the config file like we used to? And maybe NOT have Nepomuk in kmail? I don't need it."
    author: ""
  - subject: "forum.kde.org"
    date: 2011-11-04
    body: "Best place to get assistance...\r\nhttp://forum.kde.org/viewforum.php?f=20\r\nKDE Software > Office & Productivity\r\n\r\nAlso visit:\r\nhttp://userbase.kde.org/KMail/FAQs_Hints_and_Tips\r\nhttp://userbase.kde.org/Kontact\r\n\r\nIt will help others to add problems, solutions and discoveries to the forums."
    author: "kallecarl"
  - subject: "mtime propagation"
    date: 2011-11-04
    body: "At startup this would be useful:\r\nhttp://trueg.wordpress.com/2011/11/02/kde-4-7-3-the-first-nepomuk-stability-release/#comment-1819"
    author: "renox"
  - subject: "That's doesn't sound very"
    date: 2011-11-05
    body: "That's doesn't sound very encouraging.  \r\n\r\nThe following paragraph might put a jinx on me but I may have found the problem with kmail and its repeated asking of passwords. When I setup kmail I used the wizard. It appears this wizard guesses wrongly about the pop and smtp when clicking the auto detect feature. After correcting those settings it seems to have worked; at least for now. Erp, it was a jinx.\r\n\r\nAt this point I am not the least bit sure if it is nepomuk causing this password issue I seem to be having or if its kwallet. However I don't think it is kwallet because the same thing happens if I say no to kwallet integration and in my mind that points back to kmail and whatever it does with passwords/account settings.\r\n\r\nThere is a thrid annoyance I have with kmail. It used to be with kmail1 I would do something simple; like copy .kde/share/app/kmail elsewhere and ta da, I had a backup. Now it looks like I need to gather up a half dozen or more hidden directories just to make sure I got everything mail related. That's simply an explosion of complication.\r\n\r\n\r\n\r\nI know Sebastian has been doing an outstanding job of hammering out nepomuk issues and someone really really should hire him."
    author: "stumbles"
  - subject: "cite correctly"
    date: 2011-11-05
    body: "I said \"I don't like things that do a lot of work in the background *trashing my system*\". Did you missed this fundamental part?\r\nDid you missed that I want it turned off *by default* since is useless for 90% of people while harms 100% of them?\r\nKubuntu offers \"kubuntu-low-fat-settings\"? Why have the system be resource hungry from the start? And you know that if you in 11.10 turn nepomuk off it keeps annoying you? (is a bug that some dev marked as \"won't fix\", there is a long thread on this \"obvious\" problem that some dev don't find so obvious, amazing).\r\nYou have a 4 years old laptop? Lucky man, I've 12 years old PC at school, and while they work(ed) really fine with XP I had to expand to 1GB ram to use Kubuntu\r\nI really appreciate KDE devs work, but I'm really puzzled when they take some routes that are against efficiency and productivity and, also, irritate users \"just for fun\".\r\nAnd since I value a lot their work, I criticize those choices, hoping they will address the issues :)"
    author: ""
  - subject: "KMail"
    date: 2011-11-05
    body: "I tried a couple of times to use KMail but without success : too slow or too unstable or both. I use Thunderbird on a daily basis since ten years, having a lot of old mails stored (more than 2Gb) ; TB never, never crashes, does its jobs faithfully and has nice extensions.\r\nKMail appealed me because it has really interesting options for fine-tuning almost everything ; alas, it doesn't seem to work reliably like the other KDE software pieces.\r\n"
    author: ""
  - subject: "I do not think that is totally fair"
    date: 2011-11-06
    body: "I can very much understand the rationale behind new technologies like Akonadi and Nepomuk and I like the general idea. I agree that Kmail2 is not actually ready for production use (I would not say it is broken), although it is improving quickly.\r\n\r\nWhat I do not like is that Kmail2 was delivered in Kubuntu Oneric without appropriate warnings and the alternative to use Oneric with Kmail1. That reminds me a little bit how distros to quickly adapted to KDE4. Kubuntu devs said they had now alternative, that could not deliver a fresh KDE with old Kmail, because that was not supported by KDEPIM developers. If that is true that was not such good decision. People who did not test and follow blogs before would just stumble upon all the problems after upgrading. I assume you are one of them may be with another distro. Me was lucky, I tested before, so I did not upgrade and now I am following bug-count. And it is getting better. I am hopping for 4.7.4 or one release later for a stable and working Kmail2. So stay tight and do not give up on it."
    author: ""
  - subject: "Crazy Situation"
    date: 2011-11-06
    body: "I really can't believe that after years that this is still going on.  Given the number of bugs in Nepomuk, it would be better for the KDE leadership to remove it from the distribution until quality and design are improved.  It is my opinion formed from several less than stellar experiences that its inclusion represents a failure in the leadership of kde.  Continuing to include it seems to be a cave-in to a small faction of interests with a grand vision.  It does not work, ie too many bugs, there are too many other important kde applications that have been designed to depend upon it, and its code is fragile, ie fix one place causes problems elsewhere.\r\n\r\nUsers are not guinea pigs and developers have a responsibility for quality assurance.  That is not happening here. KDE leadership has the horse before the wagon.  There should be no need for a so-called fat-free version.  This is a sign of just how sick the patient has become.  The reality is that Nepomuk is a form of gangrene on my computer.  Time to amputate.\r\n\r\nIf the functionality of Nepomuk is really that important, it may be time to start a parallel effort or a secondary approach.   Another group of programers might be more successful in a nepomuk like implementation.  If that is not possible or an interim solution is needed, some more basic applications to replace kmail and other applications currently tied to nepomuk could be built."
    author: ""
  - subject: "Re: crazy situation"
    date: 2011-11-06
    body: "Did you miss the part where Nepomuk had quite a number of bugs fixed in this new release? Why, after getting it fixed up, would someone start another project to rewrite it?"
    author: "bluelightning"
  - subject: "oversimplification + hyperbole = not useful"
    date: 2011-11-06
    body: "Given your wise insights, it would be great for you to jump into the design and architecture discussions that are held _in the open_ in KDE.\r\n\r\nYou state several things as facts that are not, in fact, true...\r\n* doesn't work? Works fine for me, and everyone else I know about first-hand.\r\n* too many bugs? What does that even mean?\r\n* fragile? Don't think so\r\n* fix one place causes problems elsewhere? How is that different from any other piece of software?\r\n\r\nDon't look now, but there is no gangrene on your computer. No need to amputate. The system including Nepomuk (and several other large functions) is getting better and better. Here's hoping you are not in a medical field.\r\n\r\nIncidentally, the horse is supposed to be before the wagon. This is typical of the muddled thinking and painful analogies that lead to unsupported assertions and simplistic architectural suggestions. To one degree or another, most don't know what they're talking about. And they don't think about the whole system before spouting off on their own small problem. \r\n\r\nActually it's a feature of open source projects that users _are_ guinea pigs...to the extent that they choose. You can have a rock solid, never-have-a-problem installation. They are readily available...most likely not with the latest features though.\r\n\r\nPerhaps you could read about kubuntu-low-fat-settings (http://www.kubuntu.org/news/11.10-release). It was not designed as a Nepomuk work around. It was offered as a service for a particular set of users.\r\n\r\nIt appears that many of the complaints stem from people not reading the release notes, not reading the warnings, not reading that such-and-so feature is experimental. For example, to assert that there should be no need for Kubuntu low-fat-settings demonstrates a lack of awareness.\r\n\r\n<snark>It's all KDE leadership's fault. We want to do whatever we please on whatever hardware with whatever devices. And we expect you to make it happen with no problems. No, we won't read the directions and warnings...any software that requires a manual is bad software. No we won't post in the forums; KDE should be answering those questions or not publishing anything that requires assistance. Think of the old people and children...they're not computer literate. What about them? </snark>\r\n\r\nKMail2?\r\ndid you read https://wiki.kubuntu.org/OneiricOcelot/Final/Kubuntu/Kmail2\r\nEvery comment I can recall about KMail2 problems is addressed here.\r\n\r\nFinally...\r\nwhat are you contributing to KDE? Please don't start in with \"I'm not a coder\". There are hundreds of things that non-techies can do. It is of no value to make ill thought out, exaggerated and often baseless, selfish complaints..for whatever reason you think that is going to make any difference whatsoever.\r\n\r\nhttp://community.kde.org/Getinvolved. All we're trying to do is the best we can."
    author: "kallecarl"
  - subject: "\"There are hundreds of things"
    date: 2011-11-06
    body: "\"There are hundreds of things that non-techies can do.\". Yes that is very true *except* fix nepomuk/kmail etal bugs; and that is the problem for the \"I'm not a coder.\" types. But it is much easier to poo-poo them."
    author: "stumbles"
  - subject: "poo-poo?"
    date: 2011-11-07
    body: "My comments are not intended to poo-poo anyone having issues. I am not making light of their problems nor expressing contempt for them personally. The easier course of action would be to do and say nothing. To the contrary, I think that there are opportunities for people to put their energy into making all manner of KDE things better, rather than making vague, mildly insulting and ultimately useless complaints.\r\n\r\nOne issue is with people complaining here about such things as not being warned about KMail2 migration. That's just plain hogwash. There were plenty of warnings. There were detailed manuals written about how to migrate and how to recover. I understand that people just dive right in--I do that too. But when things go wacky, it doesn't provide any value to blame KDE leadership (for example).\r\n\r\nAs I read your comment, this is a slightly more sophisticated version of \"I'm not a coder\". It goes like this...\r\nMy problem is one that only a coder can fix. I would really, really, really like to help, but I just can't...cuz I'm not a coder. And I don't see what helping KDE in other areas would do to fix _my_ problem.\r\n\r\nApparently people think that complaining is actually doing something productive. It isn't.\r\n "
    author: "kallecarl"
  - subject: "yes, I did."
    date: 2011-11-08
    body: ">KMail2?\r\n>did you read https://wiki.kubuntu.org/OneiricOcelot/Final/Kubuntu/Kmail2\r\n>Every comment I can recall about KMail2 problems is addressed here.\r\nActually I have read it and I am glad have tested it before in a separate environment. This info misses the point. It does not say: If you want to work with Kmail2 in a productive way, do not upgrade to Oneric. That is really the important point, because after you have successfully managed migration, Kmail2 is still less stable, less reliable, slower and has still too many bugs. I can see, it is improving and some people could live with it, but at the moment, I do not understand why it was put in a distro release without a warning."
    author: ""
  - subject: "qa and developers"
    date: 2011-11-09
    body: "A lot of the debate here hovers around quality of programs and users acting as programmers. Some comments presumably from developers lament users who do not participate as programmers. Not mentioned in the lament is why? To speed delivery of the program is one that everyone probably agrees on. Improving design, function, and taking on bigger goals the same. However, the tone sometimes seems to be that users don't deserve working software or to comment on quality unless they are also programmers.  I would push back that developers have a duty to conduct thorough testing before releasing the product even if it means slower development or less ambitious projects. "
    author: ""
  - subject: "+1: Agree, turn Nepomuk off by default!!"
    date: 2011-11-11
    body: "+1: Agree, turn Nepomuk off by default!!"
    author: ""
  - subject: "kmail2"
    date: 2011-12-27
    body: "I don't have a problem that KMail doesn't work but what push me toward Thunderbird is how is KMail slow with email which don't have pop filters. It take so long time and the bug or whatever is is in the KDE 4.7.3.\r\n\r\nI didn't decided yet or switched to KDE 3 or choose something other.\r\n"
    author: ""
---
Today KDE <a href="http://kde.org/announcements/announce-4.7.3.php">released</a> updates for its Workspaces, Applications, and Development Platform.
These updates are the third in a series of monthly stabilization updates to the 4.7 series. 4.7.3 updates bring many bugfixes and translation updates on top of the latest edition in the 4.7 series and are recommended updates for everyone running 4.7.0 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.

The October updates contain many performance improvements and bugfixes for applications using the Nepomuk semantic framework.
<br /><br />
To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.7.3.php">4.7.3 Info Page</a>. The <a href="http://www.kde.org/announcements/changelogs/changelog4_7_2to4_7_3.php">changelog</a> and <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.7.3&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a> list more, but not all improvements since 4.7.2.
Note that these changelogs are incomplete. For a complete list of changes that went into 4.7.3, you can browse the Subversion and Git logs. 4.7.3 also ships a more complete set of translations for many of the 55+ supported languages.
To find out more about the KDE Workspace and Applications 4.7, please refer to the <a href="http://www.kde.org/announcements/4.7/">4.7 release notes</a> and its earlier versions.
<!--break-->
