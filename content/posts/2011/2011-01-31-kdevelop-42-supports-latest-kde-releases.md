---
title: "KDevelop 4.2 Supports Latest KDE Releases"
date:    2011-01-31
authors:
  - "Milian Wolff"
slug:    kdevelop-42-supports-latest-kde-releases
comments:
  - subject: "Nice work"
    date: 2011-02-01
    body: "The lock removing feature is exactly what I missed sometimes. Thanks"
    author: "slangkamp"
  - subject: "Krazy"
    date: 2011-02-01
    body: "The coding assistant is interesting, would it also provide a framework for krazy-style coding style review as you type?"
    author: "vinum"
  - subject: "sure, someone would just need"
    date: 2011-02-02
    body: "sure, someone would just need to write the code :)"
    author: "Milian Wolff"
  - subject: "Thanks!"
    date: 2011-02-03
    body: "Thanks for the new version. KDevelop is my primary IDE for C++ developing and it seriously rocks! In particular, I like the C++ support which  significantly increases my productivity, e.g., by the nice autocompletion, syntax highlighting, code browsing, support for refactoring etc. Also in many cases KDevelop shows me syntactical mistakes even before I start compilation. At the same time the IDE does not get in my way.\r\n\r\nAll in all, it is a pleasure to work with KDevelop. You guys did a decent job--thanks a lot!"
    author: "mkrohn5"
  - subject: "Homepage"
    date: 2011-02-03
    body: "Thanks for the new version, very much appreciated.\r\n\r\nI just wanted to mention, that on the KDevelop homepage on the top right, Download, Latest release still says \"KDevelop 4.1.1\" and the latest topic in section \"News of 2011:\" is \"KDevelop 4.2 Beta 1 (development) released\"\r\n\r\nWould be nice if the homepage could be updated to spread the news."
    author: "Roland"
---
Only three months after the last feature release, the KDevelop hackers are proud and happy to announce the release of KDevelop 4.2. As usual, we also make available updated versions of the KDevelop PHP plugins.

You should find that KDevelop 4.2 is significantly more stable and polished than 4.1, with some important changes under the hood; we suggest that everyone updates to this version, if possible.

Please note that this version is required for users of KDE Platform 4.6 or higher. It will also work with KDE Platform 4.5 but Platform 4.4 or older are not supported.
<!--break-->
Here are some statistics to show you how active the last months were for us:

<table style="width: 100%;">
<tr>
<th>Package</th>
<th>Commits since 4.1</th>
<th>Diffstat</th>
</tr>
<tr>
<td>KDevplatform</td>
<td>603</td>
<td>452 files changed, 14540 insertions(+), 10584 deletions(-)</td>
</tr>
<tr>
<td>KDevelop</td>
<td>317</td>
<td>230 files changed, 7666 insertions(+), 2794 deletions(-)</td>
</tr>
<tr>
<td>KDev-PHP</td>
<td>57</td>
<td>61 files changed, 733 insertions(+), 727 deletions(-)</td>
</tr>
<tr>
<td>KDev-PHP-Docs</td>
<td>26</td>
<td>5 files changed, 39 insertions(+), 56 deletions(-)</td>
</tr>
</table>

<h2>New Features and other Notable Changes</h2>

<h3><code>KTextEditor::MovingInterface</code></h3>
David Nolden almost single handedly ported the KDevelop code to <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/group__kte__group__moving__classes.html">a new Kate architecture</a>. This should hopefully make KDevelop more stable and better suited for future development.

<h3>Find and Replace in Files</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/search-replace.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/search-replace-sml.png" /></a><br />Find and replace</div>KDevelop 4 came with a <em>"Find in Files"</em> plugin from the start, but many users missed having <em>"search and replace"</em> functionality in it. A group of French students (Silvere Lestang, Julien Desgats, Benjamin Port) working on KDevelop as a university project, finally tackled this and added <a href="http://blog.ben2367.fr/2010/12/02/find-and-replace/">this feature</a>.

<h3 style="clear:both;">More Embedded Documentation</h3>

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/man-documentation.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/man-documentation-sml.png" /></a><br />Embedded documentation</div>The same group of students worked on other parts of KDevelop as well. Benjamin Port took care of the QtHelp plugin, which now supports <a href="http://blog.ben2367.fr/2010/11/11/kdevelop-when-development-documentation-come-to-you/">adding arbitrary `.qch` files</a>.

Yannik Motta and Benjamin Port also wrote a completely new documentation plugin, that <a href="http://blog.ben2367.fr/2010/12/01/man-page-documentation-in-kdevelop/">integrates Man pages</a> into KDevelop.

<h3 style="clear:both;">Improved Problems Toolview</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/problemview.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/problemview-sml.png" /></a><br />Problems toolview</div>Thanks to Dmitry Risenberg, the toolview that shows problems in your projects is largely improved: It is now capable of tracking <tt>TODO</tt> and <tt>FIXME</tt> comments, with which you can easily locate outstanding issues in your code. And for a better overview, the reported problems may now be filtered by severity (error/warning/hint) and the scope can be restricted to e.g. only the files of the current project. Optionally, the toolview also shows errors in imported/included files.

<h3>Improved C++ Support</h3>

KDevelop 4.2 comes with much improved C++ support, thanks to the contributions of Ciprian Ciubotariu, who added Argument-Dependent-Lookup functionality, and Dmitry Risenberg, who made the preprocessor implementation more standards compliant. This results in better support for many projects, most notably those using Boost (e.g. Boost::Test) and others.

<h3 style="clear:both;">Rename Assistant</h3>

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/rename-assistant.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/rename-assistant-sml.png" /></a><br />Rename assistant</div>Olivier Jean de Gaalon contributed a new C++ code assistant: Whenever you rename the declaration of a local variable, it offers you to rename all uses of that declaration as well. He also improved the design of the assistants, making them more visually pleasing.

<h3 style="clear:both;">Better Handling of Locked Sessions</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/locked-session.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/locked-session-sml.png" /></a><br />Locked session handling</div>When you try to open an already opened session in KDevelop, it will now raise the appropriate window. When it cannot find such a window, e.g. due to a crash, a dialog will be shown that enables you to remove the lockfile and continue the startup process.

<h3 style="clear:both;">Other notable changes</h3>

There were also some other important improvements and additions in this release:

<ul>
<li>Improvements to file and folder handling by Olivier Jean de Gaalon, in particular for CMake support</li>
<li>Milian Wolff fixed the PHP support to now highlight variables in the global context in different colors, as was done before in functions and methods (<a href="http://users.physik.fu-berlin.de/~milianw/kdevelop/screenshots/4.2.0/php-colors.png">screenshot</a>).</li>
<li>Eugene Agafonov added a filter to the Projects toolview to help you find files in it (<a href="http://users.physik.fu-berlin.de/~milianw/kdevelop/screenshots/4.2.0/filter-pmv.png">screenshot</a>).</li>
<li>Milian also made KDevelop optionally remember the choice to open given types of files in external applications, which is e.g. useful to always open <code>.ui</code> files in QtDesigner.</li>
<li>Internal code for WorkingSets got refactored and cleaned up, which should result in better performance and more stability.</li>
</ul>

Thanks to all contributors, especially to the numerous new names in this list. Lets look forward to a great KDevelop 4.3 release later this year.