---
title: "Desktop Summit Call for Volunteers"
date:    2011-06-23
authors:
  - "sealne"
slug:    desktop-summit-call-volunteers
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 266px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DS-logo.png"></a></div>

The Desktop Summit 2011 in Berlin, Germany Needs Your Help.

Are you attending the <a href="https://www.desktopsummit.org/">Desktop Summit</a>? Are you interested in helping the GNOME and KDE communities organize this year's Summit? Do you want to work with other Free Software enthusiasts to make the Desktop Summit rock? Would you like to own one of the exclusive Desktop Summit Team T-Shirts?

Then please sign up as a volunteer. We need a lot of volunteers and a variety of skills for all kinds of tasks.

More details about the tasks are available on the <a href="https://www.desktopsummit.org/news/call-for-volunteers">Call for Volunteers</a> and the wiki page to <a href="http://wiki.desktopsummit.org/Volunteers">sign-up</a> as a volunteer.
<!--break-->
