---
title: "From Platform to Frameworks -- KDE hackers meet in Switzerland"
date:    2011-06-29
authors:
  - "sebas"
slug:    platform-frameworks-kde-hackers-meet-switzerland
comments:
  - subject: "I like the maintainer of the"
    date: 2011-06-30
    body: "I like the maintainer of the C#-bindings on the tree, lol."
    author: "trixl."
  - subject: "That's Steven Kelly, he's MVC"
    date: 2011-06-30
    body: "That's Steven Kelly, he's MVC magician, but to my knowledge not the maintainer of the C# bindings... (check <a href=\"http://granjow.net/uploads/temp/GroupPic2011_.jpg\">here</a>)"
    author: "sebas"
  - subject: "Some more sponsors"
    date: 2011-06-30
    body: "There were some more sponsors I'd like to thank:\r\n<ul>\r\n<li>Bar Informatik AG for the internet connection</li>\r\n<li>BudgetComputer.ch for the demo PCs</li>\r\n<li>/ch/open for their financial support</li>\r\n<li>Jungwacht Blauring for their financial support</li>\r\n<li>Raiffeisen Mischabel Zermatt for their financial support</li>\r\n</ul>\r\nAnd big thanks go to the cook and all the other helpers without them this meeting wouldn't have been possible!"
    author: "unormal"
  - subject: "Is there something that dfaure CANNOT do?"
    date: 2011-06-30
    body: "Congratulations David!"
    author: ""
  - subject: "Where is the shower?"
    date: 2011-06-30
    body: "\"What songs do you sing in the shower?\r\n\r\nI don't sing in the shower. [...] I play the piano though, and after I do I sing some songs of mine.\"\r\n\r\nhttp://www.behindkde.org/node/43"
    author: "mkrohn5"
  - subject: "Names to faces?"
    date: 2011-07-01
    body: "I probably ask for this every time there's a group photo, but it would be nice if there were names against each person in the picture.\r\n\r\nIs that funky web photo script (with hover-over name popups) from a few years ago still kicking around somewhere?"
    author: "bluelightning"
  - subject: "Yes it is"
    date: 2011-07-05
    body: "You can find last years akademy photo here\r\nhttp://people.canonical.com/~jriddell/akademy/akademy-2010-group-photo.html the JS is in the file."
    author: ""
  - subject: "Three years later..."
    date: 2014-07-07
    body: "And three years later, <a href=\"https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers\">frameworks 5.0 is released</a> and KDE is doing a fundraiser for the <a href=\"http://www.kde.org/fundraisers/randameetings2014/\">Randa 2014 meeting</a>! If that ain't proof these meetings are worth it, nothing is ;-)"
    author: "jospoortvliet"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 250px;"><a href="/sites/dot.kde.org/files/GroupPic2011.jpg"><img src="/sites/dot.kde.org/files/GroupPic2011_wee.jpg" ></a><br /><center>Randa Group Photo</center></div>

<p>In early June 2011, a sizable group of KDE hackers met high up in the Swiss Alps. In Randa, four co-located meetings took place to further KDE technologies. One of these groups, Platform 11, had as its goal to take the KDE development platform to its next level. This group consisted of about 25 people who work on and around kdelibs, the build-system, distributions, and 3rd party developers, and was intended to represent needs and wishes as completely as possible, while trying to find better ways to organize the KDE development platform.</p>
<!--break-->
<p>The overall scheduling was done by Kevin Ottens, who used a <a href="http://en.wikipedia.org/wiki/Kanban">Kanban wall</a> to manage discussions, tasks, and results. The meeting started with small, focused groups that discussed current problems for a while, and then came up with several short suggestion notes for meetings. The suggestion notes were then clustered into topics to create an agenda for the meeting. In the following days, groups of up to 5 people dove into these topics one by one. Sometimes this would take two hours of discussion; sometimes it would be a two day class-by-class analysis of kdelibs and other modules to examine work and decide on what needs to be done. Each day the whole group would gather at least once to present the results of the previous day's sessions, to review the solutions the subgroups came up with, and to move on to the next topics. This way, a lot of ground was covered, and a lot of details were examined with input from all sides. These results and plans have since been discussed on various mailing lists such as kde-core-devel, refined and further followed up on.</p>

<h2>Platform to Frameworks</h2>

<p>One of the primary results of Platform 11 was gaining consensus on making KDE's development platform more modular, with each library (or technology within it) clearly defined in its purpose and how it can be deployed for use in a Qt or KDE application. The goals are to create a more maintainable set of libraries with higher quality, to make KDE libraries accessible to the current community of Qt developers, and to provide KDE with a set of libraries that are well-suited for use in mobile and consumer electronic devices. The end result is a shift from a "platform" to a set of integratable "frameworks". This is reflected in what will be the name for this next version of KDE's libraries and basic application runtime requirements: KDE Frameworks.</p>

<p>All of the libraries and run-time requirements in KDE Frameworks are being placed into one of three categories: 
<ul>
<li><strong>Functional Qt Addons</strong>, which provide a well defined purpose (e.g. configuration management) and carry no additional runtime dependencies other than Qt;</li>
<li><strong>Operating System Integration</strong>, Qt Addons that can have operating system-specific dependencies to provide their features (such as how a theoretical libktimezone would use ktimezoned on Linux but the native API on Microsoft Windows); and</li>
<li><strong>Solutions</strong>, which implement a full technology or stack, including a library and mandatory runtime dependencies.</li>
</ul>
Each of these categories contains a hierarchy of dependencies to prevent internal dependency tangles. A preliminary plan of how this may look for some of the existing libraries can be <a href="http://files.kde.org/ervin/platform11/kde-frameworks-dependencies-plan.pdf">found in this PDF</a>.</p>

<p>These rules will help guide development and make it easier for developers to use specific libraries in their Qt applications. The reduced dependency graph and the ability to rely on libraries being individually available with only their own dependencies should increase the appeal of the KDE libraries significantly to Qt developers. It will also make deploying KDE libraries across different platforms easier.</p>

<p>To realize the goal of transforming KDE Platform into KDE Frameworks, the team at Platform 11 swept through every class and library in the kdesupport, kdelibs and kde-runtime modules, and did initial surveys of kdepimlibs and kdepim-runtime. Each item was identified and categorized as to where it fits within the Frameworks scheme.</p>

<h2>KDE Frameworks and Qt 5</h2>

<p>With Qt 5 on the horizon, the sprint took place at exactly the right time. With an idea of the future of the KDE Development Frameworks, natural overlap in Qt can be reduced by merging certain parts into Qt. With Qt opening up its governance model, we are presented with a nice opportunity to take a more proactive role in the future of Qt. A team of KDE developers has naturally taken part in the recent Qt Contributors Summit, and presented some ideas from the sprint in Switzerland. First patches have already been merged into the Qt 5 source-tree, and more are yet to come.</p>

<h2>Timeline</h2>

<p>When KDE Platform 4.7.0 is released, a set of feature branches will be opened up in KDE's git for work to commence on KDE Frameworks. Initially work will proceed in the existing modules (e.g. kdesupport, kdelibs, kde-runtime, etc.), though it is expected that these will eventually break out into several modules with one module per library or solution.</p>

<p>Means to build all of these new modules in one monolithic build, as is done with kdelibs today, will be provided to maintain the level of ease for those who wish to build the entire set of Frameworks rather than cherry pick through them. KDE will still also provide monolithic tarballs at release time as we have done in the past for those who would like to get larger chunks in one go. This was seen as critical to several of KDE's distribution partners in terms of their available people resources and the complexity of packaging a more modularized set of libraries.</p>

<p>While work proceeds on KDE Frameworks, further releases of the KDE Workspaces and Applications 4.x will continue. These releases will target the existing KDE Platform 4.x, allowing for the Frameworks evolution to be undertaken without disrupting application development. Only when Frameworks moves into the stabilization phases will application developers be broadly invited to start targeting them. As with Qt 5, the intention is to keep source compatibility high so as to minimize disruptions in existing applications as well as the existing workspaces such as Plasma Desktop.</p>

<p>No firm delivery date was decided on for KDE Frameworks. The goal of a release in a timely fashion with the first release of Qt 5 was entertained, but discussions were postponed until KDE Platform 4.7 is officially released and Frameworks development can begin in earnest.</p>

<h2>Not Just Technical</h2>

<p>While there was certainly a lot of highly technical content, there were also less technical moments that helped draw members of the community closer together. There was a visit to scenic Zermatt, a football (or soccer to the North Americans) game and a foosball tournament. One of the more memorable moments, however, had to be when David Faure grabbed an entirely different sort of keyboard and shared his skills as a jazz piano hacker with everyone.</p>

<p><iframe width="425" height="349" src="http://www.youtube.com/embed/cI3jpUP8rSE" frameborder="0" allowfullscreen></iframe></p>

<p>The team thanks Mario Fux, his family and the rest of the organization team for taking good care of us. Thank you also to the sponsors Swisscom, openSUSE and KDE e.V. (through the <a href="http://jointhegame.kde.org">supporting membership programme</a>) for financially supporting this meeting.</p>