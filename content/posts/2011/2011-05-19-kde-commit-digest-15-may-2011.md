---
title: "KDE Commit Digest for 15 May 2011"
date:    2011-05-19
authors:
  - "vladislavb"
slug:    kde-commit-digest-15-may-2011
comments:
  - subject: "Thanks"
    date: 2011-05-19
    body: "Just wanted to say thank you for another nice Commit Digest."
    author: ""
  - subject: "\"Issue cannot be found\""
    date: 2011-05-19
    body: "First the server was down for maintenance for some time, now I get a persistent \"Issue cannot be found\""
    author: ""
  - subject: "Thanks, the site is reachable"
    date: 2011-05-20
    body: "Thanks, the site is reachable now."
    author: "vladislavb"
  - subject: "wake from suspend for a selected alarm in KDE-PIM"
    date: 2011-05-23
    body: "This is really interesting option.... \r\nOption to wake from suspend for a selected alarm in KDE-PIM.\r\nI presume by suspend it can also be suspend2RAM/disk and even S5"
    author: ""
---
In <a href="http://commit-digest.org/issues/2011-05-15/">this week's KDE Commit-Digest</a>:

<ul>
<li>Further bugfixing, optimization and feature work in <a href="http://www.calligra-suite.org/">Calligra</a> including improved DOC, DOCX, WMF, MSO and SVM file support</li>
<li><a href="http://krita.org/">Krita</a> sees an updated Phong Bumpmap and <a href="http://ghns.freedesktop.org/">Get Hot New Stuff</a> support for presets</li>
<li>A group of database-related bugs fixed in <a href="http://www.digikam.org/">Digikam</a></li>
<!--break-->
<li>The Strigi Indexer has been moved to a separate process to allow for a more robust <a href="http://nepomuk.kde.org/">Nepomuk Service</a></li>
<li><a href="http://ktorrent.org/">KTorrent</a> sees a revamped Scanfolder plugin</li>
<li>Copy/paste support and "ctrl+mouse wheel" zooming integrated in <a href="http://edu.kde.org/step/">Step</a></li>
<li><a href="http://api.kde.org/4.0-api/kdelibs-apidocs/kdecore/html/classKCalendarSystem.html">KCalendarSystem</a> and the Locale KCM now support alternative (localized) Week Number Systems</li>
<li><a href="http://commit-digest.org/issues/2011-05-15/moreinfo/167741c41a0d48a688620dfa3543c1ec795daa4b/">New Date/Time widgets</a> developed by the KDE-PIM team moved to <a href="http://api.kde.org/4.0-api/kdelibs-apidocs/">KDE-Libs</a> alongside bugfixing</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> now supports raster graphics and has basic multihead support</li>
<li>Option to wake from suspend for a selected alarm in <a href="http://community.kde.org/KDE_PIM">KDE-PIM</a></li>
<li><a href="http://userbase.kde.org/NetworkManagement">NetworkManagement</a> sees bugfixing and automatic BSSID selection support</li>
<li>Support for MMS in <a href="http://www.kde.org/applications/internet/kget/">KGet</a> allowing multiple connections per download</li>
<li>Continued work on folder icons, this time 22x22, in <a href="http://www.oxygen-icons.org/">Oxygen</a></li>
<li>More robust DAAP protocol parsing in <a href="http://amarok.kde.org/">Amarok</a> alongside other bugfixing</li>
<li>Bugfixing and optimization in <a href="http://userbase.kde.org/Juk">Juk</a></li>
<li>Layout improvements in <a href="https://jontheechidna.wordpress.com/2010/07/05/introducing-qapt-and-the-muon-package-manager/">Muon</a></li>
<li>Bugfixing in <a href="http://www.kde.org/workspaces/">KDE-Workspace</a>, <a href="http://kmymoney2.sourceforge.net/">KMyMoney</a>, and <a href="http://utils.kde.org/projects/ark/">Ark</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-05-15/">Read the rest of the Digest here</a>.