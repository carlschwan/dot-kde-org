---
title: "Be Part of Krita's First Training DVD"
date:    2011-06-05
authors:
  - "Boudewijn Rempt"
slug:    be-part-kritas-first-training-dvd
comments:
  - subject: "wow"
    date: 2011-06-05
    body: "Awesome initiative, guys & girls! I look forward to the end result D:"
    author: ""
  - subject: "target audience"
    date: 2011-06-05
    body: "Will the training DVD be aimed at professional artists (who are merely new to Krita), or also at drawing/painting hobbyists or even newbies?"
    author: ""
  - subject: "Animtim - DVD audience"
    date: 2011-06-05
    body: "I'll try to get it as simple but complete as possible so anyone can learn from it, with strong basics for the beginner and some professional tips as well."
    author: ""
  - subject: "Animtim - target audience"
    date: 2011-06-05
    body: "I'll try to get it as simple but complete as possible so anyone can learn from it, with strong basics for the beginner and some professional tips as well."
    author: ""
  - subject: "donation progress"
    date: 2011-06-06
    body: "Will you show a button somewhere (Krita website?) that shows how much was donated already, and how much is still needed?"
    author: ""
  - subject: "Yes, we're working on that."
    date: 2011-06-06
    body: "Yes, we're working on that."
    author: "Boudewijn Rempt"
  - subject: "yaniyakov"
    date: 2011-06-06
    body: "I like it :)"
    author: ""
  - subject: "Congrats and a question about a tablet"
    date: 2011-06-06
    body: "This is a really great initiative. I already made a donation.\r\n\r\nI just want to make a question, my apologies in advance if it is not the best place to ask.\r\n\r\nAs I suppose, for a better drawing experience a tablet will be highly recommended. I've read in the past that Wacom tablets have good support on Linux, my question is they still work on Linux?, is anyone using Wacom Bamboo with Krita on Linux? if is not asking too much which kind works better in Krita?"
    author: ""
  - subject: "Thank you!"
    date: 2011-06-06
    body: "For your donation!\r\n\r\nAt the Krita sprint, the table was littered with wacom tablets :-)\r\n\r\nWacom tablets are still the best choice -- a near monopoly because of an excellent product. They are very well supported under Linux, but there's one gotcha: sometimes Qt and the wacom drivers march out of step, and wacom support can be broken for some time, sometimes.\r\n\r\nThe best wacom to get is the Intuos -- it's more expensive than bamboo but is much, much nicer to work with. If you want to really get the feel of what a tablet can offer you, get an Intuos. It's higher resolution, more pressure levels, tilt, art pen and airbrush. Get at least A5 -- some people think A4 is too big, and A6 is definitely too small.\r\n\r\nFortunately, previous generation Intuos tablets are still very useful, so it's a good idea to look out for a second hand tablet.\r\n\r\n"
    author: "Boudewijn Rempt"
  - subject: "Good question!"
    date: 2011-06-06
    body: "I want to use Krita with something like Wacom Bamboo, Pen-n-Touch too.\r\n\r\nBeen following...\r\nhttp://sourceforge.net/apps/mediawiki/linuxwacom/index.php?title=Main_Page\r\nbut haven't taken the plunge. \r\n\r\nKrita looks like the best excuse for getting a new toy B^)\r\n\r\nThank you Krita and Calligra\r\n"
    author: ""
  - subject: "it works"
    date: 2011-06-06
    body: "the bamboo -- it should work out of the box with any distribution released in the past three or so years. There are some exceptions, where you hit temporary snags. My favorite is OpenSUSE, which hasn't failed me for years, but all other current distributions can be made to support wacom tablets with relatively little trouble."
    author: "Boudewijn Rempt"
---
In the last year, thanks to the help from the many sponsors in the KDE community and beyond, Krita has grown into a mature and polished application for digital artists. We've already seen <a href="http://krita.org/showcase">many great pieces of art</a> being produced with Krita! 
 
But there's one thing still lacking--besides a Windows installer, which is coming--and that's good training material. The Krita team is working on a manual, but manuals aren't the kind of training material artists really want.

At <a href="http://dot.kde.org/2011/06/02/what-happens-when-artists-and-developers-come-together-2011-krita-sprint">the recent Krita sprint</a>, artist Animtim suggested a plan for creating a training DVD for Krita, and we were very happy. Animtim is now a full member of the Krita team and we're confident that his plan will bear fruit! But <strong>your</strong> donations are needed to make it happen.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 480px;"><embed src="http://blip.tv/play/AYK_lHoA" type="application/x-shockwave-flash" width="480" height="300" allowscriptaccess="always" allowfullscreen="true"></embed><br />Krita Training DVD Trailer (<a href="http://blip.tv/file/get/Krita-CreatingComicsWithKritaPreTrailer537.webm">Download in WebM format</a> )</div>Basically, the plan is: Animtim will use Krita to create a series of tutorials on how to create a comic book, end to end. The DVD will demonstrate--step-by-step--how to prepare the page, sketch characters, ink with bitmap and vector tools, and add color. Finally, it will show how to create a print-ready PDF with Scribus or publish the comic to the web. This Krita tutorial series will be useful for anyone who wants to create comics in print and on the web. As you can see from the trailer, it is a blast. A by-product of this effort will be a full 20-page comic, a printed copy of which will accompany the final DVD.

The plan has two stages: first, Animtim will create all this material, with a permissive Creative Commons license, of course! That will take about two months. We need your help! We want him to work full-time on this project, which will take about &euro;2000 altogether. Your donations are essential! 

If this works out, the next stage is the actual DVD production. Before Krita 2.4 is released in October this year, we will open pre-ordering for the actual DVD's. Those who donate will get a discount for the whole package of DVD and comic; everyone who pre-orders will get their copy personally dedicated to them by the artist. 

The <a href="http://krita.org/index.php&option=com_content&id=20">2009 Krita community donation drive</a> made it possible for <a href="http://dot.kde.org/2009/12/02/krita-team-seeking-sponsorship-take-krita-next-level">Lukáš Tvrdý to work for several months</a>, improving Krita performance, stability and usability. Your donations for a training DVD will give people hands-on experience with sketching and painting, whether they are creating comics or using Krita in other ways.
 
Thanks for your help to enable Animtim and the Krita team reach this goal!

<h2>Donate</h2>

You can donate via PayPal using the button on the left or email <a title="Boudewijn Rempt" mce_href="mailto:boud@kde.org" href="mailto:boud@kde.org">boud@kde.org</a> to arrange an alternative payment method.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="WZTQME3Y3JFW2">
<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypal.com/nl_NL/i/scr/pixel.gif" width="1" height="1">
</form>