---
title: "KDE Commit-Digest for 4th September 2011"
date:    2011-09-12
authors:
  - "mrybczyn"
slug:    kde-commit-digest-4th-september-2011
---
In <a href="http://commit-digest.org/issues/2011-09-04/">this week's KDE Commit-Digest</a>:

<ul>
<li>Much work done on <a href="http://community.kde.org/Plasma/Active">Plasma/Active</a>, including homescreen loading, AppletContainer and activity switching optimization.</li>
<li><a href="http://userbase.kde.org/Dolphin">Dolphin</a> gets a Bazaar version control plugin.</li>
<li><a href="http://kst-plot.kde.org/">Kst</a> now supports scripting.</li>
<li>Line modification system added in <a href="http://userbase.kde.org/Kate">Kate</a>.</li>
<li><a href="http://userbase.kde.org/Skrooge">Skrooge</a> gets a copy menu for cells.</li>
<li><a href="http://owncloud.org">Owncloud</a> has an initial implementation of password reset and calendar updates.</li>
<li>There are optimizations in <a href="http://userbase.kde.org/Plasma/Public_Transport">Public Transport</a> and <a href="http://www.calligra-suite.org/">Calligra</a>.</li>
<li><a href="http://userbase.kde.org/NetworkManagement">Network Management</a> sorts connections by type and then by activation state.</li>
<li>libqapt supports APT MultiArch.</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> gets bugfixes, mostly in support of file formats.</li>
</ul>

<a href="http://commit-digest.org/issues/2011-09-04/">Read the rest of the Digest here</a>.
<!--break-->
