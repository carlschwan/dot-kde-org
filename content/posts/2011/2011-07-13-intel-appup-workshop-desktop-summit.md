---
title: "Intel AppUp Workshop at Desktop Summit"
date:    2011-07-13
authors:
  - "kallecarl"
slug:    intel-appup-workshop-desktop-summit
---
The DesktopSummit 2011 team is pleased to announce the <a href="http://www.appup.com/">Intel AppUp<sup><small>SM</small></sup></a> <a href="http://ce1.com/intel/2011/berlinappuplab2/">Application Lab: MeeGo Series</a>. The session will take place at the Humboldt University in Berlin, Germany as part of the <a href="https://desktopsummit.org/">Desktop Summit</a>. <a href="http://www.intel.com/">Intel&#174;</a> is the Platinum Sponsor of the Desktop Summit. 

<img style="float:right; padding: 5px; padding-right: 0; border: 0;" src="http://dot.kde.org/sites/dot.kde.org/files/Intel_AppUp_Logo_black.png"  />Scheduled from 15:00-18:00 on August 10th, this free, hands-on training aims to show application developers how to distribute and monetize their applications through the Intel AppUp Developer Program and its community.
<!--break-->
Intel AppUp is a web store focused on applications built for the low-power Intel Atom&#153; chips, commonly used in netbooks and mobile devices. Although current store apps are built for other mobile operating systems, the August 10th event will concentrate on the MeeGo Linux-based mobile operating system.

At the session, developers will be shown the advantages of the MeeGo platform. Available tools, code validation and the <a href="http://appdeveloper.intel.com/en-us/community">AppUp Developer community</a> will also presented. The session is intended to help session developers reduce their time-to-market using AppUp developer program support for multiple OSs and runtimes.

Capacity for this event is limited to 150 seats. Book your free spot soon. To register for the Intel AppUp Application Lab, you must be logged in at the Intel AppUp developer program <a href="http://appdeveloper.intel.com/events">event website</a>.