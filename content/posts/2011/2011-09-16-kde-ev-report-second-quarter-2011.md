---
title: "KDE e.V. Report for Second Quarter 2011"
date:    2011-09-16
authors:
  - "kallecarl"
slug:    kde-ev-report-second-quarter-2011
---
<a href="http://ev.kde.org/">KDE e.V.</a>, the non-profit organization that supports the KDE community in legal and financial matters, has published its <a href="http://ev.kde.org/reports/ev-quarterly-2011_Q2.pdf">report for the second quarter of 2011</a> (pdf). The report introduces the KDE e.V. administrative staff, and has information about the various contributor sprints and events that KDE members attended in the second quarter of 2011.<br />

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://ev.kde.org/reports/ev-quarterly-2011_Q1.pdf"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEeVReport1Q2011225px.png" /></a></div>
Not announced previously...the delightful <a href="http://ev.kde.org/reports/ev-quarterly-2011_Q1.pdf">First Quarter 2011 Report</a> (pdf) is also available. It features the KDE Conference in India and a wealth of related imagery, especially the KDE India logo that Eugene Trounev created for the conference. Many thanks to Rob Oakes for the design and creation of the report. 





