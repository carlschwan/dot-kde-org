---
title: "January Stable Updates Available"
date:    2011-01-07
authors:
  - "sebas"
slug:    january-stable-updates-available
comments:
  - subject: "Link correction"
    date: 2011-01-07
    body: "Correcte link: http://www.kde.org/announcements/announce-4.5.5.php"
    author: "tuxcanfly"
  - subject: "Corrected!"
    date: 2011-01-07
    body: "Thanks for letting us know!"
    author: "oriol"
---
Today, KDE has <a href="http://www.kde.org/announcements/announce-4.5.5.php">issued</a> a series of stable updates to the Plasma workspaces, the various applications and the KDE development frameworks, versioned 4.5.5. Between 4.5.4 and 4.5.5 there have been 54 commits to the codebase, so the somewhat meagre changelog does not include all the fixes. The usual rules apply, so this update only includes bugfix and translation update. KDE recommends everyone running 4.5.4 or earlier versions to install this update.