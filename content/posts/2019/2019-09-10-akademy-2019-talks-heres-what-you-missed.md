---
title: "Akademy 2019 Talks: Here's What You Missed"
date:    2019-09-10
authors:
  - "unknow"
slug:    akademy-2019-talks-heres-what-you-missed
comments:
  - subject: "Our first goals and the new goals"
    date: 2019-09-11
    body: "<p>I hope we can stress more that our first goals are atill ongoing, and crucially important! In fact, we are making them foundational to everything we do in KDE. The new goals enrich us, and I hope in time we can make them foundational as well. We're building well.</p>"
    author: "Anonymous"
---
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="https://devel-home.kde.org/~duffus/akademy/2019/groupphoto/"><img src="/sites/dot.kde.org/files/akademy2019-groupphoto_1500.jpg" width="800" /></a></figure>
</div>

According to the now traditional schedule, Akademy 2019 started with two days of conference talks. Hosted by <a href="https://unixmib.github.io/">unixMIB</a> at the University of Milano-Bicocca in Milan, Italy, the central conference of the KDE community attracted more than a hundred attendees during this past weekend. Many of them were attending Akademy for the first time ever, which is always a reason to celebrate.

For those of you who were not able to join us, we've prepared a recap of all the talks from this year's Akademy. The conference program on both Saturday and Sunday was split into two tracks after the lunch break, and included plenty of time for socializing (and hacking!) in between.

<h2>Day 1 - Saturday, September 7: Goals, Reports, and the Future of Qt</h2>

Akademy 2019 started in the morning of September 7 with an introductory session by Lydia Pintscher, President of KDE e.V., followed by the first keynote. In the keynote, <a href="https://dot.kde.org/2019/08/29/lars-knoll-cto-qt-and-keynote-speaker-akademy-2019">Lars Knoll from Qt presented the path towards Qt 6</a> all the way from the very beginning of the project. Lars also spoke of what upcoming changes in Qt 6 may potentially impact the KDE ecosystem. 

The next batch of talks was dedicated to the KDE community goals. Ivan Čukić started by presenting the progress of the Privacy and Security goal in his talk "Everything to hide: Helping protect the privacy of our users". Ivan pointed out that security and privacy should come before usability, even if some users hate it, because it's our duty and responsibility to protect them. 

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/security.jpg"><img src="/sites/dot.kde.org/files/security.jpg" width="800" /></a><br /><figcaption>Ivan shows how to capture a password from an insecure application.</figcaption></figure>
</div>

Eike Hein talked about the Usability and Productivity goal and wondered: "Are we there yet?". Massive improvements have been made to KDE software as part of this goal, and Eike emphasized the importance of communicating this progress (as illustrated in weekly blog posts by Nate Graham). The achievements of the third community goal - Onboarding New Contributors - were presented by Neofytos Kolokotronis, who listed the adoption of Matrix as a communication tool, the on-going adoption of GitLab, and the creation of the KDE Welcome team as some of the major moments. 

After looking back at the previous set of goals, it was time to look forward to the new ones. During the panel with Ivan Čukić, Eike Hein, and Neofytos Kolokotronis, Lydia Pintscher announced <a href="https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps">the three new goals that the KDE community is going to focus on</a>. The creators of the goal proposals spent some time talking about their plans and tasks that will kick off the new goals.

In the afternoon round of quick talks, Adriaan de Groot presented <a href="https://github.com/adriaandegroot/QuatBot">QuatBot</a>, a meeting-managing bot he wrote for the Matrix IM service, and talked about the power and versatility of KDE Frameworks. Attendees also got a chance to hear how Carl Schwan brought in new contributors from Reddit and Aleix Pol dispensing valuable advice on how to organize a sprint.

Over in the Security track, Albert Astals talked about the cool ways developers can use oss-fuzz to test their code, and encouraged KDE developers to use it for projects such as Baloo, kfilemetadata, and PIM-related code. Volker Krause presented parts of the work carried out for the Privacy goal in his talk "Secure HTTP Usage", and warned about the importance of having secure defaults in KDE software.

The Community session included a talk on building developer portals by Ivana Isadora Devcic, followed by Ray Paik's talk on making a difference in the community. As a Community Manager at GitLab, Ray shared his experience with identifying crucial community metrics, attracting new contributors, and improving leadership and inclusivity efforts.

Meanwhile, the tech talk session continued with Marco Martin and Bhushan Shah discussing the future of Plasma on embedded devices. They rightfully pointed out that the assumption your software will only be used on a desktop is not true anymore, and explained how KDE Frameworks enable creating software for different platforms. Aleix Pol talked about the details of optimizing Plasma to run fast on low-end hardware; more specifically, on the Pinebook. Aditya Mehra presented a demo of Plasma and Mycroft being used to voice-control a car, and Kai Uwe Broulik gave an in-depth look into the overhauled notification system shipped with the latest version of Plasma. 

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/20190907_154401_1500.jpg"><img src="/sites/dot.kde.org/files/20190907_154401_1500.jpg" width="800" /></a><br /><figcaption>Aditya shows us how some day KDE tech may control your car.</figcaption></figure>
</div>

The first day of Akademy 2019 closed with reports by Google Summer of Code students developing fresh new code for KDE, and the KDE e.V. Board and Working Group reports that provided an insight into growth and health of the KDE community.

<h2>Day 2 - Sunday, September 8: New Technology, FOSS Revolution in Italy, and Akademy Awards</h2>

The second day of Akademy 2019 opened with a keynote <a href="https://dot.kde.org/2019/09/05/developers-italia-and-new-guidelines-let-open-source-revolution-start-interview-leonardo">"Developers Italia and the New Guidelines: Let the Open Source Revolution Start" by Leonardo Favario from the Team Digitale IT</a>. Leonardo presented the work that his team has been doing to establish guidelines for Free and open source software distribution in the Italian administration. Continuing on a similar topic, Michiel Leenaars talked about NGIO (Next Generation Internet Zero); a EU initiative focused on helping non-profit organizations build a better Internet for everyone.

The tech talks on Sunday were fascinating, with new, innovative technology introduced left and right. Cristoph Haag explained how Collabora made Plasma desktop usable in a Virtual Reality environment, and set up demos that the attendees could play with during the day. Trung Thanh Dinh showed how AI face recognition can be used in <a href="https://www.digikam.org">digiKam</a>, KDE's photo management app, and Eike Hein presented a completely new KDE application called <a href="https://kirogi.org/">Kirogi</a>, which provides a FLOSS ground control for consumer drones that works on mobile devices.

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/kirogi.jpg"><img src="/sites/dot.kde.org/files/kirogi.jpg" width="800" /></a><br /><figcaption>Eike points to the skies, which is where KDE is going next with Kirogi.</figcaption></figure>
</div>

In the afternoon sessions, Katarina Behrens from the Document Foundation talked about integrating LibreOffice products with KDE Plasma, while Timothée Giet and Aiswarya Kaitheri Kandoth told the story of how a single floppy disk with LaTeX on it resulted in schools using GNU/Linux and GCompris in Kerala, India. 

Volker Krause gave two more talks - one about the development and usage of <a href="https://www.volkerkrause.eu/2019/03/02/kpublictransport-introduction.html">KPublicTransport</a>, a framework for interacting with data from public transport operators; and another on how the limitations of the Android development platform impact KDE Frameworks. In another developer-oriented talk, Daniel Vràtil gave his perspective on using C++ to build APIs. Attendees also heard from Caio Jordao Carvalho, who presented the progress on kpmcore, the heart of KDE's partitioning and disk management tools.

Meanwhile, a session on different ways to package and distribute KDE software was chaired by Dan Leinir Turthra Jensen, with participants explaining advantages and shortcomings of different solutions (AppImage, Flatpak, Snap, Steam, Google Play...). 

The session was followed by two community-related talks. In "What We Do in the Promos", Paul Brown gave a realistic look into how people outside the FOSS bubble perceive (or do not perceive) KDE software, and explained the reasoning behind activities carried out by KDE Promo. Afterwards, Aniketh Girish explained how code reviews can be toxic and put off new contributors, so he offered some advice to prevent that. Last but not least, Dan Leinir Turthra Jensen presented the "Get Hot New Stuff" project and its development.

Following the lightning talks from Akademy 2019 sponsors, the second day of the conference closed with the announcement of Akademy Awards winners:

<ul>
<li><b>Best Application:</b> Marco Martin for work on the Kirigami framework</li>
<li><b>Best Non-Application:</b>  Nate Graham for persistent work on the "KDE Usability & Productivity" blog</li>
<li><b>Jury Award:</b>  Volker Krause for long-term contributions to KDE including KNode, KDE PIM, KDE Itinerary and the UserFeedback framework</li>
</ul>

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/DSC02595_CC_1500.jpg"><img src="/sites/dot.kde.org/files/DSC02595_CC_1500.jpg" width="800" /></a><br /><figcaption>The organizers win a special award for an excellent Akademy.</figcaption></figure>
</div>

Akademy 2019 continues this week with daily BoF (Birds of a Feather) sessions, meetings, and various activities that help us strengthen the community bonds. The <a href="https://dot.kde.org/2019/09/09/akademy-2019-monday-bof-wrapup">recap video of the first BoF day</a> is already available -  stay tuned for more. And for something completely different, take a look at <a href="https://ervin.ipsquad.net/photos/akademy-2019/">the sketchnotes from Akademy 2019 talks</a> by Kevin Ottens.

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/09-DevPortal.png"><img src="/sites/dot.kde.org/files/09-DevPortal.png" width="800" /></a><br /><figcaption>Kevin sketches Akademy talks.</figcaption></figure>
</div>

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href="https://devel-home.kde.org/~duffus/akademy/2019/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2019-groupphoto_1500.jpg" /></a><br />Akademy 2019, Milano</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->

