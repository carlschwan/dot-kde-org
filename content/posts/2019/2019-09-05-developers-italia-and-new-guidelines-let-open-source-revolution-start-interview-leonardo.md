---
title: "Developers Italia and the New Guidelines: Let the Open Source Revolution Start! An Interview with Leonardo Favario"
date:    2019-09-05
authors:
  - "unknow"
slug:    developers-italia-and-new-guidelines-let-open-source-revolution-start-interview-leonardo
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/leo.jpg"><img src="/sites/dot.kde.org/files/leo.jpg" /></a><br /><figcaption>Leonardo Favario, Open Source Project Leader at the Italian Digital Transformation Team.</figcaption></figure> 

<i>Leonardo Favario is the Open Source Project Leader at the <a href="https://teamdigitale.governo.it/en/team">Italian Digital Transformation Team</a>. Italy has an ambitious agenda to move government IT to open source. In principle, all software written by government should be published as open source. This is a big change from the past and requires many changes in how software is being developed. </i>

<i>Leonardo will present this work to the KDE community in an <a href="https://conf.kde.org/en/akademy2019/public/events/159">Akademy keynote</a> on Sunday, September 8. We were interested in hearing more about this initiative, and Leonardo kindly sat down with us for an interview.</i>

<span style="color:#f55;font-weight:bold">Jos van den Oever:</span> Hello Leonardo, you’re the speaker of the second keynote at Akademy 2019. Thank you for agreeing to this interview. We’re very happy to have a keynote from our Italian host with such wonderful Free Software developments.

<span style="color:#55f;font-weight:bold">Leonardo Favario:</span> Hello Jos and thanks for contacting us! I can do an interview even though it's been quite hectic lately.

<span style="color:#f55;font-weight:bold">Jos:</span> <a href="https://teamdigitale.governo.it/en/people/leonardo-favario.html">Your bio</a> on the <a href="https://teamdigitale.governo.it/en/">Team Digitale</a> website is wonderfully written. You have been in software and even Free Software from an early age. Do you contribute to any FOSS communities?

<span style="color:#55f;font-weight:bold">Leonardo:</span> Yes, I must admit that for me the Free Software movement was love-at-first-sight! I immediately felt it was a natural tool to empower people and I really enjoyed the thriving communities that were flourishing around even small pieces of wonderfully written code. As such, as many youngsters do, I jumped from channel to channel trying to fit all the small pieces together and get the complete puzzle in place. Soon I decided that lurking was not my way of being, so I started to create communities around Free Software, getting friends to work together. I am particularly fond of communities striving to improve education by using technology and that’s where I have been active lately, especially in Italy. One example is the <a href="https://open.edx.org/">Open edX</a> community where it’s possible to find a great combination of actors, ranging from full stack devs to educators, all trying to work together on the future of education. That’s something that I love about FOSS communities.

<span style="color:#f55;font-weight:bold">Jos:</span> You have a background in education and communication, which seems like a good fit for the work that Team Digitale does. How did you get into the team and what is your role there?

<span style="color:#55f;font-weight:bold">Leonardo:</span> I am a computer engineer by training, but I decided to get my PhD in Control and Computer Engineering to dive deeper into the research world and understand the ways we can still improve the world we live in through software.

Well, Team Digitale does exactly that! Formally speaking, its name is the “Italian Digital Transformation Team” and I felt a strong connection with each of those words. I was lucky to join this awesome combination of extremely talented folks at the very beginning of this year as an Open Source Project Leader. My role, as the name suggests, is focused on drafting strategies regarding Free and open source software for everything related to Developers Italia, the community of public service developers.

<span style="color:#f55;font-weight:bold">Jos:</span> Can you tell us more about Team Digitale? How did it come to be and where is it going?

<span style="color:#55f;font-weight:bold">Leonardo:</span> The Italian Digital Transformation Team (from now on Team Digitale) was born at the end of 2016 to build "the Operating System” for the country. We refer to the OS to indicate a series of fundamental components on top of which we can build simpler and more efficient services for the citizens, the Public Administration and businesses, through innovative digital products.

We are now exactly at the end of the third year of operations and a lot has been accomplished so far. First and foremost, the Team consists of around 35 people, generally way younger than the average age of the Italian Public Administration employees. These young people did not have a formal experience in government before joining. This allows us to tackle every issue from a different perspective, and I believe this is a great advantage in order to come up with innovative solutions. 

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/image.jpg"><img src="/sites/dot.kde.org/files/image.jpg" width="800" /></a><br /><figcaption>The Digital Transformation Team.</figcaption></figure>
</div>

From an organizational standpoint, Team Digitale is divided into subteams, each one focused on different topics ranging from APIs to Data Analytics.  

<span style="color:#f55;font-weight:bold">Jos:</span> Your presentation will cover <I>publiccode.yml</I>, a way to describe software projects. Can you give us a bit of background on this project, and tell us where it's headed?

<span style="color:#55f;font-weight:bold">Leonardo:</span> Exactly, I will talk about Developers Italia and the new open source catalog that we have been building during the past months. In fact, in order to make open source software easily discoverable and indexable, the Team created a catalog which is automatically populated by a crawler every night. As such, you may understand that we needed to collect information regarding each piece of software in a standard way to later publish them in our catalog. That’s why we crafted a metadata specification, called <I>publiccode.yml</I>. This helps to achieve exactly that goal. Consequently, by creating a specific publiccode.yml file for the software and inserting it into the source code repository, the government entity that is publishing the software can transparently communicate the metadata. Our crawler can automatically recognize it, extract the information from the file, create the related info page, and show the information in the catalog.

Technically speaking, the specification format is interesting because it can be written and read by non-tech experts as well; for example, by Public Administration (PA) managers. This can help its adoption in wider circles. 

<span style="color:#f55;font-weight:bold">Jos:</span> How is this information you collect used by Italians?

<span style="color:#55f;font-weight:bold">Leonardo:</span> Right now, there are packages in the catalog that are already reused by many local administrations. See for example <a href="https://developers.italia.it/en/software/cnr-consiglionazionaledellericerche-cool-jconon">the one for managing a public call</a> or <a href="https://developers.italia.it/en/software/c_a116-opencontent-openagenda">the software to insert and manage events</a>.

The existence of the catalog itself is a big change, because it is necessary to evaluate, study, and test an application before actually reusing it. The absence of a real browsable container makes such tasks difficult to accomplish. Since we started the catalog in late May of this year, we are witnessing a positive spread among different players, and this is reflected in the discussions arising in various communication channels.

<span style="color:#f55;font-weight:bold">Jos:</span> KDE projects have an <a href="https://www.freedesktop.org/software/appstream/docs/">AppStream</a> <a href="https://community.kde.org/Guidelines_and_HOWTOs/AppStream">description</a>. Is this comparable to publiccode.yml? 

<span style="color:#55f;font-weight:bold">Leonardo:</span> Yes and no. As far as I can see, the AppStream description goes into details which are relevant for packaging, managing dependencies, and so on. The publiccode.yml format, instead, is designed for reusability: it contains standard metadata (which may overlap with some of the typical entries of AppStream like, title, description, development state...). However, the publiccode.yml format introduces some information that a PA expects to find, such as the maintenance contracts together with the contractor details, or the information regarding the legal context in which the software has been designed.

Furthermore, it contains information regarding the tree of forks (which is useful for understanding the root that originated the current version) or information regarding the dependencies, which can be open or closed. This is crucial when deciding if a piece software can be reused inside a PA environment.

Additional keys have been specifically crafted to endorse the PA use cases in order to be able to easily fulfill all the possible needs during the selection and validation phase.

<span style="color:#f55;font-weight:bold">Jos:</span> Is the Italian government pushing towards having most off-the-shelf and custom software published in the open? Do you have any insight into the percentage of the tax money that is spent on code that goes public, versus the code that stays closed?

<span style="color:#55f;font-weight:bold">Leonardo:</span> Well, I want to say that the Italian legislation regarding Free and open source software is one of the most advanced in the world (but I’m happy to be proven wrong). In fact, we have the Digital Administration Code which states - in articles 68 and 69 - that PAs must prefer FOSS in the acquisition phase, and must always release their code when developing something from scratch. Team Digitale played a consistent role in this initiative. The new guidelines regarding the acquisition and reuse (which are the topic of my talk) reinforce those statements, and underline how to properly complete the process of releasing software. As such, the Developers Italia catalog is a strong action in this direction - helping PAs find and publish open source software. 

<span style="color:#f55;font-weight:bold">Jos:</span> Do you see communities forming around government source code? Are they mostly Italian or also international?

<span style="color:#55f;font-weight:bold">Leonardo:</span> I would say that in the last two to three years, something started happening, and I am not just happy but optimistic about this. In many conferences and events around the world, the phrase “Open Government” has been circulating a lot. If we don’t start to understand that we have to go FOSS-by-default, it will continue to be a just buzzword. So yes, I am excited to see that some communities are starting to form, and I am also proud to state that we are going exactly in this direction with Developers Italia. For example, our chat channel has more than 2600 people subscribed, and the Forum reached nearly 90 000 page views in the last month. This clearly indicates that if we put private citizens, Public Administrators, and small/medium enterprises all together in the same channel, it is possible to start a chain reaction that can lead to positive outcomes. I personally believe that we are still in the very early stages of this sort of community building, but the future looks golden. 

<span style="color:#f55;font-weight:bold">Jos:</span> The Government of Belgium is extending the Public Sector Information directive <a href="https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/open-data-directive">to cover computer programs</a> so even if code is not actively published, citizens can still request it. What is the status of Italy in this regard?

<span style="color:#55f;font-weight:bold">Leonardo:</span> As I already mentioned above, our legislation framework is quite strong when it comes to the ad-hoc designed software, or when dealing with an acquisition of an already existing one. However, a lot of software has not been released yet and this has to change in the future.

There are many reasons why this is happening; we could have a discussion around this for weeks. But, I believe that the freshly published guidelines go exactly in the right direction, since they contain practical down-to-earth guides on how to design, write, and maintain software in the open. This practical approach is starting to pay off: in just a few weeks, we have already collected <a href="https://developers.italia.it/en/software">more than 40 software packages</a> in the catalog, and many more are coming.

I believe it’s a matter of getting things started, and finding a way to provide support where and when it’s needed. Let’s catch up in a year and I’ll let you know if my optimistic view was correct. :)

<span style="color:#f55;font-weight:bold">Jos:</span> I've worked in the Dutch government and that is still mostly a Windows environment. It's impossible to do work when avoiding proprietary software. How does Team Digitale solve this problem?

<span style="color:#55f;font-weight:bold">Leonardo:</span> We prefer Free and open source technologies when they are available, according to our national policy. Our initiative is mainly focused on ensuring that custom software developed by or for the government gets released as open source, since the expense for custom software is huge and its quality is quite low. We push this FOSS route by showing that it’s convenient and it delivers quality.

That said, our team members don’t have particular restrictions regarding the OS or the software they use, and this freedom allows each one of us to pick the tools that best fit our needs. 

<span style="color:#f55;font-weight:bold">Jos:</span> Can Team Digitale help reduce the dependency of government on proprietary cloud solutions?

<span style="color:#55f;font-weight:bold">Leonardo:</span> Right now the Italian panorama is highly fragmented with regard to IT infrastructure, and a strong cloud migration strategy is needed to improve the reliability of services and their security. So we have a dedicated team handling such issues, tracing possibilities for different use cases.

Of course, the team’s mission is to facilitate the migration while avoiding the introduction of new vendor lock-in or strong dependencies on proprietary software.

<span style="color:#f55;font-weight:bold">Jos:</span> Do you use any KDE software and if so, what are your impressions? What do you hope to achieve with your presentation and your presence at Akademy?

<span style="color:#55f;font-weight:bold">Leonardo:</span> Personally, yes. I’ve been using KDE for many years and I just can’t live without Kdenlive! Hopefully I’ll be able to contribute to it in the future. 

Regarding Akademy I’m excited, it’s a great opportunity to get to know the great KDE community, show what we’re doing for the FOSS ecosystem, and learn a lot! See you there!

<hr />

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href="http://devel-home.kde.org/~duffus/akademy/2018/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2018-groupphoto-teensie.jpg" /></a><br />Akademy 2018, Vienna</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->