---
title: "Host Akademy 2020 in your City!"
date:    2019-12-27
authors:
  - "unknow"
slug:    host-akademy-2020-your-city
---
By Aleix Pol

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/akademy2019-groupphoto_1500.jpg" alt="" width="800" />

Hosting an event is a big and significant way of contributing to Free Software. One of the biggest challenges in international distributed teams like KDE is communicating effectively with one another. <a href="https://akademy.kde.org/">Akademy</a>, the yearly global conference of the KDE community, solves that by bringing the community together in one place, allowing us to share what we have been up to and have it reach its potential.

By organising Akademy we are then turning one of our weak points into a strength. We get to work together like a local team does, while remaining flexible and geographically distributed for most of the rest of the year. It becomes therefore one of the best ways for Free Software to thrive in your area.

<h2>What is Akademy</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 400px"><a href="/sites/dot.kde.org/files/kirogi_cropped.jpg"><img class="img-fluid" src="/sites/dot.kde.org/files/kirogi_cropped.jpg"/></a></figure>

While Akademy has evolved over the years, its main structure remains similar: We have two conference days, the KDE e.V. Annual General Meeting and few days with smaller meetings and trainings. Akademy is open for everyone to join and participate, regardless of their background, studies or origin.

We would like you to consider hosting Akademy. We could look into doing it in 2020, although if you think this is too short-notice, 2021 could also be discussed.

You can find the full description of what's necessary <a href="https://ev.kde.org/akademy/CallforHosts_2020.pdf">in this simple-to -follow brochure</a>. <a href="mailto:akademy-proposals@kde.org">Reach out to the KDE e.V. Board and the Akademy team</a>  and put your thoughts in action.
<!--break-->