---
title: "Visit our Booth at OpenExpo Europe"
date:    2019-03-02
authors:
  - "Paul Brown"
slug:    come-and-see-us-openexpo-europe
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/OpenExpo_panorama.png" alt="OpenExpo 2018" width="800" />

<b>On the 20th of June, KDE will be attending <a href="https://openexpoeurope.com/">OpenExpo Europe 2019</a>.</b>

The event is held in Madrid, Spain, and the organisers have kindly given us a space on the exhibition floor. We will be showcasing the best of what KDE has to offer in the business world. This will include devices that will show off the versatility and potential of Plasma and Plasma Mobile on everything - from mobiles, embedded devices, SBCs and low-powered devices (like the <a href="https://www.pine64.org/?page_id=3832">Pinebook</a>), to its capability for adapting to vehicle infotainment systems and high-end ultrabooks, like the <a href="https://slimbook.es/en/kde-slimbook-2-ultrabook-laptop">KDE Slimbook</a>.

We will also be running videos and slide shows demonstrating the flexibility of Plasma, Plasma Mobile and all our applications on all platforms, and informing attendees how KDE Frameworks, such as <a href="https://kde.org/products/kirigami/">Kirigami</a>, can be useful for fast and flexible multiplatform development.

This is an event aimed mainly at businesses that want to work with other businesses, so if you or your company would like to get in touch with other IT companies, especially Spanish ones (which is what we want to do), this may be a good chance to broaden your market. 

For those looking to share their knowledge, <a href="https://openexpoeurope.com/oe2019/how-to-be-part/">the OpenExpo Europe CfP (Call for Participation) is also still open</a> and they are looking for speakers.

Mark your calendars, come and visit our stand, and keep up to date with all the new stuff KDE is working on!
<!--break-->