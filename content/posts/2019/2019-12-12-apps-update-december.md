---
title: "Apps Update for December"
date:    2019-12-12
authors:
  - "Paul Brown"
slug:    apps-update-december
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/desktop.png" alt="" width="800" />

Creating new applications is the easy part. Maintaining them, making them safer and faster and adding features that make them more useful to users is what marks the difference between one-shot wonders and solid tools you can trust and enjoy for years. That is why KDE developers are constantly renewing and updating their applications, making them more reliable, more useful, and in general, just better.

What follows is just a minor sample of what you can expect from the latest round of <a href="https://kde.org/announcements/releases/2019-12-apps-update/">updates for  applications made by the KDE community over the last month</a>:

<h2><a href="https://kde.org/applications/office/org.kde.calligraplan">Calligra Plan</a> is Back</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kplan.png"><img src="/sites/dot.kde.org/files/kplan.png" width="400" /></a><br /><figcaption>Calligra Plan lets you plan your projects in detail.</figcaption></figure> 

Calligra Plan, KDE's project planning and management tool, gets its first big update in two years.

In case you were not aware, Plan helps you manage small and large projects which require multiple resources. In order for you to model your project, Plan offers different types of task dependencies and timing constraints. You can define your tasks, estimate the effort needed to perform each, allocate resources and then schedule the project according to your needs and the actual resources available.

One of Plan's strengths is its excellent support for Gantt charts. Gantt charts help you plan, coordinate, and track specific tasks in a project. Using Gantt charts in Plan you will be able to better monitor your project's workflow.

<h2><a href="https://kdenlive.org">Kdenlive</a> Pumps up the Volume</h2>

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png"><img src="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" width="400" /></a><br /><figcaption>Kdenlive rocking a brand new audio mixer.</figcaption></figure> 

Kdenlive developers have been adding new features and squashing bugs like crazy -- the latest version alone comes with more than 200 changes.

A lot of work has gone into improving support for audio. In the "bugs solved", department they have gotten rid of an error that would eat up memory. They have also made saving audio thumbnails much more efficient.

But the most exciting new feature is that Kdenlive now comes with a spectacular sound mixer (see image). Developers have also added a new audio clip display in the clip monitor and the project bin so you can better synchronize your moving images with the soundtrack.

<h2>Für <a href="https://kde.org/applications/multimedia/org.kde.elisa">Elisa</a></h2>

Talking of sound, Elisa is one of KDE's most popular up-and-coming music players. Elisa belongs to the deceptively simple, very light, very good-looking variety of players, with an intuitive and elegant interface and, in its latest version, Elisa has upgraded its looks even further to adapt better to High DPI screens. It also now integrates better with the looks other KDE applications.

Indexing music files has also improved and Elisa now supports web radios and comes with a few examples for you to try.

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 800px;"><img src="/sites/dot.kde.org/files/elisa.png" alt="The Elisa music player."><br />The Elisa music player.</figure></div>

<h2><a href="https://community.kde.org/KDEConnect">KDE Connect</a>: Let Your Phone Rule your Desktop</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/KDEConnect.png"><img src="/sites/dot.kde.org/files/KDEConnect.png" width="250" /></a><br /><figcaption>KDE Connect now lets you control<br />the global volume of your system.</figcaption></figure> 


Most people who get to know KDE Connect, end up raving about it just because of how darned useful it is. 

The latest version of KDE Connect packs even more features. One of the more noticeable is that there is a new SMS app that lets you read and write SMS from your computer with the full conversation history.

Developers are also adding new functionalities to existing features to make them even more useful. For example, you could already use KDE Connect to control the volume of media playing on your desktop, say, in VLC. But now you can use KDE Connect to also control your system's global volume from your phone. When giving a talk, you can control your presentation using KDE Connect to flip forward and back through your slides, and apart from integrating with other KDE apps, you can now also send files from Thunar (Xfce's file manager) and Elementary applications such as Pantheon Files.

Talking of other platforms, you can now run the mobile component of KDE Connect not only on Android, but also on all those mobile Linux platforms we'll be seeing in upcoming devices like the PinePhone and the Librem 5. The new version also provides features for desktop-to-desktop users, such as media control across desktops, remote input, device ringing, file transfers and running commands.

<h2>And Much More</h2>

But that is not all by any means: <a href="https://userbase.kde.org/Dolphin">Dolphin</a>, <a href="https://kde.org/applications/utilities/org.kde.spectacle">Spectacle</a>, <a href="https://kde.org/applications/office/org.kde.okular">Okular</a> and dozens of other applications have included new features you are sure to find useful. Even more projects, broaching apps, libraries and frameworks, have tweaked their code making them more stable and secure.

If you want to get an idea of the full range of changes, visit the <a href="https://kde.org/announcements/releases/2019-12-apps-update/">official release announcement</a>, or check out the <a href="https://kde.org/announcements/changelog-releases.php?version=19.12.0">changelog</a> for every single detail of what has changed.

Getting applications made by KDE is also now easier: most are now available as <a href="https://flathub.org/apps/search/kde">Flatpaks</a>, <a href="https://snapcraft.io/search?q=kde">Snaps</a> and AppImages. You just have to download them and they run straight out of the box. Many programs are also available for more platforms, such as Android, macOs and Windows. <a href="https://krita.org">Krita</a> and Okular have been available in the Microsoft Store for some time now, and they have recently been joined by <a href="https://kde.org/applications/office/org.kde.kile">Kile</a>, a user-friendly LaTeX document editor.

Distributions will be updating their own repos and making the new versions available to Linux users over the next few weeks. Look out for your updates!
<!--break-->