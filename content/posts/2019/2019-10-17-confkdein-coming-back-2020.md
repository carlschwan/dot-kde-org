---
title: "conf.kde.in Is Coming Back In 2020"
date:    2019-10-17
authors:
  - "unknow"
slug:    confkdein-coming-back-2020
---
<div style='text-align: center'>
<figure  style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/mait-1_cropped.jpg"><img src="/sites/dot.kde.org/files/mait-1_cropped.jpg" width="800" /></a></figure>
</div>

<b>Join us for <a href="https://conf.kde.in/">conf.kde.in</a> from the 17th to 19th of January 2020 in Delhi, India.</b>

conf.kde.in 2020 will focus on promoting Free and Open Source software, including (but not limited to) Qt and KDE products.

<h2>The Venue</h2>

conf.kde.in 2020 will be held in <a href="https://www.mait.ac.in/">Maharaja Agrasen Institute of Technology</a>, located in Rohini, Delhi, India. MAIT was established by the Maharaja Agrasen Technical Education Society and promoted by well-known industrialists, businessmen, professionals and philanthropists. The aim of MAIT is to promote quality education in the field of Technology.

MAIT endeavors to provide industry-relevant education and training through its well-crafted and practical training programs for the students in different semesters of their courses. The campus is composed of 10 blocks with a learning resource center. MAIT has been ranked as the 10th best private engineering institute in India by the Dataquest T-School Survey. MAIT always supports Free and Open Source communities and tech-related activities.

<h2>About conf.kde.in</h2>

conf.kde.in started in 2011 at RVCE in Bangalore as a 5-day event with 300 participants. This kicked off a series of KDE events in India. We held a KDE Meetup in 2013, and another conf.kde.in 2014 at DA-IICT. In 2015, the third conf.kde.in was held at Amrita University in Kerala, and in 2016 at LNMIIT Jaipur. The Jaipur conference attracted members of the KDE Community from all over the world. Attendees from different backgrounds came to meet each other, give talks, and share in the spirit of KDE. The 2017 conference was held in IIT Guwahati, Assam and sought to cater to new members of KDE, as well as to seasoned developers. 

<p><figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href=""><img style="float: center"  src="/sites/dot.kde.org/files/kde-meetup-team_1500.jpg" /></a><br />KDE Meetup 2014</figure></p>

All of these events have been successful in attracting a lot of Indian students to mentoring programs such as Google Summer of Code (GSoC), Season of KDE, and Google Code-In.

conf.kde.in 2020 will generate even more interest and participation by creating a fertile environment for people to get started with KDE, Qt and FOSS through numerous talks, hands-on sessions and demonstrations.

<h2>Call For Papers</h2>

Join us! <a href="https://conf.kde.org/users/sign_in?conference_acronym=cki2020&locale=en">Submit a paper</a>, explain the content for a 30-minute presentation or a workshop on any aspect of KDE, Qt or any other FOSS topic you want to cover, and become a conf.kde.in Speaker.

Remember to include all pertinent information about your background, other talks you've given, and anything else that gives a sense of what attendees can expect from your presentation.

See you in 2020 in India!
<!--break-->