---
title: "Powered by Plasma: ALBA Synchrotron in Barcelona, Spain"
date:    2019-07-19
authors:
  - "skadinna"
slug:    powered-plasma-alba-synchrotron-barcelona-spain
comments:
  - subject: "Congratulations"
    date: 2019-07-19
    body: "<p>It's great to see KDE software on a place like this.</p>"
    author: "Josep"
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/plasma-alba-segment.jpg" alt="" width="800" />

<B>As you go about your daily tasks, you’re probably unaware that Plasma runs on the computers in one of Europe’s largest research facilities. We were also oblivious – until we met Sergi Blanch-Torné at FOSDEM 2019.</B>

<I>We’re always looking for interesting stories from people who use KDE software at their workplace, in school, or in government institutions. You can imagine our delight, then, when we met Sergi Blanch-Torné at this year’s FOSDEM.</I>

<I>Sergi is a Controls Software Engineer at ALBA, a KDE user, and a Free software advocate and contributor. Not only was he willing to tell us about his favorite KDE apps, but he also works at one of the most amazing places on Earth! In this interview, he tells us what it’s like to work at ALBA, and answers the burning question: “what even is a synchrotron?”.</I>

<I><a href="https://www.cells.es/en">ALBA</a> is a third-generation synchrotron radiation facility in the Barcelona Synchrotron Park, in Cerdanyola del Vallès, Spain. Managed by the Consortium for the Construction, Equipping and Exploitation of the Synchrotron Light Source (CELLS), it is jointly funded by the Spanish and the Catalonian Administration.</I>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 400px"><a href="/sites/dot.kde.org/files/plasma-alba-facility-outside.jpg"><img src="/sites/dot.kde.org/files/plasma-alba-facility-outside.jpg" /></a><figcaption>Aerial view of the ALBA facility. Source: <a href="https://www.cells.es/en/media/gallery">CELLS media gallery.</a></figcaption></figure>

<I>With its eight operational beamlines (and additional four in construction), ALBA has been serving more than 2000 researchers every year since first opening for experiments in 2010. It comprises a complex of electron accelerators that produce synchrotron light – electromagnetic radiation covering a continuum of wavelengths, ranging from infrared to hard X rays (including visible light). Synchrotron light is millions of times brighter than the surface of the Sun, which allows scientists to visualize atomic structures in extremely high resolutions.</I>

<I>ALBA also happens to be the place where Plasma powers the majority of desktop computers in the controls department. Read more on this, plus a bunch of fascinating details on how synchrotrons work, in our interview with Sergi.</I>

<!--break-->

<span style="color:#f55;font-weight:bold">Ivana Devcic:</span> Sergi, thank you for accepting our invitation to the interview, and for taking the time out of your day to do this! Our story begins at this year's FOSDEM, where you met with developers from the KDE community, is that right?

<span style="color:#55f;font-weight:bold">Sergi Blanch-Torné:</span> That's right. I'm pretty much a regular at FOSDEM at this point. I think I attended it for the first time in 2004. One day at work, we had some trouble with Plasma. Conveniently, it was a few weeks before FOSDEM. I told my boss that there will surely be KDE people to ask for help, and there really were! And it was fun to find out they are from Catalonia too.  

So we <a href="https://www.cells.es/en/outreach/visit-alba">arranged a visit</a> with Aleix Pol and Albert Astals. They looked at the issue and found the right configuration that solved it.

<span style="color:#f55;font-weight:bold">Ivana:</span> Glad to hear you solved the issue! It would make for an awkward start to our interview otherwise. Could you please introduce yourself a bit to our readers?

<span style="color:#55f;font-weight:bold">Sergi:</span> Of course. I'm from Juneda, a small village in the west of Catalonia. Just 20 km away from the province capital Lleida. An hour and a half from the capital, Barcelona. I studied computer science at the University of Lleida. It was there that I started using KDE software, back in 1998. They are Linux evangelists at my university, and KDE's desktop environment was (and still is) the default desktop on the computers running Linux. I still contribute to my university with a <a href="https://slides.com/sergiblanch/eps_2019">yearly talk</a> on how a computer scientist can end up in a weirder job than expected.

After getting my Bachelor's degree, I went to an Erasmus project to the Vrije Universiteit in Amsterdam, Netherlands. That's where I improved my English a lot. When I came back from Erasmus, I tried moving to another country, but ended up in Barcelona. 

The final project for my Bachelor's degree was related to Free software. I've had a great relationship with the Mathematics department, and there is a research group that works on cryptography. We thought of doing something practical that could be used by the Free Software community. At the time, the elliptic curve crypto wasn't really known to the general public, so we decided to prepare an implementation for GnuPG. I still maintain <a href="https://www.calcurco.cat/eccGnuPG/index.en.html">the website of our project</a>.

Sadly, I don't have the time to contribute with code any more, but I like to be in contact with the community. That's why every year at FOSDEM, we meet with Werner Koch, the project leader of GnuPG.

<span style="color:#f55;font-weight:bold">Ivana:</span> You've been involved with Free Software for quite a while. How did this lead to working at ALBA?
 
<span style="color:#55f;font-weight:bold">Sergi:</span> When I was looking for a job abroad after returning from Erasmus, I heard about <a href="https://www.esrf.eu/">the ESRF (European Synchrotron Radiation Facility)</a>, the biggest synchrotron in Europe. I applied for a position there, and in one of the interviews in the hiring process they told me that a synchrotron was in construction near Barcelona. So I also applied there. Many of my current supervisors at ALBA were working at ESRF before. 

<figure style="border: 1px solid grey;"><img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/plasma-alba-facility-inside.jpg" alt="" width="800" /><figcaption style="text-align: center">General view inside the facility. Photo by Sergi Blanch-Torné.</figcaption></figure>

Here at ALBA, I got a position in the controls section. We're part of the computing division that has different sections. One is called the <I>Management Information System</I>. That's where engineers who work mainly with the web and administrative applications. Then there are the people from infrastructure systems. And the sections for electronics and controls, that we have a lot in common with.

<span style="color:#f55;font-weight:bold">Ivana:</span> The controls section... Sounds exciting. But what exactly do you control from there?

<span style="color:#55f;font-weight:bold">Sergi:</span> To help you understand how it all works, maybe I should abstract first what a synchrotron is. A synchrotron is a kind of particle accelerator. In our case, it is an electron accelerator. You've probably heard about the LHC in CERN -- that's a collider, a very different facility, but also an accelerator. 

The difference is in the scale and in the purpose. At the LHC they have 27 kilometers in the circumference perimeter. We have about 400 meters. Colliders are made to produce those collisions we read about in the press, for discovering particles like the Higgs Boson. When those electrons in the accelerator are bent to maintain the circumference, they produce photons - basically, they generate light. In the LHC or other colliders, they like to set this light generation to the minimum. But in the synchrotrons, it is the light that is actually used.

<p align="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/b3mEmE4Gu_A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

We could say that the accelerator itself is a bulb that generates an incredibly brilliant light. And it is because of this brilliance that those facilities are used, instead of other alternatives such as X-rays. There are experiments that require this kind of facility, because with a normal X-ray generator researchers would not be able to see what the experiment requires. In essence, you could also say that this is a big machine we use to see small things. We could even simplify it to the point of saying a synchrotron is like a huge microscope.

It has applications in biology, chemistry, pharmacology... also in paleontology, or even art. Here at ALBA we did experiments on how a virus passes the membrane of a cell. We looked at the mechanisms of how the cell is invaded, and took pictures of that. We can also take a fossil and see inside - without having to crack it open and break it, we are able to see the remaining structure inside. Or we can stress a plastic to know how it will degrade under certain conditions. Sometimes famous artists used layers of paint to draw over something. We can see what's under the paint without destroying the layers. 

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 400px"><a href="/sites/dot.kde.org/files/plasma-alba-beamline-end-station.jpg"><img src="/sites/dot.kde.org/files/plasma-alba-beamline-end-station.jpg" /></a><figcaption>Beamline 24 end station, where the experiments happen. Photo by Sergi Blanch-Torné.</figcaption></figure>

<span style="color:#f55;font-weight:bold">Ivana:</span> Fascinating! 

<span style="color:#55f;font-weight:bold">Sergi:</span> It really is. And my job is to have the instruments under control and integrated to allow an experiment to be carried out. We provide the researchers with the tools they need, but we are rarely there when they're using the synchrotron. 

To make sure an accelerator works correctly, many elements need to work together seamlessly. The electron beam is like, say, a train with its carriages. There is not a single electron, but a bunch revolving around in a circle. They are packed in bunches, and those bunches are separated from each other by 2 nanoseconds. So an extremely high level of precision is needed. 

First, there is a linear accelerator, with an electron gun that generates those bunches of electrons. We call it <I>linac</I> for short. This linac produces electron beams that travel at speeds close to the speed of light. Then they are passed through a booster ring, where they are accelerated even more. Even closer to the speed of light, but never reaching its physical bound. This acceleration process is repeated 3 times per second, and the beam is stored in a ring that is designed to generate this extremely brilliant light. 

For this whole process, there are hundreds or even thousands of elements to control. They have to work together, they have to be prepared to act just in that one precise moment. There are stepper motors, encoders, detectors, cameras, oscilloscopes, and even other weirder elements that need to work together. And we make sure they work properly. 

<span style="color:#f55;font-weight:bold">Ivana:</span> Do you use any specialized software in your work?

<span style="color:#55f;font-weight:bold">Sergi:</span> Currently, the control room is running Debian 9 with Plasma. Almost everything in the controls section runs Linux. There are a few computers that require Windows because some manufacturers didn't provide the drivers for Linux...

<figure style="border: 1px solid grey;"><img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/plasma-alba-controlroom-computers.jpg" alt="" width="800" /><figcaption style="text-align: center">Computers in the control room, powered by Plasma. Photo by Sergi Blanch-Torné.</figcaption></figure>

As for the tools we use, the ESRF has been publishing the controls system as Free Software for decades now. There is a community of synchrotrons in Europe and many institutions and research facilities participate in this community. It's all about collaboration of several institutions and engineers like me in other facilities. In a way, it's exactly like in FOSS communities: we work on solving something that others can benefit from, and vice versa. To be more specific in terms of software, the distributed control system we use is called <a href="http://tango-controls.org">Tango</a>.

<span style="color:#f55;font-weight:bold">Ivana:</span> It really does sound like your work has a lot in common with FOSS communities and their principles. After all, science and software freedom have always been intertwined. If I understood you correctly, you also write your own software at ALBA?

<span style="color:#55f;font-weight:bold">Sergi:</span> Yes, we make Free Software, and it's one of the reasons why I work here. I'm happy to have a job that produces Free Software. 

We are a public institution and, even though we make Free Software, there are things we still don't publish. That's why we have to keep pushing in this direction. Some of us have also participated in the <a href="https://fsfe.org/campaigns/publiccode/publiccode.en.html">FSFE “Public Money? Public Code!” campaign</a>. It's an incredibly important initiative.

<span style="color:#f55;font-weight:bold">Ivana:</span> Can you share an example of what kind of software you produce? 

<span style="color:#55f;font-weight:bold">Sergi:</span> Sure! From the control room, the people in charge of the facility need to have precise control of all the elements to ensure the stable control of the beam. They are not close to those elements, so it's necessary to be able to control everything remotely, from the network. And those people like graphical interfaces, so, a long time ago, we started a project based on Qt to build a framework that would make it easier to build those graphical interfaces. 

We called it <a href="http://taurus-scada.org">Taurus</a>. This is a LGPL framework, and since the latest release (4.5 at the time of writing), it supports Plasma 5.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 400px"><a href="/sites/dot.kde.org/files/plasma-alba-taurus-screenshot.png"><img src="/sites/dot.kde.org/files/plasma-alba-taurus-screenshot.png" /></a><figcaption>Screenshot of the Taurus software, courtesy of Sergi Blanch-Torné.</figcaption></figure>

<span style="color:#f55;font-weight:bold">Ivana:</span> It’s nice to hear that. :) Apart from using Plasma as the desktop environment on your computers, do you use any other KDE apps at ALBA? 

<span style="color:#55f;font-weight:bold">Sergi:</span> We are a scientific paper factory, so we use Kile and Okular all the time. In the controls section, we use Kate and KWrite quite often. Of course, there are also the Emacs people and the Vim users...

Personally, I use <a href="https://kontact.kde.org/">KMail</a>, <a href="https://amarok.kde.org/">Amarok</a>, <a href="https://www.digikam.org/">digiKam</a>, and for my astronomy hobby, <a href="https://edu.kde.org/kstars/">KStars</a>. Whenever I show someone new how to start playing with a (domestic) telescope, KStars is the perfect application to learn everything you need to know. 

<span style="color:#f55;font-weight:bold">Ivana:</span> We’ll make sure to forward the praise to the KStars team. Do you have any advice for young scientists who would like to work at ALBA or similar institutions? What kinds of skills should they have? Any particular programming languages that would be good to know? 

<span style="color:#55f;font-weight:bold">Sergi:</span> For sure! The important thing is to apply. Apply to any job you think will make you happy. If there's a nice work opportunity, don't pass up on it. 

At ALBA, we work with C/C++ and Python. Sometimes, but rarely in Java. I use Cython very often. 

There is a variety of skills that we, as a group, provide. One single person can't do everything alone, so the community aspect is crucial to us. Our work ranges from kernel drivers to graphical interfaces. Some of us are better in one area, some in another. The important thing is that in the end, the group is capable of working on the full stack. And the main driving force is that we're all contributing to science.

<span style="color:#f55;font-weight:bold">Ivana:</span> We hope you know your contributions are very much appreciated. This has been a fantastic interview – we’ve learned so many things! Thank you, Sergi. It would make us all happy if you could come to Akademy, KDE’s annual conference, so please think about it. :) 

<span style="color:#55f;font-weight:bold">Sergi:</span> Thanks for having me! The next Akademy is in Italy, isn't it? I’ll be there. 

<hr />

<I>Would you like to meet KDE developers, or learn how to use Plasma in your company? Come to Milan, Italy, from September 7th to 13th and <a href="https://akademy.kde.org/2019">join us at Akademy</a>! Our community conference is free and open to everyone.</I>