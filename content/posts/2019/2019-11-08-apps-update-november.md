---
title: "Apps Update for November"
date:    2019-11-08
authors:
  - "jriddell"
slug:    apps-update-november
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/apps.png" alt="" width="800" />

<h2>LabPlot</h2>

The big release this month has been LabPlot 2.7. LabPlot is fast becoming one of KDE's highest profile apps. It is an application for interactive graphing and analysis of scientific data. LabPlot provides an easy way to create, manage and edit plots. It allows you to produce plots based on data from a spreadsheet or on data imported from external files. Plots can be exported to several pixmap and vector graphic formats.

In this release we made the user experience while working with LabPlot easier and more fun. Entering and working with data in spreadsheets is slicker and when reading live data from file sources you can now use a relative path to find a live data source. This allows you to, for example, copy the folder containing the project file together with the data file or files across different folders on your computer without losing the connection to the file or files. In the Project Explorer you can now move top-level objects to different folders via drag & drop.

The data picker, which allows you to digitize data points on images, has had an overhaul in 2.7. The devs have greatly simplified the overall workflow and the process of digitizing data points as you can see in this video.

<div align="center"><br /><iframe width="560" height="315" src="https://www.youtube.com/embed/D_Pz5aA-4Xg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br /></div>

Check out the Labplot YouTube channel for more videos on using this advanced application.

<h2>Bugfixes</h2>

Alternative panel Latte Dock got a <a href="https://psifidotos.blogspot.com/2019/10/latte-bug-fix-release-v094.html">bugfix release, 0.9.4</a>.  It fixes autoloading in some distros such as Manjaro.

KDevelop is on its monthly <a href="https://www.kdevelop.org/news/kdevelop-544-released">bugfix release</a> which tidied up CLang support for some distros.

Over 100 apps gets released as part of the KDE Applications bundle which has just had its 19.08.3 bugfix releases and includes:

<ul>
<li>In the video-editor Kdenlive, compositions no longer disappear when reopening a project with locked tracks.</li>

<li>Okular's annotation view now shows creation times in local time zone instead of UTC.</li>

<li>Keyboard control has been improved in the Spectacle screenshot utility.</li>
</ul>

<h2>Snap Store</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://dashboard.snapcraft.io/site_media/appmedia/2019/10/19-08.png"><img src="https://dashboard.snapcraft.io/site_media/appmedia/2019/10/19-08.png" width="400" /></a><br /><figcaption>Kdenlive Snap</figcaption></figure> 

Snaps are one of the new container-based package formats for Linux.  <a href="https://snapcraft.io/publisher/kde">KDE has over 50 apps</a> published on the Snap store and ready to be installed on almost any Linux distro.  On many Ubuntu flavors and derivatives, they come ready to be used.  On others you may need to use your package manager to <a href="https://snapcraft.io/docs/installing-snapd">install snapd first</a>. This is usually as simple as running a command such as <code>sudo dnf install snapd</code> or <code>sudo pacman -S snapd</code>.  Most of KDE's Snap packages are built by the KDE neon team on their servers and  the aim is to get packaging and building integrated more directly with app's repositories and continuous integration setups. This means they are updated more frequently and the moment changes are made so you always get the latest and greatest features and fixes.

New this month in the Snap store is <a href="https://snapcraft.io/kdenlive">KDE's video editor, Kdenlive</a>.  

<br clear="all"/>
<h2>Coming Up</h2>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/ktrip.jpg"><img src="/sites/dot.kde.org/files/ktrip.jpg" width="400" /></a><br /><figcaption>KTrip</figcaption></figure> 

We have a couple of nice progressions towards stable releases from KDE apps.  First, the mobile journey search app <a href="https://invent.kde.org/kde/ktrip">KTrip</a> has moved into kdereview, meaning the authors want it checked over for sanity before making a stable release.  In a first for KDE developer Nicolas Fella, he worked out how to get KTrip into F-Droid, the free software app store for Android.

Then, the developer tool <a href="https://github.com/KDE/elf-dissector">ELF Dissector</a> passed kdereview, meaning KDE has approved it as something we are happy to put our name on when it gets released.  It's a static analysis tool for ELF libraries and executables. It does things like inspect forward and backward dependencies (on a library or symbol level), identify load-time performance bottlenecks such as expensive static constructors or excessive relocations, or size profiling of ELF files.

<h2>Help Out</h2>

By getting KDE's apps into the most popular of channels like the Windows Store, Google Play and F-Droid, we can reach more users and boost KDE's adoption through its software. Now that Kate is successfully shipping in the Windows Store, Kate developer Christoph Cullmann wrote a guide to <a href="https://kate-editor.org/post/2019/2019-11-03-windows-store-submission-guide/">Windows Store submission</a>.  Check it out.

<a href="https://phabricator.kde.org/project/view/313/">KDE's All About the Apps Goal</a> has loads of other things you can do to help get our applications to users, so come along and give us a hand.
<!--break-->
