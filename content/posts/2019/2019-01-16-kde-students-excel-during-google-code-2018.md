---
title: "KDE Students Excel during Google Code-in 2018"
date:    2019-01-16
authors:
  - "unknow"
slug:    kde-students-excel-during-google-code-2018
comments:
  - subject: "congratulations"
    date: 2019-01-18
    body: "<p>Congrats to all participants !</p>"
    author: "Anonymous"
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/awards_0.jpg" alt="" width="800" />

<b>After many years of successful Google Code-in participation, this year we did it again! <a href="https://codein.withgoogle.com/archive/2018/organization/5630742793027584/">KDE attracted a number of students</a> with exciting tasks for their eager young minds.</b>

<a href="https://codein.withgoogle.com/">Google Code-in</a> is a program for pre-university students aged from 13 to 17 and sponsored by Google Open Source. KDE has always worked to get new people involved in Free and open source (FOSS) projects with the aim of making the world a better place.

<a href="https://codein.withgoogle.com/archive/2018/">This year was no different</a>. Our students worked very hard, and some of them already have their contributions committed to the KDE codebase!

We designed tasks in a way that made them exciting for all students. Students who were not skilled in programming took on tasks of writing blogs or documentation. To help students who had no experience with FOSS or with the community, we set up introductory tasks for IRC and mailing lists, both of which are essential in FOSS as communication channels. 

The students who had some prior programming experience received tutorial tasks to get a better understanding of how KDE software works. Those types of tasks also helped them become familiar with the Qt framework on which all KDE software is based. Finally, students good at programming were put to work contributing to on-going KDE projects. They created new features or solved known bugs and wrote unit tests.

We’re happy that some really enthusiastic and persistent students joined us this year. Thanks to their passion for programming, they completed many tasks and delivered quality code we merged into our project repositories.

It wasn’t easy for the mentors to select winners, as every student had accomplished great things. Still, we finally settled on pranav and triplequantum (their GCI names). Finalists were TURX, TUX, UA and waleko.

KDE would like to congratulate all the winners and finalists, and we warm-heartedly welcome all our new contributors!

<em>Author: Pranam Lashkari</em>
<!--break-->