---
title: "Come Home to KDE at FOSDEM 2019"
date:    2019-01-30
authors:
  - "Paul Brown"
slug:    come-home-kde-fosdem-2019
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/flag_2000.png" alt="" width="800" />

<b>Despite the rain and the cold, there is always a place that is warm and welcoming in Brussels in February - the KDE booth at <a href="https://www.fosdem.org/2019/">FOSDEM 2019</a>.</b>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/dvg584vwkaaehqb.jpg%3Alarge.jpeg"><img src="/sites/dot.kde.org/files/dvg584vwkaaehqb.jpg%3Alarge.jpeg" /></a><figcaption>Live Krita demo at FOSDEM 2018.</figcaption></figure>

This year we are jam-packing it with interesting stuff. The first thing you'll see as you arrive is Krita's demo. <a href="https://wolthera.info/">Wolthera van Hövell</a>, a talented artist that regularly contributes to <a href="https://krita.org/en/">Krita</a>, will be painting live at the booth, demonstrating all the new features on a large screen for everybody to enjoy. Then on Sunday, Camille Moulin will be demonstrating how to edit video using <a href="https://krita.org/en/">Kdenlive</a>.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/photo_2018-02-03_12-40-57.jpg"><img src="/sites/dot.kde.org/files/photo_2018-02-03_12-40-57.jpg" /></a><figcaption>Plasma Mobile devices you can play with!</figcaption></figure>

We will also have a wide variety of hardware devices you will be able to play with. We will be showing off how Plasma works on everything: from high-end laptops like the <a href="https://kde.slimbook.es/">KDE Slimbook</a>, to underpowered and very affordable notebooks, such as the <a href="https://www.pine64.org/?page_id=3707">Pinebook</a>. A variety of SBCs (single-board computers) will also be on show, ranging from Raspberry Pis to Pine64s. Of course, we will show the progress we are making on Plasma Mobile, so the KDE booth will be well-equipped with phones you can test on-site, and exotic new hardware, like a RISC-V board.

Besides demos and devices, the booth staff will be made up by some of KDE's most notorious members. Aleix Pol of <a href="https://www.kde.org/applications/education/kalgebra/">KAlgebra</a> and <a href="https://userbase.kde.org/Discover">Discover</a> fame; Jonathan Riddell, creator of the <a href="https://neon.kde.org/">KDE neon</a> system; Bhushan Shah, the main developer of <a href="https://www.plasma-mobile.org/">Plasma Mobile</a>; Adriaan de Groot, the main developer of the <a href="https://calamares.io/">Calamares</a> operating system installer; and Boudewijn Rempt from Krita will all be on hand to answer your questions, field suggestions or just chat.

In the merch department, you will be able to get your hands on our brand new Katie and Konqi stickers, especially designed to celebrate Plasma Mobile, along with a variety of other kute designs. You will also be able to purchase KDE gear, such as T-shirts, sweaters and hoodies. This is a great opportunity to not only spruce up your wardrobe, but also to contribute to KDE with a donation to our community - truly a win-win situation!

In the midst of the Belgium winter, what better place to feel fuzzy, warm and welcome than at our booth? <a href="https://www.fosdem.org/2019/schedule/buildings/#k">Come feel at home and visit us on the bottom floor of building K(DE) at the ULB Solbosch Campus in Brussels</a>.

<!--break-->