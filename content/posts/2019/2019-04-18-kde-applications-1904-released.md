---
title: "KDE Applications 19.04 Released"
date:    2019-04-18
authors:
  - "Paul Brown"
slug:    kde-applications-1904-released
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/thumbnail_enhanced_1000x486.jpg" alt="openexpo" width="800" />

<b><a href="https://kde.org/announcements/announce-applications-19.04.0.php">The first new applications bundle release of the year</a> adds stability, coherence and new features that help users become more comfortable and more productive using KDE software.</b>

KDE's file manager, <a href="https://kde.org/applications/system/dolphin/">Dolphin</a>, can now show previews of more types of files, including Microsoft Office files, Blender 3D scenes, and EPUB eBooks. Continuing in the same department, previews of text files containing code or markup will now show syntax highlighting. All of the above will help users identify the contents of a file even before opening it. Other usability improvements include the option to choose which split to close when clicking the <I>Close Split</I> button, smarter tab placement, and a more practical way of tagging files.

<a href="https://kde.org/applications/graphics/okular">Okular</a>, KDE's document viewer, now lets users verify PDFs that have been digitally signed. Digital signatures are used in official documents to confirm that the document comes from the right source, and that it has not been tampered with. Okular also lets you edit LaTeX documents directly in the viewer and has improved touchscreen compatibility, making it much easier to use it in presentation mode.

<p align="center"><iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/f8d1d17d-c044-4578-81de-b6c3b5f9bc9c" frameborder="0" allowfullscreen></iframe></p>

<a href="https://kontact.kde.org/">Kontact</a> has also improved, with <a href="https://kontact.kde.org/components/kmail.html">KMail</a> receiving most of the changes. Starting with this release, KMail can correct your grammar in the texts of your messages. A new thing you will find in this version of Kontact is KItinerary, a travel assistant developed by Volker Krausse that advises you on how to best get to your destination using meta-data from your e-mails. 

Many other apps have been improved: <a href="https://kde.org/applications/system/konsole">Konsole</a> has made using tabs easier, <a href="https://kde.org/applications/graphics/spectacle">Spectacle</a> gives you more options when taking and saving screenshots, and <a href="https://kdenlive.org/en/">Kdenlive</a> has overhauled the timeline, making editing video easier and more fun, just to mention a few changes.

<p align="center"><iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/8b5c1cb8-9b56-4b56-aeaf-11982a67e40a" frameborder="0" allowfullscreen></iframe></p>

As always, you can read more about KDE Applications 19.04 in <a href="https://kde.org/announcements/announce-applications-19.04.0.php">the official announcement</a>. Many distributions are currently <a href="https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro">packaging Applications 19.04 ready for update and install</a>. For the first time, you can install many of the apps on any distro directly from the <a href="https://snapcraft.io/publisher/kde">Snap Store</a>.

<p align="center"><iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/3f9aa37b-1361-4594-ab31-c04fd7efce60" frameborder="0" allowfullscreen></iframe></p>
<!--break-->