---
title: "The Linux Application Summit is coming to Barcelona in November"
date:    2019-07-31
authors:
  - "Paul Brown"
slug:    linux-application-summit-coming-barcelona-november
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/icon-1527675_1280_cut.jpg" alt="" width="800" />

<b>The <a href="https://kde.org/">KDE</a> and <a href="https://www.gnome.org/">GNOME</a> Communities are pleased to announce <a href="http://linuxappsummit.org/">Linux App Summit 2019</a>.</b>

LAS 2019 will be held in Barcelona, Spain from November 12th to November 15th. Our <a href="https://linuxappsummit.org/cfp/">Call for Participation</a> opens on July 31st, and will run until August 18th.

LAS is a conference focused on building an application market. Through LAS, the GNOME and KDE communities intend to help build an ecosystem that will encourage the creation of quality applications, seek ways of compensating application developers, and foster a thriving market for the Linux operating system. We are excited about combining our efforts in app development for Linux and we aim to take on an active role leading the way into the future.

<h2>Venue</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 400px"><a href="/sites/dot.kde.org/files/llealtat_santsecna_2.jpg"><img src="/sites/dot.kde.org/files/llealtat_santsecna_2.jpg" /></a><figcaption>The inside of the <i>La Lleialtat Santsenca</i> building where LAS will take place. Photo credit: <a href="https://twitter.com/ttncat">@ttncat</a> and <a href="https://twitter.com/SeguimFils">@SeguimFils</a></figcaption></figure>

The conference will take place in the beautiful <a href="https://goo.gl/maps/AX49Gc8g6yq229oo9">Lleialtat Santsenca</a>, a community space that hosts many meetups and conferences, and which has been featured in <a href="https://www.archdaily.com/889515/civic-centre-lleialtat-santsenca-1214-harquitectes">architectural articles</a> due to the startling contrast between its art deco facade and its überfunctional minimalist interior. The core conference days will be from November 12th to 14th, and November 15th will be reserved for BoFs and hackathons. Participants are welcome to stay longer and hack together over the weekend. We encourage participants to collaborate and not just meet and talk about change, but actually make it happen. 

Previous iterations of this conference (under the name <i>Linux Application Summit</i>) have been held in the United States, in Portland, Oregon, and <a href="https://las.gnome.org/conferences/LAS">Denver, Colorado</a>. This year, Barcelona, Spain was chosen as the hosting location due to growing international interest. Barcelona is known for embracing open source technology, and in fact made headlines back in 2017 when it announced that it would no longer use Microsoft and instead use Linux (<a href="https://elpais.com/ccaa/2017/12/01/catalunya/1512145439_132556.html">Spanish language article</a>). Apart from enthusiastically embracing open source, Barcelona is known as one of the world's most influential cities, and is a major touristic and cultural attraction. We are thrilled to have the first international version of the Linux App Summit hosted there.

Everyone is invited to attend LAS, and there will be travel sponsorship available for those who need it in order to make the event more inclusive. A special invitation will be extended to companies, media, and individuals who are interested in learning more about the Linux desktop application space and growing their user base. 

<h2>Important Dates</h2>

<ul>
<li>31 July - CfP opens. Submit your talk idea! Visit our <a href="https://linuxappsummit.org/cfp/">Call for Participation (CfP) page</a> for more information.</li>
<li>31 Aug - CfP closes.</li>
<li>Week of 9 Sept - Speaker notification begins.</li>
<li>12 to 15 Nov - LAS conference dates. There will be 3 core days and 1 day for BoFs and hackathons. Participants are welcome to remain longer and plan BoFs or hackathons over the weekend.</li> 
</ul>

<h2>Join Us!</h2>
The organizing team, GNOME and KDE, are excited about the possibilities of our combined influence. We will no longer sit passive, but will lead and plan for the future. <a href="http://linuxappsummit.org/">Join us in Barcelona!</a> 

<i><b>Sponsor LAS 2019!</b> Find out about the available packages contacting us at <a href='mailto&#58;&#37;73&#112;o&#37;6E%73&#111;rs%&#52;0&#108;inuxa%70%70&#37;73&#117;mm&#37;&#54;&#57;t&#46;&#111;rg'>&#115;&#112;o&#110;so&#114;s&#64;linuxapp&#115;umm&#105;&#116;&#46;org</a>. If you have any questions, contact <a href='mailto&#58;i%6E&#102;o%&#52;0li&#110;&#117;&#120;%61%7&#48;p%73umm&#105;%74&#46;%6F&#114;g'>in&#102;o&#64;linuxap&#112;&#115;umm&#105;t&#46;org</a>. Don't forget to follow us on Twitter <a href="https://twitter.com/LinuxAppSummit">@LinuxAppSummit</a>.</i>
<!--break-->
<hr />
Top image by <a href="https://pixabay.com/users/geralt-9301/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1527675">Gerd Altmann</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1527675">Pixabay</a>