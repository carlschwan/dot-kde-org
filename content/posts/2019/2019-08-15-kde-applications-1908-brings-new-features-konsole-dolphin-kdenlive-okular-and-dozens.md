---
title: "KDE Applications 19.08 Brings New Features to Konsole, Dolphin, Kdenlive, Okular and Dozens of Other Apps"
date:    2019-08-15
authors:
  - "skadinna"
slug:    kde-applications-1908-brings-new-features-konsole-dolphin-kdenlive-okular-and-dozens
---
<div style="margin: 0em 0em 2em 0em">
    <p align="center">
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/6443ce38-0a96-4b49-8fc5-a50832ed93ce" frameborder="0" allowfullscreen></iframe>
    </p>
</div>

Can you believe we've already passed the half-year mark? <b><a href="https://kde.org/announcements/announce-applications-19.08.0">That means it's just the right time for a new release of KDE Applications!</a></B> Our developers have worked hard on resolving bugs and introducing features that will help you be more productive as you get back to school, or return to work after your summer vacation. 

The KDE Applications 19.08 release brings several improvements that truly elevate your favorite KDE apps to the next level. Take <a href="https://konsole.kde.org/">Konsole</a>, our powerful terminal emulator, which has seen major improvements to its tiling abilities. We've made tiling a bit more advanced, so now you can split your tabs as many times as you want, both horizontally and vertically. The layout is completely customizable, so feel free to drag and drop the panes inside Konsole to achieve the perfect workspace for your needs. 

<video width="800px" class="img-fluid" loop="" muted="" autoplay="" data-peertube="https://peertube.mastodon.host/videos/embed/ae19d1ec-dee7-41b2-a9b7-d78d0ea63411">
    <source src="https://kde.org/announcements/announce-applications-1908/konsole-tabs.webm" type="video/webm">
</video>

<a href="https://kde.org/applications/system/org.kde.dolphin">Dolphin</a>, KDE's file explorer, introduces features that will help you step up your file management game. Let's start with bookmarks, a feature that allows you to create a quick-access link to a folder, or save a group of specific tabs for future reference. We've also made tab management smarter to help you declutter your desktop. Dolphin will now automatically open folders from other apps in new tabs of an existing window, instead of in their own separate windows.  Other improvements include a more usable information panel and a new global shortcut for launching Dolphin - press Meta + E to try it out!

<a href="https://kde.org/applications/office/org.kde.okular">Okular</a>, our document viewer, continues with a steady stream of usability improvements for your document-viewing pleasure. In Applications 19.08, we have made annotations easier to configure, customize, and manage. Okular's ePub support has also greatly improved in this release, so Okular is now more stable and works better when previewing large files.

All this sounds exciting for those who read and sort through documents, but what about those who write a lot of text or emails? They will be glad to hear we've made <a href="https://kate-editor.org/">Kate</a>, our advanced text editor, better at sorting recent files. Similar to Dolphin, Kate will now focus on an existing window when opening files from other apps. Your email-writing experience also receives a boost with the new version of <a href="https://kontact.kde.org/">Kontact</a>, or more specifically, KMail. After updating to Applications 19.08, you'll be able to write your emails in Markdown, and insert - wait for it - emoji into them! The new integration with grammar-checking tools like LanguageTool and Grammalecte will help you prevent embarrassing mistakes and typos that always seem to creep into the most important business emails.

Photographers and other creatives will appreciate changes to <a href="https://kde.org/applications/graphics/org.kde.gwenview">Gwenview</a>, KDE's image viewer. Gwenview can now display extended EXIF metadata for RAW images, share photos and access remote files more easily, and generate better thumbnails. If you are pressed for system resources, Gwenview has your back with the "Low usage resource mode" that you can enable at will. In the video-editing department, <a href="https://kdenlive.org/en/">Kdenlive</a> shines with a new set of keyboard+mouse combinations and improved 3-point editing operations.

We should also mention <a href="https://kde.org/applications/utilities/org.kde.spectacle">Spectacle</a>, KDE's screenshot application. The new version lets you open the screenshot (or its containing folder) right after you've saved it. Our developers introduced a number of nice and useful touches to the Delay functionality. For example, you may notice a progress bar in the panel, indicating the remaining time until the screenshot is done. Sometimes, it's the small details that make using KDE Applications and Plasma so enjoyable. 

<video width="800px" class="img-fluid mb-3 mt-3" loop="" muted="" autoplay="" data-peertube="https://peertube.mastodon.host/videos/embed/1c19fd9c-42e6-4985-9033-59ea5553adee">
    <source src="https://kde.org/announcements/announce-applications-1908/spectacle_progress.webm" type="video/webm">
</video>

Speaking of details, to find out more about other changes in KDE Applications 19.08, make sure to read <a href="https://kde.org/announcements/announce-applications-19.08.0">the official announcement</a>.

<b>Happy updating!</b>

<!--break-->

<hr>

<em>If you happen to be in or close to Milan, Italy this September, come and <a href="https://akademy.kde.org/2019">join us at Akademy</a>, our annual conference. It's a great opportunity to meet the creators of your favorite KDE apps in person, and get an early sneak peek at all the things we have in store for the future of KDE.</em>