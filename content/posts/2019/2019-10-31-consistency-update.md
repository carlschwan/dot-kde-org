---
title: "Consistency Update"
date:    2019-10-31
authors:
  - "unknow"
slug:    consistency-update
---
<p style="font-size: small">By Niccolò Venerandi</p>

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/puzzle-2778019_1280.jpg" alt="" width="800" />

<p>It's been a month since Consistency was announced as an official goal for KDE at Akademy. During this time, we have focused on setting up all the tools needed to support the goal and tracking already active consistency tasks. Here's an update on what we have done so far and the main tasks we're working on.</p>

<h3>Community Page</h3>

<p>We have created a <a href="https://community.kde.org/Goals/Consistency">Consistency page</a> on the community wiki where you can learn what the consistency goal is and find out how you can easily get involved in it. Check it out, regardless of your level of technical expertise!</p>

<h3>Matrix Channel</h3>

<p>There is also a <a href="https://webchat.kde.org/#/room/%23consistency:kde.org">Consistency channel</a> on KDE's Matrix instance. Access it through the webchat page or at <b>consistency:kde.org</b>. You are welcome to come in and join us to discuss anything related to the consistency goal!</p>

<h3>Sprint!</h3>

<p>A sprint is in the works. If you would like to participate, join in <a href="https://phabricator.kde.org/T11791">the discussion</a> and come and discuss the time and the place on the Matrix channel as well.</p>

<h3>Phabricator Workboard</h3>

<p>We created a <a href="https://phabricator.kde.org/project/board/312/">Consistency workboard</a> so you can track all the tasks and keep up with their development. You can add yourself as a member or watcher to receive Phabricator updates.</p>

<p>Tasks are organized into the following categories:</p>

<ul>
  <li><B>Reported</B> shows consistency problems that still need to be addressed, but are currently not being worked on, or are not actively developed yet</li>
  <li><B>VDG Discussion</B> lists tasks that the VDG (Visual Design Group) are discussing</li>
  <li><B>HIG Specification</B> shows tasks that are waiting for an HIG (Human Interface Guidelines) specification so they can be developed in a consistent way</li>
  <li>Under <B>Apps Implementation</B> you can find tasks that are actively being worked on</li>
  <li><B>Meta</B> contains all the tasks that are not exactly consistency problems, but are related to the consistency goal in some way</li>
</ul>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 750px"><img src="https://i.postimg.cc/xCggRW6p/Screenshot-20191021-185844.png" alt="Current phabricator status"><br />The Consistency goal's workboard.</figure></div>

<h2>Consistency Tasks</h2>

<p>There are already many tasks in the Consistency project. Some tasks are new, some existed before. Many of these tasks are quite interesting, so read on to get an idea of what lays ahead for this goal.</p>

<h3><a href="https://phabricator.kde.org/T11124">Unify Highlight Effect Style</a></h3>

<p>This task was already in progress when the Consistency goal was selected, but it is nevertheless a great example of what we'd like to see happen in the goal.</p>

<p>Currently, Plasma has a discrepancy in its highlight effect. The first kind of effect is a plain rectangle using the highlight color, while the second one is a rounded rectangle with an outline and semi-transparent background. Although the former is more common, we think the latter is more appropriate to use in all situations.</p>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 250px"><img src="https://phabricator.kde.org/file/data/vu4vfkzz3zc3oesgsexg/PHID-FILE-k2a5ckqtq4gfluouut37/Screenshot_20190620_164924.png" alt="Here's the correct highlight effect in Plasma"><br />Here's the correct highlight effect in Plasma</figure></div>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 500px;"><img src="https://phabricator.kde.org/file/data/ep6mndryi2wiuhxaf23g/PHID-FILE-ub3nq2pz5q5bnk456ofg/Screenshot_20190620_164908.png" alt="Current dolphin"><br />Here's what it looks like in Dolphin now.</figure></div>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 500px;"><img src="https://phabricator.kde.org/file/data/2r3vrnkv3y2rlq6kttbe/PHID-FILE-6t2tgdqdvzhsh5zjm7ii/DolphinNewHighlightEffects.png" alt="Dolphin mockup"><br />Dolphin mockup showing correct highlighting.</figure></div>

<p>A few more examples of what the new highlight could consistently look like in various use-cases:</p>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 500px;"><img src="https://phabricator.kde.org/file/data/yfhqje45xgu5px5ck4k6/PHID-FILE-jm7c57rdd4oovevmyl3g/image.png" alt="Big icons sidebar highlight"><br />Big icons sidebar highlight.</figure></div>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 500px;"><img src="https://i.postimg.cc/MGQHVy6Y/panelsmock.jpg" alt="In plasmoids"><br />In plasmoids.</figure></div>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 500px;"><img src="https://i.postimg.cc/W4XWmJ5h/menu-4-1.png" alt="In menus"><br />In menus.</figure></div>

<p>This is a great example of what consistency can be: not simply applying the same style everywhere, but finding something that a single app does very well, and bringing that to all the other apps. Noah Davis is actively developing this task, and he's doing a great job!</p>

<h3><a href="https://phabricator.kde.org/T11153">Unify Sidebar Navigation and Appearance</a></h3>

<p>These tasks originated directly from the Consistency goal.</p> 

<p>Sidebars are used in many applications and it would be great that they were consistent. There are two main aspects to this: the type of sidebar (system settings-like lists, big square icons, etc.) and the navigation within the sidebar (tabs, combo boxes, etc.).</p>

<p>What is the best solution? That part is currently under discussion. We welcome everyone's opinions on the matter or, even better, an expert assessment on the feasibility of each of the options.</p>

<p>Let's quickly illustrate some options:</p>

<p>For the sidebar appearance, the current main option relies on using lists and big square icons, depending on the number of elements:</p>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 250px"><img src="https://kde.org/announcements/plasma-5.17/settings-consistency-wee.png" alt="Sidebars"><br />Sidebars.</figure></div>

<p>On the other hand, the option for navigating sidebar views includes tabs that become icons-only when horizontal space is insufficient, vertical tabs on the left, and combo boxes:</p>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 250px"><img src="https://phabricator.kde.org/file/data/2s3pmqrxmafmtqwxgtab/PHID-FILE-7rcoqthsnwykfgo4oizq/image.png" alt="Option 1"><br />Option 1.</figure></div>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 250px"><img src="https://phabricator.kde.org/file/data/kyjnpporm7kmjqqufqav/PHID-FILE-4b5r4q7iaqsd7xngoase/proposal.png" alt="Option 1b"><br />Option 1b.</figure></div>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 250px"><img src="https://phabricator.kde.org/file/data/443emkvfkjx32qz5guoy/PHID-FILE-47mxpjaq3rmtuyvvsugx/image.png" alt="Option 2"><br />Option 2.</figure></div>

<div align="center"><figure style="float: none; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey; width: 250px"><img src="https://phabricator.kde.org/file/data/ddf5c4esgjxh2tr6bg3w/PHID-FILE-qnzotvnwphzvdknkblih/image.png" alt="Option 3"><br />Option 3.</figure></div>

<p>Furthermore, Nate Graham is focused on making sure that <a href="https://phabricator.kde.org/T10165">all big icons displayed in sidebars are colorful</a>. He has already fixed a lot of them, and only a few are missing that we know of. Finally, there's also a task to <a href="https://phabricator.kde.org/T11191">create an HIG specification for sidebars</a> as soon as the discussion settles. We welcome help with any of these tasks. :-)</p>

<h3><a href="https://phabricator.kde.org/T10827">Website Redesign</a></h3>

<p>This task was already ongoing when the Consistency goal was chosen and it aims to modernize old web pages that follow obsolete styles. There are many of them and some are well-hidden. Carl Schwan created and works on this task alongside many other contributors. Check it out and see if you too can find any old websites that need updating!</p>

<h2>That's the end of this update!</h2>

<p>If you would like to help out, come join us in the <a href="https://webchat.kde.org/#/room/%23consistency:kde.org">matrix room</a> and let's make KDE software more consistent together!</p>
<!--break-->