---
title: "Meet KDE e.V.'s New Board"
date:    2019-09-27
authors:
  - "Paul Brown"
slug:    meet-kde-evs-new-board
---
Akademy 2019 brought the KDE community some exciting news and major changes. The new community-wide goals have been announced, and KDE contributors presented new ideas and projects they are working on. 

One important change that took place during Akademy 2019 is related to the KDE e.V., the foundation that legally represents the KDE community. Members of KDE e.V. elected two new members for the KDE e.V. Board. For the next couple of years, they will be the people who will legally represent the KDE community and manage the day-to-day running of KDE e.V.

Let’s meet the new members of the KDE e.V. Board! 

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:150px"><a href="/sites/dot.kde.org/files/Aleix_800.jpg"><img src="/sites/dot.kde.org/files/Aleix_800.jpg" /></a><br /><figcaption>Aleix Pol</figcaption></figure> 

<B>Aleix Pol</B> is KDE e.V.'s new President. Aleix has been involved with KDE since he was a student back in 2007. In those early days, he worked on KDevelop in several Google Summer of Code projects, and has gone on to create and maintain <a href="https://userbase.kde.org/Discover">Discover</a>, <a href="https://kde.org/applications/education/org.kde.kalgebra">Kalgebra</a>, and other high-profile KDE projects. Aleix's whole adult life has been linked to KDE one way or another and, among other things, he co-founded and was one of the first presidents of <a href="https://www.kde-espana.org/">KDE España</a>, the Spanish KDE association. Aleix has been a KDE e.V. Board member and vice-president since he was elected to the post during the 2014 Akademy held in Brno, Czech Republic.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:150px"><a href="/sites/dot.kde.org/files/Lydia_1500.jpg"><img src="/sites/dot.kde.org/files/Lydia_1500.jpg" /></a><br /><figcaption>Lydia Pintscher</figcaption></figure> 

<B>Lydia Pintscher</B> has moved on from the presidency, staying on the Board as vice-president. Lydia, a computer scientist with a degree from Karlsruhe University and Product Manager for <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">Wikidata</a>, has been a member of the board since 2011 and president of KDE e.V. since 2014. During her tenure, KDE has evolved and developed a vision statement, and instated the "Goals" initiative that gives the community clear targets to work towards. 

KDE also started scaling up during Lydia's presidency, which resulted in employing promotion and documentation experts who are helping with community growth. In a similar bid, the Board headed by Lydia started building up a network of like-minded organizations and companies around KDE that led to the constitution of the <a href="https://ev.kde.org/advisoryboard.php">Advisory Board</a>.

Lydia has now moved on to a vice-presidency post, alongside <B>Eike Hein</B>. Eike is also the Treasurer of KDE e.V.. Since Eike has become Treasurer, KDE has received an unprecedented number of donations and new sponsorships - not only proof that more and more companies see KDE as a reliable FLOSS project, but also a testimony to Eike's persistence. Eike also maintains <a href="https://konversation.kde.org/">Konversation</a>, KDE's IRC/IM client, and has written many core UI pieces of the Plasma 5 desktop, such as the taskbar, the menus, the desktop icon file management, and more. He recently started a new pet project, <a href="https://kirogi.org/">Kirogi</a>: a ground control application for piloting drones.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:150px"><a href="/sites/dot.kde.org/files/Andy_800.jpg"><img src="/sites/dot.kde.org/files/Andy_800.jpg" /></a><br /><figcaption>Andy Betts</figcaption></figure> 

The outgoing Board members are <B>Andy Betts</B> and <B>Thomas Pfeiffer</B>. Andy, who has a a Master in Business Administration, brought his management skills to his post and helped KDE e.V. improve its processes. He is also a talented graphics designer, and has provided advice and skills to the Visual Design Group and Promo and Communications team. Unfortunately, Andy had to give up his seat to tend to other time-consuming matters.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:150px"><a href="/sites/dot.kde.org/files/Thomas_1500.jpg"><img src="/sites/dot.kde.org/files/Thomas_1500.jpg" /></a><br /><figcaption>Thomas Pfeiffer</figcaption></figure> 

Thomas, on the other hand, has been an expert in UX (User Experience) and Usability, the branch of design that seeks to make tools and computer interfaces easier to use, since his university days. On the KDE e.V. Board, Thomas was instrumental in setting up the Advisory Board and managed it, ensuring a healthy communication with our industry and community partners. He was also key in supporting the process towards defining KDE's vision statement, and started the discussion about reducing KDE e.V.'s environmental footprint, which is still ongoing. Apart from his work on the Board, he provided feedback and helped improve the interfaces of Plasma and many of KDE's applications.

The new members stepping in for Andy and Thomas are <B>Neofytos Kolokotronis</B> and <B>Adriaan de Groot</B>.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:150px"><a href="/sites/dot.kde.org/files/neofytos_800.jpg"><img src="/sites/dot.kde.org/files/neofytos_800.jpg" /></a><br /><figcaption>Neofytos Kolokotronis</figcaption></figure> 

To describe Neofytos’ tenure within KDE as "meteoric" would be an understatement. He became active in the community in 2017, proposed a community goal, <a href="http://neofytosk.com/post/streamlined-onboarding-how-far-has-kde-come/">Streamline Onboarding</a> of new community members, and managed to get it picked. He was then elected as a member of the Financial Working Group, and now, in 2019, is a Board member of KDE e.V..

But Neofytos is not a newcomer to the FLOSS world by any means. Although he studied medicine and holds a degree in psychology, his day job revolves around consulting in technology and innovation, mostly doing project management. He uses those skills to improve the communities he works with.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:150px"><a href="/sites/dot.kde.org/files/Adriaan_1500.jpg"><img src="/sites/dot.kde.org/files/Adriaan_1500.jpg" /></a><br /><figcaption>Adriaan de Groot</figcaption></figure> 

While Neofytos is a relatively new hand within KDE e.V., Adriaan de Groot is anything but. Indeed, Adriaan is a KDE veteran who already served on the Board 10 years ago. If you have had any contact with KDE at events at all, you may have met him: he is the tall, congenial KDE-booth staffer and master-of-ceremonies at the BoF wrapups during Akademy. Adriaan is the main developer of <a href="https://calamares.io/">Calamares</a>, the universal distribution installer. Calamares is what allows you to easily install Manjaro, Neon, Netrunner, Open Mandriva, and so many other independent distributions. Adriaan is also a diehard <a href="https://www.freebsd.org/">FreeBSD</a> hacker and user, among many other things.

Leading a community as large and diverse as KDE is not an easy task. Thankfully, the Board has always been made up of talented, persistent and savvy people, and this new iteration is no exception. With Aleix, Lydia, Eike, Neofytos and Adriaan at the helm, KDE is guaranteed a bright future. Congratulations to the new Board members! We can’t wait to see what the community will achieve with their support!
<!--break-->