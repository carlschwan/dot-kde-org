---
title: "Akademy 2019 registration now open"
date:    2019-06-06
authors:
  - "sealne"
slug:    akademy-2019-registration-now-open
---
<img class="alignnone size-full wp-image-135" src="https://cdn.kde.org/akademy/2019/imgoing/Akademy2019BannerBoscoVerticale.png" alt="" width="800" />

<strong>Akademy is free to attend, however you need to <a href="https://akademy.kde.org/2019/register">register</a> to reserve your space.</strong>

Once you have registered, take a look at our <a href="https://akademy.kde.org/2019/travelling-milan">guide on how to travel to Milan</a> and check out <a href="https://akademy.kde.org/2019/accommodation">the accommodation</a> we have arranged and recommend for attendees. We also have a <a href="https://akademy.kde.org/2019/getting-around-milan">guide on how to get from different locations within Milan to Akademy</a>. This guide also includes information on how to move around the city in general -- useful for sightseeing! 

<B style="color: red;">IMPORTANT:</B> All attendees are expected to read and required to follow Akademy's <a href="https://akademy.kde.org/2019/code-conduct">Code of Conduct</a>.

<h2>Badges</h2>

Show your friends you are attending Akademy 2019 by displaying a badge on your blog, your website or social media account:

<div>
<a href="https://community.kde.org/Akademy/2019/Badges">
<img src="/sites/dot.kde.org/files/Akademy2019BannerDuomoMilan-wee.png" width="390" /><img src="/sites/dot.kde.org/files/Akademy2019BannerUnicredit-wee.png" width="390" /><img src="/sites/dot.kde.org/files/Akademy2019BannerBoscoVerticale-wee.png" width="390" /><img src="/sites/dot.kde.org/files/Akademy2019BannerDuomo-wee.png" width="390" />
</a>
</div>

<br />

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://devel-home.kde.org/~duffus/akademy/2018/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2018-groupphoto-teensie.jpg" /></a><br />Akademy 2018, Vienna</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->