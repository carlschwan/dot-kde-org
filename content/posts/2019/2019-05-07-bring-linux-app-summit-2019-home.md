---
title: "Bring the Linux App Summit 2019 home"
date:    2019-05-07
authors:
  - "Paul Brown"
slug:    bring-linux-app-summit-2019-home
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/DSC02236_letterbox_1500w.jpg" alt="Photo by Paul Brown, distributed under CC By SA license." width="800" />

<b>The GNOME and KDE communities are looking for locations for the Linux App Summit (LAS) 2019, an event that will be held sometime between September and December 2019.</b>

The Linux App Summit is an evolution of the Libre Application Summit and has a specific focus on the creation of applications that target Linux devices. By co-hosting the conference, KDE and GNOME want to create a space for a more widespread collaboration and work towards a common goal: make the Linux application ecosystem flourish.

If you are interested in hosting LAS 2019 in your town, send us an e-mail to <a href="mailto:appsummit@lists.freedesktop.org"> appsummit@lists.freedesktop.org</a> by May 15th with your proposed location. This will allow the organizing committee to establish contact with you and give you assistance as you put together a bid later on.

For more information on relevant deadlines, please check out <a href="http://linuxappsummit.org/cfl/">the LAS website</a>.

We look forward to hearing from you!
<!--break-->