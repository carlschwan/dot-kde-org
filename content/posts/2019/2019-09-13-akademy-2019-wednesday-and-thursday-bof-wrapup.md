---
title: "Akademy 2019 Wednesday and Thursday BoF Wrapup"
date:    2019-09-13
authors:
  - "sealne"
slug:    akademy-2019-wednesday-and-thursday-bof-wrapup
---
<a href="https://community.kde.org/Akademy/2019/Wednesday">Wednesday</a> continued the Akademy BoFs, group sessions and hacking in the morning followed by the daytrip in the afternoon to Lake Como, to have some fun, get away from laptops and get to know each other better. <a href="https://community.kde.org/Akademy/2019/Thursday">Thursday</a> was back to BoFs, meetings and hacking culminating in a wrapup session at the end covering the last two days so that what happened in the different rooms can be shared with everyone including those not present.

Watch Thursday's wrapup session in the video below

<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2019/bof_wrapups/thursday.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2019/bof_wrapups/thursday.mp4">Wednesday and Thursday BoF Wrapup video</a>
</video> 


<!--break-->