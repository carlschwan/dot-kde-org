---
title: "KDE Privacy Sprint, 2019 Edition"
date:    2019-06-05
authors:
  - "unknow"
slug:    kde-privacy-sprint-2019-edition
comments:
  - subject: "Fix existing issues in KWallet"
    date: 2019-06-06
    body: "<p>I really like the idea of impovements&nbsp; but I think continuing freedesktop secret service support or issues on gpg encrpted (tell requesting apps to wait until the wallet is open) are more important.</p><p>Also KWallet supporr is long broken since <span>NPAPI support is dead in modern browsers.<br /></span></p>"
    author: "Thaodan"
  - subject: "This is what I would call privacy improvements"
    date: 2019-06-07
    body: "<p>When it comes to privacy, this is what I want to see in any modern desktop environment:</p><p><br /><br />1. Clear indication in the system tray + sound warning&nbsp; when the webcam and the mike is in use and which one of the programs are using them.</p><p><br />2. Clear indication in the system tray + sound warning&nbsp; when a program scans the WIFI networks around me, which could be used to detect my location with incredible accuracy.</p><p><br />3. A firewall easy to understand and use, where I can choose which programs (not ports) are allowed to send and receive data on the network, with a difference between local network and internet.</p><p><br />Such a firewall already exists for Android and it's called AFWall+</p><p><br />https://f-droid.org/en/packages/dev.ukanth.ufirewall/</p><p><br />4. A privacy control panel where I can blacklist / whitelist programs that are forbidden / allowed to use, the webcam, mike, radio networks scan.</p><p><br />5. More notifications about possible privacy issues like:</p><p><br />This program is accessing / accessed the webcam / mike / location scans the hardware</p><p><br />This program is requesting network access with buttons to Allow or block.</p><p><br />Thank you!<br /><br /><br /><br /></p>"
    author: "Unico"
  - subject: "kwallet and wayland"
    date: 2019-06-25
    body: "<p>I have also found kwallet to be an outdated less secure solution in comparison to something like KeepassXC. I'd like to see kwallet get a serious re-work from the bottem up. The https issues and tor proxy idea should be done after password security, since poor password protection is a privacy issue even if your working offline.</p><p>Also the Wayland show stoppers need to be given priority! Get that stuff out of the way so plasma can be stable again! The world is ready for the majority Linux distros to be shipped without the keylogging features of XOrg.</p>"
    author: "anon kde random post 1"
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/dayne-topkin-78982-unsplash_small.jpg" alt="" width="800" />

<b>From the 22nd to 26th of March, members of the KDE Privacy team met up in Leipzig, Germany, for our Spring 2019 sprint.</b>

During the sprint, we floated a lot of different ideas that sparked plenty of discussions. The notion of privacy encompasses a wide range of topics, technologies and methods, so it is often difficult to decide what to focus on. However, all the aspects we worked on are important. We ended up tackling a variety of issues, and we are confident that our contributions will improve data protection for all users of KDE software.

Both Sandro Knauß and Volker Krause regularly work on KDE's Kontact suite (email, calendar, contacts, etc.), but this time they took on network-related issues. One of the problems is that there are still too many <code>http</code> links (instead of secure <code>https</code> links) within our codebase. This is a threat to users' communication, as <code>http</code> connections - and hence all the messages that travel over them - are unencrypted.

To make it easier for all KDE developers, Sandro and Volker wrote an ECM-injected global unit test. The test gets added to every application and prints out warnings about <code>http</code> links used in your code. Another script tries to update all the links in your codebase to use <code>https</code>, but checks beforehand if the <code>https</code> links would work. For example, sourceforge.org subdomains don't provide a certificate, so the script would ignore those.

Things are further complicated by <code>http</code> links that are used as identifiers in XML documents, and those links cannot be changed. All of the above exceptions and niche cases are the reason a simple search-and-replace would not work.

When the script ran, many of the links it found were updates of user-facing links that a normal capable browser would "fix" on the fly. However, it also found privacy leaks, as some links were routed through URL shorteners and pastebin services, as well as to default download locations.

Another thing we identified is that, unfortunately, the KDE mirror network is still using <code>http</code> and the underlying software is not ready to work with <code>https</code>. This means there is still some work we need to carry out to make <a href="http://mirrorbrain.org ">mirrorbrain</a> capable of using <code>https</code>. The website needs a valid certificate, too.

Meanwhile, Ivan Čukić and David Edmundson worked on improving Plasma Vault, KDE's solution for encrypting folders. The aim was to fix the issues that arise when other KDE software components interact with vaults. They made several major improvements: 
<ul>
<li>vaults can now be opened and closed directly from <a href="https://userbase.kde.org/Dolphin">Dolphin</a>;</li> 
<li>offline vaults force the network to be disconnected as soon as the password entry dialogue is shown;</li>  
<li>and thumbnails are not generated for files in FUSE-encrypted directories unless the thumbnail cache is located in the same encrypted mount.</li></ul>  

David and Ivan also spent some time on <a href="https://utils.kde.org/projects/kwalletmanager/">KWallet</a>, KDE's password manager. In a breakout session, David investigated how to handle KWallet sandboxing, and Ivan explored the possibility of doing elliptic-curve encrypted inter-process communication, which could be useful for handling passwords with KWallet.

Florian Müller looked into using the <a href"https://www.torproject.org/">Tor Browser</a> as the default browser in Plasma. He found that it is mostly blocked, as Tor Browser is started with <code>--no-remote</code>, which makes it impossible to trigger new tabs from the outside. To solve the problem, Florian filed a patch against <code>torbrowser-launcher</code>. 

The integration of Tor goes way beyond of just using the browser, though. In fact, the team wants all applications to be able to use Tor. To see if this was possible, we picked some applications and worked on configuring their proxy settings. During the testing, we used a <code>.onion</code> address to make sure that data was correctly sent via the Tor network.

On Monday morning, Jos van den Oever presented a proof-of-concept privacy proxy. The proxy is run by the user, and it intercepts all web traffic, storing it in a local archive. This proxy makes it possible to revisit parts of the Web even without an Internet connection. Additionally, the proxy can block unwanted content by defining filters. 

The presentation was followed by a discussion on how to use such a proxy in KDE software in a user-friendly manner. Jos himself has been using his own proxy privately for a few years, but the code needs to be cleaned up and updated to the current version of Rust libraries before it can be released.

Then again, working for the future is what the Privacy team does most of the time. Gradually, most or all these features (and quite a few more) will make their way into Plasma Desktop and Plasma Mobile, making your desktop and mobile devices a safe environment against data leaks and snooping without sacrificing functionality.
<!--break-->