---
title: "Akademy 2019: Talk Schedule is out!"
date:    2019-07-11
authors:
  - "Paul Brown"
slug:    akademy-2019-talk-schedule-out
comments:
  - subject: "Will talks be recorded"
    date: 2019-07-11
    body: "<p>and streamed / uploaded?</p>"
    author: "NotJay"
---
<a href="https://akademy.kde.org/2019">
  <img src="https://cdn.kde.org/akademy/2019/imgoing/Akademy2019BannerDuomo.png" width="800" alt="I'm going to Akademy 2019" />
</a>

<B><a href="https://akademy.kde.org/2019/program">The schedule for Akademy 2019</a> is out and it is full of interesting and intriguing talks, panels and keynotes.</B>

On day one (Saturday, September 7), the teams that have been working on the <a href="https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond">community goals</a> over the last two years will discuss <a href="https://conf.kde.org/en/akademy2019/public/events/157">how things worked out</a> and <a href="https://conf.kde.org/en/akademy2019/public/events/119">what has been achieved</a> (spoiler: a lot). As many of the procedures and processes developed for the goals have now been worked into the everyday ways the KDE community operates and builds things, it is time to look for new goals. That is precisely what will be happening next, when the panel unveils <a href="https://conf.kde.org/en/akademy2019/public/events/155">what the community has decided to work on in the next two years</a>.

Apart from goals, there will also be time for the bleeding-edge tech KDE is so well-known for. You will find out from Aleix Pol how developers managed to make a complex graphical environment like the <a href="https://conf.kde.org/en/akademy2019/public/events/121">Plasma desktop start up faster</a>, and Marco Martin and Bhushan Shah will show us how Plasma can work everywhere, including on <a href="https://conf.kde.org/en/akademy2019/public/events/109">embedded devices</a>. Taking things a step further still, Aditya Mehra will demonstrate how the open source <a href="https://conf.kde.org/en/akademy2019/public/events/141">Mycroft AI assistant</a> can be the next great thing to assist you while you drive your car.

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/akademy-2018-audience2.jpg" alt="" width="800" />

On Sunday, the schedule is equally full of challenging ideas and fun stuff. You will see what's new in KDE's effort to create <a href="https://conf.kde.org/en/akademy2019/public/events/136">a completely open, privacy-protecting travel assistant</a>, courtesy of Volker Krause. In Akademy 2018 Volker introduced <a href="https://api.kde.org/kdepim/kitinerary/html/index.html">KItinerary</a> and this year he will be talking about <a href="https://api.kde.org/kdepim/kpublictransport/html/index.html">KPublicTransport</a>. Having teamed up with the Open Transport community, KDE is now building a framework which will allow apps to give users a complete travel solution without having to depend on leaky proprietary services.
		
As projects like Mycroft show, KDE is working on integrating AI into the desktop. Trung Thanh Dinh will be explaining <a href="https://conf.kde.org/en/akademy2019/public/events/127">how AI can also be used in the area of face recognition</a>, and how that can be leveraged by KDE's applications. Another thing on the list of revolutionary technologies is that KDE is setting its sights on virtual reality. Cristoph Haag will explain how <a href="https://conf.kde.org/en/akademy2019/public/events/137">VR requires a completely different approach to user interfaces</a> from what we are used to.

Obviously, that is not all. It is but a small cross-section of what you will be able to see at Akademy 2019. Soon we will also unveil our two keynote speakers with interviews here, on the Dot. After the weekend of talks, panels and keynotes, the rest of the week will be dedicated to BoFs (Birds of a Feather sessions), where community members with similar interests get together and work on their projects, as well as coding sessions, meetings, and social activities.

Do not miss Akademy 2019! Join us, <a href="https://akademy.kde.org/2019/register">register for the event now</a>, book your accommodation soon (Milan gets busy!) and meet up with all your KDE friends.

Besides. did we say it is in Milan? That means pasta, pizza, gelato and Gothic architecture. What's not to love?

<h2>Badges</h2>

Show your friends you are attending Akademy 2019 by displaying a badge on your blog, your website or social media account:

<div>
<a href="https://community.kde.org/Akademy/2019/Badges">
<img src="/sites/dot.kde.org/files/Akademy2019BannerDuomoMilan-wee.png" width="390" /><img src="/sites/dot.kde.org/files/Akademy2019BannerUnicredit-wee.png" width="390" /><img src="/sites/dot.kde.org/files/Akademy2019BannerBoscoVerticale-wee.png" width="390" /><img src="/sites/dot.kde.org/files/Akademy2019BannerDuomo-wee.png" width="390" />
</a>
</div>

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href="http://devel-home.kde.org/~duffus/akademy/2018/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2018-groupphoto-teensie.jpg" /></a><br />Akademy 2018, Vienna</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->