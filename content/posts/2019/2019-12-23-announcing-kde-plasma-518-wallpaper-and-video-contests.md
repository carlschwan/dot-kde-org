---
title: "Announcing KDE Plasma 5.18 Wallpaper and Video Contests"
date:    2019-12-23
authors:
  - "unknow"
slug:    announcing-kde-plasma-518-wallpaper-and-video-contests
---
By Niccolò Venerandi

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/filmmaker-2838932_1280_cut.jpg" alt="" width="800" />

<B>KDE wants you to create the next wallpaper for Plasma 5.18 and the promotional videos for Plasma and applications of KDE.</B>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/images.community/thumb/a/ab/Txedo_InfinityBookPro14_logo.png/750px-Txedo_InfinityBookPro14_logo.png"><img src="https://community.kde.org/images.community/thumb/a/ab/Txedo_InfinityBookPro14_logo.png/750px-Txedo_InfinityBookPro14_logo.png" width="200" /></a><br /><figcaption>TUXEDO InfinityBook Pro 14</figcaption></figure> 

The chance of getting your work seen by thousands of people and organizations worldwide, including at NASA and CERN, is within your grasp! You can also win some really astounding prizes courtesy of our friends at <a href="https://www.tuxedocomputers.com">TUXEDO Computers</a>.

<h2>Prizes</h2>

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/images.community/thumb/e/e3/Tuxedogaming.jpg/600px-Tuxedogaming.jpg"><img src="https://community.kde.org/images.community/thumb/e/e3/Tuxedogaming.jpg/600px-Tuxedogaming.jpg" width="200" /></a><br /><figcaption>TUXEDO Gaming PC</figcaption></figure> 

The winner of the wallpaper contest will have their work included as the default background on KDE's upcoming Plasma 5.18 desktop. This means you will not only earn the admiration of thousands of Plasma users, but you can also win a very cool <a href="https://www.tuxedocomputers.com/ibp14">TUXEDO InfinityBook Pro 14</a> computer.

More details about the InfinityBook Pro are available on the <a href="https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.18_Wallpaper_Competition">Wallpaper Contest's page</a>.

Is fillmmaking more your thing? Try your hand at shooting and editing an exciting promotional video for Plasma or for applications KDE makes. The winners of the best Plasma promotional video will win a <B>TUXEDO Gaming PC</B>, and if you win the best Applications video competition, you'll get a <B>TUXEDO InfinityBox</B>.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/images.community/thumb/0/0f/Tuxedopc.jpg/800px-Tuxedopc.jpg"><img src="https://community.kde.org/images.community/thumb/0/0f/Tuxedopc.jpg/800px-Tuxedopc.jpg" width="200" /></a><br /><figcaption>TUXEDO InfinityBox</figcaption></figure> 

You can read about the specs of each machine on <a href="https://community.kde.org/Promo/Plasma_5.18_Video_Competition">the Video Contest's page</a>.

Twelve finalists will also receive a package of goodies containing among other things:

<ul>
<li>A KDE Baseball cap</li>
<li>A plush Tux</li>
<li>KDE Stickers</li>
<li>A frozen glass coffee mug</li>
</ul>

<h2>How to Participate</h2>

Taking part is easy!  Check out <a href="https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.18_Wallpaper_Competition">the rules for Wallpaper Competition</a> and send in your masterpiece. Remember that, in order to submit a wallpaper, you need to follow the link to the appropriate subforum where you can create a new post. You can also find suggestions and helpful material on the webpages.

Want to make a video instead? Read <a href="https://community.kde.org/Promo/Plasma_5.18_Video_Competition">the rules for the Video Competition</a> carefully and start shooting your clip!

We can't wait to see what you will create!
<!--break-->