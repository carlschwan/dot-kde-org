---
title: "Announcing Season of KDE 2020"
date:    2019-11-29
authors:
  - "unknow"
slug:    announcing-season-kde-2020
---
<p style="font-size: small">By Caio Jordão Carvalho</p>

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/SoK.jpg" alt="" width="800" />

<b>After a one-year hiatus, KDE Student Programs is very happy to announce <a href="https://season.kde.org/">Season of KDE 2020</a>!</b>

Focused on offering an opportunity to anyone (not just enrolled students) contributing to the KDE community, this is a program that is comparable to the well-known Google Summer of Code, with some special differences. A key difference is that <a href="https://community.kde.org/SoK">SoK</a> projects are not limited to code-focused work, but any that benefit our community. For instance, projects can be about documentation, reports, translation, system administration, web and other types of work as well as code. Each contributor will work with a mentor and within a team that will also help the contributor.

<h2>Schedule</h2>

This year we have decreased the duration of the projects. Previously, all projects were 80 days long. However, during SoK 2018 we included the  option of 40-day projects. This new option was widely adopted by participants during 2018 and, so we decided to keep only this alternative.

<h3>Timeline:</h3>

<ul>
<li><b>From the 9th of December 2019 to the 3rd of January 2020:</b> Participant and Mentor Application period</li>
<li><b>6th of January 2020:</b> Projects announced</li>
<li><b>8th of January 2020, 00:00 UTC:</b> SoK work period begins</li>
<li><b>17th of February 2020, 23:59 UTC:</b> End of work</li>
<li><b>21st of February 2020:</b> Results announced</li>
<li><b>28th of February 2020:</b> Certificates issued</li>
<li><b>Beginning of Q3 2020:</b> Merchandise and Swag sent out by courier</li>
</ul>

<h2>Getting Started</h2>

Prospective participants should get in touch with us before the application period begins to discuss possible projects. You can connect with us <a href="https://riot.im/app/#/room/%23kde-soc:kde.org">on Matrix</a>, in the #kde-soc room on IRC, in <a href="https://telegram.me/joinchat/A-9tjgavn9YJ2myF1aTK4A">KDE-SoC</a> on Telegram, or through <a href="https://mail.kde.org/mailman/listinfo/kde-soc">our mailing list</a>. Besides talking to the SoK team, contact the <a href="https://kde.org/applications">application maintainer and team</a> with whom you want to work.

If you’re looking for project ideas, you can find some on our <a href="https://community.kde.org/SoK/Ideas/2020">KDE Season of Code 2020 Ideas Page</a>. Mentors please add ideas, so that we have a central repository of project ideas for  Season of KDE 2020 and even GSoC 2020. Applicants will work with the teams to develop a proposal, and the SoK admin team will help too.

Help us spread the word! Tell your friends, blog, tweet, and share on Facebook using the #2020SeasonKDE hashtag.

Participants and mentors can apply <a href="https://season.kde.org">here</a> once applications open.
<!--break-->
