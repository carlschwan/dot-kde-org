---
title: "Akademy 2019 Tuesday BoF Wrapup"
date:    2019-09-10
authors:
  - "sealne"
slug:    akademy-2019-tuesday-bof-wrapup
---
<a href="https://community.kde.org/Akademy/2019/Tuesday">Tuesday</a> continued the Akademy BoFs, group sessions and hacking. There is a wrapup session at the end of the day so that what happened in the different rooms can be shared with everyone including those not present.

Watch Tuesday's wrapup session in the video below

<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2019/bof_wrapups/tuesday.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2019/bof_wrapups/tuesday.mp4">Tuesday BoF Wrapup video</a>
</video> 


<!--break-->