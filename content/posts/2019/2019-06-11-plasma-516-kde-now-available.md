---
title: "Plasma 5.16 by KDE is Now Available"
date:    2019-06-11
authors:
  - "Paul Brown"
slug:    plasma-516-kde-now-available
comments:
  - subject: "do not disturb and calendar reminders"
    date: 2019-06-19
    body: "<p>I wonder if it would make sense to port the KOrganizer reminder daemon to use the notification system rather than its own popup dialog.&nbsp; I have always wanted to block the reminders if I'm in a \"do not disturb mode\".&nbsp; Who knows the notification system and can talk to me about the pros/cons.&nbsp; One obvious need is the ability to snooze each reminder.</p><p>&nbsp;</p>"
    author: "awinterz"
  - subject: "Great suggestion!"
    date: 2019-06-23
    body: "<p>I hope the developers agree.</p>"
    author: "rspires1952"
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/Plasma%205.16%20%282%29.png" alt="" width="800" />

<B>Say hello to <a href="https://kde.org/announcements/plasma-5.16.0.php">Plasma 5.16</a>, a the newest iteration of KDE's desktop environment, chock-a-block with new features and improvements.</B>

For starters, check out the new notification system! Not only can you mute notifications altogether with the <I>Do Not Disturb</I> mode, but the system also groups notifications by app. Like this, when you run through the history of past notifications, you can see all the messages from KDE Connect in one category, the download information in another, email alerts in a third, and so on.

<a href="https://userbase.kde.org/Discover">Discover</a>, Plasma's software manager, is also cleaner and clearer as it now has two distinct areas for <I>downloading</I> and <I>installing</I> software on the <I>Update</I> page. Besides, when updating, the completion bar now works correctly and the packages disappear from the list as the software manager completes their installation.

<p align="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/T-29hJUxoFQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

With each new version, we make Plasma safer, and protect your data better. In 5.16 we have made using <I>Vaults</I> easier and more convenient. Vaults are a built-in utility to encrypt folders, installed by default on Plasma, and you can now open them directly from Dolphin.

Protecting your privacy requires that we focus on the small details, too. When any application accesses the microphone, an icon will pop up in your system tray, showing that something is listening. It doesn't matter whether it is graphical application, a web app or a process started on the command line - the icon will show up alerting you to the fact. You can deactivate your mic by clicking the icon with the middle button on your mouse.

Plasma 5.16 is also spectacular to look at, with our new wallpaper called <I>Ice Cold</I>. Designed by Santiago Cézar, it is the winner of a contest with more than 150 entries. Ice Cold will keep you cool and feeling fresh for the upcoming summer months. Again, giving you options is what Plasma is all about, so we have included a few other contest submissions for you to choose from. 

Talking of options, many of the pages in the <I>System Settings</I> have been redesigned, with clearer icons and friendlier layouts. Getting back to wallpapers: the <I>Wallpaper Slideshow</I> settings window displays the images in the folders you selected, and lets you select only the graphics you want to display in the slideshow. To improve visibility, window and menu shadow colors are pure black - something that works especially well when using dark themes.

And if creating themes is your thing, many of the hard-coded look and feel options are gone know, allowing you full freedom to design widgets to your liking. You'll notice it in the small things, like the analog clock handles: Plasma themes now let you tweak their look by adjusting the offset and toggling the blur behind panels.

For a more comprehensive overview of what to expect in Plasma 5.16, check out the <a href="https://kde.org/announcements/plasma-5.16.0.php">official announcement</a> or <a href="https://kde.org/announcements/plasma-5.15.5-5.16.0-changelog.php">the changelog</a> for the complete list of changes.

Functional, feature-rich, privacy-protecting and beautiful... What else could you ask for? Look out for Plasma 5.16 in a distribution near you!
<!--break-->