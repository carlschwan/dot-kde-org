---
title: "Plasma 5.17 Beta Out for Testing"
date:    2019-09-19
authors:
  - "jriddell"
slug:    plasma-517-beta-out-testing
---
    <figure class="topImage">
        <a href="http://www.kde.org/announcements/plasma-5.17/plasma-5.17.png" data-toggle="lightbox">
            <img src="http://www.kde.org/announcements/plasma-5.17/plasma-5.17-wee.png" height="342" width="600" style="width: 100%; max-width: 600px; height: auto;" alt="Plasma 5.17 Beta" />
        </a>
        <figcaption>KDE Plasma 5.17 Beta</figcaption>
    </figure>

    <p>Thursday, 19 September 2019.</p>
    <p>Today KDE launches the beta release of Plasma 5.17.</p>

    <p>We've added a bunch of new features and improvements to KDE's lightweight yet full featured desktop environment.</p>
    
    <p><a href='https://kde.org/plasma-desktop'>Plasma's updated web page</a> gives more background on why you should use it on your computer.</p>

    <figure style="float: right; text-align: right">
    <a href="http://www.kde.org/announcements/plasma-5.17/guillermo.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/guillermo-wee.png" style="padding: 10px" width="350" height="334" alt="Guillermo Amaral" />
    </a>
    <figcaption style="text-align: right">Guillermo Amaral</figcaption>
    </figure>

    <p>System Settings has gained new features to help you manage your fancy Thunderbolt hardware, plus Night Color is now on X11 and a bunch of pages got redesigned to help you get your configuration done easier.  Our notifications continue to improve with a new icon and automatic do-not-disturb mode for presentations.  Our Breeze GTK theme now provides a better appearance for the Chromium/Chrome web browsers and applies your color scheme to GTK and GNOME apps. The window manager KWin has received many HiDPI and multi-screen improvements, and now supports fractional scaling on Wayland.</p>
    <p>You can test the Plasma 5.17 beta for the next three weeks until the final release in mid-October.  Give it a whirl with your favorite distribution!</p>

    <p>The Plasma 5.17 series is dedicated to our friend Guillermo Amaral. Guillermo was an enthusiastic KDE developer who rightly self described as 'an incredibly handsome multidisciplinary self-taught engineer'.  He brought cheer to anyone he met.  He lost his battle with cancer last summer but will be remembered as a friend to all he met.</p>

    <br clear="all" />

    <h3 id="desktop">Plasma</h3>
    <figure style="float: right; text-align: right">
    <a href="http://www.kde.org/announcements/plasma-5.17/unsplash-pic-of-day.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/unsplash-pic-of-day-wee.png" style="border: 0px" width="350" height="280" alt="<a href='https://unsplash.com/'>Unsplash</a> Picture of the Day" />
    </a>
    <figcaption style="text-align: right"><a href='https://unsplash.com/'>Unsplash</a> Pic of the Day</figcaption>
    <br />
    <a href="http://www.kde.org/announcements/plasma-5.17/inches-to-cm.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/inches-to-cm-wee.png" style="border: 0px" width="350" height="159" alt="<a href='https://unsplash.com/'>Unsplash</a> Pic of the Day" />
    </a>
    <figcaption style="text-align: right">KRunner now converts fractional units</figcaption>
    <br />
    <a href="http://www.kde.org/announcements/plasma-5.17/notification-widget.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/notification-widget-wee.png" style="border: 0px" width="250" height="230" alt="Improved Notifications widget and widget editing UX" />
    </a>
    <figcaption style="text-align: right">Improved Notifications widget and widget editing UX</figcaption>
    </figure>

    <ul>
<li>Do Not Disturb mode is automatically enabled when mirroring screens (e.g. when delivering a presentation)</li>
<li>The Notifications widget now uses an improved icon instead of displaying the number of unread notifications</li>
<li>Improved widget positioning UX, particularly for touch</li>
<li>Improved the Task Manager's middle-click behavior: middle-clicking on an open app's task opens a new instance, while middle-clicking on its thumbnail will close that instance</li>
<li>Slight RGB hinting is now the default font rendering mode</li>
<li>Plasma now starts even faster!</li>
<li>Conversion of fractional units into other units (e.g. 3/16" == 4.76 mm) in KRunner and Kickoff</li>
<li>Wallpaper slideshows can now have user-chosen ordering rather than always being random</li>
<li>New <a href='https://unsplash.com/'>Unsplash</a> picture of the day wallpaper source with categories</li>
<li>Much better support for public WiFi login</li>
<li>Added the ability to set a maximum volume that's lower than 100%</li>
<li>Pasting text into a sticky note strips the formatting by default</li>
<li>Kickoff's recent documents section now works with GNOME/GTK apps</li>
<li>Fixed Kickoff tab appearance being broken with vertical panels</li>
    </ul>

    <br clear="all" />
    
    <h3 id="systemsettings">System Settings: Thunderbolt, X11 Night Color and Overhauled Interfaces</h3>
    <figure style="float: right; text-align: right">
    <a href="http://www.kde.org/announcements/plasma-5.17/night-color.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/night-color-wee.png" style="border: 0px" width="350" height="250" alt="Night Color settings are now available on X11 too" />
    </a>
    <figcaption style="text-align: right">Night Color settings are now available on X11 too</figcaption>
    <br />
    <a href="http://www.kde.org/announcements/plasma-5.17/thunderbolt.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/thunderbolt-wee.png" style="border: 0px" width="350" height="344" alt="Thunderbolt device management" />
    </a>
    <figcaption style="text-align: right">Thunderbolt device management</figcaption>
    <br />
    <a href="http://www.kde.org/announcements/plasma-5.17/settings-consistency.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/settings-consistency-wee.png" style="border: 0px" width="390" height="280" alt="Reorganized Appearance settings, consistent sidebars and headers" />
    </a>
    <figcaption style="text-align: right">Reorganized Appearance settings, consistent sidebars and headers</figcaption>
    </figure>

    <ul>
<li>New settings panel for managing and configuring Thunderbolt devices</li>
<li>The Night Color settings are now available on X11 too.  It gets a modernized and redesigned user interface, and the feature can be manually invoked in the settings or with a keyboard shortcut.</li>
<li>Overhauled the user interface for the Displays, Energy, Activities, Boot Splash, Desktop Effects, Screen Locking, Screen Edges, Touch Screen, and Window Behavior settings pages and the SDDM advanced settings tab</li>
<li>Reorganized and renamed some settings pages in the Appearance section</li>
<li>Basic system information is now available through System Settings</li>
<li>Added accessibility feature to move your cursor with the keyboard when using Libinput</li>
<li>You can now apply a user's font, color scheme, icon theme, and other settings to the SDDM login screen to ensure visual continuity on single-user systems</li>
<li>New 'sleep for a few hours and then hibernate' feature</li>
<li>The Colors page now displays the color scheme's titlebar colors</li>
<li>It is now possible to assign a global keyboard shortcut to turn off the screen</li>
<li>Standardized appearance for list headers</li>
<li>The 'Automatically switch all running streams when a new output becomes available' feature now works properly</li>
    </ul>

    <br clear="all" />
    
    <h3 id="breeze">Breeze Theme</h3>
    <figure style="float: right; text-align: right">
    <a href="http://www.kde.org/announcements/plasma-5.17/window-borders.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/window-borders-wee.png" style="border: 0px" width="350" height="250" alt="Window borders are now turned off by default" />
    </a>
    <figcaption style="text-align: right">Window borders are now turned off by default</figcaption>
    </figure>

    <ul>
<li>The Breeze GTK theme now respects your chosen color scheme</li>
<li>Active and inactive tabs in Google Chrome and Chromium now look visually distinct</li>
<li>Window borders are now turned off by default</li>
<li>Sidebars in settings windows now have a consistent modernized appearance</li>
    </ul>

    <br clear="all" />
    
    <h3 id="ksysguard">System Monitor</h3>
    <figure style="float: right; text-align: right">
    <a href="http://www.kde.org/announcements/plasma-5.17/ksysguard.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/ksysguard-wee.png" style="border: 0px" width="350" height="157" alt="CGroups in System Monitor" />
    </a>
    <figcaption style="text-align: right">CGroups in System Monitor</figcaption>
    </figure>

    <ul>
<li>System Monitor can now show CGroup details to look at container limits</li>
<li>Each process can now report its network usage statistics</li>
<li>It is now possible to see NVidia GPU stats</li>
    </ul>

    <br clear="all" />
    
    <h3 id="discover">Discover</h3>
    <figure style="float: right; text-align: right">
    <a href="http://www.kde.org/announcements/plasma-5.17/discover.png" data-toggle="lightbox">
    <img src="http://www.kde.org/announcements/plasma-5.17/discover-wee.png" style="border: 0px" width="350" height="255" alt="Discover now has icons on the sidebar" />
    </a>
    <figcaption style="text-align: right">Discover now has icons on the sidebar</figcaption>
    </figure>

    <ul>
<li>Real progress bars and spinners in various parts of the UI to better communicate progress information</li>
<li>Better 'No connection' error messages</li>
<li>Icons in the sidebar and icons for Snap apps</li>
    </ul>
    <br clear="all" />
    
    <h3 id="kwin">KWin: Improved Display Management</h3>
    <ul>
<li>Fractional scaling added on Wayland</li>
<li>It is now once again possible to close windows in the Present Windows effect with a middle-click</li>
<li>Option to configure whether screen settings apply only for the current screen arrangement or to all screen arrangements</li>
<li>Many multi-screen and HiDPI improvements</li>
<li>On Wayland, it is now possible to resize GTK headerbar windows from window edges</li>
<li>Scrolling with a wheel mouse on Wayland now always scrolls the correct number of lines</li>
<li>On X11, it is now possible to use the Meta key as a modifier for the window switcher that's bound to Alt+Tab by default</li>
    </ul>

    <br clear="all" />

    <a href="http://www.kde.org/announcements/plasma-5.16.5-5.16.90-changelog.php">Full Plasma 5.16.90 changelog</a>

    <!-- // Boilerplate again -->
    <section class="row get-it">
        <article class="col-md">
            <h2>Live Images</h2>
            <p>
                The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more">Download live images with Plasma 5</a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more">Download Docker images with Plasma 5</a>
        </article>

        <article class="col-md">
            <h2>Package Downloads</h2>
            <p>
                Distributions have created, or are in the process of creating, packages listed on our wiki page.            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more">Get KDE Software on Your Linux Distro wiki page</a>
        </article>

        <article class="col-md">
            <h2>Source Downloads</h2>
            <p>
                You can install Plasma 5 directly from source.            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'>Community instructions to compile it</a>
            <a href='http://www.kde.org/info/plasma-5.15.90.php' class='learn-more'>Source Info Page</a>
        </article>
    </section>

    <section class="give-feedback">
        <h2>Feedback</h2>

        <p class="kSocialLinks">
            You can give us feedback and get updates on our social media channels:            <a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
            <a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
            <a class="shareDiaspora" href="https://joindiaspora.com/people/9c3d1a454919ef06" rel="nofollow">Share on Diaspora</a>
            <a class="shareReddit" href="http://www.reddit.com/r/kde/" rel="nofollow">Share on Reddit</a>
            <a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" rel="nofollow">Share on YouTube</a>
            <a class="shareMastodon" href="https://mastodon.technology/@kde" rel="nofollow">Share on Mastodon</a>
            <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">Share on LinkedIn</a>
            <a class="sharePeerTube" href="https://peertube.mastodon.host/accounts/kde/videos" rel="nofollow">Share on PeerTube</a>
        </p>
        <p>
            Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.        </p>

        <p>You can provide feedback direct to the developers via the <a href='https://webchat.kde.org/#/room/#cutehmi:kde.org'>Plasma Matrix chat room</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided'>bugzilla</a>. If you like what the team is doing, please let them know!
        <p>Your feedback is greatly appreciated.</p>
<!--break-->

