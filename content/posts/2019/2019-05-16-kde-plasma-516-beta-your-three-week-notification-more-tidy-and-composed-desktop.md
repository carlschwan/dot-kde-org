---
title: "KDE Plasma 5.16 Beta: Your Three Week Notification for a More Tidy and Composed Desktop"
date:    2019-05-16
authors:
  - "jriddell"
slug:    kde-plasma-516-beta-your-three-week-notification-more-tidy-and-composed-desktop
comments:
  - subject: "(fyi I can't load images when"
    date: 2019-05-16
    body: "<p>(fyi I can't load images when using more restricted security settings because of mixed https/http resources)</p>"
    author: "assburguer"
  - subject: "Praise you!"
    date: 2019-05-18
    body: "<p>Absolutely great!</p>"
    author: "newk"
  - subject: "Nice improvements, but..."
    date: 2019-06-07
    body: "<p>Nice improvements, but besides the microphone icon, I wish that also when an app is recording video, a webcam icon would appear in the System Tray which allows to stop it using mouse middle click similar to the microphone one.</p>"
    author: "Unico"
---
    <figure class="topImage">
        <a href="https://www.kde.org/announcements/plasma-5.16/plasma-5.16-full-2.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.16/plasma-5.16-full-2-wee.png" height="338" width="600" style="width: 100%; max-width: 600px; height: auto;" alt="Plasma 5.16" />
        </a>
        <figcaption>KDE Plasma 5.16</figcaption>
    </figure>

    <p>Thursday, 16 May 2019.</p>
    <p>Today KDE launches the beta release of <a href="https://kde.org/announcements/plasma-5.15.90.php">Plasma 5.16</a>.</p>

    <p>In this release, many aspects of Plasma have been polished and 
    rewritten to provide high consistency and bring new features. There is a completely rewritten notification system supporting Do Not Disturb mode, more intelligent history with grouping, critical notifications in fullscreen apps, improved notifications for file transfer jobs, a much more usable System Settings page to configure everything, and many other things. The System and 
    Widget Settings have been refined and worked on by porting code to 
    newer Kirigami and Qt technologies and polishing the user interface. 
    And of course the VDG and Plasma team effort towards <a href="https://community.kde.org/Goals/Usability_%26_Productivity">Usability & Productivity 
    goal</a> continues, getting feedback on all the papercuts in our software that make your life less 
    smooth and fixing them to ensure an intuitive and consistent workflow for your 
    daily use.</p>

    <p>For the first time, the default wallpaper of Plasma 5.16 will 
    be decided by a contest where everyone can participate and submit art. The 
    winner will receive a Slimbook One v2 computer, an eco-friendly, compact 
    machine, measuring only 12.4 x 12.8 x 3.7 cm. It comes with an i5 processor, 8 
    GB of RAM, and is capable of outputting video in glorious 4K. Naturally, your 
    One will come decked out with the upcoming KDE Plasma 5.16 desktop, your 
    spectacular wallpaper, and a bunch of other great software made by KDE. You can find 
    more information and submitted work on <a href="https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.16_Wallpaper_Competition">the competition wiki 
    page</a>, and you can <a href="https://forum.kde.org/viewtopic.php?f=312&t=160487">submit your own wallpaper in the 
    subforum</a>.</p>

    <h3 id="desktop">Desktop Management</h3>
    <figure style="float: right; text-align: right">
    <a href="https://www.kde.org/announcements/plasma-5.16/notifications.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/notifications-wee.png" style="border: 0px" width="350" height="315" alt="New Notifications" />
    </a>
    <figcaption style="text-align: right">New Notifications</figcaption>
    <br clear="all" />
    <a href="https://www.kde.org/announcements/plasma-5.16/plasma-theme-fixes.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/plasma-theme-fixes.png" style="border: 0px" width="281" height="251" alt="Theme Engine Fixes for Clock Hands!" />
    </a>
    <figcaption style="text-align: right">Theme Engine Fixes for Clock Hands!</figcaption>
    <br clear="all" />
    <a href="https://www.kde.org/announcements/plasma-5.16/panel-edit-mode-alternatives.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/panel-edit-mode-alternatives-wee.png" style="border: 0px" width="350" height="183" alt="Panel Editing Offers Alternatives" />
    </a>
    <figcaption style="text-align: right">Panel Editing Offers Alternatives</figcaption>
    <br clear="all" />
    <a href="https://www.kde.org/announcements/plasma-5.16/sddm-theme.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/sddm-theme-wee.png" style="border: 0px" width="350" height="490" alt="Login Screen Theme Improved" />
    </a>
    <figcaption style="text-align: right">Login Screen Theme Improved</figcaption>
    </figure>

    <ul>
    <li>Completely rewritten notification system supporting Do Not Disturb mode, more intelligent history with grouping, critical notifications in fullscreen apps, improved notifications for file transfer jobs, a much more usable System Settings page to configure everything, and more!</li>
    <li>Plasma themes are now correctly applied to panels when selecting a new theme.</li>
    <li>More options for Plasma themes: offset of analog clock hands and toggling blur behind.</li>
    <li>All widget configuration settings have been modernized and now feature an improved UI. The Color Picker widget also improved, now allowing dragging colors from the plasmoid to text editors, palette of photo editors, etc.</li>
    <li>The look and feel of lock, login and logout screen have been improved with new icons, labels, hover behavior, login button layout and more.</li>
    <li>When an app is recording audio, a microphone icon will now appear in the System Tray which allows for changing and muting the volume using mouse middle click and wheel. The Show Desktop icon is now also present in the panel by default.</li>
    <li>The Wallpaper Slideshow settings window now displays the images in the selected folders, and allows selecting and deselecting them.</li>
    <li>The Task Manager features better organized context menus and can now be configured to move a window from a different virtual desktop to the current one on middle click.</li>
    <li>The default Breeze window and menu shadow color are back to being pure black, which improves visibility of many things especially when using a dark color scheme.</li>
    <li>The "Show Alternatives..." button is now visible in panel edit mode, use it to quickly change widgets to similar alternatives.</li>
    <li>Plasma Vaults can now be locked and unlocked directly from Dolphin.</li>
    </ul>

    <br clear="all" />
    
    <h3 id="settings">Settings</h3>
    <figure style="float: right; text-align: right">
    <a href="https://www.kde.org/announcements/plasma-5.16/color-scheme.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/color-scheme-wee.png" style="border: 0px" width="350" height="321" alt="Color Scheme" />
    </a>
    <figcaption style="text-align: right">Color Scheme</figcaption>
    <br clear="all"/>
    <a href="https://www.kde.org/announcements/plasma-5.16/application-style.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/application-style-wee.png" style="border: 0px" width="350" height="194" alt="Application Style and Appearance Settings" />
    </a>
    <figcaption style="text-align: right">Application Style and Appearance Settings</figcaption>
    </figure>

    <ul>
    <li>There has been a general polish in all pages; the entire Appearance section has been refined, the Look and Feel page has moved to the top level, and improved icons have been added in many pages.</li>

    <li>The Color Scheme and Window Decorations pages have been redesigned with a more consistent grid view. The Color Scheme page now supports filtering by light and dark themes, drag and drop to install themes, undo deletion and double click to apply.</li>

    <li>The theme preview of the Login Screen page has been overhauled.</li>

    <li>The Desktop Session page now features a "Reboot to UEFI Setup" option.</li>

    <li>There is now full support for configuring touchpads using the Libinput driver on X11.</li>
    </ul>

    <br clear="all" />
    
    <h3 id="settings">Window Management</h3>
    <figure style="float: right; text-align: right">
    <a href="https://www.kde.org/announcements/plasma-5.16/window-management.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/window-management-wee.png" style="border: 0px" width="350" height="197" alt="Window Management" />
    </a>
    <figcaption style="text-align: right">Window Management</figcaption>
    </figure>

    <ul>
    <li>Initial support for using Wayland with proprietary Nvidia drivers has been added. When using Qt 5.13 with this driver, graphics are also no longer distorted after waking the computer from sleep.</li>

    <li>Wayland now features drag and drop between XWayland and Wayland native windows.</li>

    <li>Also on Wayland, the System Settings Libinput touchpad page now allows you to configure the click method, switching between "areas" or "clickfinger".</li>

    <li>KWin's blur effect now looks more natural and correct to the human eye by not unnecessary darkening the area between blurred colors.</li>

    <li>Two new default shortcuts have been added: Meta+L can now be used by default to lock the screen and Meta+D can be used to show and hide the desktop.</li>

    <li>GTK windows now apply correct active and inactive colour scheme.</li>
    </ul>

    <br clear="all" />
    
    <h3 id="plasma-nm">Plasma Network Manager</h3>
    <figure style="float: right; text-align: right">
    <a href="https://www.kde.org/announcements/plasma-5.16/plasma-nm-wireguard.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/plasma-nm-wireguard-wee.png" style="border: 0px" width="350" height="287" alt="Plasma Network Manager with Wireguard" />
    </a>
    <figcaption style="text-align: right">Plasma Network Manager with Wireguard</figcaption>
    </figure>
    
    <ul>
    <li>The Networks widget is now faster to refresh Wi-Fi networks and more reliable at doing so. It also has a button to display a search field to help you find a particular network from among the available choices. Right-clicking on any network will expose a "Configure…" action.</li>
    <li>WireGuard is now compatible with NetworkManager 1.16.</li>
    <li>One Time Password (OTP) support in Openconnect VPN plugin has been added.</li>
    </ul>

    <br clear="all" />
    
    <h3 id="settings">Discover</h3>
    <figure style="float: right; text-align: right">
    <a href="https://www.kde.org/announcements/plasma-5.16/discover-update.png" data-toggle="lightbox">
    <img src="https://www.kde.org/announcements/plasma-5.16/discover-update-wee.png" style="border: 0px" width="350" height="250" alt="Updates in Discover" />
    </a>
    <figcaption style="text-align: right">Updates in Discover</figcaption>
    </figure>

    <ul>
    <li>In Discover's Update page, apps and packages now have distinct "downloading" and "installing" sections. When an item has finished installing, it disappears from the view.</li>
    <li>Tasks completion indicator now looks better by using a real progress bar. Discover now also displays a busy indicator when checking for updates.</li>
    <li>Improved support and reliability for AppImages and other apps that come from store.kde.org.</li>
    <li>Discover now allows you to force quit when installation or update operations are proceeding.</li>
    <li>The sources menu now shows the version number for each different source for that app.</li>
    </ul>

<a href="https://kde.org/announcements/plasma-5.15.90.php">Read the full announcement</a>
<!--break-->
