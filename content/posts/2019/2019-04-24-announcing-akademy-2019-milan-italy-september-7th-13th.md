---
title: "Announcing Akademy 2019 in Milan, Italy (September 7th - 13th)"
date:    2019-04-24
authors:
  - "sealne"
slug:    announcing-akademy-2019-milan-italy-september-7th-13th
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/milanpanoramic.jpg" alt="Photo by Romain Pontida, distributed under CC By SA license. Original at https://www.flickr.com/photos/romainpontida/39707850020/" width="800" />

<b><a href="https://akademy.kde.org/2019">Akademy 2019</a> will be held at the <a href="https://www.unimib.it/unimib-international">University of Milano-Bicocca</a> in <a href="https://en.wikipedia.org/wiki/Milan">Milan</a>, Italy, from Saturday the 7th to Friday the 13th of September.</b>

The conference is expected to draw hundreds of attendees from the global <a href="https://www.kde.org">KDE</a> community to discuss and plan the future of the community and its technology. Many participants from the broad Free and Open Source software community, local organizations and software companies will also attend.

<a href="https://ev.kde.org">KDE e.V.</a> is organizing Akademy 2019 with <a href="https://unixmib.github.io/">unixMiB</a> &mdash; the Linux User Group of the University of Milano-Bicocca. unixMiB aims to spread Open Source philosophy among students.

<h2>Akademy 2019 Program</h2>

Akademy 2019 will start with 2 days of talks on Saturday and Sunday (7th and 8th of September), followed by 5 days of workshops, Birds of a Feather (BoF), training and coding sessions.

<h2>Call for Papers</h2>

The goal of the conference section of Akademy is to learn and teach new skills, and share our passion around what we're doing together at KDE.

For the sharing of ideas, experiences and state of things, we will have short Fast Track sessions in a single-track section of Akademy. Teaching and sharing technical details is done through longer sessions in the multi-track section of Akademy.

If you have an idea or a story that you would like to present, please tell us about it. If you know of someone else who should present, please encourage them to get in touch. For more details, see the proposal guidelines and the <a href="http://akademy.kde.org/2019/cfp">Call for Papers</a>.

<strong>The submission deadline is 31st May, 23:59:59 CEST.</strong>

<h2>About Milan</h2>

With a population of over 1.5 million inhabitants, Milan has a rich history that stretches back to 600 BC. The city boasts interesting museums, fascinating archaeological sites and breathtaking architecture and monuments. It is also renowned for its excellent food and wine.

Milan is considered a <a href='https://en.wikipedia.org/wiki/Global_city'>global city</a>, with strengths in the field of the arts, commerce, design, education, entertainment, fashion, finance, healthcare, media, services, research, and tourism. It is the third-largest economy among European cities after Paris and London, but the fastest in growth among the three. It is also the wealthiest among European non-capital cities.

Apart from being recognized as the world's fashion and design capital, Milan has a strong IT-based sector and is home to powerhouses such as Disney and SUSE. The adjacent towns are home to IBM Italia, Samsung and the CILEA supercomputing center, one of the most advanced HPC centers in Europe.

<h2>About the University of Milano-Bicocca</h2>

Akademy will be held in the halls of the University of Milano-Bicocca. Milano-Bicocca is a public university that provides undergraduate, graduate and post-graduate education in a wide range of disciplinary fields including Economics, Informatics, Statistics, Law, Education, Sociology, Medicine and Surgery, Maths, Natural Sciences, Physics and Astrophysics, Chemistry, Computer Sciences, Biotechnology, and Psychology.

The university is located to the north and east of the center of Milan and can easily be reached by bus and metro/underground.

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://devel-home.kde.org/~duffus/akademy/2018/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2018-groupphoto-teensie.jpg" /></a><br />Akademy 2018, Vienna</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->