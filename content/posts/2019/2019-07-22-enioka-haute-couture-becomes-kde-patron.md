---
title: "enioka Haute Couture Becomes a KDE Patron"
date:    2019-07-22
authors:
  - "unknow"
slug:    enioka-haute-couture-becomes-kde-patron
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/enioka-haute-couture-logo.svg_.png" alt="" width="800" />

<b><a href="https://hc.enioka.com/en/">enioka Haute Couture</a> is joining <a href="https://ev.kde.org/supporting-members.php ">KDE as a Patron</a> and will support the work of KDE e.V. through the corporate membership program.</b>

enioka Haute Couture is a software development house that creates complete and tailor-made solutions. enioka strives to return ownership of the software development and innovation to its customers. To that effect, it co-creates the software with its customers' teams to allow them to retain control of their projects in complex systems or organizations.

"We are excited to welcome enioka Haute Couture as a Patron of KDE. They truly understand what it means to empower people when creating software; something KDE cares deeply about", said Lydia Pintscher, President of KDE e.V.

"enioka Haute Couture is a company driven by its values and its <a href="https://hc.enioka.com/en/manifesto.html">manifesto</a>, in the same vein as KDE which we've chosen to support. We are grateful for the existence of all the communities creating Free Software. They are a real enabler in our mission to give back control of their development to our customers. It is time for us to express our gratitude by supporting a community like KDE", said Marc-Antoine Gouillart, CTO at enioka Haute Couture.

enioka Haute Couture will join KDE's other Patrons: Private Internet Access, The Qt Company, SUSE, Google, Blue Systems and Canonical to continue supporting Free Software and KDE development through the KDE e.V.
<!--break-->