---
title: "Lars Knoll, CTO at Qt and Keynote Speaker at Akademy 2019"
date:    2019-08-29
authors:
  - "Paul Brown"
slug:    lars-knoll-cto-qt-and-keynote-speaker-akademy-2019
comments:
  - subject: "Font size"
    date: 2019-08-30
    body: "<p>Look at his monitor and QT Creator in the included image.&nbsp; How small is that font?&nbsp; It's like 12px on a 4k unscaled display.&nbsp; I need to find a 4k monitor to try that.&nbsp; Maybe that's why I'm a horrible developer...my resolution is too low.</p>"
    author: "TacoTaco"
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/F94I7754_edited_cut_1500.jpg"><img src="/sites/dot.kde.org/files/F94I7754_edited_cut_1500.jpg" /></a><br /><figcaption>Lars Knoll at the Qt Company's headquarters.</figcaption></figure> 

<i>Lars Knoll has been working on <a href="https://www.qt.io/">Qt</a> for a long time. It is now 20 years since he joined Trolltech and even longer since he started developing crucial pieces of the KDE ecosystem. In that time, he went from theoretical physicist to software engineer to project leader, and survived the many roughs-and-tumbles Qt went through, including the event that nearly killed the project brought on by a certain company from Redmond.</i>

<i>Lars will be delivering the <a href="https://conf.kde.org/en/akademy2019/public/events/156">opening keynote</a> at this year's <a href="https://akademy.kde.org/2019">Akademy</a> and he kindly agreed to talk to us about the past, present and future of Qt.</i>

<span style="color:#f55;font-weight:bold">Paul Brown:</span> Hello Lars, how are you this morning? You seem very busy...

<span style="color:#55f;font-weight:bold">Lars Knoll:</span> Hi Paul, I'm doing good. There's more than enough to do, but I had a good weekend.

<span style="color:#f55;font-weight:bold">Paul:</span> Great! So tell me a bit about yourself. Looking over your résumé, you seem to have been in software production forever. Is this something you always wanted to do, since you were young?

<span style="color:#55f;font-weight:bold">Lars:</span> No, not really. Of course I played around with computers a bit when I was young. I had a Commodore 64 back then, but I mostly used it for games.

I actually went and studied physics when I went to University, and had quite a few years where I did very little with computers.

Things started picking up again during my masters and PhD thesis time in Heidelberg. I needed to use computers a lot to analyze the data that we collected during our experiments. We used Linux computers and Unix machines at that time, and I had to do quite a bit of my programming in Fortran. I really didn't like that language, so I started teaching myself C and some C++ to have a better language to work with. That was around 1996, 1997, if I remember correctly.

At that time, I also read about KDE for the first time, as a project to create a Desktop for Linux. That was something that also triggered my interest, and I started looking into it a bit and started subsequently to use it, as it was way easier than FVWM which is what I was using before.

<span style="color:#f55;font-weight:bold">Paul:</span> So... Wait a second... It was KDE that led you to Qt and not the other way around?

<figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;width:400px"><a href="/sites/dot.kde.org/files/Lars_Knoll_work-1500.jpg"><img src="/sites/dot.kde.org/files/Lars_Knoll_work-1500.jpg" /></a></figure> 

<span style="color:#55f;font-weight:bold">Lars:</span> Yes, I heard about KDE before Qt. I found creating a desktop for Linux interesting and started looking a bit into what was happening there.

The first thing I did was triggered by a need I had. I had my computer hooked up to a modem and wanted to use that as an answering machine for my phone. There were some command line based apps and tools available for Linux, but nothing graphical. So I started writing my own app, and I used the KDE/Qt libraries to do that. That's how I then slowly got involved with KDE. I started reading the newsgroups and mailing lists and fixed some bugs in different parts of KDE. Mainly in the apps I used myself, like KMail and the file manager for example.

<span style="color:#f55;font-weight:bold">Paul:</span> The famous "scratching your own itch" that gets most people started down the road to Free Software.

<span style="color:#55f;font-weight:bold">Lars:</span> Pretty much. At least at the beginning that's what got me started.

KDE had in it's first versions a small web browser based on a library called <I>khtmlw</I>. It was doing HTML 3.2 quite ok, but was getting old pretty fast. That's when HTML 4 was announced and websites started using more and more CSS and Javascript.

I still remember that around '98, Martin Jones, who was contributing a lot to KDE at that time and later went on to work for Trolltech, sent a mail to the <I>kfm-devel</I> mailing list saying that we needed to rewrite that engine. I didn't react at that time, but half a year later, I had some time and interest and sat down and read through the HTML and CSS specs and started writing some code trying to generate a DOM API from the IDL in the specs.

It worked out pretty well and I announced that on the mailing list almost exactly 20 years ago. That was basically the start of <a href="https://en.wikipedia.org/wiki/KHTML">khtml</a>, which became a HTML 4 compatible browser engine. As most people in the KDE community know, it got picked up by Apple in 2002 for their web browser and finally became the basis for the WebKit open source project.

In any case, I guess that got me lots of contacts with different people in the KDE community. I went to one of the first KDE meetings (they weren't yet called Akademy back then) and I met many community members in person for the first time there.

Matthias Ettrich, who founded KDE, went to work for Trolltech in '98 or '99. In Winter of '99, I got a call from Eirik Chambe-Eng, one of the founders of Trolltech. He invited me over for a job interview.

I had about 6 months of my PhD left, and was open to working abroad for some time after that. I went to Oslo in March and really liked the company and signed up there and then. I started working for them in July of 2000.

<span style="color:#f55;font-weight:bold">Paul:</span> Wow. Just to think there may be some of your code still floating around in Webkit...

<span style="color:#55f;font-weight:bold">Lars:</span> My copyright is certainly still there. So is the copyright of many of the old KDE people that worked on it with me.

<span style="color:#f55;font-weight:bold">Paul:</span> Have you contributed anything else to KDE since then?

<span style="color:#55f;font-weight:bold">Lars:</span> Not directly. I worked on khtml until around 2003 in KDE. After that I contributed some bug fixes and smaller things here and there until around 2005. But most of my work has involved contributing to Qt. I still felt very connected to KDE, but I also saw that the work I did on Qt was helping KDE a lot as well.

And then, in 2005, my daughter was born, and that also naturally left a lot less time to do coding in my spare time.

<span style="color:#f55;font-weight:bold">Paul:</span> Qt has gone through a lot and you have been on it for nearly all of it. What has made you stick with it?

<span style="color:#55f;font-weight:bold">Lars:</span> The 20 years with Qt have certainly sometimes felt a bit like a roller-coaster ride. We've gone through lots of ups and downs. Especially combining open source and a business has not always been easy and is a constant balancing act.

But there's been lots of things that made me stay around. One thing is the technology that I really love. When I first came in contact with Qt during the KDE years, I really fell in love with the framework. It is something that made it easy and fun to develop with. Developing that further and thinking about how to make the lives of other developers easier is something I find very gratifying.

The other part is certainly the people I've been and am working with. Lots of incredibly talented people work on Qt and it's fun and interesting to work with them, both in our office, but also remotely through the Qt project and open governance.

Another part that kept me around is that my work is very varied. In my current position, I do quite a lot of different jobs, from presentations to low level programming. In addition, with Qt being such a wide framework, there's always something new to look into and learn about.

<span style="color:#f55;font-weight:bold">Paul:</span> You have lived through a lot of, let's say... interesting moments: the buy out of Trolltech by Nokia, then the Nokia-Microsoft fracas, then Nokia went from being the most important mobile phones manufacturer to... well... whatever it is today, and Qt was then sold off again... What was the most tense moment?

<span style="color:#55f;font-weight:bold">Lars:</span> That's a difficult one to answer. There have been quite a few tense moments. If I have to pick something, I'd say the year from the Nokia-Microsoft announcement until we were sold to Digia was the most tense.

After the Microsoft announcement I was extremely close to quitting my job. I had worked very hard to try to get Nokia to move into the right direction. Just when we were starting to see the first successes, with Symbian phones having Qt on it, a working App Store and the N9 almost out, Nokia completely changed direction and threw it all into the trash can.

I only stayed because some people convinced me to stick around with Qt. We got sold out of Nokia a year later after the final project we were working on got canceled. I had another tense three months where our future was uncertain and I tried hard to keep at least some of the good engineers around.

<span style="color:#f55;font-weight:bold">Paul:</span> Ah yes! I remember reading about the exodus of engineers.

<figure style="float: right; padding: 1ex; margin: 1ex 0ex 1ex 1ex; border: 1px solid grey;width:400px"><a href="/sites/dot.kde.org/files/Lars_Knoll_full.jpg"><img src="/sites/dot.kde.org/files/Lars_Knoll_full.jpg" /></a></figure> 

<span style="color:#55f;font-weight:bold">Lars:</span> That's what happens when things are uncertain and there's a need for good engineers around. In Oslo for example, pretty much everybody immediately got lots of calls from different recruiters.

But yes, after we came out of Nokia, we had quite a few scars and lost many good people, but we were able to start building things up again.

Things weren't all that easy in the first Digia years either. Qt and the rest of Digia had very different business models. With all of Qt being available under LGPL v2 it wasn't even clear whether we could build a business on top of it. At least not whether that business could live from selling the product and not consultancy.

Building the business on doing consultancy was not an option we wanted to take, as it would have stalled product development to a very large degree, leading to a model that would not have been sustainable in the long term. 

<span style="color:#f55;font-weight:bold">Paul:</span> Fast forwarding to the present... so how are things today? What is your business model now?

<span style="color:#55f;font-weight:bold">Lars:</span> As with any business, things are never really easy. But we've been managing to grow the business over the years. But we still don't have as many engineers working on the product as we had in Nokia times. This also means that we need to grow further to make this sustainable.

The business model is basically the same as what we had in Trolltech times: we have a product that is dual licensed and available under (L)GPL and also a commercial license. The OSS licenses help us create, maintain and grow a large ecosystem and we earn a living from the people that need a commercial license for various reasons.

To help us get here, we've been changing the license of Qt to LGPLv3 (after negotiating a new agreement with KDE), and are introducing some of the new functionality we develop under GPL.

<span style="color:#f55;font-weight:bold">Paul:</span> Talking of which. What new stuff are you and your team working on now?

<span style="color:#55f;font-weight:bold">Lars:</span> Well, some of that is going to be the focus of my talk at Akademy. We are now looking into the next major version of Qt, Qt 6.

<span style="color:#f55;font-weight:bold">Paul:</span> There's a lot of buzz about that...

<span style="color:#55f;font-weight:bold">Lars:</span> I've recently posted a longer blog post about some of my ideas for it. To name a few here: We need to do quite a bit of work on our graphics stack. OpenGL isn't the only 3D API in town anymore, and we need to make sure Qt can work with Vulkan, Metal and Direct3D. In addition, many users want to easily integrate 3D content into their UIs and we need to prepare Qt for that.

QML has been really important for Qt's success over the last years, but we see that there are quite a few things we could have done better. We are now looking into developing the next generation of the QML language, one in which we can improve on some of those things.

I also would like to make it a lot easier to integrate UX designers into the workflow. This is something that we've been working on already for some time, but my goal is that Qt 6 helps us take the next steps there.

<span style="color:#f55;font-weight:bold">Paul:</span> What do you mean by that? Making Qt Creator friendlier for designers?

<span style="color:#55f;font-weight:bold">Lars:</span> Qt Design Studio is exactly that: it's Qt Creator made designer-friendly.

<span style="color:#f55;font-weight:bold">Paul:</span> Right.

<span style="color:#55f;font-weight:bold">Lars:</span> Other things for Qt 6 will be upgrading to C++17 and see what we can use from the new C++ standards, hopefully expose some of the concepts that we added in QML to C++ (especially the concept of bindings), and of course do some house cleaning.

<span style="color:#f55;font-weight:bold">Paul:</span> How do you think this will affect KDE?

<span style="color:#55f;font-weight:bold">Lars:</span> That's a good question, and something I hope to be able to discuss with people at Akademy. My goal for Qt 6 is that it be mostly source compatible with Qt 5.15 so KDE should be able to easily port over to it. But traditionally, major version changes in Qt have also triggered larger changes in KDE. Let's see if this happens again this time.

<span style="color:#f55;font-weight:bold">Paul:</span> Fascinating stuff, Lars. Thank you so much for taking time out of your busy schedule to talk to us!

<span style="color:#55f;font-weight:bold">Lars:</span> Of course! It's been a pleasure talking to you.

<span style="color:#f55;font-weight:bold">Paul:</span> We all look forward to your keynote. See you in <a href="https://akademy.kde.org/2019">Akademy 2019</a>!

<hr />

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href="http://devel-home.kde.org/~duffus/akademy/2018/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2018-groupphoto-teensie.jpg" /></a><br />Akademy 2018, Vienna</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->