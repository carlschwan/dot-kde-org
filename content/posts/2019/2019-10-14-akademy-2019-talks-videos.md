---
title: "Akademy 2019 Talks Videos"
date:    2019-10-14
authors:
  - "sealne"
slug:    akademy-2019-talks-videos
---
We now have the Akademy 2019 <a href="https://files.kde.org/akademy/2019/">videos</a> ready for you to enjoy, see the previous <a href="https://dot.kde.org/2019/09/10/akademy-2019-talks-heres-what-you-missed">summary of talks on the dot</a> for some inspiration on what to watch.  The <a href="https://conf.kde.org/en/akademy2019/public/schedule/1">talk schedule</a> has the full list

We had keynotes on <a href="https://conf.kde.org/en/akademy2019/public/events/159">Developers Italia and the New Guidelines: Let the Open Source Revolution Start!</a> by Leonardo Favario and <a href="https://conf.kde.org/en/akademy2019/public/events/156">Towards Qt 6</a> by Lars Knoll

We also got updates on KDE Community's <a href="https://conf.kde.org/en/akademy2019/public/events#kde%20goals">goals</a>

Another thing to check out are the previously announced <a href="https://files.kde.org/akademy/2019/bof_wrapups/">BoF wrapups</a> letting you know what went on during the week following the talks

<h2>Recommendations</h2>

Here are some talks recommended by attendees:

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:330px">
<video width="325" controls="1" poster="/sites/dot.kde.org/files/138-What_we_do_in_the_Promos.png"><source src="https://files.kde.org/akademy/2019/138-What_we_do_in_the_Promos.mp4" type="video/mp4">Your browser does not support the video tag. 
<a href="https://files.kde.org/akademy/2019/138-What_we_do_in_the_Promos.mp4">What we do in the Promos</a></video>
<figcaption><a href="https://conf.kde.org/en/akademy2019/public/events/138">What we do in the Promos</a>
<strong>Piyush</strong>: <em>i attended Paul's talk. It was really nice to have an insight on promo's day to day tasks and challenges!</em></figcaption>
</figure>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:330px">
<video width="325" controls="1" poster="/sites/dot.kde.org/files/103-Strengthen_Code_Review_Culture_rm_rf_Toxic_Behaviors.png"><source src="https://files.kde.org/akademy/2019/103-Strengthen_Code_Review_Culture_rm_rf_Toxic_Behaviors.mp4" type="video/mp4">Your browser does not support the video tag. <a href="https://files.kde.org/akademy/2019/103-Strengthen_Code_Review_Culture_rm_rf_Toxic_Behaviors.mp4">Strengthen Code Review Culture: rm -rf ‘Toxic Behaviors’</a></video>
<figcaption><a href="https://conf.kde.org/en/akademy2019/public/events/103">Strengthen Code Review Culture: rm -rf ‘Toxic Behaviors’</a>
<strong>Philip</strong>: <em>I liked the code review one</em>
<strong>Valorie</strong>: <em>and I agree, Aniketh's Code Review talk was excellent</em></figcaption>
</figure>

<br clear="all" />

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:330px;">
<video width="325" controls="1" poster="/sites/dot.kde.org/files/165-Software_Distribution_0.png"><source src="https://files.kde.org/akademy/2019/165-Software_Distribution.mp4" type="video/mp4">Your browser does not support the video tag. <a href="https://files.kde.org/akademy/2019/165-Software_Distribution.mp4"></a></video>
<figcaption>Software Distribution: <a href="https://conf.kde.org/en/akademy2019/public/events/165">lightning talks</a> & <a href="https://conf.kde.org/en/akademy2019/public/events/166">discussion</a>
<strong>Jon</strong>: <em>Software Distribution talk! (although I prefer my original name for it of Getting KDE Software to Users)</em></figcaption>
</figure>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:330px;">
<video width="325" controls="1" poster="/sites/dot.kde.org/files/102-Taking_KDE_to_the_skies_Making_the_drone_ground_control_Kirogi.png"><source src="https://files.kde.org/akademy/2019/102-Taking_KDE_to_the_skies_Making_the_drone_ground_control_Kirogi.mp4" type="video/mp4">
Your browser does not support the video tag. <a href="https://files.kde.org/akademy/2019/102-Taking_KDE_to_the_skies_Making_the_drone_ground_control_Kirogi.mp4">Taking KDE to the skies: Making the drone ground control Kirogi</a></video>
<figcaption><a href="https://conf.kde.org/en/akademy2019/public/events/102">Taking KDE to the skies: Making the drone ground control Kirogi</a>
<strong>Ivana</strong>: <em>I nominate Eike's talk about Kirogi. It was such a cool talk that told the story of developing an app in a way that even non-devs could understand, and I think it really showcased how KDE is still going strong and taking the lead in the innovation game</em>
<strong>Hannah</strong>: <em>The talk was horrible.... It made me want to buy a drone</em></figcaption>
</figure>

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:330px">
<video width="325" controls="1" poster="/sites/dot.kde.org/files/141-Mycroft_on_Plasma_Automobile_Demo.png"><source src="https://files.kde.org/akademy/2019/141-Mycroft_on_Plasma_Automobile_Demo.mp4" type="video/mp4">Your browser does not support the video tag. <a href="https://files.kde.org/akademy/2019/141-Mycroft_on_Plasma_Automobile_Demo.mp4">Mycroft on Plasma Automobile Demo</a></video>
<figcaption><a href="https://conf.kde.org/en/akademy2019/public/events/141">Mycroft on Plasma Automobile Demo</a>
<strong>Bhushan</strong>: <em>Automative demo one</em></figcaption>
</figure>
<br clear="all" />

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href="https://devel-home.kde.org/~duffus/akademy/2019/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2019-groupphoto_1500.jpg" /></a><br />Akademy 2019, Milan</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->