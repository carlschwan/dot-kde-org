---
title: "Translation Workshop in Indonesia this Weekend"
date:    2019-11-21
authors:
  - "jriddell"
slug:    translation-workshop-indonesia-weekend
---
<img src="/sites/dot.kde.org/files/indonesia.png" width="732" height="336" />

The <a href="https://kde-indonesia.id/">KDE Indonesia Community</a> will once again hold a Kopdar (local term for BoF). This meeting is the second meeting after the successful meeting in 2018. The activity will be held this weekend with talks and activities about translating KDE software into Indonesian. The main event is for KDE fans in particular and Linux in general to collaborate in KDE translation.

The event will be held on:
<b>Day:</b> Saturday, 23 November 2019
<b>Time:</b> 19.00 (UTC + 7)
<b>Venue:</b> Midtrans Office Jl. Gandok Baru No.46, Sleman, Yogyakarta
<b>Speaker:</b> Wantoyek
<b>Topic:</b> The First Step to Becoming a KDE Translator

The purpose of this event is to invite KDE activists to participate in contributing to the community, especially as translators. The KDE Indonesia community also opens opportunities to donate activities for anyone who wants to support this activity, please contact Rifky Affand (rifki.affandi3@gmail.com). See you in DIY Yogyakarta, KDE lovers!

To register go to <a href="https://s.klas.or.id/kopdar-kde">the registration form</a> and join the <a href="https://t.me/kdeid">KDE Indonesia Telegram channel</a>.
<!--break-->
