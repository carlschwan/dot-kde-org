---
title: "Akademy 2019 Monday BoF Wrapup"
date:    2019-09-09
authors:
  - "sealne"
slug:    akademy-2019-monday-bof-wrapup
---
<a href="https://community.kde.org/Akademy/2019/Monday">Monday</a> was the first day of Akademy BoFs, group sessions and hacking. There is a wrapup session at the end of the day so that what happened in the different rooms can be shared with everyone including those not present.

Watch Monday's wrapup session in the video below

<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2019/bof_wrapups/monday.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2019/bof_wrapups/monday.mp4">Monday BoF Wrapup video</a>
</video> 


<!--break-->