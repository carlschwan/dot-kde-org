---
title: "KDE is adding Matrix to its instant messaging infrastructure"
date:    2019-02-20
authors:
  - "Paul Brown"
slug:    kde-adding-matrix-its-im-framework
comments:
  - subject: "Official Matrix client for KDE"
    date: 2019-02-20
    body: "<p>Hi! This is a great news!</p><p>But I hope we will see an official KDE matrix client in the near future! Thanks. :)</p>"
    author: "Jane Mios"
  - subject: "I very much applaud this"
    date: 2019-02-21
    body: "<p>I very much applaud this announcement, and I hope to see Matrix becoming the central means of communication for KDE projects before too long :)</p>"
    author: "Unremarkable"
  - subject: "You are right. These Qt"
    date: 2019-02-21
    body: "<p>You are right. These Qt-clients are really bad. They supports only some little part of the matrix features and always was too buggy. <span class=\"tlid-translation translation\"><span title=\"\">It is impossible to compare them with the official Riot client. </span></span>We need stable KDE client out of the box - in Kubuntu, Neon e.t.c.</p>"
    author: "Dmitri Panov"
  - subject: "Great decision. Thank you for"
    date: 2019-02-21
    body: "<p>Great decision. Thank you for bringing a lot more people and hopefully support to Matrix. If I do use a DE it's usually the Kool one.</p><p>I hope to see many closed down chat platforms (the list is endless) die and bow to matrix, regardless of the client and what it's goals are, I hope they use matrix spec and it's federation to communicate.</p>"
    author: "John"
  - subject: "It would be nice! KDE"
    date: 2019-02-23
    body: "<p>It would be nice! KDE ecosystem need IM communication software.</p>"
    author: "Stiven"
  - subject: "I agree with Jane, it would"
    date: 2019-02-23
    body: "<p>I agree with Jane, it would be nice to see native kde app!</p>"
    author: "Marvin T."
  - subject: "Thanks for awesome article! I"
    date: 2019-02-28
    body: "<p>Thanks for awesome article! <span class=\"tlid-translation translation\"><span title=\"\">I have never used Matrix before. But I wish like to try it. </span></span>Please <span class=\"tlid-translation translation\"><span title=\"\">add me to the list of people who want a KDE client. Kopete and Telepathy is dead and never was a something solid and secure. Matrix should be. KDE client too.<br /></span></span></p>"
    author: "Lamar"
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/matrixdotstory01.png" alt="" width="800" />

<b>KDE is adopting non-proprietary and decentralized instant messaging services and is now <a href="https://community.kde.org/Matrix">running its own community-managed instance of Matrix</a>.</b>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/matrixdotstory02.jpg"><img src="/sites/dot.kde.org/files/matrixdotstory02.jpg" /></a><figcaption>Matrix works fine on mobile as well.</figcaption></figure>

KDE has been looking for a better way of chatting and live-sharing information for several years now. IRC has been a good solution for a long time, but our channels are currently on servers KDE cannot control. It also lacks features users have come to expect from more modern IM services. Other alternatives, such as Telegram, Slack and Discord, although feature-rich, are centralized and built around closed-source technologies and offer even less control than IRC. This flies in the face of KDE's principles that require we use and support technologies based on Free software.

However, our search for a better solution has finally come to an end: as of today we are officially using <a href="https://matrix.org/blog/2019/02/20/welcome-to-matrix-kde/">Matrix</a> for collaboration within KDE! Matrix is an open protocol and network for decentralised communication, backed by an open standard and open source reference implementations for servers, clients, client SDKs, bridges, bots and more. It provides all the features you’d expect from a modern chat system: infinite scrollback, file transfer, typing notifications, read receipts, presence, search, push notifications, stickers, VoIP calling and conferencing, etc. It even provides end-to-end encryption (based on Signal’s double ratchet algorithm) for when you want some privacy.

All the existing rooms on Matrix (and their counterparts on IRC, Telegram and elsewhere) continue to exist. The new service provides a dedicated server for KDE users to access them using names like #kde:kde.org.

For more information <a href="https://community.kde.org/Matrix">go visit our wiki page which contains details and instructions on how to get started</a>.

You can also try KDE's Matrix service right now by <a href="https://webchat.kde.org">checking in to KDE's webchat</a> or by <a href="https://matrix.org/docs/projects/clients-matrix">installing a Matrix client</a> like Riot and connecting to the kde.modular.im server!
<!--break-->