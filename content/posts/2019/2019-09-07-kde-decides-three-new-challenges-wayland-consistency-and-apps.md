---
title: "KDE Decides the Three New Challenges: Wayland, Consistency, and Apps"
date:    2019-09-07
authors:
  - "Paul Brown"
slug:    kde-decides-three-new-challenges-wayland-consistency-and-apps
---
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/LydiaEikeIvanNeo.jpg"><img src="/sites/dot.kde.org/files/LydiaEikeIvanNeo.jpg" width="800" /></a><br /><figcaption>Lydia Pintscher, Eike Hein, Ivan Cukic  and Neofytos Kolokotronis discuss the results of the past goals.</figcaption></figure>
</div>

<b><i>The KDE Community has spoken! On the first day of Akademy 2019, Lydia Pintscher, President of KDE e.V., announced the three new goals we will prioritize over the next 2 years.</b></i>

The goals were selected by community vote from a dozen proposals, all created by community members. Read on to learn more details about each of the new goals.

<h2>Finalize the transition to Wayland and embrace the future of desktop</h2>

Despite its many merits, the X server has become very long in the tooth, and Wayland is poised to become a more modern and dependable alternative. However, KDE's software is still quite far off from being completely implemented on the newer protocol.

"As technology and the needs of modern computer users advance, X server has been proven less and less capable to keep up", says Fanis Bampaloukas, author of the first proposal. "I propose to make our goal to migrate the core of the Plasma desktop, and make X server an optional compile and runtime dependency".

To achieve this goal, Fanis says KDE will have to fix major breakages and implement missing features.

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/JonandNico.jpg"><img src="/sites/dot.kde.org/files/JonandNico.jpg" width="800" /></a><br /><figcaption>Jonathan Riddell and Niccolò Venerandi explain their ideas for new KDE goals.</figcaption></figure>
</div>

<h2>Improve consistency across the board</h2>

As KDE relies on "volunteers, each with different ideas and "scratching their own itches", there is often a lack of organization and consistency within the app ecosystem" says Niccolò Venerandi, author of the second goal proposal. Niccolò points out that inconsistencies are not only found in the design of applications, but also in their features. Tabs, for example, are implemented differently on Falkon, Konsole, Dolphin and Kate, making them confusing for users and difficult to fix for developers.

To solve inconsistencies, Niccolò suggests the unification of behavior in app elements (such as sidebars, hamburger menus and tabs), ending the fragmentation of apps with overlapping features (like having several multimedia players), and laying down criteria for hosting application websites, among many other things.

<h2>KDE is all about the apps</h2>

"KDE has over 200 applications and countless add-ons, plugins and Plasmoids" says Jonathan Riddell, author of the third proposal. "But much of the support we offer has fallen short; until recently there wasn't even an up-to-date website listing them all".

The importance of KDE's app ecosystem cannot be stressed enough. Often it is the gateway to other apps, the Plasma desktop, and to becoming a member of the KDE community. In his proposal, Jonathan recommends modernizing the platforms through which KDE developers communicate with users, improving the way each app is packaged, and continuing with reworking to the documentation and metadata supplied with the apps.

<h2>Past Goals</h2>

The previous three goals were chosen by the community back in 2017. 

All goals have had a net positive impact on the community. The <B><a href="http://neofytosk.com/post/streamlined-onboarding-how-far-has-kde-come/">Onboarding initiative</a></B>, for example, has led to a substantial increase in the number of new contributors - especially young students - that has brought a renewed vigor to the community.

The <B><a href="https://dot.kde.org/2019/06/05/kde-privacy-sprint-2019-edition">Privacy project</a></B> has encouraged developers to implement measures for keeping users secure and their data private.

Finally, the <B><a href="https://pointieststick.com/2019/06/05/kde-usability-and-productivity-are-we-there-yet/">Usability project</a></B> has removed hundreds of design flaws and niggling papercuts. As a result, using Plasma and KDE applications in general is now a much more enjoyable experience.

KDE goals give the community focus and solve problems that otherwise may not be fully addressed. The same way 2017 proposals helped make KDE a better community with better software, this year's goals will help push the project towards a brighter future. 
<hr />

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href="http://devel-home.kde.org/~duffus/akademy/2018/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/akademy2018-groupphoto-teensie.jpg" /></a><br />Akademy 2018, Vienna</figure></p>

For most of the year, KDE - one of the largest free and open software communities in the world - works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">the Akademy Team</a>.
<!--break-->