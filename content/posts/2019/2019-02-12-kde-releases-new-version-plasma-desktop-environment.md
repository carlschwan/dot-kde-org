---
title: "KDE releases a new version of the Plasma desktop environment"
date:    2019-02-12
authors:
  - "Paul Brown"
slug:    kde-releases-new-version-plasma-desktop-environment
---
<a href="https://www.kde.org/announcements/plasma-5.15.0.php"><img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/plasma515.jpg" alt="" width="800" /></a>

<B><a href="https://www.kde.org/announcements/plasma-5.15.0.php">Say hello to Plasma 5.15</a>, the newest version of KDE's acclaimed desktop environment.</B>

This February release of KDE Plasma comes with a wide range of new features and improvements. The main focus of developers has been stamping out all minor problems and papercuts of the desktop, aiming to make Plasma smoother and easier to use.

Plasma's configuration interfaces have been redesigned, expanded and clarified to cover more user cases and make it simpler to adapt Plasma to everybody's needs. Plasma has also improved the integration of non-native applications, so Firefox, for example, can now optionally use native KDE open/save dialogs. Likewise, GTK and GNOME apps now respect the global scale factor used by high-DPI screens.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:250px"><a href="/sites/dot.kde.org/files/discover-release-upgrade.png"><img src="/sites/dot.kde.org/files/discover-release-upgrade.png" /></a><figcaption>Updating your system is now easier and more reliable with the new and improved Discover.</figcaption></figure>

Developers have also been hard at work on Discover, Plasma's built-in software manager. Options for upgrading your distribution are now included in Discover's Update Notifier widget, which will also display a "<I>Restart</I>" button if a restart is recommended when updating is done. Talking of updates, it is now possible to uncheck and re-check all available updates to make it easier to pick and choose the ones you want to apply. Another improvement is that repository management in Discover is now more practical and usable, especially for Ubuntu-based distros.

We have also solved problems with text readability and icon clarity. KDE designers have improved a variety of Breeze device and preference icons, including the multimedia icons and all icons that depict a stylized version of a Plasma wallpaper. The <I>Places</I>, <I>Vault</I> and Python bytecode files all have redesigned icons to make them easier to identify.

There are literally hundreds more improvements and tweaks included in this release, all implemented to make your life as a Plasma user much more enjoyable. Read the <a href="https://www.kde.org/announcements/plasma-5.15.0.php">official announcement</a> and check out the <a href="https://www.kde.org/announcements/plasma-5.14.5-5.15.0-changelog.php">Plasma 5.15 changelog</a> for more details on this new version of the Plasma desktop.
<!--break-->