---
title: "Google Summer of Code 2019"
date:    2019-02-27
authors:
  - "Paul Brown"
slug:    google-summer-code-2019
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/GSoC2019_02.jpg" alt="GSoC" width="800" />

<b>The KDE Community is happy to announce that we have been selected to participate in <a href="https://summerofcode.withgoogle.com/organizations/">Google Summer of Code</a>. This will be our our 14th year of mentoring students.</b>

Attention students: If you are a student who would like to work with KDE this summer you can apply to SoC, find more info on <a href="https://community.kde.org/GSoC ">the KDE GSoC wiki page</a>. Please note that your project proposal will need to link to some commits to the KDE codebase, so get started now fixing some bugs! If you are wondering what you can work on, also check out our <a href="https://community.kde.org/GSoC/2019/Ideas">ideas page</a>.

Come and talk to the team on Matrix at <a href="https://webchat.kde.org/#/room/#freenode_#kde-soc:matrix.org">kde-soc:kde.org</a>, on IRC in the <a href="irc://#kde-soc@freenode.net">#kde-soc</a> channel or join the <a href="https://mail.kde.org/mailman/listinfo/kde-soc">student mailing list</a>.

<p align="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/S6IP_6HG2QE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
<!--break-->

