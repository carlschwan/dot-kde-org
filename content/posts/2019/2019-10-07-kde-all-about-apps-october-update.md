---
title: "KDE is All About the Apps: October Update"
date:    2019-10-07
authors:
  - "jriddell"
slug:    kde-all-about-apps-october-update
comments:
  - subject: "Amarok!"
    date: 2019-10-10
    body: "<p>How close is Amarok to a KF5/Qt5 version?</p><p>I really hope such a beautiful piece of software is not destroyed. There is no capable replacement for it :-(</p>"
    author: "Syam"
  - subject: "Amarok"
    date: 2019-10-12
    body: "<p>Just wanted to write a comment regarding Amarok - I realise it hasn't had an official release but I've been using the KF5 port of Amarok (git versions) without major problems for a while now. I'm surprised to hear it's \"become deprecated\" as there still seem to be commits happening. As with any software there are bugs (eg. the Wikipedia and lyrics integrations don't currently work for me) but the collections/search/cueing/playback seems solid to me, which covers most of my usecases.</p><p>And a thanks to anyone developing Amarok - I've tried plenty of other music players but still prefer to use Amarok.</p>"
    author: "os"
  - subject: "Thanks for the awesome work!"
    date: 2019-10-13
    body: "<p>Thanks for the awesome work! KDE is the best! :)</p>"
    author: "Dino"
---
<strong>KDE is all about the Apps!</strong>

We are a community of thousands of contributors who make hundreds of Apps using collaborative open source methods. Our apps run on Linux with Plasma, of course, but also fit in well with GNOME, Enlightenment, XFCE, or any other desktop you happen to be using. Many of our apps run on Windows, Android and macOS. 

A new <a href="https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps">goal for the KDE community</a> is to push how we are All About the Apps. We will be highlighting our best software and promoting it to increase its adoption outside the circle of current KDE fans (who we still love very much!). This is a monthly update of what's new in our apps. If you'd like to help out with this community goal, take a look at the <a href="https://phabricator.kde.org/project/view/313/">All About the Apps workboard,</a> and join us in our <a href="https://webchat.kde.org/#/room/#kde-all-about-apps:kde.org">Matrix chat channel</a>.

<h3>App Updates</h3>

The elite painting app <strong>Krita</strong> received a <a href="https://krita.org/en/item/krita-4-2-7-released/">monthly bugfix release, 4.2.7</a>. The developers have improved the layout and functionality of the color selection dialog, and made it possible to save group layers to file layers even if they are empty. The sort order of images imported as frames was fixed, a bunch of crashes removed, and dozens of other bugs tidied up. 

<div style="margin: 0em 0em 2em 0em"><p align="center"><iframe title="Improve your Sketches with Krita" src="https://www.youtube.com/embed/HtBzzXulIr0?feature=oembed" frameborder="0" width="560" height="315"></iframe></p></div>

To celebrate, the Krita team also made a video with artist Ramon Miranda that offers some advice for improving your sketches. Krita is available in your favorite Linux distribution, for Windows, macOS, as a Linux AppImage, on <a href="https://flathub.org/apps/details/org.kde.krita">Flathub</a>, and in the <a href="https://snapcraft.io/krita">Snap</a> store.

<strong>KMyMoney</strong>, the app for managing your finances, <a href="https://kmymoney.org/release-notes.php">also got a new release - 5.0.7</a>. This release introduces updates required for the new regulations of the <a href="https://en.wikipedia.org/wiki/Payment_Services_Directive">Payment Services Directive</a>, which affects the online capabilities for German bank users.

<img class="alignnone size-full wp-image-135" src="https://cdn.kde.org/screenshots/kmymoney/kmymoney.png" alt="" width="800" />

To make KMyMoney compatible with those regulations (especially the <a href="https://en.wikipedia.org/wiki/Strong_customer_authentication">strong customer authentication</a> part), developers had to adapt it to updated APIs of the <a href="https://www.aquamaniac.de/rdm/">Gwenhywfar and AqBanking libraries</a> which provide the banking protocol implementations.

Coming from KDE and used by many of us, the distributed compiler cluster <a href="https://github.com/icecc/icecream">Icecream</a> and <a href="https://github.com/icecc/icemon/releases">Icecream Monitor</a> have been updated. The new release improves Objective C and C++ support, removes hardcoded compiler paths, and fixes job preloading to again allow sending one extra job to a fully busy node. In the monitor app several new ice cream flavors have also been added, we're not quite sure what this means but it sounds delicious.

In the last month, <strong>Latte Dock</strong> (panel for the Plasma desktop) had two new releases, making improvements to its new Win Indicator look. 

<div style="margin: 0em 0em 2em 0em"><p align="center"><iframe src="https://www.youtube.com/embed/MSfOsboLies" frameborder="0" width="560" height="315"></iframe></p></div>

<strong>KDevelop</strong>, the discerning coder's IDE, <a href="https://www.kdevelop.org/news/kdevelop-542-released">published a bugfix release - 5.4.2</a>. You can get it from your Linux distribution or as an AppImage, and you can also compile versions for Windows and macOS.

<strong>RSIBreak</strong>, the app that helps you prevent damage to your wrists got a new release versioned 0.12.11.

<div style="margin: 0em 0em 2em 0em"><p align="center"><img class="alignnone size-full wp-image-135" src="https://kde.org/applications//thumbnails/org.kde.rsibreak/rsibreak.png" alt="" width="600" /></p></div>

Photo management and editing app <a href="https://www.digikam.org/news/2019-09-08-6.3.0_release_announcement/">digiKam released the version 6.3</a>. The highlight of the new release is the <a href="https://pixls.us/blog/2018/02/g-mic-2-2/">G'Mic plugin</a>. 
G'Mic is the image processing library with over 950 different filters, so you can make all your photos truly beautiful. digiKam can be installed from your Linux distro, AppImage bundles, macOS package, and Windows 32/64-bit installers.

<div style="margin: 0em 0em 2em 0em"><p align="center"><img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/digikam-gmic.png" alt="" width="600" /></p></div>

Telescope and astronomy app <strong>KStars</strong> also had a <a href="http://knro.blogspot.com/2019/09/kstars-v336-is-released.html">new release, versioned 3.3.6</a>. The KStars Live Video window can now show debayer frames in real-time, making it possible to create color video streams. 

<div style="margin: 0em 0em 2em 0em"><p align="center"><img class="alignnone size-full wp-image-135" src="http://3.bp.blogspot.com/-C5VxPnK8CHQ/XXZK5JSGyuI/AAAAAAAAGO8/8NNDFAYUmLM7vu0ZfmIOgHYqFib65onEQCK4BGAYYCw/s1600/Screenshot_20190909_155028.jpg" alt="" width="600" /></p></div>

The weather data can be directly displayed in the Observatory Module, and the user interface has been improved in a number of ways. As one of the most feature-rich free astronomy apps, KStars caters to a wide variety of use cases, so you will surely find tools that are useful to you regardless of your level of experience. KStars is available pretty much everywhere - as a Windows installer, macOS installer, Android app, Snap package, and in your Linux distribution.

<h3>Bug Fixes</h3>

We are continually improving our apps, so plenty of bug fixes have been made. Here are some highlights.

<ul>

<li>Our document viewer <strong>Okular</strong> gained support for HighDPI screens. This one-line fix to add automatic scaling based on the pixel density of the monitor will make viewing documents on fancy monitors so much better.</li>

<li>The advanced text editor <strong>Kate</strong> was similarly updated to work with HiDPI screens throughout.</li>

<li>The chess game <strong>Knights</strong> had a one-line fix in version 19.08.2. Thanks to the fix, you can now start a game when the second player is a computer engine again.</li>

<li>Video editor <strong>Kdenlive</strong> fixed screengrabs in Linux to eliminate crashes, and in Windows to correctly grab the audio.</li>

<li>CD burner app <strong>K3b</strong> fixed a crash where it couldn't find the supporting command-line tool mkisofs.</li>

</ul>

<h3>Supporting Bits</h3>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 400px"><a href="/sites/dot.kde.org/files/breeze-icons-oct.png#overlay-context=2019/10/07/kde-all-about-apps-october"><img src="/sites/dot.kde.org/files/breeze-icons-oct.png#overlay-context=2019/10/07/kde-all-about-apps-october"/></a></figure>

Libraries and artwork support our apps to make our software work beautifully.

The Breeze icon theme got new icons for activities, trash, batteries, QR codes, and more. Libical, which is used by Kontact to talk to iCalendar protocols and data formats, had a <a href="https://github.com/libical/libical/releases/tag/v3.0.6">bugfix release (3.0.6)</a>.

<a href="https://techbase.kde.org/Projects/Snorenotify">Snorenotify</a> is a notification framework supporting Linux, Windows and macOS. Snoretoast is a command-line application used within Snorenotify for Windows Toast notifications. It is also used in Quassel and Tomahawk, and the good news is that it got a <a href="https://download.kde.org/stable/snoretoast/">new release this month (0.7.0)</a>.

<h3>New in App Stores</h3>

Our software is increasingly available directly through app stores. To celebrate and highlight this (and to help you find them more easily!), this month we added Windows Store links to the <a href="https://kde.org/applications/">KDE Applications web page</a>.

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/windows.png" alt="" width="800" />

More KDE applications found their way to the Windows Store:

<ul>

<li><a href="https://www.microsoft.com/store/apps/9N41MSQ1WNM8">Okular</a> - more than a reader</li>
<li><a href="https://www.microsoft.com/store/apps/9NWMW7BB59HW">Kate</a> - Advanced Text Editor</li>
<li><a href="https://www.microsoft.com/store/apps/9PFXCD722M2C">Filelight</a> - Filetree Usage Viewer</li></ul>

<h3>Welcoming New Projects</h3>

New projects are started in the KDE community all the time. When those projects are ready for wider use, they go through a process called "KDE review", where other KDE contributors will check them for code quality, features, licensing, and how well they work on different platforms. Last but not least, we decide whether we are happy to give it the KDE stamp of approval.

In KDE review this month is <strong>Ruqola</strong>, a chat app which talks on the Rocket Chat network and uses the Kirigami UI framework. For the more technically-inclined, <strong>Elf-Inspector</strong> is an app providing tools for inspecting, analyzing, and optimizing ELF files (the executable file format used on Linux).

<h3>Saying Goodbye</h3>

Sometimes, apps are left behind when their code does not keep up with the rest of the world. 

This month, a new version of our multimedia library Phonon was released. In this version, we removed Qt4 support - sensible enough, as Qt4 hasn't been supported since 2015. As a result, the music player app <strong>Amarok</strong> has become deprecated (at least for now). Don't lose hope, though: the Qt5 port is progressing, but it's not there yet. 

The web browser <strong>Rekonq</strong> was marked as unmaintained, meaning it's unlikely to ever come back. However, the work carries on in <a href="https://www.falkon.org/">Falkon</a>, so make sure to check out and support the project if you are interested in lightweight web browsers. Also considered unmaintained is the bootup configuration tool systemd-kcm.

<hr>

Enjoy your apps from KDE, and stay tuned for more updates!
<!--break-->
