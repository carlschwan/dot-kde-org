---
title: "Announcing Our Google Summer of Code 2019 Students"
date:    2019-06-09
authors:
  - "valoriez"
slug:    announcing-our-google-summer-code-2019-students
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/kde-gsoc-header.jpg" width="800" />

<strong>The KDE Community welcomes our <a href="https://summerofcode.withgoogle.com/">Google Summer of Code</a> students for 2019!</strong>

These students will be working with our development teams throughout the summer, and many of them will join us this September at <a href="https://akademy.kde.org/2019">Akademy</a>, our annual community meeting.

<a href="https://krita.org/">Krita</a> will have four students this year: Alberto Flores will work with the SVG pipe/animated brush, Kuntal M. is porting the magnetic lasso, Sharaf Zaman will port Krita to Android, and Tusooa Windy will bring a better undo/redo for Krita.

<a href="https://www.digikam.org/">digiKam</a> will mentor three students this year. Thanh Trung Dinh will bring AI Face Recognition with the OpenCV DNN module to digiKam, Igor Antropov will improve the Faces Management workflow, and Ahmed Fathy will make a zoomable and resizable brush for Healing Clone Tool.

<a href="https://labplot.kde.org/">Labplot</a> gets attention from two students in 2019. While Devanshu Agarwal will provide statistical analysis for Labplot, Ferencz Kovács will work on the support for importing educational data sets available on the Internet.

Another two students - Piyush Aggarwal and Weixuan Xiazo - will work on <a href="https://community.kde.org/KDEConnect">KDE Connect</a>. Their projects are quite exciting: Piyush will be porting KDE Connect to Windows, while Weixuan brings KDEconnect to MacOS.

Akshay Kumar will bring <a href="https://gcompris.net/index-en.html">Gcompris</a> one step closer to version 1.0, and Akhil K Gangadharan will revamp the Titler tool for <a href="https://kdenlive.org">Kdenlive</a>. Prasenjit Kumar Shaw will make data sync for <a href="https://www.falkon.org/">Falkon</a> a thing, and Rituka Patwal will bring Nextcloud integration to <a href="https://www.plasma-mobile.org/">Plasma Mobile</a>.

João Netto will improve JavaScript support on <a href="https://okular.kde.org/">Okular</a>, and Karina Pereira Passos will improve <a href="https://userbase.kde.org/Khipu">Khipu</a> and <a href="https://api.kde.org/4.x-api/kdeedu-apidocs/analitza/html/index.html">Analitza</a>. Nikita Sirgienko will implement the import/export of Jupyter notebooks in <a href="https://edu.kde.org/cantor/">Cantor</a>.

Atul Bisht will create a barcode scanning plugin in <a href="https://api.kde.org/frameworks/purpose/html/index.html">Purpose</a>. Filip Fila will work on ensuring consistency between the SDDM login manager and the <a href="https://kde.org/plasma-desktop">Plasma</a> desktop, and SonGeon will focus on integrating kmarkdown-qtview with WYSIWYG markdown editor for KDE as a whole.

<a href="https://neon.kde.org/">KDE neon</a> will get a KDE ISO image writer courtesy of Farid Boudedja, while Caio Tonetti will make an improved graph theory IDE for <a href="https://edu.kde.org/rocs/">Rocs</a>. Alexander Saoutkin will be polishing <a href="https://techbase.kde.org/Projects/KioFuse">KIOFuse</a>, and Shubham will port authentication to Polkit-qt for <a href="https://kde.org/applications/system/kdepartitionmanager/">KDE Partition Manager</a>.

We look forward to our students' contributions, and we're excited to share their progress with the rest of the world. As Google Summer of Code moves forward, you'll be able to read detailed reports from our students, and find out how their work will impact your favorite KDE software. Stay tuned, and wish them luck!
<!--break-->