---
title: "Akademy 2012 Call for Volunteers"
date:    2012-06-02
authors:
  - "kallecarl"
slug:    akademy-2012-call-volunteers
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/HeaderScreenShotv3_1.png" /></div>Are you attending Akademy 2012 in Tallinn? Are you interested in making the conference rock for attendees? Helping the Akademy Team organize everything? Do you want an exclusive free Akademy 2012 Team t-shirt?
<!--break-->
Then please sign up as a volunteer. We need a lot of motivated people and a variety of skills for all kinds of tasks.

More details and sign up instructions are available at the Akademy 2012 site on the <a href="http://akademy.kde.org/call-volunteers">Call for Volunteers Page</a>.