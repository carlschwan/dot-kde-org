---
title: "Randa Makes a Difference"
date:    2012-09-12
authors:
  - "kallecarl"
slug:    randa-makes-difference
comments:
  - subject: "Great idea!"
    date: 2012-09-28
    body: "Everything that can be done to facilitate the creation of new widgets is a good idea. I have wanted to try PlasMate."
    author: "Samuel Maudo"
---
21 year old twins from Greece are two of the dedicated people headed for <a href="http://dot.kde.org/2012/08/13/support-2012-kde-randa-meetings-inspired-and-intense">Randa, Switzerland to work on KDE software</a>. Giorgos and Antonis Tsiapaliokas are working on <a href="http://techbase.kde.org/Projects/Plasma/PlasMate">Plasmate</a>, the KDE Software Development Kit for Plasma Workspaces. Giorgos wrote about his KDE background and what is motivating him to be part of the intensive coding in Randa.

<h3>Giorgos:</h3>
Within KDE, I’ve mostly worked with Plasma. Lately the Plasmate SDK has been my focus. It was also my GSoC project. The goal right now is to make Plasmate ready for release. If all goes well, version 1.0 will be released by November 1. Plasmate will make it possible for more people to have the satisfaction of contributing to KDE, especially those who are just getting started.

In Randa, I will be working with the rest of the Plasma team on Plasmate’s release. This will be the first release, so it has to be perfect in order to attract more people to it. I also want to help with the development of libplasma2, the most vital library for the Plasma Workspaces and an important library for the future of Plasma Workspaces.
 
I learned about free software from my brother Antonis, who is also going to Randa. He was involved with free software before me and helped me in my first steps. When I used Linux and KDE for the first time, I found it very usable and cool. So I asked Antonis, “Who has written this cool software?” and he told me “normal people”. 

That was the main reason why I decided to contribute in free software. Normal people like me were free to improve their favorite applications, but it wasn't just the freedom that motivated me. It was also the fact that those people left a legacy for me; it can’t be described in words what this meant to me. Back then I thought that KDE software couldn't be improved. And then I started to contribute in Plasma. 

In the beginning I thought that it would be hard, but from the first moment that I joined IRC, I had all those cool people from Plasma who wanted to help me. In about a month, the Plasma team was like a F/OSS family for me. So from that moment I just couldn't leave Plasma. 
 
I decided to go to Randa because I share responsibility for Plasmate and I want it to be perfect. When you are involved in KDE, over time you get more rights and more responsibilities. It is a meritocracy. So I wanted to take responsibility, just like others before me have done. Their work is why in the first place I had a cool desktop environment, but it’s also how I feel about KDE. Randa is not just a place where we will make great coding progress. It’s also a place where all those people will come to life. This is very exciting. 

The economic situation here in Greece is very bad. This makes it hard for people to contribute to free software. First of all, people my age have to finish their college as soon as possible because they have to find work. So having a hobby like free software development and attracting other people to it is very difficult. Also attending a free software event outside of Greece is almost impossible, because financial resources are scarce. The economic situation here makes it hard for free software developers. I am very grateful for the financial help that makes it possible for me to be part of Randa.

<h3>Make a Difference</h3>
KDE is one of the leading free software projects in the world, thanks in large part to skilled, committed developers like Giorgos and the others who will be in Randa. For 7 intense days starting on September 21st, KDE software developers will be working to benefit all KDE users. You can make a difference by contributing financially. The <a href="http://pledgie.com/campaigns/18045">Randa fundraising effort</a> has already generated over € 6,000; the goal of € 10,000 is easily within reach. There are 10 days remaining to raise money for the Randa sprints. Please donate if you can. And share the responsibility and satisfaction.