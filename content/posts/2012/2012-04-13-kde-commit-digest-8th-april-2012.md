---
title: "KDE Commit-Digest for 8th April 2012"
date:    2012-04-13
authors:
  - "mrybczyn"
slug:    kde-commit-digest-8th-april-2012
---
In <a href="http://commit-digest.org/issues/2012-04-08/">this week's KDE Commit-Digest</a>:

<ul><li>In <a href="http://www.kdevelop.org/">KDevelop</a>, C++11 gets noexcept keyword support; external scripts can be run from Project context menu; performance improved in compiler error-detection regexps</li>
<li><a href="http://amarok.kde.org/">Amarok</a> gets a diagnostics dialog for easy bug reporting</li>
<li><a href="http://konversation.kde.org/">Konversation</a> supports SSL Client Certificate authentication using PEM files</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> metadata can be shown inside views</li>
<li>Amarok can distinguish between mp4, m4a, m4v types in Amarok::FileType</li>
<li>New functions in <a href="http://www.kde.org/applications/utilities/kate/">Kate</a> document JavaScript API</li>
<li>Previously hidden option allows for specifying whether an upscaling of images should be done.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-04-08/">Read the rest of the Digest here</a>.