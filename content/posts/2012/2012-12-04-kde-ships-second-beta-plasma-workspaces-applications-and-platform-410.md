---
title: "KDE Ships Second Beta of Plasma Workspaces, Applications and Platform 4.10"
date:    2012-12-04
authors:
  - "sebas"
slug:    kde-ships-second-beta-plasma-workspaces-applications-and-platform-410
comments:
  - subject: "What about Akregator2"
    date: 2012-12-17
    body: "The <a href=\"http://techbase.kde.org/Schedules/KDE4/4.10_Feature_Plan\">Feature Plan</a> mentions Akregator2, the release notes don't. Does this mean it has been postponed to 4.11?"
    author: "mark"
---
Today KDE <a href="http://kde.org/announcements/announce-4.10-beta2.php">released</a> the second beta for its renewed Workspaces, Applications, and Development Platform. Thanks to the feedback from the first beta, KDE has already improved quality noticeably. Further polishing new and old functionality will lead to a rock-stable, fast and beautiful release in January 2013. One outstanding freeze is the artwork freeze, which is planned to bring an updated look to Plasma Workspaces.