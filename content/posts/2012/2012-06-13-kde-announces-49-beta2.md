---
title: "KDE Announces 4.9 Beta2"
date:    2012-06-13
authors:
  - "sebas"
slug:    kde-announces-49-beta2
comments:
  - subject: "Testing effort status"
    date: 2012-06-13
    body: "Since the Beta 1 release, the QA and Testing Team so far opened or confirmed 101 bugs reports and 33 of those are already closed. Regressions especially are targeted to be fixed. New people joined the team and are working enthusiastically in testing, triaging and even patching! You can join too, we are available on IRC Freenode in the #kde-quality channel."
    author: "amahfouf"
  - subject: "Dolphin"
    date: 2012-06-13
    body: "Me and inline renaming, reunited at last"
    author: ""
  - subject: "File | Open"
    date: 2012-06-15
    body: "Was the \"use the Dolphin2 engine to display the File | Open and File | Save dialogs\" feature dropped?"
    author: "Alejandro Nova"
  - subject: "Feature is not planned for 4.9"
    date: 2012-06-15
    body: "This feature has not been planned for 4.9 and personally I think it would not be very effective to implement this before Frameworks 5 and Qt 5 have been released."
    author: "ppenz"
---
Today KDE <a href="http://kde.org/announcements/announce-4.9-beta2.php">released</a> the second beta for its renewed Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing new and old functionality. Highlights of 4.9 will include the following and more:

<ul>
    <li>
    Qt Quick in Plasma Workspaces -- Qt Quick is continuing to make its way into the Plasma Workspaces, the Qt Quick Plasma Components, which have been introduced with 4.8, mature further. Additional integration of various KDE APIs was added through new modules. More parts of Plasma Desktop have been ported to QML. While preserving their functionality, the Qt Quick replacements of common plasmoids are visually more attractive, easier to enhance and extend and behave better on touchscreens.
    </li>
    <li>The Dolphin file manager has improved its display and sorting and searching based on metadata. Files can now be renamed inline, giving a smoother user experience.
    <li>
    Deeper integration of Activities for files, windows and other resources: Users can now more easily associate files and windows with an Activity, and enjoy a more properly organized workspace. Folderview can now show files related to an Activity, making it easier to organize files into meaningful contexts.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making the KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.9_Feature_Plan">4.9 Feature Plan</a>. As with any large number of changes, we need to give 4.9 a good testing in order to maintain and improve the quality and our user's user experience when they get the update. If you want to make a difference, check out the <a href="http://dot.kde.org/2012/06/04/kde-announces-49-beta1-and-testing-initiative">Testing Initiative</a>.
<!--break-->
