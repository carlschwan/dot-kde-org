---
title: "KDE Commit-Digest for 29th January 2012"
date:    2012-02-06
authors:
  - "mutlu"
slug:    kde-commit-digest-29th-january-2012
---
In <a href="http://commit-digest.org/issues/2012-01-29/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://edu.kde.org/marble/">Marble</a> gains support for bicycle and pedestrian routing, and a data model to manage offline routing and offline search data</li>
<li>Development of Ekos begins: a new tool for <a href="http://edu.kde.org/kstars/">KStars</a> to utilize INDI-based (<a href="http://indilib.org/">Instrument-Neutral-Distributed-Interface</a>) devices in astrophotography</li>
<li><a href="http://www.digikam.org/">Digikam</a> gets a progress manager and Notably—a note information widget</li>
<li>A timeline and a tag widget are added to <a href="http://plasma-active.org/">Plasma Active</a>, as well as filtering and zooming capabilities</li>
<li>A number of improvements in <a href="http://www.calligra-suite.org/">Calligra</a>: slideshow feature, paragraph padding, new toolbar system, improved undo of style changes and updated MSOffice file format support</li>
<li><a href="http://joerg-weblog.blogspot.com/2012/01/hello-planet-most-of-you-dont-know-me.html">Conquirere's Zotero</a> synchronization capabilities continue to advance</li>
<li>Bug fixes and improvements in <a href="http://userbase.kde.org/KWin">Kwin</a></li>
<li>Improvements to Oxygen's GTK engine, icons, and fonts</li>
<li>Many small improvements to the KDE development tools and <a href="http://nepomuk.kde.org/">Nepomuk</a></li>
<li>Numerous bug fixes for <a href="http://dolphin.kde.org/">Dolphin</a> and KDE PIM</li>
<li>Various bugs squashed across all areas of KDE:<a href="http://kphotoalbum.org/">KPhotoAlbum</a>, <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://userbase.kde.org/NetworkManagement">Network Management</a>, Plasma addons, Plasma Active, <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://konsole.kde.org/">Konsole</a>, Kajongg, <a href="http://userbase.kde.org/Rekonq">Rekonq</a>, and many others.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-01-29/">Read the rest of the Digest here</a>.

<small><em>Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.</em></small>