---
title: "KDE Commit-Digest for 19th August 2012"
date:    2012-10-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-19th-august-2012
---
In <a href="http://commit-digest.org/issues/2012-08-19/">this week's KDE Commit-Digest</a>:

<ul><li>Rewrite of <a href="http://indilib.org/index.php?title=Main_Page">INDI code</a> in <a href="http://edu.kde.org/kstars/">KStars</a></li>
<li>EPUB 2.0.1 export filter for Words finished in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Improvements in dashboard and patch review in KDevPlatform</li>
<li>Muon Package Manager provides more information on estimated time needed to commit updates</li>
<li><a href="http://edu.kde.org/cantor/">Cantor</a> adds support for scrolling with an active drag</li>
<li><a href="http://wiki.openstreetmap.org/wiki/Kothic">Kothic</a> JSON parser for vector data tiles added to <a href="http://edu.kde.org/marble/">Marble</a></li>
<li><a href="http://kate-editor.org/">Kate</a> plugins benefit from better signalling making it easier for plugins to talk with each other</li>
<li>Lightdm saves/restores last logged in user.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-08-19/">Read the rest of the Digest here</a>.