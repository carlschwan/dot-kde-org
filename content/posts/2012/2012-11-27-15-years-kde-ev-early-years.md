---
title: "15 years of KDE e.V. - The Early Years"
date:    2012-11-27
authors:
  - "claudiar"
slug:    15-years-kde-ev-early-years
comments:
  - subject: "Congratulations"
    date: 2012-11-28
    body: "Congratulations to KDE. I've been using it since the early days in 1997 and I'm still amazed about this wonderful piece of software for the Linux comunity. Thanks a lot. You've made a great job. Go on this way..."
    author: "Ari"
  - subject: "thanks"
    date: 2012-11-28
    body: "Believe me, KDE isn't going anywhere other than in cool directions. There's plenty to be proud of and happy with, considering the growing ecosystems we're a part of (Qt, Linux, Open Source in general) and the amount of energy in our community."
    author: "jospoortvliet"
  - subject: "Real success"
    date: 2012-11-28
    body: "Congratulations, I celebrate these 15 years and hope another 15 more ;)\r\nThanks for so much good work for free technologies. Hope we'll see in Bilbao in Akademy 2013."
    author: "Dani Guti\u00e9rrez"
---
Today (November 27, 2012) is the 15th birthday of <a href="http://ev.kde.org/">KDE e.V.</a> (eingetragener Verein; registered association), the legal entity which represents the KDE Community in legal and financial matters. We interviewed two of the founding members (Matthias and Matthias) on the why, what and when of KDE e.V. in the beginning. Tomorrow, emeritus board member Mirko Böhm shares his thoughts. On Thursday there will be interviews with current e.V. Board members.

<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://ev.kde.org"><img src="http://dot.kde.org/sites/dot.kde.org/files/ev-LogoAt15Shaded4.png" width="250" /></div>
<h2>Matthias Kalle Dalheimer</h2>
<strong>You were one of the founding members of KDE e.V. back in 1997. Please tell us about the early days of KDE and KDE e.V.? What led to registering an e.V. so early in the history of KDE?</strong>
In spring 1997, I was single-handedly organizing the very first KDE conference—KDE One in Arnsberg, Germany. The budget was 14,000 German Marks (about €7,000). That's nothing compared to today's Akademy budgets, but it was a huge amount of money for me personally back then, given that I had all the risk of accommodation/food/travel providers. In the end, all sponsors came through with their pledges and we stayed within the budget. But I still didn't want to take that risk again (even more so since KDE Two already was much larger). That was the main reason for us to found KDE e.V. (in the kitchen of Matthias Ettrich's student apartment in Tübingen, Germany, where I had driven a few hours from a family visit in the Mannheim area).

<strong>What were your goals for the organization?</strong>
Initially we only wanted to provide a legal entity that could appear as a partner for conference suppliers (hotels, airlines, caterers) and sponsors. Anything else, such as holding trademarks or domain names, came later. The organization certainly fulfilled the (very limited) role it was intended for back then.

<strong>What role did the e.V. as an organization play for the development of KDE (the software) and KDE (the community)?</strong>
For the software, at that time, not a whole lot, nor was it intended to. In fact, there was initially an explicit separation between KDE e.V. and the steering committee of KDE development. As for the community, since KDE e.V. enabled larger and larger conferences to be held, this has clearly had an impact on building a community.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/collage_0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/collage_small_0.jpg" /></a><br />Collage of early KDE meetings and desktops</div>

<strong>How does it make you feel thinking about what KDE has become?</strong>
I don't think we had any doubts back then that there was a huge potential for the software, so from that point of view, I am not surprised. The one bit I was positively surprised by was how schools in many countries have adopted KDE. We weren't thinking of school students as a target group very much when we set out; we thought more about individual home users.

How large KDE has become as a movement, as a software project, as a provider and maintainer of infrastructure, is something we did not expect, though (nor did we give it much thought). So considering that, I am indeed proud to see what has become out of those first tentative lines of code we wrote back in October 1996 when KDE began.

<strong>Any anecdotes or funny stories from the early years of KDE e.V. that you'd like to share?</strong>
I can't really separate my general KDE memories from KDE e.V. memories, so this may not be specific to e.V., but …

At one of the earliest KDE conferences in Erlangen (KDE Two/1999), Matthias Ettrich and Preston Brown went off on a drinking spree. They came back to our meeting room in a rather intoxicated state and boasted loudly "Now we are going to write our own ORB!" (Object Request Broker, KDE was using CORBA at that time). They immediately set to work, despite their somewhat shaky state, worked through the night, and had the first version of DCOP (Desktop Communication Protocol, later replaced by DBus) by the next morning.

When the first system configuration cache KSycoca was written, it was originally suggested to name it System LookUp Table. For obvious reasons, this name didn't stick.

One of the very active developers of the early days, Bernd Wuebben, was working for an investment company in New York City in 2001. After 9/11, we didn't hear from him for several days and were of course very worried about him. He resurfaced a few days later, he'd been to a business meeting in Florida and had a hard time getting back home what with all flights being grounded the days after…

As mentioned previously, KDE e.V. was founded in Matthias Ettrich's small student kitchen. German law requires a club to have seven members at its inception in order to be formally registered. By forcefully enlisting some developers' girlfriends, we did get up to the 7 founding members (who all had to be in the same physical location for that purpose), however the kitchen was too small, so some of us had to shout our approval through the doorway…

<h2>Matthias Ettrich</h2>
<strong>You were one of the founding members of KDE e.V. back in 1997. Please say something about the early days of KDE and KDE e.V.? What led to registering an e.V. so early on in the history of KDE?</strong>
During the early times, KDE was criticized heavily for having an unreliable foundation in Qt. While Qt was dual-licensed from the beginning, free of charge and all its source code publicly available, it didn’t satisfy the Free Software Foundation’s criteria for free software, nor the Open Source definition. This led to the concern that the “evil trolls”—the company Trolltech which I later became part of—could change the licensing scheme and destroy KDE. Truth was that Trolltech back then consisted of 5 engineers who were working on low salaries, despite the dawning of the dotcom era, and struggling to find a way that would allow them to work on free software and make a living at the same time. 

Haavard Nord, the CEO, suggested at a Linux Kongress in Würzburg that a foundation be established that could own the rights to Qt and could relicense it should Trolltech turn evil. In order to be able to join such foundation, we needed a legal entity, and thus the idea of KDE e.V. was born. Its initial purpose was to represent the KDE community in the KDE Free Qt Foundation.

Founding the e.V. was surprisingly challenging. We needed 7 initial members in one room for the first assembly,an impossible task with the time and financial constraints back then. Martin Konold and I were in Tübingen, Kalle Dalheimer was visiting us, and we pulled in friends and spouses. One initial member, Michael Renner, later wrote a book for O’Reilly called “Linux, Wegweiser für Onliner” (Linux, guide for Internet users). The O’Reilly contact came through Kalle Dalheimer, so the book might be seen as a direct outcome of the founding assembly.

<strong>What were your goals for the organization?</strong>
It soon became clear that a project of the scope of KDE could make good use of a legal entity. Martin Konold, who did the initial paperwork, saw this from the beginning and phrased the statutes accordingly. The first developer meetings were still organized by private individuals, something which bears an unjustifiable legal risk for the people doing that. Organizing events thus quickly became another task for the e.V., as well as ownership of common properties, like trademarks and domains.

<strong>From a present day perspective: did the organization achieve what you thought it would back then?</strong>
Yes it did. During the first years, the organization struggled to gain members, which meant that the e.V. hardly represented the active KDE community. That was not so much the organization’s fault, but a general attitude in the community. Many didn’t want to be bothered with legal and organizational matters. At the same time, the e.V. was seen as a possible danger to KDE, and not the necessary support organization that it actually was.

<strong>What role did the e.V. as an organization play for the development of KDE (the software) and KDE (the community)?</strong>
The e.V. enabled—and still enables—the KDE community which it is part of. Communities need infrastructure and occasional face time, and the e.V. provides this. What KDE e.V. did not do was impact KDE software development in any way. A software project of KDE’s dimensions needs some structure in order to evolve over time and adapt to the changing world around it. KDE does not have this structure, and a vast majority of the community members never wanted such a structure.

<strong>How does it make you feel thinking about what KDE e.V. has become?</strong>
KDE, once thought of as a software product, became a community. KDE e.V. became a true representative entity of that community, and serves it well. This is good, and lots of good people seem to get a lot of enjoyment out of creating and using KDE software. So I’m definitely looking at the e.V. with one very happy eye. 

From another perspective, I wonder and have some regret that the KDE Project may have missed some chances. Imagine if we would have had enough structure in the project to make product releases and not just source blobs for distributors to select what they wanted. Imagine how the power to include or exclude pieces of software in such releases could have gently, but purposely steered the development process. Imagine how this might have opened up more intense cooperation with commercial entities. Imagine how this could have led to design houses having a channel to bring a perspective of design, user expectations and usability which KDE lacked for years. Today it’s no longer about the Linux desktop. If KDE was a steerable project with financial support and architecture boards, we would not only focus on touch input, but also look into ways of delivering our software as applications to iOS and Android users. Those are the computing platforms a lot of people use today, and we are not involved at all. All that work we did on Unix and Linux, and now that the entire world finally uses it on mobile devices, free software only plays a minor enabling role, and KDE software plays almost no role. So I see that KDE could have played a far greater role in technology than it does.

When we created the KDE Community, we used the Internet to do social networking. We used FTP, IRC, NNTP, CVS, SMTP, IMAP, some HTTP. Today almost the entire world does social networking, and many do this from Linux-based devices. Except they are not using free software, and they are giving up control over their data. And what do we do? We use the same stuff as they do and even create free software to interface with these commercial, closed, advertisement-financed services. 

We still want to liberate computer users. Including everyone—geeks and ordinary people too. And now that the whole world uses computers in similar ways like we did in 1997, we should not give up without a fight. I think that what's needed is a free distributed social network. There are tons of technical solutions already such as <a href="https://joindiaspora.com/">Diaspora</a>; the challenge is to make one popular. In order to take on such an effort, there should be an active community of a significant size with many smart tech-savvy people who can influence their environments, and a legal entity to support this. KDE has all that. Who will make it happen?

<h2>Join the Game</h2>
Please consider supporting KDE e.V. by becoming a <a href="http://jointhegame.kde.org/">supporting member</a>. We're playing a big game and invite you to join it.