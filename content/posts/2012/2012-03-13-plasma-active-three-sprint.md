---
title: "Plasma Active Three Sprint"
date:    2012-03-13
authors:
  - "sebas"
slug:    plasma-active-three-sprint
comments:
  - subject: "Awww"
    date: 2012-03-14
    body: "2 of you don't have a tablet to hold. That's sad. *hugs*"
    author: ""
  - subject: "Amazing"
    date: 2012-03-14
    body: "Great to see this progressing so fast. Congratulations and thanks to all involved making that possible :)\r\n\r\nWhile the article does a great job of answering most of the questions, I have two questions that are still open:\r\n\r\n1. I have gotten feedback from people trying the Spark (or a prototype of it) that the performance rocks. Someone is able to run e.g. Kontact Touch and other applications on it without any notable slow-down. That sounds *very* promising. Nevertheless, I still ask myself how much RAM is available for applications like Calligra Active? After booting up, how much of the 512MB RAM is in use already and how much is free? I understand that this is work in progress, so it would be fine to just give estimates of what's expected when everything is in place and RTM (Released To Market).\r\n\r\n2. A question to those who have already had some experience with the Plasma Active QML components: Does the component-framework differ a lot to the MeeGo components? In other words, how much work (yes, that's subjective) is it to \"port\" a MeeGo application to Plasma Active? I assume Qt-Mobility is shipped too?\r\n\r\nThanks :)"
    author: ""
  - subject: "Please keep Akonadi and Nepomuk optional"
    date: 2012-03-15
    body: "I like KDE, but please make semantic desktop optional. It may be useful for others, but for me, my friends and other users which I know, it is not too helpful. We usually disable this to make KDE work the way we want."
    author: ""
  - subject: "Those components will be improved"
    date: 2012-03-16
    body: "Rest assured those components will be improved, and Plasma Active is a different use case then a normal desktop. The reason for disabling them is typically a) performance b) using some other app.\r\n\r\nAs user/reader of Planet KDE I can say that:\r\n\r\nProblem a is being addressed, problem b as well. Akonadi doesn't start if it's not being used. Problem was, something was always using it (e.g. the plasma calendar)\r\n\r\nAnd in general:\r\n\r\nThe rules of a tablet also change how we can use computers. On a desktop you can worry about shutting down apps and where to store files. On a tablet, this may not be a requirement anymore. On tablets, things often suspend, and come back up quick. Apps typically show their data (e.g. a photo app shows albums, a coding app shows projects) so there is no visible filesystem anymore. And yet people are productive on these devices!\r\n\r\nSome old problems simply don't matter anymore on these devices. The workflow changes, and not every habit needs to be taken with you on a tablet."
    author: ""
  - subject: "Interaction options requested"
    date: 2012-04-01
    body: "I'm going to jump in and add to Anonymous' wish for a semantic desktop. I understand why this new glitzy sort of thing is necessary, and I think you've done a beautiful job of it. But... for those of us who like the old way of doing things (a panel with a menu, a task manager, and a system tray on it, a variety of widgets to pick from, including launchers for any program on the system), please make it optional for us to choose to use that method of interacting with our computers. Then whoever wants the new stuff can have it, but us old dogs can still enjoy KDE. (:\r\n"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PA3SprintB.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/PA3Sprint350.JPG" /></a><br /><small>Plasma Active Three sprint - (top left to bottom right): Aleix Pol,<br /> Martin Gräßlin, Marco Martin, Martin Klapetek, Martin Brook,<br /> Sebastian Kügler, Laszlo Papp, Alex Fiestas, Dario Freddi,<br /> Maurice de la Ferte <br /><strong>Not shown</strong>: Cornelius Schumacher, Fania Bremmer, Seif Lotfy,<br /> Thomas Pfeiffer. (click for larger)</small></div>

Last week, a group of Plasma Active hackers and designers met in <a href="http://www.basyskom.com">basysKom</a>'s office in Darmstadt. The officially dubbed "Plasma Active Three Sprint" had as its goals to plan the next release of KDE's device-spectrum user experience, define work needed to accomplish this release, design user interfaces for new features and enhancements, and of course <em>get cracking</em>. Another point of focus was to work on a few things that need to be done before the launch of the <a href="http://makeplaylive.com">SPARK</a>, the first consumer device featuring a fully free and openly developed software stack, running KDE software.
<!--break-->
Since its inception less than a year ago, a lot has happened to get Plasma Active into a state suitable for end-users. In the first year, the team finished the <a href="http://dot.kde.org/2011/10/09/plasma-active-one-released">first usable version of Plasma Active</a>, suitable for tablet computers, reaching a milestone by bringing KDE software and a fully Free stack to a new class of devices. Moreover, just before the end of last year, the team released <a href="http://dot.kde.org/2011/12/14/plasma-active-two-released">Plasma Active Two</a>, bringing considerable performance and usability improvements with support for a wider range of devices. The cycle towards Plasma Active Three will be considerably longer: The team plans to release Plasma Active Three in late August 2012, after the KDE 4.9.0 releases. Platform-wise, the goal is to make it possible to run Plasma Active on an unmodified KDE stack. (The first two releases needed some patches to the vanilla KDE Platform to make it more suitable for the special needs of the new class of supported devices.)

<h3>Integrated Social Networking & Accounts</h3>

Social networking is one of the primary use-cases of mobile devices. While many bits and pieces already exist, for example the microblogging Plasma widget and Share Like Connect, there is not system-wide, integrated solution. Accounts are not shared between apps currently; every app handles data by itself. In addition, there is no integration with other components of the system such as Nepomuk. The team came up with a  plan to overcome all these shortcomings by centralizing access to data, and having apps become consumers the data. In this way, apps can concentrate on the user experience, rather than on protocols, data retrieval and mangling. The overall architecture will employ Akonadi as access layer, allowing unified access and caching. Nepomuk, KDE's semantic system, provides the means to structure the data in user-meaningful terms, for example making it possible to access different kinds of data by associating with person (as opposed to data associated with "users of a specific service"). Additional value is created from the unification of contacts which has been a long ongoing topic of research and development in the PIM and Telepathy communities.

Centralized management of accounts will be one of the underpinnings of integration, allowing for secure handling of account credentials, and a more user friendly setup of various web services used. Work on this part will commence in the coming weeks, spearheaded by KDE's Martin Klapetek. Lend him a hand if you're interested in making this topic a success story.

<h3>New Apps</h3>

While Plasma Active is more than just an app launcher, it still features apps. During the sprint, developers demoed and presented Active versions of applications many already know from the Plasma Desktop and Netbook Workspaces. Alex Fiestas and Aleix Pol showed a QML-ified version of the webcam app <a href="http://www.afiestas.org/what-is-kamoso/">Kamoso</a> which raised many eyebrows thanks to its cool and fun graphical effects. Aleix Pol has also been working on a Plasma Active port of KAlgebra, an educational application that helps <em>do the math</em>. Laszlo Papp has thrown in a good amount of packaging expertise, and also worked on an Active port of Kanagram. Sebastian Kügler showed preliminary work on a tablet version of a microblogging app supporting Twitter and Identi.ca. Marco Martin presented a preliminary version of an app store client, which will provide the primary interface to download and install new apps (and content), and a touch-friendly file manager which gives the user access to digital assets through the Nepomuk semantic system.

Experience gained during the creation of these apps is being used to improve the Developer Story for Plasma Active. The third Plasma Active sprint revealed a blooming ecosystem, and a growing team of people dedicated to taking truly Free and open systems beyond the desktop.

<h3>The Developer Story</h3>

In order to make it easy for developers to bring their apps to users of devices, a strong 'developer story' is needed. There should be a clear route from "I have this idea" to an app available in the Plasma Active appstore and onto users' devices. The Plasma Active team came up with three groups of developers. 
<ul>
<li>Relatively simple apps. Technically speaking, this can include any app that can be implemented without the need of any compiled code -- think of Plasma widgets or Plasma Quick apps that use existing frameworks features to do their jobs.</li> 
<li>More complex apps such as Kontact Touch, Calligra Active or other existing, or newly planned apps</li>
<li>contributions to Plasma Active or Mer systems</li>
</ul>
App development is not necessarily different from the development of the workspace and the core system. Still, the level of complexity for new developers should be kept to a minimum, so we can reach a wide audience. Developers will have access to existing kdelibs frameworks such as libplasma, theming, localization features through KLocale, access to existing functionality using dataengine or other features available on Plasma Active Devices, WebKit, and of course the excellent Qt libraries.

The rough plan is to accommodate simple apps development with the Plasmate IDE, which provides a workflow-based tool that guides developers through the steps of creating, editing, packaging and publishing of apps. Complex app and system developers will be advised to use a specialized version of the Mer SDK (a virtual machine image-based SDK) which provides all the tools readily set up to get apps cross-built and running on a specific device.

<h3>Special guests</h3>

The Plasma Active team was happy to welcome new members from outside of the core KDE community: Martin Brook, who has been doing a lot of great work on device adaptation through the <a href="http://merproject.org/">Mer operating system</a>, joined the meeting, sharing his experience, insights, thoughts and expertise, and working on improvements to the deployed images on top of Mer. Another special guest (and couch-surfing host) was GNOME and Zeitgeist hacker Seif Lotfy, contributing an open mind and and additional points of view.

<h3>Credit where credit is due</h3>
<p>
The whole Plasma Active project could not have been incepted without the great work done on the KDE platform and the desktop components. Conversely, much of the work done on Plasma Active directly benefits the Desktop and Netbook Workspaces. Special thanks go to <a href="http://basyskom.com">basysKom</a> for kindly hosting this meeting, and to the <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://jointhegame.kde.org">its supporters</a> for making this meeting possible with organizational and financial support. Your author thanks his employer, <a href="http://www.open-slx.com">open-slx</a> for supporting his work on Plasma Active.