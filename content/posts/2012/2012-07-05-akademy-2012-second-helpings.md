---
title: "Akademy 2012 \u2012 Second helpings"
date:    2012-07-05
authors:
  - "kallecarl"
slug:    akademy-2012-second-helpings
comments:
  - subject: "Videos ?"
    date: 2012-07-05
    body: "Previous years had videos of the talks, which was awesome.\r\n\r\nIn any case, I hope everyone is having the fun they deserve for giving us such an amazing desktop."
    author: ""
  - subject: "videos are coming"
    date: 2012-07-06
    body: "There will be videos as soon as they have been converted to a proper format. Watch for an announcement.\r\n\r\nYes, people are enjoying themselves at Akademy. It is a genuine pleasure to work hard in cooperation with other committed people."
    author: "kallecarl"
  - subject: "Fun? No! hard work"
    date: 2012-07-06
    body: "We are not at all having fun. We are doing hard work from early morning to late evening. Next up: deprecate KTabBar. Then Something else with Tabs. and then something else with bars.\r\n\r\n/Sune"
    author: ""
---
<h2>Will Schroeder Keynote</h2>

Will Schroeder is the CEO of Kitware Inc., a company that builds open source scientific software that also depends on open source. He suggested that open source is the most effective way to get things done through agile, collaborative innovation.
<!--break-->
Traditionally science was open, critically reviewed and widely available. Results were shared and new innovations could build on previous discoveries. It is now largely closed and restricted by patents.

This is not just a theoretical issue. Sune Vuorela mentioned examples in Will's talk where the lack of openness is potentially harmful. For example, 90% of cancer research results simply cannot be reproduced. In many cases, there is no rigorous peer review (test data are not available, analysis software is missing or proprietary, logic is flawed). The experiments simply cannot be redone. So a peer review involves no duplication of research, and is not much more than an expert opinion that the experiment makes sense. This does not apply to just research, but also clinical drug trials.

Another example involved brain imaging research. Several groups tried to replicate project results using original input data but they didn't have access to the original software. About half of the results were similar to the original. However several were substantially different. Without open access to the original software, it was not possible to know how the research was analyzed. In these cases, the method of getting a result is more important than the same result as the original research. Open source scientific methods provide credibility that is increasingly compromised.

Free and open access to information has benefits in all fields, not just software. In science being open was once standard practice, maybe it's time to return to sharing information openly.

<div style="padding: 1ex; margin: 1ex; border: 1px solid grey;text-align: center"><a href="http://byte.kde.org/~duffus/akademy/2012/groupphoto/"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-group-med.jpg" /></a><br />Akademy 2012 Group Photo</div>

<h2>Sessions - Day Two</h2>
The Akademy Program Committee did an outstanding job of pulling together a diverse set of presentations. There has been <a href="http://akademy2012.kde.org/program">plenty of choice</a>.

Martin Gräßlin spoke about the recent developments in KDE Plasma's Window Manager (KWin), which has seen considerable progress. In the upcoming release, KWin gains support for strong scripting capabilities so that a window manager can be implemented on top of the well-established code base. Some KWin effects have been ported to JavaScript for more consistent animations; there are still many effects that could benefit from such a port. Adding scripting support has decreased the complexity of KWin. As other presenters did, Martin invited people to participate by working on these easy and fun ports. There are plenty of opportunities to contribute to KDE excellence.

The KDE Connect and KDE e.V. presentations gave attendees some insight into what KDE is doing to increase our reach and influence in open source. Other presentations ranged from promoting an open source project and preserving freedom of use to specific applications and topic areas.  

<h2>Akademy Awards</h2>

On Sunday, the traditional Akademy awards were presented to people whose work and dedication were especially important to KDE over the past year.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;text-align: center"><a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-akademyawards.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-akademyawards-wee.jpg" /></a><br />Akademy Award Winners</div>

The organization award to Laur Mõtus and the entire volunteer team for organizing a wonderful Akademy. The venue is outstanding, with plenty of room, a comfortable layout and an attractive setting. Effective organization has meant that attendees feel taken care of and can focus on content. The team's courtesy has made every participant feel welcome. (And there are power sockets for every seat in the auditorium.) Huge thanks to Laur and the team.

The non-application award went to Lydia Pintscher for the unbelievable amount of work she puts into KDE, especially the effort and care she lavishes on the Google Summer of Code and the Season of KDE. KDE is the largest participant in the Google Summer of Code; there is no way KDE could handle that without Lydia. And she does this in addition to being an active KDE e.V. Board member. Incredible! Congratulations Lydia! 

K&eacute;vin Ottens and Nicol&aacute;s Alvarez received the Jury award together. Their work on the future of KDE has long term value and importance. Nicol&aacute;s received the award for his work on the conversion of KDE projects to git, ushering in a new age of agility for our coding teams. K&eacute;vin was recognized for his work on KDE Frameworks 5, giving a new lease of life to KDE applications and desktop.

Camilla Boeman received the Application Award in recognition of her work on Calligra Words. In the past two years, she has brought together a team of developers spread from Europe to India who are dedicated to Calligra Words, and the team delivered the first release. Congratulations to Camilla, her team and all Calligra people.

<h2>KDE e.V. AGM</h2>

On Tuesday the AGM (Annual General Meeting) of KDE e.V. was held. KDE e.V. is the registered non-profit organization that represents the KDE Community in legal and financial matters. As with similar organizations, it is a requirement for the members to get together once a year to discuss any major decisions and future directions, report on the previous year's activities and to elect Board Members. Earlier this year Frank Karlitschek stepped down and Celeste Lyn Paul's term came to an end. Their work within KDE is very much appreciated. Agustin Benito Bethencourt has been working as a provisional Board Member since Frank left. He and Pradeepto Bhattacharya were formally elected to the Board. Active contributors to KDE are encouraged to join the e.V.

<h2>Akademy 2012 Tallin, Estonia</h2>
For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy2012.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

For more information, contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.

<em>Photos by Matthias Welwarsky (CC BY-SA)</em>