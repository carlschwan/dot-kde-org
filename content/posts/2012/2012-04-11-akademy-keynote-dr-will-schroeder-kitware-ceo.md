---
title: "Akademy Keynote: Dr. Will Schroeder, KitWare CEO"
date:    2012-04-11
authors:
  - "tcanabrava"
slug:    akademy-keynote-dr-will-schroeder-kitware-ceo
comments:
  - subject: "Interesting interview, Tomaz!"
    date: 2012-04-11
    body: "Interesting interview, Tomaz!"
    author: "jospoortvliet"
  - subject: "Thanks for the interview, was"
    date: 2012-04-11
    body: "Thanks for the interview, was worth reading! :D"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/schroeder300.JPG" /><br /><strong>Will Schroeder</strong></div>
Dr. Will Schroeder is one of the keynote speakers for <a href="http://akademy.kde.org/">Akademy 2012</a> in Tallinn, Estonia. Will is the CEO of <a href="http://www.kitware.com/">Kitware Inc.</a>, a proven leader in the creation and support of open-source software.

I asked him to share his background and insights from his experiences with Free and Open Source Software. People attending Akademy 2012 are fortunate to have Will Schroeder helping to expand horizons.

<h2>Background</h2>

<strong>Tomaz:</strong> Some of us are aware of your projects, such as the Medicine Insight Toolkit and the Visualization Toolkit. Would you please tell us more about yourself?

<strong>Will:</strong> I consider myself a computational scientist, I very much enjoy creating software, particularly complex algorithms. While I was trained initially as a mechanical engineer, once I began using computer simulation tools, and writing the code behind them, I realized how much I enjoyed the creative act of developing software. I am also pretty good at written communication, so I've been able to combine these skills to build and advocate for scientific computing tools. I also consider myself a risk taker, which enabled me, along with my co-founders, to start the crazy open-source experiment that became Kitware, which is now a 100+ person company with a very pure open-source business model. I am passionate about science and thoroughly enjoy collaborating with researchers to discover new knowledge, and disseminate it to the world. 

<strong>Tomaz:</strong> How did you become an OpenSource advocate?

<strong>Will:</strong> It really started as a desire to collaborate. I was working at a industrial research lab doing really great stuff with many of my co-workers. We had cutting-edge, proprietary visualization software, and were trying to transition it across the company, and collaborate with other, outside researchers. But there was resistance by company employees who wanted to learn a proprietary tool with a very small community. Plus company policies placed all sorts of IP barriers in the way of external collaboration. And there was the practical matter of maintaining complex code with a small research staff. When we wrote the Visualization Toolkit textbook, we wrote it mostly as a means to collaborate, and wanted to have minimal restrictions on what we called our "demonstration code". The desire to collaborate remains to this day, which is why we advocate permissive licenses (we avoid reciprocal giveback licensing conditions) and do everything we can to remove barriers to working together.

<strong>Tomaz:</strong> From your <a href="http://www.kitware.com/company/team/schroeder.html">brief biography at the Kitware website</a>, it seems that your current hobby is researching and creating code. What do you do for leisure?

<strong>Will:</strong> My wife Susan and I have a house in Maine near Acadia National Park which we consider to be one of the most beautiful places in the world. I spend as much time as I can outdoors there hiking, running, biking or sea kayaking. Susan is an artist and makes a living as a landscape architect; together we both like to design and build things including gardens and living spaces. Right now I am totally into home control systems including A/V, climate, and lighting. I do everything from pulling wire to programming controllers. I also dabble at writing poetry; Rumi is my favorite poet.

<strong>Tomaz:</strong> One of the most unusual compliments that I have received on one of my talks was, "You look like a hippie trying to save the world with peace, love and free software" and to be honest, sometimes I do feel that way. What were your main reasons for releasing valuable software like VTK?

<strong>Will:</strong> What I love about being part of the open source movement is that it has a heart, a brain and it's dangerous. At its heart, the noble aspirations of sharing, collaboration and community are inspiring and bring deep purpose to our efforts. However open source is incredibly smart in that it is the most effective way to get things done through agile, collaborative innovation. So this makes open source dangerous to many entrenched technology interests; those that want to control markets and innovation. These parties feel existentially threatened; and they should, because open source methods deliver much more value more quickly--conventional software models have great difficulty competing with passionate communities. So to make a long story short, we wanted the fruits of our passion, VTK, to be part of this great movement and to enable us to collaborate across a community.

<h2>Kitware</h2>

<strong>Tomaz:</strong> What was the main idea in the beginning of Kitware? What were your thoughts back then?

<strong>Will:</strong> I had an inkling that writing the <a href="http://www.vtk.org/">VTK book</a> would change my life, but I never guessed that it would grow into a successful, profitable company. Early on we could see that VTK was in high demand and a community was forming, plus I was itchy for a new challenge. I knew we had some good technology, and I really loved the openness that the business model promised. But what was really important to me at that time were my co-founders. I know my strengths and weaknesses, and I knew to make it work I would need help from those that were better at various aspects of technology and business than I am. Hence Ken Martin, our CFO is one of the best software developers I have known, yet has an uncanny way with finance and business (he actually has helped write our internal finance system which is awesome). The same is true of Lisa (graphics), Bill (CMake/software process), and Charles (Algorithms). To this day we have a very strong team relationship. It has had the added benefit of sharing the management load. 

<strong>Tomaz:</strong> Why did you start doing the VTK? How did that help Kitware?

<strong>Will:</strong> I have absolutely loved scientific and numerical computing since my college days (where I actually used punch cards). The elegance of expression in the form of 3D graphics is hard to beat, and greatly appealed to me since I am a visual thinker. This love of graphics eventually led me to a research group working in visualization (Bill Lorensen was in the group). VTK was a natural extension of this effort, which produced a large community and eventually resulted in business opportunities that became Kitware. 

<strong>Tomaz:</strong> What do you feel about the huge amount of Free and Commercial software using Kitware products like CMake and CTest?

<strong>Will:</strong> The more people that use our software the better! This is an insidious part of our business model; our tools are adopted widely and companies then engage us for support and technology integration. We also greatly enjoy sharing, plus from experience I know that many users eventually give back to the community in one way or another. For example, we've had oil & gas companies use our software with no initial intention of ever contributing code. However, after working with us, they often see the advantage of releasing bug fixes or contributing selected infrastructure code to the community to reduce long-term maintenance. Many people don't realize it, but commercial enterprises and government funding have contributed a huge amount to the open source community with very little credit in many cases.

<h2>Kitware and KDE</h2>

<strong>Tomaz:</strong> How did the cooperation between kitware and KDE start?

<strong>Will:</strong> A lot of the credit belongs to Alex Neundorf. As part of the KDE community, he had been monitoring the build challenge and looking for a better solution. He became an early advocate for CMake, and after initial attempts by the KDE community at adopting other build systems failed, he suggested giving CMake a try. Fortunately Bill Hoffman, Kitware's CTO and CMake developer, was able to find some time and money to devote concentrated effort to the challenge. He and Alex, along with other CMake developers and KDE community members, were able to show significant progress in just two weeks. Alex has written about the process and given proper credit, in this great article <a href="http://lwn.net/Articles/188693/"><em>Why the KDE project switched to CMake -- and how</em></a>.

<strong>Tomaz:</strong> In the transition from KDE3 to KDE4, someone said that "KDE is the biggest testcase that there is". How do you feel about one of your company products, CMake, being stress tested by KDE? 

<strong>Will:</strong> We thrive on these opportunities. We are determined to create the best scientific computing software in the world. Yet we realize that this is a hard problem and we will stumble and make mistakes along the way. A challenge like this enables us to make our code even better, and yes, admit to a lot of mistakes but correct them. Plus we receive the help of really smart people from other communities like KDE. It takes some humility to look to the future and realize that the process can be hard at times but the end result is worth it.

<strong>Tomaz:</strong> Did CMake and Kitware itself get a boost in cooperation and overall software quality after teaming up with KDE?

<strong>Will:</strong> Absolutely! KDE was huge for CMake and for Kitware. The challenge forced us to dig deep to add new capabilities and fix various problems. We also felt the scrutiny of the KDE community and knew this would likely be our only chance to make it work. Surprisingly some of our funding sponsors benefitted from the success of the CMake/KDE relationship too; the added features, scalability and robustness meant that our other scientific computing tools became that much better.

<strong>Tomaz:</strong> Where does Kitware and its software really shine in your opinion?

<strong>Will:</strong> We are developing software and software processes that are becoming an essential part of scientific computing. We are tackling and solving significant problems related to data scale and complexity, and making significant impacts in high performance computing, health care, simulation, modeling, and security. We are also participating in, and in some cases leading the Open Science movement, and helping knit together efforts in scientific computing from around the world.

<h2>The Importance of Akademy</h2>

<strong>Tomaz:</strong> It's common to get passionate when working for a community-based project, but there's always distance between the contributors. What do you think an event such Akademy does for Free and Open Source Software Communities?

<strong>Will:</strong> Collaboration is a joy when done right, but can be a terrible experience when communities go down the dark side. Often times the difference between the two experiences is establishing a personal relationship with the developers on the other end of the wire. Conferences like Akademy enable us to build bonds that encourage the development of our natural talents and to leverage them, and enable us to overlook the human quirks that we all share. To me collaborating with talented people is what makes this adventure great, and conferences are essential to this experience.

<strong>Tomaz:</strong> Thanks for your time, and it's been an honor to interview you.