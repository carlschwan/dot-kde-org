---
title: "Simon Speech Recognition Project Moves to KDE"
date:    2012-04-08
authors:
  - "vladislavb"
slug:    simon-speech-recognition-project-moves-kde
comments:
  - subject: "Congrats!"
    date: 2012-04-09
    body: "I knew this was a good idea :)"
    author: "troy"
  - subject: "voice control for all!!"
    date: 2012-04-11
    body: "I know that the benefits of this work will help the handicapped and seniors (not fun to use a mouse-keyboard-touch when you have arthitis so this is a blessing). But as someone who has a laptop in the kitchen for recipes or entertainment, I see many uses for Simon. Heck, my dad plugs one of those folding keyboards when he works in the garage and doesn't want to stain his laptop.\r\n\r\nA good voice recognition program on Linux would benefit many people.\r\n\r\nCan't wait to try it and show it to my mother. \r\nFinger free computer use would be awesome for her.\r\n\r\nI remember reading about Simon a while back and am happy to hear that it's doing well and moving along nicely.\r\nDefinitely will be keeping an eye on it.\r\n"
    author: ""
  - subject: "With pleasure!"
    date: 2012-04-15
    body: "Nice article! I am sure other sysadmin members think the same: it was a pleasure to help you move over."
    author: ""
---
<a href="http://www.simon-listens.org">Simon</a>, KDE's speech recognition software, has recently migrated from Sourceforge to <a href="https://projects.kde.org/projects/extragear/accessibility/simon">KDE's Git infrastructure</a>. Developed to allow people with physical disabilities to <a href="http://youtu.be/bjJCl72f-Gs?t=8m34s">control their computers</a> <a href="http://youtu.be/3_RTJsq9m4I">entirely by voice</a>, Simon has found its way into voice-controlled media centers in <a href="http://youtu.be/35tyZntA9j4?t=3m24s">homes for the elderly</a> and most recently in <a href="http://www.youtube.com/watch?v=PDkEe4G4PX4">assistive care-giving robots</a>.

The move has also brought Simon into <a href="https://projects.kde.org/projects/extragear/">KDE Extragear</a>, the kde-accessibility mailing list, and <a href="http://forum.kde.org/viewforum.php?f=216">KDE's accessibility sub-forum</a> for user support.

The community around Simon has grown since the move, so this is a good time to join in for those interested in improving Simon or KDE's other <a href="http://accessibility.kde.org">accessibility projects</a>. The Simon Listens e.V. is also accepting <a href="http://translate.google.com/translate?hl=en&sl=de&tl=en&u=http%3A%2F%2Fsimon-listens.org%2Findex.php%3Fsupport">tax-deductible donations</a> (translated from German) to further support development of their software.

Simon's lead developer Peter Grasch shares his experience migrating to the KDE ecosystem.
<!--break-->
<h2>Joining KDE</h2>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/PeterGrasch2012_300.jpg" /><br /><strong>Peter Grasch</strong></div>
The first KDE event I ever attended was the Akademy in Tampere, Finland back in 2010. I gave a talk about Simon and tried my best to get people excited about speech recognition and accessibility in general. I wasn't alone, though. Troy Unrau, who had already written about Simon for the Dot before, introduced me to most of KDE's royalty and got the <a href="http://simon-listens.blogspot.com/">Simon Listens blog</a> aggregated to <a href="http://www.planetkde.org/">Planet KDE</a>. Most importantly, he suggested moving the project from Sourceforge to the <a href="https://projects.kde.org">KDE infrastructure</a>. Almost two years have passed since, and I'm now proud to announce that Simon has <a href="https://projects.kde.org/projects/extragear/accessibility/simon">finally moved to KDE's Extragear</a>!

<h2>Motivation</h2>

Sourceforge technically has everything you'd want from a project host. It is a reliable site, has unlimited storage, free access for everybody, and a sophisticated user system with lots of tools like trackers, statistics, etc. readily available. However, in practice, the provided tools turned out to be quite primitive and the site itself was slow and confusing.

So as you can probably guess, getting my hands on some of that <a href="https://projects.kde.org">projects.kde.org</a> goodness was always something of a goal of mine. And why wouldn't it be? The infrastructure is not only amazingly versatile and sophisticated, it also brings what I already have considered to be a "KDE project" for a long time nearer to the KDE community.

<h2>Background</h2>

To apply to become part of the KDE Software Collection, the first step is usually to write an email to the kde-core-devel mailing list. The first time I wrote that mail was in July of 2010 and it was met with a lot of positive responses. However, at that time most KDE software was still residing in the old subversion repositories. Since we had already switched to Git on Sourceforge, we put the move on hold until KDE's Git was up and running.

However, with multiple active research projects around Simon and other commitments battling for our limited free time, the move to KDE was delayed time and time again - even after the Git infrastructure was ready to accept new projects. Then came August of the next year and with it the Desktop summit in Berlin. This time it was <a href="http://blogs.fsfe.org/gladhorn/">Frederik Gladhorn</a> of <a href="http://qt-project.org/wiki/Qt_Accessibility">Qt accessibility</a> fame that bugged me about still being hosted on Sourceforge. Stoically, I re-affirmed our intention of "eventually" moving to the KDE infrastructure, telling him "as soon as we are ready".

Fast forward a few months to Christmas and Frederik apparently got tired of waiting. He opened a sysadmin request to ask what would be necessary to move Simon's codebase to KDE's Git and nonchalantly added me to the CC list. And so, amidst finishing up an EU research project and dealing with university finals, in January I moved Simon's codebase to KDE's playground repository.

<h2>Making the Move</h2>

The first thing I learned was that over the last couple of years we gobbled up a lot of history in our Sourceforge repository and that history was largely complete and utter rubbish. Hundreds of commits without any commit messages, commits containing temporary files, and missing or incorrect authors all over the place were all reminders of Simon's humble beginnings.

That all wouldn't have happened on the KDE Git infrastructure, though. There, Git hooks automatically check commits for those and other problems and simply reject bad commits.

Sadly however, it also did that for the now almost five-year-old commit history of the Simon repository. No matter. After a quick chat with the KDE sysadmin team, which was extremely responsive and helpful as always, they disabled the hooks for the initial import of the repository.

Together with the repository migration, we also had a few more tasks to take care of. However, thanks to the stellar sysadmin team, this wasn't difficult. First, we needed to ensure that all our contributors had commit accounts. We only have a handful of active contributors at the moment who mostly already had accounts. Adam Nash, our Google Summer of Code student from last year, applied for an account to continue work on his project and got approved in less than a day. Next on the list was a new category for Simon on <a href="https://bugs.kde.org">KDE's bugtracker</a>, which was also quickly taken care of.

We also had an online forum on sourceforge that we used to communicate with our users. KDE of course has great <a href="http://forum.kde.org">community forums</a> but they didn't yet have a category for accessibility applications. After another sysadmin request, we had a <a href="http://forum.kde.org/viewforum.php?f=216">new category</a> to support users of KDE's assistive technologies. Frederik and I were both promoted to forum moderators and our new home there is already seeing steady traffic.

Lastly, we needed a mailing list and after discussing it on kde-accessibility@kde.org, we joined the discussion there.

<h2>Review Period</h2>

In order for a project to move to its target location in KDE's project hierarchy (for example, in one of the core modules like KDE Graphics, KDE Workspace or Extragear), the codebase needs to undergo some quality assurance tests. This was initially done using KDE's automated code checking software, Krazy. An overview of all of KDEs software can be found on the <a href="http://www.englishbreakfastnetwork.org">English Breakfast Network</a>, which runs Krazy on KDE's entire codebase every night. After any problems reported by the static analysis are fixed and a code review is formally requested on the mailing list, a two week review period commences. Except for a minor performance suggestion (that has since been implemented), we passed without a hitch and are now "settling into" KDE's Extragear :).

<h2>The Payoff</h2>

So why did we go through all of that trouble? That's easy. The move resulted in a greater awareness of Simon within KDE and lowered the entry barrier for further contributions. Here are some of the facts since we migrated to KDE:
<ul>
<li>After pushing our code to playground, it took only a day until the first commit from a new contributor.</li>
<li>After the first week, we had six contributors besides myself committing code. Five had never committed to Simon before. Together we racked up 54 commits in a single week.</li>
<li>At the same time, translations started to pop up like mushrooms after a heavy rain. Simon has since been completely translated to Ukrainian, Swedish, Brazilian and Portuguese. Existing translations of Czech, German, Dutch, Spanish and French were extended and updated. Polish, Slovak, Irish Gaelic and even Lower Saxon were introduced and completed to varying extents.</li>
</ul>

<h2>Final thoughts</h2>

While this long article might look like all this was a laborious and time-consuming process spanning months, I have to say that it didn't take that much time at all. Though we let quite some time pass between the different stages of migration due to other pressing commitments, all in all, it could have easily been completed in less than a month.

One question that has presented itself time and time again during the migration was how much of a separate identity we wanted to preserve. Of course, since we are already clear about being a KDE project, we are aligned with KDE community overall. However, what remained were the little details. Do we really need a separate sub-forum? Or should we just create our own forum about KDE accessibility in general? Should we create our own mailing list or simply use the existing kde-accessibility list? Would it be a bit presumptuous to the current subscriber base? 

In our case, we decided against a separate forum and mailing list after discussing it with the existing KDE accessibility team. We came to see that, with a team as small as the KDE accessibility team, it wouldn't have made sense to introduce additional fragmentation. However, it was definitely something I spent some time thinking about.

In closing, I want to say that our experience with the KDE community so far has been entirely positive. Looking at the migration as a project administrator, the move brought us into a better ecosystem all around and added helping hands. Everyone involved in migration, including the new contacts made during the process, was hard working, efficient and, most importantly, genuinely nice people.

Thank you to the whole KDE community for your involvement and the awesome work that's being done all around. And especially, the translators and the sysadmin team are really under-appreciated heroes! If you have a project that you think should be a KDE project, don't wait as long as we did! Apply today!