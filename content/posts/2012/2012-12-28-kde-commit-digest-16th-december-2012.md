---
title: "KDE Commit-Digest for 16th December 2012"
date:    2012-12-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-16th-december-2012
---
<a href="http://commit-digest.org/issues/2012-12-16/">This week's KDE Commit-Digest</a> starts with two stories: insights about Google Code-In work on KDE-PIM and interesting recent events in the spotlight. Of course, there is also a summary of development effort:

<ul><li><a href="http://www.digikam.org/">Digikam</a> extracts JPEG preview from RAW files, gets various updates in Queue Manager</li>
<li>Kscreen moves OutputView to QML; multiple other changes</li>
<li>nepomuk-metadata-extractor adds a progress window when processing a folder with many files</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> uses nepomuk2; code cleanup to stop using deprecated types and methods</li>
<li><a href="http://www.kexi-project.org/">Kexi</a> can import csv data into an existing table</li>
<li>Last.fm plugin configuration reworked for smoother experience in <a href="http://amarok.kde.org/">Amarok</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-12-16/">Read the rest of the Digest here</a>.