---
title: "4.9 Releases \u2013 Quality, Stability"
date:    2012-08-01
authors:
  - "kallecarl"
slug:    49-releases-–-quality-stability
comments:
  - subject: "Great looking super mega release"
    date: 2012-08-05
    body: "Pure awesomeness. Best thing ever!"
    author: "kragil"
  - subject: "KDE 4.9*"
    date: 2012-08-07
    body: "Thank you for this release!"
    author: ""
  - subject: "Thanks"
    date: 2012-08-09
    body: "Best release yet, keep up the good work ;)"
    author: ""
  - subject: "Thank you for this great release!"
    date: 2012-08-10
    body: "Thank you, all developers of KDE!!!\r\nI worked lot with KDE 3.5 many years ago...\r\n...then unfortunately I got a little bit frustrated with the new 4.0 release those days and for a long time I did not try KDE any more...\r\n\r\nIn the meantime I programmed a lot with Qt4, but under windows...\r\nNow at work I have to program under Linux again, also in Qt.\r\n\r\nTrying the new release of KDE (now for 4 weeks 4.8 and then 4.9) I have to tell you that I'm SOOOO HAPPY with this new desktop!!!!\r\nYou brought back the stability and usability of the 3 series and enhanced it with new features!!!\r\n\r\nI also like the activity concept very much and I'm excited how you will extend it in the future!\r\n\r\nSo THANK YOU, THANK YOU and THANK YOU again!!!  ;-)"
    author: ""
---
KDE has announced its <a href="http://kde.org/announcements/4.9/">Version 4.9 Releases</a> to KDE Plasma Workspaces, KDE Applications, and the KDE Development Platform, dedicated to the memory of KDE contributor Claire Lotion. The <a href="http://kde.org/announcements/4.9/">full announcement</a> has details.

These releases got special attention to the general levels of quality and stability. In particular, there was a focus on eliminating any regressions from previous releases. Thanks to the efforts of the KDE Quality Team and its new contributors, the 4.9 Releases are the best ever.
<!--break-->
<div style="text-align:center;padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kde49desktopb.png" /><img src="http://dot.kde.org/sites/dot.kde.org/files/kde49desktopb525.png" /><br /></div><div style="text-align: center;"><big><strong>Plasma and Applications 4.9</strong></big></div>
<br />

<h2>Plasma Workspaces, Applications, and Platform Releases</h2>
Substantial improvements were made to Plasma Workspace core components in this release. There are new KDE Applications and major improvements to Okular, Kopete, KDE PIM, education, games and others. The KDE Platform release includes quality improvements and preparation for Frameworks 5 based on Qt5. The <a href="http://kde.org/announcements/4.9/">announcements</a> have more details.

<h2>Help Spread the Word</h2>
The KDE team depends on software developers. And non-technical contributors are also critical to success. <a href="https://bugs.kde.org/">Report bugs</a>. Encourage others to join the <a href="http://community.kde.org/Getinvolved">KDE Community</a>. Or <a href="http://jointhegame.kde.org/">Join The Game</a>.

Please spread the word on the social web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.9 releases.

Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from identi.ca, twitter, youtube, flickr, picasaweb, blogs and other social networking sites.