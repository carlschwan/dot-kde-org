---
title: "KDE Commit-Digest for 26th August 2012"
date:    2012-10-25
authors:
  - "mrybczyn"
slug:    kde-commit-digest-26th-august-2012
---
In <a href="http://commit-digest.org/issues/2012-08-26/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.calligra-suite.org/">Calligra</a> adds a new application, Author</li>
<li><a href="http://www.kexi-project.org/">Kexi</a> gets support for user data storage and remembers column widths of table views</li>
<li>Aurorae supports Plastik's configuration options</li>
<li><a href="http://edu.kde.org/kgeography/">KGeography</a> can scramble colors</li>
<li><a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a> improves the presentation of a journey</li>
<li>User can configure application shutdown time in KDE Workspace</li>
<li>Numerous refactorings in KDE Workspace in Bookmarks runner, including new support for Chrome/Chromium bookmarks, and both Chrome and Firefox favicons</li>
<li>Phonon-VLC offers a new state: paused before playing.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-08-26/">Read the rest of the Digest here</a>.