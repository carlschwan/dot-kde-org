---
title: "Akademy 2012 Survey and Videos"
date:    2012-07-22
authors:
  - "sealne"
slug:    akademy-2012-survey-and-videos
comments:
  - subject: "Video formats"
    date: 2012-07-22
    body: "unfortunately due to the amount of post processing required, we were not able to make watchable quality versions of the videos in any other formats"
    author: "sealne"
  - subject: "Nice keynote from Mathias Klang..."
    date: 2012-07-23
    body: "... worth to watch - worth to spread.\r\n\r\nAnd nice to get some updates about Vivaldi and the kernel problems.\r\n\r\nWill the still missing videos appear later, e.g \"Plasma Active\u2014Freeing the Device Spectrum\"?\r\n\r\nThanks\r\n\r\n  Thorsten"
    author: ""
  - subject: "Remaining Videos"
    date: 2012-07-23
    body: "Sorry the Plasma Active video is available, I just missed adding a link to it, linked now.\r\n\r\nWe hope to have the remaining videos available within the next few days, there are a few problems with the source video for those"
    author: "sealne"
  - subject: "Slides sources?"
    date: 2012-07-23
    body: "KDE took part in the Open Document day and as such should commit itself to also always attach sources of slides.\r\nThis will help other people giving talks all around the world.\r\n\r\nI'd like to see all Akademy material having free sources available (press books, logos, ...), there is a promo git repository for this if needed.\r\n\r\nAnne-Marie"
    author: "annnma"
  - subject: "I unfortunately have to"
    date: 2012-07-23
    body: "I unfortunately have to disagree. First of all this would have to be communicated together with the call for communication - it is highly unusual to share the sources for a presentation.\r\n\r\nFor example I did my slides with LaTeX and as I did the presentation only for compiling it once to pdf, the source code of the presentation does not have the quality so that I would share it. Having the presentation source code in a sharable state would have meant quite some more work which would have made me think twice whether I want to give a talk at Akademy."
    author: "mgraesslin"
  - subject: "I don't see why you need"
    date: 2012-07-24
    body: "I don't see why you need \"quality\" in your latex code. I do all my talks in Latex Beamer and provide a tarball of the tex file + the screenshots or I add them to my personal git  scratch pad. This allows other people to pick my Latex theme for example in their talks and to reuse my slides. Just like I make my C++ code available.\r\nPersonally I indeed would see the use of an Open Standard as source a requirement in all KDE communication, starting with Akademy. Then making it available under a free license is another step towards freedom. \r\n"
    author: "annnma"
  - subject: "Latex sources would be quite"
    date: 2012-07-24
    body: "Latex sources would be quite a treat. And I think all people who would use the sources know latex well enough that they would not mind the quality - they likely created their own share of hacked together latex presentations :)\r\n\r\nActually with Latex you are in a position that you can share your talk very easily: just tar the latex and the source images and the sources are there."
    author: "ArneBab"
  - subject: "I don't see why you need"
    date: 2012-07-24
    body: "<cite>I don't see why you need \"quality\" in your latex code.</cite>\r\nBecause I don't want to give something away which I would feel ashamed of to be honest. My code looks different (no matter if C++ or LaTeX) if I write it to be only used by myself or if I want to share it. \r\n\r\nAnd another reason is that very often my LaTeX code contains slides I removed later on. So it's much more than what I had in the presentation.\r\n\r\nLong story short: without putting time into it, I cannot share it. And if someone wants to have it shared at least for me it would have to be said before I start writing the presentation."
    author: "mgraesslin"
  - subject: "Many thanks to the team and Kenny"
    date: 2012-07-25
    body: "for the awesome videos! Great work, very enjoyable to watch. Obviously a lot of work but totally worth it for us who were not there! \r\n"
    author: "annnma"
---
Now that Akademy 2012 is behind us and life is getting back to normal, we can now share videos and slides from the conference talks with those who weren't there.
<!--break-->
All available videos and slides are linked to from the <a href="http://akademy2012.kde.org/program">Akademy Program</a>. We are indebted to everyone who helped record the talks, Marko Puusaar from the Estonian IT College and especially Kenny Coyle who put a lot of time and effort into video post-processing.

Please spend a few minutes completing the <a href="http://akademy2012.kde.org/akademy-2012-survey">Akademy 2012 Survey</a>. The information you provide is important for shaping future KDE conferences and is useful to the Akademy Team and Program Committee.

If you haven't left feedback for talks you attended, there is a <a href="http://akademy2012.kde.org/program">feedback link below each talk</a> to help the speakers and future program committees.

Akademy 2012 wouldn't have been possible without the support of our sponsors: Blue Systems, Nokia, Digia, Google, Intel, Collabora, Zabbix, froglogic, Red Flag Software, and media partner, Linux Magazine. Many thanks also to our venue, the Estonian IT College and its staff, speakers and the program committee, BoF leaders, the organizing team and volunteers, and everyone else who helped make Akademy 2012 in Tallinn a success.

<div style="padding: 1ex; margin: 1ex; text-align: center"><a href="http://byte.kde.org/~duffus/akademy/2012/groupphoto/"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-team_wee.jpg" /></a><br />The Akademy 2012 Team & Volunteers &nbsp; &nbsp; &nbsp;<small><em>Photo by Matthias Welwarsky (CC BY-SA)</em></small></div>

Thank you to those who were part of Akademy 2012 in Tallinn. If you have comments or ideas that are not covered in the survey, please <a href="mailto:akademy-team@kde.org">email the Team</a>.