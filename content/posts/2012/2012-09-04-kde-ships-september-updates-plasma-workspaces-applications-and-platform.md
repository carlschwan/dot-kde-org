---
title: "KDE Ships September Updates to Plasma Workspaces, Applications and Platform"
date:    2012-09-04
authors:
  - "sebas"
slug:    kde-ships-september-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "I come back"
    date: 2012-09-06
    body: "After years with Ubuntu/Gnome, I come back to KDE again. It's the best of the best.\r\nI can't wait for 4.9.1 :)"
    author: "NR"
  - subject: "I came back too"
    date: 2012-09-08
    body: "I was a happy kde 3.5 user for some time, but when kde 4.0 came out it didn't work for me. I switched to gnome. Now, years later, due to the lack of a viable future for gnome, I started looking into alternatives. I discovered that kde 4.8 was quite usable, and finally switched back. I'm running 4.9 now, looking forward to 4.9.1"
    author: "Jake"
  - subject: "Excellent DE"
    date: 2012-09-08
    body: "Upgraded from KDE 4.9.0 to 4.9.1 yesterday in Gentoo, and it went very smoothly. KDE these days is a pleasure to use.\r\n\r\nMy only grumbles are with a few KDE applications. KsCD has refused to play audio CDs on my machines for many releases now (<a href=\"https://bugs.kde.org/show_bug.cgi?id=302806\">Bug Report</a>). Dragon Player cannot play audio CDs on my machines either. Amarok loses CD covers, cannot play audio CDs using the KDE GStreamer Phonon backend, and, when using the VLC Phonon backend, the disc spins so fast that the noise makes Amarok impractical. Fortunately there are plenty of alternative multimedia applications to choose from."
    author: "Fitzcarraldo"
  - subject: "Kaffeine, for example."
    date: 2012-09-08
    body: "Kaffeine, for example"
    author: "Sys"
  - subject: "New to KDE"
    date: 2012-09-10
    body: "Hi\r\n\r\nI have been a happy Gnome user on Ubuntu for many years, I have always liked the way it was customizable to my needs and tastes.\r\n\r\nAfter having forced myself to use Unity and (for a very short time) Gnome 3, these last years, in the hope it would grow on me, I have concluded that they probably would be great on a tablet or mobile phone, but not on a desk- or lap-top, and that I am incompatible with both desktop environments.\r\n\r\nSo I have had to find another desktop.\r\n\r\nMany years ago I tested KDE with little success, so I started testing Xfce but it was simply too buggy.\r\n\r\nIt was with some trepidation I installed KDE, but after having played with the latest KDE this weekend I realised this was my future desktop environment, and now I find it even better than the Gnome 2, as I remember it ;)\r\n\r\nGreat work, just what I needed !\r\n\r\nKim"
    author: "Kim Foder"
  - subject: "Using KDE 4.9.1 in archlinux"
    date: 2012-09-10
    body: "Using KDE 4.9.1 in archlinux and am truly loving it ... used to be long time Gnomer but now I really glad that there is DE that is good and productive enough :) also tried LXDE and Xfce even though they are pretty light weight i think KDE offers the best productive tools for work.\r\nGreat job ... hope I can contribute at some point in the future :)"
    author: "archmonk"
---
Today KDE <a href="http://www.kde.org/announcements/announce-4.9.1.php">released updates</a> for its Workspaces, Applications, and Development Platform.
These updates are the first in a series of monthly stabilization updates to the 4.9 series. 4.9.1 updates bring many bugfixes and translation updates on top of the latest edition in the 4.9 series and are recommended for everyone running 4.9.0 or earlier versions. The release only contains bugfixes and translation updates so it is a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
<br /><br />
Significant bugfixes include improvements to the Kontact Suite, bugfixes in Dolphin and many more corrections and performance improvements all over the place.
The changes are listed on <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.9.1&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a>, though this list might not contain all improvements since 4.9.0. For a complete list of changes that went into 4.9.1, you can browse the Subversion and Git logs. 4.9.1 also ships a more complete set of translations for many of the 55+ supported languages.
To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.9.1.php">4.9.1 Info Page</a>. If you would like to find out more about the KDE Workspaces and Applications 4.9, please refer to the <a href="http://www.kde.org/announcements/4.9/">4.9 release notes</a> and its earlier versions.