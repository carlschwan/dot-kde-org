---
title: "Open Advice\u2013Project Inspiration"
date:    2012-02-08
authors:
  - "kallecarl"
slug:    open-advice–project-inspiration
comments:
  - subject: "It's great!"
    date: 2012-02-15
    body: "Congratulations on your book, Lydia and contributors. I have it on my Kindle, and am loving it so far. Valuable work for all of FOSS.\r\n\r\nValorie, aka Linux Grandma"
    author: ""
---
Thinking of starting a free and open source project? Looking for inspiration on an existing project? Lydia Pintscher has pulled together useful wisdom from free and open community leaders in a new book—<a href="http://open-advice.org/"><em>Open Advice</em></a>.
<!--break-->
Lydia is a <a href="http://ev.kde.org/">KDE e.V.</a> Board member. She's part of the <a href=" http://ev.kde.org/workinggroups/cwg.php">KDE Community Working Group</a>, the Google Summer of Code Coordinator for KDE and contributes to projects and people outside KDE as well. Lydia is highly qualified to help people discover key concepts that make free and open software projects effective.

<a href="http://open-advice.org/"><em>Open Advice</em></a> is a collection of essays from leading FOSS contributors providing their personal answers to the question–"What would you have liked to know when you started contributing?" Lydia is the editor and has just published this book of valuable insights.

<h3>Why Open Advice?</h3>
As Lydia explains on <a href="http://blog.lydiapintscher.de/2012/02/04/open-advice/">her blog</a>, she helps "...people make amazing things happen and realize what they are really capable of". But commonly there are two obstacles: people don't think that they have what the project requires (aka "I'm not a programmer."), or they are overwhelmed. Open Advice offers guidance in those areas and much more.

<em>Open Advice</em> presents practical observations from people who have had real success with their FOSS contributions. It is an introduction to FOSS heroes who will inspire others to take the leap.  

The book is available as a paperback, free PDF or source at the <a href="http://open-advice.org/">Open Advice website</a>.

<small><em>Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.</em></small>