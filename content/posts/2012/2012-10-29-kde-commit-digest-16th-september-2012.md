---
title: "KDE Commit-Digest for 16th September 2012"
date:    2012-10-29
authors:
  - "mrybczyn"
slug:    kde-commit-digest-16th-september-2012
---
In <a href="http://commit-digest.org/issues/2012-09-16/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> adds support for mod, s3m, it and xm with <a href="http://developer.kde.org/~wheeler/taglib.html">TagLib</a> 1.8</li>
<li><a href="http://konsole.kde.org/">Konsole</a> can trim spaces at end of line</li>
<li><a href="http://joerg-weblog.blogspot.com/2012/01/hello-planet-most-of-you-dont-know-me.html">Conquirere</a> replaces KBibTeX online search with the Metadata Extractor plugin version</li>
<li>Work on packagekit dataengine for use in application launchers in <a href="http://www.kde.org/workspaces/">KDE-Workspaces</a>; ongoing work on KDE Frameworks</li>
<li>Amarok updates Amazon store support</li>
<li>Bugfixes in <a href="http://pim.kde.org/">KDE-PIM</a>, KDE base applications, <a href="http://www.calligra-suite.org/">Calligra</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-09-16/">Read the rest of the Digest here</a>.