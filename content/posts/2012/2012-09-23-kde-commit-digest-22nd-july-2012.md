---
title: "KDE Commit-Digest for 22nd July 2012"
date:    2012-09-23
authors:
  - "mrybczyn"
slug:    kde-commit-digest-22nd-july-2012
---
In <a href="http://commit-digest.org/issues/2012-07-22/">this week's KDE Commit-Digest</a>:

<ul><li>Improvements in QML shutdown dialog</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> allows toggling full window view mode for Todo and Month views and gets a special character adding feature</li>
<li><a href="http://edu.kde.org/ktouch/">KTouch</a> adds a skeleton for keyboard layout properties widget</li>
<li><a href="http://www.digikam.org/">Digikam</a> gets a new tool: filmtool, an automatic whitepoint finder</li>
<li>In <a href="http://techbase.kde.org/Projects/Kooka">Kooka</a>, the user can now drag and drop in the gallery</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> supports animation motion along a path</li>
<li>New <a href="http://amarok.kde.org/">Amarok's</a> Context View is available.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-07-22/">Read the rest of the Digest here</a>.