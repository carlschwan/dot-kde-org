---
title: "Support the 2012 KDE Randa Meetings: Inspired and Intense"
date:    2012-08-13
authors:
  - "kallecarl"
slug:    support-2012-kde-randa-meetings-inspired-and-intense
comments:
  - subject: "Some feedback"
    date: 2012-08-13
    body: "It is really good that KDE tries to raise funds from the community. This should be done more, but also for apps which lack manpower and which lack features or are buggy. \r\n\r\nI also like the idea of using an external platform for this which may bring more attention. To me, the Pledgie platform looks boring and is not user friendly. When I want to donate, it asks me to create an account first (which takes extra time). It also allows donations without account, but this is not very visible and I nearly did not see it. I cannot recommend an alternative, but I would recommend looking for one that is visual pleasing and has a good usability.\r\n\r\nAnd I have another suggestion. There are campaigns where a sponsor matches every donation with an equal donation. Since KDE already has corporate sponsors who might anyway also sponsor the event, it might be helpful to set up a matching donation campaign next time."
    author: "mark"
  - subject: "Thx for the feedback"
    date: 2012-08-14
    body: "Thank you for your feedback. We'll try to consider it for the next time."
    author: "unormal"
  - subject: "Donated"
    date: 2012-08-14
    body: "Go Randa :-)"
    author: ""
  - subject: "You rock :D"
    date: 2012-08-20
    body: "Thanks!!!"
    author: "jospoortvliet"
---
The KDE Randa Meetings are a small gathering of KDE contributors in the village of Randa, Switzerland. For the fourth year, the intense Randa sprints will include key KDE projects and top developers, all collaborating concurrently under one roof, isolated from noise and distractions. <a href="http://pledgie.com/campaigns/18045">Funds are being raised</a> to support the meetings.
<!--break-->
"The sprints in Randa are special. Surrounded by the mountain landscape, intense collaboration happens as everyone gathers in one place. Meeting there means getting work done and no escape from planning, discussing and coding." says two-time Randa attendee and KDE Accessibility hacker, Frederik Gladhorn.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/RandaClassroom.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/RandaClassroom525.jpg" /></a><br /><strong><big>Strategy, Planning, Coordination</big></strong> &nbsp; <small>photo by Kévin Ottens</small></div>

Randa 2012 will be held from 21 to 27 September 2012. The meeting participants have ambitious goals that will benefit KDE users and developers.
<ul>
<li>The KDE Accessibility team and GNOME Accessibility experts are making it easier to use accessible applications, particularly for people who are visually impaired. Blind and visually impaired users have been invited to provide guidance and assist with testing.</li>
<li>KDE Education developers are working on existing and new educational apps for mobile devices.</li>
<li>The KDE Multimedia and Amarok teams will be discussing the future of Amarok, particularly on mobile devices. In addition, they will be working with Qt5 in the Phonon Multimedia Framework, which is the foundation for KDE sound applications.</li>
<li>Planning for the Plasma Workspaces future, particularly as it relates to KDE Frameworks 5, based on Qt5. This work is critical to defining a common direction and the requirements for moving to an upgraded development environment with minimal disruption to users. The team will also be doing intensive coding.</li>
</ul>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/RandaWork.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/RandaWork525.JPG" /></a><br /><strong><big>At it</big></strong> &nbsp; <small>photo by Anne-Marie Mahfouf</small></div>

Every previous Randa meeting has been concentrated and productive, generating exceptional results; organizers and participants expect the same and more this year. The Randa meetings have produced significant breakthroughs in the past such as:
<ul>
<li>KDE Applications and Plasma Workspaces running on ARM CPUs,</li>
<li>A secure authorization framework,</li>
<li>Designing KDE Frameworks as a replacement for kdelibs and KDE Platform, improving upstream relationships and dramatically simplifying development,</li>
<li>Complete Amarok user documentation and considerable bug squashing,</li>
<li>The KDE Multimedia team streamlined applications and development frameworks to improve performance and simplify development.</li>
</ul>
This year, 35 developers from all over the world will be at the Randa meeting. In seven inspiring days full of hard work, they will accomplish more than even they can imagine. Driven by a commitment to supporting Free Software for all people. 

While the participants are unpaid volunteers, there are hard expenses, such as accommodation, food and travel. If you are not attending, you can still support the Randa meeting by making a donation. As in the past, the Randa meeting will benefit everyone who uses KDE software.

The fundraising goal is €10,000. Please <a href="http://pledgie.com/campaigns/18045">donate what you can</a> to help make Randa 2012 possible.