---
title: "The KDE Manifesto"
date:    2012-10-08
authors:
  - "jospoortvliet"
slug:    kde-manifesto
comments:
  - subject: "awesome + 2 questions"
    date: 2012-10-09
    body: "Awesome manifesto! It really clarifies what KDE stands for.\r\n\r\nThere are two parts I didn't understand:\r\n- The paragraph \"Software assets access model\" seemed to be like a contradiction. What is it?\r\n- I read some people don't like the Qt CFA. I really can't be bothered. How does KDE stand in this?"
    author: "vdboor"
  - subject: "clap clap"
    date: 2012-10-09
    body: "really good idea, I support this excellent initiative"
    author: "Nef"
  - subject: "Missing a link to KDE home page"
    date: 2012-10-09
    body: "Congrats, it's a really great manifesto! Love it!\r\nAnyway, shouldn't there be a link to main kde.org website, so that people coming there from the news will be able to know us?"
    author: "gerlos"
  - subject: "Extension needed"
    date: 2012-10-09
    body: "In my opinion the Manifesto should be in short time extended for non-human beings, for example aliens and centaurs. When we find them they also ought to be able to participate in community and use KDE software.\r\n"
    author: "niko\u015b"
  - subject: "Link to kde.org added"
    date: 2012-10-09
    body: "Thank you.\r\n\r\nLink added."
    author: "kallecarl"
  - subject: "No contradiction"
    date: 2012-10-09
    body: "It is not a contradiction, is just narrowing the phrase so that it becomes VERY exact. This is probably one of the most controversial things put to print in the Manifesto, but also how KDE has always worked."
    author: "carewolf"
  - subject: "\"KDE\" doesn't \"stand\" in "
    date: 2012-10-10
    body: "\"KDE\" doesn't \"stand\" in  this. Qt's CFA is for individual contributors.\r\n\r\nI think KDE benefits from good collaboration with Qt."
    author: "sebas"
  - subject: "The Manifesto"
    date: 2012-10-11
    body: "It's amazing to see one's own feelings and perceptions shared by people who one doesn't know written down with such clarity.\r\nNow more than ever I'm a very happy user of this magnificent desktop environment."
    author: "martinc"
  - subject: "Manifesto background..."
    date: 2012-10-12
    body: "<a href=\"http://ervin.ipsquad.net/2012/10/12/kde-manifesto-unexpected-party/\">from K\u00e9vin Ottens</a> who led the Manifesto writing project"
    author: "kallecarl"
  - subject: "Agreed ;-)"
    date: 2012-10-13
    body: "Agreed ;-)"
    author: "jospoortvliet"
---
<blockquote>"We are a community of technologists, designers, writers and advocates who work to ensure freedom for all people through our software."</blockquote> 

Since it began more than 15 years ago, the <a href="http://kde.org/">international KDE community</a> has grown bigger and more diverse than could have been imagined at the beginning. These forces created a need for clarity about what pulls us together as a community. Over the last six months or so, we have examined this critical issue, moving beyond assumptions and what has been taken for granted. In a rigorous project led by Kévin Ottens, many thoughts were distilled down to essentials. Today, we present the result of that effort: the KDE Manifesto.

<h2>What is KDE?</h2> 
A community effort like ours, with such a size and history, is unusual, important and exemplary in today's competitive world. Clarity about who we are and what binds us together establishes an identity that can be preserved and built upon. But we have never been much for formality, writing things down and creating processes. So having a written Manifesto is a big deal.

<h2>Organization and Being KDE</h2> 
KDE has a flat organizational structure with largely self-directed teams. Teams are created by the people who want to do the work, and are rarely endorsed in a formal way. Being KDE is mostly self-selecting¸ based on a shared understanding built through direct interaction with other people and working together with them. KDE sysadmins decide on allowing access to infrastructure, and the KDE e.V. board makes specific funding decisions. But, aside from those, there are few rules or formal processes outside of some technical requirements. 

The KDE Manifesto is not intended to change the organization or the way it works. Its aim is only to describe how the KDE Community sees itself. What binds us together are certain values and their practical implications, without regard for who a person is or what background and skills they bring. It is a living document, so it will change over time as KDE continues to grow and mature. We are sharing the Manifesto to help people understand what KDE is all about, what we want to accomplish and why we do what we do. 

<h2>The Manifesto</h2> 
The Manifesto begins with an overarching declaration: 
<ul style="list-style: none;">
 <li>“We are a community of technologists, designers, writers and advocates who work to ensure freedom for all people through our software.”</li>
</ul> 

It continues by laying out KDE shared values as they have developed over the last 15 years, along with a brief description of each:
<ul style="list-style: none;"><li>Open Governance</li>
<li>Free Software</li>
<li>Inclusivity</li>
<li>Innovation</li>
<li>Common Ownership</li> 
<li>End-User Focus</li>
</ul>

Two supporting pages present the benefits and principles of being associated with KDE. More so than the Manifesto itself, they are living documents, although we expect little changes to be needed in the coming years.

<h2>You are invited</h2>
We invite you to read <a href="http://manifesto.kde.org/index.html">the KDE Manifesto</a> and find out what it is to be in KDE!