---
title: "KDE PIM Sprint 10: ACCOMPLISHED!"
date:    2012-02-17
authors:
  - "jospoortvliet"
slug:    kde-pim-sprint-10-accomplished
comments:
  - subject: "Nokia and Qt"
    date: 2012-02-17
    body: "Erm, what? Did I read that correctly? Nokia will be using Qt, it's not throwing the project in the trash bin any more? I'm really starting to get lost in all their OS and toolkit plans. That, and the unclear state of commercial Qt - who runs it today? Digia? A Nokia branch built up from former Trolls?\r\n\r\nNice to hear about such sprints, really nice! It gives the users the warm feeling of their favourite app being supported and not abandoned, as it often happens with Open Source. Not to say about the bugfix boost those sprints give to a project. Keep up the good work!\r\n\r\nRegards,\r\nIgnat Semenov"
    author: ""
  - subject: "Sounds great!"
    date: 2012-02-20
    body: "The sprint sounds great - and I did not know that the PIM team is that big. Photos are great!\r\n\r\nAnd Kudos for going to the ACTA protest!\r\n\r\nSince you wrote about usability, I recently got into thinking what I miss for my personal/private communication. I came up with people-based communication.\r\n\r\nI\u2019d love to have a view of my friends with photos, a simple option to contact them (text field) and a field with the latest information I have from them - regardless of the communication channel.\r\n\r\nA view which integrates information from RSS feeds, website-updates (via shimbun), emails, chat, instant messaging and microblogging. \r\n\r\nI got to that when I though about what Google+, Facebook and all the other websites actually do. They reimplement all the standard communication protocols and unify the information focussed on people.\r\n\r\nA thread is characterized by the thread starter, and so on.\r\n\r\nThen there is a way to see all friends (not just anyone in the addressbook, but friends, neatly sorted into cathegories), what\u2019s new and how to contact them.\r\n\r\nEssentially unifying information from all ways to communicate into the desktop using people as the organization structure. And I think akonadi is a perfect base for that.\r\n\r\nChoqok-like information aggregation along with an elegant addressbook with status (last message) and a least effort way to contact someone (for example just click into a text field and start typing)."
    author: "ArneBab"
  - subject: "Nokia owns Qt"
    date: 2012-02-24
    body: "Well, Nokia still owns Qt and is hiring people to work on it all the time. So they have plans with it - and rumor has it that the plan is called 'Meltemi' and is a new MeeGo/N9(Swipe!) like OS for low-end phones to replace SymbianV3+. Also, the upcoming version of Windows Phone (8) is rumored to have support for Qt :D"
    author: "jospoortvliet"
  - subject: "yep yep"
    date: 2012-02-24
    body: "So this is EXACTLY the kind of cool stuff one can (and hopefully will) do with the new KDE PIM. There was lots of talk about those things at the meeting but the hope is also that non-current-KDE-PIM people will come in and start doing those things..."
    author: "jospoortvliet"
---
The 10<sup>th</sup> KDE PIM Meeting in Osnabrück finished on 12 February. Starting with pizza Friday afternoon running until Sunday around 17:00, the meeting attracted more than 20 hackers working on the various parts of KDE PIM. There was talk, code, beer, a group picture and even an anti-ACTA demonstration. Read on for a report on the meeting.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/KDEPIM_Meetings_Osnabrueck10_group.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEPIM_Meetings_Osnabrueck10_group525.jpg" /></a><br />KDE PIM Hacker Team—Freezing for Freedom (details below; click for larger)<br /><small><em>Photography by Guy Maurel and Bernhard Reiter</em></small></div>

The meeting officially started on Friday when everyone had arrived around 16:30. Cornelius took the lead, introducing 10 years of KDE PIM sprints making it the longest running series in KDE, as well as the first real sprint! Only three people have attended all ten meetings—Cornelius, Ingo and Bernhard.

<h2>History of KDE PIM</h2>
The first KDE PIM meeting brought together developers working independently on their respective applications. It was decided at that meeting to move KMail from the KDE Network group to the brand new KDE PIM CVS location, creating a real 'team' dedicated to maintaining the applications. At that meeting, the team also decided to continue the integration work started shortly before in the form of Kontact Shell, using the applications in KParts form to bring them together in one UI.

At the KDE PIM sprint in 2004, deeper integration was decided upon: the creation of one unified layer instead of the separate data storage locations. And now, at this tenth PIM sprint, the first real unified KDE PIM can be celebrated. Release 4.7 brought users 5 years of hard work, and in 4.8.0, most problems from this huge merger have already been fixed.

(<a href="http://community.kde.org/KDE_PIM/History">Read more about KDE PIM history</a>)

<h2>Going to work</h2>
After reminiscing, we went through introductions, started creating our <a href="http://en.wikipedia.org/wiki/Kanban">Kanban</a> list of TODO items and got to work. During the day there were the usual moments of synchronization and overview-of-work-done. Throughout the sprint, there were bug fixing frenzies and "fix the problems of my mail setup" sessions. For example, the number of bug reports for the IMAP resource was reduced from <a href="http://ervin.ipsquad.net/2012/02/12/kde-pim-sprint/">forty to twenty</a>.

An enjoyable Chinese dinner was sponsored by <a href="http://intevation.net/">Intevation</a>, <a href="http://www.kdab.com/">KDAB</a> and <a href="http://kolabsys.com/">Kolab Systems</a>. The same sponsors hosted an Arabian dinner another evening. Thank you for keeping us well fed!

<h3>Frameworks 5</h3>
The next morning we started by discussing progress from Friday. Difficult items were brought for the group to discuss. Some people gave reports on what had been accomplished. Part of this was a discussion about getting KDE PIM ready for KDE Frameworks 5. Kevin Ottens, our KDE Frameworks 5 Release Manager, was present so this was a good time to discuss time lines and priorities. The conclusion was that KDE PIM will not likely go Frameworks 5 before the beginning of 2013. This will certainly be one of the hot topics for Osnabrück 11.

<h3>OpenUsability, Kolab, REST APIs</h3>
Björn Balazs from <a href="http://www.openusability.org/">OpenUsability.org</a> joined us and presented a plan to gather more user feedback on KDE PIM. The idea was enthusiastically received. Some work was started to display images of KDE PIM hacker Stephen in the UI to put a "human face" on our apps. The exact implementation and upgrade options (Upgrade to images of KDE PIM hacker Volker) were put on the table and are still not exactly clear. But we had fun with the joke. The collaboration between KDE PIM, OpenUsability and openSUSE is expected to lead to something interesting in this area.

A couple of discussions related to Kolab and its relationship with KDE PIM. Christian Mollekopf presented the next generation of the Kolab XML format, which is based on xCard and xCal. Georg Greve presented the concept of putting Akonadi on the server to provide a scalable Groupware solution making use of the experience and high-level functionality provided by the KDE PIM backend. Both are part of the <a href="http://blogs.fsfe.org/greve/?p=470">Kolab 3.0</a> development cycle, which is currently ongoing and welcomes participation. On top of that, there was some discussion about IMAP 5, which could converge protocol efforts around Kolab and Akonadi into a standard for wider usage.

Tomas Koch, a student who is working on his thesis around KDE PIM, brought up the topic of creating a REST backend for Kolab, an interesting approach for refreshing the traditional protocols used for email and other PIM data. He'll work on this and keep the team informed about what comes out of it.

<h2>Demonstration and more</h2>
After lunch there was a group picture. The team then joined the <a href="http://www.stopacta.info/">anti-ACTA</a> demonstration in Osnabrück! Over a thousand people gathered and walked through the city. Due to the freezing cold, yours truly didn't see it through all the way, but several of the hackers withstood the cold, showing their resolve against government silliness.

<h3>Marketing</h3>
After warming up, everyone went back to work until it was 17:00 and time for more reports and discussions. This time the topic was marketing. The refactoring of the KDE PIM backend is done, and there is a solid foundation. Now we will switch our perspective to the user side, continually improving the applications and resolving issues. Users are encouraged to submit bug reports. Any assistance with pruning existing bug reports would be appreciated as it helps the development team focus on fixing things.

<h2>Platform Discussions and Telepathy</h2>
On Sunday a meeting took place to discuss the available platforms for Kontact Desktop and Kontact Touch. <em>Android</em> is hard—it's a limited platform with things like daemon-killing processes it deems inactive, a rather big challenge for the heavily multi-process design of Kontact. <em>Blackberry</em>, however, is more like a home fixture. Its (BSD licensed) QNX base offers a full Unix environment. With Qt as its primary UI toolkit and the excellent Qt port actually developed by some of the people in the room, support for this platform should not be hard. <em>iPhone</em>, on the other hand, simply won't be possible due to legal restrictions.

<h3>MeeGo and Mer</h3>
<em>MeeGo</em> versions of Kontact Touch are up and running, but the platform has some issues of its own. There are problems with the UI falling back to QWidgets in some cases. These are completely unuseable on MeeGo (unlike on the earlier GTK-based Maemo devices). Fixing this requires either the creation of a touch-compatible QWidget theme, or porting everything that is still not QML-based. The first is outside the comfort zone of most developers at the PIM meeting and the second is a serious amount of work. 

The QML porting work is also hampered because the MeeGo Community Open Build Service is heavily underpowered in terms of hardware—building a package takes over 6 hours. This is a well-documented issue, but Nokia is unwilling to fix it with MeeGo and its community support in a minimal maintenance mode. <em>Mer</em> is suffering from the same issues as they are still on the Community MeeGo infrastructure. The possibility of moving development over to the public <a href="http://build.opensuse.org">openSUSE Build Service</a> or a self-hosted server were suggested but a decision to move is up to the Mer project. While Mer could make this change, MeeGo development needs to have the signing of applications available on its community build service, so moving isn't a solution for MeeGo work in any case.

<h3>Nokia's Next Billion</h3>
Meanwhile, Nokia's <em>'next billion' OS</em>—rumored to be Linux/Wayland/Qt-based—will probably not be hard to support but there is nothing public yet. <em>Windows Mobile 8</em>, which is supposed to have the ability to run native code, is another possible target for KDE PIM. Porting will probably be quite easy. The question is how relevant this OS will become in the market and what restrictions it will have.

<h3>Windows, Mac OS X</h3>
Then there was a discussion about KDE PIM on <em>Windows and Mac</em>. Both are close to "good enough". During the meeting, Till did a build and generated a tarball for installation. The biggest issue with these platforms is that they are understaffed and need some love. Each one has only two people supporting it. There is almost no awareness of the state of KDE on Windows and KDE on Mac. Both projects need only a small push to be ready to go.

<h3>Telepathic Collaboration</h3>
There was also a video conference call on Sunday with some members of the Telepathy team to discuss the KDE contact aggregation strategy. Decisions came rather quickly: it makes most sense to store relations between contacts in Nepomuk as this is exactly what Nepomuk has been designed to do. Some performance issues still exist, but the team expects that these can be addressed rather quickly.

<h2>Done!</h2>
At the end of the day (when people had started to leave), we thought of a few t-shirt slogans for KDE, including "I abstracted your abstraction layer" and "I do funky things with your ....".

The office was closed around 17:00 and the last remaining KDE PIMsters could only conclude that it had been a fun and useful event... And finish with a group hug.

<small><em>Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.</em></small>