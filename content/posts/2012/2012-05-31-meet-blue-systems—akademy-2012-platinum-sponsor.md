---
title: "Meet Blue Systems\u2014Akademy 2012 Platinum Sponsor"
date:    2012-05-31
authors:
  - "tcanabrava"
slug:    meet-blue-systems—akademy-2012-platinum-sponsor
---
This year we have Blue Systems as a Platinum sponsor of Akademy 2012 Tallinn. Recently, they made news by offering continous support for Jonathan Riddell, lead developer of Kubuntu, the KDE distribution that they would like to become one of the best on DistroWatch.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/blue-systems-logo300.png" /></div>
They also sponsor Netrunner and MintKDE which are based on Kubuntu; all three distributions have now been strengthened. In addition, Blue Systems sponsors kde-gtk-config, the Folderview Plasmoid, rekonq, and QML ports of MenuEditor and  Muon Discover.
 
Blue Systems hasn’t been very visible in the KDE ecosystem, so I wanted to find out more. Talking with people at the Company and others familiar with it, clearly Blue Systems’ priorities are different from those of most companies. KDE and Blue Systems are a pretty good match in more areas than just technology. Akademy 2012 attendees are fortunate to have access to this partnership.
 
<h2>People Actually Doing the Work</h2>
Blue Systems works as part of the KDE Community directly, helping improve KDE and maintain high quality standards. Polishing the end user experience is one of the priorities. Accomplishing these goals means that the main focus is on supporting the developers actually doing the work. Akademy is therefore interesting and worthy of support because it’s about the social side of things for people who actually do something for KDE. These producers are essential to KDE success and a critical part of Blue Systems sponsorships.

People who commit themselves to making great free software are the true achievers. At Blue Systems, there is attention to why developers do what they do, why they are committed to something, why people believe in KDE. They think about the relevance of KDE to <strong>users</strong> over the long term, wondering if KDE would fade away if not for enthusiasts. So one of their goals is to consider what it takes to expand that enthusiasm and build a growing userbase around KDE.

<h2>Big Picture—Freedom and Opportunity</h2>
Collaboration between Blue Systems and KDE also fits into a larger worldview—the Internet as a commodity like electricity; free software with universal and free Internet access providing equal opportunities, recognizing that these are futuristic considerations with political ramifications. The Internet and the whole technology infrastructure are so fundamental and important to modern life that they should be like air, available to everyone.

Such thoughts should be automatic when people consider how Free and Open Source Software will succeed. It’s not easy to see how to get there, but there really should be plenty of resources to make this happen. If Information = Power, then unlimited information should create a lot of power. In such an environment, creativity and innovation can grow without limits as well.

So this raises important questions about the future and where we see ourselves in it. Seeing how the future relates to what we do right here and right now. Those questions point towards understanding the passions that drive us. And our work with FOSS really comes down to a fundamental choice, in the case of Blue Systems — Do we want success for ourselves and our organizations with the freedom of others as a byproduct OR do we want freedom for all the people and any organization's success as a byproduct?

In a true libre spirit, it will work out for the Company to become successful by making everyone else successful in what <strong>they</strong> are doing. But once that focus is lost, there’s a world of trouble with the danger of turning into the same "control" angle where everyone else is coming from and stuck in. This is a major difference in the basic thinking of how to run any company. The main goal at Blue Systems is therefore doing what it can to support freedom and opportunity.

Thank you Blue Systems for being partners with the KDE Community.

<h2>Akademy 2012 Tallin, Estonia</h2>
For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.
For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.