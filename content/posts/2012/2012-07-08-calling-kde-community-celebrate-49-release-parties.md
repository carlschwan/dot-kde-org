---
title: "Calling on the KDE Community to Celebrate: 4.9 Release Parties"
date:    2012-07-08
authors:
  - "tcanabrava"
slug:    calling-kde-community-celebrate-49-release-parties
---
The date for the release of the next milestone in KDE's 4.x series is quickly approaching. Developers, testers and bug chasers have been busy putting the final touches on the latest version of our software, so it is once again time to get together and celebrate our community's accomplishment.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ReleaseParty1.jpg" width="250"/><br />A KDE Release Party</div>The 4.9 release of KDE's Plasma Workspaces, Platform and Applications will be issued on August 1st. There's a link in our <a href="http://community.kde.org/Promo/Events/Release_Parties/4.9">wiki</a> listing the planned parties around the world. Feel free to join them, add yours, and spread the word.

So, what are 'release parties' about?

<ul><li>The KDE Community will meet all around the world for a fun time to celebrate the new release.</li>

<li>It's not just a release party. It's a time for the Community to socialize, rejoice together, and share KDE's commitment to freedom.</li>

<li>Anyone can organize a party. Just add it to the wiki and invite people to sign up.</li>

<li>What happens at the party is up to you. Your event could involve anything from meeting for drinks to an all-out event with talks, demos and an Upgrade Fest. Be creative!</li>

<li>Everyone is welcome—contributors, soon-to-be contributors, users, and anyone else who just wants to celebrate.</li>

<li>Release parties are a KDE tradition. Check out <a href="http://dot.kde.org/2010/03/21/kde-partying-around-world-new-release">our article describing some of the events that took place 2 years ago</a> for some interesting ideas.</li>

<li>If you're going to be at a major event (such as <a href="http://www.oscon.com/oscon2012">OSCON</a>) between now and August 1st, wear blue and your KDE paraphernalia. Let people know that August 1st is KDE Release Day!</li>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ReleaseParty2.jpg" width="250" /><br />KDE celebrates another release</div><li>The new KDE software is an excellent way to show off Free Software. Invite people who might be wondering what the excitement is all about.</li></ul>

So go ahead and reserve a table at a local restaurant, shoot out some emails to locals, and show up. Oh... and take pictures, it's always wonderful to see people excited about KDE.