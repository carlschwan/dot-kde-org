---
title: "The DigiKam team meets in Genoa, Italy"
date:    2012-02-22
authors:
  - "mck182"
slug:    digikam-team-meets-genoa-italy
comments:
  - subject: "Pretty cool stuff"
    date: 2012-02-24
    body: "Is this the first DigiKam sprint? The work done on DigiKam is really impressive and it's cool to read such an excellent report on what is coming... I would especially love the face recognition in combination with Nepomuk - as soon as I can use that to pick a face from DigiKam as image in my chat app, in Kontact etcetera - I'm happy :D"
    author: "jospoortvliet"
  - subject: "Incredible what 8 people can"
    date: 2012-02-25
    body: "Incredible what 8 people can do! What could be done if Digikam's team were 25 persons instead 8...\r\nMy most sincere contrats for you great work, ragazzi!"
    author: ""
  - subject: "Cherry on top of the ..."
    date: 2012-02-29
    body: "It would be more productive to spend some time making the kio-camera actually work. In its current state it is completely unusable. Not to mention that after _every_ _update_ Digikam crashes on start and I have to start removing/recreating things so that it starts again. \r\n\r\nSeriously folks, reliability and stability is a couple of orders of magnitude more important than new features.\r\n\r\n"
    author: ""
  - subject: "forum? bug?"
    date: 2012-02-29
    body: "Is there anything about this at forum.kde.org? Have you filed a report at bugs.kde.org?"
    author: "kallecarl"
  - subject: "A quick search in bugs, with"
    date: 2012-02-29
    body: "A quick search in bugs with the keywords \"digikam startup crash\" gives already 6 open bugs, and a total of 68 bugs... It has been documented (and corrected) several times, but as the previous poster said there must be a problem of reliability when something gets broken too easily after a repair.\r\n\r\nFor the kio-camera, there is for example bug 206120, which has no activity in 2.5 years.\r\n"
    author: "alvise"
  - subject: "On the other hand, the number"
    date: 2012-03-02
    body: "On the other hand, the number of bugs fixed by Digikam team each release is huge. Maybe this bug is hard to fix or this functionality has almost no users?"
    author: "jospoortvliet"
---
In the middle of cold January, the team behind <a href="http://www.digikam.org/">DigiKam</a> met in Genoa, the sunny old port city in northern Italy. DigiKam is the <a href="http://www.digikam.org/drupal/about/awards">award-winning</a> KDE photo management application. Participants started gathering in the evening on Thursday 12 January, getting together over dinner to form new friendships and to find out how people's lives were going. The next day, real work started. 

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DigiKamHacking5.jpg" /><br />Real Work</div>

The main focus of this sprint was on optimization, but lots of new features found their places as well. The two lead developers—Gilles Caulier and Marcel Wiesweg—met for the first time with the newest core developer, Francesco Riosa. The core team put their heads together to figure out how to optimize database handling and processes so that performance can be improved. 

Gilles said, "We have talked about separating database information into dedicated files to prevent bloating files with unrelated information about images. This is already the case about thumbnails, which are hosted in a separate database file. I introduced the idea to do the same with fingerprints because this info can grow quickly and it has an influence on database performance, especially with SQLite."

Work then continued on improving even a simple operation such as rotating an image. Until now, the code for rotation was a plugin, and so required different handling. Marcel commented, "This had quite a few shortcomings; some RAW files simply cannot be rotated. For other formats the rotation step involves lossy editing. Building on work which Gilles started, I have integrated the rotation code into DigiKam, reusing our own existing editing framework. Rotation is multithreaded, fully integrated, and can be customized in the setup. It is now possible to rotate by setting a flag in the database, editing the metadata, or by rotating the full pixel data; falling back to the best possible solution if an operation is not supported". (RAW metadata may not be editable; for some formats, rotation by pixel data may involve quality loss.) Thanks to this work, DigiKam also gained multi-threaded metadata editing to go along with multiple-core CPUs, standard these days. "I figured out a way to distribute the work—based on inter-thread Qt signals and slots—to multiple threads without having to change the existing code. The solution works well.", added Marcel.

Francesco worked on internal DigiKam dependencies, mainly porting LCMS to the LCMS version 2 library. <a href="www.littlecms.com">LCMS</a> is a color management system with a small footprint; the API changed significantly between LCMS versions. The porting task is not trivial, but it's important to keep DigiKam in sync for the future. Other library dependencies were improved as well.

Gilles introduced a new progress manager. It informs the user of background activity, including (the new) rotation, scanning directories, DB processing, batch image processing, thumbnail generation and much more. The progress manager presents running tasks in a readable way and allows users to cancel any operation easily. Now even <a href="http://userbase.kde.org/KIPI">kipi-plugins</a> can take advantage of the new progress manager and other plugins already do. Besides the DigiKam collection management tool, Gilles also managed to close 40 bug reports in just 2 days.

"The plan is to stabilize new features with beta releases. We plan 3 betas before an official 2.6.0 release in May.", said Gilles. The <a href="http://digikam.org/drupal/node/643">first beta</a> is already out.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DigiKamDinner.jpg" /><br />Local Food</div>

The hacking room was generously provided by a local FLOSS awareness group called <a href="http://alid.it/">Alid</a> (website in Italian), which focuses on bringing free software to local schools, enabling kids to be familiar with libre software starting from age 6. While the DigiKam team was deep in work, some members of Alid were preparing something for our hungry stomachs. Thanks to their great care, we got to try lots of local Italian specialties.

This was Dhruv Patel's first time in Europe, and, in fact, his first time outside India. Dhruv participated in Season of KDE last year, where he started a QML presentation tool for DigiKam. Dhruv continued the work in Genoa while others gave valuable feedback.

Local developer Daniele and I, both being KDE Telepathy developers, decided to give DigiKam some Instant Messaging love. Daniele wrote a new kipi plugin that allows people to share pictures with their online friends directly via IM file transfer. I explored the possibility to integrate face recognition with Nepomuk and display any semantic data about the recognized person, mainly their online status and a possibility to start chatting directly from the face recognition interface. A working prototype was spawned, but the road to the full feature is still very long (including a complete rewrite of the DigiKam interface with Nepomuk).

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DigiKamGroupPier.jpg" /><br />DigiKam Team in Genoa</div>

Lots of other new features were born in Genoa. Angelo, also a local developer, worked on improving printing of images from DigiKam. Benjamin from France did tremendous work on a panoroma stitching tool, which we tested right away as Genoa offered many nice vistas. It is now easy and straightforward to use—just give it a set of pics, let it do its magic, make the final crop and bam!...panorama picture. Lukasz, a contributor from Poland, worked on the Photo Layouts Editor kipi-plugin. After his sprint work, users are now able to use SVG templates (in addition to transparent PNG and GIF formats) while composing images into a layout.

Lots of bugs were fixed during the intensive work weekend. Everything points to DigiKam 2.6.0 being a great release.

None of this would have been possible if it wasn't for KDE e.V., the main sponsor of this coding sprint. All the participants send a big 'Thank you!' to the <a href="http://jointhegame.kde.org/">supporting members of KDE e.V.</a> for allowing us to make a huge leap forward with DigiKam and to find new friends from all over the world.

<a href="https://plus.google.com/photos/100127992760143250422/albums/5697140051571476737">More photos</a> from the event.

<small><em>Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.</em></small>
