---
title: "KDE Commit-Digest for 7th October 2012"
date:    2012-11-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-7th-october-2012
---
In <a href="http://commit-digest.org/issues/2012-10-07/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kde.org/workspaces/">KDE Workspaces</a> adds preview thumbnail to the color wallpaper configuration dialog for each background mode; more changes around OpenGL compositing</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> offers preview for text documents, speed up of painting with brush</li>
<li><a href="http://edu.kde.org/ktouch/">KTouch</a> updates the handling of localized lesson and course titles</li>
<li>Multiple bugfixes for KDE 4.9.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-10-07/">Read the rest of the Digest here</a>.