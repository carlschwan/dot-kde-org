---
title: "KDE Commit-Digest for 10th June 2012"
date:    2012-07-02
authors:
  - "mrybczyn"
slug:    kde-commit-digest-10th-june-2012
comments:
  - subject: "Alternative authentication, especially using fingerprint readers"
    date: 2012-07-02
    body: "When will KDE, specifically kdm, allow one to login using \"alternative authentication methods\", say, by using the fingerprint reader found on many laptops?\r\n\r\nfingerprint-gui is now at version 1.04 and can allow me to su (sudo), etc., throughout most (if not all) of the KDE \"environment\", yet I cannot log in to KDE via my fingerprint reader.\r\n\r\nThank you,\r\n\r\nKen R\r\n"
    author: ""
  - subject: "KDM supports that"
    date: 2012-07-03
    body: "I am using the fingerprint reader on my system here, so yeah it is working in KDM.\r\n\r\nThe issue is more that you need the pam modules which might not be provided by your distribution."
    author: "mgraesslin"
  - subject: "Figured it out"
    date: 2012-07-05
    body: "NOTE: I'm using fingerprint-gui 1.04 on Gentoo.\r\n\r\nI determined that an \"ENTER\" after the username is entered is required on the KDM/KDE login screen.\r\n\r\nDoesn't matter if in my kdmrc that PreselectUSer=Previous or =None ,\r\nbut, it IS necessary to explicitly set \"FocusPasswd=false\".\r\n\r\nIt wasn't a file in /etc/udev/rules.d/, it's not the presence of /usr/lib64/kde4/libexec/polkit-kde-authentication-agent-1 nor \r\n/usr/share/autostart/polkit-kde-authentication-agent-1.desktop (and the ensuing process), nor SEVERAL other dead-ends, it appears, at least in my case, that an \"ENTER\" after the username is required for the fingerprint-gui gui to appear and prompt for my swipe.\r\n\r\nThanks, your positive response started me back down the road to a little \"forensics\"...\r\n\r\nKen R\r\n(and now, it's Founder's Double Trouble time-not the best, but above mid-pack isn't bad....)\r\n"
    author: ""
---
In <a href="http://commit-digest.org/issues/2012-06-10/">this week's KDE Commit-Digest</a>:

<ul><li>Multiple new features for a non-editable view of animations in <a href="http://www.calligra-suite.org/">Calligra</a>; contour data is created and removed using image analysis from UI and a ClipCommand, a page/slide navigator control for the statusbar is used in Stage and Flow; <a href="http://www.kde.org/applications/office/kexi/">Kexi</a> gets an option for setting default length limit of text values and Kexi forms get Date Picker widget</li>
<li>Pate plugin (plugins in <a href="http://www.python.org/">Python</a>) can add configuration pages to Kate's settings; initial version of Script IDE submitted</li>
<li>Public Transport gains new KDevelop-like TimetableMate GUI, update accessor file format and fixes in a single commit</li>
<li>Faster computation of the shortest path in <a href="http://edu.kde.org/rocs/">Rocs</a></li>
<li>Season of KDE and Google Summer of Code work in several projects, including <a href="http://userbase.kde.org/Plasma">Plasma</a>, <a href="http://edu.kde.org/marble/">Marble</a> and Cantor</li>
<li>Bugfixes in Calligra, <a href="http://amarok.kde.org/">Amarok</a>, <a href="http://konversation.kde.org/">Konversation</a>, Muon and other programs</li>
<li>Amarok developers start giving bug fixes as birthday presents</li>
<li>Bugs status: 438 opened, 419 closed.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-06-10/">Read the rest of the Digest here</a>.
<!--break-->
