---
title: "KDE Commit-Digest for 8th July 2012"
date:    2012-07-21
authors:
  - "mrybczyn"
slug:    kde-commit-digest-8th-july-2012
---
In <a href="http://commit-digest.org/issues/2012-07-08/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.calligra.org/kexi/">Kexi</a>'s data handling library KexiDB became a common component of <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Remote installer implemented in <a href="http://techbase.kde.org/Projects/Plasma/PlasMate">Plasmate</a></li>
<li>Work on the GUI for video slide shows in <a href="http://userbase.kde.org/KIPI">Kipi</a></li>
<!--break-->
<li>Smarter manual save dialog in <a href="http://www.kde.org/applications/graphics/skanlite/">Skanlite</a></li>
<li><a href="http://www.digikam.org/">Digikam</a> can be compiled with paralelized PGF codec</li>
<li>DocumentCursor class added to <a href="http://kate-editor.org/">Kate</a></li>
<li>Activity ranking plugin in its own thread and a separate GlobalShortcuts plugin added to KActivities</li>
<li>Work on recursive move replay in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>In <a href="http://phonon.kde.org/">Phonon-VLC</a>, new videomemorystream is implemented and vdo is based on it now</li>
<li>Performance improvements in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>Important refactoring work in libksane and kdelibs</li>
<li>Bug closing continues: 320 opened, 843 closed.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-07-08/">Read the rest of the Digest here</a>.