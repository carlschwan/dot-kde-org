---
title: "An Opportunity to Contribute to Research and KDE"
date:    2012-08-08
authors:
  - "seele"
slug:    opportunity-contribute-research-and-kde
---
A research team from the University of Maryland Baltimore County has launched an online study to explore the usability of KDE notifications. Participants are asked to describe a recent KDE notification experience to deepen the understanding of what makes a good or bad notification. Results from the research will help improve the usability of KDE notifications, and will make a contribution to the academic field of Human-Computer Interaction.
<!--break-->
<h2>Research Supports KDE</h2>
In addition to collecting information that will contribute to the academic study of interruptions and notifications, this research will help raise money for the KDE e.V., the non-profit organization dedicated to supporting and promoting the work of the KDE Community. 

The research team will donate $1 (US) to KDE e.V. for every valid user experience report up to $1000. Follow-up interviews with selected participants will earn $5 with a maximum of an additional $250. In total, a donation of up to $1250 is possible for participation in this research project. Of course, the results will also help improve the desktop experience of all KDE users.

If you want to help KDE, Academic research and your own computing experience, <a href="https://www.surveygizmo.com/s3/959309/KDE-Notification-User-Experience-Study">join the study</a>!

If this method for engaging with users is successful for this research project, it could encourage more use of KDE as a platform for academic research. This is likely to bring new users, contributors, and technologies to the KDE ecosystem. It could also create a new fundraising framework to help support KDE e.V. activities. 

<h2>You Can Make a Difference</h2>
Find out how to <a href="https://www.surveygizmo.com/s3/959309/KDE-Notification-User-Experience-Study">participate in the study</a>.

More information about the research is available on the <a href="http://community.kde.org/Projects/Usability/KDE_Notification_User_Experience_Study">KDE Community Wiki</a>.

Learn more <a href="http://ev.kde.org/">KDE e.V.</a>. There are a variety of <a href="http://community.kde.org/Getinvolved">opportunities to participate</a> in KDE, and you can directly support KDE e.V. by <a href="http://jointhegame.kde.org/">Joining The Game</a>.  

The first stage of the study will remain open until 1000 valid responses are collected or until August 31, 2012. The second stage of interviews will be conducted until 50 interviews are completed or until September 30, 2012.