---
title: "KDE China User: A KDE Meeting in Beijing"
date:    2012-04-18
authors:
  - "viky leaf"
slug:    kde-china-user-kde-meeting-beijing
comments:
  - subject: "A good start!"
    date: 2012-04-17
    body: "A good start!"
    author: "viky leaf"
  - subject: "Awesome"
    date: 2012-04-18
    body: "Nice to see this KDE meeting in China. Really nice!"
    author: "djszapi"
  - subject: ":)"
    date: 2012-04-18
    body: "Definitely!\r\n\r\nGood luck!!"
    author: "apol"
  - subject: "\"community event\" without community?"
    date: 2012-04-19
    body: "It would be good for events like this to be notified on the <a href=\"https://mail.kde.org/mailman/listinfo/kde-china\">kde-china mailing list</a> or in the #kde-cn IRC channel on freenode.net, where many Chinese KDE users, translators, and community developers appear. We just got news from here. It would be better if more community members were involved in the discussion."
    author: "yue"
  - subject: "It's great"
    date: 2012-04-19
    body: "to hear reports from around the world. But now I'm wondering whether Yue, our Beijing-based Flow mantainer, was present as well."
    author: ""
  - subject: "Great report!"
    date: 2012-04-19
    body: "Thanks for the update from China!  It seems like KDE on Red Flag has been going on forever, but we in the West hear very little about it.  Any chance there will be more reports like this, or blogs?"
    author: "Bille"
  - subject: "Yes, that's true. KDE on Red"
    date: 2012-04-20
    body: "Yes, that's true. KDE on Red Flag has been going on since 1999. You can visit http://www.redflag-linux.com/en/ for more information. Hope more stories happen between them!"
    author: "viky leaf"
  - subject: "Tried to notify other people"
    date: 2012-04-20
    body: "In fact we had notified some organizer at kde-china, but no reply was heard. Maybe we had done something wrong or didn't use the kde-china mailing list.\r\n\r\nThis event is mainly based on the community of RedFlag and Qomo, and our users and fans across the country. In future events, we will try to reach our keen supporters via more approaches.\r\n"
    author: "viky leaf"
---
<div style="float: right; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/DSC_0320.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/DSC_0320_small.jpg" width="240" height="160" alt="Talk at KDE China User meeting"></a><br /> <strong>KDE China User meeting</strong></div>

On March 30th, a grand meeting called <i>KDE China User</i> was held in Beijing. This meeting provided KDE users, community members and technophiles with a nice "face-to-face" opportunity to discuss KDE's latest and cutting edge technology trends. Read on to find out what transpired at the meeting and what the future might hold for KDE in China.
<!--break-->
<h2>About the organization</h2>
The meeting was sponsored by <a href="http://www.redflag-linux.com">RedFlag Linux</a> and hosted by <a href="http://www.linux-ren.org">Linux-ren.org</a> (Chinese). RedFlag is the biggest Linux vendor in Asia and has long been using KDE software by default in its desktop Linux, promoting KDE in China. Linux-ren is the biggest Linux community in China and has its own community release, Qomo Linux, which also uses KDE software. Both organizations have been fostering a great number of KDE software users in China.

<h2>The meeting</h2>
At the meeting, Rick Xing, Director of Linux Desktop Research and Development at Red Flag, gave us a detailed technical overview of KDE. He explained the development history of KDE software, from KDE 1.0 to the latest Plasma Workspaces, Applications and KDE Platform. In particular, he gave an in-depth introduction to the latest KDE software, and gave a live demonstration of the Plasma Desktop workspace and its various visual effects and capabilities.

<div style="float: none; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/DSC_0332.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/DSC_0332_small.jpg" width="500" height="334" alt="Group discussion at KDE China User"></a><br /> <strong>KDE China User discussion</strong></div>

An informal salon discussion took place at the end of the meeting. Rick Xing and Zuo Hongsheng, the leader of Qomo project, had an intensive discussion with participants on topics such as KDE's current situation, how to participate in the KDE community, how to translate KDE news and documents effectively, and how to improve support for the localization and popularity of KDE.

<h2>The future</h2>
The discussion raised eager expectations for Linux-ren.org to assemble resources from the community. People want timely and accurate translations of KDE's official news, technology materials and such. They also want to serve KDE China users better by integrating translated KDE summary information into RedFlag Linux and Qomo Linux. Linux-ren.org is pleased and ready to realize these goals.

Linux-ren.org will continue holding such KDE user meetings in the future, and will bring similar discussions into many universities in China.