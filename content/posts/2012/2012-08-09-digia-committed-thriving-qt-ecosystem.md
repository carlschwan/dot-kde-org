---
title: "Digia Committed to Thriving Qt Ecosystem"
date:    2012-08-09
authors:
  - "nightrose"
slug:    digia-committed-thriving-qt-ecosystem
comments:
  - subject: "What was really acquired?"
    date: 2012-08-09
    body: "Did Digia acquire all the Qt IP? The right to decide what goes in the Free Qt release? And the Qt Commercial patchset be merged into the Free Qt releases?\r\n\r\nIf the answer is yes to all, then it's time to celebrate. "
    author: ""
  - subject: "Super \u00fcber mega great, pure awesome"
    date: 2012-08-09
    body: "Qt rules and Digia is great. This is going to be so awesome. KDE will rule the world."
    author: ""
  - subject: "cautious kudos"
    date: 2012-08-10
    body: "kudos to Digia for taking the time and having the mindset to addressing the KDE community.\r\n\r\nI, for one, welcome our new cute overlords, may our relationship thrive and the benefit be mutual. "
    author: ""
  - subject: "TA"
    date: 2012-08-10
    body: "> The right to decide what goes in the Free Qt release?\r\n\r\nWhat do mean by \"Free Qt release\"? The official open source release of Qt has been maintained by the \"Qt project\" for some time now. See qt-project.org\r\n\r\n> And the Qt Commercial patchset be merged into the Free Qt releases?\r\n\r\nDigia has been merging bugfixes from the \"Qt commercial\" release into the official Qt project tree all the time. Of course, the Qt project is independent and has its own review process, so there may be a time gap until a patch is accepted."
    author: ""
  - subject: "A bright future"
    date: 2012-08-10
    body: "I feel that KDE has a bright future now that Digia has aquired Qt. Clearly, Digia is committed to Qt. And I love their renewed focus on the desktop. Welcome Digia!"
    author: ""
  - subject: "Thanks to Nokia as well"
    date: 2012-08-10
    body: "On the other hand, many thanks to Nokia as well for investing a lot into Qt for years.\r\n\r\nI hope the CLA can be loosened up a bit in the (near) future. That still holds many contributors back from participating to the Qt Project."
    author: "djszapi"
  - subject: "\"thanks to Nokia as well for investing a lot into Qt for years.\""
    date: 2012-08-10
    body: "agreed.\r\n\r\ni doubt QT 5.0 would be where it is today with Nokia pumping resources into the project over the last three (?) years."
    author: ""
  - subject: "Yes, thanks Nokia"
    date: 2012-08-10
    body: "Nokia may have given up and dropped every Qt-based product, but they did invest in Qt when it was needed, and put a lot of money into making Qt better, and making Qt5 awesome."
    author: ""
  - subject: "right to decide what goes in the Free Qt release?"
    date: 2012-08-11
    body: "http://qt-project.org/wiki/The_Qt_Governance_Model\r\n\r\n\"The Qt Project is a meritocratic, consensus-based community interested in Qt.\r\n\r\nAnyone who shares that interest can join the community, participate in its decision making processes, and contribute to Qt\u2019s development.\"\r\n\r\nThis is less about companies than people and their knowledge-area, acceptance and concrete code contributions. The whole governance model is based and focused on solutions to problems, on people. Digia now has more influence but they have less control over whatever rights and contracts than that of the Qt developers who are part of the Qt community, people who are very good at what they do and who are working in the best interest of Qt. \r\n\r\nLike in the past, there will be conflicts, but--like in the past--they will be solved. Compared to before with Trolltech and Nokia, the whole Qt-project has been moved (some time ago already) to that new collaborative governance model. Let's drive Qt forward together :)"
    author: ""
  - subject: "it's simple"
    date: 2012-08-14
    body: "All those spots in Qt that say Copyright XXYY Nokia will now say Copyright XXYY Digia."
    author: "eean"
  - subject: "well"
    date: 2012-08-14
    body: "The copyright assignment is much more important for Digia. For Nokia I suppose it was just to keep copyright ownership clean, but for Digia it's part of their business strategy."
    author: "eean"
  - subject: "Qt-kdelibs merge? | Very good news for the KDE/Qt ecosystem!"
    date: 2012-08-14
    body: "Wow, that sounds really great! Thanks to Digia (and Nokia)!\r\n\r\nThe recent news around Jolla/the Mer Project and Qt make me hope that the already presumed dead MeeGo/Openmoko dream is more alive than it was for a long time: A cross-device GNU+Linux-based FOSS ecosystem (http://www.micuintus.de/2011/02/12/the-death-of-the/).\r\n\r\nAnd I do think it is a good thing for a cross-platform toolkit company like Trolltech (which is currently called \u00bbQt Development Frameworks\u00ab AFAICS) not to belong to a single (or dual) platform vendor like Nokia. Imho the much wider focus of Digia (desktop, RTOSes, iOS/Android) is a huge benefit for the Qt project.\r\n\r\nNow does this letter to the KDE community mean that Digia is actually in favor of the kdelibs->Qt merge, which was suggested around two years ago (http://www.osnews.com/story/23975/Should_kdelibs_Be_Merged_with_Qt_/)?\r\n"
    author: "micu"
---
Following the recent <a href="http://www.digia.com/en/Qt/About-us/News/Digia-to-Acquire-Qt-from-Nokia/">announcement</a> that Digia will be acquiring the complete Qt business from Nokia, Digia has set out its plans in a letter to the KDE Community.

Digia Director of R&D, Tuukka Turunen, sets out their aims of securing the future of Qt as the best cross-platform development framework and seeking greater cooperation with KDE and other partners in the Qt ecosystem.

The full text of the letter is reproduced below.

<blockquote style="font-style: italic;">
Dear KDE Community,

As you may have heard, Digia announced it plans to acquire the Qt technology from Nokia. This transaction secures Qt’s future as the leading cross-platform development framework. It also brings over a part of the Qt team previously at Nokia who, together with the Digia Qt R&D team, will be able to keep developing Qt further.

With this acquisition Digia will be the main company responsible for all of Qt, not just the commercial licensing business. We believe in the power of the Qt dual license. It is a great value for Qt that it can be used under an open source and commercial license. We want to continue the symbiosis with the KDE community and the KDE Free Qt Foundation.

Digia will take forth operating the Qt Project, including hosting the key systems via the Qt Project Foundation. It is very important for us to have a growing number of contributions from different members of the Qt community. We want to work with the entire Qt ecosystem through the Qt Project to make sure Qt will continue to thrive both under commercial and open-source licenses.

Continuing the development of Qt is both a challenge and an opportunity. It will be on the mutual shoulders of the community and Digia to secure the future of Qt as the best possible cross- platform development framework, a challenge we are ready to take on. The KDE community is a key driver and contributor to Qt and we therefore would like to further develop our relationship with you, via an even stronger dialogue and cooperation in the future.

We will continue the work originally set forth by Trolltech over 15 years ago to develop a framework that allows to write code once and deploy it everywhere. We will carry on improving Qt so that both our customers and the open-source users can rely on Digia’s continued investment to provide a development framework that will make their projects successful. We look forward to working with KDE in order to further strengthen and extend Qt’s global reach.

In about a month, the legalities of the acquisition will be concluded. Prior to that, we want to plan things together with you and other key stakeholders of the Qt community. We want to discuss and agree on the future of Qt so that we can all work efficiently together after the transaction is completed.

Yours,
Tuukka Turunen
Director, R&D
</blockquote>

It is great to hear Digia's commitment to Qt and the Qt Project. As lined out in a previous <a href="http://dot.kde.org/2012/07/09/kde-rely-qt-protect-qts-freedom-contribute-it">statement by KDE e.V.</a> we are going to continue to rely on Qt, protect Qt's freedom and contribute to it. Representatives of the KDE Free Qt Foundation and Digia will soon get together to discuss details of Qt's transition from Nokia to Digia.