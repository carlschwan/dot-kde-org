---
title: "5th release of KDE Telepathy Instant Messaging suite"
date:    2012-08-27
authors:
  - "mck182"
slug:    5th-release-kde-telepathy-instant-messaging-suite
comments:
  - subject: "Wow, impressive"
    date: 2012-08-27
    body: "Great work! :-)"
    author: "djszapi"
  - subject: "I'd suggest you to make the"
    date: 2012-08-27
    body: "I'd suggest you to make the mobile phone icon lighter...like http://img594.imageshack.us/img594/1655/ktp051.png\r\n\r\ncompare with the current one: http://dot.kde.org/sites/dot.kde.org/files/ktp05_1.png\r\n\r\nthe mobile phone icon is not so much important like the status, and having the same lightness is distracting and adding noise... lighter imho it's giving the same information without messing with the cleanliness of the client :)\r\n\r\nany ppa for kubuntu? this https://launchpad.net/~telepathy-kde/+archive/ppa still has 0.4.1 :( "
    author: "Anonymous"
  - subject: "IRC"
    date: 2012-08-27
    body: "I am a bit sad about the news on the IRC support. While I understand it to be dropped because of lack of man power, and I also think a good integration of IRC would require a lot of work, and share few things with other protocols, I really hope in the future it will find its way in. It is a must for a full communication experience, at least for the likes of those who read these news :)"
    author: "Gallaecio"
  - subject: "For the future I'd like to "
    date: 2012-08-28
    body: "For the future I'd like to \"merge\" those two icons, ie. have only one presence icon. When the user is on mobile, it will show a presence icon shaped as a mobile. Need to work with Oxygen team on this ;)\r\n\r\nAs for Kubuntu PPA - there are no packages for Precise yet, should be soon though."
    author: "Martin Klapetek"
  - subject: "Compiling the source too hard"
    date: 2012-08-29
    body: "Could you make downloading the code from git and compiling it a little less of a pain? A single 'git clone' would be nice."
    author: "madman"
  - subject: "Krunner commands"
    date: 2012-09-10
    body: "Good progress.\r\n\r\nHowever I do wish when people mention new krunner controls they also mention how to invoke them."
    author: "Blackpaw"
  - subject: "Avoid IRC support"
    date: 2012-12-02
    body: "I think that the idea for avoiding IRC support is definitely a good one. Konversation is quite a stable client (though it doesn't get as much love as Quassel does) and it has the integration that can equate to that of Konversation. I don't see a need, my friends!"
    author: "Jacky Alcine"
---
The KDE Telepathy team is pleased to announce the fifth major release of the new KDE Instant Messaging suite. The immediately available 0.5 version brings polish in many places, better stability, <a href="https://bugs.kde.org/report.cgi?x_axis_field=&y_axis_field=component&z_axis_field=&query_format=report-table&short_desc_type=allwordssubstr&short_desc=&product=telepathy&bug_status=RESOLVED&resolution=FIXED&longdesc_type=allwordssubstr&longdesc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_id=&bug_id_type=anyexact&votes=&votes_type=greaterthaneq&emailtype1=substring&email1=&emailtype2=substring&email2=&emailtype3=substring&email3=&chfield=bug_status&chfieldvalue=&chfieldfrom=2012-06-10&chfieldto=Now&j_top=AND&f0=OP&j0=AND&f1=OP&j2=OR&f3=CP&f4=CP&f5=noop&o5=noop&v5=&format=table&action=wrap">58 reported bugs fixed</a> and some nice new features too, making the instant messaging experience in KDE Workspaces more pleasant and enjoyable. Special attention was given to the log viewer, which received improvements galore.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/ktp05_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/ktp05-wee.png" width="400" height="305" /></a>
</div>

<h3>New Features</h3>

The chatting application was given a message processing plugin system developed as part of this year's Google Summer of Code, featuring an image plugin, which displays a preview of any image link you send or receive. More plugins, like embedding a YouTube videos, LaTeX fomulae and reading incoming messages out loud are in the works and will be available with the next release.

Users now have bigger control over instant messaging using KRunner. You can change your status, add status message, start a chat, audio or video call directly from KRunner.

Contact list is now able to tell you which friends are online on their mobile phones by showing a mobile icon next to the normal presence icon. New mini-list type was added, which aims at showing you as many contacts in the list as possible by using your configured smallest font. Work was also done on improving accessibility.

Calling features have been improved and stabilized. Users are now able to dial-out an arbitrary number with SIP accounts, right from the contact list.

Lots of improvements were done in to group chats. In relation to that, after careful consideration, we have decided to not support IRC in this release. There are plenty of very good IRC clients which can be used in KDE Workspaces and our team is just not big enough to continue working on IRC support. This way we can focus better on filling the missing components in KDE and not duplicating functionality.

The Quick Chat plasmoid also received many updates and has improved a lot, however it still has not left the alpha stage for this release.

Among other things, we have now included a tool for very easy debugging of your Telepathy installation. So if there's anything not working properly, you can get us help you very easily.

<h3>Want to Contribute?</h3>
If you like this project and would like to help out, just send an email to kde-telepathy@kde.org and we'll get you started. And you don't need to be a coding expert, we'll do our best to help you with the basics. So don't hesitate to contact us.

<h3>Getting the Code</h3>
The code is available on KDE's FTP network for <a href="http://download.kde.org/unstable/kde-telepathy/0.5.0/src/">public download</a>. There might be packages for your distribution. If you want to build the code on your own, download all the packages and follow <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/Getting_Set_Up">our guide</a>.