---
title: "KDE Commit-Digest for 12th August 2012"
date:    2012-10-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-12th-august-2012
---
In <a href="http://commit-digest.org/issues/2012-08-12/">this week's KDE Commit-Digest</a>:

<ul><li>Start of rewriting the Maxima backend in <a href="http://edu.kde.org/cantor/">Cantor</a></li>
<li>It is now possible to pause the background manager in <a href="http://kphotoalbum.org/">KPhotoAlbum</a></li>
<li>Refactoring in <a href="http://www.kdenlive.org/">Kdenlive</a></li>
<li>Implemented Latex plugin and Bugzilla, Youtube filter in <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/">KTelepathy</a></li>
<li>Action support for author list in <a href="http://krecipes.sourceforge.net/">KRecipes</a></li>
<li>Bugfixes in KDevPlatform, <a href="http://nepomuk.kde.org/">Nepomuk</a>, <a href="http://www.calligra-suite.org/">Calligra</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-08-12/">Read the rest of the Digest here</a>.