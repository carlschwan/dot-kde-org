---
title: "KDE Commit-Digest for 19th February 2012"
date:    2012-02-29
authors:
  - "mrybczyn"
slug:    kde-commit-digest-19th-february-2012
comments:
  - subject: "rekonq"
    date: 2012-02-29
    body: "I thought rekonq wasn't officially a part of KDE?"
    author: ""
  - subject: "KDEPIM & Nepomuk"
    date: 2012-03-01
    body: "* Work on extended quick search to include fulltext results from Nepomuk in KDE-PIM\r\n\r\n- now THAT is cool! It means that the quick search field above mail folders (which now only searches in the subjects & from) will be able to search the FULL TEXT right away! Awesomeness :D\r\n\r\nCombined with the many improvements in desktop search lately, including some cool new features, things are coming together..."
    author: ""
  - subject: "Wow"
    date: 2012-03-01
    body: "No one seems to have said it in a while, so I'll say what I'm sure many of us are thinking: it's always a pleasure to read these and see the never-ending amazing progress being made. There's a lot of very creative work going on, as well as the impressive progress on bug-fixing.\r\n\r\nThanks to the digest team and all the developers/translators/bug triagers/documentation writers/packagers/etc.\r\n"
    author: ""
  - subject: "Basket is dead, unfortunately"
    date: 2012-03-01
    body: "One year and nothing. Basket devel-list (http://sourceforge.net/mailarchive/forum.php?forum_name=basket-devel) had 20 messages exchanged, mainly about how to contribute. No developing. What a pity. Such a nice app!\r\nMax"
    author: ""
  - subject: "it is because"
    date: 2012-03-01
    body: "It's on the KDE git server :-)"
    author: "Boudewijn Rempt"
  - subject: "re: rekonq"
    date: 2012-03-01
    body: "It's in <a href=\"http://extragear.kde.org/home/about.php\">extragear</a>.\r\n"
    author: "bluelightning"
  - subject: "And to add to that - it's at"
    date: 2012-03-02
    body: "And to add to that - it's at 0.9 now, see http://adjamblog.wordpress.com/2012/03/01/rekonq-0-9-stable/#comments\r\n\r\nThat is good news - having a browser which has proper integration in the desktop is a big improvement over what Firefox and Chromium offer. Both are nice and fast but their UI's are mediocre to bad, offers no integration in the desktop (unless you install plugins which don't work half the time) and they eat loads of memory. Rekonq seems lighter and has proper integration - a big plus."
    author: "jospoortvliet"
  - subject: "Basket is dead, unfortunately"
    date: 2012-03-04
    body: "Indeed, a great app!! I use the 1.81 version and would love to see it maintained and developed further. \r\n"
    author: ""
  - subject: "RE: Basket is dead, unfortunately"
    date: 2012-03-08
    body: "+1 for wanting Basket to continue increasing in awesomeness.\r\n\r\nI am very much impressed with it and would love to continue using it.\r\n\r\nMaybe BasKet could be turned into a service of some sort that Kate/Kmail/Akonadi/Nepomuk could interact with?  Keeping track of miscellaneous notes is a regular part of the usual \"Desktop Experience\" and making BasKet-like features easy to access would be a great benefit to me."
    author: ""
---
In <a href="http://commit-digest.org/issues/2012-02-19/">this week's KDE Commit-Digest</a>:

<ul>
<li>Addition of a video wall script in KDE-Workspace</li>
<li>Support of third party window switchers by a KCM service that finds all available window switcher layouts</li>
<li>Work on extended quick search to include fulltext results from <a href="http://nepomuk.kde.org/">Nepomuk</a> in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>Improved Nepomuk feeder throttling</li>
<li>Improvements in <a href="http://www.kexi-project.org/">Kexi</a> Feedback Agent</li>
<li>Astromobile subproject has been added to <a href="http://simon-listens.org/">Simon</a></li>
<li>Audio alignment works in <a href="http://www.kdenlive.org/">Kdenlive</a></li>
<li>New History Manager in <a href="http://rekonq.kde.org/">rekonq</a></li>
<li>Addition of FileDeleter which manages the removal of files in <a href="http://www.kde.org/applications/internet/kget/">KGet</a></li>
<li>Work on history and state saving and management in <a href="http://games.kde.org/game.php?game=kpat">KPatience</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-02-19/">Read the rest of the Digest here</a>.