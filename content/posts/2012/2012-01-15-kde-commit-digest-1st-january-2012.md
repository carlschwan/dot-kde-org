---
title: "KDE Commit-Digest for 1st January 2012"
date:    2012-01-15
authors:
  - "mrybczyn"
slug:    kde-commit-digest-1st-january-2012
comments:
  - subject: "Statistics broken?"
    date: 2012-01-16
    body: "The statistics seem to be broken:\r\n<cite>\r\nCommits \t0 by 0 developers\r\nBugs Opened \t0 in the last 7 days\r\nBugs Closed \t0 in the last 7 days\r\n</cite>\r\n"
    author: ""
  - subject: "yes, we are working on it"
    date: 2012-01-24
    body: "Thanks, we are aware of it!"
    author: "vladislavb"
---
In <a href="http://commit-digest.org/issues/2012-01-01/">this week's KDE Commit-Digest</a>:

<ul>
<li>Work on inter-file search in <a href="http://userbase.kde.org/Lokalize">Lokalize</a></li>
<li>New weather services: geonames and favorite weather station</li>
<li>Google Code-in work on <a href="http://edu.kde.org/marble/">Marble</a>; plugins integrated</li>
<li><a href="http://www.mltframework.org/twiki/bin/view/MLT/WebHome">MLT</a> clip analysis and freesound.org integration in <a href="http://userbase.kde.org/Kdenlive">Kdenlive</a></li>
<li>Usability improvements in <a href="http://userbase.kde.org/NetworkManagement">Network Management</a></li>
</ul>

<a href="http://commit-digest.org/issues/2012-01-01/">Read the rest of the Digest here</a>.