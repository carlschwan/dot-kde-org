---
title: "KDE Ships April Updates to Plasma Workspaces, Applications and Platform"
date:    2012-04-04
authors:
  - "sebas"
slug:    kde-ships-april-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "Keep on rocking!"
    date: 2012-04-05
    body: "<strong>\\o/</strong>\r\n"
    author: "jankusanagi"
  - subject: "4.8 is an awsome release. I"
    date: 2012-04-05
    body: "4.8 is an awsome release. I am downloading the update as I am writing this."
    author: ""
  - subject: "Pages don't render correctly"
    date: 2012-04-06
    body: "Neither\r\nhttp://www.kde.org/announcements/announce-4.8.2.php\r\nnor \r\nhttp://www.kde.org/announcements/announce-4.8.1.php\r\nrender correctly with Internet Explorer 9!"
    author: ""
  - subject: "Broken browser dude"
    date: 2012-04-06
    body: "Broken browser dude, report a bug to Microsoft and wait until they fix it.\r\n\r\n"
    author: "jospoortvliet"
  - subject: "Keep going...keep stableness.."
    date: 2012-04-06
    body: "i just changed from unity to kde 4.8.1. excellent DE. looks stable."
    author: ""
  - subject: "Complaints alone don't fix anything"
    date: 2012-04-06
    body: "Based on these and your earlier complaints, these sites have been checked on IE9 and FireFox 11 and higher with no rendering problems. Previously someone requested that you post screenshots, and there was no response. \r\n\r\nIf you can't help solve the problem, you'll have to live with it."
    author: "kallecarl"
---
Today KDE <a href="http://www.kde.org/announcements/announce-4.8.2.php">released</a> updates for its Workspaces, Applications, and Development Platform.
These updates are the second in a series of monthly stabilization updates to the 4.8 series. 4.8.2 updates bring many bugfixes and translation updates on top of the latest edition in the 4.8 series and are recommended updates for everyone running 4.8.1 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
<br /><br />
Significant bugfixes include making encryption of multiple folders using GPG work, XRender fixes in the KWin window and compositing manager, a series of bugfixes to the newly introduced Dolphin view engine and improvements in the Plasma Quick-based new window switcher, which made its first appearance in 4.8.0. Kontact and its device counterpart Kontact Touch have received a number of important bugfixes as well as performance improvements.
 The <a href="http://www.kde.org/announcements/changelogs/changelog4_8_1to4_8_2.php">changelog</a> and <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.8.2&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a> list more, but not all improvements since 4.8.1.
Note that these changelogs are incomplete. For a complete list of changes that went into 4.8.2, you can browse the Subversion and Git logs. 4.8.2 also ships a more complete set of translations for many of the 55+ supported languages.
To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.8.2.php">4.8.2 Info Page</a>. If you would like to find out more about the KDE Workspaces and Applications 4.8, please refer to the <a href="http://www.kde.org/announcements/4.8/">4.8 release notes</a> and its earlier versions.
<!--break-->
