---
title: "Release 4.8: Features, Stability, Performance"
date:    2012-01-25
authors:
  - "sebas"
slug:    release-48-features-stability-performance
comments:
  - subject: "Thank you!"
    date: 2012-01-25
    body: "That's a really nice gift for my birthday!"
    author: "jfebrer"
  - subject: "ksmserver"
    date: 2012-01-25
    body: "Installed the 4.8.0 update (rpm's for opensuse 11.4). I now get an error window at login stating that the ksmserver cannot be started."
    author: "trixl."
  - subject: "support"
    date: 2012-01-25
    body: "The best place to get support is at forum.kde.org. Software > Workspace > Plasma is probably the best location."
    author: "kallecarl"
  - subject: "The same for me!"
    date: 2012-01-26
    body: "My birthday was on January 19th, and that day I had the 4.8.0 pre-builds!\r\n\r\nLove this release."
    author: "Alejandro Nova"
  - subject: "nepomuk"
    date: 2012-01-26
    body: "This is the release in which nepomuk finally works as intended, fast and reliable. Time for every application to take advantage of it."
    author: "nickkefi"
  - subject: "performance ?"
    date: 2012-01-26
    body: "In opensuse 12.1, I installed kde 4.8 from the release, 4.8 repository. KDE startup takes 25-30 secs now. With 4.7.4 from factory, it used to start up in 10-15 secs ( sometimes < 10 secs) from cold boot.\r\n\r\n4.8 seems to have hinting turned off and the fonts look OK. Esp the serif fonts - they look better than with hinting."
    author: "mvaar"
  - subject: "Allow me to discus quality"
    date: 2012-01-26
    body: "First of all, congratulations! This is another good upgrade, and it has some lovely things. For example: Gwenview's effects (really on par with modern requirements), Dolphin's new view and the power settings are a nice treat. I also love the \"email indexing\" checkbox in the desktop search settings.\r\n\r\nHowever, something keeps bothering me. That's the perceived quality. I wonder how it can be improved.\r\n\r\nWithin mere 2 hours, I managed to find so many little nitpicks. Allow me to give some examples:\r\n\r\n- Dragging a folder from Dolphin to Plasma crashes the desktop\r\n- my Akonadi now displays ghost connectors. During the upgrade I didn't have the akonadi-google and akonadi-facebook packages installed. I recreated them, and in Kontact I see both the old and new config.\r\n- KDM themes: new ones retrieved with GHNS don't show up in the list of themes.\r\n- Bluetooth: I press \"Enable bluetooth\" in the menu. Nothing happens, no feedback, bluetooth still disabled.\r\n- opening KInfoCenter from Kickoff: it crashed. (not from konsole, strangely enough)\r\n- openSUSE specific: KDM theme is gone\r\n\r\nThat's just by clicking around in a newly upgraded system, to see what's changed. Some of the things I'm wondering about are:\r\n- would people be interested in having a test group that clicks through the new releases?\r\n- is there anything to do in the dev process? In my experience, details are ignored in a rush to release/freeze moment. Better not rush, and pay attention to details.\r\n\r\nOff course, the next release will be better, and the next one after, etc.. yet this keeps coming back. Is there a way to make a leap forward for this?\r\n\r\n\r\nApart from the bugs, there are a few other things I would love to see improved. I hope this gives some inspiration:\r\n\r\n1. Plasma can use more fluidity (QML?):\r\n- Switching between \"add widgets\" / \"add activity\" -> toolbar bumps/flickers\r\n- Adding a widget by dragging -> often not possible, at once there is a space. It's very bumpy. If you take the OSX Dock for example, icons float to make room.\r\n- Dragging a applet from taskbar to desktop is happens in jumpy bumpy way. Try dragging a folder to your taskbar, drag it to the desktop, drag it back. It's really hard.\r\n\r\n2. System settings.\r\nThis can use more further overall finetuning:\r\n- Why does the \"sharing\" module only have 2 textfields for example?\r\n- Why does every module use a different layout? (even within some modules, like \"standard applications\" there is inconsistency)\r\n- Can the \"device not found / supported\" message be the same everywhere? It almost is :P\r\n- Why are some modules the first icon in the left navigation? (basically sorted A-Z, e.g. for shortcuts, and multimedia)\r\n- Why are there 2 modules for appearance? (please not the tech argument for kdelibs vs kdebase ;)\r\n- Why is file associations the 2nd module in the list?\r\n\r\nSystemsettings is getting pretty good, but it's not really there yet. It's an app everyone uses, and I think it should be a hallmark of KDE.\r\n\r\nThanks for reading, I hope this post was useful as feedback and inspiration on where to improve things further."
    author: "vdboor"
  - subject: "Does it also happen after a"
    date: 2012-01-26
    body: "Does it also happen after a logout / login / reboot? After the first upgrade, KDE may need some time rescanning the .desktop file changes."
    author: "vdboor"
  - subject: "\"KDE startup takes 25-30 secs"
    date: 2012-01-26
    body: "\"KDE startup takes 25-30 secs now\"\r\nMaybe just the first one after the update or every time?"
    author: "nickkefi"
  - subject: "no/no/yes"
    date: 2012-01-26
    body: "After logout/login, it is quick 5-7 secs. After a reboot, always takes longer. \r\n\r\nKDE 4.7.4 was starting quick after reboot esp the factory version (probably closer to 4.7.5). The release 4.7.4 is again slow (bit not as slow as 4.8).\r\n\r\nI tried removing .kde4 dir, and all the stuff under /tmp. Then rebooting twice to see if configuration was the problem but even with clean config, it is slow."
    author: "mvaar"
  - subject: "forum material"
    date: 2012-01-26
    body: "sounds like a conversation that would be helpful at forum.kde.org.\r\n\r\nUsers don't really have a good way to find solutions in the comments of Dot stories."
    author: "kallecarl"
  - subject: "-> bugs.kde.org"
    date: 2012-01-26
    body: "Well I hope you reported all these in the proper place http://bugs.kde.org.\r\n\r\nBugs there get noticed, blog post comments will not be remembered."
    author: "David Edmundson"
  - subject: "distro forums too"
    date: 2012-01-26
    body: "Considering that issues might be distro or packaging related...\r\n\r\nIt would be good to use distro forums in addition to forum.kde.org. Might help others too."
    author: "kallecarl"
  - subject: "Good idea"
    date: 2012-01-27
    body: "Its the little things, and as KDE gets better and better, think we all agree on that, its going to be in the little things that will make all the difference.\r\n\"- would people be interested in having a test group that clicks through the new releases?\" Seams like a really good idea, we would need to provide such a group with builds and some time frame for evaluation, the group would create a sensible list of things to address, and we would communicate that to the several people that would be required to intervene.\r\n\r\nIts not about bugs per si, it would be about quality making things more polished here and there. After a few sickles I'm sure the perceived quality would see visible improvements..."
    author: "pinheiro"
  - subject: "no, these are not \"bugs\""
    date: 2012-01-27
    body: "in the conventional sense. I see a lot of issues with this version. The eye candy may have improved but the quality is not up to KDE standards.\r\n\r\nKde wallet has stopped working. My WPA keys are not being retrieved and I cannot use wifi! The wallet manager keeps asking password for every action, even after correct password is entered. This also happened in later point releases of 4.7 ( I believe 4.7.2/3).\r\n\r\nAnd nepomuk has pretty much made the desktop unusable. I finally managed to turn it off but even after it is off, every time I select nepomuk applet and try to bring up the context menu, it takes several minutes to respond.\r\n"
    author: "mvaar"
  - subject: "More to the story?"
    date: 2012-01-27
    body: "Those certainly sound like bugs, so it's not clear why you say that they are not bugs.\r\n\r\nThere must be more to your situation than the latest release of KDE. I started using 4.8 with the first RC. And have upgraded to the final release version. I have none of those issues.\r\n-KWallet works flawlessly\r\n-Wireless works flawlessly\r\n-Nepomuk keeps an index of everything I've selected and runs without a hitch.\r\nIn fact, the whole system works like a charm. And I see very little change in the appearance...certainly no gratuitous new eyecandy. Unless snappy performance or the Dolphin snazziness are eyecandy.\r\n\r\nAre these problems addressed in the forum for your distribution? Or at forum.kde.org? Those are places where problems can be resolved. Maybe you'll help someone else who has similar experiences in the bargain.\r\n\r\nIt doesn't really improve anything to complain in Dot comments about things that don't work for you. "
    author: "kallecarl"
  - subject: "it illustrates something"
    date: 2012-01-27
    body: "The fact that these examples (and mine above) are posted here illustrate an other problem. Something more global we'd like to address, instead of individual issues. Those are fine for a bug tracker. Stearing the project in new directions does not happen from bugzilla though. That needs discussions on places like these (and the mailing lists to get more in-depth approaches)"
    author: "vdboor"
  - subject: "Understood, and..."
    date: 2012-01-27
    body: "Your comment and \"no, these are not \"bugs'\" (above) clearly state something about KDE quality. \r\n\r\nYet the specifics of the latter indicate bug or forum material, particularly in light of many other installations with no such quality complaints. So individual issues are what is being communicated...at least in the \"not bugs\" comment...under a \"KDE quality\" false flag. That comment leaves the impression that Release 4.8 is mostly about eye candy, which is simply not true.\r\n\r\nYou have suggested ways that KDE quality can be improved, and they will probably be considered by KDE developers. (I'm not one and don't speak for them.) However, reporting bugs and participating in applicable forums are ways people can _actively_ affect quality."
    author: "kallecarl"
  - subject: "Actually, there is only one sore spot left."
    date: 2012-01-27
    body: "Akonadi-Nepomuk Feeder, specifically the mail indexing part. You can't disable the mail indexer and the contact indexer (the UI will set the right option in akonadi-nepomuk-feederrc but akonadi_nepomuk_feeder won't acknowledge it), and it will index mail by ~1 day (if you have 40,000 of them like I do). Also, if you have lots of contacts (like anyone with the Google Contact Feeder and the Facebook Akonadi Resource has), you will suffer.\r\n\r\nPerhaps we should organize another \"Akonadi-Nepomuk Feeder sprint\" to seek and destroy all the remaining bugs in Akonadi-Nepomuk Feeder."
    author: "Alejandro Nova"
  - subject: "Check for distro bugs."
    date: 2012-01-27
    body: "Not all Nepomuk bugs are upstream bugs. Use a distro that does not amplify upstream issues by using ancient packages or unsupported mixes. Chakra has proved to be a good choice for conscious KDE testing.\r\n\r\nIn fact, I filed a bug requesting the proper handling of KDE packages in Kubuntu in Launchpad. It has gone unanswered.\r\n"
    author: "Alejandro Nova"
  - subject: "disable mail indexer"
    date: 2012-01-28
    body: "In Kubuntu 11.10, there is a checkbox option to enable/disable Email Indexer. System Settings > Desktop Search"
    author: "kallecarl"
  - subject: "curious about bug report"
    date: 2012-01-28
    body: "Second the motion to check distros. Forum.kde.org and distro forums are outstanding for getting problems resolved and for helping others.\r\n\r\nIf that truly is the bug you reported, it wouldn't be a surprise that it hasn't been answered. Seems like a tall order.\r\n\r\nKubuntu packaging works fine on 2 computers here and for the machines I support for a few friends. On the other hand, Chakra sounds like a mighty fine distro (http://chakra-linux.org/wiki/index.php/Why_We_Love_Chakra). From the Chakra site...\"In my opinion KDE is the finest desktop environment in the world, for any platform, and Chakra provides the smoothest, fastest KDE desktop experience I've ever encountered.\" Wow! Chakra for President.  "
    author: "kallecarl"
  - subject: "First of all thanks very much"
    date: 2012-01-28
    body: "First of all thanks very much for this release guys. KDE is really cool!\r\nIt seems to be a very stable and polished release but I still have two complaints:\r\n\r\nI'm still having trouble with Kmail2, especially the migrator.\r\n\r\nThe other thing is the start up. I would like it to start faster.\r\n\r\nIf these two things could be changed then KDE would be perfect in my eyes ;)"
    author: "Bobby"
  - subject: "KDE Spotlight Alternative?"
    date: 2012-01-28
    body: "I love Apple's desktop-wide search feature called Spotlight.  The search applet let's you find programs and files from the convenience of your toolbar.  I was hoping this same functionality would be available with the development of KDE Nepomuk and Plasma, but I haven't found any way to set it up.\r\n\r\nIs anything like this possible with this new release of KDE?"
    author: "batonac"
  - subject: "KMail works again :-)"
    date: 2012-01-29
    body: "Since I upgraded to the \"new and better\" KMail solution in July my e-mail archive has been unaccessible to me. I know there are many users who have suffered my problems.\r\n\r\nThe good news is: tonight, after upgrading to 4.8 I could start KMail, and it downloaded all my e-mail and has all my old e-mail accessible. My hat off and a thousand times \"thank you\" to whoever has fixed everything!"
    author: "kjetil-kilhavn"
  - subject: "KRunner or kickoff launcher search?"
    date: 2012-01-29
    body: "The search bar in the kickoff launcher sounds similar.\r\n\r\nKRunner is also a similar search and launch toolset. Alt+F2 drops KRunner (with a search box) from the top of the screen. It does what you describe and more. The wrench icon lets you select additional functions and search parameters. The graph shows system activity. "
    author: "kallecarl"
  - subject: "Make sure you have enabled"
    date: 2012-01-29
    body: "Make sure you have enabled Nepomuk File Indexer in System Settigs and the Nepomuk runner in KRunner's configuration.\r\n\r\nPersonally I prefer fsrunner (http://kde-apps.org/content/show.php/?content=100057) because it shows the path to the files, which lets me differentiate between files and folders with the same name. It only searches filenames though."
    author: "Hans"
  - subject: "Unfortunately, not even close"
    date: 2012-01-29
    body: "Unfortunately, not even close :(\r\n\r\nIf you are unfamiliar with OS X, here's a basic example:\r\n\r\nhttp://youtu.be/yQnMUMm8DGE\r\n\r\nor in Finder\r\n\r\nhttp://youtu.be/FvZZPK7OAr0"
    author: "gedgon"
  - subject: "You are right about that."
    date: 2012-02-01
    body: "You are right about that. Nepomuk does have the potential but the feature in Mac OS is more advanced, more refined and definitely cooler. It would be nice if we had something like that in KDE.\r\n\r\nOf course I can find my stuff in KDE but a feature like spotlight would be good for the desktop flair."
    author: "Bobby"
  - subject: "Agreed on forum material!"
    date: 2012-02-01
    body: "Hi, your comment about being forum material makes much sense to me. When time allows me, I'll see what I can come up with. This is good to consider for other dot stories as well. Using the forum simply didn't occur to me before."
    author: "vdboor"
  - subject: "The things you mention are"
    date: 2012-02-14
    body: "The things you mention are very much related to 'polish'. They depend on:\r\nproper (in-depth) testing & Q&A\r\npeople fixing those issues\r\n\r\nBoth are missing. Ok, we get quite a number of people testing, but the number of bug reports coming in is sometimes so big (and the quality so low) that developers get over-run and can't keep up.\r\n\r\nThe second, polish - fixing these small things is often far less interesting to do in your free time. This used to have people paid to fix those things: at SUSE. But SUSE doesn't do desktop anymore (and since the Novell takeover it would've been GNOME anyway). GNOME still has a company doing this: Red Hat. That's why, despite technically being behind the curve a bit, it has the 'polish'. (Canonical also does a bit on GNOME but they not always contribute upstream).\r\n\r\nSo how can we solve this? Well, of course if more people test & clean up bugzilla; and if more people fix the boring-yet-important bugs, things will get better. Another way is to wait for a .3 or .4 release until you adopt a new release: we do continue to fix those 'small' things for a couple of months. See for example how KMail has progressed between the 4.7.0 and 4.7.4 release!\r\n\r\nAnother way is to find money for fixing those things. Maybe the Spark initiative will be able to find that... I'm keeping my fingers crossed for that one, personally."
    author: "jospoortvliet"
  - subject: "Well, the technology is there"
    date: 2012-02-14
    body: "Well, the technology is there - technically, Nepomuk should even be able to do quite a few things Spotlight can't. But in reality the GUI is simply not there and it can use some optimizations here and there..."
    author: "jospoortvliet"
---
The latest set of KDE releases has been announced. It includes major updates to Plasma Workspaces, Applications, and the Development Platform. Version 4.8 provides many new features, as well as improved stability and performance. Check out the highlights below and read <a href="http://www.kde.org/announcements/4.8/">the full announcement</a>.
<div style="float: right; padding: 1ex; margin-top: 1ex; margin-right: 0px; margin-bottom: 3ex; margin-left: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/plasma-desktop-4.8B.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma-desktop-4.8525.png" /></a><br />Dolphin gets a new display engine (click for larger)</div>
<!--break-->
<h3>Adaptive Power Management in Plasma Workspaces</h3>

Plasma Workspaces highlights include <a href="http://philipp.knechtges.com/?p=10">KWin optimizations</a>, power management redesign, and integration with Activities. <a href="http://www.kde.org/announcements/4.8/plasma.php">Read more...</a>

<h3>Faster, More Scalable File Management</h3>

KDE applications released today include Dolphin with its new display engine and semantic goodies, new Kate features and improvements, and Gwenview enhancements. Enjoy new Marble features such as interactive Elevation Profile, satellite tracking and Krunner integration. <a href="http://www.kde.org/announcements/4.8/applications.php">Read more...</a>

<h3>More Stability, Performance, Features in the KDE Platform</h3>

KDE Platform provides the foundation for KDE software. Platform 4.8 brings easier user interface development and substantial improvement in password management. KDE Telepathy framework reaches its first beta milestone. Resolution of numerous bugs means that stability and performance continue to improve. <a href="http://www.kde.org/announcements/4.8/platform.php">Read more...</a>

<h3>Spread the Word and See What's Happening: Tag as "KDE"</h3>
Even though the KDE team has plenty of developers, non-technical users are also critical to success. <a href="https://bugs.kde.org/">Report bugs</a>. Encourage others to join the <a href="http://community.kde.org/Getinvolved">KDE Community</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, Vimeo and others. Please tag uploaded materials with "KDE" to make them easy to find, and so that the KDE promo team can analyze coverage for the 4.8 releases.

You can follow what is happening on the social web on the KDE live feed. This site aggregates real-time activity on identi.ca, twitter, youtube, flickr, picasaweb, blogs and other social networking sites. The live feed can be found on <a href="http://buzz.kde.org">buzz.kde.org</a>.

Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.