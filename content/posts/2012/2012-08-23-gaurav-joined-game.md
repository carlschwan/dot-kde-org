---
title: "Gaurav Joined the Game"
date:    2012-08-23
authors:
  - "jaysonr"
slug:    gaurav-joined-game
comments:
  - subject: "Is there an official email address for jtg?"
    date: 2012-08-24
    body: "I joined in the beginning of the programme, but apart from the welcome package I never received any newsletter or invitations. Who do I contact for fixing this?"
    author: "mark"
  - subject: "Contact Claudia"
    date: 2012-08-25
    body: "Hey :)\r\n\r\nPlease Contact Claudia (rauch at kde org) to get this fixed. She'll be able to help you out."
    author: "nightrose"
  - subject: "More information, more dev, less marketing"
    date: 2012-08-27
    body: "I left the game because I didn't really know for what the money was spent.\r\nThe  Quarterly Reports didn't have enough information, just stuff like \"Randa Sprint: 10'000\" (and now, what was done there, where did you spend the money?, ...).\r\nWhen I spend money I'd like to know for what or being able to choose for what. I don't want to support marketing/promotion stuff, just development. I will spend my money on Open Source Kickstarter project where they tell me what exactly is done with the money.\r\n\r\nI also never heard about AGM, or maybe I misunderstood it and thought only active contributors (contributing code, documentation, ...) can take part."
    author: "Lu"
  - subject: "We're actively looking for"
    date: 2012-08-27
    body: "We're actively looking for this type of feedback to make sure that people understand the benefits of members and that members have transparency on how funds are used.  You can provide this information to Claudia (per previous thread) and maybe one day will rejoin."
    author: "WadeJOlson"
---
In June 2010, <a href="http://ev.kde.org">KDE e.V.</a> launched the <a href="http://dot.kde.org/2010/06/07/join-kde-game-linuxtag-2010">individual supporting membership program</a>, asking people to <a href="http://jointhegame.kde.org/">Join the Game</a>.

There are many reasons to support KDE with a regular financial contribution. It is important to KDE e.V. by helping to create a predictable income. This money is used to support events that accelerate development of KDE software, enhance promotion efforts and help grow the Community. KDE contributors and users are scattered throughout the world and have many different backgrounds, so their reasons for contributing are diverse. Claudia Rauch and Jayson Rowe from the Join the Game Team asked supporting member <strong>Gaurav Chaturvedi</strong> why he joined the game.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/gaurav300.jpg" /><br /><strong>Gaurav Chaturvedi</strong></div>

<strong>Jayson:</strong> Hi Gaurav, please introduce yourself to our readers.

<strong>Gaurav:</strong> My name is Gaurav Chaturvedi (Tazz on IRC), I live in Mumbai, India. I work here as a System Administrator for a web hosting / domain selling company.

<strong>Jayson:</strong> How long have you used KDE software?

<strong>Gaurav:</strong> I use KDE software on my primary desktop. I have been using KDE since the KDE 3.4 days.

<strong>Jayson:</strong> How and when did you discover KDE Software and the KDE community?

<strong>Gaurav:</strong> I think I discovered KDE when I tried out Knoppix for the first time. This was around 2006, I think. I was already on IRC and I wanted to get involved. So someone suggested I go search for pradeepto, so I did a /whois for pradeepto. And that's how I landed in #kde-in [on Freenode] :) [Editor note: Pradeepto Bhattacharya is a recent addition to the KDE e.V. Board. He has been active in organizing KDE India.]

<strong>Jayson:</strong> Why did you join the game?

<strong>Gaurav:</strong> Well, I have gotten a lot from the KDE Community and I just wanted to give something back.

<strong>Claudia:</strong> What did you get from the Community, how would you describe that experience?

<strong>Gaurav:</strong> Apart from the awesomeness that is KDE software, the community as a whole is very warm, welcoming and helpful. I met a lot of great people because of the KDE community, and now they are good friends of mine. We also have a bunch of crazies hanging out in the "quasi-secret twin channel of #kde-devel" come say 'hi'. ;)

<strong>Jayson:</strong> How did you find out about the KDE e.V. 'Join the Game' individual supporting membership program?

<strong>Gaurav:</strong> I found out about 'Join the Game' when Jos [Poortvliet] blogged about it. I remember reading about it on <a href="http://dot.kde.org">the Dot</a>. 

<strong>Jayson:</strong> Do you make any other contributions to KDE besides your financial one?

<strong>Gaurav:</strong> None recently. Long time ago I started off working with <a href="http://www.englishbreakfastnetwork.org/">The English Breakfast Network (EBN)</a>, I submitted patches to 'annma', Anne-Marie Mahfouf from the KDE Edu team. Till she got fed up with me, and helped me get an SVN account. Thats when I started breaking trunk. :)

Later on I wrote documentation for a couple of small KDE apps. (kget and kmix). I also did a Google Summer of Code project with KDE. These days, my non-KDE related day job takes up all my time.

<strong>Jayson:</strong> What are your favorite KDE applications? What applications do you think could most be improved?

<strong>Gaurav:</strong> My favorite KDE applications would be konsole, yakuake, kate, amarok, digikam. No particular KDE application comes to mind, but then there is always room for improvement, in all the applications.

<strong>Jayson:</strong> Can you describe your role or an impact as a KDE e.V. player (supporting member) in the Game?

<strong>Gaurav:</strong> I think my role as a KDE e.V. supporting member is to help get KDE e.V. the financial backing it needs, and then to promote "Join the Game" and enable others to become KDE e.V. supporting members.

<strong>Jayson:</strong> We provide reports about KDE's activities first-hand for our members. Would you like to learn more about KDE e.V. support for KDE Community?

<strong>Gaurav:</strong> I think in its current form the Quarterly Reports serve the purpose well.

<strong>Claudia:</strong> Any specific topics that would interest you?

<strong>Gaurav:</strong> Oh yes. In the quarterly reports, I always keep an eye out for all the developer sprints and the various activities that are supported by KDE e.V. for the popularization of KDE software and our community.

<strong>Jayson:</strong> Do you have any ideas how KDE e.V. could improve the promotion of the 'Join the Game' program?

<strong>Gaurav:</strong> One of the benefits for supporting members is that they can attend the KDE e.V. Annual General Meeting (AGM). This was a really insightful experience for me. It helped me understand how KDE really functions, how decisions are taken. It also motivated me to help KDE more. Maybe the ability to attend the AGM should be advertised a bit more.

Thank you Gaurav for sharing your thoughts with us.

Learn more about <a href="http://jointhegame.kde.org/">Join the Game</a> from previous interviews with <a href="http://dot.kde.org/2011/01/11/when-will-you-join-game">Paul Eggleton</a> and <a href="http://dot.kde.org/2011/08/11/joining-game-desktop-summit">Jonathan Kolberg</a>.
