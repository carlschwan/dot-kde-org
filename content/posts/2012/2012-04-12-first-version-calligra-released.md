---
title: "The First Version of Calligra Released"
date:    2012-04-12
authors:
  - "ingwa"
slug:    first-version-calligra-released
comments:
  - subject: "congratulations!"
    date: 2012-04-12
    body: "congratulations!  "
    author: ""
  - subject: "Hooray!"
    date: 2012-04-13
    body: "This is very exciting, I can't wait to try it."
    author: ""
  - subject: "Great news, But site is down... (13.04.2012 11:14 GMT -1)"
    date: 2012-04-13
    body: "I am specially excited to try out Braindumpnote. "
    author: ""
  - subject: "yup, site down for me too"
    date: 2012-04-13
    body: "The site is down for me too, I guess they're having a lot more traffic than usual.\r\n\r\nAnd yes, braindumpnote is exciting - there's still a lot to do in the UI department but the basics are very cool and due to the framework it's build upon it can do a lot already.\r\n\r\nThe idea of Calligra being 'the webkit of office', which is how the developers describe it, is a very interesting one. It would be interesting to see more third parties attempt to build an custom UI on top of the Calligra core."
    author: "jospoortvliet"
  - subject: "Good news"
    date: 2012-04-13
    body: "Good news, I was looking forward to Calligra Flow release. Thanks to Calligra development team!"
    author: ""
---
<img src="http://people.canonical.com/~jriddell/calligra-logo-transparent-for-light-600.png" width="436" height="282" />
The Calligra team has <a href="http://www.calligra.org/news/calligra-2-4-released/">announced</a> the first release ever of the Calligra suite. This marks the end of a long development period lasting almost one and a half year. It is the first release in a long series which is planned to make improved applications every 4 months.

<a href="http://www.calligra.org/tour/calligra-suite/">Take the tour</a> to discover some of the apps of Calligra.</a>

<div style="width: 510px; border: solid thin grey; margin: 1ex; padding: 1ex"><a href="http://www.calligra.org/news/calligra-2-4-released/attachment/krita-screen_01_davidrevoy/"><img src="http://www.calligra.org/wp-content/uploads/2012/04/krita-screen_01_davidrevoy-500x301.jpg" alt="" width="500" height="301" /></a><p class="wp-caption-text">Krita 2.4 with a piece of art by David Revoy</p></div>

<h2>Introduction to Calligra</h2>

Calligra is an integrated suite of applications that cover office, creative and management needs. It is the most comprehensive free suite of applications anywhere and we expect the number of applications to grow as the project matures. Calligra offers its applications on both desktop computers and mobile platforms like tablets and smartphones.

The desktop version of Calligra is called the Calligra Suite. Version 2.4 of the Calligra Suite contains the following applications:

<h3>Office Applications</h3>

<ul>
<li> Calligra Words - Word processor</li>
<li> Calligra Sheets - Spreadsheet</li>
<li> Calligra Stage - Presentation</li>
<li> Calligra Flow - Diagrams and Flowcharts</li>
<li> Kexi - Visual Database Creator</li>
<li> Braindump Note - Mind Mapping</li>
</ul>

<h3>Graphic Applications</h3>

<ul>
<li> Krita - Drawing Application</li>
<li> Karbon - Vector Graphics</li>
</ul>

<h3>Management Applications</h3>

<ul>
<li> Calligra Plan - Project Management</li>
</ul>

There are also versions for mobile platforms and these have their own user interfaces suited for touch computing. This release contains two such user interfaces:

<ul>
<li> Calligra Mobile - A version of Calligra for the Nokia N900 smartphone . This version is both a viewer and a document editor. It supports text documents, spreadsheets and presentations.</li>
<li> Calligra Active - A more modern user interface for Calligra that is well integrated into the Plasma Active tablet environment. At this time Calligra Active is a viewer only.</li>
</ul>

<h2>New in This Release</h2>

We recommend to <a href="http://www.calligra.org/news/calligra-2-4-released/">read the announcement</a> for a full list but the list of improvements is impressive: two new applications, a totally rewritten text layout engine, improved Microsoft filters and a new user interface for tablets and mobile devices.

Calligra is unique among the free application suites in two ways: it is the only one to have a serious offering on mobile and touch based devices. This makes it right from the start an important member of the family of free office suites. It is also unique in that it contains creative applications. No other application suite packages a painting application and a high-level vector drawing application.

We are convinced that Calligra will continue to grow and mature and become a truly outstanding suite.