---
title: "KDE Ships First Beta of Plasma Workspaces, Applications and Platform 4.10"
date:    2012-11-21
authors:
  - "sebas"
slug:    kde-ships-first-beta-plasma-workspaces-applications-and-platform-410
comments:
  - subject: "PIM?"
    date: 2012-11-21
    body: "I don't see much news from PIM land, and it is a pity, since they are my most important applications. I see some things in the release plan, though. \r\n\r\nAre less developers involved in KDE PIM now than before? Or perhaps they are working on other endeavors?"
    author: "Alejandro Exojo"
  - subject: "No Integration?"
    date: 2012-11-21
    body: "Hello.\r\nI'm new to KDE, and I must say I love it. Only one thing bothers me, and I'm not at all aware of a defect, but a virtue: integration.\r\n\r\nI would like for example to use Kmail, but have it not depend on Akonadi. Instead it would use a different database to manage contacts. Perhaps LDIF (LDAP Data Interchange Format).\r\n\r\nAkonadi MySQL needs to run, and with few resources this can be a problem. In the end, users may end up disabling Nepomuk and Akonadi, but then we cannot use KMail, KOrganizer.\r\n\r\nI'm not saying to remove the integration. On the contrary, I just think that there should be an alternative to Akonadi for computers with low resources."
    author: "elav"
  - subject: "nothing about PIM"
    date: 2012-11-21
    body: "exactly, no news about PIM, but we have new print support in KSudoku  :-))"
    author: "Enrico"
  - subject: "Bug fixes daily"
    date: 2012-11-21
    body: "There are bug fixes daily, and bug fixing really should be the priority, so not much presence in 4.10 announcements, which mostly lists new features. But yes, more developers are needed to further improve KDE's PIM applications."
    author: "christoph"
  - subject: "Multiple screens"
    date: 2012-11-21
    body: "Has the new screen handling as described in http://www.progdan.cz/2012/09/display-management-in-kde/ made it in 4.10?"
    author: "Rb"
  - subject: "Contrast"
    date: 2012-11-21
    body: "Please check the bad contrast of the Plasma panel. The systray and the panel is white on light gray, which is really hard to see."
    author: "xk"
  - subject: "Akonadi is not a database"
    date: 2012-11-21
    body: "Akonadi is not a database per se, but an access layer for PIM data. It provides normalized access to all kinds of datastructures you tell it about. It uses a database backend for faster access and caching.\r\n\r\nYou can choose Akonadi's database backend. The default is MySQL, but you can also use sqlite as storage (whether or not that improves or hurts performance depends a bit on the amount of data, MySQL is usually faster). In any case, it is recommended to use the latest release, since performance, especially in Nepomuk and Akonadi, has improved dramatically lately. It is not very well reflected in the release notes, but the PIM team has been focused on fixing bugs, and only on fixing bugs. This pays off and really starts to show.\r\nThe problem with the old architecture is that it had quite a lot of limitations that were hard to overcome with its architecture. Also, many things are just a lot better in KMail2 (IMAP support, especially disconnected, PUSH mail) that I personally much prefer KMail2 to its predecessor. That is not to say there are no bugs, or performance problems, but we receive many reports that more and more of those are getting fixed, and we hope to improve the quality there even further.\r\n\r\nWe use Akonadi and Nepomuk heavily in Plasma Active, by the way. I've used it on devices as low-powered as a Nokia N900, on all kinds of netbook hardware and on 3 year old tablets. It is actually suited for low power devices, I honestly think that Akonadi and Nepomuk can be used just fine, even on low-powered devices. Otherwise, they're easy to switch off.\r\n\r\nI've experienced some problems with old databases and caches, maybe that is the problem on your system? In my case, after I wiped a Nepomuk database full of crap that had been piled up since 4.3 or so (and some useful bits, such as tags), it became entirely unnoticable. There's also a database cleaner in Nepomuk in 4.10, maybe that can be used in this kind of problems?"
    author: "sebas"
  - subject: "Attachments within GPG-encrypted emails can't be saved"
    date: 2012-11-22
    body: "It's important to me to be able to save attachments in encrypted emails. \r\nSo this bug (https://bugs.kde.org/show_bug.cgi?id=294272) continues to be a problem.\r\n\r\nHow do people handle encrypted emails? Is there a viable workaround?"
    author: "Anonymous"
  - subject: "Obviously the skills needed"
    date: 2012-11-22
    body: "Obviously the skills needed to fix stuff in PIM land and to implement printing in sudoku are the same... oh wait, they are not "
    author: "Anonymous"
  - subject: "That's a really good"
    date: 2012-11-22
    body: "That's a really good direction for KDE PIM I think. "
    author: "Felix"
  - subject: "I rather think the problem is"
    date: 2012-11-22
    body: "I rather think the problem is KDE if it expects a user who gives a rat's behind about databases to hunt for deprecated config files. Honestly, even if I change from OpenSUSE Gnome Shell to Arch Linux Cinnamon and then to Ubuntu Unity, I never or very rarely have to purge the Zeitgeist config or some other obscure stuff hidden away in the realms of \".\" . I am aware that my comparison here is rather naive. KDE is much more complex than the other DEs, but for all the great software and options it has, a normal user is often very overwhelmed with the constant workload they have when migrating from KDE 4.x to KDE 4.x+1.  It's a lovely desktop for sure, but there is something really wrong with it if normal users just give up and install Thunderbird. I must say though that in 4.8 and .9, KDE PIM finally worked for me. But this may be a hint for 5.x!\r\n"
    author: "Thanatos"
  - subject: "I know there's the new ktp"
    date: 2012-11-22
    body: "I know there's the new ktp project which replaces Kopete but does it have GPG encryption support already for XMPP? This feature is really useful and I was wondering why it had been removed from Kopete. OTR encryption is also useful but GPG should be much safer :)\r\nAnyway, nice to see a new release happening."
    author: "Anonymous"
  - subject: "look and feel in kwin (bug #213847) "
    date: 2012-11-22
    body: "I hoped \"windows that are moved to another desktop should be treated as sticky windows\" would make it into this release. It would realy make a difference if you could put this into 4.10. I though this would only be a minor fix, but it's been delayed for a couple of releases now.\r\n\r\nwhat do you think?"
    author: "hoodie"
  - subject: "the maintainer works on KSudoku"
    date: 2012-11-22
    body: "That's because the KSudoku maintainer spends their spare time on KSudoku, not on KDEPIM.  We are a volunteer project, people get to work on what they like, we do not order people to work on things they are not interested in.  And just because no one filled in the feature list with new PIM features doesn't mean that no one has worked on PIM. There was a post a few weeks ago about the PIM bug-fixing sprint."
    author: "odysseus"
  - subject: "Re: Akonadi is not a database"
    date: 2012-11-22
    body: "Thanks Sebastian:\r\nI understand. I think they've done a great job, and I'm not using the latest version of KDE, as it has not been included in Debian. I use Akonadi to manage Kmail contacts and calendars. Again, it is not that integration is bad, quite the contrary. Just wish I did not have to depend on Kmail Akonadi to work, but not everything can be perfect for me."
    author: "elav"
  - subject: "That's it?"
    date: 2012-11-22
    body: "That is it? That is what is new?\r\nDisappointing few and boring stuff."
    author: "Bob"
  - subject: "You missed the feature plan"
    date: 2012-11-22
    body: "No, that's not it. The text mentions the feature plan, which you can find here, and not even that one is complete: http://techbase.kde.org/Schedules/KDE4/4.10_Feature_Plan"
    author: "sebas"
  - subject: "Few new stuff is good"
    date: 2012-11-22
    body: "Few new stuff is good, means we are reaching feature completion ;-)"
    author: "Anonymous"
  - subject: "Help is welcome"
    date: 2012-11-22
    body: "The title of that bug is first of all incorrect. It's not about making windows as sticky, but just to have the animation when moving a window to another desktop look better.\r\n\r\nAll the places are there it just needs someone to fix the effects. Given that it is just a visual issue in a non-default feature it has rather low priority. Help is always welcome :-) For me it's a rather uninteresting thing as I don't use the feature to move windows to other desktops."
    author: "mgraesslin"
  - subject: "Look fine here\u2026"
    date: 2012-11-22
    body: "Really? Looks fine here: http://imageshack.us/a/img580/6710/systray.png"
    author: "Anonymous"
  - subject: "Thanks for the great work"
    date: 2012-11-22
    body: "Hello, thanks for the great work.\r\n\r\nIs there any chance to get fixed :\r\n\r\nKTP taskbar notifications (invisible - stuck \"new message' icon)?\r\nKTP notification system (like popup new window on incoming message , blinking icon when new message comes) and get some more configuration options for it ?\r\n\r\nIn KATE - add the selection size (in letters) displayed in status bar, or some more cool stuff for sysadmins similar to VIM (not talking about VIM mode , but search replace, buffers, editing tools  etc)\r\n\r\nOverall - add more contrast and colors to Oxygen - currently it looks slick but the usability/ergonomics is painful when you use it 10h/day\r\n\r\nRemove ALL the borders and padding (95% for sure) from default oxygen theme. Take a look at the modern GUIs. They are a pleasure and easy (for the eyes)  to use (Android, win8).\r\n\r\nthanks\r\nKarol"
    author: "Anonymous"
  - subject: "Dolphin font?"
    date: 2012-11-22
    body: "What is the name of that font in Dolphin?"
    author: "Anono"
  - subject: "Not the place"
    date: 2012-11-22
    body: "This is not the place to report bugs"
    author: "Anonymous"
  - subject: "ActiveSync anyone?"
    date: 2012-11-23
    body: "Akonadi/Kontact with working ActiveSync would be the real killer email app. Every other open-source platform--Android, WebOS, etc--has had ActiveSync working for at least a couple of years now. All we have on Linux is davmail, which is not feature complete, and most important, is something we must jump through hoops with to get it working in Kontact. I cannot, for example, yet have full contacts and calendar integration with Exchange. Of course I can do Google mail/contacts/calendar but it's not much use at work. Moreover, Google integration does not work behind proxies :)\r\n\r\nBy the way, I see similar missing features (for office use) in most areas of KDE (except the desktop where it excels). \r\n\r\nIs it time for KDE to seriously look at integration into protocols and applications that are needed on a daily basis for \"the working class\" i.e. 9-5 workers like me? Or is it that KDE will just be the playground for developers and casual browsers only?\r\n\r\nI would be glad if someone posted an honest reply to the above."
    author: "KenP"
  - subject: "Font?"
    date: 2012-11-23
    body: "No idea what screenshot you mean, but KDE ships by default with `Oxygen font\u00b4!\r\n\r\nhttp://www.webupd8.org/2012/01/oxygen-font-new-kde-desktop-font-family.html\r\nhttp://www.google.com/webfonts/specimen/Oxygen\r\n"
    author: "Kyril"
  - subject: "New KDE-PIM feature"
    date: 2012-11-23
    body: "Kmail gains email completion support even if nepomuk is disabled."
    author: "C\u00e9dric"
  - subject: "PIM improvements"
    date: 2012-11-24
    body: "Bugfixes are constantly being made. The biggest news is improvements in the Akonadi-Nepomuk interaction, which should make email indexing faster and should cause less high cpu-high io usage in the system with desktop search turned on."
    author: "Andris"
  - subject: "Honestly? I don't think so."
    date: 2012-11-24
    body: "Honestly? I don't think so."
    author: "davkol"
  - subject: "Still not clear"
    date: 2012-11-24
    body: "There's still a misunderstanding here: saying you want KMail without Akonadi is like saying you want a car but without the car as you only care for the windshield.\r\n\r\nIt's not like KMail wouldn't work without Akonadi - there IS no KMail without Akonadi. See, for an explanation <a href=\"http://community.kde.org/KDE_PIM/History#Unifying_the_PIM_backend\">here</a>."
    author: "jospoortvliet"
  - subject: "it's there"
    date: 2012-11-24
    body: "The bugreport you link to HAS a workaround by somebody. It's unfortunate this isn't fixed yet - I can only guess that it's a resource issue.\r\n\r\nThere aren't a lot of KDE PIM developers and it is hard to recruit new ones as KDE PIM is quite big and complicated. This was actually one of the reasons behind developing the new PIM Backend - it would make it easier for people to get involved and use KDE PIM functionality in their applications."
    author: "jospoortvliet"
  - subject: "Re: Still not clear"
    date: 2012-11-27
    body: "No, actually I want a car that can change or add the pieces I want. ;) Let me see the link"
    author: "elav"
  - subject: "+1 for ActiveSync"
    date: 2012-11-30
    body: "Agree with KenP,  Akonadi/Kontact + ActiveSync would be a killer. Another way to look at this - develop an Akonadi resource for OpenExchange? The OpenExchange people look very productive, and they seem to concentrate on OpenExchange itself so an Akonadi resource would seem to need some additional developer support.   "
    author: "RodS"
  - subject: "That's what you have"
    date: 2012-11-30
    body: "Well that's exactly what you have. Following the car analogy, Akonadi is the car. And you can change all the pieces used to access your PIM data."
    author: "Mort"
---
Today KDE released the first beta for its renewed Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing new and old functionality.

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="http://kde.org/announcements/announce-4.10-beta1_thumb.png"><img src="http://kde.org/announcements/announce-4.10-beta1_thumb.png" align="center" width="640" alt="Plasma Desktop with the Dolphin file manager" title="Plasma Desktop with the Dolphin file manager" /></a>
<br />
<em>Plasma Desktop with Dolphin</em>
</div>

Highlights of 4.10 include the following and more:

<ul>
    <li>
    <strong>Qt Quick in Plasma Workspaces</strong> -- Qt Quick is continuing to make its way into the Plasma Workspaces. Plasma Quick, KDE's extensions on top of QtQuick allow deeper integration with the system and more powerful apps and Plasma components. Plasma Containments can now be written in QtQuick. Various Plasma widgets have been rewritten in QtQuick, notably the system tray, pager, notifications, lock & logout, weather and weather station, comic strip and calculator plasmoids. Many performance, quality and usability improvements make Plasma Desktop and Netbook workspaces easier to use.
    </li>
    <li>
    <strong>New Screen Locker</strong> -- A new screen locking mechanism based on QtQuick brings more flexibility and security to Plasma Desktop.
    </li>
    <li>
    <strong>Animated Wallpapers</strong> -- Thanks to a new QtQuick-based wallpaper engine, animated wallpapers are now much easier to create.
    </li>
    <li>
    <strong>Improved Zooming in Okular</strong> -- A technique called tiled rendering allows Okular to zoom in much further while reducing memory consumption. Okular Active, the touch-friendly version of the powerful document reader is now a KDE Application.
    </li>
    <li>
    <strong>Faster indexing</strong> -- Improvements in the Nepomuk semantic engine allow faster indexing of files. The new Tags kioslave allows users to browse their files by tags in any KDE-powered application.
    </li>
    <li>
    <strong>Color Correction</strong> -- Gwenview, KDE's smart image viewer, and Plasma's Window Manager now support color correction and can be adjusted to the color profile of different monitors, allowing for more natural representation of photos and graphics.
    </li>
    <li>
    <strong>Notifications</strong> -- Plasma's notifications are now rendered using QtQuick. Notifications themselves, especially concerning power management, have been cleaned up.
    </li>
    <li>
    <strong>New Print Manager</strong> -- Setup of printers and monitoring jobs was improved thanks to a new implementation of the Print Manager.
    </li>
    <li>
    <strong>Kate, KDE's Advanced Text Editor</strong> received multiple improvements regarding user feedback. It is now extensible using Python plugins.
    </li>
    <li>
    <strong>KTouch</strong> -- KDE's touch-typing learning utility has been rewritten and features a cleaner, more elegant user interface.
    </li>
    <li>
    <strong>libkdegames improvements</strong> -- Many parts of libkdegames have been rewritten, porting instructions for 3rd party developers are <a href="http://techbase.kde.org/Projects/Games/Porting_to_libkdegames_v5">available</a>.
    </li>
    <li>
    <strong>KSudoku</strong> now allows puzzles to be printed.
    </li>
    <li>
    <strong>KJumpingCube</strong> has seen a large number of improvements making the game more enjoyable.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.10_Feature_Plan">4.10 Feature Plan</a>. As with any large number of changes, we need to give 4.10 a good testing in order to maintain and improve the quality and user experience when users install the update.

Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider testing 4.10 thoroughly and report any bugs you find to <a href="http://bugs.kde.org">bugs.kde.org</a>.