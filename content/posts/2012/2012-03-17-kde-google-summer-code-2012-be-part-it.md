---
title: "KDE & Google Summer of Code 2012 - be part of it!"
date:    2012-03-17
authors:
  - "camilasan"
slug:    kde-google-summer-code-2012-be-part-it
comments:
  - subject: "Yay KDE and GSoC"
    date: 2012-03-19
    body: "From what I've seen, GSoC and GCI have been great for KDE, even though they are a lot of work. What do you think, mentors? Has it been worth the work? Former students, if you aren't applying again, consider making the move to mentor. Working with the folks in this project is so rewarding, both personally and for the growth of KDE and F/OSS as a whole.\r\n\r\nAlso, if you aren't qualified or have the time for GSoC, watch for the announcement for Season of KDE, which has been amazing as well. "
    author: "valoriez"
---
<div style="float: right; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/GSoC2012_300x200.png"" /></div>

We are glad to announce that KDE has been accepted as a mentoring organization for Google Summer of Code for the 8<sup>th</sup> time. We look forward to working with great students throughout the summer again.
<!--break-->
To find out more about the program, visit the <a href="http://www.google-melange.com/">GSoC website</a> and pay special attention to the <a href="http://www.google-melange.com/document/show/gsoc_program/google/gsoc2012/faqs">FAQ</a> and <a href="http://www.google-melange.com/gsoc/events/google/gsoc2012">timeline</a> - print it and hang it on the wall in your work room.

If you want to take part in Google Summer of Code as a student, now is the time to start working on your application. Ask questions in the IRC channel #kde-soc on <a href="http://freenode.net/">freenode</a> or join the <a href="https://mail.kde.org/mailman/listinfo/kde-soc">KDE Summer of Code mailing list</a>. We will be happy to help you. Also, get tips about how to <a href="http://teom.wordpress.com/2012/03/01/how-to-write-a-kick-ass-proposal-for-google-summer-of-code/">write a good proposal</a> and how to have <a href="http://google-opensource.blogspot.com/2011/03/dos-and-donts-of-google-summer-of-code.html">a successful project</a>. <a  href="http://community.kde.org/GSoC/2011/StatusReports">Blog posts</a> of students from last year can be useful as well.

Don't be shy! It is important to get in contact right away with the team you want to work with to talk about your application. You can either come up with your own idea or get inspired by <a href="http://community.kde.org/GSoC/2012/Ideas">our ideas list</a>. New ideas are always welcome.

We hope to see many great applications from students all around the world. Enjoy the summer, show your code - be part of KDE!