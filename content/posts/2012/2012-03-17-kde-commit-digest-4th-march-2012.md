---
title: "KDE Commit-Digest for 4th March 2012"
date:    2012-03-17
authors:
  - "mrybczyn"
slug:    kde-commit-digest-4th-march-2012
---
In <a href="http://commit-digest.org/issues/2012-03-04/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kdevelop.org/">KDevelop</a> gets sessions data engine and plasmoid, also memory usage optimization and various fixes</li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, fixes for mouse events; <a href="http://www.kexi-project.org/">Kexi</a> switched to a new optimized SQLite database compacting tool; multiple bugfixes</li>
<li>Implemented ResourceWatcher signals for added and removed types in <a href="http://nepomuk.kde.org/">Nepomuk</a></li>
<li>Start of <a href="http://userbase.kde.org/Plasma">Plasma</a> Network Management interface using QML</li>
<li>RDF parsing and searching notes by people implemented in <a href="http://vhanda.in/blog/2012/02/notably-v0.4/">Notably</a></li>
<li>A library is created in KDE Libs for classes that are planned to be submitted to Qt5</li>
<li>Improved mounted disk partitions handling in <a href="http://solid.kde.org/">Solid</a></li>
<li>Bugfixes in <a href="http://docs.kde.org/development/en/kdesdk/lokalize/index.html">Lokalize</a>, <a href="http://uml.sourceforge.net/">Umbrello</a>, <a href="http://pim.kde.org/">KDE-PIM</a>, <a href="http://kst.kde.org/">Kst</a></li>
</ul>

<a href="http://commit-digest.org/issues/2012-03-04/">Read the rest of the Digest here</a>.