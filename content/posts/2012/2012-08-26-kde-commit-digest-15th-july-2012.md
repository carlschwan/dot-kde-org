---
title: "KDE Commit Digest for 15th July 2012"
date:    2012-08-26
authors:
  - "mrybczyn"
slug:    kde-commit-digest-15th-july-2012
comments:
  - subject: "Yay!"
    date: 2012-08-27
    body: "A new KCD, thanks!\r\n\r\nI see there's work going on in KTouch, too - that's great as it's a very nice touch-typing-teaching-app. Cookies for Sebastian Gottfried ;-)"
    author: "jospoortvliet"
---
In <a href="http://commit-digest.org/issues/2012-07-15/">this week's KDE Commit Digest</a>:

<ul><li>Re-implemented dropping of files on folders in the Places panel in kdebase</li>
<li>Improved <a href="http://www.cmake.org/">CMake</a> and auto-completion support in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Line spacing can be now configured in <a href="http://konsole.kde.org/">Konsole</a></li>
<li>Better equation support in analitza, a library to add mathematical features in KDE-Edu</li>
<li>Image conversion to portable pixmap format (ppm) stream in kipi-plugins</li>
<li>Improvements in LogViewer in Ktp, part of KDE Telepathy</li>
<li><a href="http://pim.kde.org/akonadi/">Akonadi</a> now caches prepared queries and re-uses them whenever possible</li>
</ul>

<a href="http://commit-digest.org/issues/2012-07-15/">Read the rest of the Digest here</a>.