---
title: "The KDE and openSUSE communities welcome you to COSCUP 2012!"
date:    2012-04-03
authors:
  - "jospoortvliet"
slug:    kde-and-opensuse-communities-welcome-you-coscup-2012
---

<a href="http://aseigo.blogspot.com/2012/02/coscup-2012.html"><strong>COSCUP</strong></a> is the largest FLOSS conference organized by local communities in Taiwan. The conference has sessions for new users, enthusiastic promoters, coders or anyone who is interested in cutting-edge FLOSS technologies. The goal is to create a friendly and informative environment for people from different communities to make friends, learn new technologies and inspire each other.

<a href="http://www.flickr.com/photos/coscup/6061662897/" title="Plasma Active: A Truly Open and Innovative Solution for Consumer Devices by COSCUP, on Flickr"><img src="http://farm7.staticflickr.com/6074/6061662897_d346cd639f.jpg" width="500" height="333" alt="Plasma Active: A Truly Open and Innovative Solution for Consumer Devices"></a>
<strong>Aaron in action at COSCUP 2011</strong>

At <a href="http://coscup.org/2011/en">COSCUP 2011</a>  (2012 site not up yet), several KDE and openSUSE contributors gave talks and presented ‘cool stuff’ at their booths. This year, we will have a dedicated track for KDE and openSUSE. Starting today, you can send in session proposals (instructions below).

<a href="http://www.flickr.com/photos/coscup/6069121401/" title="OpenSUSE攤位 by COSCUP, on Flickr"><img src="http://farm7.staticflickr.com/6194/6069121401_2254f25a6e.jpg" width="500" height="375" alt="OpenSUSE攤位"></a>
<strong>Geeko's at their booth at COSCUP 2011</strong>

<h2>Two day track dedicated to KDE and openSUSE</h2>
We are glad to announce that in 2012, the COSCUP conference will feature a full two-day track dedicated to KDE and openSUSE. People from both organizations have teamed up to bring you the best talks possible.

But for that, we need your help!

<h2>Call for Papers</h2>
The Program Committee is looking for talks about KDE and openSUSE. Here are some ideas for topics:
<ul><li>desktop technologies (d-bus, gstreamer, etcetera)
embedded and mobile</li>
<li>distribution technologies (packaging tools, build systems, et cetera)</li>
<li>challenges in adapting software to local needs (L10N, I18N)</li>
<li>cross-desktop and cross-distribution collaboration</li>
<li>cloud computing and integration into the desktop</li>
<li>or anything else you think the world should hear about!</li></ul>

<a href="http://www.flickr.com/photos/coscup/6062335955/" title="BoF - KDE 社群 by COSCUP, on Flickr"><img src="http://farm7.staticflickr.com/6187/6062335955_aa7400e9f3.jpg" width="500" height="334" alt="BoF - KDE 社群"></a>
<strong>KDE BOF at COSCUP 2011</strong>

Although KDE and openSUSE have always been close, the talks do not necessarily have to be related to both KDE and openSUSE (like ownCloud or Apper). In the spirit of our openness and collaboration, we welcome talk proposals about KDE on other distributions (Fedora, Ubuntu, et cetera), or other desktops and non-desktop technologies on openSUSE (GNOME, OpenStack, et cetera).

The call for papers ends on June 15. Please email a proposal of about 200 words, accompanied by a ~50 word biography, in either English or Chinese, to <a href="mailto:cfp@opensuse.org ">the Program Committee</a> before that date.

<h2>Program Committee</h2>
The Program Committee for the KDE/openSUSE track at COSCUP consists of the following people:
<li>Franklin Weng (KDE)</li>
<li>Aaron J. Seigo (KDE)</li>
<li>Sakana (openSUSE)</li>
<li>Al Cho (openSUSE)</li>
<li>Bryen Yunashko (openSUSE)</li>
<li>Greg Kroah-Hartman (the Linux Foundation)</li>

<h2>Making this possible</h2>
Remember that events like these are made possible by the <a href="http://jointhegame.kde.org/">members of KDE</a>!