---
title: "KDE Commit-Digest for 11th November 2012"
date:    2012-11-14
authors:
  - "mrybczyn"
slug:    kde-commit-digest-11th-november-2012
comments:
  - subject: "GPX in Marble, eh?"
    date: 2012-11-15
    body: "GPX handling? I wasn't even aware Marble had such capabilities!\r\n\r\nMaybe this means we're moving towards a brilliant offline OpenStreetMap editor in the spirit of JOSM, though only the Marble team knows. :) Would be brilliant though.\r\n\r\nWhile OSM is frankly useless for the countryside due to the inaccuracy of the maps (though I have improved the map for my area), it is brilliant for finding your way around a city and referring a friend to it too.\r\n\r\nIt is also like crack; once you start editing, nothing can stop you, and as far as I know there is no OSM rehab program. :) It is really, really addictive, at least for me, and I have went to OCD-like lengths to map every store in my local town. Feel free to get started, just click edit anywhere on osm.org and you'll get a much friendlier (though somewhat less versatile) Flash-based editor called Potlatch 2. Be warned! This is not just Wikipaedia for maps, it's drugs on your computer!"
    author: "Y.A."
  - subject: "Wow!"
    date: 2012-11-15
    body: "Magnificent effort to get up to date again! Thanks!"
    author: "Boudewijn Rempt"
---
In <a href="http://commit-digest.org/issues/2012-11-11/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kdevelop.org/">KDevelop</a> adds a document list context menu button to main tab bar</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> improves <a href="http://en.wikipedia.org/wiki/GPS_eXchange_Format">GPX (GPS eXchange Format)</a> handling</li>
<li><a href="http://www.digikam.org/">Digikam</a> gets initial version of a noise reduction algorithm</li>
<li>KDE-Runtime suports ToolBox bindings for declarative containments</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> KCM allows filtering of files based on mimetype</li>
<li><a href="http://skrooge.org/">Skrooge</a> supports new templates for monthly reports</li>
<li><a href="http://www.kde.org/applications/system/kdepartitionmanager/">PartitionManager</a> gains better support of btrfs.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-11-11/">Read the rest of the Digest here</a>.