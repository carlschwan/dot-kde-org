---
title: "Akademy 2012 Call for Papers; Registration opens"
date:    2012-02-03
authors:
  - "kallecarl"
slug:    akademy-2012-call-papers-registration-opens
comments:
  - subject: "Registration"
    date: 2012-02-03
    body: "You need to use your identity.kde.org account to register, if you don't have one create one at identity.kde.org and then go to back to the akademy website"
    author: "tsdgeos"
---
<div style="float: right; padding: 1ex; margin: 0ex 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/akademysocial.jpg" alt="Akademy social event" width="300" /></div>

Akademy—the annual world summit of the KDE Community—will take place from 30 June to 6 July in Tallinn, Estonia. This 10th edition of the KDE community conference will celebrate 15 years of KDE!

<a href="http://akademy2012.kde.org/how-to-register">Registration</a> for the conference is now open, and the <a href="http://akademy2012.kde.org/cfp">Call for Papers</a> has been published.
<!--break-->
Akademy features a 2-day conference packed with presentations on the latest KDE developments, followed by 5 days of workshops, birds of a feather (BoF) and coding sessions.

Akademy 2012 will highlight KDE's progress since its founding over 15 years ago. It will be both a showcase for the community's innovative spirit and a place to further embrace partnerships with other Free Software communities such as the Qt Project.

To further these and our other goals, we are asking for presentation proposals on the following topics:
<ul>
<li>Maturity; Innovation</li>
<li>Embracing the Qt Project and other partners</li>
<li>Activities relevant to the KDE Frameworks efforts</li>
<li>Development of beautiful and fast applications</li>
<li>Increasing our reach through efforts like accessibility, promotion, translation and localization</li>
<li>Other topics that are relevant to KDE and the Qt ecosystems</li>
</ul>

The Call for Papers will be open until <strong>15 March 2012</strong>. More information is available on the <a href="http://akademy2012.kde.org">Akademy website</a>.

<h2>About Akademy</h2>
Akademy is the place to learn about the latest features and share ideas for future releases of KDE's software. The conference will bring together hundreds of KDE contributors, users and industry partners. Participants attend to gain in-depth technical insights, meet and work with KDE and Qt experts, and take part in deciding the future direction of KDE.

Lydia Pintscher (Akademy program committee and KDE e.V. Board member) says, "Akademy is one of the most important events in KDE's annual schedule. It's where the community comes together and makes things happen. We're incredibly excited to put this year's focus on collaboration with our partners from around the Qt ecosystem because there is great potential for everyone involved. We hope to see you there. Whether you've been with KDE since its beginning 15 years ago, just joined or are involved in a related project - Akademy is for you!"

<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/college_0.jpeg" alt="Estonian IT College"width="300" height="200" /></div>

If you are planning on attending, please plan your travel soon. Tallinn is a cultural center and a popular travel destination. Hotel tips and more information will be available soon on the Akademy website. 

The KDE e.V. may reimburse travel costs for those visiting KDE conferences and events. If you find money to be a problem in participating in Akademy, read more about reimbursement on the <a href="http://ev.kde.org/rules/reimbursement_policy.php">KDE e.V. website</a>.