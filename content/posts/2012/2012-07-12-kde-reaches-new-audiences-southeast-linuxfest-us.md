---
title: "KDE Reaches New Audiences at SouthEast LinuxFest (U.S.)"
date:    2012-07-12
authors:
  - "heath"
slug:    kde-reaches-new-audiences-southeast-linuxfest-us
comments:
  - subject: "Awesome!"
    date: 2012-07-12
    body: "And I feel so silly that I forgot to bring Krita DVD's to akademy. But at least here they got into the right hands :-)\r\n\r\nBoudewijn"
    author: ""
  - subject: "I have high hopes for Krita."
    date: 2012-07-20
    body: "I have high hopes for Krita. I'm almost certain that with more exposure it would net many who are not satisfied with Gimp's drawing capabilities.\r\n\r\nI really feel as if it is on the stepping stone to becoming one of the big name applications. All it needs is a little push.\r\n\r\nKeep up the great work."
    author: ""
  - subject: "People are happy with the Gimp"
    date: 2012-08-02
    body: "Gimp works great. "
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/SELFExhibit350.jpg" /><br />KDE at SouthEast (US) LinuxFest</div>

KDE was represented at the <a href="http://www.southeastlinuxfest.org/">SouthEast LinuxFest</A> again this year with a booth showing off our latest software. The Calligra Suite, Kontact, Telepathy, and Plasma Workspaces were featured. Krita particularly intrigued many people. As a result, many copies of the Comics with Krita training DVD were handed out along with the accompanying comic book, Wasted Mutants and Wisdom Mountain.
<!--break-->
<div style="float: right; padding: 1ex; margin: 5ex 1ex 1ex 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/TuxLikesKDE350.jpg" /><br />KDE - Tux's favorite user interface</div>

Many who have not used KDE software recently, or ever, said that they were impressed with KDE's technology, with special praise for Kwin's improvements and all around Workspace stability. There was a contest at the booth to see who could crash a KDE application in under a minute. Several people tried hard to accomplish this task, but no one following the rules (no command line, magic keys, or rebooting) was able to make any KDE program crash. 

Some visitors to the booth asked about getting more visibility for KDE and organizing a KDE USA community. There were suggestions to have KDE representation at more open source software events and sprints involving U.S. KDE contributors.  Anyone who is interested can come to the Freenode IRC channel #kde-usa and sign up for the <a href="https://mail.kde.org/mailman/listinfo/kde-usa">KDE-USA mailing list</a>. If you are a KDE user in the U.S. and want a stronger KDE presence, there are ways for the US KDE community to organize. Get involved!

<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br />This work by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">KDE</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.