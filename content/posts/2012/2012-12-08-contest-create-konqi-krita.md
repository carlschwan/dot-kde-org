---
title: "Contest: Create Konqi with Krita"
date:    2012-12-08
authors:
  - "neverendingo"
slug:    contest-create-konqi-krita
---
<p><a href="http://en.wikipedia.org/wiki/Konqui">Konqui the friendly dragon</a> has been KDE's mascot for over ten years. It's time for a new look!</p>
<p><img src="http://kde.org/stuff/clipart/konqi-klogo-official-400x500.png" border="0" width="400" height="500" /></p>
<p>So <a href="http://forum.kde.org">KDE forum</a> admin Neverendingo is organizing a splendid contest together with the Krita community! There are two prizes: a <a href="http://krita.org/component/content/article/1-krita-informations/104-training-dvd-01-comics-with-krita">DVD+Comics pack</a> and the last original <a href="http://rempt.xs4all.nl/fading/index.cgi/hacking/krita/hackathon-1.html">First Krita Sprint t-shirt</a>! And of course undying fame! The jury consists of the well-known artists <a href="http://www.timotheegiet.com/">Animtim</a>, <a href="http://davidrevoy.com">Deevad</a> and <a href="http://pinheiro-kde.blogspot.nl/">Nuno Pinheiro</a>.</p>
<p>So fire up Krita and get painting!</p>
<p>There are some ground rules:</p>
<ul>
<li>Must be done in Krita</li>
<li>The artwork will be licensed under the LGPL V2+.</li>
<li>It must honor KDE's <a href="http://www.kde.org/code-of-conduct/">Code of Conduct,</a> which means, for instance, that Konqui shouldn't be beating a garden gnome with a big hammer.</li>
</ul>
<p>What is needed is an inspiring, versatile portrait without background that supports publicity for KDE.</p>
<p>When you're done, go over to the <a href="http://forum.kde.org/viewforum.php?f=254">Krita contests forum</a> and add your contest entry. Create a topic for each submission. Make as many submissions as you want. And feel free to update your work with followup posts. Each person can have only one entry in the contest; your most recent work at the deadline counts. The contest starts now and you have until January 31st, so you have the whole holiday season to paint!</p>