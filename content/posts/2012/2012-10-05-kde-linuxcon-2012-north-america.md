---
title: "KDE at LinuxCon 2012 North America"
date:    2012-10-05
authors:
  - "kallecarl"
slug:    kde-linuxcon-2012-north-america
comments:
  - subject: "Slides"
    date: 2012-10-08
    body: "It is nice to read those reports. They give a sense of what it is going on in the community. It is however cruel to tease us with cool-sounding presentations but no links to the slides :("
    author: "hmmm"
  - subject: "link to slides added"
    date: 2012-10-08
    body: "These slides and others can be found at the link in the last sentence of the section above on \"The Conference\"."
    author: "kallecarl"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/FeaturingAlexandra350.png" /><br /><strong>Featuring KDE and Alexandra</strong></div>
KDE had a visible presence at LinuxCon 2012 in San Diego, California, August 29-31. Thanks to the Linux Foundation for donating an exhibit space to KDE.

<h2>The Conference</h2>
LinuxCon 2012 was the fourth year this annual event has taken place. Sponsored and produced by the Linux Foundation, it is a premier Linux event and attracts big names in open source, both companies and individuals. The event has a corporate feel with many leading tech companies exhibiting and their representatives making presentations. 

The general themes were:
<ul>
<li>Cloud implementation and services</li>
<li>Security</li>
<li>Embedded systems</li>
<li>Various applications and tools</li>
<li>Kernel topics</li>
<li>Community</li>
<li>Intellectual property, patents, licenses</li>
</ul>
There was a large, diverse crowd, because LinuxCon is held jointly with several other high level Linux-related conferences, including:
<ul>
<li>Xen Summit</li>
<li>GStreamer Conference</li>
<li>Linux Kernel Summit</li>
<li>Linux Plumbers Conference</li>
<li>Linux Security Summit</li>
</ul>
The <a href="http://events.linuxfoundation.org/events/linuxcon">LinuxCon 2012 website</a> has more information, including a <a href= "http://events.linuxfoundation.org/events/linuxcon/schedule">full schedule</a> with presentation descriptions and <a href="http://events.linuxfoundation.org/events/linuxcon/slides">slides</a> from many of the presentations.

<h2>KDE presence</h2>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/FrankCarl350.png" /><br /><strong>Carl and Frank</strong></div>
KDE was one of about 30 exhibitors in the Technology Showcase that included leading tech companies and a few other non-profits such as Fedora and the Electronic Fronter Foundation. KDE got excellent exposure. 

The exhibit featured Plasma Workspaces on a laptop, netbook and tablet. The new, speedy laptop was running the most current version of KDE software. A high performance beauty. People were interested in Activities, which was demonstrated with an all-purpose configuration, and a SysAdmin Activity with every conceivable indicator and tool widget. From this, people could quickly understand the Activity concept and its usefulness. The netbook and tablet illustrated the continuity and advantages of KDE technology shared across different formats. The tablet running Mer, Plasma Active and Contour got quite a bit of attention particularly following my presentation, The Grass Roots Tablet (more below).

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Dario350.png" /><br /><strong>Dario down with KDE</strong></div>
Several KDE stalwarts attended LinuxCon or one of the associated conferences. Dario Freddi (attending the Linux Plumbers Conference) was amazed when he saw the exhibit. Likewise, Frank Karlitschek was pleased to see KDE in this setting. He was there along with others from ownCloud, working on some new open cloud offerings. Alexandra Leisse, Guillermo Antonio Amaral and Helio Castro also hung out at the exhibit and talked with visitors.

Many of the exhibit visitors are KDE users, some for quite a long time. They are quite happy with KDE. Of course, there were a few people who use other—mostly light weight—desktop environments. Considering all comments and interactions, KDE was quite well received.
 
<h2>Presentation – <em>The Grass Roots Tablet</em></h2>

My presentation—<em>The Grass Roots Tablet</em> (<a href="http://events.linuxfoundation.org/images/stories/pdf/lcna_co2012_symons.pdf">slides</a>)—had a good sized audience and got a positive response. The presentation addressed the closed nature of the mobile device market and the need for the Free Software community to consider the long term implications of a market dominated by a few mega-companies. It also covered Plasma Active and the collaborative, community project to introduce an alternative tablet, which has the possibility of creating opportunities for Free Technology.
 
<h2>Thanks and appreciation</h2>
Many thanks to KDE e.V., the Board and Claudia for making this opportunity possible. It's clear from the response at LinuxCon that the KDE Community is a significant positive influence in Free Software and is respected accordingly. I appreciate the contributions that each person makes to KDE. They made it a pleasure to represent KDE at this prestigious event.