---
title: "KDE Commit-Digest for 22nd April 2012"
date:    2012-05-01
authors:
  - "mrybczyn"
slug:    kde-commit-digest-22nd-april-2012
---
In <a href="http://commit-digest.org/issues/2012-04-22/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://konsole.kde.org/">Konsole</a> gains a Web Shortcuts menu that allows searching selected text using any of the search providers; search scrollback is now possible in Konsole KPart</li>
<li>New placement policy in KDEWorkspace: allow "under mouse"; work to include screenlocker into ksmserver</li>
<li><a href="http://amarok.kde.org/">Amarok</a> ships the Free Music Charts by default; Amarok's iPod collection completely rewritten</li>
<li><a href="http://www.digikam.org/">Digikam</a> supports keyboard shortcuts in the timeline view</li>
<li>GlobalShortcuts Plugin added into ActivityManager in KDE-Base</li>
<li>Work on the rule files in <a href="http://multimedia.kde.org/">kdemultimedia</a></li>
</ul>

<a href="http://commit-digest.org/issues/2012-04-22/">Read the rest of the Digest here</a>.