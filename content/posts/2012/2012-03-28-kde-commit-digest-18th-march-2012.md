---
title: "KDE Commit-Digest for 18th March 2012"
date:    2012-03-28
authors:
  - "vladislavb"
slug:    kde-commit-digest-18th-march-2012
comments:
  - subject: "KDevelop"
    date: 2012-03-28
    body: "kdesrc-build != KDevelop!"
    author: ""
  - subject: "yes, thanks for noticing,"
    date: 2012-03-29
    body: "yes, thanks for noticing, i'll fix that now."
    author: "vladislavb"
  - subject: "Akonadi and Exchange 2003"
    date: 2012-03-29
    body: "Whoa ... when did this happen? Anyway, now that I'm over the shock, how can I test it? I am assuming I can create a mail source which is not plain IMAP but exchange based with push email support etc."
    author: ""
  - subject: "No diff links?"
    date: 2012-03-29
    body: "No links at \"Diffs: 1, 2\" in the Bug Fixes section of the Commit Digest like there used to be? Is there another quick way to have a look at diffs?"
    author: ""
  - subject: "They are linked up for"
    date: 2012-03-31
    body: "They are linked up for commits pulled from svn but not from git. I believe we looked into this before, but i'll try and look into it again."
    author: "vladislavb"
---
In <a href="http://commit-digest.org/issues/2012-03-18/">this week's KDE Commit-Digest</a>:

              <ul><li><a href="http://pim.kde.org/akonadi/">Akonadi</a> improves Google Applets and Exchange 2003 support</li>
<li><a href="http://simon-listens.org/">Simon</a> introduces additional "finalize" step for command plugins</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> now supports live updates for thumbnails in tabbox and a new example script: "demands attention on current desktop," along with many bugfixes</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a>  adds the ability to store a .kdev4 file in a project directory, opening files at a given line, and includes further work on a new debugger, as well as many bugfixes</li>
<!--break-->
<li><a href="http://kdesvn-build.kde.org/">Kdesrc-Build</a> introduces support for bzr (libdbusmenu-qt).</li>
<li>Tab completion and smart completion implemented across <a href="http://edu.kde.org/cantor/">Cantor's</a> backends including Maxima, Qalculate, Sage, Octave and Scilab</li>
<li><a href="http://okular.org/">Okular</a> introduces improved support for videos in pdf documents</li>
<li>Work in <a href="http://community.kde.org/Plasma/Active">Plasma Active</a> on threaded models and multitouch selecting</li>
<li><a href="http://dolphin.kde.org/">Dolphin</a> introduces support for dynamic roles</li>
<li>In <a href="http://www.calligra.org/">Calligra</a>, <a href="http://www.krita.org/">Krita</a> introduces a texturing paintop option and <a href="http://www.calligra.org/karbon/">Karbon</a> improves XFig support, along with many bugfixes throughout it's applications</li>
<li>Much work on Media Center, which now supports loading backends, playing media and storing thumbnails</li>
<li>In <a href="http://www.kdenlive.org/">Kdenlive</a>, image sequences can now start at an arbitrary frame</li>
<li><a href="http://rekonq.kde.org/">Rekonq</a> introduces support for Google Sync Bookmarks along with many bugfixes</li>
<li>Many improvements to the <a href="http://sprints.kde.org">KDE Sprints website</a></li>
<li><a href="http://ktorrent.org/">KTorrent</a> adds IPv6 support to DHT (BEP 32) and improves DHT code</li>
<li>Bug fixes in KDE-PIM, Oxygen-GTK, <a href="https://launchpad.net/telepathy-kde">KDE Telepathy</a>, <a href="http://uml.sourceforge.net/">Umbrello</a>, <a href="http://www.digikam.org/">Digikam</a>, <a href="http://www.kde.org/workspaces/">Plasma</a>, <a href="http://jontheechidna.wordpress.com/2010/08/27/muon-and-qapt-1-0-1/">Muon</a>, and <a href="http://milianw.de/blog/improving-massif-visualizer-for-large-data-files">Massif-Visualizer</a></li>
<li><br /><br />
Krita 2.4 is <a href="http://krita.org/component/content/article/10-news/108-first-release-candidate-for-krita-24-released">dedicated to the memory of Jean Giraud Moebius</a></li>
</ul>

                <a href="http://commit-digest.org/issues/2012-03-18/">Read the rest of the Digest here</a>.