---
title: "Beta Testing Program"
date:    2012-05-30
authors:
  - "unknow"
slug:    beta-testing-program
---
<h3>Thorough and Rigorous Beta Testing</h3>
The KDE Community is committed to improving quality substantially with a new program that starts with the 4.9 releases. The 4.9 beta releases of Plasma Workspaces, KDE Applications, and KDE Platform are the first phase of a testing process that involves volunteers called “Beta Testers”. They will receive training, test the two beta releases and report issues through <a href="http://bugs.kde.org">KDE Bugzilla</a>.

<h4>The Beta Testing Team</h4>
The Beta Testing Team is a group of people who will do a 'Testapalooza' on the beta releases, catching as many bugs as possible. This is organized in a structured process so that developers know clearly what to fix before the final release. Anyone can help, even people without programming skills.

<h4>How It Works</h4>
There are two groups of beta testers:
<ul>
<li>informal testers install the beta from their distribution and use it as they normally would. They focus on familiar applications and functions, and report any bugs they can reproduce reliably.</li>
<li>selected functional testing involves a list of applets, new programs and applications that require full testing. Volunteers will choose from the list and test according to <a href="http://community.kde.org/Getinvolved/Testing/Beta">established beta testing guidelines</a>.</li>
</ul>
Testing is coordinated to make sure that people's testing time and effort are used as effectively as possible. Coordination gets the most value out of the overall testing program.

<h4>Get Involved</h4>
This is an excellent way for anyone to make a difference for KDE. Continuous quality improvement sets KDE apart. Assisting developers by identifying legitimate bugs leverages their time and skills. This means higher quality software, more features, happier developers. The Beta Testing Program is structured so that any KDE user can give back to KDE, regardless of their skill level.

If you want to be part of this quality improvement program, please contact the Team on the IRC channel #kde-quality on freenode.net. The Team Leaders want to need to know ahead of time who is involved in order to coordinate all of the testing activities. They are also committed to having this project be fun and rewarding.

After checking in, you can install the beta through your distribution package manager. The <a href="http://community.kde.org/Getinvolved/Testing/Beta/InstallingBeta1">KDE Community wiki</a> has instructions. This page will be updated as beta packages for other distributions become available. With the beta installed, you can proceed with testing. Please contact the Team on IRC #kde-quality if you need help getting started.

<h4>Training</h4>
Effective bug reporting is critical to the success of the Program. Bugzilla is a valuable tool for developers and increases their efficiency at closing bugs. Reporting works best when done properly, so the Program includes Bugzilla training. An IRC bugzilla training will be offered in #kde-bugs (freenode) on the weekend of June 9th and 10th. Please visit the #kde-quality IRC channel for information about the Program and the Bugzilla training, or if you have any questions.

Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining in.   