---
title: "Remembering Claire Lotion"
date:    2012-05-20
authors:
  - "KDE.org"
slug:    remembering-claire-lotion
comments:
  - subject: "Very sad"
    date: 2012-05-21
    body: "I'm very sad that I won't ever get to meet Claire. The love, respect, and sorrow at her untimely death widely expressed in the KDE community tells me that she was a person worth knowing, and that she made our corner of the world a better place.\r\n\r\nTo her family I wish the comfort of knowing she was wonderful, and appreciated, and will be dearly missed.\r\n\r\nValorie"
    author: ""
  - subject: "Rest In Peace"
    date: 2012-05-21
    body: "Rest in peace Claire.. I didn't really know you, we never really met, but I certainly felt like I lost a close friend..\r\nA sad day, you will be missed...."
    author: ""
  - subject: "I'll miss you"
    date: 2012-05-21
    body: "I'll miss you, Claire. Without you I wouldn't be where I am today - and I know I'm not the only one. Thank you for the difference you made in my life."
    author: "jospoortvliet"
  - subject: "So sorry to hear about this."
    date: 2012-05-21
    body: "So sorry to hear about this. RIP Claire and my deepest sympathies to her family."
    author: ""
  - subject: "What a loss"
    date: 2012-05-22
    body: "What a terrible loss to her family, friends and the project. I have a deep respect for what she did and think she was a light in the life of many people. Good that release will be dedicated to her. "
    author: ""
  - subject: "Shocked!"
    date: 2012-05-22
    body: "I count Claire as a good friend and we met often back when I was still living in The Netherlands.\r\n\r\nI fondly remember the time when she and Jos and me drove from Amsterdam to Berlin to attend a kde meeting, and back a couple of days later. This was about 5 years ago. Not a bored moment and lots of \"improve the world\" plans came from that trip.  As they say, looking back you have better vision, I believe she came up with some suggestions for how the kde and linux movement could improve that even now show great insight.  She was a smart cookie.\r\n\r\nI cried when I heard this news last night, she really did deserve a much more fruitful and long live.\r\n\r\nThanks for the guys that suggested her name be set in stone on our next kde release."
    author: "zander"
  - subject: "Really sad news... :("
    date: 2012-05-22
    body: "I met her once 6 years ago at aKademy Dublin and we became friends. A very nice and smart person. She'll be missed by all our friends.\r\n\r\nLeo"
    author: ""
  - subject: "You speak from my heart,"
    date: 2012-05-22
    body: "You speak from my heart, Valorie, I couldn't have expressed that better.\r\n\r\n\r\n"
    author: "Mamarok"
  - subject: "I feel very sad"
    date: 2012-05-23
    body: "I feel very sad to hear of this tragedy. May her soul be blessed and rest in peace. You will always be remembered Claire.\r\n\r\nRegards\r\n\r\nShreya\r\n"
    author: "shreyapandit"
  - subject: "very sad"
    date: 2012-05-23
    body: "It saddens me to hear of this tragic news. I wish all strength to her family in this tough time."
    author: "shreyapandit"
  - subject: "Sad to hear"
    date: 2012-06-16
    body: "It is sad to hear of the death of someone who helped the community like that.  Thank you to Claire for your efforts."
    author: ""
  - subject: "Huh...."
    date: 2012-07-17
    body: "I was just looking my old schoolmate up, just to find out she is dead....\r\nMy condolences for the family.\r\n\r\nErik Trenjeska Schutte"
    author: ""
  - subject: "Only just caught up with the"
    date: 2012-08-01
    body: "Only just caught up with the news. Very sad to lose someone like this"
    author: ""
  - subject: "R.I.P"
    date: 2012-08-02
    body: "R.I.P "
    author: ""
  - subject: "R.I.P.\nThanks, Claire."
    date: 2012-08-02
    body: "R.I.P.  \r\nThanks, Claire. "
    author: ""
  - subject: "Just heard this..."
    date: 2012-08-02
    body: "Speechless actually.. -nielsvm"
    author: ""
  - subject: "Claire"
    date: 2012-08-04
    body: "I was greatly saddened to hear about Claire.  We met at Trinity College at the KDE meeting back in 2006 and had a great time together.  She was so funny and energetic while shining brilliant in her own genuinely humble manner.  She will be greatly missed!"
    author: ""
  - subject: "RIP Claire"
    date: 2012-08-04
    body: "rest easy"
    author: ""
  - subject: "Learn and progress"
    date: 2012-08-04
    body: "We think we are so civilized and we think we know so much, but there is one area in which we hardly ever make any progress: many people consider \"death\" to be some kind of shameful and absolute ending as if one has lost a competition. Well, competition is meaningless but \"death\" is a wonderful opportunity to learn and progress.\r\n\r\nIn the West we still have this notion that we are \"born\" and eventually will \"die\". We repeat this notion over and over without realizing that we are not the body (a shell) we reside in.\r\n\r\nWouldn't it be more logical and sustainable to learn how to deal with the inevitability that we still call \"death\"? Both for ourselves and others?\r\n\r\nCongratulations with your transition, Claire!"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/claire_lotion.png" /><br /><strong>Claire Lotion</strong></div>
This week we heard about the death of KDE contributor Claire Lotion. People within the KDE Community were shocked and upset by this tragedy.

Claire was a vibrant person with strong ideals. She will be remembered for pursuing these ideals, and as a good friend and colleague. She also thought a lot about how KDE as an open source community could find connections to the real business world. Her energy, fresh look at things, positive mindset and intense commitment inspired many people. So did her dancing.

Claire spoke at major KDE conferences and was particularly active with the Multimedia meeting in Zundert, the Netherlands. Without her, the event would not have happened. She did the fundraising, handled the organizational matters, and managed to get people to and from the event location. It brought people together from all around the world (from as far away as Australia). Most had never met each other in person. It also changed how KDE meetings have been done since then, helping to shape the concept of developer sprints, now one of the most important activities in KDE.

We wish the family strength and comfort in dealing with this tragedy. The next major KDE software release (planned for the first of August) will be dedicated to Claire's memory. We will do everything in our power to make that release rock and make her proud of us.

