---
title: "KDE Enhances Mirroring Network"
date:    2012-03-26
authors:
  - "talbers"
slug:    kde-enhances-mirroring-network
comments:
  - subject: "Links don't render with FF 12 and IE 9!"
    date: 2012-03-27
    body: "Neither the \"page for analitza\" nor the \"current list of the official KDE mirrors\" render correctly using Firefox 12.0 or Internet Explorer 9.\r\n\r\nTested links:\r\n\r\nhttp://download.kde.org/stable/4.8.1/src/analitza-4.8.1.tar.bz2.mirrorlist\r\n\r\nhttp://download.kde.org/extra/mirrors.html"
    author: ""
  - subject: "Awesome!"
    date: 2012-03-27
    body: "Great job! The work on frameworks, websites and similar things is appreciated as much as doing some fancy UIs. Thank you to all involved!"
    author: ""
  - subject: "Screenshot?"
    date: 2012-03-29
    body: "Can you provide a screenshot of the mis-rendering you get in those browsers? It looks okay to me on Firefox 12, and I don't currently have access to IE."
    author: ""
---
<div style="float: right; margin: 1ex;"><a href="http://mirrorbrain.org/"><img src="http://dot.kde.org/sites/dot.kde.org/files/th-9_mirrorbrainlogo512x512.png" /></div>
KDE deployed a new mirror network this weekend. The mirror network is used to spread released software to users. In the old system, users had to select a mirror server manually. The new system selects a mirror automatically, based on the country where the user is located. If there is no mirror for that country, a mirror is selected based on continent. In either case, mirrors that are geographically closer are preferred. If there is an even better mirror—such as one in the same network as the user, it is preferred over all others. Here is the current list of the <a href="http://download.kde.org/extra/mirrors.html">official KDE mirrors</a>.
<!--break-->
<h2>Information about files</h2>
For each available file, the KDE mirror network has a special information page. For example, here is <a href="http://download.kde.org/stable/4.8.1/src/analitza-4.8.1.tar.bz2.mirrorlist">the page for analitza</a>, which holds a list of mirrors. But it is not just a simple list; it will only show the mirrors that are up and running, and the ones that can actually serve the file. It will look different to each user, listing the best mirrors depending on the user's location.

The information page also shows checksums, PGP signature (if available) and <a href="http://en.wikipedia.org/wiki/Metalink">Metalinks</a>. Clicking on a Metalink launches a download manager like KGet that starts downloading the file from several mirrors at the same time, by file section. The Metalink metadata holds information about checksum for each section to ensure that the complete file was downloaded correctly.

<h2>MirrorBrain</h2>
The software used for this KDE mirroring network is <a href="http://mirrorbrain.org/">MirrorBrain</a>. MirrorBrain also powers downloads of openSUSE, Libre Office and many others. Peter Pöml, the author of MirrorBrain, says, "6 years after its conception, I am still in love with MirrorBrain. A lot remains to be done, but it's great to see how it can help people solve their problems. If anyone wants to support the development, kindly <a href="http://mirrorbrain.org/donations/">donate to the project</a>."

The KDE Sysadmin team also decided to use MirrorBrain to make files.kde.org available via a mirroring system. Files.kde.org holds application data, such as additional maps for Marble and KStars, as well as Akademy videos and files related to Necessitas and debug packages for Windows builds. "It is becoming increasingly important for our users to update maps and other in-app data. With that demand growing, it makes sense to power this file distribution with a professional mirror network and keep it manageable for us.", according to Tom Albers from the KDE Sysadmin Team.

<h2>Faster downloads for all!</h2>
Thanks to MirrorBrain, KDE can leverage its extensive network of mirror providers more efficiently while providing an easier interface and faster download speeds to users. 

All in all, this means that users are saved the trouble of selecting a mirror, and they get faster and more reliable downloads which tax our network less.