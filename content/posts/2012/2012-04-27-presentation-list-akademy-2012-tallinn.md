---
title: "Presentation List for Akademy 2012 Tallinn"
date:    2012-04-27
authors:
  - "thiago"
slug:    presentation-list-akademy-2012-tallinn
comments:
  - subject: "So few talks"
    date: 2012-04-27
    body: "In Akademy 2010 we had around 28 talks per day, and now we have 28 talks for 2 days? Are we having just a single track?"
    author: "tsdgeos"
  - subject: "Missing talks from the list ?"
    date: 2012-05-01
    body: "Perhaps it is just me, but I do not see 28 talks mentioned over there."
    author: ""
  - subject: "Some sessions have not been published"
    date: 2012-05-01
    body: "You are correct. Not all of the talks are listed. For example, there is the traditional KDE Community keynote talk that is still in the works. The other sessions will be added to the list published at the <a href=\"http://akademy.kde.org/\">Akademy website</a>."
    author: "kallecarl"
  - subject: "Awesome"
    date: 2012-05-01
    body: "Great, thanks!"
    author: "djszapi"
  - subject: "Two parallel sessions"
    date: 2012-05-05
    body: "According to the following aKademy 2012 video, there will be two parallel sessions (exactly, from 4:37 on):\r\nhttp://www.youtube.com/watch?v=rSoX6DIaWlw&feature=youtu.be"
    author: "djszapi"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/HeaderScreenShotv3.png" /></div>
The Akademy 2012 Program Committee is proud to present the <a href="http://akademy.kde.org/conference-sessions">Akademy 2012 Sessions</a>. Given the broad nature of KDE, the proposals submitted contained a wealth of interesting and valuable topics. From those, 28 proposals were selected that we felt would address the most relevant topics and be deep enough in the areas of most interest to the KDE Community.
<!--break-->
<h2>Session focus areas</h2>

At Akademy 2012, there will be a lot of focus on KDE Frameworks 5, which will adapt the KDE codebase to the Qt 5 release. For that reason, the committee selected a series of talks relating to the Frameworks project and to Qt 5.

Plasma Workspaces is another important topic. So the schedule will include a grouping of talks about Plasma user interfaces, the project's flexibility and the baby of the family—Plasma Active. We also included talks about open devices based on KDE technologies. In addition, there are sessions discussing a range of other KDE technologies and projects, and the Community's outreach to vendors and efforts to enlarge our ecosystem.

<h2>Full schedule overview</h2>

Please check out all of the <a href="http://akademy.kde.org/conference-sessions">sessions and the keynotes</a> offered during the first two days of Akademy. As the <a href="http://akademy.kde.org/schedule-overview">schedule overview</a> shows, the four days following the sessions will consist of Birds of a Feather (BoF) sessions, other workshops and hacking sessions. Collaborators are encouraged to begin considering topics for those sessions. Some special workshops are already planned. There will be opportunities for attendees to schedule BoFs before and during Akademy.

<h2>Attend Akademy 2012</h2>

If you're planning to attend Akademy 2012 in Tallinn (or have been part of past Akademies), please blog about Akademy, your reasons, your expectations and your experiences. "I'm going" badges are available at the <a href="http://community.kde.org/Akademy/2012">Akademy 2012 Tallinn wiki</a>. Your contribution may make the difference between someone attending or not.

Akademy offers a rare opportunity for inspired, intensive work on important projects. Yours could be one of those. <a href="http://akademy.kde.org/how-to-register">Register</a> now and <a href="http://akademy.kde.org/hotel-travel">book your travel</a> soon, because <a href="http://akademy.kde.org/">Akademy 2012</a> will take place at the height of the vacation season in Estonia. 

<h2>The Program Committee</h2>
 Aaron Seigo
 Celeste Lyn Paul
 Kevin Krammer
 Lydia Pintscher
 Thiago Macieira

<h2>Akademy 2012 Tallin, Estonia</h2>

For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.