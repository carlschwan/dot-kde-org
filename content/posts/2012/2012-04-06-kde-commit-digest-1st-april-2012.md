---
title: "KDE Commit-Digest for 1st April 2012"
date:    2012-04-06
authors:
  - "vladislavb"
slug:    kde-commit-digest-1st-april-2012
---
In <a href="http://commit-digest.org/issues/2012-04-01/">this week's KDE Commit-Digest</a>:

              <ul><li>KDev-Python's <a href="http://scummos.blogspot.com/2012/03/debugging-support-in-kdev-python.html">new debugger</a> gains a DUChain-supported local declaration list</li>
<li><a href="http://www.digikam.org/">Digikam</a> adds <a href="http://loc.alize.us/">loc.alize.us</a> support to their geolocation service</li>
<li><a href="http://okular.org/">Okular</a> now remembers exact bookmark positions and more than one bookmark per page</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> adds Global Shortcut support for scripts and scripted effects and cursor-key navigation to Flip Switch and Cover Switch effects</li>
<!--break-->
<li><a href="http://userbase.kde.org/Plasma/Kickoff">Kickoff</a> can now be used only with a keyboard</li>
<li>Katepart improves their auto-bracketing feature</li>
<li><a href="http://community.kde.org/Plasma">Plasma</a> applet <a href="http://kde-apps.org/content/show.php/Mini+Player?content=95501">Miniplayer</a> reaches version 2.3, adding a track properties dialog, improved filtering and other improvements</li>
<li><a href="http://nepomuk.kde.org/">NEPOMUK</a> enables <a href="https://trueg.wordpress.com/2012/04/03/nepomuk-tasks-let-the-virtuoso-inferencing-begin/">Virtuoso inference</a> on all queries and includes optimization fixes</li>
<li><a href="http://www.calligra.org/">Calligra</a> further unifies toolbox behavior across all its programs, implements support for loading/saving layer locks in the <a href="http://en.wikipedia.org/wiki/OpenRaster">OpenRaster file format (ORA)</a> and includes numerous bugfixes</li>
<li>Calligra's <a href="http://krita.org">Krita</a> adds a textured painting option for brushes inspired by <a href="http://www.davidrevoy.com/index.php?article107/textured-brush-in-floss-digital-painting">David Revoy's article</a></li>
<li><a href="http://amarok.kde.org/">Amarok</a> adds the ability to move track position bookmarks by dragging</li>
<li><a href="http://konversation.kde.org/">Konversation</a> adds an option to restrict logging to private conversations</li>
<li><a href="http://userbase.kde.org/KMLDonkey">KMLDonkey</a> releases 2.1.2 and 2.1.3, including various bugfixes, optimizations and new features</li>
<li><a href="http://techbase.kde.org/Development/Tutorials/KAuth/KAuth_Basics">KAuth</a> introducing unit tests, completing all the needed requirements to be ready for being part of frameworks</li>
<li>KSCD, <a href="http://developer.kde.org/~wheeler/juk.html">JuK</a>, and other applications in KDE Multimedia can now build outside of KMM</li>
<li>Plasma Mediacenter improves its playlist</li>
<li>Bugfixes in <a href="http://www.kde.org/applications/utilities/jovie/">Jovie</a>, <a href="http://www.kdevelop.org/">KDevelop</a>, KDE-Workspace, <a href="http://pim.kde.org/">KDE-PIM</a>, <a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a>, Phonon-Gstreamer and Phonon-VLC.</li>
</ul>

                <a href="http://commit-digest.org/issues/2012-04-01/">Read the rest of the Digest here</a>.