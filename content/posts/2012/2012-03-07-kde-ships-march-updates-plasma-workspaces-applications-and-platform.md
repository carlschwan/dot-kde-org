---
title: "KDE Ships March Updates to Plasma Workspaces, Applications and Platform"
date:    2012-03-07
authors:
  - "sebas"
slug:    kde-ships-march-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "KDEPIM"
    date: 2012-03-08
    body: "perhaps i missed it, but there no entries for KDEPIM in the changelog. There was a blog post recently about many bugfixes for KDEPIM 4.8.1.\r\n\r\nIf somebody only reads the official announcement, it appears that KDEPIM didn't make any progress.\r\n\r\nThe changelog exists, all that's needed is to copy/paste it into the official announcement changelog. Anyway, here it is:\r\nhttp://cgbdx.wordpress.com/2012/03/01/kdepim-bug-fixes-in-kde-4-8-1-8/\r\n\r\nKeep up the great work! Can't wait for the kubuntu packages to land."
    author: ""
  - subject: "yup yup"
    date: 2012-03-08
    body: "Often the changelogs don't get updated, indeed. At least Dolphin has its impressively long list of fixes in there :D"
    author: "jospoortvliet"
  - subject: "Can't wait for the kubuntu packages to land."
    date: 2012-03-08
    body: "You can get them right now if you add the PPA  repository as outlined at the link below. It is the same as for 4.8.0.\r\n\r\nhttp://www.kubuntu.org/news/kde-sc-4.8.0\r\n\r\nI did the upgrade yesterday and I noticed good performance improvement for some reason.\r\n\r\n"
    author: "Abe"
  - subject: "Probably because of the"
    date: 2012-03-09
    body: "Probably because of the nepomuk performance increase. Going to test it today"
    author: ""
  - subject: "Yup yup, packages are"
    date: 2012-03-09
    body: "Yup yup, packages are becoming available, in Fedora:\r\nhttp://isemenov.blogspot.com/2012/03/kde-481-available-in-fedora.html\r\n\r\nand openSUSE:\r\nhttp://kdeatopensuse.wordpress.com/2012/03/07/kde-sc-4-8-1-packages-for-opensuse/\r\n(openSUSE users who are subscribed to the KDE 4.8 repository of course don't have to do anything other than their regular update)"
    author: "jospoortvliet"
  - subject: "Must be some thing else"
    date: 2012-03-09
    body: "Because I am not running nepomuk on this old clunker. I use this machine specially for testing performance. It is so slow any small performance increase is noticeable.\r\n\r\n It must be some code optimization.\r\n"
    author: "Abe"
---
Today KDE <a href="http://kde.org/announcements/announce-4.8.1.php">released</a> updates for its Workspaces, Applications, and Development Platform. These updates are the first in a series of monthly stabilization updates to the 4.8 series. 4.8.1 updates bring many bugfixes and translation updates on top of the latest edition in the 4.8 series and are recommended updates for everyone running 4.8.0 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come. The March updates contain many performance improvements and bugfixes for applications using the Nepomuk semantic framework.
<!--break-->
