---
title: "KDE Makes Second 4.8 Release Candidate Available"
date:    2012-01-05
authors:
  - "sebas"
slug:    kde-makes-second-48-release-candidate-available
comments:
  - subject: "Bugs"
    date: 2012-01-05
    body: "RC2 definitely looks better than RC1, some really bad bugs, such as kmix crashing when pulseaudio is enabled (default on most distributions by now) seem to be gone. Also dolphin seems to be a little bit less crashy by now.  \r\n\r\nHowever, amaroks plasma parts are still broken since a kdelibs commit shortly before RC1, so if this stays in until release, I think a lot of users will have problems. \r\n\r\nThank you for your ongoing good work, however, a better QA system as discussed on the release team mailing list is imho necessary, so that some bugs don't make it into an official release. "
    author: ""
  - subject: "Looking quite forward to KDE"
    date: 2012-01-05
    body: "Looking quite forward to KDE 4.8, thanks for all your hard work!\r\n\r\nTwo small things:\r\n\r\n1. It's KSecretsService, although KSecretService sounds cooler ;-)\r\n\r\n2. I wish KDE would start using \"release candidate\" as being an actual candidate for release. This RC is not something you would consider simply renaming to 4.8, so it's by definition not a release candidate except in name. KDE has this strange tradition of calling late betas \"release candidates\", which has the effect that there are no <strong>actual</strong> candidates for release except the actual release. Just a pet peeve of mine."
    author: "Haakon"
  - subject: "Our QA system are our users"
    date: 2012-01-05
    body: "<cite>a better QA system as discussed on the release team mailing list is imho necessary, so that some bugs don't make it into an official release.</cite>\r\nThis QA system has to come from our users. Only if users test and report bugs we can fix them. We need users always trying the latest versions already during development so that bugs are spotted in a timely manner.\r\n\r\nThere are many ways how you could test, be it <a href=\"http://techbase.kde.org/Getting_Started/Using_Project_Neon_to_contribute_to_KDE\">Project Neon</a> or <a href=\"http://en.opensuse.org/KDE_repositories#Unstable:SC_aka._KUSC_.28KDE_trunk.29\">Unstable repo on openSUSE</a>."
    author: "mgraesslin"
  - subject: "Re: Bugs"
    date: 2012-01-05
    body: "> Also dolphin seems to be a little bit less crashy by now.\r\n\r\nPlease report any Dolphin crashes that occur with RC2 to bugs.kde.org. I'm not aware currently of any crash that is related to the new view-engine that has not been fixed yet with RC2. Of course this does not mean that there are no issues left but \"a little bit less crashy\" is not enough information for us developers to be aware how to reproduce the crashes and to fix them ;-) Thanks!"
    author: "ppenz"
  - subject: "he still has a point"
    date: 2012-01-05
    body: "I don't think having more ways to test should fix it all. Stuff slips in between the releases that shouldn't have to be there. If the process is somewhat broken, adding more testers is not the solution.\r\n\r\nYou can take this QA stuff for granted, mention it's \"early stuff\", \"still a .0\" release instead of .1 or .2 release or \"users should report bugs\" and what not else. However, the point remains that we can all look honestly to ourselves, and ask the question: \"How can we do better?\"\r\n\r\nOn so many levels you'd be surprised with what creative solutions your mind can come when you let it ask the question (and ponder about the answers), instead of letting your mind think about \"why it is like this, period.\". In both cases, you'll find answers. One is more productive though. :)\r\n\r\nHence, I'm really happy to read these discussions about a QA process, and whole hearty welcome it to improve the releases."
    author: ""
  - subject: "--vdboor"
    date: 2012-01-05
    body: "oops, what was me. Forgot to login,"
    author: "vdboor"
  - subject: "Hi Martin"
    date: 2012-01-06
    body: "Being on gentoo with live ebuilds, submitting about 3-5 bug reports per release, I think I would be one of them. \r\n\r\nHowever, some bugs I reported tend to be happily ignored for quite a while, and then fixed after the release, having everybody using binary distributions to wait for a bit until the fixes come in. \r\n\r\nThat's what should happen _before_ a release, and yes, that would mean breaking the release schedule. "
    author: ""
  - subject: "Hi Peter, "
    date: 2012-01-06
    body: "Thanks, I reported all bugs I found with dolphin so far, and you fixed many of them. You even implemented some of my feature requests, so I am quite happy. So thank you very much for your good work, and yes, I will keep submitting them every time I have the chance to. "
    author: ""
  - subject: "I agree, but ..."
    date: 2012-01-06
    body: ">This QA system has to come from our users. Only if users test and \r\n>report bugs we can fix them. We need users always trying the latest \r\n>versions already during development so that bugs are spotted in a \r\n>timely manner.\r\nI agree, but the since KDE4 testing packages of Beta and RC-packages often came late or not at all. At least for Kubuntu. This has luckily improved for the 4.8-testing-cycle and I hope it stays that way. For Debian testing packages are not available at all. I might remember this wrong, but from my feeling the overall testing opportunities have descreased in KDE4 compared to KDE3.\r\n\r\nThe other thing is that I get the feeling even though bugs and regressions are reported during the test cycle they are not always really taken care of. I assume this has some this has something to do with the lack of manpower in some parts of KDE which are not so much maintained. But nevertheless it is sometimes frustraiting as user. Lateley especially with Kmail. Although many bugs are fixed now, this is what should happened during a limited testing cycle and should have never made it into a release (at least not without Kmail1 as an alternative along)."
    author: "mark"
  - subject: "Dolphis is not crashing that"
    date: 2012-01-06
    body: "Dolphis is not crashing that much but it's not saving any setting.\r\nOverall RC2 is not bad but it's still not up to RC quality. \r\nI have problems with plasma freezing (even though winter is not that cold this year) and crashing but still things are shaping up."
    author: "Bobby"
  - subject: "What Amarok plasma parts are broken?"
    date: 2012-01-07
    body: "I'm running KDE 4.8 RC2 and I don't see any breakage with Amarok. What are you experiencing?"
    author: "Alejandro Nova"
  - subject: "Even when the user report"
    date: 2012-01-07
    body: "Even when the user report bugs it is not always fixed becouse lag of man power. Kde 4 still have a lot of known problem with Multi-Screen-Setup and it looks like it will take a long time to fix:\r\nhttps://bugs.kde.org/show_bug.cgi?id=256242\r\n\r\nAt this moment in time I am depending on Multi-Screen-Setup since I have 2 graphics cards to power my 3 screens. For my kde 4 is still to buggy to use on daily basis.\r\nI will return to kde 4 when either Multi-Screen-Setup is working nicly again (it did in KDE 3 days) or I can use another way to conf my screens (wayland maybe but that will req Nvidia to support it. And they dont have any plans for it)\r\n"
    author: ""
  - subject: "Multi-Screen is working fine"
    date: 2012-01-07
    body: "First of all: to not confuse users please talk about Multi-Head or Separate X Server per screen. The multi-screen modes our users know (xrandr/twinview) is working fine!\r\n\r\nYou already brought up Multi-Head in the dot story for beta 2. Repeating this in each release note won't make your feature get implemented: the opposite is the case. I explained to you and others why we won't add Multi-Head support and this is not a bug, but missing features!\r\n\r\n<cite>I will return to kde 4 when either Multi-Screen-Setup is working nicly again (it did in KDE 3 days)</cite>\r\nThis is just not true. I went through old bug reports still open for KWin and most of our really old bug reports are about Multi-Head and all of them have the note from former KWin maintainer that Multi-Head is NOT supported in KDE 3 (see e.g. <a href=\"https://bugs.kde.org/show_bug.cgi?id=134677#c1\">Bug 134677</a>)! We can live in the illusion that the grass has always been greener on the other side of the fence but when we look at it properly we see that it is just not the case."
    author: "mgraesslin"
  - subject: "Use Project Neon"
    date: 2012-01-07
    body: "If you are using Kubuntu, you can use Project Neon to get daily builds. It is so important to get bugs reported in a timely manner, that is best the same day as the bug is introduced. The longer the difference between bug introduced and bug reported, the more difficult it gets to find it and fix it.\r\n\r\n<cite>The other thing is that I get the feeling even though bugs and regressions are reported during the test cycle they are not always really taken care of. </cite>\r\nNot everybody has always time. Many KDE developers are volunteers. Think about students having to learn for exams, think about developers being on vacation or spending Christmas time with their family. This just shows again how important it is as a developer to get the bugs reported in a timely manner, when you have time during development."
    author: "mgraesslin"
  - subject: "Christmas holidays"
    date: 2012-01-07
    body: "It was lots of fun to watch the commit mailing list between boxing day and new year's eve -- all kinds of projects suddenly doubled or tripled their commit rate. Vacation! Holidays! Delicious hacking time!\r\n\r\nFor Krita we went from four to one release critical bugs in those five days, not to mention many, many other bug fixes!\r\n\r\nBecause, yes, most of us have a job these days, or our universities want us to finally hurry up a bit."
    author: "Boudewijn Rempt"
  - subject: "KDE 4.8 RC2, netbook interface (Acer Aspire One, openSUSE 12.1)"
    date: 2012-01-07
    body: "Snappy response, finally giving netbook interface elegance, all generally working well.  \r\n\r\nThank you\r\n\r\nMy big question remains about my desktop and kmail 4.8 - generally resisting upgrade from openSUSE 11.4, kmail 4.4.10 - not much information around.\r\n\r\nDoes anyone know the current state of play?\r\n\r\n\r\n\r\n"
    author: ""
  - subject: "if xrandr and twinview is"
    date: 2012-01-07
    body: "if xrandr and twinview is working fine it must be me how are unable to configure it correctly and therefor I could use some help.\r\nI have a geforce 560 ti to power middlescreen and a geforc 9600 to power right and left screen. Any idear how can set it up to run nicely with xrandr or twinview.\r\n\r\nAs described in the bug report it was unmaintained, eg it was still present bu noone would like to fix bug in it (or I am wrong again).\r\n\r\nSo we have different understanding of what a bug is bug I can accept it is call semi-missing features (some part is there but other part is stil broken). \r\n\r\nMy point of the prevous post was not to get people to fix it but to highligt that even that users contribute with information it will not necessarily be fixed.\r\n\r\nSorry for my onw incompitent makeing mu onw point clear, fixing semi-missing features, and settup  xrandr and twinview using to cards."
    author: ""
  - subject: "Complain to X not to KDE"
    date: 2012-01-07
    body: "XRandR support for multiple cards is missing in X. Nothing we can do about. XRandR/TwinView with one card is working wonderful.\r\n\r\nBut Multi-Head is just a different story. These are not bugs, but missing features. KDE never supported Multi-Head properly. So no matter how much you point to this bug report, it doesn't change the situation. And even if we consider these to be bugs, it is not relevant as it affects only a very small group of our userbase. We fix important bugs first and Multi-Head support is not on our priority list."
    author: "mgraesslin"
  - subject: "forum.kde.org"
    date: 2012-01-07
    body: "KDE forums are intended for KDE users to get and give support.\r\n\r\nPosting your request for assistance at forum.kde.org will be more fruitful than bringing up an issue repeatedly in Dot comments or in bug comments. Complaining about KDE and bad-mouthing developers makes the issue less attractive to work on.\r\n\r\nAdding extraneous and duplicate information only makes it more difficult to address real issues.\r\n\r\nThere is a clear, reasoned statement in comment #1 in the cited bug report that this is not a high priority. Further complaining and additional duplicate bug reports are pointless. Actually they get in the way of progress.\r\n\r\nFor people who are sincere about improving KDE quality and want to do something constructive, please visit http://community.kde.org/Getinvolved Click on \"Development\". Under \"Tasks\", there is information about working with bugs. Major technical skill is not necessarily required.\r\n "
    author: "kallecarl"
  - subject: "I think kmail 4.8 is now good"
    date: 2012-01-07
    body: "I think kmail 4.8 is now good enough, especially as it no longer tries to convert your old config which was prone to failure, but imports it instead.  It's a bit more work but a lot safer.  Take a backup, a deep breath and go for it.  I'm using 4.7.4 now, and it finally works for me, but only after completely clearing out my old config and re-importing all my mail."
    author: "odysseus"
  - subject: "can u test kleopatra?"
    date: 2012-01-08
    body: "when adding gnupg signatures the owner-trust was not changeable. (disabled ok butten).\r\nIs this bug (reported early 2011) fixed?"
    author: ""
  - subject: "You are wrong"
    date: 2012-01-11
    body: "That kind of minset is bad and it's what it's hurting us as a community, I suggest you take some time to think about it.\r\n\r\nI know users should also play their role and contribute more. That's another problem.\r\n\r\nBUT ALL BUGS SHOULD BE IMPORTANT, WITHOUT EXCEPTIONS."
    author: ""
  - subject: "Nice"
    date: 2012-01-11
    body: "I wish more people were like you, I congratulate you for that.\r\n\r\nWe as users have to keep reporting bugs or even try to fix the bugs ourselves.\r\n\r\nThank you so much for that.\r\n\r\nKeep up the great work!"
    author: ""
  - subject: "did you already report the"
    date: 2012-01-11
    body: "did you already report the bug to Xorg?"
    author: ""
  - subject: "These are missing features and no bugs"
    date: 2012-01-11
    body: "Please don't make the mistake to think that a missing feature is a bug. KDE had never properly supported Multi-Head. The feature \"Multi-Head\" is just missing. A non-existing feature is not a bug. The fact that there is some code for Multi-Head does not mean that the feature exists or is supported. If you disagree in that point then there is only the solution of removing all the broken and unsupported Multi-Head related code.\r\n\r\nAdding support for Multi-Head is by that an implementation of a new feature and not bug fixing. This means the rules for implementing new features applies and not those for bug fixing.\r\n\r\nFeatures are mostly implemented if they are important for the goals of the project and what the developers want to achieve. Another possibility is that someone implements it \"to scratch his own itch\".\r\n\r\nIn case of Multi-Head I am sorry to say and as I wrote before that it is not any priority of the current development team. Multi-Head has a very limited use case and we have to decide carefully where to spend the limited resources for. Multi-Head is considering our userbase quite unimportant. Also nobody of the development team uses a Multi-Head setup and at least I consider it as a bad choice for user setups (there are special cases where it makes sense).\r\n\r\nOver the last four years I have been involved with KWin only one user drafted a patch to improve the situation. This shows quite nicely that even the users demanding the support do not care about it - or do not exist.\r\n\r\nIf you are unhappy that support for Multi-Head is not implemented properly, you will have to implement it. Don't expect that spare time developers implement features completely useless for themselves. If you are not able to implement it yourself there is still the possibility to contract developers to implement it."
    author: "mgraesslin"
  - subject: "> KDE has this strange"
    date: 2012-01-12
    body: "> KDE has this strange tradition of calling late betas \"release\r\n> candidates\", which has the effect that there are no actual\r\n> candidates for release except the actual release.\r\n\r\nYes, I really second that!\r\n\r\nThis way more than one nasty or stupid bug slipped in and although often fixed soon afterwards all users of binary packages had to rely on their packagers to incorporate a hotfix, which never always happens for all available distributions.\r\n\r\nFrom a user's point of view, that's a rather unsatisfying situation."
    author: ""
  - subject: "Multiple/single cards."
    date: 2012-01-18
    body: "Hello,\r\nI know I'm late to te party but I have one question, if anybody is reading this. If I understood correctly there is no way with KDE to use several monitors across several cards. I'm in the process of buying a new pc and was thinking about a dual monitor setup. How can I do that? Should I buy a card with two connectors? What if I wanted to have three or four monitors?\r\n\r\nThanks\r\nBye\r\n\r\nPS: About the three/four monitors. I'm just curious and, just to be clear, the whole posting has no polemic intent. Just trying to figure out how the multimonitor stuff works."
    author: ""
  - subject: "where to get an answer"
    date: 2012-01-18
    body: "dot.kde.org is primarily for news. There are other sites that are better for the kind of information you want.\r\n\r\nhttp://forum.kde.org/\r\n\r\nhttp://lmgtfy.com/?q=kde+multiple+monitor"
    author: "kallecarl"
---
Today KDE released the second release candidate for its renewed Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing new and old functionality. Please give this release another good round of testing to help us release a rock-solid 4.8 later this month.<br />
<!--break-->
Some 4.8 highlights are:

<ul>
    <li>
    Qt Quick in Plasma Workspaces—Qt Quick is making its way into the Plasma Workspaces, the new Plasma Components provide a standardized API implementation of widgets with native Plasma Look and Feel. The device notifier widget has been ported using these components and is now written in pure QML. KWin's window switcher is now also QML-based, paving the way for newly designed window switchers.
    </li>
    <li>
    Dolphin's file view has been rewritten for better performance, scalability and more attractive visual appearance.
    </li>
    <li>
    KSecretService optionally enables a shared password storage, therefore making your saved passwords available to many other applicatios, leading to a more secure system and better integration of non-KDE apps into the Plasma Workspaces and KDE apps into non-Plasma workspaces.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making the KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>

Bugs can be reported using <a href="http://bugs.kde.org">KDE's Bugzilla</a>.