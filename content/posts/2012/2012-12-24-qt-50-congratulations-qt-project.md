---
title: "Qt 5.0 - Congratulations to the Qt Project "
date:    2012-12-24
authors:
  - "votick"
slug:    qt-50-congratulations-qt-project
comments:
  - subject: "A wonderful Christmas gift."
    date: 2012-12-27
    body: "A wonderful Christmas gift. Thanks for the hard work of everyone involved!"
    author: "Haakon"
  - subject: "Congratulations"
    date: 2013-01-10
    body: "Very happy to hear the news. Thank you all for making it happen..:)"
    author: "thiyagi"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>
Since its beginning in 1996, KDE has developed as an organization creating a wide range of software. From the start, KDE relied on Qt, the toolkit that helps to power KDE’s software. KDE and Qt have always worked together closely. This partnership has grown even closer since Qt development moved to the <a href="http://qt-project.org/">Qt Project</a>. The quality of current and future KDE applications depends on innovation and improvements within Qt. A few days ago, the Qt Project achieved a major milestone—the release of Qt 5.0. The release included significant contributions from KDE Community members. Congratulations to everyone who made a contribution!

<h2>Notable features of Qt 5</h2>
<ul>
<li>More flexibility within QML, Qt Widget, Javascript and C++</li>
<li>Modularized codebase and consolidated platform abstraction</li>
<li>Dramatic improvements to graphics and and overall performance, including constrained hardware environments</li>
<li>Improved connectivity capabilities</li>
<li>User-friendly content handling</li>
<li>JSON support</li>
<li>New location APIs</li>
<li>Intuitive development interfaces for better coding efficiency</li>
</ul>

This release and the Qt ecosystem strongly support the continued growth of KDE and its development. KDE teams, projects, developers, and associates are grateful for the quality, consistency, efficiency, creativity and reliability of Qt.

KDE e.V. board member Lydia Pintscher said, “2012 was not an easy year for the Qt ecosystem. But thanks to the strong support for the Qt Project from both commercial and non-commercial partners, it flourished and has a future that seems brighter than ever. The release of Qt 5.0 is a testament to this.”

Congratulations and thanks to the Qt Project for great software and a strong partnership with KDE.