---
title: "KDE Commit-Digest for 15th April 2012"
date:    2012-04-22
authors:
  - "mrybczyn"
slug:    kde-commit-digest-15th-april-2012
---
In <a href="http://commit-digest.org/issues/2012-04-15/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://dolphin.kde.org/">Dolphin</a> now uses KMessageWidget for error and information messages, and has faster file sorting</li>
<li>Initial Fuse backend implementation for <a href="http://solid.kde.org/">Solid</a></li>
<li>It is now possible to save images from HTML mails in <a href="http://kontact.kde.org/kmail/">KMail</a></li>
<li>OSRM (Open Source Routing Machine) is supported as routing backend in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li><a href="http://userbase.kde.org/Lokalize">Lokalize</a> adds basic support for QT Linguist .ts files</li>
<li>Improved sorting by meridian transit time / observation time for an observation session in <a href="http://edu.kde.org/kstars/">KStars</a>; improved DSS image retrieval feature.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-04-15/">Read the rest of the Digest here</a>.