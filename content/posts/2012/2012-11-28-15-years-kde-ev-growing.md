---
title: "15 years of KDE e.V. - Growing Up"
date:    2012-11-28
authors:
  - "jospoortvliet"
slug:    15-years-kde-ev-growing
---
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://ev.kde.org"><img src="http://dot.kde.org/sites/dot.kde.org/files/ev-LogoAt15Shaded4.png" width="200" /></div>
Yesterday (November 27, 2012) was the 15th birthday of KDE e.V. (eingetragener Verein; registered association), the legal entity which represents the KDE Community in legal and financial matters. We <a href="http://dot.kde.org/2012/11/27/15-years-kde-ev-early-years">published interviews</a> with two of the founding members (Matthias Ettrich and Kalle Dalheimer) on the why, what and when of KDE e.V. in the beginning. Today, emeritus board member Mirko Böhm shares his thoughts in the video interview (transcript included). Tomorrow there will be interviews with current e.V. Board members.

<iframe width="640" height="360" src="http://www.youtube.com/embed/ti1LsN9uysw" frameborder="0" allowfullscreen></iframe>
<a href="http://files.kde.org/promo/Mirko%20Boehm.webm">direct download</a>
(Transcript below)

As bonus, we have for you a short video showing the KDE desktop progressing through the last 15+ years:
<iframe width="640" height="360" src="http://www.youtube.com/embed/XWMYVN1fl9k" frameborder="0" allowfullscreen></iframe>

<h2>Join the Game</h2>
Please consider supporting KDE e.V. by becoming a <a href="http://jointhegame.kde.org/">supporting member</a>. We're playing a big game and invite you to join it.

<hr>

<h2>Transcript - Mirko Böhm on the e.V. and KDE's future</h2>
I am <strong>Camila</strong>. I’m here with Mirko Böhm to talk about KDE e.V. that’s completing 15 years this year, 2012. How did you get involved with KDE? How did you start?

<strong>Mirko:</strong> Well I was young and I needed the money … 
Um … no, that’s not quite it.

It was 96 or 97 and I had to do programming classes at the University and I didn’t feel like completing exercises and handing them in just to throw them away afterwards. So I was looking for a reasonable project to invest my time in and that was about when KDE started. And so I decided to, well, learn programming more or less on KDE. That’s how I got involved, so I initially was a developer for quite some time. And then in 1999, Martin Konold asked me to run as a Board member. Mostly because I also had somewhat of a hand to get things done and to help other people organize their work. So it was a good fit, I think. Then I was on the Board from 1999 to 2006.

<strong>Camila:</strong> What’s the job of KDE e.V. and its Board?

<strong>Mirko:</strong> Well KDE e.V. is a support organization for the KDE Community. So KDE builds … well at the time, it was a desktop project and we were developing the new Linux desktop. It was in bad shape before we started. And KDE e.V. was founded to support the community with the kinds of tasks you need a legal entity for. For example, to accept donations, to engage into contracts, to organize events and conferences and these kinds of things. It’s usually easier to get this done with an organization behind you as it really represents more than individual people and has more continuity.

<strong>Camila:</strong> So you have been involved for quite a long time in KDE, in the KDE Community, and the KDE e.V. So what makes KDE special?

<strong>Mirko:</strong> What makes it special in my opinion is that KDE e.V. is run by the Community. We have no single company or important sponsor who is influencing how KDE is developed. That is also the reasoning behind the understanding that KDE e.V. supports KDE, but doesn’t for example set the technical direction. So the KDE Community is today still relatively independent in the direction in which KDE is developed. And KDE e.V. takes a passive stance in supporting the Community to get this done. This is different from the setup of many other organizations where either the central group takes a very active influence on where the project is going or where a dominating sponsor over time starts to influence where the project goes. I think that this is one of the reasons why the KDE Community has succeeded over such a long period of time—more than 15 years now—was able to continue innovating, to focus on technical merit during development, to still attract developers and I think get to where it is today. I think that makes KDE e.V. special. And it’s a successful model of how an organization can support a community in getting its work done.

<strong>Camila:</strong> What challenges do you see for the future of KDE e.V.?

<strong>Mirko:</strong> Challenges for the future of KDE e.V. … Well first of all, since KDE e.V. is a supporting organization, it doesn’t really face challenges on its own, or not directly. The challenges are on KDE as a community, where at the moment we’re changing from developing the desktop, or being the desktop project for the Free Software environment. We’re changing towards becoming like an ecosystem of communities with a common interest developing applications on the desktop for free software users. So KDE is changing and KDE e.V. needs to follow along with these changes. To continue to represent the Community. And since the Community will become more diverse and will attract other groups that so far were maybe independent, it needs to change so that it still represents everybody who is involved in KDE. And for that, I think, it might need to be even more representative for the different parts of the communities. And another thing is at the moment, the biggest influence in KDE in general are still technical contributors and we need make sure that we hear all the voices and are listening to everybody who has an important say and contributions to KDE.

<strong>Camila:</strong> What plans do you have for yourself in the future with KDE and open source?

<strong>Mirko:</strong> Okay. Well last year I started teaching at the University in Berlin about open source and intellectual property. One of the things I research there is how open source communities are governed or run. And what actually makes a really open open source community. We know that it’s not just about the source code. It’s also about how the community works. On one hand, KDE is a great sample for this kind of research, because it’s one of the communities that is really run by volunteers and has found its own way of how it works. On the other hand, I think that KDE can benefit from this research as it can help shape where this new setup for the community can go. How we can integrate all these other projects better. For example, how we can model KDE e.V. for the future. It will kind of be more complex contributions in terms of more thinking and more writing and less coding. But I still think it’s going to be fun and KDE will benefit from it.

<strong>Camila</strong>:How do you feel when you think about what KDE has become?

<strong>Mirko:</strong> I’m really proud of what KDE has achieved. So we always have to keep in mind where we came from. In 96, 97, <a href="http://en.wikipedia.org/wiki/FVWM">FWVM</a> was the dominating window manager and if you’ve ever used it, it was not fun. And KDE set out to revolutionize the free software desktop, to make it fun and productive. And I think we’ve achieved that. We’ve had four iterations of the desktop environment. They were all innovative. They were all interesting and great achievements. So from that point of view, I’m really proud of what KDE has achieved. I’m also really excited about these changes to become even more inviting to other projects that want to work on the Linux desktop. So yeah, I’m quite proud of what we have achieved and I’m looking forward to the next years of KDE.

<strong>Camila:</strong> Thank you, Mirko. [toasting] To KDE e.V. and 15 years. KDE and 15 years. Yeah.

<i>(transcript by Carl Symons)</i>