---
title: "KDE - Best Desktop Environment of the Year"
date:    2012-03-05
authors:
  - "heath"
slug:    kde-best-desktop-environment-year
comments:
  - subject: "Awesomeness!!!"
    date: 2012-03-05
    body: "Congrats to KDE from the most fun KDE distro, openSUSE ;-)\r\n\r\nYou girls & guys just keep making it better... And we're happy to ship it! Keep it up!"
    author: "jospoortvliet"
  - subject: "Congratulations"
    date: 2012-03-05
    body: "From Argentina, using Archlinux (the best KDE distro, in my opinion).\r\n\r\nCongratulation to all family (developers, users, bloggers, etc).\r\n\r\n"
    author: ""
  - subject: "Huh, stability?"
    date: 2012-03-05
    body: "I also like KDE a lot. But it is far from stable. Akregator often crashes several times a day (so does Konqueror, probably they use the same code). Dolphin acts weird and also crashes a lot. Plasma does weird things, too, often hangs, especially when I try custom plasmoids I downloa. Nepomuk... is it usable yet? Not for me. And then the KDEPIM mess... at least it did not eat my mails, as it happened for others. I like KMail, but I'm using Claws most of the time, which works as expected. Just right now KMail suddenly refuses to show any content in the gentoo-user mailing list, and I get an 'Unable to fetch from backend' error from time to time. Whatever. \r\nI still like KDE, it's very configurable and has so many features I do not want to miss, like window groups, or window preferences for specific applications.  But I wouldn't suggest it to people who are inexperienced, and do not know what to do when unexpected things happen. Which happens all the time.\r\nAnyway, congratulations for the award :)"
    author: "Wonko"
  - subject: "And Choqok the best of twitter client - by filipesaraiva"
    date: 2012-03-05
    body: "Yes, KDE is great and, choqok received a awards too!\r\nLifeHacker chose choqok as the best twitter client in Linux world!\r\nhttp://lifehacker.com/5879964/the-best-twitter-client-for-linux"
    author: ""
  - subject: "Definitely my favorite DE"
    date: 2012-03-05
    body: "I tried a few times KDE on Kubuntu and it was unstable back then, mostly because of Nepomuk. I tried openSuse but I had problems with hardware support.\r\nBut since KDE 4.7 I never had a crash, happily using Kubuntu 11.10 with KDE 4.8. I went through it's \"System Settings\", took a look at every setting and just felt in love :)"
    author: ""
  - subject: "DigiKam,Okular and K3B"
    date: 2012-03-06
    body: "Life hacker's choice !!! \r\nDigikam : Best Photo management app http://lifehacker.com/5877908/the-best-photo-management-app-for-linux\r\n\r\nOkular: Best PDF Reader \r\nhttp://lifehacker.com/5875879/the-best-pdf-viewer-for-linux\r\n\r\nK3B: Best Disk burning app\r\nhttp://lifehacker.com/5860851/the-best-disc-burning-app-for-linux"
    author: ""
  - subject: "Well, not everything is"
    date: 2012-03-06
    body: "Well, not everything is perfect. But on the other hand - especially in KDE PIM - have you seen HOW much work has been done lately? Seriously... See:\r\nhttp://cgbdx.wordpress.com/2011/12/07/is-kmail-still-being-developed/\r\nhttp://cgbdx.wordpress.com/2012/03/01/kdepim-bug-fixes-in-kde-4-8-1-8/\r\n\r\nYep, it's not all perfect yet. More work is to be done. But at the same time - nothing is perfect, and it's pretty good :D"
    author: "jospoortvliet"
  - subject: "Plasma One became really fluid"
    date: 2012-03-06
    body: "I am currently watching the Action Show, and it really became fluid.\r\n\r\nIn the last video I saw about it, I was a bit disappointed that I could watch the single frames.\r\n\r\nIn the Action Show, it just flowed. And since it is free software, maybe I can get it onto an XO\u2026"
    author: "ArneBab"
  - subject: "Fedora with KDE rules"
    date: 2012-03-06
    body: "Fedora with KDE rules"
    author: ""
  - subject: "well deserved"
    date: 2012-03-06
    body: "You are making awesome progress, the whole desktop is alive and well with KDE and all those apps you all work so hard on, keep it up! :D"
    author: ""
  - subject: "Congrats to KDE"
    date: 2012-03-06
    body: "I switched permanently to KDE after trying the Mint Kde version.\r\nIt is rock stable yet you can fine tune it and V4.8 rocks.\r\nWhat a change from the 4.6 version....\r\n\r\nGreat great job !"
    author: ""
  - subject: "It's deserved"
    date: 2012-03-06
    body: "Really... Just look at what has been achieved lately.\r\nIt took a while since the 4.0 release ;-) but KDE really shines now.\r\nAll the \"groundwork\" which was laid and was causing so much pain to adapt all the bits and pieces to the new platform... but it's paying off now.\r\nWithout looking \"under the hood\" KDE already looks like a shiny modern desktop with all the bells and whistles you expect nowadays.\r\nBut... just look at Nepomuk and Akonadi... this is where KDE is really taking now. KDE is leaps ahead... the separate strings tie together now.\r\nIs it pathetic? It really was visionary what you have decided some years ago.\r\n"
    author: "thomas"
  - subject: "Re"
    date: 2012-03-06
    body: "Crashes might be because of the distro and how you set it up. For example, when I was using Kubuntu 10.04 or 10.10 it was crashing pretty often but in openSuse with the same KDE version and software it wasn't crashing at all."
    author: ""
  - subject: "Congrats for feature set"
    date: 2012-03-06
    body: "Over the past decade, KDE has been my preferred desktop much of the time.  But generally KDE 4 has not been stable enough to suit my wife, who is a more \"typical\" computer user.  The priorities of the KDE team probably satisfy the KDE team and the Linux community in general, but leave some doubt as to whether KDE will ever provide a desktop solution for the bulk of typical users or just for computer savvy Linux enthusiasts.\r\n\r\nThe enthusiasm of the community is understandable but is based upon new features rather than upon stability.  The above poster's opinion about Mint KDE contrasts with my own disappointment, and judging from other reports, my own experience is shared by some others who seek a system that \"just works\".  Certainly out-of-the-box, Mint KDE requires some effort.\r\n\r\nKDE deserves accolades for features.  They have identified and implemented the best collection of features in existence.  But the team and the enthusiasts who applaud incomplete efforts (with regard to stability) are reenforcing an approach that tends to produce desktop systems that perpetuate the common opinion that Linux is for experts."
    author: ""
  - subject: "Yes, I read that"
    date: 2012-03-06
    body: "I'm interested in what's going on with KDE, so I'm reading stuff like KDE.NEWS or Planet KDE. And yes, I was impressed by the many bug fixes. Same goes for <a href=http://freininghaus.wordpress.com/2012/02/29/dolphin-bug-fixes-in-kde-4-8-1/>Dolphin</a>.\r\n\r\nSo, it's great to hear about this progress. On the other hand it shows that there are still so many bugs. Nothing is perfect, yes. But I experience crashes all the time, every day. Kontact for example does that often. Even when I do not use Akregator to open web pages in tabs, it crashes from time to time. Which s annoying, because it does not save which Akregator articles I have already read. Same goes for KNode. So I close Kontact every day and restart it, in order to make it saves the current state, and do not have to go though dozends ofposts I already read.\r\nAkregator also has a bug, one of my feeds shows articles from other feeds, too. I was filing a bug about this, but when I wanted to commit it, Konqueror crashed, and I was too annoyed to write the whole report again. This happens frequently, I want to reproduce a problem or file a bug, and while doing so, other problems happen. Whenever I try new things, I can be sure to stumble upon some bugs.\r\n\r\nAnd there are very basic things that I would expect to just work. Like shutting down the PC. But most of the time when logging out, some applications (KMyMoney, KWin, plasma-desktop) show a DrKonqui crash window which I have to close first. And whenever I log in (happens about once a week only) I wonder what will happen this time - duplicate activities again, or missing plasmoids which I have to re-create by copying back an old plasma-dektop-appletsrc file? Will Kontact complain it is already running? I am also scared of saving my session - I always back up my .kde4 directory before doing so, as it often messed up my setup in the past. \r\n\r\nThose are no showstoppers, but for inexperienced users I would not recommend KDE, at least not yet. For us geeks, KDE is cool, I would not want to switch to anything else, I got so accustomed to it, and I would miss so many functions. KWin is really impressive, and since I have 16 GB of RAM, it doesn't matter that it uses 1.2 GB right now. Plasma-desktop only uses half of that... oh, and I don't even want to know why the akonadi_imap_resource process needs a whole GB.\r\n\r\nOkay, enough of ranting, congratulations again! Maybe it works better for most other people, and I must admit that I run an <a href=http://www.wonkology.org/comp/desktop/2011-07-24/>awful lot of stuff</a> most of the time, and I do many different things, without logging out for several days. Most of my problems would probably not happen if I only used a single desktop with a web browser and a simple mail setup. So, keep up the good work, maybe fix even more bugs, and keep KDE being the best desktop environment there is!"
    author: "Wonko"
  - subject: "bugs and forum"
    date: 2012-03-07
    body: "Have you shared these at bugs.kde.org or tried to get or give help in forum.kde.org?\r\n\r\nKDE is the Community. Which works by people doing what they can to make it the best ever."
    author: "kallecarl"
  - subject: "Time to start again..."
    date: 2012-03-07
    body: "Your post is a galore of what I also saw between KDE 4.2 and KDE 4.5. Maybe it's time for you to start with a fresh .kde4 folder. KMail now saves mail in ~/.local, not in ~/.kde4."
    author: ""
  - subject: "Best desktop"
    date: 2012-03-07
    body: "When KDE4 was first released I was one of those who abandoned KDE. I was angry that the developers had trashed all the functionality I found so useful in KDE3. Now with a few reservations, namely the Places panel in Dolphin, It is the best desktop, as of 4.7.4, again. All of the functionality has been restored, and some new functionality added.\r\n\r\nTo say that I'm happy to be moving back to KDE is an understatement."
    author: ""
  - subject: "Big hugs to you all working hard for KDE"
    date: 2012-03-07
    body: "Let me tell you guys do an amazing job, and I've never been able to revert back to something else since KDE4. You have done an amazing job rewriting everything, providing great tools and environment. \r\n\r\nOf course KDE4.0/1/2 had few problems here and there, but it was really young.\r\n\r\nYour last updates are simply formidable. There is still few things I would improve of course. But let me tell you this, you totally deserve the title of \"Best Desktop Environment of the Year\". I truly wish I had time to work on this amazing project.\r\n\r\nKeep going! Best wishes."
    author: ""
  - subject: "Love it!"
    date: 2012-03-07
    body: "I have to admit, when KDE 4.5 first hit, it was unstable enough that I switched back to Gnome.  I got constant crashes, it froze up randomly (but quickly thawed out again) every few minutes...I was NOT a happy camper.  I gave it a shot again around 4.7 though, and...WOW!  This title is very well earned, congrats to the developers who made it happen!"
    author: ""
  - subject: "For LifeHacker, being a KDE app is a downside."
    date: 2012-03-07
    body: "\"Where it falls short\"\r\n\r\n\"Okular is a KDE program, which means GNOME users will have to download lots of dependencies to run it, which will take up a good amount of space (not to mention it'll look a bit out of place with their other programs).\"\r\n\r\n\"K3b [...] is a KDE program, however, meaning if you use any other desktop environment it'll require a good amount of KDE dependencies to install, which some users don't like.\"\r\n\r\n\"[Digikam's] other downside is that it's a KDE program, which means GNOME users\u2014who are in the majority\u2014might find that it feels a little bit out of place, not to mention comes with a heaping pile of dependencies to install. Still, it's worth it for the power behind the program.\"\r\n\r\nFor them, being a GNOME app gives points by itself, so, this is a double win!"
    author: ""
  - subject: "How do you tell if someone is"
    date: 2012-03-07
    body: "How do you tell if someone is an Arch Linux user... You don't. They'll tell you!\r\n\r\nI'm an Arch Linux user myself. It is awesome!"
    author: ""
  - subject: "Sometimes"
    date: 2012-03-08
    body: "If I had free time and a better knowledge of Qt & C++ stuff, I might be a KDE programmer. So, I try to help a little by reporting bugs, which I usually do when a bug is reproducible. And because I'm a Gentoo user I can easily offer to recompile stuff with debug information in order to create better crash reports.\r\n\r\nThis can become very time-consuming sometimes, like when I tracked a nasty Dolphin crash problem which in the end was not Dolphin's fault, but a bug in ffmpeg, so I had to also report this upstream and so on. But I like to help, and because I'm running brand-new software I feel somewhat obliged to solve this, before the mass of users will run into this.\r\n\r\nBut most of the daily problems I just note in my personal log. If a problem happens regularly, I will eventually file a bug when I think it's worth it. For example, Kontact not saving which KNode/Akregator articles were read is probably not important to file, because this probably won't matter once those applications got akonadified, too. And sometimes I'm just too busy with other things.\r\n\r\nI also reported some wishes, but they do not seem to get much attention. Which is a little sad, because sometimes it's just very tiny changes which would make me much happier. Like <a href=https://bugs.kde.org/show_bug.cgi?id=256187>bug #256187</a>, the file watcher plasmoid reads the whole file it watches into memory, and this gave me a lot of trouble until I found out this was the cause for plasma using that much memory. Or my wish that the file watcher plasmoid could behave like tail --follow=name, so it could also watch log files that are rotated.\r\n\r\nI didn't use the forum yet, but I think I will have a look, thanks for the reminder. I'm more a mailing list guy, I post there from time to time. People are helpful and nice there, despite my occasional rants :)"
    author: "Wonko"
  - subject: "Akregator and Konqueror"
    date: 2012-03-09
    body: "Akregator and Konqui crashes might be crashing in the web engine or flash.\r\nWith webkit backend and certain versions of flash, I have frequent crashes. With an older flash, I don't have problems."
    author: ""
  - subject: "congratulations"
    date: 2012-03-09
    body: "I'm a KDE user since the 4.0 version, and use Fedora distro !\r\nUntil this I had used Gnome.\r\n\r\nI have accompanied the KDE progress, and it's very good. Each time is more mature, less bugs, and much innovation. And the KDE Team is very, very fast and dynamic.\r\n\r\nToday, I see the bugs site very well, different, it's good. I have a suggestion: it had a fixed frame, and other scroll.\r\nBut I think if items like home, search ... (in second line), is in fixed frame it will be very better :-)\r\n\r\nCongratulations, excellent jobs :-)\r\n"
    author: ""
  - subject: "kde rocks"
    date: 2012-03-11
    body: "I love KDE. I use it on Kubuntu. It's awesome."
    author: ""
  - subject: "Ever got a perfect KDE update?"
    date: 2012-03-11
    body: "Have you ever got a perfect KDE update on Kubuntu?"
    author: ""
  - subject: "perfect KDE update "
    date: 2012-03-11
    body: "Yes. I have run KDE since mid-3.5 days, including 4.0 from the beginning.\r\n\r\nI have had \"perfect updates\" (whatever that means; I take it to mean \"no problems\") whenever the update advisory includes something like: \r\n\"...recommended updates for everyone running 4.8.0 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone.\"\r\n\r\nYour question seems to imply that you have not had the same experience. If that's true, then what happened for you? If you encountered problems, did you use Kubuntu Bugs, bugs.kde.org or the forums for Kubuntu or KDE? "
    author: "kallecarl"
  - subject: "Go KDE!!"
    date: 2012-03-12
    body: "Running this great desktop since 3.x, congratulations to all developers and community, KDE has no rival in awesomeness.\r\n\r\nRegards from Cuba.\r\n"
    author: ""
  - subject: "KDE is a winner"
    date: 2012-05-11
    body: "I completely agree that KDE has clear discernible strengths making it a winner against other Desktop Environments like Windows 7, Unity, Gnome 3, Cinnamon, etc.\r\n\r\nhttp://www.kde.org/download/distributions.php\r\n\r\nI'm surprised to see Mint at the top of the distro list. I haven't had good luck with it.\r\n\r\nI love the Folder Plasmoid Widget and Tabs and Split View in Dolphin.\r\n\r\nElectricPrism"
    author: ""
  - subject: "time - and time for devs"
    date: 2013-10-17
    body: "I know your experience (being a Gentoo user myself), and also the missing time to report every issue.\r\n\r\nIn the end, most of this comes down to missing time, and I think we need more funding for KDE developers who want to do the hard work of realizing all the small wishes people have. That\u2019s not the kind of work into which you can dig for the fun of a challenge, and also not the kind of work which gives you awesome skills which bring you profit on the long term (though it will give you a wide array of knowledge about KDE) or the kind of work for which you will go into the history books, so I think they should be paid for that (the same goes for Gentoo maintainers - for example I donate to Gentoo e.V. in the hope that someday enough people will do so that the *community* can employ a full-time maintainer).\r\n\r\nSo I wish for much more regular donations from the community - maybe with easier (or more advertised) ways to use a subscription model without using paypal\u2026\r\n\r\nIf we had something about 3 developers who work full-time on a long-term contract with the job description \u201cmake the KDE experience seamless for end-users\u201d, that could change a lot. We only need about 6000 people who donate 5\u20ac a month for that.\r\n\r\nIdeally we could estimate how much we would have to donate to provide this not only for KDE but for the whole KDE-X-GNU-Linux stack with the assumption that 10% of the users donate."
    author: "ArneBab"
  - subject: "only a single cost\u2026"
    date: 2013-10-17
    body: "They somehow omit that all those points are only valid for at most one of the entries in the list (the first you install): As soon as you have the dependencies, the other programs need no additional dependencies to work.\r\n\r\nFor all gnome users who use k3b, Okular, Choqok and Digikam have no additional dependencies\u2026"
    author: "ArneBab"
---
The 2011 Members' Choice Awards from <a href="http://www.linuxquestions.org/questions/linux-news-59/2011-linuxquestions-org-members-choice-award-winners-928502/">LinuxQuestions.org</a> has honored the KDE Community with "Best Desktop Environment of the Year". In addition, Tech Radar has declared that KDE Workspaces are <a href="http://www.techradar.com/news/software/operating-systems/what-s-the-best-linux-desktop-environment--1045280">the best desktop environment</a>. KDE innovation, performance and stability have particular appeal for users. Tech Radar reports that KDE Workspaces have "the best mix of cutting edge software and stability...", and writes, "We were amazed by the level of comfort the users experienced with KDE."  Chris from the Linux Action Show is <a href="http://www.youtube.com/watch?feature=player_detailpage&v=-1iYRkYtyxA#t=1095s">switching to KDE!</a>

The Tech Radar article makes a special point about business users. "Business users feel at home in the common desktop metaphor that KDE so beautifully creates. This, coupled with its well-integrated applications, means KDE wows almost every business user I present it to." Recognizing the love that has been shown to Plasma Workspaces, Tech Radar writes, "When all's said and done...every one of our testers was floored by KDE 4.7 across all devices."

There were comments in both articles about various KDE performance factors. Martin Gräßlin's <a href="http://blog.martin-graesslin.com/blog/2012/02/about-compositors-and-game-rendering-performance/">blog post</a> addresses issues such as these.

Two individual KDE applications also came away as winners this year. <a href="http://amarok.kde.org/">Amarok</a> scored the Audio Media Player of the Year award and <a href="http://dolphin.kde.org/">Dolphin</a> was selected as the Best File Manager. Congratulations to these teams and to everyone who contributed to making KDE Workspaces the best desktop of 2011!

<strong>Update:</strong> A comment from filipesaraiva points out that Choqok was recently named "The Best Twitter Client for Linux" by <a href="http://lifehacker.com/5879964/the-best-twitter-client-for-linux">Lifehacker</a>.

<strong>Update 2:</strong> An anonymous commented that Life Hacker has more favorite KDE applications:
<a href="http://lifehacker.com/5877908/the-best-photo-management-app-for-linux">Digikam : Best Photo management app</a>
<a href="http://lifehacker.com/5875879/the-best-pdf-viewer-for-linux">Okular: Best PDF Reader</a>
<a href="http://lifehacker.com/5860851/the-best-disc-burning-app-for-linux">K3B: Best Disk burning app</a>