---
title: "KDE PIM Needs You!"
date:    2012-03-29
authors:
  - "jospoortvliet"
slug:    kde-pim-needs-you
comments:
  - subject: "Thanks for this important update, Jos"
    date: 2012-04-03
    body: "I'm looking forward to trying out KDE PIM in Kubuntu 12.04. \r\n\r\nJust a quick note that \"the bugday being organized for KDE PIM\" link appears to be broken. \r\n\r\n- James"
    author: "dequire"
---
About a month ago, the KDE PIM team had their <a href="http://dot.kde.org/2012/02/17/kde-pim-sprint-10-accomplished">yearly sprint in Osnabrück</a>. At this sprint, there was much new code, squashed bugs and considerable progress. One important thing on the table was the future of KDE PIM. Read on to learn how <strong>YOU</strong> can help fullfill the potential of KDE PIM!
<!--break-->
<h2>Substantial Structural Changes</h2>
As was detailed in the sprint report, the KDE PIM team has spend the last 6 years reworking the infrastructure of KDE PIM. They started with completely siloed applications and a variety of home-grown storage and caching layers. And created a unified layer capable of serving the needs of all current and future KDE PIM applications. It has been a long and bumpy ride. 

This was especially true during the last 4 years as KDE PIM users were served by a 'hybrid' between the new stack and the old one. During that time, the Kontact suite has not been stable. About 8 months ago, the KDE PIM team decided that the pain caused by running the aging Kontact 4.4 suite on a much newer KDE Foundation could no longer be tolerated. They made the difficult decision to move to the fully integrated, new Kontact backend (Akonadi), knowing that there would be pain involved with that too. The Kontact version in KDE Release 4.7 replaced the hybrid model entirely and migrated all existing data over to the new backend.

<div><a href="http://community.kde.org/KDE_PIM/History"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE PIM structure.png" /></a><br /><big><strong>From monolithic siloes to integrated modules</strong></big></div>

Feedback came in, of course, that not everything was stable. The new system worked properly for the KDE PIM developers. But users reported serious issues. The wide gamut of possible usecases and huge amounts of data caused unforeseen problems. So the Team set out to fix them. <a href="http://cgbdx.wordpress.com/2011/12/07/is-kmail-still-being-developed/">690 bugs were marked</a> <strong>RESOLVED</strong> in KMail alone in the 6 months following the 4.7 Release. The KDE 4.8.1 Release introduces another <a href="http://cgbdx.wordpress.com/2012/03/01/kdepim-bug-fixes-in-kde-4-8-1-8/">another 50 fixes across KDE PIM</a>.

<h2>Forward: AWESOME</h2>
There are already significant performance improvements in the speed of checking for new mail. At the last Sprint, it was clear that there is more performance improvement to come. There will be further optimizations to the database. And there are still bugs to be fixed! In addition to that, there is a renewed energy and a team focus on needs requested by users and on doing cool, new things. The new framework offers exciting opportunities, some of which are already being seized. People are working on <a href="http://algorithmsforthekitchen.com/blog/?p=137">moving the news reader to the new backend</a> which will allow easier cloud syncing. New connectors can now be written for things like facebook and other services (making it possible to write <a href="http://viranchmehta.wordpress.com/2012/03/02/facebook-notifier-kde-plasmoid">more powerful widgets and tools</a>). New interfaces for <a href="http://blog.cornelius-schumacher.de/2012/02/releasing-polka-09.html">addressbooks</a> and <a href="http://zanshin.kde.org/">GTD-based task management & notes</a> are being developed. And small improvements like making the quick filter bar search the full contents of mails. 

Within the <a href="http://plasma-active.org/">Plasma Active</a> and <a href="http://kolab.org">Kolab</a> communities, people are looking into bringing KDE PIM technologies onto the server so that one day you'll have the power of your desktop on the go and on all devices. 

Right now, for example, you can use your Google addressbook in Kolab's webmail client. But what if you want to access BOTH your Facebook and Google accounts with Kolab webmail? Currently you can't; Akonadi is limited to the desktop. For these web-based applications, Akonadi capabilities need to run server-side. If you want to know more about this, check out the <a href="http://blogs.fsfe.org/greve/?p=470">Kolab 3.0 Primer</a> or get involved with any of the communities working on Plasma, Nepomuk, Akonadi, or Telepathy.

<h2>The problem</h2>
So with the infrastructure in place, the KDE PIM team is focused on user issues and feature requests. Developers see great promise in using the new infrastructure to satisfy users. But something is blocking the team. Existing bugreports and a flood of new ones coming in from older KDE PIM releases (the old, hybrid version shipped with KDE releases prior to 4.7, and the first release with the new backend). Bugreports that are useful for improving the recent releases are being outmatched. The team does not like to close old bugs without spending some time on a proper answer. But the time it takes just to go through now irrelevant bugreports (many of which contain quite demotivating language) makes it hard to get work productive done. Help is needed.

<h2>How to help</h2>
KDE PIM currently needs two things: 
<ul>
<li>a clean way for the developers to find pertinent bugs to fix, and</li>
<li>Cool Stuff™.</li>
</ul> 

If you're a developer, the way forward is clear: start <a href="WHERE">diving in the documentation</a> and be creative with the new KDE PIM backend!

For end users, you can help too. 

First, you can participate in <a href="In the process of being organized...">the bugday being organized for KDE PIM</a>! It's easy to do, and it makes a massive difference for the KDE PIM team. And it is fun!

Second, you can keep making bugreports. We ask that you do this based on a <strong>very</strong> recent KDE PIM version and be responsive to requests for additional information! Bugreports for versions of KDE PIM before KDE Release 4.8.1 are mostly noise. With well over 700 bugs fixed in the last 6 months, most of the reported issues have already been fixed.

Last—just as important—keep in mind that we want to make KDE PIM work for enthusiasts (probably you, reading this) first. We're adding advanced features and making it more stable. But, until you've used it and sent in bug reports, and we've fixed them, we can't recommend this to grandma yet!