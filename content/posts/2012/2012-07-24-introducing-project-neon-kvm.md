---
title: "Introducing Project Neon KVM"
date:    2012-07-24
authors:
  - "rohangarg"
slug:    introducing-project-neon-kvm
comments:
  - subject: "Keeping up to date..."
    date: 2012-07-25
    body: "Once we download the KVM image, will it be able to stay up to date via apt-get update/dist-upgrade, or do you have to download the KVM image every week or two?"
    author: ""
  - subject: "Keeping up to date... "
    date: 2012-07-25
    body: "Either method is fine, though if bandwidth is constrained, I'd suggest just using <code>sudo apt-get update && sudo apt-get dist-upgrade</code>"
    author: "rohangarg"
  - subject: "Debian"
    date: 2012-07-28
    body: "It would be great if the debian packages would also be brushed up a little. They seem to need more love than the ubuntu ones.\r\n"
    author: ""
  - subject: "Debian"
    date: 2012-07-30
    body: "Unfortunately the Kubuntu team is absolutely stretched to the limit, while we try and contribute back to Debian as much as we can, so while Debian users have to wait a bit longer, I'm sure they can try out our shiny KVM images to checkout what awesome features that they're going to get in the next release ;) "
    author: "rohangarg"
  - subject: "Compositing"
    date: 2012-07-31
    body: "Great initiative! When trunk builds become more common an application will gain stability.\r\n\r\nWould it be possible to switch-off/auto-detect compositing in a more convenient way? With compositing activated my screen gets black.\r\n\r\nI ran into a small issue and noticed the crash dialogue in Neon desktop, but then grabbing for the debug packages failed, virtually trashed the desktop by heavy writing on the hard disc. Why aren't for a test desktop environment as Neon debug symbols installed by default?"
    author: "vinum"
  - subject: "In a virtual machine KWin"
    date: 2012-07-31
    body: "In a virtual machine KWin should fall back to XRender-based compositing. I don't see any reason why this should not work. If you can figure out what is going wrong, please report a bug.\r\n\r\nKind regards\r\nMartin Gr\u00e4\u00dflin"
    author: ""
  - subject: "Merci"
    date: 2012-08-01
    body: "Thanks for the reply, I fixed it for me but I am not sure everyone can. ;-)"
    author: "vinum"
  - subject: "KVM switches"
    date: 2012-08-14
    body: "KVM switches are popular among users who have upgraded their home PC systems and want to still use their old computers but do not want to invest in a second keyboard, monitor and mouse. "
    author: ""
  - subject: "Nice idea"
    date: 2012-08-29
    body: "So more users can participate in testing. If it only worked on Debian 7.\r\n\r\n~/ubuntu-kvm$ ./run.sh \r\n./run.sh: 3: exec: kvm: not found\r\n\r\n# aptitude install qemu-kvm\r\n...\r\nSetting up qemu-kvm (1.1.0+dfsg-3) ...\r\nERROR: could not insert 'kvm_intel': Operation not supported\r\n[FAIL] Module kvm_intel failed to load ... failed!\r\ninvoke-rc.d: initscript qemu-kvm, action \"start\" failed.\r\nSetting up bridge-utils (1.5-4) ...\r\nSetting up sharutils (1:4.11.1-1) ...\r\n\r\n~/ubuntu-kvm$ ./run.sh \r\nCould not access KVM kernel module: No such file or directory\r\nfailed to initialize KVM: No such file or directory\r\nNo accelerator found!\r\n\r\n"
    author: "Boris Hollas"
  - subject: "How odd ..."
    date: 2012-09-16
    body: "Looks like its failing to insert the kvm kernel module, I am afraid you will have to figure this one out using Google."
    author: "Rohan Garg"
---
<h2>Background</h2>
<a href="https://wiki.kubuntu.org/Kubuntu/ProjectNeon">Project Neon</a> provides daily builds of KDE modules for <a href="http://www.kubuntu.org/">Kubuntu</a>. It is an easy way to get the latest code without having to build the entire KDE-Git/SVN tree and maintain the checkout. Project Neon is unstable, but it installs alongside stable packages. It is suitable for contributors such as new developers, translators, usability designers, documenters, promoters, and bug triagers. With Project Neon, people can experiment freely without risk to a working KDE environment.

Project Neon is especially useful for reporting bugs. With its daily builds, bugs can be reported in the most timely manner. The more time that elapses between when a bug is introduced and when it is reported, the more difficult it gets to find it and fix it. With Project Neon, a bug can be reported on the same day that it is introduced.

<h2>Introducing Project Neon KVM</h2>
The Project Neon team has developed another product—<strong>Project Neon KVM Image</strong>. Users can download an image and deploy it anywhere, using <a href="http://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine">KVM virtualization technology</a>. Now Project Neon is available more widely.

Project Neon KVM is for people who use a Linux distribution other than Kubuntu or a non-Linux operating system. Without disturbing the host environment, Project Neon KVM can be used to test new KDE releases, to try out new features, or to develop something entirely new.

The image contains a bare install of Project Neon daily build packages. Project Neon KVM images are built weekly and can be <a href="http://kubuntu.s3.amazonaws.com/list.html">downloaded</a> directly or as a torrent. At least 2GB's of RAM are suggested to run these images. Alternatively, ubuntu-vm/run.sh in the tarball can be edited to allocate less memory to the KVM. The default username/password for the VM is 'neon' (without the quotes).

The Project Neon team is on Freenode IRC at #project-neon. Mailing list information and archive are available at https://launchpad.net/~neon.

A special thank you to Jonathan Riddell for hosting the images.