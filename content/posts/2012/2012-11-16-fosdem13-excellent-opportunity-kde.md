---
title: "FOSDEM'13 Excellent Opportunity for KDE"
date:    2012-11-16
authors:
  - "kallecarl"
slug:    fosdem13-excellent-opportunity-kde
---
<a href="http://fosdem.org/2013/">FOSDEM</a> is one of the largest gatherings of Free Software contributors in the world and happens each February in Brussels (this year on the 2nd & 3rd of February). It’s one of the few community-centered conferences in Europe, and the largest volunteer-run Free Software event in Europe as well. Proposals are now invited for talks on KDE, KDE software and general desktop topics. KDE will be in the Cross Desktop Developer Room (devroom), along with Enlightenment, Gnome, Razor, Unity and XFCE. This is a unique opportunity to share KDE with a wide audience of developers.

Everyone from the KDE Community is encouraged to participate. We want KDE contributors to talk about what they are working on, no matter what area of work is involved. Please don’t think that your potential contribution is too insignificant.  All parts of KDE should be represented in the devroom. Check out the <a href="http://community.kde.org/Promo/Events/FOSDEM">Community Wiki</a> to see the recent history of KDE participation at FOSDEM, and to get some ideas for what to present. If you are interested in your project, others will be too. Please <a href="https://lists.fosdem.org/pipermail/fosdem/2012-October/001643.html">propose a talk</a>. The deadline for proposals is December 14th.

People are also needed to staff the KDE stall, one of the busiest at FOSDEM. This is an opportunity to meet people, talk about KDE and our future plans, demo KDE software, sell merchandise and hand out literature.

In addition to the Cross Desktop devroom, the program is packed with high profile speakers, other stalls, and events for all major free software. There are also other devrooms providing opportunities for learning and collaboration. The entire event is a hacker-dream-come-true. 

KDE is one of the largest and most influential Free Software communities in the world. We have a lot to offer at FOSDEM. Sign up to participate at the <a href="http://community.kde.org/Promo/Events/FOSDEM/2013">KDE at FOSDEM 2013 wikipage</a>.