---
title: "KDE Commit-Digest for 25th November 2012"
date:    2012-12-02
authors:
  - "mrybczyn"
slug:    kde-commit-digest-25th-november-2012
---
<a href="http://commit-digest.org/issues/2012-11-25/">This week KDE Commit-Digest</a> introduces QML containments. Additionally, the list of changes includes :

<ul><li><a href="http://amarok.kde.org/">Amarok</a> can now transcode even with ffmpeg >= 1.0; statistics synchronization work is merged</li>
<li><a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a> adds numerous improvements: new provider states, prevents multiple feed imports, provider model/dialog gets dynamic</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> memory footprint reduced</li>
<li>KDE-libs introduces an optimization by caching locally favicon names</li>
<li><a href="http://krita.org/">Krita</a> improves zoom factor handling</li>
<li><a href="http://community.kde.org/KDE_PIM">KDE PIM</a> fixes implementation of the PurgeBuffer.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-11-25/">Read the rest of the Digest here</a>.