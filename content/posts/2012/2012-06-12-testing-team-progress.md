---
title: "Testing Team Progress"
date:    2012-06-12
authors:
  - "amahfouf"
slug:    testing-team-progress
---
The KDE Testing Team will continue its efforts with 4.9 Beta 2. The <a href="http://dot.kde.org/2012/06/04/kde-announces-49-beta1-and-testing-initiative">testing intensive</a> started with KDE 4.9 Beta 1 last week; 83 bug reports were opened or confirmed, 29 of which are already closed, the bugs having been fixed. Some bugs were even corrected without anyone opening a bug report. Newcomers joined the team and are doing a great job. A big thank you!

We would be happy to include more people. You are more than welcome to join. Just install KDE 4.9 Beta 2 from your distribution and choose an area of testing. Contact the Team to coordinate testing and bug reporting. The <a href="http://dot.kde.org/2012/06/04/kde-announces-49-beta1-and-testing-initiative">Beta 1 Dot story</a> has more information and instructions.

Members of the Testing Team are available on IRC at Freenode.net in the #kde-testing channel and via the
mailing list (https://mail.kde.org/mailman/listinfo/kde-testing).