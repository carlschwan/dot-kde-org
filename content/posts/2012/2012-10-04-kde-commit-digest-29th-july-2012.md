---
title: "KDE Commit-Digest for 29th July 2012"
date:    2012-10-04
authors:
  - "tdfischer"
slug:    kde-commit-digest-29th-july-2012
---
In <a href="http://commit-digest.org/issues/2012-07-29/">this week's KDE Commit-Digest</a>:

<ul><li><ul>
<li>Improvements to the <a href="http://edu.kde.org/kstars/">KStars</a> QML UI.</li>
<li>KDE Frameworks ported to the new Qt5 event filter API.</li>
<li><a href="http://www.behindkde.org/vishesh-handa">Vishesh</a>'s work improving Nepomuk stability and design marches onward!</li>
<li>A handful of Amarok bugs were closed that were related to Phonon about-to-finish behaviors.</li>
<li>Multi-monitor support in lightdm <a href="http://agateau.com/2012/04/21/lightdm-kde-0-1-0-released/userbar.png">Userbar theme</a>.</li>
<li>The new "<a href="http://community.kde.org/Plasma/Active/Development/ActiveHIG/Drawers">drawer</a>" QML component added to plasma-mobile.</li>
<li>Kamera kioslave is able to use libgphoto2 2.5</li>
</ul>

<a href="http://commit-digest.org/issues/2012-07-29/">Read the rest of the Digest here</a>.