---
title: "Kate/KDevelop October Sprint: What's new in KDevelop"
date:    2012-12-17
authors:
  - "Milian Wolff"
slug:    katekdevelop-october-sprint-whats-new-kdevelop
comments:
  - subject: "Great work!"
    date: 2012-12-18
    body: "I've been using Kate for since the beginning of its existence for all kinds of work and I still love it :)\r\n\r\nWhile I try to use KDevelop for all my KDE work since about the beginning of the year, I still find myself launching Kate first and then remembering that I have a KDevelop session for the task as well.\r\n\r\nI think what I am missing is a Plasma applet like the session launcher for Kate."
    author: "Kevin Krammer"
  - subject: "This all is great ... "
    date: 2012-12-18
    body: "and I am looking forward to get my hands on it. What color theme has been used for the dark screenshots?"
    author: "Wolfgang"
  - subject: "Plasmoid"
    date: 2012-12-19
    body: "There's already such a plasmoid, it's called \"KDevelop Sessions\".\r\nIsn't that what you mean?"
    author: "apol"
  - subject: "May be a default color scheme"
    date: 2012-12-19
    body: "I don't remember what it's called, but I think that it's one of the default color schemes included."
    author: "Zash"
  - subject: "Thank you."
    date: 2012-12-19
    body: "Thank you. Then it might be Obsidian Coast (just a guess)."
    author: "Wolfgang"
  - subject: "Obsidian Coast"
    date: 2012-12-19
    body: "The color theme is called Obsidian Coast.\r\n\r\nCheers"
    author: "Milian Wolff"
  - subject: "Very Excited!"
    date: 2013-01-20
    body: "Must say, after working with kCachegrind a bit I was kind of looking forward to some internal valgrind/callgrind support. (Unless I'm missing that there is existing integrated support for these programs, which is very possible.)\r\nBut these additions look fascinating, and unit testing is a much-needed addition! I love using kDevelop, please keep up the good work, it's a great development tool!"
    author: "Trent Reed"
  - subject: "AWESOME!"
    date: 2013-04-27
    body: "Waiting to try it out soon..."
    author: "Madura"
---
This is the second part of the report on the joint Kate/KDevelop development sprint that took place in Vienna from the 23rd to 29th of October this year. It provides an overview of the changes in KDevelop. For more background and details about what happened with Kate during the sprint, make sure to read <a href="http://dot.kde.org/2012/11/24/katekdevelop-october-sprint-whats-new-kate">the first part of this report</a>.

Many thanks to the KDE e.V., its <a href="http://ev.kde.org/getinvolved/supporting-members.php">sponsors and donors</a> and the <a href="http://jointhegame.kde.org/">Join The Game contributors</a> for funding the development sprint. Special thanks go to <a href="http://www.jowenn.net/">Joseph Wenninger</a> for organizing the sprint, and also for funding parts of it. And finally thanks to <a href="http://www.vivid-planet.com/">Vivid Planet</a> for inviting all attending developers to a great dinner.

<h2>What's new in KDevelop</h2>
The KDevelop hackers were very busy and productive during the sprint. About 550 commits were made in just one week. They address performance issues, fix bugs, polish the user interface or integrate new features. In the following, noteworthy changes are described.
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/testing.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/testing_wee.jpg" width="300" /></a><br />Unit Test Integration</div>
<h3>Unit Test</h3>
It was disappointing that Miha Čančula could not attend the development sprint. However, he worked quite hard in the months before. One thing he accomplished was a framework for Unit Test integration for KDevelop. This allows you to quickly run and debug the unit tests of your projects directly from KDevelop. Make sure to read <a href="http://noughmad.com/2012/04/24/becoming-a-kdeveloper/">his report</a> on the new testing framework.

During the sprint we sat together and did a final review of the work and merged the unit test support into our development branches. Afterwards, Niko and Aleix got busy polishing the newly merged code and greatly improved and stabilized it. As with the rest of the KDevelop codebase, this new framework is easily extensible through plugins. Currently we have support for CTest/QTestLib and PHPUnit, while support for Ruby's Test::Unit is in the works.
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/filetemplates.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/filetemplates_wee.png" width="300" /></a><br />File Templates</div>
<h3>File/Project Templates</h3>
Unit test support was something that Miha worked on in his spare time. This year he was also <a href="http://www.google-melange.com/gsoc/proposal/review/google/gsoc2012/noughmad/37002">a Google Summer of Code (GSoC) student</a> for KDevelop: He worked hard on improved project templates support and—more importantly—wrote a new Grantlee-based "file template" feature. This allows for the creation of snippets for new files which replaces and extends the existing "Create new Class" wizard that was in KDevelop until now. Make sure to read <a href="http://noughmad.com/2012/08/19/gsoc-templates-in-kdevelop-final-report/">his final GSoC report</a>.

As usual, new classes can be created, but there are now special templates for extended semantics and frameworks. For example, this allows for the creation of C++ classes following the PIMPL idiom or which implement the Qt shared value semantics. And there are also file templates for GObject classes, PHP classes, CMake modules, etc.

All this work was finally merged into master during the sprint in Vienna. Aleix and Milian polished the implementation, while Alexander and Miquel created proper Ruby file templates. The good thing is that KDevelop users can <a href="http://techbase.kde.org/Projects/KDevelop4/File_template_specification">create their own templates</a> and share them with colleagues or other KDevelop users. Good general purpose templates can be sent to us, and it will be included with other examples.
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/tupletypes.png" width="376" /><br />Python Language Support</div>
<h3>Python Support</h3>
Sven worked on more than his pet project—Python language support, but he sure managed to improve it a lot. He concentrated on code optimizations, clean ups and most importantly bugfixes. This paved the way for <a href="http://scummos.blogspot.de/2012/10/vienna-kdevkate-sprint-first-kdev.html">the first beta release</a> of kdev-python 1.4. Shortly after the sprint, he <a href="http://scummos.blogspot.de/2012/11/kdev-python-14-stable-released.html">successfully released kdev-python 1.4</a> which works together with KDevelop 4.4 and supports python 2.7. For KDevelop 4.5, he is busily hacking away on kdev-python 1.5 which will finally support Python 3.
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/example.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/example_wee.png" width="300" /></a><br />Ruby Language Support</div>
<h3>Ruby Support</h3>
On the Ruby front, we saw lots of improvement thanks to the hard work of Miquel and Alexander. Building <a href="http://www.mssola.com/2012/08/gsoc-improving-kdevelop-ruby-support.html">on Miquel's work</a> during GSoC this year, they improved the Ruby plugin until it successfully parsed <a href="http://adymo.blogspot.de/2012/10/katekdevelop-sprint-new-ruby-language.html">Alexander's large Ruby on Rails source project</a>. While this work required many changes to the language plugin, the changes also worked on proper Ruby support for the new <a href="http://adymo.blogspot.de/2012/10/katekdevelop-sprint-more-progress-with.html">File Templates and Unit Testing features for KDevelop</a>.

<h3>QML/JS Support</h3>
Aleix started a proof of concept language support plugin for <a href="http://milianw.de/blog/qmljavascript-language-plugin-for-kdevelop">both JavaScript and QML for KDevelop</a>. He based it on the excellent related work done by the QtCreator team. All that is required now, and which was somewhat started already by Milian, is to integrate the parser into a proper KDevelop language plugin. 
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/kdev-js-errors1.png" width="340" /><br />Basic JavaScript Language Support</div>
If you are interested in helping out, contact us on <a href="http://kdevelop.org/mailinglists">our development mailing list</a>. While writing language plugins is not easy, it is a rewarding task that will teach you a lot about programming in general and C++, JavaScript and QML in particular.

<h3>C++ Support</h3>

Besides working on shiny new language support plugins, our existing C++ language support was improved as well during the sprint. Sven fixed a few annoying bugs in C++ code completion, while Olivier worked on a new "lookahead matching" code completion feature. He got this into master shortly after the sprint and awaits feedback from our daring users: Is it useful? Is it fast enough? Can it be better? How to reduce noise?
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/lookahead.png" width="340" /><br />C++ Look-ahead code completion</div>
Olivier also improved the template support in our C++ language support. He introduced a new template resolver which chooses the correct specializations in most places. He also managed to remove many useless instantiations (up to 75% in some cases) so there are smaller DUChain caches and faster performance in general.

<h3>Web Development</h3>
While it is sad, it is still a reality: Quanta is dead since we do not have enough people. The bright side is that KDevelop is a very good—in many ways even superior—alternative to the Quanta you may know and love from KDE 3 times. Especially the excellent language support for PHP, Ruby and Python makes KDevelop users much more productive when writing code for the web. Still, we understand that people miss neat Quanta features.

There was a so-called Quanta 4 port which was basically just an IDE on top of KDevplatform (just like KDevelop) with some additional web development plugins. But there was never a proper release of these quite functional plugins. Thus Niko finally took some time during the sprint to create separate repositories for the plugins that used to live <a href="http://nikosams.blogspot.de/2012/11/quanta-update.html">inside the Quanta repository</a>. Afterwards he made sure that the plugins work properly with a recent KDevelop version and created <a href="http://nikosams.blogspot.de/2012/10/execute-sql-kdevelop-plugin.html">a first release for the SQL plugin</a>. He then created <a href="http://nikosams.blogspot.de/2012/10/kdevelop-xdebug-php-debugger-beta.html">the XDebug PHP Debugger</a> and the <a href="http://nikosams.blogspot.de/2012/11/kdevelop-upload-plugin-like-quanta-had.html">Upload plugin</a>. There is also a quite nice CSS language support plugin. The other plugins are not in good shape and will need some work. These include XML/HTML language support, Crossfire debugger integration and a PHP source formatter plugin.

Users can expect to see new stable releases of many of those plugins in the future, together with the KDevelop 4.5 release.

<h3>Polishing</h3>
<div style="float: center; padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/configdialog.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/configdialog_wee.png" width="700" /></a><br />Beautified Configuration Dialog</div>
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/launchconfigdialog.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/launchconfigdialog_wee.png" width="300" /></a><br />Polished Launch Configuration Dialog</div>
New features were not the only things that happened at the sprint. Face-to-face discussions and paired programming enabled us to improve and polish many parts of the user interface. This is always a huge advantage of such hack sprints and shows how important it is to meet regularly in person.
So what has changed? Sven and Aleix spent some time on beautifying <a href="http://scummos.blogspot.de/2012/10/vienna-kdevkate-sprint-kdevelop.html">our settings dialog</a> which meant adding nice icons here and there as well as restructuring the form layouts a bit.

They also worked on improving the <a href="http://scummos.blogspot.de/2012/10/vienna-kdevkate-sprint-first-kdev.html">launch configuration dialog</a>. They tried to simplify it as much as possible to make it easier for newcomers to create custom launch configurations. The simpler UI is more visually appealing thanks to the reduced cruft and clutter.
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/openwith.png" width="300" /><br />Improved Open With</div>
Besides visual polish, the KDevelop source base saw lots of other smaller improvements over the week of the sprint. Milian further optimized the Quick Open feature for large projects with thousands of files. He made sure that the results of the "Show Uses" action are always shown in a toolview and never in a tooltip which could accidentally be closed. The "Open with" plugin also <a href="http://milianw.de/blog/random-new-stuff-from-the-kdevelop-sprint">saw many improvements</a>.

Aleix made further improvements to the CMake support in KDevelop. He also polished the Welcome Page which <a href="http://www.proli.net/2012/04/27/youre-welcome-to-kdevelop/">was added in KDevelop 4.4</a>, improved our "Pick Session" dialog and created a new Plasmoid launcher. Together with Milian, he also created a new assistant which automatically renames a file if it contains a class which was renamed. For example, there is a class Foo in Foo.h and Foo.cpp, and the class Foo is renamed to class Bar. Now KDevelop offers you the ability to automatically rename the files Foo.h and Foo.cpp to Bar.h and Bar.cpp respectively. 
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/rename-file.png" width="300" /><br />Rename File Assistant</div>
Niko polished and <a href="http://nikosams.blogspot.de/2012/10/kdevelop-project-and-filesystem-view.html">fixed bugs in the generic manager and the file system view</a>. He added a drag'n'drop context menu to the project menu, similar to how this works in Dolphin and KMail. The file system now supports bookmarks thanks to Niko. He also improved Git integration and fixed some bugs in the GDB debugger support.

Finally (probably important for packagers), Olivier changed the location of our DUChain cache, from <em>`~/.kdevduchain`</em> to <em>`$XDG_CACHE_HOME/kdevduchain`</em>, which by default is <em>`~/.cache/kdevduchain`</em>, but which is often remapped to some other location if the home directory lives on a network share. A custom location can be set using the new `$KDEV_DUCHAIN_DIR` environment variable. (Note: The old cache directory is not removed automatically. When running KDevelop master, or KDevelop 4.5 in the future, make sure to remove <em>`~/.kdevduchain`</em>).

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/copypaste-drop.png" width="300" /><br />Drag'n'Drop Context Menu</div>

<h2>Conclusion</h2>
This year's Kate/KDevelop sprint was a huge success. Many important new changes were completed and lots of bugs been fixed. In short: The KDevelop 4.5 release will be an exciting milestone.

In addition, from a social perspective, the sprint was an excellent experience. It was especially valuable that some new people attended and were warmly welcomed. We hope to see more new people in the future. And just like in previous years, a joint sprint between KDevelop and Kate developers was fun and productive. If you are interested in joining us in 2013, consider submitting your first patches to either <a href=" http://kdevelop.org/">KDevelop</a> or <a href="http://kate-editor.org/">Kate</a>. 

Many again thanks to all supporters and donors. Thanks to Joseph for organizing the sprint. And a big applause for all the attendees and their excellent work on improving both Kate and KDevelop.