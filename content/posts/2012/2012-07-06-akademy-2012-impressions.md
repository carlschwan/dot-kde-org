---
title: "Akademy 2012 Impressions"
date:    2012-07-06
authors:
  - "sealne"
slug:    akademy-2012-impressions
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/AkademyLogo225px.png" /></div>

Almost all communication between KDE community members happens online, and includes people from all around the world. At Akademy, KDE people meet each other and work together in person. Virtual communication is necessary and valuable for day-to-day work; working face-to-face is much more effective. And Akademy provides much more than that.
<!--break-->
<h2>Personal contacts</h2>
Strong, lasting friendships developed here make it easier to work together online in the future. "I loved how relaxed everything was. Everybody hugs everyone. It's one big family." One person's strongest impression came from the Friday pre-registration event "seeing people again. And watching new friendships being made." "Until now I only knew people by their IRC nicknames. Now I've met them and also became known to them for what I'm contributing."

<h2>Effective working environment</h2>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;text-align: center"><a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-informal.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-informal-wee.jpg" /></a><br />Informal chats</div>
The Akademy environment supports getting things done. It's possible to condense weeks or even months of work into a few days. "I joined the Frameworks team. And submitted my first patch the next day." "I got some help fixing my computer, which hasn't worked for months." "We did some major work discussing and defining the fundamental issues of the KDE Community. We simply could not have done this without being together in person."

With so much project activity happening within the same time frame, it's easy to catch up with what's going on for other people and projects. And there's interaction between the various projects and teams to find common ground and leverage points. There was a wide range of <a href="http://community.kde.org/Akademy/2012#Workshops_and_BoFs">Birds of a Feather (BoF)</a> sessions. Some BoFs introduced new opportunities; others were intensive working sessions.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;text-align: center"><a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-robotics.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-robotics-wee.jpg" /></a><br />IT College students <br />demonstrated their robotics projects</div>
Several Akademy attendees mentioned how smoothly things went. "The organization was excellent." The network, the rooms, the venue itself and its setting, the Akademy Team, all contributed to making it easy for attendees to concentrate on the work at hand. "There was more of a community feeling than we've had at Akademy for years. I felt like we really got back to our roots."

Akademy is fun; it's a pleasure working hard with other committed people. People also enjoyed meeting over lunch or dinner away from Akademy to discuss and plan, or just to get better acquainted. Some people took a break from computers and made Geary plush toys based on the Akademy 2012 mascot.

<div style="padding: 1ex; margin: 1ex; border: 1px solid grey; text-align: center; width: 515px"><a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-makingplushy.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-makingplushy-wee3.jpg" /></a> &nbsp; &nbsp; <img src="/sites/dot.kde.org/files/geary-wee2.jpg" /><br />Making plushy Gearys during a break</div>

<h2>Long term commitment</h2>
Akademy gave several people the impression of strong ongoing commitments. "I appreciated the sense of spirit around Qt and KDE. Despite the turmoil, there is a strong underlying feeling of a bright future for KDE and Qt." "Slackware is the mother of all distros, the oldest distro still around. The core contributors love KDE. And are committed to having KDE be a critical part of Slackware."

<h2>Inspiration</h2>
Being around other committed KDE people helps recharge energy and rediscover inspiration. Martin Gräßlin singled out the Akademy Awards in this regard. As a winner last year, he was on the jury this year, and was happy with the decisions about <a href="http://dot.kde.org/2012/07/03/akademy-2012-second-helpings">the winners</a>. And he was even happier to see so many attendee signatures on the Awards themselves, showing their agreement with the choices. "These deeper connections mean a lot after Akademy when we're on IRC or email."

<h2>Akademy 2012 Tallin, Estonia</h2>
For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

<em>Photos by Kenny Duffus except robotics by Jonathan Riddell (CC BY-SA)</em>