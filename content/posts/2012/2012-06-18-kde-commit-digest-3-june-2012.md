---
title: "KDE Commit-Digest for 3 June 2012"
date:    2012-06-18
authors:
  - "mrybczyn"
slug:    kde-commit-digest-3-june-2012
comments:
  - subject: "575 bugs killed"
    date: 2012-06-25
    body: "Wow! Myriam is a super bug-triager. Closing 575 bugs is awesome!\r\n\r\nBoudewijn"
    author: ""
  - subject: "Yes, congratulations!"
    date: 2012-06-26
    body: "Indeed! And I'm glad to see Martin Gr\u00e4\u00dflin proceeding on his \"bugless kwin\" plan. And that \"Bugs Opened: 372, Bugs Closed\t1133\" really makes me smile. :)\r\n\r\nYou know, I'm the kind of user that is more excited by a squashed bug, than by a newly implemented feature... oh, well, to each his own, isn't it? \r\n\r\nMay I advance a request to the news.kde.org team? I would really like to see some more articles dedicated to bug-triaging, QA activity, and, why not, about people involved... what do you think?"
    author: "Vajsvarana"
---
In <a href="http://commit-digest.org/issues/2012-06-03/">this week's KDE Commit-Digest</a>:

<ul><li>Android native look&feel plugin added to android-qt</li>
<li>New assistant for new classes from templates in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Fancy bookmarking in <a href="http://userbase.kde.org/Rekonq">rekonq</a></li>
<li>Experimental rgbaf32 colorspace in the lcms color engine, all missing chart marker types implemented in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Rewrite the Topic tab in the Channel Options dialog in <a href="http://konversation.kde.org/">Konversation</a></li>
<li>GSoC work in <a href="http://edu.kde.org/kstars/">KStars</a> and Calligra</li>
<li>Season of KDE work in <a href="http://edu.kde.org/marble/">Marble</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-06-03/">Read the rest of the Digest here</a>.