---
title: "KDE Commit-Digest for 25th March 2012"
date:    2012-04-01
authors:
  - "vladislavb"
slug:    kde-commit-digest-25th-march-2012
comments:
  - subject: "very good"
    date: 2012-04-03
    body: "very nice improvments,GO KDE!"
    author: ""
---
In <a href="http://commit-digest.org/issues/2012-03-25/">this week's KDE Commit-Digest</a>:

              <ul><li>Work in <a href="http://uml.sourceforge.net/">Umbrello</a> towards supporting the automatic layout of diagrams and exporting to the <a href="http://www.graphviz.org/">graphviz</a> dot format, along with optimization, bugfixes, and further migration of qt3 classes to qt4</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> sees further work on the new debugger, which now displays variable values in tooltips and has a proper queue and optimization</li>
<li><a href="http://edu.kde.org/kig/">Kig</a> refactors and improves the PGF/TikZ exporter</li>
<li><a href="http://www.digikam.org/">Digikam</a> includes further work to support XMP sidecar files and fixes to compile on ARM processors</li>
<!--break-->
<li><a href="http://okular.kde.org/">Okular's</a> PDF Generator can now handle embedded movies</li>
<li><a href="http://dolphin.kde.org/">Dolphin</a> (and Kdebase) now includes support for additional mouse buttons (forward, backward) for history navigation</li>
<li>Fancytasks 1.1.0 "Back in game" adds many improvements including toggle fullscreen, minimize, maximize and other actions, support for showing tasks from current activity only, own pin task action, per launcher task connecting rules and many bugfixes</li>
<li>QZeitgeist addd support for retrieving thumbnails for URLs</li>
<li>Kwin adds proper key navigation to layout-based window switchers and new "Present Windows" like Window Switcher layout, along with bugfixes</li>
<li>KWebkitPart adds HTML 5 local storage functionality, fixing broken behavior when using Owncloud services</li>
<li>KDE PIM improves the settings import, along with optimization and many bugfixes</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> improves the visual manipulation of tables, docx comments support, and <a href="http://krita.org/">Krita</a> adds color mixing to the deform brush, along with many bugfixes</li>
<li><a href="http://community.kde.org/Plasma">Plasma</a> Media Center implements keyboard naviation along with many other essential components for a media player</li>
<li><a href="http://www.kdenlive.org/">Kdenlive</a> now supports dragging from the effect stack to another clip and a new reset effect</li>
<li><a href="http://amarok.kde.org/">Amarok</a> introduces an improved Free Music Charts script</li>
<li><a href="http://userbase.kde.org/KMLDonkey">KMLDonkey</a> introduces a new QML-based version for the N9 (and Meego) and improves performance for slower internet connections</li>
<li>Kde Telepathy introduces a new calling UI, support for video calls, improved support for Adium themes, display of recent group chats, and other improvements, optimizations and fixes</li>
<li><a href="http://oxygen-icons.org/">Oxygen</a> monospace font improvements</li>
<li>Muon supports multithread object creation, improved UI for software sources, and bugfixes and optimizations</li>
<li>Kajongg introduces its own game server at kajongg.org.</li>
<li>KSudoko adds settings to control the look and feel of 3-D puzzles and three new Roxdoku puzzles</li>
<li><a href="http://games.kde.org/">Kdegames</a> now includes support for players chatting with each other</li>
<li>Bugfixes in <a href="http://konsole.kde.org/">Konsole</a>, <a href="http://kate-editor.org/">Kate</a>, Help Center, KMix, Folderview, Oxygen-GTK, <a href="http://utils.kde.org/projects/kgpg/">KGPG</a> and <a href="http://noughmad.eu/knights">Knights</a></li>
</ul>

Work is being done on statistics showing "0".

                <a href="http://commit-digest.org/issues/2012-03-25/">Read the rest of the Digest here</a>.