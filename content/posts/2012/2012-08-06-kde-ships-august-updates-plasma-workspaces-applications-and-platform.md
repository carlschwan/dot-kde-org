---
title: "KDE Ships August Updates to Plasma Workspaces, Applications and Platform"
date:    2012-08-06
authors:
  - "sebas"
slug:    kde-ships-august-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "changelog"
    date: 2012-08-06
    body: "please change the changelog link from\r\nhttp://www.kde.org/announcements/changelogs/changelog4_8_3to4_8_4.php\r\nto\r\nhttp://www.kde.org/announcements/changelogs/changelog4_8_4to4_8_5.php"
    author: ""
  - subject: "fixed the changelog..."
    date: 2012-08-06
    body: "thanks for the notes!"
    author: "sebas"
---
<p align="justify">Today KDE <a href="http://www.kde.org/announcements/announce-4.8.5.php">released updates</a> for its Workspaces, Applications, and Development Platform.
These updates are the fifth in a series of monthly stabilization updates to the 4.8 series. 4.8.5 updates bring many bugfixes and translation updates on top of the latest edition in the 4.8 series and are recommended updates for everyone running 4.8.4 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
<br /><br />
Significant bugfixes include improvements to the Kontact Suite, bugfixes in Dolphin and many more corrections and performance improvements all over the place.
The <a href="http://www.kde.org/announcements/changelogs/changelog4_8_4to4_8_5.php">changelog</a> and <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.8.5&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a> list more, but not all improvements since 4.8.4.
Note that these changelogs are incomplete. For a complete list of changes that went into 4.8.5, you can browse the Subversion and Git logs. 4.8.5 also ships a more complete set of translations for many of the 55+ supported languages.
To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.8.5.php">4.8.5 Info Page</a>. If you would like to find out more about the KDE Workspaces and Applications 4.8, please refer to the <a href="http://www.kde.org/announcements/4.8/">4.8 release notes</a> and its earlier versions.
</p>
<!--break-->
