---
title: "Amarok 2.6 \"In Dulci Jubilo\" Released"
date:    2012-08-15
authors:
  - "Mamarok"
slug:    amarok-26-dulci-jubilo-released
comments:
  - subject: "Next one should be Ommadawn"
    date: 2012-08-15
    body: "Ommadawn ;)"
    author: ""
  - subject: "Which theme is that?"
    date: 2012-08-22
    body: "Which theme is that?"
    author: "Amit"
  - subject: "maybe obsidian cost?"
    date: 2012-08-23
    body: "maybe obsidian cost?"
    author: "TIll Eulenspiegel"
---
Let's rejoice and celebrate: <a href="http://amarok.kde.org/en/releases/2.6">Amarok 2.6 is here</a>! The Amarok Team is proud to present you the result of their work over the last 7 months. With a reasonable set of new features, we focused this release on bug fixing and improving the overall stability, making this the most stable Amarok release ever.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Amarok-2.6-InDulciJubilo-screenie4.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/Amarok-2.6-InDulciJubilo-screenie4525.png" /></a><br />Amarok 2.6 "In Dulci Jubilo"</div>

<h2>New features</h2>

This version comes with a complete overhaul of iPod, iPad and iPhone support including Solid support for device playlists (however, the underlying library, <a href="http://www.gtkpod.org/wiki/Libgpod">libgpod</a>, still <a href="http://www.gtkpod.org/wiki/Template:SupportedHardware">doesn't support iOS 5 devices completely</a>). Transcoding for iPod-like and USB Mass Storage devices is now provided that complements the transcoding features for the Local Collection. The Free Music Chart service is now activated by default and quite a lot of work was put into the album art support: Amarok now has embedded cover support for Ogg and FLAC files and album art is supported for tracks on the filesystem and USB Mass Storage devices.

Amarok now also includes a new Diagnostic Dialog in the Help menu, reporting versions of Amarok, Qt, KDE and Phonon libraries as well as the Phonon backend in use and the status of plugins and scripts. The Amarok developers have also refined their unit tests, and we use a continuous integration server which was of great help during the 12-week testing period.

<h2>Translations and bug fixes</h2>

Amarok 2.6.0 comes translated into 35 languages and more are being worked on for future releases. More than 110 reported bugs were fixed (of which 40 alone were reported and fixed in the testing period) and more than 20 wishes were implemented in this version. A total of 640 bug reports were closed; most were duplicates.

<h2>Did you know?</h2>

Amarok releases are usually named for Mike Oldfield (<a href="http://en.wikipedia.org/wiki/Amarok_(album)">of "Amarok" fame</a>) track names. We chose "In Dulci Jubilo" this time to celebrate Oldfield playing this in the XXX Olympiad opening ceremony.

<h2>Getting Amarok</h2>

You can download the tarballs for <a href="http://download.kde.org/download.php?url=stable/amarok/2.6.0/src/amarok-2.6.0.tar.bz2.mirrorlist">Linux</a> and <a href="http://amarok.kde.org/wiki/Download:Windows">Windows</a> from the KDE's mirror infrastructure. For the optimal sound experience we also recommend to use the new Phonon backends, released this weekend as well: <a href="http://download.kde.org/stable/phonon/phonon-backend-vlc/0.6.0/src/phonon-backend-vlc-0.6.0.tar.xz.mirrorlist">phonon-backend-vlc 0.6</a> or <a href="http://download.kde.org/stable/phonon/phonon-backend-gstreamer/4.6.2/src/phonon-backend-gstreamer-4.6.2.tar.xz.mirrorlist">phonon-backend-gstreamer 4.6.2</a>