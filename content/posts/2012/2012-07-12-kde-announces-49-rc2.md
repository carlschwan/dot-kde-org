---
title: "KDE Announces 4.9 RC2"
date:    2012-07-12
authors:
  - "sebas"
slug:    kde-announces-49-rc2
comments:
  - subject: "A shame that kde 4.9 is too late"
    date: 2012-07-16
    body: "A shame that kde 4.9 is too late for Debian wheezy:\r\nhttp://lists.debian.org/debian-devel-announce/2012/06/msg00009.html\r\n\r\nLooks like a nice release"
    author: ""
  - subject: "KDE is super awesome"
    date: 2012-07-17
    body: "Thank you for making KDE even more awesome."
    author: ""
  - subject: "Well if it would be a point release..."
    date: 2012-07-17
    body: "They might have included it, as it is only bugfixes... I'd really love it if my favorite distro would be a little less reluctant to do such updates. While I understand their reasons..."
    author: ""
  - subject: "RC2 for Kubuntu?"
    date: 2012-07-18
    body: "Does anybody know when 4.9RC2 will be available for Kubuntu quantal? Beta2 is the latest shown. But even that has caused no trouble. It's great that these early releases are so stable."
    author: ""
  - subject: "KDE RC2 for FreeBSD"
    date: 2012-07-20
    body: "Does anybody know when 4.9RC2 will be available for FreeBSD?"
    author: ""
---
Today KDE <a href="http://kde.org/announcements/announce-4.9-rc2.php">released</a> the second release candidate for its Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and other polishing. Highlights of 4.9 will include:

<ul>
    <li>
    Qt Quick in Plasma Workspaces. Qt Quick continues to make its way into the Plasma Workspaces. The Qt Quick Plasma components, which were introduced with 4.8, mature further. Additional integration of various KDE APIs was added through new modules. More parts of Plasma Desktop have been ported to QML. While preserving their functionality, the Qt Quick replacements of common Plasmoids are more attractive, easier to enhance and extend and behave better on touchscreens.
    </li>
    <li>The Dolphin file manager has been improved in its display and sorting and searching based on metadata. Files can now be renamed inline, giving a smoother user experience.
    <li>
    Deeper integration of Activities for files, windows and other resources. It is now easier for users to associate files and windows with an Activity, and enjoy better Workspace organization. Folderview can now show files related to an Activity, making it easier to organize files into meaningful contexts.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.9_Feature_Plan">4.9 Feature Plan</a>. There are many changes, so 4.9 needs a good testing in order to maintain and improve quality and to deliver an exceptional user experience.
<!--break-->
<h2>Testing in Progress</h2>

As with earlier 4.9 releases, testing continues for the release candidate RC2.
 
The KDE Testing Team is collaborating directly with the developers of several KDE projects. There is also more work going on in collaboration with the KDE sysadmins to facilitate automatic testing for all KDE projects. Continuous integration is already used by several core components of KDE.

To join the KDE Testing Team, please come to the #kde-quality channel on irc.freenode.net or join the mailing list <a href="https://mail.kde.org/mailman/listinfo/kde-testing">kde-testing@kde.org</a>.

<!--Screenshot is outdated; needs to be replaced with current version-->
<!--<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="http://kde.org/announcements/4.8/screenshots/plasma-desktop-4.8.png"><img src="http://kde.org/announcements/4.8/screenshots/thumbs/plasma-desktop-4.8.png" align="center" width="525" alt="Plasma Desktop with the Dolphin file manager" title="Plasma Desktop with the Dolphin file manager" /></a>
<br />
<em>Plasma Desktop with Dolphin and Gwenview</em>
</div>-->

<h3>KDE 4.9 Release Candidate 2</h3>

The KDE Software Compilation, including all its libraries and its applications, is available for free under Open Source licenses. KDE's software can be obtained in source and various binary formats from <a href="http://download.kde.org/unstable/4.8.97/">download.kde.org</a> or from any of the <a href="http://www.kde.org/download/distributions.php">major GNU/Linux and UNIX systems</a> shipping today's release.