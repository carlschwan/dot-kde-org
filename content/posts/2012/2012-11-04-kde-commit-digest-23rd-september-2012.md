---
title: "KDE Commit-Digest for 23rd September 2012"
date:    2012-11-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-23rd-september-2012
---
In <a href="http://commit-digest.org/issues/2012-09-23/">this week's KDE Commit-Digest</a>:

<ul><li>KDE-Workspace introduces dedicated OpenGL1 and OpenGL2 compositing types and sees more work on compositing</li>
<li>User interface improvements in KDE-Workspace: a keyboard shortcut can stop the current activity in <a href="http://www.kde.org/workspaces/plasmadesktop/">Plasma</a>, drop behavior is improved</li>
<li><a href="http://www.konqueror.org/">Konqueror</a> re-adds the ability to select the sessions to restore</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> adds a slide chooser</li>
<li><a href="http://userbase.kde.org/Rekonq">Rekonq</a> offers integrated spell-checking</li>
<li>Internationalization work in <a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a></li>
<li>Refactoring work in <a href="http://amarok.kde.org/">Amarok</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-09-23/">Read the rest of the Digest here</a>.