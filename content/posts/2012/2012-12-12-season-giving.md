---
title: "Season of Giving"
date:    2012-12-12
authors:
  - "Wade"
slug:    season-giving
---
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://sathyasays.com/2008/12/15/celebrate-the-holiday-spirit-with-some-kde-christmas-wallpapers/"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde_treeglow2_small.jpg" width="300" /><br />KDE holiday wallpapers!</div>
For KDE, giving is always in season. In 2012, KDE community members all over the world met and collaborated with a strong desire to produce the best software possible.  They've spent countless hours delivering new features, while supporting existing versions with <a href="http://www.kde.org/info/releases.php">timely releases</a> and bugfixes. Working with students in programs such as the Google Summer of Code, Google Code-In and the Season of KDE, KDE mentors gave time and guidance towards developing new technical talent. Truly, the KDE Community thrives by giving all year round.

KDE is one of the largest free software communities in the world. This year, we paved the way for future growth by giving ourselves a <a href="http://manifesto.kde.org/">Manifesto</a> that promotes the best aspects of free software: openness, inclusivity and innovation.  And yet, despite its size, KDE operates transparently and prudently through <a href="http://ev.kde.org/">KDE e.V. and its Board</a>. Every day, all over the globe, more and more individuals, schools, businesses and governments rely on KDE; they trust KDE to give them full control over their data and hardware and to support ongoing efforts to make technology open and available. This is a gift the KDE Community gives gladly to anyone who wants it.



During this season of giving, some people ask how to say "Thank You" for another year of hard work from this international community. You can play an important and generous part by <a href="http://jointhegame.kde.org/">Joining the Game</a>. You can be confident that every donation is used judiciously to grow the community, attract new contributors of all talents and build better software than ever before. So if you are looking for a perfect gift to a community that seems to require very little ... if you want to give something back for what you have been given, please consider KDE's <a href="http://jointhegame.kde.org/">Join the Game membership program</a>.