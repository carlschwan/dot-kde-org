---
title: "KDE Commit-Digest for 18th December 2011"
date:    2012-01-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-18th-december-2011
comments:
  - subject: "Good numbers !"
    date: 2012-01-10
    body: "Just looking at the quantity of bugs indicated in those digests:\r\nOn Nov. 20 we had : 23460 bugs\r\nOn Dec. 18 we had: 22106 bugs\r\n\r\nThat is a diminution in 1 month of more than 1100 open bugs. If we take into consideration that an average of ~500 new bugs are opened every week, then we can suppose that more than 3000 bugs were closed in less than a month !\r\n\r\nCriticists against KDE quality and QC have been extremely vocal, especially since the beginning of the KDE 4 era. I would like to take these bug numbers to show that, even though everything is not yet perfect, the KDE community is silently listening to users and makes lots of improvements to the software compilation.\r\n\r\nThanks"
    author: "alvise"
  - subject: "Remarkable quality"
    date: 2012-01-10
    body: "According to Steve McConnell (Code Complete), \"There are about 15 - 50 errors per 1000 lines of delivered code.\" KDE has over 6 million (6,000K) lines of code (LOC, not including Qt). At the low end of McConnell's range, that would be 90,000 bugs.\r\n\r\nSpeaking of 90,000, KWin has that many LOC and 367 reported bugs...about 1/4 of the lowest range of McConnell's numbers.\r\n\r\nI would take these bug numbers (which include duplicates) to show that KDE has exceptional quality.\r\n\r\nRecognizing, of course, that even one bug can be irritating from an individual user's point of view.\r\n\r\nDebugging typically accounts for 40% or more of development time. So any assistance with bug triaging, accurate reporting, and minor fixes can make a big difference in KDE software quality.\r\n\r\nIf you're interested in contributing, please check out http://community.kde.org/Getinvolved > Development > Tasks. There's also a new resource available at http://www.flossmanuals.net/kde-guide/.\r\n"
    author: "kallecarl"
  - subject: "Re: Good Numbers !"
    date: 2012-01-10
    body: "You might want to consider categorizing those bug reports. Doing it comprehensively doesn't speak to the issues at hand, i.e., backend vs frontend issues.\r\n\r\nMost people in the end will be users. And, then there is the developer community. Each has a different perspective and use. \r\n\r\nAs more of a user than developer these days due to disability and full-time work, I am more interested in understanding the concerns of new version releases with regards to end-user performance rather than developer.\r\n\r\nMy proposal, with regards to the above statistics, is to separate into at least these two categories, end-user and developer, so that each can get a clear idea of what is really ready for use now or to wait later.\r\n\r\nYou will see a huge reduction of flaming on the core KDE development team.\r\n\r\nRegards,\r\nBruce Wolfe\r\n\r\nP.S. If it means anything, I am a social worker/scientist with an emphasis on social development."
    author: "bmw"
  - subject: "Re: Good Numbers !"
    date: 2012-01-12
    body: "Sure.\r\n\r\nBut the simple fact that overall quantity of open bugs is decreasing after a long period of increase, shows that there is a very positive inversion of trend."
    author: ""
  - subject: "Quality"
    date: 2012-01-13
    body: "You consider 15 - 50 errors per 1000 lines of delivered code exceptional quality? I know 1000 lines sounds like lots of lines, but that's 1,5 - 5 errors per 100. Now 100 lines is not that much, 100 lines is just a small piece of code, particularly considering how verbose C++ is (which probably accounts for most of the sources). And what you're saying is that there's a good chance that each of those small 100 lines code snippets contains at least 1 or 2 bugs. Worst case scenario is 1 bug per every 20 lines. \r\n\r\nMaybe this is normal or at least usual in huge software projects. Quite frankly I don't know. But it does sound like quite lot a to me.  "
    author: "jadrian"
  - subject: "Comparative quality"
    date: 2012-01-13
    body: "No, I don't consider 15 - 50 errors per 1000 lines of delivered code exceptional quality. That information was taken from Code Complete, describing the range of errors of most software.\r\n\r\nCompared to that, the KDE errors/KLOC ratio is significantly less than the low end of the Code Complete range. In other words, \"exceptional quality\" relative to \"most software\".\r\n\r\nSo you're skeptical of what Steve McConnell wrote. That's fine. My point is really about putting things in context. From time to time, people drop in here or at bugs.kde.org and complain about KDE quality. Usually because their pet peeve has not been resolved. These complaints are not accurate indications of KDE software quality.\r\n\r\nIn addition, the number of open KDE bugs is not accurate because, for one thing, it includes duplicates. It would improve quality for people to get involved...\r\n1 - bug triaging and dedupping would reduce the number of \"errors\".\r\n2 - developers who are now dealing with bug management could put more time into fixing bugs instead.\r\n\r\n"
    author: "kallecarl"
  - subject: "mark"
    date: 2012-01-15
    body: "And does anyone compare KDE 4 to KDE 3?\r\n\r\nNo.\r\n\r\nWhat if the same amount of energy would have been invested into KDE 3, without all the hype?\r\n\r\nAnyway, the really more interesting thing is the QT 3 vs. QT 4 situation.\r\n\r\nQT 4 exploded in size.\r\n\r\nWhat is the definite advantage that this increase in size really has now?"
    author: ""
  - subject: "Good point"
    date: 2012-01-15
    body: "Just imagine how useful that information might be!\r\n\r\nPlease <a href=\"http://dot.kde.org/content/contact-dot-editors\">Contribute a Dot Story</a> when you have completed your analysis."
    author: "kallecarl"
---
In <a href="http://commit-digest.org/issues/2011-12-18/">this week's KDE Commit-Digest</a>:

<ul>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, extended undo/redo functionality, updated Excel 2000/2003 support, optimization of formula parsing and numerous bugfixes</li>
<li><a href="http://kde.org/applications/multimedia/kmix/">Kmix</a> now shows <a href="http://www.mpris.org/2.1/spec/">MPRIS2</a> streams</li>
<li>More useful capacity bar when hovering collection in <a href="http://amarok.kde.org/">Amarok</a></li>
<li>Work on import feature in <a href="http://uml.sourceforge.net/">Umbrello</a></li>
<li>Command line parameters support in kmail-mobile</li>
<li>Incremental fetching of Global Address List (GAL) in <a href="http://pim.kde.org/akonadi/">Akonadi</a></li>
</ul>

<a href="http://commit-digest.org/issues/2011-12-18/">Read the rest of the Digest here</a>.