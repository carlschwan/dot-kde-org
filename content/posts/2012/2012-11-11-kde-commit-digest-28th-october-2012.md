---
title: "KDE Commit-Digest for 28th October 2012"
date:    2012-11-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-28th-october-2012
---
In <a href="http://commit-digest.org/issues/2012-10-28/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.calligra-suite.org/">Calligra</a> improves Video Shape: adds playback controls to video playback (Volume Control, Playback seeker), ability to save video within ODF documents and thumbnails; <a href="http://www.kexi-project.org/">Kexi</a> shows indication that edited or pasted text is <a href="http://blogs.kde.org/2012/10/27/kexi-26-text-trimming">trimmed</a></li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> adds bookmarks functionality to Filesystem Toolview; massive code refactoring and bug fixes</li>
<li><a href="http://userbase.kde.org/Rocs">Rocs</a> adds initial version for documentation widget</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> gains a new effect: Maximize Window</li>
<li><a href="http://userbase.kde.org/Rekonq">Rekonq</a> reports the open/close document events to Activity Manager daemon.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-10-28/">Read the rest of the Digest here</a>.