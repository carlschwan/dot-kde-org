---
title: "LaKademy 2012 \u2012 Artwork, Localization, Promotion, Development"
date:    2012-05-06
authors:
  - "sandroandrade"
slug:    lakademy-2012-‒-artwork-localization-promotion-development
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/LaKademyLogo300.jpg" /></div>
<a href="http://br.kde.org/LAkademy">LaKademy</a> is over. April 27th to May 1st were days of hard work, meeting old and new friends, having much fun while realizing how far crazy guy imaginations can go. It was a great summit and sprint. Compared to the first (and only one so far) Akademy-BR in 2010, there is clear evidence that we are making progress building a strong and mature KDE community in Latin America. We met in a lovely hostel in Porto Alegre, Brazil and were well taken care of during the event.
<!--break-->
LaKademy brought together 19 Latin American participants, including developers, translators, designers and promoters. <a href="http://liveblue.wordpress.com/2012/05/06/lakademy-hierarchical-edge-bundles-and-kdevelop/">The full report</a> has more information about major LaKademy outcomes related to artwork, localization, promotion, and developing.

Thank you <a href="http://ev.kde.org/">KDE e.V.</a> and Claudia Rauch for your support.