---
title: "Registration opens for Akademy BoFs and Workshops"
date:    2012-06-05
authors:
  - "kallecarl"
slug:    registration-opens-akademy-bofs-and-workshops
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/AkademyLogo225px.png" /></div>The schedule for <a href="http://akademy.kde.org/registration-open-workshop-bof-sessions">Akademy Workshops and Birds of a Feather</a> sessions (BoFs) is available for registration. Organized in the Unconference style, people attending Akademy can select the preferred times and locations for topics they will lead.
<!--break-->
These sessions are a vital part of Akademy. New projects are proposed; existing ones get intensely focused attention. People are encouraged to attend Akademy to take part in these sessions that play such a critical role within KDE.

The KDE Community prizes innovation and creativity. Akademy Workshops and BoFs offer space for possibility that is hard to find anywhere else. Akademy 2012 Tallinn is the place to be if you want to make a difference working with enthusiastic, supportive people.

<h2>Akademy 2012 Tallin, Estonia</h2>

For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.