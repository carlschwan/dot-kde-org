---
title: "Akademy Keynote: Dr. Mathias Klang - Freedom of Expression"
date:    2012-04-22
authors:
  - "tcanabrava"
slug:    akademy-keynote-dr-mathias-klang-freedom-expression
comments:
  - subject: "Great interview!"
    date: 2012-04-23
    body: "This is one of the most interesting interviews I've read.\r\n\r\nThis might be a silly question, given the context, but anyway: can I quote Dr. Klang's point of view about free software?\r\n\r\nTo me, that's a beutiful and simple way to explain the importance of free software to non-programmers o \"computer illiterates\".\r\n\r\nThank you!\r\n"
    author: "ricardo.barberis"
  - subject: "CC license terms"
    date: 2012-04-23
    body: "This article reports that he publishes his blog (and comments such as his view about free software) under CC License. His blog <a href=\"http://www.digital-rights.net/\">Sound and Fury</a> has copyright terms <a href=\"http://creativecommons.org/licenses/by-nc/3.0/\">(CC BY-NC 3.0)</a>.\r\n\r\nIf you really want to stay on the straight and narrow, you should consult an IP attorney. It benefits both KDE and Dr. Klang for you to quote. Just give credit appropriately."
    author: "kallecarl"
  - subject: "just ask him"
    date: 2012-07-02
    body: "You can simply send him a mail. Maybe he\u2019ll allow you to use his text under cc by or GPL."
    author: "ArneBab"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KlangCreativeCommons300px.jpg" /><br /><strong>Mathias Klang</strong>&nbsp; <small>photo by Ola Waagen</small></div>
Dr. Mathias Klang is a researcher and senior lecturer at the University of Göteborg in Sweden. His research revolves within the field of legal informatics with particular interest in copyright, democracy, human rights, free expression, censorship, open access and ethics. He holds Master of Laws and Ph.D. degrees. 

Dr. Klang will deliver one of the keynote talks at <a href="http://akademy2012.kde.org/">Akademy 2012</a> in Tallinn, Estonia. My <a href="http://dot.kde.org/2012/04/11/akademy-keynote-dr-will-schroeder-kitware-ceo">interview with another keynoter, Dr. Will Schroeder</a>, was published recently on the Dot.
<!--break-->
<strong>Tomaz:</strong> Some Creative Commons advocates claim that it is hard to do a Creative Commons book. What is your view?

<strong>Mathias:</strong> Most of the material I produce on a daily basis (blogging, slides) are published under CC licenses. Most of my larger works (articles, books) are also under similar licenses. But unfortunately not all my material can be spread in the same way. This pains me but it is also part of a realization that as an ambitious academic, I need to publish in certain journals, not all of which are open access or accept CC licensing alternatives. This need is based on the reward mechanism within academia—without publications, no promotion; without promotion, no freedom to publish more material under CC licenses. This pragmatic approach is close to not practicing what I preach. My defense is that I attempt to maintain a positive open/free balance.

A note on the publishing question: few non-English speaking countries are happy with CC publishing. It's usually the publishers who argue that there is no market for the books. Personally I think there is, and that many publishers are just being very conservative and afraid of change or experimentation.

Finally it's also important to point out the CC licensing is about choice. Free Software has a much more rigorous approach.

<strong>Tomaz:</strong> How would you summarize your point of view about digital rights and specifically, free software?

<strong>Mathias:</strong> For me, Free Software is not really about <strong>MY</strong> ability to read, adapt or spread code (as I am not a programmer). Free Software for me is similar to Freedom of Expression and Freedom of Information. Without freedoms of expression and information, we cannot look at what our politicians are doing and spread information about any abuses that may exist. The fact that I am not able to look for good/bad/evil code does not change the fact that <strong>the ability to do so</strong> is vitally important to me. I rely on political journalists to use the Freedom of Information Act to find information, and freedom of expression to inform me. The same approach applies to software. I rely on programmers to look beneath the hood and inform me. And I will argue forever their right to do so—if for nothing else than that it is vital to my ability to live in a good society.

My concern in relation to proprietary or Free Software lies in the fact that we are talking about the fundamental infrastructure of modern society. Through software we make decisions, communicate, control and create freedoms. If I am unable to see who is controlling what, then all these things are at risk of being taken away from me.

<strong>Tomaz:</strong> You have written that the law seems to always be trying to catch up with our activities. Considering the volume of activity on the Internet, is it necessary to regulate it?

<strong>Mathias:</strong> I am always concerned with calls to regulate the Internet. This is because we tend to follow one-sided views. When we are upset about, for example, terrorism, the cry is for more surveillance; when a scandal in relation to data integrity appears, we cry for the need for privacy. We should not regulate while our understanding of the technology remains all too immature. Take the example of cars: we know they cause environmental problems and accidents, but we also know that they are important to our lifestyles. When we talk regulation, we begin with a balance. When we talk about Internet regulation, we lack balance.

<strong>Tomaz:</strong> You are a big fan of the Creative Commons license. What makes you such a supporter?

<strong>Mathias:</strong> It's all about choice. Before I came online (yes I am that old), I thought that the best method was to control cultural production through systems such as copyright. The Internet taught me to share. I realized empirically that altruism and sharing actually will reward the sharer to a much higher degree than control. CC helps me share, helps me signal my desire to share. Basically CC removes some of the friction inherent in sharing. It's a beautiful system through simplicity: I find something I need/like and see the CC license, therefore I can use it. Or if I create something, I can put it online and let people share it. I don't have to handle permissions or requests. The more my stuff spreads, the bigger I become. Sharing is friendly and profitable.

<strong>Tomaz:</strong> And you publish your intellectual work with a Creative Commons license?

<strong>Mathias:</strong> Most of it. Some journals don't let me even use pre-publication copies. But most of the time, I make my material available under CC licenses. Two books out of three so far.

<strong>Tomaz:</strong> Do you think we’re heading for a “copyright-free” society?

<strong>Mathias:</strong> No. I don't think so. And I am not sure it would be a good thing either. All licenses require ownership first. Without the ability to say "this is mine", there is no ability to license. But I do think, or hope, that we will see major changes to copyright (in the long run). Some of the things I would like to see are shorter (and maybe renewable) terms of protection, increased strength in personal use, and exceptions for cultural artifacts in museums and archives.

<strong>Tomaz:</strong> Earlier this year, the Internet went through a major commotion when the U.S. Congress proposed the Stop Online Piracy Act (SOPA) and the Protect Intellectual Property Act (PIPA). What’s your opinion about this legislation? What should governments do?

<strong>Mathias:</strong> So I can understand the desire of corporations to regulate. They are stuck in outdated business models (some more profitable than others). The problem is the costs of production and policing these rules. The rules will not stop illegal acts. And the legality of such laws is complex to discern (even to lawyers). In the end, we will have to decide whether it is okay to carry the costs of copyright surveillance, and determine if this surveillance is actually making a positive difference.

It is interesting to note that illegal file sharing did not decrease through harsher regulation and increased surveillance. Illegal file sharing disappears when useful legal alternatives are made available at a market price.

Additionally there is a side effect that damages the rights we are supposed to have in society.

<strong>Tomaz:</strong> How far do you think property rights extend to intellectual works on the Internet?

<strong>Mathias:</strong> We have taken old models built for completely different technological stages and applied them with force to a reality that they are not built for. There is space for intellectual property rights in digital environments, but we have not seen this done properly yet. So far we are still trying to force the square plug into the round hole ... we are wasting time on outdated models instead of planning for a future reality.

<strong>Tomaz:</strong> Back to free software. Do the number and variety of Free and Open Source licenses slow down their adoption?

<strong>Mathias:</strong> Nice question! Maybe (sorry for the boring answer). Generally no, I don't think that adoption is hindered. The problem is not really on the licensing side. Naturally licenses are hugely important, but the adoption of Free Software is not really hindered by licenses. The problem usually occurs when people want the license, but don't want to follow the terms of the license.

<strong>Tomaz:</strong> How can Free Software licensing be taken seriously with rigorous licenses like MIT, the Apache License or the GPL, and joke ones like "Do Whatever You Want"?

<strong>Mathias:</strong> It's fine for those who want to joke around. Really I think the projects using joke licenses are really about people who do not have enough confidence in what they are doing. The work is vitally important—not each project on its own but the sum total—and throwing away time and energy on silly licenses is just a little sad. I wish the developers would show more confidence in their work.

<strong>Tomaz:</strong> Thank you for sharing your insights on these subjects of critical interest to the KDE Community. I'm looking forward to hearing you speak at Akademy 2012 in Tallinn.

<h2>Akademy 2012 Tallin, Estonia</h2>

For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet face to face to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. In addition, the Community welcomes companies building on KDE technology, or looking to begin using it.