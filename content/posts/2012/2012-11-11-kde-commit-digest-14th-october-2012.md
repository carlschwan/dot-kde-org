---
title: "KDE Commit-Digest for 14th October 2012"
date:    2012-11-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-14th-october-2012
---
In <a href="http://commit-digest.org/issues/2012-10-14/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://nepomuk.kde.org/">Nepomuk</a> gains new backup system</li>
<li>In <a href="http://userbase.kde.org/KWin">KWin</a>, decoration can announce whether it currently requires an alpha channel</li>
<li>Apper uses PackageKit-qt API in QML</li>
<li><a href="http://edu.kde.org/kalgebra/">KAlgebra</a> adds a new Plasmoid for plotting graphs</li>
<li><a href="http://yakuake.kde.org/">Yakuake</a> adds GHNS (<a href="ghns.freedesktop.org">Get Hot New Stuff</a>) support for skins</li>
<li>Internationalization fixes in <a href="http://edu.kde.org/kstars/">KStars</a></li>
<li>Bugfixes in <a href="http://www.digikam.org/">Digikam</a>, <a href="http://okular.org/">Okular</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-10-14/">Read the rest of the Digest here</a>.