---
title: "KDE Ships October Updates to Plasma Workspaces, Applications and Platform"
date:    2012-10-02
authors:
  - "sebas"
slug:    kde-ships-october-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "Bugs fixed"
    date: 2012-10-03
    body: "Bug reports for 305629 and 305633 show \"Version Fixed In: 4.10\". For some reason, they are included in Bugzilla list for 4.9.2.\r\n"
    author: "m_goku"
  - subject: "Bugzilla query seems wrong"
    date: 2012-10-03
    body: "The query seems to include all bugs for which the \"Version fixed in\" field changed once to 4.9.2. A better search would be: https://bugs.kde.org/buglist.cgi?j_top=OR&f1=cf_versionfixedin&list_id=232972&o1=equals&o2=equals&query_format=advanced&f2=target_milestone&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&v1=4.9.2&v2=4.9.2"
    author: "mgraesslin"
  - subject: "Bugzilla query link replaced..."
    date: 2012-10-04
    body: "to show \"Fixed In Version: 4.9.2\" as suggested by Martin Gr\u00e4\u00dflin. "
    author: "kallecarl"
  - subject: "Cool new clock!"
    date: 2012-10-21
    body: "I just wanted to say thank you to whoever worked on the plasma clock. It\u2019s much easier to grasp the time with a single glance, now, and that\u2019s an activity I do many times a day, so you made my life much more pleasant by improving that small but really significant detail.\r\n\r\nThank you for the love you give your code!"
    author: "ArneBab"
---
Today KDE <a href="http://kde.org/announcements/announce-4.9.2.php">released</a> updates for its Workspaces, Applications, and Development Platform.
These updates are the second in a series of monthly stabilization updates to the 4.9 series. 4.9.2 updates bring many bugfixes and translation updates on top of the latest edition in the 4.9 series and are recommended updates for everyone running 4.9.1 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
<br /><br />
Significant bugfixes include improvements to the Kontact Suite, bugfixes in Dolphin, Plasma and many more corrections and performance improvements all over the place. KDE's development platform has received a number of updates which affect multiple applications. The changes are listed on <a href="https://bugs.kde.org/buglist.cgi?j_top=OR&f1=cf_versionfixedin&list_id=232972&o1=equals&o2=equals&query_format=advanced&f2=target_milestone&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&v1=4.9.2&v2=4.9.2">Bugzilla</a>. For a detailed list of changes that went into 4.9.2, you can browse the Subversion and Git logs. 4.9.2 also ships a more complete set of translations for many of the 55+ supported languages.

To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.9.2.php">4.9.2 Info Page</a>. If you would like to find out more about the KDE Workspaces and Applications 4.9, please refer to the <a href="http://www.kde.org/announcements/4.9/">4.9 release notes</a> and earlier versions.