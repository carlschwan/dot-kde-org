---
title: "KDE Commit-Digest for 11th March 2012"
date:    2012-03-18
authors:
  - "mrybczyn"
slug:    kde-commit-digest-11th-march-2012
comments:
  - subject: "Dziekuje (Thank you)"
    date: 2012-03-20
    body: "Dziekuje droga Marto za te nowinki, \u015bwietnie si\u0119 czyta o zmianach zachodz\u0105cych w najlepszym DE na Linuxa :)"
    author: ""
  - subject: "na zdrowie"
    date: 2012-03-23
    body: "na zdrowie"
    author: ""
---
In <a href="http://commit-digest.org/issues/2012-03-11/">this week's KDE Commit-Digest</a>:

<ul><li>Work on diagram auto layout feature in <a href="http://uml.sourceforge.net/">Umbrello</a></li>
<li>'Create new class' dialog for <a href="http://www.ruby-lang.org/en/">Ruby</a> support in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Work on object custom dimensions tabs and support for Matlab's .mat format in <a href="http://kst.kde.org/">Kst</a></li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> gains a horizontal coordinates grid</li>
<li>Support of <a href="http://wiki.openstreetmap.org/wiki/TangoGPS">TangoGPS</a> .log files in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>In <a href="http://nepomuk.kde.org/">Nepomuk</a>, support for urlregex to find the right plugin; work on TV show support</li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, multiple improvements in bibliography support, improved Microsoft Office format support</li>
<li>Scopes manager merged in <a href="http://www.kdenlive.org/">Kdenlive</a></li>
<li>Import/Export Routes for VPN connections in <a href="http://userbase.kde.org/NetworkManagement">Network Management</a></li>
<li>Improved sound effects in <a href="http://www.kde.org/applications/games/kajongg/">Kajongg</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-03-11/">Read the rest of the Digest here</a>.