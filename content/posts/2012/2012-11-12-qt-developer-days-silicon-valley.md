---
title: "Qt Developer Days in Silicon Valley"
date:    2012-11-12
authors:
  - "kallecarl"
slug:    qt-developer-days-silicon-valley
comments:
  - subject: "just signed up"
    date: 2012-11-13
    body: "Just registered for the one in Silicon Valley. This is such a critical juncture for Qt, it will be great to hear about future plans."
    author: "eean"
---
<a href="http://www.qtdeveloperdays.com/">Qt Developer Days 2012</a> are the premier Qt events of the year. The 2012 Conference for Europe started today in <a href="http://qtconference.kdab.com/">Berlin</a> co-hosted by <a href="http://ev.kde.org/">KDE e.V.</a>; there will be news and more information from the Conference over the next few days. Meanwhile, preparations are well underway for Qt Developer Days – North America in Silicon Valley, which will take place December 5 – 7. It is presented by <a href="http://www.ics.com/">ICS</a>, <a href="http://www.kdab.com/">KDAB</a> and <a href="http://digia.com/">Digia</a> and is being organized by ICS. 

The KDE e.V. Board expressed support in the following statement:
<blockquote>The KDE community is delighted to be a partner in the upcoming Qt Developer Days 2012. KDE has built its software on Qt for the last 15 years, will continue to do so and is looking forward to help shape its future. Qt has a bright future and events like the upcoming Qt Developer Days 2012, in Europe and North America, are an essential part of the collaboration in an ecosystem with so many partners like the Qt Project.</blockquote>

KDE has been based on the <a href="http://qt.digia.com/Product/">Qt framework</a> from the beginning, and has been instrumental in Qt’s progress ever since—from licensing to governance to development and extensive use. Qt development is done within the <a href="http://qt-project.org/">open source Qt Project</a> with contributions from individuals and companies that use Qt applications. 

There are plenty of opportunities at Qt Developer Days:
<ul>
<li><a href="http://www.qtdeveloperdays.com/northamerica/detailed-schedule">training and information sessions</a> from Qt experts</li> 
<li>connect with people and companies in the Qt ecosystem</li>
<li>find a job or fill a position</li>
<li>recruit contributors</li>
<li>join a project or create one</li>
<li>collaborate</li>
<li>help create the future of Qt</li>
</ul>

Qt and related applications are already used by over 450,000 developers from around the world. Even so, there is a shortage of quality Qt developers. Work is underway to bring Qt support to Android, iOS and Windows 8, which will further increase Qt's influence, especially in mobile and other devices, as well as the need for skilled Qt developers.

Jos Poortvliet, Community Manager for OpenSUSE and long-time KDE contributor, said, “[There will be many] people there from all over the (huge) Qt ecosystem. It'll be interesting to talk to people outside of the usual Linux crowd: Qt has managed to grow well beyond the Linux Desktop into an industry standard for a wide variety of use cases.”

Experienced Qt developers, people just starting to explore Qt possibilities, KDE Google Summer of Code or Code-In participant or Free Software aficionado, whatever interest you have in Qt, Developer Days has something valuable to offer. Qt Developer Days has been a marquee Qt event since it was started by ICS in 2004. It is a must-attend event for anyone involved with Qt. <a href="http://www.qtdeveloperdays.com/northamerica">Register soon</a>.

ICS is sponsoring complimentary attendance for a limited number of people associated with KDE. Please <a href="mailto:csymons@kde.org?cc=rauch@kde.org&subject=Qt%20Developer%20Days%20information%20">email for additional information</a>.