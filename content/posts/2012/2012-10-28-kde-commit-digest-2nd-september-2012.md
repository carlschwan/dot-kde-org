---
title: "KDE Commit-Digest for 2nd September 2012"
date:    2012-10-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-2nd-september-2012
---
In <a href="http://commit-digest.org/issues/2012-09-02/">this week's KDE Commit-Digest</a>:

<ul><li>KActivities enables logging of desktop events</li>
<li>Scripts can add menus to useractions menu in KDE-Workspace</li>
<li>Keyboard navigation works in <a href="http://en.opensuse.org/Kickoff">Kickoff</a> GUI</li>
<li>KWin's global <a href="http://www.freedesktop.org/wiki/Software/dbus/">D-Bus</a> interface splits into two</li>
<li>New KNumber library in <a href="http://utils.kde.org/projects/kcalc/">KCalc</a></li>
<li>New options in the <a href="http://edu.kde.org/kstars/">KStars</a> field of view (FOV) calculator</li>
<li>Various optimizations: <a href="http://www.ruby-lang.org/en/">Ruby</a> parser in <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://pim.kde.org/akonadi/">Akonadi</a> database accesses</li>
<li>Bugfixes in KDE-Workspace, KDE-libs, <a href="http://nepomuk.kde.org/">NEPOMUK</a>, KStars, <a href="http://pim.kde.org/">KDE-PIM</a>, Calligra.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-09-02/">Read the rest of the Digest here</a>.