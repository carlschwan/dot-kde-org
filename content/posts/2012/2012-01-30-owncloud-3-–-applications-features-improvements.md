---
title: "ownCloud 3 \u2013 Applications, Features, Improvements"
date:    2012-01-30
authors:
  - "kallecarl"
slug:    owncloud-3-–-applications-features-improvements
comments:
  - subject: "risk"
    date: 2012-02-01
    body: "Creating an interface to allow multiple companies and individuals the ability to host their files remotely using software like this will be interesting in the aspect of whether or not creating your own online storage will remove the problems involved with the initial reason for backing up your files - Namely, the idea that the smaller an infrastructure, the greater the damage to the whole than a a larger infrastructure that spreads the risk of failure over its entirety. "
    author: "coreywidman"
---
ownCloud allows full control of cloud data storage. Version 3 of this open source File, Sync & Share Project has just been announced. Now users can do more than ever with their own private file-sharing cloud. In addition, its open, modular structure makes ownCloud the only cloud technology to offer several important and useful functions. Visit <a href="http://owncloud.org/">ownCloud</a> for the full announcement and download instructions.
<!--break-->
With the added functionality of version 3, ownCloud offers:
<ul>
<li>Privately controlled cloud file management</li>
<li>Freedom from jurisdiction disputes and censorship dilemmas</li>
<li>Ease of use</li>
<li>Transparent management</li>
<li>No proprietary lock-in</li>
<li>Broad community and commercial support base</li>
<li>Selective, secure file sharing</li>
<li>Application store for third party applications and plugins made possible by ownCloud’s extensibility and openness</li> 
<li>Direct editing of text files</li>
<li>Integrated PDF viewer and Photo Gallery</li>
<li>User friendly web interface to calendaring and other Personal Information Management tools</li>
<li>Support for integrating external groupware solutions or webmail interfaces into the ownCloud GUI</li>
<li>Improved integration between ownCloud and LDAP or Active Directory servers.</li>
</ul>

“Version 3 shows major progress for ownCloud. And not just in the technology. We’re seeing more and more contributions from our expanding community. Increased participation from individuals and organizations shows the value of ownCloud and the strength of open source technology.” said Frank Karlitschek, ownCloud founder.

<h3>About ownCloud</h3>
<a href="http://owncloud.org/">ownCloud</a> is a popular open source community project started at a KDE Community event in 2010 to bring greater flexibility, access and security to data in the cloud. <a href="http://dot.kde.org/2011/10/11/owncloud-2-released">The project was warmly welcomed</a> as a way to extend KDE innovation beyond the desktop.

<h3>About ownCloud, Inc.</h3>
<a href="http://www.owncloud.com">ownCloud Inc.</a> offers paid software and services for ownCloud. Company headquarters are in Boston. There’s an international office and development center in Germany. Commercial product and service offerings will be announced in the first quarter of 2012.