---
title: "KDE Commit-Digest for 21st October 2012"
date:    2012-11-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-21st-october-2012
---
In <a href="http://commit-digest.org/issues/2012-10-21/">this week's KDE Commit-Digest</a>:

<ul><li>SMB KIO (Samba) stores passwords in wallet when the "Remember password" checkbox is checked in the authorization dialog</li>
<li><a href="http://www.kde.org/applications/development/kcachegrind/">KCachegrind</a> loads multiple files on command line into one window</li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, "Rename" action in Project Navigator can be used to change object's caption</li>
<li><a href="http://amarok.kde.org/">Amarok</a> supports Amazon MP3 in Spain and Italy</li>
<li><a href="http://www.konqueror.org/">Konqueror</a> reports the open/close document events to Activity Manager daemon</li>
<li><a href="http://www.digikam.org/">Digikam</a> disables the current implementation of a Digikam-Nepomuk integration, work being done on a new one</li>
<li><a href="http://gwenview.sourceforge.net/">Gwenview</a> adds the possibility to adjust the aspect ratio of a thumbnail</li>
</ul>

<a href="http://commit-digest.org/issues/2012-10-21/">Read the rest of the Digest here</a>.