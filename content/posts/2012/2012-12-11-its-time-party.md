---
title: "It's time to party!"
date:    2012-12-11
authors:
  - "joseeantonior"
slug:    its-time-party
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="hhttp://dot.kde.org/sites/dot.kde.org/files/cake_0.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/cake_small.png" /></a><br />Cool Cake.</div>
As many of you may know, the release of KDE 4.10 is coming soon. That means the time for Release Parties is close and planning can start! These events can happen anywhere around the world, you just need to have somewhere to do it, some attendees, and the party will get started.

<h2>Wanna go?</h2>
Going to a release party is simple. Look at <a href="http://community.kde.org/Promo/Events/Release_Parties/4.10">the wiki page</a> and find one in your eare. If there isn't one yet, keep checking - we keep adding them. Then, show up, bring a smile, and you're guaranteed to have a good time. You'll meet new people or old friends, you'll hear the latest gossip, learn new tricks, or just find out what this KDE thing is all about.

But if you aren't sure if there will be one, how about you organize your own party?

<h2>Wanna organize one?</h2>
With the success KDE is having, hosting a release party is a great job. You'll end up with a bunch of enthusiastic users and/or developers eating, drinking and swapping stories. No way that can go wrong!

Organizing one isn't too hard. You need to find a suitable venue, which can host your approximate number of guests. Don't sweat it: a cafe is a fine location, or your home or a room at the place where you work/study. Then, you need to get some merchandising ready, such as t-shirts, stickers, posters, etc., and get some speakers or activities people may find useful or interesting. Again, don't worry, the cool stuff™ we can send to you or you just print something and for content - just a demo of the new KDE followed by drinks is already a fine starter for the event. Once you've got these, you should add the details to this <a href="http://community.kde.org/Promo/Events/Release_Parties/4.10">wiki page</a> for people to know about the event. Now, go, promote the event: tell your friends, family, put up a facebook event - people will come! That's the basics. Of course, creativity leads to bonus points - we've seen Konqi constumes, birthday cakes and much more. But without those you'll have plenty of fun - see it's not that hard?

<h2>Or help out at one...</h2>
You should check <a href="http://community.kde.org/Promo/Events/Release_Parties/4.10">the wiki page</a> to see if there already are any Release Parties in your area. If you are not interested in organizing one, you may be able to assist with the organization of one close by!

Attending a party, helping out or organizing your own - I can assure you will have lots of fun!