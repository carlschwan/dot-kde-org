---
title: "Second Calligra Release Available"
date:    2012-08-15
authors:
  - "jriddell"
slug:    second-calligra-release-available
comments:
  - subject: "Fantastic progress"
    date: 2012-08-19
    body: "Cheers to all the developers, Calligra is clearly moving in the right direction.\r\n\r\nI have seen many of my own personal wishes for the software met, and now prefer it for paint/photo, vector graphics, and spreadsheets. (And if I ever made a presentation, a database, or a word processed document I'm sure I would prefer Calligra for those too.)\r\n\r\nKeep up the great work."
    author: ""
  - subject: "The best office suit for KDE."
    date: 2012-08-21
    body: "The best office suit for KDE. But only for Kubuntu ?! When it will be available for OpenSUSE 12.2."
    author: "Swadhin "
  - subject: "openSUSE packages"
    date: 2012-08-23
    body: "You can find KDE 4.9 and Calligra 2.5 for openSUSE 12.2 here: http://download.opensuse.org/repositories/KDE:/Release:/49/openSUSE_12.2"
    author: "Bille"
---
Calligra—the productivity suite from KDE—has <a href="http://www.calligra.org/news/calligra-2-5-released/">made its second release</a>.  The nine applications in the suite have all received new features and bug fixes, such as improved table editing in Words, neater cell editor in Sheets and a compositions docker in Krita for movie storyboard generation.
<!--break-->
<a href="http://www.calligra.org/news/calligra-2-5-released/"><img src="http://starsky.19inch.net/~jr/tmp/B9Q92.png" width="500" height="154" /></a>

There are new file format filters such as an MS Visio import filter and improved Microsoft OOXML import.  As an integrated suite, an improvement to one area often helps all applications. For example, a user profile can have different settings for work and home.

If you have not tried Calligra as your productivity suite, now is an excellent time to take a look.  File import support is top rate for Microsoft formats, Kexi (the database application) is slick and feature filled, Krita is a world-class art application.

Packages are <a href="http://www.kubuntu.org/news/calligra-2.5">available for Kubuntu</a>, who are evaluating it as their default office suite. Packages are expected to be released for other distros soon.  Packages for Microsoft Windows are available from Calligra consulting company <a href="http://www.kogmbh.com/">KO GmbH</a>.